.class Lcom/google/android/play/cardview/CardViewGroupDelegates$CardViewGroupEclairMr1;
.super Ljava/lang/Object;
.source "CardViewGroupDelegates.java"

# interfaces
.implements Lcom/google/android/play/cardview/CardViewGroupDelegate;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/play/cardview/CardViewGroupDelegates;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "CardViewGroupEclairMr1"
.end annotation


# direct methods
.method private constructor <init>()V
    .locals 0

    .prologue
    .line 105
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/google/android/play/cardview/CardViewGroupDelegates$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/google/android/play/cardview/CardViewGroupDelegates$1;

    .prologue
    .line 105
    invoke-direct {p0}, Lcom/google/android/play/cardview/CardViewGroupDelegates$CardViewGroupEclairMr1;-><init>()V

    return-void
.end method


# virtual methods
.method protected getBackgroundColor(Landroid/content/res/TypedArray;)Landroid/content/res/ColorStateList;
    .locals 1
    .param p1, "array"    # Landroid/content/res/TypedArray;

    .prologue
    .line 129
    sget v0, Lcom/google/android/play/R$styleable;->PlayCardViewGroup_playCardBackgroundColor:I

    invoke-virtual {p1, v0}, Landroid/content/res/TypedArray;->getColorStateList(I)Landroid/content/res/ColorStateList;

    move-result-object v0

    return-object v0
.end method

.method protected getElevation(Landroid/content/res/TypedArray;)F
    .locals 2
    .param p1, "array"    # Landroid/content/res/TypedArray;

    .prologue
    .line 137
    sget v0, Lcom/google/android/play/R$styleable;->PlayCardViewGroup_playCardElevation:I

    const/4 v1, 0x0

    invoke-virtual {p1, v0, v1}, Landroid/content/res/TypedArray;->getDimension(IF)F

    move-result v0

    return v0
.end method

.method protected getInset(Landroid/content/res/TypedArray;)I
    .locals 2
    .param p1, "array"    # Landroid/content/res/TypedArray;

    .prologue
    .line 141
    sget v0, Lcom/google/android/play/R$styleable;->PlayCardViewGroup_playCardInset:I

    const/4 v1, 0x0

    invoke-virtual {p1, v0, v1}, Landroid/content/res/TypedArray;->getDimensionPixelSize(II)I

    move-result v0

    return v0
.end method

.method protected getRadius(Landroid/content/res/TypedArray;)F
    .locals 2
    .param p1, "array"    # Landroid/content/res/TypedArray;

    .prologue
    .line 133
    sget v0, Lcom/google/android/play/R$styleable;->PlayCardViewGroup_playCardCornerRadius:I

    const/4 v1, 0x0

    invoke-virtual {p1, v0, v1}, Landroid/content/res/TypedArray;->getDimension(IF)F

    move-result v0

    return v0
.end method

.method protected getStyledAttrs(Landroid/content/Context;Landroid/util/AttributeSet;I)Landroid/content/res/TypedArray;
    .locals 2
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;
    .param p3, "defStyle"    # I

    .prologue
    .line 124
    sget-object v0, Lcom/google/android/play/R$styleable;->PlayCardViewGroup:[I

    const/4 v1, 0x0

    invoke-virtual {p1, p2, v0, p3, v1}, Landroid/content/Context;->obtainStyledAttributes(Landroid/util/AttributeSet;[III)Landroid/content/res/TypedArray;

    move-result-object v0

    return-object v0
.end method

.method public initialize(Landroid/view/View;Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 7
    .param p1, "cardView"    # Landroid/view/View;
    .param p2, "context"    # Landroid/content/Context;
    .param p3, "attrs"    # Landroid/util/AttributeSet;
    .param p4, "defStyle"    # I

    .prologue
    .line 111
    invoke-virtual {p0, p2, p3, p4}, Lcom/google/android/play/cardview/CardViewGroupDelegates$CardViewGroupEclairMr1;->getStyledAttrs(Landroid/content/Context;Landroid/util/AttributeSet;I)Landroid/content/res/TypedArray;

    move-result-object v6

    .line 112
    .local v6, "array":Landroid/content/res/TypedArray;
    new-instance v0, Lcom/google/android/play/cardview/RoundRectDrawableWithShadow;

    invoke-virtual {p2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {p0, v6}, Lcom/google/android/play/cardview/CardViewGroupDelegates$CardViewGroupEclairMr1;->getBackgroundColor(Landroid/content/res/TypedArray;)Landroid/content/res/ColorStateList;

    move-result-object v2

    invoke-virtual {p0, v6}, Lcom/google/android/play/cardview/CardViewGroupDelegates$CardViewGroupEclairMr1;->getRadius(Landroid/content/res/TypedArray;)F

    move-result v3

    invoke-virtual {p0, v6}, Lcom/google/android/play/cardview/CardViewGroupDelegates$CardViewGroupEclairMr1;->getElevation(Landroid/content/res/TypedArray;)F

    move-result v4

    invoke-virtual {p0, v6}, Lcom/google/android/play/cardview/CardViewGroupDelegates$CardViewGroupEclairMr1;->getInset(Landroid/content/res/TypedArray;)I

    move-result v5

    int-to-float v5, v5

    invoke-direct/range {v0 .. v5}, Lcom/google/android/play/cardview/RoundRectDrawableWithShadow;-><init>(Landroid/content/res/Resources;Landroid/content/res/ColorStateList;FFF)V

    .line 119
    .local v0, "cardBackground":Landroid/graphics/drawable/Drawable;
    invoke-virtual {p1, v0}, Landroid/view/View;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 120
    invoke-virtual {v6}, Landroid/content/res/TypedArray;->recycle()V

    .line 121
    return-void
.end method

.method public setBackgroundColor(Landroid/view/View;I)V
    .locals 3
    .param p1, "cardView"    # Landroid/view/View;
    .param p2, "color"    # I

    .prologue
    .line 146
    invoke-virtual {p1}, Landroid/view/View;->getBackground()Landroid/graphics/drawable/Drawable;

    move-result-object v0

    .line 147
    .local v0, "background":Landroid/graphics/drawable/Drawable;
    instance-of v1, v0, Lcom/google/android/play/cardview/CardViewBackgroundDrawable;

    if-eqz v1, :cond_0

    .line 148
    check-cast v0, Lcom/google/android/play/cardview/CardViewBackgroundDrawable;

    .end local v0    # "background":Landroid/graphics/drawable/Drawable;
    invoke-virtual {v0, p2}, Lcom/google/android/play/cardview/CardViewBackgroundDrawable;->setBackgroundColor(I)V

    .line 153
    :goto_0
    return-void

    .line 150
    .restart local v0    # "background":Landroid/graphics/drawable/Drawable;
    :cond_0
    const-string v1, "CardViewGroupDelegates"

    const-string v2, "Unable to set background color. CardView is not using a CardViewBackgroundDrawable"

    invoke-static {v1, v2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method public setBackgroundResource(Landroid/view/View;I)V
    .locals 6
    .param p1, "cardView"    # Landroid/view/View;
    .param p2, "resId"    # I

    .prologue
    .line 157
    if-nez p2, :cond_0

    .line 174
    :goto_0
    return-void

    .line 160
    :cond_0
    invoke-virtual {p1}, Landroid/view/View;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    .line 161
    .local v3, "resources":Landroid/content/res/Resources;
    invoke-virtual {p1}, Landroid/view/View;->getBackground()Landroid/graphics/drawable/Drawable;

    move-result-object v0

    .line 162
    .local v0, "background":Landroid/graphics/drawable/Drawable;
    instance-of v4, v0, Lcom/google/android/play/cardview/CardViewBackgroundDrawable;

    if-eqz v4, :cond_1

    .line 164
    :try_start_0
    invoke-virtual {v3, p2}, Landroid/content/res/Resources;->getColorStateList(I)Landroid/content/res/ColorStateList;

    move-result-object v1

    .line 165
    .local v1, "colorStateList":Landroid/content/res/ColorStateList;
    check-cast v0, Lcom/google/android/play/cardview/CardViewBackgroundDrawable;

    .end local v0    # "background":Landroid/graphics/drawable/Drawable;
    invoke-virtual {v0, v1}, Lcom/google/android/play/cardview/CardViewBackgroundDrawable;->setBackgroundColorStateList(Landroid/content/res/ColorStateList;)V
    :try_end_0
    .catch Landroid/content/res/Resources$NotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 167
    .end local v1    # "colorStateList":Landroid/content/res/ColorStateList;
    :catch_0
    move-exception v2

    .line 168
    .local v2, "ex":Landroid/content/res/Resources$NotFoundException;
    const-string v4, "CardViewGroupDelegates"

    const-string v5, "Unable to set background - ColorStateList not found."

    invoke-static {v4, v5, v2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_0

    .line 171
    .end local v2    # "ex":Landroid/content/res/Resources$NotFoundException;
    .restart local v0    # "background":Landroid/graphics/drawable/Drawable;
    :cond_1
    const-string v4, "CardViewGroupDelegates"

    const-string v5, "Unable to set background. CardView is not using a CardViewBackgroundDrawable."

    invoke-static {v4, v5}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method public setCardElevation(Landroid/view/View;F)V
    .locals 2
    .param p1, "cardView"    # Landroid/view/View;
    .param p2, "elevation"    # F

    .prologue
    .line 178
    invoke-virtual {p1}, Landroid/view/View;->getBackground()Landroid/graphics/drawable/Drawable;

    move-result-object v0

    .line 179
    .local v0, "background":Landroid/graphics/drawable/Drawable;
    instance-of v1, v0, Lcom/google/android/play/cardview/RoundRectDrawableWithShadow;

    if-eqz v1, :cond_0

    .line 180
    check-cast v0, Lcom/google/android/play/cardview/RoundRectDrawableWithShadow;

    .end local v0    # "background":Landroid/graphics/drawable/Drawable;
    invoke-virtual {v0, p2}, Lcom/google/android/play/cardview/RoundRectDrawableWithShadow;->setShadowSize(F)V

    .line 182
    :cond_0
    return-void
.end method
