.class Lcom/google/android/play/layout/FlowLayoutManager$NestedFlowLineInfo;
.super Lcom/google/android/play/layout/FlowLayoutManager$LineInfo;
.source "FlowLayoutManager.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/play/layout/FlowLayoutManager;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "NestedFlowLineInfo"
.end annotation


# static fields
.field private static final sPool:Landroid/support/v4/util/Pools$Pool;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/support/v4/util/Pools$Pool",
            "<",
            "Lcom/google/android/play/layout/FlowLayoutManager$NestedFlowLineInfo;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public mCreator:Lcom/google/android/play/layout/FlowLayoutManager$ItemInfo;

.field public mCreatorHeightWrapsChildFlow:Z

.field public mExtraHeight:I

.field public mFlowHeight:I

.field public mFlowInsetBottom:I

.field public mFlowInsetTop:I

.field public mFlowStartOffset:I

.field public mFlowWidth:I

.field public mParagraph:Lcom/google/android/play/layout/FlowLayoutManager$ParagraphInfo;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 1354
    new-instance v0, Landroid/support/v4/util/Pools$SimplePool;

    const/16 v1, 0xa

    invoke-direct {v0, v1}, Landroid/support/v4/util/Pools$SimplePool;-><init>(I)V

    sput-object v0, Lcom/google/android/play/layout/FlowLayoutManager$NestedFlowLineInfo;->sPool:Landroid/support/v4/util/Pools$Pool;

    return-void
.end method

.method private constructor <init>()V
    .locals 1

    .prologue
    .line 1416
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/google/android/play/layout/FlowLayoutManager$LineInfo;-><init>(Lcom/google/android/play/layout/FlowLayoutManager$1;)V

    .line 1417
    invoke-virtual {p0}, Lcom/google/android/play/layout/FlowLayoutManager$NestedFlowLineInfo;->reset()V

    .line 1418
    return-void
.end method

.method private init(Lcom/google/android/play/layout/FlowLayoutManager$ItemInfo;Lcom/google/android/play/layout/FlowLayoutManager$LayoutParams;I)V
    .locals 10
    .param p1, "measuredCreator"    # Lcom/google/android/play/layout/FlowLayoutManager$ItemInfo;
    .param p2, "layoutParams"    # Lcom/google/android/play/layout/FlowLayoutManager$LayoutParams;
    .param p3, "lineWidth"    # I

    .prologue
    const/4 v6, 0x1

    const/4 v7, 0x0

    .line 1421
    iget-boolean v8, p1, Lcom/google/android/play/layout/FlowLayoutManager$ItemInfo;->mMeasuredInCurrentPass:Z

    if-nez v8, :cond_0

    .line 1422
    new-instance v6, Ljava/lang/IllegalArgumentException;

    const-string v7, "creator not measured"

    invoke-direct {v6, v7}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v6

    .line 1424
    :cond_0
    iget v8, p2, Lcom/google/android/play/layout/FlowLayoutManager$LayoutParams;->flow:I

    and-int/lit8 v8, v8, 0x4

    if-eqz v8, :cond_1

    move v4, v6

    .line 1425
    .local v4, "flowUnder":Z
    :goto_0
    iget v8, p2, Lcom/google/android/play/layout/FlowLayoutManager$LayoutParams;->flow:I

    and-int/lit8 v8, v8, 0x2

    if-eqz v8, :cond_2

    move v0, v6

    .line 1426
    .local v0, "flowEnd":Z
    :goto_1
    iget v8, p2, Lcom/google/android/play/layout/FlowLayoutManager$LayoutParams;->flow:I

    and-int/lit8 v8, v8, 0x1

    if-eqz v8, :cond_3

    move v3, v6

    .line 1427
    .local v3, "flowStart":Z
    :goto_2
    if-nez v4, :cond_4

    if-nez v0, :cond_4

    if-nez v3, :cond_4

    .line 1428
    new-instance v6, Ljava/lang/IllegalArgumentException;

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "Unknown flow value: 0x"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    iget v8, p2, Lcom/google/android/play/layout/FlowLayoutManager$LayoutParams;->flow:I

    invoke-static {v8}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-direct {v6, v7}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v6

    .end local v0    # "flowEnd":Z
    .end local v3    # "flowStart":Z
    .end local v4    # "flowUnder":Z
    :cond_1
    move v4, v7

    .line 1424
    goto :goto_0

    .restart local v4    # "flowUnder":Z
    :cond_2
    move v0, v7

    .line 1425
    goto :goto_1

    .restart local v0    # "flowEnd":Z
    :cond_3
    move v3, v7

    .line 1426
    goto :goto_2

    .line 1436
    .restart local v3    # "flowStart":Z
    :cond_4
    iput-object p1, p0, Lcom/google/android/play/layout/FlowLayoutManager$NestedFlowLineInfo;->mCreator:Lcom/google/android/play/layout/FlowLayoutManager$ItemInfo;

    .line 1437
    iget v8, p2, Lcom/google/android/play/layout/FlowLayoutManager$LayoutParams;->heightCompound:I

    const/4 v9, -0x4

    if-ne v8, v9, :cond_9

    :goto_3
    iput-boolean v6, p0, Lcom/google/android/play/layout/FlowLayoutManager$NestedFlowLineInfo;->mCreatorHeightWrapsChildFlow:Z

    .line 1439
    if-eqz v4, :cond_a

    move v5, v7

    .line 1441
    .local v5, "widthUsedByCreator":I
    :goto_4
    iget-object v6, p0, Lcom/google/android/play/layout/FlowLayoutManager$NestedFlowLineInfo;->mCreator:Lcom/google/android/play/layout/FlowLayoutManager$ItemInfo;

    iget v6, v6, Lcom/google/android/play/layout/FlowLayoutManager$ItemInfo;->mGridCellSize:F

    invoke-virtual {p2, v6}, Lcom/google/android/play/layout/FlowLayoutManager$LayoutParams;->getFlowInsetStart(F)I

    move-result v2

    .line 1442
    .local v2, "flowInsetStart":I
    iget-object v6, p0, Lcom/google/android/play/layout/FlowLayoutManager$NestedFlowLineInfo;->mCreator:Lcom/google/android/play/layout/FlowLayoutManager$ItemInfo;

    iget v6, v6, Lcom/google/android/play/layout/FlowLayoutManager$ItemInfo;->mGridCellSize:F

    invoke-virtual {p2, v6}, Lcom/google/android/play/layout/FlowLayoutManager$LayoutParams;->getFlowInsetEnd(F)I

    move-result v1

    .line 1448
    .local v1, "flowInsetEnd":I
    if-nez v0, :cond_5

    if-eqz v4, :cond_6

    :cond_5
    iget v6, p2, Lcom/google/android/play/layout/FlowLayoutManager$LayoutParams;->flowInsetStartCompound:I

    invoke-static {v6}, Lcom/google/android/play/utils/Compound;->isCompoundFloat(I)Z

    move-result v6

    if-eqz v6, :cond_6

    iget-object v6, p0, Lcom/google/android/play/layout/FlowLayoutManager$NestedFlowLineInfo;->mCreator:Lcom/google/android/play/layout/FlowLayoutManager$ItemInfo;

    iget v6, v6, Lcom/google/android/play/layout/FlowLayoutManager$ItemInfo;->mGridInsetStart:I

    if-eqz v6, :cond_6

    iget v6, p0, Lcom/google/android/play/layout/FlowLayoutManager$NestedFlowLineInfo;->mOffsetStart:I

    iget-object v8, p0, Lcom/google/android/play/layout/FlowLayoutManager$NestedFlowLineInfo;->mCreator:Lcom/google/android/play/layout/FlowLayoutManager$ItemInfo;

    iget v8, v8, Lcom/google/android/play/layout/FlowLayoutManager$ItemInfo;->mGridInsetStart:I

    if-ge v6, v8, :cond_6

    .line 1452
    iget-object v6, p0, Lcom/google/android/play/layout/FlowLayoutManager$NestedFlowLineInfo;->mCreator:Lcom/google/android/play/layout/FlowLayoutManager$ItemInfo;

    iget v6, v6, Lcom/google/android/play/layout/FlowLayoutManager$ItemInfo;->mGridInsetStart:I

    iget v8, p0, Lcom/google/android/play/layout/FlowLayoutManager$NestedFlowLineInfo;->mOffsetStart:I

    sub-int/2addr v6, v8

    add-int/2addr v2, v6

    .line 1455
    :cond_6
    iget-object v6, p0, Lcom/google/android/play/layout/FlowLayoutManager$NestedFlowLineInfo;->mCreator:Lcom/google/android/play/layout/FlowLayoutManager$ItemInfo;

    iget v6, v6, Lcom/google/android/play/layout/FlowLayoutManager$ItemInfo;->mGridCellSize:F

    invoke-virtual {p2, v6}, Lcom/google/android/play/layout/FlowLayoutManager$LayoutParams;->getFlowWidth(F)I

    move-result v6

    iput v6, p0, Lcom/google/android/play/layout/FlowLayoutManager$NestedFlowLineInfo;->mFlowWidth:I

    .line 1456
    iget v6, p0, Lcom/google/android/play/layout/FlowLayoutManager$NestedFlowLineInfo;->mFlowWidth:I

    if-gez v6, :cond_7

    .line 1458
    sub-int v6, p3, v5

    sub-int/2addr v6, v2

    sub-int/2addr v6, v1

    invoke-static {v7, v6}, Ljava/lang/Math;->max(II)I

    move-result v6

    iput v6, p0, Lcom/google/android/play/layout/FlowLayoutManager$NestedFlowLineInfo;->mFlowWidth:I

    .line 1462
    :cond_7
    if-eqz v0, :cond_b

    .line 1464
    iget-object v6, p0, Lcom/google/android/play/layout/FlowLayoutManager$NestedFlowLineInfo;->mCreator:Lcom/google/android/play/layout/FlowLayoutManager$ItemInfo;

    iget-object v8, p0, Lcom/google/android/play/layout/FlowLayoutManager$NestedFlowLineInfo;->mCreator:Lcom/google/android/play/layout/FlowLayoutManager$ItemInfo;

    iget v8, v8, Lcom/google/android/play/layout/FlowLayoutManager$ItemInfo;->mMarginEnd:I

    sub-int v8, p3, v8

    iget-object v9, p0, Lcom/google/android/play/layout/FlowLayoutManager$NestedFlowLineInfo;->mCreator:Lcom/google/android/play/layout/FlowLayoutManager$ItemInfo;

    iget v9, v9, Lcom/google/android/play/layout/FlowLayoutManager$ItemInfo;->mDecoratedWidth:I

    sub-int/2addr v8, v9

    iput v8, v6, Lcom/google/android/play/layout/FlowLayoutManager$ItemInfo;->mMarginStart:I

    .line 1466
    sub-int v6, p3, v5

    sub-int/2addr v6, v1

    iget v8, p0, Lcom/google/android/play/layout/FlowLayoutManager$NestedFlowLineInfo;->mFlowWidth:I

    sub-int/2addr v6, v8

    iput v6, p0, Lcom/google/android/play/layout/FlowLayoutManager$NestedFlowLineInfo;->mFlowStartOffset:I

    .line 1473
    :goto_5
    iget-object v6, p0, Lcom/google/android/play/layout/FlowLayoutManager$NestedFlowLineInfo;->mCreator:Lcom/google/android/play/layout/FlowLayoutManager$ItemInfo;

    iget v6, v6, Lcom/google/android/play/layout/FlowLayoutManager$ItemInfo;->mGridCellSize:F

    invoke-virtual {p2, v6}, Lcom/google/android/play/layout/FlowLayoutManager$LayoutParams;->getFlowInsetTop(F)I

    move-result v6

    iput v6, p0, Lcom/google/android/play/layout/FlowLayoutManager$NestedFlowLineInfo;->mFlowInsetTop:I

    .line 1474
    iget-object v6, p0, Lcom/google/android/play/layout/FlowLayoutManager$NestedFlowLineInfo;->mCreator:Lcom/google/android/play/layout/FlowLayoutManager$ItemInfo;

    iget v6, v6, Lcom/google/android/play/layout/FlowLayoutManager$ItemInfo;->mGridCellSize:F

    invoke-virtual {p2, v6}, Lcom/google/android/play/layout/FlowLayoutManager$LayoutParams;->getFlowInsetBottom(F)I

    move-result v6

    iput v6, p0, Lcom/google/android/play/layout/FlowLayoutManager$NestedFlowLineInfo;->mFlowInsetBottom:I

    .line 1475
    iget-object v6, p0, Lcom/google/android/play/layout/FlowLayoutManager$NestedFlowLineInfo;->mCreator:Lcom/google/android/play/layout/FlowLayoutManager$ItemInfo;

    iget v6, v6, Lcom/google/android/play/layout/FlowLayoutManager$ItemInfo;->mGridCellSize:F

    invoke-virtual {p2, v6}, Lcom/google/android/play/layout/FlowLayoutManager$LayoutParams;->getFlowHeight(F)I

    move-result v6

    iput v6, p0, Lcom/google/android/play/layout/FlowLayoutManager$NestedFlowLineInfo;->mFlowHeight:I

    .line 1476
    iget v6, p0, Lcom/google/android/play/layout/FlowLayoutManager$NestedFlowLineInfo;->mFlowHeight:I

    if-gez v6, :cond_8

    .line 1478
    iget-object v6, p0, Lcom/google/android/play/layout/FlowLayoutManager$NestedFlowLineInfo;->mCreator:Lcom/google/android/play/layout/FlowLayoutManager$ItemInfo;

    iget v6, v6, Lcom/google/android/play/layout/FlowLayoutManager$ItemInfo;->mDecoratedHeight:I

    iget v8, p0, Lcom/google/android/play/layout/FlowLayoutManager$NestedFlowLineInfo;->mFlowInsetTop:I

    sub-int/2addr v6, v8

    iget v8, p0, Lcom/google/android/play/layout/FlowLayoutManager$NestedFlowLineInfo;->mFlowInsetBottom:I

    sub-int/2addr v6, v8

    invoke-static {v7, v6}, Ljava/lang/Math;->max(II)I

    move-result v6

    iput v6, p0, Lcom/google/android/play/layout/FlowLayoutManager$NestedFlowLineInfo;->mFlowHeight:I

    .line 1481
    :cond_8
    return-void

    .end local v1    # "flowInsetEnd":I
    .end local v2    # "flowInsetStart":I
    .end local v5    # "widthUsedByCreator":I
    :cond_9
    move v6, v7

    .line 1437
    goto/16 :goto_3

    .line 1439
    :cond_a
    iget-object v6, p0, Lcom/google/android/play/layout/FlowLayoutManager$NestedFlowLineInfo;->mCreator:Lcom/google/android/play/layout/FlowLayoutManager$ItemInfo;

    iget v6, v6, Lcom/google/android/play/layout/FlowLayoutManager$ItemInfo;->mMarginStart:I

    iget-object v8, p0, Lcom/google/android/play/layout/FlowLayoutManager$NestedFlowLineInfo;->mCreator:Lcom/google/android/play/layout/FlowLayoutManager$ItemInfo;

    iget v8, v8, Lcom/google/android/play/layout/FlowLayoutManager$ItemInfo;->mDecoratedWidth:I

    add-int/2addr v6, v8

    iget-object v8, p0, Lcom/google/android/play/layout/FlowLayoutManager$NestedFlowLineInfo;->mCreator:Lcom/google/android/play/layout/FlowLayoutManager$ItemInfo;

    iget v8, v8, Lcom/google/android/play/layout/FlowLayoutManager$ItemInfo;->mMarginEnd:I

    add-int v5, v6, v8

    goto/16 :goto_4

    .line 1470
    .restart local v1    # "flowInsetEnd":I
    .restart local v2    # "flowInsetStart":I
    .restart local v5    # "widthUsedByCreator":I
    :cond_b
    add-int v6, v5, v2

    iput v6, p0, Lcom/google/android/play/layout/FlowLayoutManager$NestedFlowLineInfo;->mFlowStartOffset:I

    goto :goto_5
.end method

.method public static obtain(IIILcom/google/android/play/layout/FlowLayoutManager$ItemInfo;Lcom/google/android/play/layout/FlowLayoutManager$LayoutParams;)Lcom/google/android/play/layout/FlowLayoutManager$NestedFlowLineInfo;
    .locals 2
    .param p0, "positionStart"    # I
    .param p1, "lineWidth"    # I
    .param p2, "offsetStart"    # I
    .param p3, "measuredCreator"    # Lcom/google/android/play/layout/FlowLayoutManager$ItemInfo;
    .param p4, "creatorLayoutParams"    # Lcom/google/android/play/layout/FlowLayoutManager$LayoutParams;

    .prologue
    .line 1359
    sget-object v1, Lcom/google/android/play/layout/FlowLayoutManager$NestedFlowLineInfo;->sPool:Landroid/support/v4/util/Pools$Pool;

    invoke-interface {v1}, Landroid/support/v4/util/Pools$Pool;->acquire()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/play/layout/FlowLayoutManager$NestedFlowLineInfo;

    .line 1360
    .local v0, "line":Lcom/google/android/play/layout/FlowLayoutManager$NestedFlowLineInfo;
    if-nez v0, :cond_0

    .line 1361
    new-instance v0, Lcom/google/android/play/layout/FlowLayoutManager$NestedFlowLineInfo;

    .end local v0    # "line":Lcom/google/android/play/layout/FlowLayoutManager$NestedFlowLineInfo;
    invoke-direct {v0}, Lcom/google/android/play/layout/FlowLayoutManager$NestedFlowLineInfo;-><init>()V

    .line 1363
    .restart local v0    # "line":Lcom/google/android/play/layout/FlowLayoutManager$NestedFlowLineInfo;
    :cond_0
    iput p0, v0, Lcom/google/android/play/layout/FlowLayoutManager$NestedFlowLineInfo;->mPositionStart:I

    .line 1364
    iput p2, v0, Lcom/google/android/play/layout/FlowLayoutManager$NestedFlowLineInfo;->mOffsetStart:I

    .line 1365
    invoke-direct {v0, p3, p4, p1}, Lcom/google/android/play/layout/FlowLayoutManager$NestedFlowLineInfo;->init(Lcom/google/android/play/layout/FlowLayoutManager$ItemInfo;Lcom/google/android/play/layout/FlowLayoutManager$LayoutParams;I)V

    .line 1366
    return-object v0
.end method


# virtual methods
.method public clearMeasuredInCurrentPass()V
    .locals 2

    .prologue
    .line 1485
    iget-object v0, p0, Lcom/google/android/play/layout/FlowLayoutManager$NestedFlowLineInfo;->mCreator:Lcom/google/android/play/layout/FlowLayoutManager$ItemInfo;

    if-eqz v0, :cond_0

    .line 1486
    iget-object v0, p0, Lcom/google/android/play/layout/FlowLayoutManager$NestedFlowLineInfo;->mCreator:Lcom/google/android/play/layout/FlowLayoutManager$ItemInfo;

    const/4 v1, 0x0

    iput-boolean v1, v0, Lcom/google/android/play/layout/FlowLayoutManager$ItemInfo;->mMeasuredInCurrentPass:Z

    .line 1488
    :cond_0
    iget-object v0, p0, Lcom/google/android/play/layout/FlowLayoutManager$NestedFlowLineInfo;->mParagraph:Lcom/google/android/play/layout/FlowLayoutManager$ParagraphInfo;

    if-eqz v0, :cond_1

    .line 1489
    iget-object v0, p0, Lcom/google/android/play/layout/FlowLayoutManager$NestedFlowLineInfo;->mParagraph:Lcom/google/android/play/layout/FlowLayoutManager$ParagraphInfo;

    invoke-virtual {v0}, Lcom/google/android/play/layout/FlowLayoutManager$ParagraphInfo;->clearMeasuredInCurrentPass()V

    .line 1491
    :cond_1
    return-void
.end method

.method debugPrint(ILjava/lang/StringBuilder;)V
    .locals 2
    .param p1, "lineIndex"    # I
    .param p2, "output"    # Ljava/lang/StringBuilder;

    .prologue
    .line 1607
    const/16 v0, 0x40

    invoke-virtual {p2, v0}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lcom/google/android/play/layout/FlowLayoutManager$NestedFlowLineInfo;->mPositionStart:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "(flow"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1608
    iget-object v0, p0, Lcom/google/android/play/layout/FlowLayoutManager$NestedFlowLineInfo;->mParagraph:Lcom/google/android/play/layout/FlowLayoutManager$ParagraphInfo;

    if-eqz v0, :cond_0

    .line 1609
    iget-object v0, p0, Lcom/google/android/play/layout/FlowLayoutManager$NestedFlowLineInfo;->mParagraph:Lcom/google/android/play/layout/FlowLayoutManager$ParagraphInfo;

    invoke-virtual {v0, p2}, Lcom/google/android/play/layout/FlowLayoutManager$ParagraphInfo;->debugPrint(Ljava/lang/StringBuilder;)V

    .line 1613
    :goto_0
    const/16 v0, 0x29

    invoke-virtual {p2, v0}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 1614
    return-void

    .line 1611
    :cond_0
    const-string v0, "{}"

    invoke-virtual {p2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_0
.end method

.method public getItemTopOffset(I)I
    .locals 2
    .param p1, "position"    # I

    .prologue
    .line 1545
    iget v0, p0, Lcom/google/android/play/layout/FlowLayoutManager$NestedFlowLineInfo;->mPositionStart:I

    if-ne p1, v0, :cond_0

    .line 1546
    iget-object v0, p0, Lcom/google/android/play/layout/FlowLayoutManager$NestedFlowLineInfo;->mCreator:Lcom/google/android/play/layout/FlowLayoutManager$ItemInfo;

    iget v0, v0, Lcom/google/android/play/layout/FlowLayoutManager$ItemInfo;->mTopOffset:I

    .line 1550
    :goto_0
    return v0

    .line 1547
    :cond_0
    iget-object v0, p0, Lcom/google/android/play/layout/FlowLayoutManager$NestedFlowLineInfo;->mParagraph:Lcom/google/android/play/layout/FlowLayoutManager$ParagraphInfo;

    if-eqz v0, :cond_1

    .line 1548
    iget-object v0, p0, Lcom/google/android/play/layout/FlowLayoutManager$NestedFlowLineInfo;->mCreator:Lcom/google/android/play/layout/FlowLayoutManager$ItemInfo;

    iget v0, v0, Lcom/google/android/play/layout/FlowLayoutManager$ItemInfo;->mTopOffset:I

    iget v1, p0, Lcom/google/android/play/layout/FlowLayoutManager$NestedFlowLineInfo;->mFlowInsetTop:I

    add-int/2addr v0, v1

    iget-object v1, p0, Lcom/google/android/play/layout/FlowLayoutManager$NestedFlowLineInfo;->mParagraph:Lcom/google/android/play/layout/FlowLayoutManager$ParagraphInfo;

    invoke-virtual {v1, p1}, Lcom/google/android/play/layout/FlowLayoutManager$ParagraphInfo;->getItemTopOffset(I)I

    move-result v1

    add-int/2addr v0, v1

    goto :goto_0

    .line 1550
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected invalidateFromInternal(I)Z
    .locals 3
    .param p1, "position"    # I

    .prologue
    const/4 v1, 0x1

    const/4 v0, 0x0

    .line 1556
    iget-object v2, p0, Lcom/google/android/play/layout/FlowLayoutManager$NestedFlowLineInfo;->mParagraph:Lcom/google/android/play/layout/FlowLayoutManager$ParagraphInfo;

    if-nez v2, :cond_0

    .line 1568
    :goto_0
    :pswitch_0
    return v0

    .line 1560
    :cond_0
    iget-object v2, p0, Lcom/google/android/play/layout/FlowLayoutManager$NestedFlowLineInfo;->mParagraph:Lcom/google/android/play/layout/FlowLayoutManager$ParagraphInfo;

    invoke-virtual {v2, p1}, Lcom/google/android/play/layout/FlowLayoutManager$ParagraphInfo;->invalidateFrom(I)I

    move-result v2

    packed-switch v2, :pswitch_data_0

    :pswitch_1
    move v0, v1

    .line 1568
    goto :goto_0

    .line 1564
    :pswitch_2
    iget-object v0, p0, Lcom/google/android/play/layout/FlowLayoutManager$NestedFlowLineInfo;->mParagraph:Lcom/google/android/play/layout/FlowLayoutManager$ParagraphInfo;

    invoke-virtual {v0}, Lcom/google/android/play/layout/FlowLayoutManager$ParagraphInfo;->recycle()V

    .line 1565
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/play/layout/FlowLayoutManager$NestedFlowLineInfo;->mParagraph:Lcom/google/android/play/layout/FlowLayoutManager$ParagraphInfo;

    move v0, v1

    .line 1566
    goto :goto_0

    .line 1560
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method public offsetPositions(I)V
    .locals 1
    .param p1, "delta"    # I

    .prologue
    .line 1574
    invoke-super {p0, p1}, Lcom/google/android/play/layout/FlowLayoutManager$LineInfo;->offsetPositions(I)V

    .line 1575
    iget-object v0, p0, Lcom/google/android/play/layout/FlowLayoutManager$NestedFlowLineInfo;->mParagraph:Lcom/google/android/play/layout/FlowLayoutManager$ParagraphInfo;

    if-eqz v0, :cond_0

    .line 1576
    iget-object v0, p0, Lcom/google/android/play/layout/FlowLayoutManager$NestedFlowLineInfo;->mParagraph:Lcom/google/android/play/layout/FlowLayoutManager$ParagraphInfo;

    invoke-virtual {v0, p1}, Lcom/google/android/play/layout/FlowLayoutManager$ParagraphInfo;->offsetPositions(I)V

    .line 1578
    :cond_0
    return-void
.end method

.method protected onArrange(ZI)I
    .locals 8
    .param p1, "fullArrangementRequired"    # Z
    .param p2, "totalItemCount"    # I

    .prologue
    const/4 v5, 0x1

    const/4 v6, 0x0

    .line 1508
    iget-object v7, p0, Lcom/google/android/play/layout/FlowLayoutManager$NestedFlowLineInfo;->mCreator:Lcom/google/android/play/layout/FlowLayoutManager$ItemInfo;

    if-nez v7, :cond_0

    .line 1539
    :goto_0
    return v6

    .line 1513
    :cond_0
    iget v7, p0, Lcom/google/android/play/layout/FlowLayoutManager$NestedFlowLineInfo;->mPositionStart:I

    if-nez v7, :cond_1

    move v2, v5

    .line 1514
    .local v2, "isFirstLine":Z
    :goto_1
    iget v7, p0, Lcom/google/android/play/layout/FlowLayoutManager$NestedFlowLineInfo;->mPositionStart:I

    add-int/lit8 v7, v7, 0x1

    if-ne v7, p2, :cond_2

    move v3, v5

    .line 1515
    .local v3, "isLastLine":Z
    :goto_2
    iget-object v7, p0, Lcom/google/android/play/layout/FlowLayoutManager$NestedFlowLineInfo;->mCreator:Lcom/google/android/play/layout/FlowLayoutManager$ItemInfo;

    if-eqz v2, :cond_3

    iget-object v5, p0, Lcom/google/android/play/layout/FlowLayoutManager$NestedFlowLineInfo;->mCreator:Lcom/google/android/play/layout/FlowLayoutManager$ItemInfo;

    iget v5, v5, Lcom/google/android/play/layout/FlowLayoutManager$ItemInfo;->mMarginTopForFirstLine:I

    :goto_3
    iput v5, v7, Lcom/google/android/play/layout/FlowLayoutManager$ItemInfo;->mTopOffset:I

    .line 1517
    iget-object v5, p0, Lcom/google/android/play/layout/FlowLayoutManager$NestedFlowLineInfo;->mCreator:Lcom/google/android/play/layout/FlowLayoutManager$ItemInfo;

    iget v5, v5, Lcom/google/android/play/layout/FlowLayoutManager$ItemInfo;->mTopOffset:I

    iget-object v7, p0, Lcom/google/android/play/layout/FlowLayoutManager$NestedFlowLineInfo;->mCreator:Lcom/google/android/play/layout/FlowLayoutManager$ItemInfo;

    iget v7, v7, Lcom/google/android/play/layout/FlowLayoutManager$ItemInfo;->mDecoratedHeight:I

    add-int/2addr v7, v5

    if-eqz v3, :cond_4

    iget-object v5, p0, Lcom/google/android/play/layout/FlowLayoutManager$NestedFlowLineInfo;->mCreator:Lcom/google/android/play/layout/FlowLayoutManager$ItemInfo;

    iget v5, v5, Lcom/google/android/play/layout/FlowLayoutManager$ItemInfo;->mMarginBottomForLastLine:I

    :goto_4
    add-int/2addr v5, v7

    invoke-static {v6, v5}, Ljava/lang/Math;->max(II)I

    move-result v0

    .line 1521
    .local v0, "creatorHeight":I
    iget-object v5, p0, Lcom/google/android/play/layout/FlowLayoutManager$NestedFlowLineInfo;->mParagraph:Lcom/google/android/play/layout/FlowLayoutManager$ParagraphInfo;

    if-eqz v5, :cond_5

    iget-object v5, p0, Lcom/google/android/play/layout/FlowLayoutManager$NestedFlowLineInfo;->mParagraph:Lcom/google/android/play/layout/FlowLayoutManager$ParagraphInfo;

    invoke-virtual {v5, p2}, Lcom/google/android/play/layout/FlowLayoutManager$ParagraphInfo;->arrangeIfNecessary(I)I

    move-result v4

    .line 1524
    .local v4, "paragraphHeight":I
    :goto_5
    iget-boolean v5, p0, Lcom/google/android/play/layout/FlowLayoutManager$NestedFlowLineInfo;->mCreatorHeightWrapsChildFlow:Z

    if-eqz v5, :cond_6

    .line 1527
    iget v5, p0, Lcom/google/android/play/layout/FlowLayoutManager$NestedFlowLineInfo;->mFlowInsetTop:I

    add-int/2addr v5, v4

    iget v7, p0, Lcom/google/android/play/layout/FlowLayoutManager$NestedFlowLineInfo;->mFlowInsetBottom:I

    add-int/2addr v5, v7

    iget-object v7, p0, Lcom/google/android/play/layout/FlowLayoutManager$NestedFlowLineInfo;->mCreator:Lcom/google/android/play/layout/FlowLayoutManager$ItemInfo;

    iget v7, v7, Lcom/google/android/play/layout/FlowLayoutManager$ItemInfo;->mDecoratedHeight:I

    sub-int/2addr v5, v7

    invoke-static {v6, v5}, Ljava/lang/Math;->max(II)I

    move-result v5

    iput v5, p0, Lcom/google/android/play/layout/FlowLayoutManager$NestedFlowLineInfo;->mExtraHeight:I

    .line 1529
    iget v5, p0, Lcom/google/android/play/layout/FlowLayoutManager$NestedFlowLineInfo;->mFlowHeight:I

    invoke-static {v5, v4}, Ljava/lang/Math;->max(II)I

    move-result v5

    iget v6, p0, Lcom/google/android/play/layout/FlowLayoutManager$NestedFlowLineInfo;->mFlowInsetBottom:I

    add-int v1, v5, v6

    .line 1539
    .local v1, "flowContextHeightPlusInsetBottom":I
    :goto_6
    iget-object v5, p0, Lcom/google/android/play/layout/FlowLayoutManager$NestedFlowLineInfo;->mCreator:Lcom/google/android/play/layout/FlowLayoutManager$ItemInfo;

    iget v5, v5, Lcom/google/android/play/layout/FlowLayoutManager$ItemInfo;->mTopOffset:I

    iget v6, p0, Lcom/google/android/play/layout/FlowLayoutManager$NestedFlowLineInfo;->mFlowInsetTop:I

    add-int/2addr v5, v6

    add-int/2addr v5, v1

    invoke-static {v0, v5}, Ljava/lang/Math;->max(II)I

    move-result v6

    goto :goto_0

    .end local v0    # "creatorHeight":I
    .end local v1    # "flowContextHeightPlusInsetBottom":I
    .end local v2    # "isFirstLine":Z
    .end local v3    # "isLastLine":Z
    .end local v4    # "paragraphHeight":I
    :cond_1
    move v2, v6

    .line 1513
    goto :goto_1

    .restart local v2    # "isFirstLine":Z
    :cond_2
    move v3, v6

    .line 1514
    goto :goto_2

    .line 1515
    .restart local v3    # "isLastLine":Z
    :cond_3
    iget-object v5, p0, Lcom/google/android/play/layout/FlowLayoutManager$NestedFlowLineInfo;->mCreator:Lcom/google/android/play/layout/FlowLayoutManager$ItemInfo;

    iget v5, v5, Lcom/google/android/play/layout/FlowLayoutManager$ItemInfo;->mMarginTop:I

    goto :goto_3

    .line 1517
    :cond_4
    iget-object v5, p0, Lcom/google/android/play/layout/FlowLayoutManager$NestedFlowLineInfo;->mCreator:Lcom/google/android/play/layout/FlowLayoutManager$ItemInfo;

    iget v5, v5, Lcom/google/android/play/layout/FlowLayoutManager$ItemInfo;->mMarginBottom:I

    goto :goto_4

    .restart local v0    # "creatorHeight":I
    :cond_5
    move v4, v6

    .line 1521
    goto :goto_5

    .line 1534
    .restart local v4    # "paragraphHeight":I
    :cond_6
    iput v6, p0, Lcom/google/android/play/layout/FlowLayoutManager$NestedFlowLineInfo;->mExtraHeight:I

    .line 1535
    iget v5, p0, Lcom/google/android/play/layout/FlowLayoutManager$NestedFlowLineInfo;->mFlowHeight:I

    iget v6, p0, Lcom/google/android/play/layout/FlowLayoutManager$NestedFlowLineInfo;->mFlowInsetBottom:I

    add-int/2addr v5, v6

    invoke-static {v5, v4}, Ljava/lang/Math;->max(II)I

    move-result v1

    .restart local v1    # "flowContextHeightPlusInsetBottom":I
    goto :goto_6
.end method

.method public recycle()V
    .locals 1

    .prologue
    .line 1582
    invoke-virtual {p0}, Lcom/google/android/play/layout/FlowLayoutManager$NestedFlowLineInfo;->reset()V

    .line 1583
    sget-object v0, Lcom/google/android/play/layout/FlowLayoutManager$NestedFlowLineInfo;->sPool:Landroid/support/v4/util/Pools$Pool;

    invoke-interface {v0, p0}, Landroid/support/v4/util/Pools$Pool;->release(Ljava/lang/Object;)Z

    .line 1584
    return-void
.end method

.method protected reset()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x0

    .line 1588
    invoke-super {p0}, Lcom/google/android/play/layout/FlowLayoutManager$LineInfo;->reset()V

    .line 1589
    iget-object v0, p0, Lcom/google/android/play/layout/FlowLayoutManager$NestedFlowLineInfo;->mCreator:Lcom/google/android/play/layout/FlowLayoutManager$ItemInfo;

    if-eqz v0, :cond_0

    .line 1590
    iget-object v0, p0, Lcom/google/android/play/layout/FlowLayoutManager$NestedFlowLineInfo;->mCreator:Lcom/google/android/play/layout/FlowLayoutManager$ItemInfo;

    invoke-virtual {v0}, Lcom/google/android/play/layout/FlowLayoutManager$ItemInfo;->recycle()V

    .line 1591
    iput-object v2, p0, Lcom/google/android/play/layout/FlowLayoutManager$NestedFlowLineInfo;->mCreator:Lcom/google/android/play/layout/FlowLayoutManager$ItemInfo;

    .line 1593
    :cond_0
    iput v1, p0, Lcom/google/android/play/layout/FlowLayoutManager$NestedFlowLineInfo;->mFlowWidth:I

    .line 1594
    iput v1, p0, Lcom/google/android/play/layout/FlowLayoutManager$NestedFlowLineInfo;->mFlowHeight:I

    .line 1595
    iput v1, p0, Lcom/google/android/play/layout/FlowLayoutManager$NestedFlowLineInfo;->mFlowStartOffset:I

    .line 1596
    iput v1, p0, Lcom/google/android/play/layout/FlowLayoutManager$NestedFlowLineInfo;->mFlowInsetTop:I

    .line 1597
    iput v1, p0, Lcom/google/android/play/layout/FlowLayoutManager$NestedFlowLineInfo;->mFlowInsetBottom:I

    .line 1598
    iput v1, p0, Lcom/google/android/play/layout/FlowLayoutManager$NestedFlowLineInfo;->mExtraHeight:I

    .line 1599
    iget-object v0, p0, Lcom/google/android/play/layout/FlowLayoutManager$NestedFlowLineInfo;->mParagraph:Lcom/google/android/play/layout/FlowLayoutManager$ParagraphInfo;

    if-eqz v0, :cond_1

    .line 1600
    iget-object v0, p0, Lcom/google/android/play/layout/FlowLayoutManager$NestedFlowLineInfo;->mParagraph:Lcom/google/android/play/layout/FlowLayoutManager$ParagraphInfo;

    invoke-virtual {v0}, Lcom/google/android/play/layout/FlowLayoutManager$ParagraphInfo;->recycle()V

    .line 1601
    iput-object v2, p0, Lcom/google/android/play/layout/FlowLayoutManager$NestedFlowLineInfo;->mParagraph:Lcom/google/android/play/layout/FlowLayoutManager$ParagraphInfo;

    .line 1603
    :cond_1
    return-void
.end method

.method public validPositionEnd()I
    .locals 1

    .prologue
    .line 1495
    iget-object v0, p0, Lcom/google/android/play/layout/FlowLayoutManager$NestedFlowLineInfo;->mCreator:Lcom/google/android/play/layout/FlowLayoutManager$ItemInfo;

    if-nez v0, :cond_0

    iget v0, p0, Lcom/google/android/play/layout/FlowLayoutManager$NestedFlowLineInfo;->mPositionStart:I

    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lcom/google/android/play/layout/FlowLayoutManager$NestedFlowLineInfo;->mParagraph:Lcom/google/android/play/layout/FlowLayoutManager$ParagraphInfo;

    if-nez v0, :cond_1

    iget v0, p0, Lcom/google/android/play/layout/FlowLayoutManager$NestedFlowLineInfo;->mPositionStart:I

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_1
    iget-object v0, p0, Lcom/google/android/play/layout/FlowLayoutManager$NestedFlowLineInfo;->mParagraph:Lcom/google/android/play/layout/FlowLayoutManager$ParagraphInfo;

    invoke-virtual {v0}, Lcom/google/android/play/layout/FlowLayoutManager$ParagraphInfo;->validPositionEnd()I

    move-result v0

    goto :goto_0
.end method
