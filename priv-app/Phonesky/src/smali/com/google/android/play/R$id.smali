.class public final Lcom/google/android/play/R$id;
.super Ljava/lang/Object;
.source "R.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/play/R;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "id"
.end annotation


# static fields
.field public static final account_info_container:I = 0x7f0a02e2

.field public static final account_name:I = 0x7f0a00b9

.field public static final action_bar:I = 0x7f0a00a6

.field public static final action_bar_container:I = 0x7f0a00a5

.field public static final action_button:I = 0x7f0a030c

.field public static final action_text:I = 0x7f0a02de

.field public static final alt_play_background:I = 0x7f0a02e6

.field public static final avatar:I = 0x7f0a0141

.field public static final background_container:I = 0x7f0a02e5

.field public static final content_container:I = 0x7f0a02e7

.field public static final controls_container:I = 0x7f0a02e8

.field public static final cover_photo:I = 0x7f0a02ad

.field public static final display_name:I = 0x7f0a02af

.field public static final end_button:I = 0x7f0a02fd

.field public static final flm_paddingEnd:I = 0x7f0a0022

.field public static final flm_paddingStart:I = 0x7f0a0021

.field public static final flm_width:I = 0x7f0a0023

.field public static final header_shadow:I = 0x7f0a02ef

.field public static final hero_container:I = 0x7f0a02e9

.field public static final icon:I = 0x7f0a0081

.field public static final li_badge:I = 0x7f0a02d9

.field public static final li_description:I = 0x7f0a01b7

.field public static final li_label:I = 0x7f0a01ff

.field public static final li_overflow:I = 0x7f0a01b4

.field public static final li_rating:I = 0x7f0a0201

.field public static final li_snippet_1:I = 0x7f0a02ca

.field public static final li_snippet_2:I = 0x7f0a02c9

.field public static final li_snippet_avatar:I = 0x7f0a02da

.field public static final li_snippet_text:I = 0x7f0a02db

.field public static final li_subtitle:I = 0x7f0a0200

.field public static final li_thumbnail:I = 0x7f0a00d6

.field public static final li_thumbnail_frame:I = 0x7f0a01b5

.field public static final li_title:I = 0x7f0a01b6

.field public static final loading_progress_bar:I = 0x7f0a0159

.field public static final navigation_button:I = 0x7f0a0308

.field public static final page_indicator:I = 0x7f0a02fc

.field public static final pager_tab_strip:I = 0x7f0a02eb

.field public static final play_drawer_list:I = 0x7f0a02dd

.field public static final play_header_banner:I = 0x7f0a02f1

.field public static final play_header_list_tab_container:I = 0x7f0a02ed

.field public static final play_header_list_tab_scroll:I = 0x7f0a02ec

.field public static final play_header_listview:I = 0x7f0a0027

.field public static final play_header_spacer:I = 0x7f0a0029

.field public static final play_header_toolbar:I = 0x7f0a02f0

.field public static final play_header_viewpager:I = 0x7f0a0028

.field public static final play_onboard__OnboardTutorialPage_backgroundColor:I = 0x7f0a002d

.field public static final play_onboard__OnboardTutorialPage_bodyText:I = 0x7f0a002f

.field public static final play_onboard__OnboardTutorialPage_iconDrawableId:I = 0x7f0a0030

.field public static final play_onboard__OnboardTutorialPage_titleText:I = 0x7f0a002e

.field public static final play_search_plate:I = 0x7f0a0303

.field public static final play_search_suggestions_list:I = 0x7f0a0304

.field public static final rating_badge_container:I = 0x7f0a02d8

.field public static final scroll_proxy:I = 0x7f0a02f4

.field public static final search_box_idle_text:I = 0x7f0a030a

.field public static final search_box_text_input:I = 0x7f0a030b

.field public static final secondary_avatar_left:I = 0x7f0a02e0

.field public static final secondary_avatar_right:I = 0x7f0a02e1

.field public static final shortcut:I = 0x7f0a009f

.field public static final start_button:I = 0x7f0a02fb

.field public static final suggest_text:I = 0x7f0a0306

.field public static final suggestion_divider:I = 0x7f0a0307

.field public static final suggestion_list_recycler_view:I = 0x7f0a030f

.field public static final swipe_refresh_layout:I = 0x7f0a02f3

.field public static final switch_button:I = 0x7f0a02df

.field public static final tab_bar:I = 0x7f0a02ea

.field public static final tab_bar_title:I = 0x7f0a02ee

.field public static final text_container:I = 0x7f0a0309

.field public static final title:I = 0x7f0a009c

.field public static final toggle_account_list_button:I = 0x7f0a02e3
