.class public final Lcom/google/android/play/analytics/EventLogger;
.super Ljava/lang/Object;
.source "EventLogger.java"

# interfaces
.implements Lcom/google/android/play/analytics/RollingFileStream$WriteCallbacks;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/play/analytics/EventLogger$Configuration;,
        Lcom/google/android/play/analytics/EventLogger$LogSource;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/google/android/play/analytics/RollingFileStream$WriteCallbacks",
        "<",
        "Lcom/google/android/play/analytics/ClientAnalytics$LogEvent;",
        ">;"
    }
.end annotation


# static fields
.field private static final EMPTY_EXPERIMENTS:Lcom/google/android/play/analytics/ClientAnalytics$ActiveExperiments;

.field private static final sInstantiatedAccounts:Ljava/util/HashSet;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashSet",
            "<",
            "Landroid/accounts/Account;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private final mAccount:Landroid/accounts/Account;

.field private final mAndroidId:J

.field private final mAppVersion:Ljava/lang/String;

.field private final mAuthTokenType:Ljava/lang/String;

.field private final mContext:Landroid/content/Context;

.field private final mCountry:Ljava/lang/String;

.field private final mDelayBetweenUploadsMs:J

.field private final mDeviceId:J

.field private final mHandler:Landroid/os/Handler;

.field private final mHttpClient:Lcom/google/android/volley/GoogleHttpClient;

.field private mLastSentExperiments:Lcom/google/android/play/analytics/ClientAnalytics$ActiveExperiments;

.field private final mLogSource:I

.field private final mLoggingId:Ljava/lang/String;

.field private final mMaxNumberOfRedirects:I

.field private final mMaxUploadSize:J

.field private final mMccmnc:Ljava/lang/String;

.field private final mMinDelayBetweenUploadsMs:J

.field private final mMinImmediateUploadSize:J

.field private volatile mNextAllowedUploadTimeMs:J

.field private final mProtoCache:Lcom/google/android/play/analytics/ProtoCache;

.field private final mRollingFileStream:Lcom/google/android/play/analytics/RollingFileStream;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/play/analytics/RollingFileStream",
            "<",
            "Lcom/google/android/play/analytics/ClientAnalytics$LogEvent;",
            ">;"
        }
    .end annotation
.end field

.field private final mServerUrl:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 72
    new-instance v0, Lcom/google/android/play/analytics/ClientAnalytics$ActiveExperiments;

    invoke-direct {v0}, Lcom/google/android/play/analytics/ClientAnalytics$ActiveExperiments;-><init>()V

    sput-object v0, Lcom/google/android/play/analytics/EventLogger;->EMPTY_EXPERIMENTS:Lcom/google/android/play/analytics/ClientAnalytics$ActiveExperiments;

    .line 75
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    sput-object v0, Lcom/google/android/play/analytics/EventLogger;->sInstantiatedAccounts:Ljava/util/HashSet;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Lcom/google/android/play/analytics/EventLogger$LogSource;Ljava/lang/String;JLjava/lang/String;Ljava/lang/String;Lcom/google/android/play/analytics/EventLogger$Configuration;Landroid/accounts/Account;)V
    .locals 14
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "loggingId"    # Ljava/lang/String;
    .param p3, "authTokenType"    # Ljava/lang/String;
    .param p4, "logSource"    # Lcom/google/android/play/analytics/EventLogger$LogSource;
    .param p5, "useragent"    # Ljava/lang/String;
    .param p6, "androidId"    # J
    .param p8, "appVersion"    # Ljava/lang/String;
    .param p9, "mccmnc"    # Ljava/lang/String;
    .param p10, "config"    # Lcom/google/android/play/analytics/EventLogger$Configuration;
    .param p11, "account"    # Landroid/accounts/Account;

    .prologue
    .line 167
    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/Locale;->getCountry()Ljava/lang/String;

    move-result-object v10

    move-object v0, p0

    move-object v1, p1

    move-object/from16 v2, p2

    move-object/from16 v3, p3

    move-object/from16 v4, p4

    move-object/from16 v5, p5

    move-wide/from16 v6, p6

    move-object/from16 v8, p8

    move-object/from16 v9, p9

    move-object/from16 v11, p10

    move-object/from16 v12, p11

    invoke-direct/range {v0 .. v12}, Lcom/google/android/play/analytics/EventLogger;-><init>(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Lcom/google/android/play/analytics/EventLogger$LogSource;Ljava/lang/String;JLjava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/google/android/play/analytics/EventLogger$Configuration;Landroid/accounts/Account;)V

    .line 169
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Lcom/google/android/play/analytics/EventLogger$LogSource;Ljava/lang/String;JLjava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/google/android/play/analytics/EventLogger$Configuration;Landroid/accounts/Account;)V
    .locals 20
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "loggingId"    # Ljava/lang/String;
    .param p3, "authTokenType"    # Ljava/lang/String;
    .param p4, "logSource"    # Lcom/google/android/play/analytics/EventLogger$LogSource;
    .param p5, "useragent"    # Ljava/lang/String;
    .param p6, "androidId"    # J
    .param p8, "appVersion"    # Ljava/lang/String;
    .param p9, "mccmnc"    # Ljava/lang/String;
    .param p10, "country"    # Ljava/lang/String;
    .param p11, "config"    # Lcom/google/android/play/analytics/EventLogger$Configuration;
    .param p12, "account"    # Landroid/accounts/Account;

    .prologue
    .line 173
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    .line 174
    sget-object v6, Lcom/google/android/play/analytics/EventLogger;->sInstantiatedAccounts:Ljava/util/HashSet;

    monitor-enter v6

    .line 175
    :try_start_0
    sget-object v4, Lcom/google/android/play/analytics/EventLogger;->sInstantiatedAccounts:Ljava/util/HashSet;

    move-object/from16 v0, p12

    invoke-virtual {v4, v0}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    move-result v4

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "Already instantiated an EventLogger for "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    move-object/from16 v0, p12

    invoke-virtual {v7, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v4, v7}, Lcom/google/android/play/utils/Assertions;->checkState(ZLjava/lang/String;)V

    .line 177
    monitor-exit v6
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 178
    move-object/from16 v0, p1

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/google/android/play/analytics/EventLogger;->mContext:Landroid/content/Context;

    .line 179
    invoke-virtual/range {p4 .. p4}, Lcom/google/android/play/analytics/EventLogger$LogSource;->getProtoValue()I

    move-result v4

    move-object/from16 v0, p0

    iput v4, v0, Lcom/google/android/play/analytics/EventLogger;->mLogSource:I

    .line 180
    move-object/from16 v0, p2

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/google/android/play/analytics/EventLogger;->mLoggingId:Ljava/lang/String;

    .line 181
    move-object/from16 v0, p12

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/google/android/play/analytics/EventLogger;->mAccount:Landroid/accounts/Account;

    .line 182
    move-object/from16 v0, p3

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/google/android/play/analytics/EventLogger;->mAuthTokenType:Ljava/lang/String;

    .line 183
    invoke-static {}, Lcom/google/android/play/analytics/ProtoCache;->getInstance()Lcom/google/android/play/analytics/ProtoCache;

    move-result-object v4

    move-object/from16 v0, p0

    iput-object v4, v0, Lcom/google/android/play/analytics/EventLogger;->mProtoCache:Lcom/google/android/play/analytics/ProtoCache;

    .line 184
    new-instance v4, Lcom/google/android/volley/GoogleHttpClient;

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/google/android/play/analytics/EventLogger;->mContext:Landroid/content/Context;

    const/4 v7, 0x1

    move-object/from16 v0, p5

    invoke-direct {v4, v6, v0, v7}, Lcom/google/android/volley/GoogleHttpClient;-><init>(Landroid/content/Context;Ljava/lang/String;Z)V

    move-object/from16 v0, p0

    iput-object v4, v0, Lcom/google/android/play/analytics/EventLogger;->mHttpClient:Lcom/google/android/volley/GoogleHttpClient;

    .line 185
    move-wide/from16 v0, p6

    move-object/from16 v2, p0

    iput-wide v0, v2, Lcom/google/android/play/analytics/EventLogger;->mAndroidId:J

    .line 186
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/play/analytics/EventLogger;->mContext:Landroid/content/Context;

    invoke-virtual {v4}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v4

    const-string v6, "android_id"

    invoke-static {v4, v6}, Landroid/provider/Settings$Secure;->getString(Landroid/content/ContentResolver;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v16

    .line 187
    .local v16, "deviceIdString":Ljava/lang/String;
    const-wide/16 v14, 0x0

    .line 189
    .local v14, "deviceId":J
    const/16 v4, 0x10

    :try_start_1
    move-object/from16 v0, v16

    invoke-static {v0, v4}, Ljava/lang/Long;->parseLong(Ljava/lang/String;I)J
    :try_end_1
    .catch Ljava/lang/NumberFormatException; {:try_start_1 .. :try_end_1} :catch_0

    move-result-wide v14

    .line 193
    :goto_0
    move-object/from16 v0, p0

    iput-wide v14, v0, Lcom/google/android/play/analytics/EventLogger;->mDeviceId:J

    .line 194
    move-object/from16 v0, p8

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/google/android/play/analytics/EventLogger;->mAppVersion:Ljava/lang/String;

    .line 195
    move-object/from16 v0, p9

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/google/android/play/analytics/EventLogger;->mMccmnc:Ljava/lang/String;

    .line 196
    move-object/from16 v0, p10

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/google/android/play/analytics/EventLogger;->mCountry:Ljava/lang/String;

    .line 197
    move-object/from16 v0, p11

    iget-object v4, v0, Lcom/google/android/play/analytics/EventLogger$Configuration;->mServerUrl:Ljava/lang/String;

    move-object/from16 v0, p0

    iput-object v4, v0, Lcom/google/android/play/analytics/EventLogger;->mServerUrl:Ljava/lang/String;

    .line 198
    move-object/from16 v0, p11

    iget-wide v6, v0, Lcom/google/android/play/analytics/EventLogger$Configuration;->delayBetweenUploadsMs:J

    move-object/from16 v0, p0

    iput-wide v6, v0, Lcom/google/android/play/analytics/EventLogger;->mDelayBetweenUploadsMs:J

    .line 199
    move-object/from16 v0, p11

    iget-wide v6, v0, Lcom/google/android/play/analytics/EventLogger$Configuration;->minDelayBetweenUploadsMs:J

    move-object/from16 v0, p0

    iput-wide v6, v0, Lcom/google/android/play/analytics/EventLogger;->mMinDelayBetweenUploadsMs:J

    .line 200
    move-object/from16 v0, p11

    iget v4, v0, Lcom/google/android/play/analytics/EventLogger$Configuration;->maxNumberOfRedirects:I

    move-object/from16 v0, p0

    iput v4, v0, Lcom/google/android/play/analytics/EventLogger;->mMaxNumberOfRedirects:I

    .line 205
    move-object/from16 v0, p11

    iget-wide v6, v0, Lcom/google/android/play/analytics/EventLogger$Configuration;->recommendedLogFileSize:J

    const-wide/16 v8, 0x32

    mul-long/2addr v6, v8

    const-wide/16 v8, 0x64

    div-long/2addr v6, v8

    const-wide/16 v8, 0x1

    add-long/2addr v6, v8

    move-object/from16 v0, p0

    iput-wide v6, v0, Lcom/google/android/play/analytics/EventLogger;->mMinImmediateUploadSize:J

    .line 209
    move-object/from16 v0, p11

    iget-wide v6, v0, Lcom/google/android/play/analytics/EventLogger$Configuration;->recommendedLogFileSize:J

    const-wide/16 v8, 0x7d

    mul-long/2addr v6, v8

    const-wide/16 v8, 0x64

    div-long/2addr v6, v8

    move-object/from16 v0, p0

    iput-wide v6, v0, Lcom/google/android/play/analytics/EventLogger;->mMaxUploadSize:J

    .line 212
    new-instance v13, Ljava/io/File;

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/play/analytics/EventLogger;->mContext:Landroid/content/Context;

    invoke-virtual {v4}, Landroid/content/Context;->getCacheDir()Ljava/io/File;

    move-result-object v4

    move-object/from16 v0, p11

    iget-object v6, v0, Lcom/google/android/play/analytics/EventLogger$Configuration;->logDirectoryName:Ljava/lang/String;

    invoke-direct {v13, v4, v6}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    .line 213
    .local v13, "baseDirectory":Ljava/io/File;
    if-nez p12, :cond_0

    const-string v18, "null_account"

    .line 216
    .local v18, "subDirectory":Ljava/lang/String;
    :goto_1
    new-instance v5, Ljava/io/File;

    invoke-static/range {v18 .. v18}, Landroid/net/Uri;->encode(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-direct {v5, v13, v4}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    .line 217
    .local v5, "userDirectory":Ljava/io/File;
    move-object/from16 v0, p12

    invoke-static {v0, v13, v5}, Lcom/google/android/play/analytics/EventLogger;->maybeRenameLegacyDir(Landroid/accounts/Account;Ljava/io/File;Ljava/io/File;)V

    .line 218
    new-instance v4, Lcom/google/android/play/analytics/RollingFileStream;

    const-string v6, "eventlog.store"

    const-string v7, ".log"

    move-object/from16 v0, p11

    iget-wide v8, v0, Lcom/google/android/play/analytics/EventLogger$Configuration;->recommendedLogFileSize:J

    move-object/from16 v0, p11

    iget-wide v10, v0, Lcom/google/android/play/analytics/EventLogger$Configuration;->maxStorageSize:J

    move-object/from16 v12, p0

    invoke-direct/range {v4 .. v12}, Lcom/google/android/play/analytics/RollingFileStream;-><init>(Ljava/io/File;Ljava/lang/String;Ljava/lang/String;JJLcom/google/android/play/analytics/RollingFileStream$WriteCallbacks;)V

    move-object/from16 v0, p0

    iput-object v4, v0, Lcom/google/android/play/analytics/EventLogger;->mRollingFileStream:Lcom/google/android/play/analytics/RollingFileStream;

    .line 220
    new-instance v4, Lcom/google/android/play/analytics/EventLogger$1;

    invoke-static {}, Lcom/google/android/play/analytics/EventLogger;->startHandlerThread()Landroid/os/HandlerThread;

    move-result-object v6

    invoke-virtual {v6}, Landroid/os/HandlerThread;->getLooper()Landroid/os/Looper;

    move-result-object v6

    move-object/from16 v0, p0

    invoke-direct {v4, v0, v6}, Lcom/google/android/play/analytics/EventLogger$1;-><init>(Lcom/google/android/play/analytics/EventLogger;Landroid/os/Looper;)V

    move-object/from16 v0, p0

    iput-object v4, v0, Lcom/google/android/play/analytics/EventLogger;->mHandler:Landroid/os/Handler;

    .line 226
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/play/analytics/EventLogger;->mHandler:Landroid/os/Handler;

    const/4 v6, 0x1

    invoke-virtual {v4, v6}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    .line 227
    return-void

    .line 177
    .end local v5    # "userDirectory":Ljava/io/File;
    .end local v13    # "baseDirectory":Ljava/io/File;
    .end local v14    # "deviceId":J
    .end local v16    # "deviceIdString":Ljava/lang/String;
    .end local v18    # "subDirectory":Ljava/lang/String;
    :catchall_0
    move-exception v4

    :try_start_2
    monitor-exit v6
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    throw v4

    .line 190
    .restart local v14    # "deviceId":J
    .restart local v16    # "deviceIdString":Ljava/lang/String;
    :catch_0
    move-exception v17

    .line 191
    .local v17, "e":Ljava/lang/NumberFormatException;
    const-string v4, "PlayEventLogger"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "Invalid device id "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    move-object/from16 v0, v16

    invoke-virtual {v6, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v4, v6}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0

    .line 213
    .end local v17    # "e":Ljava/lang/NumberFormatException;
    .restart local v13    # "baseDirectory":Ljava/io/File;
    :cond_0
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, p12

    iget-object v6, v0, Landroid/accounts/Account;->type:Ljava/lang/String;

    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v6, "."

    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    move-object/from16 v0, p12

    iget-object v6, v0, Landroid/accounts/Account;->name:Ljava/lang/String;

    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v18

    goto :goto_1
.end method

.method static synthetic access$000(Lcom/google/android/play/analytics/EventLogger;Landroid/os/Message;)V
    .locals 0
    .param p0, "x0"    # Lcom/google/android/play/analytics/EventLogger;
    .param p1, "x1"    # Landroid/os/Message;

    .prologue
    .line 66
    invoke-direct {p0, p1}, Lcom/google/android/play/analytics/EventLogger;->dispatchMessage(Landroid/os/Message;)V

    return-void
.end method

.method private addEventImpl(Landroid/os/Message;)V
    .locals 6
    .param p1, "msg"    # Landroid/os/Message;

    .prologue
    .line 432
    iget-object v1, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v1, Lcom/google/android/play/analytics/ClientAnalytics$LogEvent;

    .line 438
    .local v1, "event":Lcom/google/android/play/analytics/ClientAnalytics$LogEvent;
    :try_start_0
    iget-object v3, p0, Lcom/google/android/play/analytics/EventLogger;->mRollingFileStream:Lcom/google/android/play/analytics/RollingFileStream;

    invoke-virtual {v3, v1}, Lcom/google/android/play/analytics/RollingFileStream;->write(Ljava/lang/Object;)Z

    move-result v2

    .line 439
    .local v2, "fileEnded":Z
    if-eqz v2, :cond_0

    .line 440
    invoke-direct {p0}, Lcom/google/android/play/analytics/EventLogger;->maybeQueueImmediateUpload()V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 445
    :cond_0
    iget-object v3, p0, Lcom/google/android/play/analytics/EventLogger;->mProtoCache:Lcom/google/android/play/analytics/ProtoCache;

    invoke-virtual {v3, v1}, Lcom/google/android/play/analytics/ProtoCache;->recycle(Lcom/google/android/play/analytics/ClientAnalytics$LogEvent;)V

    .line 447
    .end local v2    # "fileEnded":Z
    :goto_0
    return-void

    .line 442
    :catch_0
    move-exception v0

    .line 443
    .local v0, "e":Ljava/io/IOException;
    :try_start_1
    const-string v3, "PlayEventLogger"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Could not write string ("

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ") to file: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v0}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 445
    iget-object v3, p0, Lcom/google/android/play/analytics/EventLogger;->mProtoCache:Lcom/google/android/play/analytics/ProtoCache;

    invoke-virtual {v3, v1}, Lcom/google/android/play/analytics/ProtoCache;->recycle(Lcom/google/android/play/analytics/ClientAnalytics$LogEvent;)V

    goto :goto_0

    .end local v0    # "e":Ljava/io/IOException;
    :catchall_0
    move-exception v3

    iget-object v4, p0, Lcom/google/android/play/analytics/EventLogger;->mProtoCache:Lcom/google/android/play/analytics/ProtoCache;

    invoke-virtual {v4, v1}, Lcom/google/android/play/analytics/ProtoCache;->recycle(Lcom/google/android/play/analytics/ClientAnalytics$LogEvent;)V

    throw v3
.end method

.method private createByteArrayFrom(Ljava/io/InputStream;I)[B
    .locals 4
    .param p1, "inputStream"    # Ljava/io/InputStream;
    .param p2, "byteBufferSize"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 763
    new-instance v1, Ljava/io/ByteArrayOutputStream;

    invoke-direct {v1}, Ljava/io/ByteArrayOutputStream;-><init>()V

    .line 764
    .local v1, "byteStream":Ljava/io/ByteArrayOutputStream;
    new-array v0, p2, [B

    .line 767
    .local v0, "buffer":[B
    :cond_0
    invoke-virtual {p1, v0}, Ljava/io/InputStream;->read([B)I

    move-result v2

    .line 768
    .local v2, "length":I
    if-lez v2, :cond_1

    .line 769
    const/4 v3, 0x0

    invoke-virtual {v1, v0, v3, v2}, Ljava/io/ByteArrayOutputStream;->write([BII)V

    .line 771
    :cond_1
    if-gez v2, :cond_0

    .line 772
    invoke-virtual {v1}, Ljava/io/ByteArrayOutputStream;->toByteArray()[B

    move-result-object v3

    return-object v3
.end method

.method private dispatchMessage(Landroid/os/Message;)V
    .locals 4
    .param p1, "msg"    # Landroid/os/Message;

    .prologue
    .line 400
    iget v1, p1, Landroid/os/Message;->what:I

    packed-switch v1, :pswitch_data_0

    .line 427
    const-string v1, "PlayEventLogger"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Unknown msg: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget v3, p1, Landroid/os/Message;->what:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 429
    :goto_0
    return-void

    .line 403
    :pswitch_0
    invoke-direct {p0}, Lcom/google/android/play/analytics/EventLogger;->maybeQueueImmediateUpload()V

    .line 406
    iget-wide v2, p0, Lcom/google/android/play/analytics/EventLogger;->mDelayBetweenUploadsMs:J

    invoke-direct {p0, v2, v3}, Lcom/google/android/play/analytics/EventLogger;->queueUpload(J)V

    goto :goto_0

    .line 409
    :pswitch_1
    invoke-direct {p0, p1}, Lcom/google/android/play/analytics/EventLogger;->addEventImpl(Landroid/os/Message;)V

    goto :goto_0

    .line 412
    :pswitch_2
    iget-object v1, p0, Lcom/google/android/play/analytics/EventLogger;->mHandler:Landroid/os/Handler;

    const/4 v2, 0x3

    invoke-virtual {v1, v2}, Landroid/os/Handler;->removeMessages(I)V

    .line 413
    invoke-direct {p0}, Lcom/google/android/play/analytics/EventLogger;->uploadEventsImpl()Z

    move-result v0

    .line 418
    .local v0, "uploadSuccess":Z
    if-eqz v0, :cond_0

    .line 419
    invoke-direct {p0}, Lcom/google/android/play/analytics/EventLogger;->maybeQueueImmediateUpload()V

    .line 423
    :cond_0
    iget-wide v2, p0, Lcom/google/android/play/analytics/EventLogger;->mDelayBetweenUploadsMs:J

    invoke-direct {p0, v2, v3}, Lcom/google/android/play/analytics/EventLogger;->queueUpload(J)V

    goto :goto_0

    .line 400
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method private getAuthToken(Landroid/accounts/Account;)Ljava/lang/String;
    .locals 6
    .param p1, "account"    # Landroid/accounts/Account;

    .prologue
    const/4 v0, 0x0

    .line 786
    if-nez p1, :cond_0

    .line 787
    const-string v3, "PlayEventLogger"

    const-string v4, "No account for auth token provided"

    invoke-static {v3, v4}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 804
    :goto_0
    return-object v0

    .line 791
    :cond_0
    :try_start_0
    iget-object v3, p0, Lcom/google/android/play/analytics/EventLogger;->mContext:Landroid/content/Context;

    invoke-static {v3}, Landroid/accounts/AccountManager;->get(Landroid/content/Context;)Landroid/accounts/AccountManager;

    move-result-object v2

    .line 792
    .local v2, "manager":Landroid/accounts/AccountManager;
    iget-object v3, p0, Lcom/google/android/play/analytics/EventLogger;->mAuthTokenType:Ljava/lang/String;

    const/4 v4, 0x1

    invoke-virtual {v2, p1, v3, v4}, Landroid/accounts/AccountManager;->blockingGetAuthToken(Landroid/accounts/Account;Ljava/lang/String;Z)Ljava/lang/String;
    :try_end_0
    .catch Landroid/accounts/OperationCanceledException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Landroid/accounts/AuthenticatorException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_2
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_3

    move-result-object v0

    .line 794
    .local v0, "authToken":Ljava/lang/String;
    goto :goto_0

    .line 795
    .end local v0    # "authToken":Ljava/lang/String;
    .end local v2    # "manager":Landroid/accounts/AccountManager;
    :catch_0
    move-exception v1

    .line 796
    .local v1, "e":Landroid/accounts/OperationCanceledException;
    const-string v3, "PlayEventLogger"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Failed to get auth token: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v1}, Landroid/accounts/OperationCanceledException;->getMessage()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_0

    .line 797
    .end local v1    # "e":Landroid/accounts/OperationCanceledException;
    :catch_1
    move-exception v1

    .line 798
    .local v1, "e":Landroid/accounts/AuthenticatorException;
    const-string v3, "PlayEventLogger"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Failed to get auth token: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v1}, Landroid/accounts/AuthenticatorException;->getMessage()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_0

    .line 799
    .end local v1    # "e":Landroid/accounts/AuthenticatorException;
    :catch_2
    move-exception v1

    .line 800
    .local v1, "e":Ljava/io/IOException;
    const-string v3, "PlayEventLogger"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Failed to get auth token: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v1}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_0

    .line 801
    .end local v1    # "e":Ljava/io/IOException;
    :catch_3
    move-exception v1

    .line 802
    .local v1, "e":Ljava/lang/IllegalArgumentException;
    const-string v3, "PlayEventLogger"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Failed to get auth token: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v1}, Ljava/lang/IllegalArgumentException;->getMessage()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto/16 :goto_0
.end method

.method private static getRadioVersion()Ljava/lang/String;
    .locals 2

    .prologue
    .line 821
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0xe

    if-lt v0, v1, :cond_0

    .line 822
    invoke-static {}, Landroid/os/Build;->getRadioVersion()Ljava/lang/String;

    move-result-object v0

    .line 824
    :goto_0
    return-object v0

    :cond_0
    sget-object v0, Landroid/os/Build;->RADIO:Ljava/lang/String;

    goto :goto_0
.end method

.method private handleResponse(Lorg/apache/http/HttpResponse;)V
    .locals 10
    .param p1, "response"    # Lorg/apache/http/HttpResponse;

    .prologue
    .line 734
    :try_start_0
    invoke-interface {p1}, Lorg/apache/http/HttpResponse;->getEntity()Lorg/apache/http/HttpEntity;

    move-result-object v6

    invoke-interface {v6}, Lorg/apache/http/HttpEntity;->getContent()Ljava/io/InputStream;

    move-result-object v2

    .line 737
    .local v2, "inputStream":Ljava/io/InputStream;
    const/16 v6, 0x80

    invoke-direct {p0, v2, v6}, Lcom/google/android/play/analytics/EventLogger;->createByteArrayFrom(Ljava/io/InputStream;I)[B

    move-result-object v0

    .line 740
    .local v0, "data":[B
    invoke-static {v0}, Lcom/google/android/play/analytics/ClientAnalytics$LogResponse;->parseFrom([B)Lcom/google/android/play/analytics/ClientAnalytics$LogResponse;

    move-result-object v3

    .line 743
    .local v3, "logResponse":Lcom/google/android/play/analytics/ClientAnalytics$LogResponse;
    iget-wide v6, v3, Lcom/google/android/play/analytics/ClientAnalytics$LogResponse;->nextRequestWaitMillis:J

    const-wide/16 v8, 0x0

    cmp-long v6, v6, v8

    if-ltz v6, :cond_0

    .line 744
    iget-wide v4, v3, Lcom/google/android/play/analytics/ClientAnalytics$LogResponse;->nextRequestWaitMillis:J

    .line 746
    .local v4, "waitTimeInMillis":J
    invoke-direct {p0, v4, v5}, Lcom/google/android/play/analytics/EventLogger;->setNextUploadTimeAfter(J)V
    :try_end_0
    .catch Lcom/google/protobuf/nano/InvalidProtocolBufferNanoException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/IllegalStateException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_2

    .line 755
    .end local v0    # "data":[B
    .end local v2    # "inputStream":Ljava/io/InputStream;
    .end local v3    # "logResponse":Lcom/google/android/play/analytics/ClientAnalytics$LogResponse;
    .end local v4    # "waitTimeInMillis":J
    :cond_0
    :goto_0
    return-void

    .line 748
    :catch_0
    move-exception v1

    .line 749
    .local v1, "e":Lcom/google/protobuf/nano/InvalidProtocolBufferNanoException;
    const-string v6, "PlayEventLogger"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "Error parsing content: "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v1}, Lcom/google/protobuf/nano/InvalidProtocolBufferNanoException;->getMessage()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 750
    .end local v1    # "e":Lcom/google/protobuf/nano/InvalidProtocolBufferNanoException;
    :catch_1
    move-exception v1

    .line 751
    .local v1, "e":Ljava/lang/IllegalStateException;
    const-string v6, "PlayEventLogger"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "Error getting the content of the response body: "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v1}, Ljava/lang/IllegalStateException;->getMessage()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 752
    .end local v1    # "e":Ljava/lang/IllegalStateException;
    :catch_2
    move-exception v1

    .line 753
    .local v1, "e":Ljava/io/IOException;
    const-string v6, "PlayEventLogger"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "Error reading the content of the response body: "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v1}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method private varargs logEvent(Ljava/lang/String;Lcom/google/android/play/analytics/ClientAnalytics$ActiveExperiments;[BJ[Ljava/lang/String;)V
    .locals 8
    .param p1, "tag"    # Ljava/lang/String;
    .param p2, "experiments"    # Lcom/google/android/play/analytics/ClientAnalytics$ActiveExperiments;
    .param p3, "sourceExtension"    # [B
    .param p4, "eventTimeMs"    # J
    .param p6, "extras"    # [Ljava/lang/String;

    .prologue
    .line 343
    if-eqz p6, :cond_0

    array-length v5, p6

    rem-int/lit8 v5, v5, 0x2

    if-nez v5, :cond_2

    :cond_0
    const/4 v5, 0x1

    :goto_0
    const-string v6, "Extras must be null or of even length."

    invoke-static {v5, v6}, Lcom/google/android/play/utils/Assertions;->checkState(ZLjava/lang/String;)V

    .line 345
    iget-object v5, p0, Lcom/google/android/play/analytics/EventLogger;->mProtoCache:Lcom/google/android/play/analytics/ProtoCache;

    invoke-virtual {v5}, Lcom/google/android/play/analytics/ProtoCache;->obtainEvent()Lcom/google/android/play/analytics/ClientAnalytics$LogEvent;

    move-result-object v0

    .line 346
    .local v0, "event":Lcom/google/android/play/analytics/ClientAnalytics$LogEvent;
    iput-wide p4, v0, Lcom/google/android/play/analytics/ClientAnalytics$LogEvent;->eventTimeMs:J

    .line 347
    invoke-static {}, Ljava/util/TimeZone;->getDefault()Ljava/util/TimeZone;

    move-result-object v5

    invoke-virtual {v5}, Ljava/util/TimeZone;->getRawOffset()I

    move-result v5

    div-int/lit16 v5, v5, 0x3e8

    int-to-long v6, v5

    iput-wide v6, v0, Lcom/google/android/play/analytics/ClientAnalytics$LogEvent;->timezoneOffsetSeconds:J

    .line 348
    iput-object p1, v0, Lcom/google/android/play/analytics/ClientAnalytics$LogEvent;->tag:Ljava/lang/String;

    .line 349
    iput-object p2, v0, Lcom/google/android/play/analytics/ClientAnalytics$LogEvent;->exp:Lcom/google/android/play/analytics/ClientAnalytics$ActiveExperiments;

    .line 351
    if-eqz p3, :cond_1

    .line 352
    iput-object p3, v0, Lcom/google/android/play/analytics/ClientAnalytics$LogEvent;->sourceExtension:[B

    .line 354
    :cond_1
    if-eqz p6, :cond_4

    array-length v5, p6

    if-eqz v5, :cond_4

    .line 355
    array-length v5, p6

    div-int/lit8 v4, v5, 0x2

    .line 356
    .local v4, "numPairs":I
    new-array v5, v4, [Lcom/google/android/play/analytics/ClientAnalytics$LogEventKeyValues;

    iput-object v5, v0, Lcom/google/android/play/analytics/ClientAnalytics$LogEvent;->value:[Lcom/google/android/play/analytics/ClientAnalytics$LogEventKeyValues;

    .line 357
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_1
    if-ge v1, v4, :cond_4

    .line 358
    iget-object v5, p0, Lcom/google/android/play/analytics/EventLogger;->mProtoCache:Lcom/google/android/play/analytics/ProtoCache;

    invoke-virtual {v5}, Lcom/google/android/play/analytics/ProtoCache;->obtainKeyValue()Lcom/google/android/play/analytics/ClientAnalytics$LogEventKeyValues;

    move-result-object v3

    .line 359
    .local v3, "keyValue":Lcom/google/android/play/analytics/ClientAnalytics$LogEventKeyValues;
    mul-int/lit8 v2, v1, 0x2

    .line 360
    .local v2, "keyIndex":I
    aget-object v5, p6, v2

    iput-object v5, v3, Lcom/google/android/play/analytics/ClientAnalytics$LogEventKeyValues;->key:Ljava/lang/String;

    .line 361
    add-int/lit8 v5, v2, 0x1

    aget-object v5, p6, v5

    if-eqz v5, :cond_3

    add-int/lit8 v5, v2, 0x1

    aget-object v5, p6, v5

    :goto_2
    iput-object v5, v3, Lcom/google/android/play/analytics/ClientAnalytics$LogEventKeyValues;->value:Ljava/lang/String;

    .line 363
    iget-object v5, v0, Lcom/google/android/play/analytics/ClientAnalytics$LogEvent;->value:[Lcom/google/android/play/analytics/ClientAnalytics$LogEventKeyValues;

    aput-object v3, v5, v1

    .line 357
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 343
    .end local v0    # "event":Lcom/google/android/play/analytics/ClientAnalytics$LogEvent;
    .end local v1    # "i":I
    .end local v2    # "keyIndex":I
    .end local v3    # "keyValue":Lcom/google/android/play/analytics/ClientAnalytics$LogEventKeyValues;
    .end local v4    # "numPairs":I
    :cond_2
    const/4 v5, 0x0

    goto :goto_0

    .line 361
    .restart local v0    # "event":Lcom/google/android/play/analytics/ClientAnalytics$LogEvent;
    .restart local v1    # "i":I
    .restart local v2    # "keyIndex":I
    .restart local v3    # "keyValue":Lcom/google/android/play/analytics/ClientAnalytics$LogEventKeyValues;
    .restart local v4    # "numPairs":I
    :cond_3
    const-string v5, "null"

    goto :goto_2

    .line 366
    .end local v1    # "i":I
    .end local v2    # "keyIndex":I
    .end local v3    # "keyValue":Lcom/google/android/play/analytics/ClientAnalytics$LogEventKeyValues;
    .end local v4    # "numPairs":I
    :cond_4
    iget-object v5, p0, Lcom/google/android/play/analytics/EventLogger;->mHandler:Landroid/os/Handler;

    const/4 v6, 0x2

    invoke-virtual {v5, v6, v0}, Landroid/os/Handler;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v5

    invoke-virtual {v5}, Landroid/os/Message;->sendToTarget()V

    .line 367
    return-void
.end method

.method private maybeQueueImmediateUpload()V
    .locals 4

    .prologue
    .line 394
    iget-object v0, p0, Lcom/google/android/play/analytics/EventLogger;->mRollingFileStream:Lcom/google/android/play/analytics/RollingFileStream;

    invoke-virtual {v0}, Lcom/google/android/play/analytics/RollingFileStream;->totalUnreadFileLength()J

    move-result-wide v0

    iget-wide v2, p0, Lcom/google/android/play/analytics/EventLogger;->mMinImmediateUploadSize:J

    cmp-long v0, v0, v2

    if-ltz v0, :cond_0

    .line 395
    const-wide/16 v0, 0x0

    invoke-direct {p0, v0, v1}, Lcom/google/android/play/analytics/EventLogger;->queueUpload(J)V

    .line 397
    :cond_0
    return-void
.end method

.method private static maybeRenameLegacyDir(Landroid/accounts/Account;Ljava/io/File;Ljava/io/File;)V
    .locals 2
    .param p0, "account"    # Landroid/accounts/Account;
    .param p1, "baseDir"    # Ljava/io/File;
    .param p2, "newDir"    # Ljava/io/File;

    .prologue
    .line 808
    if-eqz p0, :cond_0

    invoke-virtual {p2}, Ljava/io/File;->exists()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 817
    :cond_0
    :goto_0
    return-void

    .line 813
    :cond_1
    new-instance v0, Ljava/io/File;

    iget-object v1, p0, Landroid/accounts/Account;->name:Ljava/lang/String;

    invoke-static {v1}, Landroid/net/Uri;->encode(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, p1, v1}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    .line 814
    .local v0, "legacyDir":Ljava/io/File;
    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-virtual {v0}, Ljava/io/File;->isDirectory()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 815
    invoke-virtual {v0, p2}, Ljava/io/File;->renameTo(Ljava/io/File;)Z

    goto :goto_0
.end method

.method private queueUpload(J)V
    .locals 9
    .param p1, "delayMs"    # J

    .prologue
    const/4 v6, 0x3

    .line 373
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    .line 375
    .local v0, "currentTimeMillis":J
    const-wide/16 v4, 0x0

    cmp-long v4, p1, v4

    if-lez v4, :cond_1

    .line 376
    add-long v2, v0, p1

    .line 377
    .local v2, "scheduledUploadMs":J
    iget-wide v4, p0, Lcom/google/android/play/analytics/EventLogger;->mNextAllowedUploadTimeMs:J

    cmp-long v4, v2, v4

    if-gez v4, :cond_0

    .line 378
    iget-wide v4, p0, Lcom/google/android/play/analytics/EventLogger;->mNextAllowedUploadTimeMs:J

    sub-long p1, v4, v0

    .line 380
    :cond_0
    iget-object v4, p0, Lcom/google/android/play/analytics/EventLogger;->mHandler:Landroid/os/Handler;

    invoke-virtual {v4, v6, p1, p2}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z

    .line 389
    .end local v2    # "scheduledUploadMs":J
    :goto_0
    iget-wide v4, p0, Lcom/google/android/play/analytics/EventLogger;->mNextAllowedUploadTimeMs:J

    iget-wide v6, p0, Lcom/google/android/play/analytics/EventLogger;->mMinDelayBetweenUploadsMs:J

    add-long/2addr v6, v0

    invoke-static {v4, v5, v6, v7}, Ljava/lang/Math;->max(JJ)J

    move-result-wide v4

    iput-wide v4, p0, Lcom/google/android/play/analytics/EventLogger;->mNextAllowedUploadTimeMs:J

    .line 391
    return-void

    .line 382
    :cond_1
    iget-object v4, p0, Lcom/google/android/play/analytics/EventLogger;->mHandler:Landroid/os/Handler;

    invoke-virtual {v4, v6}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    goto :goto_0
.end method

.method private readSerializedLogEvents()[[B
    .locals 12
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 584
    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    .line 585
    .local v4, "repeatedSerializedLogEvents":Ljava/util/ArrayList;, "Ljava/util/ArrayList<[B>;"
    const-wide/16 v0, 0x0

    .line 588
    .local v0, "accumulatedLength":J
    :cond_0
    iget-object v7, p0, Lcom/google/android/play/analytics/EventLogger;->mRollingFileStream:Lcom/google/android/play/analytics/RollingFileStream;

    invoke-virtual {v7}, Lcom/google/android/play/analytics/RollingFileStream;->read()[B

    move-result-object v6

    .line 589
    .local v6, "serializedLogEvents":[B
    if-nez v6, :cond_2

    .line 600
    :cond_1
    :goto_0
    invoke-virtual {v4}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v7

    if-eqz v7, :cond_4

    .line 602
    const/4 v7, 0x0

    check-cast v7, [[B

    .line 607
    :goto_1
    return-object v7

    .line 593
    :cond_2
    array-length v7, v6

    if-lez v7, :cond_3

    .line 594
    invoke-virtual {v4, v6}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 595
    array-length v7, v6

    int-to-long v8, v7

    add-long/2addr v0, v8

    .line 597
    :cond_3
    iget-object v7, p0, Lcom/google/android/play/analytics/EventLogger;->mRollingFileStream:Lcom/google/android/play/analytics/RollingFileStream;

    invoke-virtual {v7}, Lcom/google/android/play/analytics/RollingFileStream;->peekNextReadLength()J

    move-result-wide v2

    .line 598
    .local v2, "nextReadLength":J
    const-wide/16 v8, 0x0

    cmp-long v7, v2, v8

    if-ltz v7, :cond_1

    add-long v8, v0, v2

    iget-wide v10, p0, Lcom/google/android/play/analytics/EventLogger;->mMaxUploadSize:J

    cmp-long v7, v8, v10

    if-lez v7, :cond_0

    goto :goto_0

    .line 605
    .end local v2    # "nextReadLength":J
    :cond_4
    invoke-virtual {v4}, Ljava/util/ArrayList;->size()I

    move-result v7

    new-array v5, v7, [[B

    .line 606
    .local v5, "repeatedSerializedLogEventsArray":[[B
    invoke-virtual {v4, v5}, Ljava/util/ArrayList;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-object v7, v5

    .line 607
    goto :goto_1
.end method

.method private setNextUploadTimeAfter(J)V
    .locals 5
    .param p1, "waitTimeInMillis"    # J

    .prologue
    .line 781
    iget-wide v2, p0, Lcom/google/android/play/analytics/EventLogger;->mMinDelayBetweenUploadsMs:J

    invoke-static {v2, v3, p1, p2}, Ljava/lang/Math;->max(JJ)J

    move-result-wide v0

    .line 782
    .local v0, "actualWaitTime":J
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    add-long/2addr v2, v0

    iput-wide v2, p0, Lcom/google/android/play/analytics/EventLogger;->mNextAllowedUploadTimeMs:J

    .line 783
    return-void
.end method

.method private static startHandlerThread()Landroid/os/HandlerThread;
    .locals 4

    .prologue
    .line 233
    new-instance v1, Ljava/util/concurrent/Semaphore;

    const/4 v2, 0x0

    invoke-direct {v1, v2}, Ljava/util/concurrent/Semaphore;-><init>(I)V

    .line 234
    .local v1, "semaphore":Ljava/util/concurrent/Semaphore;
    new-instance v0, Lcom/google/android/play/analytics/EventLogger$2;

    const-string v2, "PlayEventLogger"

    const/16 v3, 0xa

    invoke-direct {v0, v2, v3, v1}, Lcom/google/android/play/analytics/EventLogger$2;-><init>(Ljava/lang/String;ILjava/util/concurrent/Semaphore;)V

    .line 240
    .local v0, "handlerThread":Landroid/os/HandlerThread;
    invoke-virtual {v0}, Landroid/os/HandlerThread;->start()V

    .line 242
    invoke-virtual {v1}, Ljava/util/concurrent/Semaphore;->acquireUninterruptibly()V

    .line 243
    return-object v0
.end method

.method private uploadEventsImpl()Z
    .locals 10

    .prologue
    const/4 v6, 0x0

    .line 500
    const/4 v1, 0x0

    .line 502
    .local v1, "canDeleteLog":Z
    :try_start_0
    iget-object v7, p0, Lcom/google/android/play/analytics/EventLogger;->mRollingFileStream:Lcom/google/android/play/analytics/RollingFileStream;

    invoke-virtual {v7}, Lcom/google/android/play/analytics/RollingFileStream;->hasUnreadFiles()Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result v7

    if-nez v7, :cond_1

    .line 574
    if-eqz v1, :cond_0

    .line 575
    iget-object v7, p0, Lcom/google/android/play/analytics/EventLogger;->mRollingFileStream:Lcom/google/android/play/analytics/RollingFileStream;

    invoke-virtual {v7}, Lcom/google/android/play/analytics/RollingFileStream;->deleteAllReadFiles()V

    .line 577
    :goto_0
    return v6

    :cond_0
    iget-object v7, p0, Lcom/google/android/play/analytics/EventLogger;->mRollingFileStream:Lcom/google/android/play/analytics/RollingFileStream;

    invoke-virtual {v7}, Lcom/google/android/play/analytics/RollingFileStream;->markAllFilesAsUnread()V

    goto :goto_0

    .line 513
    :cond_1
    :try_start_1
    new-instance v4, Lcom/google/android/play/analytics/ClientAnalytics$LogRequest;

    invoke-direct {v4}, Lcom/google/android/play/analytics/ClientAnalytics$LogRequest;-><init>()V

    .line 514
    .local v4, "logRequest":Lcom/google/android/play/analytics/ClientAnalytics$LogRequest;
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v8

    iput-wide v8, v4, Lcom/google/android/play/analytics/ClientAnalytics$LogRequest;->requestTimeMs:J

    .line 516
    new-instance v0, Lcom/google/android/play/analytics/ClientAnalytics$AndroidClientInfo;

    invoke-direct {v0}, Lcom/google/android/play/analytics/ClientAnalytics$AndroidClientInfo;-><init>()V

    .line 517
    .local v0, "androidClientInfo":Lcom/google/android/play/analytics/ClientAnalytics$AndroidClientInfo;
    iget-wide v8, p0, Lcom/google/android/play/analytics/EventLogger;->mAndroidId:J

    iput-wide v8, v0, Lcom/google/android/play/analytics/ClientAnalytics$AndroidClientInfo;->androidId:J

    .line 518
    iget-wide v8, p0, Lcom/google/android/play/analytics/EventLogger;->mDeviceId:J

    iput-wide v8, v0, Lcom/google/android/play/analytics/ClientAnalytics$AndroidClientInfo;->deviceId:J

    .line 519
    iget-object v7, p0, Lcom/google/android/play/analytics/EventLogger;->mLoggingId:Ljava/lang/String;

    if-eqz v7, :cond_2

    .line 520
    iget-object v7, p0, Lcom/google/android/play/analytics/EventLogger;->mLoggingId:Ljava/lang/String;

    iput-object v7, v0, Lcom/google/android/play/analytics/ClientAnalytics$AndroidClientInfo;->loggingId:Ljava/lang/String;

    .line 522
    :cond_2
    sget v7, Landroid/os/Build$VERSION;->SDK_INT:I

    iput v7, v0, Lcom/google/android/play/analytics/ClientAnalytics$AndroidClientInfo;->sdkVersion:I

    .line 523
    sget-object v7, Landroid/os/Build;->MANUFACTURER:Ljava/lang/String;

    iput-object v7, v0, Lcom/google/android/play/analytics/ClientAnalytics$AndroidClientInfo;->manufacturer:Ljava/lang/String;

    .line 524
    sget-object v7, Landroid/os/Build;->MODEL:Ljava/lang/String;

    iput-object v7, v0, Lcom/google/android/play/analytics/ClientAnalytics$AndroidClientInfo;->model:Ljava/lang/String;

    .line 525
    sget-object v7, Landroid/os/Build;->PRODUCT:Ljava/lang/String;

    iput-object v7, v0, Lcom/google/android/play/analytics/ClientAnalytics$AndroidClientInfo;->product:Ljava/lang/String;

    .line 526
    sget-object v7, Landroid/os/Build;->HARDWARE:Ljava/lang/String;

    iput-object v7, v0, Lcom/google/android/play/analytics/ClientAnalytics$AndroidClientInfo;->hardware:Ljava/lang/String;

    .line 527
    sget-object v7, Landroid/os/Build;->DEVICE:Ljava/lang/String;

    iput-object v7, v0, Lcom/google/android/play/analytics/ClientAnalytics$AndroidClientInfo;->device:Ljava/lang/String;

    .line 528
    sget-object v7, Landroid/os/Build;->ID:Ljava/lang/String;

    iput-object v7, v0, Lcom/google/android/play/analytics/ClientAnalytics$AndroidClientInfo;->osBuild:Ljava/lang/String;

    .line 529
    sget-object v7, Landroid/os/Build;->BRAND:Ljava/lang/String;

    iput-object v7, v0, Lcom/google/android/play/analytics/ClientAnalytics$AndroidClientInfo;->brand:Ljava/lang/String;

    .line 530
    sget-object v7, Landroid/os/Build;->BOARD:Ljava/lang/String;

    iput-object v7, v0, Lcom/google/android/play/analytics/ClientAnalytics$AndroidClientInfo;->board:Ljava/lang/String;

    .line 531
    sget-object v7, Landroid/os/Build;->FINGERPRINT:Ljava/lang/String;

    iput-object v7, v0, Lcom/google/android/play/analytics/ClientAnalytics$AndroidClientInfo;->fingerprint:Ljava/lang/String;

    .line 532
    invoke-static {}, Lcom/google/android/play/analytics/EventLogger;->getRadioVersion()Ljava/lang/String;

    move-result-object v5

    .line 533
    .local v5, "radioVersion":Ljava/lang/String;
    if-eqz v5, :cond_3

    .line 534
    iput-object v5, v0, Lcom/google/android/play/analytics/ClientAnalytics$AndroidClientInfo;->radioVersion:Ljava/lang/String;

    .line 536
    :cond_3
    iget-object v7, p0, Lcom/google/android/play/analytics/EventLogger;->mMccmnc:Ljava/lang/String;

    if-eqz v7, :cond_4

    .line 537
    iget-object v7, p0, Lcom/google/android/play/analytics/EventLogger;->mMccmnc:Ljava/lang/String;

    iput-object v7, v0, Lcom/google/android/play/analytics/ClientAnalytics$AndroidClientInfo;->mccMnc:Ljava/lang/String;

    .line 539
    :cond_4
    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v7

    invoke-virtual {v7}, Ljava/util/Locale;->getLanguage()Ljava/lang/String;

    move-result-object v7

    iput-object v7, v0, Lcom/google/android/play/analytics/ClientAnalytics$AndroidClientInfo;->locale:Ljava/lang/String;

    .line 540
    iget-object v7, p0, Lcom/google/android/play/analytics/EventLogger;->mCountry:Ljava/lang/String;

    if-eqz v7, :cond_5

    .line 541
    iget-object v7, p0, Lcom/google/android/play/analytics/EventLogger;->mCountry:Ljava/lang/String;

    iput-object v7, v0, Lcom/google/android/play/analytics/ClientAnalytics$AndroidClientInfo;->country:Ljava/lang/String;

    .line 543
    :cond_5
    iget-object v7, p0, Lcom/google/android/play/analytics/EventLogger;->mAppVersion:Ljava/lang/String;

    if-eqz v7, :cond_6

    .line 544
    iget-object v7, p0, Lcom/google/android/play/analytics/EventLogger;->mAppVersion:Ljava/lang/String;

    iput-object v7, v0, Lcom/google/android/play/analytics/ClientAnalytics$AndroidClientInfo;->applicationBuild:Ljava/lang/String;

    .line 547
    :cond_6
    new-instance v2, Lcom/google/android/play/analytics/ClientAnalytics$ClientInfo;

    invoke-direct {v2}, Lcom/google/android/play/analytics/ClientAnalytics$ClientInfo;-><init>()V

    .line 548
    .local v2, "clientInfo":Lcom/google/android/play/analytics/ClientAnalytics$ClientInfo;
    const/4 v7, 0x4

    iput v7, v2, Lcom/google/android/play/analytics/ClientAnalytics$ClientInfo;->clientType:I

    .line 549
    iput-object v0, v2, Lcom/google/android/play/analytics/ClientAnalytics$ClientInfo;->androidClientInfo:Lcom/google/android/play/analytics/ClientAnalytics$AndroidClientInfo;

    .line 550
    iput-object v2, v4, Lcom/google/android/play/analytics/ClientAnalytics$LogRequest;->clientInfo:Lcom/google/android/play/analytics/ClientAnalytics$ClientInfo;

    .line 551
    iget v7, p0, Lcom/google/android/play/analytics/EventLogger;->mLogSource:I

    iput v7, v4, Lcom/google/android/play/analytics/ClientAnalytics$LogRequest;->logSource:I
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 554
    :try_start_2
    invoke-direct {p0}, Lcom/google/android/play/analytics/EventLogger;->readSerializedLogEvents()[[B

    move-result-object v7

    iput-object v7, v4, Lcom/google/android/play/analytics/ClientAnalytics$LogRequest;->serializedLogEvents:[[B

    .line 555
    iget-object v7, v4, Lcom/google/android/play/analytics/ClientAnalytics$LogRequest;->serializedLogEvents:[[B

    if-nez v7, :cond_8

    .line 556
    const-string v7, "PlayEventLogger"

    const-string v8, "uploadEventsImpl: Thought we had files ready to send, but didn\'t"

    invoke-static {v7, v8}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_0
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 571
    :try_start_3
    iget-object v7, p0, Lcom/google/android/play/analytics/EventLogger;->mProtoCache:Lcom/google/android/play/analytics/ProtoCache;

    invoke-virtual {v7, v4}, Lcom/google/android/play/analytics/ProtoCache;->recycleLogRequest(Lcom/google/android/play/analytics/ClientAnalytics$LogRequest;)V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    .line 574
    if-eqz v1, :cond_7

    .line 575
    iget-object v7, p0, Lcom/google/android/play/analytics/EventLogger;->mRollingFileStream:Lcom/google/android/play/analytics/RollingFileStream;

    invoke-virtual {v7}, Lcom/google/android/play/analytics/RollingFileStream;->deleteAllReadFiles()V

    goto/16 :goto_0

    .line 577
    :cond_7
    iget-object v7, p0, Lcom/google/android/play/analytics/EventLogger;->mRollingFileStream:Lcom/google/android/play/analytics/RollingFileStream;

    invoke-virtual {v7}, Lcom/google/android/play/analytics/RollingFileStream;->markAllFilesAsUnread()V

    goto/16 :goto_0

    .line 563
    :cond_8
    :try_start_4
    iget-object v7, p0, Lcom/google/android/play/analytics/EventLogger;->mAccount:Landroid/accounts/Account;

    iget-object v8, p0, Lcom/google/android/play/analytics/EventLogger;->mServerUrl:Ljava/lang/String;

    iget v9, p0, Lcom/google/android/play/analytics/EventLogger;->mMaxNumberOfRedirects:I

    invoke-direct {p0, v7, v4, v8, v9}, Lcom/google/android/play/analytics/EventLogger;->uploadLog(Landroid/accounts/Account;Lcom/google/android/play/analytics/ClientAnalytics$LogRequest;Ljava/lang/String;I)Z
    :try_end_4
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_0
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    move-result v1

    .line 571
    :try_start_5
    iget-object v6, p0, Lcom/google/android/play/analytics/EventLogger;->mProtoCache:Lcom/google/android/play/analytics/ProtoCache;

    invoke-virtual {v6, v4}, Lcom/google/android/play/analytics/ProtoCache;->recycleLogRequest(Lcom/google/android/play/analytics/ClientAnalytics$LogRequest;)V
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_1

    .line 574
    if-eqz v1, :cond_9

    .line 575
    iget-object v6, p0, Lcom/google/android/play/analytics/EventLogger;->mRollingFileStream:Lcom/google/android/play/analytics/RollingFileStream;

    invoke-virtual {v6}, Lcom/google/android/play/analytics/RollingFileStream;->deleteAllReadFiles()V

    :goto_1
    move v6, v1

    .line 577
    goto/16 :goto_0

    :cond_9
    iget-object v6, p0, Lcom/google/android/play/analytics/EventLogger;->mRollingFileStream:Lcom/google/android/play/analytics/RollingFileStream;

    invoke-virtual {v6}, Lcom/google/android/play/analytics/RollingFileStream;->markAllFilesAsUnread()V

    goto :goto_1

    .line 566
    :catch_0
    move-exception v3

    .line 567
    .local v3, "e":Ljava/io/IOException;
    :try_start_6
    const-string v7, "PlayEventLogger"

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "Upload failed "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v3}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v9

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, "("

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v3}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, ")"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v7, v8}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_0

    .line 571
    :try_start_7
    iget-object v7, p0, Lcom/google/android/play/analytics/EventLogger;->mProtoCache:Lcom/google/android/play/analytics/ProtoCache;

    invoke-virtual {v7, v4}, Lcom/google/android/play/analytics/ProtoCache;->recycleLogRequest(Lcom/google/android/play/analytics/ClientAnalytics$LogRequest;)V
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_1

    .line 574
    if-eqz v1, :cond_a

    .line 575
    iget-object v7, p0, Lcom/google/android/play/analytics/EventLogger;->mRollingFileStream:Lcom/google/android/play/analytics/RollingFileStream;

    invoke-virtual {v7}, Lcom/google/android/play/analytics/RollingFileStream;->deleteAllReadFiles()V

    goto/16 :goto_0

    .line 577
    :cond_a
    iget-object v7, p0, Lcom/google/android/play/analytics/EventLogger;->mRollingFileStream:Lcom/google/android/play/analytics/RollingFileStream;

    invoke-virtual {v7}, Lcom/google/android/play/analytics/RollingFileStream;->markAllFilesAsUnread()V

    goto/16 :goto_0

    .line 571
    .end local v3    # "e":Ljava/io/IOException;
    :catchall_0
    move-exception v6

    :try_start_8
    iget-object v7, p0, Lcom/google/android/play/analytics/EventLogger;->mProtoCache:Lcom/google/android/play/analytics/ProtoCache;

    invoke-virtual {v7, v4}, Lcom/google/android/play/analytics/ProtoCache;->recycleLogRequest(Lcom/google/android/play/analytics/ClientAnalytics$LogRequest;)V

    throw v6
    :try_end_8
    .catchall {:try_start_8 .. :try_end_8} :catchall_1

    .line 574
    .end local v0    # "androidClientInfo":Lcom/google/android/play/analytics/ClientAnalytics$AndroidClientInfo;
    .end local v2    # "clientInfo":Lcom/google/android/play/analytics/ClientAnalytics$ClientInfo;
    .end local v4    # "logRequest":Lcom/google/android/play/analytics/ClientAnalytics$LogRequest;
    .end local v5    # "radioVersion":Ljava/lang/String;
    :catchall_1
    move-exception v6

    if-eqz v1, :cond_b

    .line 575
    iget-object v7, p0, Lcom/google/android/play/analytics/EventLogger;->mRollingFileStream:Lcom/google/android/play/analytics/RollingFileStream;

    invoke-virtual {v7}, Lcom/google/android/play/analytics/RollingFileStream;->deleteAllReadFiles()V

    .line 577
    :goto_2
    throw v6

    :cond_b
    iget-object v7, p0, Lcom/google/android/play/analytics/EventLogger;->mRollingFileStream:Lcom/google/android/play/analytics/RollingFileStream;

    invoke-virtual {v7}, Lcom/google/android/play/analytics/RollingFileStream;->markAllFilesAsUnread()V

    goto :goto_2
.end method

.method private uploadLog(Landroid/accounts/Account;Lcom/google/android/play/analytics/ClientAnalytics$LogRequest;Ljava/lang/String;I)Z
    .locals 30
    .param p1, "uploadAccount"    # Landroid/accounts/Account;
    .param p2, "logRequest"    # Lcom/google/android/play/analytics/ClientAnalytics$LogRequest;
    .param p3, "requestUrl"    # Ljava/lang/String;
    .param p4, "numRedirectsRemaining"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 620
    invoke-direct/range {p0 .. p1}, Lcom/google/android/play/analytics/EventLogger;->getAuthToken(Landroid/accounts/Account;)Ljava/lang/String;

    move-result-object v6

    .line 622
    .local v6, "authToken":Ljava/lang/String;
    new-instance v17, Lorg/apache/http/client/methods/HttpPost;

    move-object/from16 v0, v17

    move-object/from16 v1, p3

    invoke-direct {v0, v1}, Lorg/apache/http/client/methods/HttpPost;-><init>(Ljava/lang/String;)V

    .line 623
    .local v17, "post":Lorg/apache/http/client/methods/HttpPost;
    if-eqz v6, :cond_0

    .line 624
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/play/analytics/EventLogger;->mAuthTokenType:Ljava/lang/String;

    move-object/from16 v27, v0

    const-string v28, "oauth2:"

    invoke-virtual/range {v27 .. v28}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v27

    if-eqz v27, :cond_1

    const-string v7, "Bearer "

    .line 626
    .local v7, "authTokenPrefix":Ljava/lang/String;
    :goto_0
    const-string v27, "Authorization"

    new-instance v28, Ljava/lang/StringBuilder;

    invoke-direct/range {v28 .. v28}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, v28

    invoke-virtual {v0, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v28

    move-object/from16 v0, v28

    invoke-virtual {v0, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v28

    invoke-virtual/range {v28 .. v28}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v28

    move-object/from16 v0, v17

    move-object/from16 v1, v27

    move-object/from16 v2, v28

    invoke-virtual {v0, v1, v2}, Lorg/apache/http/client/methods/HttpPost;->addHeader(Ljava/lang/String;Ljava/lang/String;)V

    .line 629
    .end local v7    # "authTokenPrefix":Ljava/lang/String;
    :cond_0
    new-instance v8, Ljava/io/ByteArrayOutputStream;

    invoke-direct {v8}, Ljava/io/ByteArrayOutputStream;-><init>()V

    .line 630
    .local v8, "byteStream":Ljava/io/ByteArrayOutputStream;
    new-instance v12, Ljava/util/zip/GZIPOutputStream;

    invoke-direct {v12, v8}, Ljava/util/zip/GZIPOutputStream;-><init>(Ljava/io/OutputStream;)V

    .line 631
    .local v12, "gzipStream":Ljava/util/zip/GZIPOutputStream;
    invoke-static/range {p2 .. p2}, Lcom/google/protobuf/nano/MessageNano;->toByteArray(Lcom/google/protobuf/nano/MessageNano;)[B

    move-result-object v18

    .line 632
    .local v18, "rawBytes":[B
    move-object/from16 v0, v18

    invoke-virtual {v12, v0}, Ljava/util/zip/GZIPOutputStream;->write([B)V

    .line 634
    invoke-virtual {v12}, Ljava/util/zip/GZIPOutputStream;->close()V

    .line 635
    invoke-virtual {v8}, Ljava/io/ByteArrayOutputStream;->toByteArray()[B

    move-result-object v10

    .line 636
    .local v10, "compressedLogs":[B
    new-instance v11, Lorg/apache/http/entity/ByteArrayEntity;

    invoke-direct {v11, v10}, Lorg/apache/http/entity/ByteArrayEntity;-><init>([B)V

    .line 638
    .local v11, "entity":Lorg/apache/http/entity/ByteArrayEntity;
    const-string v27, "gzip"

    move-object/from16 v0, v27

    invoke-virtual {v11, v0}, Lorg/apache/http/entity/ByteArrayEntity;->setContentEncoding(Ljava/lang/String;)V

    .line 639
    const-string v27, "application/x-gzip"

    move-object/from16 v0, v27

    invoke-virtual {v11, v0}, Lorg/apache/http/entity/ByteArrayEntity;->setContentType(Ljava/lang/String;)V

    .line 641
    move-object/from16 v0, v17

    invoke-virtual {v0, v11}, Lorg/apache/http/client/methods/HttpPost;->setEntity(Lorg/apache/http/HttpEntity;)V

    .line 647
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/play/analytics/EventLogger;->mHttpClient:Lcom/google/android/volley/GoogleHttpClient;

    move-object/from16 v27, v0

    move-object/from16 v0, v27

    move-object/from16 v1, v17

    invoke-virtual {v0, v1}, Lcom/google/android/volley/GoogleHttpClient;->execute(Lorg/apache/http/client/methods/HttpUriRequest;)Lorg/apache/http/HttpResponse;

    move-result-object v20

    .line 648
    .local v20, "response":Lorg/apache/http/HttpResponse;
    invoke-interface/range {v20 .. v20}, Lorg/apache/http/HttpResponse;->getStatusLine()Lorg/apache/http/StatusLine;

    move-result-object v26

    .line 649
    .local v26, "statusLine":Lorg/apache/http/StatusLine;
    invoke-interface/range {v26 .. v26}, Lorg/apache/http/StatusLine;->getStatusCode()I

    move-result v25

    .line 653
    .local v25, "statusCode":I
    const/16 v27, 0xc8

    move/from16 v0, v27

    move/from16 v1, v25

    if-gt v0, v1, :cond_2

    const/16 v27, 0x12c

    move/from16 v0, v25

    move/from16 v1, v27

    if-ge v0, v1, :cond_2

    .line 657
    const/4 v9, 0x1

    .line 660
    .local v9, "canDeleteLog":Z
    move-object/from16 v0, p0

    move-object/from16 v1, v20

    invoke-direct {v0, v1}, Lcom/google/android/play/analytics/EventLogger;->handleResponse(Lorg/apache/http/HttpResponse;)V

    .line 729
    :goto_1
    return v9

    .line 624
    .end local v8    # "byteStream":Ljava/io/ByteArrayOutputStream;
    .end local v9    # "canDeleteLog":Z
    .end local v10    # "compressedLogs":[B
    .end local v11    # "entity":Lorg/apache/http/entity/ByteArrayEntity;
    .end local v12    # "gzipStream":Ljava/util/zip/GZIPOutputStream;
    .end local v18    # "rawBytes":[B
    .end local v20    # "response":Lorg/apache/http/HttpResponse;
    .end local v25    # "statusCode":I
    .end local v26    # "statusLine":Lorg/apache/http/StatusLine;
    :cond_1
    const-string v7, "GoogleLogin auth="

    goto/16 :goto_0

    .line 661
    .restart local v8    # "byteStream":Ljava/io/ByteArrayOutputStream;
    .restart local v10    # "compressedLogs":[B
    .restart local v11    # "entity":Lorg/apache/http/entity/ByteArrayEntity;
    .restart local v12    # "gzipStream":Ljava/util/zip/GZIPOutputStream;
    .restart local v18    # "rawBytes":[B
    .restart local v20    # "response":Lorg/apache/http/HttpResponse;
    .restart local v25    # "statusCode":I
    .restart local v26    # "statusLine":Lorg/apache/http/StatusLine;
    :cond_2
    const/16 v27, 0x12c

    move/from16 v0, v27

    move/from16 v1, v25

    if-gt v0, v1, :cond_5

    const/16 v27, 0x190

    move/from16 v0, v25

    move/from16 v1, v27

    if-ge v0, v1, :cond_5

    .line 662
    if-lez p4, :cond_4

    .line 663
    const-string v27, "Location"

    move-object/from16 v0, v20

    move-object/from16 v1, v27

    invoke-interface {v0, v1}, Lorg/apache/http/HttpResponse;->getFirstHeader(Ljava/lang/String;)Lorg/apache/http/Header;

    move-result-object v13

    .line 664
    .local v13, "locationHeader":Lorg/apache/http/Header;
    if-nez v13, :cond_3

    .line 665
    const-string v27, "PlayEventLogger"

    new-instance v28, Ljava/lang/StringBuilder;

    invoke-direct/range {v28 .. v28}, Ljava/lang/StringBuilder;-><init>()V

    const-string v29, "Status "

    invoke-virtual/range {v28 .. v29}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v28

    move-object/from16 v0, v28

    move/from16 v1, v25

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v28

    const-string v29, "... redirect: no location header"

    invoke-virtual/range {v28 .. v29}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v28

    invoke-virtual/range {v28 .. v28}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v28

    invoke-static/range {v27 .. v28}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 666
    const/4 v9, 0x1

    .restart local v9    # "canDeleteLog":Z
    goto :goto_1

    .line 668
    .end local v9    # "canDeleteLog":Z
    :cond_3
    invoke-interface {v13}, Lorg/apache/http/Header;->getValue()Ljava/lang/String;

    move-result-object v19

    .line 669
    .local v19, "redirectUrl":Ljava/lang/String;
    add-int/lit8 v27, p4, -0x1

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    move-object/from16 v2, p2

    move-object/from16 v3, v19

    move/from16 v4, v27

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/google/android/play/analytics/EventLogger;->uploadLog(Landroid/accounts/Account;Lcom/google/android/play/analytics/ClientAnalytics$LogRequest;Ljava/lang/String;I)Z

    move-result v9

    .restart local v9    # "canDeleteLog":Z
    goto :goto_1

    .line 673
    .end local v9    # "canDeleteLog":Z
    .end local v13    # "locationHeader":Lorg/apache/http/Header;
    .end local v19    # "redirectUrl":Ljava/lang/String;
    :cond_4
    const-string v27, "PlayEventLogger"

    new-instance v28, Ljava/lang/StringBuilder;

    invoke-direct/range {v28 .. v28}, Ljava/lang/StringBuilder;-><init>()V

    const-string v29, "Server returned "

    invoke-virtual/range {v28 .. v29}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v28

    move-object/from16 v0, v28

    move/from16 v1, v25

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v28

    const-string v29, "... redirect, but no more redirects"

    invoke-virtual/range {v28 .. v29}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v28

    const-string v29, " allowed."

    invoke-virtual/range {v28 .. v29}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v28

    invoke-virtual/range {v28 .. v28}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v28

    invoke-static/range {v27 .. v28}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 676
    const/4 v9, 0x0

    .restart local v9    # "canDeleteLog":Z
    goto/16 :goto_1

    .line 678
    .end local v9    # "canDeleteLog":Z
    :cond_5
    const/16 v27, 0x190

    move/from16 v0, v25

    move/from16 v1, v27

    if-ne v0, v1, :cond_6

    .line 679
    const-string v27, "PlayEventLogger"

    const-string v28, "Server returned 400... deleting local malformed logs"

    invoke-static/range {v27 .. v28}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 680
    const/4 v9, 0x1

    .restart local v9    # "canDeleteLog":Z
    goto/16 :goto_1

    .line 681
    .end local v9    # "canDeleteLog":Z
    :cond_6
    const/16 v27, 0x191

    move/from16 v0, v25

    move/from16 v1, v27

    if-ne v0, v1, :cond_7

    .line 682
    const-string v27, "PlayEventLogger"

    const-string v28, "Server returned 401... invalidating auth token"

    invoke-static/range {v27 .. v28}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 683
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/play/analytics/EventLogger;->mContext:Landroid/content/Context;

    move-object/from16 v27, v0

    invoke-static/range {v27 .. v27}, Landroid/accounts/AccountManager;->get(Landroid/content/Context;)Landroid/accounts/AccountManager;

    move-result-object v14

    .line 684
    .local v14, "manager":Landroid/accounts/AccountManager;
    move-object/from16 v0, p1

    iget-object v0, v0, Landroid/accounts/Account;->type:Ljava/lang/String;

    move-object/from16 v27, v0

    move-object/from16 v0, v27

    invoke-virtual {v14, v0, v6}, Landroid/accounts/AccountManager;->invalidateAuthToken(Ljava/lang/String;Ljava/lang/String;)V

    .line 685
    const/4 v9, 0x0

    .line 686
    .restart local v9    # "canDeleteLog":Z
    goto/16 :goto_1

    .end local v9    # "canDeleteLog":Z
    .end local v14    # "manager":Landroid/accounts/AccountManager;
    :cond_7
    const/16 v27, 0x1f4

    move/from16 v0, v25

    move/from16 v1, v27

    if-ne v0, v1, :cond_8

    .line 687
    const-string v27, "PlayEventLogger"

    const-string v28, "Server returned 500... server crashed"

    invoke-static/range {v27 .. v28}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 689
    const/4 v9, 0x0

    .restart local v9    # "canDeleteLog":Z
    goto/16 :goto_1

    .line 690
    .end local v9    # "canDeleteLog":Z
    :cond_8
    const/16 v27, 0x1f5

    move/from16 v0, v25

    move/from16 v1, v27

    if-ne v0, v1, :cond_9

    .line 691
    const-string v27, "PlayEventLogger"

    const-string v28, "Server returned 501... service doesn\'t seem to exist"

    invoke-static/range {v27 .. v28}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 693
    const/4 v9, 0x0

    .restart local v9    # "canDeleteLog":Z
    goto/16 :goto_1

    .line 694
    .end local v9    # "canDeleteLog":Z
    :cond_9
    const/16 v27, 0x1f6

    move/from16 v0, v25

    move/from16 v1, v27

    if-ne v0, v1, :cond_a

    .line 695
    const-string v27, "PlayEventLogger"

    const-string v28, "Server returned 502... servers are down"

    invoke-static/range {v27 .. v28}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 697
    const/4 v9, 0x0

    .restart local v9    # "canDeleteLog":Z
    goto/16 :goto_1

    .line 698
    .end local v9    # "canDeleteLog":Z
    :cond_a
    const/16 v27, 0x1f7

    move/from16 v0, v25

    move/from16 v1, v27

    if-ne v0, v1, :cond_d

    .line 699
    const-string v27, "Retry-After"

    move-object/from16 v0, v20

    move-object/from16 v1, v27

    invoke-interface {v0, v1}, Lorg/apache/http/HttpResponse;->getFirstHeader(Ljava/lang/String;)Lorg/apache/http/Header;

    move-result-object v21

    .line 700
    .local v21, "retryHeader":Lorg/apache/http/Header;
    if-eqz v21, :cond_c

    .line 701
    const/16 v16, 0x0

    .line 702
    .local v16, "parsedRetryValue":Z
    invoke-interface/range {v21 .. v21}, Lorg/apache/http/Header;->getValue()Ljava/lang/String;

    move-result-object v24

    .line 704
    .local v24, "retryValue":Ljava/lang/String;
    :try_start_0
    invoke-static/range {v24 .. v24}, Ljava/lang/Long;->valueOf(Ljava/lang/String;)Ljava/lang/Long;

    move-result-object v27

    invoke-virtual/range {v27 .. v27}, Ljava/lang/Long;->longValue()J

    move-result-wide v22

    .line 705
    .local v22, "retryAfterInSeconds":J
    const-string v27, "PlayEventLogger"

    new-instance v28, Ljava/lang/StringBuilder;

    invoke-direct/range {v28 .. v28}, Ljava/lang/StringBuilder;-><init>()V

    const-string v29, "Server said to retry after "

    invoke-virtual/range {v28 .. v29}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v28

    move-object/from16 v0, v28

    move-wide/from16 v1, v22

    invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v28

    const-string v29, " seconds"

    invoke-virtual/range {v28 .. v29}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v28

    invoke-virtual/range {v28 .. v28}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v28

    invoke-static/range {v27 .. v28}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 706
    const-wide/16 v28, 0x3e8

    mul-long v28, v28, v22

    move-object/from16 v0, p0

    move-wide/from16 v1, v28

    invoke-direct {v0, v1, v2}, Lcom/google/android/play/analytics/EventLogger;->setNextUploadTimeAfter(J)V
    :try_end_0
    .catch Ljava/lang/NumberFormatException; {:try_start_0 .. :try_end_0} :catch_0

    .line 707
    const/16 v16, 0x1

    .line 712
    .end local v22    # "retryAfterInSeconds":J
    :goto_2
    if-nez v16, :cond_b

    const/4 v9, 0x1

    .line 713
    .restart local v9    # "canDeleteLog":Z
    :goto_3
    goto/16 :goto_1

    .line 708
    .end local v9    # "canDeleteLog":Z
    :catch_0
    move-exception v15

    .line 709
    .local v15, "ne":Ljava/lang/NumberFormatException;
    const-string v27, "PlayEventLogger"

    new-instance v28, Ljava/lang/StringBuilder;

    invoke-direct/range {v28 .. v28}, Ljava/lang/StringBuilder;-><init>()V

    const-string v29, "Unknown retry value: "

    invoke-virtual/range {v28 .. v29}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v28

    move-object/from16 v0, v28

    move-object/from16 v1, v24

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v28

    invoke-virtual/range {v28 .. v28}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v28

    invoke-static/range {v27 .. v28}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_2

    .line 712
    .end local v15    # "ne":Ljava/lang/NumberFormatException;
    :cond_b
    const/4 v9, 0x0

    goto :goto_3

    .line 714
    .end local v16    # "parsedRetryValue":Z
    .end local v24    # "retryValue":Ljava/lang/String;
    :cond_c
    const-string v27, "PlayEventLogger"

    const-string v28, "Status 503 without retry-after header"

    invoke-static/range {v27 .. v28}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 716
    const/4 v9, 0x1

    .restart local v9    # "canDeleteLog":Z
    goto/16 :goto_1

    .line 718
    .end local v9    # "canDeleteLog":Z
    .end local v21    # "retryHeader":Lorg/apache/http/Header;
    :cond_d
    const/16 v27, 0x1f8

    move/from16 v0, v25

    move/from16 v1, v27

    if-ne v0, v1, :cond_e

    .line 719
    const-string v27, "PlayEventLogger"

    const-string v28, "Server returned 504... timeout"

    invoke-static/range {v27 .. v28}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 721
    const/4 v9, 0x0

    .restart local v9    # "canDeleteLog":Z
    goto/16 :goto_1

    .line 723
    .end local v9    # "canDeleteLog":Z
    :cond_e
    const-string v27, "PlayEventLogger"

    new-instance v28, Ljava/lang/StringBuilder;

    invoke-direct/range {v28 .. v28}, Ljava/lang/StringBuilder;-><init>()V

    const-string v29, "Unexpected error received from server: "

    invoke-virtual/range {v28 .. v29}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v28

    move-object/from16 v0, v28

    move/from16 v1, v25

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v28

    const-string v29, " "

    invoke-virtual/range {v28 .. v29}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v28

    invoke-interface/range {v26 .. v26}, Lorg/apache/http/StatusLine;->getReasonPhrase()Ljava/lang/String;

    move-result-object v29

    invoke-virtual/range {v28 .. v29}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v28

    invoke-virtual/range {v28 .. v28}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v28

    invoke-static/range {v27 .. v28}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 726
    const/4 v9, 0x1

    .restart local v9    # "canDeleteLog":Z
    goto/16 :goto_1
.end method

.method private writeRawVarint32(ILjava/io/OutputStream;)V
    .locals 1
    .param p1, "value"    # I
    .param p2, "out"    # Ljava/io/OutputStream;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 481
    :goto_0
    and-int/lit8 v0, p1, -0x80

    if-nez v0, :cond_0

    .line 482
    invoke-virtual {p2, p1}, Ljava/io/OutputStream;->write(I)V

    .line 483
    return-void

    .line 485
    :cond_0
    and-int/lit8 v0, p1, 0x7f

    or-int/lit16 v0, v0, 0x80

    invoke-virtual {p2, v0}, Ljava/io/OutputStream;->write(I)V

    .line 486
    ushr-int/lit8 p1, p1, 0x7

    goto :goto_0
.end method


# virtual methods
.method public varargs logEvent(Ljava/lang/String;Lcom/google/android/play/analytics/ClientAnalytics$ActiveExperiments;[B[Ljava/lang/String;)V
    .locals 7
    .param p1, "tag"    # Ljava/lang/String;
    .param p2, "experiments"    # Lcom/google/android/play/analytics/ClientAnalytics$ActiveExperiments;
    .param p3, "sourceExtension"    # [B
    .param p4, "extras"    # [Ljava/lang/String;

    .prologue
    .line 319
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v4

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v6, p4

    invoke-direct/range {v0 .. v6}, Lcom/google/android/play/analytics/EventLogger;->logEvent(Ljava/lang/String;Lcom/google/android/play/analytics/ClientAnalytics$ActiveExperiments;[BJ[Ljava/lang/String;)V

    .line 320
    return-void
.end method

.method public onNewOutputFile()V
    .locals 1

    .prologue
    .line 452
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/play/analytics/EventLogger;->mLastSentExperiments:Lcom/google/android/play/analytics/ClientAnalytics$ActiveExperiments;

    .line 453
    return-void
.end method

.method public onWrite(Lcom/google/android/play/analytics/ClientAnalytics$LogEvent;Ljava/io/OutputStream;)V
    .locals 3
    .param p1, "event"    # Lcom/google/android/play/analytics/ClientAnalytics$LogEvent;
    .param p2, "out"    # Ljava/io/OutputStream;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 457
    iget-object v0, p1, Lcom/google/android/play/analytics/ClientAnalytics$LogEvent;->exp:Lcom/google/android/play/analytics/ClientAnalytics$ActiveExperiments;

    .line 458
    .local v0, "eventExperiments":Lcom/google/android/play/analytics/ClientAnalytics$ActiveExperiments;
    iget-object v2, p0, Lcom/google/android/play/analytics/EventLogger;->mLastSentExperiments:Lcom/google/android/play/analytics/ClientAnalytics$ActiveExperiments;

    if-ne v0, v2, :cond_1

    .line 461
    const/4 v2, 0x0

    iput-object v2, p1, Lcom/google/android/play/analytics/ClientAnalytics$LogEvent;->exp:Lcom/google/android/play/analytics/ClientAnalytics$ActiveExperiments;

    .line 470
    :cond_0
    :goto_0
    invoke-static {p1}, Lcom/google/protobuf/nano/MessageNano;->toByteArray(Lcom/google/protobuf/nano/MessageNano;)[B

    move-result-object v1

    .line 471
    .local v1, "serializedMessage":[B
    array-length v2, v1

    invoke-direct {p0, v2, p2}, Lcom/google/android/play/analytics/EventLogger;->writeRawVarint32(ILjava/io/OutputStream;)V

    .line 472
    invoke-virtual {p2, v1}, Ljava/io/OutputStream;->write([B)V

    .line 473
    return-void

    .line 463
    .end local v1    # "serializedMessage":[B
    :cond_1
    iget-object v2, p1, Lcom/google/android/play/analytics/ClientAnalytics$LogEvent;->exp:Lcom/google/android/play/analytics/ClientAnalytics$ActiveExperiments;

    iput-object v2, p0, Lcom/google/android/play/analytics/EventLogger;->mLastSentExperiments:Lcom/google/android/play/analytics/ClientAnalytics$ActiveExperiments;

    .line 464
    iget-object v2, p1, Lcom/google/android/play/analytics/ClientAnalytics$LogEvent;->exp:Lcom/google/android/play/analytics/ClientAnalytics$ActiveExperiments;

    if-nez v2, :cond_0

    .line 467
    sget-object v2, Lcom/google/android/play/analytics/EventLogger;->EMPTY_EXPERIMENTS:Lcom/google/android/play/analytics/ClientAnalytics$ActiveExperiments;

    iput-object v2, p1, Lcom/google/android/play/analytics/ClientAnalytics$LogEvent;->exp:Lcom/google/android/play/analytics/ClientAnalytics$ActiveExperiments;

    goto :goto_0
.end method

.method public bridge synthetic onWrite(Ljava/lang/Object;Ljava/io/OutputStream;)V
    .locals 0
    .param p1, "x0"    # Ljava/lang/Object;
    .param p2, "x1"    # Ljava/io/OutputStream;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 66
    check-cast p1, Lcom/google/android/play/analytics/ClientAnalytics$LogEvent;

    .end local p1    # "x0":Ljava/lang/Object;
    invoke-virtual {p0, p1, p2}, Lcom/google/android/play/analytics/EventLogger;->onWrite(Lcom/google/android/play/analytics/ClientAnalytics$LogEvent;Ljava/io/OutputStream;)V

    return-void
.end method
