.class public Lcom/google/android/play/search/PlaySearch;
.super Landroid/widget/LinearLayout;
.source "PlaySearch.java"

# interfaces
.implements Lcom/google/android/play/search/PlaySearchListener;


# instance fields
.field private mController:Lcom/google/android/play/search/PlaySearchController;

.field private mListener:Lcom/google/android/play/search/PlaySearchListener;

.field private mSearchPlate:Lcom/google/android/play/search/PlaySearchPlate;

.field private mSuggestionsList:Lcom/google/android/play/search/PlaySearchSuggestionsList;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 24
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/google/android/play/search/PlaySearch;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 25
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    .line 28
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/google/android/play/search/PlaySearch;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 29
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 0
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;
    .param p3, "defStyle"    # I

    .prologue
    .line 32
    invoke-direct {p0, p1, p2, p3}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 33
    return-void
.end method


# virtual methods
.method public onFinishInflate()V
    .locals 2

    .prologue
    .line 37
    invoke-super {p0}, Landroid/widget/LinearLayout;->onFinishInflate()V

    .line 38
    sget v0, Lcom/google/android/play/R$id;->play_search_plate:I

    invoke-virtual {p0, v0}, Lcom/google/android/play/search/PlaySearch;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/play/search/PlaySearchPlate;

    iput-object v0, p0, Lcom/google/android/play/search/PlaySearch;->mSearchPlate:Lcom/google/android/play/search/PlaySearchPlate;

    .line 39
    sget v0, Lcom/google/android/play/R$id;->play_search_suggestions_list:I

    invoke-virtual {p0, v0}, Lcom/google/android/play/search/PlaySearch;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/play/search/PlaySearchSuggestionsList;

    iput-object v0, p0, Lcom/google/android/play/search/PlaySearch;->mSuggestionsList:Lcom/google/android/play/search/PlaySearchSuggestionsList;

    .line 42
    new-instance v0, Lcom/google/android/play/search/PlaySearchController;

    invoke-direct {v0}, Lcom/google/android/play/search/PlaySearchController;-><init>()V

    iput-object v0, p0, Lcom/google/android/play/search/PlaySearch;->mController:Lcom/google/android/play/search/PlaySearchController;

    .line 43
    iget-object v0, p0, Lcom/google/android/play/search/PlaySearch;->mController:Lcom/google/android/play/search/PlaySearchController;

    invoke-virtual {v0, p0}, Lcom/google/android/play/search/PlaySearchController;->addPlaySearchListener(Lcom/google/android/play/search/PlaySearchListener;)V

    .line 44
    iget-object v0, p0, Lcom/google/android/play/search/PlaySearch;->mSearchPlate:Lcom/google/android/play/search/PlaySearchPlate;

    iget-object v1, p0, Lcom/google/android/play/search/PlaySearch;->mController:Lcom/google/android/play/search/PlaySearchController;

    invoke-virtual {v0, v1}, Lcom/google/android/play/search/PlaySearchPlate;->setPlaySearchController(Lcom/google/android/play/search/PlaySearchController;)V

    .line 45
    iget-object v0, p0, Lcom/google/android/play/search/PlaySearch;->mSuggestionsList:Lcom/google/android/play/search/PlaySearchSuggestionsList;

    iget-object v1, p0, Lcom/google/android/play/search/PlaySearch;->mController:Lcom/google/android/play/search/PlaySearchController;

    invoke-virtual {v0, v1}, Lcom/google/android/play/search/PlaySearchSuggestionsList;->setPlaySearchController(Lcom/google/android/play/search/PlaySearchController;)V

    .line 46
    return-void
.end method

.method public onModeChanged(I)V
    .locals 1
    .param p1, "searchMode"    # I

    .prologue
    .line 78
    iget-object v0, p0, Lcom/google/android/play/search/PlaySearch;->mListener:Lcom/google/android/play/search/PlaySearchListener;

    if-eqz v0, :cond_0

    .line 79
    iget-object v0, p0, Lcom/google/android/play/search/PlaySearch;->mListener:Lcom/google/android/play/search/PlaySearchListener;

    invoke-interface {v0, p1}, Lcom/google/android/play/search/PlaySearchListener;->onModeChanged(I)V

    .line 81
    :cond_0
    return-void
.end method

.method public onQueryChanged(Ljava/lang/String;)V
    .locals 1
    .param p1, "text"    # Ljava/lang/String;

    .prologue
    .line 71
    iget-object v0, p0, Lcom/google/android/play/search/PlaySearch;->mListener:Lcom/google/android/play/search/PlaySearchListener;

    if-eqz v0, :cond_0

    .line 72
    iget-object v0, p0, Lcom/google/android/play/search/PlaySearch;->mListener:Lcom/google/android/play/search/PlaySearchListener;

    invoke-interface {v0, p1}, Lcom/google/android/play/search/PlaySearchListener;->onQueryChanged(Ljava/lang/String;)V

    .line 74
    :cond_0
    return-void
.end method
