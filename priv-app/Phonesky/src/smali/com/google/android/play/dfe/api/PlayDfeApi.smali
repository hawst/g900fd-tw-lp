.class public interface abstract Lcom/google/android/play/dfe/api/PlayDfeApi;
.super Ljava/lang/Object;
.source "PlayDfeApi.java"


# static fields
.field public static final BASE_URI:Landroid/net/Uri;

.field public static final PLUS_PROFILE_URI:Landroid/net/Uri;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 28
    const-string v0, "https://android.clients.google.com/fdfe/"

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    sput-object v0, Lcom/google/android/play/dfe/api/PlayDfeApi;->BASE_URI:Landroid/net/Uri;

    .line 31
    const-string v0, "api/plusProfile"

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    sput-object v0, Lcom/google/android/play/dfe/api/PlayDfeApi;->PLUS_PROFILE_URI:Landroid/net/Uri;

    return-void
.end method


# virtual methods
.method public abstract getPlusProfile(Lcom/android/volley/Response$Listener;Lcom/android/volley/Response$ErrorListener;Z)Lcom/android/volley/Request;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/android/volley/Response$Listener",
            "<",
            "Lcom/google/android/finsky/protos/PlusProfile$PlusProfileResponse;",
            ">;",
            "Lcom/android/volley/Response$ErrorListener;",
            "Z)",
            "Lcom/android/volley/Request",
            "<*>;"
        }
    .end annotation
.end method

.method public abstract invalidatePlusProfile(Z)V
.end method
