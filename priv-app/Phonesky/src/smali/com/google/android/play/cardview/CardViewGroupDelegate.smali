.class public interface abstract Lcom/google/android/play/cardview/CardViewGroupDelegate;
.super Ljava/lang/Object;
.source "CardViewGroupDelegate.java"


# virtual methods
.method public abstract initialize(Landroid/view/View;Landroid/content/Context;Landroid/util/AttributeSet;I)V
.end method

.method public abstract setBackgroundColor(Landroid/view/View;I)V
.end method

.method public abstract setBackgroundResource(Landroid/view/View;I)V
.end method

.method public abstract setCardElevation(Landroid/view/View;F)V
.end method
