.class final Lcom/google/android/play/layout/FlowLayoutManager$ParagraphInfo;
.super Lcom/google/android/play/layout/FlowLayoutManager$MultiItemInfo;
.source "FlowLayoutManager.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/play/layout/FlowLayoutManager;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1a
    name = "ParagraphInfo"
.end annotation


# static fields
.field private static final sPool:Landroid/support/v4/util/Pools$Pool;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/support/v4/util/Pools$Pool",
            "<",
            "Lcom/google/android/play/layout/FlowLayoutManager$ParagraphInfo;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public final mLines:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/android/play/layout/FlowLayoutManager$LineInfo;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 1624
    new-instance v0, Landroid/support/v4/util/Pools$SimplePool;

    const/16 v1, 0xf

    invoke-direct {v0, v1}, Landroid/support/v4/util/Pools$SimplePool;-><init>(I)V

    sput-object v0, Lcom/google/android/play/layout/FlowLayoutManager$ParagraphInfo;->sPool:Landroid/support/v4/util/Pools$Pool;

    return-void
.end method

.method private constructor <init>()V
    .locals 1

    .prologue
    .line 1636
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/google/android/play/layout/FlowLayoutManager$MultiItemInfo;-><init>(Lcom/google/android/play/layout/FlowLayoutManager$1;)V

    .line 1643
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/android/play/layout/FlowLayoutManager$ParagraphInfo;->mLines:Ljava/util/List;

    .line 1637
    invoke-virtual {p0}, Lcom/google/android/play/layout/FlowLayoutManager$ParagraphInfo;->reset()V

    .line 1638
    return-void
.end method

.method public static obtain(I)Lcom/google/android/play/layout/FlowLayoutManager$ParagraphInfo;
    .locals 2
    .param p0, "positionStart"    # I

    .prologue
    .line 1628
    sget-object v1, Lcom/google/android/play/layout/FlowLayoutManager$ParagraphInfo;->sPool:Landroid/support/v4/util/Pools$Pool;

    invoke-interface {v1}, Landroid/support/v4/util/Pools$Pool;->acquire()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/play/layout/FlowLayoutManager$ParagraphInfo;

    .line 1629
    .local v0, "paragraph":Lcom/google/android/play/layout/FlowLayoutManager$ParagraphInfo;
    if-nez v0, :cond_0

    .line 1630
    new-instance v0, Lcom/google/android/play/layout/FlowLayoutManager$ParagraphInfo;

    .end local v0    # "paragraph":Lcom/google/android/play/layout/FlowLayoutManager$ParagraphInfo;
    invoke-direct {v0}, Lcom/google/android/play/layout/FlowLayoutManager$ParagraphInfo;-><init>()V

    .line 1632
    .restart local v0    # "paragraph":Lcom/google/android/play/layout/FlowLayoutManager$ParagraphInfo;
    :cond_0
    iput p0, v0, Lcom/google/android/play/layout/FlowLayoutManager$ParagraphInfo;->mPositionStart:I

    .line 1633
    return-object v0
.end method


# virtual methods
.method public addLine(Lcom/google/android/play/layout/FlowLayoutManager$LineInfo;)V
    .locals 1
    .param p1, "newLine"    # Lcom/google/android/play/layout/FlowLayoutManager$LineInfo;

    .prologue
    .line 1650
    iget-object v0, p0, Lcom/google/android/play/layout/FlowLayoutManager$ParagraphInfo;->mLines:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1651
    invoke-virtual {p0}, Lcom/google/android/play/layout/FlowLayoutManager$ParagraphInfo;->invalidateHeight()V

    .line 1652
    return-void
.end method

.method public clearMeasuredInCurrentPass()V
    .locals 2

    .prologue
    .line 1655
    iget-object v1, p0, Lcom/google/android/play/layout/FlowLayoutManager$ParagraphInfo;->mLines:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    add-int/lit8 v0, v1, -0x1

    .local v0, "i":I
    :goto_0
    if-ltz v0, :cond_0

    .line 1656
    iget-object v1, p0, Lcom/google/android/play/layout/FlowLayoutManager$ParagraphInfo;->mLines:Ljava/util/List;

    invoke-interface {v1, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/play/layout/FlowLayoutManager$LineInfo;

    invoke-virtual {v1}, Lcom/google/android/play/layout/FlowLayoutManager$LineInfo;->clearMeasuredInCurrentPass()V

    .line 1655
    add-int/lit8 v0, v0, -0x1

    goto :goto_0

    .line 1658
    :cond_0
    return-void
.end method

.method debugPrint(Ljava/lang/StringBuilder;)V
    .locals 3
    .param p1, "output"    # Ljava/lang/StringBuilder;

    .prologue
    const/4 v2, 0x0

    .line 1749
    const/16 v1, 0x7b

    invoke-virtual {p1, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 1750
    iget-object v1, p0, Lcom/google/android/play/layout/FlowLayoutManager$ParagraphInfo;->mLines:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_0

    .line 1751
    iget-object v1, p0, Lcom/google/android/play/layout/FlowLayoutManager$ParagraphInfo;->mLines:Ljava/util/List;

    invoke-interface {v1, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/play/layout/FlowLayoutManager$LineInfo;

    invoke-virtual {v1, v2, p1}, Lcom/google/android/play/layout/FlowLayoutManager$LineInfo;->debugPrint(ILjava/lang/StringBuilder;)V

    .line 1752
    const/4 v0, 0x1

    .local v0, "i":I
    :goto_0
    iget-object v1, p0, Lcom/google/android/play/layout/FlowLayoutManager$ParagraphInfo;->mLines:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    if-ge v0, v1, :cond_0

    .line 1753
    const/16 v1, 0x2c

    invoke-virtual {p1, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 1754
    iget-object v1, p0, Lcom/google/android/play/layout/FlowLayoutManager$ParagraphInfo;->mLines:Ljava/util/List;

    invoke-interface {v1, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/play/layout/FlowLayoutManager$LineInfo;

    invoke-virtual {v1, v0, p1}, Lcom/google/android/play/layout/FlowLayoutManager$LineInfo;->debugPrint(ILjava/lang/StringBuilder;)V

    .line 1752
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 1757
    .end local v0    # "i":I
    :cond_0
    const/16 v1, 0x7d

    invoke-virtual {p1, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 1758
    return-void
.end method

.method public getItemTopOffset(I)I
    .locals 5
    .param p1, "position"    # I

    .prologue
    .line 1682
    const/4 v3, 0x0

    .line 1683
    .local v3, "topOffset":I
    const/4 v0, 0x0

    .line 1686
    .local v0, "found":Z
    iget-object v4, p0, Lcom/google/android/play/layout/FlowLayoutManager$ParagraphInfo;->mLines:Ljava/util/List;

    invoke-interface {v4}, Ljava/util/List;->size()I

    move-result v4

    add-int/lit8 v1, v4, -0x1

    .local v1, "i":I
    :goto_0
    if-ltz v1, :cond_2

    .line 1687
    iget-object v4, p0, Lcom/google/android/play/layout/FlowLayoutManager$ParagraphInfo;->mLines:Ljava/util/List;

    invoke-interface {v4, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/google/android/play/layout/FlowLayoutManager$LineInfo;

    .line 1688
    .local v2, "line":Lcom/google/android/play/layout/FlowLayoutManager$LineInfo;
    if-eqz v0, :cond_1

    .line 1689
    iget v4, v2, Lcom/google/android/play/layout/FlowLayoutManager$LineInfo;->mTotalHeight:I

    add-int/2addr v3, v4

    .line 1686
    :cond_0
    :goto_1
    add-int/lit8 v1, v1, -0x1

    goto :goto_0

    .line 1690
    :cond_1
    iget v4, v2, Lcom/google/android/play/layout/FlowLayoutManager$LineInfo;->mPositionStart:I

    if-gt v4, p1, :cond_0

    .line 1691
    invoke-virtual {v2, p1}, Lcom/google/android/play/layout/FlowLayoutManager$LineInfo;->getItemTopOffset(I)I

    move-result v3

    .line 1692
    const/4 v0, 0x1

    goto :goto_1

    .line 1695
    .end local v2    # "line":Lcom/google/android/play/layout/FlowLayoutManager$LineInfo;
    :cond_2
    return v3
.end method

.method public final getLastLine()Lcom/google/android/play/layout/FlowLayoutManager$LineInfo;
    .locals 2

    .prologue
    .line 1646
    iget-object v0, p0, Lcom/google/android/play/layout/FlowLayoutManager$ParagraphInfo;->mLines:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/google/android/play/layout/FlowLayoutManager$ParagraphInfo;->mLines:Ljava/util/List;

    iget-object v1, p0, Lcom/google/android/play/layout/FlowLayoutManager$ParagraphInfo;->mLines:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    add-int/lit8 v1, v1, -0x1

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/play/layout/FlowLayoutManager$LineInfo;

    goto :goto_0
.end method

.method protected invalidateFromInternal(I)Z
    .locals 6
    .param p1, "position"    # I

    .prologue
    const/4 v3, 0x1

    const/4 v4, 0x0

    .line 1700
    iget-object v5, p0, Lcom/google/android/play/layout/FlowLayoutManager$ParagraphInfo;->mLines:Ljava/util/List;

    invoke-interface {v5}, Ljava/util/List;->size()I

    move-result v2

    .line 1701
    .local v2, "lineCount":I
    if-nez v2, :cond_1

    move v3, v4

    .line 1716
    :cond_0
    :goto_0
    :pswitch_0
    return v3

    .line 1706
    :cond_1
    add-int/lit8 v0, v2, -0x1

    .local v0, "i":I
    :goto_1
    if-ltz v0, :cond_2

    .line 1707
    iget-object v5, p0, Lcom/google/android/play/layout/FlowLayoutManager$ParagraphInfo;->mLines:Ljava/util/List;

    invoke-interface {v5, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/play/layout/FlowLayoutManager$LineInfo;

    .line 1708
    .local v1, "line":Lcom/google/android/play/layout/FlowLayoutManager$LineInfo;
    invoke-virtual {v1, p1}, Lcom/google/android/play/layout/FlowLayoutManager$LineInfo;->invalidateFrom(I)I

    move-result v5

    packed-switch v5, :pswitch_data_0

    .line 1706
    :goto_2
    add-int/lit8 v0, v0, -0x1

    goto :goto_1

    .line 1712
    :pswitch_1
    add-int/lit8 v5, v2, -0x1

    if-ne v0, v5, :cond_0

    move v3, v4

    goto :goto_0

    .line 1718
    :pswitch_2
    invoke-virtual {v1}, Lcom/google/android/play/layout/FlowLayoutManager$LineInfo;->recycle()V

    .line 1719
    iget-object v5, p0, Lcom/google/android/play/layout/FlowLayoutManager$ParagraphInfo;->mLines:Ljava/util/List;

    invoke-interface {v5, v0}, Ljava/util/List;->remove(I)Ljava/lang/Object;

    goto :goto_2

    .line 1723
    .end local v1    # "line":Lcom/google/android/play/layout/FlowLayoutManager$LineInfo;
    :cond_2
    new-instance v3, Ljava/lang/Error;

    const-string v4, "Should not reach here"

    invoke-direct {v3, v4}, Ljava/lang/Error;-><init>(Ljava/lang/String;)V

    throw v3

    .line 1708
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_0
        :pswitch_2
    .end packed-switch
.end method

.method public offsetPositions(I)V
    .locals 2
    .param p1, "delta"    # I

    .prologue
    .line 1728
    invoke-super {p0, p1}, Lcom/google/android/play/layout/FlowLayoutManager$MultiItemInfo;->offsetPositions(I)V

    .line 1729
    iget-object v1, p0, Lcom/google/android/play/layout/FlowLayoutManager$ParagraphInfo;->mLines:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    add-int/lit8 v0, v1, -0x1

    .local v0, "i":I
    :goto_0
    if-ltz v0, :cond_0

    .line 1730
    iget-object v1, p0, Lcom/google/android/play/layout/FlowLayoutManager$ParagraphInfo;->mLines:Ljava/util/List;

    invoke-interface {v1, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/play/layout/FlowLayoutManager$LineInfo;

    invoke-virtual {v1, p1}, Lcom/google/android/play/layout/FlowLayoutManager$LineInfo;->offsetPositions(I)V

    .line 1729
    add-int/lit8 v0, v0, -0x1

    goto :goto_0

    .line 1732
    :cond_0
    return-void
.end method

.method protected onArrange(ZI)I
    .locals 4
    .param p1, "fullArrangementRequired"    # Z
    .param p2, "totalItemCount"    # I

    .prologue
    .line 1668
    const/4 v2, 0x0

    .line 1669
    .local v2, "totalHeight":I
    iget-object v3, p0, Lcom/google/android/play/layout/FlowLayoutManager$ParagraphInfo;->mLines:Ljava/util/List;

    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v1

    .line 1670
    .local v1, "lineCount":I
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    if-ge v0, v1, :cond_0

    .line 1671
    iget-object v3, p0, Lcom/google/android/play/layout/FlowLayoutManager$ParagraphInfo;->mLines:Ljava/util/List;

    invoke-interface {v3, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/google/android/play/layout/FlowLayoutManager$LineInfo;

    invoke-virtual {v3, p2}, Lcom/google/android/play/layout/FlowLayoutManager$LineInfo;->arrangeIfNecessary(I)I

    move-result v3

    add-int/2addr v2, v3

    .line 1670
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 1673
    :cond_0
    return v2
.end method

.method public recycle()V
    .locals 1

    .prologue
    .line 1735
    invoke-virtual {p0}, Lcom/google/android/play/layout/FlowLayoutManager$ParagraphInfo;->reset()V

    .line 1736
    sget-object v0, Lcom/google/android/play/layout/FlowLayoutManager$ParagraphInfo;->sPool:Landroid/support/v4/util/Pools$Pool;

    invoke-interface {v0, p0}, Landroid/support/v4/util/Pools$Pool;->release(Ljava/lang/Object;)Z

    .line 1737
    return-void
.end method

.method protected reset()V
    .locals 2

    .prologue
    .line 1741
    invoke-super {p0}, Lcom/google/android/play/layout/FlowLayoutManager$MultiItemInfo;->reset()V

    .line 1742
    iget-object v1, p0, Lcom/google/android/play/layout/FlowLayoutManager$ParagraphInfo;->mLines:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    add-int/lit8 v0, v1, -0x1

    .local v0, "i":I
    :goto_0
    if-ltz v0, :cond_0

    .line 1743
    iget-object v1, p0, Lcom/google/android/play/layout/FlowLayoutManager$ParagraphInfo;->mLines:Ljava/util/List;

    invoke-interface {v1, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/play/layout/FlowLayoutManager$LineInfo;

    invoke-virtual {v1}, Lcom/google/android/play/layout/FlowLayoutManager$LineInfo;->recycle()V

    .line 1742
    add-int/lit8 v0, v0, -0x1

    goto :goto_0

    .line 1745
    :cond_0
    iget-object v1, p0, Lcom/google/android/play/layout/FlowLayoutManager$ParagraphInfo;->mLines:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->clear()V

    .line 1746
    return-void
.end method

.method public validPositionEnd()I
    .locals 2

    .prologue
    .line 1662
    iget-object v0, p0, Lcom/google/android/play/layout/FlowLayoutManager$ParagraphInfo;->mLines:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    iget v0, p0, Lcom/google/android/play/layout/FlowLayoutManager$ParagraphInfo;->mPositionStart:I

    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lcom/google/android/play/layout/FlowLayoutManager$ParagraphInfo;->mLines:Ljava/util/List;

    iget-object v1, p0, Lcom/google/android/play/layout/FlowLayoutManager$ParagraphInfo;->mLines:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    add-int/lit8 v1, v1, -0x1

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/play/layout/FlowLayoutManager$LineInfo;

    invoke-virtual {v0}, Lcom/google/android/play/layout/FlowLayoutManager$LineInfo;->validPositionEnd()I

    move-result v0

    goto :goto_0
.end method
