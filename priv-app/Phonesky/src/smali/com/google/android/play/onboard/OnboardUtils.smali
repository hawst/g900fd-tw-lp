.class public Lcom/google/android/play/onboard/OnboardUtils;
.super Ljava/lang/Object;
.source "OnboardUtils.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/play/onboard/OnboardUtils$Predicate;
    }
.end annotation


# direct methods
.method public static getAllDescendants(Landroid/view/ViewGroup;Lcom/google/android/play/onboard/OnboardUtils$Predicate;)Ljava/util/Collection;
    .locals 7
    .param p0, "root"    # Landroid/view/ViewGroup;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/view/ViewGroup;",
            "Lcom/google/android/play/onboard/OnboardUtils$Predicate",
            "<",
            "Landroid/view/View;",
            ">;)",
            "Ljava/util/Collection",
            "<",
            "Landroid/view/View;",
            ">;"
        }
    .end annotation

    .prologue
    .line 38
    .local p1, "optPredicate":Lcom/google/android/play/onboard/OnboardUtils$Predicate;, "Lcom/google/android/play/onboard/OnboardUtils$Predicate<Landroid/view/View;>;"
    invoke-static {}, Lcom/google/android/play/utils/collections/Lists;->newArrayList()Ljava/util/ArrayList;

    move-result-object v4

    .line 39
    .local v4, "matches":Ljava/util/List;, "Ljava/util/List<Landroid/view/View;>;"
    invoke-static {}, Lcom/google/android/play/utils/collections/Lists;->newArrayList()Ljava/util/ArrayList;

    move-result-object v5

    .line 42
    .local v5, "unvisited":Ljava/util/List;, "Ljava/util/List<Landroid/view/View;>;"
    invoke-interface {v5, p0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 43
    :cond_0
    invoke-interface {v5}, Ljava/util/List;->isEmpty()Z

    move-result v6

    if-nez v6, :cond_3

    .line 44
    const/4 v6, 0x0

    invoke-interface {v5, v6}, Ljava/util/List;->remove(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    .line 45
    .local v0, "child":Landroid/view/View;
    if-eqz p1, :cond_1

    invoke-interface {p1, v0}, Lcom/google/android/play/onboard/OnboardUtils$Predicate;->apply(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_2

    .line 46
    :cond_1
    invoke-interface {v4, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 48
    :cond_2
    instance-of v6, v0, Landroid/view/ViewGroup;

    if-eqz v6, :cond_0

    move-object v2, v0

    .line 49
    check-cast v2, Landroid/view/ViewGroup;

    .line 50
    .local v2, "group":Landroid/view/ViewGroup;
    invoke-virtual {v2}, Landroid/view/ViewGroup;->getChildCount()I

    move-result v1

    .line 51
    .local v1, "count":I
    const/4 v3, 0x0

    .local v3, "i":I
    :goto_0
    if-ge v3, v1, :cond_0

    .line 52
    invoke-virtual {v2, v3}, Landroid/view/ViewGroup;->getChildAt(I)Landroid/view/View;

    move-result-object v6

    invoke-interface {v5, v6}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 51
    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    .line 57
    .end local v0    # "child":Landroid/view/View;
    .end local v1    # "count":I
    .end local v2    # "group":Landroid/view/ViewGroup;
    .end local v3    # "i":I
    :cond_3
    return-object v4
.end method
