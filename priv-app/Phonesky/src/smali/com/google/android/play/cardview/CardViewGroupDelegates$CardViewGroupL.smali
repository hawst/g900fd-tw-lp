.class Lcom/google/android/play/cardview/CardViewGroupDelegates$CardViewGroupL;
.super Lcom/google/android/play/cardview/CardViewGroupDelegates$CardViewGroupEclairMr1;
.source "CardViewGroupDelegates.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/play/cardview/CardViewGroupDelegates;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "CardViewGroupL"
.end annotation


# direct methods
.method private constructor <init>()V
    .locals 1

    .prologue
    .line 71
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/google/android/play/cardview/CardViewGroupDelegates$CardViewGroupEclairMr1;-><init>(Lcom/google/android/play/cardview/CardViewGroupDelegates$1;)V

    return-void
.end method

.method synthetic constructor <init>(Lcom/google/android/play/cardview/CardViewGroupDelegates$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/google/android/play/cardview/CardViewGroupDelegates$1;

    .prologue
    .line 71
    invoke-direct {p0}, Lcom/google/android/play/cardview/CardViewGroupDelegates$CardViewGroupL;-><init>()V

    return-void
.end method


# virtual methods
.method protected getClipToOutline(Landroid/content/res/TypedArray;)Z
    .locals 2
    .param p1, "array"    # Landroid/content/res/TypedArray;

    .prologue
    .line 92
    sget v0, Lcom/google/android/play/R$styleable;->PlayCardViewGroup_playCardClipToOutline:I

    const/4 v1, 0x1

    invoke-virtual {p1, v0, v1}, Landroid/content/res/TypedArray;->getBoolean(IZ)Z

    move-result v0

    return v0
.end method

.method public initialize(Landroid/view/View;Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 5
    .param p1, "cardView"    # Landroid/view/View;
    .param p2, "context"    # Landroid/content/Context;
    .param p3, "attrs"    # Landroid/util/AttributeSet;
    .param p4, "defStyle"    # I

    .prologue
    .line 76
    invoke-virtual {p0, p2, p3, p4}, Lcom/google/android/play/cardview/CardViewGroupDelegates$CardViewGroupL;->getStyledAttrs(Landroid/content/Context;Landroid/util/AttributeSet;I)Landroid/content/res/TypedArray;

    move-result-object v0

    .line 78
    .local v0, "array":Landroid/content/res/TypedArray;
    new-instance v1, Lcom/google/android/play/cardview/RoundRectDrawable;

    invoke-virtual {p0, v0}, Lcom/google/android/play/cardview/CardViewGroupDelegates$CardViewGroupL;->getBackgroundColor(Landroid/content/res/TypedArray;)Landroid/content/res/ColorStateList;

    move-result-object v2

    invoke-virtual {p0, v0}, Lcom/google/android/play/cardview/CardViewGroupDelegates$CardViewGroupL;->getRadius(Landroid/content/res/TypedArray;)F

    move-result v3

    invoke-virtual {p0, v0}, Lcom/google/android/play/cardview/CardViewGroupDelegates$CardViewGroupL;->getInset(Landroid/content/res/TypedArray;)I

    move-result v4

    int-to-float v4, v4

    invoke-direct {v1, v2, v3, v4}, Lcom/google/android/play/cardview/RoundRectDrawable;-><init>(Landroid/content/res/ColorStateList;FF)V

    .line 83
    .local v1, "cardBackground":Landroid/graphics/drawable/Drawable;
    const/4 v2, 0x1

    invoke-virtual {p1, v2}, Landroid/view/View;->setClipToOutline(Z)V

    .line 84
    invoke-virtual {p0, v0}, Lcom/google/android/play/cardview/CardViewGroupDelegates$CardViewGroupL;->getElevation(Landroid/content/res/TypedArray;)F

    move-result v2

    invoke-virtual {p1, v2}, Landroid/view/View;->setElevation(F)V

    .line 85
    invoke-virtual {p1, v1}, Landroid/view/View;->setBackground(Landroid/graphics/drawable/Drawable;)V

    .line 86
    invoke-virtual {p0, v0}, Lcom/google/android/play/cardview/CardViewGroupDelegates$CardViewGroupL;->getClipToOutline(Landroid/content/res/TypedArray;)Z

    move-result v2

    invoke-virtual {p1, v2}, Landroid/view/View;->setClipToOutline(Z)V

    .line 88
    invoke-virtual {v0}, Landroid/content/res/TypedArray;->recycle()V

    .line 89
    return-void
.end method

.method public setCardElevation(Landroid/view/View;F)V
    .locals 0
    .param p1, "cardView"    # Landroid/view/View;
    .param p2, "elevation"    # F

    .prologue
    .line 97
    invoke-virtual {p1, p2}, Landroid/view/View;->setElevation(F)V

    .line 98
    return-void
.end method
