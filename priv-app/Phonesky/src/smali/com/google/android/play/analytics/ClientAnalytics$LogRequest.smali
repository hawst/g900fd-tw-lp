.class public final Lcom/google/android/play/analytics/ClientAnalytics$LogRequest;
.super Lcom/google/protobuf/nano/MessageNano;
.source "ClientAnalytics.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/play/analytics/ClientAnalytics;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "LogRequest"
.end annotation


# instance fields
.field public clientInfo:Lcom/google/android/play/analytics/ClientAnalytics$ClientInfo;

.field public logEvent:[Lcom/google/android/play/analytics/ClientAnalytics$LogEvent;

.field public logSource:I

.field public logSourceName:Ljava/lang/String;

.field public requestTimeMs:J

.field public serializedLogEvents:[[B

.field public zwiebackCookie:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1966
    invoke-direct {p0}, Lcom/google/protobuf/nano/MessageNano;-><init>()V

    .line 1967
    invoke-virtual {p0}, Lcom/google/android/play/analytics/ClientAnalytics$LogRequest;->clear()Lcom/google/android/play/analytics/ClientAnalytics$LogRequest;

    .line 1968
    return-void
.end method


# virtual methods
.method public clear()Lcom/google/android/play/analytics/ClientAnalytics$LogRequest;
    .locals 3

    .prologue
    const/4 v2, -0x1

    .line 1971
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/google/android/play/analytics/ClientAnalytics$LogRequest;->requestTimeMs:J

    .line 1972
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/play/analytics/ClientAnalytics$LogRequest;->clientInfo:Lcom/google/android/play/analytics/ClientAnalytics$ClientInfo;

    .line 1973
    iput v2, p0, Lcom/google/android/play/analytics/ClientAnalytics$LogRequest;->logSource:I

    .line 1974
    const-string v0, ""

    iput-object v0, p0, Lcom/google/android/play/analytics/ClientAnalytics$LogRequest;->logSourceName:Ljava/lang/String;

    .line 1975
    const-string v0, ""

    iput-object v0, p0, Lcom/google/android/play/analytics/ClientAnalytics$LogRequest;->zwiebackCookie:Ljava/lang/String;

    .line 1976
    invoke-static {}, Lcom/google/android/play/analytics/ClientAnalytics$LogEvent;->emptyArray()[Lcom/google/android/play/analytics/ClientAnalytics$LogEvent;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/play/analytics/ClientAnalytics$LogRequest;->logEvent:[Lcom/google/android/play/analytics/ClientAnalytics$LogEvent;

    .line 1977
    sget-object v0, Lcom/google/protobuf/nano/WireFormatNano;->EMPTY_BYTES_ARRAY:[[B

    iput-object v0, p0, Lcom/google/android/play/analytics/ClientAnalytics$LogRequest;->serializedLogEvents:[[B

    .line 1978
    iput v2, p0, Lcom/google/android/play/analytics/ClientAnalytics$LogRequest;->cachedSize:I

    .line 1979
    return-object p0
.end method

.method protected computeSerializedSize()I
    .locals 10

    .prologue
    .line 2021
    invoke-super {p0}, Lcom/google/protobuf/nano/MessageNano;->computeSerializedSize()I

    move-result v4

    .line 2022
    .local v4, "size":I
    iget-object v5, p0, Lcom/google/android/play/analytics/ClientAnalytics$LogRequest;->clientInfo:Lcom/google/android/play/analytics/ClientAnalytics$ClientInfo;

    if-eqz v5, :cond_0

    .line 2023
    const/4 v5, 0x1

    iget-object v6, p0, Lcom/google/android/play/analytics/ClientAnalytics$LogRequest;->clientInfo:Lcom/google/android/play/analytics/ClientAnalytics$ClientInfo;

    invoke-static {v5, v6}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeMessageSize(ILcom/google/protobuf/nano/MessageNano;)I

    move-result v5

    add-int/2addr v4, v5

    .line 2026
    :cond_0
    iget v5, p0, Lcom/google/android/play/analytics/ClientAnalytics$LogRequest;->logSource:I

    const/4 v6, -0x1

    if-eq v5, v6, :cond_1

    .line 2027
    const/4 v5, 0x2

    iget v6, p0, Lcom/google/android/play/analytics/ClientAnalytics$LogRequest;->logSource:I

    invoke-static {v5, v6}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeInt32Size(II)I

    move-result v5

    add-int/2addr v4, v5

    .line 2030
    :cond_1
    iget-object v5, p0, Lcom/google/android/play/analytics/ClientAnalytics$LogRequest;->logEvent:[Lcom/google/android/play/analytics/ClientAnalytics$LogEvent;

    if-eqz v5, :cond_3

    iget-object v5, p0, Lcom/google/android/play/analytics/ClientAnalytics$LogRequest;->logEvent:[Lcom/google/android/play/analytics/ClientAnalytics$LogEvent;

    array-length v5, v5

    if-lez v5, :cond_3

    .line 2031
    const/4 v3, 0x0

    .local v3, "i":I
    :goto_0
    iget-object v5, p0, Lcom/google/android/play/analytics/ClientAnalytics$LogRequest;->logEvent:[Lcom/google/android/play/analytics/ClientAnalytics$LogEvent;

    array-length v5, v5

    if-ge v3, v5, :cond_3

    .line 2032
    iget-object v5, p0, Lcom/google/android/play/analytics/ClientAnalytics$LogRequest;->logEvent:[Lcom/google/android/play/analytics/ClientAnalytics$LogEvent;

    aget-object v2, v5, v3

    .line 2033
    .local v2, "element":Lcom/google/android/play/analytics/ClientAnalytics$LogEvent;
    if-eqz v2, :cond_2

    .line 2034
    const/4 v5, 0x3

    invoke-static {v5, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeMessageSize(ILcom/google/protobuf/nano/MessageNano;)I

    move-result v5

    add-int/2addr v4, v5

    .line 2031
    :cond_2
    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    .line 2039
    .end local v2    # "element":Lcom/google/android/play/analytics/ClientAnalytics$LogEvent;
    .end local v3    # "i":I
    :cond_3
    iget-wide v6, p0, Lcom/google/android/play/analytics/ClientAnalytics$LogRequest;->requestTimeMs:J

    const-wide/16 v8, 0x0

    cmp-long v5, v6, v8

    if-eqz v5, :cond_4

    .line 2040
    const/4 v5, 0x4

    iget-wide v6, p0, Lcom/google/android/play/analytics/ClientAnalytics$LogRequest;->requestTimeMs:J

    invoke-static {v5, v6, v7}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeInt64Size(IJ)I

    move-result v5

    add-int/2addr v4, v5

    .line 2043
    :cond_4
    iget-object v5, p0, Lcom/google/android/play/analytics/ClientAnalytics$LogRequest;->serializedLogEvents:[[B

    if-eqz v5, :cond_7

    iget-object v5, p0, Lcom/google/android/play/analytics/ClientAnalytics$LogRequest;->serializedLogEvents:[[B

    array-length v5, v5

    if-lez v5, :cond_7

    .line 2044
    const/4 v0, 0x0

    .line 2045
    .local v0, "dataCount":I
    const/4 v1, 0x0

    .line 2046
    .local v1, "dataSize":I
    const/4 v3, 0x0

    .restart local v3    # "i":I
    :goto_1
    iget-object v5, p0, Lcom/google/android/play/analytics/ClientAnalytics$LogRequest;->serializedLogEvents:[[B

    array-length v5, v5

    if-ge v3, v5, :cond_6

    .line 2047
    iget-object v5, p0, Lcom/google/android/play/analytics/ClientAnalytics$LogRequest;->serializedLogEvents:[[B

    aget-object v2, v5, v3

    .line 2048
    .local v2, "element":[B
    if-eqz v2, :cond_5

    .line 2049
    add-int/lit8 v0, v0, 0x1

    .line 2050
    invoke-static {v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeBytesSizeNoTag([B)I

    move-result v5

    add-int/2addr v1, v5

    .line 2046
    :cond_5
    add-int/lit8 v3, v3, 0x1

    goto :goto_1

    .line 2054
    .end local v2    # "element":[B
    :cond_6
    add-int/2addr v4, v1

    .line 2055
    mul-int/lit8 v5, v0, 0x1

    add-int/2addr v4, v5

    .line 2057
    .end local v0    # "dataCount":I
    .end local v1    # "dataSize":I
    .end local v3    # "i":I
    :cond_7
    iget-object v5, p0, Lcom/google/android/play/analytics/ClientAnalytics$LogRequest;->logSourceName:Ljava/lang/String;

    const-string v6, ""

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-nez v5, :cond_8

    .line 2058
    const/4 v5, 0x6

    iget-object v6, p0, Lcom/google/android/play/analytics/ClientAnalytics$LogRequest;->logSourceName:Ljava/lang/String;

    invoke-static {v5, v6}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeStringSize(ILjava/lang/String;)I

    move-result v5

    add-int/2addr v4, v5

    .line 2061
    :cond_8
    iget-object v5, p0, Lcom/google/android/play/analytics/ClientAnalytics$LogRequest;->zwiebackCookie:Ljava/lang/String;

    const-string v6, ""

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-nez v5, :cond_9

    .line 2062
    const/4 v5, 0x7

    iget-object v6, p0, Lcom/google/android/play/analytics/ClientAnalytics$LogRequest;->zwiebackCookie:Ljava/lang/String;

    invoke-static {v5, v6}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeStringSize(ILjava/lang/String;)I

    move-result v5

    add-int/2addr v4, v5

    .line 2065
    :cond_9
    return v4
.end method

.method public mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/android/play/analytics/ClientAnalytics$LogRequest;
    .locals 8
    .param p1, "input"    # Lcom/google/protobuf/nano/CodedInputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v5, 0x0

    .line 2073
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readTag()I

    move-result v3

    .line 2074
    .local v3, "tag":I
    sparse-switch v3, :sswitch_data_0

    .line 2078
    invoke-static {p1, v3}, Lcom/google/protobuf/nano/WireFormatNano;->parseUnknownField(Lcom/google/protobuf/nano/CodedInputByteBufferNano;I)Z

    move-result v6

    if-nez v6, :cond_0

    .line 2079
    :sswitch_0
    return-object p0

    .line 2084
    :sswitch_1
    iget-object v6, p0, Lcom/google/android/play/analytics/ClientAnalytics$LogRequest;->clientInfo:Lcom/google/android/play/analytics/ClientAnalytics$ClientInfo;

    if-nez v6, :cond_1

    .line 2085
    new-instance v6, Lcom/google/android/play/analytics/ClientAnalytics$ClientInfo;

    invoke-direct {v6}, Lcom/google/android/play/analytics/ClientAnalytics$ClientInfo;-><init>()V

    iput-object v6, p0, Lcom/google/android/play/analytics/ClientAnalytics$LogRequest;->clientInfo:Lcom/google/android/play/analytics/ClientAnalytics$ClientInfo;

    .line 2087
    :cond_1
    iget-object v6, p0, Lcom/google/android/play/analytics/ClientAnalytics$LogRequest;->clientInfo:Lcom/google/android/play/analytics/ClientAnalytics$ClientInfo;

    invoke-virtual {p1, v6}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readMessage(Lcom/google/protobuf/nano/MessageNano;)V

    goto :goto_0

    .line 2091
    :sswitch_2
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readInt32()I

    move-result v4

    .line 2092
    .local v4, "value":I
    packed-switch v4, :pswitch_data_0

    goto :goto_0

    .line 2147
    :pswitch_0
    iput v4, p0, Lcom/google/android/play/analytics/ClientAnalytics$LogRequest;->logSource:I

    goto :goto_0

    .line 2153
    .end local v4    # "value":I
    :sswitch_3
    const/16 v6, 0x1a

    invoke-static {p1, v6}, Lcom/google/protobuf/nano/WireFormatNano;->getRepeatedFieldArrayLength(Lcom/google/protobuf/nano/CodedInputByteBufferNano;I)I

    move-result v0

    .line 2155
    .local v0, "arrayLength":I
    iget-object v6, p0, Lcom/google/android/play/analytics/ClientAnalytics$LogRequest;->logEvent:[Lcom/google/android/play/analytics/ClientAnalytics$LogEvent;

    if-nez v6, :cond_3

    move v1, v5

    .line 2156
    .local v1, "i":I
    :goto_1
    add-int v6, v1, v0

    new-array v2, v6, [Lcom/google/android/play/analytics/ClientAnalytics$LogEvent;

    .line 2158
    .local v2, "newArray":[Lcom/google/android/play/analytics/ClientAnalytics$LogEvent;
    if-eqz v1, :cond_2

    .line 2159
    iget-object v6, p0, Lcom/google/android/play/analytics/ClientAnalytics$LogRequest;->logEvent:[Lcom/google/android/play/analytics/ClientAnalytics$LogEvent;

    invoke-static {v6, v5, v2, v5, v1}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 2161
    :cond_2
    :goto_2
    array-length v6, v2

    add-int/lit8 v6, v6, -0x1

    if-ge v1, v6, :cond_4

    .line 2162
    new-instance v6, Lcom/google/android/play/analytics/ClientAnalytics$LogEvent;

    invoke-direct {v6}, Lcom/google/android/play/analytics/ClientAnalytics$LogEvent;-><init>()V

    aput-object v6, v2, v1

    .line 2163
    aget-object v6, v2, v1

    invoke-virtual {p1, v6}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readMessage(Lcom/google/protobuf/nano/MessageNano;)V

    .line 2164
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readTag()I

    .line 2161
    add-int/lit8 v1, v1, 0x1

    goto :goto_2

    .line 2155
    .end local v1    # "i":I
    .end local v2    # "newArray":[Lcom/google/android/play/analytics/ClientAnalytics$LogEvent;
    :cond_3
    iget-object v6, p0, Lcom/google/android/play/analytics/ClientAnalytics$LogRequest;->logEvent:[Lcom/google/android/play/analytics/ClientAnalytics$LogEvent;

    array-length v1, v6

    goto :goto_1

    .line 2167
    .restart local v1    # "i":I
    .restart local v2    # "newArray":[Lcom/google/android/play/analytics/ClientAnalytics$LogEvent;
    :cond_4
    new-instance v6, Lcom/google/android/play/analytics/ClientAnalytics$LogEvent;

    invoke-direct {v6}, Lcom/google/android/play/analytics/ClientAnalytics$LogEvent;-><init>()V

    aput-object v6, v2, v1

    .line 2168
    aget-object v6, v2, v1

    invoke-virtual {p1, v6}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readMessage(Lcom/google/protobuf/nano/MessageNano;)V

    .line 2169
    iput-object v2, p0, Lcom/google/android/play/analytics/ClientAnalytics$LogRequest;->logEvent:[Lcom/google/android/play/analytics/ClientAnalytics$LogEvent;

    goto :goto_0

    .line 2173
    .end local v0    # "arrayLength":I
    .end local v1    # "i":I
    .end local v2    # "newArray":[Lcom/google/android/play/analytics/ClientAnalytics$LogEvent;
    :sswitch_4
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readInt64()J

    move-result-wide v6

    iput-wide v6, p0, Lcom/google/android/play/analytics/ClientAnalytics$LogRequest;->requestTimeMs:J

    goto :goto_0

    .line 2177
    :sswitch_5
    const/16 v6, 0x2a

    invoke-static {p1, v6}, Lcom/google/protobuf/nano/WireFormatNano;->getRepeatedFieldArrayLength(Lcom/google/protobuf/nano/CodedInputByteBufferNano;I)I

    move-result v0

    .line 2179
    .restart local v0    # "arrayLength":I
    iget-object v6, p0, Lcom/google/android/play/analytics/ClientAnalytics$LogRequest;->serializedLogEvents:[[B

    if-nez v6, :cond_6

    move v1, v5

    .line 2180
    .restart local v1    # "i":I
    :goto_3
    add-int v6, v1, v0

    new-array v2, v6, [[B

    .line 2181
    .local v2, "newArray":[[B
    if-eqz v1, :cond_5

    .line 2182
    iget-object v6, p0, Lcom/google/android/play/analytics/ClientAnalytics$LogRequest;->serializedLogEvents:[[B

    invoke-static {v6, v5, v2, v5, v1}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 2184
    :cond_5
    :goto_4
    array-length v6, v2

    add-int/lit8 v6, v6, -0x1

    if-ge v1, v6, :cond_7

    .line 2185
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readBytes()[B

    move-result-object v6

    aput-object v6, v2, v1

    .line 2186
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readTag()I

    .line 2184
    add-int/lit8 v1, v1, 0x1

    goto :goto_4

    .line 2179
    .end local v1    # "i":I
    .end local v2    # "newArray":[[B
    :cond_6
    iget-object v6, p0, Lcom/google/android/play/analytics/ClientAnalytics$LogRequest;->serializedLogEvents:[[B

    array-length v1, v6

    goto :goto_3

    .line 2189
    .restart local v1    # "i":I
    .restart local v2    # "newArray":[[B
    :cond_7
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readBytes()[B

    move-result-object v6

    aput-object v6, v2, v1

    .line 2190
    iput-object v2, p0, Lcom/google/android/play/analytics/ClientAnalytics$LogRequest;->serializedLogEvents:[[B

    goto/16 :goto_0

    .line 2194
    .end local v0    # "arrayLength":I
    .end local v1    # "i":I
    .end local v2    # "newArray":[[B
    :sswitch_6
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readString()Ljava/lang/String;

    move-result-object v6

    iput-object v6, p0, Lcom/google/android/play/analytics/ClientAnalytics$LogRequest;->logSourceName:Ljava/lang/String;

    goto/16 :goto_0

    .line 2198
    :sswitch_7
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readString()Ljava/lang/String;

    move-result-object v6

    iput-object v6, p0, Lcom/google/android/play/analytics/ClientAnalytics$LogRequest;->zwiebackCookie:Ljava/lang/String;

    goto/16 :goto_0

    .line 2074
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x10 -> :sswitch_2
        0x1a -> :sswitch_3
        0x20 -> :sswitch_4
        0x2a -> :sswitch_5
        0x32 -> :sswitch_6
        0x3a -> :sswitch_7
    .end sparse-switch

    .line 2092
    :pswitch_data_0
    .packed-switch -0x1
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
    .end packed-switch
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/protobuf/nano/MessageNano;
    .locals 1
    .param p1, "x0"    # Lcom/google/protobuf/nano/CodedInputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 1872
    invoke-virtual {p0, p1}, Lcom/google/android/play/analytics/ClientAnalytics$LogRequest;->mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/android/play/analytics/ClientAnalytics$LogRequest;

    move-result-object v0

    return-object v0
.end method

.method public writeTo(Lcom/google/protobuf/nano/CodedOutputByteBufferNano;)V
    .locals 6
    .param p1, "output"    # Lcom/google/protobuf/nano/CodedOutputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 1985
    iget-object v2, p0, Lcom/google/android/play/analytics/ClientAnalytics$LogRequest;->clientInfo:Lcom/google/android/play/analytics/ClientAnalytics$ClientInfo;

    if-eqz v2, :cond_0

    .line 1986
    const/4 v2, 0x1

    iget-object v3, p0, Lcom/google/android/play/analytics/ClientAnalytics$LogRequest;->clientInfo:Lcom/google/android/play/analytics/ClientAnalytics$ClientInfo;

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeMessage(ILcom/google/protobuf/nano/MessageNano;)V

    .line 1988
    :cond_0
    iget v2, p0, Lcom/google/android/play/analytics/ClientAnalytics$LogRequest;->logSource:I

    const/4 v3, -0x1

    if-eq v2, v3, :cond_1

    .line 1989
    const/4 v2, 0x2

    iget v3, p0, Lcom/google/android/play/analytics/ClientAnalytics$LogRequest;->logSource:I

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeInt32(II)V

    .line 1991
    :cond_1
    iget-object v2, p0, Lcom/google/android/play/analytics/ClientAnalytics$LogRequest;->logEvent:[Lcom/google/android/play/analytics/ClientAnalytics$LogEvent;

    if-eqz v2, :cond_3

    iget-object v2, p0, Lcom/google/android/play/analytics/ClientAnalytics$LogRequest;->logEvent:[Lcom/google/android/play/analytics/ClientAnalytics$LogEvent;

    array-length v2, v2

    if-lez v2, :cond_3

    .line 1992
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    iget-object v2, p0, Lcom/google/android/play/analytics/ClientAnalytics$LogRequest;->logEvent:[Lcom/google/android/play/analytics/ClientAnalytics$LogEvent;

    array-length v2, v2

    if-ge v1, v2, :cond_3

    .line 1993
    iget-object v2, p0, Lcom/google/android/play/analytics/ClientAnalytics$LogRequest;->logEvent:[Lcom/google/android/play/analytics/ClientAnalytics$LogEvent;

    aget-object v0, v2, v1

    .line 1994
    .local v0, "element":Lcom/google/android/play/analytics/ClientAnalytics$LogEvent;
    if-eqz v0, :cond_2

    .line 1995
    const/4 v2, 0x3

    invoke-virtual {p1, v2, v0}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeMessage(ILcom/google/protobuf/nano/MessageNano;)V

    .line 1992
    :cond_2
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 1999
    .end local v0    # "element":Lcom/google/android/play/analytics/ClientAnalytics$LogEvent;
    .end local v1    # "i":I
    :cond_3
    iget-wide v2, p0, Lcom/google/android/play/analytics/ClientAnalytics$LogRequest;->requestTimeMs:J

    const-wide/16 v4, 0x0

    cmp-long v2, v2, v4

    if-eqz v2, :cond_4

    .line 2000
    const/4 v2, 0x4

    iget-wide v4, p0, Lcom/google/android/play/analytics/ClientAnalytics$LogRequest;->requestTimeMs:J

    invoke-virtual {p1, v2, v4, v5}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeInt64(IJ)V

    .line 2002
    :cond_4
    iget-object v2, p0, Lcom/google/android/play/analytics/ClientAnalytics$LogRequest;->serializedLogEvents:[[B

    if-eqz v2, :cond_6

    iget-object v2, p0, Lcom/google/android/play/analytics/ClientAnalytics$LogRequest;->serializedLogEvents:[[B

    array-length v2, v2

    if-lez v2, :cond_6

    .line 2003
    const/4 v1, 0x0

    .restart local v1    # "i":I
    :goto_1
    iget-object v2, p0, Lcom/google/android/play/analytics/ClientAnalytics$LogRequest;->serializedLogEvents:[[B

    array-length v2, v2

    if-ge v1, v2, :cond_6

    .line 2004
    iget-object v2, p0, Lcom/google/android/play/analytics/ClientAnalytics$LogRequest;->serializedLogEvents:[[B

    aget-object v0, v2, v1

    .line 2005
    .local v0, "element":[B
    if-eqz v0, :cond_5

    .line 2006
    const/4 v2, 0x5

    invoke-virtual {p1, v2, v0}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeBytes(I[B)V

    .line 2003
    :cond_5
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 2010
    .end local v0    # "element":[B
    .end local v1    # "i":I
    :cond_6
    iget-object v2, p0, Lcom/google/android/play/analytics/ClientAnalytics$LogRequest;->logSourceName:Ljava/lang/String;

    const-string v3, ""

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_7

    .line 2011
    const/4 v2, 0x6

    iget-object v3, p0, Lcom/google/android/play/analytics/ClientAnalytics$LogRequest;->logSourceName:Ljava/lang/String;

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeString(ILjava/lang/String;)V

    .line 2013
    :cond_7
    iget-object v2, p0, Lcom/google/android/play/analytics/ClientAnalytics$LogRequest;->zwiebackCookie:Ljava/lang/String;

    const-string v3, ""

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_8

    .line 2014
    const/4 v2, 0x7

    iget-object v3, p0, Lcom/google/android/play/analytics/ClientAnalytics$LogRequest;->zwiebackCookie:Ljava/lang/String;

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeString(ILjava/lang/String;)V

    .line 2016
    :cond_8
    invoke-super {p0, p1}, Lcom/google/protobuf/nano/MessageNano;->writeTo(Lcom/google/protobuf/nano/CodedOutputByteBufferNano;)V

    .line 2017
    return-void
.end method
