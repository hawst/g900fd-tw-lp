.class public Lcom/google/android/play/utils/UrlSpanUtils;
.super Ljava/lang/Object;
.source "UrlSpanUtils.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/play/utils/UrlSpanUtils$SelfishUrlSpan;,
        Lcom/google/android/play/utils/UrlSpanUtils$Listener;
    }
.end annotation


# direct methods
.method public static selfishifyUrlSpans(Ljava/lang/CharSequence;Lcom/google/android/play/utils/UrlSpanUtils$Listener;)V
    .locals 1
    .param p0, "string"    # Ljava/lang/CharSequence;
    .param p1, "listener"    # Lcom/google/android/play/utils/UrlSpanUtils$Listener;

    .prologue
    .line 42
    const/4 v0, 0x0

    invoke-static {p0, v0, p1}, Lcom/google/android/play/utils/UrlSpanUtils;->selfishifyUrlSpans(Ljava/lang/CharSequence;Ljava/lang/CharSequence;Lcom/google/android/play/utils/UrlSpanUtils$Listener;)V

    .line 43
    return-void
.end method

.method public static selfishifyUrlSpans(Ljava/lang/CharSequence;Ljava/lang/CharSequence;Lcom/google/android/play/utils/UrlSpanUtils$Listener;)V
    .locals 12
    .param p0, "string"    # Ljava/lang/CharSequence;
    .param p1, "targetUrl"    # Ljava/lang/CharSequence;
    .param p2, "listener"    # Lcom/google/android/play/utils/UrlSpanUtils$Listener;

    .prologue
    const/4 v11, 0x0

    .line 61
    if-nez p2, :cond_0

    .line 62
    new-instance v9, Ljava/lang/IllegalStateException;

    const-string v10, "listener should not be null"

    invoke-direct {v9, v10}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v9

    .line 64
    :cond_0
    instance-of v9, p0, Landroid/text/Spannable;

    if-nez v9, :cond_2

    .line 78
    :cond_1
    return-void

    :cond_2
    move-object v5, p0

    .line 67
    check-cast v5, Landroid/text/Spannable;

    .line 68
    .local v5, "spannable":Landroid/text/Spannable;
    invoke-interface {v5}, Landroid/text/Spannable;->length()I

    move-result v9

    const-class v10, Landroid/text/style/URLSpan;

    invoke-interface {v5, v11, v9, v10}, Landroid/text/Spannable;->getSpans(IILjava/lang/Class;)[Ljava/lang/Object;

    move-result-object v6

    check-cast v6, [Landroid/text/style/URLSpan;

    .line 69
    .local v6, "spans":[Landroid/text/style/URLSpan;
    move-object v0, v6

    .local v0, "arr$":[Landroid/text/style/URLSpan;
    array-length v3, v0

    .local v3, "len$":I
    const/4 v2, 0x0

    .local v2, "i$":I
    :goto_0
    if-ge v2, v3, :cond_1

    aget-object v4, v0, v2

    .line 70
    .local v4, "span":Landroid/text/style/URLSpan;
    invoke-virtual {v4}, Landroid/text/style/URLSpan;->getURL()Ljava/lang/String;

    move-result-object v8

    .line 71
    .local v8, "url":Ljava/lang/String;
    if-eqz p1, :cond_3

    invoke-virtual {p1, v8}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_4

    .line 72
    :cond_3
    invoke-interface {v5, v4}, Landroid/text/Spannable;->getSpanStart(Ljava/lang/Object;)I

    move-result v7

    .line 73
    .local v7, "start":I
    invoke-interface {v5, v4}, Landroid/text/Spannable;->getSpanEnd(Ljava/lang/Object;)I

    move-result v1

    .line 74
    .local v1, "end":I
    invoke-interface {v5, v4}, Landroid/text/Spannable;->removeSpan(Ljava/lang/Object;)V

    .line 75
    new-instance v9, Lcom/google/android/play/utils/UrlSpanUtils$SelfishUrlSpan;

    invoke-direct {v9, v8, p2}, Lcom/google/android/play/utils/UrlSpanUtils$SelfishUrlSpan;-><init>(Ljava/lang/String;Lcom/google/android/play/utils/UrlSpanUtils$Listener;)V

    invoke-interface {v5, v9, v7, v1, v11}, Landroid/text/Spannable;->setSpan(Ljava/lang/Object;III)V

    .line 69
    .end local v1    # "end":I
    .end local v7    # "start":I
    :cond_4
    add-int/lit8 v2, v2, 0x1

    goto :goto_0
.end method
