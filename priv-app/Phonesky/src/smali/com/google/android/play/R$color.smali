.class public final Lcom/google/android/play/R$color;
.super Ljava/lang/Object;
.source "R.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/play/R;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "color"
.end annotation


# static fields
.field public static final play_action_button_text:I = 0x7f0900ec

.field public static final play_apps_primary_text:I = 0x7f0900ed

.field public static final play_avatar_focused_outline:I = 0x7f090081

.field public static final play_avatar_outline:I = 0x7f09007e

.field public static final play_avatar_pressed_fill:I = 0x7f09007f

.field public static final play_avatar_pressed_outline:I = 0x7f090080

.field public static final play_books_primary_text:I = 0x7f0900ef

.field public static final play_card_shadow_end_color:I = 0x7f09008b

.field public static final play_card_shadow_start_color:I = 0x7f09008a

.field public static final play_disabled_grey:I = 0x7f090050

.field public static final play_dismissed_overlay:I = 0x7f090078

.field public static final play_fg_primary:I = 0x7f09005a

.field public static final play_fg_secondary:I = 0x7f09005b

.field public static final play_header_list_tab_text_color:I = 0x7f0900f1

.field public static final play_header_list_tab_underline_color:I = 0x7f090083

.field public static final play_main_background:I = 0x7f090054

.field public static final play_movies_primary_text:I = 0x7f0900f2

.field public static final play_multi_primary_text:I = 0x7f0900f4

.field public static final play_music_primary_text:I = 0x7f0900f6

.field public static final play_newsstand_primary_text:I = 0x7f0900f8

.field public static final play_onboard_accent_color_a:I = 0x7f090097

.field public static final play_onboard_accent_color_b:I = 0x7f090098

.field public static final play_onboard_accent_color_c:I = 0x7f090099

.field public static final play_onboard_accent_color_d:I = 0x7f09009a

.field public static final play_onboard_app_color_dark:I = 0x7f090090

.field public static final play_reason_separator:I = 0x7f090055

.field public static final play_search_plate_navigation_button_color:I = 0x7f09008d

.field public static final play_tab_strip_text_selected:I = 0x7f090059

.field public static final play_transparent:I = 0x7f090051

.field public static final play_white:I = 0x7f09004c
