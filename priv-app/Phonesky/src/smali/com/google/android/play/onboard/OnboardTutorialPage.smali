.class public Lcom/google/android/play/onboard/OnboardTutorialPage;
.super Lcom/google/android/libraries/bind/widget/BindingFrameLayout;
.source "OnboardTutorialPage.java"


# static fields
.field public static final DK_BACKGROUND_COLOR:I

.field public static final DK_BODY_TEXT:I

.field public static final DK_ICON_DRAWABLE_ID:I

.field public static final DK_TITLE_TEXT:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 22
    sget v0, Lcom/google/android/play/R$id;->play_onboard__OnboardTutorialPage_backgroundColor:I

    sput v0, Lcom/google/android/play/onboard/OnboardTutorialPage;->DK_BACKGROUND_COLOR:I

    .line 27
    sget v0, Lcom/google/android/play/R$id;->play_onboard__OnboardTutorialPage_titleText:I

    sput v0, Lcom/google/android/play/onboard/OnboardTutorialPage;->DK_TITLE_TEXT:I

    .line 31
    sget v0, Lcom/google/android/play/R$id;->play_onboard__OnboardTutorialPage_bodyText:I

    sput v0, Lcom/google/android/play/onboard/OnboardTutorialPage;->DK_BODY_TEXT:I

    .line 35
    sget v0, Lcom/google/android/play/R$id;->play_onboard__OnboardTutorialPage_iconDrawableId:I

    sput v0, Lcom/google/android/play/onboard/OnboardTutorialPage;->DK_ICON_DRAWABLE_ID:I

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 41
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/google/android/play/onboard/OnboardTutorialPage;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 42
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    .line 45
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/google/android/play/onboard/OnboardTutorialPage;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 46
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 0
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;
    .param p3, "defStyle"    # I

    .prologue
    .line 49
    invoke-direct {p0, p1, p2, p3}, Lcom/google/android/libraries/bind/widget/BindingFrameLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 50
    return-void
.end method
