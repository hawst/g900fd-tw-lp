.class Lcom/google/android/play/headerlist/PlayHeaderListRecyclerViewListener$1;
.super Landroid/support/v7/widget/RecyclerView$AdapterDataObserver;
.source "PlayHeaderListRecyclerViewListener.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/play/headerlist/PlayHeaderListRecyclerViewListener;-><init>(Lcom/google/android/play/headerlist/PlayHeaderListLayout;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/play/headerlist/PlayHeaderListRecyclerViewListener;


# direct methods
.method constructor <init>(Lcom/google/android/play/headerlist/PlayHeaderListRecyclerViewListener;)V
    .locals 0

    .prologue
    .line 28
    iput-object p1, p0, Lcom/google/android/play/headerlist/PlayHeaderListRecyclerViewListener$1;->this$0:Lcom/google/android/play/headerlist/PlayHeaderListRecyclerViewListener;

    invoke-direct {p0}, Landroid/support/v7/widget/RecyclerView$AdapterDataObserver;-><init>()V

    return-void
.end method


# virtual methods
.method public onChanged()V
    .locals 2

    .prologue
    .line 31
    iget-object v0, p0, Lcom/google/android/play/headerlist/PlayHeaderListRecyclerViewListener$1;->this$0:Lcom/google/android/play/headerlist/PlayHeaderListRecyclerViewListener;

    const/4 v1, 0x0

    # invokes: Lcom/google/android/play/headerlist/PlayHeaderListRecyclerViewListener;->reset(Z)V
    invoke-static {v0, v1}, Lcom/google/android/play/headerlist/PlayHeaderListRecyclerViewListener;->access$000(Lcom/google/android/play/headerlist/PlayHeaderListRecyclerViewListener;Z)V

    .line 37
    iget-object v0, p0, Lcom/google/android/play/headerlist/PlayHeaderListRecyclerViewListener$1;->this$0:Lcom/google/android/play/headerlist/PlayHeaderListRecyclerViewListener;

    # getter for: Lcom/google/android/play/headerlist/PlayHeaderListRecyclerViewListener;->mLayout:Lcom/google/android/play/headerlist/PlayHeaderListLayout;
    invoke-static {v0}, Lcom/google/android/play/headerlist/PlayHeaderListRecyclerViewListener;->access$100(Lcom/google/android/play/headerlist/PlayHeaderListRecyclerViewListener;)Lcom/google/android/play/headerlist/PlayHeaderListLayout;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->syncCurrentListViewOnNextScroll()V

    .line 38
    return-void
.end method

.method public onItemRangeRemoved(II)V
    .locals 0
    .param p1, "positionStart"    # I
    .param p2, "itemCount"    # I

    .prologue
    .line 57
    invoke-virtual {p0}, Lcom/google/android/play/headerlist/PlayHeaderListRecyclerViewListener$1;->onChanged()V

    .line 58
    return-void
.end method
