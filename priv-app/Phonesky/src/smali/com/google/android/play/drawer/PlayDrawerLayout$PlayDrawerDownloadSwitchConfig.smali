.class public Lcom/google/android/play/drawer/PlayDrawerLayout$PlayDrawerDownloadSwitchConfig;
.super Ljava/lang/Object;
.source "PlayDrawerLayout.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/play/drawer/PlayDrawerLayout;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "PlayDrawerDownloadSwitchConfig"
.end annotation


# instance fields
.field public final actionText:Ljava/lang/String;

.field public final checkedTextColor:I

.field public final isChecked:Z

.field public final thumbDrawableId:I

.field public final trackDrawableId:I
