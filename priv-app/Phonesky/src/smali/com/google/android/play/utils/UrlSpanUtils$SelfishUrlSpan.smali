.class Lcom/google/android/play/utils/UrlSpanUtils$SelfishUrlSpan;
.super Landroid/text/style/URLSpan;
.source "UrlSpanUtils.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/play/utils/UrlSpanUtils;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "SelfishUrlSpan"
.end annotation


# instance fields
.field private final listener:Lcom/google/android/play/utils/UrlSpanUtils$Listener;


# direct methods
.method public constructor <init>(Ljava/lang/String;Lcom/google/android/play/utils/UrlSpanUtils$Listener;)V
    .locals 0
    .param p1, "url"    # Ljava/lang/String;
    .param p2, "listener"    # Lcom/google/android/play/utils/UrlSpanUtils$Listener;

    .prologue
    .line 87
    invoke-direct {p0, p1}, Landroid/text/style/URLSpan;-><init>(Ljava/lang/String;)V

    .line 88
    iput-object p2, p0, Lcom/google/android/play/utils/UrlSpanUtils$SelfishUrlSpan;->listener:Lcom/google/android/play/utils/UrlSpanUtils$Listener;

    .line 89
    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 2
    .param p1, "view"    # Landroid/view/View;

    .prologue
    .line 93
    iget-object v0, p0, Lcom/google/android/play/utils/UrlSpanUtils$SelfishUrlSpan;->listener:Lcom/google/android/play/utils/UrlSpanUtils$Listener;

    invoke-virtual {p0}, Lcom/google/android/play/utils/UrlSpanUtils$SelfishUrlSpan;->getURL()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, p1, v1}, Lcom/google/android/play/utils/UrlSpanUtils$Listener;->onClick(Landroid/view/View;Ljava/lang/String;)V

    .line 94
    return-void
.end method
