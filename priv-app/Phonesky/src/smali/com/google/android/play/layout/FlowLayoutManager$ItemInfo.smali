.class final Lcom/google/android/play/layout/FlowLayoutManager$ItemInfo;
.super Ljava/lang/Object;
.source "FlowLayoutManager.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/play/layout/FlowLayoutManager;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1a
    name = "ItemInfo"
.end annotation


# static fields
.field private static final sPool:Landroid/support/v4/util/Pools$Pool;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/support/v4/util/Pools$Pool",
            "<",
            "Lcom/google/android/play/layout/FlowLayoutManager$ItemInfo;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public mBaseline:I

.field public mDecoratedHeight:I

.field public mDecoratedWidth:I

.field public mGridCellSize:F

.field public mGridInsetEnd:I

.field public mGridInsetStart:I

.field public mMarginBottom:I

.field public mMarginBottomForLastLine:I

.field public mMarginEnd:I

.field public mMarginStart:I

.field public mMarginTop:I

.field public mMarginTopForFirstLine:I

.field public mMeasuredInCurrentPass:Z

.field public mTopOffset:I

.field public mVAlign:I


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 857
    new-instance v0, Landroid/support/v4/util/Pools$SimplePool;

    const/16 v1, 0x1e

    invoke-direct {v0, v1}, Landroid/support/v4/util/Pools$SimplePool;-><init>(I)V

    sput-object v0, Lcom/google/android/play/layout/FlowLayoutManager$ItemInfo;->sPool:Landroid/support/v4/util/Pools$Pool;

    return-void
.end method

.method private constructor <init>()V
    .locals 0

    .prologue
    .line 901
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 902
    invoke-direct {p0}, Lcom/google/android/play/layout/FlowLayoutManager$ItemInfo;->reset()V

    .line 903
    return-void
.end method

.method public static obtain()Lcom/google/android/play/layout/FlowLayoutManager$ItemInfo;
    .locals 2

    .prologue
    .line 860
    sget-object v1, Lcom/google/android/play/layout/FlowLayoutManager$ItemInfo;->sPool:Landroid/support/v4/util/Pools$Pool;

    invoke-interface {v1}, Landroid/support/v4/util/Pools$Pool;->acquire()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/play/layout/FlowLayoutManager$ItemInfo;

    .line 861
    .local v0, "item":Lcom/google/android/play/layout/FlowLayoutManager$ItemInfo;
    if-eqz v0, :cond_0

    .end local v0    # "item":Lcom/google/android/play/layout/FlowLayoutManager$ItemInfo;
    :goto_0
    return-object v0

    .restart local v0    # "item":Lcom/google/android/play/layout/FlowLayoutManager$ItemInfo;
    :cond_0
    new-instance v0, Lcom/google/android/play/layout/FlowLayoutManager$ItemInfo;

    .end local v0    # "item":Lcom/google/android/play/layout/FlowLayoutManager$ItemInfo;
    invoke-direct {v0}, Lcom/google/android/play/layout/FlowLayoutManager$ItemInfo;-><init>()V

    goto :goto_0
.end method

.method private reset()V
    .locals 2

    .prologue
    const/4 v0, -0x1

    const/4 v1, 0x0

    .line 966
    iput v0, p0, Lcom/google/android/play/layout/FlowLayoutManager$ItemInfo;->mDecoratedWidth:I

    .line 967
    iput v0, p0, Lcom/google/android/play/layout/FlowLayoutManager$ItemInfo;->mDecoratedHeight:I

    .line 968
    iput v1, p0, Lcom/google/android/play/layout/FlowLayoutManager$ItemInfo;->mMarginTop:I

    .line 969
    iput v1, p0, Lcom/google/android/play/layout/FlowLayoutManager$ItemInfo;->mMarginTopForFirstLine:I

    .line 970
    iput v1, p0, Lcom/google/android/play/layout/FlowLayoutManager$ItemInfo;->mMarginStart:I

    .line 971
    iput v1, p0, Lcom/google/android/play/layout/FlowLayoutManager$ItemInfo;->mMarginEnd:I

    .line 972
    iput v1, p0, Lcom/google/android/play/layout/FlowLayoutManager$ItemInfo;->mMarginBottom:I

    .line 973
    iput v1, p0, Lcom/google/android/play/layout/FlowLayoutManager$ItemInfo;->mMarginBottomForLastLine:I

    .line 974
    iput v1, p0, Lcom/google/android/play/layout/FlowLayoutManager$ItemInfo;->mBaseline:I

    .line 975
    iput v1, p0, Lcom/google/android/play/layout/FlowLayoutManager$ItemInfo;->mVAlign:I

    .line 976
    iput v1, p0, Lcom/google/android/play/layout/FlowLayoutManager$ItemInfo;->mGridInsetStart:I

    .line 977
    iput v1, p0, Lcom/google/android/play/layout/FlowLayoutManager$ItemInfo;->mGridInsetEnd:I

    .line 978
    const/high16 v0, 0x7fc00000    # NaNf

    iput v0, p0, Lcom/google/android/play/layout/FlowLayoutManager$ItemInfo;->mGridCellSize:F

    .line 979
    iput v1, p0, Lcom/google/android/play/layout/FlowLayoutManager$ItemInfo;->mTopOffset:I

    .line 980
    iput-boolean v1, p0, Lcom/google/android/play/layout/FlowLayoutManager$ItemInfo;->mMeasuredInCurrentPass:Z

    .line 981
    return-void
.end method


# virtual methods
.method public hasSameGridAs(Lcom/google/android/play/layout/FlowLayoutManager$ItemInfo;)Z
    .locals 2
    .param p1, "other"    # Lcom/google/android/play/layout/FlowLayoutManager$ItemInfo;

    .prologue
    .line 921
    iget v0, p0, Lcom/google/android/play/layout/FlowLayoutManager$ItemInfo;->mGridCellSize:F

    iget v1, p1, Lcom/google/android/play/layout/FlowLayoutManager$ItemInfo;->mGridCellSize:F

    invoke-static {v0, v1}, Ljava/lang/Float;->compare(FF)I

    move-result v0

    if-nez v0, :cond_0

    iget v0, p0, Lcom/google/android/play/layout/FlowLayoutManager$ItemInfo;->mGridInsetStart:I

    iget v1, p1, Lcom/google/android/play/layout/FlowLayoutManager$ItemInfo;->mGridInsetStart:I

    if-ne v0, v1, :cond_0

    iget v0, p0, Lcom/google/android/play/layout/FlowLayoutManager$ItemInfo;->mGridInsetEnd:I

    iget v1, p1, Lcom/google/android/play/layout/FlowLayoutManager$ItemInfo;->mGridInsetEnd:I

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public loadMeasurements(Lcom/google/android/play/layout/FlowLayoutManager;Landroid/view/View;Z)V
    .locals 5
    .param p1, "layoutManager"    # Lcom/google/android/play/layout/FlowLayoutManager;
    .param p2, "child"    # Landroid/view/View;
    .param p3, "expectSame"    # Z

    .prologue
    .line 934
    invoke-virtual {p1, p2}, Lcom/google/android/play/layout/FlowLayoutManager;->getDecoratedMeasuredWidth(Landroid/view/View;)I

    move-result v2

    .line 935
    .local v2, "decoratedWidth":I
    invoke-virtual {p1, p2}, Lcom/google/android/play/layout/FlowLayoutManager;->getDecoratedMeasuredHeight(Landroid/view/View;)I

    move-result v1

    .line 936
    .local v1, "decoratedHeight":I
    invoke-virtual {p2}, Landroid/view/View;->getBaseline()I

    move-result v0

    .line 938
    .local v0, "baseline":I
    if-ltz v0, :cond_0

    invoke-virtual {p2}, Landroid/view/View;->getMeasuredHeight()I

    move-result v3

    if-le v0, v3, :cond_3

    .line 939
    :cond_0
    move v0, v1

    .line 943
    :goto_0
    if-eqz p3, :cond_2

    iget v3, p0, Lcom/google/android/play/layout/FlowLayoutManager$ItemInfo;->mDecoratedWidth:I

    if-ne v2, v3, :cond_1

    iget v3, p0, Lcom/google/android/play/layout/FlowLayoutManager$ItemInfo;->mDecoratedHeight:I

    if-ne v1, v3, :cond_1

    iget v3, p0, Lcom/google/android/play/layout/FlowLayoutManager$ItemInfo;->mBaseline:I

    if-eq v0, v3, :cond_2

    .line 945
    :cond_1
    const-string v3, "FlowLayoutManager"

    const-string v4, "Child measurement changed without notifying from the adapter! Some layout may be incorrect."

    invoke-static {v3, v4}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 954
    :cond_2
    iput v2, p0, Lcom/google/android/play/layout/FlowLayoutManager$ItemInfo;->mDecoratedWidth:I

    .line 955
    iput v1, p0, Lcom/google/android/play/layout/FlowLayoutManager$ItemInfo;->mDecoratedHeight:I

    .line 956
    iput v0, p0, Lcom/google/android/play/layout/FlowLayoutManager$ItemInfo;->mBaseline:I

    .line 957
    const/4 v3, 0x1

    iput-boolean v3, p0, Lcom/google/android/play/layout/FlowLayoutManager$ItemInfo;->mMeasuredInCurrentPass:Z

    .line 958
    return-void

    .line 941
    :cond_3
    invoke-virtual {p1, p2}, Lcom/google/android/play/layout/FlowLayoutManager;->getTopDecorationHeight(Landroid/view/View;)I

    move-result v3

    add-int/2addr v0, v3

    goto :goto_0
.end method

.method public loadParams(Lcom/google/android/play/layout/FlowLayoutManager$LayoutParams;I)V
    .locals 1
    .param p1, "params"    # Lcom/google/android/play/layout/FlowLayoutManager$LayoutParams;
    .param p2, "fullContextWidth"    # I

    .prologue
    .line 909
    invoke-virtual {p1, p2, p0}, Lcom/google/android/play/layout/FlowLayoutManager$LayoutParams;->getGridDefinition(ILcom/google/android/play/layout/FlowLayoutManager$ItemInfo;)F

    .line 910
    iget v0, p0, Lcom/google/android/play/layout/FlowLayoutManager$ItemInfo;->mGridCellSize:F

    invoke-virtual {p1, v0}, Lcom/google/android/play/layout/FlowLayoutManager$LayoutParams;->getTopMargin(F)I

    move-result v0

    iput v0, p0, Lcom/google/android/play/layout/FlowLayoutManager$ItemInfo;->mMarginTop:I

    .line 911
    iget v0, p0, Lcom/google/android/play/layout/FlowLayoutManager$ItemInfo;->mGridCellSize:F

    invoke-virtual {p1, v0}, Lcom/google/android/play/layout/FlowLayoutManager$LayoutParams;->getTopMarginForFirstLine(F)I

    move-result v0

    iput v0, p0, Lcom/google/android/play/layout/FlowLayoutManager$ItemInfo;->mMarginTopForFirstLine:I

    .line 912
    iget v0, p0, Lcom/google/android/play/layout/FlowLayoutManager$ItemInfo;->mGridCellSize:F

    invoke-virtual {p1, v0}, Lcom/google/android/play/layout/FlowLayoutManager$LayoutParams;->getStartMargin(F)I

    move-result v0

    iput v0, p0, Lcom/google/android/play/layout/FlowLayoutManager$ItemInfo;->mMarginStart:I

    .line 913
    iget v0, p0, Lcom/google/android/play/layout/FlowLayoutManager$ItemInfo;->mGridCellSize:F

    invoke-virtual {p1, v0}, Lcom/google/android/play/layout/FlowLayoutManager$LayoutParams;->getEndMargin(F)I

    move-result v0

    iput v0, p0, Lcom/google/android/play/layout/FlowLayoutManager$ItemInfo;->mMarginEnd:I

    .line 914
    iget v0, p0, Lcom/google/android/play/layout/FlowLayoutManager$ItemInfo;->mGridCellSize:F

    invoke-virtual {p1, v0}, Lcom/google/android/play/layout/FlowLayoutManager$LayoutParams;->getBottomMargin(F)I

    move-result v0

    iput v0, p0, Lcom/google/android/play/layout/FlowLayoutManager$ItemInfo;->mMarginBottom:I

    .line 915
    iget v0, p0, Lcom/google/android/play/layout/FlowLayoutManager$ItemInfo;->mGridCellSize:F

    invoke-virtual {p1, v0}, Lcom/google/android/play/layout/FlowLayoutManager$LayoutParams;->getBottomMarginForLastLine(F)I

    move-result v0

    iput v0, p0, Lcom/google/android/play/layout/FlowLayoutManager$ItemInfo;->mMarginBottomForLastLine:I

    .line 916
    iget v0, p1, Lcom/google/android/play/layout/FlowLayoutManager$LayoutParams;->vAlign:I

    iput v0, p0, Lcom/google/android/play/layout/FlowLayoutManager$ItemInfo;->mVAlign:I

    .line 917
    return-void
.end method

.method public recycle()V
    .locals 1

    .prologue
    .line 961
    invoke-direct {p0}, Lcom/google/android/play/layout/FlowLayoutManager$ItemInfo;->reset()V

    .line 962
    sget-object v0, Lcom/google/android/play/layout/FlowLayoutManager$ItemInfo;->sPool:Landroid/support/v4/util/Pools$Pool;

    invoke-interface {v0, p0}, Landroid/support/v4/util/Pools$Pool;->release(Ljava/lang/Object;)Z

    .line 963
    return-void
.end method
