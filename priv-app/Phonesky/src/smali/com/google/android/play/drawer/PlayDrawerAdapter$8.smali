.class Lcom/google/android/play/drawer/PlayDrawerAdapter$8;
.super Ljava/lang/Object;
.source "PlayDrawerAdapter.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/play/drawer/PlayDrawerAdapter;->getPrimaryActionView(Landroid/view/View;Landroid/view/ViewGroup;Lcom/google/android/play/drawer/PlayDrawerLayout$PlayDrawerPrimaryAction;ZZ)Landroid/view/View;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/play/drawer/PlayDrawerAdapter;

.field final synthetic val$primaryAction:Lcom/google/android/play/drawer/PlayDrawerLayout$PlayDrawerPrimaryAction;


# direct methods
.method constructor <init>(Lcom/google/android/play/drawer/PlayDrawerAdapter;Lcom/google/android/play/drawer/PlayDrawerLayout$PlayDrawerPrimaryAction;)V
    .locals 0

    .prologue
    .line 549
    iput-object p1, p0, Lcom/google/android/play/drawer/PlayDrawerAdapter$8;->this$0:Lcom/google/android/play/drawer/PlayDrawerAdapter;

    iput-object p2, p0, Lcom/google/android/play/drawer/PlayDrawerAdapter$8;->val$primaryAction:Lcom/google/android/play/drawer/PlayDrawerLayout$PlayDrawerPrimaryAction;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 2
    .param p1, "v"    # Landroid/view/View;

    .prologue
    .line 552
    iget-object v0, p0, Lcom/google/android/play/drawer/PlayDrawerAdapter$8;->this$0:Lcom/google/android/play/drawer/PlayDrawerAdapter;

    # getter for: Lcom/google/android/play/drawer/PlayDrawerAdapter;->mPlayDrawerContentClickListener:Lcom/google/android/play/drawer/PlayDrawerLayout$PlayDrawerContentClickListener;
    invoke-static {v0}, Lcom/google/android/play/drawer/PlayDrawerAdapter;->access$700(Lcom/google/android/play/drawer/PlayDrawerAdapter;)Lcom/google/android/play/drawer/PlayDrawerLayout$PlayDrawerContentClickListener;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/play/drawer/PlayDrawerAdapter$8;->val$primaryAction:Lcom/google/android/play/drawer/PlayDrawerLayout$PlayDrawerPrimaryAction;

    invoke-interface {v0, v1}, Lcom/google/android/play/drawer/PlayDrawerLayout$PlayDrawerContentClickListener;->onPrimaryActionClicked(Lcom/google/android/play/drawer/PlayDrawerLayout$PlayDrawerPrimaryAction;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 553
    iget-object v0, p0, Lcom/google/android/play/drawer/PlayDrawerAdapter$8;->this$0:Lcom/google/android/play/drawer/PlayDrawerAdapter;

    # getter for: Lcom/google/android/play/drawer/PlayDrawerAdapter;->mPlayDrawerLayout:Lcom/google/android/play/drawer/PlayDrawerLayout;
    invoke-static {v0}, Lcom/google/android/play/drawer/PlayDrawerAdapter;->access$800(Lcom/google/android/play/drawer/PlayDrawerAdapter;)Lcom/google/android/play/drawer/PlayDrawerLayout;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/play/drawer/PlayDrawerLayout;->closeDrawer()V

    .line 555
    :cond_0
    return-void
.end method
