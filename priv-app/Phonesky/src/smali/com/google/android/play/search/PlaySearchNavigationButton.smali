.class public Lcom/google/android/play/search/PlaySearchNavigationButton;
.super Landroid/widget/ImageView;
.source "PlaySearchNavigationButton.java"

# interfaces
.implements Lcom/google/android/play/search/PlaySearchListener;


# static fields
.field private static final IS_HONEYCOMB_OR_GREATER:Z


# instance fields
.field private mArrowOrBurgerDrawable:Lcom/google/android/play/search/ArrowOrBurgerDrawable;

.field private mController:Lcom/google/android/play/search/PlaySearchController;

.field private mCurrentMode:I


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 30
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0xb

    if-lt v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    sput-boolean v0, Lcom/google/android/play/search/PlaySearchNavigationButton;->IS_HONEYCOMB_OR_GREATER:Z

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 44
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/google/android/play/search/PlaySearchNavigationButton;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 45
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    .line 48
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/google/android/play/search/PlaySearchNavigationButton;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 49
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 3
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;
    .param p3, "defStyle"    # I

    .prologue
    .line 52
    invoke-direct {p0, p1, p2, p3}, Landroid/widget/ImageView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 53
    new-instance v0, Lcom/google/android/play/search/ArrowOrBurgerDrawable;

    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    sget v2, Lcom/google/android/play/R$color;->play_search_plate_navigation_button_color:I

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    const/4 v2, 0x0

    invoke-direct {v0, v1, v2}, Lcom/google/android/play/search/ArrowOrBurgerDrawable;-><init>(IZ)V

    iput-object v0, p0, Lcom/google/android/play/search/PlaySearchNavigationButton;->mArrowOrBurgerDrawable:Lcom/google/android/play/search/ArrowOrBurgerDrawable;

    .line 56
    return-void
.end method

.method static synthetic access$000(Lcom/google/android/play/search/PlaySearchNavigationButton;)I
    .locals 1
    .param p0, "x0"    # Lcom/google/android/play/search/PlaySearchNavigationButton;

    .prologue
    .line 20
    iget v0, p0, Lcom/google/android/play/search/PlaySearchNavigationButton;->mCurrentMode:I

    return v0
.end method

.method static synthetic access$100(Lcom/google/android/play/search/PlaySearchNavigationButton;)Lcom/google/android/play/search/PlaySearchController;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/play/search/PlaySearchNavigationButton;

    .prologue
    .line 20
    iget-object v0, p0, Lcom/google/android/play/search/PlaySearchNavigationButton;->mController:Lcom/google/android/play/search/PlaySearchController;

    return-object v0
.end method

.method static synthetic access$200(Lcom/google/android/play/search/PlaySearchNavigationButton;)Lcom/google/android/play/search/ArrowOrBurgerDrawable;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/play/search/PlaySearchNavigationButton;

    .prologue
    .line 20
    iget-object v0, p0, Lcom/google/android/play/search/PlaySearchNavigationButton;->mArrowOrBurgerDrawable:Lcom/google/android/play/search/ArrowOrBurgerDrawable;

    return-object v0
.end method

.method private changeModeAnimated(Z)V
    .locals 7
    .param p1, "showBurger"    # Z

    .prologue
    const/4 v6, 0x2

    const/high16 v2, 0x3f800000    # 1.0f

    .line 123
    if-eqz p1, :cond_0

    const/4 v1, 0x0

    .line 125
    .local v1, "finalValue":F
    :goto_0
    new-array v3, v6, [F

    fill-array-data v3, :array_0

    invoke-static {v3}, Landroid/animation/ValueAnimator;->ofFloat([F)Landroid/animation/ValueAnimator;

    move-result-object v0

    .line 126
    .local v0, "arrowOrBurgerAnimator":Landroid/animation/ValueAnimator;
    new-instance v3, Lcom/google/android/play/search/PlaySearchNavigationButton$2;

    invoke-direct {v3, p0}, Lcom/google/android/play/search/PlaySearchNavigationButton$2;-><init>(Lcom/google/android/play/search/PlaySearchNavigationButton;)V

    invoke-virtual {v0, v3}, Landroid/animation/ValueAnimator;->addUpdateListener(Landroid/animation/ValueAnimator$AnimatorUpdateListener;)V

    .line 132
    const-wide/16 v4, 0x15e

    invoke-virtual {v0, v4, v5}, Landroid/animation/ValueAnimator;->setDuration(J)Landroid/animation/ValueAnimator;

    .line 133
    sget-object v3, Lcom/google/android/play/search/BakedBezierInterpolator;->INSTANCE:Lcom/google/android/play/search/BakedBezierInterpolator;

    invoke-virtual {v0, v3}, Landroid/animation/ValueAnimator;->setInterpolator(Landroid/animation/TimeInterpolator;)V

    .line 134
    new-array v3, v6, [F

    const/4 v4, 0x0

    sub-float/2addr v2, v1

    aput v2, v3, v4

    const/4 v2, 0x1

    aput v1, v3, v2

    invoke-virtual {v0, v3}, Landroid/animation/ValueAnimator;->setFloatValues([F)V

    .line 135
    invoke-virtual {v0}, Landroid/animation/ValueAnimator;->start()V

    .line 136
    return-void

    .end local v0    # "arrowOrBurgerAnimator":Landroid/animation/ValueAnimator;
    .end local v1    # "finalValue":F
    :cond_0
    move v1, v2

    .line 123
    goto :goto_0

    .line 125
    nop

    :array_0
    .array-data 4
        0x0
        0x3f800000    # 1.0f
    .end array-data
.end method

.method private setMode(I)V
    .locals 3
    .param p1, "mode"    # I

    .prologue
    const/4 v1, 0x1

    .line 101
    iget v2, p0, Lcom/google/android/play/search/PlaySearchNavigationButton;->mCurrentMode:I

    if-ne v2, p1, :cond_0

    .line 119
    :goto_0
    return-void

    .line 105
    :cond_0
    if-ne p1, v1, :cond_1

    .line 106
    .local v1, "showBurger":Z
    :goto_1
    sget-boolean v2, Lcom/google/android/play/search/PlaySearchNavigationButton;->IS_HONEYCOMB_OR_GREATER:Z

    if-eqz v2, :cond_2

    .line 107
    invoke-direct {p0, v1}, Lcom/google/android/play/search/PlaySearchNavigationButton;->changeModeAnimated(Z)V

    .line 112
    :goto_2
    if-eqz v1, :cond_3

    .line 113
    sget v0, Lcom/google/android/play/R$string;->play_accessibility_search_plate_menu_button:I

    .line 117
    .local v0, "descriptionResId":I
    :goto_3
    invoke-virtual {p0}, Lcom/google/android/play/search/PlaySearchNavigationButton;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-virtual {v2, v0}, Landroid/content/Context;->getText(I)Ljava/lang/CharSequence;

    move-result-object v2

    invoke-virtual {p0, v2}, Lcom/google/android/play/search/PlaySearchNavigationButton;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 118
    iput p1, p0, Lcom/google/android/play/search/PlaySearchNavigationButton;->mCurrentMode:I

    goto :goto_0

    .line 105
    .end local v0    # "descriptionResId":I
    .end local v1    # "showBurger":Z
    :cond_1
    const/4 v1, 0x0

    goto :goto_1

    .line 109
    .restart local v1    # "showBurger":Z
    :cond_2
    iget-object v2, p0, Lcom/google/android/play/search/PlaySearchNavigationButton;->mArrowOrBurgerDrawable:Lcom/google/android/play/search/ArrowOrBurgerDrawable;

    invoke-virtual {v2, v1}, Lcom/google/android/play/search/ArrowOrBurgerDrawable;->setAsBurger(Z)V

    goto :goto_2

    .line 115
    :cond_3
    sget v0, Lcom/google/android/play/R$string;->play_accessibility_search_plate_back_button:I

    .restart local v0    # "descriptionResId":I
    goto :goto_3
.end method


# virtual methods
.method public onFinishInflate()V
    .locals 2

    .prologue
    const/4 v1, 0x1

    .line 60
    invoke-super {p0}, Landroid/widget/ImageView;->onFinishInflate()V

    .line 61
    iget-object v0, p0, Lcom/google/android/play/search/PlaySearchNavigationButton;->mArrowOrBurgerDrawable:Lcom/google/android/play/search/ArrowOrBurgerDrawable;

    invoke-virtual {p0, v0}, Lcom/google/android/play/search/PlaySearchNavigationButton;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 62
    iget-object v0, p0, Lcom/google/android/play/search/PlaySearchNavigationButton;->mArrowOrBurgerDrawable:Lcom/google/android/play/search/ArrowOrBurgerDrawable;

    invoke-virtual {v0, v1}, Lcom/google/android/play/search/ArrowOrBurgerDrawable;->setAsBurger(Z)V

    .line 63
    new-instance v0, Lcom/google/android/play/search/PlaySearchNavigationButton$1;

    invoke-direct {v0, p0}, Lcom/google/android/play/search/PlaySearchNavigationButton$1;-><init>(Lcom/google/android/play/search/PlaySearchNavigationButton;)V

    invoke-virtual {p0, v0}, Lcom/google/android/play/search/PlaySearchNavigationButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 72
    invoke-direct {p0, v1}, Lcom/google/android/play/search/PlaySearchNavigationButton;->setMode(I)V

    .line 73
    return-void
.end method

.method public onModeChanged(I)V
    .locals 2
    .param p1, "searchMode"    # I

    .prologue
    const/4 v1, 0x2

    const/4 v0, 0x1

    .line 88
    if-ne p1, v0, :cond_1

    .line 89
    invoke-direct {p0, v0}, Lcom/google/android/play/search/PlaySearchNavigationButton;->setMode(I)V

    .line 93
    :cond_0
    :goto_0
    return-void

    .line 90
    :cond_1
    if-ne p1, v1, :cond_0

    .line 91
    invoke-direct {p0, v1}, Lcom/google/android/play/search/PlaySearchNavigationButton;->setMode(I)V

    goto :goto_0
.end method

.method public onQueryChanged(Ljava/lang/String;)V
    .locals 0
    .param p1, "text"    # Ljava/lang/String;

    .prologue
    .line 98
    return-void
.end method

.method public setPlaySearchController(Lcom/google/android/play/search/PlaySearchController;)V
    .locals 1
    .param p1, "playSearchController"    # Lcom/google/android/play/search/PlaySearchController;

    .prologue
    .line 79
    iget-object v0, p0, Lcom/google/android/play/search/PlaySearchNavigationButton;->mController:Lcom/google/android/play/search/PlaySearchController;

    if-eqz v0, :cond_0

    .line 80
    iget-object v0, p0, Lcom/google/android/play/search/PlaySearchNavigationButton;->mController:Lcom/google/android/play/search/PlaySearchController;

    invoke-virtual {v0, p0}, Lcom/google/android/play/search/PlaySearchController;->removePlaySearchListener(Lcom/google/android/play/search/PlaySearchListener;)V

    .line 82
    :cond_0
    iput-object p1, p0, Lcom/google/android/play/search/PlaySearchNavigationButton;->mController:Lcom/google/android/play/search/PlaySearchController;

    .line 83
    iget-object v0, p0, Lcom/google/android/play/search/PlaySearchNavigationButton;->mController:Lcom/google/android/play/search/PlaySearchController;

    invoke-virtual {v0, p0}, Lcom/google/android/play/search/PlaySearchController;->addPlaySearchListener(Lcom/google/android/play/search/PlaySearchListener;)V

    .line 84
    return-void
.end method
