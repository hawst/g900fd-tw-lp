.class public Lcom/google/android/play/search/PlaySearchSuggestionLayoutManager;
.super Landroid/support/v7/widget/LinearLayoutManager;
.source "PlaySearchSuggestionLayoutManager.java"


# instance fields
.field private final mMaxHeight:I

.field private final mOneSuggestionHeightPx:F


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 5
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 26
    invoke-direct {p0, p1}, Landroid/support/v7/widget/LinearLayoutManager;-><init>(Landroid/content/Context;)V

    .line 27
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    sget v4, Lcom/google/android/play/R$dimen;->play_search_one_suggestion_height:I

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v3

    iput v3, p0, Lcom/google/android/play/search/PlaySearchSuggestionLayoutManager;->mOneSuggestionHeightPx:F

    .line 29
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    invoke-virtual {v3}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v3

    iget v2, v3, Landroid/util/DisplayMetrics;->heightPixels:I

    .line 30
    .local v2, "screenHeight":I
    int-to-float v3, v2

    const v4, 0x3ecccccd    # 0.4f

    mul-float/2addr v3, v4

    float-to-int v1, v3

    .line 31
    .local v1, "maxUsableScreenHeight":I
    int-to-float v3, v1

    iget v4, p0, Lcom/google/android/play/search/PlaySearchSuggestionLayoutManager;->mOneSuggestionHeightPx:F

    div-float/2addr v3, v4

    float-to-int v0, v3

    .line 32
    .local v0, "maxSuggestionsThatCanBeFit":I
    int-to-float v3, v0

    iget v4, p0, Lcom/google/android/play/search/PlaySearchSuggestionLayoutManager;->mOneSuggestionHeightPx:F

    mul-float/2addr v3, v4

    float-to-int v3, v3

    iput v3, p0, Lcom/google/android/play/search/PlaySearchSuggestionLayoutManager;->mMaxHeight:I

    .line 33
    return-void
.end method


# virtual methods
.method public final onLayoutChildren(Landroid/support/v7/widget/RecyclerView$Recycler;Landroid/support/v7/widget/RecyclerView$State;)V
    .locals 1
    .param p1, "recycler"    # Landroid/support/v7/widget/RecyclerView$Recycler;
    .param p2, "state"    # Landroid/support/v7/widget/RecyclerView$State;

    .prologue
    const/4 v0, 0x0

    .line 45
    invoke-super {p0, p1, p2}, Landroid/support/v7/widget/LinearLayoutManager;->onLayoutChildren(Landroid/support/v7/widget/RecyclerView$Recycler;Landroid/support/v7/widget/RecyclerView$State;)V

    .line 46
    invoke-virtual {p0, p1, p2, v0, v0}, Lcom/google/android/play/search/PlaySearchSuggestionLayoutManager;->onMeasure(Landroid/support/v7/widget/RecyclerView$Recycler;Landroid/support/v7/widget/RecyclerView$State;II)V

    .line 47
    return-void
.end method

.method public final onMeasure(Landroid/support/v7/widget/RecyclerView$Recycler;Landroid/support/v7/widget/RecyclerView$State;II)V
    .locals 4
    .param p1, "recycler"    # Landroid/support/v7/widget/RecyclerView$Recycler;
    .param p2, "state"    # Landroid/support/v7/widget/RecyclerView$State;
    .param p3, "widthSpec"    # I
    .param p4, "heightSpec"    # I

    .prologue
    .line 38
    iget v1, p0, Lcom/google/android/play/search/PlaySearchSuggestionLayoutManager;->mMaxHeight:I

    invoke-virtual {p0}, Lcom/google/android/play/search/PlaySearchSuggestionLayoutManager;->getItemCount()I

    move-result v2

    int-to-float v2, v2

    iget v3, p0, Lcom/google/android/play/search/PlaySearchSuggestionLayoutManager;->mOneSuggestionHeightPx:F

    mul-float/2addr v2, v3

    float-to-int v2, v2

    invoke-static {v1, v2}, Ljava/lang/Math;->min(II)I

    move-result v0

    .line 39
    .local v0, "maxHeight":I
    const/high16 v1, -0x80000000

    invoke-static {v0, v1}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v1

    invoke-virtual {p0, p3, v1}, Lcom/google/android/play/search/PlaySearchSuggestionLayoutManager;->setMeasuredDimension(II)V

    .line 41
    return-void
.end method
