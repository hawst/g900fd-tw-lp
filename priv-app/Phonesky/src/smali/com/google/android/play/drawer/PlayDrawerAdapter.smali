.class Lcom/google/android/play/drawer/PlayDrawerAdapter;
.super Landroid/widget/BaseAdapter;
.source "PlayDrawerAdapter.java"


# instance fields
.field private final mAccountDocV2s:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Lcom/google/android/finsky/protos/DocumentV2$DocV2;",
            ">;"
        }
    .end annotation
.end field

.field private mAccountListExpanded:Z

.field private final mBitmapLoader:Lcom/google/android/play/image/BitmapLoader;

.field private mContext:Landroid/content/Context;

.field private mCurrentAccount:Landroid/accounts/Account;

.field private mDownloadOnlyEnabled:Z

.field private mDownloadSwitchConfig:Lcom/google/android/play/drawer/PlayDrawerLayout$PlayDrawerDownloadSwitchConfig;

.field private mHasAccounts:Z

.field private final mInflater:Landroid/view/LayoutInflater;

.field private final mIsAccountDocLoaded:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private mListView:Landroid/widget/ListView;

.field private mNonCurrentAccounts:[Landroid/accounts/Account;

.field private mPlayDfeApiProvider:Lcom/google/android/play/dfe/api/PlayDfeApiProvider;

.field private mPlayDrawerContentClickListener:Lcom/google/android/play/drawer/PlayDrawerLayout$PlayDrawerContentClickListener;

.field private mPlayDrawerLayout:Lcom/google/android/play/drawer/PlayDrawerLayout;

.field private final mPrimaryActions:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/android/play/drawer/PlayDrawerLayout$PlayDrawerPrimaryAction;",
            ">;"
        }
    .end annotation
.end field

.field private mProfileContainerPosition:I

.field private mProfileInfoView:Lcom/google/android/play/drawer/PlayDrawerProfileInfoView;

.field private final mSecondaryActions:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/android/play/drawer/PlayDrawerLayout$PlayDrawerSecondaryAction;",
            ">;"
        }
    .end annotation
.end field

.field private mShowDownloadOnlyToggle:Z


# direct methods
.method public constructor <init>(Landroid/content/Context;ZLcom/google/android/play/drawer/PlayDrawerLayout$PlayDrawerContentClickListener;Lcom/google/android/play/dfe/api/PlayDfeApiProvider;Lcom/google/android/play/image/BitmapLoader;Lcom/google/android/play/drawer/PlayDrawerLayout;Landroid/widget/ListView;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "isAccountListExpanded"    # Z
    .param p3, "playDrawerContentClickListener"    # Lcom/google/android/play/drawer/PlayDrawerLayout$PlayDrawerContentClickListener;
    .param p4, "playDfeApiProvider"    # Lcom/google/android/play/dfe/api/PlayDfeApiProvider;
    .param p5, "bitmapLoader"    # Lcom/google/android/play/image/BitmapLoader;
    .param p6, "playDrawerLayout"    # Lcom/google/android/play/drawer/PlayDrawerLayout;
    .param p7, "listView"    # Landroid/widget/ListView;

    .prologue
    .line 89
    invoke-direct {p0}, Landroid/widget/BaseAdapter;-><init>()V

    .line 72
    invoke-static {}, Lcom/google/android/play/utils/collections/Lists;->newArrayList()Ljava/util/ArrayList;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/play/drawer/PlayDrawerAdapter;->mPrimaryActions:Ljava/util/List;

    .line 73
    invoke-static {}, Lcom/google/android/play/utils/collections/Lists;->newArrayList()Ljava/util/ArrayList;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/play/drawer/PlayDrawerAdapter;->mSecondaryActions:Ljava/util/List;

    .line 79
    invoke-static {}, Lcom/google/android/play/utils/collections/Maps;->newHashMap()Ljava/util/HashMap;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/play/drawer/PlayDrawerAdapter;->mAccountDocV2s:Ljava/util/Map;

    .line 80
    invoke-static {}, Lcom/google/android/play/utils/collections/Sets;->newHashSet()Ljava/util/HashSet;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/play/drawer/PlayDrawerAdapter;->mIsAccountDocLoaded:Ljava/util/Set;

    .line 91
    iput-object p1, p0, Lcom/google/android/play/drawer/PlayDrawerAdapter;->mContext:Landroid/content/Context;

    .line 92
    invoke-static {p1}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/play/drawer/PlayDrawerAdapter;->mInflater:Landroid/view/LayoutInflater;

    .line 93
    iput-object p4, p0, Lcom/google/android/play/drawer/PlayDrawerAdapter;->mPlayDfeApiProvider:Lcom/google/android/play/dfe/api/PlayDfeApiProvider;

    .line 94
    iput-object p5, p0, Lcom/google/android/play/drawer/PlayDrawerAdapter;->mBitmapLoader:Lcom/google/android/play/image/BitmapLoader;

    .line 95
    iput-object p3, p0, Lcom/google/android/play/drawer/PlayDrawerAdapter;->mPlayDrawerContentClickListener:Lcom/google/android/play/drawer/PlayDrawerLayout$PlayDrawerContentClickListener;

    .line 96
    iput-object p6, p0, Lcom/google/android/play/drawer/PlayDrawerAdapter;->mPlayDrawerLayout:Lcom/google/android/play/drawer/PlayDrawerLayout;

    .line 97
    iput-object p7, p0, Lcom/google/android/play/drawer/PlayDrawerAdapter;->mListView:Landroid/widget/ListView;

    .line 99
    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/play/drawer/PlayDrawerAdapter;->mProfileContainerPosition:I

    .line 100
    iput-boolean p2, p0, Lcom/google/android/play/drawer/PlayDrawerAdapter;->mAccountListExpanded:Z

    .line 101
    return-void
.end method

.method static synthetic access$000(Lcom/google/android/play/drawer/PlayDrawerAdapter;)Ljava/util/Map;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/play/drawer/PlayDrawerAdapter;

    .prologue
    .line 49
    iget-object v0, p0, Lcom/google/android/play/drawer/PlayDrawerAdapter;->mAccountDocV2s:Ljava/util/Map;

    return-object v0
.end method

.method static synthetic access$100(Lcom/google/android/play/drawer/PlayDrawerAdapter;)Ljava/util/Set;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/play/drawer/PlayDrawerAdapter;

    .prologue
    .line 49
    iget-object v0, p0, Lcom/google/android/play/drawer/PlayDrawerAdapter;->mIsAccountDocLoaded:Ljava/util/Set;

    return-object v0
.end method

.method static synthetic access$1000(Lcom/google/android/play/drawer/PlayDrawerAdapter;)V
    .locals 0
    .param p0, "x0"    # Lcom/google/android/play/drawer/PlayDrawerAdapter;

    .prologue
    .line 49
    invoke-direct {p0}, Lcom/google/android/play/drawer/PlayDrawerAdapter;->toggleAccountsList()V

    return-void
.end method

.method static synthetic access$1102(Lcom/google/android/play/drawer/PlayDrawerAdapter;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/google/android/play/drawer/PlayDrawerAdapter;
    .param p1, "x1"    # Z

    .prologue
    .line 49
    iput-boolean p1, p0, Lcom/google/android/play/drawer/PlayDrawerAdapter;->mDownloadOnlyEnabled:Z

    return p1
.end method

.method static synthetic access$200(Lcom/google/android/play/drawer/PlayDrawerAdapter;)Landroid/accounts/Account;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/play/drawer/PlayDrawerAdapter;

    .prologue
    .line 49
    iget-object v0, p0, Lcom/google/android/play/drawer/PlayDrawerAdapter;->mCurrentAccount:Landroid/accounts/Account;

    return-object v0
.end method

.method static synthetic access$300(Lcom/google/android/play/drawer/PlayDrawerAdapter;)Z
    .locals 1
    .param p0, "x0"    # Lcom/google/android/play/drawer/PlayDrawerAdapter;

    .prologue
    .line 49
    invoke-direct {p0}, Lcom/google/android/play/drawer/PlayDrawerAdapter;->isProfileContainerVisible()Z

    move-result v0

    return v0
.end method

.method static synthetic access$400(Lcom/google/android/play/drawer/PlayDrawerAdapter;)[Landroid/accounts/Account;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/play/drawer/PlayDrawerAdapter;

    .prologue
    .line 49
    iget-object v0, p0, Lcom/google/android/play/drawer/PlayDrawerAdapter;->mNonCurrentAccounts:[Landroid/accounts/Account;

    return-object v0
.end method

.method static synthetic access$500(Lcom/google/android/play/drawer/PlayDrawerAdapter;)Lcom/google/android/play/image/BitmapLoader;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/play/drawer/PlayDrawerAdapter;

    .prologue
    .line 49
    iget-object v0, p0, Lcom/google/android/play/drawer/PlayDrawerAdapter;->mBitmapLoader:Lcom/google/android/play/image/BitmapLoader;

    return-object v0
.end method

.method static synthetic access$600(Lcom/google/android/play/drawer/PlayDrawerAdapter;)Lcom/google/android/play/drawer/PlayDrawerProfileInfoView;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/play/drawer/PlayDrawerAdapter;

    .prologue
    .line 49
    iget-object v0, p0, Lcom/google/android/play/drawer/PlayDrawerAdapter;->mProfileInfoView:Lcom/google/android/play/drawer/PlayDrawerProfileInfoView;

    return-object v0
.end method

.method static synthetic access$700(Lcom/google/android/play/drawer/PlayDrawerAdapter;)Lcom/google/android/play/drawer/PlayDrawerLayout$PlayDrawerContentClickListener;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/play/drawer/PlayDrawerAdapter;

    .prologue
    .line 49
    iget-object v0, p0, Lcom/google/android/play/drawer/PlayDrawerAdapter;->mPlayDrawerContentClickListener:Lcom/google/android/play/drawer/PlayDrawerLayout$PlayDrawerContentClickListener;

    return-object v0
.end method

.method static synthetic access$800(Lcom/google/android/play/drawer/PlayDrawerAdapter;)Lcom/google/android/play/drawer/PlayDrawerLayout;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/play/drawer/PlayDrawerAdapter;

    .prologue
    .line 49
    iget-object v0, p0, Lcom/google/android/play/drawer/PlayDrawerAdapter;->mPlayDrawerLayout:Lcom/google/android/play/drawer/PlayDrawerLayout;

    return-object v0
.end method

.method static synthetic access$900(Lcom/google/android/play/drawer/PlayDrawerAdapter;)Z
    .locals 1
    .param p0, "x0"    # Lcom/google/android/play/drawer/PlayDrawerAdapter;

    .prologue
    .line 49
    iget-boolean v0, p0, Lcom/google/android/play/drawer/PlayDrawerAdapter;->mAccountListExpanded:Z

    return v0
.end method

.method private getAccountView(Landroid/view/View;Landroid/view/ViewGroup;Landroid/accounts/Account;)Landroid/view/View;
    .locals 5
    .param p1, "convertView"    # Landroid/view/View;
    .param p2, "parent"    # Landroid/view/ViewGroup;
    .param p3, "account"    # Landroid/accounts/Account;

    .prologue
    .line 488
    if-eqz p1, :cond_0

    move-object v2, p1

    :goto_0
    check-cast v2, Lcom/google/android/play/drawer/PlayDrawerAccountRow;

    move-object v1, v2

    check-cast v1, Lcom/google/android/play/drawer/PlayDrawerAccountRow;

    .line 490
    .local v1, "accountRow":Lcom/google/android/play/drawer/PlayDrawerAccountRow;
    iget-object v0, p3, Landroid/accounts/Account;->name:Ljava/lang/String;

    .line 491
    .local v0, "accountName":Ljava/lang/String;
    iget-object v2, p0, Lcom/google/android/play/drawer/PlayDrawerAdapter;->mAccountDocV2s:Ljava/util/Map;

    invoke-interface {v2, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/google/android/finsky/protos/DocumentV2$DocV2;

    iget-object v3, p0, Lcom/google/android/play/drawer/PlayDrawerAdapter;->mBitmapLoader:Lcom/google/android/play/image/BitmapLoader;

    invoke-virtual {v1, v2, v0, v3}, Lcom/google/android/play/drawer/PlayDrawerAccountRow;->bind(Lcom/google/android/finsky/protos/DocumentV2$DocV2;Ljava/lang/String;Lcom/google/android/play/image/BitmapLoader;)V

    .line 492
    new-instance v2, Lcom/google/android/play/drawer/PlayDrawerAdapter$7;

    invoke-direct {v2, p0, v0}, Lcom/google/android/play/drawer/PlayDrawerAdapter$7;-><init>(Lcom/google/android/play/drawer/PlayDrawerAdapter;Ljava/lang/String;)V

    invoke-virtual {v1, v2}, Lcom/google/android/play/drawer/PlayDrawerAccountRow;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 500
    return-object v1

    .line 488
    .end local v0    # "accountName":Ljava/lang/String;
    .end local v1    # "accountRow":Lcom/google/android/play/drawer/PlayDrawerAccountRow;
    :cond_0
    iget-object v2, p0, Lcom/google/android/play/drawer/PlayDrawerAdapter;->mInflater:Landroid/view/LayoutInflater;

    sget v3, Lcom/google/android/play/R$layout;->play_drawer_account_row:I

    const/4 v4, 0x0

    invoke-virtual {v2, v3, p2, v4}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v2

    goto :goto_0
.end method

.method private getDownloadToggleView(Landroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 4
    .param p1, "convertView"    # Landroid/view/View;
    .param p2, "parent"    # Landroid/view/ViewGroup;

    .prologue
    .line 590
    if-nez p1, :cond_0

    .line 592
    iget-object v1, p0, Lcom/google/android/play/drawer/PlayDrawerAdapter;->mInflater:Landroid/view/LayoutInflater;

    sget v2, Lcom/google/android/play/R$layout;->play_drawer_download_toggle:I

    const/4 v3, 0x0

    invoke-virtual {v1, v2, p2, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/play/drawer/PlayDrawerDownloadSwitchRow;

    .line 597
    .local v0, "toggleRow":Lcom/google/android/play/drawer/PlayDrawerDownloadSwitchRow;
    iget-object v1, p0, Lcom/google/android/play/drawer/PlayDrawerAdapter;->mDownloadSwitchConfig:Lcom/google/android/play/drawer/PlayDrawerLayout$PlayDrawerDownloadSwitchConfig;

    invoke-virtual {v0, v1}, Lcom/google/android/play/drawer/PlayDrawerDownloadSwitchRow;->configure(Lcom/google/android/play/drawer/PlayDrawerLayout$PlayDrawerDownloadSwitchConfig;)V

    .line 598
    new-instance v1, Lcom/google/android/play/drawer/PlayDrawerAdapter$10;

    invoke-direct {v1, p0}, Lcom/google/android/play/drawer/PlayDrawerAdapter$10;-><init>(Lcom/google/android/play/drawer/PlayDrawerAdapter;)V

    invoke-virtual {v0, v1}, Lcom/google/android/play/drawer/PlayDrawerDownloadSwitchRow;->setOnCheckedChangeListener(Lcom/google/android/play/drawer/PlayDrawerDownloadSwitchRow$OnCheckedChangeListener;)V

    .line 610
    :goto_0
    iget-boolean v1, p0, Lcom/google/android/play/drawer/PlayDrawerAdapter;->mDownloadOnlyEnabled:Z

    invoke-virtual {v0, v1}, Lcom/google/android/play/drawer/PlayDrawerDownloadSwitchRow;->setCheckedNoCallbacks(Z)V

    .line 612
    return-object v0

    .end local v0    # "toggleRow":Lcom/google/android/play/drawer/PlayDrawerDownloadSwitchRow;
    :cond_0
    move-object v0, p1

    .line 608
    check-cast v0, Lcom/google/android/play/drawer/PlayDrawerDownloadSwitchRow;

    .restart local v0    # "toggleRow":Lcom/google/android/play/drawer/PlayDrawerDownloadSwitchRow;
    goto :goto_0
.end method

.method private getPrimaryActionView(Landroid/view/View;Landroid/view/ViewGroup;Lcom/google/android/play/drawer/PlayDrawerLayout$PlayDrawerPrimaryAction;ZZ)Landroid/view/View;
    .locals 7
    .param p1, "convertView"    # Landroid/view/View;
    .param p2, "parent"    # Landroid/view/ViewGroup;
    .param p3, "primaryAction"    # Lcom/google/android/play/drawer/PlayDrawerLayout$PlayDrawerPrimaryAction;
    .param p4, "active"    # Z
    .param p5, "disabled"    # Z

    .prologue
    const/4 v6, 0x0

    .line 511
    invoke-virtual {p2}, Landroid/view/ViewGroup;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    .line 514
    .local v3, "res":Landroid/content/res/Resources;
    if-eqz p4, :cond_0

    .line 515
    sget v2, Lcom/google/android/play/R$layout;->play_drawer_primary_action_active:I

    .line 522
    .local v2, "layoutId":I
    :goto_0
    if-eqz p1, :cond_2

    move-object v4, p1

    :goto_1
    check-cast v4, Landroid/widget/TextView;

    move-object v0, v4

    check-cast v0, Landroid/widget/TextView;

    .line 524
    .local v0, "destinationRow":Landroid/widget/TextView;
    iget-object v4, p3, Lcom/google/android/play/drawer/PlayDrawerLayout$PlayDrawerPrimaryAction;->actionText:Ljava/lang/String;

    invoke-virtual {v0, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 525
    iget v4, p3, Lcom/google/android/play/drawer/PlayDrawerLayout$PlayDrawerPrimaryAction;->iconResId:I

    if-lez v4, :cond_5

    .line 527
    if-eqz p4, :cond_3

    iget v4, p3, Lcom/google/android/play/drawer/PlayDrawerLayout$PlayDrawerPrimaryAction;->activeIconResId:I

    if-lez v4, :cond_3

    .line 528
    iget v4, p3, Lcom/google/android/play/drawer/PlayDrawerLayout$PlayDrawerPrimaryAction;->activeIconResId:I

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    .line 532
    .local v1, "drawable":Landroid/graphics/drawable/Drawable;
    :goto_2
    if-eqz p5, :cond_4

    .line 533
    const/16 v4, 0x42

    invoke-virtual {v1, v4}, Landroid/graphics/drawable/Drawable;->setAlpha(I)V

    .line 537
    :goto_3
    iget-object v4, p0, Lcom/google/android/play/drawer/PlayDrawerAdapter;->mContext:Landroid/content/Context;

    invoke-static {v4}, Lcom/google/android/play/utils/PlayUtils;->useLtr(Landroid/content/Context;)Z

    move-result v4

    invoke-static {v0, v1, v4}, Lcom/google/android/play/drawer/PlayDrawerAdapter;->setDrawableStart(Landroid/widget/TextView;Landroid/graphics/drawable/Drawable;Z)V

    .line 542
    .end local v1    # "drawable":Landroid/graphics/drawable/Drawable;
    :goto_4
    if-eqz p4, :cond_6

    iget v4, p3, Lcom/google/android/play/drawer/PlayDrawerLayout$PlayDrawerPrimaryAction;->activeTextColorResId:I

    if-lez v4, :cond_6

    .line 543
    iget v4, p3, Lcom/google/android/play/drawer/PlayDrawerLayout$PlayDrawerPrimaryAction;->activeTextColorResId:I

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getColor(I)I

    move-result v4

    invoke-virtual {v0, v4}, Landroid/widget/TextView;->setTextColor(I)V

    .line 549
    :goto_5
    new-instance v4, Lcom/google/android/play/drawer/PlayDrawerAdapter$8;

    invoke-direct {v4, p0, p3}, Lcom/google/android/play/drawer/PlayDrawerAdapter$8;-><init>(Lcom/google/android/play/drawer/PlayDrawerAdapter;Lcom/google/android/play/drawer/PlayDrawerLayout$PlayDrawerPrimaryAction;)V

    invoke-virtual {v0, v4}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 557
    iget-object v4, p0, Lcom/google/android/play/drawer/PlayDrawerAdapter;->mContext:Landroid/content/Context;

    invoke-static {v4}, Lcom/google/android/play/utils/PlayUtils;->useLtr(Landroid/content/Context;)Z

    move-result v4

    invoke-static {v0, v4}, Lcom/google/android/play/drawer/PlayDrawerAdapter;->setTextAlignmentStart(Landroid/widget/TextView;Z)V

    .line 558
    return-object v0

    .line 516
    .end local v0    # "destinationRow":Landroid/widget/TextView;
    .end local v2    # "layoutId":I
    :cond_0
    if-eqz p5, :cond_1

    .line 517
    sget v2, Lcom/google/android/play/R$layout;->play_drawer_primary_action_disabled:I

    .restart local v2    # "layoutId":I
    goto :goto_0

    .line 519
    .end local v2    # "layoutId":I
    :cond_1
    sget v2, Lcom/google/android/play/R$layout;->play_drawer_primary_action_regular:I

    .restart local v2    # "layoutId":I
    goto :goto_0

    .line 522
    :cond_2
    iget-object v4, p0, Lcom/google/android/play/drawer/PlayDrawerAdapter;->mInflater:Landroid/view/LayoutInflater;

    const/4 v5, 0x0

    invoke-virtual {v4, v2, p2, v5}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v4

    goto :goto_1

    .line 530
    .restart local v0    # "destinationRow":Landroid/widget/TextView;
    :cond_3
    iget v4, p3, Lcom/google/android/play/drawer/PlayDrawerLayout$PlayDrawerPrimaryAction;->iconResId:I

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    .restart local v1    # "drawable":Landroid/graphics/drawable/Drawable;
    goto :goto_2

    .line 535
    :cond_4
    const/16 v4, 0xff

    invoke-virtual {v1, v4}, Landroid/graphics/drawable/Drawable;->setAlpha(I)V

    goto :goto_3

    .line 539
    .end local v1    # "drawable":Landroid/graphics/drawable/Drawable;
    :cond_5
    invoke-virtual {v0, v6, v6, v6, v6}, Landroid/widget/TextView;->setCompoundDrawablesWithIntrinsicBounds(Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;)V

    goto :goto_4

    .line 544
    :cond_6
    if-eqz p5, :cond_7

    .line 545
    sget v4, Lcom/google/android/play/R$color;->play_disabled_grey:I

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getColor(I)I

    move-result v4

    invoke-virtual {v0, v4}, Landroid/widget/TextView;->setTextColor(I)V

    goto :goto_5

    .line 547
    :cond_7
    sget v4, Lcom/google/android/play/R$color;->play_fg_primary:I

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getColor(I)I

    move-result v4

    invoke-virtual {v0, v4}, Landroid/widget/TextView;->setTextColor(I)V

    goto :goto_5
.end method

.method private getPrimaryActionsTopSpacing(Landroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 4
    .param p1, "convertView"    # Landroid/view/View;
    .param p2, "parent"    # Landroid/view/ViewGroup;

    .prologue
    .line 504
    if-eqz p1, :cond_0

    move-object v0, p1

    .line 506
    .local v0, "spacingView":Landroid/view/View;
    :goto_0
    return-object v0

    .line 504
    .end local v0    # "spacingView":Landroid/view/View;
    :cond_0
    iget-object v1, p0, Lcom/google/android/play/drawer/PlayDrawerAdapter;->mInflater:Landroid/view/LayoutInflater;

    sget v2, Lcom/google/android/play/R$layout;->play_drawer_primary_actions_top_spacing:I

    const/4 v3, 0x0

    invoke-virtual {v1, v2, p2, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    goto :goto_0
.end method

.method private getProfileInfoView(Landroid/view/View;Landroid/view/ViewGroup;I)Landroid/view/View;
    .locals 8
    .param p1, "convertView"    # Landroid/view/View;
    .param p2, "parent"    # Landroid/view/ViewGroup;
    .param p3, "position"    # I

    .prologue
    const/4 v7, 0x1

    const/4 v6, 0x0

    .line 341
    if-eqz p1, :cond_0

    move-object v2, p1

    :goto_0
    check-cast v2, Lcom/google/android/play/drawer/PlayDrawerProfileInfoView;

    check-cast v2, Lcom/google/android/play/drawer/PlayDrawerProfileInfoView;

    iput-object v2, p0, Lcom/google/android/play/drawer/PlayDrawerAdapter;->mProfileInfoView:Lcom/google/android/play/drawer/PlayDrawerProfileInfoView;

    .line 343
    iput p3, p0, Lcom/google/android/play/drawer/PlayDrawerAdapter;->mProfileContainerPosition:I

    .line 347
    iget-object v0, p0, Lcom/google/android/play/drawer/PlayDrawerAdapter;->mCurrentAccount:Landroid/accounts/Account;

    .line 348
    .local v0, "finalCurrentAccount":Landroid/accounts/Account;
    iget-object v2, p0, Lcom/google/android/play/drawer/PlayDrawerAdapter;->mCurrentAccount:Landroid/accounts/Account;

    iget-object v1, v2, Landroid/accounts/Account;->name:Ljava/lang/String;

    .line 352
    .local v1, "finalCurrentAccountName":Ljava/lang/String;
    iget-object v2, p0, Lcom/google/android/play/drawer/PlayDrawerAdapter;->mProfileInfoView:Lcom/google/android/play/drawer/PlayDrawerProfileInfoView;

    iget-object v3, p0, Lcom/google/android/play/drawer/PlayDrawerAdapter;->mNonCurrentAccounts:[Landroid/accounts/Account;

    iget-object v4, p0, Lcom/google/android/play/drawer/PlayDrawerAdapter;->mAccountDocV2s:Ljava/util/Map;

    iget-object v5, p0, Lcom/google/android/play/drawer/PlayDrawerAdapter;->mBitmapLoader:Lcom/google/android/play/image/BitmapLoader;

    invoke-virtual {v2, v0, v3, v4, v5}, Lcom/google/android/play/drawer/PlayDrawerProfileInfoView;->configure(Landroid/accounts/Account;[Landroid/accounts/Account;Ljava/util/Map;Lcom/google/android/play/image/BitmapLoader;)V

    .line 359
    iget-object v2, p0, Lcom/google/android/play/drawer/PlayDrawerAdapter;->mPlayDfeApiProvider:Lcom/google/android/play/dfe/api/PlayDfeApiProvider;

    invoke-interface {v2, v0}, Lcom/google/android/play/dfe/api/PlayDfeApiProvider;->getPlayDfeApi(Landroid/accounts/Account;)Lcom/google/android/play/dfe/api/PlayDfeApi;

    move-result-object v2

    new-instance v3, Lcom/google/android/play/drawer/PlayDrawerAdapter$1;

    invoke-direct {v3, p0, v1, v0}, Lcom/google/android/play/drawer/PlayDrawerAdapter$1;-><init>(Lcom/google/android/play/drawer/PlayDrawerAdapter;Ljava/lang/String;Landroid/accounts/Account;)V

    new-instance v4, Lcom/google/android/play/drawer/PlayDrawerAdapter$2;

    invoke-direct {v4, p0}, Lcom/google/android/play/drawer/PlayDrawerAdapter$2;-><init>(Lcom/google/android/play/drawer/PlayDrawerAdapter;)V

    invoke-interface {v2, v3, v4, v7}, Lcom/google/android/play/dfe/api/PlayDfeApi;->getPlusProfile(Lcom/android/volley/Response$Listener;Lcom/android/volley/Response$ErrorListener;Z)Lcom/android/volley/Request;

    .line 395
    invoke-direct {p0}, Lcom/google/android/play/drawer/PlayDrawerAdapter;->loadAllSecondaryAccountDocV2sOnce()V

    .line 397
    iget-object v2, p0, Lcom/google/android/play/drawer/PlayDrawerAdapter;->mProfileInfoView:Lcom/google/android/play/drawer/PlayDrawerProfileInfoView;

    iget-boolean v3, p0, Lcom/google/android/play/drawer/PlayDrawerAdapter;->mAccountListExpanded:Z

    invoke-virtual {v2, v3}, Lcom/google/android/play/drawer/PlayDrawerProfileInfoView;->setAccountListExpanded(Z)V

    .line 399
    iget-object v2, p0, Lcom/google/android/play/drawer/PlayDrawerAdapter;->mProfileInfoView:Lcom/google/android/play/drawer/PlayDrawerProfileInfoView;

    new-instance v3, Lcom/google/android/play/drawer/PlayDrawerAdapter$3;

    invoke-direct {v3, p0}, Lcom/google/android/play/drawer/PlayDrawerAdapter$3;-><init>(Lcom/google/android/play/drawer/PlayDrawerAdapter;)V

    invoke-virtual {v2, v3}, Lcom/google/android/play/drawer/PlayDrawerProfileInfoView;->setOnAvatarClickListener(Lcom/google/android/play/drawer/PlayDrawerProfileInfoView$OnAvatarClickedListener;)V

    .line 419
    iget-object v2, p0, Lcom/google/android/play/drawer/PlayDrawerAdapter;->mNonCurrentAccounts:[Landroid/accounts/Account;

    array-length v2, v2

    if-lez v2, :cond_1

    .line 420
    iget-object v2, p0, Lcom/google/android/play/drawer/PlayDrawerAdapter;->mProfileInfoView:Lcom/google/android/play/drawer/PlayDrawerProfileInfoView;

    invoke-virtual {v2, v7}, Lcom/google/android/play/drawer/PlayDrawerProfileInfoView;->setAccountListEnabled(Z)V

    .line 423
    iget-object v2, p0, Lcom/google/android/play/drawer/PlayDrawerAdapter;->mProfileInfoView:Lcom/google/android/play/drawer/PlayDrawerProfileInfoView;

    new-instance v3, Lcom/google/android/play/drawer/PlayDrawerAdapter$4;

    invoke-direct {v3, p0}, Lcom/google/android/play/drawer/PlayDrawerAdapter$4;-><init>(Lcom/google/android/play/drawer/PlayDrawerAdapter;)V

    invoke-virtual {v2, v3}, Lcom/google/android/play/drawer/PlayDrawerProfileInfoView;->setAccountTogglerListener(Landroid/view/View$OnClickListener;)V

    .line 437
    :goto_1
    iget-object v2, p0, Lcom/google/android/play/drawer/PlayDrawerAdapter;->mProfileInfoView:Lcom/google/android/play/drawer/PlayDrawerProfileInfoView;

    return-object v2

    .line 341
    .end local v0    # "finalCurrentAccount":Landroid/accounts/Account;
    .end local v1    # "finalCurrentAccountName":Ljava/lang/String;
    :cond_0
    iget-object v2, p0, Lcom/google/android/play/drawer/PlayDrawerAdapter;->mInflater:Landroid/view/LayoutInflater;

    sget v3, Lcom/google/android/play/R$layout;->play_drawer_profile_info:I

    invoke-virtual {v2, v3, p2, v6}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v2

    goto :goto_0

    .line 433
    .restart local v0    # "finalCurrentAccount":Landroid/accounts/Account;
    .restart local v1    # "finalCurrentAccountName":Ljava/lang/String;
    :cond_1
    iget-object v2, p0, Lcom/google/android/play/drawer/PlayDrawerAdapter;->mProfileInfoView:Lcom/google/android/play/drawer/PlayDrawerProfileInfoView;

    invoke-virtual {v2, v6}, Lcom/google/android/play/drawer/PlayDrawerProfileInfoView;->setAccountListEnabled(Z)V

    .line 434
    iget-object v2, p0, Lcom/google/android/play/drawer/PlayDrawerAdapter;->mProfileInfoView:Lcom/google/android/play/drawer/PlayDrawerProfileInfoView;

    const/4 v3, 0x0

    invoke-virtual {v2, v3}, Lcom/google/android/play/drawer/PlayDrawerProfileInfoView;->setAccountTogglerListener(Landroid/view/View$OnClickListener;)V

    goto :goto_1
.end method

.method private getSecondaryActionView(Landroid/view/View;Landroid/view/ViewGroup;Lcom/google/android/play/drawer/PlayDrawerLayout$PlayDrawerSecondaryAction;)Landroid/view/View;
    .locals 4
    .param p1, "convertView"    # Landroid/view/View;
    .param p2, "parent"    # Landroid/view/ViewGroup;
    .param p3, "secondaryAction"    # Lcom/google/android/play/drawer/PlayDrawerLayout$PlayDrawerSecondaryAction;

    .prologue
    .line 570
    if-eqz p1, :cond_0

    move-object v1, p1

    :goto_0
    check-cast v1, Landroid/widget/TextView;

    move-object v0, v1

    check-cast v0, Landroid/widget/TextView;

    .line 574
    .local v0, "secondaryRow":Landroid/widget/TextView;
    iget-object v1, p3, Lcom/google/android/play/drawer/PlayDrawerLayout$PlayDrawerSecondaryAction;->actionText:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 576
    new-instance v1, Lcom/google/android/play/drawer/PlayDrawerAdapter$9;

    invoke-direct {v1, p0, p3}, Lcom/google/android/play/drawer/PlayDrawerAdapter$9;-><init>(Lcom/google/android/play/drawer/PlayDrawerAdapter;Lcom/google/android/play/drawer/PlayDrawerLayout$PlayDrawerSecondaryAction;)V

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 584
    iget-object v1, p0, Lcom/google/android/play/drawer/PlayDrawerAdapter;->mContext:Landroid/content/Context;

    invoke-static {v1}, Lcom/google/android/play/utils/PlayUtils;->useLtr(Landroid/content/Context;)Z

    move-result v1

    invoke-static {v0, v1}, Lcom/google/android/play/drawer/PlayDrawerAdapter;->setTextAlignmentStart(Landroid/widget/TextView;Z)V

    .line 585
    return-object v0

    .line 570
    .end local v0    # "secondaryRow":Landroid/widget/TextView;
    :cond_0
    iget-object v1, p0, Lcom/google/android/play/drawer/PlayDrawerAdapter;->mInflater:Landroid/view/LayoutInflater;

    sget v2, Lcom/google/android/play/R$layout;->play_drawer_secondary_action:I

    const/4 v3, 0x0

    invoke-virtual {v1, v2, p2, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    goto :goto_0
.end method

.method private getSecondaryActionsTopSeparator(Landroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 4
    .param p1, "convertView"    # Landroid/view/View;
    .param p2, "parent"    # Landroid/view/ViewGroup;

    .prologue
    .line 562
    if-eqz p1, :cond_0

    move-object v0, p1

    .line 565
    .local v0, "separatorView":Landroid/view/View;
    :goto_0
    return-object v0

    .line 562
    .end local v0    # "separatorView":Landroid/view/View;
    :cond_0
    iget-object v1, p0, Lcom/google/android/play/drawer/PlayDrawerAdapter;->mInflater:Landroid/view/LayoutInflater;

    sget v2, Lcom/google/android/play/R$layout;->play_drawer_secondary_actions_top_separator:I

    const/4 v3, 0x0

    invoke-virtual {v1, v2, p2, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    goto :goto_0
.end method

.method private isProfileContainerVisible()Z
    .locals 4

    .prologue
    .line 622
    iget-object v3, p0, Lcom/google/android/play/drawer/PlayDrawerAdapter;->mListView:Landroid/widget/ListView;

    invoke-virtual {v3}, Landroid/widget/ListView;->getFirstVisiblePosition()I

    move-result v0

    .line 623
    .local v0, "firstVisiblePosition":I
    iget-object v3, p0, Lcom/google/android/play/drawer/PlayDrawerAdapter;->mListView:Landroid/widget/ListView;

    invoke-virtual {v3}, Landroid/widget/ListView;->getChildCount()I

    move-result v3

    add-int/2addr v3, v0

    add-int/lit8 v2, v3, -0x1

    .line 624
    .local v2, "lastVisiblePosition":I
    iget v3, p0, Lcom/google/android/play/drawer/PlayDrawerAdapter;->mProfileContainerPosition:I

    if-lt v3, v0, :cond_0

    iget v3, p0, Lcom/google/android/play/drawer/PlayDrawerAdapter;->mProfileContainerPosition:I

    if-gt v3, v2, :cond_0

    const/4 v1, 0x1

    .line 626
    .local v1, "isProfileContainerVisible":Z
    :goto_0
    return v1

    .line 624
    .end local v1    # "isProfileContainerVisible":Z
    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method private loadAllSecondaryAccountDocV2sOnce()V
    .locals 7

    .prologue
    .line 441
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_0
    iget-object v3, p0, Lcom/google/android/play/drawer/PlayDrawerAdapter;->mNonCurrentAccounts:[Landroid/accounts/Account;

    array-length v3, v3

    if-ge v2, v3, :cond_1

    .line 442
    iget-object v3, p0, Lcom/google/android/play/drawer/PlayDrawerAdapter;->mNonCurrentAccounts:[Landroid/accounts/Account;

    aget-object v0, v3, v2

    .line 443
    .local v0, "account":Landroid/accounts/Account;
    iget-object v1, v0, Landroid/accounts/Account;->name:Ljava/lang/String;

    .line 444
    .local v1, "accountName":Ljava/lang/String;
    iget-object v3, p0, Lcom/google/android/play/drawer/PlayDrawerAdapter;->mIsAccountDocLoaded:Ljava/util/Set;

    invoke-interface {v3, v1}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 441
    :goto_1
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 448
    :cond_0
    iget-object v3, p0, Lcom/google/android/play/drawer/PlayDrawerAdapter;->mPlayDfeApiProvider:Lcom/google/android/play/dfe/api/PlayDfeApiProvider;

    invoke-interface {v3, v0}, Lcom/google/android/play/dfe/api/PlayDfeApiProvider;->getPlayDfeApi(Landroid/accounts/Account;)Lcom/google/android/play/dfe/api/PlayDfeApi;

    move-result-object v3

    new-instance v4, Lcom/google/android/play/drawer/PlayDrawerAdapter$5;

    invoke-direct {v4, p0, v1}, Lcom/google/android/play/drawer/PlayDrawerAdapter$5;-><init>(Lcom/google/android/play/drawer/PlayDrawerAdapter;Ljava/lang/String;)V

    new-instance v5, Lcom/google/android/play/drawer/PlayDrawerAdapter$6;

    invoke-direct {v5, p0}, Lcom/google/android/play/drawer/PlayDrawerAdapter$6;-><init>(Lcom/google/android/play/drawer/PlayDrawerAdapter;)V

    const/4 v6, 0x1

    invoke-interface {v3, v4, v5, v6}, Lcom/google/android/play/dfe/api/PlayDfeApi;->getPlusProfile(Lcom/android/volley/Response$Listener;Lcom/android/volley/Response$ErrorListener;Z)Lcom/android/volley/Request;

    goto :goto_1

    .line 480
    .end local v0    # "account":Landroid/accounts/Account;
    .end local v1    # "accountName":Ljava/lang/String;
    :cond_1
    return-void
.end method

.method public static setDrawableStart(Landroid/widget/TextView;Landroid/graphics/drawable/Drawable;Z)V
    .locals 1
    .param p0, "textView"    # Landroid/widget/TextView;
    .param p1, "drawable"    # Landroid/graphics/drawable/Drawable;
    .param p2, "useLtr"    # Z

    .prologue
    const/4 v0, 0x0

    .line 640
    if-eqz p2, :cond_0

    .line 641
    invoke-virtual {p0, p1, v0, v0, v0}, Landroid/widget/TextView;->setCompoundDrawablesWithIntrinsicBounds(Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;)V

    .line 645
    :goto_0
    return-void

    .line 643
    :cond_0
    invoke-virtual {p0, v0, v0, p1, v0}, Landroid/widget/TextView;->setCompoundDrawablesWithIntrinsicBounds(Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;)V

    goto :goto_0
.end method

.method public static setTextAlignmentStart(Landroid/widget/TextView;Z)V
    .locals 1
    .param p0, "textView"    # Landroid/widget/TextView;
    .param p1, "useLtr"    # Z

    .prologue
    .line 656
    if-eqz p1, :cond_0

    .line 657
    const/16 v0, 0x13

    invoke-virtual {p0, v0}, Landroid/widget/TextView;->setGravity(I)V

    .line 661
    :goto_0
    return-void

    .line 659
    :cond_0
    const/16 v0, 0x15

    invoke-virtual {p0, v0}, Landroid/widget/TextView;->setGravity(I)V

    goto :goto_0
.end method

.method private toggleAccountsList()V
    .locals 1

    .prologue
    .line 483
    iget-boolean v0, p0, Lcom/google/android/play/drawer/PlayDrawerAdapter;->mAccountListExpanded:Z

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    iput-boolean v0, p0, Lcom/google/android/play/drawer/PlayDrawerAdapter;->mAccountListExpanded:Z

    .line 484
    invoke-virtual {p0}, Lcom/google/android/play/drawer/PlayDrawerAdapter;->notifyDataSetChanged()V

    .line 485
    return-void

    .line 483
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public areAllItemsEnabled()Z
    .locals 1

    .prologue
    .line 289
    const/4 v0, 0x0

    return v0
.end method

.method public collapseAccountListIfNeeded()V
    .locals 1

    .prologue
    .line 156
    iget-object v0, p0, Lcom/google/android/play/drawer/PlayDrawerAdapter;->mNonCurrentAccounts:[Landroid/accounts/Account;

    array-length v0, v0

    if-lez v0, :cond_0

    iget-boolean v0, p0, Lcom/google/android/play/drawer/PlayDrawerAdapter;->mAccountListExpanded:Z

    if-eqz v0, :cond_0

    .line 157
    invoke-direct {p0}, Lcom/google/android/play/drawer/PlayDrawerAdapter;->toggleAccountsList()V

    .line 159
    :cond_0
    return-void
.end method

.method public getCount()I
    .locals 2

    .prologue
    .line 164
    iget-boolean v1, p0, Lcom/google/android/play/drawer/PlayDrawerAdapter;->mHasAccounts:Z

    if-eqz v1, :cond_1

    const/4 v0, 0x1

    .line 166
    .local v0, "result":I
    :goto_0
    iget-boolean v1, p0, Lcom/google/android/play/drawer/PlayDrawerAdapter;->mAccountListExpanded:Z

    if-nez v1, :cond_2

    .line 168
    add-int/lit8 v0, v0, 0x1

    .line 170
    iget-object v1, p0, Lcom/google/android/play/drawer/PlayDrawerAdapter;->mPrimaryActions:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    add-int/2addr v0, v1

    .line 172
    add-int/lit8 v0, v0, 0x1

    .line 174
    iget-boolean v1, p0, Lcom/google/android/play/drawer/PlayDrawerAdapter;->mShowDownloadOnlyToggle:Z

    if-eqz v1, :cond_0

    .line 175
    add-int/lit8 v0, v0, 0x1

    .line 178
    :cond_0
    iget-object v1, p0, Lcom/google/android/play/drawer/PlayDrawerAdapter;->mSecondaryActions:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    add-int/2addr v0, v1

    .line 183
    :goto_1
    return v0

    .line 164
    .end local v0    # "result":I
    :cond_1
    const/4 v0, 0x0

    goto :goto_0

    .line 180
    .restart local v0    # "result":I
    :cond_2
    iget-object v1, p0, Lcom/google/android/play/drawer/PlayDrawerAdapter;->mNonCurrentAccounts:[Landroid/accounts/Account;

    array-length v1, v1

    add-int/2addr v0, v1

    goto :goto_1
.end method

.method public getItem(I)Ljava/lang/Object;
    .locals 3
    .param p1, "position"    # I

    .prologue
    const/4 v1, 0x0

    .line 188
    iget-boolean v2, p0, Lcom/google/android/play/drawer/PlayDrawerAdapter;->mHasAccounts:Z

    if-eqz v2, :cond_2

    .line 189
    if-nez p1, :cond_1

    .line 190
    iget-object v1, p0, Lcom/google/android/play/drawer/PlayDrawerAdapter;->mCurrentAccount:Landroid/accounts/Account;

    iget-object v1, v1, Landroid/accounts/Account;->name:Ljava/lang/String;

    .line 224
    :cond_0
    :goto_0
    return-object v1

    .line 192
    :cond_1
    add-int/lit8 p1, p1, -0x1

    .line 195
    :cond_2
    iget-boolean v2, p0, Lcom/google/android/play/drawer/PlayDrawerAdapter;->mAccountListExpanded:Z

    if-nez v2, :cond_6

    .line 197
    if-eqz p1, :cond_0

    .line 200
    add-int/lit8 p1, p1, -0x1

    .line 203
    iget-object v2, p0, Lcom/google/android/play/drawer/PlayDrawerAdapter;->mPrimaryActions:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v0

    .line 204
    .local v0, "primaryActionsCount":I
    if-ge p1, v0, :cond_3

    .line 205
    iget-object v1, p0, Lcom/google/android/play/drawer/PlayDrawerAdapter;->mPrimaryActions:Ljava/util/List;

    invoke-interface {v1, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    goto :goto_0

    .line 207
    :cond_3
    sub-int/2addr p1, v0

    .line 209
    iget-boolean v2, p0, Lcom/google/android/play/drawer/PlayDrawerAdapter;->mShowDownloadOnlyToggle:Z

    if-eqz v2, :cond_5

    .line 210
    if-nez p1, :cond_4

    .line 211
    iget-object v1, p0, Lcom/google/android/play/drawer/PlayDrawerAdapter;->mDownloadSwitchConfig:Lcom/google/android/play/drawer/PlayDrawerLayout$PlayDrawerDownloadSwitchConfig;

    goto :goto_0

    .line 213
    :cond_4
    add-int/lit8 p1, p1, -0x1

    .line 217
    :cond_5
    if-eqz p1, :cond_0

    .line 220
    add-int/lit8 p1, p1, -0x1

    .line 222
    iget-object v1, p0, Lcom/google/android/play/drawer/PlayDrawerAdapter;->mSecondaryActions:Ljava/util/List;

    invoke-interface {v1, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    goto :goto_0

    .line 224
    .end local v0    # "primaryActionsCount":I
    :cond_6
    iget-object v1, p0, Lcom/google/android/play/drawer/PlayDrawerAdapter;->mNonCurrentAccounts:[Landroid/accounts/Account;

    aget-object v1, v1, p1

    goto :goto_0
.end method

.method public getItemId(I)J
    .locals 2
    .param p1, "position"    # I

    .prologue
    .line 230
    int-to-long v0, p1

    return-wide v0
.end method

.method public getItemViewType(I)I
    .locals 3
    .param p1, "position"    # I

    .prologue
    .line 235
    iget-boolean v2, p0, Lcom/google/android/play/drawer/PlayDrawerAdapter;->mHasAccounts:Z

    if-eqz v2, :cond_1

    .line 236
    if-nez p1, :cond_0

    .line 237
    const/4 v2, 0x0

    .line 278
    :goto_0
    return v2

    .line 239
    :cond_0
    add-int/lit8 p1, p1, -0x1

    .line 242
    :cond_1
    iget-boolean v2, p0, Lcom/google/android/play/drawer/PlayDrawerAdapter;->mAccountListExpanded:Z

    if-nez v2, :cond_9

    .line 244
    if-nez p1, :cond_2

    .line 245
    const/4 v2, 0x2

    goto :goto_0

    .line 247
    :cond_2
    add-int/lit8 p1, p1, -0x1

    .line 249
    iget-object v2, p0, Lcom/google/android/play/drawer/PlayDrawerAdapter;->mPrimaryActions:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v1

    .line 250
    .local v1, "primaryActionsCount":I
    if-ge p1, v1, :cond_5

    .line 251
    iget-object v2, p0, Lcom/google/android/play/drawer/PlayDrawerAdapter;->mPrimaryActions:Ljava/util/List;

    invoke-interface {v2, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/play/drawer/PlayDrawerLayout$PlayDrawerPrimaryAction;

    .line 252
    .local v0, "action":Lcom/google/android/play/drawer/PlayDrawerLayout$PlayDrawerPrimaryAction;
    iget-boolean v2, p0, Lcom/google/android/play/drawer/PlayDrawerAdapter;->mShowDownloadOnlyToggle:Z

    if-eqz v2, :cond_3

    iget-boolean v2, p0, Lcom/google/android/play/drawer/PlayDrawerAdapter;->mDownloadOnlyEnabled:Z

    if-eqz v2, :cond_3

    iget-boolean v2, v0, Lcom/google/android/play/drawer/PlayDrawerLayout$PlayDrawerPrimaryAction;->isAvailableInDownloadOnly:Z

    if-nez v2, :cond_3

    .line 254
    const/4 v2, 0x5

    goto :goto_0

    .line 255
    :cond_3
    iget-boolean v2, v0, Lcom/google/android/play/drawer/PlayDrawerLayout$PlayDrawerPrimaryAction;->isActive:Z

    if-eqz v2, :cond_4

    .line 256
    const/4 v2, 0x3

    goto :goto_0

    .line 258
    :cond_4
    const/4 v2, 0x4

    goto :goto_0

    .line 261
    .end local v0    # "action":Lcom/google/android/play/drawer/PlayDrawerLayout$PlayDrawerPrimaryAction;
    :cond_5
    iget-object v2, p0, Lcom/google/android/play/drawer/PlayDrawerAdapter;->mPrimaryActions:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v2

    sub-int/2addr p1, v2

    .line 263
    if-nez p1, :cond_6

    .line 264
    const/4 v2, 0x6

    goto :goto_0

    .line 266
    :cond_6
    add-int/lit8 p1, p1, -0x1

    .line 268
    iget-boolean v2, p0, Lcom/google/android/play/drawer/PlayDrawerAdapter;->mShowDownloadOnlyToggle:Z

    if-eqz v2, :cond_8

    .line 269
    if-nez p1, :cond_7

    .line 271
    const/16 v2, 0x8

    goto :goto_0

    .line 273
    :cond_7
    add-int/lit8 p1, p1, -0x1

    .line 276
    :cond_8
    const/4 v2, 0x7

    goto :goto_0

    .line 278
    .end local v1    # "primaryActionsCount":I
    :cond_9
    const/4 v2, 0x1

    goto :goto_0
.end method

.method public getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 8
    .param p1, "position"    # I
    .param p2, "convertView"    # Landroid/view/View;
    .param p3, "parent"    # Landroid/view/ViewGroup;

    .prologue
    .line 309
    invoke-virtual {p0, p1}, Lcom/google/android/play/drawer/PlayDrawerAdapter;->getItemViewType(I)I

    move-result v7

    .line 310
    .local v7, "viewType":I
    invoke-virtual {p0, p1}, Lcom/google/android/play/drawer/PlayDrawerAdapter;->getItem(I)Ljava/lang/Object;

    move-result-object v6

    .line 312
    .local v6, "data":Ljava/lang/Object;
    packed-switch v7, :pswitch_data_0

    .line 336
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "View type "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " not supported"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 314
    :pswitch_0
    invoke-direct {p0, p2, p3, p1}, Lcom/google/android/play/drawer/PlayDrawerAdapter;->getProfileInfoView(Landroid/view/View;Landroid/view/ViewGroup;I)Landroid/view/View;

    move-result-object v0

    .line 333
    .end local v6    # "data":Ljava/lang/Object;
    :goto_0
    return-object v0

    .line 316
    .restart local v6    # "data":Ljava/lang/Object;
    :pswitch_1
    check-cast v6, Landroid/accounts/Account;

    .end local v6    # "data":Ljava/lang/Object;
    invoke-direct {p0, p2, p3, v6}, Lcom/google/android/play/drawer/PlayDrawerAdapter;->getAccountView(Landroid/view/View;Landroid/view/ViewGroup;Landroid/accounts/Account;)Landroid/view/View;

    move-result-object v0

    goto :goto_0

    .line 318
    .restart local v6    # "data":Ljava/lang/Object;
    :pswitch_2
    invoke-direct {p0, p2, p3}, Lcom/google/android/play/drawer/PlayDrawerAdapter;->getPrimaryActionsTopSpacing(Landroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    goto :goto_0

    :pswitch_3
    move-object v3, v6

    .line 320
    check-cast v3, Lcom/google/android/play/drawer/PlayDrawerLayout$PlayDrawerPrimaryAction;

    const/4 v4, 0x1

    const/4 v5, 0x0

    move-object v0, p0

    move-object v1, p2

    move-object v2, p3

    invoke-direct/range {v0 .. v5}, Lcom/google/android/play/drawer/PlayDrawerAdapter;->getPrimaryActionView(Landroid/view/View;Landroid/view/ViewGroup;Lcom/google/android/play/drawer/PlayDrawerLayout$PlayDrawerPrimaryAction;ZZ)Landroid/view/View;

    move-result-object v0

    goto :goto_0

    :pswitch_4
    move-object v3, v6

    .line 323
    check-cast v3, Lcom/google/android/play/drawer/PlayDrawerLayout$PlayDrawerPrimaryAction;

    const/4 v4, 0x0

    const/4 v5, 0x0

    move-object v0, p0

    move-object v1, p2

    move-object v2, p3

    invoke-direct/range {v0 .. v5}, Lcom/google/android/play/drawer/PlayDrawerAdapter;->getPrimaryActionView(Landroid/view/View;Landroid/view/ViewGroup;Lcom/google/android/play/drawer/PlayDrawerLayout$PlayDrawerPrimaryAction;ZZ)Landroid/view/View;

    move-result-object v0

    goto :goto_0

    :pswitch_5
    move-object v3, v6

    .line 326
    check-cast v3, Lcom/google/android/play/drawer/PlayDrawerLayout$PlayDrawerPrimaryAction;

    const/4 v4, 0x0

    const/4 v5, 0x1

    move-object v0, p0

    move-object v1, p2

    move-object v2, p3

    invoke-direct/range {v0 .. v5}, Lcom/google/android/play/drawer/PlayDrawerAdapter;->getPrimaryActionView(Landroid/view/View;Landroid/view/ViewGroup;Lcom/google/android/play/drawer/PlayDrawerLayout$PlayDrawerPrimaryAction;ZZ)Landroid/view/View;

    move-result-object v0

    goto :goto_0

    .line 329
    :pswitch_6
    invoke-direct {p0, p2, p3}, Lcom/google/android/play/drawer/PlayDrawerAdapter;->getSecondaryActionsTopSeparator(Landroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    goto :goto_0

    .line 331
    :pswitch_7
    invoke-direct {p0, p2, p3}, Lcom/google/android/play/drawer/PlayDrawerAdapter;->getDownloadToggleView(Landroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    goto :goto_0

    .line 333
    :pswitch_8
    check-cast v6, Lcom/google/android/play/drawer/PlayDrawerLayout$PlayDrawerSecondaryAction;

    .end local v6    # "data":Ljava/lang/Object;
    invoke-direct {p0, p2, p3, v6}, Lcom/google/android/play/drawer/PlayDrawerAdapter;->getSecondaryActionView(Landroid/view/View;Landroid/view/ViewGroup;Lcom/google/android/play/drawer/PlayDrawerLayout$PlayDrawerSecondaryAction;)Landroid/view/View;

    move-result-object v0

    goto :goto_0

    .line 312
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_8
        :pswitch_7
    .end packed-switch
.end method

.method public getViewTypeCount()I
    .locals 1

    .prologue
    .line 284
    const/16 v0, 0x9

    return v0
.end method

.method public hasStableIds()Z
    .locals 1

    .prologue
    .line 617
    const/4 v0, 0x0

    return v0
.end method

.method public isAccountListExpanded()Z
    .locals 1

    .prologue
    .line 104
    iget-boolean v0, p0, Lcom/google/android/play/drawer/PlayDrawerAdapter;->mAccountListExpanded:Z

    return v0
.end method

.method public isEnabled(I)Z
    .locals 2
    .param p1, "position"    # I

    .prologue
    .line 296
    invoke-virtual {p0, p1}, Lcom/google/android/play/drawer/PlayDrawerAdapter;->getItemViewType(I)I

    move-result v0

    .line 298
    .local v0, "viewType":I
    sparse-switch v0, :sswitch_data_0

    .line 303
    const/4 v1, 0x1

    :goto_0
    return v1

    .line 301
    :sswitch_0
    const/4 v1, 0x0

    goto :goto_0

    .line 298
    nop

    :sswitch_data_0
    .sparse-switch
        0x2 -> :sswitch_0
        0x6 -> :sswitch_0
    .end sparse-switch
.end method

.method public updateContent(Ljava/lang/String;[Landroid/accounts/Account;Ljava/util/List;Lcom/google/android/play/drawer/PlayDrawerLayout$PlayDrawerDownloadSwitchConfig;Ljava/util/List;)V
    .locals 9
    .param p1, "currentAccountName"    # Ljava/lang/String;
    .param p2, "accounts"    # [Landroid/accounts/Account;
    .param p4, "downloadSwitchConfig"    # Lcom/google/android/play/drawer/PlayDrawerLayout$PlayDrawerDownloadSwitchConfig;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "[",
            "Landroid/accounts/Account;",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/play/drawer/PlayDrawerLayout$PlayDrawerPrimaryAction;",
            ">;",
            "Lcom/google/android/play/drawer/PlayDrawerLayout$PlayDrawerDownloadSwitchConfig;",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/play/drawer/PlayDrawerLayout$PlayDrawerSecondaryAction;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 112
    .local p3, "primaryActions":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/play/drawer/PlayDrawerLayout$PlayDrawerPrimaryAction;>;"
    .local p5, "secondaryActions":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/play/drawer/PlayDrawerLayout$PlayDrawerSecondaryAction;>;"
    array-length v6, p2

    .line 113
    .local v6, "numAccounts":I
    if-nez v6, :cond_1

    .line 114
    const/4 v7, 0x0

    iput-object v7, p0, Lcom/google/android/play/drawer/PlayDrawerAdapter;->mCurrentAccount:Landroid/accounts/Account;

    .line 115
    const/4 v7, 0x0

    new-array v7, v7, [Landroid/accounts/Account;

    iput-object v7, p0, Lcom/google/android/play/drawer/PlayDrawerAdapter;->mNonCurrentAccounts:[Landroid/accounts/Account;

    .line 136
    :cond_0
    :goto_0
    if-lez v6, :cond_4

    const/4 v7, 0x1

    :goto_1
    iput-boolean v7, p0, Lcom/google/android/play/drawer/PlayDrawerAdapter;->mHasAccounts:Z

    .line 139
    iget-object v7, p0, Lcom/google/android/play/drawer/PlayDrawerAdapter;->mPrimaryActions:Ljava/util/List;

    invoke-interface {v7}, Ljava/util/List;->clear()V

    .line 140
    iget-object v7, p0, Lcom/google/android/play/drawer/PlayDrawerAdapter;->mSecondaryActions:Ljava/util/List;

    invoke-interface {v7}, Ljava/util/List;->clear()V

    .line 141
    iget-object v7, p0, Lcom/google/android/play/drawer/PlayDrawerAdapter;->mPrimaryActions:Ljava/util/List;

    invoke-interface {v7, p3}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    .line 142
    iget-object v7, p0, Lcom/google/android/play/drawer/PlayDrawerAdapter;->mSecondaryActions:Ljava/util/List;

    invoke-interface {v7, p5}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    .line 144
    iput-object p4, p0, Lcom/google/android/play/drawer/PlayDrawerAdapter;->mDownloadSwitchConfig:Lcom/google/android/play/drawer/PlayDrawerLayout$PlayDrawerDownloadSwitchConfig;

    .line 145
    if-eqz p4, :cond_5

    const/4 v7, 0x1

    :goto_2
    iput-boolean v7, p0, Lcom/google/android/play/drawer/PlayDrawerAdapter;->mShowDownloadOnlyToggle:Z

    .line 146
    iget-boolean v7, p0, Lcom/google/android/play/drawer/PlayDrawerAdapter;->mShowDownloadOnlyToggle:Z

    if-eqz v7, :cond_6

    iget-boolean v7, p4, Lcom/google/android/play/drawer/PlayDrawerLayout$PlayDrawerDownloadSwitchConfig;->isChecked:Z

    :goto_3
    iput-boolean v7, p0, Lcom/google/android/play/drawer/PlayDrawerAdapter;->mDownloadOnlyEnabled:Z

    .line 148
    invoke-virtual {p0}, Lcom/google/android/play/drawer/PlayDrawerAdapter;->notifyDataSetChanged()V

    .line 149
    return-void

    .line 118
    :cond_1
    add-int/lit8 v7, v6, -0x1

    new-array v7, v7, [Landroid/accounts/Account;

    iput-object v7, p0, Lcom/google/android/play/drawer/PlayDrawerAdapter;->mNonCurrentAccounts:[Landroid/accounts/Account;

    .line 119
    const/4 v4, 0x0

    .line 120
    .local v4, "nonCurrentIndex":I
    move-object v1, p2

    .local v1, "arr$":[Landroid/accounts/Account;
    array-length v3, v1

    .local v3, "len$":I
    const/4 v2, 0x0

    .local v2, "i$":I
    move v5, v4

    .end local v4    # "nonCurrentIndex":I
    .local v5, "nonCurrentIndex":I
    :goto_4
    if-ge v2, v3, :cond_0

    aget-object v0, v1, v2

    .line 121
    .local v0, "account":Landroid/accounts/Account;
    iget-object v7, v0, Landroid/accounts/Account;->name:Ljava/lang/String;

    invoke-virtual {p1, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_2

    .line 122
    iput-object v0, p0, Lcom/google/android/play/drawer/PlayDrawerAdapter;->mCurrentAccount:Landroid/accounts/Account;

    move v4, v5

    .line 120
    .end local v5    # "nonCurrentIndex":I
    .restart local v4    # "nonCurrentIndex":I
    :goto_5
    add-int/lit8 v2, v2, 0x1

    move v5, v4

    .end local v4    # "nonCurrentIndex":I
    .restart local v5    # "nonCurrentIndex":I
    goto :goto_4

    .line 125
    :cond_2
    add-int/lit8 v7, v6, -0x1

    if-ne v5, v7, :cond_3

    .line 127
    const-string v7, "current account not found in accounts"

    const/4 v8, 0x0

    new-array v8, v8, [Ljava/lang/Object;

    invoke-static {v7, v8}, Lcom/google/android/play/utils/PlayCommonLog;->wtf(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 129
    iput-object v0, p0, Lcom/google/android/play/drawer/PlayDrawerAdapter;->mCurrentAccount:Landroid/accounts/Account;

    goto :goto_0

    .line 132
    :cond_3
    iget-object v7, p0, Lcom/google/android/play/drawer/PlayDrawerAdapter;->mNonCurrentAccounts:[Landroid/accounts/Account;

    add-int/lit8 v4, v5, 0x1

    .end local v5    # "nonCurrentIndex":I
    .restart local v4    # "nonCurrentIndex":I
    aput-object v0, v7, v5

    goto :goto_5

    .line 136
    .end local v0    # "account":Landroid/accounts/Account;
    .end local v1    # "arr$":[Landroid/accounts/Account;
    .end local v2    # "i$":I
    .end local v3    # "len$":I
    .end local v4    # "nonCurrentIndex":I
    :cond_4
    const/4 v7, 0x0

    goto :goto_1

    .line 145
    :cond_5
    const/4 v7, 0x0

    goto :goto_2

    .line 146
    :cond_6
    const/4 v7, 0x0

    goto :goto_3
.end method
