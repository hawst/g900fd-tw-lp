.class public final Lcom/google/android/play/R$dimen;
.super Ljava/lang/Object;
.source "R.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/play/R;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "dimen"
.end annotation


# static fields
.field public static final abc_action_bar_default_height_material:I = 0x7f0b0031

.field public static final play_avatar_decoration_threshold_max:I = 0x7f0b0073

.field public static final play_avatar_decoration_threshold_min:I = 0x7f0b0072

.field public static final play_avatar_drop_shadow_max:I = 0x7f0b0077

.field public static final play_avatar_drop_shadow_min:I = 0x7f0b0076

.field public static final play_avatar_noring_outline:I = 0x7f0b0078

.field public static final play_avatar_ring_size_max:I = 0x7f0b0075

.field public static final play_avatar_ring_size_min:I = 0x7f0b0074

.field public static final play_card_default_inset:I = 0x7f0b0092

.field public static final play_card_extra_vspace:I = 0x7f0b0057

.field public static final play_card_label_icon_gap:I = 0x7f0b0066

.field public static final play_card_label_texts_gap:I = 0x7f0b0067

.field public static final play_card_overflow_touch_extend:I = 0x7f0b005a

.field public static final play_card_snippet_avatar_large_size:I = 0x7f0b005e

.field public static final play_card_snippet_avatar_size:I = 0x7f0b005d

.field public static final play_card_snippet_text_extra_margin_left:I = 0x7f0b0062

.field public static final play_drawer_max_width:I = 0x7f0b006f

.field public static final play_hairline_separator_thickness:I = 0x7f0b0052

.field public static final play_header_list_banner_height:I = 0x7f0b007a

.field public static final play_header_list_floating_elevation:I = 0x7f0b0080

.field public static final play_header_list_tab_floating_padding:I = 0x7f0b0082

.field public static final play_header_list_tab_strip_height:I = 0x7f0b007b

.field public static final play_header_list_tab_strip_selected_underline_height:I = 0x7f0b007c

.field public static final play_medium_size:I = 0x7f0b00a6

.field public static final play_mini_card_content_height:I = 0x7f0b0064

.field public static final play_mini_card_label_threshold:I = 0x7f0b0065

.field public static final play_search_one_suggestion_height:I = 0x7f0b009f

.field public static final play_small_card_content_min_height:I = 0x7f0b0063

.field public static final play_snippet_large_size:I = 0x7f0b00ad

.field public static final play_snippet_regular_size:I = 0x7f0b00ac

.field public static final play_star_height_default:I = 0x7f0b008f

.field public static final play_tab_strip_selected_underline_height:I = 0x7f0b004d

.field public static final play_tab_strip_title_offset:I = 0x7f0b0050

.field public static final play_text_view_fadeout:I = 0x7f0b0053

.field public static final play_text_view_fadeout_hint_margin:I = 0x7f0b0054

.field public static final play_text_view_outline:I = 0x7f0b0055
