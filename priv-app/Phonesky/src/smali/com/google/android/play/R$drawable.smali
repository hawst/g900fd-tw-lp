.class public final Lcom/google/android/play/R$drawable;
.super Ljava/lang/Object;
.source "R.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/play/R;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "drawable"
.end annotation


# static fields
.field public static final bg_default_profile_art:I = 0x7f02003d

.field public static final drawer_shadow:I = 0x7f02007f

.field public static final ic_down_white_16:I = 0x7f0200a4

.field public static final ic_mic_dark:I = 0x7f0200fd

.field public static final ic_profile_none:I = 0x7f020132

.field public static final ic_up_white_16:I = 0x7f020140

.field public static final play_action_button_apps:I = 0x7f020157

.field public static final play_action_button_apps_secondary:I = 0x7f020159

.field public static final play_action_button_books:I = 0x7f02015c

.field public static final play_action_button_books_secondary:I = 0x7f02015e

.field public static final play_action_button_movies:I = 0x7f020161

.field public static final play_action_button_movies_secondary:I = 0x7f020163

.field public static final play_action_button_multi:I = 0x7f020166

.field public static final play_action_button_multi_secondary:I = 0x7f020168

.field public static final play_action_button_music:I = 0x7f02016b

.field public static final play_action_button_music_secondary:I = 0x7f02016d

.field public static final play_action_button_newsstand:I = 0x7f020170

.field public static final play_action_button_newsstand_secondary:I = 0x7f020172

.field public static final play_action_button_secondary:I = 0x7f020175

.field public static final play_header_list_tab_high_contrast_bg:I = 0x7f020179

.field public static final play_highlight_overlay_dark:I = 0x7f02017e

.field public static final play_ic_clear:I = 0x7f020185
