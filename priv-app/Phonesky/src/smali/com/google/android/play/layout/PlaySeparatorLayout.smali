.class public Lcom/google/android/play/layout/PlaySeparatorLayout;
.super Landroid/widget/RelativeLayout;
.source "PlaySeparatorLayout.java"


# instance fields
.field private final mHalfSeparatorThickness:I

.field private final mSeparatorPaddingBottom:I

.field private final mSeparatorPaddingLeft:I

.field private final mSeparatorPaddingRight:I

.field private final mSeparatorPaddingTop:I

.field private final mSeparatorPaint:Landroid/graphics/Paint;

.field private final mSeparatorThickness:I

.field private mSeparatorVisible:Z


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 31
    const/4 v0, 0x0

    const/4 v1, 0x0

    invoke-direct {p0, p1, v0, v1}, Lcom/google/android/play/layout/PlaySeparatorLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 32
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    .line 35
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/google/android/play/layout/PlaySeparatorLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 36
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 5
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;
    .param p3, "defStyle"    # I

    .prologue
    const/4 v4, 0x0

    .line 39
    invoke-direct {p0, p1, p2, p3}, Landroid/widget/RelativeLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 40
    invoke-virtual {p0, v4}, Lcom/google/android/play/layout/PlaySeparatorLayout;->setWillNotDraw(Z)V

    .line 42
    iput-boolean v4, p0, Lcom/google/android/play/layout/PlaySeparatorLayout;->mSeparatorVisible:Z

    .line 43
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    .line 44
    .local v0, "res":Landroid/content/res/Resources;
    sget v2, Lcom/google/android/play/R$dimen;->play_hairline_separator_thickness:I

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v2

    iput v2, p0, Lcom/google/android/play/layout/PlaySeparatorLayout;->mSeparatorThickness:I

    .line 45
    iget v2, p0, Lcom/google/android/play/layout/PlaySeparatorLayout;->mSeparatorThickness:I

    add-int/lit8 v2, v2, 0x1

    div-int/lit8 v2, v2, 0x2

    iput v2, p0, Lcom/google/android/play/layout/PlaySeparatorLayout;->mHalfSeparatorThickness:I

    .line 46
    new-instance v2, Landroid/graphics/Paint;

    invoke-direct {v2}, Landroid/graphics/Paint;-><init>()V

    iput-object v2, p0, Lcom/google/android/play/layout/PlaySeparatorLayout;->mSeparatorPaint:Landroid/graphics/Paint;

    .line 47
    iget-object v2, p0, Lcom/google/android/play/layout/PlaySeparatorLayout;->mSeparatorPaint:Landroid/graphics/Paint;

    sget v3, Lcom/google/android/play/R$color;->play_reason_separator:I

    invoke-virtual {v0, v3}, Landroid/content/res/Resources;->getColor(I)I

    move-result v3

    invoke-virtual {v2, v3}, Landroid/graphics/Paint;->setColor(I)V

    .line 48
    iget-object v2, p0, Lcom/google/android/play/layout/PlaySeparatorLayout;->mSeparatorPaint:Landroid/graphics/Paint;

    iget v3, p0, Lcom/google/android/play/layout/PlaySeparatorLayout;->mSeparatorThickness:I

    int-to-float v3, v3

    invoke-virtual {v2, v3}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    .line 51
    sget-object v2, Lcom/google/android/play/R$styleable;->PlaySeparatorLayout:[I

    invoke-virtual {p1, p2, v2}, Landroid/content/Context;->obtainStyledAttributes(Landroid/util/AttributeSet;[I)Landroid/content/res/TypedArray;

    move-result-object v1

    .line 53
    .local v1, "viewAttrs":Landroid/content/res/TypedArray;
    sget v2, Lcom/google/android/play/R$styleable;->PlaySeparatorLayout_separator_padding_top:I

    invoke-virtual {v1, v2, v4}, Landroid/content/res/TypedArray;->getDimensionPixelSize(II)I

    move-result v2

    iput v2, p0, Lcom/google/android/play/layout/PlaySeparatorLayout;->mSeparatorPaddingTop:I

    .line 55
    sget v2, Lcom/google/android/play/R$styleable;->PlaySeparatorLayout_separator_padding_bottom:I

    invoke-virtual {v1, v2, v4}, Landroid/content/res/TypedArray;->getDimensionPixelSize(II)I

    move-result v2

    iput v2, p0, Lcom/google/android/play/layout/PlaySeparatorLayout;->mSeparatorPaddingBottom:I

    .line 57
    sget v2, Lcom/google/android/play/R$styleable;->PlaySeparatorLayout_separator_padding_left:I

    invoke-virtual {v1, v2, v4}, Landroid/content/res/TypedArray;->getDimensionPixelSize(II)I

    move-result v2

    iput v2, p0, Lcom/google/android/play/layout/PlaySeparatorLayout;->mSeparatorPaddingLeft:I

    .line 59
    sget v2, Lcom/google/android/play/R$styleable;->PlaySeparatorLayout_separator_padding_right:I

    invoke-virtual {v1, v2, v4}, Landroid/content/res/TypedArray;->getDimensionPixelSize(II)I

    move-result v2

    iput v2, p0, Lcom/google/android/play/layout/PlaySeparatorLayout;->mSeparatorPaddingRight:I

    .line 61
    invoke-virtual {v1}, Landroid/content/res/TypedArray;->recycle()V

    .line 62
    return-void
.end method


# virtual methods
.method protected onDraw(Landroid/graphics/Canvas;)V
    .locals 7
    .param p1, "canvas"    # Landroid/graphics/Canvas;

    .prologue
    .line 77
    invoke-super {p0, p1}, Landroid/widget/RelativeLayout;->onDraw(Landroid/graphics/Canvas;)V

    .line 79
    iget-boolean v0, p0, Lcom/google/android/play/layout/PlaySeparatorLayout;->mSeparatorVisible:Z

    if-eqz v0, :cond_0

    .line 80
    iget v0, p0, Lcom/google/android/play/layout/PlaySeparatorLayout;->mSeparatorPaddingTop:I

    iget v1, p0, Lcom/google/android/play/layout/PlaySeparatorLayout;->mHalfSeparatorThickness:I

    add-int v6, v0, v1

    .line 81
    .local v6, "separatorTop":I
    iget v0, p0, Lcom/google/android/play/layout/PlaySeparatorLayout;->mSeparatorPaddingLeft:I

    int-to-float v1, v0

    int-to-float v2, v6

    invoke-virtual {p0}, Lcom/google/android/play/layout/PlaySeparatorLayout;->getWidth()I

    move-result v0

    iget v3, p0, Lcom/google/android/play/layout/PlaySeparatorLayout;->mSeparatorPaddingRight:I

    sub-int/2addr v0, v3

    int-to-float v3, v0

    int-to-float v4, v6

    iget-object v5, p0, Lcom/google/android/play/layout/PlaySeparatorLayout;->mSeparatorPaint:Landroid/graphics/Paint;

    move-object v0, p1

    invoke-virtual/range {v0 .. v5}, Landroid/graphics/Canvas;->drawLine(FFFFLandroid/graphics/Paint;)V

    .line 85
    .end local v6    # "separatorTop":I
    :cond_0
    return-void
.end method

.method public setSeparatorVisible(Z)V
    .locals 4
    .param p1, "isSeparatorVisible"    # Z

    .prologue
    const/4 v1, 0x0

    .line 65
    iget-boolean v2, p0, Lcom/google/android/play/layout/PlaySeparatorLayout;->mSeparatorVisible:Z

    if-ne v2, p1, :cond_0

    .line 73
    :goto_0
    return-void

    .line 68
    :cond_0
    iput-boolean p1, p0, Lcom/google/android/play/layout/PlaySeparatorLayout;->mSeparatorVisible:Z

    .line 69
    if-eqz p1, :cond_1

    iget v2, p0, Lcom/google/android/play/layout/PlaySeparatorLayout;->mSeparatorPaddingTop:I

    iget v3, p0, Lcom/google/android/play/layout/PlaySeparatorLayout;->mSeparatorPaddingBottom:I

    add-int/2addr v2, v3

    iget v3, p0, Lcom/google/android/play/layout/PlaySeparatorLayout;->mSeparatorThickness:I

    add-int v0, v2, v3

    .line 71
    .local v0, "topPadding":I
    :goto_1
    invoke-virtual {p0, v1, v0, v1, v1}, Lcom/google/android/play/layout/PlaySeparatorLayout;->setPadding(IIII)V

    .line 72
    invoke-virtual {p0}, Lcom/google/android/play/layout/PlaySeparatorLayout;->invalidate()V

    goto :goto_0

    .end local v0    # "topPadding":I
    :cond_1
    move v0, v1

    .line 69
    goto :goto_1
.end method
