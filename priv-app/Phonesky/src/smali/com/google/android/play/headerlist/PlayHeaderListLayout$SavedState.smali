.class Lcom/google/android/play/headerlist/PlayHeaderListLayout$SavedState;
.super Landroid/view/View$BaseSavedState;
.source "PlayHeaderListLayout.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/play/headerlist/PlayHeaderListLayout;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = "SavedState"
.end annotation


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/google/android/play/headerlist/PlayHeaderListLayout$SavedState;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field final mFloatingFraction:F


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1079
    new-instance v0, Lcom/google/android/play/headerlist/PlayHeaderListLayout$SavedState$1;

    invoke-direct {v0}, Lcom/google/android/play/headerlist/PlayHeaderListLayout$SavedState$1;-><init>()V

    sput-object v0, Lcom/google/android/play/headerlist/PlayHeaderListLayout$SavedState;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method private constructor <init>(Landroid/os/Parcel;)V
    .locals 1
    .param p1, "in"    # Landroid/os/Parcel;

    .prologue
    .line 1063
    invoke-direct {p0, p1}, Landroid/view/View$BaseSavedState;-><init>(Landroid/os/Parcel;)V

    .line 1064
    invoke-virtual {p1}, Landroid/os/Parcel;->readFloat()F

    move-result v0

    iput v0, p0, Lcom/google/android/play/headerlist/PlayHeaderListLayout$SavedState;->mFloatingFraction:F

    .line 1065
    return-void
.end method

.method synthetic constructor <init>(Landroid/os/Parcel;Lcom/google/android/play/headerlist/PlayHeaderListLayout$1;)V
    .locals 0
    .param p1, "x0"    # Landroid/os/Parcel;
    .param p2, "x1"    # Lcom/google/android/play/headerlist/PlayHeaderListLayout$1;

    .prologue
    .line 1054
    invoke-direct {p0, p1}, Lcom/google/android/play/headerlist/PlayHeaderListLayout$SavedState;-><init>(Landroid/os/Parcel;)V

    return-void
.end method

.method constructor <init>(Landroid/os/Parcelable;Lcom/google/android/play/headerlist/PlayHeaderListLayout;)V
    .locals 1
    .param p1, "superState"    # Landroid/os/Parcelable;
    .param p2, "layout"    # Lcom/google/android/play/headerlist/PlayHeaderListLayout;

    .prologue
    .line 1058
    invoke-direct {p0, p1}, Landroid/view/View$BaseSavedState;-><init>(Landroid/os/Parcelable;)V

    .line 1059
    # getter for: Lcom/google/android/play/headerlist/PlayHeaderListLayout;->mFloatingFraction:F
    invoke-static {p2}, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->access$1000(Lcom/google/android/play/headerlist/PlayHeaderListLayout;)F

    move-result v0

    iput v0, p0, Lcom/google/android/play/headerlist/PlayHeaderListLayout$SavedState;->mFloatingFraction:F

    .line 1060
    return-void
.end method


# virtual methods
.method public toString()Ljava/lang/String;
    .locals 5

    .prologue
    .line 1075
    sget-object v0, Ljava/util/Locale;->US:Ljava/util/Locale;

    const-string v1, "floatingFraction: %f"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    iget v4, p0, Lcom/google/android/play/headerlist/PlayHeaderListLayout$SavedState;->mFloatingFraction:F

    invoke-static {v4}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-static {v0, v1, v2}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 1
    .param p1, "out"    # Landroid/os/Parcel;
    .param p2, "flags"    # I

    .prologue
    .line 1069
    invoke-super {p0, p1, p2}, Landroid/view/View$BaseSavedState;->writeToParcel(Landroid/os/Parcel;I)V

    .line 1070
    iget v0, p0, Lcom/google/android/play/headerlist/PlayHeaderListLayout$SavedState;->mFloatingFraction:F

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeFloat(F)V

    .line 1071
    return-void
.end method
