.class public Lcom/google/android/play/dfe/api/DfeRetryPolicy;
.super Lcom/android/volley/DefaultRetryPolicy;
.source "DfeRetryPolicy.java"


# instance fields
.field private mHadAuthException:Z

.field private final mPlayDfeApiContext:Lcom/google/android/play/dfe/api/PlayDfeApiContext;


# direct methods
.method public constructor <init>(IIFLcom/google/android/play/dfe/api/PlayDfeApiContext;)V
    .locals 0
    .param p1, "initialTimeoutMs"    # I
    .param p2, "maxNumRetries"    # I
    .param p3, "backoffMultiplier"    # F
    .param p4, "context"    # Lcom/google/android/play/dfe/api/PlayDfeApiContext;

    .prologue
    .line 41
    invoke-direct {p0, p1, p2, p3}, Lcom/android/volley/DefaultRetryPolicy;-><init>(IIF)V

    .line 42
    iput-object p4, p0, Lcom/google/android/play/dfe/api/DfeRetryPolicy;->mPlayDfeApiContext:Lcom/google/android/play/dfe/api/PlayDfeApiContext;

    .line 43
    return-void
.end method

.method public constructor <init>(Lcom/google/android/play/dfe/api/PlayDfeApiContext;)V
    .locals 3
    .param p1, "context"    # Lcom/google/android/play/dfe/api/PlayDfeApiContext;

    .prologue
    .line 27
    sget-object v0, Lcom/google/android/play/utils/config/PlayG;->dfeRequestTimeoutMs:Lcom/google/android/play/utils/config/GservicesValue;

    invoke-virtual {v0}, Lcom/google/android/play/utils/config/GservicesValue;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v1

    sget-object v0, Lcom/google/android/play/utils/config/PlayG;->dfeMaxRetries:Lcom/google/android/play/utils/config/GservicesValue;

    invoke-virtual {v0}, Lcom/google/android/play/utils/config/GservicesValue;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v2

    sget-object v0, Lcom/google/android/play/utils/config/PlayG;->dfeBackoffMultipler:Lcom/google/android/play/utils/config/GservicesValue;

    invoke-virtual {v0}, Lcom/google/android/play/utils/config/GservicesValue;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Float;

    invoke-virtual {v0}, Ljava/lang/Float;->floatValue()F

    move-result v0

    invoke-direct {p0, v1, v2, v0}, Lcom/android/volley/DefaultRetryPolicy;-><init>(IIF)V

    .line 29
    iput-object p1, p0, Lcom/google/android/play/dfe/api/DfeRetryPolicy;->mPlayDfeApiContext:Lcom/google/android/play/dfe/api/PlayDfeApiContext;

    .line 30
    return-void
.end method


# virtual methods
.method public retry(Lcom/android/volley/VolleyError;)V
    .locals 1
    .param p1, "error"    # Lcom/android/volley/VolleyError;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/android/volley/VolleyError;
        }
    .end annotation

    .prologue
    .line 47
    instance-of v0, p1, Lcom/android/volley/AuthFailureError;

    if-eqz v0, :cond_1

    .line 48
    iget-boolean v0, p0, Lcom/google/android/play/dfe/api/DfeRetryPolicy;->mHadAuthException:Z

    if-eqz v0, :cond_0

    .line 49
    throw p1

    .line 51
    :cond_0
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/play/dfe/api/DfeRetryPolicy;->mHadAuthException:Z

    .line 52
    iget-object v0, p0, Lcom/google/android/play/dfe/api/DfeRetryPolicy;->mPlayDfeApiContext:Lcom/google/android/play/dfe/api/PlayDfeApiContext;

    invoke-virtual {v0}, Lcom/google/android/play/dfe/api/PlayDfeApiContext;->invalidateAuthToken()V

    .line 54
    :cond_1
    invoke-super {p0, p1}, Lcom/android/volley/DefaultRetryPolicy;->retry(Lcom/android/volley/VolleyError;)V

    .line 55
    return-void
.end method
