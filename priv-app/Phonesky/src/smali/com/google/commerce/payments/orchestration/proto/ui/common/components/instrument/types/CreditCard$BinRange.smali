.class public final Lcom/google/commerce/payments/orchestration/proto/ui/common/components/instrument/types/CreditCard$BinRange;
.super Lcom/google/protobuf/nano/MessageNano;
.source "CreditCard.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/commerce/payments/orchestration/proto/ui/common/components/instrument/types/CreditCard;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "BinRange"
.end annotation


# static fields
.field private static volatile _emptyArray:[Lcom/google/commerce/payments/orchestration/proto/ui/common/components/instrument/types/CreditCard$BinRange;


# instance fields
.field public cardNumberLength:I

.field public digitGrouping:[I

.field public end:Ljava/lang/String;

.field public errorMessage:Ljava/lang/String;

.field public start:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 628
    invoke-direct {p0}, Lcom/google/protobuf/nano/MessageNano;-><init>()V

    .line 629
    invoke-virtual {p0}, Lcom/google/commerce/payments/orchestration/proto/ui/common/components/instrument/types/CreditCard$BinRange;->clear()Lcom/google/commerce/payments/orchestration/proto/ui/common/components/instrument/types/CreditCard$BinRange;

    .line 630
    return-void
.end method

.method public static emptyArray()[Lcom/google/commerce/payments/orchestration/proto/ui/common/components/instrument/types/CreditCard$BinRange;
    .locals 2

    .prologue
    .line 602
    sget-object v0, Lcom/google/commerce/payments/orchestration/proto/ui/common/components/instrument/types/CreditCard$BinRange;->_emptyArray:[Lcom/google/commerce/payments/orchestration/proto/ui/common/components/instrument/types/CreditCard$BinRange;

    if-nez v0, :cond_1

    .line 603
    sget-object v1, Lcom/google/protobuf/nano/InternalNano;->LAZY_INIT_LOCK:Ljava/lang/Object;

    monitor-enter v1

    .line 605
    :try_start_0
    sget-object v0, Lcom/google/commerce/payments/orchestration/proto/ui/common/components/instrument/types/CreditCard$BinRange;->_emptyArray:[Lcom/google/commerce/payments/orchestration/proto/ui/common/components/instrument/types/CreditCard$BinRange;

    if-nez v0, :cond_0

    .line 606
    const/4 v0, 0x0

    new-array v0, v0, [Lcom/google/commerce/payments/orchestration/proto/ui/common/components/instrument/types/CreditCard$BinRange;

    sput-object v0, Lcom/google/commerce/payments/orchestration/proto/ui/common/components/instrument/types/CreditCard$BinRange;->_emptyArray:[Lcom/google/commerce/payments/orchestration/proto/ui/common/components/instrument/types/CreditCard$BinRange;

    .line 608
    :cond_0
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 610
    :cond_1
    sget-object v0, Lcom/google/commerce/payments/orchestration/proto/ui/common/components/instrument/types/CreditCard$BinRange;->_emptyArray:[Lcom/google/commerce/payments/orchestration/proto/ui/common/components/instrument/types/CreditCard$BinRange;

    return-object v0

    .line 608
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method


# virtual methods
.method public clear()Lcom/google/commerce/payments/orchestration/proto/ui/common/components/instrument/types/CreditCard$BinRange;
    .locals 1

    .prologue
    .line 633
    const-string v0, ""

    iput-object v0, p0, Lcom/google/commerce/payments/orchestration/proto/ui/common/components/instrument/types/CreditCard$BinRange;->start:Ljava/lang/String;

    .line 634
    const-string v0, ""

    iput-object v0, p0, Lcom/google/commerce/payments/orchestration/proto/ui/common/components/instrument/types/CreditCard$BinRange;->end:Ljava/lang/String;

    .line 635
    const/4 v0, 0x0

    iput v0, p0, Lcom/google/commerce/payments/orchestration/proto/ui/common/components/instrument/types/CreditCard$BinRange;->cardNumberLength:I

    .line 636
    sget-object v0, Lcom/google/protobuf/nano/WireFormatNano;->EMPTY_INT_ARRAY:[I

    iput-object v0, p0, Lcom/google/commerce/payments/orchestration/proto/ui/common/components/instrument/types/CreditCard$BinRange;->digitGrouping:[I

    .line 637
    const-string v0, ""

    iput-object v0, p0, Lcom/google/commerce/payments/orchestration/proto/ui/common/components/instrument/types/CreditCard$BinRange;->errorMessage:Ljava/lang/String;

    .line 638
    const/4 v0, -0x1

    iput v0, p0, Lcom/google/commerce/payments/orchestration/proto/ui/common/components/instrument/types/CreditCard$BinRange;->cachedSize:I

    .line 639
    return-object p0
.end method

.method protected computeSerializedSize()I
    .locals 6

    .prologue
    .line 667
    invoke-super {p0}, Lcom/google/protobuf/nano/MessageNano;->computeSerializedSize()I

    move-result v3

    .line 668
    .local v3, "size":I
    iget-object v4, p0, Lcom/google/commerce/payments/orchestration/proto/ui/common/components/instrument/types/CreditCard$BinRange;->start:Ljava/lang/String;

    const-string v5, ""

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_0

    .line 669
    const/4 v4, 0x1

    iget-object v5, p0, Lcom/google/commerce/payments/orchestration/proto/ui/common/components/instrument/types/CreditCard$BinRange;->start:Ljava/lang/String;

    invoke-static {v4, v5}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeStringSize(ILjava/lang/String;)I

    move-result v4

    add-int/2addr v3, v4

    .line 672
    :cond_0
    iget-object v4, p0, Lcom/google/commerce/payments/orchestration/proto/ui/common/components/instrument/types/CreditCard$BinRange;->end:Ljava/lang/String;

    const-string v5, ""

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_1

    .line 673
    const/4 v4, 0x2

    iget-object v5, p0, Lcom/google/commerce/payments/orchestration/proto/ui/common/components/instrument/types/CreditCard$BinRange;->end:Ljava/lang/String;

    invoke-static {v4, v5}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeStringSize(ILjava/lang/String;)I

    move-result v4

    add-int/2addr v3, v4

    .line 676
    :cond_1
    iget v4, p0, Lcom/google/commerce/payments/orchestration/proto/ui/common/components/instrument/types/CreditCard$BinRange;->cardNumberLength:I

    if-eqz v4, :cond_2

    .line 677
    const/4 v4, 0x3

    iget v5, p0, Lcom/google/commerce/payments/orchestration/proto/ui/common/components/instrument/types/CreditCard$BinRange;->cardNumberLength:I

    invoke-static {v4, v5}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeInt32Size(II)I

    move-result v4

    add-int/2addr v3, v4

    .line 680
    :cond_2
    iget-object v4, p0, Lcom/google/commerce/payments/orchestration/proto/ui/common/components/instrument/types/CreditCard$BinRange;->digitGrouping:[I

    if-eqz v4, :cond_4

    iget-object v4, p0, Lcom/google/commerce/payments/orchestration/proto/ui/common/components/instrument/types/CreditCard$BinRange;->digitGrouping:[I

    array-length v4, v4

    if-lez v4, :cond_4

    .line 681
    const/4 v0, 0x0

    .line 682
    .local v0, "dataSize":I
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_0
    iget-object v4, p0, Lcom/google/commerce/payments/orchestration/proto/ui/common/components/instrument/types/CreditCard$BinRange;->digitGrouping:[I

    array-length v4, v4

    if-ge v2, v4, :cond_3

    .line 683
    iget-object v4, p0, Lcom/google/commerce/payments/orchestration/proto/ui/common/components/instrument/types/CreditCard$BinRange;->digitGrouping:[I

    aget v1, v4, v2

    .line 684
    .local v1, "element":I
    invoke-static {v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeInt32SizeNoTag(I)I

    move-result v4

    add-int/2addr v0, v4

    .line 682
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 687
    .end local v1    # "element":I
    :cond_3
    add-int/2addr v3, v0

    .line 688
    iget-object v4, p0, Lcom/google/commerce/payments/orchestration/proto/ui/common/components/instrument/types/CreditCard$BinRange;->digitGrouping:[I

    array-length v4, v4

    mul-int/lit8 v4, v4, 0x1

    add-int/2addr v3, v4

    .line 690
    .end local v0    # "dataSize":I
    .end local v2    # "i":I
    :cond_4
    iget-object v4, p0, Lcom/google/commerce/payments/orchestration/proto/ui/common/components/instrument/types/CreditCard$BinRange;->errorMessage:Ljava/lang/String;

    const-string v5, ""

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_5

    .line 691
    const/4 v4, 0x5

    iget-object v5, p0, Lcom/google/commerce/payments/orchestration/proto/ui/common/components/instrument/types/CreditCard$BinRange;->errorMessage:Ljava/lang/String;

    invoke-static {v4, v5}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeStringSize(ILjava/lang/String;)I

    move-result v4

    add-int/2addr v3, v4

    .line 694
    :cond_5
    return v3
.end method

.method public mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/commerce/payments/orchestration/proto/ui/common/components/instrument/types/CreditCard$BinRange;
    .locals 9
    .param p1, "input"    # Lcom/google/protobuf/nano/CodedInputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v7, 0x0

    .line 702
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readTag()I

    move-result v6

    .line 703
    .local v6, "tag":I
    sparse-switch v6, :sswitch_data_0

    .line 707
    invoke-static {p1, v6}, Lcom/google/protobuf/nano/WireFormatNano;->parseUnknownField(Lcom/google/protobuf/nano/CodedInputByteBufferNano;I)Z

    move-result v8

    if-nez v8, :cond_0

    .line 708
    :sswitch_0
    return-object p0

    .line 713
    :sswitch_1
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readString()Ljava/lang/String;

    move-result-object v8

    iput-object v8, p0, Lcom/google/commerce/payments/orchestration/proto/ui/common/components/instrument/types/CreditCard$BinRange;->start:Ljava/lang/String;

    goto :goto_0

    .line 717
    :sswitch_2
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readString()Ljava/lang/String;

    move-result-object v8

    iput-object v8, p0, Lcom/google/commerce/payments/orchestration/proto/ui/common/components/instrument/types/CreditCard$BinRange;->end:Ljava/lang/String;

    goto :goto_0

    .line 721
    :sswitch_3
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readInt32()I

    move-result v8

    iput v8, p0, Lcom/google/commerce/payments/orchestration/proto/ui/common/components/instrument/types/CreditCard$BinRange;->cardNumberLength:I

    goto :goto_0

    .line 725
    :sswitch_4
    const/16 v8, 0x20

    invoke-static {p1, v8}, Lcom/google/protobuf/nano/WireFormatNano;->getRepeatedFieldArrayLength(Lcom/google/protobuf/nano/CodedInputByteBufferNano;I)I

    move-result v0

    .line 727
    .local v0, "arrayLength":I
    iget-object v8, p0, Lcom/google/commerce/payments/orchestration/proto/ui/common/components/instrument/types/CreditCard$BinRange;->digitGrouping:[I

    if-nez v8, :cond_2

    move v1, v7

    .line 728
    .local v1, "i":I
    :goto_1
    add-int v8, v1, v0

    new-array v4, v8, [I

    .line 729
    .local v4, "newArray":[I
    if-eqz v1, :cond_1

    .line 730
    iget-object v8, p0, Lcom/google/commerce/payments/orchestration/proto/ui/common/components/instrument/types/CreditCard$BinRange;->digitGrouping:[I

    invoke-static {v8, v7, v4, v7, v1}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 732
    :cond_1
    :goto_2
    array-length v8, v4

    add-int/lit8 v8, v8, -0x1

    if-ge v1, v8, :cond_3

    .line 733
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readInt32()I

    move-result v8

    aput v8, v4, v1

    .line 734
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readTag()I

    .line 732
    add-int/lit8 v1, v1, 0x1

    goto :goto_2

    .line 727
    .end local v1    # "i":I
    .end local v4    # "newArray":[I
    :cond_2
    iget-object v8, p0, Lcom/google/commerce/payments/orchestration/proto/ui/common/components/instrument/types/CreditCard$BinRange;->digitGrouping:[I

    array-length v1, v8

    goto :goto_1

    .line 737
    .restart local v1    # "i":I
    .restart local v4    # "newArray":[I
    :cond_3
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readInt32()I

    move-result v8

    aput v8, v4, v1

    .line 738
    iput-object v4, p0, Lcom/google/commerce/payments/orchestration/proto/ui/common/components/instrument/types/CreditCard$BinRange;->digitGrouping:[I

    goto :goto_0

    .line 742
    .end local v0    # "arrayLength":I
    .end local v1    # "i":I
    .end local v4    # "newArray":[I
    :sswitch_5
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readRawVarint32()I

    move-result v2

    .line 743
    .local v2, "length":I
    invoke-virtual {p1, v2}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->pushLimit(I)I

    move-result v3

    .line 745
    .local v3, "limit":I
    const/4 v0, 0x0

    .line 746
    .restart local v0    # "arrayLength":I
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->getPosition()I

    move-result v5

    .line 747
    .local v5, "startPos":I
    :goto_3
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->getBytesUntilLimit()I

    move-result v8

    if-lez v8, :cond_4

    .line 748
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readInt32()I

    .line 749
    add-int/lit8 v0, v0, 0x1

    goto :goto_3

    .line 751
    :cond_4
    invoke-virtual {p1, v5}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->rewindToPosition(I)V

    .line 752
    iget-object v8, p0, Lcom/google/commerce/payments/orchestration/proto/ui/common/components/instrument/types/CreditCard$BinRange;->digitGrouping:[I

    if-nez v8, :cond_6

    move v1, v7

    .line 753
    .restart local v1    # "i":I
    :goto_4
    add-int v8, v1, v0

    new-array v4, v8, [I

    .line 754
    .restart local v4    # "newArray":[I
    if-eqz v1, :cond_5

    .line 755
    iget-object v8, p0, Lcom/google/commerce/payments/orchestration/proto/ui/common/components/instrument/types/CreditCard$BinRange;->digitGrouping:[I

    invoke-static {v8, v7, v4, v7, v1}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 757
    :cond_5
    :goto_5
    array-length v8, v4

    if-ge v1, v8, :cond_7

    .line 758
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readInt32()I

    move-result v8

    aput v8, v4, v1

    .line 757
    add-int/lit8 v1, v1, 0x1

    goto :goto_5

    .line 752
    .end local v1    # "i":I
    .end local v4    # "newArray":[I
    :cond_6
    iget-object v8, p0, Lcom/google/commerce/payments/orchestration/proto/ui/common/components/instrument/types/CreditCard$BinRange;->digitGrouping:[I

    array-length v1, v8

    goto :goto_4

    .line 760
    .restart local v1    # "i":I
    .restart local v4    # "newArray":[I
    :cond_7
    iput-object v4, p0, Lcom/google/commerce/payments/orchestration/proto/ui/common/components/instrument/types/CreditCard$BinRange;->digitGrouping:[I

    .line 761
    invoke-virtual {p1, v3}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->popLimit(I)V

    goto/16 :goto_0

    .line 765
    .end local v0    # "arrayLength":I
    .end local v1    # "i":I
    .end local v2    # "length":I
    .end local v3    # "limit":I
    .end local v4    # "newArray":[I
    .end local v5    # "startPos":I
    :sswitch_6
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readString()Ljava/lang/String;

    move-result-object v8

    iput-object v8, p0, Lcom/google/commerce/payments/orchestration/proto/ui/common/components/instrument/types/CreditCard$BinRange;->errorMessage:Ljava/lang/String;

    goto/16 :goto_0

    .line 703
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x18 -> :sswitch_3
        0x20 -> :sswitch_4
        0x22 -> :sswitch_5
        0x2a -> :sswitch_6
    .end sparse-switch
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/protobuf/nano/MessageNano;
    .locals 1
    .param p1, "x0"    # Lcom/google/protobuf/nano/CodedInputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 596
    invoke-virtual {p0, p1}, Lcom/google/commerce/payments/orchestration/proto/ui/common/components/instrument/types/CreditCard$BinRange;->mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/commerce/payments/orchestration/proto/ui/common/components/instrument/types/CreditCard$BinRange;

    move-result-object v0

    return-object v0
.end method

.method public writeTo(Lcom/google/protobuf/nano/CodedOutputByteBufferNano;)V
    .locals 3
    .param p1, "output"    # Lcom/google/protobuf/nano/CodedOutputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 645
    iget-object v1, p0, Lcom/google/commerce/payments/orchestration/proto/ui/common/components/instrument/types/CreditCard$BinRange;->start:Ljava/lang/String;

    const-string v2, ""

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 646
    const/4 v1, 0x1

    iget-object v2, p0, Lcom/google/commerce/payments/orchestration/proto/ui/common/components/instrument/types/CreditCard$BinRange;->start:Ljava/lang/String;

    invoke-virtual {p1, v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeString(ILjava/lang/String;)V

    .line 648
    :cond_0
    iget-object v1, p0, Lcom/google/commerce/payments/orchestration/proto/ui/common/components/instrument/types/CreditCard$BinRange;->end:Ljava/lang/String;

    const-string v2, ""

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_1

    .line 649
    const/4 v1, 0x2

    iget-object v2, p0, Lcom/google/commerce/payments/orchestration/proto/ui/common/components/instrument/types/CreditCard$BinRange;->end:Ljava/lang/String;

    invoke-virtual {p1, v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeString(ILjava/lang/String;)V

    .line 651
    :cond_1
    iget v1, p0, Lcom/google/commerce/payments/orchestration/proto/ui/common/components/instrument/types/CreditCard$BinRange;->cardNumberLength:I

    if-eqz v1, :cond_2

    .line 652
    const/4 v1, 0x3

    iget v2, p0, Lcom/google/commerce/payments/orchestration/proto/ui/common/components/instrument/types/CreditCard$BinRange;->cardNumberLength:I

    invoke-virtual {p1, v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeInt32(II)V

    .line 654
    :cond_2
    iget-object v1, p0, Lcom/google/commerce/payments/orchestration/proto/ui/common/components/instrument/types/CreditCard$BinRange;->digitGrouping:[I

    if-eqz v1, :cond_3

    iget-object v1, p0, Lcom/google/commerce/payments/orchestration/proto/ui/common/components/instrument/types/CreditCard$BinRange;->digitGrouping:[I

    array-length v1, v1

    if-lez v1, :cond_3

    .line 655
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    iget-object v1, p0, Lcom/google/commerce/payments/orchestration/proto/ui/common/components/instrument/types/CreditCard$BinRange;->digitGrouping:[I

    array-length v1, v1

    if-ge v0, v1, :cond_3

    .line 656
    const/4 v1, 0x4

    iget-object v2, p0, Lcom/google/commerce/payments/orchestration/proto/ui/common/components/instrument/types/CreditCard$BinRange;->digitGrouping:[I

    aget v2, v2, v0

    invoke-virtual {p1, v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeInt32(II)V

    .line 655
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 659
    .end local v0    # "i":I
    :cond_3
    iget-object v1, p0, Lcom/google/commerce/payments/orchestration/proto/ui/common/components/instrument/types/CreditCard$BinRange;->errorMessage:Ljava/lang/String;

    const-string v2, ""

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_4

    .line 660
    const/4 v1, 0x5

    iget-object v2, p0, Lcom/google/commerce/payments/orchestration/proto/ui/common/components/instrument/types/CreditCard$BinRange;->errorMessage:Ljava/lang/String;

    invoke-virtual {p1, v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeString(ILjava/lang/String;)V

    .line 662
    :cond_4
    invoke-super {p0, p1}, Lcom/google/protobuf/nano/MessageNano;->writeTo(Lcom/google/protobuf/nano/CodedOutputByteBufferNano;)V

    .line 663
    return-void
.end method
