.class public final Lcom/google/commerce/payments/orchestration/proto/api/integratorbuyflow/DataTokens$AndroidEnvironmentConfig;
.super Lcom/google/protobuf/nano/MessageNano;
.source "DataTokens.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/commerce/payments/orchestration/proto/api/integratorbuyflow/DataTokens;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "AndroidEnvironmentConfig"
.end annotation


# instance fields
.field public accountName:Ljava/lang/String;

.field public authTokenType:Ljava/lang/String;

.field public serverBasePath:Ljava/lang/String;

.field public serverEesBasePath:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 247
    invoke-direct {p0}, Lcom/google/protobuf/nano/MessageNano;-><init>()V

    .line 248
    invoke-virtual {p0}, Lcom/google/commerce/payments/orchestration/proto/api/integratorbuyflow/DataTokens$AndroidEnvironmentConfig;->clear()Lcom/google/commerce/payments/orchestration/proto/api/integratorbuyflow/DataTokens$AndroidEnvironmentConfig;

    .line 249
    return-void
.end method


# virtual methods
.method public clear()Lcom/google/commerce/payments/orchestration/proto/api/integratorbuyflow/DataTokens$AndroidEnvironmentConfig;
    .locals 1

    .prologue
    .line 252
    const-string v0, ""

    iput-object v0, p0, Lcom/google/commerce/payments/orchestration/proto/api/integratorbuyflow/DataTokens$AndroidEnvironmentConfig;->serverBasePath:Ljava/lang/String;

    .line 253
    const-string v0, ""

    iput-object v0, p0, Lcom/google/commerce/payments/orchestration/proto/api/integratorbuyflow/DataTokens$AndroidEnvironmentConfig;->serverEesBasePath:Ljava/lang/String;

    .line 254
    const-string v0, ""

    iput-object v0, p0, Lcom/google/commerce/payments/orchestration/proto/api/integratorbuyflow/DataTokens$AndroidEnvironmentConfig;->authTokenType:Ljava/lang/String;

    .line 255
    const-string v0, ""

    iput-object v0, p0, Lcom/google/commerce/payments/orchestration/proto/api/integratorbuyflow/DataTokens$AndroidEnvironmentConfig;->accountName:Ljava/lang/String;

    .line 256
    const/4 v0, -0x1

    iput v0, p0, Lcom/google/commerce/payments/orchestration/proto/api/integratorbuyflow/DataTokens$AndroidEnvironmentConfig;->cachedSize:I

    .line 257
    return-object p0
.end method

.method protected computeSerializedSize()I
    .locals 3

    .prologue
    .line 280
    invoke-super {p0}, Lcom/google/protobuf/nano/MessageNano;->computeSerializedSize()I

    move-result v0

    .line 281
    .local v0, "size":I
    iget-object v1, p0, Lcom/google/commerce/payments/orchestration/proto/api/integratorbuyflow/DataTokens$AndroidEnvironmentConfig;->serverBasePath:Ljava/lang/String;

    const-string v2, ""

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 282
    const/4 v1, 0x1

    iget-object v2, p0, Lcom/google/commerce/payments/orchestration/proto/api/integratorbuyflow/DataTokens$AndroidEnvironmentConfig;->serverBasePath:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeStringSize(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 285
    :cond_0
    iget-object v1, p0, Lcom/google/commerce/payments/orchestration/proto/api/integratorbuyflow/DataTokens$AndroidEnvironmentConfig;->authTokenType:Ljava/lang/String;

    const-string v2, ""

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_1

    .line 286
    const/4 v1, 0x2

    iget-object v2, p0, Lcom/google/commerce/payments/orchestration/proto/api/integratorbuyflow/DataTokens$AndroidEnvironmentConfig;->authTokenType:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeStringSize(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 289
    :cond_1
    iget-object v1, p0, Lcom/google/commerce/payments/orchestration/proto/api/integratorbuyflow/DataTokens$AndroidEnvironmentConfig;->accountName:Ljava/lang/String;

    const-string v2, ""

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_2

    .line 290
    const/4 v1, 0x3

    iget-object v2, p0, Lcom/google/commerce/payments/orchestration/proto/api/integratorbuyflow/DataTokens$AndroidEnvironmentConfig;->accountName:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeStringSize(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 293
    :cond_2
    iget-object v1, p0, Lcom/google/commerce/payments/orchestration/proto/api/integratorbuyflow/DataTokens$AndroidEnvironmentConfig;->serverEesBasePath:Ljava/lang/String;

    const-string v2, ""

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_3

    .line 294
    const/4 v1, 0x4

    iget-object v2, p0, Lcom/google/commerce/payments/orchestration/proto/api/integratorbuyflow/DataTokens$AndroidEnvironmentConfig;->serverEesBasePath:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeStringSize(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 297
    :cond_3
    return v0
.end method

.method public mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/commerce/payments/orchestration/proto/api/integratorbuyflow/DataTokens$AndroidEnvironmentConfig;
    .locals 2
    .param p1, "input"    # Lcom/google/protobuf/nano/CodedInputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 305
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readTag()I

    move-result v0

    .line 306
    .local v0, "tag":I
    sparse-switch v0, :sswitch_data_0

    .line 310
    invoke-static {p1, v0}, Lcom/google/protobuf/nano/WireFormatNano;->parseUnknownField(Lcom/google/protobuf/nano/CodedInputByteBufferNano;I)Z

    move-result v1

    if-nez v1, :cond_0

    .line 311
    :sswitch_0
    return-object p0

    .line 316
    :sswitch_1
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readString()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/google/commerce/payments/orchestration/proto/api/integratorbuyflow/DataTokens$AndroidEnvironmentConfig;->serverBasePath:Ljava/lang/String;

    goto :goto_0

    .line 320
    :sswitch_2
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readString()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/google/commerce/payments/orchestration/proto/api/integratorbuyflow/DataTokens$AndroidEnvironmentConfig;->authTokenType:Ljava/lang/String;

    goto :goto_0

    .line 324
    :sswitch_3
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readString()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/google/commerce/payments/orchestration/proto/api/integratorbuyflow/DataTokens$AndroidEnvironmentConfig;->accountName:Ljava/lang/String;

    goto :goto_0

    .line 328
    :sswitch_4
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readString()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/google/commerce/payments/orchestration/proto/api/integratorbuyflow/DataTokens$AndroidEnvironmentConfig;->serverEesBasePath:Ljava/lang/String;

    goto :goto_0

    .line 306
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
        0x22 -> :sswitch_4
    .end sparse-switch
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/protobuf/nano/MessageNano;
    .locals 1
    .param p1, "x0"    # Lcom/google/protobuf/nano/CodedInputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 218
    invoke-virtual {p0, p1}, Lcom/google/commerce/payments/orchestration/proto/api/integratorbuyflow/DataTokens$AndroidEnvironmentConfig;->mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/commerce/payments/orchestration/proto/api/integratorbuyflow/DataTokens$AndroidEnvironmentConfig;

    move-result-object v0

    return-object v0
.end method

.method public writeTo(Lcom/google/protobuf/nano/CodedOutputByteBufferNano;)V
    .locals 2
    .param p1, "output"    # Lcom/google/protobuf/nano/CodedOutputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 263
    iget-object v0, p0, Lcom/google/commerce/payments/orchestration/proto/api/integratorbuyflow/DataTokens$AndroidEnvironmentConfig;->serverBasePath:Ljava/lang/String;

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 264
    const/4 v0, 0x1

    iget-object v1, p0, Lcom/google/commerce/payments/orchestration/proto/api/integratorbuyflow/DataTokens$AndroidEnvironmentConfig;->serverBasePath:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeString(ILjava/lang/String;)V

    .line 266
    :cond_0
    iget-object v0, p0, Lcom/google/commerce/payments/orchestration/proto/api/integratorbuyflow/DataTokens$AndroidEnvironmentConfig;->authTokenType:Ljava/lang/String;

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 267
    const/4 v0, 0x2

    iget-object v1, p0, Lcom/google/commerce/payments/orchestration/proto/api/integratorbuyflow/DataTokens$AndroidEnvironmentConfig;->authTokenType:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeString(ILjava/lang/String;)V

    .line 269
    :cond_1
    iget-object v0, p0, Lcom/google/commerce/payments/orchestration/proto/api/integratorbuyflow/DataTokens$AndroidEnvironmentConfig;->accountName:Ljava/lang/String;

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_2

    .line 270
    const/4 v0, 0x3

    iget-object v1, p0, Lcom/google/commerce/payments/orchestration/proto/api/integratorbuyflow/DataTokens$AndroidEnvironmentConfig;->accountName:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeString(ILjava/lang/String;)V

    .line 272
    :cond_2
    iget-object v0, p0, Lcom/google/commerce/payments/orchestration/proto/api/integratorbuyflow/DataTokens$AndroidEnvironmentConfig;->serverEesBasePath:Ljava/lang/String;

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_3

    .line 273
    const/4 v0, 0x4

    iget-object v1, p0, Lcom/google/commerce/payments/orchestration/proto/api/integratorbuyflow/DataTokens$AndroidEnvironmentConfig;->serverEesBasePath:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeString(ILjava/lang/String;)V

    .line 275
    :cond_3
    invoke-super {p0, p1}, Lcom/google/protobuf/nano/MessageNano;->writeTo(Lcom/google/protobuf/nano/CodedOutputByteBufferNano;)V

    .line 276
    return-void
.end method
