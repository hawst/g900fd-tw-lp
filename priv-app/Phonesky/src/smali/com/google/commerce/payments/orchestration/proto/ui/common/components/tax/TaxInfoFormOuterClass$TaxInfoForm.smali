.class public final Lcom/google/commerce/payments/orchestration/proto/ui/common/components/tax/TaxInfoFormOuterClass$TaxInfoForm;
.super Lcom/google/protobuf/nano/MessageNano;
.source "TaxInfoFormOuterClass.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/commerce/payments/orchestration/proto/ui/common/components/tax/TaxInfoFormOuterClass;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "TaxInfoForm"
.end annotation


# static fields
.field private static volatile _emptyArray:[Lcom/google/commerce/payments/orchestration/proto/ui/common/components/tax/TaxInfoFormOuterClass$TaxInfoForm;


# instance fields
.field public id:Ljava/lang/String;

.field public label:Ljava/lang/String;

.field public taxInfoField:[Lcom/google/commerce/payments/orchestration/proto/ui/common/generic/UiFieldOuterClass$UiField;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 37
    invoke-direct {p0}, Lcom/google/protobuf/nano/MessageNano;-><init>()V

    .line 38
    invoke-virtual {p0}, Lcom/google/commerce/payments/orchestration/proto/ui/common/components/tax/TaxInfoFormOuterClass$TaxInfoForm;->clear()Lcom/google/commerce/payments/orchestration/proto/ui/common/components/tax/TaxInfoFormOuterClass$TaxInfoForm;

    .line 39
    return-void
.end method

.method public static emptyArray()[Lcom/google/commerce/payments/orchestration/proto/ui/common/components/tax/TaxInfoFormOuterClass$TaxInfoForm;
    .locals 2

    .prologue
    .line 17
    sget-object v0, Lcom/google/commerce/payments/orchestration/proto/ui/common/components/tax/TaxInfoFormOuterClass$TaxInfoForm;->_emptyArray:[Lcom/google/commerce/payments/orchestration/proto/ui/common/components/tax/TaxInfoFormOuterClass$TaxInfoForm;

    if-nez v0, :cond_1

    .line 18
    sget-object v1, Lcom/google/protobuf/nano/InternalNano;->LAZY_INIT_LOCK:Ljava/lang/Object;

    monitor-enter v1

    .line 20
    :try_start_0
    sget-object v0, Lcom/google/commerce/payments/orchestration/proto/ui/common/components/tax/TaxInfoFormOuterClass$TaxInfoForm;->_emptyArray:[Lcom/google/commerce/payments/orchestration/proto/ui/common/components/tax/TaxInfoFormOuterClass$TaxInfoForm;

    if-nez v0, :cond_0

    .line 21
    const/4 v0, 0x0

    new-array v0, v0, [Lcom/google/commerce/payments/orchestration/proto/ui/common/components/tax/TaxInfoFormOuterClass$TaxInfoForm;

    sput-object v0, Lcom/google/commerce/payments/orchestration/proto/ui/common/components/tax/TaxInfoFormOuterClass$TaxInfoForm;->_emptyArray:[Lcom/google/commerce/payments/orchestration/proto/ui/common/components/tax/TaxInfoFormOuterClass$TaxInfoForm;

    .line 23
    :cond_0
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 25
    :cond_1
    sget-object v0, Lcom/google/commerce/payments/orchestration/proto/ui/common/components/tax/TaxInfoFormOuterClass$TaxInfoForm;->_emptyArray:[Lcom/google/commerce/payments/orchestration/proto/ui/common/components/tax/TaxInfoFormOuterClass$TaxInfoForm;

    return-object v0

    .line 23
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method


# virtual methods
.method public clear()Lcom/google/commerce/payments/orchestration/proto/ui/common/components/tax/TaxInfoFormOuterClass$TaxInfoForm;
    .locals 1

    .prologue
    .line 42
    const-string v0, ""

    iput-object v0, p0, Lcom/google/commerce/payments/orchestration/proto/ui/common/components/tax/TaxInfoFormOuterClass$TaxInfoForm;->id:Ljava/lang/String;

    .line 43
    const-string v0, ""

    iput-object v0, p0, Lcom/google/commerce/payments/orchestration/proto/ui/common/components/tax/TaxInfoFormOuterClass$TaxInfoForm;->label:Ljava/lang/String;

    .line 44
    invoke-static {}, Lcom/google/commerce/payments/orchestration/proto/ui/common/generic/UiFieldOuterClass$UiField;->emptyArray()[Lcom/google/commerce/payments/orchestration/proto/ui/common/generic/UiFieldOuterClass$UiField;

    move-result-object v0

    iput-object v0, p0, Lcom/google/commerce/payments/orchestration/proto/ui/common/components/tax/TaxInfoFormOuterClass$TaxInfoForm;->taxInfoField:[Lcom/google/commerce/payments/orchestration/proto/ui/common/generic/UiFieldOuterClass$UiField;

    .line 45
    const/4 v0, -0x1

    iput v0, p0, Lcom/google/commerce/payments/orchestration/proto/ui/common/components/tax/TaxInfoFormOuterClass$TaxInfoForm;->cachedSize:I

    .line 46
    return-object p0
.end method

.method protected computeSerializedSize()I
    .locals 5

    .prologue
    .line 71
    invoke-super {p0}, Lcom/google/protobuf/nano/MessageNano;->computeSerializedSize()I

    move-result v2

    .line 72
    .local v2, "size":I
    iget-object v3, p0, Lcom/google/commerce/payments/orchestration/proto/ui/common/components/tax/TaxInfoFormOuterClass$TaxInfoForm;->id:Ljava/lang/String;

    const-string v4, ""

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_0

    .line 73
    const/4 v3, 0x1

    iget-object v4, p0, Lcom/google/commerce/payments/orchestration/proto/ui/common/components/tax/TaxInfoFormOuterClass$TaxInfoForm;->id:Ljava/lang/String;

    invoke-static {v3, v4}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeStringSize(ILjava/lang/String;)I

    move-result v3

    add-int/2addr v2, v3

    .line 76
    :cond_0
    iget-object v3, p0, Lcom/google/commerce/payments/orchestration/proto/ui/common/components/tax/TaxInfoFormOuterClass$TaxInfoForm;->label:Ljava/lang/String;

    const-string v4, ""

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_1

    .line 77
    const/4 v3, 0x2

    iget-object v4, p0, Lcom/google/commerce/payments/orchestration/proto/ui/common/components/tax/TaxInfoFormOuterClass$TaxInfoForm;->label:Ljava/lang/String;

    invoke-static {v3, v4}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeStringSize(ILjava/lang/String;)I

    move-result v3

    add-int/2addr v2, v3

    .line 80
    :cond_1
    iget-object v3, p0, Lcom/google/commerce/payments/orchestration/proto/ui/common/components/tax/TaxInfoFormOuterClass$TaxInfoForm;->taxInfoField:[Lcom/google/commerce/payments/orchestration/proto/ui/common/generic/UiFieldOuterClass$UiField;

    if-eqz v3, :cond_3

    iget-object v3, p0, Lcom/google/commerce/payments/orchestration/proto/ui/common/components/tax/TaxInfoFormOuterClass$TaxInfoForm;->taxInfoField:[Lcom/google/commerce/payments/orchestration/proto/ui/common/generic/UiFieldOuterClass$UiField;

    array-length v3, v3

    if-lez v3, :cond_3

    .line 81
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    iget-object v3, p0, Lcom/google/commerce/payments/orchestration/proto/ui/common/components/tax/TaxInfoFormOuterClass$TaxInfoForm;->taxInfoField:[Lcom/google/commerce/payments/orchestration/proto/ui/common/generic/UiFieldOuterClass$UiField;

    array-length v3, v3

    if-ge v1, v3, :cond_3

    .line 82
    iget-object v3, p0, Lcom/google/commerce/payments/orchestration/proto/ui/common/components/tax/TaxInfoFormOuterClass$TaxInfoForm;->taxInfoField:[Lcom/google/commerce/payments/orchestration/proto/ui/common/generic/UiFieldOuterClass$UiField;

    aget-object v0, v3, v1

    .line 83
    .local v0, "element":Lcom/google/commerce/payments/orchestration/proto/ui/common/generic/UiFieldOuterClass$UiField;
    if-eqz v0, :cond_2

    .line 84
    const/4 v3, 0x4

    invoke-static {v3, v0}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeMessageSize(ILcom/google/protobuf/nano/MessageNano;)I

    move-result v3

    add-int/2addr v2, v3

    .line 81
    :cond_2
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 89
    .end local v0    # "element":Lcom/google/commerce/payments/orchestration/proto/ui/common/generic/UiFieldOuterClass$UiField;
    .end local v1    # "i":I
    :cond_3
    return v2
.end method

.method public mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/commerce/payments/orchestration/proto/ui/common/components/tax/TaxInfoFormOuterClass$TaxInfoForm;
    .locals 6
    .param p1, "input"    # Lcom/google/protobuf/nano/CodedInputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v4, 0x0

    .line 97
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readTag()I

    move-result v3

    .line 98
    .local v3, "tag":I
    sparse-switch v3, :sswitch_data_0

    .line 102
    invoke-static {p1, v3}, Lcom/google/protobuf/nano/WireFormatNano;->parseUnknownField(Lcom/google/protobuf/nano/CodedInputByteBufferNano;I)Z

    move-result v5

    if-nez v5, :cond_0

    .line 103
    :sswitch_0
    return-object p0

    .line 108
    :sswitch_1
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readString()Ljava/lang/String;

    move-result-object v5

    iput-object v5, p0, Lcom/google/commerce/payments/orchestration/proto/ui/common/components/tax/TaxInfoFormOuterClass$TaxInfoForm;->id:Ljava/lang/String;

    goto :goto_0

    .line 112
    :sswitch_2
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readString()Ljava/lang/String;

    move-result-object v5

    iput-object v5, p0, Lcom/google/commerce/payments/orchestration/proto/ui/common/components/tax/TaxInfoFormOuterClass$TaxInfoForm;->label:Ljava/lang/String;

    goto :goto_0

    .line 116
    :sswitch_3
    const/16 v5, 0x22

    invoke-static {p1, v5}, Lcom/google/protobuf/nano/WireFormatNano;->getRepeatedFieldArrayLength(Lcom/google/protobuf/nano/CodedInputByteBufferNano;I)I

    move-result v0

    .line 118
    .local v0, "arrayLength":I
    iget-object v5, p0, Lcom/google/commerce/payments/orchestration/proto/ui/common/components/tax/TaxInfoFormOuterClass$TaxInfoForm;->taxInfoField:[Lcom/google/commerce/payments/orchestration/proto/ui/common/generic/UiFieldOuterClass$UiField;

    if-nez v5, :cond_2

    move v1, v4

    .line 119
    .local v1, "i":I
    :goto_1
    add-int v5, v1, v0

    new-array v2, v5, [Lcom/google/commerce/payments/orchestration/proto/ui/common/generic/UiFieldOuterClass$UiField;

    .line 121
    .local v2, "newArray":[Lcom/google/commerce/payments/orchestration/proto/ui/common/generic/UiFieldOuterClass$UiField;
    if-eqz v1, :cond_1

    .line 122
    iget-object v5, p0, Lcom/google/commerce/payments/orchestration/proto/ui/common/components/tax/TaxInfoFormOuterClass$TaxInfoForm;->taxInfoField:[Lcom/google/commerce/payments/orchestration/proto/ui/common/generic/UiFieldOuterClass$UiField;

    invoke-static {v5, v4, v2, v4, v1}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 124
    :cond_1
    :goto_2
    array-length v5, v2

    add-int/lit8 v5, v5, -0x1

    if-ge v1, v5, :cond_3

    .line 125
    new-instance v5, Lcom/google/commerce/payments/orchestration/proto/ui/common/generic/UiFieldOuterClass$UiField;

    invoke-direct {v5}, Lcom/google/commerce/payments/orchestration/proto/ui/common/generic/UiFieldOuterClass$UiField;-><init>()V

    aput-object v5, v2, v1

    .line 126
    aget-object v5, v2, v1

    invoke-virtual {p1, v5}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readMessage(Lcom/google/protobuf/nano/MessageNano;)V

    .line 127
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readTag()I

    .line 124
    add-int/lit8 v1, v1, 0x1

    goto :goto_2

    .line 118
    .end local v1    # "i":I
    .end local v2    # "newArray":[Lcom/google/commerce/payments/orchestration/proto/ui/common/generic/UiFieldOuterClass$UiField;
    :cond_2
    iget-object v5, p0, Lcom/google/commerce/payments/orchestration/proto/ui/common/components/tax/TaxInfoFormOuterClass$TaxInfoForm;->taxInfoField:[Lcom/google/commerce/payments/orchestration/proto/ui/common/generic/UiFieldOuterClass$UiField;

    array-length v1, v5

    goto :goto_1

    .line 130
    .restart local v1    # "i":I
    .restart local v2    # "newArray":[Lcom/google/commerce/payments/orchestration/proto/ui/common/generic/UiFieldOuterClass$UiField;
    :cond_3
    new-instance v5, Lcom/google/commerce/payments/orchestration/proto/ui/common/generic/UiFieldOuterClass$UiField;

    invoke-direct {v5}, Lcom/google/commerce/payments/orchestration/proto/ui/common/generic/UiFieldOuterClass$UiField;-><init>()V

    aput-object v5, v2, v1

    .line 131
    aget-object v5, v2, v1

    invoke-virtual {p1, v5}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readMessage(Lcom/google/protobuf/nano/MessageNano;)V

    .line 132
    iput-object v2, p0, Lcom/google/commerce/payments/orchestration/proto/ui/common/components/tax/TaxInfoFormOuterClass$TaxInfoForm;->taxInfoField:[Lcom/google/commerce/payments/orchestration/proto/ui/common/generic/UiFieldOuterClass$UiField;

    goto :goto_0

    .line 98
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x22 -> :sswitch_3
    .end sparse-switch
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/protobuf/nano/MessageNano;
    .locals 1
    .param p1, "x0"    # Lcom/google/protobuf/nano/CodedInputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 8
    invoke-virtual {p0, p1}, Lcom/google/commerce/payments/orchestration/proto/ui/common/components/tax/TaxInfoFormOuterClass$TaxInfoForm;->mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/commerce/payments/orchestration/proto/ui/common/components/tax/TaxInfoFormOuterClass$TaxInfoForm;

    move-result-object v0

    return-object v0
.end method

.method public writeTo(Lcom/google/protobuf/nano/CodedOutputByteBufferNano;)V
    .locals 4
    .param p1, "output"    # Lcom/google/protobuf/nano/CodedOutputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 52
    iget-object v2, p0, Lcom/google/commerce/payments/orchestration/proto/ui/common/components/tax/TaxInfoFormOuterClass$TaxInfoForm;->id:Ljava/lang/String;

    const-string v3, ""

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 53
    const/4 v2, 0x1

    iget-object v3, p0, Lcom/google/commerce/payments/orchestration/proto/ui/common/components/tax/TaxInfoFormOuterClass$TaxInfoForm;->id:Ljava/lang/String;

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeString(ILjava/lang/String;)V

    .line 55
    :cond_0
    iget-object v2, p0, Lcom/google/commerce/payments/orchestration/proto/ui/common/components/tax/TaxInfoFormOuterClass$TaxInfoForm;->label:Ljava/lang/String;

    const-string v3, ""

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_1

    .line 56
    const/4 v2, 0x2

    iget-object v3, p0, Lcom/google/commerce/payments/orchestration/proto/ui/common/components/tax/TaxInfoFormOuterClass$TaxInfoForm;->label:Ljava/lang/String;

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeString(ILjava/lang/String;)V

    .line 58
    :cond_1
    iget-object v2, p0, Lcom/google/commerce/payments/orchestration/proto/ui/common/components/tax/TaxInfoFormOuterClass$TaxInfoForm;->taxInfoField:[Lcom/google/commerce/payments/orchestration/proto/ui/common/generic/UiFieldOuterClass$UiField;

    if-eqz v2, :cond_3

    iget-object v2, p0, Lcom/google/commerce/payments/orchestration/proto/ui/common/components/tax/TaxInfoFormOuterClass$TaxInfoForm;->taxInfoField:[Lcom/google/commerce/payments/orchestration/proto/ui/common/generic/UiFieldOuterClass$UiField;

    array-length v2, v2

    if-lez v2, :cond_3

    .line 59
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    iget-object v2, p0, Lcom/google/commerce/payments/orchestration/proto/ui/common/components/tax/TaxInfoFormOuterClass$TaxInfoForm;->taxInfoField:[Lcom/google/commerce/payments/orchestration/proto/ui/common/generic/UiFieldOuterClass$UiField;

    array-length v2, v2

    if-ge v1, v2, :cond_3

    .line 60
    iget-object v2, p0, Lcom/google/commerce/payments/orchestration/proto/ui/common/components/tax/TaxInfoFormOuterClass$TaxInfoForm;->taxInfoField:[Lcom/google/commerce/payments/orchestration/proto/ui/common/generic/UiFieldOuterClass$UiField;

    aget-object v0, v2, v1

    .line 61
    .local v0, "element":Lcom/google/commerce/payments/orchestration/proto/ui/common/generic/UiFieldOuterClass$UiField;
    if-eqz v0, :cond_2

    .line 62
    const/4 v2, 0x4

    invoke-virtual {p1, v2, v0}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeMessage(ILcom/google/protobuf/nano/MessageNano;)V

    .line 59
    :cond_2
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 66
    .end local v0    # "element":Lcom/google/commerce/payments/orchestration/proto/ui/common/generic/UiFieldOuterClass$UiField;
    .end local v1    # "i":I
    :cond_3
    invoke-super {p0, p1}, Lcom/google/protobuf/nano/MessageNano;->writeTo(Lcom/google/protobuf/nano/CodedOutputByteBufferNano;)V

    .line 67
    return-void
.end method
