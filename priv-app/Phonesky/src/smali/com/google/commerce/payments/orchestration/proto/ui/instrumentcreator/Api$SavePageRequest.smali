.class public final Lcom/google/commerce/payments/orchestration/proto/ui/instrumentcreator/Api$SavePageRequest;
.super Lcom/google/protobuf/nano/MessageNano;
.source "Api.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/commerce/payments/orchestration/proto/ui/instrumentcreator/Api;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "SavePageRequest"
.end annotation


# instance fields
.field public context:Lcom/google/commerce/payments/orchestration/proto/ui/common/RequestContextOuterClass$RequestContext;

.field public pageValue:Lcom/google/commerce/payments/orchestration/proto/ui/instrumentcreator/Api$PageValue;

.field public parameters:Lcom/google/commerce/payments/orchestration/proto/ui/instrumentcreator/Api$InstrumentManagerParameters;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 454
    invoke-direct {p0}, Lcom/google/protobuf/nano/MessageNano;-><init>()V

    .line 455
    invoke-virtual {p0}, Lcom/google/commerce/payments/orchestration/proto/ui/instrumentcreator/Api$SavePageRequest;->clear()Lcom/google/commerce/payments/orchestration/proto/ui/instrumentcreator/Api$SavePageRequest;

    .line 456
    return-void
.end method


# virtual methods
.method public clear()Lcom/google/commerce/payments/orchestration/proto/ui/instrumentcreator/Api$SavePageRequest;
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 459
    iput-object v0, p0, Lcom/google/commerce/payments/orchestration/proto/ui/instrumentcreator/Api$SavePageRequest;->context:Lcom/google/commerce/payments/orchestration/proto/ui/common/RequestContextOuterClass$RequestContext;

    .line 460
    iput-object v0, p0, Lcom/google/commerce/payments/orchestration/proto/ui/instrumentcreator/Api$SavePageRequest;->parameters:Lcom/google/commerce/payments/orchestration/proto/ui/instrumentcreator/Api$InstrumentManagerParameters;

    .line 461
    iput-object v0, p0, Lcom/google/commerce/payments/orchestration/proto/ui/instrumentcreator/Api$SavePageRequest;->pageValue:Lcom/google/commerce/payments/orchestration/proto/ui/instrumentcreator/Api$PageValue;

    .line 462
    const/4 v0, -0x1

    iput v0, p0, Lcom/google/commerce/payments/orchestration/proto/ui/instrumentcreator/Api$SavePageRequest;->cachedSize:I

    .line 463
    return-object p0
.end method

.method protected computeSerializedSize()I
    .locals 3

    .prologue
    .line 483
    invoke-super {p0}, Lcom/google/protobuf/nano/MessageNano;->computeSerializedSize()I

    move-result v0

    .line 484
    .local v0, "size":I
    iget-object v1, p0, Lcom/google/commerce/payments/orchestration/proto/ui/instrumentcreator/Api$SavePageRequest;->context:Lcom/google/commerce/payments/orchestration/proto/ui/common/RequestContextOuterClass$RequestContext;

    if-eqz v1, :cond_0

    .line 485
    const/4 v1, 0x1

    iget-object v2, p0, Lcom/google/commerce/payments/orchestration/proto/ui/instrumentcreator/Api$SavePageRequest;->context:Lcom/google/commerce/payments/orchestration/proto/ui/common/RequestContextOuterClass$RequestContext;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeMessageSize(ILcom/google/protobuf/nano/MessageNano;)I

    move-result v1

    add-int/2addr v0, v1

    .line 488
    :cond_0
    iget-object v1, p0, Lcom/google/commerce/payments/orchestration/proto/ui/instrumentcreator/Api$SavePageRequest;->parameters:Lcom/google/commerce/payments/orchestration/proto/ui/instrumentcreator/Api$InstrumentManagerParameters;

    if-eqz v1, :cond_1

    .line 489
    const/4 v1, 0x2

    iget-object v2, p0, Lcom/google/commerce/payments/orchestration/proto/ui/instrumentcreator/Api$SavePageRequest;->parameters:Lcom/google/commerce/payments/orchestration/proto/ui/instrumentcreator/Api$InstrumentManagerParameters;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeMessageSize(ILcom/google/protobuf/nano/MessageNano;)I

    move-result v1

    add-int/2addr v0, v1

    .line 492
    :cond_1
    iget-object v1, p0, Lcom/google/commerce/payments/orchestration/proto/ui/instrumentcreator/Api$SavePageRequest;->pageValue:Lcom/google/commerce/payments/orchestration/proto/ui/instrumentcreator/Api$PageValue;

    if-eqz v1, :cond_2

    .line 493
    const/4 v1, 0x4

    iget-object v2, p0, Lcom/google/commerce/payments/orchestration/proto/ui/instrumentcreator/Api$SavePageRequest;->pageValue:Lcom/google/commerce/payments/orchestration/proto/ui/instrumentcreator/Api$PageValue;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeMessageSize(ILcom/google/protobuf/nano/MessageNano;)I

    move-result v1

    add-int/2addr v0, v1

    .line 496
    :cond_2
    return v0
.end method

.method public mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/commerce/payments/orchestration/proto/ui/instrumentcreator/Api$SavePageRequest;
    .locals 2
    .param p1, "input"    # Lcom/google/protobuf/nano/CodedInputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 504
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readTag()I

    move-result v0

    .line 505
    .local v0, "tag":I
    sparse-switch v0, :sswitch_data_0

    .line 509
    invoke-static {p1, v0}, Lcom/google/protobuf/nano/WireFormatNano;->parseUnknownField(Lcom/google/protobuf/nano/CodedInputByteBufferNano;I)Z

    move-result v1

    if-nez v1, :cond_0

    .line 510
    :sswitch_0
    return-object p0

    .line 515
    :sswitch_1
    iget-object v1, p0, Lcom/google/commerce/payments/orchestration/proto/ui/instrumentcreator/Api$SavePageRequest;->context:Lcom/google/commerce/payments/orchestration/proto/ui/common/RequestContextOuterClass$RequestContext;

    if-nez v1, :cond_1

    .line 516
    new-instance v1, Lcom/google/commerce/payments/orchestration/proto/ui/common/RequestContextOuterClass$RequestContext;

    invoke-direct {v1}, Lcom/google/commerce/payments/orchestration/proto/ui/common/RequestContextOuterClass$RequestContext;-><init>()V

    iput-object v1, p0, Lcom/google/commerce/payments/orchestration/proto/ui/instrumentcreator/Api$SavePageRequest;->context:Lcom/google/commerce/payments/orchestration/proto/ui/common/RequestContextOuterClass$RequestContext;

    .line 518
    :cond_1
    iget-object v1, p0, Lcom/google/commerce/payments/orchestration/proto/ui/instrumentcreator/Api$SavePageRequest;->context:Lcom/google/commerce/payments/orchestration/proto/ui/common/RequestContextOuterClass$RequestContext;

    invoke-virtual {p1, v1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readMessage(Lcom/google/protobuf/nano/MessageNano;)V

    goto :goto_0

    .line 522
    :sswitch_2
    iget-object v1, p0, Lcom/google/commerce/payments/orchestration/proto/ui/instrumentcreator/Api$SavePageRequest;->parameters:Lcom/google/commerce/payments/orchestration/proto/ui/instrumentcreator/Api$InstrumentManagerParameters;

    if-nez v1, :cond_2

    .line 523
    new-instance v1, Lcom/google/commerce/payments/orchestration/proto/ui/instrumentcreator/Api$InstrumentManagerParameters;

    invoke-direct {v1}, Lcom/google/commerce/payments/orchestration/proto/ui/instrumentcreator/Api$InstrumentManagerParameters;-><init>()V

    iput-object v1, p0, Lcom/google/commerce/payments/orchestration/proto/ui/instrumentcreator/Api$SavePageRequest;->parameters:Lcom/google/commerce/payments/orchestration/proto/ui/instrumentcreator/Api$InstrumentManagerParameters;

    .line 525
    :cond_2
    iget-object v1, p0, Lcom/google/commerce/payments/orchestration/proto/ui/instrumentcreator/Api$SavePageRequest;->parameters:Lcom/google/commerce/payments/orchestration/proto/ui/instrumentcreator/Api$InstrumentManagerParameters;

    invoke-virtual {p1, v1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readMessage(Lcom/google/protobuf/nano/MessageNano;)V

    goto :goto_0

    .line 529
    :sswitch_3
    iget-object v1, p0, Lcom/google/commerce/payments/orchestration/proto/ui/instrumentcreator/Api$SavePageRequest;->pageValue:Lcom/google/commerce/payments/orchestration/proto/ui/instrumentcreator/Api$PageValue;

    if-nez v1, :cond_3

    .line 530
    new-instance v1, Lcom/google/commerce/payments/orchestration/proto/ui/instrumentcreator/Api$PageValue;

    invoke-direct {v1}, Lcom/google/commerce/payments/orchestration/proto/ui/instrumentcreator/Api$PageValue;-><init>()V

    iput-object v1, p0, Lcom/google/commerce/payments/orchestration/proto/ui/instrumentcreator/Api$SavePageRequest;->pageValue:Lcom/google/commerce/payments/orchestration/proto/ui/instrumentcreator/Api$PageValue;

    .line 532
    :cond_3
    iget-object v1, p0, Lcom/google/commerce/payments/orchestration/proto/ui/instrumentcreator/Api$SavePageRequest;->pageValue:Lcom/google/commerce/payments/orchestration/proto/ui/instrumentcreator/Api$PageValue;

    invoke-virtual {p1, v1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readMessage(Lcom/google/protobuf/nano/MessageNano;)V

    goto :goto_0

    .line 505
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x22 -> :sswitch_3
    .end sparse-switch
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/protobuf/nano/MessageNano;
    .locals 1
    .param p1, "x0"    # Lcom/google/protobuf/nano/CodedInputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 428
    invoke-virtual {p0, p1}, Lcom/google/commerce/payments/orchestration/proto/ui/instrumentcreator/Api$SavePageRequest;->mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/commerce/payments/orchestration/proto/ui/instrumentcreator/Api$SavePageRequest;

    move-result-object v0

    return-object v0
.end method

.method public writeTo(Lcom/google/protobuf/nano/CodedOutputByteBufferNano;)V
    .locals 2
    .param p1, "output"    # Lcom/google/protobuf/nano/CodedOutputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 469
    iget-object v0, p0, Lcom/google/commerce/payments/orchestration/proto/ui/instrumentcreator/Api$SavePageRequest;->context:Lcom/google/commerce/payments/orchestration/proto/ui/common/RequestContextOuterClass$RequestContext;

    if-eqz v0, :cond_0

    .line 470
    const/4 v0, 0x1

    iget-object v1, p0, Lcom/google/commerce/payments/orchestration/proto/ui/instrumentcreator/Api$SavePageRequest;->context:Lcom/google/commerce/payments/orchestration/proto/ui/common/RequestContextOuterClass$RequestContext;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeMessage(ILcom/google/protobuf/nano/MessageNano;)V

    .line 472
    :cond_0
    iget-object v0, p0, Lcom/google/commerce/payments/orchestration/proto/ui/instrumentcreator/Api$SavePageRequest;->parameters:Lcom/google/commerce/payments/orchestration/proto/ui/instrumentcreator/Api$InstrumentManagerParameters;

    if-eqz v0, :cond_1

    .line 473
    const/4 v0, 0x2

    iget-object v1, p0, Lcom/google/commerce/payments/orchestration/proto/ui/instrumentcreator/Api$SavePageRequest;->parameters:Lcom/google/commerce/payments/orchestration/proto/ui/instrumentcreator/Api$InstrumentManagerParameters;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeMessage(ILcom/google/protobuf/nano/MessageNano;)V

    .line 475
    :cond_1
    iget-object v0, p0, Lcom/google/commerce/payments/orchestration/proto/ui/instrumentcreator/Api$SavePageRequest;->pageValue:Lcom/google/commerce/payments/orchestration/proto/ui/instrumentcreator/Api$PageValue;

    if-eqz v0, :cond_2

    .line 476
    const/4 v0, 0x4

    iget-object v1, p0, Lcom/google/commerce/payments/orchestration/proto/ui/instrumentcreator/Api$SavePageRequest;->pageValue:Lcom/google/commerce/payments/orchestration/proto/ui/instrumentcreator/Api$PageValue;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeMessage(ILcom/google/protobuf/nano/MessageNano;)V

    .line 478
    :cond_2
    invoke-super {p0, p1}, Lcom/google/protobuf/nano/MessageNano;->writeTo(Lcom/google/protobuf/nano/CodedOutputByteBufferNano;)V

    .line 479
    return-void
.end method
