.class public final Lcom/google/commerce/payments/orchestration/proto/ui/common/components/instrument/types/CreditCard$CardType;
.super Lcom/google/protobuf/nano/MessageNano;
.source "CreditCard.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/commerce/payments/orchestration/proto/ui/common/components/instrument/types/CreditCard;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "CardType"
.end annotation


# static fields
.field private static volatile _emptyArray:[Lcom/google/commerce/payments/orchestration/proto/ui/common/components/instrument/types/CreditCard$CardType;


# instance fields
.field public binRange:[Lcom/google/commerce/payments/orchestration/proto/ui/common/components/instrument/types/CreditCard$BinRange;

.field public cvcLength:I

.field public icon:Lcom/google/commerce/payments/orchestration/proto/ui/common/generic/ImageWithCaptionOuterClass$ImageWithCaption;

.field public typeToken:[B


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 467
    invoke-direct {p0}, Lcom/google/protobuf/nano/MessageNano;-><init>()V

    .line 468
    invoke-virtual {p0}, Lcom/google/commerce/payments/orchestration/proto/ui/common/components/instrument/types/CreditCard$CardType;->clear()Lcom/google/commerce/payments/orchestration/proto/ui/common/components/instrument/types/CreditCard$CardType;

    .line 469
    return-void
.end method

.method public static emptyArray()[Lcom/google/commerce/payments/orchestration/proto/ui/common/components/instrument/types/CreditCard$CardType;
    .locals 2

    .prologue
    .line 444
    sget-object v0, Lcom/google/commerce/payments/orchestration/proto/ui/common/components/instrument/types/CreditCard$CardType;->_emptyArray:[Lcom/google/commerce/payments/orchestration/proto/ui/common/components/instrument/types/CreditCard$CardType;

    if-nez v0, :cond_1

    .line 445
    sget-object v1, Lcom/google/protobuf/nano/InternalNano;->LAZY_INIT_LOCK:Ljava/lang/Object;

    monitor-enter v1

    .line 447
    :try_start_0
    sget-object v0, Lcom/google/commerce/payments/orchestration/proto/ui/common/components/instrument/types/CreditCard$CardType;->_emptyArray:[Lcom/google/commerce/payments/orchestration/proto/ui/common/components/instrument/types/CreditCard$CardType;

    if-nez v0, :cond_0

    .line 448
    const/4 v0, 0x0

    new-array v0, v0, [Lcom/google/commerce/payments/orchestration/proto/ui/common/components/instrument/types/CreditCard$CardType;

    sput-object v0, Lcom/google/commerce/payments/orchestration/proto/ui/common/components/instrument/types/CreditCard$CardType;->_emptyArray:[Lcom/google/commerce/payments/orchestration/proto/ui/common/components/instrument/types/CreditCard$CardType;

    .line 450
    :cond_0
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 452
    :cond_1
    sget-object v0, Lcom/google/commerce/payments/orchestration/proto/ui/common/components/instrument/types/CreditCard$CardType;->_emptyArray:[Lcom/google/commerce/payments/orchestration/proto/ui/common/components/instrument/types/CreditCard$CardType;

    return-object v0

    .line 450
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method


# virtual methods
.method public clear()Lcom/google/commerce/payments/orchestration/proto/ui/common/components/instrument/types/CreditCard$CardType;
    .locals 1

    .prologue
    .line 472
    invoke-static {}, Lcom/google/commerce/payments/orchestration/proto/ui/common/components/instrument/types/CreditCard$BinRange;->emptyArray()[Lcom/google/commerce/payments/orchestration/proto/ui/common/components/instrument/types/CreditCard$BinRange;

    move-result-object v0

    iput-object v0, p0, Lcom/google/commerce/payments/orchestration/proto/ui/common/components/instrument/types/CreditCard$CardType;->binRange:[Lcom/google/commerce/payments/orchestration/proto/ui/common/components/instrument/types/CreditCard$BinRange;

    .line 473
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/commerce/payments/orchestration/proto/ui/common/components/instrument/types/CreditCard$CardType;->icon:Lcom/google/commerce/payments/orchestration/proto/ui/common/generic/ImageWithCaptionOuterClass$ImageWithCaption;

    .line 474
    const/4 v0, 0x0

    iput v0, p0, Lcom/google/commerce/payments/orchestration/proto/ui/common/components/instrument/types/CreditCard$CardType;->cvcLength:I

    .line 475
    sget-object v0, Lcom/google/protobuf/nano/WireFormatNano;->EMPTY_BYTES:[B

    iput-object v0, p0, Lcom/google/commerce/payments/orchestration/proto/ui/common/components/instrument/types/CreditCard$CardType;->typeToken:[B

    .line 476
    const/4 v0, -0x1

    iput v0, p0, Lcom/google/commerce/payments/orchestration/proto/ui/common/components/instrument/types/CreditCard$CardType;->cachedSize:I

    .line 477
    return-object p0
.end method

.method protected computeSerializedSize()I
    .locals 5

    .prologue
    .line 505
    invoke-super {p0}, Lcom/google/protobuf/nano/MessageNano;->computeSerializedSize()I

    move-result v2

    .line 506
    .local v2, "size":I
    iget-object v3, p0, Lcom/google/commerce/payments/orchestration/proto/ui/common/components/instrument/types/CreditCard$CardType;->binRange:[Lcom/google/commerce/payments/orchestration/proto/ui/common/components/instrument/types/CreditCard$BinRange;

    if-eqz v3, :cond_1

    iget-object v3, p0, Lcom/google/commerce/payments/orchestration/proto/ui/common/components/instrument/types/CreditCard$CardType;->binRange:[Lcom/google/commerce/payments/orchestration/proto/ui/common/components/instrument/types/CreditCard$BinRange;

    array-length v3, v3

    if-lez v3, :cond_1

    .line 507
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    iget-object v3, p0, Lcom/google/commerce/payments/orchestration/proto/ui/common/components/instrument/types/CreditCard$CardType;->binRange:[Lcom/google/commerce/payments/orchestration/proto/ui/common/components/instrument/types/CreditCard$BinRange;

    array-length v3, v3

    if-ge v1, v3, :cond_1

    .line 508
    iget-object v3, p0, Lcom/google/commerce/payments/orchestration/proto/ui/common/components/instrument/types/CreditCard$CardType;->binRange:[Lcom/google/commerce/payments/orchestration/proto/ui/common/components/instrument/types/CreditCard$BinRange;

    aget-object v0, v3, v1

    .line 509
    .local v0, "element":Lcom/google/commerce/payments/orchestration/proto/ui/common/components/instrument/types/CreditCard$BinRange;
    if-eqz v0, :cond_0

    .line 510
    const/4 v3, 0x1

    invoke-static {v3, v0}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeMessageSize(ILcom/google/protobuf/nano/MessageNano;)I

    move-result v3

    add-int/2addr v2, v3

    .line 507
    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 515
    .end local v0    # "element":Lcom/google/commerce/payments/orchestration/proto/ui/common/components/instrument/types/CreditCard$BinRange;
    .end local v1    # "i":I
    :cond_1
    iget v3, p0, Lcom/google/commerce/payments/orchestration/proto/ui/common/components/instrument/types/CreditCard$CardType;->cvcLength:I

    if-eqz v3, :cond_2

    .line 516
    const/4 v3, 0x3

    iget v4, p0, Lcom/google/commerce/payments/orchestration/proto/ui/common/components/instrument/types/CreditCard$CardType;->cvcLength:I

    invoke-static {v3, v4}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeInt32Size(II)I

    move-result v3

    add-int/2addr v2, v3

    .line 519
    :cond_2
    iget-object v3, p0, Lcom/google/commerce/payments/orchestration/proto/ui/common/components/instrument/types/CreditCard$CardType;->typeToken:[B

    sget-object v4, Lcom/google/protobuf/nano/WireFormatNano;->EMPTY_BYTES:[B

    invoke-static {v3, v4}, Ljava/util/Arrays;->equals([B[B)Z

    move-result v3

    if-nez v3, :cond_3

    .line 520
    const/4 v3, 0x4

    iget-object v4, p0, Lcom/google/commerce/payments/orchestration/proto/ui/common/components/instrument/types/CreditCard$CardType;->typeToken:[B

    invoke-static {v3, v4}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeBytesSize(I[B)I

    move-result v3

    add-int/2addr v2, v3

    .line 523
    :cond_3
    iget-object v3, p0, Lcom/google/commerce/payments/orchestration/proto/ui/common/components/instrument/types/CreditCard$CardType;->icon:Lcom/google/commerce/payments/orchestration/proto/ui/common/generic/ImageWithCaptionOuterClass$ImageWithCaption;

    if-eqz v3, :cond_4

    .line 524
    const/16 v3, 0x9

    iget-object v4, p0, Lcom/google/commerce/payments/orchestration/proto/ui/common/components/instrument/types/CreditCard$CardType;->icon:Lcom/google/commerce/payments/orchestration/proto/ui/common/generic/ImageWithCaptionOuterClass$ImageWithCaption;

    invoke-static {v3, v4}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeMessageSize(ILcom/google/protobuf/nano/MessageNano;)I

    move-result v3

    add-int/2addr v2, v3

    .line 527
    :cond_4
    return v2
.end method

.method public mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/commerce/payments/orchestration/proto/ui/common/components/instrument/types/CreditCard$CardType;
    .locals 6
    .param p1, "input"    # Lcom/google/protobuf/nano/CodedInputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v4, 0x0

    .line 535
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readTag()I

    move-result v3

    .line 536
    .local v3, "tag":I
    sparse-switch v3, :sswitch_data_0

    .line 540
    invoke-static {p1, v3}, Lcom/google/protobuf/nano/WireFormatNano;->parseUnknownField(Lcom/google/protobuf/nano/CodedInputByteBufferNano;I)Z

    move-result v5

    if-nez v5, :cond_0

    .line 541
    :sswitch_0
    return-object p0

    .line 546
    :sswitch_1
    const/16 v5, 0xa

    invoke-static {p1, v5}, Lcom/google/protobuf/nano/WireFormatNano;->getRepeatedFieldArrayLength(Lcom/google/protobuf/nano/CodedInputByteBufferNano;I)I

    move-result v0

    .line 548
    .local v0, "arrayLength":I
    iget-object v5, p0, Lcom/google/commerce/payments/orchestration/proto/ui/common/components/instrument/types/CreditCard$CardType;->binRange:[Lcom/google/commerce/payments/orchestration/proto/ui/common/components/instrument/types/CreditCard$BinRange;

    if-nez v5, :cond_2

    move v1, v4

    .line 549
    .local v1, "i":I
    :goto_1
    add-int v5, v1, v0

    new-array v2, v5, [Lcom/google/commerce/payments/orchestration/proto/ui/common/components/instrument/types/CreditCard$BinRange;

    .line 551
    .local v2, "newArray":[Lcom/google/commerce/payments/orchestration/proto/ui/common/components/instrument/types/CreditCard$BinRange;
    if-eqz v1, :cond_1

    .line 552
    iget-object v5, p0, Lcom/google/commerce/payments/orchestration/proto/ui/common/components/instrument/types/CreditCard$CardType;->binRange:[Lcom/google/commerce/payments/orchestration/proto/ui/common/components/instrument/types/CreditCard$BinRange;

    invoke-static {v5, v4, v2, v4, v1}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 554
    :cond_1
    :goto_2
    array-length v5, v2

    add-int/lit8 v5, v5, -0x1

    if-ge v1, v5, :cond_3

    .line 555
    new-instance v5, Lcom/google/commerce/payments/orchestration/proto/ui/common/components/instrument/types/CreditCard$BinRange;

    invoke-direct {v5}, Lcom/google/commerce/payments/orchestration/proto/ui/common/components/instrument/types/CreditCard$BinRange;-><init>()V

    aput-object v5, v2, v1

    .line 556
    aget-object v5, v2, v1

    invoke-virtual {p1, v5}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readMessage(Lcom/google/protobuf/nano/MessageNano;)V

    .line 557
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readTag()I

    .line 554
    add-int/lit8 v1, v1, 0x1

    goto :goto_2

    .line 548
    .end local v1    # "i":I
    .end local v2    # "newArray":[Lcom/google/commerce/payments/orchestration/proto/ui/common/components/instrument/types/CreditCard$BinRange;
    :cond_2
    iget-object v5, p0, Lcom/google/commerce/payments/orchestration/proto/ui/common/components/instrument/types/CreditCard$CardType;->binRange:[Lcom/google/commerce/payments/orchestration/proto/ui/common/components/instrument/types/CreditCard$BinRange;

    array-length v1, v5

    goto :goto_1

    .line 560
    .restart local v1    # "i":I
    .restart local v2    # "newArray":[Lcom/google/commerce/payments/orchestration/proto/ui/common/components/instrument/types/CreditCard$BinRange;
    :cond_3
    new-instance v5, Lcom/google/commerce/payments/orchestration/proto/ui/common/components/instrument/types/CreditCard$BinRange;

    invoke-direct {v5}, Lcom/google/commerce/payments/orchestration/proto/ui/common/components/instrument/types/CreditCard$BinRange;-><init>()V

    aput-object v5, v2, v1

    .line 561
    aget-object v5, v2, v1

    invoke-virtual {p1, v5}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readMessage(Lcom/google/protobuf/nano/MessageNano;)V

    .line 562
    iput-object v2, p0, Lcom/google/commerce/payments/orchestration/proto/ui/common/components/instrument/types/CreditCard$CardType;->binRange:[Lcom/google/commerce/payments/orchestration/proto/ui/common/components/instrument/types/CreditCard$BinRange;

    goto :goto_0

    .line 566
    .end local v0    # "arrayLength":I
    .end local v1    # "i":I
    .end local v2    # "newArray":[Lcom/google/commerce/payments/orchestration/proto/ui/common/components/instrument/types/CreditCard$BinRange;
    :sswitch_2
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readInt32()I

    move-result v5

    iput v5, p0, Lcom/google/commerce/payments/orchestration/proto/ui/common/components/instrument/types/CreditCard$CardType;->cvcLength:I

    goto :goto_0

    .line 570
    :sswitch_3
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readBytes()[B

    move-result-object v5

    iput-object v5, p0, Lcom/google/commerce/payments/orchestration/proto/ui/common/components/instrument/types/CreditCard$CardType;->typeToken:[B

    goto :goto_0

    .line 574
    :sswitch_4
    iget-object v5, p0, Lcom/google/commerce/payments/orchestration/proto/ui/common/components/instrument/types/CreditCard$CardType;->icon:Lcom/google/commerce/payments/orchestration/proto/ui/common/generic/ImageWithCaptionOuterClass$ImageWithCaption;

    if-nez v5, :cond_4

    .line 575
    new-instance v5, Lcom/google/commerce/payments/orchestration/proto/ui/common/generic/ImageWithCaptionOuterClass$ImageWithCaption;

    invoke-direct {v5}, Lcom/google/commerce/payments/orchestration/proto/ui/common/generic/ImageWithCaptionOuterClass$ImageWithCaption;-><init>()V

    iput-object v5, p0, Lcom/google/commerce/payments/orchestration/proto/ui/common/components/instrument/types/CreditCard$CardType;->icon:Lcom/google/commerce/payments/orchestration/proto/ui/common/generic/ImageWithCaptionOuterClass$ImageWithCaption;

    .line 577
    :cond_4
    iget-object v5, p0, Lcom/google/commerce/payments/orchestration/proto/ui/common/components/instrument/types/CreditCard$CardType;->icon:Lcom/google/commerce/payments/orchestration/proto/ui/common/generic/ImageWithCaptionOuterClass$ImageWithCaption;

    invoke-virtual {p1, v5}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readMessage(Lcom/google/protobuf/nano/MessageNano;)V

    goto :goto_0

    .line 536
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x18 -> :sswitch_2
        0x22 -> :sswitch_3
        0x4a -> :sswitch_4
    .end sparse-switch
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/protobuf/nano/MessageNano;
    .locals 1
    .param p1, "x0"    # Lcom/google/protobuf/nano/CodedInputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 438
    invoke-virtual {p0, p1}, Lcom/google/commerce/payments/orchestration/proto/ui/common/components/instrument/types/CreditCard$CardType;->mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/commerce/payments/orchestration/proto/ui/common/components/instrument/types/CreditCard$CardType;

    move-result-object v0

    return-object v0
.end method

.method public writeTo(Lcom/google/protobuf/nano/CodedOutputByteBufferNano;)V
    .locals 4
    .param p1, "output"    # Lcom/google/protobuf/nano/CodedOutputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 483
    iget-object v2, p0, Lcom/google/commerce/payments/orchestration/proto/ui/common/components/instrument/types/CreditCard$CardType;->binRange:[Lcom/google/commerce/payments/orchestration/proto/ui/common/components/instrument/types/CreditCard$BinRange;

    if-eqz v2, :cond_1

    iget-object v2, p0, Lcom/google/commerce/payments/orchestration/proto/ui/common/components/instrument/types/CreditCard$CardType;->binRange:[Lcom/google/commerce/payments/orchestration/proto/ui/common/components/instrument/types/CreditCard$BinRange;

    array-length v2, v2

    if-lez v2, :cond_1

    .line 484
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    iget-object v2, p0, Lcom/google/commerce/payments/orchestration/proto/ui/common/components/instrument/types/CreditCard$CardType;->binRange:[Lcom/google/commerce/payments/orchestration/proto/ui/common/components/instrument/types/CreditCard$BinRange;

    array-length v2, v2

    if-ge v1, v2, :cond_1

    .line 485
    iget-object v2, p0, Lcom/google/commerce/payments/orchestration/proto/ui/common/components/instrument/types/CreditCard$CardType;->binRange:[Lcom/google/commerce/payments/orchestration/proto/ui/common/components/instrument/types/CreditCard$BinRange;

    aget-object v0, v2, v1

    .line 486
    .local v0, "element":Lcom/google/commerce/payments/orchestration/proto/ui/common/components/instrument/types/CreditCard$BinRange;
    if-eqz v0, :cond_0

    .line 487
    const/4 v2, 0x1

    invoke-virtual {p1, v2, v0}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeMessage(ILcom/google/protobuf/nano/MessageNano;)V

    .line 484
    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 491
    .end local v0    # "element":Lcom/google/commerce/payments/orchestration/proto/ui/common/components/instrument/types/CreditCard$BinRange;
    .end local v1    # "i":I
    :cond_1
    iget v2, p0, Lcom/google/commerce/payments/orchestration/proto/ui/common/components/instrument/types/CreditCard$CardType;->cvcLength:I

    if-eqz v2, :cond_2

    .line 492
    const/4 v2, 0x3

    iget v3, p0, Lcom/google/commerce/payments/orchestration/proto/ui/common/components/instrument/types/CreditCard$CardType;->cvcLength:I

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeInt32(II)V

    .line 494
    :cond_2
    iget-object v2, p0, Lcom/google/commerce/payments/orchestration/proto/ui/common/components/instrument/types/CreditCard$CardType;->typeToken:[B

    sget-object v3, Lcom/google/protobuf/nano/WireFormatNano;->EMPTY_BYTES:[B

    invoke-static {v2, v3}, Ljava/util/Arrays;->equals([B[B)Z

    move-result v2

    if-nez v2, :cond_3

    .line 495
    const/4 v2, 0x4

    iget-object v3, p0, Lcom/google/commerce/payments/orchestration/proto/ui/common/components/instrument/types/CreditCard$CardType;->typeToken:[B

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeBytes(I[B)V

    .line 497
    :cond_3
    iget-object v2, p0, Lcom/google/commerce/payments/orchestration/proto/ui/common/components/instrument/types/CreditCard$CardType;->icon:Lcom/google/commerce/payments/orchestration/proto/ui/common/generic/ImageWithCaptionOuterClass$ImageWithCaption;

    if-eqz v2, :cond_4

    .line 498
    const/16 v2, 0x9

    iget-object v3, p0, Lcom/google/commerce/payments/orchestration/proto/ui/common/components/instrument/types/CreditCard$CardType;->icon:Lcom/google/commerce/payments/orchestration/proto/ui/common/generic/ImageWithCaptionOuterClass$ImageWithCaption;

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeMessage(ILcom/google/protobuf/nano/MessageNano;)V

    .line 500
    :cond_4
    invoke-super {p0, p1}, Lcom/google/protobuf/nano/MessageNano;->writeTo(Lcom/google/protobuf/nano/CodedOutputByteBufferNano;)V

    .line 501
    return-void
.end method
