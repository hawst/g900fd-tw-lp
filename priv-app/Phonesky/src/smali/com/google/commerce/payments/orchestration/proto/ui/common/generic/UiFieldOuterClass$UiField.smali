.class public final Lcom/google/commerce/payments/orchestration/proto/ui/common/generic/UiFieldOuterClass$UiField;
.super Lcom/google/protobuf/nano/MessageNano;
.source "UiFieldOuterClass.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/commerce/payments/orchestration/proto/ui/common/generic/UiFieldOuterClass;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "UiField"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/commerce/payments/orchestration/proto/ui/common/generic/UiFieldOuterClass$UiField$TextField;
    }
.end annotation


# static fields
.field private static volatile _emptyArray:[Lcom/google/commerce/payments/orchestration/proto/ui/common/generic/UiFieldOuterClass$UiField;


# instance fields
.field public isOptional:Z

.field public label:Ljava/lang/String;

.field public name:Ljava/lang/String;

.field public textField:Lcom/google/commerce/payments/orchestration/proto/ui/common/generic/UiFieldOuterClass$UiField$TextField;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 320
    invoke-direct {p0}, Lcom/google/protobuf/nano/MessageNano;-><init>()V

    .line 321
    invoke-virtual {p0}, Lcom/google/commerce/payments/orchestration/proto/ui/common/generic/UiFieldOuterClass$UiField;->clear()Lcom/google/commerce/payments/orchestration/proto/ui/common/generic/UiFieldOuterClass$UiField;

    .line 322
    return-void
.end method

.method public static emptyArray()[Lcom/google/commerce/payments/orchestration/proto/ui/common/generic/UiFieldOuterClass$UiField;
    .locals 2

    .prologue
    .line 297
    sget-object v0, Lcom/google/commerce/payments/orchestration/proto/ui/common/generic/UiFieldOuterClass$UiField;->_emptyArray:[Lcom/google/commerce/payments/orchestration/proto/ui/common/generic/UiFieldOuterClass$UiField;

    if-nez v0, :cond_1

    .line 298
    sget-object v1, Lcom/google/protobuf/nano/InternalNano;->LAZY_INIT_LOCK:Ljava/lang/Object;

    monitor-enter v1

    .line 300
    :try_start_0
    sget-object v0, Lcom/google/commerce/payments/orchestration/proto/ui/common/generic/UiFieldOuterClass$UiField;->_emptyArray:[Lcom/google/commerce/payments/orchestration/proto/ui/common/generic/UiFieldOuterClass$UiField;

    if-nez v0, :cond_0

    .line 301
    const/4 v0, 0x0

    new-array v0, v0, [Lcom/google/commerce/payments/orchestration/proto/ui/common/generic/UiFieldOuterClass$UiField;

    sput-object v0, Lcom/google/commerce/payments/orchestration/proto/ui/common/generic/UiFieldOuterClass$UiField;->_emptyArray:[Lcom/google/commerce/payments/orchestration/proto/ui/common/generic/UiFieldOuterClass$UiField;

    .line 303
    :cond_0
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 305
    :cond_1
    sget-object v0, Lcom/google/commerce/payments/orchestration/proto/ui/common/generic/UiFieldOuterClass$UiField;->_emptyArray:[Lcom/google/commerce/payments/orchestration/proto/ui/common/generic/UiFieldOuterClass$UiField;

    return-object v0

    .line 303
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method


# virtual methods
.method public clear()Lcom/google/commerce/payments/orchestration/proto/ui/common/generic/UiFieldOuterClass$UiField;
    .locals 1

    .prologue
    .line 325
    const-string v0, ""

    iput-object v0, p0, Lcom/google/commerce/payments/orchestration/proto/ui/common/generic/UiFieldOuterClass$UiField;->name:Ljava/lang/String;

    .line 326
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/commerce/payments/orchestration/proto/ui/common/generic/UiFieldOuterClass$UiField;->isOptional:Z

    .line 327
    const-string v0, ""

    iput-object v0, p0, Lcom/google/commerce/payments/orchestration/proto/ui/common/generic/UiFieldOuterClass$UiField;->label:Ljava/lang/String;

    .line 328
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/commerce/payments/orchestration/proto/ui/common/generic/UiFieldOuterClass$UiField;->textField:Lcom/google/commerce/payments/orchestration/proto/ui/common/generic/UiFieldOuterClass$UiField$TextField;

    .line 329
    const/4 v0, -0x1

    iput v0, p0, Lcom/google/commerce/payments/orchestration/proto/ui/common/generic/UiFieldOuterClass$UiField;->cachedSize:I

    .line 330
    return-object p0
.end method

.method protected computeSerializedSize()I
    .locals 3

    .prologue
    .line 353
    invoke-super {p0}, Lcom/google/protobuf/nano/MessageNano;->computeSerializedSize()I

    move-result v0

    .line 354
    .local v0, "size":I
    iget-object v1, p0, Lcom/google/commerce/payments/orchestration/proto/ui/common/generic/UiFieldOuterClass$UiField;->name:Ljava/lang/String;

    const-string v2, ""

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 355
    const/4 v1, 0x1

    iget-object v2, p0, Lcom/google/commerce/payments/orchestration/proto/ui/common/generic/UiFieldOuterClass$UiField;->name:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeStringSize(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 358
    :cond_0
    iget-boolean v1, p0, Lcom/google/commerce/payments/orchestration/proto/ui/common/generic/UiFieldOuterClass$UiField;->isOptional:Z

    if-eqz v1, :cond_1

    .line 359
    const/4 v1, 0x3

    iget-boolean v2, p0, Lcom/google/commerce/payments/orchestration/proto/ui/common/generic/UiFieldOuterClass$UiField;->isOptional:Z

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeBoolSize(IZ)I

    move-result v1

    add-int/2addr v0, v1

    .line 362
    :cond_1
    iget-object v1, p0, Lcom/google/commerce/payments/orchestration/proto/ui/common/generic/UiFieldOuterClass$UiField;->label:Ljava/lang/String;

    const-string v2, ""

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_2

    .line 363
    const/4 v1, 0x4

    iget-object v2, p0, Lcom/google/commerce/payments/orchestration/proto/ui/common/generic/UiFieldOuterClass$UiField;->label:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeStringSize(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 366
    :cond_2
    iget-object v1, p0, Lcom/google/commerce/payments/orchestration/proto/ui/common/generic/UiFieldOuterClass$UiField;->textField:Lcom/google/commerce/payments/orchestration/proto/ui/common/generic/UiFieldOuterClass$UiField$TextField;

    if-eqz v1, :cond_3

    .line 367
    const/4 v1, 0x6

    iget-object v2, p0, Lcom/google/commerce/payments/orchestration/proto/ui/common/generic/UiFieldOuterClass$UiField;->textField:Lcom/google/commerce/payments/orchestration/proto/ui/common/generic/UiFieldOuterClass$UiField$TextField;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeMessageSize(ILcom/google/protobuf/nano/MessageNano;)I

    move-result v1

    add-int/2addr v0, v1

    .line 370
    :cond_3
    return v0
.end method

.method public mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/commerce/payments/orchestration/proto/ui/common/generic/UiFieldOuterClass$UiField;
    .locals 2
    .param p1, "input"    # Lcom/google/protobuf/nano/CodedInputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 378
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readTag()I

    move-result v0

    .line 379
    .local v0, "tag":I
    sparse-switch v0, :sswitch_data_0

    .line 383
    invoke-static {p1, v0}, Lcom/google/protobuf/nano/WireFormatNano;->parseUnknownField(Lcom/google/protobuf/nano/CodedInputByteBufferNano;I)Z

    move-result v1

    if-nez v1, :cond_0

    .line 384
    :sswitch_0
    return-object p0

    .line 389
    :sswitch_1
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readString()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/google/commerce/payments/orchestration/proto/ui/common/generic/UiFieldOuterClass$UiField;->name:Ljava/lang/String;

    goto :goto_0

    .line 393
    :sswitch_2
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readBool()Z

    move-result v1

    iput-boolean v1, p0, Lcom/google/commerce/payments/orchestration/proto/ui/common/generic/UiFieldOuterClass$UiField;->isOptional:Z

    goto :goto_0

    .line 397
    :sswitch_3
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readString()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/google/commerce/payments/orchestration/proto/ui/common/generic/UiFieldOuterClass$UiField;->label:Ljava/lang/String;

    goto :goto_0

    .line 401
    :sswitch_4
    iget-object v1, p0, Lcom/google/commerce/payments/orchestration/proto/ui/common/generic/UiFieldOuterClass$UiField;->textField:Lcom/google/commerce/payments/orchestration/proto/ui/common/generic/UiFieldOuterClass$UiField$TextField;

    if-nez v1, :cond_1

    .line 402
    new-instance v1, Lcom/google/commerce/payments/orchestration/proto/ui/common/generic/UiFieldOuterClass$UiField$TextField;

    invoke-direct {v1}, Lcom/google/commerce/payments/orchestration/proto/ui/common/generic/UiFieldOuterClass$UiField$TextField;-><init>()V

    iput-object v1, p0, Lcom/google/commerce/payments/orchestration/proto/ui/common/generic/UiFieldOuterClass$UiField;->textField:Lcom/google/commerce/payments/orchestration/proto/ui/common/generic/UiFieldOuterClass$UiField$TextField;

    .line 404
    :cond_1
    iget-object v1, p0, Lcom/google/commerce/payments/orchestration/proto/ui/common/generic/UiFieldOuterClass$UiField;->textField:Lcom/google/commerce/payments/orchestration/proto/ui/common/generic/UiFieldOuterClass$UiField$TextField;

    invoke-virtual {p1, v1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readMessage(Lcom/google/protobuf/nano/MessageNano;)V

    goto :goto_0

    .line 379
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x18 -> :sswitch_2
        0x22 -> :sswitch_3
        0x32 -> :sswitch_4
    .end sparse-switch
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/protobuf/nano/MessageNano;
    .locals 1
    .param p1, "x0"    # Lcom/google/protobuf/nano/CodedInputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 8
    invoke-virtual {p0, p1}, Lcom/google/commerce/payments/orchestration/proto/ui/common/generic/UiFieldOuterClass$UiField;->mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/commerce/payments/orchestration/proto/ui/common/generic/UiFieldOuterClass$UiField;

    move-result-object v0

    return-object v0
.end method

.method public writeTo(Lcom/google/protobuf/nano/CodedOutputByteBufferNano;)V
    .locals 2
    .param p1, "output"    # Lcom/google/protobuf/nano/CodedOutputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 336
    iget-object v0, p0, Lcom/google/commerce/payments/orchestration/proto/ui/common/generic/UiFieldOuterClass$UiField;->name:Ljava/lang/String;

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 337
    const/4 v0, 0x1

    iget-object v1, p0, Lcom/google/commerce/payments/orchestration/proto/ui/common/generic/UiFieldOuterClass$UiField;->name:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeString(ILjava/lang/String;)V

    .line 339
    :cond_0
    iget-boolean v0, p0, Lcom/google/commerce/payments/orchestration/proto/ui/common/generic/UiFieldOuterClass$UiField;->isOptional:Z

    if-eqz v0, :cond_1

    .line 340
    const/4 v0, 0x3

    iget-boolean v1, p0, Lcom/google/commerce/payments/orchestration/proto/ui/common/generic/UiFieldOuterClass$UiField;->isOptional:Z

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeBool(IZ)V

    .line 342
    :cond_1
    iget-object v0, p0, Lcom/google/commerce/payments/orchestration/proto/ui/common/generic/UiFieldOuterClass$UiField;->label:Ljava/lang/String;

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_2

    .line 343
    const/4 v0, 0x4

    iget-object v1, p0, Lcom/google/commerce/payments/orchestration/proto/ui/common/generic/UiFieldOuterClass$UiField;->label:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeString(ILjava/lang/String;)V

    .line 345
    :cond_2
    iget-object v0, p0, Lcom/google/commerce/payments/orchestration/proto/ui/common/generic/UiFieldOuterClass$UiField;->textField:Lcom/google/commerce/payments/orchestration/proto/ui/common/generic/UiFieldOuterClass$UiField$TextField;

    if-eqz v0, :cond_3

    .line 346
    const/4 v0, 0x6

    iget-object v1, p0, Lcom/google/commerce/payments/orchestration/proto/ui/common/generic/UiFieldOuterClass$UiField;->textField:Lcom/google/commerce/payments/orchestration/proto/ui/common/generic/UiFieldOuterClass$UiField$TextField;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeMessage(ILcom/google/protobuf/nano/MessageNano;)V

    .line 348
    :cond_3
    invoke-super {p0, p1}, Lcom/google/protobuf/nano/MessageNano;->writeTo(Lcom/google/protobuf/nano/CodedOutputByteBufferNano;)V

    .line 349
    return-void
.end method
