.class public final Lcom/google/commerce/payments/orchestration/proto/ui/common/components/instrument/types/UsernamePassword$UsernamePasswordFormValue;
.super Lcom/google/protobuf/nano/MessageNano;
.source "UsernamePassword.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/commerce/payments/orchestration/proto/ui/common/components/instrument/types/UsernamePassword;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "UsernamePasswordFormValue"
.end annotation


# instance fields
.field public encryptionType:I

.field public hashedDeviceId:Ljava/lang/String;

.field public legalDocData:Ljava/lang/String;

.field public password:Lcom/google/commerce/payments/orchestration/proto/ui/common/generic/UiFieldOuterClass$UiFieldValue;

.field public username:Lcom/google/commerce/payments/orchestration/proto/ui/common/generic/UiFieldOuterClass$UiFieldValue;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 269
    invoke-direct {p0}, Lcom/google/protobuf/nano/MessageNano;-><init>()V

    .line 270
    invoke-virtual {p0}, Lcom/google/commerce/payments/orchestration/proto/ui/common/components/instrument/types/UsernamePassword$UsernamePasswordFormValue;->clear()Lcom/google/commerce/payments/orchestration/proto/ui/common/components/instrument/types/UsernamePassword$UsernamePasswordFormValue;

    .line 271
    return-void
.end method


# virtual methods
.method public clear()Lcom/google/commerce/payments/orchestration/proto/ui/common/components/instrument/types/UsernamePassword$UsernamePasswordFormValue;
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 274
    iput-object v0, p0, Lcom/google/commerce/payments/orchestration/proto/ui/common/components/instrument/types/UsernamePassword$UsernamePasswordFormValue;->username:Lcom/google/commerce/payments/orchestration/proto/ui/common/generic/UiFieldOuterClass$UiFieldValue;

    .line 275
    iput-object v0, p0, Lcom/google/commerce/payments/orchestration/proto/ui/common/components/instrument/types/UsernamePassword$UsernamePasswordFormValue;->password:Lcom/google/commerce/payments/orchestration/proto/ui/common/generic/UiFieldOuterClass$UiFieldValue;

    .line 276
    const-string v0, ""

    iput-object v0, p0, Lcom/google/commerce/payments/orchestration/proto/ui/common/components/instrument/types/UsernamePassword$UsernamePasswordFormValue;->hashedDeviceId:Ljava/lang/String;

    .line 277
    const/4 v0, 0x0

    iput v0, p0, Lcom/google/commerce/payments/orchestration/proto/ui/common/components/instrument/types/UsernamePassword$UsernamePasswordFormValue;->encryptionType:I

    .line 278
    const-string v0, ""

    iput-object v0, p0, Lcom/google/commerce/payments/orchestration/proto/ui/common/components/instrument/types/UsernamePassword$UsernamePasswordFormValue;->legalDocData:Ljava/lang/String;

    .line 279
    const/4 v0, -0x1

    iput v0, p0, Lcom/google/commerce/payments/orchestration/proto/ui/common/components/instrument/types/UsernamePassword$UsernamePasswordFormValue;->cachedSize:I

    .line 280
    return-object p0
.end method

.method protected computeSerializedSize()I
    .locals 3

    .prologue
    .line 306
    invoke-super {p0}, Lcom/google/protobuf/nano/MessageNano;->computeSerializedSize()I

    move-result v0

    .line 307
    .local v0, "size":I
    iget-object v1, p0, Lcom/google/commerce/payments/orchestration/proto/ui/common/components/instrument/types/UsernamePassword$UsernamePasswordFormValue;->username:Lcom/google/commerce/payments/orchestration/proto/ui/common/generic/UiFieldOuterClass$UiFieldValue;

    if-eqz v1, :cond_0

    .line 308
    const/4 v1, 0x1

    iget-object v2, p0, Lcom/google/commerce/payments/orchestration/proto/ui/common/components/instrument/types/UsernamePassword$UsernamePasswordFormValue;->username:Lcom/google/commerce/payments/orchestration/proto/ui/common/generic/UiFieldOuterClass$UiFieldValue;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeMessageSize(ILcom/google/protobuf/nano/MessageNano;)I

    move-result v1

    add-int/2addr v0, v1

    .line 311
    :cond_0
    iget-object v1, p0, Lcom/google/commerce/payments/orchestration/proto/ui/common/components/instrument/types/UsernamePassword$UsernamePasswordFormValue;->password:Lcom/google/commerce/payments/orchestration/proto/ui/common/generic/UiFieldOuterClass$UiFieldValue;

    if-eqz v1, :cond_1

    .line 312
    const/4 v1, 0x2

    iget-object v2, p0, Lcom/google/commerce/payments/orchestration/proto/ui/common/components/instrument/types/UsernamePassword$UsernamePasswordFormValue;->password:Lcom/google/commerce/payments/orchestration/proto/ui/common/generic/UiFieldOuterClass$UiFieldValue;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeMessageSize(ILcom/google/protobuf/nano/MessageNano;)I

    move-result v1

    add-int/2addr v0, v1

    .line 315
    :cond_1
    iget v1, p0, Lcom/google/commerce/payments/orchestration/proto/ui/common/components/instrument/types/UsernamePassword$UsernamePasswordFormValue;->encryptionType:I

    if-eqz v1, :cond_2

    .line 316
    const/4 v1, 0x3

    iget v2, p0, Lcom/google/commerce/payments/orchestration/proto/ui/common/components/instrument/types/UsernamePassword$UsernamePasswordFormValue;->encryptionType:I

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeInt32Size(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 319
    :cond_2
    iget-object v1, p0, Lcom/google/commerce/payments/orchestration/proto/ui/common/components/instrument/types/UsernamePassword$UsernamePasswordFormValue;->legalDocData:Ljava/lang/String;

    const-string v2, ""

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_3

    .line 320
    const/4 v1, 0x4

    iget-object v2, p0, Lcom/google/commerce/payments/orchestration/proto/ui/common/components/instrument/types/UsernamePassword$UsernamePasswordFormValue;->legalDocData:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeStringSize(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 323
    :cond_3
    iget-object v1, p0, Lcom/google/commerce/payments/orchestration/proto/ui/common/components/instrument/types/UsernamePassword$UsernamePasswordFormValue;->hashedDeviceId:Ljava/lang/String;

    const-string v2, ""

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_4

    .line 324
    const/4 v1, 0x5

    iget-object v2, p0, Lcom/google/commerce/payments/orchestration/proto/ui/common/components/instrument/types/UsernamePassword$UsernamePasswordFormValue;->hashedDeviceId:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeStringSize(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 327
    :cond_4
    return v0
.end method

.method public mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/commerce/payments/orchestration/proto/ui/common/components/instrument/types/UsernamePassword$UsernamePasswordFormValue;
    .locals 3
    .param p1, "input"    # Lcom/google/protobuf/nano/CodedInputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 335
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readTag()I

    move-result v0

    .line 336
    .local v0, "tag":I
    sparse-switch v0, :sswitch_data_0

    .line 340
    invoke-static {p1, v0}, Lcom/google/protobuf/nano/WireFormatNano;->parseUnknownField(Lcom/google/protobuf/nano/CodedInputByteBufferNano;I)Z

    move-result v2

    if-nez v2, :cond_0

    .line 341
    :sswitch_0
    return-object p0

    .line 346
    :sswitch_1
    iget-object v2, p0, Lcom/google/commerce/payments/orchestration/proto/ui/common/components/instrument/types/UsernamePassword$UsernamePasswordFormValue;->username:Lcom/google/commerce/payments/orchestration/proto/ui/common/generic/UiFieldOuterClass$UiFieldValue;

    if-nez v2, :cond_1

    .line 347
    new-instance v2, Lcom/google/commerce/payments/orchestration/proto/ui/common/generic/UiFieldOuterClass$UiFieldValue;

    invoke-direct {v2}, Lcom/google/commerce/payments/orchestration/proto/ui/common/generic/UiFieldOuterClass$UiFieldValue;-><init>()V

    iput-object v2, p0, Lcom/google/commerce/payments/orchestration/proto/ui/common/components/instrument/types/UsernamePassword$UsernamePasswordFormValue;->username:Lcom/google/commerce/payments/orchestration/proto/ui/common/generic/UiFieldOuterClass$UiFieldValue;

    .line 349
    :cond_1
    iget-object v2, p0, Lcom/google/commerce/payments/orchestration/proto/ui/common/components/instrument/types/UsernamePassword$UsernamePasswordFormValue;->username:Lcom/google/commerce/payments/orchestration/proto/ui/common/generic/UiFieldOuterClass$UiFieldValue;

    invoke-virtual {p1, v2}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readMessage(Lcom/google/protobuf/nano/MessageNano;)V

    goto :goto_0

    .line 353
    :sswitch_2
    iget-object v2, p0, Lcom/google/commerce/payments/orchestration/proto/ui/common/components/instrument/types/UsernamePassword$UsernamePasswordFormValue;->password:Lcom/google/commerce/payments/orchestration/proto/ui/common/generic/UiFieldOuterClass$UiFieldValue;

    if-nez v2, :cond_2

    .line 354
    new-instance v2, Lcom/google/commerce/payments/orchestration/proto/ui/common/generic/UiFieldOuterClass$UiFieldValue;

    invoke-direct {v2}, Lcom/google/commerce/payments/orchestration/proto/ui/common/generic/UiFieldOuterClass$UiFieldValue;-><init>()V

    iput-object v2, p0, Lcom/google/commerce/payments/orchestration/proto/ui/common/components/instrument/types/UsernamePassword$UsernamePasswordFormValue;->password:Lcom/google/commerce/payments/orchestration/proto/ui/common/generic/UiFieldOuterClass$UiFieldValue;

    .line 356
    :cond_2
    iget-object v2, p0, Lcom/google/commerce/payments/orchestration/proto/ui/common/components/instrument/types/UsernamePassword$UsernamePasswordFormValue;->password:Lcom/google/commerce/payments/orchestration/proto/ui/common/generic/UiFieldOuterClass$UiFieldValue;

    invoke-virtual {p1, v2}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readMessage(Lcom/google/protobuf/nano/MessageNano;)V

    goto :goto_0

    .line 360
    :sswitch_3
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readInt32()I

    move-result v1

    .line 361
    .local v1, "value":I
    packed-switch v1, :pswitch_data_0

    goto :goto_0

    .line 365
    :pswitch_0
    iput v1, p0, Lcom/google/commerce/payments/orchestration/proto/ui/common/components/instrument/types/UsernamePassword$UsernamePasswordFormValue;->encryptionType:I

    goto :goto_0

    .line 371
    .end local v1    # "value":I
    :sswitch_4
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readString()Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/google/commerce/payments/orchestration/proto/ui/common/components/instrument/types/UsernamePassword$UsernamePasswordFormValue;->legalDocData:Ljava/lang/String;

    goto :goto_0

    .line 375
    :sswitch_5
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readString()Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/google/commerce/payments/orchestration/proto/ui/common/components/instrument/types/UsernamePassword$UsernamePasswordFormValue;->hashedDeviceId:Ljava/lang/String;

    goto :goto_0

    .line 336
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x18 -> :sswitch_3
        0x22 -> :sswitch_4
        0x2a -> :sswitch_5
    .end sparse-switch

    .line 361
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_0
        :pswitch_0
    .end packed-switch
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/protobuf/nano/MessageNano;
    .locals 1
    .param p1, "x0"    # Lcom/google/protobuf/nano/CodedInputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 237
    invoke-virtual {p0, p1}, Lcom/google/commerce/payments/orchestration/proto/ui/common/components/instrument/types/UsernamePassword$UsernamePasswordFormValue;->mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/commerce/payments/orchestration/proto/ui/common/components/instrument/types/UsernamePassword$UsernamePasswordFormValue;

    move-result-object v0

    return-object v0
.end method

.method public writeTo(Lcom/google/protobuf/nano/CodedOutputByteBufferNano;)V
    .locals 2
    .param p1, "output"    # Lcom/google/protobuf/nano/CodedOutputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 286
    iget-object v0, p0, Lcom/google/commerce/payments/orchestration/proto/ui/common/components/instrument/types/UsernamePassword$UsernamePasswordFormValue;->username:Lcom/google/commerce/payments/orchestration/proto/ui/common/generic/UiFieldOuterClass$UiFieldValue;

    if-eqz v0, :cond_0

    .line 287
    const/4 v0, 0x1

    iget-object v1, p0, Lcom/google/commerce/payments/orchestration/proto/ui/common/components/instrument/types/UsernamePassword$UsernamePasswordFormValue;->username:Lcom/google/commerce/payments/orchestration/proto/ui/common/generic/UiFieldOuterClass$UiFieldValue;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeMessage(ILcom/google/protobuf/nano/MessageNano;)V

    .line 289
    :cond_0
    iget-object v0, p0, Lcom/google/commerce/payments/orchestration/proto/ui/common/components/instrument/types/UsernamePassword$UsernamePasswordFormValue;->password:Lcom/google/commerce/payments/orchestration/proto/ui/common/generic/UiFieldOuterClass$UiFieldValue;

    if-eqz v0, :cond_1

    .line 290
    const/4 v0, 0x2

    iget-object v1, p0, Lcom/google/commerce/payments/orchestration/proto/ui/common/components/instrument/types/UsernamePassword$UsernamePasswordFormValue;->password:Lcom/google/commerce/payments/orchestration/proto/ui/common/generic/UiFieldOuterClass$UiFieldValue;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeMessage(ILcom/google/protobuf/nano/MessageNano;)V

    .line 292
    :cond_1
    iget v0, p0, Lcom/google/commerce/payments/orchestration/proto/ui/common/components/instrument/types/UsernamePassword$UsernamePasswordFormValue;->encryptionType:I

    if-eqz v0, :cond_2

    .line 293
    const/4 v0, 0x3

    iget v1, p0, Lcom/google/commerce/payments/orchestration/proto/ui/common/components/instrument/types/UsernamePassword$UsernamePasswordFormValue;->encryptionType:I

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeInt32(II)V

    .line 295
    :cond_2
    iget-object v0, p0, Lcom/google/commerce/payments/orchestration/proto/ui/common/components/instrument/types/UsernamePassword$UsernamePasswordFormValue;->legalDocData:Ljava/lang/String;

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_3

    .line 296
    const/4 v0, 0x4

    iget-object v1, p0, Lcom/google/commerce/payments/orchestration/proto/ui/common/components/instrument/types/UsernamePassword$UsernamePasswordFormValue;->legalDocData:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeString(ILjava/lang/String;)V

    .line 298
    :cond_3
    iget-object v0, p0, Lcom/google/commerce/payments/orchestration/proto/ui/common/components/instrument/types/UsernamePassword$UsernamePasswordFormValue;->hashedDeviceId:Ljava/lang/String;

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_4

    .line 299
    const/4 v0, 0x5

    iget-object v1, p0, Lcom/google/commerce/payments/orchestration/proto/ui/common/components/instrument/types/UsernamePassword$UsernamePasswordFormValue;->hashedDeviceId:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeString(ILjava/lang/String;)V

    .line 301
    :cond_4
    invoke-super {p0, p1}, Lcom/google/protobuf/nano/MessageNano;->writeTo(Lcom/google/protobuf/nano/CodedOutputByteBufferNano;)V

    .line 302
    return-void
.end method
