.class public final Lcom/google/commerce/payments/orchestration/proto/ui/common/ResponseContextOuterClass$ResponseContext;
.super Lcom/google/protobuf/nano/MessageNano;
.source "ResponseContextOuterClass.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/commerce/payments/orchestration/proto/ui/common/ResponseContextOuterClass;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "ResponseContext"
.end annotation


# instance fields
.field public logToken:[B

.field public responseTimeMillis:J

.field public sessionData:[B


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 34
    invoke-direct {p0}, Lcom/google/protobuf/nano/MessageNano;-><init>()V

    .line 35
    invoke-virtual {p0}, Lcom/google/commerce/payments/orchestration/proto/ui/common/ResponseContextOuterClass$ResponseContext;->clear()Lcom/google/commerce/payments/orchestration/proto/ui/common/ResponseContextOuterClass$ResponseContext;

    .line 36
    return-void
.end method


# virtual methods
.method public clear()Lcom/google/commerce/payments/orchestration/proto/ui/common/ResponseContextOuterClass$ResponseContext;
    .locals 2

    .prologue
    .line 39
    const-wide/16 v0, -0x1

    iput-wide v0, p0, Lcom/google/commerce/payments/orchestration/proto/ui/common/ResponseContextOuterClass$ResponseContext;->responseTimeMillis:J

    .line 40
    sget-object v0, Lcom/google/protobuf/nano/WireFormatNano;->EMPTY_BYTES:[B

    iput-object v0, p0, Lcom/google/commerce/payments/orchestration/proto/ui/common/ResponseContextOuterClass$ResponseContext;->sessionData:[B

    .line 41
    sget-object v0, Lcom/google/protobuf/nano/WireFormatNano;->EMPTY_BYTES:[B

    iput-object v0, p0, Lcom/google/commerce/payments/orchestration/proto/ui/common/ResponseContextOuterClass$ResponseContext;->logToken:[B

    .line 42
    const/4 v0, -0x1

    iput v0, p0, Lcom/google/commerce/payments/orchestration/proto/ui/common/ResponseContextOuterClass$ResponseContext;->cachedSize:I

    .line 43
    return-object p0
.end method

.method protected computeSerializedSize()I
    .locals 6

    .prologue
    .line 63
    invoke-super {p0}, Lcom/google/protobuf/nano/MessageNano;->computeSerializedSize()I

    move-result v0

    .line 64
    .local v0, "size":I
    iget-wide v2, p0, Lcom/google/commerce/payments/orchestration/proto/ui/common/ResponseContextOuterClass$ResponseContext;->responseTimeMillis:J

    const-wide/16 v4, -0x1

    cmp-long v1, v2, v4

    if-eqz v1, :cond_0

    .line 65
    const/4 v1, 0x1

    iget-wide v2, p0, Lcom/google/commerce/payments/orchestration/proto/ui/common/ResponseContextOuterClass$ResponseContext;->responseTimeMillis:J

    invoke-static {v1, v2, v3}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeInt64Size(IJ)I

    move-result v1

    add-int/2addr v0, v1

    .line 68
    :cond_0
    iget-object v1, p0, Lcom/google/commerce/payments/orchestration/proto/ui/common/ResponseContextOuterClass$ResponseContext;->sessionData:[B

    sget-object v2, Lcom/google/protobuf/nano/WireFormatNano;->EMPTY_BYTES:[B

    invoke-static {v1, v2}, Ljava/util/Arrays;->equals([B[B)Z

    move-result v1

    if-nez v1, :cond_1

    .line 69
    const/4 v1, 0x2

    iget-object v2, p0, Lcom/google/commerce/payments/orchestration/proto/ui/common/ResponseContextOuterClass$ResponseContext;->sessionData:[B

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeBytesSize(I[B)I

    move-result v1

    add-int/2addr v0, v1

    .line 72
    :cond_1
    iget-object v1, p0, Lcom/google/commerce/payments/orchestration/proto/ui/common/ResponseContextOuterClass$ResponseContext;->logToken:[B

    sget-object v2, Lcom/google/protobuf/nano/WireFormatNano;->EMPTY_BYTES:[B

    invoke-static {v1, v2}, Ljava/util/Arrays;->equals([B[B)Z

    move-result v1

    if-nez v1, :cond_2

    .line 73
    const/4 v1, 0x5

    iget-object v2, p0, Lcom/google/commerce/payments/orchestration/proto/ui/common/ResponseContextOuterClass$ResponseContext;->logToken:[B

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeBytesSize(I[B)I

    move-result v1

    add-int/2addr v0, v1

    .line 76
    :cond_2
    return v0
.end method

.method public mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/commerce/payments/orchestration/proto/ui/common/ResponseContextOuterClass$ResponseContext;
    .locals 4
    .param p1, "input"    # Lcom/google/protobuf/nano/CodedInputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 84
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readTag()I

    move-result v0

    .line 85
    .local v0, "tag":I
    sparse-switch v0, :sswitch_data_0

    .line 89
    invoke-static {p1, v0}, Lcom/google/protobuf/nano/WireFormatNano;->parseUnknownField(Lcom/google/protobuf/nano/CodedInputByteBufferNano;I)Z

    move-result v1

    if-nez v1, :cond_0

    .line 90
    :sswitch_0
    return-object p0

    .line 95
    :sswitch_1
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readInt64()J

    move-result-wide v2

    iput-wide v2, p0, Lcom/google/commerce/payments/orchestration/proto/ui/common/ResponseContextOuterClass$ResponseContext;->responseTimeMillis:J

    goto :goto_0

    .line 99
    :sswitch_2
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readBytes()[B

    move-result-object v1

    iput-object v1, p0, Lcom/google/commerce/payments/orchestration/proto/ui/common/ResponseContextOuterClass$ResponseContext;->sessionData:[B

    goto :goto_0

    .line 103
    :sswitch_3
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readBytes()[B

    move-result-object v1

    iput-object v1, p0, Lcom/google/commerce/payments/orchestration/proto/ui/common/ResponseContextOuterClass$ResponseContext;->logToken:[B

    goto :goto_0

    .line 85
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x12 -> :sswitch_2
        0x2a -> :sswitch_3
    .end sparse-switch
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/protobuf/nano/MessageNano;
    .locals 1
    .param p1, "x0"    # Lcom/google/protobuf/nano/CodedInputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 8
    invoke-virtual {p0, p1}, Lcom/google/commerce/payments/orchestration/proto/ui/common/ResponseContextOuterClass$ResponseContext;->mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/commerce/payments/orchestration/proto/ui/common/ResponseContextOuterClass$ResponseContext;

    move-result-object v0

    return-object v0
.end method

.method public writeTo(Lcom/google/protobuf/nano/CodedOutputByteBufferNano;)V
    .locals 4
    .param p1, "output"    # Lcom/google/protobuf/nano/CodedOutputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 49
    iget-wide v0, p0, Lcom/google/commerce/payments/orchestration/proto/ui/common/ResponseContextOuterClass$ResponseContext;->responseTimeMillis:J

    const-wide/16 v2, -0x1

    cmp-long v0, v0, v2

    if-eqz v0, :cond_0

    .line 50
    const/4 v0, 0x1

    iget-wide v2, p0, Lcom/google/commerce/payments/orchestration/proto/ui/common/ResponseContextOuterClass$ResponseContext;->responseTimeMillis:J

    invoke-virtual {p1, v0, v2, v3}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeInt64(IJ)V

    .line 52
    :cond_0
    iget-object v0, p0, Lcom/google/commerce/payments/orchestration/proto/ui/common/ResponseContextOuterClass$ResponseContext;->sessionData:[B

    sget-object v1, Lcom/google/protobuf/nano/WireFormatNano;->EMPTY_BYTES:[B

    invoke-static {v0, v1}, Ljava/util/Arrays;->equals([B[B)Z

    move-result v0

    if-nez v0, :cond_1

    .line 53
    const/4 v0, 0x2

    iget-object v1, p0, Lcom/google/commerce/payments/orchestration/proto/ui/common/ResponseContextOuterClass$ResponseContext;->sessionData:[B

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeBytes(I[B)V

    .line 55
    :cond_1
    iget-object v0, p0, Lcom/google/commerce/payments/orchestration/proto/ui/common/ResponseContextOuterClass$ResponseContext;->logToken:[B

    sget-object v1, Lcom/google/protobuf/nano/WireFormatNano;->EMPTY_BYTES:[B

    invoke-static {v0, v1}, Ljava/util/Arrays;->equals([B[B)Z

    move-result v0

    if-nez v0, :cond_2

    .line 56
    const/4 v0, 0x5

    iget-object v1, p0, Lcom/google/commerce/payments/orchestration/proto/ui/common/ResponseContextOuterClass$ResponseContext;->logToken:[B

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeBytes(I[B)V

    .line 58
    :cond_2
    invoke-super {p0, p1}, Lcom/google/protobuf/nano/MessageNano;->writeTo(Lcom/google/protobuf/nano/CodedOutputByteBufferNano;)V

    .line 59
    return-void
.end method
