.class public final Lcom/google/commerce/payments/orchestration/proto/ui/common/components/instrument/types/CreditCard$CreditCardForm;
.super Lcom/google/protobuf/nano/MessageNano;
.source "CreditCard.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/commerce/payments/orchestration/proto/ui/common/components/instrument/types/CreditCard;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "CreditCardForm"
.end annotation


# instance fields
.field public allowCameraInput:Z

.field public allowedCardType:[Lcom/google/commerce/payments/orchestration/proto/ui/common/components/instrument/types/CreditCard$CardType;

.field public allowedType:[I

.field public billingAddress:Lcom/google/commerce/payments/orchestration/proto/ui/common/components/AddressFormOuterClass$AddressForm;

.field public id:Ljava/lang/String;

.field public initialValue:Lcom/google/commerce/payments/orchestration/proto/ui/common/components/instrument/types/CreditCard$CreditCardFormValue;

.field public invalidBin:[Lcom/google/commerce/payments/orchestration/proto/ui/common/components/instrument/types/CreditCard$BinRange;

.field public legalMessages:Lcom/google/commerce/payments/orchestration/proto/ui/common/components/legal/LegalMessageSetOuterClass$LegalMessageSet;

.field public maxExpirationMonth:I

.field public maxExpirationYear:I

.field public minExpirationMonth:I

.field public minExpirationYear:I

.field public title:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 80
    invoke-direct {p0}, Lcom/google/protobuf/nano/MessageNano;-><init>()V

    .line 81
    invoke-virtual {p0}, Lcom/google/commerce/payments/orchestration/proto/ui/common/components/instrument/types/CreditCard$CreditCardForm;->clear()Lcom/google/commerce/payments/orchestration/proto/ui/common/components/instrument/types/CreditCard$CreditCardForm;

    .line 82
    return-void
.end method


# virtual methods
.method public clear()Lcom/google/commerce/payments/orchestration/proto/ui/common/components/instrument/types/CreditCard$CreditCardForm;
    .locals 3

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x0

    .line 85
    const-string v0, ""

    iput-object v0, p0, Lcom/google/commerce/payments/orchestration/proto/ui/common/components/instrument/types/CreditCard$CreditCardForm;->id:Ljava/lang/String;

    .line 86
    const-string v0, ""

    iput-object v0, p0, Lcom/google/commerce/payments/orchestration/proto/ui/common/components/instrument/types/CreditCard$CreditCardForm;->title:Ljava/lang/String;

    .line 87
    invoke-static {}, Lcom/google/commerce/payments/orchestration/proto/ui/common/components/instrument/types/CreditCard$CardType;->emptyArray()[Lcom/google/commerce/payments/orchestration/proto/ui/common/components/instrument/types/CreditCard$CardType;

    move-result-object v0

    iput-object v0, p0, Lcom/google/commerce/payments/orchestration/proto/ui/common/components/instrument/types/CreditCard$CreditCardForm;->allowedCardType:[Lcom/google/commerce/payments/orchestration/proto/ui/common/components/instrument/types/CreditCard$CardType;

    .line 88
    invoke-static {}, Lcom/google/commerce/payments/orchestration/proto/ui/common/components/instrument/types/CreditCard$BinRange;->emptyArray()[Lcom/google/commerce/payments/orchestration/proto/ui/common/components/instrument/types/CreditCard$BinRange;

    move-result-object v0

    iput-object v0, p0, Lcom/google/commerce/payments/orchestration/proto/ui/common/components/instrument/types/CreditCard$CreditCardForm;->invalidBin:[Lcom/google/commerce/payments/orchestration/proto/ui/common/components/instrument/types/CreditCard$BinRange;

    .line 89
    sget-object v0, Lcom/google/protobuf/nano/WireFormatNano;->EMPTY_INT_ARRAY:[I

    iput-object v0, p0, Lcom/google/commerce/payments/orchestration/proto/ui/common/components/instrument/types/CreditCard$CreditCardForm;->allowedType:[I

    .line 90
    iput-object v2, p0, Lcom/google/commerce/payments/orchestration/proto/ui/common/components/instrument/types/CreditCard$CreditCardForm;->billingAddress:Lcom/google/commerce/payments/orchestration/proto/ui/common/components/AddressFormOuterClass$AddressForm;

    .line 91
    iput-object v2, p0, Lcom/google/commerce/payments/orchestration/proto/ui/common/components/instrument/types/CreditCard$CreditCardForm;->initialValue:Lcom/google/commerce/payments/orchestration/proto/ui/common/components/instrument/types/CreditCard$CreditCardFormValue;

    .line 92
    iput-object v2, p0, Lcom/google/commerce/payments/orchestration/proto/ui/common/components/instrument/types/CreditCard$CreditCardForm;->legalMessages:Lcom/google/commerce/payments/orchestration/proto/ui/common/components/legal/LegalMessageSetOuterClass$LegalMessageSet;

    .line 93
    iput v1, p0, Lcom/google/commerce/payments/orchestration/proto/ui/common/components/instrument/types/CreditCard$CreditCardForm;->minExpirationMonth:I

    .line 94
    iput v1, p0, Lcom/google/commerce/payments/orchestration/proto/ui/common/components/instrument/types/CreditCard$CreditCardForm;->minExpirationYear:I

    .line 95
    iput v1, p0, Lcom/google/commerce/payments/orchestration/proto/ui/common/components/instrument/types/CreditCard$CreditCardForm;->maxExpirationMonth:I

    .line 96
    iput v1, p0, Lcom/google/commerce/payments/orchestration/proto/ui/common/components/instrument/types/CreditCard$CreditCardForm;->maxExpirationYear:I

    .line 97
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/commerce/payments/orchestration/proto/ui/common/components/instrument/types/CreditCard$CreditCardForm;->allowCameraInput:Z

    .line 98
    const/4 v0, -0x1

    iput v0, p0, Lcom/google/commerce/payments/orchestration/proto/ui/common/components/instrument/types/CreditCard$CreditCardForm;->cachedSize:I

    .line 99
    return-object p0
.end method

.method protected computeSerializedSize()I
    .locals 6

    .prologue
    .line 161
    invoke-super {p0}, Lcom/google/protobuf/nano/MessageNano;->computeSerializedSize()I

    move-result v3

    .line 162
    .local v3, "size":I
    iget-object v4, p0, Lcom/google/commerce/payments/orchestration/proto/ui/common/components/instrument/types/CreditCard$CreditCardForm;->allowedType:[I

    if-eqz v4, :cond_1

    iget-object v4, p0, Lcom/google/commerce/payments/orchestration/proto/ui/common/components/instrument/types/CreditCard$CreditCardForm;->allowedType:[I

    array-length v4, v4

    if-lez v4, :cond_1

    .line 163
    const/4 v0, 0x0

    .line 164
    .local v0, "dataSize":I
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_0
    iget-object v4, p0, Lcom/google/commerce/payments/orchestration/proto/ui/common/components/instrument/types/CreditCard$CreditCardForm;->allowedType:[I

    array-length v4, v4

    if-ge v2, v4, :cond_0

    .line 165
    iget-object v4, p0, Lcom/google/commerce/payments/orchestration/proto/ui/common/components/instrument/types/CreditCard$CreditCardForm;->allowedType:[I

    aget v1, v4, v2

    .line 166
    .local v1, "element":I
    invoke-static {v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeInt32SizeNoTag(I)I

    move-result v4

    add-int/2addr v0, v4

    .line 164
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 169
    .end local v1    # "element":I
    :cond_0
    add-int/2addr v3, v0

    .line 170
    iget-object v4, p0, Lcom/google/commerce/payments/orchestration/proto/ui/common/components/instrument/types/CreditCard$CreditCardForm;->allowedType:[I

    array-length v4, v4

    mul-int/lit8 v4, v4, 0x1

    add-int/2addr v3, v4

    .line 172
    .end local v0    # "dataSize":I
    .end local v2    # "i":I
    :cond_1
    iget-object v4, p0, Lcom/google/commerce/payments/orchestration/proto/ui/common/components/instrument/types/CreditCard$CreditCardForm;->billingAddress:Lcom/google/commerce/payments/orchestration/proto/ui/common/components/AddressFormOuterClass$AddressForm;

    if-eqz v4, :cond_2

    .line 173
    const/4 v4, 0x2

    iget-object v5, p0, Lcom/google/commerce/payments/orchestration/proto/ui/common/components/instrument/types/CreditCard$CreditCardForm;->billingAddress:Lcom/google/commerce/payments/orchestration/proto/ui/common/components/AddressFormOuterClass$AddressForm;

    invoke-static {v4, v5}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeMessageSize(ILcom/google/protobuf/nano/MessageNano;)I

    move-result v4

    add-int/2addr v3, v4

    .line 176
    :cond_2
    iget-object v4, p0, Lcom/google/commerce/payments/orchestration/proto/ui/common/components/instrument/types/CreditCard$CreditCardForm;->initialValue:Lcom/google/commerce/payments/orchestration/proto/ui/common/components/instrument/types/CreditCard$CreditCardFormValue;

    if-eqz v4, :cond_3

    .line 177
    const/4 v4, 0x3

    iget-object v5, p0, Lcom/google/commerce/payments/orchestration/proto/ui/common/components/instrument/types/CreditCard$CreditCardForm;->initialValue:Lcom/google/commerce/payments/orchestration/proto/ui/common/components/instrument/types/CreditCard$CreditCardFormValue;

    invoke-static {v4, v5}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeMessageSize(ILcom/google/protobuf/nano/MessageNano;)I

    move-result v4

    add-int/2addr v3, v4

    .line 180
    :cond_3
    iget-object v4, p0, Lcom/google/commerce/payments/orchestration/proto/ui/common/components/instrument/types/CreditCard$CreditCardForm;->legalMessages:Lcom/google/commerce/payments/orchestration/proto/ui/common/components/legal/LegalMessageSetOuterClass$LegalMessageSet;

    if-eqz v4, :cond_4

    .line 181
    const/4 v4, 0x4

    iget-object v5, p0, Lcom/google/commerce/payments/orchestration/proto/ui/common/components/instrument/types/CreditCard$CreditCardForm;->legalMessages:Lcom/google/commerce/payments/orchestration/proto/ui/common/components/legal/LegalMessageSetOuterClass$LegalMessageSet;

    invoke-static {v4, v5}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeMessageSize(ILcom/google/protobuf/nano/MessageNano;)I

    move-result v4

    add-int/2addr v3, v4

    .line 184
    :cond_4
    iget-object v4, p0, Lcom/google/commerce/payments/orchestration/proto/ui/common/components/instrument/types/CreditCard$CreditCardForm;->id:Ljava/lang/String;

    const-string v5, ""

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_5

    .line 185
    const/4 v4, 0x5

    iget-object v5, p0, Lcom/google/commerce/payments/orchestration/proto/ui/common/components/instrument/types/CreditCard$CreditCardForm;->id:Ljava/lang/String;

    invoke-static {v4, v5}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeStringSize(ILjava/lang/String;)I

    move-result v4

    add-int/2addr v3, v4

    .line 188
    :cond_5
    iget-object v4, p0, Lcom/google/commerce/payments/orchestration/proto/ui/common/components/instrument/types/CreditCard$CreditCardForm;->allowedCardType:[Lcom/google/commerce/payments/orchestration/proto/ui/common/components/instrument/types/CreditCard$CardType;

    if-eqz v4, :cond_7

    iget-object v4, p0, Lcom/google/commerce/payments/orchestration/proto/ui/common/components/instrument/types/CreditCard$CreditCardForm;->allowedCardType:[Lcom/google/commerce/payments/orchestration/proto/ui/common/components/instrument/types/CreditCard$CardType;

    array-length v4, v4

    if-lez v4, :cond_7

    .line 189
    const/4 v2, 0x0

    .restart local v2    # "i":I
    :goto_1
    iget-object v4, p0, Lcom/google/commerce/payments/orchestration/proto/ui/common/components/instrument/types/CreditCard$CreditCardForm;->allowedCardType:[Lcom/google/commerce/payments/orchestration/proto/ui/common/components/instrument/types/CreditCard$CardType;

    array-length v4, v4

    if-ge v2, v4, :cond_7

    .line 190
    iget-object v4, p0, Lcom/google/commerce/payments/orchestration/proto/ui/common/components/instrument/types/CreditCard$CreditCardForm;->allowedCardType:[Lcom/google/commerce/payments/orchestration/proto/ui/common/components/instrument/types/CreditCard$CardType;

    aget-object v1, v4, v2

    .line 191
    .local v1, "element":Lcom/google/commerce/payments/orchestration/proto/ui/common/components/instrument/types/CreditCard$CardType;
    if-eqz v1, :cond_6

    .line 192
    const/4 v4, 0x6

    invoke-static {v4, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeMessageSize(ILcom/google/protobuf/nano/MessageNano;)I

    move-result v4

    add-int/2addr v3, v4

    .line 189
    :cond_6
    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    .line 197
    .end local v1    # "element":Lcom/google/commerce/payments/orchestration/proto/ui/common/components/instrument/types/CreditCard$CardType;
    .end local v2    # "i":I
    :cond_7
    iget-object v4, p0, Lcom/google/commerce/payments/orchestration/proto/ui/common/components/instrument/types/CreditCard$CreditCardForm;->invalidBin:[Lcom/google/commerce/payments/orchestration/proto/ui/common/components/instrument/types/CreditCard$BinRange;

    if-eqz v4, :cond_9

    iget-object v4, p0, Lcom/google/commerce/payments/orchestration/proto/ui/common/components/instrument/types/CreditCard$CreditCardForm;->invalidBin:[Lcom/google/commerce/payments/orchestration/proto/ui/common/components/instrument/types/CreditCard$BinRange;

    array-length v4, v4

    if-lez v4, :cond_9

    .line 198
    const/4 v2, 0x0

    .restart local v2    # "i":I
    :goto_2
    iget-object v4, p0, Lcom/google/commerce/payments/orchestration/proto/ui/common/components/instrument/types/CreditCard$CreditCardForm;->invalidBin:[Lcom/google/commerce/payments/orchestration/proto/ui/common/components/instrument/types/CreditCard$BinRange;

    array-length v4, v4

    if-ge v2, v4, :cond_9

    .line 199
    iget-object v4, p0, Lcom/google/commerce/payments/orchestration/proto/ui/common/components/instrument/types/CreditCard$CreditCardForm;->invalidBin:[Lcom/google/commerce/payments/orchestration/proto/ui/common/components/instrument/types/CreditCard$BinRange;

    aget-object v1, v4, v2

    .line 200
    .local v1, "element":Lcom/google/commerce/payments/orchestration/proto/ui/common/components/instrument/types/CreditCard$BinRange;
    if-eqz v1, :cond_8

    .line 201
    const/4 v4, 0x7

    invoke-static {v4, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeMessageSize(ILcom/google/protobuf/nano/MessageNano;)I

    move-result v4

    add-int/2addr v3, v4

    .line 198
    :cond_8
    add-int/lit8 v2, v2, 0x1

    goto :goto_2

    .line 206
    .end local v1    # "element":Lcom/google/commerce/payments/orchestration/proto/ui/common/components/instrument/types/CreditCard$BinRange;
    .end local v2    # "i":I
    :cond_9
    iget v4, p0, Lcom/google/commerce/payments/orchestration/proto/ui/common/components/instrument/types/CreditCard$CreditCardForm;->minExpirationMonth:I

    if-eqz v4, :cond_a

    .line 207
    const/16 v4, 0x8

    iget v5, p0, Lcom/google/commerce/payments/orchestration/proto/ui/common/components/instrument/types/CreditCard$CreditCardForm;->minExpirationMonth:I

    invoke-static {v4, v5}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeInt32Size(II)I

    move-result v4

    add-int/2addr v3, v4

    .line 210
    :cond_a
    iget v4, p0, Lcom/google/commerce/payments/orchestration/proto/ui/common/components/instrument/types/CreditCard$CreditCardForm;->minExpirationYear:I

    if-eqz v4, :cond_b

    .line 211
    const/16 v4, 0x9

    iget v5, p0, Lcom/google/commerce/payments/orchestration/proto/ui/common/components/instrument/types/CreditCard$CreditCardForm;->minExpirationYear:I

    invoke-static {v4, v5}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeInt32Size(II)I

    move-result v4

    add-int/2addr v3, v4

    .line 214
    :cond_b
    iget v4, p0, Lcom/google/commerce/payments/orchestration/proto/ui/common/components/instrument/types/CreditCard$CreditCardForm;->maxExpirationMonth:I

    if-eqz v4, :cond_c

    .line 215
    const/16 v4, 0xa

    iget v5, p0, Lcom/google/commerce/payments/orchestration/proto/ui/common/components/instrument/types/CreditCard$CreditCardForm;->maxExpirationMonth:I

    invoke-static {v4, v5}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeInt32Size(II)I

    move-result v4

    add-int/2addr v3, v4

    .line 218
    :cond_c
    iget v4, p0, Lcom/google/commerce/payments/orchestration/proto/ui/common/components/instrument/types/CreditCard$CreditCardForm;->maxExpirationYear:I

    if-eqz v4, :cond_d

    .line 219
    const/16 v4, 0xb

    iget v5, p0, Lcom/google/commerce/payments/orchestration/proto/ui/common/components/instrument/types/CreditCard$CreditCardForm;->maxExpirationYear:I

    invoke-static {v4, v5}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeInt32Size(II)I

    move-result v4

    add-int/2addr v3, v4

    .line 222
    :cond_d
    iget-boolean v4, p0, Lcom/google/commerce/payments/orchestration/proto/ui/common/components/instrument/types/CreditCard$CreditCardForm;->allowCameraInput:Z

    const/4 v5, 0x1

    if-eq v4, v5, :cond_e

    .line 223
    const/16 v4, 0xc

    iget-boolean v5, p0, Lcom/google/commerce/payments/orchestration/proto/ui/common/components/instrument/types/CreditCard$CreditCardForm;->allowCameraInput:Z

    invoke-static {v4, v5}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeBoolSize(IZ)I

    move-result v4

    add-int/2addr v3, v4

    .line 226
    :cond_e
    iget-object v4, p0, Lcom/google/commerce/payments/orchestration/proto/ui/common/components/instrument/types/CreditCard$CreditCardForm;->title:Ljava/lang/String;

    const-string v5, ""

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_f

    .line 227
    const/16 v4, 0xd

    iget-object v5, p0, Lcom/google/commerce/payments/orchestration/proto/ui/common/components/instrument/types/CreditCard$CreditCardForm;->title:Ljava/lang/String;

    invoke-static {v4, v5}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeStringSize(ILjava/lang/String;)I

    move-result v4

    add-int/2addr v3, v4

    .line 230
    :cond_f
    return v3
.end method

.method public mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/commerce/payments/orchestration/proto/ui/common/components/instrument/types/CreditCard$CreditCardForm;
    .locals 17
    .param p1, "input"    # Lcom/google/protobuf/nano/CodedInputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 238
    :cond_0
    :goto_0
    invoke-virtual/range {p1 .. p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readTag()I

    move-result v9

    .line 239
    .local v9, "tag":I
    sparse-switch v9, :sswitch_data_0

    .line 243
    move-object/from16 v0, p1

    invoke-static {v0, v9}, Lcom/google/protobuf/nano/WireFormatNano;->parseUnknownField(Lcom/google/protobuf/nano/CodedInputByteBufferNano;I)Z

    move-result v14

    if-nez v14, :cond_0

    .line 244
    :sswitch_0
    return-object p0

    .line 249
    :sswitch_1
    const/16 v14, 0x8

    move-object/from16 v0, p1

    invoke-static {v0, v14}, Lcom/google/protobuf/nano/WireFormatNano;->getRepeatedFieldArrayLength(Lcom/google/protobuf/nano/CodedInputByteBufferNano;I)I

    move-result v5

    .line 251
    .local v5, "length":I
    new-array v12, v5, [I

    .line 252
    .local v12, "validValues":[I
    const/4 v10, 0x0

    .line 253
    .local v10, "validCount":I
    const/4 v3, 0x0

    .local v3, "i":I
    move v11, v10

    .end local v10    # "validCount":I
    .local v11, "validCount":I
    :goto_1
    if-ge v3, v5, :cond_2

    .line 254
    if-eqz v3, :cond_1

    .line 255
    invoke-virtual/range {p1 .. p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readTag()I

    .line 257
    :cond_1
    invoke-virtual/range {p1 .. p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readInt32()I

    move-result v13

    .line 258
    .local v13, "value":I
    packed-switch v13, :pswitch_data_0

    move v10, v11

    .line 253
    .end local v11    # "validCount":I
    .restart local v10    # "validCount":I
    :goto_2
    add-int/lit8 v3, v3, 0x1

    move v11, v10

    .end local v10    # "validCount":I
    .restart local v11    # "validCount":I
    goto :goto_1

    .line 267
    :pswitch_0
    add-int/lit8 v10, v11, 0x1

    .end local v11    # "validCount":I
    .restart local v10    # "validCount":I
    aput v13, v12, v11

    goto :goto_2

    .line 271
    .end local v10    # "validCount":I
    .end local v13    # "value":I
    .restart local v11    # "validCount":I
    :cond_2
    if-eqz v11, :cond_0

    .line 272
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/google/commerce/payments/orchestration/proto/ui/common/components/instrument/types/CreditCard$CreditCardForm;->allowedType:[I

    if-nez v14, :cond_3

    const/4 v3, 0x0

    .line 273
    :goto_3
    if-nez v3, :cond_4

    array-length v14, v12

    if-ne v11, v14, :cond_4

    .line 274
    move-object/from16 v0, p0

    iput-object v12, v0, Lcom/google/commerce/payments/orchestration/proto/ui/common/components/instrument/types/CreditCard$CreditCardForm;->allowedType:[I

    goto :goto_0

    .line 272
    :cond_3
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/google/commerce/payments/orchestration/proto/ui/common/components/instrument/types/CreditCard$CreditCardForm;->allowedType:[I

    array-length v3, v14

    goto :goto_3

    .line 276
    :cond_4
    add-int v14, v3, v11

    new-array v7, v14, [I

    .line 277
    .local v7, "newArray":[I
    if-eqz v3, :cond_5

    .line 278
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/google/commerce/payments/orchestration/proto/ui/common/components/instrument/types/CreditCard$CreditCardForm;->allowedType:[I

    const/4 v15, 0x0

    const/16 v16, 0x0

    move/from16 v0, v16

    invoke-static {v14, v15, v7, v0, v3}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 280
    :cond_5
    const/4 v14, 0x0

    invoke-static {v12, v14, v7, v3, v11}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 281
    move-object/from16 v0, p0

    iput-object v7, v0, Lcom/google/commerce/payments/orchestration/proto/ui/common/components/instrument/types/CreditCard$CreditCardForm;->allowedType:[I

    goto :goto_0

    .line 287
    .end local v3    # "i":I
    .end local v5    # "length":I
    .end local v7    # "newArray":[I
    .end local v11    # "validCount":I
    .end local v12    # "validValues":[I
    :sswitch_2
    invoke-virtual/range {p1 .. p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readRawVarint32()I

    move-result v2

    .line 288
    .local v2, "bytes":I
    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->pushLimit(I)I

    move-result v6

    .line 290
    .local v6, "limit":I
    const/4 v1, 0x0

    .line 291
    .local v1, "arrayLength":I
    invoke-virtual/range {p1 .. p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->getPosition()I

    move-result v8

    .line 292
    .local v8, "startPos":I
    :goto_4
    invoke-virtual/range {p1 .. p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->getBytesUntilLimit()I

    move-result v14

    if-lez v14, :cond_6

    .line 293
    invoke-virtual/range {p1 .. p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readInt32()I

    move-result v14

    packed-switch v14, :pswitch_data_1

    goto :goto_4

    .line 302
    :pswitch_1
    add-int/lit8 v1, v1, 0x1

    goto :goto_4

    .line 306
    :cond_6
    if-eqz v1, :cond_a

    .line 307
    move-object/from16 v0, p1

    invoke-virtual {v0, v8}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->rewindToPosition(I)V

    .line 308
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/google/commerce/payments/orchestration/proto/ui/common/components/instrument/types/CreditCard$CreditCardForm;->allowedType:[I

    if-nez v14, :cond_8

    const/4 v3, 0x0

    .line 309
    .restart local v3    # "i":I
    :goto_5
    add-int v14, v3, v1

    new-array v7, v14, [I

    .line 310
    .restart local v7    # "newArray":[I
    if-eqz v3, :cond_7

    .line 311
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/google/commerce/payments/orchestration/proto/ui/common/components/instrument/types/CreditCard$CreditCardForm;->allowedType:[I

    const/4 v15, 0x0

    const/16 v16, 0x0

    move/from16 v0, v16

    invoke-static {v14, v15, v7, v0, v3}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 313
    :cond_7
    :goto_6
    invoke-virtual/range {p1 .. p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->getBytesUntilLimit()I

    move-result v14

    if-lez v14, :cond_9

    .line 314
    invoke-virtual/range {p1 .. p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readInt32()I

    move-result v13

    .line 315
    .restart local v13    # "value":I
    packed-switch v13, :pswitch_data_2

    goto :goto_6

    .line 324
    :pswitch_2
    add-int/lit8 v4, v3, 0x1

    .end local v3    # "i":I
    .local v4, "i":I
    aput v13, v7, v3

    move v3, v4

    .end local v4    # "i":I
    .restart local v3    # "i":I
    goto :goto_6

    .line 308
    .end local v3    # "i":I
    .end local v7    # "newArray":[I
    .end local v13    # "value":I
    :cond_8
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/google/commerce/payments/orchestration/proto/ui/common/components/instrument/types/CreditCard$CreditCardForm;->allowedType:[I

    array-length v3, v14

    goto :goto_5

    .line 328
    .restart local v3    # "i":I
    .restart local v7    # "newArray":[I
    :cond_9
    move-object/from16 v0, p0

    iput-object v7, v0, Lcom/google/commerce/payments/orchestration/proto/ui/common/components/instrument/types/CreditCard$CreditCardForm;->allowedType:[I

    .line 330
    .end local v3    # "i":I
    .end local v7    # "newArray":[I
    :cond_a
    move-object/from16 v0, p1

    invoke-virtual {v0, v6}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->popLimit(I)V

    goto/16 :goto_0

    .line 334
    .end local v1    # "arrayLength":I
    .end local v2    # "bytes":I
    .end local v6    # "limit":I
    .end local v8    # "startPos":I
    :sswitch_3
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/google/commerce/payments/orchestration/proto/ui/common/components/instrument/types/CreditCard$CreditCardForm;->billingAddress:Lcom/google/commerce/payments/orchestration/proto/ui/common/components/AddressFormOuterClass$AddressForm;

    if-nez v14, :cond_b

    .line 335
    new-instance v14, Lcom/google/commerce/payments/orchestration/proto/ui/common/components/AddressFormOuterClass$AddressForm;

    invoke-direct {v14}, Lcom/google/commerce/payments/orchestration/proto/ui/common/components/AddressFormOuterClass$AddressForm;-><init>()V

    move-object/from16 v0, p0

    iput-object v14, v0, Lcom/google/commerce/payments/orchestration/proto/ui/common/components/instrument/types/CreditCard$CreditCardForm;->billingAddress:Lcom/google/commerce/payments/orchestration/proto/ui/common/components/AddressFormOuterClass$AddressForm;

    .line 337
    :cond_b
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/google/commerce/payments/orchestration/proto/ui/common/components/instrument/types/CreditCard$CreditCardForm;->billingAddress:Lcom/google/commerce/payments/orchestration/proto/ui/common/components/AddressFormOuterClass$AddressForm;

    move-object/from16 v0, p1

    invoke-virtual {v0, v14}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readMessage(Lcom/google/protobuf/nano/MessageNano;)V

    goto/16 :goto_0

    .line 341
    :sswitch_4
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/google/commerce/payments/orchestration/proto/ui/common/components/instrument/types/CreditCard$CreditCardForm;->initialValue:Lcom/google/commerce/payments/orchestration/proto/ui/common/components/instrument/types/CreditCard$CreditCardFormValue;

    if-nez v14, :cond_c

    .line 342
    new-instance v14, Lcom/google/commerce/payments/orchestration/proto/ui/common/components/instrument/types/CreditCard$CreditCardFormValue;

    invoke-direct {v14}, Lcom/google/commerce/payments/orchestration/proto/ui/common/components/instrument/types/CreditCard$CreditCardFormValue;-><init>()V

    move-object/from16 v0, p0

    iput-object v14, v0, Lcom/google/commerce/payments/orchestration/proto/ui/common/components/instrument/types/CreditCard$CreditCardForm;->initialValue:Lcom/google/commerce/payments/orchestration/proto/ui/common/components/instrument/types/CreditCard$CreditCardFormValue;

    .line 344
    :cond_c
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/google/commerce/payments/orchestration/proto/ui/common/components/instrument/types/CreditCard$CreditCardForm;->initialValue:Lcom/google/commerce/payments/orchestration/proto/ui/common/components/instrument/types/CreditCard$CreditCardFormValue;

    move-object/from16 v0, p1

    invoke-virtual {v0, v14}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readMessage(Lcom/google/protobuf/nano/MessageNano;)V

    goto/16 :goto_0

    .line 348
    :sswitch_5
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/google/commerce/payments/orchestration/proto/ui/common/components/instrument/types/CreditCard$CreditCardForm;->legalMessages:Lcom/google/commerce/payments/orchestration/proto/ui/common/components/legal/LegalMessageSetOuterClass$LegalMessageSet;

    if-nez v14, :cond_d

    .line 349
    new-instance v14, Lcom/google/commerce/payments/orchestration/proto/ui/common/components/legal/LegalMessageSetOuterClass$LegalMessageSet;

    invoke-direct {v14}, Lcom/google/commerce/payments/orchestration/proto/ui/common/components/legal/LegalMessageSetOuterClass$LegalMessageSet;-><init>()V

    move-object/from16 v0, p0

    iput-object v14, v0, Lcom/google/commerce/payments/orchestration/proto/ui/common/components/instrument/types/CreditCard$CreditCardForm;->legalMessages:Lcom/google/commerce/payments/orchestration/proto/ui/common/components/legal/LegalMessageSetOuterClass$LegalMessageSet;

    .line 351
    :cond_d
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/google/commerce/payments/orchestration/proto/ui/common/components/instrument/types/CreditCard$CreditCardForm;->legalMessages:Lcom/google/commerce/payments/orchestration/proto/ui/common/components/legal/LegalMessageSetOuterClass$LegalMessageSet;

    move-object/from16 v0, p1

    invoke-virtual {v0, v14}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readMessage(Lcom/google/protobuf/nano/MessageNano;)V

    goto/16 :goto_0

    .line 355
    :sswitch_6
    invoke-virtual/range {p1 .. p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readString()Ljava/lang/String;

    move-result-object v14

    move-object/from16 v0, p0

    iput-object v14, v0, Lcom/google/commerce/payments/orchestration/proto/ui/common/components/instrument/types/CreditCard$CreditCardForm;->id:Ljava/lang/String;

    goto/16 :goto_0

    .line 359
    :sswitch_7
    const/16 v14, 0x32

    move-object/from16 v0, p1

    invoke-static {v0, v14}, Lcom/google/protobuf/nano/WireFormatNano;->getRepeatedFieldArrayLength(Lcom/google/protobuf/nano/CodedInputByteBufferNano;I)I

    move-result v1

    .line 361
    .restart local v1    # "arrayLength":I
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/google/commerce/payments/orchestration/proto/ui/common/components/instrument/types/CreditCard$CreditCardForm;->allowedCardType:[Lcom/google/commerce/payments/orchestration/proto/ui/common/components/instrument/types/CreditCard$CardType;

    if-nez v14, :cond_f

    const/4 v3, 0x0

    .line 362
    .restart local v3    # "i":I
    :goto_7
    add-int v14, v3, v1

    new-array v7, v14, [Lcom/google/commerce/payments/orchestration/proto/ui/common/components/instrument/types/CreditCard$CardType;

    .line 364
    .local v7, "newArray":[Lcom/google/commerce/payments/orchestration/proto/ui/common/components/instrument/types/CreditCard$CardType;
    if-eqz v3, :cond_e

    .line 365
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/google/commerce/payments/orchestration/proto/ui/common/components/instrument/types/CreditCard$CreditCardForm;->allowedCardType:[Lcom/google/commerce/payments/orchestration/proto/ui/common/components/instrument/types/CreditCard$CardType;

    const/4 v15, 0x0

    const/16 v16, 0x0

    move/from16 v0, v16

    invoke-static {v14, v15, v7, v0, v3}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 367
    :cond_e
    :goto_8
    array-length v14, v7

    add-int/lit8 v14, v14, -0x1

    if-ge v3, v14, :cond_10

    .line 368
    new-instance v14, Lcom/google/commerce/payments/orchestration/proto/ui/common/components/instrument/types/CreditCard$CardType;

    invoke-direct {v14}, Lcom/google/commerce/payments/orchestration/proto/ui/common/components/instrument/types/CreditCard$CardType;-><init>()V

    aput-object v14, v7, v3

    .line 369
    aget-object v14, v7, v3

    move-object/from16 v0, p1

    invoke-virtual {v0, v14}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readMessage(Lcom/google/protobuf/nano/MessageNano;)V

    .line 370
    invoke-virtual/range {p1 .. p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readTag()I

    .line 367
    add-int/lit8 v3, v3, 0x1

    goto :goto_8

    .line 361
    .end local v3    # "i":I
    .end local v7    # "newArray":[Lcom/google/commerce/payments/orchestration/proto/ui/common/components/instrument/types/CreditCard$CardType;
    :cond_f
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/google/commerce/payments/orchestration/proto/ui/common/components/instrument/types/CreditCard$CreditCardForm;->allowedCardType:[Lcom/google/commerce/payments/orchestration/proto/ui/common/components/instrument/types/CreditCard$CardType;

    array-length v3, v14

    goto :goto_7

    .line 373
    .restart local v3    # "i":I
    .restart local v7    # "newArray":[Lcom/google/commerce/payments/orchestration/proto/ui/common/components/instrument/types/CreditCard$CardType;
    :cond_10
    new-instance v14, Lcom/google/commerce/payments/orchestration/proto/ui/common/components/instrument/types/CreditCard$CardType;

    invoke-direct {v14}, Lcom/google/commerce/payments/orchestration/proto/ui/common/components/instrument/types/CreditCard$CardType;-><init>()V

    aput-object v14, v7, v3

    .line 374
    aget-object v14, v7, v3

    move-object/from16 v0, p1

    invoke-virtual {v0, v14}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readMessage(Lcom/google/protobuf/nano/MessageNano;)V

    .line 375
    move-object/from16 v0, p0

    iput-object v7, v0, Lcom/google/commerce/payments/orchestration/proto/ui/common/components/instrument/types/CreditCard$CreditCardForm;->allowedCardType:[Lcom/google/commerce/payments/orchestration/proto/ui/common/components/instrument/types/CreditCard$CardType;

    goto/16 :goto_0

    .line 379
    .end local v1    # "arrayLength":I
    .end local v3    # "i":I
    .end local v7    # "newArray":[Lcom/google/commerce/payments/orchestration/proto/ui/common/components/instrument/types/CreditCard$CardType;
    :sswitch_8
    const/16 v14, 0x3a

    move-object/from16 v0, p1

    invoke-static {v0, v14}, Lcom/google/protobuf/nano/WireFormatNano;->getRepeatedFieldArrayLength(Lcom/google/protobuf/nano/CodedInputByteBufferNano;I)I

    move-result v1

    .line 381
    .restart local v1    # "arrayLength":I
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/google/commerce/payments/orchestration/proto/ui/common/components/instrument/types/CreditCard$CreditCardForm;->invalidBin:[Lcom/google/commerce/payments/orchestration/proto/ui/common/components/instrument/types/CreditCard$BinRange;

    if-nez v14, :cond_12

    const/4 v3, 0x0

    .line 382
    .restart local v3    # "i":I
    :goto_9
    add-int v14, v3, v1

    new-array v7, v14, [Lcom/google/commerce/payments/orchestration/proto/ui/common/components/instrument/types/CreditCard$BinRange;

    .line 384
    .local v7, "newArray":[Lcom/google/commerce/payments/orchestration/proto/ui/common/components/instrument/types/CreditCard$BinRange;
    if-eqz v3, :cond_11

    .line 385
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/google/commerce/payments/orchestration/proto/ui/common/components/instrument/types/CreditCard$CreditCardForm;->invalidBin:[Lcom/google/commerce/payments/orchestration/proto/ui/common/components/instrument/types/CreditCard$BinRange;

    const/4 v15, 0x0

    const/16 v16, 0x0

    move/from16 v0, v16

    invoke-static {v14, v15, v7, v0, v3}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 387
    :cond_11
    :goto_a
    array-length v14, v7

    add-int/lit8 v14, v14, -0x1

    if-ge v3, v14, :cond_13

    .line 388
    new-instance v14, Lcom/google/commerce/payments/orchestration/proto/ui/common/components/instrument/types/CreditCard$BinRange;

    invoke-direct {v14}, Lcom/google/commerce/payments/orchestration/proto/ui/common/components/instrument/types/CreditCard$BinRange;-><init>()V

    aput-object v14, v7, v3

    .line 389
    aget-object v14, v7, v3

    move-object/from16 v0, p1

    invoke-virtual {v0, v14}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readMessage(Lcom/google/protobuf/nano/MessageNano;)V

    .line 390
    invoke-virtual/range {p1 .. p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readTag()I

    .line 387
    add-int/lit8 v3, v3, 0x1

    goto :goto_a

    .line 381
    .end local v3    # "i":I
    .end local v7    # "newArray":[Lcom/google/commerce/payments/orchestration/proto/ui/common/components/instrument/types/CreditCard$BinRange;
    :cond_12
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/google/commerce/payments/orchestration/proto/ui/common/components/instrument/types/CreditCard$CreditCardForm;->invalidBin:[Lcom/google/commerce/payments/orchestration/proto/ui/common/components/instrument/types/CreditCard$BinRange;

    array-length v3, v14

    goto :goto_9

    .line 393
    .restart local v3    # "i":I
    .restart local v7    # "newArray":[Lcom/google/commerce/payments/orchestration/proto/ui/common/components/instrument/types/CreditCard$BinRange;
    :cond_13
    new-instance v14, Lcom/google/commerce/payments/orchestration/proto/ui/common/components/instrument/types/CreditCard$BinRange;

    invoke-direct {v14}, Lcom/google/commerce/payments/orchestration/proto/ui/common/components/instrument/types/CreditCard$BinRange;-><init>()V

    aput-object v14, v7, v3

    .line 394
    aget-object v14, v7, v3

    move-object/from16 v0, p1

    invoke-virtual {v0, v14}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readMessage(Lcom/google/protobuf/nano/MessageNano;)V

    .line 395
    move-object/from16 v0, p0

    iput-object v7, v0, Lcom/google/commerce/payments/orchestration/proto/ui/common/components/instrument/types/CreditCard$CreditCardForm;->invalidBin:[Lcom/google/commerce/payments/orchestration/proto/ui/common/components/instrument/types/CreditCard$BinRange;

    goto/16 :goto_0

    .line 399
    .end local v1    # "arrayLength":I
    .end local v3    # "i":I
    .end local v7    # "newArray":[Lcom/google/commerce/payments/orchestration/proto/ui/common/components/instrument/types/CreditCard$BinRange;
    :sswitch_9
    invoke-virtual/range {p1 .. p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readInt32()I

    move-result v14

    move-object/from16 v0, p0

    iput v14, v0, Lcom/google/commerce/payments/orchestration/proto/ui/common/components/instrument/types/CreditCard$CreditCardForm;->minExpirationMonth:I

    goto/16 :goto_0

    .line 403
    :sswitch_a
    invoke-virtual/range {p1 .. p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readInt32()I

    move-result v14

    move-object/from16 v0, p0

    iput v14, v0, Lcom/google/commerce/payments/orchestration/proto/ui/common/components/instrument/types/CreditCard$CreditCardForm;->minExpirationYear:I

    goto/16 :goto_0

    .line 407
    :sswitch_b
    invoke-virtual/range {p1 .. p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readInt32()I

    move-result v14

    move-object/from16 v0, p0

    iput v14, v0, Lcom/google/commerce/payments/orchestration/proto/ui/common/components/instrument/types/CreditCard$CreditCardForm;->maxExpirationMonth:I

    goto/16 :goto_0

    .line 411
    :sswitch_c
    invoke-virtual/range {p1 .. p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readInt32()I

    move-result v14

    move-object/from16 v0, p0

    iput v14, v0, Lcom/google/commerce/payments/orchestration/proto/ui/common/components/instrument/types/CreditCard$CreditCardForm;->maxExpirationYear:I

    goto/16 :goto_0

    .line 415
    :sswitch_d
    invoke-virtual/range {p1 .. p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readBool()Z

    move-result v14

    move-object/from16 v0, p0

    iput-boolean v14, v0, Lcom/google/commerce/payments/orchestration/proto/ui/common/components/instrument/types/CreditCard$CreditCardForm;->allowCameraInput:Z

    goto/16 :goto_0

    .line 419
    :sswitch_e
    invoke-virtual/range {p1 .. p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readString()Ljava/lang/String;

    move-result-object v14

    move-object/from16 v0, p0

    iput-object v14, v0, Lcom/google/commerce/payments/orchestration/proto/ui/common/components/instrument/types/CreditCard$CreditCardForm;->title:Ljava/lang/String;

    goto/16 :goto_0

    .line 239
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0xa -> :sswitch_2
        0x12 -> :sswitch_3
        0x1a -> :sswitch_4
        0x22 -> :sswitch_5
        0x2a -> :sswitch_6
        0x32 -> :sswitch_7
        0x3a -> :sswitch_8
        0x40 -> :sswitch_9
        0x48 -> :sswitch_a
        0x50 -> :sswitch_b
        0x58 -> :sswitch_c
        0x60 -> :sswitch_d
        0x6a -> :sswitch_e
    .end sparse-switch

    .line 258
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
    .end packed-switch

    .line 293
    :pswitch_data_1
    .packed-switch 0x0
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
    .end packed-switch

    .line 315
    :pswitch_data_2
    .packed-switch 0x0
        :pswitch_2
        :pswitch_2
        :pswitch_2
        :pswitch_2
        :pswitch_2
        :pswitch_2
        :pswitch_2
        :pswitch_2
    .end packed-switch
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/protobuf/nano/MessageNano;
    .locals 1
    .param p1, "x0"    # Lcom/google/protobuf/nano/CodedInputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 8
    invoke-virtual {p0, p1}, Lcom/google/commerce/payments/orchestration/proto/ui/common/components/instrument/types/CreditCard$CreditCardForm;->mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/commerce/payments/orchestration/proto/ui/common/components/instrument/types/CreditCard$CreditCardForm;

    move-result-object v0

    return-object v0
.end method

.method public writeTo(Lcom/google/protobuf/nano/CodedOutputByteBufferNano;)V
    .locals 5
    .param p1, "output"    # Lcom/google/protobuf/nano/CodedOutputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v4, 0x1

    .line 105
    iget-object v2, p0, Lcom/google/commerce/payments/orchestration/proto/ui/common/components/instrument/types/CreditCard$CreditCardForm;->allowedType:[I

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/google/commerce/payments/orchestration/proto/ui/common/components/instrument/types/CreditCard$CreditCardForm;->allowedType:[I

    array-length v2, v2

    if-lez v2, :cond_0

    .line 106
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    iget-object v2, p0, Lcom/google/commerce/payments/orchestration/proto/ui/common/components/instrument/types/CreditCard$CreditCardForm;->allowedType:[I

    array-length v2, v2

    if-ge v1, v2, :cond_0

    .line 107
    iget-object v2, p0, Lcom/google/commerce/payments/orchestration/proto/ui/common/components/instrument/types/CreditCard$CreditCardForm;->allowedType:[I

    aget v2, v2, v1

    invoke-virtual {p1, v4, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeInt32(II)V

    .line 106
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 110
    .end local v1    # "i":I
    :cond_0
    iget-object v2, p0, Lcom/google/commerce/payments/orchestration/proto/ui/common/components/instrument/types/CreditCard$CreditCardForm;->billingAddress:Lcom/google/commerce/payments/orchestration/proto/ui/common/components/AddressFormOuterClass$AddressForm;

    if-eqz v2, :cond_1

    .line 111
    const/4 v2, 0x2

    iget-object v3, p0, Lcom/google/commerce/payments/orchestration/proto/ui/common/components/instrument/types/CreditCard$CreditCardForm;->billingAddress:Lcom/google/commerce/payments/orchestration/proto/ui/common/components/AddressFormOuterClass$AddressForm;

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeMessage(ILcom/google/protobuf/nano/MessageNano;)V

    .line 113
    :cond_1
    iget-object v2, p0, Lcom/google/commerce/payments/orchestration/proto/ui/common/components/instrument/types/CreditCard$CreditCardForm;->initialValue:Lcom/google/commerce/payments/orchestration/proto/ui/common/components/instrument/types/CreditCard$CreditCardFormValue;

    if-eqz v2, :cond_2

    .line 114
    const/4 v2, 0x3

    iget-object v3, p0, Lcom/google/commerce/payments/orchestration/proto/ui/common/components/instrument/types/CreditCard$CreditCardForm;->initialValue:Lcom/google/commerce/payments/orchestration/proto/ui/common/components/instrument/types/CreditCard$CreditCardFormValue;

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeMessage(ILcom/google/protobuf/nano/MessageNano;)V

    .line 116
    :cond_2
    iget-object v2, p0, Lcom/google/commerce/payments/orchestration/proto/ui/common/components/instrument/types/CreditCard$CreditCardForm;->legalMessages:Lcom/google/commerce/payments/orchestration/proto/ui/common/components/legal/LegalMessageSetOuterClass$LegalMessageSet;

    if-eqz v2, :cond_3

    .line 117
    const/4 v2, 0x4

    iget-object v3, p0, Lcom/google/commerce/payments/orchestration/proto/ui/common/components/instrument/types/CreditCard$CreditCardForm;->legalMessages:Lcom/google/commerce/payments/orchestration/proto/ui/common/components/legal/LegalMessageSetOuterClass$LegalMessageSet;

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeMessage(ILcom/google/protobuf/nano/MessageNano;)V

    .line 119
    :cond_3
    iget-object v2, p0, Lcom/google/commerce/payments/orchestration/proto/ui/common/components/instrument/types/CreditCard$CreditCardForm;->id:Ljava/lang/String;

    const-string v3, ""

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_4

    .line 120
    const/4 v2, 0x5

    iget-object v3, p0, Lcom/google/commerce/payments/orchestration/proto/ui/common/components/instrument/types/CreditCard$CreditCardForm;->id:Ljava/lang/String;

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeString(ILjava/lang/String;)V

    .line 122
    :cond_4
    iget-object v2, p0, Lcom/google/commerce/payments/orchestration/proto/ui/common/components/instrument/types/CreditCard$CreditCardForm;->allowedCardType:[Lcom/google/commerce/payments/orchestration/proto/ui/common/components/instrument/types/CreditCard$CardType;

    if-eqz v2, :cond_6

    iget-object v2, p0, Lcom/google/commerce/payments/orchestration/proto/ui/common/components/instrument/types/CreditCard$CreditCardForm;->allowedCardType:[Lcom/google/commerce/payments/orchestration/proto/ui/common/components/instrument/types/CreditCard$CardType;

    array-length v2, v2

    if-lez v2, :cond_6

    .line 123
    const/4 v1, 0x0

    .restart local v1    # "i":I
    :goto_1
    iget-object v2, p0, Lcom/google/commerce/payments/orchestration/proto/ui/common/components/instrument/types/CreditCard$CreditCardForm;->allowedCardType:[Lcom/google/commerce/payments/orchestration/proto/ui/common/components/instrument/types/CreditCard$CardType;

    array-length v2, v2

    if-ge v1, v2, :cond_6

    .line 124
    iget-object v2, p0, Lcom/google/commerce/payments/orchestration/proto/ui/common/components/instrument/types/CreditCard$CreditCardForm;->allowedCardType:[Lcom/google/commerce/payments/orchestration/proto/ui/common/components/instrument/types/CreditCard$CardType;

    aget-object v0, v2, v1

    .line 125
    .local v0, "element":Lcom/google/commerce/payments/orchestration/proto/ui/common/components/instrument/types/CreditCard$CardType;
    if-eqz v0, :cond_5

    .line 126
    const/4 v2, 0x6

    invoke-virtual {p1, v2, v0}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeMessage(ILcom/google/protobuf/nano/MessageNano;)V

    .line 123
    :cond_5
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 130
    .end local v0    # "element":Lcom/google/commerce/payments/orchestration/proto/ui/common/components/instrument/types/CreditCard$CardType;
    .end local v1    # "i":I
    :cond_6
    iget-object v2, p0, Lcom/google/commerce/payments/orchestration/proto/ui/common/components/instrument/types/CreditCard$CreditCardForm;->invalidBin:[Lcom/google/commerce/payments/orchestration/proto/ui/common/components/instrument/types/CreditCard$BinRange;

    if-eqz v2, :cond_8

    iget-object v2, p0, Lcom/google/commerce/payments/orchestration/proto/ui/common/components/instrument/types/CreditCard$CreditCardForm;->invalidBin:[Lcom/google/commerce/payments/orchestration/proto/ui/common/components/instrument/types/CreditCard$BinRange;

    array-length v2, v2

    if-lez v2, :cond_8

    .line 131
    const/4 v1, 0x0

    .restart local v1    # "i":I
    :goto_2
    iget-object v2, p0, Lcom/google/commerce/payments/orchestration/proto/ui/common/components/instrument/types/CreditCard$CreditCardForm;->invalidBin:[Lcom/google/commerce/payments/orchestration/proto/ui/common/components/instrument/types/CreditCard$BinRange;

    array-length v2, v2

    if-ge v1, v2, :cond_8

    .line 132
    iget-object v2, p0, Lcom/google/commerce/payments/orchestration/proto/ui/common/components/instrument/types/CreditCard$CreditCardForm;->invalidBin:[Lcom/google/commerce/payments/orchestration/proto/ui/common/components/instrument/types/CreditCard$BinRange;

    aget-object v0, v2, v1

    .line 133
    .local v0, "element":Lcom/google/commerce/payments/orchestration/proto/ui/common/components/instrument/types/CreditCard$BinRange;
    if-eqz v0, :cond_7

    .line 134
    const/4 v2, 0x7

    invoke-virtual {p1, v2, v0}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeMessage(ILcom/google/protobuf/nano/MessageNano;)V

    .line 131
    :cond_7
    add-int/lit8 v1, v1, 0x1

    goto :goto_2

    .line 138
    .end local v0    # "element":Lcom/google/commerce/payments/orchestration/proto/ui/common/components/instrument/types/CreditCard$BinRange;
    .end local v1    # "i":I
    :cond_8
    iget v2, p0, Lcom/google/commerce/payments/orchestration/proto/ui/common/components/instrument/types/CreditCard$CreditCardForm;->minExpirationMonth:I

    if-eqz v2, :cond_9

    .line 139
    const/16 v2, 0x8

    iget v3, p0, Lcom/google/commerce/payments/orchestration/proto/ui/common/components/instrument/types/CreditCard$CreditCardForm;->minExpirationMonth:I

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeInt32(II)V

    .line 141
    :cond_9
    iget v2, p0, Lcom/google/commerce/payments/orchestration/proto/ui/common/components/instrument/types/CreditCard$CreditCardForm;->minExpirationYear:I

    if-eqz v2, :cond_a

    .line 142
    const/16 v2, 0x9

    iget v3, p0, Lcom/google/commerce/payments/orchestration/proto/ui/common/components/instrument/types/CreditCard$CreditCardForm;->minExpirationYear:I

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeInt32(II)V

    .line 144
    :cond_a
    iget v2, p0, Lcom/google/commerce/payments/orchestration/proto/ui/common/components/instrument/types/CreditCard$CreditCardForm;->maxExpirationMonth:I

    if-eqz v2, :cond_b

    .line 145
    const/16 v2, 0xa

    iget v3, p0, Lcom/google/commerce/payments/orchestration/proto/ui/common/components/instrument/types/CreditCard$CreditCardForm;->maxExpirationMonth:I

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeInt32(II)V

    .line 147
    :cond_b
    iget v2, p0, Lcom/google/commerce/payments/orchestration/proto/ui/common/components/instrument/types/CreditCard$CreditCardForm;->maxExpirationYear:I

    if-eqz v2, :cond_c

    .line 148
    const/16 v2, 0xb

    iget v3, p0, Lcom/google/commerce/payments/orchestration/proto/ui/common/components/instrument/types/CreditCard$CreditCardForm;->maxExpirationYear:I

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeInt32(II)V

    .line 150
    :cond_c
    iget-boolean v2, p0, Lcom/google/commerce/payments/orchestration/proto/ui/common/components/instrument/types/CreditCard$CreditCardForm;->allowCameraInput:Z

    if-eq v2, v4, :cond_d

    .line 151
    const/16 v2, 0xc

    iget-boolean v3, p0, Lcom/google/commerce/payments/orchestration/proto/ui/common/components/instrument/types/CreditCard$CreditCardForm;->allowCameraInput:Z

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeBool(IZ)V

    .line 153
    :cond_d
    iget-object v2, p0, Lcom/google/commerce/payments/orchestration/proto/ui/common/components/instrument/types/CreditCard$CreditCardForm;->title:Ljava/lang/String;

    const-string v3, ""

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_e

    .line 154
    const/16 v2, 0xd

    iget-object v3, p0, Lcom/google/commerce/payments/orchestration/proto/ui/common/components/instrument/types/CreditCard$CreditCardForm;->title:Ljava/lang/String;

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeString(ILjava/lang/String;)V

    .line 156
    :cond_e
    invoke-super {p0, p1}, Lcom/google/protobuf/nano/MessageNano;->writeTo(Lcom/google/protobuf/nano/CodedOutputByteBufferNano;)V

    .line 157
    return-void
.end method
