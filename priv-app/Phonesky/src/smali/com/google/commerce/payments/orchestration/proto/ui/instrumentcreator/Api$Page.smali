.class public final Lcom/google/commerce/payments/orchestration/proto/ui/instrumentcreator/Api$Page;
.super Lcom/google/protobuf/nano/MessageNano;
.source "Api.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/commerce/payments/orchestration/proto/ui/instrumentcreator/Api;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Page"
.end annotation


# instance fields
.field public creditCardExpirationDateForm:Lcom/google/commerce/payments/orchestration/proto/ui/common/components/instrument/types/CreditCard$CreditCardExpirationDateForm;

.field public customerForm:Lcom/google/commerce/payments/orchestration/proto/ui/common/components/customer/CustomerFormOuterClass$CustomerForm;

.field public instrumentForm:Lcom/google/commerce/payments/orchestration/proto/ui/common/components/instrument/InstrumentFormOuterClass$InstrumentForm;

.field public refreshTriggerField:[Lcom/google/commerce/payments/orchestration/proto/ui/common/FormFieldReferenceOuterClass$FormFieldReference;

.field public submitButtonText:Ljava/lang/String;

.field public title:Ljava/lang/String;

.field public titleImage:Lcom/google/commerce/payments/orchestration/proto/ui/common/generic/ImageWithCaptionOuterClass$ImageWithCaption;

.field public topInfoMessage:Lcom/google/commerce/payments/orchestration/proto/ui/common/components/InfoMessageOuterClass$InfoMessage;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 991
    invoke-direct {p0}, Lcom/google/protobuf/nano/MessageNano;-><init>()V

    .line 992
    invoke-virtual {p0}, Lcom/google/commerce/payments/orchestration/proto/ui/instrumentcreator/Api$Page;->clear()Lcom/google/commerce/payments/orchestration/proto/ui/instrumentcreator/Api$Page;

    .line 993
    return-void
.end method


# virtual methods
.method public clear()Lcom/google/commerce/payments/orchestration/proto/ui/instrumentcreator/Api$Page;
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 996
    const-string v0, ""

    iput-object v0, p0, Lcom/google/commerce/payments/orchestration/proto/ui/instrumentcreator/Api$Page;->title:Ljava/lang/String;

    .line 997
    iput-object v1, p0, Lcom/google/commerce/payments/orchestration/proto/ui/instrumentcreator/Api$Page;->titleImage:Lcom/google/commerce/payments/orchestration/proto/ui/common/generic/ImageWithCaptionOuterClass$ImageWithCaption;

    .line 998
    iput-object v1, p0, Lcom/google/commerce/payments/orchestration/proto/ui/instrumentcreator/Api$Page;->topInfoMessage:Lcom/google/commerce/payments/orchestration/proto/ui/common/components/InfoMessageOuterClass$InfoMessage;

    .line 999
    const-string v0, ""

    iput-object v0, p0, Lcom/google/commerce/payments/orchestration/proto/ui/instrumentcreator/Api$Page;->submitButtonText:Ljava/lang/String;

    .line 1000
    invoke-static {}, Lcom/google/commerce/payments/orchestration/proto/ui/common/FormFieldReferenceOuterClass$FormFieldReference;->emptyArray()[Lcom/google/commerce/payments/orchestration/proto/ui/common/FormFieldReferenceOuterClass$FormFieldReference;

    move-result-object v0

    iput-object v0, p0, Lcom/google/commerce/payments/orchestration/proto/ui/instrumentcreator/Api$Page;->refreshTriggerField:[Lcom/google/commerce/payments/orchestration/proto/ui/common/FormFieldReferenceOuterClass$FormFieldReference;

    .line 1001
    iput-object v1, p0, Lcom/google/commerce/payments/orchestration/proto/ui/instrumentcreator/Api$Page;->customerForm:Lcom/google/commerce/payments/orchestration/proto/ui/common/components/customer/CustomerFormOuterClass$CustomerForm;

    .line 1002
    iput-object v1, p0, Lcom/google/commerce/payments/orchestration/proto/ui/instrumentcreator/Api$Page;->instrumentForm:Lcom/google/commerce/payments/orchestration/proto/ui/common/components/instrument/InstrumentFormOuterClass$InstrumentForm;

    .line 1003
    iput-object v1, p0, Lcom/google/commerce/payments/orchestration/proto/ui/instrumentcreator/Api$Page;->creditCardExpirationDateForm:Lcom/google/commerce/payments/orchestration/proto/ui/common/components/instrument/types/CreditCard$CreditCardExpirationDateForm;

    .line 1004
    const/4 v0, -0x1

    iput v0, p0, Lcom/google/commerce/payments/orchestration/proto/ui/instrumentcreator/Api$Page;->cachedSize:I

    .line 1005
    return-object p0
.end method

.method protected computeSerializedSize()I
    .locals 5

    .prologue
    .line 1045
    invoke-super {p0}, Lcom/google/protobuf/nano/MessageNano;->computeSerializedSize()I

    move-result v2

    .line 1046
    .local v2, "size":I
    iget-object v3, p0, Lcom/google/commerce/payments/orchestration/proto/ui/instrumentcreator/Api$Page;->customerForm:Lcom/google/commerce/payments/orchestration/proto/ui/common/components/customer/CustomerFormOuterClass$CustomerForm;

    if-eqz v3, :cond_0

    .line 1047
    const/4 v3, 0x1

    iget-object v4, p0, Lcom/google/commerce/payments/orchestration/proto/ui/instrumentcreator/Api$Page;->customerForm:Lcom/google/commerce/payments/orchestration/proto/ui/common/components/customer/CustomerFormOuterClass$CustomerForm;

    invoke-static {v3, v4}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeMessageSize(ILcom/google/protobuf/nano/MessageNano;)I

    move-result v3

    add-int/2addr v2, v3

    .line 1050
    :cond_0
    iget-object v3, p0, Lcom/google/commerce/payments/orchestration/proto/ui/instrumentcreator/Api$Page;->instrumentForm:Lcom/google/commerce/payments/orchestration/proto/ui/common/components/instrument/InstrumentFormOuterClass$InstrumentForm;

    if-eqz v3, :cond_1

    .line 1051
    const/4 v3, 0x2

    iget-object v4, p0, Lcom/google/commerce/payments/orchestration/proto/ui/instrumentcreator/Api$Page;->instrumentForm:Lcom/google/commerce/payments/orchestration/proto/ui/common/components/instrument/InstrumentFormOuterClass$InstrumentForm;

    invoke-static {v3, v4}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeMessageSize(ILcom/google/protobuf/nano/MessageNano;)I

    move-result v3

    add-int/2addr v2, v3

    .line 1054
    :cond_1
    iget-object v3, p0, Lcom/google/commerce/payments/orchestration/proto/ui/instrumentcreator/Api$Page;->creditCardExpirationDateForm:Lcom/google/commerce/payments/orchestration/proto/ui/common/components/instrument/types/CreditCard$CreditCardExpirationDateForm;

    if-eqz v3, :cond_2

    .line 1055
    const/4 v3, 0x3

    iget-object v4, p0, Lcom/google/commerce/payments/orchestration/proto/ui/instrumentcreator/Api$Page;->creditCardExpirationDateForm:Lcom/google/commerce/payments/orchestration/proto/ui/common/components/instrument/types/CreditCard$CreditCardExpirationDateForm;

    invoke-static {v3, v4}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeMessageSize(ILcom/google/protobuf/nano/MessageNano;)I

    move-result v3

    add-int/2addr v2, v3

    .line 1058
    :cond_2
    iget-object v3, p0, Lcom/google/commerce/payments/orchestration/proto/ui/instrumentcreator/Api$Page;->title:Ljava/lang/String;

    const-string v4, ""

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_3

    .line 1059
    const/4 v3, 0x4

    iget-object v4, p0, Lcom/google/commerce/payments/orchestration/proto/ui/instrumentcreator/Api$Page;->title:Ljava/lang/String;

    invoke-static {v3, v4}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeStringSize(ILjava/lang/String;)I

    move-result v3

    add-int/2addr v2, v3

    .line 1062
    :cond_3
    iget-object v3, p0, Lcom/google/commerce/payments/orchestration/proto/ui/instrumentcreator/Api$Page;->submitButtonText:Ljava/lang/String;

    const-string v4, ""

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_4

    .line 1063
    const/4 v3, 0x5

    iget-object v4, p0, Lcom/google/commerce/payments/orchestration/proto/ui/instrumentcreator/Api$Page;->submitButtonText:Ljava/lang/String;

    invoke-static {v3, v4}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeStringSize(ILjava/lang/String;)I

    move-result v3

    add-int/2addr v2, v3

    .line 1066
    :cond_4
    iget-object v3, p0, Lcom/google/commerce/payments/orchestration/proto/ui/instrumentcreator/Api$Page;->refreshTriggerField:[Lcom/google/commerce/payments/orchestration/proto/ui/common/FormFieldReferenceOuterClass$FormFieldReference;

    if-eqz v3, :cond_6

    iget-object v3, p0, Lcom/google/commerce/payments/orchestration/proto/ui/instrumentcreator/Api$Page;->refreshTriggerField:[Lcom/google/commerce/payments/orchestration/proto/ui/common/FormFieldReferenceOuterClass$FormFieldReference;

    array-length v3, v3

    if-lez v3, :cond_6

    .line 1067
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    iget-object v3, p0, Lcom/google/commerce/payments/orchestration/proto/ui/instrumentcreator/Api$Page;->refreshTriggerField:[Lcom/google/commerce/payments/orchestration/proto/ui/common/FormFieldReferenceOuterClass$FormFieldReference;

    array-length v3, v3

    if-ge v1, v3, :cond_6

    .line 1068
    iget-object v3, p0, Lcom/google/commerce/payments/orchestration/proto/ui/instrumentcreator/Api$Page;->refreshTriggerField:[Lcom/google/commerce/payments/orchestration/proto/ui/common/FormFieldReferenceOuterClass$FormFieldReference;

    aget-object v0, v3, v1

    .line 1069
    .local v0, "element":Lcom/google/commerce/payments/orchestration/proto/ui/common/FormFieldReferenceOuterClass$FormFieldReference;
    if-eqz v0, :cond_5

    .line 1070
    const/16 v3, 0x8

    invoke-static {v3, v0}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeMessageSize(ILcom/google/protobuf/nano/MessageNano;)I

    move-result v3

    add-int/2addr v2, v3

    .line 1067
    :cond_5
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 1075
    .end local v0    # "element":Lcom/google/commerce/payments/orchestration/proto/ui/common/FormFieldReferenceOuterClass$FormFieldReference;
    .end local v1    # "i":I
    :cond_6
    iget-object v3, p0, Lcom/google/commerce/payments/orchestration/proto/ui/instrumentcreator/Api$Page;->topInfoMessage:Lcom/google/commerce/payments/orchestration/proto/ui/common/components/InfoMessageOuterClass$InfoMessage;

    if-eqz v3, :cond_7

    .line 1076
    const/16 v3, 0xa

    iget-object v4, p0, Lcom/google/commerce/payments/orchestration/proto/ui/instrumentcreator/Api$Page;->topInfoMessage:Lcom/google/commerce/payments/orchestration/proto/ui/common/components/InfoMessageOuterClass$InfoMessage;

    invoke-static {v3, v4}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeMessageSize(ILcom/google/protobuf/nano/MessageNano;)I

    move-result v3

    add-int/2addr v2, v3

    .line 1079
    :cond_7
    iget-object v3, p0, Lcom/google/commerce/payments/orchestration/proto/ui/instrumentcreator/Api$Page;->titleImage:Lcom/google/commerce/payments/orchestration/proto/ui/common/generic/ImageWithCaptionOuterClass$ImageWithCaption;

    if-eqz v3, :cond_8

    .line 1080
    const/16 v3, 0xb

    iget-object v4, p0, Lcom/google/commerce/payments/orchestration/proto/ui/instrumentcreator/Api$Page;->titleImage:Lcom/google/commerce/payments/orchestration/proto/ui/common/generic/ImageWithCaptionOuterClass$ImageWithCaption;

    invoke-static {v3, v4}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeMessageSize(ILcom/google/protobuf/nano/MessageNano;)I

    move-result v3

    add-int/2addr v2, v3

    .line 1083
    :cond_8
    return v2
.end method

.method public mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/commerce/payments/orchestration/proto/ui/instrumentcreator/Api$Page;
    .locals 6
    .param p1, "input"    # Lcom/google/protobuf/nano/CodedInputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v4, 0x0

    .line 1091
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readTag()I

    move-result v3

    .line 1092
    .local v3, "tag":I
    sparse-switch v3, :sswitch_data_0

    .line 1096
    invoke-static {p1, v3}, Lcom/google/protobuf/nano/WireFormatNano;->parseUnknownField(Lcom/google/protobuf/nano/CodedInputByteBufferNano;I)Z

    move-result v5

    if-nez v5, :cond_0

    .line 1097
    :sswitch_0
    return-object p0

    .line 1102
    :sswitch_1
    iget-object v5, p0, Lcom/google/commerce/payments/orchestration/proto/ui/instrumentcreator/Api$Page;->customerForm:Lcom/google/commerce/payments/orchestration/proto/ui/common/components/customer/CustomerFormOuterClass$CustomerForm;

    if-nez v5, :cond_1

    .line 1103
    new-instance v5, Lcom/google/commerce/payments/orchestration/proto/ui/common/components/customer/CustomerFormOuterClass$CustomerForm;

    invoke-direct {v5}, Lcom/google/commerce/payments/orchestration/proto/ui/common/components/customer/CustomerFormOuterClass$CustomerForm;-><init>()V

    iput-object v5, p0, Lcom/google/commerce/payments/orchestration/proto/ui/instrumentcreator/Api$Page;->customerForm:Lcom/google/commerce/payments/orchestration/proto/ui/common/components/customer/CustomerFormOuterClass$CustomerForm;

    .line 1105
    :cond_1
    iget-object v5, p0, Lcom/google/commerce/payments/orchestration/proto/ui/instrumentcreator/Api$Page;->customerForm:Lcom/google/commerce/payments/orchestration/proto/ui/common/components/customer/CustomerFormOuterClass$CustomerForm;

    invoke-virtual {p1, v5}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readMessage(Lcom/google/protobuf/nano/MessageNano;)V

    goto :goto_0

    .line 1109
    :sswitch_2
    iget-object v5, p0, Lcom/google/commerce/payments/orchestration/proto/ui/instrumentcreator/Api$Page;->instrumentForm:Lcom/google/commerce/payments/orchestration/proto/ui/common/components/instrument/InstrumentFormOuterClass$InstrumentForm;

    if-nez v5, :cond_2

    .line 1110
    new-instance v5, Lcom/google/commerce/payments/orchestration/proto/ui/common/components/instrument/InstrumentFormOuterClass$InstrumentForm;

    invoke-direct {v5}, Lcom/google/commerce/payments/orchestration/proto/ui/common/components/instrument/InstrumentFormOuterClass$InstrumentForm;-><init>()V

    iput-object v5, p0, Lcom/google/commerce/payments/orchestration/proto/ui/instrumentcreator/Api$Page;->instrumentForm:Lcom/google/commerce/payments/orchestration/proto/ui/common/components/instrument/InstrumentFormOuterClass$InstrumentForm;

    .line 1112
    :cond_2
    iget-object v5, p0, Lcom/google/commerce/payments/orchestration/proto/ui/instrumentcreator/Api$Page;->instrumentForm:Lcom/google/commerce/payments/orchestration/proto/ui/common/components/instrument/InstrumentFormOuterClass$InstrumentForm;

    invoke-virtual {p1, v5}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readMessage(Lcom/google/protobuf/nano/MessageNano;)V

    goto :goto_0

    .line 1116
    :sswitch_3
    iget-object v5, p0, Lcom/google/commerce/payments/orchestration/proto/ui/instrumentcreator/Api$Page;->creditCardExpirationDateForm:Lcom/google/commerce/payments/orchestration/proto/ui/common/components/instrument/types/CreditCard$CreditCardExpirationDateForm;

    if-nez v5, :cond_3

    .line 1117
    new-instance v5, Lcom/google/commerce/payments/orchestration/proto/ui/common/components/instrument/types/CreditCard$CreditCardExpirationDateForm;

    invoke-direct {v5}, Lcom/google/commerce/payments/orchestration/proto/ui/common/components/instrument/types/CreditCard$CreditCardExpirationDateForm;-><init>()V

    iput-object v5, p0, Lcom/google/commerce/payments/orchestration/proto/ui/instrumentcreator/Api$Page;->creditCardExpirationDateForm:Lcom/google/commerce/payments/orchestration/proto/ui/common/components/instrument/types/CreditCard$CreditCardExpirationDateForm;

    .line 1119
    :cond_3
    iget-object v5, p0, Lcom/google/commerce/payments/orchestration/proto/ui/instrumentcreator/Api$Page;->creditCardExpirationDateForm:Lcom/google/commerce/payments/orchestration/proto/ui/common/components/instrument/types/CreditCard$CreditCardExpirationDateForm;

    invoke-virtual {p1, v5}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readMessage(Lcom/google/protobuf/nano/MessageNano;)V

    goto :goto_0

    .line 1123
    :sswitch_4
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readString()Ljava/lang/String;

    move-result-object v5

    iput-object v5, p0, Lcom/google/commerce/payments/orchestration/proto/ui/instrumentcreator/Api$Page;->title:Ljava/lang/String;

    goto :goto_0

    .line 1127
    :sswitch_5
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readString()Ljava/lang/String;

    move-result-object v5

    iput-object v5, p0, Lcom/google/commerce/payments/orchestration/proto/ui/instrumentcreator/Api$Page;->submitButtonText:Ljava/lang/String;

    goto :goto_0

    .line 1131
    :sswitch_6
    const/16 v5, 0x42

    invoke-static {p1, v5}, Lcom/google/protobuf/nano/WireFormatNano;->getRepeatedFieldArrayLength(Lcom/google/protobuf/nano/CodedInputByteBufferNano;I)I

    move-result v0

    .line 1133
    .local v0, "arrayLength":I
    iget-object v5, p0, Lcom/google/commerce/payments/orchestration/proto/ui/instrumentcreator/Api$Page;->refreshTriggerField:[Lcom/google/commerce/payments/orchestration/proto/ui/common/FormFieldReferenceOuterClass$FormFieldReference;

    if-nez v5, :cond_5

    move v1, v4

    .line 1134
    .local v1, "i":I
    :goto_1
    add-int v5, v1, v0

    new-array v2, v5, [Lcom/google/commerce/payments/orchestration/proto/ui/common/FormFieldReferenceOuterClass$FormFieldReference;

    .line 1136
    .local v2, "newArray":[Lcom/google/commerce/payments/orchestration/proto/ui/common/FormFieldReferenceOuterClass$FormFieldReference;
    if-eqz v1, :cond_4

    .line 1137
    iget-object v5, p0, Lcom/google/commerce/payments/orchestration/proto/ui/instrumentcreator/Api$Page;->refreshTriggerField:[Lcom/google/commerce/payments/orchestration/proto/ui/common/FormFieldReferenceOuterClass$FormFieldReference;

    invoke-static {v5, v4, v2, v4, v1}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 1139
    :cond_4
    :goto_2
    array-length v5, v2

    add-int/lit8 v5, v5, -0x1

    if-ge v1, v5, :cond_6

    .line 1140
    new-instance v5, Lcom/google/commerce/payments/orchestration/proto/ui/common/FormFieldReferenceOuterClass$FormFieldReference;

    invoke-direct {v5}, Lcom/google/commerce/payments/orchestration/proto/ui/common/FormFieldReferenceOuterClass$FormFieldReference;-><init>()V

    aput-object v5, v2, v1

    .line 1141
    aget-object v5, v2, v1

    invoke-virtual {p1, v5}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readMessage(Lcom/google/protobuf/nano/MessageNano;)V

    .line 1142
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readTag()I

    .line 1139
    add-int/lit8 v1, v1, 0x1

    goto :goto_2

    .line 1133
    .end local v1    # "i":I
    .end local v2    # "newArray":[Lcom/google/commerce/payments/orchestration/proto/ui/common/FormFieldReferenceOuterClass$FormFieldReference;
    :cond_5
    iget-object v5, p0, Lcom/google/commerce/payments/orchestration/proto/ui/instrumentcreator/Api$Page;->refreshTriggerField:[Lcom/google/commerce/payments/orchestration/proto/ui/common/FormFieldReferenceOuterClass$FormFieldReference;

    array-length v1, v5

    goto :goto_1

    .line 1145
    .restart local v1    # "i":I
    .restart local v2    # "newArray":[Lcom/google/commerce/payments/orchestration/proto/ui/common/FormFieldReferenceOuterClass$FormFieldReference;
    :cond_6
    new-instance v5, Lcom/google/commerce/payments/orchestration/proto/ui/common/FormFieldReferenceOuterClass$FormFieldReference;

    invoke-direct {v5}, Lcom/google/commerce/payments/orchestration/proto/ui/common/FormFieldReferenceOuterClass$FormFieldReference;-><init>()V

    aput-object v5, v2, v1

    .line 1146
    aget-object v5, v2, v1

    invoke-virtual {p1, v5}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readMessage(Lcom/google/protobuf/nano/MessageNano;)V

    .line 1147
    iput-object v2, p0, Lcom/google/commerce/payments/orchestration/proto/ui/instrumentcreator/Api$Page;->refreshTriggerField:[Lcom/google/commerce/payments/orchestration/proto/ui/common/FormFieldReferenceOuterClass$FormFieldReference;

    goto/16 :goto_0

    .line 1151
    .end local v0    # "arrayLength":I
    .end local v1    # "i":I
    .end local v2    # "newArray":[Lcom/google/commerce/payments/orchestration/proto/ui/common/FormFieldReferenceOuterClass$FormFieldReference;
    :sswitch_7
    iget-object v5, p0, Lcom/google/commerce/payments/orchestration/proto/ui/instrumentcreator/Api$Page;->topInfoMessage:Lcom/google/commerce/payments/orchestration/proto/ui/common/components/InfoMessageOuterClass$InfoMessage;

    if-nez v5, :cond_7

    .line 1152
    new-instance v5, Lcom/google/commerce/payments/orchestration/proto/ui/common/components/InfoMessageOuterClass$InfoMessage;

    invoke-direct {v5}, Lcom/google/commerce/payments/orchestration/proto/ui/common/components/InfoMessageOuterClass$InfoMessage;-><init>()V

    iput-object v5, p0, Lcom/google/commerce/payments/orchestration/proto/ui/instrumentcreator/Api$Page;->topInfoMessage:Lcom/google/commerce/payments/orchestration/proto/ui/common/components/InfoMessageOuterClass$InfoMessage;

    .line 1154
    :cond_7
    iget-object v5, p0, Lcom/google/commerce/payments/orchestration/proto/ui/instrumentcreator/Api$Page;->topInfoMessage:Lcom/google/commerce/payments/orchestration/proto/ui/common/components/InfoMessageOuterClass$InfoMessage;

    invoke-virtual {p1, v5}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readMessage(Lcom/google/protobuf/nano/MessageNano;)V

    goto/16 :goto_0

    .line 1158
    :sswitch_8
    iget-object v5, p0, Lcom/google/commerce/payments/orchestration/proto/ui/instrumentcreator/Api$Page;->titleImage:Lcom/google/commerce/payments/orchestration/proto/ui/common/generic/ImageWithCaptionOuterClass$ImageWithCaption;

    if-nez v5, :cond_8

    .line 1159
    new-instance v5, Lcom/google/commerce/payments/orchestration/proto/ui/common/generic/ImageWithCaptionOuterClass$ImageWithCaption;

    invoke-direct {v5}, Lcom/google/commerce/payments/orchestration/proto/ui/common/generic/ImageWithCaptionOuterClass$ImageWithCaption;-><init>()V

    iput-object v5, p0, Lcom/google/commerce/payments/orchestration/proto/ui/instrumentcreator/Api$Page;->titleImage:Lcom/google/commerce/payments/orchestration/proto/ui/common/generic/ImageWithCaptionOuterClass$ImageWithCaption;

    .line 1161
    :cond_8
    iget-object v5, p0, Lcom/google/commerce/payments/orchestration/proto/ui/instrumentcreator/Api$Page;->titleImage:Lcom/google/commerce/payments/orchestration/proto/ui/common/generic/ImageWithCaptionOuterClass$ImageWithCaption;

    invoke-virtual {p1, v5}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readMessage(Lcom/google/protobuf/nano/MessageNano;)V

    goto/16 :goto_0

    .line 1092
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
        0x22 -> :sswitch_4
        0x2a -> :sswitch_5
        0x42 -> :sswitch_6
        0x52 -> :sswitch_7
        0x5a -> :sswitch_8
    .end sparse-switch
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/protobuf/nano/MessageNano;
    .locals 1
    .param p1, "x0"    # Lcom/google/protobuf/nano/CodedInputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 950
    invoke-virtual {p0, p1}, Lcom/google/commerce/payments/orchestration/proto/ui/instrumentcreator/Api$Page;->mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/commerce/payments/orchestration/proto/ui/instrumentcreator/Api$Page;

    move-result-object v0

    return-object v0
.end method

.method public writeTo(Lcom/google/protobuf/nano/CodedOutputByteBufferNano;)V
    .locals 4
    .param p1, "output"    # Lcom/google/protobuf/nano/CodedOutputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 1011
    iget-object v2, p0, Lcom/google/commerce/payments/orchestration/proto/ui/instrumentcreator/Api$Page;->customerForm:Lcom/google/commerce/payments/orchestration/proto/ui/common/components/customer/CustomerFormOuterClass$CustomerForm;

    if-eqz v2, :cond_0

    .line 1012
    const/4 v2, 0x1

    iget-object v3, p0, Lcom/google/commerce/payments/orchestration/proto/ui/instrumentcreator/Api$Page;->customerForm:Lcom/google/commerce/payments/orchestration/proto/ui/common/components/customer/CustomerFormOuterClass$CustomerForm;

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeMessage(ILcom/google/protobuf/nano/MessageNano;)V

    .line 1014
    :cond_0
    iget-object v2, p0, Lcom/google/commerce/payments/orchestration/proto/ui/instrumentcreator/Api$Page;->instrumentForm:Lcom/google/commerce/payments/orchestration/proto/ui/common/components/instrument/InstrumentFormOuterClass$InstrumentForm;

    if-eqz v2, :cond_1

    .line 1015
    const/4 v2, 0x2

    iget-object v3, p0, Lcom/google/commerce/payments/orchestration/proto/ui/instrumentcreator/Api$Page;->instrumentForm:Lcom/google/commerce/payments/orchestration/proto/ui/common/components/instrument/InstrumentFormOuterClass$InstrumentForm;

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeMessage(ILcom/google/protobuf/nano/MessageNano;)V

    .line 1017
    :cond_1
    iget-object v2, p0, Lcom/google/commerce/payments/orchestration/proto/ui/instrumentcreator/Api$Page;->creditCardExpirationDateForm:Lcom/google/commerce/payments/orchestration/proto/ui/common/components/instrument/types/CreditCard$CreditCardExpirationDateForm;

    if-eqz v2, :cond_2

    .line 1018
    const/4 v2, 0x3

    iget-object v3, p0, Lcom/google/commerce/payments/orchestration/proto/ui/instrumentcreator/Api$Page;->creditCardExpirationDateForm:Lcom/google/commerce/payments/orchestration/proto/ui/common/components/instrument/types/CreditCard$CreditCardExpirationDateForm;

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeMessage(ILcom/google/protobuf/nano/MessageNano;)V

    .line 1020
    :cond_2
    iget-object v2, p0, Lcom/google/commerce/payments/orchestration/proto/ui/instrumentcreator/Api$Page;->title:Ljava/lang/String;

    const-string v3, ""

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_3

    .line 1021
    const/4 v2, 0x4

    iget-object v3, p0, Lcom/google/commerce/payments/orchestration/proto/ui/instrumentcreator/Api$Page;->title:Ljava/lang/String;

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeString(ILjava/lang/String;)V

    .line 1023
    :cond_3
    iget-object v2, p0, Lcom/google/commerce/payments/orchestration/proto/ui/instrumentcreator/Api$Page;->submitButtonText:Ljava/lang/String;

    const-string v3, ""

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_4

    .line 1024
    const/4 v2, 0x5

    iget-object v3, p0, Lcom/google/commerce/payments/orchestration/proto/ui/instrumentcreator/Api$Page;->submitButtonText:Ljava/lang/String;

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeString(ILjava/lang/String;)V

    .line 1026
    :cond_4
    iget-object v2, p0, Lcom/google/commerce/payments/orchestration/proto/ui/instrumentcreator/Api$Page;->refreshTriggerField:[Lcom/google/commerce/payments/orchestration/proto/ui/common/FormFieldReferenceOuterClass$FormFieldReference;

    if-eqz v2, :cond_6

    iget-object v2, p0, Lcom/google/commerce/payments/orchestration/proto/ui/instrumentcreator/Api$Page;->refreshTriggerField:[Lcom/google/commerce/payments/orchestration/proto/ui/common/FormFieldReferenceOuterClass$FormFieldReference;

    array-length v2, v2

    if-lez v2, :cond_6

    .line 1027
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    iget-object v2, p0, Lcom/google/commerce/payments/orchestration/proto/ui/instrumentcreator/Api$Page;->refreshTriggerField:[Lcom/google/commerce/payments/orchestration/proto/ui/common/FormFieldReferenceOuterClass$FormFieldReference;

    array-length v2, v2

    if-ge v1, v2, :cond_6

    .line 1028
    iget-object v2, p0, Lcom/google/commerce/payments/orchestration/proto/ui/instrumentcreator/Api$Page;->refreshTriggerField:[Lcom/google/commerce/payments/orchestration/proto/ui/common/FormFieldReferenceOuterClass$FormFieldReference;

    aget-object v0, v2, v1

    .line 1029
    .local v0, "element":Lcom/google/commerce/payments/orchestration/proto/ui/common/FormFieldReferenceOuterClass$FormFieldReference;
    if-eqz v0, :cond_5

    .line 1030
    const/16 v2, 0x8

    invoke-virtual {p1, v2, v0}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeMessage(ILcom/google/protobuf/nano/MessageNano;)V

    .line 1027
    :cond_5
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 1034
    .end local v0    # "element":Lcom/google/commerce/payments/orchestration/proto/ui/common/FormFieldReferenceOuterClass$FormFieldReference;
    .end local v1    # "i":I
    :cond_6
    iget-object v2, p0, Lcom/google/commerce/payments/orchestration/proto/ui/instrumentcreator/Api$Page;->topInfoMessage:Lcom/google/commerce/payments/orchestration/proto/ui/common/components/InfoMessageOuterClass$InfoMessage;

    if-eqz v2, :cond_7

    .line 1035
    const/16 v2, 0xa

    iget-object v3, p0, Lcom/google/commerce/payments/orchestration/proto/ui/instrumentcreator/Api$Page;->topInfoMessage:Lcom/google/commerce/payments/orchestration/proto/ui/common/components/InfoMessageOuterClass$InfoMessage;

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeMessage(ILcom/google/protobuf/nano/MessageNano;)V

    .line 1037
    :cond_7
    iget-object v2, p0, Lcom/google/commerce/payments/orchestration/proto/ui/instrumentcreator/Api$Page;->titleImage:Lcom/google/commerce/payments/orchestration/proto/ui/common/generic/ImageWithCaptionOuterClass$ImageWithCaption;

    if-eqz v2, :cond_8

    .line 1038
    const/16 v2, 0xb

    iget-object v3, p0, Lcom/google/commerce/payments/orchestration/proto/ui/instrumentcreator/Api$Page;->titleImage:Lcom/google/commerce/payments/orchestration/proto/ui/common/generic/ImageWithCaptionOuterClass$ImageWithCaption;

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeMessage(ILcom/google/protobuf/nano/MessageNano;)V

    .line 1040
    :cond_8
    invoke-super {p0, p1}, Lcom/google/protobuf/nano/MessageNano;->writeTo(Lcom/google/protobuf/nano/CodedOutputByteBufferNano;)V

    .line 1041
    return-void
.end method
