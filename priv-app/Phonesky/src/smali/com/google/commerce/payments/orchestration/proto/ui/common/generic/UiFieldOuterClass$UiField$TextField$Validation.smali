.class public final Lcom/google/commerce/payments/orchestration/proto/ui/common/generic/UiFieldOuterClass$UiField$TextField$Validation;
.super Lcom/google/protobuf/nano/MessageNano;
.source "UiFieldOuterClass.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/commerce/payments/orchestration/proto/ui/common/generic/UiFieldOuterClass$UiField$TextField;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Validation"
.end annotation


# static fields
.field private static volatile _emptyArray:[Lcom/google/commerce/payments/orchestration/proto/ui/common/generic/UiFieldOuterClass$UiField$TextField$Validation;


# instance fields
.field public errorMessage:Ljava/lang/String;

.field public regex:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 43
    invoke-direct {p0}, Lcom/google/protobuf/nano/MessageNano;-><init>()V

    .line 44
    invoke-virtual {p0}, Lcom/google/commerce/payments/orchestration/proto/ui/common/generic/UiFieldOuterClass$UiField$TextField$Validation;->clear()Lcom/google/commerce/payments/orchestration/proto/ui/common/generic/UiFieldOuterClass$UiField$TextField$Validation;

    .line 45
    return-void
.end method

.method public static emptyArray()[Lcom/google/commerce/payments/orchestration/proto/ui/common/generic/UiFieldOuterClass$UiField$TextField$Validation;
    .locals 2

    .prologue
    .line 26
    sget-object v0, Lcom/google/commerce/payments/orchestration/proto/ui/common/generic/UiFieldOuterClass$UiField$TextField$Validation;->_emptyArray:[Lcom/google/commerce/payments/orchestration/proto/ui/common/generic/UiFieldOuterClass$UiField$TextField$Validation;

    if-nez v0, :cond_1

    .line 27
    sget-object v1, Lcom/google/protobuf/nano/InternalNano;->LAZY_INIT_LOCK:Ljava/lang/Object;

    monitor-enter v1

    .line 29
    :try_start_0
    sget-object v0, Lcom/google/commerce/payments/orchestration/proto/ui/common/generic/UiFieldOuterClass$UiField$TextField$Validation;->_emptyArray:[Lcom/google/commerce/payments/orchestration/proto/ui/common/generic/UiFieldOuterClass$UiField$TextField$Validation;

    if-nez v0, :cond_0

    .line 30
    const/4 v0, 0x0

    new-array v0, v0, [Lcom/google/commerce/payments/orchestration/proto/ui/common/generic/UiFieldOuterClass$UiField$TextField$Validation;

    sput-object v0, Lcom/google/commerce/payments/orchestration/proto/ui/common/generic/UiFieldOuterClass$UiField$TextField$Validation;->_emptyArray:[Lcom/google/commerce/payments/orchestration/proto/ui/common/generic/UiFieldOuterClass$UiField$TextField$Validation;

    .line 32
    :cond_0
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 34
    :cond_1
    sget-object v0, Lcom/google/commerce/payments/orchestration/proto/ui/common/generic/UiFieldOuterClass$UiField$TextField$Validation;->_emptyArray:[Lcom/google/commerce/payments/orchestration/proto/ui/common/generic/UiFieldOuterClass$UiField$TextField$Validation;

    return-object v0

    .line 32
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method


# virtual methods
.method public clear()Lcom/google/commerce/payments/orchestration/proto/ui/common/generic/UiFieldOuterClass$UiField$TextField$Validation;
    .locals 1

    .prologue
    .line 48
    const-string v0, ""

    iput-object v0, p0, Lcom/google/commerce/payments/orchestration/proto/ui/common/generic/UiFieldOuterClass$UiField$TextField$Validation;->regex:Ljava/lang/String;

    .line 49
    const-string v0, ""

    iput-object v0, p0, Lcom/google/commerce/payments/orchestration/proto/ui/common/generic/UiFieldOuterClass$UiField$TextField$Validation;->errorMessage:Ljava/lang/String;

    .line 50
    const/4 v0, -0x1

    iput v0, p0, Lcom/google/commerce/payments/orchestration/proto/ui/common/generic/UiFieldOuterClass$UiField$TextField$Validation;->cachedSize:I

    .line 51
    return-object p0
.end method

.method protected computeSerializedSize()I
    .locals 3

    .prologue
    .line 68
    invoke-super {p0}, Lcom/google/protobuf/nano/MessageNano;->computeSerializedSize()I

    move-result v0

    .line 69
    .local v0, "size":I
    iget-object v1, p0, Lcom/google/commerce/payments/orchestration/proto/ui/common/generic/UiFieldOuterClass$UiField$TextField$Validation;->regex:Ljava/lang/String;

    const-string v2, ""

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 70
    const/4 v1, 0x1

    iget-object v2, p0, Lcom/google/commerce/payments/orchestration/proto/ui/common/generic/UiFieldOuterClass$UiField$TextField$Validation;->regex:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeStringSize(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 73
    :cond_0
    iget-object v1, p0, Lcom/google/commerce/payments/orchestration/proto/ui/common/generic/UiFieldOuterClass$UiField$TextField$Validation;->errorMessage:Ljava/lang/String;

    const-string v2, ""

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_1

    .line 74
    const/4 v1, 0x2

    iget-object v2, p0, Lcom/google/commerce/payments/orchestration/proto/ui/common/generic/UiFieldOuterClass$UiField$TextField$Validation;->errorMessage:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeStringSize(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 77
    :cond_1
    return v0
.end method

.method public mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/commerce/payments/orchestration/proto/ui/common/generic/UiFieldOuterClass$UiField$TextField$Validation;
    .locals 2
    .param p1, "input"    # Lcom/google/protobuf/nano/CodedInputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 85
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readTag()I

    move-result v0

    .line 86
    .local v0, "tag":I
    sparse-switch v0, :sswitch_data_0

    .line 90
    invoke-static {p1, v0}, Lcom/google/protobuf/nano/WireFormatNano;->parseUnknownField(Lcom/google/protobuf/nano/CodedInputByteBufferNano;I)Z

    move-result v1

    if-nez v1, :cond_0

    .line 91
    :sswitch_0
    return-object p0

    .line 96
    :sswitch_1
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readString()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/google/commerce/payments/orchestration/proto/ui/common/generic/UiFieldOuterClass$UiField$TextField$Validation;->regex:Ljava/lang/String;

    goto :goto_0

    .line 100
    :sswitch_2
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readString()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/google/commerce/payments/orchestration/proto/ui/common/generic/UiFieldOuterClass$UiField$TextField$Validation;->errorMessage:Ljava/lang/String;

    goto :goto_0

    .line 86
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
    .end sparse-switch
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/protobuf/nano/MessageNano;
    .locals 1
    .param p1, "x0"    # Lcom/google/protobuf/nano/CodedInputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 20
    invoke-virtual {p0, p1}, Lcom/google/commerce/payments/orchestration/proto/ui/common/generic/UiFieldOuterClass$UiField$TextField$Validation;->mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/commerce/payments/orchestration/proto/ui/common/generic/UiFieldOuterClass$UiField$TextField$Validation;

    move-result-object v0

    return-object v0
.end method

.method public writeTo(Lcom/google/protobuf/nano/CodedOutputByteBufferNano;)V
    .locals 2
    .param p1, "output"    # Lcom/google/protobuf/nano/CodedOutputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 57
    iget-object v0, p0, Lcom/google/commerce/payments/orchestration/proto/ui/common/generic/UiFieldOuterClass$UiField$TextField$Validation;->regex:Ljava/lang/String;

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 58
    const/4 v0, 0x1

    iget-object v1, p0, Lcom/google/commerce/payments/orchestration/proto/ui/common/generic/UiFieldOuterClass$UiField$TextField$Validation;->regex:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeString(ILjava/lang/String;)V

    .line 60
    :cond_0
    iget-object v0, p0, Lcom/google/commerce/payments/orchestration/proto/ui/common/generic/UiFieldOuterClass$UiField$TextField$Validation;->errorMessage:Ljava/lang/String;

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 61
    const/4 v0, 0x2

    iget-object v1, p0, Lcom/google/commerce/payments/orchestration/proto/ui/common/generic/UiFieldOuterClass$UiField$TextField$Validation;->errorMessage:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeString(ILjava/lang/String;)V

    .line 63
    :cond_1
    invoke-super {p0, p1}, Lcom/google/protobuf/nano/MessageNano;->writeTo(Lcom/google/protobuf/nano/CodedOutputByteBufferNano;)V

    .line 64
    return-void
.end method
