.class public final Lcom/google/commerce/payments/orchestration/proto/ui/common/components/AddressFormOuterClass$AddressForm;
.super Lcom/google/protobuf/nano/MessageNano;
.source "AddressFormOuterClass.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/commerce/payments/orchestration/proto/ui/common/components/AddressFormOuterClass;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "AddressForm"
.end annotation


# instance fields
.field public allowedCountryCode:[Ljava/lang/String;

.field public hiddenField:[I

.field public hideFormFieldsToggleLabel:Ljava/lang/String;

.field public id:Ljava/lang/String;

.field public initialCountryI18NDataJson:Ljava/lang/String;

.field public initialValue:Lcom/google/commerce/payments/orchestration/proto/ui/common/components/AddressFormOuterClass$AddressFormValue;

.field public phoneNumberForm:Z

.field public postalCodeOnlyCountryCode:[Ljava/lang/String;

.field public readOnlyField:[I

.field public recipientLabel:Ljava/lang/String;

.field public title:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 71
    invoke-direct {p0}, Lcom/google/protobuf/nano/MessageNano;-><init>()V

    .line 72
    invoke-virtual {p0}, Lcom/google/commerce/payments/orchestration/proto/ui/common/components/AddressFormOuterClass$AddressForm;->clear()Lcom/google/commerce/payments/orchestration/proto/ui/common/components/AddressFormOuterClass$AddressForm;

    .line 73
    return-void
.end method


# virtual methods
.method public clear()Lcom/google/commerce/payments/orchestration/proto/ui/common/components/AddressFormOuterClass$AddressForm;
    .locals 1

    .prologue
    .line 76
    const-string v0, ""

    iput-object v0, p0, Lcom/google/commerce/payments/orchestration/proto/ui/common/components/AddressFormOuterClass$AddressForm;->id:Ljava/lang/String;

    .line 77
    const-string v0, ""

    iput-object v0, p0, Lcom/google/commerce/payments/orchestration/proto/ui/common/components/AddressFormOuterClass$AddressForm;->title:Ljava/lang/String;

    .line 78
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/commerce/payments/orchestration/proto/ui/common/components/AddressFormOuterClass$AddressForm;->initialValue:Lcom/google/commerce/payments/orchestration/proto/ui/common/components/AddressFormOuterClass$AddressFormValue;

    .line 79
    const-string v0, ""

    iput-object v0, p0, Lcom/google/commerce/payments/orchestration/proto/ui/common/components/AddressFormOuterClass$AddressForm;->initialCountryI18NDataJson:Ljava/lang/String;

    .line 80
    const-string v0, ""

    iput-object v0, p0, Lcom/google/commerce/payments/orchestration/proto/ui/common/components/AddressFormOuterClass$AddressForm;->hideFormFieldsToggleLabel:Ljava/lang/String;

    .line 81
    sget-object v0, Lcom/google/protobuf/nano/WireFormatNano;->EMPTY_STRING_ARRAY:[Ljava/lang/String;

    iput-object v0, p0, Lcom/google/commerce/payments/orchestration/proto/ui/common/components/AddressFormOuterClass$AddressForm;->allowedCountryCode:[Ljava/lang/String;

    .line 82
    sget-object v0, Lcom/google/protobuf/nano/WireFormatNano;->EMPTY_STRING_ARRAY:[Ljava/lang/String;

    iput-object v0, p0, Lcom/google/commerce/payments/orchestration/proto/ui/common/components/AddressFormOuterClass$AddressForm;->postalCodeOnlyCountryCode:[Ljava/lang/String;

    .line 83
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/commerce/payments/orchestration/proto/ui/common/components/AddressFormOuterClass$AddressForm;->phoneNumberForm:Z

    .line 84
    const-string v0, ""

    iput-object v0, p0, Lcom/google/commerce/payments/orchestration/proto/ui/common/components/AddressFormOuterClass$AddressForm;->recipientLabel:Ljava/lang/String;

    .line 85
    sget-object v0, Lcom/google/protobuf/nano/WireFormatNano;->EMPTY_INT_ARRAY:[I

    iput-object v0, p0, Lcom/google/commerce/payments/orchestration/proto/ui/common/components/AddressFormOuterClass$AddressForm;->readOnlyField:[I

    .line 86
    sget-object v0, Lcom/google/protobuf/nano/WireFormatNano;->EMPTY_INT_ARRAY:[I

    iput-object v0, p0, Lcom/google/commerce/payments/orchestration/proto/ui/common/components/AddressFormOuterClass$AddressForm;->hiddenField:[I

    .line 87
    const/4 v0, -0x1

    iput v0, p0, Lcom/google/commerce/payments/orchestration/proto/ui/common/components/AddressFormOuterClass$AddressForm;->cachedSize:I

    .line 88
    return-object p0
.end method

.method protected computeSerializedSize()I
    .locals 7

    .prologue
    .line 146
    invoke-super {p0}, Lcom/google/protobuf/nano/MessageNano;->computeSerializedSize()I

    move-result v4

    .line 147
    .local v4, "size":I
    iget-object v5, p0, Lcom/google/commerce/payments/orchestration/proto/ui/common/components/AddressFormOuterClass$AddressForm;->allowedCountryCode:[Ljava/lang/String;

    if-eqz v5, :cond_2

    iget-object v5, p0, Lcom/google/commerce/payments/orchestration/proto/ui/common/components/AddressFormOuterClass$AddressForm;->allowedCountryCode:[Ljava/lang/String;

    array-length v5, v5

    if-lez v5, :cond_2

    .line 148
    const/4 v0, 0x0

    .line 149
    .local v0, "dataCount":I
    const/4 v1, 0x0

    .line 150
    .local v1, "dataSize":I
    const/4 v3, 0x0

    .local v3, "i":I
    :goto_0
    iget-object v5, p0, Lcom/google/commerce/payments/orchestration/proto/ui/common/components/AddressFormOuterClass$AddressForm;->allowedCountryCode:[Ljava/lang/String;

    array-length v5, v5

    if-ge v3, v5, :cond_1

    .line 151
    iget-object v5, p0, Lcom/google/commerce/payments/orchestration/proto/ui/common/components/AddressFormOuterClass$AddressForm;->allowedCountryCode:[Ljava/lang/String;

    aget-object v2, v5, v3

    .line 152
    .local v2, "element":Ljava/lang/String;
    if-eqz v2, :cond_0

    .line 153
    add-int/lit8 v0, v0, 0x1

    .line 154
    invoke-static {v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeStringSizeNoTag(Ljava/lang/String;)I

    move-result v5

    add-int/2addr v1, v5

    .line 150
    :cond_0
    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    .line 158
    .end local v2    # "element":Ljava/lang/String;
    :cond_1
    add-int/2addr v4, v1

    .line 159
    mul-int/lit8 v5, v0, 0x1

    add-int/2addr v4, v5

    .line 161
    .end local v0    # "dataCount":I
    .end local v1    # "dataSize":I
    .end local v3    # "i":I
    :cond_2
    iget-boolean v5, p0, Lcom/google/commerce/payments/orchestration/proto/ui/common/components/AddressFormOuterClass$AddressForm;->phoneNumberForm:Z

    if-eqz v5, :cond_3

    .line 162
    const/4 v5, 0x5

    iget-boolean v6, p0, Lcom/google/commerce/payments/orchestration/proto/ui/common/components/AddressFormOuterClass$AddressForm;->phoneNumberForm:Z

    invoke-static {v5, v6}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeBoolSize(IZ)I

    move-result v5

    add-int/2addr v4, v5

    .line 165
    :cond_3
    iget-object v5, p0, Lcom/google/commerce/payments/orchestration/proto/ui/common/components/AddressFormOuterClass$AddressForm;->id:Ljava/lang/String;

    const-string v6, ""

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-nez v5, :cond_4

    .line 166
    const/4 v5, 0x6

    iget-object v6, p0, Lcom/google/commerce/payments/orchestration/proto/ui/common/components/AddressFormOuterClass$AddressForm;->id:Ljava/lang/String;

    invoke-static {v5, v6}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeStringSize(ILjava/lang/String;)I

    move-result v5

    add-int/2addr v4, v5

    .line 169
    :cond_4
    iget-object v5, p0, Lcom/google/commerce/payments/orchestration/proto/ui/common/components/AddressFormOuterClass$AddressForm;->initialValue:Lcom/google/commerce/payments/orchestration/proto/ui/common/components/AddressFormOuterClass$AddressFormValue;

    if-eqz v5, :cond_5

    .line 170
    const/4 v5, 0x7

    iget-object v6, p0, Lcom/google/commerce/payments/orchestration/proto/ui/common/components/AddressFormOuterClass$AddressForm;->initialValue:Lcom/google/commerce/payments/orchestration/proto/ui/common/components/AddressFormOuterClass$AddressFormValue;

    invoke-static {v5, v6}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeMessageSize(ILcom/google/protobuf/nano/MessageNano;)I

    move-result v5

    add-int/2addr v4, v5

    .line 173
    :cond_5
    iget-object v5, p0, Lcom/google/commerce/payments/orchestration/proto/ui/common/components/AddressFormOuterClass$AddressForm;->postalCodeOnlyCountryCode:[Ljava/lang/String;

    if-eqz v5, :cond_8

    iget-object v5, p0, Lcom/google/commerce/payments/orchestration/proto/ui/common/components/AddressFormOuterClass$AddressForm;->postalCodeOnlyCountryCode:[Ljava/lang/String;

    array-length v5, v5

    if-lez v5, :cond_8

    .line 174
    const/4 v0, 0x0

    .line 175
    .restart local v0    # "dataCount":I
    const/4 v1, 0x0

    .line 176
    .restart local v1    # "dataSize":I
    const/4 v3, 0x0

    .restart local v3    # "i":I
    :goto_1
    iget-object v5, p0, Lcom/google/commerce/payments/orchestration/proto/ui/common/components/AddressFormOuterClass$AddressForm;->postalCodeOnlyCountryCode:[Ljava/lang/String;

    array-length v5, v5

    if-ge v3, v5, :cond_7

    .line 177
    iget-object v5, p0, Lcom/google/commerce/payments/orchestration/proto/ui/common/components/AddressFormOuterClass$AddressForm;->postalCodeOnlyCountryCode:[Ljava/lang/String;

    aget-object v2, v5, v3

    .line 178
    .restart local v2    # "element":Ljava/lang/String;
    if-eqz v2, :cond_6

    .line 179
    add-int/lit8 v0, v0, 0x1

    .line 180
    invoke-static {v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeStringSizeNoTag(Ljava/lang/String;)I

    move-result v5

    add-int/2addr v1, v5

    .line 176
    :cond_6
    add-int/lit8 v3, v3, 0x1

    goto :goto_1

    .line 184
    .end local v2    # "element":Ljava/lang/String;
    :cond_7
    add-int/2addr v4, v1

    .line 185
    mul-int/lit8 v5, v0, 0x1

    add-int/2addr v4, v5

    .line 187
    .end local v0    # "dataCount":I
    .end local v1    # "dataSize":I
    .end local v3    # "i":I
    :cond_8
    iget-object v5, p0, Lcom/google/commerce/payments/orchestration/proto/ui/common/components/AddressFormOuterClass$AddressForm;->recipientLabel:Ljava/lang/String;

    const-string v6, ""

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-nez v5, :cond_9

    .line 188
    const/16 v5, 0xa

    iget-object v6, p0, Lcom/google/commerce/payments/orchestration/proto/ui/common/components/AddressFormOuterClass$AddressForm;->recipientLabel:Ljava/lang/String;

    invoke-static {v5, v6}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeStringSize(ILjava/lang/String;)I

    move-result v5

    add-int/2addr v4, v5

    .line 191
    :cond_9
    iget-object v5, p0, Lcom/google/commerce/payments/orchestration/proto/ui/common/components/AddressFormOuterClass$AddressForm;->hideFormFieldsToggleLabel:Ljava/lang/String;

    const-string v6, ""

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-nez v5, :cond_a

    .line 192
    const/16 v5, 0xb

    iget-object v6, p0, Lcom/google/commerce/payments/orchestration/proto/ui/common/components/AddressFormOuterClass$AddressForm;->hideFormFieldsToggleLabel:Ljava/lang/String;

    invoke-static {v5, v6}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeStringSize(ILjava/lang/String;)I

    move-result v5

    add-int/2addr v4, v5

    .line 195
    :cond_a
    iget-object v5, p0, Lcom/google/commerce/payments/orchestration/proto/ui/common/components/AddressFormOuterClass$AddressForm;->readOnlyField:[I

    if-eqz v5, :cond_c

    iget-object v5, p0, Lcom/google/commerce/payments/orchestration/proto/ui/common/components/AddressFormOuterClass$AddressForm;->readOnlyField:[I

    array-length v5, v5

    if-lez v5, :cond_c

    .line 196
    const/4 v1, 0x0

    .line 197
    .restart local v1    # "dataSize":I
    const/4 v3, 0x0

    .restart local v3    # "i":I
    :goto_2
    iget-object v5, p0, Lcom/google/commerce/payments/orchestration/proto/ui/common/components/AddressFormOuterClass$AddressForm;->readOnlyField:[I

    array-length v5, v5

    if-ge v3, v5, :cond_b

    .line 198
    iget-object v5, p0, Lcom/google/commerce/payments/orchestration/proto/ui/common/components/AddressFormOuterClass$AddressForm;->readOnlyField:[I

    aget v2, v5, v3

    .line 199
    .local v2, "element":I
    invoke-static {v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeInt32SizeNoTag(I)I

    move-result v5

    add-int/2addr v1, v5

    .line 197
    add-int/lit8 v3, v3, 0x1

    goto :goto_2

    .line 202
    .end local v2    # "element":I
    :cond_b
    add-int/2addr v4, v1

    .line 203
    iget-object v5, p0, Lcom/google/commerce/payments/orchestration/proto/ui/common/components/AddressFormOuterClass$AddressForm;->readOnlyField:[I

    array-length v5, v5

    mul-int/lit8 v5, v5, 0x1

    add-int/2addr v4, v5

    .line 205
    .end local v1    # "dataSize":I
    .end local v3    # "i":I
    :cond_c
    iget-object v5, p0, Lcom/google/commerce/payments/orchestration/proto/ui/common/components/AddressFormOuterClass$AddressForm;->title:Ljava/lang/String;

    const-string v6, ""

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-nez v5, :cond_d

    .line 206
    const/16 v5, 0xd

    iget-object v6, p0, Lcom/google/commerce/payments/orchestration/proto/ui/common/components/AddressFormOuterClass$AddressForm;->title:Ljava/lang/String;

    invoke-static {v5, v6}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeStringSize(ILjava/lang/String;)I

    move-result v5

    add-int/2addr v4, v5

    .line 209
    :cond_d
    iget-object v5, p0, Lcom/google/commerce/payments/orchestration/proto/ui/common/components/AddressFormOuterClass$AddressForm;->hiddenField:[I

    if-eqz v5, :cond_f

    iget-object v5, p0, Lcom/google/commerce/payments/orchestration/proto/ui/common/components/AddressFormOuterClass$AddressForm;->hiddenField:[I

    array-length v5, v5

    if-lez v5, :cond_f

    .line 210
    const/4 v1, 0x0

    .line 211
    .restart local v1    # "dataSize":I
    const/4 v3, 0x0

    .restart local v3    # "i":I
    :goto_3
    iget-object v5, p0, Lcom/google/commerce/payments/orchestration/proto/ui/common/components/AddressFormOuterClass$AddressForm;->hiddenField:[I

    array-length v5, v5

    if-ge v3, v5, :cond_e

    .line 212
    iget-object v5, p0, Lcom/google/commerce/payments/orchestration/proto/ui/common/components/AddressFormOuterClass$AddressForm;->hiddenField:[I

    aget v2, v5, v3

    .line 213
    .restart local v2    # "element":I
    invoke-static {v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeInt32SizeNoTag(I)I

    move-result v5

    add-int/2addr v1, v5

    .line 211
    add-int/lit8 v3, v3, 0x1

    goto :goto_3

    .line 216
    .end local v2    # "element":I
    :cond_e
    add-int/2addr v4, v1

    .line 217
    iget-object v5, p0, Lcom/google/commerce/payments/orchestration/proto/ui/common/components/AddressFormOuterClass$AddressForm;->hiddenField:[I

    array-length v5, v5

    mul-int/lit8 v5, v5, 0x1

    add-int/2addr v4, v5

    .line 219
    .end local v1    # "dataSize":I
    .end local v3    # "i":I
    :cond_f
    iget-object v5, p0, Lcom/google/commerce/payments/orchestration/proto/ui/common/components/AddressFormOuterClass$AddressForm;->initialCountryI18NDataJson:Ljava/lang/String;

    const-string v6, ""

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-nez v5, :cond_10

    .line 220
    const/16 v5, 0xf

    iget-object v6, p0, Lcom/google/commerce/payments/orchestration/proto/ui/common/components/AddressFormOuterClass$AddressForm;->initialCountryI18NDataJson:Ljava/lang/String;

    invoke-static {v5, v6}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeStringSize(ILjava/lang/String;)I

    move-result v5

    add-int/2addr v4, v5

    .line 223
    :cond_10
    return v4
.end method

.method public mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/commerce/payments/orchestration/proto/ui/common/components/AddressFormOuterClass$AddressForm;
    .locals 17
    .param p1, "input"    # Lcom/google/protobuf/nano/CodedInputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 231
    :cond_0
    :goto_0
    invoke-virtual/range {p1 .. p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readTag()I

    move-result v9

    .line 232
    .local v9, "tag":I
    sparse-switch v9, :sswitch_data_0

    .line 236
    move-object/from16 v0, p1

    invoke-static {v0, v9}, Lcom/google/protobuf/nano/WireFormatNano;->parseUnknownField(Lcom/google/protobuf/nano/CodedInputByteBufferNano;I)Z

    move-result v14

    if-nez v14, :cond_0

    .line 237
    :sswitch_0
    return-object p0

    .line 242
    :sswitch_1
    const/16 v14, 0x22

    move-object/from16 v0, p1

    invoke-static {v0, v14}, Lcom/google/protobuf/nano/WireFormatNano;->getRepeatedFieldArrayLength(Lcom/google/protobuf/nano/CodedInputByteBufferNano;I)I

    move-result v1

    .line 244
    .local v1, "arrayLength":I
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/google/commerce/payments/orchestration/proto/ui/common/components/AddressFormOuterClass$AddressForm;->allowedCountryCode:[Ljava/lang/String;

    if-nez v14, :cond_2

    const/4 v3, 0x0

    .line 245
    .local v3, "i":I
    :goto_1
    add-int v14, v3, v1

    new-array v7, v14, [Ljava/lang/String;

    .line 246
    .local v7, "newArray":[Ljava/lang/String;
    if-eqz v3, :cond_1

    .line 247
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/google/commerce/payments/orchestration/proto/ui/common/components/AddressFormOuterClass$AddressForm;->allowedCountryCode:[Ljava/lang/String;

    const/4 v15, 0x0

    const/16 v16, 0x0

    move/from16 v0, v16

    invoke-static {v14, v15, v7, v0, v3}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 249
    :cond_1
    :goto_2
    array-length v14, v7

    add-int/lit8 v14, v14, -0x1

    if-ge v3, v14, :cond_3

    .line 250
    invoke-virtual/range {p1 .. p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readString()Ljava/lang/String;

    move-result-object v14

    aput-object v14, v7, v3

    .line 251
    invoke-virtual/range {p1 .. p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readTag()I

    .line 249
    add-int/lit8 v3, v3, 0x1

    goto :goto_2

    .line 244
    .end local v3    # "i":I
    .end local v7    # "newArray":[Ljava/lang/String;
    :cond_2
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/google/commerce/payments/orchestration/proto/ui/common/components/AddressFormOuterClass$AddressForm;->allowedCountryCode:[Ljava/lang/String;

    array-length v3, v14

    goto :goto_1

    .line 254
    .restart local v3    # "i":I
    .restart local v7    # "newArray":[Ljava/lang/String;
    :cond_3
    invoke-virtual/range {p1 .. p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readString()Ljava/lang/String;

    move-result-object v14

    aput-object v14, v7, v3

    .line 255
    move-object/from16 v0, p0

    iput-object v7, v0, Lcom/google/commerce/payments/orchestration/proto/ui/common/components/AddressFormOuterClass$AddressForm;->allowedCountryCode:[Ljava/lang/String;

    goto :goto_0

    .line 259
    .end local v1    # "arrayLength":I
    .end local v3    # "i":I
    .end local v7    # "newArray":[Ljava/lang/String;
    :sswitch_2
    invoke-virtual/range {p1 .. p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readBool()Z

    move-result v14

    move-object/from16 v0, p0

    iput-boolean v14, v0, Lcom/google/commerce/payments/orchestration/proto/ui/common/components/AddressFormOuterClass$AddressForm;->phoneNumberForm:Z

    goto :goto_0

    .line 263
    :sswitch_3
    invoke-virtual/range {p1 .. p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readString()Ljava/lang/String;

    move-result-object v14

    move-object/from16 v0, p0

    iput-object v14, v0, Lcom/google/commerce/payments/orchestration/proto/ui/common/components/AddressFormOuterClass$AddressForm;->id:Ljava/lang/String;

    goto :goto_0

    .line 267
    :sswitch_4
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/google/commerce/payments/orchestration/proto/ui/common/components/AddressFormOuterClass$AddressForm;->initialValue:Lcom/google/commerce/payments/orchestration/proto/ui/common/components/AddressFormOuterClass$AddressFormValue;

    if-nez v14, :cond_4

    .line 268
    new-instance v14, Lcom/google/commerce/payments/orchestration/proto/ui/common/components/AddressFormOuterClass$AddressFormValue;

    invoke-direct {v14}, Lcom/google/commerce/payments/orchestration/proto/ui/common/components/AddressFormOuterClass$AddressFormValue;-><init>()V

    move-object/from16 v0, p0

    iput-object v14, v0, Lcom/google/commerce/payments/orchestration/proto/ui/common/components/AddressFormOuterClass$AddressForm;->initialValue:Lcom/google/commerce/payments/orchestration/proto/ui/common/components/AddressFormOuterClass$AddressFormValue;

    .line 270
    :cond_4
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/google/commerce/payments/orchestration/proto/ui/common/components/AddressFormOuterClass$AddressForm;->initialValue:Lcom/google/commerce/payments/orchestration/proto/ui/common/components/AddressFormOuterClass$AddressFormValue;

    move-object/from16 v0, p1

    invoke-virtual {v0, v14}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readMessage(Lcom/google/protobuf/nano/MessageNano;)V

    goto :goto_0

    .line 274
    :sswitch_5
    const/16 v14, 0x4a

    move-object/from16 v0, p1

    invoke-static {v0, v14}, Lcom/google/protobuf/nano/WireFormatNano;->getRepeatedFieldArrayLength(Lcom/google/protobuf/nano/CodedInputByteBufferNano;I)I

    move-result v1

    .line 276
    .restart local v1    # "arrayLength":I
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/google/commerce/payments/orchestration/proto/ui/common/components/AddressFormOuterClass$AddressForm;->postalCodeOnlyCountryCode:[Ljava/lang/String;

    if-nez v14, :cond_6

    const/4 v3, 0x0

    .line 277
    .restart local v3    # "i":I
    :goto_3
    add-int v14, v3, v1

    new-array v7, v14, [Ljava/lang/String;

    .line 278
    .restart local v7    # "newArray":[Ljava/lang/String;
    if-eqz v3, :cond_5

    .line 279
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/google/commerce/payments/orchestration/proto/ui/common/components/AddressFormOuterClass$AddressForm;->postalCodeOnlyCountryCode:[Ljava/lang/String;

    const/4 v15, 0x0

    const/16 v16, 0x0

    move/from16 v0, v16

    invoke-static {v14, v15, v7, v0, v3}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 281
    :cond_5
    :goto_4
    array-length v14, v7

    add-int/lit8 v14, v14, -0x1

    if-ge v3, v14, :cond_7

    .line 282
    invoke-virtual/range {p1 .. p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readString()Ljava/lang/String;

    move-result-object v14

    aput-object v14, v7, v3

    .line 283
    invoke-virtual/range {p1 .. p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readTag()I

    .line 281
    add-int/lit8 v3, v3, 0x1

    goto :goto_4

    .line 276
    .end local v3    # "i":I
    .end local v7    # "newArray":[Ljava/lang/String;
    :cond_6
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/google/commerce/payments/orchestration/proto/ui/common/components/AddressFormOuterClass$AddressForm;->postalCodeOnlyCountryCode:[Ljava/lang/String;

    array-length v3, v14

    goto :goto_3

    .line 286
    .restart local v3    # "i":I
    .restart local v7    # "newArray":[Ljava/lang/String;
    :cond_7
    invoke-virtual/range {p1 .. p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readString()Ljava/lang/String;

    move-result-object v14

    aput-object v14, v7, v3

    .line 287
    move-object/from16 v0, p0

    iput-object v7, v0, Lcom/google/commerce/payments/orchestration/proto/ui/common/components/AddressFormOuterClass$AddressForm;->postalCodeOnlyCountryCode:[Ljava/lang/String;

    goto/16 :goto_0

    .line 291
    .end local v1    # "arrayLength":I
    .end local v3    # "i":I
    .end local v7    # "newArray":[Ljava/lang/String;
    :sswitch_6
    invoke-virtual/range {p1 .. p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readString()Ljava/lang/String;

    move-result-object v14

    move-object/from16 v0, p0

    iput-object v14, v0, Lcom/google/commerce/payments/orchestration/proto/ui/common/components/AddressFormOuterClass$AddressForm;->recipientLabel:Ljava/lang/String;

    goto/16 :goto_0

    .line 295
    :sswitch_7
    invoke-virtual/range {p1 .. p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readString()Ljava/lang/String;

    move-result-object v14

    move-object/from16 v0, p0

    iput-object v14, v0, Lcom/google/commerce/payments/orchestration/proto/ui/common/components/AddressFormOuterClass$AddressForm;->hideFormFieldsToggleLabel:Ljava/lang/String;

    goto/16 :goto_0

    .line 299
    :sswitch_8
    const/16 v14, 0x60

    move-object/from16 v0, p1

    invoke-static {v0, v14}, Lcom/google/protobuf/nano/WireFormatNano;->getRepeatedFieldArrayLength(Lcom/google/protobuf/nano/CodedInputByteBufferNano;I)I

    move-result v5

    .line 301
    .local v5, "length":I
    new-array v12, v5, [I

    .line 302
    .local v12, "validValues":[I
    const/4 v10, 0x0

    .line 303
    .local v10, "validCount":I
    const/4 v3, 0x0

    .restart local v3    # "i":I
    move v11, v10

    .end local v10    # "validCount":I
    .local v11, "validCount":I
    :goto_5
    if-ge v3, v5, :cond_9

    .line 304
    if-eqz v3, :cond_8

    .line 305
    invoke-virtual/range {p1 .. p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readTag()I

    .line 307
    :cond_8
    invoke-virtual/range {p1 .. p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readInt32()I

    move-result v13

    .line 308
    .local v13, "value":I
    packed-switch v13, :pswitch_data_0

    move v10, v11

    .line 303
    .end local v11    # "validCount":I
    .restart local v10    # "validCount":I
    :goto_6
    add-int/lit8 v3, v3, 0x1

    move v11, v10

    .end local v10    # "validCount":I
    .restart local v11    # "validCount":I
    goto :goto_5

    .line 320
    :pswitch_0
    add-int/lit8 v10, v11, 0x1

    .end local v11    # "validCount":I
    .restart local v10    # "validCount":I
    aput v13, v12, v11

    goto :goto_6

    .line 324
    .end local v10    # "validCount":I
    .end local v13    # "value":I
    .restart local v11    # "validCount":I
    :cond_9
    if-eqz v11, :cond_0

    .line 325
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/google/commerce/payments/orchestration/proto/ui/common/components/AddressFormOuterClass$AddressForm;->readOnlyField:[I

    if-nez v14, :cond_a

    const/4 v3, 0x0

    .line 326
    :goto_7
    if-nez v3, :cond_b

    array-length v14, v12

    if-ne v11, v14, :cond_b

    .line 327
    move-object/from16 v0, p0

    iput-object v12, v0, Lcom/google/commerce/payments/orchestration/proto/ui/common/components/AddressFormOuterClass$AddressForm;->readOnlyField:[I

    goto/16 :goto_0

    .line 325
    :cond_a
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/google/commerce/payments/orchestration/proto/ui/common/components/AddressFormOuterClass$AddressForm;->readOnlyField:[I

    array-length v3, v14

    goto :goto_7

    .line 329
    :cond_b
    add-int v14, v3, v11

    new-array v7, v14, [I

    .line 330
    .local v7, "newArray":[I
    if-eqz v3, :cond_c

    .line 331
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/google/commerce/payments/orchestration/proto/ui/common/components/AddressFormOuterClass$AddressForm;->readOnlyField:[I

    const/4 v15, 0x0

    const/16 v16, 0x0

    move/from16 v0, v16

    invoke-static {v14, v15, v7, v0, v3}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 333
    :cond_c
    const/4 v14, 0x0

    invoke-static {v12, v14, v7, v3, v11}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 334
    move-object/from16 v0, p0

    iput-object v7, v0, Lcom/google/commerce/payments/orchestration/proto/ui/common/components/AddressFormOuterClass$AddressForm;->readOnlyField:[I

    goto/16 :goto_0

    .line 340
    .end local v3    # "i":I
    .end local v5    # "length":I
    .end local v7    # "newArray":[I
    .end local v11    # "validCount":I
    .end local v12    # "validValues":[I
    :sswitch_9
    invoke-virtual/range {p1 .. p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readRawVarint32()I

    move-result v2

    .line 341
    .local v2, "bytes":I
    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->pushLimit(I)I

    move-result v6

    .line 343
    .local v6, "limit":I
    const/4 v1, 0x0

    .line 344
    .restart local v1    # "arrayLength":I
    invoke-virtual/range {p1 .. p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->getPosition()I

    move-result v8

    .line 345
    .local v8, "startPos":I
    :goto_8
    invoke-virtual/range {p1 .. p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->getBytesUntilLimit()I

    move-result v14

    if-lez v14, :cond_d

    .line 346
    invoke-virtual/range {p1 .. p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readInt32()I

    move-result v14

    packed-switch v14, :pswitch_data_1

    goto :goto_8

    .line 358
    :pswitch_1
    add-int/lit8 v1, v1, 0x1

    goto :goto_8

    .line 362
    :cond_d
    if-eqz v1, :cond_11

    .line 363
    move-object/from16 v0, p1

    invoke-virtual {v0, v8}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->rewindToPosition(I)V

    .line 364
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/google/commerce/payments/orchestration/proto/ui/common/components/AddressFormOuterClass$AddressForm;->readOnlyField:[I

    if-nez v14, :cond_f

    const/4 v3, 0x0

    .line 365
    .restart local v3    # "i":I
    :goto_9
    add-int v14, v3, v1

    new-array v7, v14, [I

    .line 366
    .restart local v7    # "newArray":[I
    if-eqz v3, :cond_e

    .line 367
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/google/commerce/payments/orchestration/proto/ui/common/components/AddressFormOuterClass$AddressForm;->readOnlyField:[I

    const/4 v15, 0x0

    const/16 v16, 0x0

    move/from16 v0, v16

    invoke-static {v14, v15, v7, v0, v3}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 369
    :cond_e
    :goto_a
    invoke-virtual/range {p1 .. p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->getBytesUntilLimit()I

    move-result v14

    if-lez v14, :cond_10

    .line 370
    invoke-virtual/range {p1 .. p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readInt32()I

    move-result v13

    .line 371
    .restart local v13    # "value":I
    packed-switch v13, :pswitch_data_2

    goto :goto_a

    .line 383
    :pswitch_2
    add-int/lit8 v4, v3, 0x1

    .end local v3    # "i":I
    .local v4, "i":I
    aput v13, v7, v3

    move v3, v4

    .end local v4    # "i":I
    .restart local v3    # "i":I
    goto :goto_a

    .line 364
    .end local v3    # "i":I
    .end local v7    # "newArray":[I
    .end local v13    # "value":I
    :cond_f
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/google/commerce/payments/orchestration/proto/ui/common/components/AddressFormOuterClass$AddressForm;->readOnlyField:[I

    array-length v3, v14

    goto :goto_9

    .line 387
    .restart local v3    # "i":I
    .restart local v7    # "newArray":[I
    :cond_10
    move-object/from16 v0, p0

    iput-object v7, v0, Lcom/google/commerce/payments/orchestration/proto/ui/common/components/AddressFormOuterClass$AddressForm;->readOnlyField:[I

    .line 389
    .end local v3    # "i":I
    .end local v7    # "newArray":[I
    :cond_11
    move-object/from16 v0, p1

    invoke-virtual {v0, v6}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->popLimit(I)V

    goto/16 :goto_0

    .line 393
    .end local v1    # "arrayLength":I
    .end local v2    # "bytes":I
    .end local v6    # "limit":I
    .end local v8    # "startPos":I
    :sswitch_a
    invoke-virtual/range {p1 .. p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readString()Ljava/lang/String;

    move-result-object v14

    move-object/from16 v0, p0

    iput-object v14, v0, Lcom/google/commerce/payments/orchestration/proto/ui/common/components/AddressFormOuterClass$AddressForm;->title:Ljava/lang/String;

    goto/16 :goto_0

    .line 397
    :sswitch_b
    const/16 v14, 0x70

    move-object/from16 v0, p1

    invoke-static {v0, v14}, Lcom/google/protobuf/nano/WireFormatNano;->getRepeatedFieldArrayLength(Lcom/google/protobuf/nano/CodedInputByteBufferNano;I)I

    move-result v5

    .line 399
    .restart local v5    # "length":I
    new-array v12, v5, [I

    .line 400
    .restart local v12    # "validValues":[I
    const/4 v10, 0x0

    .line 401
    .restart local v10    # "validCount":I
    const/4 v3, 0x0

    .restart local v3    # "i":I
    move v11, v10

    .end local v10    # "validCount":I
    .restart local v11    # "validCount":I
    :goto_b
    if-ge v3, v5, :cond_13

    .line 402
    if-eqz v3, :cond_12

    .line 403
    invoke-virtual/range {p1 .. p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readTag()I

    .line 405
    :cond_12
    invoke-virtual/range {p1 .. p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readInt32()I

    move-result v13

    .line 406
    .restart local v13    # "value":I
    packed-switch v13, :pswitch_data_3

    move v10, v11

    .line 401
    .end local v11    # "validCount":I
    .restart local v10    # "validCount":I
    :goto_c
    add-int/lit8 v3, v3, 0x1

    move v11, v10

    .end local v10    # "validCount":I
    .restart local v11    # "validCount":I
    goto :goto_b

    .line 418
    :pswitch_3
    add-int/lit8 v10, v11, 0x1

    .end local v11    # "validCount":I
    .restart local v10    # "validCount":I
    aput v13, v12, v11

    goto :goto_c

    .line 422
    .end local v10    # "validCount":I
    .end local v13    # "value":I
    .restart local v11    # "validCount":I
    :cond_13
    if-eqz v11, :cond_0

    .line 423
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/google/commerce/payments/orchestration/proto/ui/common/components/AddressFormOuterClass$AddressForm;->hiddenField:[I

    if-nez v14, :cond_14

    const/4 v3, 0x0

    .line 424
    :goto_d
    if-nez v3, :cond_15

    array-length v14, v12

    if-ne v11, v14, :cond_15

    .line 425
    move-object/from16 v0, p0

    iput-object v12, v0, Lcom/google/commerce/payments/orchestration/proto/ui/common/components/AddressFormOuterClass$AddressForm;->hiddenField:[I

    goto/16 :goto_0

    .line 423
    :cond_14
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/google/commerce/payments/orchestration/proto/ui/common/components/AddressFormOuterClass$AddressForm;->hiddenField:[I

    array-length v3, v14

    goto :goto_d

    .line 427
    :cond_15
    add-int v14, v3, v11

    new-array v7, v14, [I

    .line 428
    .restart local v7    # "newArray":[I
    if-eqz v3, :cond_16

    .line 429
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/google/commerce/payments/orchestration/proto/ui/common/components/AddressFormOuterClass$AddressForm;->hiddenField:[I

    const/4 v15, 0x0

    const/16 v16, 0x0

    move/from16 v0, v16

    invoke-static {v14, v15, v7, v0, v3}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 431
    :cond_16
    const/4 v14, 0x0

    invoke-static {v12, v14, v7, v3, v11}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 432
    move-object/from16 v0, p0

    iput-object v7, v0, Lcom/google/commerce/payments/orchestration/proto/ui/common/components/AddressFormOuterClass$AddressForm;->hiddenField:[I

    goto/16 :goto_0

    .line 438
    .end local v3    # "i":I
    .end local v5    # "length":I
    .end local v7    # "newArray":[I
    .end local v11    # "validCount":I
    .end local v12    # "validValues":[I
    :sswitch_c
    invoke-virtual/range {p1 .. p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readRawVarint32()I

    move-result v2

    .line 439
    .restart local v2    # "bytes":I
    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->pushLimit(I)I

    move-result v6

    .line 441
    .restart local v6    # "limit":I
    const/4 v1, 0x0

    .line 442
    .restart local v1    # "arrayLength":I
    invoke-virtual/range {p1 .. p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->getPosition()I

    move-result v8

    .line 443
    .restart local v8    # "startPos":I
    :goto_e
    invoke-virtual/range {p1 .. p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->getBytesUntilLimit()I

    move-result v14

    if-lez v14, :cond_17

    .line 444
    invoke-virtual/range {p1 .. p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readInt32()I

    move-result v14

    packed-switch v14, :pswitch_data_4

    goto :goto_e

    .line 456
    :pswitch_4
    add-int/lit8 v1, v1, 0x1

    goto :goto_e

    .line 460
    :cond_17
    if-eqz v1, :cond_1b

    .line 461
    move-object/from16 v0, p1

    invoke-virtual {v0, v8}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->rewindToPosition(I)V

    .line 462
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/google/commerce/payments/orchestration/proto/ui/common/components/AddressFormOuterClass$AddressForm;->hiddenField:[I

    if-nez v14, :cond_19

    const/4 v3, 0x0

    .line 463
    .restart local v3    # "i":I
    :goto_f
    add-int v14, v3, v1

    new-array v7, v14, [I

    .line 464
    .restart local v7    # "newArray":[I
    if-eqz v3, :cond_18

    .line 465
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/google/commerce/payments/orchestration/proto/ui/common/components/AddressFormOuterClass$AddressForm;->hiddenField:[I

    const/4 v15, 0x0

    const/16 v16, 0x0

    move/from16 v0, v16

    invoke-static {v14, v15, v7, v0, v3}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 467
    :cond_18
    :goto_10
    invoke-virtual/range {p1 .. p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->getBytesUntilLimit()I

    move-result v14

    if-lez v14, :cond_1a

    .line 468
    invoke-virtual/range {p1 .. p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readInt32()I

    move-result v13

    .line 469
    .restart local v13    # "value":I
    packed-switch v13, :pswitch_data_5

    goto :goto_10

    .line 481
    :pswitch_5
    add-int/lit8 v4, v3, 0x1

    .end local v3    # "i":I
    .restart local v4    # "i":I
    aput v13, v7, v3

    move v3, v4

    .end local v4    # "i":I
    .restart local v3    # "i":I
    goto :goto_10

    .line 462
    .end local v3    # "i":I
    .end local v7    # "newArray":[I
    .end local v13    # "value":I
    :cond_19
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/google/commerce/payments/orchestration/proto/ui/common/components/AddressFormOuterClass$AddressForm;->hiddenField:[I

    array-length v3, v14

    goto :goto_f

    .line 485
    .restart local v3    # "i":I
    .restart local v7    # "newArray":[I
    :cond_1a
    move-object/from16 v0, p0

    iput-object v7, v0, Lcom/google/commerce/payments/orchestration/proto/ui/common/components/AddressFormOuterClass$AddressForm;->hiddenField:[I

    .line 487
    .end local v3    # "i":I
    .end local v7    # "newArray":[I
    :cond_1b
    move-object/from16 v0, p1

    invoke-virtual {v0, v6}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->popLimit(I)V

    goto/16 :goto_0

    .line 491
    .end local v1    # "arrayLength":I
    .end local v2    # "bytes":I
    .end local v6    # "limit":I
    .end local v8    # "startPos":I
    :sswitch_d
    invoke-virtual/range {p1 .. p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readString()Ljava/lang/String;

    move-result-object v14

    move-object/from16 v0, p0

    iput-object v14, v0, Lcom/google/commerce/payments/orchestration/proto/ui/common/components/AddressFormOuterClass$AddressForm;->initialCountryI18NDataJson:Ljava/lang/String;

    goto/16 :goto_0

    .line 232
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x22 -> :sswitch_1
        0x28 -> :sswitch_2
        0x32 -> :sswitch_3
        0x3a -> :sswitch_4
        0x4a -> :sswitch_5
        0x52 -> :sswitch_6
        0x5a -> :sswitch_7
        0x60 -> :sswitch_8
        0x62 -> :sswitch_9
        0x6a -> :sswitch_a
        0x70 -> :sswitch_b
        0x72 -> :sswitch_c
        0x7a -> :sswitch_d
    .end sparse-switch

    .line 308
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
    .end packed-switch

    .line 346
    :pswitch_data_1
    .packed-switch 0x1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
    .end packed-switch

    .line 371
    :pswitch_data_2
    .packed-switch 0x1
        :pswitch_2
        :pswitch_2
        :pswitch_2
        :pswitch_2
        :pswitch_2
        :pswitch_2
        :pswitch_2
        :pswitch_2
        :pswitch_2
        :pswitch_2
        :pswitch_2
    .end packed-switch

    .line 406
    :pswitch_data_3
    .packed-switch 0x1
        :pswitch_3
        :pswitch_3
        :pswitch_3
        :pswitch_3
        :pswitch_3
        :pswitch_3
        :pswitch_3
        :pswitch_3
        :pswitch_3
        :pswitch_3
        :pswitch_3
    .end packed-switch

    .line 444
    :pswitch_data_4
    .packed-switch 0x1
        :pswitch_4
        :pswitch_4
        :pswitch_4
        :pswitch_4
        :pswitch_4
        :pswitch_4
        :pswitch_4
        :pswitch_4
        :pswitch_4
        :pswitch_4
        :pswitch_4
    .end packed-switch

    .line 469
    :pswitch_data_5
    .packed-switch 0x1
        :pswitch_5
        :pswitch_5
        :pswitch_5
        :pswitch_5
        :pswitch_5
        :pswitch_5
        :pswitch_5
        :pswitch_5
        :pswitch_5
        :pswitch_5
        :pswitch_5
    .end packed-switch
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/protobuf/nano/MessageNano;
    .locals 1
    .param p1, "x0"    # Lcom/google/protobuf/nano/CodedInputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 8
    invoke-virtual {p0, p1}, Lcom/google/commerce/payments/orchestration/proto/ui/common/components/AddressFormOuterClass$AddressForm;->mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/commerce/payments/orchestration/proto/ui/common/components/AddressFormOuterClass$AddressForm;

    move-result-object v0

    return-object v0
.end method

.method public writeTo(Lcom/google/protobuf/nano/CodedOutputByteBufferNano;)V
    .locals 4
    .param p1, "output"    # Lcom/google/protobuf/nano/CodedOutputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 94
    iget-object v2, p0, Lcom/google/commerce/payments/orchestration/proto/ui/common/components/AddressFormOuterClass$AddressForm;->allowedCountryCode:[Ljava/lang/String;

    if-eqz v2, :cond_1

    iget-object v2, p0, Lcom/google/commerce/payments/orchestration/proto/ui/common/components/AddressFormOuterClass$AddressForm;->allowedCountryCode:[Ljava/lang/String;

    array-length v2, v2

    if-lez v2, :cond_1

    .line 95
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    iget-object v2, p0, Lcom/google/commerce/payments/orchestration/proto/ui/common/components/AddressFormOuterClass$AddressForm;->allowedCountryCode:[Ljava/lang/String;

    array-length v2, v2

    if-ge v1, v2, :cond_1

    .line 96
    iget-object v2, p0, Lcom/google/commerce/payments/orchestration/proto/ui/common/components/AddressFormOuterClass$AddressForm;->allowedCountryCode:[Ljava/lang/String;

    aget-object v0, v2, v1

    .line 97
    .local v0, "element":Ljava/lang/String;
    if-eqz v0, :cond_0

    .line 98
    const/4 v2, 0x4

    invoke-virtual {p1, v2, v0}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeString(ILjava/lang/String;)V

    .line 95
    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 102
    .end local v0    # "element":Ljava/lang/String;
    .end local v1    # "i":I
    :cond_1
    iget-boolean v2, p0, Lcom/google/commerce/payments/orchestration/proto/ui/common/components/AddressFormOuterClass$AddressForm;->phoneNumberForm:Z

    if-eqz v2, :cond_2

    .line 103
    const/4 v2, 0x5

    iget-boolean v3, p0, Lcom/google/commerce/payments/orchestration/proto/ui/common/components/AddressFormOuterClass$AddressForm;->phoneNumberForm:Z

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeBool(IZ)V

    .line 105
    :cond_2
    iget-object v2, p0, Lcom/google/commerce/payments/orchestration/proto/ui/common/components/AddressFormOuterClass$AddressForm;->id:Ljava/lang/String;

    const-string v3, ""

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_3

    .line 106
    const/4 v2, 0x6

    iget-object v3, p0, Lcom/google/commerce/payments/orchestration/proto/ui/common/components/AddressFormOuterClass$AddressForm;->id:Ljava/lang/String;

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeString(ILjava/lang/String;)V

    .line 108
    :cond_3
    iget-object v2, p0, Lcom/google/commerce/payments/orchestration/proto/ui/common/components/AddressFormOuterClass$AddressForm;->initialValue:Lcom/google/commerce/payments/orchestration/proto/ui/common/components/AddressFormOuterClass$AddressFormValue;

    if-eqz v2, :cond_4

    .line 109
    const/4 v2, 0x7

    iget-object v3, p0, Lcom/google/commerce/payments/orchestration/proto/ui/common/components/AddressFormOuterClass$AddressForm;->initialValue:Lcom/google/commerce/payments/orchestration/proto/ui/common/components/AddressFormOuterClass$AddressFormValue;

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeMessage(ILcom/google/protobuf/nano/MessageNano;)V

    .line 111
    :cond_4
    iget-object v2, p0, Lcom/google/commerce/payments/orchestration/proto/ui/common/components/AddressFormOuterClass$AddressForm;->postalCodeOnlyCountryCode:[Ljava/lang/String;

    if-eqz v2, :cond_6

    iget-object v2, p0, Lcom/google/commerce/payments/orchestration/proto/ui/common/components/AddressFormOuterClass$AddressForm;->postalCodeOnlyCountryCode:[Ljava/lang/String;

    array-length v2, v2

    if-lez v2, :cond_6

    .line 112
    const/4 v1, 0x0

    .restart local v1    # "i":I
    :goto_1
    iget-object v2, p0, Lcom/google/commerce/payments/orchestration/proto/ui/common/components/AddressFormOuterClass$AddressForm;->postalCodeOnlyCountryCode:[Ljava/lang/String;

    array-length v2, v2

    if-ge v1, v2, :cond_6

    .line 113
    iget-object v2, p0, Lcom/google/commerce/payments/orchestration/proto/ui/common/components/AddressFormOuterClass$AddressForm;->postalCodeOnlyCountryCode:[Ljava/lang/String;

    aget-object v0, v2, v1

    .line 114
    .restart local v0    # "element":Ljava/lang/String;
    if-eqz v0, :cond_5

    .line 115
    const/16 v2, 0x9

    invoke-virtual {p1, v2, v0}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeString(ILjava/lang/String;)V

    .line 112
    :cond_5
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 119
    .end local v0    # "element":Ljava/lang/String;
    .end local v1    # "i":I
    :cond_6
    iget-object v2, p0, Lcom/google/commerce/payments/orchestration/proto/ui/common/components/AddressFormOuterClass$AddressForm;->recipientLabel:Ljava/lang/String;

    const-string v3, ""

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_7

    .line 120
    const/16 v2, 0xa

    iget-object v3, p0, Lcom/google/commerce/payments/orchestration/proto/ui/common/components/AddressFormOuterClass$AddressForm;->recipientLabel:Ljava/lang/String;

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeString(ILjava/lang/String;)V

    .line 122
    :cond_7
    iget-object v2, p0, Lcom/google/commerce/payments/orchestration/proto/ui/common/components/AddressFormOuterClass$AddressForm;->hideFormFieldsToggleLabel:Ljava/lang/String;

    const-string v3, ""

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_8

    .line 123
    const/16 v2, 0xb

    iget-object v3, p0, Lcom/google/commerce/payments/orchestration/proto/ui/common/components/AddressFormOuterClass$AddressForm;->hideFormFieldsToggleLabel:Ljava/lang/String;

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeString(ILjava/lang/String;)V

    .line 125
    :cond_8
    iget-object v2, p0, Lcom/google/commerce/payments/orchestration/proto/ui/common/components/AddressFormOuterClass$AddressForm;->readOnlyField:[I

    if-eqz v2, :cond_9

    iget-object v2, p0, Lcom/google/commerce/payments/orchestration/proto/ui/common/components/AddressFormOuterClass$AddressForm;->readOnlyField:[I

    array-length v2, v2

    if-lez v2, :cond_9

    .line 126
    const/4 v1, 0x0

    .restart local v1    # "i":I
    :goto_2
    iget-object v2, p0, Lcom/google/commerce/payments/orchestration/proto/ui/common/components/AddressFormOuterClass$AddressForm;->readOnlyField:[I

    array-length v2, v2

    if-ge v1, v2, :cond_9

    .line 127
    const/16 v2, 0xc

    iget-object v3, p0, Lcom/google/commerce/payments/orchestration/proto/ui/common/components/AddressFormOuterClass$AddressForm;->readOnlyField:[I

    aget v3, v3, v1

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeInt32(II)V

    .line 126
    add-int/lit8 v1, v1, 0x1

    goto :goto_2

    .line 130
    .end local v1    # "i":I
    :cond_9
    iget-object v2, p0, Lcom/google/commerce/payments/orchestration/proto/ui/common/components/AddressFormOuterClass$AddressForm;->title:Ljava/lang/String;

    const-string v3, ""

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_a

    .line 131
    const/16 v2, 0xd

    iget-object v3, p0, Lcom/google/commerce/payments/orchestration/proto/ui/common/components/AddressFormOuterClass$AddressForm;->title:Ljava/lang/String;

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeString(ILjava/lang/String;)V

    .line 133
    :cond_a
    iget-object v2, p0, Lcom/google/commerce/payments/orchestration/proto/ui/common/components/AddressFormOuterClass$AddressForm;->hiddenField:[I

    if-eqz v2, :cond_b

    iget-object v2, p0, Lcom/google/commerce/payments/orchestration/proto/ui/common/components/AddressFormOuterClass$AddressForm;->hiddenField:[I

    array-length v2, v2

    if-lez v2, :cond_b

    .line 134
    const/4 v1, 0x0

    .restart local v1    # "i":I
    :goto_3
    iget-object v2, p0, Lcom/google/commerce/payments/orchestration/proto/ui/common/components/AddressFormOuterClass$AddressForm;->hiddenField:[I

    array-length v2, v2

    if-ge v1, v2, :cond_b

    .line 135
    const/16 v2, 0xe

    iget-object v3, p0, Lcom/google/commerce/payments/orchestration/proto/ui/common/components/AddressFormOuterClass$AddressForm;->hiddenField:[I

    aget v3, v3, v1

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeInt32(II)V

    .line 134
    add-int/lit8 v1, v1, 0x1

    goto :goto_3

    .line 138
    .end local v1    # "i":I
    :cond_b
    iget-object v2, p0, Lcom/google/commerce/payments/orchestration/proto/ui/common/components/AddressFormOuterClass$AddressForm;->initialCountryI18NDataJson:Ljava/lang/String;

    const-string v3, ""

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_c

    .line 139
    const/16 v2, 0xf

    iget-object v3, p0, Lcom/google/commerce/payments/orchestration/proto/ui/common/components/AddressFormOuterClass$AddressForm;->initialCountryI18NDataJson:Ljava/lang/String;

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeString(ILjava/lang/String;)V

    .line 141
    :cond_c
    invoke-super {p0, p1}, Lcom/google/protobuf/nano/MessageNano;->writeTo(Lcom/google/protobuf/nano/CodedOutputByteBufferNano;)V

    .line 142
    return-void
.end method
