.class public final Lcom/google/commerce/payments/orchestration/proto/common/ContextOuterClass$NativeClientContext;
.super Lcom/google/protobuf/nano/MessageNano;
.source "ContextOuterClass.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/commerce/payments/orchestration/proto/common/ContextOuterClass;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "NativeClientContext"
.end annotation


# instance fields
.field public device:Ljava/lang/String;

.field public mccMnc:Ljava/lang/String;

.field public osVersion:Ljava/lang/String;

.field public packageName:Ljava/lang/String;

.field public packageVersionCode:Ljava/lang/String;

.field public packageVersionName:Ljava/lang/String;

.field public riskData:Lpaymentfraud/mobile/DeviceFingerprinting$Parsed;

.field public screenHeightPx:I

.field public screenWidthPx:I

.field public screenXDpi:F

.field public screenYDpi:F


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 58
    invoke-direct {p0}, Lcom/google/protobuf/nano/MessageNano;-><init>()V

    .line 59
    invoke-virtual {p0}, Lcom/google/commerce/payments/orchestration/proto/common/ContextOuterClass$NativeClientContext;->clear()Lcom/google/commerce/payments/orchestration/proto/common/ContextOuterClass$NativeClientContext;

    .line 60
    return-void
.end method


# virtual methods
.method public clear()Lcom/google/commerce/payments/orchestration/proto/common/ContextOuterClass$NativeClientContext;
    .locals 3

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x0

    .line 63
    const-string v0, ""

    iput-object v0, p0, Lcom/google/commerce/payments/orchestration/proto/common/ContextOuterClass$NativeClientContext;->mccMnc:Ljava/lang/String;

    .line 64
    const-string v0, ""

    iput-object v0, p0, Lcom/google/commerce/payments/orchestration/proto/common/ContextOuterClass$NativeClientContext;->osVersion:Ljava/lang/String;

    .line 65
    const-string v0, ""

    iput-object v0, p0, Lcom/google/commerce/payments/orchestration/proto/common/ContextOuterClass$NativeClientContext;->device:Ljava/lang/String;

    .line 66
    iput v2, p0, Lcom/google/commerce/payments/orchestration/proto/common/ContextOuterClass$NativeClientContext;->screenWidthPx:I

    .line 67
    iput v2, p0, Lcom/google/commerce/payments/orchestration/proto/common/ContextOuterClass$NativeClientContext;->screenHeightPx:I

    .line 68
    iput v1, p0, Lcom/google/commerce/payments/orchestration/proto/common/ContextOuterClass$NativeClientContext;->screenXDpi:F

    .line 69
    iput v1, p0, Lcom/google/commerce/payments/orchestration/proto/common/ContextOuterClass$NativeClientContext;->screenYDpi:F

    .line 70
    const-string v0, ""

    iput-object v0, p0, Lcom/google/commerce/payments/orchestration/proto/common/ContextOuterClass$NativeClientContext;->packageName:Ljava/lang/String;

    .line 71
    const-string v0, ""

    iput-object v0, p0, Lcom/google/commerce/payments/orchestration/proto/common/ContextOuterClass$NativeClientContext;->packageVersionCode:Ljava/lang/String;

    .line 72
    const-string v0, ""

    iput-object v0, p0, Lcom/google/commerce/payments/orchestration/proto/common/ContextOuterClass$NativeClientContext;->packageVersionName:Ljava/lang/String;

    .line 73
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/commerce/payments/orchestration/proto/common/ContextOuterClass$NativeClientContext;->riskData:Lpaymentfraud/mobile/DeviceFingerprinting$Parsed;

    .line 74
    const/4 v0, -0x1

    iput v0, p0, Lcom/google/commerce/payments/orchestration/proto/common/ContextOuterClass$NativeClientContext;->cachedSize:I

    .line 75
    return-object p0
.end method

.method protected computeSerializedSize()I
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 121
    invoke-super {p0}, Lcom/google/protobuf/nano/MessageNano;->computeSerializedSize()I

    move-result v0

    .line 122
    .local v0, "size":I
    iget-object v1, p0, Lcom/google/commerce/payments/orchestration/proto/common/ContextOuterClass$NativeClientContext;->mccMnc:Ljava/lang/String;

    const-string v2, ""

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 123
    const/4 v1, 0x2

    iget-object v2, p0, Lcom/google/commerce/payments/orchestration/proto/common/ContextOuterClass$NativeClientContext;->mccMnc:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeStringSize(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 126
    :cond_0
    iget-object v1, p0, Lcom/google/commerce/payments/orchestration/proto/common/ContextOuterClass$NativeClientContext;->osVersion:Ljava/lang/String;

    const-string v2, ""

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_1

    .line 127
    const/4 v1, 0x3

    iget-object v2, p0, Lcom/google/commerce/payments/orchestration/proto/common/ContextOuterClass$NativeClientContext;->osVersion:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeStringSize(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 130
    :cond_1
    iget-object v1, p0, Lcom/google/commerce/payments/orchestration/proto/common/ContextOuterClass$NativeClientContext;->device:Ljava/lang/String;

    const-string v2, ""

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_2

    .line 131
    const/4 v1, 0x4

    iget-object v2, p0, Lcom/google/commerce/payments/orchestration/proto/common/ContextOuterClass$NativeClientContext;->device:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeStringSize(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 134
    :cond_2
    iget v1, p0, Lcom/google/commerce/payments/orchestration/proto/common/ContextOuterClass$NativeClientContext;->screenWidthPx:I

    if-eqz v1, :cond_3

    .line 135
    const/4 v1, 0x5

    iget v2, p0, Lcom/google/commerce/payments/orchestration/proto/common/ContextOuterClass$NativeClientContext;->screenWidthPx:I

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeInt32Size(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 138
    :cond_3
    iget v1, p0, Lcom/google/commerce/payments/orchestration/proto/common/ContextOuterClass$NativeClientContext;->screenHeightPx:I

    if-eqz v1, :cond_4

    .line 139
    const/4 v1, 0x6

    iget v2, p0, Lcom/google/commerce/payments/orchestration/proto/common/ContextOuterClass$NativeClientContext;->screenHeightPx:I

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeInt32Size(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 142
    :cond_4
    iget v1, p0, Lcom/google/commerce/payments/orchestration/proto/common/ContextOuterClass$NativeClientContext;->screenXDpi:F

    invoke-static {v1}, Ljava/lang/Float;->floatToIntBits(F)I

    move-result v1

    invoke-static {v3}, Ljava/lang/Float;->floatToIntBits(F)I

    move-result v2

    if-eq v1, v2, :cond_5

    .line 144
    const/4 v1, 0x7

    iget v2, p0, Lcom/google/commerce/payments/orchestration/proto/common/ContextOuterClass$NativeClientContext;->screenXDpi:F

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeFloatSize(IF)I

    move-result v1

    add-int/2addr v0, v1

    .line 147
    :cond_5
    iget v1, p0, Lcom/google/commerce/payments/orchestration/proto/common/ContextOuterClass$NativeClientContext;->screenYDpi:F

    invoke-static {v1}, Ljava/lang/Float;->floatToIntBits(F)I

    move-result v1

    invoke-static {v3}, Ljava/lang/Float;->floatToIntBits(F)I

    move-result v2

    if-eq v1, v2, :cond_6

    .line 149
    const/16 v1, 0x8

    iget v2, p0, Lcom/google/commerce/payments/orchestration/proto/common/ContextOuterClass$NativeClientContext;->screenYDpi:F

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeFloatSize(IF)I

    move-result v1

    add-int/2addr v0, v1

    .line 152
    :cond_6
    iget-object v1, p0, Lcom/google/commerce/payments/orchestration/proto/common/ContextOuterClass$NativeClientContext;->packageName:Ljava/lang/String;

    const-string v2, ""

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_7

    .line 153
    const/16 v1, 0x9

    iget-object v2, p0, Lcom/google/commerce/payments/orchestration/proto/common/ContextOuterClass$NativeClientContext;->packageName:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeStringSize(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 156
    :cond_7
    iget-object v1, p0, Lcom/google/commerce/payments/orchestration/proto/common/ContextOuterClass$NativeClientContext;->packageVersionCode:Ljava/lang/String;

    const-string v2, ""

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_8

    .line 157
    const/16 v1, 0xa

    iget-object v2, p0, Lcom/google/commerce/payments/orchestration/proto/common/ContextOuterClass$NativeClientContext;->packageVersionCode:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeStringSize(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 160
    :cond_8
    iget-object v1, p0, Lcom/google/commerce/payments/orchestration/proto/common/ContextOuterClass$NativeClientContext;->packageVersionName:Ljava/lang/String;

    const-string v2, ""

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_9

    .line 161
    const/16 v1, 0xb

    iget-object v2, p0, Lcom/google/commerce/payments/orchestration/proto/common/ContextOuterClass$NativeClientContext;->packageVersionName:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeStringSize(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 164
    :cond_9
    iget-object v1, p0, Lcom/google/commerce/payments/orchestration/proto/common/ContextOuterClass$NativeClientContext;->riskData:Lpaymentfraud/mobile/DeviceFingerprinting$Parsed;

    if-eqz v1, :cond_a

    .line 165
    const/16 v1, 0xc

    iget-object v2, p0, Lcom/google/commerce/payments/orchestration/proto/common/ContextOuterClass$NativeClientContext;->riskData:Lpaymentfraud/mobile/DeviceFingerprinting$Parsed;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeMessageSize(ILcom/google/protobuf/nano/MessageNano;)I

    move-result v1

    add-int/2addr v0, v1

    .line 168
    :cond_a
    return v0
.end method

.method public mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/commerce/payments/orchestration/proto/common/ContextOuterClass$NativeClientContext;
    .locals 2
    .param p1, "input"    # Lcom/google/protobuf/nano/CodedInputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 176
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readTag()I

    move-result v0

    .line 177
    .local v0, "tag":I
    sparse-switch v0, :sswitch_data_0

    .line 181
    invoke-static {p1, v0}, Lcom/google/protobuf/nano/WireFormatNano;->parseUnknownField(Lcom/google/protobuf/nano/CodedInputByteBufferNano;I)Z

    move-result v1

    if-nez v1, :cond_0

    .line 182
    :sswitch_0
    return-object p0

    .line 187
    :sswitch_1
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readString()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/google/commerce/payments/orchestration/proto/common/ContextOuterClass$NativeClientContext;->mccMnc:Ljava/lang/String;

    goto :goto_0

    .line 191
    :sswitch_2
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readString()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/google/commerce/payments/orchestration/proto/common/ContextOuterClass$NativeClientContext;->osVersion:Ljava/lang/String;

    goto :goto_0

    .line 195
    :sswitch_3
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readString()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/google/commerce/payments/orchestration/proto/common/ContextOuterClass$NativeClientContext;->device:Ljava/lang/String;

    goto :goto_0

    .line 199
    :sswitch_4
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readInt32()I

    move-result v1

    iput v1, p0, Lcom/google/commerce/payments/orchestration/proto/common/ContextOuterClass$NativeClientContext;->screenWidthPx:I

    goto :goto_0

    .line 203
    :sswitch_5
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readInt32()I

    move-result v1

    iput v1, p0, Lcom/google/commerce/payments/orchestration/proto/common/ContextOuterClass$NativeClientContext;->screenHeightPx:I

    goto :goto_0

    .line 207
    :sswitch_6
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readFloat()F

    move-result v1

    iput v1, p0, Lcom/google/commerce/payments/orchestration/proto/common/ContextOuterClass$NativeClientContext;->screenXDpi:F

    goto :goto_0

    .line 211
    :sswitch_7
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readFloat()F

    move-result v1

    iput v1, p0, Lcom/google/commerce/payments/orchestration/proto/common/ContextOuterClass$NativeClientContext;->screenYDpi:F

    goto :goto_0

    .line 215
    :sswitch_8
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readString()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/google/commerce/payments/orchestration/proto/common/ContextOuterClass$NativeClientContext;->packageName:Ljava/lang/String;

    goto :goto_0

    .line 219
    :sswitch_9
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readString()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/google/commerce/payments/orchestration/proto/common/ContextOuterClass$NativeClientContext;->packageVersionCode:Ljava/lang/String;

    goto :goto_0

    .line 223
    :sswitch_a
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readString()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/google/commerce/payments/orchestration/proto/common/ContextOuterClass$NativeClientContext;->packageVersionName:Ljava/lang/String;

    goto :goto_0

    .line 227
    :sswitch_b
    iget-object v1, p0, Lcom/google/commerce/payments/orchestration/proto/common/ContextOuterClass$NativeClientContext;->riskData:Lpaymentfraud/mobile/DeviceFingerprinting$Parsed;

    if-nez v1, :cond_1

    .line 228
    new-instance v1, Lpaymentfraud/mobile/DeviceFingerprinting$Parsed;

    invoke-direct {v1}, Lpaymentfraud/mobile/DeviceFingerprinting$Parsed;-><init>()V

    iput-object v1, p0, Lcom/google/commerce/payments/orchestration/proto/common/ContextOuterClass$NativeClientContext;->riskData:Lpaymentfraud/mobile/DeviceFingerprinting$Parsed;

    .line 230
    :cond_1
    iget-object v1, p0, Lcom/google/commerce/payments/orchestration/proto/common/ContextOuterClass$NativeClientContext;->riskData:Lpaymentfraud/mobile/DeviceFingerprinting$Parsed;

    invoke-virtual {p1, v1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readMessage(Lcom/google/protobuf/nano/MessageNano;)V

    goto :goto_0

    .line 177
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x12 -> :sswitch_1
        0x1a -> :sswitch_2
        0x22 -> :sswitch_3
        0x28 -> :sswitch_4
        0x30 -> :sswitch_5
        0x3d -> :sswitch_6
        0x45 -> :sswitch_7
        0x4a -> :sswitch_8
        0x52 -> :sswitch_9
        0x5a -> :sswitch_a
        0x62 -> :sswitch_b
    .end sparse-switch
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/protobuf/nano/MessageNano;
    .locals 1
    .param p1, "x0"    # Lcom/google/protobuf/nano/CodedInputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 8
    invoke-virtual {p0, p1}, Lcom/google/commerce/payments/orchestration/proto/common/ContextOuterClass$NativeClientContext;->mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/commerce/payments/orchestration/proto/common/ContextOuterClass$NativeClientContext;

    move-result-object v0

    return-object v0
.end method

.method public writeTo(Lcom/google/protobuf/nano/CodedOutputByteBufferNano;)V
    .locals 3
    .param p1, "output"    # Lcom/google/protobuf/nano/CodedOutputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v2, 0x0

    .line 81
    iget-object v0, p0, Lcom/google/commerce/payments/orchestration/proto/common/ContextOuterClass$NativeClientContext;->mccMnc:Ljava/lang/String;

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 82
    const/4 v0, 0x2

    iget-object v1, p0, Lcom/google/commerce/payments/orchestration/proto/common/ContextOuterClass$NativeClientContext;->mccMnc:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeString(ILjava/lang/String;)V

    .line 84
    :cond_0
    iget-object v0, p0, Lcom/google/commerce/payments/orchestration/proto/common/ContextOuterClass$NativeClientContext;->osVersion:Ljava/lang/String;

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 85
    const/4 v0, 0x3

    iget-object v1, p0, Lcom/google/commerce/payments/orchestration/proto/common/ContextOuterClass$NativeClientContext;->osVersion:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeString(ILjava/lang/String;)V

    .line 87
    :cond_1
    iget-object v0, p0, Lcom/google/commerce/payments/orchestration/proto/common/ContextOuterClass$NativeClientContext;->device:Ljava/lang/String;

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_2

    .line 88
    const/4 v0, 0x4

    iget-object v1, p0, Lcom/google/commerce/payments/orchestration/proto/common/ContextOuterClass$NativeClientContext;->device:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeString(ILjava/lang/String;)V

    .line 90
    :cond_2
    iget v0, p0, Lcom/google/commerce/payments/orchestration/proto/common/ContextOuterClass$NativeClientContext;->screenWidthPx:I

    if-eqz v0, :cond_3

    .line 91
    const/4 v0, 0x5

    iget v1, p0, Lcom/google/commerce/payments/orchestration/proto/common/ContextOuterClass$NativeClientContext;->screenWidthPx:I

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeInt32(II)V

    .line 93
    :cond_3
    iget v0, p0, Lcom/google/commerce/payments/orchestration/proto/common/ContextOuterClass$NativeClientContext;->screenHeightPx:I

    if-eqz v0, :cond_4

    .line 94
    const/4 v0, 0x6

    iget v1, p0, Lcom/google/commerce/payments/orchestration/proto/common/ContextOuterClass$NativeClientContext;->screenHeightPx:I

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeInt32(II)V

    .line 96
    :cond_4
    iget v0, p0, Lcom/google/commerce/payments/orchestration/proto/common/ContextOuterClass$NativeClientContext;->screenXDpi:F

    invoke-static {v0}, Ljava/lang/Float;->floatToIntBits(F)I

    move-result v0

    invoke-static {v2}, Ljava/lang/Float;->floatToIntBits(F)I

    move-result v1

    if-eq v0, v1, :cond_5

    .line 98
    const/4 v0, 0x7

    iget v1, p0, Lcom/google/commerce/payments/orchestration/proto/common/ContextOuterClass$NativeClientContext;->screenXDpi:F

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeFloat(IF)V

    .line 100
    :cond_5
    iget v0, p0, Lcom/google/commerce/payments/orchestration/proto/common/ContextOuterClass$NativeClientContext;->screenYDpi:F

    invoke-static {v0}, Ljava/lang/Float;->floatToIntBits(F)I

    move-result v0

    invoke-static {v2}, Ljava/lang/Float;->floatToIntBits(F)I

    move-result v1

    if-eq v0, v1, :cond_6

    .line 102
    const/16 v0, 0x8

    iget v1, p0, Lcom/google/commerce/payments/orchestration/proto/common/ContextOuterClass$NativeClientContext;->screenYDpi:F

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeFloat(IF)V

    .line 104
    :cond_6
    iget-object v0, p0, Lcom/google/commerce/payments/orchestration/proto/common/ContextOuterClass$NativeClientContext;->packageName:Ljava/lang/String;

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_7

    .line 105
    const/16 v0, 0x9

    iget-object v1, p0, Lcom/google/commerce/payments/orchestration/proto/common/ContextOuterClass$NativeClientContext;->packageName:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeString(ILjava/lang/String;)V

    .line 107
    :cond_7
    iget-object v0, p0, Lcom/google/commerce/payments/orchestration/proto/common/ContextOuterClass$NativeClientContext;->packageVersionCode:Ljava/lang/String;

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_8

    .line 108
    const/16 v0, 0xa

    iget-object v1, p0, Lcom/google/commerce/payments/orchestration/proto/common/ContextOuterClass$NativeClientContext;->packageVersionCode:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeString(ILjava/lang/String;)V

    .line 110
    :cond_8
    iget-object v0, p0, Lcom/google/commerce/payments/orchestration/proto/common/ContextOuterClass$NativeClientContext;->packageVersionName:Ljava/lang/String;

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_9

    .line 111
    const/16 v0, 0xb

    iget-object v1, p0, Lcom/google/commerce/payments/orchestration/proto/common/ContextOuterClass$NativeClientContext;->packageVersionName:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeString(ILjava/lang/String;)V

    .line 113
    :cond_9
    iget-object v0, p0, Lcom/google/commerce/payments/orchestration/proto/common/ContextOuterClass$NativeClientContext;->riskData:Lpaymentfraud/mobile/DeviceFingerprinting$Parsed;

    if-eqz v0, :cond_a

    .line 114
    const/16 v0, 0xc

    iget-object v1, p0, Lcom/google/commerce/payments/orchestration/proto/common/ContextOuterClass$NativeClientContext;->riskData:Lpaymentfraud/mobile/DeviceFingerprinting$Parsed;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeMessage(ILcom/google/protobuf/nano/MessageNano;)V

    .line 116
    :cond_a
    invoke-super {p0, p1}, Lcom/google/protobuf/nano/MessageNano;->writeTo(Lcom/google/protobuf/nano/CodedOutputByteBufferNano;)V

    .line 117
    return-void
.end method
