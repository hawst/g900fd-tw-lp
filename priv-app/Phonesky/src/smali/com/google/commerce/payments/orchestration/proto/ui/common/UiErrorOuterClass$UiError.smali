.class public final Lcom/google/commerce/payments/orchestration/proto/ui/common/UiErrorOuterClass$UiError;
.super Lcom/google/protobuf/nano/MessageNano;
.source "UiErrorOuterClass.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/commerce/payments/orchestration/proto/ui/common/UiErrorOuterClass;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "UiError"
.end annotation


# instance fields
.field public action:I

.field public errorCode:Ljava/lang/String;

.field public formFieldMessage:[Lcom/google/commerce/payments/orchestration/proto/ui/common/UiErrorOuterClass$FormFieldMessage;

.field public internalDetails:Ljava/lang/String;

.field public message:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 44
    invoke-direct {p0}, Lcom/google/protobuf/nano/MessageNano;-><init>()V

    .line 45
    invoke-virtual {p0}, Lcom/google/commerce/payments/orchestration/proto/ui/common/UiErrorOuterClass$UiError;->clear()Lcom/google/commerce/payments/orchestration/proto/ui/common/UiErrorOuterClass$UiError;

    .line 46
    return-void
.end method


# virtual methods
.method public clear()Lcom/google/commerce/payments/orchestration/proto/ui/common/UiErrorOuterClass$UiError;
    .locals 1

    .prologue
    .line 49
    const-string v0, ""

    iput-object v0, p0, Lcom/google/commerce/payments/orchestration/proto/ui/common/UiErrorOuterClass$UiError;->message:Ljava/lang/String;

    .line 50
    const/4 v0, 0x1

    iput v0, p0, Lcom/google/commerce/payments/orchestration/proto/ui/common/UiErrorOuterClass$UiError;->action:I

    .line 51
    invoke-static {}, Lcom/google/commerce/payments/orchestration/proto/ui/common/UiErrorOuterClass$FormFieldMessage;->emptyArray()[Lcom/google/commerce/payments/orchestration/proto/ui/common/UiErrorOuterClass$FormFieldMessage;

    move-result-object v0

    iput-object v0, p0, Lcom/google/commerce/payments/orchestration/proto/ui/common/UiErrorOuterClass$UiError;->formFieldMessage:[Lcom/google/commerce/payments/orchestration/proto/ui/common/UiErrorOuterClass$FormFieldMessage;

    .line 52
    const-string v0, ""

    iput-object v0, p0, Lcom/google/commerce/payments/orchestration/proto/ui/common/UiErrorOuterClass$UiError;->errorCode:Ljava/lang/String;

    .line 53
    const-string v0, ""

    iput-object v0, p0, Lcom/google/commerce/payments/orchestration/proto/ui/common/UiErrorOuterClass$UiError;->internalDetails:Ljava/lang/String;

    .line 54
    const/4 v0, -0x1

    iput v0, p0, Lcom/google/commerce/payments/orchestration/proto/ui/common/UiErrorOuterClass$UiError;->cachedSize:I

    .line 55
    return-object p0
.end method

.method protected computeSerializedSize()I
    .locals 6

    .prologue
    const/4 v5, 0x1

    .line 86
    invoke-super {p0}, Lcom/google/protobuf/nano/MessageNano;->computeSerializedSize()I

    move-result v2

    .line 87
    .local v2, "size":I
    iget-object v3, p0, Lcom/google/commerce/payments/orchestration/proto/ui/common/UiErrorOuterClass$UiError;->message:Ljava/lang/String;

    const-string v4, ""

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_0

    .line 88
    iget-object v3, p0, Lcom/google/commerce/payments/orchestration/proto/ui/common/UiErrorOuterClass$UiError;->message:Ljava/lang/String;

    invoke-static {v5, v3}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeStringSize(ILjava/lang/String;)I

    move-result v3

    add-int/2addr v2, v3

    .line 91
    :cond_0
    iget-object v3, p0, Lcom/google/commerce/payments/orchestration/proto/ui/common/UiErrorOuterClass$UiError;->formFieldMessage:[Lcom/google/commerce/payments/orchestration/proto/ui/common/UiErrorOuterClass$FormFieldMessage;

    if-eqz v3, :cond_2

    iget-object v3, p0, Lcom/google/commerce/payments/orchestration/proto/ui/common/UiErrorOuterClass$UiError;->formFieldMessage:[Lcom/google/commerce/payments/orchestration/proto/ui/common/UiErrorOuterClass$FormFieldMessage;

    array-length v3, v3

    if-lez v3, :cond_2

    .line 92
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    iget-object v3, p0, Lcom/google/commerce/payments/orchestration/proto/ui/common/UiErrorOuterClass$UiError;->formFieldMessage:[Lcom/google/commerce/payments/orchestration/proto/ui/common/UiErrorOuterClass$FormFieldMessage;

    array-length v3, v3

    if-ge v1, v3, :cond_2

    .line 93
    iget-object v3, p0, Lcom/google/commerce/payments/orchestration/proto/ui/common/UiErrorOuterClass$UiError;->formFieldMessage:[Lcom/google/commerce/payments/orchestration/proto/ui/common/UiErrorOuterClass$FormFieldMessage;

    aget-object v0, v3, v1

    .line 94
    .local v0, "element":Lcom/google/commerce/payments/orchestration/proto/ui/common/UiErrorOuterClass$FormFieldMessage;
    if-eqz v0, :cond_1

    .line 95
    const/4 v3, 0x2

    invoke-static {v3, v0}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeMessageSize(ILcom/google/protobuf/nano/MessageNano;)I

    move-result v3

    add-int/2addr v2, v3

    .line 92
    :cond_1
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 100
    .end local v0    # "element":Lcom/google/commerce/payments/orchestration/proto/ui/common/UiErrorOuterClass$FormFieldMessage;
    .end local v1    # "i":I
    :cond_2
    iget-object v3, p0, Lcom/google/commerce/payments/orchestration/proto/ui/common/UiErrorOuterClass$UiError;->errorCode:Ljava/lang/String;

    const-string v4, ""

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_3

    .line 101
    const/4 v3, 0x3

    iget-object v4, p0, Lcom/google/commerce/payments/orchestration/proto/ui/common/UiErrorOuterClass$UiError;->errorCode:Ljava/lang/String;

    invoke-static {v3, v4}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeStringSize(ILjava/lang/String;)I

    move-result v3

    add-int/2addr v2, v3

    .line 104
    :cond_3
    iget-object v3, p0, Lcom/google/commerce/payments/orchestration/proto/ui/common/UiErrorOuterClass$UiError;->internalDetails:Ljava/lang/String;

    const-string v4, ""

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_4

    .line 105
    const/4 v3, 0x4

    iget-object v4, p0, Lcom/google/commerce/payments/orchestration/proto/ui/common/UiErrorOuterClass$UiError;->internalDetails:Ljava/lang/String;

    invoke-static {v3, v4}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeStringSize(ILjava/lang/String;)I

    move-result v3

    add-int/2addr v2, v3

    .line 108
    :cond_4
    iget v3, p0, Lcom/google/commerce/payments/orchestration/proto/ui/common/UiErrorOuterClass$UiError;->action:I

    if-eq v3, v5, :cond_5

    .line 109
    const/4 v3, 0x5

    iget v4, p0, Lcom/google/commerce/payments/orchestration/proto/ui/common/UiErrorOuterClass$UiError;->action:I

    invoke-static {v3, v4}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeInt32Size(II)I

    move-result v3

    add-int/2addr v2, v3

    .line 112
    :cond_5
    return v2
.end method

.method public mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/commerce/payments/orchestration/proto/ui/common/UiErrorOuterClass$UiError;
    .locals 7
    .param p1, "input"    # Lcom/google/protobuf/nano/CodedInputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v5, 0x0

    .line 120
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readTag()I

    move-result v3

    .line 121
    .local v3, "tag":I
    sparse-switch v3, :sswitch_data_0

    .line 125
    invoke-static {p1, v3}, Lcom/google/protobuf/nano/WireFormatNano;->parseUnknownField(Lcom/google/protobuf/nano/CodedInputByteBufferNano;I)Z

    move-result v6

    if-nez v6, :cond_0

    .line 126
    :sswitch_0
    return-object p0

    .line 131
    :sswitch_1
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readString()Ljava/lang/String;

    move-result-object v6

    iput-object v6, p0, Lcom/google/commerce/payments/orchestration/proto/ui/common/UiErrorOuterClass$UiError;->message:Ljava/lang/String;

    goto :goto_0

    .line 135
    :sswitch_2
    const/16 v6, 0x12

    invoke-static {p1, v6}, Lcom/google/protobuf/nano/WireFormatNano;->getRepeatedFieldArrayLength(Lcom/google/protobuf/nano/CodedInputByteBufferNano;I)I

    move-result v0

    .line 137
    .local v0, "arrayLength":I
    iget-object v6, p0, Lcom/google/commerce/payments/orchestration/proto/ui/common/UiErrorOuterClass$UiError;->formFieldMessage:[Lcom/google/commerce/payments/orchestration/proto/ui/common/UiErrorOuterClass$FormFieldMessage;

    if-nez v6, :cond_2

    move v1, v5

    .line 138
    .local v1, "i":I
    :goto_1
    add-int v6, v1, v0

    new-array v2, v6, [Lcom/google/commerce/payments/orchestration/proto/ui/common/UiErrorOuterClass$FormFieldMessage;

    .line 140
    .local v2, "newArray":[Lcom/google/commerce/payments/orchestration/proto/ui/common/UiErrorOuterClass$FormFieldMessage;
    if-eqz v1, :cond_1

    .line 141
    iget-object v6, p0, Lcom/google/commerce/payments/orchestration/proto/ui/common/UiErrorOuterClass$UiError;->formFieldMessage:[Lcom/google/commerce/payments/orchestration/proto/ui/common/UiErrorOuterClass$FormFieldMessage;

    invoke-static {v6, v5, v2, v5, v1}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 143
    :cond_1
    :goto_2
    array-length v6, v2

    add-int/lit8 v6, v6, -0x1

    if-ge v1, v6, :cond_3

    .line 144
    new-instance v6, Lcom/google/commerce/payments/orchestration/proto/ui/common/UiErrorOuterClass$FormFieldMessage;

    invoke-direct {v6}, Lcom/google/commerce/payments/orchestration/proto/ui/common/UiErrorOuterClass$FormFieldMessage;-><init>()V

    aput-object v6, v2, v1

    .line 145
    aget-object v6, v2, v1

    invoke-virtual {p1, v6}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readMessage(Lcom/google/protobuf/nano/MessageNano;)V

    .line 146
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readTag()I

    .line 143
    add-int/lit8 v1, v1, 0x1

    goto :goto_2

    .line 137
    .end local v1    # "i":I
    .end local v2    # "newArray":[Lcom/google/commerce/payments/orchestration/proto/ui/common/UiErrorOuterClass$FormFieldMessage;
    :cond_2
    iget-object v6, p0, Lcom/google/commerce/payments/orchestration/proto/ui/common/UiErrorOuterClass$UiError;->formFieldMessage:[Lcom/google/commerce/payments/orchestration/proto/ui/common/UiErrorOuterClass$FormFieldMessage;

    array-length v1, v6

    goto :goto_1

    .line 149
    .restart local v1    # "i":I
    .restart local v2    # "newArray":[Lcom/google/commerce/payments/orchestration/proto/ui/common/UiErrorOuterClass$FormFieldMessage;
    :cond_3
    new-instance v6, Lcom/google/commerce/payments/orchestration/proto/ui/common/UiErrorOuterClass$FormFieldMessage;

    invoke-direct {v6}, Lcom/google/commerce/payments/orchestration/proto/ui/common/UiErrorOuterClass$FormFieldMessage;-><init>()V

    aput-object v6, v2, v1

    .line 150
    aget-object v6, v2, v1

    invoke-virtual {p1, v6}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readMessage(Lcom/google/protobuf/nano/MessageNano;)V

    .line 151
    iput-object v2, p0, Lcom/google/commerce/payments/orchestration/proto/ui/common/UiErrorOuterClass$UiError;->formFieldMessage:[Lcom/google/commerce/payments/orchestration/proto/ui/common/UiErrorOuterClass$FormFieldMessage;

    goto :goto_0

    .line 155
    .end local v0    # "arrayLength":I
    .end local v1    # "i":I
    .end local v2    # "newArray":[Lcom/google/commerce/payments/orchestration/proto/ui/common/UiErrorOuterClass$FormFieldMessage;
    :sswitch_3
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readString()Ljava/lang/String;

    move-result-object v6

    iput-object v6, p0, Lcom/google/commerce/payments/orchestration/proto/ui/common/UiErrorOuterClass$UiError;->errorCode:Ljava/lang/String;

    goto :goto_0

    .line 159
    :sswitch_4
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readString()Ljava/lang/String;

    move-result-object v6

    iput-object v6, p0, Lcom/google/commerce/payments/orchestration/proto/ui/common/UiErrorOuterClass$UiError;->internalDetails:Ljava/lang/String;

    goto :goto_0

    .line 163
    :sswitch_5
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readInt32()I

    move-result v4

    .line 164
    .local v4, "value":I
    packed-switch v4, :pswitch_data_0

    goto :goto_0

    .line 167
    :pswitch_0
    iput v4, p0, Lcom/google/commerce/payments/orchestration/proto/ui/common/UiErrorOuterClass$UiError;->action:I

    goto :goto_0

    .line 121
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
        0x22 -> :sswitch_4
        0x28 -> :sswitch_5
    .end sparse-switch

    .line 164
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_0
    .end packed-switch
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/protobuf/nano/MessageNano;
    .locals 1
    .param p1, "x0"    # Lcom/google/protobuf/nano/CodedInputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 8
    invoke-virtual {p0, p1}, Lcom/google/commerce/payments/orchestration/proto/ui/common/UiErrorOuterClass$UiError;->mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/commerce/payments/orchestration/proto/ui/common/UiErrorOuterClass$UiError;

    move-result-object v0

    return-object v0
.end method

.method public writeTo(Lcom/google/protobuf/nano/CodedOutputByteBufferNano;)V
    .locals 5
    .param p1, "output"    # Lcom/google/protobuf/nano/CodedOutputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v4, 0x1

    .line 61
    iget-object v2, p0, Lcom/google/commerce/payments/orchestration/proto/ui/common/UiErrorOuterClass$UiError;->message:Ljava/lang/String;

    const-string v3, ""

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 62
    iget-object v2, p0, Lcom/google/commerce/payments/orchestration/proto/ui/common/UiErrorOuterClass$UiError;->message:Ljava/lang/String;

    invoke-virtual {p1, v4, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeString(ILjava/lang/String;)V

    .line 64
    :cond_0
    iget-object v2, p0, Lcom/google/commerce/payments/orchestration/proto/ui/common/UiErrorOuterClass$UiError;->formFieldMessage:[Lcom/google/commerce/payments/orchestration/proto/ui/common/UiErrorOuterClass$FormFieldMessage;

    if-eqz v2, :cond_2

    iget-object v2, p0, Lcom/google/commerce/payments/orchestration/proto/ui/common/UiErrorOuterClass$UiError;->formFieldMessage:[Lcom/google/commerce/payments/orchestration/proto/ui/common/UiErrorOuterClass$FormFieldMessage;

    array-length v2, v2

    if-lez v2, :cond_2

    .line 65
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    iget-object v2, p0, Lcom/google/commerce/payments/orchestration/proto/ui/common/UiErrorOuterClass$UiError;->formFieldMessage:[Lcom/google/commerce/payments/orchestration/proto/ui/common/UiErrorOuterClass$FormFieldMessage;

    array-length v2, v2

    if-ge v1, v2, :cond_2

    .line 66
    iget-object v2, p0, Lcom/google/commerce/payments/orchestration/proto/ui/common/UiErrorOuterClass$UiError;->formFieldMessage:[Lcom/google/commerce/payments/orchestration/proto/ui/common/UiErrorOuterClass$FormFieldMessage;

    aget-object v0, v2, v1

    .line 67
    .local v0, "element":Lcom/google/commerce/payments/orchestration/proto/ui/common/UiErrorOuterClass$FormFieldMessage;
    if-eqz v0, :cond_1

    .line 68
    const/4 v2, 0x2

    invoke-virtual {p1, v2, v0}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeMessage(ILcom/google/protobuf/nano/MessageNano;)V

    .line 65
    :cond_1
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 72
    .end local v0    # "element":Lcom/google/commerce/payments/orchestration/proto/ui/common/UiErrorOuterClass$FormFieldMessage;
    .end local v1    # "i":I
    :cond_2
    iget-object v2, p0, Lcom/google/commerce/payments/orchestration/proto/ui/common/UiErrorOuterClass$UiError;->errorCode:Ljava/lang/String;

    const-string v3, ""

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_3

    .line 73
    const/4 v2, 0x3

    iget-object v3, p0, Lcom/google/commerce/payments/orchestration/proto/ui/common/UiErrorOuterClass$UiError;->errorCode:Ljava/lang/String;

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeString(ILjava/lang/String;)V

    .line 75
    :cond_3
    iget-object v2, p0, Lcom/google/commerce/payments/orchestration/proto/ui/common/UiErrorOuterClass$UiError;->internalDetails:Ljava/lang/String;

    const-string v3, ""

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_4

    .line 76
    const/4 v2, 0x4

    iget-object v3, p0, Lcom/google/commerce/payments/orchestration/proto/ui/common/UiErrorOuterClass$UiError;->internalDetails:Ljava/lang/String;

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeString(ILjava/lang/String;)V

    .line 78
    :cond_4
    iget v2, p0, Lcom/google/commerce/payments/orchestration/proto/ui/common/UiErrorOuterClass$UiError;->action:I

    if-eq v2, v4, :cond_5

    .line 79
    const/4 v2, 0x5

    iget v3, p0, Lcom/google/commerce/payments/orchestration/proto/ui/common/UiErrorOuterClass$UiError;->action:I

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeInt32(II)V

    .line 81
    :cond_5
    invoke-super {p0, p1}, Lcom/google/protobuf/nano/MessageNano;->writeTo(Lcom/google/protobuf/nano/CodedOutputByteBufferNano;)V

    .line 82
    return-void
.end method
