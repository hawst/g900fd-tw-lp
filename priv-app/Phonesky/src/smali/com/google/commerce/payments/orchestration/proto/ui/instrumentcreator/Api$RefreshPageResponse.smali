.class public final Lcom/google/commerce/payments/orchestration/proto/ui/instrumentcreator/Api$RefreshPageResponse;
.super Lcom/google/protobuf/nano/MessageNano;
.source "Api.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/commerce/payments/orchestration/proto/ui/instrumentcreator/Api;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "RefreshPageResponse"
.end annotation


# instance fields
.field public context:Lcom/google/commerce/payments/orchestration/proto/ui/common/ResponseContextOuterClass$ResponseContext;

.field public error:Lcom/google/commerce/payments/orchestration/proto/ui/common/UiErrorOuterClass$UiError;

.field public nextPage:Lcom/google/commerce/payments/orchestration/proto/ui/instrumentcreator/Api$Page;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 853
    invoke-direct {p0}, Lcom/google/protobuf/nano/MessageNano;-><init>()V

    .line 854
    invoke-virtual {p0}, Lcom/google/commerce/payments/orchestration/proto/ui/instrumentcreator/Api$RefreshPageResponse;->clear()Lcom/google/commerce/payments/orchestration/proto/ui/instrumentcreator/Api$RefreshPageResponse;

    .line 855
    return-void
.end method


# virtual methods
.method public clear()Lcom/google/commerce/payments/orchestration/proto/ui/instrumentcreator/Api$RefreshPageResponse;
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 858
    iput-object v0, p0, Lcom/google/commerce/payments/orchestration/proto/ui/instrumentcreator/Api$RefreshPageResponse;->error:Lcom/google/commerce/payments/orchestration/proto/ui/common/UiErrorOuterClass$UiError;

    .line 859
    iput-object v0, p0, Lcom/google/commerce/payments/orchestration/proto/ui/instrumentcreator/Api$RefreshPageResponse;->context:Lcom/google/commerce/payments/orchestration/proto/ui/common/ResponseContextOuterClass$ResponseContext;

    .line 860
    iput-object v0, p0, Lcom/google/commerce/payments/orchestration/proto/ui/instrumentcreator/Api$RefreshPageResponse;->nextPage:Lcom/google/commerce/payments/orchestration/proto/ui/instrumentcreator/Api$Page;

    .line 861
    const/4 v0, -0x1

    iput v0, p0, Lcom/google/commerce/payments/orchestration/proto/ui/instrumentcreator/Api$RefreshPageResponse;->cachedSize:I

    .line 862
    return-object p0
.end method

.method protected computeSerializedSize()I
    .locals 3

    .prologue
    .line 882
    invoke-super {p0}, Lcom/google/protobuf/nano/MessageNano;->computeSerializedSize()I

    move-result v0

    .line 883
    .local v0, "size":I
    iget-object v1, p0, Lcom/google/commerce/payments/orchestration/proto/ui/instrumentcreator/Api$RefreshPageResponse;->error:Lcom/google/commerce/payments/orchestration/proto/ui/common/UiErrorOuterClass$UiError;

    if-eqz v1, :cond_0

    .line 884
    const/4 v1, 0x1

    iget-object v2, p0, Lcom/google/commerce/payments/orchestration/proto/ui/instrumentcreator/Api$RefreshPageResponse;->error:Lcom/google/commerce/payments/orchestration/proto/ui/common/UiErrorOuterClass$UiError;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeMessageSize(ILcom/google/protobuf/nano/MessageNano;)I

    move-result v1

    add-int/2addr v0, v1

    .line 887
    :cond_0
    iget-object v1, p0, Lcom/google/commerce/payments/orchestration/proto/ui/instrumentcreator/Api$RefreshPageResponse;->context:Lcom/google/commerce/payments/orchestration/proto/ui/common/ResponseContextOuterClass$ResponseContext;

    if-eqz v1, :cond_1

    .line 888
    const/4 v1, 0x2

    iget-object v2, p0, Lcom/google/commerce/payments/orchestration/proto/ui/instrumentcreator/Api$RefreshPageResponse;->context:Lcom/google/commerce/payments/orchestration/proto/ui/common/ResponseContextOuterClass$ResponseContext;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeMessageSize(ILcom/google/protobuf/nano/MessageNano;)I

    move-result v1

    add-int/2addr v0, v1

    .line 891
    :cond_1
    iget-object v1, p0, Lcom/google/commerce/payments/orchestration/proto/ui/instrumentcreator/Api$RefreshPageResponse;->nextPage:Lcom/google/commerce/payments/orchestration/proto/ui/instrumentcreator/Api$Page;

    if-eqz v1, :cond_2

    .line 892
    const/4 v1, 0x3

    iget-object v2, p0, Lcom/google/commerce/payments/orchestration/proto/ui/instrumentcreator/Api$RefreshPageResponse;->nextPage:Lcom/google/commerce/payments/orchestration/proto/ui/instrumentcreator/Api$Page;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeMessageSize(ILcom/google/protobuf/nano/MessageNano;)I

    move-result v1

    add-int/2addr v0, v1

    .line 895
    :cond_2
    return v0
.end method

.method public mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/commerce/payments/orchestration/proto/ui/instrumentcreator/Api$RefreshPageResponse;
    .locals 2
    .param p1, "input"    # Lcom/google/protobuf/nano/CodedInputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 903
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readTag()I

    move-result v0

    .line 904
    .local v0, "tag":I
    sparse-switch v0, :sswitch_data_0

    .line 908
    invoke-static {p1, v0}, Lcom/google/protobuf/nano/WireFormatNano;->parseUnknownField(Lcom/google/protobuf/nano/CodedInputByteBufferNano;I)Z

    move-result v1

    if-nez v1, :cond_0

    .line 909
    :sswitch_0
    return-object p0

    .line 914
    :sswitch_1
    iget-object v1, p0, Lcom/google/commerce/payments/orchestration/proto/ui/instrumentcreator/Api$RefreshPageResponse;->error:Lcom/google/commerce/payments/orchestration/proto/ui/common/UiErrorOuterClass$UiError;

    if-nez v1, :cond_1

    .line 915
    new-instance v1, Lcom/google/commerce/payments/orchestration/proto/ui/common/UiErrorOuterClass$UiError;

    invoke-direct {v1}, Lcom/google/commerce/payments/orchestration/proto/ui/common/UiErrorOuterClass$UiError;-><init>()V

    iput-object v1, p0, Lcom/google/commerce/payments/orchestration/proto/ui/instrumentcreator/Api$RefreshPageResponse;->error:Lcom/google/commerce/payments/orchestration/proto/ui/common/UiErrorOuterClass$UiError;

    .line 917
    :cond_1
    iget-object v1, p0, Lcom/google/commerce/payments/orchestration/proto/ui/instrumentcreator/Api$RefreshPageResponse;->error:Lcom/google/commerce/payments/orchestration/proto/ui/common/UiErrorOuterClass$UiError;

    invoke-virtual {p1, v1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readMessage(Lcom/google/protobuf/nano/MessageNano;)V

    goto :goto_0

    .line 921
    :sswitch_2
    iget-object v1, p0, Lcom/google/commerce/payments/orchestration/proto/ui/instrumentcreator/Api$RefreshPageResponse;->context:Lcom/google/commerce/payments/orchestration/proto/ui/common/ResponseContextOuterClass$ResponseContext;

    if-nez v1, :cond_2

    .line 922
    new-instance v1, Lcom/google/commerce/payments/orchestration/proto/ui/common/ResponseContextOuterClass$ResponseContext;

    invoke-direct {v1}, Lcom/google/commerce/payments/orchestration/proto/ui/common/ResponseContextOuterClass$ResponseContext;-><init>()V

    iput-object v1, p0, Lcom/google/commerce/payments/orchestration/proto/ui/instrumentcreator/Api$RefreshPageResponse;->context:Lcom/google/commerce/payments/orchestration/proto/ui/common/ResponseContextOuterClass$ResponseContext;

    .line 924
    :cond_2
    iget-object v1, p0, Lcom/google/commerce/payments/orchestration/proto/ui/instrumentcreator/Api$RefreshPageResponse;->context:Lcom/google/commerce/payments/orchestration/proto/ui/common/ResponseContextOuterClass$ResponseContext;

    invoke-virtual {p1, v1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readMessage(Lcom/google/protobuf/nano/MessageNano;)V

    goto :goto_0

    .line 928
    :sswitch_3
    iget-object v1, p0, Lcom/google/commerce/payments/orchestration/proto/ui/instrumentcreator/Api$RefreshPageResponse;->nextPage:Lcom/google/commerce/payments/orchestration/proto/ui/instrumentcreator/Api$Page;

    if-nez v1, :cond_3

    .line 929
    new-instance v1, Lcom/google/commerce/payments/orchestration/proto/ui/instrumentcreator/Api$Page;

    invoke-direct {v1}, Lcom/google/commerce/payments/orchestration/proto/ui/instrumentcreator/Api$Page;-><init>()V

    iput-object v1, p0, Lcom/google/commerce/payments/orchestration/proto/ui/instrumentcreator/Api$RefreshPageResponse;->nextPage:Lcom/google/commerce/payments/orchestration/proto/ui/instrumentcreator/Api$Page;

    .line 931
    :cond_3
    iget-object v1, p0, Lcom/google/commerce/payments/orchestration/proto/ui/instrumentcreator/Api$RefreshPageResponse;->nextPage:Lcom/google/commerce/payments/orchestration/proto/ui/instrumentcreator/Api$Page;

    invoke-virtual {p1, v1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readMessage(Lcom/google/protobuf/nano/MessageNano;)V

    goto :goto_0

    .line 904
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
    .end sparse-switch
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/protobuf/nano/MessageNano;
    .locals 1
    .param p1, "x0"    # Lcom/google/protobuf/nano/CodedInputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 827
    invoke-virtual {p0, p1}, Lcom/google/commerce/payments/orchestration/proto/ui/instrumentcreator/Api$RefreshPageResponse;->mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/commerce/payments/orchestration/proto/ui/instrumentcreator/Api$RefreshPageResponse;

    move-result-object v0

    return-object v0
.end method

.method public writeTo(Lcom/google/protobuf/nano/CodedOutputByteBufferNano;)V
    .locals 2
    .param p1, "output"    # Lcom/google/protobuf/nano/CodedOutputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 868
    iget-object v0, p0, Lcom/google/commerce/payments/orchestration/proto/ui/instrumentcreator/Api$RefreshPageResponse;->error:Lcom/google/commerce/payments/orchestration/proto/ui/common/UiErrorOuterClass$UiError;

    if-eqz v0, :cond_0

    .line 869
    const/4 v0, 0x1

    iget-object v1, p0, Lcom/google/commerce/payments/orchestration/proto/ui/instrumentcreator/Api$RefreshPageResponse;->error:Lcom/google/commerce/payments/orchestration/proto/ui/common/UiErrorOuterClass$UiError;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeMessage(ILcom/google/protobuf/nano/MessageNano;)V

    .line 871
    :cond_0
    iget-object v0, p0, Lcom/google/commerce/payments/orchestration/proto/ui/instrumentcreator/Api$RefreshPageResponse;->context:Lcom/google/commerce/payments/orchestration/proto/ui/common/ResponseContextOuterClass$ResponseContext;

    if-eqz v0, :cond_1

    .line 872
    const/4 v0, 0x2

    iget-object v1, p0, Lcom/google/commerce/payments/orchestration/proto/ui/instrumentcreator/Api$RefreshPageResponse;->context:Lcom/google/commerce/payments/orchestration/proto/ui/common/ResponseContextOuterClass$ResponseContext;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeMessage(ILcom/google/protobuf/nano/MessageNano;)V

    .line 874
    :cond_1
    iget-object v0, p0, Lcom/google/commerce/payments/orchestration/proto/ui/instrumentcreator/Api$RefreshPageResponse;->nextPage:Lcom/google/commerce/payments/orchestration/proto/ui/instrumentcreator/Api$Page;

    if-eqz v0, :cond_2

    .line 875
    const/4 v0, 0x3

    iget-object v1, p0, Lcom/google/commerce/payments/orchestration/proto/ui/instrumentcreator/Api$RefreshPageResponse;->nextPage:Lcom/google/commerce/payments/orchestration/proto/ui/instrumentcreator/Api$Page;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeMessage(ILcom/google/protobuf/nano/MessageNano;)V

    .line 877
    :cond_2
    invoke-super {p0, p1}, Lcom/google/protobuf/nano/MessageNano;->writeTo(Lcom/google/protobuf/nano/CodedOutputByteBufferNano;)V

    .line 878
    return-void
.end method
