.class public final Lcom/google/commerce/payments/orchestration/proto/ui/common/components/customer/CustomerFormOuterClass$CustomerForm;
.super Lcom/google/protobuf/nano/MessageNano;
.source "CustomerFormOuterClass.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/commerce/payments/orchestration/proto/ui/common/components/customer/CustomerFormOuterClass;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "CustomerForm"
.end annotation


# instance fields
.field public id:Ljava/lang/String;

.field public initialTaxInfoForm:I

.field public instrumentForm:Lcom/google/commerce/payments/orchestration/proto/ui/common/components/instrument/InstrumentFormOuterClass$InstrumentForm;

.field public legalAddressForm:Lcom/google/commerce/payments/orchestration/proto/ui/common/components/AddressFormOuterClass$AddressForm;

.field public legalCountrySelector:Lcom/google/commerce/payments/orchestration/proto/ui/common/components/AddressFormOuterClass$CountrySelector;

.field public legalMessages:Lcom/google/commerce/payments/orchestration/proto/ui/common/components/legal/LegalMessageSetOuterClass$LegalMessageSet;

.field public taxInfoForm:[Lcom/google/commerce/payments/orchestration/proto/ui/common/components/tax/TaxInfoFormOuterClass$TaxInfoForm;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 50
    invoke-direct {p0}, Lcom/google/protobuf/nano/MessageNano;-><init>()V

    .line 51
    invoke-virtual {p0}, Lcom/google/commerce/payments/orchestration/proto/ui/common/components/customer/CustomerFormOuterClass$CustomerForm;->clear()Lcom/google/commerce/payments/orchestration/proto/ui/common/components/customer/CustomerFormOuterClass$CustomerForm;

    .line 52
    return-void
.end method


# virtual methods
.method public clear()Lcom/google/commerce/payments/orchestration/proto/ui/common/components/customer/CustomerFormOuterClass$CustomerForm;
    .locals 3

    .prologue
    const/4 v2, -0x1

    const/4 v1, 0x0

    .line 55
    const-string v0, ""

    iput-object v0, p0, Lcom/google/commerce/payments/orchestration/proto/ui/common/components/customer/CustomerFormOuterClass$CustomerForm;->id:Ljava/lang/String;

    .line 56
    iput-object v1, p0, Lcom/google/commerce/payments/orchestration/proto/ui/common/components/customer/CustomerFormOuterClass$CustomerForm;->legalCountrySelector:Lcom/google/commerce/payments/orchestration/proto/ui/common/components/AddressFormOuterClass$CountrySelector;

    .line 57
    iput-object v1, p0, Lcom/google/commerce/payments/orchestration/proto/ui/common/components/customer/CustomerFormOuterClass$CustomerForm;->legalAddressForm:Lcom/google/commerce/payments/orchestration/proto/ui/common/components/AddressFormOuterClass$AddressForm;

    .line 58
    iput-object v1, p0, Lcom/google/commerce/payments/orchestration/proto/ui/common/components/customer/CustomerFormOuterClass$CustomerForm;->instrumentForm:Lcom/google/commerce/payments/orchestration/proto/ui/common/components/instrument/InstrumentFormOuterClass$InstrumentForm;

    .line 59
    iput-object v1, p0, Lcom/google/commerce/payments/orchestration/proto/ui/common/components/customer/CustomerFormOuterClass$CustomerForm;->legalMessages:Lcom/google/commerce/payments/orchestration/proto/ui/common/components/legal/LegalMessageSetOuterClass$LegalMessageSet;

    .line 60
    invoke-static {}, Lcom/google/commerce/payments/orchestration/proto/ui/common/components/tax/TaxInfoFormOuterClass$TaxInfoForm;->emptyArray()[Lcom/google/commerce/payments/orchestration/proto/ui/common/components/tax/TaxInfoFormOuterClass$TaxInfoForm;

    move-result-object v0

    iput-object v0, p0, Lcom/google/commerce/payments/orchestration/proto/ui/common/components/customer/CustomerFormOuterClass$CustomerForm;->taxInfoForm:[Lcom/google/commerce/payments/orchestration/proto/ui/common/components/tax/TaxInfoFormOuterClass$TaxInfoForm;

    .line 61
    iput v2, p0, Lcom/google/commerce/payments/orchestration/proto/ui/common/components/customer/CustomerFormOuterClass$CustomerForm;->initialTaxInfoForm:I

    .line 62
    iput v2, p0, Lcom/google/commerce/payments/orchestration/proto/ui/common/components/customer/CustomerFormOuterClass$CustomerForm;->cachedSize:I

    .line 63
    return-object p0
.end method

.method protected computeSerializedSize()I
    .locals 5

    .prologue
    .line 100
    invoke-super {p0}, Lcom/google/protobuf/nano/MessageNano;->computeSerializedSize()I

    move-result v2

    .line 101
    .local v2, "size":I
    iget-object v3, p0, Lcom/google/commerce/payments/orchestration/proto/ui/common/components/customer/CustomerFormOuterClass$CustomerForm;->legalAddressForm:Lcom/google/commerce/payments/orchestration/proto/ui/common/components/AddressFormOuterClass$AddressForm;

    if-eqz v3, :cond_0

    .line 102
    const/4 v3, 0x1

    iget-object v4, p0, Lcom/google/commerce/payments/orchestration/proto/ui/common/components/customer/CustomerFormOuterClass$CustomerForm;->legalAddressForm:Lcom/google/commerce/payments/orchestration/proto/ui/common/components/AddressFormOuterClass$AddressForm;

    invoke-static {v3, v4}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeMessageSize(ILcom/google/protobuf/nano/MessageNano;)I

    move-result v3

    add-int/2addr v2, v3

    .line 105
    :cond_0
    iget-object v3, p0, Lcom/google/commerce/payments/orchestration/proto/ui/common/components/customer/CustomerFormOuterClass$CustomerForm;->instrumentForm:Lcom/google/commerce/payments/orchestration/proto/ui/common/components/instrument/InstrumentFormOuterClass$InstrumentForm;

    if-eqz v3, :cond_1

    .line 106
    const/4 v3, 0x2

    iget-object v4, p0, Lcom/google/commerce/payments/orchestration/proto/ui/common/components/customer/CustomerFormOuterClass$CustomerForm;->instrumentForm:Lcom/google/commerce/payments/orchestration/proto/ui/common/components/instrument/InstrumentFormOuterClass$InstrumentForm;

    invoke-static {v3, v4}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeMessageSize(ILcom/google/protobuf/nano/MessageNano;)I

    move-result v3

    add-int/2addr v2, v3

    .line 109
    :cond_1
    iget-object v3, p0, Lcom/google/commerce/payments/orchestration/proto/ui/common/components/customer/CustomerFormOuterClass$CustomerForm;->legalMessages:Lcom/google/commerce/payments/orchestration/proto/ui/common/components/legal/LegalMessageSetOuterClass$LegalMessageSet;

    if-eqz v3, :cond_2

    .line 110
    const/4 v3, 0x4

    iget-object v4, p0, Lcom/google/commerce/payments/orchestration/proto/ui/common/components/customer/CustomerFormOuterClass$CustomerForm;->legalMessages:Lcom/google/commerce/payments/orchestration/proto/ui/common/components/legal/LegalMessageSetOuterClass$LegalMessageSet;

    invoke-static {v3, v4}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeMessageSize(ILcom/google/protobuf/nano/MessageNano;)I

    move-result v3

    add-int/2addr v2, v3

    .line 113
    :cond_2
    iget-object v3, p0, Lcom/google/commerce/payments/orchestration/proto/ui/common/components/customer/CustomerFormOuterClass$CustomerForm;->id:Ljava/lang/String;

    const-string v4, ""

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_3

    .line 114
    const/4 v3, 0x5

    iget-object v4, p0, Lcom/google/commerce/payments/orchestration/proto/ui/common/components/customer/CustomerFormOuterClass$CustomerForm;->id:Ljava/lang/String;

    invoke-static {v3, v4}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeStringSize(ILjava/lang/String;)I

    move-result v3

    add-int/2addr v2, v3

    .line 117
    :cond_3
    iget-object v3, p0, Lcom/google/commerce/payments/orchestration/proto/ui/common/components/customer/CustomerFormOuterClass$CustomerForm;->legalCountrySelector:Lcom/google/commerce/payments/orchestration/proto/ui/common/components/AddressFormOuterClass$CountrySelector;

    if-eqz v3, :cond_4

    .line 118
    const/4 v3, 0x6

    iget-object v4, p0, Lcom/google/commerce/payments/orchestration/proto/ui/common/components/customer/CustomerFormOuterClass$CustomerForm;->legalCountrySelector:Lcom/google/commerce/payments/orchestration/proto/ui/common/components/AddressFormOuterClass$CountrySelector;

    invoke-static {v3, v4}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeMessageSize(ILcom/google/protobuf/nano/MessageNano;)I

    move-result v3

    add-int/2addr v2, v3

    .line 121
    :cond_4
    iget-object v3, p0, Lcom/google/commerce/payments/orchestration/proto/ui/common/components/customer/CustomerFormOuterClass$CustomerForm;->taxInfoForm:[Lcom/google/commerce/payments/orchestration/proto/ui/common/components/tax/TaxInfoFormOuterClass$TaxInfoForm;

    if-eqz v3, :cond_6

    iget-object v3, p0, Lcom/google/commerce/payments/orchestration/proto/ui/common/components/customer/CustomerFormOuterClass$CustomerForm;->taxInfoForm:[Lcom/google/commerce/payments/orchestration/proto/ui/common/components/tax/TaxInfoFormOuterClass$TaxInfoForm;

    array-length v3, v3

    if-lez v3, :cond_6

    .line 122
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    iget-object v3, p0, Lcom/google/commerce/payments/orchestration/proto/ui/common/components/customer/CustomerFormOuterClass$CustomerForm;->taxInfoForm:[Lcom/google/commerce/payments/orchestration/proto/ui/common/components/tax/TaxInfoFormOuterClass$TaxInfoForm;

    array-length v3, v3

    if-ge v1, v3, :cond_6

    .line 123
    iget-object v3, p0, Lcom/google/commerce/payments/orchestration/proto/ui/common/components/customer/CustomerFormOuterClass$CustomerForm;->taxInfoForm:[Lcom/google/commerce/payments/orchestration/proto/ui/common/components/tax/TaxInfoFormOuterClass$TaxInfoForm;

    aget-object v0, v3, v1

    .line 124
    .local v0, "element":Lcom/google/commerce/payments/orchestration/proto/ui/common/components/tax/TaxInfoFormOuterClass$TaxInfoForm;
    if-eqz v0, :cond_5

    .line 125
    const/4 v3, 0x7

    invoke-static {v3, v0}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeMessageSize(ILcom/google/protobuf/nano/MessageNano;)I

    move-result v3

    add-int/2addr v2, v3

    .line 122
    :cond_5
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 130
    .end local v0    # "element":Lcom/google/commerce/payments/orchestration/proto/ui/common/components/tax/TaxInfoFormOuterClass$TaxInfoForm;
    .end local v1    # "i":I
    :cond_6
    iget v3, p0, Lcom/google/commerce/payments/orchestration/proto/ui/common/components/customer/CustomerFormOuterClass$CustomerForm;->initialTaxInfoForm:I

    const/4 v4, -0x1

    if-eq v3, v4, :cond_7

    .line 131
    const/16 v3, 0x8

    iget v4, p0, Lcom/google/commerce/payments/orchestration/proto/ui/common/components/customer/CustomerFormOuterClass$CustomerForm;->initialTaxInfoForm:I

    invoke-static {v3, v4}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeInt32Size(II)I

    move-result v3

    add-int/2addr v2, v3

    .line 134
    :cond_7
    return v2
.end method

.method public mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/commerce/payments/orchestration/proto/ui/common/components/customer/CustomerFormOuterClass$CustomerForm;
    .locals 6
    .param p1, "input"    # Lcom/google/protobuf/nano/CodedInputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v4, 0x0

    .line 142
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readTag()I

    move-result v3

    .line 143
    .local v3, "tag":I
    sparse-switch v3, :sswitch_data_0

    .line 147
    invoke-static {p1, v3}, Lcom/google/protobuf/nano/WireFormatNano;->parseUnknownField(Lcom/google/protobuf/nano/CodedInputByteBufferNano;I)Z

    move-result v5

    if-nez v5, :cond_0

    .line 148
    :sswitch_0
    return-object p0

    .line 153
    :sswitch_1
    iget-object v5, p0, Lcom/google/commerce/payments/orchestration/proto/ui/common/components/customer/CustomerFormOuterClass$CustomerForm;->legalAddressForm:Lcom/google/commerce/payments/orchestration/proto/ui/common/components/AddressFormOuterClass$AddressForm;

    if-nez v5, :cond_1

    .line 154
    new-instance v5, Lcom/google/commerce/payments/orchestration/proto/ui/common/components/AddressFormOuterClass$AddressForm;

    invoke-direct {v5}, Lcom/google/commerce/payments/orchestration/proto/ui/common/components/AddressFormOuterClass$AddressForm;-><init>()V

    iput-object v5, p0, Lcom/google/commerce/payments/orchestration/proto/ui/common/components/customer/CustomerFormOuterClass$CustomerForm;->legalAddressForm:Lcom/google/commerce/payments/orchestration/proto/ui/common/components/AddressFormOuterClass$AddressForm;

    .line 156
    :cond_1
    iget-object v5, p0, Lcom/google/commerce/payments/orchestration/proto/ui/common/components/customer/CustomerFormOuterClass$CustomerForm;->legalAddressForm:Lcom/google/commerce/payments/orchestration/proto/ui/common/components/AddressFormOuterClass$AddressForm;

    invoke-virtual {p1, v5}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readMessage(Lcom/google/protobuf/nano/MessageNano;)V

    goto :goto_0

    .line 160
    :sswitch_2
    iget-object v5, p0, Lcom/google/commerce/payments/orchestration/proto/ui/common/components/customer/CustomerFormOuterClass$CustomerForm;->instrumentForm:Lcom/google/commerce/payments/orchestration/proto/ui/common/components/instrument/InstrumentFormOuterClass$InstrumentForm;

    if-nez v5, :cond_2

    .line 161
    new-instance v5, Lcom/google/commerce/payments/orchestration/proto/ui/common/components/instrument/InstrumentFormOuterClass$InstrumentForm;

    invoke-direct {v5}, Lcom/google/commerce/payments/orchestration/proto/ui/common/components/instrument/InstrumentFormOuterClass$InstrumentForm;-><init>()V

    iput-object v5, p0, Lcom/google/commerce/payments/orchestration/proto/ui/common/components/customer/CustomerFormOuterClass$CustomerForm;->instrumentForm:Lcom/google/commerce/payments/orchestration/proto/ui/common/components/instrument/InstrumentFormOuterClass$InstrumentForm;

    .line 163
    :cond_2
    iget-object v5, p0, Lcom/google/commerce/payments/orchestration/proto/ui/common/components/customer/CustomerFormOuterClass$CustomerForm;->instrumentForm:Lcom/google/commerce/payments/orchestration/proto/ui/common/components/instrument/InstrumentFormOuterClass$InstrumentForm;

    invoke-virtual {p1, v5}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readMessage(Lcom/google/protobuf/nano/MessageNano;)V

    goto :goto_0

    .line 167
    :sswitch_3
    iget-object v5, p0, Lcom/google/commerce/payments/orchestration/proto/ui/common/components/customer/CustomerFormOuterClass$CustomerForm;->legalMessages:Lcom/google/commerce/payments/orchestration/proto/ui/common/components/legal/LegalMessageSetOuterClass$LegalMessageSet;

    if-nez v5, :cond_3

    .line 168
    new-instance v5, Lcom/google/commerce/payments/orchestration/proto/ui/common/components/legal/LegalMessageSetOuterClass$LegalMessageSet;

    invoke-direct {v5}, Lcom/google/commerce/payments/orchestration/proto/ui/common/components/legal/LegalMessageSetOuterClass$LegalMessageSet;-><init>()V

    iput-object v5, p0, Lcom/google/commerce/payments/orchestration/proto/ui/common/components/customer/CustomerFormOuterClass$CustomerForm;->legalMessages:Lcom/google/commerce/payments/orchestration/proto/ui/common/components/legal/LegalMessageSetOuterClass$LegalMessageSet;

    .line 170
    :cond_3
    iget-object v5, p0, Lcom/google/commerce/payments/orchestration/proto/ui/common/components/customer/CustomerFormOuterClass$CustomerForm;->legalMessages:Lcom/google/commerce/payments/orchestration/proto/ui/common/components/legal/LegalMessageSetOuterClass$LegalMessageSet;

    invoke-virtual {p1, v5}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readMessage(Lcom/google/protobuf/nano/MessageNano;)V

    goto :goto_0

    .line 174
    :sswitch_4
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readString()Ljava/lang/String;

    move-result-object v5

    iput-object v5, p0, Lcom/google/commerce/payments/orchestration/proto/ui/common/components/customer/CustomerFormOuterClass$CustomerForm;->id:Ljava/lang/String;

    goto :goto_0

    .line 178
    :sswitch_5
    iget-object v5, p0, Lcom/google/commerce/payments/orchestration/proto/ui/common/components/customer/CustomerFormOuterClass$CustomerForm;->legalCountrySelector:Lcom/google/commerce/payments/orchestration/proto/ui/common/components/AddressFormOuterClass$CountrySelector;

    if-nez v5, :cond_4

    .line 179
    new-instance v5, Lcom/google/commerce/payments/orchestration/proto/ui/common/components/AddressFormOuterClass$CountrySelector;

    invoke-direct {v5}, Lcom/google/commerce/payments/orchestration/proto/ui/common/components/AddressFormOuterClass$CountrySelector;-><init>()V

    iput-object v5, p0, Lcom/google/commerce/payments/orchestration/proto/ui/common/components/customer/CustomerFormOuterClass$CustomerForm;->legalCountrySelector:Lcom/google/commerce/payments/orchestration/proto/ui/common/components/AddressFormOuterClass$CountrySelector;

    .line 181
    :cond_4
    iget-object v5, p0, Lcom/google/commerce/payments/orchestration/proto/ui/common/components/customer/CustomerFormOuterClass$CustomerForm;->legalCountrySelector:Lcom/google/commerce/payments/orchestration/proto/ui/common/components/AddressFormOuterClass$CountrySelector;

    invoke-virtual {p1, v5}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readMessage(Lcom/google/protobuf/nano/MessageNano;)V

    goto :goto_0

    .line 185
    :sswitch_6
    const/16 v5, 0x3a

    invoke-static {p1, v5}, Lcom/google/protobuf/nano/WireFormatNano;->getRepeatedFieldArrayLength(Lcom/google/protobuf/nano/CodedInputByteBufferNano;I)I

    move-result v0

    .line 187
    .local v0, "arrayLength":I
    iget-object v5, p0, Lcom/google/commerce/payments/orchestration/proto/ui/common/components/customer/CustomerFormOuterClass$CustomerForm;->taxInfoForm:[Lcom/google/commerce/payments/orchestration/proto/ui/common/components/tax/TaxInfoFormOuterClass$TaxInfoForm;

    if-nez v5, :cond_6

    move v1, v4

    .line 188
    .local v1, "i":I
    :goto_1
    add-int v5, v1, v0

    new-array v2, v5, [Lcom/google/commerce/payments/orchestration/proto/ui/common/components/tax/TaxInfoFormOuterClass$TaxInfoForm;

    .line 190
    .local v2, "newArray":[Lcom/google/commerce/payments/orchestration/proto/ui/common/components/tax/TaxInfoFormOuterClass$TaxInfoForm;
    if-eqz v1, :cond_5

    .line 191
    iget-object v5, p0, Lcom/google/commerce/payments/orchestration/proto/ui/common/components/customer/CustomerFormOuterClass$CustomerForm;->taxInfoForm:[Lcom/google/commerce/payments/orchestration/proto/ui/common/components/tax/TaxInfoFormOuterClass$TaxInfoForm;

    invoke-static {v5, v4, v2, v4, v1}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 193
    :cond_5
    :goto_2
    array-length v5, v2

    add-int/lit8 v5, v5, -0x1

    if-ge v1, v5, :cond_7

    .line 194
    new-instance v5, Lcom/google/commerce/payments/orchestration/proto/ui/common/components/tax/TaxInfoFormOuterClass$TaxInfoForm;

    invoke-direct {v5}, Lcom/google/commerce/payments/orchestration/proto/ui/common/components/tax/TaxInfoFormOuterClass$TaxInfoForm;-><init>()V

    aput-object v5, v2, v1

    .line 195
    aget-object v5, v2, v1

    invoke-virtual {p1, v5}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readMessage(Lcom/google/protobuf/nano/MessageNano;)V

    .line 196
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readTag()I

    .line 193
    add-int/lit8 v1, v1, 0x1

    goto :goto_2

    .line 187
    .end local v1    # "i":I
    .end local v2    # "newArray":[Lcom/google/commerce/payments/orchestration/proto/ui/common/components/tax/TaxInfoFormOuterClass$TaxInfoForm;
    :cond_6
    iget-object v5, p0, Lcom/google/commerce/payments/orchestration/proto/ui/common/components/customer/CustomerFormOuterClass$CustomerForm;->taxInfoForm:[Lcom/google/commerce/payments/orchestration/proto/ui/common/components/tax/TaxInfoFormOuterClass$TaxInfoForm;

    array-length v1, v5

    goto :goto_1

    .line 199
    .restart local v1    # "i":I
    .restart local v2    # "newArray":[Lcom/google/commerce/payments/orchestration/proto/ui/common/components/tax/TaxInfoFormOuterClass$TaxInfoForm;
    :cond_7
    new-instance v5, Lcom/google/commerce/payments/orchestration/proto/ui/common/components/tax/TaxInfoFormOuterClass$TaxInfoForm;

    invoke-direct {v5}, Lcom/google/commerce/payments/orchestration/proto/ui/common/components/tax/TaxInfoFormOuterClass$TaxInfoForm;-><init>()V

    aput-object v5, v2, v1

    .line 200
    aget-object v5, v2, v1

    invoke-virtual {p1, v5}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readMessage(Lcom/google/protobuf/nano/MessageNano;)V

    .line 201
    iput-object v2, p0, Lcom/google/commerce/payments/orchestration/proto/ui/common/components/customer/CustomerFormOuterClass$CustomerForm;->taxInfoForm:[Lcom/google/commerce/payments/orchestration/proto/ui/common/components/tax/TaxInfoFormOuterClass$TaxInfoForm;

    goto/16 :goto_0

    .line 205
    .end local v0    # "arrayLength":I
    .end local v1    # "i":I
    .end local v2    # "newArray":[Lcom/google/commerce/payments/orchestration/proto/ui/common/components/tax/TaxInfoFormOuterClass$TaxInfoForm;
    :sswitch_7
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readInt32()I

    move-result v5

    iput v5, p0, Lcom/google/commerce/payments/orchestration/proto/ui/common/components/customer/CustomerFormOuterClass$CustomerForm;->initialTaxInfoForm:I

    goto/16 :goto_0

    .line 143
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x22 -> :sswitch_3
        0x2a -> :sswitch_4
        0x32 -> :sswitch_5
        0x3a -> :sswitch_6
        0x40 -> :sswitch_7
    .end sparse-switch
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/protobuf/nano/MessageNano;
    .locals 1
    .param p1, "x0"    # Lcom/google/protobuf/nano/CodedInputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 8
    invoke-virtual {p0, p1}, Lcom/google/commerce/payments/orchestration/proto/ui/common/components/customer/CustomerFormOuterClass$CustomerForm;->mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/commerce/payments/orchestration/proto/ui/common/components/customer/CustomerFormOuterClass$CustomerForm;

    move-result-object v0

    return-object v0
.end method

.method public writeTo(Lcom/google/protobuf/nano/CodedOutputByteBufferNano;)V
    .locals 4
    .param p1, "output"    # Lcom/google/protobuf/nano/CodedOutputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 69
    iget-object v2, p0, Lcom/google/commerce/payments/orchestration/proto/ui/common/components/customer/CustomerFormOuterClass$CustomerForm;->legalAddressForm:Lcom/google/commerce/payments/orchestration/proto/ui/common/components/AddressFormOuterClass$AddressForm;

    if-eqz v2, :cond_0

    .line 70
    const/4 v2, 0x1

    iget-object v3, p0, Lcom/google/commerce/payments/orchestration/proto/ui/common/components/customer/CustomerFormOuterClass$CustomerForm;->legalAddressForm:Lcom/google/commerce/payments/orchestration/proto/ui/common/components/AddressFormOuterClass$AddressForm;

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeMessage(ILcom/google/protobuf/nano/MessageNano;)V

    .line 72
    :cond_0
    iget-object v2, p0, Lcom/google/commerce/payments/orchestration/proto/ui/common/components/customer/CustomerFormOuterClass$CustomerForm;->instrumentForm:Lcom/google/commerce/payments/orchestration/proto/ui/common/components/instrument/InstrumentFormOuterClass$InstrumentForm;

    if-eqz v2, :cond_1

    .line 73
    const/4 v2, 0x2

    iget-object v3, p0, Lcom/google/commerce/payments/orchestration/proto/ui/common/components/customer/CustomerFormOuterClass$CustomerForm;->instrumentForm:Lcom/google/commerce/payments/orchestration/proto/ui/common/components/instrument/InstrumentFormOuterClass$InstrumentForm;

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeMessage(ILcom/google/protobuf/nano/MessageNano;)V

    .line 75
    :cond_1
    iget-object v2, p0, Lcom/google/commerce/payments/orchestration/proto/ui/common/components/customer/CustomerFormOuterClass$CustomerForm;->legalMessages:Lcom/google/commerce/payments/orchestration/proto/ui/common/components/legal/LegalMessageSetOuterClass$LegalMessageSet;

    if-eqz v2, :cond_2

    .line 76
    const/4 v2, 0x4

    iget-object v3, p0, Lcom/google/commerce/payments/orchestration/proto/ui/common/components/customer/CustomerFormOuterClass$CustomerForm;->legalMessages:Lcom/google/commerce/payments/orchestration/proto/ui/common/components/legal/LegalMessageSetOuterClass$LegalMessageSet;

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeMessage(ILcom/google/protobuf/nano/MessageNano;)V

    .line 78
    :cond_2
    iget-object v2, p0, Lcom/google/commerce/payments/orchestration/proto/ui/common/components/customer/CustomerFormOuterClass$CustomerForm;->id:Ljava/lang/String;

    const-string v3, ""

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_3

    .line 79
    const/4 v2, 0x5

    iget-object v3, p0, Lcom/google/commerce/payments/orchestration/proto/ui/common/components/customer/CustomerFormOuterClass$CustomerForm;->id:Ljava/lang/String;

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeString(ILjava/lang/String;)V

    .line 81
    :cond_3
    iget-object v2, p0, Lcom/google/commerce/payments/orchestration/proto/ui/common/components/customer/CustomerFormOuterClass$CustomerForm;->legalCountrySelector:Lcom/google/commerce/payments/orchestration/proto/ui/common/components/AddressFormOuterClass$CountrySelector;

    if-eqz v2, :cond_4

    .line 82
    const/4 v2, 0x6

    iget-object v3, p0, Lcom/google/commerce/payments/orchestration/proto/ui/common/components/customer/CustomerFormOuterClass$CustomerForm;->legalCountrySelector:Lcom/google/commerce/payments/orchestration/proto/ui/common/components/AddressFormOuterClass$CountrySelector;

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeMessage(ILcom/google/protobuf/nano/MessageNano;)V

    .line 84
    :cond_4
    iget-object v2, p0, Lcom/google/commerce/payments/orchestration/proto/ui/common/components/customer/CustomerFormOuterClass$CustomerForm;->taxInfoForm:[Lcom/google/commerce/payments/orchestration/proto/ui/common/components/tax/TaxInfoFormOuterClass$TaxInfoForm;

    if-eqz v2, :cond_6

    iget-object v2, p0, Lcom/google/commerce/payments/orchestration/proto/ui/common/components/customer/CustomerFormOuterClass$CustomerForm;->taxInfoForm:[Lcom/google/commerce/payments/orchestration/proto/ui/common/components/tax/TaxInfoFormOuterClass$TaxInfoForm;

    array-length v2, v2

    if-lez v2, :cond_6

    .line 85
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    iget-object v2, p0, Lcom/google/commerce/payments/orchestration/proto/ui/common/components/customer/CustomerFormOuterClass$CustomerForm;->taxInfoForm:[Lcom/google/commerce/payments/orchestration/proto/ui/common/components/tax/TaxInfoFormOuterClass$TaxInfoForm;

    array-length v2, v2

    if-ge v1, v2, :cond_6

    .line 86
    iget-object v2, p0, Lcom/google/commerce/payments/orchestration/proto/ui/common/components/customer/CustomerFormOuterClass$CustomerForm;->taxInfoForm:[Lcom/google/commerce/payments/orchestration/proto/ui/common/components/tax/TaxInfoFormOuterClass$TaxInfoForm;

    aget-object v0, v2, v1

    .line 87
    .local v0, "element":Lcom/google/commerce/payments/orchestration/proto/ui/common/components/tax/TaxInfoFormOuterClass$TaxInfoForm;
    if-eqz v0, :cond_5

    .line 88
    const/4 v2, 0x7

    invoke-virtual {p1, v2, v0}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeMessage(ILcom/google/protobuf/nano/MessageNano;)V

    .line 85
    :cond_5
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 92
    .end local v0    # "element":Lcom/google/commerce/payments/orchestration/proto/ui/common/components/tax/TaxInfoFormOuterClass$TaxInfoForm;
    .end local v1    # "i":I
    :cond_6
    iget v2, p0, Lcom/google/commerce/payments/orchestration/proto/ui/common/components/customer/CustomerFormOuterClass$CustomerForm;->initialTaxInfoForm:I

    const/4 v3, -0x1

    if-eq v2, v3, :cond_7

    .line 93
    const/16 v2, 0x8

    iget v3, p0, Lcom/google/commerce/payments/orchestration/proto/ui/common/components/customer/CustomerFormOuterClass$CustomerForm;->initialTaxInfoForm:I

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeInt32(II)V

    .line 95
    :cond_7
    invoke-super {p0, p1}, Lcom/google/protobuf/nano/MessageNano;->writeTo(Lcom/google/protobuf/nano/CodedOutputByteBufferNano;)V

    .line 96
    return-void
.end method
