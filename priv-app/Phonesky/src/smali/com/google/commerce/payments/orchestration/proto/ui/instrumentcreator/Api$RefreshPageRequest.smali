.class public final Lcom/google/commerce/payments/orchestration/proto/ui/instrumentcreator/Api$RefreshPageRequest;
.super Lcom/google/protobuf/nano/MessageNano;
.source "Api.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/commerce/payments/orchestration/proto/ui/instrumentcreator/Api;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "RefreshPageRequest"
.end annotation


# instance fields
.field public context:Lcom/google/commerce/payments/orchestration/proto/ui/common/RequestContextOuterClass$RequestContext;

.field public pageValue:Lcom/google/commerce/payments/orchestration/proto/ui/instrumentcreator/Api$PageValue;

.field public refreshTriggerField:Lcom/google/commerce/payments/orchestration/proto/ui/common/FormFieldReferenceOuterClass$FormFieldReference;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 730
    invoke-direct {p0}, Lcom/google/protobuf/nano/MessageNano;-><init>()V

    .line 731
    invoke-virtual {p0}, Lcom/google/commerce/payments/orchestration/proto/ui/instrumentcreator/Api$RefreshPageRequest;->clear()Lcom/google/commerce/payments/orchestration/proto/ui/instrumentcreator/Api$RefreshPageRequest;

    .line 732
    return-void
.end method


# virtual methods
.method public clear()Lcom/google/commerce/payments/orchestration/proto/ui/instrumentcreator/Api$RefreshPageRequest;
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 735
    iput-object v0, p0, Lcom/google/commerce/payments/orchestration/proto/ui/instrumentcreator/Api$RefreshPageRequest;->context:Lcom/google/commerce/payments/orchestration/proto/ui/common/RequestContextOuterClass$RequestContext;

    .line 736
    iput-object v0, p0, Lcom/google/commerce/payments/orchestration/proto/ui/instrumentcreator/Api$RefreshPageRequest;->pageValue:Lcom/google/commerce/payments/orchestration/proto/ui/instrumentcreator/Api$PageValue;

    .line 737
    iput-object v0, p0, Lcom/google/commerce/payments/orchestration/proto/ui/instrumentcreator/Api$RefreshPageRequest;->refreshTriggerField:Lcom/google/commerce/payments/orchestration/proto/ui/common/FormFieldReferenceOuterClass$FormFieldReference;

    .line 738
    const/4 v0, -0x1

    iput v0, p0, Lcom/google/commerce/payments/orchestration/proto/ui/instrumentcreator/Api$RefreshPageRequest;->cachedSize:I

    .line 739
    return-object p0
.end method

.method protected computeSerializedSize()I
    .locals 3

    .prologue
    .line 759
    invoke-super {p0}, Lcom/google/protobuf/nano/MessageNano;->computeSerializedSize()I

    move-result v0

    .line 760
    .local v0, "size":I
    iget-object v1, p0, Lcom/google/commerce/payments/orchestration/proto/ui/instrumentcreator/Api$RefreshPageRequest;->context:Lcom/google/commerce/payments/orchestration/proto/ui/common/RequestContextOuterClass$RequestContext;

    if-eqz v1, :cond_0

    .line 761
    const/4 v1, 0x1

    iget-object v2, p0, Lcom/google/commerce/payments/orchestration/proto/ui/instrumentcreator/Api$RefreshPageRequest;->context:Lcom/google/commerce/payments/orchestration/proto/ui/common/RequestContextOuterClass$RequestContext;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeMessageSize(ILcom/google/protobuf/nano/MessageNano;)I

    move-result v1

    add-int/2addr v0, v1

    .line 764
    :cond_0
    iget-object v1, p0, Lcom/google/commerce/payments/orchestration/proto/ui/instrumentcreator/Api$RefreshPageRequest;->pageValue:Lcom/google/commerce/payments/orchestration/proto/ui/instrumentcreator/Api$PageValue;

    if-eqz v1, :cond_1

    .line 765
    const/4 v1, 0x2

    iget-object v2, p0, Lcom/google/commerce/payments/orchestration/proto/ui/instrumentcreator/Api$RefreshPageRequest;->pageValue:Lcom/google/commerce/payments/orchestration/proto/ui/instrumentcreator/Api$PageValue;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeMessageSize(ILcom/google/protobuf/nano/MessageNano;)I

    move-result v1

    add-int/2addr v0, v1

    .line 768
    :cond_1
    iget-object v1, p0, Lcom/google/commerce/payments/orchestration/proto/ui/instrumentcreator/Api$RefreshPageRequest;->refreshTriggerField:Lcom/google/commerce/payments/orchestration/proto/ui/common/FormFieldReferenceOuterClass$FormFieldReference;

    if-eqz v1, :cond_2

    .line 769
    const/4 v1, 0x3

    iget-object v2, p0, Lcom/google/commerce/payments/orchestration/proto/ui/instrumentcreator/Api$RefreshPageRequest;->refreshTriggerField:Lcom/google/commerce/payments/orchestration/proto/ui/common/FormFieldReferenceOuterClass$FormFieldReference;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeMessageSize(ILcom/google/protobuf/nano/MessageNano;)I

    move-result v1

    add-int/2addr v0, v1

    .line 772
    :cond_2
    return v0
.end method

.method public mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/commerce/payments/orchestration/proto/ui/instrumentcreator/Api$RefreshPageRequest;
    .locals 2
    .param p1, "input"    # Lcom/google/protobuf/nano/CodedInputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 780
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readTag()I

    move-result v0

    .line 781
    .local v0, "tag":I
    sparse-switch v0, :sswitch_data_0

    .line 785
    invoke-static {p1, v0}, Lcom/google/protobuf/nano/WireFormatNano;->parseUnknownField(Lcom/google/protobuf/nano/CodedInputByteBufferNano;I)Z

    move-result v1

    if-nez v1, :cond_0

    .line 786
    :sswitch_0
    return-object p0

    .line 791
    :sswitch_1
    iget-object v1, p0, Lcom/google/commerce/payments/orchestration/proto/ui/instrumentcreator/Api$RefreshPageRequest;->context:Lcom/google/commerce/payments/orchestration/proto/ui/common/RequestContextOuterClass$RequestContext;

    if-nez v1, :cond_1

    .line 792
    new-instance v1, Lcom/google/commerce/payments/orchestration/proto/ui/common/RequestContextOuterClass$RequestContext;

    invoke-direct {v1}, Lcom/google/commerce/payments/orchestration/proto/ui/common/RequestContextOuterClass$RequestContext;-><init>()V

    iput-object v1, p0, Lcom/google/commerce/payments/orchestration/proto/ui/instrumentcreator/Api$RefreshPageRequest;->context:Lcom/google/commerce/payments/orchestration/proto/ui/common/RequestContextOuterClass$RequestContext;

    .line 794
    :cond_1
    iget-object v1, p0, Lcom/google/commerce/payments/orchestration/proto/ui/instrumentcreator/Api$RefreshPageRequest;->context:Lcom/google/commerce/payments/orchestration/proto/ui/common/RequestContextOuterClass$RequestContext;

    invoke-virtual {p1, v1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readMessage(Lcom/google/protobuf/nano/MessageNano;)V

    goto :goto_0

    .line 798
    :sswitch_2
    iget-object v1, p0, Lcom/google/commerce/payments/orchestration/proto/ui/instrumentcreator/Api$RefreshPageRequest;->pageValue:Lcom/google/commerce/payments/orchestration/proto/ui/instrumentcreator/Api$PageValue;

    if-nez v1, :cond_2

    .line 799
    new-instance v1, Lcom/google/commerce/payments/orchestration/proto/ui/instrumentcreator/Api$PageValue;

    invoke-direct {v1}, Lcom/google/commerce/payments/orchestration/proto/ui/instrumentcreator/Api$PageValue;-><init>()V

    iput-object v1, p0, Lcom/google/commerce/payments/orchestration/proto/ui/instrumentcreator/Api$RefreshPageRequest;->pageValue:Lcom/google/commerce/payments/orchestration/proto/ui/instrumentcreator/Api$PageValue;

    .line 801
    :cond_2
    iget-object v1, p0, Lcom/google/commerce/payments/orchestration/proto/ui/instrumentcreator/Api$RefreshPageRequest;->pageValue:Lcom/google/commerce/payments/orchestration/proto/ui/instrumentcreator/Api$PageValue;

    invoke-virtual {p1, v1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readMessage(Lcom/google/protobuf/nano/MessageNano;)V

    goto :goto_0

    .line 805
    :sswitch_3
    iget-object v1, p0, Lcom/google/commerce/payments/orchestration/proto/ui/instrumentcreator/Api$RefreshPageRequest;->refreshTriggerField:Lcom/google/commerce/payments/orchestration/proto/ui/common/FormFieldReferenceOuterClass$FormFieldReference;

    if-nez v1, :cond_3

    .line 806
    new-instance v1, Lcom/google/commerce/payments/orchestration/proto/ui/common/FormFieldReferenceOuterClass$FormFieldReference;

    invoke-direct {v1}, Lcom/google/commerce/payments/orchestration/proto/ui/common/FormFieldReferenceOuterClass$FormFieldReference;-><init>()V

    iput-object v1, p0, Lcom/google/commerce/payments/orchestration/proto/ui/instrumentcreator/Api$RefreshPageRequest;->refreshTriggerField:Lcom/google/commerce/payments/orchestration/proto/ui/common/FormFieldReferenceOuterClass$FormFieldReference;

    .line 808
    :cond_3
    iget-object v1, p0, Lcom/google/commerce/payments/orchestration/proto/ui/instrumentcreator/Api$RefreshPageRequest;->refreshTriggerField:Lcom/google/commerce/payments/orchestration/proto/ui/common/FormFieldReferenceOuterClass$FormFieldReference;

    invoke-virtual {p1, v1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readMessage(Lcom/google/protobuf/nano/MessageNano;)V

    goto :goto_0

    .line 781
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
    .end sparse-switch
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/protobuf/nano/MessageNano;
    .locals 1
    .param p1, "x0"    # Lcom/google/protobuf/nano/CodedInputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 704
    invoke-virtual {p0, p1}, Lcom/google/commerce/payments/orchestration/proto/ui/instrumentcreator/Api$RefreshPageRequest;->mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/commerce/payments/orchestration/proto/ui/instrumentcreator/Api$RefreshPageRequest;

    move-result-object v0

    return-object v0
.end method

.method public writeTo(Lcom/google/protobuf/nano/CodedOutputByteBufferNano;)V
    .locals 2
    .param p1, "output"    # Lcom/google/protobuf/nano/CodedOutputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 745
    iget-object v0, p0, Lcom/google/commerce/payments/orchestration/proto/ui/instrumentcreator/Api$RefreshPageRequest;->context:Lcom/google/commerce/payments/orchestration/proto/ui/common/RequestContextOuterClass$RequestContext;

    if-eqz v0, :cond_0

    .line 746
    const/4 v0, 0x1

    iget-object v1, p0, Lcom/google/commerce/payments/orchestration/proto/ui/instrumentcreator/Api$RefreshPageRequest;->context:Lcom/google/commerce/payments/orchestration/proto/ui/common/RequestContextOuterClass$RequestContext;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeMessage(ILcom/google/protobuf/nano/MessageNano;)V

    .line 748
    :cond_0
    iget-object v0, p0, Lcom/google/commerce/payments/orchestration/proto/ui/instrumentcreator/Api$RefreshPageRequest;->pageValue:Lcom/google/commerce/payments/orchestration/proto/ui/instrumentcreator/Api$PageValue;

    if-eqz v0, :cond_1

    .line 749
    const/4 v0, 0x2

    iget-object v1, p0, Lcom/google/commerce/payments/orchestration/proto/ui/instrumentcreator/Api$RefreshPageRequest;->pageValue:Lcom/google/commerce/payments/orchestration/proto/ui/instrumentcreator/Api$PageValue;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeMessage(ILcom/google/protobuf/nano/MessageNano;)V

    .line 751
    :cond_1
    iget-object v0, p0, Lcom/google/commerce/payments/orchestration/proto/ui/instrumentcreator/Api$RefreshPageRequest;->refreshTriggerField:Lcom/google/commerce/payments/orchestration/proto/ui/common/FormFieldReferenceOuterClass$FormFieldReference;

    if-eqz v0, :cond_2

    .line 752
    const/4 v0, 0x3

    iget-object v1, p0, Lcom/google/commerce/payments/orchestration/proto/ui/instrumentcreator/Api$RefreshPageRequest;->refreshTriggerField:Lcom/google/commerce/payments/orchestration/proto/ui/common/FormFieldReferenceOuterClass$FormFieldReference;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeMessage(ILcom/google/protobuf/nano/MessageNano;)V

    .line 754
    :cond_2
    invoke-super {p0, p1}, Lcom/google/protobuf/nano/MessageNano;->writeTo(Lcom/google/protobuf/nano/CodedOutputByteBufferNano;)V

    .line 755
    return-void
.end method
