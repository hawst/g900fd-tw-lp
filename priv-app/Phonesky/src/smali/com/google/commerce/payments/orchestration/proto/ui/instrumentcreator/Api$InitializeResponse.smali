.class public final Lcom/google/commerce/payments/orchestration/proto/ui/instrumentcreator/Api$InitializeResponse;
.super Lcom/google/protobuf/nano/MessageNano;
.source "Api.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/commerce/payments/orchestration/proto/ui/instrumentcreator/Api;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "InitializeResponse"
.end annotation


# instance fields
.field public context:Lcom/google/commerce/payments/orchestration/proto/ui/common/ResponseContextOuterClass$ResponseContext;

.field public error:Lcom/google/commerce/payments/orchestration/proto/ui/common/UiErrorOuterClass$UiError;

.field public initialPage:Lcom/google/commerce/payments/orchestration/proto/ui/instrumentcreator/Api$Page;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 147
    invoke-direct {p0}, Lcom/google/protobuf/nano/MessageNano;-><init>()V

    .line 148
    invoke-virtual {p0}, Lcom/google/commerce/payments/orchestration/proto/ui/instrumentcreator/Api$InitializeResponse;->clear()Lcom/google/commerce/payments/orchestration/proto/ui/instrumentcreator/Api$InitializeResponse;

    .line 149
    return-void
.end method


# virtual methods
.method public clear()Lcom/google/commerce/payments/orchestration/proto/ui/instrumentcreator/Api$InitializeResponse;
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 152
    iput-object v0, p0, Lcom/google/commerce/payments/orchestration/proto/ui/instrumentcreator/Api$InitializeResponse;->error:Lcom/google/commerce/payments/orchestration/proto/ui/common/UiErrorOuterClass$UiError;

    .line 153
    iput-object v0, p0, Lcom/google/commerce/payments/orchestration/proto/ui/instrumentcreator/Api$InitializeResponse;->context:Lcom/google/commerce/payments/orchestration/proto/ui/common/ResponseContextOuterClass$ResponseContext;

    .line 154
    iput-object v0, p0, Lcom/google/commerce/payments/orchestration/proto/ui/instrumentcreator/Api$InitializeResponse;->initialPage:Lcom/google/commerce/payments/orchestration/proto/ui/instrumentcreator/Api$Page;

    .line 155
    const/4 v0, -0x1

    iput v0, p0, Lcom/google/commerce/payments/orchestration/proto/ui/instrumentcreator/Api$InitializeResponse;->cachedSize:I

    .line 156
    return-object p0
.end method

.method protected computeSerializedSize()I
    .locals 3

    .prologue
    .line 176
    invoke-super {p0}, Lcom/google/protobuf/nano/MessageNano;->computeSerializedSize()I

    move-result v0

    .line 177
    .local v0, "size":I
    iget-object v1, p0, Lcom/google/commerce/payments/orchestration/proto/ui/instrumentcreator/Api$InitializeResponse;->initialPage:Lcom/google/commerce/payments/orchestration/proto/ui/instrumentcreator/Api$Page;

    if-eqz v1, :cond_0

    .line 178
    const/4 v1, 0x4

    iget-object v2, p0, Lcom/google/commerce/payments/orchestration/proto/ui/instrumentcreator/Api$InitializeResponse;->initialPage:Lcom/google/commerce/payments/orchestration/proto/ui/instrumentcreator/Api$Page;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeMessageSize(ILcom/google/protobuf/nano/MessageNano;)I

    move-result v1

    add-int/2addr v0, v1

    .line 181
    :cond_0
    iget-object v1, p0, Lcom/google/commerce/payments/orchestration/proto/ui/instrumentcreator/Api$InitializeResponse;->error:Lcom/google/commerce/payments/orchestration/proto/ui/common/UiErrorOuterClass$UiError;

    if-eqz v1, :cond_1

    .line 182
    const/4 v1, 0x5

    iget-object v2, p0, Lcom/google/commerce/payments/orchestration/proto/ui/instrumentcreator/Api$InitializeResponse;->error:Lcom/google/commerce/payments/orchestration/proto/ui/common/UiErrorOuterClass$UiError;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeMessageSize(ILcom/google/protobuf/nano/MessageNano;)I

    move-result v1

    add-int/2addr v0, v1

    .line 185
    :cond_1
    iget-object v1, p0, Lcom/google/commerce/payments/orchestration/proto/ui/instrumentcreator/Api$InitializeResponse;->context:Lcom/google/commerce/payments/orchestration/proto/ui/common/ResponseContextOuterClass$ResponseContext;

    if-eqz v1, :cond_2

    .line 186
    const/4 v1, 0x6

    iget-object v2, p0, Lcom/google/commerce/payments/orchestration/proto/ui/instrumentcreator/Api$InitializeResponse;->context:Lcom/google/commerce/payments/orchestration/proto/ui/common/ResponseContextOuterClass$ResponseContext;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeMessageSize(ILcom/google/protobuf/nano/MessageNano;)I

    move-result v1

    add-int/2addr v0, v1

    .line 189
    :cond_2
    return v0
.end method

.method public mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/commerce/payments/orchestration/proto/ui/instrumentcreator/Api$InitializeResponse;
    .locals 2
    .param p1, "input"    # Lcom/google/protobuf/nano/CodedInputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 197
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readTag()I

    move-result v0

    .line 198
    .local v0, "tag":I
    sparse-switch v0, :sswitch_data_0

    .line 202
    invoke-static {p1, v0}, Lcom/google/protobuf/nano/WireFormatNano;->parseUnknownField(Lcom/google/protobuf/nano/CodedInputByteBufferNano;I)Z

    move-result v1

    if-nez v1, :cond_0

    .line 203
    :sswitch_0
    return-object p0

    .line 208
    :sswitch_1
    iget-object v1, p0, Lcom/google/commerce/payments/orchestration/proto/ui/instrumentcreator/Api$InitializeResponse;->initialPage:Lcom/google/commerce/payments/orchestration/proto/ui/instrumentcreator/Api$Page;

    if-nez v1, :cond_1

    .line 209
    new-instance v1, Lcom/google/commerce/payments/orchestration/proto/ui/instrumentcreator/Api$Page;

    invoke-direct {v1}, Lcom/google/commerce/payments/orchestration/proto/ui/instrumentcreator/Api$Page;-><init>()V

    iput-object v1, p0, Lcom/google/commerce/payments/orchestration/proto/ui/instrumentcreator/Api$InitializeResponse;->initialPage:Lcom/google/commerce/payments/orchestration/proto/ui/instrumentcreator/Api$Page;

    .line 211
    :cond_1
    iget-object v1, p0, Lcom/google/commerce/payments/orchestration/proto/ui/instrumentcreator/Api$InitializeResponse;->initialPage:Lcom/google/commerce/payments/orchestration/proto/ui/instrumentcreator/Api$Page;

    invoke-virtual {p1, v1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readMessage(Lcom/google/protobuf/nano/MessageNano;)V

    goto :goto_0

    .line 215
    :sswitch_2
    iget-object v1, p0, Lcom/google/commerce/payments/orchestration/proto/ui/instrumentcreator/Api$InitializeResponse;->error:Lcom/google/commerce/payments/orchestration/proto/ui/common/UiErrorOuterClass$UiError;

    if-nez v1, :cond_2

    .line 216
    new-instance v1, Lcom/google/commerce/payments/orchestration/proto/ui/common/UiErrorOuterClass$UiError;

    invoke-direct {v1}, Lcom/google/commerce/payments/orchestration/proto/ui/common/UiErrorOuterClass$UiError;-><init>()V

    iput-object v1, p0, Lcom/google/commerce/payments/orchestration/proto/ui/instrumentcreator/Api$InitializeResponse;->error:Lcom/google/commerce/payments/orchestration/proto/ui/common/UiErrorOuterClass$UiError;

    .line 218
    :cond_2
    iget-object v1, p0, Lcom/google/commerce/payments/orchestration/proto/ui/instrumentcreator/Api$InitializeResponse;->error:Lcom/google/commerce/payments/orchestration/proto/ui/common/UiErrorOuterClass$UiError;

    invoke-virtual {p1, v1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readMessage(Lcom/google/protobuf/nano/MessageNano;)V

    goto :goto_0

    .line 222
    :sswitch_3
    iget-object v1, p0, Lcom/google/commerce/payments/orchestration/proto/ui/instrumentcreator/Api$InitializeResponse;->context:Lcom/google/commerce/payments/orchestration/proto/ui/common/ResponseContextOuterClass$ResponseContext;

    if-nez v1, :cond_3

    .line 223
    new-instance v1, Lcom/google/commerce/payments/orchestration/proto/ui/common/ResponseContextOuterClass$ResponseContext;

    invoke-direct {v1}, Lcom/google/commerce/payments/orchestration/proto/ui/common/ResponseContextOuterClass$ResponseContext;-><init>()V

    iput-object v1, p0, Lcom/google/commerce/payments/orchestration/proto/ui/instrumentcreator/Api$InitializeResponse;->context:Lcom/google/commerce/payments/orchestration/proto/ui/common/ResponseContextOuterClass$ResponseContext;

    .line 225
    :cond_3
    iget-object v1, p0, Lcom/google/commerce/payments/orchestration/proto/ui/instrumentcreator/Api$InitializeResponse;->context:Lcom/google/commerce/payments/orchestration/proto/ui/common/ResponseContextOuterClass$ResponseContext;

    invoke-virtual {p1, v1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readMessage(Lcom/google/protobuf/nano/MessageNano;)V

    goto :goto_0

    .line 198
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x22 -> :sswitch_1
        0x2a -> :sswitch_2
        0x32 -> :sswitch_3
    .end sparse-switch
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/protobuf/nano/MessageNano;
    .locals 1
    .param p1, "x0"    # Lcom/google/protobuf/nano/CodedInputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 121
    invoke-virtual {p0, p1}, Lcom/google/commerce/payments/orchestration/proto/ui/instrumentcreator/Api$InitializeResponse;->mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/commerce/payments/orchestration/proto/ui/instrumentcreator/Api$InitializeResponse;

    move-result-object v0

    return-object v0
.end method

.method public writeTo(Lcom/google/protobuf/nano/CodedOutputByteBufferNano;)V
    .locals 2
    .param p1, "output"    # Lcom/google/protobuf/nano/CodedOutputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 162
    iget-object v0, p0, Lcom/google/commerce/payments/orchestration/proto/ui/instrumentcreator/Api$InitializeResponse;->initialPage:Lcom/google/commerce/payments/orchestration/proto/ui/instrumentcreator/Api$Page;

    if-eqz v0, :cond_0

    .line 163
    const/4 v0, 0x4

    iget-object v1, p0, Lcom/google/commerce/payments/orchestration/proto/ui/instrumentcreator/Api$InitializeResponse;->initialPage:Lcom/google/commerce/payments/orchestration/proto/ui/instrumentcreator/Api$Page;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeMessage(ILcom/google/protobuf/nano/MessageNano;)V

    .line 165
    :cond_0
    iget-object v0, p0, Lcom/google/commerce/payments/orchestration/proto/ui/instrumentcreator/Api$InitializeResponse;->error:Lcom/google/commerce/payments/orchestration/proto/ui/common/UiErrorOuterClass$UiError;

    if-eqz v0, :cond_1

    .line 166
    const/4 v0, 0x5

    iget-object v1, p0, Lcom/google/commerce/payments/orchestration/proto/ui/instrumentcreator/Api$InitializeResponse;->error:Lcom/google/commerce/payments/orchestration/proto/ui/common/UiErrorOuterClass$UiError;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeMessage(ILcom/google/protobuf/nano/MessageNano;)V

    .line 168
    :cond_1
    iget-object v0, p0, Lcom/google/commerce/payments/orchestration/proto/ui/instrumentcreator/Api$InitializeResponse;->context:Lcom/google/commerce/payments/orchestration/proto/ui/common/ResponseContextOuterClass$ResponseContext;

    if-eqz v0, :cond_2

    .line 169
    const/4 v0, 0x6

    iget-object v1, p0, Lcom/google/commerce/payments/orchestration/proto/ui/instrumentcreator/Api$InitializeResponse;->context:Lcom/google/commerce/payments/orchestration/proto/ui/common/ResponseContextOuterClass$ResponseContext;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeMessage(ILcom/google/protobuf/nano/MessageNano;)V

    .line 171
    :cond_2
    invoke-super {p0, p1}, Lcom/google/protobuf/nano/MessageNano;->writeTo(Lcom/google/protobuf/nano/CodedOutputByteBufferNano;)V

    .line 172
    return-void
.end method
