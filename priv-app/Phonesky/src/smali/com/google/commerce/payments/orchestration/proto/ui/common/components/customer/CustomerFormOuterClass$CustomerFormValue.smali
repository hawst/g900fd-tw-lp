.class public final Lcom/google/commerce/payments/orchestration/proto/ui/common/components/customer/CustomerFormOuterClass$CustomerFormValue;
.super Lcom/google/protobuf/nano/MessageNano;
.source "CustomerFormOuterClass.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/commerce/payments/orchestration/proto/ui/common/components/customer/CustomerFormOuterClass;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "CustomerFormValue"
.end annotation


# instance fields
.field public instrument:Lcom/google/commerce/payments/orchestration/proto/ui/common/components/instrument/InstrumentFormOuterClass$InstrumentFormValue;

.field public legalAddress:Lcom/google/commerce/payments/orchestration/proto/ui/common/components/AddressFormOuterClass$AddressFormValue;

.field public legalCountryCode:Ljava/lang/String;

.field public legalDocData:Ljava/lang/String;

.field public taxInfo:Lcom/google/commerce/payments/orchestration/proto/ui/common/components/tax/TaxInfoFormOuterClass$TaxInfoFormValue;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 256
    invoke-direct {p0}, Lcom/google/protobuf/nano/MessageNano;-><init>()V

    .line 257
    invoke-virtual {p0}, Lcom/google/commerce/payments/orchestration/proto/ui/common/components/customer/CustomerFormOuterClass$CustomerFormValue;->clear()Lcom/google/commerce/payments/orchestration/proto/ui/common/components/customer/CustomerFormOuterClass$CustomerFormValue;

    .line 258
    return-void
.end method


# virtual methods
.method public clear()Lcom/google/commerce/payments/orchestration/proto/ui/common/components/customer/CustomerFormOuterClass$CustomerFormValue;
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 261
    iput-object v1, p0, Lcom/google/commerce/payments/orchestration/proto/ui/common/components/customer/CustomerFormOuterClass$CustomerFormValue;->instrument:Lcom/google/commerce/payments/orchestration/proto/ui/common/components/instrument/InstrumentFormOuterClass$InstrumentFormValue;

    .line 262
    iput-object v1, p0, Lcom/google/commerce/payments/orchestration/proto/ui/common/components/customer/CustomerFormOuterClass$CustomerFormValue;->legalAddress:Lcom/google/commerce/payments/orchestration/proto/ui/common/components/AddressFormOuterClass$AddressFormValue;

    .line 263
    const-string v0, ""

    iput-object v0, p0, Lcom/google/commerce/payments/orchestration/proto/ui/common/components/customer/CustomerFormOuterClass$CustomerFormValue;->legalDocData:Ljava/lang/String;

    .line 264
    const-string v0, ""

    iput-object v0, p0, Lcom/google/commerce/payments/orchestration/proto/ui/common/components/customer/CustomerFormOuterClass$CustomerFormValue;->legalCountryCode:Ljava/lang/String;

    .line 265
    iput-object v1, p0, Lcom/google/commerce/payments/orchestration/proto/ui/common/components/customer/CustomerFormOuterClass$CustomerFormValue;->taxInfo:Lcom/google/commerce/payments/orchestration/proto/ui/common/components/tax/TaxInfoFormOuterClass$TaxInfoFormValue;

    .line 266
    const/4 v0, -0x1

    iput v0, p0, Lcom/google/commerce/payments/orchestration/proto/ui/common/components/customer/CustomerFormOuterClass$CustomerFormValue;->cachedSize:I

    .line 267
    return-object p0
.end method

.method protected computeSerializedSize()I
    .locals 3

    .prologue
    .line 293
    invoke-super {p0}, Lcom/google/protobuf/nano/MessageNano;->computeSerializedSize()I

    move-result v0

    .line 294
    .local v0, "size":I
    iget-object v1, p0, Lcom/google/commerce/payments/orchestration/proto/ui/common/components/customer/CustomerFormOuterClass$CustomerFormValue;->instrument:Lcom/google/commerce/payments/orchestration/proto/ui/common/components/instrument/InstrumentFormOuterClass$InstrumentFormValue;

    if-eqz v1, :cond_0

    .line 295
    const/4 v1, 0x1

    iget-object v2, p0, Lcom/google/commerce/payments/orchestration/proto/ui/common/components/customer/CustomerFormOuterClass$CustomerFormValue;->instrument:Lcom/google/commerce/payments/orchestration/proto/ui/common/components/instrument/InstrumentFormOuterClass$InstrumentFormValue;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeMessageSize(ILcom/google/protobuf/nano/MessageNano;)I

    move-result v1

    add-int/2addr v0, v1

    .line 298
    :cond_0
    iget-object v1, p0, Lcom/google/commerce/payments/orchestration/proto/ui/common/components/customer/CustomerFormOuterClass$CustomerFormValue;->legalDocData:Ljava/lang/String;

    const-string v2, ""

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_1

    .line 299
    const/4 v1, 0x3

    iget-object v2, p0, Lcom/google/commerce/payments/orchestration/proto/ui/common/components/customer/CustomerFormOuterClass$CustomerFormValue;->legalDocData:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeStringSize(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 302
    :cond_1
    iget-object v1, p0, Lcom/google/commerce/payments/orchestration/proto/ui/common/components/customer/CustomerFormOuterClass$CustomerFormValue;->legalAddress:Lcom/google/commerce/payments/orchestration/proto/ui/common/components/AddressFormOuterClass$AddressFormValue;

    if-eqz v1, :cond_2

    .line 303
    const/4 v1, 0x4

    iget-object v2, p0, Lcom/google/commerce/payments/orchestration/proto/ui/common/components/customer/CustomerFormOuterClass$CustomerFormValue;->legalAddress:Lcom/google/commerce/payments/orchestration/proto/ui/common/components/AddressFormOuterClass$AddressFormValue;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeMessageSize(ILcom/google/protobuf/nano/MessageNano;)I

    move-result v1

    add-int/2addr v0, v1

    .line 306
    :cond_2
    iget-object v1, p0, Lcom/google/commerce/payments/orchestration/proto/ui/common/components/customer/CustomerFormOuterClass$CustomerFormValue;->legalCountryCode:Ljava/lang/String;

    const-string v2, ""

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_3

    .line 307
    const/4 v1, 0x5

    iget-object v2, p0, Lcom/google/commerce/payments/orchestration/proto/ui/common/components/customer/CustomerFormOuterClass$CustomerFormValue;->legalCountryCode:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeStringSize(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 310
    :cond_3
    iget-object v1, p0, Lcom/google/commerce/payments/orchestration/proto/ui/common/components/customer/CustomerFormOuterClass$CustomerFormValue;->taxInfo:Lcom/google/commerce/payments/orchestration/proto/ui/common/components/tax/TaxInfoFormOuterClass$TaxInfoFormValue;

    if-eqz v1, :cond_4

    .line 311
    const/4 v1, 0x6

    iget-object v2, p0, Lcom/google/commerce/payments/orchestration/proto/ui/common/components/customer/CustomerFormOuterClass$CustomerFormValue;->taxInfo:Lcom/google/commerce/payments/orchestration/proto/ui/common/components/tax/TaxInfoFormOuterClass$TaxInfoFormValue;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeMessageSize(ILcom/google/protobuf/nano/MessageNano;)I

    move-result v1

    add-int/2addr v0, v1

    .line 314
    :cond_4
    return v0
.end method

.method public mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/commerce/payments/orchestration/proto/ui/common/components/customer/CustomerFormOuterClass$CustomerFormValue;
    .locals 2
    .param p1, "input"    # Lcom/google/protobuf/nano/CodedInputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 322
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readTag()I

    move-result v0

    .line 323
    .local v0, "tag":I
    sparse-switch v0, :sswitch_data_0

    .line 327
    invoke-static {p1, v0}, Lcom/google/protobuf/nano/WireFormatNano;->parseUnknownField(Lcom/google/protobuf/nano/CodedInputByteBufferNano;I)Z

    move-result v1

    if-nez v1, :cond_0

    .line 328
    :sswitch_0
    return-object p0

    .line 333
    :sswitch_1
    iget-object v1, p0, Lcom/google/commerce/payments/orchestration/proto/ui/common/components/customer/CustomerFormOuterClass$CustomerFormValue;->instrument:Lcom/google/commerce/payments/orchestration/proto/ui/common/components/instrument/InstrumentFormOuterClass$InstrumentFormValue;

    if-nez v1, :cond_1

    .line 334
    new-instance v1, Lcom/google/commerce/payments/orchestration/proto/ui/common/components/instrument/InstrumentFormOuterClass$InstrumentFormValue;

    invoke-direct {v1}, Lcom/google/commerce/payments/orchestration/proto/ui/common/components/instrument/InstrumentFormOuterClass$InstrumentFormValue;-><init>()V

    iput-object v1, p0, Lcom/google/commerce/payments/orchestration/proto/ui/common/components/customer/CustomerFormOuterClass$CustomerFormValue;->instrument:Lcom/google/commerce/payments/orchestration/proto/ui/common/components/instrument/InstrumentFormOuterClass$InstrumentFormValue;

    .line 336
    :cond_1
    iget-object v1, p0, Lcom/google/commerce/payments/orchestration/proto/ui/common/components/customer/CustomerFormOuterClass$CustomerFormValue;->instrument:Lcom/google/commerce/payments/orchestration/proto/ui/common/components/instrument/InstrumentFormOuterClass$InstrumentFormValue;

    invoke-virtual {p1, v1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readMessage(Lcom/google/protobuf/nano/MessageNano;)V

    goto :goto_0

    .line 340
    :sswitch_2
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readString()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/google/commerce/payments/orchestration/proto/ui/common/components/customer/CustomerFormOuterClass$CustomerFormValue;->legalDocData:Ljava/lang/String;

    goto :goto_0

    .line 344
    :sswitch_3
    iget-object v1, p0, Lcom/google/commerce/payments/orchestration/proto/ui/common/components/customer/CustomerFormOuterClass$CustomerFormValue;->legalAddress:Lcom/google/commerce/payments/orchestration/proto/ui/common/components/AddressFormOuterClass$AddressFormValue;

    if-nez v1, :cond_2

    .line 345
    new-instance v1, Lcom/google/commerce/payments/orchestration/proto/ui/common/components/AddressFormOuterClass$AddressFormValue;

    invoke-direct {v1}, Lcom/google/commerce/payments/orchestration/proto/ui/common/components/AddressFormOuterClass$AddressFormValue;-><init>()V

    iput-object v1, p0, Lcom/google/commerce/payments/orchestration/proto/ui/common/components/customer/CustomerFormOuterClass$CustomerFormValue;->legalAddress:Lcom/google/commerce/payments/orchestration/proto/ui/common/components/AddressFormOuterClass$AddressFormValue;

    .line 347
    :cond_2
    iget-object v1, p0, Lcom/google/commerce/payments/orchestration/proto/ui/common/components/customer/CustomerFormOuterClass$CustomerFormValue;->legalAddress:Lcom/google/commerce/payments/orchestration/proto/ui/common/components/AddressFormOuterClass$AddressFormValue;

    invoke-virtual {p1, v1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readMessage(Lcom/google/protobuf/nano/MessageNano;)V

    goto :goto_0

    .line 351
    :sswitch_4
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readString()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/google/commerce/payments/orchestration/proto/ui/common/components/customer/CustomerFormOuterClass$CustomerFormValue;->legalCountryCode:Ljava/lang/String;

    goto :goto_0

    .line 355
    :sswitch_5
    iget-object v1, p0, Lcom/google/commerce/payments/orchestration/proto/ui/common/components/customer/CustomerFormOuterClass$CustomerFormValue;->taxInfo:Lcom/google/commerce/payments/orchestration/proto/ui/common/components/tax/TaxInfoFormOuterClass$TaxInfoFormValue;

    if-nez v1, :cond_3

    .line 356
    new-instance v1, Lcom/google/commerce/payments/orchestration/proto/ui/common/components/tax/TaxInfoFormOuterClass$TaxInfoFormValue;

    invoke-direct {v1}, Lcom/google/commerce/payments/orchestration/proto/ui/common/components/tax/TaxInfoFormOuterClass$TaxInfoFormValue;-><init>()V

    iput-object v1, p0, Lcom/google/commerce/payments/orchestration/proto/ui/common/components/customer/CustomerFormOuterClass$CustomerFormValue;->taxInfo:Lcom/google/commerce/payments/orchestration/proto/ui/common/components/tax/TaxInfoFormOuterClass$TaxInfoFormValue;

    .line 358
    :cond_3
    iget-object v1, p0, Lcom/google/commerce/payments/orchestration/proto/ui/common/components/customer/CustomerFormOuterClass$CustomerFormValue;->taxInfo:Lcom/google/commerce/payments/orchestration/proto/ui/common/components/tax/TaxInfoFormOuterClass$TaxInfoFormValue;

    invoke-virtual {p1, v1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readMessage(Lcom/google/protobuf/nano/MessageNano;)V

    goto :goto_0

    .line 323
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x1a -> :sswitch_2
        0x22 -> :sswitch_3
        0x2a -> :sswitch_4
        0x32 -> :sswitch_5
    .end sparse-switch
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/protobuf/nano/MessageNano;
    .locals 1
    .param p1, "x0"    # Lcom/google/protobuf/nano/CodedInputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 224
    invoke-virtual {p0, p1}, Lcom/google/commerce/payments/orchestration/proto/ui/common/components/customer/CustomerFormOuterClass$CustomerFormValue;->mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/commerce/payments/orchestration/proto/ui/common/components/customer/CustomerFormOuterClass$CustomerFormValue;

    move-result-object v0

    return-object v0
.end method

.method public writeTo(Lcom/google/protobuf/nano/CodedOutputByteBufferNano;)V
    .locals 2
    .param p1, "output"    # Lcom/google/protobuf/nano/CodedOutputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 273
    iget-object v0, p0, Lcom/google/commerce/payments/orchestration/proto/ui/common/components/customer/CustomerFormOuterClass$CustomerFormValue;->instrument:Lcom/google/commerce/payments/orchestration/proto/ui/common/components/instrument/InstrumentFormOuterClass$InstrumentFormValue;

    if-eqz v0, :cond_0

    .line 274
    const/4 v0, 0x1

    iget-object v1, p0, Lcom/google/commerce/payments/orchestration/proto/ui/common/components/customer/CustomerFormOuterClass$CustomerFormValue;->instrument:Lcom/google/commerce/payments/orchestration/proto/ui/common/components/instrument/InstrumentFormOuterClass$InstrumentFormValue;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeMessage(ILcom/google/protobuf/nano/MessageNano;)V

    .line 276
    :cond_0
    iget-object v0, p0, Lcom/google/commerce/payments/orchestration/proto/ui/common/components/customer/CustomerFormOuterClass$CustomerFormValue;->legalDocData:Ljava/lang/String;

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 277
    const/4 v0, 0x3

    iget-object v1, p0, Lcom/google/commerce/payments/orchestration/proto/ui/common/components/customer/CustomerFormOuterClass$CustomerFormValue;->legalDocData:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeString(ILjava/lang/String;)V

    .line 279
    :cond_1
    iget-object v0, p0, Lcom/google/commerce/payments/orchestration/proto/ui/common/components/customer/CustomerFormOuterClass$CustomerFormValue;->legalAddress:Lcom/google/commerce/payments/orchestration/proto/ui/common/components/AddressFormOuterClass$AddressFormValue;

    if-eqz v0, :cond_2

    .line 280
    const/4 v0, 0x4

    iget-object v1, p0, Lcom/google/commerce/payments/orchestration/proto/ui/common/components/customer/CustomerFormOuterClass$CustomerFormValue;->legalAddress:Lcom/google/commerce/payments/orchestration/proto/ui/common/components/AddressFormOuterClass$AddressFormValue;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeMessage(ILcom/google/protobuf/nano/MessageNano;)V

    .line 282
    :cond_2
    iget-object v0, p0, Lcom/google/commerce/payments/orchestration/proto/ui/common/components/customer/CustomerFormOuterClass$CustomerFormValue;->legalCountryCode:Ljava/lang/String;

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_3

    .line 283
    const/4 v0, 0x5

    iget-object v1, p0, Lcom/google/commerce/payments/orchestration/proto/ui/common/components/customer/CustomerFormOuterClass$CustomerFormValue;->legalCountryCode:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeString(ILjava/lang/String;)V

    .line 285
    :cond_3
    iget-object v0, p0, Lcom/google/commerce/payments/orchestration/proto/ui/common/components/customer/CustomerFormOuterClass$CustomerFormValue;->taxInfo:Lcom/google/commerce/payments/orchestration/proto/ui/common/components/tax/TaxInfoFormOuterClass$TaxInfoFormValue;

    if-eqz v0, :cond_4

    .line 286
    const/4 v0, 0x6

    iget-object v1, p0, Lcom/google/commerce/payments/orchestration/proto/ui/common/components/customer/CustomerFormOuterClass$CustomerFormValue;->taxInfo:Lcom/google/commerce/payments/orchestration/proto/ui/common/components/tax/TaxInfoFormOuterClass$TaxInfoFormValue;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeMessage(ILcom/google/protobuf/nano/MessageNano;)V

    .line 288
    :cond_4
    invoke-super {p0, p1}, Lcom/google/protobuf/nano/MessageNano;->writeTo(Lcom/google/protobuf/nano/CodedOutputByteBufferNano;)V

    .line 289
    return-void
.end method
