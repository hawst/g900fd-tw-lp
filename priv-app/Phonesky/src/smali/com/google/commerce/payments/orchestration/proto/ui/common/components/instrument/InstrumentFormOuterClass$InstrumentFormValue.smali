.class public final Lcom/google/commerce/payments/orchestration/proto/ui/common/components/instrument/InstrumentFormOuterClass$InstrumentFormValue;
.super Lcom/google/protobuf/nano/MessageNano;
.source "InstrumentFormOuterClass.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/commerce/payments/orchestration/proto/ui/common/components/instrument/InstrumentFormOuterClass;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "InstrumentFormValue"
.end annotation


# instance fields
.field public creditCard:Lcom/google/commerce/payments/orchestration/proto/ui/common/components/instrument/types/CreditCard$CreditCardFormValue;

.field public usernamePassword:Lcom/google/commerce/payments/orchestration/proto/ui/common/components/instrument/types/UsernamePassword$UsernamePasswordFormValue;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 136
    invoke-direct {p0}, Lcom/google/protobuf/nano/MessageNano;-><init>()V

    .line 137
    invoke-virtual {p0}, Lcom/google/commerce/payments/orchestration/proto/ui/common/components/instrument/InstrumentFormOuterClass$InstrumentFormValue;->clear()Lcom/google/commerce/payments/orchestration/proto/ui/common/components/instrument/InstrumentFormOuterClass$InstrumentFormValue;

    .line 138
    return-void
.end method


# virtual methods
.method public clear()Lcom/google/commerce/payments/orchestration/proto/ui/common/components/instrument/InstrumentFormOuterClass$InstrumentFormValue;
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 141
    iput-object v0, p0, Lcom/google/commerce/payments/orchestration/proto/ui/common/components/instrument/InstrumentFormOuterClass$InstrumentFormValue;->creditCard:Lcom/google/commerce/payments/orchestration/proto/ui/common/components/instrument/types/CreditCard$CreditCardFormValue;

    .line 142
    iput-object v0, p0, Lcom/google/commerce/payments/orchestration/proto/ui/common/components/instrument/InstrumentFormOuterClass$InstrumentFormValue;->usernamePassword:Lcom/google/commerce/payments/orchestration/proto/ui/common/components/instrument/types/UsernamePassword$UsernamePasswordFormValue;

    .line 143
    const/4 v0, -0x1

    iput v0, p0, Lcom/google/commerce/payments/orchestration/proto/ui/common/components/instrument/InstrumentFormOuterClass$InstrumentFormValue;->cachedSize:I

    .line 144
    return-object p0
.end method

.method protected computeSerializedSize()I
    .locals 3

    .prologue
    .line 161
    invoke-super {p0}, Lcom/google/protobuf/nano/MessageNano;->computeSerializedSize()I

    move-result v0

    .line 162
    .local v0, "size":I
    iget-object v1, p0, Lcom/google/commerce/payments/orchestration/proto/ui/common/components/instrument/InstrumentFormOuterClass$InstrumentFormValue;->creditCard:Lcom/google/commerce/payments/orchestration/proto/ui/common/components/instrument/types/CreditCard$CreditCardFormValue;

    if-eqz v1, :cond_0

    .line 163
    const/4 v1, 0x1

    iget-object v2, p0, Lcom/google/commerce/payments/orchestration/proto/ui/common/components/instrument/InstrumentFormOuterClass$InstrumentFormValue;->creditCard:Lcom/google/commerce/payments/orchestration/proto/ui/common/components/instrument/types/CreditCard$CreditCardFormValue;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeMessageSize(ILcom/google/protobuf/nano/MessageNano;)I

    move-result v1

    add-int/2addr v0, v1

    .line 166
    :cond_0
    iget-object v1, p0, Lcom/google/commerce/payments/orchestration/proto/ui/common/components/instrument/InstrumentFormOuterClass$InstrumentFormValue;->usernamePassword:Lcom/google/commerce/payments/orchestration/proto/ui/common/components/instrument/types/UsernamePassword$UsernamePasswordFormValue;

    if-eqz v1, :cond_1

    .line 167
    const/4 v1, 0x6

    iget-object v2, p0, Lcom/google/commerce/payments/orchestration/proto/ui/common/components/instrument/InstrumentFormOuterClass$InstrumentFormValue;->usernamePassword:Lcom/google/commerce/payments/orchestration/proto/ui/common/components/instrument/types/UsernamePassword$UsernamePasswordFormValue;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeMessageSize(ILcom/google/protobuf/nano/MessageNano;)I

    move-result v1

    add-int/2addr v0, v1

    .line 170
    :cond_1
    return v0
.end method

.method public mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/commerce/payments/orchestration/proto/ui/common/components/instrument/InstrumentFormOuterClass$InstrumentFormValue;
    .locals 2
    .param p1, "input"    # Lcom/google/protobuf/nano/CodedInputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 178
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readTag()I

    move-result v0

    .line 179
    .local v0, "tag":I
    sparse-switch v0, :sswitch_data_0

    .line 183
    invoke-static {p1, v0}, Lcom/google/protobuf/nano/WireFormatNano;->parseUnknownField(Lcom/google/protobuf/nano/CodedInputByteBufferNano;I)Z

    move-result v1

    if-nez v1, :cond_0

    .line 184
    :sswitch_0
    return-object p0

    .line 189
    :sswitch_1
    iget-object v1, p0, Lcom/google/commerce/payments/orchestration/proto/ui/common/components/instrument/InstrumentFormOuterClass$InstrumentFormValue;->creditCard:Lcom/google/commerce/payments/orchestration/proto/ui/common/components/instrument/types/CreditCard$CreditCardFormValue;

    if-nez v1, :cond_1

    .line 190
    new-instance v1, Lcom/google/commerce/payments/orchestration/proto/ui/common/components/instrument/types/CreditCard$CreditCardFormValue;

    invoke-direct {v1}, Lcom/google/commerce/payments/orchestration/proto/ui/common/components/instrument/types/CreditCard$CreditCardFormValue;-><init>()V

    iput-object v1, p0, Lcom/google/commerce/payments/orchestration/proto/ui/common/components/instrument/InstrumentFormOuterClass$InstrumentFormValue;->creditCard:Lcom/google/commerce/payments/orchestration/proto/ui/common/components/instrument/types/CreditCard$CreditCardFormValue;

    .line 192
    :cond_1
    iget-object v1, p0, Lcom/google/commerce/payments/orchestration/proto/ui/common/components/instrument/InstrumentFormOuterClass$InstrumentFormValue;->creditCard:Lcom/google/commerce/payments/orchestration/proto/ui/common/components/instrument/types/CreditCard$CreditCardFormValue;

    invoke-virtual {p1, v1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readMessage(Lcom/google/protobuf/nano/MessageNano;)V

    goto :goto_0

    .line 196
    :sswitch_2
    iget-object v1, p0, Lcom/google/commerce/payments/orchestration/proto/ui/common/components/instrument/InstrumentFormOuterClass$InstrumentFormValue;->usernamePassword:Lcom/google/commerce/payments/orchestration/proto/ui/common/components/instrument/types/UsernamePassword$UsernamePasswordFormValue;

    if-nez v1, :cond_2

    .line 197
    new-instance v1, Lcom/google/commerce/payments/orchestration/proto/ui/common/components/instrument/types/UsernamePassword$UsernamePasswordFormValue;

    invoke-direct {v1}, Lcom/google/commerce/payments/orchestration/proto/ui/common/components/instrument/types/UsernamePassword$UsernamePasswordFormValue;-><init>()V

    iput-object v1, p0, Lcom/google/commerce/payments/orchestration/proto/ui/common/components/instrument/InstrumentFormOuterClass$InstrumentFormValue;->usernamePassword:Lcom/google/commerce/payments/orchestration/proto/ui/common/components/instrument/types/UsernamePassword$UsernamePasswordFormValue;

    .line 199
    :cond_2
    iget-object v1, p0, Lcom/google/commerce/payments/orchestration/proto/ui/common/components/instrument/InstrumentFormOuterClass$InstrumentFormValue;->usernamePassword:Lcom/google/commerce/payments/orchestration/proto/ui/common/components/instrument/types/UsernamePassword$UsernamePasswordFormValue;

    invoke-virtual {p1, v1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readMessage(Lcom/google/protobuf/nano/MessageNano;)V

    goto :goto_0

    .line 179
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x32 -> :sswitch_2
    .end sparse-switch
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/protobuf/nano/MessageNano;
    .locals 1
    .param p1, "x0"    # Lcom/google/protobuf/nano/CodedInputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 113
    invoke-virtual {p0, p1}, Lcom/google/commerce/payments/orchestration/proto/ui/common/components/instrument/InstrumentFormOuterClass$InstrumentFormValue;->mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/commerce/payments/orchestration/proto/ui/common/components/instrument/InstrumentFormOuterClass$InstrumentFormValue;

    move-result-object v0

    return-object v0
.end method

.method public writeTo(Lcom/google/protobuf/nano/CodedOutputByteBufferNano;)V
    .locals 2
    .param p1, "output"    # Lcom/google/protobuf/nano/CodedOutputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 150
    iget-object v0, p0, Lcom/google/commerce/payments/orchestration/proto/ui/common/components/instrument/InstrumentFormOuterClass$InstrumentFormValue;->creditCard:Lcom/google/commerce/payments/orchestration/proto/ui/common/components/instrument/types/CreditCard$CreditCardFormValue;

    if-eqz v0, :cond_0

    .line 151
    const/4 v0, 0x1

    iget-object v1, p0, Lcom/google/commerce/payments/orchestration/proto/ui/common/components/instrument/InstrumentFormOuterClass$InstrumentFormValue;->creditCard:Lcom/google/commerce/payments/orchestration/proto/ui/common/components/instrument/types/CreditCard$CreditCardFormValue;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeMessage(ILcom/google/protobuf/nano/MessageNano;)V

    .line 153
    :cond_0
    iget-object v0, p0, Lcom/google/commerce/payments/orchestration/proto/ui/common/components/instrument/InstrumentFormOuterClass$InstrumentFormValue;->usernamePassword:Lcom/google/commerce/payments/orchestration/proto/ui/common/components/instrument/types/UsernamePassword$UsernamePasswordFormValue;

    if-eqz v0, :cond_1

    .line 154
    const/4 v0, 0x6

    iget-object v1, p0, Lcom/google/commerce/payments/orchestration/proto/ui/common/components/instrument/InstrumentFormOuterClass$InstrumentFormValue;->usernamePassword:Lcom/google/commerce/payments/orchestration/proto/ui/common/components/instrument/types/UsernamePassword$UsernamePasswordFormValue;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeMessage(ILcom/google/protobuf/nano/MessageNano;)V

    .line 156
    :cond_1
    invoke-super {p0, p1}, Lcom/google/protobuf/nano/MessageNano;->writeTo(Lcom/google/protobuf/nano/CodedOutputByteBufferNano;)V

    .line 157
    return-void
.end method
