.class public final Lcom/google/commerce/payments/orchestration/proto/ui/common/RequestContextOuterClass$RequestContext;
.super Lcom/google/protobuf/nano/MessageNano;
.source "RequestContextOuterClass.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/commerce/payments/orchestration/proto/ui/common/RequestContextOuterClass;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "RequestContext"
.end annotation


# instance fields
.field public clientType:I

.field public clientVersion:J

.field public languageCode:Ljava/lang/String;

.field public nativeContext:Lcom/google/commerce/payments/orchestration/proto/common/ContextOuterClass$NativeClientContext;

.field public sessionData:[B


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 46
    invoke-direct {p0}, Lcom/google/protobuf/nano/MessageNano;-><init>()V

    .line 47
    invoke-virtual {p0}, Lcom/google/commerce/payments/orchestration/proto/ui/common/RequestContextOuterClass$RequestContext;->clear()Lcom/google/commerce/payments/orchestration/proto/ui/common/RequestContextOuterClass$RequestContext;

    .line 48
    return-void
.end method


# virtual methods
.method public clear()Lcom/google/commerce/payments/orchestration/proto/ui/common/RequestContextOuterClass$RequestContext;
    .locals 2

    .prologue
    .line 51
    sget-object v0, Lcom/google/protobuf/nano/WireFormatNano;->EMPTY_BYTES:[B

    iput-object v0, p0, Lcom/google/commerce/payments/orchestration/proto/ui/common/RequestContextOuterClass$RequestContext;->sessionData:[B

    .line 52
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/commerce/payments/orchestration/proto/ui/common/RequestContextOuterClass$RequestContext;->nativeContext:Lcom/google/commerce/payments/orchestration/proto/common/ContextOuterClass$NativeClientContext;

    .line 53
    const-string v0, ""

    iput-object v0, p0, Lcom/google/commerce/payments/orchestration/proto/ui/common/RequestContextOuterClass$RequestContext;->languageCode:Ljava/lang/String;

    .line 54
    const/4 v0, 0x0

    iput v0, p0, Lcom/google/commerce/payments/orchestration/proto/ui/common/RequestContextOuterClass$RequestContext;->clientType:I

    .line 55
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/google/commerce/payments/orchestration/proto/ui/common/RequestContextOuterClass$RequestContext;->clientVersion:J

    .line 56
    const/4 v0, -0x1

    iput v0, p0, Lcom/google/commerce/payments/orchestration/proto/ui/common/RequestContextOuterClass$RequestContext;->cachedSize:I

    .line 57
    return-object p0
.end method

.method protected computeSerializedSize()I
    .locals 6

    .prologue
    .line 83
    invoke-super {p0}, Lcom/google/protobuf/nano/MessageNano;->computeSerializedSize()I

    move-result v0

    .line 84
    .local v0, "size":I
    iget-object v1, p0, Lcom/google/commerce/payments/orchestration/proto/ui/common/RequestContextOuterClass$RequestContext;->sessionData:[B

    sget-object v2, Lcom/google/protobuf/nano/WireFormatNano;->EMPTY_BYTES:[B

    invoke-static {v1, v2}, Ljava/util/Arrays;->equals([B[B)Z

    move-result v1

    if-nez v1, :cond_0

    .line 85
    const/4 v1, 0x2

    iget-object v2, p0, Lcom/google/commerce/payments/orchestration/proto/ui/common/RequestContextOuterClass$RequestContext;->sessionData:[B

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeBytesSize(I[B)I

    move-result v1

    add-int/2addr v0, v1

    .line 88
    :cond_0
    iget-object v1, p0, Lcom/google/commerce/payments/orchestration/proto/ui/common/RequestContextOuterClass$RequestContext;->languageCode:Ljava/lang/String;

    const-string v2, ""

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_1

    .line 89
    const/4 v1, 0x7

    iget-object v2, p0, Lcom/google/commerce/payments/orchestration/proto/ui/common/RequestContextOuterClass$RequestContext;->languageCode:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeStringSize(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 92
    :cond_1
    iget v1, p0, Lcom/google/commerce/payments/orchestration/proto/ui/common/RequestContextOuterClass$RequestContext;->clientType:I

    if-eqz v1, :cond_2

    .line 93
    const/16 v1, 0x8

    iget v2, p0, Lcom/google/commerce/payments/orchestration/proto/ui/common/RequestContextOuterClass$RequestContext;->clientType:I

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeInt32Size(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 96
    :cond_2
    iget-wide v2, p0, Lcom/google/commerce/payments/orchestration/proto/ui/common/RequestContextOuterClass$RequestContext;->clientVersion:J

    const-wide/16 v4, 0x0

    cmp-long v1, v2, v4

    if-eqz v1, :cond_3

    .line 97
    const/16 v1, 0x9

    iget-wide v2, p0, Lcom/google/commerce/payments/orchestration/proto/ui/common/RequestContextOuterClass$RequestContext;->clientVersion:J

    invoke-static {v1, v2, v3}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeInt64Size(IJ)I

    move-result v1

    add-int/2addr v0, v1

    .line 100
    :cond_3
    iget-object v1, p0, Lcom/google/commerce/payments/orchestration/proto/ui/common/RequestContextOuterClass$RequestContext;->nativeContext:Lcom/google/commerce/payments/orchestration/proto/common/ContextOuterClass$NativeClientContext;

    if-eqz v1, :cond_4

    .line 101
    const/16 v1, 0xa

    iget-object v2, p0, Lcom/google/commerce/payments/orchestration/proto/ui/common/RequestContextOuterClass$RequestContext;->nativeContext:Lcom/google/commerce/payments/orchestration/proto/common/ContextOuterClass$NativeClientContext;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeMessageSize(ILcom/google/protobuf/nano/MessageNano;)I

    move-result v1

    add-int/2addr v0, v1

    .line 104
    :cond_4
    return v0
.end method

.method public mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/commerce/payments/orchestration/proto/ui/common/RequestContextOuterClass$RequestContext;
    .locals 4
    .param p1, "input"    # Lcom/google/protobuf/nano/CodedInputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 112
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readTag()I

    move-result v0

    .line 113
    .local v0, "tag":I
    sparse-switch v0, :sswitch_data_0

    .line 117
    invoke-static {p1, v0}, Lcom/google/protobuf/nano/WireFormatNano;->parseUnknownField(Lcom/google/protobuf/nano/CodedInputByteBufferNano;I)Z

    move-result v2

    if-nez v2, :cond_0

    .line 118
    :sswitch_0
    return-object p0

    .line 123
    :sswitch_1
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readBytes()[B

    move-result-object v2

    iput-object v2, p0, Lcom/google/commerce/payments/orchestration/proto/ui/common/RequestContextOuterClass$RequestContext;->sessionData:[B

    goto :goto_0

    .line 127
    :sswitch_2
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readString()Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/google/commerce/payments/orchestration/proto/ui/common/RequestContextOuterClass$RequestContext;->languageCode:Ljava/lang/String;

    goto :goto_0

    .line 131
    :sswitch_3
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readInt32()I

    move-result v1

    .line 132
    .local v1, "value":I
    packed-switch v1, :pswitch_data_0

    goto :goto_0

    .line 137
    :pswitch_0
    iput v1, p0, Lcom/google/commerce/payments/orchestration/proto/ui/common/RequestContextOuterClass$RequestContext;->clientType:I

    goto :goto_0

    .line 143
    .end local v1    # "value":I
    :sswitch_4
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readInt64()J

    move-result-wide v2

    iput-wide v2, p0, Lcom/google/commerce/payments/orchestration/proto/ui/common/RequestContextOuterClass$RequestContext;->clientVersion:J

    goto :goto_0

    .line 147
    :sswitch_5
    iget-object v2, p0, Lcom/google/commerce/payments/orchestration/proto/ui/common/RequestContextOuterClass$RequestContext;->nativeContext:Lcom/google/commerce/payments/orchestration/proto/common/ContextOuterClass$NativeClientContext;

    if-nez v2, :cond_1

    .line 148
    new-instance v2, Lcom/google/commerce/payments/orchestration/proto/common/ContextOuterClass$NativeClientContext;

    invoke-direct {v2}, Lcom/google/commerce/payments/orchestration/proto/common/ContextOuterClass$NativeClientContext;-><init>()V

    iput-object v2, p0, Lcom/google/commerce/payments/orchestration/proto/ui/common/RequestContextOuterClass$RequestContext;->nativeContext:Lcom/google/commerce/payments/orchestration/proto/common/ContextOuterClass$NativeClientContext;

    .line 150
    :cond_1
    iget-object v2, p0, Lcom/google/commerce/payments/orchestration/proto/ui/common/RequestContextOuterClass$RequestContext;->nativeContext:Lcom/google/commerce/payments/orchestration/proto/common/ContextOuterClass$NativeClientContext;

    invoke-virtual {p1, v2}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readMessage(Lcom/google/protobuf/nano/MessageNano;)V

    goto :goto_0

    .line 113
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x12 -> :sswitch_1
        0x3a -> :sswitch_2
        0x40 -> :sswitch_3
        0x48 -> :sswitch_4
        0x52 -> :sswitch_5
    .end sparse-switch

    .line 132
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
    .end packed-switch
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/protobuf/nano/MessageNano;
    .locals 1
    .param p1, "x0"    # Lcom/google/protobuf/nano/CodedInputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 8
    invoke-virtual {p0, p1}, Lcom/google/commerce/payments/orchestration/proto/ui/common/RequestContextOuterClass$RequestContext;->mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/commerce/payments/orchestration/proto/ui/common/RequestContextOuterClass$RequestContext;

    move-result-object v0

    return-object v0
.end method

.method public writeTo(Lcom/google/protobuf/nano/CodedOutputByteBufferNano;)V
    .locals 4
    .param p1, "output"    # Lcom/google/protobuf/nano/CodedOutputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 63
    iget-object v0, p0, Lcom/google/commerce/payments/orchestration/proto/ui/common/RequestContextOuterClass$RequestContext;->sessionData:[B

    sget-object v1, Lcom/google/protobuf/nano/WireFormatNano;->EMPTY_BYTES:[B

    invoke-static {v0, v1}, Ljava/util/Arrays;->equals([B[B)Z

    move-result v0

    if-nez v0, :cond_0

    .line 64
    const/4 v0, 0x2

    iget-object v1, p0, Lcom/google/commerce/payments/orchestration/proto/ui/common/RequestContextOuterClass$RequestContext;->sessionData:[B

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeBytes(I[B)V

    .line 66
    :cond_0
    iget-object v0, p0, Lcom/google/commerce/payments/orchestration/proto/ui/common/RequestContextOuterClass$RequestContext;->languageCode:Ljava/lang/String;

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 67
    const/4 v0, 0x7

    iget-object v1, p0, Lcom/google/commerce/payments/orchestration/proto/ui/common/RequestContextOuterClass$RequestContext;->languageCode:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeString(ILjava/lang/String;)V

    .line 69
    :cond_1
    iget v0, p0, Lcom/google/commerce/payments/orchestration/proto/ui/common/RequestContextOuterClass$RequestContext;->clientType:I

    if-eqz v0, :cond_2

    .line 70
    const/16 v0, 0x8

    iget v1, p0, Lcom/google/commerce/payments/orchestration/proto/ui/common/RequestContextOuterClass$RequestContext;->clientType:I

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeInt32(II)V

    .line 72
    :cond_2
    iget-wide v0, p0, Lcom/google/commerce/payments/orchestration/proto/ui/common/RequestContextOuterClass$RequestContext;->clientVersion:J

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-eqz v0, :cond_3

    .line 73
    const/16 v0, 0x9

    iget-wide v2, p0, Lcom/google/commerce/payments/orchestration/proto/ui/common/RequestContextOuterClass$RequestContext;->clientVersion:J

    invoke-virtual {p1, v0, v2, v3}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeInt64(IJ)V

    .line 75
    :cond_3
    iget-object v0, p0, Lcom/google/commerce/payments/orchestration/proto/ui/common/RequestContextOuterClass$RequestContext;->nativeContext:Lcom/google/commerce/payments/orchestration/proto/common/ContextOuterClass$NativeClientContext;

    if-eqz v0, :cond_4

    .line 76
    const/16 v0, 0xa

    iget-object v1, p0, Lcom/google/commerce/payments/orchestration/proto/ui/common/RequestContextOuterClass$RequestContext;->nativeContext:Lcom/google/commerce/payments/orchestration/proto/common/ContextOuterClass$NativeClientContext;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeMessage(ILcom/google/protobuf/nano/MessageNano;)V

    .line 78
    :cond_4
    invoke-super {p0, p1}, Lcom/google/protobuf/nano/MessageNano;->writeTo(Lcom/google/protobuf/nano/CodedOutputByteBufferNano;)V

    .line 79
    return-void
.end method
