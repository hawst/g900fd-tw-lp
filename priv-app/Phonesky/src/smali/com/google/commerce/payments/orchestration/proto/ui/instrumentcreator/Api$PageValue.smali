.class public final Lcom/google/commerce/payments/orchestration/proto/ui/instrumentcreator/Api$PageValue;
.super Lcom/google/protobuf/nano/MessageNano;
.source "Api.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/commerce/payments/orchestration/proto/ui/instrumentcreator/Api;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "PageValue"
.end annotation


# instance fields
.field public newCreditCardExpirationDate:Lcom/google/commerce/payments/orchestration/proto/ui/common/components/instrument/types/CreditCard$CreditCardExpirationDateFormValue;

.field public newCustomer:Lcom/google/commerce/payments/orchestration/proto/ui/common/components/customer/CustomerFormOuterClass$CustomerFormValue;

.field public newInstrument:Lcom/google/commerce/payments/orchestration/proto/ui/common/components/instrument/InstrumentFormOuterClass$InstrumentFormValue;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1206
    invoke-direct {p0}, Lcom/google/protobuf/nano/MessageNano;-><init>()V

    .line 1207
    invoke-virtual {p0}, Lcom/google/commerce/payments/orchestration/proto/ui/instrumentcreator/Api$PageValue;->clear()Lcom/google/commerce/payments/orchestration/proto/ui/instrumentcreator/Api$PageValue;

    .line 1208
    return-void
.end method


# virtual methods
.method public clear()Lcom/google/commerce/payments/orchestration/proto/ui/instrumentcreator/Api$PageValue;
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 1211
    iput-object v0, p0, Lcom/google/commerce/payments/orchestration/proto/ui/instrumentcreator/Api$PageValue;->newCustomer:Lcom/google/commerce/payments/orchestration/proto/ui/common/components/customer/CustomerFormOuterClass$CustomerFormValue;

    .line 1212
    iput-object v0, p0, Lcom/google/commerce/payments/orchestration/proto/ui/instrumentcreator/Api$PageValue;->newInstrument:Lcom/google/commerce/payments/orchestration/proto/ui/common/components/instrument/InstrumentFormOuterClass$InstrumentFormValue;

    .line 1213
    iput-object v0, p0, Lcom/google/commerce/payments/orchestration/proto/ui/instrumentcreator/Api$PageValue;->newCreditCardExpirationDate:Lcom/google/commerce/payments/orchestration/proto/ui/common/components/instrument/types/CreditCard$CreditCardExpirationDateFormValue;

    .line 1214
    const/4 v0, -0x1

    iput v0, p0, Lcom/google/commerce/payments/orchestration/proto/ui/instrumentcreator/Api$PageValue;->cachedSize:I

    .line 1215
    return-object p0
.end method

.method protected computeSerializedSize()I
    .locals 3

    .prologue
    .line 1235
    invoke-super {p0}, Lcom/google/protobuf/nano/MessageNano;->computeSerializedSize()I

    move-result v0

    .line 1236
    .local v0, "size":I
    iget-object v1, p0, Lcom/google/commerce/payments/orchestration/proto/ui/instrumentcreator/Api$PageValue;->newCustomer:Lcom/google/commerce/payments/orchestration/proto/ui/common/components/customer/CustomerFormOuterClass$CustomerFormValue;

    if-eqz v1, :cond_0

    .line 1237
    const/4 v1, 0x1

    iget-object v2, p0, Lcom/google/commerce/payments/orchestration/proto/ui/instrumentcreator/Api$PageValue;->newCustomer:Lcom/google/commerce/payments/orchestration/proto/ui/common/components/customer/CustomerFormOuterClass$CustomerFormValue;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeMessageSize(ILcom/google/protobuf/nano/MessageNano;)I

    move-result v1

    add-int/2addr v0, v1

    .line 1240
    :cond_0
    iget-object v1, p0, Lcom/google/commerce/payments/orchestration/proto/ui/instrumentcreator/Api$PageValue;->newInstrument:Lcom/google/commerce/payments/orchestration/proto/ui/common/components/instrument/InstrumentFormOuterClass$InstrumentFormValue;

    if-eqz v1, :cond_1

    .line 1241
    const/4 v1, 0x2

    iget-object v2, p0, Lcom/google/commerce/payments/orchestration/proto/ui/instrumentcreator/Api$PageValue;->newInstrument:Lcom/google/commerce/payments/orchestration/proto/ui/common/components/instrument/InstrumentFormOuterClass$InstrumentFormValue;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeMessageSize(ILcom/google/protobuf/nano/MessageNano;)I

    move-result v1

    add-int/2addr v0, v1

    .line 1244
    :cond_1
    iget-object v1, p0, Lcom/google/commerce/payments/orchestration/proto/ui/instrumentcreator/Api$PageValue;->newCreditCardExpirationDate:Lcom/google/commerce/payments/orchestration/proto/ui/common/components/instrument/types/CreditCard$CreditCardExpirationDateFormValue;

    if-eqz v1, :cond_2

    .line 1245
    const/4 v1, 0x3

    iget-object v2, p0, Lcom/google/commerce/payments/orchestration/proto/ui/instrumentcreator/Api$PageValue;->newCreditCardExpirationDate:Lcom/google/commerce/payments/orchestration/proto/ui/common/components/instrument/types/CreditCard$CreditCardExpirationDateFormValue;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeMessageSize(ILcom/google/protobuf/nano/MessageNano;)I

    move-result v1

    add-int/2addr v0, v1

    .line 1248
    :cond_2
    return v0
.end method

.method public mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/commerce/payments/orchestration/proto/ui/instrumentcreator/Api$PageValue;
    .locals 2
    .param p1, "input"    # Lcom/google/protobuf/nano/CodedInputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 1256
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readTag()I

    move-result v0

    .line 1257
    .local v0, "tag":I
    sparse-switch v0, :sswitch_data_0

    .line 1261
    invoke-static {p1, v0}, Lcom/google/protobuf/nano/WireFormatNano;->parseUnknownField(Lcom/google/protobuf/nano/CodedInputByteBufferNano;I)Z

    move-result v1

    if-nez v1, :cond_0

    .line 1262
    :sswitch_0
    return-object p0

    .line 1267
    :sswitch_1
    iget-object v1, p0, Lcom/google/commerce/payments/orchestration/proto/ui/instrumentcreator/Api$PageValue;->newCustomer:Lcom/google/commerce/payments/orchestration/proto/ui/common/components/customer/CustomerFormOuterClass$CustomerFormValue;

    if-nez v1, :cond_1

    .line 1268
    new-instance v1, Lcom/google/commerce/payments/orchestration/proto/ui/common/components/customer/CustomerFormOuterClass$CustomerFormValue;

    invoke-direct {v1}, Lcom/google/commerce/payments/orchestration/proto/ui/common/components/customer/CustomerFormOuterClass$CustomerFormValue;-><init>()V

    iput-object v1, p0, Lcom/google/commerce/payments/orchestration/proto/ui/instrumentcreator/Api$PageValue;->newCustomer:Lcom/google/commerce/payments/orchestration/proto/ui/common/components/customer/CustomerFormOuterClass$CustomerFormValue;

    .line 1270
    :cond_1
    iget-object v1, p0, Lcom/google/commerce/payments/orchestration/proto/ui/instrumentcreator/Api$PageValue;->newCustomer:Lcom/google/commerce/payments/orchestration/proto/ui/common/components/customer/CustomerFormOuterClass$CustomerFormValue;

    invoke-virtual {p1, v1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readMessage(Lcom/google/protobuf/nano/MessageNano;)V

    goto :goto_0

    .line 1274
    :sswitch_2
    iget-object v1, p0, Lcom/google/commerce/payments/orchestration/proto/ui/instrumentcreator/Api$PageValue;->newInstrument:Lcom/google/commerce/payments/orchestration/proto/ui/common/components/instrument/InstrumentFormOuterClass$InstrumentFormValue;

    if-nez v1, :cond_2

    .line 1275
    new-instance v1, Lcom/google/commerce/payments/orchestration/proto/ui/common/components/instrument/InstrumentFormOuterClass$InstrumentFormValue;

    invoke-direct {v1}, Lcom/google/commerce/payments/orchestration/proto/ui/common/components/instrument/InstrumentFormOuterClass$InstrumentFormValue;-><init>()V

    iput-object v1, p0, Lcom/google/commerce/payments/orchestration/proto/ui/instrumentcreator/Api$PageValue;->newInstrument:Lcom/google/commerce/payments/orchestration/proto/ui/common/components/instrument/InstrumentFormOuterClass$InstrumentFormValue;

    .line 1277
    :cond_2
    iget-object v1, p0, Lcom/google/commerce/payments/orchestration/proto/ui/instrumentcreator/Api$PageValue;->newInstrument:Lcom/google/commerce/payments/orchestration/proto/ui/common/components/instrument/InstrumentFormOuterClass$InstrumentFormValue;

    invoke-virtual {p1, v1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readMessage(Lcom/google/protobuf/nano/MessageNano;)V

    goto :goto_0

    .line 1281
    :sswitch_3
    iget-object v1, p0, Lcom/google/commerce/payments/orchestration/proto/ui/instrumentcreator/Api$PageValue;->newCreditCardExpirationDate:Lcom/google/commerce/payments/orchestration/proto/ui/common/components/instrument/types/CreditCard$CreditCardExpirationDateFormValue;

    if-nez v1, :cond_3

    .line 1282
    new-instance v1, Lcom/google/commerce/payments/orchestration/proto/ui/common/components/instrument/types/CreditCard$CreditCardExpirationDateFormValue;

    invoke-direct {v1}, Lcom/google/commerce/payments/orchestration/proto/ui/common/components/instrument/types/CreditCard$CreditCardExpirationDateFormValue;-><init>()V

    iput-object v1, p0, Lcom/google/commerce/payments/orchestration/proto/ui/instrumentcreator/Api$PageValue;->newCreditCardExpirationDate:Lcom/google/commerce/payments/orchestration/proto/ui/common/components/instrument/types/CreditCard$CreditCardExpirationDateFormValue;

    .line 1284
    :cond_3
    iget-object v1, p0, Lcom/google/commerce/payments/orchestration/proto/ui/instrumentcreator/Api$PageValue;->newCreditCardExpirationDate:Lcom/google/commerce/payments/orchestration/proto/ui/common/components/instrument/types/CreditCard$CreditCardExpirationDateFormValue;

    invoke-virtual {p1, v1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readMessage(Lcom/google/protobuf/nano/MessageNano;)V

    goto :goto_0

    .line 1257
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
    .end sparse-switch
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/protobuf/nano/MessageNano;
    .locals 1
    .param p1, "x0"    # Lcom/google/protobuf/nano/CodedInputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 1180
    invoke-virtual {p0, p1}, Lcom/google/commerce/payments/orchestration/proto/ui/instrumentcreator/Api$PageValue;->mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/commerce/payments/orchestration/proto/ui/instrumentcreator/Api$PageValue;

    move-result-object v0

    return-object v0
.end method

.method public writeTo(Lcom/google/protobuf/nano/CodedOutputByteBufferNano;)V
    .locals 2
    .param p1, "output"    # Lcom/google/protobuf/nano/CodedOutputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 1221
    iget-object v0, p0, Lcom/google/commerce/payments/orchestration/proto/ui/instrumentcreator/Api$PageValue;->newCustomer:Lcom/google/commerce/payments/orchestration/proto/ui/common/components/customer/CustomerFormOuterClass$CustomerFormValue;

    if-eqz v0, :cond_0

    .line 1222
    const/4 v0, 0x1

    iget-object v1, p0, Lcom/google/commerce/payments/orchestration/proto/ui/instrumentcreator/Api$PageValue;->newCustomer:Lcom/google/commerce/payments/orchestration/proto/ui/common/components/customer/CustomerFormOuterClass$CustomerFormValue;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeMessage(ILcom/google/protobuf/nano/MessageNano;)V

    .line 1224
    :cond_0
    iget-object v0, p0, Lcom/google/commerce/payments/orchestration/proto/ui/instrumentcreator/Api$PageValue;->newInstrument:Lcom/google/commerce/payments/orchestration/proto/ui/common/components/instrument/InstrumentFormOuterClass$InstrumentFormValue;

    if-eqz v0, :cond_1

    .line 1225
    const/4 v0, 0x2

    iget-object v1, p0, Lcom/google/commerce/payments/orchestration/proto/ui/instrumentcreator/Api$PageValue;->newInstrument:Lcom/google/commerce/payments/orchestration/proto/ui/common/components/instrument/InstrumentFormOuterClass$InstrumentFormValue;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeMessage(ILcom/google/protobuf/nano/MessageNano;)V

    .line 1227
    :cond_1
    iget-object v0, p0, Lcom/google/commerce/payments/orchestration/proto/ui/instrumentcreator/Api$PageValue;->newCreditCardExpirationDate:Lcom/google/commerce/payments/orchestration/proto/ui/common/components/instrument/types/CreditCard$CreditCardExpirationDateFormValue;

    if-eqz v0, :cond_2

    .line 1228
    const/4 v0, 0x3

    iget-object v1, p0, Lcom/google/commerce/payments/orchestration/proto/ui/instrumentcreator/Api$PageValue;->newCreditCardExpirationDate:Lcom/google/commerce/payments/orchestration/proto/ui/common/components/instrument/types/CreditCard$CreditCardExpirationDateFormValue;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeMessage(ILcom/google/protobuf/nano/MessageNano;)V

    .line 1230
    :cond_2
    invoke-super {p0, p1}, Lcom/google/protobuf/nano/MessageNano;->writeTo(Lcom/google/protobuf/nano/CodedOutputByteBufferNano;)V

    .line 1231
    return-void
.end method
