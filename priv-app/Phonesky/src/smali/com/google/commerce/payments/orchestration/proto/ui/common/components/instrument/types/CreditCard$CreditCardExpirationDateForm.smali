.class public final Lcom/google/commerce/payments/orchestration/proto/ui/common/components/instrument/types/CreditCard$CreditCardExpirationDateForm;
.super Lcom/google/protobuf/nano/MessageNano;
.source "CreditCard.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/commerce/payments/orchestration/proto/ui/common/components/instrument/types/CreditCard;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "CreditCardExpirationDateForm"
.end annotation


# instance fields
.field public cardLabel:Ljava/lang/String;

.field public cvcLength:I

.field public icon:Lcom/google/commerce/payments/orchestration/proto/ui/common/generic/ImageWithCaptionOuterClass$ImageWithCaption;

.field public id:Ljava/lang/String;

.field public maxMonth:I

.field public maxYear:I

.field public minMonth:I

.field public minYear:I


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1070
    invoke-direct {p0}, Lcom/google/protobuf/nano/MessageNano;-><init>()V

    .line 1071
    invoke-virtual {p0}, Lcom/google/commerce/payments/orchestration/proto/ui/common/components/instrument/types/CreditCard$CreditCardExpirationDateForm;->clear()Lcom/google/commerce/payments/orchestration/proto/ui/common/components/instrument/types/CreditCard$CreditCardExpirationDateForm;

    .line 1072
    return-void
.end method


# virtual methods
.method public clear()Lcom/google/commerce/payments/orchestration/proto/ui/common/components/instrument/types/CreditCard$CreditCardExpirationDateForm;
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 1075
    const-string v0, ""

    iput-object v0, p0, Lcom/google/commerce/payments/orchestration/proto/ui/common/components/instrument/types/CreditCard$CreditCardExpirationDateForm;->id:Ljava/lang/String;

    .line 1076
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/commerce/payments/orchestration/proto/ui/common/components/instrument/types/CreditCard$CreditCardExpirationDateForm;->icon:Lcom/google/commerce/payments/orchestration/proto/ui/common/generic/ImageWithCaptionOuterClass$ImageWithCaption;

    .line 1077
    const-string v0, ""

    iput-object v0, p0, Lcom/google/commerce/payments/orchestration/proto/ui/common/components/instrument/types/CreditCard$CreditCardExpirationDateForm;->cardLabel:Ljava/lang/String;

    .line 1078
    iput v1, p0, Lcom/google/commerce/payments/orchestration/proto/ui/common/components/instrument/types/CreditCard$CreditCardExpirationDateForm;->cvcLength:I

    .line 1079
    iput v1, p0, Lcom/google/commerce/payments/orchestration/proto/ui/common/components/instrument/types/CreditCard$CreditCardExpirationDateForm;->minMonth:I

    .line 1080
    iput v1, p0, Lcom/google/commerce/payments/orchestration/proto/ui/common/components/instrument/types/CreditCard$CreditCardExpirationDateForm;->minYear:I

    .line 1081
    iput v1, p0, Lcom/google/commerce/payments/orchestration/proto/ui/common/components/instrument/types/CreditCard$CreditCardExpirationDateForm;->maxMonth:I

    .line 1082
    iput v1, p0, Lcom/google/commerce/payments/orchestration/proto/ui/common/components/instrument/types/CreditCard$CreditCardExpirationDateForm;->maxYear:I

    .line 1083
    const/4 v0, -0x1

    iput v0, p0, Lcom/google/commerce/payments/orchestration/proto/ui/common/components/instrument/types/CreditCard$CreditCardExpirationDateForm;->cachedSize:I

    .line 1084
    return-object p0
.end method

.method protected computeSerializedSize()I
    .locals 3

    .prologue
    .line 1119
    invoke-super {p0}, Lcom/google/protobuf/nano/MessageNano;->computeSerializedSize()I

    move-result v0

    .line 1120
    .local v0, "size":I
    iget-object v1, p0, Lcom/google/commerce/payments/orchestration/proto/ui/common/components/instrument/types/CreditCard$CreditCardExpirationDateForm;->cardLabel:Ljava/lang/String;

    const-string v2, ""

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 1121
    const/4 v1, 0x2

    iget-object v2, p0, Lcom/google/commerce/payments/orchestration/proto/ui/common/components/instrument/types/CreditCard$CreditCardExpirationDateForm;->cardLabel:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeStringSize(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 1124
    :cond_0
    iget v1, p0, Lcom/google/commerce/payments/orchestration/proto/ui/common/components/instrument/types/CreditCard$CreditCardExpirationDateForm;->cvcLength:I

    if-eqz v1, :cond_1

    .line 1125
    const/4 v1, 0x3

    iget v2, p0, Lcom/google/commerce/payments/orchestration/proto/ui/common/components/instrument/types/CreditCard$CreditCardExpirationDateForm;->cvcLength:I

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeInt32Size(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 1128
    :cond_1
    iget v1, p0, Lcom/google/commerce/payments/orchestration/proto/ui/common/components/instrument/types/CreditCard$CreditCardExpirationDateForm;->minMonth:I

    if-eqz v1, :cond_2

    .line 1129
    const/4 v1, 0x4

    iget v2, p0, Lcom/google/commerce/payments/orchestration/proto/ui/common/components/instrument/types/CreditCard$CreditCardExpirationDateForm;->minMonth:I

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeInt32Size(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 1132
    :cond_2
    iget v1, p0, Lcom/google/commerce/payments/orchestration/proto/ui/common/components/instrument/types/CreditCard$CreditCardExpirationDateForm;->minYear:I

    if-eqz v1, :cond_3

    .line 1133
    const/4 v1, 0x5

    iget v2, p0, Lcom/google/commerce/payments/orchestration/proto/ui/common/components/instrument/types/CreditCard$CreditCardExpirationDateForm;->minYear:I

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeInt32Size(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 1136
    :cond_3
    iget v1, p0, Lcom/google/commerce/payments/orchestration/proto/ui/common/components/instrument/types/CreditCard$CreditCardExpirationDateForm;->maxMonth:I

    if-eqz v1, :cond_4

    .line 1137
    const/4 v1, 0x6

    iget v2, p0, Lcom/google/commerce/payments/orchestration/proto/ui/common/components/instrument/types/CreditCard$CreditCardExpirationDateForm;->maxMonth:I

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeInt32Size(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 1140
    :cond_4
    iget v1, p0, Lcom/google/commerce/payments/orchestration/proto/ui/common/components/instrument/types/CreditCard$CreditCardExpirationDateForm;->maxYear:I

    if-eqz v1, :cond_5

    .line 1141
    const/4 v1, 0x7

    iget v2, p0, Lcom/google/commerce/payments/orchestration/proto/ui/common/components/instrument/types/CreditCard$CreditCardExpirationDateForm;->maxYear:I

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeInt32Size(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 1144
    :cond_5
    iget-object v1, p0, Lcom/google/commerce/payments/orchestration/proto/ui/common/components/instrument/types/CreditCard$CreditCardExpirationDateForm;->id:Ljava/lang/String;

    const-string v2, ""

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_6

    .line 1145
    const/16 v1, 0x8

    iget-object v2, p0, Lcom/google/commerce/payments/orchestration/proto/ui/common/components/instrument/types/CreditCard$CreditCardExpirationDateForm;->id:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeStringSize(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 1148
    :cond_6
    iget-object v1, p0, Lcom/google/commerce/payments/orchestration/proto/ui/common/components/instrument/types/CreditCard$CreditCardExpirationDateForm;->icon:Lcom/google/commerce/payments/orchestration/proto/ui/common/generic/ImageWithCaptionOuterClass$ImageWithCaption;

    if-eqz v1, :cond_7

    .line 1149
    const/16 v1, 0x9

    iget-object v2, p0, Lcom/google/commerce/payments/orchestration/proto/ui/common/components/instrument/types/CreditCard$CreditCardExpirationDateForm;->icon:Lcom/google/commerce/payments/orchestration/proto/ui/common/generic/ImageWithCaptionOuterClass$ImageWithCaption;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeMessageSize(ILcom/google/protobuf/nano/MessageNano;)I

    move-result v1

    add-int/2addr v0, v1

    .line 1152
    :cond_7
    return v0
.end method

.method public mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/commerce/payments/orchestration/proto/ui/common/components/instrument/types/CreditCard$CreditCardExpirationDateForm;
    .locals 2
    .param p1, "input"    # Lcom/google/protobuf/nano/CodedInputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 1160
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readTag()I

    move-result v0

    .line 1161
    .local v0, "tag":I
    sparse-switch v0, :sswitch_data_0

    .line 1165
    invoke-static {p1, v0}, Lcom/google/protobuf/nano/WireFormatNano;->parseUnknownField(Lcom/google/protobuf/nano/CodedInputByteBufferNano;I)Z

    move-result v1

    if-nez v1, :cond_0

    .line 1166
    :sswitch_0
    return-object p0

    .line 1171
    :sswitch_1
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readString()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/google/commerce/payments/orchestration/proto/ui/common/components/instrument/types/CreditCard$CreditCardExpirationDateForm;->cardLabel:Ljava/lang/String;

    goto :goto_0

    .line 1175
    :sswitch_2
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readInt32()I

    move-result v1

    iput v1, p0, Lcom/google/commerce/payments/orchestration/proto/ui/common/components/instrument/types/CreditCard$CreditCardExpirationDateForm;->cvcLength:I

    goto :goto_0

    .line 1179
    :sswitch_3
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readInt32()I

    move-result v1

    iput v1, p0, Lcom/google/commerce/payments/orchestration/proto/ui/common/components/instrument/types/CreditCard$CreditCardExpirationDateForm;->minMonth:I

    goto :goto_0

    .line 1183
    :sswitch_4
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readInt32()I

    move-result v1

    iput v1, p0, Lcom/google/commerce/payments/orchestration/proto/ui/common/components/instrument/types/CreditCard$CreditCardExpirationDateForm;->minYear:I

    goto :goto_0

    .line 1187
    :sswitch_5
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readInt32()I

    move-result v1

    iput v1, p0, Lcom/google/commerce/payments/orchestration/proto/ui/common/components/instrument/types/CreditCard$CreditCardExpirationDateForm;->maxMonth:I

    goto :goto_0

    .line 1191
    :sswitch_6
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readInt32()I

    move-result v1

    iput v1, p0, Lcom/google/commerce/payments/orchestration/proto/ui/common/components/instrument/types/CreditCard$CreditCardExpirationDateForm;->maxYear:I

    goto :goto_0

    .line 1195
    :sswitch_7
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readString()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/google/commerce/payments/orchestration/proto/ui/common/components/instrument/types/CreditCard$CreditCardExpirationDateForm;->id:Ljava/lang/String;

    goto :goto_0

    .line 1199
    :sswitch_8
    iget-object v1, p0, Lcom/google/commerce/payments/orchestration/proto/ui/common/components/instrument/types/CreditCard$CreditCardExpirationDateForm;->icon:Lcom/google/commerce/payments/orchestration/proto/ui/common/generic/ImageWithCaptionOuterClass$ImageWithCaption;

    if-nez v1, :cond_1

    .line 1200
    new-instance v1, Lcom/google/commerce/payments/orchestration/proto/ui/common/generic/ImageWithCaptionOuterClass$ImageWithCaption;

    invoke-direct {v1}, Lcom/google/commerce/payments/orchestration/proto/ui/common/generic/ImageWithCaptionOuterClass$ImageWithCaption;-><init>()V

    iput-object v1, p0, Lcom/google/commerce/payments/orchestration/proto/ui/common/components/instrument/types/CreditCard$CreditCardExpirationDateForm;->icon:Lcom/google/commerce/payments/orchestration/proto/ui/common/generic/ImageWithCaptionOuterClass$ImageWithCaption;

    .line 1202
    :cond_1
    iget-object v1, p0, Lcom/google/commerce/payments/orchestration/proto/ui/common/components/instrument/types/CreditCard$CreditCardExpirationDateForm;->icon:Lcom/google/commerce/payments/orchestration/proto/ui/common/generic/ImageWithCaptionOuterClass$ImageWithCaption;

    invoke-virtual {p1, v1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readMessage(Lcom/google/protobuf/nano/MessageNano;)V

    goto :goto_0

    .line 1161
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x12 -> :sswitch_1
        0x18 -> :sswitch_2
        0x20 -> :sswitch_3
        0x28 -> :sswitch_4
        0x30 -> :sswitch_5
        0x38 -> :sswitch_6
        0x42 -> :sswitch_7
        0x4a -> :sswitch_8
    .end sparse-switch
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/protobuf/nano/MessageNano;
    .locals 1
    .param p1, "x0"    # Lcom/google/protobuf/nano/CodedInputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 1024
    invoke-virtual {p0, p1}, Lcom/google/commerce/payments/orchestration/proto/ui/common/components/instrument/types/CreditCard$CreditCardExpirationDateForm;->mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/commerce/payments/orchestration/proto/ui/common/components/instrument/types/CreditCard$CreditCardExpirationDateForm;

    move-result-object v0

    return-object v0
.end method

.method public writeTo(Lcom/google/protobuf/nano/CodedOutputByteBufferNano;)V
    .locals 2
    .param p1, "output"    # Lcom/google/protobuf/nano/CodedOutputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 1090
    iget-object v0, p0, Lcom/google/commerce/payments/orchestration/proto/ui/common/components/instrument/types/CreditCard$CreditCardExpirationDateForm;->cardLabel:Ljava/lang/String;

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 1091
    const/4 v0, 0x2

    iget-object v1, p0, Lcom/google/commerce/payments/orchestration/proto/ui/common/components/instrument/types/CreditCard$CreditCardExpirationDateForm;->cardLabel:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeString(ILjava/lang/String;)V

    .line 1093
    :cond_0
    iget v0, p0, Lcom/google/commerce/payments/orchestration/proto/ui/common/components/instrument/types/CreditCard$CreditCardExpirationDateForm;->cvcLength:I

    if-eqz v0, :cond_1

    .line 1094
    const/4 v0, 0x3

    iget v1, p0, Lcom/google/commerce/payments/orchestration/proto/ui/common/components/instrument/types/CreditCard$CreditCardExpirationDateForm;->cvcLength:I

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeInt32(II)V

    .line 1096
    :cond_1
    iget v0, p0, Lcom/google/commerce/payments/orchestration/proto/ui/common/components/instrument/types/CreditCard$CreditCardExpirationDateForm;->minMonth:I

    if-eqz v0, :cond_2

    .line 1097
    const/4 v0, 0x4

    iget v1, p0, Lcom/google/commerce/payments/orchestration/proto/ui/common/components/instrument/types/CreditCard$CreditCardExpirationDateForm;->minMonth:I

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeInt32(II)V

    .line 1099
    :cond_2
    iget v0, p0, Lcom/google/commerce/payments/orchestration/proto/ui/common/components/instrument/types/CreditCard$CreditCardExpirationDateForm;->minYear:I

    if-eqz v0, :cond_3

    .line 1100
    const/4 v0, 0x5

    iget v1, p0, Lcom/google/commerce/payments/orchestration/proto/ui/common/components/instrument/types/CreditCard$CreditCardExpirationDateForm;->minYear:I

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeInt32(II)V

    .line 1102
    :cond_3
    iget v0, p0, Lcom/google/commerce/payments/orchestration/proto/ui/common/components/instrument/types/CreditCard$CreditCardExpirationDateForm;->maxMonth:I

    if-eqz v0, :cond_4

    .line 1103
    const/4 v0, 0x6

    iget v1, p0, Lcom/google/commerce/payments/orchestration/proto/ui/common/components/instrument/types/CreditCard$CreditCardExpirationDateForm;->maxMonth:I

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeInt32(II)V

    .line 1105
    :cond_4
    iget v0, p0, Lcom/google/commerce/payments/orchestration/proto/ui/common/components/instrument/types/CreditCard$CreditCardExpirationDateForm;->maxYear:I

    if-eqz v0, :cond_5

    .line 1106
    const/4 v0, 0x7

    iget v1, p0, Lcom/google/commerce/payments/orchestration/proto/ui/common/components/instrument/types/CreditCard$CreditCardExpirationDateForm;->maxYear:I

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeInt32(II)V

    .line 1108
    :cond_5
    iget-object v0, p0, Lcom/google/commerce/payments/orchestration/proto/ui/common/components/instrument/types/CreditCard$CreditCardExpirationDateForm;->id:Ljava/lang/String;

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_6

    .line 1109
    const/16 v0, 0x8

    iget-object v1, p0, Lcom/google/commerce/payments/orchestration/proto/ui/common/components/instrument/types/CreditCard$CreditCardExpirationDateForm;->id:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeString(ILjava/lang/String;)V

    .line 1111
    :cond_6
    iget-object v0, p0, Lcom/google/commerce/payments/orchestration/proto/ui/common/components/instrument/types/CreditCard$CreditCardExpirationDateForm;->icon:Lcom/google/commerce/payments/orchestration/proto/ui/common/generic/ImageWithCaptionOuterClass$ImageWithCaption;

    if-eqz v0, :cond_7

    .line 1112
    const/16 v0, 0x9

    iget-object v1, p0, Lcom/google/commerce/payments/orchestration/proto/ui/common/components/instrument/types/CreditCard$CreditCardExpirationDateForm;->icon:Lcom/google/commerce/payments/orchestration/proto/ui/common/generic/ImageWithCaptionOuterClass$ImageWithCaption;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeMessage(ILcom/google/protobuf/nano/MessageNano;)V

    .line 1114
    :cond_7
    invoke-super {p0, p1}, Lcom/google/protobuf/nano/MessageNano;->writeTo(Lcom/google/protobuf/nano/CodedOutputByteBufferNano;)V

    .line 1115
    return-void
.end method
