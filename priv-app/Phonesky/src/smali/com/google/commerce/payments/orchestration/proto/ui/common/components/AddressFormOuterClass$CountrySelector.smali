.class public final Lcom/google/commerce/payments/orchestration/proto/ui/common/components/AddressFormOuterClass$CountrySelector;
.super Lcom/google/protobuf/nano/MessageNano;
.source "AddressFormOuterClass.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/commerce/payments/orchestration/proto/ui/common/components/AddressFormOuterClass;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "CountrySelector"
.end annotation


# instance fields
.field public allowedCountryCode:[Ljava/lang/String;

.field public initialCountryCode:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 650
    invoke-direct {p0}, Lcom/google/protobuf/nano/MessageNano;-><init>()V

    .line 651
    invoke-virtual {p0}, Lcom/google/commerce/payments/orchestration/proto/ui/common/components/AddressFormOuterClass$CountrySelector;->clear()Lcom/google/commerce/payments/orchestration/proto/ui/common/components/AddressFormOuterClass$CountrySelector;

    .line 652
    return-void
.end method


# virtual methods
.method public clear()Lcom/google/commerce/payments/orchestration/proto/ui/common/components/AddressFormOuterClass$CountrySelector;
    .locals 1

    .prologue
    .line 655
    sget-object v0, Lcom/google/protobuf/nano/WireFormatNano;->EMPTY_STRING_ARRAY:[Ljava/lang/String;

    iput-object v0, p0, Lcom/google/commerce/payments/orchestration/proto/ui/common/components/AddressFormOuterClass$CountrySelector;->allowedCountryCode:[Ljava/lang/String;

    .line 656
    const-string v0, ""

    iput-object v0, p0, Lcom/google/commerce/payments/orchestration/proto/ui/common/components/AddressFormOuterClass$CountrySelector;->initialCountryCode:Ljava/lang/String;

    .line 657
    const/4 v0, -0x1

    iput v0, p0, Lcom/google/commerce/payments/orchestration/proto/ui/common/components/AddressFormOuterClass$CountrySelector;->cachedSize:I

    .line 658
    return-object p0
.end method

.method protected computeSerializedSize()I
    .locals 7

    .prologue
    .line 680
    invoke-super {p0}, Lcom/google/protobuf/nano/MessageNano;->computeSerializedSize()I

    move-result v4

    .line 681
    .local v4, "size":I
    iget-object v5, p0, Lcom/google/commerce/payments/orchestration/proto/ui/common/components/AddressFormOuterClass$CountrySelector;->allowedCountryCode:[Ljava/lang/String;

    if-eqz v5, :cond_2

    iget-object v5, p0, Lcom/google/commerce/payments/orchestration/proto/ui/common/components/AddressFormOuterClass$CountrySelector;->allowedCountryCode:[Ljava/lang/String;

    array-length v5, v5

    if-lez v5, :cond_2

    .line 682
    const/4 v0, 0x0

    .line 683
    .local v0, "dataCount":I
    const/4 v1, 0x0

    .line 684
    .local v1, "dataSize":I
    const/4 v3, 0x0

    .local v3, "i":I
    :goto_0
    iget-object v5, p0, Lcom/google/commerce/payments/orchestration/proto/ui/common/components/AddressFormOuterClass$CountrySelector;->allowedCountryCode:[Ljava/lang/String;

    array-length v5, v5

    if-ge v3, v5, :cond_1

    .line 685
    iget-object v5, p0, Lcom/google/commerce/payments/orchestration/proto/ui/common/components/AddressFormOuterClass$CountrySelector;->allowedCountryCode:[Ljava/lang/String;

    aget-object v2, v5, v3

    .line 686
    .local v2, "element":Ljava/lang/String;
    if-eqz v2, :cond_0

    .line 687
    add-int/lit8 v0, v0, 0x1

    .line 688
    invoke-static {v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeStringSizeNoTag(Ljava/lang/String;)I

    move-result v5

    add-int/2addr v1, v5

    .line 684
    :cond_0
    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    .line 692
    .end local v2    # "element":Ljava/lang/String;
    :cond_1
    add-int/2addr v4, v1

    .line 693
    mul-int/lit8 v5, v0, 0x1

    add-int/2addr v4, v5

    .line 695
    .end local v0    # "dataCount":I
    .end local v1    # "dataSize":I
    .end local v3    # "i":I
    :cond_2
    iget-object v5, p0, Lcom/google/commerce/payments/orchestration/proto/ui/common/components/AddressFormOuterClass$CountrySelector;->initialCountryCode:Ljava/lang/String;

    const-string v6, ""

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-nez v5, :cond_3

    .line 696
    const/4 v5, 0x3

    iget-object v6, p0, Lcom/google/commerce/payments/orchestration/proto/ui/common/components/AddressFormOuterClass$CountrySelector;->initialCountryCode:Ljava/lang/String;

    invoke-static {v5, v6}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeStringSize(ILjava/lang/String;)I

    move-result v5

    add-int/2addr v4, v5

    .line 699
    :cond_3
    return v4
.end method

.method public mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/commerce/payments/orchestration/proto/ui/common/components/AddressFormOuterClass$CountrySelector;
    .locals 6
    .param p1, "input"    # Lcom/google/protobuf/nano/CodedInputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v4, 0x0

    .line 707
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readTag()I

    move-result v3

    .line 708
    .local v3, "tag":I
    sparse-switch v3, :sswitch_data_0

    .line 712
    invoke-static {p1, v3}, Lcom/google/protobuf/nano/WireFormatNano;->parseUnknownField(Lcom/google/protobuf/nano/CodedInputByteBufferNano;I)Z

    move-result v5

    if-nez v5, :cond_0

    .line 713
    :sswitch_0
    return-object p0

    .line 718
    :sswitch_1
    const/16 v5, 0x12

    invoke-static {p1, v5}, Lcom/google/protobuf/nano/WireFormatNano;->getRepeatedFieldArrayLength(Lcom/google/protobuf/nano/CodedInputByteBufferNano;I)I

    move-result v0

    .line 720
    .local v0, "arrayLength":I
    iget-object v5, p0, Lcom/google/commerce/payments/orchestration/proto/ui/common/components/AddressFormOuterClass$CountrySelector;->allowedCountryCode:[Ljava/lang/String;

    if-nez v5, :cond_2

    move v1, v4

    .line 721
    .local v1, "i":I
    :goto_1
    add-int v5, v1, v0

    new-array v2, v5, [Ljava/lang/String;

    .line 722
    .local v2, "newArray":[Ljava/lang/String;
    if-eqz v1, :cond_1

    .line 723
    iget-object v5, p0, Lcom/google/commerce/payments/orchestration/proto/ui/common/components/AddressFormOuterClass$CountrySelector;->allowedCountryCode:[Ljava/lang/String;

    invoke-static {v5, v4, v2, v4, v1}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 725
    :cond_1
    :goto_2
    array-length v5, v2

    add-int/lit8 v5, v5, -0x1

    if-ge v1, v5, :cond_3

    .line 726
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readString()Ljava/lang/String;

    move-result-object v5

    aput-object v5, v2, v1

    .line 727
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readTag()I

    .line 725
    add-int/lit8 v1, v1, 0x1

    goto :goto_2

    .line 720
    .end local v1    # "i":I
    .end local v2    # "newArray":[Ljava/lang/String;
    :cond_2
    iget-object v5, p0, Lcom/google/commerce/payments/orchestration/proto/ui/common/components/AddressFormOuterClass$CountrySelector;->allowedCountryCode:[Ljava/lang/String;

    array-length v1, v5

    goto :goto_1

    .line 730
    .restart local v1    # "i":I
    .restart local v2    # "newArray":[Ljava/lang/String;
    :cond_3
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readString()Ljava/lang/String;

    move-result-object v5

    aput-object v5, v2, v1

    .line 731
    iput-object v2, p0, Lcom/google/commerce/payments/orchestration/proto/ui/common/components/AddressFormOuterClass$CountrySelector;->allowedCountryCode:[Ljava/lang/String;

    goto :goto_0

    .line 735
    .end local v0    # "arrayLength":I
    .end local v1    # "i":I
    .end local v2    # "newArray":[Ljava/lang/String;
    :sswitch_2
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readString()Ljava/lang/String;

    move-result-object v5

    iput-object v5, p0, Lcom/google/commerce/payments/orchestration/proto/ui/common/components/AddressFormOuterClass$CountrySelector;->initialCountryCode:Ljava/lang/String;

    goto :goto_0

    .line 708
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x12 -> :sswitch_1
        0x1a -> :sswitch_2
    .end sparse-switch
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/protobuf/nano/MessageNano;
    .locals 1
    .param p1, "x0"    # Lcom/google/protobuf/nano/CodedInputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 627
    invoke-virtual {p0, p1}, Lcom/google/commerce/payments/orchestration/proto/ui/common/components/AddressFormOuterClass$CountrySelector;->mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/commerce/payments/orchestration/proto/ui/common/components/AddressFormOuterClass$CountrySelector;

    move-result-object v0

    return-object v0
.end method

.method public writeTo(Lcom/google/protobuf/nano/CodedOutputByteBufferNano;)V
    .locals 4
    .param p1, "output"    # Lcom/google/protobuf/nano/CodedOutputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 664
    iget-object v2, p0, Lcom/google/commerce/payments/orchestration/proto/ui/common/components/AddressFormOuterClass$CountrySelector;->allowedCountryCode:[Ljava/lang/String;

    if-eqz v2, :cond_1

    iget-object v2, p0, Lcom/google/commerce/payments/orchestration/proto/ui/common/components/AddressFormOuterClass$CountrySelector;->allowedCountryCode:[Ljava/lang/String;

    array-length v2, v2

    if-lez v2, :cond_1

    .line 665
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    iget-object v2, p0, Lcom/google/commerce/payments/orchestration/proto/ui/common/components/AddressFormOuterClass$CountrySelector;->allowedCountryCode:[Ljava/lang/String;

    array-length v2, v2

    if-ge v1, v2, :cond_1

    .line 666
    iget-object v2, p0, Lcom/google/commerce/payments/orchestration/proto/ui/common/components/AddressFormOuterClass$CountrySelector;->allowedCountryCode:[Ljava/lang/String;

    aget-object v0, v2, v1

    .line 667
    .local v0, "element":Ljava/lang/String;
    if-eqz v0, :cond_0

    .line 668
    const/4 v2, 0x2

    invoke-virtual {p1, v2, v0}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeString(ILjava/lang/String;)V

    .line 665
    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 672
    .end local v0    # "element":Ljava/lang/String;
    .end local v1    # "i":I
    :cond_1
    iget-object v2, p0, Lcom/google/commerce/payments/orchestration/proto/ui/common/components/AddressFormOuterClass$CountrySelector;->initialCountryCode:Ljava/lang/String;

    const-string v3, ""

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_2

    .line 673
    const/4 v2, 0x3

    iget-object v3, p0, Lcom/google/commerce/payments/orchestration/proto/ui/common/components/AddressFormOuterClass$CountrySelector;->initialCountryCode:Ljava/lang/String;

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeString(ILjava/lang/String;)V

    .line 675
    :cond_2
    invoke-super {p0, p1}, Lcom/google/protobuf/nano/MessageNano;->writeTo(Lcom/google/protobuf/nano/CodedOutputByteBufferNano;)V

    .line 676
    return-void
.end method
