.class public final Lcom/google/commerce/payments/orchestration/proto/api/integratorbuyflow/DataTokens$ClientToken;
.super Lcom/google/protobuf/nano/MessageNano;
.source "DataTokens.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/commerce/payments/orchestration/proto/api/integratorbuyflow/DataTokens;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "ClientToken"
.end annotation


# instance fields
.field public requestContext:Lcom/google/commerce/payments/orchestration/proto/ui/common/RequestContextOuterClass$RequestContext;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 367
    invoke-direct {p0}, Lcom/google/protobuf/nano/MessageNano;-><init>()V

    .line 368
    invoke-virtual {p0}, Lcom/google/commerce/payments/orchestration/proto/api/integratorbuyflow/DataTokens$ClientToken;->clear()Lcom/google/commerce/payments/orchestration/proto/api/integratorbuyflow/DataTokens$ClientToken;

    .line 369
    return-void
.end method


# virtual methods
.method public clear()Lcom/google/commerce/payments/orchestration/proto/api/integratorbuyflow/DataTokens$ClientToken;
    .locals 1

    .prologue
    .line 372
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/commerce/payments/orchestration/proto/api/integratorbuyflow/DataTokens$ClientToken;->requestContext:Lcom/google/commerce/payments/orchestration/proto/ui/common/RequestContextOuterClass$RequestContext;

    .line 373
    const/4 v0, -0x1

    iput v0, p0, Lcom/google/commerce/payments/orchestration/proto/api/integratorbuyflow/DataTokens$ClientToken;->cachedSize:I

    .line 374
    return-object p0
.end method

.method protected computeSerializedSize()I
    .locals 3

    .prologue
    .line 388
    invoke-super {p0}, Lcom/google/protobuf/nano/MessageNano;->computeSerializedSize()I

    move-result v0

    .line 389
    .local v0, "size":I
    iget-object v1, p0, Lcom/google/commerce/payments/orchestration/proto/api/integratorbuyflow/DataTokens$ClientToken;->requestContext:Lcom/google/commerce/payments/orchestration/proto/ui/common/RequestContextOuterClass$RequestContext;

    if-eqz v1, :cond_0

    .line 390
    const/4 v1, 0x1

    iget-object v2, p0, Lcom/google/commerce/payments/orchestration/proto/api/integratorbuyflow/DataTokens$ClientToken;->requestContext:Lcom/google/commerce/payments/orchestration/proto/ui/common/RequestContextOuterClass$RequestContext;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeMessageSize(ILcom/google/protobuf/nano/MessageNano;)I

    move-result v1

    add-int/2addr v0, v1

    .line 393
    :cond_0
    return v0
.end method

.method public mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/commerce/payments/orchestration/proto/api/integratorbuyflow/DataTokens$ClientToken;
    .locals 2
    .param p1, "input"    # Lcom/google/protobuf/nano/CodedInputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 401
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readTag()I

    move-result v0

    .line 402
    .local v0, "tag":I
    sparse-switch v0, :sswitch_data_0

    .line 406
    invoke-static {p1, v0}, Lcom/google/protobuf/nano/WireFormatNano;->parseUnknownField(Lcom/google/protobuf/nano/CodedInputByteBufferNano;I)Z

    move-result v1

    if-nez v1, :cond_0

    .line 407
    :sswitch_0
    return-object p0

    .line 412
    :sswitch_1
    iget-object v1, p0, Lcom/google/commerce/payments/orchestration/proto/api/integratorbuyflow/DataTokens$ClientToken;->requestContext:Lcom/google/commerce/payments/orchestration/proto/ui/common/RequestContextOuterClass$RequestContext;

    if-nez v1, :cond_1

    .line 413
    new-instance v1, Lcom/google/commerce/payments/orchestration/proto/ui/common/RequestContextOuterClass$RequestContext;

    invoke-direct {v1}, Lcom/google/commerce/payments/orchestration/proto/ui/common/RequestContextOuterClass$RequestContext;-><init>()V

    iput-object v1, p0, Lcom/google/commerce/payments/orchestration/proto/api/integratorbuyflow/DataTokens$ClientToken;->requestContext:Lcom/google/commerce/payments/orchestration/proto/ui/common/RequestContextOuterClass$RequestContext;

    .line 415
    :cond_1
    iget-object v1, p0, Lcom/google/commerce/payments/orchestration/proto/api/integratorbuyflow/DataTokens$ClientToken;->requestContext:Lcom/google/commerce/payments/orchestration/proto/ui/common/RequestContextOuterClass$RequestContext;

    invoke-virtual {p1, v1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readMessage(Lcom/google/protobuf/nano/MessageNano;)V

    goto :goto_0

    .line 402
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
    .end sparse-switch
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/protobuf/nano/MessageNano;
    .locals 1
    .param p1, "x0"    # Lcom/google/protobuf/nano/CodedInputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 347
    invoke-virtual {p0, p1}, Lcom/google/commerce/payments/orchestration/proto/api/integratorbuyflow/DataTokens$ClientToken;->mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/commerce/payments/orchestration/proto/api/integratorbuyflow/DataTokens$ClientToken;

    move-result-object v0

    return-object v0
.end method

.method public writeTo(Lcom/google/protobuf/nano/CodedOutputByteBufferNano;)V
    .locals 2
    .param p1, "output"    # Lcom/google/protobuf/nano/CodedOutputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 380
    iget-object v0, p0, Lcom/google/commerce/payments/orchestration/proto/api/integratorbuyflow/DataTokens$ClientToken;->requestContext:Lcom/google/commerce/payments/orchestration/proto/ui/common/RequestContextOuterClass$RequestContext;

    if-eqz v0, :cond_0

    .line 381
    const/4 v0, 0x1

    iget-object v1, p0, Lcom/google/commerce/payments/orchestration/proto/api/integratorbuyflow/DataTokens$ClientToken;->requestContext:Lcom/google/commerce/payments/orchestration/proto/ui/common/RequestContextOuterClass$RequestContext;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeMessage(ILcom/google/protobuf/nano/MessageNano;)V

    .line 383
    :cond_0
    invoke-super {p0, p1}, Lcom/google/protobuf/nano/MessageNano;->writeTo(Lcom/google/protobuf/nano/CodedOutputByteBufferNano;)V

    .line 384
    return-void
.end method
