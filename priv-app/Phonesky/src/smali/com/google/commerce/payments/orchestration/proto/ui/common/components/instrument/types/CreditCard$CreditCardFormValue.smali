.class public final Lcom/google/commerce/payments/orchestration/proto/ui/common/components/instrument/types/CreditCard$CreditCardFormValue;
.super Lcom/google/protobuf/nano/MessageNano;
.source "CreditCard.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/commerce/payments/orchestration/proto/ui/common/components/instrument/types/CreditCard;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "CreditCardFormValue"
.end annotation


# instance fields
.field public billingAddress:Lcom/google/commerce/payments/orchestration/proto/ui/common/components/AddressFormOuterClass$AddressFormValue;

.field public cardNumber:Ljava/lang/String;

.field public cardholderName:Ljava/lang/String;

.field public cvc:Ljava/lang/String;

.field public expirationMonth:I

.field public expirationYear:I

.field public lastFourDigits:Ljava/lang/String;

.field public legalDocData:Ljava/lang/String;

.field public type:I

.field public typeToken:[B


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 839
    invoke-direct {p0}, Lcom/google/protobuf/nano/MessageNano;-><init>()V

    .line 840
    invoke-virtual {p0}, Lcom/google/commerce/payments/orchestration/proto/ui/common/components/instrument/types/CreditCard$CreditCardFormValue;->clear()Lcom/google/commerce/payments/orchestration/proto/ui/common/components/instrument/types/CreditCard$CreditCardFormValue;

    .line 841
    return-void
.end method


# virtual methods
.method public clear()Lcom/google/commerce/payments/orchestration/proto/ui/common/components/instrument/types/CreditCard$CreditCardFormValue;
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 844
    const-string v0, ""

    iput-object v0, p0, Lcom/google/commerce/payments/orchestration/proto/ui/common/components/instrument/types/CreditCard$CreditCardFormValue;->cardholderName:Ljava/lang/String;

    .line 845
    const-string v0, ""

    iput-object v0, p0, Lcom/google/commerce/payments/orchestration/proto/ui/common/components/instrument/types/CreditCard$CreditCardFormValue;->cardNumber:Ljava/lang/String;

    .line 846
    const-string v0, ""

    iput-object v0, p0, Lcom/google/commerce/payments/orchestration/proto/ui/common/components/instrument/types/CreditCard$CreditCardFormValue;->cvc:Ljava/lang/String;

    .line 847
    iput v1, p0, Lcom/google/commerce/payments/orchestration/proto/ui/common/components/instrument/types/CreditCard$CreditCardFormValue;->expirationMonth:I

    .line 848
    iput v1, p0, Lcom/google/commerce/payments/orchestration/proto/ui/common/components/instrument/types/CreditCard$CreditCardFormValue;->expirationYear:I

    .line 849
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/commerce/payments/orchestration/proto/ui/common/components/instrument/types/CreditCard$CreditCardFormValue;->billingAddress:Lcom/google/commerce/payments/orchestration/proto/ui/common/components/AddressFormOuterClass$AddressFormValue;

    .line 850
    const-string v0, ""

    iput-object v0, p0, Lcom/google/commerce/payments/orchestration/proto/ui/common/components/instrument/types/CreditCard$CreditCardFormValue;->lastFourDigits:Ljava/lang/String;

    .line 851
    iput v1, p0, Lcom/google/commerce/payments/orchestration/proto/ui/common/components/instrument/types/CreditCard$CreditCardFormValue;->type:I

    .line 852
    sget-object v0, Lcom/google/protobuf/nano/WireFormatNano;->EMPTY_BYTES:[B

    iput-object v0, p0, Lcom/google/commerce/payments/orchestration/proto/ui/common/components/instrument/types/CreditCard$CreditCardFormValue;->typeToken:[B

    .line 853
    const-string v0, ""

    iput-object v0, p0, Lcom/google/commerce/payments/orchestration/proto/ui/common/components/instrument/types/CreditCard$CreditCardFormValue;->legalDocData:Ljava/lang/String;

    .line 854
    const/4 v0, -0x1

    iput v0, p0, Lcom/google/commerce/payments/orchestration/proto/ui/common/components/instrument/types/CreditCard$CreditCardFormValue;->cachedSize:I

    .line 855
    return-object p0
.end method

.method protected computeSerializedSize()I
    .locals 3

    .prologue
    .line 896
    invoke-super {p0}, Lcom/google/protobuf/nano/MessageNano;->computeSerializedSize()I

    move-result v0

    .line 897
    .local v0, "size":I
    iget-object v1, p0, Lcom/google/commerce/payments/orchestration/proto/ui/common/components/instrument/types/CreditCard$CreditCardFormValue;->cardNumber:Ljava/lang/String;

    const-string v2, ""

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 898
    const/4 v1, 0x1

    iget-object v2, p0, Lcom/google/commerce/payments/orchestration/proto/ui/common/components/instrument/types/CreditCard$CreditCardFormValue;->cardNumber:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeStringSize(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 901
    :cond_0
    iget-object v1, p0, Lcom/google/commerce/payments/orchestration/proto/ui/common/components/instrument/types/CreditCard$CreditCardFormValue;->cvc:Ljava/lang/String;

    const-string v2, ""

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_1

    .line 902
    const/4 v1, 0x2

    iget-object v2, p0, Lcom/google/commerce/payments/orchestration/proto/ui/common/components/instrument/types/CreditCard$CreditCardFormValue;->cvc:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeStringSize(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 905
    :cond_1
    iget v1, p0, Lcom/google/commerce/payments/orchestration/proto/ui/common/components/instrument/types/CreditCard$CreditCardFormValue;->expirationMonth:I

    if-eqz v1, :cond_2

    .line 906
    const/4 v1, 0x3

    iget v2, p0, Lcom/google/commerce/payments/orchestration/proto/ui/common/components/instrument/types/CreditCard$CreditCardFormValue;->expirationMonth:I

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeInt32Size(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 909
    :cond_2
    iget v1, p0, Lcom/google/commerce/payments/orchestration/proto/ui/common/components/instrument/types/CreditCard$CreditCardFormValue;->expirationYear:I

    if-eqz v1, :cond_3

    .line 910
    const/4 v1, 0x4

    iget v2, p0, Lcom/google/commerce/payments/orchestration/proto/ui/common/components/instrument/types/CreditCard$CreditCardFormValue;->expirationYear:I

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeInt32Size(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 913
    :cond_3
    iget-object v1, p0, Lcom/google/commerce/payments/orchestration/proto/ui/common/components/instrument/types/CreditCard$CreditCardFormValue;->lastFourDigits:Ljava/lang/String;

    const-string v2, ""

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_4

    .line 914
    const/4 v1, 0x6

    iget-object v2, p0, Lcom/google/commerce/payments/orchestration/proto/ui/common/components/instrument/types/CreditCard$CreditCardFormValue;->lastFourDigits:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeStringSize(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 917
    :cond_4
    iget v1, p0, Lcom/google/commerce/payments/orchestration/proto/ui/common/components/instrument/types/CreditCard$CreditCardFormValue;->type:I

    if-eqz v1, :cond_5

    .line 918
    const/4 v1, 0x7

    iget v2, p0, Lcom/google/commerce/payments/orchestration/proto/ui/common/components/instrument/types/CreditCard$CreditCardFormValue;->type:I

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeInt32Size(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 921
    :cond_5
    iget-object v1, p0, Lcom/google/commerce/payments/orchestration/proto/ui/common/components/instrument/types/CreditCard$CreditCardFormValue;->cardholderName:Ljava/lang/String;

    const-string v2, ""

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_6

    .line 922
    const/16 v1, 0x9

    iget-object v2, p0, Lcom/google/commerce/payments/orchestration/proto/ui/common/components/instrument/types/CreditCard$CreditCardFormValue;->cardholderName:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeStringSize(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 925
    :cond_6
    iget-object v1, p0, Lcom/google/commerce/payments/orchestration/proto/ui/common/components/instrument/types/CreditCard$CreditCardFormValue;->billingAddress:Lcom/google/commerce/payments/orchestration/proto/ui/common/components/AddressFormOuterClass$AddressFormValue;

    if-eqz v1, :cond_7

    .line 926
    const/16 v1, 0xa

    iget-object v2, p0, Lcom/google/commerce/payments/orchestration/proto/ui/common/components/instrument/types/CreditCard$CreditCardFormValue;->billingAddress:Lcom/google/commerce/payments/orchestration/proto/ui/common/components/AddressFormOuterClass$AddressFormValue;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeMessageSize(ILcom/google/protobuf/nano/MessageNano;)I

    move-result v1

    add-int/2addr v0, v1

    .line 929
    :cond_7
    iget-object v1, p0, Lcom/google/commerce/payments/orchestration/proto/ui/common/components/instrument/types/CreditCard$CreditCardFormValue;->typeToken:[B

    sget-object v2, Lcom/google/protobuf/nano/WireFormatNano;->EMPTY_BYTES:[B

    invoke-static {v1, v2}, Ljava/util/Arrays;->equals([B[B)Z

    move-result v1

    if-nez v1, :cond_8

    .line 930
    const/16 v1, 0xb

    iget-object v2, p0, Lcom/google/commerce/payments/orchestration/proto/ui/common/components/instrument/types/CreditCard$CreditCardFormValue;->typeToken:[B

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeBytesSize(I[B)I

    move-result v1

    add-int/2addr v0, v1

    .line 933
    :cond_8
    iget-object v1, p0, Lcom/google/commerce/payments/orchestration/proto/ui/common/components/instrument/types/CreditCard$CreditCardFormValue;->legalDocData:Ljava/lang/String;

    const-string v2, ""

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_9

    .line 934
    const/16 v1, 0xc

    iget-object v2, p0, Lcom/google/commerce/payments/orchestration/proto/ui/common/components/instrument/types/CreditCard$CreditCardFormValue;->legalDocData:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeStringSize(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 937
    :cond_9
    return v0
.end method

.method public mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/commerce/payments/orchestration/proto/ui/common/components/instrument/types/CreditCard$CreditCardFormValue;
    .locals 3
    .param p1, "input"    # Lcom/google/protobuf/nano/CodedInputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 945
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readTag()I

    move-result v0

    .line 946
    .local v0, "tag":I
    sparse-switch v0, :sswitch_data_0

    .line 950
    invoke-static {p1, v0}, Lcom/google/protobuf/nano/WireFormatNano;->parseUnknownField(Lcom/google/protobuf/nano/CodedInputByteBufferNano;I)Z

    move-result v2

    if-nez v2, :cond_0

    .line 951
    :sswitch_0
    return-object p0

    .line 956
    :sswitch_1
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readString()Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/google/commerce/payments/orchestration/proto/ui/common/components/instrument/types/CreditCard$CreditCardFormValue;->cardNumber:Ljava/lang/String;

    goto :goto_0

    .line 960
    :sswitch_2
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readString()Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/google/commerce/payments/orchestration/proto/ui/common/components/instrument/types/CreditCard$CreditCardFormValue;->cvc:Ljava/lang/String;

    goto :goto_0

    .line 964
    :sswitch_3
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readInt32()I

    move-result v2

    iput v2, p0, Lcom/google/commerce/payments/orchestration/proto/ui/common/components/instrument/types/CreditCard$CreditCardFormValue;->expirationMonth:I

    goto :goto_0

    .line 968
    :sswitch_4
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readInt32()I

    move-result v2

    iput v2, p0, Lcom/google/commerce/payments/orchestration/proto/ui/common/components/instrument/types/CreditCard$CreditCardFormValue;->expirationYear:I

    goto :goto_0

    .line 972
    :sswitch_5
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readString()Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/google/commerce/payments/orchestration/proto/ui/common/components/instrument/types/CreditCard$CreditCardFormValue;->lastFourDigits:Ljava/lang/String;

    goto :goto_0

    .line 976
    :sswitch_6
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readInt32()I

    move-result v1

    .line 977
    .local v1, "value":I
    packed-switch v1, :pswitch_data_0

    goto :goto_0

    .line 984
    :pswitch_0
    iput v1, p0, Lcom/google/commerce/payments/orchestration/proto/ui/common/components/instrument/types/CreditCard$CreditCardFormValue;->type:I

    goto :goto_0

    .line 990
    .end local v1    # "value":I
    :sswitch_7
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readString()Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/google/commerce/payments/orchestration/proto/ui/common/components/instrument/types/CreditCard$CreditCardFormValue;->cardholderName:Ljava/lang/String;

    goto :goto_0

    .line 994
    :sswitch_8
    iget-object v2, p0, Lcom/google/commerce/payments/orchestration/proto/ui/common/components/instrument/types/CreditCard$CreditCardFormValue;->billingAddress:Lcom/google/commerce/payments/orchestration/proto/ui/common/components/AddressFormOuterClass$AddressFormValue;

    if-nez v2, :cond_1

    .line 995
    new-instance v2, Lcom/google/commerce/payments/orchestration/proto/ui/common/components/AddressFormOuterClass$AddressFormValue;

    invoke-direct {v2}, Lcom/google/commerce/payments/orchestration/proto/ui/common/components/AddressFormOuterClass$AddressFormValue;-><init>()V

    iput-object v2, p0, Lcom/google/commerce/payments/orchestration/proto/ui/common/components/instrument/types/CreditCard$CreditCardFormValue;->billingAddress:Lcom/google/commerce/payments/orchestration/proto/ui/common/components/AddressFormOuterClass$AddressFormValue;

    .line 997
    :cond_1
    iget-object v2, p0, Lcom/google/commerce/payments/orchestration/proto/ui/common/components/instrument/types/CreditCard$CreditCardFormValue;->billingAddress:Lcom/google/commerce/payments/orchestration/proto/ui/common/components/AddressFormOuterClass$AddressFormValue;

    invoke-virtual {p1, v2}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readMessage(Lcom/google/protobuf/nano/MessageNano;)V

    goto :goto_0

    .line 1001
    :sswitch_9
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readBytes()[B

    move-result-object v2

    iput-object v2, p0, Lcom/google/commerce/payments/orchestration/proto/ui/common/components/instrument/types/CreditCard$CreditCardFormValue;->typeToken:[B

    goto :goto_0

    .line 1005
    :sswitch_a
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readString()Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/google/commerce/payments/orchestration/proto/ui/common/components/instrument/types/CreditCard$CreditCardFormValue;->legalDocData:Ljava/lang/String;

    goto :goto_0

    .line 946
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x18 -> :sswitch_3
        0x20 -> :sswitch_4
        0x32 -> :sswitch_5
        0x38 -> :sswitch_6
        0x4a -> :sswitch_7
        0x52 -> :sswitch_8
        0x5a -> :sswitch_9
        0x62 -> :sswitch_a
    .end sparse-switch

    .line 977
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
    .end packed-switch
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/protobuf/nano/MessageNano;
    .locals 1
    .param p1, "x0"    # Lcom/google/protobuf/nano/CodedInputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 784
    invoke-virtual {p0, p1}, Lcom/google/commerce/payments/orchestration/proto/ui/common/components/instrument/types/CreditCard$CreditCardFormValue;->mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/commerce/payments/orchestration/proto/ui/common/components/instrument/types/CreditCard$CreditCardFormValue;

    move-result-object v0

    return-object v0
.end method

.method public writeTo(Lcom/google/protobuf/nano/CodedOutputByteBufferNano;)V
    .locals 2
    .param p1, "output"    # Lcom/google/protobuf/nano/CodedOutputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 861
    iget-object v0, p0, Lcom/google/commerce/payments/orchestration/proto/ui/common/components/instrument/types/CreditCard$CreditCardFormValue;->cardNumber:Ljava/lang/String;

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 862
    const/4 v0, 0x1

    iget-object v1, p0, Lcom/google/commerce/payments/orchestration/proto/ui/common/components/instrument/types/CreditCard$CreditCardFormValue;->cardNumber:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeString(ILjava/lang/String;)V

    .line 864
    :cond_0
    iget-object v0, p0, Lcom/google/commerce/payments/orchestration/proto/ui/common/components/instrument/types/CreditCard$CreditCardFormValue;->cvc:Ljava/lang/String;

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 865
    const/4 v0, 0x2

    iget-object v1, p0, Lcom/google/commerce/payments/orchestration/proto/ui/common/components/instrument/types/CreditCard$CreditCardFormValue;->cvc:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeString(ILjava/lang/String;)V

    .line 867
    :cond_1
    iget v0, p0, Lcom/google/commerce/payments/orchestration/proto/ui/common/components/instrument/types/CreditCard$CreditCardFormValue;->expirationMonth:I

    if-eqz v0, :cond_2

    .line 868
    const/4 v0, 0x3

    iget v1, p0, Lcom/google/commerce/payments/orchestration/proto/ui/common/components/instrument/types/CreditCard$CreditCardFormValue;->expirationMonth:I

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeInt32(II)V

    .line 870
    :cond_2
    iget v0, p0, Lcom/google/commerce/payments/orchestration/proto/ui/common/components/instrument/types/CreditCard$CreditCardFormValue;->expirationYear:I

    if-eqz v0, :cond_3

    .line 871
    const/4 v0, 0x4

    iget v1, p0, Lcom/google/commerce/payments/orchestration/proto/ui/common/components/instrument/types/CreditCard$CreditCardFormValue;->expirationYear:I

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeInt32(II)V

    .line 873
    :cond_3
    iget-object v0, p0, Lcom/google/commerce/payments/orchestration/proto/ui/common/components/instrument/types/CreditCard$CreditCardFormValue;->lastFourDigits:Ljava/lang/String;

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_4

    .line 874
    const/4 v0, 0x6

    iget-object v1, p0, Lcom/google/commerce/payments/orchestration/proto/ui/common/components/instrument/types/CreditCard$CreditCardFormValue;->lastFourDigits:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeString(ILjava/lang/String;)V

    .line 876
    :cond_4
    iget v0, p0, Lcom/google/commerce/payments/orchestration/proto/ui/common/components/instrument/types/CreditCard$CreditCardFormValue;->type:I

    if-eqz v0, :cond_5

    .line 877
    const/4 v0, 0x7

    iget v1, p0, Lcom/google/commerce/payments/orchestration/proto/ui/common/components/instrument/types/CreditCard$CreditCardFormValue;->type:I

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeInt32(II)V

    .line 879
    :cond_5
    iget-object v0, p0, Lcom/google/commerce/payments/orchestration/proto/ui/common/components/instrument/types/CreditCard$CreditCardFormValue;->cardholderName:Ljava/lang/String;

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_6

    .line 880
    const/16 v0, 0x9

    iget-object v1, p0, Lcom/google/commerce/payments/orchestration/proto/ui/common/components/instrument/types/CreditCard$CreditCardFormValue;->cardholderName:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeString(ILjava/lang/String;)V

    .line 882
    :cond_6
    iget-object v0, p0, Lcom/google/commerce/payments/orchestration/proto/ui/common/components/instrument/types/CreditCard$CreditCardFormValue;->billingAddress:Lcom/google/commerce/payments/orchestration/proto/ui/common/components/AddressFormOuterClass$AddressFormValue;

    if-eqz v0, :cond_7

    .line 883
    const/16 v0, 0xa

    iget-object v1, p0, Lcom/google/commerce/payments/orchestration/proto/ui/common/components/instrument/types/CreditCard$CreditCardFormValue;->billingAddress:Lcom/google/commerce/payments/orchestration/proto/ui/common/components/AddressFormOuterClass$AddressFormValue;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeMessage(ILcom/google/protobuf/nano/MessageNano;)V

    .line 885
    :cond_7
    iget-object v0, p0, Lcom/google/commerce/payments/orchestration/proto/ui/common/components/instrument/types/CreditCard$CreditCardFormValue;->typeToken:[B

    sget-object v1, Lcom/google/protobuf/nano/WireFormatNano;->EMPTY_BYTES:[B

    invoke-static {v0, v1}, Ljava/util/Arrays;->equals([B[B)Z

    move-result v0

    if-nez v0, :cond_8

    .line 886
    const/16 v0, 0xb

    iget-object v1, p0, Lcom/google/commerce/payments/orchestration/proto/ui/common/components/instrument/types/CreditCard$CreditCardFormValue;->typeToken:[B

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeBytes(I[B)V

    .line 888
    :cond_8
    iget-object v0, p0, Lcom/google/commerce/payments/orchestration/proto/ui/common/components/instrument/types/CreditCard$CreditCardFormValue;->legalDocData:Ljava/lang/String;

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_9

    .line 889
    const/16 v0, 0xc

    iget-object v1, p0, Lcom/google/commerce/payments/orchestration/proto/ui/common/components/instrument/types/CreditCard$CreditCardFormValue;->legalDocData:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeString(ILjava/lang/String;)V

    .line 891
    :cond_9
    invoke-super {p0, p1}, Lcom/google/protobuf/nano/MessageNano;->writeTo(Lcom/google/protobuf/nano/CodedOutputByteBufferNano;)V

    .line 892
    return-void
.end method
