.class public final Lpaymentfraud/mobile/DeviceFingerprinting$Parsed$State$PackageInfo;
.super Lcom/google/protobuf/nano/MessageNano;
.source "DeviceFingerprinting.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lpaymentfraud/mobile/DeviceFingerprinting$Parsed$State;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "PackageInfo"
.end annotation


# static fields
.field private static volatile _emptyArray:[Lpaymentfraud/mobile/DeviceFingerprinting$Parsed$State$PackageInfo;


# instance fields
.field public firstInstallTime:J

.field public installLocation:Ljava/lang/String;

.field public lastUpdateTime:J

.field public name:Ljava/lang/String;

.field public versionCode:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 321
    invoke-direct {p0}, Lcom/google/protobuf/nano/MessageNano;-><init>()V

    .line 322
    invoke-virtual {p0}, Lpaymentfraud/mobile/DeviceFingerprinting$Parsed$State$PackageInfo;->clear()Lpaymentfraud/mobile/DeviceFingerprinting$Parsed$State$PackageInfo;

    .line 323
    return-void
.end method

.method public static emptyArray()[Lpaymentfraud/mobile/DeviceFingerprinting$Parsed$State$PackageInfo;
    .locals 2

    .prologue
    .line 295
    sget-object v0, Lpaymentfraud/mobile/DeviceFingerprinting$Parsed$State$PackageInfo;->_emptyArray:[Lpaymentfraud/mobile/DeviceFingerprinting$Parsed$State$PackageInfo;

    if-nez v0, :cond_1

    .line 296
    sget-object v1, Lcom/google/protobuf/nano/InternalNano;->LAZY_INIT_LOCK:Ljava/lang/Object;

    monitor-enter v1

    .line 298
    :try_start_0
    sget-object v0, Lpaymentfraud/mobile/DeviceFingerprinting$Parsed$State$PackageInfo;->_emptyArray:[Lpaymentfraud/mobile/DeviceFingerprinting$Parsed$State$PackageInfo;

    if-nez v0, :cond_0

    .line 299
    const/4 v0, 0x0

    new-array v0, v0, [Lpaymentfraud/mobile/DeviceFingerprinting$Parsed$State$PackageInfo;

    sput-object v0, Lpaymentfraud/mobile/DeviceFingerprinting$Parsed$State$PackageInfo;->_emptyArray:[Lpaymentfraud/mobile/DeviceFingerprinting$Parsed$State$PackageInfo;

    .line 301
    :cond_0
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 303
    :cond_1
    sget-object v0, Lpaymentfraud/mobile/DeviceFingerprinting$Parsed$State$PackageInfo;->_emptyArray:[Lpaymentfraud/mobile/DeviceFingerprinting$Parsed$State$PackageInfo;

    return-object v0

    .line 301
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method


# virtual methods
.method public clear()Lpaymentfraud/mobile/DeviceFingerprinting$Parsed$State$PackageInfo;
    .locals 4

    .prologue
    const-wide/16 v2, -0x1

    .line 326
    const-string v0, ""

    iput-object v0, p0, Lpaymentfraud/mobile/DeviceFingerprinting$Parsed$State$PackageInfo;->name:Ljava/lang/String;

    .line 327
    const-string v0, ""

    iput-object v0, p0, Lpaymentfraud/mobile/DeviceFingerprinting$Parsed$State$PackageInfo;->versionCode:Ljava/lang/String;

    .line 328
    iput-wide v2, p0, Lpaymentfraud/mobile/DeviceFingerprinting$Parsed$State$PackageInfo;->lastUpdateTime:J

    .line 329
    iput-wide v2, p0, Lpaymentfraud/mobile/DeviceFingerprinting$Parsed$State$PackageInfo;->firstInstallTime:J

    .line 330
    const-string v0, ""

    iput-object v0, p0, Lpaymentfraud/mobile/DeviceFingerprinting$Parsed$State$PackageInfo;->installLocation:Ljava/lang/String;

    .line 331
    const/4 v0, -0x1

    iput v0, p0, Lpaymentfraud/mobile/DeviceFingerprinting$Parsed$State$PackageInfo;->cachedSize:I

    .line 332
    return-object p0
.end method

.method protected computeSerializedSize()I
    .locals 6

    .prologue
    const-wide/16 v4, -0x1

    .line 358
    invoke-super {p0}, Lcom/google/protobuf/nano/MessageNano;->computeSerializedSize()I

    move-result v0

    .line 359
    .local v0, "size":I
    iget-object v1, p0, Lpaymentfraud/mobile/DeviceFingerprinting$Parsed$State$PackageInfo;->name:Ljava/lang/String;

    const-string v2, ""

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 360
    const/4 v1, 0x1

    iget-object v2, p0, Lpaymentfraud/mobile/DeviceFingerprinting$Parsed$State$PackageInfo;->name:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeStringSize(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 363
    :cond_0
    iget-object v1, p0, Lpaymentfraud/mobile/DeviceFingerprinting$Parsed$State$PackageInfo;->versionCode:Ljava/lang/String;

    const-string v2, ""

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_1

    .line 364
    const/4 v1, 0x2

    iget-object v2, p0, Lpaymentfraud/mobile/DeviceFingerprinting$Parsed$State$PackageInfo;->versionCode:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeStringSize(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 367
    :cond_1
    iget-wide v2, p0, Lpaymentfraud/mobile/DeviceFingerprinting$Parsed$State$PackageInfo;->lastUpdateTime:J

    cmp-long v1, v2, v4

    if-eqz v1, :cond_2

    .line 368
    const/4 v1, 0x3

    iget-wide v2, p0, Lpaymentfraud/mobile/DeviceFingerprinting$Parsed$State$PackageInfo;->lastUpdateTime:J

    invoke-static {v1, v2, v3}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeInt64Size(IJ)I

    move-result v1

    add-int/2addr v0, v1

    .line 371
    :cond_2
    iget-wide v2, p0, Lpaymentfraud/mobile/DeviceFingerprinting$Parsed$State$PackageInfo;->firstInstallTime:J

    cmp-long v1, v2, v4

    if-eqz v1, :cond_3

    .line 372
    const/4 v1, 0x4

    iget-wide v2, p0, Lpaymentfraud/mobile/DeviceFingerprinting$Parsed$State$PackageInfo;->firstInstallTime:J

    invoke-static {v1, v2, v3}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeInt64Size(IJ)I

    move-result v1

    add-int/2addr v0, v1

    .line 375
    :cond_3
    iget-object v1, p0, Lpaymentfraud/mobile/DeviceFingerprinting$Parsed$State$PackageInfo;->installLocation:Ljava/lang/String;

    const-string v2, ""

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_4

    .line 376
    const/4 v1, 0x5

    iget-object v2, p0, Lpaymentfraud/mobile/DeviceFingerprinting$Parsed$State$PackageInfo;->installLocation:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeStringSize(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 379
    :cond_4
    return v0
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/protobuf/nano/MessageNano;
    .locals 1
    .param p1, "x0"    # Lcom/google/protobuf/nano/CodedInputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 289
    invoke-virtual {p0, p1}, Lpaymentfraud/mobile/DeviceFingerprinting$Parsed$State$PackageInfo;->mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lpaymentfraud/mobile/DeviceFingerprinting$Parsed$State$PackageInfo;

    move-result-object v0

    return-object v0
.end method

.method public mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lpaymentfraud/mobile/DeviceFingerprinting$Parsed$State$PackageInfo;
    .locals 4
    .param p1, "input"    # Lcom/google/protobuf/nano/CodedInputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 387
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readTag()I

    move-result v0

    .line 388
    .local v0, "tag":I
    sparse-switch v0, :sswitch_data_0

    .line 392
    invoke-static {p1, v0}, Lcom/google/protobuf/nano/WireFormatNano;->parseUnknownField(Lcom/google/protobuf/nano/CodedInputByteBufferNano;I)Z

    move-result v1

    if-nez v1, :cond_0

    .line 393
    :sswitch_0
    return-object p0

    .line 398
    :sswitch_1
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readString()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lpaymentfraud/mobile/DeviceFingerprinting$Parsed$State$PackageInfo;->name:Ljava/lang/String;

    goto :goto_0

    .line 402
    :sswitch_2
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readString()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lpaymentfraud/mobile/DeviceFingerprinting$Parsed$State$PackageInfo;->versionCode:Ljava/lang/String;

    goto :goto_0

    .line 406
    :sswitch_3
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readInt64()J

    move-result-wide v2

    iput-wide v2, p0, Lpaymentfraud/mobile/DeviceFingerprinting$Parsed$State$PackageInfo;->lastUpdateTime:J

    goto :goto_0

    .line 410
    :sswitch_4
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readInt64()J

    move-result-wide v2

    iput-wide v2, p0, Lpaymentfraud/mobile/DeviceFingerprinting$Parsed$State$PackageInfo;->firstInstallTime:J

    goto :goto_0

    .line 414
    :sswitch_5
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readString()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lpaymentfraud/mobile/DeviceFingerprinting$Parsed$State$PackageInfo;->installLocation:Ljava/lang/String;

    goto :goto_0

    .line 388
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x18 -> :sswitch_3
        0x20 -> :sswitch_4
        0x2a -> :sswitch_5
    .end sparse-switch
.end method

.method public writeTo(Lcom/google/protobuf/nano/CodedOutputByteBufferNano;)V
    .locals 6
    .param p1, "output"    # Lcom/google/protobuf/nano/CodedOutputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const-wide/16 v4, -0x1

    .line 338
    iget-object v0, p0, Lpaymentfraud/mobile/DeviceFingerprinting$Parsed$State$PackageInfo;->name:Ljava/lang/String;

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 339
    const/4 v0, 0x1

    iget-object v1, p0, Lpaymentfraud/mobile/DeviceFingerprinting$Parsed$State$PackageInfo;->name:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeString(ILjava/lang/String;)V

    .line 341
    :cond_0
    iget-object v0, p0, Lpaymentfraud/mobile/DeviceFingerprinting$Parsed$State$PackageInfo;->versionCode:Ljava/lang/String;

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 342
    const/4 v0, 0x2

    iget-object v1, p0, Lpaymentfraud/mobile/DeviceFingerprinting$Parsed$State$PackageInfo;->versionCode:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeString(ILjava/lang/String;)V

    .line 344
    :cond_1
    iget-wide v0, p0, Lpaymentfraud/mobile/DeviceFingerprinting$Parsed$State$PackageInfo;->lastUpdateTime:J

    cmp-long v0, v0, v4

    if-eqz v0, :cond_2

    .line 345
    const/4 v0, 0x3

    iget-wide v2, p0, Lpaymentfraud/mobile/DeviceFingerprinting$Parsed$State$PackageInfo;->lastUpdateTime:J

    invoke-virtual {p1, v0, v2, v3}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeInt64(IJ)V

    .line 347
    :cond_2
    iget-wide v0, p0, Lpaymentfraud/mobile/DeviceFingerprinting$Parsed$State$PackageInfo;->firstInstallTime:J

    cmp-long v0, v0, v4

    if-eqz v0, :cond_3

    .line 348
    const/4 v0, 0x4

    iget-wide v2, p0, Lpaymentfraud/mobile/DeviceFingerprinting$Parsed$State$PackageInfo;->firstInstallTime:J

    invoke-virtual {p1, v0, v2, v3}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeInt64(IJ)V

    .line 350
    :cond_3
    iget-object v0, p0, Lpaymentfraud/mobile/DeviceFingerprinting$Parsed$State$PackageInfo;->installLocation:Ljava/lang/String;

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_4

    .line 351
    const/4 v0, 0x5

    iget-object v1, p0, Lpaymentfraud/mobile/DeviceFingerprinting$Parsed$State$PackageInfo;->installLocation:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeString(ILjava/lang/String;)V

    .line 353
    :cond_4
    invoke-super {p0, p1}, Lcom/google/protobuf/nano/MessageNano;->writeTo(Lcom/google/protobuf/nano/CodedOutputByteBufferNano;)V

    .line 354
    return-void
.end method
