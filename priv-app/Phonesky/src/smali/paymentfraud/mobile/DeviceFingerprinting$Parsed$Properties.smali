.class public final Lpaymentfraud/mobile/DeviceFingerprinting$Parsed$Properties;
.super Lcom/google/protobuf/nano/MessageNano;
.source "DeviceFingerprinting.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lpaymentfraud/mobile/DeviceFingerprinting$Parsed;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Properties"
.end annotation


# instance fields
.field public androidBuildBrand:Ljava/lang/String;

.field public androidId:J

.field public buildFingerprint:Ljava/lang/String;

.field public deviceName:Ljava/lang/String;

.field public esn:Ljava/lang/String;

.field public imei:Ljava/lang/String;

.field public manufacturer:Ljava/lang/String;

.field public meid:Ljava/lang/String;

.field public modelName:Ljava/lang/String;

.field public operatingSystem:I

.field public osVersion:Ljava/lang/String;

.field public phoneNumber:Ljava/lang/String;

.field public productName:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 72
    invoke-direct {p0}, Lcom/google/protobuf/nano/MessageNano;-><init>()V

    .line 73
    invoke-virtual {p0}, Lpaymentfraud/mobile/DeviceFingerprinting$Parsed$Properties;->clear()Lpaymentfraud/mobile/DeviceFingerprinting$Parsed$Properties;

    .line 74
    return-void
.end method


# virtual methods
.method public clear()Lpaymentfraud/mobile/DeviceFingerprinting$Parsed$Properties;
    .locals 2

    .prologue
    .line 77
    const/16 v0, 0x64

    iput v0, p0, Lpaymentfraud/mobile/DeviceFingerprinting$Parsed$Properties;->operatingSystem:I

    .line 78
    const-string v0, ""

    iput-object v0, p0, Lpaymentfraud/mobile/DeviceFingerprinting$Parsed$Properties;->imei:Ljava/lang/String;

    .line 79
    const-string v0, ""

    iput-object v0, p0, Lpaymentfraud/mobile/DeviceFingerprinting$Parsed$Properties;->meid:Ljava/lang/String;

    .line 80
    const-string v0, ""

    iput-object v0, p0, Lpaymentfraud/mobile/DeviceFingerprinting$Parsed$Properties;->esn:Ljava/lang/String;

    .line 81
    const-string v0, ""

    iput-object v0, p0, Lpaymentfraud/mobile/DeviceFingerprinting$Parsed$Properties;->phoneNumber:Ljava/lang/String;

    .line 82
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lpaymentfraud/mobile/DeviceFingerprinting$Parsed$Properties;->androidId:J

    .line 83
    const-string v0, ""

    iput-object v0, p0, Lpaymentfraud/mobile/DeviceFingerprinting$Parsed$Properties;->deviceName:Ljava/lang/String;

    .line 84
    const-string v0, ""

    iput-object v0, p0, Lpaymentfraud/mobile/DeviceFingerprinting$Parsed$Properties;->productName:Ljava/lang/String;

    .line 85
    const-string v0, ""

    iput-object v0, p0, Lpaymentfraud/mobile/DeviceFingerprinting$Parsed$Properties;->modelName:Ljava/lang/String;

    .line 86
    const-string v0, ""

    iput-object v0, p0, Lpaymentfraud/mobile/DeviceFingerprinting$Parsed$Properties;->manufacturer:Ljava/lang/String;

    .line 87
    const-string v0, ""

    iput-object v0, p0, Lpaymentfraud/mobile/DeviceFingerprinting$Parsed$Properties;->buildFingerprint:Ljava/lang/String;

    .line 88
    const-string v0, ""

    iput-object v0, p0, Lpaymentfraud/mobile/DeviceFingerprinting$Parsed$Properties;->osVersion:Ljava/lang/String;

    .line 89
    const-string v0, ""

    iput-object v0, p0, Lpaymentfraud/mobile/DeviceFingerprinting$Parsed$Properties;->androidBuildBrand:Ljava/lang/String;

    .line 90
    const/4 v0, -0x1

    iput v0, p0, Lpaymentfraud/mobile/DeviceFingerprinting$Parsed$Properties;->cachedSize:I

    .line 91
    return-object p0
.end method

.method protected computeSerializedSize()I
    .locals 6

    .prologue
    .line 141
    invoke-super {p0}, Lcom/google/protobuf/nano/MessageNano;->computeSerializedSize()I

    move-result v0

    .line 142
    .local v0, "size":I
    iget v1, p0, Lpaymentfraud/mobile/DeviceFingerprinting$Parsed$Properties;->operatingSystem:I

    const/16 v2, 0x64

    if-eq v1, v2, :cond_0

    .line 143
    const/4 v1, 0x1

    iget v2, p0, Lpaymentfraud/mobile/DeviceFingerprinting$Parsed$Properties;->operatingSystem:I

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeInt32Size(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 146
    :cond_0
    iget-object v1, p0, Lpaymentfraud/mobile/DeviceFingerprinting$Parsed$Properties;->imei:Ljava/lang/String;

    const-string v2, ""

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_1

    .line 147
    const/4 v1, 0x2

    iget-object v2, p0, Lpaymentfraud/mobile/DeviceFingerprinting$Parsed$Properties;->imei:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeStringSize(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 150
    :cond_1
    iget-object v1, p0, Lpaymentfraud/mobile/DeviceFingerprinting$Parsed$Properties;->meid:Ljava/lang/String;

    const-string v2, ""

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_2

    .line 151
    const/4 v1, 0x3

    iget-object v2, p0, Lpaymentfraud/mobile/DeviceFingerprinting$Parsed$Properties;->meid:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeStringSize(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 154
    :cond_2
    iget-object v1, p0, Lpaymentfraud/mobile/DeviceFingerprinting$Parsed$Properties;->esn:Ljava/lang/String;

    const-string v2, ""

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_3

    .line 155
    const/4 v1, 0x5

    iget-object v2, p0, Lpaymentfraud/mobile/DeviceFingerprinting$Parsed$Properties;->esn:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeStringSize(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 158
    :cond_3
    iget-object v1, p0, Lpaymentfraud/mobile/DeviceFingerprinting$Parsed$Properties;->phoneNumber:Ljava/lang/String;

    const-string v2, ""

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_4

    .line 159
    const/4 v1, 0x6

    iget-object v2, p0, Lpaymentfraud/mobile/DeviceFingerprinting$Parsed$Properties;->phoneNumber:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeStringSize(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 162
    :cond_4
    iget-wide v2, p0, Lpaymentfraud/mobile/DeviceFingerprinting$Parsed$Properties;->androidId:J

    const-wide/16 v4, 0x0

    cmp-long v1, v2, v4

    if-eqz v1, :cond_5

    .line 163
    const/4 v1, 0x7

    iget-wide v2, p0, Lpaymentfraud/mobile/DeviceFingerprinting$Parsed$Properties;->androidId:J

    invoke-static {v1, v2, v3}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeInt64Size(IJ)I

    move-result v1

    add-int/2addr v0, v1

    .line 166
    :cond_5
    iget-object v1, p0, Lpaymentfraud/mobile/DeviceFingerprinting$Parsed$Properties;->deviceName:Ljava/lang/String;

    const-string v2, ""

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_6

    .line 167
    const/16 v1, 0x9

    iget-object v2, p0, Lpaymentfraud/mobile/DeviceFingerprinting$Parsed$Properties;->deviceName:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeStringSize(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 170
    :cond_6
    iget-object v1, p0, Lpaymentfraud/mobile/DeviceFingerprinting$Parsed$Properties;->productName:Ljava/lang/String;

    const-string v2, ""

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_7

    .line 171
    const/16 v1, 0xa

    iget-object v2, p0, Lpaymentfraud/mobile/DeviceFingerprinting$Parsed$Properties;->productName:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeStringSize(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 174
    :cond_7
    iget-object v1, p0, Lpaymentfraud/mobile/DeviceFingerprinting$Parsed$Properties;->modelName:Ljava/lang/String;

    const-string v2, ""

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_8

    .line 175
    const/16 v1, 0xb

    iget-object v2, p0, Lpaymentfraud/mobile/DeviceFingerprinting$Parsed$Properties;->modelName:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeStringSize(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 178
    :cond_8
    iget-object v1, p0, Lpaymentfraud/mobile/DeviceFingerprinting$Parsed$Properties;->manufacturer:Ljava/lang/String;

    const-string v2, ""

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_9

    .line 179
    const/16 v1, 0xc

    iget-object v2, p0, Lpaymentfraud/mobile/DeviceFingerprinting$Parsed$Properties;->manufacturer:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeStringSize(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 182
    :cond_9
    iget-object v1, p0, Lpaymentfraud/mobile/DeviceFingerprinting$Parsed$Properties;->buildFingerprint:Ljava/lang/String;

    const-string v2, ""

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_a

    .line 183
    const/16 v1, 0xd

    iget-object v2, p0, Lpaymentfraud/mobile/DeviceFingerprinting$Parsed$Properties;->buildFingerprint:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeStringSize(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 186
    :cond_a
    iget-object v1, p0, Lpaymentfraud/mobile/DeviceFingerprinting$Parsed$Properties;->osVersion:Ljava/lang/String;

    const-string v2, ""

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_b

    .line 187
    const/16 v1, 0xf

    iget-object v2, p0, Lpaymentfraud/mobile/DeviceFingerprinting$Parsed$Properties;->osVersion:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeStringSize(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 190
    :cond_b
    iget-object v1, p0, Lpaymentfraud/mobile/DeviceFingerprinting$Parsed$Properties;->androidBuildBrand:Ljava/lang/String;

    const-string v2, ""

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_c

    .line 191
    const/16 v1, 0x15

    iget-object v2, p0, Lpaymentfraud/mobile/DeviceFingerprinting$Parsed$Properties;->androidBuildBrand:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeStringSize(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 194
    :cond_c
    return v0
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/protobuf/nano/MessageNano;
    .locals 1
    .param p1, "x0"    # Lcom/google/protobuf/nano/CodedInputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 12
    invoke-virtual {p0, p1}, Lpaymentfraud/mobile/DeviceFingerprinting$Parsed$Properties;->mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lpaymentfraud/mobile/DeviceFingerprinting$Parsed$Properties;

    move-result-object v0

    return-object v0
.end method

.method public mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lpaymentfraud/mobile/DeviceFingerprinting$Parsed$Properties;
    .locals 4
    .param p1, "input"    # Lcom/google/protobuf/nano/CodedInputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 202
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readTag()I

    move-result v0

    .line 203
    .local v0, "tag":I
    sparse-switch v0, :sswitch_data_0

    .line 207
    invoke-static {p1, v0}, Lcom/google/protobuf/nano/WireFormatNano;->parseUnknownField(Lcom/google/protobuf/nano/CodedInputByteBufferNano;I)Z

    move-result v2

    if-nez v2, :cond_0

    .line 208
    :sswitch_0
    return-object p0

    .line 213
    :sswitch_1
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readInt32()I

    move-result v1

    .line 214
    .local v1, "value":I
    sparse-switch v1, :sswitch_data_1

    goto :goto_0

    .line 217
    :sswitch_2
    iput v1, p0, Lpaymentfraud/mobile/DeviceFingerprinting$Parsed$Properties;->operatingSystem:I

    goto :goto_0

    .line 223
    .end local v1    # "value":I
    :sswitch_3
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readString()Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lpaymentfraud/mobile/DeviceFingerprinting$Parsed$Properties;->imei:Ljava/lang/String;

    goto :goto_0

    .line 227
    :sswitch_4
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readString()Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lpaymentfraud/mobile/DeviceFingerprinting$Parsed$Properties;->meid:Ljava/lang/String;

    goto :goto_0

    .line 231
    :sswitch_5
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readString()Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lpaymentfraud/mobile/DeviceFingerprinting$Parsed$Properties;->esn:Ljava/lang/String;

    goto :goto_0

    .line 235
    :sswitch_6
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readString()Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lpaymentfraud/mobile/DeviceFingerprinting$Parsed$Properties;->phoneNumber:Ljava/lang/String;

    goto :goto_0

    .line 239
    :sswitch_7
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readInt64()J

    move-result-wide v2

    iput-wide v2, p0, Lpaymentfraud/mobile/DeviceFingerprinting$Parsed$Properties;->androidId:J

    goto :goto_0

    .line 243
    :sswitch_8
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readString()Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lpaymentfraud/mobile/DeviceFingerprinting$Parsed$Properties;->deviceName:Ljava/lang/String;

    goto :goto_0

    .line 247
    :sswitch_9
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readString()Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lpaymentfraud/mobile/DeviceFingerprinting$Parsed$Properties;->productName:Ljava/lang/String;

    goto :goto_0

    .line 251
    :sswitch_a
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readString()Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lpaymentfraud/mobile/DeviceFingerprinting$Parsed$Properties;->modelName:Ljava/lang/String;

    goto :goto_0

    .line 255
    :sswitch_b
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readString()Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lpaymentfraud/mobile/DeviceFingerprinting$Parsed$Properties;->manufacturer:Ljava/lang/String;

    goto :goto_0

    .line 259
    :sswitch_c
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readString()Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lpaymentfraud/mobile/DeviceFingerprinting$Parsed$Properties;->buildFingerprint:Ljava/lang/String;

    goto :goto_0

    .line 263
    :sswitch_d
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readString()Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lpaymentfraud/mobile/DeviceFingerprinting$Parsed$Properties;->osVersion:Ljava/lang/String;

    goto :goto_0

    .line 267
    :sswitch_e
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readString()Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lpaymentfraud/mobile/DeviceFingerprinting$Parsed$Properties;->androidBuildBrand:Ljava/lang/String;

    goto :goto_0

    .line 203
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x12 -> :sswitch_3
        0x1a -> :sswitch_4
        0x2a -> :sswitch_5
        0x32 -> :sswitch_6
        0x38 -> :sswitch_7
        0x4a -> :sswitch_8
        0x52 -> :sswitch_9
        0x5a -> :sswitch_a
        0x62 -> :sswitch_b
        0x6a -> :sswitch_c
        0x7a -> :sswitch_d
        0xaa -> :sswitch_e
    .end sparse-switch

    .line 214
    :sswitch_data_1
    .sparse-switch
        0x0 -> :sswitch_2
        0x64 -> :sswitch_2
    .end sparse-switch
.end method

.method public writeTo(Lcom/google/protobuf/nano/CodedOutputByteBufferNano;)V
    .locals 4
    .param p1, "output"    # Lcom/google/protobuf/nano/CodedOutputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 97
    iget v0, p0, Lpaymentfraud/mobile/DeviceFingerprinting$Parsed$Properties;->operatingSystem:I

    const/16 v1, 0x64

    if-eq v0, v1, :cond_0

    .line 98
    const/4 v0, 0x1

    iget v1, p0, Lpaymentfraud/mobile/DeviceFingerprinting$Parsed$Properties;->operatingSystem:I

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeInt32(II)V

    .line 100
    :cond_0
    iget-object v0, p0, Lpaymentfraud/mobile/DeviceFingerprinting$Parsed$Properties;->imei:Ljava/lang/String;

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 101
    const/4 v0, 0x2

    iget-object v1, p0, Lpaymentfraud/mobile/DeviceFingerprinting$Parsed$Properties;->imei:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeString(ILjava/lang/String;)V

    .line 103
    :cond_1
    iget-object v0, p0, Lpaymentfraud/mobile/DeviceFingerprinting$Parsed$Properties;->meid:Ljava/lang/String;

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_2

    .line 104
    const/4 v0, 0x3

    iget-object v1, p0, Lpaymentfraud/mobile/DeviceFingerprinting$Parsed$Properties;->meid:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeString(ILjava/lang/String;)V

    .line 106
    :cond_2
    iget-object v0, p0, Lpaymentfraud/mobile/DeviceFingerprinting$Parsed$Properties;->esn:Ljava/lang/String;

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_3

    .line 107
    const/4 v0, 0x5

    iget-object v1, p0, Lpaymentfraud/mobile/DeviceFingerprinting$Parsed$Properties;->esn:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeString(ILjava/lang/String;)V

    .line 109
    :cond_3
    iget-object v0, p0, Lpaymentfraud/mobile/DeviceFingerprinting$Parsed$Properties;->phoneNumber:Ljava/lang/String;

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_4

    .line 110
    const/4 v0, 0x6

    iget-object v1, p0, Lpaymentfraud/mobile/DeviceFingerprinting$Parsed$Properties;->phoneNumber:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeString(ILjava/lang/String;)V

    .line 112
    :cond_4
    iget-wide v0, p0, Lpaymentfraud/mobile/DeviceFingerprinting$Parsed$Properties;->androidId:J

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-eqz v0, :cond_5

    .line 113
    const/4 v0, 0x7

    iget-wide v2, p0, Lpaymentfraud/mobile/DeviceFingerprinting$Parsed$Properties;->androidId:J

    invoke-virtual {p1, v0, v2, v3}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeInt64(IJ)V

    .line 115
    :cond_5
    iget-object v0, p0, Lpaymentfraud/mobile/DeviceFingerprinting$Parsed$Properties;->deviceName:Ljava/lang/String;

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_6

    .line 116
    const/16 v0, 0x9

    iget-object v1, p0, Lpaymentfraud/mobile/DeviceFingerprinting$Parsed$Properties;->deviceName:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeString(ILjava/lang/String;)V

    .line 118
    :cond_6
    iget-object v0, p0, Lpaymentfraud/mobile/DeviceFingerprinting$Parsed$Properties;->productName:Ljava/lang/String;

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_7

    .line 119
    const/16 v0, 0xa

    iget-object v1, p0, Lpaymentfraud/mobile/DeviceFingerprinting$Parsed$Properties;->productName:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeString(ILjava/lang/String;)V

    .line 121
    :cond_7
    iget-object v0, p0, Lpaymentfraud/mobile/DeviceFingerprinting$Parsed$Properties;->modelName:Ljava/lang/String;

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_8

    .line 122
    const/16 v0, 0xb

    iget-object v1, p0, Lpaymentfraud/mobile/DeviceFingerprinting$Parsed$Properties;->modelName:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeString(ILjava/lang/String;)V

    .line 124
    :cond_8
    iget-object v0, p0, Lpaymentfraud/mobile/DeviceFingerprinting$Parsed$Properties;->manufacturer:Ljava/lang/String;

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_9

    .line 125
    const/16 v0, 0xc

    iget-object v1, p0, Lpaymentfraud/mobile/DeviceFingerprinting$Parsed$Properties;->manufacturer:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeString(ILjava/lang/String;)V

    .line 127
    :cond_9
    iget-object v0, p0, Lpaymentfraud/mobile/DeviceFingerprinting$Parsed$Properties;->buildFingerprint:Ljava/lang/String;

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_a

    .line 128
    const/16 v0, 0xd

    iget-object v1, p0, Lpaymentfraud/mobile/DeviceFingerprinting$Parsed$Properties;->buildFingerprint:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeString(ILjava/lang/String;)V

    .line 130
    :cond_a
    iget-object v0, p0, Lpaymentfraud/mobile/DeviceFingerprinting$Parsed$Properties;->osVersion:Ljava/lang/String;

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_b

    .line 131
    const/16 v0, 0xf

    iget-object v1, p0, Lpaymentfraud/mobile/DeviceFingerprinting$Parsed$Properties;->osVersion:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeString(ILjava/lang/String;)V

    .line 133
    :cond_b
    iget-object v0, p0, Lpaymentfraud/mobile/DeviceFingerprinting$Parsed$Properties;->androidBuildBrand:Ljava/lang/String;

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_c

    .line 134
    const/16 v0, 0x15

    iget-object v1, p0, Lpaymentfraud/mobile/DeviceFingerprinting$Parsed$Properties;->androidBuildBrand:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeString(ILjava/lang/String;)V

    .line 136
    :cond_c
    invoke-super {p0, p1}, Lcom/google/protobuf/nano/MessageNano;->writeTo(Lcom/google/protobuf/nano/CodedOutputByteBufferNano;)V

    .line 137
    return-void
.end method
