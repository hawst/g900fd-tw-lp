.class public final Lpaymentfraud/mobile/DeviceFingerprinting$Parsed$State$Location;
.super Lcom/google/protobuf/nano/MessageNano;
.source "DeviceFingerprinting.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lpaymentfraud/mobile/DeviceFingerprinting$Parsed$State;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Location"
.end annotation


# instance fields
.field public accuracy:F

.field public altitude:D

.field public latitude:D

.field public longitude:D

.field public timeInMs:D


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 465
    invoke-direct {p0}, Lcom/google/protobuf/nano/MessageNano;-><init>()V

    .line 466
    invoke-virtual {p0}, Lpaymentfraud/mobile/DeviceFingerprinting$Parsed$State$Location;->clear()Lpaymentfraud/mobile/DeviceFingerprinting$Parsed$State$Location;

    .line 467
    return-void
.end method


# virtual methods
.method public clear()Lpaymentfraud/mobile/DeviceFingerprinting$Parsed$State$Location;
    .locals 2

    .prologue
    const-wide/16 v0, 0x0

    .line 470
    iput-wide v0, p0, Lpaymentfraud/mobile/DeviceFingerprinting$Parsed$State$Location;->altitude:D

    .line 471
    iput-wide v0, p0, Lpaymentfraud/mobile/DeviceFingerprinting$Parsed$State$Location;->latitude:D

    .line 472
    iput-wide v0, p0, Lpaymentfraud/mobile/DeviceFingerprinting$Parsed$State$Location;->longitude:D

    .line 473
    const/4 v0, 0x0

    iput v0, p0, Lpaymentfraud/mobile/DeviceFingerprinting$Parsed$State$Location;->accuracy:F

    .line 474
    const-wide/high16 v0, -0x4010000000000000L    # -1.0

    iput-wide v0, p0, Lpaymentfraud/mobile/DeviceFingerprinting$Parsed$State$Location;->timeInMs:D

    .line 475
    const/4 v0, -0x1

    iput v0, p0, Lpaymentfraud/mobile/DeviceFingerprinting$Parsed$State$Location;->cachedSize:I

    .line 476
    return-object p0
.end method

.method protected computeSerializedSize()I
    .locals 8

    .prologue
    const-wide/16 v6, 0x0

    .line 507
    invoke-super {p0}, Lcom/google/protobuf/nano/MessageNano;->computeSerializedSize()I

    move-result v0

    .line 508
    .local v0, "size":I
    iget-wide v2, p0, Lpaymentfraud/mobile/DeviceFingerprinting$Parsed$State$Location;->altitude:D

    invoke-static {v2, v3}, Ljava/lang/Double;->doubleToLongBits(D)J

    move-result-wide v2

    invoke-static {v6, v7}, Ljava/lang/Double;->doubleToLongBits(D)J

    move-result-wide v4

    cmp-long v1, v2, v4

    if-eqz v1, :cond_0

    .line 510
    const/4 v1, 0x1

    iget-wide v2, p0, Lpaymentfraud/mobile/DeviceFingerprinting$Parsed$State$Location;->altitude:D

    invoke-static {v1, v2, v3}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeDoubleSize(ID)I

    move-result v1

    add-int/2addr v0, v1

    .line 513
    :cond_0
    iget-wide v2, p0, Lpaymentfraud/mobile/DeviceFingerprinting$Parsed$State$Location;->latitude:D

    invoke-static {v2, v3}, Ljava/lang/Double;->doubleToLongBits(D)J

    move-result-wide v2

    invoke-static {v6, v7}, Ljava/lang/Double;->doubleToLongBits(D)J

    move-result-wide v4

    cmp-long v1, v2, v4

    if-eqz v1, :cond_1

    .line 515
    const/4 v1, 0x2

    iget-wide v2, p0, Lpaymentfraud/mobile/DeviceFingerprinting$Parsed$State$Location;->latitude:D

    invoke-static {v1, v2, v3}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeDoubleSize(ID)I

    move-result v1

    add-int/2addr v0, v1

    .line 518
    :cond_1
    iget-wide v2, p0, Lpaymentfraud/mobile/DeviceFingerprinting$Parsed$State$Location;->longitude:D

    invoke-static {v2, v3}, Ljava/lang/Double;->doubleToLongBits(D)J

    move-result-wide v2

    invoke-static {v6, v7}, Ljava/lang/Double;->doubleToLongBits(D)J

    move-result-wide v4

    cmp-long v1, v2, v4

    if-eqz v1, :cond_2

    .line 520
    const/4 v1, 0x3

    iget-wide v2, p0, Lpaymentfraud/mobile/DeviceFingerprinting$Parsed$State$Location;->longitude:D

    invoke-static {v1, v2, v3}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeDoubleSize(ID)I

    move-result v1

    add-int/2addr v0, v1

    .line 523
    :cond_2
    iget v1, p0, Lpaymentfraud/mobile/DeviceFingerprinting$Parsed$State$Location;->accuracy:F

    invoke-static {v1}, Ljava/lang/Float;->floatToIntBits(F)I

    move-result v1

    const/4 v2, 0x0

    invoke-static {v2}, Ljava/lang/Float;->floatToIntBits(F)I

    move-result v2

    if-eq v1, v2, :cond_3

    .line 525
    const/4 v1, 0x4

    iget v2, p0, Lpaymentfraud/mobile/DeviceFingerprinting$Parsed$State$Location;->accuracy:F

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeFloatSize(IF)I

    move-result v1

    add-int/2addr v0, v1

    .line 528
    :cond_3
    iget-wide v2, p0, Lpaymentfraud/mobile/DeviceFingerprinting$Parsed$State$Location;->timeInMs:D

    invoke-static {v2, v3}, Ljava/lang/Double;->doubleToLongBits(D)J

    move-result-wide v2

    const-wide/high16 v4, -0x4010000000000000L    # -1.0

    invoke-static {v4, v5}, Ljava/lang/Double;->doubleToLongBits(D)J

    move-result-wide v4

    cmp-long v1, v2, v4

    if-eqz v1, :cond_4

    .line 530
    const/4 v1, 0x5

    iget-wide v2, p0, Lpaymentfraud/mobile/DeviceFingerprinting$Parsed$State$Location;->timeInMs:D

    invoke-static {v1, v2, v3}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeDoubleSize(ID)I

    move-result v1

    add-int/2addr v0, v1

    .line 533
    :cond_4
    return v0
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/protobuf/nano/MessageNano;
    .locals 1
    .param p1, "x0"    # Lcom/google/protobuf/nano/CodedInputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 433
    invoke-virtual {p0, p1}, Lpaymentfraud/mobile/DeviceFingerprinting$Parsed$State$Location;->mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lpaymentfraud/mobile/DeviceFingerprinting$Parsed$State$Location;

    move-result-object v0

    return-object v0
.end method

.method public mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lpaymentfraud/mobile/DeviceFingerprinting$Parsed$State$Location;
    .locals 4
    .param p1, "input"    # Lcom/google/protobuf/nano/CodedInputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 541
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readTag()I

    move-result v0

    .line 542
    .local v0, "tag":I
    sparse-switch v0, :sswitch_data_0

    .line 546
    invoke-static {p1, v0}, Lcom/google/protobuf/nano/WireFormatNano;->parseUnknownField(Lcom/google/protobuf/nano/CodedInputByteBufferNano;I)Z

    move-result v1

    if-nez v1, :cond_0

    .line 547
    :sswitch_0
    return-object p0

    .line 552
    :sswitch_1
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readDouble()D

    move-result-wide v2

    iput-wide v2, p0, Lpaymentfraud/mobile/DeviceFingerprinting$Parsed$State$Location;->altitude:D

    goto :goto_0

    .line 556
    :sswitch_2
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readDouble()D

    move-result-wide v2

    iput-wide v2, p0, Lpaymentfraud/mobile/DeviceFingerprinting$Parsed$State$Location;->latitude:D

    goto :goto_0

    .line 560
    :sswitch_3
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readDouble()D

    move-result-wide v2

    iput-wide v2, p0, Lpaymentfraud/mobile/DeviceFingerprinting$Parsed$State$Location;->longitude:D

    goto :goto_0

    .line 564
    :sswitch_4
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readFloat()F

    move-result v1

    iput v1, p0, Lpaymentfraud/mobile/DeviceFingerprinting$Parsed$State$Location;->accuracy:F

    goto :goto_0

    .line 568
    :sswitch_5
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readDouble()D

    move-result-wide v2

    iput-wide v2, p0, Lpaymentfraud/mobile/DeviceFingerprinting$Parsed$State$Location;->timeInMs:D

    goto :goto_0

    .line 542
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x9 -> :sswitch_1
        0x11 -> :sswitch_2
        0x19 -> :sswitch_3
        0x25 -> :sswitch_4
        0x29 -> :sswitch_5
    .end sparse-switch
.end method

.method public writeTo(Lcom/google/protobuf/nano/CodedOutputByteBufferNano;)V
    .locals 6
    .param p1, "output"    # Lcom/google/protobuf/nano/CodedOutputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const-wide/16 v4, 0x0

    .line 482
    iget-wide v0, p0, Lpaymentfraud/mobile/DeviceFingerprinting$Parsed$State$Location;->altitude:D

    invoke-static {v0, v1}, Ljava/lang/Double;->doubleToLongBits(D)J

    move-result-wide v0

    invoke-static {v4, v5}, Ljava/lang/Double;->doubleToLongBits(D)J

    move-result-wide v2

    cmp-long v0, v0, v2

    if-eqz v0, :cond_0

    .line 484
    const/4 v0, 0x1

    iget-wide v2, p0, Lpaymentfraud/mobile/DeviceFingerprinting$Parsed$State$Location;->altitude:D

    invoke-virtual {p1, v0, v2, v3}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeDouble(ID)V

    .line 486
    :cond_0
    iget-wide v0, p0, Lpaymentfraud/mobile/DeviceFingerprinting$Parsed$State$Location;->latitude:D

    invoke-static {v0, v1}, Ljava/lang/Double;->doubleToLongBits(D)J

    move-result-wide v0

    invoke-static {v4, v5}, Ljava/lang/Double;->doubleToLongBits(D)J

    move-result-wide v2

    cmp-long v0, v0, v2

    if-eqz v0, :cond_1

    .line 488
    const/4 v0, 0x2

    iget-wide v2, p0, Lpaymentfraud/mobile/DeviceFingerprinting$Parsed$State$Location;->latitude:D

    invoke-virtual {p1, v0, v2, v3}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeDouble(ID)V

    .line 490
    :cond_1
    iget-wide v0, p0, Lpaymentfraud/mobile/DeviceFingerprinting$Parsed$State$Location;->longitude:D

    invoke-static {v0, v1}, Ljava/lang/Double;->doubleToLongBits(D)J

    move-result-wide v0

    invoke-static {v4, v5}, Ljava/lang/Double;->doubleToLongBits(D)J

    move-result-wide v2

    cmp-long v0, v0, v2

    if-eqz v0, :cond_2

    .line 492
    const/4 v0, 0x3

    iget-wide v2, p0, Lpaymentfraud/mobile/DeviceFingerprinting$Parsed$State$Location;->longitude:D

    invoke-virtual {p1, v0, v2, v3}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeDouble(ID)V

    .line 494
    :cond_2
    iget v0, p0, Lpaymentfraud/mobile/DeviceFingerprinting$Parsed$State$Location;->accuracy:F

    invoke-static {v0}, Ljava/lang/Float;->floatToIntBits(F)I

    move-result v0

    const/4 v1, 0x0

    invoke-static {v1}, Ljava/lang/Float;->floatToIntBits(F)I

    move-result v1

    if-eq v0, v1, :cond_3

    .line 496
    const/4 v0, 0x4

    iget v1, p0, Lpaymentfraud/mobile/DeviceFingerprinting$Parsed$State$Location;->accuracy:F

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeFloat(IF)V

    .line 498
    :cond_3
    iget-wide v0, p0, Lpaymentfraud/mobile/DeviceFingerprinting$Parsed$State$Location;->timeInMs:D

    invoke-static {v0, v1}, Ljava/lang/Double;->doubleToLongBits(D)J

    move-result-wide v0

    const-wide/high16 v2, -0x4010000000000000L    # -1.0

    invoke-static {v2, v3}, Ljava/lang/Double;->doubleToLongBits(D)J

    move-result-wide v2

    cmp-long v0, v0, v2

    if-eqz v0, :cond_4

    .line 500
    const/4 v0, 0x5

    iget-wide v2, p0, Lpaymentfraud/mobile/DeviceFingerprinting$Parsed$State$Location;->timeInMs:D

    invoke-virtual {p1, v0, v2, v3}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeDouble(ID)V

    .line 502
    :cond_4
    invoke-super {p0, p1}, Lcom/google/protobuf/nano/MessageNano;->writeTo(Lcom/google/protobuf/nano/CodedOutputByteBufferNano;)V

    .line 503
    return-void
.end method
