.class public final Lpaymentfraud/mobile/DeviceFingerprinting$Parsed$State;
.super Lcom/google/protobuf/nano/MessageNano;
.source "DeviceFingerprinting.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lpaymentfraud/mobile/DeviceFingerprinting$Parsed;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "State"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lpaymentfraud/mobile/DeviceFingerprinting$Parsed$State$Location;,
        Lpaymentfraud/mobile/DeviceFingerprinting$Parsed$State$PackageInfo;
    }
.end annotation


# instance fields
.field public cellOperator:Ljava/lang/String;

.field public devModeOn:Z

.field public gmtOffsetMillis:J

.field public installedPackages:[Lpaymentfraud/mobile/DeviceFingerprinting$Parsed$State$PackageInfo;

.field public ipAddr:[Ljava/lang/String;

.field public language:Ljava/lang/String;

.field public lastGpsLocation:Lpaymentfraud/mobile/DeviceFingerprinting$Parsed$State$Location;

.field public locale:Ljava/lang/String;

.field public nonPlayInstallAllowed:Z

.field public percentBattery:I

.field public simOperator:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 634
    invoke-direct {p0}, Lcom/google/protobuf/nano/MessageNano;-><init>()V

    .line 635
    invoke-virtual {p0}, Lpaymentfraud/mobile/DeviceFingerprinting$Parsed$State;->clear()Lpaymentfraud/mobile/DeviceFingerprinting$Parsed$State;

    .line 636
    return-void
.end method


# virtual methods
.method public clear()Lpaymentfraud/mobile/DeviceFingerprinting$Parsed$State;
    .locals 4

    .prologue
    const/4 v3, 0x0

    const/4 v2, -0x1

    .line 639
    invoke-static {}, Lpaymentfraud/mobile/DeviceFingerprinting$Parsed$State$PackageInfo;->emptyArray()[Lpaymentfraud/mobile/DeviceFingerprinting$Parsed$State$PackageInfo;

    move-result-object v0

    iput-object v0, p0, Lpaymentfraud/mobile/DeviceFingerprinting$Parsed$State;->installedPackages:[Lpaymentfraud/mobile/DeviceFingerprinting$Parsed$State$PackageInfo;

    .line 640
    iput v2, p0, Lpaymentfraud/mobile/DeviceFingerprinting$Parsed$State;->percentBattery:I

    .line 641
    const-wide/32 v0, -0x5265c00

    iput-wide v0, p0, Lpaymentfraud/mobile/DeviceFingerprinting$Parsed$State;->gmtOffsetMillis:J

    .line 642
    const/4 v0, 0x0

    iput-object v0, p0, Lpaymentfraud/mobile/DeviceFingerprinting$Parsed$State;->lastGpsLocation:Lpaymentfraud/mobile/DeviceFingerprinting$Parsed$State$Location;

    .line 643
    iput-boolean v3, p0, Lpaymentfraud/mobile/DeviceFingerprinting$Parsed$State;->devModeOn:Z

    .line 644
    iput-boolean v3, p0, Lpaymentfraud/mobile/DeviceFingerprinting$Parsed$State;->nonPlayInstallAllowed:Z

    .line 645
    const-string v0, ""

    iput-object v0, p0, Lpaymentfraud/mobile/DeviceFingerprinting$Parsed$State;->language:Ljava/lang/String;

    .line 646
    sget-object v0, Lcom/google/protobuf/nano/WireFormatNano;->EMPTY_STRING_ARRAY:[Ljava/lang/String;

    iput-object v0, p0, Lpaymentfraud/mobile/DeviceFingerprinting$Parsed$State;->ipAddr:[Ljava/lang/String;

    .line 647
    const-string v0, ""

    iput-object v0, p0, Lpaymentfraud/mobile/DeviceFingerprinting$Parsed$State;->locale:Ljava/lang/String;

    .line 648
    const-string v0, ""

    iput-object v0, p0, Lpaymentfraud/mobile/DeviceFingerprinting$Parsed$State;->cellOperator:Ljava/lang/String;

    .line 649
    const-string v0, ""

    iput-object v0, p0, Lpaymentfraud/mobile/DeviceFingerprinting$Parsed$State;->simOperator:Ljava/lang/String;

    .line 650
    iput v2, p0, Lpaymentfraud/mobile/DeviceFingerprinting$Parsed$State;->cachedSize:I

    .line 651
    return-object p0
.end method

.method protected computeSerializedSize()I
    .locals 10

    .prologue
    .line 705
    invoke-super {p0}, Lcom/google/protobuf/nano/MessageNano;->computeSerializedSize()I

    move-result v4

    .line 706
    .local v4, "size":I
    iget-object v5, p0, Lpaymentfraud/mobile/DeviceFingerprinting$Parsed$State;->installedPackages:[Lpaymentfraud/mobile/DeviceFingerprinting$Parsed$State$PackageInfo;

    if-eqz v5, :cond_1

    iget-object v5, p0, Lpaymentfraud/mobile/DeviceFingerprinting$Parsed$State;->installedPackages:[Lpaymentfraud/mobile/DeviceFingerprinting$Parsed$State$PackageInfo;

    array-length v5, v5

    if-lez v5, :cond_1

    .line 707
    const/4 v3, 0x0

    .local v3, "i":I
    :goto_0
    iget-object v5, p0, Lpaymentfraud/mobile/DeviceFingerprinting$Parsed$State;->installedPackages:[Lpaymentfraud/mobile/DeviceFingerprinting$Parsed$State$PackageInfo;

    array-length v5, v5

    if-ge v3, v5, :cond_1

    .line 708
    iget-object v5, p0, Lpaymentfraud/mobile/DeviceFingerprinting$Parsed$State;->installedPackages:[Lpaymentfraud/mobile/DeviceFingerprinting$Parsed$State$PackageInfo;

    aget-object v2, v5, v3

    .line 709
    .local v2, "element":Lpaymentfraud/mobile/DeviceFingerprinting$Parsed$State$PackageInfo;
    if-eqz v2, :cond_0

    .line 710
    const/4 v5, 0x1

    invoke-static {v5, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeMessageSize(ILcom/google/protobuf/nano/MessageNano;)I

    move-result v5

    add-int/2addr v4, v5

    .line 707
    :cond_0
    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    .line 715
    .end local v2    # "element":Lpaymentfraud/mobile/DeviceFingerprinting$Parsed$State$PackageInfo;
    .end local v3    # "i":I
    :cond_1
    iget v5, p0, Lpaymentfraud/mobile/DeviceFingerprinting$Parsed$State;->percentBattery:I

    const/4 v6, -0x1

    if-eq v5, v6, :cond_2

    .line 716
    const/4 v5, 0x3

    iget v6, p0, Lpaymentfraud/mobile/DeviceFingerprinting$Parsed$State;->percentBattery:I

    invoke-static {v5, v6}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeInt32Size(II)I

    move-result v5

    add-int/2addr v4, v5

    .line 719
    :cond_2
    iget-wide v6, p0, Lpaymentfraud/mobile/DeviceFingerprinting$Parsed$State;->gmtOffsetMillis:J

    const-wide/32 v8, -0x5265c00

    cmp-long v5, v6, v8

    if-eqz v5, :cond_3

    .line 720
    const/4 v5, 0x4

    iget-wide v6, p0, Lpaymentfraud/mobile/DeviceFingerprinting$Parsed$State;->gmtOffsetMillis:J

    invoke-static {v5, v6, v7}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeInt64Size(IJ)I

    move-result v5

    add-int/2addr v4, v5

    .line 723
    :cond_3
    iget-object v5, p0, Lpaymentfraud/mobile/DeviceFingerprinting$Parsed$State;->lastGpsLocation:Lpaymentfraud/mobile/DeviceFingerprinting$Parsed$State$Location;

    if-eqz v5, :cond_4

    .line 724
    const/4 v5, 0x6

    iget-object v6, p0, Lpaymentfraud/mobile/DeviceFingerprinting$Parsed$State;->lastGpsLocation:Lpaymentfraud/mobile/DeviceFingerprinting$Parsed$State$Location;

    invoke-static {v5, v6}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeMessageSize(ILcom/google/protobuf/nano/MessageNano;)I

    move-result v5

    add-int/2addr v4, v5

    .line 727
    :cond_4
    iget-boolean v5, p0, Lpaymentfraud/mobile/DeviceFingerprinting$Parsed$State;->devModeOn:Z

    if-eqz v5, :cond_5

    .line 728
    const/4 v5, 0x7

    iget-boolean v6, p0, Lpaymentfraud/mobile/DeviceFingerprinting$Parsed$State;->devModeOn:Z

    invoke-static {v5, v6}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeBoolSize(IZ)I

    move-result v5

    add-int/2addr v4, v5

    .line 731
    :cond_5
    iget-boolean v5, p0, Lpaymentfraud/mobile/DeviceFingerprinting$Parsed$State;->nonPlayInstallAllowed:Z

    if-eqz v5, :cond_6

    .line 732
    const/16 v5, 0x8

    iget-boolean v6, p0, Lpaymentfraud/mobile/DeviceFingerprinting$Parsed$State;->nonPlayInstallAllowed:Z

    invoke-static {v5, v6}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeBoolSize(IZ)I

    move-result v5

    add-int/2addr v4, v5

    .line 735
    :cond_6
    iget-object v5, p0, Lpaymentfraud/mobile/DeviceFingerprinting$Parsed$State;->language:Ljava/lang/String;

    const-string v6, ""

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-nez v5, :cond_7

    .line 736
    const/16 v5, 0x9

    iget-object v6, p0, Lpaymentfraud/mobile/DeviceFingerprinting$Parsed$State;->language:Ljava/lang/String;

    invoke-static {v5, v6}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeStringSize(ILjava/lang/String;)I

    move-result v5

    add-int/2addr v4, v5

    .line 739
    :cond_7
    iget-object v5, p0, Lpaymentfraud/mobile/DeviceFingerprinting$Parsed$State;->ipAddr:[Ljava/lang/String;

    if-eqz v5, :cond_a

    iget-object v5, p0, Lpaymentfraud/mobile/DeviceFingerprinting$Parsed$State;->ipAddr:[Ljava/lang/String;

    array-length v5, v5

    if-lez v5, :cond_a

    .line 740
    const/4 v0, 0x0

    .line 741
    .local v0, "dataCount":I
    const/4 v1, 0x0

    .line 742
    .local v1, "dataSize":I
    const/4 v3, 0x0

    .restart local v3    # "i":I
    :goto_1
    iget-object v5, p0, Lpaymentfraud/mobile/DeviceFingerprinting$Parsed$State;->ipAddr:[Ljava/lang/String;

    array-length v5, v5

    if-ge v3, v5, :cond_9

    .line 743
    iget-object v5, p0, Lpaymentfraud/mobile/DeviceFingerprinting$Parsed$State;->ipAddr:[Ljava/lang/String;

    aget-object v2, v5, v3

    .line 744
    .local v2, "element":Ljava/lang/String;
    if-eqz v2, :cond_8

    .line 745
    add-int/lit8 v0, v0, 0x1

    .line 746
    invoke-static {v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeStringSizeNoTag(Ljava/lang/String;)I

    move-result v5

    add-int/2addr v1, v5

    .line 742
    :cond_8
    add-int/lit8 v3, v3, 0x1

    goto :goto_1

    .line 750
    .end local v2    # "element":Ljava/lang/String;
    :cond_9
    add-int/2addr v4, v1

    .line 751
    mul-int/lit8 v5, v0, 0x1

    add-int/2addr v4, v5

    .line 753
    .end local v0    # "dataCount":I
    .end local v1    # "dataSize":I
    .end local v3    # "i":I
    :cond_a
    iget-object v5, p0, Lpaymentfraud/mobile/DeviceFingerprinting$Parsed$State;->locale:Ljava/lang/String;

    const-string v6, ""

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-nez v5, :cond_b

    .line 754
    const/16 v5, 0xb

    iget-object v6, p0, Lpaymentfraud/mobile/DeviceFingerprinting$Parsed$State;->locale:Ljava/lang/String;

    invoke-static {v5, v6}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeStringSize(ILjava/lang/String;)I

    move-result v5

    add-int/2addr v4, v5

    .line 757
    :cond_b
    iget-object v5, p0, Lpaymentfraud/mobile/DeviceFingerprinting$Parsed$State;->cellOperator:Ljava/lang/String;

    const-string v6, ""

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-nez v5, :cond_c

    .line 758
    const/16 v5, 0xe

    iget-object v6, p0, Lpaymentfraud/mobile/DeviceFingerprinting$Parsed$State;->cellOperator:Ljava/lang/String;

    invoke-static {v5, v6}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeStringSize(ILjava/lang/String;)I

    move-result v5

    add-int/2addr v4, v5

    .line 761
    :cond_c
    iget-object v5, p0, Lpaymentfraud/mobile/DeviceFingerprinting$Parsed$State;->simOperator:Ljava/lang/String;

    const-string v6, ""

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-nez v5, :cond_d

    .line 762
    const/16 v5, 0xf

    iget-object v6, p0, Lpaymentfraud/mobile/DeviceFingerprinting$Parsed$State;->simOperator:Ljava/lang/String;

    invoke-static {v5, v6}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeStringSize(ILjava/lang/String;)I

    move-result v5

    add-int/2addr v4, v5

    .line 765
    :cond_d
    return v4
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/protobuf/nano/MessageNano;
    .locals 1
    .param p1, "x0"    # Lcom/google/protobuf/nano/CodedInputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 286
    invoke-virtual {p0, p1}, Lpaymentfraud/mobile/DeviceFingerprinting$Parsed$State;->mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lpaymentfraud/mobile/DeviceFingerprinting$Parsed$State;

    move-result-object v0

    return-object v0
.end method

.method public mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lpaymentfraud/mobile/DeviceFingerprinting$Parsed$State;
    .locals 8
    .param p1, "input"    # Lcom/google/protobuf/nano/CodedInputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v4, 0x0

    .line 773
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readTag()I

    move-result v3

    .line 774
    .local v3, "tag":I
    sparse-switch v3, :sswitch_data_0

    .line 778
    invoke-static {p1, v3}, Lcom/google/protobuf/nano/WireFormatNano;->parseUnknownField(Lcom/google/protobuf/nano/CodedInputByteBufferNano;I)Z

    move-result v5

    if-nez v5, :cond_0

    .line 779
    :sswitch_0
    return-object p0

    .line 784
    :sswitch_1
    const/16 v5, 0xa

    invoke-static {p1, v5}, Lcom/google/protobuf/nano/WireFormatNano;->getRepeatedFieldArrayLength(Lcom/google/protobuf/nano/CodedInputByteBufferNano;I)I

    move-result v0

    .line 786
    .local v0, "arrayLength":I
    iget-object v5, p0, Lpaymentfraud/mobile/DeviceFingerprinting$Parsed$State;->installedPackages:[Lpaymentfraud/mobile/DeviceFingerprinting$Parsed$State$PackageInfo;

    if-nez v5, :cond_2

    move v1, v4

    .line 787
    .local v1, "i":I
    :goto_1
    add-int v5, v1, v0

    new-array v2, v5, [Lpaymentfraud/mobile/DeviceFingerprinting$Parsed$State$PackageInfo;

    .line 789
    .local v2, "newArray":[Lpaymentfraud/mobile/DeviceFingerprinting$Parsed$State$PackageInfo;
    if-eqz v1, :cond_1

    .line 790
    iget-object v5, p0, Lpaymentfraud/mobile/DeviceFingerprinting$Parsed$State;->installedPackages:[Lpaymentfraud/mobile/DeviceFingerprinting$Parsed$State$PackageInfo;

    invoke-static {v5, v4, v2, v4, v1}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 792
    :cond_1
    :goto_2
    array-length v5, v2

    add-int/lit8 v5, v5, -0x1

    if-ge v1, v5, :cond_3

    .line 793
    new-instance v5, Lpaymentfraud/mobile/DeviceFingerprinting$Parsed$State$PackageInfo;

    invoke-direct {v5}, Lpaymentfraud/mobile/DeviceFingerprinting$Parsed$State$PackageInfo;-><init>()V

    aput-object v5, v2, v1

    .line 794
    aget-object v5, v2, v1

    invoke-virtual {p1, v5}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readMessage(Lcom/google/protobuf/nano/MessageNano;)V

    .line 795
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readTag()I

    .line 792
    add-int/lit8 v1, v1, 0x1

    goto :goto_2

    .line 786
    .end local v1    # "i":I
    .end local v2    # "newArray":[Lpaymentfraud/mobile/DeviceFingerprinting$Parsed$State$PackageInfo;
    :cond_2
    iget-object v5, p0, Lpaymentfraud/mobile/DeviceFingerprinting$Parsed$State;->installedPackages:[Lpaymentfraud/mobile/DeviceFingerprinting$Parsed$State$PackageInfo;

    array-length v1, v5

    goto :goto_1

    .line 798
    .restart local v1    # "i":I
    .restart local v2    # "newArray":[Lpaymentfraud/mobile/DeviceFingerprinting$Parsed$State$PackageInfo;
    :cond_3
    new-instance v5, Lpaymentfraud/mobile/DeviceFingerprinting$Parsed$State$PackageInfo;

    invoke-direct {v5}, Lpaymentfraud/mobile/DeviceFingerprinting$Parsed$State$PackageInfo;-><init>()V

    aput-object v5, v2, v1

    .line 799
    aget-object v5, v2, v1

    invoke-virtual {p1, v5}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readMessage(Lcom/google/protobuf/nano/MessageNano;)V

    .line 800
    iput-object v2, p0, Lpaymentfraud/mobile/DeviceFingerprinting$Parsed$State;->installedPackages:[Lpaymentfraud/mobile/DeviceFingerprinting$Parsed$State$PackageInfo;

    goto :goto_0

    .line 804
    .end local v0    # "arrayLength":I
    .end local v1    # "i":I
    .end local v2    # "newArray":[Lpaymentfraud/mobile/DeviceFingerprinting$Parsed$State$PackageInfo;
    :sswitch_2
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readInt32()I

    move-result v5

    iput v5, p0, Lpaymentfraud/mobile/DeviceFingerprinting$Parsed$State;->percentBattery:I

    goto :goto_0

    .line 808
    :sswitch_3
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readInt64()J

    move-result-wide v6

    iput-wide v6, p0, Lpaymentfraud/mobile/DeviceFingerprinting$Parsed$State;->gmtOffsetMillis:J

    goto :goto_0

    .line 812
    :sswitch_4
    iget-object v5, p0, Lpaymentfraud/mobile/DeviceFingerprinting$Parsed$State;->lastGpsLocation:Lpaymentfraud/mobile/DeviceFingerprinting$Parsed$State$Location;

    if-nez v5, :cond_4

    .line 813
    new-instance v5, Lpaymentfraud/mobile/DeviceFingerprinting$Parsed$State$Location;

    invoke-direct {v5}, Lpaymentfraud/mobile/DeviceFingerprinting$Parsed$State$Location;-><init>()V

    iput-object v5, p0, Lpaymentfraud/mobile/DeviceFingerprinting$Parsed$State;->lastGpsLocation:Lpaymentfraud/mobile/DeviceFingerprinting$Parsed$State$Location;

    .line 815
    :cond_4
    iget-object v5, p0, Lpaymentfraud/mobile/DeviceFingerprinting$Parsed$State;->lastGpsLocation:Lpaymentfraud/mobile/DeviceFingerprinting$Parsed$State$Location;

    invoke-virtual {p1, v5}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readMessage(Lcom/google/protobuf/nano/MessageNano;)V

    goto :goto_0

    .line 819
    :sswitch_5
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readBool()Z

    move-result v5

    iput-boolean v5, p0, Lpaymentfraud/mobile/DeviceFingerprinting$Parsed$State;->devModeOn:Z

    goto :goto_0

    .line 823
    :sswitch_6
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readBool()Z

    move-result v5

    iput-boolean v5, p0, Lpaymentfraud/mobile/DeviceFingerprinting$Parsed$State;->nonPlayInstallAllowed:Z

    goto :goto_0

    .line 827
    :sswitch_7
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readString()Ljava/lang/String;

    move-result-object v5

    iput-object v5, p0, Lpaymentfraud/mobile/DeviceFingerprinting$Parsed$State;->language:Ljava/lang/String;

    goto/16 :goto_0

    .line 831
    :sswitch_8
    const/16 v5, 0x52

    invoke-static {p1, v5}, Lcom/google/protobuf/nano/WireFormatNano;->getRepeatedFieldArrayLength(Lcom/google/protobuf/nano/CodedInputByteBufferNano;I)I

    move-result v0

    .line 833
    .restart local v0    # "arrayLength":I
    iget-object v5, p0, Lpaymentfraud/mobile/DeviceFingerprinting$Parsed$State;->ipAddr:[Ljava/lang/String;

    if-nez v5, :cond_6

    move v1, v4

    .line 834
    .restart local v1    # "i":I
    :goto_3
    add-int v5, v1, v0

    new-array v2, v5, [Ljava/lang/String;

    .line 835
    .local v2, "newArray":[Ljava/lang/String;
    if-eqz v1, :cond_5

    .line 836
    iget-object v5, p0, Lpaymentfraud/mobile/DeviceFingerprinting$Parsed$State;->ipAddr:[Ljava/lang/String;

    invoke-static {v5, v4, v2, v4, v1}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 838
    :cond_5
    :goto_4
    array-length v5, v2

    add-int/lit8 v5, v5, -0x1

    if-ge v1, v5, :cond_7

    .line 839
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readString()Ljava/lang/String;

    move-result-object v5

    aput-object v5, v2, v1

    .line 840
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readTag()I

    .line 838
    add-int/lit8 v1, v1, 0x1

    goto :goto_4

    .line 833
    .end local v1    # "i":I
    .end local v2    # "newArray":[Ljava/lang/String;
    :cond_6
    iget-object v5, p0, Lpaymentfraud/mobile/DeviceFingerprinting$Parsed$State;->ipAddr:[Ljava/lang/String;

    array-length v1, v5

    goto :goto_3

    .line 843
    .restart local v1    # "i":I
    .restart local v2    # "newArray":[Ljava/lang/String;
    :cond_7
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readString()Ljava/lang/String;

    move-result-object v5

    aput-object v5, v2, v1

    .line 844
    iput-object v2, p0, Lpaymentfraud/mobile/DeviceFingerprinting$Parsed$State;->ipAddr:[Ljava/lang/String;

    goto/16 :goto_0

    .line 848
    .end local v0    # "arrayLength":I
    .end local v1    # "i":I
    .end local v2    # "newArray":[Ljava/lang/String;
    :sswitch_9
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readString()Ljava/lang/String;

    move-result-object v5

    iput-object v5, p0, Lpaymentfraud/mobile/DeviceFingerprinting$Parsed$State;->locale:Ljava/lang/String;

    goto/16 :goto_0

    .line 852
    :sswitch_a
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readString()Ljava/lang/String;

    move-result-object v5

    iput-object v5, p0, Lpaymentfraud/mobile/DeviceFingerprinting$Parsed$State;->cellOperator:Ljava/lang/String;

    goto/16 :goto_0

    .line 856
    :sswitch_b
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readString()Ljava/lang/String;

    move-result-object v5

    iput-object v5, p0, Lpaymentfraud/mobile/DeviceFingerprinting$Parsed$State;->simOperator:Ljava/lang/String;

    goto/16 :goto_0

    .line 774
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x18 -> :sswitch_2
        0x20 -> :sswitch_3
        0x32 -> :sswitch_4
        0x38 -> :sswitch_5
        0x40 -> :sswitch_6
        0x4a -> :sswitch_7
        0x52 -> :sswitch_8
        0x5a -> :sswitch_9
        0x72 -> :sswitch_a
        0x7a -> :sswitch_b
    .end sparse-switch
.end method

.method public writeTo(Lcom/google/protobuf/nano/CodedOutputByteBufferNano;)V
    .locals 6
    .param p1, "output"    # Lcom/google/protobuf/nano/CodedOutputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 657
    iget-object v2, p0, Lpaymentfraud/mobile/DeviceFingerprinting$Parsed$State;->installedPackages:[Lpaymentfraud/mobile/DeviceFingerprinting$Parsed$State$PackageInfo;

    if-eqz v2, :cond_1

    iget-object v2, p0, Lpaymentfraud/mobile/DeviceFingerprinting$Parsed$State;->installedPackages:[Lpaymentfraud/mobile/DeviceFingerprinting$Parsed$State$PackageInfo;

    array-length v2, v2

    if-lez v2, :cond_1

    .line 658
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    iget-object v2, p0, Lpaymentfraud/mobile/DeviceFingerprinting$Parsed$State;->installedPackages:[Lpaymentfraud/mobile/DeviceFingerprinting$Parsed$State$PackageInfo;

    array-length v2, v2

    if-ge v1, v2, :cond_1

    .line 659
    iget-object v2, p0, Lpaymentfraud/mobile/DeviceFingerprinting$Parsed$State;->installedPackages:[Lpaymentfraud/mobile/DeviceFingerprinting$Parsed$State$PackageInfo;

    aget-object v0, v2, v1

    .line 660
    .local v0, "element":Lpaymentfraud/mobile/DeviceFingerprinting$Parsed$State$PackageInfo;
    if-eqz v0, :cond_0

    .line 661
    const/4 v2, 0x1

    invoke-virtual {p1, v2, v0}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeMessage(ILcom/google/protobuf/nano/MessageNano;)V

    .line 658
    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 665
    .end local v0    # "element":Lpaymentfraud/mobile/DeviceFingerprinting$Parsed$State$PackageInfo;
    .end local v1    # "i":I
    :cond_1
    iget v2, p0, Lpaymentfraud/mobile/DeviceFingerprinting$Parsed$State;->percentBattery:I

    const/4 v3, -0x1

    if-eq v2, v3, :cond_2

    .line 666
    const/4 v2, 0x3

    iget v3, p0, Lpaymentfraud/mobile/DeviceFingerprinting$Parsed$State;->percentBattery:I

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeInt32(II)V

    .line 668
    :cond_2
    iget-wide v2, p0, Lpaymentfraud/mobile/DeviceFingerprinting$Parsed$State;->gmtOffsetMillis:J

    const-wide/32 v4, -0x5265c00

    cmp-long v2, v2, v4

    if-eqz v2, :cond_3

    .line 669
    const/4 v2, 0x4

    iget-wide v4, p0, Lpaymentfraud/mobile/DeviceFingerprinting$Parsed$State;->gmtOffsetMillis:J

    invoke-virtual {p1, v2, v4, v5}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeInt64(IJ)V

    .line 671
    :cond_3
    iget-object v2, p0, Lpaymentfraud/mobile/DeviceFingerprinting$Parsed$State;->lastGpsLocation:Lpaymentfraud/mobile/DeviceFingerprinting$Parsed$State$Location;

    if-eqz v2, :cond_4

    .line 672
    const/4 v2, 0x6

    iget-object v3, p0, Lpaymentfraud/mobile/DeviceFingerprinting$Parsed$State;->lastGpsLocation:Lpaymentfraud/mobile/DeviceFingerprinting$Parsed$State$Location;

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeMessage(ILcom/google/protobuf/nano/MessageNano;)V

    .line 674
    :cond_4
    iget-boolean v2, p0, Lpaymentfraud/mobile/DeviceFingerprinting$Parsed$State;->devModeOn:Z

    if-eqz v2, :cond_5

    .line 675
    const/4 v2, 0x7

    iget-boolean v3, p0, Lpaymentfraud/mobile/DeviceFingerprinting$Parsed$State;->devModeOn:Z

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeBool(IZ)V

    .line 677
    :cond_5
    iget-boolean v2, p0, Lpaymentfraud/mobile/DeviceFingerprinting$Parsed$State;->nonPlayInstallAllowed:Z

    if-eqz v2, :cond_6

    .line 678
    const/16 v2, 0x8

    iget-boolean v3, p0, Lpaymentfraud/mobile/DeviceFingerprinting$Parsed$State;->nonPlayInstallAllowed:Z

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeBool(IZ)V

    .line 680
    :cond_6
    iget-object v2, p0, Lpaymentfraud/mobile/DeviceFingerprinting$Parsed$State;->language:Ljava/lang/String;

    const-string v3, ""

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_7

    .line 681
    const/16 v2, 0x9

    iget-object v3, p0, Lpaymentfraud/mobile/DeviceFingerprinting$Parsed$State;->language:Ljava/lang/String;

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeString(ILjava/lang/String;)V

    .line 683
    :cond_7
    iget-object v2, p0, Lpaymentfraud/mobile/DeviceFingerprinting$Parsed$State;->ipAddr:[Ljava/lang/String;

    if-eqz v2, :cond_9

    iget-object v2, p0, Lpaymentfraud/mobile/DeviceFingerprinting$Parsed$State;->ipAddr:[Ljava/lang/String;

    array-length v2, v2

    if-lez v2, :cond_9

    .line 684
    const/4 v1, 0x0

    .restart local v1    # "i":I
    :goto_1
    iget-object v2, p0, Lpaymentfraud/mobile/DeviceFingerprinting$Parsed$State;->ipAddr:[Ljava/lang/String;

    array-length v2, v2

    if-ge v1, v2, :cond_9

    .line 685
    iget-object v2, p0, Lpaymentfraud/mobile/DeviceFingerprinting$Parsed$State;->ipAddr:[Ljava/lang/String;

    aget-object v0, v2, v1

    .line 686
    .local v0, "element":Ljava/lang/String;
    if-eqz v0, :cond_8

    .line 687
    const/16 v2, 0xa

    invoke-virtual {p1, v2, v0}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeString(ILjava/lang/String;)V

    .line 684
    :cond_8
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 691
    .end local v0    # "element":Ljava/lang/String;
    .end local v1    # "i":I
    :cond_9
    iget-object v2, p0, Lpaymentfraud/mobile/DeviceFingerprinting$Parsed$State;->locale:Ljava/lang/String;

    const-string v3, ""

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_a

    .line 692
    const/16 v2, 0xb

    iget-object v3, p0, Lpaymentfraud/mobile/DeviceFingerprinting$Parsed$State;->locale:Ljava/lang/String;

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeString(ILjava/lang/String;)V

    .line 694
    :cond_a
    iget-object v2, p0, Lpaymentfraud/mobile/DeviceFingerprinting$Parsed$State;->cellOperator:Ljava/lang/String;

    const-string v3, ""

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_b

    .line 695
    const/16 v2, 0xe

    iget-object v3, p0, Lpaymentfraud/mobile/DeviceFingerprinting$Parsed$State;->cellOperator:Ljava/lang/String;

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeString(ILjava/lang/String;)V

    .line 697
    :cond_b
    iget-object v2, p0, Lpaymentfraud/mobile/DeviceFingerprinting$Parsed$State;->simOperator:Ljava/lang/String;

    const-string v3, ""

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_c

    .line 698
    const/16 v2, 0xf

    iget-object v3, p0, Lpaymentfraud/mobile/DeviceFingerprinting$Parsed$State;->simOperator:Ljava/lang/String;

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeString(ILjava/lang/String;)V

    .line 700
    :cond_c
    invoke-super {p0, p1}, Lcom/google/protobuf/nano/MessageNano;->writeTo(Lcom/google/protobuf/nano/CodedOutputByteBufferNano;)V

    .line 701
    return-void
.end method
