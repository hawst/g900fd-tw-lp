.class public final Lpaymentfraud/mobile/DeviceFingerprinting;
.super Lcom/google/protobuf/nano/MessageNano;
.source "DeviceFingerprinting.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lpaymentfraud/mobile/DeviceFingerprinting$Parsed;
    }
.end annotation


# instance fields
.field public parsed:Lpaymentfraud/mobile/DeviceFingerprinting$Parsed;


# virtual methods
.method protected computeSerializedSize()I
    .locals 3

    .prologue
    .line 1015
    invoke-super {p0}, Lcom/google/protobuf/nano/MessageNano;->computeSerializedSize()I

    move-result v0

    .line 1016
    .local v0, "size":I
    iget-object v1, p0, Lpaymentfraud/mobile/DeviceFingerprinting;->parsed:Lpaymentfraud/mobile/DeviceFingerprinting$Parsed;

    if-eqz v1, :cond_0

    .line 1017
    const/4 v1, 0x2

    iget-object v2, p0, Lpaymentfraud/mobile/DeviceFingerprinting;->parsed:Lpaymentfraud/mobile/DeviceFingerprinting$Parsed;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeMessageSize(ILcom/google/protobuf/nano/MessageNano;)I

    move-result v1

    add-int/2addr v0, v1

    .line 1020
    :cond_0
    return v0
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/protobuf/nano/MessageNano;
    .locals 1
    .param p1, "x0"    # Lcom/google/protobuf/nano/CodedInputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 5
    invoke-virtual {p0, p1}, Lpaymentfraud/mobile/DeviceFingerprinting;->mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lpaymentfraud/mobile/DeviceFingerprinting;

    move-result-object v0

    return-object v0
.end method

.method public mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lpaymentfraud/mobile/DeviceFingerprinting;
    .locals 2
    .param p1, "input"    # Lcom/google/protobuf/nano/CodedInputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 1028
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readTag()I

    move-result v0

    .line 1029
    .local v0, "tag":I
    sparse-switch v0, :sswitch_data_0

    .line 1033
    invoke-static {p1, v0}, Lcom/google/protobuf/nano/WireFormatNano;->parseUnknownField(Lcom/google/protobuf/nano/CodedInputByteBufferNano;I)Z

    move-result v1

    if-nez v1, :cond_0

    .line 1034
    :sswitch_0
    return-object p0

    .line 1039
    :sswitch_1
    iget-object v1, p0, Lpaymentfraud/mobile/DeviceFingerprinting;->parsed:Lpaymentfraud/mobile/DeviceFingerprinting$Parsed;

    if-nez v1, :cond_1

    .line 1040
    new-instance v1, Lpaymentfraud/mobile/DeviceFingerprinting$Parsed;

    invoke-direct {v1}, Lpaymentfraud/mobile/DeviceFingerprinting$Parsed;-><init>()V

    iput-object v1, p0, Lpaymentfraud/mobile/DeviceFingerprinting;->parsed:Lpaymentfraud/mobile/DeviceFingerprinting$Parsed;

    .line 1042
    :cond_1
    iget-object v1, p0, Lpaymentfraud/mobile/DeviceFingerprinting;->parsed:Lpaymentfraud/mobile/DeviceFingerprinting$Parsed;

    invoke-virtual {p1, v1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readMessage(Lcom/google/protobuf/nano/MessageNano;)V

    goto :goto_0

    .line 1029
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x12 -> :sswitch_1
    .end sparse-switch
.end method

.method public writeTo(Lcom/google/protobuf/nano/CodedOutputByteBufferNano;)V
    .locals 2
    .param p1, "output"    # Lcom/google/protobuf/nano/CodedOutputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 1007
    iget-object v0, p0, Lpaymentfraud/mobile/DeviceFingerprinting;->parsed:Lpaymentfraud/mobile/DeviceFingerprinting$Parsed;

    if-eqz v0, :cond_0

    .line 1008
    const/4 v0, 0x2

    iget-object v1, p0, Lpaymentfraud/mobile/DeviceFingerprinting;->parsed:Lpaymentfraud/mobile/DeviceFingerprinting$Parsed;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeMessage(ILcom/google/protobuf/nano/MessageNano;)V

    .line 1010
    :cond_0
    invoke-super {p0, p1}, Lcom/google/protobuf/nano/MessageNano;->writeTo(Lcom/google/protobuf/nano/CodedOutputByteBufferNano;)V

    .line 1011
    return-void
.end method
