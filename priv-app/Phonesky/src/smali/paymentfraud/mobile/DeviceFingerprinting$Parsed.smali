.class public final Lpaymentfraud/mobile/DeviceFingerprinting$Parsed;
.super Lcom/google/protobuf/nano/MessageNano;
.source "DeviceFingerprinting.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lpaymentfraud/mobile/DeviceFingerprinting;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Parsed"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lpaymentfraud/mobile/DeviceFingerprinting$Parsed$State;,
        Lpaymentfraud/mobile/DeviceFingerprinting$Parsed$Properties;
    }
.end annotation


# instance fields
.field public properties:Lpaymentfraud/mobile/DeviceFingerprinting$Parsed$Properties;

.field public state:Lpaymentfraud/mobile/DeviceFingerprinting$Parsed$State;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 895
    invoke-direct {p0}, Lcom/google/protobuf/nano/MessageNano;-><init>()V

    .line 896
    invoke-virtual {p0}, Lpaymentfraud/mobile/DeviceFingerprinting$Parsed;->clear()Lpaymentfraud/mobile/DeviceFingerprinting$Parsed;

    .line 897
    return-void
.end method


# virtual methods
.method public clear()Lpaymentfraud/mobile/DeviceFingerprinting$Parsed;
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 900
    iput-object v0, p0, Lpaymentfraud/mobile/DeviceFingerprinting$Parsed;->properties:Lpaymentfraud/mobile/DeviceFingerprinting$Parsed$Properties;

    .line 901
    iput-object v0, p0, Lpaymentfraud/mobile/DeviceFingerprinting$Parsed;->state:Lpaymentfraud/mobile/DeviceFingerprinting$Parsed$State;

    .line 902
    const/4 v0, -0x1

    iput v0, p0, Lpaymentfraud/mobile/DeviceFingerprinting$Parsed;->cachedSize:I

    .line 903
    return-object p0
.end method

.method protected computeSerializedSize()I
    .locals 3

    .prologue
    .line 920
    invoke-super {p0}, Lcom/google/protobuf/nano/MessageNano;->computeSerializedSize()I

    move-result v0

    .line 921
    .local v0, "size":I
    iget-object v1, p0, Lpaymentfraud/mobile/DeviceFingerprinting$Parsed;->properties:Lpaymentfraud/mobile/DeviceFingerprinting$Parsed$Properties;

    if-eqz v1, :cond_0

    .line 922
    const/4 v1, 0x1

    iget-object v2, p0, Lpaymentfraud/mobile/DeviceFingerprinting$Parsed;->properties:Lpaymentfraud/mobile/DeviceFingerprinting$Parsed$Properties;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeMessageSize(ILcom/google/protobuf/nano/MessageNano;)I

    move-result v1

    add-int/2addr v0, v1

    .line 925
    :cond_0
    iget-object v1, p0, Lpaymentfraud/mobile/DeviceFingerprinting$Parsed;->state:Lpaymentfraud/mobile/DeviceFingerprinting$Parsed$State;

    if-eqz v1, :cond_1

    .line 926
    const/4 v1, 0x2

    iget-object v2, p0, Lpaymentfraud/mobile/DeviceFingerprinting$Parsed;->state:Lpaymentfraud/mobile/DeviceFingerprinting$Parsed$State;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeMessageSize(ILcom/google/protobuf/nano/MessageNano;)I

    move-result v1

    add-int/2addr v0, v1

    .line 929
    :cond_1
    return v0
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/protobuf/nano/MessageNano;
    .locals 1
    .param p1, "x0"    # Lcom/google/protobuf/nano/CodedInputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 9
    invoke-virtual {p0, p1}, Lpaymentfraud/mobile/DeviceFingerprinting$Parsed;->mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lpaymentfraud/mobile/DeviceFingerprinting$Parsed;

    move-result-object v0

    return-object v0
.end method

.method public mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lpaymentfraud/mobile/DeviceFingerprinting$Parsed;
    .locals 2
    .param p1, "input"    # Lcom/google/protobuf/nano/CodedInputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 937
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readTag()I

    move-result v0

    .line 938
    .local v0, "tag":I
    sparse-switch v0, :sswitch_data_0

    .line 942
    invoke-static {p1, v0}, Lcom/google/protobuf/nano/WireFormatNano;->parseUnknownField(Lcom/google/protobuf/nano/CodedInputByteBufferNano;I)Z

    move-result v1

    if-nez v1, :cond_0

    .line 943
    :sswitch_0
    return-object p0

    .line 948
    :sswitch_1
    iget-object v1, p0, Lpaymentfraud/mobile/DeviceFingerprinting$Parsed;->properties:Lpaymentfraud/mobile/DeviceFingerprinting$Parsed$Properties;

    if-nez v1, :cond_1

    .line 949
    new-instance v1, Lpaymentfraud/mobile/DeviceFingerprinting$Parsed$Properties;

    invoke-direct {v1}, Lpaymentfraud/mobile/DeviceFingerprinting$Parsed$Properties;-><init>()V

    iput-object v1, p0, Lpaymentfraud/mobile/DeviceFingerprinting$Parsed;->properties:Lpaymentfraud/mobile/DeviceFingerprinting$Parsed$Properties;

    .line 951
    :cond_1
    iget-object v1, p0, Lpaymentfraud/mobile/DeviceFingerprinting$Parsed;->properties:Lpaymentfraud/mobile/DeviceFingerprinting$Parsed$Properties;

    invoke-virtual {p1, v1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readMessage(Lcom/google/protobuf/nano/MessageNano;)V

    goto :goto_0

    .line 955
    :sswitch_2
    iget-object v1, p0, Lpaymentfraud/mobile/DeviceFingerprinting$Parsed;->state:Lpaymentfraud/mobile/DeviceFingerprinting$Parsed$State;

    if-nez v1, :cond_2

    .line 956
    new-instance v1, Lpaymentfraud/mobile/DeviceFingerprinting$Parsed$State;

    invoke-direct {v1}, Lpaymentfraud/mobile/DeviceFingerprinting$Parsed$State;-><init>()V

    iput-object v1, p0, Lpaymentfraud/mobile/DeviceFingerprinting$Parsed;->state:Lpaymentfraud/mobile/DeviceFingerprinting$Parsed$State;

    .line 958
    :cond_2
    iget-object v1, p0, Lpaymentfraud/mobile/DeviceFingerprinting$Parsed;->state:Lpaymentfraud/mobile/DeviceFingerprinting$Parsed$State;

    invoke-virtual {p1, v1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readMessage(Lcom/google/protobuf/nano/MessageNano;)V

    goto :goto_0

    .line 938
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
    .end sparse-switch
.end method

.method public writeTo(Lcom/google/protobuf/nano/CodedOutputByteBufferNano;)V
    .locals 2
    .param p1, "output"    # Lcom/google/protobuf/nano/CodedOutputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 909
    iget-object v0, p0, Lpaymentfraud/mobile/DeviceFingerprinting$Parsed;->properties:Lpaymentfraud/mobile/DeviceFingerprinting$Parsed$Properties;

    if-eqz v0, :cond_0

    .line 910
    const/4 v0, 0x1

    iget-object v1, p0, Lpaymentfraud/mobile/DeviceFingerprinting$Parsed;->properties:Lpaymentfraud/mobile/DeviceFingerprinting$Parsed$Properties;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeMessage(ILcom/google/protobuf/nano/MessageNano;)V

    .line 912
    :cond_0
    iget-object v0, p0, Lpaymentfraud/mobile/DeviceFingerprinting$Parsed;->state:Lpaymentfraud/mobile/DeviceFingerprinting$Parsed$State;

    if-eqz v0, :cond_1

    .line 913
    const/4 v0, 0x2

    iget-object v1, p0, Lpaymentfraud/mobile/DeviceFingerprinting$Parsed;->state:Lpaymentfraud/mobile/DeviceFingerprinting$Parsed$State;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeMessage(ILcom/google/protobuf/nano/MessageNano;)V

    .line 915
    :cond_1
    invoke-super {p0, p1}, Lcom/google/protobuf/nano/MessageNano;->writeTo(Lcom/google/protobuf/nano/CodedOutputByteBufferNano;)V

    .line 916
    return-void
.end method
