.class public Lorg/keyczar/util/Util;
.super Ljava/lang/Object;
.source "Util.java"


# static fields
.field private static final DIGEST_QUEUE:Ljava/util/concurrent/ConcurrentLinkedQueue;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/concurrent/ConcurrentLinkedQueue",
            "<",
            "Ljava/security/MessageDigest;",
            ">;"
        }
    .end annotation
.end field

.field private static final GSON_THREAD_LOCAL:Ljava/lang/ThreadLocal;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/ThreadLocal",
            "<",
            "Lcom/google/gson/Gson;",
            ">;"
        }
    .end annotation
.end field

.field private static final RAND_QUEUE:Ljava/util/concurrent/ConcurrentLinkedQueue;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/concurrent/ConcurrentLinkedQueue",
            "<",
            "Ljava/security/SecureRandom;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 49
    new-instance v0, Ljava/util/concurrent/ConcurrentLinkedQueue;

    invoke-direct {v0}, Ljava/util/concurrent/ConcurrentLinkedQueue;-><init>()V

    sput-object v0, Lorg/keyczar/util/Util;->DIGEST_QUEUE:Ljava/util/concurrent/ConcurrentLinkedQueue;

    .line 51
    new-instance v0, Ljava/util/concurrent/ConcurrentLinkedQueue;

    invoke-direct {v0}, Ljava/util/concurrent/ConcurrentLinkedQueue;-><init>()V

    sput-object v0, Lorg/keyczar/util/Util;->RAND_QUEUE:Ljava/util/concurrent/ConcurrentLinkedQueue;

    .line 59
    new-instance v0, Lorg/keyczar/util/Util$1;

    invoke-direct {v0}, Lorg/keyczar/util/Util$1;-><init>()V

    sput-object v0, Lorg/keyczar/util/Util;->GSON_THREAD_LOCAL:Ljava/lang/ThreadLocal;

    return-void
.end method

.method public static varargs cat([[B)[B
    .locals 9
    .param p0, "arrays"    # [[B

    .prologue
    .line 397
    const/4 v4, 0x0

    .line 398
    .local v4, "length":I
    move-object v0, p0

    .local v0, "arr$":[[B
    array-length v3, v0

    .local v3, "len$":I
    const/4 v2, 0x0

    .local v2, "i$":I
    :goto_0
    if-ge v2, v3, :cond_0

    aget-object v1, v0, v2

    .line 399
    .local v1, "array":[B
    array-length v7, v1

    add-int/2addr v4, v7

    .line 398
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 401
    .end local v1    # "array":[B
    :cond_0
    new-array v6, v4, [B

    .line 402
    .local v6, "result":[B
    const/4 v5, 0x0

    .line 403
    .local v5, "pos":I
    move-object v0, p0

    array-length v3, v0

    const/4 v2, 0x0

    :goto_1
    if-ge v2, v3, :cond_1

    aget-object v1, v0, v2

    .line 404
    .restart local v1    # "array":[B
    const/4 v7, 0x0

    array-length v8, v1

    invoke-static {v1, v7, v6, v5, v8}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 405
    array-length v7, v1

    add-int/2addr v5, v7

    .line 403
    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    .line 407
    .end local v1    # "array":[B
    :cond_1
    return-object v6
.end method

.method public static decodeBigInteger(Ljava/lang/String;)Ljava/math/BigInteger;
    .locals 2
    .param p0, "value"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/keyczar/exceptions/Base64DecodingException;
        }
    .end annotation

    .prologue
    .line 446
    new-instance v0, Ljava/math/BigInteger;

    invoke-static {p0}, Lorg/keyczar/util/Base64Coder;->decodeWebSafe(Ljava/lang/String;)[B

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/math/BigInteger;-><init>([B)V

    return-object v0
.end method

.method public static fromInt(I)[B
    .locals 2
    .param p0, "input"    # I

    .prologue
    .line 99
    const/4 v1, 0x4

    new-array v0, v1, [B

    .line 100
    .local v0, "output":[B
    const/4 v1, 0x0

    invoke-static {p0, v0, v1}, Lorg/keyczar/util/Util;->writeInt(I[BI)V

    .line 101
    return-object v0
.end method

.method public static gson()Lcom/google/gson/Gson;
    .locals 1

    .prologue
    .line 71
    sget-object v0, Lorg/keyczar/util/Util;->GSON_THREAD_LOCAL:Ljava/lang/ThreadLocal;

    invoke-virtual {v0}, Ljava/lang/ThreadLocal;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/gson/Gson;

    return-object v0
.end method

.method public static varargs hash([[B)[B
    .locals 8
    .param p0, "inputs"    # [[B
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/keyczar/exceptions/KeyczarException;
        }
    .end annotation

    .prologue
    .line 219
    sget-object v7, Lorg/keyczar/util/Util;->DIGEST_QUEUE:Ljava/util/concurrent/ConcurrentLinkedQueue;

    invoke-virtual {v7}, Ljava/util/concurrent/ConcurrentLinkedQueue;->poll()Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Ljava/security/MessageDigest;

    .line 220
    .local v6, "md":Ljava/security/MessageDigest;
    if-nez v6, :cond_0

    .line 222
    :try_start_0
    const-string v7, "SHA-1"

    invoke-static {v7}, Ljava/security/MessageDigest;->getInstance(Ljava/lang/String;)Ljava/security/MessageDigest;
    :try_end_0
    .catch Ljava/security/NoSuchAlgorithmException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v6

    .line 227
    :cond_0
    move-object v0, p0

    .local v0, "arr$":[[B
    array-length v5, v0

    .local v5, "len$":I
    const/4 v4, 0x0

    .local v4, "i$":I
    :goto_0
    if-ge v4, v5, :cond_1

    aget-object v1, v0, v4

    .line 228
    .local v1, "array":[B
    invoke-virtual {v6, v1}, Ljava/security/MessageDigest;->update([B)V

    .line 227
    add-int/lit8 v4, v4, 0x1

    goto :goto_0

    .line 223
    .end local v0    # "arr$":[[B
    .end local v1    # "array":[B
    .end local v4    # "i$":I
    .end local v5    # "len$":I
    :catch_0
    move-exception v3

    .line 224
    .local v3, "e":Ljava/security/NoSuchAlgorithmException;
    new-instance v7, Lorg/keyczar/exceptions/KeyczarException;

    invoke-direct {v7, v3}, Lorg/keyczar/exceptions/KeyczarException;-><init>(Ljava/lang/Throwable;)V

    throw v7

    .line 230
    .end local v3    # "e":Ljava/security/NoSuchAlgorithmException;
    .restart local v0    # "arr$":[[B
    .restart local v4    # "i$":I
    .restart local v5    # "len$":I
    :cond_1
    invoke-virtual {v6}, Ljava/security/MessageDigest;->digest()[B

    move-result-object v2

    .line 231
    .local v2, "digest":[B
    sget-object v7, Lorg/keyczar/util/Util;->DIGEST_QUEUE:Ljava/util/concurrent/ConcurrentLinkedQueue;

    invoke-virtual {v7, v6}, Ljava/util/concurrent/ConcurrentLinkedQueue;->add(Ljava/lang/Object;)Z

    .line 232
    return-object v2
.end method

.method public static lenPrefix([B)[B
    .locals 2
    .param p0, "data"    # [B

    .prologue
    .line 154
    if-eqz p0, :cond_0

    array-length v0, p0

    if-nez v0, :cond_1

    .line 155
    :cond_0
    const/4 v0, 0x0

    invoke-static {v0}, Lorg/keyczar/util/Util;->fromInt(I)[B

    move-result-object v0

    .line 157
    :goto_0
    return-object v0

    :cond_1
    array-length v0, p0

    add-int/lit8 v0, v0, 0x4

    invoke-static {v0}, Ljava/nio/ByteBuffer;->allocate(I)Ljava/nio/ByteBuffer;

    move-result-object v0

    array-length v1, p0

    invoke-virtual {v0, v1}, Ljava/nio/ByteBuffer;->putInt(I)Ljava/nio/ByteBuffer;

    move-result-object v0

    invoke-virtual {v0, p0}, Ljava/nio/ByteBuffer;->put([B)Ljava/nio/ByteBuffer;

    move-result-object v0

    invoke-virtual {v0}, Ljava/nio/ByteBuffer;->array()[B

    move-result-object v0

    goto :goto_0
.end method

.method public static varargs prefixHash([[B)[B
    .locals 8
    .param p0, "inputs"    # [[B
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/keyczar/exceptions/KeyczarException;
        }
    .end annotation

    .prologue
    .line 128
    sget-object v7, Lorg/keyczar/util/Util;->DIGEST_QUEUE:Ljava/util/concurrent/ConcurrentLinkedQueue;

    invoke-virtual {v7}, Ljava/util/concurrent/ConcurrentLinkedQueue;->poll()Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Ljava/security/MessageDigest;

    .line 129
    .local v6, "md":Ljava/security/MessageDigest;
    if-nez v6, :cond_0

    .line 131
    :try_start_0
    const-string v7, "SHA-1"

    invoke-static {v7}, Ljava/security/MessageDigest;->getInstance(Ljava/lang/String;)Ljava/security/MessageDigest;
    :try_end_0
    .catch Ljava/security/NoSuchAlgorithmException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v6

    .line 136
    :cond_0
    move-object v0, p0

    .local v0, "arr$":[[B
    array-length v5, v0

    .local v5, "len$":I
    const/4 v4, 0x0

    .local v4, "i$":I
    :goto_0
    if-ge v4, v5, :cond_1

    aget-object v1, v0, v4

    .line 137
    .local v1, "array":[B
    array-length v7, v1

    invoke-static {v7}, Lorg/keyczar/util/Util;->fromInt(I)[B

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/security/MessageDigest;->update([B)V

    .line 138
    invoke-virtual {v6, v1}, Ljava/security/MessageDigest;->update([B)V

    .line 136
    add-int/lit8 v4, v4, 0x1

    goto :goto_0

    .line 132
    .end local v0    # "arr$":[[B
    .end local v1    # "array":[B
    .end local v4    # "i$":I
    .end local v5    # "len$":I
    :catch_0
    move-exception v3

    .line 133
    .local v3, "e":Ljava/security/NoSuchAlgorithmException;
    new-instance v7, Lorg/keyczar/exceptions/KeyczarException;

    invoke-direct {v7, v3}, Lorg/keyczar/exceptions/KeyczarException;-><init>(Ljava/lang/Throwable;)V

    throw v7

    .line 140
    .end local v3    # "e":Ljava/security/NoSuchAlgorithmException;
    .restart local v0    # "arr$":[[B
    .restart local v4    # "i$":I
    .restart local v5    # "len$":I
    :cond_1
    invoke-virtual {v6}, Ljava/security/MessageDigest;->digest()[B

    move-result-object v2

    .line 141
    .local v2, "digest":[B
    sget-object v7, Lorg/keyczar/util/Util;->DIGEST_QUEUE:Ljava/util/concurrent/ConcurrentLinkedQueue;

    invoke-virtual {v7, v6}, Ljava/util/concurrent/ConcurrentLinkedQueue;->add(Ljava/lang/Object;)Z

    .line 142
    return-object v2
.end method

.method public static rand([B)V
    .locals 2
    .param p0, "dest"    # [B

    .prologue
    .line 242
    sget-object v1, Lorg/keyczar/util/Util;->RAND_QUEUE:Ljava/util/concurrent/ConcurrentLinkedQueue;

    invoke-virtual {v1}, Ljava/util/concurrent/ConcurrentLinkedQueue;->poll()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/security/SecureRandom;

    .line 243
    .local v0, "random":Ljava/security/SecureRandom;
    if-nez v0, :cond_0

    .line 244
    new-instance v0, Ljava/security/SecureRandom;

    .end local v0    # "random":Ljava/security/SecureRandom;
    invoke-direct {v0}, Ljava/security/SecureRandom;-><init>()V

    .line 246
    .restart local v0    # "random":Ljava/security/SecureRandom;
    :cond_0
    invoke-virtual {v0, p0}, Ljava/security/SecureRandom;->nextBytes([B)V

    .line 247
    sget-object v1, Lorg/keyczar/util/Util;->RAND_QUEUE:Ljava/util/concurrent/ConcurrentLinkedQueue;

    invoke-virtual {v1, v0}, Ljava/util/concurrent/ConcurrentLinkedQueue;->add(Ljava/lang/Object;)Z

    .line 248
    return-void
.end method

.method public static rand(I)[B
    .locals 1
    .param p0, "len"    # I

    .prologue
    .line 256
    new-array v0, p0, [B

    .line 257
    .local v0, "output":[B
    invoke-static {v0}, Lorg/keyczar/util/Util;->rand([B)V

    .line 258
    return-object v0
.end method

.method static readInt([BI)I
    .locals 3
    .param p0, "src"    # [B
    .param p1, "offset"    # I

    .prologue
    .line 272
    const/4 v1, 0x0

    .line 273
    .local v1, "output":I
    add-int/lit8 v0, p1, 0x1

    .end local p1    # "offset":I
    .local v0, "offset":I
    aget-byte v2, p0, p1

    and-int/lit16 v2, v2, 0xff

    shl-int/lit8 v2, v2, 0x18

    or-int/2addr v1, v2

    .line 274
    add-int/lit8 p1, v0, 0x1

    .end local v0    # "offset":I
    .restart local p1    # "offset":I
    aget-byte v2, p0, v0

    and-int/lit16 v2, v2, 0xff

    shl-int/lit8 v2, v2, 0x10

    or-int/2addr v1, v2

    .line 275
    add-int/lit8 v0, p1, 0x1

    .end local p1    # "offset":I
    .restart local v0    # "offset":I
    aget-byte v2, p0, p1

    and-int/lit16 v2, v2, 0xff

    shl-int/lit8 v2, v2, 0x8

    or-int/2addr v1, v2

    .line 276
    add-int/lit8 p1, v0, 0x1

    .end local v0    # "offset":I
    .restart local p1    # "offset":I
    aget-byte v2, p0, v0

    and-int/lit16 v2, v2, 0xff

    or-int/2addr v1, v2

    .line 277
    return v1
.end method

.method public static safeArrayEquals([B[B)Z
    .locals 6
    .param p0, "a1"    # [B
    .param p1, "a2"    # [B

    .prologue
    const/4 v2, 0x1

    const/4 v3, 0x0

    .line 377
    if-eqz p0, :cond_0

    if-nez p1, :cond_3

    .line 378
    :cond_0
    if-ne p0, p1, :cond_2

    .line 387
    :cond_1
    :goto_0
    return v2

    :cond_2
    move v2, v3

    .line 378
    goto :goto_0

    .line 380
    :cond_3
    array-length v4, p0

    array-length v5, p1

    if-eq v4, v5, :cond_4

    move v2, v3

    .line 381
    goto :goto_0

    .line 383
    :cond_4
    const/4 v1, 0x0

    .line 384
    .local v1, "result":B
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_1
    array-length v4, p0

    if-ge v0, v4, :cond_5

    .line 385
    aget-byte v4, p0, v0

    aget-byte v5, p1, v0

    xor-int/2addr v4, v5

    or-int/2addr v4, v1

    int-to-byte v1, v4

    .line 384
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 387
    :cond_5
    if-eqz v1, :cond_1

    move v2, v3

    goto :goto_0
.end method

.method public static stripLeadingZeros([B)[B
    .locals 4
    .param p0, "input"    # [B

    .prologue
    .line 75
    const/4 v1, 0x0

    .line 78
    .local v1, "zeros":I
    :goto_0
    array-length v2, p0

    if-ge v1, v2, :cond_0

    aget-byte v2, p0, v1

    if-nez v2, :cond_0

    .line 79
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 82
    :cond_0
    if-nez v1, :cond_1

    .line 87
    .end local p0    # "input":[B
    :goto_1
    return-object p0

    .line 85
    .restart local p0    # "input":[B
    :cond_1
    array-length v2, p0

    sub-int/2addr v2, v1

    new-array v0, v2, [B

    .line 86
    .local v0, "output":[B
    const/4 v2, 0x0

    array-length v3, v0

    invoke-static {p0, v1, v0, v2, v3}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    move-object p0, v0

    .line 87
    goto :goto_1
.end method

.method public static toInt([B)I
    .locals 1
    .param p0, "src"    # [B

    .prologue
    .line 313
    const/4 v0, 0x0

    invoke-static {p0, v0}, Lorg/keyczar/util/Util;->readInt([BI)I

    move-result v0

    return v0
.end method

.method static writeInt(I[BI)V
    .locals 2
    .param p0, "input"    # I
    .param p1, "dest"    # [B
    .param p2, "offset"    # I

    .prologue
    .line 339
    add-int/lit8 v0, p2, 0x1

    .end local p2    # "offset":I
    .local v0, "offset":I
    shr-int/lit8 v1, p0, 0x18

    int-to-byte v1, v1

    aput-byte v1, p1, p2

    .line 340
    add-int/lit8 p2, v0, 0x1

    .end local v0    # "offset":I
    .restart local p2    # "offset":I
    shr-int/lit8 v1, p0, 0x10

    int-to-byte v1, v1

    aput-byte v1, p1, v0

    .line 341
    add-int/lit8 v0, p2, 0x1

    .end local p2    # "offset":I
    .restart local v0    # "offset":I
    shr-int/lit8 v1, p0, 0x8

    int-to-byte v1, v1

    aput-byte v1, p1, p2

    .line 342
    add-int/lit8 p2, v0, 0x1

    .end local v0    # "offset":I
    .restart local p2    # "offset":I
    int-to-byte v1, p0

    aput-byte v1, p1, v0

    .line 343
    return-void
.end method
