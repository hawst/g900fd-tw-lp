.class public final Landroid/support/v7/appcompat/R$attr;
.super Ljava/lang/Object;
.source "R.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Landroid/support/v7/appcompat/R;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "attr"
.end annotation


# static fields
.field public static final actionBarPopupTheme:I = 0x7f010093

.field public static final actionBarSize:I = 0x7f010098

.field public static final actionBarStyle:I = 0x7f010094

.field public static final actionBarTabStyle:I = 0x7f01008e

.field public static final actionBarTabTextStyle:I = 0x7f010090

.field public static final actionBarTheme:I = 0x7f010096

.field public static final actionBarWidgetTheme:I = 0x7f010097

.field public static final actionDropDownStyle:I = 0x7f0100ac

.field public static final actionModePopupWindowStyle:I = 0x7f0100a9

.field public static final actionModeStyle:I = 0x7f01009d

.field public static final actionOverflowButtonStyle:I = 0x7f010091

.field public static final actionOverflowMenuStyle:I = 0x7f010092

.field public static final colorButtonNormal:I = 0x7f0100d7

.field public static final colorControlActivated:I = 0x7f0100d5

.field public static final colorControlHighlight:I = 0x7f0100d6

.field public static final colorControlNormal:I = 0x7f0100d4

.field public static final colorSwitchThumbNormal:I = 0x7f0100d8

.field public static final drawerArrowStyle:I = 0x7f010125

.field public static final dropDownListViewStyle:I = 0x7f0100c9

.field public static final homeAsUpIndicator:I = 0x7f0100b0

.field public static final listPopupWindowStyle:I = 0x7f0100ca

.field public static final panelMenuListTheme:I = 0x7f0100cf

.field public static final popupMenuStyle:I = 0x7f0100bb

.field public static final searchViewStyle:I = 0x7f0100c3

.field public static final textColorSearchUrl:I = 0x7f0100c2

.field public static final toolbarNavigationButtonStyle:I = 0x7f0100ba

.field public static final toolbarStyle:I = 0x7f0100b9
