.class public Lcom/google/android/setupwizard/carrier/ActivationActivity;
.super Lcom/google/android/setupwizard/BaseActivity;
.source "ActivationActivity.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field private final GENERIC_TEST_SUCCESS_DELAY:I

.field private final STATE_ACTIVATING:I

.field private final STATE_CANCEL_COMPLETED:I

.field private final STATE_CANCEL_REQUESTED:I

.field private final STATE_FAILED:I

.field private final STATE_NOT_STARTED:I

.field private final STATE_SHUTTING_DOWN:I

.field private final STATE_SUCCESS:I

.field private final STATE_WAITING_FOR_ACTIVATION_STATE:I

.field private mActivationText:Landroid/widget/TextView;

.field private mActivationTitle:Landroid/widget/TextView;

.field private mAllowDontShowMode:Z

.field private mCancelButton:Landroid/widget/Button;

.field private mCancelEnabled:Z

.field private mCancelEndtime:J

.field private mCancelLabel:I

.field private mCountdownText:Landroid/widget/TextView;

.field private mDoExitToHome:Z

.field private mFilter:Landroid/content/IntentFilter;

.field private mGenericEndtime:J

.field private mGenericMode:Z

.field private mGenericOtaspDefinitelyNeeded:Z

.field private final mGenericTestSuccess:Ljava/lang/Runnable;

.field private final mHandler:Landroid/os/Handler;

.field private mNetworkBars:Landroid/widget/ImageView;

.field private mNetworkName:Ljava/lang/CharSequence;

.field private mNetworkNotifier:Landroid/view/View;

.field private mNextButton:Landroid/widget/Button;

.field private mNextEnabled:Z

.field private mNextLabel:I

.field private final mPhoneStateListener:Landroid/telephony/PhoneStateListener;

.field private mProgressBar:Landroid/widget/ProgressBar;

.field private mReceiver:Landroid/content/BroadcastReceiver;

.field private mServiceState:Landroid/telephony/ServiceState;

.field private mSharedPrefs:Landroid/content/SharedPreferences;

.field private mSignalStrength:Landroid/telephony/SignalStrength;

.field private mSignalStrengthImages:Landroid/content/res/TypedArray;

.field private mSkipButton:Landroid/widget/Button;

.field private mState:I

.field private final mTimeOutWaitingForCancel:Ljava/lang/Runnable;

.field private final mTimeOutWaitingForGenericActivation:Ljava/lang/Runnable;

.field private mTopDivider:Landroid/view/View;

.field private mTryAgainButton:Landroid/widget/Button;


# direct methods
.method public constructor <init>()V
    .locals 3

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 61
    invoke-direct {p0}, Lcom/google/android/setupwizard/BaseActivity;-><init>()V

    .line 122
    iput-boolean v2, p0, Lcom/google/android/setupwizard/carrier/ActivationActivity;->mCancelEnabled:Z

    .line 123
    const v0, 0x7f07004a

    iput v0, p0, Lcom/google/android/setupwizard/carrier/ActivationActivity;->mCancelLabel:I

    .line 124
    iput-boolean v2, p0, Lcom/google/android/setupwizard/carrier/ActivationActivity;->mNextEnabled:Z

    .line 125
    const v0, 0x7f070024

    iput v0, p0, Lcom/google/android/setupwizard/carrier/ActivationActivity;->mNextLabel:I

    .line 133
    iput-boolean v1, p0, Lcom/google/android/setupwizard/carrier/ActivationActivity;->mAllowDontShowMode:Z

    .line 147
    iput-boolean v1, p0, Lcom/google/android/setupwizard/carrier/ActivationActivity;->mGenericOtaspDefinitelyNeeded:Z

    .line 157
    new-instance v0, Lcom/google/android/setupwizard/carrier/ActivationActivity$1;

    invoke-direct {v0, p0}, Lcom/google/android/setupwizard/carrier/ActivationActivity$1;-><init>(Lcom/google/android/setupwizard/carrier/ActivationActivity;)V

    iput-object v0, p0, Lcom/google/android/setupwizard/carrier/ActivationActivity;->mTimeOutWaitingForGenericActivation:Ljava/lang/Runnable;

    .line 172
    const/16 v0, 0x2710

    iput v0, p0, Lcom/google/android/setupwizard/carrier/ActivationActivity;->GENERIC_TEST_SUCCESS_DELAY:I

    .line 173
    new-instance v0, Lcom/google/android/setupwizard/carrier/ActivationActivity$2;

    invoke-direct {v0, p0}, Lcom/google/android/setupwizard/carrier/ActivationActivity$2;-><init>(Lcom/google/android/setupwizard/carrier/ActivationActivity;)V

    iput-object v0, p0, Lcom/google/android/setupwizard/carrier/ActivationActivity;->mGenericTestSuccess:Ljava/lang/Runnable;

    .line 183
    iput v1, p0, Lcom/google/android/setupwizard/carrier/ActivationActivity;->STATE_NOT_STARTED:I

    .line 185
    iput v2, p0, Lcom/google/android/setupwizard/carrier/ActivationActivity;->STATE_WAITING_FOR_ACTIVATION_STATE:I

    .line 187
    const/4 v0, 0x2

    iput v0, p0, Lcom/google/android/setupwizard/carrier/ActivationActivity;->STATE_ACTIVATING:I

    .line 189
    const/4 v0, 0x3

    iput v0, p0, Lcom/google/android/setupwizard/carrier/ActivationActivity;->STATE_CANCEL_REQUESTED:I

    .line 191
    const/4 v0, 0x4

    iput v0, p0, Lcom/google/android/setupwizard/carrier/ActivationActivity;->STATE_CANCEL_COMPLETED:I

    .line 193
    const/4 v0, 0x5

    iput v0, p0, Lcom/google/android/setupwizard/carrier/ActivationActivity;->STATE_SUCCESS:I

    .line 195
    const/4 v0, 0x6

    iput v0, p0, Lcom/google/android/setupwizard/carrier/ActivationActivity;->STATE_FAILED:I

    .line 200
    const/4 v0, 0x7

    iput v0, p0, Lcom/google/android/setupwizard/carrier/ActivationActivity;->STATE_SHUTTING_DOWN:I

    .line 201
    iput v1, p0, Lcom/google/android/setupwizard/carrier/ActivationActivity;->mState:I

    .line 219
    new-instance v0, Lcom/google/android/setupwizard/carrier/ActivationActivity$3;

    invoke-direct {v0, p0}, Lcom/google/android/setupwizard/carrier/ActivationActivity$3;-><init>(Lcom/google/android/setupwizard/carrier/ActivationActivity;)V

    iput-object v0, p0, Lcom/google/android/setupwizard/carrier/ActivationActivity;->mPhoneStateListener:Landroid/telephony/PhoneStateListener;

    .line 260
    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    iput-object v0, p0, Lcom/google/android/setupwizard/carrier/ActivationActivity;->mHandler:Landroid/os/Handler;

    .line 261
    new-instance v0, Lcom/google/android/setupwizard/carrier/ActivationActivity$4;

    invoke-direct {v0, p0}, Lcom/google/android/setupwizard/carrier/ActivationActivity$4;-><init>(Lcom/google/android/setupwizard/carrier/ActivationActivity;)V

    iput-object v0, p0, Lcom/google/android/setupwizard/carrier/ActivationActivity;->mTimeOutWaitingForCancel:Ljava/lang/Runnable;

    return-void
.end method

.method static synthetic access$002(Lcom/google/android/setupwizard/carrier/ActivationActivity;J)J
    .locals 1
    .param p0, "x0"    # Lcom/google/android/setupwizard/carrier/ActivationActivity;
    .param p1, "x1"    # J

    .prologue
    .line 61
    iput-wide p1, p0, Lcom/google/android/setupwizard/carrier/ActivationActivity;->mGenericEndtime:J

    return-wide p1
.end method

.method static synthetic access$100(Lcom/google/android/setupwizard/carrier/ActivationActivity;)I
    .locals 1
    .param p0, "x0"    # Lcom/google/android/setupwizard/carrier/ActivationActivity;

    .prologue
    .line 61
    iget v0, p0, Lcom/google/android/setupwizard/carrier/ActivationActivity;->mState:I

    return v0
.end method

.method static synthetic access$1002(Lcom/google/android/setupwizard/carrier/ActivationActivity;J)J
    .locals 1
    .param p0, "x0"    # Lcom/google/android/setupwizard/carrier/ActivationActivity;
    .param p1, "x1"    # J

    .prologue
    .line 61
    iput-wide p1, p0, Lcom/google/android/setupwizard/carrier/ActivationActivity;->mCancelEndtime:J

    return-wide p1
.end method

.method static synthetic access$1100(Lcom/google/android/setupwizard/carrier/ActivationActivity;)V
    .locals 0
    .param p0, "x0"    # Lcom/google/android/setupwizard/carrier/ActivationActivity;

    .prologue
    .line 61
    invoke-direct {p0}, Lcom/google/android/setupwizard/carrier/ActivationActivity;->onCancelCompleted()V

    return-void
.end method

.method static synthetic access$1200(Lcom/google/android/setupwizard/carrier/ActivationActivity;)Ljava/lang/CharSequence;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/setupwizard/carrier/ActivationActivity;

    .prologue
    .line 61
    iget-object v0, p0, Lcom/google/android/setupwizard/carrier/ActivationActivity;->mNetworkName:Ljava/lang/CharSequence;

    return-object v0
.end method

.method static synthetic access$1202(Lcom/google/android/setupwizard/carrier/ActivationActivity;Ljava/lang/CharSequence;)Ljava/lang/CharSequence;
    .locals 0
    .param p0, "x0"    # Lcom/google/android/setupwizard/carrier/ActivationActivity;
    .param p1, "x1"    # Ljava/lang/CharSequence;

    .prologue
    .line 61
    iput-object p1, p0, Lcom/google/android/setupwizard/carrier/ActivationActivity;->mNetworkName:Ljava/lang/CharSequence;

    return-object p1
.end method

.method static synthetic access$1300(Landroid/content/Intent;)Ljava/lang/CharSequence;
    .locals 1
    .param p0, "x0"    # Landroid/content/Intent;

    .prologue
    .line 61
    invoke-static {p0}, Lcom/google/android/setupwizard/carrier/ActivationActivity;->getNetworkNameFromIntent(Landroid/content/Intent;)Ljava/lang/CharSequence;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$1400(Lcom/google/android/setupwizard/carrier/ActivationActivity;)Z
    .locals 1
    .param p0, "x0"    # Lcom/google/android/setupwizard/carrier/ActivationActivity;

    .prologue
    .line 61
    invoke-direct {p0}, Lcom/google/android/setupwizard/carrier/ActivationActivity;->shouldChangeTitleUsingNetworkName()Z

    move-result v0

    return v0
.end method

.method static synthetic access$1500(Lcom/google/android/setupwizard/carrier/ActivationActivity;)Landroid/widget/TextView;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/setupwizard/carrier/ActivationActivity;

    .prologue
    .line 61
    iget-object v0, p0, Lcom/google/android/setupwizard/carrier/ActivationActivity;->mActivationTitle:Landroid/widget/TextView;

    return-object v0
.end method

.method static synthetic access$1600(Lcom/google/android/setupwizard/carrier/ActivationActivity;)Landroid/widget/TextView;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/setupwizard/carrier/ActivationActivity;

    .prologue
    .line 61
    iget-object v0, p0, Lcom/google/android/setupwizard/carrier/ActivationActivity;->mCountdownText:Landroid/widget/TextView;

    return-object v0
.end method

.method static synthetic access$1700(Lcom/google/android/setupwizard/carrier/ActivationActivity;)V
    .locals 0
    .param p0, "x0"    # Lcom/google/android/setupwizard/carrier/ActivationActivity;

    .prologue
    .line 61
    invoke-direct {p0}, Lcom/google/android/setupwizard/carrier/ActivationActivity;->doShutdown()V

    return-void
.end method

.method static synthetic access$200(Lcom/google/android/setupwizard/carrier/ActivationActivity;)V
    .locals 0
    .param p0, "x0"    # Lcom/google/android/setupwizard/carrier/ActivationActivity;

    .prologue
    .line 61
    invoke-direct {p0}, Lcom/google/android/setupwizard/carrier/ActivationActivity;->onFailure()V

    return-void
.end method

.method static synthetic access$300(Lcom/google/android/setupwizard/carrier/ActivationActivity;)V
    .locals 0
    .param p0, "x0"    # Lcom/google/android/setupwizard/carrier/ActivationActivity;

    .prologue
    .line 61
    invoke-direct {p0}, Lcom/google/android/setupwizard/carrier/ActivationActivity;->onSuccess()V

    return-void
.end method

.method static synthetic access$402(Lcom/google/android/setupwizard/carrier/ActivationActivity;Landroid/telephony/SignalStrength;)Landroid/telephony/SignalStrength;
    .locals 0
    .param p0, "x0"    # Lcom/google/android/setupwizard/carrier/ActivationActivity;
    .param p1, "x1"    # Landroid/telephony/SignalStrength;

    .prologue
    .line 61
    iput-object p1, p0, Lcom/google/android/setupwizard/carrier/ActivationActivity;->mSignalStrength:Landroid/telephony/SignalStrength;

    return-object p1
.end method

.method static synthetic access$500(Lcom/google/android/setupwizard/carrier/ActivationActivity;)V
    .locals 0
    .param p0, "x0"    # Lcom/google/android/setupwizard/carrier/ActivationActivity;

    .prologue
    .line 61
    invoke-direct {p0}, Lcom/google/android/setupwizard/carrier/ActivationActivity;->updateSignalStrength()V

    return-void
.end method

.method static synthetic access$602(Lcom/google/android/setupwizard/carrier/ActivationActivity;Landroid/telephony/ServiceState;)Landroid/telephony/ServiceState;
    .locals 0
    .param p0, "x0"    # Lcom/google/android/setupwizard/carrier/ActivationActivity;
    .param p1, "x1"    # Landroid/telephony/ServiceState;

    .prologue
    .line 61
    iput-object p1, p0, Lcom/google/android/setupwizard/carrier/ActivationActivity;->mServiceState:Landroid/telephony/ServiceState;

    return-object p1
.end method

.method static synthetic access$700(Lcom/google/android/setupwizard/carrier/ActivationActivity;)V
    .locals 0
    .param p0, "x0"    # Lcom/google/android/setupwizard/carrier/ActivationActivity;

    .prologue
    .line 61
    invoke-direct {p0}, Lcom/google/android/setupwizard/carrier/ActivationActivity;->maybeActivateOrExit()V

    return-void
.end method

.method static synthetic access$800(Lcom/google/android/setupwizard/carrier/ActivationActivity;)Z
    .locals 1
    .param p0, "x0"    # Lcom/google/android/setupwizard/carrier/ActivationActivity;

    .prologue
    .line 61
    iget-boolean v0, p0, Lcom/google/android/setupwizard/carrier/ActivationActivity;->mGenericMode:Z

    return v0
.end method

.method static synthetic access$900(Lcom/google/android/setupwizard/carrier/ActivationActivity;)V
    .locals 0
    .param p0, "x0"    # Lcom/google/android/setupwizard/carrier/ActivationActivity;

    .prologue
    .line 61
    invoke-direct {p0}, Lcom/google/android/setupwizard/carrier/ActivationActivity;->maybeEndGenericActivation()V

    return-void
.end method

.method private animateProgressBar(Z)V
    .locals 4
    .param p1, "doAnimate"    # Z

    .prologue
    const/16 v2, 0x8

    const/4 v1, 0x0

    .line 1036
    iget-object v3, p0, Lcom/google/android/setupwizard/carrier/ActivationActivity;->mProgressBar:Landroid/widget/ProgressBar;

    if-eqz p1, :cond_1

    move v0, v1

    :goto_0
    invoke-virtual {v3, v0}, Landroid/widget/ProgressBar;->setVisibility(I)V

    .line 1037
    iget-object v0, p0, Lcom/google/android/setupwizard/carrier/ActivationActivity;->mTopDivider:Landroid/view/View;

    if-eqz v0, :cond_0

    .line 1038
    iget-object v0, p0, Lcom/google/android/setupwizard/carrier/ActivationActivity;->mTopDivider:Landroid/view/View;

    if-eqz p1, :cond_2

    :goto_1
    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    .line 1040
    :cond_0
    return-void

    :cond_1
    move v0, v2

    .line 1036
    goto :goto_0

    :cond_2
    move v2, v1

    .line 1038
    goto :goto_1
.end method

.method private static calculateSignalStrength(Landroid/telephony/SignalStrength;Landroid/telephony/ServiceState;I)I
    .locals 1
    .param p0, "signalStrength"    # Landroid/telephony/SignalStrength;
    .param p1, "serviceState"    # Landroid/telephony/ServiceState;
    .param p2, "phoneState"    # I

    .prologue
    .line 1126
    invoke-static {p0}, Lcom/google/android/setupwizard/carrier/ActivationActivity;->isCdma(Landroid/telephony/SignalStrength;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 1127
    invoke-static {p0}, Lcom/google/android/setupwizard/carrier/ActivationActivity;->getGsmLevel(Landroid/telephony/SignalStrength;)I

    move-result v0

    .line 1135
    :goto_0
    return v0

    .line 1132
    :cond_0
    if-nez p2, :cond_1

    invoke-static {p1}, Lcom/google/android/setupwizard/carrier/ActivationActivity;->isEvdo(Landroid/telephony/ServiceState;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1133
    invoke-static {p0}, Lcom/google/android/setupwizard/carrier/ActivationActivity;->getEvdoLevel(Landroid/telephony/SignalStrength;)I

    move-result v0

    goto :goto_0

    .line 1135
    :cond_1
    invoke-static {p0}, Lcom/google/android/setupwizard/carrier/ActivationActivity;->getCdmaLevel(Landroid/telephony/SignalStrength;)I

    move-result v0

    goto :goto_0
.end method

.method private doShutdown()V
    .locals 3

    .prologue
    .line 987
    new-instance v0, Landroid/content/Intent;

    const-string v1, "android.intent.action.ACTION_REQUEST_SHUTDOWN"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 988
    .local v0, "shutdownIntent":Landroid/content/Intent;
    const-string v1, "android.intent.extra.KEY_CONFIRM"

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 989
    invoke-virtual {p0, v0}, Lcom/google/android/setupwizard/carrier/ActivationActivity;->startActivity(Landroid/content/Intent;)V

    .line 990
    return-void
.end method

.method private static getCdmaLevel(Landroid/telephony/SignalStrength;)I
    .locals 5
    .param p0, "signalStrength"    # Landroid/telephony/SignalStrength;

    .prologue
    .line 1179
    invoke-virtual {p0}, Landroid/telephony/SignalStrength;->getCdmaDbm()I

    move-result v0

    .line 1180
    .local v0, "cdmaDbm":I
    invoke-virtual {p0}, Landroid/telephony/SignalStrength;->getCdmaEcio()I

    move-result v1

    .line 1181
    .local v1, "cdmaEcio":I
    const/4 v2, 0x0

    .line 1182
    .local v2, "levelDbm":I
    const/4 v3, 0x0

    .line 1184
    .local v3, "levelEcio":I
    const/16 v4, -0x4b

    if-lt v0, v4, :cond_0

    .line 1185
    const/4 v2, 0x4

    .line 1196
    :goto_0
    const/16 v4, -0x5a

    if-lt v1, v4, :cond_4

    .line 1197
    const/4 v3, 0x4

    .line 1207
    :goto_1
    if-ge v2, v3, :cond_8

    .end local v2    # "levelDbm":I
    :goto_2
    return v2

    .line 1186
    .restart local v2    # "levelDbm":I
    :cond_0
    const/16 v4, -0x55

    if-lt v0, v4, :cond_1

    .line 1187
    const/4 v2, 0x3

    goto :goto_0

    .line 1188
    :cond_1
    const/16 v4, -0x5f

    if-lt v0, v4, :cond_2

    .line 1189
    const/4 v2, 0x2

    goto :goto_0

    .line 1190
    :cond_2
    const/16 v4, -0x64

    if-lt v0, v4, :cond_3

    .line 1191
    const/4 v2, 0x1

    goto :goto_0

    .line 1193
    :cond_3
    const/4 v2, 0x0

    goto :goto_0

    .line 1198
    :cond_4
    const/16 v4, -0x6e

    if-lt v1, v4, :cond_5

    .line 1199
    const/4 v3, 0x3

    goto :goto_1

    .line 1200
    :cond_5
    const/16 v4, -0x82

    if-lt v1, v4, :cond_6

    .line 1201
    const/4 v3, 0x2

    goto :goto_1

    .line 1202
    :cond_6
    const/16 v4, -0x96

    if-lt v1, v4, :cond_7

    .line 1203
    const/4 v3, 0x1

    goto :goto_1

    .line 1205
    :cond_7
    const/4 v3, 0x0

    goto :goto_1

    :cond_8
    move v2, v3

    .line 1207
    goto :goto_2
.end method

.method private static getEvdoLevel(Landroid/telephony/SignalStrength;)I
    .locals 5
    .param p0, "signalStrength"    # Landroid/telephony/SignalStrength;

    .prologue
    .line 1214
    invoke-virtual {p0}, Landroid/telephony/SignalStrength;->getEvdoDbm()I

    move-result v0

    .line 1215
    .local v0, "evdoDbm":I
    invoke-virtual {p0}, Landroid/telephony/SignalStrength;->getEvdoSnr()I

    move-result v1

    .line 1216
    .local v1, "evdoSnr":I
    const/4 v2, 0x0

    .line 1217
    .local v2, "levelEvdoDbm":I
    const/4 v3, 0x0

    .line 1219
    .local v3, "levelEvdoSnr":I
    const/16 v4, -0x41

    if-lt v0, v4, :cond_0

    .line 1220
    const/4 v2, 0x4

    .line 1230
    :goto_0
    const/4 v4, 0x7

    if-lt v1, v4, :cond_4

    .line 1231
    const/4 v3, 0x4

    .line 1241
    :goto_1
    if-ge v2, v3, :cond_8

    .end local v2    # "levelEvdoDbm":I
    :goto_2
    return v2

    .line 1221
    .restart local v2    # "levelEvdoDbm":I
    :cond_0
    const/16 v4, -0x4b

    if-lt v0, v4, :cond_1

    .line 1222
    const/4 v2, 0x3

    goto :goto_0

    .line 1223
    :cond_1
    const/16 v4, -0x5a

    if-lt v0, v4, :cond_2

    .line 1224
    const/4 v2, 0x2

    goto :goto_0

    .line 1225
    :cond_2
    const/16 v4, -0x69

    if-lt v0, v4, :cond_3

    .line 1226
    const/4 v2, 0x1

    goto :goto_0

    .line 1228
    :cond_3
    const/4 v2, 0x0

    goto :goto_0

    .line 1232
    :cond_4
    const/4 v4, 0x5

    if-lt v1, v4, :cond_5

    .line 1233
    const/4 v3, 0x3

    goto :goto_1

    .line 1234
    :cond_5
    const/4 v4, 0x3

    if-lt v1, v4, :cond_6

    .line 1235
    const/4 v3, 0x2

    goto :goto_1

    .line 1236
    :cond_6
    const/4 v4, 0x1

    if-lt v1, v4, :cond_7

    .line 1237
    const/4 v3, 0x1

    goto :goto_1

    .line 1239
    :cond_7
    const/4 v3, 0x0

    goto :goto_1

    :cond_8
    move v2, v3

    .line 1241
    goto :goto_2
.end method

.method private getGenericActivationTimeout()I
    .locals 3

    .prologue
    .line 1031
    invoke-virtual {p0}, Lcom/google/android/setupwizard/carrier/ActivationActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f090001

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v0

    .line 1032
    .local v0, "timeout":I
    const-string v1, "ro.activation.timeout.millis"

    invoke-static {v1, v0}, Landroid/os/SystemProperties;->getInt(Ljava/lang/String;I)I

    move-result v1

    return v1
.end method

.method private static getGsmLevel(Landroid/telephony/SignalStrength;)I
    .locals 3
    .param p0, "signalStrength"    # Landroid/telephony/SignalStrength;

    .prologue
    const/4 v1, 0x2

    .line 1157
    invoke-virtual {p0}, Landroid/telephony/SignalStrength;->getGsmSignalStrength()I

    move-result v0

    .line 1163
    .local v0, "asu":I
    if-le v0, v1, :cond_0

    const/16 v2, 0x63

    if-ne v0, v2, :cond_2

    .line 1164
    :cond_0
    const/4 v1, 0x0

    .line 1172
    :cond_1
    :goto_0
    return v1

    .line 1165
    :cond_2
    const/16 v2, 0xc

    if-lt v0, v2, :cond_3

    .line 1166
    const/4 v1, 0x4

    goto :goto_0

    .line 1167
    :cond_3
    const/16 v2, 0x8

    if-lt v0, v2, :cond_4

    .line 1168
    const/4 v1, 0x3

    goto :goto_0

    .line 1169
    :cond_4
    const/4 v2, 0x5

    if-ge v0, v2, :cond_1

    .line 1172
    const/4 v1, 0x1

    goto :goto_0
.end method

.method private static getNetworkNameFromIntent(Landroid/content/Intent;)Ljava/lang/CharSequence;
    .locals 8
    .param p0, "intent"    # Landroid/content/Intent;

    .prologue
    const/4 v4, 0x1

    const/4 v5, 0x0

    .line 1249
    const-string v6, "showPlmn"

    const-string v7, "plmn"

    invoke-static {p0, v6, v7}, Lcom/google/android/setupwizard/carrier/ActivationActivity;->getSpecificNetworkName(Landroid/content/Intent;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/CharSequence;

    move-result-object v2

    .line 1250
    .local v2, "plmn":Ljava/lang/CharSequence;
    const-string v6, "showSpn"

    const-string v7, "spn"

    invoke-static {p0, v6, v7}, Lcom/google/android/setupwizard/carrier/ActivationActivity;->getSpecificNetworkName(Landroid/content/Intent;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/CharSequence;

    move-result-object v3

    .line 1251
    .local v3, "spn":Ljava/lang/CharSequence;
    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v6

    if-nez v6, :cond_1

    move v0, v4

    .line 1252
    .local v0, "havePlmn":Z
    :goto_0
    invoke-static {v3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v6

    if-nez v6, :cond_2

    move v1, v4

    .line 1254
    .local v1, "haveSpn":Z
    :goto_1
    if-eqz v0, :cond_3

    if-eqz v1, :cond_3

    .line 1255
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "|"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 1261
    .end local v2    # "plmn":Ljava/lang/CharSequence;
    :cond_0
    :goto_2
    return-object v2

    .end local v0    # "havePlmn":Z
    .end local v1    # "haveSpn":Z
    .restart local v2    # "plmn":Ljava/lang/CharSequence;
    :cond_1
    move v0, v5

    .line 1251
    goto :goto_0

    .restart local v0    # "havePlmn":Z
    :cond_2
    move v1, v5

    .line 1252
    goto :goto_1

    .line 1256
    .restart local v1    # "haveSpn":Z
    :cond_3
    if-nez v0, :cond_0

    .line 1258
    if-eqz v1, :cond_4

    move-object v2, v3

    .line 1259
    goto :goto_2

    .line 1261
    :cond_4
    const-string v2, ""

    goto :goto_2
.end method

.method private static getSpecificNetworkName(Landroid/content/Intent;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/CharSequence;
    .locals 1
    .param p0, "intent"    # Landroid/content/Intent;
    .param p1, "showNameParam"    # Ljava/lang/String;
    .param p2, "nameParam"    # Ljava/lang/String;

    .prologue
    .line 1273
    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1274
    invoke-virtual {p0, p2}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 1276
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private goNextOrFinish(I)V
    .locals 3
    .param p1, "result"    # I

    .prologue
    .line 996
    const-string v0, "ActivationActivity"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "goNextOrFinish("

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ")"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 997
    iget-boolean v0, p0, Lcom/google/android/setupwizard/carrier/ActivationActivity;->mDoExitToHome:Z

    if-eqz v0, :cond_0

    .line 998
    invoke-virtual {p0, p1}, Lcom/google/android/setupwizard/carrier/ActivationActivity;->finishAction(I)V

    .line 999
    new-instance v0, Landroid/content/Intent;

    const-string v1, "android.intent.action.MAIN"

    const/4 v2, 0x0

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;Landroid/net/Uri;)V

    const-string v1, "android.intent.category.HOME"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->addCategory(Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    const/high16 v1, 0x10200000

    invoke-virtual {v0, v1}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/android/setupwizard/carrier/ActivationActivity;->startActivity(Landroid/content/Intent;)V

    .line 1006
    :goto_0
    return-void

    .line 1004
    :cond_0
    invoke-virtual {p0, p1}, Lcom/google/android/setupwizard/carrier/ActivationActivity;->finishAction(I)V

    goto :goto_0
.end method

.method private initStandaloneView()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 740
    iget-object v0, p0, Lcom/google/android/setupwizard/carrier/ActivationActivity;->mActivationText:Landroid/widget/TextView;

    const v1, 0x7f07005a

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    .line 741
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/google/android/setupwizard/carrier/ActivationActivity;->setNextButtonVisible(Z)V

    .line 742
    const v0, 0x7f07005d

    invoke-direct {p0, v0}, Lcom/google/android/setupwizard/carrier/ActivationActivity;->setNextButtonLabel(I)V

    .line 743
    invoke-direct {p0, v2}, Lcom/google/android/setupwizard/carrier/ActivationActivity;->animateProgressBar(Z)V

    .line 744
    iget-object v0, p0, Lcom/google/android/setupwizard/carrier/ActivationActivity;->mSkipButton:Landroid/widget/Button;

    invoke-virtual {v0, v2}, Landroid/widget/Button;->setVisibility(I)V

    .line 745
    invoke-direct {p0, v2}, Lcom/google/android/setupwizard/carrier/ActivationActivity;->setCancelButtonVisible(Z)V

    .line 746
    return-void
.end method

.method private static isCdma(Landroid/telephony/SignalStrength;)Z
    .locals 1
    .param p0, "signalStrength"    # Landroid/telephony/SignalStrength;

    .prologue
    .line 1141
    if-eqz p0, :cond_0

    invoke-virtual {p0}, Landroid/telephony/SignalStrength;->isGsm()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private static isEvdo(Landroid/telephony/ServiceState;)Z
    .locals 3
    .param p0, "serviceState"    # Landroid/telephony/ServiceState;

    .prologue
    const/4 v1, 0x0

    .line 1145
    if-nez p0, :cond_1

    .line 1147
    :cond_0
    :goto_0
    return v1

    .line 1146
    :cond_1
    invoke-virtual {p0}, Landroid/telephony/ServiceState;->getNetworkType()I

    move-result v0

    .line 1147
    .local v0, "networkType":I
    const/4 v2, 0x5

    if-eq v0, v2, :cond_2

    const/4 v2, 0x6

    if-eq v0, v2, :cond_2

    const/16 v2, 0xc

    if-ne v0, v2, :cond_0

    :cond_2
    const/4 v1, 0x1

    goto :goto_0
.end method

.method private maybeActivateOrExit()V
    .locals 3

    .prologue
    .line 324
    const-string v0, "ActivationActivity"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "maybeActivateOrExit() mState == "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/google/android/setupwizard/carrier/ActivationActivity;->mState:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 329
    iget v0, p0, Lcom/google/android/setupwizard/carrier/ActivationActivity;->mState:I

    const/4 v1, 0x1

    if-eq v0, v1, :cond_1

    .line 349
    :cond_0
    :goto_0
    return-void

    .line 333
    :cond_1
    invoke-static {}, Lcom/google/android/setupwizard/carrier/PhoneMonitor;->otaspStateIsKnown()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 336
    invoke-static {}, Lcom/google/android/setupwizard/carrier/PhoneMonitor;->otaspIsNeeded()Z

    move-result v0

    if-nez v0, :cond_3

    invoke-virtual {p0}, Lcom/google/android/setupwizard/carrier/ActivationActivity;->isFirstRun()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 337
    const-string v0, "ActivationActivity"

    const-string v1, "maybeActivateOrExit(): activation not needed"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 338
    invoke-virtual {p0}, Lcom/google/android/setupwizard/carrier/ActivationActivity;->isPessimisticMode()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 339
    const-string v0, "ActivationActivity"

    const-string v1, "Be pessimistic. Show failure screen anyway."

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 340
    invoke-direct {p0}, Lcom/google/android/setupwizard/carrier/ActivationActivity;->onFailure()V

    goto :goto_0

    .line 342
    :cond_2
    invoke-direct {p0}, Lcom/google/android/setupwizard/carrier/ActivationActivity;->onSuccess()V

    goto :goto_0

    .line 344
    :cond_3
    invoke-static {}, Lcom/google/android/setupwizard/carrier/PhoneMonitor;->canActivate()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 345
    const-string v0, "ActivationActivity"

    const-string v1, "maybeActivateOrExit(): activation required"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 346
    invoke-direct {p0}, Lcom/google/android/setupwizard/carrier/ActivationActivity;->performActivationCall()V

    goto :goto_0
.end method

.method private maybeEndGenericActivation()V
    .locals 3

    .prologue
    .line 358
    const-string v0, "ActivationActivity"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "maybeEndGenericActivation() mState == "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/google/android/setupwizard/carrier/ActivationActivity;->mState:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 360
    iget-boolean v0, p0, Lcom/google/android/setupwizard/carrier/ActivationActivity;->mGenericMode:Z

    if-eqz v0, :cond_0

    invoke-static {}, Lcom/google/android/setupwizard/carrier/PhoneMonitor;->otaspIsNeeded()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 361
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/setupwizard/carrier/ActivationActivity;->mGenericOtaspDefinitelyNeeded:Z

    .line 363
    :cond_0
    iget-boolean v0, p0, Lcom/google/android/setupwizard/carrier/ActivationActivity;->mGenericMode:Z

    if-eqz v0, :cond_1

    iget v0, p0, Lcom/google/android/setupwizard/carrier/ActivationActivity;->mState:I

    const/4 v1, 0x2

    if-ne v0, v1, :cond_1

    invoke-static {}, Lcom/google/android/setupwizard/carrier/PhoneMonitor;->otaspIsNotNeeded()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 364
    invoke-direct {p0}, Lcom/google/android/setupwizard/carrier/ActivationActivity;->onSuccess()V

    .line 366
    :cond_1
    return-void
.end method

.method private onActualSkip()V
    .locals 3

    .prologue
    const/4 v2, 0x1

    .line 749
    const-string v0, "ActivationActivity"

    const-string v1, "onActualSkip"

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 752
    iget-boolean v0, p0, Lcom/google/android/setupwizard/carrier/ActivationActivity;->mAllowDontShowMode:Z

    if-eqz v0, :cond_0

    .line 753
    iget-object v0, p0, Lcom/google/android/setupwizard/carrier/ActivationActivity;->mSharedPrefs:Landroid/content/SharedPreferences;

    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    const-string v1, "dont_show_again"

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 755
    :cond_0
    invoke-direct {p0, v2}, Lcom/google/android/setupwizard/carrier/ActivationActivity;->goNextOrFinish(I)V

    .line 756
    return-void
.end method

.method private onCancelCompleted()V
    .locals 6

    .prologue
    const/4 v5, 0x4

    .line 911
    iget v0, p0, Lcom/google/android/setupwizard/carrier/ActivationActivity;->mState:I

    const/4 v1, 0x3

    if-eq v0, v1, :cond_0

    .line 912
    const-string v0, "ActivationActivity"

    const-string v1, "Unexpected state migration request (%d -> %d)."

    const/4 v2, 0x2

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    iget v4, p0, Lcom/google/android/setupwizard/carrier/ActivationActivity;->mState:I

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v2, v3

    const/4 v3, 0x1

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 918
    :cond_0
    const-string v0, "onCancelCompleted()"

    invoke-direct {p0, v5, v0}, Lcom/google/android/setupwizard/carrier/ActivationActivity;->setState(ILjava/lang/String;)V

    .line 919
    invoke-direct {p0}, Lcom/google/android/setupwizard/carrier/ActivationActivity;->showCancelCompletedScreen()V

    .line 920
    return-void
.end method

.method private onCancelRequested()V
    .locals 9

    .prologue
    const/4 v8, 0x3

    const/4 v4, 0x2

    const/4 v7, 0x1

    .line 867
    const-string v2, "ActivationActivity"

    const-string v3, "Cancel requested"

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 868
    iget v2, p0, Lcom/google/android/setupwizard/carrier/ActivationActivity;->mState:I

    if-eq v2, v7, :cond_0

    iget v2, p0, Lcom/google/android/setupwizard/carrier/ActivationActivity;->mState:I

    if-eq v2, v4, :cond_0

    .line 870
    const-string v2, "ActivationActivity"

    const-string v3, "Unexpected state migration request (%d -> %d). Ignored."

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    iget v6, p0, Lcom/google/android/setupwizard/carrier/ActivationActivity;->mState:I

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    aput-object v6, v4, v5

    invoke-static {v8}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v4, v7

    invoke-static {v3, v4}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 886
    :goto_0
    return-void

    .line 876
    :cond_0
    const-string v2, "onCancelRequested()"

    invoke-direct {p0, v8, v2}, Lcom/google/android/setupwizard/carrier/ActivationActivity;->setState(ILjava/lang/String;)V

    .line 878
    const-string v2, "phone"

    invoke-static {v2}, Landroid/os/ServiceManager;->checkService(Ljava/lang/String;)Landroid/os/IBinder;

    move-result-object v2

    invoke-static {v2}, Lcom/android/internal/telephony/ITelephony$Stub;->asInterface(Landroid/os/IBinder;)Lcom/android/internal/telephony/ITelephony;

    move-result-object v1

    .line 881
    .local v1, "telephony":Lcom/android/internal/telephony/ITelephony;
    :try_start_0
    invoke-interface {v1}, Lcom/android/internal/telephony/ITelephony;->endCall()Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 885
    :goto_1
    invoke-direct {p0}, Lcom/google/android/setupwizard/carrier/ActivationActivity;->showCancelRequestedScreen()V

    goto :goto_0

    .line 882
    :catch_0
    move-exception v0

    .line 883
    .local v0, "e":Landroid/os/RemoteException;
    const-string v2, "ActivationActivity"

    const-string v3, "Exception hanging up activation call: "

    invoke-static {v2, v3, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_1
.end method

.method private onFailure()V
    .locals 2

    .prologue
    .line 827
    const-string v0, "ActivationActivity"

    const-string v1, "onFailure"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 828
    const/4 v0, 0x6

    const-string v1, "onFailure()"

    invoke-direct {p0, v0, v1}, Lcom/google/android/setupwizard/carrier/ActivationActivity;->setState(ILjava/lang/String;)V

    .line 829
    invoke-direct {p0}, Lcom/google/android/setupwizard/carrier/ActivationActivity;->showFailureScreen()V

    .line 830
    return-void
.end method

.method private onNextButtonClick()V
    .locals 4

    .prologue
    const/4 v3, 0x5

    .line 289
    iget v1, p0, Lcom/google/android/setupwizard/carrier/ActivationActivity;->mState:I

    const/4 v2, 0x7

    if-ne v1, v2, :cond_0

    .line 290
    invoke-direct {p0}, Lcom/google/android/setupwizard/carrier/ActivationActivity;->doShutdown()V

    .line 304
    :goto_0
    return-void

    .line 291
    :cond_0
    iget-boolean v1, p0, Lcom/google/android/setupwizard/carrier/ActivationActivity;->mGenericMode:Z

    if-eqz v1, :cond_3

    iget v1, p0, Lcom/google/android/setupwizard/carrier/ActivationActivity;->mState:I

    if-ne v1, v3, :cond_1

    iget-boolean v1, p0, Lcom/google/android/setupwizard/carrier/ActivationActivity;->mGenericOtaspDefinitelyNeeded:Z

    if-nez v1, :cond_2

    :cond_1
    iget v1, p0, Lcom/google/android/setupwizard/carrier/ActivationActivity;->mState:I

    const/4 v2, 0x6

    if-ne v1, v2, :cond_3

    .line 293
    :cond_2
    const-string v1, "power"

    invoke-virtual {p0, v1}, Lcom/google/android/setupwizard/carrier/ActivationActivity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/os/PowerManager;

    .line 294
    .local v0, "powerManager":Landroid/os/PowerManager;
    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/os/PowerManager;->reboot(Ljava/lang/String;)V

    goto :goto_0

    .line 295
    .end local v0    # "powerManager":Landroid/os/PowerManager;
    :cond_3
    iget v1, p0, Lcom/google/android/setupwizard/carrier/ActivationActivity;->mState:I

    if-eq v1, v3, :cond_4

    invoke-virtual {p0}, Lcom/google/android/setupwizard/carrier/ActivationActivity;->isFirstRun()Z

    move-result v1

    if-eqz v1, :cond_5

    .line 296
    :cond_4
    const-string v1, "ActivationActivity"

    const-string v2, "onClickView: exiting after success"

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 297
    const/4 v1, -0x1

    invoke-direct {p0, v1}, Lcom/google/android/setupwizard/carrier/ActivationActivity;->goNextOrFinish(I)V

    goto :goto_0

    .line 301
    :cond_5
    const-string v1, "ActivationActivity"

    const-string v2, "onClickView: Activiting, next button pressed"

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 302
    invoke-direct {p0}, Lcom/google/android/setupwizard/carrier/ActivationActivity;->startActivationProcess()V

    goto :goto_0
.end method

.method private onSpcRetriesFailure()V
    .locals 6

    .prologue
    .line 938
    const-string v0, "ActivationActivity"

    const-string v1, "onSpcRetriesFailure"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 939
    const/4 v0, 0x7

    const-string v1, "onSpcRetriesFailure()"

    invoke-direct {p0, v0, v1}, Lcom/google/android/setupwizard/carrier/ActivationActivity;->setState(ILjava/lang/String;)V

    .line 941
    invoke-direct {p0}, Lcom/google/android/setupwizard/carrier/ActivationActivity;->showShuttingDownScreen()V

    .line 943
    new-instance v0, Lcom/google/android/setupwizard/carrier/ActivationActivity$6;

    const-wide/32 v2, 0xea60

    const-wide/16 v4, 0x3e8

    move-object v1, p0

    invoke-direct/range {v0 .. v5}, Lcom/google/android/setupwizard/carrier/ActivationActivity$6;-><init>(Lcom/google/android/setupwizard/carrier/ActivationActivity;JJ)V

    invoke-virtual {v0}, Lcom/google/android/setupwizard/carrier/ActivationActivity$6;->start()Landroid/os/CountDownTimer;

    .line 955
    return-void
.end method

.method private onStateChanges()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    .line 376
    const-string v0, "ActivationActivity"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "onStateChanges() mState == "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/google/android/setupwizard/carrier/ActivationActivity;->mState:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 378
    iget-boolean v0, p0, Lcom/google/android/setupwizard/carrier/ActivationActivity;->mGenericMode:Z

    if-eqz v0, :cond_2

    .line 379
    invoke-static {}, Lcom/google/android/setupwizard/carrier/PhoneMonitor;->otaspIsNeeded()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 380
    const-string v0, "ActivationActivity"

    const-string v1, "onStateChanges(): generic otasp definitely needed"

    invoke-static {v0, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 381
    iput-boolean v3, p0, Lcom/google/android/setupwizard/carrier/ActivationActivity;->mGenericOtaspDefinitelyNeeded:Z

    .line 383
    :cond_0
    iget v0, p0, Lcom/google/android/setupwizard/carrier/ActivationActivity;->mState:I

    const/4 v1, 0x2

    if-ne v0, v1, :cond_1

    invoke-static {}, Lcom/google/android/setupwizard/carrier/PhoneMonitor;->otaspIsNotNeeded()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 384
    const-string v0, "ActivationActivity"

    const-string v1, "onStateChanges(): generic otasp succeeded"

    invoke-static {v0, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 385
    invoke-direct {p0}, Lcom/google/android/setupwizard/carrier/ActivationActivity;->onSuccess()V

    .line 400
    :cond_1
    :goto_0
    return-void

    .line 387
    :cond_2
    iget v0, p0, Lcom/google/android/setupwizard/carrier/ActivationActivity;->mState:I

    if-ne v0, v3, :cond_1

    invoke-static {}, Lcom/google/android/setupwizard/carrier/PhoneMonitor;->otaspStateIsKnown()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 392
    invoke-static {}, Lcom/google/android/setupwizard/carrier/PhoneMonitor;->otaspIsNeeded()Z

    move-result v0

    if-nez v0, :cond_3

    invoke-virtual {p0}, Lcom/google/android/setupwizard/carrier/ActivationActivity;->isFirstRun()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 393
    const-string v0, "ActivationActivity"

    const-string v1, "onStateChanges(): activation not needed after all"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 394
    invoke-direct {p0}, Lcom/google/android/setupwizard/carrier/ActivationActivity;->onSuccess()V

    goto :goto_0

    .line 395
    :cond_3
    invoke-static {}, Lcom/google/android/setupwizard/carrier/PhoneMonitor;->canActivate()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 396
    const-string v0, "ActivationActivity"

    const-string v1, "onStateChanges(): activation call now possible"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 397
    invoke-direct {p0}, Lcom/google/android/setupwizard/carrier/ActivationActivity;->performActivationCall()V

    goto :goto_0
.end method

.method private onSuccess()V
    .locals 2

    .prologue
    .line 802
    const-string v0, "ActivationActivity"

    const-string v1, "onSuccess"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 803
    const/4 v0, 0x5

    const-string v1, "onSuccess"

    invoke-direct {p0, v0, v1}, Lcom/google/android/setupwizard/carrier/ActivationActivity;->setState(ILjava/lang/String;)V

    .line 804
    invoke-direct {p0}, Lcom/google/android/setupwizard/carrier/ActivationActivity;->showSuccessScreen()V

    .line 805
    return-void
.end method

.method private performActivationCall()V
    .locals 7

    .prologue
    .line 721
    const-string v4, "ActivationActivity"

    const-string v5, "startActivationCall"

    invoke-static {v4, v5}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 722
    const/4 v4, 0x2

    const-string v5, "performActivationCall()"

    invoke-direct {p0, v4, v5}, Lcom/google/android/setupwizard/carrier/ActivationActivity;->setState(ILjava/lang/String;)V

    .line 726
    :try_start_0
    new-instance v1, Landroid/content/Intent;

    const-string v4, "com.android.phone.PERFORM_CDMA_PROVISIONING"

    invoke-direct {v1, v4}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 727
    .local v1, "otaspIntent":Landroid/content/Intent;
    new-instance v3, Landroid/content/Intent;

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v4

    invoke-direct {v3, p0, v4}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 728
    .local v3, "resultIntent":Landroid/content/Intent;
    const/4 v4, 0x0

    const/4 v5, 0x0

    invoke-static {p0, v4, v3, v5}, Landroid/app/PendingIntent;->getActivity(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v2

    .line 729
    .local v2, "pendingResultIntent":Landroid/app/PendingIntent;
    const-string v4, "otasp_result_code_pending_intent"

    invoke-virtual {v1, v4, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 730
    const/16 v4, 0x4e21

    invoke-virtual {p0, v1, v4}, Lcom/google/android/setupwizard/carrier/ActivationActivity;->startActivityForResult(Landroid/content/Intent;I)V
    :try_end_0
    .catch Landroid/content/ActivityNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    .line 734
    .end local v1    # "otaspIntent":Landroid/content/Intent;
    .end local v2    # "pendingResultIntent":Landroid/app/PendingIntent;
    .end local v3    # "resultIntent":Landroid/content/Intent;
    :goto_0
    return-void

    .line 731
    :catch_0
    move-exception v0

    .line 732
    .local v0, "e":Landroid/content/ActivityNotFoundException;
    const-string v4, "ActivationActivity"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "Couldn\'t start activation call; ActivityNotFoundException: "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method private setCancelButtonEnabled(Z)V
    .locals 1
    .param p1, "enabled"    # Z

    .prologue
    .line 1068
    iget-object v0, p0, Lcom/google/android/setupwizard/carrier/ActivationActivity;->mCancelButton:Landroid/widget/Button;

    if-eqz v0, :cond_0

    .line 1069
    iget-object v0, p0, Lcom/google/android/setupwizard/carrier/ActivationActivity;->mCancelButton:Landroid/widget/Button;

    invoke-virtual {v0, p1}, Landroid/widget/Button;->setEnabled(Z)V

    .line 1071
    :cond_0
    iput-boolean p1, p0, Lcom/google/android/setupwizard/carrier/ActivationActivity;->mCancelEnabled:Z

    .line 1072
    invoke-direct {p0}, Lcom/google/android/setupwizard/carrier/ActivationActivity;->updateNavBarButton()V

    .line 1073
    return-void
.end method

.method private setCancelButtonLabel(I)V
    .locals 1
    .param p1, "label"    # I

    .prologue
    .line 1076
    iget-object v0, p0, Lcom/google/android/setupwizard/carrier/ActivationActivity;->mCancelButton:Landroid/widget/Button;

    if-eqz v0, :cond_0

    .line 1077
    iget-object v0, p0, Lcom/google/android/setupwizard/carrier/ActivationActivity;->mCancelButton:Landroid/widget/Button;

    invoke-virtual {v0, p1}, Landroid/widget/Button;->setText(I)V

    .line 1079
    :cond_0
    iput p1, p0, Lcom/google/android/setupwizard/carrier/ActivationActivity;->mCancelLabel:I

    .line 1080
    invoke-direct {p0}, Lcom/google/android/setupwizard/carrier/ActivationActivity;->updateNavBarButton()V

    .line 1081
    return-void
.end method

.method private setCancelButtonVisible(Z)V
    .locals 2
    .param p1, "visible"    # Z

    .prologue
    .line 1059
    iget-object v0, p0, Lcom/google/android/setupwizard/carrier/ActivationActivity;->mCancelButton:Landroid/widget/Button;

    if-eqz v0, :cond_0

    .line 1060
    iget-object v1, p0, Lcom/google/android/setupwizard/carrier/ActivationActivity;->mCancelButton:Landroid/widget/Button;

    if-eqz p1, :cond_1

    const/4 v0, 0x0

    :goto_0
    invoke-virtual {v1, v0}, Landroid/widget/Button;->setVisibility(I)V

    .line 1061
    iget-object v0, p0, Lcom/google/android/setupwizard/carrier/ActivationActivity;->mCancelButton:Landroid/widget/Button;

    invoke-virtual {v0, p1}, Landroid/widget/Button;->setEnabled(Z)V

    .line 1063
    :cond_0
    iput-boolean p1, p0, Lcom/google/android/setupwizard/carrier/ActivationActivity;->mCancelEnabled:Z

    .line 1064
    invoke-direct {p0}, Lcom/google/android/setupwizard/carrier/ActivationActivity;->updateNavBarButton()V

    .line 1065
    return-void

    .line 1060
    :cond_1
    const/16 v0, 0x8

    goto :goto_0
.end method

.method private setNextButtonLabel(I)V
    .locals 1
    .param p1, "label"    # I

    .prologue
    .line 1051
    iget-object v0, p0, Lcom/google/android/setupwizard/carrier/ActivationActivity;->mNextButton:Landroid/widget/Button;

    if-eqz v0, :cond_0

    .line 1052
    iget-object v0, p0, Lcom/google/android/setupwizard/carrier/ActivationActivity;->mNextButton:Landroid/widget/Button;

    invoke-virtual {v0, p1}, Landroid/widget/Button;->setText(I)V

    .line 1054
    :cond_0
    iput p1, p0, Lcom/google/android/setupwizard/carrier/ActivationActivity;->mNextLabel:I

    .line 1055
    invoke-direct {p0}, Lcom/google/android/setupwizard/carrier/ActivationActivity;->updateNavBarButton()V

    .line 1056
    return-void
.end method

.method private setNextButtonVisible(Z)V
    .locals 2
    .param p1, "visible"    # Z

    .prologue
    .line 1043
    iget-object v0, p0, Lcom/google/android/setupwizard/carrier/ActivationActivity;->mNextButton:Landroid/widget/Button;

    if-eqz v0, :cond_0

    .line 1044
    iget-object v1, p0, Lcom/google/android/setupwizard/carrier/ActivationActivity;->mNextButton:Landroid/widget/Button;

    if-eqz p1, :cond_1

    const/4 v0, 0x0

    :goto_0
    invoke-virtual {v1, v0}, Landroid/widget/Button;->setVisibility(I)V

    .line 1046
    :cond_0
    iput-boolean p1, p0, Lcom/google/android/setupwizard/carrier/ActivationActivity;->mNextEnabled:Z

    .line 1047
    invoke-direct {p0}, Lcom/google/android/setupwizard/carrier/ActivationActivity;->updateNavBarButton()V

    .line 1048
    return-void

    .line 1044
    :cond_1
    const/16 v0, 0x8

    goto :goto_0
.end method

.method private setSkipButtonVisible(Z)V
    .locals 2
    .param p1, "visible"    # Z

    .prologue
    .line 1084
    iget-object v0, p0, Lcom/google/android/setupwizard/carrier/ActivationActivity;->mSkipButton:Landroid/widget/Button;

    if-eqz v0, :cond_0

    .line 1085
    iget-object v1, p0, Lcom/google/android/setupwizard/carrier/ActivationActivity;->mSkipButton:Landroid/widget/Button;

    if-eqz p1, :cond_1

    const/4 v0, 0x0

    :goto_0
    invoke-virtual {v1, v0}, Landroid/widget/Button;->setVisibility(I)V

    .line 1087
    :cond_0
    return-void

    .line 1085
    :cond_1
    const/16 v0, 0x8

    goto :goto_0
.end method

.method private setState(ILjava/lang/String;)V
    .locals 3
    .param p1, "state"    # I
    .param p2, "functionName"    # Ljava/lang/String;

    .prologue
    .line 204
    const-string v0, "ActivationActivity"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "setState(): "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    if-eqz p2, :cond_0

    .end local p2    # "functionName":Ljava/lang/String;
    :goto_0
    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " changing mState from "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/google/android/setupwizard/carrier/ActivationActivity;->mState:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " to "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 206
    iput p1, p0, Lcom/google/android/setupwizard/carrier/ActivationActivity;->mState:I

    .line 207
    return-void

    .line 204
    .restart local p2    # "functionName":Ljava/lang/String;
    :cond_0
    const-string p2, ""

    goto :goto_0
.end method

.method private setTryAgainButtonVisible(Z)V
    .locals 2
    .param p1, "visible"    # Z

    .prologue
    .line 1090
    iget-object v0, p0, Lcom/google/android/setupwizard/carrier/ActivationActivity;->mTryAgainButton:Landroid/widget/Button;

    if-eqz v0, :cond_0

    .line 1091
    iget-object v1, p0, Lcom/google/android/setupwizard/carrier/ActivationActivity;->mTryAgainButton:Landroid/widget/Button;

    if-eqz p1, :cond_1

    const/4 v0, 0x0

    :goto_0
    invoke-virtual {v1, v0}, Landroid/widget/Button;->setVisibility(I)V

    .line 1093
    :cond_0
    return-void

    .line 1091
    :cond_1
    const/16 v0, 0x8

    goto :goto_0
.end method

.method private shouldChangeTitleUsingNetworkName()Z
    .locals 3

    .prologue
    const/4 v0, 0x1

    .line 1024
    iget v1, p0, Lcom/google/android/setupwizard/carrier/ActivationActivity;->mState:I

    if-eqz v1, :cond_0

    iget v1, p0, Lcom/google/android/setupwizard/carrier/ActivationActivity;->mState:I

    if-eq v1, v0, :cond_0

    iget v1, p0, Lcom/google/android/setupwizard/carrier/ActivationActivity;->mState:I

    const/4 v2, 0x2

    if-ne v1, v2, :cond_1

    :cond_0
    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private showActivationProcessScreen()V
    .locals 6

    .prologue
    const v2, 0x7f070057

    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 759
    const-string v0, "ActivationActivity"

    const-string v1, "showProgress()"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 761
    iget-object v0, p0, Lcom/google/android/setupwizard/carrier/ActivationActivity;->mNetworkName:Ljava/lang/CharSequence;

    invoke-virtual {p0, v2}, Lcom/google/android/setupwizard/carrier/ActivationActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 762
    iget-object v0, p0, Lcom/google/android/setupwizard/carrier/ActivationActivity;->mActivationTitle:Landroid/widget/TextView;

    const v1, 0x7f070055

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    .line 766
    :goto_0
    invoke-direct {p0, v4}, Lcom/google/android/setupwizard/carrier/ActivationActivity;->setTryAgainButtonVisible(Z)V

    .line 767
    invoke-direct {p0, v5}, Lcom/google/android/setupwizard/carrier/ActivationActivity;->animateProgressBar(Z)V

    .line 768
    invoke-direct {p0, v4}, Lcom/google/android/setupwizard/carrier/ActivationActivity;->setNextButtonVisible(Z)V

    .line 769
    invoke-direct {p0, v4}, Lcom/google/android/setupwizard/carrier/ActivationActivity;->setSkipButtonVisible(Z)V

    .line 770
    iget-boolean v0, p0, Lcom/google/android/setupwizard/carrier/ActivationActivity;->mGenericMode:Z

    if-eqz v0, :cond_2

    .line 771
    invoke-direct {p0, v4}, Lcom/google/android/setupwizard/carrier/ActivationActivity;->setCancelButtonVisible(Z)V

    .line 780
    :cond_0
    :goto_1
    monitor-enter p0

    .line 781
    :try_start_0
    iget-boolean v0, p0, Lcom/google/android/setupwizard/carrier/ActivationActivity;->mGenericMode:Z

    if-eqz v0, :cond_3

    .line 782
    iget-object v0, p0, Lcom/google/android/setupwizard/carrier/ActivationActivity;->mActivationText:Landroid/widget/TextView;

    const v1, 0x7f070060

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    .line 798
    :goto_2
    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 799
    return-void

    .line 764
    :cond_1
    iget-object v0, p0, Lcom/google/android/setupwizard/carrier/ActivationActivity;->mActivationTitle:Landroid/widget/TextView;

    const v1, 0x7f070056

    new-array v2, v5, [Ljava/lang/Object;

    iget-object v3, p0, Lcom/google/android/setupwizard/carrier/ActivationActivity;->mNetworkName:Ljava/lang/CharSequence;

    aput-object v3, v2, v4

    invoke-virtual {p0, v1, v2}, Lcom/google/android/setupwizard/carrier/ActivationActivity;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0

    .line 773
    :cond_2
    invoke-direct {p0, v5}, Lcom/google/android/setupwizard/carrier/ActivationActivity;->setCancelButtonVisible(Z)V

    .line 774
    invoke-virtual {p0}, Lcom/google/android/setupwizard/carrier/ActivationActivity;->isFirstRun()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 775
    const v0, 0x7f070046

    invoke-direct {p0, v0}, Lcom/google/android/setupwizard/carrier/ActivationActivity;->setCancelButtonLabel(I)V

    goto :goto_1

    .line 783
    :cond_3
    :try_start_1
    invoke-virtual {p0}, Lcom/google/android/setupwizard/carrier/ActivationActivity;->isFirstRun()Z

    move-result v0

    if-eqz v0, :cond_5

    .line 784
    iget-object v0, p0, Lcom/google/android/setupwizard/carrier/ActivationActivity;->mNetworkName:Ljava/lang/CharSequence;

    const v1, 0x7f070057

    invoke-virtual {p0, v1}, Lcom/google/android/setupwizard/carrier/ActivationActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 785
    iget-object v0, p0, Lcom/google/android/setupwizard/carrier/ActivationActivity;->mActivationText:Landroid/widget/TextView;

    const v1, 0x7f07005e

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    goto :goto_2

    .line 798
    :catchall_0
    move-exception v0

    monitor-exit p0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0

    .line 788
    :cond_4
    :try_start_2
    iget-object v0, p0, Lcom/google/android/setupwizard/carrier/ActivationActivity;->mActivationText:Landroid/widget/TextView;

    const v1, 0x7f07005f

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    goto :goto_2

    .line 791
    :cond_5
    iget-object v0, p0, Lcom/google/android/setupwizard/carrier/ActivationActivity;->mNetworkName:Ljava/lang/CharSequence;

    const v1, 0x7f070057

    invoke-virtual {p0, v1}, Lcom/google/android/setupwizard/carrier/ActivationActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_6

    .line 792
    iget-object v0, p0, Lcom/google/android/setupwizard/carrier/ActivationActivity;->mActivationText:Landroid/widget/TextView;

    const v1, 0x7f070062

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    goto :goto_2

    .line 795
    :cond_6
    iget-object v0, p0, Lcom/google/android/setupwizard/carrier/ActivationActivity;->mActivationText:Landroid/widget/TextView;

    const v1, 0x7f070063

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_2
.end method

.method private showCancelCompletedScreen()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 923
    invoke-direct {p0, v2}, Lcom/google/android/setupwizard/carrier/ActivationActivity;->animateProgressBar(Z)V

    .line 924
    invoke-direct {p0, v2}, Lcom/google/android/setupwizard/carrier/ActivationActivity;->setCancelButtonVisible(Z)V

    .line 925
    iget-object v0, p0, Lcom/google/android/setupwizard/carrier/ActivationActivity;->mActivationTitle:Landroid/widget/TextView;

    const v1, 0x7f07006e

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    .line 926
    invoke-direct {p0, v3}, Lcom/google/android/setupwizard/carrier/ActivationActivity;->setNextButtonVisible(Z)V

    .line 927
    invoke-virtual {p0}, Lcom/google/android/setupwizard/carrier/ActivationActivity;->isFirstRun()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 928
    invoke-direct {p0, v3}, Lcom/google/android/setupwizard/carrier/ActivationActivity;->setTryAgainButtonVisible(Z)V

    .line 929
    iget-object v0, p0, Lcom/google/android/setupwizard/carrier/ActivationActivity;->mActivationText:Landroid/widget/TextView;

    const v1, 0x7f07006f

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    .line 935
    :goto_0
    return-void

    .line 931
    :cond_0
    const v0, 0x7f07005d

    invoke-direct {p0, v0}, Lcom/google/android/setupwizard/carrier/ActivationActivity;->setNextButtonLabel(I)V

    .line 932
    invoke-direct {p0, v2}, Lcom/google/android/setupwizard/carrier/ActivationActivity;->setSkipButtonVisible(Z)V

    .line 933
    iget-object v0, p0, Lcom/google/android/setupwizard/carrier/ActivationActivity;->mActivationText:Landroid/widget/TextView;

    const v1, 0x7f070070

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    goto :goto_0
.end method

.method private showCancelRequestedScreen()V
    .locals 6

    .prologue
    const-wide/16 v4, 0x2710

    const/4 v2, 0x0

    .line 892
    iget-object v0, p0, Lcom/google/android/setupwizard/carrier/ActivationActivity;->mActivationTitle:Landroid/widget/TextView;

    const v1, 0x7f07006c

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    .line 893
    iget-object v0, p0, Lcom/google/android/setupwizard/carrier/ActivationActivity;->mActivationText:Landroid/widget/TextView;

    const v1, 0x7f07006d

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    .line 894
    invoke-direct {p0, v2}, Lcom/google/android/setupwizard/carrier/ActivationActivity;->animateProgressBar(Z)V

    .line 895
    invoke-direct {p0, v2}, Lcom/google/android/setupwizard/carrier/ActivationActivity;->setNextButtonVisible(Z)V

    .line 896
    invoke-direct {p0, v2}, Lcom/google/android/setupwizard/carrier/ActivationActivity;->setSkipButtonVisible(Z)V

    .line 897
    invoke-direct {p0, v2}, Lcom/google/android/setupwizard/carrier/ActivationActivity;->setCancelButtonEnabled(Z)V

    .line 903
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v0

    add-long/2addr v0, v4

    iput-wide v0, p0, Lcom/google/android/setupwizard/carrier/ActivationActivity;->mCancelEndtime:J

    .line 904
    iget-object v0, p0, Lcom/google/android/setupwizard/carrier/ActivationActivity;->mHandler:Landroid/os/Handler;

    iget-object v1, p0, Lcom/google/android/setupwizard/carrier/ActivationActivity;->mTimeOutWaitingForCancel:Ljava/lang/Runnable;

    invoke-virtual {v0, v1, v4, v5}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 905
    return-void
.end method

.method private showFailureScreen()V
    .locals 5

    .prologue
    const v0, 0x7f07006a

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 833
    const-string v1, "ActivationActivity"

    const-string v2, "showFailureScreen()"

    invoke-static {v1, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 835
    iget-object v1, p0, Lcom/google/android/setupwizard/carrier/ActivationActivity;->mNetworkNotifier:Landroid/view/View;

    if-eqz v1, :cond_0

    .line 836
    iget-object v1, p0, Lcom/google/android/setupwizard/carrier/ActivationActivity;->mNetworkNotifier:Landroid/view/View;

    const/16 v2, 0x8

    invoke-virtual {v1, v2}, Landroid/view/View;->setVisibility(I)V

    .line 838
    :cond_0
    invoke-direct {p0, v3}, Lcom/google/android/setupwizard/carrier/ActivationActivity;->animateProgressBar(Z)V

    .line 839
    invoke-direct {p0, v4}, Lcom/google/android/setupwizard/carrier/ActivationActivity;->setNextButtonVisible(Z)V

    .line 840
    iget-object v1, p0, Lcom/google/android/setupwizard/carrier/ActivationActivity;->mActivationTitle:Landroid/widget/TextView;

    const v2, 0x7f070068

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(I)V

    .line 841
    invoke-direct {p0, v3}, Lcom/google/android/setupwizard/carrier/ActivationActivity;->setCancelButtonVisible(Z)V

    .line 842
    invoke-virtual {p0}, Lcom/google/android/setupwizard/carrier/ActivationActivity;->isFirstRun()Z

    move-result v1

    if-eqz v1, :cond_3

    .line 843
    iget-boolean v1, p0, Lcom/google/android/setupwizard/carrier/ActivationActivity;->mGenericMode:Z

    if-eqz v1, :cond_1

    .line 844
    invoke-direct {p0, v3}, Lcom/google/android/setupwizard/carrier/ActivationActivity;->setTryAgainButtonVisible(Z)V

    .line 845
    const v1, 0x7f07004b

    invoke-direct {p0, v1}, Lcom/google/android/setupwizard/carrier/ActivationActivity;->setNextButtonLabel(I)V

    .line 850
    :goto_0
    iget-object v1, p0, Lcom/google/android/setupwizard/carrier/ActivationActivity;->mActivationText:Landroid/widget/TextView;

    iget-boolean v2, p0, Lcom/google/android/setupwizard/carrier/ActivationActivity;->mGenericMode:Z

    if-eqz v2, :cond_2

    :goto_1
    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setText(I)V

    .line 860
    :goto_2
    return-void

    .line 847
    :cond_1
    invoke-direct {p0, v4}, Lcom/google/android/setupwizard/carrier/ActivationActivity;->setTryAgainButtonVisible(Z)V

    goto :goto_0

    .line 850
    :cond_2
    const v0, 0x7f070069

    goto :goto_1

    .line 853
    :cond_3
    invoke-direct {p0, v3}, Lcom/google/android/setupwizard/carrier/ActivationActivity;->setTryAgainButtonVisible(Z)V

    .line 854
    const v1, 0x7f07004c

    invoke-direct {p0, v1}, Lcom/google/android/setupwizard/carrier/ActivationActivity;->setNextButtonLabel(I)V

    .line 855
    invoke-direct {p0, v4}, Lcom/google/android/setupwizard/carrier/ActivationActivity;->setSkipButtonVisible(Z)V

    .line 856
    iget-object v1, p0, Lcom/google/android/setupwizard/carrier/ActivationActivity;->mSkipButton:Landroid/widget/Button;

    invoke-virtual {v1, v3}, Landroid/widget/Button;->setVisibility(I)V

    .line 857
    iget-object v1, p0, Lcom/google/android/setupwizard/carrier/ActivationActivity;->mActivationText:Landroid/widget/TextView;

    iget-boolean v2, p0, Lcom/google/android/setupwizard/carrier/ActivationActivity;->mGenericMode:Z

    if-eqz v2, :cond_4

    :goto_3
    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setText(I)V

    goto :goto_2

    :cond_4
    const v0, 0x7f07006b

    goto :goto_3
.end method

.method private showShuttingDownScreen()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 958
    iget-object v1, p0, Lcom/google/android/setupwizard/carrier/ActivationActivity;->mNetworkNotifier:Landroid/view/View;

    if-eqz v1, :cond_0

    .line 959
    iget-object v1, p0, Lcom/google/android/setupwizard/carrier/ActivationActivity;->mNetworkNotifier:Landroid/view/View;

    const/16 v2, 0x8

    invoke-virtual {v1, v2}, Landroid/view/View;->setVisibility(I)V

    .line 961
    :cond_0
    invoke-direct {p0, v3}, Lcom/google/android/setupwizard/carrier/ActivationActivity;->animateProgressBar(Z)V

    .line 962
    const v1, 0x7f070072

    invoke-direct {p0, v1}, Lcom/google/android/setupwizard/carrier/ActivationActivity;->setNextButtonLabel(I)V

    .line 963
    const/4 v1, 0x1

    invoke-direct {p0, v1}, Lcom/google/android/setupwizard/carrier/ActivationActivity;->setNextButtonVisible(Z)V

    .line 964
    invoke-direct {p0, v3}, Lcom/google/android/setupwizard/carrier/ActivationActivity;->setSkipButtonVisible(Z)V

    .line 965
    invoke-direct {p0, v3}, Lcom/google/android/setupwizard/carrier/ActivationActivity;->setCancelButtonVisible(Z)V

    .line 966
    iget-object v1, p0, Lcom/google/android/setupwizard/carrier/ActivationActivity;->mActivationTitle:Landroid/widget/TextView;

    const v2, 0x7f070068

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(I)V

    .line 967
    iget-object v1, p0, Lcom/google/android/setupwizard/carrier/ActivationActivity;->mActivationText:Landroid/widget/TextView;

    const v2, 0x7f070071

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(I)V

    .line 969
    const-string v1, "statusbar"

    invoke-virtual {p0, v1}, Lcom/google/android/setupwizard/carrier/ActivationActivity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/StatusBarManager;

    .line 971
    .local v0, "statusBarManager":Landroid/app/StatusBarManager;
    if-eqz v0, :cond_1

    .line 972
    const/high16 v1, 0x1a40000

    invoke-virtual {v0, v1}, Landroid/app/StatusBarManager;->disable(I)V

    .line 982
    :goto_0
    const v1, 0x7f0e002e

    invoke-virtual {p0, v1}, Lcom/google/android/setupwizard/carrier/ActivationActivity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    iput-object v1, p0, Lcom/google/android/setupwizard/carrier/ActivationActivity;->mCountdownText:Landroid/widget/TextView;

    .line 983
    iget-object v1, p0, Lcom/google/android/setupwizard/carrier/ActivationActivity;->mCountdownText:Landroid/widget/TextView;

    invoke-virtual {v1, v3}, Landroid/widget/TextView;->setVisibility(I)V

    .line 984
    return-void

    .line 978
    :cond_1
    const-string v1, "ActivationActivity"

    const-string v2, "Unable to instantiate status bar manager"

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method private showSuccessScreen()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 808
    const-string v0, "ActivationActivity"

    const-string v1, "showSuccessScreen"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 811
    invoke-direct {p0, v2}, Lcom/google/android/setupwizard/carrier/ActivationActivity;->animateProgressBar(Z)V

    .line 812
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/google/android/setupwizard/carrier/ActivationActivity;->setNextButtonVisible(Z)V

    .line 813
    iget-boolean v0, p0, Lcom/google/android/setupwizard/carrier/ActivationActivity;->mGenericMode:Z

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/google/android/setupwizard/carrier/ActivationActivity;->mGenericOtaspDefinitelyNeeded:Z

    if-eqz v0, :cond_0

    const v0, 0x7f070073

    :goto_0
    invoke-direct {p0, v0}, Lcom/google/android/setupwizard/carrier/ActivationActivity;->setNextButtonLabel(I)V

    .line 815
    invoke-direct {p0, v2}, Lcom/google/android/setupwizard/carrier/ActivationActivity;->setSkipButtonVisible(Z)V

    .line 816
    invoke-direct {p0, v2}, Lcom/google/android/setupwizard/carrier/ActivationActivity;->setCancelButtonVisible(Z)V

    .line 817
    iget-object v0, p0, Lcom/google/android/setupwizard/carrier/ActivationActivity;->mActivationTitle:Landroid/widget/TextView;

    const v1, 0x7f070064

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    .line 818
    invoke-virtual {p0}, Lcom/google/android/setupwizard/carrier/ActivationActivity;->isFirstRun()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 819
    iget-object v1, p0, Lcom/google/android/setupwizard/carrier/ActivationActivity;->mActivationText:Landroid/widget/TextView;

    iget-boolean v0, p0, Lcom/google/android/setupwizard/carrier/ActivationActivity;->mGenericMode:Z

    if-eqz v0, :cond_1

    iget-boolean v0, p0, Lcom/google/android/setupwizard/carrier/ActivationActivity;->mGenericOtaspDefinitelyNeeded:Z

    if-eqz v0, :cond_1

    const v0, 0x7f070066

    :goto_1
    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setText(I)V

    .line 824
    :goto_2
    return-void

    .line 813
    :cond_0
    const v0, 0x7f070024

    goto :goto_0

    .line 819
    :cond_1
    const v0, 0x7f070065

    goto :goto_1

    .line 822
    :cond_2
    iget-object v0, p0, Lcom/google/android/setupwizard/carrier/ActivationActivity;->mActivationText:Landroid/widget/TextView;

    const v1, 0x7f070067

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    goto :goto_2
.end method

.method private startActivationProcess()V
    .locals 6

    .prologue
    .line 692
    iget-boolean v1, p0, Lcom/google/android/setupwizard/carrier/ActivationActivity;->mGenericMode:Z

    if-eqz v1, :cond_1

    const/4 v1, 0x2

    :goto_0
    const-string v2, "startActivationProcess()"

    invoke-direct {p0, v1, v2}, Lcom/google/android/setupwizard/carrier/ActivationActivity;->setState(ILjava/lang/String;)V

    .line 695
    iget-boolean v1, p0, Lcom/google/android/setupwizard/carrier/ActivationActivity;->mGenericMode:Z

    if-eqz v1, :cond_0

    .line 698
    invoke-direct {p0}, Lcom/google/android/setupwizard/carrier/ActivationActivity;->getGenericActivationTimeout()I

    move-result v0

    .line 699
    .local v0, "genericTimeout":I
    iget-object v1, p0, Lcom/google/android/setupwizard/carrier/ActivationActivity;->TAG:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "setting generic timeout in "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " ms"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 700
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v2

    int-to-long v4, v0

    add-long/2addr v2, v4

    iput-wide v2, p0, Lcom/google/android/setupwizard/carrier/ActivationActivity;->mGenericEndtime:J

    .line 701
    iget-object v1, p0, Lcom/google/android/setupwizard/carrier/ActivationActivity;->mHandler:Landroid/os/Handler;

    iget-object v2, p0, Lcom/google/android/setupwizard/carrier/ActivationActivity;->mTimeOutWaitingForGenericActivation:Ljava/lang/Runnable;

    int-to-long v4, v0

    invoke-virtual {v1, v2, v4, v5}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 711
    .end local v0    # "genericTimeout":I
    :cond_0
    invoke-direct {p0}, Lcom/google/android/setupwizard/carrier/ActivationActivity;->showActivationProcessScreen()V

    .line 714
    invoke-direct {p0}, Lcom/google/android/setupwizard/carrier/ActivationActivity;->onStateChanges()V

    .line 715
    return-void

    .line 692
    :cond_1
    const/4 v1, 0x1

    goto :goto_0
.end method

.method private updateNavBarButton()V
    .locals 3

    .prologue
    const/4 v2, 0x1

    .line 1096
    iget-object v0, p0, Lcom/google/android/setupwizard/carrier/ActivationActivity;->mNavigationBar:Lcom/android/setupwizard/navigationbar/SetupWizardNavBar;

    if-nez v0, :cond_0

    .line 1109
    :goto_0
    return-void

    .line 1099
    :cond_0
    iget-boolean v0, p0, Lcom/google/android/setupwizard/carrier/ActivationActivity;->mNextEnabled:Z

    if-eqz v0, :cond_1

    .line 1100
    iget-object v0, p0, Lcom/google/android/setupwizard/carrier/ActivationActivity;->mNavigationBar:Lcom/android/setupwizard/navigationbar/SetupWizardNavBar;

    invoke-virtual {v0}, Lcom/android/setupwizard/navigationbar/SetupWizardNavBar;->getNextButton()Landroid/widget/Button;

    move-result-object v0

    iget v1, p0, Lcom/google/android/setupwizard/carrier/ActivationActivity;->mNextLabel:I

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setText(I)V

    .line 1101
    iget-object v0, p0, Lcom/google/android/setupwizard/carrier/ActivationActivity;->mNavigationBar:Lcom/android/setupwizard/navigationbar/SetupWizardNavBar;

    invoke-virtual {v0}, Lcom/android/setupwizard/navigationbar/SetupWizardNavBar;->getNextButton()Landroid/widget/Button;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/widget/Button;->setEnabled(Z)V

    goto :goto_0

    .line 1102
    :cond_1
    iget-boolean v0, p0, Lcom/google/android/setupwizard/carrier/ActivationActivity;->mCancelEnabled:Z

    if-eqz v0, :cond_2

    .line 1103
    iget-object v0, p0, Lcom/google/android/setupwizard/carrier/ActivationActivity;->mNavigationBar:Lcom/android/setupwizard/navigationbar/SetupWizardNavBar;

    invoke-virtual {v0}, Lcom/android/setupwizard/navigationbar/SetupWizardNavBar;->getNextButton()Landroid/widget/Button;

    move-result-object v0

    iget v1, p0, Lcom/google/android/setupwizard/carrier/ActivationActivity;->mCancelLabel:I

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setText(I)V

    .line 1104
    iget-object v0, p0, Lcom/google/android/setupwizard/carrier/ActivationActivity;->mNavigationBar:Lcom/android/setupwizard/navigationbar/SetupWizardNavBar;

    invoke-virtual {v0}, Lcom/android/setupwizard/navigationbar/SetupWizardNavBar;->getNextButton()Landroid/widget/Button;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/widget/Button;->setEnabled(Z)V

    goto :goto_0

    .line 1106
    :cond_2
    iget-object v0, p0, Lcom/google/android/setupwizard/carrier/ActivationActivity;->mNavigationBar:Lcom/android/setupwizard/navigationbar/SetupWizardNavBar;

    invoke-virtual {v0}, Lcom/android/setupwizard/navigationbar/SetupWizardNavBar;->getNextButton()Landroid/widget/Button;

    move-result-object v0

    iget v1, p0, Lcom/google/android/setupwizard/carrier/ActivationActivity;->mNextLabel:I

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setText(I)V

    .line 1107
    iget-object v0, p0, Lcom/google/android/setupwizard/carrier/ActivationActivity;->mNavigationBar:Lcom/android/setupwizard/navigationbar/SetupWizardNavBar;

    invoke-virtual {v0}, Lcom/android/setupwizard/navigationbar/SetupWizardNavBar;->getNextButton()Landroid/widget/Button;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setEnabled(Z)V

    goto :goto_0
.end method

.method private updateSignalStrength()V
    .locals 6

    .prologue
    .line 1013
    :try_start_0
    iget-object v1, p0, Lcom/google/android/setupwizard/carrier/ActivationActivity;->mSignalStrength:Landroid/telephony/SignalStrength;

    iget-object v2, p0, Lcom/google/android/setupwizard/carrier/ActivationActivity;->mServiceState:Landroid/telephony/ServiceState;

    const/4 v3, 0x0

    invoke-static {v1, v2, v3}, Lcom/google/android/setupwizard/carrier/ActivationActivity;->calculateSignalStrength(Landroid/telephony/SignalStrength;Landroid/telephony/ServiceState;I)I

    move-result v0

    .line 1016
    .local v0, "signalStrength":I
    iget-object v1, p0, Lcom/google/android/setupwizard/carrier/ActivationActivity;->mNetworkBars:Landroid/widget/ImageView;

    iget-object v2, p0, Lcom/google/android/setupwizard/carrier/ActivationActivity;->mSignalStrengthImages:Landroid/content/res/TypedArray;

    invoke-virtual {v2, v0}, Landroid/content/res/TypedArray;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 1017
    iget-object v1, p0, Lcom/google/android/setupwizard/carrier/ActivationActivity;->mNetworkBars:Landroid/widget/ImageView;

    const v2, 0x7f070058

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v3, v4

    invoke-virtual {p0, v2, v3}, Lcom/google/android/setupwizard/carrier/ActivationActivity;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/ImageView;->setContentDescription(Ljava/lang/CharSequence;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 1021
    .end local v0    # "signalStrength":I
    :goto_0
    return-void

    .line 1019
    :catch_0
    move-exception v1

    goto :goto_0
.end method


# virtual methods
.method protected onActivityResult(IILandroid/content/Intent;)V
    .locals 3
    .param p1, "requestCode"    # I
    .param p2, "resultCode"    # I
    .param p3, "data"    # Landroid/content/Intent;

    .prologue
    const/16 v2, 0x4e21

    .line 676
    if-ne p1, v2, :cond_1

    const/4 v0, 0x2

    if-eq p2, v0, :cond_1

    iget v0, p0, Lcom/google/android/setupwizard/carrier/ActivationActivity;->mState:I

    const/4 v1, 0x7

    if-eq v0, v1, :cond_1

    .line 679
    invoke-direct {p0}, Lcom/google/android/setupwizard/carrier/ActivationActivity;->onFailure()V

    .line 683
    :cond_0
    :goto_0
    return-void

    .line 680
    :cond_1
    if-eq p1, v2, :cond_0

    .line 681
    invoke-super {p0, p1, p2, p3}, Lcom/google/android/setupwizard/BaseActivity;->onActivityResult(IILandroid/content/Intent;)V

    goto :goto_0
.end method

.method public onClick(Landroid/view/View;)V
    .locals 2
    .param p1, "view"    # Landroid/view/View;

    .prologue
    .line 271
    iget-object v0, p0, Lcom/google/android/setupwizard/carrier/ActivationActivity;->mNextButton:Landroid/widget/Button;

    if-ne p1, v0, :cond_1

    .line 272
    invoke-direct {p0}, Lcom/google/android/setupwizard/carrier/ActivationActivity;->onNextButtonClick()V

    .line 286
    :cond_0
    :goto_0
    return-void

    .line 273
    :cond_1
    iget v0, p0, Lcom/google/android/setupwizard/carrier/ActivationActivity;->mState:I

    const/4 v1, 0x7

    if-eq v0, v1, :cond_0

    .line 274
    iget-object v0, p0, Lcom/google/android/setupwizard/carrier/ActivationActivity;->mSkipButton:Landroid/widget/Button;

    if-ne p1, v0, :cond_2

    .line 275
    const-string v0, "ActivationActivity"

    const-string v1, "onClickView: Activation is not needed, skipping"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 276
    invoke-direct {p0}, Lcom/google/android/setupwizard/carrier/ActivationActivity;->onActualSkip()V

    goto :goto_0

    .line 277
    :cond_2
    iget-object v0, p0, Lcom/google/android/setupwizard/carrier/ActivationActivity;->mCancelButton:Landroid/widget/Button;

    if-ne p1, v0, :cond_3

    .line 278
    invoke-direct {p0}, Lcom/google/android/setupwizard/carrier/ActivationActivity;->onCancelRequested()V

    goto :goto_0

    .line 279
    :cond_3
    iget-object v0, p0, Lcom/google/android/setupwizard/carrier/ActivationActivity;->mTryAgainButton:Landroid/widget/Button;

    if-ne p1, v0, :cond_4

    .line 280
    const-string v0, "ActivationActivity"

    const-string v1, "onClickView: Try again button"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 281
    invoke-direct {p0}, Lcom/google/android/setupwizard/carrier/ActivationActivity;->startActivationProcess()V

    goto :goto_0

    .line 283
    :cond_4
    const-string v0, "ActivationActivity"

    const-string v1, "onClickView: ignorning"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 12
    .param p1, "icicle"    # Landroid/os/Bundle;

    .prologue
    const/4 v11, 0x1

    const-wide/16 v4, 0x0

    const/4 v10, 0x0

    .line 404
    invoke-super {p0, p1}, Lcom/google/android/setupwizard/BaseActivity;->onCreate(Landroid/os/Bundle;)V

    .line 411
    if-nez p1, :cond_0

    invoke-virtual {p0}, Lcom/google/android/setupwizard/carrier/ActivationActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v6

    const v7, 0x7f0c0001

    invoke-virtual {v6, v7}, Landroid/content/res/Resources;->getBoolean(I)Z

    move-result v6

    if-eqz v6, :cond_0

    .line 412
    iget-object v4, p0, Lcom/google/android/setupwizard/carrier/ActivationActivity;->TAG:Ljava/lang/String;

    const-string v5, "CDMA always: launching CDMA_PROVISIONING from ActivationActivity"

    invoke-static {v4, v5}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 413
    new-instance v1, Landroid/content/Intent;

    const-string v4, "com.android.phone.PERFORM_CDMA_PROVISIONING"

    invoke-direct {v1, v4}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 414
    .local v1, "newIntent":Landroid/content/Intent;
    const/high16 v4, 0x10000000

    invoke-virtual {v1, v4}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    .line 415
    invoke-virtual {p0, v1}, Lcom/google/android/setupwizard/carrier/ActivationActivity;->startActivity(Landroid/content/Intent;)V

    .line 416
    invoke-virtual {p0}, Lcom/google/android/setupwizard/carrier/ActivationActivity;->finishAction()V

    .line 584
    .end local v1    # "newIntent":Landroid/content/Intent;
    :goto_0
    return-void

    .line 423
    :cond_0
    invoke-virtual {p0}, Lcom/google/android/setupwizard/carrier/ActivationActivity;->getDecorView()Landroid/view/View;

    move-result-object v6

    const/high16 v7, 0x1a40000

    invoke-virtual {v6, v7}, Landroid/view/View;->setSystemUiVisibility(I)V

    .line 430
    const-string v6, "ActivationActivity"

    const-string v7, "onCreate: E"

    invoke-static {v6, v7}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 432
    invoke-static {}, Lcom/google/android/setupwizard/carrier/PhoneMonitor;->isLTE()Z

    move-result v6

    iput-boolean v6, p0, Lcom/google/android/setupwizard/carrier/ActivationActivity;->mGenericMode:Z

    .line 433
    const-string v6, "onCreate()"

    invoke-direct {p0, v10, v6}, Lcom/google/android/setupwizard/carrier/ActivationActivity;->setState(ILjava/lang/String;)V

    .line 445
    if-nez p1, :cond_4

    .line 446
    invoke-virtual {p0}, Lcom/google/android/setupwizard/carrier/ActivationActivity;->getIntent()Landroid/content/Intent;

    move-result-object v4

    const-string v5, "exitToHome"

    invoke-virtual {v4, v5, v10}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v4

    iput-boolean v4, p0, Lcom/google/android/setupwizard/carrier/ActivationActivity;->mDoExitToHome:Z

    .line 447
    invoke-virtual {p0}, Lcom/google/android/setupwizard/carrier/ActivationActivity;->getIntent()Landroid/content/Intent;

    move-result-object v4

    const-string v5, "com.android.phone.VOICELESS_PROVISIONING_OFFER_DONTSHOW"

    invoke-virtual {v4, v5, v10}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v4

    iput-boolean v4, p0, Lcom/google/android/setupwizard/carrier/ActivationActivity;->mAllowDontShowMode:Z

    .line 451
    const-string v4, "ActivationActivity"

    const-string v5, "onCreate: mDoExitToHome: %b, mAllowDontShowMode: %b"

    const/4 v6, 0x2

    new-array v6, v6, [Ljava/lang/Object;

    iget-boolean v7, p0, Lcom/google/android/setupwizard/carrier/ActivationActivity;->mDoExitToHome:Z

    invoke-static {v7}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v7

    aput-object v7, v6, v10

    iget-boolean v7, p0, Lcom/google/android/setupwizard/carrier/ActivationActivity;->mAllowDontShowMode:Z

    invoke-static {v7}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v7

    aput-object v7, v6, v11

    invoke-static {v5, v6}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 481
    :cond_1
    :goto_1
    invoke-static {p0}, Landroid/preference/PreferenceManager;->getDefaultSharedPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v4

    iput-object v4, p0, Lcom/google/android/setupwizard/carrier/ActivationActivity;->mSharedPrefs:Landroid/content/SharedPreferences;

    .line 482
    iget-boolean v4, p0, Lcom/google/android/setupwizard/carrier/ActivationActivity;->mAllowDontShowMode:Z

    if-eqz v4, :cond_2

    iget-object v4, p0, Lcom/google/android/setupwizard/carrier/ActivationActivity;->mSharedPrefs:Landroid/content/SharedPreferences;

    const-string v5, "dont_show_again"

    invoke-interface {v4, v5, v10}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v4

    if-eqz v4, :cond_2

    .line 483
    iget-object v4, p0, Lcom/google/android/setupwizard/carrier/ActivationActivity;->TAG:Ljava/lang/String;

    const-string v5, "skipping due to don\'t show mode"

    invoke-static {v4, v5}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 484
    invoke-direct {p0, v11}, Lcom/google/android/setupwizard/carrier/ActivationActivity;->goNextOrFinish(I)V

    .line 487
    :cond_2
    invoke-virtual {p0}, Lcom/google/android/setupwizard/carrier/ActivationActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const/high16 v5, 0x7f0b0000

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->obtainTypedArray(I)Landroid/content/res/TypedArray;

    move-result-object v4

    iput-object v4, p0, Lcom/google/android/setupwizard/carrier/ActivationActivity;->mSignalStrengthImages:Landroid/content/res/TypedArray;

    .line 489
    invoke-virtual {p0}, Lcom/google/android/setupwizard/carrier/ActivationActivity;->isFirstRun()Z

    move-result v4

    if-eqz v4, :cond_8

    .line 490
    const v4, 0x7f030001

    const v5, 0x7f030023

    invoke-virtual {p0, v4, v5}, Lcom/google/android/setupwizard/carrier/ActivationActivity;->setTemplateContent(II)V

    .line 491
    const v4, 0x7f070054

    invoke-virtual {p0, v4}, Lcom/google/android/setupwizard/carrier/ActivationActivity;->setHeaderText(I)V

    .line 492
    const v4, 0x7f0e005d

    invoke-virtual {p0, v4}, Lcom/google/android/setupwizard/carrier/ActivationActivity;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/TextView;

    iput-object v4, p0, Lcom/google/android/setupwizard/carrier/ActivationActivity;->mActivationTitle:Landroid/widget/TextView;

    .line 493
    const v4, 0x7f0e0030

    invoke-virtual {p0, v4}, Lcom/google/android/setupwizard/carrier/ActivationActivity;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/Button;

    iput-object v4, p0, Lcom/google/android/setupwizard/carrier/ActivationActivity;->mTryAgainButton:Landroid/widget/Button;

    .line 494
    iget-object v4, p0, Lcom/google/android/setupwizard/carrier/ActivationActivity;->mTryAgainButton:Landroid/widget/Button;

    invoke-virtual {v4, p0}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 515
    :goto_2
    const v4, 0x7f070057

    invoke-virtual {p0, v4}, Lcom/google/android/setupwizard/carrier/ActivationActivity;->getString(I)Ljava/lang/String;

    move-result-object v4

    iput-object v4, p0, Lcom/google/android/setupwizard/carrier/ActivationActivity;->mNetworkName:Ljava/lang/CharSequence;

    .line 517
    const v4, 0x7f0e002c

    invoke-virtual {p0, v4}, Lcom/google/android/setupwizard/carrier/ActivationActivity;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/TextView;

    iput-object v4, p0, Lcom/google/android/setupwizard/carrier/ActivationActivity;->mActivationText:Landroid/widget/TextView;

    .line 518
    const v4, 0x7f0e002a

    invoke-virtual {p0, v4}, Lcom/google/android/setupwizard/carrier/ActivationActivity;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/ProgressBar;

    iput-object v4, p0, Lcom/google/android/setupwizard/carrier/ActivationActivity;->mProgressBar:Landroid/widget/ProgressBar;

    .line 521
    new-instance v4, Landroid/content/IntentFilter;

    invoke-direct {v4}, Landroid/content/IntentFilter;-><init>()V

    iput-object v4, p0, Lcom/google/android/setupwizard/carrier/ActivationActivity;->mFilter:Landroid/content/IntentFilter;

    .line 522
    iget-object v4, p0, Lcom/google/android/setupwizard/carrier/ActivationActivity;->mFilter:Landroid/content/IntentFilter;

    const-string v5, "android.provider.Telephony.SPN_STRINGS_UPDATED"

    invoke-virtual {v4, v5}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 523
    new-instance v4, Lcom/google/android/setupwizard/carrier/ActivationActivity$5;

    invoke-direct {v4, p0}, Lcom/google/android/setupwizard/carrier/ActivationActivity$5;-><init>(Lcom/google/android/setupwizard/carrier/ActivationActivity;)V

    iput-object v4, p0, Lcom/google/android/setupwizard/carrier/ActivationActivity;->mReceiver:Landroid/content/BroadcastReceiver;

    .line 535
    iget-object v4, p0, Lcom/google/android/setupwizard/carrier/ActivationActivity;->mPhoneStateListener:Landroid/telephony/PhoneStateListener;

    invoke-static {v4}, Lcom/google/android/setupwizard/carrier/PhoneMonitor;->addListener(Landroid/telephony/PhoneStateListener;)V

    .line 537
    if-nez p1, :cond_a

    .line 538
    invoke-virtual {p0}, Lcom/google/android/setupwizard/carrier/ActivationActivity;->getIntent()Landroid/content/Intent;

    move-result-object v4

    const-string v5, "autoStart"

    invoke-virtual {v4, v5, v10}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v0

    .line 539
    .local v0, "autoStart":Z
    invoke-virtual {p0}, Lcom/google/android/setupwizard/carrier/ActivationActivity;->isFirstRun()Z

    move-result v4

    if-nez v4, :cond_3

    if-nez v0, :cond_3

    iget-boolean v4, p0, Lcom/google/android/setupwizard/carrier/ActivationActivity;->mGenericMode:Z

    if-eqz v4, :cond_9

    .line 540
    :cond_3
    invoke-direct {p0}, Lcom/google/android/setupwizard/carrier/ActivationActivity;->startActivationProcess()V

    .line 583
    .end local v0    # "autoStart":Z
    :goto_3
    const-string v4, "ActivationActivity"

    const-string v5, "onCreate: X"

    invoke-static {v4, v5}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0

    .line 456
    :cond_4
    const-string v6, "exitToHome"

    invoke-virtual {p1, v6}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v6

    iput-boolean v6, p0, Lcom/google/android/setupwizard/carrier/ActivationActivity;->mDoExitToHome:Z

    .line 457
    const-string v6, "com.android.phone.VOICELESS_PROVISIONING_OFFER_DONTSHOW"

    invoke-virtual {p1, v6}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v6

    iput-boolean v6, p0, Lcom/google/android/setupwizard/carrier/ActivationActivity;->mAllowDontShowMode:Z

    .line 459
    const-string v6, "prevState"

    invoke-virtual {p1, v6}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v6

    const-string v7, "onCreate() icicle"

    invoke-direct {p0, v6, v7}, Lcom/google/android/setupwizard/carrier/ActivationActivity;->setState(ILjava/lang/String;)V

    .line 460
    const-string v6, "isGenMode"

    invoke-virtual {p1, v6}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v6

    iput-boolean v6, p0, Lcom/google/android/setupwizard/carrier/ActivationActivity;->mGenericMode:Z

    .line 461
    const-string v6, "cancelEndtime"

    invoke-virtual {p1, v6}, Landroid/os/Bundle;->getLong(Ljava/lang/String;)J

    move-result-wide v6

    iput-wide v6, p0, Lcom/google/android/setupwizard/carrier/ActivationActivity;->mCancelEndtime:J

    .line 462
    const-string v6, "genericEndtime"

    invoke-virtual {p1, v6}, Landroid/os/Bundle;->getLong(Ljava/lang/String;)J

    move-result-wide v6

    iput-wide v6, p0, Lcom/google/android/setupwizard/carrier/ActivationActivity;->mGenericEndtime:J

    .line 463
    const-string v6, "genericActivationNeeded"

    invoke-virtual {p1, v6}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v6

    iput-boolean v6, p0, Lcom/google/android/setupwizard/carrier/ActivationActivity;->mGenericOtaspDefinitelyNeeded:Z

    .line 465
    iget-wide v6, p0, Lcom/google/android/setupwizard/carrier/ActivationActivity;->mCancelEndtime:J

    cmp-long v6, v6, v4

    if-lez v6, :cond_5

    .line 466
    iget-wide v6, p0, Lcom/google/android/setupwizard/carrier/ActivationActivity;->mCancelEndtime:J

    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v8

    sub-long v2, v6, v8

    .line 467
    .local v2, "newTimeout":J
    iget-object v6, p0, Lcom/google/android/setupwizard/carrier/ActivationActivity;->mHandler:Landroid/os/Handler;

    iget-object v7, p0, Lcom/google/android/setupwizard/carrier/ActivationActivity;->mTimeOutWaitingForCancel:Ljava/lang/Runnable;

    cmp-long v8, v2, v4

    if-lez v8, :cond_7

    .end local v2    # "newTimeout":J
    :goto_4
    invoke-virtual {v6, v7, v2, v3}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 469
    :cond_5
    iget-wide v6, p0, Lcom/google/android/setupwizard/carrier/ActivationActivity;->mGenericEndtime:J

    cmp-long v6, v6, v4

    if-lez v6, :cond_1

    .line 470
    iget-wide v6, p0, Lcom/google/android/setupwizard/carrier/ActivationActivity;->mGenericEndtime:J

    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v8

    sub-long v2, v6, v8

    .line 471
    .restart local v2    # "newTimeout":J
    cmp-long v4, v2, v4

    if-gez v4, :cond_6

    .line 472
    const-wide/16 v2, 0x0

    .line 474
    :cond_6
    iget-object v4, p0, Lcom/google/android/setupwizard/carrier/ActivationActivity;->TAG:Ljava/lang/String;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "setting generic timeout in "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, " ms"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 475
    iget-object v4, p0, Lcom/google/android/setupwizard/carrier/ActivationActivity;->mHandler:Landroid/os/Handler;

    iget-object v5, p0, Lcom/google/android/setupwizard/carrier/ActivationActivity;->mTimeOutWaitingForGenericActivation:Ljava/lang/Runnable;

    invoke-virtual {v4, v5, v2, v3}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    goto/16 :goto_1

    :cond_7
    move-wide v2, v4

    .line 467
    goto :goto_4

    .line 496
    .end local v2    # "newTimeout":J
    :cond_8
    const v4, 0x7f030002

    invoke-virtual {p0, v4}, Lcom/google/android/setupwizard/carrier/ActivationActivity;->setContentView(I)V

    .line 498
    const v4, 0x7f0e0031

    invoke-virtual {p0, v4}, Lcom/google/android/setupwizard/carrier/ActivationActivity;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/TextView;

    iput-object v4, p0, Lcom/google/android/setupwizard/carrier/ActivationActivity;->mActivationTitle:Landroid/widget/TextView;

    .line 500
    const v4, 0x7f0e0039

    invoke-virtual {p0, v4}, Lcom/google/android/setupwizard/carrier/ActivationActivity;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/Button;

    iput-object v4, p0, Lcom/google/android/setupwizard/carrier/ActivationActivity;->mNextButton:Landroid/widget/Button;

    .line 501
    iget-object v4, p0, Lcom/google/android/setupwizard/carrier/ActivationActivity;->mNextButton:Landroid/widget/Button;

    invoke-virtual {v4, p0}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 503
    const v4, 0x7f0e0037

    invoke-virtual {p0, v4}, Lcom/google/android/setupwizard/carrier/ActivationActivity;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/Button;

    iput-object v4, p0, Lcom/google/android/setupwizard/carrier/ActivationActivity;->mCancelButton:Landroid/widget/Button;

    .line 504
    iget-object v4, p0, Lcom/google/android/setupwizard/carrier/ActivationActivity;->mCancelButton:Landroid/widget/Button;

    invoke-virtual {v4, p0}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 506
    const v4, 0x7f0e0038

    invoke-virtual {p0, v4}, Lcom/google/android/setupwizard/carrier/ActivationActivity;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/Button;

    iput-object v4, p0, Lcom/google/android/setupwizard/carrier/ActivationActivity;->mSkipButton:Landroid/widget/Button;

    .line 507
    iget-object v4, p0, Lcom/google/android/setupwizard/carrier/ActivationActivity;->mSkipButton:Landroid/widget/Button;

    invoke-virtual {v4, p0}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 509
    const v4, 0x7f0e0033

    invoke-virtual {p0, v4}, Lcom/google/android/setupwizard/carrier/ActivationActivity;->findViewById(I)Landroid/view/View;

    move-result-object v4

    iput-object v4, p0, Lcom/google/android/setupwizard/carrier/ActivationActivity;->mTopDivider:Landroid/view/View;

    .line 511
    const v4, 0x7f0e0034

    invoke-virtual {p0, v4}, Lcom/google/android/setupwizard/carrier/ActivationActivity;->findViewById(I)Landroid/view/View;

    move-result-object v4

    iput-object v4, p0, Lcom/google/android/setupwizard/carrier/ActivationActivity;->mNetworkNotifier:Landroid/view/View;

    .line 512
    const v4, 0x7f0e0035

    invoke-virtual {p0, v4}, Lcom/google/android/setupwizard/carrier/ActivationActivity;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/ImageView;

    iput-object v4, p0, Lcom/google/android/setupwizard/carrier/ActivationActivity;->mNetworkBars:Landroid/widget/ImageView;

    goto/16 :goto_2

    .line 542
    .restart local v0    # "autoStart":Z
    :cond_9
    invoke-direct {p0}, Lcom/google/android/setupwizard/carrier/ActivationActivity;->initStandaloneView()V

    goto/16 :goto_3

    .line 546
    .end local v0    # "autoStart":Z
    :cond_a
    const-string v4, "ActivationActivity"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "restoring UI state to "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget v6, p0, Lcom/google/android/setupwizard/carrier/ActivationActivity;->mState:I

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 547
    iget v4, p0, Lcom/google/android/setupwizard/carrier/ActivationActivity;->mState:I

    packed-switch v4, :pswitch_data_0

    .line 577
    const-string v4, "ActivationActivity"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "unknown state: "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget v6, p0, Lcom/google/android/setupwizard/carrier/ActivationActivity;->mState:I

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 579
    invoke-direct {p0}, Lcom/google/android/setupwizard/carrier/ActivationActivity;->initStandaloneView()V

    goto/16 :goto_3

    .line 549
    :pswitch_0
    invoke-virtual {p0}, Lcom/google/android/setupwizard/carrier/ActivationActivity;->getIntent()Landroid/content/Intent;

    move-result-object v4

    const-string v5, "autoStart"

    invoke-virtual {v4, v5, v10}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v0

    .line 551
    .restart local v0    # "autoStart":Z
    invoke-virtual {p0}, Lcom/google/android/setupwizard/carrier/ActivationActivity;->isFirstRun()Z

    move-result v4

    if-nez v4, :cond_b

    if-eqz v0, :cond_c

    .line 552
    :cond_b
    invoke-direct {p0}, Lcom/google/android/setupwizard/carrier/ActivationActivity;->startActivationProcess()V

    goto/16 :goto_3

    .line 554
    :cond_c
    invoke-direct {p0}, Lcom/google/android/setupwizard/carrier/ActivationActivity;->initStandaloneView()V

    goto/16 :goto_3

    .line 559
    .end local v0    # "autoStart":Z
    :pswitch_1
    invoke-direct {p0}, Lcom/google/android/setupwizard/carrier/ActivationActivity;->showActivationProcessScreen()V

    goto/16 :goto_3

    .line 562
    :pswitch_2
    invoke-direct {p0}, Lcom/google/android/setupwizard/carrier/ActivationActivity;->showCancelRequestedScreen()V

    goto/16 :goto_3

    .line 565
    :pswitch_3
    invoke-direct {p0}, Lcom/google/android/setupwizard/carrier/ActivationActivity;->showCancelCompletedScreen()V

    goto/16 :goto_3

    .line 568
    :pswitch_4
    invoke-direct {p0}, Lcom/google/android/setupwizard/carrier/ActivationActivity;->onSuccess()V

    goto/16 :goto_3

    .line 571
    :pswitch_5
    invoke-direct {p0}, Lcom/google/android/setupwizard/carrier/ActivationActivity;->showFailureScreen()V

    goto/16 :goto_3

    .line 574
    :pswitch_6
    invoke-direct {p0}, Lcom/google/android/setupwizard/carrier/ActivationActivity;->showShuttingDownScreen()V

    goto/16 :goto_3

    .line 547
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
    .end packed-switch
.end method

.method protected onDestroy()V
    .locals 2

    .prologue
    .line 654
    invoke-super {p0}, Lcom/google/android/setupwizard/BaseActivity;->onDestroy()V

    .line 655
    iget-object v0, p0, Lcom/google/android/setupwizard/carrier/ActivationActivity;->mHandler:Landroid/os/Handler;

    iget-object v1, p0, Lcom/google/android/setupwizard/carrier/ActivationActivity;->mTimeOutWaitingForGenericActivation:Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    .line 656
    iget-object v0, p0, Lcom/google/android/setupwizard/carrier/ActivationActivity;->mHandler:Landroid/os/Handler;

    iget-object v1, p0, Lcom/google/android/setupwizard/carrier/ActivationActivity;->mTimeOutWaitingForCancel:Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    .line 658
    iget-object v0, p0, Lcom/google/android/setupwizard/carrier/ActivationActivity;->mPhoneStateListener:Landroid/telephony/PhoneStateListener;

    invoke-static {v0}, Lcom/google/android/setupwizard/carrier/PhoneMonitor;->removeListener(Landroid/telephony/PhoneStateListener;)V

    .line 659
    return-void
.end method

.method protected onNewIntent(Landroid/content/Intent;)V
    .locals 5
    .param p1, "intent"    # Landroid/content/Intent;

    .prologue
    const/4 v4, 0x4

    .line 591
    invoke-super {p0, p1}, Lcom/google/android/setupwizard/BaseActivity;->onNewIntent(Landroid/content/Intent;)V

    .line 593
    const-string v1, "ActivationActivity"

    const-string v2, "onNewIntent: E"

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 594
    const-string v1, "otasp_result_code"

    invoke-virtual {p1, v1}, Landroid/content/Intent;->hasExtra(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_4

    iget v1, p0, Lcom/google/android/setupwizard/carrier/ActivationActivity;->mState:I

    const/4 v2, 0x7

    if-eq v1, v2, :cond_4

    .line 596
    const-string v1, "otasp_result_code"

    const/4 v2, 0x0

    invoke-virtual {p1, v1, v2}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v0

    .line 598
    .local v0, "resultCode":I
    const-string v1, "ActivationActivity"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Got OTASP result: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 600
    iget v1, p0, Lcom/google/android/setupwizard/carrier/ActivationActivity;->mState:I

    const/4 v2, 0x3

    if-eq v1, v2, :cond_0

    iget v1, p0, Lcom/google/android/setupwizard/carrier/ActivationActivity;->mState:I

    if-ne v1, v4, :cond_3

    .line 601
    :cond_0
    const/4 v1, 0x1

    if-eq v0, v1, :cond_1

    .line 602
    const-string v1, "ActivationActivity"

    const-string v2, "OTASP_USER_SKIPPED not returned."

    invoke-static {v1, v2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 604
    :cond_1
    iget v1, p0, Lcom/google/android/setupwizard/carrier/ActivationActivity;->mState:I

    if-ne v1, v4, :cond_2

    .line 605
    const-string v1, "ActivationActivity"

    const-string v2, "mState is already STATE_CANCEL_COMPLETED"

    invoke-static {v1, v2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 607
    :cond_2
    iget-object v1, p0, Lcom/google/android/setupwizard/carrier/ActivationActivity;->mHandler:Landroid/os/Handler;

    iget-object v2, p0, Lcom/google/android/setupwizard/carrier/ActivationActivity;->mTimeOutWaitingForCancel:Ljava/lang/Runnable;

    invoke-virtual {v1, v2}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    .line 608
    invoke-direct {p0}, Lcom/google/android/setupwizard/carrier/ActivationActivity;->onCancelCompleted()V

    .line 636
    .end local v0    # "resultCode":I
    :goto_0
    return-void

    .line 612
    .restart local v0    # "resultCode":I
    :cond_3
    packed-switch v0, :pswitch_data_0

    .line 631
    const-string v1, "ActivationActivity"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Unknown otasp activation result"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 635
    .end local v0    # "resultCode":I
    :cond_4
    :goto_1
    const-string v1, "ActivationActivity"

    const-string v2, "onNewIntent: X"

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 614
    .restart local v0    # "resultCode":I
    :pswitch_0
    invoke-direct {p0}, Lcom/google/android/setupwizard/carrier/ActivationActivity;->onActualSkip()V

    goto :goto_1

    .line 617
    :pswitch_1
    invoke-virtual {p0}, Lcom/google/android/setupwizard/carrier/ActivationActivity;->isPessimisticMode()Z

    move-result v1

    if-eqz v1, :cond_5

    .line 618
    const-string v1, "ActivationActivity"

    const-string v2, "Be pessimistic. Show failure screen anyway."

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 619
    invoke-direct {p0}, Lcom/google/android/setupwizard/carrier/ActivationActivity;->onFailure()V

    goto :goto_1

    .line 621
    :cond_5
    invoke-direct {p0}, Lcom/google/android/setupwizard/carrier/ActivationActivity;->onSuccess()V

    goto :goto_1

    .line 625
    :pswitch_2
    invoke-direct {p0}, Lcom/google/android/setupwizard/carrier/ActivationActivity;->onFailure()V

    goto :goto_1

    .line 628
    :pswitch_3
    invoke-direct {p0}, Lcom/google/android/setupwizard/carrier/ActivationActivity;->onSpcRetriesFailure()V

    goto :goto_1

    .line 612
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method public onPause()V
    .locals 2

    .prologue
    .line 640
    invoke-super {p0}, Lcom/google/android/setupwizard/BaseActivity;->onPause()V

    .line 641
    const-string v0, "ActivationActivity"

    const-string v1, "onPause"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 642
    iget-object v0, p0, Lcom/google/android/setupwizard/carrier/ActivationActivity;->mReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {p0, v0}, Lcom/google/android/setupwizard/carrier/ActivationActivity;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    .line 643
    return-void
.end method

.method public onResume()V
    .locals 2

    .prologue
    .line 647
    invoke-super {p0}, Lcom/google/android/setupwizard/BaseActivity;->onResume()V

    .line 648
    const-string v0, "ActivationActivity"

    const-string v1, "onResume"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 649
    iget-object v0, p0, Lcom/google/android/setupwizard/carrier/ActivationActivity;->mReceiver:Landroid/content/BroadcastReceiver;

    iget-object v1, p0, Lcom/google/android/setupwizard/carrier/ActivationActivity;->mFilter:Landroid/content/IntentFilter;

    invoke-virtual {p0, v0, v1}, Lcom/google/android/setupwizard/carrier/ActivationActivity;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    .line 650
    return-void
.end method

.method protected onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 4
    .param p1, "outState"    # Landroid/os/Bundle;

    .prologue
    .line 663
    const-string v0, "ActivationActivity"

    const-string v1, "onSaveInstanceState()"

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 664
    invoke-super {p0, p1}, Lcom/google/android/setupwizard/BaseActivity;->onSaveInstanceState(Landroid/os/Bundle;)V

    .line 665
    const-string v0, "exitToHome"

    iget-boolean v1, p0, Lcom/google/android/setupwizard/carrier/ActivationActivity;->mDoExitToHome:Z

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 666
    const-string v0, "com.android.phone.VOICELESS_PROVISIONING_OFFER_DONTSHOW"

    iget-boolean v1, p0, Lcom/google/android/setupwizard/carrier/ActivationActivity;->mAllowDontShowMode:Z

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 667
    const-string v0, "prevState"

    iget v1, p0, Lcom/google/android/setupwizard/carrier/ActivationActivity;->mState:I

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 668
    const-string v0, "isGenMode"

    iget-boolean v1, p0, Lcom/google/android/setupwizard/carrier/ActivationActivity;->mGenericMode:Z

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 669
    const-string v0, "cancelEndtime"

    iget-wide v2, p0, Lcom/google/android/setupwizard/carrier/ActivationActivity;->mCancelEndtime:J

    invoke-virtual {p1, v0, v2, v3}, Landroid/os/Bundle;->putLong(Ljava/lang/String;J)V

    .line 670
    const-string v0, "genericEndtime"

    iget-wide v2, p0, Lcom/google/android/setupwizard/carrier/ActivationActivity;->mGenericEndtime:J

    invoke-virtual {p1, v0, v2, v3}, Landroid/os/Bundle;->putLong(Ljava/lang/String;J)V

    .line 671
    const-string v0, "genericActivationNeeded"

    iget-boolean v1, p0, Lcom/google/android/setupwizard/carrier/ActivationActivity;->mGenericOtaspDefinitelyNeeded:Z

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 672
    return-void
.end method

.method protected start()V
    .locals 1

    .prologue
    .line 308
    iget-boolean v0, p0, Lcom/google/android/setupwizard/carrier/ActivationActivity;->mNextEnabled:Z

    if-eqz v0, :cond_1

    .line 309
    invoke-direct {p0}, Lcom/google/android/setupwizard/carrier/ActivationActivity;->onNextButtonClick()V

    .line 313
    :cond_0
    :goto_0
    return-void

    .line 310
    :cond_1
    iget-boolean v0, p0, Lcom/google/android/setupwizard/carrier/ActivationActivity;->mCancelEnabled:Z

    if-eqz v0, :cond_0

    .line 311
    invoke-direct {p0}, Lcom/google/android/setupwizard/carrier/ActivationActivity;->onCancelRequested()V

    goto :goto_0
.end method
