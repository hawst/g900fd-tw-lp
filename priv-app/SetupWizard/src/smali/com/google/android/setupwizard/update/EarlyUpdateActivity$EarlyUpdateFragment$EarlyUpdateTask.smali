.class Lcom/google/android/setupwizard/update/EarlyUpdateActivity$EarlyUpdateFragment$EarlyUpdateTask;
.super Landroid/os/AsyncTask;
.source "EarlyUpdateActivity.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/setupwizard/update/EarlyUpdateActivity$EarlyUpdateFragment;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "EarlyUpdateTask"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/os/AsyncTask",
        "<",
        "Ljava/lang/Void;",
        "Landroid/os/Bundle;",
        "Ljava/lang/Integer;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/setupwizard/update/EarlyUpdateActivity$EarlyUpdateFragment;


# direct methods
.method private constructor <init>(Lcom/google/android/setupwizard/update/EarlyUpdateActivity$EarlyUpdateFragment;)V
    .locals 0

    .prologue
    .line 260
    iput-object p1, p0, Lcom/google/android/setupwizard/update/EarlyUpdateActivity$EarlyUpdateFragment$EarlyUpdateTask;->this$0:Lcom/google/android/setupwizard/update/EarlyUpdateActivity$EarlyUpdateFragment;

    invoke-direct {p0}, Landroid/os/AsyncTask;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/google/android/setupwizard/update/EarlyUpdateActivity$EarlyUpdateFragment;Lcom/google/android/setupwizard/update/EarlyUpdateActivity$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/google/android/setupwizard/update/EarlyUpdateActivity$EarlyUpdateFragment;
    .param p2, "x1"    # Lcom/google/android/setupwizard/update/EarlyUpdateActivity$1;

    .prologue
    .line 260
    invoke-direct {p0, p1}, Lcom/google/android/setupwizard/update/EarlyUpdateActivity$EarlyUpdateFragment$EarlyUpdateTask;-><init>(Lcom/google/android/setupwizard/update/EarlyUpdateActivity$EarlyUpdateFragment;)V

    return-void
.end method


# virtual methods
.method protected varargs doInBackground([Ljava/lang/Void;)Ljava/lang/Integer;
    .locals 7
    .param p1, "voids"    # [Ljava/lang/Void;

    .prologue
    const/4 v6, 0x1

    .line 265
    :try_start_0
    iget-object v3, p0, Lcom/google/android/setupwizard/update/EarlyUpdateActivity$EarlyUpdateFragment$EarlyUpdateTask;->this$0:Lcom/google/android/setupwizard/update/EarlyUpdateActivity$EarlyUpdateFragment;

    iget-object v4, p0, Lcom/google/android/setupwizard/update/EarlyUpdateActivity$EarlyUpdateFragment$EarlyUpdateTask;->this$0:Lcom/google/android/setupwizard/update/EarlyUpdateActivity$EarlyUpdateFragment;

    # getter for: Lcom/google/android/setupwizard/update/EarlyUpdateActivity$EarlyUpdateFragment;->mPlayHelper:Lcom/google/android/setupwizard/util/GooglePlaySetupHelper;
    invoke-static {v4}, Lcom/google/android/setupwizard/update/EarlyUpdateActivity$EarlyUpdateFragment;->access$800(Lcom/google/android/setupwizard/update/EarlyUpdateActivity$EarlyUpdateFragment;)Lcom/google/android/setupwizard/util/GooglePlaySetupHelper;

    move-result-object v4

    invoke-virtual {v4}, Lcom/google/android/setupwizard/util/GooglePlaySetupHelper;->getEarlyUpdate()Landroid/os/Bundle;

    move-result-object v4

    # setter for: Lcom/google/android/setupwizard/update/EarlyUpdateActivity$EarlyUpdateFragment;->mCurrentUpdate:Landroid/os/Bundle;
    invoke-static {v3, v4}, Lcom/google/android/setupwizard/update/EarlyUpdateActivity$EarlyUpdateFragment;->access$202(Lcom/google/android/setupwizard/update/EarlyUpdateActivity$EarlyUpdateFragment;Landroid/os/Bundle;)Landroid/os/Bundle;
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 270
    iget-object v3, p0, Lcom/google/android/setupwizard/update/EarlyUpdateActivity$EarlyUpdateFragment$EarlyUpdateTask;->this$0:Lcom/google/android/setupwizard/update/EarlyUpdateActivity$EarlyUpdateFragment;

    # getter for: Lcom/google/android/setupwizard/update/EarlyUpdateActivity$EarlyUpdateFragment;->mCurrentUpdate:Landroid/os/Bundle;
    invoke-static {v3}, Lcom/google/android/setupwizard/update/EarlyUpdateActivity$EarlyUpdateFragment;->access$200(Lcom/google/android/setupwizard/update/EarlyUpdateActivity$EarlyUpdateFragment;)Landroid/os/Bundle;

    move-result-object v3

    if-nez v3, :cond_0

    .line 271
    const-string v3, "SetupWizard.EarlyUpdate"

    const-string v4, "Found no more early updates"

    invoke-static {v3, v4}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 272
    const/4 v3, -0x1

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    .line 295
    :goto_0
    return-object v3

    .line 266
    :catch_0
    move-exception v1

    .line 267
    .local v1, "e":Landroid/os/RemoteException;
    const-string v3, "SetupWizard.EarlyUpdate"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Cannot get early update: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v1}, Landroid/os/RemoteException;->getMessage()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 268
    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    goto :goto_0

    .line 275
    .end local v1    # "e":Landroid/os/RemoteException;
    :cond_0
    iget-object v3, p0, Lcom/google/android/setupwizard/update/EarlyUpdateActivity$EarlyUpdateFragment$EarlyUpdateTask;->this$0:Lcom/google/android/setupwizard/update/EarlyUpdateActivity$EarlyUpdateFragment;

    # getter for: Lcom/google/android/setupwizard/update/EarlyUpdateActivity$EarlyUpdateFragment;->mCurrentUpdate:Landroid/os/Bundle;
    invoke-static {v3}, Lcom/google/android/setupwizard/update/EarlyUpdateActivity$EarlyUpdateFragment;->access$200(Lcom/google/android/setupwizard/update/EarlyUpdateActivity$EarlyUpdateFragment;)Landroid/os/Bundle;

    move-result-object v3

    const-string v4, "result_code"

    invoke-virtual {v3, v4}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v2

    .line 276
    .local v2, "resultCode":I
    iget-object v3, p0, Lcom/google/android/setupwizard/update/EarlyUpdateActivity$EarlyUpdateFragment$EarlyUpdateTask;->this$0:Lcom/google/android/setupwizard/update/EarlyUpdateActivity$EarlyUpdateFragment;

    # getter for: Lcom/google/android/setupwizard/update/EarlyUpdateActivity$EarlyUpdateFragment;->mCurrentUpdate:Landroid/os/Bundle;
    invoke-static {v3}, Lcom/google/android/setupwizard/update/EarlyUpdateActivity$EarlyUpdateFragment;->access$200(Lcom/google/android/setupwizard/update/EarlyUpdateActivity$EarlyUpdateFragment;)Landroid/os/Bundle;

    move-result-object v3

    const-string v4, "critical"

    invoke-virtual {v3, v4}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v0

    .line 277
    .local v0, "critical":Z
    const-string v3, "SetupWizard.EarlyUpdate"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "EarlyUpdateTask "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-object v5, p0, Lcom/google/android/setupwizard/update/EarlyUpdateActivity$EarlyUpdateFragment$EarlyUpdateTask;->this$0:Lcom/google/android/setupwizard/update/EarlyUpdateActivity$EarlyUpdateFragment;

    # getter for: Lcom/google/android/setupwizard/update/EarlyUpdateActivity$EarlyUpdateFragment;->mCurrentUpdate:Landroid/os/Bundle;
    invoke-static {v5}, Lcom/google/android/setupwizard/update/EarlyUpdateActivity$EarlyUpdateFragment;->access$200(Lcom/google/android/setupwizard/update/EarlyUpdateActivity$EarlyUpdateFragment;)Landroid/os/Bundle;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 279
    if-gez v2, :cond_2

    .line 284
    if-eqz v0, :cond_1

    .line 285
    const-string v3, "SetupWizard.EarlyUpdate"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Critical update failed with resultCode="

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 286
    const/4 v3, 0x0

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    goto :goto_0

    .line 288
    :cond_1
    const-string v3, "SetupWizard.EarlyUpdate"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Non-critical update failed with resultCode="

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 289
    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    goto/16 :goto_0

    .line 292
    :cond_2
    iget-object v3, p0, Lcom/google/android/setupwizard/update/EarlyUpdateActivity$EarlyUpdateFragment$EarlyUpdateTask;->this$0:Lcom/google/android/setupwizard/update/EarlyUpdateActivity$EarlyUpdateFragment;

    # getter for: Lcom/google/android/setupwizard/update/EarlyUpdateActivity$EarlyUpdateFragment;->mPlayHelper:Lcom/google/android/setupwizard/util/GooglePlaySetupHelper;
    invoke-static {v3}, Lcom/google/android/setupwizard/update/EarlyUpdateActivity$EarlyUpdateFragment;->access$800(Lcom/google/android/setupwizard/update/EarlyUpdateActivity$EarlyUpdateFragment;)Lcom/google/android/setupwizard/util/GooglePlaySetupHelper;

    move-result-object v3

    invoke-virtual {v3}, Lcom/google/android/setupwizard/util/GooglePlaySetupHelper;->startEarlyUpdate()V

    .line 295
    const/4 v3, 0x0

    goto/16 :goto_0
.end method

.method protected bridge synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1
    .param p1, "x0"    # [Ljava/lang/Object;

    .prologue
    .line 260
    check-cast p1, [Ljava/lang/Void;

    .end local p1    # "x0":[Ljava/lang/Object;
    invoke-virtual {p0, p1}, Lcom/google/android/setupwizard/update/EarlyUpdateActivity$EarlyUpdateFragment$EarlyUpdateTask;->doInBackground([Ljava/lang/Void;)Ljava/lang/Integer;

    move-result-object v0

    return-object v0
.end method

.method protected onPostExecute(Ljava/lang/Integer;)V
    .locals 2
    .param p1, "result"    # Ljava/lang/Integer;

    .prologue
    .line 300
    invoke-super {p0, p1}, Landroid/os/AsyncTask;->onPostExecute(Ljava/lang/Object;)V

    .line 301
    if-eqz p1, :cond_0

    .line 303
    iget-object v0, p0, Lcom/google/android/setupwizard/update/EarlyUpdateActivity$EarlyUpdateFragment$EarlyUpdateTask;->this$0:Lcom/google/android/setupwizard/update/EarlyUpdateActivity$EarlyUpdateFragment;

    invoke-virtual {p1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/google/android/setupwizard/update/EarlyUpdateActivity$EarlyUpdateFragment;->finish(I)V

    .line 305
    :cond_0
    return-void
.end method

.method protected bridge synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 0
    .param p1, "x0"    # Ljava/lang/Object;

    .prologue
    .line 260
    check-cast p1, Ljava/lang/Integer;

    .end local p1    # "x0":Ljava/lang/Object;
    invoke-virtual {p0, p1}, Lcom/google/android/setupwizard/update/EarlyUpdateActivity$EarlyUpdateFragment$EarlyUpdateTask;->onPostExecute(Ljava/lang/Integer;)V

    return-void
.end method
