.class Lcom/google/android/setupwizard/user/NameActivity$NameWatcher;
.super Ljava/lang/Object;
.source "NameActivity.java"

# interfaces
.implements Landroid/text/TextWatcher;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/setupwizard/user/NameActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "NameWatcher"
.end annotation


# instance fields
.field private final mEditText:Landroid/widget/EditText;

.field final synthetic this$0:Lcom/google/android/setupwizard/user/NameActivity;


# direct methods
.method public constructor <init>(Lcom/google/android/setupwizard/user/NameActivity;Landroid/widget/EditText;)V
    .locals 0
    .param p2, "editText"    # Landroid/widget/EditText;

    .prologue
    .line 84
    iput-object p1, p0, Lcom/google/android/setupwizard/user/NameActivity$NameWatcher;->this$0:Lcom/google/android/setupwizard/user/NameActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 85
    iput-object p2, p0, Lcom/google/android/setupwizard/user/NameActivity$NameWatcher;->mEditText:Landroid/widget/EditText;

    .line 86
    return-void
.end method


# virtual methods
.method public afterTextChanged(Landroid/text/Editable;)V
    .locals 0
    .param p1, "s"    # Landroid/text/Editable;

    .prologue
    .line 101
    return-void
.end method

.method public beforeTextChanged(Ljava/lang/CharSequence;III)V
    .locals 0
    .param p1, "s"    # Ljava/lang/CharSequence;
    .param p2, "start"    # I
    .param p3, "count"    # I
    .param p4, "after"    # I

    .prologue
    .line 90
    return-void
.end method

.method public onTextChanged(Ljava/lang/CharSequence;III)V
    .locals 3
    .param p1, "s"    # Ljava/lang/CharSequence;
    .param p2, "start"    # I
    .param p3, "before"    # I
    .param p4, "count"    # I

    .prologue
    .line 94
    # invokes: Lcom/google/android/setupwizard/user/NameActivity;->isNamePrefixValid(Ljava/lang/CharSequence;)Z
    invoke-static {p1}, Lcom/google/android/setupwizard/user/NameActivity;->access$000(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v0, 0x0

    .line 95
    .local v0, "error":Ljava/lang/String;
    :goto_0
    iget-object v1, p0, Lcom/google/android/setupwizard/user/NameActivity$NameWatcher;->mEditText:Landroid/widget/EditText;

    invoke-virtual {v1, v0}, Landroid/widget/EditText;->setError(Ljava/lang/CharSequence;)V

    .line 96
    iget-object v1, p0, Lcom/google/android/setupwizard/user/NameActivity$NameWatcher;->this$0:Lcom/google/android/setupwizard/user/NameActivity;

    invoke-virtual {v1}, Lcom/google/android/setupwizard/user/NameActivity;->updateWidgetState()V

    .line 97
    return-void

    .line 94
    .end local v0    # "error":Ljava/lang/String;
    :cond_0
    iget-object v1, p0, Lcom/google/android/setupwizard/user/NameActivity$NameWatcher;->this$0:Lcom/google/android/setupwizard/user/NameActivity;

    const v2, 0x7f0700b1

    invoke-virtual {v1, v2}, Lcom/google/android/setupwizard/user/NameActivity;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method
