.class Lcom/google/android/setupwizard/util/BackupRestoreHelper$2;
.super Ljava/lang/Object;
.source "BackupRestoreHelper.java"

# interfaces
.implements Landroid/content/ServiceConnection;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/setupwizard/util/BackupRestoreHelper;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/setupwizard/util/BackupRestoreHelper;


# direct methods
.method constructor <init>(Lcom/google/android/setupwizard/util/BackupRestoreHelper;)V
    .locals 0

    .prologue
    .line 121
    iput-object p1, p0, Lcom/google/android/setupwizard/util/BackupRestoreHelper$2;->this$0:Lcom/google/android/setupwizard/util/BackupRestoreHelper;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onServiceConnected(Landroid/content/ComponentName;Landroid/os/IBinder;)V
    .locals 2
    .param p1, "name"    # Landroid/content/ComponentName;
    .param p2, "service"    # Landroid/os/IBinder;

    .prologue
    .line 124
    const-string v0, "SetupWizard.BackupRestoreHelper"

    const-string v1, "Waiting for restore to finish"

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 125
    return-void
.end method

.method public onServiceDisconnected(Landroid/content/ComponentName;)V
    .locals 6
    .param p1, "name"    # Landroid/content/ComponentName;

    .prologue
    .line 129
    const-string v0, "SetupWizard.BackupRestoreHelper"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Restore finished after "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v2

    iget-object v4, p0, Lcom/google/android/setupwizard/util/BackupRestoreHelper$2;->this$0:Lcom/google/android/setupwizard/util/BackupRestoreHelper;

    # getter for: Lcom/google/android/setupwizard/util/BackupRestoreHelper;->mStartTime:J
    invoke-static {v4}, Lcom/google/android/setupwizard/util/BackupRestoreHelper;->access$100(Lcom/google/android/setupwizard/util/BackupRestoreHelper;)J

    move-result-wide v4

    sub-long/2addr v2, v4

    invoke-virtual {v1, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " ms"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 132
    :try_start_0
    iget-object v0, p0, Lcom/google/android/setupwizard/util/BackupRestoreHelper$2;->this$0:Lcom/google/android/setupwizard/util/BackupRestoreHelper;

    # getter for: Lcom/google/android/setupwizard/util/BackupRestoreHelper;->mContext:Landroid/content/Context;
    invoke-static {v0}, Lcom/google/android/setupwizard/util/BackupRestoreHelper;->access$200(Lcom/google/android/setupwizard/util/BackupRestoreHelper;)Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0, p0}, Landroid/content/Context;->unbindService(Landroid/content/ServiceConnection;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 136
    :goto_0
    return-void

    .line 133
    :catch_0
    move-exception v0

    goto :goto_0
.end method
