.class public Lcom/google/android/setupwizard/carrier/PhoneMonitor;
.super Ljava/lang/Object;
.source "PhoneMonitor.java"


# static fields
.field private static final FILTER_MOCK_OTASP_MODE:Landroid/content/IntentFilter;

.field private static sInstance:Lcom/google/android/setupwizard/carrier/PhoneMonitor;


# instance fields
.field private mCellNetworkState:I

.field private mCellNetworkType:I

.field private mCellSignalStrength:I

.field private mContext:Landroid/content/Context;

.field private mListeners:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Landroid/telephony/PhoneStateListener;",
            ">;"
        }
    .end annotation
.end field

.field private mMockOtaspMode:I

.field private final mMockOtaspReceiver:Landroid/content/BroadcastReceiver;

.field protected mOtaspMode:I

.field protected mPhoneServiceState:I

.field private final mPhoneStateListener:Landroid/telephony/PhoneStateListener;

.field private mServiceState:Landroid/telephony/ServiceState;

.field private mTelephony:Landroid/telephony/TelephonyManager;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 47
    new-instance v0, Landroid/content/IntentFilter;

    const-string v1, "com.google.android.setupwizard.MOCK_OTASP_MODE"

    invoke-direct {v0, v1}, Landroid/content/IntentFilter;-><init>(Ljava/lang/String;)V

    sput-object v0, Lcom/google/android/setupwizard/carrier/PhoneMonitor;->FILTER_MOCK_OTASP_MODE:Landroid/content/IntentFilter;

    return-void
.end method

.method private constructor <init>(Landroid/content/Context;)V
    .locals 3
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    const/4 v2, -0x1

    const/4 v1, 0x0

    .line 413
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 353
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/android/setupwizard/carrier/PhoneMonitor;->mListeners:Ljava/util/ArrayList;

    .line 357
    iput v2, p0, Lcom/google/android/setupwizard/carrier/PhoneMonitor;->mCellSignalStrength:I

    .line 358
    iput v1, p0, Lcom/google/android/setupwizard/carrier/PhoneMonitor;->mCellNetworkState:I

    .line 359
    iput v1, p0, Lcom/google/android/setupwizard/carrier/PhoneMonitor;->mCellNetworkType:I

    .line 364
    iput v2, p0, Lcom/google/android/setupwizard/carrier/PhoneMonitor;->mMockOtaspMode:I

    .line 367
    new-instance v0, Lcom/google/android/setupwizard/carrier/PhoneMonitor$1;

    invoke-direct {v0, p0}, Lcom/google/android/setupwizard/carrier/PhoneMonitor$1;-><init>(Lcom/google/android/setupwizard/carrier/PhoneMonitor;)V

    iput-object v0, p0, Lcom/google/android/setupwizard/carrier/PhoneMonitor;->mMockOtaspReceiver:Landroid/content/BroadcastReceiver;

    .line 375
    new-instance v0, Lcom/google/android/setupwizard/carrier/PhoneMonitor$2;

    invoke-direct {v0, p0}, Lcom/google/android/setupwizard/carrier/PhoneMonitor$2;-><init>(Lcom/google/android/setupwizard/carrier/PhoneMonitor;)V

    iput-object v0, p0, Lcom/google/android/setupwizard/carrier/PhoneMonitor;->mPhoneStateListener:Landroid/telephony/PhoneStateListener;

    .line 414
    iput-object p1, p0, Lcom/google/android/setupwizard/carrier/PhoneMonitor;->mContext:Landroid/content/Context;

    .line 415
    const-string v0, "ro.debuggable"

    invoke-static {v0, v1}, Landroid/os/SystemProperties;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 416
    iget-object v0, p0, Lcom/google/android/setupwizard/carrier/PhoneMonitor;->mContext:Landroid/content/Context;

    iget-object v1, p0, Lcom/google/android/setupwizard/carrier/PhoneMonitor;->mMockOtaspReceiver:Landroid/content/BroadcastReceiver;

    sget-object v2, Lcom/google/android/setupwizard/carrier/PhoneMonitor;->FILTER_MOCK_OTASP_MODE:Landroid/content/IntentFilter;

    invoke-virtual {v0, v1, v2}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    .line 418
    :cond_0
    iget-object v0, p0, Lcom/google/android/setupwizard/carrier/PhoneMonitor;->mContext:Landroid/content/Context;

    const-string v1, "phone"

    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/telephony/TelephonyManager;

    iput-object v0, p0, Lcom/google/android/setupwizard/carrier/PhoneMonitor;->mTelephony:Landroid/telephony/TelephonyManager;

    .line 420
    const-string v0, "PhoneMonitor"

    const-string v1, "Register our PhoneStateListener"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 421
    iget-object v0, p0, Lcom/google/android/setupwizard/carrier/PhoneMonitor;->mTelephony:Landroid/telephony/TelephonyManager;

    if-eqz v0, :cond_1

    .line 422
    iget-object v0, p0, Lcom/google/android/setupwizard/carrier/PhoneMonitor;->mTelephony:Landroid/telephony/TelephonyManager;

    iget-object v1, p0, Lcom/google/android/setupwizard/carrier/PhoneMonitor;->mPhoneStateListener:Landroid/telephony/PhoneStateListener;

    const/16 v2, 0x341

    invoke-virtual {v0, v1, v2}, Landroid/telephony/TelephonyManager;->listen(Landroid/telephony/PhoneStateListener;I)V

    .line 430
    :goto_0
    return-void

    .line 428
    :cond_1
    const-string v0, "PhoneMonitor"

    const-string v1, "Cannot register OTASP listener as TelephonyManager is null."

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method static synthetic access$002(Lcom/google/android/setupwizard/carrier/PhoneMonitor;I)I
    .locals 0
    .param p0, "x0"    # Lcom/google/android/setupwizard/carrier/PhoneMonitor;
    .param p1, "x1"    # I

    .prologue
    .line 40
    iput p1, p0, Lcom/google/android/setupwizard/carrier/PhoneMonitor;->mMockOtaspMode:I

    return p1
.end method

.method static synthetic access$100(Lcom/google/android/setupwizard/carrier/PhoneMonitor;)I
    .locals 1
    .param p0, "x0"    # Lcom/google/android/setupwizard/carrier/PhoneMonitor;

    .prologue
    .line 40
    invoke-direct {p0}, Lcom/google/android/setupwizard/carrier/PhoneMonitor;->getOtaspModeImpl()I

    move-result v0

    return v0
.end method

.method static synthetic access$200(Lcom/google/android/setupwizard/carrier/PhoneMonitor;)Landroid/telephony/PhoneStateListener;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/setupwizard/carrier/PhoneMonitor;

    .prologue
    .line 40
    iget-object v0, p0, Lcom/google/android/setupwizard/carrier/PhoneMonitor;->mPhoneStateListener:Landroid/telephony/PhoneStateListener;

    return-object v0
.end method

.method static synthetic access$302(Lcom/google/android/setupwizard/carrier/PhoneMonitor;Landroid/telephony/ServiceState;)Landroid/telephony/ServiceState;
    .locals 0
    .param p0, "x0"    # Lcom/google/android/setupwizard/carrier/PhoneMonitor;
    .param p1, "x1"    # Landroid/telephony/ServiceState;

    .prologue
    .line 40
    iput-object p1, p0, Lcom/google/android/setupwizard/carrier/PhoneMonitor;->mServiceState:Landroid/telephony/ServiceState;

    return-object p1
.end method

.method static synthetic access$400(Lcom/google/android/setupwizard/carrier/PhoneMonitor;)Ljava/util/ArrayList;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/setupwizard/carrier/PhoneMonitor;

    .prologue
    .line 40
    iget-object v0, p0, Lcom/google/android/setupwizard/carrier/PhoneMonitor;->mListeners:Ljava/util/ArrayList;

    return-object v0
.end method

.method static synthetic access$502(Lcom/google/android/setupwizard/carrier/PhoneMonitor;I)I
    .locals 0
    .param p0, "x0"    # Lcom/google/android/setupwizard/carrier/PhoneMonitor;
    .param p1, "x1"    # I

    .prologue
    .line 40
    iput p1, p0, Lcom/google/android/setupwizard/carrier/PhoneMonitor;->mCellNetworkType:I

    return p1
.end method

.method static synthetic access$602(Lcom/google/android/setupwizard/carrier/PhoneMonitor;I)I
    .locals 0
    .param p0, "x0"    # Lcom/google/android/setupwizard/carrier/PhoneMonitor;
    .param p1, "x1"    # I

    .prologue
    .line 40
    iput p1, p0, Lcom/google/android/setupwizard/carrier/PhoneMonitor;->mCellNetworkState:I

    return p1
.end method

.method static synthetic access$702(Lcom/google/android/setupwizard/carrier/PhoneMonitor;I)I
    .locals 0
    .param p0, "x0"    # Lcom/google/android/setupwizard/carrier/PhoneMonitor;
    .param p1, "x1"    # I

    .prologue
    .line 40
    iput p1, p0, Lcom/google/android/setupwizard/carrier/PhoneMonitor;->mCellSignalStrength:I

    return p1
.end method

.method public static addListener(Landroid/telephony/PhoneStateListener;)V
    .locals 1
    .param p0, "listener"    # Landroid/telephony/PhoneStateListener;

    .prologue
    .line 102
    sget-object v0, Lcom/google/android/setupwizard/carrier/PhoneMonitor;->sInstance:Lcom/google/android/setupwizard/carrier/PhoneMonitor;

    invoke-direct {v0, p0}, Lcom/google/android/setupwizard/carrier/PhoneMonitor;->addListenerImpl(Landroid/telephony/PhoneStateListener;)V

    .line 103
    return-void
.end method

.method private addListenerImpl(Landroid/telephony/PhoneStateListener;)V
    .locals 1
    .param p1, "listener"    # Landroid/telephony/PhoneStateListener;

    .prologue
    .line 451
    iget-object v0, p0, Lcom/google/android/setupwizard/carrier/PhoneMonitor;->mListeners:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 452
    return-void
.end method

.method public static canActivate()Z
    .locals 1

    .prologue
    .line 62
    sget-object v0, Lcom/google/android/setupwizard/carrier/PhoneMonitor;->sInstance:Lcom/google/android/setupwizard/carrier/PhoneMonitor;

    invoke-direct {v0}, Lcom/google/android/setupwizard/carrier/PhoneMonitor;->canActivateImpl()Z

    move-result v0

    return v0
.end method

.method private canActivateImpl()Z
    .locals 2

    .prologue
    .line 459
    iget v0, p0, Lcom/google/android/setupwizard/carrier/PhoneMonitor;->mPhoneServiceState:I

    if-eqz v0, :cond_0

    iget v0, p0, Lcom/google/android/setupwizard/carrier/PhoneMonitor;->mPhoneServiceState:I

    const/4 v1, 0x2

    if-ne v0, v1, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static canSkipWifiSetup()Z
    .locals 1

    .prologue
    .line 91
    sget-object v0, Lcom/google/android/setupwizard/carrier/PhoneMonitor;->sInstance:Lcom/google/android/setupwizard/carrier/PhoneMonitor;

    invoke-direct {v0}, Lcom/google/android/setupwizard/carrier/PhoneMonitor;->canSkipWifiSetupImpl()Z

    move-result v0

    return v0
.end method

.method private canSkipWifiSetupImpl()Z
    .locals 5

    .prologue
    const/4 v1, 0x0

    .line 477
    iget v2, p0, Lcom/google/android/setupwizard/carrier/PhoneMonitor;->mCellNetworkType:I

    packed-switch v2, :pswitch_data_0

    .line 488
    :cond_0
    :goto_0
    :pswitch_0
    return v1

    .line 484
    :pswitch_1
    iget-object v2, p0, Lcom/google/android/setupwizard/carrier/PhoneMonitor;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    const-string v3, "ro.setupwizard.gservices_min_good_cell_level"

    const v4, 0x7fffffff

    invoke-static {v2, v3, v4}, Lcom/google/android/gsf/Gservices;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v0

    .line 486
    .local v0, "minimumLevel":I
    iget v2, p0, Lcom/google/android/setupwizard/carrier/PhoneMonitor;->mCellSignalStrength:I

    if-lt v2, v0, :cond_0

    const/4 v1, 0x1

    goto :goto_0

    .line 477
    :pswitch_data_0
    .packed-switch 0x8
        :pswitch_1
        :pswitch_0
        :pswitch_1
        :pswitch_0
        :pswitch_0
        :pswitch_1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public static getActivationTimeout()I
    .locals 3

    .prologue
    .line 185
    invoke-static {}, Lcom/google/android/setupwizard/carrier/PhoneMonitor;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f090001

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v0

    .line 186
    .local v0, "timeout":I
    const-string v1, "ro.activation.timeout.millis"

    invoke-static {v1, v0}, Landroid/os/SystemProperties;->getInt(Ljava/lang/String;I)I

    move-result v1

    return v1
.end method

.method private static getContext()Landroid/content/Context;
    .locals 1

    .prologue
    .line 339
    sget-object v0, Lcom/google/android/setupwizard/carrier/PhoneMonitor;->sInstance:Lcom/google/android/setupwizard/carrier/PhoneMonitor;

    iget-object v0, v0, Lcom/google/android/setupwizard/carrier/PhoneMonitor;->mContext:Landroid/content/Context;

    return-object v0
.end method

.method private getDataRegStateImpl()I
    .locals 1

    .prologue
    .line 496
    iget-object v0, p0, Lcom/google/android/setupwizard/carrier/PhoneMonitor;->mServiceState:Landroid/telephony/ServiceState;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/setupwizard/carrier/PhoneMonitor;->mServiceState:Landroid/telephony/ServiceState;

    invoke-virtual {v0}, Landroid/telephony/ServiceState;->getDataRegState()I

    move-result v0

    :goto_0
    return v0

    :cond_0
    const/4 v0, -0x1

    goto :goto_0
.end method

.method protected static getDataServiceStateName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 294
    sget-object v0, Lcom/google/android/setupwizard/carrier/PhoneMonitor;->sInstance:Lcom/google/android/setupwizard/carrier/PhoneMonitor;

    invoke-direct {v0}, Lcom/google/android/setupwizard/carrier/PhoneMonitor;->getDataRegStateImpl()I

    move-result v0

    invoke-static {v0}, Lcom/google/android/setupwizard/carrier/PhoneMonitor;->getServiceStateName(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method protected static getDataStateName()Ljava/lang/String;
    .locals 2

    .prologue
    .line 268
    sget-object v1, Lcom/google/android/setupwizard/carrier/PhoneMonitor;->sInstance:Lcom/google/android/setupwizard/carrier/PhoneMonitor;

    iget-object v0, v1, Lcom/google/android/setupwizard/carrier/PhoneMonitor;->mTelephony:Landroid/telephony/TelephonyManager;

    .line 269
    .local v0, "tm":Landroid/telephony/TelephonyManager;
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Landroid/telephony/TelephonyManager;->getDataState()I

    move-result v1

    invoke-static {v1}, Lcom/google/android/setupwizard/carrier/PhoneMonitor;->getDataStateName(I)Ljava/lang/String;

    move-result-object v1

    :goto_0
    return-object v1

    :cond_0
    const-string v1, ""

    goto :goto_0
.end method

.method protected static getDataStateName(I)Ljava/lang/String;
    .locals 3
    .param p0, "dataState"    # I

    .prologue
    .line 273
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 274
    .local v0, "sb":Ljava/lang/StringBuilder;
    packed-switch p0, :pswitch_data_0

    .line 281
    :goto_0
    const-string v1, "("

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ")"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 282
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    return-object v1

    .line 275
    :pswitch_0
    const-string v1, "DATA_UNKNOWN"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_0

    .line 276
    :pswitch_1
    const-string v1, "DATA_DISCONNECTED"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_0

    .line 277
    :pswitch_2
    const-string v1, "DATA_CONNECTING"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_0

    .line 278
    :pswitch_3
    const-string v1, "DATA_CONNECTED"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_0

    .line 279
    :pswitch_4
    const-string v1, "DATA_SUSPENDED"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_0

    .line 274
    :pswitch_data_0
    .packed-switch -0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
    .end packed-switch
.end method

.method public static getLteOnCdmaMode()I
    .locals 3

    .prologue
    const/4 v2, -0x1

    .line 117
    invoke-static {}, Lcom/google/android/setupwizard/carrier/PhoneMonitor;->getTelephony()Landroid/telephony/TelephonyManager;

    move-result-object v0

    .line 120
    .local v0, "tm":Landroid/telephony/TelephonyManager;
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Landroid/telephony/TelephonyManager;->getLteOnCdmaMode()I

    move-result v1

    if-ne v1, v2, :cond_1

    .line 121
    :cond_0
    const-string v1, "telephony.lteOnCdmaDevice"

    invoke-static {v1, v2}, Landroid/os/SystemProperties;->getInt(Ljava/lang/String;I)I

    move-result v1

    .line 124
    :goto_0
    return v1

    :cond_1
    invoke-virtual {v0}, Landroid/telephony/TelephonyManager;->getLteOnCdmaMode()I

    move-result v1

    goto :goto_0
.end method

.method protected static getMcc()I
    .locals 1

    .prologue
    .line 209
    invoke-static {}, Lcom/google/android/setupwizard/carrier/PhoneMonitor;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v0

    iget v0, v0, Landroid/content/res/Configuration;->mcc:I

    return v0
.end method

.method protected static getMnc()I
    .locals 1

    .prologue
    .line 213
    invoke-static {}, Lcom/google/android/setupwizard/carrier/PhoneMonitor;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v0

    iget v0, v0, Landroid/content/res/Configuration;->mnc:I

    return v0
.end method

.method public static getMobileDataTimeout()I
    .locals 3

    .prologue
    .line 191
    invoke-static {}, Lcom/google/android/setupwizard/carrier/PhoneMonitor;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f090002

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v0

    .line 192
    .local v0, "timeout":I
    const-string v1, "ro.setupwizard.mobile_data_time"

    invoke-static {v1, v0}, Landroid/os/SystemProperties;->getInt(Ljava/lang/String;I)I

    move-result v1

    return v1
.end method

.method protected static getNetworkTypeName()Ljava/lang/String;
    .locals 2

    .prologue
    .line 229
    sget-object v1, Lcom/google/android/setupwizard/carrier/PhoneMonitor;->sInstance:Lcom/google/android/setupwizard/carrier/PhoneMonitor;

    iget-object v0, v1, Lcom/google/android/setupwizard/carrier/PhoneMonitor;->mTelephony:Landroid/telephony/TelephonyManager;

    .line 230
    .local v0, "tm":Landroid/telephony/TelephonyManager;
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Landroid/telephony/TelephonyManager;->getNetworkTypeName()Ljava/lang/String;

    move-result-object v1

    :goto_0
    return-object v1

    :cond_0
    const-string v1, ""

    goto :goto_0
.end method

.method private getOtaspModeImpl()I
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 500
    const-string v1, "ro.debuggable"

    invoke-static {v1, v0}, Landroid/os/SystemProperties;->getBoolean(Ljava/lang/String;Z)Z

    move-result v1

    if-eqz v1, :cond_0

    iget v1, p0, Lcom/google/android/setupwizard/carrier/PhoneMonitor;->mMockOtaspMode:I

    const/4 v2, -0x1

    if-le v1, v2, :cond_0

    const/4 v0, 0x1

    .line 502
    .local v0, "useMockValue":Z
    :cond_0
    if-eqz v0, :cond_1

    iget v1, p0, Lcom/google/android/setupwizard/carrier/PhoneMonitor;->mMockOtaspMode:I

    :goto_0
    return v1

    :cond_1
    iget v1, p0, Lcom/google/android/setupwizard/carrier/PhoneMonitor;->mOtaspMode:I

    goto :goto_0
.end method

.method protected static getOtaspModeName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 311
    sget-object v0, Lcom/google/android/setupwizard/carrier/PhoneMonitor;->sInstance:Lcom/google/android/setupwizard/carrier/PhoneMonitor;

    invoke-direct {v0}, Lcom/google/android/setupwizard/carrier/PhoneMonitor;->getOtaspModeImpl()I

    move-result v0

    invoke-static {v0}, Lcom/google/android/setupwizard/carrier/PhoneMonitor;->getOtaspModeName(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method protected static getOtaspModeName(I)Ljava/lang/String;
    .locals 3
    .param p0, "otaspMode"    # I

    .prologue
    .line 315
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 316
    .local v0, "sb":Ljava/lang/StringBuilder;
    packed-switch p0, :pswitch_data_0

    .line 322
    :goto_0
    const-string v1, "("

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ")"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 323
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    return-object v1

    .line 317
    :pswitch_0
    const-string v1, "OTASP_UNINITIALIZED"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_0

    .line 318
    :pswitch_1
    const-string v1, "OTASP_UNKNOWN"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_0

    .line 319
    :pswitch_2
    const-string v1, "OTASP_NEEDED"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_0

    .line 320
    :pswitch_3
    const-string v1, "OTASP_NOT_NEEDED"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_0

    .line 316
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method protected static getPhoneState()Ljava/lang/String;
    .locals 2

    .prologue
    .line 327
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-static {}, Lcom/google/android/setupwizard/carrier/PhoneMonitor;->getSimStateName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-static {}, Lcom/google/android/setupwizard/carrier/PhoneMonitor;->getPhoneTypeName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " \""

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-static {}, Lcom/google/android/setupwizard/carrier/PhoneMonitor;->getNetworkTypeName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "\""

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " mcc"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-static {}, Lcom/google/android/setupwizard/carrier/PhoneMonitor;->getMcc()I

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "mnc"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-static {}, Lcom/google/android/setupwizard/carrier/PhoneMonitor;->getMnc()I

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " V:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-static {}, Lcom/google/android/setupwizard/carrier/PhoneMonitor;->getVoiceServiceStateName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " D:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-static {}, Lcom/google/android/setupwizard/carrier/PhoneMonitor;->getDataServiceStateName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-static {}, Lcom/google/android/setupwizard/carrier/PhoneMonitor;->getDataStateName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-static {}, Lcom/google/android/setupwizard/carrier/PhoneMonitor;->getOtaspModeName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " HFA_ACTIVATED="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-static {}, Lcom/google/android/setupwizard/carrier/PhoneMonitor;->isHfaActivated()Z

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method protected static getPhoneTypeName()Ljava/lang/String;
    .locals 2

    .prologue
    .line 204
    sget-object v1, Lcom/google/android/setupwizard/carrier/PhoneMonitor;->sInstance:Lcom/google/android/setupwizard/carrier/PhoneMonitor;

    iget-object v0, v1, Lcom/google/android/setupwizard/carrier/PhoneMonitor;->mTelephony:Landroid/telephony/TelephonyManager;

    .line 205
    .local v0, "tm":Landroid/telephony/TelephonyManager;
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Landroid/telephony/TelephonyManager;->getCurrentPhoneType()I

    move-result v1

    :goto_0
    invoke-static {v1}, Lcom/google/android/setupwizard/carrier/PhoneMonitor;->getPhoneTypeName(I)Ljava/lang/String;

    move-result-object v1

    return-object v1

    :cond_0
    const/4 v1, -0x1

    goto :goto_0
.end method

.method protected static getPhoneTypeName(I)Ljava/lang/String;
    .locals 3
    .param p0, "phoneType"    # I

    .prologue
    .line 217
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 218
    .local v0, "sb":Ljava/lang/StringBuilder;
    packed-switch p0, :pswitch_data_0

    .line 224
    :goto_0
    const-string v1, "("

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ")"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 225
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    return-object v1

    .line 219
    :pswitch_0
    const-string v1, "PHONE_TYPE_NONE"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_0

    .line 220
    :pswitch_1
    const-string v1, "PHONE_TYPE_GSM"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_0

    .line 221
    :pswitch_2
    const-string v1, "PHONE_TYPE_CDMA"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_0

    .line 222
    :pswitch_3
    const-string v1, "PHONE_TYPE_SIP"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_0

    .line 218
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method protected static getServiceStateName(I)Ljava/lang/String;
    .locals 3
    .param p0, "serviceState"    # I

    .prologue
    .line 298
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 299
    .local v0, "sb":Ljava/lang/StringBuilder;
    packed-switch p0, :pswitch_data_0

    .line 305
    :goto_0
    const-string v1, "("

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ")"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 306
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    return-object v1

    .line 300
    :pswitch_0
    const-string v1, "STATE_IN_SERVICE"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_0

    .line 301
    :pswitch_1
    const-string v1, "STATE_OUT_OF_SERVICE"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_0

    .line 302
    :pswitch_2
    const-string v1, "STATE_EMERGENCY_ONLY"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_0

    .line 303
    :pswitch_3
    const-string v1, "STATE_POWER_OFF"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_0

    .line 299
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method protected static getSimStateName()Ljava/lang/String;
    .locals 2

    .prologue
    .line 234
    sget-object v1, Lcom/google/android/setupwizard/carrier/PhoneMonitor;->sInstance:Lcom/google/android/setupwizard/carrier/PhoneMonitor;

    iget-object v0, v1, Lcom/google/android/setupwizard/carrier/PhoneMonitor;->mTelephony:Landroid/telephony/TelephonyManager;

    .line 235
    .local v0, "tm":Landroid/telephony/TelephonyManager;
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Landroid/telephony/TelephonyManager;->getSimState()I

    move-result v1

    invoke-static {v1}, Lcom/google/android/setupwizard/carrier/PhoneMonitor;->getSimStateName(I)Ljava/lang/String;

    move-result-object v1

    :goto_0
    return-object v1

    :cond_0
    const-string v1, ""

    goto :goto_0
.end method

.method protected static getSimStateName(I)Ljava/lang/String;
    .locals 3
    .param p0, "simState"    # I

    .prologue
    .line 239
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 240
    .local v0, "sb":Ljava/lang/StringBuilder;
    packed-switch p0, :pswitch_data_0

    .line 263
    :goto_0
    const-string v1, "("

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ")"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 264
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    return-object v1

    .line 242
    :pswitch_0
    const-string v1, "SIM_STATE_UNKNOWN"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_0

    .line 245
    :pswitch_1
    const-string v1, "SIM_STATE_ABSENT"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_0

    .line 248
    :pswitch_2
    const-string v1, "SIM_STATE_PIN_REQUIRED"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_0

    .line 251
    :pswitch_3
    const-string v1, "SIM_STATE_PUK_REQUIRED"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_0

    .line 254
    :pswitch_4
    const-string v1, "SIM_STATE_NETWORK_LOCKED"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_0

    .line 257
    :pswitch_5
    const-string v1, "SIM_STATE_READY"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_0

    .line 260
    :pswitch_6
    const-string v1, "SIM_STATE_CARD_IO_ERROR"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_0

    .line 240
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
    .end packed-switch
.end method

.method private static getTelephony()Landroid/telephony/TelephonyManager;
    .locals 1

    .prologue
    .line 343
    sget-object v0, Lcom/google/android/setupwizard/carrier/PhoneMonitor;->sInstance:Lcom/google/android/setupwizard/carrier/PhoneMonitor;

    iget-object v0, v0, Lcom/google/android/setupwizard/carrier/PhoneMonitor;->mTelephony:Landroid/telephony/TelephonyManager;

    return-object v0
.end method

.method private getVoiceRegStateImpl()I
    .locals 1

    .prologue
    .line 492
    iget-object v0, p0, Lcom/google/android/setupwizard/carrier/PhoneMonitor;->mServiceState:Landroid/telephony/ServiceState;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/setupwizard/carrier/PhoneMonitor;->mServiceState:Landroid/telephony/ServiceState;

    invoke-virtual {v0}, Landroid/telephony/ServiceState;->getVoiceRegState()I

    move-result v0

    :goto_0
    return v0

    :cond_0
    const/4 v0, -0x1

    goto :goto_0
.end method

.method protected static getVoiceServiceStateName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 290
    sget-object v0, Lcom/google/android/setupwizard/carrier/PhoneMonitor;->sInstance:Lcom/google/android/setupwizard/carrier/PhoneMonitor;

    invoke-direct {v0}, Lcom/google/android/setupwizard/carrier/PhoneMonitor;->getVoiceRegStateImpl()I

    move-result v0

    invoke-static {v0}, Lcom/google/android/setupwizard/carrier/PhoneMonitor;->getServiceStateName(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public static hasSimProblem()Z
    .locals 4

    .prologue
    const/4 v1, 0x1

    .line 155
    invoke-static {}, Lcom/google/android/setupwizard/carrier/PhoneMonitor;->getTelephony()Landroid/telephony/TelephonyManager;

    move-result-object v0

    .line 156
    .local v0, "tm":Landroid/telephony/TelephonyManager;
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Landroid/telephony/TelephonyManager;->getCurrentPhoneType()I

    move-result v2

    if-ne v2, v1, :cond_0

    invoke-virtual {v0}, Landroid/telephony/TelephonyManager;->getSimState()I

    move-result v2

    const/4 v3, 0x5

    if-eq v2, v3, :cond_0

    invoke-virtual {v0}, Landroid/telephony/TelephonyManager;->getSimState()I

    move-result v2

    if-eqz v2, :cond_0

    :goto_0
    return v1

    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public static initInstance(Landroid/content/Context;)V
    .locals 2
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 53
    sget-object v0, Lcom/google/android/setupwizard/carrier/PhoneMonitor;->sInstance:Lcom/google/android/setupwizard/carrier/PhoneMonitor;

    if-nez v0, :cond_0

    .line 54
    new-instance v0, Lcom/google/android/setupwizard/carrier/PhoneMonitor;

    invoke-virtual {p0}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/google/android/setupwizard/carrier/PhoneMonitor;-><init>(Landroid/content/Context;)V

    sput-object v0, Lcom/google/android/setupwizard/carrier/PhoneMonitor;->sInstance:Lcom/google/android/setupwizard/carrier/PhoneMonitor;

    .line 56
    :cond_0
    return-void
.end method

.method public static isCdmaActivationRequired()Z
    .locals 1

    .prologue
    .line 128
    invoke-static {}, Lcom/google/android/setupwizard/carrier/PhoneMonitor;->isHfaActivated()Z

    move-result v0

    if-nez v0, :cond_0

    invoke-static {}, Lcom/google/android/setupwizard/carrier/PhoneMonitor;->getMobileDataTimeout()I

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static isHfaActivated()Z
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 132
    invoke-static {}, Lcom/google/android/setupwizard/carrier/PhoneMonitor;->getContext()Landroid/content/Context;

    move-result-object v0

    const-string v1, "SetupWizardLocalPrefs"

    invoke-virtual {v0, v1, v2}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v0

    const-string v1, "hfa_activated"

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    return v0
.end method

.method public static isHfaActivationRequired()Z
    .locals 5

    .prologue
    .line 143
    invoke-static {}, Lcom/google/android/setupwizard/carrier/PhoneMonitor;->getContext()Landroid/content/Context;

    move-result-object v3

    invoke-virtual {v3}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const/high16 v4, 0x7f0c0000

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getBoolean(I)Z

    move-result v1

    .line 144
    .local v1, "resFlag":Z
    const-string v3, "ro.setupwizard.hfa_always"

    invoke-static {v3, v1}, Landroid/os/SystemProperties;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    .line 145
    .local v0, "always":Z
    if-nez v0, :cond_0

    invoke-static {}, Lcom/google/android/setupwizard/carrier/PhoneMonitor;->otaspIsNotNeeded()Z

    move-result v3

    if-nez v3, :cond_1

    :cond_0
    const/4 v2, 0x1

    .line 146
    .local v2, "result":Z
    :goto_0
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "isHfaActivationRequired result="

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " always="

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Lcom/google/android/setupwizard/carrier/PhoneMonitor;->logPhoneState(Ljava/lang/String;)V

    .line 147
    return v2

    .line 145
    .end local v2    # "result":Z
    :cond_1
    const/4 v2, 0x0

    goto :goto_0
.end method

.method public static isLTE()Z
    .locals 2

    .prologue
    const/4 v0, 0x1

    .line 113
    invoke-static {}, Lcom/google/android/setupwizard/carrier/PhoneMonitor;->getLteOnCdmaMode()I

    move-result v1

    if-ne v1, v0, :cond_0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static logPhoneState(Ljava/lang/String;)V
    .locals 3
    .param p0, "prefix"    # Ljava/lang/String;

    .prologue
    .line 200
    const-string v0, "PhoneMonitor"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-static {}, Lcom/google/android/setupwizard/carrier/PhoneMonitor;->getPhoneState()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 201
    return-void
.end method

.method public static lteUnknown()Z
    .locals 2

    .prologue
    .line 98
    invoke-static {}, Lcom/google/android/setupwizard/carrier/PhoneMonitor;->getLteOnCdmaMode()I

    move-result v0

    const/4 v1, -0x1

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static otaspIsNeeded()Z
    .locals 1

    .prologue
    .line 76
    sget-object v0, Lcom/google/android/setupwizard/carrier/PhoneMonitor;->sInstance:Lcom/google/android/setupwizard/carrier/PhoneMonitor;

    invoke-direct {v0}, Lcom/google/android/setupwizard/carrier/PhoneMonitor;->otaspIsNeededImpl()Z

    move-result v0

    return v0
.end method

.method private otaspIsNeededImpl()Z
    .locals 2

    .prologue
    .line 469
    invoke-direct {p0}, Lcom/google/android/setupwizard/carrier/PhoneMonitor;->getOtaspModeImpl()I

    move-result v0

    const/4 v1, 0x2

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static otaspIsNotNeeded()Z
    .locals 1

    .prologue
    .line 83
    sget-object v0, Lcom/google/android/setupwizard/carrier/PhoneMonitor;->sInstance:Lcom/google/android/setupwizard/carrier/PhoneMonitor;

    invoke-direct {v0}, Lcom/google/android/setupwizard/carrier/PhoneMonitor;->otaspIsNotNeededImpl()Z

    move-result v0

    return v0
.end method

.method private otaspIsNotNeededImpl()Z
    .locals 2

    .prologue
    .line 473
    invoke-direct {p0}, Lcom/google/android/setupwizard/carrier/PhoneMonitor;->getOtaspModeImpl()I

    move-result v0

    const/4 v1, 0x3

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static otaspStateIsKnown()Z
    .locals 1

    .prologue
    .line 69
    sget-object v0, Lcom/google/android/setupwizard/carrier/PhoneMonitor;->sInstance:Lcom/google/android/setupwizard/carrier/PhoneMonitor;

    invoke-direct {v0}, Lcom/google/android/setupwizard/carrier/PhoneMonitor;->otaspStateIsKnownImpl()Z

    move-result v0

    return v0
.end method

.method private otaspStateIsKnownImpl()Z
    .locals 2

    .prologue
    .line 464
    invoke-direct {p0}, Lcom/google/android/setupwizard/carrier/PhoneMonitor;->getOtaspModeImpl()I

    move-result v0

    const/4 v1, 0x2

    if-eq v0, v1, :cond_0

    invoke-direct {p0}, Lcom/google/android/setupwizard/carrier/PhoneMonitor;->getOtaspModeImpl()I

    move-result v0

    const/4 v1, 0x3

    if-ne v0, v1, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static removeListener(Landroid/telephony/PhoneStateListener;)V
    .locals 1
    .param p0, "listener"    # Landroid/telephony/PhoneStateListener;

    .prologue
    .line 106
    sget-object v0, Lcom/google/android/setupwizard/carrier/PhoneMonitor;->sInstance:Lcom/google/android/setupwizard/carrier/PhoneMonitor;

    invoke-direct {v0, p0}, Lcom/google/android/setupwizard/carrier/PhoneMonitor;->removeListenerImpl(Landroid/telephony/PhoneStateListener;)V

    .line 107
    return-void
.end method

.method private removeListenerImpl(Landroid/telephony/PhoneStateListener;)V
    .locals 1
    .param p1, "listener"    # Landroid/telephony/PhoneStateListener;

    .prologue
    .line 455
    iget-object v0, p0, Lcom/google/android/setupwizard/carrier/PhoneMonitor;->mListeners:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->remove(Ljava/lang/Object;)Z

    .line 456
    return-void
.end method

.method public static simMissing()Z
    .locals 3

    .prologue
    const/4 v1, 0x1

    .line 166
    invoke-static {}, Lcom/google/android/setupwizard/carrier/PhoneMonitor;->getTelephony()Landroid/telephony/TelephonyManager;

    move-result-object v0

    .line 167
    .local v0, "tm":Landroid/telephony/TelephonyManager;
    if-eqz v0, :cond_1

    invoke-virtual {v0}, Landroid/telephony/TelephonyManager;->getSimState()I

    move-result v2

    if-ne v2, v1, :cond_1

    invoke-virtual {v0}, Landroid/telephony/TelephonyManager;->getCurrentPhoneType()I

    move-result v2

    if-eq v2, v1, :cond_0

    invoke-static {}, Lcom/google/android/setupwizard/carrier/PhoneMonitor;->isLTE()Z

    move-result v2

    if-eqz v2, :cond_1

    :cond_0
    :goto_0
    return v1

    :cond_1
    const/4 v1, 0x0

    goto :goto_0
.end method
