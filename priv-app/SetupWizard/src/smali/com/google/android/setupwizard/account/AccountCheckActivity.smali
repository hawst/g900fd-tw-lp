.class public Lcom/google/android/setupwizard/account/AccountCheckActivity;
.super Lcom/google/android/setupwizard/BaseActivity;
.source "AccountCheckActivity.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 26
    invoke-direct {p0}, Lcom/google/android/setupwizard/BaseActivity;-><init>()V

    return-void
.end method


# virtual methods
.method protected onCreate(Landroid/os/Bundle;)V
    .locals 1
    .param p1, "icicle"    # Landroid/os/Bundle;

    .prologue
    .line 31
    invoke-super {p0, p1}, Lcom/google/android/setupwizard/BaseActivity;->onCreate(Landroid/os/Bundle;)V

    .line 32
    invoke-virtual {p0}, Lcom/google/android/setupwizard/account/AccountCheckActivity;->hasNoAccounts()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 33
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/google/android/setupwizard/account/AccountCheckActivity;->nextScreen(I)V

    .line 39
    :goto_0
    invoke-virtual {p0}, Lcom/google/android/setupwizard/account/AccountCheckActivity;->finish()V

    .line 40
    return-void

    .line 34
    :cond_0
    invoke-virtual {p0}, Lcom/google/android/setupwizard/account/AccountCheckActivity;->hasMfmAccount()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 35
    const/4 v0, 0x4

    invoke-virtual {p0, v0}, Lcom/google/android/setupwizard/account/AccountCheckActivity;->nextScreen(I)V

    goto :goto_0

    .line 37
    :cond_1
    const/4 v0, -0x1

    invoke-virtual {p0, v0}, Lcom/google/android/setupwizard/account/AccountCheckActivity;->nextScreen(I)V

    goto :goto_0
.end method
