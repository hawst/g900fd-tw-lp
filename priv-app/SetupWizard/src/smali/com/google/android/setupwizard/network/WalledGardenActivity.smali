.class public Lcom/google/android/setupwizard/network/WalledGardenActivity;
.super Lcom/google/android/setupwizard/BaseActivity;
.source "WalledGardenActivity.java"

# interfaces
.implements Lcom/google/android/setupwizard/util/WaitForGservicesTask$WaitForGservicesListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/setupwizard/network/WalledGardenActivity$WifiSkipDialog;,
        Lcom/google/android/setupwizard/network/WalledGardenActivity$MyWebViewClient;,
        Lcom/google/android/setupwizard/network/WalledGardenActivity$MyChromeClient;
    }
.end annotation


# static fields
.field private static final GSERVICES_TIMEOUT_MS:I

.field private static final GSERVICES_USER_TIMEOUT_MS:I


# instance fields
.field private final mConnectionCheckedReceiver:Landroid/content/BroadcastReceiver;

.field private final mHandler:Landroid/os/Handler;

.field private mShowWebView:Z

.field private mWebView:Landroid/webkit/WebView;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    const v1, 0x1d4c0

    .line 69
    const-string v0, "ro.setupwizard.gservices_delay"

    invoke-static {v0, v1}, Landroid/os/SystemProperties;->getInt(Ljava/lang/String;I)I

    move-result v0

    sput v0, Lcom/google/android/setupwizard/network/WalledGardenActivity;->GSERVICES_TIMEOUT_MS:I

    .line 72
    const-string v0, "ro.setupwizard.user_gs_delay"

    invoke-static {v0, v1}, Landroid/os/SystemProperties;->getInt(Ljava/lang/String;I)I

    move-result v0

    sput v0, Lcom/google/android/setupwizard/network/WalledGardenActivity;->GSERVICES_USER_TIMEOUT_MS:I

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 51
    invoke-direct {p0}, Lcom/google/android/setupwizard/BaseActivity;-><init>()V

    .line 76
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/setupwizard/network/WalledGardenActivity;->mShowWebView:Z

    .line 77
    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    iput-object v0, p0, Lcom/google/android/setupwizard/network/WalledGardenActivity;->mHandler:Landroid/os/Handler;

    .line 79
    new-instance v0, Lcom/google/android/setupwizard/network/WalledGardenActivity$1;

    invoke-direct {v0, p0}, Lcom/google/android/setupwizard/network/WalledGardenActivity$1;-><init>(Lcom/google/android/setupwizard/network/WalledGardenActivity;)V

    iput-object v0, p0, Lcom/google/android/setupwizard/network/WalledGardenActivity;->mConnectionCheckedReceiver:Landroid/content/BroadcastReceiver;

    .line 299
    return-void
.end method

.method static synthetic access$000(Lcom/google/android/setupwizard/network/WalledGardenActivity;)V
    .locals 0
    .param p0, "x0"    # Lcom/google/android/setupwizard/network/WalledGardenActivity;

    .prologue
    .line 51
    invoke-direct {p0}, Lcom/google/android/setupwizard/network/WalledGardenActivity;->connected()V

    return-void
.end method

.method static synthetic access$100(Lcom/google/android/setupwizard/network/WalledGardenActivity;)Z
    .locals 1
    .param p0, "x0"    # Lcom/google/android/setupwizard/network/WalledGardenActivity;

    .prologue
    .line 51
    iget-boolean v0, p0, Lcom/google/android/setupwizard/network/WalledGardenActivity;->mShowWebView:Z

    return v0
.end method

.method static synthetic access$102(Lcom/google/android/setupwizard/network/WalledGardenActivity;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/google/android/setupwizard/network/WalledGardenActivity;
    .param p1, "x1"    # Z

    .prologue
    .line 51
    iput-boolean p1, p0, Lcom/google/android/setupwizard/network/WalledGardenActivity;->mShowWebView:Z

    return p1
.end method

.method static synthetic access$200(Lcom/google/android/setupwizard/network/WalledGardenActivity;I)V
    .locals 0
    .param p0, "x0"    # Lcom/google/android/setupwizard/network/WalledGardenActivity;
    .param p1, "x1"    # I

    .prologue
    .line 51
    invoke-virtual {p0, p1}, Lcom/google/android/setupwizard/network/WalledGardenActivity;->finishAction(I)V

    return-void
.end method

.method static synthetic access$500(Lcom/google/android/setupwizard/network/WalledGardenActivity;)Landroid/webkit/WebView;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/setupwizard/network/WalledGardenActivity;

    .prologue
    .line 51
    iget-object v0, p0, Lcom/google/android/setupwizard/network/WalledGardenActivity;->mWebView:Landroid/webkit/WebView;

    return-object v0
.end method

.method static synthetic access$600(Lcom/google/android/setupwizard/network/WalledGardenActivity;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/setupwizard/network/WalledGardenActivity;

    .prologue
    .line 51
    iget-object v0, p0, Lcom/google/android/setupwizard/network/WalledGardenActivity;->TAG:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$700(Lcom/google/android/setupwizard/network/WalledGardenActivity;Ljava/lang/String;)V
    .locals 0
    .param p0, "x0"    # Lcom/google/android/setupwizard/network/WalledGardenActivity;
    .param p1, "x1"    # Ljava/lang/String;

    .prologue
    .line 51
    invoke-direct {p0, p1}, Lcom/google/android/setupwizard/network/WalledGardenActivity;->onPageLoadFinished(Ljava/lang/String;)V

    return-void
.end method

.method static synthetic access$800(Lcom/google/android/setupwizard/network/WalledGardenActivity;I)V
    .locals 0
    .param p0, "x0"    # Lcom/google/android/setupwizard/network/WalledGardenActivity;
    .param p1, "x1"    # I

    .prologue
    .line 51
    invoke-virtual {p0, p1}, Lcom/google/android/setupwizard/network/WalledGardenActivity;->nextScreen(I)V

    return-void
.end method

.method private connected()V
    .locals 4

    .prologue
    .line 195
    invoke-virtual {p0}, Lcom/google/android/setupwizard/network/WalledGardenActivity;->isPrimaryUser()Z

    move-result v1

    if-eqz v1, :cond_0

    sget v0, Lcom/google/android/setupwizard/network/WalledGardenActivity;->GSERVICES_TIMEOUT_MS:I

    .line 196
    .local v0, "timeout":I
    :goto_0
    if-eqz v0, :cond_1

    .line 197
    int-to-long v2, v0

    const-string v1, "gservices_task"

    invoke-static {p0, v2, v3, v1}, Lcom/google/android/setupwizard/util/WaitForGservicesTask;->getInstanceWithTag(Landroid/app/Activity;JLjava/lang/String;)Lcom/google/android/setupwizard/util/WaitForGservicesTask;

    .line 201
    :goto_1
    return-void

    .line 195
    .end local v0    # "timeout":I
    :cond_0
    sget v0, Lcom/google/android/setupwizard/network/WalledGardenActivity;->GSERVICES_USER_TIMEOUT_MS:I

    goto :goto_0

    .line 199
    .restart local v0    # "timeout":I
    :cond_1
    const/4 v1, -0x1

    invoke-direct {p0, v1}, Lcom/google/android/setupwizard/network/WalledGardenActivity;->delayedFinish(I)V

    goto :goto_1
.end method

.method private delayedFinish(I)V
    .locals 4
    .param p1, "result"    # I

    .prologue
    .line 204
    iget-object v0, p0, Lcom/google/android/setupwizard/network/WalledGardenActivity;->mHandler:Landroid/os/Handler;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacksAndMessages(Ljava/lang/Object;)V

    .line 205
    iget-object v0, p0, Lcom/google/android/setupwizard/network/WalledGardenActivity;->mHandler:Landroid/os/Handler;

    new-instance v1, Lcom/google/android/setupwizard/network/WalledGardenActivity$2;

    invoke-direct {v1, p0, p1}, Lcom/google/android/setupwizard/network/WalledGardenActivity$2;-><init>(Lcom/google/android/setupwizard/network/WalledGardenActivity;I)V

    const-wide/16 v2, 0x3e8

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 211
    return-void
.end method

.method private onPageLoadFinished(Ljava/lang/String;)V
    .locals 0
    .param p1, "url"    # Ljava/lang/String;

    .prologue
    .line 246
    invoke-direct {p0}, Lcom/google/android/setupwizard/network/WalledGardenActivity;->verifyConnectionOrFinish()V

    .line 247
    return-void
.end method

.method private setupOptionsAndCallbacks()V
    .locals 6

    .prologue
    const/4 v5, 0x0

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 214
    iget-object v1, p0, Lcom/google/android/setupwizard/network/WalledGardenActivity;->mWebView:Landroid/webkit/WebView;

    new-instance v2, Lcom/google/android/setupwizard/network/WalledGardenActivity$MyWebViewClient;

    invoke-direct {v2, p0, v5}, Lcom/google/android/setupwizard/network/WalledGardenActivity$MyWebViewClient;-><init>(Lcom/google/android/setupwizard/network/WalledGardenActivity;Lcom/google/android/setupwizard/network/WalledGardenActivity$1;)V

    invoke-virtual {v1, v2}, Landroid/webkit/WebView;->setWebViewClient(Landroid/webkit/WebViewClient;)V

    .line 215
    iget-object v1, p0, Lcom/google/android/setupwizard/network/WalledGardenActivity;->mWebView:Landroid/webkit/WebView;

    new-instance v2, Lcom/google/android/setupwizard/network/WalledGardenActivity$MyChromeClient;

    invoke-direct {v2, p0, v5}, Lcom/google/android/setupwizard/network/WalledGardenActivity$MyChromeClient;-><init>(Lcom/google/android/setupwizard/network/WalledGardenActivity;Lcom/google/android/setupwizard/network/WalledGardenActivity$1;)V

    invoke-virtual {v1, v2}, Landroid/webkit/WebView;->setWebChromeClient(Landroid/webkit/WebChromeClient;)V

    .line 218
    iget-object v1, p0, Lcom/google/android/setupwizard/network/WalledGardenActivity;->mWebView:Landroid/webkit/WebView;

    new-instance v2, Lcom/google/android/setupwizard/network/WalledGardenActivity$3;

    invoke-direct {v2, p0}, Lcom/google/android/setupwizard/network/WalledGardenActivity$3;-><init>(Lcom/google/android/setupwizard/network/WalledGardenActivity;)V

    invoke-virtual {v1, v2}, Landroid/webkit/WebView;->setOnLongClickListener(Landroid/view/View$OnLongClickListener;)V

    .line 224
    iget-object v1, p0, Lcom/google/android/setupwizard/network/WalledGardenActivity;->mWebView:Landroid/webkit/WebView;

    invoke-virtual {v1}, Landroid/webkit/WebView;->getSettings()Landroid/webkit/WebSettings;

    move-result-object v0

    .line 225
    .local v0, "s":Landroid/webkit/WebSettings;
    invoke-virtual {v0, v4}, Landroid/webkit/WebSettings;->setJavaScriptEnabled(Z)V

    .line 226
    invoke-virtual {v0, v3}, Landroid/webkit/WebSettings;->setSupportMultipleWindows(Z)V

    .line 227
    invoke-virtual {v0, v3}, Landroid/webkit/WebSettings;->setSaveFormData(Z)V

    .line 228
    invoke-virtual {v0, v3}, Landroid/webkit/WebSettings;->setSavePassword(Z)V

    .line 229
    invoke-virtual {v0, v3}, Landroid/webkit/WebSettings;->setAllowFileAccess(Z)V

    .line 230
    invoke-virtual {v0, v3}, Landroid/webkit/WebSettings;->setDatabaseEnabled(Z)V

    .line 231
    invoke-virtual {v0, v3}, Landroid/webkit/WebSettings;->setJavaScriptCanOpenWindowsAutomatically(Z)V

    .line 232
    invoke-virtual {v0, v4}, Landroid/webkit/WebSettings;->setLoadsImagesAutomatically(Z)V

    .line 233
    invoke-virtual {v0, v3}, Landroid/webkit/WebSettings;->setNeedInitialFocus(Z)V

    .line 234
    invoke-virtual {v0, v4}, Landroid/webkit/WebSettings;->setUseWideViewPort(Z)V

    .line 235
    invoke-virtual {v0, v4}, Landroid/webkit/WebSettings;->setLoadWithOverviewMode(Z)V

    .line 236
    invoke-virtual {v0, v4}, Landroid/webkit/WebSettings;->setSupportZoom(Z)V

    .line 237
    invoke-virtual {v0, v4}, Landroid/webkit/WebSettings;->setBuiltInZoomControls(Z)V

    .line 238
    invoke-virtual {v0, v3}, Landroid/webkit/WebSettings;->setDisplayZoomControls(Z)V

    .line 240
    iget-object v1, p0, Lcom/google/android/setupwizard/network/WalledGardenActivity;->mWebView:Landroid/webkit/WebView;

    invoke-virtual {v1, v4}, Landroid/webkit/WebView;->setFocusable(Z)V

    .line 241
    iget-object v1, p0, Lcom/google/android/setupwizard/network/WalledGardenActivity;->mWebView:Landroid/webkit/WebView;

    invoke-virtual {v1, v4}, Landroid/webkit/WebView;->setFocusableInTouchMode(Z)V

    .line 242
    iget-object v1, p0, Lcom/google/android/setupwizard/network/WalledGardenActivity;->mWebView:Landroid/webkit/WebView;

    invoke-virtual {v1, v3}, Landroid/webkit/WebView;->setLongClickable(Z)V

    .line 243
    return-void
.end method

.method private verifyConnectionOrFinish()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 183
    invoke-static {}, Lcom/google/android/setupwizard/network/NetworkMonitor;->getInstance()Lcom/google/android/setupwizard/network/NetworkMonitor;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/setupwizard/network/NetworkMonitor;->checkIsNetworkConnected()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 184
    const/4 v0, 0x1

    invoke-static {p0, v1, v0}, Lcom/google/android/setupwizard/network/CheckConnectionTask;->checkConnection(Landroid/content/Context;ZZ)V

    .line 189
    :goto_0
    return-void

    .line 187
    :cond_0
    invoke-virtual {p0, v1}, Lcom/google/android/setupwizard/network/WalledGardenActivity;->finishAction(I)V

    goto :goto_0
.end method


# virtual methods
.method protected getPreviousTransition()I
    .locals 1

    .prologue
    .line 177
    const/4 v0, 0x2

    return v0
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 3
    .param p1, "icicle"    # Landroid/os/Bundle;

    .prologue
    .line 113
    invoke-super {p0, p1}, Lcom/google/android/setupwizard/BaseActivity;->onCreate(Landroid/os/Bundle;)V

    .line 115
    if-eqz p1, :cond_0

    const-string v1, "showWebView"

    invoke-virtual {p1, v1}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v1, 0x1

    :goto_0
    iput-boolean v1, p0, Lcom/google/android/setupwizard/network/WalledGardenActivity;->mShowWebView:Z

    .line 116
    iget-boolean v1, p0, Lcom/google/android/setupwizard/network/WalledGardenActivity;->mShowWebView:Z

    if-eqz v1, :cond_2

    .line 117
    const v1, 0x7f03001f

    const v2, 0x7f030018

    invoke-virtual {p0, v1, v2}, Lcom/google/android/setupwizard/network/WalledGardenActivity;->setTemplateContent(II)V

    .line 118
    const v1, 0x7f0e0062

    invoke-virtual {p0, v1}, Lcom/google/android/setupwizard/network/WalledGardenActivity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/webkit/WebView;

    iput-object v1, p0, Lcom/google/android/setupwizard/network/WalledGardenActivity;->mWebView:Landroid/webkit/WebView;

    .line 119
    iget-object v1, p0, Lcom/google/android/setupwizard/network/WalledGardenActivity;->mWebView:Landroid/webkit/WebView;

    invoke-virtual {v1, p0}, Landroid/webkit/WebView;->setOnApplyWindowInsetsListener(Landroid/view/View$OnApplyWindowInsetsListener;)V

    .line 121
    invoke-direct {p0}, Lcom/google/android/setupwizard/network/WalledGardenActivity;->setupOptionsAndCallbacks()V

    .line 123
    const-string v1, "webViewState"

    invoke-virtual {p1, v1}, Landroid/os/Bundle;->getBundle(Ljava/lang/String;)Landroid/os/Bundle;

    move-result-object v0

    .line 124
    .local v0, "webViewState":Landroid/os/Bundle;
    if-eqz v0, :cond_1

    invoke-virtual {v0}, Landroid/os/Bundle;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_1

    .line 125
    iget-object v1, p0, Lcom/google/android/setupwizard/network/WalledGardenActivity;->mWebView:Landroid/webkit/WebView;

    invoke-virtual {v1, v0}, Landroid/webkit/WebView;->restoreState(Landroid/os/Bundle;)Landroid/webkit/WebBackForwardList;

    .line 136
    .end local v0    # "webViewState":Landroid/os/Bundle;
    :goto_1
    return-void

    .line 115
    :cond_0
    const/4 v1, 0x0

    goto :goto_0

    .line 129
    .restart local v0    # "webViewState":Landroid/os/Bundle;
    :cond_1
    iget-object v1, p0, Lcom/google/android/setupwizard/network/WalledGardenActivity;->mWebView:Landroid/webkit/WebView;

    invoke-static {p0}, Lcom/google/android/setupwizard/network/CheckConnectionTask;->getCheckConnectionUrl(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/webkit/WebView;->loadUrl(Ljava/lang/String;)V

    goto :goto_1

    .line 133
    .end local v0    # "webViewState":Landroid/os/Bundle;
    :cond_2
    const v1, 0x7f03001e

    const v2, 0x7f030023

    invoke-virtual {p0, v1, v2}, Lcom/google/android/setupwizard/network/WalledGardenActivity;->setTemplateContent(II)V

    .line 134
    const v1, 0x7f070087

    invoke-virtual {p0, v1}, Lcom/google/android/setupwizard/network/WalledGardenActivity;->setHeaderText(I)V

    goto :goto_1
.end method

.method public onGservicesReady()V
    .locals 1

    .prologue
    .line 101
    const/4 v0, -0x1

    invoke-direct {p0, v0}, Lcom/google/android/setupwizard/network/WalledGardenActivity;->delayedFinish(I)V

    .line 102
    return-void
.end method

.method public onGservicesTimeout()V
    .locals 2

    .prologue
    .line 106
    iget-object v0, p0, Lcom/google/android/setupwizard/network/WalledGardenActivity;->TAG:Ljava/lang/String;

    const-string v1, "Gservices timed out"

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 108
    const/4 v0, -0x1

    invoke-direct {p0, v0}, Lcom/google/android/setupwizard/network/WalledGardenActivity;->delayedFinish(I)V

    .line 109
    return-void
.end method

.method public onNavigationBarCreated(Lcom/android/setupwizard/navigationbar/SetupWizardNavBar;)V
    .locals 2
    .param p1, "bar"    # Lcom/android/setupwizard/navigationbar/SetupWizardNavBar;

    .prologue
    .line 165
    invoke-super {p0, p1}, Lcom/google/android/setupwizard/BaseActivity;->onNavigationBarCreated(Lcom/android/setupwizard/navigationbar/SetupWizardNavBar;)V

    .line 166
    invoke-virtual {p1}, Lcom/android/setupwizard/navigationbar/SetupWizardNavBar;->getNextButton()Landroid/widget/Button;

    move-result-object v0

    const v1, 0x7f070046

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setText(I)V

    .line 167
    invoke-virtual {p1}, Lcom/android/setupwizard/navigationbar/SetupWizardNavBar;->getNextButton()Landroid/widget/Button;

    move-result-object v1

    iget-boolean v0, p0, Lcom/google/android/setupwizard/network/WalledGardenActivity;->mShowWebView:Z

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/google/android/setupwizard/network/WalledGardenActivity;->isNetworkRequired()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-virtual {v1, v0}, Landroid/widget/Button;->setEnabled(Z)V

    .line 168
    return-void

    .line 167
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected onPause()V
    .locals 2

    .prologue
    .line 140
    iget-object v0, p0, Lcom/google/android/setupwizard/network/WalledGardenActivity;->mHandler:Landroid/os/Handler;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacksAndMessages(Ljava/lang/Object;)V

    .line 141
    iget-object v0, p0, Lcom/google/android/setupwizard/network/WalledGardenActivity;->mConnectionCheckedReceiver:Landroid/content/BroadcastReceiver;

    invoke-static {p0, v0}, Lcom/google/android/setupwizard/network/CheckConnectionTask;->unregister(Landroid/content/Context;Landroid/content/BroadcastReceiver;)V

    .line 142
    invoke-super {p0}, Lcom/google/android/setupwizard/BaseActivity;->onPause()V

    .line 143
    return-void
.end method

.method protected onResume()V
    .locals 1

    .prologue
    .line 147
    invoke-super {p0}, Lcom/google/android/setupwizard/BaseActivity;->onResume()V

    .line 148
    iget-object v0, p0, Lcom/google/android/setupwizard/network/WalledGardenActivity;->mConnectionCheckedReceiver:Landroid/content/BroadcastReceiver;

    invoke-static {p0, v0}, Lcom/google/android/setupwizard/network/CheckConnectionTask;->register(Landroid/content/Context;Landroid/content/BroadcastReceiver;)V

    .line 149
    invoke-direct {p0}, Lcom/google/android/setupwizard/network/WalledGardenActivity;->verifyConnectionOrFinish()V

    .line 150
    return-void
.end method

.method protected onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 3
    .param p1, "outState"    # Landroid/os/Bundle;

    .prologue
    .line 154
    iget-object v1, p0, Lcom/google/android/setupwizard/network/WalledGardenActivity;->mWebView:Landroid/webkit/WebView;

    if-eqz v1, :cond_0

    .line 155
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 156
    .local v0, "webViewState":Landroid/os/Bundle;
    iget-object v1, p0, Lcom/google/android/setupwizard/network/WalledGardenActivity;->mWebView:Landroid/webkit/WebView;

    invoke-virtual {v1, v0}, Landroid/webkit/WebView;->saveState(Landroid/os/Bundle;)Landroid/webkit/WebBackForwardList;

    .line 157
    const-string v1, "webViewState"

    invoke-virtual {p1, v1, v0}, Landroid/os/Bundle;->putBundle(Ljava/lang/String;Landroid/os/Bundle;)V

    .line 159
    .end local v0    # "webViewState":Landroid/os/Bundle;
    :cond_0
    const-string v1, "showWebView"

    iget-boolean v2, p0, Lcom/google/android/setupwizard/network/WalledGardenActivity;->mShowWebView:Z

    invoke-virtual {p1, v1, v2}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 160
    invoke-super {p0, p1}, Lcom/google/android/setupwizard/BaseActivity;->onSaveInstanceState(Landroid/os/Bundle;)V

    .line 161
    return-void
.end method

.method protected start()V
    .locals 3

    .prologue
    .line 172
    new-instance v0, Lcom/google/android/setupwizard/network/WalledGardenActivity$WifiSkipDialog;

    invoke-direct {v0}, Lcom/google/android/setupwizard/network/WalledGardenActivity$WifiSkipDialog;-><init>()V

    invoke-virtual {p0}, Lcom/google/android/setupwizard/network/WalledGardenActivity;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Lcom/google/android/setupwizard/network/WalledGardenActivity$WifiSkipDialog;->show(Landroid/app/FragmentManager;Ljava/lang/String;)V

    .line 173
    return-void
.end method
