.class public Lcom/google/android/setupwizard/accessibility/AccessibilityGestureHelper;
.super Ljava/lang/Object;
.source "AccessibilityGestureHelper.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/setupwizard/accessibility/AccessibilityGestureHelper$AccessibilityCallback;
    }
.end annotation


# instance fields
.field private mAccessibilityEnabled:Z

.field private mCallback:Lcom/google/android/setupwizard/accessibility/AccessibilityGestureHelper$AccessibilityCallback;

.field private final mContext:Landroid/content/Context;

.field private mFinishedWarning:Z

.field private final mHandler:Landroid/os/Handler;

.field private final mLongPressDetector:Lcom/google/android/setupwizard/accessibility/MultitouchLongPressDetector;

.field private final mLongPressListener:Lcom/google/android/setupwizard/accessibility/MultitouchLongPressDetector$LongPressListener;

.field private mStartedWarning:Z

.field private final mTone:Landroid/media/Ringtone;

.field private final mTts:Landroid/speech/tts/TextToSpeech;

.field private final mTtsInitListener:Landroid/speech/tts/TextToSpeech$OnInitListener;

.field private final mTtsProgressListener:Landroid/speech/tts/UtteranceProgressListener;

.field private mTtsReady:Z


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 8
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    const/4 v3, 0x1

    const/4 v4, 0x0

    .line 81
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 282
    new-instance v5, Lcom/google/android/setupwizard/accessibility/AccessibilityGestureHelper$2;

    invoke-direct {v5, p0}, Lcom/google/android/setupwizard/accessibility/AccessibilityGestureHelper$2;-><init>(Lcom/google/android/setupwizard/accessibility/AccessibilityGestureHelper;)V

    iput-object v5, p0, Lcom/google/android/setupwizard/accessibility/AccessibilityGestureHelper;->mLongPressListener:Lcom/google/android/setupwizard/accessibility/MultitouchLongPressDetector$LongPressListener;

    .line 297
    new-instance v5, Landroid/os/Handler;

    invoke-direct {v5}, Landroid/os/Handler;-><init>()V

    iput-object v5, p0, Lcom/google/android/setupwizard/accessibility/AccessibilityGestureHelper;->mHandler:Landroid/os/Handler;

    .line 302
    new-instance v5, Lcom/google/android/setupwizard/accessibility/AccessibilityGestureHelper$3;

    invoke-direct {v5, p0}, Lcom/google/android/setupwizard/accessibility/AccessibilityGestureHelper$3;-><init>(Lcom/google/android/setupwizard/accessibility/AccessibilityGestureHelper;)V

    iput-object v5, p0, Lcom/google/android/setupwizard/accessibility/AccessibilityGestureHelper;->mTtsInitListener:Landroid/speech/tts/TextToSpeech$OnInitListener;

    .line 312
    new-instance v5, Lcom/google/android/setupwizard/accessibility/AccessibilityGestureHelper$4;

    invoke-direct {v5, p0}, Lcom/google/android/setupwizard/accessibility/AccessibilityGestureHelper$4;-><init>(Lcom/google/android/setupwizard/accessibility/AccessibilityGestureHelper;)V

    iput-object v5, p0, Lcom/google/android/setupwizard/accessibility/AccessibilityGestureHelper;->mTtsProgressListener:Landroid/speech/tts/UtteranceProgressListener;

    .line 82
    invoke-virtual {p1}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v5

    iput-object v5, p0, Lcom/google/android/setupwizard/accessibility/AccessibilityGestureHelper;->mContext:Landroid/content/Context;

    .line 84
    new-instance v5, Landroid/speech/tts/TextToSpeech;

    iget-object v6, p0, Lcom/google/android/setupwizard/accessibility/AccessibilityGestureHelper;->mContext:Landroid/content/Context;

    iget-object v7, p0, Lcom/google/android/setupwizard/accessibility/AccessibilityGestureHelper;->mTtsInitListener:Landroid/speech/tts/TextToSpeech$OnInitListener;

    invoke-direct {v5, v6, v7}, Landroid/speech/tts/TextToSpeech;-><init>(Landroid/content/Context;Landroid/speech/tts/TextToSpeech$OnInitListener;)V

    iput-object v5, p0, Lcom/google/android/setupwizard/accessibility/AccessibilityGestureHelper;->mTts:Landroid/speech/tts/TextToSpeech;

    .line 85
    iget-object v5, p0, Lcom/google/android/setupwizard/accessibility/AccessibilityGestureHelper;->mTts:Landroid/speech/tts/TextToSpeech;

    iget-object v6, p0, Lcom/google/android/setupwizard/accessibility/AccessibilityGestureHelper;->mTtsProgressListener:Landroid/speech/tts/UtteranceProgressListener;

    invoke-virtual {v5, v6}, Landroid/speech/tts/TextToSpeech;->setOnUtteranceProgressListener(Landroid/speech/tts/UtteranceProgressListener;)I

    .line 87
    iget-object v5, p0, Lcom/google/android/setupwizard/accessibility/AccessibilityGestureHelper;->mContext:Landroid/content/Context;

    sget-object v6, Landroid/provider/Settings$System;->DEFAULT_NOTIFICATION_URI:Landroid/net/Uri;

    invoke-static {v5, v6}, Landroid/media/RingtoneManager;->getRingtone(Landroid/content/Context;Landroid/net/Uri;)Landroid/media/Ringtone;

    move-result-object v5

    iput-object v5, p0, Lcom/google/android/setupwizard/accessibility/AccessibilityGestureHelper;->mTone:Landroid/media/Ringtone;

    .line 88
    iget-object v5, p0, Lcom/google/android/setupwizard/accessibility/AccessibilityGestureHelper;->mTone:Landroid/media/Ringtone;

    if-eqz v5, :cond_0

    .line 89
    iget-object v5, p0, Lcom/google/android/setupwizard/accessibility/AccessibilityGestureHelper;->mTone:Landroid/media/Ringtone;

    const/4 v6, 0x3

    invoke-virtual {v5, v6}, Landroid/media/Ringtone;->setStreamType(I)V

    .line 92
    :cond_0
    iget-object v5, p0, Lcom/google/android/setupwizard/accessibility/AccessibilityGestureHelper;->mContext:Landroid/content/Context;

    invoke-virtual {v5}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    .line 93
    .local v1, "resolver":Landroid/content/ContentResolver;
    iget-object v5, p0, Lcom/google/android/setupwizard/accessibility/AccessibilityGestureHelper;->mContext:Landroid/content/Context;

    invoke-virtual {v5}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    .line 94
    .local v0, "res":Landroid/content/res/Resources;
    const v5, 0x7f0a0039

    invoke-virtual {v0, v5}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v2

    .line 96
    .local v2, "touchSlop":I
    new-instance v5, Lcom/google/android/setupwizard/accessibility/MultitouchLongPressDetector;

    const/16 v6, 0x7d0

    const/4 v7, 0x2

    invoke-direct {v5, v6, v7, v2}, Lcom/google/android/setupwizard/accessibility/MultitouchLongPressDetector;-><init>(III)V

    iput-object v5, p0, Lcom/google/android/setupwizard/accessibility/AccessibilityGestureHelper;->mLongPressDetector:Lcom/google/android/setupwizard/accessibility/MultitouchLongPressDetector;

    .line 98
    iget-object v5, p0, Lcom/google/android/setupwizard/accessibility/AccessibilityGestureHelper;->mLongPressDetector:Lcom/google/android/setupwizard/accessibility/MultitouchLongPressDetector;

    iget-object v6, p0, Lcom/google/android/setupwizard/accessibility/AccessibilityGestureHelper;->mLongPressListener:Lcom/google/android/setupwizard/accessibility/MultitouchLongPressDetector$LongPressListener;

    invoke-virtual {v5, v6}, Lcom/google/android/setupwizard/accessibility/MultitouchLongPressDetector;->setListener(Lcom/google/android/setupwizard/accessibility/MultitouchLongPressDetector$LongPressListener;)V

    .line 100
    const-string v5, "accessibility_enabled"

    invoke-static {v1, v5, v4}, Landroid/provider/Settings$Secure;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v5

    if-ne v5, v3, :cond_1

    :goto_0
    iput-boolean v3, p0, Lcom/google/android/setupwizard/accessibility/AccessibilityGestureHelper;->mAccessibilityEnabled:Z

    .line 102
    return-void

    :cond_1
    move v3, v4

    .line 100
    goto :goto_0
.end method

.method static synthetic access$000(Lcom/google/android/setupwizard/accessibility/AccessibilityGestureHelper;)V
    .locals 0
    .param p0, "x0"    # Lcom/google/android/setupwizard/accessibility/AccessibilityGestureHelper;

    .prologue
    .line 46
    invoke-direct {p0}, Lcom/google/android/setupwizard/accessibility/AccessibilityGestureHelper;->afterWarningDelay()V

    return-void
.end method

.method static synthetic access$100(Lcom/google/android/setupwizard/accessibility/AccessibilityGestureHelper;)Z
    .locals 1
    .param p0, "x0"    # Lcom/google/android/setupwizard/accessibility/AccessibilityGestureHelper;

    .prologue
    .line 46
    iget-boolean v0, p0, Lcom/google/android/setupwizard/accessibility/AccessibilityGestureHelper;->mAccessibilityEnabled:Z

    return v0
.end method

.method static synthetic access$200(Lcom/google/android/setupwizard/accessibility/AccessibilityGestureHelper;)Z
    .locals 1
    .param p0, "x0"    # Lcom/google/android/setupwizard/accessibility/AccessibilityGestureHelper;

    .prologue
    .line 46
    iget-boolean v0, p0, Lcom/google/android/setupwizard/accessibility/AccessibilityGestureHelper;->mStartedWarning:Z

    return v0
.end method

.method static synthetic access$202(Lcom/google/android/setupwizard/accessibility/AccessibilityGestureHelper;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/google/android/setupwizard/accessibility/AccessibilityGestureHelper;
    .param p1, "x1"    # Z

    .prologue
    .line 46
    iput-boolean p1, p0, Lcom/google/android/setupwizard/accessibility/AccessibilityGestureHelper;->mStartedWarning:Z

    return p1
.end method

.method static synthetic access$300(Lcom/google/android/setupwizard/accessibility/AccessibilityGestureHelper;)V
    .locals 0
    .param p0, "x0"    # Lcom/google/android/setupwizard/accessibility/AccessibilityGestureHelper;

    .prologue
    .line 46
    invoke-direct {p0}, Lcom/google/android/setupwizard/accessibility/AccessibilityGestureHelper;->speakWarning()V

    return-void
.end method

.method static synthetic access$400(Lcom/google/android/setupwizard/accessibility/AccessibilityGestureHelper;)Z
    .locals 1
    .param p0, "x0"    # Lcom/google/android/setupwizard/accessibility/AccessibilityGestureHelper;

    .prologue
    .line 46
    iget-boolean v0, p0, Lcom/google/android/setupwizard/accessibility/AccessibilityGestureHelper;->mFinishedWarning:Z

    return v0
.end method

.method static synthetic access$500(Lcom/google/android/setupwizard/accessibility/AccessibilityGestureHelper;)V
    .locals 0
    .param p0, "x0"    # Lcom/google/android/setupwizard/accessibility/AccessibilityGestureHelper;

    .prologue
    .line 46
    invoke-direct {p0}, Lcom/google/android/setupwizard/accessibility/AccessibilityGestureHelper;->enableAllAccessibilityServices()V

    return-void
.end method

.method static synthetic access$602(Lcom/google/android/setupwizard/accessibility/AccessibilityGestureHelper;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/google/android/setupwizard/accessibility/AccessibilityGestureHelper;
    .param p1, "x1"    # Z

    .prologue
    .line 46
    iput-boolean p1, p0, Lcom/google/android/setupwizard/accessibility/AccessibilityGestureHelper;->mTtsReady:Z

    return p1
.end method

.method static synthetic access$700(Lcom/google/android/setupwizard/accessibility/AccessibilityGestureHelper;)V
    .locals 0
    .param p0, "x0"    # Lcom/google/android/setupwizard/accessibility/AccessibilityGestureHelper;

    .prologue
    .line 46
    invoke-direct {p0}, Lcom/google/android/setupwizard/accessibility/AccessibilityGestureHelper;->onWarningSpoken()V

    return-void
.end method

.method private afterWarningDelay()V
    .locals 2

    .prologue
    .line 260
    iget-object v0, p0, Lcom/google/android/setupwizard/accessibility/AccessibilityGestureHelper;->mLongPressDetector:Lcom/google/android/setupwizard/accessibility/MultitouchLongPressDetector;

    invoke-virtual {v0}, Lcom/google/android/setupwizard/accessibility/MultitouchLongPressDetector;->getPointerCount()I

    move-result v0

    const/4 v1, 0x2

    if-lt v0, v1, :cond_0

    iget-boolean v0, p0, Lcom/google/android/setupwizard/accessibility/AccessibilityGestureHelper;->mAccessibilityEnabled:Z

    if-nez v0, :cond_0

    .line 262
    invoke-direct {p0}, Lcom/google/android/setupwizard/accessibility/AccessibilityGestureHelper;->enableAllAccessibilityServices()V

    .line 265
    :cond_0
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/setupwizard/accessibility/AccessibilityGestureHelper;->mFinishedWarning:Z

    .line 266
    return-void
.end method

.method private enableAllAccessibilityServices()V
    .locals 7

    .prologue
    const/4 v6, 0x1

    .line 155
    iget-object v2, p0, Lcom/google/android/setupwizard/accessibility/AccessibilityGestureHelper;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    .line 156
    .local v0, "resolver":Landroid/content/ContentResolver;
    iget-object v2, p0, Lcom/google/android/setupwizard/accessibility/AccessibilityGestureHelper;->mContext:Landroid/content/Context;

    const/4 v3, 0x7

    invoke-direct {p0, v2, v3}, Lcom/google/android/setupwizard/accessibility/AccessibilityGestureHelper;->getAccessibilityServicesFiltered(Landroid/content/Context;I)Ljava/lang/String;

    move-result-object v1

    .line 160
    .local v1, "servicesToEnable":Ljava/lang/String;
    const-string v2, "enabled_accessibility_services"

    invoke-static {v0, v2, v1}, Landroid/provider/Settings$Secure;->putString(Landroid/content/ContentResolver;Ljava/lang/String;Ljava/lang/String;)Z

    .line 164
    const-string v2, "touch_exploration_granted_accessibility_services"

    invoke-static {v0, v2, v1}, Landroid/provider/Settings$Secure;->putString(Landroid/content/ContentResolver;Ljava/lang/String;Ljava/lang/String;)Z

    .line 169
    const-string v2, "touch_exploration_enabled"

    invoke-static {v0, v2, v6}, Landroid/provider/Settings$Secure;->putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z

    .line 173
    const-string v2, "accessibility_enabled"

    invoke-static {v0, v2, v6}, Landroid/provider/Settings$Secure;->putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z

    .line 176
    const-string v2, "accessibility_script_injection"

    invoke-static {v0, v2, v6}, Landroid/provider/Settings$Secure;->putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z

    .line 179
    const-string v2, "enable_accessibility_global_gesture_enabled"

    invoke-static {v0, v2, v6}, Landroid/provider/Settings$Global;->putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z

    .line 182
    iput-boolean v6, p0, Lcom/google/android/setupwizard/accessibility/AccessibilityGestureHelper;->mAccessibilityEnabled:Z

    .line 184
    iget-object v2, p0, Lcom/google/android/setupwizard/accessibility/AccessibilityGestureHelper;->mTone:Landroid/media/Ringtone;

    if-eqz v2, :cond_0

    .line 185
    iget-object v2, p0, Lcom/google/android/setupwizard/accessibility/AccessibilityGestureHelper;->mTone:Landroid/media/Ringtone;

    invoke-virtual {v2}, Landroid/media/Ringtone;->play()V

    .line 188
    :cond_0
    iget-object v2, p0, Lcom/google/android/setupwizard/accessibility/AccessibilityGestureHelper;->mTts:Landroid/speech/tts/TextToSpeech;

    iget-object v3, p0, Lcom/google/android/setupwizard/accessibility/AccessibilityGestureHelper;->mContext:Landroid/content/Context;

    const v4, 0x7f07009f

    invoke-virtual {v3, v4}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v3

    const/4 v4, 0x0

    const/4 v5, 0x0

    invoke-virtual {v2, v3, v4, v5}, Landroid/speech/tts/TextToSpeech;->speak(Ljava/lang/String;ILjava/util/HashMap;)I

    .line 190
    iget-object v2, p0, Lcom/google/android/setupwizard/accessibility/AccessibilityGestureHelper;->mCallback:Lcom/google/android/setupwizard/accessibility/AccessibilityGestureHelper$AccessibilityCallback;

    if-eqz v2, :cond_1

    .line 191
    iget-object v2, p0, Lcom/google/android/setupwizard/accessibility/AccessibilityGestureHelper;->mCallback:Lcom/google/android/setupwizard/accessibility/AccessibilityGestureHelper$AccessibilityCallback;

    invoke-interface {v2, v6}, Lcom/google/android/setupwizard/accessibility/AccessibilityGestureHelper$AccessibilityCallback;->onAccessibilityStateChanged(Z)V

    .line 193
    :cond_1
    return-void
.end method

.method private getAccessibilityServicesFiltered(Landroid/content/Context;I)Ljava/lang/String;
    .locals 9
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "requiredFeedbackTypes"    # I

    .prologue
    .line 207
    const-string v7, "accessibility"

    invoke-virtual {p1, v7}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Landroid/view/accessibility/AccessibilityManager;

    .line 209
    .local v4, "manager":Landroid/view/accessibility/AccessibilityManager;
    invoke-virtual {v4}, Landroid/view/accessibility/AccessibilityManager;->getInstalledAccessibilityServiceList()Ljava/util/List;

    move-result-object v1

    .line 211
    .local v1, "accessibilityServices":Ljava/util/List;, "Ljava/util/List<Landroid/accessibilityservice/AccessibilityServiceInfo;>;"
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    .line 213
    .local v6, "servicesToEnable":Ljava/lang/StringBuilder;
    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    .local v3, "i$":Ljava/util/Iterator;
    :cond_0
    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v7

    if-eqz v7, :cond_1

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/accessibilityservice/AccessibilityServiceInfo;

    .line 214
    .local v0, "accessibilityService":Landroid/accessibilityservice/AccessibilityServiceInfo;
    iget v7, v0, Landroid/accessibilityservice/AccessibilityServiceInfo;->feedbackType:I

    and-int/2addr v7, p2

    if-eqz v7, :cond_0

    .line 218
    invoke-virtual {v0}, Landroid/accessibilityservice/AccessibilityServiceInfo;->getResolveInfo()Landroid/content/pm/ResolveInfo;

    move-result-object v7

    iget-object v5, v7, Landroid/content/pm/ResolveInfo;->serviceInfo:Landroid/content/pm/ServiceInfo;

    .line 219
    .local v5, "serviceInfo":Landroid/content/pm/ServiceInfo;
    new-instance v2, Landroid/content/ComponentName;

    iget-object v7, v5, Landroid/content/pm/ServiceInfo;->packageName:Ljava/lang/String;

    iget-object v8, v5, Landroid/content/pm/ServiceInfo;->name:Ljava/lang/String;

    invoke-direct {v2, v7, v8}, Landroid/content/ComponentName;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 222
    .local v2, "componentName":Landroid/content/ComponentName;
    invoke-virtual {v2}, Landroid/content/ComponentName;->flattenToString()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 223
    const/16 v7, 0x3a

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    goto :goto_0

    .line 226
    .end local v0    # "accessibilityService":Landroid/accessibilityservice/AccessibilityServiceInfo;
    .end local v2    # "componentName":Landroid/content/ComponentName;
    .end local v5    # "serviceInfo":Landroid/content/pm/ServiceInfo;
    :cond_1
    invoke-virtual {v6}, Ljava/lang/StringBuilder;->length()I

    move-result v7

    if-lez v7, :cond_2

    .line 227
    invoke-virtual {v6}, Ljava/lang/StringBuilder;->length()I

    move-result v7

    add-int/lit8 v7, v7, -0x1

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->deleteCharAt(I)Ljava/lang/StringBuilder;

    .line 230
    :cond_2
    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    return-object v7
.end method

.method private onWarningSpoken()V
    .locals 4

    .prologue
    .line 248
    iget-object v0, p0, Lcom/google/android/setupwizard/accessibility/AccessibilityGestureHelper;->mHandler:Landroid/os/Handler;

    new-instance v1, Lcom/google/android/setupwizard/accessibility/AccessibilityGestureHelper$1;

    invoke-direct {v1, p0}, Lcom/google/android/setupwizard/accessibility/AccessibilityGestureHelper$1;-><init>(Lcom/google/android/setupwizard/accessibility/AccessibilityGestureHelper;)V

    const-wide/16 v2, 0x7d0

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 254
    return-void
.end method

.method private speakWarning()V
    .locals 4

    .prologue
    .line 237
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    .line 238
    .local v0, "params":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/String;Ljava/lang/String;>;"
    const-string v2, "utteranceId"

    const-string v3, "continueToEnable"

    invoke-virtual {v0, v2, v3}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 240
    iget-object v2, p0, Lcom/google/android/setupwizard/accessibility/AccessibilityGestureHelper;->mContext:Landroid/content/Context;

    const v3, 0x7f07009d

    invoke-virtual {v2, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 241
    .local v1, "text":Ljava/lang/String;
    iget-object v2, p0, Lcom/google/android/setupwizard/accessibility/AccessibilityGestureHelper;->mTts:Landroid/speech/tts/TextToSpeech;

    const/4 v3, 0x0

    invoke-virtual {v2, v1, v3, v0}, Landroid/speech/tts/TextToSpeech;->speak(Ljava/lang/String;ILjava/util/HashMap;)I

    .line 242
    return-void
.end method


# virtual methods
.method public dispatchTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 1
    .param p1, "event"    # Landroid/view/MotionEvent;

    .prologue
    .line 130
    iget-object v0, p0, Lcom/google/android/setupwizard/accessibility/AccessibilityGestureHelper;->mLongPressDetector:Lcom/google/android/setupwizard/accessibility/MultitouchLongPressDetector;

    invoke-virtual {v0, p1}, Lcom/google/android/setupwizard/accessibility/MultitouchLongPressDetector;->onTouchEvent(Landroid/view/MotionEvent;)Z

    move-result v0

    return v0
.end method

.method public isAccessibilityEnabled()Z
    .locals 1

    .prologue
    .line 119
    iget-boolean v0, p0, Lcom/google/android/setupwizard/accessibility/AccessibilityGestureHelper;->mAccessibilityEnabled:Z

    return v0
.end method

.method public isTtsLocaleAvailable(Ljava/util/Locale;)Z
    .locals 3
    .param p1, "locale"    # Ljava/util/Locale;

    .prologue
    const/4 v1, 0x1

    .line 142
    iget-object v2, p0, Lcom/google/android/setupwizard/accessibility/AccessibilityGestureHelper;->mTts:Landroid/speech/tts/TextToSpeech;

    invoke-virtual {v2, p1}, Landroid/speech/tts/TextToSpeech;->isLanguageAvailable(Ljava/util/Locale;)I

    move-result v0

    .line 143
    .local v0, "result":I
    if-eq v0, v1, :cond_0

    const/4 v2, 0x2

    if-ne v0, v2, :cond_1

    :cond_0
    :goto_0
    return v1

    :cond_1
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public onDestroy()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 108
    iput-object v1, p0, Lcom/google/android/setupwizard/accessibility/AccessibilityGestureHelper;->mCallback:Lcom/google/android/setupwizard/accessibility/AccessibilityGestureHelper$AccessibilityCallback;

    .line 109
    iget-object v0, p0, Lcom/google/android/setupwizard/accessibility/AccessibilityGestureHelper;->mTts:Landroid/speech/tts/TextToSpeech;

    invoke-virtual {v0, v1}, Landroid/speech/tts/TextToSpeech;->setOnUtteranceProgressListener(Landroid/speech/tts/UtteranceProgressListener;)I

    .line 110
    iget-object v0, p0, Lcom/google/android/setupwizard/accessibility/AccessibilityGestureHelper;->mTts:Landroid/speech/tts/TextToSpeech;

    invoke-virtual {v0, v1}, Landroid/speech/tts/TextToSpeech;->setOnUtteranceCompletedListener(Landroid/speech/tts/TextToSpeech$OnUtteranceCompletedListener;)I

    .line 111
    iget-object v0, p0, Lcom/google/android/setupwizard/accessibility/AccessibilityGestureHelper;->mTts:Landroid/speech/tts/TextToSpeech;

    invoke-virtual {v0}, Landroid/speech/tts/TextToSpeech;->shutdown()V

    .line 112
    return-void
.end method

.method public setCallback(Lcom/google/android/setupwizard/accessibility/AccessibilityGestureHelper$AccessibilityCallback;)V
    .locals 0
    .param p1, "callback"    # Lcom/google/android/setupwizard/accessibility/AccessibilityGestureHelper$AccessibilityCallback;

    .prologue
    .line 115
    iput-object p1, p0, Lcom/google/android/setupwizard/accessibility/AccessibilityGestureHelper;->mCallback:Lcom/google/android/setupwizard/accessibility/AccessibilityGestureHelper$AccessibilityCallback;

    .line 116
    return-void
.end method
