.class public Lcom/google/android/setupwizard/time/DateTimeSetupActivity$TimeDialog;
.super Landroid/app/DialogFragment;
.source "DateTimeSetupActivity.java"

# interfaces
.implements Landroid/app/TimePickerDialog$OnTimeSetListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/setupwizard/time/DateTimeSetupActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "TimeDialog"
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 228
    invoke-direct {p0}, Landroid/app/DialogFragment;-><init>()V

    return-void
.end method


# virtual methods
.method public onCreateDialog(Landroid/os/Bundle;)Landroid/app/Dialog;
    .locals 7
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 241
    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object v6

    .line 242
    .local v6, "c":Ljava/util/Calendar;
    new-instance v0, Landroid/app/TimePickerDialog;

    invoke-virtual {p0}, Lcom/google/android/setupwizard/time/DateTimeSetupActivity$TimeDialog;->getActivity()Landroid/app/Activity;

    move-result-object v1

    const/16 v2, 0xb

    invoke-virtual {v6, v2}, Ljava/util/Calendar;->get(I)I

    move-result v3

    const/16 v2, 0xc

    invoke-virtual {v6, v2}, Ljava/util/Calendar;->get(I)I

    move-result v4

    invoke-virtual {p0}, Lcom/google/android/setupwizard/time/DateTimeSetupActivity$TimeDialog;->getActivity()Landroid/app/Activity;

    move-result-object v2

    invoke-static {v2}, Landroid/text/format/DateFormat;->is24HourFormat(Landroid/content/Context;)Z

    move-result v5

    move-object v2, p0

    invoke-direct/range {v0 .. v5}, Landroid/app/TimePickerDialog;-><init>(Landroid/content/Context;Landroid/app/TimePickerDialog$OnTimeSetListener;IIZ)V

    return-object v0
.end method

.method public onResume()V
    .locals 4

    .prologue
    .line 248
    invoke-super {p0}, Landroid/app/DialogFragment;->onResume()V

    .line 249
    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object v0

    .line 250
    .local v0, "c":Ljava/util/Calendar;
    invoke-virtual {p0}, Lcom/google/android/setupwizard/time/DateTimeSetupActivity$TimeDialog;->getDialog()Landroid/app/Dialog;

    move-result-object v1

    check-cast v1, Landroid/app/TimePickerDialog;

    const/16 v2, 0xb

    invoke-virtual {v0, v2}, Ljava/util/Calendar;->get(I)I

    move-result v2

    const/16 v3, 0xc

    invoke-virtual {v0, v3}, Ljava/util/Calendar;->get(I)I

    move-result v3

    invoke-virtual {v1, v2, v3}, Landroid/app/TimePickerDialog;->updateTime(II)V

    .line 252
    return-void
.end method

.method public onTimeSet(Landroid/widget/TimePicker;II)V
    .locals 1
    .param p1, "view"    # Landroid/widget/TimePicker;
    .param p2, "hour"    # I
    .param p3, "minute"    # I

    .prologue
    .line 256
    invoke-virtual {p0}, Lcom/google/android/setupwizard/time/DateTimeSetupActivity$TimeDialog;->getActivity()Landroid/app/Activity;

    move-result-object v0

    check-cast v0, Lcom/google/android/setupwizard/time/DateTimeSetupActivity;

    .line 257
    .local v0, "activity":Lcom/google/android/setupwizard/time/DateTimeSetupActivity;
    # invokes: Lcom/google/android/setupwizard/time/DateTimeSetupActivity;->setTime(II)V
    invoke-static {v0, p2, p3}, Lcom/google/android/setupwizard/time/DateTimeSetupActivity;->access$200(Lcom/google/android/setupwizard/time/DateTimeSetupActivity;II)V

    .line 258
    # invokes: Lcom/google/android/setupwizard/time/DateTimeSetupActivity;->updateTimeDisplay()V
    invoke-static {v0}, Lcom/google/android/setupwizard/time/DateTimeSetupActivity;->access$100(Lcom/google/android/setupwizard/time/DateTimeSetupActivity;)V

    .line 259
    return-void
.end method

.method public show(Landroid/app/FragmentManager;Ljava/lang/String;)V
    .locals 1
    .param p1, "manager"    # Landroid/app/FragmentManager;
    .param p2, "tag"    # Ljava/lang/String;

    .prologue
    .line 233
    invoke-virtual {p1, p2}, Landroid/app/FragmentManager;->findFragmentByTag(Ljava/lang/String;)Landroid/app/Fragment;

    move-result-object v0

    if-nez v0, :cond_0

    .line 235
    invoke-super {p0, p1, p2}, Landroid/app/DialogFragment;->show(Landroid/app/FragmentManager;Ljava/lang/String;)V

    .line 237
    :cond_0
    return-void
.end method
