.class public Lcom/google/android/setupwizard/user/WelcomeWrapper;
.super Lcom/google/android/setupwizard/SubactivityWrapper;
.source "WelcomeWrapper.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 24
    invoke-direct {p0}, Lcom/google/android/setupwizard/SubactivityWrapper;-><init>()V

    return-void
.end method


# virtual methods
.method protected onSubactivityLaunch()Landroid/content/Intent;
    .locals 2

    .prologue
    .line 38
    invoke-virtual {p0}, Lcom/google/android/setupwizard/user/WelcomeWrapper;->isEduSignin()Z

    move-result v1

    if-eqz v1, :cond_1

    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/google/android/setupwizard/edu/WelcomeEduActivity;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 41
    .local v0, "intent":Landroid/content/Intent;
    :goto_0
    invoke-virtual {p0}, Lcom/google/android/setupwizard/user/WelcomeWrapper;->isUsingWizardManager()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 42
    const/high16 v1, 0x2000000

    invoke-virtual {v0, v1}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    .line 45
    :cond_0
    return-object v0

    .line 38
    .end local v0    # "intent":Landroid/content/Intent;
    :cond_1
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/google/android/setupwizard/user/WelcomeActivity;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    goto :goto_0
.end method

.method protected requestCode()I
    .locals 1

    .prologue
    .line 28
    const/16 v0, 0x2711

    return v0
.end method
