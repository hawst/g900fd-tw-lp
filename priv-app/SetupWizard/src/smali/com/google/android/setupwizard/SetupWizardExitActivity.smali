.class public Lcom/google/android/setupwizard/SetupWizardExitActivity;
.super Lcom/google/android/setupwizard/BaseActivity;
.source "SetupWizardExitActivity.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 39
    invoke-direct {p0}, Lcom/google/android/setupwizard/BaseActivity;-><init>()V

    return-void
.end method

.method private disableComponent(Landroid/content/ComponentName;)V
    .locals 3
    .param p1, "component"    # Landroid/content/ComponentName;

    .prologue
    .line 137
    invoke-virtual {p0}, Lcom/google/android/setupwizard/SetupWizardExitActivity;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v0

    .line 138
    .local v0, "pm":Landroid/content/pm/PackageManager;
    const/4 v1, 0x2

    const/4 v2, 0x1

    invoke-virtual {v0, p1, v1, v2}, Landroid/content/pm/PackageManager;->setComponentEnabledSetting(Landroid/content/ComponentName;II)V

    .line 141
    return-void
.end method

.method private disableComponent(Ljava/lang/String;)V
    .locals 3
    .param p1, "name"    # Ljava/lang/String;

    .prologue
    .line 144
    iget-object v0, p0, Lcom/google/android/setupwizard/SetupWizardExitActivity;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "disableComponent "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 145
    new-instance v0, Landroid/content/ComponentName;

    invoke-direct {v0, p0, p1}, Landroid/content/ComponentName;-><init>(Landroid/content/Context;Ljava/lang/String;)V

    invoke-direct {p0, v0}, Lcom/google/android/setupwizard/SetupWizardExitActivity;->disableComponent(Landroid/content/ComponentName;)V

    .line 146
    return-void
.end method

.method private disableComponentArray([Landroid/content/pm/ComponentInfo;)V
    .locals 5
    .param p1, "components"    # [Landroid/content/pm/ComponentInfo;

    .prologue
    .line 129
    if-eqz p1, :cond_0

    .line 130
    move-object v0, p1

    .local v0, "arr$":[Landroid/content/pm/ComponentInfo;
    array-length v3, v0

    .local v3, "len$":I
    const/4 v1, 0x0

    .local v1, "i$":I
    :goto_0
    if-ge v1, v3, :cond_0

    aget-object v2, v0, v1

    .line 131
    .local v2, "info":Landroid/content/pm/ComponentInfo;
    iget-object v4, v2, Landroid/content/pm/ComponentInfo;->name:Ljava/lang/String;

    invoke-direct {p0, v4}, Lcom/google/android/setupwizard/SetupWizardExitActivity;->disableComponent(Ljava/lang/String;)V

    .line 130
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 134
    .end local v0    # "arr$":[Landroid/content/pm/ComponentInfo;
    .end local v1    # "i$":I
    .end local v2    # "info":Landroid/content/pm/ComponentInfo;
    .end local v3    # "len$":I
    :cond_0
    return-void
.end method

.method private disableComponentSets(I)V
    .locals 3
    .param p1, "flags"    # I

    .prologue
    .line 114
    :try_start_0
    invoke-virtual {p0}, Lcom/google/android/setupwizard/SetupWizardExitActivity;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v1

    invoke-virtual {p0}, Lcom/google/android/setupwizard/SetupWizardExitActivity;->getPackageName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2, p1}, Landroid/content/pm/PackageManager;->getPackageInfo(Ljava/lang/String;I)Landroid/content/pm/PackageInfo;

    move-result-object v0

    .line 115
    .local v0, "allInfo":Landroid/content/pm/PackageInfo;
    and-int/lit8 v1, p1, 0x1

    if-eqz v1, :cond_0

    .line 116
    iget-object v1, v0, Landroid/content/pm/PackageInfo;->activities:[Landroid/content/pm/ActivityInfo;

    invoke-direct {p0, v1}, Lcom/google/android/setupwizard/SetupWizardExitActivity;->disableComponentArray([Landroid/content/pm/ComponentInfo;)V

    .line 118
    :cond_0
    and-int/lit8 v1, p1, 0x2

    if-eqz v1, :cond_1

    .line 119
    iget-object v1, v0, Landroid/content/pm/PackageInfo;->receivers:[Landroid/content/pm/ActivityInfo;

    invoke-direct {p0, v1}, Lcom/google/android/setupwizard/SetupWizardExitActivity;->disableComponentArray([Landroid/content/pm/ComponentInfo;)V

    .line 121
    :cond_1
    and-int/lit8 v1, p1, 0x4

    if-eqz v1, :cond_2

    .line 122
    iget-object v1, v0, Landroid/content/pm/PackageInfo;->services:[Landroid/content/pm/ServiceInfo;

    invoke-direct {p0, v1}, Lcom/google/android/setupwizard/SetupWizardExitActivity;->disableComponentArray([Landroid/content/pm/ComponentInfo;)V
    :try_end_0
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    .line 126
    .end local v0    # "allInfo":Landroid/content/pm/PackageInfo;
    :cond_2
    :goto_0
    return-void

    .line 124
    :catch_0
    move-exception v1

    goto :goto_0
.end method

.method private disableHome()V
    .locals 5

    .prologue
    .line 152
    new-instance v1, Landroid/content/Intent;

    const-string v2, "android.intent.action.MAIN"

    invoke-direct {v1, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 153
    .local v1, "intent":Landroid/content/Intent;
    const-string v2, "android.intent.category.HOME"

    invoke-virtual {v1, v2}, Landroid/content/Intent;->addCategory(Ljava/lang/String;)Landroid/content/Intent;

    .line 154
    invoke-virtual {p0}, Lcom/google/android/setupwizard/SetupWizardExitActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/content/Intent;->setPackage(Ljava/lang/String;)Landroid/content/Intent;

    .line 155
    invoke-virtual {p0}, Lcom/google/android/setupwizard/SetupWizardExitActivity;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/content/Intent;->resolveActivity(Landroid/content/pm/PackageManager;)Landroid/content/ComponentName;

    move-result-object v0

    .line 156
    .local v0, "cn":Landroid/content/ComponentName;
    iget-object v2, p0, Lcom/google/android/setupwizard/SetupWizardExitActivity;->TAG:Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "resolveActivity for intent="

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " returns "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 157
    if-eqz v0, :cond_0

    .line 158
    iget-object v2, p0, Lcom/google/android/setupwizard/SetupWizardExitActivity;->TAG:Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Disabling home activity "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v0}, Landroid/content/ComponentName;->getShortClassName()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 159
    invoke-direct {p0, v0}, Lcom/google/android/setupwizard/SetupWizardExitActivity;->disableComponent(Landroid/content/ComponentName;)V

    .line 161
    :cond_0
    return-void
.end method

.method private updateLastSetupShown()V
    .locals 5

    .prologue
    .line 169
    :try_start_0
    invoke-virtual {p0}, Lcom/google/android/setupwizard/SetupWizardExitActivity;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v2

    invoke-virtual {p0}, Lcom/google/android/setupwizard/SetupWizardExitActivity;->getPackageName()Ljava/lang/String;

    move-result-object v3

    const/16 v4, 0x80

    invoke-virtual {v2, v3, v4}, Landroid/content/pm/PackageManager;->getApplicationInfo(Ljava/lang/String;I)Landroid/content/pm/ApplicationInfo;

    move-result-object v0

    .line 171
    .local v0, "applicationInfo":Landroid/content/pm/ApplicationInfo;
    if-eqz v0, :cond_0

    iget-object v2, v0, Landroid/content/pm/ApplicationInfo;->metaData:Landroid/os/Bundle;

    if-eqz v2, :cond_0

    .line 172
    iget-object v2, v0, Landroid/content/pm/ApplicationInfo;->metaData:Landroid/os/Bundle;

    const-string v3, "android.SETUP_VERSION"

    invoke-virtual {v2, v3}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 173
    .local v1, "vers":Ljava/lang/String;
    if-eqz v1, :cond_0

    .line 174
    invoke-virtual {p0}, Lcom/google/android/setupwizard/SetupWizardExitActivity;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    const-string v3, "last_setup_shown"

    invoke-static {v2, v3, v1}, Landroid/provider/Settings$Secure;->putString(Landroid/content/ContentResolver;Ljava/lang/String;Ljava/lang/String;)Z
    :try_end_0
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    .line 180
    .end local v0    # "applicationInfo":Landroid/content/pm/ApplicationInfo;
    .end local v1    # "vers":Ljava/lang/String;
    :cond_0
    :goto_0
    return-void

    .line 178
    :catch_0
    move-exception v2

    goto :goto_0
.end method


# virtual methods
.method protected onCreate(Landroid/os/Bundle;)V
    .locals 4
    .param p1, "icicle"    # Landroid/os/Bundle;

    .prologue
    const/4 v3, 0x1

    .line 46
    invoke-super {p0, p1}, Lcom/google/android/setupwizard/BaseActivity;->onCreate(Landroid/os/Bundle;)V

    .line 49
    invoke-static {p0}, Lcom/google/android/setupwizard/util/SetupStatusBarManager;->onSetupComplete(Landroid/content/Context;)V

    .line 50
    invoke-virtual {p0}, Lcom/google/android/setupwizard/SetupWizardExitActivity;->restoreScreenTimeout()V

    .line 51
    sget-boolean v1, Lcom/google/android/setupwizard/SetupWizardExitActivity;->ROTATION_LOCKED:Z

    if-nez v1, :cond_0

    invoke-virtual {p0}, Lcom/google/android/setupwizard/SetupWizardExitActivity;->isEduSignin()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 52
    :cond_0
    invoke-virtual {p0}, Lcom/google/android/setupwizard/SetupWizardExitActivity;->unlockRotation()V

    .line 56
    :cond_1
    invoke-virtual {p0}, Lcom/google/android/setupwizard/SetupWizardExitActivity;->getLocalPreferences()Landroid/content/SharedPreferences;

    move-result-object v1

    invoke-interface {v1}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v1

    invoke-interface {v1}, Landroid/content/SharedPreferences$Editor;->clear()Landroid/content/SharedPreferences$Editor;

    move-result-object v1

    invoke-interface {v1}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 59
    invoke-virtual {p0}, Lcom/google/android/setupwizard/SetupWizardExitActivity;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    const-string v2, "captive_portal_detection_enabled"

    invoke-static {v1, v2, v3}, Landroid/provider/Settings$Global;->putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z

    .line 66
    const-string v1, "connectivity"

    invoke-virtual {p0, v1}, Lcom/google/android/setupwizard/SetupWizardExitActivity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/net/ConnectivityManager;

    .line 68
    .local v0, "connectivityManager":Landroid/net/ConnectivityManager;
    const v1, 0xea60

    invoke-virtual {v0, v1}, Landroid/net/ConnectivityManager;->checkMobileProvisioning(I)I

    .line 73
    invoke-static {p0, v3}, Lcom/google/android/gsf/UseLocationForServices;->forceSetUseLocationForServices(Landroid/content/Context;Z)Z

    .line 80
    invoke-virtual {p0}, Lcom/google/android/setupwizard/SetupWizardExitActivity;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    const-string v2, "device_provisioned"

    invoke-static {v1, v2, v3}, Landroid/provider/Settings$Global;->putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z

    .line 81
    invoke-virtual {p0}, Lcom/google/android/setupwizard/SetupWizardExitActivity;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    const-string v2, "user_setup_complete"

    invoke-static {v1, v2, v3}, Landroid/provider/Settings$Secure;->putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z

    .line 84
    invoke-direct {p0}, Lcom/google/android/setupwizard/SetupWizardExitActivity;->updateLastSetupShown()V

    .line 87
    const/4 v1, 0x6

    invoke-direct {p0, v1}, Lcom/google/android/setupwizard/SetupWizardExitActivity;->disableComponentSets(I)V

    .line 90
    invoke-direct {p0}, Lcom/google/android/setupwizard/SetupWizardExitActivity;->disableHome()V

    .line 94
    new-instance v1, Landroid/content/Intent;

    const-string v2, "com.google.android.setupwizard.SETUP_WIZARD_FINISHED"

    invoke-direct {v1, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0, v1}, Lcom/google/android/setupwizard/SetupWizardExitActivity;->sendBroadcast(Landroid/content/Intent;)V

    .line 96
    iget-object v1, p0, Lcom/google/android/setupwizard/SetupWizardExitActivity;->TAG:Ljava/lang/String;

    const-string v2, "*** SetupWizard Exit ***"

    invoke-static {v1, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 99
    new-instance v1, Landroid/content/Intent;

    const-string v2, "android.intent.action.MAIN"

    const/4 v3, 0x0

    invoke-direct {v1, v2, v3}, Landroid/content/Intent;-><init>(Ljava/lang/String;Landroid/net/Uri;)V

    const-string v2, "android.intent.category.HOME"

    invoke-virtual {v1, v2}, Landroid/content/Intent;->addCategory(Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v1

    const v2, 0x10208000

    invoke-virtual {v1, v2}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    move-result-object v1

    invoke-virtual {p0, v1}, Lcom/google/android/setupwizard/SetupWizardExitActivity;->startActivity(Landroid/content/Intent;)V

    .line 106
    const/4 v1, -0x1

    invoke-virtual {p0, v1}, Lcom/google/android/setupwizard/SetupWizardExitActivity;->setResultCode(I)V

    .line 108
    invoke-virtual {p0}, Lcom/google/android/setupwizard/SetupWizardExitActivity;->finish()V

    .line 109
    return-void
.end method
