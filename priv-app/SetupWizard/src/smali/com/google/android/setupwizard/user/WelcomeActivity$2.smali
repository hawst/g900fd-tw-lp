.class Lcom/google/android/setupwizard/user/WelcomeActivity$2;
.super Ljava/lang/Object;
.source "WelcomeActivity.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/setupwizard/user/WelcomeActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/setupwizard/user/WelcomeActivity;


# direct methods
.method constructor <init>(Lcom/google/android/setupwizard/user/WelcomeActivity;)V
    .locals 0

    .prologue
    .line 122
    iput-object p1, p0, Lcom/google/android/setupwizard/user/WelcomeActivity$2;->this$0:Lcom/google/android/setupwizard/user/WelcomeActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 3

    .prologue
    .line 125
    iget-object v0, p0, Lcom/google/android/setupwizard/user/WelcomeActivity$2;->this$0:Lcom/google/android/setupwizard/user/WelcomeActivity;

    # getter for: Lcom/google/android/setupwizard/user/WelcomeActivity;->TAG:Ljava/lang/String;
    invoke-static {v0}, Lcom/google/android/setupwizard/user/WelcomeActivity;->access$100(Lcom/google/android/setupwizard/user/WelcomeActivity;)Ljava/lang/String;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "WelcomeActivity.mUpdateLocale.run() mPickerLocale="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/setupwizard/user/WelcomeActivity$2;->this$0:Lcom/google/android/setupwizard/user/WelcomeActivity;

    # getter for: Lcom/google/android/setupwizard/user/WelcomeActivity;->mPickerLocale:Ljava/util/Locale;
    invoke-static {v2}, Lcom/google/android/setupwizard/user/WelcomeActivity;->access$200(Lcom/google/android/setupwizard/user/WelcomeActivity;)Ljava/util/Locale;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 126
    iget-object v0, p0, Lcom/google/android/setupwizard/user/WelcomeActivity$2;->this$0:Lcom/google/android/setupwizard/user/WelcomeActivity;

    # getter for: Lcom/google/android/setupwizard/user/WelcomeActivity;->mPickerLocale:Ljava/util/Locale;
    invoke-static {v0}, Lcom/google/android/setupwizard/user/WelcomeActivity;->access$200(Lcom/google/android/setupwizard/user/WelcomeActivity;)Ljava/util/Locale;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 130
    iget-object v0, p0, Lcom/google/android/setupwizard/user/WelcomeActivity$2;->this$0:Lcom/google/android/setupwizard/user/WelcomeActivity;

    # getter for: Lcom/google/android/setupwizard/user/WelcomeActivity;->mPickerLocale:Ljava/util/Locale;
    invoke-static {v0}, Lcom/google/android/setupwizard/user/WelcomeActivity;->access$200(Lcom/google/android/setupwizard/user/WelcomeActivity;)Ljava/util/Locale;

    move-result-object v0

    invoke-static {v0}, Lcom/android/internal/app/LocalePicker;->updateLocale(Ljava/util/Locale;)V

    .line 131
    iget-object v0, p0, Lcom/google/android/setupwizard/user/WelcomeActivity$2;->this$0:Lcom/google/android/setupwizard/user/WelcomeActivity;

    const/4 v1, 0x0

    # setter for: Lcom/google/android/setupwizard/user/WelcomeActivity;->mPickerLocale:Ljava/util/Locale;
    invoke-static {v0, v1}, Lcom/google/android/setupwizard/user/WelcomeActivity;->access$202(Lcom/google/android/setupwizard/user/WelcomeActivity;Ljava/util/Locale;)Ljava/util/Locale;

    .line 133
    :cond_0
    return-void
.end method
