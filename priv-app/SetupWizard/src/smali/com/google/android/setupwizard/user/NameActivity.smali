.class public Lcom/google/android/setupwizard/user/NameActivity;
.super Lcom/google/android/setupwizard/BaseActivity;
.source "NameActivity.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/setupwizard/user/NameActivity$UpdateProfileAsyncTask;,
        Lcom/google/android/setupwizard/user/NameActivity$NameWatcher;
    }
.end annotation


# instance fields
.field private mFirstNameEdit:Landroid/widget/EditText;

.field private mLastNameEdit:Landroid/widget/EditText;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 43
    invoke-direct {p0}, Lcom/google/android/setupwizard/BaseActivity;-><init>()V

    .line 174
    return-void
.end method

.method static synthetic access$000(Ljava/lang/CharSequence;)Z
    .locals 1
    .param p0, "x0"    # Ljava/lang/CharSequence;

    .prologue
    .line 43
    invoke-static {p0}, Lcom/google/android/setupwizard/user/NameActivity;->isNamePrefixValid(Ljava/lang/CharSequence;)Z

    move-result v0

    return v0
.end method

.method static synthetic access$100(Lcom/google/android/setupwizard/user/NameActivity;)V
    .locals 0
    .param p0, "x0"    # Lcom/google/android/setupwizard/user/NameActivity;

    .prologue
    .line 43
    invoke-virtual {p0}, Lcom/google/android/setupwizard/user/NameActivity;->nextScreen()V

    return-void
.end method

.method private static isNamePrefixValid(Ljava/lang/CharSequence;)Z
    .locals 1
    .param p0, "name"    # Ljava/lang/CharSequence;

    .prologue
    .line 138
    invoke-static {p0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    invoke-static {p0}, Landroid/text/TextUtils;->isGraphic(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private static isNameValid(Ljava/lang/CharSequence;)Z
    .locals 1
    .param p0, "name"    # Ljava/lang/CharSequence;

    .prologue
    .line 130
    invoke-static {p0}, Landroid/text/TextUtils;->isGraphic(Ljava/lang/CharSequence;)Z

    move-result v0

    return v0
.end method

.method private setNameFromAccount()Z
    .locals 8

    .prologue
    const/4 v4, 0x1

    const/4 v5, 0x0

    .line 160
    invoke-virtual {p0}, Lcom/google/android/setupwizard/user/NameActivity;->getAccount()Landroid/accounts/Account;

    move-result-object v0

    .line 161
    .local v0, "account":Landroid/accounts/Account;
    if-eqz v0, :cond_1

    .line 162
    invoke-static {p0}, Landroid/accounts/AccountManager;->get(Landroid/content/Context;)Landroid/accounts/AccountManager;

    move-result-object v1

    .line 163
    .local v1, "accountManager":Landroid/accounts/AccountManager;
    const-string v6, "firstName"

    invoke-virtual {v1, v0, v6}, Landroid/accounts/AccountManager;->getUserData(Landroid/accounts/Account;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 164
    .local v2, "firstName":Ljava/lang/String;
    const-string v6, "lastName"

    invoke-virtual {v1, v0, v6}, Landroid/accounts/AccountManager;->getUserData(Landroid/accounts/Account;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 166
    .local v3, "lastName":Ljava/lang/String;
    if-nez v2, :cond_0

    if-eqz v3, :cond_1

    .line 167
    :cond_0
    new-instance v6, Lcom/google/android/setupwizard/user/NameActivity$UpdateProfileAsyncTask;

    invoke-direct {v6, p0, v5}, Lcom/google/android/setupwizard/user/NameActivity$UpdateProfileAsyncTask;-><init>(Lcom/google/android/setupwizard/user/NameActivity;Z)V

    const/4 v7, 0x2

    new-array v7, v7, [Ljava/lang/String;

    aput-object v2, v7, v5

    aput-object v3, v7, v4

    invoke-virtual {v6, v7}, Lcom/google/android/setupwizard/user/NameActivity$UpdateProfileAsyncTask;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    .line 171
    .end local v1    # "accountManager":Landroid/accounts/AccountManager;
    .end local v2    # "firstName":Ljava/lang/String;
    .end local v3    # "lastName":Ljava/lang/String;
    :goto_0
    return v4

    :cond_1
    move v4, v5

    goto :goto_0
.end method

.method private shouldDisplayLastNameFirst()Z
    .locals 3

    .prologue
    .line 145
    invoke-virtual {p0}, Lcom/google/android/setupwizard/user/NameActivity;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    const-string v2, "google_setup:lastnamefirst_countries"

    invoke-static {v1, v2}, Lcom/google/android/gsf/Gservices;->getString(Landroid/content/ContentResolver;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 147
    .local v0, "lastFirstCountries":Ljava/lang/String;
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 148
    const-string v0, "*ja*ko*hu*zh*"

    .line 150
    :cond_0
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "*"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v2

    invoke-virtual {v2}, Ljava/util/Locale;->getLanguage()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "*"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v1

    return v1
.end method


# virtual methods
.method public getFirstName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 119
    iget-object v0, p0, Lcom/google/android/setupwizard/user/NameActivity;->mFirstNameEdit:Landroid/widget/EditText;

    invoke-virtual {v0}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getLastName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 123
    iget-object v0, p0, Lcom/google/android/setupwizard/user/NameActivity;->mLastNameEdit:Landroid/widget/EditText;

    invoke-virtual {v0}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 4
    .param p1, "icicle"    # Landroid/os/Bundle;

    .prologue
    const v2, 0x7f030023

    .line 55
    invoke-super {p0, p1}, Lcom/google/android/setupwizard/BaseActivity;->onCreate(Landroid/os/Bundle;)V

    .line 56
    invoke-direct {p0}, Lcom/google/android/setupwizard/user/NameActivity;->setNameFromAccount()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 57
    invoke-virtual {p0}, Lcom/google/android/setupwizard/user/NameActivity;->nextScreen()V

    .line 58
    invoke-virtual {p0}, Lcom/google/android/setupwizard/user/NameActivity;->finish()V

    .line 79
    :goto_0
    return-void

    .line 62
    :cond_0
    invoke-direct {p0}, Lcom/google/android/setupwizard/user/NameActivity;->shouldDisplayLastNameFirst()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 63
    const v1, 0x7f030010

    invoke-virtual {p0, v1, v2}, Lcom/google/android/setupwizard/user/NameActivity;->setTemplateContent(II)V

    .line 67
    :goto_1
    const v1, 0x7f0700ad

    invoke-virtual {p0, v1}, Lcom/google/android/setupwizard/user/NameActivity;->setHeaderText(I)V

    .line 68
    const v1, 0x7f020055

    const v2, 0x7f020056

    invoke-virtual {p0, v1, v2}, Lcom/google/android/setupwizard/user/NameActivity;->setIllustration(II)V

    .line 69
    const v1, 0x7f0e0050

    invoke-virtual {p0, v1}, Lcom/google/android/setupwizard/user/NameActivity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/EditText;

    iput-object v1, p0, Lcom/google/android/setupwizard/user/NameActivity;->mFirstNameEdit:Landroid/widget/EditText;

    .line 70
    const v1, 0x7f0e0051

    invoke-virtual {p0, v1}, Lcom/google/android/setupwizard/user/NameActivity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/EditText;

    iput-object v1, p0, Lcom/google/android/setupwizard/user/NameActivity;->mLastNameEdit:Landroid/widget/EditText;

    .line 71
    iget-object v1, p0, Lcom/google/android/setupwizard/user/NameActivity;->mFirstNameEdit:Landroid/widget/EditText;

    new-instance v2, Lcom/google/android/setupwizard/user/NameActivity$NameWatcher;

    iget-object v3, p0, Lcom/google/android/setupwizard/user/NameActivity;->mFirstNameEdit:Landroid/widget/EditText;

    invoke-direct {v2, p0, v3}, Lcom/google/android/setupwizard/user/NameActivity$NameWatcher;-><init>(Lcom/google/android/setupwizard/user/NameActivity;Landroid/widget/EditText;)V

    invoke-virtual {v1, v2}, Landroid/widget/EditText;->addTextChangedListener(Landroid/text/TextWatcher;)V

    .line 72
    iget-object v1, p0, Lcom/google/android/setupwizard/user/NameActivity;->mLastNameEdit:Landroid/widget/EditText;

    new-instance v2, Lcom/google/android/setupwizard/user/NameActivity$NameWatcher;

    iget-object v3, p0, Lcom/google/android/setupwizard/user/NameActivity;->mLastNameEdit:Landroid/widget/EditText;

    invoke-direct {v2, p0, v3}, Lcom/google/android/setupwizard/user/NameActivity$NameWatcher;-><init>(Lcom/google/android/setupwizard/user/NameActivity;Landroid/widget/EditText;)V

    invoke-virtual {v1, v2}, Landroid/widget/EditText;->addTextChangedListener(Landroid/text/TextWatcher;)V

    .line 74
    invoke-virtual {p0}, Lcom/google/android/setupwizard/user/NameActivity;->getWindow()Landroid/view/Window;

    move-result-object v1

    const/16 v2, 0x10

    invoke-virtual {v1, v2}, Landroid/view/Window;->setSoftInputMode(I)V

    .line 75
    const v1, 0x7f0e005e

    invoke-virtual {p0, v1}, Lcom/google/android/setupwizard/user/NameActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    .line 76
    .local v0, "bottomScrollView":Landroid/view/View;
    invoke-virtual {v0, p0}, Landroid/view/View;->setOnApplyWindowInsetsListener(Landroid/view/View$OnApplyWindowInsetsListener;)V

    .line 78
    invoke-virtual {p0}, Lcom/google/android/setupwizard/user/NameActivity;->updateWidgetState()V

    goto :goto_0

    .line 65
    .end local v0    # "bottomScrollView":Landroid/view/View;
    :cond_1
    const v1, 0x7f03000f

    invoke-virtual {p0, v1, v2}, Lcom/google/android/setupwizard/user/NameActivity;->setTemplateContent(II)V

    goto :goto_1
.end method

.method protected start()V
    .locals 5

    .prologue
    const/4 v4, 0x1

    .line 106
    new-instance v0, Lcom/google/android/setupwizard/user/NameActivity$UpdateProfileAsyncTask;

    invoke-direct {v0, p0, v4}, Lcom/google/android/setupwizard/user/NameActivity$UpdateProfileAsyncTask;-><init>(Lcom/google/android/setupwizard/user/NameActivity;Z)V

    const/4 v1, 0x2

    new-array v1, v1, [Ljava/lang/String;

    const/4 v2, 0x0

    invoke-virtual {p0}, Lcom/google/android/setupwizard/user/NameActivity;->getFirstName()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v1, v2

    invoke-virtual {p0}, Lcom/google/android/setupwizard/user/NameActivity;->getLastName()Ljava/lang/String;

    move-result-object v2

    aput-object v2, v1, v4

    invoke-virtual {v0, v1}, Lcom/google/android/setupwizard/user/NameActivity$UpdateProfileAsyncTask;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    .line 107
    return-void
.end method

.method public updateWidgetState()V
    .locals 4

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 110
    iget-object v3, p0, Lcom/google/android/setupwizard/user/NameActivity;->mFirstNameEdit:Landroid/widget/EditText;

    invoke-virtual {v3}, Landroid/widget/EditText;->getError()Ljava/lang/CharSequence;

    move-result-object v3

    if-nez v3, :cond_2

    iget-object v3, p0, Lcom/google/android/setupwizard/user/NameActivity;->mLastNameEdit:Landroid/widget/EditText;

    invoke-virtual {v3}, Landroid/widget/EditText;->getError()Ljava/lang/CharSequence;

    move-result-object v3

    if-nez v3, :cond_2

    move v0, v2

    .line 111
    .local v0, "enableNext":Z
    :goto_0
    if-eqz v0, :cond_1

    invoke-virtual {p0}, Lcom/google/android/setupwizard/user/NameActivity;->isPrimaryUser()Z

    move-result v3

    if-nez v3, :cond_1

    .line 113
    invoke-virtual {p0}, Lcom/google/android/setupwizard/user/NameActivity;->getFirstName()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Lcom/google/android/setupwizard/user/NameActivity;->isNameValid(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_0

    invoke-virtual {p0}, Lcom/google/android/setupwizard/user/NameActivity;->getLastName()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Lcom/google/android/setupwizard/user/NameActivity;->isNameValid(Ljava/lang/CharSequence;)Z

    move-result v3

    if-eqz v3, :cond_3

    :cond_0
    move v0, v2

    .line 115
    :cond_1
    :goto_1
    invoke-virtual {p0, v0}, Lcom/google/android/setupwizard/user/NameActivity;->setNextAllowed(Z)V

    .line 116
    return-void

    .end local v0    # "enableNext":Z
    :cond_2
    move v0, v1

    .line 110
    goto :goto_0

    .restart local v0    # "enableNext":Z
    :cond_3
    move v0, v1

    .line 113
    goto :goto_1
.end method
