.class public Lcom/google/android/setupwizard/user/SetupCompleteActivity;
.super Lcom/google/android/setupwizard/BaseActivity;
.source "SetupCompleteActivity.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 29
    invoke-direct {p0}, Lcom/google/android/setupwizard/BaseActivity;-><init>()V

    return-void
.end method


# virtual methods
.method protected onCreate(Landroid/os/Bundle;)V
    .locals 2
    .param p1, "icicle"    # Landroid/os/Bundle;

    .prologue
    .line 33
    invoke-super {p0, p1}, Lcom/google/android/setupwizard/BaseActivity;->onCreate(Landroid/os/Bundle;)V

    .line 34
    const v0, 0x7f030014

    const v1, 0x7f030018

    invoke-virtual {p0, v0, v1}, Lcom/google/android/setupwizard/user/SetupCompleteActivity;->setTemplateContent(II)V

    .line 35
    return-void
.end method

.method public onNavigationBarCreated(Lcom/android/setupwizard/navigationbar/SetupWizardNavBar;)V
    .locals 2
    .param p1, "bar"    # Lcom/android/setupwizard/navigationbar/SetupWizardNavBar;

    .prologue
    .line 39
    invoke-super {p0, p1}, Lcom/google/android/setupwizard/BaseActivity;->onNavigationBarCreated(Lcom/android/setupwizard/navigationbar/SetupWizardNavBar;)V

    .line 40
    invoke-virtual {p1}, Lcom/android/setupwizard/navigationbar/SetupWizardNavBar;->getNextButton()Landroid/widget/Button;

    move-result-object v0

    const v1, 0x7f070045

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setText(I)V

    .line 41
    return-void
.end method

.method public start()V
    .locals 1

    .prologue
    .line 45
    invoke-virtual {p0}, Lcom/google/android/setupwizard/user/SetupCompleteActivity;->nextScreen()V

    .line 46
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/google/android/setupwizard/user/SetupCompleteActivity;->setNextAllowed(Z)V

    .line 47
    return-void
.end method
