.class public Lcom/google/android/setupwizard/time/ZonePicker;
.super Landroid/app/ListFragment;
.source "ZonePicker.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/setupwizard/time/ZonePicker$TimeZoneAdapter;,
        Lcom/google/android/setupwizard/time/ZonePicker$ZoneGetter;,
        Lcom/google/android/setupwizard/time/ZonePicker$ZoneSelectionListener;
    }
.end annotation


# instance fields
.field private mListener:Lcom/google/android/setupwizard/time/ZonePicker$ZoneSelectionListener;


# direct methods
.method public static constructTimezoneAdapter(Landroid/content/Context;ZI)Landroid/widget/SimpleAdapter;
    .locals 8
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "sortedByName"    # Z
    .param p2, "layoutId"    # I

    .prologue
    const/4 v7, 0x2

    .line 100
    new-array v4, v7, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string v3, "name"

    aput-object v3, v4, v1

    const/4 v1, 0x1

    const-string v3, "gmt"

    aput-object v3, v4, v1

    .line 101
    .local v4, "from":[Ljava/lang/String;
    new-array v5, v7, [I

    fill-array-data v5, :array_0

    .line 103
    .local v5, "to":[I
    new-instance v6, Lcom/google/android/setupwizard/time/ZonePicker$ZoneGetter;

    invoke-direct {v6}, Lcom/google/android/setupwizard/time/ZonePicker$ZoneGetter;-><init>()V

    .line 104
    .local v6, "zoneGetter":Lcom/google/android/setupwizard/time/ZonePicker$ZoneGetter;
    # invokes: Lcom/google/android/setupwizard/time/ZonePicker$ZoneGetter;->getZones(Landroid/content/Context;)Ljava/util/List;
    invoke-static {v6, p0}, Lcom/google/android/setupwizard/time/ZonePicker$ZoneGetter;->access$000(Lcom/google/android/setupwizard/time/ZonePicker$ZoneGetter;Landroid/content/Context;)Ljava/util/List;

    move-result-object v2

    .line 105
    .local v2, "list":Ljava/util/List;, "Ljava/util/List<Ljava/util/HashMap<Ljava/lang/String;Ljava/lang/Object;>;>;"
    new-instance v0, Lcom/google/android/setupwizard/time/ZonePicker$TimeZoneAdapter;

    move-object v1, p0

    move v3, p2

    invoke-direct/range {v0 .. v5}, Lcom/google/android/setupwizard/time/ZonePicker$TimeZoneAdapter;-><init>(Landroid/content/Context;Ljava/util/List;I[Ljava/lang/String;[I)V

    .line 107
    .local v0, "adapter":Landroid/widget/SimpleAdapter;
    return-object v0

    .line 101
    nop

    :array_0
    .array-data 4
        0x1020014
        0x1020015
    .end array-data
.end method

.method public static getTimeZoneForLocale(Landroid/widget/SimpleAdapter;Ljava/util/Locale;)Ljava/util/TimeZone;
    .locals 18
    .param p0, "adapter"    # Landroid/widget/SimpleAdapter;
    .param p1, "locale"    # Ljava/util/Locale;

    .prologue
    .line 152
    invoke-virtual/range {p0 .. p0}, Landroid/widget/SimpleAdapter;->getCount()I

    move-result v9

    .line 154
    .local v9, "listSize":I
    invoke-virtual/range {p0 .. p0}, Landroid/widget/SimpleAdapter;->getCount()I

    move-result v16

    const/16 v17, 0x1

    invoke-static/range {v16 .. v17}, Lcom/google/common/collect/HashMultimap;->create(II)Lcom/google/common/collect/HashMultimap;

    move-result-object v13

    .line 156
    .local v13, "timezoneOffsets":Lcom/google/common/collect/HashMultimap;, "Lcom/google/common/collect/HashMultimap<Ljava/lang/Integer;Ljava/util/TimeZone;>;"
    new-instance v14, Ljava/util/HashMap;

    invoke-virtual/range {p0 .. p0}, Landroid/widget/SimpleAdapter;->getCount()I

    move-result v16

    move/from16 v0, v16

    invoke-direct {v14, v0}, Ljava/util/HashMap;-><init>(I)V

    .line 158
    .local v14, "timezones":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/String;Ljava/util/TimeZone;>;"
    const/4 v4, 0x0

    .local v4, "i":I
    :goto_0
    if-ge v4, v9, :cond_0

    .line 159
    move-object/from16 v0, p0

    invoke-virtual {v0, v4}, Landroid/widget/SimpleAdapter;->getItem(I)Ljava/lang/Object;

    move-result-object v11

    check-cast v11, Ljava/util/HashMap;

    .line 160
    .local v11, "map":Ljava/util/HashMap;, "Ljava/util/HashMap<**>;"
    const-string v16, "id"

    move-object/from16 v0, v16

    invoke-virtual {v11, v0}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Ljava/lang/String;

    .line 161
    .local v7, "id":Ljava/lang/String;
    invoke-static {v7}, Ljava/util/TimeZone;->getTimeZone(Ljava/lang/String;)Ljava/util/TimeZone;

    move-result-object v12

    .line 162
    .local v12, "timezone":Ljava/util/TimeZone;
    invoke-virtual {v12}, Ljava/util/TimeZone;->getRawOffset()I

    move-result v16

    invoke-static/range {v16 .. v16}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v16

    move-object/from16 v0, v16

    invoke-virtual {v13, v0, v12}, Lcom/google/common/collect/HashMultimap;->put(Ljava/lang/Object;Ljava/lang/Object;)Z

    .line 163
    invoke-virtual {v14, v7, v12}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 158
    add-int/lit8 v4, v4, 0x1

    goto :goto_0

    .line 167
    .end local v7    # "id":Ljava/lang/String;
    .end local v11    # "map":Ljava/util/HashMap;, "Ljava/util/HashMap<**>;"
    .end local v12    # "timezone":Ljava/util/TimeZone;
    :cond_0
    invoke-static/range {p1 .. p1}, Llibcore/icu/TimeZoneNames;->forLocale(Ljava/util/Locale;)[Ljava/lang/String;

    move-result-object v10

    .line 168
    .local v10, "localeTimeZones":[Ljava/lang/String;
    move-object v3, v10

    .local v3, "arr$":[Ljava/lang/String;
    array-length v8, v3

    .local v8, "len$":I
    const/4 v5, 0x0

    .local v5, "i$":I
    move v6, v5

    .end local v5    # "i$":I
    .local v6, "i$":I
    :goto_1
    if-ge v6, v8, :cond_4

    aget-object v7, v3, v6

    .line 169
    .restart local v7    # "id":Ljava/lang/String;
    invoke-virtual {v14, v7}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v15

    check-cast v15, Ljava/util/TimeZone;

    .line 170
    .local v15, "tz":Ljava/util/TimeZone;
    if-eqz v15, :cond_1

    .line 191
    .end local v6    # "i$":I
    .end local v7    # "id":Ljava/lang/String;
    .end local v15    # "tz":Ljava/util/TimeZone;
    :goto_2
    return-object v15

    .line 181
    .restart local v6    # "i$":I
    .restart local v7    # "id":Ljava/lang/String;
    .restart local v15    # "tz":Ljava/util/TimeZone;
    :cond_1
    invoke-static {v7}, Ljava/util/TimeZone;->getTimeZone(Ljava/lang/String;)Ljava/util/TimeZone;

    move-result-object v15

    .line 182
    invoke-virtual {v15}, Ljava/util/TimeZone;->getRawOffset()I

    move-result v16

    invoke-static/range {v16 .. v16}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v16

    move-object/from16 v0, v16

    invoke-virtual {v13, v0}, Lcom/google/common/collect/HashMultimap;->get(Ljava/lang/Object;)Ljava/util/Set;

    move-result-object v2

    .line 183
    .local v2, "altTimeZones":Ljava/util/Set;, "Ljava/util/Set<Ljava/util/TimeZone;>;"
    invoke-interface {v2}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v5

    .end local v6    # "i$":I
    .local v5, "i$":Ljava/util/Iterator;
    :cond_2
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v16

    if-eqz v16, :cond_3

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/util/TimeZone;

    .line 184
    .local v1, "altTimeZone":Ljava/util/TimeZone;
    invoke-static {v15, v1}, Lcom/google/android/setupwizard/time/ZonePicker;->isSimilarTimeZone(Ljava/util/TimeZone;Ljava/util/TimeZone;)Z

    move-result v16

    if-eqz v16, :cond_2

    move-object v15, v1

    .line 185
    goto :goto_2

    .line 168
    .end local v1    # "altTimeZone":Ljava/util/TimeZone;
    :cond_3
    add-int/lit8 v5, v6, 0x1

    .local v5, "i$":I
    move v6, v5

    .end local v5    # "i$":I
    .restart local v6    # "i$":I
    goto :goto_1

    .line 191
    .end local v2    # "altTimeZones":Ljava/util/Set;, "Ljava/util/Set<Ljava/util/TimeZone;>;"
    .end local v7    # "id":Ljava/lang/String;
    .end local v15    # "tz":Ljava/util/TimeZone;
    :cond_4
    const-string v16, "America/Los_Angeles"

    invoke-static/range {v16 .. v16}, Ljava/util/TimeZone;->getTimeZone(Ljava/lang/String;)Ljava/util/TimeZone;

    move-result-object v15

    goto :goto_2
.end method

.method public static getTimeZoneIndex(Landroid/widget/SimpleAdapter;Ljava/util/TimeZone;)I
    .locals 6
    .param p0, "adapter"    # Landroid/widget/SimpleAdapter;
    .param p1, "tz"    # Ljava/util/TimeZone;

    .prologue
    .line 121
    invoke-virtual {p1}, Ljava/util/TimeZone;->getID()Ljava/lang/String;

    move-result-object v0

    .line 122
    .local v0, "defaultId":Ljava/lang/String;
    invoke-virtual {p0}, Landroid/widget/SimpleAdapter;->getCount()I

    move-result v3

    .line 123
    .local v3, "listSize":I
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    if-ge v1, v3, :cond_1

    .line 125
    invoke-virtual {p0, v1}, Landroid/widget/SimpleAdapter;->getItem(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/util/HashMap;

    .line 126
    .local v4, "map":Ljava/util/HashMap;, "Ljava/util/HashMap<**>;"
    const-string v5, "id"

    invoke-virtual {v4, v5}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    .line 127
    .local v2, "id":Ljava/lang/String;
    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_0

    .line 132
    .end local v1    # "i":I
    .end local v2    # "id":Ljava/lang/String;
    .end local v4    # "map":Ljava/util/HashMap;, "Ljava/util/HashMap<**>;"
    :goto_1
    return v1

    .line 123
    .restart local v1    # "i":I
    .restart local v2    # "id":Ljava/lang/String;
    .restart local v4    # "map":Ljava/util/HashMap;, "Ljava/util/HashMap<**>;"
    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 132
    .end local v2    # "id":Ljava/lang/String;
    .end local v4    # "map":Ljava/util/HashMap;, "Ljava/util/HashMap<**>;"
    :cond_1
    const/4 v1, -0x1

    goto :goto_1
.end method

.method private static isSimilarTimeZone(Ljava/util/TimeZone;Ljava/util/TimeZone;)Z
    .locals 8
    .param p0, "tz1"    # Ljava/util/TimeZone;
    .param p1, "tz2"    # Ljava/util/TimeZone;

    .prologue
    const/4 v3, 0x1

    const/4 v4, 0x0

    .line 201
    invoke-virtual {p0}, Ljava/util/TimeZone;->getRawOffset()I

    move-result v5

    invoke-virtual {p1}, Ljava/util/TimeZone;->getRawOffset()I

    move-result v6

    if-ne v5, v6, :cond_0

    move v1, v3

    .line 202
    .local v1, "sameOffset":Z
    :goto_0
    invoke-virtual {p0}, Ljava/util/TimeZone;->getDSTSavings()I

    move-result v5

    invoke-virtual {p1}, Ljava/util/TimeZone;->getDSTSavings()I

    move-result v6

    if-ne v5, v6, :cond_1

    move v0, v3

    .line 203
    .local v0, "sameDST":Z
    :goto_1
    invoke-virtual {p0}, Ljava/util/TimeZone;->getID()Ljava/lang/String;

    move-result-object v5

    const-string v6, "/"

    invoke-virtual {v5, v6}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v5

    aget-object v5, v5, v4

    invoke-virtual {p1}, Ljava/util/TimeZone;->getID()Ljava/lang/String;

    move-result-object v6

    const-string v7, "/"

    invoke-virtual {v6, v7}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v6

    aget-object v6, v6, v4

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    .line 204
    .local v2, "sameRegion":Z
    if-eqz v1, :cond_2

    if-eqz v0, :cond_2

    if-eqz v2, :cond_2

    :goto_2
    return v3

    .end local v0    # "sameDST":Z
    .end local v1    # "sameOffset":Z
    .end local v2    # "sameRegion":Z
    :cond_0
    move v1, v4

    .line 201
    goto :goto_0

    .restart local v1    # "sameOffset":Z
    :cond_1
    move v0, v4

    .line 202
    goto :goto_1

    .restart local v0    # "sameDST":Z
    .restart local v2    # "sameRegion":Z
    :cond_2
    move v3, v4

    .line 204
    goto :goto_2
.end method

.method public static obtainTimeZoneFromItem(Ljava/lang/Object;)Ljava/util/TimeZone;
    .locals 1
    .param p0, "item"    # Ljava/lang/Object;

    .prologue
    .line 233
    check-cast p0, Ljava/util/Map;

    .end local p0    # "item":Ljava/lang/Object;
    const-string v0, "id"

    invoke-interface {p0, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Ljava/util/TimeZone;->getTimeZone(Ljava/lang/String;)Ljava/util/TimeZone;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public onActivityCreated(Landroid/os/Bundle;)V
    .locals 1
    .param p1, "savedInstanseState"    # Landroid/os/Bundle;

    .prologue
    .line 238
    invoke-super {p0, p1}, Landroid/app/ListFragment;->onActivityCreated(Landroid/os/Bundle;)V

    .line 240
    invoke-virtual {p0}, Lcom/google/android/setupwizard/time/ZonePicker;->getActivity()Landroid/app/Activity;

    move-result-object v0

    .line 241
    .local v0, "activity":Landroid/app/Activity;
    return-void
.end method

.method public onListItemClick(Landroid/widget/ListView;Landroid/view/View;IJ)V
    .locals 6
    .param p1, "listView"    # Landroid/widget/ListView;
    .param p2, "v"    # Landroid/view/View;
    .param p3, "position"    # I
    .param p4, "id"    # J

    .prologue
    .line 336
    invoke-virtual {p1, p3}, Landroid/widget/ListView;->getItemAtPosition(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/util/Map;

    .line 337
    .local v2, "map":Ljava/util/Map;, "Ljava/util/Map<**>;"
    const-string v5, "id"

    invoke-interface {v2, v5}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/String;

    .line 340
    .local v4, "tzId":Ljava/lang/String;
    invoke-virtual {p0}, Lcom/google/android/setupwizard/time/ZonePicker;->getActivity()Landroid/app/Activity;

    move-result-object v0

    .line 341
    .local v0, "activity":Landroid/app/Activity;
    const-string v5, "alarm"

    invoke-virtual {v0, v5}, Landroid/app/Activity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/app/AlarmManager;

    .line 342
    .local v1, "alarm":Landroid/app/AlarmManager;
    invoke-virtual {v1, v4}, Landroid/app/AlarmManager;->setTimeZone(Ljava/lang/String;)V

    .line 343
    invoke-static {v4}, Ljava/util/TimeZone;->getTimeZone(Ljava/lang/String;)Ljava/util/TimeZone;

    move-result-object v3

    .line 344
    .local v3, "tz":Ljava/util/TimeZone;
    iget-object v5, p0, Lcom/google/android/setupwizard/time/ZonePicker;->mListener:Lcom/google/android/setupwizard/time/ZonePicker$ZoneSelectionListener;

    if-eqz v5, :cond_0

    .line 345
    iget-object v5, p0, Lcom/google/android/setupwizard/time/ZonePicker;->mListener:Lcom/google/android/setupwizard/time/ZonePicker$ZoneSelectionListener;

    invoke-interface {v5, v3}, Lcom/google/android/setupwizard/time/ZonePicker$ZoneSelectionListener;->onZoneSelected(Ljava/util/TimeZone;)V

    .line 347
    :cond_0
    return-void
.end method
