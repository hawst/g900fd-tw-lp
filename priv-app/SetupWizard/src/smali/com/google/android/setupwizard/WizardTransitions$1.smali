.class final Lcom/google/android/setupwizard/WizardTransitions$1;
.super Ljava/lang/Object;
.source "WizardTransitions.java"

# interfaces
.implements Landroid/os/Parcelable$Creator;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/setupwizard/WizardTransitions;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Landroid/os/Parcelable$Creator",
        "<",
        "Lcom/google/android/setupwizard/WizardTransitions;",
        ">;"
    }
.end annotation


# direct methods
.method constructor <init>()V
    .locals 0

    .prologue
    .line 97
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public createFromParcel(Landroid/os/Parcel;)Lcom/google/android/setupwizard/WizardTransitions;
    .locals 5
    .param p1, "source"    # Landroid/os/Parcel;

    .prologue
    .line 101
    new-instance v2, Lcom/google/android/setupwizard/WizardTransitions;

    invoke-direct {v2}, Lcom/google/android/setupwizard/WizardTransitions;-><init>()V

    .line 102
    .local v2, "transitions":Lcom/google/android/setupwizard/WizardTransitions;
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v3

    # setter for: Lcom/google/android/setupwizard/WizardTransitions;->mDefaultAction:Ljava/lang/String;
    invoke-static {v2, v3}, Lcom/google/android/setupwizard/WizardTransitions;->access$002(Lcom/google/android/setupwizard/WizardTransitions;Ljava/lang/String;)Ljava/lang/String;

    .line 103
    const/4 v3, 0x0

    invoke-virtual {p1, v3}, Landroid/os/Parcel;->readSparseArray(Ljava/lang/ClassLoader;)Landroid/util/SparseArray;

    move-result-object v0

    .line 104
    .local v0, "actions":Landroid/util/SparseArray;, "Landroid/util/SparseArray<Ljava/lang/String;>;"
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    invoke-virtual {v0}, Landroid/util/SparseArray;->size()I

    move-result v3

    if-ge v1, v3, :cond_0

    .line 105
    invoke-virtual {v0, v1}, Landroid/util/SparseArray;->keyAt(I)I

    move-result v3

    invoke-virtual {v0, v1}, Landroid/util/SparseArray;->valueAt(I)Ljava/lang/Object;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, Lcom/google/android/setupwizard/WizardTransitions;->put(ILjava/lang/Object;)V

    .line 104
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 107
    :cond_0
    return-object v2
.end method

.method public bridge synthetic createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;
    .locals 1
    .param p1, "x0"    # Landroid/os/Parcel;

    .prologue
    .line 97
    invoke-virtual {p0, p1}, Lcom/google/android/setupwizard/WizardTransitions$1;->createFromParcel(Landroid/os/Parcel;)Lcom/google/android/setupwizard/WizardTransitions;

    move-result-object v0

    return-object v0
.end method

.method public newArray(I)[Lcom/google/android/setupwizard/WizardTransitions;
    .locals 1
    .param p1, "size"    # I

    .prologue
    .line 112
    new-array v0, p1, [Lcom/google/android/setupwizard/WizardTransitions;

    return-object v0
.end method

.method public bridge synthetic newArray(I)[Ljava/lang/Object;
    .locals 1
    .param p1, "x0"    # I

    .prologue
    .line 97
    invoke-virtual {p0, p1}, Lcom/google/android/setupwizard/WizardTransitions$1;->newArray(I)[Lcom/google/android/setupwizard/WizardTransitions;

    move-result-object v0

    return-object v0
.end method
