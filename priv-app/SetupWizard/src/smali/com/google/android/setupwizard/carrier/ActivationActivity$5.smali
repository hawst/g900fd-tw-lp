.class Lcom/google/android/setupwizard/carrier/ActivationActivity$5;
.super Landroid/content/BroadcastReceiver;
.source "ActivationActivity.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/setupwizard/carrier/ActivationActivity;->onCreate(Landroid/os/Bundle;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/setupwizard/carrier/ActivationActivity;


# direct methods
.method constructor <init>(Lcom/google/android/setupwizard/carrier/ActivationActivity;)V
    .locals 0

    .prologue
    .line 523
    iput-object p1, p0, Lcom/google/android/setupwizard/carrier/ActivationActivity$5;->this$0:Lcom/google/android/setupwizard/carrier/ActivationActivity;

    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    return-void
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 6
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "intent"    # Landroid/content/Intent;

    .prologue
    .line 526
    iget-object v0, p0, Lcom/google/android/setupwizard/carrier/ActivationActivity$5;->this$0:Lcom/google/android/setupwizard/carrier/ActivationActivity;

    # invokes: Lcom/google/android/setupwizard/carrier/ActivationActivity;->getNetworkNameFromIntent(Landroid/content/Intent;)Ljava/lang/CharSequence;
    invoke-static {p2}, Lcom/google/android/setupwizard/carrier/ActivationActivity;->access$1300(Landroid/content/Intent;)Ljava/lang/CharSequence;

    move-result-object v1

    # setter for: Lcom/google/android/setupwizard/carrier/ActivationActivity;->mNetworkName:Ljava/lang/CharSequence;
    invoke-static {v0, v1}, Lcom/google/android/setupwizard/carrier/ActivationActivity;->access$1202(Lcom/google/android/setupwizard/carrier/ActivationActivity;Ljava/lang/CharSequence;)Ljava/lang/CharSequence;

    .line 527
    iget-object v0, p0, Lcom/google/android/setupwizard/carrier/ActivationActivity$5;->this$0:Lcom/google/android/setupwizard/carrier/ActivationActivity;

    # invokes: Lcom/google/android/setupwizard/carrier/ActivationActivity;->shouldChangeTitleUsingNetworkName()Z
    invoke-static {v0}, Lcom/google/android/setupwizard/carrier/ActivationActivity;->access$1400(Lcom/google/android/setupwizard/carrier/ActivationActivity;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 528
    iget-object v0, p0, Lcom/google/android/setupwizard/carrier/ActivationActivity$5;->this$0:Lcom/google/android/setupwizard/carrier/ActivationActivity;

    # getter for: Lcom/google/android/setupwizard/carrier/ActivationActivity;->mActivationTitle:Landroid/widget/TextView;
    invoke-static {v0}, Lcom/google/android/setupwizard/carrier/ActivationActivity;->access$1500(Lcom/google/android/setupwizard/carrier/ActivationActivity;)Landroid/widget/TextView;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/setupwizard/carrier/ActivationActivity$5;->this$0:Lcom/google/android/setupwizard/carrier/ActivationActivity;

    const v2, 0x7f070056

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    iget-object v5, p0, Lcom/google/android/setupwizard/carrier/ActivationActivity$5;->this$0:Lcom/google/android/setupwizard/carrier/ActivationActivity;

    # getter for: Lcom/google/android/setupwizard/carrier/ActivationActivity;->mNetworkName:Ljava/lang/CharSequence;
    invoke-static {v5}, Lcom/google/android/setupwizard/carrier/ActivationActivity;->access$1200(Lcom/google/android/setupwizard/carrier/ActivationActivity;)Ljava/lang/CharSequence;

    move-result-object v5

    aput-object v5, v3, v4

    invoke-virtual {v1, v2, v3}, Lcom/google/android/setupwizard/carrier/ActivationActivity;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 531
    :cond_0
    return-void
.end method
