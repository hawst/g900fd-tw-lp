.class public Lcom/google/android/setupwizard/carrier/MobileDataActivity;
.super Lcom/google/android/setupwizard/BaseActivity;
.source "MobileDataActivity.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/setupwizard/carrier/MobileDataActivity$HfaChangeReceiver;
    }
.end annotation


# static fields
.field private static sCurrentActivity:Lcom/google/android/setupwizard/carrier/MobileDataActivity;


# instance fields
.field private final TEST_SUCCESS_DELAY:I

.field private final mCheckActivation:Ljava/lang/Runnable;

.field private final mCheckConnection:Ljava/lang/Runnable;

.field private final mConnectionCheckedReceiver:Landroid/content/BroadcastReceiver;

.field private mEndTime:J

.field private final mHandler:Landroid/os/Handler;

.field private final mPhoneStateListener:Landroid/telephony/PhoneStateListener;

.field private mStartTime:J

.field private mState:I

.field private final mTestSuccess:Ljava/lang/Runnable;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 52
    invoke-direct {p0}, Lcom/google/android/setupwizard/BaseActivity;-><init>()V

    .line 75
    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    iput-object v0, p0, Lcom/google/android/setupwizard/carrier/MobileDataActivity;->mHandler:Landroid/os/Handler;

    .line 76
    new-instance v0, Lcom/google/android/setupwizard/carrier/MobileDataActivity$1;

    invoke-direct {v0, p0}, Lcom/google/android/setupwizard/carrier/MobileDataActivity$1;-><init>(Lcom/google/android/setupwizard/carrier/MobileDataActivity;)V

    iput-object v0, p0, Lcom/google/android/setupwizard/carrier/MobileDataActivity;->mCheckActivation:Ljava/lang/Runnable;

    .line 104
    new-instance v0, Lcom/google/android/setupwizard/carrier/MobileDataActivity$2;

    invoke-direct {v0, p0}, Lcom/google/android/setupwizard/carrier/MobileDataActivity$2;-><init>(Lcom/google/android/setupwizard/carrier/MobileDataActivity;)V

    iput-object v0, p0, Lcom/google/android/setupwizard/carrier/MobileDataActivity;->mCheckConnection:Ljava/lang/Runnable;

    .line 116
    const/16 v0, 0x2710

    iput v0, p0, Lcom/google/android/setupwizard/carrier/MobileDataActivity;->TEST_SUCCESS_DELAY:I

    .line 117
    new-instance v0, Lcom/google/android/setupwizard/carrier/MobileDataActivity$3;

    invoke-direct {v0, p0}, Lcom/google/android/setupwizard/carrier/MobileDataActivity$3;-><init>(Lcom/google/android/setupwizard/carrier/MobileDataActivity;)V

    iput-object v0, p0, Lcom/google/android/setupwizard/carrier/MobileDataActivity;->mTestSuccess:Ljava/lang/Runnable;

    .line 138
    const/4 v0, 0x0

    iput v0, p0, Lcom/google/android/setupwizard/carrier/MobileDataActivity;->mState:I

    .line 146
    new-instance v0, Lcom/google/android/setupwizard/carrier/MobileDataActivity$4;

    invoke-direct {v0, p0}, Lcom/google/android/setupwizard/carrier/MobileDataActivity$4;-><init>(Lcom/google/android/setupwizard/carrier/MobileDataActivity;)V

    iput-object v0, p0, Lcom/google/android/setupwizard/carrier/MobileDataActivity;->mConnectionCheckedReceiver:Landroid/content/BroadcastReceiver;

    .line 161
    new-instance v0, Lcom/google/android/setupwizard/carrier/MobileDataActivity$5;

    invoke-direct {v0, p0}, Lcom/google/android/setupwizard/carrier/MobileDataActivity$5;-><init>(Lcom/google/android/setupwizard/carrier/MobileDataActivity;)V

    iput-object v0, p0, Lcom/google/android/setupwizard/carrier/MobileDataActivity;->mPhoneStateListener:Landroid/telephony/PhoneStateListener;

    .line 189
    return-void
.end method

.method static synthetic access$000(Lcom/google/android/setupwizard/carrier/MobileDataActivity;)I
    .locals 1
    .param p0, "x0"    # Lcom/google/android/setupwizard/carrier/MobileDataActivity;

    .prologue
    .line 52
    iget v0, p0, Lcom/google/android/setupwizard/carrier/MobileDataActivity;->mState:I

    return v0
.end method

.method static synthetic access$100(Lcom/google/android/setupwizard/carrier/MobileDataActivity;)J
    .locals 2
    .param p0, "x0"    # Lcom/google/android/setupwizard/carrier/MobileDataActivity;

    .prologue
    .line 52
    iget-wide v0, p0, Lcom/google/android/setupwizard/carrier/MobileDataActivity;->mEndTime:J

    return-wide v0
.end method

.method static synthetic access$1000(Lcom/google/android/setupwizard/carrier/MobileDataActivity;I)V
    .locals 0
    .param p0, "x0"    # Lcom/google/android/setupwizard/carrier/MobileDataActivity;
    .param p1, "x1"    # I

    .prologue
    .line 52
    invoke-virtual {p0, p1}, Lcom/google/android/setupwizard/carrier/MobileDataActivity;->finishAction(I)V

    return-void
.end method

.method static synthetic access$1100(Lcom/google/android/setupwizard/carrier/MobileDataActivity;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/setupwizard/carrier/MobileDataActivity;

    .prologue
    .line 52
    iget-object v0, p0, Lcom/google/android/setupwizard/carrier/MobileDataActivity;->TAG:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$1200(Lcom/google/android/setupwizard/carrier/MobileDataActivity;)V
    .locals 0
    .param p0, "x0"    # Lcom/google/android/setupwizard/carrier/MobileDataActivity;

    .prologue
    .line 52
    invoke-direct {p0}, Lcom/google/android/setupwizard/carrier/MobileDataActivity;->waitForDataService()V

    return-void
.end method

.method static synthetic access$1300(Lcom/google/android/setupwizard/carrier/MobileDataActivity;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/setupwizard/carrier/MobileDataActivity;

    .prologue
    .line 52
    iget-object v0, p0, Lcom/google/android/setupwizard/carrier/MobileDataActivity;->TAG:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$1400(Lcom/google/android/setupwizard/carrier/MobileDataActivity;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/setupwizard/carrier/MobileDataActivity;

    .prologue
    .line 52
    iget-object v0, p0, Lcom/google/android/setupwizard/carrier/MobileDataActivity;->TAG:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$1500()Lcom/google/android/setupwizard/carrier/MobileDataActivity;
    .locals 1

    .prologue
    .line 52
    sget-object v0, Lcom/google/android/setupwizard/carrier/MobileDataActivity;->sCurrentActivity:Lcom/google/android/setupwizard/carrier/MobileDataActivity;

    return-object v0
.end method

.method static synthetic access$1600(Lcom/google/android/setupwizard/carrier/MobileDataActivity;I)V
    .locals 0
    .param p0, "x0"    # Lcom/google/android/setupwizard/carrier/MobileDataActivity;
    .param p1, "x1"    # I

    .prologue
    .line 52
    invoke-direct {p0, p1}, Lcom/google/android/setupwizard/carrier/MobileDataActivity;->onHfaStatusChange(I)V

    return-void
.end method

.method static synthetic access$1700(Lcom/google/android/setupwizard/carrier/MobileDataActivity;I)V
    .locals 0
    .param p0, "x0"    # Lcom/google/android/setupwizard/carrier/MobileDataActivity;
    .param p1, "x1"    # I

    .prologue
    .line 52
    invoke-direct {p0, p1}, Lcom/google/android/setupwizard/carrier/MobileDataActivity;->onFinish(I)V

    return-void
.end method

.method static synthetic access$1800(Lcom/google/android/setupwizard/carrier/MobileDataActivity;)V
    .locals 0
    .param p0, "x0"    # Lcom/google/android/setupwizard/carrier/MobileDataActivity;

    .prologue
    .line 52
    invoke-direct {p0}, Lcom/google/android/setupwizard/carrier/MobileDataActivity;->onRetry()V

    return-void
.end method

.method static synthetic access$200(Lcom/google/android/setupwizard/carrier/MobileDataActivity;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/setupwizard/carrier/MobileDataActivity;

    .prologue
    .line 52
    iget-object v0, p0, Lcom/google/android/setupwizard/carrier/MobileDataActivity;->TAG:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$300(Lcom/google/android/setupwizard/carrier/MobileDataActivity;)V
    .locals 0
    .param p0, "x0"    # Lcom/google/android/setupwizard/carrier/MobileDataActivity;

    .prologue
    .line 52
    invoke-direct {p0}, Lcom/google/android/setupwizard/carrier/MobileDataActivity;->onSuccess()V

    return-void
.end method

.method static synthetic access$400(Lcom/google/android/setupwizard/carrier/MobileDataActivity;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/setupwizard/carrier/MobileDataActivity;

    .prologue
    .line 52
    iget-object v0, p0, Lcom/google/android/setupwizard/carrier/MobileDataActivity;->TAG:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$500(Lcom/google/android/setupwizard/carrier/MobileDataActivity;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/setupwizard/carrier/MobileDataActivity;

    .prologue
    .line 52
    iget-object v0, p0, Lcom/google/android/setupwizard/carrier/MobileDataActivity;->TAG:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$600(Lcom/google/android/setupwizard/carrier/MobileDataActivity;)Ljava/lang/Runnable;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/setupwizard/carrier/MobileDataActivity;

    .prologue
    .line 52
    iget-object v0, p0, Lcom/google/android/setupwizard/carrier/MobileDataActivity;->mCheckConnection:Ljava/lang/Runnable;

    return-object v0
.end method

.method static synthetic access$700(Lcom/google/android/setupwizard/carrier/MobileDataActivity;)Landroid/os/Handler;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/setupwizard/carrier/MobileDataActivity;

    .prologue
    .line 52
    iget-object v0, p0, Lcom/google/android/setupwizard/carrier/MobileDataActivity;->mHandler:Landroid/os/Handler;

    return-object v0
.end method

.method static synthetic access$800(Lcom/google/android/setupwizard/carrier/MobileDataActivity;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/setupwizard/carrier/MobileDataActivity;

    .prologue
    .line 52
    iget-object v0, p0, Lcom/google/android/setupwizard/carrier/MobileDataActivity;->TAG:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$900(Lcom/google/android/setupwizard/carrier/MobileDataActivity;)Ljava/lang/Runnable;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/setupwizard/carrier/MobileDataActivity;

    .prologue
    .line 52
    iget-object v0, p0, Lcom/google/android/setupwizard/carrier/MobileDataActivity;->mCheckActivation:Ljava/lang/Runnable;

    return-object v0
.end method

.method private static getStateName(I)Ljava/lang/String;
    .locals 3
    .param p0, "state"    # I

    .prologue
    .line 282
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 283
    .local v0, "sb":Ljava/lang/StringBuilder;
    packed-switch p0, :pswitch_data_0

    .line 291
    :goto_0
    :pswitch_0
    const-string v1, "("

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ")"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 292
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    return-object v1

    .line 284
    :pswitch_1
    const-string v1, "STATE_NOT_STARTED"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_0

    .line 285
    :pswitch_2
    const-string v1, "STATE_ACTIVATING"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_0

    .line 286
    :pswitch_3
    const-string v1, "STATE_CHECKING_CONNECTION"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_0

    .line 287
    :pswitch_4
    const-string v1, "STATE_FINISHED"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_0

    .line 288
    :pswitch_5
    const-string v1, "STATE_FAILED"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_0

    .line 289
    :pswitch_6
    const-string v1, "STATE_SKIPPED"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_0

    .line 283
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_0
        :pswitch_2
        :pswitch_3
        :pswitch_0
        :pswitch_4
        :pswitch_5
        :pswitch_6
    .end packed-switch
.end method

.method private onFailure()V
    .locals 3

    .prologue
    .line 475
    iget-object v0, p0, Lcom/google/android/setupwizard/carrier/MobileDataActivity;->TAG:Ljava/lang/String;

    const-string v1, "onFailure"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 476
    iget-object v0, p0, Lcom/google/android/setupwizard/carrier/MobileDataActivity;->mHandler:Landroid/os/Handler;

    iget-object v1, p0, Lcom/google/android/setupwizard/carrier/MobileDataActivity;->mCheckActivation:Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    .line 477
    const/4 v0, 0x6

    const-string v1, "onFailure"

    invoke-direct {p0, v0, v1}, Lcom/google/android/setupwizard/carrier/MobileDataActivity;->setState(ILjava/lang/String;)V

    .line 478
    new-instance v0, Landroid/app/AlertDialog$Builder;

    invoke-direct {v0, p0}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    const v1, 0x7f070068

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    const v1, 0x7f070069

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setMessage(I)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    const v1, 0x7f07004c

    new-instance v2, Lcom/google/android/setupwizard/carrier/MobileDataActivity$7;

    invoke-direct {v2, p0}, Lcom/google/android/setupwizard/carrier/MobileDataActivity$7;-><init>(Lcom/google/android/setupwizard/carrier/MobileDataActivity;)V

    invoke-virtual {v0, v1, v2}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    const v1, 0x7f070046

    new-instance v2, Lcom/google/android/setupwizard/carrier/MobileDataActivity$6;

    invoke-direct {v2, p0}, Lcom/google/android/setupwizard/carrier/MobileDataActivity$6;-><init>(Lcom/google/android/setupwizard/carrier/MobileDataActivity;)V

    invoke-virtual {v0, v1, v2}, Landroid/app/AlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setCancelable(Z)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->show()Landroid/app/AlertDialog;

    .line 499
    return-void
.end method

.method private onFinish(I)V
    .locals 3
    .param p1, "result"    # I

    .prologue
    .line 462
    iget-object v0, p0, Lcom/google/android/setupwizard/carrier/MobileDataActivity;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "onFinish("

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const/16 v2, 0x2726

    invoke-static {v2, p1}, Lcom/google/android/setupwizard/carrier/MobileDataActivity;->getResultName(II)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ")"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 464
    const/4 v0, 0x5

    const-string v1, "onFinish"

    invoke-direct {p0, v0, v1}, Lcom/google/android/setupwizard/carrier/MobileDataActivity;->setState(ILjava/lang/String;)V

    .line 465
    invoke-virtual {p0, p1}, Lcom/google/android/setupwizard/carrier/MobileDataActivity;->finishAction(I)V

    .line 466
    return-void
.end method

.method private onHfaStatusChange(I)V
    .locals 3
    .param p1, "hfaStatus"    # I

    .prologue
    .line 377
    iget-object v0, p0, Lcom/google/android/setupwizard/carrier/MobileDataActivity;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "onHfaStatusChange("

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ") "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-static {}, Lcom/google/android/setupwizard/carrier/PhoneMonitor;->getPhoneState()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 380
    const/4 v0, 0x2

    if-ne p1, v0, :cond_1

    .line 382
    iget-object v0, p0, Lcom/google/android/setupwizard/carrier/MobileDataActivity;->TAG:Ljava/lang/String;

    const-string v1, "HFA success; wait for data service"

    invoke-static {v0, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 384
    invoke-direct {p0}, Lcom/google/android/setupwizard/carrier/MobileDataActivity;->waitForDataService()V

    .line 389
    :cond_0
    :goto_0
    return-void

    .line 385
    :cond_1
    const/4 v0, 0x3

    if-ne p1, v0, :cond_0

    .line 386
    iget-object v0, p0, Lcom/google/android/setupwizard/carrier/MobileDataActivity;->TAG:Ljava/lang/String;

    const-string v1, "HFA failure; showing user dialog"

    invoke-static {v0, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 387
    invoke-direct {p0}, Lcom/google/android/setupwizard/carrier/MobileDataActivity;->onFailure()V

    goto :goto_0
.end method

.method private onRetry()V
    .locals 2

    .prologue
    .line 469
    iget-object v0, p0, Lcom/google/android/setupwizard/carrier/MobileDataActivity;->TAG:Ljava/lang/String;

    const-string v1, "onRetry"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 470
    const/4 v0, 0x0

    const-string v1, "onRetry"

    invoke-direct {p0, v0, v1}, Lcom/google/android/setupwizard/carrier/MobileDataActivity;->setState(ILjava/lang/String;)V

    .line 471
    invoke-virtual {p0}, Lcom/google/android/setupwizard/carrier/MobileDataActivity;->recreate()V

    .line 472
    return-void
.end method

.method private onSkip()V
    .locals 3

    .prologue
    .line 502
    iget-object v0, p0, Lcom/google/android/setupwizard/carrier/MobileDataActivity;->TAG:Ljava/lang/String;

    const-string v1, "onSkip"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 504
    iget-object v0, p0, Lcom/google/android/setupwizard/carrier/MobileDataActivity;->mHandler:Landroid/os/Handler;

    iget-object v1, p0, Lcom/google/android/setupwizard/carrier/MobileDataActivity;->mCheckActivation:Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    .line 505
    const/4 v0, 0x7

    const-string v1, "onSkip"

    invoke-direct {p0, v0, v1}, Lcom/google/android/setupwizard/carrier/MobileDataActivity;->setState(ILjava/lang/String;)V

    .line 506
    new-instance v0, Landroid/app/AlertDialog$Builder;

    invoke-direct {v0, p0}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    const v1, 0x7f070074

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setMessage(I)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    const v1, 0x7f070048

    new-instance v2, Lcom/google/android/setupwizard/carrier/MobileDataActivity$9;

    invoke-direct {v2, p0}, Lcom/google/android/setupwizard/carrier/MobileDataActivity$9;-><init>(Lcom/google/android/setupwizard/carrier/MobileDataActivity;)V

    invoke-virtual {v0, v1, v2}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    const v1, 0x7f070047

    new-instance v2, Lcom/google/android/setupwizard/carrier/MobileDataActivity$8;

    invoke-direct {v2, p0}, Lcom/google/android/setupwizard/carrier/MobileDataActivity$8;-><init>(Lcom/google/android/setupwizard/carrier/MobileDataActivity;)V

    invoke-virtual {v0, v1, v2}, Landroid/app/AlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setCancelable(Z)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->show()Landroid/app/AlertDialog;

    .line 526
    return-void
.end method

.method private onSuccess()V
    .locals 2

    .prologue
    .line 456
    iget-object v0, p0, Lcom/google/android/setupwizard/carrier/MobileDataActivity;->TAG:Ljava/lang/String;

    const-string v1, "onSuccess"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 457
    iget-object v0, p0, Lcom/google/android/setupwizard/carrier/MobileDataActivity;->mHandler:Landroid/os/Handler;

    iget-object v1, p0, Lcom/google/android/setupwizard/carrier/MobileDataActivity;->mCheckActivation:Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    .line 458
    const/4 v0, -0x1

    invoke-direct {p0, v0}, Lcom/google/android/setupwizard/carrier/MobileDataActivity;->onFinish(I)V

    .line 459
    return-void
.end method

.method private setState(ILjava/lang/String;)V
    .locals 3
    .param p1, "state"    # I
    .param p2, "functionName"    # Ljava/lang/String;

    .prologue
    .line 141
    iget-object v0, p0, Lcom/google/android/setupwizard/carrier/MobileDataActivity;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "setState(): "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    if-eqz p2, :cond_0

    .end local p2    # "functionName":Ljava/lang/String;
    :goto_0
    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/google/android/setupwizard/carrier/MobileDataActivity;->mState:I

    invoke-static {v2}, Lcom/google/android/setupwizard/carrier/MobileDataActivity;->getStateName(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " to "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-static {p1}, Lcom/google/android/setupwizard/carrier/MobileDataActivity;->getStateName(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 143
    iput p1, p0, Lcom/google/android/setupwizard/carrier/MobileDataActivity;->mState:I

    .line 144
    return-void

    .line 141
    .restart local p2    # "functionName":Ljava/lang/String;
    :cond_0
    const-string p2, ""

    goto :goto_0
.end method

.method private startActivationProcess()V
    .locals 11

    .prologue
    const/4 v10, 0x0

    .line 396
    const/4 v5, 0x2

    const-string v6, "startActivationProcess"

    invoke-direct {p0, v5, v6}, Lcom/google/android/setupwizard/carrier/MobileDataActivity;->setState(ILjava/lang/String;)V

    .line 403
    invoke-static {}, Lcom/google/android/setupwizard/carrier/PhoneMonitor;->getActivationTimeout()I

    move-result v4

    .line 404
    .local v4, "timeout":I
    iget-object v5, p0, Lcom/google/android/setupwizard/carrier/MobileDataActivity;->TAG:Ljava/lang/String;

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "setting activation timeout in "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, " ms"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 405
    if-eqz v4, :cond_0

    .line 406
    iget-wide v6, p0, Lcom/google/android/setupwizard/carrier/MobileDataActivity;->mStartTime:J

    int-to-long v8, v4

    add-long/2addr v6, v8

    iput-wide v6, p0, Lcom/google/android/setupwizard/carrier/MobileDataActivity;->mEndTime:J

    .line 407
    iget-object v5, p0, Lcom/google/android/setupwizard/carrier/MobileDataActivity;->mHandler:Landroid/os/Handler;

    iget-object v6, p0, Lcom/google/android/setupwizard/carrier/MobileDataActivity;->mCheckActivation:Ljava/lang/Runnable;

    int-to-long v8, v4

    invoke-virtual {v5, v6, v8, v9}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 415
    :cond_0
    iget-object v5, p0, Lcom/google/android/setupwizard/carrier/MobileDataActivity;->TAG:Ljava/lang/String;

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "startActivationProcess() "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-static {}, Lcom/google/android/setupwizard/carrier/PhoneMonitor;->getPhoneState()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 417
    new-instance v3, Landroid/content/Intent;

    const-string v5, "com.google.android.setupwizard.HFA_CHANGE"

    invoke-direct {v3, v5}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 418
    .local v3, "returnIntent":Landroid/content/Intent;
    invoke-static {p0, v10, v3, v10}, Landroid/app/PendingIntent;->getBroadcast(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v2

    .line 419
    .local v2, "pending":Landroid/app/PendingIntent;
    new-instance v1, Landroid/content/Intent;

    const-string v5, "com.android.phone.PERFORM_CDMA_PROVISIONING"

    invoke-direct {v1, v5}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 420
    .local v1, "newIntent":Landroid/content/Intent;
    const-string v5, "otasp_result_code_pending_intent"

    invoke-virtual {v1, v5, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 423
    :try_start_0
    iget-object v5, p0, Lcom/google/android/setupwizard/carrier/MobileDataActivity;->TAG:Ljava/lang/String;

    const-string v6, "startActivationProcess() ACTION_PERFORM_CDMA_PROVISIONING"

    invoke-static {v5, v6}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 424
    invoke-virtual {p0, v1}, Lcom/google/android/setupwizard/carrier/MobileDataActivity;->startFirstRunActivity(Landroid/content/Intent;)V
    :try_end_0
    .catch Landroid/content/ActivityNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    .line 430
    :goto_0
    return-void

    .line 425
    :catch_0
    move-exception v0

    .line 426
    .local v0, "e":Landroid/content/ActivityNotFoundException;
    iget-object v5, p0, Lcom/google/android/setupwizard/carrier/MobileDataActivity;->TAG:Ljava/lang/String;

    const-string v6, "startActivationProcess() Failed to launch 10023"

    invoke-static {v5, v6, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 428
    invoke-direct {p0}, Lcom/google/android/setupwizard/carrier/MobileDataActivity;->onFailure()V

    goto :goto_0
.end method

.method private waitForDataService()V
    .locals 6

    .prologue
    .line 433
    const/4 v1, 0x3

    const-string v2, "waitForDataService"

    invoke-direct {p0, v1, v2}, Lcom/google/android/setupwizard/carrier/MobileDataActivity;->setState(ILjava/lang/String;)V

    .line 436
    iget-object v1, p0, Lcom/google/android/setupwizard/carrier/MobileDataActivity;->mHandler:Landroid/os/Handler;

    iget-object v2, p0, Lcom/google/android/setupwizard/carrier/MobileDataActivity;->mCheckActivation:Ljava/lang/Runnable;

    invoke-virtual {v1, v2}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    .line 437
    invoke-static {}, Lcom/google/android/setupwizard/carrier/PhoneMonitor;->getMobileDataTimeout()I

    move-result v0

    .line 438
    .local v0, "timeout":I
    iget-object v1, p0, Lcom/google/android/setupwizard/carrier/MobileDataActivity;->TAG:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "setting data timeout in "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " ms"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 439
    if-nez v0, :cond_0

    .line 441
    invoke-direct {p0}, Lcom/google/android/setupwizard/carrier/MobileDataActivity;->onSuccess()V

    .line 453
    :goto_0
    return-void

    .line 445
    :cond_0
    iget-wide v2, p0, Lcom/google/android/setupwizard/carrier/MobileDataActivity;->mStartTime:J

    int-to-long v4, v0

    add-long/2addr v2, v4

    iput-wide v2, p0, Lcom/google/android/setupwizard/carrier/MobileDataActivity;->mEndTime:J

    .line 446
    iget-object v1, p0, Lcom/google/android/setupwizard/carrier/MobileDataActivity;->mHandler:Landroid/os/Handler;

    iget-object v2, p0, Lcom/google/android/setupwizard/carrier/MobileDataActivity;->mCheckActivation:Ljava/lang/Runnable;

    int-to-long v4, v0

    invoke-virtual {v1, v2, v4, v5}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 448
    iget-object v1, p0, Lcom/google/android/setupwizard/carrier/MobileDataActivity;->mConnectionCheckedReceiver:Landroid/content/BroadcastReceiver;

    invoke-static {p0, v1}, Lcom/google/android/setupwizard/network/CheckConnectionTask;->register(Landroid/content/Context;Landroid/content/BroadcastReceiver;)V

    .line 451
    iget-object v1, p0, Lcom/google/android/setupwizard/carrier/MobileDataActivity;->mHandler:Landroid/os/Handler;

    iget-object v2, p0, Lcom/google/android/setupwizard/carrier/MobileDataActivity;->mCheckConnection:Ljava/lang/Runnable;

    invoke-virtual {v1, v2}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    goto :goto_0
.end method


# virtual methods
.method protected getNextTransition()I
    .locals 1

    .prologue
    .line 372
    const/4 v0, 0x1

    return v0
.end method

.method protected getPreviousTransition()I
    .locals 1

    .prologue
    .line 366
    const/4 v0, 0x2

    return v0
.end method

.method protected onActivityResult(IILandroid/content/Intent;)V
    .locals 3
    .param p1, "requestCode"    # I
    .param p2, "resultCode"    # I
    .param p3, "data"    # Landroid/content/Intent;

    .prologue
    .line 347
    iget-object v0, p0, Lcom/google/android/setupwizard/carrier/MobileDataActivity;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "onActivityResult("

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ")"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 348
    const/16 v0, 0x4e21

    if-eq p1, v0, :cond_0

    .line 349
    invoke-super {p0, p1, p2, p3}, Lcom/google/android/setupwizard/BaseActivity;->onActivityResult(IILandroid/content/Intent;)V

    .line 351
    :cond_0
    return-void
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 6
    .param p1, "icicle"    # Landroid/os/Bundle;

    .prologue
    .line 212
    invoke-super {p0, p1}, Lcom/google/android/setupwizard/BaseActivity;->onCreate(Landroid/os/Bundle;)V

    .line 214
    invoke-virtual {p0}, Lcom/google/android/setupwizard/carrier/MobileDataActivity;->getDecorView()Landroid/view/View;

    move-result-object v0

    const/high16 v1, 0x1e40000

    invoke-virtual {v0, v1}, Landroid/view/View;->setSystemUiVisibility(I)V

    .line 221
    iget-object v0, p0, Lcom/google/android/setupwizard/carrier/MobileDataActivity;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "onCreate: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Lcom/google/android/setupwizard/carrier/MobileDataActivity;->getIntent()Landroid/content/Intent;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Lcom/google/android/setupwizard/carrier/MobileDataActivity;->getIntent()Landroid/content/Intent;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 223
    if-nez p1, :cond_0

    .line 224
    const/4 v0, 0x0

    const-string v1, "onCreate"

    invoke-direct {p0, v0, v1}, Lcom/google/android/setupwizard/carrier/MobileDataActivity;->setState(ILjava/lang/String;)V

    .line 225
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/google/android/setupwizard/carrier/MobileDataActivity;->mStartTime:J

    .line 236
    :cond_0
    if-eqz p1, :cond_1

    .line 237
    const-string v0, "prevState"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v0

    const-string v1, "onCreate icicle"

    invoke-direct {p0, v0, v1}, Lcom/google/android/setupwizard/carrier/MobileDataActivity;->setState(ILjava/lang/String;)V

    .line 238
    const-string v0, "starttime"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getLong(Ljava/lang/String;)J

    move-result-wide v0

    iput-wide v0, p0, Lcom/google/android/setupwizard/carrier/MobileDataActivity;->mStartTime:J

    .line 239
    const-string v0, "endtime"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getLong(Ljava/lang/String;)J

    move-result-wide v0

    iput-wide v0, p0, Lcom/google/android/setupwizard/carrier/MobileDataActivity;->mEndTime:J

    .line 241
    iget-wide v0, p0, Lcom/google/android/setupwizard/carrier/MobileDataActivity;->mEndTime:J

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-lez v0, :cond_1

    .line 242
    iget-object v0, p0, Lcom/google/android/setupwizard/carrier/MobileDataActivity;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "setting activation timeout at "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-wide v2, p0, Lcom/google/android/setupwizard/carrier/MobileDataActivity;->mEndTime:J

    invoke-virtual {v1, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " ms"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 243
    iget-object v0, p0, Lcom/google/android/setupwizard/carrier/MobileDataActivity;->mHandler:Landroid/os/Handler;

    iget-object v1, p0, Lcom/google/android/setupwizard/carrier/MobileDataActivity;->mCheckActivation:Ljava/lang/Runnable;

    iget-wide v2, p0, Lcom/google/android/setupwizard/carrier/MobileDataActivity;->mEndTime:J

    iget-wide v4, p0, Lcom/google/android/setupwizard/carrier/MobileDataActivity;->mStartTime:J

    sub-long/2addr v2, v4

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 247
    :cond_1
    const v0, 0x7f03000e

    const v1, 0x7f030023

    invoke-virtual {p0, v0, v1}, Lcom/google/android/setupwizard/carrier/MobileDataActivity;->setTemplateContent(II)V

    .line 248
    const v0, 0x7f070054

    invoke-virtual {p0, v0}, Lcom/google/android/setupwizard/carrier/MobileDataActivity;->setHeaderText(I)V

    .line 250
    iget-object v0, p0, Lcom/google/android/setupwizard/carrier/MobileDataActivity;->TAG:Ljava/lang/String;

    const-string v1, "onCreate: X"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 251
    return-void
.end method

.method protected onDestroy()V
    .locals 2

    .prologue
    .line 268
    invoke-super {p0}, Lcom/google/android/setupwizard/BaseActivity;->onDestroy()V

    .line 269
    const/4 v0, 0x0

    sput-object v0, Lcom/google/android/setupwizard/carrier/MobileDataActivity;->sCurrentActivity:Lcom/google/android/setupwizard/carrier/MobileDataActivity;

    .line 270
    iget-object v0, p0, Lcom/google/android/setupwizard/carrier/MobileDataActivity;->mHandler:Landroid/os/Handler;

    iget-object v1, p0, Lcom/google/android/setupwizard/carrier/MobileDataActivity;->mCheckActivation:Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    .line 271
    return-void
.end method

.method public onNavigationBarCreated(Lcom/android/setupwizard/navigationbar/SetupWizardNavBar;)V
    .locals 2
    .param p1, "bar"    # Lcom/android/setupwizard/navigationbar/SetupWizardNavBar;

    .prologue
    .line 355
    invoke-super {p0, p1}, Lcom/google/android/setupwizard/BaseActivity;->onNavigationBarCreated(Lcom/android/setupwizard/navigationbar/SetupWizardNavBar;)V

    .line 356
    invoke-virtual {p1}, Lcom/android/setupwizard/navigationbar/SetupWizardNavBar;->getNextButton()Landroid/widget/Button;

    move-result-object v0

    const v1, 0x7f070046

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setText(I)V

    .line 357
    return-void
.end method

.method public onNewIntent(Landroid/content/Intent;)V
    .locals 8
    .param p1, "intent"    # Landroid/content/Intent;

    .prologue
    const/4 v2, -0x1

    .line 255
    const-string v1, "startingState"

    invoke-virtual {p1, v1, v2}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v0

    .line 256
    .local v0, "state":I
    if-eq v0, v2, :cond_0

    .line 257
    const-string v1, "onNewIntent"

    invoke-direct {p0, v0, v1}, Lcom/google/android/setupwizard/carrier/MobileDataActivity;->setState(ILjava/lang/String;)V

    .line 258
    iget-object v1, p0, Lcom/google/android/setupwizard/carrier/MobileDataActivity;->mHandler:Landroid/os/Handler;

    iget-object v2, p0, Lcom/google/android/setupwizard/carrier/MobileDataActivity;->mCheckActivation:Ljava/lang/Runnable;

    iget-wide v4, p0, Lcom/google/android/setupwizard/carrier/MobileDataActivity;->mEndTime:J

    iget-wide v6, p0, Lcom/google/android/setupwizard/carrier/MobileDataActivity;->mStartTime:J

    sub-long/2addr v4, v6

    invoke-virtual {v1, v2, v4, v5}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 264
    :goto_0
    return-void

    .line 262
    :cond_0
    const/4 v1, 0x6

    const-string v2, "onNewIntent missing EXTRA_STARTING_STATE"

    invoke-direct {p0, v1, v2}, Lcom/google/android/setupwizard/carrier/MobileDataActivity;->setState(ILjava/lang/String;)V

    goto :goto_0
.end method

.method protected onPause()V
    .locals 1

    .prologue
    .line 275
    invoke-super {p0}, Lcom/google/android/setupwizard/BaseActivity;->onPause()V

    .line 276
    const/4 v0, 0x0

    sput-object v0, Lcom/google/android/setupwizard/carrier/MobileDataActivity;->sCurrentActivity:Lcom/google/android/setupwizard/carrier/MobileDataActivity;

    .line 277
    iget-object v0, p0, Lcom/google/android/setupwizard/carrier/MobileDataActivity;->mPhoneStateListener:Landroid/telephony/PhoneStateListener;

    invoke-static {v0}, Lcom/google/android/setupwizard/carrier/PhoneMonitor;->removeListener(Landroid/telephony/PhoneStateListener;)V

    .line 278
    iget-object v0, p0, Lcom/google/android/setupwizard/carrier/MobileDataActivity;->mConnectionCheckedReceiver:Landroid/content/BroadcastReceiver;

    invoke-static {p0, v0}, Lcom/google/android/setupwizard/network/CheckConnectionTask;->unregister(Landroid/content/Context;Landroid/content/BroadcastReceiver;)V

    .line 279
    return-void
.end method

.method protected onResume()V
    .locals 3

    .prologue
    .line 297
    invoke-super {p0}, Lcom/google/android/setupwizard/BaseActivity;->onResume()V

    .line 298
    iget-object v0, p0, Lcom/google/android/setupwizard/carrier/MobileDataActivity;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "onResume "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/google/android/setupwizard/carrier/MobileDataActivity;->mState:I

    invoke-static {v2}, Lcom/google/android/setupwizard/carrier/MobileDataActivity;->getStateName(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-static {}, Lcom/google/android/setupwizard/carrier/PhoneMonitor;->getPhoneState()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 299
    sput-object p0, Lcom/google/android/setupwizard/carrier/MobileDataActivity;->sCurrentActivity:Lcom/google/android/setupwizard/carrier/MobileDataActivity;

    .line 301
    invoke-static {}, Lcom/google/android/setupwizard/carrier/PhoneMonitor;->simMissing()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 302
    const/4 v0, 0x2

    invoke-virtual {p0, v0}, Lcom/google/android/setupwizard/carrier/MobileDataActivity;->finishAction(I)V

    .line 335
    :goto_0
    return-void

    .line 303
    :cond_0
    invoke-static {}, Lcom/google/android/setupwizard/carrier/PhoneMonitor;->isHfaActivationRequired()Z

    move-result v0

    if-nez v0, :cond_1

    invoke-static {}, Lcom/google/android/setupwizard/carrier/PhoneMonitor;->otaspIsNotNeeded()Z

    move-result v0

    if-nez v0, :cond_2

    invoke-static {}, Lcom/google/android/setupwizard/carrier/PhoneMonitor;->isHfaActivated()Z

    move-result v0

    if-nez v0, :cond_2

    .line 306
    :cond_1
    iget v0, p0, Lcom/google/android/setupwizard/carrier/MobileDataActivity;->mState:I

    packed-switch v0, :pswitch_data_0

    .line 327
    :pswitch_0
    iget-object v0, p0, Lcom/google/android/setupwizard/carrier/MobileDataActivity;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "unknown state: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/google/android/setupwizard/carrier/MobileDataActivity;->mState:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 329
    invoke-direct {p0}, Lcom/google/android/setupwizard/carrier/MobileDataActivity;->onFailure()V

    goto :goto_0

    .line 308
    :pswitch_1
    invoke-direct {p0}, Lcom/google/android/setupwizard/carrier/MobileDataActivity;->startActivationProcess()V

    .line 309
    iget-object v0, p0, Lcom/google/android/setupwizard/carrier/MobileDataActivity;->mPhoneStateListener:Landroid/telephony/PhoneStateListener;

    invoke-static {v0}, Lcom/google/android/setupwizard/carrier/PhoneMonitor;->addListener(Landroid/telephony/PhoneStateListener;)V

    goto :goto_0

    .line 312
    :pswitch_2
    iget-object v0, p0, Lcom/google/android/setupwizard/carrier/MobileDataActivity;->mPhoneStateListener:Landroid/telephony/PhoneStateListener;

    invoke-static {v0}, Lcom/google/android/setupwizard/carrier/PhoneMonitor;->addListener(Landroid/telephony/PhoneStateListener;)V

    goto :goto_0

    .line 315
    :pswitch_3
    invoke-direct {p0}, Lcom/google/android/setupwizard/carrier/MobileDataActivity;->waitForDataService()V

    goto :goto_0

    .line 318
    :pswitch_4
    invoke-direct {p0}, Lcom/google/android/setupwizard/carrier/MobileDataActivity;->onSuccess()V

    goto :goto_0

    .line 321
    :pswitch_5
    invoke-direct {p0}, Lcom/google/android/setupwizard/carrier/MobileDataActivity;->onSkip()V

    goto :goto_0

    .line 324
    :pswitch_6
    invoke-direct {p0}, Lcom/google/android/setupwizard/carrier/MobileDataActivity;->onFailure()V

    goto :goto_0

    .line 333
    :cond_2
    invoke-direct {p0}, Lcom/google/android/setupwizard/carrier/MobileDataActivity;->waitForDataService()V

    goto :goto_0

    .line 306
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_0
        :pswitch_2
        :pswitch_3
        :pswitch_0
        :pswitch_4
        :pswitch_6
        :pswitch_5
    .end packed-switch
.end method

.method protected onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 4
    .param p1, "outState"    # Landroid/os/Bundle;

    .prologue
    .line 339
    invoke-super {p0, p1}, Lcom/google/android/setupwizard/BaseActivity;->onSaveInstanceState(Landroid/os/Bundle;)V

    .line 340
    const-string v0, "prevState"

    iget v1, p0, Lcom/google/android/setupwizard/carrier/MobileDataActivity;->mState:I

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 341
    const-string v0, "starttime"

    iget-wide v2, p0, Lcom/google/android/setupwizard/carrier/MobileDataActivity;->mStartTime:J

    invoke-virtual {p1, v0, v2, v3}, Landroid/os/Bundle;->putLong(Ljava/lang/String;J)V

    .line 342
    const-string v0, "endtime"

    iget-wide v2, p0, Lcom/google/android/setupwizard/carrier/MobileDataActivity;->mEndTime:J

    invoke-virtual {p1, v0, v2, v3}, Landroid/os/Bundle;->putLong(Ljava/lang/String;J)V

    .line 343
    return-void
.end method

.method protected start()V
    .locals 0

    .prologue
    .line 361
    invoke-direct {p0}, Lcom/google/android/setupwizard/carrier/MobileDataActivity;->onSkip()V

    .line 362
    return-void
.end method
