.class public Lcom/google/android/setupwizard/util/PackageMonitor;
.super Ljava/lang/Object;
.source "PackageMonitor.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/setupwizard/util/PackageMonitor$OnPackageStatusChangedListener;
    }
.end annotation


# instance fields
.field private mListener:Lcom/google/android/setupwizard/util/PackageMonitor$OnPackageStatusChangedListener;

.field private final mPackageInstaller:Landroid/content/pm/PackageInstaller;

.field private final mPackageSessionCallback:Landroid/content/pm/PackageInstaller$SessionCallback;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 89
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 37
    new-instance v0, Lcom/google/android/setupwizard/util/PackageMonitor$1;

    invoke-direct {v0, p0}, Lcom/google/android/setupwizard/util/PackageMonitor$1;-><init>(Lcom/google/android/setupwizard/util/PackageMonitor;)V

    iput-object v0, p0, Lcom/google/android/setupwizard/util/PackageMonitor;->mPackageSessionCallback:Landroid/content/pm/PackageInstaller$SessionCallback;

    .line 90
    invoke-virtual {p1}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/pm/PackageManager;->getPackageInstaller()Landroid/content/pm/PackageInstaller;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/setupwizard/util/PackageMonitor;->mPackageInstaller:Landroid/content/pm/PackageInstaller;

    .line 91
    return-void
.end method

.method static synthetic access$000(Lcom/google/android/setupwizard/util/PackageMonitor;)Landroid/content/pm/PackageInstaller;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/setupwizard/util/PackageMonitor;

    .prologue
    .line 30
    iget-object v0, p0, Lcom/google/android/setupwizard/util/PackageMonitor;->mPackageInstaller:Landroid/content/pm/PackageInstaller;

    return-object v0
.end method

.method static synthetic access$100(Lcom/google/android/setupwizard/util/PackageMonitor;)Lcom/google/android/setupwizard/util/PackageMonitor$OnPackageStatusChangedListener;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/setupwizard/util/PackageMonitor;

    .prologue
    .line 30
    iget-object v0, p0, Lcom/google/android/setupwizard/util/PackageMonitor;->mListener:Lcom/google/android/setupwizard/util/PackageMonitor$OnPackageStatusChangedListener;

    return-object v0
.end method


# virtual methods
.method public setOnPackageStatusChangedListener(Lcom/google/android/setupwizard/util/PackageMonitor$OnPackageStatusChangedListener;)V
    .locals 2
    .param p1, "listener"    # Lcom/google/android/setupwizard/util/PackageMonitor$OnPackageStatusChangedListener;

    .prologue
    .line 97
    iput-object p1, p0, Lcom/google/android/setupwizard/util/PackageMonitor;->mListener:Lcom/google/android/setupwizard/util/PackageMonitor$OnPackageStatusChangedListener;

    .line 98
    iget-object v0, p0, Lcom/google/android/setupwizard/util/PackageMonitor;->mListener:Lcom/google/android/setupwizard/util/PackageMonitor$OnPackageStatusChangedListener;

    if-nez v0, :cond_0

    .line 99
    iget-object v0, p0, Lcom/google/android/setupwizard/util/PackageMonitor;->mPackageInstaller:Landroid/content/pm/PackageInstaller;

    iget-object v1, p0, Lcom/google/android/setupwizard/util/PackageMonitor;->mPackageSessionCallback:Landroid/content/pm/PackageInstaller$SessionCallback;

    invoke-virtual {v0, v1}, Landroid/content/pm/PackageInstaller;->removeSessionCallback(Landroid/content/pm/PackageInstaller$SessionCallback;)V

    .line 103
    :goto_0
    return-void

    .line 101
    :cond_0
    iget-object v0, p0, Lcom/google/android/setupwizard/util/PackageMonitor;->mPackageInstaller:Landroid/content/pm/PackageInstaller;

    iget-object v1, p0, Lcom/google/android/setupwizard/util/PackageMonitor;->mPackageSessionCallback:Landroid/content/pm/PackageInstaller$SessionCallback;

    invoke-virtual {v0, v1}, Landroid/content/pm/PackageInstaller;->addSessionCallback(Landroid/content/pm/PackageInstaller$SessionCallback;)V

    goto :goto_0
.end method
