.class public Lcom/google/android/setupwizard/network/AccountCheckinActivity;
.super Lcom/google/android/setupwizard/BaseActivity;
.source "AccountCheckinActivity.java"

# interfaces
.implements Lcom/google/android/setupwizard/util/WaitForGservicesTask$WaitForGservicesListener;


# static fields
.field private static final ACCOUNT_CHECKIN_TIMEOUT_MS:Lcom/google/android/setupwizard/util/GservicesValue;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/setupwizard/util/GservicesValue",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private final mHandler:Landroid/os/Handler;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 43
    const-string v0, "google_setup:account_checkin_timeout"

    const/16 v1, 0x3a98

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/setupwizard/util/GservicesValue;->value(Ljava/lang/String;Ljava/lang/Integer;)Lcom/google/android/setupwizard/util/GservicesValue;

    move-result-object v0

    sput-object v0, Lcom/google/android/setupwizard/network/AccountCheckinActivity;->ACCOUNT_CHECKIN_TIMEOUT_MS:Lcom/google/android/setupwizard/util/GservicesValue;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 37
    invoke-direct {p0}, Lcom/google/android/setupwizard/BaseActivity;-><init>()V

    .line 46
    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    iput-object v0, p0, Lcom/google/android/setupwizard/network/AccountCheckinActivity;->mHandler:Landroid/os/Handler;

    return-void
.end method

.method static synthetic access$000(Lcom/google/android/setupwizard/network/AccountCheckinActivity;I)V
    .locals 0
    .param p0, "x0"    # Lcom/google/android/setupwizard/network/AccountCheckinActivity;
    .param p1, "x1"    # I

    .prologue
    .line 37
    invoke-virtual {p0, p1}, Lcom/google/android/setupwizard/network/AccountCheckinActivity;->finishAction(I)V

    return-void
.end method

.method private delayedFinish(I)V
    .locals 4
    .param p1, "result"    # I

    .prologue
    .line 94
    iget-object v0, p0, Lcom/google/android/setupwizard/network/AccountCheckinActivity;->mHandler:Landroid/os/Handler;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacksAndMessages(Ljava/lang/Object;)V

    .line 95
    iget-object v0, p0, Lcom/google/android/setupwizard/network/AccountCheckinActivity;->mHandler:Landroid/os/Handler;

    new-instance v1, Lcom/google/android/setupwizard/network/AccountCheckinActivity$1;

    invoke-direct {v1, p0, p1}, Lcom/google/android/setupwizard/network/AccountCheckinActivity$1;-><init>(Lcom/google/android/setupwizard/network/AccountCheckinActivity;I)V

    const-wide/16 v2, 0x3e8

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 101
    return-void
.end method


# virtual methods
.method protected getPreviousTransition()I
    .locals 1

    .prologue
    .line 90
    const/4 v0, 0x2

    return v0
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 5
    .param p1, "icicle"    # Landroid/os/Bundle;

    .prologue
    const/4 v4, 0x1

    .line 62
    invoke-super {p0, p1}, Lcom/google/android/setupwizard/BaseActivity;->onCreate(Landroid/os/Bundle;)V

    .line 64
    const v1, 0x7f030017

    const v2, 0x7f030023

    invoke-virtual {p0, v1, v2}, Lcom/google/android/setupwizard/network/AccountCheckinActivity;->setTemplateContent(II)V

    .line 65
    const v1, 0x7f070051

    invoke-virtual {p0, v1}, Lcom/google/android/setupwizard/network/AccountCheckinActivity;->setHeaderText(I)V

    .line 66
    sget-object v1, Lcom/google/android/setupwizard/network/AccountCheckinActivity;->ACCOUNT_CHECKIN_TIMEOUT_MS:Lcom/google/android/setupwizard/util/GservicesValue;

    invoke-virtual {v1}, Lcom/google/android/setupwizard/util/GservicesValue;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v0

    .line 67
    .local v0, "timeout":I
    if-eqz v0, :cond_0

    .line 68
    int-to-long v2, v0

    const-string v1, "gservices_task"

    invoke-static {p0, v2, v3, v1, v4}, Lcom/google/android/setupwizard/util/WaitForGservicesTask;->getInstanceWithTag(Landroid/app/Activity;JLjava/lang/String;Z)Lcom/google/android/setupwizard/util/WaitForGservicesTask;

    .line 69
    invoke-static {p0, v4}, Lcom/google/android/setupwizard/util/GservicesChangedReceiver;->tickleCheckinService(Landroid/content/Context;Z)V

    .line 73
    :goto_0
    return-void

    .line 71
    :cond_0
    const/4 v1, -0x1

    invoke-virtual {p0, v1}, Lcom/google/android/setupwizard/network/AccountCheckinActivity;->finishAction(I)V

    goto :goto_0
.end method

.method public onGservicesReady()V
    .locals 1

    .prologue
    .line 50
    const/4 v0, -0x1

    invoke-direct {p0, v0}, Lcom/google/android/setupwizard/network/AccountCheckinActivity;->delayedFinish(I)V

    .line 51
    return-void
.end method

.method public onGservicesTimeout()V
    .locals 2

    .prologue
    .line 55
    iget-object v0, p0, Lcom/google/android/setupwizard/network/AccountCheckinActivity;->TAG:Ljava/lang/String;

    const-string v1, "Account checkin timed out"

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 57
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/google/android/setupwizard/network/AccountCheckinActivity;->delayedFinish(I)V

    .line 58
    return-void
.end method

.method public onNavigationBarCreated(Lcom/android/setupwizard/navigationbar/SetupWizardNavBar;)V
    .locals 2
    .param p1, "bar"    # Lcom/android/setupwizard/navigationbar/SetupWizardNavBar;

    .prologue
    .line 83
    invoke-super {p0, p1}, Lcom/google/android/setupwizard/BaseActivity;->onNavigationBarCreated(Lcom/android/setupwizard/navigationbar/SetupWizardNavBar;)V

    .line 84
    invoke-virtual {p1}, Lcom/android/setupwizard/navigationbar/SetupWizardNavBar;->getNextButton()Landroid/widget/Button;

    move-result-object v0

    const v1, 0x7f070046

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setText(I)V

    .line 85
    invoke-virtual {p1}, Lcom/android/setupwizard/navigationbar/SetupWizardNavBar;->getNextButton()Landroid/widget/Button;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setEnabled(Z)V

    .line 86
    return-void
.end method

.method protected onPause()V
    .locals 2

    .prologue
    .line 77
    iget-object v0, p0, Lcom/google/android/setupwizard/network/AccountCheckinActivity;->mHandler:Landroid/os/Handler;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacksAndMessages(Ljava/lang/Object;)V

    .line 78
    invoke-super {p0}, Lcom/google/android/setupwizard/BaseActivity;->onPause()V

    .line 79
    return-void
.end method
