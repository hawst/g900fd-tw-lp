.class Lcom/google/android/setupwizard/carrier/ActivationActivity$6;
.super Landroid/os/CountDownTimer;
.source "ActivationActivity.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/setupwizard/carrier/ActivationActivity;->onSpcRetriesFailure()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/setupwizard/carrier/ActivationActivity;


# direct methods
.method constructor <init>(Lcom/google/android/setupwizard/carrier/ActivationActivity;JJ)V
    .locals 0
    .param p2, "x0"    # J
    .param p4, "x1"    # J

    .prologue
    .line 943
    iput-object p1, p0, Lcom/google/android/setupwizard/carrier/ActivationActivity$6;->this$0:Lcom/google/android/setupwizard/carrier/ActivationActivity;

    invoke-direct {p0, p2, p3, p4, p5}, Landroid/os/CountDownTimer;-><init>(JJ)V

    return-void
.end method


# virtual methods
.method public onFinish()V
    .locals 1

    .prologue
    .line 952
    iget-object v0, p0, Lcom/google/android/setupwizard/carrier/ActivationActivity$6;->this$0:Lcom/google/android/setupwizard/carrier/ActivationActivity;

    # invokes: Lcom/google/android/setupwizard/carrier/ActivationActivity;->doShutdown()V
    invoke-static {v0}, Lcom/google/android/setupwizard/carrier/ActivationActivity;->access$1700(Lcom/google/android/setupwizard/carrier/ActivationActivity;)V

    .line 953
    return-void
.end method

.method public onTick(J)V
    .locals 7
    .param p1, "millisUntilFinished"    # J

    .prologue
    .line 946
    const-wide/16 v2, 0x3e8

    div-long v2, p1, v2

    long-to-int v0, v2

    .line 947
    .local v0, "secondsLeft":I
    iget-object v1, p0, Lcom/google/android/setupwizard/carrier/ActivationActivity$6;->this$0:Lcom/google/android/setupwizard/carrier/ActivationActivity;

    # getter for: Lcom/google/android/setupwizard/carrier/ActivationActivity;->mCountdownText:Landroid/widget/TextView;
    invoke-static {v1}, Lcom/google/android/setupwizard/carrier/ActivationActivity;->access$1600(Lcom/google/android/setupwizard/carrier/ActivationActivity;)Landroid/widget/TextView;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/setupwizard/carrier/ActivationActivity$6;->this$0:Lcom/google/android/setupwizard/carrier/ActivationActivity;

    invoke-virtual {v2}, Lcom/google/android/setupwizard/carrier/ActivationActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const/high16 v3, 0x7f0d0000

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    aput-object v6, v4, v5

    invoke-virtual {v2, v3, v0, v4}, Landroid/content/res/Resources;->getQuantityString(II[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 949
    return-void
.end method
