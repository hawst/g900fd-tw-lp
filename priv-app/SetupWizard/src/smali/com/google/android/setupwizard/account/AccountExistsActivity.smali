.class public Lcom/google/android/setupwizard/account/AccountExistsActivity;
.super Lcom/google/android/setupwizard/BaseActivity;
.source "AccountExistsActivity.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 14
    invoke-direct {p0}, Lcom/google/android/setupwizard/BaseActivity;-><init>()V

    return-void
.end method


# virtual methods
.method protected onCreate(Landroid/os/Bundle;)V
    .locals 2
    .param p1, "icicle"    # Landroid/os/Bundle;

    .prologue
    .line 18
    invoke-super {p0, p1}, Lcom/google/android/setupwizard/BaseActivity;->onCreate(Landroid/os/Bundle;)V

    .line 19
    const/high16 v0, 0x7f030000

    const v1, 0x7f030023

    invoke-virtual {p0, v0, v1}, Lcom/google/android/setupwizard/account/AccountExistsActivity;->setTemplateContent(II)V

    .line 20
    const v0, 0x7f070077

    invoke-virtual {p0, v0}, Lcom/google/android/setupwizard/account/AccountExistsActivity;->setHeaderText(I)V

    .line 21
    return-void
.end method

.method protected start()V
    .locals 0

    .prologue
    .line 25
    invoke-virtual {p0}, Lcom/google/android/setupwizard/account/AccountExistsActivity;->nextScreen()V

    .line 26
    return-void
.end method
