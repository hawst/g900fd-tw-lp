.class Lcom/google/android/setupwizard/user/WelcomeActivity$5;
.super Landroid/content/BroadcastReceiver;
.source "WelcomeActivity.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/setupwizard/user/WelcomeActivity;->onCreate(Landroid/os/Bundle;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/setupwizard/user/WelcomeActivity;


# direct methods
.method constructor <init>(Lcom/google/android/setupwizard/user/WelcomeActivity;)V
    .locals 0

    .prologue
    .line 200
    iput-object p1, p0, Lcom/google/android/setupwizard/user/WelcomeActivity$5;->this$0:Lcom/google/android/setupwizard/user/WelcomeActivity;

    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    return-void
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 4
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "intent"    # Landroid/content/Intent;

    .prologue
    .line 203
    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v0

    .line 204
    .local v0, "locale":Ljava/util/Locale;
    iget-object v1, p0, Lcom/google/android/setupwizard/user/WelcomeActivity$5;->this$0:Lcom/google/android/setupwizard/user/WelcomeActivity;

    # getter for: Lcom/google/android/setupwizard/user/WelcomeActivity;->TAG:Ljava/lang/String;
    invoke-static {v1}, Lcom/google/android/setupwizard/user/WelcomeActivity;->access$600(Lcom/google/android/setupwizard/user/WelcomeActivity;)Ljava/lang/String;

    move-result-object v1

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "WelcomeActivity.mLocaleReceiver.onReceive() locale="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 205
    iget-object v1, p0, Lcom/google/android/setupwizard/user/WelcomeActivity$5;->this$0:Lcom/google/android/setupwizard/user/WelcomeActivity;

    # invokes: Lcom/google/android/setupwizard/user/WelcomeActivity;->updateForLocale(Ljava/util/Locale;)V
    invoke-static {v1, v0}, Lcom/google/android/setupwizard/user/WelcomeActivity;->access$700(Lcom/google/android/setupwizard/user/WelcomeActivity;Ljava/util/Locale;)V

    .line 206
    return-void
.end method
