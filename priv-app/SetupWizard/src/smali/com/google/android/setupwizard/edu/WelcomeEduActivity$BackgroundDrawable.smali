.class Lcom/google/android/setupwizard/edu/WelcomeEduActivity$BackgroundDrawable;
.super Landroid/graphics/drawable/Drawable;
.source "WelcomeEduActivity.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/setupwizard/edu/WelcomeEduActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = "BackgroundDrawable"
.end annotation


# instance fields
.field mCroppedBitmap:Landroid/graphics/Bitmap;

.field mIntrinsicHeight:I

.field mIntrinsicWidth:I

.field mMatrix:Landroid/graphics/Matrix;

.field mSourceBitmap:Landroid/graphics/Bitmap;


# direct methods
.method public constructor <init>(Landroid/graphics/drawable/Drawable;)V
    .locals 1
    .param p1, "d"    # Landroid/graphics/drawable/Drawable;

    .prologue
    .line 122
    invoke-direct {p0}, Landroid/graphics/drawable/Drawable;-><init>()V

    .line 123
    instance-of v0, p1, Landroid/graphics/drawable/BitmapDrawable;

    if-eqz v0, :cond_0

    .line 124
    check-cast p1, Landroid/graphics/drawable/BitmapDrawable;

    .end local p1    # "d":Landroid/graphics/drawable/Drawable;
    invoke-virtual {p1}, Landroid/graphics/drawable/BitmapDrawable;->getBitmap()Landroid/graphics/Bitmap;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/android/setupwizard/edu/WelcomeEduActivity$BackgroundDrawable;->setBitmap(Landroid/graphics/Bitmap;)V

    .line 127
    :cond_0
    return-void
.end method


# virtual methods
.method public draw(Landroid/graphics/Canvas;)V
    .locals 13
    .param p1, "canvas"    # Landroid/graphics/Canvas;

    .prologue
    const/4 v8, 0x0

    const/high16 v12, 0x3f400000    # 0.75f

    const/high16 v11, 0x3f000000    # 0.5f

    .line 131
    iget-object v9, p0, Lcom/google/android/setupwizard/edu/WelcomeEduActivity$BackgroundDrawable;->mSourceBitmap:Landroid/graphics/Bitmap;

    if-nez v9, :cond_0

    .line 185
    :goto_0
    return-void

    .line 133
    :cond_0
    invoke-virtual {p1}, Landroid/graphics/Canvas;->getWidth()I

    move-result v6

    .line 134
    .local v6, "vwidth":I
    invoke-virtual {p1}, Landroid/graphics/Canvas;->getHeight()I

    move-result v5

    .line 136
    .local v5, "vheight":I
    iget-object v9, p0, Lcom/google/android/setupwizard/edu/WelcomeEduActivity$BackgroundDrawable;->mCroppedBitmap:Landroid/graphics/Bitmap;

    if-nez v9, :cond_1

    .line 137
    new-instance v9, Landroid/graphics/Matrix;

    invoke-direct {v9}, Landroid/graphics/Matrix;-><init>()V

    iput-object v9, p0, Lcom/google/android/setupwizard/edu/WelcomeEduActivity$BackgroundDrawable;->mMatrix:Landroid/graphics/Matrix;

    .line 139
    iget v9, p0, Lcom/google/android/setupwizard/edu/WelcomeEduActivity$BackgroundDrawable;->mIntrinsicHeight:I

    if-gt v5, v9, :cond_5

    iget v9, p0, Lcom/google/android/setupwizard/edu/WelcomeEduActivity$BackgroundDrawable;->mIntrinsicWidth:I

    if-gt v6, v9, :cond_5

    .line 140
    int-to-float v9, v5

    iget v10, p0, Lcom/google/android/setupwizard/edu/WelcomeEduActivity$BackgroundDrawable;->mIntrinsicHeight:I

    int-to-float v10, v10

    div-float/2addr v9, v10

    cmpg-float v9, v9, v12

    if-gez v9, :cond_4

    .line 143
    iget v9, p0, Lcom/google/android/setupwizard/edu/WelcomeEduActivity$BackgroundDrawable;->mIntrinsicHeight:I

    int-to-float v9, v9

    mul-float/2addr v9, v12

    float-to-int v2, v9

    .line 144
    .local v2, "newHeight":I
    iget v9, p0, Lcom/google/android/setupwizard/edu/WelcomeEduActivity$BackgroundDrawable;->mIntrinsicWidth:I

    int-to-float v9, v9

    mul-float/2addr v9, v12

    float-to-int v3, v9

    .line 145
    .local v3, "newWidth":I
    if-lt v3, v6, :cond_2

    sub-int v9, v3, v6

    int-to-float v9, v9

    mul-float/2addr v9, v11

    add-float/2addr v9, v11

    float-to-int v7, v9

    .line 147
    .local v7, "x":I
    :goto_1
    iget-object v9, p0, Lcom/google/android/setupwizard/edu/WelcomeEduActivity$BackgroundDrawable;->mSourceBitmap:Landroid/graphics/Bitmap;

    invoke-static {v9, v7, v8, v3, v2}, Landroid/graphics/Bitmap;->createBitmap(Landroid/graphics/Bitmap;IIII)Landroid/graphics/Bitmap;

    move-result-object v8

    iput-object v8, p0, Lcom/google/android/setupwizard/edu/WelcomeEduActivity$BackgroundDrawable;->mCroppedBitmap:Landroid/graphics/Bitmap;

    .line 152
    mul-int v8, v3, v5

    mul-int v9, v6, v2

    if-le v8, v9, :cond_3

    .line 153
    int-to-float v8, v5

    int-to-float v9, v2

    div-float v4, v8, v9

    .line 158
    .local v4, "scale":F
    :goto_2
    iget-object v8, p0, Lcom/google/android/setupwizard/edu/WelcomeEduActivity$BackgroundDrawable;->mMatrix:Landroid/graphics/Matrix;

    invoke-virtual {v8, v4, v4}, Landroid/graphics/Matrix;->setScale(FF)V

    .line 184
    .end local v2    # "newHeight":I
    .end local v3    # "newWidth":I
    .end local v4    # "scale":F
    .end local v7    # "x":I
    :cond_1
    :goto_3
    iget-object v8, p0, Lcom/google/android/setupwizard/edu/WelcomeEduActivity$BackgroundDrawable;->mCroppedBitmap:Landroid/graphics/Bitmap;

    iget-object v9, p0, Lcom/google/android/setupwizard/edu/WelcomeEduActivity$BackgroundDrawable;->mMatrix:Landroid/graphics/Matrix;

    const/4 v10, 0x0

    invoke-virtual {p1, v8, v9, v10}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;Landroid/graphics/Matrix;Landroid/graphics/Paint;)V

    goto :goto_0

    .restart local v2    # "newHeight":I
    .restart local v3    # "newWidth":I
    :cond_2
    move v7, v8

    .line 145
    goto :goto_1

    .line 155
    .restart local v7    # "x":I
    :cond_3
    int-to-float v8, v6

    int-to-float v9, v3

    div-float v4, v8, v9

    .restart local v4    # "scale":F
    goto :goto_2

    .line 160
    .end local v2    # "newHeight":I
    .end local v3    # "newWidth":I
    .end local v4    # "scale":F
    .end local v7    # "x":I
    :cond_4
    iget v9, p0, Lcom/google/android/setupwizard/edu/WelcomeEduActivity$BackgroundDrawable;->mIntrinsicWidth:I

    sub-int/2addr v9, v6

    int-to-float v9, v9

    mul-float/2addr v9, v11

    add-float/2addr v9, v11

    float-to-int v7, v9

    .line 161
    .restart local v7    # "x":I
    iget-object v9, p0, Lcom/google/android/setupwizard/edu/WelcomeEduActivity$BackgroundDrawable;->mSourceBitmap:Landroid/graphics/Bitmap;

    iget v10, p0, Lcom/google/android/setupwizard/edu/WelcomeEduActivity$BackgroundDrawable;->mIntrinsicHeight:I

    invoke-static {v9, v7, v8, v6, v10}, Landroid/graphics/Bitmap;->createBitmap(Landroid/graphics/Bitmap;IIII)Landroid/graphics/Bitmap;

    move-result-object v8

    iput-object v8, p0, Lcom/google/android/setupwizard/edu/WelcomeEduActivity$BackgroundDrawable;->mCroppedBitmap:Landroid/graphics/Bitmap;

    goto :goto_3

    .line 167
    .end local v7    # "x":I
    :cond_5
    iget-object v8, p0, Lcom/google/android/setupwizard/edu/WelcomeEduActivity$BackgroundDrawable;->mSourceBitmap:Landroid/graphics/Bitmap;

    iput-object v8, p0, Lcom/google/android/setupwizard/edu/WelcomeEduActivity$BackgroundDrawable;->mCroppedBitmap:Landroid/graphics/Bitmap;

    .line 169
    const/4 v0, 0x0

    .local v0, "dx":F
    const/4 v1, 0x0

    .line 171
    .local v1, "dy":F
    iget v8, p0, Lcom/google/android/setupwizard/edu/WelcomeEduActivity$BackgroundDrawable;->mIntrinsicWidth:I

    mul-int/2addr v8, v5

    iget v9, p0, Lcom/google/android/setupwizard/edu/WelcomeEduActivity$BackgroundDrawable;->mIntrinsicHeight:I

    mul-int/2addr v9, v6

    if-le v8, v9, :cond_6

    .line 172
    int-to-float v8, v5

    iget v9, p0, Lcom/google/android/setupwizard/edu/WelcomeEduActivity$BackgroundDrawable;->mIntrinsicHeight:I

    int-to-float v9, v9

    div-float v4, v8, v9

    .line 173
    .restart local v4    # "scale":F
    int-to-float v8, v6

    iget v9, p0, Lcom/google/android/setupwizard/edu/WelcomeEduActivity$BackgroundDrawable;->mIntrinsicWidth:I

    int-to-float v9, v9

    mul-float/2addr v9, v4

    sub-float/2addr v8, v9

    mul-float v0, v8, v11

    .line 179
    :goto_4
    iget-object v8, p0, Lcom/google/android/setupwizard/edu/WelcomeEduActivity$BackgroundDrawable;->mMatrix:Landroid/graphics/Matrix;

    invoke-virtual {v8, v4, v4}, Landroid/graphics/Matrix;->setScale(FF)V

    .line 180
    iget-object v8, p0, Lcom/google/android/setupwizard/edu/WelcomeEduActivity$BackgroundDrawable;->mMatrix:Landroid/graphics/Matrix;

    add-float v9, v0, v11

    float-to-int v9, v9

    int-to-float v9, v9

    add-float v10, v1, v11

    float-to-int v10, v10

    int-to-float v10, v10

    invoke-virtual {v8, v9, v10}, Landroid/graphics/Matrix;->postTranslate(FF)Z

    goto :goto_3

    .line 175
    .end local v4    # "scale":F
    :cond_6
    int-to-float v8, v6

    iget v9, p0, Lcom/google/android/setupwizard/edu/WelcomeEduActivity$BackgroundDrawable;->mIntrinsicWidth:I

    int-to-float v9, v9

    div-float v4, v8, v9

    .line 176
    .restart local v4    # "scale":F
    int-to-float v8, v5

    iget v9, p0, Lcom/google/android/setupwizard/edu/WelcomeEduActivity$BackgroundDrawable;->mIntrinsicHeight:I

    int-to-float v9, v9

    mul-float/2addr v9, v4

    sub-float/2addr v8, v9

    mul-float v1, v8, v11

    goto :goto_4
.end method

.method public getOpacity()I
    .locals 1

    .prologue
    .line 199
    const/4 v0, 0x0

    return v0
.end method

.method public setAlpha(I)V
    .locals 0
    .param p1, "alpha"    # I

    .prologue
    .line 190
    return-void
.end method

.method setBitmap(Landroid/graphics/Bitmap;)V
    .locals 1
    .param p1, "bitmap"    # Landroid/graphics/Bitmap;

    .prologue
    .line 114
    iput-object p1, p0, Lcom/google/android/setupwizard/edu/WelcomeEduActivity$BackgroundDrawable;->mSourceBitmap:Landroid/graphics/Bitmap;

    .line 115
    iget-object v0, p0, Lcom/google/android/setupwizard/edu/WelcomeEduActivity$BackgroundDrawable;->mSourceBitmap:Landroid/graphics/Bitmap;

    if-nez v0, :cond_0

    .line 119
    :goto_0
    return-void

    .line 116
    :cond_0
    iget-object v0, p0, Lcom/google/android/setupwizard/edu/WelcomeEduActivity$BackgroundDrawable;->mSourceBitmap:Landroid/graphics/Bitmap;

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v0

    iput v0, p0, Lcom/google/android/setupwizard/edu/WelcomeEduActivity$BackgroundDrawable;->mIntrinsicWidth:I

    .line 117
    iget-object v0, p0, Lcom/google/android/setupwizard/edu/WelcomeEduActivity$BackgroundDrawable;->mSourceBitmap:Landroid/graphics/Bitmap;

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v0

    iput v0, p0, Lcom/google/android/setupwizard/edu/WelcomeEduActivity$BackgroundDrawable;->mIntrinsicHeight:I

    .line 118
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/setupwizard/edu/WelcomeEduActivity$BackgroundDrawable;->mMatrix:Landroid/graphics/Matrix;

    goto :goto_0
.end method

.method public setColorFilter(Landroid/graphics/ColorFilter;)V
    .locals 0
    .param p1, "cf"    # Landroid/graphics/ColorFilter;

    .prologue
    .line 195
    return-void
.end method
