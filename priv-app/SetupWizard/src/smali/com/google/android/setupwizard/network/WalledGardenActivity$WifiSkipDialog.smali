.class public Lcom/google/android/setupwizard/network/WalledGardenActivity$WifiSkipDialog;
.super Landroid/app/DialogFragment;
.source "WalledGardenActivity.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/setupwizard/network/WalledGardenActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "WifiSkipDialog"
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 301
    invoke-direct {p0}, Landroid/app/DialogFragment;-><init>()V

    .line 303
    return-void
.end method


# virtual methods
.method public onCreateDialog(Landroid/os/Bundle;)Landroid/app/Dialog;
    .locals 3
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 307
    new-instance v0, Landroid/app/AlertDialog$Builder;

    invoke-virtual {p0}, Lcom/google/android/setupwizard/network/WalledGardenActivity$WifiSkipDialog;->getActivity()Landroid/app/Activity;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    const v1, 0x7f070089

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setMessage(I)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setCancelable(Z)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    const v1, 0x7f070047

    new-instance v2, Lcom/google/android/setupwizard/network/WalledGardenActivity$WifiSkipDialog$2;

    invoke-direct {v2, p0}, Lcom/google/android/setupwizard/network/WalledGardenActivity$WifiSkipDialog$2;-><init>(Lcom/google/android/setupwizard/network/WalledGardenActivity$WifiSkipDialog;)V

    invoke-virtual {v0, v1, v2}, Landroid/app/AlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    const v1, 0x7f070048

    new-instance v2, Lcom/google/android/setupwizard/network/WalledGardenActivity$WifiSkipDialog$1;

    invoke-direct {v2, p0}, Lcom/google/android/setupwizard/network/WalledGardenActivity$WifiSkipDialog$1;-><init>(Lcom/google/android/setupwizard/network/WalledGardenActivity$WifiSkipDialog;)V

    invoke-virtual {v0, v1, v2}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v0

    return-object v0
.end method
