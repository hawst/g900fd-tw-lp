.class public Lcom/google/android/setupwizard/WizardScript;
.super Ljava/lang/Object;
.source "WizardScript.java"

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/google/android/setupwizard/WizardScript;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private final mActions:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Lcom/google/android/setupwizard/WizardAction;",
            ">;"
        }
    .end annotation
.end field

.field private final mFirstActionId:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 212
    new-instance v0, Lcom/google/android/setupwizard/WizardScript$1;

    invoke-direct {v0}, Lcom/google/android/setupwizard/WizardScript$1;-><init>()V

    sput-object v0, Lcom/google/android/setupwizard/WizardScript;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(Ljava/util/Map;Ljava/lang/String;)V
    .locals 1
    .param p2, "firstActionId"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Lcom/google/android/setupwizard/WizardAction;",
            ">;",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    .line 57
    .local p1, "actions":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Lcom/google/android/setupwizard/WizardAction;>;"
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 58
    invoke-static {p1}, Ljava/util/Collections;->unmodifiableMap(Ljava/util/Map;)Ljava/util/Map;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/setupwizard/WizardScript;->mActions:Ljava/util/Map;

    .line 59
    iput-object p2, p0, Lcom/google/android/setupwizard/WizardScript;->mFirstActionId:Ljava/lang/String;

    .line 60
    return-void
.end method

.method public static loadFromUri(Landroid/content/Context;Ljava/lang/String;)Lcom/google/android/setupwizard/WizardScript;
    .locals 11
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "uriString"    # Ljava/lang/String;

    .prologue
    .line 63
    const/4 v5, 0x0

    .line 65
    .local v5, "script":Lcom/google/android/setupwizard/WizardScript;
    :try_start_0
    invoke-static {p1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v7

    .line 66
    .local v7, "uri":Landroid/net/Uri;
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v8

    invoke-virtual {v8, v7}, Landroid/content/ContentResolver;->getResourceId(Landroid/net/Uri;)Landroid/content/ContentResolver$OpenResourceIdResult;

    move-result-object v4

    .line 67
    .local v4, "result":Landroid/content/ContentResolver$OpenResourceIdResult;
    iget-object v8, v4, Landroid/content/ContentResolver$OpenResourceIdResult;->r:Landroid/content/res/Resources;

    iget v9, v4, Landroid/content/ContentResolver$OpenResourceIdResult;->id:I

    invoke-virtual {v8, v9}, Landroid/content/res/Resources;->getResourceTypeName(I)Ljava/lang/String;

    move-result-object v3

    .line 69
    .local v3, "resourceType":Ljava/lang/String;
    const-string v8, "xml"

    invoke-virtual {v8, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_2

    .line 71
    iget-object v8, v4, Landroid/content/ContentResolver$OpenResourceIdResult;->r:Landroid/content/res/Resources;

    iget v9, v4, Landroid/content/ContentResolver$OpenResourceIdResult;->id:I

    invoke-virtual {v8, v9}, Landroid/content/res/Resources;->getXml(I)Landroid/content/res/XmlResourceParser;

    move-result-object v2

    .line 81
    .local v2, "parser":Lorg/xmlpull/v1/XmlPullParser;
    :cond_0
    :goto_0
    invoke-interface {v2}, Lorg/xmlpull/v1/XmlPullParser;->next()I

    move-result v6

    .local v6, "type":I
    const/4 v8, 0x1

    if-eq v6, v8, :cond_1

    const/4 v8, 0x2

    if-ne v6, v8, :cond_0

    .line 85
    :cond_1
    invoke-static {v2}, Lcom/google/android/setupwizard/WizardScript;->parseWizardScript(Lorg/xmlpull/v1/XmlPullParser;)Lcom/google/android/setupwizard/WizardScript;

    move-result-object v5

    .line 96
    .end local v2    # "parser":Lorg/xmlpull/v1/XmlPullParser;
    .end local v3    # "resourceType":Ljava/lang/String;
    .end local v4    # "result":Landroid/content/ContentResolver$OpenResourceIdResult;
    .end local v6    # "type":I
    .end local v7    # "uri":Landroid/net/Uri;
    :goto_1
    return-object v5

    .line 74
    .restart local v3    # "resourceType":Ljava/lang/String;
    .restart local v4    # "result":Landroid/content/ContentResolver$OpenResourceIdResult;
    .restart local v7    # "uri":Landroid/net/Uri;
    :cond_2
    iget-object v8, v4, Landroid/content/ContentResolver$OpenResourceIdResult;->r:Landroid/content/res/Resources;

    iget v9, v4, Landroid/content/ContentResolver$OpenResourceIdResult;->id:I

    invoke-virtual {v8, v9}, Landroid/content/res/Resources;->openRawResource(I)Ljava/io/InputStream;

    move-result-object v1

    .line 75
    .local v1, "in":Ljava/io/InputStream;
    invoke-static {}, Landroid/util/Xml;->newPullParser()Lorg/xmlpull/v1/XmlPullParser;

    move-result-object v2

    .line 76
    .restart local v2    # "parser":Lorg/xmlpull/v1/XmlPullParser;
    const/4 v8, 0x0

    invoke-interface {v2, v1, v8}, Lorg/xmlpull/v1/XmlPullParser;->setInput(Ljava/io/InputStream;Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/io/FileNotFoundException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Lorg/xmlpull/v1/XmlPullParserException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_2

    goto :goto_0

    .line 86
    .end local v1    # "in":Ljava/io/InputStream;
    .end local v2    # "parser":Lorg/xmlpull/v1/XmlPullParser;
    .end local v3    # "resourceType":Ljava/lang/String;
    .end local v4    # "result":Landroid/content/ContentResolver$OpenResourceIdResult;
    .end local v7    # "uri":Landroid/net/Uri;
    :catch_0
    move-exception v0

    .line 87
    .local v0, "e":Ljava/io/FileNotFoundException;
    const-string v8, "WizardScript"

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "Cannot find file: "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v8, v9}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 88
    const-string v8, "WizardScript"

    invoke-virtual {v0}, Ljava/io/FileNotFoundException;->getMessage()Ljava/lang/String;

    move-result-object v9

    invoke-static {v8, v9}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1

    .line 89
    .end local v0    # "e":Ljava/io/FileNotFoundException;
    :catch_1
    move-exception v0

    .line 90
    .local v0, "e":Lorg/xmlpull/v1/XmlPullParserException;
    const-string v8, "WizardScript"

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "Ill-formatted wizard_script: "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v8, v9}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 91
    const-string v8, "WizardScript"

    invoke-virtual {v0}, Lorg/xmlpull/v1/XmlPullParserException;->getMessage()Ljava/lang/String;

    move-result-object v9

    invoke-static {v8, v9}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1

    .line 92
    .end local v0    # "e":Lorg/xmlpull/v1/XmlPullParserException;
    :catch_2
    move-exception v0

    .line 93
    .local v0, "e":Ljava/io/IOException;
    const-string v8, "WizardScript"

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "Unable to read wizard_script: "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v8, v9}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 94
    const-string v8, "WizardScript"

    invoke-virtual {v0}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v9

    invoke-static {v8, v9}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1
.end method

.method private static parseWizardScript(Lorg/xmlpull/v1/XmlPullParser;)Lcom/google/android/setupwizard/WizardScript;
    .locals 9
    .param p0, "parser"    # Lorg/xmlpull/v1/XmlPullParser;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/xmlpull/v1/XmlPullParserException;,
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v8, 0x3

    .line 112
    invoke-interface {p0}, Lorg/xmlpull/v1/XmlPullParser;->getName()Ljava/lang/String;

    move-result-object v3

    .line 113
    .local v3, "nodeName":Ljava/lang/String;
    const-string v6, "WizardScript"

    invoke-virtual {v6, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-nez v6, :cond_0

    .line 114
    new-instance v6, Lorg/xmlpull/v1/XmlPullParserException;

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "XML document must start with <WizardScript> tag; found "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, " at "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-interface {p0}, Lorg/xmlpull/v1/XmlPullParser;->getPositionDescription()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-direct {v6, v7}, Lorg/xmlpull/v1/XmlPullParserException;-><init>(Ljava/lang/String;)V

    throw v6

    .line 119
    :cond_0
    const-string v6, "http://schemas.android.com/apk/res/com.google.android.setupwizard"

    const-string v7, "firstAction"

    invoke-interface {p0, v6, v7}, Lorg/xmlpull/v1/XmlPullParser;->getAttributeValue(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 120
    .local v2, "firstActionId":Ljava/lang/String;
    if-nez v2, :cond_1

    .line 121
    new-instance v6, Lorg/xmlpull/v1/XmlPullParserException;

    const-string v7, "WizardScript must define a firstAction"

    invoke-direct {v6, v7}, Lorg/xmlpull/v1/XmlPullParserException;-><init>(Ljava/lang/String;)V

    throw v6

    .line 123
    :cond_1
    new-instance v1, Ljava/util/HashMap;

    invoke-direct {v1}, Ljava/util/HashMap;-><init>()V

    .line 125
    .local v1, "actions":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/String;Lcom/google/android/setupwizard/WizardAction;>;"
    invoke-interface {p0}, Lorg/xmlpull/v1/XmlPullParser;->getDepth()I

    move-result v4

    .line 128
    .local v4, "outerDepth":I
    :cond_2
    :goto_0
    invoke-interface {p0}, Lorg/xmlpull/v1/XmlPullParser;->next()I

    move-result v5

    .local v5, "type":I
    const/4 v6, 0x1

    if-eq v5, v6, :cond_5

    if-ne v5, v8, :cond_3

    invoke-interface {p0}, Lorg/xmlpull/v1/XmlPullParser;->getDepth()I

    move-result v6

    if-le v6, v4, :cond_5

    .line 129
    :cond_3
    if-eq v5, v8, :cond_2

    const/4 v6, 0x4

    if-eq v5, v6, :cond_2

    .line 133
    invoke-interface {p0}, Lorg/xmlpull/v1/XmlPullParser;->getName()Ljava/lang/String;

    move-result-object v3

    .line 134
    const-string v6, "WizardAction"

    invoke-virtual {v6, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_4

    .line 135
    invoke-static {p0}, Lcom/google/android/setupwizard/WizardAction;->parseWizardAction(Lorg/xmlpull/v1/XmlPullParser;)Lcom/google/android/setupwizard/WizardAction;

    move-result-object v0

    .line 136
    .local v0, "action":Lcom/google/android/setupwizard/WizardAction;
    if-eqz v0, :cond_2

    .line 137
    invoke-virtual {v0}, Lcom/google/android/setupwizard/WizardAction;->getId()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v1, v6, v0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    .line 140
    .end local v0    # "action":Lcom/google/android/setupwizard/WizardAction;
    :cond_4
    invoke-static {p0}, Lcom/android/internal/util/XmlUtils;->skipCurrentTag(Lorg/xmlpull/v1/XmlPullParser;)V

    goto :goto_0

    .line 144
    :cond_5
    new-instance v6, Lcom/google/android/setupwizard/WizardScript;

    invoke-direct {v6, v1, v2}, Lcom/google/android/setupwizard/WizardScript;-><init>(Ljava/util/Map;Ljava/lang/String;)V

    return-object v6
.end method


# virtual methods
.method public describeContents()I
    .locals 1

    .prologue
    .line 201
    const/4 v0, 0x0

    return v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 4
    .param p1, "obj"    # Ljava/lang/Object;

    .prologue
    const/4 v1, 0x0

    .line 185
    instance-of v2, p1, Lcom/google/android/setupwizard/WizardScript;

    if-eqz v2, :cond_0

    move-object v0, p1

    .line 186
    check-cast v0, Lcom/google/android/setupwizard/WizardScript;

    .line 187
    .local v0, "o":Lcom/google/android/setupwizard/WizardScript;
    iget-object v2, p0, Lcom/google/android/setupwizard/WizardScript;->mFirstActionId:Ljava/lang/String;

    if-eqz v2, :cond_1

    iget-object v2, p0, Lcom/google/android/setupwizard/WizardScript;->mFirstActionId:Ljava/lang/String;

    iget-object v3, v0, Lcom/google/android/setupwizard/WizardScript;->mFirstActionId:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    :goto_0
    iget-object v2, p0, Lcom/google/android/setupwizard/WizardScript;->mActions:Ljava/util/Map;

    if-eqz v2, :cond_2

    iget-object v2, p0, Lcom/google/android/setupwizard/WizardScript;->mActions:Ljava/util/Map;

    iget-object v3, v0, Lcom/google/android/setupwizard/WizardScript;->mActions:Ljava/util/Map;

    invoke-interface {v2, v3}, Ljava/util/Map;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    :goto_1
    const/4 v1, 0x1

    .line 191
    .end local v0    # "o":Lcom/google/android/setupwizard/WizardScript;
    :cond_0
    return v1

    .line 187
    .restart local v0    # "o":Lcom/google/android/setupwizard/WizardScript;
    :cond_1
    iget-object v2, v0, Lcom/google/android/setupwizard/WizardScript;->mFirstActionId:Ljava/lang/String;

    if-nez v2, :cond_0

    goto :goto_0

    :cond_2
    iget-object v2, v0, Lcom/google/android/setupwizard/WizardScript;->mActions:Ljava/util/Map;

    if-nez v2, :cond_0

    goto :goto_1
.end method

.method public getAction(Ljava/lang/String;)Lcom/google/android/setupwizard/WizardAction;
    .locals 1
    .param p1, "actionId"    # Ljava/lang/String;

    .prologue
    .line 172
    iget-object v0, p0, Lcom/google/android/setupwizard/WizardScript;->mActions:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/setupwizard/WizardAction;

    return-object v0
.end method

.method public getFirstAction()Lcom/google/android/setupwizard/WizardAction;
    .locals 1

    .prologue
    .line 168
    iget-object v0, p0, Lcom/google/android/setupwizard/WizardScript;->mFirstActionId:Ljava/lang/String;

    invoke-virtual {p0, v0}, Lcom/google/android/setupwizard/WizardScript;->getAction(Ljava/lang/String;)Lcom/google/android/setupwizard/WizardAction;

    move-result-object v0

    return-object v0
.end method

.method public getFirstActionId()Ljava/lang/String;
    .locals 1

    .prologue
    .line 149
    iget-object v0, p0, Lcom/google/android/setupwizard/WizardScript;->mFirstActionId:Ljava/lang/String;

    return-object v0
.end method

.method public getNextAction(Ljava/lang/String;I)Lcom/google/android/setupwizard/WizardAction;
    .locals 2
    .param p1, "currentActionId"    # Ljava/lang/String;
    .param p2, "resultCode"    # I

    .prologue
    .line 176
    invoke-virtual {p0, p1, p2}, Lcom/google/android/setupwizard/WizardScript;->getNextActionId(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    .line 177
    .local v0, "nextActionId":Ljava/lang/String;
    if-eqz v0, :cond_0

    .line 178
    invoke-virtual {p0, v0}, Lcom/google/android/setupwizard/WizardScript;->getAction(Ljava/lang/String;)Lcom/google/android/setupwizard/WizardAction;

    move-result-object v1

    .line 180
    :goto_0
    return-object v1

    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public getNextActionId(Ljava/lang/String;I)Ljava/lang/String;
    .locals 4
    .param p1, "currentActionId"    # Ljava/lang/String;
    .param p2, "resultCode"    # I

    .prologue
    .line 153
    if-eqz p2, :cond_1

    .line 154
    iget-object v1, p0, Lcom/google/android/setupwizard/WizardScript;->mActions:Ljava/util/Map;

    invoke-interface {v1, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/setupwizard/WizardAction;

    .line 155
    .local v0, "action":Lcom/google/android/setupwizard/WizardAction;
    const-string v2, "WizardScript"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "getNextActionId("

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v3, ","

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v3, ")"

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v3, " current uri="

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    if-nez v0, :cond_0

    const-string v1, "n/a"

    :goto_0
    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v2, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 157
    if-eqz v0, :cond_2

    .line 158
    invoke-virtual {v0, p2}, Lcom/google/android/setupwizard/WizardAction;->getNextAction(I)Ljava/lang/String;

    move-result-object v1

    .line 164
    .end local v0    # "action":Lcom/google/android/setupwizard/WizardAction;
    :goto_1
    return-object v1

    .line 155
    .restart local v0    # "action":Lcom/google/android/setupwizard/WizardAction;
    :cond_0
    invoke-virtual {v0}, Lcom/google/android/setupwizard/WizardAction;->getUri()Ljava/lang/String;

    move-result-object v1

    goto :goto_0

    .line 161
    .end local v0    # "action":Lcom/google/android/setupwizard/WizardAction;
    :cond_1
    const-string v1, "WizardScript"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "getNextActionId("

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ","

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ")"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " RESULT_CANCELED not expected; ignored"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 164
    :cond_2
    const/4 v1, 0x0

    goto :goto_1
.end method

.method public hashCode()I
    .locals 3

    .prologue
    .line 196
    const/4 v0, 0x2

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    iget-object v2, p0, Lcom/google/android/setupwizard/WizardScript;->mFirstActionId:Ljava/lang/String;

    aput-object v2, v0, v1

    const/4 v1, 0x1

    iget-object v2, p0, Lcom/google/android/setupwizard/WizardScript;->mActions:Ljava/util/Map;

    aput-object v2, v0, v1

    invoke-static {v0}, Ljava/util/Objects;->hash([Ljava/lang/Object;)I

    move-result v0

    return v0
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 2
    .param p1, "dest"    # Landroid/os/Parcel;
    .param p2, "flags"    # I

    .prologue
    .line 206
    iget-object v1, p0, Lcom/google/android/setupwizard/WizardScript;->mFirstActionId:Ljava/lang/String;

    invoke-virtual {p1, v1}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 208
    iget-object v1, p0, Lcom/google/android/setupwizard/WizardScript;->mActions:Ljava/util/Map;

    invoke-interface {v1}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v0

    .line 209
    .local v0, "values":Ljava/util/Collection;, "Ljava/util/Collection<Lcom/google/android/setupwizard/WizardAction;>;"
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1, v0}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    invoke-virtual {p1, v1}, Landroid/os/Parcel;->writeTypedList(Ljava/util/List;)V

    .line 210
    return-void
.end method
