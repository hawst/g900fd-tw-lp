.class public Lcom/google/android/setupwizard/network/ConnectionCheckActivity;
.super Lcom/google/android/setupwizard/BaseActivity;
.source "ConnectionCheckActivity.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 26
    invoke-direct {p0}, Lcom/google/android/setupwizard/BaseActivity;-><init>()V

    return-void
.end method


# virtual methods
.method protected onCreate(Landroid/os/Bundle;)V
    .locals 1
    .param p1, "icicle"    # Landroid/os/Bundle;

    .prologue
    .line 30
    invoke-super {p0, p1}, Lcom/google/android/setupwizard/BaseActivity;->onCreate(Landroid/os/Bundle;)V

    .line 31
    invoke-static {}, Lcom/google/android/setupwizard/network/NetworkMonitor;->getInstance()Lcom/google/android/setupwizard/network/NetworkMonitor;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/setupwizard/network/NetworkMonitor;->checkIsNetworkConnected()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 32
    const/4 v0, -0x1

    invoke-virtual {p0, v0}, Lcom/google/android/setupwizard/network/ConnectionCheckActivity;->nextScreen(I)V

    .line 36
    :goto_0
    invoke-virtual {p0}, Lcom/google/android/setupwizard/network/ConnectionCheckActivity;->finish()V

    .line 37
    return-void

    .line 34
    :cond_0
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/google/android/setupwizard/network/ConnectionCheckActivity;->nextScreen(I)V

    goto :goto_0
.end method
