.class public Lcom/google/android/setupwizard/account/RestoreProgressActivity;
.super Lcom/google/android/setupwizard/BaseActivity;
.source "RestoreProgressActivity.java"


# instance fields
.field private final mReceiver:Landroid/content/BroadcastReceiver;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 35
    invoke-direct {p0}, Lcom/google/android/setupwizard/BaseActivity;-><init>()V

    .line 39
    new-instance v0, Lcom/google/android/setupwizard/account/RestoreProgressActivity$1;

    invoke-direct {v0, p0}, Lcom/google/android/setupwizard/account/RestoreProgressActivity$1;-><init>(Lcom/google/android/setupwizard/account/RestoreProgressActivity;)V

    iput-object v0, p0, Lcom/google/android/setupwizard/account/RestoreProgressActivity;->mReceiver:Landroid/content/BroadcastReceiver;

    return-void
.end method

.method static synthetic access$000(Lcom/google/android/setupwizard/account/RestoreProgressActivity;Ljava/lang/String;)V
    .locals 0
    .param p0, "x0"    # Lcom/google/android/setupwizard/account/RestoreProgressActivity;
    .param p1, "x1"    # Ljava/lang/String;

    .prologue
    .line 35
    invoke-direct {p0, p1}, Lcom/google/android/setupwizard/account/RestoreProgressActivity;->updateInterfaceState(Ljava/lang/String;)V

    return-void
.end method

.method private updateInterfaceState(Ljava/lang/String;)V
    .locals 4
    .param p1, "state"    # Ljava/lang/String;

    .prologue
    const/4 v3, 0x1

    .line 48
    iget-object v0, p0, Lcom/google/android/setupwizard/account/RestoreProgressActivity;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "updateInterfaceState: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 49
    if-nez p1, :cond_1

    .line 51
    invoke-virtual {p0, v3}, Lcom/google/android/setupwizard/account/RestoreProgressActivity;->finishAction(I)V

    .line 64
    :cond_0
    :goto_0
    return-void

    .line 52
    :cond_1
    const-string v0, "preparing"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 53
    iget-object v0, p0, Lcom/google/android/setupwizard/account/RestoreProgressActivity;->TAG:Ljava/lang/String;

    const-string v1, "Preparing restore"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 54
    :cond_2
    const-string v0, "started"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 55
    iget-object v0, p0, Lcom/google/android/setupwizard/account/RestoreProgressActivity;->TAG:Ljava/lang/String;

    const-string v1, "Restore started"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 56
    :cond_3
    const-string v0, "failed"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 57
    iget-object v0, p0, Lcom/google/android/setupwizard/account/RestoreProgressActivity;->TAG:Ljava/lang/String;

    const-string v1, "Restore failed"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 58
    const v0, 0x7f0700ac

    const/4 v1, 0x0

    invoke-static {p0, v0, v1}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    .line 59
    invoke-virtual {p0, v3}, Lcom/google/android/setupwizard/account/RestoreProgressActivity;->finishAction(I)V

    goto :goto_0

    .line 60
    :cond_4
    const-string v0, "finished"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 61
    iget-object v0, p0, Lcom/google/android/setupwizard/account/RestoreProgressActivity;->TAG:Ljava/lang/String;

    const-string v1, "Restore completed"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 62
    const/4 v0, -0x1

    invoke-virtual {p0, v0}, Lcom/google/android/setupwizard/account/RestoreProgressActivity;->finishAction(I)V

    goto :goto_0
.end method


# virtual methods
.method protected getPreviousTransition()I
    .locals 1

    .prologue
    .line 89
    const/4 v0, 0x2

    return v0
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 2
    .param p1, "icicle"    # Landroid/os/Bundle;

    .prologue
    .line 68
    invoke-super {p0, p1}, Lcom/google/android/setupwizard/BaseActivity;->onCreate(Landroid/os/Bundle;)V

    .line 69
    const v0, 0x7f030013

    const v1, 0x7f030023

    invoke-virtual {p0, v0, v1}, Lcom/google/android/setupwizard/account/RestoreProgressActivity;->setTemplateContent(II)V

    .line 70
    const v0, 0x7f0700a8

    invoke-virtual {p0, v0}, Lcom/google/android/setupwizard/account/RestoreProgressActivity;->setHeaderText(I)V

    .line 71
    return-void
.end method

.method public onNavigationBarCreated(Lcom/android/setupwizard/navigationbar/SetupWizardNavBar;)V
    .locals 2
    .param p1, "bar"    # Lcom/android/setupwizard/navigationbar/SetupWizardNavBar;

    .prologue
    .line 94
    invoke-super {p0, p1}, Lcom/google/android/setupwizard/BaseActivity;->onNavigationBarCreated(Lcom/android/setupwizard/navigationbar/SetupWizardNavBar;)V

    .line 95
    invoke-virtual {p1}, Lcom/android/setupwizard/navigationbar/SetupWizardNavBar;->getNextButton()Landroid/widget/Button;

    move-result-object v0

    const v1, 0x7f070046

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setText(I)V

    .line 96
    invoke-virtual {p1}, Lcom/android/setupwizard/navigationbar/SetupWizardNavBar;->getNextButton()Landroid/widget/Button;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setEnabled(Z)V

    .line 97
    return-void
.end method

.method protected onPause()V
    .locals 2

    .prologue
    .line 83
    invoke-static {p0}, Landroid/support/v4/content/LocalBroadcastManager;->getInstance(Landroid/content/Context;)Landroid/support/v4/content/LocalBroadcastManager;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/setupwizard/account/RestoreProgressActivity;->mReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {v0, v1}, Landroid/support/v4/content/LocalBroadcastManager;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    .line 84
    invoke-super {p0}, Lcom/google/android/setupwizard/BaseActivity;->onPause()V

    .line 85
    return-void
.end method

.method protected onResume()V
    .locals 3

    .prologue
    .line 75
    invoke-super {p0}, Lcom/google/android/setupwizard/BaseActivity;->onResume()V

    .line 76
    new-instance v0, Landroid/content/IntentFilter;

    const-string v1, "com.google.android.setupwizard.RESTORE_STATE_CHANGE"

    invoke-direct {v0, v1}, Landroid/content/IntentFilter;-><init>(Ljava/lang/String;)V

    .line 77
    .local v0, "filter":Landroid/content/IntentFilter;
    invoke-static {p0}, Landroid/support/v4/content/LocalBroadcastManager;->getInstance(Landroid/content/Context;)Landroid/support/v4/content/LocalBroadcastManager;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/setupwizard/account/RestoreProgressActivity;->mReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {v1, v2, v0}, Landroid/support/v4/content/LocalBroadcastManager;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)V

    .line 78
    invoke-static {}, Lcom/google/android/setupwizard/util/BackupRestoreHelper;->getState()Ljava/lang/String;

    move-result-object v1

    invoke-direct {p0, v1}, Lcom/google/android/setupwizard/account/RestoreProgressActivity;->updateInterfaceState(Ljava/lang/String;)V

    .line 79
    return-void
.end method
