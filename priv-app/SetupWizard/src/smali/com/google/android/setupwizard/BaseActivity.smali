.class public Lcom/google/android/setupwizard/BaseActivity;
.super Landroid/app/Activity;
.source "BaseActivity.java"

# interfaces
.implements Landroid/view/View$OnApplyWindowInsetsListener;
.implements Lcom/android/setupwizard/navigationbar/SetupWizardNavBar$NavigationBarListener;
.implements Lcom/google/android/setupwizard/util/BottomScrollView$BottomScrollListener;
.implements Lcom/google/android/setupwizard/util/FourCornersGestureDetector$OnFourCornersListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/setupwizard/BaseActivity$ProvisioningMode;
    }
.end annotation


# static fields
.field public static final FILTER_HFA_CHANGE:Landroid/content/IntentFilter;

.field protected static final ROTATION_LOCKED:Z

.field protected static final SHOW_BACKUP_PICKER:Z


# instance fields
.field protected final TAG:Ljava/lang/String;

.field private mBackAllowed:Z

.field private mBackButton:Landroid/view/View;

.field protected mBootstrapMode:Ljava/lang/Boolean;

.field private mDefaultButton:Landroid/view/View;

.field private mDefaultButtonShouldScroll:Z

.field private mFourCornersDetector:Lcom/google/android/setupwizard/util/FourCornersGestureDetector;

.field private final mHandler:Landroid/os/Handler;

.field private mIsFirstRun:Z

.field private mIsPessimisticMode:Z

.field private mIsPrimaryUser:Z

.field private mIsResumeFromUpdate:Z

.field private final mLayoutNextRunnable:Ljava/lang/Runnable;

.field protected mNavigationBar:Lcom/android/setupwizard/navigationbar/SetupWizardNavBar;

.field private mNextAllowed:Z

.field private final mOnDefaultButtonClicked:Landroid/view/View$OnClickListener;

.field private mProvisioningMode:Lcom/google/android/setupwizard/BaseActivity$ProvisioningMode;

.field private mResultCode:I

.field private mResultData:Landroid/content/Intent;

.field mScreenSize:I

.field protected mScrollView:Lcom/google/android/setupwizard/util/BottomScrollView;

.field private mTelephonyInterface:Lcom/android/internal/telephony/ITelephony;

.field private mThemeType:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 127
    const-string v0, "ro.setupwizard.rotation_locked"

    const/4 v1, 0x0

    invoke-static {v0, v1}, Landroid/os/SystemProperties;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    sput-boolean v0, Lcom/google/android/setupwizard/BaseActivity;->ROTATION_LOCKED:Z

    .line 131
    const-string v0, "setupwizard.show_backup_picker"

    const/4 v1, 0x1

    invoke-static {v0, v1}, Landroid/os/SystemProperties;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    sput-boolean v0, Lcom/google/android/setupwizard/BaseActivity;->SHOW_BACKUP_PICKER:Z

    .line 241
    new-instance v0, Landroid/content/IntentFilter;

    const-string v1, "com.google.android.setupwizard.HFA_CHANGE"

    invoke-direct {v0, v1}, Landroid/content/IntentFilter;-><init>(Ljava/lang/String;)V

    sput-object v0, Lcom/google/android/setupwizard/BaseActivity;->FILTER_HFA_CHANGE:Landroid/content/IntentFilter;

    return-void
.end method

.method public constructor <init>()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 109
    invoke-direct {p0}, Landroid/app/Activity;-><init>()V

    .line 111
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "SetupWizard."

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/setupwizard/BaseActivity;->TAG:Ljava/lang/String;

    .line 426
    sget-object v0, Lcom/google/android/setupwizard/BaseActivity$ProvisioningMode;->OPTIONAL:Lcom/google/android/setupwizard/BaseActivity$ProvisioningMode;

    iput-object v0, p0, Lcom/google/android/setupwizard/BaseActivity;->mProvisioningMode:Lcom/google/android/setupwizard/BaseActivity$ProvisioningMode;

    .line 443
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/setupwizard/BaseActivity;->mBootstrapMode:Ljava/lang/Boolean;

    .line 450
    iput-boolean v2, p0, Lcom/google/android/setupwizard/BaseActivity;->mDefaultButtonShouldScroll:Z

    .line 457
    iput-boolean v3, p0, Lcom/google/android/setupwizard/BaseActivity;->mBackAllowed:Z

    .line 458
    iput-boolean v3, p0, Lcom/google/android/setupwizard/BaseActivity;->mNextAllowed:Z

    .line 460
    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    iput-object v0, p0, Lcom/google/android/setupwizard/BaseActivity;->mHandler:Landroid/os/Handler;

    .line 464
    iput v2, p0, Lcom/google/android/setupwizard/BaseActivity;->mResultCode:I

    .line 544
    new-instance v0, Lcom/google/android/setupwizard/BaseActivity$2;

    invoke-direct {v0, p0}, Lcom/google/android/setupwizard/BaseActivity$2;-><init>(Lcom/google/android/setupwizard/BaseActivity;)V

    iput-object v0, p0, Lcom/google/android/setupwizard/BaseActivity;->mOnDefaultButtonClicked:Landroid/view/View$OnClickListener;

    .line 1130
    new-instance v0, Lcom/google/android/setupwizard/BaseActivity$4;

    invoke-direct {v0, p0}, Lcom/google/android/setupwizard/BaseActivity$4;-><init>(Lcom/google/android/setupwizard/BaseActivity;)V

    iput-object v0, p0, Lcom/google/android/setupwizard/BaseActivity;->mLayoutNextRunnable:Ljava/lang/Runnable;

    return-void
.end method

.method static synthetic access$000(Lcom/google/android/setupwizard/BaseActivity;)Landroid/view/View;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/setupwizard/BaseActivity;

    .prologue
    .line 109
    iget-object v0, p0, Lcom/google/android/setupwizard/BaseActivity;->mDefaultButton:Landroid/view/View;

    return-object v0
.end method

.method static synthetic access$100(Lcom/google/android/setupwizard/BaseActivity;)V
    .locals 0
    .param p0, "x0"    # Lcom/google/android/setupwizard/BaseActivity;

    .prologue
    .line 109
    invoke-direct {p0}, Lcom/google/android/setupwizard/BaseActivity;->onNextPressed()V

    return-void
.end method

.method private enableComponentArray([Landroid/content/pm/ComponentInfo;)V
    .locals 5
    .param p1, "components"    # [Landroid/content/pm/ComponentInfo;

    .prologue
    .line 2516
    if-eqz p1, :cond_0

    .line 2517
    move-object v0, p1

    .local v0, "arr$":[Landroid/content/pm/ComponentInfo;
    array-length v3, v0

    .local v3, "len$":I
    const/4 v1, 0x0

    .local v1, "i$":I
    :goto_0
    if-ge v1, v3, :cond_0

    aget-object v2, v0, v1

    .line 2518
    .local v2, "info":Landroid/content/pm/ComponentInfo;
    iget-object v4, v2, Landroid/content/pm/ComponentInfo;->name:Ljava/lang/String;

    invoke-virtual {p0, v4}, Lcom/google/android/setupwizard/BaseActivity;->enableComponent(Ljava/lang/String;)V

    .line 2517
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 2521
    .end local v0    # "arr$":[Landroid/content/pm/ComponentInfo;
    .end local v1    # "i$":I
    .end local v2    # "info":Landroid/content/pm/ComponentInfo;
    .end local v3    # "len$":I
    :cond_0
    return-void
.end method

.method protected static getRequestName(I)Ljava/lang/String;
    .locals 3
    .param p0, "requestCode"    # I

    .prologue
    .line 935
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 936
    .local v0, "sb":Ljava/lang/StringBuilder;
    packed-switch p0, :pswitch_data_0

    .line 960
    :goto_0
    :pswitch_0
    const-string v1, "("

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ")"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 961
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    return-object v1

    .line 937
    :pswitch_1
    const-string v1, "NEXT_REQUEST"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_0

    .line 938
    :pswitch_2
    const-string v1, "WELCOME_REQUEST"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_0

    .line 939
    :pswitch_3
    const-string v1, "ACTIVATION_REQUEST"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_0

    .line 940
    :pswitch_4
    const-string v1, "OLD_ACTIVATION_REQUEST"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_0

    .line 941
    :pswitch_5
    const-string v1, "WIFI_ACTIVITY_REQUEST"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_0

    .line 942
    :pswitch_6
    const-string v1, "LOCATION_SHARING_REQUEST"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_0

    .line 943
    :pswitch_7
    const-string v1, "ACCOUNT_SETUP_REQUEST"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_0

    .line 944
    :pswitch_8
    const-string v1, "SIM_MISSING_REQUEST"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_0

    .line 945
    :pswitch_9
    const-string v1, "NAME_REQUEST"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_0

    .line 946
    :pswitch_a
    const-string v1, "PHOTO_REQUEST"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_0

    .line 947
    :pswitch_b
    const-string v1, "SETUP_COMPLETE_REQUEST"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_0

    .line 948
    :pswitch_c
    const-string v1, "NO_ACCOUNT_TOS_REQUEST"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_0

    .line 949
    :pswitch_d
    const-string v1, "WALLED_GARDEN_REQUEST"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_0

    .line 950
    :pswitch_e
    const-string v1, "DATE_TIME_SETUP_REQUEST"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_0

    .line 951
    :pswitch_f
    const-string v1, "OTA_UPDATE_REQUEST"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_0

    .line 952
    :pswitch_10
    const-string v1, "SYSTEM_UPDATE_REQUEST"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_0

    .line 953
    :pswitch_11
    const-string v1, "WELCOME_USER_REQUEST"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_0

    .line 954
    :pswitch_12
    const-string v1, "MOBILE_DATA_ACTIVITY_REQUEST"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_0

    .line 955
    :pswitch_13
    const-string v1, "CDMA_ACTIVATION_REQUEST"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto/16 :goto_0

    .line 956
    :pswitch_14
    const-string v1, "GOOGLE_NOW_REQUEST"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto/16 :goto_0

    .line 957
    :pswitch_15
    const-string v1, "CARRIER_SETUP_REQUEST"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto/16 :goto_0

    .line 958
    :pswitch_16
    const-string v1, "RESTORE_REQUEST"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto/16 :goto_0

    .line 936
    :pswitch_data_0
    .packed-switch 0x2710
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_0
        :pswitch_7
        :pswitch_0
        :pswitch_8
        :pswitch_9
        :pswitch_a
        :pswitch_b
        :pswitch_c
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_d
        :pswitch_e
        :pswitch_f
        :pswitch_10
        :pswitch_11
        :pswitch_12
        :pswitch_13
        :pswitch_14
        :pswitch_15
        :pswitch_0
        :pswitch_16
    .end packed-switch
.end method

.method protected static getResultName(II)Ljava/lang/String;
    .locals 3
    .param p0, "requestCode"    # I
    .param p1, "resultCode"    # I

    .prologue
    .line 965
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 966
    .local v0, "sb":Ljava/lang/StringBuilder;
    sparse-switch p0, :sswitch_data_0

    .line 1012
    packed-switch p1, :pswitch_data_0

    .line 1021
    :goto_0
    const-string v1, "("

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ")"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1022
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    return-object v1

    .line 968
    :sswitch_0
    packed-switch p1, :pswitch_data_1

    goto :goto_0

    .line 969
    :pswitch_0
    const-string v1, "RESULT_OK"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_0

    .line 970
    :pswitch_1
    const-string v1, "RESULT_CANCELED"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_0

    .line 971
    :pswitch_2
    const-string v1, "RESULT_WIFI_SKIP"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_0

    .line 975
    :sswitch_1
    packed-switch p1, :pswitch_data_2

    :pswitch_3
    goto :goto_0

    .line 976
    :pswitch_4
    const-string v1, "RESULT_OK"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_0

    .line 977
    :pswitch_5
    const-string v1, "RESULT_CANCELED"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_0

    .line 978
    :pswitch_6
    const-string v1, "RESULT_GLS_USER_EXIT"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_0

    .line 980
    :pswitch_7
    const-string v1, "RESULT_GLS_USERNAME_UNAVAILABLE"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_0

    .line 981
    :pswitch_8
    const-string v1, "RESULT_GLS_TASK_FAILED"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_0

    .line 982
    :pswitch_9
    const-string v1, "RESULT_GLS_BAD_PASSWORD"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_0

    .line 983
    :pswitch_a
    const-string v1, "RESULT_GLS_NEEDS_BROWSER"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_0

    .line 984
    :pswitch_b
    const-string v1, "RESULT_GLS_SKIP"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_0

    .line 985
    :pswitch_c
    const-string v1, "RESULT_GLS_LOGIN"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_0

    .line 986
    :pswitch_d
    const-string v1, "RESULT_GLS_CREATE"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_0

    .line 990
    :sswitch_2
    packed-switch p1, :pswitch_data_3

    :pswitch_e
    goto :goto_0

    .line 991
    :pswitch_f
    const-string v1, "RESULT_OK"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_0

    .line 992
    :pswitch_10
    const-string v1, "RESULT_CANCELED"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_0

    .line 993
    :pswitch_11
    const-string v1, "RESULT_ACTIVITY_NOT_FOUND"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_0

    .line 994
    :pswitch_12
    const-string v1, "RESULT_NOW_USER_BACK"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto/16 :goto_0

    .line 995
    :pswitch_13
    const-string v1, "RESULT_NOW_USER_OPTED_IN"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto/16 :goto_0

    .line 997
    :pswitch_14
    const-string v1, "RESULT_NOW_USER_NOT_OPTED_IN"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto/16 :goto_0

    .line 1001
    :sswitch_3
    packed-switch p1, :pswitch_data_4

    goto/16 :goto_0

    .line 1002
    :pswitch_15
    const-string v1, "RESULT_OK"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto/16 :goto_0

    .line 1003
    :pswitch_16
    const-string v1, "RESULT_CANCELED"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto/16 :goto_0

    .line 1005
    :pswitch_17
    const-string v1, "RESULT_CARRIER_SETUP_NOT_READY"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto/16 :goto_0

    .line 1007
    :pswitch_18
    const-string v1, "RESULT_CARRIER_SETUP_SKIPPED"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto/16 :goto_0

    .line 1008
    :pswitch_19
    const-string v1, "RESULT_CARRIER_SETUP_ERROR"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto/16 :goto_0

    .line 1013
    :pswitch_1a
    const-string v1, "RESULT_OK"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto/16 :goto_0

    .line 1014
    :pswitch_1b
    const-string v1, "RESULT_CANCELED"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto/16 :goto_0

    .line 1015
    :pswitch_1c
    const-string v1, "RESULT_SKIP"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto/16 :goto_0

    .line 1016
    :pswitch_1d
    const-string v1, "RESULT_RETRY"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto/16 :goto_0

    .line 1017
    :pswitch_1e
    const-string v1, "RESULT_ACTIVITY_NOT_FOUND"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto/16 :goto_0

    .line 966
    nop

    :sswitch_data_0
    .sparse-switch
        0x2714 -> :sswitch_0
        0x2717 -> :sswitch_1
        0x2728 -> :sswitch_2
        0x2729 -> :sswitch_3
    .end sparse-switch

    .line 1012
    :pswitch_data_0
    .packed-switch -0x1
        :pswitch_1a
        :pswitch_1b
        :pswitch_1c
        :pswitch_1d
        :pswitch_1e
    .end packed-switch

    .line 968
    :pswitch_data_1
    .packed-switch -0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch

    .line 975
    :pswitch_data_2
    .packed-switch -0x1
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_3
        :pswitch_7
        :pswitch_8
        :pswitch_9
        :pswitch_a
        :pswitch_b
        :pswitch_c
        :pswitch_d
    .end packed-switch

    .line 990
    :pswitch_data_3
    .packed-switch -0x1
        :pswitch_f
        :pswitch_10
        :pswitch_e
        :pswitch_e
        :pswitch_11
        :pswitch_e
        :pswitch_e
        :pswitch_e
        :pswitch_e
        :pswitch_e
        :pswitch_e
        :pswitch_12
        :pswitch_13
        :pswitch_14
    .end packed-switch

    .line 1001
    :pswitch_data_4
    .packed-switch -0x1
        :pswitch_15
        :pswitch_16
        :pswitch_17
        :pswitch_18
        :pswitch_19
    .end packed-switch
.end method

.method private launchAccountSetup()V
    .locals 2

    .prologue
    .line 1814
    iget-object v0, p0, Lcom/google/android/setupwizard/BaseActivity;->TAG:Ljava/lang/String;

    const-string v1, "launchAccountSetup()"

    invoke-static {v0, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 1815
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/google/android/setupwizard/account/AccountSetupWrapper;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const/16 v1, 0x2717

    invoke-virtual {p0, v0, v1}, Lcom/google/android/setupwizard/BaseActivity;->startFirstRunActivityForResult(Landroid/content/Intent;I)V

    .line 1817
    return-void
.end method

.method private launchActivationActivity()V
    .locals 10

    .prologue
    const v9, 0x1120043

    const/4 v8, 0x0

    .line 1628
    iget-object v5, p0, Lcom/google/android/setupwizard/BaseActivity;->TAG:Ljava/lang/String;

    const-string v6, "launchActivationActivity() E"

    invoke-static {v5, v6}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 1632
    invoke-virtual {p0}, Lcom/google/android/setupwizard/BaseActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    .line 1633
    .local v3, "res":Landroid/content/res/Resources;
    iget-object v5, p0, Lcom/google/android/setupwizard/BaseActivity;->TAG:Ljava/lang/String;

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "launchActivationActivity() voice: "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v3, v9}, Landroid/content/res/Resources;->getBoolean(I)Z

    move-result v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, ", lte: "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-static {}, Lcom/google/android/setupwizard/carrier/PhoneMonitor;->isLTE()Z

    move-result v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, ", cdma_always: "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-static {}, Lcom/google/android/setupwizard/carrier/PhoneMonitor;->isCdmaActivationRequired()Z

    move-result v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, ", mcc: "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v3}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v7

    iget v7, v7, Landroid/content/res/Configuration;->mcc:I

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, ", mnc: "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v3}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v7

    iget v7, v7, Landroid/content/res/Configuration;->mnc:I

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 1639
    invoke-static {}, Lcom/google/android/setupwizard/carrier/PhoneMonitor;->isCdmaActivationRequired()Z

    move-result v5

    if-eqz v5, :cond_0

    .line 1640
    iget-object v5, p0, Lcom/google/android/setupwizard/BaseActivity;->TAG:Ljava/lang/String;

    const-string v6, "CDMA activation should have been triggered in launchActivationActivityOrSkip"

    invoke-static {v5, v6}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 1642
    invoke-virtual {p0, v8}, Lcom/google/android/setupwizard/BaseActivity;->onActivationActivityCompleted(I)V

    .line 1667
    :goto_0
    iget-object v5, p0, Lcom/google/android/setupwizard/BaseActivity;->TAG:Ljava/lang/String;

    const-string v6, "launchActivationActivity() X RESULT_OK"

    invoke-static {v5, v6}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 1668
    return-void

    .line 1643
    :cond_0
    invoke-virtual {v3, v9}, Landroid/content/res/Resources;->getBoolean(I)Z

    move-result v5

    if-eqz v5, :cond_1

    invoke-static {}, Lcom/google/android/setupwizard/carrier/PhoneMonitor;->isLTE()Z

    move-result v5

    if-nez v5, :cond_1

    .line 1647
    new-instance v1, Landroid/content/Intent;

    const-string v5, "com.android.phone.PERFORM_CDMA_PROVISIONING"

    invoke-direct {v1, v5}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 1652
    .local v1, "newIntent":Landroid/content/Intent;
    new-instance v4, Landroid/content/Intent;

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v5

    invoke-direct {v4, p0, v5}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 1653
    .local v4, "returnIntent":Landroid/content/Intent;
    invoke-static {p0, v8, v4, v8}, Landroid/app/PendingIntent;->getActivity(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v2

    .line 1655
    .local v2, "pending":Landroid/app/PendingIntent;
    const-string v5, "otasp_result_code_pending_intent"

    invoke-virtual {v1, v5, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 1657
    const/16 v5, 0x2713

    :try_start_0
    invoke-virtual {p0, v1, v5}, Lcom/google/android/setupwizard/BaseActivity;->startFirstRunActivityForResult(Landroid/content/Intent;I)V
    :try_end_0
    .catch Landroid/content/ActivityNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 1658
    :catch_0
    move-exception v0

    .line 1659
    .local v0, "e":Landroid/content/ActivityNotFoundException;
    iget-object v5, p0, Lcom/google/android/setupwizard/BaseActivity;->TAG:Ljava/lang/String;

    const-string v6, "launchActivationActivity() Failed to launch 10003"

    invoke-static {v5, v6, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 1661
    invoke-virtual {p0, v8}, Lcom/google/android/setupwizard/BaseActivity;->onActivationActivityCompleted(I)V

    goto :goto_0

    .line 1664
    .end local v0    # "e":Landroid/content/ActivityNotFoundException;
    .end local v1    # "newIntent":Landroid/content/Intent;
    .end local v2    # "pending":Landroid/app/PendingIntent;
    .end local v4    # "returnIntent":Landroid/content/Intent;
    :cond_1
    new-instance v1, Landroid/content/Intent;

    const-class v5, Lcom/google/android/setupwizard/carrier/ActivationActivity;

    invoke-direct {v1, p0, v5}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 1665
    .restart local v1    # "newIntent":Landroid/content/Intent;
    const/16 v5, 0x2712

    invoke-virtual {p0, v1, v5}, Lcom/google/android/setupwizard/BaseActivity;->startFirstRunActivityForResult(Landroid/content/Intent;I)V

    goto :goto_0
.end method

.method private launchActivationActivityOrSkip()V
    .locals 2

    .prologue
    .line 1593
    iget-object v0, p0, Lcom/google/android/setupwizard/BaseActivity;->TAG:Ljava/lang/String;

    const-string v1, "launchActivationActivityOrSkip()"

    invoke-static {v0, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 1599
    invoke-static {}, Lcom/google/android/setupwizard/carrier/PhoneMonitor;->isCdmaActivationRequired()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1600
    iget-object v0, p0, Lcom/google/android/setupwizard/BaseActivity;->TAG:Ljava/lang/String;

    const-string v1, "cdma activation required"

    invoke-static {v0, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 1603
    invoke-direct {p0}, Lcom/google/android/setupwizard/BaseActivity;->launchMobileDataActivity()V

    .line 1620
    :goto_0
    return-void

    .line 1604
    :cond_0
    invoke-static {}, Lcom/google/android/setupwizard/carrier/PhoneMonitor;->otaspStateIsKnown()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-static {}, Lcom/google/android/setupwizard/carrier/PhoneMonitor;->otaspIsNeeded()Z

    move-result v0

    if-nez v0, :cond_2

    .line 1609
    :cond_1
    iget-object v0, p0, Lcom/google/android/setupwizard/BaseActivity;->TAG:Ljava/lang/String;

    const-string v1, "otasp not needed or not known, skipping activation"

    invoke-static {v0, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 1610
    const/4 v0, -0x1

    invoke-virtual {p0, v0}, Lcom/google/android/setupwizard/BaseActivity;->onActivationActivityCompleted(I)V

    goto :goto_0

    .line 1611
    :cond_2
    invoke-static {}, Lcom/google/android/setupwizard/carrier/PhoneMonitor;->lteUnknown()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 1614
    iget-object v0, p0, Lcom/google/android/setupwizard/BaseActivity;->TAG:Ljava/lang/String;

    const-string v1, "LTE state still unknown, skipping activation"

    invoke-static {v0, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 1615
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/google/android/setupwizard/BaseActivity;->onActivationActivityCompleted(I)V

    goto :goto_0

    .line 1617
    :cond_3
    iget-object v0, p0, Lcom/google/android/setupwizard/BaseActivity;->TAG:Ljava/lang/String;

    const-string v1, "falling through to launch activation"

    invoke-static {v0, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 1618
    invoke-direct {p0}, Lcom/google/android/setupwizard/BaseActivity;->launchActivationActivity()V

    goto :goto_0
.end method

.method private launchCarrierSetupOrSkip()V
    .locals 2

    .prologue
    .line 1678
    iget-object v0, p0, Lcom/google/android/setupwizard/BaseActivity;->TAG:Ljava/lang/String;

    const-string v1, "launchCarrierSetupOrSkip()"

    invoke-static {v0, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 1679
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/google/android/setupwizard/carrier/CarrierSetupWrapper;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const/16 v1, 0x2729

    invoke-virtual {p0, v0, v1}, Lcom/google/android/setupwizard/BaseActivity;->startFirstRunActivityForResult(Landroid/content/Intent;I)V

    .line 1681
    return-void
.end method

.method private launchDateTimeSetupOrSkip()V
    .locals 2

    .prologue
    .line 1782
    iget-object v0, p0, Lcom/google/android/setupwizard/BaseActivity;->TAG:Ljava/lang/String;

    const-string v1, "launchDateTimeSetupOrSkip()"

    invoke-static {v0, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 1783
    invoke-virtual {p0}, Lcom/google/android/setupwizard/BaseActivity;->isPrimaryUser()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-virtual {p0}, Lcom/google/android/setupwizard/BaseActivity;->isTimeSet()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/google/android/setupwizard/BaseActivity;->isTimeZoneSet()Z

    move-result v0

    if-nez v0, :cond_1

    .line 1784
    :cond_0
    invoke-direct {p0}, Lcom/google/android/setupwizard/BaseActivity;->startDateTimeSetup()V

    .line 1788
    :goto_0
    return-void

    .line 1786
    :cond_1
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/google/android/setupwizard/BaseActivity;->onDateTimeSetupCompleted(Z)V

    goto :goto_0
.end method

.method private launchGoogleNow()V
    .locals 2

    .prologue
    .line 1898
    iget-object v0, p0, Lcom/google/android/setupwizard/BaseActivity;->TAG:Ljava/lang/String;

    const-string v1, "launchGoogleNow()"

    invoke-static {v0, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 1899
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/google/android/setupwizard/account/GoogleNowWrapper;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const/16 v1, 0x2728

    invoke-virtual {p0, v0, v1}, Lcom/google/android/setupwizard/BaseActivity;->startFirstRunActivityForResult(Landroid/content/Intent;I)V

    .line 1901
    return-void
.end method

.method private launchMobileDataActivity()V
    .locals 2

    .prologue
    .line 1671
    iget-object v0, p0, Lcom/google/android/setupwizard/BaseActivity;->TAG:Ljava/lang/String;

    const-string v1, "launchMobileDataActivity()"

    invoke-static {v0, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 1672
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/google/android/setupwizard/carrier/MobileDataActivity;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const/16 v1, 0x2726

    invoke-virtual {p0, v0, v1}, Lcom/google/android/setupwizard/BaseActivity;->startFirstRunActivityForResult(Landroid/content/Intent;I)V

    .line 1675
    return-void
.end method

.method private launchNameActivity()V
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 1876
    iget-object v2, p0, Lcom/google/android/setupwizard/BaseActivity;->TAG:Ljava/lang/String;

    const-string v3, "launchNameActivity()"

    invoke-static {v2, v3}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 1877
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    .line 1878
    .local v0, "nextIntent":Landroid/content/Intent;
    const-string v2, "com.google.android.gsf.login"

    const-string v3, "com.google.android.gsf.login.NameActivity"

    invoke-virtual {v0, v2, v3}, Landroid/content/Intent;->setClassName(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 1879
    invoke-virtual {p0}, Lcom/google/android/setupwizard/BaseActivity;->getLocalPreferences()Landroid/content/SharedPreferences;

    move-result-object v1

    .line 1880
    .local v1, "pref":Landroid/content/SharedPreferences;
    const-string v2, "firstName"

    invoke-interface {v1, v2}, Landroid/content/SharedPreferences;->contains(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 1881
    const-string v2, "firstName"

    const-string v3, "firstName"

    invoke-interface {v1, v3, v4}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 1884
    :cond_0
    const-string v2, "lastName"

    invoke-interface {v1, v2}, Landroid/content/SharedPreferences;->contains(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 1885
    const-string v2, "lastName"

    const-string v3, "lastName"

    invoke-interface {v1, v3, v4}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 1888
    :cond_1
    invoke-virtual {p0}, Lcom/google/android/setupwizard/BaseActivity;->isPrimaryUser()Z

    move-result v2

    if-eqz v2, :cond_2

    .line 1889
    const-string v2, "nameType"

    const/4 v3, 0x1

    invoke-virtual {v0, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 1893
    :goto_0
    const-string v2, "suppressNameCheck"

    invoke-virtual {p0}, Lcom/google/android/setupwizard/BaseActivity;->shouldSuppressNameCheck()Z

    move-result v3

    invoke-virtual {v0, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 1894
    const/16 v2, 0x271a

    invoke-virtual {p0, v0, v2}, Lcom/google/android/setupwizard/BaseActivity;->startFirstRunActivityForResult(Landroid/content/Intent;I)V

    .line 1895
    return-void

    .line 1891
    :cond_2
    const-string v2, "nameType"

    const/4 v3, 0x2

    invoke-virtual {v0, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    goto :goto_0
.end method

.method private launchOtaUpdateCheck()V
    .locals 3

    .prologue
    .line 1864
    iget-object v1, p0, Lcom/google/android/setupwizard/BaseActivity;->TAG:Ljava/lang/String;

    const-string v2, "launchOtaUpdateCheck()"

    invoke-static {v1, v2}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 1865
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/google/android/setupwizard/update/OtaUpdateActivity;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 1866
    .local v0, "nextIntent":Landroid/content/Intent;
    const/16 v1, 0x2723

    invoke-virtual {p0, v0, v1}, Lcom/google/android/setupwizard/BaseActivity;->startFirstRunActivityForResult(Landroid/content/Intent;I)V

    .line 1867
    return-void
.end method

.method private launchSimMissingActivity()V
    .locals 2

    .prologue
    .line 1734
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/google/android/setupwizard/carrier/SimMissingActivity;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 1735
    .local v0, "intent":Landroid/content/Intent;
    const/16 v1, 0x2719

    invoke-virtual {p0, v0, v1}, Lcom/google/android/setupwizard/BaseActivity;->startFirstRunActivityForResult(Landroid/content/Intent;I)V

    .line 1736
    return-void
.end method

.method private launchSystemUpdateActivity()V
    .locals 3

    .prologue
    .line 1870
    iget-object v1, p0, Lcom/google/android/setupwizard/BaseActivity;->TAG:Ljava/lang/String;

    const-string v2, "launchSystemUpdateActivity()"

    invoke-static {v1, v2}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 1871
    new-instance v0, Landroid/content/Intent;

    const-string v1, "android.settings.SYSTEM_UPDATE_SETTINGS"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 1872
    .local v0, "nextIntent":Landroid/content/Intent;
    const/16 v1, 0x2724

    invoke-virtual {p0, v0, v1}, Lcom/google/android/setupwizard/BaseActivity;->startFirstRunActivityForResult(Landroid/content/Intent;I)V

    .line 1873
    return-void
.end method

.method private launchWalledGardenCheck()V
    .locals 3

    .prologue
    .line 1858
    iget-object v1, p0, Lcom/google/android/setupwizard/BaseActivity;->TAG:Ljava/lang/String;

    const-string v2, "launchWalledGardenCheck()"

    invoke-static {v1, v2}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 1859
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/google/android/setupwizard/network/WalledGardenActivity;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 1860
    .local v0, "nextIntent":Landroid/content/Intent;
    const/16 v1, 0x2721

    invoke-virtual {p0, v0, v1}, Lcom/google/android/setupwizard/BaseActivity;->startFirstRunActivityForResult(Landroid/content/Intent;I)V

    .line 1861
    return-void
.end method

.method private launchWifiSetupOrSkip()V
    .locals 5

    .prologue
    .line 1687
    iget-object v2, p0, Lcom/google/android/setupwizard/BaseActivity;->TAG:Ljava/lang/String;

    const-string v3, "launchWifiSetup()"

    invoke-static {v2, v3}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 1690
    invoke-virtual {p0}, Lcom/google/android/setupwizard/BaseActivity;->getLocalPreferences()Landroid/content/SharedPreferences;

    move-result-object v2

    invoke-interface {v2}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v2

    const-string v3, "wifiScreenShown"

    invoke-interface {v2, v3}, Landroid/content/SharedPreferences$Editor;->remove(Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    move-result-object v2

    invoke-interface {v2}, Landroid/content/SharedPreferences$Editor;->apply()V

    .line 1696
    const-string v2, "SetupWizardPrefs"

    const/4 v3, 0x4

    invoke-virtual {p0, v2, v3}, Lcom/google/android/setupwizard/BaseActivity;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v2

    const-string v3, "connection_status"

    const/4 v4, 0x0

    invoke-interface {v2, v3, v4}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v1

    .line 1700
    .local v1, "hasValidConnection":Z
    invoke-static {}, Lcom/google/android/setupwizard/carrier/PhoneMonitor;->canSkipWifiSetup()Z

    move-result v0

    .line 1703
    .local v0, "canSkipWifi":Z
    if-eqz v1, :cond_0

    if-eqz v0, :cond_0

    .line 1704
    const/4 v2, 0x1

    invoke-virtual {p0, v2}, Lcom/google/android/setupwizard/BaseActivity;->onWifiSetupCompleted(I)V

    .line 1709
    :goto_0
    return-void

    .line 1706
    :cond_0
    new-instance v2, Landroid/content/Intent;

    const-class v3, Lcom/google/android/setupwizard/network/WifiSettingsWrapper;

    invoke-direct {v2, p0, v3}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const/16 v3, 0x2714

    invoke-virtual {p0, v2, v3}, Lcom/google/android/setupwizard/BaseActivity;->startFirstRunActivityForResult(Landroid/content/Intent;I)V

    goto :goto_0
.end method

.method private logActivityState(Ljava/lang/String;)V
    .locals 3
    .param p1, "prefix"    # Ljava/lang/String;

    .prologue
    .line 793
    iget-object v0, p0, Lcom/google/android/setupwizard/BaseActivity;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " isResumed="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Lcom/google/android/setupwizard/BaseActivity;->isResumed()Z

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " isFinishing="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Lcom/google/android/setupwizard/BaseActivity;->isFinishing()Z

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " isDestroyed="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Lcom/google/android/setupwizard/BaseActivity;->isDestroyed()Z

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " topTask="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Lcom/google/android/setupwizard/BaseActivity;->topTask()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 798
    return-void
.end method

.method private noName()Z
    .locals 3

    .prologue
    .line 2121
    invoke-virtual {p0}, Lcom/google/android/setupwizard/BaseActivity;->getLocalPreferences()Landroid/content/SharedPreferences;

    move-result-object v0

    const-string v1, "doShowNameActivity"

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    return v0
.end method

.method private onAccountSetupCompleted(ZLandroid/content/Intent;)V
    .locals 3
    .param p1, "goForward"    # Z
    .param p2, "resultData"    # Landroid/content/Intent;

    .prologue
    .line 1509
    iget-object v0, p0, Lcom/google/android/setupwizard/BaseActivity;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "onAccountSetupCompleted goForward="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " resultData="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 1511
    if-eqz p1, :cond_1

    .line 1512
    invoke-virtual {p0}, Lcom/google/android/setupwizard/BaseActivity;->hasNoAccounts()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1513
    invoke-direct {p0}, Lcom/google/android/setupwizard/BaseActivity;->startLocationSharingActivity()V

    .line 1520
    :goto_0
    return-void

    .line 1515
    :cond_0
    invoke-direct {p0}, Lcom/google/android/setupwizard/BaseActivity;->launchDateTimeSetupOrSkip()V

    goto :goto_0

    .line 1518
    :cond_1
    invoke-virtual {p0}, Lcom/google/android/setupwizard/BaseActivity;->startWelcomeActivity()V

    goto :goto_0
.end method

.method private onDateTimeSetupCompleted(Z)V
    .locals 3
    .param p1, "goForward"    # Z

    .prologue
    .line 1797
    iget-object v0, p0, Lcom/google/android/setupwizard/BaseActivity;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "onDateTimeSetupCompleted("

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ")"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 1798
    if-eqz p1, :cond_1

    .line 1799
    invoke-direct {p0}, Lcom/google/android/setupwizard/BaseActivity;->noName()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1800
    invoke-direct {p0}, Lcom/google/android/setupwizard/BaseActivity;->launchNameActivity()V

    .line 1811
    :goto_0
    return-void

    .line 1802
    :cond_0
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/google/android/setupwizard/BaseActivity;->onNameActivityCompleted(Z)V

    goto :goto_0

    .line 1805
    :cond_1
    invoke-virtual {p0}, Lcom/google/android/setupwizard/BaseActivity;->hasNoAccounts()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 1806
    invoke-direct {p0}, Lcom/google/android/setupwizard/BaseActivity;->startLocationSharingActivity()V

    goto :goto_0

    .line 1808
    :cond_2
    invoke-direct {p0}, Lcom/google/android/setupwizard/BaseActivity;->launchAccountSetup()V

    goto :goto_0
.end method

.method private onGoogleNowCompleted(I)V
    .locals 3
    .param p1, "resultCode"    # I

    .prologue
    .line 1566
    iget-object v0, p0, Lcom/google/android/setupwizard/BaseActivity;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "onGoogleNowCompleted resultCode="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const/16 v2, 0x2728

    invoke-static {v2, p1}, Lcom/google/android/setupwizard/BaseActivity;->getResultName(II)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 1569
    const/4 v0, 0x3

    if-ne p1, v0, :cond_0

    .line 1570
    invoke-direct {p0}, Lcom/google/android/setupwizard/BaseActivity;->startSetupCompleteActivity()V

    .line 1574
    :goto_0
    return-void

    .line 1572
    :cond_0
    invoke-virtual {p0}, Lcom/google/android/setupwizard/BaseActivity;->onSetupComplete()V

    goto :goto_0
.end method

.method private onNameActivityCompleted(Z)V
    .locals 3
    .param p1, "goForward"    # Z

    .prologue
    .line 1523
    iget-object v0, p0, Lcom/google/android/setupwizard/BaseActivity;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "onNameActivityCompleted goForward="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 1524
    if-eqz p1, :cond_1

    .line 1525
    invoke-virtual {p0}, Lcom/google/android/setupwizard/BaseActivity;->hasNoAccounts()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1526
    invoke-direct {p0}, Lcom/google/android/setupwizard/BaseActivity;->startNoAccountTosActivity()V

    .line 1533
    :goto_0
    return-void

    .line 1528
    :cond_0
    invoke-direct {p0}, Lcom/google/android/setupwizard/BaseActivity;->launchGoogleNow()V

    goto :goto_0

    .line 1531
    :cond_1
    invoke-direct {p0}, Lcom/google/android/setupwizard/BaseActivity;->launchDateTimeSetupOrSkip()V

    goto :goto_0
.end method

.method private onNextPressed()V
    .locals 2

    .prologue
    .line 529
    iget-boolean v0, p0, Lcom/google/android/setupwizard/BaseActivity;->mDefaultButtonShouldScroll:Z

    if-eqz v0, :cond_0

    .line 530
    iget-object v0, p0, Lcom/google/android/setupwizard/BaseActivity;->mScrollView:Lcom/google/android/setupwizard/util/BottomScrollView;

    const/16 v1, 0x82

    invoke-virtual {v0, v1}, Lcom/google/android/setupwizard/util/BottomScrollView;->pageScroll(I)Z

    .line 534
    :goto_0
    return-void

    .line 532
    :cond_0
    invoke-virtual {p0}, Lcom/google/android/setupwizard/BaseActivity;->start()V

    goto :goto_0
.end method

.method private onNoAccountTosCompleted(Z)V
    .locals 3
    .param p1, "goForward"    # Z

    .prologue
    .line 1549
    iget-object v0, p0, Lcom/google/android/setupwizard/BaseActivity;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "onNoAccountTosCompleted goForward="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 1550
    if-eqz p1, :cond_1

    .line 1551
    invoke-virtual {p0}, Lcom/google/android/setupwizard/BaseActivity;->isNexus()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1552
    invoke-direct {p0}, Lcom/google/android/setupwizard/BaseActivity;->startSetupCompleteActivity()V

    .line 1563
    :goto_0
    return-void

    .line 1554
    :cond_0
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/google/android/setupwizard/BaseActivity;->onSetupCompleteCompleted(Z)V

    goto :goto_0

    .line 1557
    :cond_1
    invoke-direct {p0}, Lcom/google/android/setupwizard/BaseActivity;->noName()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 1558
    invoke-direct {p0}, Lcom/google/android/setupwizard/BaseActivity;->launchNameActivity()V

    goto :goto_0

    .line 1560
    :cond_2
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/google/android/setupwizard/BaseActivity;->onNameActivityCompleted(Z)V

    goto :goto_0
.end method

.method private onPhotoActivityCompleted(Z)V
    .locals 3
    .param p1, "goForward"    # Z

    .prologue
    .line 1536
    iget-object v0, p0, Lcom/google/android/setupwizard/BaseActivity;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "onPhotoActivityCompleted goForward="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 1537
    if-eqz p1, :cond_1

    .line 1538
    invoke-virtual {p0}, Lcom/google/android/setupwizard/BaseActivity;->hasNoAccounts()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1539
    invoke-direct {p0}, Lcom/google/android/setupwizard/BaseActivity;->startLocationSharingActivity()V

    .line 1546
    :goto_0
    return-void

    .line 1541
    :cond_0
    invoke-direct {p0}, Lcom/google/android/setupwizard/BaseActivity;->launchDateTimeSetupOrSkip()V

    goto :goto_0

    .line 1544
    :cond_1
    invoke-direct {p0}, Lcom/google/android/setupwizard/BaseActivity;->launchNameActivity()V

    goto :goto_0
.end method

.method private onSimMissingActivityCompleted(I)V
    .locals 3
    .param p1, "resultCode"    # I

    .prologue
    .line 1739
    iget-object v0, p0, Lcom/google/android/setupwizard/BaseActivity;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "onSimMissingActivityCompleted("

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ")"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 1740
    if-nez p1, :cond_0

    .line 1741
    invoke-virtual {p0}, Lcom/google/android/setupwizard/BaseActivity;->onWelcomeScreenCompleted()V

    .line 1745
    :goto_0
    return-void

    .line 1743
    :cond_0
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/google/android/setupwizard/BaseActivity;->onActivationActivityCompleted(I)V

    goto :goto_0
.end method

.method private setThemeFromConfig()V
    .locals 6

    .prologue
    .line 2354
    invoke-virtual {p0}, Lcom/google/android/setupwizard/BaseActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f070039

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    iput-object v3, p0, Lcom/google/android/setupwizard/BaseActivity;->mThemeType:Ljava/lang/String;

    .line 2356
    const-string v3, "ro.setupwizard.theme"

    iget-object v4, p0, Lcom/google/android/setupwizard/BaseActivity;->mThemeType:Ljava/lang/String;

    invoke-static {v3, v4}, Landroid/os/SystemProperties;->get(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    iput-object v3, p0, Lcom/google/android/setupwizard/BaseActivity;->mThemeType:Ljava/lang/String;

    .line 2357
    invoke-virtual {p0}, Lcom/google/android/setupwizard/BaseActivity;->getIntent()Landroid/content/Intent;

    move-result-object v1

    .line 2358
    .local v1, "intent":Landroid/content/Intent;
    if-eqz v1, :cond_0

    const-string v3, "theme"

    invoke-virtual {v1, v3}, Landroid/content/Intent;->hasExtra(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 2360
    const-string v3, "theme"

    invoke-virtual {v1, v3}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 2361
    .local v0, "extraTheme":Ljava/lang/String;
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_0

    .line 2362
    iput-object v0, p0, Lcom/google/android/setupwizard/BaseActivity;->mThemeType:Ljava/lang/String;

    .line 2366
    .end local v0    # "extraTheme":Ljava/lang/String;
    :cond_0
    iget-object v3, p0, Lcom/google/android/setupwizard/BaseActivity;->mThemeType:Ljava/lang/String;

    invoke-virtual {p0, v3}, Lcom/google/android/setupwizard/BaseActivity;->getStyleForTheme(Ljava/lang/String;)I

    move-result v2

    .line 2367
    .local v2, "themeId":I
    iget-object v3, p0, Lcom/google/android/setupwizard/BaseActivity;->TAG:Ljava/lang/String;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "setThemeFromConfig() mThemeType="

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-object v5, p0, Lcom/google/android/setupwizard/BaseActivity;->mThemeType:Ljava/lang/String;

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " themeId="

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 2368
    if-eqz v2, :cond_1

    .line 2369
    invoke-virtual {p0, v2}, Lcom/google/android/setupwizard/BaseActivity;->setTheme(I)V

    .line 2371
    :cond_1
    return-void
.end method

.method private startDateTimeSetup()V
    .locals 2

    .prologue
    .line 1791
    iget-object v0, p0, Lcom/google/android/setupwizard/BaseActivity;->TAG:Ljava/lang/String;

    const-string v1, "startDateTimeSetup()"

    invoke-static {v0, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 1792
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/google/android/setupwizard/time/DateTimeSetupActivity;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const/16 v1, 0x2722

    invoke-virtual {p0, v0, v1}, Lcom/google/android/setupwizard/BaseActivity;->startFirstRunActivityForResult(Landroid/content/Intent;I)V

    .line 1794
    return-void
.end method

.method private startLocationSharingActivity()V
    .locals 5

    .prologue
    const/4 v2, 0x1

    .line 1748
    iget-object v3, p0, Lcom/google/android/setupwizard/BaseActivity;->TAG:Ljava/lang/String;

    const-string v4, "startLocationSharingActivity()"

    invoke-static {v3, v4}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 1749
    new-instance v1, Landroid/content/Intent;

    const-class v3, Lcom/google/android/setupwizard/user/LocationSharingActivity;

    invoke-direct {v1, p0, v3}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 1750
    .local v1, "intent":Landroid/content/Intent;
    invoke-virtual {p0}, Lcom/google/android/setupwizard/BaseActivity;->getLocalPreferences()Landroid/content/SharedPreferences;

    move-result-object v3

    const-string v4, "accountSetupSkipped"

    invoke-interface {v3, v4, v2}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    .line 1752
    .local v0, "accountSetupSkipped":Z
    const-string v3, "noBack"

    if-nez v0, :cond_0

    :goto_0
    invoke-virtual {v1, v3, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 1753
    const/16 v2, 0x2715

    invoke-virtual {p0, v1, v2}, Lcom/google/android/setupwizard/BaseActivity;->startFirstRunActivityForResult(Landroid/content/Intent;I)V

    .line 1754
    return-void

    .line 1752
    :cond_0
    const/4 v2, 0x0

    goto :goto_0
.end method

.method private startNoAccountTosActivity()V
    .locals 3

    .prologue
    .line 1757
    iget-object v1, p0, Lcom/google/android/setupwizard/BaseActivity;->TAG:Ljava/lang/String;

    const-string v2, "startNoAccountTosActivity()"

    invoke-static {v1, v2}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 1758
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/google/android/setupwizard/user/NoAccountTosActivity;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 1759
    .local v0, "intent":Landroid/content/Intent;
    const/16 v1, 0x271d

    invoke-virtual {p0, v0, v1}, Lcom/google/android/setupwizard/BaseActivity;->startFirstRunActivityForResult(Landroid/content/Intent;I)V

    .line 1760
    return-void
.end method

.method private startSetupCompleteActivity()V
    .locals 3

    .prologue
    .line 1763
    iget-object v1, p0, Lcom/google/android/setupwizard/BaseActivity;->TAG:Ljava/lang/String;

    const-string v2, "startSetupCompleteActivity()"

    invoke-static {v1, v2}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 1764
    invoke-virtual {p0}, Lcom/google/android/setupwizard/BaseActivity;->isNexus()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 1765
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/google/android/setupwizard/user/SetupCompleteActivity;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 1766
    .local v0, "intent":Landroid/content/Intent;
    const/16 v1, 0x271c

    invoke-virtual {p0, v0, v1}, Lcom/google/android/setupwizard/BaseActivity;->startFirstRunActivityForResult(Landroid/content/Intent;I)V

    .line 1770
    .end local v0    # "intent":Landroid/content/Intent;
    :goto_0
    return-void

    .line 1768
    :cond_0
    const/4 v1, 0x1

    invoke-virtual {p0, v1}, Lcom/google/android/setupwizard/BaseActivity;->onSetupCompleteCompleted(Z)V

    goto :goto_0
.end method

.method private startSetupWizardExitActivity()V
    .locals 3

    .prologue
    .line 1773
    iget-object v1, p0, Lcom/google/android/setupwizard/BaseActivity;->TAG:Ljava/lang/String;

    const-string v2, "startSetupWizardExitActivity()"

    invoke-static {v1, v2}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 1774
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/google/android/setupwizard/SetupWizardExitActivity;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 1775
    .local v0, "intent":Landroid/content/Intent;
    invoke-virtual {p0, v0}, Lcom/google/android/setupwizard/BaseActivity;->startFirstRunActivity(Landroid/content/Intent;)V

    .line 1776
    return-void
.end method


# virtual methods
.method protected applyBackwardTransition(I)V
    .locals 2
    .param p1, "transitionId"    # I

    .prologue
    const/4 v1, 0x0

    .line 1280
    const/4 v0, 0x1

    if-ne p1, v0, :cond_1

    .line 1281
    const v0, 0x7f040002

    const v1, 0x7f040003

    invoke-virtual {p0, v0, v1}, Lcom/google/android/setupwizard/BaseActivity;->overridePendingTransition(II)V

    .line 1289
    :cond_0
    :goto_0
    return-void

    .line 1282
    :cond_1
    const/4 v0, 0x2

    if-ne p1, v0, :cond_2

    .line 1283
    const/high16 v0, 0x10a0000

    const v1, 0x10a0001

    invoke-virtual {p0, v0, v1}, Lcom/google/android/setupwizard/BaseActivity;->overridePendingTransition(II)V

    goto :goto_0

    .line 1284
    :cond_2
    const/4 v0, -0x1

    if-ne p1, v0, :cond_0

    .line 1286
    invoke-virtual {p0, v1, v1}, Lcom/google/android/setupwizard/BaseActivity;->overridePendingTransition(II)V

    goto :goto_0
.end method

.method protected applyForwardTransition(I)V
    .locals 2
    .param p1, "transitionId"    # I

    .prologue
    const/4 v1, 0x0

    .line 1260
    const/4 v0, 0x1

    if-ne p1, v0, :cond_1

    .line 1261
    const v0, 0x7f040004

    const v1, 0x7f040005

    invoke-virtual {p0, v0, v1}, Lcom/google/android/setupwizard/BaseActivity;->overridePendingTransition(II)V

    .line 1269
    :cond_0
    :goto_0
    return-void

    .line 1262
    :cond_1
    const/4 v0, 0x2

    if-ne p1, v0, :cond_2

    .line 1263
    const/high16 v0, 0x10a0000

    const v1, 0x10a0001

    invoke-virtual {p0, v0, v1}, Lcom/google/android/setupwizard/BaseActivity;->overridePendingTransition(II)V

    goto :goto_0

    .line 1264
    :cond_2
    const/4 v0, -0x1

    if-ne p1, v0, :cond_0

    .line 1266
    invoke-virtual {p0, v1, v1}, Lcom/google/android/setupwizard/BaseActivity;->overridePendingTransition(II)V

    goto :goto_0
.end method

.method protected carrierSetupLaunched()Z
    .locals 4

    .prologue
    const/4 v2, 0x0

    .line 1834
    invoke-virtual {p0}, Lcom/google/android/setupwizard/BaseActivity;->getLocalPreferences()Landroid/content/SharedPreferences;

    move-result-object v1

    .line 1835
    .local v1, "pref":Landroid/content/SharedPreferences;
    const-string v3, "carrierSetupResult"

    invoke-interface {v1, v3, v2}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v0

    .line 1836
    .local v0, "carrierSetupResult":I
    const-string v3, "carrierSetupResult"

    invoke-interface {v1, v3}, Landroid/content/SharedPreferences;->contains(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_1

    const/4 v3, -0x1

    if-eq v0, v3, :cond_0

    const/4 v3, 0x2

    if-eq v0, v3, :cond_0

    const/4 v3, 0x3

    if-ne v0, v3, :cond_1

    :cond_0
    const/4 v2, 0x1

    :cond_1
    return v2
.end method

.method protected carrierSetupSuccessfullyRan()Z
    .locals 4

    .prologue
    const/4 v2, 0x0

    .line 1848
    invoke-virtual {p0}, Lcom/google/android/setupwizard/BaseActivity;->getLocalPreferences()Landroid/content/SharedPreferences;

    move-result-object v1

    .line 1849
    .local v1, "pref":Landroid/content/SharedPreferences;
    const-string v3, "carrierSetupResult"

    invoke-interface {v1, v3, v2}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v0

    .line 1850
    .local v0, "carrierSetupResult":I
    const-string v3, "carrierSetupResult"

    invoke-interface {v1, v3}, Landroid/content/SharedPreferences;->contains(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_0

    const/4 v3, -0x1

    if-ne v0, v3, :cond_0

    const/4 v2, 0x1

    :cond_0
    return v2
.end method

.method public dispatchKeyEvent(Landroid/view/KeyEvent;)Z
    .locals 8
    .param p1, "ev"    # Landroid/view/KeyEvent;

    .prologue
    const/4 v5, 0x0

    const/4 v4, 0x1

    .line 586
    invoke-virtual {p1}, Landroid/view/KeyEvent;->getKeyCode()I

    move-result v6

    const/4 v7, 0x5

    if-ne v6, v7, :cond_2

    move v1, v4

    .line 587
    .local v1, "isCallKey":Z
    :goto_0
    invoke-virtual {p1}, Landroid/view/KeyEvent;->getKeyCode()I

    move-result v6

    const/16 v7, 0x18

    if-ne v6, v7, :cond_3

    move v3, v4

    .line 588
    .local v3, "isVolUpKey":Z
    :goto_1
    invoke-virtual {p1}, Landroid/view/KeyEvent;->getKeyCode()I

    move-result v6

    const/16 v7, 0x19

    if-ne v6, v7, :cond_4

    move v2, v4

    .line 589
    .local v2, "isVolDownKey":Z
    :goto_2
    invoke-virtual {p1}, Landroid/view/KeyEvent;->getKeyCode()I

    move-result v6

    const/4 v7, 0x4

    if-ne v6, v7, :cond_5

    move v0, v4

    .line 590
    .local v0, "isBackKey":Z
    :goto_3
    invoke-virtual {p1}, Landroid/view/KeyEvent;->isSystem()Z

    move-result v5

    if-eqz v5, :cond_0

    if-nez v3, :cond_0

    if-nez v2, :cond_0

    if-eqz v0, :cond_6

    invoke-virtual {p0}, Lcom/google/android/setupwizard/BaseActivity;->isBackAllowed()Z

    move-result v5

    if-eqz v5, :cond_6

    .line 592
    :cond_0
    invoke-super {p0, p1}, Landroid/app/Activity;->dispatchKeyEvent(Landroid/view/KeyEvent;)Z

    move-result v4

    .line 602
    :cond_1
    :goto_4
    return v4

    .end local v0    # "isBackKey":Z
    .end local v1    # "isCallKey":Z
    .end local v2    # "isVolDownKey":Z
    .end local v3    # "isVolUpKey":Z
    :cond_2
    move v1, v5

    .line 586
    goto :goto_0

    .restart local v1    # "isCallKey":Z
    :cond_3
    move v3, v5

    .line 587
    goto :goto_1

    .restart local v3    # "isVolUpKey":Z
    :cond_4
    move v2, v5

    .line 588
    goto :goto_2

    .restart local v2    # "isVolDownKey":Z
    :cond_5
    move v0, v5

    .line 589
    goto :goto_3

    .line 593
    .restart local v0    # "isBackKey":Z
    :cond_6
    if-eqz v1, :cond_1

    .line 596
    invoke-virtual {p1}, Landroid/view/KeyEvent;->getAction()I

    move-result v5

    if-nez v5, :cond_1

    invoke-virtual {p1}, Landroid/view/KeyEvent;->getRepeatCount()I

    move-result v5

    if-nez v5, :cond_1

    .line 597
    invoke-virtual {p0}, Lcom/google/android/setupwizard/BaseActivity;->placeEmergencyCall()V

    goto :goto_4
.end method

.method public dispatchTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 1
    .param p1, "ev"    # Landroid/view/MotionEvent;

    .prologue
    .line 573
    iget-object v0, p0, Lcom/google/android/setupwizard/BaseActivity;->mFourCornersDetector:Lcom/google/android/setupwizard/util/FourCornersGestureDetector;

    if-eqz v0, :cond_0

    .line 574
    iget-object v0, p0, Lcom/google/android/setupwizard/BaseActivity;->mFourCornersDetector:Lcom/google/android/setupwizard/util/FourCornersGestureDetector;

    invoke-virtual {v0, p1}, Lcom/google/android/setupwizard/util/FourCornersGestureDetector;->onTouchEvent(Landroid/view/MotionEvent;)V

    .line 576
    :cond_0
    invoke-super {p0, p1}, Landroid/app/Activity;->dispatchTouchEvent(Landroid/view/MotionEvent;)Z

    move-result v0

    return v0
.end method

.method protected enableComponent(Landroid/content/ComponentName;)V
    .locals 3
    .param p1, "componentName"    # Landroid/content/ComponentName;

    .prologue
    .line 2529
    invoke-virtual {p0}, Lcom/google/android/setupwizard/BaseActivity;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v0

    .line 2530
    .local v0, "pm":Landroid/content/pm/PackageManager;
    const/4 v1, 0x0

    const/4 v2, 0x1

    invoke-virtual {v0, p1, v1, v2}, Landroid/content/pm/PackageManager;->setComponentEnabledSetting(Landroid/content/ComponentName;II)V

    .line 2533
    return-void
.end method

.method protected enableComponent(Ljava/lang/String;)V
    .locals 1
    .param p1, "name"    # Ljava/lang/String;

    .prologue
    .line 2536
    new-instance v0, Landroid/content/ComponentName;

    invoke-direct {v0, p0, p1}, Landroid/content/ComponentName;-><init>(Landroid/content/Context;Ljava/lang/String;)V

    invoke-virtual {p0, v0}, Lcom/google/android/setupwizard/BaseActivity;->enableComponent(Landroid/content/ComponentName;)V

    .line 2537
    return-void
.end method

.method protected enableComponentSets(I)V
    .locals 3
    .param p1, "flags"    # I

    .prologue
    .line 2501
    :try_start_0
    invoke-virtual {p0}, Lcom/google/android/setupwizard/BaseActivity;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v1

    invoke-virtual {p0}, Lcom/google/android/setupwizard/BaseActivity;->getPackageName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2, p1}, Landroid/content/pm/PackageManager;->getPackageInfo(Ljava/lang/String;I)Landroid/content/pm/PackageInfo;

    move-result-object v0

    .line 2502
    .local v0, "allInfo":Landroid/content/pm/PackageInfo;
    and-int/lit8 v1, p1, 0x1

    if-eqz v1, :cond_0

    .line 2503
    iget-object v1, v0, Landroid/content/pm/PackageInfo;->activities:[Landroid/content/pm/ActivityInfo;

    invoke-direct {p0, v1}, Lcom/google/android/setupwizard/BaseActivity;->enableComponentArray([Landroid/content/pm/ComponentInfo;)V

    .line 2505
    :cond_0
    and-int/lit8 v1, p1, 0x2

    if-eqz v1, :cond_1

    .line 2506
    iget-object v1, v0, Landroid/content/pm/PackageInfo;->receivers:[Landroid/content/pm/ActivityInfo;

    invoke-direct {p0, v1}, Lcom/google/android/setupwizard/BaseActivity;->enableComponentArray([Landroid/content/pm/ComponentInfo;)V

    .line 2508
    :cond_1
    and-int/lit8 v1, p1, 0x4

    if-eqz v1, :cond_2

    .line 2509
    iget-object v1, v0, Landroid/content/pm/PackageInfo;->services:[Landroid/content/pm/ServiceInfo;

    invoke-direct {p0, v1}, Lcom/google/android/setupwizard/BaseActivity;->enableComponentArray([Landroid/content/pm/ComponentInfo;)V
    :try_end_0
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    .line 2513
    .end local v0    # "allInfo":Landroid/content/pm/PackageInfo;
    :cond_2
    :goto_0
    return-void

    .line 2511
    :catch_0
    move-exception v1

    goto :goto_0
.end method

.method public finish()V
    .locals 2

    .prologue
    .line 1155
    iget-object v0, p0, Lcom/google/android/setupwizard/BaseActivity;->TAG:Ljava/lang/String;

    const-string v1, "BaseActivity.finish"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1156
    invoke-super {p0}, Landroid/app/Activity;->finish()V

    .line 1157
    iget v0, p0, Lcom/google/android/setupwizard/BaseActivity;->mResultCode:I

    if-nez v0, :cond_0

    .line 1160
    invoke-virtual {p0}, Lcom/google/android/setupwizard/BaseActivity;->getPreviousTransition()I

    move-result v0

    invoke-virtual {p0, v0}, Lcom/google/android/setupwizard/BaseActivity;->applyBackwardTransition(I)V

    .line 1162
    :cond_0
    return-void
.end method

.method protected finishAction()V
    .locals 1

    .prologue
    .line 1165
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/google/android/setupwizard/BaseActivity;->finishAction(I)V

    .line 1166
    return-void
.end method

.method protected finishAction(I)V
    .locals 1
    .param p1, "resultCode"    # I

    .prologue
    .line 1169
    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, Lcom/google/android/setupwizard/BaseActivity;->finishAction(ILandroid/content/Intent;)V

    .line 1170
    return-void
.end method

.method protected finishAction(ILandroid/content/Intent;)V
    .locals 3
    .param p1, "resultCode"    # I
    .param p2, "data"    # Landroid/content/Intent;

    .prologue
    .line 1173
    iget-object v0, p0, Lcom/google/android/setupwizard/BaseActivity;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "BaseActivity.finishAction resultCode="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " data="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 1175
    invoke-virtual {p0, p1, p2}, Lcom/google/android/setupwizard/BaseActivity;->setResultCode(ILandroid/content/Intent;)V

    .line 1176
    invoke-virtual {p0}, Lcom/google/android/setupwizard/BaseActivity;->finish()V

    .line 1177
    invoke-virtual {p0}, Lcom/google/android/setupwizard/BaseActivity;->isUsingWizardManager()Z

    move-result v0

    if-eqz v0, :cond_0

    if-eqz p1, :cond_0

    .line 1178
    invoke-virtual {p0}, Lcom/google/android/setupwizard/BaseActivity;->sendActionResults()V

    .line 1180
    :cond_0
    return-void
.end method

.method protected getAccount()Landroid/accounts/Account;
    .locals 3

    .prologue
    .line 2095
    invoke-virtual {p0}, Lcom/google/android/setupwizard/BaseActivity;->getAccounts()[Landroid/accounts/Account;

    move-result-object v0

    .line 2096
    .local v0, "accounts":[Landroid/accounts/Account;
    array-length v1, v0

    const/4 v2, 0x1

    if-lt v1, v2, :cond_0

    .line 2097
    const/4 v1, 0x0

    aget-object v1, v0, v1

    .line 2099
    :goto_0
    return-object v1

    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method protected getAccount(Ljava/lang/String;)Landroid/accounts/Account;
    .locals 6
    .param p1, "name"    # Ljava/lang/String;

    .prologue
    .line 2103
    invoke-virtual {p0}, Lcom/google/android/setupwizard/BaseActivity;->getAccounts()[Landroid/accounts/Account;

    move-result-object v1

    .line 2104
    .local v1, "accounts":[Landroid/accounts/Account;
    move-object v2, v1

    .local v2, "arr$":[Landroid/accounts/Account;
    array-length v4, v2

    .local v4, "len$":I
    const/4 v3, 0x0

    .local v3, "i$":I
    :goto_0
    if-ge v3, v4, :cond_1

    aget-object v0, v2, v3

    .line 2105
    .local v0, "account":Landroid/accounts/Account;
    iget-object v5, v0, Landroid/accounts/Account;->name:Ljava/lang/String;

    invoke-virtual {v5, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_0

    .line 2109
    .end local v0    # "account":Landroid/accounts/Account;
    :goto_1
    return-object v0

    .line 2104
    .restart local v0    # "account":Landroid/accounts/Account;
    :cond_0
    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    .line 2109
    .end local v0    # "account":Landroid/accounts/Account;
    :cond_1
    const/4 v0, 0x0

    goto :goto_1
.end method

.method protected getAccountDomains()[Ljava/lang/String;
    .locals 5

    .prologue
    const/4 v2, 0x0

    .line 2209
    const-string v3, "SetupWizardPrefs"

    const/4 v4, 0x4

    invoke-virtual {p0, v3, v4}, Lcom/google/android/setupwizard/BaseActivity;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v1

    .line 2211
    .local v1, "prefs":Landroid/content/SharedPreferences;
    const-string v3, "setupwizard.account_domains"

    invoke-interface {v1, v3, v2}, Landroid/content/SharedPreferences;->getStringSet(Ljava/lang/String;Ljava/util/Set;)Ljava/util/Set;

    move-result-object v0

    .line 2212
    .local v0, "domains":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/String;>;"
    if-nez v0, :cond_0

    :goto_0
    return-object v2

    :cond_0
    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/String;

    invoke-interface {v0, v2}, Ljava/util/Set;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v2

    check-cast v2, [Ljava/lang/String;

    goto :goto_0
.end method

.method protected getAccounts()[Landroid/accounts/Account;
    .locals 2

    .prologue
    .line 2090
    invoke-static {p0}, Landroid/accounts/AccountManager;->get(Landroid/content/Context;)Landroid/accounts/AccountManager;

    move-result-object v0

    const-string v1, "com.google"

    invoke-virtual {v0, v1}, Landroid/accounts/AccountManager;->getAccountsByType(Ljava/lang/String;)[Landroid/accounts/Account;

    move-result-object v0

    return-object v0
.end method

.method public getDecorView()Landroid/view/View;
    .locals 1

    .prologue
    .line 872
    invoke-virtual {p0}, Lcom/google/android/setupwizard/BaseActivity;->getWindow()Landroid/view/Window;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/Window;->getDecorView()Landroid/view/View;

    move-result-object v0

    return-object v0
.end method

.method protected getLastActivity()I
    .locals 3

    .prologue
    .line 2140
    const-string v1, "SetupWizardPrefs"

    const/4 v2, 0x4

    invoke-virtual {p0, v1, v2}, Lcom/google/android/setupwizard/BaseActivity;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v0

    .line 2142
    .local v0, "prefs":Landroid/content/SharedPreferences;
    const-string v1, "lastActivity"

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v1

    return v1
.end method

.method protected getLocalPreferences()Landroid/content/SharedPreferences;
    .locals 2

    .prologue
    .line 2427
    const-string v0, "SetupWizardLocalPrefs"

    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1}, Lcom/google/android/setupwizard/BaseActivity;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v0

    return-object v0
.end method

.method protected getNetworkRequirement()Ljava/lang/String;
    .locals 1

    .prologue
    .line 2063
    invoke-virtual {p0}, Lcom/google/android/setupwizard/BaseActivity;->isPrimaryUser()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2064
    const-string v0, "ro.setupwizard.require_network"

    invoke-static {v0}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 2066
    :goto_0
    return-object v0

    :cond_0
    const-string v0, "ro.setupwizard.user_req_network"

    invoke-static {v0}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method protected getNextTransition()I
    .locals 1

    .prologue
    .line 1306
    const/4 v0, 0x0

    return v0
.end method

.method protected getPrefsString(Ljava/lang/String;)Ljava/lang/String;
    .locals 2
    .param p1, "key"    # Ljava/lang/String;

    .prologue
    .line 2431
    const-string v0, "SetupWizardPrefs"

    const/4 v1, 0x4

    invoke-virtual {p0, v0, v1}, Lcom/google/android/setupwizard/BaseActivity;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v0

    const-string v1, ""

    invoke-interface {v0, p1, v1}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method protected getPreviousTransition()I
    .locals 1

    .prologue
    .line 1297
    const/4 v0, 0x1

    return v0
.end method

.method protected getProvisioningMode()Lcom/google/android/setupwizard/BaseActivity$ProvisioningMode;
    .locals 1

    .prologue
    .line 1955
    iget-object v0, p0, Lcom/google/android/setupwizard/BaseActivity;->mProvisioningMode:Lcom/google/android/setupwizard/BaseActivity$ProvisioningMode;

    return-object v0
.end method

.method protected getRequireLockPin()Z
    .locals 3

    .prologue
    .line 2295
    const-string v1, "SetupWizardPrefs"

    const/4 v2, 0x4

    invoke-virtual {p0, v1, v2}, Lcom/google/android/setupwizard/BaseActivity;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v0

    .line 2297
    .local v0, "prefs":Landroid/content/SharedPreferences;
    const-string v1, "setupwizard.require_lock_pin"

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v1

    return v1
.end method

.method protected getResultData()Landroid/content/Intent;
    .locals 1

    .prologue
    .line 811
    const/4 v0, 0x0

    return-object v0
.end method

.method protected getShowLockPinScreen()Z
    .locals 3

    .prologue
    .line 2273
    const-string v1, "SetupWizardPrefs"

    const/4 v2, 0x4

    invoke-virtual {p0, v1, v2}, Lcom/google/android/setupwizard/BaseActivity;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v0

    .line 2275
    .local v0, "prefs":Landroid/content/SharedPreferences;
    const-string v1, "setupwizard.show_lock_pin_screen"

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v1

    return v1
.end method

.method protected getShowSelfieScreen()Z
    .locals 3

    .prologue
    .line 2316
    const-string v1, "SetupWizardPrefs"

    const/4 v2, 0x4

    invoke-virtual {p0, v1, v2}, Lcom/google/android/setupwizard/BaseActivity;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v0

    .line 2318
    .local v0, "prefs":Landroid/content/SharedPreferences;
    const-string v1, "setupwizard.show_selfie_screen"

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v1

    return v1
.end method

.method public getStyleForTheme(Ljava/lang/String;)I
    .locals 1
    .param p1, "themeName"    # Ljava/lang/String;

    .prologue
    .line 2335
    const-string v0, "holo_light"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2336
    const v0, 0x7f060023

    .line 2344
    :goto_0
    return v0

    .line 2337
    :cond_0
    const-string v0, "holo"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 2338
    const v0, 0x7f060022

    goto :goto_0

    .line 2339
    :cond_1
    const-string v0, "material_light"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 2340
    const v0, 0x7f060025

    goto :goto_0

    .line 2341
    :cond_2
    const-string v0, "material"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 2342
    const v0, 0x7f060024

    goto :goto_0

    .line 2344
    :cond_3
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected getTelephonyInterface()Lcom/android/internal/telephony/ITelephony;
    .locals 2

    .prologue
    .line 1943
    iget-object v1, p0, Lcom/google/android/setupwizard/BaseActivity;->mTelephonyInterface:Lcom/android/internal/telephony/ITelephony;

    if-nez v1, :cond_0

    .line 1944
    const-string v1, "phone"

    invoke-static {v1}, Landroid/os/ServiceManager;->checkService(Ljava/lang/String;)Landroid/os/IBinder;

    move-result-object v0

    .line 1945
    .local v0, "binder":Landroid/os/IBinder;
    invoke-static {v0}, Lcom/android/internal/telephony/ITelephony$Stub;->asInterface(Landroid/os/IBinder;)Lcom/android/internal/telephony/ITelephony;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/setupwizard/BaseActivity;->mTelephonyInterface:Lcom/android/internal/telephony/ITelephony;

    .line 1947
    .end local v0    # "binder":Landroid/os/IBinder;
    :cond_0
    iget-object v1, p0, Lcom/google/android/setupwizard/BaseActivity;->mTelephonyInterface:Lcom/android/internal/telephony/ITelephony;

    return-object v1
.end method

.method protected hasMfmAccount()Z
    .locals 8

    .prologue
    const-wide/16 v6, 0x0

    .line 2113
    invoke-virtual {p0}, Lcom/google/android/setupwizard/BaseActivity;->getLocalPreferences()Landroid/content/SharedPreferences;

    move-result-object v0

    .line 2114
    .local v0, "localPref":Landroid/content/SharedPreferences;
    const-string v4, "restoreAccount"

    const-string v5, ""

    invoke-interface {v0, v4, v5}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {p0, v4}, Lcom/google/android/setupwizard/BaseActivity;->getAccount(Ljava/lang/String;)Landroid/accounts/Account;

    move-result-object v1

    .line 2115
    .local v1, "mfmAccount":Landroid/accounts/Account;
    const-string v4, "restoreToken"

    invoke-interface {v0, v4, v6, v7}, Landroid/content/SharedPreferences;->getLong(Ljava/lang/String;J)J

    move-result-wide v2

    .line 2116
    .local v2, "mfmRestoreToken":J
    if-eqz v1, :cond_0

    cmp-long v4, v2, v6

    if-eqz v4, :cond_0

    const/4 v4, 0x1

    :goto_0
    return v4

    :cond_0
    const/4 v4, 0x0

    goto :goto_0
.end method

.method public hasMultipleUsers()Z
    .locals 2

    .prologue
    const/4 v1, 0x1

    .line 1991
    const-string v0, "user"

    invoke-virtual {p0, v0}, Lcom/google/android/setupwizard/BaseActivity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/os/UserManager;

    invoke-virtual {v0}, Landroid/os/UserManager;->getUsers()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-le v0, v1, :cond_0

    move v0, v1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected hasNoAccounts()Z
    .locals 2

    .prologue
    .line 2085
    invoke-static {p0}, Landroid/accounts/AccountManager;->get(Landroid/content/Context;)Landroid/accounts/AccountManager;

    move-result-object v0

    const-string v1, "com.google"

    invoke-virtual {v0, v1}, Landroid/accounts/AccountManager;->getAccountsByType(Ljava/lang/String;)[Landroid/accounts/Account;

    move-result-object v0

    array-length v0, v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected isBackAllowed()Z
    .locals 1

    .prologue
    .line 806
    iget-boolean v0, p0, Lcom/google/android/setupwizard/BaseActivity;->mBackAllowed:Z

    return v0
.end method

.method protected isEduSignin()Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 2219
    invoke-virtual {p0}, Lcom/google/android/setupwizard/BaseActivity;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    const-string v2, "com.google.android.nfcprovision_is_edu_device"

    invoke-static {v1, v2, v0}, Landroid/provider/Settings$Global;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v1

    if-eqz v1, :cond_0

    const/4 v0, 0x1

    :cond_0
    return v0
.end method

.method protected isFirstRun()Z
    .locals 1

    .prologue
    .line 1969
    iget-boolean v0, p0, Lcom/google/android/setupwizard/BaseActivity;->mIsFirstRun:Z

    return v0
.end method

.method protected isMobileNetworkSupported()Z
    .locals 4

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 2036
    const-string v3, "connectivity"

    invoke-virtual {p0, v3}, Lcom/google/android/setupwizard/BaseActivity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/net/ConnectivityManager;

    .line 2038
    .local v0, "cm":Landroid/net/ConnectivityManager;
    if-eqz v0, :cond_0

    invoke-virtual {v0, v2}, Landroid/net/ConnectivityManager;->isNetworkSupported(I)Z

    move-result v3

    if-ne v3, v1, :cond_0

    :goto_0
    return v1

    :cond_0
    move v1, v2

    goto :goto_0
.end method

.method protected isNetworkConnected()Z
    .locals 1

    .prologue
    .line 2081
    invoke-static {}, Lcom/google/android/setupwizard/network/NetworkMonitor;->getInstance()Lcom/google/android/setupwizard/network/NetworkMonitor;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/setupwizard/network/NetworkMonitor;->isNetworkConnected()Z

    move-result v0

    return v0
.end method

.method protected isNetworkRequired()Z
    .locals 5

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 2045
    invoke-virtual {p0}, Lcom/google/android/setupwizard/BaseActivity;->getNetworkRequirement()Ljava/lang/String;

    move-result-object v1

    .line 2046
    .local v1, "networkRequirement":Ljava/lang/String;
    const-string v4, "wifi"

    invoke-virtual {v4, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_0

    const-string v4, "any"

    invoke-virtual {v4, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_3

    :cond_0
    move v0, v3

    .line 2048
    .local v0, "networkRequired":Z
    :goto_0
    if-nez v0, :cond_1

    invoke-virtual {p0}, Lcom/google/android/setupwizard/BaseActivity;->isNetworkRequiredLegacy()Z

    move-result v4

    if-eqz v4, :cond_2

    :cond_1
    move v2, v3

    :cond_2
    return v2

    .end local v0    # "networkRequired":Z
    :cond_3
    move v0, v2

    .line 2046
    goto :goto_0
.end method

.method protected isNetworkRequiredLegacy()Z
    .locals 3
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    const/4 v0, 0x0

    .line 2053
    invoke-virtual {p0}, Lcom/google/android/setupwizard/BaseActivity;->isPrimaryUser()Z

    move-result v1

    if-eqz v1, :cond_0

    const-string v1, "ro.setupwizard.network_required"

    invoke-static {v1, v0}, Landroid/os/SystemProperties;->getBoolean(Ljava/lang/String;Z)Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v0, 0x1

    .line 2055
    .local v0, "required":Z
    :cond_0
    if-eqz v0, :cond_1

    .line 2056
    iget-object v1, p0, Lcom/google/android/setupwizard/BaseActivity;->TAG:Ljava/lang/String;

    const-string v2, "ro.setupwizard.network_required is deprecated. Please use ro.setupwizard.require_network = any to get the same results."

    invoke-static {v1, v2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 2059
    :cond_1
    return v0
.end method

.method protected isNexus()Z
    .locals 3

    .prologue
    .line 2010
    invoke-virtual {p0}, Lcom/google/android/setupwizard/BaseActivity;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v1

    const-string v2, "com.google.android.feature.GOOGLE_BUILD"

    invoke-virtual {v1, v2}, Landroid/content/pm/PackageManager;->hasSystemFeature(Ljava/lang/String;)Z

    move-result v0

    .line 2012
    .local v0, "isNexus":Z
    return v0
.end method

.method protected isPessimisticMode()Z
    .locals 1

    .prologue
    .line 2032
    iget-boolean v0, p0, Lcom/google/android/setupwizard/BaseActivity;->mIsPessimisticMode:Z

    return v0
.end method

.method protected isPrimaryUser()Z
    .locals 1

    .prologue
    .line 1987
    iget-boolean v0, p0, Lcom/google/android/setupwizard/BaseActivity;->mIsPrimaryUser:Z

    return v0
.end method

.method protected isResumeFromUpdate()Z
    .locals 1

    .prologue
    .line 1980
    iget-boolean v0, p0, Lcom/google/android/setupwizard/BaseActivity;->mIsResumeFromUpdate:Z

    return v0
.end method

.method protected isTimeSet()Z
    .locals 1

    .prologue
    .line 2185
    invoke-static {}, Lcom/google/android/setupwizard/time/DateTimeMonitor;->getInstance()Lcom/google/android/setupwizard/time/DateTimeMonitor;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/setupwizard/time/DateTimeMonitor;->isTimeSet()Z

    move-result v0

    return v0
.end method

.method protected isTimeZoneSet()Z
    .locals 1

    .prologue
    .line 2189
    invoke-static {}, Lcom/google/android/setupwizard/time/DateTimeMonitor;->getInstance()Lcom/google/android/setupwizard/time/DateTimeMonitor;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/setupwizard/time/DateTimeMonitor;->isTimeZoneSet()Z

    move-result v0

    return v0
.end method

.method protected isUsingWizardManager()Z
    .locals 2

    .prologue
    .line 1228
    invoke-virtual {p0}, Lcom/google/android/setupwizard/BaseActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string v1, "scriptUri"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->hasExtra(Ljava/lang/String;)Z

    move-result v0

    return v0
.end method

.method protected isWifiRequired()Z
    .locals 2

    .prologue
    .line 2074
    const-string v0, "wifi"

    invoke-virtual {p0}, Lcom/google/android/setupwizard/BaseActivity;->getNetworkRequirement()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method protected lockRotation()V
    .locals 7

    .prologue
    const/4 v6, 0x0

    .line 2146
    iget-object v3, p0, Lcom/google/android/setupwizard/BaseActivity;->TAG:Ljava/lang/String;

    const-string v4, "Locking Rotation"

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 2151
    const-string v3, "SetupWizardPrefs"

    const/4 v4, 0x4

    invoke-virtual {p0, v3, v4}, Lcom/google/android/setupwizard/BaseActivity;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v1

    .line 2153
    .local v1, "prefs":Landroid/content/SharedPreferences;
    invoke-virtual {p0}, Lcom/google/android/setupwizard/BaseActivity;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v3

    const-string v4, "accelerometer_rotation"

    const/4 v5, 0x1

    invoke-static {v3, v4, v5}, Landroid/provider/Settings$System;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v0

    .line 2155
    .local v0, "accelerometerRotation":I
    invoke-virtual {p0}, Lcom/google/android/setupwizard/BaseActivity;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v3

    const-string v4, "user_rotation"

    invoke-static {v3, v4, v6}, Landroid/provider/Settings$System;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v2

    .line 2158
    .local v2, "userRotation":I
    invoke-interface {v1}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v3

    const-string v4, "setupwizard.accelerometer_rotation"

    invoke-interface {v3, v4, v0}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    move-result-object v3

    const-string v4, "setupwizard.user_rotation"

    invoke-interface {v3, v4, v2}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    move-result-object v3

    invoke-interface {v3}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 2163
    invoke-virtual {p0}, Lcom/google/android/setupwizard/BaseActivity;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v3

    const-string v4, "accelerometer_rotation"

    invoke-static {v3, v4, v6}, Landroid/provider/Settings$System;->putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z

    .line 2165
    invoke-virtual {p0}, Lcom/google/android/setupwizard/BaseActivity;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v3

    const-string v4, "user_rotation"

    invoke-static {v3, v4, v6}, Landroid/provider/Settings$System;->putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z

    .line 2167
    return-void
.end method

.method protected nextAction(I)Z
    .locals 1
    .param p1, "resultCode"    # I

    .prologue
    .line 1201
    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, Lcom/google/android/setupwizard/BaseActivity;->nextAction(ILandroid/content/Intent;)Z

    move-result v0

    return v0
.end method

.method protected nextAction(ILandroid/content/Intent;)Z
    .locals 3
    .param p1, "resultCode"    # I
    .param p2, "data"    # Landroid/content/Intent;

    .prologue
    .line 1205
    iget-object v0, p0, Lcom/google/android/setupwizard/BaseActivity;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "nextAction("

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ") this="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 1207
    invoke-virtual {p0, p1, p2}, Lcom/google/android/setupwizard/BaseActivity;->setResultCode(ILandroid/content/Intent;)V

    .line 1209
    invoke-virtual {p0}, Lcom/google/android/setupwizard/BaseActivity;->isUsingWizardManager()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1210
    if-nez p1, :cond_0

    .line 1211
    invoke-virtual {p0}, Lcom/google/android/setupwizard/BaseActivity;->finish()V

    .line 1216
    :goto_0
    const/4 v0, 0x1

    .line 1218
    :goto_1
    return v0

    .line 1213
    :cond_0
    invoke-virtual {p0}, Lcom/google/android/setupwizard/BaseActivity;->sendActionResults()V

    .line 1214
    invoke-virtual {p0}, Lcom/google/android/setupwizard/BaseActivity;->getNextTransition()I

    move-result v0

    invoke-virtual {p0, v0}, Lcom/google/android/setupwizard/BaseActivity;->applyForwardTransition(I)V

    goto :goto_0

    .line 1218
    :cond_1
    const/4 v0, 0x0

    goto :goto_1
.end method

.method protected nextScreen()V
    .locals 1

    .prologue
    .line 1310
    const/4 v0, -0x1

    invoke-virtual {p0, v0}, Lcom/google/android/setupwizard/BaseActivity;->nextScreen(I)V

    .line 1311
    return-void
.end method

.method protected nextScreen(I)V
    .locals 1
    .param p1, "resultCode"    # I

    .prologue
    .line 1314
    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, Lcom/google/android/setupwizard/BaseActivity;->nextScreen(ILandroid/content/Intent;)V

    .line 1315
    return-void
.end method

.method protected nextScreen(ILandroid/content/Intent;)V
    .locals 6
    .param p1, "resultCode"    # I
    .param p2, "data"    # Landroid/content/Intent;

    .prologue
    const/4 v1, 0x0

    const/4 v5, -0x1

    const/4 v0, 0x1

    .line 1318
    iget-object v2, p0, Lcom/google/android/setupwizard/BaseActivity;->TAG:Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "nextScreen resultCode="

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " data="

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " this="

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 1320
    invoke-virtual {p0, p1, p2}, Lcom/google/android/setupwizard/BaseActivity;->nextAction(ILandroid/content/Intent;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 1355
    :goto_0
    return-void

    .line 1324
    :cond_0
    const-class v2, Lcom/google/android/setupwizard/SetupWizardActivity;

    invoke-virtual {v2, p0}, Ljava/lang/Class;->isInstance(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 1325
    invoke-virtual {p0}, Lcom/google/android/setupwizard/BaseActivity;->startWelcomeActivity()V

    goto :goto_0

    .line 1326
    :cond_1
    const-class v2, Lcom/google/android/setupwizard/user/WelcomeWrapper;

    invoke-virtual {v2, p0}, Ljava/lang/Class;->isInstance(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 1327
    invoke-virtual {p0}, Lcom/google/android/setupwizard/BaseActivity;->onWelcomeScreenCompleted()V

    goto :goto_0

    .line 1328
    :cond_2
    const-class v2, Lcom/google/android/setupwizard/user/WelcomeUserWrapper;

    invoke-virtual {v2, p0}, Ljava/lang/Class;->isInstance(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    .line 1329
    invoke-virtual {p0}, Lcom/google/android/setupwizard/BaseActivity;->onWelcomeUserScreenCompleted()V

    goto :goto_0

    .line 1330
    :cond_3
    const-class v2, Lcom/google/android/setupwizard/carrier/SimMissingActivity;

    invoke-virtual {v2, p0}, Ljava/lang/Class;->isInstance(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_4

    .line 1331
    invoke-direct {p0, p1}, Lcom/google/android/setupwizard/BaseActivity;->onSimMissingActivityCompleted(I)V

    goto :goto_0

    .line 1332
    :cond_4
    const-class v2, Lcom/google/android/setupwizard/network/WifiSettingsWrapper;

    invoke-virtual {v2, p0}, Ljava/lang/Class;->isInstance(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_5

    .line 1333
    invoke-virtual {p0, p1}, Lcom/google/android/setupwizard/BaseActivity;->onWifiSetupCompleted(I)V

    goto :goto_0

    .line 1334
    :cond_5
    const-class v2, Lcom/google/android/setupwizard/carrier/ActivationActivity;

    invoke-virtual {v2, p0}, Ljava/lang/Class;->isInstance(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_6

    .line 1335
    invoke-virtual {p0, p1}, Lcom/google/android/setupwizard/BaseActivity;->onActivationActivityCompleted(I)V

    goto :goto_0

    .line 1336
    :cond_6
    const-class v2, Lcom/google/android/setupwizard/network/WalledGardenActivity;

    invoke-virtual {v2, p0}, Ljava/lang/Class;->isInstance(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_7

    .line 1337
    invoke-virtual {p0, p1}, Lcom/google/android/setupwizard/BaseActivity;->onWalledGardenCompleted(I)V

    goto :goto_0

    .line 1338
    :cond_7
    const-class v2, Lcom/google/android/setupwizard/update/OtaUpdateActivity;

    invoke-virtual {v2, p0}, Ljava/lang/Class;->isInstance(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_8

    .line 1339
    invoke-virtual {p0, p1}, Lcom/google/android/setupwizard/BaseActivity;->onOtaUpdateCompleted(I)V

    goto :goto_0

    .line 1340
    :cond_8
    const-class v2, Lcom/google/android/setupwizard/account/AccountSetupWrapper;

    invoke-virtual {v2, p0}, Ljava/lang/Class;->isInstance(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_9

    .line 1341
    invoke-direct {p0, v0, p2}, Lcom/google/android/setupwizard/BaseActivity;->onAccountSetupCompleted(ZLandroid/content/Intent;)V

    goto :goto_0

    .line 1342
    :cond_9
    const-class v2, Lcom/google/android/setupwizard/user/LocationSharingActivity;

    invoke-virtual {v2, p0}, Ljava/lang/Class;->isInstance(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_b

    .line 1343
    if-ne p1, v5, :cond_a

    :goto_1
    invoke-virtual {p0, v0}, Lcom/google/android/setupwizard/BaseActivity;->onLocationSharingActivityCompleted(Z)V

    goto :goto_0

    :cond_a
    move v0, v1

    goto :goto_1

    .line 1344
    :cond_b
    const-class v2, Lcom/google/android/setupwizard/time/DateTimeSetupActivity;

    invoke-virtual {v2, p0}, Ljava/lang/Class;->isInstance(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_d

    .line 1345
    if-ne p1, v5, :cond_c

    :goto_2
    invoke-direct {p0, v0}, Lcom/google/android/setupwizard/BaseActivity;->onDateTimeSetupCompleted(Z)V

    goto/16 :goto_0

    :cond_c
    move v0, v1

    goto :goto_2

    .line 1346
    :cond_d
    const-class v2, Lcom/google/android/setupwizard/user/NoAccountTosActivity;

    invoke-virtual {v2, p0}, Ljava/lang/Class;->isInstance(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_f

    .line 1347
    if-ne p1, v5, :cond_e

    :goto_3
    invoke-direct {p0, v0}, Lcom/google/android/setupwizard/BaseActivity;->onNoAccountTosCompleted(Z)V

    goto/16 :goto_0

    :cond_e
    move v0, v1

    goto :goto_3

    .line 1348
    :cond_f
    const-class v2, Lcom/google/android/setupwizard/account/GoogleNowWrapper;

    invoke-virtual {v2, p0}, Ljava/lang/Class;->isInstance(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_10

    .line 1349
    invoke-direct {p0, p1}, Lcom/google/android/setupwizard/BaseActivity;->onGoogleNowCompleted(I)V

    goto/16 :goto_0

    .line 1350
    :cond_10
    const-class v2, Lcom/google/android/setupwizard/user/SetupCompleteActivity;

    invoke-virtual {v2, p0}, Ljava/lang/Class;->isInstance(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_12

    .line 1351
    if-ne p1, v5, :cond_11

    :goto_4
    invoke-virtual {p0, v0}, Lcom/google/android/setupwizard/BaseActivity;->onSetupCompleteCompleted(Z)V

    goto/16 :goto_0

    :cond_11
    move v0, v1

    goto :goto_4

    .line 1353
    :cond_12
    iget-object v0, p0, Lcom/google/android/setupwizard/BaseActivity;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "nextScreen received unknown activity "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0
.end method

.method protected onActivationActivityCompleted(I)V
    .locals 3
    .param p1, "resultCode"    # I

    .prologue
    .line 1435
    iget-object v0, p0, Lcom/google/android/setupwizard/BaseActivity;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "onActivationActivityCompleted resultCode="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const/16 v2, 0x2712

    invoke-static {v2, p1}, Lcom/google/android/setupwizard/BaseActivity;->getResultName(II)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 1442
    const/4 v0, -0x1

    if-ne p1, v0, :cond_0

    invoke-static {}, Lcom/google/android/setupwizard/carrier/PhoneMonitor;->getMobileDataTimeout()I

    move-result v0

    if-eqz v0, :cond_0

    .line 1443
    invoke-direct {p0}, Lcom/google/android/setupwizard/BaseActivity;->launchMobileDataActivity()V

    .line 1447
    :goto_0
    return-void

    .line 1445
    :cond_0
    invoke-direct {p0}, Lcom/google/android/setupwizard/BaseActivity;->launchCarrierSetupOrSkip()V

    goto :goto_0
.end method

.method protected onActivityResult(IILandroid/content/Intent;)V
    .locals 6
    .param p1, "requestCode"    # I
    .param p2, "resultCode"    # I
    .param p3, "data"    # Landroid/content/Intent;

    .prologue
    const/4 v5, -0x1

    const/4 v1, 0x0

    const/4 v0, 0x1

    .line 1027
    iget-object v2, p0, Lcom/google/android/setupwizard/BaseActivity;->TAG:Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "BaseActivity.onActivityResult("

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-static {p1}, Lcom/google/android/setupwizard/BaseActivity;->getRequestName(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ", "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-static {p1, p2}, Lcom/google/android/setupwizard/BaseActivity;->getResultName(II)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ") lastActivity="

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {p0}, Lcom/google/android/setupwizard/BaseActivity;->getLastActivity()I

    move-result v4

    invoke-static {v4}, Lcom/google/android/setupwizard/BaseActivity;->getRequestName(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 1031
    invoke-virtual {p0}, Lcom/google/android/setupwizard/BaseActivity;->getNextTransition()I

    move-result v2

    invoke-virtual {p0, v2}, Lcom/google/android/setupwizard/BaseActivity;->applyBackwardTransition(I)V

    .line 1032
    const/16 v2, 0x2710

    if-ne p1, v2, :cond_1

    if-nez p2, :cond_1

    .line 1126
    :cond_0
    :goto_0
    return-void

    .line 1036
    :cond_1
    invoke-virtual {p0, p2}, Lcom/google/android/setupwizard/BaseActivity;->nextAction(I)Z

    move-result v2

    if-nez v2, :cond_0

    .line 1040
    if-nez p2, :cond_2

    .line 1041
    iget-object v0, p0, Lcom/google/android/setupwizard/BaseActivity;->TAG:Ljava/lang/String;

    const-string v1, "  RESULT_CANCELED, going back"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1042
    invoke-super {p0, p1, p2, p3}, Landroid/app/Activity;->onActivityResult(IILandroid/content/Intent;)V

    .line 1043
    invoke-virtual {p0, p2}, Lcom/google/android/setupwizard/BaseActivity;->setResultCode(I)V

    goto :goto_0

    .line 1047
    :cond_2
    packed-switch p1, :pswitch_data_0

    .line 1120
    :pswitch_0
    iget-object v0, p0, Lcom/google/android/setupwizard/BaseActivity;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "onActivityResult() is called with unsupported requestCode. requestCode: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-static {p1}, Lcom/google/android/setupwizard/BaseActivity;->getRequestName(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " resultCode: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " data: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    new-instance v2, Ljava/lang/RuntimeException;

    invoke-direct {v2}, Ljava/lang/RuntimeException;-><init>()V

    invoke-static {v0, v1, v2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_0

    .line 1049
    :pswitch_1
    invoke-virtual {p0}, Lcom/google/android/setupwizard/BaseActivity;->onWelcomeScreenCompleted()V

    goto :goto_0

    .line 1052
    :pswitch_2
    invoke-virtual {p0}, Lcom/google/android/setupwizard/BaseActivity;->onWelcomeUserScreenCompleted()V

    goto :goto_0

    .line 1055
    :pswitch_3
    invoke-virtual {p0, p2}, Lcom/google/android/setupwizard/BaseActivity;->onOldActivationActivityCompleted(I)V

    goto :goto_0

    .line 1058
    :pswitch_4
    invoke-virtual {p0, p2}, Lcom/google/android/setupwizard/BaseActivity;->onCdmaActivationActivityCompleted(I)V

    goto :goto_0

    .line 1061
    :pswitch_5
    invoke-virtual {p0, p2}, Lcom/google/android/setupwizard/BaseActivity;->onActivationActivityCompleted(I)V

    goto :goto_0

    .line 1064
    :pswitch_6
    invoke-virtual {p0, p2}, Lcom/google/android/setupwizard/BaseActivity;->onMobileDataActivityCompleted(I)V

    goto :goto_0

    .line 1070
    :pswitch_7
    invoke-virtual {p0}, Lcom/google/android/setupwizard/BaseActivity;->getLocalPreferences()Landroid/content/SharedPreferences;

    move-result-object v1

    invoke-interface {v1}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v1

    const-string v2, "wifiScreenShown"

    invoke-interface {v1, v2, v0}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->apply()V

    .line 1071
    invoke-virtual {p0, p2}, Lcom/google/android/setupwizard/BaseActivity;->onWifiSetupCompleted(I)V

    goto :goto_0

    .line 1074
    :pswitch_8
    invoke-virtual {p0, p2}, Lcom/google/android/setupwizard/BaseActivity;->onWalledGardenCompleted(I)V

    goto :goto_0

    .line 1077
    :pswitch_9
    invoke-virtual {p0, p2}, Lcom/google/android/setupwizard/BaseActivity;->onOtaUpdateCompleted(I)V

    goto/16 :goto_0

    .line 1080
    :pswitch_a
    invoke-virtual {p0, p2}, Lcom/google/android/setupwizard/BaseActivity;->onSystemUpdateActivityCompleted(I)V

    goto/16 :goto_0

    .line 1083
    :pswitch_b
    if-ne p2, v5, :cond_3

    :goto_1
    invoke-direct {p0, v0}, Lcom/google/android/setupwizard/BaseActivity;->onDateTimeSetupCompleted(Z)V

    goto/16 :goto_0

    :cond_3
    move v0, v1

    goto :goto_1

    .line 1086
    :pswitch_c
    iget-object v2, p0, Lcom/google/android/setupwizard/BaseActivity;->TAG:Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "account setup completed with resultCode "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 1087
    invoke-virtual {p0}, Lcom/google/android/setupwizard/BaseActivity;->getLocalPreferences()Landroid/content/SharedPreferences;

    move-result-object v2

    invoke-interface {v2}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v3

    const-string v4, "accountSetupSkipped"

    const/4 v2, 0x7

    if-ne p2, v2, :cond_4

    move v2, v0

    :goto_2
    invoke-interface {v3, v4, v2}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    move-result-object v2

    invoke-interface {v2}, Landroid/content/SharedPreferences$Editor;->apply()V

    .line 1091
    if-eqz p2, :cond_5

    :goto_3
    invoke-direct {p0, v0, p3}, Lcom/google/android/setupwizard/BaseActivity;->onAccountSetupCompleted(ZLandroid/content/Intent;)V

    goto/16 :goto_0

    :cond_4
    move v2, v1

    .line 1087
    goto :goto_2

    :cond_5
    move v0, v1

    .line 1091
    goto :goto_3

    .line 1094
    :pswitch_d
    if-ne p2, v5, :cond_6

    :goto_4
    invoke-direct {p0, v0}, Lcom/google/android/setupwizard/BaseActivity;->onNameActivityCompleted(Z)V

    goto/16 :goto_0

    :cond_6
    move v0, v1

    goto :goto_4

    .line 1097
    :pswitch_e
    if-ne p2, v5, :cond_7

    :goto_5
    invoke-direct {p0, v0}, Lcom/google/android/setupwizard/BaseActivity;->onPhotoActivityCompleted(Z)V

    goto/16 :goto_0

    :cond_7
    move v0, v1

    goto :goto_5

    .line 1100
    :pswitch_f
    invoke-direct {p0, p2}, Lcom/google/android/setupwizard/BaseActivity;->onSimMissingActivityCompleted(I)V

    goto/16 :goto_0

    .line 1103
    :pswitch_10
    if-ne p2, v5, :cond_8

    :goto_6
    invoke-virtual {p0, v0}, Lcom/google/android/setupwizard/BaseActivity;->onLocationSharingActivityCompleted(Z)V

    goto/16 :goto_0

    :cond_8
    move v0, v1

    goto :goto_6

    .line 1106
    :pswitch_11
    if-ne p2, v5, :cond_9

    :goto_7
    invoke-direct {p0, v0}, Lcom/google/android/setupwizard/BaseActivity;->onNoAccountTosCompleted(Z)V

    goto/16 :goto_0

    :cond_9
    move v0, v1

    goto :goto_7

    .line 1109
    :pswitch_12
    invoke-direct {p0, p2}, Lcom/google/android/setupwizard/BaseActivity;->onGoogleNowCompleted(I)V

    goto/16 :goto_0

    .line 1112
    :pswitch_13
    if-ne p2, v5, :cond_a

    :goto_8
    invoke-virtual {p0, v0}, Lcom/google/android/setupwizard/BaseActivity;->onSetupCompleteCompleted(Z)V

    goto/16 :goto_0

    :cond_a
    move v0, v1

    goto :goto_8

    .line 1115
    :pswitch_14
    invoke-virtual {p0, p2}, Lcom/google/android/setupwizard/BaseActivity;->onCarrierSetupCompleted(I)V

    goto/16 :goto_0

    .line 1047
    :pswitch_data_0
    .packed-switch 0x2711
        :pswitch_1
        :pswitch_5
        :pswitch_3
        :pswitch_7
        :pswitch_10
        :pswitch_0
        :pswitch_c
        :pswitch_0
        :pswitch_f
        :pswitch_d
        :pswitch_e
        :pswitch_13
        :pswitch_11
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_8
        :pswitch_b
        :pswitch_9
        :pswitch_a
        :pswitch_2
        :pswitch_6
        :pswitch_4
        :pswitch_12
        :pswitch_14
    .end packed-switch
.end method

.method public onApplyWindowInsets(Landroid/view/View;Landroid/view/WindowInsets;)Landroid/view/WindowInsets;
    .locals 6
    .param p1, "v"    # Landroid/view/View;
    .param p2, "insets"    # Landroid/view/WindowInsets;

    .prologue
    const/4 v3, 0x0

    .line 1141
    invoke-virtual {p2}, Landroid/view/WindowInsets;->getSystemWindowInsetBottom()I

    move-result v2

    iget-object v4, p0, Lcom/google/android/setupwizard/BaseActivity;->mNavigationBar:Lcom/android/setupwizard/navigationbar/SetupWizardNavBar;

    invoke-virtual {v4}, Lcom/android/setupwizard/navigationbar/SetupWizardNavBar;->getView()Landroid/view/View;

    move-result-object v4

    invoke-virtual {v4}, Landroid/view/View;->getHeight()I

    move-result v4

    sub-int/2addr v2, v4

    invoke-static {v2, v3}, Ljava/lang/Math;->max(II)I

    move-result v0

    .line 1144
    .local v0, "bottomMargin":I
    invoke-virtual {p1}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v1

    check-cast v1, Landroid/view/ViewGroup$MarginLayoutParams;

    .line 1145
    .local v1, "lp":Landroid/view/ViewGroup$MarginLayoutParams;
    iget v2, v1, Landroid/view/ViewGroup$MarginLayoutParams;->leftMargin:I

    iget v4, v1, Landroid/view/ViewGroup$MarginLayoutParams;->topMargin:I

    iget v5, v1, Landroid/view/ViewGroup$MarginLayoutParams;->rightMargin:I

    invoke-virtual {v1, v2, v4, v5, v0}, Landroid/view/ViewGroup$MarginLayoutParams;->setMargins(IIII)V

    .line 1146
    invoke-virtual {p1}, Landroid/view/View;->requestLayout()V

    .line 1147
    iget-object v2, p0, Lcom/google/android/setupwizard/BaseActivity;->mHandler:Landroid/os/Handler;

    iget-object v4, p0, Lcom/google/android/setupwizard/BaseActivity;->mLayoutNextRunnable:Ljava/lang/Runnable;

    invoke-virtual {v2, v4}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 1149
    iget-object v2, p0, Lcom/google/android/setupwizard/BaseActivity;->mNavigationBar:Lcom/android/setupwizard/navigationbar/SetupWizardNavBar;

    invoke-virtual {v2}, Lcom/android/setupwizard/navigationbar/SetupWizardNavBar;->getView()Landroid/view/View;

    move-result-object v4

    if-lez v0, :cond_0

    const/4 v2, 0x4

    :goto_0
    invoke-virtual {v4, v2}, Landroid/view/View;->setVisibility(I)V

    .line 1150
    const/4 v2, 0x1

    invoke-virtual {p2, v3, v3, v3, v2}, Landroid/view/WindowInsets;->consumeSystemWindowInsets(ZZZZ)Landroid/view/WindowInsets;

    move-result-object v2

    return-object v2

    :cond_0
    move v2, v3

    .line 1149
    goto :goto_0
.end method

.method public onBackPressed()V
    .locals 2

    .prologue
    .line 816
    iget-object v0, p0, Lcom/google/android/setupwizard/BaseActivity;->TAG:Ljava/lang/String;

    const-string v1, "BaseActivity.onBackPressed()"

    invoke-static {v0, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 817
    const/4 v0, 0x0

    invoke-virtual {p0}, Lcom/google/android/setupwizard/BaseActivity;->getResultData()Landroid/content/Intent;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lcom/google/android/setupwizard/BaseActivity;->setResultCode(ILandroid/content/Intent;)V

    .line 818
    invoke-super {p0}, Landroid/app/Activity;->onBackPressed()V

    .line 819
    return-void
.end method

.method protected onCarrierSetupCompleted(I)V
    .locals 2
    .param p1, "resultCode"    # I

    .prologue
    .line 1623
    invoke-virtual {p0}, Lcom/google/android/setupwizard/BaseActivity;->getLocalPreferences()Landroid/content/SharedPreferences;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    const-string v1, "carrierSetupResult"

    invoke-interface {v0, v1, p1}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->apply()V

    .line 1624
    invoke-direct {p0}, Lcom/google/android/setupwizard/BaseActivity;->launchWifiSetupOrSkip()V

    .line 1625
    return-void
.end method

.method protected onCdmaActivationActivityCompleted(I)V
    .locals 3
    .param p1, "resultCode"    # I

    .prologue
    .line 1429
    iget-object v0, p0, Lcom/google/android/setupwizard/BaseActivity;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "onCdmaActivationActivityCompleted resultCode="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const/16 v2, 0x2727

    invoke-static {v2, p1}, Lcom/google/android/setupwizard/BaseActivity;->getResultName(II)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 1431
    invoke-virtual {p0, p1}, Lcom/google/android/setupwizard/BaseActivity;->onActivationActivityCompleted(I)V

    .line 1432
    return-void
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 17
    .param p1, "icicle"    # Landroid/os/Bundle;

    .prologue
    .line 653
    new-instance v14, Ljava/lang/StringBuilder;

    invoke-direct {v14}, Ljava/lang/StringBuilder;-><init>()V

    const-string v15, "BaseActivity.onCreate icicle="

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    move-object/from16 v0, p1

    invoke-virtual {v14, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v14

    invoke-virtual {v14}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v14

    move-object/from16 v0, p0

    invoke-direct {v0, v14}, Lcom/google/android/setupwizard/BaseActivity;->logActivityState(Ljava/lang/String;)V

    .line 654
    invoke-super/range {p0 .. p1}, Landroid/app/Activity;->onCreate(Landroid/os/Bundle;)V

    .line 655
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/setupwizard/BaseActivity;->getPreviousTransition()I

    move-result v14

    move-object/from16 v0, p0

    invoke-virtual {v0, v14}, Lcom/google/android/setupwizard/BaseActivity;->applyForwardTransition(I)V

    .line 658
    invoke-direct/range {p0 .. p0}, Lcom/google/android/setupwizard/BaseActivity;->setThemeFromConfig()V

    .line 660
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/setupwizard/BaseActivity;->getIntent()Landroid/content/Intent;

    move-result-object v14

    const-string v15, "noBack"

    const/16 v16, 0x0

    invoke-virtual/range {v14 .. v16}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v14

    if-nez v14, :cond_1

    const/4 v14, 0x1

    :goto_0
    move-object/from16 v0, p0

    invoke-virtual {v0, v14}, Lcom/google/android/setupwizard/BaseActivity;->setBackAllowed(Z)V

    .line 662
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/setupwizard/BaseActivity;->getWindow()Landroid/view/Window;

    move-result-object v14

    invoke-virtual {v14}, Landroid/view/Window;->getDecorView()Landroid/view/View;

    move-result-object v2

    .line 663
    .local v2, "decorView":Landroid/view/View;
    const/16 v8, 0x1602

    .line 667
    .local v8, "immersiveVis":I
    invoke-virtual {v2}, Landroid/view/View;->getViewTreeObserver()Landroid/view/ViewTreeObserver;

    move-result-object v14

    new-instance v15, Lcom/google/android/setupwizard/BaseActivity$3;

    move-object/from16 v0, p0

    invoke-direct {v15, v0, v2}, Lcom/google/android/setupwizard/BaseActivity$3;-><init>(Lcom/google/android/setupwizard/BaseActivity;Landroid/view/View;)V

    invoke-virtual {v14, v15}, Landroid/view/ViewTreeObserver;->addOnPreDrawListener(Landroid/view/ViewTreeObserver$OnPreDrawListener;)V

    .line 678
    invoke-virtual {v2}, Landroid/view/View;->getSystemUiVisibility()I

    move-result v14

    or-int/lit16 v13, v14, 0x1602

    .line 679
    .local v13, "vis":I
    invoke-virtual {v2, v13}, Landroid/view/View;->setSystemUiVisibility(I)V

    .line 681
    const-string v14, "setupwizard.pessimistic"

    const/4 v15, 0x0

    invoke-static {v14, v15}, Landroid/os/SystemProperties;->getBoolean(Ljava/lang/String;Z)Z

    move-result v14

    move-object/from16 v0, p0

    iput-boolean v14, v0, Lcom/google/android/setupwizard/BaseActivity;->mIsPessimisticMode:Z

    .line 683
    move-object/from16 v0, p0

    iget-boolean v14, v0, Lcom/google/android/setupwizard/BaseActivity;->mIsPessimisticMode:Z

    if-eqz v14, :cond_0

    .line 684
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/google/android/setupwizard/BaseActivity;->TAG:Ljava/lang/String;

    const-string v15, "In pessimistic mode."

    invoke-static {v14, v15}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 687
    :cond_0
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/setupwizard/BaseActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v14

    invoke-virtual {v14}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v14

    iget v14, v14, Landroid/content/res/Configuration;->screenLayout:I

    and-int/lit8 v14, v14, 0xf

    move-object/from16 v0, p0

    iput v14, v0, Lcom/google/android/setupwizard/BaseActivity;->mScreenSize:I

    .line 690
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/setupwizard/BaseActivity;->getIntent()Landroid/content/Intent;

    move-result-object v9

    .line 691
    .local v9, "intent":Landroid/content/Intent;
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/google/android/setupwizard/BaseActivity;->TAG:Ljava/lang/String;

    new-instance v15, Ljava/lang/StringBuilder;

    invoke-direct {v15}, Ljava/lang/StringBuilder;-><init>()V

    const-string v16, "intent="

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    invoke-virtual {v15, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v15

    invoke-virtual {v15}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v15

    invoke-static {v14, v15}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 692
    if-eqz v9, :cond_4

    .line 693
    invoke-virtual {v9}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v1

    .line 694
    .local v1, "bundle":Landroid/os/Bundle;
    if-eqz v1, :cond_3

    .line 695
    const/4 v5, 0x0

    .line 696
    .local v5, "i":I
    invoke-virtual {v1}, Landroid/os/Bundle;->keySet()Ljava/util/Set;

    move-result-object v14

    invoke-interface {v14}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v7

    .local v7, "i$":Ljava/util/Iterator;
    :goto_1
    invoke-interface {v7}, Ljava/util/Iterator;->hasNext()Z

    move-result v14

    if-eqz v14, :cond_3

    invoke-interface {v7}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v10

    check-cast v10, Ljava/lang/String;

    .line 697
    .local v10, "key":Ljava/lang/String;
    invoke-virtual {v1, v10}, Landroid/os/Bundle;->get(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v12

    .line 698
    .local v12, "value":Ljava/lang/Object;
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/google/android/setupwizard/BaseActivity;->TAG:Ljava/lang/String;

    new-instance v14, Ljava/lang/StringBuilder;

    invoke-direct {v14}, Ljava/lang/StringBuilder;-><init>()V

    const-string v16, "extra["

    move-object/from16 v0, v16

    invoke-virtual {v14, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    add-int/lit8 v6, v5, 0x1

    .end local v5    # "i":I
    .local v6, "i":I
    invoke-virtual {v14, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v14

    const-string v16, "]: "

    move-object/from16 v0, v16

    invoke-virtual {v14, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    invoke-virtual {v14, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    const-string v16, "="

    move-object/from16 v0, v16

    invoke-virtual {v14, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    invoke-virtual {v14, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v14

    const-string v16, " ("

    move-object/from16 v0, v16

    invoke-virtual {v14, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v16

    if-eqz v12, :cond_2

    invoke-virtual {v12}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v14

    invoke-virtual {v14}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v14

    :goto_2
    move-object/from16 v0, v16

    invoke-virtual {v0, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    const-string v16, ")"

    move-object/from16 v0, v16

    invoke-virtual {v14, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    invoke-virtual {v14}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v14

    invoke-static {v15, v14}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    move v5, v6

    .line 700
    .end local v6    # "i":I
    .restart local v5    # "i":I
    goto :goto_1

    .line 660
    .end local v1    # "bundle":Landroid/os/Bundle;
    .end local v2    # "decorView":Landroid/view/View;
    .end local v5    # "i":I
    .end local v7    # "i$":Ljava/util/Iterator;
    .end local v8    # "immersiveVis":I
    .end local v9    # "intent":Landroid/content/Intent;
    .end local v10    # "key":Ljava/lang/String;
    .end local v12    # "value":Ljava/lang/Object;
    .end local v13    # "vis":I
    :cond_1
    const/4 v14, 0x0

    goto/16 :goto_0

    .line 698
    .restart local v1    # "bundle":Landroid/os/Bundle;
    .restart local v2    # "decorView":Landroid/view/View;
    .restart local v6    # "i":I
    .restart local v7    # "i$":Ljava/util/Iterator;
    .restart local v8    # "immersiveVis":I
    .restart local v9    # "intent":Landroid/content/Intent;
    .restart local v10    # "key":Ljava/lang/String;
    .restart local v12    # "value":Ljava/lang/Object;
    .restart local v13    # "vis":I
    :cond_2
    const/4 v14, 0x0

    goto :goto_2

    .line 703
    .end local v6    # "i":I
    .end local v7    # "i$":Ljava/util/Iterator;
    .end local v10    # "key":Ljava/lang/String;
    .end local v12    # "value":Ljava/lang/Object;
    :cond_3
    const-string v14, "firstRun"

    const/4 v15, 0x0

    invoke-virtual {v9, v14, v15}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v14

    move-object/from16 v0, p0

    iput-boolean v14, v0, Lcom/google/android/setupwizard/BaseActivity;->mIsFirstRun:Z

    .line 707
    .end local v1    # "bundle":Landroid/os/Bundle;
    :cond_4
    invoke-static {}, Landroid/os/UserHandle;->myUserId()I

    move-result v14

    if-nez v14, :cond_8

    const/4 v14, 0x1

    :goto_3
    move-object/from16 v0, p0

    iput-boolean v14, v0, Lcom/google/android/setupwizard/BaseActivity;->mIsPrimaryUser:Z

    .line 708
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/google/android/setupwizard/BaseActivity;->TAG:Ljava/lang/String;

    new-instance v15, Ljava/lang/StringBuilder;

    invoke-direct {v15}, Ljava/lang/StringBuilder;-><init>()V

    const-string v16, "BaseActivity.onCreate() mIsFirstRun="

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/google/android/setupwizard/BaseActivity;->mIsFirstRun:Z

    move/from16 v16, v0

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v15

    const-string v16, " mIsPrimaryUser="

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/google/android/setupwizard/BaseActivity;->mIsPrimaryUser:Z

    move/from16 v16, v0

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v15

    const-string v16, " mIsResumeFromUpdate="

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/google/android/setupwizard/BaseActivity;->mIsResumeFromUpdate:Z

    move/from16 v16, v0

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v15

    invoke-virtual {v15}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v15

    invoke-static {v14, v15}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 713
    const-string v14, "ro.setupwizard.mode"

    sget-object v15, Lcom/google/android/setupwizard/BaseActivity$ProvisioningMode;->OPTIONAL:Lcom/google/android/setupwizard/BaseActivity$ProvisioningMode;

    invoke-virtual {v15}, Lcom/google/android/setupwizard/BaseActivity$ProvisioningMode;->name()Ljava/lang/String;

    move-result-object v15

    invoke-static {v14, v15}, Landroid/os/SystemProperties;->get(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v11

    .line 717
    .local v11, "provisioningMode":Ljava/lang/String;
    :try_start_0
    invoke-static {v11}, Lcom/google/android/setupwizard/BaseActivity$ProvisioningMode;->valueOf(Ljava/lang/String;)Lcom/google/android/setupwizard/BaseActivity$ProvisioningMode;

    move-result-object v14

    move-object/from16 v0, p0

    iput-object v14, v0, Lcom/google/android/setupwizard/BaseActivity;->mProvisioningMode:Lcom/google/android/setupwizard/BaseActivity$ProvisioningMode;
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0

    .line 724
    :goto_4
    const-string v14, "ro.debuggable"

    const/4 v15, 0x0

    invoke-static {v14, v15}, Landroid/os/SystemProperties;->getBoolean(Ljava/lang/String;Z)Z

    move-result v14

    if-nez v14, :cond_5

    const-string v14, "ro.setupwizard.enable_bypass"

    const/4 v15, 0x0

    invoke-static {v14, v15}, Landroid/os/SystemProperties;->getBoolean(Ljava/lang/String;Z)Z

    move-result v14

    if-eqz v14, :cond_9

    :cond_5
    const/4 v4, 0x1

    .line 730
    .local v4, "enableBypass":Z
    :goto_5
    invoke-static {}, Landroid/app/ActivityManager;->isRunningInTestHarness()Z

    move-result v14

    if-eqz v14, :cond_6

    if-eqz v4, :cond_6

    .line 731
    sget-object v14, Lcom/google/android/setupwizard/BaseActivity$ProvisioningMode;->DISABLED:Lcom/google/android/setupwizard/BaseActivity$ProvisioningMode;

    move-object/from16 v0, p0

    iput-object v14, v0, Lcom/google/android/setupwizard/BaseActivity;->mProvisioningMode:Lcom/google/android/setupwizard/BaseActivity$ProvisioningMode;

    .line 734
    :cond_6
    if-eqz v4, :cond_7

    .line 735
    new-instance v14, Lcom/google/android/setupwizard/util/FourCornersGestureDetector;

    invoke-virtual/range {p0 .. p0}, Lcom/google/android/setupwizard/BaseActivity;->getDecorView()Landroid/view/View;

    move-result-object v15

    move-object/from16 v0, p0

    invoke-direct {v14, v0, v15}, Lcom/google/android/setupwizard/util/FourCornersGestureDetector;-><init>(Lcom/google/android/setupwizard/util/FourCornersGestureDetector$OnFourCornersListener;Landroid/view/View;)V

    move-object/from16 v0, p0

    iput-object v14, v0, Lcom/google/android/setupwizard/BaseActivity;->mFourCornersDetector:Lcom/google/android/setupwizard/util/FourCornersGestureDetector;

    .line 737
    :cond_7
    return-void

    .line 707
    .end local v4    # "enableBypass":Z
    .end local v11    # "provisioningMode":Ljava/lang/String;
    :cond_8
    const/4 v14, 0x0

    goto/16 :goto_3

    .line 718
    .restart local v11    # "provisioningMode":Ljava/lang/String;
    :catch_0
    move-exception v3

    .line 720
    .local v3, "e":Ljava/lang/IllegalArgumentException;
    sget-object v14, Lcom/google/android/setupwizard/BaseActivity$ProvisioningMode;->OPTIONAL:Lcom/google/android/setupwizard/BaseActivity$ProvisioningMode;

    move-object/from16 v0, p0

    iput-object v14, v0, Lcom/google/android/setupwizard/BaseActivity;->mProvisioningMode:Lcom/google/android/setupwizard/BaseActivity$ProvisioningMode;

    goto :goto_4

    .line 724
    .end local v3    # "e":Ljava/lang/IllegalArgumentException;
    :cond_9
    const/4 v4, 0x0

    goto :goto_5
.end method

.method protected onDestroy()V
    .locals 1

    .prologue
    .line 784
    const-string v0, "BaseActivity.onDestroy"

    invoke-direct {p0, v0}, Lcom/google/android/setupwizard/BaseActivity;->logActivityState(Ljava/lang/String;)V

    .line 785
    invoke-super {p0}, Landroid/app/Activity;->onDestroy()V

    .line 786
    return-void
.end method

.method public onFourCorners(I)V
    .locals 4
    .param p1, "pattern"    # I

    .prologue
    const/4 v3, 0x1

    .line 2444
    iget-object v0, p0, Lcom/google/android/setupwizard/BaseActivity;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "onFourCorners pattern="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " USER REQUESTED EARLY EXIT"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 2445
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/google/android/setupwizard/BaseActivity;->setResult(I)V

    .line 2446
    const/16 v0, 0x3214

    if-ne v0, p1, :cond_0

    .line 2447
    invoke-virtual {p0}, Lcom/google/android/setupwizard/BaseActivity;->finish()V

    .line 2448
    invoke-virtual {p0}, Lcom/google/android/setupwizard/BaseActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    const v1, 0x7f070053

    invoke-static {v0, v1, v3}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    .line 2455
    :goto_0
    return-void

    .line 2451
    :cond_0
    invoke-virtual {p0}, Lcom/google/android/setupwizard/BaseActivity;->onSetupComplete()V

    .line 2452
    invoke-virtual {p0}, Lcom/google/android/setupwizard/BaseActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    const v1, 0x7f070052

    invoke-static {v0, v1, v3}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    goto :goto_0
.end method

.method protected onLocationSharingActivityCompleted(Z)V
    .locals 3
    .param p1, "goForward"    # Z

    .prologue
    .line 1500
    iget-object v0, p0, Lcom/google/android/setupwizard/BaseActivity;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "onLocationSharingActivityCompleted goForward="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 1501
    if-eqz p1, :cond_0

    .line 1502
    invoke-direct {p0}, Lcom/google/android/setupwizard/BaseActivity;->launchDateTimeSetupOrSkip()V

    .line 1506
    :goto_0
    return-void

    .line 1504
    :cond_0
    invoke-direct {p0}, Lcom/google/android/setupwizard/BaseActivity;->launchAccountSetup()V

    goto :goto_0
.end method

.method protected onMobileDataActivityCompleted(I)V
    .locals 3
    .param p1, "resultCode"    # I

    .prologue
    .line 1450
    iget-object v0, p0, Lcom/google/android/setupwizard/BaseActivity;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "onMobileDataActivityCompleted resultCode="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const/16 v2, 0x2726

    invoke-static {v2, p1}, Lcom/google/android/setupwizard/BaseActivity;->getResultName(II)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 1452
    const/4 v0, 0x2

    if-ne p1, v0, :cond_0

    .line 1453
    invoke-direct {p0}, Lcom/google/android/setupwizard/BaseActivity;->launchActivationActivityOrSkip()V

    .line 1457
    :goto_0
    return-void

    .line 1455
    :cond_0
    invoke-direct {p0}, Lcom/google/android/setupwizard/BaseActivity;->launchCarrierSetupOrSkip()V

    goto :goto_0
.end method

.method public onNavigateBack()V
    .locals 0

    .prologue
    .line 2459
    invoke-virtual {p0}, Lcom/google/android/setupwizard/BaseActivity;->onBackPressed()V

    .line 2460
    return-void
.end method

.method public onNavigateNext()V
    .locals 0

    .prologue
    .line 2464
    invoke-direct {p0}, Lcom/google/android/setupwizard/BaseActivity;->onNextPressed()V

    .line 2465
    return-void
.end method

.method public onNavigationBarCreated(Lcom/android/setupwizard/navigationbar/SetupWizardNavBar;)V
    .locals 2
    .param p1, "bar"    # Lcom/android/setupwizard/navigationbar/SetupWizardNavBar;

    .prologue
    .line 2469
    iput-object p1, p0, Lcom/google/android/setupwizard/BaseActivity;->mNavigationBar:Lcom/android/setupwizard/navigationbar/SetupWizardNavBar;

    .line 2470
    iget-object v0, p0, Lcom/google/android/setupwizard/BaseActivity;->mNavigationBar:Lcom/android/setupwizard/navigationbar/SetupWizardNavBar;

    invoke-virtual {v0}, Lcom/android/setupwizard/navigationbar/SetupWizardNavBar;->getBackButton()Landroid/widget/Button;

    move-result-object v0

    iget-boolean v1, p0, Lcom/google/android/setupwizard/BaseActivity;->mBackAllowed:Z

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setEnabled(Z)V

    .line 2471
    invoke-virtual {p1}, Lcom/android/setupwizard/navigationbar/SetupWizardNavBar;->getView()Landroid/view/View;

    move-result-object v0

    new-instance v1, Lcom/google/android/setupwizard/BaseActivity$5;

    invoke-direct {v1, p0}, Lcom/google/android/setupwizard/BaseActivity$5;-><init>(Lcom/google/android/setupwizard/BaseActivity;)V

    invoke-virtual {v0, v1}, Landroid/view/View;->addOnLayoutChangeListener(Landroid/view/View$OnLayoutChangeListener;)V

    .line 2478
    return-void
.end method

.method protected onOldActivationActivityCompleted(I)V
    .locals 5
    .param p1, "resultCode"    # I

    .prologue
    .line 1412
    iget-object v2, p0, Lcom/google/android/setupwizard/BaseActivity;->TAG:Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "onOldActivationActivityCompleted resultCode="

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const/16 v4, 0x2713

    invoke-static {v4, p1}, Lcom/google/android/setupwizard/BaseActivity;->getResultName(II)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 1415
    :try_start_0
    invoke-virtual {p0}, Lcom/google/android/setupwizard/BaseActivity;->getTelephonyInterface()Lcom/android/internal/telephony/ITelephony;

    move-result-object v1

    .line 1416
    .local v1, "telephonyInterface":Lcom/android/internal/telephony/ITelephony;
    if-eqz v1, :cond_0

    invoke-interface {v1}, Lcom/android/internal/telephony/ITelephony;->needsOtaServiceProvisioning()Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v2

    if-eqz v2, :cond_1

    .line 1418
    :cond_0
    const/4 p1, 0x0

    .line 1425
    .end local v1    # "telephonyInterface":Lcom/android/internal/telephony/ITelephony;
    :goto_0
    invoke-virtual {p0, p1}, Lcom/google/android/setupwizard/BaseActivity;->onActivationActivityCompleted(I)V

    .line 1426
    return-void

    .line 1420
    .restart local v1    # "telephonyInterface":Lcom/android/internal/telephony/ITelephony;
    :cond_1
    const/4 p1, -0x1

    goto :goto_0

    .line 1422
    .end local v1    # "telephonyInterface":Lcom/android/internal/telephony/ITelephony;
    :catch_0
    move-exception v0

    .line 1423
    .local v0, "e":Landroid/os/RemoteException;
    const/4 p1, 0x0

    goto :goto_0
.end method

.method protected onOtaUpdateCompleted(I)V
    .locals 3
    .param p1, "resultCode"    # I

    .prologue
    .line 1482
    iget-object v0, p0, Lcom/google/android/setupwizard/BaseActivity;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "onOtaUpdateCompleted resultCode="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const/16 v2, 0x2723

    invoke-static {v2, p1}, Lcom/google/android/setupwizard/BaseActivity;->getResultName(II)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 1484
    const/4 v0, 0x1

    if-ne p1, v0, :cond_0

    .line 1485
    invoke-direct {p0}, Lcom/google/android/setupwizard/BaseActivity;->launchAccountSetup()V

    .line 1491
    :goto_0
    return-void

    .line 1486
    :cond_0
    if-nez p1, :cond_1

    .line 1487
    invoke-direct {p0}, Lcom/google/android/setupwizard/BaseActivity;->launchCarrierSetupOrSkip()V

    goto :goto_0

    .line 1489
    :cond_1
    invoke-direct {p0}, Lcom/google/android/setupwizard/BaseActivity;->launchSystemUpdateActivity()V

    goto :goto_0
.end method

.method protected onPause()V
    .locals 1

    .prologue
    .line 772
    const-string v0, "BaseActivity.onPause"

    invoke-direct {p0, v0}, Lcom/google/android/setupwizard/BaseActivity;->logActivityState(Ljava/lang/String;)V

    .line 773
    invoke-super {p0}, Landroid/app/Activity;->onPause()V

    .line 774
    return-void
.end method

.method public onRequiresScroll()V
    .locals 5

    .prologue
    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 2399
    iget-boolean v2, p0, Lcom/google/android/setupwizard/BaseActivity;->mDefaultButtonShouldScroll:Z

    if-nez v2, :cond_3

    .line 2400
    const v2, 0x7f0e003c

    invoke-virtual {p0, v2}, Lcom/google/android/setupwizard/BaseActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    .line 2401
    .local v0, "buttonFrame":Landroid/view/View;
    if-eqz v0, :cond_0

    .line 2402
    const/high16 v2, 0x7f040000

    invoke-static {p0, v2}, Landroid/view/animation/AnimationUtils;->loadAnimation(Landroid/content/Context;I)Landroid/view/animation/Animation;

    move-result-object v2

    invoke-virtual {v0, v2}, Landroid/view/View;->startAnimation(Landroid/view/animation/Animation;)V

    .line 2405
    :cond_0
    iget-object v2, p0, Lcom/google/android/setupwizard/BaseActivity;->mDefaultButton:Landroid/view/View;

    if-eqz v2, :cond_1

    .line 2407
    iget-object v2, p0, Lcom/google/android/setupwizard/BaseActivity;->mDefaultButton:Landroid/view/View;

    invoke-virtual {v2, v4}, Landroid/view/View;->setEnabled(Z)V

    .line 2409
    :cond_1
    iget-object v2, p0, Lcom/google/android/setupwizard/BaseActivity;->mNavigationBar:Lcom/android/setupwizard/navigationbar/SetupWizardNavBar;

    if-eqz v2, :cond_2

    .line 2411
    iget-object v2, p0, Lcom/google/android/setupwizard/BaseActivity;->mNavigationBar:Lcom/android/setupwizard/navigationbar/SetupWizardNavBar;

    invoke-virtual {v2}, Lcom/android/setupwizard/navigationbar/SetupWizardNavBar;->getNextButton()Landroid/widget/Button;

    move-result-object v1

    .line 2412
    .local v1, "navNextButton":Landroid/widget/Button;
    const v2, 0x7f02005b

    invoke-virtual {v1, v3, v3, v2, v3}, Landroid/widget/Button;->setCompoundDrawablesRelativeWithIntrinsicBounds(IIII)V

    .line 2414
    const v2, 0x7f070049

    invoke-virtual {v1, v2}, Landroid/widget/Button;->setText(I)V

    .line 2415
    invoke-virtual {v1, v4}, Landroid/widget/Button;->setEnabled(Z)V

    .line 2417
    .end local v1    # "navNextButton":Landroid/widget/Button;
    :cond_2
    iput-boolean v4, p0, Lcom/google/android/setupwizard/BaseActivity;->mDefaultButtonShouldScroll:Z

    .line 2419
    .end local v0    # "buttonFrame":Landroid/view/View;
    :cond_3
    return-void
.end method

.method protected onRestart()V
    .locals 1

    .prologue
    .line 761
    const-string v0, "BaseActivity.onRestart"

    invoke-direct {p0, v0}, Lcom/google/android/setupwizard/BaseActivity;->logActivityState(Ljava/lang/String;)V

    .line 762
    invoke-super {p0}, Landroid/app/Activity;->onRestart()V

    .line 763
    return-void
.end method

.method protected onRestoreInstanceState(Landroid/os/Bundle;)V
    .locals 6
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    const/4 v5, -0x1

    .line 633
    iget-object v2, p0, Lcom/google/android/setupwizard/BaseActivity;->TAG:Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "BaseActivity.onRestoreInstanceState("

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ")"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 635
    invoke-super {p0, p1}, Landroid/app/Activity;->onRestoreInstanceState(Landroid/os/Bundle;)V

    .line 636
    const-string v2, "currentFocus"

    invoke-virtual {p1, v2, v5}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v0

    .line 637
    .local v0, "currentId":I
    if-eq v0, v5, :cond_0

    .line 638
    invoke-virtual {p0, v0}, Lcom/google/android/setupwizard/BaseActivity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    .line 639
    .local v1, "view":Landroid/view/View;
    if-eqz v1, :cond_0

    invoke-virtual {v1}, Landroid/view/View;->requestFocus()Z

    .line 641
    .end local v1    # "view":Landroid/view/View;
    :cond_0
    return-void
.end method

.method protected onResume()V
    .locals 1

    .prologue
    .line 766
    const-string v0, "BaseActivity.onResume"

    invoke-direct {p0, v0}, Lcom/google/android/setupwizard/BaseActivity;->logActivityState(Ljava/lang/String;)V

    .line 767
    invoke-super {p0}, Landroid/app/Activity;->onResume()V

    .line 768
    return-void
.end method

.method protected onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 4
    .param p1, "outState"    # Landroid/os/Bundle;

    .prologue
    .line 645
    invoke-super {p0, p1}, Landroid/app/Activity;->onSaveInstanceState(Landroid/os/Bundle;)V

    .line 646
    invoke-virtual {p0}, Lcom/google/android/setupwizard/BaseActivity;->getCurrentFocus()Landroid/view/View;

    move-result-object v0

    .line 647
    .local v0, "current":Landroid/view/View;
    const-string v2, "currentFocus"

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Landroid/view/View;->getId()I

    move-result v1

    :goto_0
    invoke-virtual {p1, v2, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 648
    iget-object v1, p0, Lcom/google/android/setupwizard/BaseActivity;->TAG:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "BaseActivity.onSaveInstanceState("

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ")"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 649
    return-void

    .line 647
    :cond_0
    const/4 v1, -0x1

    goto :goto_0
.end method

.method public onScrolledToBottom()V
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 2375
    iget-boolean v2, p0, Lcom/google/android/setupwizard/BaseActivity;->mDefaultButtonShouldScroll:Z

    if-eqz v2, :cond_3

    .line 2376
    const v2, 0x7f0e003c

    invoke-virtual {p0, v2}, Lcom/google/android/setupwizard/BaseActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    .line 2377
    .local v0, "buttonFrame":Landroid/view/View;
    if-eqz v0, :cond_0

    .line 2378
    const v2, 0x7f040001

    invoke-static {p0, v2}, Landroid/view/animation/AnimationUtils;->loadAnimation(Landroid/content/Context;I)Landroid/view/animation/Animation;

    move-result-object v2

    invoke-virtual {v0, v2}, Landroid/view/View;->startAnimation(Landroid/view/animation/Animation;)V

    .line 2381
    :cond_0
    iget-object v2, p0, Lcom/google/android/setupwizard/BaseActivity;->mDefaultButton:Landroid/view/View;

    if-eqz v2, :cond_1

    .line 2383
    iget-object v2, p0, Lcom/google/android/setupwizard/BaseActivity;->mDefaultButton:Landroid/view/View;

    iget-boolean v3, p0, Lcom/google/android/setupwizard/BaseActivity;->mNextAllowed:Z

    invoke-virtual {v2, v3}, Landroid/view/View;->setEnabled(Z)V

    .line 2385
    :cond_1
    iget-object v2, p0, Lcom/google/android/setupwizard/BaseActivity;->mNavigationBar:Lcom/android/setupwizard/navigationbar/SetupWizardNavBar;

    if-eqz v2, :cond_2

    .line 2387
    iget-object v2, p0, Lcom/google/android/setupwizard/BaseActivity;->mNavigationBar:Lcom/android/setupwizard/navigationbar/SetupWizardNavBar;

    invoke-virtual {v2}, Lcom/android/setupwizard/navigationbar/SetupWizardNavBar;->getNextButton()Landroid/widget/Button;

    move-result-object v1

    .line 2388
    .local v1, "navNextButton":Landroid/widget/Button;
    const v2, 0x7f020061

    invoke-virtual {v1, v4, v4, v2, v4}, Landroid/widget/Button;->setCompoundDrawablesRelativeWithIntrinsicBounds(IIII)V

    .line 2390
    const v2, 0x7f070024

    invoke-virtual {v1, v2}, Landroid/widget/Button;->setText(I)V

    .line 2391
    iget-boolean v2, p0, Lcom/google/android/setupwizard/BaseActivity;->mNextAllowed:Z

    invoke-virtual {v1, v2}, Landroid/widget/Button;->setEnabled(Z)V

    .line 2393
    .end local v1    # "navNextButton":Landroid/widget/Button;
    :cond_2
    iput-boolean v4, p0, Lcom/google/android/setupwizard/BaseActivity;->mDefaultButtonShouldScroll:Z

    .line 2395
    .end local v0    # "buttonFrame":Landroid/view/View;
    :cond_3
    return-void
.end method

.method protected onSetupComplete()V
    .locals 0

    .prologue
    .line 930
    invoke-direct {p0}, Lcom/google/android/setupwizard/BaseActivity;->startSetupWizardExitActivity()V

    .line 931
    invoke-virtual {p0}, Lcom/google/android/setupwizard/BaseActivity;->finish()V

    .line 932
    return-void
.end method

.method protected onSetupCompleteCompleted(Z)V
    .locals 3
    .param p1, "goForward"    # Z

    .prologue
    .line 1577
    iget-object v0, p0, Lcom/google/android/setupwizard/BaseActivity;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "onSetupCompleteCompleted goForward="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 1578
    if-eqz p1, :cond_0

    .line 1579
    invoke-virtual {p0}, Lcom/google/android/setupwizard/BaseActivity;->onSetupComplete()V

    .line 1587
    :goto_0
    return-void

    .line 1581
    :cond_0
    invoke-virtual {p0}, Lcom/google/android/setupwizard/BaseActivity;->hasNoAccounts()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1582
    invoke-direct {p0}, Lcom/google/android/setupwizard/BaseActivity;->startNoAccountTosActivity()V

    goto :goto_0

    .line 1584
    :cond_1
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/google/android/setupwizard/BaseActivity;->onNoAccountTosCompleted(Z)V

    goto :goto_0
.end method

.method protected onSetupStart()V
    .locals 3

    .prologue
    .line 880
    invoke-virtual {p0}, Lcom/google/android/setupwizard/BaseActivity;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const-string v1, "captive_portal_detection_enabled"

    const/4 v2, 0x0

    invoke-static {v0, v1, v2}, Landroid/provider/Settings$Global;->putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z

    .line 883
    invoke-virtual {p0}, Lcom/google/android/setupwizard/BaseActivity;->isFirstRun()Z

    move-result v0

    invoke-static {p0, v0}, Lcom/google/android/setupwizard/util/SetupStatusBarManager;->onSetupStart(Landroid/content/Context;Z)V

    .line 885
    invoke-virtual {p0}, Lcom/google/android/setupwizard/BaseActivity;->setScreenTimeout()V

    .line 888
    sget-boolean v0, Lcom/google/android/setupwizard/BaseActivity;->ROTATION_LOCKED:Z

    if-eqz v0, :cond_0

    .line 889
    invoke-virtual {p0}, Lcom/google/android/setupwizard/BaseActivity;->lockRotation()V

    .line 891
    :cond_0
    return-void
.end method

.method protected onStart()V
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 741
    const-string v1, "BaseActivity.onStart"

    invoke-direct {p0, v1}, Lcom/google/android/setupwizard/BaseActivity;->logActivityState(Ljava/lang/String;)V

    .line 742
    invoke-super {p0}, Landroid/app/Activity;->onStart()V

    .line 744
    const v1, 0x7f0e005e

    invoke-virtual {p0, v1}, Lcom/google/android/setupwizard/BaseActivity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/google/android/setupwizard/util/BottomScrollView;

    iput-object v1, p0, Lcom/google/android/setupwizard/BaseActivity;->mScrollView:Lcom/google/android/setupwizard/util/BottomScrollView;

    .line 745
    iget-object v1, p0, Lcom/google/android/setupwizard/BaseActivity;->mScrollView:Lcom/google/android/setupwizard/util/BottomScrollView;

    if-eqz v1, :cond_0

    .line 746
    iget-object v1, p0, Lcom/google/android/setupwizard/BaseActivity;->mScrollView:Lcom/google/android/setupwizard/util/BottomScrollView;

    invoke-virtual {v1, p0}, Lcom/google/android/setupwizard/util/BottomScrollView;->setBottomScrollListener(Lcom/google/android/setupwizard/util/BottomScrollView$BottomScrollListener;)V

    .line 751
    :cond_0
    invoke-virtual {p0}, Lcom/google/android/setupwizard/BaseActivity;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    const-string v2, "user_setup_complete"

    invoke-static {v1, v2, v0}, Landroid/provider/Settings$Secure;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v1

    if-eqz v1, :cond_1

    const/4 v0, 0x1

    .line 753
    .local v0, "completed":Z
    :cond_1
    if-eqz v0, :cond_2

    .line 754
    iget-object v1, p0, Lcom/google/android/setupwizard/BaseActivity;->TAG:Ljava/lang/String;

    const-string v2, "BaseActivity.onStart() USER_SETUP_COMPLETE=true, time to go!!!"

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 755
    invoke-virtual {p0}, Lcom/google/android/setupwizard/BaseActivity;->onSetupComplete()V

    .line 757
    :cond_2
    return-void
.end method

.method protected onStop()V
    .locals 1

    .prologue
    .line 778
    const-string v0, "BaseActivity.onStop"

    invoke-direct {p0, v0}, Lcom/google/android/setupwizard/BaseActivity;->logActivityState(Ljava/lang/String;)V

    .line 779
    invoke-super {p0}, Landroid/app/Activity;->onStop()V

    .line 780
    return-void
.end method

.method protected onSystemUpdateActivityCompleted(I)V
    .locals 3
    .param p1, "resultCode"    # I

    .prologue
    .line 1494
    iget-object v0, p0, Lcom/google/android/setupwizard/BaseActivity;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "onSystemUpdateActivityCompleted resultCode="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const/16 v2, 0x2724

    invoke-static {v2, p1}, Lcom/google/android/setupwizard/BaseActivity;->getResultName(II)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 1496
    invoke-direct {p0}, Lcom/google/android/setupwizard/BaseActivity;->launchAccountSetup()V

    .line 1497
    return-void
.end method

.method protected onWalledGardenCompleted(I)V
    .locals 3
    .param p1, "resultCode"    # I

    .prologue
    .line 1472
    iget-object v0, p0, Lcom/google/android/setupwizard/BaseActivity;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "onWalledGardenCompleted resultCode="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const/16 v2, 0x2721

    invoke-static {v2, p1}, Lcom/google/android/setupwizard/BaseActivity;->getResultName(II)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 1474
    if-nez p1, :cond_0

    .line 1475
    invoke-direct {p0}, Lcom/google/android/setupwizard/BaseActivity;->launchCarrierSetupOrSkip()V

    .line 1479
    :goto_0
    return-void

    .line 1477
    :cond_0
    invoke-direct {p0}, Lcom/google/android/setupwizard/BaseActivity;->launchOtaUpdateCheck()V

    goto :goto_0
.end method

.method protected onWelcomeScreenCompleted()V
    .locals 3

    .prologue
    .line 1374
    iget-object v1, p0, Lcom/google/android/setupwizard/BaseActivity;->TAG:Ljava/lang/String;

    const-string v2, "onWelcomeScreenCompleted"

    invoke-static {v1, v2}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 1376
    const-string v1, "phone"

    invoke-virtual {p0, v1}, Lcom/google/android/setupwizard/BaseActivity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/telephony/TelephonyManager;

    .line 1377
    .local v0, "tm":Landroid/telephony/TelephonyManager;
    if-eqz v0, :cond_0

    const-string v1, "00101"

    invoke-virtual {v0}, Landroid/telephony/TelephonyManager;->getSimOperator()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 1379
    invoke-virtual {p0}, Lcom/google/android/setupwizard/BaseActivity;->onSetupComplete()V

    .line 1400
    :goto_0
    return-void

    .line 1380
    :cond_0
    invoke-virtual {p0}, Lcom/google/android/setupwizard/BaseActivity;->isPessimisticMode()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 1381
    iget-object v1, p0, Lcom/google/android/setupwizard/BaseActivity;->TAG:Ljava/lang/String;

    const-string v2, "Be pessimistic. Launch ActivationActivity anyway."

    invoke-static {v1, v2}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 1382
    invoke-direct {p0}, Lcom/google/android/setupwizard/BaseActivity;->launchActivationActivity()V

    goto :goto_0

    .line 1383
    :cond_1
    invoke-virtual {p0}, Lcom/google/android/setupwizard/BaseActivity;->isMobileNetworkSupported()Z

    move-result v1

    if-nez v1, :cond_2

    .line 1384
    iget-object v1, p0, Lcom/google/android/setupwizard/BaseActivity;->TAG:Ljava/lang/String;

    const-string v2, "no mobile network support; skipping activation"

    invoke-static {v1, v2}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 1385
    invoke-direct {p0}, Lcom/google/android/setupwizard/BaseActivity;->launchWifiSetupOrSkip()V

    goto :goto_0

    .line 1386
    :cond_2
    invoke-static {}, Lcom/google/android/setupwizard/carrier/PhoneMonitor;->simMissing()Z

    move-result v1

    if-eqz v1, :cond_4

    .line 1387
    iget-object v1, p0, Lcom/google/android/setupwizard/BaseActivity;->TAG:Ljava/lang/String;

    const-string v2, "missing SIM; launching SimMissingActivity"

    invoke-static {v1, v2}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 1388
    invoke-virtual {p0}, Lcom/google/android/setupwizard/BaseActivity;->shouldSuppressSimWarning()Z

    move-result v1

    if-eqz v1, :cond_3

    .line 1390
    invoke-direct {p0}, Lcom/google/android/setupwizard/BaseActivity;->launchWifiSetupOrSkip()V

    goto :goto_0

    .line 1392
    :cond_3
    invoke-direct {p0}, Lcom/google/android/setupwizard/BaseActivity;->launchSimMissingActivity()V

    goto :goto_0

    .line 1394
    :cond_4
    invoke-static {}, Lcom/google/android/setupwizard/carrier/PhoneMonitor;->hasSimProblem()Z

    move-result v1

    if-eqz v1, :cond_5

    .line 1395
    iget-object v1, p0, Lcom/google/android/setupwizard/BaseActivity;->TAG:Ljava/lang/String;

    const-string v2, "bad SIM; launching wifi"

    invoke-static {v1, v2}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 1396
    invoke-direct {p0}, Lcom/google/android/setupwizard/BaseActivity;->launchWifiSetupOrSkip()V

    goto :goto_0

    .line 1398
    :cond_5
    invoke-direct {p0}, Lcom/google/android/setupwizard/BaseActivity;->launchActivationActivityOrSkip()V

    goto :goto_0
.end method

.method protected onWelcomeUserScreenCompleted()V
    .locals 2

    .prologue
    .line 1403
    iget-object v0, p0, Lcom/google/android/setupwizard/BaseActivity;->TAG:Ljava/lang/String;

    const-string v1, "onWelcomeUserScreenCompleted"

    invoke-static {v0, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 1404
    invoke-static {p0}, Landroid/os/UserManager;->get(Landroid/content/Context;)Landroid/os/UserManager;

    move-result-object v0

    invoke-virtual {v0}, Landroid/os/UserManager;->isLinkedUser()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1405
    invoke-direct {p0}, Lcom/google/android/setupwizard/BaseActivity;->startNoAccountTosActivity()V

    .line 1409
    :goto_0
    return-void

    .line 1407
    :cond_0
    invoke-direct {p0}, Lcom/google/android/setupwizard/BaseActivity;->launchAccountSetup()V

    goto :goto_0
.end method

.method protected onWifiSetupCompleted(I)V
    .locals 3
    .param p1, "resultCode"    # I

    .prologue
    .line 1460
    iget-object v0, p0, Lcom/google/android/setupwizard/BaseActivity;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "onWifiSetupCompleted resultCode="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const/16 v2, 0x2714

    invoke-static {v2, p1}, Lcom/google/android/setupwizard/BaseActivity;->getResultName(II)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 1462
    if-nez p1, :cond_0

    .line 1463
    invoke-virtual {p0}, Lcom/google/android/setupwizard/BaseActivity;->startWelcomeActivity()V

    .line 1469
    :goto_0
    return-void

    .line 1464
    :cond_0
    invoke-virtual {p0}, Lcom/google/android/setupwizard/BaseActivity;->isNetworkConnected()Z

    move-result v0

    if-nez v0, :cond_1

    invoke-virtual {p0}, Lcom/google/android/setupwizard/BaseActivity;->isNetworkRequired()Z

    move-result v0

    if-nez v0, :cond_1

    .line 1465
    invoke-direct {p0}, Lcom/google/android/setupwizard/BaseActivity;->startLocationSharingActivity()V

    goto :goto_0

    .line 1467
    :cond_1
    invoke-direct {p0}, Lcom/google/android/setupwizard/BaseActivity;->launchWalledGardenCheck()V

    goto :goto_0
.end method

.method protected placeEmergencyCall()V
    .locals 4

    .prologue
    .line 610
    :try_start_0
    new-instance v1, Landroid/content/Intent;

    const-string v2, "com.android.phone.EmergencyDialer.DIAL"

    invoke-direct {v1, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 611
    .local v1, "intent":Landroid/content/Intent;
    const/high16 v2, 0x10800000

    invoke-virtual {v1, v2}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    .line 613
    invoke-virtual {p0, v1}, Lcom/google/android/setupwizard/BaseActivity;->startFirstRunActivity(Landroid/content/Intent;)V
    :try_end_0
    .catch Landroid/content/ActivityNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    .line 618
    .end local v1    # "intent":Landroid/content/Intent;
    :goto_0
    return-void

    .line 614
    :catch_0
    move-exception v0

    .line 616
    .local v0, "e":Landroid/content/ActivityNotFoundException;
    iget-object v2, p0, Lcom/google/android/setupwizard/BaseActivity;->TAG:Ljava/lang/String;

    const-string v3, "Can\'t find the emergency dialer: com.android.phone.EmergencyDialer.DIAL"

    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method protected provisioningDisabled()Z
    .locals 2

    .prologue
    .line 1959
    iget-object v0, p0, Lcom/google/android/setupwizard/BaseActivity;->mProvisioningMode:Lcom/google/android/setupwizard/BaseActivity$ProvisioningMode;

    sget-object v1, Lcom/google/android/setupwizard/BaseActivity$ProvisioningMode;->DISABLED:Lcom/google/android/setupwizard/BaseActivity$ProvisioningMode;

    if-eq v0, v1, :cond_0

    iget-object v0, p0, Lcom/google/android/setupwizard/BaseActivity;->mProvisioningMode:Lcom/google/android/setupwizard/BaseActivity$ProvisioningMode;

    sget-object v1, Lcom/google/android/setupwizard/BaseActivity$ProvisioningMode;->EMULATOR:Lcom/google/android/setupwizard/BaseActivity$ProvisioningMode;

    if-ne v0, v1, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected putPrefsString(Ljava/lang/String;Ljava/lang/String;)V
    .locals 2
    .param p1, "key"    # Ljava/lang/String;
    .param p2, "value"    # Ljava/lang/String;

    .prologue
    .line 2436
    const-string v0, "SetupWizardPrefs"

    const/4 v1, 0x4

    invoke-virtual {p0, v0, v1}, Lcom/google/android/setupwizard/BaseActivity;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    invoke-interface {v0, p1, p2}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->apply()V

    .line 2440
    return-void
.end method

.method protected putThemeExtra(Landroid/content/Intent;)V
    .locals 2
    .param p1, "intent"    # Landroid/content/Intent;

    .prologue
    .line 2324
    invoke-virtual {p0}, Lcom/google/android/setupwizard/BaseActivity;->isUsingWizardManager()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2325
    const-string v0, "theme"

    iget-object v1, p0, Lcom/google/android/setupwizard/BaseActivity;->mThemeType:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 2330
    :goto_0
    return-void

    .line 2328
    :cond_0
    const-string v0, "theme"

    const-string v1, "holo"

    invoke-virtual {p1, v0, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    goto :goto_0
.end method

.method protected restoreScreenTimeout()V
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 914
    invoke-virtual {p0}, Lcom/google/android/setupwizard/BaseActivity;->getLocalPreferences()Landroid/content/SharedPreferences;

    move-result-object v2

    const-string v3, "screenTimeout"

    invoke-interface {v2, v3}, Landroid/content/SharedPreferences;->contains(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 915
    invoke-virtual {p0}, Lcom/google/android/setupwizard/BaseActivity;->getLocalPreferences()Landroid/content/SharedPreferences;

    move-result-object v2

    const-string v3, "screenTimeout"

    invoke-interface {v2, v3, v4}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v1

    .line 917
    .local v1, "origTimeout":I
    invoke-virtual {p0}, Lcom/google/android/setupwizard/BaseActivity;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    const-string v3, "screen_off_timeout"

    invoke-static {v2, v3, v4}, Landroid/provider/Settings$System;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v0

    .line 919
    .local v0, "currentScreenTimeout":I
    if-lez v1, :cond_0

    const v2, 0x1d8a8

    if-ne v0, v2, :cond_0

    .line 920
    invoke-virtual {p0}, Lcom/google/android/setupwizard/BaseActivity;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    const-string v3, "screen_off_timeout"

    invoke-static {v2, v3, v1}, Landroid/provider/Settings$System;->putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z

    .line 924
    .end local v0    # "currentScreenTimeout":I
    .end local v1    # "origTimeout":I
    :cond_0
    return-void
.end method

.method protected sendActionResults()V
    .locals 4

    .prologue
    .line 1236
    iget-object v1, p0, Lcom/google/android/setupwizard/BaseActivity;->TAG:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "sendActionResults resultCode="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget v3, p0, Lcom/google/android/setupwizard/BaseActivity;->mResultCode:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " data="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/google/android/setupwizard/BaseActivity;->mResultData:Landroid/content/Intent;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1238
    new-instance v0, Landroid/content/Intent;

    const-string v1, "com.android.wizard.NEXT"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 1239
    .local v0, "intent":Landroid/content/Intent;
    const-string v1, "scriptUri"

    invoke-virtual {p0}, Lcom/google/android/setupwizard/BaseActivity;->getIntent()Landroid/content/Intent;

    move-result-object v2

    const-string v3, "scriptUri"

    invoke-virtual {v2, v3}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 1241
    const-string v1, "actionId"

    invoke-virtual {p0}, Lcom/google/android/setupwizard/BaseActivity;->getIntent()Landroid/content/Intent;

    move-result-object v2

    const-string v3, "actionId"

    invoke-virtual {v2, v3}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 1243
    const-string v1, "com.android.setupwizard.ResultCode"

    iget v2, p0, Lcom/google/android/setupwizard/BaseActivity;->mResultCode:I

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 1244
    iget-object v1, p0, Lcom/google/android/setupwizard/BaseActivity;->mResultData:Landroid/content/Intent;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/google/android/setupwizard/BaseActivity;->mResultData:Landroid/content/Intent;

    invoke-virtual {v1}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 1245
    iget-object v1, p0, Lcom/google/android/setupwizard/BaseActivity;->mResultData:Landroid/content/Intent;

    invoke-virtual {v1}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/content/Intent;->putExtras(Landroid/os/Bundle;)Landroid/content/Intent;

    .line 1247
    :cond_0
    invoke-virtual {p0, v0}, Lcom/google/android/setupwizard/BaseActivity;->putThemeExtra(Landroid/content/Intent;)V

    .line 1248
    const/16 v1, 0x2710

    invoke-virtual {p0, v0, v1}, Lcom/google/android/setupwizard/BaseActivity;->startActivityForResult(Landroid/content/Intent;I)V

    .line 1249
    return-void
.end method

.method protected setAccountDomains([Ljava/lang/String;)V
    .locals 5
    .param p1, "domains"    # [Ljava/lang/String;

    .prologue
    .line 2197
    const-string v1, "SetupWizardPrefs"

    const/4 v2, 0x4

    invoke-virtual {p0, v1, v2}, Lcom/google/android/setupwizard/BaseActivity;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v0

    .line 2199
    .local v0, "prefs":Landroid/content/SharedPreferences;
    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v1

    const-string v2, "setupwizard.account_domains"

    new-instance v3, Ljava/util/HashSet;

    invoke-static {p1}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v4

    invoke-direct {v3, v4}, Ljava/util/HashSet;-><init>(Ljava/util/Collection;)V

    invoke-interface {v1, v2, v3}, Landroid/content/SharedPreferences$Editor;->putStringSet(Ljava/lang/String;Ljava/util/Set;)Landroid/content/SharedPreferences$Editor;

    move-result-object v1

    invoke-interface {v1}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 2203
    return-void
.end method

.method protected setBackAllowed(Z)V
    .locals 4
    .param p1, "allowed"    # Z

    .prologue
    .line 470
    iput-boolean p1, p0, Lcom/google/android/setupwizard/BaseActivity;->mBackAllowed:Z

    .line 473
    invoke-virtual {p0}, Lcom/google/android/setupwizard/BaseActivity;->getWindow()Landroid/view/Window;

    move-result-object v2

    invoke-virtual {v2}, Landroid/view/Window;->getDecorView()Landroid/view/View;

    move-result-object v0

    .line 474
    .local v0, "decorView":Landroid/view/View;
    invoke-virtual {v0}, Landroid/view/View;->getSystemUiVisibility()I

    move-result v1

    .line 475
    .local v1, "visibility":I
    if-eqz p1, :cond_2

    .line 476
    const v2, -0x400001

    and-int/2addr v1, v2

    .line 480
    :goto_0
    invoke-virtual {v0, v1}, Landroid/view/View;->setSystemUiVisibility(I)V

    .line 483
    iget-object v2, p0, Lcom/google/android/setupwizard/BaseActivity;->mBackButton:Landroid/view/View;

    if-eqz v2, :cond_0

    .line 484
    iget-object v3, p0, Lcom/google/android/setupwizard/BaseActivity;->mBackButton:Landroid/view/View;

    if-eqz p1, :cond_3

    const/4 v2, 0x0

    :goto_1
    invoke-virtual {v3, v2}, Landroid/view/View;->setVisibility(I)V

    .line 488
    :cond_0
    iget-object v2, p0, Lcom/google/android/setupwizard/BaseActivity;->mNavigationBar:Lcom/android/setupwizard/navigationbar/SetupWizardNavBar;

    if-eqz v2, :cond_1

    .line 489
    iget-object v2, p0, Lcom/google/android/setupwizard/BaseActivity;->mNavigationBar:Lcom/android/setupwizard/navigationbar/SetupWizardNavBar;

    invoke-virtual {v2}, Lcom/android/setupwizard/navigationbar/SetupWizardNavBar;->getBackButton()Landroid/widget/Button;

    move-result-object v2

    invoke-virtual {v2, p1}, Landroid/widget/Button;->setEnabled(Z)V

    .line 491
    :cond_1
    return-void

    .line 478
    :cond_2
    const/high16 v2, 0x400000

    or-int/2addr v1, v2

    goto :goto_0

    .line 484
    :cond_3
    const/16 v2, 0x8

    goto :goto_1
.end method

.method protected final setDefaultButton(Landroid/view/View;)V
    .locals 1
    .param p1, "view"    # Landroid/view/View;

    .prologue
    .line 563
    iput-object p1, p0, Lcom/google/android/setupwizard/BaseActivity;->mDefaultButton:Landroid/view/View;

    .line 564
    iget-object v0, p0, Lcom/google/android/setupwizard/BaseActivity;->mOnDefaultButtonClicked:Landroid/view/View$OnClickListener;

    invoke-virtual {p1, v0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 565
    return-void
.end method

.method protected setHeaderText(I)V
    .locals 2
    .param p1, "headerText"    # I

    .prologue
    .line 841
    const v1, 0x7f0e005d

    invoke-virtual {p0, v1}, Lcom/google/android/setupwizard/BaseActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 842
    .local v0, "titleView":Landroid/widget/TextView;
    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(I)V

    .line 843
    return-void
.end method

.method protected setHeaderText(Ljava/lang/CharSequence;)V
    .locals 2
    .param p1, "headerText"    # Ljava/lang/CharSequence;

    .prologue
    .line 846
    const v1, 0x7f0e005d

    invoke-virtual {p0, v1}, Lcom/google/android/setupwizard/BaseActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 847
    .local v0, "titleView":Landroid/widget/TextView;
    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 848
    return-void
.end method

.method protected setIllustration(II)V
    .locals 6
    .param p1, "phoneAsset"    # I
    .param p2, "tabletAsset"    # I

    .prologue
    .line 851
    const v4, 0x7f0e0060

    invoke-virtual {p0, v4}, Lcom/google/android/setupwizard/BaseActivity;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Lcom/google/android/setupwizard/util/SetupWizardIllustration;

    .line 853
    .local v2, "phoneIllustration":Lcom/google/android/setupwizard/util/SetupWizardIllustration;
    if-eqz v2, :cond_0

    .line 854
    invoke-virtual {p0, p1}, Lcom/google/android/setupwizard/BaseActivity;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    .line 855
    .local v0, "drawable":Landroid/graphics/drawable/Drawable;
    const/4 v4, 0x1

    invoke-virtual {v0, v4}, Landroid/graphics/drawable/Drawable;->setAutoMirrored(Z)V

    .line 856
    invoke-virtual {v2, v0}, Lcom/google/android/setupwizard/util/SetupWizardIllustration;->setForeground(Landroid/graphics/drawable/Drawable;)V

    .line 858
    .end local v0    # "drawable":Landroid/graphics/drawable/Drawable;
    :cond_0
    const v4, 0x7f0e005c

    invoke-virtual {p0, v4}, Lcom/google/android/setupwizard/BaseActivity;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Lcom/google/android/setupwizard/util/SetupWizardIllustration;

    .line 860
    .local v3, "tabletIllustration":Lcom/google/android/setupwizard/util/SetupWizardIllustration;
    if-eqz v3, :cond_2

    .line 861
    const v4, 0x7f020059

    invoke-virtual {p0, v4}, Lcom/google/android/setupwizard/BaseActivity;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    check-cast v1, Landroid/graphics/drawable/LayerDrawable;

    .line 862
    .local v1, "layers":Landroid/graphics/drawable/LayerDrawable;
    invoke-virtual {p0, p2}, Lcom/google/android/setupwizard/BaseActivity;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    .line 863
    .restart local v0    # "drawable":Landroid/graphics/drawable/Drawable;
    instance-of v4, v0, Landroid/graphics/drawable/BitmapDrawable;

    if-eqz v4, :cond_1

    move-object v4, v0

    .line 864
    check-cast v4, Landroid/graphics/drawable/BitmapDrawable;

    const v5, 0x800033

    invoke-virtual {v4, v5}, Landroid/graphics/drawable/BitmapDrawable;->setGravity(I)V

    .line 866
    :cond_1
    const v4, 0x7f0e006f

    invoke-virtual {v1, v4, v0}, Landroid/graphics/drawable/LayerDrawable;->setDrawableByLayerId(ILandroid/graphics/drawable/Drawable;)Z

    .line 867
    invoke-virtual {v3, v1}, Lcom/google/android/setupwizard/util/SetupWizardIllustration;->setForeground(Landroid/graphics/drawable/Drawable;)V

    .line 869
    .end local v0    # "drawable":Landroid/graphics/drawable/Drawable;
    .end local v1    # "layers":Landroid/graphics/drawable/LayerDrawable;
    :cond_2
    return-void
.end method

.method protected setLastActivity(I)V
    .locals 3
    .param p1, "requestCode"    # I

    .prologue
    .line 2129
    const-string v1, "SetupWizardPrefs"

    const/4 v2, 0x4

    invoke-virtual {p0, v1, v2}, Lcom/google/android/setupwizard/BaseActivity;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v0

    .line 2131
    .local v0, "prefs":Landroid/content/SharedPreferences;
    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v1

    const-string v2, "lastActivity"

    invoke-interface {v1, v2, p1}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    move-result-object v1

    invoke-interface {v1}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 2134
    return-void
.end method

.method protected setNextAllowed(Z)V
    .locals 1
    .param p1, "allowed"    # Z

    .prologue
    .line 499
    iput-boolean p1, p0, Lcom/google/android/setupwizard/BaseActivity;->mNextAllowed:Z

    .line 501
    iget-boolean v0, p0, Lcom/google/android/setupwizard/BaseActivity;->mDefaultButtonShouldScroll:Z

    if-nez v0, :cond_1

    .line 502
    iget-object v0, p0, Lcom/google/android/setupwizard/BaseActivity;->mDefaultButton:Landroid/view/View;

    if-eqz v0, :cond_0

    .line 503
    iget-object v0, p0, Lcom/google/android/setupwizard/BaseActivity;->mDefaultButton:Landroid/view/View;

    invoke-virtual {v0, p1}, Landroid/view/View;->setEnabled(Z)V

    .line 505
    :cond_0
    iget-object v0, p0, Lcom/google/android/setupwizard/BaseActivity;->mNavigationBar:Lcom/android/setupwizard/navigationbar/SetupWizardNavBar;

    if-eqz v0, :cond_1

    .line 506
    iget-object v0, p0, Lcom/google/android/setupwizard/BaseActivity;->mNavigationBar:Lcom/android/setupwizard/navigationbar/SetupWizardNavBar;

    invoke-virtual {v0}, Lcom/android/setupwizard/navigationbar/SetupWizardNavBar;->getNextButton()Landroid/widget/Button;

    move-result-object v0

    invoke-virtual {v0, p1}, Landroid/widget/Button;->setEnabled(Z)V

    .line 509
    :cond_1
    return-void
.end method

.method protected setRequireLockPin(Z)V
    .locals 3
    .param p1, "require"    # Z

    .prologue
    .line 2284
    const-string v1, "SetupWizardPrefs"

    const/4 v2, 0x4

    invoke-virtual {p0, v1, v2}, Lcom/google/android/setupwizard/BaseActivity;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v0

    .line 2286
    .local v0, "prefs":Landroid/content/SharedPreferences;
    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v1

    const-string v2, "setupwizard.require_lock_pin"

    invoke-interface {v1, v2, p1}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    move-result-object v1

    invoke-interface {v1}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 2289
    return-void
.end method

.method protected setResultCode(I)V
    .locals 1
    .param p1, "resultCode"    # I

    .prologue
    .line 1183
    invoke-virtual {p0}, Lcom/google/android/setupwizard/BaseActivity;->getResultData()Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {p0, p1, v0}, Lcom/google/android/setupwizard/BaseActivity;->setResultCode(ILandroid/content/Intent;)V

    .line 1184
    return-void
.end method

.method protected setResultCode(ILandroid/content/Intent;)V
    .locals 3
    .param p1, "resultCode"    # I
    .param p2, "data"    # Landroid/content/Intent;

    .prologue
    .line 1194
    iget-object v0, p0, Lcom/google/android/setupwizard/BaseActivity;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "BaseActivity.setResultCode result="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const/4 v2, 0x0

    invoke-static {v2, p1}, Lcom/google/android/setupwizard/BaseActivity;->getResultName(II)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " data="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1195
    iput p1, p0, Lcom/google/android/setupwizard/BaseActivity;->mResultCode:I

    .line 1196
    iput-object p2, p0, Lcom/google/android/setupwizard/BaseActivity;->mResultData:Landroid/content/Intent;

    .line 1197
    invoke-virtual {p0, p1, p2}, Lcom/google/android/setupwizard/BaseActivity;->setResult(ILandroid/content/Intent;)V

    .line 1198
    return-void
.end method

.method protected setScreenTimeout()V
    .locals 4

    .prologue
    const v3, 0x1d8a8

    .line 898
    invoke-virtual {p0}, Lcom/google/android/setupwizard/BaseActivity;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    const-string v2, "screen_off_timeout"

    invoke-static {v1, v2, v3}, Landroid/provider/Settings$System;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v0

    .line 900
    .local v0, "origTimeout":I
    invoke-virtual {p0}, Lcom/google/android/setupwizard/BaseActivity;->getLocalPreferences()Landroid/content/SharedPreferences;

    move-result-object v1

    invoke-interface {v1}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v1

    const-string v2, "screenTimeout"

    invoke-interface {v1, v2, v0}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    move-result-object v1

    invoke-interface {v1}, Landroid/content/SharedPreferences$Editor;->apply()V

    .line 904
    invoke-virtual {p0}, Lcom/google/android/setupwizard/BaseActivity;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    const-string v2, "screen_off_timeout"

    invoke-static {v1, v2, v3}, Landroid/provider/Settings$System;->putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z

    .line 906
    return-void
.end method

.method protected setShowLockPinScreen(Z)V
    .locals 3
    .param p1, "show"    # Z

    .prologue
    .line 2262
    const-string v1, "SetupWizardPrefs"

    const/4 v2, 0x4

    invoke-virtual {p0, v1, v2}, Lcom/google/android/setupwizard/BaseActivity;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v0

    .line 2264
    .local v0, "prefs":Landroid/content/SharedPreferences;
    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v1

    const-string v2, "setupwizard.show_lock_pin_screen"

    invoke-interface {v1, v2, p1}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    move-result-object v1

    invoke-interface {v1}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 2267
    return-void
.end method

.method protected setShowSelfieScreen(Z)V
    .locals 3
    .param p1, "show"    # Z

    .prologue
    .line 2305
    const-string v1, "SetupWizardPrefs"

    const/4 v2, 0x4

    invoke-virtual {p0, v1, v2}, Lcom/google/android/setupwizard/BaseActivity;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v0

    .line 2307
    .local v0, "prefs":Landroid/content/SharedPreferences;
    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v1

    const-string v2, "setupwizard.show_selfie_screen"

    invoke-interface {v1, v2, p1}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    move-result-object v1

    invoke-interface {v1}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 2310
    return-void
.end method

.method protected setTemplateContent(II)V
    .locals 3
    .param p1, "resid"    # I
    .param p2, "layoutTemplate"    # I

    .prologue
    .line 822
    invoke-virtual {p0, p2}, Lcom/google/android/setupwizard/BaseActivity;->setContentView(I)V

    .line 823
    const v2, 0x7f0e005a

    invoke-virtual {p0, v2}, Lcom/google/android/setupwizard/BaseActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    .line 824
    .local v0, "container":Landroid/view/ViewGroup;
    if-eqz v0, :cond_0

    .line 825
    invoke-virtual {v0}, Landroid/view/ViewGroup;->removeAllViews()V

    .line 826
    invoke-static {p0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v1

    .line 827
    .local v1, "inflater":Landroid/view/LayoutInflater;
    invoke-virtual {v1, p1, v0}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    .line 829
    .end local v1    # "inflater":Landroid/view/LayoutInflater;
    :cond_0
    return-void
.end method

.method protected shouldSuppressCreditCardRequest()Z
    .locals 1

    .prologue
    .line 2254
    invoke-virtual {p0}, Lcom/google/android/setupwizard/BaseActivity;->isEduSignin()Z

    move-result v0

    return v0
.end method

.method protected shouldSuppressLoginTos()Z
    .locals 1

    .prologue
    .line 2245
    invoke-virtual {p0}, Lcom/google/android/setupwizard/BaseActivity;->isEduSignin()Z

    move-result v0

    return v0
.end method

.method protected shouldSuppressNameCheck()Z
    .locals 1

    .prologue
    .line 2236
    invoke-virtual {p0}, Lcom/google/android/setupwizard/BaseActivity;->isEduSignin()Z

    move-result v0

    return v0
.end method

.method protected shouldSuppressSimWarning()Z
    .locals 1

    .prologue
    .line 2228
    invoke-virtual {p0}, Lcom/google/android/setupwizard/BaseActivity;->isEduSignin()Z

    move-result v0

    return v0
.end method

.method protected start()V
    .locals 0

    .prologue
    .line 542
    return-void
.end method

.method protected startFirstRunActivity(Landroid/content/Intent;)V
    .locals 3
    .param p1, "intent"    # Landroid/content/Intent;

    .prologue
    .line 1912
    iget-object v0, p0, Lcom/google/android/setupwizard/BaseActivity;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "starting activity "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 1913
    const-string v0, "firstRun"

    invoke-virtual {p0}, Lcom/google/android/setupwizard/BaseActivity;->isFirstRun()Z

    move-result v1

    invoke-virtual {p1, v0, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 1914
    const-string v0, "hasMultipleUsers"

    invoke-virtual {p0}, Lcom/google/android/setupwizard/BaseActivity;->hasMultipleUsers()Z

    move-result v1

    invoke-virtual {p1, v0, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 1915
    const-string v0, "resumeFromUpdate"

    invoke-virtual {p0}, Lcom/google/android/setupwizard/BaseActivity;->isResumeFromUpdate()Z

    move-result v1

    invoke-virtual {p1, v0, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 1916
    const-string v0, "useImmersiveMode"

    const/4 v1, 0x1

    invoke-virtual {p1, v0, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 1917
    invoke-virtual {p0, p1}, Lcom/google/android/setupwizard/BaseActivity;->putThemeExtra(Landroid/content/Intent;)V

    .line 1919
    invoke-virtual {p0, p1}, Lcom/google/android/setupwizard/BaseActivity;->startActivity(Landroid/content/Intent;)V

    .line 1920
    invoke-virtual {p0}, Lcom/google/android/setupwizard/BaseActivity;->getNextTransition()I

    move-result v0

    invoke-virtual {p0, v0}, Lcom/google/android/setupwizard/BaseActivity;->applyForwardTransition(I)V

    .line 1921
    return-void
.end method

.method protected startFirstRunActivityForResult(Landroid/content/Intent;I)V
    .locals 3
    .param p1, "intent"    # Landroid/content/Intent;
    .param p2, "requestCode"    # I

    .prologue
    .line 1924
    iget-object v0, p0, Lcom/google/android/setupwizard/BaseActivity;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "starting activity for result "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-static {p2}, Lcom/google/android/setupwizard/BaseActivity;->getRequestName(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 1925
    const-string v0, "firstRun"

    invoke-virtual {p0}, Lcom/google/android/setupwizard/BaseActivity;->isFirstRun()Z

    move-result v1

    invoke-virtual {p1, v0, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 1926
    const-string v0, "hasMultipleUsers"

    invoke-virtual {p0}, Lcom/google/android/setupwizard/BaseActivity;->hasMultipleUsers()Z

    move-result v1

    invoke-virtual {p1, v0, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 1927
    const-string v0, "resumeFromUpdate"

    invoke-virtual {p0}, Lcom/google/android/setupwizard/BaseActivity;->isResumeFromUpdate()Z

    move-result v1

    invoke-virtual {p1, v0, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 1928
    const-string v0, "useImmersiveMode"

    const/4 v1, 0x1

    invoke-virtual {p1, v0, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 1929
    invoke-virtual {p0, p1}, Lcom/google/android/setupwizard/BaseActivity;->putThemeExtra(Landroid/content/Intent;)V

    .line 1931
    invoke-virtual {p0, p2}, Lcom/google/android/setupwizard/BaseActivity;->setLastActivity(I)V

    .line 1932
    invoke-virtual {p0, p1, p2}, Lcom/google/android/setupwizard/BaseActivity;->startActivityForResult(Landroid/content/Intent;I)V

    .line 1933
    invoke-virtual {p0}, Lcom/google/android/setupwizard/BaseActivity;->getNextTransition()I

    move-result v0

    invoke-virtual {p0, v0}, Lcom/google/android/setupwizard/BaseActivity;->applyForwardTransition(I)V

    .line 1934
    return-void
.end method

.method protected startWelcomeActivity()V
    .locals 6

    .prologue
    .line 1358
    invoke-virtual {p0}, Lcom/google/android/setupwizard/BaseActivity;->getLastActivity()I

    move-result v1

    .line 1359
    .local v1, "lastActivity":I
    iget-object v3, p0, Lcom/google/android/setupwizard/BaseActivity;->TAG:Ljava/lang/String;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "startWelcomeActivity() lastActivity="

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-static {v1}, Lcom/google/android/setupwizard/BaseActivity;->getRequestName(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1363
    invoke-virtual {p0}, Lcom/google/android/setupwizard/BaseActivity;->isPrimaryUser()Z

    move-result v3

    if-eqz v3, :cond_0

    .line 1364
    new-instance v0, Landroid/content/Intent;

    const-class v3, Lcom/google/android/setupwizard/user/WelcomeWrapper;

    invoke-direct {v0, p0, v3}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 1365
    .local v0, "intent":Landroid/content/Intent;
    const/16 v2, 0x2711

    .line 1370
    .local v2, "requestCode":I
    :goto_0
    invoke-virtual {p0, v0, v2}, Lcom/google/android/setupwizard/BaseActivity;->startFirstRunActivityForResult(Landroid/content/Intent;I)V

    .line 1371
    return-void

    .line 1367
    .end local v0    # "intent":Landroid/content/Intent;
    .end local v2    # "requestCode":I
    :cond_0
    new-instance v0, Landroid/content/Intent;

    const-class v3, Lcom/google/android/setupwizard/user/WelcomeUserWrapper;

    invoke-direct {v0, p0, v3}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 1368
    .restart local v0    # "intent":Landroid/content/Intent;
    const/16 v2, 0x2725

    .restart local v2    # "requestCode":I
    goto :goto_0
.end method

.method protected topTask()Ljava/lang/String;
    .locals 6

    .prologue
    .line 2485
    const-string v3, "activity"

    invoke-virtual {p0, v3}, Lcom/google/android/setupwizard/BaseActivity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/ActivityManager;

    .line 2487
    .local v0, "activityManager":Landroid/app/ActivityManager;
    const/4 v3, 0x1

    invoke-virtual {v0, v3}, Landroid/app/ActivityManager;->getRunningTasks(I)Ljava/util/List;

    move-result-object v2

    .line 2488
    .local v2, "list":Ljava/util/List;, "Ljava/util/List<Landroid/app/ActivityManager$RunningTaskInfo;>;"
    const/4 v3, 0x0

    invoke-interface {v2, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/app/ActivityManager$RunningTaskInfo;

    .line 2489
    .local v1, "info":Landroid/app/ActivityManager$RunningTaskInfo;
    if-eqz v1, :cond_0

    iget-object v3, v1, Landroid/app/ActivityManager$RunningTaskInfo;->topActivity:Landroid/content/ComponentName;

    if-nez v3, :cond_2

    .line 2490
    :cond_0
    iget-object v4, p0, Lcom/google/android/setupwizard/BaseActivity;->TAG:Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "topTask info="

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v5, " info.topActivity="

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    if-eqz v1, :cond_1

    iget-object v3, v1, Landroid/app/ActivityManager$RunningTaskInfo;->topActivity:Landroid/content/ComponentName;

    :goto_0
    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v4, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 2492
    const-string v3, ""

    .line 2495
    :goto_1
    return-object v3

    .line 2490
    :cond_1
    const-string v3, "null"

    goto :goto_0

    .line 2495
    :cond_2
    iget-object v3, v1, Landroid/app/ActivityManager$RunningTaskInfo;->topActivity:Landroid/content/ComponentName;

    invoke-virtual {v3}, Landroid/content/ComponentName;->getClassName()Ljava/lang/String;

    move-result-object v3

    goto :goto_1
.end method

.method protected tryEnablingWifi(Z)Z
    .locals 8
    .param p1, "turnOn"    # Z

    .prologue
    const/4 v5, 0x0

    .line 1717
    :try_start_0
    const-string v6, "wifi"

    invoke-static {v6}, Landroid/os/ServiceManager;->getService(Ljava/lang/String;)Landroid/os/IBinder;

    move-result-object v0

    .line 1718
    .local v0, "binder":Landroid/os/IBinder;
    if-eqz v0, :cond_2

    .line 1719
    invoke-static {v0}, Landroid/net/wifi/IWifiManager$Stub;->asInterface(Landroid/os/IBinder;)Landroid/net/wifi/IWifiManager;

    move-result-object v2

    .line 1720
    .local v2, "service":Landroid/net/wifi/IWifiManager;
    invoke-interface {v2}, Landroid/net/wifi/IWifiManager;->getWifiEnabledState()I

    move-result v4

    .line 1721
    .local v4, "wifiState":I
    const/4 v6, 0x3

    if-ne v4, v6, :cond_1

    const/4 v3, 0x1

    .line 1722
    .local v3, "wifiOn":Z
    :goto_0
    if-eq p1, v3, :cond_0

    .line 1723
    invoke-interface {v2, p1}, Landroid/net/wifi/IWifiManager;->setWifiEnabled(Z)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 1730
    .end local v0    # "binder":Landroid/os/IBinder;
    .end local v2    # "service":Landroid/net/wifi/IWifiManager;
    .end local v3    # "wifiOn":Z
    .end local v4    # "wifiState":I
    .end local p1    # "turnOn":Z
    :cond_0
    :goto_1
    return p1

    .restart local v0    # "binder":Landroid/os/IBinder;
    .restart local v2    # "service":Landroid/net/wifi/IWifiManager;
    .restart local v4    # "wifiState":I
    .restart local p1    # "turnOn":Z
    :cond_1
    move v3, v5

    .line 1721
    goto :goto_0

    .line 1727
    .end local v0    # "binder":Landroid/os/IBinder;
    .end local v2    # "service":Landroid/net/wifi/IWifiManager;
    .end local v4    # "wifiState":I
    :catch_0
    move-exception v1

    .line 1728
    .local v1, "e":Landroid/os/RemoteException;
    iget-object v6, p0, Lcom/google/android/setupwizard/BaseActivity;->TAG:Ljava/lang/String;

    invoke-virtual {v1}, Landroid/os/RemoteException;->getMessage()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .end local v1    # "e":Landroid/os/RemoteException;
    :cond_2
    move p1, v5

    .line 1730
    goto :goto_1
.end method

.method protected unlockRotation()V
    .locals 5

    .prologue
    .line 2170
    iget-object v3, p0, Lcom/google/android/setupwizard/BaseActivity;->TAG:Ljava/lang/String;

    const-string v4, "Unlocking Rotation"

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 2171
    const-string v3, "SetupWizardPrefs"

    const/4 v4, 0x4

    invoke-virtual {p0, v3, v4}, Lcom/google/android/setupwizard/BaseActivity;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v1

    .line 2173
    .local v1, "prefs":Landroid/content/SharedPreferences;
    const-string v3, "setupwizard.accelerometer_rotation"

    const/4 v4, 0x1

    invoke-interface {v1, v3, v4}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v0

    .line 2175
    .local v0, "accelerometerRotation":I
    const-string v3, "setupwizard.user_rotation"

    const/4 v4, 0x0

    invoke-interface {v1, v3, v4}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v2

    .line 2178
    .local v2, "userRotation":I
    invoke-virtual {p0}, Lcom/google/android/setupwizard/BaseActivity;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v3

    const-string v4, "accelerometer_rotation"

    invoke-static {v3, v4, v0}, Landroid/provider/Settings$System;->putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z

    .line 2180
    invoke-virtual {p0}, Lcom/google/android/setupwizard/BaseActivity;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v3

    const-string v4, "user_rotation"

    invoke-static {v3, v4, v2}, Landroid/provider/Settings$System;->putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z

    .line 2182
    return-void
.end method

.method protected wifiScreenShown()Z
    .locals 3

    .prologue
    .line 1854
    invoke-virtual {p0}, Lcom/google/android/setupwizard/BaseActivity;->getLocalPreferences()Landroid/content/SharedPreferences;

    move-result-object v0

    const-string v1, "wifiScreenShown"

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    return v0
.end method
