.class public Lcom/google/android/setupwizard/WizardAction;
.super Ljava/lang/Object;
.source "WizardAction.java"

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/google/android/setupwizard/WizardAction;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private final mId:Ljava/lang/String;

.field private final mTransitions:Lcom/google/android/setupwizard/WizardTransitions;

.field private final mUri:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 186
    new-instance v0, Lcom/google/android/setupwizard/WizardAction$1;

    invoke-direct {v0}, Lcom/google/android/setupwizard/WizardAction$1;-><init>()V

    sput-object v0, Lcom/google/android/setupwizard/WizardAction;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;Lcom/google/android/setupwizard/WizardTransitions;)V
    .locals 2
    .param p1, "id"    # Ljava/lang/String;
    .param p2, "uri"    # Ljava/lang/String;
    .param p3, "transitions"    # Lcom/google/android/setupwizard/WizardTransitions;

    .prologue
    .line 57
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 58
    if-nez p3, :cond_0

    .line 59
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "transitions of WizardAction cannot be null"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 61
    :cond_0
    iput-object p1, p0, Lcom/google/android/setupwizard/WizardAction;->mId:Ljava/lang/String;

    .line 62
    iput-object p2, p0, Lcom/google/android/setupwizard/WizardAction;->mUri:Ljava/lang/String;

    .line 63
    iput-object p3, p0, Lcom/google/android/setupwizard/WizardAction;->mTransitions:Lcom/google/android/setupwizard/WizardTransitions;

    .line 64
    return-void
.end method

.method private static parseResult(Lcom/google/android/setupwizard/WizardTransitions;Lorg/xmlpull/v1/XmlPullParser;)V
    .locals 4
    .param p0, "transitions"    # Lcom/google/android/setupwizard/WizardTransitions;
    .param p1, "parser"    # Lorg/xmlpull/v1/XmlPullParser;

    .prologue
    .line 115
    const-string v2, "http://schemas.android.com/apk/res/com.google.android.setupwizard"

    const-string v3, "resultCode"

    invoke-interface {p1, v2, v3}, Lorg/xmlpull/v1/XmlPullParser;->getAttributeValue(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 116
    .local v1, "resultString":Ljava/lang/String;
    const-string v2, "http://schemas.android.com/apk/res/com.google.android.setupwizard"

    const-string v3, "action"

    invoke-interface {p1, v2, v3}, Lorg/xmlpull/v1/XmlPullParser;->getAttributeValue(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 117
    .local v0, "action":Ljava/lang/String;
    if-nez v1, :cond_0

    .line 118
    invoke-virtual {p0, v0}, Lcom/google/android/setupwizard/WizardTransitions;->setDefaultAction(Ljava/lang/String;)V

    .line 122
    :goto_0
    return-void

    .line 120
    :cond_0
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-virtual {p0, v2, v0}, Lcom/google/android/setupwizard/WizardTransitions;->put(ILjava/lang/Object;)V

    goto :goto_0
.end method

.method public static parseWizardAction(Lorg/xmlpull/v1/XmlPullParser;)Lcom/google/android/setupwizard/WizardAction;
    .locals 9
    .param p0, "parser"    # Lorg/xmlpull/v1/XmlPullParser;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/xmlpull/v1/XmlPullParserException;,
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v8, 0x3

    .line 83
    const/4 v6, 0x0

    const-string v7, "id"

    invoke-interface {p0, v6, v7}, Lorg/xmlpull/v1/XmlPullParser;->getAttributeValue(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 84
    .local v0, "id":Ljava/lang/String;
    const-string v6, "http://schemas.android.com/apk/res/com.google.android.setupwizard"

    const-string v7, "uri"

    invoke-interface {p0, v6, v7}, Lorg/xmlpull/v1/XmlPullParser;->getAttributeValue(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    .line 85
    .local v5, "uri":Ljava/lang/String;
    new-instance v3, Lcom/google/android/setupwizard/WizardTransitions;

    invoke-direct {v3}, Lcom/google/android/setupwizard/WizardTransitions;-><init>()V

    .line 86
    .local v3, "transitions":Lcom/google/android/setupwizard/WizardTransitions;
    if-nez v0, :cond_0

    .line 87
    new-instance v6, Lorg/xmlpull/v1/XmlPullParserException;

    const-string v7, "WizardAction must define an id"

    invoke-direct {v6, v7}, Lorg/xmlpull/v1/XmlPullParserException;-><init>(Ljava/lang/String;)V

    throw v6

    .line 88
    :cond_0
    if-nez v5, :cond_1

    .line 89
    new-instance v6, Lorg/xmlpull/v1/XmlPullParserException;

    const-string v7, "WizardAction must define an intent URI"

    invoke-direct {v6, v7}, Lorg/xmlpull/v1/XmlPullParserException;-><init>(Ljava/lang/String;)V

    throw v6

    .line 94
    :cond_1
    invoke-interface {p0}, Lorg/xmlpull/v1/XmlPullParser;->getDepth()I

    move-result v1

    .line 97
    .local v1, "innerDepth":I
    :cond_2
    :goto_0
    invoke-interface {p0}, Lorg/xmlpull/v1/XmlPullParser;->next()I

    move-result v4

    .local v4, "type":I
    const/4 v6, 0x1

    if-eq v4, v6, :cond_5

    if-ne v4, v8, :cond_3

    invoke-interface {p0}, Lorg/xmlpull/v1/XmlPullParser;->getDepth()I

    move-result v6

    if-le v6, v1, :cond_5

    .line 98
    :cond_3
    if-eq v4, v8, :cond_2

    const/4 v6, 0x4

    if-eq v4, v6, :cond_2

    .line 104
    invoke-interface {p0}, Lorg/xmlpull/v1/XmlPullParser;->getName()Ljava/lang/String;

    move-result-object v2

    .line 105
    .local v2, "innerNodeName":Ljava/lang/String;
    const-string v6, "result"

    invoke-virtual {v2, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_4

    .line 106
    invoke-static {v3, p0}, Lcom/google/android/setupwizard/WizardAction;->parseResult(Lcom/google/android/setupwizard/WizardTransitions;Lorg/xmlpull/v1/XmlPullParser;)V

    goto :goto_0

    .line 108
    :cond_4
    invoke-static {p0}, Lcom/android/internal/util/XmlUtils;->skipCurrentTag(Lorg/xmlpull/v1/XmlPullParser;)V

    goto :goto_0

    .line 111
    .end local v2    # "innerNodeName":Ljava/lang/String;
    :cond_5
    new-instance v6, Lcom/google/android/setupwizard/WizardAction;

    invoke-direct {v6, v0, v5, v3}, Lcom/google/android/setupwizard/WizardAction;-><init>(Ljava/lang/String;Ljava/lang/String;Lcom/google/android/setupwizard/WizardTransitions;)V

    return-object v6
.end method


# virtual methods
.method public describeContents()I
    .locals 1

    .prologue
    .line 176
    const/4 v0, 0x0

    return v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 4
    .param p1, "obj"    # Ljava/lang/Object;

    .prologue
    const/4 v1, 0x0

    .line 155
    instance-of v2, p1, Lcom/google/android/setupwizard/WizardAction;

    if-eqz v2, :cond_0

    move-object v0, p1

    .line 156
    check-cast v0, Lcom/google/android/setupwizard/WizardAction;

    .line 157
    .local v0, "o":Lcom/google/android/setupwizard/WizardAction;
    iget-object v2, p0, Lcom/google/android/setupwizard/WizardAction;->mId:Ljava/lang/String;

    if-eqz v2, :cond_1

    iget-object v2, p0, Lcom/google/android/setupwizard/WizardAction;->mId:Ljava/lang/String;

    iget-object v3, v0, Lcom/google/android/setupwizard/WizardAction;->mId:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    :goto_0
    iget-object v2, p0, Lcom/google/android/setupwizard/WizardAction;->mUri:Ljava/lang/String;

    if-eqz v2, :cond_2

    iget-object v2, p0, Lcom/google/android/setupwizard/WizardAction;->mUri:Ljava/lang/String;

    iget-object v3, v0, Lcom/google/android/setupwizard/WizardAction;->mUri:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    :goto_1
    iget-object v2, p0, Lcom/google/android/setupwizard/WizardAction;->mTransitions:Lcom/google/android/setupwizard/WizardTransitions;

    iget-object v3, v0, Lcom/google/android/setupwizard/WizardAction;->mTransitions:Lcom/google/android/setupwizard/WizardTransitions;

    invoke-virtual {v2, v3}, Lcom/google/android/setupwizard/WizardTransitions;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    const/4 v1, 0x1

    .line 161
    .end local v0    # "o":Lcom/google/android/setupwizard/WizardAction;
    :cond_0
    return v1

    .line 157
    .restart local v0    # "o":Lcom/google/android/setupwizard/WizardAction;
    :cond_1
    iget-object v2, v0, Lcom/google/android/setupwizard/WizardAction;->mId:Ljava/lang/String;

    if-nez v2, :cond_0

    goto :goto_0

    :cond_2
    iget-object v2, v0, Lcom/google/android/setupwizard/WizardAction;->mUri:Ljava/lang/String;

    if-nez v2, :cond_0

    goto :goto_1
.end method

.method public getId()Ljava/lang/String;
    .locals 1

    .prologue
    .line 142
    iget-object v0, p0, Lcom/google/android/setupwizard/WizardAction;->mId:Ljava/lang/String;

    return-object v0
.end method

.method protected getIntent()Landroid/content/Intent;
    .locals 5

    .prologue
    .line 132
    const/4 v1, 0x0

    .line 134
    .local v1, "intent":Landroid/content/Intent;
    :try_start_0
    iget-object v2, p0, Lcom/google/android/setupwizard/WizardAction;->mUri:Ljava/lang/String;

    const/4 v3, 0x1

    invoke-static {v2, v3}, Landroid/content/Intent;->parseUri(Ljava/lang/String;I)Landroid/content/Intent;
    :try_end_0
    .catch Ljava/net/URISyntaxException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    .line 138
    :goto_0
    return-object v1

    .line 135
    :catch_0
    move-exception v0

    .line 136
    .local v0, "e":Ljava/net/URISyntaxException;
    const-string v2, "WizardAction"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "getIntent() FAIL due to bad URI: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p0, Lcom/google/android/setupwizard/WizardAction;->mUri:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method public getNextAction(I)Ljava/lang/String;
    .locals 1
    .param p1, "resultCode"    # I

    .prologue
    .line 150
    iget-object v0, p0, Lcom/google/android/setupwizard/WizardAction;->mTransitions:Lcom/google/android/setupwizard/WizardTransitions;

    invoke-virtual {v0, p1}, Lcom/google/android/setupwizard/WizardTransitions;->getAction(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getUri()Ljava/lang/String;
    .locals 1

    .prologue
    .line 146
    iget-object v0, p0, Lcom/google/android/setupwizard/WizardAction;->mUri:Ljava/lang/String;

    return-object v0
.end method

.method public hashCode()I
    .locals 3

    .prologue
    .line 166
    const/4 v0, 0x3

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    iget-object v2, p0, Lcom/google/android/setupwizard/WizardAction;->mId:Ljava/lang/String;

    aput-object v2, v0, v1

    const/4 v1, 0x1

    iget-object v2, p0, Lcom/google/android/setupwizard/WizardAction;->mUri:Ljava/lang/String;

    aput-object v2, v0, v1

    const/4 v1, 0x2

    iget-object v2, p0, Lcom/google/android/setupwizard/WizardAction;->mTransitions:Lcom/google/android/setupwizard/WizardTransitions;

    aput-object v2, v0, v1

    invoke-static {v0}, Ljava/util/Objects;->hash([Ljava/lang/Object;)I

    move-result v0

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    .prologue
    .line 171
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "WizardAction (id="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/setupwizard/WizardAction;->mId:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " uri="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/setupwizard/WizardAction;->mUri:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " transitions="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/setupwizard/WizardAction;->mTransitions:Lcom/google/android/setupwizard/WizardTransitions;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ")"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 1
    .param p1, "dest"    # Landroid/os/Parcel;
    .param p2, "flags"    # I

    .prologue
    .line 181
    iget-object v0, p0, Lcom/google/android/setupwizard/WizardAction;->mUri:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 182
    iget-object v0, p0, Lcom/google/android/setupwizard/WizardAction;->mId:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 183
    iget-object v0, p0, Lcom/google/android/setupwizard/WizardAction;->mTransitions:Lcom/google/android/setupwizard/WizardTransitions;

    invoke-virtual {p1, v0, p2}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    .line 184
    return-void
.end method
