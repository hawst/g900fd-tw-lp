.class final Lcom/google/android/setupwizard/WizardScript$1;
.super Ljava/lang/Object;
.source "WizardScript.java"

# interfaces
.implements Landroid/os/Parcelable$Creator;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/setupwizard/WizardScript;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Landroid/os/Parcelable$Creator",
        "<",
        "Lcom/google/android/setupwizard/WizardScript;",
        ">;"
    }
.end annotation


# direct methods
.method constructor <init>()V
    .locals 0

    .prologue
    .line 213
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public createFromParcel(Landroid/os/Parcel;)Lcom/google/android/setupwizard/WizardScript;
    .locals 6
    .param p1, "source"    # Landroid/os/Parcel;

    .prologue
    .line 216
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v3

    .line 218
    .local v3, "firstActionId":Ljava/lang/String;
    new-instance v2, Ljava/util/HashMap;

    invoke-direct {v2}, Ljava/util/HashMap;-><init>()V

    .line 219
    .local v2, "actions":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/String;Lcom/google/android/setupwizard/WizardAction;>;"
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 220
    .local v1, "actionList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/google/android/setupwizard/WizardAction;>;"
    sget-object v5, Lcom/google/android/setupwizard/WizardAction;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-virtual {p1, v1, v5}, Landroid/os/Parcel;->readTypedList(Ljava/util/List;Landroid/os/Parcelable$Creator;)V

    .line 221
    invoke-virtual {v1}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v4

    .local v4, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_0

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/setupwizard/WizardAction;

    .line 222
    .local v0, "action":Lcom/google/android/setupwizard/WizardAction;
    invoke-virtual {v0}, Lcom/google/android/setupwizard/WizardAction;->getId()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v2, v5, v0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    .line 224
    .end local v0    # "action":Lcom/google/android/setupwizard/WizardAction;
    :cond_0
    new-instance v5, Lcom/google/android/setupwizard/WizardScript;

    invoke-direct {v5, v2, v3}, Lcom/google/android/setupwizard/WizardScript;-><init>(Ljava/util/Map;Ljava/lang/String;)V

    return-object v5
.end method

.method public bridge synthetic createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;
    .locals 1
    .param p1, "x0"    # Landroid/os/Parcel;

    .prologue
    .line 213
    invoke-virtual {p0, p1}, Lcom/google/android/setupwizard/WizardScript$1;->createFromParcel(Landroid/os/Parcel;)Lcom/google/android/setupwizard/WizardScript;

    move-result-object v0

    return-object v0
.end method

.method public newArray(I)[Lcom/google/android/setupwizard/WizardScript;
    .locals 1
    .param p1, "size"    # I

    .prologue
    .line 229
    new-array v0, p1, [Lcom/google/android/setupwizard/WizardScript;

    return-object v0
.end method

.method public bridge synthetic newArray(I)[Ljava/lang/Object;
    .locals 1
    .param p1, "x0"    # I

    .prologue
    .line 213
    invoke-virtual {p0, p1}, Lcom/google/android/setupwizard/WizardScript$1;->newArray(I)[Lcom/google/android/setupwizard/WizardScript;

    move-result-object v0

    return-object v0
.end method
