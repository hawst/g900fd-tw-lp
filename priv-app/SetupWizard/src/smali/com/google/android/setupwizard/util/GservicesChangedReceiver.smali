.class public Lcom/google/android/setupwizard/util/GservicesChangedReceiver;
.super Landroid/content/BroadcastReceiver;
.source "GservicesChangedReceiver.java"


# static fields
.field public static final ANDROID_ID:Lcom/google/android/setupwizard/util/GservicesValue;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/setupwizard/util/GservicesValue",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public static final FILTER_CHECKIN_SUCCESS:Landroid/content/IntentFilter;

.field private static final INTENT_CHECKIN_SUCCESS:Landroid/content/Intent;

.field private static mAccountCheckin:Z

.field private static mAlreadyConnected:Z

.field private static mWaitForAccountCheckin:Z


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 36
    new-instance v0, Landroid/content/Intent;

    const-string v1, "com.google.android.setupwizard.CHECKIN_SUCCESS"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    sput-object v0, Lcom/google/android/setupwizard/util/GservicesChangedReceiver;->INTENT_CHECKIN_SUCCESS:Landroid/content/Intent;

    .line 37
    new-instance v0, Landroid/content/IntentFilter;

    const-string v1, "com.google.android.setupwizard.CHECKIN_SUCCESS"

    invoke-direct {v0, v1}, Landroid/content/IntentFilter;-><init>(Ljava/lang/String;)V

    sput-object v0, Lcom/google/android/setupwizard/util/GservicesChangedReceiver;->FILTER_CHECKIN_SUCCESS:Landroid/content/IntentFilter;

    .line 40
    const-string v1, "android_id"

    const/4 v0, 0x0

    check-cast v0, Ljava/lang/String;

    invoke-static {v1, v0}, Lcom/google/android/setupwizard/util/GservicesValue;->value(Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/setupwizard/util/GservicesValue;

    move-result-object v0

    sput-object v0, Lcom/google/android/setupwizard/util/GservicesChangedReceiver;->ANDROID_ID:Lcom/google/android/setupwizard/util/GservicesValue;

    .line 44
    sput-boolean v2, Lcom/google/android/setupwizard/util/GservicesChangedReceiver;->mAlreadyConnected:Z

    .line 46
    sput-boolean v2, Lcom/google/android/setupwizard/util/GservicesChangedReceiver;->mWaitForAccountCheckin:Z

    .line 47
    sput-boolean v2, Lcom/google/android/setupwizard/util/GservicesChangedReceiver;->mAccountCheckin:Z

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 32
    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    return-void
.end method

.method public static hasAccountCheckin()Z
    .locals 1

    .prologue
    .line 75
    sget-boolean v0, Lcom/google/android/setupwizard/util/GservicesChangedReceiver;->mAccountCheckin:Z

    return v0
.end method

.method public static hasGservicesChanged()Z
    .locals 1

    .prologue
    .line 71
    sget-boolean v0, Lcom/google/android/setupwizard/util/GservicesChangedReceiver;->mAlreadyConnected:Z

    return v0
.end method

.method private isConnected()Z
    .locals 2

    .prologue
    .line 100
    sget-object v1, Lcom/google/android/setupwizard/util/GservicesChangedReceiver;->ANDROID_ID:Lcom/google/android/setupwizard/util/GservicesValue;

    invoke-virtual {v1}, Lcom/google/android/setupwizard/util/GservicesValue;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 101
    .local v0, "digest":Ljava/lang/String;
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v1

    if-lez v1, :cond_0

    const/4 v1, 0x1

    :goto_0
    return v1

    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public static resetForTest()V
    .locals 1

    .prologue
    .line 106
    const/4 v0, 0x0

    sput-boolean v0, Lcom/google/android/setupwizard/util/GservicesChangedReceiver;->mAlreadyConnected:Z

    .line 107
    return-void
.end method

.method public static startWaitingForAccountCheckin()V
    .locals 1

    .prologue
    .line 66
    const/4 v0, 0x1

    sput-boolean v0, Lcom/google/android/setupwizard/util/GservicesChangedReceiver;->mWaitForAccountCheckin:Z

    .line 67
    return-void
.end method

.method public static tickleCheckinService(Landroid/content/Context;)V
    .locals 1
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 79
    const/4 v0, 0x0

    invoke-static {p0, v0}, Lcom/google/android/setupwizard/util/GservicesChangedReceiver;->tickleCheckinService(Landroid/content/Context;Z)V

    .line 80
    return-void
.end method

.method public static tickleCheckinService(Landroid/content/Context;Z)V
    .locals 3
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "force"    # Z

    .prologue
    .line 83
    if-nez p1, :cond_0

    invoke-static {}, Lcom/google/android/setupwizard/util/GservicesChangedReceiver;->hasGservicesChanged()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 97
    :goto_0
    return-void

    .line 90
    :cond_0
    new-instance v0, Landroid/content/Intent;

    const-string v1, "android.server.checkin.CHECKIN"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 91
    .local v0, "intent":Landroid/content/Intent;
    const/high16 v1, 0x10000000

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    .line 94
    const-string v1, "force"

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 96
    invoke-virtual {p0, v0}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V

    goto :goto_0
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 4
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "intent"    # Landroid/content/Intent;

    .prologue
    const/4 v3, 0x1

    .line 52
    const-string v1, "success"

    const/4 v2, 0x0

    invoke-virtual {p2, v1, v2}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v0

    .line 53
    .local v0, "success":Z
    sget-boolean v1, Lcom/google/android/setupwizard/util/GservicesChangedReceiver;->mAlreadyConnected:Z

    if-nez v1, :cond_0

    if-eqz v0, :cond_0

    invoke-direct {p0}, Lcom/google/android/setupwizard/util/GservicesChangedReceiver;->isConnected()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 54
    const-string v1, "SetupWizard"

    const-string v2, "Connected to Gservices successfully"

    invoke-static {v1, v2}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 55
    sput-boolean v3, Lcom/google/android/setupwizard/util/GservicesChangedReceiver;->mAlreadyConnected:Z

    .line 56
    sget-object v1, Lcom/google/android/setupwizard/util/GservicesChangedReceiver;->INTENT_CHECKIN_SUCCESS:Landroid/content/Intent;

    invoke-virtual {p1, v1}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V

    .line 58
    :cond_0
    sget-boolean v1, Lcom/google/android/setupwizard/util/GservicesChangedReceiver;->mWaitForAccountCheckin:Z

    if-eqz v1, :cond_1

    sget-boolean v1, Lcom/google/android/setupwizard/util/GservicesChangedReceiver;->mAccountCheckin:Z

    if-nez v1, :cond_1

    if-eqz v0, :cond_1

    invoke-direct {p0}, Lcom/google/android/setupwizard/util/GservicesChangedReceiver;->isConnected()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 59
    const-string v1, "SetupWizard"

    const-string v2, "Received checkin for account"

    invoke-static {v1, v2}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 60
    sput-boolean v3, Lcom/google/android/setupwizard/util/GservicesChangedReceiver;->mAccountCheckin:Z

    .line 61
    sget-object v1, Lcom/google/android/setupwizard/util/GservicesChangedReceiver;->INTENT_CHECKIN_SUCCESS:Landroid/content/Intent;

    invoke-virtual {p1, v1}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V

    .line 63
    :cond_1
    return-void
.end method
