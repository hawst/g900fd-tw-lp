.class Lcom/google/android/setupwizard/carrier/PhoneMonitor$1;
.super Landroid/content/BroadcastReceiver;
.source "PhoneMonitor.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/setupwizard/carrier/PhoneMonitor;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/setupwizard/carrier/PhoneMonitor;


# direct methods
.method constructor <init>(Lcom/google/android/setupwizard/carrier/PhoneMonitor;)V
    .locals 0

    .prologue
    .line 367
    iput-object p1, p0, Lcom/google/android/setupwizard/carrier/PhoneMonitor$1;->this$0:Lcom/google/android/setupwizard/carrier/PhoneMonitor;

    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    return-void
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 3
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "intent"    # Landroid/content/Intent;

    .prologue
    .line 370
    iget-object v0, p0, Lcom/google/android/setupwizard/carrier/PhoneMonitor$1;->this$0:Lcom/google/android/setupwizard/carrier/PhoneMonitor;

    const-string v1, "mockOtaspMode"

    const/4 v2, -0x1

    invoke-virtual {p2, v1, v2}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v1

    # setter for: Lcom/google/android/setupwizard/carrier/PhoneMonitor;->mMockOtaspMode:I
    invoke-static {v0, v1}, Lcom/google/android/setupwizard/carrier/PhoneMonitor;->access$002(Lcom/google/android/setupwizard/carrier/PhoneMonitor;I)I

    .line 371
    iget-object v0, p0, Lcom/google/android/setupwizard/carrier/PhoneMonitor$1;->this$0:Lcom/google/android/setupwizard/carrier/PhoneMonitor;

    # getter for: Lcom/google/android/setupwizard/carrier/PhoneMonitor;->mPhoneStateListener:Landroid/telephony/PhoneStateListener;
    invoke-static {v0}, Lcom/google/android/setupwizard/carrier/PhoneMonitor;->access$200(Lcom/google/android/setupwizard/carrier/PhoneMonitor;)Landroid/telephony/PhoneStateListener;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/setupwizard/carrier/PhoneMonitor$1;->this$0:Lcom/google/android/setupwizard/carrier/PhoneMonitor;

    # invokes: Lcom/google/android/setupwizard/carrier/PhoneMonitor;->getOtaspModeImpl()I
    invoke-static {v1}, Lcom/google/android/setupwizard/carrier/PhoneMonitor;->access$100(Lcom/google/android/setupwizard/carrier/PhoneMonitor;)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/telephony/PhoneStateListener;->onOtaspChanged(I)V

    .line 372
    return-void
.end method
