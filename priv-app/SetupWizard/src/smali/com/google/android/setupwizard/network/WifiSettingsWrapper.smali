.class public Lcom/google/android/setupwizard/network/WifiSettingsWrapper;
.super Lcom/google/android/setupwizard/SubactivityWrapper;
.source "WifiSettingsWrapper.java"


# instance fields
.field private mRequireUserNetworkSelection:Z


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 29
    invoke-direct {p0}, Lcom/google/android/setupwizard/SubactivityWrapper;-><init>()V

    .line 55
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/setupwizard/network/WifiSettingsWrapper;->mRequireUserNetworkSelection:Z

    return-void
.end method


# virtual methods
.method protected getPreviousTransition()I
    .locals 1

    .prologue
    .line 130
    const/4 v0, 0x1

    return v0
.end method

.method protected onActivityResult(IILandroid/content/Intent;)V
    .locals 1
    .param p1, "requestCode"    # I
    .param p2, "resultCode"    # I
    .param p3, "data"    # Landroid/content/Intent;

    .prologue
    .line 109
    invoke-virtual {p0}, Lcom/google/android/setupwizard/network/WifiSettingsWrapper;->requestCode()I

    move-result v0

    if-eq p1, v0, :cond_0

    if-nez p2, :cond_0

    .line 112
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/setupwizard/network/WifiSettingsWrapper;->mRequireUserNetworkSelection:Z

    .line 114
    :cond_0
    invoke-super {p0, p1, p2, p3}, Lcom/google/android/setupwizard/SubactivityWrapper;->onActivityResult(IILandroid/content/Intent;)V

    .line 115
    return-void
.end method

.method protected onRestoreInstanceState(Landroid/os/Bundle;)V
    .locals 1
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 59
    const-string v0, "wifi_require_user_network_selection"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/google/android/setupwizard/network/WifiSettingsWrapper;->mRequireUserNetworkSelection:Z

    .line 61
    invoke-super {p0, p1}, Lcom/google/android/setupwizard/SubactivityWrapper;->onRestoreInstanceState(Landroid/os/Bundle;)V

    .line 62
    return-void
.end method

.method protected onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 2
    .param p1, "outState"    # Landroid/os/Bundle;

    .prologue
    .line 66
    const-string v0, "wifi_require_user_network_selection"

    iget-boolean v1, p0, Lcom/google/android/setupwizard/network/WifiSettingsWrapper;->mRequireUserNetworkSelection:Z

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 67
    invoke-super {p0, p1}, Lcom/google/android/setupwizard/SubactivityWrapper;->onSaveInstanceState(Landroid/os/Bundle;)V

    .line 68
    return-void
.end method

.method protected onSubactivityLaunch()Landroid/content/Intent;
    .locals 6

    .prologue
    const/4 v0, 0x0

    const/4 v3, 0x1

    .line 77
    iget-object v4, p0, Lcom/google/android/setupwizard/network/WifiSettingsWrapper;->TAG:Ljava/lang/String;

    const-string v5, "onSubactivityLaunch"

    invoke-static {v4, v5}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 81
    invoke-virtual {p0}, Lcom/google/android/setupwizard/network/WifiSettingsWrapper;->isEduSignin()Z

    move-result v4

    if-eqz v4, :cond_1

    invoke-virtual {p0}, Lcom/google/android/setupwizard/network/WifiSettingsWrapper;->isNetworkConnected()Z

    move-result v4

    if-eqz v4, :cond_1

    .line 82
    const/4 v2, 0x0

    .line 104
    :cond_0
    :goto_0
    return-object v2

    .line 88
    :cond_1
    const-string v4, "SetupWizardPrefs"

    const/4 v5, 0x4

    invoke-virtual {p0, v4, v5}, Lcom/google/android/setupwizard/network/WifiSettingsWrapper;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v4

    const-string v5, "connection_status"

    invoke-interface {v4, v5, v0}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v1

    .line 91
    .local v1, "hasValidConnection":Z
    invoke-virtual {p0, v3}, Lcom/google/android/setupwizard/network/WifiSettingsWrapper;->tryEnablingWifi(Z)Z

    .line 93
    invoke-virtual {p0}, Lcom/google/android/setupwizard/network/WifiSettingsWrapper;->isWifiRequired()Z

    move-result v4

    if-nez v4, :cond_3

    invoke-virtual {p0}, Lcom/google/android/setupwizard/network/WifiSettingsWrapper;->isNetworkRequired()Z

    move-result v4

    if-eqz v4, :cond_2

    if-eqz v1, :cond_3

    :cond_2
    move v0, v3

    .line 95
    .local v0, "allowSkip":Z
    :cond_3
    new-instance v2, Landroid/content/Intent;

    const-string v4, "com.android.net.wifi.SETUP_WIFI_NETWORK"

    invoke-direct {v2, v4}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 96
    .local v2, "launchWifi":Landroid/content/Intent;
    const-string v4, "allowSkip"

    invoke-virtual {v2, v4, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 97
    const-string v4, "wifi_auto_finish_on_connect"

    invoke-virtual {v2, v4, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 98
    const-string v3, "wifi_require_user_network_selection"

    iget-boolean v4, p0, Lcom/google/android/setupwizard/network/WifiSettingsWrapper;->mRequireUserNetworkSelection:Z

    invoke-virtual {v2, v3, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 100
    invoke-virtual {p0}, Lcom/google/android/setupwizard/network/WifiSettingsWrapper;->isUsingWizardManager()Z

    move-result v3

    if-eqz v3, :cond_0

    .line 101
    const/high16 v3, 0x2000000

    invoke-virtual {v2, v3}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    goto :goto_0
.end method

.method protected onSubactivityResult(IILandroid/content/Intent;)V
    .locals 3
    .param p1, "requestCode"    # I
    .param p2, "resultCode"    # I
    .param p3, "data"    # Landroid/content/Intent;

    .prologue
    .line 119
    if-eqz p2, :cond_0

    .line 123
    invoke-virtual {p0}, Lcom/google/android/setupwizard/network/WifiSettingsWrapper;->getLocalPreferences()Landroid/content/SharedPreferences;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    const-string v1, "wifiScreenShown"

    const/4 v2, 0x1

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->apply()V

    .line 125
    :cond_0
    invoke-super {p0, p1, p2, p3}, Lcom/google/android/setupwizard/SubactivityWrapper;->onSubactivityResult(IILandroid/content/Intent;)V

    .line 126
    return-void
.end method

.method protected requestCode()I
    .locals 1

    .prologue
    .line 72
    const/16 v0, 0x2714

    return v0
.end method
