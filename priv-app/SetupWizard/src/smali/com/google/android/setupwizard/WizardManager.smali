.class public Lcom/google/android/setupwizard/WizardManager;
.super Landroid/app/Activity;
.source "WizardManager.java"


# static fields
.field private static sLoadedScripts:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Lcom/google/android/setupwizard/WizardScript;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 43
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    sput-object v0, Lcom/google/android/setupwizard/WizardManager;->sLoadedScripts:Ljava/util/HashMap;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 39
    invoke-direct {p0}, Landroid/app/Activity;-><init>()V

    return-void
.end method

.method private addExtrasToIntent(Landroid/content/Intent;)V
    .locals 2
    .param p1, "intent"    # Landroid/content/Intent;

    .prologue
    const/4 v1, 0x1

    .line 169
    const-string v0, "useImmersiveMode"

    invoke-virtual {p1, v0, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 170
    const-string v0, "firstRun"

    invoke-virtual {p1, v0, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 171
    const-string v0, "theme"

    invoke-direct {p0}, Lcom/google/android/setupwizard/WizardManager;->getThemeName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 172
    return-void
.end method

.method private disableComponent(Ljava/lang/String;)V
    .locals 4
    .param p1, "name"    # Ljava/lang/String;

    .prologue
    .line 223
    invoke-virtual {p0}, Lcom/google/android/setupwizard/WizardManager;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v0

    .line 224
    .local v0, "pm":Landroid/content/pm/PackageManager;
    new-instance v1, Landroid/content/ComponentName;

    invoke-direct {v1, p0, p1}, Landroid/content/ComponentName;-><init>(Landroid/content/Context;Ljava/lang/String;)V

    const/4 v2, 0x2

    const/4 v3, 0x1

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/pm/PackageManager;->setComponentEnabledSetting(Landroid/content/ComponentName;II)V

    .line 227
    return-void
.end method

.method private getScript(Ljava/lang/String;)Lcom/google/android/setupwizard/WizardScript;
    .locals 2
    .param p1, "scriptUri"    # Ljava/lang/String;

    .prologue
    .line 214
    sget-object v1, Lcom/google/android/setupwizard/WizardManager;->sLoadedScripts:Ljava/util/HashMap;

    invoke-virtual {v1, p1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/setupwizard/WizardScript;

    .line 215
    .local v0, "script":Lcom/google/android/setupwizard/WizardScript;
    if-nez v0, :cond_0

    .line 216
    invoke-static {p0, p1}, Lcom/google/android/setupwizard/WizardScript;->loadFromUri(Landroid/content/Context;Ljava/lang/String;)Lcom/google/android/setupwizard/WizardScript;

    move-result-object v0

    .line 217
    sget-object v1, Lcom/google/android/setupwizard/WizardManager;->sLoadedScripts:Ljava/util/HashMap;

    invoke-virtual {v1, p1, v0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 219
    :cond_0
    return-object v0
.end method

.method private getThemeName()Ljava/lang/String;
    .locals 5

    .prologue
    .line 175
    invoke-virtual {p0}, Lcom/google/android/setupwizard/WizardManager;->getIntent()Landroid/content/Intent;

    move-result-object v3

    const-string v4, "theme"

    invoke-virtual {v3, v4}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 176
    .local v0, "intentThemeName":Ljava/lang/String;
    if-eqz v0, :cond_0

    .line 188
    .end local v0    # "intentThemeName":Ljava/lang/String;
    :goto_0
    return-object v0

    .line 179
    .restart local v0    # "intentThemeName":Ljava/lang/String;
    :cond_0
    const-string v3, "ro.setupwizard.theme"

    const/4 v4, 0x0

    invoke-static {v3, v4}, Landroid/os/SystemProperties;->get(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 180
    .local v2, "sysPropThemeName":Ljava/lang/String;
    if-eqz v2, :cond_1

    move-object v0, v2

    .line 181
    goto :goto_0

    .line 183
    :cond_1
    invoke-virtual {p0}, Lcom/google/android/setupwizard/WizardManager;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f070039

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 184
    .local v1, "resourceThemeName":Ljava/lang/String;
    if-eqz v1, :cond_2

    move-object v0, v1

    .line 185
    goto :goto_0

    .line 188
    :cond_2
    const-string v0, "material_light"

    goto :goto_0
.end method

.method private onLoad(Ljava/lang/String;)V
    .locals 1
    .param p1, "scriptUri"    # Ljava/lang/String;

    .prologue
    .line 83
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/google/android/setupwizard/WizardManager;->onLoad(Ljava/lang/String;Landroid/content/Intent;)V

    .line 84
    return-void
.end method

.method private onLoad(Ljava/lang/String;Landroid/content/Intent;)V
    .locals 7
    .param p1, "scriptUri"    # Ljava/lang/String;
    .param p2, "extras"    # Landroid/content/Intent;

    .prologue
    const/4 v6, 0x1

    .line 87
    invoke-direct {p0, p1}, Lcom/google/android/setupwizard/WizardManager;->getScript(Ljava/lang/String;)Lcom/google/android/setupwizard/WizardScript;

    move-result-object v2

    .line 88
    .local v2, "script":Lcom/google/android/setupwizard/WizardScript;
    invoke-virtual {v2}, Lcom/google/android/setupwizard/WizardScript;->getFirstAction()Lcom/google/android/setupwizard/WizardAction;

    move-result-object v0

    .line 90
    .local v0, "action":Lcom/google/android/setupwizard/WizardAction;
    :goto_0
    if-eqz v0, :cond_0

    invoke-virtual {p0, v0}, Lcom/google/android/setupwizard/WizardManager;->isActionAvailable(Lcom/google/android/setupwizard/WizardAction;)Z

    move-result v3

    if-nez v3, :cond_0

    .line 91
    const-string v3, "WizardManager"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "onLoad action not available "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 92
    invoke-virtual {v0}, Lcom/google/android/setupwizard/WizardAction;->getId()Ljava/lang/String;

    move-result-object v3

    const/4 v4, 0x3

    invoke-virtual {v2, v3, v4}, Lcom/google/android/setupwizard/WizardScript;->getNextAction(Ljava/lang/String;I)Lcom/google/android/setupwizard/WizardAction;

    move-result-object v0

    goto :goto_0

    .line 95
    :cond_0
    if-eqz v0, :cond_1

    .line 96
    invoke-virtual {p0, p1, v0, p2}, Lcom/google/android/setupwizard/WizardManager;->sendAction(Ljava/lang/String;Lcom/google/android/setupwizard/WizardAction;Landroid/content/Intent;)V

    .line 105
    :goto_1
    invoke-virtual {p0}, Lcom/google/android/setupwizard/WizardManager;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v1

    .line 106
    .local v1, "pm":Landroid/content/pm/PackageManager;
    new-instance v3, Landroid/content/ComponentName;

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v4

    invoke-direct {v3, p0, v4}, Landroid/content/ComponentName;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    invoke-virtual {v1, v3, v6, v6}, Landroid/content/pm/PackageManager;->setComponentEnabledSetting(Landroid/content/ComponentName;II)V

    .line 109
    return-void

    .line 98
    .end local v1    # "pm":Landroid/content/pm/PackageManager;
    :cond_1
    const-string v3, "WizardManager"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "onLoad could not resolve first action scriptUri="

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " actionId="

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v2}, Lcom/google/android/setupwizard/WizardScript;->getFirstActionId()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 100
    invoke-virtual {p0, p1}, Lcom/google/android/setupwizard/WizardManager;->onExit(Ljava/lang/String;)V

    goto :goto_1
.end method

.method private onNext(Ljava/lang/String;Ljava/lang/String;I)V
    .locals 5
    .param p1, "scriptUri"    # Ljava/lang/String;
    .param p2, "actionId"    # Ljava/lang/String;
    .param p3, "resultCode"    # I

    .prologue
    .line 112
    invoke-direct {p0, p1}, Lcom/google/android/setupwizard/WizardManager;->getScript(Ljava/lang/String;)Lcom/google/android/setupwizard/WizardScript;

    move-result-object v1

    .line 113
    .local v1, "script":Lcom/google/android/setupwizard/WizardScript;
    invoke-virtual {v1, p2, p3}, Lcom/google/android/setupwizard/WizardScript;->getNextAction(Ljava/lang/String;I)Lcom/google/android/setupwizard/WizardAction;

    move-result-object v0

    .line 115
    .local v0, "action":Lcom/google/android/setupwizard/WizardAction;
    :goto_0
    if-eqz v0, :cond_0

    invoke-virtual {p0, v0}, Lcom/google/android/setupwizard/WizardManager;->isActionAvailable(Lcom/google/android/setupwizard/WizardAction;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 116
    const-string v2, "WizardManager"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "onNext action not available "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 117
    invoke-virtual {v0}, Lcom/google/android/setupwizard/WizardAction;->getId()Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x3

    invoke-virtual {v1, v2, v3}, Lcom/google/android/setupwizard/WizardScript;->getNextAction(Ljava/lang/String;I)Lcom/google/android/setupwizard/WizardAction;

    move-result-object v0

    goto :goto_0

    .line 120
    :cond_0
    if-eqz v0, :cond_1

    .line 121
    invoke-virtual {p0, p1, v0}, Lcom/google/android/setupwizard/WizardManager;->sendAction(Ljava/lang/String;Lcom/google/android/setupwizard/WizardAction;)V

    .line 125
    :goto_1
    return-void

    .line 123
    :cond_1
    invoke-virtual {p0, p1}, Lcom/google/android/setupwizard/WizardManager;->onExit(Ljava/lang/String;)V

    goto :goto_1
.end method


# virtual methods
.method isActionAvailable(Lcom/google/android/setupwizard/WizardAction;)Z
    .locals 1
    .param p1, "action"    # Lcom/google/android/setupwizard/WizardAction;

    .prologue
    .line 210
    invoke-virtual {p1}, Lcom/google/android/setupwizard/WizardAction;->getIntent()Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/android/setupwizard/WizardManager;->isIntentAvailable(Landroid/content/Intent;)Z

    move-result v0

    return v0
.end method

.method isIntentAvailable(Landroid/content/Intent;)Z
    .locals 3
    .param p1, "intent"    # Landroid/content/Intent;

    .prologue
    .line 203
    invoke-virtual {p0}, Lcom/google/android/setupwizard/WizardManager;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v1

    .line 204
    .local v1, "packageManager":Landroid/content/pm/PackageManager;
    const/high16 v2, 0x10000

    invoke-virtual {v1, p1, v2}, Landroid/content/pm/PackageManager;->queryIntentActivities(Landroid/content/Intent;I)Ljava/util/List;

    move-result-object v0

    .line 206
    .local v0, "list":Ljava/util/List;, "Ljava/util/List<Landroid/content/pm/ResolveInfo;>;"
    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v2

    if-lez v2, :cond_0

    const/4 v2, 0x1

    :goto_0
    return v2

    :cond_0
    const/4 v2, 0x0

    goto :goto_0
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 8
    .param p1, "icicle"    # Landroid/os/Bundle;

    .prologue
    .line 53
    const-string v5, "WizardManager"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "onCreate("

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, ") this="

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, " topTask="

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {p0}, Lcom/google/android/setupwizard/WizardManager;->topTask()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 54
    invoke-super {p0, p1}, Landroid/app/Activity;->onCreate(Landroid/os/Bundle;)V

    .line 56
    invoke-virtual {p0}, Lcom/google/android/setupwizard/WizardManager;->getIntent()Landroid/content/Intent;

    move-result-object v2

    .line 58
    .local v2, "intent":Landroid/content/Intent;
    if-eqz v2, :cond_2

    .line 59
    invoke-virtual {v2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    .line 60
    .local v0, "action":Ljava/lang/String;
    const-string v5, "com.android.setupwizard.ResultCode"

    const/4 v6, 0x0

    invoke-virtual {v2, v5, v6}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v3

    .line 62
    .local v3, "resultCode":I
    const-string v5, "scriptUri"

    invoke-virtual {v2, v5}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 63
    .local v4, "scriptUri":Ljava/lang/String;
    const-string v5, "actionId"

    invoke-virtual {v2, v5}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 64
    .local v1, "actionId":Ljava/lang/String;
    const-string v5, "WizardManager"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "  action="

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, " resultCode="

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, " scriptUri="

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, " actionId="

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, " extras="

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v2}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 68
    const-string v5, "com.android.wizard.LOAD"

    invoke-virtual {v5, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_0

    .line 69
    invoke-direct {p0, v4}, Lcom/google/android/setupwizard/WizardManager;->onLoad(Ljava/lang/String;)V

    .line 79
    .end local v0    # "action":Ljava/lang/String;
    .end local v1    # "actionId":Ljava/lang/String;
    .end local v3    # "resultCode":I
    .end local v4    # "scriptUri":Ljava/lang/String;
    :goto_0
    invoke-virtual {p0}, Lcom/google/android/setupwizard/WizardManager;->finish()V

    .line 80
    return-void

    .line 70
    .restart local v0    # "action":Ljava/lang/String;
    .restart local v1    # "actionId":Ljava/lang/String;
    .restart local v3    # "resultCode":I
    .restart local v4    # "scriptUri":Ljava/lang/String;
    :cond_0
    const-string v5, "com.android.wizard.NEXT"

    invoke-virtual {v5, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_1

    .line 71
    invoke-direct {p0, v4, v1, v3}, Lcom/google/android/setupwizard/WizardManager;->onNext(Ljava/lang/String;Ljava/lang/String;I)V

    goto :goto_0

    .line 73
    :cond_1
    const-string v5, "WizardManager"

    const-string v6, "ERROR: Unknown action"

    invoke-static {v5, v6}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 76
    .end local v0    # "action":Ljava/lang/String;
    .end local v1    # "actionId":Ljava/lang/String;
    .end local v3    # "resultCode":I
    .end local v4    # "scriptUri":Ljava/lang/String;
    :cond_2
    const-string v5, "WizardManager"

    const-string v6, "ERROR: Intent not available"

    invoke-static {v5, v6}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method protected onExit(Ljava/lang/String;)V
    .locals 3
    .param p1, "scriptUri"    # Ljava/lang/String;

    .prologue
    .line 128
    const-string v0, "WizardManager"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "onExit scriptUri="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 133
    sget-object v0, Lcom/google/android/setupwizard/WizardManager;->sLoadedScripts:Ljava/util/HashMap;

    invoke-virtual {v0, p1}, Ljava/util/HashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 136
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/google/android/setupwizard/WizardManager;->disableComponent(Ljava/lang/String;)V

    .line 137
    return-void
.end method

.method protected sendAction(Ljava/lang/String;Lcom/google/android/setupwizard/WizardAction;)V
    .locals 1
    .param p1, "scriptUri"    # Ljava/lang/String;
    .param p2, "action"    # Lcom/google/android/setupwizard/WizardAction;

    .prologue
    .line 140
    const/4 v0, 0x0

    invoke-virtual {p0, p1, p2, v0}, Lcom/google/android/setupwizard/WizardManager;->sendAction(Ljava/lang/String;Lcom/google/android/setupwizard/WizardAction;Landroid/content/Intent;)V

    .line 141
    return-void
.end method

.method protected sendAction(Ljava/lang/String;Lcom/google/android/setupwizard/WizardAction;Landroid/content/Intent;)V
    .locals 4
    .param p1, "scriptUri"    # Ljava/lang/String;
    .param p2, "action"    # Lcom/google/android/setupwizard/WizardAction;
    .param p3, "extras"    # Landroid/content/Intent;

    .prologue
    .line 151
    const-string v1, "WizardManager"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "sendAction scriptUri="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " action="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " extras="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 153
    invoke-virtual {p2}, Lcom/google/android/setupwizard/WizardAction;->getIntent()Landroid/content/Intent;

    move-result-object v0

    .line 154
    .local v0, "intent":Landroid/content/Intent;
    const/high16 v1, 0x4000000

    invoke-virtual {v0, v1}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    .line 155
    const-string v1, "WizardManager"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "sendAction intent="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " extras="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v0}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 158
    const-string v1, "scriptUri"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 159
    const-string v1, "actionId"

    invoke-virtual {p2}, Lcom/google/android/setupwizard/WizardAction;->getId()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 160
    if-eqz p3, :cond_0

    .line 161
    invoke-virtual {v0, p3}, Landroid/content/Intent;->putExtras(Landroid/content/Intent;)Landroid/content/Intent;

    .line 163
    :cond_0
    invoke-direct {p0, v0}, Lcom/google/android/setupwizard/WizardManager;->addExtrasToIntent(Landroid/content/Intent;)V

    .line 165
    invoke-virtual {p0, v0}, Lcom/google/android/setupwizard/WizardManager;->startActivity(Landroid/content/Intent;)V

    .line 166
    return-void
.end method

.method protected topTask()Ljava/lang/String;
    .locals 6

    .prologue
    .line 234
    const-string v3, "activity"

    invoke-virtual {p0, v3}, Lcom/google/android/setupwizard/WizardManager;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/ActivityManager;

    .line 236
    .local v0, "activityManager":Landroid/app/ActivityManager;
    const/4 v3, 0x1

    invoke-virtual {v0, v3}, Landroid/app/ActivityManager;->getRunningTasks(I)Ljava/util/List;

    move-result-object v2

    .line 237
    .local v2, "list":Ljava/util/List;, "Ljava/util/List<Landroid/app/ActivityManager$RunningTaskInfo;>;"
    const/4 v3, 0x0

    invoke-interface {v2, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/app/ActivityManager$RunningTaskInfo;

    .line 238
    .local v1, "info":Landroid/app/ActivityManager$RunningTaskInfo;
    if-eqz v1, :cond_0

    iget-object v3, v1, Landroid/app/ActivityManager$RunningTaskInfo;->topActivity:Landroid/content/ComponentName;

    if-nez v3, :cond_2

    .line 239
    :cond_0
    const-string v4, "WizardManager"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "topTask info="

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v5, " info.topActivity="

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    if-eqz v1, :cond_1

    iget-object v3, v1, Landroid/app/ActivityManager$RunningTaskInfo;->topActivity:Landroid/content/ComponentName;

    :goto_0
    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v4, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 241
    const-string v3, ""

    .line 244
    :goto_1
    return-object v3

    .line 239
    :cond_1
    const-string v3, "null"

    goto :goto_0

    .line 244
    :cond_2
    iget-object v3, v1, Landroid/app/ActivityManager$RunningTaskInfo;->topActivity:Landroid/content/ComponentName;

    invoke-virtual {v3}, Landroid/content/ComponentName;->getClassName()Ljava/lang/String;

    move-result-object v3

    goto :goto_1
.end method
