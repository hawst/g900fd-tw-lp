.class Lcom/google/android/setupwizard/carrier/MobileDataActivity$5;
.super Landroid/telephony/PhoneStateListener;
.source "MobileDataActivity.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/setupwizard/carrier/MobileDataActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/setupwizard/carrier/MobileDataActivity;


# direct methods
.method constructor <init>(Lcom/google/android/setupwizard/carrier/MobileDataActivity;)V
    .locals 0

    .prologue
    .line 161
    iput-object p1, p0, Lcom/google/android/setupwizard/carrier/MobileDataActivity$5;->this$0:Lcom/google/android/setupwizard/carrier/MobileDataActivity;

    invoke-direct {p0}, Landroid/telephony/PhoneStateListener;-><init>()V

    return-void
.end method


# virtual methods
.method public onDataConnectionStateChanged(I)V
    .locals 3
    .param p1, "state"    # I

    .prologue
    .line 178
    invoke-super {p0, p1}, Landroid/telephony/PhoneStateListener;->onDataConnectionStateChanged(I)V

    .line 179
    iget-object v0, p0, Lcom/google/android/setupwizard/carrier/MobileDataActivity$5;->this$0:Lcom/google/android/setupwizard/carrier/MobileDataActivity;

    # getter for: Lcom/google/android/setupwizard/carrier/MobileDataActivity;->TAG:Ljava/lang/String;
    invoke-static {v0}, Lcom/google/android/setupwizard/carrier/MobileDataActivity;->access$1300(Lcom/google/android/setupwizard/carrier/MobileDataActivity;)Ljava/lang/String;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "onDataConnectionStateChanged state="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 181
    if-nez p1, :cond_0

    iget-object v0, p0, Lcom/google/android/setupwizard/carrier/MobileDataActivity$5;->this$0:Lcom/google/android/setupwizard/carrier/MobileDataActivity;

    # getter for: Lcom/google/android/setupwizard/carrier/MobileDataActivity;->mState:I
    invoke-static {v0}, Lcom/google/android/setupwizard/carrier/MobileDataActivity;->access$000(Lcom/google/android/setupwizard/carrier/MobileDataActivity;)I

    move-result v0

    const/4 v1, 0x3

    if-ne v0, v1, :cond_0

    .line 183
    iget-object v0, p0, Lcom/google/android/setupwizard/carrier/MobileDataActivity$5;->this$0:Lcom/google/android/setupwizard/carrier/MobileDataActivity;

    # getter for: Lcom/google/android/setupwizard/carrier/MobileDataActivity;->TAG:Ljava/lang/String;
    invoke-static {v0}, Lcom/google/android/setupwizard/carrier/MobileDataActivity;->access$1400(Lcom/google/android/setupwizard/carrier/MobileDataActivity;)Ljava/lang/String;

    move-result-object v0

    const-string v1, "Activation succeeded without data connection"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 184
    iget-object v0, p0, Lcom/google/android/setupwizard/carrier/MobileDataActivity$5;->this$0:Lcom/google/android/setupwizard/carrier/MobileDataActivity;

    # invokes: Lcom/google/android/setupwizard/carrier/MobileDataActivity;->onSuccess()V
    invoke-static {v0}, Lcom/google/android/setupwizard/carrier/MobileDataActivity;->access$300(Lcom/google/android/setupwizard/carrier/MobileDataActivity;)V

    .line 186
    :cond_0
    return-void
.end method

.method public onServiceStateChanged(Landroid/telephony/ServiceState;)V
    .locals 3
    .param p1, "state"    # Landroid/telephony/ServiceState;

    .prologue
    .line 164
    iget-object v0, p0, Lcom/google/android/setupwizard/carrier/MobileDataActivity$5;->this$0:Lcom/google/android/setupwizard/carrier/MobileDataActivity;

    # getter for: Lcom/google/android/setupwizard/carrier/MobileDataActivity;->TAG:Ljava/lang/String;
    invoke-static {v0}, Lcom/google/android/setupwizard/carrier/MobileDataActivity;->access$800(Lcom/google/android/setupwizard/carrier/MobileDataActivity;)Ljava/lang/String;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "onServiceStateChanged state=\""

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "\" "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-static {}, Lcom/google/android/setupwizard/carrier/PhoneMonitor;->getPhoneState()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 167
    invoke-static {}, Lcom/google/android/setupwizard/carrier/PhoneMonitor;->simMissing()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 168
    iget-object v0, p0, Lcom/google/android/setupwizard/carrier/MobileDataActivity$5;->this$0:Lcom/google/android/setupwizard/carrier/MobileDataActivity;

    # getter for: Lcom/google/android/setupwizard/carrier/MobileDataActivity;->mHandler:Landroid/os/Handler;
    invoke-static {v0}, Lcom/google/android/setupwizard/carrier/MobileDataActivity;->access$700(Lcom/google/android/setupwizard/carrier/MobileDataActivity;)Landroid/os/Handler;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/setupwizard/carrier/MobileDataActivity$5;->this$0:Lcom/google/android/setupwizard/carrier/MobileDataActivity;

    # getter for: Lcom/google/android/setupwizard/carrier/MobileDataActivity;->mCheckActivation:Ljava/lang/Runnable;
    invoke-static {v1}, Lcom/google/android/setupwizard/carrier/MobileDataActivity;->access$900(Lcom/google/android/setupwizard/carrier/MobileDataActivity;)Ljava/lang/Runnable;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    .line 169
    iget-object v0, p0, Lcom/google/android/setupwizard/carrier/MobileDataActivity$5;->this$0:Lcom/google/android/setupwizard/carrier/MobileDataActivity;

    const/4 v1, 0x2

    # invokes: Lcom/google/android/setupwizard/carrier/MobileDataActivity;->finishAction(I)V
    invoke-static {v0, v1}, Lcom/google/android/setupwizard/carrier/MobileDataActivity;->access$1000(Lcom/google/android/setupwizard/carrier/MobileDataActivity;I)V

    .line 174
    :cond_0
    :goto_0
    return-void

    .line 170
    :cond_1
    invoke-static {}, Lcom/google/android/setupwizard/carrier/PhoneMonitor;->otaspIsNotNeeded()Z

    move-result v0

    if-nez v0, :cond_2

    invoke-static {}, Lcom/google/android/setupwizard/carrier/PhoneMonitor;->isHfaActivated()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 171
    :cond_2
    iget-object v0, p0, Lcom/google/android/setupwizard/carrier/MobileDataActivity$5;->this$0:Lcom/google/android/setupwizard/carrier/MobileDataActivity;

    # getter for: Lcom/google/android/setupwizard/carrier/MobileDataActivity;->TAG:Ljava/lang/String;
    invoke-static {v0}, Lcom/google/android/setupwizard/carrier/MobileDataActivity;->access$1100(Lcom/google/android/setupwizard/carrier/MobileDataActivity;)Ljava/lang/String;

    move-result-object v0

    const-string v1, "HFA success; wait for data service"

    invoke-static {v0, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 172
    iget-object v0, p0, Lcom/google/android/setupwizard/carrier/MobileDataActivity$5;->this$0:Lcom/google/android/setupwizard/carrier/MobileDataActivity;

    # invokes: Lcom/google/android/setupwizard/carrier/MobileDataActivity;->waitForDataService()V
    invoke-static {v0}, Lcom/google/android/setupwizard/carrier/MobileDataActivity;->access$1200(Lcom/google/android/setupwizard/carrier/MobileDataActivity;)V

    goto :goto_0
.end method
