.class public Lcom/google/android/setupwizard/user/GoogleServicesActivity;
.super Lcom/google/android/setupwizard/BaseActivity;
.source "GoogleServicesActivity.java"

# interfaces
.implements Landroid/widget/CompoundButton$OnCheckedChangeListener;
.implements Lcom/google/android/gms/common/api/GoogleApiClient$ConnectionCallbacks;
.implements Lcom/google/android/gms/common/api/GoogleApiClient$OnConnectionFailedListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/setupwizard/user/GoogleServicesActivity$LearnMoreDialog;
    }
.end annotation


# static fields
.field public static final COUNTRY:Lcom/google/android/setupwizard/util/GservicesValue;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/setupwizard/util/GservicesValue",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private static final FILTER_LINK_SPAN_CLICKED:Landroid/content/IntentFilter;


# instance fields
.field private mBackupCheckBox:Landroid/widget/CheckBox;

.field private mLocationSharingCheckBox:Landroid/widget/CheckBox;

.field private mLocationWifiScanCheckBox:Landroid/widget/CheckBox;

.field private mPlayEmailCheckBox:Landroid/widget/CheckBox;

.field private final mReceiver:Landroid/content/BroadcastReceiver;

.field private mUsageReportingCheckBox:Landroid/widget/CheckBox;

.field private mUsageReportingClient:Lcom/google/android/gms/common/api/GoogleApiClient;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 82
    new-instance v0, Landroid/content/IntentFilter;

    const-string v1, "com.google.android.setupwizard.LINK_SPAN_CLICKED"

    invoke-direct {v0, v1}, Landroid/content/IntentFilter;-><init>(Ljava/lang/String;)V

    sput-object v0, Lcom/google/android/setupwizard/user/GoogleServicesActivity;->FILTER_LINK_SPAN_CLICKED:Landroid/content/IntentFilter;

    .line 94
    const-string v1, "device_country"

    const/4 v0, 0x0

    check-cast v0, Ljava/lang/String;

    invoke-static {v1, v0}, Lcom/google/android/setupwizard/util/GservicesValue;->value(Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/setupwizard/util/GservicesValue;

    move-result-object v0

    sput-object v0, Lcom/google/android/setupwizard/user/GoogleServicesActivity;->COUNTRY:Lcom/google/android/setupwizard/util/GservicesValue;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 76
    invoke-direct {p0}, Lcom/google/android/setupwizard/BaseActivity;-><init>()V

    .line 104
    new-instance v0, Lcom/google/android/setupwizard/user/GoogleServicesActivity$1;

    invoke-direct {v0, p0}, Lcom/google/android/setupwizard/user/GoogleServicesActivity$1;-><init>(Lcom/google/android/setupwizard/user/GoogleServicesActivity;)V

    iput-object v0, p0, Lcom/google/android/setupwizard/user/GoogleServicesActivity;->mReceiver:Landroid/content/BroadcastReceiver;

    .line 484
    return-void
.end method

.method static synthetic access$000(Lcom/google/android/setupwizard/user/GoogleServicesActivity;Ljava/lang/String;)Z
    .locals 1
    .param p0, "x0"    # Lcom/google/android/setupwizard/user/GoogleServicesActivity;
    .param p1, "x1"    # Ljava/lang/String;

    .prologue
    .line 76
    invoke-direct {p0, p1}, Lcom/google/android/setupwizard/user/GoogleServicesActivity;->isUserInCountry(Ljava/lang/String;)Z

    move-result v0

    return v0
.end method

.method private getPlayEmailCheckBoxDefault()Z
    .locals 5

    .prologue
    const/4 v2, 0x0

    .line 523
    invoke-virtual {p0}, Lcom/google/android/setupwizard/user/GoogleServicesActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f0b0001

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getStringArray(I)[Ljava/lang/String;

    move-result-object v1

    .line 525
    .local v1, "optInCountries":[Ljava/lang/String;
    invoke-direct {p0}, Lcom/google/android/setupwizard/user/GoogleServicesActivity;->getUserCountry()Ljava/lang/String;

    move-result-object v0

    .line 527
    .local v0, "countryCode":Ljava/lang/String;
    if-eqz v0, :cond_0

    const-string v3, ""

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_0

    if-nez v1, :cond_2

    .line 528
    :cond_0
    iget-object v3, p0, Lcom/google/android/setupwizard/user/GoogleServicesActivity;->TAG:Ljava/lang/String;

    const-string v4, "Problem looking up opt-in countries"

    invoke-static {v3, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 532
    :cond_1
    :goto_0
    return v2

    :cond_2
    invoke-static {v1}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v3

    invoke-interface {v3, v0}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_1

    const/4 v2, 0x1

    goto :goto_0
.end method

.method private getUserCountry()Ljava/lang/String;
    .locals 9

    .prologue
    .line 437
    :try_start_0
    invoke-static {p0}, Landroid/telephony/TelephonyManager;->from(Landroid/content/Context;)Landroid/telephony/TelephonyManager;

    move-result-object v5

    .line 438
    .local v5, "tm":Landroid/telephony/TelephonyManager;
    invoke-static {}, Landroid/telephony/SubscriptionManager;->getActiveSubInfoList()Ljava/util/List;

    move-result-object v4

    .line 439
    .local v4, "subList":Ljava/util/List;, "Ljava/util/List<Landroid/telephony/SubInfoRecord;>;"
    if-eqz v4, :cond_1

    .line 440
    invoke-interface {v4}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    .local v2, "i$":Ljava/util/Iterator;
    :cond_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v6

    if-eqz v6, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/telephony/SubInfoRecord;

    .line 441
    .local v3, "sub":Landroid/telephony/SubInfoRecord;
    iget v6, v3, Landroid/telephony/SubInfoRecord;->slotId:I

    invoke-virtual {v5, v6}, Landroid/telephony/TelephonyManager;->getSimState(I)I

    move-result v6

    const/4 v7, 0x5

    if-ne v6, v7, :cond_0

    .line 442
    iget-object v6, p0, Lcom/google/android/setupwizard/user/GoogleServicesActivity;->TAG:Ljava/lang/String;

    const-string v7, "Returning user country using first ready SIM"

    invoke-static {v6, v7}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 443
    iget-wide v6, v3, Landroid/telephony/SubInfoRecord;->subId:J

    invoke-virtual {v5, v6, v7}, Landroid/telephony/TelephonyManager;->getSimCountryIso(J)Ljava/lang/String;
    :try_end_0
    .catch Ljava/lang/SecurityException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 464
    .end local v2    # "i$":Ljava/util/Iterator;
    .end local v3    # "sub":Landroid/telephony/SubInfoRecord;
    .end local v4    # "subList":Ljava/util/List;, "Ljava/util/List<Landroid/telephony/SubInfoRecord;>;"
    .end local v5    # "tm":Landroid/telephony/TelephonyManager;
    :goto_0
    return-object v0

    .line 447
    :catch_0
    move-exception v1

    .line 450
    .local v1, "e":Ljava/lang/SecurityException;
    iget-object v6, p0, Lcom/google/android/setupwizard/user/GoogleServicesActivity;->TAG:Ljava/lang/String;

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "Unable to get country from SIM: "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v1}, Ljava/lang/SecurityException;->getMessage()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 456
    .end local v1    # "e":Ljava/lang/SecurityException;
    :cond_1
    sget-object v6, Lcom/google/android/setupwizard/user/GoogleServicesActivity;->COUNTRY:Lcom/google/android/setupwizard/util/GservicesValue;

    invoke-virtual {v6}, Lcom/google/android/setupwizard/util/GservicesValue;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 457
    .local v0, "country":Ljava/lang/String;
    if-eqz v0, :cond_2

    .line 458
    iget-object v6, p0, Lcom/google/android/setupwizard/user/GoogleServicesActivity;->TAG:Ljava/lang/String;

    const-string v7, "Returning user country using Gservices device_country"

    invoke-static {v6, v7}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 463
    :cond_2
    iget-object v6, p0, Lcom/google/android/setupwizard/user/GoogleServicesActivity;->TAG:Ljava/lang/String;

    const-string v7, "Returning user country using Locale"

    invoke-static {v6, v7}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 464
    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v6

    invoke-virtual {v6}, Ljava/util/Locale;->getCountry()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method private initUsageReportingClient()V
    .locals 2

    .prologue
    .line 272
    new-instance v0, Lcom/google/android/gms/common/api/GoogleApiClient$Builder;

    invoke-direct {v0, p0}, Lcom/google/android/gms/common/api/GoogleApiClient$Builder;-><init>(Landroid/content/Context;)V

    sget-object v1, Lcom/google/android/gms/usagereporting/UsageReporting;->API:Lcom/google/android/gms/common/api/Api;

    invoke-virtual {v0, v1}, Lcom/google/android/gms/common/api/GoogleApiClient$Builder;->addApi(Lcom/google/android/gms/common/api/Api;)Lcom/google/android/gms/common/api/GoogleApiClient$Builder;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/google/android/gms/common/api/GoogleApiClient$Builder;->addConnectionCallbacks(Lcom/google/android/gms/common/api/GoogleApiClient$ConnectionCallbacks;)Lcom/google/android/gms/common/api/GoogleApiClient$Builder;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/google/android/gms/common/api/GoogleApiClient$Builder;->addOnConnectionFailedListener(Lcom/google/android/gms/common/api/GoogleApiClient$OnConnectionFailedListener;)Lcom/google/android/gms/common/api/GoogleApiClient$Builder;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/common/api/GoogleApiClient$Builder;->build()Lcom/google/android/gms/common/api/GoogleApiClient;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/setupwizard/user/GoogleServicesActivity;->mUsageReportingClient:Lcom/google/android/gms/common/api/GoogleApiClient;

    .line 277
    return-void
.end method

.method private isUserInCountry(Ljava/lang/String;)Z
    .locals 4
    .param p1, "countryCode"    # Ljava/lang/String;

    .prologue
    .line 429
    invoke-direct {p0}, Lcom/google/android/setupwizard/user/GoogleServicesActivity;->getUserCountry()Ljava/lang/String;

    move-result-object v0

    .line 430
    .local v0, "userCountry":Ljava/lang/String;
    iget-object v1, p0, Lcom/google/android/setupwizard/user/GoogleServicesActivity;->TAG:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "User country is "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 431
    invoke-virtual {p1, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v1

    return v1
.end method

.method private isVendorSetup()Z
    .locals 2

    .prologue
    .line 280
    const-string v0, "com.google.android.setupwizard.VENDOR_SETUP"

    invoke-virtual {p0}, Lcom/google/android/setupwizard/user/GoogleServicesActivity;->getIntent()Landroid/content/Intent;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method private loadPreferences()V
    .locals 8

    .prologue
    const/4 v7, 0x1

    .line 399
    invoke-virtual {p0}, Lcom/google/android/setupwizard/user/GoogleServicesActivity;->getLocalPreferences()Landroid/content/SharedPreferences;

    move-result-object v5

    .line 401
    .local v5, "prefs":Landroid/content/SharedPreferences;
    iget-object v6, p0, Lcom/google/android/setupwizard/user/GoogleServicesActivity;->mBackupCheckBox:Landroid/widget/CheckBox;

    if-eqz v6, :cond_0

    .line 402
    const-string v6, "agreeBackup"

    invoke-interface {v5, v6, v7}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    .line 403
    .local v0, "agreedBackup":Z
    iget-object v6, p0, Lcom/google/android/setupwizard/user/GoogleServicesActivity;->mBackupCheckBox:Landroid/widget/CheckBox;

    invoke-virtual {v6, v0}, Landroid/widget/CheckBox;->setChecked(Z)V

    .line 405
    .end local v0    # "agreedBackup":Z
    :cond_0
    iget-object v6, p0, Lcom/google/android/setupwizard/user/GoogleServicesActivity;->mLocationSharingCheckBox:Landroid/widget/CheckBox;

    if-eqz v6, :cond_1

    .line 406
    const-string v6, "agreeLocationSharing"

    invoke-interface {v5, v6, v7}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v1

    .line 407
    .local v1, "agreedLocation":Z
    iget-object v6, p0, Lcom/google/android/setupwizard/user/GoogleServicesActivity;->mLocationSharingCheckBox:Landroid/widget/CheckBox;

    invoke-virtual {v6, v1}, Landroid/widget/CheckBox;->setChecked(Z)V

    .line 409
    .end local v1    # "agreedLocation":Z
    :cond_1
    iget-object v6, p0, Lcom/google/android/setupwizard/user/GoogleServicesActivity;->mLocationWifiScanCheckBox:Landroid/widget/CheckBox;

    if-eqz v6, :cond_2

    .line 410
    const-string v6, "agreeScanWifi"

    invoke-interface {v5, v6, v7}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v4

    .line 411
    .local v4, "agreedWifiScan":Z
    iget-object v6, p0, Lcom/google/android/setupwizard/user/GoogleServicesActivity;->mLocationWifiScanCheckBox:Landroid/widget/CheckBox;

    invoke-virtual {v6, v4}, Landroid/widget/CheckBox;->setChecked(Z)V

    .line 413
    .end local v4    # "agreedWifiScan":Z
    :cond_2
    iget-object v6, p0, Lcom/google/android/setupwizard/user/GoogleServicesActivity;->mUsageReportingCheckBox:Landroid/widget/CheckBox;

    if-eqz v6, :cond_3

    .line 414
    const-string v6, "agreeUsageReporting"

    invoke-interface {v5, v6, v7}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v3

    .line 415
    .local v3, "agreedUsageReporting":Z
    iget-object v6, p0, Lcom/google/android/setupwizard/user/GoogleServicesActivity;->mUsageReportingCheckBox:Landroid/widget/CheckBox;

    invoke-virtual {v6, v3}, Landroid/widget/CheckBox;->setChecked(Z)V

    .line 417
    .end local v3    # "agreedUsageReporting":Z
    :cond_3
    iget-object v6, p0, Lcom/google/android/setupwizard/user/GoogleServicesActivity;->mPlayEmailCheckBox:Landroid/widget/CheckBox;

    if-eqz v6, :cond_4

    .line 418
    const-string v6, "agreePlayEmail"

    invoke-direct {p0}, Lcom/google/android/setupwizard/user/GoogleServicesActivity;->getPlayEmailCheckBoxDefault()Z

    move-result v7

    invoke-interface {v5, v6, v7}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v2

    .line 420
    .local v2, "agreedPlayEmail":Z
    iget-object v6, p0, Lcom/google/android/setupwizard/user/GoogleServicesActivity;->mPlayEmailCheckBox:Landroid/widget/CheckBox;

    invoke-virtual {v6, v2}, Landroid/widget/CheckBox;->setChecked(Z)V

    .line 422
    .end local v2    # "agreedPlayEmail":Z
    :cond_4
    return-void
.end method

.method private savePreferences()V
    .locals 3

    .prologue
    .line 377
    invoke-virtual {p0}, Lcom/google/android/setupwizard/user/GoogleServicesActivity;->getLocalPreferences()Landroid/content/SharedPreferences;

    move-result-object v1

    invoke-interface {v1}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    .line 379
    .local v0, "editor":Landroid/content/SharedPreferences$Editor;
    iget-object v1, p0, Lcom/google/android/setupwizard/user/GoogleServicesActivity;->mBackupCheckBox:Landroid/widget/CheckBox;

    if-eqz v1, :cond_0

    .line 380
    const-string v1, "agreeBackup"

    iget-object v2, p0, Lcom/google/android/setupwizard/user/GoogleServicesActivity;->mBackupCheckBox:Landroid/widget/CheckBox;

    invoke-virtual {v2}, Landroid/widget/CheckBox;->isChecked()Z

    move-result v2

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    .line 382
    :cond_0
    iget-object v1, p0, Lcom/google/android/setupwizard/user/GoogleServicesActivity;->mLocationSharingCheckBox:Landroid/widget/CheckBox;

    if-eqz v1, :cond_1

    .line 383
    const-string v1, "agreeLocationSharing"

    iget-object v2, p0, Lcom/google/android/setupwizard/user/GoogleServicesActivity;->mLocationSharingCheckBox:Landroid/widget/CheckBox;

    invoke-virtual {v2}, Landroid/widget/CheckBox;->isChecked()Z

    move-result v2

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    .line 385
    :cond_1
    iget-object v1, p0, Lcom/google/android/setupwizard/user/GoogleServicesActivity;->mLocationWifiScanCheckBox:Landroid/widget/CheckBox;

    if-eqz v1, :cond_2

    .line 386
    const-string v1, "agreeScanWifi"

    iget-object v2, p0, Lcom/google/android/setupwizard/user/GoogleServicesActivity;->mLocationWifiScanCheckBox:Landroid/widget/CheckBox;

    invoke-virtual {v2}, Landroid/widget/CheckBox;->isChecked()Z

    move-result v2

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    .line 388
    :cond_2
    iget-object v1, p0, Lcom/google/android/setupwizard/user/GoogleServicesActivity;->mUsageReportingCheckBox:Landroid/widget/CheckBox;

    if-eqz v1, :cond_3

    .line 389
    const-string v1, "agreeUsageReporting"

    iget-object v2, p0, Lcom/google/android/setupwizard/user/GoogleServicesActivity;->mUsageReportingCheckBox:Landroid/widget/CheckBox;

    invoke-virtual {v2}, Landroid/widget/CheckBox;->isChecked()Z

    move-result v2

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    .line 391
    :cond_3
    iget-object v1, p0, Lcom/google/android/setupwizard/user/GoogleServicesActivity;->mPlayEmailCheckBox:Landroid/widget/CheckBox;

    if-eqz v1, :cond_4

    .line 392
    const-string v1, "agreePlayEmail"

    iget-object v2, p0, Lcom/google/android/setupwizard/user/GoogleServicesActivity;->mPlayEmailCheckBox:Landroid/widget/CheckBox;

    invoke-virtual {v2}, Landroid/widget/CheckBox;->isChecked()Z

    move-result v2

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    .line 395
    :cond_4
    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 396
    return-void
.end method

.method private setBackup(Z)V
    .locals 1
    .param p1, "enabled"    # Z

    .prologue
    .line 301
    invoke-virtual {p0}, Lcom/google/android/setupwizard/user/GoogleServicesActivity;->getAccount()Landroid/accounts/Account;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/setupwizard/util/BackupRestoreHelper;->setBackupAccount(Landroid/accounts/Account;)Z

    .line 302
    invoke-static {p1}, Lcom/google/android/setupwizard/util/BackupRestoreHelper;->setBackupEnabled(Z)Z

    .line 303
    return-void
.end method

.method private setLocationSharing(Z)V
    .locals 3
    .param p1, "flag"    # Z

    .prologue
    .line 307
    invoke-virtual {p0}, Lcom/google/android/setupwizard/user/GoogleServicesActivity;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    .line 308
    .local v1, "res":Landroid/content/ContentResolver;
    if-eqz p1, :cond_0

    const-string v0, "1"

    .line 314
    .local v0, "optInValue":Ljava/lang/String;
    :goto_0
    const-string v2, "network_location_opt_in"

    invoke-static {v1, v2, v0}, Lcom/google/android/gsf/GoogleSettingsContract$Partner;->putString(Landroid/content/ContentResolver;Ljava/lang/String;Ljava/lang/String;)Z

    .line 319
    const-string v2, "network"

    invoke-static {v1, v2, p1}, Landroid/provider/Settings$Secure;->setLocationProviderEnabled(Landroid/content/ContentResolver;Ljava/lang/String;Z)V

    .line 320
    return-void

    .line 308
    .end local v0    # "optInValue":Ljava/lang/String;
    :cond_0
    const-string v0, "0"

    goto :goto_0
.end method

.method private setLocationWifiScan(Z)V
    .locals 3
    .param p1, "flag"    # Z

    .prologue
    .line 323
    invoke-virtual {p0}, Lcom/google/android/setupwizard/user/GoogleServicesActivity;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    const-string v2, "wifi_scan_always_enabled"

    if-eqz p1, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-static {v1, v2, v0}, Landroid/provider/Settings$Global;->putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z

    .line 325
    return-void

    .line 323
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private setPlayEmail(Z)V
    .locals 3
    .param p1, "flag"    # Z

    .prologue
    .line 340
    new-instance v0, Landroid/content/Intent;

    const-string v1, "com.android.vending.TOS_ACKED"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 341
    .local v0, "playIntent":Landroid/content/Intent;
    const-string v1, "com.android.vending"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setPackage(Ljava/lang/String;)Landroid/content/Intent;

    .line 342
    const-string v1, "TosAckedReceiver.account"

    invoke-virtual {p0}, Lcom/google/android/setupwizard/user/GoogleServicesActivity;->getAccount()Landroid/accounts/Account;

    move-result-object v2

    iget-object v2, v2, Landroid/accounts/Account;->name:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 343
    const-string v1, "TosAckedReceiver.optIn"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 344
    const-string v1, "com.android.vending.TOS_ACKED"

    invoke-virtual {p0, v0, v1}, Lcom/google/android/setupwizard/user/GoogleServicesActivity;->sendBroadcast(Landroid/content/Intent;Ljava/lang/String;)V

    .line 345
    return-void
.end method

.method private setUsageReporting(Z)V
    .locals 4
    .param p1, "flag"    # Z

    .prologue
    .line 329
    iget-object v1, p0, Lcom/google/android/setupwizard/user/GoogleServicesActivity;->mUsageReportingClient:Lcom/google/android/gms/common/api/GoogleApiClient;

    invoke-interface {v1}, Lcom/google/android/gms/common/api/GoogleApiClient;->isConnected()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 330
    if-eqz p1, :cond_0

    const/4 v0, 0x1

    .line 332
    .local v0, "optInFlag":I
    :goto_0
    sget-object v1, Lcom/google/android/gms/usagereporting/UsageReporting;->UsageReportingApi:Lcom/google/android/gms/usagereporting/UsageReportingApi;

    iget-object v2, p0, Lcom/google/android/setupwizard/user/GoogleServicesActivity;->mUsageReportingClient:Lcom/google/android/gms/common/api/GoogleApiClient;

    new-instance v3, Lcom/google/android/gms/usagereporting/UsageReportingOptInOptions;

    invoke-direct {v3, v0}, Lcom/google/android/gms/usagereporting/UsageReportingOptInOptions;-><init>(I)V

    invoke-interface {v1, v2, v3}, Lcom/google/android/gms/usagereporting/UsageReportingApi;->setOptInOptions(Lcom/google/android/gms/common/api/GoogleApiClient;Lcom/google/android/gms/usagereporting/UsageReportingOptInOptions;)Lcom/google/android/gms/common/api/PendingResult;

    .line 337
    .end local v0    # "optInFlag":I
    :goto_1
    return-void

    .line 330
    :cond_0
    const/4 v0, 0x2

    goto :goto_0

    .line 335
    :cond_1
    iget-object v1, p0, Lcom/google/android/setupwizard/user/GoogleServicesActivity;->TAG:Ljava/lang/String;

    const-string v2, "GMS client not connected, cannot set UsageReporting opt-in"

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1
.end method


# virtual methods
.method protected getNextTransition()I
    .locals 1

    .prologue
    .line 537
    invoke-direct {p0}, Lcom/google/android/setupwizard/user/GoogleServicesActivity;->isVendorSetup()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_0
    invoke-super {p0}, Lcom/google/android/setupwizard/BaseActivity;->getNextTransition()I

    move-result v0

    goto :goto_0
.end method

.method protected getPreviousTransition()I
    .locals 1

    .prologue
    .line 542
    invoke-direct {p0}, Lcom/google/android/setupwizard/user/GoogleServicesActivity;->isVendorSetup()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_0
    invoke-super {p0}, Lcom/google/android/setupwizard/BaseActivity;->getPreviousTransition()I

    move-result v0

    goto :goto_0
.end method

.method protected initViews()V
    .locals 13

    .prologue
    .line 186
    invoke-virtual {p0}, Lcom/google/android/setupwizard/user/GoogleServicesActivity;->getAccount()Landroid/accounts/Account;

    move-result-object v10

    if-eqz v10, :cond_9

    const/4 v5, 0x1

    .line 187
    .local v5, "hasAccount":Z
    :goto_0
    const v7, 0x7f030007

    .line 188
    .local v7, "layout":I
    invoke-virtual {p0}, Lcom/google/android/setupwizard/user/GoogleServicesActivity;->hasMfmAccount()Z

    move-result v10

    if-eqz v10, :cond_a

    .line 189
    const v7, 0x7f030009

    .line 195
    :cond_0
    :goto_1
    const v10, 0x7f030024

    invoke-virtual {p0, v7, v10}, Lcom/google/android/setupwizard/user/GoogleServicesActivity;->setTemplateContent(II)V

    .line 196
    const v10, 0x7f0700a1

    invoke-virtual {p0, v10}, Lcom/google/android/setupwizard/user/GoogleServicesActivity;->setHeaderText(I)V

    .line 198
    const v10, 0x7f0e0046

    invoke-virtual {p0, v10}, Lcom/google/android/setupwizard/user/GoogleServicesActivity;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/TextView;

    .line 199
    .local v4, "descriptionView":Landroid/widget/TextView;
    if-eqz v4, :cond_1

    .line 200
    new-instance v3, Ljava/util/HashSet;

    invoke-direct {v3}, Ljava/util/HashSet;-><init>()V

    .line 201
    .local v3, "descriptionLinks":Ljava/util/HashSet;, "Ljava/util/HashSet<Ljava/lang/String;>;"
    const-string v10, "google_privacy"

    invoke-virtual {v3, v10}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    .line 202
    invoke-static {v4, v3}, Lcom/google/android/setupwizard/util/LinkSpan;->linkify(Landroid/widget/TextView;Ljava/util/Set;)V

    .line 205
    .end local v3    # "descriptionLinks":Ljava/util/HashSet;, "Ljava/util/HashSet<Ljava/lang/String;>;"
    :cond_1
    const v10, 0x7f0e0047

    invoke-virtual {p0, v10}, Lcom/google/android/setupwizard/user/GoogleServicesActivity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    .line 206
    .local v1, "agreementView":Landroid/widget/TextView;
    if-eqz v1, :cond_3

    .line 208
    if-nez v5, :cond_2

    const-string v10, "de"

    invoke-direct {p0, v10}, Lcom/google/android/setupwizard/user/GoogleServicesActivity;->isUserInCountry(Ljava/lang/String;)Z

    move-result v10

    if-eqz v10, :cond_2

    .line 209
    const v10, 0x7f070080

    invoke-virtual {v1, v10}, Landroid/widget/TextView;->setText(I)V

    .line 212
    :cond_2
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    .line 213
    .local v0, "agreementLinks":Ljava/util/HashSet;, "Ljava/util/HashSet<Ljava/lang/String;>;"
    const-string v10, "tos"

    invoke-virtual {v0, v10}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    .line 214
    const-string v10, "google_privacy"

    invoke-virtual {v0, v10}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    .line 215
    invoke-static {v1, v0}, Lcom/google/android/setupwizard/util/LinkSpan;->linkify(Landroid/widget/TextView;Ljava/util/Set;)V

    .line 219
    .end local v0    # "agreementLinks":Ljava/util/HashSet;, "Ljava/util/HashSet<Ljava/lang/String;>;"
    :cond_3
    const v10, 0x7f0e0048

    invoke-virtual {p0, v10}, Lcom/google/android/setupwizard/user/GoogleServicesActivity;->findViewById(I)Landroid/view/View;

    move-result-object v10

    check-cast v10, Landroid/widget/CheckBox;

    iput-object v10, p0, Lcom/google/android/setupwizard/user/GoogleServicesActivity;->mBackupCheckBox:Landroid/widget/CheckBox;

    .line 220
    iget-object v10, p0, Lcom/google/android/setupwizard/user/GoogleServicesActivity;->mBackupCheckBox:Landroid/widget/CheckBox;

    if-eqz v10, :cond_4

    .line 221
    invoke-virtual {p0}, Lcom/google/android/setupwizard/user/GoogleServicesActivity;->isPrimaryUser()Z

    move-result v10

    if-eqz v10, :cond_c

    .line 222
    iget-object v10, p0, Lcom/google/android/setupwizard/user/GoogleServicesActivity;->mBackupCheckBox:Landroid/widget/CheckBox;

    invoke-virtual {v10, p0}, Landroid/widget/CheckBox;->setOnCheckedChangeListener(Landroid/widget/CompoundButton$OnCheckedChangeListener;)V

    .line 223
    new-instance v2, Ljava/util/HashSet;

    invoke-direct {v2}, Ljava/util/HashSet;-><init>()V

    .line 224
    .local v2, "backupRestoreLinks":Ljava/util/HashSet;, "Ljava/util/HashSet<Ljava/lang/String;>;"
    const-string v10, "backup_details"

    invoke-virtual {v2, v10}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    .line 225
    iget-object v10, p0, Lcom/google/android/setupwizard/user/GoogleServicesActivity;->mBackupCheckBox:Landroid/widget/CheckBox;

    invoke-static {v10, v2}, Lcom/google/android/setupwizard/util/LinkSpan;->linkify(Landroid/widget/TextView;Ljava/util/Set;)V

    .line 233
    .end local v2    # "backupRestoreLinks":Ljava/util/HashSet;, "Ljava/util/HashSet<Ljava/lang/String;>;"
    :cond_4
    :goto_2
    const v10, 0x7f0e004a

    invoke-virtual {p0, v10}, Lcom/google/android/setupwizard/user/GoogleServicesActivity;->findViewById(I)Landroid/view/View;

    move-result-object v10

    check-cast v10, Landroid/widget/CheckBox;

    iput-object v10, p0, Lcom/google/android/setupwizard/user/GoogleServicesActivity;->mLocationSharingCheckBox:Landroid/widget/CheckBox;

    .line 234
    iget-object v10, p0, Lcom/google/android/setupwizard/user/GoogleServicesActivity;->mLocationSharingCheckBox:Landroid/widget/CheckBox;

    if-eqz v10, :cond_5

    .line 235
    iget-object v10, p0, Lcom/google/android/setupwizard/user/GoogleServicesActivity;->mLocationSharingCheckBox:Landroid/widget/CheckBox;

    invoke-virtual {v10, p0}, Landroid/widget/CheckBox;->setOnCheckedChangeListener(Landroid/widget/CompoundButton$OnCheckedChangeListener;)V

    .line 236
    new-instance v8, Ljava/util/HashSet;

    invoke-direct {v8}, Ljava/util/HashSet;-><init>()V

    .line 237
    .local v8, "locationLinks":Ljava/util/HashSet;, "Ljava/util/HashSet<Ljava/lang/String;>;"
    const-string v10, "location_details"

    invoke-virtual {v8, v10}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    .line 238
    iget-object v10, p0, Lcom/google/android/setupwizard/user/GoogleServicesActivity;->mLocationSharingCheckBox:Landroid/widget/CheckBox;

    invoke-static {v10, v8}, Lcom/google/android/setupwizard/util/LinkSpan;->linkify(Landroid/widget/TextView;Ljava/util/Set;)V

    .line 242
    .end local v8    # "locationLinks":Ljava/util/HashSet;, "Ljava/util/HashSet<Ljava/lang/String;>;"
    :cond_5
    const v10, 0x7f0e004b

    invoke-virtual {p0, v10}, Lcom/google/android/setupwizard/user/GoogleServicesActivity;->findViewById(I)Landroid/view/View;

    move-result-object v10

    check-cast v10, Landroid/widget/CheckBox;

    iput-object v10, p0, Lcom/google/android/setupwizard/user/GoogleServicesActivity;->mLocationWifiScanCheckBox:Landroid/widget/CheckBox;

    .line 243
    iget-object v10, p0, Lcom/google/android/setupwizard/user/GoogleServicesActivity;->mLocationWifiScanCheckBox:Landroid/widget/CheckBox;

    if-eqz v10, :cond_6

    .line 244
    iget-object v10, p0, Lcom/google/android/setupwizard/user/GoogleServicesActivity;->mLocationWifiScanCheckBox:Landroid/widget/CheckBox;

    invoke-virtual {v10, p0}, Landroid/widget/CheckBox;->setOnCheckedChangeListener(Landroid/widget/CompoundButton$OnCheckedChangeListener;)V

    .line 248
    :cond_6
    const v10, 0x7f0e004c

    invoke-virtual {p0, v10}, Lcom/google/android/setupwizard/user/GoogleServicesActivity;->findViewById(I)Landroid/view/View;

    move-result-object v10

    check-cast v10, Landroid/widget/CheckBox;

    iput-object v10, p0, Lcom/google/android/setupwizard/user/GoogleServicesActivity;->mUsageReportingCheckBox:Landroid/widget/CheckBox;

    .line 249
    iget-object v10, p0, Lcom/google/android/setupwizard/user/GoogleServicesActivity;->mUsageReportingCheckBox:Landroid/widget/CheckBox;

    if-eqz v10, :cond_7

    .line 250
    iget-object v10, p0, Lcom/google/android/setupwizard/user/GoogleServicesActivity;->mUsageReportingCheckBox:Landroid/widget/CheckBox;

    invoke-virtual {v10, p0}, Landroid/widget/CheckBox;->setOnCheckedChangeListener(Landroid/widget/CompoundButton$OnCheckedChangeListener;)V

    .line 251
    new-instance v9, Ljava/util/HashSet;

    invoke-direct {v9}, Ljava/util/HashSet;-><init>()V

    .line 252
    .local v9, "usageReportingLinks":Ljava/util/HashSet;, "Ljava/util/HashSet<Ljava/lang/String;>;"
    const-string v10, "usage_reporting_details"

    invoke-virtual {v9, v10}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    .line 253
    iget-object v10, p0, Lcom/google/android/setupwizard/user/GoogleServicesActivity;->mUsageReportingCheckBox:Landroid/widget/CheckBox;

    invoke-static {v10, v9}, Lcom/google/android/setupwizard/util/LinkSpan;->linkify(Landroid/widget/TextView;Ljava/util/Set;)V

    .line 257
    .end local v9    # "usageReportingLinks":Ljava/util/HashSet;, "Ljava/util/HashSet<Ljava/lang/String;>;"
    :cond_7
    const v10, 0x7f0e0049

    invoke-virtual {p0, v10}, Lcom/google/android/setupwizard/user/GoogleServicesActivity;->findViewById(I)Landroid/view/View;

    move-result-object v10

    check-cast v10, Landroid/widget/CheckBox;

    iput-object v10, p0, Lcom/google/android/setupwizard/user/GoogleServicesActivity;->mPlayEmailCheckBox:Landroid/widget/CheckBox;

    .line 258
    iget-object v10, p0, Lcom/google/android/setupwizard/user/GoogleServicesActivity;->mPlayEmailCheckBox:Landroid/widget/CheckBox;

    if-eqz v10, :cond_8

    .line 259
    if-eqz v5, :cond_d

    invoke-virtual {p0}, Lcom/google/android/setupwizard/user/GoogleServicesActivity;->getLocalPreferences()Landroid/content/SharedPreferences;

    move-result-object v10

    const-string v11, "is_new_account"

    const/4 v12, 0x0

    invoke-interface {v10, v11, v12}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v10

    if-eqz v10, :cond_d

    const/4 v6, 0x1

    .line 261
    .local v6, "hasNewAccount":Z
    :goto_3
    if-eqz v6, :cond_e

    .line 262
    iget-object v10, p0, Lcom/google/android/setupwizard/user/GoogleServicesActivity;->mPlayEmailCheckBox:Landroid/widget/CheckBox;

    invoke-virtual {v10, p0}, Landroid/widget/CheckBox;->setOnCheckedChangeListener(Landroid/widget/CompoundButton$OnCheckedChangeListener;)V

    .line 269
    .end local v6    # "hasNewAccount":Z
    :cond_8
    :goto_4
    return-void

    .line 186
    .end local v1    # "agreementView":Landroid/widget/TextView;
    .end local v4    # "descriptionView":Landroid/widget/TextView;
    .end local v5    # "hasAccount":Z
    .end local v7    # "layout":I
    :cond_9
    const/4 v5, 0x0

    goto/16 :goto_0

    .line 190
    .restart local v5    # "hasAccount":Z
    .restart local v7    # "layout":I
    :cond_a
    invoke-direct {p0}, Lcom/google/android/setupwizard/user/GoogleServicesActivity;->isVendorSetup()Z

    move-result v10

    if-eqz v10, :cond_b

    if-eqz v5, :cond_b

    .line 191
    const v7, 0x7f03000b

    goto/16 :goto_1

    .line 192
    :cond_b
    if-nez v5, :cond_0

    .line 193
    const v7, 0x7f03000a

    goto/16 :goto_1

    .line 227
    .restart local v1    # "agreementView":Landroid/widget/TextView;
    .restart local v4    # "descriptionView":Landroid/widget/TextView;
    :cond_c
    iget-object v10, p0, Lcom/google/android/setupwizard/user/GoogleServicesActivity;->mBackupCheckBox:Landroid/widget/CheckBox;

    const/16 v11, 0x8

    invoke-virtual {v10, v11}, Landroid/widget/CheckBox;->setVisibility(I)V

    .line 228
    const/4 v10, 0x0

    iput-object v10, p0, Lcom/google/android/setupwizard/user/GoogleServicesActivity;->mBackupCheckBox:Landroid/widget/CheckBox;

    goto/16 :goto_2

    .line 259
    :cond_d
    const/4 v6, 0x0

    goto :goto_3

    .line 264
    .restart local v6    # "hasNewAccount":Z
    :cond_e
    iget-object v10, p0, Lcom/google/android/setupwizard/user/GoogleServicesActivity;->mPlayEmailCheckBox:Landroid/widget/CheckBox;

    const/16 v11, 0x8

    invoke-virtual {v10, v11}, Landroid/widget/CheckBox;->setVisibility(I)V

    .line 265
    const/4 v10, 0x0

    iput-object v10, p0, Lcom/google/android/setupwizard/user/GoogleServicesActivity;->mPlayEmailCheckBox:Landroid/widget/CheckBox;

    goto :goto_4
.end method

.method public onCheckedChanged(Landroid/widget/CompoundButton;Z)V
    .locals 0
    .param p1, "buttonView"    # Landroid/widget/CompoundButton;
    .param p2, "isChecked"    # Z

    .prologue
    .line 298
    return-void
.end method

.method public onConnected(Landroid/os/Bundle;)V
    .locals 0
    .param p1, "bundle"    # Landroid/os/Bundle;

    .prologue
    .line 470
    return-void
.end method

.method public onConnectionFailed(Lcom/google/android/gms/common/ConnectionResult;)V
    .locals 3
    .param p1, "connectionResult"    # Lcom/google/android/gms/common/ConnectionResult;

    .prologue
    .line 481
    iget-object v0, p0, Lcom/google/android/setupwizard/user/GoogleServicesActivity;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "GMS connection has failed: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 482
    return-void
.end method

.method public onConnectionSuspended(I)V
    .locals 2
    .param p1, "cause"    # I

    .prologue
    .line 475
    iget-object v0, p0, Lcom/google/android/setupwizard/user/GoogleServicesActivity;->TAG:Ljava/lang/String;

    const-string v1, "GMS connection suspended"

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 476
    return-void
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 3
    .param p1, "icicle"    # Landroid/os/Bundle;

    .prologue
    .line 151
    invoke-super {p0, p1}, Lcom/google/android/setupwizard/BaseActivity;->onCreate(Landroid/os/Bundle;)V

    .line 153
    invoke-direct {p0}, Lcom/google/android/setupwizard/user/GoogleServicesActivity;->isVendorSetup()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 154
    iget-object v0, p0, Lcom/google/android/setupwizard/user/GoogleServicesActivity;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Starting GoogleServices for vendor, action: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Lcom/google/android/setupwizard/user/GoogleServicesActivity;->getIntent()Landroid/content/Intent;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 164
    :cond_0
    invoke-virtual {p0}, Lcom/google/android/setupwizard/user/GoogleServicesActivity;->initViews()V

    .line 165
    invoke-direct {p0}, Lcom/google/android/setupwizard/user/GoogleServicesActivity;->initUsageReportingClient()V

    .line 166
    invoke-direct {p0}, Lcom/google/android/setupwizard/user/GoogleServicesActivity;->loadPreferences()V

    .line 167
    :goto_0
    return-void

    .line 155
    :cond_1
    invoke-virtual {p0}, Lcom/google/android/setupwizard/user/GoogleServicesActivity;->isUsingWizardManager()Z

    move-result v0

    if-nez v0, :cond_0

    .line 158
    iget-object v0, p0, Lcom/google/android/setupwizard/user/GoogleServicesActivity;->TAG:Ljava/lang/String;

    const-string v1, "GoogleServices skipped. Not using WizardManager nor VENDOR_SETUP action."

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 159
    const/4 v0, -0x1

    invoke-virtual {p0, v0}, Lcom/google/android/setupwizard/user/GoogleServicesActivity;->setResultCode(I)V

    .line 160
    invoke-virtual {p0}, Lcom/google/android/setupwizard/user/GoogleServicesActivity;->finish()V

    goto :goto_0
.end method

.method protected onPause()V
    .locals 2

    .prologue
    .line 179
    iget-object v0, p0, Lcom/google/android/setupwizard/user/GoogleServicesActivity;->mUsageReportingClient:Lcom/google/android/gms/common/api/GoogleApiClient;

    invoke-interface {v0}, Lcom/google/android/gms/common/api/GoogleApiClient;->disconnect()V

    .line 180
    invoke-static {p0}, Landroid/support/v4/content/LocalBroadcastManager;->getInstance(Landroid/content/Context;)Landroid/support/v4/content/LocalBroadcastManager;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/setupwizard/user/GoogleServicesActivity;->mReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {v0, v1}, Landroid/support/v4/content/LocalBroadcastManager;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    .line 181
    invoke-direct {p0}, Lcom/google/android/setupwizard/user/GoogleServicesActivity;->savePreferences()V

    .line 182
    invoke-super {p0}, Lcom/google/android/setupwizard/BaseActivity;->onPause()V

    .line 183
    return-void
.end method

.method protected onResume()V
    .locals 3

    .prologue
    .line 171
    invoke-super {p0}, Lcom/google/android/setupwizard/BaseActivity;->onResume()V

    .line 172
    invoke-static {p0}, Landroid/support/v4/content/LocalBroadcastManager;->getInstance(Landroid/content/Context;)Landroid/support/v4/content/LocalBroadcastManager;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/setupwizard/user/GoogleServicesActivity;->mReceiver:Landroid/content/BroadcastReceiver;

    sget-object v2, Lcom/google/android/setupwizard/user/GoogleServicesActivity;->FILTER_LINK_SPAN_CLICKED:Landroid/content/IntentFilter;

    invoke-virtual {v0, v1, v2}, Landroid/support/v4/content/LocalBroadcastManager;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)V

    .line 174
    iget-object v0, p0, Lcom/google/android/setupwizard/user/GoogleServicesActivity;->mUsageReportingClient:Lcom/google/android/gms/common/api/GoogleApiClient;

    invoke-interface {v0}, Lcom/google/android/gms/common/api/GoogleApiClient;->connect()V

    .line 175
    return-void
.end method

.method public setOptIns()V
    .locals 1

    .prologue
    .line 348
    iget-object v0, p0, Lcom/google/android/setupwizard/user/GoogleServicesActivity;->mBackupCheckBox:Landroid/widget/CheckBox;

    if-eqz v0, :cond_0

    .line 349
    iget-object v0, p0, Lcom/google/android/setupwizard/user/GoogleServicesActivity;->mBackupCheckBox:Landroid/widget/CheckBox;

    invoke-virtual {v0}, Landroid/widget/CheckBox;->isChecked()Z

    move-result v0

    invoke-direct {p0, v0}, Lcom/google/android/setupwizard/user/GoogleServicesActivity;->setBackup(Z)V

    .line 351
    :cond_0
    iget-object v0, p0, Lcom/google/android/setupwizard/user/GoogleServicesActivity;->mLocationSharingCheckBox:Landroid/widget/CheckBox;

    if-eqz v0, :cond_1

    .line 352
    iget-object v0, p0, Lcom/google/android/setupwizard/user/GoogleServicesActivity;->mLocationSharingCheckBox:Landroid/widget/CheckBox;

    invoke-virtual {v0}, Landroid/widget/CheckBox;->isChecked()Z

    move-result v0

    invoke-direct {p0, v0}, Lcom/google/android/setupwizard/user/GoogleServicesActivity;->setLocationSharing(Z)V

    .line 354
    :cond_1
    iget-object v0, p0, Lcom/google/android/setupwizard/user/GoogleServicesActivity;->mLocationWifiScanCheckBox:Landroid/widget/CheckBox;

    if-eqz v0, :cond_2

    .line 355
    iget-object v0, p0, Lcom/google/android/setupwizard/user/GoogleServicesActivity;->mLocationWifiScanCheckBox:Landroid/widget/CheckBox;

    invoke-virtual {v0}, Landroid/widget/CheckBox;->isChecked()Z

    move-result v0

    invoke-direct {p0, v0}, Lcom/google/android/setupwizard/user/GoogleServicesActivity;->setLocationWifiScan(Z)V

    .line 357
    :cond_2
    iget-object v0, p0, Lcom/google/android/setupwizard/user/GoogleServicesActivity;->mUsageReportingCheckBox:Landroid/widget/CheckBox;

    if-eqz v0, :cond_3

    .line 358
    iget-object v0, p0, Lcom/google/android/setupwizard/user/GoogleServicesActivity;->mUsageReportingCheckBox:Landroid/widget/CheckBox;

    invoke-virtual {v0}, Landroid/widget/CheckBox;->isChecked()Z

    move-result v0

    invoke-direct {p0, v0}, Lcom/google/android/setupwizard/user/GoogleServicesActivity;->setUsageReporting(Z)V

    .line 360
    :cond_3
    iget-object v0, p0, Lcom/google/android/setupwizard/user/GoogleServicesActivity;->mPlayEmailCheckBox:Landroid/widget/CheckBox;

    if-eqz v0, :cond_4

    .line 361
    iget-object v0, p0, Lcom/google/android/setupwizard/user/GoogleServicesActivity;->mPlayEmailCheckBox:Landroid/widget/CheckBox;

    invoke-virtual {v0}, Landroid/widget/CheckBox;->isChecked()Z

    move-result v0

    invoke-direct {p0, v0}, Lcom/google/android/setupwizard/user/GoogleServicesActivity;->setPlayEmail(Z)V

    .line 369
    :cond_4
    const/4 v0, 0x1

    invoke-static {p0, v0}, Lcom/google/android/gsf/UseLocationForServices;->forceSetUseLocationForServices(Landroid/content/Context;Z)Z

    .line 370
    return-void
.end method

.method public start()V
    .locals 2

    .prologue
    const/4 v1, -0x1

    .line 285
    invoke-virtual {p0}, Lcom/google/android/setupwizard/user/GoogleServicesActivity;->setOptIns()V

    .line 286
    invoke-direct {p0}, Lcom/google/android/setupwizard/user/GoogleServicesActivity;->isVendorSetup()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 287
    invoke-virtual {p0, v1}, Lcom/google/android/setupwizard/user/GoogleServicesActivity;->setResultCode(I)V

    .line 288
    invoke-virtual {p0}, Lcom/google/android/setupwizard/user/GoogleServicesActivity;->finish()V

    .line 292
    :goto_0
    return-void

    .line 290
    :cond_0
    invoke-virtual {p0, v1}, Lcom/google/android/setupwizard/user/GoogleServicesActivity;->nextScreen(I)V

    goto :goto_0
.end method
