.class Lcom/google/android/setupwizard/user/WelcomeActivity$3;
.super Landroid/content/BroadcastReceiver;
.source "WelcomeActivity.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/setupwizard/user/WelcomeActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/setupwizard/user/WelcomeActivity;


# direct methods
.method constructor <init>(Lcom/google/android/setupwizard/user/WelcomeActivity;)V
    .locals 0

    .prologue
    .line 136
    iput-object p1, p0, Lcom/google/android/setupwizard/user/WelcomeActivity$3;->this$0:Lcom/google/android/setupwizard/user/WelcomeActivity;

    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    return-void
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 3
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "intent"    # Landroid/content/Intent;

    .prologue
    const/4 v2, 0x0

    .line 139
    const-string v1, "connected"

    invoke-virtual {p2, v1, v2}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v0

    .line 141
    .local v0, "connected":Z
    if-eqz v0, :cond_0

    .line 142
    iget-object v1, p0, Lcom/google/android/setupwizard/user/WelcomeActivity$3;->this$0:Lcom/google/android/setupwizard/user/WelcomeActivity;

    # invokes: Lcom/google/android/setupwizard/user/WelcomeActivity;->checkWarmWelcomeData()V
    invoke-static {v1}, Lcom/google/android/setupwizard/user/WelcomeActivity;->access$300(Lcom/google/android/setupwizard/user/WelcomeActivity;)V

    .line 147
    :goto_0
    return-void

    .line 145
    :cond_0
    iget-object v1, p0, Lcom/google/android/setupwizard/user/WelcomeActivity$3;->this$0:Lcom/google/android/setupwizard/user/WelcomeActivity;

    # invokes: Lcom/google/android/setupwizard/user/WelcomeActivity;->setWarmerWelcomeWaitingMode(Z)V
    invoke-static {v1, v2}, Lcom/google/android/setupwizard/user/WelcomeActivity;->access$400(Lcom/google/android/setupwizard/user/WelcomeActivity;Z)V

    goto :goto_0
.end method
