.class public Lcom/google/android/setupwizard/user/LocationSharingActivity;
.super Lcom/google/android/setupwizard/BaseActivity;
.source "LocationSharingActivity.java"


# instance fields
.field private mAgreeSharing:Landroid/widget/CompoundButton;

.field private mAgreeUseScanMode:Landroid/widget/CompoundButton;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 35
    invoke-direct {p0}, Lcom/google/android/setupwizard/BaseActivity;-><init>()V

    return-void
.end method

.method private initViews()V
    .locals 2

    .prologue
    .line 53
    const v0, 0x7f03000d

    const v1, 0x7f030023

    invoke-virtual {p0, v0, v1}, Lcom/google/android/setupwizard/user/LocationSharingActivity;->setTemplateContent(II)V

    .line 54
    const v0, 0x7f07007d

    invoke-virtual {p0, v0}, Lcom/google/android/setupwizard/user/LocationSharingActivity;->setHeaderText(I)V

    .line 55
    const v0, 0x7f0e004f

    invoke-virtual {p0, v0}, Lcom/google/android/setupwizard/user/LocationSharingActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/CompoundButton;

    iput-object v0, p0, Lcom/google/android/setupwizard/user/LocationSharingActivity;->mAgreeSharing:Landroid/widget/CompoundButton;

    .line 56
    const v0, 0x7f0e004b

    invoke-virtual {p0, v0}, Lcom/google/android/setupwizard/user/LocationSharingActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/CompoundButton;

    iput-object v0, p0, Lcom/google/android/setupwizard/user/LocationSharingActivity;->mAgreeUseScanMode:Landroid/widget/CompoundButton;

    .line 62
    invoke-virtual {p0}, Lcom/google/android/setupwizard/user/LocationSharingActivity;->getWindow()Landroid/view/Window;

    move-result-object v0

    const/4 v1, 0x3

    invoke-virtual {v0, v1}, Landroid/view/Window;->setSoftInputMode(I)V

    .line 63
    return-void
.end method

.method private loadPreferences()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    .line 106
    const/4 v1, 0x0

    invoke-virtual {p0, v1}, Lcom/google/android/setupwizard/user/LocationSharingActivity;->getPreferences(I)Landroid/content/SharedPreferences;

    move-result-object v0

    .line 109
    .local v0, "mSharedPrefs":Landroid/content/SharedPreferences;
    iget-object v1, p0, Lcom/google/android/setupwizard/user/LocationSharingActivity;->mAgreeSharing:Landroid/widget/CompoundButton;

    const-string v2, "agreeSharing"

    invoke-interface {v0, v2, v3}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v2

    invoke-virtual {v1, v2}, Landroid/widget/CompoundButton;->setChecked(Z)V

    .line 110
    iget-object v1, p0, Lcom/google/android/setupwizard/user/LocationSharingActivity;->mAgreeUseScanMode:Landroid/widget/CompoundButton;

    const-string v2, "agreeUseWifiScanMode"

    invoke-interface {v0, v2, v3}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v2

    invoke-virtual {v1, v2}, Landroid/widget/CompoundButton;->setChecked(Z)V

    .line 111
    return-void
.end method

.method private savePreferences()V
    .locals 4

    .prologue
    .line 96
    const/4 v2, 0x0

    invoke-virtual {p0, v2}, Lcom/google/android/setupwizard/user/LocationSharingActivity;->getPreferences(I)Landroid/content/SharedPreferences;

    move-result-object v1

    .line 97
    .local v1, "mSharedPrefs":Landroid/content/SharedPreferences;
    invoke-interface {v1}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    .line 99
    .local v0, "editor":Landroid/content/SharedPreferences$Editor;
    const-string v2, "agreeSharing"

    iget-object v3, p0, Lcom/google/android/setupwizard/user/LocationSharingActivity;->mAgreeSharing:Landroid/widget/CompoundButton;

    invoke-virtual {v3}, Landroid/widget/CompoundButton;->isChecked()Z

    move-result v3

    invoke-interface {v0, v2, v3}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    .line 100
    const-string v2, "agreeUseWifiScanMode"

    iget-object v3, p0, Lcom/google/android/setupwizard/user/LocationSharingActivity;->mAgreeUseScanMode:Landroid/widget/CompoundButton;

    invoke-virtual {v3}, Landroid/widget/CompoundButton;->isChecked()Z

    move-result v3

    invoke-interface {v0, v2, v3}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    .line 102
    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 103
    return-void
.end method


# virtual methods
.method protected onCreate(Landroid/os/Bundle;)V
    .locals 0
    .param p1, "icicle"    # Landroid/os/Bundle;

    .prologue
    .line 45
    invoke-super {p0, p1}, Lcom/google/android/setupwizard/BaseActivity;->onCreate(Landroid/os/Bundle;)V

    .line 46
    invoke-direct {p0}, Lcom/google/android/setupwizard/user/LocationSharingActivity;->initViews()V

    .line 47
    if-nez p1, :cond_0

    .line 48
    invoke-direct {p0}, Lcom/google/android/setupwizard/user/LocationSharingActivity;->loadPreferences()V

    .line 50
    :cond_0
    return-void
.end method

.method protected onPause()V
    .locals 0

    .prologue
    .line 67
    invoke-super {p0}, Lcom/google/android/setupwizard/BaseActivity;->onPause()V

    .line 69
    invoke-direct {p0}, Lcom/google/android/setupwizard/user/LocationSharingActivity;->savePreferences()V

    .line 70
    return-void
.end method

.method protected start()V
    .locals 4

    .prologue
    .line 74
    iget-object v2, p0, Lcom/google/android/setupwizard/user/LocationSharingActivity;->mAgreeSharing:Landroid/widget/CompoundButton;

    invoke-virtual {v2}, Landroid/widget/CompoundButton;->isChecked()Z

    move-result v0

    .line 75
    .local v0, "agreedSharing":Z
    invoke-virtual {p0}, Lcom/google/android/setupwizard/user/LocationSharingActivity;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    .line 76
    .local v1, "res":Landroid/content/ContentResolver;
    if-eqz v0, :cond_0

    .line 78
    const-string v2, "network_location_opt_in"

    const-string v3, "1"

    invoke-static {v1, v2, v3}, Lcom/google/android/gsf/GoogleSettingsContract$Partner;->putString(Landroid/content/ContentResolver;Ljava/lang/String;Ljava/lang/String;)Z

    .line 82
    :cond_0
    const-string v3, "wifi_scan_always_enabled"

    iget-object v2, p0, Lcom/google/android/setupwizard/user/LocationSharingActivity;->mAgreeUseScanMode:Landroid/widget/CompoundButton;

    invoke-virtual {v2}, Landroid/widget/CompoundButton;->isChecked()Z

    move-result v2

    if-eqz v2, :cond_1

    const/4 v2, 0x1

    :goto_0
    invoke-static {v1, v3, v2}, Landroid/provider/Settings$Global;->putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z

    .line 89
    const-string v2, "network"

    invoke-static {v1, v2, v0}, Landroid/provider/Settings$Secure;->setLocationProviderEnabled(Landroid/content/ContentResolver;Ljava/lang/String;Z)V

    .line 92
    invoke-virtual {p0}, Lcom/google/android/setupwizard/user/LocationSharingActivity;->nextScreen()V

    .line 93
    return-void

    .line 82
    :cond_1
    const/4 v2, 0x0

    goto :goto_0
.end method
