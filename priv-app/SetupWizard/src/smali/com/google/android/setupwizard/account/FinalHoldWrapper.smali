.class public Lcom/google/android/setupwizard/account/FinalHoldWrapper;
.super Lcom/google/android/setupwizard/SubactivityWrapper;
.source "FinalHoldWrapper.java"

# interfaces
.implements Lcom/google/android/setupwizard/util/GooglePlaySetupHelper$GooglePlayConnectionListener;


# instance fields
.field private mPlaySetupHelper:Lcom/google/android/setupwizard/util/GooglePlaySetupHelper;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 29
    invoke-direct {p0}, Lcom/google/android/setupwizard/SubactivityWrapper;-><init>()V

    return-void
.end method


# virtual methods
.method protected getSubactivityNextTransition()I
    .locals 1

    .prologue
    .line 80
    const/4 v0, 0x1

    return v0
.end method

.method protected getSubactivityPreviousTransition()I
    .locals 1

    .prologue
    .line 75
    const/4 v0, 0x1

    return v0
.end method

.method protected launchSubactivity()V
    .locals 1

    .prologue
    .line 48
    invoke-static {p0, p0}, Lcom/google/android/setupwizard/util/GooglePlaySetupHelper;->connect(Landroid/content/Context;Lcom/google/android/setupwizard/util/GooglePlaySetupHelper$GooglePlayConnectionListener;)Lcom/google/android/setupwizard/util/GooglePlaySetupHelper;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/setupwizard/account/FinalHoldWrapper;->mPlaySetupHelper:Lcom/google/android/setupwizard/util/GooglePlaySetupHelper;

    .line 49
    return-void
.end method

.method protected onDestroy()V
    .locals 1

    .prologue
    .line 53
    iget-object v0, p0, Lcom/google/android/setupwizard/account/FinalHoldWrapper;->mPlaySetupHelper:Lcom/google/android/setupwizard/util/GooglePlaySetupHelper;

    if-eqz v0, :cond_0

    .line 54
    iget-object v0, p0, Lcom/google/android/setupwizard/account/FinalHoldWrapper;->mPlaySetupHelper:Lcom/google/android/setupwizard/util/GooglePlaySetupHelper;

    invoke-virtual {v0}, Lcom/google/android/setupwizard/util/GooglePlaySetupHelper;->disconnect()V

    .line 55
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/setupwizard/account/FinalHoldWrapper;->mPlaySetupHelper:Lcom/google/android/setupwizard/util/GooglePlaySetupHelper;

    .line 57
    :cond_0
    invoke-super {p0}, Lcom/google/android/setupwizard/SubactivityWrapper;->onDestroy()V

    .line 58
    return-void
.end method

.method public onGooglePlayConnected(Lcom/google/android/setupwizard/util/GooglePlaySetupHelper;)V
    .locals 3
    .param p1, "helper"    # Lcom/google/android/setupwizard/util/GooglePlaySetupHelper;

    .prologue
    .line 62
    invoke-virtual {p1}, Lcom/google/android/setupwizard/util/GooglePlaySetupHelper;->getFinalHoldIntent()Landroid/content/Intent;

    move-result-object v0

    .line 64
    .local v0, "subactivityIntent":Landroid/content/Intent;
    if-eqz v0, :cond_0

    .line 65
    invoke-virtual {p0, v0}, Lcom/google/android/setupwizard/account/FinalHoldWrapper;->launchSubactivity(Landroid/content/Intent;)V

    .line 71
    :goto_0
    return-void

    .line 67
    :cond_0
    iget-object v1, p0, Lcom/google/android/setupwizard/account/FinalHoldWrapper;->TAG:Ljava/lang/String;

    const-string v2, "Null intent received for final hold"

    invoke-static {v1, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 68
    invoke-virtual {p0}, Lcom/google/android/setupwizard/account/FinalHoldWrapper;->nextScreen()V

    .line 69
    invoke-virtual {p0}, Lcom/google/android/setupwizard/account/FinalHoldWrapper;->finish()V

    goto :goto_0
.end method

.method protected onSubactivityLaunch()Landroid/content/Intent;
    .locals 1

    .prologue
    .line 43
    const/4 v0, 0x0

    return-object v0
.end method

.method protected requestCode()I
    .locals 1

    .prologue
    .line 37
    const/16 v0, 0x2733

    return v0
.end method
