.class Lcom/google/android/setupwizard/user/GoogleServicesActivity$1;
.super Landroid/content/BroadcastReceiver;
.source "GoogleServicesActivity.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/setupwizard/user/GoogleServicesActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/setupwizard/user/GoogleServicesActivity;


# direct methods
.method constructor <init>(Lcom/google/android/setupwizard/user/GoogleServicesActivity;)V
    .locals 0

    .prologue
    .line 104
    iput-object p1, p0, Lcom/google/android/setupwizard/user/GoogleServicesActivity$1;->this$0:Lcom/google/android/setupwizard/user/GoogleServicesActivity;

    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    return-void
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 4
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "intent"    # Landroid/content/Intent;

    .prologue
    .line 107
    const-string v1, "id"

    invoke-virtual {p2, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 108
    .local v0, "id":Ljava/lang/String;
    const-string v1, "backup_details"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 110
    const v1, 0x7f07002f

    invoke-static {v1}, Lcom/google/android/setupwizard/user/GoogleServicesActivity$LearnMoreDialog;->newInstance(I)Lcom/google/android/setupwizard/user/GoogleServicesActivity$LearnMoreDialog;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/setupwizard/user/GoogleServicesActivity$1;->this$0:Lcom/google/android/setupwizard/user/GoogleServicesActivity;

    invoke-virtual {v2}, Lcom/google/android/setupwizard/user/GoogleServicesActivity;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v2

    const-string v3, "dialog"

    invoke-virtual {v1, v2, v3}, Lcom/google/android/setupwizard/user/GoogleServicesActivity$LearnMoreDialog;->show(Landroid/app/FragmentManager;Ljava/lang/String;)V

    .line 146
    :cond_0
    :goto_0
    return-void

    .line 113
    :cond_1
    const-string v1, "location_details"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 114
    const v1, 0x7f070031

    invoke-static {v1}, Lcom/google/android/setupwizard/user/GoogleServicesActivity$LearnMoreDialog;->newInstance(I)Lcom/google/android/setupwizard/user/GoogleServicesActivity$LearnMoreDialog;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/setupwizard/user/GoogleServicesActivity$1;->this$0:Lcom/google/android/setupwizard/user/GoogleServicesActivity;

    invoke-virtual {v2}, Lcom/google/android/setupwizard/user/GoogleServicesActivity;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v2

    const-string v3, "dialog"

    invoke-virtual {v1, v2, v3}, Lcom/google/android/setupwizard/user/GoogleServicesActivity$LearnMoreDialog;->show(Landroid/app/FragmentManager;Ljava/lang/String;)V

    goto :goto_0

    .line 117
    :cond_2
    const-string v1, "usage_reporting_details"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_3

    .line 118
    const v1, 0x7f070036

    invoke-static {v1}, Lcom/google/android/setupwizard/user/GoogleServicesActivity$LearnMoreDialog;->newInstance(I)Lcom/google/android/setupwizard/user/GoogleServicesActivity$LearnMoreDialog;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/setupwizard/user/GoogleServicesActivity$1;->this$0:Lcom/google/android/setupwizard/user/GoogleServicesActivity;

    invoke-virtual {v2}, Lcom/google/android/setupwizard/user/GoogleServicesActivity;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v2

    const-string v3, "dialog"

    invoke-virtual {v1, v2, v3}, Lcom/google/android/setupwizard/user/GoogleServicesActivity$LearnMoreDialog;->show(Landroid/app/FragmentManager;Ljava/lang/String;)V

    goto :goto_0

    .line 121
    :cond_3
    const-string v1, "google_privacy"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_5

    .line 122
    iget-object v1, p0, Lcom/google/android/setupwizard/user/GoogleServicesActivity$1;->this$0:Lcom/google/android/setupwizard/user/GoogleServicesActivity;

    const-string v2, "de"

    # invokes: Lcom/google/android/setupwizard/user/GoogleServicesActivity;->isUserInCountry(Ljava/lang/String;)Z
    invoke-static {v1, v2}, Lcom/google/android/setupwizard/user/GoogleServicesActivity;->access$000(Lcom/google/android/setupwizard/user/GoogleServicesActivity;Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_4

    .line 124
    sget-object v1, Lcom/google/android/setupwizard/util/AndroidPolicy;->GOOGLE_PRIVACY_POLICY_GERMANY:Lcom/google/android/setupwizard/util/AndroidPolicy;

    invoke-static {v1}, Lcom/google/android/setupwizard/util/AndroidPolicy$PolicyDialogFragment;->newInstance(Lcom/google/android/setupwizard/util/AndroidPolicy;)Lcom/google/android/setupwizard/util/AndroidPolicy$PolicyDialogFragment;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/setupwizard/user/GoogleServicesActivity$1;->this$0:Lcom/google/android/setupwizard/user/GoogleServicesActivity;

    invoke-virtual {v2}, Lcom/google/android/setupwizard/user/GoogleServicesActivity;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v2

    const-string v3, "dialog"

    invoke-virtual {v1, v2, v3}, Lcom/google/android/setupwizard/util/AndroidPolicy$PolicyDialogFragment;->show(Landroid/app/FragmentManager;Ljava/lang/String;)V

    goto :goto_0

    .line 129
    :cond_4
    sget-object v1, Lcom/google/android/setupwizard/util/AndroidPolicy;->GOOGLE_PRIVACY_POLICY:Lcom/google/android/setupwizard/util/AndroidPolicy;

    invoke-static {v1}, Lcom/google/android/setupwizard/util/AndroidPolicy$PolicyDialogFragment;->newInstance(Lcom/google/android/setupwizard/util/AndroidPolicy;)Lcom/google/android/setupwizard/util/AndroidPolicy$PolicyDialogFragment;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/setupwizard/user/GoogleServicesActivity$1;->this$0:Lcom/google/android/setupwizard/user/GoogleServicesActivity;

    invoke-virtual {v2}, Lcom/google/android/setupwizard/user/GoogleServicesActivity;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v2

    const-string v3, "dialog"

    invoke-virtual {v1, v2, v3}, Lcom/google/android/setupwizard/util/AndroidPolicy$PolicyDialogFragment;->show(Landroid/app/FragmentManager;Ljava/lang/String;)V

    goto :goto_0

    .line 133
    :cond_5
    const-string v1, "tos"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 134
    iget-object v1, p0, Lcom/google/android/setupwizard/user/GoogleServicesActivity$1;->this$0:Lcom/google/android/setupwizard/user/GoogleServicesActivity;

    const-string v2, "de"

    # invokes: Lcom/google/android/setupwizard/user/GoogleServicesActivity;->isUserInCountry(Ljava/lang/String;)Z
    invoke-static {v1, v2}, Lcom/google/android/setupwizard/user/GoogleServicesActivity;->access$000(Lcom/google/android/setupwizard/user/GoogleServicesActivity;Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_6

    .line 136
    sget-object v1, Lcom/google/android/setupwizard/util/AndroidPolicy;->GOOGLE_TERMS_OF_SERVICE_GERMANY:Lcom/google/android/setupwizard/util/AndroidPolicy;

    invoke-static {v1}, Lcom/google/android/setupwizard/util/AndroidPolicy$PolicyDialogFragment;->newInstance(Lcom/google/android/setupwizard/util/AndroidPolicy;)Lcom/google/android/setupwizard/util/AndroidPolicy$PolicyDialogFragment;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/setupwizard/user/GoogleServicesActivity$1;->this$0:Lcom/google/android/setupwizard/user/GoogleServicesActivity;

    invoke-virtual {v2}, Lcom/google/android/setupwizard/user/GoogleServicesActivity;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v2

    const-string v3, "dialog"

    invoke-virtual {v1, v2, v3}, Lcom/google/android/setupwizard/util/AndroidPolicy$PolicyDialogFragment;->show(Landroid/app/FragmentManager;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 141
    :cond_6
    sget-object v1, Lcom/google/android/setupwizard/util/AndroidPolicy;->GOOGLE_TERMS_OF_SERVICE:Lcom/google/android/setupwizard/util/AndroidPolicy;

    invoke-static {v1}, Lcom/google/android/setupwizard/util/AndroidPolicy$PolicyDialogFragment;->newInstance(Lcom/google/android/setupwizard/util/AndroidPolicy;)Lcom/google/android/setupwizard/util/AndroidPolicy$PolicyDialogFragment;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/setupwizard/user/GoogleServicesActivity$1;->this$0:Lcom/google/android/setupwizard/user/GoogleServicesActivity;

    invoke-virtual {v2}, Lcom/google/android/setupwizard/user/GoogleServicesActivity;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v2

    const-string v3, "dialog"

    invoke-virtual {v1, v2, v3}, Lcom/google/android/setupwizard/util/AndroidPolicy$PolicyDialogFragment;->show(Landroid/app/FragmentManager;Ljava/lang/String;)V

    goto/16 :goto_0
.end method
