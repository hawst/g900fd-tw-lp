.class public Lcom/google/android/setupwizard/util/LinkAccessibilityHelper;
.super Lcom/google/android/setupwizard/util/ExploreByTouchHelper;
.source "LinkAccessibilityHelper.java"


# instance fields
.field private final mTempRect:Landroid/graphics/Rect;

.field private final mView:Landroid/widget/TextView;


# direct methods
.method public constructor <init>(Landroid/widget/TextView;)V
    .locals 1
    .param p1, "view"    # Landroid/widget/TextView;

    .prologue
    .line 22
    invoke-direct {p0, p1}, Lcom/google/android/setupwizard/util/ExploreByTouchHelper;-><init>(Landroid/view/View;)V

    .line 19
    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    iput-object v0, p0, Lcom/google/android/setupwizard/util/LinkAccessibilityHelper;->mTempRect:Landroid/graphics/Rect;

    .line 23
    iput-object p1, p0, Lcom/google/android/setupwizard/util/LinkAccessibilityHelper;->mView:Landroid/widget/TextView;

    .line 24
    return-void
.end method

.method private getBoundsForSpan(Lcom/google/android/setupwizard/util/LinkSpan;Landroid/graphics/Rect;)Landroid/graphics/Rect;
    .locals 11
    .param p1, "span"    # Lcom/google/android/setupwizard/util/LinkSpan;
    .param p2, "outRect"    # Landroid/graphics/Rect;

    .prologue
    .line 126
    iget-object v9, p0, Lcom/google/android/setupwizard/util/LinkAccessibilityHelper;->mView:Landroid/widget/TextView;

    invoke-virtual {v9}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v6

    .line 127
    .local v6, "text":Ljava/lang/CharSequence;
    invoke-virtual {p2}, Landroid/graphics/Rect;->setEmpty()V

    .line 128
    instance-of v9, v6, Landroid/text/Spanned;

    if-eqz v9, :cond_1

    move-object v5, v6

    .line 129
    check-cast v5, Landroid/text/Spanned;

    .line 130
    .local v5, "spannedText":Landroid/text/Spanned;
    invoke-interface {v5, p1}, Landroid/text/Spanned;->getSpanStart(Ljava/lang/Object;)I

    move-result v4

    .line 131
    .local v4, "spanStart":I
    invoke-interface {v5, p1}, Landroid/text/Spanned;->getSpanEnd(Ljava/lang/Object;)I

    move-result v3

    .line 132
    .local v3, "spanEnd":I
    iget-object v9, p0, Lcom/google/android/setupwizard/util/LinkAccessibilityHelper;->mView:Landroid/widget/TextView;

    invoke-virtual {v9}, Landroid/widget/TextView;->getLayout()Landroid/text/Layout;

    move-result-object v0

    .line 133
    .local v0, "layout":Landroid/text/Layout;
    invoke-virtual {v0, v4}, Landroid/text/Layout;->getPrimaryHorizontal(I)F

    move-result v8

    .line 134
    .local v8, "xStart":F
    invoke-virtual {v0, v3}, Landroid/text/Layout;->getPrimaryHorizontal(I)F

    move-result v7

    .line 135
    .local v7, "xEnd":F
    invoke-virtual {v0, v4}, Landroid/text/Layout;->getLineForOffset(I)I

    move-result v2

    .line 136
    .local v2, "lineStart":I
    invoke-virtual {v0, v3}, Landroid/text/Layout;->getLineForOffset(I)I

    move-result v1

    .line 137
    .local v1, "lineEnd":I
    invoke-virtual {v0, v2, p2}, Landroid/text/Layout;->getLineBounds(ILandroid/graphics/Rect;)I

    .line 138
    float-to-int v9, v8

    iput v9, p2, Landroid/graphics/Rect;->left:I

    .line 139
    if-ne v1, v2, :cond_0

    .line 140
    float-to-int v9, v7

    iput v9, p2, Landroid/graphics/Rect;->right:I

    .line 144
    :cond_0
    iget-object v9, p0, Lcom/google/android/setupwizard/util/LinkAccessibilityHelper;->mView:Landroid/widget/TextView;

    invoke-virtual {v9}, Landroid/widget/TextView;->getTotalPaddingLeft()I

    move-result v9

    iget-object v10, p0, Lcom/google/android/setupwizard/util/LinkAccessibilityHelper;->mView:Landroid/widget/TextView;

    invoke-virtual {v10}, Landroid/widget/TextView;->getTotalPaddingTop()I

    move-result v10

    invoke-virtual {p2, v9, v10}, Landroid/graphics/Rect;->offset(II)V

    .line 146
    .end local v0    # "layout":Landroid/text/Layout;
    .end local v1    # "lineEnd":I
    .end local v2    # "lineStart":I
    .end local v3    # "spanEnd":I
    .end local v4    # "spanStart":I
    .end local v5    # "spannedText":Landroid/text/Spanned;
    .end local v7    # "xEnd":F
    .end local v8    # "xStart":F
    :cond_1
    return-object p2
.end method

.method private getSpanForOffset(I)Lcom/google/android/setupwizard/util/LinkSpan;
    .locals 5
    .param p1, "offset"    # I

    .prologue
    .line 102
    iget-object v3, p0, Lcom/google/android/setupwizard/util/LinkAccessibilityHelper;->mView:Landroid/widget/TextView;

    invoke-virtual {v3}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v2

    .line 103
    .local v2, "text":Ljava/lang/CharSequence;
    instance-of v3, v2, Landroid/text/Spanned;

    if-eqz v3, :cond_0

    move-object v0, v2

    .line 104
    check-cast v0, Landroid/text/Spanned;

    .line 105
    .local v0, "spannedText":Landroid/text/Spanned;
    const-class v3, Lcom/google/android/setupwizard/util/LinkSpan;

    invoke-interface {v0, p1, p1, v3}, Landroid/text/Spanned;->getSpans(IILjava/lang/Class;)[Ljava/lang/Object;

    move-result-object v1

    check-cast v1, [Lcom/google/android/setupwizard/util/LinkSpan;

    .line 106
    .local v1, "spans":[Lcom/google/android/setupwizard/util/LinkSpan;
    array-length v3, v1

    const/4 v4, 0x1

    if-ne v3, v4, :cond_0

    .line 107
    const/4 v3, 0x0

    aget-object v3, v1, v3

    .line 110
    .end local v0    # "spannedText":Landroid/text/Spanned;
    .end local v1    # "spans":[Lcom/google/android/setupwizard/util/LinkSpan;
    :goto_0
    return-object v3

    :cond_0
    const/4 v3, 0x0

    goto :goto_0
.end method

.method private getTextForSpan(Lcom/google/android/setupwizard/util/LinkSpan;)Ljava/lang/CharSequence;
    .locals 4
    .param p1, "span"    # Lcom/google/android/setupwizard/util/LinkSpan;

    .prologue
    .line 114
    iget-object v2, p0, Lcom/google/android/setupwizard/util/LinkAccessibilityHelper;->mView:Landroid/widget/TextView;

    invoke-virtual {v2}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v1

    .line 115
    .local v1, "text":Ljava/lang/CharSequence;
    instance-of v2, v1, Landroid/text/Spanned;

    if-eqz v2, :cond_0

    move-object v0, v1

    .line 116
    check-cast v0, Landroid/text/Spanned;

    .line 117
    .local v0, "spannedText":Landroid/text/Spanned;
    invoke-interface {v0, p1}, Landroid/text/Spanned;->getSpanStart(Ljava/lang/Object;)I

    move-result v2

    invoke-interface {v0, p1}, Landroid/text/Spanned;->getSpanEnd(Ljava/lang/Object;)I

    move-result v3

    invoke-interface {v0, v2, v3}, Landroid/text/Spanned;->subSequence(II)Ljava/lang/CharSequence;

    move-result-object v1

    .line 120
    .end local v0    # "spannedText":Landroid/text/Spanned;
    .end local v1    # "text":Ljava/lang/CharSequence;
    :cond_0
    return-object v1
.end method


# virtual methods
.method protected getVirtualViewAt(FF)I
    .locals 7
    .param p1, "x"    # F
    .param p2, "y"    # F

    .prologue
    .line 28
    iget-object v5, p0, Lcom/google/android/setupwizard/util/LinkAccessibilityHelper;->mView:Landroid/widget/TextView;

    invoke-virtual {v5}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v4

    .line 29
    .local v4, "text":Ljava/lang/CharSequence;
    instance-of v5, v4, Landroid/text/Spanned;

    if-eqz v5, :cond_0

    move-object v3, v4

    .line 30
    check-cast v3, Landroid/text/Spanned;

    .line 31
    .local v3, "spannedText":Landroid/text/Spanned;
    iget-object v5, p0, Lcom/google/android/setupwizard/util/LinkAccessibilityHelper;->mView:Landroid/widget/TextView;

    invoke-virtual {v5, p1, p2}, Landroid/widget/TextView;->getOffsetForPosition(FF)I

    move-result v2

    .line 32
    .local v2, "offset":I
    const-class v5, Lcom/google/android/setupwizard/util/LinkSpan;

    invoke-interface {v3, v2, v2, v5}, Landroid/text/Spanned;->getSpans(IILjava/lang/Class;)[Ljava/lang/Object;

    move-result-object v1

    check-cast v1, [Lcom/google/android/setupwizard/util/LinkSpan;

    .line 33
    .local v1, "linkSpans":[Lcom/google/android/setupwizard/util/LinkSpan;
    array-length v5, v1

    const/4 v6, 0x1

    if-ne v5, v6, :cond_0

    .line 34
    const/4 v5, 0x0

    aget-object v0, v1, v5

    .line 35
    .local v0, "linkSpan":Lcom/google/android/setupwizard/util/LinkSpan;
    invoke-interface {v3, v0}, Landroid/text/Spanned;->getSpanStart(Ljava/lang/Object;)I

    move-result v5

    .line 38
    .end local v0    # "linkSpan":Lcom/google/android/setupwizard/util/LinkSpan;
    .end local v1    # "linkSpans":[Lcom/google/android/setupwizard/util/LinkSpan;
    .end local v2    # "offset":I
    .end local v3    # "spannedText":Landroid/text/Spanned;
    :goto_0
    return v5

    :cond_0
    const/high16 v5, -0x80000000

    goto :goto_0
.end method

.method protected getVisibleVirtualViews(Ljava/util/List;)V
    .locals 10
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Ljava/lang/Integer;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 43
    .local p1, "virtualViewIds":Ljava/util/List;, "Ljava/util/List<Ljava/lang/Integer;>;"
    iget-object v7, p0, Lcom/google/android/setupwizard/util/LinkAccessibilityHelper;->mView:Landroid/widget/TextView;

    invoke-virtual {v7}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v6

    .line 44
    .local v6, "text":Ljava/lang/CharSequence;
    instance-of v7, v6, Landroid/text/Spanned;

    if-eqz v7, :cond_0

    move-object v5, v6

    .line 45
    check-cast v5, Landroid/text/Spanned;

    .line 46
    .local v5, "spannedText":Landroid/text/Spanned;
    const/4 v7, 0x0

    invoke-interface {v5}, Landroid/text/Spanned;->length()I

    move-result v8

    const-class v9, Lcom/google/android/setupwizard/util/LinkSpan;

    invoke-interface {v5, v7, v8, v9}, Landroid/text/Spanned;->getSpans(IILjava/lang/Class;)[Ljava/lang/Object;

    move-result-object v3

    check-cast v3, [Lcom/google/android/setupwizard/util/LinkSpan;

    .line 47
    .local v3, "linkSpans":[Lcom/google/android/setupwizard/util/LinkSpan;
    move-object v0, v3

    .local v0, "arr$":[Lcom/google/android/setupwizard/util/LinkSpan;
    array-length v2, v0

    .local v2, "len$":I
    const/4 v1, 0x0

    .local v1, "i$":I
    :goto_0
    if-ge v1, v2, :cond_0

    aget-object v4, v0, v1

    .line 48
    .local v4, "span":Lcom/google/android/setupwizard/util/LinkSpan;
    invoke-interface {v5, v4}, Landroid/text/Spanned;->getSpanStart(Ljava/lang/Object;)I

    move-result v7

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    invoke-interface {p1, v7}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 47
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 51
    .end local v0    # "arr$":[Lcom/google/android/setupwizard/util/LinkSpan;
    .end local v1    # "i$":I
    .end local v2    # "len$":I
    .end local v3    # "linkSpans":[Lcom/google/android/setupwizard/util/LinkSpan;
    .end local v4    # "span":Lcom/google/android/setupwizard/util/LinkSpan;
    .end local v5    # "spannedText":Landroid/text/Spanned;
    :cond_0
    return-void
.end method

.method protected onPerformActionForVirtualView(IILandroid/os/Bundle;)Z
    .locals 4
    .param p1, "virtualViewId"    # I
    .param p2, "action"    # I
    .param p3, "arguments"    # Landroid/os/Bundle;

    .prologue
    .line 89
    const/16 v1, 0x10

    if-ne p2, v1, :cond_1

    .line 90
    invoke-direct {p0, p1}, Lcom/google/android/setupwizard/util/LinkAccessibilityHelper;->getSpanForOffset(I)Lcom/google/android/setupwizard/util/LinkSpan;

    move-result-object v0

    .line 91
    .local v0, "span":Lcom/google/android/setupwizard/util/LinkSpan;
    if-eqz v0, :cond_0

    .line 92
    iget-object v1, p0, Lcom/google/android/setupwizard/util/LinkAccessibilityHelper;->mView:Landroid/widget/TextView;

    invoke-virtual {v0, v1}, Lcom/google/android/setupwizard/util/LinkSpan;->onClick(Landroid/view/View;)V

    .line 93
    const/4 v1, 0x1

    .line 98
    .end local v0    # "span":Lcom/google/android/setupwizard/util/LinkSpan;
    :goto_0
    return v1

    .line 95
    .restart local v0    # "span":Lcom/google/android/setupwizard/util/LinkSpan;
    :cond_0
    const-string v1, "SetupWizard.LinkAccessibilityHelper"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "LinkSpan is null for offset: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 98
    .end local v0    # "span":Lcom/google/android/setupwizard/util/LinkSpan;
    :cond_1
    const/4 v1, 0x0

    goto :goto_0
.end method

.method protected onPopulateEventForVirtualView(ILandroid/view/accessibility/AccessibilityEvent;)V
    .locals 4
    .param p1, "virtualViewId"    # I
    .param p2, "event"    # Landroid/view/accessibility/AccessibilityEvent;

    .prologue
    .line 55
    invoke-direct {p0, p1}, Lcom/google/android/setupwizard/util/LinkAccessibilityHelper;->getSpanForOffset(I)Lcom/google/android/setupwizard/util/LinkSpan;

    move-result-object v0

    .line 56
    .local v0, "span":Lcom/google/android/setupwizard/util/LinkSpan;
    if-eqz v0, :cond_0

    .line 57
    invoke-direct {p0, v0}, Lcom/google/android/setupwizard/util/LinkAccessibilityHelper;->getTextForSpan(Lcom/google/android/setupwizard/util/LinkSpan;)Ljava/lang/CharSequence;

    move-result-object v1

    invoke-virtual {p2, v1}, Landroid/view/accessibility/AccessibilityEvent;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 62
    :goto_0
    return-void

    .line 59
    :cond_0
    const-string v1, "SetupWizard.LinkAccessibilityHelper"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "LinkSpan is null for offset: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 60
    iget-object v1, p0, Lcom/google/android/setupwizard/util/LinkAccessibilityHelper;->mView:Landroid/widget/TextView;

    invoke-virtual {v1}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v1

    invoke-virtual {p2, v1}, Landroid/view/accessibility/AccessibilityEvent;->setContentDescription(Ljava/lang/CharSequence;)V

    goto :goto_0
.end method

.method protected onPopulateNodeForVirtualView(ILandroid/view/accessibility/AccessibilityNodeInfo;)V
    .locals 6
    .param p1, "virtualViewId"    # I
    .param p2, "info"    # Landroid/view/accessibility/AccessibilityNodeInfo;

    .prologue
    const/4 v5, 0x0

    const/4 v4, 0x1

    .line 66
    invoke-direct {p0, p1}, Lcom/google/android/setupwizard/util/LinkAccessibilityHelper;->getSpanForOffset(I)Lcom/google/android/setupwizard/util/LinkSpan;

    move-result-object v0

    .line 67
    .local v0, "span":Lcom/google/android/setupwizard/util/LinkSpan;
    if-eqz v0, :cond_0

    .line 68
    invoke-direct {p0, v0}, Lcom/google/android/setupwizard/util/LinkAccessibilityHelper;->getTextForSpan(Lcom/google/android/setupwizard/util/LinkSpan;)Ljava/lang/CharSequence;

    move-result-object v1

    invoke-virtual {p2, v1}, Landroid/view/accessibility/AccessibilityNodeInfo;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 73
    :goto_0
    invoke-virtual {p2, v4}, Landroid/view/accessibility/AccessibilityNodeInfo;->setFocusable(Z)V

    .line 74
    invoke-virtual {p2, v4}, Landroid/view/accessibility/AccessibilityNodeInfo;->setClickable(Z)V

    .line 75
    iget-object v1, p0, Lcom/google/android/setupwizard/util/LinkAccessibilityHelper;->mTempRect:Landroid/graphics/Rect;

    invoke-direct {p0, v0, v1}, Lcom/google/android/setupwizard/util/LinkAccessibilityHelper;->getBoundsForSpan(Lcom/google/android/setupwizard/util/LinkSpan;Landroid/graphics/Rect;)Landroid/graphics/Rect;

    .line 76
    iget-object v1, p0, Lcom/google/android/setupwizard/util/LinkAccessibilityHelper;->mTempRect:Landroid/graphics/Rect;

    invoke-virtual {v1}, Landroid/graphics/Rect;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_1

    .line 77
    iget-object v1, p0, Lcom/google/android/setupwizard/util/LinkAccessibilityHelper;->mTempRect:Landroid/graphics/Rect;

    invoke-direct {p0, v0, v1}, Lcom/google/android/setupwizard/util/LinkAccessibilityHelper;->getBoundsForSpan(Lcom/google/android/setupwizard/util/LinkSpan;Landroid/graphics/Rect;)Landroid/graphics/Rect;

    move-result-object v1

    invoke-virtual {p2, v1}, Landroid/view/accessibility/AccessibilityNodeInfo;->setBoundsInParent(Landroid/graphics/Rect;)V

    .line 83
    :goto_1
    const/16 v1, 0x10

    invoke-virtual {p2, v1}, Landroid/view/accessibility/AccessibilityNodeInfo;->addAction(I)V

    .line 84
    return-void

    .line 70
    :cond_0
    const-string v1, "SetupWizard.LinkAccessibilityHelper"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "LinkSpan is null for offset: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 71
    iget-object v1, p0, Lcom/google/android/setupwizard/util/LinkAccessibilityHelper;->mView:Landroid/widget/TextView;

    invoke-virtual {v1}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v1

    invoke-virtual {p2, v1}, Landroid/view/accessibility/AccessibilityNodeInfo;->setContentDescription(Ljava/lang/CharSequence;)V

    goto :goto_0

    .line 79
    :cond_1
    const-string v1, "SetupWizard.LinkAccessibilityHelper"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "LinkSpan bounds is empty for: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 80
    iget-object v1, p0, Lcom/google/android/setupwizard/util/LinkAccessibilityHelper;->mTempRect:Landroid/graphics/Rect;

    invoke-virtual {v1, v5, v5, v4, v4}, Landroid/graphics/Rect;->set(IIII)V

    .line 81
    iget-object v1, p0, Lcom/google/android/setupwizard/util/LinkAccessibilityHelper;->mTempRect:Landroid/graphics/Rect;

    invoke-virtual {p2, v1}, Landroid/view/accessibility/AccessibilityNodeInfo;->setBoundsInParent(Landroid/graphics/Rect;)V

    goto :goto_1
.end method
