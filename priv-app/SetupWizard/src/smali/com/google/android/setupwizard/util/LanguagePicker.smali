.class public Lcom/google/android/setupwizard/util/LanguagePicker;
.super Landroid/widget/FrameLayout;
.source "LanguagePicker.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/setupwizard/util/LanguagePicker$OnLanguageSelectedListener;
    }
.end annotation


# instance fields
.field private mNumberPicker:Landroid/widget/NumberPicker;

.field private final mOnItemSelectedListener:Landroid/widget/AdapterView$OnItemSelectedListener;

.field private mOnLanguageSelectedListener:Lcom/google/android/setupwizard/util/LanguagePicker$OnLanguageSelectedListener;

.field private final mOnValueChangedListener:Landroid/widget/NumberPicker$OnValueChangeListener;

.field private mSpinner:Landroid/widget/Spinner;

.field private mSpinnerAdapter:Landroid/widget/ArrayAdapter;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 48
    invoke-direct {p0, p1}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;)V

    .line 78
    new-instance v0, Lcom/google/android/setupwizard/util/LanguagePicker$1;

    invoke-direct {v0, p0}, Lcom/google/android/setupwizard/util/LanguagePicker$1;-><init>(Lcom/google/android/setupwizard/util/LanguagePicker;)V

    iput-object v0, p0, Lcom/google/android/setupwizard/util/LanguagePicker;->mOnValueChangedListener:Landroid/widget/NumberPicker$OnValueChangeListener;

    .line 88
    new-instance v0, Lcom/google/android/setupwizard/util/LanguagePicker$2;

    invoke-direct {v0, p0}, Lcom/google/android/setupwizard/util/LanguagePicker$2;-><init>(Lcom/google/android/setupwizard/util/LanguagePicker;)V

    iput-object v0, p0, Lcom/google/android/setupwizard/util/LanguagePicker;->mOnItemSelectedListener:Landroid/widget/AdapterView$OnItemSelectedListener;

    .line 49
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    .line 52
    invoke-direct {p0, p1, p2}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 78
    new-instance v0, Lcom/google/android/setupwizard/util/LanguagePicker$1;

    invoke-direct {v0, p0}, Lcom/google/android/setupwizard/util/LanguagePicker$1;-><init>(Lcom/google/android/setupwizard/util/LanguagePicker;)V

    iput-object v0, p0, Lcom/google/android/setupwizard/util/LanguagePicker;->mOnValueChangedListener:Landroid/widget/NumberPicker$OnValueChangeListener;

    .line 88
    new-instance v0, Lcom/google/android/setupwizard/util/LanguagePicker$2;

    invoke-direct {v0, p0}, Lcom/google/android/setupwizard/util/LanguagePicker$2;-><init>(Lcom/google/android/setupwizard/util/LanguagePicker;)V

    iput-object v0, p0, Lcom/google/android/setupwizard/util/LanguagePicker;->mOnItemSelectedListener:Landroid/widget/AdapterView$OnItemSelectedListener;

    .line 53
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;
    .param p3, "defStyleAttr"    # I

    .prologue
    .line 56
    invoke-direct {p0, p1, p2, p3}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 78
    new-instance v0, Lcom/google/android/setupwizard/util/LanguagePicker$1;

    invoke-direct {v0, p0}, Lcom/google/android/setupwizard/util/LanguagePicker$1;-><init>(Lcom/google/android/setupwizard/util/LanguagePicker;)V

    iput-object v0, p0, Lcom/google/android/setupwizard/util/LanguagePicker;->mOnValueChangedListener:Landroid/widget/NumberPicker$OnValueChangeListener;

    .line 88
    new-instance v0, Lcom/google/android/setupwizard/util/LanguagePicker$2;

    invoke-direct {v0, p0}, Lcom/google/android/setupwizard/util/LanguagePicker$2;-><init>(Lcom/google/android/setupwizard/util/LanguagePicker;)V

    iput-object v0, p0, Lcom/google/android/setupwizard/util/LanguagePicker;->mOnItemSelectedListener:Landroid/widget/AdapterView$OnItemSelectedListener;

    .line 57
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;II)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;
    .param p3, "defStyleAttr"    # I
    .param p4, "defStyleRes"    # I

    .prologue
    .line 60
    invoke-direct {p0, p1, p2, p3, p4}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;II)V

    .line 78
    new-instance v0, Lcom/google/android/setupwizard/util/LanguagePicker$1;

    invoke-direct {v0, p0}, Lcom/google/android/setupwizard/util/LanguagePicker$1;-><init>(Lcom/google/android/setupwizard/util/LanguagePicker;)V

    iput-object v0, p0, Lcom/google/android/setupwizard/util/LanguagePicker;->mOnValueChangedListener:Landroid/widget/NumberPicker$OnValueChangeListener;

    .line 88
    new-instance v0, Lcom/google/android/setupwizard/util/LanguagePicker$2;

    invoke-direct {v0, p0}, Lcom/google/android/setupwizard/util/LanguagePicker$2;-><init>(Lcom/google/android/setupwizard/util/LanguagePicker;)V

    iput-object v0, p0, Lcom/google/android/setupwizard/util/LanguagePicker;->mOnItemSelectedListener:Landroid/widget/AdapterView$OnItemSelectedListener;

    .line 61
    return-void
.end method

.method static synthetic access$000(Lcom/google/android/setupwizard/util/LanguagePicker;)Lcom/google/android/setupwizard/util/LanguagePicker$OnLanguageSelectedListener;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/setupwizard/util/LanguagePicker;

    .prologue
    .line 39
    iget-object v0, p0, Lcom/google/android/setupwizard/util/LanguagePicker;->mOnLanguageSelectedListener:Lcom/google/android/setupwizard/util/LanguagePicker$OnLanguageSelectedListener;

    return-object v0
.end method


# virtual methods
.method protected onFinishInflate()V
    .locals 3

    .prologue
    .line 65
    invoke-super {p0}, Landroid/widget/FrameLayout;->onFinishInflate()V

    .line 67
    const v0, 0x7f0e004d

    invoke-virtual {p0, v0}, Lcom/google/android/setupwizard/util/LanguagePicker;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/NumberPicker;

    iput-object v0, p0, Lcom/google/android/setupwizard/util/LanguagePicker;->mNumberPicker:Landroid/widget/NumberPicker;

    .line 68
    const v0, 0x7f0e004e

    invoke-virtual {p0, v0}, Lcom/google/android/setupwizard/util/LanguagePicker;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Spinner;

    iput-object v0, p0, Lcom/google/android/setupwizard/util/LanguagePicker;->mSpinner:Landroid/widget/Spinner;

    .line 70
    new-instance v0, Landroid/widget/ArrayAdapter;

    invoke-virtual {p0}, Lcom/google/android/setupwizard/util/LanguagePicker;->getContext()Landroid/content/Context;

    move-result-object v1

    const v2, 0x1090008

    invoke-direct {v0, v1, v2}, Landroid/widget/ArrayAdapter;-><init>(Landroid/content/Context;I)V

    iput-object v0, p0, Lcom/google/android/setupwizard/util/LanguagePicker;->mSpinnerAdapter:Landroid/widget/ArrayAdapter;

    .line 71
    iget-object v0, p0, Lcom/google/android/setupwizard/util/LanguagePicker;->mSpinnerAdapter:Landroid/widget/ArrayAdapter;

    const v1, 0x1090009

    invoke-virtual {v0, v1}, Landroid/widget/ArrayAdapter;->setDropDownViewResource(I)V

    .line 72
    iget-object v0, p0, Lcom/google/android/setupwizard/util/LanguagePicker;->mSpinner:Landroid/widget/Spinner;

    iget-object v1, p0, Lcom/google/android/setupwizard/util/LanguagePicker;->mSpinnerAdapter:Landroid/widget/ArrayAdapter;

    invoke-virtual {v0, v1}, Landroid/widget/Spinner;->setAdapter(Landroid/widget/SpinnerAdapter;)V

    .line 75
    iget-object v0, p0, Lcom/google/android/setupwizard/util/LanguagePicker;->mNumberPicker:Landroid/widget/NumberPicker;

    const/4 v1, 0x2

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/widget/NumberPicker;->setLayerType(ILandroid/graphics/Paint;)V

    .line 76
    return-void
.end method

.method public setLocales([Ljava/lang/String;I)V
    .locals 3
    .param p1, "localeNames"    # [Ljava/lang/String;
    .param p2, "selectedPosition"    # I

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x0

    .line 115
    iget-object v0, p0, Lcom/google/android/setupwizard/util/LanguagePicker;->mNumberPicker:Landroid/widget/NumberPicker;

    invoke-virtual {v0, v2}, Landroid/widget/NumberPicker;->setOnValueChangedListener(Landroid/widget/NumberPicker$OnValueChangeListener;)V

    .line 116
    iget-object v0, p0, Lcom/google/android/setupwizard/util/LanguagePicker;->mNumberPicker:Landroid/widget/NumberPicker;

    invoke-virtual {v0, v1}, Landroid/widget/NumberPicker;->setValue(I)V

    .line 117
    iget-object v0, p0, Lcom/google/android/setupwizard/util/LanguagePicker;->mNumberPicker:Landroid/widget/NumberPicker;

    invoke-virtual {v0, v1}, Landroid/widget/NumberPicker;->setMaxValue(I)V

    .line 118
    iget-object v0, p0, Lcom/google/android/setupwizard/util/LanguagePicker;->mNumberPicker:Landroid/widget/NumberPicker;

    invoke-virtual {v0, p1}, Landroid/widget/NumberPicker;->setDisplayedValues([Ljava/lang/String;)V

    .line 119
    iget-object v0, p0, Lcom/google/android/setupwizard/util/LanguagePicker;->mNumberPicker:Landroid/widget/NumberPicker;

    array-length v1, p1

    add-int/lit8 v1, v1, -0x1

    invoke-virtual {v0, v1}, Landroid/widget/NumberPicker;->setMaxValue(I)V

    .line 120
    iget-object v0, p0, Lcom/google/android/setupwizard/util/LanguagePicker;->mNumberPicker:Landroid/widget/NumberPicker;

    invoke-virtual {v0, p2}, Landroid/widget/NumberPicker;->setValue(I)V

    .line 121
    iget-object v0, p0, Lcom/google/android/setupwizard/util/LanguagePicker;->mNumberPicker:Landroid/widget/NumberPicker;

    const/high16 v1, 0x60000

    invoke-virtual {v0, v1}, Landroid/widget/NumberPicker;->setDescendantFocusability(I)V

    .line 122
    iget-object v0, p0, Lcom/google/android/setupwizard/util/LanguagePicker;->mNumberPicker:Landroid/widget/NumberPicker;

    iget-object v1, p0, Lcom/google/android/setupwizard/util/LanguagePicker;->mOnValueChangedListener:Landroid/widget/NumberPicker$OnValueChangeListener;

    invoke-virtual {v0, v1}, Landroid/widget/NumberPicker;->setOnValueChangedListener(Landroid/widget/NumberPicker$OnValueChangeListener;)V

    .line 125
    iget-object v0, p0, Lcom/google/android/setupwizard/util/LanguagePicker;->mSpinner:Landroid/widget/Spinner;

    invoke-virtual {v0, v2}, Landroid/widget/Spinner;->setOnItemSelectedListener(Landroid/widget/AdapterView$OnItemSelectedListener;)V

    .line 126
    iget-object v0, p0, Lcom/google/android/setupwizard/util/LanguagePicker;->mSpinnerAdapter:Landroid/widget/ArrayAdapter;

    invoke-virtual {v0}, Landroid/widget/ArrayAdapter;->clear()V

    .line 127
    iget-object v0, p0, Lcom/google/android/setupwizard/util/LanguagePicker;->mSpinnerAdapter:Landroid/widget/ArrayAdapter;

    invoke-static {p1}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/ArrayAdapter;->addAll(Ljava/util/Collection;)V

    .line 128
    iget-object v0, p0, Lcom/google/android/setupwizard/util/LanguagePicker;->mSpinnerAdapter:Landroid/widget/ArrayAdapter;

    invoke-virtual {v0}, Landroid/widget/ArrayAdapter;->notifyDataSetChanged()V

    .line 129
    iget-object v0, p0, Lcom/google/android/setupwizard/util/LanguagePicker;->mSpinner:Landroid/widget/Spinner;

    invoke-virtual {v0, p2}, Landroid/widget/Spinner;->setSelection(I)V

    .line 130
    iget-object v0, p0, Lcom/google/android/setupwizard/util/LanguagePicker;->mSpinner:Landroid/widget/Spinner;

    iget-object v1, p0, Lcom/google/android/setupwizard/util/LanguagePicker;->mOnItemSelectedListener:Landroid/widget/AdapterView$OnItemSelectedListener;

    invoke-virtual {v0, v1}, Landroid/widget/Spinner;->setOnItemSelectedListener(Landroid/widget/AdapterView$OnItemSelectedListener;)V

    .line 131
    return-void
.end method

.method public setOnLanguageSelectedListener(Lcom/google/android/setupwizard/util/LanguagePicker$OnLanguageSelectedListener;)V
    .locals 0
    .param p1, "listener"    # Lcom/google/android/setupwizard/util/LanguagePicker$OnLanguageSelectedListener;

    .prologue
    .line 134
    iput-object p1, p0, Lcom/google/android/setupwizard/util/LanguagePicker;->mOnLanguageSelectedListener:Lcom/google/android/setupwizard/util/LanguagePicker$OnLanguageSelectedListener;

    .line 135
    return-void
.end method

.method public useNumberPicker()V
    .locals 2

    .prologue
    .line 104
    iget-object v0, p0, Lcom/google/android/setupwizard/util/LanguagePicker;->mNumberPicker:Landroid/widget/NumberPicker;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/NumberPicker;->setVisibility(I)V

    .line 105
    iget-object v0, p0, Lcom/google/android/setupwizard/util/LanguagePicker;->mSpinner:Landroid/widget/Spinner;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/Spinner;->setVisibility(I)V

    .line 106
    return-void
.end method

.method public useSpinner()V
    .locals 2

    .prologue
    .line 109
    iget-object v0, p0, Lcom/google/android/setupwizard/util/LanguagePicker;->mNumberPicker:Landroid/widget/NumberPicker;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/NumberPicker;->setVisibility(I)V

    .line 110
    iget-object v0, p0, Lcom/google/android/setupwizard/util/LanguagePicker;->mSpinner:Landroid/widget/Spinner;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/Spinner;->setVisibility(I)V

    .line 111
    return-void
.end method
