.class public Lcom/google/android/setupwizard/update/OtaUpdateActivity;
.super Lcom/google/android/setupwizard/BaseActivity;
.source "OtaUpdateActivity.java"


# static fields
.field private static final TOS_MAP:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Lcom/google/android/setupwizard/util/AndroidPolicy;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private final mLinkSpanReceiver:Landroid/content/BroadcastReceiver;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    .line 50
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    sput-object v0, Lcom/google/android/setupwizard/update/OtaUpdateActivity;->TOS_MAP:Ljava/util/HashMap;

    .line 52
    sget-object v0, Lcom/google/android/setupwizard/update/OtaUpdateActivity;->TOS_MAP:Ljava/util/HashMap;

    const-string v1, "tos"

    sget-object v2, Lcom/google/android/setupwizard/util/AndroidPolicy;->GOOGLE_TERMS_OF_SERVICE:Lcom/google/android/setupwizard/util/AndroidPolicy;

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 53
    sget-object v0, Lcom/google/android/setupwizard/update/OtaUpdateActivity;->TOS_MAP:Ljava/util/HashMap;

    const-string v1, "google_privacy"

    sget-object v2, Lcom/google/android/setupwizard/util/AndroidPolicy;->GOOGLE_PRIVACY_POLICY:Lcom/google/android/setupwizard/util/AndroidPolicy;

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 54
    sget-object v0, Lcom/google/android/setupwizard/update/OtaUpdateActivity;->TOS_MAP:Ljava/util/HashMap;

    const-string v1, "google_plus_privacy"

    sget-object v2, Lcom/google/android/setupwizard/util/AndroidPolicy;->GOOGLE_PLUS_PRIVACY_POLICY:Lcom/google/android/setupwizard/util/AndroidPolicy;

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 55
    sget-object v0, Lcom/google/android/setupwizard/update/OtaUpdateActivity;->TOS_MAP:Ljava/util/HashMap;

    const-string v1, "android_privacy"

    sget-object v2, Lcom/google/android/setupwizard/util/AndroidPolicy;->ANDROID_PRIVACY_POLICY:Lcom/google/android/setupwizard/util/AndroidPolicy;

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 56
    sget-object v0, Lcom/google/android/setupwizard/update/OtaUpdateActivity;->TOS_MAP:Ljava/util/HashMap;

    const-string v1, "play_tos"

    sget-object v2, Lcom/google/android/setupwizard/util/AndroidPolicy;->GOOGLE_PLAY_TOS_POLICY:Lcom/google/android/setupwizard/util/AndroidPolicy;

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 57
    sget-object v0, Lcom/google/android/setupwizard/update/OtaUpdateActivity;->TOS_MAP:Ljava/util/HashMap;

    const-string v1, "chrome_tos"

    sget-object v2, Lcom/google/android/setupwizard/util/AndroidPolicy;->CHROME_TERMS_OF_SERVICE:Lcom/google/android/setupwizard/util/AndroidPolicy;

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 58
    sget-object v0, Lcom/google/android/setupwizard/update/OtaUpdateActivity;->TOS_MAP:Ljava/util/HashMap;

    const-string v1, "chrome_privacy"

    sget-object v2, Lcom/google/android/setupwizard/util/AndroidPolicy;->CHROME_PRIVACY_POLICY:Lcom/google/android/setupwizard/util/AndroidPolicy;

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 59
    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 48
    invoke-direct {p0}, Lcom/google/android/setupwizard/BaseActivity;-><init>()V

    .line 70
    new-instance v0, Lcom/google/android/setupwizard/update/OtaUpdateActivity$1;

    invoke-direct {v0, p0}, Lcom/google/android/setupwizard/update/OtaUpdateActivity$1;-><init>(Lcom/google/android/setupwizard/update/OtaUpdateActivity;)V

    iput-object v0, p0, Lcom/google/android/setupwizard/update/OtaUpdateActivity;->mLinkSpanReceiver:Landroid/content/BroadcastReceiver;

    return-void
.end method

.method static synthetic access$000()Ljava/util/HashMap;
    .locals 1

    .prologue
    .line 48
    sget-object v0, Lcom/google/android/setupwizard/update/OtaUpdateActivity;->TOS_MAP:Ljava/util/HashMap;

    return-object v0
.end method


# virtual methods
.method public onCreate(Landroid/os/Bundle;)V
    .locals 9
    .param p1, "icicle"    # Landroid/os/Bundle;

    .prologue
    const/4 v8, 0x1

    .line 81
    invoke-super {p0, p1}, Lcom/google/android/setupwizard/BaseActivity;->onCreate(Landroid/os/Bundle;)V

    .line 83
    invoke-virtual {p0}, Lcom/google/android/setupwizard/update/OtaUpdateActivity;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v5

    const-string v6, "update_url"

    invoke-static {v5, v6}, Lcom/google/android/gsf/Gservices;->getString(Landroid/content/ContentResolver;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 84
    .local v4, "updateUrl":Ljava/lang/String;
    invoke-virtual {p0}, Lcom/google/android/setupwizard/update/OtaUpdateActivity;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v5

    const-string v6, "update_size"

    invoke-static {v5, v6}, Lcom/google/android/gsf/Gservices;->getString(Landroid/content/ContentResolver;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 85
    .local v3, "updateSize":Ljava/lang/String;
    invoke-virtual {p0}, Lcom/google/android/setupwizard/update/OtaUpdateActivity;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v5

    const-string v6, "update_required_setup"

    invoke-static {v5, v6}, Lcom/google/android/gsf/Gservices;->getString(Landroid/content/ContentResolver;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 88
    .local v2, "updateRequiredSetup":Ljava/lang/String;
    iget-object v5, p0, Lcom/google/android/setupwizard/update/OtaUpdateActivity;->TAG:Ljava/lang/String;

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "Checking update: isPrimaryUser="

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {p0}, Lcom/google/android/setupwizard/update/OtaUpdateActivity;->isPrimaryUser()Z

    move-result v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, " updateUrl="

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, " updateSize="

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, " updateRequiredSetup="

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 90
    invoke-virtual {p0}, Lcom/google/android/setupwizard/update/OtaUpdateActivity;->isPrimaryUser()Z

    move-result v5

    if-eqz v5, :cond_0

    invoke-static {v4}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v5

    if-nez v5, :cond_0

    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v5

    if-eqz v5, :cond_1

    .line 93
    :cond_0
    invoke-virtual {p0, v8}, Lcom/google/android/setupwizard/update/OtaUpdateActivity;->finishAction(I)V

    .line 112
    :goto_0
    return-void

    .line 97
    :cond_1
    invoke-virtual {p0}, Lcom/google/android/setupwizard/update/OtaUpdateActivity;->getAccountDomains()[Ljava/lang/String;

    move-result-object v5

    if-eqz v5, :cond_2

    .line 99
    invoke-virtual {p0}, Lcom/google/android/setupwizard/update/OtaUpdateActivity;->start()V

    goto :goto_0

    .line 103
    :cond_2
    const v5, 0x7f030012

    const v6, 0x7f030023

    invoke-virtual {p0, v5, v6}, Lcom/google/android/setupwizard/update/OtaUpdateActivity;->setTemplateContent(II)V

    .line 104
    const v5, 0x7f07008b

    invoke-virtual {p0, v5}, Lcom/google/android/setupwizard/update/OtaUpdateActivity;->setHeaderText(I)V

    .line 106
    const v5, 0x7f0e0053

    invoke-virtual {p0, v5}, Lcom/google/android/setupwizard/update/OtaUpdateActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 107
    .local v0, "size":Landroid/widget/TextView;
    const v5, 0x7f07008d

    new-array v6, v8, [Ljava/lang/Object;

    const/4 v7, 0x0

    aput-object v3, v6, v7

    invoke-virtual {p0, v5, v6}, Lcom/google/android/setupwizard/update/OtaUpdateActivity;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v0, v5}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 109
    const v5, 0x7f0e0054

    invoke-virtual {p0, v5}, Lcom/google/android/setupwizard/update/OtaUpdateActivity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    .line 110
    .local v1, "terms":Landroid/widget/TextView;
    const v5, 0x7f07008e

    sget-object v6, Lcom/google/android/setupwizard/update/OtaUpdateActivity;->TOS_MAP:Ljava/util/HashMap;

    invoke-virtual {v6}, Ljava/util/HashMap;->keySet()Ljava/util/Set;

    move-result-object v6

    invoke-static {p0, v5, v6}, Lcom/google/android/setupwizard/util/LinkSpan;->linkify(Landroid/content/Context;ILjava/util/Set;)Ljava/lang/CharSequence;

    move-result-object v5

    invoke-virtual {v1, v5}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 111
    invoke-static {}, Landroid/text/method/LinkMovementMethod;->getInstance()Landroid/text/method/MovementMethod;

    move-result-object v5

    invoke-virtual {v1, v5}, Landroid/widget/TextView;->setMovementMethod(Landroid/text/method/MovementMethod;)V

    goto :goto_0
.end method

.method protected onPause()V
    .locals 2

    .prologue
    .line 123
    invoke-static {p0}, Landroid/support/v4/content/LocalBroadcastManager;->getInstance(Landroid/content/Context;)Landroid/support/v4/content/LocalBroadcastManager;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/setupwizard/update/OtaUpdateActivity;->mLinkSpanReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {v0, v1}, Landroid/support/v4/content/LocalBroadcastManager;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    .line 124
    invoke-super {p0}, Lcom/google/android/setupwizard/BaseActivity;->onPause()V

    .line 125
    return-void
.end method

.method protected onResume()V
    .locals 3

    .prologue
    .line 116
    invoke-super {p0}, Lcom/google/android/setupwizard/BaseActivity;->onResume()V

    .line 117
    new-instance v0, Landroid/content/IntentFilter;

    const-string v1, "com.google.android.setupwizard.LINK_SPAN_CLICKED"

    invoke-direct {v0, v1}, Landroid/content/IntentFilter;-><init>(Ljava/lang/String;)V

    .line 118
    .local v0, "filter":Landroid/content/IntentFilter;
    invoke-static {p0}, Landroid/support/v4/content/LocalBroadcastManager;->getInstance(Landroid/content/Context;)Landroid/support/v4/content/LocalBroadcastManager;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/setupwizard/update/OtaUpdateActivity;->mLinkSpanReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {v1, v2, v0}, Landroid/support/v4/content/LocalBroadcastManager;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)V

    .line 119
    return-void
.end method

.method protected start()V
    .locals 0

    .prologue
    .line 129
    invoke-virtual {p0}, Lcom/google/android/setupwizard/update/OtaUpdateActivity;->nextScreen()V

    .line 130
    return-void
.end method
