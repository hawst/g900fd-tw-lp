.class public Lcom/google/android/setupwizard/util/SetupWizardIllustration;
.super Landroid/widget/FrameLayout;
.source "SetupWizardIllustration.java"


# instance fields
.field private mAspectRatio:F

.field private mBackground:Landroid/graphics/drawable/Drawable;

.field private mBaselineGridSize:F

.field private mForeground:Landroid/graphics/drawable/Drawable;

.field private final mForegroundBounds:Landroid/graphics/Rect;

.field private mScaleX:F

.field private mScaleY:F

.field private final mViewBounds:Landroid/graphics/Rect;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 54
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/google/android/setupwizard/util/SetupWizardIllustration;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 55
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    .line 58
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/google/android/setupwizard/util/SetupWizardIllustration;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 59
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;
    .param p3, "defStyleAttr"    # I

    .prologue
    .line 62
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, p3, v0}, Lcom/google/android/setupwizard/util/SetupWizardIllustration;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;II)V

    .line 63
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;II)V
    .locals 5
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;
    .param p3, "defStyleAttr"    # I
    .param p4, "defStyleRes"    # I

    .prologue
    const/high16 v4, 0x3f800000    # 1.0f

    const/4 v2, 0x0

    const/4 v3, 0x0

    .line 67
    invoke-direct {p0, p1, p2, p3, p4}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;II)V

    .line 47
    new-instance v1, Landroid/graphics/Rect;

    invoke-direct {v1}, Landroid/graphics/Rect;-><init>()V

    iput-object v1, p0, Lcom/google/android/setupwizard/util/SetupWizardIllustration;->mViewBounds:Landroid/graphics/Rect;

    .line 48
    new-instance v1, Landroid/graphics/Rect;

    invoke-direct {v1}, Landroid/graphics/Rect;-><init>()V

    iput-object v1, p0, Lcom/google/android/setupwizard/util/SetupWizardIllustration;->mForegroundBounds:Landroid/graphics/Rect;

    .line 49
    iput v4, p0, Lcom/google/android/setupwizard/util/SetupWizardIllustration;->mScaleX:F

    .line 50
    iput v4, p0, Lcom/google/android/setupwizard/util/SetupWizardIllustration;->mScaleY:F

    .line 51
    iput v2, p0, Lcom/google/android/setupwizard/util/SetupWizardIllustration;->mAspectRatio:F

    .line 68
    if-eqz p2, :cond_0

    .line 69
    sget-object v1, Lcom/google/android/setupwizard/R$styleable;->SetupWizardIllustration:[I

    invoke-virtual {p1, p2, v1, v3, v3}, Landroid/content/Context;->obtainStyledAttributes(Landroid/util/AttributeSet;[III)Landroid/content/res/TypedArray;

    move-result-object v0

    .line 71
    .local v0, "a":Landroid/content/res/TypedArray;
    invoke-virtual {v0, v3, v2}, Landroid/content/res/TypedArray;->getFloat(IF)F

    move-result v1

    iput v1, p0, Lcom/google/android/setupwizard/util/SetupWizardIllustration;->mAspectRatio:F

    .line 72
    invoke-virtual {v0}, Landroid/content/res/TypedArray;->recycle()V

    .line 75
    .end local v0    # "a":Landroid/content/res/TypedArray;
    :cond_0
    invoke-virtual {p0}, Lcom/google/android/setupwizard/util/SetupWizardIllustration;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v1

    iget v1, v1, Landroid/util/DisplayMetrics;->density:F

    const/high16 v2, 0x41000000    # 8.0f

    mul-float/2addr v1, v2

    iput v1, p0, Lcom/google/android/setupwizard/util/SetupWizardIllustration;->mBaselineGridSize:F

    .line 76
    invoke-virtual {p0, v3}, Lcom/google/android/setupwizard/util/SetupWizardIllustration;->setWillNotDraw(Z)V

    .line 77
    return-void
.end method

.method private applyGravity(IIILandroid/graphics/Rect;Landroid/graphics/Rect;I)V
    .locals 2
    .param p1, "gravity"    # I
    .param p2, "width"    # I
    .param p3, "height"    # I
    .param p4, "container"    # Landroid/graphics/Rect;
    .param p5, "outRect"    # Landroid/graphics/Rect;
    .param p6, "layoutDirection"    # I

    .prologue
    .line 178
    invoke-static/range {p1 .. p6}, Landroid/view/Gravity;->apply(IIILandroid/graphics/Rect;Landroid/graphics/Rect;I)V

    .line 179
    invoke-virtual {p5}, Landroid/graphics/Rect;->width()I

    move-result v0

    int-to-float v0, v0

    int-to-float v1, p2

    div-float/2addr v0, v1

    iput v0, p0, Lcom/google/android/setupwizard/util/SetupWizardIllustration;->mScaleX:F

    .line 180
    invoke-virtual {p5}, Landroid/graphics/Rect;->height()I

    move-result v0

    int-to-float v0, v0

    int-to-float v1, p3

    div-float/2addr v0, v1

    iput v0, p0, Lcom/google/android/setupwizard/util/SetupWizardIllustration;->mScaleY:F

    .line 181
    return-void
.end method


# virtual methods
.method public onDraw(Landroid/graphics/Canvas;)V
    .locals 7
    .param p1, "canvas"    # Landroid/graphics/Canvas;

    .prologue
    const/4 v6, 0x1

    const/high16 v5, 0x3f800000    # 1.0f

    const/high16 v4, -0x40800000    # -1.0f

    const/4 v3, 0x0

    .line 142
    invoke-virtual {p0}, Lcom/google/android/setupwizard/util/SetupWizardIllustration;->getLayoutDirection()I

    move-result v0

    .line 143
    .local v0, "layoutDirection":I
    iget-object v1, p0, Lcom/google/android/setupwizard/util/SetupWizardIllustration;->mBackground:Landroid/graphics/drawable/Drawable;

    if-eqz v1, :cond_1

    .line 145
    invoke-virtual {p1}, Landroid/graphics/Canvas;->save()I

    .line 146
    iget-object v1, p0, Lcom/google/android/setupwizard/util/SetupWizardIllustration;->mForegroundBounds:Landroid/graphics/Rect;

    invoke-virtual {v1}, Landroid/graphics/Rect;->height()I

    move-result v1

    int-to-float v1, v1

    invoke-virtual {p1, v3, v1}, Landroid/graphics/Canvas;->translate(FF)V

    .line 148
    iget v1, p0, Lcom/google/android/setupwizard/util/SetupWizardIllustration;->mScaleX:F

    iget v2, p0, Lcom/google/android/setupwizard/util/SetupWizardIllustration;->mScaleY:F

    invoke-virtual {p1, v1, v2, v3, v3}, Landroid/graphics/Canvas;->scale(FFFF)V

    .line 149
    if-ne v0, v6, :cond_0

    iget-object v1, p0, Lcom/google/android/setupwizard/util/SetupWizardIllustration;->mBackground:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v1}, Landroid/graphics/drawable/Drawable;->isAutoMirrored()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 151
    invoke-virtual {p1, v4, v5}, Landroid/graphics/Canvas;->scale(FF)V

    .line 152
    iget-object v1, p0, Lcom/google/android/setupwizard/util/SetupWizardIllustration;->mBackground:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v1}, Landroid/graphics/drawable/Drawable;->getBounds()Landroid/graphics/Rect;

    move-result-object v1

    invoke-virtual {v1}, Landroid/graphics/Rect;->width()I

    move-result v1

    neg-int v1, v1

    int-to-float v1, v1

    invoke-virtual {p1, v1, v3}, Landroid/graphics/Canvas;->translate(FF)V

    .line 154
    :cond_0
    iget-object v1, p0, Lcom/google/android/setupwizard/util/SetupWizardIllustration;->mBackground:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v1, p1}, Landroid/graphics/drawable/Drawable;->draw(Landroid/graphics/Canvas;)V

    .line 155
    invoke-virtual {p1}, Landroid/graphics/Canvas;->restore()V

    .line 157
    :cond_1
    iget-object v1, p0, Lcom/google/android/setupwizard/util/SetupWizardIllustration;->mForeground:Landroid/graphics/drawable/Drawable;

    if-eqz v1, :cond_3

    .line 158
    invoke-virtual {p1}, Landroid/graphics/Canvas;->save()I

    .line 159
    if-ne v0, v6, :cond_2

    iget-object v1, p0, Lcom/google/android/setupwizard/util/SetupWizardIllustration;->mForeground:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v1}, Landroid/graphics/drawable/Drawable;->isAutoMirrored()Z

    move-result v1

    if-eqz v1, :cond_2

    .line 161
    invoke-virtual {p1, v4, v5}, Landroid/graphics/Canvas;->scale(FF)V

    .line 162
    iget-object v1, p0, Lcom/google/android/setupwizard/util/SetupWizardIllustration;->mForegroundBounds:Landroid/graphics/Rect;

    invoke-virtual {v1}, Landroid/graphics/Rect;->width()I

    move-result v1

    neg-int v1, v1

    int-to-float v1, v1

    invoke-virtual {p1, v1, v3}, Landroid/graphics/Canvas;->translate(FF)V

    .line 165
    :cond_2
    iget-object v1, p0, Lcom/google/android/setupwizard/util/SetupWizardIllustration;->mForeground:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v1, p1}, Landroid/graphics/drawable/Drawable;->draw(Landroid/graphics/Canvas;)V

    .line 166
    invoke-virtual {p1}, Landroid/graphics/Canvas;->restore()V

    .line 168
    :cond_3
    invoke-super {p0, p1}, Landroid/widget/FrameLayout;->onDraw(Landroid/graphics/Canvas;)V

    .line 169
    return-void
.end method

.method protected onLayout(ZIIII)V
    .locals 18
    .param p1, "changed"    # Z
    .param p2, "left"    # I
    .param p3, "top"    # I
    .param p4, "right"    # I
    .param p5, "bottom"    # I

    .prologue
    .line 111
    sub-int v17, p4, p2

    .line 112
    .local v17, "layoutWidth":I
    sub-int v16, p5, p3

    .line 113
    .local v16, "layoutHeight":I
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/setupwizard/util/SetupWizardIllustration;->mForeground:Landroid/graphics/drawable/Drawable;

    if-eqz v2, :cond_1

    .line 114
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/setupwizard/util/SetupWizardIllustration;->mForeground:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v2}, Landroid/graphics/drawable/Drawable;->getIntrinsicWidth()I

    move-result v4

    .line 115
    .local v4, "intrinsicWidth":I
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/setupwizard/util/SetupWizardIllustration;->mForeground:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v2}, Landroid/graphics/drawable/Drawable;->getIntrinsicHeight()I

    move-result v5

    .line 116
    .local v5, "intrinsicHeight":I
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/setupwizard/util/SetupWizardIllustration;->getLayoutDirection()I

    move-result v8

    .line 118
    .local v8, "layoutDirection":I
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/setupwizard/util/SetupWizardIllustration;->mViewBounds:Landroid/graphics/Rect;

    const/4 v3, 0x0

    const/4 v6, 0x0

    move/from16 v0, v17

    move/from16 v1, v16

    invoke-virtual {v2, v3, v6, v0, v1}, Landroid/graphics/Rect;->set(IIII)V

    .line 119
    const/16 v3, 0x37

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/google/android/setupwizard/util/SetupWizardIllustration;->mViewBounds:Landroid/graphics/Rect;

    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/google/android/setupwizard/util/SetupWizardIllustration;->mForegroundBounds:Landroid/graphics/Rect;

    move-object/from16 v2, p0

    invoke-direct/range {v2 .. v8}, Lcom/google/android/setupwizard/util/SetupWizardIllustration;->applyGravity(IIILandroid/graphics/Rect;Landroid/graphics/Rect;I)V

    .line 121
    move-object/from16 v0, p0

    iget v2, v0, Lcom/google/android/setupwizard/util/SetupWizardIllustration;->mAspectRatio:F

    const/4 v3, 0x0

    cmpl-float v2, v2, v3

    if-eqz v2, :cond_0

    .line 124
    int-to-float v2, v4

    move-object/from16 v0, p0

    iget v3, v0, Lcom/google/android/setupwizard/util/SetupWizardIllustration;->mScaleX:F

    mul-float/2addr v2, v3

    float-to-int v11, v2

    .line 125
    .local v11, "scaledIntrinsicWidth":I
    int-to-float v2, v5

    move-object/from16 v0, p0

    iget v3, v0, Lcom/google/android/setupwizard/util/SetupWizardIllustration;->mScaleX:F

    mul-float/2addr v2, v3

    float-to-int v12, v2

    .line 126
    .local v12, "scaledIntrinsicHeight":I
    const/16 v10, 0x37

    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/google/android/setupwizard/util/SetupWizardIllustration;->mViewBounds:Landroid/graphics/Rect;

    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/google/android/setupwizard/util/SetupWizardIllustration;->mForegroundBounds:Landroid/graphics/Rect;

    move-object/from16 v9, p0

    move v15, v8

    invoke-direct/range {v9 .. v15}, Lcom/google/android/setupwizard/util/SetupWizardIllustration;->applyGravity(IIILandroid/graphics/Rect;Landroid/graphics/Rect;I)V

    .line 129
    .end local v11    # "scaledIntrinsicWidth":I
    .end local v12    # "scaledIntrinsicHeight":I
    :cond_0
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/setupwizard/util/SetupWizardIllustration;->mForeground:Landroid/graphics/drawable/Drawable;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/setupwizard/util/SetupWizardIllustration;->mForegroundBounds:Landroid/graphics/Rect;

    invoke-virtual {v2, v3}, Landroid/graphics/drawable/Drawable;->setBounds(Landroid/graphics/Rect;)V

    .line 131
    .end local v4    # "intrinsicWidth":I
    .end local v5    # "intrinsicHeight":I
    .end local v8    # "layoutDirection":I
    :cond_1
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/setupwizard/util/SetupWizardIllustration;->mBackground:Landroid/graphics/drawable/Drawable;

    if-eqz v2, :cond_2

    .line 134
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/setupwizard/util/SetupWizardIllustration;->mBackground:Landroid/graphics/drawable/Drawable;

    const/4 v3, 0x0

    const/4 v6, 0x0

    move/from16 v0, v17

    int-to-float v7, v0

    move-object/from16 v0, p0

    iget v9, v0, Lcom/google/android/setupwizard/util/SetupWizardIllustration;->mScaleX:F

    div-float/2addr v7, v9

    float-to-double v14, v7

    invoke-static {v14, v15}, Ljava/lang/Math;->ceil(D)D

    move-result-wide v14

    double-to-int v7, v14

    move-object/from16 v0, p0

    iget-object v9, v0, Lcom/google/android/setupwizard/util/SetupWizardIllustration;->mForegroundBounds:Landroid/graphics/Rect;

    invoke-virtual {v9}, Landroid/graphics/Rect;->height()I

    move-result v9

    sub-int v9, v16, v9

    int-to-float v9, v9

    move-object/from16 v0, p0

    iget v10, v0, Lcom/google/android/setupwizard/util/SetupWizardIllustration;->mScaleY:F

    div-float/2addr v9, v10

    float-to-double v14, v9

    invoke-static {v14, v15}, Ljava/lang/Math;->ceil(D)D

    move-result-wide v14

    double-to-int v9, v14

    invoke-virtual {v2, v3, v6, v7, v9}, Landroid/graphics/drawable/Drawable;->setBounds(IIII)V

    .line 137
    :cond_2
    invoke-super/range {p0 .. p5}, Landroid/widget/FrameLayout;->onLayout(ZIIII)V

    .line 138
    return-void
.end method

.method protected onMeasure(II)V
    .locals 6
    .param p1, "widthMeasureSpec"    # I
    .param p2, "heightMeasureSpec"    # I

    .prologue
    const/4 v5, 0x0

    .line 99
    iget v2, p0, Lcom/google/android/setupwizard/util/SetupWizardIllustration;->mAspectRatio:F

    const/4 v3, 0x0

    cmpl-float v2, v2, v3

    if-eqz v2, :cond_0

    .line 100
    invoke-static {p1}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result v1

    .line 101
    .local v1, "parentWidth":I
    int-to-float v2, v1

    iget v3, p0, Lcom/google/android/setupwizard/util/SetupWizardIllustration;->mAspectRatio:F

    div-float/2addr v2, v3

    float-to-int v0, v2

    .line 102
    .local v0, "illustrationHeight":I
    int-to-float v2, v0

    int-to-float v3, v0

    iget v4, p0, Lcom/google/android/setupwizard/util/SetupWizardIllustration;->mBaselineGridSize:F

    rem-float/2addr v3, v4

    sub-float/2addr v2, v3

    float-to-int v0, v2

    .line 103
    invoke-virtual {p0, v5, v0, v5, v5}, Lcom/google/android/setupwizard/util/SetupWizardIllustration;->setPadding(IIII)V

    .line 105
    .end local v0    # "illustrationHeight":I
    .end local v1    # "parentWidth":I
    :cond_0
    sget-object v2, Landroid/view/ViewOutlineProvider;->BOUNDS:Landroid/view/ViewOutlineProvider;

    invoke-virtual {p0, v2}, Lcom/google/android/setupwizard/util/SetupWizardIllustration;->setOutlineProvider(Landroid/view/ViewOutlineProvider;)V

    .line 106
    invoke-super {p0, p1, p2}, Landroid/widget/FrameLayout;->onMeasure(II)V

    .line 107
    return-void
.end method

.method public setBackground(Landroid/graphics/drawable/Drawable;)V
    .locals 0
    .param p1, "background"    # Landroid/graphics/drawable/Drawable;

    .prologue
    .line 85
    iput-object p1, p0, Lcom/google/android/setupwizard/util/SetupWizardIllustration;->mBackground:Landroid/graphics/drawable/Drawable;

    .line 86
    return-void
.end method

.method public setForeground(Landroid/graphics/drawable/Drawable;)V
    .locals 0
    .param p1, "foreground"    # Landroid/graphics/drawable/Drawable;

    .prologue
    .line 94
    iput-object p1, p0, Lcom/google/android/setupwizard/util/SetupWizardIllustration;->mForeground:Landroid/graphics/drawable/Drawable;

    .line 95
    return-void
.end method
