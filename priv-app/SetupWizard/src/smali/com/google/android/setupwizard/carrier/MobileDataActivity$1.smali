.class Lcom/google/android/setupwizard/carrier/MobileDataActivity$1;
.super Ljava/lang/Object;
.source "MobileDataActivity.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/setupwizard/carrier/MobileDataActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/setupwizard/carrier/MobileDataActivity;


# direct methods
.method constructor <init>(Lcom/google/android/setupwizard/carrier/MobileDataActivity;)V
    .locals 0

    .prologue
    .line 76
    iput-object p1, p0, Lcom/google/android/setupwizard/carrier/MobileDataActivity$1;->this$0:Lcom/google/android/setupwizard/carrier/MobileDataActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 8

    .prologue
    const/4 v7, 0x5

    .line 79
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v2

    .line 85
    .local v2, "time":J
    iget-object v4, p0, Lcom/google/android/setupwizard/carrier/MobileDataActivity$1;->this$0:Lcom/google/android/setupwizard/carrier/MobileDataActivity;

    const-string v5, "SetupWizardPrefs"

    const/4 v6, 0x4

    invoke-virtual {v4, v5, v6}, Lcom/google/android/setupwizard/carrier/MobileDataActivity;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v4

    const-string v5, "connection_status"

    const/4 v6, 0x0

    invoke-interface {v4, v5, v6}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    .line 92
    .local v0, "hasValidConnection":Z
    new-instance v1, Landroid/content/Intent;

    iget-object v4, p0, Lcom/google/android/setupwizard/carrier/MobileDataActivity$1;->this$0:Lcom/google/android/setupwizard/carrier/MobileDataActivity;

    const-class v5, Lcom/google/android/setupwizard/carrier/MobileDataActivity;

    invoke-direct {v1, v4, v5}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 93
    .local v1, "intent":Landroid/content/Intent;
    if-nez v0, :cond_0

    iget-object v4, p0, Lcom/google/android/setupwizard/carrier/MobileDataActivity$1;->this$0:Lcom/google/android/setupwizard/carrier/MobileDataActivity;

    # getter for: Lcom/google/android/setupwizard/carrier/MobileDataActivity;->mState:I
    invoke-static {v4}, Lcom/google/android/setupwizard/carrier/MobileDataActivity;->access$000(Lcom/google/android/setupwizard/carrier/MobileDataActivity;)I

    move-result v4

    if-ne v4, v7, :cond_1

    .line 94
    :cond_0
    const-string v4, "startingState"

    invoke-virtual {v1, v4, v7}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 100
    :goto_0
    iget-object v4, p0, Lcom/google/android/setupwizard/carrier/MobileDataActivity$1;->this$0:Lcom/google/android/setupwizard/carrier/MobileDataActivity;

    invoke-virtual {v4, v1}, Lcom/google/android/setupwizard/carrier/MobileDataActivity;->startActivity(Landroid/content/Intent;)V

    .line 101
    return-void

    .line 95
    :cond_1
    iget-object v4, p0, Lcom/google/android/setupwizard/carrier/MobileDataActivity$1;->this$0:Lcom/google/android/setupwizard/carrier/MobileDataActivity;

    # getter for: Lcom/google/android/setupwizard/carrier/MobileDataActivity;->mEndTime:J
    invoke-static {v4}, Lcom/google/android/setupwizard/carrier/MobileDataActivity;->access$100(Lcom/google/android/setupwizard/carrier/MobileDataActivity;)J

    move-result-wide v4

    cmp-long v4, v2, v4

    if-gez v4, :cond_2

    .line 96
    const-string v4, "startingState"

    iget-object v5, p0, Lcom/google/android/setupwizard/carrier/MobileDataActivity$1;->this$0:Lcom/google/android/setupwizard/carrier/MobileDataActivity;

    # getter for: Lcom/google/android/setupwizard/carrier/MobileDataActivity;->mState:I
    invoke-static {v5}, Lcom/google/android/setupwizard/carrier/MobileDataActivity;->access$000(Lcom/google/android/setupwizard/carrier/MobileDataActivity;)I

    move-result v5

    invoke-virtual {v1, v4, v5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    goto :goto_0

    .line 98
    :cond_2
    const-string v4, "startingState"

    const/4 v5, 0x6

    invoke-virtual {v1, v4, v5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    goto :goto_0
.end method
