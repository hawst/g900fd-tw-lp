.class public Lcom/google/android/setupwizard/user/WelcomeUserActivity;
.super Lcom/google/android/setupwizard/BaseActivity;
.source "WelcomeUserActivity.java"


# instance fields
.field private mAccessibilityHelper:Lcom/google/android/setupwizard/accessibility/AccessibilityGestureHelper;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 29
    invoke-direct {p0}, Lcom/google/android/setupwizard/BaseActivity;-><init>()V

    return-void
.end method


# virtual methods
.method public dispatchTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 1
    .param p1, "ev"    # Landroid/view/MotionEvent;

    .prologue
    .line 83
    iget-object v0, p0, Lcom/google/android/setupwizard/user/WelcomeUserActivity;->mAccessibilityHelper:Lcom/google/android/setupwizard/accessibility/AccessibilityGestureHelper;

    invoke-virtual {v0, p1}, Lcom/google/android/setupwizard/accessibility/AccessibilityGestureHelper;->dispatchTouchEvent(Landroid/view/MotionEvent;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 84
    const/4 v0, 0x1

    .line 86
    :goto_0
    return v0

    :cond_0
    invoke-super {p0, p1}, Lcom/google/android/setupwizard/BaseActivity;->dispatchTouchEvent(Landroid/view/MotionEvent;)Z

    move-result v0

    goto :goto_0
.end method

.method public getStyleForTheme(Ljava/lang/String;)I
    .locals 1
    .param p1, "themeName"    # Ljava/lang/String;

    .prologue
    .line 91
    const v0, 0x7f060026

    return v0
.end method

.method public onBackPressed()V
    .locals 2

    .prologue
    .line 76
    iget-object v0, p0, Lcom/google/android/setupwizard/user/WelcomeUserActivity;->TAG:Ljava/lang/String;

    const-string v1, "WelcomeUserActivity.onBackPressed()"

    invoke-static {v0, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 77
    return-void
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 3
    .param p1, "icicle"    # Landroid/os/Bundle;

    .prologue
    .line 36
    iget-object v1, p0, Lcom/google/android/setupwizard/user/WelcomeUserActivity;->TAG:Ljava/lang/String;

    const-string v2, "WelcomeUserActivity.onCreate()"

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 37
    invoke-super {p0, p1}, Lcom/google/android/setupwizard/BaseActivity;->onCreate(Landroid/os/Bundle;)V

    .line 39
    new-instance v1, Lcom/google/android/setupwizard/accessibility/AccessibilityGestureHelper;

    invoke-direct {v1, p0}, Lcom/google/android/setupwizard/accessibility/AccessibilityGestureHelper;-><init>(Landroid/content/Context;)V

    iput-object v1, p0, Lcom/google/android/setupwizard/user/WelcomeUserActivity;->mAccessibilityHelper:Lcom/google/android/setupwizard/accessibility/AccessibilityGestureHelper;

    .line 41
    const v1, 0x7f030022

    invoke-virtual {p0, v1}, Lcom/google/android/setupwizard/user/WelcomeUserActivity;->setContentView(I)V

    .line 43
    const v1, 0x7f0e0066

    invoke-virtual {p0, v1}, Lcom/google/android/setupwizard/user/WelcomeUserActivity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    invoke-virtual {p0, v1}, Lcom/google/android/setupwizard/user/WelcomeUserActivity;->setDefaultButton(Landroid/view/View;)V

    .line 44
    const/4 v1, 0x0

    invoke-virtual {p0, v1}, Lcom/google/android/setupwizard/user/WelcomeUserActivity;->setBackAllowed(Z)V

    .line 46
    invoke-static {p0}, Landroid/os/UserManager;->get(Landroid/content/Context;)Landroid/os/UserManager;

    move-result-object v1

    invoke-virtual {v1}, Landroid/os/UserManager;->isLinkedUser()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 47
    const v1, 0x7f0e006d

    invoke-virtual {p0, v1}, Lcom/google/android/setupwizard/user/WelcomeUserActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 48
    .local v0, "info":Landroid/widget/TextView;
    const v1, 0x7f070040

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    .line 52
    .end local v0    # "info":Landroid/widget/TextView;
    :cond_0
    return-void
.end method

.method public onDestroy()V
    .locals 2

    .prologue
    .line 65
    iget-object v0, p0, Lcom/google/android/setupwizard/user/WelcomeUserActivity;->TAG:Ljava/lang/String;

    const-string v1, "WelcomeUserActivity.onDestroy()"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 66
    invoke-super {p0}, Lcom/google/android/setupwizard/BaseActivity;->onDestroy()V

    .line 68
    iget-object v0, p0, Lcom/google/android/setupwizard/user/WelcomeUserActivity;->mAccessibilityHelper:Lcom/google/android/setupwizard/accessibility/AccessibilityGestureHelper;

    invoke-virtual {v0}, Lcom/google/android/setupwizard/accessibility/AccessibilityGestureHelper;->onDestroy()V

    .line 69
    return-void
.end method

.method protected start()V
    .locals 2

    .prologue
    const/4 v1, -0x1

    .line 56
    invoke-virtual {p0}, Lcom/google/android/setupwizard/user/WelcomeUserActivity;->isUsingWizardManager()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 57
    invoke-virtual {p0, v1}, Lcom/google/android/setupwizard/user/WelcomeUserActivity;->nextScreen(I)V

    .line 61
    :goto_0
    return-void

    .line 59
    :cond_0
    invoke-virtual {p0, v1}, Lcom/google/android/setupwizard/user/WelcomeUserActivity;->finishAction(I)V

    goto :goto_0
.end method
