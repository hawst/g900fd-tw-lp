.class public Lcom/google/android/setupwizard/util/LinkCheckBox;
.super Landroid/widget/CheckBox;
.source "LinkCheckBox.java"


# instance fields
.field private mAccessibilityHelper:Lcom/google/android/setupwizard/util/LinkAccessibilityHelper;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 29
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/google/android/setupwizard/util/LinkCheckBox;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 30
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    .line 33
    invoke-direct {p0, p1, p2}, Landroid/widget/CheckBox;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 34
    new-instance v0, Lcom/google/android/setupwizard/util/LinkAccessibilityHelper;

    invoke-direct {v0, p0}, Lcom/google/android/setupwizard/util/LinkAccessibilityHelper;-><init>(Landroid/widget/TextView;)V

    iput-object v0, p0, Lcom/google/android/setupwizard/util/LinkCheckBox;->mAccessibilityHelper:Lcom/google/android/setupwizard/util/LinkAccessibilityHelper;

    .line 35
    iget-object v0, p0, Lcom/google/android/setupwizard/util/LinkCheckBox;->mAccessibilityHelper:Lcom/google/android/setupwizard/util/LinkAccessibilityHelper;

    invoke-virtual {p0, v0}, Lcom/google/android/setupwizard/util/LinkCheckBox;->setAccessibilityDelegate(Landroid/view/View$AccessibilityDelegate;)V

    .line 36
    return-void
.end method


# virtual methods
.method protected dispatchHoverEvent(Landroid/view/MotionEvent;)Z
    .locals 1
    .param p1, "event"    # Landroid/view/MotionEvent;

    .prologue
    .line 40
    iget-object v0, p0, Lcom/google/android/setupwizard/util/LinkCheckBox;->mAccessibilityHelper:Lcom/google/android/setupwizard/util/LinkAccessibilityHelper;

    invoke-virtual {v0, p1}, Lcom/google/android/setupwizard/util/LinkAccessibilityHelper;->dispatchHoverEvent(Landroid/view/MotionEvent;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 41
    const/4 v0, 0x1

    .line 43
    :goto_0
    return v0

    :cond_0
    invoke-super {p0, p1}, Landroid/widget/CheckBox;->dispatchHoverEvent(Landroid/view/MotionEvent;)Z

    move-result v0

    goto :goto_0
.end method
