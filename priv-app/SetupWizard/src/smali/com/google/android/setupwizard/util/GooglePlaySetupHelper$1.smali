.class Lcom/google/android/setupwizard/util/GooglePlaySetupHelper$1;
.super Ljava/lang/Object;
.source "GooglePlaySetupHelper.java"

# interfaces
.implements Landroid/content/ServiceConnection;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/setupwizard/util/GooglePlaySetupHelper;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/setupwizard/util/GooglePlaySetupHelper;


# direct methods
.method constructor <init>(Lcom/google/android/setupwizard/util/GooglePlaySetupHelper;)V
    .locals 0

    .prologue
    .line 73
    iput-object p1, p0, Lcom/google/android/setupwizard/util/GooglePlaySetupHelper$1;->this$0:Lcom/google/android/setupwizard/util/GooglePlaySetupHelper;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onServiceConnected(Landroid/content/ComponentName;Landroid/os/IBinder;)V
    .locals 3
    .param p1, "componentName"    # Landroid/content/ComponentName;
    .param p2, "iBinder"    # Landroid/os/IBinder;

    .prologue
    .line 76
    const-string v0, "SetupWizard.GooglePlaySetupHelper"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "onServiceConnected componentName="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 77
    iget-object v0, p0, Lcom/google/android/setupwizard/util/GooglePlaySetupHelper$1;->this$0:Lcom/google/android/setupwizard/util/GooglePlaySetupHelper;

    invoke-static {p2}, Lcom/android/vending/setup/IPlaySetupService$Stub;->asInterface(Landroid/os/IBinder;)Lcom/android/vending/setup/IPlaySetupService;

    move-result-object v1

    # setter for: Lcom/google/android/setupwizard/util/GooglePlaySetupHelper;->mPlaySetupService:Lcom/android/vending/setup/IPlaySetupService;
    invoke-static {v0, v1}, Lcom/google/android/setupwizard/util/GooglePlaySetupHelper;->access$002(Lcom/google/android/setupwizard/util/GooglePlaySetupHelper;Lcom/android/vending/setup/IPlaySetupService;)Lcom/android/vending/setup/IPlaySetupService;

    .line 78
    iget-object v0, p0, Lcom/google/android/setupwizard/util/GooglePlaySetupHelper$1;->this$0:Lcom/google/android/setupwizard/util/GooglePlaySetupHelper;

    # getter for: Lcom/google/android/setupwizard/util/GooglePlaySetupHelper;->mListener:Lcom/google/android/setupwizard/util/GooglePlaySetupHelper$GooglePlayConnectionListener;
    invoke-static {v0}, Lcom/google/android/setupwizard/util/GooglePlaySetupHelper;->access$100(Lcom/google/android/setupwizard/util/GooglePlaySetupHelper;)Lcom/google/android/setupwizard/util/GooglePlaySetupHelper$GooglePlayConnectionListener;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 79
    iget-object v0, p0, Lcom/google/android/setupwizard/util/GooglePlaySetupHelper$1;->this$0:Lcom/google/android/setupwizard/util/GooglePlaySetupHelper;

    # getter for: Lcom/google/android/setupwizard/util/GooglePlaySetupHelper;->mListener:Lcom/google/android/setupwizard/util/GooglePlaySetupHelper$GooglePlayConnectionListener;
    invoke-static {v0}, Lcom/google/android/setupwizard/util/GooglePlaySetupHelper;->access$100(Lcom/google/android/setupwizard/util/GooglePlaySetupHelper;)Lcom/google/android/setupwizard/util/GooglePlaySetupHelper$GooglePlayConnectionListener;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/setupwizard/util/GooglePlaySetupHelper$1;->this$0:Lcom/google/android/setupwizard/util/GooglePlaySetupHelper;

    invoke-interface {v0, v1}, Lcom/google/android/setupwizard/util/GooglePlaySetupHelper$GooglePlayConnectionListener;->onGooglePlayConnected(Lcom/google/android/setupwizard/util/GooglePlaySetupHelper;)V

    .line 80
    iget-object v0, p0, Lcom/google/android/setupwizard/util/GooglePlaySetupHelper$1;->this$0:Lcom/google/android/setupwizard/util/GooglePlaySetupHelper;

    const/4 v1, 0x0

    # setter for: Lcom/google/android/setupwizard/util/GooglePlaySetupHelper;->mListener:Lcom/google/android/setupwizard/util/GooglePlaySetupHelper$GooglePlayConnectionListener;
    invoke-static {v0, v1}, Lcom/google/android/setupwizard/util/GooglePlaySetupHelper;->access$102(Lcom/google/android/setupwizard/util/GooglePlaySetupHelper;Lcom/google/android/setupwizard/util/GooglePlaySetupHelper$GooglePlayConnectionListener;)Lcom/google/android/setupwizard/util/GooglePlaySetupHelper$GooglePlayConnectionListener;

    .line 82
    :cond_0
    return-void
.end method

.method public onServiceDisconnected(Landroid/content/ComponentName;)V
    .locals 3
    .param p1, "componentName"    # Landroid/content/ComponentName;

    .prologue
    .line 86
    iget-object v0, p0, Lcom/google/android/setupwizard/util/GooglePlaySetupHelper$1;->this$0:Lcom/google/android/setupwizard/util/GooglePlaySetupHelper;

    const/4 v1, 0x0

    # setter for: Lcom/google/android/setupwizard/util/GooglePlaySetupHelper;->mPlaySetupService:Lcom/android/vending/setup/IPlaySetupService;
    invoke-static {v0, v1}, Lcom/google/android/setupwizard/util/GooglePlaySetupHelper;->access$002(Lcom/google/android/setupwizard/util/GooglePlaySetupHelper;Lcom/android/vending/setup/IPlaySetupService;)Lcom/android/vending/setup/IPlaySetupService;

    .line 87
    const-string v0, "SetupWizard.GooglePlaySetupHelper"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "onServiceDisconnected componentName="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 88
    return-void
.end method
