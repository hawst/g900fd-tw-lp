.class Lcom/google/android/setupwizard/util/LanguagePicker$1;
.super Ljava/lang/Object;
.source "LanguagePicker.java"

# interfaces
.implements Landroid/widget/NumberPicker$OnValueChangeListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/setupwizard/util/LanguagePicker;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/setupwizard/util/LanguagePicker;


# direct methods
.method constructor <init>(Lcom/google/android/setupwizard/util/LanguagePicker;)V
    .locals 0

    .prologue
    .line 79
    iput-object p1, p0, Lcom/google/android/setupwizard/util/LanguagePicker$1;->this$0:Lcom/google/android/setupwizard/util/LanguagePicker;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onValueChange(Landroid/widget/NumberPicker;II)V
    .locals 2
    .param p1, "picker"    # Landroid/widget/NumberPicker;
    .param p2, "oldVal"    # I
    .param p3, "newVal"    # I

    .prologue
    .line 82
    iget-object v0, p0, Lcom/google/android/setupwizard/util/LanguagePicker$1;->this$0:Lcom/google/android/setupwizard/util/LanguagePicker;

    # getter for: Lcom/google/android/setupwizard/util/LanguagePicker;->mOnLanguageSelectedListener:Lcom/google/android/setupwizard/util/LanguagePicker$OnLanguageSelectedListener;
    invoke-static {v0}, Lcom/google/android/setupwizard/util/LanguagePicker;->access$000(Lcom/google/android/setupwizard/util/LanguagePicker;)Lcom/google/android/setupwizard/util/LanguagePicker$OnLanguageSelectedListener;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 83
    iget-object v0, p0, Lcom/google/android/setupwizard/util/LanguagePicker$1;->this$0:Lcom/google/android/setupwizard/util/LanguagePicker;

    # getter for: Lcom/google/android/setupwizard/util/LanguagePicker;->mOnLanguageSelectedListener:Lcom/google/android/setupwizard/util/LanguagePicker$OnLanguageSelectedListener;
    invoke-static {v0}, Lcom/google/android/setupwizard/util/LanguagePicker;->access$000(Lcom/google/android/setupwizard/util/LanguagePicker;)Lcom/google/android/setupwizard/util/LanguagePicker$OnLanguageSelectedListener;

    move-result-object v0

    invoke-virtual {p1}, Landroid/widget/NumberPicker;->getValue()I

    move-result v1

    invoke-interface {v0, v1}, Lcom/google/android/setupwizard/util/LanguagePicker$OnLanguageSelectedListener;->onLanguageSelected(I)V

    .line 85
    :cond_0
    return-void
.end method
