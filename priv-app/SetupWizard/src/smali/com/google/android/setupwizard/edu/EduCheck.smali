.class public Lcom/google/android/setupwizard/edu/EduCheck;
.super Lcom/google/android/setupwizard/BaseActivity;
.source "EduCheck.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 25
    invoke-direct {p0}, Lcom/google/android/setupwizard/BaseActivity;-><init>()V

    return-void
.end method


# virtual methods
.method protected onCreate(Landroid/os/Bundle;)V
    .locals 2
    .param p1, "icicle"    # Landroid/os/Bundle;

    .prologue
    .line 32
    invoke-super {p0, p1}, Lcom/google/android/setupwizard/BaseActivity;->onCreate(Landroid/os/Bundle;)V

    .line 33
    invoke-virtual {p0}, Lcom/google/android/setupwizard/edu/EduCheck;->isEduSignin()Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v0, -0x1

    .line 34
    .local v0, "resultCode":I
    :goto_0
    invoke-virtual {p0, v0}, Lcom/google/android/setupwizard/edu/EduCheck;->nextScreen(I)V

    .line 35
    invoke-virtual {p0}, Lcom/google/android/setupwizard/edu/EduCheck;->finish()V

    .line 36
    return-void

    .line 33
    .end local v0    # "resultCode":I
    :cond_0
    const/4 v0, 0x1

    goto :goto_0
.end method
