.class Lcom/google/android/setupwizard/accessibility/AccessibilityGestureHelper$2;
.super Ljava/lang/Object;
.source "AccessibilityGestureHelper.java"

# interfaces
.implements Lcom/google/android/setupwizard/accessibility/MultitouchLongPressDetector$LongPressListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/setupwizard/accessibility/AccessibilityGestureHelper;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/setupwizard/accessibility/AccessibilityGestureHelper;


# direct methods
.method constructor <init>(Lcom/google/android/setupwizard/accessibility/AccessibilityGestureHelper;)V
    .locals 0

    .prologue
    .line 282
    iput-object p1, p0, Lcom/google/android/setupwizard/accessibility/AccessibilityGestureHelper$2;->this$0:Lcom/google/android/setupwizard/accessibility/AccessibilityGestureHelper;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onLongPress(Landroid/view/MotionEvent;)V
    .locals 2
    .param p1, "downEvent"    # Landroid/view/MotionEvent;

    .prologue
    .line 285
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getPointerCount()I

    move-result v0

    const/4 v1, 0x2

    if-lt v0, v1, :cond_0

    iget-object v0, p0, Lcom/google/android/setupwizard/accessibility/AccessibilityGestureHelper$2;->this$0:Lcom/google/android/setupwizard/accessibility/AccessibilityGestureHelper;

    # getter for: Lcom/google/android/setupwizard/accessibility/AccessibilityGestureHelper;->mAccessibilityEnabled:Z
    invoke-static {v0}, Lcom/google/android/setupwizard/accessibility/AccessibilityGestureHelper;->access$100(Lcom/google/android/setupwizard/accessibility/AccessibilityGestureHelper;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 287
    iget-object v0, p0, Lcom/google/android/setupwizard/accessibility/AccessibilityGestureHelper$2;->this$0:Lcom/google/android/setupwizard/accessibility/AccessibilityGestureHelper;

    # getter for: Lcom/google/android/setupwizard/accessibility/AccessibilityGestureHelper;->mStartedWarning:Z
    invoke-static {v0}, Lcom/google/android/setupwizard/accessibility/AccessibilityGestureHelper;->access$200(Lcom/google/android/setupwizard/accessibility/AccessibilityGestureHelper;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 288
    iget-object v0, p0, Lcom/google/android/setupwizard/accessibility/AccessibilityGestureHelper$2;->this$0:Lcom/google/android/setupwizard/accessibility/AccessibilityGestureHelper;

    const/4 v1, 0x1

    # setter for: Lcom/google/android/setupwizard/accessibility/AccessibilityGestureHelper;->mStartedWarning:Z
    invoke-static {v0, v1}, Lcom/google/android/setupwizard/accessibility/AccessibilityGestureHelper;->access$202(Lcom/google/android/setupwizard/accessibility/AccessibilityGestureHelper;Z)Z

    .line 289
    iget-object v0, p0, Lcom/google/android/setupwizard/accessibility/AccessibilityGestureHelper$2;->this$0:Lcom/google/android/setupwizard/accessibility/AccessibilityGestureHelper;

    # invokes: Lcom/google/android/setupwizard/accessibility/AccessibilityGestureHelper;->speakWarning()V
    invoke-static {v0}, Lcom/google/android/setupwizard/accessibility/AccessibilityGestureHelper;->access$300(Lcom/google/android/setupwizard/accessibility/AccessibilityGestureHelper;)V

    .line 294
    :cond_0
    :goto_0
    return-void

    .line 290
    :cond_1
    iget-object v0, p0, Lcom/google/android/setupwizard/accessibility/AccessibilityGestureHelper$2;->this$0:Lcom/google/android/setupwizard/accessibility/AccessibilityGestureHelper;

    # getter for: Lcom/google/android/setupwizard/accessibility/AccessibilityGestureHelper;->mFinishedWarning:Z
    invoke-static {v0}, Lcom/google/android/setupwizard/accessibility/AccessibilityGestureHelper;->access$400(Lcom/google/android/setupwizard/accessibility/AccessibilityGestureHelper;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 291
    iget-object v0, p0, Lcom/google/android/setupwizard/accessibility/AccessibilityGestureHelper$2;->this$0:Lcom/google/android/setupwizard/accessibility/AccessibilityGestureHelper;

    # invokes: Lcom/google/android/setupwizard/accessibility/AccessibilityGestureHelper;->enableAllAccessibilityServices()V
    invoke-static {v0}, Lcom/google/android/setupwizard/accessibility/AccessibilityGestureHelper;->access$500(Lcom/google/android/setupwizard/accessibility/AccessibilityGestureHelper;)V

    goto :goto_0
.end method
