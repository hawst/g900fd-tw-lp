.class Lcom/google/android/setupwizard/time/DateTimeMonitor$1;
.super Landroid/content/BroadcastReceiver;
.source "DateTimeMonitor.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/setupwizard/time/DateTimeMonitor;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/setupwizard/time/DateTimeMonitor;


# direct methods
.method constructor <init>(Lcom/google/android/setupwizard/time/DateTimeMonitor;)V
    .locals 0

    .prologue
    .line 64
    iput-object p1, p0, Lcom/google/android/setupwizard/time/DateTimeMonitor$1;->this$0:Lcom/google/android/setupwizard/time/DateTimeMonitor;

    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    return-void
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 4
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "intent"    # Landroid/content/Intent;

    .prologue
    .line 67
    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    .line 68
    .local v0, "action":Ljava/lang/String;
    # getter for: Lcom/google/android/setupwizard/time/DateTimeMonitor;->LOCAL_LOGV:Z
    invoke-static {}, Lcom/google/android/setupwizard/time/DateTimeMonitor;->access$000()Z

    move-result v1

    if-eqz v1, :cond_0

    const-string v1, "DateTimeMonitor"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "onReceive() "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 70
    :cond_0
    const-string v1, "android.intent.action.NETWORK_SET_TIME"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_1

    const-string v1, "android.intent.action.TIME_SET"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_3

    .line 72
    :cond_1
    iget-object v1, p0, Lcom/google/android/setupwizard/time/DateTimeMonitor$1;->this$0:Lcom/google/android/setupwizard/time/DateTimeMonitor;

    # invokes: Lcom/google/android/setupwizard/time/DateTimeMonitor;->timeChanged()V
    invoke-static {v1}, Lcom/google/android/setupwizard/time/DateTimeMonitor;->access$100(Lcom/google/android/setupwizard/time/DateTimeMonitor;)V

    .line 73
    iget-object v1, p0, Lcom/google/android/setupwizard/time/DateTimeMonitor$1;->this$0:Lcom/google/android/setupwizard/time/DateTimeMonitor;

    invoke-virtual {v1}, Lcom/google/android/setupwizard/time/DateTimeMonitor;->isTimeZoneSet()Z

    move-result v1

    if-nez v1, :cond_2

    .line 74
    iget-object v1, p0, Lcom/google/android/setupwizard/time/DateTimeMonitor$1;->this$0:Lcom/google/android/setupwizard/time/DateTimeMonitor;

    # invokes: Lcom/google/android/setupwizard/time/DateTimeMonitor;->setTimeZoneFromGservices()V
    invoke-static {v1}, Lcom/google/android/setupwizard/time/DateTimeMonitor;->access$200(Lcom/google/android/setupwizard/time/DateTimeMonitor;)V

    .line 86
    :cond_2
    :goto_0
    return-void

    .line 77
    :cond_3
    const-string v1, "android.intent.action.NETWORK_SET_TIMEZONE"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_4

    const-string v1, "android.intent.action.TIMEZONE_CHANGED"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_5

    .line 79
    :cond_4
    iget-object v1, p0, Lcom/google/android/setupwizard/time/DateTimeMonitor$1;->this$0:Lcom/google/android/setupwizard/time/DateTimeMonitor;

    # invokes: Lcom/google/android/setupwizard/time/DateTimeMonitor;->timeZoneChanged()V
    invoke-static {v1}, Lcom/google/android/setupwizard/time/DateTimeMonitor;->access$300(Lcom/google/android/setupwizard/time/DateTimeMonitor;)V

    goto :goto_0

    .line 81
    :cond_5
    const-string v1, "com.google.gservices.intent.action.GSERVICES_CHANGED"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 82
    iget-object v1, p0, Lcom/google/android/setupwizard/time/DateTimeMonitor$1;->this$0:Lcom/google/android/setupwizard/time/DateTimeMonitor;

    invoke-virtual {v1}, Lcom/google/android/setupwizard/time/DateTimeMonitor;->isTimeZoneSet()Z

    move-result v1

    if-nez v1, :cond_2

    .line 83
    iget-object v1, p0, Lcom/google/android/setupwizard/time/DateTimeMonitor$1;->this$0:Lcom/google/android/setupwizard/time/DateTimeMonitor;

    # invokes: Lcom/google/android/setupwizard/time/DateTimeMonitor;->setTimeZoneFromGservices()V
    invoke-static {v1}, Lcom/google/android/setupwizard/time/DateTimeMonitor;->access$200(Lcom/google/android/setupwizard/time/DateTimeMonitor;)V

    goto :goto_0
.end method
