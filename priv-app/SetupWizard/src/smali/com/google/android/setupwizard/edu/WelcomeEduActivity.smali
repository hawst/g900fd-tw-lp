.class public Lcom/google/android/setupwizard/edu/WelcomeEduActivity;
.super Lcom/google/android/setupwizard/BaseActivity;
.source "WelcomeEduActivity.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/setupwizard/edu/WelcomeEduActivity$BackgroundDrawable;
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 44
    invoke-direct {p0}, Lcom/google/android/setupwizard/BaseActivity;-><init>()V

    .line 100
    return-void
.end method

.method private finishAndReturnResult()V
    .locals 2

    .prologue
    const/4 v1, -0x1

    .line 82
    invoke-virtual {p0}, Lcom/google/android/setupwizard/edu/WelcomeEduActivity;->isUsingWizardManager()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 83
    invoke-virtual {p0, v1}, Lcom/google/android/setupwizard/edu/WelcomeEduActivity;->nextAction(I)Z

    .line 87
    :goto_0
    return-void

    .line 85
    :cond_0
    invoke-virtual {p0, v1}, Lcom/google/android/setupwizard/edu/WelcomeEduActivity;->finishAction(I)V

    goto :goto_0
.end method

.method private getWelcomeBackgroundDrawableName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 245
    invoke-static {p0}, Lcom/google/android/setupwizard/edu/WelcomeEduActivity;->getWelcomeBackgroundDrawableName(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public static getWelcomeBackgroundDrawableName(Landroid/content/Context;)Ljava/lang/String;
    .locals 3
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 238
    const-string v1, "SetupWizardPrefs"

    const/4 v2, 0x4

    invoke-virtual {p0, v1, v2}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v0

    .line 240
    .local v0, "prefs":Landroid/content/SharedPreferences;
    const-string v1, "setupwizard.background_resource"

    const-string v2, "edu_bg"

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    return-object v1
.end method

.method private getWelcomeBackgroundPackageName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 271
    invoke-static {p0}, Lcom/google/android/setupwizard/edu/WelcomeEduActivity;->getWelcomeBackgroundPackageName(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public static getWelcomeBackgroundPackageName(Landroid/content/Context;)Ljava/lang/String;
    .locals 3
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 264
    const-string v1, "SetupWizardPrefs"

    const/4 v2, 0x4

    invoke-virtual {p0, v1, v2}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v0

    .line 266
    .local v0, "prefs":Landroid/content/SharedPreferences;
    const-string v1, "setupwizard.background_package_name"

    const-string v2, "com.google.android.nfcprovision"

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    return-object v1
.end method

.method private initViews()V
    .locals 7

    .prologue
    const/16 v6, 0x8

    .line 60
    invoke-virtual {p0}, Lcom/google/android/setupwizard/edu/WelcomeEduActivity;->getLayoutInflater()Landroid/view/LayoutInflater;

    move-result-object v2

    .line 61
    .local v2, "inflater":Landroid/view/LayoutInflater;
    const v4, 0x7f030021

    const/4 v5, 0x0

    invoke-virtual {v2, v4, v5}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v4

    invoke-virtual {p0, v4}, Lcom/google/android/setupwizard/edu/WelcomeEduActivity;->setContentView(Landroid/view/View;)V

    .line 63
    new-instance v1, Lcom/google/android/setupwizard/edu/WelcomeEduActivity$BackgroundDrawable;

    invoke-virtual {p0}, Lcom/google/android/setupwizard/edu/WelcomeEduActivity;->getWelcomeBackground()Landroid/graphics/drawable/Drawable;

    move-result-object v4

    invoke-direct {v1, v4}, Lcom/google/android/setupwizard/edu/WelcomeEduActivity$BackgroundDrawable;-><init>(Landroid/graphics/drawable/Drawable;)V

    .line 64
    .local v1, "bg":Lcom/google/android/setupwizard/edu/WelcomeEduActivity$BackgroundDrawable;
    if-eqz v1, :cond_0

    .line 65
    const v4, 0x7f0e0055

    invoke-virtual {p0, v4}, Lcom/google/android/setupwizard/edu/WelcomeEduActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    .line 66
    .local v0, "background":Landroid/widget/ImageView;
    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 69
    .end local v0    # "background":Landroid/widget/ImageView;
    :cond_0
    invoke-virtual {p0}, Lcom/google/android/setupwizard/edu/WelcomeEduActivity;->isPrimaryUser()Z

    move-result v4

    if-eqz v4, :cond_1

    .line 70
    const v4, 0x7f0e006c

    invoke-virtual {p0, v4}, Lcom/google/android/setupwizard/edu/WelcomeEduActivity;->findViewById(I)Landroid/view/View;

    move-result-object v4

    invoke-virtual {v4, v6}, Landroid/view/View;->setVisibility(I)V

    .line 71
    const v4, 0x7f0e006d

    invoke-virtual {p0, v4}, Lcom/google/android/setupwizard/edu/WelcomeEduActivity;->findViewById(I)Landroid/view/View;

    move-result-object v4

    invoke-virtual {v4, v6}, Landroid/view/View;->setVisibility(I)V

    .line 76
    :goto_0
    return-void

    .line 73
    :cond_1
    const v4, 0x7f0e006b

    invoke-virtual {p0, v4}, Lcom/google/android/setupwizard/edu/WelcomeEduActivity;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/TextView;

    .line 74
    .local v3, "welcomeText":Landroid/widget/TextView;
    const v4, 0x7f070042

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setText(I)V

    goto :goto_0
.end method

.method public static setWelcomeBackgroundDrawableName(Ljava/lang/String;Landroid/content/Context;)V
    .locals 3
    .param p0, "resourceName"    # Ljava/lang/String;
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 227
    const-string v1, "SetupWizardPrefs"

    const/4 v2, 0x4

    invoke-virtual {p1, v1, v2}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v0

    .line 229
    .local v0, "prefs":Landroid/content/SharedPreferences;
    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v1

    const-string v2, "setupwizard.background_resource"

    invoke-interface {v1, v2, p0}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    move-result-object v1

    invoke-interface {v1}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 232
    return-void
.end method

.method public static setWelcomeBackgroundPackageName(Ljava/lang/String;Landroid/content/Context;)V
    .locals 3
    .param p0, "packageName"    # Ljava/lang/String;
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 253
    const-string v1, "SetupWizardPrefs"

    const/4 v2, 0x4

    invoke-virtual {p1, v1, v2}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v0

    .line 255
    .local v0, "prefs":Landroid/content/SharedPreferences;
    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v1

    const-string v2, "setupwizard.background_package_name"

    invoke-interface {v1, v2, p0}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    move-result-object v1

    invoke-interface {v1}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 258
    return-void
.end method


# virtual methods
.method protected getWelcomeBackground()Landroid/graphics/drawable/Drawable;
    .locals 9

    .prologue
    const/4 v6, 0x0

    .line 204
    invoke-virtual {p0}, Lcom/google/android/setupwizard/edu/WelcomeEduActivity;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v4

    .line 205
    .local v4, "pm":Landroid/content/pm/PackageManager;
    invoke-direct {p0}, Lcom/google/android/setupwizard/edu/WelcomeEduActivity;->getWelcomeBackgroundDrawableName()Ljava/lang/String;

    move-result-object v0

    .line 206
    .local v0, "drawableName":Ljava/lang/String;
    invoke-direct {p0}, Lcom/google/android/setupwizard/edu/WelcomeEduActivity;->getWelcomeBackgroundPackageName()Ljava/lang/String;

    move-result-object v3

    .line 209
    .local v3, "packageName":Ljava/lang/String;
    :try_start_0
    invoke-virtual {v4, v3}, Landroid/content/pm/PackageManager;->getResourcesForApplication(Ljava/lang/String;)Landroid/content/res/Resources;

    move-result-object v5

    .line 210
    .local v5, "resources":Landroid/content/res/Resources;
    if-nez v5, :cond_0

    .line 219
    .end local v5    # "resources":Landroid/content/res/Resources;
    :goto_0
    return-object v6

    .line 213
    .restart local v5    # "resources":Landroid/content/res/Resources;
    :cond_0
    const-string v7, "drawable"

    invoke-virtual {v5, v0, v7, v3}, Landroid/content/res/Resources;->getIdentifier(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)I

    move-result v2

    .line 214
    .local v2, "id":I
    invoke-virtual {v5, v2}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;
    :try_end_0
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v6

    goto :goto_0

    .line 215
    .end local v2    # "id":I
    .end local v5    # "resources":Landroid/content/res/Resources;
    :catch_0
    move-exception v1

    .line 216
    .local v1, "e":Landroid/content/pm/PackageManager$NameNotFoundException;
    iget-object v7, p0, Lcom/google/android/setupwizard/edu/WelcomeEduActivity;->TAG:Ljava/lang/String;

    const-string v8, "Could not find specified background for Welcome screen. Reverting to default."

    invoke-static {v7, v8, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_0
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 0
    .param p1, "icicle"    # Landroid/os/Bundle;

    .prologue
    .line 51
    invoke-super {p0, p1}, Lcom/google/android/setupwizard/BaseActivity;->onCreate(Landroid/os/Bundle;)V

    .line 55
    invoke-virtual {p0}, Lcom/google/android/setupwizard/edu/WelcomeEduActivity;->lockRotation()V

    .line 56
    invoke-direct {p0}, Lcom/google/android/setupwizard/edu/WelcomeEduActivity;->initViews()V

    .line 57
    return-void
.end method

.method public start(Landroid/view/View;)V
    .locals 0
    .param p1, "v"    # Landroid/view/View;

    .prologue
    .line 90
    invoke-direct {p0}, Lcom/google/android/setupwizard/edu/WelcomeEduActivity;->finishAndReturnResult()V

    .line 91
    return-void
.end method
