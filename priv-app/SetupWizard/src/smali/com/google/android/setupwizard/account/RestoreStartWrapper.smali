.class public Lcom/google/android/setupwizard/account/RestoreStartWrapper;
.super Lcom/google/android/setupwizard/SubactivityWrapper;
.source "RestoreStartWrapper.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/setupwizard/account/RestoreStartWrapper$1;,
        Lcom/google/android/setupwizard/account/RestoreStartWrapper$GetRestoreFlowFragment;,
        Lcom/google/android/setupwizard/account/RestoreStartWrapper$DelayedRestoreFragment;
    }
.end annotation


# instance fields
.field private mAccount:Landroid/accounts/Account;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 45
    invoke-direct {p0}, Lcom/google/android/setupwizard/SubactivityWrapper;-><init>()V

    .line 211
    return-void
.end method

.method static synthetic access$000(Lcom/google/android/setupwizard/account/RestoreStartWrapper;)V
    .locals 0
    .param p0, "x0"    # Lcom/google/android/setupwizard/account/RestoreStartWrapper;

    .prologue
    .line 45
    invoke-virtual {p0}, Lcom/google/android/setupwizard/account/RestoreStartWrapper;->nextScreen()V

    return-void
.end method

.method static synthetic access$100(Lcom/google/android/setupwizard/account/RestoreStartWrapper;Landroid/content/Intent;)V
    .locals 0
    .param p0, "x0"    # Lcom/google/android/setupwizard/account/RestoreStartWrapper;
    .param p1, "x1"    # Landroid/content/Intent;

    .prologue
    .line 45
    invoke-virtual {p0, p1}, Lcom/google/android/setupwizard/account/RestoreStartWrapper;->launchSubactivity(Landroid/content/Intent;)V

    return-void
.end method

.method static synthetic access$200(Lcom/google/android/setupwizard/account/RestoreStartWrapper;)V
    .locals 0
    .param p0, "x0"    # Lcom/google/android/setupwizard/account/RestoreStartWrapper;

    .prologue
    .line 45
    invoke-virtual {p0}, Lcom/google/android/setupwizard/account/RestoreStartWrapper;->nextScreen()V

    return-void
.end method

.method static synthetic access$500(Lcom/google/android/setupwizard/account/RestoreStartWrapper;)Landroid/accounts/Account;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/setupwizard/account/RestoreStartWrapper;

    .prologue
    .line 45
    iget-object v0, p0, Lcom/google/android/setupwizard/account/RestoreStartWrapper;->mAccount:Landroid/accounts/Account;

    return-object v0
.end method


# virtual methods
.method protected getSubactivityNextTransition()I
    .locals 1

    .prologue
    .line 160
    const/4 v0, 0x1

    return v0
.end method

.method protected getSubactivityPreviousTransition()I
    .locals 1

    .prologue
    .line 155
    const/4 v0, 0x1

    return v0
.end method

.method protected launchSubactivity()V
    .locals 8

    .prologue
    const-wide/16 v6, 0x0

    .line 84
    iget-object v4, p0, Lcom/google/android/setupwizard/account/RestoreStartWrapper;->TAG:Ljava/lang/String;

    const-string v5, "RestoreStartWrapper.launchSubactivity"

    invoke-static {v4, v5}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 86
    invoke-static {}, Lcom/google/android/setupwizard/util/BackupRestoreHelper;->hasAttemptedRestore()Z

    move-result v4

    if-eqz v4, :cond_0

    .line 87
    iget-object v4, p0, Lcom/google/android/setupwizard/account/RestoreStartWrapper;->TAG:Ljava/lang/String;

    const-string v5, "Restore attempted previously; skipping"

    invoke-static {v4, v5}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 89
    invoke-virtual {p0}, Lcom/google/android/setupwizard/account/RestoreStartWrapper;->nextScreen()V

    .line 90
    invoke-virtual {p0}, Lcom/google/android/setupwizard/account/RestoreStartWrapper;->finish()V

    .line 127
    :goto_0
    return-void

    .line 94
    :cond_0
    invoke-virtual {p0}, Lcom/google/android/setupwizard/account/RestoreStartWrapper;->getLocalPreferences()Landroid/content/SharedPreferences;

    move-result-object v0

    .line 95
    .local v0, "localPref":Landroid/content/SharedPreferences;
    const-string v4, "restoreAccount"

    const-string v5, ""

    invoke-interface {v0, v4, v5}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {p0, v4}, Lcom/google/android/setupwizard/account/RestoreStartWrapper;->getAccount(Ljava/lang/String;)Landroid/accounts/Account;

    move-result-object v1

    .line 96
    .local v1, "mfmAccount":Landroid/accounts/Account;
    const-string v4, "restoreToken"

    invoke-interface {v0, v4, v6, v7}, Landroid/content/SharedPreferences;->getLong(Ljava/lang/String;J)J

    move-result-wide v2

    .line 98
    .local v2, "mfmRestoreToken":J
    if-eqz v1, :cond_1

    cmp-long v4, v2, v6

    if-eqz v4, :cond_1

    .line 102
    iget-object v4, p0, Lcom/google/android/setupwizard/account/RestoreStartWrapper;->TAG:Ljava/lang/String;

    const-string v5, "MFM restore token found"

    invoke-static {v4, v5}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 103
    invoke-virtual {p0}, Lcom/google/android/setupwizard/account/RestoreStartWrapper;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v4

    invoke-static {v4, v1, v2, v3}, Lcom/google/android/setupwizard/account/RestoreStartWrapper$DelayedRestoreFragment;->attach(Landroid/app/FragmentManager;Landroid/accounts/Account;J)V

    goto :goto_0

    .line 105
    :cond_1
    invoke-virtual {p0}, Lcom/google/android/setupwizard/account/RestoreStartWrapper;->getAccount()Landroid/accounts/Account;

    move-result-object v4

    iput-object v4, p0, Lcom/google/android/setupwizard/account/RestoreStartWrapper;->mAccount:Landroid/accounts/Account;

    .line 106
    iget-object v4, p0, Lcom/google/android/setupwizard/account/RestoreStartWrapper;->mAccount:Landroid/accounts/Account;

    if-eqz v4, :cond_3

    .line 107
    iget-object v4, p0, Lcom/google/android/setupwizard/account/RestoreStartWrapper;->mAccount:Landroid/accounts/Account;

    invoke-static {v4}, Lcom/google/android/setupwizard/util/BackupRestoreHelper;->setBackupAccount(Landroid/accounts/Account;)Z

    .line 108
    sget-boolean v4, Lcom/google/android/setupwizard/account/RestoreStartWrapper;->SHOW_BACKUP_PICKER:Z

    if-nez v4, :cond_2

    .line 111
    iget-object v4, p0, Lcom/google/android/setupwizard/account/RestoreStartWrapper;->TAG:Ljava/lang/String;

    const-string v5, "show_backup_picker is not set. Skipping backup selection UI."

    invoke-static {v4, v5}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 113
    invoke-static {v6, v7}, Lcom/google/android/setupwizard/util/BackupRestoreHelper;->startRestore(J)V

    .line 114
    invoke-virtual {p0}, Lcom/google/android/setupwizard/account/RestoreStartWrapper;->nextScreen()V

    .line 115
    invoke-virtual {p0}, Lcom/google/android/setupwizard/account/RestoreStartWrapper;->finish()V

    goto :goto_0

    .line 119
    :cond_2
    iget-object v4, p0, Lcom/google/android/setupwizard/account/RestoreStartWrapper;->TAG:Ljava/lang/String;

    const-string v5, "showing backup picker"

    invoke-static {v4, v5}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 120
    invoke-virtual {p0}, Lcom/google/android/setupwizard/account/RestoreStartWrapper;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v4

    invoke-static {v4}, Lcom/google/android/setupwizard/account/RestoreStartWrapper$GetRestoreFlowFragment;->attach(Landroid/app/FragmentManager;)V

    goto :goto_0

    .line 122
    :cond_3
    iget-object v4, p0, Lcom/google/android/setupwizard/account/RestoreStartWrapper;->TAG:Ljava/lang/String;

    const-string v5, "account not set, skipping restore"

    invoke-static {v4, v5}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 123
    const/4 v4, 0x1

    invoke-virtual {p0, v4}, Lcom/google/android/setupwizard/account/RestoreStartWrapper;->nextScreen(I)V

    .line 124
    invoke-virtual {p0}, Lcom/google/android/setupwizard/account/RestoreStartWrapper;->finish()V

    goto :goto_0
.end method

.method protected onRestoreInstanceState(Landroid/os/Bundle;)V
    .locals 1
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 56
    invoke-super {p0, p1}, Lcom/google/android/setupwizard/SubactivityWrapper;->onRestoreInstanceState(Landroid/os/Bundle;)V

    .line 57
    const-string v0, "account"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Landroid/accounts/Account;

    iput-object v0, p0, Lcom/google/android/setupwizard/account/RestoreStartWrapper;->mAccount:Landroid/accounts/Account;

    .line 58
    return-void
.end method

.method protected onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 2
    .param p1, "outState"    # Landroid/os/Bundle;

    .prologue
    .line 62
    const-string v0, "account"

    iget-object v1, p0, Lcom/google/android/setupwizard/account/RestoreStartWrapper;->mAccount:Landroid/accounts/Account;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 63
    invoke-super {p0, p1}, Lcom/google/android/setupwizard/SubactivityWrapper;->onSaveInstanceState(Landroid/os/Bundle;)V

    .line 64
    return-void
.end method

.method protected onSubactivityLaunch()Landroid/content/Intent;
    .locals 1

    .prologue
    .line 74
    const/4 v0, 0x0

    return-object v0
.end method

.method protected onSubactivityResult(IILandroid/content/Intent;)V
    .locals 8
    .param p1, "requestCode"    # I
    .param p2, "resultCode"    # I
    .param p3, "data"    # Landroid/content/Intent;

    .prologue
    const/4 v7, 0x1

    const/4 v4, -0x1

    .line 131
    if-ne p2, v4, :cond_1

    .line 132
    if-eqz p3, :cond_0

    .line 133
    const-string v4, "restoreToken"

    invoke-virtual {p3, v4}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 136
    .local v1, "restoreTokenString":Ljava/lang/String;
    const/16 v4, 0x10

    :try_start_0
    invoke-static {v1, v4}, Ljava/lang/Long;->valueOf(Ljava/lang/String;I)Ljava/lang/Long;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    .line 137
    .local v2, "token":J
    invoke-static {v2, v3}, Lcom/google/android/setupwizard/util/BackupRestoreHelper;->startRestore(J)V

    .line 140
    const/4 v4, -0x1

    invoke-virtual {p0, v4, p3}, Lcom/google/android/setupwizard/account/RestoreStartWrapper;->finishAction(ILandroid/content/Intent;)V
    :try_end_0
    .catch Ljava/lang/NumberFormatException; {:try_start_0 .. :try_end_0} :catch_0

    .line 151
    .end local v1    # "restoreTokenString":Ljava/lang/String;
    .end local v2    # "token":J
    :goto_0
    return-void

    .line 141
    .restart local v1    # "restoreTokenString":Ljava/lang/String;
    :catch_0
    move-exception v0

    .line 142
    .local v0, "e":Ljava/lang/NumberFormatException;
    iget-object v4, p0, Lcom/google/android/setupwizard/account/RestoreStartWrapper;->TAG:Ljava/lang/String;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "Cannot parse hexadecimal restoreToken: "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 143
    invoke-virtual {p0, v7}, Lcom/google/android/setupwizard/account/RestoreStartWrapper;->finishAction(I)V

    goto :goto_0

    .line 146
    .end local v0    # "e":Ljava/lang/NumberFormatException;
    .end local v1    # "restoreTokenString":Ljava/lang/String;
    :cond_0
    invoke-virtual {p0, v7}, Lcom/google/android/setupwizard/account/RestoreStartWrapper;->finishAction(I)V

    goto :goto_0

    .line 149
    :cond_1
    invoke-super {p0, p1, p2, p3}, Lcom/google/android/setupwizard/SubactivityWrapper;->onSubactivityResult(IILandroid/content/Intent;)V

    goto :goto_0
.end method

.method protected requestCode()I
    .locals 1

    .prologue
    .line 68
    const/16 v0, 0x272b

    return v0
.end method
