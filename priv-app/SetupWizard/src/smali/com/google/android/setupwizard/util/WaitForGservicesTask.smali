.class public Lcom/google/android/setupwizard/util/WaitForGservicesTask;
.super Landroid/app/Fragment;
.source "WaitForGservicesTask.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/setupwizard/util/WaitForGservicesTask$WaitForGservicesListener;
    }
.end annotation


# instance fields
.field private mCallback:Lcom/google/android/setupwizard/util/WaitForGservicesTask$WaitForGservicesListener;

.field private final mCheckInCompleteReceiver:Landroid/content/BroadcastReceiver;

.field private final mHandler:Landroid/os/Handler;

.field private final mTimeoutRunnable:Ljava/lang/Runnable;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 74
    invoke-direct {p0}, Landroid/app/Fragment;-><init>()V

    .line 48
    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    iput-object v0, p0, Lcom/google/android/setupwizard/util/WaitForGservicesTask;->mHandler:Landroid/os/Handler;

    .line 51
    new-instance v0, Lcom/google/android/setupwizard/util/WaitForGservicesTask$1;

    invoke-direct {v0, p0}, Lcom/google/android/setupwizard/util/WaitForGservicesTask$1;-><init>(Lcom/google/android/setupwizard/util/WaitForGservicesTask;)V

    iput-object v0, p0, Lcom/google/android/setupwizard/util/WaitForGservicesTask;->mCheckInCompleteReceiver:Landroid/content/BroadcastReceiver;

    .line 61
    new-instance v0, Lcom/google/android/setupwizard/util/WaitForGservicesTask$2;

    invoke-direct {v0, p0}, Lcom/google/android/setupwizard/util/WaitForGservicesTask$2;-><init>(Lcom/google/android/setupwizard/util/WaitForGservicesTask;)V

    iput-object v0, p0, Lcom/google/android/setupwizard/util/WaitForGservicesTask;->mTimeoutRunnable:Ljava/lang/Runnable;

    .line 76
    return-void
.end method

.method static synthetic access$000(Lcom/google/android/setupwizard/util/WaitForGservicesTask;)Z
    .locals 1
    .param p0, "x0"    # Lcom/google/android/setupwizard/util/WaitForGservicesTask;

    .prologue
    .line 44
    invoke-direct {p0}, Lcom/google/android/setupwizard/util/WaitForGservicesTask;->isReady()Z

    move-result v0

    return v0
.end method

.method static synthetic access$100(Lcom/google/android/setupwizard/util/WaitForGservicesTask;)Lcom/google/android/setupwizard/util/WaitForGservicesTask$WaitForGservicesListener;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/setupwizard/util/WaitForGservicesTask;

    .prologue
    .line 44
    iget-object v0, p0, Lcom/google/android/setupwizard/util/WaitForGservicesTask;->mCallback:Lcom/google/android/setupwizard/util/WaitForGservicesTask$WaitForGservicesListener;

    return-object v0
.end method

.method static synthetic access$200(Lcom/google/android/setupwizard/util/WaitForGservicesTask;)V
    .locals 0
    .param p0, "x0"    # Lcom/google/android/setupwizard/util/WaitForGservicesTask;

    .prologue
    .line 44
    invoke-direct {p0}, Lcom/google/android/setupwizard/util/WaitForGservicesTask;->done()V

    return-void
.end method

.method private done()V
    .locals 2

    .prologue
    .line 184
    :try_start_0
    invoke-virtual {p0}, Lcom/google/android/setupwizard/util/WaitForGservicesTask;->getActivity()Landroid/app/Activity;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/setupwizard/util/WaitForGservicesTask;->mCheckInCompleteReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {v0, v1}, Landroid/app/Activity;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0

    .line 188
    :goto_0
    iget-object v0, p0, Lcom/google/android/setupwizard/util/WaitForGservicesTask;->mHandler:Landroid/os/Handler;

    iget-object v1, p0, Lcom/google/android/setupwizard/util/WaitForGservicesTask;->mTimeoutRunnable:Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    .line 189
    return-void

    .line 185
    :catch_0
    move-exception v0

    goto :goto_0
.end method

.method public static getInstanceWithTag(Landroid/app/Activity;JLjava/lang/String;)Lcom/google/android/setupwizard/util/WaitForGservicesTask;
    .locals 1
    .param p0, "activity"    # Landroid/app/Activity;
    .param p1, "timeoutMillis"    # J
    .param p3, "tag"    # Ljava/lang/String;

    .prologue
    .line 97
    const/4 v0, 0x0

    invoke-static {p0, p1, p2, p3, v0}, Lcom/google/android/setupwizard/util/WaitForGservicesTask;->getInstanceWithTag(Landroid/app/Activity;JLjava/lang/String;Z)Lcom/google/android/setupwizard/util/WaitForGservicesTask;

    move-result-object v0

    return-object v0
.end method

.method public static getInstanceWithTag(Landroid/app/Activity;JLjava/lang/String;Z)Lcom/google/android/setupwizard/util/WaitForGservicesTask;
    .locals 3
    .param p0, "activity"    # Landroid/app/Activity;
    .param p1, "timeoutMillis"    # J
    .param p3, "tag"    # Ljava/lang/String;
    .param p4, "accountCheckin"    # Z

    .prologue
    .line 112
    invoke-virtual {p0}, Landroid/app/Activity;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v0

    .line 113
    .local v0, "fragmentManager":Landroid/app/FragmentManager;
    invoke-virtual {v0, p3}, Landroid/app/FragmentManager;->findFragmentByTag(Ljava/lang/String;)Landroid/app/Fragment;

    move-result-object v1

    check-cast v1, Lcom/google/android/setupwizard/util/WaitForGservicesTask;

    .line 114
    .local v1, "task":Lcom/google/android/setupwizard/util/WaitForGservicesTask;
    if-nez v1, :cond_0

    .line 115
    invoke-static {p1, p2, p4}, Lcom/google/android/setupwizard/util/WaitForGservicesTask;->newInstance(JZ)Lcom/google/android/setupwizard/util/WaitForGservicesTask;

    move-result-object v1

    .line 116
    invoke-virtual {v0}, Landroid/app/FragmentManager;->beginTransaction()Landroid/app/FragmentTransaction;

    move-result-object v2

    invoke-virtual {v2, v1, p3}, Landroid/app/FragmentTransaction;->add(Landroid/app/Fragment;Ljava/lang/String;)Landroid/app/FragmentTransaction;

    move-result-object v2

    invoke-virtual {v2}, Landroid/app/FragmentTransaction;->commit()I

    .line 118
    :cond_0
    return-object v1
.end method

.method private isReady()Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 176
    invoke-virtual {p0}, Lcom/google/android/setupwizard/util/WaitForGservicesTask;->getArguments()Landroid/os/Bundle;

    move-result-object v1

    .line 177
    .local v1, "args":Landroid/os/Bundle;
    if-eqz v1, :cond_0

    const-string v2, "accountCheckin"

    invoke-virtual {v1, v2, v0}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v2

    if-eqz v2, :cond_0

    const/4 v0, 0x1

    .line 178
    .local v0, "accountCheckin":Z
    :cond_0
    if-eqz v0, :cond_1

    invoke-static {}, Lcom/google/android/setupwizard/util/GservicesChangedReceiver;->hasAccountCheckin()Z

    move-result v2

    :goto_0
    return v2

    :cond_1
    invoke-static {}, Lcom/google/android/setupwizard/util/GservicesChangedReceiver;->hasGservicesChanged()Z

    move-result v2

    goto :goto_0
.end method

.method public static newInstance(JZ)Lcom/google/android/setupwizard/util/WaitForGservicesTask;
    .locals 4
    .param p0, "timeoutMillis"    # J
    .param p2, "accountCheckin"    # Z

    .prologue
    .line 87
    new-instance v1, Lcom/google/android/setupwizard/util/WaitForGservicesTask;

    invoke-direct {v1}, Lcom/google/android/setupwizard/util/WaitForGservicesTask;-><init>()V

    .line 88
    .local v1, "task":Lcom/google/android/setupwizard/util/WaitForGservicesTask;
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 89
    .local v0, "args":Landroid/os/Bundle;
    const-string v2, "timeoutMillis"

    invoke-virtual {v0, v2, p0, p1}, Landroid/os/Bundle;->putLong(Ljava/lang/String;J)V

    .line 90
    const-string v2, "accountCheckin"

    invoke-virtual {v0, v2, p2}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 91
    invoke-virtual {v1, v0}, Lcom/google/android/setupwizard/util/WaitForGservicesTask;->setArguments(Landroid/os/Bundle;)V

    .line 92
    return-object v1
.end method


# virtual methods
.method public onAttach(Landroid/app/Activity;)V
    .locals 1
    .param p1, "activity"    # Landroid/app/Activity;

    .prologue
    .line 123
    invoke-super {p0, p1}, Landroid/app/Fragment;->onAttach(Landroid/app/Activity;)V

    .line 126
    invoke-virtual {p0}, Lcom/google/android/setupwizard/util/WaitForGservicesTask;->getActivity()Landroid/app/Activity;

    move-result-object v0

    check-cast v0, Lcom/google/android/setupwizard/util/WaitForGservicesTask$WaitForGservicesListener;

    iput-object v0, p0, Lcom/google/android/setupwizard/util/WaitForGservicesTask;->mCallback:Lcom/google/android/setupwizard/util/WaitForGservicesTask$WaitForGservicesListener;

    .line 127
    return-void
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 6
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 131
    invoke-super {p0, p1}, Landroid/app/Fragment;->onCreate(Landroid/os/Bundle;)V

    .line 132
    const/4 v1, 0x1

    invoke-virtual {p0, v1}, Lcom/google/android/setupwizard/util/WaitForGservicesTask;->setRetainInstance(Z)V

    .line 134
    invoke-virtual {p0}, Lcom/google/android/setupwizard/util/WaitForGservicesTask;->getArguments()Landroid/os/Bundle;

    move-result-object v0

    .line 135
    .local v0, "args":Landroid/os/Bundle;
    if-eqz v0, :cond_2

    .line 136
    const-string v1, "accountCheckin"

    const/4 v4, 0x0

    invoke-virtual {v0, v1, v4}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 137
    invoke-static {}, Lcom/google/android/setupwizard/util/GservicesChangedReceiver;->startWaitingForAccountCheckin()V

    .line 139
    :cond_0
    const-string v1, "timeoutMillis"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getLong(Ljava/lang/String;)J

    move-result-wide v2

    .line 140
    .local v2, "timeoutMillis":J
    const-wide/16 v4, 0x0

    cmp-long v1, v2, v4

    if-ltz v1, :cond_1

    .line 141
    iget-object v1, p0, Lcom/google/android/setupwizard/util/WaitForGservicesTask;->mHandler:Landroid/os/Handler;

    iget-object v4, p0, Lcom/google/android/setupwizard/util/WaitForGservicesTask;->mTimeoutRunnable:Ljava/lang/Runnable;

    invoke-virtual {v1, v4, v2, v3}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 146
    .end local v2    # "timeoutMillis":J
    :cond_1
    :goto_0
    return-void

    .line 144
    :cond_2
    const-string v1, "SetupWizard"

    const-string v4, "WaitForGservicesTask argument bundle is null"

    invoke-static {v1, v4}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method public onDestroy()V
    .locals 2

    .prologue
    .line 171
    iget-object v0, p0, Lcom/google/android/setupwizard/util/WaitForGservicesTask;->mHandler:Landroid/os/Handler;

    iget-object v1, p0, Lcom/google/android/setupwizard/util/WaitForGservicesTask;->mTimeoutRunnable:Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    .line 172
    invoke-super {p0}, Landroid/app/Fragment;->onDestroy()V

    .line 173
    return-void
.end method

.method public onPause()V
    .locals 2

    .prologue
    .line 162
    :try_start_0
    invoke-virtual {p0}, Lcom/google/android/setupwizard/util/WaitForGservicesTask;->getActivity()Landroid/app/Activity;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/setupwizard/util/WaitForGservicesTask;->mCheckInCompleteReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {v0, v1}, Landroid/app/Activity;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0

    .line 166
    :goto_0
    invoke-super {p0}, Landroid/app/Fragment;->onPause()V

    .line 167
    return-void

    .line 163
    :catch_0
    move-exception v0

    goto :goto_0
.end method

.method public onResume()V
    .locals 3

    .prologue
    .line 150
    invoke-super {p0}, Landroid/app/Fragment;->onResume()V

    .line 151
    invoke-virtual {p0}, Lcom/google/android/setupwizard/util/WaitForGservicesTask;->getActivity()Landroid/app/Activity;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/setupwizard/util/WaitForGservicesTask;->mCheckInCompleteReceiver:Landroid/content/BroadcastReceiver;

    sget-object v2, Lcom/google/android/setupwizard/util/GservicesChangedReceiver;->FILTER_CHECKIN_SUCCESS:Landroid/content/IntentFilter;

    invoke-virtual {v0, v1, v2}, Landroid/app/Activity;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    .line 153
    invoke-direct {p0}, Lcom/google/android/setupwizard/util/WaitForGservicesTask;->isReady()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 154
    iget-object v0, p0, Lcom/google/android/setupwizard/util/WaitForGservicesTask;->mCallback:Lcom/google/android/setupwizard/util/WaitForGservicesTask$WaitForGservicesListener;

    invoke-interface {v0}, Lcom/google/android/setupwizard/util/WaitForGservicesTask$WaitForGservicesListener;->onGservicesReady()V

    .line 155
    invoke-direct {p0}, Lcom/google/android/setupwizard/util/WaitForGservicesTask;->done()V

    .line 157
    :cond_0
    return-void
.end method
