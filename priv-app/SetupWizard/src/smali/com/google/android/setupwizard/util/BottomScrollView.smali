.class public Lcom/google/android/setupwizard/util/BottomScrollView;
.super Landroid/widget/ScrollView;
.source "BottomScrollView.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/setupwizard/util/BottomScrollView$BottomScrollListener;
    }
.end annotation


# instance fields
.field private mListener:Lcom/google/android/setupwizard/util/BottomScrollView$BottomScrollListener;

.field private mRequiringScroll:Z

.field private mScrollThreshold:I


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 36
    invoke-direct {p0, p1}, Landroid/widget/ScrollView;-><init>(Landroid/content/Context;)V

    .line 33
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/setupwizard/util/BottomScrollView;->mRequiringScroll:Z

    .line 37
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    .line 40
    invoke-direct {p0, p1, p2}, Landroid/widget/ScrollView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 33
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/setupwizard/util/BottomScrollView;->mRequiringScroll:Z

    .line 41
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;
    .param p3, "defStyle"    # I

    .prologue
    .line 44
    invoke-direct {p0, p1, p2, p3}, Landroid/widget/ScrollView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 33
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/setupwizard/util/BottomScrollView;->mRequiringScroll:Z

    .line 45
    return-void
.end method

.method private checkScroll()V
    .locals 2

    .prologue
    .line 73
    iget-object v0, p0, Lcom/google/android/setupwizard/util/BottomScrollView;->mListener:Lcom/google/android/setupwizard/util/BottomScrollView$BottomScrollListener;

    if-eqz v0, :cond_0

    .line 74
    invoke-virtual {p0}, Lcom/google/android/setupwizard/util/BottomScrollView;->getScrollY()I

    move-result v0

    iget v1, p0, Lcom/google/android/setupwizard/util/BottomScrollView;->mScrollThreshold:I

    if-lt v0, v1, :cond_1

    .line 75
    iget-object v0, p0, Lcom/google/android/setupwizard/util/BottomScrollView;->mListener:Lcom/google/android/setupwizard/util/BottomScrollView$BottomScrollListener;

    invoke-interface {v0}, Lcom/google/android/setupwizard/util/BottomScrollView$BottomScrollListener;->onScrolledToBottom()V

    .line 81
    :cond_0
    :goto_0
    return-void

    .line 76
    :cond_1
    iget-boolean v0, p0, Lcom/google/android/setupwizard/util/BottomScrollView;->mRequiringScroll:Z

    if-nez v0, :cond_0

    .line 77
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/setupwizard/util/BottomScrollView;->mRequiringScroll:Z

    .line 78
    iget-object v0, p0, Lcom/google/android/setupwizard/util/BottomScrollView;->mListener:Lcom/google/android/setupwizard/util/BottomScrollView$BottomScrollListener;

    invoke-interface {v0}, Lcom/google/android/setupwizard/util/BottomScrollView$BottomScrollListener;->onRequiresScroll()V

    goto :goto_0
.end method


# virtual methods
.method protected onLayout(ZIIII)V
    .locals 4
    .param p1, "changed"    # Z
    .param p2, "l"    # I
    .param p3, "t"    # I
    .param p4, "r"    # I
    .param p5, "b"    # I

    .prologue
    const/4 v3, 0x0

    .line 53
    invoke-super/range {p0 .. p5}, Landroid/widget/ScrollView;->onLayout(ZIIII)V

    .line 54
    invoke-virtual {p0, v3}, Lcom/google/android/setupwizard/util/BottomScrollView;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    .line 55
    .local v0, "child":Landroid/view/View;
    if-eqz v0, :cond_0

    .line 56
    invoke-virtual {p0, v3}, Lcom/google/android/setupwizard/util/BottomScrollView;->getChildAt(I)Landroid/view/View;

    move-result-object v1

    invoke-virtual {v1}, Landroid/view/View;->getHeight()I

    move-result v1

    sub-int/2addr v1, p5

    add-int/2addr v1, p3

    invoke-virtual {p0}, Lcom/google/android/setupwizard/util/BottomScrollView;->getPaddingBottom()I

    move-result v2

    sub-int/2addr v1, v2

    invoke-static {v3, v1}, Ljava/lang/Math;->max(II)I

    move-result v1

    iput v1, p0, Lcom/google/android/setupwizard/util/BottomScrollView;->mScrollThreshold:I

    .line 59
    :cond_0
    sub-int v1, p5, p3

    if-lez v1, :cond_1

    .line 60
    invoke-direct {p0}, Lcom/google/android/setupwizard/util/BottomScrollView;->checkScroll()V

    .line 62
    :cond_1
    return-void
.end method

.method protected onScrollChanged(IIII)V
    .locals 0
    .param p1, "l"    # I
    .param p2, "t"    # I
    .param p3, "oldl"    # I
    .param p4, "oldt"    # I

    .prologue
    .line 66
    invoke-super {p0, p1, p2, p3, p4}, Landroid/widget/ScrollView;->onScrollChanged(IIII)V

    .line 67
    if-eq p4, p2, :cond_0

    .line 68
    invoke-direct {p0}, Lcom/google/android/setupwizard/util/BottomScrollView;->checkScroll()V

    .line 70
    :cond_0
    return-void
.end method

.method public setBottomScrollListener(Lcom/google/android/setupwizard/util/BottomScrollView$BottomScrollListener;)V
    .locals 0
    .param p1, "l"    # Lcom/google/android/setupwizard/util/BottomScrollView$BottomScrollListener;

    .prologue
    .line 48
    iput-object p1, p0, Lcom/google/android/setupwizard/util/BottomScrollView;->mListener:Lcom/google/android/setupwizard/util/BottomScrollView$BottomScrollListener;

    .line 49
    return-void
.end method
