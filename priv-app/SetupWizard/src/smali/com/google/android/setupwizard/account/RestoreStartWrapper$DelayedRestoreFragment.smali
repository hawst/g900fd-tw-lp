.class public Lcom/google/android/setupwizard/account/RestoreStartWrapper$DelayedRestoreFragment;
.super Landroid/app/Fragment;
.source "RestoreStartWrapper.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/setupwizard/account/RestoreStartWrapper;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "DelayedRestoreFragment"
.end annotation


# static fields
.field public static final DELAY_RESTORE_MILLIS:Lcom/google/android/setupwizard/util/GservicesValue;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/setupwizard/util/GservicesValue",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private final mHandler:Landroid/os/Handler;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 170
    const-string v0, "google_setup:restore_delay"

    const/16 v1, 0x7d0

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/setupwizard/util/GservicesValue;->value(Ljava/lang/String;Ljava/lang/Integer;)Lcom/google/android/setupwizard/util/GservicesValue;

    move-result-object v0

    sput-object v0, Lcom/google/android/setupwizard/account/RestoreStartWrapper$DelayedRestoreFragment;->DELAY_RESTORE_MILLIS:Lcom/google/android/setupwizard/util/GservicesValue;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 184
    invoke-direct {p0}, Landroid/app/Fragment;-><init>()V

    .line 182
    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    iput-object v0, p0, Lcom/google/android/setupwizard/account/RestoreStartWrapper$DelayedRestoreFragment;->mHandler:Landroid/os/Handler;

    .line 185
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/google/android/setupwizard/account/RestoreStartWrapper$DelayedRestoreFragment;->setRetainInstance(Z)V

    .line 186
    return-void
.end method

.method public static attach(Landroid/app/FragmentManager;Landroid/accounts/Account;J)V
    .locals 4
    .param p0, "fm"    # Landroid/app/FragmentManager;
    .param p1, "account"    # Landroid/accounts/Account;
    .param p2, "restoreToken"    # J

    .prologue
    .line 174
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 175
    .local v0, "args":Landroid/os/Bundle;
    const-string v2, "mfmAccount"

    invoke-virtual {v0, v2, p1}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 176
    const-string v2, "mfmRestoreToken"

    invoke-virtual {v0, v2, p2, p3}, Landroid/os/Bundle;->putLong(Ljava/lang/String;J)V

    .line 177
    new-instance v1, Lcom/google/android/setupwizard/account/RestoreStartWrapper$DelayedRestoreFragment;

    invoke-direct {v1}, Lcom/google/android/setupwizard/account/RestoreStartWrapper$DelayedRestoreFragment;-><init>()V

    .line 178
    .local v1, "fragment":Lcom/google/android/setupwizard/account/RestoreStartWrapper$DelayedRestoreFragment;
    invoke-virtual {v1, v0}, Lcom/google/android/setupwizard/account/RestoreStartWrapper$DelayedRestoreFragment;->setArguments(Landroid/os/Bundle;)V

    .line 179
    invoke-virtual {p0}, Landroid/app/FragmentManager;->beginTransaction()Landroid/app/FragmentTransaction;

    move-result-object v2

    const-string v3, "delayFragment"

    invoke-virtual {v2, v1, v3}, Landroid/app/FragmentTransaction;->add(Landroid/app/Fragment;Ljava/lang/String;)Landroid/app/FragmentTransaction;

    move-result-object v2

    invoke-virtual {v2}, Landroid/app/FragmentTransaction;->commit()I

    .line 180
    return-void
.end method


# virtual methods
.method public onCreate(Landroid/os/Bundle;)V
    .locals 6
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 190
    invoke-super {p0, p1}, Landroid/app/Fragment;->onCreate(Landroid/os/Bundle;)V

    .line 191
    invoke-virtual {p0}, Lcom/google/android/setupwizard/account/RestoreStartWrapper$DelayedRestoreFragment;->getArguments()Landroid/os/Bundle;

    move-result-object v1

    const-string v2, "mfmAccount"

    invoke-virtual {v1, v2}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Landroid/accounts/Account;

    .line 192
    .local v0, "account":Landroid/accounts/Account;
    invoke-static {v0}, Lcom/google/android/setupwizard/util/BackupRestoreHelper;->setBackupAccount(Landroid/accounts/Account;)Z

    .line 193
    const/4 v1, 0x1

    invoke-static {v1}, Lcom/google/android/setupwizard/util/BackupRestoreHelper;->setBackupEnabled(Z)Z

    .line 194
    iget-object v2, p0, Lcom/google/android/setupwizard/account/RestoreStartWrapper$DelayedRestoreFragment;->mHandler:Landroid/os/Handler;

    new-instance v3, Lcom/google/android/setupwizard/account/RestoreStartWrapper$DelayedRestoreFragment$1;

    invoke-direct {v3, p0}, Lcom/google/android/setupwizard/account/RestoreStartWrapper$DelayedRestoreFragment$1;-><init>(Lcom/google/android/setupwizard/account/RestoreStartWrapper$DelayedRestoreFragment;)V

    sget-object v1, Lcom/google/android/setupwizard/account/RestoreStartWrapper$DelayedRestoreFragment;->DELAY_RESTORE_MILLIS:Lcom/google/android/setupwizard/util/GservicesValue;

    invoke-virtual {v1}, Lcom/google/android/setupwizard/util/GservicesValue;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    int-to-long v4, v1

    invoke-virtual {v2, v3, v4, v5}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 200
    return-void
.end method

.method public startRestoreImmediate()V
    .locals 5

    .prologue
    .line 203
    invoke-virtual {p0}, Lcom/google/android/setupwizard/account/RestoreStartWrapper$DelayedRestoreFragment;->getArguments()Landroid/os/Bundle;

    move-result-object v1

    const-string v4, "mfmRestoreToken"

    invoke-virtual {v1, v4}, Landroid/os/Bundle;->getLong(Ljava/lang/String;)J

    move-result-wide v2

    .line 204
    .local v2, "restoreToken":J
    invoke-static {v2, v3}, Lcom/google/android/setupwizard/util/BackupRestoreHelper;->startRestore(J)V

    .line 205
    invoke-virtual {p0}, Lcom/google/android/setupwizard/account/RestoreStartWrapper$DelayedRestoreFragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    check-cast v0, Lcom/google/android/setupwizard/account/RestoreStartWrapper;

    .line 206
    .local v0, "activity":Lcom/google/android/setupwizard/account/RestoreStartWrapper;
    # invokes: Lcom/google/android/setupwizard/account/RestoreStartWrapper;->nextScreen()V
    invoke-static {v0}, Lcom/google/android/setupwizard/account/RestoreStartWrapper;->access$000(Lcom/google/android/setupwizard/account/RestoreStartWrapper;)V

    .line 207
    invoke-virtual {v0}, Lcom/google/android/setupwizard/account/RestoreStartWrapper;->finish()V

    .line 208
    return-void
.end method
