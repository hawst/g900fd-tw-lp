.class Lcom/google/android/setupwizard/util/AndroidPolicy$PolicyDialog;
.super Landroid/app/AlertDialog;
.source "AndroidPolicy.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/setupwizard/util/AndroidPolicy;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "PolicyDialog"
.end annotation


# instance fields
.field private final mHelper:Lcom/google/android/common/GoogleWebContentHelper;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/google/android/setupwizard/util/AndroidPolicy;Landroid/os/Bundle;)V
    .locals 5
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "policy"    # Lcom/google/android/setupwizard/util/AndroidPolicy;
    .param p3, "savedState"    # Landroid/os/Bundle;

    .prologue
    .line 235
    invoke-direct {p0, p1}, Landroid/app/AlertDialog;-><init>(Landroid/content/Context;)V

    .line 236
    invoke-virtual {p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    .line 237
    .local v0, "cr":Landroid/content/ContentResolver;
    new-instance v2, Lcom/google/android/common/GoogleWebContentHelper;

    invoke-direct {v2, p1}, Lcom/google/android/common/GoogleWebContentHelper;-><init>(Landroid/content/Context;)V

    iput-object v2, p0, Lcom/google/android/setupwizard/util/AndroidPolicy$PolicyDialog;->mHelper:Lcom/google/android/common/GoogleWebContentHelper;

    .line 238
    if-eqz p3, :cond_2

    const-string v2, "savedWebView"

    invoke-virtual {p3, v2}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 239
    invoke-direct {p0}, Lcom/google/android/setupwizard/util/AndroidPolicy$PolicyDialog;->getWebView()Landroid/webkit/WebView;

    move-result-object v1

    .line 240
    .local v1, "webView":Landroid/webkit/WebView;
    if-eqz v1, :cond_0

    .line 241
    invoke-virtual {v1, p3}, Landroid/webkit/WebView;->restoreState(Landroid/os/Bundle;)Landroid/webkit/WebBackForwardList;

    .line 250
    .end local v1    # "webView":Landroid/webkit/WebView;
    :cond_0
    :goto_0
    invoke-virtual {p2}, Lcom/google/android/setupwizard/util/AndroidPolicy;->getTitleResId()I

    move-result v2

    invoke-virtual {p0, v2}, Lcom/google/android/setupwizard/util/AndroidPolicy$PolicyDialog;->setTitle(I)V

    .line 251
    const/4 v2, -0x1

    const v3, 0x7f07004d

    invoke-virtual {p1, v3}, Landroid/content/Context;->getText(I)Ljava/lang/CharSequence;

    move-result-object v3

    new-instance v4, Lcom/google/android/setupwizard/util/AndroidPolicy$PolicyDialog$1;

    invoke-direct {v4, p0}, Lcom/google/android/setupwizard/util/AndroidPolicy$PolicyDialog$1;-><init>(Lcom/google/android/setupwizard/util/AndroidPolicy$PolicyDialog;)V

    invoke-virtual {p0, v2, v3, v4}, Lcom/google/android/setupwizard/util/AndroidPolicy$PolicyDialog;->setButton(ILjava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)V

    .line 258
    iget-object v2, p0, Lcom/google/android/setupwizard/util/AndroidPolicy$PolicyDialog;->mHelper:Lcom/google/android/common/GoogleWebContentHelper;

    invoke-virtual {v2}, Lcom/google/android/common/GoogleWebContentHelper;->getLayout()Landroid/view/ViewGroup;

    move-result-object v2

    invoke-virtual {p0, v2}, Lcom/google/android/setupwizard/util/AndroidPolicy$PolicyDialog;->setView(Landroid/view/View;)V

    .line 262
    invoke-direct {p0}, Lcom/google/android/setupwizard/util/AndroidPolicy$PolicyDialog;->getWebView()Landroid/webkit/WebView;

    move-result-object v1

    .line 263
    .restart local v1    # "webView":Landroid/webkit/WebView;
    if-eqz v1, :cond_1

    .line 264
    new-instance v2, Lcom/google/android/setupwizard/util/AndroidPolicy$PolicyDialog$2;

    invoke-direct {v2, p0}, Lcom/google/android/setupwizard/util/AndroidPolicy$PolicyDialog$2;-><init>(Lcom/google/android/setupwizard/util/AndroidPolicy$PolicyDialog;)V

    invoke-virtual {v1, v2}, Landroid/webkit/WebView;->setOnLongClickListener(Landroid/view/View$OnLongClickListener;)V

    .line 271
    :cond_1
    return-void

    .line 244
    .end local v1    # "webView":Landroid/webkit/WebView;
    :cond_2
    iget-object v2, p0, Lcom/google/android/setupwizard/util/AndroidPolicy$PolicyDialog;->mHelper:Lcom/google/android/common/GoogleWebContentHelper;

    invoke-virtual {p2, v0, p1}, Lcom/google/android/setupwizard/util/AndroidPolicy;->getSecureUrl(Landroid/content/ContentResolver;Landroid/content/Context;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p2, v0, p1}, Lcom/google/android/setupwizard/util/AndroidPolicy;->getPrettyUrl(Landroid/content/ContentResolver;Landroid/content/Context;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, Lcom/google/android/common/GoogleWebContentHelper;->setUrls(Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/common/GoogleWebContentHelper;

    move-result-object v3

    invoke-static {}, Lcom/google/android/setupwizard/util/AndroidPolicy$PolicyDialog;->isWifiOnlyBuild()Z

    move-result v2

    if-eqz v2, :cond_3

    const v2, 0x7f07009c

    :goto_1
    invoke-virtual {p1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v3, v2}, Lcom/google/android/common/GoogleWebContentHelper;->setUnsuccessfulMessage(Ljava/lang/String;)Lcom/google/android/common/GoogleWebContentHelper;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/android/common/GoogleWebContentHelper;->loadUrl()Lcom/google/android/common/GoogleWebContentHelper;

    goto :goto_0

    :cond_3
    const v2, 0x7f07004f

    goto :goto_1
.end method

.method private getWebView()Landroid/webkit/WebView;
    .locals 4

    .prologue
    .line 293
    iget-object v3, p0, Lcom/google/android/setupwizard/util/AndroidPolicy$PolicyDialog;->mHelper:Lcom/google/android/common/GoogleWebContentHelper;

    invoke-virtual {v3}, Lcom/google/android/common/GoogleWebContentHelper;->getLayout()Landroid/view/ViewGroup;

    move-result-object v1

    .line 294
    .local v1, "layout":Landroid/view/ViewGroup;
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    invoke-virtual {v1}, Landroid/view/ViewGroup;->getChildCount()I

    move-result v3

    if-ge v0, v3, :cond_1

    .line 295
    invoke-virtual {v1, v0}, Landroid/view/ViewGroup;->getChildAt(I)Landroid/view/View;

    move-result-object v2

    .line 296
    .local v2, "view":Landroid/view/View;
    instance-of v3, v2, Landroid/webkit/WebView;

    if-eqz v3, :cond_0

    .line 297
    check-cast v2, Landroid/webkit/WebView;

    .line 300
    .end local v2    # "view":Landroid/view/View;
    :goto_1
    return-object v2

    .line 294
    .restart local v2    # "view":Landroid/view/View;
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 300
    .end local v2    # "view":Landroid/view/View;
    :cond_1
    const/4 v2, 0x0

    goto :goto_1
.end method

.method private static isWifiOnlyBuild()Z
    .locals 3

    .prologue
    .line 304
    const-string v0, "wifi-only"

    const-string v1, "ro.carrier"

    const/4 v2, 0x0

    invoke-static {v1, v2}, Landroid/os/SystemProperties;->get(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method


# virtual methods
.method public dispatchKeyEvent(Landroid/view/KeyEvent;)Z
    .locals 1
    .param p1, "event"    # Landroid/view/KeyEvent;

    .prologue
    .line 285
    iget-object v0, p0, Lcom/google/android/setupwizard/util/AndroidPolicy$PolicyDialog;->mHelper:Lcom/google/android/common/GoogleWebContentHelper;

    invoke-virtual {v0, p1}, Lcom/google/android/common/GoogleWebContentHelper;->handleKey(Landroid/view/KeyEvent;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 286
    const/4 v0, 0x1

    .line 288
    :goto_0
    return v0

    :cond_0
    invoke-super {p0, p1}, Landroid/app/AlertDialog;->dispatchKeyEvent(Landroid/view/KeyEvent;)Z

    move-result v0

    goto :goto_0
.end method

.method public saveState(Landroid/os/Bundle;)V
    .locals 3
    .param p1, "outState"    # Landroid/os/Bundle;

    .prologue
    .line 276
    invoke-direct {p0}, Lcom/google/android/setupwizard/util/AndroidPolicy$PolicyDialog;->getWebView()Landroid/webkit/WebView;

    move-result-object v0

    .line 277
    .local v0, "webView":Landroid/webkit/WebView;
    if-eqz v0, :cond_0

    .line 278
    const-string v1, "savedWebView"

    const/4 v2, 0x1

    invoke-virtual {p1, v1, v2}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 279
    invoke-virtual {v0, p1}, Landroid/webkit/WebView;->saveState(Landroid/os/Bundle;)Landroid/webkit/WebBackForwardList;

    .line 281
    :cond_0
    return-void
.end method
