.class public Lcom/google/android/setupwizard/util/AndroidPolicy;
.super Ljava/lang/Object;
.source "AndroidPolicy.java"

# interfaces
.implements Landroid/os/Parcelable;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/setupwizard/util/AndroidPolicy$PolicyDialogFragment;,
        Lcom/google/android/setupwizard/util/AndroidPolicy$PolicyDialog;
    }
.end annotation


# static fields
.field public static ANDROID_PRIVACY_POLICY:Lcom/google/android/setupwizard/util/AndroidPolicy;

.field public static BASIC_LEGAL:Lcom/google/android/setupwizard/util/AndroidPolicy;

.field public static CHROME_PRIVACY_POLICY:Lcom/google/android/setupwizard/util/AndroidPolicy;

.field public static CHROME_TERMS_OF_SERVICE:Lcom/google/android/setupwizard/util/AndroidPolicy;

.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/google/android/setupwizard/util/AndroidPolicy;",
            ">;"
        }
    .end annotation
.end field

.field public static GOOGLE_PLAY_TOS_POLICY:Lcom/google/android/setupwizard/util/AndroidPolicy;

.field public static GOOGLE_PLUS_PRIVACY_POLICY:Lcom/google/android/setupwizard/util/AndroidPolicy;

.field public static GOOGLE_PRIVACY_POLICY:Lcom/google/android/setupwizard/util/AndroidPolicy;

.field public static GOOGLE_PRIVACY_POLICY_GERMANY:Lcom/google/android/setupwizard/util/AndroidPolicy;

.field public static GOOGLE_TERMS_OF_SERVICE:Lcom/google/android/setupwizard/util/AndroidPolicy;

.field public static GOOGLE_TERMS_OF_SERVICE_GERMANY:Lcom/google/android/setupwizard/util/AndroidPolicy;


# instance fields
.field private final mFailUrl:Ljava/lang/String;

.field private final mFallbackUrl:Ljava/lang/String;

.field private final mGservicesProperty:Ljava/lang/String;

.field private final mTitleResourceId:I


# direct methods
.method static constructor <clinit>()V
    .locals 6

    .prologue
    const v5, 0x7f070097

    const v4, 0x7f070096

    .line 55
    new-instance v0, Lcom/google/android/setupwizard/util/AndroidPolicy;

    const-string v1, "setup_google_tos_url"

    const-string v2, "http://www.google.com/intl/%y_%z/mobile/android/google-tos.html"

    const-string v3, "file:///android_asset/html/en_us/google-tos.html"

    invoke-direct {v0, v4, v1, v2, v3}, Lcom/google/android/setupwizard/util/AndroidPolicy;-><init>(ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    sput-object v0, Lcom/google/android/setupwizard/util/AndroidPolicy;->GOOGLE_TERMS_OF_SERVICE:Lcom/google/android/setupwizard/util/AndroidPolicy;

    .line 67
    new-instance v0, Lcom/google/android/setupwizard/util/AndroidPolicy;

    const-string v1, "setup_google_tos_url_germany"

    const-string v2, "https://accounts.google.com/TOS?loc=DE&hl=de"

    const-string v3, ""

    invoke-direct {v0, v4, v1, v2, v3}, Lcom/google/android/setupwizard/util/AndroidPolicy;-><init>(ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    sput-object v0, Lcom/google/android/setupwizard/util/AndroidPolicy;->GOOGLE_TERMS_OF_SERVICE_GERMANY:Lcom/google/android/setupwizard/util/AndroidPolicy;

    .line 74
    new-instance v0, Lcom/google/android/setupwizard/util/AndroidPolicy;

    const-string v1, "setup_google_privacy_url"

    const-string v2, "http://www.google.com/intl/%s/mobile/android/google-privacy.html"

    const-string v3, "file:///android_asset/html/en_us/google-privacy.html"

    invoke-direct {v0, v5, v1, v2, v3}, Lcom/google/android/setupwizard/util/AndroidPolicy;-><init>(ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    sput-object v0, Lcom/google/android/setupwizard/util/AndroidPolicy;->GOOGLE_PRIVACY_POLICY:Lcom/google/android/setupwizard/util/AndroidPolicy;

    .line 80
    new-instance v0, Lcom/google/android/setupwizard/util/AndroidPolicy;

    const-string v1, "setup_google_privacy_url_germany"

    const-string v2, "https://accounts.google.com/TOS?loc=DE&privacy=true"

    const-string v3, ""

    invoke-direct {v0, v5, v1, v2, v3}, Lcom/google/android/setupwizard/util/AndroidPolicy;-><init>(ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    sput-object v0, Lcom/google/android/setupwizard/util/AndroidPolicy;->GOOGLE_PRIVACY_POLICY_GERMANY:Lcom/google/android/setupwizard/util/AndroidPolicy;

    .line 86
    new-instance v0, Lcom/google/android/setupwizard/util/AndroidPolicy;

    const v1, 0x7f070098

    const-string v2, "setup_google_privacy_url"

    const-string v3, "http://www.google.com/intl/%s/+/policy/"

    const-string v4, "file:///android_asset/html/en_us/google-privacy.html"

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/google/android/setupwizard/util/AndroidPolicy;-><init>(ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    sput-object v0, Lcom/google/android/setupwizard/util/AndroidPolicy;->GOOGLE_PLUS_PRIVACY_POLICY:Lcom/google/android/setupwizard/util/AndroidPolicy;

    .line 92
    new-instance v0, Lcom/google/android/setupwizard/util/AndroidPolicy;

    const v1, 0x7f070095

    const-string v2, "setup_android_privacy_url"

    const-string v3, "http://www.google.com/intl/%s/mobile/android/privacy.html"

    const-string v4, "file:///android_asset/html/en_us/android-privacy.html"

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/google/android/setupwizard/util/AndroidPolicy;-><init>(ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    sput-object v0, Lcom/google/android/setupwizard/util/AndroidPolicy;->ANDROID_PRIVACY_POLICY:Lcom/google/android/setupwizard/util/AndroidPolicy;

    .line 98
    new-instance v0, Lcom/google/android/setupwizard/util/AndroidPolicy;

    const v1, 0x7f070099

    const-string v2, "setup_google_play_tos_url"

    const-string v3, "http://play.google.com/intl/%y_%z/about/play-terms.html"

    const-string v4, "file:///android_asset/html/en_us/google-play-tos.html"

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/google/android/setupwizard/util/AndroidPolicy;-><init>(ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    sput-object v0, Lcom/google/android/setupwizard/util/AndroidPolicy;->GOOGLE_PLAY_TOS_POLICY:Lcom/google/android/setupwizard/util/AndroidPolicy;

    .line 104
    new-instance v0, Lcom/google/android/setupwizard/util/AndroidPolicy;

    const v1, 0x7f07009a

    const-string v2, "setup_chrome_tos_url"

    const-string v3, "http://www.google.com/chrome/intl/%y-%z/eula_text.html"

    const-string v4, "file:///android_asset/html/en_us/chrome-tos.html"

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/google/android/setupwizard/util/AndroidPolicy;-><init>(ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    sput-object v0, Lcom/google/android/setupwizard/util/AndroidPolicy;->CHROME_TERMS_OF_SERVICE:Lcom/google/android/setupwizard/util/AndroidPolicy;

    .line 110
    new-instance v0, Lcom/google/android/setupwizard/util/AndroidPolicy;

    const v1, 0x7f07009b

    const-string v2, "setup_chrome_privacy_url"

    const-string v3, "http://www.google.com/chrome/intl/%y-%z/privacy.html"

    const-string v4, "file:///android_asset/html/en_us/chrome-privacy.html"

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/google/android/setupwizard/util/AndroidPolicy;-><init>(ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    sput-object v0, Lcom/google/android/setupwizard/util/AndroidPolicy;->CHROME_PRIVACY_POLICY:Lcom/google/android/setupwizard/util/AndroidPolicy;

    .line 116
    new-instance v0, Lcom/google/android/setupwizard/util/AndroidPolicy;

    const v1, 0x7f07004e

    const-string v2, "google_setup:generic_tos_url"

    const-string v3, "http://www.google.com/intl/%y_%z/mobile/android/basic/phone-legal.html"

    const-string v4, "file:///android_asset/html/en_us/basic-phone-legal.html"

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/google/android/setupwizard/util/AndroidPolicy;-><init>(ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    sput-object v0, Lcom/google/android/setupwizard/util/AndroidPolicy;->BASIC_LEGAL:Lcom/google/android/setupwizard/util/AndroidPolicy;

    .line 350
    new-instance v0, Lcom/google/android/setupwizard/util/AndroidPolicy$1;

    invoke-direct {v0}, Lcom/google/android/setupwizard/util/AndroidPolicy$1;-><init>()V

    sput-object v0, Lcom/google/android/setupwizard/util/AndroidPolicy;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 0
    .param p1, "titleId"    # I
    .param p2, "gsProperty"    # Ljava/lang/String;
    .param p3, "fallback"    # Ljava/lang/String;
    .param p4, "fail"    # Ljava/lang/String;

    .prologue
    .line 135
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 136
    iput p1, p0, Lcom/google/android/setupwizard/util/AndroidPolicy;->mTitleResourceId:I

    .line 137
    iput-object p2, p0, Lcom/google/android/setupwizard/util/AndroidPolicy;->mGservicesProperty:Ljava/lang/String;

    .line 138
    iput-object p3, p0, Lcom/google/android/setupwizard/util/AndroidPolicy;->mFallbackUrl:Ljava/lang/String;

    .line 139
    iput-object p4, p0, Lcom/google/android/setupwizard/util/AndroidPolicy;->mFailUrl:Ljava/lang/String;

    .line 140
    return-void
.end method


# virtual methods
.method public describeContents()I
    .locals 1

    .prologue
    .line 339
    const/4 v0, 0x0

    return v0
.end method

.method public getPrettyUrl(Landroid/content/ContentResolver;Landroid/content/Context;)Ljava/lang/String;
    .locals 1
    .param p1, "cr"    # Landroid/content/ContentResolver;
    .param p2, "context"    # Landroid/content/Context;

    .prologue
    .line 226
    invoke-virtual {p0, p1, p2}, Lcom/google/android/setupwizard/util/AndroidPolicy;->getSecureUrl(Landroid/content/ContentResolver;Landroid/content/Context;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getSecureUrl(Landroid/content/ContentResolver;Landroid/content/Context;)Ljava/lang/String;
    .locals 10
    .param p1, "cr"    # Landroid/content/ContentResolver;
    .param p2, "context"    # Landroid/content/Context;

    .prologue
    const/4 v9, 0x2

    .line 147
    iget-object v6, p0, Lcom/google/android/setupwizard/util/AndroidPolicy;->mGservicesProperty:Ljava/lang/String;

    invoke-static {p1, v6}, Lcom/google/android/gsf/Gservices;->getString(Landroid/content/ContentResolver;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    .line 149
    .local v5, "url":Ljava/lang/String;
    invoke-static {v5}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v6

    if-eqz v6, :cond_1

    .line 150
    const-string v6, "AndroidPolicy"

    invoke-static {v6, v9}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v6

    if-eqz v6, :cond_0

    .line 151
    const-string v6, "AndroidPolicy"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v8, p0, Lcom/google/android/setupwizard/util/AndroidPolicy;->mGservicesProperty:Ljava/lang/String;

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, " not in gservices, using: "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    iget-object v8, p0, Lcom/google/android/setupwizard/util/AndroidPolicy;->mFallbackUrl:Ljava/lang/String;

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 154
    :cond_0
    iget-object v5, p0, Lcom/google/android/setupwizard/util/AndroidPolicy;->mFallbackUrl:Ljava/lang/String;

    .line 156
    :cond_1
    invoke-static {v5}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v6

    if-eqz v6, :cond_3

    .line 157
    const-string v6, "AndroidPolicy"

    invoke-static {v6, v9}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v6

    if-eqz v6, :cond_2

    .line 158
    const-string v6, "AndroidPolicy"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v8, p0, Lcom/google/android/setupwizard/util/AndroidPolicy;->mFallbackUrl:Ljava/lang/String;

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, " not set, using: "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    iget-object v8, p0, Lcom/google/android/setupwizard/util/AndroidPolicy;->mFailUrl:Ljava/lang/String;

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 160
    :cond_2
    iget-object v5, p0, Lcom/google/android/setupwizard/util/AndroidPolicy;->mFailUrl:Ljava/lang/String;

    .line 164
    :cond_3
    const-string v6, "%m"

    invoke-virtual {v5, v6}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v6

    if-eqz v6, :cond_4

    .line 166
    :try_start_0
    new-instance v0, Landroid/content/res/Configuration;

    invoke-direct {v0}, Landroid/content/res/Configuration;-><init>()V

    .line 167
    .local v0, "config":Landroid/content/res/Configuration;
    invoke-static {p1, v0}, Landroid/provider/Settings$System;->getConfiguration(Landroid/content/ContentResolver;Landroid/content/res/Configuration;)V

    .line 168
    iget v6, v0, Landroid/content/res/Configuration;->mcc:I

    if-eqz v6, :cond_9

    .line 169
    const-string v6, "%m"

    iget v7, v0, Landroid/content/res/Configuration;->mcc:I

    invoke-static {v7}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v5, v6, v7}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_1

    move-result-object v5

    .line 179
    .end local v0    # "config":Landroid/content/res/Configuration;
    :cond_4
    :goto_0
    const-string v6, "%s"

    invoke-virtual {v5, v6}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v6

    if-eqz v6, :cond_5

    .line 180
    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v1

    .line 181
    .local v1, "locale":Ljava/util/Locale;
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1}, Ljava/util/Locale;->getLanguage()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, "_"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v1}, Ljava/util/Locale;->getCountry()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    .line 182
    .local v4, "tmp":Ljava/lang/String;
    const-string v6, "%s"

    invoke-virtual {v5, v6, v4}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v5

    .line 187
    .end local v1    # "locale":Ljava/util/Locale;
    .end local v4    # "tmp":Ljava/lang/String;
    :cond_5
    const-string v6, "%y"

    invoke-virtual {v5, v6}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v6

    if-eqz v6, :cond_6

    .line 188
    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v1

    .line 189
    .restart local v1    # "locale":Ljava/util/Locale;
    const-string v6, "%y"

    invoke-virtual {v1}, Ljava/util/Locale;->getLanguage()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v5, v6, v7}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v5

    .line 195
    .end local v1    # "locale":Ljava/util/Locale;
    :cond_6
    const-string v6, "%z"

    invoke-virtual {v5, v6}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v6

    if-eqz v6, :cond_8

    .line 197
    :try_start_1
    const-string v6, "phone"

    invoke-virtual {p2, v6}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/telephony/TelephonyManager;

    .line 200
    .local v3, "telephony":Landroid/telephony/TelephonyManager;
    new-instance v0, Landroid/content/res/Configuration;

    invoke-direct {v0}, Landroid/content/res/Configuration;-><init>()V

    .line 201
    .restart local v0    # "config":Landroid/content/res/Configuration;
    invoke-static {p1, v0}, Landroid/provider/Settings$System;->getConfiguration(Landroid/content/ContentResolver;Landroid/content/res/Configuration;)V

    .line 203
    if-eqz v3, :cond_a

    iget v6, v0, Landroid/content/res/Configuration;->mcc:I

    if-eqz v6, :cond_a

    .line 204
    invoke-virtual {v3}, Landroid/telephony/TelephonyManager;->getSimCountryIso()Ljava/lang/String;

    move-result-object v2

    .line 205
    .local v2, "simCountryIso":Ljava/lang/String;
    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v6

    if-eqz v6, :cond_7

    .line 206
    const-string v2, "us"

    .line 208
    :cond_7
    const-string v6, "%z"

    invoke-virtual {v5, v6, v2}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    move-result-object v5

    .line 222
    .end local v0    # "config":Landroid/content/res/Configuration;
    .end local v2    # "simCountryIso":Ljava/lang/String;
    .end local v3    # "telephony":Landroid/telephony/TelephonyManager;
    :cond_8
    :goto_1
    return-object v5

    .line 173
    .restart local v0    # "config":Landroid/content/res/Configuration;
    :cond_9
    :try_start_2
    const-string v6, "%m"

    const-string v7, "%s"

    invoke-virtual {v5, v6, v7}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_1

    move-result-object v5

    goto/16 :goto_0

    .line 213
    .restart local v3    # "telephony":Landroid/telephony/TelephonyManager;
    :cond_a
    :try_start_3
    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v1

    .line 214
    .restart local v1    # "locale":Ljava/util/Locale;
    const-string v6, "%z"

    invoke-virtual {v1}, Ljava/util/Locale;->getCountry()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v5, v6, v7}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;
    :try_end_3
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_0

    move-result-object v5

    goto :goto_1

    .line 217
    .end local v0    # "config":Landroid/content/res/Configuration;
    .end local v1    # "locale":Ljava/util/Locale;
    .end local v3    # "telephony":Landroid/telephony/TelephonyManager;
    :catch_0
    move-exception v6

    goto :goto_1

    .line 175
    :catch_1
    move-exception v6

    goto/16 :goto_0
.end method

.method public getTitleResId()I
    .locals 1

    .prologue
    .line 143
    iget v0, p0, Lcom/google/android/setupwizard/util/AndroidPolicy;->mTitleResourceId:I

    return v0
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 1
    .param p1, "dest"    # Landroid/os/Parcel;
    .param p2, "flags"    # I

    .prologue
    .line 344
    iget v0, p0, Lcom/google/android/setupwizard/util/AndroidPolicy;->mTitleResourceId:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 345
    iget-object v0, p0, Lcom/google/android/setupwizard/util/AndroidPolicy;->mGservicesProperty:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 346
    iget-object v0, p0, Lcom/google/android/setupwizard/util/AndroidPolicy;->mFallbackUrl:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 347
    iget-object v0, p0, Lcom/google/android/setupwizard/util/AndroidPolicy;->mFailUrl:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 348
    return-void
.end method
