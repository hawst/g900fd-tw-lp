.class Lcom/google/android/setupwizard/account/RestoreService$1;
.super Landroid/app/backup/RestoreObserver;
.source "RestoreService.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/setupwizard/account/RestoreService;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/setupwizard/account/RestoreService;


# direct methods
.method constructor <init>(Lcom/google/android/setupwizard/account/RestoreService;)V
    .locals 0

    .prologue
    .line 181
    iput-object p1, p0, Lcom/google/android/setupwizard/account/RestoreService$1;->this$0:Lcom/google/android/setupwizard/account/RestoreService;

    invoke-direct {p0}, Landroid/app/backup/RestoreObserver;-><init>()V

    return-void
.end method


# virtual methods
.method public onUpdate(ILjava/lang/String;)V
    .locals 4
    .param p1, "nowBeingRestored"    # I
    .param p2, "currentPackage"    # Ljava/lang/String;

    .prologue
    .line 198
    # getter for: Lcom/google/android/setupwizard/account/RestoreService;->LOCAL_LOGV:Z
    invoke-static {}, Lcom/google/android/setupwizard/account/RestoreService;->access$000()Z

    move-result v1

    if-eqz v1, :cond_0

    const-string v1, "SetupWizard.RestoreService"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "onUpdate: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " = "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 199
    :cond_0
    new-instance v0, Landroid/content/Intent;

    const-string v1, "com.google.android.setupwizard.RESTORE_UPDATE_PACKAGE"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 200
    .local v0, "intent":Landroid/content/Intent;
    const-string v1, "restore_package_count"

    iget-object v2, p0, Lcom/google/android/setupwizard/account/RestoreService$1;->this$0:Lcom/google/android/setupwizard/account/RestoreService;

    # getter for: Lcom/google/android/setupwizard/account/RestoreService;->mNumPackages:I
    invoke-static {v2}, Lcom/google/android/setupwizard/account/RestoreService;->access$100(Lcom/google/android/setupwizard/account/RestoreService;)I

    move-result v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 201
    const-string v1, "restoring_package_index"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 202
    const-string v1, "restoring_package_name"

    invoke-virtual {v0, v1, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 203
    iget-object v1, p0, Lcom/google/android/setupwizard/account/RestoreService$1;->this$0:Lcom/google/android/setupwizard/account/RestoreService;

    invoke-static {v1}, Landroid/support/v4/content/LocalBroadcastManager;->getInstance(Landroid/content/Context;)Landroid/support/v4/content/LocalBroadcastManager;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/support/v4/content/LocalBroadcastManager;->sendBroadcast(Landroid/content/Intent;)Z

    .line 204
    return-void
.end method

.method public restoreFinished(I)V
    .locals 3
    .param p1, "error"    # I

    .prologue
    .line 208
    # getter for: Lcom/google/android/setupwizard/account/RestoreService;->LOCAL_LOGV:Z
    invoke-static {}, Lcom/google/android/setupwizard/account/RestoreService;->access$000()Z

    move-result v0

    if-eqz v0, :cond_0

    const-string v0, "SetupWizard.RestoreService"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "restoreFinished: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 209
    :cond_0
    iget-object v0, p0, Lcom/google/android/setupwizard/account/RestoreService$1;->this$0:Lcom/google/android/setupwizard/account/RestoreService;

    # setter for: Lcom/google/android/setupwizard/account/RestoreService;->mError:I
    invoke-static {v0, p1}, Lcom/google/android/setupwizard/account/RestoreService;->access$302(Lcom/google/android/setupwizard/account/RestoreService;I)I

    .line 210
    iget-object v0, p0, Lcom/google/android/setupwizard/account/RestoreService$1;->this$0:Lcom/google/android/setupwizard/account/RestoreService;

    # getter for: Lcom/google/android/setupwizard/account/RestoreService;->mRestoreSession:Landroid/app/backup/RestoreSession;
    invoke-static {v0}, Lcom/google/android/setupwizard/account/RestoreService;->access$400(Lcom/google/android/setupwizard/account/RestoreService;)Landroid/app/backup/RestoreSession;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 211
    iget-object v0, p0, Lcom/google/android/setupwizard/account/RestoreService$1;->this$0:Lcom/google/android/setupwizard/account/RestoreService;

    # getter for: Lcom/google/android/setupwizard/account/RestoreService;->mRestoreSession:Landroid/app/backup/RestoreSession;
    invoke-static {v0}, Lcom/google/android/setupwizard/account/RestoreService;->access$400(Lcom/google/android/setupwizard/account/RestoreService;)Landroid/app/backup/RestoreSession;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/backup/RestoreSession;->endRestoreSession()V

    .line 212
    iget-object v0, p0, Lcom/google/android/setupwizard/account/RestoreService$1;->this$0:Lcom/google/android/setupwizard/account/RestoreService;

    invoke-virtual {v0}, Lcom/google/android/setupwizard/account/RestoreService;->stopSelf()V

    .line 214
    :cond_1
    return-void
.end method

.method public restoreSetsAvailable([Landroid/app/backup/RestoreSet;)V
    .locals 3
    .param p1, "result"    # [Landroid/app/backup/RestoreSet;

    .prologue
    .line 184
    # getter for: Lcom/google/android/setupwizard/account/RestoreService;->LOCAL_LOGV:Z
    invoke-static {}, Lcom/google/android/setupwizard/account/RestoreService;->access$000()Z

    move-result v0

    if-eqz v0, :cond_0

    const-string v0, "SetupWizard.RestoreService"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "restoreSetsAvailable: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 185
    :cond_0
    iget-object v0, p0, Lcom/google/android/setupwizard/account/RestoreService$1;->this$0:Lcom/google/android/setupwizard/account/RestoreService;

    invoke-virtual {v0, p1}, Lcom/google/android/setupwizard/account/RestoreService;->performRestore([Landroid/app/backup/RestoreSet;)Z

    .line 186
    return-void
.end method

.method public restoreStarting(I)V
    .locals 3
    .param p1, "numPackages"    # I

    .prologue
    .line 190
    # getter for: Lcom/google/android/setupwizard/account/RestoreService;->LOCAL_LOGV:Z
    invoke-static {}, Lcom/google/android/setupwizard/account/RestoreService;->access$000()Z

    move-result v0

    if-eqz v0, :cond_0

    const-string v0, "SetupWizard.RestoreService"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "restoreStarting: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " packages"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 191
    :cond_0
    iget-object v0, p0, Lcom/google/android/setupwizard/account/RestoreService$1;->this$0:Lcom/google/android/setupwizard/account/RestoreService;

    # setter for: Lcom/google/android/setupwizard/account/RestoreService;->mNumPackages:I
    invoke-static {v0, p1}, Lcom/google/android/setupwizard/account/RestoreService;->access$102(Lcom/google/android/setupwizard/account/RestoreService;I)I

    .line 192
    iget-object v0, p0, Lcom/google/android/setupwizard/account/RestoreService$1;->this$0:Lcom/google/android/setupwizard/account/RestoreService;

    const-string v1, "started"

    # invokes: Lcom/google/android/setupwizard/account/RestoreService;->setState(Ljava/lang/String;)V
    invoke-static {v0, v1}, Lcom/google/android/setupwizard/account/RestoreService;->access$200(Lcom/google/android/setupwizard/account/RestoreService;Ljava/lang/String;)V

    .line 193
    return-void
.end method
