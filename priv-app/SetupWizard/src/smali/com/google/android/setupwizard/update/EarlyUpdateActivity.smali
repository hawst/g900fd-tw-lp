.class public Lcom/google/android/setupwizard/update/EarlyUpdateActivity;
.super Lcom/google/android/setupwizard/BaseActivity;
.source "EarlyUpdateActivity.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/setupwizard/update/EarlyUpdateActivity$EarlyUpdateFragment;
    }
.end annotation


# instance fields
.field private mFinishResult:I

.field private final mHandler:Landroid/os/Handler;

.field private mStatusText:Landroid/widget/TextView;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 38
    invoke-direct {p0}, Lcom/google/android/setupwizard/BaseActivity;-><init>()V

    .line 60
    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    iput-object v0, p0, Lcom/google/android/setupwizard/update/EarlyUpdateActivity;->mHandler:Landroid/os/Handler;

    .line 62
    const v0, 0x7fffffff

    iput v0, p0, Lcom/google/android/setupwizard/update/EarlyUpdateActivity;->mFinishResult:I

    .line 137
    return-void
.end method

.method static synthetic access$000(Lcom/google/android/setupwizard/update/EarlyUpdateActivity;I)V
    .locals 0
    .param p0, "x0"    # Lcom/google/android/setupwizard/update/EarlyUpdateActivity;
    .param p1, "x1"    # I

    .prologue
    .line 38
    invoke-virtual {p0, p1}, Lcom/google/android/setupwizard/update/EarlyUpdateActivity;->finishAction(I)V

    return-void
.end method

.method static synthetic access$700(Lcom/google/android/setupwizard/update/EarlyUpdateActivity;I)V
    .locals 0
    .param p0, "x0"    # Lcom/google/android/setupwizard/update/EarlyUpdateActivity;
    .param p1, "x1"    # I

    .prologue
    .line 38
    invoke-direct {p0, p1}, Lcom/google/android/setupwizard/update/EarlyUpdateActivity;->delayedFinish(I)V

    return-void
.end method

.method private delayedFinish(I)V
    .locals 4
    .param p1, "result"    # I

    .prologue
    .line 127
    iput p1, p0, Lcom/google/android/setupwizard/update/EarlyUpdateActivity;->mFinishResult:I

    .line 128
    iget-object v0, p0, Lcom/google/android/setupwizard/update/EarlyUpdateActivity;->mHandler:Landroid/os/Handler;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacksAndMessages(Ljava/lang/Object;)V

    .line 129
    iget-object v0, p0, Lcom/google/android/setupwizard/update/EarlyUpdateActivity;->mHandler:Landroid/os/Handler;

    new-instance v1, Lcom/google/android/setupwizard/update/EarlyUpdateActivity$1;

    invoke-direct {v1, p0, p1}, Lcom/google/android/setupwizard/update/EarlyUpdateActivity$1;-><init>(Lcom/google/android/setupwizard/update/EarlyUpdateActivity;I)V

    const-wide/16 v2, 0x3e8

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 135
    return-void
.end method


# virtual methods
.method protected getPreviousTransition()I
    .locals 1

    .prologue
    .line 92
    const/4 v0, 0x2

    return v0
.end method

.method public onBackPressed()V
    .locals 2

    .prologue
    .line 108
    const v0, 0x7fffffff

    iput v0, p0, Lcom/google/android/setupwizard/update/EarlyUpdateActivity;->mFinishResult:I

    .line 109
    iget-object v0, p0, Lcom/google/android/setupwizard/update/EarlyUpdateActivity;->mHandler:Landroid/os/Handler;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacksAndMessages(Ljava/lang/Object;)V

    .line 110
    invoke-super {p0}, Lcom/google/android/setupwizard/BaseActivity;->onBackPressed()V

    .line 111
    return-void
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 2
    .param p1, "icicle"    # Landroid/os/Bundle;

    .prologue
    .line 68
    invoke-super {p0, p1}, Lcom/google/android/setupwizard/BaseActivity;->onCreate(Landroid/os/Bundle;)V

    .line 70
    const v0, 0x7f030006

    const v1, 0x7f030023

    invoke-virtual {p0, v0, v1}, Lcom/google/android/setupwizard/update/EarlyUpdateActivity;->setTemplateContent(II)V

    .line 71
    const v0, 0x7f07008f

    invoke-virtual {p0, v0}, Lcom/google/android/setupwizard/update/EarlyUpdateActivity;->setHeaderText(I)V

    .line 72
    const v0, 0x7f0e0029

    invoke-virtual {p0, v0}, Lcom/google/android/setupwizard/update/EarlyUpdateActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/google/android/setupwizard/update/EarlyUpdateActivity;->mStatusText:Landroid/widget/TextView;

    .line 74
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/google/android/setupwizard/update/EarlyUpdateActivity;->setNextAllowed(Z)V

    .line 76
    invoke-virtual {p0}, Lcom/google/android/setupwizard/update/EarlyUpdateActivity;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/setupwizard/update/EarlyUpdateActivity$EarlyUpdateFragment;->attach(Landroid/app/FragmentManager;)V

    .line 77
    return-void
.end method

.method public onNavigationBarCreated(Lcom/android/setupwizard/navigationbar/SetupWizardNavBar;)V
    .locals 2
    .param p1, "bar"    # Lcom/android/setupwizard/navigationbar/SetupWizardNavBar;

    .prologue
    .line 97
    invoke-super {p0, p1}, Lcom/google/android/setupwizard/BaseActivity;->onNavigationBarCreated(Lcom/android/setupwizard/navigationbar/SetupWizardNavBar;)V

    .line 98
    invoke-virtual {p1}, Lcom/android/setupwizard/navigationbar/SetupWizardNavBar;->getNextButton()Landroid/widget/Button;

    move-result-object v0

    const v1, 0x7f070046

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setText(I)V

    .line 99
    return-void
.end method

.method protected onPause()V
    .locals 2

    .prologue
    .line 81
    iget v0, p0, Lcom/google/android/setupwizard/update/EarlyUpdateActivity;->mFinishResult:I

    const v1, 0x7fffffff

    if-eq v0, v1, :cond_0

    .line 84
    iget v0, p0, Lcom/google/android/setupwizard/update/EarlyUpdateActivity;->mFinishResult:I

    invoke-virtual {p0, v0}, Lcom/google/android/setupwizard/update/EarlyUpdateActivity;->finishAction(I)V

    .line 86
    :cond_0
    iget-object v0, p0, Lcom/google/android/setupwizard/update/EarlyUpdateActivity;->mHandler:Landroid/os/Handler;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacksAndMessages(Ljava/lang/Object;)V

    .line 87
    invoke-super {p0}, Lcom/google/android/setupwizard/BaseActivity;->onPause()V

    .line 88
    return-void
.end method

.method protected start()V
    .locals 1

    .prologue
    .line 103
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/google/android/setupwizard/update/EarlyUpdateActivity;->finishAction(I)V

    .line 104
    return-void
.end method

.method public updateState(ILandroid/os/Bundle;)V
    .locals 6
    .param p1, "state"    # I
    .param p2, "packageInfo"    # Landroid/os/Bundle;

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x1

    .line 114
    if-eqz p1, :cond_0

    const/4 v3, 0x3

    if-eq p1, v3, :cond_0

    if-nez p2, :cond_2

    .line 115
    :cond_0
    iget-object v1, p0, Lcom/google/android/setupwizard/update/EarlyUpdateActivity;->mStatusText:Landroid/widget/TextView;

    const v2, 0x7f070090

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(I)V

    .line 124
    :cond_1
    :goto_0
    return-void

    .line 116
    :cond_2
    if-ne p1, v1, :cond_4

    .line 117
    const-string v3, "title"

    invoke-virtual {p2, v3}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 118
    .local v0, "title":Ljava/lang/String;
    iget-object v3, p0, Lcom/google/android/setupwizard/update/EarlyUpdateActivity;->mStatusText:Landroid/widget/TextView;

    const v4, 0x7f070091

    new-array v5, v1, [Ljava/lang/Object;

    aput-object v0, v5, v2

    invoke-virtual {p0, v4, v5}, Lcom/google/android/setupwizard/update/EarlyUpdateActivity;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 119
    const-string v3, "critical"

    invoke-virtual {p2, v3}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v3

    if-nez v3, :cond_3

    :goto_1
    invoke-virtual {p0, v1}, Lcom/google/android/setupwizard/update/EarlyUpdateActivity;->setNextAllowed(Z)V

    goto :goto_0

    :cond_3
    move v1, v2

    goto :goto_1

    .line 120
    .end local v0    # "title":Ljava/lang/String;
    :cond_4
    const/4 v3, 0x2

    if-ne p1, v3, :cond_1

    .line 121
    const-string v3, "title"

    invoke-virtual {p2, v3}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 122
    .restart local v0    # "title":Ljava/lang/String;
    iget-object v3, p0, Lcom/google/android/setupwizard/update/EarlyUpdateActivity;->mStatusText:Landroid/widget/TextView;

    const v4, 0x7f070092

    new-array v1, v1, [Ljava/lang/Object;

    aput-object v0, v1, v2

    invoke-virtual {p0, v4, v1}, Lcom/google/android/setupwizard/update/EarlyUpdateActivity;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v3, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0
.end method
