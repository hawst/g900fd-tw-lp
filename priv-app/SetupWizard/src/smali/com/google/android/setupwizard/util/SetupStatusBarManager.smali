.class public Lcom/google/android/setupwizard/util/SetupStatusBarManager;
.super Ljava/lang/Object;
.source "SetupStatusBarManager.java"


# static fields
.field private static sStatusBarManager:Landroid/app/StatusBarManager;


# direct methods
.method private static disableNotifications(Landroid/content/Context;)V
    .locals 3
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 66
    sget-object v1, Lcom/google/android/setupwizard/util/SetupStatusBarManager;->sStatusBarManager:Landroid/app/StatusBarManager;

    if-nez v1, :cond_0

    .line 67
    const-string v1, "statusbar"

    invoke-virtual {p0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/app/StatusBarManager;

    sput-object v1, Lcom/google/android/setupwizard/util/SetupStatusBarManager;->sStatusBarManager:Landroid/app/StatusBarManager;

    .line 70
    :cond_0
    sget-object v1, Lcom/google/android/setupwizard/util/SetupStatusBarManager;->sStatusBarManager:Landroid/app/StatusBarManager;

    if-eqz v1, :cond_1

    .line 71
    const-string v1, "ro.setupwizard.stat_bar_disable"

    const/high16 v2, 0x3a50000

    invoke-static {v1, v2}, Landroid/os/SystemProperties;->getInt(Ljava/lang/String;I)I

    move-result v0

    .line 73
    .local v0, "disableFlags":I
    sget-object v1, Lcom/google/android/setupwizard/util/SetupStatusBarManager;->sStatusBarManager:Landroid/app/StatusBarManager;

    invoke-virtual {v1, v0}, Landroid/app/StatusBarManager;->disable(I)V

    .line 77
    .end local v0    # "disableFlags":I
    :goto_0
    return-void

    .line 75
    :cond_1
    const-string v1, "SetupWizard"

    const-string v2, "Skip disabling notfications - could not get StatusBarManager"

    invoke-static {v1, v2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method private static enableNotifications()V
    .locals 2

    .prologue
    .line 54
    sget-object v0, Lcom/google/android/setupwizard/util/SetupStatusBarManager;->sStatusBarManager:Landroid/app/StatusBarManager;

    if-eqz v0, :cond_0

    .line 55
    sget-object v0, Lcom/google/android/setupwizard/util/SetupStatusBarManager;->sStatusBarManager:Landroid/app/StatusBarManager;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/app/StatusBarManager;->disable(I)V

    .line 56
    const/4 v0, 0x0

    sput-object v0, Lcom/google/android/setupwizard/util/SetupStatusBarManager;->sStatusBarManager:Landroid/app/StatusBarManager;

    .line 60
    :goto_0
    return-void

    .line 58
    :cond_0
    const-string v0, "SetupWizard"

    const-string v1, "Skip enabling notfications - sStatusBarManager is null"

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method public static onSetupComplete(Landroid/content/Context;)V
    .locals 0
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 45
    invoke-static {}, Lcom/google/android/setupwizard/util/SetupStatusBarManager;->enableNotifications()V

    .line 46
    return-void
.end method

.method public static onSetupStart(Landroid/content/Context;Z)V
    .locals 0
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "isFirstRun"    # Z

    .prologue
    .line 39
    if-eqz p1, :cond_0

    .line 40
    invoke-static {p0}, Lcom/google/android/setupwizard/util/SetupStatusBarManager;->disableNotifications(Landroid/content/Context;)V

    .line 42
    :cond_0
    return-void
.end method
