.class Lcom/google/android/setupwizard/carrier/ActivationActivity$3;
.super Landroid/telephony/PhoneStateListener;
.source "ActivationActivity.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/setupwizard/carrier/ActivationActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/setupwizard/carrier/ActivationActivity;


# direct methods
.method constructor <init>(Lcom/google/android/setupwizard/carrier/ActivationActivity;)V
    .locals 0

    .prologue
    .line 219
    iput-object p1, p0, Lcom/google/android/setupwizard/carrier/ActivationActivity$3;->this$0:Lcom/google/android/setupwizard/carrier/ActivationActivity;

    invoke-direct {p0}, Landroid/telephony/PhoneStateListener;-><init>()V

    return-void
.end method


# virtual methods
.method public onOtaspChanged(I)V
    .locals 3
    .param p1, "otaspMode"    # I

    .prologue
    .line 243
    const-string v0, "ActivationActivity"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "mPhoneStateListener.onOtaspChanged("

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ")"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " mState="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/setupwizard/carrier/ActivationActivity$3;->this$0:Lcom/google/android/setupwizard/carrier/ActivationActivity;

    # getter for: Lcom/google/android/setupwizard/carrier/ActivationActivity;->mState:I
    invoke-static {v2}, Lcom/google/android/setupwizard/carrier/ActivationActivity;->access$100(Lcom/google/android/setupwizard/carrier/ActivationActivity;)I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 246
    iget-object v0, p0, Lcom/google/android/setupwizard/carrier/ActivationActivity$3;->this$0:Lcom/google/android/setupwizard/carrier/ActivationActivity;

    # getter for: Lcom/google/android/setupwizard/carrier/ActivationActivity;->mGenericMode:Z
    invoke-static {v0}, Lcom/google/android/setupwizard/carrier/ActivationActivity;->access$800(Lcom/google/android/setupwizard/carrier/ActivationActivity;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 247
    iget-object v0, p0, Lcom/google/android/setupwizard/carrier/ActivationActivity$3;->this$0:Lcom/google/android/setupwizard/carrier/ActivationActivity;

    # invokes: Lcom/google/android/setupwizard/carrier/ActivationActivity;->maybeEndGenericActivation()V
    invoke-static {v0}, Lcom/google/android/setupwizard/carrier/ActivationActivity;->access$900(Lcom/google/android/setupwizard/carrier/ActivationActivity;)V

    .line 251
    :goto_0
    return-void

    .line 249
    :cond_0
    iget-object v0, p0, Lcom/google/android/setupwizard/carrier/ActivationActivity$3;->this$0:Lcom/google/android/setupwizard/carrier/ActivationActivity;

    # invokes: Lcom/google/android/setupwizard/carrier/ActivationActivity;->maybeActivateOrExit()V
    invoke-static {v0}, Lcom/google/android/setupwizard/carrier/ActivationActivity;->access$700(Lcom/google/android/setupwizard/carrier/ActivationActivity;)V

    goto :goto_0
.end method

.method public onServiceStateChanged(Landroid/telephony/ServiceState;)V
    .locals 3
    .param p1, "state"    # Landroid/telephony/ServiceState;

    .prologue
    .line 233
    const-string v0, "ActivationActivity"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "mPhoneStateListener.onServiceStateChanged("

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ")"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 235
    iget-object v0, p0, Lcom/google/android/setupwizard/carrier/ActivationActivity$3;->this$0:Lcom/google/android/setupwizard/carrier/ActivationActivity;

    # setter for: Lcom/google/android/setupwizard/carrier/ActivationActivity;->mServiceState:Landroid/telephony/ServiceState;
    invoke-static {v0, p1}, Lcom/google/android/setupwizard/carrier/ActivationActivity;->access$602(Lcom/google/android/setupwizard/carrier/ActivationActivity;Landroid/telephony/ServiceState;)Landroid/telephony/ServiceState;

    .line 236
    iget-object v0, p0, Lcom/google/android/setupwizard/carrier/ActivationActivity$3;->this$0:Lcom/google/android/setupwizard/carrier/ActivationActivity;

    # invokes: Lcom/google/android/setupwizard/carrier/ActivationActivity;->updateSignalStrength()V
    invoke-static {v0}, Lcom/google/android/setupwizard/carrier/ActivationActivity;->access$500(Lcom/google/android/setupwizard/carrier/ActivationActivity;)V

    .line 237
    iget-object v0, p0, Lcom/google/android/setupwizard/carrier/ActivationActivity$3;->this$0:Lcom/google/android/setupwizard/carrier/ActivationActivity;

    # invokes: Lcom/google/android/setupwizard/carrier/ActivationActivity;->maybeActivateOrExit()V
    invoke-static {v0}, Lcom/google/android/setupwizard/carrier/ActivationActivity;->access$700(Lcom/google/android/setupwizard/carrier/ActivationActivity;)V

    .line 238
    return-void
.end method

.method public onSignalStrengthsChanged(Landroid/telephony/SignalStrength;)V
    .locals 3
    .param p1, "signalStrength"    # Landroid/telephony/SignalStrength;

    .prologue
    .line 223
    const-string v0, "ActivationActivity"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "mPhoneStateListener.onSignalStrengthsChanged("

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ")"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 226
    iget-object v0, p0, Lcom/google/android/setupwizard/carrier/ActivationActivity$3;->this$0:Lcom/google/android/setupwizard/carrier/ActivationActivity;

    # setter for: Lcom/google/android/setupwizard/carrier/ActivationActivity;->mSignalStrength:Landroid/telephony/SignalStrength;
    invoke-static {v0, p1}, Lcom/google/android/setupwizard/carrier/ActivationActivity;->access$402(Lcom/google/android/setupwizard/carrier/ActivationActivity;Landroid/telephony/SignalStrength;)Landroid/telephony/SignalStrength;

    .line 227
    iget-object v0, p0, Lcom/google/android/setupwizard/carrier/ActivationActivity$3;->this$0:Lcom/google/android/setupwizard/carrier/ActivationActivity;

    # invokes: Lcom/google/android/setupwizard/carrier/ActivationActivity;->updateSignalStrength()V
    invoke-static {v0}, Lcom/google/android/setupwizard/carrier/ActivationActivity;->access$500(Lcom/google/android/setupwizard/carrier/ActivationActivity;)V

    .line 228
    return-void
.end method
