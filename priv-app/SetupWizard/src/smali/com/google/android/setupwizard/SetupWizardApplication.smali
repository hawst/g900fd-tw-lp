.class public Lcom/google/android/setupwizard/SetupWizardApplication;
.super Landroid/app/Application;
.source "SetupWizardApplication.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 17
    invoke-direct {p0}, Landroid/app/Application;-><init>()V

    return-void
.end method


# virtual methods
.method public onCreate()V
    .locals 2

    .prologue
    .line 20
    invoke-super {p0}, Landroid/app/Application;->onCreate()V

    .line 22
    invoke-virtual {p0}, Lcom/google/android/setupwizard/SetupWizardApplication;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    .line 24
    .local v0, "context":Landroid/content/Context;
    invoke-static {v0}, Lcom/google/android/setupwizard/util/GservicesValue;->init(Landroid/content/Context;)V

    .line 25
    invoke-static {v0}, Lcom/google/android/setupwizard/carrier/PhoneMonitor;->initInstance(Landroid/content/Context;)V

    .line 26
    invoke-static {v0}, Lcom/google/android/setupwizard/network/NetworkMonitor;->initInstance(Landroid/content/Context;)V

    .line 27
    invoke-static {v0}, Lcom/google/android/setupwizard/util/BackupRestoreHelper;->initInstance(Landroid/content/Context;)V

    .line 30
    invoke-static {}, Landroid/os/UserHandle;->myUserId()I

    move-result v1

    if-nez v1, :cond_0

    .line 31
    invoke-static {v0}, Lcom/google/android/setupwizard/time/DateTimeMonitor;->initInstance(Landroid/content/Context;)V

    .line 33
    :cond_0
    return-void
.end method
