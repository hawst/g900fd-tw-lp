.class public Lcom/google/android/setupwizard/account/StartVpaActivity;
.super Lcom/google/android/setupwizard/BaseActivity;
.source "StartVpaActivity.java"

# interfaces
.implements Lcom/google/android/setupwizard/util/GooglePlaySetupHelper$GooglePlayConnectionListener;


# instance fields
.field private mPlayHelper:Lcom/google/android/setupwizard/util/GooglePlaySetupHelper;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 29
    invoke-direct {p0}, Lcom/google/android/setupwizard/BaseActivity;-><init>()V

    return-void
.end method


# virtual methods
.method protected getPreviousTransition()I
    .locals 1

    .prologue
    .line 60
    const/4 v0, 0x2

    return v0
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 2
    .param p1, "icicle"    # Landroid/os/Bundle;

    .prologue
    .line 36
    invoke-super {p0, p1}, Lcom/google/android/setupwizard/BaseActivity;->onCreate(Landroid/os/Bundle;)V

    .line 37
    invoke-static {p0, p0}, Lcom/google/android/setupwizard/util/GooglePlaySetupHelper;->connect(Landroid/content/Context;Lcom/google/android/setupwizard/util/GooglePlaySetupHelper$GooglePlayConnectionListener;)Lcom/google/android/setupwizard/util/GooglePlaySetupHelper;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/setupwizard/account/StartVpaActivity;->mPlayHelper:Lcom/google/android/setupwizard/util/GooglePlaySetupHelper;

    .line 38
    const v0, 0x7f030017

    const v1, 0x7f030023

    invoke-virtual {p0, v0, v1}, Lcom/google/android/setupwizard/account/StartVpaActivity;->setTemplateContent(II)V

    .line 39
    const v0, 0x7f070051

    invoke-virtual {p0, v0}, Lcom/google/android/setupwizard/account/StartVpaActivity;->setHeaderText(I)V

    .line 40
    return-void
.end method

.method protected onDestroy()V
    .locals 1

    .prologue
    .line 52
    iget-object v0, p0, Lcom/google/android/setupwizard/account/StartVpaActivity;->mPlayHelper:Lcom/google/android/setupwizard/util/GooglePlaySetupHelper;

    if-eqz v0, :cond_0

    .line 53
    iget-object v0, p0, Lcom/google/android/setupwizard/account/StartVpaActivity;->mPlayHelper:Lcom/google/android/setupwizard/util/GooglePlaySetupHelper;

    invoke-virtual {v0}, Lcom/google/android/setupwizard/util/GooglePlaySetupHelper;->disconnect()V

    .line 55
    :cond_0
    invoke-super {p0}, Lcom/google/android/setupwizard/BaseActivity;->onDestroy()V

    .line 56
    return-void
.end method

.method public onGooglePlayConnected(Lcom/google/android/setupwizard/util/GooglePlaySetupHelper;)V
    .locals 2
    .param p1, "helper"    # Lcom/google/android/setupwizard/util/GooglePlaySetupHelper;

    .prologue
    .line 44
    iget-object v0, p0, Lcom/google/android/setupwizard/account/StartVpaActivity;->TAG:Ljava/lang/String;

    const-string v1, "Service connected. Starting VPA"

    invoke-static {v0, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 45
    invoke-virtual {p1}, Lcom/google/android/setupwizard/util/GooglePlaySetupHelper;->startVpa()V

    .line 46
    invoke-virtual {p0}, Lcom/google/android/setupwizard/account/StartVpaActivity;->nextScreen()V

    .line 47
    invoke-virtual {p0}, Lcom/google/android/setupwizard/account/StartVpaActivity;->finish()V

    .line 48
    return-void
.end method
