.class public Lcom/google/android/setupwizard/util/FourCornersGestureDetector;
.super Ljava/lang/Object;
.source "FourCornersGestureDetector.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/setupwizard/util/FourCornersGestureDetector$OnFourCornersListener;
    }
.end annotation


# instance fields
.field private final mListener:Lcom/google/android/setupwizard/util/FourCornersGestureDetector$OnFourCornersListener;

.field private mPattern:I

.field private final mView:Landroid/view/View;


# direct methods
.method public constructor <init>(Lcom/google/android/setupwizard/util/FourCornersGestureDetector$OnFourCornersListener;Landroid/view/View;)V
    .locals 1
    .param p1, "listener"    # Lcom/google/android/setupwizard/util/FourCornersGestureDetector$OnFourCornersListener;
    .param p2, "view"    # Landroid/view/View;

    .prologue
    .line 65
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 57
    const/4 v0, 0x0

    iput v0, p0, Lcom/google/android/setupwizard/util/FourCornersGestureDetector;->mPattern:I

    .line 66
    iput-object p1, p0, Lcom/google/android/setupwizard/util/FourCornersGestureDetector;->mListener:Lcom/google/android/setupwizard/util/FourCornersGestureDetector$OnFourCornersListener;

    .line 67
    iput-object p2, p0, Lcom/google/android/setupwizard/util/FourCornersGestureDetector;->mView:Landroid/view/View;

    .line 68
    return-void
.end method


# virtual methods
.method public onTouchEvent(Landroid/view/MotionEvent;)V
    .locals 14
    .param p1, "ev"    # Landroid/view/MotionEvent;

    .prologue
    const/high16 v13, 0x40400000    # 3.0f

    const/4 v10, 0x0

    const/4 v9, 0x1

    .line 77
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v11

    if-ne v11, v9, :cond_2

    .line 78
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v5

    .line 79
    .local v5, "x":F
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v7

    .line 80
    .local v7, "y":F
    iget-object v11, p0, Lcom/google/android/setupwizard/util/FourCornersGestureDetector;->mView:Landroid/view/View;

    invoke-virtual {v11}, Landroid/view/View;->getRight()I

    move-result v11

    iget-object v12, p0, Lcom/google/android/setupwizard/util/FourCornersGestureDetector;->mView:Landroid/view/View;

    invoke-virtual {v12}, Landroid/view/View;->getLeft()I

    move-result v12

    sub-int/2addr v11, v12

    int-to-float v11, v11

    div-float v6, v11, v13

    .line 81
    .local v6, "x_tolerance":F
    iget-object v11, p0, Lcom/google/android/setupwizard/util/FourCornersGestureDetector;->mView:Landroid/view/View;

    invoke-virtual {v11}, Landroid/view/View;->getBottom()I

    move-result v11

    iget-object v12, p0, Lcom/google/android/setupwizard/util/FourCornersGestureDetector;->mView:Landroid/view/View;

    invoke-virtual {v12}, Landroid/view/View;->getTop()I

    move-result v12

    sub-int/2addr v11, v12

    int-to-float v11, v11

    div-float v8, v11, v13

    .line 83
    .local v8, "y_tolerance":F
    const/4 v3, 0x0

    .line 84
    .local v3, "tap":B
    iget-object v11, p0, Lcom/google/android/setupwizard/util/FourCornersGestureDetector;->mView:Landroid/view/View;

    invoke-virtual {v11}, Landroid/view/View;->getLeft()I

    move-result v11

    int-to-float v11, v11

    sub-float v11, v5, v11

    cmpg-float v11, v11, v6

    if-gez v11, :cond_3

    move v1, v9

    .line 85
    .local v1, "left":Z
    :goto_0
    iget-object v11, p0, Lcom/google/android/setupwizard/util/FourCornersGestureDetector;->mView:Landroid/view/View;

    invoke-virtual {v11}, Landroid/view/View;->getRight()I

    move-result v11

    int-to-float v11, v11

    sub-float/2addr v11, v5

    cmpg-float v11, v11, v6

    if-gez v11, :cond_4

    move v2, v9

    .line 86
    .local v2, "right":Z
    :goto_1
    iget-object v11, p0, Lcom/google/android/setupwizard/util/FourCornersGestureDetector;->mView:Landroid/view/View;

    invoke-virtual {v11}, Landroid/view/View;->getTop()I

    move-result v11

    int-to-float v11, v11

    sub-float v11, v7, v11

    cmpg-float v11, v11, v8

    if-gez v11, :cond_5

    move v4, v9

    .line 87
    .local v4, "top":Z
    :goto_2
    iget-object v11, p0, Lcom/google/android/setupwizard/util/FourCornersGestureDetector;->mView:Landroid/view/View;

    invoke-virtual {v11}, Landroid/view/View;->getBottom()I

    move-result v11

    int-to-float v11, v11

    sub-float/2addr v11, v7

    cmpg-float v11, v11, v8

    if-gez v11, :cond_6

    move v0, v9

    .line 89
    .local v0, "bottom":Z
    :goto_3
    if-eqz v1, :cond_7

    if-eqz v4, :cond_7

    .line 90
    const/4 v3, 0x1

    .line 100
    :cond_0
    :goto_4
    iget v9, p0, Lcom/google/android/setupwizard/util/FourCornersGestureDetector;->mPattern:I

    shl-int/lit8 v9, v9, 0x4

    or-int/2addr v9, v3

    const v10, 0xffff

    and-int/2addr v9, v10

    iput v9, p0, Lcom/google/android/setupwizard/util/FourCornersGestureDetector;->mPattern:I

    .line 102
    iget v9, p0, Lcom/google/android/setupwizard/util/FourCornersGestureDetector;->mPattern:I

    const/16 v10, 0x1243

    if-eq v9, v10, :cond_1

    iget v9, p0, Lcom/google/android/setupwizard/util/FourCornersGestureDetector;->mPattern:I

    const/16 v10, 0x3214

    if-ne v9, v10, :cond_2

    .line 103
    :cond_1
    iget-object v9, p0, Lcom/google/android/setupwizard/util/FourCornersGestureDetector;->mListener:Lcom/google/android/setupwizard/util/FourCornersGestureDetector$OnFourCornersListener;

    iget v10, p0, Lcom/google/android/setupwizard/util/FourCornersGestureDetector;->mPattern:I

    invoke-interface {v9, v10}, Lcom/google/android/setupwizard/util/FourCornersGestureDetector$OnFourCornersListener;->onFourCorners(I)V

    .line 109
    .end local v0    # "bottom":Z
    .end local v1    # "left":Z
    .end local v2    # "right":Z
    .end local v3    # "tap":B
    .end local v4    # "top":Z
    .end local v5    # "x":F
    .end local v6    # "x_tolerance":F
    .end local v7    # "y":F
    .end local v8    # "y_tolerance":F
    :cond_2
    return-void

    .restart local v3    # "tap":B
    .restart local v5    # "x":F
    .restart local v6    # "x_tolerance":F
    .restart local v7    # "y":F
    .restart local v8    # "y_tolerance":F
    :cond_3
    move v1, v10

    .line 84
    goto :goto_0

    .restart local v1    # "left":Z
    :cond_4
    move v2, v10

    .line 85
    goto :goto_1

    .restart local v2    # "right":Z
    :cond_5
    move v4, v10

    .line 86
    goto :goto_2

    .restart local v4    # "top":Z
    :cond_6
    move v0, v10

    .line 87
    goto :goto_3

    .line 91
    .restart local v0    # "bottom":Z
    :cond_7
    if-eqz v2, :cond_8

    if-eqz v4, :cond_8

    .line 92
    const/4 v3, 0x2

    goto :goto_4

    .line 93
    :cond_8
    if-eqz v1, :cond_9

    if-eqz v0, :cond_9

    .line 94
    const/4 v3, 0x3

    goto :goto_4

    .line 95
    :cond_9
    if-eqz v2, :cond_0

    if-eqz v0, :cond_0

    .line 96
    const/4 v3, 0x4

    goto :goto_4
.end method
