.class public Lcom/google/android/setupwizard/util/GooglePlaySetupHelper;
.super Ljava/lang/Object;
.source "GooglePlaySetupHelper.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/setupwizard/util/GooglePlaySetupHelper$GooglePlayConnectionListener;
    }
.end annotation


# static fields
.field private static final GOOGLE_PLAY_SETUP_SERVICE_INTENT:Landroid/content/Intent;


# instance fields
.field private final mConnection:Landroid/content/ServiceConnection;

.field private final mContext:Landroid/content/Context;

.field private mListener:Lcom/google/android/setupwizard/util/GooglePlaySetupHelper$GooglePlayConnectionListener;

.field private mPlaySetupService:Lcom/android/vending/setup/IPlaySetupService;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 56
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    const-string v1, "com.android.vending"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setPackage(Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    const-string v1, "com.android.vending.setup.IPlaySetupService.BIND"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    sput-object v0, Lcom/google/android/setupwizard/util/GooglePlaySetupHelper;->GOOGLE_PLAY_SETUP_SERVICE_INTENT:Landroid/content/Intent;

    return-void
.end method

.method protected constructor <init>(Landroid/content/Context;Lcom/google/android/setupwizard/util/GooglePlaySetupHelper$GooglePlayConnectionListener;)V
    .locals 4
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "listener"    # Lcom/google/android/setupwizard/util/GooglePlaySetupHelper$GooglePlayConnectionListener;

    .prologue
    .line 91
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 73
    new-instance v1, Lcom/google/android/setupwizard/util/GooglePlaySetupHelper$1;

    invoke-direct {v1, p0}, Lcom/google/android/setupwizard/util/GooglePlaySetupHelper$1;-><init>(Lcom/google/android/setupwizard/util/GooglePlaySetupHelper;)V

    iput-object v1, p0, Lcom/google/android/setupwizard/util/GooglePlaySetupHelper;->mConnection:Landroid/content/ServiceConnection;

    .line 92
    iput-object p1, p0, Lcom/google/android/setupwizard/util/GooglePlaySetupHelper;->mContext:Landroid/content/Context;

    .line 93
    iput-object p2, p0, Lcom/google/android/setupwizard/util/GooglePlaySetupHelper;->mListener:Lcom/google/android/setupwizard/util/GooglePlaySetupHelper$GooglePlayConnectionListener;

    .line 94
    sget-object v1, Lcom/google/android/setupwizard/util/GooglePlaySetupHelper;->GOOGLE_PLAY_SETUP_SERVICE_INTENT:Landroid/content/Intent;

    iget-object v2, p0, Lcom/google/android/setupwizard/util/GooglePlaySetupHelper;->mConnection:Landroid/content/ServiceConnection;

    const/4 v3, 0x1

    invoke-virtual {p1, v1, v2, v3}, Landroid/content/Context;->bindService(Landroid/content/Intent;Landroid/content/ServiceConnection;I)Z

    move-result v0

    .line 96
    .local v0, "bound":Z
    const-string v1, "SetupWizard.GooglePlaySetupHelper"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "GooglePlaySetupHelper bound="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 97
    return-void
.end method

.method static synthetic access$002(Lcom/google/android/setupwizard/util/GooglePlaySetupHelper;Lcom/android/vending/setup/IPlaySetupService;)Lcom/android/vending/setup/IPlaySetupService;
    .locals 0
    .param p0, "x0"    # Lcom/google/android/setupwizard/util/GooglePlaySetupHelper;
    .param p1, "x1"    # Lcom/android/vending/setup/IPlaySetupService;

    .prologue
    .line 33
    iput-object p1, p0, Lcom/google/android/setupwizard/util/GooglePlaySetupHelper;->mPlaySetupService:Lcom/android/vending/setup/IPlaySetupService;

    return-object p1
.end method

.method static synthetic access$100(Lcom/google/android/setupwizard/util/GooglePlaySetupHelper;)Lcom/google/android/setupwizard/util/GooglePlaySetupHelper$GooglePlayConnectionListener;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/setupwizard/util/GooglePlaySetupHelper;

    .prologue
    .line 33
    iget-object v0, p0, Lcom/google/android/setupwizard/util/GooglePlaySetupHelper;->mListener:Lcom/google/android/setupwizard/util/GooglePlaySetupHelper$GooglePlayConnectionListener;

    return-object v0
.end method

.method static synthetic access$102(Lcom/google/android/setupwizard/util/GooglePlaySetupHelper;Lcom/google/android/setupwizard/util/GooglePlaySetupHelper$GooglePlayConnectionListener;)Lcom/google/android/setupwizard/util/GooglePlaySetupHelper$GooglePlayConnectionListener;
    .locals 0
    .param p0, "x0"    # Lcom/google/android/setupwizard/util/GooglePlaySetupHelper;
    .param p1, "x1"    # Lcom/google/android/setupwizard/util/GooglePlaySetupHelper$GooglePlayConnectionListener;

    .prologue
    .line 33
    iput-object p1, p0, Lcom/google/android/setupwizard/util/GooglePlaySetupHelper;->mListener:Lcom/google/android/setupwizard/util/GooglePlaySetupHelper$GooglePlayConnectionListener;

    return-object p1
.end method

.method public static connect(Landroid/content/Context;Lcom/google/android/setupwizard/util/GooglePlaySetupHelper$GooglePlayConnectionListener;)Lcom/google/android/setupwizard/util/GooglePlaySetupHelper;
    .locals 1
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "listener"    # Lcom/google/android/setupwizard/util/GooglePlaySetupHelper$GooglePlayConnectionListener;

    .prologue
    .line 62
    new-instance v0, Lcom/google/android/setupwizard/util/GooglePlaySetupHelper;

    invoke-direct {v0, p0, p1}, Lcom/google/android/setupwizard/util/GooglePlaySetupHelper;-><init>(Landroid/content/Context;Lcom/google/android/setupwizard/util/GooglePlaySetupHelper$GooglePlayConnectionListener;)V

    return-object v0
.end method


# virtual methods
.method public disconnect()V
    .locals 2

    .prologue
    .line 104
    iget-object v0, p0, Lcom/google/android/setupwizard/util/GooglePlaySetupHelper;->mContext:Landroid/content/Context;

    iget-object v1, p0, Lcom/google/android/setupwizard/util/GooglePlaySetupHelper;->mConnection:Landroid/content/ServiceConnection;

    invoke-virtual {v0, v1}, Landroid/content/Context;->unbindService(Landroid/content/ServiceConnection;)V

    .line 105
    return-void
.end method

.method public getEarlyUpdate()Landroid/os/Bundle;
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 111
    iget-object v0, p0, Lcom/google/android/setupwizard/util/GooglePlaySetupHelper;->mPlaySetupService:Lcom/android/vending/setup/IPlaySetupService;

    if-eqz v0, :cond_0

    .line 112
    iget-object v0, p0, Lcom/google/android/setupwizard/util/GooglePlaySetupHelper;->mPlaySetupService:Lcom/android/vending/setup/IPlaySetupService;

    invoke-interface {v0}, Lcom/android/vending/setup/IPlaySetupService;->getEarlyUpdate()Landroid/os/Bundle;

    move-result-object v0

    return-object v0

    .line 114
    :cond_0
    new-instance v0, Landroid/os/RemoteException;

    const-string v1, "Service not available"

    invoke-direct {v0, v1}, Landroid/os/RemoteException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public getFinalHoldFlow()Landroid/os/Bundle;
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 218
    iget-object v2, p0, Lcom/google/android/setupwizard/util/GooglePlaySetupHelper;->mPlaySetupService:Lcom/android/vending/setup/IPlaySetupService;

    if-eqz v2, :cond_0

    .line 220
    :try_start_0
    iget-object v2, p0, Lcom/google/android/setupwizard/util/GooglePlaySetupHelper;->mPlaySetupService:Lcom/android/vending/setup/IPlaySetupService;

    invoke-interface {v2}, Lcom/android/vending/setup/IPlaySetupService;->getFinalHoldFlow()Landroid/os/Bundle;
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    .line 227
    :goto_0
    return-object v1

    .line 221
    :catch_0
    move-exception v0

    .line 222
    .local v0, "e":Landroid/os/RemoteException;
    const-string v2, "SetupWizard.GooglePlaySetupHelper"

    const-string v3, "getFinalHoldFlow failed"

    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 226
    .end local v0    # "e":Landroid/os/RemoteException;
    :cond_0
    const-string v2, "SetupWizard.GooglePlaySetupHelper"

    const-string v3, "getFinalHoldFlow failed [service not available]"

    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method public getFinalHoldIntent()Landroid/content/Intent;
    .locals 2

    .prologue
    .line 236
    invoke-virtual {p0}, Lcom/google/android/setupwizard/util/GooglePlaySetupHelper;->getFinalHoldFlow()Landroid/os/Bundle;

    move-result-object v0

    .line 237
    .local v0, "bundle":Landroid/os/Bundle;
    if-eqz v0, :cond_0

    .line 238
    const-string v1, "final_hold_intent"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v1

    check-cast v1, Landroid/content/Intent;

    .line 240
    :goto_0
    return-object v1

    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public getRestoreFlow(Ljava/lang/String;)Landroid/os/Bundle;
    .locals 5
    .param p1, "accountName"    # Ljava/lang/String;

    .prologue
    const/4 v1, 0x0

    .line 170
    iget-object v2, p0, Lcom/google/android/setupwizard/util/GooglePlaySetupHelper;->mPlaySetupService:Lcom/android/vending/setup/IPlaySetupService;

    if-eqz v2, :cond_0

    .line 172
    :try_start_0
    iget-object v2, p0, Lcom/google/android/setupwizard/util/GooglePlaySetupHelper;->mPlaySetupService:Lcom/android/vending/setup/IPlaySetupService;

    invoke-interface {v2, p1}, Lcom/android/vending/setup/IPlaySetupService;->getRestoreFlow(Ljava/lang/String;)Landroid/os/Bundle;
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    .line 179
    :goto_0
    return-object v1

    .line 173
    :catch_0
    move-exception v0

    .line 174
    .local v0, "e":Landroid/os/RemoteException;
    const-string v2, "SetupWizard.GooglePlaySetupHelper"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "getRestoreFlow failed, accountName="

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 178
    .end local v0    # "e":Landroid/os/RemoteException;
    :cond_0
    const-string v2, "SetupWizard.GooglePlaySetupHelper"

    const-string v3, "getRestoreFlow failed [service not available]"

    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method public getRestoreIntent(Ljava/lang/String;)Landroid/content/Intent;
    .locals 2
    .param p1, "accountName"    # Ljava/lang/String;

    .prologue
    .line 192
    invoke-virtual {p0, p1}, Lcom/google/android/setupwizard/util/GooglePlaySetupHelper;->getRestoreFlow(Ljava/lang/String;)Landroid/os/Bundle;

    move-result-object v0

    .line 193
    .local v0, "bundle":Landroid/os/Bundle;
    if-eqz v0, :cond_0

    .line 194
    const-string v1, "available_restore_intent"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v1

    check-cast v1, Landroid/content/Intent;

    .line 196
    :goto_0
    return-object v1

    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public startDownloads()V
    .locals 4

    .prologue
    .line 203
    iget-object v1, p0, Lcom/google/android/setupwizard/util/GooglePlaySetupHelper;->mPlaySetupService:Lcom/android/vending/setup/IPlaySetupService;

    if-eqz v1, :cond_0

    .line 205
    :try_start_0
    iget-object v1, p0, Lcom/google/android/setupwizard/util/GooglePlaySetupHelper;->mPlaySetupService:Lcom/android/vending/setup/IPlaySetupService;

    invoke-interface {v1}, Lcom/android/vending/setup/IPlaySetupService;->startDownloads()V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 212
    :goto_0
    return-void

    .line 206
    :catch_0
    move-exception v0

    .line 207
    .local v0, "e":Landroid/os/RemoteException;
    const-string v1, "SetupWizard.GooglePlaySetupHelper"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "startDownloads failed e="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 210
    .end local v0    # "e":Landroid/os/RemoteException;
    :cond_0
    const-string v1, "SetupWizard.GooglePlaySetupHelper"

    const-string v2, "startDownloads failed [service not available]"

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method public startEarlyUpdate()V
    .locals 4

    .prologue
    .line 122
    iget-object v1, p0, Lcom/google/android/setupwizard/util/GooglePlaySetupHelper;->mPlaySetupService:Lcom/android/vending/setup/IPlaySetupService;

    if-eqz v1, :cond_0

    .line 124
    :try_start_0
    iget-object v1, p0, Lcom/google/android/setupwizard/util/GooglePlaySetupHelper;->mPlaySetupService:Lcom/android/vending/setup/IPlaySetupService;

    invoke-interface {v1}, Lcom/android/vending/setup/IPlaySetupService;->startEarlyUpdate()V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 131
    :goto_0
    return-void

    .line 125
    :catch_0
    move-exception v0

    .line 126
    .local v0, "e":Landroid/os/RemoteException;
    const-string v1, "SetupWizard.GooglePlaySetupHelper"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "startEarlyUpdate failed e="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 129
    .end local v0    # "e":Landroid/os/RemoteException;
    :cond_0
    const-string v1, "SetupWizard.GooglePlaySetupHelper"

    const-string v2, "startEarlyUpdate failed [service not available]"

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method public startVpa()V
    .locals 4

    .prologue
    .line 154
    iget-object v1, p0, Lcom/google/android/setupwizard/util/GooglePlaySetupHelper;->mPlaySetupService:Lcom/android/vending/setup/IPlaySetupService;

    if-eqz v1, :cond_0

    .line 156
    :try_start_0
    iget-object v1, p0, Lcom/google/android/setupwizard/util/GooglePlaySetupHelper;->mPlaySetupService:Lcom/android/vending/setup/IPlaySetupService;

    invoke-interface {v1}, Lcom/android/vending/setup/IPlaySetupService;->startVpa()V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 163
    :goto_0
    return-void

    .line 157
    :catch_0
    move-exception v0

    .line 158
    .local v0, "e":Landroid/os/RemoteException;
    const-string v1, "SetupWizard.GooglePlaySetupHelper"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "startVpa failed e="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 161
    .end local v0    # "e":Landroid/os/RemoteException;
    :cond_0
    const-string v1, "SetupWizard.GooglePlaySetupHelper"

    const-string v2, "startVpa failed [service not available]"

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method
