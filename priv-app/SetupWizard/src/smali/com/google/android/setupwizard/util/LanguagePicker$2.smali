.class Lcom/google/android/setupwizard/util/LanguagePicker$2;
.super Ljava/lang/Object;
.source "LanguagePicker.java"

# interfaces
.implements Landroid/widget/AdapterView$OnItemSelectedListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/setupwizard/util/LanguagePicker;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/setupwizard/util/LanguagePicker;


# direct methods
.method constructor <init>(Lcom/google/android/setupwizard/util/LanguagePicker;)V
    .locals 0

    .prologue
    .line 89
    iput-object p1, p0, Lcom/google/android/setupwizard/util/LanguagePicker$2;->this$0:Lcom/google/android/setupwizard/util/LanguagePicker;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onItemSelected(Landroid/widget/AdapterView;Landroid/view/View;IJ)V
    .locals 1
    .param p1, "parent"    # Landroid/widget/AdapterView;
    .param p2, "view"    # Landroid/view/View;
    .param p3, "position"    # I
    .param p4, "id"    # J

    .prologue
    .line 92
    iget-object v0, p0, Lcom/google/android/setupwizard/util/LanguagePicker$2;->this$0:Lcom/google/android/setupwizard/util/LanguagePicker;

    # getter for: Lcom/google/android/setupwizard/util/LanguagePicker;->mOnLanguageSelectedListener:Lcom/google/android/setupwizard/util/LanguagePicker$OnLanguageSelectedListener;
    invoke-static {v0}, Lcom/google/android/setupwizard/util/LanguagePicker;->access$000(Lcom/google/android/setupwizard/util/LanguagePicker;)Lcom/google/android/setupwizard/util/LanguagePicker$OnLanguageSelectedListener;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 93
    iget-object v0, p0, Lcom/google/android/setupwizard/util/LanguagePicker$2;->this$0:Lcom/google/android/setupwizard/util/LanguagePicker;

    # getter for: Lcom/google/android/setupwizard/util/LanguagePicker;->mOnLanguageSelectedListener:Lcom/google/android/setupwizard/util/LanguagePicker$OnLanguageSelectedListener;
    invoke-static {v0}, Lcom/google/android/setupwizard/util/LanguagePicker;->access$000(Lcom/google/android/setupwizard/util/LanguagePicker;)Lcom/google/android/setupwizard/util/LanguagePicker$OnLanguageSelectedListener;

    move-result-object v0

    invoke-interface {v0, p3}, Lcom/google/android/setupwizard/util/LanguagePicker$OnLanguageSelectedListener;->onLanguageSelected(I)V

    .line 95
    :cond_0
    return-void
.end method

.method public onNothingSelected(Landroid/widget/AdapterView;)V
    .locals 0
    .param p1, "parent"    # Landroid/widget/AdapterView;

    .prologue
    .line 100
    return-void
.end method
