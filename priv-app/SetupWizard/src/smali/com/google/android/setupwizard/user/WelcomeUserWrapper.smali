.class public Lcom/google/android/setupwizard/user/WelcomeUserWrapper;
.super Lcom/google/android/setupwizard/SubactivityWrapper;
.source "WelcomeUserWrapper.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 24
    invoke-direct {p0}, Lcom/google/android/setupwizard/SubactivityWrapper;-><init>()V

    return-void
.end method


# virtual methods
.method protected onSubactivityLaunch()Landroid/content/Intent;
    .locals 2

    .prologue
    .line 39
    invoke-virtual {p0}, Lcom/google/android/setupwizard/user/WelcomeUserWrapper;->getLastActivity()I

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/google/android/setupwizard/user/WelcomeUserWrapper;->isEduSignin()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/google/android/setupwizard/user/WelcomeUserWrapper;->hasNoAccounts()Z

    move-result v0

    if-nez v0, :cond_0

    .line 40
    const/4 v0, 0x0

    .line 44
    :goto_0
    return-object v0

    .line 41
    :cond_0
    invoke-virtual {p0}, Lcom/google/android/setupwizard/user/WelcomeUserWrapper;->isEduSignin()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 42
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/google/android/setupwizard/edu/WelcomeEduActivity;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    goto :goto_0

    .line 44
    :cond_1
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/google/android/setupwizard/user/WelcomeUserActivity;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    goto :goto_0
.end method

.method protected requestCode()I
    .locals 1

    .prologue
    .line 28
    const/16 v0, 0x2725

    return v0
.end method
