.class public Lcom/google/android/setupwizard/carrier/SimMissingActivity;
.super Lcom/google/android/setupwizard/BaseActivity;
.source "SimMissingActivity.java"


# instance fields
.field private final mPhoneStateListener:Landroid/telephony/PhoneStateListener;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 39
    invoke-direct {p0}, Lcom/google/android/setupwizard/BaseActivity;-><init>()V

    .line 43
    new-instance v0, Lcom/google/android/setupwizard/carrier/SimMissingActivity$1;

    invoke-direct {v0, p0}, Lcom/google/android/setupwizard/carrier/SimMissingActivity$1;-><init>(Lcom/google/android/setupwizard/carrier/SimMissingActivity;)V

    iput-object v0, p0, Lcom/google/android/setupwizard/carrier/SimMissingActivity;->mPhoneStateListener:Landroid/telephony/PhoneStateListener;

    return-void
.end method

.method static synthetic access$000(Lcom/google/android/setupwizard/carrier/SimMissingActivity;)Z
    .locals 1
    .param p0, "x0"    # Lcom/google/android/setupwizard/carrier/SimMissingActivity;

    .prologue
    .line 39
    invoke-direct {p0}, Lcom/google/android/setupwizard/carrier/SimMissingActivity;->hasHotSwappedSim()Z

    move-result v0

    return v0
.end method

.method static synthetic access$100(Lcom/google/android/setupwizard/carrier/SimMissingActivity;)V
    .locals 0
    .param p0, "x0"    # Lcom/google/android/setupwizard/carrier/SimMissingActivity;

    .prologue
    .line 39
    invoke-direct {p0}, Lcom/google/android/setupwizard/carrier/SimMissingActivity;->autoAdvance()V

    return-void
.end method

.method private autoAdvance()V
    .locals 1

    .prologue
    .line 126
    const/4 v0, 0x2

    invoke-virtual {p0, v0}, Lcom/google/android/setupwizard/carrier/SimMissingActivity;->finishAction(I)V

    .line 127
    return-void
.end method

.method private getQuantityTextForProduct(Ljava/lang/String;I)Ljava/lang/CharSequence;
    .locals 8
    .param p1, "entryNameRoot"    # Ljava/lang/String;
    .param p2, "count"    # I

    .prologue
    .line 134
    invoke-virtual {p0}, Lcom/google/android/setupwizard/carrier/SimMissingActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    .line 135
    .local v3, "res":Landroid/content/res/Resources;
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v5, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "_"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {p0}, Lcom/google/android/setupwizard/carrier/SimMissingActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v6

    const v7, 0x7f07003a

    invoke-virtual {v6, v7}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 136
    .local v0, "entryName":Ljava/lang/String;
    const-string v5, "plurals"

    invoke-virtual {p0}, Lcom/google/android/setupwizard/carrier/SimMissingActivity;->getPackageName()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v3, v0, v5, v6}, Landroid/content/res/Resources;->getIdentifier(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)I

    move-result v4

    .line 140
    .local v4, "resId":I
    invoke-virtual {p0}, Lcom/google/android/setupwizard/carrier/SimMissingActivity;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v5

    invoke-static {v5}, Lcom/google/android/setupwizard/util/Partner;->get(Landroid/content/pm/PackageManager;)Lcom/google/android/setupwizard/util/Partner;

    move-result-object v1

    .line 141
    .local v1, "partner":Lcom/google/android/setupwizard/util/Partner;
    if-eqz v1, :cond_0

    .line 142
    const-string v5, "plurals"

    invoke-virtual {v1, p1, v5}, Lcom/google/android/setupwizard/util/Partner;->getIdentifier(Ljava/lang/String;Ljava/lang/String;)I

    move-result v2

    .line 143
    .local v2, "partnerResId":I
    if-eqz v2, :cond_0

    .line 144
    invoke-virtual {v1}, Lcom/google/android/setupwizard/util/Partner;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    .line 145
    move v4, v2

    .line 149
    .end local v2    # "partnerResId":I
    :cond_0
    invoke-virtual {v3, v4, p2}, Landroid/content/res/Resources;->getQuantityText(II)Ljava/lang/CharSequence;

    move-result-object v5

    return-object v5
.end method

.method private hasHotSwappedSim()Z
    .locals 3

    .prologue
    .line 120
    invoke-virtual {p0}, Lcom/google/android/setupwizard/carrier/SimMissingActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x1120034

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getBoolean(I)Z

    move-result v0

    .line 122
    .local v0, "isHotSwapSupported":Z
    if-eqz v0, :cond_0

    invoke-static {}, Lcom/google/android/setupwizard/carrier/PhoneMonitor;->simMissing()Z

    move-result v1

    if-nez v1, :cond_0

    const/4 v1, 0x1

    :goto_0
    return v1

    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method private initView()V
    .locals 6

    .prologue
    .line 77
    const/4 v2, 0x1

    .line 79
    .local v2, "simSlotCount":I
    invoke-virtual {p0}, Lcom/google/android/setupwizard/carrier/SimMissingActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const v5, 0x7f0d0001

    invoke-virtual {v4, v5, v2}, Landroid/content/res/Resources;->getQuantityText(II)Ljava/lang/CharSequence;

    move-result-object v3

    .line 81
    .local v3, "title":Ljava/lang/CharSequence;
    const v4, 0x7f030016

    const v5, 0x7f030023

    invoke-virtual {p0, v4, v5}, Lcom/google/android/setupwizard/carrier/SimMissingActivity;->setTemplateContent(II)V

    .line 82
    invoke-virtual {p0, v3}, Lcom/google/android/setupwizard/carrier/SimMissingActivity;->setHeaderText(Ljava/lang/CharSequence;)V

    .line 83
    const v4, 0x7f0e0029

    invoke-virtual {p0, v4}, Lcom/google/android/setupwizard/carrier/SimMissingActivity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    .line 84
    .local v1, "descriptionView":Landroid/widget/TextView;
    const-string v4, "sim_missing_text"

    invoke-direct {p0, v4, v2}, Lcom/google/android/setupwizard/carrier/SimMissingActivity;->getQuantityTextForProduct(Ljava/lang/String;I)Ljava/lang/CharSequence;

    move-result-object v0

    .line 86
    .local v0, "description":Ljava/lang/CharSequence;
    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 87
    const v4, 0x7f020057

    const v5, 0x7f020058

    invoke-virtual {p0, v4, v5}, Lcom/google/android/setupwizard/carrier/SimMissingActivity;->setIllustration(II)V

    .line 89
    return-void
.end method


# virtual methods
.method protected getNextTransition()I
    .locals 1

    .prologue
    .line 115
    const/4 v0, 0x1

    return v0
.end method

.method protected onActivityResult(IILandroid/content/Intent;)V
    .locals 1
    .param p1, "requestCode"    # I
    .param p2, "resultCode"    # I
    .param p3, "data"    # Landroid/content/Intent;

    .prologue
    .line 105
    if-nez p2, :cond_0

    invoke-direct {p0}, Lcom/google/android/setupwizard/carrier/SimMissingActivity;->hasHotSwappedSim()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 106
    invoke-virtual {p0}, Lcom/google/android/setupwizard/carrier/SimMissingActivity;->finishAction()V

    .line 110
    :goto_0
    return-void

    .line 109
    :cond_0
    invoke-super {p0, p1, p2, p3}, Lcom/google/android/setupwizard/BaseActivity;->onActivityResult(IILandroid/content/Intent;)V

    goto :goto_0
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 0
    .param p1, "icicle"    # Landroid/os/Bundle;

    .prologue
    .line 54
    invoke-super {p0, p1}, Lcom/google/android/setupwizard/BaseActivity;->onCreate(Landroid/os/Bundle;)V

    .line 55
    invoke-direct {p0}, Lcom/google/android/setupwizard/carrier/SimMissingActivity;->initView()V

    .line 56
    return-void
.end method

.method public onNavigationBarCreated(Lcom/android/setupwizard/navigationbar/SetupWizardNavBar;)V
    .locals 2
    .param p1, "bar"    # Lcom/android/setupwizard/navigationbar/SetupWizardNavBar;

    .prologue
    .line 98
    invoke-super {p0, p1}, Lcom/google/android/setupwizard/BaseActivity;->onNavigationBarCreated(Lcom/android/setupwizard/navigationbar/SetupWizardNavBar;)V

    .line 99
    invoke-virtual {p1}, Lcom/android/setupwizard/navigationbar/SetupWizardNavBar;->getNextButton()Landroid/widget/Button;

    move-result-object v0

    const v1, 0x7f070046

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setText(I)V

    .line 100
    return-void
.end method

.method protected onPause()V
    .locals 1

    .prologue
    .line 71
    invoke-super {p0}, Lcom/google/android/setupwizard/BaseActivity;->onPause()V

    .line 72
    iget-object v0, p0, Lcom/google/android/setupwizard/carrier/SimMissingActivity;->mPhoneStateListener:Landroid/telephony/PhoneStateListener;

    invoke-static {v0}, Lcom/google/android/setupwizard/carrier/PhoneMonitor;->removeListener(Landroid/telephony/PhoneStateListener;)V

    .line 73
    return-void
.end method

.method protected onResume()V
    .locals 1

    .prologue
    .line 60
    invoke-super {p0}, Lcom/google/android/setupwizard/BaseActivity;->onResume()V

    .line 62
    invoke-direct {p0}, Lcom/google/android/setupwizard/carrier/SimMissingActivity;->hasHotSwappedSim()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 63
    invoke-direct {p0}, Lcom/google/android/setupwizard/carrier/SimMissingActivity;->autoAdvance()V

    .line 67
    :goto_0
    return-void

    .line 65
    :cond_0
    iget-object v0, p0, Lcom/google/android/setupwizard/carrier/SimMissingActivity;->mPhoneStateListener:Landroid/telephony/PhoneStateListener;

    invoke-static {v0}, Lcom/google/android/setupwizard/carrier/PhoneMonitor;->addListener(Landroid/telephony/PhoneStateListener;)V

    goto :goto_0
.end method

.method public start()V
    .locals 1

    .prologue
    .line 93
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/google/android/setupwizard/carrier/SimMissingActivity;->nextScreen(I)V

    .line 94
    return-void
.end method
