.class public Lcom/google/android/setupwizard/accessibility/MultitouchLongPressDetector;
.super Ljava/lang/Object;
.source "MultitouchLongPressDetector.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/setupwizard/accessibility/MultitouchLongPressDetector$LongPressListener;
    }
.end annotation


# instance fields
.field private final mHandler:Landroid/os/Handler;

.field private mInterceptEvents:Z

.field private mIsCanceled:Z

.field private mListener:Lcom/google/android/setupwizard/accessibility/MultitouchLongPressDetector$LongPressListener;

.field private final mLongPressDelay:I

.field private final mLongPressRunnable:Ljava/lang/Runnable;

.field private final mMinimumPointerCount:I

.field private mOriginEvent:Landroid/view/MotionEvent;

.field private mPointerDownCount:I

.field private final mTouchSlop:F


# direct methods
.method public constructor <init>(III)V
    .locals 1
    .param p1, "longPressDelay"    # I
    .param p2, "minPointerCount"    # I
    .param p3, "touchSlop"    # I

    .prologue
    .line 54
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 194
    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    iput-object v0, p0, Lcom/google/android/setupwizard/accessibility/MultitouchLongPressDetector;->mHandler:Landroid/os/Handler;

    .line 196
    new-instance v0, Lcom/google/android/setupwizard/accessibility/MultitouchLongPressDetector$1;

    invoke-direct {v0, p0}, Lcom/google/android/setupwizard/accessibility/MultitouchLongPressDetector$1;-><init>(Lcom/google/android/setupwizard/accessibility/MultitouchLongPressDetector;)V

    iput-object v0, p0, Lcom/google/android/setupwizard/accessibility/MultitouchLongPressDetector;->mLongPressRunnable:Ljava/lang/Runnable;

    .line 55
    iput p1, p0, Lcom/google/android/setupwizard/accessibility/MultitouchLongPressDetector;->mLongPressDelay:I

    .line 56
    iput p2, p0, Lcom/google/android/setupwizard/accessibility/MultitouchLongPressDetector;->mMinimumPointerCount:I

    .line 57
    int-to-float v0, p3

    iput v0, p0, Lcom/google/android/setupwizard/accessibility/MultitouchLongPressDetector;->mTouchSlop:F

    .line 58
    return-void
.end method

.method static synthetic access$000(Lcom/google/android/setupwizard/accessibility/MultitouchLongPressDetector;)V
    .locals 0
    .param p0, "x0"    # Lcom/google/android/setupwizard/accessibility/MultitouchLongPressDetector;

    .prologue
    .line 23
    invoke-direct {p0}, Lcom/google/android/setupwizard/accessibility/MultitouchLongPressDetector;->onLongPress()V

    return-void
.end method

.method private static getPointerDistance(Landroid/view/MotionEvent;Landroid/view/MotionEvent;I)F
    .locals 4
    .param p0, "event"    # Landroid/view/MotionEvent;
    .param p1, "other"    # Landroid/view/MotionEvent;
    .param p2, "index"    # I

    .prologue
    .line 190
    invoke-virtual {p0, p2}, Landroid/view/MotionEvent;->getX(I)F

    move-result v0

    invoke-virtual {p0, p2}, Landroid/view/MotionEvent;->getY(I)F

    move-result v1

    invoke-virtual {p1, p2}, Landroid/view/MotionEvent;->getX(I)F

    move-result v2

    invoke-virtual {p1, p2}, Landroid/view/MotionEvent;->getY(I)F

    move-result v3

    invoke-static {v0, v1, v2, v3}, Landroid/util/MathUtils;->dist(FFFF)F

    move-result v0

    return v0
.end method

.method private onLongPress()V
    .locals 2

    .prologue
    .line 171
    iget-object v1, p0, Lcom/google/android/setupwizard/accessibility/MultitouchLongPressDetector;->mListener:Lcom/google/android/setupwizard/accessibility/MultitouchLongPressDetector$LongPressListener;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/google/android/setupwizard/accessibility/MultitouchLongPressDetector;->mOriginEvent:Landroid/view/MotionEvent;

    if-eqz v1, :cond_0

    .line 172
    iget-object v1, p0, Lcom/google/android/setupwizard/accessibility/MultitouchLongPressDetector;->mOriginEvent:Landroid/view/MotionEvent;

    invoke-static {v1}, Landroid/view/MotionEvent;->obtainNoHistory(Landroid/view/MotionEvent;)Landroid/view/MotionEvent;

    move-result-object v0

    .line 173
    .local v0, "event":Landroid/view/MotionEvent;
    iget-object v1, p0, Lcom/google/android/setupwizard/accessibility/MultitouchLongPressDetector;->mListener:Lcom/google/android/setupwizard/accessibility/MultitouchLongPressDetector$LongPressListener;

    invoke-interface {v1, v0}, Lcom/google/android/setupwizard/accessibility/MultitouchLongPressDetector$LongPressListener;->onLongPress(Landroid/view/MotionEvent;)V

    .line 174
    invoke-virtual {v0}, Landroid/view/MotionEvent;->recycle()V

    .line 177
    .end local v0    # "event":Landroid/view/MotionEvent;
    :cond_0
    const/4 v1, 0x0

    invoke-direct {p0, v1}, Lcom/google/android/setupwizard/accessibility/MultitouchLongPressDetector;->setOriginEvent(Landroid/view/MotionEvent;)V

    .line 178
    return-void
.end method

.method private pointersWithinTouchSlop(Landroid/view/MotionEvent;Landroid/view/MotionEvent;)Z
    .locals 5
    .param p1, "currentEvent"    # Landroid/view/MotionEvent;
    .param p2, "previousEvent"    # Landroid/view/MotionEvent;

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 152
    if-eqz p2, :cond_0

    if-nez p1, :cond_2

    :cond_0
    move v1, v2

    .line 167
    :cond_1
    :goto_0
    return v1

    .line 157
    :cond_2
    iget v3, p0, Lcom/google/android/setupwizard/accessibility/MultitouchLongPressDetector;->mTouchSlop:F

    const/4 v4, 0x0

    cmpg-float v3, v3, v4

    if-ltz v3, :cond_1

    .line 161
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_1
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getPointerCount()I

    move-result v3

    if-ge v0, v3, :cond_1

    .line 162
    invoke-static {p1, p2, v0}, Lcom/google/android/setupwizard/accessibility/MultitouchLongPressDetector;->getPointerDistance(Landroid/view/MotionEvent;Landroid/view/MotionEvent;I)F

    move-result v3

    iget v4, p0, Lcom/google/android/setupwizard/accessibility/MultitouchLongPressDetector;->mTouchSlop:F

    cmpl-float v3, v3, v4

    if-lez v3, :cond_3

    move v1, v2

    .line 163
    goto :goto_0

    .line 161
    :cond_3
    add-int/lit8 v0, v0, 0x1

    goto :goto_1
.end method

.method private setOriginEvent(Landroid/view/MotionEvent;)V
    .locals 1
    .param p1, "event"    # Landroid/view/MotionEvent;

    .prologue
    .line 137
    iget-object v0, p0, Lcom/google/android/setupwizard/accessibility/MultitouchLongPressDetector;->mOriginEvent:Landroid/view/MotionEvent;

    if-eqz v0, :cond_0

    .line 138
    iget-object v0, p0, Lcom/google/android/setupwizard/accessibility/MultitouchLongPressDetector;->mOriginEvent:Landroid/view/MotionEvent;

    invoke-virtual {v0}, Landroid/view/MotionEvent;->recycle()V

    .line 141
    :cond_0
    if-eqz p1, :cond_1

    .line 142
    invoke-static {p1}, Landroid/view/MotionEvent;->obtainNoHistory(Landroid/view/MotionEvent;)Landroid/view/MotionEvent;

    move-result-object p1

    .line 145
    :cond_1
    iput-object p1, p0, Lcom/google/android/setupwizard/accessibility/MultitouchLongPressDetector;->mOriginEvent:Landroid/view/MotionEvent;

    .line 146
    return-void
.end method


# virtual methods
.method public getPointerCount()I
    .locals 1

    .prologue
    .line 64
    iget v0, p0, Lcom/google/android/setupwizard/accessibility/MultitouchLongPressDetector;->mPointerDownCount:I

    return v0
.end method

.method public onTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 10
    .param p1, "event"    # Landroid/view/MotionEvent;

    .prologue
    const/4 v3, 0x1

    const/4 v4, 0x0

    .line 73
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getPointerCount()I

    move-result v5

    iput v5, p0, Lcom/google/android/setupwizard/accessibility/MultitouchLongPressDetector;->mPointerDownCount:I

    .line 75
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getActionMasked()I

    move-result v0

    .line 78
    .local v0, "action":I
    if-nez v0, :cond_0

    .line 79
    iput-boolean v4, p0, Lcom/google/android/setupwizard/accessibility/MultitouchLongPressDetector;->mIsCanceled:Z

    .line 82
    :cond_0
    iget-boolean v5, p0, Lcom/google/android/setupwizard/accessibility/MultitouchLongPressDetector;->mIsCanceled:Z

    if-eqz v5, :cond_1

    .line 123
    :goto_0
    return v4

    .line 86
    :cond_1
    const/4 v2, 0x0

    .line 87
    .local v2, "startInterceptingEvents":Z
    const/4 v1, 0x0

    .line 89
    .local v1, "cancelPendingGesture":Z
    packed-switch v0, :pswitch_data_0

    .line 110
    :cond_2
    :goto_1
    :pswitch_0
    if-eqz v1, :cond_3

    .line 111
    iput-boolean v3, p0, Lcom/google/android/setupwizard/accessibility/MultitouchLongPressDetector;->mIsCanceled:Z

    .line 112
    iput-boolean v4, p0, Lcom/google/android/setupwizard/accessibility/MultitouchLongPressDetector;->mInterceptEvents:Z

    .line 113
    const/4 v5, 0x0

    invoke-direct {p0, v5}, Lcom/google/android/setupwizard/accessibility/MultitouchLongPressDetector;->setOriginEvent(Landroid/view/MotionEvent;)V

    .line 114
    iget-object v5, p0, Lcom/google/android/setupwizard/accessibility/MultitouchLongPressDetector;->mHandler:Landroid/os/Handler;

    iget-object v6, p0, Lcom/google/android/setupwizard/accessibility/MultitouchLongPressDetector;->mLongPressRunnable:Ljava/lang/Runnable;

    invoke-virtual {v5, v6}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    .line 117
    :cond_3
    if-eqz v2, :cond_5

    iget-boolean v5, p0, Lcom/google/android/setupwizard/accessibility/MultitouchLongPressDetector;->mInterceptEvents:Z

    if-nez v5, :cond_5

    .line 118
    iput-boolean v3, p0, Lcom/google/android/setupwizard/accessibility/MultitouchLongPressDetector;->mInterceptEvents:Z

    .line 119
    const/4 v3, 0x3

    invoke-virtual {p1, v3}, Landroid/view/MotionEvent;->setAction(I)V

    goto :goto_0

    .line 92
    :pswitch_1
    invoke-direct {p0, p1}, Lcom/google/android/setupwizard/accessibility/MultitouchLongPressDetector;->setOriginEvent(Landroid/view/MotionEvent;)V

    .line 93
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getPointerCount()I

    move-result v5

    iget v6, p0, Lcom/google/android/setupwizard/accessibility/MultitouchLongPressDetector;->mMinimumPointerCount:I

    if-lt v5, v6, :cond_2

    .line 94
    iget-object v5, p0, Lcom/google/android/setupwizard/accessibility/MultitouchLongPressDetector;->mHandler:Landroid/os/Handler;

    iget-object v6, p0, Lcom/google/android/setupwizard/accessibility/MultitouchLongPressDetector;->mLongPressRunnable:Ljava/lang/Runnable;

    invoke-virtual {v5, v6}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    .line 95
    iget-object v5, p0, Lcom/google/android/setupwizard/accessibility/MultitouchLongPressDetector;->mHandler:Landroid/os/Handler;

    iget-object v6, p0, Lcom/google/android/setupwizard/accessibility/MultitouchLongPressDetector;->mLongPressRunnable:Ljava/lang/Runnable;

    iget v7, p0, Lcom/google/android/setupwizard/accessibility/MultitouchLongPressDetector;->mLongPressDelay:I

    int-to-long v8, v7

    invoke-virtual {v5, v6, v8, v9}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 96
    const/4 v2, 0x1

    goto :goto_1

    .line 100
    :pswitch_2
    iget-object v5, p0, Lcom/google/android/setupwizard/accessibility/MultitouchLongPressDetector;->mOriginEvent:Landroid/view/MotionEvent;

    invoke-direct {p0, p1, v5}, Lcom/google/android/setupwizard/accessibility/MultitouchLongPressDetector;->pointersWithinTouchSlop(Landroid/view/MotionEvent;Landroid/view/MotionEvent;)Z

    move-result v5

    if-nez v5, :cond_4

    move v1, v3

    .line 101
    :goto_2
    goto :goto_1

    :cond_4
    move v1, v4

    .line 100
    goto :goto_2

    .line 105
    :pswitch_3
    const/4 v1, 0x1

    goto :goto_1

    .line 123
    :cond_5
    iget-boolean v4, p0, Lcom/google/android/setupwizard/accessibility/MultitouchLongPressDetector;->mInterceptEvents:Z

    goto :goto_0

    .line 89
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_3
        :pswitch_2
        :pswitch_3
        :pswitch_0
        :pswitch_1
        :pswitch_3
    .end packed-switch
.end method

.method public setListener(Lcom/google/android/setupwizard/accessibility/MultitouchLongPressDetector$LongPressListener;)V
    .locals 0
    .param p1, "listener"    # Lcom/google/android/setupwizard/accessibility/MultitouchLongPressDetector$LongPressListener;

    .prologue
    .line 127
    iput-object p1, p0, Lcom/google/android/setupwizard/accessibility/MultitouchLongPressDetector;->mListener:Lcom/google/android/setupwizard/accessibility/MultitouchLongPressDetector$LongPressListener;

    .line 128
    return-void
.end method
