.class Lcom/google/android/setupwizard/carrier/MobileDataActivity$4;
.super Landroid/content/BroadcastReceiver;
.source "MobileDataActivity.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/setupwizard/carrier/MobileDataActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/setupwizard/carrier/MobileDataActivity;


# direct methods
.method constructor <init>(Lcom/google/android/setupwizard/carrier/MobileDataActivity;)V
    .locals 0

    .prologue
    .line 146
    iput-object p1, p0, Lcom/google/android/setupwizard/carrier/MobileDataActivity$4;->this$0:Lcom/google/android/setupwizard/carrier/MobileDataActivity;

    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    return-void
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 4
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "intent"    # Landroid/content/Intent;

    .prologue
    .line 149
    iget-object v0, p0, Lcom/google/android/setupwizard/carrier/MobileDataActivity$4;->this$0:Lcom/google/android/setupwizard/carrier/MobileDataActivity;

    # getter for: Lcom/google/android/setupwizard/carrier/MobileDataActivity;->TAG:Ljava/lang/String;
    invoke-static {v0}, Lcom/google/android/setupwizard/carrier/MobileDataActivity;->access$400(Lcom/google/android/setupwizard/carrier/MobileDataActivity;)Ljava/lang/String;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "mConnectionCheckedReceiver "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p2}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 152
    const-string v0, "connected"

    const/4 v1, 0x0

    invoke-virtual {p2, v0, v1}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 153
    iget-object v0, p0, Lcom/google/android/setupwizard/carrier/MobileDataActivity$4;->this$0:Lcom/google/android/setupwizard/carrier/MobileDataActivity;

    # getter for: Lcom/google/android/setupwizard/carrier/MobileDataActivity;->TAG:Ljava/lang/String;
    invoke-static {v0}, Lcom/google/android/setupwizard/carrier/MobileDataActivity;->access$500(Lcom/google/android/setupwizard/carrier/MobileDataActivity;)Ljava/lang/String;

    move-result-object v0

    const-string v1, "data service detected"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 154
    iget-object v0, p0, Lcom/google/android/setupwizard/carrier/MobileDataActivity$4;->this$0:Lcom/google/android/setupwizard/carrier/MobileDataActivity;

    # invokes: Lcom/google/android/setupwizard/carrier/MobileDataActivity;->onSuccess()V
    invoke-static {v0}, Lcom/google/android/setupwizard/carrier/MobileDataActivity;->access$300(Lcom/google/android/setupwizard/carrier/MobileDataActivity;)V

    .line 158
    :goto_0
    return-void

    .line 156
    :cond_0
    iget-object v0, p0, Lcom/google/android/setupwizard/carrier/MobileDataActivity$4;->this$0:Lcom/google/android/setupwizard/carrier/MobileDataActivity;

    # getter for: Lcom/google/android/setupwizard/carrier/MobileDataActivity;->mHandler:Landroid/os/Handler;
    invoke-static {v0}, Lcom/google/android/setupwizard/carrier/MobileDataActivity;->access$700(Lcom/google/android/setupwizard/carrier/MobileDataActivity;)Landroid/os/Handler;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/setupwizard/carrier/MobileDataActivity$4;->this$0:Lcom/google/android/setupwizard/carrier/MobileDataActivity;

    # getter for: Lcom/google/android/setupwizard/carrier/MobileDataActivity;->mCheckConnection:Ljava/lang/Runnable;
    invoke-static {v1}, Lcom/google/android/setupwizard/carrier/MobileDataActivity;->access$600(Lcom/google/android/setupwizard/carrier/MobileDataActivity;)Ljava/lang/Runnable;

    move-result-object v1

    const-wide/16 v2, 0x1388

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    goto :goto_0
.end method
