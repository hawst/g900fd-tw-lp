.class public abstract Lcom/google/android/setupwizard/SubactivityWrapper;
.super Lcom/google/android/setupwizard/BaseActivity;
.source "SubactivityWrapper.java"


# instance fields
.field protected mIsSubactivityNotFound:Z


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 36
    invoke-direct {p0}, Lcom/google/android/setupwizard/BaseActivity;-><init>()V

    .line 38
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/setupwizard/SubactivityWrapper;->mIsSubactivityNotFound:Z

    return-void
.end method


# virtual methods
.method protected getPreviousTransition()I
    .locals 1

    .prologue
    .line 182
    const/4 v0, 0x0

    return v0
.end method

.method protected getSubactivityNextTransition()I
    .locals 1

    .prologue
    .line 190
    const/4 v0, 0x0

    return v0
.end method

.method protected getSubactivityPreviousTransition()I
    .locals 1

    .prologue
    .line 186
    const/4 v0, 0x0

    return v0
.end method

.method protected launchSubactivity()V
    .locals 3

    .prologue
    .line 88
    iget-object v1, p0, Lcom/google/android/setupwizard/SubactivityWrapper;->TAG:Ljava/lang/String;

    const-string v2, "SubactivityWrapper.launchSubactivity"

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 90
    invoke-virtual {p0}, Lcom/google/android/setupwizard/SubactivityWrapper;->onSubactivityLaunch()Landroid/content/Intent;

    move-result-object v0

    .line 91
    .local v0, "subactivityIntent":Landroid/content/Intent;
    if-eqz v0, :cond_0

    .line 92
    invoke-virtual {p0, v0}, Lcom/google/android/setupwizard/SubactivityWrapper;->launchSubactivity(Landroid/content/Intent;)V

    .line 98
    :goto_0
    return-void

    .line 94
    :cond_0
    iget-object v1, p0, Lcom/google/android/setupwizard/SubactivityWrapper;->TAG:Ljava/lang/String;

    const-string v2, "null intent returned; start next screen and finish"

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 95
    invoke-virtual {p0}, Lcom/google/android/setupwizard/SubactivityWrapper;->nextScreen()V

    .line 96
    invoke-virtual {p0}, Lcom/google/android/setupwizard/SubactivityWrapper;->finish()V

    goto :goto_0
.end method

.method protected launchSubactivity(Landroid/content/Intent;)V
    .locals 5
    .param p1, "subactivityIntent"    # Landroid/content/Intent;

    .prologue
    .line 101
    invoke-virtual {p0}, Lcom/google/android/setupwizard/SubactivityWrapper;->getIntent()Landroid/content/Intent;

    move-result-object v1

    .line 103
    .local v1, "intent":Landroid/content/Intent;
    const-string v2, "scriptUri"

    invoke-virtual {v1, v2}, Landroid/content/Intent;->hasExtra(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 104
    const-string v2, "scriptUri"

    const-string v3, "scriptUri"

    invoke-virtual {v1, v3}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 106
    const-string v2, "actionId"

    const-string v3, "actionId"

    invoke-virtual {v1, v3}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 111
    :cond_0
    :try_start_0
    invoke-virtual {p1}, Landroid/content/Intent;->getFlags()I

    move-result v2

    const/high16 v3, 0x2000000

    and-int/2addr v2, v3

    if-eqz v2, :cond_1

    .line 114
    invoke-virtual {p0, p1}, Lcom/google/android/setupwizard/SubactivityWrapper;->startFirstRunActivity(Landroid/content/Intent;)V

    .line 115
    const/4 v2, -0x1

    invoke-virtual {p0, v2}, Lcom/google/android/setupwizard/SubactivityWrapper;->setResultCode(I)V

    .line 116
    invoke-virtual {p0}, Lcom/google/android/setupwizard/SubactivityWrapper;->finish()V

    .line 120
    :goto_0
    const/4 v2, 0x0

    iput-boolean v2, p0, Lcom/google/android/setupwizard/SubactivityWrapper;->mIsSubactivityNotFound:Z

    .line 121
    invoke-virtual {p0}, Lcom/google/android/setupwizard/SubactivityWrapper;->getSubactivityPreviousTransition()I

    move-result v2

    invoke-virtual {p0, v2}, Lcom/google/android/setupwizard/SubactivityWrapper;->applyForwardTransition(I)V

    .line 129
    :goto_1
    return-void

    .line 118
    :cond_1
    invoke-virtual {p0}, Lcom/google/android/setupwizard/SubactivityWrapper;->requestCode()I

    move-result v2

    invoke-virtual {p0, p1, v2}, Lcom/google/android/setupwizard/SubactivityWrapper;->startFirstRunActivityForResult(Landroid/content/Intent;I)V
    :try_end_0
    .catch Landroid/content/ActivityNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 122
    :catch_0
    move-exception v0

    .line 123
    .local v0, "e":Landroid/content/ActivityNotFoundException;
    iget-object v2, p0, Lcom/google/android/setupwizard/SubactivityWrapper;->TAG:Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "intent has no matching activity; start next screen and finish; intent="

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 127
    const/4 v2, 0x1

    iput-boolean v2, p0, Lcom/google/android/setupwizard/SubactivityWrapper;->mIsSubactivityNotFound:Z

    goto :goto_1
.end method

.method protected onActivityResult(IILandroid/content/Intent;)V
    .locals 3
    .param p1, "requestCode"    # I
    .param p2, "resultCode"    # I
    .param p3, "data"    # Landroid/content/Intent;

    .prologue
    .line 138
    iget-object v0, p0, Lcom/google/android/setupwizard/SubactivityWrapper;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "SubactivityWrapper.onActivityResult("

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-static {p1}, Lcom/google/android/setupwizard/SubactivityWrapper;->getRequestName(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Lcom/google/android/setupwizard/SubactivityWrapper;->requestCode()I

    move-result v2

    invoke-static {v2, p2}, Lcom/google/android/setupwizard/SubactivityWrapper;->getResultName(II)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ")  topTask="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Lcom/google/android/setupwizard/SubactivityWrapper;->topTask()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 141
    invoke-virtual {p0}, Lcom/google/android/setupwizard/SubactivityWrapper;->requestCode()I

    move-result v0

    if-ne p1, v0, :cond_0

    .line 143
    invoke-virtual {p0, p1, p2, p3}, Lcom/google/android/setupwizard/SubactivityWrapper;->onSubactivityResult(IILandroid/content/Intent;)V

    .line 153
    :goto_0
    return-void

    .line 144
    :cond_0
    if-nez p2, :cond_1

    .line 146
    invoke-virtual {p0}, Lcom/google/android/setupwizard/SubactivityWrapper;->launchSubactivity()V

    .line 147
    invoke-virtual {p0}, Lcom/google/android/setupwizard/SubactivityWrapper;->getSubactivityNextTransition()I

    move-result v0

    invoke-virtual {p0, v0}, Lcom/google/android/setupwizard/SubactivityWrapper;->applyBackwardTransition(I)V

    goto :goto_0

    .line 151
    :cond_1
    invoke-super {p0, p1, p2, p3}, Lcom/google/android/setupwizard/BaseActivity;->onActivityResult(IILandroid/content/Intent;)V

    goto :goto_0
.end method

.method public onBackPressed()V
    .locals 1

    .prologue
    .line 72
    invoke-super {p0}, Lcom/google/android/setupwizard/BaseActivity;->onBackPressed()V

    .line 73
    const/4 v0, 0x2

    invoke-virtual {p0, v0}, Lcom/google/android/setupwizard/SubactivityWrapper;->applyBackwardTransition(I)V

    .line 74
    return-void
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 3
    .param p1, "icicle"    # Landroid/os/Bundle;

    .prologue
    .line 59
    iget-object v0, p0, Lcom/google/android/setupwizard/SubactivityWrapper;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "SubactivityWrapper.onCreate icicle="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 60
    const/4 v0, 0x2

    invoke-virtual {p0, v0}, Lcom/google/android/setupwizard/SubactivityWrapper;->applyForwardTransition(I)V

    .line 61
    invoke-super {p0, p1}, Lcom/google/android/setupwizard/BaseActivity;->onCreate(Landroid/os/Bundle;)V

    .line 63
    if-nez p1, :cond_0

    .line 64
    invoke-virtual {p0}, Lcom/google/android/setupwizard/SubactivityWrapper;->launchSubactivity()V

    .line 66
    :cond_0
    const v0, 0x7f030017

    const v1, 0x7f030023

    invoke-virtual {p0, v0, v1}, Lcom/google/android/setupwizard/SubactivityWrapper;->setTemplateContent(II)V

    .line 67
    const v0, 0x7f070051

    invoke-virtual {p0, v0}, Lcom/google/android/setupwizard/SubactivityWrapper;->setHeaderText(I)V

    .line 68
    return-void
.end method

.method public onNavigationBarCreated(Lcom/android/setupwizard/navigationbar/SetupWizardNavBar;)V
    .locals 2
    .param p1, "bar"    # Lcom/android/setupwizard/navigationbar/SetupWizardNavBar;

    .prologue
    .line 78
    invoke-super {p0, p1}, Lcom/google/android/setupwizard/BaseActivity;->onNavigationBarCreated(Lcom/android/setupwizard/navigationbar/SetupWizardNavBar;)V

    .line 79
    invoke-virtual {p1}, Lcom/android/setupwizard/navigationbar/SetupWizardNavBar;->getNextButton()Landroid/widget/Button;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setEnabled(Z)V

    .line 80
    return-void
.end method

.method protected abstract onSubactivityLaunch()Landroid/content/Intent;
.end method

.method protected onSubactivityResult(IILandroid/content/Intent;)V
    .locals 3
    .param p1, "requestCode"    # I
    .param p2, "resultCode"    # I
    .param p3, "data"    # Landroid/content/Intent;

    .prologue
    .line 160
    iget-object v1, p0, Lcom/google/android/setupwizard/SubactivityWrapper;->TAG:Ljava/lang/String;

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "SubactivityWrapper.onSubactivityResult("

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-static {p1}, Lcom/google/android/setupwizard/SubactivityWrapper;->getRequestName(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, ", "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p0}, Lcom/google/android/setupwizard/SubactivityWrapper;->requestCode()I

    move-result v2

    invoke-static {v2, p2}, Lcom/google/android/setupwizard/SubactivityWrapper;->getResultName(II)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, ", "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    if-eqz p3, :cond_0

    invoke-virtual {p3}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v0

    :goto_0
    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, ")"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 164
    if-nez p2, :cond_2

    .line 165
    iget-boolean v0, p0, Lcom/google/android/setupwizard/SubactivityWrapper;->mIsSubactivityNotFound:Z

    if-eqz v0, :cond_1

    .line 166
    const/4 v0, 0x3

    invoke-virtual {p0, v0}, Lcom/google/android/setupwizard/SubactivityWrapper;->nextScreen(I)V

    .line 167
    invoke-virtual {p0}, Lcom/google/android/setupwizard/SubactivityWrapper;->finish()V

    .line 176
    :goto_1
    return-void

    :cond_0
    move-object v0, p3

    .line 160
    goto :goto_0

    .line 169
    :cond_1
    invoke-virtual {p0}, Lcom/google/android/setupwizard/SubactivityWrapper;->getSubactivityPreviousTransition()I

    move-result v0

    invoke-virtual {p0, v0}, Lcom/google/android/setupwizard/SubactivityWrapper;->applyBackwardTransition(I)V

    .line 170
    const/4 v0, 0x0

    invoke-virtual {p0, v0, p3}, Lcom/google/android/setupwizard/SubactivityWrapper;->finishAction(ILandroid/content/Intent;)V

    goto :goto_1

    .line 173
    :cond_2
    invoke-virtual {p0}, Lcom/google/android/setupwizard/SubactivityWrapper;->getSubactivityNextTransition()I

    move-result v0

    invoke-virtual {p0, v0}, Lcom/google/android/setupwizard/SubactivityWrapper;->applyForwardTransition(I)V

    .line 174
    invoke-virtual {p0, p2, p3}, Lcom/google/android/setupwizard/SubactivityWrapper;->nextScreen(ILandroid/content/Intent;)V

    goto :goto_1
.end method

.method protected abstract requestCode()I
.end method
