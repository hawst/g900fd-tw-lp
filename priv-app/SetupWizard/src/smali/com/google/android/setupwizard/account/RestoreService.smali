.class public Lcom/google/android/setupwizard/account/RestoreService;
.super Landroid/app/Service;
.source "RestoreService.java"


# static fields
.field private static final LOCAL_LOGV:Z


# instance fields
.field private binder:Landroid/os/Binder;

.field private mError:I

.field private mNumPackages:I

.field private mRestoreObserver:Landroid/app/backup/RestoreObserver;

.field private mRestoreSession:Landroid/app/backup/RestoreSession;

.field private final mRestoreStarted:Ljava/util/concurrent/atomic/AtomicBoolean;

.field private mSpecifiedToken:J


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 43
    const-string v0, "SetupWizard"

    const/4 v1, 0x2

    invoke-static {v0, v1}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    sput-boolean v0, Lcom/google/android/setupwizard/account/RestoreService;->LOCAL_LOGV:Z

    return-void
.end method

.method public constructor <init>()V
    .locals 2

    .prologue
    .line 40
    invoke-direct {p0}, Landroid/app/Service;-><init>()V

    .line 63
    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/setupwizard/account/RestoreService;->mNumPackages:I

    .line 65
    const/high16 v0, -0x80000000

    iput v0, p0, Lcom/google/android/setupwizard/account/RestoreService;->mError:I

    .line 67
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/google/android/setupwizard/account/RestoreService;->mSpecifiedToken:J

    .line 71
    new-instance v0, Landroid/os/Binder;

    invoke-direct {v0}, Landroid/os/Binder;-><init>()V

    iput-object v0, p0, Lcom/google/android/setupwizard/account/RestoreService;->binder:Landroid/os/Binder;

    .line 73
    new-instance v0, Ljava/util/concurrent/atomic/AtomicBoolean;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Ljava/util/concurrent/atomic/AtomicBoolean;-><init>(Z)V

    iput-object v0, p0, Lcom/google/android/setupwizard/account/RestoreService;->mRestoreStarted:Ljava/util/concurrent/atomic/AtomicBoolean;

    .line 181
    new-instance v0, Lcom/google/android/setupwizard/account/RestoreService$1;

    invoke-direct {v0, p0}, Lcom/google/android/setupwizard/account/RestoreService$1;-><init>(Lcom/google/android/setupwizard/account/RestoreService;)V

    iput-object v0, p0, Lcom/google/android/setupwizard/account/RestoreService;->mRestoreObserver:Landroid/app/backup/RestoreObserver;

    return-void
.end method

.method static synthetic access$000()Z
    .locals 1

    .prologue
    .line 40
    sget-boolean v0, Lcom/google/android/setupwizard/account/RestoreService;->LOCAL_LOGV:Z

    return v0
.end method

.method static synthetic access$100(Lcom/google/android/setupwizard/account/RestoreService;)I
    .locals 1
    .param p0, "x0"    # Lcom/google/android/setupwizard/account/RestoreService;

    .prologue
    .line 40
    iget v0, p0, Lcom/google/android/setupwizard/account/RestoreService;->mNumPackages:I

    return v0
.end method

.method static synthetic access$102(Lcom/google/android/setupwizard/account/RestoreService;I)I
    .locals 0
    .param p0, "x0"    # Lcom/google/android/setupwizard/account/RestoreService;
    .param p1, "x1"    # I

    .prologue
    .line 40
    iput p1, p0, Lcom/google/android/setupwizard/account/RestoreService;->mNumPackages:I

    return p1
.end method

.method static synthetic access$200(Lcom/google/android/setupwizard/account/RestoreService;Ljava/lang/String;)V
    .locals 0
    .param p0, "x0"    # Lcom/google/android/setupwizard/account/RestoreService;
    .param p1, "x1"    # Ljava/lang/String;

    .prologue
    .line 40
    invoke-direct {p0, p1}, Lcom/google/android/setupwizard/account/RestoreService;->setState(Ljava/lang/String;)V

    return-void
.end method

.method static synthetic access$302(Lcom/google/android/setupwizard/account/RestoreService;I)I
    .locals 0
    .param p0, "x0"    # Lcom/google/android/setupwizard/account/RestoreService;
    .param p1, "x1"    # I

    .prologue
    .line 40
    iput p1, p0, Lcom/google/android/setupwizard/account/RestoreService;->mError:I

    return p1
.end method

.method static synthetic access$400(Lcom/google/android/setupwizard/account/RestoreService;)Landroid/app/backup/RestoreSession;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/setupwizard/account/RestoreService;

    .prologue
    .line 40
    iget-object v0, p0, Lcom/google/android/setupwizard/account/RestoreService;->mRestoreSession:Landroid/app/backup/RestoreSession;

    return-object v0
.end method

.method private setState(Ljava/lang/String;)V
    .locals 3
    .param p1, "state"    # Ljava/lang/String;

    .prologue
    .line 175
    new-instance v0, Landroid/content/Intent;

    const-string v1, "com.google.android.setupwizard.RESTORE_STATE_CHANGE"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 176
    .local v0, "intent":Landroid/content/Intent;
    const-string v1, "restore_state"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 177
    const-string v1, "restore_error"

    iget v2, p0, Lcom/google/android/setupwizard/account/RestoreService;->mError:I

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 178
    invoke-static {p0}, Landroid/support/v4/content/LocalBroadcastManager;->getInstance(Landroid/content/Context;)Landroid/support/v4/content/LocalBroadcastManager;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/support/v4/content/LocalBroadcastManager;->sendBroadcast(Landroid/content/Intent;)Z

    .line 179
    return-void
.end method


# virtual methods
.method public onBind(Landroid/content/Intent;)Landroid/os/IBinder;
    .locals 1
    .param p1, "intent"    # Landroid/content/Intent;

    .prologue
    .line 81
    iget-object v0, p0, Lcom/google/android/setupwizard/account/RestoreService;->binder:Landroid/os/Binder;

    return-object v0
.end method

.method public onDestroy()V
    .locals 2

    .prologue
    .line 169
    invoke-super {p0}, Landroid/app/Service;->onDestroy()V

    .line 170
    sget-boolean v0, Lcom/google/android/setupwizard/account/RestoreService;->LOCAL_LOGV:Z

    if-eqz v0, :cond_0

    const-string v0, "SetupWizard.RestoreService"

    const-string v1, "Service destroyed"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 171
    :cond_0
    iget v0, p0, Lcom/google/android/setupwizard/account/RestoreService;->mError:I

    if-nez v0, :cond_1

    const-string v0, "finished"

    :goto_0
    invoke-direct {p0, v0}, Lcom/google/android/setupwizard/account/RestoreService;->setState(Ljava/lang/String;)V

    .line 172
    return-void

    .line 171
    :cond_1
    const-string v0, "failed"

    goto :goto_0
.end method

.method public onStartCommand(Landroid/content/Intent;II)I
    .locals 4
    .param p1, "intent"    # Landroid/content/Intent;
    .param p2, "flags"    # I
    .param p3, "startId"    # I

    .prologue
    .line 86
    sget-boolean v0, Lcom/google/android/setupwizard/account/RestoreService;->LOCAL_LOGV:Z

    if-eqz v0, :cond_0

    const-string v0, "SetupWizard.RestoreService"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Restore service started "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 87
    :cond_0
    iget-object v1, p0, Lcom/google/android/setupwizard/account/RestoreService;->mRestoreStarted:Ljava/util/concurrent/atomic/AtomicBoolean;

    monitor-enter v1

    .line 88
    :try_start_0
    iget-object v0, p0, Lcom/google/android/setupwizard/account/RestoreService;->mRestoreStarted:Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicBoolean;->get()Z

    move-result v0

    if-nez v0, :cond_1

    .line 89
    const-string v0, "restoreToken"

    const-wide/16 v2, 0x0

    invoke-virtual {p1, v0, v2, v3}, Landroid/content/Intent;->getLongExtra(Ljava/lang/String;J)J

    move-result-wide v2

    iput-wide v2, p0, Lcom/google/android/setupwizard/account/RestoreService;->mSpecifiedToken:J

    .line 90
    invoke-virtual {p0, p0}, Lcom/google/android/setupwizard/account/RestoreService;->prepareRestore(Landroid/content/Context;)Z

    .line 91
    iget-object v0, p0, Lcom/google/android/setupwizard/account/RestoreService;->mRestoreStarted:Ljava/util/concurrent/atomic/AtomicBoolean;

    const/4 v2, 0x1

    invoke-virtual {v0, v2}, Ljava/util/concurrent/atomic/AtomicBoolean;->set(Z)V

    .line 92
    const-string v0, "preparing"

    invoke-direct {p0, v0}, Lcom/google/android/setupwizard/account/RestoreService;->setState(Ljava/lang/String;)V

    .line 94
    :cond_1
    monitor-exit v1

    .line 95
    const/4 v0, 0x2

    return v0

    .line 94
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method performRestore([Landroid/app/backup/RestoreSet;)Z
    .locals 12
    .param p1, "sets"    # [Landroid/app/backup/RestoreSet;

    .prologue
    const/4 v5, 0x0

    .line 137
    const/4 v4, 0x0

    .line 139
    .local v4, "startedRestore":Z
    if-eqz p1, :cond_0

    :try_start_0
    array-length v8, p1

    if-nez v8, :cond_4

    .line 140
    :cond_0
    sget-boolean v8, Lcom/google/android/setupwizard/account/RestoreService;->LOCAL_LOGV:Z

    if-eqz v8, :cond_1

    const-string v8, "SetupWizard.RestoreService"

    const-string v9, "No restore sets found"

    invoke-static {v8, v9}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 157
    :cond_1
    if-nez v4, :cond_3

    .line 158
    iget-object v8, p0, Lcom/google/android/setupwizard/account/RestoreService;->mRestoreSession:Landroid/app/backup/RestoreSession;

    if-eqz v8, :cond_2

    .line 159
    iget-object v8, p0, Lcom/google/android/setupwizard/account/RestoreService;->mRestoreSession:Landroid/app/backup/RestoreSession;

    invoke-virtual {v8}, Landroid/app/backup/RestoreSession;->endRestoreSession()V

    .line 161
    :cond_2
    invoke-virtual {p0}, Lcom/google/android/setupwizard/account/RestoreService;->stopSelf()V

    .line 164
    :cond_3
    :goto_0
    return v5

    .line 144
    :cond_4
    :try_start_1
    sget-boolean v5, Lcom/google/android/setupwizard/account/RestoreService;->LOCAL_LOGV:Z

    if-eqz v5, :cond_5

    .line 145
    const-string v5, "SetupWizard.RestoreService"

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "Found "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    array-length v9, p1

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, " available restores"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v5, v8}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 146
    move-object v0, p1

    .local v0, "arr$":[Landroid/app/backup/RestoreSet;
    array-length v2, v0

    .local v2, "len$":I
    const/4 v1, 0x0

    .local v1, "i$":I
    :goto_1
    if-ge v1, v2, :cond_5

    aget-object v3, v0, v1

    .line 147
    .local v3, "s":Landroid/app/backup/RestoreSet;
    const-string v5, "SetupWizard.RestoreService"

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "Available restore: "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    iget-object v9, v3, Landroid/app/backup/RestoreSet;->name:Ljava/lang/String;

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, " / "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    iget-wide v10, v3, Landroid/app/backup/RestoreSet;->token:J

    invoke-virtual {v8, v10, v11}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v5, v8}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 146
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 150
    .end local v0    # "arr$":[Landroid/app/backup/RestoreSet;
    .end local v1    # "i$":I
    .end local v2    # "len$":I
    .end local v3    # "s":Landroid/app/backup/RestoreSet;
    :cond_5
    iget-wide v8, p0, Lcom/google/android/setupwizard/account/RestoreService;->mSpecifiedToken:J

    const-wide/16 v10, 0x0

    cmp-long v5, v8, v10

    if-eqz v5, :cond_9

    iget-wide v6, p0, Lcom/google/android/setupwizard/account/RestoreService;->mSpecifiedToken:J

    .line 151
    .local v6, "token":J
    :goto_2
    iget-object v5, p0, Lcom/google/android/setupwizard/account/RestoreService;->mRestoreSession:Landroid/app/backup/RestoreSession;

    iget-object v8, p0, Lcom/google/android/setupwizard/account/RestoreService;->mRestoreObserver:Landroid/app/backup/RestoreObserver;

    invoke-virtual {v5, v6, v7, v8}, Landroid/app/backup/RestoreSession;->restoreAll(JLandroid/app/backup/RestoreObserver;)I
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result v5

    if-nez v5, :cond_6

    .line 152
    const/4 v4, 0x1

    .line 157
    :cond_6
    if-nez v4, :cond_8

    .line 158
    iget-object v5, p0, Lcom/google/android/setupwizard/account/RestoreService;->mRestoreSession:Landroid/app/backup/RestoreSession;

    if-eqz v5, :cond_7

    .line 159
    iget-object v5, p0, Lcom/google/android/setupwizard/account/RestoreService;->mRestoreSession:Landroid/app/backup/RestoreSession;

    invoke-virtual {v5}, Landroid/app/backup/RestoreSession;->endRestoreSession()V

    .line 161
    :cond_7
    invoke-virtual {p0}, Lcom/google/android/setupwizard/account/RestoreService;->stopSelf()V

    .line 164
    :cond_8
    const/4 v5, 0x1

    goto :goto_0

    .line 150
    .end local v6    # "token":J
    :cond_9
    const/4 v5, 0x0

    :try_start_2
    aget-object v5, p1, v5

    iget-wide v6, v5, Landroid/app/backup/RestoreSet;->token:J
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_2

    .line 157
    :catchall_0
    move-exception v5

    if-nez v4, :cond_b

    .line 158
    iget-object v8, p0, Lcom/google/android/setupwizard/account/RestoreService;->mRestoreSession:Landroid/app/backup/RestoreSession;

    if-eqz v8, :cond_a

    .line 159
    iget-object v8, p0, Lcom/google/android/setupwizard/account/RestoreService;->mRestoreSession:Landroid/app/backup/RestoreSession;

    invoke-virtual {v8}, Landroid/app/backup/RestoreSession;->endRestoreSession()V

    .line 161
    :cond_a
    invoke-virtual {p0}, Lcom/google/android/setupwizard/account/RestoreService;->stopSelf()V

    :cond_b
    throw v5
.end method

.method prepareRestore(Landroid/content/Context;)Z
    .locals 7
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    const/4 v4, 0x0

    .line 105
    const/4 v3, 0x0

    .line 106
    .local v3, "startedList":Z
    const/4 v5, 0x0

    iput-object v5, p0, Lcom/google/android/setupwizard/account/RestoreService;->mRestoreSession:Landroid/app/backup/RestoreSession;

    .line 108
    :try_start_0
    new-instance v0, Landroid/app/backup/BackupManager;

    invoke-direct {v0, p1}, Landroid/app/backup/BackupManager;-><init>(Landroid/content/Context;)V

    .line 109
    .local v0, "bm":Landroid/app/backup/BackupManager;
    invoke-virtual {v0}, Landroid/app/backup/BackupManager;->beginRestoreSession()Landroid/app/backup/RestoreSession;

    move-result-object v5

    iput-object v5, p0, Lcom/google/android/setupwizard/account/RestoreService;->mRestoreSession:Landroid/app/backup/RestoreSession;

    .line 110
    iget-object v5, p0, Lcom/google/android/setupwizard/account/RestoreService;->mRestoreSession:Landroid/app/backup/RestoreSession;

    if-nez v5, :cond_3

    .line 111
    sget-boolean v5, Lcom/google/android/setupwizard/account/RestoreService;->LOCAL_LOGV:Z

    if-eqz v5, :cond_0

    const-string v5, "SetupWizard.RestoreService"

    const-string v6, "No restore session"

    invoke-static {v5, v6}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 126
    :cond_0
    if-nez v3, :cond_2

    .line 127
    iget-object v5, p0, Lcom/google/android/setupwizard/account/RestoreService;->mRestoreSession:Landroid/app/backup/RestoreSession;

    if-eqz v5, :cond_1

    .line 128
    iget-object v5, p0, Lcom/google/android/setupwizard/account/RestoreService;->mRestoreSession:Landroid/app/backup/RestoreSession;

    invoke-virtual {v5}, Landroid/app/backup/RestoreSession;->endRestoreSession()V

    .line 130
    :cond_1
    invoke-virtual {p0}, Lcom/google/android/setupwizard/account/RestoreService;->stopSelf()V

    .line 133
    .end local v0    # "bm":Landroid/app/backup/BackupManager;
    :cond_2
    :goto_0
    return v4

    .line 114
    .restart local v0    # "bm":Landroid/app/backup/BackupManager;
    :cond_3
    :try_start_1
    iget-object v5, p0, Lcom/google/android/setupwizard/account/RestoreService;->mRestoreSession:Landroid/app/backup/RestoreSession;

    iget-object v6, p0, Lcom/google/android/setupwizard/account/RestoreService;->mRestoreObserver:Landroid/app/backup/RestoreObserver;

    invoke-virtual {v5, v6}, Landroid/app/backup/RestoreSession;->getAvailableRestoreSets(Landroid/app/backup/RestoreObserver;)I
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result v2

    .line 115
    .local v2, "err":I
    if-nez v2, :cond_5

    .line 118
    const/4 v3, 0x1

    .line 119
    const/4 v4, 0x1

    .line 126
    if-nez v3, :cond_2

    .line 127
    iget-object v5, p0, Lcom/google/android/setupwizard/account/RestoreService;->mRestoreSession:Landroid/app/backup/RestoreSession;

    if-eqz v5, :cond_4

    .line 128
    iget-object v5, p0, Lcom/google/android/setupwizard/account/RestoreService;->mRestoreSession:Landroid/app/backup/RestoreSession;

    invoke-virtual {v5}, Landroid/app/backup/RestoreSession;->endRestoreSession()V

    .line 130
    :cond_4
    invoke-virtual {p0}, Lcom/google/android/setupwizard/account/RestoreService;->stopSelf()V

    goto :goto_0

    .line 121
    :cond_5
    :try_start_2
    sget-boolean v5, Lcom/google/android/setupwizard/account/RestoreService;->LOCAL_LOGV:Z

    if-eqz v5, :cond_6

    const-string v5, "SetupWizard.RestoreService"

    const-string v6, "Unable to contact server for restore"

    invoke-static {v5, v6}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_0
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 126
    :cond_6
    if-nez v3, :cond_2

    .line 127
    iget-object v5, p0, Lcom/google/android/setupwizard/account/RestoreService;->mRestoreSession:Landroid/app/backup/RestoreSession;

    if-eqz v5, :cond_7

    .line 128
    iget-object v5, p0, Lcom/google/android/setupwizard/account/RestoreService;->mRestoreSession:Landroid/app/backup/RestoreSession;

    invoke-virtual {v5}, Landroid/app/backup/RestoreSession;->endRestoreSession()V

    .line 130
    :cond_7
    invoke-virtual {p0}, Lcom/google/android/setupwizard/account/RestoreService;->stopSelf()V

    goto :goto_0

    .line 122
    .end local v0    # "bm":Landroid/app/backup/BackupManager;
    .end local v2    # "err":I
    :catch_0
    move-exception v1

    .line 124
    .local v1, "e":Ljava/lang/Exception;
    :try_start_3
    const-string v5, "SetupWizard.RestoreService"

    const-string v6, "error starting restore"

    invoke-static {v5, v6, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 126
    if-nez v3, :cond_2

    .line 127
    iget-object v5, p0, Lcom/google/android/setupwizard/account/RestoreService;->mRestoreSession:Landroid/app/backup/RestoreSession;

    if-eqz v5, :cond_8

    .line 128
    iget-object v5, p0, Lcom/google/android/setupwizard/account/RestoreService;->mRestoreSession:Landroid/app/backup/RestoreSession;

    invoke-virtual {v5}, Landroid/app/backup/RestoreSession;->endRestoreSession()V

    .line 130
    :cond_8
    invoke-virtual {p0}, Lcom/google/android/setupwizard/account/RestoreService;->stopSelf()V

    goto :goto_0

    .line 126
    .end local v1    # "e":Ljava/lang/Exception;
    :catchall_0
    move-exception v4

    if-nez v3, :cond_a

    .line 127
    iget-object v5, p0, Lcom/google/android/setupwizard/account/RestoreService;->mRestoreSession:Landroid/app/backup/RestoreSession;

    if-eqz v5, :cond_9

    .line 128
    iget-object v5, p0, Lcom/google/android/setupwizard/account/RestoreService;->mRestoreSession:Landroid/app/backup/RestoreSession;

    invoke-virtual {v5}, Landroid/app/backup/RestoreSession;->endRestoreSession()V

    .line 130
    :cond_9
    invoke-virtual {p0}, Lcom/google/android/setupwizard/account/RestoreService;->stopSelf()V

    :cond_a
    throw v4
.end method
