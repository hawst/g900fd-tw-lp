.class public Lcom/google/android/setupwizard/carrier/HfaWrapper;
.super Lcom/google/android/setupwizard/SubactivityWrapper;
.source "HfaWrapper.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 8
    invoke-direct {p0}, Lcom/google/android/setupwizard/SubactivityWrapper;-><init>()V

    return-void
.end method


# virtual methods
.method protected onSubactivityLaunch()Landroid/content/Intent;
    .locals 4

    .prologue
    const/4 v0, 0x0

    .line 17
    iget-object v1, p0, Lcom/google/android/setupwizard/carrier/HfaWrapper;->TAG:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "onSubactivityLaunch "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-static {}, Lcom/google/android/setupwizard/carrier/PhoneMonitor;->getPhoneState()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 20
    invoke-virtual {p0}, Lcom/google/android/setupwizard/carrier/HfaWrapper;->isMobileNetworkSupported()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-virtual {p0}, Lcom/google/android/setupwizard/carrier/HfaWrapper;->isPrimaryUser()Z

    move-result v1

    if-nez v1, :cond_1

    .line 38
    :cond_0
    :goto_0
    return-object v0

    .line 24
    :cond_1
    invoke-static {}, Lcom/google/android/setupwizard/carrier/PhoneMonitor;->simMissing()Z

    move-result v1

    if-eqz v1, :cond_2

    .line 25
    invoke-virtual {p0}, Lcom/google/android/setupwizard/carrier/HfaWrapper;->shouldSuppressSimWarning()Z

    move-result v1

    if-nez v1, :cond_0

    .line 29
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/google/android/setupwizard/carrier/SimMissingActivity;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    goto :goto_0

    .line 31
    :cond_2
    invoke-static {}, Lcom/google/android/setupwizard/carrier/PhoneMonitor;->isHfaActivationRequired()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 32
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/google/android/setupwizard/carrier/MobileDataActivity;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 33
    .local v0, "intent":Landroid/content/Intent;
    invoke-virtual {p0}, Lcom/google/android/setupwizard/carrier/HfaWrapper;->isUsingWizardManager()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 34
    const/high16 v1, 0x2000000

    invoke-virtual {v0, v1}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    goto :goto_0
.end method

.method protected requestCode()I
    .locals 1

    .prologue
    .line 12
    const/16 v0, 0x2726

    return v0
.end method
