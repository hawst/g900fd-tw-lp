.class public Lcom/google/android/setupwizard/update/EarlyUpdateActivity$EarlyUpdateFragment;
.super Landroid/app/Fragment;
.source "EarlyUpdateActivity.java"

# interfaces
.implements Lcom/google/android/setupwizard/util/GooglePlaySetupHelper$GooglePlayConnectionListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/setupwizard/update/EarlyUpdateActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "EarlyUpdateFragment"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/setupwizard/update/EarlyUpdateActivity$EarlyUpdateFragment$EarlyUpdateTask;
    }
.end annotation


# instance fields
.field private mActivity:Lcom/google/android/setupwizard/update/EarlyUpdateActivity;

.field private mCompleted:Z

.field private mCurrentUpdate:Landroid/os/Bundle;

.field private final mPackageListener:Lcom/google/android/setupwizard/util/PackageMonitor$OnPackageStatusChangedListener;

.field private mPackageMonitor:Lcom/google/android/setupwizard/util/PackageMonitor;

.field private mPlayHelper:Lcom/google/android/setupwizard/util/GooglePlaySetupHelper;

.field private mState:I


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 191
    invoke-direct {p0}, Landroid/app/Fragment;-><init>()V

    .line 153
    iput v0, p0, Lcom/google/android/setupwizard/update/EarlyUpdateActivity$EarlyUpdateFragment;->mState:I

    .line 155
    iput-boolean v0, p0, Lcom/google/android/setupwizard/update/EarlyUpdateActivity$EarlyUpdateFragment;->mCompleted:Z

    .line 160
    new-instance v0, Lcom/google/android/setupwizard/update/EarlyUpdateActivity$EarlyUpdateFragment$1;

    invoke-direct {v0, p0}, Lcom/google/android/setupwizard/update/EarlyUpdateActivity$EarlyUpdateFragment$1;-><init>(Lcom/google/android/setupwizard/update/EarlyUpdateActivity$EarlyUpdateFragment;)V

    iput-object v0, p0, Lcom/google/android/setupwizard/update/EarlyUpdateActivity$EarlyUpdateFragment;->mPackageListener:Lcom/google/android/setupwizard/util/PackageMonitor$OnPackageStatusChangedListener;

    .line 192
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/google/android/setupwizard/update/EarlyUpdateActivity$EarlyUpdateFragment;->setRetainInstance(Z)V

    .line 193
    return-void
.end method

.method static synthetic access$100(Lcom/google/android/setupwizard/update/EarlyUpdateActivity$EarlyUpdateFragment;)I
    .locals 1
    .param p0, "x0"    # Lcom/google/android/setupwizard/update/EarlyUpdateActivity$EarlyUpdateFragment;

    .prologue
    .line 137
    iget v0, p0, Lcom/google/android/setupwizard/update/EarlyUpdateActivity$EarlyUpdateFragment;->mState:I

    return v0
.end method

.method static synthetic access$102(Lcom/google/android/setupwizard/update/EarlyUpdateActivity$EarlyUpdateFragment;I)I
    .locals 0
    .param p0, "x0"    # Lcom/google/android/setupwizard/update/EarlyUpdateActivity$EarlyUpdateFragment;
    .param p1, "x1"    # I

    .prologue
    .line 137
    iput p1, p0, Lcom/google/android/setupwizard/update/EarlyUpdateActivity$EarlyUpdateFragment;->mState:I

    return p1
.end method

.method static synthetic access$200(Lcom/google/android/setupwizard/update/EarlyUpdateActivity$EarlyUpdateFragment;)Landroid/os/Bundle;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/setupwizard/update/EarlyUpdateActivity$EarlyUpdateFragment;

    .prologue
    .line 137
    iget-object v0, p0, Lcom/google/android/setupwizard/update/EarlyUpdateActivity$EarlyUpdateFragment;->mCurrentUpdate:Landroid/os/Bundle;

    return-object v0
.end method

.method static synthetic access$202(Lcom/google/android/setupwizard/update/EarlyUpdateActivity$EarlyUpdateFragment;Landroid/os/Bundle;)Landroid/os/Bundle;
    .locals 0
    .param p0, "x0"    # Lcom/google/android/setupwizard/update/EarlyUpdateActivity$EarlyUpdateFragment;
    .param p1, "x1"    # Landroid/os/Bundle;

    .prologue
    .line 137
    iput-object p1, p0, Lcom/google/android/setupwizard/update/EarlyUpdateActivity$EarlyUpdateFragment;->mCurrentUpdate:Landroid/os/Bundle;

    return-object p1
.end method

.method static synthetic access$300(Lcom/google/android/setupwizard/update/EarlyUpdateActivity$EarlyUpdateFragment;)Lcom/google/android/setupwizard/update/EarlyUpdateActivity;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/setupwizard/update/EarlyUpdateActivity$EarlyUpdateFragment;

    .prologue
    .line 137
    iget-object v0, p0, Lcom/google/android/setupwizard/update/EarlyUpdateActivity$EarlyUpdateFragment;->mActivity:Lcom/google/android/setupwizard/update/EarlyUpdateActivity;

    return-object v0
.end method

.method static synthetic access$400(Lcom/google/android/setupwizard/update/EarlyUpdateActivity$EarlyUpdateFragment;Ljava/lang/String;)Z
    .locals 1
    .param p0, "x0"    # Lcom/google/android/setupwizard/update/EarlyUpdateActivity$EarlyUpdateFragment;
    .param p1, "x1"    # Ljava/lang/String;

    .prologue
    .line 137
    invoke-direct {p0, p1}, Lcom/google/android/setupwizard/update/EarlyUpdateActivity$EarlyUpdateFragment;->isCurrentPackage(Ljava/lang/String;)Z

    move-result v0

    return v0
.end method

.method static synthetic access$500(Lcom/google/android/setupwizard/update/EarlyUpdateActivity$EarlyUpdateFragment;)V
    .locals 0
    .param p0, "x0"    # Lcom/google/android/setupwizard/update/EarlyUpdateActivity$EarlyUpdateFragment;

    .prologue
    .line 137
    invoke-direct {p0}, Lcom/google/android/setupwizard/update/EarlyUpdateActivity$EarlyUpdateFragment;->requestNextUpdate()V

    return-void
.end method

.method static synthetic access$800(Lcom/google/android/setupwizard/update/EarlyUpdateActivity$EarlyUpdateFragment;)Lcom/google/android/setupwizard/util/GooglePlaySetupHelper;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/setupwizard/update/EarlyUpdateActivity$EarlyUpdateFragment;

    .prologue
    .line 137
    iget-object v0, p0, Lcom/google/android/setupwizard/update/EarlyUpdateActivity$EarlyUpdateFragment;->mPlayHelper:Lcom/google/android/setupwizard/util/GooglePlaySetupHelper;

    return-object v0
.end method

.method public static attach(Landroid/app/FragmentManager;)V
    .locals 3
    .param p0, "fm"    # Landroid/app/FragmentManager;

    .prologue
    .line 145
    const-string v0, "earlyUpdate"

    invoke-virtual {p0, v0}, Landroid/app/FragmentManager;->findFragmentByTag(Ljava/lang/String;)Landroid/app/Fragment;

    move-result-object v0

    if-nez v0, :cond_0

    .line 146
    invoke-virtual {p0}, Landroid/app/FragmentManager;->beginTransaction()Landroid/app/FragmentTransaction;

    move-result-object v0

    new-instance v1, Lcom/google/android/setupwizard/update/EarlyUpdateActivity$EarlyUpdateFragment;

    invoke-direct {v1}, Lcom/google/android/setupwizard/update/EarlyUpdateActivity$EarlyUpdateFragment;-><init>()V

    const-string v2, "earlyUpdate"

    invoke-virtual {v0, v1, v2}, Landroid/app/FragmentTransaction;->add(Landroid/app/Fragment;Ljava/lang/String;)Landroid/app/FragmentTransaction;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/FragmentTransaction;->commit()I

    .line 148
    :cond_0
    return-void
.end method

.method private isCurrentPackage(Ljava/lang/String;)Z
    .locals 5
    .param p1, "packageName"    # Ljava/lang/String;

    .prologue
    .line 244
    iget-object v2, p0, Lcom/google/android/setupwizard/update/EarlyUpdateActivity$EarlyUpdateFragment;->mCurrentUpdate:Landroid/os/Bundle;

    if-eqz v2, :cond_1

    iget-object v2, p0, Lcom/google/android/setupwizard/update/EarlyUpdateActivity$EarlyUpdateFragment;->mCurrentUpdate:Landroid/os/Bundle;

    const-string v3, "package_name"

    invoke-virtual {v2, v3}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 246
    .local v1, "currentPackageName":Ljava/lang/String;
    :goto_0
    if-eqz p1, :cond_2

    if-eqz v1, :cond_2

    invoke-virtual {p1, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    const/4 v0, 0x1

    .line 248
    .local v0, "current":Z
    :goto_1
    if-nez v0, :cond_0

    .line 249
    const-string v2, "SetupWizard.EarlyUpdate"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "isCurrentPackage FAIL packageName="

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " currentPackageName="

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 252
    :cond_0
    return v0

    .line 244
    .end local v0    # "current":Z
    .end local v1    # "currentPackageName":Ljava/lang/String;
    :cond_1
    const/4 v1, 0x0

    goto :goto_0

    .line 246
    .restart local v1    # "currentPackageName":Ljava/lang/String;
    :cond_2
    const/4 v0, 0x0

    goto :goto_1
.end method

.method private requestNextUpdate()V
    .locals 3

    .prologue
    .line 236
    iget-object v0, p0, Lcom/google/android/setupwizard/update/EarlyUpdateActivity$EarlyUpdateFragment;->mCurrentUpdate:Landroid/os/Bundle;

    if-nez v0, :cond_0

    .line 237
    new-instance v0, Lcom/google/android/setupwizard/update/EarlyUpdateActivity$EarlyUpdateFragment$EarlyUpdateTask;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/google/android/setupwizard/update/EarlyUpdateActivity$EarlyUpdateFragment$EarlyUpdateTask;-><init>(Lcom/google/android/setupwizard/update/EarlyUpdateActivity$EarlyUpdateFragment;Lcom/google/android/setupwizard/update/EarlyUpdateActivity$1;)V

    const/4 v1, 0x0

    new-array v1, v1, [Ljava/lang/Void;

    invoke-virtual {v0, v1}, Lcom/google/android/setupwizard/update/EarlyUpdateActivity$EarlyUpdateFragment$EarlyUpdateTask;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    .line 241
    :goto_0
    return-void

    .line 239
    :cond_0
    const-string v0, "SetupWizard.EarlyUpdate"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Early update is in progress for another package: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/setupwizard/update/EarlyUpdateActivity$EarlyUpdateFragment;->mCurrentUpdate:Landroid/os/Bundle;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method


# virtual methods
.method public finish(I)V
    .locals 1
    .param p1, "resultCode"    # I

    .prologue
    .line 256
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/setupwizard/update/EarlyUpdateActivity$EarlyUpdateFragment;->mCompleted:Z

    .line 257
    iget-object v0, p0, Lcom/google/android/setupwizard/update/EarlyUpdateActivity$EarlyUpdateFragment;->mActivity:Lcom/google/android/setupwizard/update/EarlyUpdateActivity;

    # invokes: Lcom/google/android/setupwizard/update/EarlyUpdateActivity;->delayedFinish(I)V
    invoke-static {v0, p1}, Lcom/google/android/setupwizard/update/EarlyUpdateActivity;->access$700(Lcom/google/android/setupwizard/update/EarlyUpdateActivity;I)V

    .line 258
    return-void
.end method

.method public onActivityCreated(Landroid/os/Bundle;)V
    .locals 3
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 203
    invoke-super {p0, p1}, Landroid/app/Fragment;->onActivityCreated(Landroid/os/Bundle;)V

    .line 204
    iget-object v0, p0, Lcom/google/android/setupwizard/update/EarlyUpdateActivity$EarlyUpdateFragment;->mActivity:Lcom/google/android/setupwizard/update/EarlyUpdateActivity;

    iget v1, p0, Lcom/google/android/setupwizard/update/EarlyUpdateActivity$EarlyUpdateFragment;->mState:I

    iget-object v2, p0, Lcom/google/android/setupwizard/update/EarlyUpdateActivity$EarlyUpdateFragment;->mCurrentUpdate:Landroid/os/Bundle;

    invoke-virtual {v0, v1, v2}, Lcom/google/android/setupwizard/update/EarlyUpdateActivity;->updateState(ILandroid/os/Bundle;)V

    .line 205
    return-void
.end method

.method public onAttach(Landroid/app/Activity;)V
    .locals 0
    .param p1, "activity"    # Landroid/app/Activity;

    .prologue
    .line 197
    invoke-super {p0, p1}, Landroid/app/Fragment;->onAttach(Landroid/app/Activity;)V

    .line 198
    check-cast p1, Lcom/google/android/setupwizard/update/EarlyUpdateActivity;

    .end local p1    # "activity":Landroid/app/Activity;
    iput-object p1, p0, Lcom/google/android/setupwizard/update/EarlyUpdateActivity$EarlyUpdateFragment;->mActivity:Lcom/google/android/setupwizard/update/EarlyUpdateActivity;

    .line 199
    return-void
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 2
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 209
    invoke-super {p0, p1}, Landroid/app/Fragment;->onCreate(Landroid/os/Bundle;)V

    .line 210
    new-instance v0, Lcom/google/android/setupwizard/util/PackageMonitor;

    iget-object v1, p0, Lcom/google/android/setupwizard/update/EarlyUpdateActivity$EarlyUpdateFragment;->mActivity:Lcom/google/android/setupwizard/update/EarlyUpdateActivity;

    invoke-direct {v0, v1}, Lcom/google/android/setupwizard/util/PackageMonitor;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/google/android/setupwizard/update/EarlyUpdateActivity$EarlyUpdateFragment;->mPackageMonitor:Lcom/google/android/setupwizard/util/PackageMonitor;

    .line 211
    iget-object v0, p0, Lcom/google/android/setupwizard/update/EarlyUpdateActivity$EarlyUpdateFragment;->mPackageMonitor:Lcom/google/android/setupwizard/util/PackageMonitor;

    iget-object v1, p0, Lcom/google/android/setupwizard/update/EarlyUpdateActivity$EarlyUpdateFragment;->mPackageListener:Lcom/google/android/setupwizard/util/PackageMonitor$OnPackageStatusChangedListener;

    invoke-virtual {v0, v1}, Lcom/google/android/setupwizard/util/PackageMonitor;->setOnPackageStatusChangedListener(Lcom/google/android/setupwizard/util/PackageMonitor$OnPackageStatusChangedListener;)V

    .line 213
    iget-object v0, p0, Lcom/google/android/setupwizard/update/EarlyUpdateActivity$EarlyUpdateFragment;->mActivity:Lcom/google/android/setupwizard/update/EarlyUpdateActivity;

    invoke-virtual {v0}, Lcom/google/android/setupwizard/update/EarlyUpdateActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0, p0}, Lcom/google/android/setupwizard/util/GooglePlaySetupHelper;->connect(Landroid/content/Context;Lcom/google/android/setupwizard/util/GooglePlaySetupHelper$GooglePlayConnectionListener;)Lcom/google/android/setupwizard/util/GooglePlaySetupHelper;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/setupwizard/update/EarlyUpdateActivity$EarlyUpdateFragment;->mPlayHelper:Lcom/google/android/setupwizard/util/GooglePlaySetupHelper;

    .line 214
    return-void
.end method

.method public onDestroy()V
    .locals 2

    .prologue
    .line 218
    iget-object v0, p0, Lcom/google/android/setupwizard/update/EarlyUpdateActivity$EarlyUpdateFragment;->mPackageMonitor:Lcom/google/android/setupwizard/util/PackageMonitor;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/google/android/setupwizard/util/PackageMonitor;->setOnPackageStatusChangedListener(Lcom/google/android/setupwizard/util/PackageMonitor$OnPackageStatusChangedListener;)V

    .line 219
    iget-object v0, p0, Lcom/google/android/setupwizard/update/EarlyUpdateActivity$EarlyUpdateFragment;->mPlayHelper:Lcom/google/android/setupwizard/util/GooglePlaySetupHelper;

    invoke-virtual {v0}, Lcom/google/android/setupwizard/util/GooglePlaySetupHelper;->disconnect()V

    .line 220
    invoke-super {p0}, Landroid/app/Fragment;->onDestroy()V

    .line 221
    return-void
.end method

.method public onGooglePlayConnected(Lcom/google/android/setupwizard/util/GooglePlaySetupHelper;)V
    .locals 2
    .param p1, "helper"    # Lcom/google/android/setupwizard/util/GooglePlaySetupHelper;

    .prologue
    .line 225
    iget-boolean v0, p0, Lcom/google/android/setupwizard/update/EarlyUpdateActivity$EarlyUpdateFragment;->mCompleted:Z

    if-nez v0, :cond_0

    .line 226
    invoke-direct {p0}, Lcom/google/android/setupwizard/update/EarlyUpdateActivity$EarlyUpdateFragment;->requestNextUpdate()V

    .line 230
    :goto_0
    return-void

    .line 228
    :cond_0
    const-string v0, "SetupWizard.EarlyUpdate"

    const-string v1, "Fragment is being removed, do not request another update."

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method
