.class public Lcom/google/android/setupwizard/account/GoogleNowWrapper;
.super Lcom/google/android/setupwizard/SubactivityWrapper;
.source "GoogleNowWrapper.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 29
    invoke-direct {p0}, Lcom/google/android/setupwizard/SubactivityWrapper;-><init>()V

    return-void
.end method


# virtual methods
.method protected onSubactivityLaunch()Landroid/content/Intent;
    .locals 2

    .prologue
    .line 40
    new-instance v0, Landroid/content/Intent;

    const-string v1, "com.google.android.apps.now.OPT_IN_WIZARD"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    return-object v0
.end method

.method protected onSubactivityResult(IILandroid/content/Intent;)V
    .locals 3
    .param p1, "requestCode"    # I
    .param p2, "resultCode"    # I
    .param p3, "data"    # Landroid/content/Intent;

    .prologue
    .line 47
    packed-switch p2, :pswitch_data_0

    .line 60
    :pswitch_0
    iget-object v0, p0, Lcom/google/android/setupwizard/account/GoogleNowWrapper;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Unknown resultCode "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 65
    :goto_0
    invoke-super {p0, p1, p2, p3}, Lcom/google/android/setupwizard/SubactivityWrapper;->onSubactivityResult(IILandroid/content/Intent;)V

    .line 66
    return-void

    .line 52
    :pswitch_1
    const/4 p2, 0x0

    .line 53
    goto :goto_0

    .line 57
    :pswitch_2
    const/4 p2, -0x1

    .line 58
    goto :goto_0

    .line 47
    nop

    :pswitch_data_0
    .packed-switch -0x1
        :pswitch_2
        :pswitch_1
        :pswitch_0
        :pswitch_0
        :pswitch_1
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_2
    .end packed-switch
.end method

.method protected requestCode()I
    .locals 1

    .prologue
    .line 35
    const/16 v0, 0x2728

    return v0
.end method
