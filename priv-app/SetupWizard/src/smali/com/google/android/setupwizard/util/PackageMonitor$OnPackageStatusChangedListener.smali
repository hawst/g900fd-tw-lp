.class public interface abstract Lcom/google/android/setupwizard/util/PackageMonitor$OnPackageStatusChangedListener;
.super Ljava/lang/Object;
.source "PackageMonitor.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/setupwizard/util/PackageMonitor;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "OnPackageStatusChangedListener"
.end annotation


# virtual methods
.method public abstract onPackageDequeued(Ljava/lang/String;Z)V
.end method

.method public abstract onPackageEnqueued(Ljava/lang/String;)V
.end method

.method public abstract onPackageInstalling(Ljava/lang/String;I)V
.end method
