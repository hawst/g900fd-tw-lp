.class public Lcom/google/android/setupwizard/edu/ChooseLockPinActivity;
.super Lcom/google/android/setupwizard/BaseActivity;
.source "ChooseLockPinActivity.java"

# interfaces
.implements Landroid/text/TextWatcher;
.implements Landroid/widget/TextView$OnEditorActionListener;


# instance fields
.field private mFirstPin:Ljava/lang/String;

.field private final mHandler:Landroid/os/Handler;

.field private mHeaderText:Landroid/widget/TextView;

.field private mPinEntry:Landroid/widget/TextView;

.field private final mPinMaxLength:I

.field private final mPinMinLength:I

.field private mUiStage:I


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 56
    invoke-direct {p0}, Lcom/google/android/setupwizard/BaseActivity;-><init>()V

    .line 66
    const/4 v0, 0x4

    iput v0, p0, Lcom/google/android/setupwizard/edu/ChooseLockPinActivity;->mPinMinLength:I

    .line 67
    const/16 v0, 0x10

    iput v0, p0, Lcom/google/android/setupwizard/edu/ChooseLockPinActivity;->mPinMaxLength:I

    .line 68
    const/4 v0, 0x1

    iput v0, p0, Lcom/google/android/setupwizard/edu/ChooseLockPinActivity;->mUiStage:I

    .line 74
    new-instance v0, Lcom/google/android/setupwizard/edu/ChooseLockPinActivity$1;

    invoke-direct {v0, p0}, Lcom/google/android/setupwizard/edu/ChooseLockPinActivity$1;-><init>(Lcom/google/android/setupwizard/edu/ChooseLockPinActivity;)V

    iput-object v0, p0, Lcom/google/android/setupwizard/edu/ChooseLockPinActivity;->mHandler:Landroid/os/Handler;

    return-void
.end method

.method private handleNext()V
    .locals 6

    .prologue
    const/4 v5, 0x2

    .line 210
    iget-object v3, p0, Lcom/google/android/setupwizard/edu/ChooseLockPinActivity;->mPinEntry:Landroid/widget/TextView;

    invoke-virtual {v3}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v3

    invoke-interface {v3}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v1

    .line 211
    .local v1, "pin":Ljava/lang/String;
    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 238
    :cond_0
    :goto_0
    return-void

    .line 214
    :cond_1
    const/4 v0, 0x0

    .line 215
    .local v0, "errorMsg":Ljava/lang/String;
    iget v3, p0, Lcom/google/android/setupwizard/edu/ChooseLockPinActivity;->mUiStage:I

    const/4 v4, 0x1

    if-ne v3, v4, :cond_3

    .line 216
    invoke-direct {p0, v1}, Lcom/google/android/setupwizard/edu/ChooseLockPinActivity;->validatePin(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 217
    if-nez v0, :cond_2

    .line 218
    iput-object v1, p0, Lcom/google/android/setupwizard/edu/ChooseLockPinActivity;->mFirstPin:Ljava/lang/String;

    .line 219
    iget-object v3, p0, Lcom/google/android/setupwizard/edu/ChooseLockPinActivity;->mPinEntry:Landroid/widget/TextView;

    const-string v4, ""

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 220
    invoke-virtual {p0, v5}, Lcom/google/android/setupwizard/edu/ChooseLockPinActivity;->updateStage(I)V

    .line 235
    :cond_2
    :goto_1
    if-eqz v0, :cond_0

    .line 236
    iget v3, p0, Lcom/google/android/setupwizard/edu/ChooseLockPinActivity;->mUiStage:I

    invoke-direct {p0, v0, v3}, Lcom/google/android/setupwizard/edu/ChooseLockPinActivity;->showError(Ljava/lang/String;I)V

    goto :goto_0

    .line 222
    :cond_3
    iget v3, p0, Lcom/google/android/setupwizard/edu/ChooseLockPinActivity;->mUiStage:I

    if-ne v3, v5, :cond_2

    .line 223
    iget-object v3, p0, Lcom/google/android/setupwizard/edu/ChooseLockPinActivity;->mFirstPin:Ljava/lang/String;

    invoke-virtual {v3, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_4

    .line 224
    invoke-direct {p0, v1}, Lcom/google/android/setupwizard/edu/ChooseLockPinActivity;->saveLockPin(Ljava/lang/String;)V

    .line 226
    const/4 v3, -0x1

    invoke-virtual {p0, v3}, Lcom/google/android/setupwizard/edu/ChooseLockPinActivity;->nextScreen(I)V

    goto :goto_1

    .line 228
    :cond_4
    iget-object v3, p0, Lcom/google/android/setupwizard/edu/ChooseLockPinActivity;->mPinEntry:Landroid/widget/TextView;

    invoke-virtual {v3}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v2

    .line 229
    .local v2, "tmp":Ljava/lang/CharSequence;
    if-eqz v2, :cond_5

    move-object v3, v2

    .line 230
    check-cast v3, Landroid/text/Spannable;

    const/4 v4, 0x0

    invoke-interface {v2}, Ljava/lang/CharSequence;->length()I

    move-result v5

    invoke-static {v3, v4, v5}, Landroid/text/Selection;->setSelection(Landroid/text/Spannable;II)V

    .line 232
    :cond_5
    const/4 v3, 0x3

    invoke-virtual {p0, v3}, Lcom/google/android/setupwizard/edu/ChooseLockPinActivity;->updateStage(I)V

    goto :goto_1
.end method

.method private saveLockPin(Ljava/lang/String;)V
    .locals 4
    .param p1, "pin"    # Ljava/lang/String;

    .prologue
    .line 241
    new-instance v0, Landroid/content/ComponentName;

    invoke-virtual {p0}, Lcom/google/android/setupwizard/edu/ChooseLockPinActivity;->getBaseContext()Landroid/content/Context;

    move-result-object v2

    const-class v3, Lcom/google/android/setupwizard/util/SetupWizardAdminReceiver;

    invoke-direct {v0, v2, v3}, Landroid/content/ComponentName;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 243
    .local v0, "component":Landroid/content/ComponentName;
    invoke-virtual {p0}, Lcom/google/android/setupwizard/edu/ChooseLockPinActivity;->getBaseContext()Landroid/content/Context;

    move-result-object v2

    const-string v3, "device_policy"

    invoke-virtual {v2, v3}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/app/admin/DevicePolicyManager;

    .line 247
    .local v1, "dpm":Landroid/app/admin/DevicePolicyManager;
    const/4 v2, 0x1

    invoke-virtual {v1, v0, v2}, Landroid/app/admin/DevicePolicyManager;->setActiveAdmin(Landroid/content/ComponentName;Z)V

    .line 248
    const/4 v2, 0x0

    invoke-virtual {v1, p1, v2}, Landroid/app/admin/DevicePolicyManager;->resetPassword(Ljava/lang/String;I)Z

    .line 249
    invoke-virtual {v1, v0}, Landroid/app/admin/DevicePolicyManager;->removeActiveAdmin(Landroid/content/ComponentName;)V

    .line 250
    return-void
.end method

.method private showError(Ljava/lang/String;I)V
    .locals 4
    .param p1, "msg"    # Ljava/lang/String;
    .param p2, "nextStage"    # I

    .prologue
    const/4 v3, 0x1

    .line 253
    iget-object v1, p0, Lcom/google/android/setupwizard/edu/ChooseLockPinActivity;->mHeaderText:Landroid/widget/TextView;

    invoke-virtual {v1, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 254
    iget-object v1, p0, Lcom/google/android/setupwizard/edu/ChooseLockPinActivity;->mHeaderText:Landroid/widget/TextView;

    iget-object v2, p0, Lcom/google/android/setupwizard/edu/ChooseLockPinActivity;->mHeaderText:Landroid/widget/TextView;

    invoke-virtual {v2}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->announceForAccessibility(Ljava/lang/CharSequence;)V

    .line 255
    iget-object v1, p0, Lcom/google/android/setupwizard/edu/ChooseLockPinActivity;->mHandler:Landroid/os/Handler;

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v1, v3, v2}, Landroid/os/Handler;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v0

    .line 256
    .local v0, "mesg":Landroid/os/Message;
    iget-object v1, p0, Lcom/google/android/setupwizard/edu/ChooseLockPinActivity;->mHandler:Landroid/os/Handler;

    invoke-virtual {v1, v3}, Landroid/os/Handler;->removeMessages(I)V

    .line 257
    iget-object v1, p0, Lcom/google/android/setupwizard/edu/ChooseLockPinActivity;->mHandler:Landroid/os/Handler;

    const-wide/16 v2, 0xbb8

    invoke-virtual {v1, v0, v2, v3}, Landroid/os/Handler;->sendMessageDelayed(Landroid/os/Message;J)Z

    .line 258
    return-void
.end method

.method private skip()V
    .locals 1

    .prologue
    .line 206
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/google/android/setupwizard/edu/ChooseLockPinActivity;->nextScreen(I)V

    .line 207
    return-void
.end method

.method private updateUi()V
    .locals 8

    .prologue
    const v6, 0x7f0700b3

    const/4 v7, 0x4

    const/4 v5, 0x1

    .line 276
    iget-object v4, p0, Lcom/google/android/setupwizard/edu/ChooseLockPinActivity;->mPinEntry:Landroid/widget/TextView;

    invoke-virtual {v4}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v4

    invoke-interface {v4}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v3

    .line 277
    .local v3, "pin":Ljava/lang/String;
    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v1

    .line 278
    .local v1, "length":I
    iget v4, p0, Lcom/google/android/setupwizard/edu/ChooseLockPinActivity;->mUiStage:I

    if-ne v4, v5, :cond_2

    if-lez v1, :cond_2

    .line 279
    if-ge v1, v7, :cond_0

    .line 280
    const v4, 0x7f0700b7

    new-array v5, v5, [Ljava/lang/Object;

    const/4 v6, 0x0

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    aput-object v7, v5, v6

    invoke-virtual {p0, v4, v5}, Lcom/google/android/setupwizard/edu/ChooseLockPinActivity;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    .line 281
    .local v2, "msg":Ljava/lang/String;
    iget-object v4, p0, Lcom/google/android/setupwizard/edu/ChooseLockPinActivity;->mHeaderText:Landroid/widget/TextView;

    invoke-virtual {v4, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 305
    .end local v2    # "msg":Ljava/lang/String;
    :goto_0
    return-void

    .line 283
    :cond_0
    invoke-direct {p0, v3}, Lcom/google/android/setupwizard/edu/ChooseLockPinActivity;->validatePin(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 284
    .local v0, "error":Ljava/lang/String;
    if-eqz v0, :cond_1

    .line 285
    iget-object v4, p0, Lcom/google/android/setupwizard/edu/ChooseLockPinActivity;->mHeaderText:Landroid/widget/TextView;

    invoke-virtual {v4, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0

    .line 287
    :cond_1
    iget-object v4, p0, Lcom/google/android/setupwizard/edu/ChooseLockPinActivity;->mHeaderText:Landroid/widget/TextView;

    invoke-virtual {v4, v6}, Landroid/widget/TextView;->setText(I)V

    goto :goto_0

    .line 291
    .end local v0    # "error":Ljava/lang/String;
    :cond_2
    iget v4, p0, Lcom/google/android/setupwizard/edu/ChooseLockPinActivity;->mUiStage:I

    packed-switch v4, :pswitch_data_0

    goto :goto_0

    .line 293
    :pswitch_0
    iget-object v4, p0, Lcom/google/android/setupwizard/edu/ChooseLockPinActivity;->mHeaderText:Landroid/widget/TextView;

    invoke-virtual {v4, v6}, Landroid/widget/TextView;->setText(I)V

    goto :goto_0

    .line 296
    :pswitch_1
    iget-object v4, p0, Lcom/google/android/setupwizard/edu/ChooseLockPinActivity;->mHeaderText:Landroid/widget/TextView;

    const v5, 0x7f0700b2

    invoke-virtual {v4, v5}, Landroid/widget/TextView;->setText(I)V

    goto :goto_0

    .line 299
    :pswitch_2
    iget-object v4, p0, Lcom/google/android/setupwizard/edu/ChooseLockPinActivity;->mHeaderText:Landroid/widget/TextView;

    const v5, 0x7f0700bb

    invoke-virtual {v4, v5}, Landroid/widget/TextView;->setText(I)V

    goto :goto_0

    .line 291
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method private validatePin(Ljava/lang/String;)Ljava/lang/String;
    .locals 8
    .param p1, "pin"    # Ljava/lang/String;

    .prologue
    const v7, 0x7f0700ba

    const/4 v6, 0x4

    const/4 v4, 0x1

    const/4 v5, 0x0

    .line 149
    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v2

    if-ge v2, v6, :cond_0

    .line 150
    const v2, 0x7f0700b7

    new-array v3, v4, [Ljava/lang/Object;

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v3, v5

    invoke-virtual {p0, v2, v3}, Lcom/google/android/setupwizard/edu/ChooseLockPinActivity;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    .line 171
    :goto_0
    return-object v2

    .line 152
    :cond_0
    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v2

    const/16 v3, 0x10

    if-le v2, v3, :cond_1

    .line 153
    const v2, 0x7f0700b8

    new-array v3, v4, [Ljava/lang/Object;

    const/16 v4, 0x11

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v3, v5

    invoke-virtual {p0, v2, v3}, Lcom/google/android/setupwizard/edu/ChooseLockPinActivity;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    goto :goto_0

    .line 156
    :cond_1
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_1
    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v2

    if-ge v1, v2, :cond_8

    .line 157
    invoke-virtual {p1, v1}, Ljava/lang/String;->charAt(I)C

    move-result v0

    .line 159
    .local v0, "c":C
    const/16 v2, 0x20

    if-lt v0, v2, :cond_2

    const/16 v2, 0x7f

    if-le v0, v2, :cond_3

    .line 160
    :cond_2
    const v2, 0x7f0700b9

    invoke-virtual {p0, v2}, Lcom/google/android/setupwizard/edu/ChooseLockPinActivity;->getString(I)Ljava/lang/String;

    move-result-object v2

    goto :goto_0

    .line 162
    :cond_3
    const/16 v2, 0x30

    if-lt v0, v2, :cond_4

    const/16 v2, 0x39

    if-gt v0, v2, :cond_4

    .line 156
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 164
    :cond_4
    const/16 v2, 0x41

    if-lt v0, v2, :cond_5

    const/16 v2, 0x5a

    if-le v0, v2, :cond_6

    :cond_5
    const/16 v2, 0x61

    if-lt v0, v2, :cond_7

    const/16 v2, 0x7a

    if-gt v0, v2, :cond_7

    .line 165
    :cond_6
    invoke-virtual {p0, v7}, Lcom/google/android/setupwizard/edu/ChooseLockPinActivity;->getString(I)Ljava/lang/String;

    move-result-object v2

    goto :goto_0

    .line 167
    :cond_7
    invoke-virtual {p0, v7}, Lcom/google/android/setupwizard/edu/ChooseLockPinActivity;->getString(I)Ljava/lang/String;

    move-result-object v2

    goto :goto_0

    .line 171
    .end local v0    # "c":C
    :cond_8
    const/4 v2, 0x0

    goto :goto_0
.end method


# virtual methods
.method public afterTextChanged(Landroid/text/Editable;)V
    .locals 2
    .param p1, "s"    # Landroid/text/Editable;

    .prologue
    .line 318
    iget v0, p0, Lcom/google/android/setupwizard/edu/ChooseLockPinActivity;->mUiStage:I

    const/4 v1, 0x3

    if-ne v0, v1, :cond_0

    .line 319
    const/4 v0, 0x2

    invoke-virtual {p0, v0}, Lcom/google/android/setupwizard/edu/ChooseLockPinActivity;->updateStage(I)V

    .line 321
    :cond_0
    return-void
.end method

.method public beforeTextChanged(Ljava/lang/CharSequence;III)V
    .locals 0
    .param p1, "s"    # Ljava/lang/CharSequence;
    .param p2, "start"    # I
    .param p3, "count"    # I
    .param p4, "after"    # I

    .prologue
    .line 326
    return-void
.end method

.method public onBackPressed()V
    .locals 0

    .prologue
    .line 203
    return-void
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 4
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    const/4 v3, 0x1

    .line 85
    invoke-virtual {p0}, Lcom/google/android/setupwizard/edu/ChooseLockPinActivity;->getShowLockPinScreen()Z

    move-result v1

    if-nez v1, :cond_0

    .line 86
    invoke-virtual {p0, v3}, Lcom/google/android/setupwizard/edu/ChooseLockPinActivity;->finishAction(I)V

    .line 89
    :cond_0
    invoke-super {p0, p1}, Lcom/google/android/setupwizard/BaseActivity;->onCreate(Landroid/os/Bundle;)V

    .line 90
    const v1, 0x7f030004

    const v2, 0x7f030018

    invoke-virtual {p0, v1, v2}, Lcom/google/android/setupwizard/edu/ChooseLockPinActivity;->setTemplateContent(II)V

    .line 92
    const v1, 0x7f0e003e

    invoke-virtual {p0, v1}, Lcom/google/android/setupwizard/edu/ChooseLockPinActivity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    iput-object v1, p0, Lcom/google/android/setupwizard/edu/ChooseLockPinActivity;->mPinEntry:Landroid/widget/TextView;

    .line 93
    iget-object v1, p0, Lcom/google/android/setupwizard/edu/ChooseLockPinActivity;->mPinEntry:Landroid/widget/TextView;

    invoke-virtual {v1, p0}, Landroid/widget/TextView;->setOnEditorActionListener(Landroid/widget/TextView$OnEditorActionListener;)V

    .line 94
    iget-object v1, p0, Lcom/google/android/setupwizard/edu/ChooseLockPinActivity;->mPinEntry:Landroid/widget/TextView;

    invoke-virtual {v1, p0}, Landroid/widget/TextView;->addTextChangedListener(Landroid/text/TextWatcher;)V

    .line 96
    invoke-virtual {p0}, Lcom/google/android/setupwizard/edu/ChooseLockPinActivity;->showSoftKeyboard()V

    .line 97
    const v1, 0x7f0e003d

    invoke-virtual {p0, v1}, Lcom/google/android/setupwizard/edu/ChooseLockPinActivity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    iput-object v1, p0, Lcom/google/android/setupwizard/edu/ChooseLockPinActivity;->mHeaderText:Landroid/widget/TextView;

    .line 99
    if-nez p1, :cond_2

    .line 100
    invoke-virtual {p0, v3}, Lcom/google/android/setupwizard/edu/ChooseLockPinActivity;->updateStage(I)V

    .line 109
    :cond_1
    :goto_0
    return-void

    .line 102
    :cond_2
    const-string v1, "first_pin"

    invoke-virtual {p1, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/setupwizard/edu/ChooseLockPinActivity;->mFirstPin:Ljava/lang/String;

    .line 103
    const-string v1, "ui_stage"

    invoke-virtual {p1, v1}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v0

    .line 104
    .local v0, "stage":I
    if-eqz v0, :cond_1

    .line 105
    iput v0, p0, Lcom/google/android/setupwizard/edu/ChooseLockPinActivity;->mUiStage:I

    .line 106
    invoke-virtual {p0, v0}, Lcom/google/android/setupwizard/edu/ChooseLockPinActivity;->updateStage(I)V

    goto :goto_0
.end method

.method public onEditorAction(Landroid/widget/TextView;ILandroid/view/KeyEvent;)Z
    .locals 1
    .param p1, "v"    # Landroid/widget/TextView;
    .param p2, "actionId"    # I
    .param p3, "event"    # Landroid/view/KeyEvent;

    .prologue
    .line 263
    if-eqz p2, :cond_0

    const/4 v0, 0x6

    if-eq p2, v0, :cond_0

    const/4 v0, 0x5

    if-ne p2, v0, :cond_1

    .line 266
    :cond_0
    invoke-direct {p0}, Lcom/google/android/setupwizard/edu/ChooseLockPinActivity;->handleNext()V

    .line 267
    const/4 v0, 0x1

    .line 269
    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public onNavigateBack()V
    .locals 2

    .prologue
    .line 196
    iget-object v0, p0, Lcom/google/android/setupwizard/edu/ChooseLockPinActivity;->mPinEntry:Landroid/widget/TextView;

    const-string v1, ""

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 197
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/google/android/setupwizard/edu/ChooseLockPinActivity;->updateStage(I)V

    .line 198
    return-void
.end method

.method public onNavigateNext()V
    .locals 0

    .prologue
    .line 189
    invoke-direct {p0}, Lcom/google/android/setupwizard/edu/ChooseLockPinActivity;->skip()V

    .line 190
    return-void
.end method

.method public onNavigationBarCreated(Lcom/android/setupwizard/navigationbar/SetupWizardNavBar;)V
    .locals 2
    .param p1, "bar"    # Lcom/android/setupwizard/navigationbar/SetupWizardNavBar;

    .prologue
    .line 176
    invoke-super {p0, p1}, Lcom/google/android/setupwizard/BaseActivity;->onNavigationBarCreated(Lcom/android/setupwizard/navigationbar/SetupWizardNavBar;)V

    .line 179
    iget-object v0, p0, Lcom/google/android/setupwizard/edu/ChooseLockPinActivity;->mNavigationBar:Lcom/android/setupwizard/navigationbar/SetupWizardNavBar;

    invoke-virtual {v0}, Lcom/android/setupwizard/navigationbar/SetupWizardNavBar;->getNextButton()Landroid/widget/Button;

    move-result-object v0

    const v1, 0x7f070046

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setText(I)V

    .line 181
    invoke-virtual {p0}, Lcom/google/android/setupwizard/edu/ChooseLockPinActivity;->getRequireLockPin()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 183
    iget-object v0, p0, Lcom/google/android/setupwizard/edu/ChooseLockPinActivity;->mNavigationBar:Lcom/android/setupwizard/navigationbar/SetupWizardNavBar;

    invoke-virtual {v0}, Lcom/android/setupwizard/navigationbar/SetupWizardNavBar;->getNextButton()Landroid/widget/Button;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setEnabled(Z)V

    .line 185
    :cond_0
    return-void
.end method

.method public onPause()V
    .locals 2

    .prologue
    .line 120
    iget-object v0, p0, Lcom/google/android/setupwizard/edu/ChooseLockPinActivity;->mHandler:Landroid/os/Handler;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeMessages(I)V

    .line 121
    invoke-super {p0}, Lcom/google/android/setupwizard/BaseActivity;->onPause()V

    .line 122
    return-void
.end method

.method public onResume()V
    .locals 1

    .prologue
    .line 113
    invoke-super {p0}, Lcom/google/android/setupwizard/BaseActivity;->onResume()V

    .line 114
    iget v0, p0, Lcom/google/android/setupwizard/edu/ChooseLockPinActivity;->mUiStage:I

    invoke-virtual {p0, v0}, Lcom/google/android/setupwizard/edu/ChooseLockPinActivity;->updateStage(I)V

    .line 115
    invoke-virtual {p0}, Lcom/google/android/setupwizard/edu/ChooseLockPinActivity;->showSoftKeyboard()V

    .line 116
    return-void
.end method

.method public onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 2
    .param p1, "outState"    # Landroid/os/Bundle;

    .prologue
    .line 126
    invoke-super {p0, p1}, Lcom/google/android/setupwizard/BaseActivity;->onSaveInstanceState(Landroid/os/Bundle;)V

    .line 127
    const-string v0, "ui_stage"

    iget v1, p0, Lcom/google/android/setupwizard/edu/ChooseLockPinActivity;->mUiStage:I

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 128
    const-string v0, "first_pin"

    iget-object v1, p0, Lcom/google/android/setupwizard/edu/ChooseLockPinActivity;->mFirstPin:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 129
    return-void
.end method

.method public onTextChanged(Ljava/lang/CharSequence;III)V
    .locals 0
    .param p1, "s"    # Ljava/lang/CharSequence;
    .param p2, "start"    # I
    .param p3, "before"    # I
    .param p4, "count"    # I

    .prologue
    .line 331
    return-void
.end method

.method public showSoftKeyboard()V
    .locals 3

    .prologue
    .line 308
    iget-object v1, p0, Lcom/google/android/setupwizard/edu/ChooseLockPinActivity;->mPinEntry:Landroid/widget/TextView;

    invoke-virtual {v1}, Landroid/widget/TextView;->requestFocus()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 309
    const-string v1, "input_method"

    invoke-virtual {p0, v1}, Lcom/google/android/setupwizard/edu/ChooseLockPinActivity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/inputmethod/InputMethodManager;

    .line 311
    .local v0, "imm":Landroid/view/inputmethod/InputMethodManager;
    iget-object v1, p0, Lcom/google/android/setupwizard/edu/ChooseLockPinActivity;->mPinEntry:Landroid/widget/TextView;

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Landroid/view/inputmethod/InputMethodManager;->showSoftInput(Landroid/view/View;I)Z

    .line 313
    .end local v0    # "imm":Landroid/view/inputmethod/InputMethodManager;
    :cond_0
    return-void
.end method

.method protected updateStage(I)V
    .locals 3
    .param p1, "stage"    # I

    .prologue
    .line 132
    iget v0, p0, Lcom/google/android/setupwizard/edu/ChooseLockPinActivity;->mUiStage:I

    .line 133
    .local v0, "previousStage":I
    iput p1, p0, Lcom/google/android/setupwizard/edu/ChooseLockPinActivity;->mUiStage:I

    .line 134
    invoke-direct {p0}, Lcom/google/android/setupwizard/edu/ChooseLockPinActivity;->updateUi()V

    .line 138
    if-eq v0, p1, :cond_0

    .line 139
    iget-object v1, p0, Lcom/google/android/setupwizard/edu/ChooseLockPinActivity;->mHeaderText:Landroid/widget/TextView;

    iget-object v2, p0, Lcom/google/android/setupwizard/edu/ChooseLockPinActivity;->mHeaderText:Landroid/widget/TextView;

    invoke-virtual {v2}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->announceForAccessibility(Ljava/lang/CharSequence;)V

    .line 141
    :cond_0
    return-void
.end method
