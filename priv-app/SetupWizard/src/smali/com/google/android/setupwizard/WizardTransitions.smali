.class public Lcom/google/android/setupwizard/WizardTransitions;
.super Landroid/util/SparseArray;
.source "WizardTransitions.java"

# interfaces
.implements Landroid/os/Parcelable;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/util/SparseArray",
        "<",
        "Ljava/lang/String;",
        ">;",
        "Landroid/os/Parcelable;"
    }
.end annotation


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/google/android/setupwizard/WizardTransitions;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private mDefaultAction:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 96
    new-instance v0, Lcom/google/android/setupwizard/WizardTransitions$1;

    invoke-direct {v0}, Lcom/google/android/setupwizard/WizardTransitions$1;-><init>()V

    sput-object v0, Lcom/google/android/setupwizard/WizardTransitions;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 29
    invoke-direct {p0}, Landroid/util/SparseArray;-><init>()V

    return-void
.end method

.method static synthetic access$002(Lcom/google/android/setupwizard/WizardTransitions;Ljava/lang/String;)Ljava/lang/String;
    .locals 0
    .param p0, "x0"    # Lcom/google/android/setupwizard/WizardTransitions;
    .param p1, "x1"    # Ljava/lang/String;

    .prologue
    .line 29
    iput-object p1, p0, Lcom/google/android/setupwizard/WizardTransitions;->mDefaultAction:Ljava/lang/String;

    return-object p1
.end method


# virtual methods
.method public describeContents()I
    .locals 1

    .prologue
    .line 86
    const/4 v0, 0x0

    return v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 11
    .param p1, "obj"    # Ljava/lang/Object;

    .prologue
    const/4 v8, 0x1

    const/4 v7, 0x0

    .line 46
    instance-of v9, p1, Lcom/google/android/setupwizard/WizardTransitions;

    if-eqz v9, :cond_0

    move-object v4, p1

    .line 47
    check-cast v4, Lcom/google/android/setupwizard/WizardTransitions;

    .line 48
    .local v4, "o":Lcom/google/android/setupwizard/WizardTransitions;
    iget-object v9, p0, Lcom/google/android/setupwizard/WizardTransitions;->mDefaultAction:Ljava/lang/String;

    if-eqz v9, :cond_1

    iget-object v9, p0, Lcom/google/android/setupwizard/WizardTransitions;->mDefaultAction:Ljava/lang/String;

    iget-object v10, v4, Lcom/google/android/setupwizard/WizardTransitions;->mDefaultAction:Ljava/lang/String;

    invoke-virtual {v9, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    .line 50
    .local v0, "equalDefault":Z
    :goto_0
    if-nez v0, :cond_3

    .line 65
    .end local v0    # "equalDefault":Z
    .end local v4    # "o":Lcom/google/android/setupwizard/WizardTransitions;
    :cond_0
    :goto_1
    return v7

    .line 48
    .restart local v4    # "o":Lcom/google/android/setupwizard/WizardTransitions;
    :cond_1
    iget-object v9, v4, Lcom/google/android/setupwizard/WizardTransitions;->mDefaultAction:Ljava/lang/String;

    if-nez v9, :cond_2

    move v0, v8

    goto :goto_0

    :cond_2
    move v0, v7

    goto :goto_0

    .line 53
    .restart local v0    # "equalDefault":Z
    :cond_3
    invoke-virtual {p0}, Lcom/google/android/setupwizard/WizardTransitions;->size()I

    move-result v9

    invoke-virtual {v4}, Lcom/google/android/setupwizard/WizardTransitions;->size()I

    move-result v10

    if-ne v9, v10, :cond_0

    .line 54
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_2
    invoke-virtual {p0}, Lcom/google/android/setupwizard/WizardTransitions;->size()I

    move-result v9

    if-ge v2, v9, :cond_6

    .line 55
    invoke-virtual {p0, v2}, Lcom/google/android/setupwizard/WizardTransitions;->keyAt(I)I

    move-result v3

    .line 56
    .local v3, "key":I
    invoke-virtual {p0, v3}, Lcom/google/android/setupwizard/WizardTransitions;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/lang/String;

    .line 57
    .local v5, "val1":Ljava/lang/String;
    invoke-virtual {v4, v3}, Lcom/google/android/setupwizard/WizardTransitions;->get(I)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Ljava/lang/String;

    .line 58
    .local v6, "val2":Ljava/lang/String;
    if-eqz v5, :cond_4

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    .line 59
    .local v1, "equals":Z
    :goto_3
    if-eqz v1, :cond_0

    .line 54
    add-int/lit8 v2, v2, 0x1

    goto :goto_2

    .line 58
    .end local v1    # "equals":Z
    :cond_4
    if-nez v6, :cond_5

    move v1, v8

    goto :goto_3

    :cond_5
    move v1, v7

    goto :goto_3

    .end local v3    # "key":I
    .end local v5    # "val1":Ljava/lang/String;
    .end local v6    # "val2":Ljava/lang/String;
    :cond_6
    move v7, v8

    .line 63
    goto :goto_1
.end method

.method public getAction(I)Ljava/lang/String;
    .locals 1
    .param p1, "resultCode"    # I

    .prologue
    .line 41
    iget-object v0, p0, Lcom/google/android/setupwizard/WizardTransitions;->mDefaultAction:Ljava/lang/String;

    invoke-virtual {p0, p1, v0}, Lcom/google/android/setupwizard/WizardTransitions;->get(ILjava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    return-object v0
.end method

.method public hashCode()I
    .locals 5

    .prologue
    .line 70
    iget-object v3, p0, Lcom/google/android/setupwizard/WizardTransitions;->mDefaultAction:Ljava/lang/String;

    invoke-virtual {v3}, Ljava/lang/String;->hashCode()I

    move-result v0

    .line 72
    .local v0, "hashCode":I
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    invoke-virtual {p0}, Lcom/google/android/setupwizard/WizardTransitions;->size()I

    move-result v3

    if-ge v1, v3, :cond_1

    .line 73
    invoke-virtual {p0, v1}, Lcom/google/android/setupwizard/WizardTransitions;->keyAt(I)I

    move-result v3

    invoke-virtual {p0, v3}, Lcom/google/android/setupwizard/WizardTransitions;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    .line 74
    .local v2, "val":Ljava/lang/String;
    mul-int/lit8 v4, v0, 0x1f

    if-eqz v2, :cond_0

    invoke-virtual {v2}, Ljava/lang/String;->hashCode()I

    move-result v3

    :goto_1
    add-int v0, v4, v3

    .line 72
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 74
    :cond_0
    const/4 v3, 0x0

    goto :goto_1

    .line 76
    .end local v2    # "val":Ljava/lang/String;
    :cond_1
    return v0
.end method

.method public setDefaultAction(Ljava/lang/String;)V
    .locals 0
    .param p1, "action"    # Ljava/lang/String;

    .prologue
    .line 33
    iput-object p1, p0, Lcom/google/android/setupwizard/WizardTransitions;->mDefaultAction:Ljava/lang/String;

    .line 34
    return-void
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    .prologue
    .line 81
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-super {p0}, Landroid/util/SparseArray;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " default: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/setupwizard/WizardTransitions;->mDefaultAction:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 1
    .param p1, "dest"    # Landroid/os/Parcel;
    .param p2, "flags"    # I

    .prologue
    .line 92
    iget-object v0, p0, Lcom/google/android/setupwizard/WizardTransitions;->mDefaultAction:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 93
    invoke-virtual {p1, p0}, Landroid/os/Parcel;->writeSparseArray(Landroid/util/SparseArray;)V

    .line 94
    return-void
.end method
