.class public abstract Lcom/google/android/setupwizard/util/GservicesValue;
.super Ljava/lang/Object;
.source "GservicesValue.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/setupwizard/util/GservicesValue$GservicesReaderDefaultValues;,
        Lcom/google/android/setupwizard/util/GservicesValue$GservicesReaderImpl;,
        Lcom/google/android/setupwizard/util/GservicesValue$GservicesReader;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;"
    }
.end annotation


# static fields
.field private static final mLock:Ljava/lang/Object;

.field private static sGservicesReader:Lcom/google/android/setupwizard/util/GservicesValue$GservicesReader;


# instance fields
.field protected final mDefaultValue:Ljava/lang/Object;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "TT;"
        }
    .end annotation
.end field

.field protected final mKey:Ljava/lang/String;

.field private mOverride:Ljava/lang/Object;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "TT;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 21
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    sput-object v0, Lcom/google/android/setupwizard/util/GservicesValue;->mLock:Ljava/lang/Object;

    .line 23
    const/4 v0, 0x0

    sput-object v0, Lcom/google/android/setupwizard/util/GservicesValue;->sGservicesReader:Lcom/google/android/setupwizard/util/GservicesValue$GservicesReader;

    return-void
.end method

.method protected constructor <init>(Ljava/lang/String;Ljava/lang/Object;)V
    .locals 1
    .param p1, "key"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "TT;)V"
        }
    .end annotation

    .prologue
    .line 71
    .local p0, "this":Lcom/google/android/setupwizard/util/GservicesValue;, "Lcom/google/android/setupwizard/util/GservicesValue<TT;>;"
    .local p2, "defaultValue":Ljava/lang/Object;, "TT;"
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 69
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/setupwizard/util/GservicesValue;->mOverride:Ljava/lang/Object;

    .line 72
    iput-object p1, p0, Lcom/google/android/setupwizard/util/GservicesValue;->mKey:Ljava/lang/String;

    .line 73
    iput-object p2, p0, Lcom/google/android/setupwizard/util/GservicesValue;->mDefaultValue:Ljava/lang/Object;

    .line 74
    return-void
.end method

.method static synthetic access$100()Lcom/google/android/setupwizard/util/GservicesValue$GservicesReader;
    .locals 1

    .prologue
    .line 17
    sget-object v0, Lcom/google/android/setupwizard/util/GservicesValue;->sGservicesReader:Lcom/google/android/setupwizard/util/GservicesValue$GservicesReader;

    return-object v0
.end method

.method public static forceInit(Landroid/content/Context;)V
    .locals 3
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 53
    sget-object v1, Lcom/google/android/setupwizard/util/GservicesValue;->mLock:Ljava/lang/Object;

    monitor-enter v1

    .line 54
    :try_start_0
    new-instance v0, Lcom/google/android/setupwizard/util/GservicesValue$GservicesReaderImpl;

    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    invoke-direct {v0, v2}, Lcom/google/android/setupwizard/util/GservicesValue$GservicesReaderImpl;-><init>(Landroid/content/ContentResolver;)V

    sput-object v0, Lcom/google/android/setupwizard/util/GservicesValue;->sGservicesReader:Lcom/google/android/setupwizard/util/GservicesValue$GservicesReader;

    .line 55
    monitor-exit v1

    .line 56
    return-void

    .line 55
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public static init(Landroid/content/Context;)V
    .locals 3
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 29
    sget-object v1, Lcom/google/android/setupwizard/util/GservicesValue;->mLock:Ljava/lang/Object;

    monitor-enter v1

    .line 30
    :try_start_0
    sget-object v0, Lcom/google/android/setupwizard/util/GservicesValue;->sGservicesReader:Lcom/google/android/setupwizard/util/GservicesValue$GservicesReader;

    if-nez v0, :cond_0

    .line 31
    new-instance v0, Lcom/google/android/setupwizard/util/GservicesValue$GservicesReaderImpl;

    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    invoke-direct {v0, v2}, Lcom/google/android/setupwizard/util/GservicesValue$GservicesReaderImpl;-><init>(Landroid/content/ContentResolver;)V

    sput-object v0, Lcom/google/android/setupwizard/util/GservicesValue;->sGservicesReader:Lcom/google/android/setupwizard/util/GservicesValue$GservicesReader;

    .line 33
    :cond_0
    monitor-exit v1

    .line 34
    return-void

    .line 33
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public static initForTests()V
    .locals 3

    .prologue
    .line 38
    sget-object v1, Lcom/google/android/setupwizard/util/GservicesValue;->mLock:Ljava/lang/Object;

    monitor-enter v1

    .line 39
    :try_start_0
    new-instance v0, Lcom/google/android/setupwizard/util/GservicesValue$GservicesReaderDefaultValues;

    const/4 v2, 0x0

    invoke-direct {v0, v2}, Lcom/google/android/setupwizard/util/GservicesValue$GservicesReaderDefaultValues;-><init>(Lcom/google/android/setupwizard/util/GservicesValue$1;)V

    sput-object v0, Lcom/google/android/setupwizard/util/GservicesValue;->sGservicesReader:Lcom/google/android/setupwizard/util/GservicesValue$GservicesReader;

    .line 40
    monitor-exit v1

    .line 41
    return-void

    .line 40
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public static value(Ljava/lang/String;Ljava/lang/Integer;)Lcom/google/android/setupwizard/util/GservicesValue;
    .locals 1
    .param p0, "key"    # Ljava/lang/String;
    .param p1, "defaultValue"    # Ljava/lang/Integer;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/lang/Integer;",
            ")",
            "Lcom/google/android/setupwizard/util/GservicesValue",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation

    .prologue
    .line 146
    new-instance v0, Lcom/google/android/setupwizard/util/GservicesValue$3;

    invoke-direct {v0, p0, p1}, Lcom/google/android/setupwizard/util/GservicesValue$3;-><init>(Ljava/lang/String;Ljava/lang/Integer;)V

    return-object v0
.end method

.method public static value(Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/setupwizard/util/GservicesValue;
    .locals 1
    .param p0, "key"    # Ljava/lang/String;
    .param p1, "defaultValue"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ")",
            "Lcom/google/android/setupwizard/util/GservicesValue",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 174
    new-instance v0, Lcom/google/android/setupwizard/util/GservicesValue$6;

    invoke-direct {v0, p0, p1}, Lcom/google/android/setupwizard/util/GservicesValue$6;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    return-object v0
.end method


# virtual methods
.method public final get()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()TT;"
        }
    .end annotation

    .prologue
    .line 103
    .local p0, "this":Lcom/google/android/setupwizard/util/GservicesValue;, "Lcom/google/android/setupwizard/util/GservicesValue<TT;>;"
    iget-object v0, p0, Lcom/google/android/setupwizard/util/GservicesValue;->mOverride:Ljava/lang/Object;

    if-eqz v0, :cond_0

    .line 104
    iget-object v0, p0, Lcom/google/android/setupwizard/util/GservicesValue;->mOverride:Ljava/lang/Object;

    .line 106
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/google/android/setupwizard/util/GservicesValue;->mKey:Ljava/lang/String;

    invoke-virtual {p0, v0}, Lcom/google/android/setupwizard/util/GservicesValue;->retrieve(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    goto :goto_0
.end method

.method public override(Ljava/lang/Object;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TT;)V"
        }
    .end annotation

    .prologue
    .line 83
    .local p0, "this":Lcom/google/android/setupwizard/util/GservicesValue;, "Lcom/google/android/setupwizard/util/GservicesValue<TT;>;"
    .local p1, "value":Ljava/lang/Object;, "TT;"
    sget-object v0, Lcom/google/android/setupwizard/util/GservicesValue;->sGservicesReader:Lcom/google/android/setupwizard/util/GservicesValue$GservicesReader;

    instance-of v0, v0, Lcom/google/android/setupwizard/util/GservicesValue$GservicesReaderDefaultValues;

    if-nez v0, :cond_0

    .line 84
    const-string v0, "GservicesValue"

    const-string v1, "GservicesValue.override(): test should probably call initForTests() first"

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 86
    :cond_0
    iput-object p1, p0, Lcom/google/android/setupwizard/util/GservicesValue;->mOverride:Ljava/lang/Object;

    .line 87
    return-void
.end method

.method public resetOverride()V
    .locals 1

    .prologue
    .line 91
    .local p0, "this":Lcom/google/android/setupwizard/util/GservicesValue;, "Lcom/google/android/setupwizard/util/GservicesValue<TT;>;"
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/setupwizard/util/GservicesValue;->mOverride:Ljava/lang/Object;

    .line 92
    return-void
.end method

.method protected abstract retrieve(Ljava/lang/String;)Ljava/lang/Object;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")TT;"
        }
    .end annotation
.end method
