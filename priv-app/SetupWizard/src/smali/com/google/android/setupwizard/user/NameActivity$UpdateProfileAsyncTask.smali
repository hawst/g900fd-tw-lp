.class Lcom/google/android/setupwizard/user/NameActivity$UpdateProfileAsyncTask;
.super Landroid/os/AsyncTask;
.source "NameActivity.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/setupwizard/user/NameActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "UpdateProfileAsyncTask"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/os/AsyncTask",
        "<",
        "Ljava/lang/String;",
        "Ljava/lang/Void;",
        "Ljava/lang/Void;",
        ">;"
    }
.end annotation


# instance fields
.field private final mShouldProceed:Z

.field final synthetic this$0:Lcom/google/android/setupwizard/user/NameActivity;


# direct methods
.method public constructor <init>(Lcom/google/android/setupwizard/user/NameActivity;Z)V
    .locals 0
    .param p2, "shouldProceed"    # Z

    .prologue
    .line 178
    iput-object p1, p0, Lcom/google/android/setupwizard/user/NameActivity$UpdateProfileAsyncTask;->this$0:Lcom/google/android/setupwizard/user/NameActivity;

    invoke-direct {p0}, Landroid/os/AsyncTask;-><init>()V

    .line 179
    iput-boolean p2, p0, Lcom/google/android/setupwizard/user/NameActivity$UpdateProfileAsyncTask;->mShouldProceed:Z

    .line 180
    return-void
.end method


# virtual methods
.method protected bridge synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1
    .param p1, "x0"    # [Ljava/lang/Object;

    .prologue
    .line 174
    check-cast p1, [Ljava/lang/String;

    .end local p1    # "x0":[Ljava/lang/Object;
    invoke-virtual {p0, p1}, Lcom/google/android/setupwizard/user/NameActivity$UpdateProfileAsyncTask;->doInBackground([Ljava/lang/String;)Ljava/lang/Void;

    move-result-object v0

    return-object v0
.end method

.method protected varargs doInBackground([Ljava/lang/String;)Ljava/lang/Void;
    .locals 5
    .param p1, "names"    # [Ljava/lang/String;

    .prologue
    const/4 v4, 0x0

    .line 184
    array-length v2, p1

    const/4 v3, 0x2

    if-eq v2, v3, :cond_0

    .line 185
    new-instance v2, Ljava/lang/IllegalArgumentException;

    const-string v3, "Update profile expects two arguments, first and last name"

    invoke-direct {v2, v3}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v2

    .line 188
    :cond_0
    const/4 v2, 0x0

    aget-object v0, p1, v2

    .line 189
    .local v0, "firstName":Ljava/lang/String;
    const/4 v2, 0x1

    aget-object v1, p1, v2

    .line 190
    .local v1, "lastName":Ljava/lang/String;
    iget-object v2, p0, Lcom/google/android/setupwizard/user/NameActivity$UpdateProfileAsyncTask;->this$0:Lcom/google/android/setupwizard/user/NameActivity;

    invoke-virtual {v2}, Lcom/google/android/setupwizard/user/NameActivity;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    invoke-static {v0, v1, v4, v2}, Lcom/google/android/setupwizard/user/ProfileHelper;->updateMeContactWith(Ljava/lang/String;Ljava/lang/String;Landroid/graphics/Bitmap;Landroid/content/ContentResolver;)V

    .line 191
    return-object v4
.end method

.method protected bridge synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 0
    .param p1, "x0"    # Ljava/lang/Object;

    .prologue
    .line 174
    check-cast p1, Ljava/lang/Void;

    .end local p1    # "x0":Ljava/lang/Object;
    invoke-virtual {p0, p1}, Lcom/google/android/setupwizard/user/NameActivity$UpdateProfileAsyncTask;->onPostExecute(Ljava/lang/Void;)V

    return-void
.end method

.method protected onPostExecute(Ljava/lang/Void;)V
    .locals 1
    .param p1, "result"    # Ljava/lang/Void;

    .prologue
    .line 196
    iget-boolean v0, p0, Lcom/google/android/setupwizard/user/NameActivity$UpdateProfileAsyncTask;->mShouldProceed:Z

    if-eqz v0, :cond_0

    .line 197
    iget-object v0, p0, Lcom/google/android/setupwizard/user/NameActivity$UpdateProfileAsyncTask;->this$0:Lcom/google/android/setupwizard/user/NameActivity;

    # invokes: Lcom/google/android/setupwizard/user/NameActivity;->nextScreen()V
    invoke-static {v0}, Lcom/google/android/setupwizard/user/NameActivity;->access$100(Lcom/google/android/setupwizard/user/NameActivity;)V

    .line 199
    :cond_0
    return-void
.end method
