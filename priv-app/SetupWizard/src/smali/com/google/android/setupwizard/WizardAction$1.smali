.class final Lcom/google/android/setupwizard/WizardAction$1;
.super Ljava/lang/Object;
.source "WizardAction.java"

# interfaces
.implements Landroid/os/Parcelable$Creator;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/setupwizard/WizardAction;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Landroid/os/Parcelable$Creator",
        "<",
        "Lcom/google/android/setupwizard/WizardAction;",
        ">;"
    }
.end annotation


# direct methods
.method constructor <init>()V
    .locals 0

    .prologue
    .line 187
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public createFromParcel(Landroid/os/Parcel;)Lcom/google/android/setupwizard/WizardAction;
    .locals 4
    .param p1, "source"    # Landroid/os/Parcel;

    .prologue
    .line 190
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v2

    .line 191
    .local v2, "uri":Ljava/lang/String;
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    .line 192
    .local v0, "id":Ljava/lang/String;
    const-class v3, Lcom/google/android/setupwizard/WizardTransitions;

    invoke-virtual {v3}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v3

    invoke-virtual {p1, v3}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object v1

    check-cast v1, Lcom/google/android/setupwizard/WizardTransitions;

    .line 194
    .local v1, "transitions":Lcom/google/android/setupwizard/WizardTransitions;
    new-instance v3, Lcom/google/android/setupwizard/WizardAction;

    invoke-direct {v3, v0, v2, v1}, Lcom/google/android/setupwizard/WizardAction;-><init>(Ljava/lang/String;Ljava/lang/String;Lcom/google/android/setupwizard/WizardTransitions;)V

    return-object v3
.end method

.method public bridge synthetic createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;
    .locals 1
    .param p1, "x0"    # Landroid/os/Parcel;

    .prologue
    .line 187
    invoke-virtual {p0, p1}, Lcom/google/android/setupwizard/WizardAction$1;->createFromParcel(Landroid/os/Parcel;)Lcom/google/android/setupwizard/WizardAction;

    move-result-object v0

    return-object v0
.end method

.method public newArray(I)[Lcom/google/android/setupwizard/WizardAction;
    .locals 1
    .param p1, "size"    # I

    .prologue
    .line 199
    new-array v0, p1, [Lcom/google/android/setupwizard/WizardAction;

    return-object v0
.end method

.method public bridge synthetic newArray(I)[Ljava/lang/Object;
    .locals 1
    .param p1, "x0"    # I

    .prologue
    .line 187
    invoke-virtual {p0, p1}, Lcom/google/android/setupwizard/WizardAction$1;->newArray(I)[Lcom/google/android/setupwizard/WizardAction;

    move-result-object v0

    return-object v0
.end method
