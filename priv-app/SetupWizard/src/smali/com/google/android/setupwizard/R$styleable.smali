.class public final Lcom/google/android/setupwizard/R$styleable;
.super Ljava/lang/Object;
.source "R.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/setupwizard/R;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "styleable"
.end annotation


# static fields
.field public static final AdsAttrs:[I

.field public static final AppDataSearch:[I

.field public static final Corpus:[I

.field public static final FeatureParam:[I

.field public static final GlobalSearch:[I

.field public static final GlobalSearchCorpus:[I

.field public static final GlobalSearchSection:[I

.field public static final IMECorpus:[I

.field public static final MapAttrs:[I

.field public static final Section:[I

.field public static final SectionFeature:[I

.field public static final SetupWizardIllustration:[I

.field public static final WalletFragmentOptions:[I

.field public static final WalletFragmentStyle:[I

.field public static final WizardAction:[I

.field public static final WizardScript:[I

.field public static final result:[I


# direct methods
.method static constructor <clinit>()V
    .locals 7

    .prologue
    const/4 v6, 0x6

    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 2007
    new-array v0, v5, [I

    fill-array-data v0, :array_0

    sput-object v0, Lcom/google/android/setupwizard/R$styleable;->AdsAttrs:[I

    .line 2077
    new-array v0, v2, [I

    sput-object v0, Lcom/google/android/setupwizard/R$styleable;->AppDataSearch:[I

    .line 2103
    const/4 v0, 0x5

    new-array v0, v0, [I

    fill-array-data v0, :array_1

    sput-object v0, Lcom/google/android/setupwizard/R$styleable;->Corpus:[I

    .line 2203
    new-array v0, v4, [I

    fill-array-data v0, :array_2

    sput-object v0, Lcom/google/android/setupwizard/R$styleable;->FeatureParam:[I

    .line 2261
    new-array v0, v6, [I

    fill-array-data v0, :array_3

    sput-object v0, Lcom/google/android/setupwizard/R$styleable;->GlobalSearch:[I

    .line 2383
    new-array v0, v3, [I

    const v1, 0x7f010018

    aput v1, v0, v2

    sput-object v0, Lcom/google/android/setupwizard/R$styleable;->GlobalSearchCorpus:[I

    .line 2423
    new-array v0, v4, [I

    fill-array-data v0, :array_4

    sput-object v0, Lcom/google/android/setupwizard/R$styleable;->GlobalSearchSection:[I

    .line 2494
    new-array v0, v6, [I

    fill-array-data v0, :array_5

    sput-object v0, Lcom/google/android/setupwizard/R$styleable;->IMECorpus:[I

    .line 2640
    const/16 v0, 0xe

    new-array v0, v0, [I

    fill-array-data v0, :array_6

    sput-object v0, Lcom/google/android/setupwizard/R$styleable;->MapAttrs:[I

    .line 2872
    const/4 v0, 0x7

    new-array v0, v0, [I

    fill-array-data v0, :array_7

    sput-object v0, Lcom/google/android/setupwizard/R$styleable;->Section:[I

    .line 3007
    new-array v0, v3, [I

    const v1, 0x7f01000f

    aput v1, v0, v2

    sput-object v0, Lcom/google/android/setupwizard/R$styleable;->SectionFeature:[I

    .line 3045
    new-array v0, v3, [I

    const v1, 0x7f01004d

    aput v1, v0, v2

    sput-object v0, Lcom/google/android/setupwizard/R$styleable;->SetupWizardIllustration:[I

    .line 3078
    const/4 v0, 0x4

    new-array v0, v0, [I

    fill-array-data v0, :array_8

    sput-object v0, Lcom/google/android/setupwizard/R$styleable;->WalletFragmentOptions:[I

    .line 3182
    const/16 v0, 0xb

    new-array v0, v0, [I

    fill-array-data v0, :array_9

    sput-object v0, Lcom/google/android/setupwizard/R$styleable;->WalletFragmentStyle:[I

    .line 3395
    new-array v0, v3, [I

    const v1, 0x7f010049

    aput v1, v0, v2

    sput-object v0, Lcom/google/android/setupwizard/R$styleable;->WizardAction:[I

    .line 3422
    new-array v0, v3, [I

    const v1, 0x7f010048

    aput v1, v0, v2

    sput-object v0, Lcom/google/android/setupwizard/R$styleable;->WizardScript:[I

    .line 3453
    new-array v0, v5, [I

    fill-array-data v0, :array_a

    sput-object v0, Lcom/google/android/setupwizard/R$styleable;->result:[I

    return-void

    .line 2007
    nop

    :array_0
    .array-data 4
        0x7f010000
        0x7f010001
        0x7f010002
    .end array-data

    .line 2103
    :array_1
    .array-data 4
        0x7f010003
        0x7f010004
        0x7f010005
        0x7f010006
        0x7f010007
    .end array-data

    .line 2203
    :array_2
    .array-data 4
        0x7f010010
        0x7f010011
    .end array-data

    .line 2261
    :array_3
    .array-data 4
        0x7f010012
        0x7f010013
        0x7f010014
        0x7f010015
        0x7f010016
        0x7f010017
    .end array-data

    .line 2423
    :array_4
    .array-data 4
        0x7f01001f
        0x7f010020
    .end array-data

    .line 2494
    :array_5
    .array-data 4
        0x7f010019
        0x7f01001a
        0x7f01001b
        0x7f01001c
        0x7f01001d
        0x7f01001e
    .end array-data

    .line 2640
    :array_6
    .array-data 4
        0x7f010021
        0x7f010022
        0x7f010023
        0x7f010024
        0x7f010025
        0x7f010026
        0x7f010027
        0x7f010028
        0x7f010029
        0x7f01002a
        0x7f01002b
        0x7f01002c
        0x7f01002d
        0x7f01002e
    .end array-data

    .line 2872
    :array_7
    .array-data 4
        0x7f010008
        0x7f010009
        0x7f01000a
        0x7f01000b
        0x7f01000c
        0x7f01000d
        0x7f01000e
    .end array-data

    .line 3078
    :array_8
    .array-data 4
        0x7f01002f
        0x7f010030
        0x7f010031
        0x7f010032
    .end array-data

    .line 3182
    :array_9
    .array-data 4
        0x7f010033
        0x7f010034
        0x7f010035
        0x7f010036
        0x7f010037
        0x7f010038
        0x7f010039
        0x7f01003a
        0x7f01003b
        0x7f01003c
        0x7f01003d
    .end array-data

    .line 3453
    :array_a
    .array-data 4
        0x7f01004a
        0x7f01004b
        0x7f01004c
    .end array-data
.end method
