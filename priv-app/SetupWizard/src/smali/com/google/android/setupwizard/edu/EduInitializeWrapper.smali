.class public Lcom/google/android/setupwizard/edu/EduInitializeWrapper;
.super Lcom/google/android/setupwizard/SubactivityWrapper;
.source "EduInitializeWrapper.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 25
    invoke-direct {p0}, Lcom/google/android/setupwizard/SubactivityWrapper;-><init>()V

    return-void
.end method


# virtual methods
.method protected onSubactivityLaunch()Landroid/content/Intent;
    .locals 3

    .prologue
    .line 35
    const/4 v0, 0x0

    .line 36
    .local v0, "i":Landroid/content/Intent;
    invoke-virtual {p0}, Lcom/google/android/setupwizard/edu/EduInitializeWrapper;->isEduSignin()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 37
    new-instance v0, Landroid/content/Intent;

    .end local v0    # "i":Landroid/content/Intent;
    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    .line 38
    .restart local v0    # "i":Landroid/content/Intent;
    const-string v1, "com.google.android.nfcprovision"

    const-string v2, "com.google.android.nfcprovision.EduPreSetupActivity"

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->setClassName(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 41
    :cond_0
    return-object v0
.end method

.method protected requestCode()I
    .locals 1

    .prologue
    .line 30
    const/16 v0, 0x2734

    return v0
.end method
