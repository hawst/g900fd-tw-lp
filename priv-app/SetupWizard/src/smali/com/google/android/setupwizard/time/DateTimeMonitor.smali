.class public Lcom/google/android/setupwizard/time/DateTimeMonitor;
.super Ljava/lang/Object;
.source "DateTimeMonitor.java"


# static fields
.field private static final LOCAL_LOGV:Z

.field private static sInstance:Lcom/google/android/setupwizard/time/DateTimeMonitor;


# instance fields
.field private final mContext:Landroid/content/Context;

.field private final mReceiver:Landroid/content/BroadcastReceiver;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 39
    const-string v0, "DateTimeMonitor"

    const/4 v1, 0x2

    invoke-static {v0, v1}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    sput-boolean v0, Lcom/google/android/setupwizard/time/DateTimeMonitor;->LOCAL_LOGV:Z

    return-void
.end method

.method private constructor <init>(Landroid/content/Context;)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 89
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 64
    new-instance v1, Lcom/google/android/setupwizard/time/DateTimeMonitor$1;

    invoke-direct {v1, p0}, Lcom/google/android/setupwizard/time/DateTimeMonitor$1;-><init>(Lcom/google/android/setupwizard/time/DateTimeMonitor;)V

    iput-object v1, p0, Lcom/google/android/setupwizard/time/DateTimeMonitor;->mReceiver:Landroid/content/BroadcastReceiver;

    .line 90
    iput-object p1, p0, Lcom/google/android/setupwizard/time/DateTimeMonitor;->mContext:Landroid/content/Context;

    .line 93
    new-instance v0, Landroid/content/IntentFilter;

    invoke-direct {v0}, Landroid/content/IntentFilter;-><init>()V

    .line 94
    .local v0, "intentFilter":Landroid/content/IntentFilter;
    const-string v1, "android.intent.action.NETWORK_SET_TIME"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 95
    const-string v1, "android.intent.action.NETWORK_SET_TIMEZONE"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 96
    const-string v1, "android.intent.action.TIME_SET"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 97
    const-string v1, "android.intent.action.TIMEZONE_CHANGED"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 98
    const-string v1, "com.google.gservices.intent.action.GSERVICES_CHANGED"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 99
    iget-object v1, p0, Lcom/google/android/setupwizard/time/DateTimeMonitor;->mReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {p1, v1, v0}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    .line 101
    invoke-virtual {p0}, Lcom/google/android/setupwizard/time/DateTimeMonitor;->isTimeZoneSet()Z

    move-result v1

    if-nez v1, :cond_0

    .line 102
    invoke-direct {p0}, Lcom/google/android/setupwizard/time/DateTimeMonitor;->setTimeZoneFromGservices()V

    .line 104
    :cond_0
    return-void
.end method

.method static synthetic access$000()Z
    .locals 1

    .prologue
    .line 34
    sget-boolean v0, Lcom/google/android/setupwizard/time/DateTimeMonitor;->LOCAL_LOGV:Z

    return v0
.end method

.method static synthetic access$100(Lcom/google/android/setupwizard/time/DateTimeMonitor;)V
    .locals 0
    .param p0, "x0"    # Lcom/google/android/setupwizard/time/DateTimeMonitor;

    .prologue
    .line 34
    invoke-direct {p0}, Lcom/google/android/setupwizard/time/DateTimeMonitor;->timeChanged()V

    return-void
.end method

.method static synthetic access$200(Lcom/google/android/setupwizard/time/DateTimeMonitor;)V
    .locals 0
    .param p0, "x0"    # Lcom/google/android/setupwizard/time/DateTimeMonitor;

    .prologue
    .line 34
    invoke-direct {p0}, Lcom/google/android/setupwizard/time/DateTimeMonitor;->setTimeZoneFromGservices()V

    return-void
.end method

.method static synthetic access$300(Lcom/google/android/setupwizard/time/DateTimeMonitor;)V
    .locals 0
    .param p0, "x0"    # Lcom/google/android/setupwizard/time/DateTimeMonitor;

    .prologue
    .line 34
    invoke-direct {p0}, Lcom/google/android/setupwizard/time/DateTimeMonitor;->timeZoneChanged()V

    return-void
.end method

.method public static getInstance()Lcom/google/android/setupwizard/time/DateTimeMonitor;
    .locals 1

    .prologue
    .line 56
    sget-object v0, Lcom/google/android/setupwizard/time/DateTimeMonitor;->sInstance:Lcom/google/android/setupwizard/time/DateTimeMonitor;

    return-object v0
.end method

.method public static initInstance(Landroid/content/Context;)V
    .locals 2
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 50
    sget-object v0, Lcom/google/android/setupwizard/time/DateTimeMonitor;->sInstance:Lcom/google/android/setupwizard/time/DateTimeMonitor;

    if-nez v0, :cond_0

    .line 51
    new-instance v0, Lcom/google/android/setupwizard/time/DateTimeMonitor;

    invoke-virtual {p0}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/google/android/setupwizard/time/DateTimeMonitor;-><init>(Landroid/content/Context;)V

    sput-object v0, Lcom/google/android/setupwizard/time/DateTimeMonitor;->sInstance:Lcom/google/android/setupwizard/time/DateTimeMonitor;

    .line 53
    :cond_0
    return-void
.end method

.method private setTimeZoneFromGservices()V
    .locals 6

    .prologue
    .line 152
    :try_start_0
    iget-object v3, p0, Lcom/google/android/setupwizard/time/DateTimeMonitor;->mContext:Landroid/content/Context;

    invoke-virtual {v3}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v3

    const-string v4, "google_setup:ip_timezone"

    invoke-static {v3, v4}, Lcom/google/android/gsf/Gservices;->getString(Landroid/content/ContentResolver;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 154
    .local v2, "timeZoneId":Ljava/lang/String;
    sget-boolean v3, Lcom/google/android/setupwizard/time/DateTimeMonitor;->LOCAL_LOGV:Z

    if-eqz v3, :cond_0

    const-string v3, "DateTimeMonitor"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "setTimeZoneFromGservices() timeZoneId="

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 155
    :cond_0
    iget-object v3, p0, Lcom/google/android/setupwizard/time/DateTimeMonitor;->mContext:Landroid/content/Context;

    const-string v4, "alarm"

    invoke-virtual {v3, v4}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/AlarmManager;

    .line 156
    .local v0, "am":Landroid/app/AlarmManager;
    invoke-virtual {v0, v2}, Landroid/app/AlarmManager;->setTimeZone(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 162
    .end local v0    # "am":Landroid/app/AlarmManager;
    .end local v2    # "timeZoneId":Ljava/lang/String;
    :goto_0
    return-void

    .line 158
    :catch_0
    move-exception v1

    .line 159
    .local v1, "e":Ljava/lang/Exception;
    const-string v3, "DateTimeMonitor"

    const-string v4, "failed to get alarm manager to set the system timezone"

    invoke-static {v3, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method private timeChanged()V
    .locals 3

    .prologue
    .line 129
    iget-object v0, p0, Lcom/google/android/setupwizard/time/DateTimeMonitor;->mContext:Landroid/content/Context;

    const-string v1, "SetupWizardPrefs"

    const/4 v2, 0x4

    invoke-virtual {v0, v1, v2}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    const-string v1, "setupwizard.time_is_set"

    const/4 v2, 0x1

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 134
    return-void
.end method

.method private timeZoneChanged()V
    .locals 3

    .prologue
    .line 140
    iget-object v0, p0, Lcom/google/android/setupwizard/time/DateTimeMonitor;->mContext:Landroid/content/Context;

    const-string v1, "SetupWizardPrefs"

    const/4 v2, 0x4

    invoke-virtual {v0, v1, v2}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    const-string v1, "setupwizard.timezone_is_set"

    const/4 v2, 0x1

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 145
    return-void
.end method


# virtual methods
.method public isTimeSet()Z
    .locals 4

    .prologue
    .line 107
    iget-object v1, p0, Lcom/google/android/setupwizard/time/DateTimeMonitor;->mContext:Landroid/content/Context;

    const-string v2, "SetupWizardPrefs"

    const/4 v3, 0x4

    invoke-virtual {v1, v2, v3}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v1

    const-string v2, "setupwizard.time_is_set"

    const/4 v3, 0x0

    invoke-interface {v1, v2, v3}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    .line 110
    .local v0, "isSet":Z
    sget-boolean v1, Lcom/google/android/setupwizard/time/DateTimeMonitor;->LOCAL_LOGV:Z

    if-eqz v1, :cond_0

    const-string v1, "DateTimeMonitor"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "isTimeSet() returns "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 111
    :cond_0
    return v0
.end method

.method public isTimeZoneSet()Z
    .locals 5

    .prologue
    const/4 v1, 0x0

    .line 115
    iget-object v2, p0, Lcom/google/android/setupwizard/time/DateTimeMonitor;->mContext:Landroid/content/Context;

    const-string v3, "SetupWizardPrefs"

    const/4 v4, 0x4

    invoke-virtual {v2, v3, v4}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v2

    const-string v3, "setupwizard.timezone_is_set"

    invoke-interface {v2, v3, v1}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    .line 118
    .local v0, "isSet":Z
    if-nez v0, :cond_0

    .line 119
    invoke-static {}, Ljava/util/TimeZone;->getDefault()Ljava/util/TimeZone;

    move-result-object v2

    invoke-virtual {v2}, Ljava/util/TimeZone;->getRawOffset()I

    move-result v2

    if-eqz v2, :cond_2

    const/4 v0, 0x1

    .line 121
    :cond_0
    :goto_0
    sget-boolean v1, Lcom/google/android/setupwizard/time/DateTimeMonitor;->LOCAL_LOGV:Z

    if-eqz v1, :cond_1

    const-string v1, "DateTimeMonitor"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "isTimeZoneSet() returns "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 122
    :cond_1
    return v0

    :cond_2
    move v0, v1

    .line 119
    goto :goto_0
.end method
