.class Lcom/google/android/setupwizard/util/PackageMonitor$1;
.super Landroid/content/pm/PackageInstaller$SessionCallback;
.source "PackageMonitor.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/setupwizard/util/PackageMonitor;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field private final mSessionPackageNameMap:Landroid/util/SparseArray;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/util/SparseArray",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field final synthetic this$0:Lcom/google/android/setupwizard/util/PackageMonitor;


# direct methods
.method constructor <init>(Lcom/google/android/setupwizard/util/PackageMonitor;)V
    .locals 1

    .prologue
    .line 37
    iput-object p1, p0, Lcom/google/android/setupwizard/util/PackageMonitor$1;->this$0:Lcom/google/android/setupwizard/util/PackageMonitor;

    invoke-direct {p0}, Landroid/content/pm/PackageInstaller$SessionCallback;-><init>()V

    .line 39
    new-instance v0, Landroid/util/SparseArray;

    invoke-direct {v0}, Landroid/util/SparseArray;-><init>()V

    iput-object v0, p0, Lcom/google/android/setupwizard/util/PackageMonitor$1;->mSessionPackageNameMap:Landroid/util/SparseArray;

    return-void
.end method

.method private getPackageName(I)Ljava/lang/String;
    .locals 2
    .param p1, "sessionId"    # I

    .prologue
    .line 42
    iget-object v1, p0, Lcom/google/android/setupwizard/util/PackageMonitor$1;->this$0:Lcom/google/android/setupwizard/util/PackageMonitor;

    # getter for: Lcom/google/android/setupwizard/util/PackageMonitor;->mPackageInstaller:Landroid/content/pm/PackageInstaller;
    invoke-static {v1}, Lcom/google/android/setupwizard/util/PackageMonitor;->access$000(Lcom/google/android/setupwizard/util/PackageMonitor;)Landroid/content/pm/PackageInstaller;

    move-result-object v1

    invoke-virtual {v1, p1}, Landroid/content/pm/PackageInstaller;->getSessionInfo(I)Landroid/content/pm/PackageInstaller$SessionInfo;

    move-result-object v0

    .line 43
    .local v0, "session":Landroid/content/pm/PackageInstaller$SessionInfo;
    if-nez v0, :cond_0

    const/4 v1, 0x0

    :goto_0
    return-object v1

    :cond_0
    invoke-virtual {v0}, Landroid/content/pm/PackageInstaller$SessionInfo;->getAppPackageName()Ljava/lang/String;

    move-result-object v1

    goto :goto_0
.end method


# virtual methods
.method public onActiveChanged(IZ)V
    .locals 0
    .param p1, "sessionId"    # I
    .param p2, "active"    # Z

    .prologue
    .line 62
    return-void
.end method

.method public onBadgingChanged(I)V
    .locals 0
    .param p1, "sessionId"    # I

    .prologue
    .line 57
    return-void
.end method

.method public onCreated(I)V
    .locals 2
    .param p1, "sessionId"    # I

    .prologue
    .line 48
    iget-object v0, p0, Lcom/google/android/setupwizard/util/PackageMonitor$1;->mSessionPackageNameMap:Landroid/util/SparseArray;

    invoke-direct {p0, p1}, Lcom/google/android/setupwizard/util/PackageMonitor$1;->getPackageName(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, p1, v1}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    .line 49
    iget-object v0, p0, Lcom/google/android/setupwizard/util/PackageMonitor$1;->this$0:Lcom/google/android/setupwizard/util/PackageMonitor;

    # getter for: Lcom/google/android/setupwizard/util/PackageMonitor;->mListener:Lcom/google/android/setupwizard/util/PackageMonitor$OnPackageStatusChangedListener;
    invoke-static {v0}, Lcom/google/android/setupwizard/util/PackageMonitor;->access$100(Lcom/google/android/setupwizard/util/PackageMonitor;)Lcom/google/android/setupwizard/util/PackageMonitor$OnPackageStatusChangedListener;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 50
    iget-object v0, p0, Lcom/google/android/setupwizard/util/PackageMonitor$1;->this$0:Lcom/google/android/setupwizard/util/PackageMonitor;

    # getter for: Lcom/google/android/setupwizard/util/PackageMonitor;->mListener:Lcom/google/android/setupwizard/util/PackageMonitor$OnPackageStatusChangedListener;
    invoke-static {v0}, Lcom/google/android/setupwizard/util/PackageMonitor;->access$100(Lcom/google/android/setupwizard/util/PackageMonitor;)Lcom/google/android/setupwizard/util/PackageMonitor$OnPackageStatusChangedListener;

    move-result-object v0

    invoke-direct {p0, p1}, Lcom/google/android/setupwizard/util/PackageMonitor$1;->getPackageName(I)Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/google/android/setupwizard/util/PackageMonitor$OnPackageStatusChangedListener;->onPackageEnqueued(Ljava/lang/String;)V

    .line 52
    :cond_0
    return-void
.end method

.method public onFinished(IZ)V
    .locals 2
    .param p1, "sessionId"    # I
    .param p2, "success"    # Z

    .prologue
    .line 75
    iget-object v0, p0, Lcom/google/android/setupwizard/util/PackageMonitor$1;->this$0:Lcom/google/android/setupwizard/util/PackageMonitor;

    # getter for: Lcom/google/android/setupwizard/util/PackageMonitor;->mListener:Lcom/google/android/setupwizard/util/PackageMonitor$OnPackageStatusChangedListener;
    invoke-static {v0}, Lcom/google/android/setupwizard/util/PackageMonitor;->access$100(Lcom/google/android/setupwizard/util/PackageMonitor;)Lcom/google/android/setupwizard/util/PackageMonitor$OnPackageStatusChangedListener;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 76
    iget-object v0, p0, Lcom/google/android/setupwizard/util/PackageMonitor$1;->this$0:Lcom/google/android/setupwizard/util/PackageMonitor;

    # getter for: Lcom/google/android/setupwizard/util/PackageMonitor;->mListener:Lcom/google/android/setupwizard/util/PackageMonitor$OnPackageStatusChangedListener;
    invoke-static {v0}, Lcom/google/android/setupwizard/util/PackageMonitor;->access$100(Lcom/google/android/setupwizard/util/PackageMonitor;)Lcom/google/android/setupwizard/util/PackageMonitor$OnPackageStatusChangedListener;

    move-result-object v1

    iget-object v0, p0, Lcom/google/android/setupwizard/util/PackageMonitor$1;->mSessionPackageNameMap:Landroid/util/SparseArray;

    invoke-virtual {v0, p1}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-interface {v1, v0, p2}, Lcom/google/android/setupwizard/util/PackageMonitor$OnPackageStatusChangedListener;->onPackageDequeued(Ljava/lang/String;Z)V

    .line 78
    :cond_0
    return-void
.end method

.method public onProgressChanged(IF)V
    .locals 3
    .param p1, "sessionId"    # I
    .param p2, "progress"    # F

    .prologue
    .line 67
    iget-object v0, p0, Lcom/google/android/setupwizard/util/PackageMonitor$1;->this$0:Lcom/google/android/setupwizard/util/PackageMonitor;

    # getter for: Lcom/google/android/setupwizard/util/PackageMonitor;->mListener:Lcom/google/android/setupwizard/util/PackageMonitor$OnPackageStatusChangedListener;
    invoke-static {v0}, Lcom/google/android/setupwizard/util/PackageMonitor;->access$100(Lcom/google/android/setupwizard/util/PackageMonitor;)Lcom/google/android/setupwizard/util/PackageMonitor$OnPackageStatusChangedListener;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 68
    iget-object v0, p0, Lcom/google/android/setupwizard/util/PackageMonitor$1;->this$0:Lcom/google/android/setupwizard/util/PackageMonitor;

    # getter for: Lcom/google/android/setupwizard/util/PackageMonitor;->mListener:Lcom/google/android/setupwizard/util/PackageMonitor$OnPackageStatusChangedListener;
    invoke-static {v0}, Lcom/google/android/setupwizard/util/PackageMonitor;->access$100(Lcom/google/android/setupwizard/util/PackageMonitor;)Lcom/google/android/setupwizard/util/PackageMonitor$OnPackageStatusChangedListener;

    move-result-object v1

    iget-object v0, p0, Lcom/google/android/setupwizard/util/PackageMonitor$1;->mSessionPackageNameMap:Landroid/util/SparseArray;

    invoke-virtual {v0, p1}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    const/high16 v2, 0x42c80000    # 100.0f

    mul-float/2addr v2, p2

    float-to-int v2, v2

    invoke-interface {v1, v0, v2}, Lcom/google/android/setupwizard/util/PackageMonitor$OnPackageStatusChangedListener;->onPackageInstalling(Ljava/lang/String;I)V

    .line 71
    :cond_0
    return-void
.end method
