.class public Lcom/google/android/setupwizard/account/RestoreStartWrapper$GetRestoreFlowFragment;
.super Landroid/app/Fragment;
.source "RestoreStartWrapper.java"

# interfaces
.implements Lcom/google/android/setupwizard/util/GooglePlaySetupHelper$GooglePlayConnectionListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/setupwizard/account/RestoreStartWrapper;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "GetRestoreFlowFragment"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/setupwizard/account/RestoreStartWrapper$GetRestoreFlowFragment$PlayGetRestoreFlowTask;
    }
.end annotation


# instance fields
.field private mActivity:Lcom/google/android/setupwizard/account/RestoreStartWrapper;

.field private mIntent:Landroid/content/Intent;

.field private mIntentReady:Z

.field private mPlayHelper:Lcom/google/android/setupwizard/util/GooglePlaySetupHelper;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 230
    invoke-direct {p0}, Landroid/app/Fragment;-><init>()V

    .line 227
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/setupwizard/account/RestoreStartWrapper$GetRestoreFlowFragment;->mIntentReady:Z

    .line 231
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/google/android/setupwizard/account/RestoreStartWrapper$GetRestoreFlowFragment;->setRetainInstance(Z)V

    .line 232
    return-void
.end method

.method static synthetic access$400(Lcom/google/android/setupwizard/account/RestoreStartWrapper$GetRestoreFlowFragment;)Lcom/google/android/setupwizard/account/RestoreStartWrapper;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/setupwizard/account/RestoreStartWrapper$GetRestoreFlowFragment;

    .prologue
    .line 211
    iget-object v0, p0, Lcom/google/android/setupwizard/account/RestoreStartWrapper$GetRestoreFlowFragment;->mActivity:Lcom/google/android/setupwizard/account/RestoreStartWrapper;

    return-object v0
.end method

.method static synthetic access$600(Lcom/google/android/setupwizard/account/RestoreStartWrapper$GetRestoreFlowFragment;)Lcom/google/android/setupwizard/util/GooglePlaySetupHelper;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/setupwizard/account/RestoreStartWrapper$GetRestoreFlowFragment;

    .prologue
    .line 211
    iget-object v0, p0, Lcom/google/android/setupwizard/account/RestoreStartWrapper$GetRestoreFlowFragment;->mPlayHelper:Lcom/google/android/setupwizard/util/GooglePlaySetupHelper;

    return-object v0
.end method

.method static synthetic access$702(Lcom/google/android/setupwizard/account/RestoreStartWrapper$GetRestoreFlowFragment;Landroid/content/Intent;)Landroid/content/Intent;
    .locals 0
    .param p0, "x0"    # Lcom/google/android/setupwizard/account/RestoreStartWrapper$GetRestoreFlowFragment;
    .param p1, "x1"    # Landroid/content/Intent;

    .prologue
    .line 211
    iput-object p1, p0, Lcom/google/android/setupwizard/account/RestoreStartWrapper$GetRestoreFlowFragment;->mIntent:Landroid/content/Intent;

    return-object p1
.end method

.method static synthetic access$802(Lcom/google/android/setupwizard/account/RestoreStartWrapper$GetRestoreFlowFragment;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/google/android/setupwizard/account/RestoreStartWrapper$GetRestoreFlowFragment;
    .param p1, "x1"    # Z

    .prologue
    .line 211
    iput-boolean p1, p0, Lcom/google/android/setupwizard/account/RestoreStartWrapper$GetRestoreFlowFragment;->mIntentReady:Z

    return p1
.end method

.method static synthetic access$900(Lcom/google/android/setupwizard/account/RestoreStartWrapper$GetRestoreFlowFragment;)V
    .locals 0
    .param p0, "x0"    # Lcom/google/android/setupwizard/account/RestoreStartWrapper$GetRestoreFlowFragment;

    .prologue
    .line 211
    invoke-direct {p0}, Lcom/google/android/setupwizard/account/RestoreStartWrapper$GetRestoreFlowFragment;->launchRestorePickerIfReady()V

    return-void
.end method

.method public static attach(Landroid/app/FragmentManager;)V
    .locals 3
    .param p0, "fm"    # Landroid/app/FragmentManager;

    .prologue
    .line 219
    const-string v0, "getRestore"

    invoke-virtual {p0, v0}, Landroid/app/FragmentManager;->findFragmentByTag(Ljava/lang/String;)Landroid/app/Fragment;

    move-result-object v0

    if-nez v0, :cond_0

    .line 220
    invoke-virtual {p0}, Landroid/app/FragmentManager;->beginTransaction()Landroid/app/FragmentTransaction;

    move-result-object v0

    new-instance v1, Lcom/google/android/setupwizard/account/RestoreStartWrapper$GetRestoreFlowFragment;

    invoke-direct {v1}, Lcom/google/android/setupwizard/account/RestoreStartWrapper$GetRestoreFlowFragment;-><init>()V

    const-string v2, "getRestore"

    invoke-virtual {v0, v1, v2}, Landroid/app/FragmentTransaction;->add(Landroid/app/Fragment;Ljava/lang/String;)Landroid/app/FragmentTransaction;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/FragmentTransaction;->commit()I

    .line 223
    :cond_0
    return-void
.end method

.method private launchRestorePickerIfReady()V
    .locals 3

    .prologue
    .line 259
    iget-boolean v0, p0, Lcom/google/android/setupwizard/account/RestoreStartWrapper$GetRestoreFlowFragment;->mIntentReady:Z

    if-nez v0, :cond_0

    .line 272
    :goto_0
    return-void

    .line 262
    :cond_0
    iget-object v0, p0, Lcom/google/android/setupwizard/account/RestoreStartWrapper$GetRestoreFlowFragment;->mIntent:Landroid/content/Intent;

    if-eqz v0, :cond_1

    .line 263
    const-string v0, "SetupWizard.GetRestoreFragment"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Starting restore set picker activity: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/setupwizard/account/RestoreStartWrapper$GetRestoreFlowFragment;->mIntent:Landroid/content/Intent;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 264
    iget-object v0, p0, Lcom/google/android/setupwizard/account/RestoreStartWrapper$GetRestoreFlowFragment;->mActivity:Lcom/google/android/setupwizard/account/RestoreStartWrapper;

    iget-object v1, p0, Lcom/google/android/setupwizard/account/RestoreStartWrapper$GetRestoreFlowFragment;->mIntent:Landroid/content/Intent;

    # invokes: Lcom/google/android/setupwizard/account/RestoreStartWrapper;->launchSubactivity(Landroid/content/Intent;)V
    invoke-static {v0, v1}, Lcom/google/android/setupwizard/account/RestoreStartWrapper;->access$100(Lcom/google/android/setupwizard/account/RestoreStartWrapper;Landroid/content/Intent;)V

    .line 271
    :goto_1
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/setupwizard/account/RestoreStartWrapper$GetRestoreFlowFragment;->mIntentReady:Z

    goto :goto_0

    .line 266
    :cond_1
    const-string v0, "SetupWizard.GetRestoreFragment"

    const-string v1, "Null intent received for restore picker"

    invoke-static {v0, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 267
    iget-object v0, p0, Lcom/google/android/setupwizard/account/RestoreStartWrapper$GetRestoreFlowFragment;->mActivity:Lcom/google/android/setupwizard/account/RestoreStartWrapper;

    # invokes: Lcom/google/android/setupwizard/account/RestoreStartWrapper;->nextScreen()V
    invoke-static {v0}, Lcom/google/android/setupwizard/account/RestoreStartWrapper;->access$200(Lcom/google/android/setupwizard/account/RestoreStartWrapper;)V

    .line 268
    iget-object v0, p0, Lcom/google/android/setupwizard/account/RestoreStartWrapper$GetRestoreFlowFragment;->mActivity:Lcom/google/android/setupwizard/account/RestoreStartWrapper;

    invoke-virtual {v0}, Lcom/google/android/setupwizard/account/RestoreStartWrapper;->finish()V

    goto :goto_1
.end method


# virtual methods
.method public onAttach(Landroid/app/Activity;)V
    .locals 0
    .param p1, "activity"    # Landroid/app/Activity;

    .prologue
    .line 236
    invoke-super {p0, p1}, Landroid/app/Fragment;->onAttach(Landroid/app/Activity;)V

    .line 237
    check-cast p1, Lcom/google/android/setupwizard/account/RestoreStartWrapper;

    .end local p1    # "activity":Landroid/app/Activity;
    iput-object p1, p0, Lcom/google/android/setupwizard/account/RestoreStartWrapper$GetRestoreFlowFragment;->mActivity:Lcom/google/android/setupwizard/account/RestoreStartWrapper;

    .line 238
    return-void
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 1
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 242
    invoke-super {p0, p1}, Landroid/app/Fragment;->onCreate(Landroid/os/Bundle;)V

    .line 243
    iget-object v0, p0, Lcom/google/android/setupwizard/account/RestoreStartWrapper$GetRestoreFlowFragment;->mActivity:Lcom/google/android/setupwizard/account/RestoreStartWrapper;

    invoke-virtual {v0}, Lcom/google/android/setupwizard/account/RestoreStartWrapper;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0, p0}, Lcom/google/android/setupwizard/util/GooglePlaySetupHelper;->connect(Landroid/content/Context;Lcom/google/android/setupwizard/util/GooglePlaySetupHelper$GooglePlayConnectionListener;)Lcom/google/android/setupwizard/util/GooglePlaySetupHelper;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/setupwizard/account/RestoreStartWrapper$GetRestoreFlowFragment;->mPlayHelper:Lcom/google/android/setupwizard/util/GooglePlaySetupHelper;

    .line 244
    return-void
.end method

.method public onDestroy()V
    .locals 1

    .prologue
    .line 254
    iget-object v0, p0, Lcom/google/android/setupwizard/account/RestoreStartWrapper$GetRestoreFlowFragment;->mPlayHelper:Lcom/google/android/setupwizard/util/GooglePlaySetupHelper;

    invoke-virtual {v0}, Lcom/google/android/setupwizard/util/GooglePlaySetupHelper;->disconnect()V

    .line 255
    invoke-super {p0}, Landroid/app/Fragment;->onDestroy()V

    .line 256
    return-void
.end method

.method public onGooglePlayConnected(Lcom/google/android/setupwizard/util/GooglePlaySetupHelper;)V
    .locals 2
    .param p1, "helper"    # Lcom/google/android/setupwizard/util/GooglePlaySetupHelper;

    .prologue
    .line 276
    const-string v0, "SetupWizard.GetRestoreFragment"

    const-string v1, "Service connected. Starting postponed launch."

    invoke-static {v0, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 277
    new-instance v0, Lcom/google/android/setupwizard/account/RestoreStartWrapper$GetRestoreFlowFragment$PlayGetRestoreFlowTask;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/google/android/setupwizard/account/RestoreStartWrapper$GetRestoreFlowFragment$PlayGetRestoreFlowTask;-><init>(Lcom/google/android/setupwizard/account/RestoreStartWrapper$GetRestoreFlowFragment;Lcom/google/android/setupwizard/account/RestoreStartWrapper$1;)V

    const/4 v1, 0x0

    new-array v1, v1, [Ljava/lang/Void;

    invoke-virtual {v0, v1}, Lcom/google/android/setupwizard/account/RestoreStartWrapper$GetRestoreFlowFragment$PlayGetRestoreFlowTask;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    .line 278
    return-void
.end method

.method public onResume()V
    .locals 0

    .prologue
    .line 248
    invoke-super {p0}, Landroid/app/Fragment;->onResume()V

    .line 249
    invoke-direct {p0}, Lcom/google/android/setupwizard/account/RestoreStartWrapper$GetRestoreFlowFragment;->launchRestorePickerIfReady()V

    .line 250
    return-void
.end method
