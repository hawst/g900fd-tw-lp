.class Lcom/google/android/setupwizard/network/WalledGardenActivity$MyWebViewClient;
.super Landroid/webkit/WebViewClient;
.source "WalledGardenActivity.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/setupwizard/network/WalledGardenActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "MyWebViewClient"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/setupwizard/network/WalledGardenActivity;


# direct methods
.method private constructor <init>(Lcom/google/android/setupwizard/network/WalledGardenActivity;)V
    .locals 0

    .prologue
    .line 267
    iput-object p1, p0, Lcom/google/android/setupwizard/network/WalledGardenActivity$MyWebViewClient;->this$0:Lcom/google/android/setupwizard/network/WalledGardenActivity;

    invoke-direct {p0}, Landroid/webkit/WebViewClient;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/google/android/setupwizard/network/WalledGardenActivity;Lcom/google/android/setupwizard/network/WalledGardenActivity$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/google/android/setupwizard/network/WalledGardenActivity;
    .param p2, "x1"    # Lcom/google/android/setupwizard/network/WalledGardenActivity$1;

    .prologue
    .line 267
    invoke-direct {p0, p1}, Lcom/google/android/setupwizard/network/WalledGardenActivity$MyWebViewClient;-><init>(Lcom/google/android/setupwizard/network/WalledGardenActivity;)V

    return-void
.end method


# virtual methods
.method public onPageFinished(Landroid/webkit/WebView;Ljava/lang/String;)V
    .locals 1
    .param p1, "view"    # Landroid/webkit/WebView;
    .param p2, "url"    # Ljava/lang/String;

    .prologue
    .line 291
    iget-object v0, p0, Lcom/google/android/setupwizard/network/WalledGardenActivity$MyWebViewClient;->this$0:Lcom/google/android/setupwizard/network/WalledGardenActivity;

    # invokes: Lcom/google/android/setupwizard/network/WalledGardenActivity;->onPageLoadFinished(Ljava/lang/String;)V
    invoke-static {v0, p2}, Lcom/google/android/setupwizard/network/WalledGardenActivity;->access$700(Lcom/google/android/setupwizard/network/WalledGardenActivity;Ljava/lang/String;)V

    .line 292
    return-void
.end method

.method public onPageStarted(Landroid/webkit/WebView;Ljava/lang/String;Landroid/graphics/Bitmap;)V
    .locals 0
    .param p1, "view"    # Landroid/webkit/WebView;
    .param p2, "url"    # Ljava/lang/String;
    .param p3, "favicon"    # Landroid/graphics/Bitmap;

    .prologue
    .line 279
    return-void
.end method

.method public onReceivedError(Landroid/webkit/WebView;ILjava/lang/String;Ljava/lang/String;)V
    .locals 5
    .param p1, "view"    # Landroid/webkit/WebView;
    .param p2, "errorCode"    # I
    .param p3, "description"    # Ljava/lang/String;
    .param p4, "failingUrl"    # Ljava/lang/String;

    .prologue
    .line 284
    iget-object v0, p0, Lcom/google/android/setupwizard/network/WalledGardenActivity$MyWebViewClient;->this$0:Lcom/google/android/setupwizard/network/WalledGardenActivity;

    # getter for: Lcom/google/android/setupwizard/network/WalledGardenActivity;->TAG:Ljava/lang/String;
    invoke-static {v0}, Lcom/google/android/setupwizard/network/WalledGardenActivity;->access$600(Lcom/google/android/setupwizard/network/WalledGardenActivity;)Ljava/lang/String;

    move-result-object v0

    const-string v1, "onReceivedError: errorCode %d, description: %s, url: %s"

    const/4 v2, 0x3

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v2, v3

    const/4 v3, 0x1

    aput-object p3, v2, v3

    const/4 v3, 0x2

    aput-object p4, v2, v3

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 286
    invoke-super {p0, p1, p2, p3, p4}, Landroid/webkit/WebViewClient;->onReceivedError(Landroid/webkit/WebView;ILjava/lang/String;Ljava/lang/String;)V

    .line 287
    return-void
.end method

.method public shouldOverrideUrlLoading(Landroid/webkit/WebView;Ljava/lang/String;)Z
    .locals 1
    .param p1, "view"    # Landroid/webkit/WebView;
    .param p2, "url"    # Ljava/lang/String;

    .prologue
    .line 274
    const/4 v0, 0x0

    return v0
.end method
