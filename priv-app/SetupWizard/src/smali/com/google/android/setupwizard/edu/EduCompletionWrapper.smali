.class public Lcom/google/android/setupwizard/edu/EduCompletionWrapper;
.super Lcom/google/android/setupwizard/SubactivityWrapper;
.source "EduCompletionWrapper.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 24
    invoke-direct {p0}, Lcom/google/android/setupwizard/SubactivityWrapper;-><init>()V

    return-void
.end method


# virtual methods
.method protected onSubactivityLaunch()Landroid/content/Intent;
    .locals 4

    .prologue
    .line 34
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    .line 35
    .local v0, "i":Landroid/content/Intent;
    new-instance v1, Landroid/content/ComponentName;

    const-string v2, "com.google.android.nfcprovision"

    const-string v3, "com.google.android.nfcprovision.EduCompletion"

    invoke-direct {v1, v2, v3}, Landroid/content/ComponentName;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setComponent(Landroid/content/ComponentName;)Landroid/content/Intent;

    .line 37
    return-object v0
.end method

.method protected requestCode()I
    .locals 1

    .prologue
    .line 29
    const/16 v0, 0x272f

    return v0
.end method
