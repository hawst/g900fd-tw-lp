.class Lcom/google/android/setupwizard/update/EarlyUpdateActivity$EarlyUpdateFragment$1;
.super Ljava/lang/Object;
.source "EarlyUpdateActivity.java"

# interfaces
.implements Lcom/google/android/setupwizard/util/PackageMonitor$OnPackageStatusChangedListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/setupwizard/update/EarlyUpdateActivity$EarlyUpdateFragment;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/setupwizard/update/EarlyUpdateActivity$EarlyUpdateFragment;


# direct methods
.method constructor <init>(Lcom/google/android/setupwizard/update/EarlyUpdateActivity$EarlyUpdateFragment;)V
    .locals 0

    .prologue
    .line 161
    iput-object p1, p0, Lcom/google/android/setupwizard/update/EarlyUpdateActivity$EarlyUpdateFragment$1;->this$0:Lcom/google/android/setupwizard/update/EarlyUpdateActivity$EarlyUpdateFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onPackageDequeued(Ljava/lang/String;Z)V
    .locals 3
    .param p1, "packageName"    # Ljava/lang/String;
    .param p2, "success"    # Z

    .prologue
    .line 179
    const-string v0, "SetupWizard.EarlyUpdate"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "onPackageDequeued packageName="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " success="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 182
    iget-object v0, p0, Lcom/google/android/setupwizard/update/EarlyUpdateActivity$EarlyUpdateFragment$1;->this$0:Lcom/google/android/setupwizard/update/EarlyUpdateActivity$EarlyUpdateFragment;

    const/4 v1, 0x3

    # setter for: Lcom/google/android/setupwizard/update/EarlyUpdateActivity$EarlyUpdateFragment;->mState:I
    invoke-static {v0, v1}, Lcom/google/android/setupwizard/update/EarlyUpdateActivity$EarlyUpdateFragment;->access$102(Lcom/google/android/setupwizard/update/EarlyUpdateActivity$EarlyUpdateFragment;I)I

    .line 183
    iget-object v0, p0, Lcom/google/android/setupwizard/update/EarlyUpdateActivity$EarlyUpdateFragment$1;->this$0:Lcom/google/android/setupwizard/update/EarlyUpdateActivity$EarlyUpdateFragment;

    # invokes: Lcom/google/android/setupwizard/update/EarlyUpdateActivity$EarlyUpdateFragment;->isCurrentPackage(Ljava/lang/String;)Z
    invoke-static {v0, p1}, Lcom/google/android/setupwizard/update/EarlyUpdateActivity$EarlyUpdateFragment;->access$400(Lcom/google/android/setupwizard/update/EarlyUpdateActivity$EarlyUpdateFragment;Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 184
    iget-object v0, p0, Lcom/google/android/setupwizard/update/EarlyUpdateActivity$EarlyUpdateFragment$1;->this$0:Lcom/google/android/setupwizard/update/EarlyUpdateActivity$EarlyUpdateFragment;

    # getter for: Lcom/google/android/setupwizard/update/EarlyUpdateActivity$EarlyUpdateFragment;->mActivity:Lcom/google/android/setupwizard/update/EarlyUpdateActivity;
    invoke-static {v0}, Lcom/google/android/setupwizard/update/EarlyUpdateActivity$EarlyUpdateFragment;->access$300(Lcom/google/android/setupwizard/update/EarlyUpdateActivity$EarlyUpdateFragment;)Lcom/google/android/setupwizard/update/EarlyUpdateActivity;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/setupwizard/update/EarlyUpdateActivity$EarlyUpdateFragment$1;->this$0:Lcom/google/android/setupwizard/update/EarlyUpdateActivity$EarlyUpdateFragment;

    # getter for: Lcom/google/android/setupwizard/update/EarlyUpdateActivity$EarlyUpdateFragment;->mState:I
    invoke-static {v1}, Lcom/google/android/setupwizard/update/EarlyUpdateActivity$EarlyUpdateFragment;->access$100(Lcom/google/android/setupwizard/update/EarlyUpdateActivity$EarlyUpdateFragment;)I

    move-result v1

    iget-object v2, p0, Lcom/google/android/setupwizard/update/EarlyUpdateActivity$EarlyUpdateFragment$1;->this$0:Lcom/google/android/setupwizard/update/EarlyUpdateActivity$EarlyUpdateFragment;

    # getter for: Lcom/google/android/setupwizard/update/EarlyUpdateActivity$EarlyUpdateFragment;->mCurrentUpdate:Landroid/os/Bundle;
    invoke-static {v2}, Lcom/google/android/setupwizard/update/EarlyUpdateActivity$EarlyUpdateFragment;->access$200(Lcom/google/android/setupwizard/update/EarlyUpdateActivity$EarlyUpdateFragment;)Landroid/os/Bundle;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/google/android/setupwizard/update/EarlyUpdateActivity;->updateState(ILandroid/os/Bundle;)V

    .line 185
    iget-object v0, p0, Lcom/google/android/setupwizard/update/EarlyUpdateActivity$EarlyUpdateFragment$1;->this$0:Lcom/google/android/setupwizard/update/EarlyUpdateActivity$EarlyUpdateFragment;

    const/4 v1, 0x0

    # setter for: Lcom/google/android/setupwizard/update/EarlyUpdateActivity$EarlyUpdateFragment;->mCurrentUpdate:Landroid/os/Bundle;
    invoke-static {v0, v1}, Lcom/google/android/setupwizard/update/EarlyUpdateActivity$EarlyUpdateFragment;->access$202(Lcom/google/android/setupwizard/update/EarlyUpdateActivity$EarlyUpdateFragment;Landroid/os/Bundle;)Landroid/os/Bundle;

    .line 186
    iget-object v0, p0, Lcom/google/android/setupwizard/update/EarlyUpdateActivity$EarlyUpdateFragment$1;->this$0:Lcom/google/android/setupwizard/update/EarlyUpdateActivity$EarlyUpdateFragment;

    # invokes: Lcom/google/android/setupwizard/update/EarlyUpdateActivity$EarlyUpdateFragment;->requestNextUpdate()V
    invoke-static {v0}, Lcom/google/android/setupwizard/update/EarlyUpdateActivity$EarlyUpdateFragment;->access$500(Lcom/google/android/setupwizard/update/EarlyUpdateActivity$EarlyUpdateFragment;)V

    .line 188
    :cond_0
    return-void
.end method

.method public onPackageEnqueued(Ljava/lang/String;)V
    .locals 3
    .param p1, "packageName"    # Ljava/lang/String;

    .prologue
    .line 164
    const-string v0, "SetupWizard.EarlyUpdate"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "onPackageEnqueued packageName="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 165
    iget-object v0, p0, Lcom/google/android/setupwizard/update/EarlyUpdateActivity$EarlyUpdateFragment$1;->this$0:Lcom/google/android/setupwizard/update/EarlyUpdateActivity$EarlyUpdateFragment;

    const/4 v1, 0x1

    # setter for: Lcom/google/android/setupwizard/update/EarlyUpdateActivity$EarlyUpdateFragment;->mState:I
    invoke-static {v0, v1}, Lcom/google/android/setupwizard/update/EarlyUpdateActivity$EarlyUpdateFragment;->access$102(Lcom/google/android/setupwizard/update/EarlyUpdateActivity$EarlyUpdateFragment;I)I

    .line 166
    iget-object v0, p0, Lcom/google/android/setupwizard/update/EarlyUpdateActivity$EarlyUpdateFragment$1;->this$0:Lcom/google/android/setupwizard/update/EarlyUpdateActivity$EarlyUpdateFragment;

    # getter for: Lcom/google/android/setupwizard/update/EarlyUpdateActivity$EarlyUpdateFragment;->mActivity:Lcom/google/android/setupwizard/update/EarlyUpdateActivity;
    invoke-static {v0}, Lcom/google/android/setupwizard/update/EarlyUpdateActivity$EarlyUpdateFragment;->access$300(Lcom/google/android/setupwizard/update/EarlyUpdateActivity$EarlyUpdateFragment;)Lcom/google/android/setupwizard/update/EarlyUpdateActivity;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/setupwizard/update/EarlyUpdateActivity$EarlyUpdateFragment$1;->this$0:Lcom/google/android/setupwizard/update/EarlyUpdateActivity$EarlyUpdateFragment;

    # getter for: Lcom/google/android/setupwizard/update/EarlyUpdateActivity$EarlyUpdateFragment;->mState:I
    invoke-static {v1}, Lcom/google/android/setupwizard/update/EarlyUpdateActivity$EarlyUpdateFragment;->access$100(Lcom/google/android/setupwizard/update/EarlyUpdateActivity$EarlyUpdateFragment;)I

    move-result v1

    iget-object v2, p0, Lcom/google/android/setupwizard/update/EarlyUpdateActivity$EarlyUpdateFragment$1;->this$0:Lcom/google/android/setupwizard/update/EarlyUpdateActivity$EarlyUpdateFragment;

    # getter for: Lcom/google/android/setupwizard/update/EarlyUpdateActivity$EarlyUpdateFragment;->mCurrentUpdate:Landroid/os/Bundle;
    invoke-static {v2}, Lcom/google/android/setupwizard/update/EarlyUpdateActivity$EarlyUpdateFragment;->access$200(Lcom/google/android/setupwizard/update/EarlyUpdateActivity$EarlyUpdateFragment;)Landroid/os/Bundle;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/google/android/setupwizard/update/EarlyUpdateActivity;->updateState(ILandroid/os/Bundle;)V

    .line 167
    return-void
.end method

.method public onPackageInstalling(Ljava/lang/String;I)V
    .locals 3
    .param p1, "packageName"    # Ljava/lang/String;
    .param p2, "progress"    # I

    .prologue
    .line 171
    const-string v0, "SetupWizard.EarlyUpdate"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "onPackageInstalling packageName="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " progress="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 173
    iget-object v0, p0, Lcom/google/android/setupwizard/update/EarlyUpdateActivity$EarlyUpdateFragment$1;->this$0:Lcom/google/android/setupwizard/update/EarlyUpdateActivity$EarlyUpdateFragment;

    const/4 v1, 0x2

    # setter for: Lcom/google/android/setupwizard/update/EarlyUpdateActivity$EarlyUpdateFragment;->mState:I
    invoke-static {v0, v1}, Lcom/google/android/setupwizard/update/EarlyUpdateActivity$EarlyUpdateFragment;->access$102(Lcom/google/android/setupwizard/update/EarlyUpdateActivity$EarlyUpdateFragment;I)I

    .line 174
    iget-object v0, p0, Lcom/google/android/setupwizard/update/EarlyUpdateActivity$EarlyUpdateFragment$1;->this$0:Lcom/google/android/setupwizard/update/EarlyUpdateActivity$EarlyUpdateFragment;

    # getter for: Lcom/google/android/setupwizard/update/EarlyUpdateActivity$EarlyUpdateFragment;->mActivity:Lcom/google/android/setupwizard/update/EarlyUpdateActivity;
    invoke-static {v0}, Lcom/google/android/setupwizard/update/EarlyUpdateActivity$EarlyUpdateFragment;->access$300(Lcom/google/android/setupwizard/update/EarlyUpdateActivity$EarlyUpdateFragment;)Lcom/google/android/setupwizard/update/EarlyUpdateActivity;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/setupwizard/update/EarlyUpdateActivity$EarlyUpdateFragment$1;->this$0:Lcom/google/android/setupwizard/update/EarlyUpdateActivity$EarlyUpdateFragment;

    # getter for: Lcom/google/android/setupwizard/update/EarlyUpdateActivity$EarlyUpdateFragment;->mState:I
    invoke-static {v1}, Lcom/google/android/setupwizard/update/EarlyUpdateActivity$EarlyUpdateFragment;->access$100(Lcom/google/android/setupwizard/update/EarlyUpdateActivity$EarlyUpdateFragment;)I

    move-result v1

    iget-object v2, p0, Lcom/google/android/setupwizard/update/EarlyUpdateActivity$EarlyUpdateFragment$1;->this$0:Lcom/google/android/setupwizard/update/EarlyUpdateActivity$EarlyUpdateFragment;

    # getter for: Lcom/google/android/setupwizard/update/EarlyUpdateActivity$EarlyUpdateFragment;->mCurrentUpdate:Landroid/os/Bundle;
    invoke-static {v2}, Lcom/google/android/setupwizard/update/EarlyUpdateActivity$EarlyUpdateFragment;->access$200(Lcom/google/android/setupwizard/update/EarlyUpdateActivity$EarlyUpdateFragment;)Landroid/os/Bundle;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/google/android/setupwizard/update/EarlyUpdateActivity;->updateState(ILandroid/os/Bundle;)V

    .line 175
    return-void
.end method
