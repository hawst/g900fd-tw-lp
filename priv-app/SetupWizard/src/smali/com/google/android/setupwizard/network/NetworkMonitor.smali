.class public Lcom/google/android/setupwizard/network/NetworkMonitor;
.super Ljava/lang/Object;
.source "NetworkMonitor.java"


# static fields
.field private static final LOCAL_LOGV:Z

.field private static sInstance:Lcom/google/android/setupwizard/network/NetworkMonitor;


# instance fields
.field private final mBroadcastReceiver:Landroid/content/BroadcastReceiver;

.field private mContext:Landroid/content/Context;

.field private mNetworkConnected:Z


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 40
    const-string v0, "NetworkMonitor"

    const/4 v1, 0x2

    invoke-static {v0, v1}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    sput-boolean v0, Lcom/google/android/setupwizard/network/NetworkMonitor;->LOCAL_LOGV:Z

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 3
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 67
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 57
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/google/android/setupwizard/network/NetworkMonitor;->mContext:Landroid/content/Context;

    .line 59
    const/4 v1, 0x0

    iput-boolean v1, p0, Lcom/google/android/setupwizard/network/NetworkMonitor;->mNetworkConnected:Z

    .line 103
    new-instance v1, Lcom/google/android/setupwizard/network/NetworkMonitor$2;

    invoke-direct {v1, p0}, Lcom/google/android/setupwizard/network/NetworkMonitor$2;-><init>(Lcom/google/android/setupwizard/network/NetworkMonitor;)V

    iput-object v1, p0, Lcom/google/android/setupwizard/network/NetworkMonitor;->mBroadcastReceiver:Landroid/content/BroadcastReceiver;

    .line 68
    iput-object p1, p0, Lcom/google/android/setupwizard/network/NetworkMonitor;->mContext:Landroid/content/Context;

    .line 69
    sget-boolean v1, Lcom/google/android/setupwizard/network/NetworkMonitor;->LOCAL_LOGV:Z

    if-eqz v1, :cond_0

    const-string v1, "NetworkMonitor"

    const-string v2, "Starting NetworkMonitor"

    invoke-static {v1, v2}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 71
    :cond_0
    new-instance v0, Landroid/content/IntentFilter;

    invoke-direct {v0}, Landroid/content/IntentFilter;-><init>()V

    .line 72
    .local v0, "filter":Landroid/content/IntentFilter;
    const-string v1, "android.net.conn.CONNECTIVITY_CHANGE"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 78
    const-string v1, "android.net.conn.CONNECTIVITY_CHANGE_IMMEDIATE"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 80
    iget-object v1, p0, Lcom/google/android/setupwizard/network/NetworkMonitor;->mBroadcastReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {p1, v1, v0}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    .line 84
    new-instance v1, Landroid/os/Handler;

    invoke-direct {v1}, Landroid/os/Handler;-><init>()V

    new-instance v2, Lcom/google/android/setupwizard/network/NetworkMonitor$1;

    invoke-direct {v2, p0}, Lcom/google/android/setupwizard/network/NetworkMonitor$1;-><init>(Lcom/google/android/setupwizard/network/NetworkMonitor;)V

    invoke-virtual {v1, v2}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 90
    return-void
.end method

.method static synthetic access$000(Lcom/google/android/setupwizard/network/NetworkMonitor;)Landroid/content/Context;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/setupwizard/network/NetworkMonitor;

    .prologue
    .line 35
    iget-object v0, p0, Lcom/google/android/setupwizard/network/NetworkMonitor;->mContext:Landroid/content/Context;

    return-object v0
.end method

.method static synthetic access$100(Lcom/google/android/setupwizard/network/NetworkMonitor;Landroid/content/Context;)Z
    .locals 1
    .param p0, "x0"    # Lcom/google/android/setupwizard/network/NetworkMonitor;
    .param p1, "x1"    # Landroid/content/Context;

    .prologue
    .line 35
    invoke-direct {p0, p1}, Lcom/google/android/setupwizard/network/NetworkMonitor;->updateNetworkStatus(Landroid/content/Context;)Z

    move-result v0

    return v0
.end method

.method static synthetic access$200()Z
    .locals 1

    .prologue
    .line 35
    sget-boolean v0, Lcom/google/android/setupwizard/network/NetworkMonitor;->LOCAL_LOGV:Z

    return v0
.end method

.method public static getInstance()Lcom/google/android/setupwizard/network/NetworkMonitor;
    .locals 1

    .prologue
    .line 51
    sget-object v0, Lcom/google/android/setupwizard/network/NetworkMonitor;->sInstance:Lcom/google/android/setupwizard/network/NetworkMonitor;

    return-object v0
.end method

.method public static initInstance(Landroid/content/Context;)V
    .locals 2
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 45
    sget-object v0, Lcom/google/android/setupwizard/network/NetworkMonitor;->sInstance:Lcom/google/android/setupwizard/network/NetworkMonitor;

    if-nez v0, :cond_0

    .line 46
    new-instance v0, Lcom/google/android/setupwizard/network/NetworkMonitor;

    invoke-virtual {p0}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/google/android/setupwizard/network/NetworkMonitor;-><init>(Landroid/content/Context;)V

    sput-object v0, Lcom/google/android/setupwizard/network/NetworkMonitor;->sInstance:Lcom/google/android/setupwizard/network/NetworkMonitor;

    .line 48
    :cond_0
    return-void
.end method

.method private onNetworkConnected()V
    .locals 2

    .prologue
    .line 125
    sget-boolean v0, Lcom/google/android/setupwizard/network/NetworkMonitor;->LOCAL_LOGV:Z

    if-eqz v0, :cond_0

    const-string v0, "NetworkMonitor"

    const-string v1, "onNetworkConnected()"

    invoke-static {v0, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 126
    :cond_0
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/setupwizard/network/NetworkMonitor;->mNetworkConnected:Z

    .line 128
    iget-object v0, p0, Lcom/google/android/setupwizard/network/NetworkMonitor;->mContext:Landroid/content/Context;

    invoke-static {v0}, Lcom/google/android/setupwizard/util/GservicesChangedReceiver;->tickleCheckinService(Landroid/content/Context;)V

    .line 129
    return-void
.end method

.method private onNetworkDisconnected()V
    .locals 2

    .prologue
    .line 135
    sget-boolean v0, Lcom/google/android/setupwizard/network/NetworkMonitor;->LOCAL_LOGV:Z

    if-eqz v0, :cond_0

    const-string v0, "NetworkMonitor"

    const-string v1, "onNetworkDisconnected()"

    invoke-static {v0, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 136
    :cond_0
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/setupwizard/network/NetworkMonitor;->mNetworkConnected:Z

    .line 137
    return-void
.end method

.method private updateNetworkStatus(Landroid/content/Context;)Z
    .locals 4
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 140
    const-string v3, "connectivity"

    invoke-virtual {p1, v3}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/net/ConnectivityManager;

    .line 142
    .local v0, "cm":Landroid/net/ConnectivityManager;
    if-eqz v0, :cond_0

    .line 143
    invoke-virtual {v0}, Landroid/net/ConnectivityManager;->getActiveNetworkInfo()Landroid/net/NetworkInfo;

    move-result-object v2

    .line 144
    .local v2, "ni":Landroid/net/NetworkInfo;
    if-eqz v2, :cond_1

    invoke-virtual {v2}, Landroid/net/NetworkInfo;->isConnected()Z

    move-result v3

    if-eqz v3, :cond_1

    const/4 v1, 0x1

    .line 145
    .local v1, "connected":Z
    :goto_0
    if-eqz v1, :cond_2

    iget-boolean v3, p0, Lcom/google/android/setupwizard/network/NetworkMonitor;->mNetworkConnected:Z

    if-nez v3, :cond_2

    .line 146
    invoke-direct {p0}, Lcom/google/android/setupwizard/network/NetworkMonitor;->onNetworkConnected()V

    .line 151
    .end local v1    # "connected":Z
    .end local v2    # "ni":Landroid/net/NetworkInfo;
    :cond_0
    :goto_1
    iget-boolean v3, p0, Lcom/google/android/setupwizard/network/NetworkMonitor;->mNetworkConnected:Z

    return v3

    .line 144
    .restart local v2    # "ni":Landroid/net/NetworkInfo;
    :cond_1
    const/4 v1, 0x0

    goto :goto_0

    .line 147
    .restart local v1    # "connected":Z
    :cond_2
    if-nez v1, :cond_0

    iget-boolean v3, p0, Lcom/google/android/setupwizard/network/NetworkMonitor;->mNetworkConnected:Z

    if-eqz v3, :cond_0

    .line 148
    invoke-direct {p0}, Lcom/google/android/setupwizard/network/NetworkMonitor;->onNetworkDisconnected()V

    goto :goto_1
.end method


# virtual methods
.method public checkIsNetworkConnected()Z
    .locals 1

    .prologue
    .line 117
    iget-object v0, p0, Lcom/google/android/setupwizard/network/NetworkMonitor;->mContext:Landroid/content/Context;

    invoke-direct {p0, v0}, Lcom/google/android/setupwizard/network/NetworkMonitor;->updateNetworkStatus(Landroid/content/Context;)Z

    .line 118
    invoke-virtual {p0}, Lcom/google/android/setupwizard/network/NetworkMonitor;->isNetworkConnected()Z

    move-result v0

    return v0
.end method

.method public isNetworkConnected()Z
    .locals 3

    .prologue
    .line 112
    sget-boolean v0, Lcom/google/android/setupwizard/network/NetworkMonitor;->LOCAL_LOGV:Z

    if-eqz v0, :cond_0

    const-string v0, "NetworkMonitor"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "isNetworkConnected() returns "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-boolean v2, p0, Lcom/google/android/setupwizard/network/NetworkMonitor;->mNetworkConnected:Z

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 113
    :cond_0
    iget-boolean v0, p0, Lcom/google/android/setupwizard/network/NetworkMonitor;->mNetworkConnected:Z

    return v0
.end method
