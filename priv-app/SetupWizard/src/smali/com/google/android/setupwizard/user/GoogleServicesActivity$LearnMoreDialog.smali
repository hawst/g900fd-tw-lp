.class public Lcom/google/android/setupwizard/user/GoogleServicesActivity$LearnMoreDialog;
.super Landroid/app/DialogFragment;
.source "GoogleServicesActivity.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/setupwizard/user/GoogleServicesActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "LearnMoreDialog"
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 484
    invoke-direct {p0}, Landroid/app/DialogFragment;-><init>()V

    return-void
.end method

.method public static newInstance(I)Lcom/google/android/setupwizard/user/GoogleServicesActivity$LearnMoreDialog;
    .locals 3
    .param p0, "messageRes"    # I

    .prologue
    .line 489
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 490
    .local v0, "args":Landroid/os/Bundle;
    const-string v2, "messageRes"

    invoke-virtual {v0, v2, p0}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 491
    new-instance v1, Lcom/google/android/setupwizard/user/GoogleServicesActivity$LearnMoreDialog;

    invoke-direct {v1}, Lcom/google/android/setupwizard/user/GoogleServicesActivity$LearnMoreDialog;-><init>()V

    .line 492
    .local v1, "dialog":Lcom/google/android/setupwizard/user/GoogleServicesActivity$LearnMoreDialog;
    invoke-virtual {v1, v0}, Lcom/google/android/setupwizard/user/GoogleServicesActivity$LearnMoreDialog;->setArguments(Landroid/os/Bundle;)V

    .line 493
    return-object v1
.end method


# virtual methods
.method public onCreateDialog(Landroid/os/Bundle;)Landroid/app/Dialog;
    .locals 4
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 505
    invoke-virtual {p0}, Lcom/google/android/setupwizard/user/GoogleServicesActivity$LearnMoreDialog;->getArguments()Landroid/os/Bundle;

    move-result-object v0

    .line 506
    .local v0, "args":Landroid/os/Bundle;
    new-instance v1, Landroid/app/AlertDialog$Builder;

    invoke-virtual {p0}, Lcom/google/android/setupwizard/user/GoogleServicesActivity$LearnMoreDialog;->getActivity()Landroid/app/Activity;

    move-result-object v2

    invoke-direct {v1, v2}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    const-string v2, "messageRes"

    invoke-virtual {v0, v2}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v2

    invoke-virtual {v1, v2}, Landroid/app/AlertDialog$Builder;->setMessage(I)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    const v2, 0x7f07004d

    new-instance v3, Lcom/google/android/setupwizard/user/GoogleServicesActivity$LearnMoreDialog$1;

    invoke-direct {v3, p0}, Lcom/google/android/setupwizard/user/GoogleServicesActivity$LearnMoreDialog$1;-><init>(Lcom/google/android/setupwizard/user/GoogleServicesActivity$LearnMoreDialog;)V

    invoke-virtual {v1, v2, v3}, Landroid/app/AlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    invoke-virtual {v1}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v1

    return-object v1
.end method

.method public show(Landroid/app/FragmentManager;Ljava/lang/String;)V
    .locals 1
    .param p1, "manager"    # Landroid/app/FragmentManager;
    .param p2, "tag"    # Ljava/lang/String;

    .prologue
    .line 498
    invoke-virtual {p1, p2}, Landroid/app/FragmentManager;->findFragmentByTag(Ljava/lang/String;)Landroid/app/Fragment;

    move-result-object v0

    if-nez v0, :cond_0

    .line 499
    invoke-super {p0, p1, p2}, Landroid/app/DialogFragment;->show(Landroid/app/FragmentManager;Ljava/lang/String;)V

    .line 501
    :cond_0
    return-void
.end method
