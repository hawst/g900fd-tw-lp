.class public Lcom/google/android/setupwizard/util/BackupRestoreHelper;
.super Ljava/lang/Object;
.source "BackupRestoreHelper.java"


# static fields
.field private static sInstance:Lcom/google/android/setupwizard/util/BackupRestoreHelper;


# instance fields
.field private final mBackupServiceConnection:Landroid/content/ServiceConnection;

.field private final mContext:Landroid/content/Context;

.field private final mReceiver:Landroid/content/BroadcastReceiver;

.field private mStartTime:J


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 139
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 108
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/google/android/setupwizard/util/BackupRestoreHelper;->mStartTime:J

    .line 110
    new-instance v0, Lcom/google/android/setupwizard/util/BackupRestoreHelper$1;

    invoke-direct {v0, p0}, Lcom/google/android/setupwizard/util/BackupRestoreHelper$1;-><init>(Lcom/google/android/setupwizard/util/BackupRestoreHelper;)V

    iput-object v0, p0, Lcom/google/android/setupwizard/util/BackupRestoreHelper;->mReceiver:Landroid/content/BroadcastReceiver;

    .line 121
    new-instance v0, Lcom/google/android/setupwizard/util/BackupRestoreHelper$2;

    invoke-direct {v0, p0}, Lcom/google/android/setupwizard/util/BackupRestoreHelper$2;-><init>(Lcom/google/android/setupwizard/util/BackupRestoreHelper;)V

    iput-object v0, p0, Lcom/google/android/setupwizard/util/BackupRestoreHelper;->mBackupServiceConnection:Landroid/content/ServiceConnection;

    .line 140
    iput-object p1, p0, Lcom/google/android/setupwizard/util/BackupRestoreHelper;->mContext:Landroid/content/Context;

    .line 141
    return-void
.end method

.method static synthetic access$000(Lcom/google/android/setupwizard/util/BackupRestoreHelper;Ljava/lang/String;)V
    .locals 0
    .param p0, "x0"    # Lcom/google/android/setupwizard/util/BackupRestoreHelper;
    .param p1, "x1"    # Ljava/lang/String;

    .prologue
    .line 39
    invoke-direct {p0, p1}, Lcom/google/android/setupwizard/util/BackupRestoreHelper;->saveState(Ljava/lang/String;)V

    return-void
.end method

.method static synthetic access$100(Lcom/google/android/setupwizard/util/BackupRestoreHelper;)J
    .locals 2
    .param p0, "x0"    # Lcom/google/android/setupwizard/util/BackupRestoreHelper;

    .prologue
    .line 39
    iget-wide v0, p0, Lcom/google/android/setupwizard/util/BackupRestoreHelper;->mStartTime:J

    return-wide v0
.end method

.method static synthetic access$200(Lcom/google/android/setupwizard/util/BackupRestoreHelper;)Landroid/content/Context;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/setupwizard/util/BackupRestoreHelper;

    .prologue
    .line 39
    iget-object v0, p0, Lcom/google/android/setupwizard/util/BackupRestoreHelper;->mContext:Landroid/content/Context;

    return-object v0
.end method

.method private static getLocalPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;
    .locals 2
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 100
    const-string v0, "SetupWizardLocalPrefs"

    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v0

    return-object v0
.end method

.method public static getState()Ljava/lang/String;
    .locals 1

    .prologue
    .line 76
    sget-object v0, Lcom/google/android/setupwizard/util/BackupRestoreHelper;->sInstance:Lcom/google/android/setupwizard/util/BackupRestoreHelper;

    invoke-direct {v0}, Lcom/google/android/setupwizard/util/BackupRestoreHelper;->getStateImpl()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private getStateImpl()Ljava/lang/String;
    .locals 3

    .prologue
    .line 158
    iget-object v0, p0, Lcom/google/android/setupwizard/util/BackupRestoreHelper;->mContext:Landroid/content/Context;

    invoke-static {v0}, Lcom/google/android/setupwizard/util/BackupRestoreHelper;->getLocalPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v0

    const-string v1, "restoreState"

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public static hasAttemptedRestore()Z
    .locals 1

    .prologue
    .line 80
    invoke-static {}, Lcom/google/android/setupwizard/util/BackupRestoreHelper;->getState()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static initInstance(Landroid/content/Context;)V
    .locals 2
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 66
    sget-object v0, Lcom/google/android/setupwizard/util/BackupRestoreHelper;->sInstance:Lcom/google/android/setupwizard/util/BackupRestoreHelper;

    if-nez v0, :cond_0

    .line 67
    new-instance v0, Lcom/google/android/setupwizard/util/BackupRestoreHelper;

    invoke-virtual {p0}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/google/android/setupwizard/util/BackupRestoreHelper;-><init>(Landroid/content/Context;)V

    sput-object v0, Lcom/google/android/setupwizard/util/BackupRestoreHelper;->sInstance:Lcom/google/android/setupwizard/util/BackupRestoreHelper;

    .line 69
    :cond_0
    return-void
.end method

.method private onRestoreDone()V
    .locals 2

    .prologue
    .line 144
    iget-object v0, p0, Lcom/google/android/setupwizard/util/BackupRestoreHelper;->mContext:Landroid/content/Context;

    invoke-static {v0}, Landroid/support/v4/content/LocalBroadcastManager;->getInstance(Landroid/content/Context;)Landroid/support/v4/content/LocalBroadcastManager;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/setupwizard/util/BackupRestoreHelper;->mReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {v0, v1}, Landroid/support/v4/content/LocalBroadcastManager;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    .line 145
    return-void
.end method

.method private saveState(Ljava/lang/String;)V
    .locals 2
    .param p1, "state"    # Ljava/lang/String;

    .prologue
    .line 148
    iget-object v0, p0, Lcom/google/android/setupwizard/util/BackupRestoreHelper;->mContext:Landroid/content/Context;

    invoke-static {v0}, Lcom/google/android/setupwizard/util/BackupRestoreHelper;->getLocalPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    const-string v1, "restoreState"

    invoke-interface {v0, v1, p1}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->apply()V

    .line 151
    const-string v0, "finished"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    const-string v0, "failed"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 153
    :cond_0
    invoke-direct {p0}, Lcom/google/android/setupwizard/util/BackupRestoreHelper;->onRestoreDone()V

    .line 155
    :cond_1
    return-void
.end method

.method public static setBackupAccount(Landroid/accounts/Account;)Z
    .locals 1
    .param p0, "account"    # Landroid/accounts/Account;

    .prologue
    .line 96
    sget-object v0, Lcom/google/android/setupwizard/util/BackupRestoreHelper;->sInstance:Lcom/google/android/setupwizard/util/BackupRestoreHelper;

    invoke-direct {v0, p0}, Lcom/google/android/setupwizard/util/BackupRestoreHelper;->setBackupAccountImpl(Landroid/accounts/Account;)Z

    move-result v0

    return v0
.end method

.method private setBackupAccountImpl(Landroid/accounts/Account;)Z
    .locals 6
    .param p1, "account"    # Landroid/accounts/Account;

    .prologue
    const/4 v2, 0x0

    .line 178
    invoke-direct {p0}, Lcom/google/android/setupwizard/util/BackupRestoreHelper;->getStateImpl()Ljava/lang/String;

    move-result-object v3

    if-eqz v3, :cond_0

    .line 179
    const-string v3, "SetupWizard.BackupRestoreHelper"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Restore already started. Current state: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-direct {p0}, Lcom/google/android/setupwizard/util/BackupRestoreHelper;->getStateImpl()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 192
    :goto_0
    return v2

    .line 182
    :cond_0
    const-string v3, "SetupWizard.BackupRestoreHelper"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Setting backup account: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 183
    new-instance v1, Landroid/content/Intent;

    const-string v3, "com.google.android.backup.SetBackupAccount"

    invoke-direct {v1, v3}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 184
    .local v1, "intent":Landroid/content/Intent;
    const-string v3, "backupAccount"

    invoke-virtual {v1, v3, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 185
    const-string v3, "backupUserHandle"

    invoke-static {}, Landroid/os/Process;->myUserHandle()Landroid/os/UserHandle;

    move-result-object v4

    invoke-virtual {v1, v3, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 186
    const-string v3, "com.google.android.backuptransport"

    invoke-virtual {v1, v3}, Landroid/content/Intent;->setPackage(Ljava/lang/String;)Landroid/content/Intent;

    .line 188
    :try_start_0
    iget-object v3, p0, Lcom/google/android/setupwizard/util/BackupRestoreHelper;->mContext:Landroid/content/Context;

    invoke-virtual {v3, v1}, Landroid/content/Context;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;
    :try_end_0
    .catch Landroid/content/ActivityNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    .line 189
    const/4 v2, 0x1

    goto :goto_0

    .line 190
    :catch_0
    move-exception v0

    .line 191
    .local v0, "e":Landroid/content/ActivityNotFoundException;
    const-string v3, "SetupWizard.BackupRestoreHelper"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Could not set backup account "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method public static setBackupEnabled(Z)Z
    .locals 1
    .param p0, "enabled"    # Z

    .prologue
    .line 92
    sget-object v0, Lcom/google/android/setupwizard/util/BackupRestoreHelper;->sInstance:Lcom/google/android/setupwizard/util/BackupRestoreHelper;

    invoke-direct {v0, p0}, Lcom/google/android/setupwizard/util/BackupRestoreHelper;->setBackupEnabledImpl(Z)Z

    move-result v0

    return v0
.end method

.method private setBackupEnabledImpl(Z)Z
    .locals 6
    .param p1, "enabled"    # Z

    .prologue
    const/4 v2, 0x0

    .line 162
    new-instance v1, Landroid/content/Intent;

    const-string v3, "com.google.android.backup.BackupEnabler"

    invoke-direct {v1, v3}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 163
    .local v1, "intent":Landroid/content/Intent;
    const-string v3, "BACKUP_ENABLE"

    invoke-virtual {v1, v3, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 164
    const-string v3, "com.google.android.backuptransport"

    invoke-virtual {v1, v3}, Landroid/content/Intent;->setPackage(Ljava/lang/String;)Landroid/content/Intent;

    .line 166
    :try_start_0
    iget-object v3, p0, Lcom/google/android/setupwizard/util/BackupRestoreHelper;->mContext:Landroid/content/Context;

    invoke-virtual {v3, v1}, Landroid/content/Context;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;
    :try_end_0
    .catch Ljava/lang/SecurityException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Landroid/content/ActivityNotFoundException; {:try_start_0 .. :try_end_0} :catch_1

    .line 167
    const/4 v2, 0x1

    .line 173
    :goto_0
    return v2

    .line 168
    :catch_0
    move-exception v0

    .line 169
    .local v0, "e":Ljava/lang/SecurityException;
    const-string v3, "SetupWizard.BackupRestoreHelper"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Could not set backup enabled "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 171
    .end local v0    # "e":Ljava/lang/SecurityException;
    :catch_1
    move-exception v0

    .line 172
    .local v0, "e":Landroid/content/ActivityNotFoundException;
    const-string v3, "SetupWizard.BackupRestoreHelper"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Could not set backup enabled "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method public static startRestore(J)V
    .locals 2
    .param p0, "restoreToken"    # J

    .prologue
    .line 88
    sget-object v0, Lcom/google/android/setupwizard/util/BackupRestoreHelper;->sInstance:Lcom/google/android/setupwizard/util/BackupRestoreHelper;

    invoke-direct {v0, p0, p1}, Lcom/google/android/setupwizard/util/BackupRestoreHelper;->startRestoreImpl(J)V

    .line 89
    return-void
.end method

.method private startRestoreImpl(J)V
    .locals 7
    .param p1, "restoreToken"    # J

    .prologue
    .line 197
    invoke-direct {p0}, Lcom/google/android/setupwizard/util/BackupRestoreHelper;->getStateImpl()Ljava/lang/String;

    move-result-object v4

    if-eqz v4, :cond_0

    .line 198
    const-string v4, "SetupWizard.BackupRestoreHelper"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "Restore already started. Current state: "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-direct {p0}, Lcom/google/android/setupwizard/util/BackupRestoreHelper;->getStateImpl()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 224
    :goto_0
    return-void

    .line 201
    :cond_0
    new-instance v1, Landroid/content/IntentFilter;

    const-string v4, "com.google.android.setupwizard.RESTORE_STATE_CHANGE"

    invoke-direct {v1, v4}, Landroid/content/IntentFilter;-><init>(Ljava/lang/String;)V

    .line 202
    .local v1, "filter":Landroid/content/IntentFilter;
    iget-object v4, p0, Lcom/google/android/setupwizard/util/BackupRestoreHelper;->mContext:Landroid/content/Context;

    invoke-static {v4}, Landroid/support/v4/content/LocalBroadcastManager;->getInstance(Landroid/content/Context;)Landroid/support/v4/content/LocalBroadcastManager;

    move-result-object v4

    iget-object v5, p0, Lcom/google/android/setupwizard/util/BackupRestoreHelper;->mReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {v4, v5, v1}, Landroid/support/v4/content/LocalBroadcastManager;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)V

    .line 204
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v4

    iput-wide v4, p0, Lcom/google/android/setupwizard/util/BackupRestoreHelper;->mStartTime:J

    .line 205
    new-instance v2, Landroid/content/Intent;

    iget-object v4, p0, Lcom/google/android/setupwizard/util/BackupRestoreHelper;->mContext:Landroid/content/Context;

    const-class v5, Lcom/google/android/setupwizard/account/RestoreService;

    invoke-direct {v2, v4, v5}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 206
    .local v2, "intent":Landroid/content/Intent;
    const-wide/16 v4, 0x0

    cmp-long v4, p1, v4

    if-eqz v4, :cond_1

    .line 207
    const-string v4, "restoreToken"

    invoke-virtual {v2, v4, p1, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;J)Landroid/content/Intent;

    .line 213
    :goto_1
    :try_start_0
    iget-object v4, p0, Lcom/google/android/setupwizard/util/BackupRestoreHelper;->mContext:Landroid/content/Context;

    iget-object v5, p0, Lcom/google/android/setupwizard/util/BackupRestoreHelper;->mBackupServiceConnection:Landroid/content/ServiceConnection;

    const/4 v6, 0x0

    invoke-virtual {v4, v2, v5, v6}, Landroid/content/Context;->bindService(Landroid/content/Intent;Landroid/content/ServiceConnection;I)Z

    move-result v4

    if-eqz v4, :cond_2

    .line 214
    iget-object v4, p0, Lcom/google/android/setupwizard/util/BackupRestoreHelper;->mContext:Landroid/content/Context;

    invoke-virtual {v4, v2}, Landroid/content/Context;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;

    move-result-object v3

    .line 215
    .local v3, "result":Landroid/content/ComponentName;
    const-string v4, "SetupWizard.BackupRestoreHelper"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "Started restoring service:"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, ", token: "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, p1, p2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catch Landroid/content/ActivityNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 220
    .end local v3    # "result":Landroid/content/ComponentName;
    :catch_0
    move-exception v0

    .line 221
    .local v0, "e":Landroid/content/ActivityNotFoundException;
    const-string v4, "SetupWizard.BackupRestoreHelper"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "Could not restore "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 222
    const-string v4, "failed"

    invoke-direct {p0, v4}, Lcom/google/android/setupwizard/util/BackupRestoreHelper;->saveState(Ljava/lang/String;)V

    goto :goto_0

    .line 210
    .end local v0    # "e":Landroid/content/ActivityNotFoundException;
    :cond_1
    const-string v4, "SetupWizard.BackupRestoreHelper"

    const-string v5, "startRestoreImpl called with restoreToken = 0"

    invoke-static {v4, v5}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1

    .line 217
    :cond_2
    :try_start_1
    const-string v4, "SetupWizard.BackupRestoreHelper"

    const-string v5, "Failed to start restoring service"

    invoke-static {v4, v5}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 218
    const-string v4, "failed"

    invoke-direct {p0, v4}, Lcom/google/android/setupwizard/util/BackupRestoreHelper;->saveState(Ljava/lang/String;)V
    :try_end_1
    .catch Landroid/content/ActivityNotFoundException; {:try_start_1 .. :try_end_1} :catch_0

    goto/16 :goto_0
.end method
