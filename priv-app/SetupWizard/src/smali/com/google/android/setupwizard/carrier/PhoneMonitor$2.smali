.class Lcom/google/android/setupwizard/carrier/PhoneMonitor$2;
.super Landroid/telephony/PhoneStateListener;
.source "PhoneMonitor.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/setupwizard/carrier/PhoneMonitor;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/setupwizard/carrier/PhoneMonitor;


# direct methods
.method constructor <init>(Lcom/google/android/setupwizard/carrier/PhoneMonitor;)V
    .locals 0

    .prologue
    .line 375
    iput-object p1, p0, Lcom/google/android/setupwizard/carrier/PhoneMonitor$2;->this$0:Lcom/google/android/setupwizard/carrier/PhoneMonitor;

    invoke-direct {p0}, Landroid/telephony/PhoneStateListener;-><init>()V

    return-void
.end method


# virtual methods
.method public onDataConnectionStateChanged(II)V
    .locals 3
    .param p1, "state"    # I
    .param p2, "networkType"    # I

    .prologue
    .line 397
    iget-object v2, p0, Lcom/google/android/setupwizard/carrier/PhoneMonitor$2;->this$0:Lcom/google/android/setupwizard/carrier/PhoneMonitor;

    # setter for: Lcom/google/android/setupwizard/carrier/PhoneMonitor;->mCellNetworkType:I
    invoke-static {v2, p2}, Lcom/google/android/setupwizard/carrier/PhoneMonitor;->access$502(Lcom/google/android/setupwizard/carrier/PhoneMonitor;I)I

    .line 398
    iget-object v2, p0, Lcom/google/android/setupwizard/carrier/PhoneMonitor$2;->this$0:Lcom/google/android/setupwizard/carrier/PhoneMonitor;

    # setter for: Lcom/google/android/setupwizard/carrier/PhoneMonitor;->mCellNetworkState:I
    invoke-static {v2, p1}, Lcom/google/android/setupwizard/carrier/PhoneMonitor;->access$602(Lcom/google/android/setupwizard/carrier/PhoneMonitor;I)I

    .line 399
    iget-object v2, p0, Lcom/google/android/setupwizard/carrier/PhoneMonitor$2;->this$0:Lcom/google/android/setupwizard/carrier/PhoneMonitor;

    # getter for: Lcom/google/android/setupwizard/carrier/PhoneMonitor;->mListeners:Ljava/util/ArrayList;
    invoke-static {v2}, Lcom/google/android/setupwizard/carrier/PhoneMonitor;->access$400(Lcom/google/android/setupwizard/carrier/PhoneMonitor;)Ljava/util/ArrayList;

    move-result-object v2

    invoke-virtual {v2}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/telephony/PhoneStateListener;

    .line 400
    .local v1, "listener":Landroid/telephony/PhoneStateListener;
    invoke-virtual {v1, p1, p2}, Landroid/telephony/PhoneStateListener;->onDataConnectionStateChanged(II)V

    goto :goto_0

    .line 402
    .end local v1    # "listener":Landroid/telephony/PhoneStateListener;
    :cond_0
    return-void
.end method

.method public onOtaspChanged(I)V
    .locals 3
    .param p1, "otaspMode"    # I

    .prologue
    .line 388
    iget-object v2, p0, Lcom/google/android/setupwizard/carrier/PhoneMonitor$2;->this$0:Lcom/google/android/setupwizard/carrier/PhoneMonitor;

    iput p1, v2, Lcom/google/android/setupwizard/carrier/PhoneMonitor;->mOtaspMode:I

    .line 389
    const-string v2, "onOtaspChanged "

    invoke-static {v2}, Lcom/google/android/setupwizard/carrier/PhoneMonitor;->logPhoneState(Ljava/lang/String;)V

    .line 390
    iget-object v2, p0, Lcom/google/android/setupwizard/carrier/PhoneMonitor$2;->this$0:Lcom/google/android/setupwizard/carrier/PhoneMonitor;

    # getter for: Lcom/google/android/setupwizard/carrier/PhoneMonitor;->mListeners:Ljava/util/ArrayList;
    invoke-static {v2}, Lcom/google/android/setupwizard/carrier/PhoneMonitor;->access$400(Lcom/google/android/setupwizard/carrier/PhoneMonitor;)Ljava/util/ArrayList;

    move-result-object v2

    invoke-virtual {v2}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/telephony/PhoneStateListener;

    .line 391
    .local v1, "listener":Landroid/telephony/PhoneStateListener;
    invoke-virtual {v1, p1}, Landroid/telephony/PhoneStateListener;->onOtaspChanged(I)V

    goto :goto_0

    .line 393
    .end local v1    # "listener":Landroid/telephony/PhoneStateListener;
    :cond_0
    return-void
.end method

.method public onServiceStateChanged(Landroid/telephony/ServiceState;)V
    .locals 4
    .param p1, "serviceState"    # Landroid/telephony/ServiceState;

    .prologue
    .line 378
    iget-object v2, p0, Lcom/google/android/setupwizard/carrier/PhoneMonitor$2;->this$0:Lcom/google/android/setupwizard/carrier/PhoneMonitor;

    # setter for: Lcom/google/android/setupwizard/carrier/PhoneMonitor;->mServiceState:Landroid/telephony/ServiceState;
    invoke-static {v2, p1}, Lcom/google/android/setupwizard/carrier/PhoneMonitor;->access$302(Lcom/google/android/setupwizard/carrier/PhoneMonitor;Landroid/telephony/ServiceState;)Landroid/telephony/ServiceState;

    .line 379
    iget-object v2, p0, Lcom/google/android/setupwizard/carrier/PhoneMonitor$2;->this$0:Lcom/google/android/setupwizard/carrier/PhoneMonitor;

    invoke-virtual {p1}, Landroid/telephony/ServiceState;->getState()I

    move-result v3

    iput v3, v2, Lcom/google/android/setupwizard/carrier/PhoneMonitor;->mPhoneServiceState:I

    .line 380
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "onServiceStateChanged state=\""

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "\" "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/google/android/setupwizard/carrier/PhoneMonitor;->logPhoneState(Ljava/lang/String;)V

    .line 381
    iget-object v2, p0, Lcom/google/android/setupwizard/carrier/PhoneMonitor$2;->this$0:Lcom/google/android/setupwizard/carrier/PhoneMonitor;

    # getter for: Lcom/google/android/setupwizard/carrier/PhoneMonitor;->mListeners:Ljava/util/ArrayList;
    invoke-static {v2}, Lcom/google/android/setupwizard/carrier/PhoneMonitor;->access$400(Lcom/google/android/setupwizard/carrier/PhoneMonitor;)Ljava/util/ArrayList;

    move-result-object v2

    invoke-virtual {v2}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/telephony/PhoneStateListener;

    .line 382
    .local v1, "listener":Landroid/telephony/PhoneStateListener;
    invoke-virtual {v1, p1}, Landroid/telephony/PhoneStateListener;->onServiceStateChanged(Landroid/telephony/ServiceState;)V

    goto :goto_0

    .line 384
    .end local v1    # "listener":Landroid/telephony/PhoneStateListener;
    :cond_0
    return-void
.end method

.method public onSignalStrengthsChanged(Landroid/telephony/SignalStrength;)V
    .locals 4
    .param p1, "signalStrength"    # Landroid/telephony/SignalStrength;

    .prologue
    .line 406
    iget-object v2, p0, Lcom/google/android/setupwizard/carrier/PhoneMonitor$2;->this$0:Lcom/google/android/setupwizard/carrier/PhoneMonitor;

    invoke-virtual {p1}, Landroid/telephony/SignalStrength;->getLevel()I

    move-result v3

    # setter for: Lcom/google/android/setupwizard/carrier/PhoneMonitor;->mCellSignalStrength:I
    invoke-static {v2, v3}, Lcom/google/android/setupwizard/carrier/PhoneMonitor;->access$702(Lcom/google/android/setupwizard/carrier/PhoneMonitor;I)I

    .line 407
    iget-object v2, p0, Lcom/google/android/setupwizard/carrier/PhoneMonitor$2;->this$0:Lcom/google/android/setupwizard/carrier/PhoneMonitor;

    # getter for: Lcom/google/android/setupwizard/carrier/PhoneMonitor;->mListeners:Ljava/util/ArrayList;
    invoke-static {v2}, Lcom/google/android/setupwizard/carrier/PhoneMonitor;->access$400(Lcom/google/android/setupwizard/carrier/PhoneMonitor;)Ljava/util/ArrayList;

    move-result-object v2

    invoke-virtual {v2}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/telephony/PhoneStateListener;

    .line 408
    .local v1, "listener":Landroid/telephony/PhoneStateListener;
    invoke-virtual {v1, p1}, Landroid/telephony/PhoneStateListener;->onSignalStrengthsChanged(Landroid/telephony/SignalStrength;)V

    goto :goto_0

    .line 410
    .end local v1    # "listener":Landroid/telephony/PhoneStateListener;
    :cond_0
    return-void
.end method
