.class public Lcom/google/android/setupwizard/SetupWizardActivity;
.super Lcom/google/android/setupwizard/BaseActivity;
.source "SetupWizardActivity.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 34
    invoke-direct {p0}, Lcom/google/android/setupwizard/BaseActivity;-><init>()V

    return-void
.end method

.method private getEditor()Landroid/content/SharedPreferences$Editor;
    .locals 2

    .prologue
    .line 117
    const-string v0, "SetupWizardPrefs"

    const/4 v1, 0x4

    invoke-virtual {p0, v0, v1}, Lcom/google/android/setupwizard/SetupWizardActivity;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    return-object v0
.end method

.method private static isLegacyMode()Z
    .locals 2

    .prologue
    .line 136
    const-string v0, "ro.setupwizard.legacy_mode"

    const/4 v1, 0x0

    invoke-static {v0, v1}, Landroid/os/SystemProperties;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    return v0
.end method

.method private startWarmSimCheck()V
    .locals 2

    .prologue
    .line 124
    invoke-direct {p0}, Lcom/google/android/setupwizard/SetupWizardActivity;->getEditor()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    const-string v1, "connection_status"

    invoke-interface {v0, v1}, Landroid/content/SharedPreferences$Editor;->remove(Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->apply()V

    .line 127
    const/4 v0, 0x1

    const/4 v1, 0x0

    invoke-static {p0, v0, v1}, Lcom/google/android/setupwizard/network/CheckConnectionTask;->checkConnection(Landroid/content/Context;ZZ)V

    .line 129
    return-void
.end method


# virtual methods
.method protected onCreate(Landroid/os/Bundle;)V
    .locals 11
    .param p1, "icicle"    # Landroid/os/Bundle;

    .prologue
    const/4 v7, 0x1

    const/4 v8, 0x0

    .line 54
    iget-object v9, p0, Lcom/google/android/setupwizard/SetupWizardActivity;->TAG:Ljava/lang/String;

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "SetupWizardActivity.onCreate("

    invoke-virtual {v6, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    if-nez p1, :cond_2

    const-string v6, "null"

    :goto_0
    invoke-virtual {v10, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v10, ") userID="

    invoke-virtual {v6, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-static {}, Landroid/os/UserHandle;->myUserId()I

    move-result v10

    invoke-virtual {v6, v10}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v9, v6}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 56
    invoke-virtual {p0}, Lcom/google/android/setupwizard/SetupWizardActivity;->getIntent()Landroid/content/Intent;

    move-result-object v2

    .line 57
    .local v2, "intent":Landroid/content/Intent;
    if-eqz v2, :cond_1

    .line 58
    const-string v6, "firstRun"

    invoke-virtual {v2, v6}, Landroid/content/Intent;->hasExtra(Ljava/lang/String;)Z

    move-result v6

    if-nez v6, :cond_0

    .line 59
    const-string v6, "firstRun"

    invoke-virtual {v2, v6, v7}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 61
    :cond_0
    invoke-virtual {p0}, Lcom/google/android/setupwizard/SetupWizardActivity;->getLastActivity()I

    move-result v6

    const/16 v9, 0x2724

    if-ne v6, v9, :cond_3

    move v3, v7

    .line 62
    .local v3, "isResumedFromUpdate":Z
    :goto_1
    const-string v6, "resumeFromUpdate"

    invoke-virtual {v2, v6, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 64
    .end local v3    # "isResumedFromUpdate":Z
    :cond_1
    invoke-super {p0, p1}, Lcom/google/android/setupwizard/BaseActivity;->onCreate(Landroid/os/Bundle;)V

    .line 66
    invoke-virtual {p0}, Lcom/google/android/setupwizard/SetupWizardActivity;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v6

    const-string v9, "device_provisioned"

    invoke-static {v6, v9, v8}, Landroid/provider/Settings$Global;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v6

    if-eqz v6, :cond_4

    move v5, v7

    .line 68
    .local v5, "provisioned":Z
    :goto_2
    iget-object v6, p0, Lcom/google/android/setupwizard/SetupWizardActivity;->TAG:Ljava/lang/String;

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "DEVICE_PROVISIONED="

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, v5}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v6, v9}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 70
    invoke-virtual {p0}, Lcom/google/android/setupwizard/SetupWizardActivity;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v6

    const-string v9, "user_setup_complete"

    invoke-static {v6, v9, v8}, Landroid/provider/Settings$Secure;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v6

    if-eqz v6, :cond_5

    move v0, v7

    .line 72
    .local v0, "completed":Z
    :goto_3
    iget-object v6, p0, Lcom/google/android/setupwizard/SetupWizardActivity;->TAG:Ljava/lang/String;

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "USER_SETUP_COMPLETE="

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v6, v9}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 78
    invoke-virtual {p0}, Lcom/google/android/setupwizard/SetupWizardActivity;->provisioningDisabled()Z

    move-result v1

    .line 79
    .local v1, "earlyExit":Z
    if-eqz v0, :cond_6

    invoke-virtual {p0}, Lcom/google/android/setupwizard/SetupWizardActivity;->getProvisioningMode()Lcom/google/android/setupwizard/BaseActivity$ProvisioningMode;

    move-result-object v6

    sget-object v9, Lcom/google/android/setupwizard/BaseActivity$ProvisioningMode;->OPTIONAL:Lcom/google/android/setupwizard/BaseActivity$ProvisioningMode;

    if-ne v6, v9, :cond_6

    :goto_4
    or-int/2addr v1, v7

    .line 82
    invoke-static {p0}, Landroid/os/UserManager;->get(Landroid/content/Context;)Landroid/os/UserManager;

    move-result-object v6

    invoke-virtual {v6}, Landroid/os/UserManager;->isLinkedUser()Z

    move-result v6

    or-int/2addr v1, v6

    .line 84
    invoke-static {p0}, Landroid/os/UserManager;->get(Landroid/content/Context;)Landroid/os/UserManager;

    move-result-object v6

    invoke-virtual {v6}, Landroid/os/UserManager;->isGuestUser()Z

    move-result v6

    or-int/2addr v1, v6

    .line 85
    if-eqz v1, :cond_7

    .line 86
    iget-object v6, p0, Lcom/google/android/setupwizard/SetupWizardActivity;->TAG:Ljava/lang/String;

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "Early exit: provisioningMode="

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {p0}, Lcom/google/android/setupwizard/SetupWizardActivity;->getProvisioningMode()Lcom/google/android/setupwizard/BaseActivity$ProvisioningMode;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 87
    invoke-virtual {p0}, Lcom/google/android/setupwizard/SetupWizardActivity;->onSetupComplete()V

    .line 114
    :goto_5
    return-void

    .line 54
    .end local v0    # "completed":Z
    .end local v1    # "earlyExit":Z
    .end local v2    # "intent":Landroid/content/Intent;
    .end local v5    # "provisioned":Z
    :cond_2
    const-string v6, "icicle"

    goto/16 :goto_0

    .restart local v2    # "intent":Landroid/content/Intent;
    :cond_3
    move v3, v8

    .line 61
    goto/16 :goto_1

    :cond_4
    move v5, v8

    .line 66
    goto/16 :goto_2

    .restart local v5    # "provisioned":Z
    :cond_5
    move v0, v8

    .line 70
    goto :goto_3

    .restart local v0    # "completed":Z
    .restart local v1    # "earlyExit":Z
    :cond_6
    move v7, v8

    .line 79
    goto :goto_4

    .line 91
    :cond_7
    if-nez p1, :cond_8

    .line 92
    invoke-virtual {p0}, Lcom/google/android/setupwizard/SetupWizardActivity;->onSetupStart()V

    .line 93
    invoke-direct {p0}, Lcom/google/android/setupwizard/SetupWizardActivity;->startWarmSimCheck()V

    .line 95
    invoke-static {}, Lcom/google/android/setupwizard/SetupWizardActivity;->isLegacyMode()Z

    move-result v6

    if-eqz v6, :cond_9

    .line 96
    invoke-virtual {p0}, Lcom/google/android/setupwizard/SetupWizardActivity;->nextScreen()V

    .line 110
    :goto_6
    invoke-virtual {p0}, Lcom/google/android/setupwizard/SetupWizardActivity;->finish()V

    .line 113
    :cond_8
    iget-object v6, p0, Lcom/google/android/setupwizard/SetupWizardActivity;->TAG:Ljava/lang/String;

    const-string v7, "SetupWizardActivity.onCreate() completed"

    invoke-static {v6, v7}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_5

    .line 100
    :cond_9
    const-class v6, Lcom/google/android/setupwizard/WizardManager;

    invoke-virtual {v6}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {p0, v6}, Lcom/google/android/setupwizard/SetupWizardActivity;->enableComponent(Ljava/lang/String;)V

    .line 101
    new-instance v4, Landroid/content/Intent;

    const-string v6, "com.android.wizard.LOAD"

    invoke-direct {v4, v6}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 102
    .local v4, "loadIntent":Landroid/content/Intent;
    invoke-virtual {p0}, Lcom/google/android/setupwizard/SetupWizardActivity;->isPrimaryUser()Z

    move-result v6

    if-eqz v6, :cond_a

    .line 103
    const-string v6, "scriptUri"

    const-string v7, "android.resource://com.google.android.setupwizard/xml/wizard_script"

    invoke-virtual {v4, v6, v7}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 107
    :goto_7
    const-string v6, "theme"

    const-string v7, "theme"

    invoke-virtual {v2, v7}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v4, v6, v7}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 108
    invoke-virtual {p0, v4}, Lcom/google/android/setupwizard/SetupWizardActivity;->startActivity(Landroid/content/Intent;)V

    goto :goto_6

    .line 105
    :cond_a
    const-string v6, "scriptUri"

    const-string v7, "android.resource://com.google.android.setupwizard/xml/wizard_script_user"

    invoke-virtual {v4, v6, v7}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    goto :goto_7
.end method

.method protected onNewIntent(Landroid/content/Intent;)V
    .locals 0
    .param p1, "intent"    # Landroid/content/Intent;

    .prologue
    .line 47
    invoke-super {p0, p1}, Lcom/google/android/setupwizard/BaseActivity;->onNewIntent(Landroid/content/Intent;)V

    .line 49
    invoke-virtual {p0, p1}, Lcom/google/android/setupwizard/SetupWizardActivity;->setIntent(Landroid/content/Intent;)V

    .line 50
    return-void
.end method
