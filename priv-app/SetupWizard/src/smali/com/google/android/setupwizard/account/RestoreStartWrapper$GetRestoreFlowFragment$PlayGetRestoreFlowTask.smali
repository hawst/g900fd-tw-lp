.class Lcom/google/android/setupwizard/account/RestoreStartWrapper$GetRestoreFlowFragment$PlayGetRestoreFlowTask;
.super Landroid/os/AsyncTask;
.source "RestoreStartWrapper.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/setupwizard/account/RestoreStartWrapper$GetRestoreFlowFragment;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "PlayGetRestoreFlowTask"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/os/AsyncTask",
        "<",
        "Ljava/lang/Void;",
        "Ljava/lang/Void;",
        "Landroid/content/Intent;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/setupwizard/account/RestoreStartWrapper$GetRestoreFlowFragment;


# direct methods
.method private constructor <init>(Lcom/google/android/setupwizard/account/RestoreStartWrapper$GetRestoreFlowFragment;)V
    .locals 0

    .prologue
    .line 280
    iput-object p1, p0, Lcom/google/android/setupwizard/account/RestoreStartWrapper$GetRestoreFlowFragment$PlayGetRestoreFlowTask;->this$0:Lcom/google/android/setupwizard/account/RestoreStartWrapper$GetRestoreFlowFragment;

    invoke-direct {p0}, Landroid/os/AsyncTask;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/google/android/setupwizard/account/RestoreStartWrapper$GetRestoreFlowFragment;Lcom/google/android/setupwizard/account/RestoreStartWrapper$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/google/android/setupwizard/account/RestoreStartWrapper$GetRestoreFlowFragment;
    .param p2, "x1"    # Lcom/google/android/setupwizard/account/RestoreStartWrapper$1;

    .prologue
    .line 280
    invoke-direct {p0, p1}, Lcom/google/android/setupwizard/account/RestoreStartWrapper$GetRestoreFlowFragment$PlayGetRestoreFlowTask;-><init>(Lcom/google/android/setupwizard/account/RestoreStartWrapper$GetRestoreFlowFragment;)V

    return-void
.end method


# virtual methods
.method protected varargs doInBackground([Ljava/lang/Void;)Landroid/content/Intent;
    .locals 2
    .param p1, "voids"    # [Ljava/lang/Void;

    .prologue
    .line 284
    iget-object v0, p0, Lcom/google/android/setupwizard/account/RestoreStartWrapper$GetRestoreFlowFragment$PlayGetRestoreFlowTask;->this$0:Lcom/google/android/setupwizard/account/RestoreStartWrapper$GetRestoreFlowFragment;

    # getter for: Lcom/google/android/setupwizard/account/RestoreStartWrapper$GetRestoreFlowFragment;->mPlayHelper:Lcom/google/android/setupwizard/util/GooglePlaySetupHelper;
    invoke-static {v0}, Lcom/google/android/setupwizard/account/RestoreStartWrapper$GetRestoreFlowFragment;->access$600(Lcom/google/android/setupwizard/account/RestoreStartWrapper$GetRestoreFlowFragment;)Lcom/google/android/setupwizard/util/GooglePlaySetupHelper;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/setupwizard/account/RestoreStartWrapper$GetRestoreFlowFragment$PlayGetRestoreFlowTask;->this$0:Lcom/google/android/setupwizard/account/RestoreStartWrapper$GetRestoreFlowFragment;

    # getter for: Lcom/google/android/setupwizard/account/RestoreStartWrapper$GetRestoreFlowFragment;->mActivity:Lcom/google/android/setupwizard/account/RestoreStartWrapper;
    invoke-static {v1}, Lcom/google/android/setupwizard/account/RestoreStartWrapper$GetRestoreFlowFragment;->access$400(Lcom/google/android/setupwizard/account/RestoreStartWrapper$GetRestoreFlowFragment;)Lcom/google/android/setupwizard/account/RestoreStartWrapper;

    move-result-object v1

    # getter for: Lcom/google/android/setupwizard/account/RestoreStartWrapper;->mAccount:Landroid/accounts/Account;
    invoke-static {v1}, Lcom/google/android/setupwizard/account/RestoreStartWrapper;->access$500(Lcom/google/android/setupwizard/account/RestoreStartWrapper;)Landroid/accounts/Account;

    move-result-object v1

    iget-object v1, v1, Landroid/accounts/Account;->name:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/google/android/setupwizard/util/GooglePlaySetupHelper;->getRestoreIntent(Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    return-object v0
.end method

.method protected bridge synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1
    .param p1, "x0"    # [Ljava/lang/Object;

    .prologue
    .line 280
    check-cast p1, [Ljava/lang/Void;

    .end local p1    # "x0":[Ljava/lang/Object;
    invoke-virtual {p0, p1}, Lcom/google/android/setupwizard/account/RestoreStartWrapper$GetRestoreFlowFragment$PlayGetRestoreFlowTask;->doInBackground([Ljava/lang/Void;)Landroid/content/Intent;

    move-result-object v0

    return-object v0
.end method

.method protected onPostExecute(Landroid/content/Intent;)V
    .locals 2
    .param p1, "intent"    # Landroid/content/Intent;

    .prologue
    .line 289
    iget-object v0, p0, Lcom/google/android/setupwizard/account/RestoreStartWrapper$GetRestoreFlowFragment$PlayGetRestoreFlowTask;->this$0:Lcom/google/android/setupwizard/account/RestoreStartWrapper$GetRestoreFlowFragment;

    # setter for: Lcom/google/android/setupwizard/account/RestoreStartWrapper$GetRestoreFlowFragment;->mIntent:Landroid/content/Intent;
    invoke-static {v0, p1}, Lcom/google/android/setupwizard/account/RestoreStartWrapper$GetRestoreFlowFragment;->access$702(Lcom/google/android/setupwizard/account/RestoreStartWrapper$GetRestoreFlowFragment;Landroid/content/Intent;)Landroid/content/Intent;

    .line 290
    iget-object v0, p0, Lcom/google/android/setupwizard/account/RestoreStartWrapper$GetRestoreFlowFragment$PlayGetRestoreFlowTask;->this$0:Lcom/google/android/setupwizard/account/RestoreStartWrapper$GetRestoreFlowFragment;

    const/4 v1, 0x1

    # setter for: Lcom/google/android/setupwizard/account/RestoreStartWrapper$GetRestoreFlowFragment;->mIntentReady:Z
    invoke-static {v0, v1}, Lcom/google/android/setupwizard/account/RestoreStartWrapper$GetRestoreFlowFragment;->access$802(Lcom/google/android/setupwizard/account/RestoreStartWrapper$GetRestoreFlowFragment;Z)Z

    .line 291
    iget-object v0, p0, Lcom/google/android/setupwizard/account/RestoreStartWrapper$GetRestoreFlowFragment$PlayGetRestoreFlowTask;->this$0:Lcom/google/android/setupwizard/account/RestoreStartWrapper$GetRestoreFlowFragment;

    invoke-virtual {v0}, Lcom/google/android/setupwizard/account/RestoreStartWrapper$GetRestoreFlowFragment;->isResumed()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 292
    iget-object v0, p0, Lcom/google/android/setupwizard/account/RestoreStartWrapper$GetRestoreFlowFragment$PlayGetRestoreFlowTask;->this$0:Lcom/google/android/setupwizard/account/RestoreStartWrapper$GetRestoreFlowFragment;

    # invokes: Lcom/google/android/setupwizard/account/RestoreStartWrapper$GetRestoreFlowFragment;->launchRestorePickerIfReady()V
    invoke-static {v0}, Lcom/google/android/setupwizard/account/RestoreStartWrapper$GetRestoreFlowFragment;->access$900(Lcom/google/android/setupwizard/account/RestoreStartWrapper$GetRestoreFlowFragment;)V

    .line 294
    :cond_0
    return-void
.end method

.method protected bridge synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 0
    .param p1, "x0"    # Ljava/lang/Object;

    .prologue
    .line 280
    check-cast p1, Landroid/content/Intent;

    .end local p1    # "x0":Ljava/lang/Object;
    invoke-virtual {p0, p1}, Lcom/google/android/setupwizard/account/RestoreStartWrapper$GetRestoreFlowFragment$PlayGetRestoreFlowTask;->onPostExecute(Landroid/content/Intent;)V

    return-void
.end method
