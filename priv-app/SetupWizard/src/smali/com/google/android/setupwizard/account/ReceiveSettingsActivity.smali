.class public Lcom/google/android/setupwizard/account/ReceiveSettingsActivity;
.super Lcom/google/android/setupwizard/BaseActivity;
.source "ReceiveSettingsActivity.java"


# instance fields
.field private mIntent:Landroid/content/Intent;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 34
    invoke-direct {p0}, Lcom/google/android/setupwizard/BaseActivity;-><init>()V

    return-void
.end method


# virtual methods
.method protected onCreate(Landroid/os/Bundle;)V
    .locals 3
    .param p1, "icicle"    # Landroid/os/Bundle;

    .prologue
    .line 43
    iget-object v1, p0, Lcom/google/android/setupwizard/account/ReceiveSettingsActivity;->TAG:Ljava/lang/String;

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "ReceiveSettingsActivity.onCreate("

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    if-nez p1, :cond_0

    const-string v0, "null"

    :goto_0
    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, ")"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 45
    invoke-super {p0, p1}, Lcom/google/android/setupwizard/BaseActivity;->onCreate(Landroid/os/Bundle;)V

    .line 46
    invoke-virtual {p0}, Lcom/google/android/setupwizard/account/ReceiveSettingsActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/setupwizard/account/ReceiveSettingsActivity;->mIntent:Landroid/content/Intent;

    .line 47
    invoke-virtual {p0}, Lcom/google/android/setupwizard/account/ReceiveSettingsActivity;->setBackgroundFromIntent()V

    .line 48
    invoke-virtual {p0}, Lcom/google/android/setupwizard/account/ReceiveSettingsActivity;->setEduScreensFromIntent()V

    .line 49
    invoke-virtual {p0}, Lcom/google/android/setupwizard/account/ReceiveSettingsActivity;->setDomainsFromIntent()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 50
    new-instance v0, Landroid/content/Intent;

    const-string v1, "com.google.android.setupwizard.SETUP_WIZARD_STORED_DOMAIN"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0, v0}, Lcom/google/android/setupwizard/account/ReceiveSettingsActivity;->sendBroadcast(Landroid/content/Intent;)V

    .line 51
    const/4 v0, -0x1

    invoke-virtual {p0, v0}, Lcom/google/android/setupwizard/account/ReceiveSettingsActivity;->finishAction(I)V

    .line 55
    :goto_1
    return-void

    .line 43
    :cond_0
    const-string v0, "icicle"

    goto :goto_0

    .line 53
    :cond_1
    invoke-virtual {p0}, Lcom/google/android/setupwizard/account/ReceiveSettingsActivity;->finishAction()V

    goto :goto_1
.end method

.method setBackgroundFromIntent()V
    .locals 5

    .prologue
    .line 69
    iget-object v2, p0, Lcom/google/android/setupwizard/account/ReceiveSettingsActivity;->mIntent:Landroid/content/Intent;

    const-string v3, "welcomeBackgroundDrawable"

    invoke-virtual {v2, v3}, Landroid/content/Intent;->hasExtra(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 70
    iget-object v2, p0, Lcom/google/android/setupwizard/account/ReceiveSettingsActivity;->mIntent:Landroid/content/Intent;

    const-string v3, "welcomeBackgroundDrawable"

    invoke-virtual {v2, v3}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 71
    .local v1, "resourceName":Ljava/lang/String;
    iget-object v2, p0, Lcom/google/android/setupwizard/account/ReceiveSettingsActivity;->mIntent:Landroid/content/Intent;

    const-string v3, "welcomeBackgroundPackage"

    invoke-virtual {v2, v3}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 73
    .local v0, "packageName":Ljava/lang/String;
    iget-object v2, p0, Lcom/google/android/setupwizard/account/ReceiveSettingsActivity;->TAG:Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "ReceiveSettingsActivity.setBackgroundFromIntent() package:"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " resourceName:"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 75
    invoke-virtual {p0}, Lcom/google/android/setupwizard/account/ReceiveSettingsActivity;->getBaseContext()Landroid/content/Context;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/google/android/setupwizard/edu/WelcomeEduActivity;->setWelcomeBackgroundDrawableName(Ljava/lang/String;Landroid/content/Context;)V

    .line 76
    invoke-virtual {p0}, Lcom/google/android/setupwizard/account/ReceiveSettingsActivity;->getBaseContext()Landroid/content/Context;

    move-result-object v2

    invoke-static {v0, v2}, Lcom/google/android/setupwizard/edu/WelcomeEduActivity;->setWelcomeBackgroundPackageName(Ljava/lang/String;Landroid/content/Context;)V

    .line 78
    .end local v0    # "packageName":Ljava/lang/String;
    .end local v1    # "resourceName":Ljava/lang/String;
    :cond_0
    return-void
.end method

.method setDomainsFromIntent()Z
    .locals 4

    .prologue
    .line 58
    iget-object v1, p0, Lcom/google/android/setupwizard/account/ReceiveSettingsActivity;->mIntent:Landroid/content/Intent;

    const-string v2, "accountDomain"

    invoke-virtual {v1, v2}, Landroid/content/Intent;->hasExtra(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 59
    iget-object v1, p0, Lcom/google/android/setupwizard/account/ReceiveSettingsActivity;->mIntent:Landroid/content/Intent;

    const-string v2, "accountDomain"

    invoke-virtual {v1, v2}, Landroid/content/Intent;->getStringArrayExtra(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v0

    .line 60
    .local v0, "domains":[Ljava/lang/String;
    iget-object v1, p0, Lcom/google/android/setupwizard/account/ReceiveSettingsActivity;->TAG:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "ReceiveSettingsActivity.setDomainFromIntent() domain(s) = "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-static {v0}, Ljava/util/Arrays;->toString([Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 62
    invoke-virtual {p0, v0}, Lcom/google/android/setupwizard/account/ReceiveSettingsActivity;->setAccountDomains([Ljava/lang/String;)V

    .line 63
    const/4 v1, 0x1

    .line 65
    .end local v0    # "domains":[Ljava/lang/String;
    :goto_0
    return v1

    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method setEduScreensFromIntent()V
    .locals 7

    .prologue
    const/4 v6, 0x0

    .line 81
    iget-object v3, p0, Lcom/google/android/setupwizard/account/ReceiveSettingsActivity;->mIntent:Landroid/content/Intent;

    const-string v4, "showLockPin"

    invoke-virtual {v3, v4}, Landroid/content/Intent;->hasExtra(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 82
    iget-object v3, p0, Lcom/google/android/setupwizard/account/ReceiveSettingsActivity;->mIntent:Landroid/content/Intent;

    const-string v4, "showLockPin"

    invoke-virtual {v3, v4, v6}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v1

    .line 83
    .local v1, "showLockPin":Z
    iget-object v3, p0, Lcom/google/android/setupwizard/account/ReceiveSettingsActivity;->mIntent:Landroid/content/Intent;

    const-string v4, "requireLockPin"

    invoke-virtual {v3, v4, v6}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v0

    .line 84
    .local v0, "requireLockPin":Z
    iget-object v3, p0, Lcom/google/android/setupwizard/account/ReceiveSettingsActivity;->TAG:Ljava/lang/String;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "ReceiveSettingsActivity.setEduScreensFromIntent() showLockPinScreen:"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ", requireLockPin: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 86
    invoke-virtual {p0, v1}, Lcom/google/android/setupwizard/account/ReceiveSettingsActivity;->setShowLockPinScreen(Z)V

    .line 87
    invoke-virtual {p0, v0}, Lcom/google/android/setupwizard/account/ReceiveSettingsActivity;->setRequireLockPin(Z)V

    .line 90
    .end local v0    # "requireLockPin":Z
    .end local v1    # "showLockPin":Z
    :cond_0
    iget-object v3, p0, Lcom/google/android/setupwizard/account/ReceiveSettingsActivity;->mIntent:Landroid/content/Intent;

    const-string v4, "showSelfie"

    invoke-virtual {v3, v4}, Landroid/content/Intent;->hasExtra(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 91
    iget-object v3, p0, Lcom/google/android/setupwizard/account/ReceiveSettingsActivity;->mIntent:Landroid/content/Intent;

    const-string v4, "showSelfie"

    invoke-virtual {v3, v4, v6}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v2

    .line 92
    .local v2, "showSelfie":Z
    iget-object v3, p0, Lcom/google/android/setupwizard/account/ReceiveSettingsActivity;->TAG:Ljava/lang/String;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "ReceiveSettingsActivity.setEduScreensFromIntent() showSelfieScreen:"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 94
    invoke-virtual {p0, v2}, Lcom/google/android/setupwizard/account/ReceiveSettingsActivity;->setShowSelfieScreen(Z)V

    .line 96
    .end local v2    # "showSelfie":Z
    :cond_1
    return-void
.end method
