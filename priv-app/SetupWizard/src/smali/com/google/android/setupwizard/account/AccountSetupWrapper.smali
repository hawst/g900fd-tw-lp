.class public Lcom/google/android/setupwizard/account/AccountSetupWrapper;
.super Lcom/google/android/setupwizard/SubactivityWrapper;
.source "AccountSetupWrapper.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 28
    invoke-direct {p0}, Lcom/google/android/setupwizard/SubactivityWrapper;-><init>()V

    return-void
.end method


# virtual methods
.method protected getNextTransition()I
    .locals 1

    .prologue
    .line 134
    const/4 v0, 0x2

    return v0
.end method

.method protected getPreviousTransition()I
    .locals 1

    .prologue
    .line 129
    const/4 v0, 0x2

    return v0
.end method

.method protected onActivityResult(IILandroid/content/Intent;)V
    .locals 1
    .param p1, "requestCode"    # I
    .param p2, "resultCode"    # I
    .param p3, "data"    # Landroid/content/Intent;

    .prologue
    .line 72
    invoke-virtual {p0}, Lcom/google/android/setupwizard/account/AccountSetupWrapper;->requestCode()I

    move-result v0

    if-eq p1, v0, :cond_0

    if-nez p2, :cond_0

    invoke-virtual {p0}, Lcom/google/android/setupwizard/account/AccountSetupWrapper;->hasNoAccounts()Z

    move-result v0

    if-nez v0, :cond_0

    .line 75
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/google/android/setupwizard/account/AccountSetupWrapper;->finishAction(I)V

    .line 78
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/google/android/setupwizard/account/AccountSetupWrapper;->applyBackwardTransition(I)V

    .line 82
    :goto_0
    return-void

    .line 80
    :cond_0
    invoke-super {p0, p1, p2, p3}, Lcom/google/android/setupwizard/SubactivityWrapper;->onActivityResult(IILandroid/content/Intent;)V

    goto :goto_0
.end method

.method protected onSubactivityLaunch()Landroid/content/Intent;
    .locals 4

    .prologue
    const/4 v3, 0x1

    .line 51
    iget-object v1, p0, Lcom/google/android/setupwizard/account/AccountSetupWrapper;->TAG:Ljava/lang/String;

    const-string v2, "onSubactivityLaunch"

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 53
    invoke-virtual {p0}, Lcom/google/android/setupwizard/account/AccountSetupWrapper;->getLocalPreferences()Landroid/content/SharedPreferences;

    move-result-object v1

    invoke-interface {v1}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v1

    const-string v2, "accountSetupLaunched"

    invoke-interface {v1, v2, v3}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    move-result-object v1

    invoke-interface {v1}, Landroid/content/SharedPreferences$Editor;->apply()V

    .line 57
    new-instance v0, Landroid/content/Intent;

    const-string v1, "com.google.android.accounts.AccountIntro"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 58
    .local v0, "nextIntent":Landroid/content/Intent;
    const-string v1, "allowSkip"

    invoke-virtual {v0, v1, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 59
    const-string v1, "allowed_domains"

    invoke-virtual {p0}, Lcom/google/android/setupwizard/account/AccountSetupWrapper;->getAccountDomains()[Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;[Ljava/lang/String;)Landroid/content/Intent;

    .line 60
    const-string v1, "isEduSignin"

    invoke-virtual {p0}, Lcom/google/android/setupwizard/account/AccountSetupWrapper;->isEduSignin()Z

    move-result v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 61
    const-string v1, "suppressLoginTos"

    invoke-virtual {p0}, Lcom/google/android/setupwizard/account/AccountSetupWrapper;->shouldSuppressLoginTos()Z

    move-result v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 62
    const-string v1, "suppressCreditCardRequestActivity"

    invoke-virtual {p0}, Lcom/google/android/setupwizard/account/AccountSetupWrapper;->shouldSuppressCreditCardRequest()Z

    move-result v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 64
    const-string v1, "carrierSetupLaunched"

    invoke-virtual {p0}, Lcom/google/android/setupwizard/account/AccountSetupWrapper;->carrierSetupLaunched()Z

    move-result v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 65
    const-string v1, "wifiScreenShown"

    invoke-virtual {p0}, Lcom/google/android/setupwizard/account/AccountSetupWrapper;->wifiScreenShown()Z

    move-result v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 67
    return-object v0
.end method

.method protected onSubactivityResult(IILandroid/content/Intent;)V
    .locals 10
    .param p1, "requestCode"    # I
    .param p2, "resultCode"    # I
    .param p3, "data"    # Landroid/content/Intent;

    .prologue
    const/4 v6, 0x0

    .line 86
    iget-object v7, p0, Lcom/google/android/setupwizard/account/AccountSetupWrapper;->TAG:Ljava/lang/String;

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "onSubactivityResult("

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-static {p1}, Lcom/google/android/setupwizard/account/AccountSetupWrapper;->getRequestName(I)Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, ", "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {p0}, Lcom/google/android/setupwizard/account/AccountSetupWrapper;->requestCode()I

    move-result v9

    invoke-static {v9, p2}, Lcom/google/android/setupwizard/account/AccountSetupWrapper;->getResultName(II)Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, ", "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, ")"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v7, v8}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 88
    invoke-super {p0, p1, p2, p3}, Lcom/google/android/setupwizard/SubactivityWrapper;->onSubactivityResult(IILandroid/content/Intent;)V

    .line 90
    invoke-virtual {p0}, Lcom/google/android/setupwizard/account/AccountSetupWrapper;->getLocalPreferences()Landroid/content/SharedPreferences;

    move-result-object v7

    invoke-interface {v7}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v1

    .line 91
    .local v1, "localPref":Landroid/content/SharedPreferences$Editor;
    if-nez p2, :cond_0

    .line 93
    const-string v7, "accountSetupLaunched"

    invoke-interface {v1, v7, v6}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    .line 124
    :goto_0
    invoke-interface {v1}, Landroid/content/SharedPreferences$Editor;->apply()V

    .line 125
    return-void

    .line 95
    :cond_0
    const/4 v2, 0x0

    .line 97
    .local v2, "nameCompleted":Z
    if-eqz p3, :cond_2

    .line 98
    const-string v7, "nameCompleted"

    invoke-virtual {p3, v7, v6}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v2

    .line 99
    if-nez v2, :cond_1

    .line 101
    const-string v7, "firstName"

    const-string v8, "firstName"

    invoke-virtual {p3, v8}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v8

    invoke-interface {v1, v7, v8}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    .line 103
    const-string v7, "lastName"

    const-string v8, "lastName"

    invoke-virtual {p3, v8}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v8

    invoke-interface {v1, v7, v8}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    .line 107
    :cond_1
    const-string v7, "is_new_account"

    const-string v8, "is_new_account"

    invoke-virtual {p3, v8, v6}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v8

    invoke-interface {v1, v7, v8}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    .line 110
    const-string v7, "restoreAccount"

    const-string v8, "restoreAccount"

    invoke-virtual {p3, v8}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v8

    invoke-interface {v1, v7, v8}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    .line 112
    const-string v7, "restoreToken"

    invoke-virtual {p3, v7}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 115
    .local v3, "restoreTokenString":Ljava/lang/String;
    const/16 v7, 0x10

    :try_start_0
    invoke-static {v3, v7}, Ljava/lang/Long;->valueOf(Ljava/lang/String;I)Ljava/lang/Long;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/Long;->longValue()J

    move-result-wide v4

    .line 116
    .local v4, "token":J
    const-string v7, "restoreToken"

    invoke-interface {v1, v7, v4, v5}, Landroid/content/SharedPreferences$Editor;->putLong(Ljava/lang/String;J)Landroid/content/SharedPreferences$Editor;
    :try_end_0
    .catch Ljava/lang/NumberFormatException; {:try_start_0 .. :try_end_0} :catch_0

    .line 122
    .end local v3    # "restoreTokenString":Ljava/lang/String;
    .end local v4    # "token":J
    :cond_2
    :goto_1
    const-string v7, "doShowNameActivity"

    if-nez v2, :cond_3

    const/4 v6, 0x1

    :cond_3
    invoke-interface {v1, v7, v6}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    goto :goto_0

    .line 117
    .restart local v3    # "restoreTokenString":Ljava/lang/String;
    :catch_0
    move-exception v0

    .line 118
    .local v0, "e":Ljava/lang/NumberFormatException;
    iget-object v7, p0, Lcom/google/android/setupwizard/account/AccountSetupWrapper;->TAG:Ljava/lang/String;

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "Cannot parse hexadecimal restoreToken: "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v7, v8}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1
.end method

.method protected requestCode()I
    .locals 1

    .prologue
    .line 46
    const/16 v0, 0x2717

    return v0
.end method
