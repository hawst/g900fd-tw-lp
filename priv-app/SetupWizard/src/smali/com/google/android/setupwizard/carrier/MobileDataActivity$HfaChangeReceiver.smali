.class public Lcom/google/android/setupwizard/carrier/MobileDataActivity$HfaChangeReceiver;
.super Landroid/content/BroadcastReceiver;
.source "MobileDataActivity.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/setupwizard/carrier/MobileDataActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "HfaChangeReceiver"
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 189
    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    return-void
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 5
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "intent"    # Landroid/content/Intent;

    .prologue
    const/4 v1, 0x0

    .line 192
    const-string v2, "otasp_result_code"

    invoke-virtual {p2, v2, v1}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v0

    .line 195
    .local v0, "hfaStatus":I
    const-string v2, "SetupWizard.MobileDataActivity"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "HfaChangeReceiver "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {p2}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " sCurrentActivity="

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    # getter for: Lcom/google/android/setupwizard/carrier/MobileDataActivity;->sCurrentActivity:Lcom/google/android/setupwizard/carrier/MobileDataActivity;
    invoke-static {}, Lcom/google/android/setupwizard/carrier/MobileDataActivity;->access$1500()Lcom/google/android/setupwizard/carrier/MobileDataActivity;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 199
    const-string v2, "SetupWizardLocalPrefs"

    invoke-virtual {p1, v2, v1}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v2

    invoke-interface {v2}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v2

    const-string v3, "hfa_activated"

    const/4 v4, 0x2

    if-ne v0, v4, :cond_0

    const/4 v1, 0x1

    :cond_0
    invoke-interface {v2, v3, v1}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    move-result-object v1

    invoke-interface {v1}, Landroid/content/SharedPreferences$Editor;->apply()V

    .line 204
    # getter for: Lcom/google/android/setupwizard/carrier/MobileDataActivity;->sCurrentActivity:Lcom/google/android/setupwizard/carrier/MobileDataActivity;
    invoke-static {}, Lcom/google/android/setupwizard/carrier/MobileDataActivity;->access$1500()Lcom/google/android/setupwizard/carrier/MobileDataActivity;

    move-result-object v1

    if-eqz v1, :cond_1

    .line 205
    # getter for: Lcom/google/android/setupwizard/carrier/MobileDataActivity;->sCurrentActivity:Lcom/google/android/setupwizard/carrier/MobileDataActivity;
    invoke-static {}, Lcom/google/android/setupwizard/carrier/MobileDataActivity;->access$1500()Lcom/google/android/setupwizard/carrier/MobileDataActivity;

    move-result-object v1

    # invokes: Lcom/google/android/setupwizard/carrier/MobileDataActivity;->onHfaStatusChange(I)V
    invoke-static {v1, v0}, Lcom/google/android/setupwizard/carrier/MobileDataActivity;->access$1600(Lcom/google/android/setupwizard/carrier/MobileDataActivity;I)V

    .line 207
    :cond_1
    return-void
.end method
