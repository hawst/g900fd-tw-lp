.class public Lcom/google/android/setupwizard/time/DateTimeSetupActivity$DateDialog;
.super Landroid/app/DialogFragment;
.source "DateTimeSetupActivity.java"

# interfaces
.implements Landroid/app/DatePickerDialog$OnDateSetListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/setupwizard/time/DateTimeSetupActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "DateDialog"
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 262
    invoke-direct {p0}, Landroid/app/DialogFragment;-><init>()V

    return-void
.end method

.method private configureDatePicker(Landroid/app/DatePickerDialog;)V
    .locals 4
    .param p1, "dialog"    # Landroid/app/DatePickerDialog;

    .prologue
    .line 300
    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object v0

    .line 301
    .local v0, "t":Ljava/util/Calendar;
    invoke-virtual {v0}, Ljava/util/Calendar;->clear()V

    .line 302
    const/16 v1, 0x7b2

    const/4 v2, 0x0

    const/4 v3, 0x1

    invoke-virtual {v0, v1, v2, v3}, Ljava/util/Calendar;->set(III)V

    .line 303
    invoke-virtual {p1}, Landroid/app/DatePickerDialog;->getDatePicker()Landroid/widget/DatePicker;

    move-result-object v1

    invoke-virtual {v0}, Ljava/util/Calendar;->getTimeInMillis()J

    move-result-wide v2

    invoke-virtual {v1, v2, v3}, Landroid/widget/DatePicker;->setMinDate(J)V

    .line 304
    invoke-virtual {v0}, Ljava/util/Calendar;->clear()V

    .line 305
    const/16 v1, 0x7f5

    const/16 v2, 0xb

    const/16 v3, 0x1f

    invoke-virtual {v0, v1, v2, v3}, Ljava/util/Calendar;->set(III)V

    .line 306
    invoke-virtual {p1}, Landroid/app/DatePickerDialog;->getDatePicker()Landroid/widget/DatePicker;

    move-result-object v1

    invoke-virtual {v0}, Ljava/util/Calendar;->getTimeInMillis()J

    move-result-wide v2

    invoke-virtual {v1, v2, v3}, Landroid/widget/DatePicker;->setMaxDate(J)V

    .line 307
    return-void
.end method


# virtual methods
.method public onCreateDialog(Landroid/os/Bundle;)Landroid/app/Dialog;
    .locals 7
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 275
    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object v6

    .line 276
    .local v6, "c":Ljava/util/Calendar;
    new-instance v0, Landroid/app/DatePickerDialog;

    invoke-virtual {p0}, Lcom/google/android/setupwizard/time/DateTimeSetupActivity$DateDialog;->getActivity()Landroid/app/Activity;

    move-result-object v1

    const/4 v2, 0x1

    invoke-virtual {v6, v2}, Ljava/util/Calendar;->get(I)I

    move-result v3

    const/4 v2, 0x2

    invoke-virtual {v6, v2}, Ljava/util/Calendar;->get(I)I

    move-result v4

    const/4 v2, 0x5

    invoke-virtual {v6, v2}, Ljava/util/Calendar;->get(I)I

    move-result v5

    move-object v2, p0

    invoke-direct/range {v0 .. v5}, Landroid/app/DatePickerDialog;-><init>(Landroid/content/Context;Landroid/app/DatePickerDialog$OnDateSetListener;III)V

    .line 278
    .local v0, "dialog":Landroid/app/DatePickerDialog;
    invoke-direct {p0, v0}, Lcom/google/android/setupwizard/time/DateTimeSetupActivity$DateDialog;->configureDatePicker(Landroid/app/DatePickerDialog;)V

    .line 279
    return-object v0
.end method

.method public onDateSet(Landroid/widget/DatePicker;III)V
    .locals 1
    .param p1, "view"    # Landroid/widget/DatePicker;
    .param p2, "year"    # I
    .param p3, "month"    # I
    .param p4, "day"    # I

    .prologue
    .line 292
    invoke-virtual {p0}, Lcom/google/android/setupwizard/time/DateTimeSetupActivity$DateDialog;->getActivity()Landroid/app/Activity;

    move-result-object v0

    check-cast v0, Lcom/google/android/setupwizard/time/DateTimeSetupActivity;

    .line 293
    .local v0, "activity":Lcom/google/android/setupwizard/time/DateTimeSetupActivity;
    # invokes: Lcom/google/android/setupwizard/time/DateTimeSetupActivity;->setDate(III)V
    invoke-static {v0, p2, p3, p4}, Lcom/google/android/setupwizard/time/DateTimeSetupActivity;->access$300(Lcom/google/android/setupwizard/time/DateTimeSetupActivity;III)V

    .line 294
    # invokes: Lcom/google/android/setupwizard/time/DateTimeSetupActivity;->updateDateDisplay()V
    invoke-static {v0}, Lcom/google/android/setupwizard/time/DateTimeSetupActivity;->access$000(Lcom/google/android/setupwizard/time/DateTimeSetupActivity;)V

    .line 295
    return-void
.end method

.method public onResume()V
    .locals 5

    .prologue
    .line 284
    invoke-super {p0}, Landroid/app/DialogFragment;->onResume()V

    .line 285
    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object v0

    .line 286
    .local v0, "c":Ljava/util/Calendar;
    invoke-virtual {p0}, Lcom/google/android/setupwizard/time/DateTimeSetupActivity$DateDialog;->getDialog()Landroid/app/Dialog;

    move-result-object v1

    check-cast v1, Landroid/app/DatePickerDialog;

    const/4 v2, 0x1

    invoke-virtual {v0, v2}, Ljava/util/Calendar;->get(I)I

    move-result v2

    const/4 v3, 0x2

    invoke-virtual {v0, v3}, Ljava/util/Calendar;->get(I)I

    move-result v3

    const/4 v4, 0x5

    invoke-virtual {v0, v4}, Ljava/util/Calendar;->get(I)I

    move-result v4

    invoke-virtual {v1, v2, v3, v4}, Landroid/app/DatePickerDialog;->updateDate(III)V

    .line 288
    return-void
.end method

.method public show(Landroid/app/FragmentManager;Ljava/lang/String;)V
    .locals 1
    .param p1, "manager"    # Landroid/app/FragmentManager;
    .param p2, "tag"    # Ljava/lang/String;

    .prologue
    .line 267
    invoke-virtual {p1, p2}, Landroid/app/FragmentManager;->findFragmentByTag(Ljava/lang/String;)Landroid/app/Fragment;

    move-result-object v0

    if-nez v0, :cond_0

    .line 269
    invoke-super {p0, p1, p2}, Landroid/app/DialogFragment;->show(Landroid/app/FragmentManager;Ljava/lang/String;)V

    .line 271
    :cond_0
    return-void
.end method
