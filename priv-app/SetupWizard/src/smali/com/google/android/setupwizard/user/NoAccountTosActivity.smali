.class public Lcom/google/android/setupwizard/user/NoAccountTosActivity;
.super Lcom/google/android/setupwizard/BaseActivity;
.source "NoAccountTosActivity.java"


# instance fields
.field private final mLinkSpanReceiver:Landroid/content/BroadcastReceiver;

.field private mTosView:Landroid/widget/TextView;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 40
    invoke-direct {p0}, Lcom/google/android/setupwizard/BaseActivity;-><init>()V

    .line 46
    new-instance v0, Lcom/google/android/setupwizard/user/NoAccountTosActivity$1;

    invoke-direct {v0, p0}, Lcom/google/android/setupwizard/user/NoAccountTosActivity$1;-><init>(Lcom/google/android/setupwizard/user/NoAccountTosActivity;)V

    iput-object v0, p0, Lcom/google/android/setupwizard/user/NoAccountTosActivity;->mLinkSpanReceiver:Landroid/content/BroadcastReceiver;

    return-void
.end method


# virtual methods
.method protected onCreate(Landroid/os/Bundle;)V
    .locals 3
    .param p1, "icicle"    # Landroid/os/Bundle;

    .prologue
    .line 57
    invoke-super {p0, p1}, Lcom/google/android/setupwizard/BaseActivity;->onCreate(Landroid/os/Bundle;)V

    .line 58
    const v1, 0x7f030011

    const v2, 0x7f030023

    invoke-virtual {p0, v1, v2}, Lcom/google/android/setupwizard/user/NoAccountTosActivity;->setTemplateContent(II)V

    .line 59
    const v1, 0x7f07007e

    invoke-virtual {p0, v1}, Lcom/google/android/setupwizard/user/NoAccountTosActivity;->setHeaderText(I)V

    .line 61
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    .line 62
    .local v0, "links":Ljava/util/HashSet;, "Ljava/util/HashSet<Ljava/lang/String;>;"
    const-string v1, "learn_more"

    invoke-virtual {v0, v1}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    .line 63
    const v1, 0x7f0e0052

    invoke-virtual {p0, v1}, Lcom/google/android/setupwizard/user/NoAccountTosActivity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    iput-object v1, p0, Lcom/google/android/setupwizard/user/NoAccountTosActivity;->mTosView:Landroid/widget/TextView;

    .line 64
    iget-object v1, p0, Lcom/google/android/setupwizard/user/NoAccountTosActivity;->mTosView:Landroid/widget/TextView;

    const v2, 0x7f07007f

    invoke-static {p0, v2, v0}, Lcom/google/android/setupwizard/util/LinkSpan;->linkify(Landroid/content/Context;ILjava/util/Set;)Ljava/lang/CharSequence;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 65
    iget-object v1, p0, Lcom/google/android/setupwizard/user/NoAccountTosActivity;->mTosView:Landroid/widget/TextView;

    invoke-static {}, Landroid/text/method/LinkMovementMethod;->getInstance()Landroid/text/method/MovementMethod;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setMovementMethod(Landroid/text/method/MovementMethod;)V

    .line 66
    return-void
.end method

.method protected onPause()V
    .locals 2

    .prologue
    .line 83
    invoke-static {p0}, Landroid/support/v4/content/LocalBroadcastManager;->getInstance(Landroid/content/Context;)Landroid/support/v4/content/LocalBroadcastManager;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/setupwizard/user/NoAccountTosActivity;->mLinkSpanReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {v0, v1}, Landroid/support/v4/content/LocalBroadcastManager;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    .line 84
    invoke-super {p0}, Lcom/google/android/setupwizard/BaseActivity;->onPause()V

    .line 85
    return-void
.end method

.method protected onResume()V
    .locals 3

    .prologue
    .line 76
    invoke-super {p0}, Lcom/google/android/setupwizard/BaseActivity;->onResume()V

    .line 77
    new-instance v0, Landroid/content/IntentFilter;

    const-string v1, "com.google.android.setupwizard.LINK_SPAN_CLICKED"

    invoke-direct {v0, v1}, Landroid/content/IntentFilter;-><init>(Ljava/lang/String;)V

    .line 78
    .local v0, "filter":Landroid/content/IntentFilter;
    invoke-static {p0}, Landroid/support/v4/content/LocalBroadcastManager;->getInstance(Landroid/content/Context;)Landroid/support/v4/content/LocalBroadcastManager;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/setupwizard/user/NoAccountTosActivity;->mLinkSpanReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {v1, v2, v0}, Landroid/support/v4/content/LocalBroadcastManager;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)V

    .line 79
    return-void
.end method

.method public start()V
    .locals 0

    .prologue
    .line 70
    invoke-super {p0}, Lcom/google/android/setupwizard/BaseActivity;->start()V

    .line 71
    invoke-virtual {p0}, Lcom/google/android/setupwizard/user/NoAccountTosActivity;->nextScreen()V

    .line 72
    return-void
.end method
