.class public Lcom/google/android/setupwizard/user/WelcomeActivity;
.super Lcom/google/android/setupwizard/BaseActivity;
.source "WelcomeActivity.java"

# interfaces
.implements Landroid/view/View$OnClickListener;
.implements Lcom/google/android/setupwizard/util/WaitForGservicesTask$WaitForGservicesListener;


# static fields
.field private static final IMMEDIATE_WARM_WELCOME:Z


# instance fields
.field private mAccessibilityHelper:Lcom/google/android/setupwizard/accessibility/AccessibilityGestureHelper;

.field private final mAdapterIndices:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field private final mConnectionCheckedReceiver:Landroid/content/BroadcastReceiver;

.field protected mEmergencyCallButton:Landroid/widget/Button;

.field private final mEnableWifi:Ljava/lang/Runnable;

.field private final mHandler:Landroid/os/Handler;

.field protected mLanguagePicker:Lcom/google/android/setupwizard/util/LanguagePicker;

.field private mLeftFrame:Landroid/view/View;

.field private mLocaleAdapter:Landroid/widget/ArrayAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/widget/ArrayAdapter",
            "<",
            "Lcom/android/internal/app/LocalePicker$LocaleInfo;",
            ">;"
        }
    .end annotation
.end field

.field private mLocaleFilter:Landroid/content/IntentFilter;

.field private mLocaleReceiver:Landroid/content/BroadcastReceiver;

.field private mPickerLocale:Ljava/util/Locale;

.field private mProgressBar:Landroid/widget/ProgressBar;

.field private mStartButton:Landroid/widget/ImageButton;

.field private mTopDivider:Landroid/view/View;

.field private final mUpdateLocale:Ljava/lang/Runnable;

.field private mWaitMsg:Landroid/widget/TextView;

.field private mWarmerWelcomeWaiting:Z

.field private mWelcomeTitle:Landroid/widget/TextView;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 106
    const-string v0, "ro.setupwizard.welcome_asap"

    const/4 v1, 0x0

    invoke-static {v0, v1}, Landroid/os/SystemProperties;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    sput-boolean v0, Lcom/google/android/setupwizard/user/WelcomeActivity;->IMMEDIATE_WARM_WELCOME:Z

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 62
    invoke-direct {p0}, Lcom/google/android/setupwizard/BaseActivity;-><init>()V

    .line 83
    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    iput-object v0, p0, Lcom/google/android/setupwizard/user/WelcomeActivity;->mHandler:Landroid/os/Handler;

    .line 89
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/android/setupwizard/user/WelcomeActivity;->mAdapterIndices:Ljava/util/List;

    .line 111
    new-instance v0, Lcom/google/android/setupwizard/user/WelcomeActivity$1;

    invoke-direct {v0, p0}, Lcom/google/android/setupwizard/user/WelcomeActivity$1;-><init>(Lcom/google/android/setupwizard/user/WelcomeActivity;)V

    iput-object v0, p0, Lcom/google/android/setupwizard/user/WelcomeActivity;->mEnableWifi:Ljava/lang/Runnable;

    .line 122
    new-instance v0, Lcom/google/android/setupwizard/user/WelcomeActivity$2;

    invoke-direct {v0, p0}, Lcom/google/android/setupwizard/user/WelcomeActivity$2;-><init>(Lcom/google/android/setupwizard/user/WelcomeActivity;)V

    iput-object v0, p0, Lcom/google/android/setupwizard/user/WelcomeActivity;->mUpdateLocale:Ljava/lang/Runnable;

    .line 136
    new-instance v0, Lcom/google/android/setupwizard/user/WelcomeActivity$3;

    invoke-direct {v0, p0}, Lcom/google/android/setupwizard/user/WelcomeActivity$3;-><init>(Lcom/google/android/setupwizard/user/WelcomeActivity;)V

    iput-object v0, p0, Lcom/google/android/setupwizard/user/WelcomeActivity;->mConnectionCheckedReceiver:Landroid/content/BroadcastReceiver;

    return-void
.end method

.method static synthetic access$000(Lcom/google/android/setupwizard/user/WelcomeActivity;Z)Z
    .locals 1
    .param p0, "x0"    # Lcom/google/android/setupwizard/user/WelcomeActivity;
    .param p1, "x1"    # Z

    .prologue
    .line 62
    invoke-virtual {p0, p1}, Lcom/google/android/setupwizard/user/WelcomeActivity;->tryEnablingWifi(Z)Z

    move-result v0

    return v0
.end method

.method static synthetic access$100(Lcom/google/android/setupwizard/user/WelcomeActivity;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/setupwizard/user/WelcomeActivity;

    .prologue
    .line 62
    iget-object v0, p0, Lcom/google/android/setupwizard/user/WelcomeActivity;->TAG:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$200(Lcom/google/android/setupwizard/user/WelcomeActivity;)Ljava/util/Locale;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/setupwizard/user/WelcomeActivity;

    .prologue
    .line 62
    iget-object v0, p0, Lcom/google/android/setupwizard/user/WelcomeActivity;->mPickerLocale:Ljava/util/Locale;

    return-object v0
.end method

.method static synthetic access$202(Lcom/google/android/setupwizard/user/WelcomeActivity;Ljava/util/Locale;)Ljava/util/Locale;
    .locals 0
    .param p0, "x0"    # Lcom/google/android/setupwizard/user/WelcomeActivity;
    .param p1, "x1"    # Ljava/util/Locale;

    .prologue
    .line 62
    iput-object p1, p0, Lcom/google/android/setupwizard/user/WelcomeActivity;->mPickerLocale:Ljava/util/Locale;

    return-object p1
.end method

.method static synthetic access$300(Lcom/google/android/setupwizard/user/WelcomeActivity;)V
    .locals 0
    .param p0, "x0"    # Lcom/google/android/setupwizard/user/WelcomeActivity;

    .prologue
    .line 62
    invoke-direct {p0}, Lcom/google/android/setupwizard/user/WelcomeActivity;->checkWarmWelcomeData()V

    return-void
.end method

.method static synthetic access$400(Lcom/google/android/setupwizard/user/WelcomeActivity;Z)V
    .locals 0
    .param p0, "x0"    # Lcom/google/android/setupwizard/user/WelcomeActivity;
    .param p1, "x1"    # Z

    .prologue
    .line 62
    invoke-direct {p0, p1}, Lcom/google/android/setupwizard/user/WelcomeActivity;->setWarmerWelcomeWaitingMode(Z)V

    return-void
.end method

.method static synthetic access$500(Lcom/google/android/setupwizard/user/WelcomeActivity;)V
    .locals 0
    .param p0, "x0"    # Lcom/google/android/setupwizard/user/WelcomeActivity;

    .prologue
    .line 62
    invoke-direct {p0}, Lcom/google/android/setupwizard/user/WelcomeActivity;->updateAvailableLocales()V

    return-void
.end method

.method static synthetic access$600(Lcom/google/android/setupwizard/user/WelcomeActivity;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/setupwizard/user/WelcomeActivity;

    .prologue
    .line 62
    iget-object v0, p0, Lcom/google/android/setupwizard/user/WelcomeActivity;->TAG:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$700(Lcom/google/android/setupwizard/user/WelcomeActivity;Ljava/util/Locale;)V
    .locals 0
    .param p0, "x0"    # Lcom/google/android/setupwizard/user/WelcomeActivity;
    .param p1, "x1"    # Ljava/util/Locale;

    .prologue
    .line 62
    invoke-direct {p0, p1}, Lcom/google/android/setupwizard/user/WelcomeActivity;->updateForLocale(Ljava/util/Locale;)V

    return-void
.end method

.method static synthetic access$800(Lcom/google/android/setupwizard/user/WelcomeActivity;I)V
    .locals 0
    .param p0, "x0"    # Lcom/google/android/setupwizard/user/WelcomeActivity;
    .param p1, "x1"    # I

    .prologue
    .line 62
    invoke-direct {p0, p1}, Lcom/google/android/setupwizard/user/WelcomeActivity;->setLocaleFromLanguagePicker(I)V

    return-void
.end method

.method private checkConnectionAndWarmWelcomeData()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 226
    invoke-static {p0, v0, v0}, Lcom/google/android/setupwizard/network/CheckConnectionTask;->checkConnection(Landroid/content/Context;ZZ)V

    .line 228
    return-void
.end method

.method private checkWarmWelcomeData()V
    .locals 3

    .prologue
    .line 231
    const-wide/16 v0, 0x2710

    const-string v2, "welcome_gservices_task"

    invoke-static {p0, v0, v1, v2}, Lcom/google/android/setupwizard/util/WaitForGservicesTask;->getInstanceWithTag(Landroid/app/Activity;JLjava/lang/String;)Lcom/google/android/setupwizard/util/WaitForGservicesTask;

    .line 233
    return-void
.end method

.method private findLocaleIndex(Ljava/util/Locale;)I
    .locals 5
    .param p1, "locale"    # Ljava/util/Locale;

    .prologue
    .line 555
    const/4 v1, 0x0

    .local v1, "i":I
    iget-object v2, p0, Lcom/google/android/setupwizard/user/WelcomeActivity;->mLocaleAdapter:Landroid/widget/ArrayAdapter;

    invoke-virtual {v2}, Landroid/widget/ArrayAdapter;->getCount()I

    move-result v0

    .local v0, "count":I
    :goto_0
    if-ge v1, v0, :cond_1

    .line 556
    iget-object v2, p0, Lcom/google/android/setupwizard/user/WelcomeActivity;->mLocaleAdapter:Landroid/widget/ArrayAdapter;

    invoke-virtual {v2, v1}, Landroid/widget/ArrayAdapter;->getItem(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/android/internal/app/LocalePicker$LocaleInfo;

    invoke-virtual {v2}, Lcom/android/internal/app/LocalePicker$LocaleInfo;->getLocale()Ljava/util/Locale;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/util/Locale;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 557
    iget-object v2, p0, Lcom/google/android/setupwizard/user/WelcomeActivity;->TAG:Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "findLocaleIndex("

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ") returns "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 558
    iget-object v2, p0, Lcom/google/android/setupwizard/user/WelcomeActivity;->mAdapterIndices:Ljava/util/List;

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-interface {v2, v3}, Ljava/util/List;->indexOf(Ljava/lang/Object;)I

    move-result v2

    .line 562
    :goto_1
    return v2

    .line 555
    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 561
    :cond_1
    iget-object v2, p0, Lcom/google/android/setupwizard/user/WelcomeActivity;->TAG:Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "findLocaleIndex("

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ") returns -1 (not found)"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 562
    const/4 v2, -0x1

    goto :goto_1
.end method

.method private finishAndReturnResult()V
    .locals 2

    .prologue
    const/4 v1, -0x1

    .line 578
    invoke-virtual {p0}, Lcom/google/android/setupwizard/user/WelcomeActivity;->isUsingWizardManager()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 579
    invoke-virtual {p0, v1}, Lcom/google/android/setupwizard/user/WelcomeActivity;->nextAction(I)Z

    .line 583
    :goto_0
    return-void

    .line 581
    :cond_0
    invoke-virtual {p0, v1}, Lcom/google/android/setupwizard/user/WelcomeActivity;->finishAction(I)V

    goto :goto_0
.end method

.method private getCurrentLocale()Ljava/util/Locale;
    .locals 6

    .prologue
    .line 313
    const/4 v0, 0x0

    .line 315
    .local v0, "config":Landroid/content/res/Configuration;
    iget-object v3, p0, Lcom/google/android/setupwizard/user/WelcomeActivity;->mPickerLocale:Ljava/util/Locale;

    if-eqz v3, :cond_0

    .line 316
    iget-object v2, p0, Lcom/google/android/setupwizard/user/WelcomeActivity;->mPickerLocale:Ljava/util/Locale;

    .line 325
    .local v2, "locale":Ljava/util/Locale;
    :goto_0
    iget-object v4, p0, Lcom/google/android/setupwizard/user/WelcomeActivity;->TAG:Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "getCurrentLocale return val="

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v5, " mPickerLocale="

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v5, p0, Lcom/google/android/setupwizard/user/WelcomeActivity;->mPickerLocale:Ljava/util/Locale;

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v5, " config.locale="

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    if-nez v0, :cond_1

    const-string v3, "config is null"

    :goto_1
    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v5, " Locale.getDefault()="

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v5

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v4, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 329
    return-object v2

    .line 319
    .end local v2    # "locale":Ljava/util/Locale;
    :cond_0
    :try_start_0
    invoke-static {}, Landroid/app/ActivityManagerNative;->getDefault()Landroid/app/IActivityManager;

    move-result-object v3

    invoke-interface {v3}, Landroid/app/IActivityManager;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v0

    .line 320
    iget-object v2, v0, Landroid/content/res/Configuration;->locale:Ljava/util/Locale;
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .restart local v2    # "locale":Ljava/util/Locale;
    goto :goto_0

    .line 321
    .end local v2    # "locale":Ljava/util/Locale;
    :catch_0
    move-exception v1

    .line 322
    .local v1, "e":Landroid/os/RemoteException;
    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v2

    .restart local v2    # "locale":Ljava/util/Locale;
    goto :goto_0

    .line 325
    .end local v1    # "e":Landroid/os/RemoteException;
    :cond_1
    iget-object v3, v0, Landroid/content/res/Configuration;->locale:Ljava/util/Locale;

    goto :goto_1
.end method

.method private getIntArray(Ljava/lang/String;)[I
    .locals 5
    .param p1, "intArrayString"    # Ljava/lang/String;

    .prologue
    .line 538
    const-string v4, ","

    invoke-static {p1, v4}, Landroid/text/TextUtils;->split(Ljava/lang/String;Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v2

    .line 539
    .local v2, "items":[Ljava/lang/String;
    array-length v4, v2

    new-array v1, v4, [I

    .line 540
    .local v1, "intArray":[I
    const/4 v0, 0x0

    .local v0, "i":I
    array-length v3, v2

    .local v3, "size":I
    :goto_0
    if-ge v0, v3, :cond_0

    .line 541
    aget-object v4, v2, v0

    invoke-static {v4}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v4

    aput v4, v1, v0

    .line 540
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 543
    :cond_0
    return-object v1
.end method

.method private getLocaleNames(Ljava/util/List;)[Ljava/lang/String;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Ljava/lang/Integer;",
            ">;)[",
            "Ljava/lang/String;"
        }
    .end annotation

    .prologue
    .line 547
    .local p1, "indices":Ljava/util/List;, "Ljava/util/List<Ljava/lang/Integer;>;"
    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v3

    new-array v1, v3, [Ljava/lang/String;

    .line 548
    .local v1, "localeNames":[Ljava/lang/String;
    const/4 v0, 0x0

    .local v0, "i":I
    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v2

    .local v2, "size":I
    :goto_0
    if-ge v0, v2, :cond_0

    .line 549
    iget-object v4, p0, Lcom/google/android/setupwizard/user/WelcomeActivity;->mLocaleAdapter:Landroid/widget/ArrayAdapter;

    invoke-interface {p1, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Integer;

    invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I

    move-result v3

    invoke-virtual {v4, v3}, Landroid/widget/ArrayAdapter;->getItem(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/android/internal/app/LocalePicker$LocaleInfo;

    invoke-virtual {v3}, Lcom/android/internal/app/LocalePicker$LocaleInfo;->getLabel()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v1, v0

    .line 548
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 551
    :cond_0
    return-object v1
.end method

.method private initViews()V
    .locals 3

    .prologue
    const/16 v2, 0x8

    .line 430
    const v0, 0x7f030020

    invoke-virtual {p0, v0}, Lcom/google/android/setupwizard/user/WelcomeActivity;->setContentView(I)V

    .line 432
    const v0, 0x7f0e0069

    invoke-virtual {p0, v0}, Lcom/google/android/setupwizard/user/WelcomeActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/setupwizard/user/WelcomeActivity;->mLeftFrame:Landroid/view/View;

    .line 433
    const v0, 0x7f0e0066

    invoke-virtual {p0, v0}, Lcom/google/android/setupwizard/user/WelcomeActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageButton;

    iput-object v0, p0, Lcom/google/android/setupwizard/user/WelcomeActivity;->mStartButton:Landroid/widget/ImageButton;

    .line 434
    iget-object v0, p0, Lcom/google/android/setupwizard/user/WelcomeActivity;->mStartButton:Landroid/widget/ImageButton;

    invoke-virtual {v0, p0}, Landroid/widget/ImageButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 435
    const v0, 0x7f0e002a

    invoke-virtual {p0, v0}, Lcom/google/android/setupwizard/user/WelcomeActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ProgressBar;

    iput-object v0, p0, Lcom/google/android/setupwizard/user/WelcomeActivity;->mProgressBar:Landroid/widget/ProgressBar;

    .line 436
    const v0, 0x7f0e0033

    invoke-virtual {p0, v0}, Lcom/google/android/setupwizard/user/WelcomeActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/setupwizard/user/WelcomeActivity;->mTopDivider:Landroid/view/View;

    .line 437
    const v0, 0x7f0e0064

    invoke-virtual {p0, v0}, Lcom/google/android/setupwizard/user/WelcomeActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/google/android/setupwizard/user/WelcomeActivity;->mWaitMsg:Landroid/widget/TextView;

    .line 438
    const v0, 0x7f0e0063

    invoke-virtual {p0, v0}, Lcom/google/android/setupwizard/user/WelcomeActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/google/android/setupwizard/user/WelcomeActivity;->mWelcomeTitle:Landroid/widget/TextView;

    .line 440
    const v0, 0x7f0e0065

    invoke-virtual {p0, v0}, Lcom/google/android/setupwizard/user/WelcomeActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/setupwizard/util/LanguagePicker;

    iput-object v0, p0, Lcom/google/android/setupwizard/user/WelcomeActivity;->mLanguagePicker:Lcom/google/android/setupwizard/util/LanguagePicker;

    .line 442
    invoke-virtual {p0}, Lcom/google/android/setupwizard/user/WelcomeActivity;->isPrimaryUser()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 443
    iget-object v0, p0, Lcom/google/android/setupwizard/user/WelcomeActivity;->mLanguagePicker:Lcom/google/android/setupwizard/util/LanguagePicker;

    new-instance v1, Lcom/google/android/setupwizard/user/WelcomeActivity$6;

    invoke-direct {v1, p0}, Lcom/google/android/setupwizard/user/WelcomeActivity$6;-><init>(Lcom/google/android/setupwizard/user/WelcomeActivity;)V

    invoke-virtual {v0, v1}, Lcom/google/android/setupwizard/util/LanguagePicker;->setOnLanguageSelectedListener(Lcom/google/android/setupwizard/util/LanguagePicker$OnLanguageSelectedListener;)V

    .line 449
    invoke-direct {p0}, Lcom/google/android/setupwizard/user/WelcomeActivity;->updateAvailableLocales()V

    .line 454
    :goto_0
    const v0, 0x7f0e0067

    invoke-virtual {p0, v0}, Lcom/google/android/setupwizard/user/WelcomeActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lcom/google/android/setupwizard/user/WelcomeActivity;->mEmergencyCallButton:Landroid/widget/Button;

    .line 455
    invoke-virtual {p0}, Lcom/google/android/setupwizard/user/WelcomeActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x1120043

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getBoolean(I)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 456
    iget-object v0, p0, Lcom/google/android/setupwizard/user/WelcomeActivity;->mEmergencyCallButton:Landroid/widget/Button;

    invoke-virtual {v0, p0}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 461
    :goto_1
    invoke-direct {p0}, Lcom/google/android/setupwizard/user/WelcomeActivity;->updateContentView()V

    .line 462
    return-void

    .line 451
    :cond_0
    iget-object v0, p0, Lcom/google/android/setupwizard/user/WelcomeActivity;->mLanguagePicker:Lcom/google/android/setupwizard/util/LanguagePicker;

    invoke-virtual {v0, v2}, Lcom/google/android/setupwizard/util/LanguagePicker;->setVisibility(I)V

    goto :goto_0

    .line 458
    :cond_1
    iget-object v0, p0, Lcom/google/android/setupwizard/user/WelcomeActivity;->mEmergencyCallButton:Landroid/widget/Button;

    invoke-virtual {v0, v2}, Landroid/widget/Button;->setVisibility(I)V

    goto :goto_1
.end method

.method private setLocaleFromLanguagePicker(I)V
    .locals 3
    .param p1, "position"    # I

    .prologue
    .line 569
    iget-object v2, p0, Lcom/google/android/setupwizard/user/WelcomeActivity;->mAdapterIndices:Ljava/util/List;

    invoke-interface {v2, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v0

    .line 570
    .local v0, "adapterIndex":I
    iget-object v2, p0, Lcom/google/android/setupwizard/user/WelcomeActivity;->mLocaleAdapter:Landroid/widget/ArrayAdapter;

    invoke-virtual {v2, v0}, Landroid/widget/ArrayAdapter;->getItem(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/android/internal/app/LocalePicker$LocaleInfo;

    .line 571
    .local v1, "localeInfo":Lcom/android/internal/app/LocalePicker$LocaleInfo;
    invoke-virtual {v1}, Lcom/android/internal/app/LocalePicker$LocaleInfo;->getLocale()Ljava/util/Locale;

    move-result-object v2

    invoke-direct {p0, v2}, Lcom/google/android/setupwizard/user/WelcomeActivity;->updateForLocale(Ljava/util/Locale;)V

    .line 572
    return-void
.end method

.method private setWarmerWelcomeWaitingMode(Z)V
    .locals 0
    .param p1, "isWaiting"    # Z

    .prologue
    .line 384
    iput-boolean p1, p0, Lcom/google/android/setupwizard/user/WelcomeActivity;->mWarmerWelcomeWaiting:Z

    .line 385
    invoke-direct {p0}, Lcom/google/android/setupwizard/user/WelcomeActivity;->updateContentView()V

    .line 386
    return-void
.end method

.method private showWarmWelcome()V
    .locals 1

    .prologue
    .line 237
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/google/android/setupwizard/user/WelcomeActivity;->setWarmerWelcomeWaitingMode(Z)V

    .line 238
    return-void
.end method

.method private updateAvailableLocales()V
    .locals 4

    .prologue
    .line 513
    iget-object v1, p0, Lcom/google/android/setupwizard/user/WelcomeActivity;->mAccessibilityHelper:Lcom/google/android/setupwizard/accessibility/AccessibilityGestureHelper;

    invoke-virtual {v1}, Lcom/google/android/setupwizard/accessibility/AccessibilityGestureHelper;->isAccessibilityEnabled()Z

    move-result v0

    .line 514
    .local v0, "accessibilityEnabled":Z
    iget-object v1, p0, Lcom/google/android/setupwizard/user/WelcomeActivity;->TAG:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "updateAvailableLocales() current locale="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-direct {p0}, Lcom/google/android/setupwizard/user/WelcomeActivity;->getCurrentLocale()Ljava/util/Locale;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " accessibilityEnabled="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 517
    if-eqz v0, :cond_0

    .line 518
    iget-object v1, p0, Lcom/google/android/setupwizard/user/WelcomeActivity;->mLanguagePicker:Lcom/google/android/setupwizard/util/LanguagePicker;

    invoke-virtual {v1}, Lcom/google/android/setupwizard/util/LanguagePicker;->useSpinner()V

    .line 519
    invoke-direct {p0}, Lcom/google/android/setupwizard/user/WelcomeActivity;->useAccessibilityLocales()V

    .line 525
    :goto_0
    iget-object v1, p0, Lcom/google/android/setupwizard/user/WelcomeActivity;->mLanguagePicker:Lcom/google/android/setupwizard/util/LanguagePicker;

    iget-object v2, p0, Lcom/google/android/setupwizard/user/WelcomeActivity;->mAdapterIndices:Ljava/util/List;

    invoke-direct {p0, v2}, Lcom/google/android/setupwizard/user/WelcomeActivity;->getLocaleNames(Ljava/util/List;)[Ljava/lang/String;

    move-result-object v2

    invoke-direct {p0}, Lcom/google/android/setupwizard/user/WelcomeActivity;->getCurrentLocale()Ljava/util/Locale;

    move-result-object v3

    invoke-direct {p0, v3}, Lcom/google/android/setupwizard/user/WelcomeActivity;->findLocaleIndex(Ljava/util/Locale;)I

    move-result v3

    invoke-virtual {v1, v2, v3}, Lcom/google/android/setupwizard/util/LanguagePicker;->setLocales([Ljava/lang/String;I)V

    .line 528
    return-void

    .line 521
    :cond_0
    iget-object v1, p0, Lcom/google/android/setupwizard/user/WelcomeActivity;->mLanguagePicker:Lcom/google/android/setupwizard/util/LanguagePicker;

    invoke-virtual {v1}, Lcom/google/android/setupwizard/util/LanguagePicker;->useNumberPicker()V

    .line 522
    invoke-direct {p0}, Lcom/google/android/setupwizard/user/WelcomeActivity;->useAllLocales()V

    goto :goto_0
.end method

.method private updateContentView()V
    .locals 4

    .prologue
    const/16 v3, 0x8

    const/4 v2, 0x4

    const/4 v1, 0x0

    .line 389
    iget-boolean v0, p0, Lcom/google/android/setupwizard/user/WelcomeActivity;->mWarmerWelcomeWaiting:Z

    if-eqz v0, :cond_4

    .line 392
    iget-object v0, p0, Lcom/google/android/setupwizard/user/WelcomeActivity;->mStartButton:Landroid/widget/ImageButton;

    invoke-virtual {v0, v2}, Landroid/widget/ImageButton;->setVisibility(I)V

    .line 393
    iget-object v0, p0, Lcom/google/android/setupwizard/user/WelcomeActivity;->mLanguagePicker:Lcom/google/android/setupwizard/util/LanguagePicker;

    invoke-virtual {v0}, Lcom/google/android/setupwizard/util/LanguagePicker;->getVisibility()I

    move-result v0

    if-eq v0, v3, :cond_0

    .line 394
    iget-object v0, p0, Lcom/google/android/setupwizard/user/WelcomeActivity;->mLanguagePicker:Lcom/google/android/setupwizard/util/LanguagePicker;

    invoke-virtual {v0, v2}, Lcom/google/android/setupwizard/util/LanguagePicker;->setVisibility(I)V

    .line 396
    :cond_0
    iget-object v0, p0, Lcom/google/android/setupwizard/user/WelcomeActivity;->mEmergencyCallButton:Landroid/widget/Button;

    invoke-virtual {v0}, Landroid/widget/Button;->getVisibility()I

    move-result v0

    if-eq v0, v3, :cond_1

    .line 397
    iget-object v0, p0, Lcom/google/android/setupwizard/user/WelcomeActivity;->mEmergencyCallButton:Landroid/widget/Button;

    invoke-virtual {v0, v2}, Landroid/widget/Button;->setVisibility(I)V

    .line 399
    :cond_1
    iget-object v0, p0, Lcom/google/android/setupwizard/user/WelcomeActivity;->mTopDivider:Landroid/view/View;

    if-eqz v0, :cond_2

    .line 400
    iget-object v0, p0, Lcom/google/android/setupwizard/user/WelcomeActivity;->mTopDivider:Landroid/view/View;

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    .line 403
    :cond_2
    iget-object v0, p0, Lcom/google/android/setupwizard/user/WelcomeActivity;->mWaitMsg:Landroid/widget/TextView;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    .line 404
    iget-object v0, p0, Lcom/google/android/setupwizard/user/WelcomeActivity;->mProgressBar:Landroid/widget/ProgressBar;

    if-eqz v0, :cond_3

    .line 405
    iget-object v0, p0, Lcom/google/android/setupwizard/user/WelcomeActivity;->mProgressBar:Landroid/widget/ProgressBar;

    invoke-virtual {v0, v1}, Landroid/widget/ProgressBar;->setVisibility(I)V

    .line 426
    :cond_3
    :goto_0
    invoke-virtual {p0}, Lcom/google/android/setupwizard/user/WelcomeActivity;->getWindow()Landroid/view/Window;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/Window;->getDecorView()Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/View;->invalidate()V

    .line 427
    return-void

    .line 410
    :cond_4
    iget-object v0, p0, Lcom/google/android/setupwizard/user/WelcomeActivity;->mStartButton:Landroid/widget/ImageButton;

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setVisibility(I)V

    .line 411
    iget-object v0, p0, Lcom/google/android/setupwizard/user/WelcomeActivity;->mLanguagePicker:Lcom/google/android/setupwizard/util/LanguagePicker;

    invoke-virtual {v0}, Lcom/google/android/setupwizard/util/LanguagePicker;->getVisibility()I

    move-result v0

    if-eq v0, v3, :cond_5

    .line 412
    iget-object v0, p0, Lcom/google/android/setupwizard/user/WelcomeActivity;->mLanguagePicker:Lcom/google/android/setupwizard/util/LanguagePicker;

    invoke-virtual {v0, v1}, Lcom/google/android/setupwizard/util/LanguagePicker;->setVisibility(I)V

    .line 414
    :cond_5
    iget-object v0, p0, Lcom/google/android/setupwizard/user/WelcomeActivity;->mEmergencyCallButton:Landroid/widget/Button;

    invoke-virtual {v0}, Landroid/widget/Button;->getVisibility()I

    move-result v0

    if-eq v0, v3, :cond_6

    .line 415
    iget-object v0, p0, Lcom/google/android/setupwizard/user/WelcomeActivity;->mEmergencyCallButton:Landroid/widget/Button;

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setVisibility(I)V

    .line 417
    :cond_6
    iget-object v0, p0, Lcom/google/android/setupwizard/user/WelcomeActivity;->mTopDivider:Landroid/view/View;

    if-eqz v0, :cond_7

    .line 418
    iget-object v0, p0, Lcom/google/android/setupwizard/user/WelcomeActivity;->mTopDivider:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 421
    :cond_7
    iget-object v0, p0, Lcom/google/android/setupwizard/user/WelcomeActivity;->mWaitMsg:Landroid/widget/TextView;

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setVisibility(I)V

    .line 422
    iget-object v0, p0, Lcom/google/android/setupwizard/user/WelcomeActivity;->mProgressBar:Landroid/widget/ProgressBar;

    if-eqz v0, :cond_3

    .line 423
    iget-object v0, p0, Lcom/google/android/setupwizard/user/WelcomeActivity;->mProgressBar:Landroid/widget/ProgressBar;

    invoke-virtual {v0, v2}, Landroid/widget/ProgressBar;->setVisibility(I)V

    goto :goto_0
.end method

.method private updateForLocale(Ljava/util/Locale;)V
    .locals 8
    .param p1, "locale"    # Ljava/util/Locale;

    .prologue
    const/4 v7, 0x0

    .line 343
    iget-object v4, p0, Lcom/google/android/setupwizard/user/WelcomeActivity;->TAG:Ljava/lang/String;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "updateForLocale("

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, ")"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 345
    iget-object v4, p0, Lcom/google/android/setupwizard/user/WelcomeActivity;->mHandler:Landroid/os/Handler;

    iget-object v5, p0, Lcom/google/android/setupwizard/user/WelcomeActivity;->mUpdateLocale:Ljava/lang/Runnable;

    invoke-virtual {v4, v5}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    .line 348
    invoke-virtual {p0}, Lcom/google/android/setupwizard/user/WelcomeActivity;->getBaseContext()Landroid/content/Context;

    move-result-object v4

    invoke-virtual {v4}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    .line 349
    .local v3, "res":Landroid/content/res/Resources;
    invoke-virtual {v3}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v0

    .line 350
    .local v0, "baseConfig":Landroid/content/res/Configuration;
    new-instance v1, Landroid/content/res/Configuration;

    invoke-direct {v1, v0}, Landroid/content/res/Configuration;-><init>(Landroid/content/res/Configuration;)V

    .line 351
    .local v1, "config":Landroid/content/res/Configuration;
    invoke-virtual {v1, p1}, Landroid/content/res/Configuration;->setLocale(Ljava/util/Locale;)V

    .line 352
    invoke-virtual {v3, v1, v7}, Landroid/content/res/Resources;->updateConfiguration(Landroid/content/res/Configuration;Landroid/util/DisplayMetrics;)V

    .line 354
    invoke-static {p1}, Landroid/text/TextUtils;->getLayoutDirectionFromLocale(Ljava/util/Locale;)I

    move-result v2

    .line 355
    .local v2, "direction":I
    iget-object v4, p0, Lcom/google/android/setupwizard/user/WelcomeActivity;->mWelcomeTitle:Landroid/widget/TextView;

    const v5, 0x7f07003c

    invoke-virtual {v4, v5}, Landroid/widget/TextView;->setText(I)V

    .line 356
    iget-object v4, p0, Lcom/google/android/setupwizard/user/WelcomeActivity;->mWaitMsg:Landroid/widget/TextView;

    const v5, 0x7f070051

    invoke-virtual {v4, v5}, Landroid/widget/TextView;->setText(I)V

    .line 357
    iget-object v4, p0, Lcom/google/android/setupwizard/user/WelcomeActivity;->mStartButton:Landroid/widget/ImageButton;

    invoke-virtual {v4, v2}, Landroid/widget/ImageButton;->setLayoutDirection(I)V

    .line 358
    iget-object v4, p0, Lcom/google/android/setupwizard/user/WelcomeActivity;->mStartButton:Landroid/widget/ImageButton;

    invoke-virtual {v4, v7}, Landroid/widget/ImageButton;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 359
    iget-object v4, p0, Lcom/google/android/setupwizard/user/WelcomeActivity;->mStartButton:Landroid/widget/ImageButton;

    const v5, 0x7f020062

    invoke-virtual {v4, v5}, Landroid/widget/ImageButton;->setImageResource(I)V

    .line 360
    iget-object v4, p0, Lcom/google/android/setupwizard/user/WelcomeActivity;->mEmergencyCallButton:Landroid/widget/Button;

    const v5, 0x7f0700a0

    invoke-virtual {v4, v5}, Landroid/widget/Button;->setText(I)V

    .line 361
    iget-object v4, p0, Lcom/google/android/setupwizard/user/WelcomeActivity;->mEmergencyCallButton:Landroid/widget/Button;

    invoke-virtual {v4, v2}, Landroid/widget/Button;->setLayoutDirection(I)V

    .line 362
    iget-object v4, p0, Lcom/google/android/setupwizard/user/WelcomeActivity;->mLeftFrame:Landroid/view/View;

    if-eqz v4, :cond_0

    .line 364
    iget-object v4, p0, Lcom/google/android/setupwizard/user/WelcomeActivity;->mLeftFrame:Landroid/view/View;

    invoke-virtual {v4, v2}, Landroid/view/View;->setLayoutDirection(I)V

    .line 365
    iget-object v4, p0, Lcom/google/android/setupwizard/user/WelcomeActivity;->mLeftFrame:Landroid/view/View;

    invoke-virtual {v4}, Landroid/view/View;->requestLayout()V

    .line 368
    :cond_0
    iput-object p1, p0, Lcom/google/android/setupwizard/user/WelcomeActivity;->mPickerLocale:Ljava/util/Locale;

    .line 370
    iget-object v4, p0, Lcom/google/android/setupwizard/user/WelcomeActivity;->mHandler:Landroid/os/Handler;

    iget-object v5, p0, Lcom/google/android/setupwizard/user/WelcomeActivity;->mUpdateLocale:Ljava/lang/Runnable;

    const-wide/16 v6, 0x3e8

    invoke-virtual {v4, v5, v6, v7}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 371
    return-void
.end method

.method private useAccessibilityLocales()V
    .locals 8

    .prologue
    .line 482
    iget-object v5, p0, Lcom/google/android/setupwizard/user/WelcomeActivity;->TAG:Ljava/lang/String;

    const-string v6, "useAccessibilityLocales()"

    invoke-static {v5, v6}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 483
    iget-object v5, p0, Lcom/google/android/setupwizard/user/WelcomeActivity;->mLocaleAdapter:Landroid/widget/ArrayAdapter;

    invoke-virtual {v5}, Landroid/widget/ArrayAdapter;->getCount()I

    move-result v4

    .line 485
    .local v4, "totalCount":I
    iget-object v5, p0, Lcom/google/android/setupwizard/user/WelcomeActivity;->mAdapterIndices:Ljava/util/List;

    invoke-interface {v5}, Ljava/util/List;->size()I

    move-result v5

    if-ne v5, v4, :cond_4

    .line 486
    iget-object v5, p0, Lcom/google/android/setupwizard/user/WelcomeActivity;->mAdapterIndices:Ljava/util/List;

    invoke-interface {v5}, Ljava/util/List;->clear()V

    .line 487
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    if-ge v0, v4, :cond_1

    .line 488
    iget-object v5, p0, Lcom/google/android/setupwizard/user/WelcomeActivity;->mLocaleAdapter:Landroid/widget/ArrayAdapter;

    invoke-virtual {v5, v0}, Landroid/widget/ArrayAdapter;->getItem(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/android/internal/app/LocalePicker$LocaleInfo;

    invoke-virtual {v5}, Lcom/android/internal/app/LocalePicker$LocaleInfo;->getLocale()Ljava/util/Locale;

    move-result-object v2

    .line 489
    .local v2, "locale":Ljava/util/Locale;
    iget-object v5, p0, Lcom/google/android/setupwizard/user/WelcomeActivity;->mAccessibilityHelper:Lcom/google/android/setupwizard/accessibility/AccessibilityGestureHelper;

    invoke-virtual {v5, v2}, Lcom/google/android/setupwizard/accessibility/AccessibilityGestureHelper;->isTtsLocaleAvailable(Ljava/util/Locale;)Z

    move-result v5

    if-eqz v5, :cond_0

    .line 490
    iget-object v5, p0, Lcom/google/android/setupwizard/user/WelcomeActivity;->mAdapterIndices:Ljava/util/List;

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    invoke-interface {v5, v6}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 487
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 493
    .end local v2    # "locale":Ljava/util/Locale;
    :cond_1
    const-string v5, "accessibility_indices"

    const-string v6, ","

    iget-object v7, p0, Lcom/google/android/setupwizard/user/WelcomeActivity;->mAdapterIndices:Ljava/util/List;

    invoke-interface {v7}, Ljava/util/List;->toArray()[Ljava/lang/Object;

    move-result-object v7

    invoke-static {v6, v7}, Landroid/text/TextUtils;->join(Ljava/lang/CharSequence;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {p0, v5, v6}, Lcom/google/android/setupwizard/user/WelcomeActivity;->putPrefsString(Ljava/lang/String;Ljava/lang/String;)V

    .line 502
    .end local v0    # "i":I
    :cond_2
    iget-object v5, p0, Lcom/google/android/setupwizard/user/WelcomeActivity;->mAdapterIndices:Ljava/util/List;

    invoke-interface {v5}, Ljava/util/List;->size()I

    move-result v5

    if-nez v5, :cond_3

    .line 507
    iget-object v5, p0, Lcom/google/android/setupwizard/user/WelcomeActivity;->TAG:Ljava/lang/String;

    const-string v6, "no locales listed for accessibility; reverting to all"

    invoke-static {v5, v6}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 508
    invoke-direct {p0}, Lcom/google/android/setupwizard/user/WelcomeActivity;->useAllLocales()V

    .line 510
    :cond_3
    return-void

    .line 495
    :cond_4
    iget-object v5, p0, Lcom/google/android/setupwizard/user/WelcomeActivity;->mAdapterIndices:Ljava/util/List;

    invoke-interface {v5}, Ljava/util/List;->size()I

    move-result v5

    if-nez v5, :cond_2

    .line 496
    const-string v5, "accessibility_indices"

    invoke-virtual {p0, v5}, Lcom/google/android/setupwizard/user/WelcomeActivity;->getPrefsString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    invoke-direct {p0, v5}, Lcom/google/android/setupwizard/user/WelcomeActivity;->getIntArray(Ljava/lang/String;)[I

    move-result-object v1

    .line 497
    .local v1, "indices":[I
    const/4 v0, 0x0

    .restart local v0    # "i":I
    array-length v3, v1

    .local v3, "size":I
    :goto_1
    if-ge v0, v3, :cond_2

    .line 498
    iget-object v5, p0, Lcom/google/android/setupwizard/user/WelcomeActivity;->mAdapterIndices:Ljava/util/List;

    aget v6, v1, v0

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    invoke-interface {v5, v6}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 497
    add-int/lit8 v0, v0, 0x1

    goto :goto_1
.end method

.method private useAllLocales()V
    .locals 4

    .prologue
    .line 468
    iget-object v2, p0, Lcom/google/android/setupwizard/user/WelcomeActivity;->TAG:Ljava/lang/String;

    const-string v3, "useAllLocales()"

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 469
    iget-object v2, p0, Lcom/google/android/setupwizard/user/WelcomeActivity;->mLocaleAdapter:Landroid/widget/ArrayAdapter;

    invoke-virtual {v2}, Landroid/widget/ArrayAdapter;->getCount()I

    move-result v1

    .line 470
    .local v1, "totalCount":I
    iget-object v2, p0, Lcom/google/android/setupwizard/user/WelcomeActivity;->mAdapterIndices:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v2

    if-eq v2, v1, :cond_0

    .line 471
    iget-object v2, p0, Lcom/google/android/setupwizard/user/WelcomeActivity;->mAdapterIndices:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->clear()V

    .line 472
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    if-ge v0, v1, :cond_0

    .line 473
    iget-object v2, p0, Lcom/google/android/setupwizard/user/WelcomeActivity;->mAdapterIndices:Ljava/util/List;

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-interface {v2, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 472
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 476
    .end local v0    # "i":I
    :cond_0
    return-void
.end method


# virtual methods
.method public dispatchTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 1
    .param p1, "ev"    # Landroid/view/MotionEvent;

    .prologue
    .line 377
    iget-object v0, p0, Lcom/google/android/setupwizard/user/WelcomeActivity;->mAccessibilityHelper:Lcom/google/android/setupwizard/accessibility/AccessibilityGestureHelper;

    invoke-virtual {v0, p1}, Lcom/google/android/setupwizard/accessibility/AccessibilityGestureHelper;->dispatchTouchEvent(Landroid/view/MotionEvent;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 378
    const/4 v0, 0x1

    .line 380
    :goto_0
    return v0

    :cond_0
    invoke-super {p0, p1}, Lcom/google/android/setupwizard/BaseActivity;->dispatchTouchEvent(Landroid/view/MotionEvent;)Z

    move-result v0

    goto :goto_0
.end method

.method protected getNextTransition()I
    .locals 1

    .prologue
    .line 303
    const/4 v0, 0x1

    return v0
.end method

.method public getStyleForTheme(Ljava/lang/String;)I
    .locals 1
    .param p1, "themeName"    # Ljava/lang/String;

    .prologue
    .line 297
    const v0, 0x7f060026

    return v0
.end method

.method public onBackPressed()V
    .locals 2

    .prologue
    .line 334
    iget-object v0, p0, Lcom/google/android/setupwizard/user/WelcomeActivity;->TAG:Ljava/lang/String;

    const-string v1, "WelcomeActivity.onBackPressed()"

    invoke-static {v0, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 335
    return-void
.end method

.method public onClick(Landroid/view/View;)V
    .locals 1
    .param p1, "v"    # Landroid/view/View;

    .prologue
    .line 163
    invoke-virtual {p0}, Lcom/google/android/setupwizard/user/WelcomeActivity;->isPrimaryUser()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 164
    invoke-direct {p0}, Lcom/google/android/setupwizard/user/WelcomeActivity;->getCurrentLocale()Ljava/util/Locale;

    move-result-object v0

    invoke-static {v0}, Lcom/android/internal/app/LocalePicker;->updateLocale(Ljava/util/Locale;)V

    .line 167
    :cond_0
    iget-object v0, p0, Lcom/google/android/setupwizard/user/WelcomeActivity;->mStartButton:Landroid/widget/ImageButton;

    if-ne p1, v0, :cond_2

    .line 168
    invoke-direct {p0}, Lcom/google/android/setupwizard/user/WelcomeActivity;->finishAndReturnResult()V

    .line 172
    :cond_1
    :goto_0
    return-void

    .line 169
    :cond_2
    iget-object v0, p0, Lcom/google/android/setupwizard/user/WelcomeActivity;->mEmergencyCallButton:Landroid/widget/Button;

    if-ne p1, v0, :cond_1

    .line 170
    invoke-virtual {p0}, Lcom/google/android/setupwizard/user/WelcomeActivity;->placeEmergencyCall()V

    goto :goto_0
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 3
    .param p1, "icicle"    # Landroid/os/Bundle;

    .prologue
    .line 176
    invoke-super {p0, p1}, Lcom/google/android/setupwizard/BaseActivity;->onCreate(Landroid/os/Bundle;)V

    .line 178
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/google/android/setupwizard/user/WelcomeActivity;->setBackAllowed(Z)V

    .line 180
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/setupwizard/user/WelcomeActivity;->mPickerLocale:Ljava/util/Locale;

    .line 181
    iget-object v0, p0, Lcom/google/android/setupwizard/user/WelcomeActivity;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "onCreate current locale="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-direct {p0}, Lcom/google/android/setupwizard/user/WelcomeActivity;->getCurrentLocale()Ljava/util/Locale;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 183
    invoke-static {p0}, Lcom/android/internal/app/LocalePicker;->constructAdapter(Landroid/content/Context;)Landroid/widget/ArrayAdapter;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/setupwizard/user/WelcomeActivity;->mLocaleAdapter:Landroid/widget/ArrayAdapter;

    .line 185
    new-instance v0, Lcom/google/android/setupwizard/accessibility/AccessibilityGestureHelper;

    invoke-direct {v0, p0}, Lcom/google/android/setupwizard/accessibility/AccessibilityGestureHelper;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/google/android/setupwizard/user/WelcomeActivity;->mAccessibilityHelper:Lcom/google/android/setupwizard/accessibility/AccessibilityGestureHelper;

    .line 186
    invoke-virtual {p0}, Lcom/google/android/setupwizard/user/WelcomeActivity;->isPrimaryUser()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 187
    iget-object v0, p0, Lcom/google/android/setupwizard/user/WelcomeActivity;->mAccessibilityHelper:Lcom/google/android/setupwizard/accessibility/AccessibilityGestureHelper;

    new-instance v1, Lcom/google/android/setupwizard/user/WelcomeActivity$4;

    invoke-direct {v1, p0}, Lcom/google/android/setupwizard/user/WelcomeActivity$4;-><init>(Lcom/google/android/setupwizard/user/WelcomeActivity;)V

    invoke-virtual {v0, v1}, Lcom/google/android/setupwizard/accessibility/AccessibilityGestureHelper;->setCallback(Lcom/google/android/setupwizard/accessibility/AccessibilityGestureHelper$AccessibilityCallback;)V

    .line 195
    :cond_0
    invoke-direct {p0}, Lcom/google/android/setupwizard/user/WelcomeActivity;->initViews()V

    .line 199
    new-instance v0, Landroid/content/IntentFilter;

    const-string v1, "android.intent.action.LOCALE_CHANGED"

    invoke-direct {v0, v1}, Landroid/content/IntentFilter;-><init>(Ljava/lang/String;)V

    iput-object v0, p0, Lcom/google/android/setupwizard/user/WelcomeActivity;->mLocaleFilter:Landroid/content/IntentFilter;

    .line 200
    new-instance v0, Lcom/google/android/setupwizard/user/WelcomeActivity$5;

    invoke-direct {v0, p0}, Lcom/google/android/setupwizard/user/WelcomeActivity$5;-><init>(Lcom/google/android/setupwizard/user/WelcomeActivity;)V

    iput-object v0, p0, Lcom/google/android/setupwizard/user/WelcomeActivity;->mLocaleReceiver:Landroid/content/BroadcastReceiver;

    .line 208
    iget-object v0, p0, Lcom/google/android/setupwizard/user/WelcomeActivity;->mLocaleReceiver:Landroid/content/BroadcastReceiver;

    iget-object v1, p0, Lcom/google/android/setupwizard/user/WelcomeActivity;->mLocaleFilter:Landroid/content/IntentFilter;

    invoke-virtual {p0, v0, v1}, Lcom/google/android/setupwizard/user/WelcomeActivity;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    .line 210
    invoke-virtual {p0}, Lcom/google/android/setupwizard/user/WelcomeActivity;->isMobileNetworkSupported()Z

    move-result v0

    if-nez v0, :cond_1

    .line 211
    iget-object v0, p0, Lcom/google/android/setupwizard/user/WelcomeActivity;->mHandler:Landroid/os/Handler;

    iget-object v1, p0, Lcom/google/android/setupwizard/user/WelcomeActivity;->mEnableWifi:Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 214
    :cond_1
    if-nez p1, :cond_3

    invoke-virtual {p0}, Lcom/google/android/setupwizard/user/WelcomeActivity;->isResumeFromUpdate()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 215
    invoke-direct {p0}, Lcom/google/android/setupwizard/user/WelcomeActivity;->finishAndReturnResult()V

    .line 223
    :cond_2
    :goto_0
    return-void

    .line 216
    :cond_3
    sget-boolean v0, Lcom/google/android/setupwizard/user/WelcomeActivity;->IMMEDIATE_WARM_WELCOME:Z

    if-eqz v0, :cond_2

    .line 217
    if-nez p1, :cond_4

    .line 219
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/google/android/setupwizard/user/WelcomeActivity;->setWarmerWelcomeWaitingMode(Z)V

    .line 221
    :cond_4
    invoke-direct {p0}, Lcom/google/android/setupwizard/user/WelcomeActivity;->checkConnectionAndWarmWelcomeData()V

    goto :goto_0
.end method

.method public onDestroy()V
    .locals 1

    .prologue
    .line 288
    invoke-super {p0}, Lcom/google/android/setupwizard/BaseActivity;->onDestroy()V

    .line 290
    iget-object v0, p0, Lcom/google/android/setupwizard/user/WelcomeActivity;->mAccessibilityHelper:Lcom/google/android/setupwizard/accessibility/AccessibilityGestureHelper;

    invoke-virtual {v0}, Lcom/google/android/setupwizard/accessibility/AccessibilityGestureHelper;->onDestroy()V

    .line 291
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/setupwizard/user/WelcomeActivity;->mAccessibilityHelper:Lcom/google/android/setupwizard/accessibility/AccessibilityGestureHelper;

    .line 292
    return-void
.end method

.method public onGservicesReady()V
    .locals 0

    .prologue
    .line 152
    invoke-direct {p0}, Lcom/google/android/setupwizard/user/WelcomeActivity;->showWarmWelcome()V

    .line 153
    return-void
.end method

.method public onGservicesTimeout()V
    .locals 1

    .prologue
    .line 158
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/google/android/setupwizard/user/WelcomeActivity;->setWarmerWelcomeWaitingMode(Z)V

    .line 159
    return-void
.end method

.method protected onPause()V
    .locals 3

    .prologue
    .line 242
    iget-object v0, p0, Lcom/google/android/setupwizard/user/WelcomeActivity;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "onPause() current locale="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-direct {p0}, Lcom/google/android/setupwizard/user/WelcomeActivity;->getCurrentLocale()Ljava/util/Locale;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 243
    iget-object v0, p0, Lcom/google/android/setupwizard/user/WelcomeActivity;->mHandler:Landroid/os/Handler;

    iget-object v1, p0, Lcom/google/android/setupwizard/user/WelcomeActivity;->mUpdateLocale:Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    .line 244
    iget-object v0, p0, Lcom/google/android/setupwizard/user/WelcomeActivity;->mConnectionCheckedReceiver:Landroid/content/BroadcastReceiver;

    invoke-static {p0, v0}, Lcom/google/android/setupwizard/network/CheckConnectionTask;->unregister(Landroid/content/Context;Landroid/content/BroadcastReceiver;)V

    .line 245
    iget-object v0, p0, Lcom/google/android/setupwizard/user/WelcomeActivity;->mLocaleReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {p0, v0}, Lcom/google/android/setupwizard/user/WelcomeActivity;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    .line 246
    invoke-virtual {p0}, Lcom/google/android/setupwizard/user/WelcomeActivity;->isPrimaryUser()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/setupwizard/user/WelcomeActivity;->mPickerLocale:Ljava/util/Locale;

    if-eqz v0, :cond_0

    .line 250
    iget-object v0, p0, Lcom/google/android/setupwizard/user/WelcomeActivity;->TAG:Ljava/lang/String;

    const-string v1, "onPause update using mPickerLocale"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 251
    iget-object v0, p0, Lcom/google/android/setupwizard/user/WelcomeActivity;->mPickerLocale:Ljava/util/Locale;

    invoke-static {v0}, Lcom/android/internal/app/LocalePicker;->updateLocale(Ljava/util/Locale;)V

    .line 254
    :cond_0
    invoke-super {p0}, Lcom/google/android/setupwizard/BaseActivity;->onPause()V

    .line 255
    return-void
.end method

.method protected onRestoreInstanceState(Landroid/os/Bundle;)V
    .locals 2
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 280
    invoke-super {p0, p1}, Lcom/google/android/setupwizard/BaseActivity;->onRestoreInstanceState(Landroid/os/Bundle;)V

    .line 282
    const-string v1, "warmer_welcome_waiting"

    invoke-virtual {p1, v1}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v0

    .line 283
    .local v0, "warmerWelcomeWaiting":Z
    invoke-direct {p0, v0}, Lcom/google/android/setupwizard/user/WelcomeActivity;->setWarmerWelcomeWaitingMode(Z)V

    .line 284
    return-void
.end method

.method protected onResume()V
    .locals 5

    .prologue
    .line 259
    invoke-direct {p0}, Lcom/google/android/setupwizard/user/WelcomeActivity;->getCurrentLocale()Ljava/util/Locale;

    move-result-object v1

    .line 260
    .local v1, "locale":Ljava/util/Locale;
    iget-object v2, p0, Lcom/google/android/setupwizard/user/WelcomeActivity;->TAG:Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "onResume() current locale="

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 261
    invoke-super {p0}, Lcom/google/android/setupwizard/BaseActivity;->onResume()V

    .line 263
    invoke-static {v1}, Landroid/text/TextUtils;->getLayoutDirectionFromLocale(Ljava/util/Locale;)I

    move-result v0

    .line 264
    .local v0, "direction":I
    iget-object v2, p0, Lcom/google/android/setupwizard/user/WelcomeActivity;->mStartButton:Landroid/widget/ImageButton;

    invoke-virtual {v2, v0}, Landroid/widget/ImageButton;->setLayoutDirection(I)V

    .line 265
    iget-object v2, p0, Lcom/google/android/setupwizard/user/WelcomeActivity;->mEmergencyCallButton:Landroid/widget/Button;

    invoke-virtual {v2, v0}, Landroid/widget/Button;->setLayoutDirection(I)V

    .line 266
    invoke-static {v1}, Lcom/android/internal/app/LocalePicker;->updateLocale(Ljava/util/Locale;)V

    .line 268
    iget-object v2, p0, Lcom/google/android/setupwizard/user/WelcomeActivity;->mConnectionCheckedReceiver:Landroid/content/BroadcastReceiver;

    invoke-static {p0, v2}, Lcom/google/android/setupwizard/network/CheckConnectionTask;->register(Landroid/content/Context;Landroid/content/BroadcastReceiver;)V

    .line 269
    iget-object v2, p0, Lcom/google/android/setupwizard/user/WelcomeActivity;->mLocaleReceiver:Landroid/content/BroadcastReceiver;

    iget-object v3, p0, Lcom/google/android/setupwizard/user/WelcomeActivity;->mLocaleFilter:Landroid/content/IntentFilter;

    invoke-virtual {p0, v2, v3}, Lcom/google/android/setupwizard/user/WelcomeActivity;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    .line 270
    return-void
.end method

.method protected onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 2
    .param p1, "outState"    # Landroid/os/Bundle;

    .prologue
    .line 274
    invoke-super {p0, p1}, Lcom/google/android/setupwizard/BaseActivity;->onSaveInstanceState(Landroid/os/Bundle;)V

    .line 275
    const-string v0, "warmer_welcome_waiting"

    iget-boolean v1, p0, Lcom/google/android/setupwizard/user/WelcomeActivity;->mWarmerWelcomeWaiting:Z

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 276
    return-void
.end method
