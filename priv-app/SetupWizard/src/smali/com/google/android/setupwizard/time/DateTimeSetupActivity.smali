.class public Lcom/google/android/setupwizard/time/DateTimeSetupActivity;
.super Lcom/google/android/setupwizard/BaseActivity;
.source "DateTimeSetupActivity.java"

# interfaces
.implements Landroid/view/View$OnClickListener;
.implements Landroid/widget/AdapterView$OnItemSelectedListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/setupwizard/time/DateTimeSetupActivity$DateDialog;,
        Lcom/google/android/setupwizard/time/DateTimeSetupActivity$TimeDialog;
    }
.end annotation


# static fields
.field private static final CLOCK_TICK_FILTER:Landroid/content/IntentFilter;


# instance fields
.field private final clockTickReceiver:Landroid/content/BroadcastReceiver;

.field private mDateTextView:Landroid/widget/TextView;

.field private mSelectedTimeZone:Ljava/util/TimeZone;

.field private mTimeTextView:Landroid/widget/TextView;

.field private mTimeZoneAdapter:Landroid/widget/SimpleAdapter;

.field private mTimeZonePicker:Landroid/widget/Spinner;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 54
    new-instance v0, Landroid/content/IntentFilter;

    const-string v1, "android.intent.action.TIME_TICK"

    invoke-direct {v0, v1}, Landroid/content/IntentFilter;-><init>(Ljava/lang/String;)V

    sput-object v0, Lcom/google/android/setupwizard/time/DateTimeSetupActivity;->CLOCK_TICK_FILTER:Landroid/content/IntentFilter;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 50
    invoke-direct {p0}, Lcom/google/android/setupwizard/BaseActivity;-><init>()V

    .line 63
    new-instance v0, Lcom/google/android/setupwizard/time/DateTimeSetupActivity$1;

    invoke-direct {v0, p0}, Lcom/google/android/setupwizard/time/DateTimeSetupActivity$1;-><init>(Lcom/google/android/setupwizard/time/DateTimeSetupActivity;)V

    iput-object v0, p0, Lcom/google/android/setupwizard/time/DateTimeSetupActivity;->clockTickReceiver:Landroid/content/BroadcastReceiver;

    .line 262
    return-void
.end method

.method static synthetic access$000(Lcom/google/android/setupwizard/time/DateTimeSetupActivity;)V
    .locals 0
    .param p0, "x0"    # Lcom/google/android/setupwizard/time/DateTimeSetupActivity;

    .prologue
    .line 50
    invoke-direct {p0}, Lcom/google/android/setupwizard/time/DateTimeSetupActivity;->updateDateDisplay()V

    return-void
.end method

.method static synthetic access$100(Lcom/google/android/setupwizard/time/DateTimeSetupActivity;)V
    .locals 0
    .param p0, "x0"    # Lcom/google/android/setupwizard/time/DateTimeSetupActivity;

    .prologue
    .line 50
    invoke-direct {p0}, Lcom/google/android/setupwizard/time/DateTimeSetupActivity;->updateTimeDisplay()V

    return-void
.end method

.method static synthetic access$200(Lcom/google/android/setupwizard/time/DateTimeSetupActivity;II)V
    .locals 0
    .param p0, "x0"    # Lcom/google/android/setupwizard/time/DateTimeSetupActivity;
    .param p1, "x1"    # I
    .param p2, "x2"    # I

    .prologue
    .line 50
    invoke-direct {p0, p1, p2}, Lcom/google/android/setupwizard/time/DateTimeSetupActivity;->setTime(II)V

    return-void
.end method

.method static synthetic access$300(Lcom/google/android/setupwizard/time/DateTimeSetupActivity;III)V
    .locals 0
    .param p0, "x0"    # Lcom/google/android/setupwizard/time/DateTimeSetupActivity;
    .param p1, "x1"    # I
    .param p2, "x2"    # I
    .param p3, "x3"    # I

    .prologue
    .line 50
    invoke-direct {p0, p1, p2, p3}, Lcom/google/android/setupwizard/time/DateTimeSetupActivity;->setDate(III)V

    return-void
.end method

.method private setDate(III)V
    .locals 2
    .param p1, "year"    # I
    .param p2, "month"    # I
    .param p3, "day"    # I

    .prologue
    .line 176
    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object v0

    .line 177
    .local v0, "c":Ljava/util/Calendar;
    const/4 v1, 0x1

    invoke-virtual {v0, v1, p1}, Ljava/util/Calendar;->set(II)V

    .line 178
    const/4 v1, 0x2

    invoke-virtual {v0, v1, p2}, Ljava/util/Calendar;->set(II)V

    .line 179
    const/4 v1, 0x5

    invoke-virtual {v0, v1, p3}, Ljava/util/Calendar;->set(II)V

    .line 180
    invoke-direct {p0, v0}, Lcom/google/android/setupwizard/time/DateTimeSetupActivity;->setDateTime(Ljava/util/Calendar;)V

    .line 181
    return-void
.end method

.method private setDateTime(Ljava/util/Calendar;)V
    .locals 4
    .param p1, "c"    # Ljava/util/Calendar;

    .prologue
    .line 193
    const-string v1, "alarm"

    invoke-virtual {p0, v1}, Lcom/google/android/setupwizard/time/DateTimeSetupActivity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/AlarmManager;

    .line 194
    .local v0, "am":Landroid/app/AlarmManager;
    if-eqz v0, :cond_0

    .line 195
    invoke-virtual {p1}, Ljava/util/Calendar;->getTimeInMillis()J

    move-result-wide v2

    invoke-virtual {v0, v2, v3}, Landroid/app/AlarmManager;->setTime(J)V

    .line 199
    :goto_0
    return-void

    .line 197
    :cond_0
    iget-object v1, p0, Lcom/google/android/setupwizard/time/DateTimeSetupActivity;->TAG:Ljava/lang/String;

    const-string v2, "failed to set the system time"

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method private setTime(II)V
    .locals 3
    .param p1, "hour"    # I
    .param p2, "minute"    # I

    .prologue
    const/4 v2, 0x0

    .line 184
    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object v0

    .line 185
    .local v0, "c":Ljava/util/Calendar;
    const/16 v1, 0xb

    invoke-virtual {v0, v1, p1}, Ljava/util/Calendar;->set(II)V

    .line 186
    const/16 v1, 0xc

    invoke-virtual {v0, v1, p2}, Ljava/util/Calendar;->set(II)V

    .line 187
    const/16 v1, 0xd

    invoke-virtual {v0, v1, v2}, Ljava/util/Calendar;->set(II)V

    .line 188
    const/16 v1, 0xe

    invoke-virtual {v0, v1, v2}, Ljava/util/Calendar;->set(II)V

    .line 189
    invoke-direct {p0, v0}, Lcom/google/android/setupwizard/time/DateTimeSetupActivity;->setDateTime(Ljava/util/Calendar;)V

    .line 190
    return-void
.end method

.method private setTimeZone(Ljava/lang/String;)V
    .locals 4
    .param p1, "tz"    # Ljava/lang/String;

    .prologue
    .line 202
    const-string v1, "alarm"

    invoke-virtual {p0, v1}, Lcom/google/android/setupwizard/time/DateTimeSetupActivity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/AlarmManager;

    .line 203
    .local v0, "am":Landroid/app/AlarmManager;
    if-eqz v0, :cond_0

    iget-object v1, p0, Lcom/google/android/setupwizard/time/DateTimeSetupActivity;->mSelectedTimeZone:Ljava/util/TimeZone;

    if-eqz v1, :cond_0

    .line 204
    invoke-virtual {v0, p1}, Landroid/app/AlarmManager;->setTimeZone(Ljava/lang/String;)V

    .line 208
    :goto_0
    return-void

    .line 206
    :cond_0
    iget-object v1, p0, Lcom/google/android/setupwizard/time/DateTimeSetupActivity;->TAG:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "failed to set the system timezone to "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/google/android/setupwizard/time/DateTimeSetupActivity;->mSelectedTimeZone:Ljava/util/TimeZone;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method private showDateDialog()V
    .locals 3

    .prologue
    .line 225
    new-instance v0, Lcom/google/android/setupwizard/time/DateTimeSetupActivity$DateDialog;

    invoke-direct {v0}, Lcom/google/android/setupwizard/time/DateTimeSetupActivity$DateDialog;-><init>()V

    invoke-virtual {p0}, Lcom/google/android/setupwizard/time/DateTimeSetupActivity;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v1

    const-string v2, "Dialog"

    invoke-virtual {v0, v1, v2}, Lcom/google/android/setupwizard/time/DateTimeSetupActivity$DateDialog;->show(Landroid/app/FragmentManager;Ljava/lang/String;)V

    .line 226
    return-void
.end method

.method private showTimeDialog()V
    .locals 3

    .prologue
    .line 221
    new-instance v0, Lcom/google/android/setupwizard/time/DateTimeSetupActivity$TimeDialog;

    invoke-direct {v0}, Lcom/google/android/setupwizard/time/DateTimeSetupActivity$TimeDialog;-><init>()V

    invoke-virtual {p0}, Lcom/google/android/setupwizard/time/DateTimeSetupActivity;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v1

    const-string v2, "Dialog"

    invoke-virtual {v0, v1, v2}, Lcom/google/android/setupwizard/time/DateTimeSetupActivity$TimeDialog;->show(Landroid/app/FragmentManager;Ljava/lang/String;)V

    .line 222
    return-void
.end method

.method private updateDateDisplay()V
    .locals 6

    .prologue
    .line 211
    iget-object v0, p0, Lcom/google/android/setupwizard/time/DateTimeSetupActivity;->mDateTextView:Landroid/widget/TextView;

    invoke-static {p0}, Landroid/text/format/DateFormat;->getDateFormat(Landroid/content/Context;)Ljava/text/DateFormat;

    move-result-object v1

    new-instance v2, Ljava/util/Date;

    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object v3

    invoke-virtual {v3}, Ljava/util/Calendar;->getTimeInMillis()J

    move-result-wide v4

    invoke-direct {v2, v4, v5}, Ljava/util/Date;-><init>(J)V

    invoke-virtual {v1, v2}, Ljava/text/DateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 213
    return-void
.end method

.method private updateTimeDisplay()V
    .locals 6

    .prologue
    .line 216
    iget-object v0, p0, Lcom/google/android/setupwizard/time/DateTimeSetupActivity;->mTimeTextView:Landroid/widget/TextView;

    invoke-static {p0}, Landroid/text/format/DateFormat;->getTimeFormat(Landroid/content/Context;)Ljava/text/DateFormat;

    move-result-object v1

    new-instance v2, Ljava/util/Date;

    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object v3

    invoke-virtual {v3}, Ljava/util/Calendar;->getTimeInMillis()J

    move-result-wide v4

    invoke-direct {v2, v4, v5}, Ljava/util/Date;-><init>(J)V

    invoke-virtual {v1, v2}, Ljava/text/DateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 218
    return-void
.end method

.method private updateTimeZone()V
    .locals 1

    .prologue
    .line 168
    iget-object v0, p0, Lcom/google/android/setupwizard/time/DateTimeSetupActivity;->mSelectedTimeZone:Ljava/util/TimeZone;

    invoke-virtual {v0}, Ljava/util/TimeZone;->getID()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/google/android/setupwizard/time/DateTimeSetupActivity;->setTimeZone(Ljava/lang/String;)V

    .line 171
    invoke-direct {p0}, Lcom/google/android/setupwizard/time/DateTimeSetupActivity;->updateDateDisplay()V

    .line 172
    invoke-direct {p0}, Lcom/google/android/setupwizard/time/DateTimeSetupActivity;->updateTimeDisplay()V

    .line 173
    return-void
.end method

.method private updateTimeZonePicker()V
    .locals 3

    .prologue
    .line 152
    invoke-static {}, Ljava/util/TimeZone;->getDefault()Ljava/util/TimeZone;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/setupwizard/time/DateTimeSetupActivity;->mSelectedTimeZone:Ljava/util/TimeZone;

    .line 153
    const-string v1, "GMT"

    iget-object v2, p0, Lcom/google/android/setupwizard/time/DateTimeSetupActivity;->mSelectedTimeZone:Ljava/util/TimeZone;

    invoke-virtual {v2}, Ljava/util/TimeZone;->getID()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 156
    iget-object v1, p0, Lcom/google/android/setupwizard/time/DateTimeSetupActivity;->mTimeZoneAdapter:Landroid/widget/SimpleAdapter;

    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/google/android/setupwizard/time/ZonePicker;->getTimeZoneForLocale(Landroid/widget/SimpleAdapter;Ljava/util/Locale;)Ljava/util/TimeZone;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/setupwizard/time/DateTimeSetupActivity;->mSelectedTimeZone:Ljava/util/TimeZone;

    .line 159
    :cond_0
    iget-object v1, p0, Lcom/google/android/setupwizard/time/DateTimeSetupActivity;->mTimeZoneAdapter:Landroid/widget/SimpleAdapter;

    iget-object v2, p0, Lcom/google/android/setupwizard/time/DateTimeSetupActivity;->mSelectedTimeZone:Ljava/util/TimeZone;

    invoke-static {v1, v2}, Lcom/google/android/setupwizard/time/ZonePicker;->getTimeZoneIndex(Landroid/widget/SimpleAdapter;Ljava/util/TimeZone;)I

    move-result v0

    .line 160
    .local v0, "index":I
    const/4 v1, -0x1

    if-eq v0, v1, :cond_1

    .line 161
    iget-object v1, p0, Lcom/google/android/setupwizard/time/DateTimeSetupActivity;->mTimeZonePicker:Landroid/widget/Spinner;

    invoke-virtual {v1, v0}, Landroid/widget/Spinner;->setSelection(I)V

    .line 162
    invoke-direct {p0}, Lcom/google/android/setupwizard/time/DateTimeSetupActivity;->updateTimeZone()V

    .line 164
    :cond_1
    return-void
.end method


# virtual methods
.method public initView()V
    .locals 4

    .prologue
    .line 106
    const v2, 0x7f030005

    const v3, 0x7f030023

    invoke-virtual {p0, v2, v3}, Lcom/google/android/setupwizard/time/DateTimeSetupActivity;->setTemplateContent(II)V

    .line 107
    const v2, 0x7f070079

    invoke-virtual {p0, v2}, Lcom/google/android/setupwizard/time/DateTimeSetupActivity;->setHeaderText(I)V

    .line 108
    const v2, 0x7f020050

    const v3, 0x7f020051

    invoke-virtual {p0, v2, v3}, Lcom/google/android/setupwizard/time/DateTimeSetupActivity;->setIllustration(II)V

    .line 110
    const v2, 0x7f0e0040

    invoke-virtual {p0, v2}, Lcom/google/android/setupwizard/time/DateTimeSetupActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    .line 112
    .local v0, "dateTextViewContainer":Landroid/widget/LinearLayout;
    invoke-virtual {v0, p0}, Landroid/widget/LinearLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 113
    const v2, 0x7f0e0042

    invoke-virtual {p0, v2}, Lcom/google/android/setupwizard/time/DateTimeSetupActivity;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    iput-object v2, p0, Lcom/google/android/setupwizard/time/DateTimeSetupActivity;->mDateTextView:Landroid/widget/TextView;

    .line 114
    const v2, 0x7f0e0043

    invoke-virtual {p0, v2}, Lcom/google/android/setupwizard/time/DateTimeSetupActivity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/LinearLayout;

    .line 116
    .local v1, "timeTextViewContainer":Landroid/widget/LinearLayout;
    invoke-virtual {v1, p0}, Landroid/widget/LinearLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 117
    const v2, 0x7f0e0045

    invoke-virtual {p0, v2}, Lcom/google/android/setupwizard/time/DateTimeSetupActivity;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    iput-object v2, p0, Lcom/google/android/setupwizard/time/DateTimeSetupActivity;->mTimeTextView:Landroid/widget/TextView;

    .line 118
    const v2, 0x7f0e003f

    invoke-virtual {p0, v2}, Lcom/google/android/setupwizard/time/DateTimeSetupActivity;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/Spinner;

    iput-object v2, p0, Lcom/google/android/setupwizard/time/DateTimeSetupActivity;->mTimeZonePicker:Landroid/widget/Spinner;

    .line 119
    return-void
.end method

.method public onClick(Landroid/view/View;)V
    .locals 1
    .param p1, "view"    # Landroid/view/View;

    .prologue
    .line 123
    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    .line 131
    :goto_0
    :pswitch_0
    return-void

    .line 125
    :pswitch_1
    invoke-direct {p0}, Lcom/google/android/setupwizard/time/DateTimeSetupActivity;->showDateDialog()V

    goto :goto_0

    .line 128
    :pswitch_2
    invoke-direct {p0}, Lcom/google/android/setupwizard/time/DateTimeSetupActivity;->showTimeDialog()V

    goto :goto_0

    .line 123
    :pswitch_data_0
    .packed-switch 0x7f0e0040
        :pswitch_1
        :pswitch_0
        :pswitch_0
        :pswitch_2
    .end packed-switch
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 2
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 73
    invoke-super {p0, p1}, Lcom/google/android/setupwizard/BaseActivity;->onCreate(Landroid/os/Bundle;)V

    .line 75
    if-nez p1, :cond_0

    invoke-virtual {p0}, Lcom/google/android/setupwizard/time/DateTimeSetupActivity;->isTimeSet()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/google/android/setupwizard/time/DateTimeSetupActivity;->isTimeZoneSet()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 76
    const/4 v0, -0x1

    invoke-virtual {p0, v0}, Lcom/google/android/setupwizard/time/DateTimeSetupActivity;->finishAction(I)V

    .line 79
    :cond_0
    invoke-virtual {p0}, Lcom/google/android/setupwizard/time/DateTimeSetupActivity;->initView()V

    .line 81
    const/4 v0, 0x0

    const v1, 0x7f03001d

    invoke-static {p0, v0, v1}, Lcom/google/android/setupwizard/time/ZonePicker;->constructTimezoneAdapter(Landroid/content/Context;ZI)Landroid/widget/SimpleAdapter;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/setupwizard/time/DateTimeSetupActivity;->mTimeZoneAdapter:Landroid/widget/SimpleAdapter;

    .line 83
    iget-object v0, p0, Lcom/google/android/setupwizard/time/DateTimeSetupActivity;->mTimeZonePicker:Landroid/widget/Spinner;

    iget-object v1, p0, Lcom/google/android/setupwizard/time/DateTimeSetupActivity;->mTimeZoneAdapter:Landroid/widget/SimpleAdapter;

    invoke-virtual {v0, v1}, Landroid/widget/Spinner;->setAdapter(Landroid/widget/SpinnerAdapter;)V

    .line 84
    return-void
.end method

.method public onItemSelected(Landroid/widget/AdapterView;Landroid/view/View;IJ)V
    .locals 1
    .param p2, "view"    # Landroid/view/View;
    .param p3, "position"    # I
    .param p4, "id"    # J
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/widget/AdapterView",
            "<*>;",
            "Landroid/view/View;",
            "IJ)V"
        }
    .end annotation

    .prologue
    .line 140
    .local p1, "adapterView":Landroid/widget/AdapterView;, "Landroid/widget/AdapterView<*>;"
    invoke-virtual {p1, p3}, Landroid/widget/AdapterView;->getItemAtPosition(I)Ljava/lang/Object;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/setupwizard/time/ZonePicker;->obtainTimeZoneFromItem(Ljava/lang/Object;)Ljava/util/TimeZone;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/setupwizard/time/DateTimeSetupActivity;->mSelectedTimeZone:Ljava/util/TimeZone;

    .line 142
    invoke-direct {p0}, Lcom/google/android/setupwizard/time/DateTimeSetupActivity;->updateTimeZone()V

    .line 143
    return-void
.end method

.method public onNothingSelected(Landroid/widget/AdapterView;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/widget/AdapterView",
            "<*>;)V"
        }
    .end annotation

    .prologue
    .line 148
    .local p1, "adapterView":Landroid/widget/AdapterView;, "Landroid/widget/AdapterView<*>;"
    return-void
.end method

.method protected onPause()V
    .locals 2

    .prologue
    .line 100
    iget-object v0, p0, Lcom/google/android/setupwizard/time/DateTimeSetupActivity;->mTimeZonePicker:Landroid/widget/Spinner;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/Spinner;->setOnItemSelectedListener(Landroid/widget/AdapterView$OnItemSelectedListener;)V

    .line 101
    iget-object v0, p0, Lcom/google/android/setupwizard/time/DateTimeSetupActivity;->clockTickReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {p0, v0}, Lcom/google/android/setupwizard/time/DateTimeSetupActivity;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    .line 102
    invoke-super {p0}, Lcom/google/android/setupwizard/BaseActivity;->onPause()V

    .line 103
    return-void
.end method

.method protected onResume()V
    .locals 2

    .prologue
    .line 88
    invoke-super {p0}, Lcom/google/android/setupwizard/BaseActivity;->onResume()V

    .line 89
    iget-object v0, p0, Lcom/google/android/setupwizard/time/DateTimeSetupActivity;->clockTickReceiver:Landroid/content/BroadcastReceiver;

    sget-object v1, Lcom/google/android/setupwizard/time/DateTimeSetupActivity;->CLOCK_TICK_FILTER:Landroid/content/IntentFilter;

    invoke-virtual {p0, v0, v1}, Lcom/google/android/setupwizard/time/DateTimeSetupActivity;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    .line 90
    invoke-direct {p0}, Lcom/google/android/setupwizard/time/DateTimeSetupActivity;->updateDateDisplay()V

    .line 91
    invoke-direct {p0}, Lcom/google/android/setupwizard/time/DateTimeSetupActivity;->updateTimeDisplay()V

    .line 93
    invoke-direct {p0}, Lcom/google/android/setupwizard/time/DateTimeSetupActivity;->updateTimeZonePicker()V

    .line 95
    iget-object v0, p0, Lcom/google/android/setupwizard/time/DateTimeSetupActivity;->mTimeZonePicker:Landroid/widget/Spinner;

    invoke-virtual {v0, p0}, Landroid/widget/Spinner;->setOnItemSelectedListener(Landroid/widget/AdapterView$OnItemSelectedListener;)V

    .line 96
    return-void
.end method

.method protected start()V
    .locals 0

    .prologue
    .line 135
    invoke-virtual {p0}, Lcom/google/android/setupwizard/time/DateTimeSetupActivity;->nextScreen()V

    .line 136
    return-void
.end method
