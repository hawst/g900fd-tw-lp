.class public Lcom/google/android/gms/internal/vd;
.super Ljava/lang/Object;

# interfaces
.implements Lcom/google/android/gms/usagereporting/UsageReportingApi;


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public setOptInOptions(Lcom/google/android/gms/common/api/GoogleApiClient;Lcom/google/android/gms/usagereporting/UsageReportingOptInOptions;)Lcom/google/android/gms/common/api/PendingResult;
    .locals 1
    .param p1, "client"    # Lcom/google/android/gms/common/api/GoogleApiClient;
    .param p2, "optInOptions"    # Lcom/google/android/gms/usagereporting/UsageReportingOptInOptions;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/gms/common/api/GoogleApiClient;",
            "Lcom/google/android/gms/usagereporting/UsageReportingOptInOptions;",
            ")",
            "Lcom/google/android/gms/common/api/PendingResult",
            "<",
            "Lcom/google/android/gms/common/api/Status;",
            ">;"
        }
    .end annotation

    .prologue
    new-instance v0, Lcom/google/android/gms/internal/vd$2;

    invoke-direct {v0, p0, p2}, Lcom/google/android/gms/internal/vd$2;-><init>(Lcom/google/android/gms/internal/vd;Lcom/google/android/gms/usagereporting/UsageReportingOptInOptions;)V

    invoke-interface {p1, v0}, Lcom/google/android/gms/common/api/GoogleApiClient;->b(Lcom/google/android/gms/common/api/BaseImplementation$a;)Lcom/google/android/gms/common/api/BaseImplementation$a;

    move-result-object v0

    return-object v0
.end method
