.class public Lcom/google/android/gms/internal/ve;
.super Lcom/google/android/gms/common/internal/e;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/gms/internal/ve$a;,
        Lcom/google/android/gms/internal/ve$b;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/android/gms/common/internal/e",
        "<",
        "Lcom/google/android/gms/internal/vc;",
        ">;"
    }
.end annotation


# instance fields
.field private aKl:Lcom/google/android/gms/internal/ve$b;


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/os/Looper;Lcom/google/android/gms/common/api/GoogleApiClient$ConnectionCallbacks;Lcom/google/android/gms/common/api/GoogleApiClient$OnConnectionFailedListener;)V
    .locals 7

    const/4 v6, 0x0

    move-object v5, v6

    check-cast v5, [Ljava/lang/String;

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    invoke-direct/range {v0 .. v5}, Lcom/google/android/gms/common/internal/e;-><init>(Landroid/content/Context;Landroid/os/Looper;Lcom/google/android/gms/common/api/GoogleApiClient$ConnectionCallbacks;Lcom/google/android/gms/common/api/GoogleApiClient$OnConnectionFailedListener;[Ljava/lang/String;)V

    iput-object v6, p0, Lcom/google/android/gms/internal/ve;->aKl:Lcom/google/android/gms/internal/ve$b;

    return-void
.end method


# virtual methods
.method protected a(Lcom/google/android/gms/common/internal/l;Lcom/google/android/gms/common/internal/e$e;)V
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    const v1, 0x5e3530

    invoke-virtual {p0}, Lcom/google/android/gms/internal/ve;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v2

    invoke-interface {p1, p2, v1, v2, v0}, Lcom/google/android/gms/common/internal/l;->t(Lcom/google/android/gms/common/internal/k;ILjava/lang/String;Landroid/os/Bundle;)V

    return-void
.end method

.method public a(Lcom/google/android/gms/usagereporting/UsageReportingOptInOptions;Lcom/google/android/gms/usagereporting/UsageReporting$a;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/gms/usagereporting/UsageReportingOptInOptions;",
            "Lcom/google/android/gms/usagereporting/UsageReporting$a",
            "<",
            "Lcom/google/android/gms/common/api/Status;",
            ">;)V"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    invoke-virtual {p0}, Lcom/google/android/gms/internal/ve;->gR()Landroid/os/IInterface;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/internal/vc;

    new-instance v1, Lcom/google/android/gms/internal/ve$2;

    invoke-direct {v1, p0, p2}, Lcom/google/android/gms/internal/ve$2;-><init>(Lcom/google/android/gms/internal/ve;Lcom/google/android/gms/usagereporting/UsageReporting$a;)V

    invoke-interface {v0, p1, v1}, Lcom/google/android/gms/internal/vc;->a(Lcom/google/android/gms/usagereporting/UsageReportingOptInOptions;Lcom/google/android/gms/internal/va;)V

    return-void
.end method

.method public disconnect()V
    .locals 3

    new-instance v1, Lcom/google/android/gms/internal/ve$4;

    invoke-direct {v1, p0}, Lcom/google/android/gms/internal/ve$4;-><init>(Lcom/google/android/gms/internal/ve;)V

    :try_start_0
    iget-object v0, p0, Lcom/google/android/gms/internal/ve;->aKl:Lcom/google/android/gms/internal/ve$b;

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/google/android/gms/internal/ve;->gR()Landroid/os/IInterface;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/internal/vc;

    iget-object v2, p0, Lcom/google/android/gms/internal/ve;->aKl:Lcom/google/android/gms/internal/ve$b;

    invoke-interface {v0, v2, v1}, Lcom/google/android/gms/internal/vc;->b(Lcom/google/android/gms/internal/vb;Lcom/google/android/gms/internal/va;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    :cond_0
    :goto_0
    invoke-super {p0}, Lcom/google/android/gms/common/internal/e;->disconnect()V

    return-void

    :catch_0
    move-exception v0

    const-string v1, "UsageReportingClientImpl"

    const-string v2, "disconnect(): Could not unregister listener from remote:"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_0
.end method

.method protected dj(Landroid/os/IBinder;)Lcom/google/android/gms/internal/vc;
    .locals 1

    invoke-static {p1}, Lcom/google/android/gms/internal/vc$a;->di(Landroid/os/IBinder;)Lcom/google/android/gms/internal/vc;

    move-result-object v0

    return-object v0
.end method

.method protected getServiceDescriptor()Ljava/lang/String;
    .locals 1

    const-string v0, "com.google.android.gms.usagereporting.internal.IUsageReportingService"

    return-object v0
.end method

.method protected getStartServiceAction()Ljava/lang/String;
    .locals 1

    const-string v0, "com.google.android.gms.usagereporting.service.START"

    return-object v0
.end method

.method protected synthetic n(Landroid/os/IBinder;)Landroid/os/IInterface;
    .locals 1

    invoke-virtual {p0, p1}, Lcom/google/android/gms/internal/ve;->dj(Landroid/os/IBinder;)Lcom/google/android/gms/internal/vc;

    move-result-object v0

    return-object v0
.end method
