.class Lcom/google/android/gms/internal/vd$2;
.super Lcom/google/android/gms/usagereporting/UsageReporting$a;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/gms/internal/vd;->setOptInOptions(Lcom/google/android/gms/common/api/GoogleApiClient;Lcom/google/android/gms/usagereporting/UsageReportingOptInOptions;)Lcom/google/android/gms/common/api/PendingResult;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/android/gms/usagereporting/UsageReporting$a",
        "<",
        "Lcom/google/android/gms/common/api/Status;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic aKg:Lcom/google/android/gms/internal/vd;

.field final synthetic aKh:Lcom/google/android/gms/usagereporting/UsageReportingOptInOptions;


# direct methods
.method constructor <init>(Lcom/google/android/gms/internal/vd;Lcom/google/android/gms/usagereporting/UsageReportingOptInOptions;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/gms/internal/vd$2;->aKg:Lcom/google/android/gms/internal/vd;

    iput-object p2, p0, Lcom/google/android/gms/internal/vd$2;->aKh:Lcom/google/android/gms/usagereporting/UsageReportingOptInOptions;

    invoke-direct {p0}, Lcom/google/android/gms/usagereporting/UsageReporting$a;-><init>()V

    return-void
.end method


# virtual methods
.method protected bridge synthetic a(Lcom/google/android/gms/common/api/Api$a;)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    check-cast p1, Lcom/google/android/gms/internal/ve;

    invoke-virtual {p0, p1}, Lcom/google/android/gms/internal/vd$2;->a(Lcom/google/android/gms/internal/ve;)V

    return-void
.end method

.method protected a(Lcom/google/android/gms/internal/ve;)V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    iget-object v0, p0, Lcom/google/android/gms/internal/vd$2;->aKh:Lcom/google/android/gms/usagereporting/UsageReportingOptInOptions;

    invoke-virtual {p1, v0, p0}, Lcom/google/android/gms/internal/ve;->a(Lcom/google/android/gms/usagereporting/UsageReportingOptInOptions;Lcom/google/android/gms/usagereporting/UsageReporting$a;)V

    return-void
.end method

.method public synthetic b(Lcom/google/android/gms/common/api/Status;)Lcom/google/android/gms/common/api/Result;
    .locals 1

    invoke-virtual {p0, p1}, Lcom/google/android/gms/internal/vd$2;->f(Lcom/google/android/gms/common/api/Status;)Lcom/google/android/gms/common/api/Status;

    move-result-object v0

    return-object v0
.end method

.method public f(Lcom/google/android/gms/common/api/Status;)Lcom/google/android/gms/common/api/Status;
    .locals 0

    return-object p1
.end method
