.class public abstract Lcom/android/providers/contacts/NameLookupBuilder;
.super Ljava/lang/Object;
.source "NameLookupBuilder.java"


# static fields
.field protected static final KOREAN_JAUM_CONVERT_MAP:[I


# instance fields
.field private mNames:[Ljava/lang/String;

.field private mNicknameClusters:[[Ljava/lang/String;

.field private mOriginNames:[Ljava/lang/String;

.field private final mSplitter:Lcom/android/providers/contacts/NameSplitter;

.field private mStringBuilder:Ljava/lang/StringBuilder;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 49
    const/16 v0, 0x1e

    new-array v0, v0, [I

    fill-array-data v0, :array_0

    sput-object v0, Lcom/android/providers/contacts/NameLookupBuilder;->KOREAN_JAUM_CONVERT_MAP:[I

    return-void

    :array_0
    .array-data 4
        0x3131
        0x3132
        0x0
        0x3134
        0x0
        0x0
        0x3137
        0x3138
        0x3139
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x3141
        0x3142
        0x3143
        0x0
        0x3145
        0x3146
        0x3147
        0x3148
        0x3149
        0x314a
        0x314b
        0x314c
        0x314d
        0x314e
    .end array-data
.end method

.method public constructor <init>(Lcom/android/providers/contacts/NameSplitter;)V
    .locals 2
    .param p1, "splitter"    # Lcom/android/providers/contacts/NameSplitter;

    .prologue
    const/16 v1, 0xa

    .line 84
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 44
    const/4 v0, 0x4

    new-array v0, v0, [[Ljava/lang/String;

    iput-object v0, p0, Lcom/android/providers/contacts/NameLookupBuilder;->mNicknameClusters:[[Ljava/lang/String;

    .line 45
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    iput-object v0, p0, Lcom/android/providers/contacts/NameLookupBuilder;->mStringBuilder:Ljava/lang/StringBuilder;

    .line 46
    new-array v0, v1, [Ljava/lang/String;

    iput-object v0, p0, Lcom/android/providers/contacts/NameLookupBuilder;->mNames:[Ljava/lang/String;

    .line 47
    new-array v0, v1, [Ljava/lang/String;

    iput-object v0, p0, Lcom/android/providers/contacts/NameLookupBuilder;->mOriginNames:[Ljava/lang/String;

    .line 85
    iput-object p1, p0, Lcom/android/providers/contacts/NameLookupBuilder;->mSplitter:Lcom/android/providers/contacts/NameSplitter;

    .line 86
    return-void
.end method

.method private appendKoreanNameConsonantsLookup(Lcom/android/providers/contacts/SearchIndexManager$IndexBuilder;Ljava/lang/String;)Ljava/lang/String;
    .locals 9
    .param p1, "builder"    # Lcom/android/providers/contacts/SearchIndexManager$IndexBuilder;
    .param p2, "name"    # Ljava/lang/String;

    .prologue
    const/16 v8, 0x3131

    const v7, 0xac00

    .line 221
    const/4 v2, 0x0

    .line 222
    .local v2, "position":I
    const/4 v1, 0x0

    .line 225
    .local v1, "consonantLength":I
    invoke-virtual {p2}, Ljava/lang/String;->length()I

    move-result v4

    .line 226
    .local v4, "stringLength":I
    iget-object v5, p0, Lcom/android/providers/contacts/NameLookupBuilder;->mStringBuilder:Ljava/lang/StringBuilder;

    const/4 v6, 0x0

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->setLength(I)V

    .line 228
    :goto_0
    add-int/lit8 v3, v2, 0x1

    .end local v2    # "position":I
    .local v3, "position":I
    invoke-virtual {p2, v2}, Ljava/lang/String;->codePointAt(I)I

    move-result v0

    .line 229
    .local v0, "character":I
    const/16 v5, 0x20

    if-eq v0, v5, :cond_0

    const/16 v5, 0x2c

    if-eq v0, v5, :cond_0

    const/16 v5, 0x2e

    if-ne v0, v5, :cond_2

    .line 262
    :cond_0
    :goto_1
    if-lt v3, v4, :cond_7

    .line 269
    :cond_1
    :goto_2
    iget-object v5, p0, Lcom/android/providers/contacts/NameLookupBuilder;->mStringBuilder:Ljava/lang/StringBuilder;

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {p1, v5}, Lcom/android/providers/contacts/SearchIndexManager$IndexBuilder;->appendName(Ljava/lang/String;)V

    .line 272
    iget-object v5, p0, Lcom/android/providers/contacts/NameLookupBuilder;->mStringBuilder:Ljava/lang/StringBuilder;

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    return-object v5

    .line 235
    :cond_2
    const/16 v5, 0x1100

    if-lt v0, v5, :cond_0

    const/16 v5, 0x1112

    if-le v0, v5, :cond_3

    if-lt v0, v8, :cond_0

    :cond_3
    const/16 v5, 0x314e

    if-le v0, v5, :cond_4

    if-lt v0, v7, :cond_0

    :cond_4
    const v5, 0xd7a3

    if-gt v0, v5, :cond_0

    .line 242
    if-lt v0, v7, :cond_6

    .line 246
    sub-int v5, v0, v7

    div-int/lit16 v5, v5, 0x24c

    add-int/lit16 v0, v5, 0x1100

    .line 260
    :cond_5
    iget-object v5, p0, Lcom/android/providers/contacts/NameLookupBuilder;->mStringBuilder:Ljava/lang/StringBuilder;

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->appendCodePoint(I)Ljava/lang/StringBuilder;

    .line 261
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 247
    :cond_6
    if-lt v0, v8, :cond_5

    .line 250
    add-int/lit16 v5, v0, -0x3131

    sget-object v6, Lcom/android/providers/contacts/NameLookupBuilder;->KOREAN_JAUM_CONVERT_MAP:[I

    array-length v6, v6

    if-ge v5, v6, :cond_1

    .line 254
    sget-object v5, Lcom/android/providers/contacts/NameLookupBuilder;->KOREAN_JAUM_CONVERT_MAP:[I

    add-int/lit16 v6, v0, -0x3131

    aget v0, v5, v6

    .line 255
    if-nez v0, :cond_5

    goto :goto_2

    :cond_7
    move v2, v3

    .end local v3    # "position":I
    .restart local v2    # "position":I
    goto :goto_0
.end method

.method private appendNameLookupForLocaleBasedName(Lcom/android/providers/contacts/SearchIndexManager$IndexBuilder;Ljava/lang/String;I)V
    .locals 3
    .param p1, "builder"    # Lcom/android/providers/contacts/SearchIndexManager$IndexBuilder;
    .param p2, "fullName"    # Ljava/lang/String;
    .param p3, "fullNameStyle"    # I

    .prologue
    .line 193
    const/4 v2, 0x5

    if-ne p3, v2, :cond_1

    .line 206
    invoke-direct {p0, p1, p2}, Lcom/android/providers/contacts/NameLookupBuilder;->appendKoreanNameConsonantsLookup(Lcom/android/providers/contacts/SearchIndexManager$IndexBuilder;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 207
    .local v0, "consonants":Ljava/lang/String;
    const/4 v1, 0x1

    .local v1, "i":I
    :goto_0
    invoke-virtual {p2}, Ljava/lang/String;->length()I

    move-result v2

    if-ge v1, v2, :cond_1

    .line 208
    invoke-virtual {p2, v1}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1, v2}, Lcom/android/providers/contacts/SearchIndexManager$IndexBuilder;->appendName(Ljava/lang/String;)V

    .line 210
    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v2

    if-le v2, v1, :cond_0

    .line 211
    invoke-virtual {v0, v1}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1, v2}, Lcom/android/providers/contacts/SearchIndexManager$IndexBuilder;->appendName(Ljava/lang/String;)V

    .line 207
    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 215
    .end local v0    # "consonants":Ljava/lang/String;
    .end local v1    # "i":I
    :cond_1
    return-void
.end method

.method public static convertCharToDigit(C)C
    .locals 4
    .param p0, "argsChar"    # C

    .prologue
    const/4 v3, 0x0

    .line 486
    const/4 v2, 0x1

    new-array v0, v2, [C

    .line 487
    .local v0, "digitChar":[C
    invoke-static {p0}, Ljava/lang/Character;->toLowerCase(C)C

    move-result v1

    .line 489
    .local v1, "lowCh":C
    packed-switch v1, :pswitch_data_0

    .line 549
    :pswitch_0
    aput-char p0, v0, v3

    .line 553
    :goto_0
    aget-char v2, v0, v3

    return v2

    .line 494
    :pswitch_1
    const/16 v2, 0x32

    aput-char v2, v0, v3

    goto :goto_0

    .line 501
    :pswitch_2
    const/16 v2, 0x33

    aput-char v2, v0, v3

    goto :goto_0

    .line 508
    :pswitch_3
    const/16 v2, 0x34

    aput-char v2, v0, v3

    goto :goto_0

    .line 515
    :pswitch_4
    const/16 v2, 0x35

    aput-char v2, v0, v3

    goto :goto_0

    .line 522
    :pswitch_5
    const/16 v2, 0x36

    aput-char v2, v0, v3

    goto :goto_0

    .line 530
    :pswitch_6
    const/16 v2, 0x37

    aput-char v2, v0, v3

    goto :goto_0

    .line 537
    :pswitch_7
    const/16 v2, 0x38

    aput-char v2, v0, v3

    goto :goto_0

    .line 545
    :pswitch_8
    const/16 v2, 0x39

    aput-char v2, v0, v3

    goto :goto_0

    .line 489
    :pswitch_data_0
    .packed-switch 0x32
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_7
        :pswitch_8
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_2
        :pswitch_2
        :pswitch_2
        :pswitch_3
        :pswitch_3
        :pswitch_3
        :pswitch_4
        :pswitch_4
        :pswitch_4
        :pswitch_5
        :pswitch_5
        :pswitch_5
        :pswitch_6
        :pswitch_6
        :pswitch_6
        :pswitch_6
        :pswitch_7
        :pswitch_7
        :pswitch_7
        :pswitch_8
        :pswitch_8
        :pswitch_8
        :pswitch_8
    .end packed-switch
.end method

.method public static convertChineseStringToPinyinAsArrayList(Ljava/lang/String;)Ljava/util/ArrayList;
    .locals 9
    .param p0, "name"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 462
    invoke-static {}, Lcom/android/providers/contacts/HanziToPinyin;->getInstance()Lcom/android/providers/contacts/HanziToPinyin;

    move-result-object v7

    invoke-virtual {v7, p0}, Lcom/android/providers/contacts/HanziToPinyin;->getTokens(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v6

    .line 463
    .local v6, "tokens":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/android/providers/contacts/HanziToPinyin$Token;>;"
    if-eqz v6, :cond_2

    invoke-virtual {v6}, Ljava/util/ArrayList;->size()I

    move-result v7

    if-lez v7, :cond_2

    .line 464
    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    .line 465
    .local v3, "pinyin":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    invoke-virtual {v6}, Ljava/util/ArrayList;->size()I

    move-result v2

    .line 466
    .local v2, "n":I
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    if-ge v0, v2, :cond_3

    .line 467
    invoke-virtual {v6, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/android/providers/contacts/HanziToPinyin$Token;

    .line 468
    .local v4, "token":Lcom/android/providers/contacts/HanziToPinyin$Token;
    iget-object v7, v4, Lcom/android/providers/contacts/HanziToPinyin$Token;->target:Ljava/lang/String;

    const-string v8, " "

    invoke-virtual {v7, v8}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v5

    .line 469
    .local v5, "tokenWords":[Ljava/lang/String;
    const/4 v1, 0x0

    .local v1, "j":I
    :goto_1
    array-length v7, v5

    if-ge v1, v7, :cond_1

    .line 470
    aget-object v7, v5, v1

    invoke-static {v7}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v7

    if-nez v7, :cond_0

    .line 471
    aget-object v7, v5, v1

    invoke-virtual {v3, v7}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 469
    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 466
    :cond_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 477
    .end local v0    # "i":I
    .end local v1    # "j":I
    .end local v2    # "n":I
    .end local v3    # "pinyin":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    .end local v4    # "token":Lcom/android/providers/contacts/HanziToPinyin$Token;
    .end local v5    # "tokenWords":[Ljava/lang/String;
    :cond_2
    const/4 v3, 0x0

    :cond_3
    return-object v3
.end method

.method public static convertHangulJamo(Ljava/lang/String;)Ljava/lang/String;
    .locals 7
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 675
    const/4 v3, 0x0

    .line 676
    .local v3, "position":I
    const/4 v1, 0x0

    .line 677
    .local v1, "consonantLength":I
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    .line 679
    .local v2, "filterBuilder":Ljava/lang/StringBuilder;
    invoke-static {p0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v5

    if-eqz v5, :cond_1

    .line 702
    .end local p0    # "name":Ljava/lang/String;
    :goto_0
    return-object p0

    .end local v3    # "position":I
    .local v0, "character":I
    .local v4, "position":I
    .restart local p0    # "name":Ljava/lang/String;
    :cond_0
    move v3, v4

    .line 684
    .end local v0    # "character":I
    .end local v4    # "position":I
    .restart local v3    # "position":I
    :cond_1
    add-int/lit8 v4, v3, 0x1

    .end local v3    # "position":I
    .restart local v4    # "position":I
    invoke-virtual {p0, v3}, Ljava/lang/String;->codePointAt(I)I

    move-result v0

    .line 688
    .restart local v0    # "character":I
    const/16 v5, 0x3131

    if-lt v0, v5, :cond_2

    const/16 v5, 0x314e

    if-gt v0, v5, :cond_2

    .line 689
    sget-object v5, Lcom/android/providers/contacts/NameLookupBuilder;->KOREAN_JAUM_CONVERT_MAP:[I

    add-int/lit16 v6, v0, -0x3131

    aget v0, v5, v6

    .line 690
    if-nez v0, :cond_3

    move v3, v4

    .line 692
    .end local v4    # "position":I
    .restart local v3    # "position":I
    goto :goto_0

    .end local v3    # "position":I
    .restart local v4    # "position":I
    :cond_2
    move v3, v4

    .line 695
    .end local v4    # "position":I
    .restart local v3    # "position":I
    goto :goto_0

    .line 698
    .end local v3    # "position":I
    .restart local v4    # "position":I
    :cond_3
    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->appendCodePoint(I)Ljava/lang/StringBuilder;

    .line 699
    add-int/lit8 v1, v1, 0x1

    .line 700
    invoke-virtual {p0}, Ljava/lang/String;->length()I

    move-result v5

    if-lt v4, v5, :cond_0

    .line 702
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p0

    .end local p0    # "name":Ljava/lang/String;
    move v3, v4

    .end local v4    # "position":I
    .restart local v3    # "position":I
    goto :goto_0
.end method

.method public static convertNameToDialArray(Ljava/lang/String;)Ljava/lang/String;
    .locals 4
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 561
    if-eqz p0, :cond_1

    .line 562
    invoke-virtual {p0}, Ljava/lang/String;->length()I

    move-result v2

    .line 563
    .local v2, "nameLen":I
    if-lez v2, :cond_1

    .line 564
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 565
    .local v0, "DialArray":Ljava/lang/StringBuilder;
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    if-ge v1, v2, :cond_0

    .line 566
    invoke-virtual {p0, v1}, Ljava/lang/String;->charAt(I)C

    move-result v3

    invoke-static {v3}, Lcom/android/providers/contacts/NameLookupBuilder;->convertCharToDigit(C)C

    move-result v3

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 565
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 568
    :cond_0
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    .line 571
    .end local v0    # "DialArray":Ljava/lang/StringBuilder;
    .end local v1    # "i":I
    .end local v2    # "nameLen":I
    :goto_1
    return-object v3

    :cond_1
    const/4 v3, 0x0

    goto :goto_1
.end method

.method private insertCollationKey(JJI)V
    .locals 9
    .param p1, "rawContactId"    # J
    .param p3, "dataId"    # J
    .param p5, "tokenCount"    # I

    .prologue
    .line 343
    iget-object v1, p0, Lcom/android/providers/contacts/NameLookupBuilder;->mStringBuilder:Ljava/lang/StringBuilder;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->setLength(I)V

    .line 345
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    if-ge v0, p5, :cond_0

    .line 349
    iget-object v1, p0, Lcom/android/providers/contacts/NameLookupBuilder;->mStringBuilder:Ljava/lang/StringBuilder;

    iget-object v2, p0, Lcom/android/providers/contacts/NameLookupBuilder;->mOriginNames:[Ljava/lang/String;

    aget-object v2, v2, v0

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 345
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 352
    :cond_0
    const/4 v6, 0x2

    iget-object v1, p0, Lcom/android/providers/contacts/NameLookupBuilder;->mStringBuilder:Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/android/providers/contacts/NameNormalizer;->normalize(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    move-object v1, p0

    move-wide v2, p1

    move-wide v4, p3

    invoke-virtual/range {v1 .. v7}, Lcom/android/providers/contacts/NameLookupBuilder;->insertNameLookup(JJILjava/lang/String;)V

    .line 354
    return-void
.end method

.method private insertNameShorthandLookup(JJLjava/lang/String;I)V
    .locals 9
    .param p1, "rawContactId"    # J
    .param p3, "dataId"    # J
    .param p5, "name"    # Ljava/lang/String;
    .param p6, "fullNameStyle"    # I

    .prologue
    .line 447
    invoke-static {}, Lcom/android/providers/contacts/ContactLocaleUtils;->getInstance()Lcom/android/providers/contacts/ContactLocaleUtils;

    move-result-object v1

    invoke-virtual {v1, p5, p6}, Lcom/android/providers/contacts/ContactLocaleUtils;->getNameLookupKeys(Ljava/lang/String;I)Ljava/util/Iterator;

    move-result-object v0

    .line 449
    .local v0, "it":Ljava/util/Iterator;, "Ljava/util/Iterator<Ljava/lang/String;>;"
    if-eqz v0, :cond_0

    .line 450
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 451
    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Ljava/lang/String;

    .line 452
    .local v8, "key":Ljava/lang/String;
    const/16 v6, 0x9

    invoke-virtual {p0, v8}, Lcom/android/providers/contacts/NameLookupBuilder;->normalizeName(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    move-object v1, p0

    move-wide v2, p1

    move-wide v4, p3

    invoke-virtual/range {v1 .. v7}, Lcom/android/providers/contacts/NameLookupBuilder;->insertNameLookup(JJILjava/lang/String;)V

    goto :goto_0

    .line 455
    .end local v8    # "key":Ljava/lang/String;
    :cond_0
    return-void
.end method

.method private insertNameVariant(JJIIZ)V
    .locals 9
    .param p1, "rawContactId"    # J
    .param p3, "dataId"    # J
    .param p5, "tokenCount"    # I
    .param p6, "lookupType"    # I
    .param p7, "buildCollationKey"    # Z

    .prologue
    .line 323
    iget-object v1, p0, Lcom/android/providers/contacts/NameLookupBuilder;->mStringBuilder:Ljava/lang/StringBuilder;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->setLength(I)V

    .line 325
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    if-ge v0, p5, :cond_1

    .line 326
    if-eqz v0, :cond_0

    .line 327
    iget-object v1, p0, Lcom/android/providers/contacts/NameLookupBuilder;->mStringBuilder:Ljava/lang/StringBuilder;

    const/16 v2, 0x2e

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 329
    :cond_0
    iget-object v1, p0, Lcom/android/providers/contacts/NameLookupBuilder;->mStringBuilder:Ljava/lang/StringBuilder;

    iget-object v2, p0, Lcom/android/providers/contacts/NameLookupBuilder;->mNames:[Ljava/lang/String;

    aget-object v2, v2, v0

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 325
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 332
    :cond_1
    iget-object v1, p0, Lcom/android/providers/contacts/NameLookupBuilder;->mStringBuilder:Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    move-object v1, p0

    move-wide v2, p1

    move-wide v4, p3

    move v6, p6

    invoke-virtual/range {v1 .. v7}, Lcom/android/providers/contacts/NameLookupBuilder;->insertNameLookup(JJILjava/lang/String;)V

    .line 334
    if-eqz p7, :cond_2

    .line 335
    invoke-direct/range {p0 .. p5}, Lcom/android/providers/contacts/NameLookupBuilder;->insertCollationKey(JJI)V

    .line 337
    :cond_2
    return-void
.end method

.method private insertNameVariants(JJIIZZ)V
    .locals 15
    .param p1, "rawContactId"    # J
    .param p3, "dataId"    # J
    .param p5, "fromIndex"    # I
    .param p6, "toIndex"    # I
    .param p7, "initiallyExact"    # Z
    .param p8, "buildCollationKey"    # Z

    .prologue
    .line 291
    move/from16 v0, p5

    move/from16 v1, p6

    if-ne v0, v1, :cond_2

    .line 292
    if-eqz p7, :cond_1

    const/4 v9, 0x0

    :goto_0
    move-object v3, p0

    move-wide/from16 v4, p1

    move-wide/from16 v6, p3

    move/from16 v8, p6

    move/from16 v10, p8

    invoke-direct/range {v3 .. v10}, Lcom/android/providers/contacts/NameLookupBuilder;->insertNameVariant(JJIIZ)V

    .line 316
    :cond_0
    return-void

    .line 292
    :cond_1
    const/4 v9, 0x1

    goto :goto_0

    .line 300
    :cond_2
    iget-object v3, p0, Lcom/android/providers/contacts/NameLookupBuilder;->mNames:[Ljava/lang/String;

    aget-object v12, v3, p5

    .line 301
    .local v12, "firstToken":Ljava/lang/String;
    iget-object v3, p0, Lcom/android/providers/contacts/NameLookupBuilder;->mOriginNames:[Ljava/lang/String;

    aget-object v2, v3, p5

    .line 302
    .local v2, "firstString":Ljava/lang/String;
    move/from16 v13, p5

    .local v13, "i":I
    :goto_1
    move/from16 v0, p6

    if-ge v13, v0, :cond_0

    .line 303
    iget-object v3, p0, Lcom/android/providers/contacts/NameLookupBuilder;->mNames:[Ljava/lang/String;

    iget-object v4, p0, Lcom/android/providers/contacts/NameLookupBuilder;->mNames:[Ljava/lang/String;

    aget-object v4, v4, v13

    aput-object v4, v3, p5

    .line 304
    iget-object v3, p0, Lcom/android/providers/contacts/NameLookupBuilder;->mNames:[Ljava/lang/String;

    aput-object v12, v3, v13

    .line 305
    iget-object v3, p0, Lcom/android/providers/contacts/NameLookupBuilder;->mOriginNames:[Ljava/lang/String;

    iget-object v4, p0, Lcom/android/providers/contacts/NameLookupBuilder;->mOriginNames:[Ljava/lang/String;

    aget-object v4, v4, v13

    aput-object v4, v3, p5

    .line 306
    iget-object v3, p0, Lcom/android/providers/contacts/NameLookupBuilder;->mOriginNames:[Ljava/lang/String;

    aput-object v2, v3, v13

    .line 308
    add-int/lit8 v8, p5, 0x1

    if-eqz p7, :cond_3

    move/from16 v0, p5

    if-ne v13, v0, :cond_3

    const/4 v10, 0x1

    :goto_2
    move-object v3, p0

    move-wide/from16 v4, p1

    move-wide/from16 v6, p3

    move/from16 v9, p6

    move/from16 v11, p8

    invoke-direct/range {v3 .. v11}, Lcom/android/providers/contacts/NameLookupBuilder;->insertNameVariants(JJIIZZ)V

    .line 311
    iget-object v3, p0, Lcom/android/providers/contacts/NameLookupBuilder;->mNames:[Ljava/lang/String;

    iget-object v4, p0, Lcom/android/providers/contacts/NameLookupBuilder;->mNames:[Ljava/lang/String;

    aget-object v4, v4, p5

    aput-object v4, v3, v13

    .line 312
    iget-object v3, p0, Lcom/android/providers/contacts/NameLookupBuilder;->mNames:[Ljava/lang/String;

    aput-object v12, v3, p5

    .line 313
    iget-object v3, p0, Lcom/android/providers/contacts/NameLookupBuilder;->mOriginNames:[Ljava/lang/String;

    iget-object v4, p0, Lcom/android/providers/contacts/NameLookupBuilder;->mOriginNames:[Ljava/lang/String;

    aget-object v4, v4, p5

    aput-object v4, v3, v13

    .line 314
    iget-object v3, p0, Lcom/android/providers/contacts/NameLookupBuilder;->mOriginNames:[Ljava/lang/String;

    aput-object v2, v3, p5

    .line 302
    add-int/lit8 v13, v13, 0x1

    goto :goto_1

    .line 308
    :cond_3
    const/4 v10, 0x0

    goto :goto_2
.end method

.method private insertNicknamePermutations(JJII)V
    .locals 15
    .param p1, "rawContactId"    # J
    .param p3, "dataId"    # J
    .param p5, "fromIndex"    # I
    .param p6, "tokenCount"    # I

    .prologue
    .line 362
    move/from16 v12, p5

    .local v12, "i":I
    :goto_0
    move/from16 v0, p6

    if-ge v12, v0, :cond_2

    .line 363
    iget-object v3, p0, Lcom/android/providers/contacts/NameLookupBuilder;->mNicknameClusters:[[Ljava/lang/String;

    aget-object v2, v3, v12

    .line 364
    .local v2, "clusters":[Ljava/lang/String;
    if-eqz v2, :cond_1

    .line 365
    iget-object v3, p0, Lcom/android/providers/contacts/NameLookupBuilder;->mNames:[Ljava/lang/String;

    aget-object v14, v3, v12

    .line 366
    .local v14, "token":Ljava/lang/String;
    const/4 v13, 0x0

    .local v13, "j":I
    :goto_1
    array-length v3, v2

    if-ge v13, v3, :cond_0

    .line 367
    iget-object v3, p0, Lcom/android/providers/contacts/NameLookupBuilder;->mNames:[Ljava/lang/String;

    aget-object v4, v2, v13

    aput-object v4, v3, v12

    .line 370
    const/4 v8, 0x0

    const/4 v10, 0x0

    const/4 v11, 0x0

    move-object v3, p0

    move-wide/from16 v4, p1

    move-wide/from16 v6, p3

    move/from16 v9, p6

    invoke-direct/range {v3 .. v11}, Lcom/android/providers/contacts/NameLookupBuilder;->insertNameVariants(JJIIZZ)V

    .line 373
    add-int/lit8 v8, v12, 0x1

    move-object v3, p0

    move-wide/from16 v4, p1

    move-wide/from16 v6, p3

    move/from16 v9, p6

    invoke-direct/range {v3 .. v9}, Lcom/android/providers/contacts/NameLookupBuilder;->insertNicknamePermutations(JJII)V

    .line 366
    add-int/lit8 v13, v13, 0x1

    goto :goto_1

    .line 375
    :cond_0
    iget-object v3, p0, Lcom/android/providers/contacts/NameLookupBuilder;->mNames:[Ljava/lang/String;

    aput-object v14, v3, v12

    .line 362
    .end local v13    # "j":I
    .end local v14    # "token":Ljava/lang/String;
    :cond_1
    add-int/lit8 v12, v12, 0x1

    goto :goto_0

    .line 378
    .end local v2    # "clusters":[Ljava/lang/String;
    :cond_2
    return-void
.end method


# virtual methods
.method public appendNameShorthandLookup(Lcom/android/providers/contacts/SearchIndexManager$IndexBuilder;Ljava/lang/String;I)V
    .locals 2
    .param p1, "builder"    # Lcom/android/providers/contacts/SearchIndexManager$IndexBuilder;
    .param p2, "name"    # Ljava/lang/String;
    .param p3, "fullNameStyle"    # I

    .prologue
    .line 432
    invoke-static {}, Lcom/android/providers/contacts/ContactLocaleUtils;->getInstance()Lcom/android/providers/contacts/ContactLocaleUtils;

    move-result-object v1

    invoke-virtual {v1, p2, p3}, Lcom/android/providers/contacts/ContactLocaleUtils;->getNameLookupKeys(Ljava/lang/String;I)Ljava/util/Iterator;

    move-result-object v0

    .line 434
    .local v0, "it":Ljava/util/Iterator;, "Ljava/util/Iterator<Ljava/lang/String;>;"
    if-eqz v0, :cond_0

    .line 435
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 436
    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    invoke-virtual {p1, v1}, Lcom/android/providers/contacts/SearchIndexManager$IndexBuilder;->appendName(Ljava/lang/String;)V

    goto :goto_0

    .line 439
    :cond_0
    return-void
.end method

.method public appendToSearchIndex(Lcom/android/providers/contacts/SearchIndexManager$IndexBuilder;Ljava/lang/String;I)V
    .locals 4
    .param p1, "builder"    # Lcom/android/providers/contacts/SearchIndexManager$IndexBuilder;
    .param p2, "name"    # Ljava/lang/String;
    .param p3, "fullNameStyle"    # I

    .prologue
    .line 175
    iget-object v2, p0, Lcom/android/providers/contacts/NameLookupBuilder;->mSplitter:Lcom/android/providers/contacts/NameSplitter;

    iget-object v3, p0, Lcom/android/providers/contacts/NameLookupBuilder;->mNames:[Ljava/lang/String;

    invoke-virtual {v2, v3, p2}, Lcom/android/providers/contacts/NameSplitter;->tokenize([Ljava/lang/String;Ljava/lang/String;)I

    move-result v1

    .line 176
    .local v1, "tokenCount":I
    if-nez v1, :cond_0

    .line 186
    :goto_0
    return-void

    .line 180
    :cond_0
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_1
    if-ge v0, v1, :cond_1

    .line 181
    iget-object v2, p0, Lcom/android/providers/contacts/NameLookupBuilder;->mNames:[Ljava/lang/String;

    aget-object v2, v2, v0

    invoke-virtual {p1, v2}, Lcom/android/providers/contacts/SearchIndexManager$IndexBuilder;->appendName(Ljava/lang/String;)V

    .line 180
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 184
    :cond_1
    invoke-virtual {p0, p1, p2, p3}, Lcom/android/providers/contacts/NameLookupBuilder;->appendNameShorthandLookup(Lcom/android/providers/contacts/SearchIndexManager$IndexBuilder;Ljava/lang/String;I)V

    .line 185
    invoke-direct {p0, p1, p2, p3}, Lcom/android/providers/contacts/NameLookupBuilder;->appendNameLookupForLocaleBasedName(Lcom/android/providers/contacts/SearchIndexManager$IndexBuilder;Ljava/lang/String;I)V

    goto :goto_0
.end method

.method protected abstract getCommonNicknameClusters(Ljava/lang/String;)[Ljava/lang/String;
.end method

.method protected abstract insertNameLookup(JJILjava/lang/String;)V
.end method

.method public insertNameLookup(JJLjava/lang/String;I)V
    .locals 17
    .param p1, "rawContactId"    # J
    .param p3, "dataId"    # J
    .param p5, "name"    # Ljava/lang/String;
    .param p6, "fullNameStyle"    # I

    .prologue
    .line 109
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/android/providers/contacts/NameLookupBuilder;->mSplitter:Lcom/android/providers/contacts/NameSplitter;

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/android/providers/contacts/NameLookupBuilder;->mNames:[Ljava/lang/String;

    move-object/from16 v0, p5

    invoke-virtual {v1, v2, v0}, Lcom/android/providers/contacts/NameSplitter;->tokenize([Ljava/lang/String;Ljava/lang/String;)I

    move-result v7

    .line 110
    .local v7, "tokenCount":I
    if-nez v7, :cond_1

    .line 172
    :cond_0
    :goto_0
    return-void

    .line 118
    :cond_1
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/android/providers/contacts/NameLookupBuilder;->mNames:[Ljava/lang/String;

    const/4 v2, 0x0

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/android/providers/contacts/NameLookupBuilder;->mOriginNames:[Ljava/lang/String;

    const/4 v4, 0x0

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/android/providers/contacts/NameLookupBuilder;->mNames:[Ljava/lang/String;

    array-length v5, v5

    invoke-static {v1, v2, v3, v4, v5}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 119
    const/4 v15, 0x0

    .local v15, "i":I
    :goto_1
    if-ge v15, v7, :cond_2

    .line 120
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/android/providers/contacts/NameLookupBuilder;->mNames:[Ljava/lang/String;

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/android/providers/contacts/NameLookupBuilder;->mNames:[Ljava/lang/String;

    aget-object v2, v2, v15

    move-object/from16 v0, p0

    invoke-virtual {v0, v2}, Lcom/android/providers/contacts/NameLookupBuilder;->normalizeName(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    aput-object v2, v1, v15

    .line 119
    add-int/lit8 v15, v15, 0x1

    goto :goto_1

    .line 123
    :cond_2
    const/4 v1, 0x4

    if-le v7, v1, :cond_3

    const/16 v16, 0x1

    .line 124
    .local v16, "tooManyTokens":Z
    :goto_2
    if-eqz v16, :cond_5

    .line 125
    const/4 v8, 0x0

    const/4 v9, 0x1

    move-object/from16 v2, p0

    move-wide/from16 v3, p1

    move-wide/from16 v5, p3

    invoke-direct/range {v2 .. v9}, Lcom/android/providers/contacts/NameLookupBuilder;->insertNameVariant(JJIIZ)V

    .line 128
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/android/providers/contacts/NameLookupBuilder;->mNames:[Ljava/lang/String;

    const/4 v2, 0x0

    new-instance v3, Lcom/android/providers/contacts/NameLookupBuilder$1;

    move-object/from16 v0, p0

    invoke-direct {v3, v0}, Lcom/android/providers/contacts/NameLookupBuilder$1;-><init>(Lcom/android/providers/contacts/NameLookupBuilder;)V

    invoke-static {v1, v2, v7, v3}, Ljava/util/Arrays;->sort([Ljava/lang/Object;IILjava/util/Comparator;)V

    .line 136
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/android/providers/contacts/NameLookupBuilder;->mOriginNames:[Ljava/lang/String;

    const/4 v2, 0x0

    new-instance v3, Lcom/android/providers/contacts/NameLookupBuilder$2;

    move-object/from16 v0, p0

    invoke-direct {v3, v0}, Lcom/android/providers/contacts/NameLookupBuilder$2;-><init>(Lcom/android/providers/contacts/NameLookupBuilder;)V

    invoke-static {v1, v2, v7, v3}, Ljava/util/Arrays;->sort([Ljava/lang/Object;IILjava/util/Comparator;)V

    .line 146
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/android/providers/contacts/NameLookupBuilder;->mOriginNames:[Ljava/lang/String;

    const/4 v2, 0x0

    aget-object v14, v1, v2

    .line 147
    .local v14, "firstString":Ljava/lang/String;
    const/4 v15, 0x4

    :goto_3
    if-ge v15, v7, :cond_4

    .line 149
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/android/providers/contacts/NameLookupBuilder;->mOriginNames:[Ljava/lang/String;

    const/4 v2, 0x0

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/android/providers/contacts/NameLookupBuilder;->mOriginNames:[Ljava/lang/String;

    aget-object v3, v3, v15

    aput-object v3, v1, v2

    .line 150
    const/4 v13, 0x4

    move-object/from16 v8, p0

    move-wide/from16 v9, p1

    move-wide/from16 v11, p3

    invoke-direct/range {v8 .. v13}, Lcom/android/providers/contacts/NameLookupBuilder;->insertCollationKey(JJI)V

    .line 147
    add-int/lit8 v15, v15, 0x1

    goto :goto_3

    .line 123
    .end local v14    # "firstString":Ljava/lang/String;
    .end local v16    # "tooManyTokens":Z
    :cond_3
    const/16 v16, 0x0

    goto :goto_2

    .line 153
    .restart local v14    # "firstString":Ljava/lang/String;
    .restart local v16    # "tooManyTokens":Z
    :cond_4
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/android/providers/contacts/NameLookupBuilder;->mOriginNames:[Ljava/lang/String;

    const/4 v2, 0x0

    aput-object v14, v1, v2

    .line 155
    const/4 v7, 0x4

    .line 159
    .end local v14    # "firstString":Ljava/lang/String;
    :cond_5
    const/4 v15, 0x0

    :goto_4
    if-ge v15, v7, :cond_6

    .line 160
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/android/providers/contacts/NameLookupBuilder;->mNicknameClusters:[[Ljava/lang/String;

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/android/providers/contacts/NameLookupBuilder;->mNames:[Ljava/lang/String;

    aget-object v2, v2, v15

    move-object/from16 v0, p0

    invoke-virtual {v0, v2}, Lcom/android/providers/contacts/NameLookupBuilder;->getCommonNicknameClusters(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v2

    aput-object v2, v1, v15

    .line 159
    add-int/lit8 v15, v15, 0x1

    goto :goto_4

    .line 163
    :cond_6
    const/4 v6, 0x0

    if-nez v16, :cond_8

    const/4 v8, 0x1

    :goto_5
    const/4 v9, 0x1

    move-object/from16 v1, p0

    move-wide/from16 v2, p1

    move-wide/from16 v4, p3

    invoke-direct/range {v1 .. v9}, Lcom/android/providers/contacts/NameLookupBuilder;->insertNameVariants(JJIIZZ)V

    .line 164
    const/4 v6, 0x0

    move-object/from16 v1, p0

    move-wide/from16 v2, p1

    move-wide/from16 v4, p3

    invoke-direct/range {v1 .. v7}, Lcom/android/providers/contacts/NameLookupBuilder;->insertNicknamePermutations(JJII)V

    .line 166
    invoke-static {}, Lcom/sec/android/app/CscFeature;->getInstance()Lcom/sec/android/app/CscFeature;

    move-result-object v1

    const-string v2, "CscFeature_Contact_EnableStrokeSortList"

    invoke-virtual {v1, v2}, Lcom/sec/android/app/CscFeature;->getEnableStatus(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_7

    invoke-static {}, Lcom/sec/android/app/CscFeature;->getInstance()Lcom/sec/android/app/CscFeature;

    move-result-object v1

    const-string v2, "CscFeature_Contact_EnableBPMFSortList"

    invoke-virtual {v1, v2}, Lcom/sec/android/app/CscFeature;->getEnableStatus(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 170
    :cond_7
    invoke-direct/range {p0 .. p6}, Lcom/android/providers/contacts/NameLookupBuilder;->insertNameShorthandLookup(JJLjava/lang/String;I)V

    goto/16 :goto_0

    .line 163
    :cond_8
    const/4 v8, 0x0

    goto :goto_5
.end method

.method public insertNameLookupForDigitName(JJLjava/lang/String;)V
    .locals 19
    .param p1, "rawContactId"    # J
    .param p3, "dataId"    # J
    .param p5, "name"    # Ljava/lang/String;

    .prologue
    .line 579
    invoke-static {}, Lcom/android/providers/contacts/ContactLocaleUtils;->getInstance()Lcom/android/providers/contacts/ContactLocaleUtils;

    move-result-object v3

    move-object/from16 v0, p5

    invoke-virtual {v3, v0}, Lcom/android/providers/contacts/ContactLocaleUtils;->getMultiPinyinsForName(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v14

    .line 580
    .local v14, "multiPinyin":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    invoke-static/range {p5 .. p5}, Lcom/android/providers/contacts/NameLookupBuilder;->convertChineseStringToPinyinAsArrayList(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v15

    .line 582
    .local v15, "pinyin":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    if-eqz v14, :cond_1

    invoke-virtual {v14}, Ljava/util/ArrayList;->size()I

    move-result v3

    const/4 v4, 0x1

    if-le v3, v4, :cond_1

    .line 583
    const-string v3, "NameLookupBuiler"

    const-string v4, "insert multi pinyin to dial name lookup"

    invoke-static {v3, v4}, Landroid/util/secutil/Log;->secI(Ljava/lang/String;Ljava/lang/String;)I

    .line 584
    invoke-virtual/range {p0 .. p5}, Lcom/android/providers/contacts/NameLookupBuilder;->insertNameLookupForMultiPinyinDigitName(JJLjava/lang/String;)V

    .line 622
    :cond_0
    :goto_0
    return-void

    .line 588
    :cond_1
    if-eqz v15, :cond_0

    .line 589
    invoke-virtual {v15}, Ljava/util/ArrayList;->size()I

    move-result v17

    .line 590
    .local v17, "pinyinCount":I
    if-lez v17, :cond_0

    .line 595
    new-instance v11, Ljava/lang/StringBuilder;

    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    .line 596
    .local v11, "fullPinyinBuilder":Ljava/lang/StringBuilder;
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    .line 598
    .local v2, "firstPinyinBuilder":Ljava/lang/StringBuilder;
    add-int/lit8 v13, v17, -0x1

    .local v13, "i":I
    :goto_1
    if-ltz v13, :cond_2

    .line 599
    invoke-virtual {v15, v13}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v18

    check-cast v18, Ljava/lang/String;

    .line 600
    .local v18, "pinyinStr":Ljava/lang/String;
    invoke-virtual {v15, v13}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    const/4 v4, 0x0

    invoke-virtual {v3, v4}, Ljava/lang/String;->charAt(I)C

    move-result v16

    .line 602
    .local v16, "pinyinChar":C
    invoke-static/range {v18 .. v18}, Lcom/android/providers/contacts/NameLookupBuilder;->convertNameToDialArray(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v12

    .line 603
    .local v12, "fullPinyinDialName":Ljava/lang/String;
    invoke-static/range {v16 .. v16}, Lcom/android/providers/contacts/NameLookupBuilder;->convertCharToDigit(C)C

    move-result v3

    invoke-static {v3}, Ljava/lang/Character;->toString(C)Ljava/lang/String;

    move-result-object v10

    .line 604
    .local v10, "firstPinyinDialName":Ljava/lang/String;
    const/4 v3, 0x0

    invoke-virtual {v11, v3, v12}, Ljava/lang/StringBuilder;->insert(ILjava/lang/String;)Ljava/lang/StringBuilder;

    .line 605
    const/4 v3, 0x0

    invoke-virtual {v2, v3, v10}, Ljava/lang/StringBuilder;->insert(ILjava/lang/String;)Ljava/lang/StringBuilder;

    .line 611
    if-nez v13, :cond_3

    .line 616
    .end local v10    # "firstPinyinDialName":Ljava/lang/String;
    .end local v12    # "fullPinyinDialName":Ljava/lang/String;
    .end local v16    # "pinyinChar":C
    .end local v18    # "pinyinStr":Ljava/lang/String;
    :cond_2
    const/4 v8, 0x7

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    move-object/from16 v0, p0

    invoke-virtual {v0, v3}, Lcom/android/providers/contacts/NameLookupBuilder;->normalizeName(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v9

    move-object/from16 v3, p0

    move-wide/from16 v4, p1

    move-wide/from16 v6, p3

    invoke-virtual/range {v3 .. v9}, Lcom/android/providers/contacts/NameLookupBuilder;->insertNameLookup(JJILjava/lang/String;)V

    .line 618
    const/16 v8, 0xa

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    move-object/from16 v0, p0

    invoke-virtual {v0, v3}, Lcom/android/providers/contacts/NameLookupBuilder;->normalizeName(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v9

    move-object/from16 v3, p0

    move-wide/from16 v4, p1

    move-wide/from16 v6, p3

    invoke-virtual/range {v3 .. v9}, Lcom/android/providers/contacts/NameLookupBuilder;->insertNameLookup(JJILjava/lang/String;)V

    goto :goto_0

    .line 613
    .restart local v10    # "firstPinyinDialName":Ljava/lang/String;
    .restart local v12    # "fullPinyinDialName":Ljava/lang/String;
    .restart local v16    # "pinyinChar":C
    .restart local v18    # "pinyinStr":Ljava/lang/String;
    :cond_3
    const/16 v8, 0x8

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    move-object/from16 v0, p0

    invoke-virtual {v0, v3}, Lcom/android/providers/contacts/NameLookupBuilder;->normalizeName(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v9

    move-object/from16 v3, p0

    move-wide/from16 v4, p1

    move-wide/from16 v6, p3

    invoke-virtual/range {v3 .. v9}, Lcom/android/providers/contacts/NameLookupBuilder;->insertNameLookup(JJILjava/lang/String;)V

    .line 598
    add-int/lit8 v13, v13, -0x1

    goto :goto_1
.end method

.method public insertNameLookupForMultiPinyinDigitName(JJLjava/lang/String;)V
    .locals 21
    .param p1, "rawContactId"    # J
    .param p3, "dataId"    # J
    .param p5, "name"    # Ljava/lang/String;

    .prologue
    .line 629
    if-nez p5, :cond_1

    .line 670
    :cond_0
    return-void

    .line 631
    :cond_1
    invoke-static {}, Lcom/android/providers/contacts/ContactLocaleUtils;->getInstance()Lcom/android/providers/contacts/ContactLocaleUtils;

    move-result-object v3

    move-object/from16 v0, p5

    invoke-virtual {v3, v0}, Lcom/android/providers/contacts/ContactLocaleUtils;->getMultiPinyinsForName(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v14

    .line 632
    .local v14, "multiPinyin":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    invoke-virtual/range {p5 .. p5}, Ljava/lang/String;->length()I

    move-result v16

    .line 633
    .local v16, "namelen":I
    const/4 v12, 0x1

    .local v12, "i":I
    :goto_0
    move/from16 v0, v16

    if-ge v12, v0, :cond_2

    .line 634
    const/16 v8, 0x8

    move-object/from16 v0, p5

    invoke-virtual {v0, v12}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v3

    move-object/from16 v0, p0

    invoke-virtual {v0, v3}, Lcom/android/providers/contacts/NameLookupBuilder;->normalizeName(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v9

    move-object/from16 v3, p0

    move-wide/from16 v4, p1

    move-wide/from16 v6, p3

    invoke-virtual/range {v3 .. v9}, Lcom/android/providers/contacts/NameLookupBuilder;->insertNameLookup(JJILjava/lang/String;)V

    .line 633
    add-int/lit8 v12, v12, 0x1

    goto :goto_0

    .line 638
    :cond_2
    invoke-virtual {v14}, Ljava/util/ArrayList;->size()I

    move-result v17

    .line 639
    .local v17, "pinyinCount":I
    if-lez v17, :cond_0

    .line 640
    const/4 v12, 0x0

    :goto_1
    move/from16 v0, v17

    if-ge v12, v0, :cond_0

    .line 641
    invoke-virtual {v14, v12}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    const-string v4, "\\|"

    invoke-virtual {v3, v4}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v2

    .line 642
    .local v2, "array":[Ljava/lang/String;
    array-length v15, v2

    .line 647
    .local v15, "multiPinyinCount":I
    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    .line 648
    .local v10, "fullBuilder":Ljava/lang/StringBuilder;
    new-instance v18, Ljava/lang/StringBuilder;

    invoke-direct/range {v18 .. v18}, Ljava/lang/StringBuilder;-><init>()V

    .line 650
    .local v18, "simpleBuilder":Ljava/lang/StringBuilder;
    add-int/lit8 v13, v15, -0x1

    .local v13, "j":I
    :goto_2
    if-ltz v13, :cond_5

    .line 651
    const-string v3, ""

    aget-object v4, v2, v13

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_4

    .line 650
    :cond_3
    :goto_3
    add-int/lit8 v13, v13, -0x1

    goto :goto_2

    .line 654
    :cond_4
    aget-object v3, v2, v13

    invoke-static {v3}, Lcom/android/providers/contacts/NameLookupBuilder;->convertNameToDialArray(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v11

    .line 655
    .local v11, "fullPinyinName":Ljava/lang/String;
    aget-object v3, v2, v13

    const/4 v4, 0x0

    invoke-virtual {v3, v4}, Ljava/lang/String;->charAt(I)C

    move-result v3

    invoke-static {v3}, Lcom/android/providers/contacts/NameLookupBuilder;->convertCharToDigit(C)C

    move-result v3

    invoke-static {v3}, Ljava/lang/Character;->toString(C)Ljava/lang/String;

    move-result-object v19

    .line 657
    .local v19, "simplePinyinName":Ljava/lang/String;
    const/4 v3, 0x0

    invoke-virtual {v10, v3, v11}, Ljava/lang/StringBuilder;->insert(ILjava/lang/String;)Ljava/lang/StringBuilder;

    .line 658
    const/4 v3, 0x0

    move-object/from16 v0, v18

    move-object/from16 v1, v19

    invoke-virtual {v0, v3, v1}, Ljava/lang/StringBuilder;->insert(ILjava/lang/String;)Ljava/lang/StringBuilder;

    .line 659
    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v3

    const/4 v4, 0x1

    if-le v3, v4, :cond_3

    .line 660
    const/16 v8, 0x8

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    move-object/from16 v0, p0

    invoke-virtual {v0, v3}, Lcom/android/providers/contacts/NameLookupBuilder;->normalizeName(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v9

    move-object/from16 v3, p0

    move-wide/from16 v4, p1

    move-wide/from16 v6, p3

    invoke-virtual/range {v3 .. v9}, Lcom/android/providers/contacts/NameLookupBuilder;->insertNameLookup(JJILjava/lang/String;)V

    goto :goto_3

    .line 664
    .end local v11    # "fullPinyinName":Ljava/lang/String;
    .end local v19    # "simplePinyinName":Ljava/lang/String;
    :cond_5
    const/4 v8, 0x7

    invoke-virtual/range {v18 .. v18}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    move-object/from16 v0, p0

    invoke-virtual {v0, v3}, Lcom/android/providers/contacts/NameLookupBuilder;->normalizeName(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v9

    move-object/from16 v3, p0

    move-wide/from16 v4, p1

    move-wide/from16 v6, p3

    invoke-virtual/range {v3 .. v9}, Lcom/android/providers/contacts/NameLookupBuilder;->insertNameLookup(JJILjava/lang/String;)V

    .line 666
    const/16 v8, 0xa

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    move-object/from16 v0, p0

    invoke-virtual {v0, v3}, Lcom/android/providers/contacts/NameLookupBuilder;->normalizeName(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v9

    move-object/from16 v3, p0

    move-wide/from16 v4, p1

    move-wide/from16 v6, p3

    invoke-virtual/range {v3 .. v9}, Lcom/android/providers/contacts/NameLookupBuilder;->insertNameLookup(JJILjava/lang/String;)V

    .line 640
    add-int/lit8 v12, v12, 0x1

    goto/16 :goto_1
.end method

.method public insertNameLookupForMultiPinyinSearch(JJLjava/lang/String;)V
    .locals 19
    .param p1, "rawContactId"    # J
    .param p3, "dataId"    # J
    .param p5, "name"    # Ljava/lang/String;

    .prologue
    .line 386
    if-nez p5, :cond_1

    .line 424
    :cond_0
    return-void

    .line 389
    :cond_1
    invoke-static {}, Lcom/android/providers/contacts/ContactLocaleUtils;->getInstance()Lcom/android/providers/contacts/ContactLocaleUtils;

    move-result-object v3

    move-object/from16 v0, p5

    invoke-virtual {v3, v0}, Lcom/android/providers/contacts/ContactLocaleUtils;->getMultiPinyinsForName(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v13

    .line 391
    .local v13, "multiPinyin":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    if-eqz v13, :cond_0

    invoke-virtual {v13}, Ljava/util/ArrayList;->size()I

    move-result v3

    const/4 v4, 0x1

    if-le v3, v4, :cond_0

    .line 396
    invoke-virtual/range {p5 .. p5}, Ljava/lang/String;->length()I

    move-result v15

    .line 397
    .local v15, "namelen":I
    const/4 v11, 0x1

    .local v11, "i":I
    :goto_0
    if-ge v11, v15, :cond_2

    .line 398
    const/4 v8, 0x6

    move-object/from16 v0, p5

    invoke-virtual {v0, v11}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v3

    move-object/from16 v0, p0

    invoke-virtual {v0, v3}, Lcom/android/providers/contacts/NameLookupBuilder;->normalizeName(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v9

    move-object/from16 v3, p0

    move-wide/from16 v4, p1

    move-wide/from16 v6, p3

    invoke-virtual/range {v3 .. v9}, Lcom/android/providers/contacts/NameLookupBuilder;->insertNameLookup(JJILjava/lang/String;)V

    .line 397
    add-int/lit8 v11, v11, 0x1

    goto :goto_0

    .line 402
    :cond_2
    invoke-virtual {v13}, Ljava/util/ArrayList;->size()I

    move-result v16

    .line 403
    .local v16, "pinyinCount":I
    if-lez v16, :cond_0

    .line 404
    const/4 v11, 0x0

    :goto_1
    move/from16 v0, v16

    if-ge v11, v0, :cond_0

    .line 405
    invoke-virtual {v13, v11}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    const-string v4, "\\|"

    invoke-virtual {v3, v4}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v2

    .line 406
    .local v2, "array":[Ljava/lang/String;
    array-length v14, v2

    .line 407
    .local v14, "multiPinyinCount":I
    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    .line 408
    .local v10, "fullBuilder":Ljava/lang/StringBuilder;
    new-instance v17, Ljava/lang/StringBuilder;

    invoke-direct/range {v17 .. v17}, Ljava/lang/StringBuilder;-><init>()V

    .line 410
    .local v17, "simpleBuilder":Ljava/lang/StringBuilder;
    add-int/lit8 v12, v14, -0x1

    .local v12, "j":I
    :goto_2
    if-ltz v12, :cond_4

    .line 411
    const-string v3, ""

    aget-object v4, v2, v12

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3

    .line 410
    :goto_3
    add-int/lit8 v12, v12, -0x1

    goto :goto_2

    .line 415
    :cond_3
    const/4 v3, 0x0

    aget-object v4, v2, v12

    invoke-virtual {v10, v3, v4}, Ljava/lang/StringBuilder;->insert(ILjava/lang/String;)Ljava/lang/StringBuilder;

    .line 416
    const/4 v3, 0x0

    aget-object v4, v2, v12

    const/4 v5, 0x0

    invoke-virtual {v4, v5}, Ljava/lang/String;->charAt(I)C

    move-result v4

    move-object/from16 v0, v17

    invoke-virtual {v0, v3, v4}, Ljava/lang/StringBuilder;->insert(IC)Ljava/lang/StringBuilder;

    .line 417
    const/4 v8, 0x6

    invoke-virtual/range {v17 .. v17}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    move-object/from16 v0, p0

    invoke-virtual {v0, v3}, Lcom/android/providers/contacts/NameLookupBuilder;->normalizeName(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v9

    move-object/from16 v3, p0

    move-wide/from16 v4, p1

    move-wide/from16 v6, p3

    invoke-virtual/range {v3 .. v9}, Lcom/android/providers/contacts/NameLookupBuilder;->insertNameLookup(JJILjava/lang/String;)V

    .line 419
    const/4 v8, 0x6

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    move-object/from16 v0, p0

    invoke-virtual {v0, v3}, Lcom/android/providers/contacts/NameLookupBuilder;->normalizeName(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v9

    move-object/from16 v3, p0

    move-wide/from16 v4, p1

    move-wide/from16 v6, p3

    invoke-virtual/range {v3 .. v9}, Lcom/android/providers/contacts/NameLookupBuilder;->insertNameLookup(JJILjava/lang/String;)V

    goto :goto_3

    .line 404
    :cond_4
    add-int/lit8 v11, v11, 0x1

    goto :goto_1
.end method

.method protected normalizeName(Ljava/lang/String;)Ljava/lang/String;
    .locals 1
    .param p1, "name"    # Ljava/lang/String;

    .prologue
    .line 276
    invoke-static {p1}, Lcom/android/providers/contacts/NameNormalizer;->normalize(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
