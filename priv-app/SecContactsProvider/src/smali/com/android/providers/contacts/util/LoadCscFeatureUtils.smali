.class public Lcom/android/providers/contacts/util/LoadCscFeatureUtils;
.super Ljava/lang/Object;
.source "LoadCscFeatureUtils.java"


# static fields
.field static mDisableEmergencyGroup:Z

.field static mEnableAAB:Z

.field static mEnableSearchNameIgnoringCommonPrefix:Z

.field private static mEnableSmartContact:Z

.field static mEnableStrokeBPMF:Z

.field static mEnableSupportChineseDigitNameSearch:Z

.field static mEnableSupportFuzzySearch:Z

.field static mEnableSupportMultiPinyinSearch:Z

.field static mFuzzySearchType:Ljava/lang/String;

.field static mMinMatch:I

.field static mNameLengthLimit:I

.field private static sLoadCscFeatureUtils:Lcom/android/providers/contacts/util/LoadCscFeatureUtils;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 32
    const/4 v0, 0x0

    sput-object v0, Lcom/android/providers/contacts/util/LoadCscFeatureUtils;->sLoadCscFeatureUtils:Lcom/android/providers/contacts/util/LoadCscFeatureUtils;

    .line 34
    sput-boolean v1, Lcom/android/providers/contacts/util/LoadCscFeatureUtils;->mEnableAAB:Z

    .line 35
    sput-boolean v1, Lcom/android/providers/contacts/util/LoadCscFeatureUtils;->mEnableSearchNameIgnoringCommonPrefix:Z

    .line 36
    sput-boolean v1, Lcom/android/providers/contacts/util/LoadCscFeatureUtils;->mEnableSupportFuzzySearch:Z

    .line 37
    sput-boolean v1, Lcom/android/providers/contacts/util/LoadCscFeatureUtils;->mEnableSupportChineseDigitNameSearch:Z

    .line 38
    sput-boolean v1, Lcom/android/providers/contacts/util/LoadCscFeatureUtils;->mEnableSupportMultiPinyinSearch:Z

    .line 39
    sput-boolean v1, Lcom/android/providers/contacts/util/LoadCscFeatureUtils;->mDisableEmergencyGroup:Z

    .line 40
    const-string v0, "NONE"

    sput-object v0, Lcom/android/providers/contacts/util/LoadCscFeatureUtils;->mFuzzySearchType:Ljava/lang/String;

    .line 41
    const/16 v0, 0x3e8

    sput v0, Lcom/android/providers/contacts/util/LoadCscFeatureUtils;->mNameLengthLimit:I

    .line 43
    sput-boolean v1, Lcom/android/providers/contacts/util/LoadCscFeatureUtils;->mEnableStrokeBPMF:Z

    .line 44
    sput-boolean v1, Lcom/android/providers/contacts/util/LoadCscFeatureUtils;->mEnableSmartContact:Z

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 28
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static getInstance()Lcom/android/providers/contacts/util/LoadCscFeatureUtils;
    .locals 3

    .prologue
    .line 47
    sget-object v0, Lcom/android/providers/contacts/util/LoadCscFeatureUtils;->sLoadCscFeatureUtils:Lcom/android/providers/contacts/util/LoadCscFeatureUtils;

    if-nez v0, :cond_0

    .line 48
    new-instance v0, Lcom/android/providers/contacts/util/LoadCscFeatureUtils;

    invoke-direct {v0}, Lcom/android/providers/contacts/util/LoadCscFeatureUtils;-><init>()V

    sput-object v0, Lcom/android/providers/contacts/util/LoadCscFeatureUtils;->sLoadCscFeatureUtils:Lcom/android/providers/contacts/util/LoadCscFeatureUtils;

    .line 49
    invoke-static {}, Lcom/android/providers/contacts/util/LoadCscFeatureUtils;->setCscFeature()V

    .line 50
    const-string v0, "LoadCscFeatureUtils"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "EnableSupportAAB = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    sget-boolean v2, Lcom/android/providers/contacts/util/LoadCscFeatureUtils;->mEnableAAB:Z

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 51
    const-string v0, "LoadCscFeatureUtils"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "EnableSearchNameIgnoringCommonPrefix = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    sget-boolean v2, Lcom/android/providers/contacts/util/LoadCscFeatureUtils;->mEnableSearchNameIgnoringCommonPrefix:Z

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 52
    const-string v0, "LoadCscFeatureUtils"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "EnableSupportFuzzySearch = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    sget-boolean v2, Lcom/android/providers/contacts/util/LoadCscFeatureUtils;->mEnableSupportFuzzySearch:Z

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 53
    const-string v0, "LoadCscFeatureUtils"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "EnableSupportChineseDigitNameSearch = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    sget-boolean v2, Lcom/android/providers/contacts/util/LoadCscFeatureUtils;->mEnableSupportChineseDigitNameSearch:Z

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 54
    const-string v0, "LoadCscFeatureUtils"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "DisableEmergencyGroup = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    sget-boolean v2, Lcom/android/providers/contacts/util/LoadCscFeatureUtils;->mDisableEmergencyGroup:Z

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 55
    const-string v0, "LoadCscFeatureUtils"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "EnableSupportMultiPinyinSearch = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    sget-boolean v2, Lcom/android/providers/contacts/util/LoadCscFeatureUtils;->mEnableSupportMultiPinyinSearch:Z

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 56
    const-string v0, "LoadCscFeatureUtils"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "MinMatch = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    sget v2, Lcom/android/providers/contacts/util/LoadCscFeatureUtils;->mMinMatch:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 57
    const-string v0, "LoadCscFeatureUtils"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "mEnableStrokeBPMF = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    sget-boolean v2, Lcom/android/providers/contacts/util/LoadCscFeatureUtils;->mEnableStrokeBPMF:Z

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 60
    :cond_0
    sget-object v0, Lcom/android/providers/contacts/util/LoadCscFeatureUtils;->sLoadCscFeatureUtils:Lcom/android/providers/contacts/util/LoadCscFeatureUtils;

    return-object v0
.end method

.method public static setCscFeature()V
    .locals 5

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 64
    invoke-static {}, Lcom/sec/android/app/CscFeature;->getInstance()Lcom/sec/android/app/CscFeature;

    move-result-object v0

    .line 65
    .local v0, "cscFeature":Lcom/sec/android/app/CscFeature;
    const-string v1, "CscFeature_Contact_EnableAAB"

    invoke-virtual {v0, v1}, Lcom/sec/android/app/CscFeature;->getEnableStatus(Ljava/lang/String;)Z

    move-result v1

    sput-boolean v1, Lcom/android/providers/contacts/util/LoadCscFeatureUtils;->mEnableAAB:Z

    .line 67
    const-string v1, "CscFeature_Contact_SearchNameIgnoringCommonPrefix"

    invoke-virtual {v0, v1}, Lcom/sec/android/app/CscFeature;->getEnableStatus(Ljava/lang/String;)Z

    move-result v1

    sput-boolean v1, Lcom/android/providers/contacts/util/LoadCscFeatureUtils;->mEnableSearchNameIgnoringCommonPrefix:Z

    .line 69
    const-string v1, "CscFeature_Contact_EnablePuzzySearch"

    invoke-virtual {v0, v1}, Lcom/sec/android/app/CscFeature;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    const-string v4, "CHNCDMA"

    invoke-virtual {v1, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    const-string v1, "CscFeature_Contact_EnablePuzzySearch"

    invoke-virtual {v0, v1}, Lcom/sec/android/app/CscFeature;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    const-string v4, "CHNGSM"

    invoke-virtual {v1, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_3

    :cond_0
    move v1, v3

    :goto_0
    sput-boolean v1, Lcom/android/providers/contacts/util/LoadCscFeatureUtils;->mEnableSupportFuzzySearch:Z

    .line 73
    const-string v1, "CscFeature_Contact_SeparateLookupTable4ChinaDialerSearch"

    invoke-virtual {v0, v1}, Lcom/sec/android/app/CscFeature;->getEnableStatus(Ljava/lang/String;)Z

    move-result v1

    sput-boolean v1, Lcom/android/providers/contacts/util/LoadCscFeatureUtils;->mEnableSupportChineseDigitNameSearch:Z

    .line 75
    const-string v1, "CscFeature_Contact_EnableMultiplePinyinSearch"

    invoke-virtual {v0, v1}, Lcom/sec/android/app/CscFeature;->getEnableStatus(Ljava/lang/String;)Z

    move-result v1

    sput-boolean v1, Lcom/android/providers/contacts/util/LoadCscFeatureUtils;->mEnableSupportMultiPinyinSearch:Z

    .line 77
    const-string v1, "CscFeature_Contact_DisableEmergencyGroup"

    invoke-virtual {v0, v1}, Lcom/sec/android/app/CscFeature;->getEnableStatus(Ljava/lang/String;)Z

    move-result v1

    sput-boolean v1, Lcom/android/providers/contacts/util/LoadCscFeatureUtils;->mDisableEmergencyGroup:Z

    .line 79
    const-string v1, "CscFeature_Contact_EnablePuzzySearch"

    invoke-virtual {v0, v1}, Lcom/sec/android/app/CscFeature;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    sput-object v1, Lcom/android/providers/contacts/util/LoadCscFeatureUtils;->mFuzzySearchType:Ljava/lang/String;

    .line 81
    const-string v1, "CscFeature_Contact_LimitNameLength"

    invoke-virtual {v0, v1}, Lcom/sec/android/app/CscFeature;->getInteger(Ljava/lang/String;)I

    move-result v1

    sput v1, Lcom/android/providers/contacts/util/LoadCscFeatureUtils;->mNameLengthLimit:I

    .line 83
    const-string v1, "CscFeature_Contact_EnableStrokeSortList"

    invoke-virtual {v0, v1}, Lcom/sec/android/app/CscFeature;->getEnableStatus(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_1

    const-string v1, "CscFeature_Contact_EnableBPMFSortList"

    invoke-virtual {v0, v1}, Lcom/sec/android/app/CscFeature;->getEnableStatus(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_2

    :cond_1
    move v2, v3

    :cond_2
    sput-boolean v2, Lcom/android/providers/contacts/util/LoadCscFeatureUtils;->mEnableStrokeBPMF:Z

    .line 87
    invoke-static {}, Landroid/telephony/PhoneNumberUtils;->getMinMatch()I

    move-result v1

    sput v1, Lcom/android/providers/contacts/util/LoadCscFeatureUtils;->mMinMatch:I

    .line 88
    const-string v1, "CscFeature_Contact_EnableClassifyContactsOnMap"

    invoke-virtual {v0, v1}, Lcom/sec/android/app/CscFeature;->getEnableStatus(Ljava/lang/String;)Z

    move-result v1

    sput-boolean v1, Lcom/android/providers/contacts/util/LoadCscFeatureUtils;->mEnableSmartContact:Z

    .line 90
    return-void

    :cond_3
    move v1, v2

    .line 69
    goto :goto_0
.end method


# virtual methods
.method public getAnrConfigValue()I
    .locals 4

    .prologue
    .line 136
    invoke-static {}, Lcom/sec/android/app/CscFeature;->getInstance()Lcom/sec/android/app/CscFeature;

    move-result-object v1

    const-string v2, "CscFeature_Contact_ANRConfig"

    invoke-virtual {v1, v2}, Lcom/sec/android/app/CscFeature;->getInteger(Ljava/lang/String;)I

    move-result v0

    .line 139
    .local v0, "count":I
    const-string v1, "LoadCscFeatureUtils"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "@@ anr config count : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 140
    return v0
.end method

.method public getDisableEmergencyGroup()Z
    .locals 1

    .prologue
    .line 117
    sget-boolean v0, Lcom/android/providers/contacts/util/LoadCscFeatureUtils;->mDisableEmergencyGroup:Z

    return v0
.end method

.method public getEnableMultiPinyinSearch()Z
    .locals 1

    .prologue
    .line 109
    sget-boolean v0, Lcom/android/providers/contacts/util/LoadCscFeatureUtils;->mEnableSupportMultiPinyinSearch:Z

    return v0
.end method

.method public getEnableSearchNameIgnoringCommonPrefix()Z
    .locals 1

    .prologue
    .line 97
    sget-boolean v0, Lcom/android/providers/contacts/util/LoadCscFeatureUtils;->mEnableSearchNameIgnoringCommonPrefix:Z

    return v0
.end method

.method public getEnableSmartContact()Z
    .locals 1

    .prologue
    .line 169
    sget-boolean v0, Lcom/android/providers/contacts/util/LoadCscFeatureUtils;->mEnableSmartContact:Z

    return v0
.end method

.method public getEnableSupportAAB()Z
    .locals 1

    .prologue
    .line 93
    sget-boolean v0, Lcom/android/providers/contacts/util/LoadCscFeatureUtils;->mEnableAAB:Z

    return v0
.end method

.method public getEnableSupportChineseDigitNameSearch()Z
    .locals 1

    .prologue
    .line 105
    sget-boolean v0, Lcom/android/providers/contacts/util/LoadCscFeatureUtils;->mEnableSupportChineseDigitNameSearch:Z

    return v0
.end method

.method public getEnableSupportFuzzySearch()Z
    .locals 1

    .prologue
    .line 101
    sget-boolean v0, Lcom/android/providers/contacts/util/LoadCscFeatureUtils;->mEnableSupportFuzzySearch:Z

    return v0
.end method

.method public getEnableSupportStrokeBPMF()Z
    .locals 1

    .prologue
    .line 158
    sget-boolean v0, Lcom/android/providers/contacts/util/LoadCscFeatureUtils;->mEnableStrokeBPMF:Z

    return v0
.end method

.method public getFuzzySearchType()Ljava/lang/String;
    .locals 1

    .prologue
    .line 150
    sget-object v0, Lcom/android/providers/contacts/util/LoadCscFeatureUtils;->mFuzzySearchType:Ljava/lang/String;

    return-object v0
.end method

.method public getMaxLinkCount()I
    .locals 4

    .prologue
    .line 144
    const/16 v0, 0xa

    .line 145
    .local v0, "count":I
    const-string v1, "LoadCscFeatureUtils"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "@@ max link : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 146
    return v0
.end method

.method public getMinMatch()I
    .locals 1

    .prologue
    .line 113
    sget v0, Lcom/android/providers/contacts/util/LoadCscFeatureUtils;->mMinMatch:I

    return v0
.end method

.method public getNameLengthLimit()I
    .locals 1

    .prologue
    .line 154
    sget v0, Lcom/android/providers/contacts/util/LoadCscFeatureUtils;->mNameLengthLimit:I

    return v0
.end method

.method public isDialerKeypadLookupSupport()Z
    .locals 1

    .prologue
    .line 164
    sget-boolean v0, Lcom/android/providers/contacts/util/LoadCscFeatureUtils;->mEnableSupportChineseDigitNameSearch:Z

    if-nez v0, :cond_0

    sget-boolean v0, Lcom/android/providers/contacts/util/LoadCscFeatureUtils;->mEnableSupportFuzzySearch:Z

    if-nez v0, :cond_0

    sget-boolean v0, Lcom/android/providers/contacts/util/LoadCscFeatureUtils;->mEnableSupportMultiPinyinSearch:Z

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isSim2DbEnabled()Z
    .locals 3

    .prologue
    .line 128
    invoke-static {}, Lcom/sec/android/app/CscFeature;->getInstance()Lcom/sec/android/app/CscFeature;

    move-result-object v1

    const-string v2, "CscFeature_Contact_EnableMenuMDN"

    invoke-virtual {v1, v2}, Lcom/sec/android/app/CscFeature;->getEnableStatus(Ljava/lang/String;)Z

    move-result v0

    .line 131
    .local v0, "isSim2DbEnabled":Z
    return v0
.end method

.method public isSimDbEnabled()Z
    .locals 3

    .prologue
    .line 121
    invoke-static {}, Lcom/sec/android/app/CscFeature;->getInstance()Lcom/sec/android/app/CscFeature;

    move-result-object v1

    const-string v2, "CscFeature_Contact_DisableSIMContacts"

    invoke-virtual {v1, v2}, Lcom/sec/android/app/CscFeature;->getEnableStatus(Ljava/lang/String;)Z

    move-result v0

    .line 124
    .local v0, "isSimDbDisabled":Z
    if-nez v0, :cond_0

    const/4 v1, 0x1

    :goto_0
    return v1

    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method
