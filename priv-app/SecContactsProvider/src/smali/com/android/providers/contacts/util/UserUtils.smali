.class public final Lcom/android/providers/contacts/util/UserUtils;
.super Ljava/lang/Object;
.source "UserUtils.java"


# static fields
.field public static final VERBOSE_LOGGING:Z


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 33
    const-string v0, "ContactsProvider"

    const/4 v1, 0x2

    invoke-static {v0, v1}, Landroid/util/secutil/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    sput-boolean v0, Lcom/android/providers/contacts/util/UserUtils;->VERBOSE_LOGGING:Z

    return-void
.end method

.method private constructor <init>()V
    .locals 0

    .prologue
    .line 35
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 36
    return-void
.end method

.method public static getCorpUserId(Landroid/content/Context;)I
    .locals 9
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    const/4 v5, -0x1

    .line 55
    invoke-static {p0}, Lcom/android/providers/contacts/util/UserUtils;->getUserManager(Landroid/content/Context;)Landroid/os/UserManager;

    move-result-object v4

    .line 56
    .local v4, "um":Landroid/os/UserManager;
    if-nez v4, :cond_1

    .line 57
    const-string v6, "ContactsProvider"

    const-string v7, "No user manager service found"

    invoke-static {v6, v7}, Landroid/util/secutil/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I

    .line 97
    :cond_0
    :goto_0
    return v5

    .line 61
    :cond_1
    invoke-virtual {v4}, Landroid/os/UserManager;->getUserHandle()I

    move-result v1

    .line 63
    .local v1, "myUser":I
    sget-boolean v6, Lcom/android/providers/contacts/util/UserUtils;->VERBOSE_LOGGING:Z

    if-eqz v6, :cond_2

    .line 64
    const-string v6, "ContactsProvider"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "getCorpUserId: myUser="

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Landroid/util/secutil/Log;->secV(Ljava/lang/String;Ljava/lang/String;)I

    .line 69
    :cond_2
    invoke-static {p0}, Lcom/android/providers/contacts/util/UserUtils;->getDevicePolicyManager(Landroid/content/Context;)Landroid/app/admin/DevicePolicyManager;

    move-result-object v6

    new-instance v7, Landroid/os/UserHandle;

    invoke-direct {v7, v1}, Landroid/os/UserHandle;-><init>(I)V

    invoke-virtual {v6, v7}, Landroid/app/admin/DevicePolicyManager;->getCrossProfileCallerIdDisabled(Landroid/os/UserHandle;)Z

    move-result v6

    if-eqz v6, :cond_3

    .line 71
    sget-boolean v6, Lcom/android/providers/contacts/util/UserUtils;->VERBOSE_LOGGING:Z

    if-eqz v6, :cond_0

    .line 72
    const-string v6, "ContactsProvider"

    const-string v7, "Enterprise caller-id disabled."

    invoke-static {v6, v7}, Landroid/util/secutil/Log;->secV(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 78
    :cond_3
    invoke-virtual {v4}, Landroid/os/UserManager;->getUsers()Ljava/util/List;

    move-result-object v6

    invoke-interface {v6}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, "i$":Ljava/util/Iterator;
    :cond_4
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v6

    if-eqz v6, :cond_6

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/content/pm/UserInfo;

    .line 79
    .local v3, "ui":Landroid/content/pm/UserInfo;
    invoke-virtual {v3}, Landroid/content/pm/UserInfo;->isManagedProfile()Z

    move-result v6

    if-eqz v6, :cond_4

    .line 82
    iget v6, v3, Landroid/content/pm/UserInfo;->id:I

    invoke-virtual {v4, v6}, Landroid/os/UserManager;->getProfileParent(I)Landroid/content/pm/UserInfo;

    move-result-object v2

    .line 83
    .local v2, "parent":Landroid/content/pm/UserInfo;
    if-eqz v2, :cond_4

    .line 87
    iget v6, v2, Landroid/content/pm/UserInfo;->id:I

    if-ne v6, v1, :cond_4

    invoke-virtual {v3}, Landroid/content/pm/UserInfo;->isKnoxWorkspace()Z

    move-result v6

    if-nez v6, :cond_4

    .line 88
    sget-boolean v5, Lcom/android/providers/contacts/util/UserUtils;->VERBOSE_LOGGING:Z

    if-eqz v5, :cond_5

    .line 89
    const-string v5, "ContactsProvider"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "Corp user="

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    iget v7, v3, Landroid/content/pm/UserInfo;->id:I

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Landroid/util/secutil/Log;->secV(Ljava/lang/String;Ljava/lang/String;)I

    .line 91
    :cond_5
    iget v5, v3, Landroid/content/pm/UserInfo;->id:I

    goto/16 :goto_0

    .line 94
    .end local v2    # "parent":Landroid/content/pm/UserInfo;
    .end local v3    # "ui":Landroid/content/pm/UserInfo;
    :cond_6
    sget-boolean v6, Lcom/android/providers/contacts/util/UserUtils;->VERBOSE_LOGGING:Z

    if-eqz v6, :cond_0

    .line 95
    const-string v6, "ContactsProvider"

    const-string v7, "Corp user not found."

    invoke-static {v6, v7}, Landroid/util/secutil/Log;->secV(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0
.end method

.method public static getCurrentUserHandle(Landroid/content/Context;)I
    .locals 1
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 47
    invoke-static {p0}, Lcom/android/providers/contacts/util/UserUtils;->getUserManager(Landroid/content/Context;)Landroid/os/UserManager;

    move-result-object v0

    invoke-virtual {v0}, Landroid/os/UserManager;->getUserHandle()I

    move-result v0

    return v0
.end method

.method private static getDevicePolicyManager(Landroid/content/Context;)Landroid/app/admin/DevicePolicyManager;
    .locals 1
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 43
    const-string v0, "device_policy"

    invoke-virtual {p0, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/admin/DevicePolicyManager;

    return-object v0
.end method

.method public static getPersonasInfo(Landroid/content/Context;)Ljava/util/List;
    .locals 5
    .param p0, "context"    # Landroid/content/Context;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            ")",
            "Ljava/util/List",
            "<",
            "Landroid/content/pm/UserInfo;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v1, 0x0

    const/4 v4, 0x0

    .line 103
    invoke-static {p0}, Lcom/android/providers/contacts/util/UserUtils;->getDevicePolicyManager(Landroid/content/Context;)Landroid/app/admin/DevicePolicyManager;

    move-result-object v2

    new-instance v3, Landroid/os/UserHandle;

    invoke-direct {v3, v4}, Landroid/os/UserHandle;-><init>(I)V

    invoke-virtual {v2, v3}, Landroid/app/admin/DevicePolicyManager;->getCrossProfileCallerIdDisabled(Landroid/os/UserHandle;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 105
    sget-boolean v2, Lcom/android/providers/contacts/util/UserUtils;->VERBOSE_LOGGING:Z

    if-eqz v2, :cond_0

    .line 106
    const-string v2, "ContactsProvider"

    const-string v3, "Enterprise caller-id disabled for KNOX."

    invoke-static {v2, v3}, Landroid/util/secutil/Log;->secV(Ljava/lang/String;Ljava/lang/String;)I

    .line 117
    :cond_0
    :goto_0
    return-object v1

    .line 110
    :cond_1
    invoke-static {p0}, Lcom/android/providers/contacts/util/UserUtils;->getUserManager(Landroid/content/Context;)Landroid/os/UserManager;

    move-result-object v0

    .line 112
    .local v0, "um":Landroid/os/UserManager;
    if-nez v0, :cond_2

    .line 113
    const-string v2, "ContactsProvider"

    const-string v3, "No user manager service found for KNOX"

    invoke-static {v2, v3}, Landroid/util/secutil/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 117
    :cond_2
    invoke-virtual {v0, v4}, Landroid/os/UserManager;->getProfiles(I)Ljava/util/List;

    move-result-object v1

    goto :goto_0
.end method

.method public static getUserManager(Landroid/content/Context;)Landroid/os/UserManager;
    .locals 1
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 39
    const-string v0, "user"

    invoke-virtual {p0, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/os/UserManager;

    return-object v0
.end method
