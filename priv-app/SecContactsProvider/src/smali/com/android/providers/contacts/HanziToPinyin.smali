.class public Lcom/android/providers/contacts/HanziToPinyin;
.super Ljava/lang/Object;
.source "HanziToPinyin.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/android/providers/contacts/HanziToPinyin$Token;
    }
.end annotation


# static fields
.field private static final PINYIN_LV:[C

.field private static final PINYIN_LVE:[C

.field private static final PINYIN_NV:[C

.field private static final PINYIN_NVE:[C

.field private static final POLYPHONE_PRIOR_PINYIN:[C

.field private static final POLYPHONE_PRIOR_PINYIN_STRING:[Ljava/lang/String;

.field private static sInstance:Lcom/android/providers/contacts/HanziToPinyin;


# instance fields
.field private final PINYIN_LVE_STR:Ljava/lang/String;

.field private final PINYIN_LV_STR:Ljava/lang/String;

.field private final PINYIN_NVE_STR:Ljava/lang/String;

.field private final PINYIN_NV_STR:Ljava/lang/String;

.field private mAsciiTransliterator:Llibcore/icu/Transliterator;

.field private mPinyinTransliterator:Llibcore/icu/Transliterator;


# direct methods
.method static constructor <clinit>()V
    .locals 6

    .prologue
    const/16 v1, 0x1a

    const/16 v5, 0xd

    const/16 v4, 0xb

    const/4 v3, 0x5

    .line 44
    const/16 v0, 0x39

    new-array v0, v0, [C

    fill-array-data v0, :array_0

    sput-object v0, Lcom/android/providers/contacts/HanziToPinyin;->PINYIN_LV:[C

    .line 55
    new-array v0, v5, [C

    fill-array-data v0, :array_1

    sput-object v0, Lcom/android/providers/contacts/HanziToPinyin;->PINYIN_LVE:[C

    .line 60
    new-array v0, v4, [C

    fill-array-data v0, :array_2

    sput-object v0, Lcom/android/providers/contacts/HanziToPinyin;->PINYIN_NV:[C

    .line 65
    new-array v0, v3, [C

    fill-array-data v0, :array_3

    sput-object v0, Lcom/android/providers/contacts/HanziToPinyin;->PINYIN_NVE:[C

    .line 81
    new-array v0, v1, [C

    fill-array-data v0, :array_4

    sput-object v0, Lcom/android/providers/contacts/HanziToPinyin;->POLYPHONE_PRIOR_PINYIN:[C

    .line 90
    new-array v0, v1, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string v2, "HE"

    aput-object v2, v0, v1

    const/4 v1, 0x1

    const-string v2, "BO"

    aput-object v2, v0, v1

    const/4 v1, 0x2

    const-string v2, "ZHA"

    aput-object v2, v0, v1

    const/4 v1, 0x3

    const-string v2, "CHANG"

    aput-object v2, v0, v1

    const/4 v1, 0x4

    const-string v2, "QIU"

    aput-object v2, v0, v1

    const-string v1, "SHAN"

    aput-object v1, v0, v3

    const/4 v1, 0x6

    const-string v2, "ZHAI"

    aput-object v2, v0, v1

    const/4 v1, 0x7

    const-string v2, "KAN"

    aput-object v2, v0, v1

    const/16 v1, 0x8

    const-string v2, "JIA"

    aput-object v2, v0, v1

    const/16 v1, 0x9

    const-string v2, "XIE"

    aput-object v2, v0, v1

    const/16 v1, 0xa

    const-string v2, "WEI"

    aput-object v2, v0, v1

    const-string v1, "MO"

    aput-object v1, v0, v4

    const/16 v1, 0xc

    const-string v2, "MO"

    aput-object v2, v0, v1

    const-string v1, "EN"

    aput-object v1, v0, v5

    const/16 v1, 0xe

    const-string v2, "PIAO"

    aput-object v2, v0, v1

    const/16 v1, 0xf

    const-string v2, "OU"

    aput-object v2, v0, v1

    const/16 v1, 0x10

    const-string v2, "CHONG"

    aput-object v2, v0, v1

    const/16 v1, 0x11

    const-string v2, "SHE"

    aput-object v2, v0, v1

    const/16 v1, 0x12

    const-string v2, "XIE"

    aput-object v2, v0, v1

    const/16 v1, 0x13

    const-string v2, "BI"

    aput-object v2, v0, v1

    const/16 v1, 0x14

    const-string v2, "SHEN"

    aput-object v2, v0, v1

    const/16 v1, 0x15

    const-string v2, "YE"

    aput-object v2, v0, v1

    const/16 v1, 0x16

    const-string v2, "FU"

    aput-object v2, v0, v1

    const/16 v1, 0x17

    const-string v2, "SHEN"

    aput-object v2, v0, v1

    const/16 v1, 0x18

    const-string v2, "CHONG"

    aput-object v2, v0, v1

    const/16 v1, 0x19

    const-string v2, "XI"

    aput-object v2, v0, v1

    sput-object v0, Lcom/android/providers/contacts/HanziToPinyin;->POLYPHONE_PRIOR_PINYIN_STRING:[Ljava/lang/String;

    return-void

    .line 44
    :array_0
    .array-data 2
        -0x6f28s
        0x52f4s
        0x4fa3s
        0x4fb6s
        0x507bs
        0x50c2s
        0x5122s
        -0x7bb2s
        -0x7a28s
        0x5f8bs
        0x5415s
        0x5442s
        -0x658cs
        -0x6a02s
        0x5d42s
        0x5c61s
        0x5c62s
        0x5c65s
        0x6314s
        0x635bs
        0x634bs
        0x6ee4s
        0x6ffes
        0x7effs
        0x7f15s
        0x578fs
        0x65c5s
        0x7208s
        0x6c00s
        0x68a0s
        0x6988s
        0x6adas
        0x6ad6s
        0x6c2fs
        0x7963s
        0x616es
        -0x7e7es
        -0x7e5es
        -0x7e61s
        0x7a06s
        0x7a6ds
        -0x6b23s
        0x7387s
        -0x76e5s
        -0x76c8s
        -0x79afs
        -0x7e70s
        0x7d7ds
        0x7da0s
        0x7dd1s
        0x7e37s
        0x7e42s
        0x7bbbs
        -0x6d3fs
        -0x6b9es
        -0x6a53s
        -0x659es
    .end array-data

    .line 55
    nop

    :array_1
    .array-data 2
        0x7565s
        0x63a0s
        -0x6af6s
        0x7567s
        0x64fds
        -0x6d1es
        0x5719s
        0x7a24s
        -0x7585s
        0x5bfds
        0x5260s
        -0x6d23s
        -0x6d23s
    .end array-data

    .line 60
    nop

    :array_2
    .array-data 2
        0x5973s
        -0x7fb1s
        0x6c91s
        -0x6e07s
        0x6712s
        -0x77bes
        -0x77bcs
        0x7c79s
        0x6067s
        -0x6b6bs
        -0x6e07s
    .end array-data

    .line 65
    nop

    :array_3
    .array-data 2
        -0x79b0s
        0x759fs
        0x5a69s
        0x7878s
        0x7627s
    .end array-data

    .line 81
    nop

    :array_4
    .array-data 2
        0x5475s
        -0x7a7cs
        0x67e5s
        -0x6a81s
        0x4ec7s
        0x5355s
        0x7fdfs
        -0x69e6s
        -0x72c2s
        -0x761ds
        -0x6969s
        0x5b37s
        0x5b24s
        0x55efs
        0x6734s
        0x533as
        -0x6e33s
        0x6298s
        -0x6777s
        0x79d8s
        -0x7c68s
        0x5c09s
        0x5b93s
        -0x73f4s
        0x79cds
        -0x71b6s
    .end array-data
.end method

.method private constructor <init>()V
    .locals 3

    .prologue
    .line 133
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 75
    const-string v1, "NV"

    iput-object v1, p0, Lcom/android/providers/contacts/HanziToPinyin;->PINYIN_NV_STR:Ljava/lang/String;

    .line 76
    const-string v1, "NVE"

    iput-object v1, p0, Lcom/android/providers/contacts/HanziToPinyin;->PINYIN_NVE_STR:Ljava/lang/String;

    .line 77
    const-string v1, "LV"

    iput-object v1, p0, Lcom/android/providers/contacts/HanziToPinyin;->PINYIN_LV_STR:Ljava/lang/String;

    .line 78
    const-string v1, "LVE"

    iput-object v1, p0, Lcom/android/providers/contacts/HanziToPinyin;->PINYIN_LVE_STR:Ljava/lang/String;

    .line 135
    :try_start_0
    new-instance v1, Llibcore/icu/Transliterator;

    const-string v2, "Han-Latin/Names; Latin-Ascii; Any-Upper"

    invoke-direct {v1, v2}, Llibcore/icu/Transliterator;-><init>(Ljava/lang/String;)V

    iput-object v1, p0, Lcom/android/providers/contacts/HanziToPinyin;->mPinyinTransliterator:Llibcore/icu/Transliterator;

    .line 136
    new-instance v1, Llibcore/icu/Transliterator;

    const-string v2, "Latin-Ascii"

    invoke-direct {v1, v2}, Llibcore/icu/Transliterator;-><init>(Ljava/lang/String;)V

    iput-object v1, p0, Lcom/android/providers/contacts/HanziToPinyin;->mAsciiTransliterator:Llibcore/icu/Transliterator;
    :try_end_0
    .catch Ljava/lang/RuntimeException; {:try_start_0 .. :try_end_0} :catch_0

    .line 141
    :goto_0
    return-void

    .line 137
    :catch_0
    move-exception v0

    .line 138
    .local v0, "e":Ljava/lang/RuntimeException;
    const-string v1, "HanziToPinyin"

    const-string v2, "Han-Latin/Names transliterator data is missing, HanziToPinyin is disabled"

    invoke-static {v1, v2}, Landroid/util/secutil/Log;->secW(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method private addToken(Ljava/lang/StringBuilder;Ljava/util/ArrayList;I)V
    .locals 2
    .param p1, "sb"    # Ljava/lang/StringBuilder;
    .param p3, "tokenType"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/StringBuilder;",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/android/providers/contacts/HanziToPinyin$Token;",
            ">;I)V"
        }
    .end annotation

    .prologue
    .line 302
    .local p2, "tokens":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/android/providers/contacts/HanziToPinyin$Token;>;"
    invoke-virtual {p1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 303
    .local v0, "str":Ljava/lang/String;
    new-instance v1, Lcom/android/providers/contacts/HanziToPinyin$Token;

    invoke-direct {v1, p3, v0, v0}, Lcom/android/providers/contacts/HanziToPinyin$Token;-><init>(ILjava/lang/String;Ljava/lang/String;)V

    invoke-virtual {p2, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 304
    const/4 v1, 0x0

    invoke-virtual {p1, v1}, Ljava/lang/StringBuilder;->setLength(I)V

    .line 305
    return-void
.end method

.method public static getInstance()Lcom/android/providers/contacts/HanziToPinyin;
    .locals 2

    .prologue
    .line 148
    const-class v1, Lcom/android/providers/contacts/HanziToPinyin;

    monitor-enter v1

    .line 149
    :try_start_0
    sget-object v0, Lcom/android/providers/contacts/HanziToPinyin;->sInstance:Lcom/android/providers/contacts/HanziToPinyin;

    if-nez v0, :cond_0

    .line 150
    new-instance v0, Lcom/android/providers/contacts/HanziToPinyin;

    invoke-direct {v0}, Lcom/android/providers/contacts/HanziToPinyin;-><init>()V

    sput-object v0, Lcom/android/providers/contacts/HanziToPinyin;->sInstance:Lcom/android/providers/contacts/HanziToPinyin;

    .line 152
    :cond_0
    sget-object v0, Lcom/android/providers/contacts/HanziToPinyin;->sInstance:Lcom/android/providers/contacts/HanziToPinyin;

    monitor-exit v1

    return-object v0

    .line 153
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method private getPolyPinyin(C)Ljava/lang/String;
    .locals 7
    .param p1, "character"    # C

    .prologue
    .line 362
    invoke-static {p1}, Ljava/lang/Character;->toString(C)Ljava/lang/String;

    move-result-object v2

    .line 363
    .local v2, "name":Ljava/lang/String;
    const/4 v4, 0x0

    .line 364
    .local v4, "polyPinyin":Ljava/lang/String;
    const/4 v3, -0x1

    .line 365
    .local v3, "offset":I
    sget-object v6, Lcom/android/providers/contacts/HanziToPinyin;->POLYPHONE_PRIOR_PINYIN:[C

    array-length v0, v6

    .line 367
    .local v0, "arrayLen":I
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    if-ge v1, v0, :cond_0

    .line 368
    sget-object v6, Lcom/android/providers/contacts/HanziToPinyin;->POLYPHONE_PRIOR_PINYIN:[C

    aget-char v6, v6, v1

    invoke-static {v6}, Ljava/lang/Character;->toString(C)Ljava/lang/String;

    move-result-object v5

    .line 369
    .local v5, "sample":Ljava/lang/String;
    invoke-virtual {v2, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_2

    .line 370
    move v3, v1

    .line 375
    .end local v5    # "sample":Ljava/lang/String;
    :cond_0
    const/4 v6, -0x1

    if-le v3, v6, :cond_1

    .line 376
    sget-object v6, Lcom/android/providers/contacts/HanziToPinyin;->POLYPHONE_PRIOR_PINYIN_STRING:[Ljava/lang/String;

    aget-object v4, v6, v3

    .line 379
    :cond_1
    return-object v4

    .line 367
    .restart local v5    # "sample":Ljava/lang/String;
    :cond_2
    add-int/lit8 v1, v1, 0x1

    goto :goto_0
.end method

.method private pinyinSuffixType(C)I
    .locals 12
    .param p1, "character"    # C

    .prologue
    .line 313
    const/4 v6, 0x0

    .line 314
    .local v6, "matched":Z
    const/4 v9, 0x0

    .line 315
    .local v9, "result":I
    invoke-static {p1}, Ljava/lang/Character;->toString(C)Ljava/lang/String;

    move-result-object v3

    .line 317
    .local v3, "letter":Ljava/lang/String;
    sget-object v0, Lcom/android/providers/contacts/HanziToPinyin;->PINYIN_LV:[C

    .local v0, "arr$":[C
    array-length v2, v0

    .local v2, "len$":I
    const/4 v1, 0x0

    .local v1, "i$":I
    :goto_0
    if-ge v1, v2, :cond_0

    aget-char v4, v0, v1

    .line 318
    .local v4, "lv":C
    invoke-static {v4}, Ljava/lang/Character;->toString(C)Ljava/lang/String;

    move-result-object v10

    .line 319
    .local v10, "sample":Ljava/lang/String;
    invoke-virtual {v3, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v11

    if-eqz v11, :cond_4

    .line 320
    const/4 v9, 0x1

    .line 321
    const/4 v6, 0x1

    .line 326
    .end local v4    # "lv":C
    .end local v10    # "sample":Ljava/lang/String;
    :cond_0
    if-nez v6, :cond_1

    .line 327
    sget-object v0, Lcom/android/providers/contacts/HanziToPinyin;->PINYIN_LVE:[C

    array-length v2, v0

    const/4 v1, 0x0

    :goto_1
    if-ge v1, v2, :cond_1

    aget-char v5, v0, v1

    .line 328
    .local v5, "lve":C
    invoke-static {v5}, Ljava/lang/Character;->toString(C)Ljava/lang/String;

    move-result-object v10

    .line 329
    .restart local v10    # "sample":Ljava/lang/String;
    invoke-virtual {v3, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v11

    if-eqz v11, :cond_5

    .line 330
    const/4 v9, 0x2

    .line 331
    const/4 v6, 0x1

    .line 337
    .end local v5    # "lve":C
    .end local v10    # "sample":Ljava/lang/String;
    :cond_1
    if-nez v6, :cond_2

    .line 338
    sget-object v0, Lcom/android/providers/contacts/HanziToPinyin;->PINYIN_NV:[C

    array-length v2, v0

    const/4 v1, 0x0

    :goto_2
    if-ge v1, v2, :cond_2

    aget-char v7, v0, v1

    .line 339
    .local v7, "nv":C
    invoke-static {v7}, Ljava/lang/Character;->toString(C)Ljava/lang/String;

    move-result-object v10

    .line 340
    .restart local v10    # "sample":Ljava/lang/String;
    invoke-virtual {v3, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v11

    if-eqz v11, :cond_6

    .line 341
    const/4 v9, 0x3

    .line 342
    const/4 v6, 0x1

    .line 348
    .end local v7    # "nv":C
    .end local v10    # "sample":Ljava/lang/String;
    :cond_2
    if-nez v6, :cond_3

    .line 349
    sget-object v0, Lcom/android/providers/contacts/HanziToPinyin;->PINYIN_NVE:[C

    array-length v2, v0

    const/4 v1, 0x0

    :goto_3
    if-ge v1, v2, :cond_3

    aget-char v8, v0, v1

    .line 350
    .local v8, "nve":C
    invoke-static {v8}, Ljava/lang/Character;->toString(C)Ljava/lang/String;

    move-result-object v10

    .line 351
    .restart local v10    # "sample":Ljava/lang/String;
    invoke-virtual {v3, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v11

    if-eqz v11, :cond_7

    .line 352
    const/4 v9, 0x4

    .line 353
    const/4 v6, 0x1

    .line 358
    .end local v8    # "nve":C
    .end local v10    # "sample":Ljava/lang/String;
    :cond_3
    return v9

    .line 317
    .restart local v4    # "lv":C
    .restart local v10    # "sample":Ljava/lang/String;
    :cond_4
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 327
    .end local v4    # "lv":C
    .restart local v5    # "lve":C
    :cond_5
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 338
    .end local v5    # "lve":C
    .restart local v7    # "nv":C
    :cond_6
    add-int/lit8 v1, v1, 0x1

    goto :goto_2

    .line 349
    .end local v7    # "nv":C
    .restart local v8    # "nve":C
    :cond_7
    add-int/lit8 v1, v1, 0x1

    goto :goto_3
.end method

.method private tokenize(CLcom/android/providers/contacts/HanziToPinyin$Token;)V
    .locals 2
    .param p1, "character"    # C
    .param p2, "token"    # Lcom/android/providers/contacts/HanziToPinyin$Token;

    .prologue
    const/4 v1, 0x1

    .line 157
    invoke-static {p1}, Ljava/lang/Character;->toString(C)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p2, Lcom/android/providers/contacts/HanziToPinyin$Token;->source:Ljava/lang/String;

    .line 160
    const/16 v0, 0x80

    if-ge p1, v0, :cond_1

    .line 161
    iput v1, p2, Lcom/android/providers/contacts/HanziToPinyin$Token;->type:I

    .line 162
    iget-object v0, p2, Lcom/android/providers/contacts/HanziToPinyin$Token;->source:Ljava/lang/String;

    iput-object v0, p2, Lcom/android/providers/contacts/HanziToPinyin$Token;->target:Ljava/lang/String;

    .line 181
    :cond_0
    :goto_0
    return-void

    .line 167
    :cond_1
    const/16 v0, 0x250

    if-lt p1, v0, :cond_2

    const/16 v0, 0x1e00

    if-gt v0, p1, :cond_4

    const/16 v0, 0x1eff

    if-ge p1, v0, :cond_4

    .line 168
    :cond_2
    iput v1, p2, Lcom/android/providers/contacts/HanziToPinyin$Token;->type:I

    .line 169
    iget-object v0, p0, Lcom/android/providers/contacts/HanziToPinyin;->mAsciiTransliterator:Llibcore/icu/Transliterator;

    if-nez v0, :cond_3

    iget-object v0, p2, Lcom/android/providers/contacts/HanziToPinyin$Token;->source:Ljava/lang/String;

    :goto_1
    iput-object v0, p2, Lcom/android/providers/contacts/HanziToPinyin$Token;->target:Ljava/lang/String;

    goto :goto_0

    :cond_3
    iget-object v0, p0, Lcom/android/providers/contacts/HanziToPinyin;->mAsciiTransliterator:Llibcore/icu/Transliterator;

    iget-object v1, p2, Lcom/android/providers/contacts/HanziToPinyin$Token;->source:Ljava/lang/String;

    invoke-virtual {v0, v1}, Llibcore/icu/Transliterator;->transliterate(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    goto :goto_1

    .line 174
    :cond_4
    const/4 v0, 0x2

    iput v0, p2, Lcom/android/providers/contacts/HanziToPinyin$Token;->type:I

    .line 175
    iget-object v0, p0, Lcom/android/providers/contacts/HanziToPinyin;->mPinyinTransliterator:Llibcore/icu/Transliterator;

    iget-object v1, p2, Lcom/android/providers/contacts/HanziToPinyin$Token;->source:Ljava/lang/String;

    invoke-virtual {v0, v1}, Llibcore/icu/Transliterator;->transliterate(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p2, Lcom/android/providers/contacts/HanziToPinyin$Token;->target:Ljava/lang/String;

    .line 176
    iget-object v0, p2, Lcom/android/providers/contacts/HanziToPinyin$Token;->target:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_5

    iget-object v0, p2, Lcom/android/providers/contacts/HanziToPinyin$Token;->source:Ljava/lang/String;

    iget-object v1, p2, Lcom/android/providers/contacts/HanziToPinyin$Token;->target:Ljava/lang/String;

    invoke-static {v0, v1}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 178
    :cond_5
    const/4 v0, 0x3

    iput v0, p2, Lcom/android/providers/contacts/HanziToPinyin$Token;->type:I

    .line 179
    iget-object v0, p2, Lcom/android/providers/contacts/HanziToPinyin$Token;->source:Ljava/lang/String;

    iput-object v0, p2, Lcom/android/providers/contacts/HanziToPinyin$Token;->target:Ljava/lang/String;

    goto :goto_0
.end method


# virtual methods
.method public getTokens(Ljava/lang/String;)Ljava/util/ArrayList;
    .locals 9
    .param p1, "input"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/android/providers/contacts/HanziToPinyin$Token;",
            ">;"
        }
    .end annotation

    .prologue
    .line 252
    new-instance v6, Ljava/util/ArrayList;

    invoke-direct {v6}, Ljava/util/ArrayList;-><init>()V

    .line 253
    .local v6, "tokens":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/android/providers/contacts/HanziToPinyin$Token;>;"
    invoke-virtual {p0}, Lcom/android/providers/contacts/HanziToPinyin;->hasChineseTransliterator()Z

    move-result v7

    if-eqz v7, :cond_0

    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v7

    if-eqz v7, :cond_1

    .line 297
    :cond_0
    :goto_0
    return-object v6

    .line 258
    :cond_1
    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v2

    .line 259
    .local v2, "inputLength":I
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    .line 260
    .local v3, "sb":Ljava/lang/StringBuilder;
    const/4 v5, 0x1

    .line 261
    .local v5, "tokenType":I
    new-instance v4, Lcom/android/providers/contacts/HanziToPinyin$Token;

    invoke-direct {v4}, Lcom/android/providers/contacts/HanziToPinyin$Token;-><init>()V

    .line 267
    .local v4, "token":Lcom/android/providers/contacts/HanziToPinyin$Token;
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_1
    if-ge v1, v2, :cond_8

    .line 268
    invoke-virtual {p1, v1}, Ljava/lang/String;->charAt(I)C

    move-result v0

    .line 269
    .local v0, "character":C
    invoke-static {v0}, Ljava/lang/Character;->isSpaceChar(C)Z

    move-result v7

    if-eqz v7, :cond_3

    .line 270
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->length()I

    move-result v7

    if-lez v7, :cond_2

    .line 271
    invoke-direct {p0, v3, v6, v5}, Lcom/android/providers/contacts/HanziToPinyin;->addToken(Ljava/lang/StringBuilder;Ljava/util/ArrayList;I)V

    .line 267
    :cond_2
    :goto_2
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 274
    :cond_3
    invoke-static {}, Lcom/android/providers/contacts/util/LoadCscFeatureUtils;->getInstance()Lcom/android/providers/contacts/util/LoadCscFeatureUtils;

    move-result-object v7

    invoke-virtual {v7}, Lcom/android/providers/contacts/util/LoadCscFeatureUtils;->getEnableSupportFuzzySearch()Z

    move-result v7

    if-eqz v7, :cond_5

    .line 275
    invoke-virtual {p0, v0, v4}, Lcom/android/providers/contacts/HanziToPinyin;->tokenizeForSpecialSuffix(CLcom/android/providers/contacts/HanziToPinyin$Token;)V

    .line 279
    :goto_3
    iget v7, v4, Lcom/android/providers/contacts/HanziToPinyin$Token;->type:I

    const/4 v8, 0x2

    if-ne v7, v8, :cond_6

    .line 280
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->length()I

    move-result v7

    if-lez v7, :cond_4

    .line 281
    invoke-direct {p0, v3, v6, v5}, Lcom/android/providers/contacts/HanziToPinyin;->addToken(Ljava/lang/StringBuilder;Ljava/util/ArrayList;I)V

    .line 283
    :cond_4
    invoke-virtual {v6, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 284
    new-instance v4, Lcom/android/providers/contacts/HanziToPinyin$Token;

    .end local v4    # "token":Lcom/android/providers/contacts/HanziToPinyin$Token;
    invoke-direct {v4}, Lcom/android/providers/contacts/HanziToPinyin$Token;-><init>()V

    .line 291
    .restart local v4    # "token":Lcom/android/providers/contacts/HanziToPinyin$Token;
    :goto_4
    iget v5, v4, Lcom/android/providers/contacts/HanziToPinyin$Token;->type:I

    goto :goto_2

    .line 277
    :cond_5
    invoke-direct {p0, v0, v4}, Lcom/android/providers/contacts/HanziToPinyin;->tokenize(CLcom/android/providers/contacts/HanziToPinyin$Token;)V

    goto :goto_3

    .line 286
    :cond_6
    iget v7, v4, Lcom/android/providers/contacts/HanziToPinyin$Token;->type:I

    if-eq v5, v7, :cond_7

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->length()I

    move-result v7

    if-lez v7, :cond_7

    .line 287
    invoke-direct {p0, v3, v6, v5}, Lcom/android/providers/contacts/HanziToPinyin;->addToken(Ljava/lang/StringBuilder;Ljava/util/ArrayList;I)V

    .line 289
    :cond_7
    iget-object v7, v4, Lcom/android/providers/contacts/HanziToPinyin$Token;->target:Ljava/lang/String;

    invoke-virtual {v3, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_4

    .line 294
    .end local v0    # "character":C
    :cond_8
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->length()I

    move-result v7

    if-lez v7, :cond_0

    .line 295
    invoke-direct {p0, v3, v6, v5}, Lcom/android/providers/contacts/HanziToPinyin;->addToken(Ljava/lang/StringBuilder;Ljava/util/ArrayList;I)V

    goto :goto_0
.end method

.method public hasChineseTransliterator()Z
    .locals 1

    .prologue
    .line 144
    iget-object v0, p0, Lcom/android/providers/contacts/HanziToPinyin;->mPinyinTransliterator:Llibcore/icu/Transliterator;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected tokenizeForSpecialSuffix(CLcom/android/providers/contacts/HanziToPinyin$Token;)V
    .locals 5
    .param p1, "character"    # C
    .param p2, "token"    # Lcom/android/providers/contacts/HanziToPinyin$Token;

    .prologue
    const/4 v4, 0x1

    const/4 v3, 0x2

    .line 184
    invoke-static {p1}, Ljava/lang/Character;->toString(C)Ljava/lang/String;

    move-result-object v2

    iput-object v2, p2, Lcom/android/providers/contacts/HanziToPinyin$Token;->source:Ljava/lang/String;

    .line 187
    const/16 v2, 0x80

    if-ge p1, v2, :cond_1

    .line 188
    iput v4, p2, Lcom/android/providers/contacts/HanziToPinyin$Token;->type:I

    .line 189
    iget-object v2, p2, Lcom/android/providers/contacts/HanziToPinyin$Token;->source:Ljava/lang/String;

    iput-object v2, p2, Lcom/android/providers/contacts/HanziToPinyin$Token;->target:Ljava/lang/String;

    .line 237
    :cond_0
    :goto_0
    return-void

    .line 194
    :cond_1
    const/16 v2, 0x250

    if-lt p1, v2, :cond_2

    const/16 v2, 0x1e00

    if-gt v2, p1, :cond_4

    const/16 v2, 0x1eff

    if-ge p1, v2, :cond_4

    .line 195
    :cond_2
    iput v4, p2, Lcom/android/providers/contacts/HanziToPinyin$Token;->type:I

    .line 196
    iget-object v2, p0, Lcom/android/providers/contacts/HanziToPinyin;->mAsciiTransliterator:Llibcore/icu/Transliterator;

    if-nez v2, :cond_3

    iget-object v2, p2, Lcom/android/providers/contacts/HanziToPinyin$Token;->source:Ljava/lang/String;

    :goto_1
    iput-object v2, p2, Lcom/android/providers/contacts/HanziToPinyin$Token;->target:Ljava/lang/String;

    goto :goto_0

    :cond_3
    iget-object v2, p0, Lcom/android/providers/contacts/HanziToPinyin;->mAsciiTransliterator:Llibcore/icu/Transliterator;

    iget-object v3, p2, Lcom/android/providers/contacts/HanziToPinyin$Token;->source:Ljava/lang/String;

    invoke-virtual {v2, v3}, Llibcore/icu/Transliterator;->transliterate(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    goto :goto_1

    .line 201
    :cond_4
    iput v3, p2, Lcom/android/providers/contacts/HanziToPinyin$Token;->type:I

    .line 202
    invoke-direct {p0, p1}, Lcom/android/providers/contacts/HanziToPinyin;->pinyinSuffixType(C)I

    move-result v1

    .line 203
    .local v1, "suffixType":I
    packed-switch v1, :pswitch_data_0

    .line 228
    invoke-direct {p0, p1}, Lcom/android/providers/contacts/HanziToPinyin;->getPolyPinyin(C)Ljava/lang/String;

    move-result-object v0

    .line 229
    .local v0, "polyPinyin":Ljava/lang/String;
    if-eqz v0, :cond_6

    .end local v0    # "polyPinyin":Ljava/lang/String;
    :goto_2
    iput-object v0, p2, Lcom/android/providers/contacts/HanziToPinyin$Token;->target:Ljava/lang/String;

    .line 232
    iget-object v2, p2, Lcom/android/providers/contacts/HanziToPinyin$Token;->target:Ljava/lang/String;

    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_5

    iget-object v2, p2, Lcom/android/providers/contacts/HanziToPinyin$Token;->source:Ljava/lang/String;

    iget-object v3, p2, Lcom/android/providers/contacts/HanziToPinyin$Token;->target:Ljava/lang/String;

    invoke-static {v2, v3}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 234
    :cond_5
    const/4 v2, 0x3

    iput v2, p2, Lcom/android/providers/contacts/HanziToPinyin$Token;->type:I

    .line 235
    iget-object v2, p2, Lcom/android/providers/contacts/HanziToPinyin$Token;->source:Ljava/lang/String;

    iput-object v2, p2, Lcom/android/providers/contacts/HanziToPinyin$Token;->target:Ljava/lang/String;

    goto :goto_0

    .line 205
    :pswitch_0
    iput v3, p2, Lcom/android/providers/contacts/HanziToPinyin$Token;->type:I

    .line 206
    const-string v2, "LV"

    iput-object v2, p2, Lcom/android/providers/contacts/HanziToPinyin$Token;->target:Ljava/lang/String;

    goto :goto_0

    .line 210
    :pswitch_1
    iput v3, p2, Lcom/android/providers/contacts/HanziToPinyin$Token;->type:I

    .line 211
    const-string v2, "LVE"

    iput-object v2, p2, Lcom/android/providers/contacts/HanziToPinyin$Token;->target:Ljava/lang/String;

    goto :goto_0

    .line 215
    :pswitch_2
    iput v3, p2, Lcom/android/providers/contacts/HanziToPinyin$Token;->type:I

    .line 216
    const-string v2, "NV"

    iput-object v2, p2, Lcom/android/providers/contacts/HanziToPinyin$Token;->target:Ljava/lang/String;

    goto :goto_0

    .line 220
    :pswitch_3
    iput v3, p2, Lcom/android/providers/contacts/HanziToPinyin$Token;->type:I

    .line 221
    const-string v2, "NVE"

    iput-object v2, p2, Lcom/android/providers/contacts/HanziToPinyin$Token;->target:Ljava/lang/String;

    goto :goto_0

    .line 229
    .restart local v0    # "polyPinyin":Ljava/lang/String;
    :cond_6
    iget-object v2, p0, Lcom/android/providers/contacts/HanziToPinyin;->mPinyinTransliterator:Llibcore/icu/Transliterator;

    iget-object v3, p2, Lcom/android/providers/contacts/HanziToPinyin$Token;->source:Ljava/lang/String;

    invoke-virtual {v2, v3}, Llibcore/icu/Transliterator;->transliterate(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    goto :goto_2

    .line 203
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method public transliterate(Ljava/lang/String;)Ljava/lang/String;
    .locals 1
    .param p1, "input"    # Ljava/lang/String;

    .prologue
    .line 240
    invoke-virtual {p0}, Lcom/android/providers/contacts/HanziToPinyin;->hasChineseTransliterator()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 241
    :cond_0
    const/4 v0, 0x0

    .line 243
    :goto_0
    return-object v0

    :cond_1
    iget-object v0, p0, Lcom/android/providers/contacts/HanziToPinyin;->mPinyinTransliterator:Llibcore/icu/Transliterator;

    invoke-virtual {v0, p1}, Llibcore/icu/Transliterator;->transliterate(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method
