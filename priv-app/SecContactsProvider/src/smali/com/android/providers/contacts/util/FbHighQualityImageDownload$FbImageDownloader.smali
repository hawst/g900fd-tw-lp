.class public Lcom/android/providers/contacts/util/FbHighQualityImageDownload$FbImageDownloader;
.super Ljava/lang/Object;
.source "FbHighQualityImageDownload.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/providers/contacts/util/FbHighQualityImageDownload;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1
    name = "FbImageDownloader"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/providers/contacts/util/FbHighQualityImageDownload;


# direct methods
.method public constructor <init>(Lcom/android/providers/contacts/util/FbHighQualityImageDownload;)V
    .locals 0

    .prologue
    .line 458
    iput-object p1, p0, Lcom/android/providers/contacts/util/FbHighQualityImageDownload$FbImageDownloader;->this$0:Lcom/android/providers/contacts/util/FbHighQualityImageDownload;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private getByteArray(Landroid/graphics/Bitmap;I)[B
    .locals 4
    .param p1, "bitmap"    # Landroid/graphics/Bitmap;
    .param p2, "quality"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 493
    new-instance v0, Ljava/io/ByteArrayOutputStream;

    invoke-direct {v0}, Ljava/io/ByteArrayOutputStream;-><init>()V

    .line 494
    .local v0, "bos":Ljava/io/ByteArrayOutputStream;
    sget-object v2, Landroid/graphics/Bitmap$CompressFormat;->JPEG:Landroid/graphics/Bitmap$CompressFormat;

    invoke-virtual {p1, v2, p2, v0}, Landroid/graphics/Bitmap;->compress(Landroid/graphics/Bitmap$CompressFormat;ILjava/io/OutputStream;)Z

    move-result v1

    .line 495
    .local v1, "compressed":Z
    invoke-virtual {v0}, Ljava/io/ByteArrayOutputStream;->flush()V

    .line 496
    invoke-virtual {v0}, Ljava/io/ByteArrayOutputStream;->close()V

    .line 498
    if-nez v1, :cond_0

    .line 499
    const-string v2, "FB_LOGS"

    const-string v3, "In getByteArray Unable to compress image"

    invoke-static {v2, v3}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 500
    new-instance v2, Ljava/io/IOException;

    const-string v3, "Unable to compress image"

    invoke-direct {v2, v3}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v2

    .line 502
    :cond_0
    invoke-virtual {v0}, Ljava/io/ByteArrayOutputStream;->toByteArray()[B

    move-result-object v2

    return-object v2
.end method

.method private setContactPhoto([BJ)V
    .locals 16
    .param p1, "bytes"    # [B
    .param p2, "contactId"    # J

    .prologue
    .line 509
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/android/providers/contacts/util/FbHighQualityImageDownload$FbImageDownloader;->this$0:Lcom/android/providers/contacts/util/FbHighQualityImageDownload;

    invoke-virtual {v3}, Lcom/android/providers/contacts/util/FbHighQualityImageDownload;->getDatabaseHelper()Lcom/android/providers/contacts/ContactsDatabaseHelper;

    move-result-object v3

    invoke-virtual {v3}, Lcom/android/providers/contacts/ContactsDatabaseHelper;->getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v2

    .line 510
    .local v2, "db":Landroid/database/sqlite/SQLiteDatabase;
    new-instance v15, Landroid/content/ContentValues;

    invoke-direct {v15}, Landroid/content/ContentValues;-><init>()V

    .line 511
    .local v15, "values":Landroid/content/ContentValues;
    const/4 v14, -0x1

    .line 512
    .local v14, "photoRow":I
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "raw_contact_id = "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    move-wide/from16 v0, p2

    invoke-virtual {v3, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " AND "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "mimetype"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "==\'"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "vnd.android.cursor.item/photo"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "\'"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    .line 514
    .local v5, "where":Ljava/lang/String;
    const-string v3, "view_data"

    const/4 v4, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    const/4 v8, 0x0

    const/4 v9, 0x0

    invoke-virtual/range {v2 .. v9}, Landroid/database/sqlite/SQLiteDatabase;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v10

    .line 516
    .local v10, "cursor":Landroid/database/Cursor;
    const/4 v11, -0x1

    .line 517
    .local v11, "dataVersion":I
    :try_start_0
    const-string v3, "_id"

    invoke-interface {v10, v3}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v13

    .line 518
    .local v13, "idIdx":I
    const-string v3, "data_version"

    invoke-interface {v10, v3}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v12

    .line 519
    .local v12, "dataVersionIndex":I
    invoke-interface {v10}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v3

    if-eqz v3, :cond_0

    .line 520
    invoke-interface {v10, v13}, Landroid/database/Cursor;->getInt(I)I

    move-result v14

    .line 521
    invoke-interface {v10, v12}, Landroid/database/Cursor;->getInt(I)I

    move-result v11

    .line 523
    :cond_0
    const-string v3, "data15"

    move-object/from16 v0, p1

    invoke-virtual {v15, v3, v0}, Landroid/content/ContentValues;->put(Ljava/lang/String;[B)V

    .line 524
    const-string v3, "data13"

    add-int/lit8 v4, v11, 0x1

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v15, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 526
    if-eqz v10, :cond_1

    .line 527
    invoke-interface {v10}, Landroid/database/Cursor;->close()V

    .line 529
    :cond_1
    const-string v3, "data"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "_id = "

    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v14}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    const/4 v6, 0x0

    invoke-virtual {v2, v3, v15, v4, v6}, Landroid/database/sqlite/SQLiteDatabase;->update(Ljava/lang/String;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    .line 530
    return-void

    .line 526
    .end local v12    # "dataVersionIndex":I
    .end local v13    # "idIdx":I
    :catchall_0
    move-exception v3

    if-eqz v10, :cond_2

    .line 527
    invoke-interface {v10}, Landroid/database/Cursor;->close()V

    :cond_2
    throw v3
.end method


# virtual methods
.method public getBitmapFromURL(Ljava/lang/String;J)V
    .locals 10
    .param p1, "fbUrl"    # Ljava/lang/String;
    .param p2, "rawContactId"    # J

    .prologue
    .line 464
    :try_start_0
    new-instance v5, Ljava/net/URL;

    invoke-direct {v5, p1}, Ljava/net/URL;-><init>(Ljava/lang/String;)V

    .line 465
    .local v5, "url":Ljava/net/URL;
    invoke-virtual {v5}, Ljava/net/URL;->openConnection()Ljava/net/URLConnection;

    move-result-object v2

    check-cast v2, Ljava/net/HttpURLConnection;

    .line 466
    .local v2, "connection":Ljava/net/HttpURLConnection;
    const/4 v6, 0x1

    invoke-virtual {v2, v6}, Ljava/net/HttpURLConnection;->setDoInput(Z)V

    .line 467
    invoke-virtual {v2}, Ljava/net/HttpURLConnection;->connect()V

    .line 468
    invoke-virtual {v2}, Ljava/net/HttpURLConnection;->getInputStream()Ljava/io/InputStream;

    move-result-object v4

    .line 469
    .local v4, "input":Ljava/io/InputStream;
    invoke-static {v4}, Landroid/graphics/BitmapFactory;->decodeStream(Ljava/io/InputStream;)Landroid/graphics/Bitmap;

    move-result-object v0

    .line 470
    .local v0, "bitmap":Landroid/graphics/Bitmap;
    if-eqz v0, :cond_0

    .line 471
    const-string v6, "FB_LOGS"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "In getBitmapFromURL Found Bitmap for rawContactsID = "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, p2, p3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 473
    const/16 v6, 0x4b

    invoke-direct {p0, v0, v6}, Lcom/android/providers/contacts/util/FbHighQualityImageDownload$FbImageDownloader;->getByteArray(Landroid/graphics/Bitmap;I)[B

    move-result-object v1

    .line 474
    .local v1, "byteArray":[B
    invoke-direct {p0, v1, p2, p3}, Lcom/android/providers/contacts/util/FbHighQualityImageDownload$FbImageDownloader;->setContactPhoto([BJ)V

    .line 476
    iget-object v6, p0, Lcom/android/providers/contacts/util/FbHighQualityImageDownload$FbImageDownloader;->this$0:Lcom/android/providers/contacts/util/FbHighQualityImageDownload;

    const/4 v7, 0x1

    const-wide/16 v8, 0x64

    # invokes: Lcom/android/providers/contacts/util/FbHighQualityImageDownload;->sendMessageToFbImageDownloadHandler(IJ)V
    invoke-static {v6, v7, v8, v9}, Lcom/android/providers/contacts/util/FbHighQualityImageDownload;->access$1000(Lcom/android/providers/contacts/util/FbHighQualityImageDownload;IJ)V

    .line 487
    .end local v0    # "bitmap":Landroid/graphics/Bitmap;
    .end local v1    # "byteArray":[B
    .end local v2    # "connection":Ljava/net/HttpURLConnection;
    .end local v4    # "input":Ljava/io/InputStream;
    .end local v5    # "url":Ljava/net/URL;
    :goto_0
    return-void

    .line 478
    .restart local v0    # "bitmap":Landroid/graphics/Bitmap;
    .restart local v2    # "connection":Ljava/net/HttpURLConnection;
    .restart local v4    # "input":Ljava/io/InputStream;
    .restart local v5    # "url":Ljava/net/URL;
    :cond_0
    const-string v6, "FB_LOGS"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "In getBitmapFromURL can\'t Found Bitmap for rawContactsID = "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, p2, p3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 480
    iget-object v6, p0, Lcom/android/providers/contacts/util/FbHighQualityImageDownload$FbImageDownloader;->this$0:Lcom/android/providers/contacts/util/FbHighQualityImageDownload;

    const/4 v7, 0x1

    const-wide/16 v8, 0x64

    # invokes: Lcom/android/providers/contacts/util/FbHighQualityImageDownload;->sendMessageToFbImageDownloadHandler(IJ)V
    invoke-static {v6, v7, v8, v9}, Lcom/android/providers/contacts/util/FbHighQualityImageDownload;->access$1000(Lcom/android/providers/contacts/util/FbHighQualityImageDownload;IJ)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 482
    .end local v0    # "bitmap":Landroid/graphics/Bitmap;
    .end local v2    # "connection":Ljava/net/HttpURLConnection;
    .end local v4    # "input":Ljava/io/InputStream;
    .end local v5    # "url":Ljava/net/URL;
    :catch_0
    move-exception v3

    .line 483
    .local v3, "e":Ljava/io/IOException;
    invoke-virtual {v3}, Ljava/io/IOException;->printStackTrace()V

    .line 485
    iget-object v6, p0, Lcom/android/providers/contacts/util/FbHighQualityImageDownload$FbImageDownloader;->this$0:Lcom/android/providers/contacts/util/FbHighQualityImageDownload;

    const/4 v7, 0x1

    const-wide/16 v8, 0x64

    # invokes: Lcom/android/providers/contacts/util/FbHighQualityImageDownload;->sendMessageToFbImageDownloadHandler(IJ)V
    invoke-static {v6, v7, v8, v9}, Lcom/android/providers/contacts/util/FbHighQualityImageDownload;->access$1000(Lcom/android/providers/contacts/util/FbHighQualityImageDownload;IJ)V

    goto :goto_0
.end method
