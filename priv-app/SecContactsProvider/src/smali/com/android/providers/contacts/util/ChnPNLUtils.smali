.class public Lcom/android/providers/contacts/util/ChnPNLUtils;
.super Ljava/lang/Object;
.source "ChnPNLUtils.java"


# static fields
.field private static bInitialized:Z

.field private static mHeaderBufSize:I

.field private static mMobileBufSize:I

.field private static mMobileCityBufSize:I

.field static mNumofMobileCity:J

.field static mNumofPrefix:J

.field static mNumofProvince:J

.field static mNumofTelephoneCity:J

.field private static mProvinceBufSize:I

.field private static mStartPos:I

.field private static mTelephoneCityBufSize:I

.field private static sChnPNLUtils:Lcom/android/providers/contacts/util/ChnPNLUtils;


# direct methods
.method static constructor <clinit>()V
    .locals 4

    .prologue
    const-wide/16 v2, 0x0

    .line 13
    const/4 v0, 0x0

    sput-object v0, Lcom/android/providers/contacts/util/ChnPNLUtils;->sChnPNLUtils:Lcom/android/providers/contacts/util/ChnPNLUtils;

    .line 15
    const/4 v0, 0x0

    sput-boolean v0, Lcom/android/providers/contacts/util/ChnPNLUtils;->bInitialized:Z

    .line 37
    const-wide/16 v0, 0x1f

    sput-wide v0, Lcom/android/providers/contacts/util/ChnPNLUtils;->mNumofProvince:J

    .line 39
    sput-wide v2, Lcom/android/providers/contacts/util/ChnPNLUtils;->mNumofTelephoneCity:J

    .line 41
    sput-wide v2, Lcom/android/providers/contacts/util/ChnPNLUtils;->mNumofMobileCity:J

    .line 43
    sput-wide v2, Lcom/android/providers/contacts/util/ChnPNLUtils;->mNumofPrefix:J

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 10
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private getBufferUnicodeString([BII)Ljava/lang/String;
    .locals 8
    .param p1, "buffer"    # [B
    .param p2, "start"    # I
    .param p3, "length"    # I

    .prologue
    .line 170
    const/4 v4, 0x0

    .line 171
    .local v4, "isStringEnd":Z
    const/4 v1, 0x0

    .line 172
    .local v1, "count":I
    const/4 v3, 0x0

    .line 173
    .local v3, "i":I
    const/16 v6, 0x80

    new-array v0, v6, [B

    .line 175
    .local v0, "buffer128":[B
    const/4 v3, 0x0

    :goto_0
    if-ge v3, p3, :cond_0

    .line 176
    add-int v6, p2, v3

    aget-byte v6, p1, v6

    aput-byte v6, v0, v3

    .line 175
    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    .line 178
    :cond_0
    const/4 v3, 0x0

    :goto_1
    if-ge v3, p3, :cond_1

    .line 179
    rem-int/lit8 v6, v3, 0x2

    if-nez v6, :cond_2

    add-int/lit8 v6, p3, -0x1

    if-ge v3, v6, :cond_2

    aget-byte v6, v0, v3

    if-nez v6, :cond_2

    add-int/lit8 v6, v3, 0x1

    aget-byte v6, v0, v6

    if-nez v6, :cond_2

    const/4 v4, 0x1

    .line 180
    :goto_2
    if-eqz v4, :cond_3

    .line 186
    :cond_1
    new-array v5, v1, [B

    .line 187
    .local v5, "newBuffer":[B
    const/4 v3, 0x0

    :goto_3
    if-ge v3, v1, :cond_4

    .line 188
    aget-byte v6, v0, v3

    aput-byte v6, v5, v3

    .line 187
    add-int/lit8 v3, v3, 0x1

    goto :goto_3

    .line 179
    .end local v5    # "newBuffer":[B
    :cond_2
    const/4 v4, 0x0

    goto :goto_2

    .line 183
    :cond_3
    add-int/lit8 v1, v1, 0x1

    .line 178
    add-int/lit8 v3, v3, 0x1

    goto :goto_1

    .line 193
    .restart local v5    # "newBuffer":[B
    :cond_4
    :try_start_0
    new-instance v6, Ljava/lang/String;

    const-string v7, "UTF-16LE"

    invoke-direct {v6, v5, v7}, Ljava/lang/String;-><init>([BLjava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 197
    :goto_4
    return-object v6

    .line 194
    :catch_0
    move-exception v2

    .line 195
    .local v2, "e":Ljava/lang/Exception;
    invoke-virtual {v2}, Ljava/lang/Exception;->printStackTrace()V

    .line 197
    const-string v6, ""

    goto :goto_4
.end method

.method private getCallNumPrefix(Ljava/lang/String;)Ljava/lang/String;
    .locals 9
    .param p1, "callNum"    # Ljava/lang/String;

    .prologue
    const/4 v8, 0x1

    const/4 v7, 0x7

    const/4 v6, 0x4

    const/4 v5, 0x3

    const/4 v4, 0x0

    .line 201
    const/4 v0, 0x0

    .line 202
    .local v0, "callNumPrefix":Ljava/lang/String;
    if-nez p1, :cond_0

    .line 203
    const/4 v2, 0x0

    .line 246
    :goto_0
    return-object v2

    .line 205
    :cond_0
    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v1

    .line 208
    .local v1, "numberLength":I
    invoke-virtual {p1, v4}, Ljava/lang/String;->charAt(I)C

    move-result v2

    sparse-switch v2, :sswitch_data_0

    .line 242
    if-lt v1, v7, :cond_1

    .line 243
    invoke-virtual {p1, v4, v7}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v0

    :cond_1
    :goto_1
    move-object v2, v0

    .line 246
    goto :goto_0

    .line 212
    :sswitch_0
    invoke-virtual {p1, v8}, Ljava/lang/String;->charAt(I)C

    move-result v2

    const/16 v3, 0x33

    if-ge v2, v3, :cond_2

    .line 213
    if-lt v1, v5, :cond_1

    .line 214
    invoke-virtual {p1, v4, v5}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v0

    goto :goto_1

    .line 216
    :cond_2
    if-lt v1, v6, :cond_1

    .line 217
    invoke-virtual {p1, v4, v6}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v0

    goto :goto_1

    .line 221
    :sswitch_1
    const-string v2, "+86"

    invoke-virtual {p1, v2}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_3

    .line 222
    const-string v2, "+86"

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v2

    invoke-virtual {p1, v2}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object p1

    .line 226
    :goto_2
    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v1

    .line 227
    invoke-virtual {p1, v4}, Ljava/lang/String;->charAt(I)C

    move-result v2

    const/16 v3, 0x30

    if-ne v2, v3, :cond_5

    .line 228
    invoke-virtual {p1, v8}, Ljava/lang/String;->charAt(I)C

    move-result v2

    const/16 v3, 0x33

    if-ge v2, v3, :cond_4

    .line 229
    if-lt v1, v5, :cond_1

    .line 230
    invoke-virtual {p1, v4, v5}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v0

    goto :goto_1

    .line 224
    :cond_3
    invoke-virtual {p1, v8}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object p1

    goto :goto_2

    .line 232
    :cond_4
    if-lt v1, v6, :cond_1

    .line 233
    invoke-virtual {p1, v4, v6}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v0

    goto :goto_1

    .line 237
    :cond_5
    if-lt v1, v7, :cond_1

    .line 238
    invoke-virtual {p1, v4, v7}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v0

    goto :goto_1

    .line 208
    nop

    :sswitch_data_0
    .sparse-switch
        0x2b -> :sswitch_1
        0x30 -> :sswitch_0
    .end sparse-switch
.end method

.method public static getInstance()Lcom/android/providers/contacts/util/ChnPNLUtils;
    .locals 3

    .prologue
    .line 58
    sget-object v0, Lcom/android/providers/contacts/util/ChnPNLUtils;->sChnPNLUtils:Lcom/android/providers/contacts/util/ChnPNLUtils;

    if-nez v0, :cond_0

    .line 59
    new-instance v0, Lcom/android/providers/contacts/util/ChnPNLUtils;

    invoke-direct {v0}, Lcom/android/providers/contacts/util/ChnPNLUtils;-><init>()V

    sput-object v0, Lcom/android/providers/contacts/util/ChnPNLUtils;->sChnPNLUtils:Lcom/android/providers/contacts/util/ChnPNLUtils;

    .line 60
    const-string v0, "ChnPNLUtils"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "getInstance() => bInitialized = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    sget-boolean v2, Lcom/android/providers/contacts/util/ChnPNLUtils;->bInitialized:Z

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 61
    sget-boolean v0, Lcom/android/providers/contacts/util/ChnPNLUtils;->bInitialized:Z

    if-nez v0, :cond_0

    .line 62
    invoke-static {}, Lcom/android/providers/contacts/util/ChnPNLUtils;->readHeaderInfo()Z

    move-result v0

    sput-boolean v0, Lcom/android/providers/contacts/util/ChnPNLUtils;->bInitialized:Z

    .line 66
    :cond_0
    sget-object v0, Lcom/android/providers/contacts/util/ChnPNLUtils;->sChnPNLUtils:Lcom/android/providers/contacts/util/ChnPNLUtils;

    return-object v0
.end method

.method private mobileHeaderSearch([BII)I
    .locals 5
    .param p1, "header"    # [B
    .param p2, "iNum"    # I
    .param p3, "count"    # I

    .prologue
    .line 156
    const/4 v2, 0x0

    .line 157
    .local v2, "i":I
    const/4 v3, 0x2

    new-array v1, v3, [B

    .line 158
    .local v1, "buffer2":[B
    const/4 v2, 0x0

    :goto_0
    if-ge v2, p3, :cond_1

    .line 159
    const/4 v3, 0x0

    mul-int/lit8 v4, v2, 0x2

    aget-byte v4, p1, v4

    aput-byte v4, v1, v3

    .line 160
    const/4 v3, 0x1

    mul-int/lit8 v4, v2, 0x2

    add-int/lit8 v4, v4, 0x1

    aget-byte v4, p1, v4

    aput-byte v4, v1, v3

    .line 161
    invoke-direct {p0, v1}, Lcom/android/providers/contacts/util/ChnPNLUtils;->readUnsignedShort([B)I

    move-result v0

    .line 162
    .local v0, "areaCode":I
    if-ne v0, p2, :cond_0

    .line 166
    .end local v0    # "areaCode":I
    .end local v2    # "i":I
    :goto_1
    return v2

    .line 158
    .restart local v0    # "areaCode":I
    .restart local v2    # "i":I
    :cond_0
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 166
    .end local v0    # "areaCode":I
    :cond_1
    const/4 v2, -0x1

    goto :goto_1
.end method

.method private static readHeaderInfo()Z
    .locals 12

    .prologue
    const/16 v8, 0x20

    const/4 v7, 0x4

    .line 70
    const/4 v6, 0x0

    .line 71
    .local v6, "ret":Z
    const/4 v4, 0x0

    .line 72
    .local v4, "ois":Ljava/io/RandomAccessFile;
    new-array v1, v7, [B

    .line 73
    .local v1, "buffer4":[B
    new-array v0, v8, [B

    .line 76
    .local v0, "buffer32":[B
    :try_start_0
    new-instance v3, Ljava/io/File;

    const-string v7, "/data/data/com.android.phone/HomeLocationDB.bin"

    invoke-direct {v3, v7}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 77
    .local v3, "myFile":Ljava/io/File;
    invoke-virtual {v3}, Ljava/io/File;->exists()Z

    move-result v7

    if-eqz v7, :cond_1

    .line 78
    new-instance v5, Ljava/io/RandomAccessFile;

    const-string v7, "/data/data/com.android.phone/HomeLocationDB.bin"

    const-string v8, "r"

    invoke-direct {v5, v7, v8}, Ljava/io/RandomAccessFile;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .end local v4    # "ois":Ljava/io/RandomAccessFile;
    .local v5, "ois":Ljava/io/RandomAccessFile;
    move-object v4, v5

    .line 82
    .end local v5    # "ois":Ljava/io/RandomAccessFile;
    .restart local v4    # "ois":Ljava/io/RandomAccessFile;
    :goto_0
    const/4 v7, 0x0

    const/4 v8, 0x4

    invoke-virtual {v4, v1, v7, v8}, Ljava/io/RandomAccessFile;->read([BII)I

    .line 83
    invoke-static {v1}, Lcom/android/providers/contacts/util/ChnPNLUtils;->readUnsignedInt([B)J

    .line 84
    const/4 v7, 0x0

    const/16 v8, 0x20

    invoke-virtual {v4, v0, v7, v8}, Ljava/io/RandomAccessFile;->read([BII)I

    .line 85
    new-instance v7, Ljava/lang/String;

    const-string v8, "UTF-16LE"

    invoke-direct {v7, v0, v8}, Ljava/lang/String;-><init>([BLjava/lang/String;)V

    .line 87
    const/4 v7, 0x0

    const/16 v8, 0x20

    invoke-virtual {v4, v0, v7, v8}, Ljava/io/RandomAccessFile;->read([BII)I

    .line 88
    new-instance v7, Ljava/lang/String;

    const-string v8, "UTF-16LE"

    invoke-direct {v7, v0, v8}, Ljava/lang/String;-><init>([BLjava/lang/String;)V

    .line 90
    const/4 v7, 0x0

    const/4 v8, 0x4

    invoke-virtual {v4, v1, v7, v8}, Ljava/io/RandomAccessFile;->read([BII)I

    .line 91
    invoke-static {v1}, Lcom/android/providers/contacts/util/ChnPNLUtils;->readUnsignedInt([B)J

    move-result-wide v8

    sput-wide v8, Lcom/android/providers/contacts/util/ChnPNLUtils;->mNumofProvince:J

    .line 92
    const/4 v7, 0x0

    const/4 v8, 0x4

    invoke-virtual {v4, v1, v7, v8}, Ljava/io/RandomAccessFile;->read([BII)I

    .line 93
    invoke-static {v1}, Lcom/android/providers/contacts/util/ChnPNLUtils;->readUnsignedInt([B)J

    move-result-wide v8

    sput-wide v8, Lcom/android/providers/contacts/util/ChnPNLUtils;->mNumofTelephoneCity:J

    .line 94
    const/4 v7, 0x0

    const/4 v8, 0x4

    invoke-virtual {v4, v1, v7, v8}, Ljava/io/RandomAccessFile;->read([BII)I

    .line 95
    invoke-static {v1}, Lcom/android/providers/contacts/util/ChnPNLUtils;->readUnsignedInt([B)J

    move-result-wide v8

    sput-wide v8, Lcom/android/providers/contacts/util/ChnPNLUtils;->mNumofMobileCity:J

    .line 96
    const/4 v7, 0x0

    const/4 v8, 0x4

    invoke-virtual {v4, v1, v7, v8}, Ljava/io/RandomAccessFile;->read([BII)I

    .line 97
    invoke-static {v1}, Lcom/android/providers/contacts/util/ChnPNLUtils;->readUnsignedInt([B)J

    move-result-wide v8

    sput-wide v8, Lcom/android/providers/contacts/util/ChnPNLUtils;->mNumofPrefix:J

    .line 98
    const/4 v6, 0x1

    .line 100
    const-string v7, "ChnPNLUtils"

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "readHeaderInfo() => mNumofProvince: "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    sget-wide v10, Lcom/android/providers/contacts/util/ChnPNLUtils;->mNumofProvince:J

    invoke-virtual {v8, v10, v11}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, ", mNumofTelephoneCity: "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    sget-wide v10, Lcom/android/providers/contacts/util/ChnPNLUtils;->mNumofTelephoneCity:J

    invoke-virtual {v8, v10, v11}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, ", mNumofMobileCity: "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    sget-wide v10, Lcom/android/providers/contacts/util/ChnPNLUtils;->mNumofMobileCity:J

    invoke-virtual {v8, v10, v11}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, ", mNumofPrefix: "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    sget-wide v10, Lcom/android/providers/contacts/util/ChnPNLUtils;->mNumofPrefix:J

    invoke-virtual {v8, v10, v11}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v7, v8}, Landroid/util/secutil/Log;->secV(Ljava/lang/String;Ljava/lang/String;)I

    .line 104
    const-wide/16 v8, 0x60

    sget-wide v10, Lcom/android/providers/contacts/util/ChnPNLUtils;->mNumofProvince:J

    mul-long/2addr v8, v10

    long-to-int v7, v8

    sput v7, Lcom/android/providers/contacts/util/ChnPNLUtils;->mProvinceBufSize:I

    .line 105
    const-wide/16 v8, 0x83

    sget-wide v10, Lcom/android/providers/contacts/util/ChnPNLUtils;->mNumofTelephoneCity:J

    mul-long/2addr v8, v10

    long-to-int v7, v8

    sput v7, Lcom/android/providers/contacts/util/ChnPNLUtils;->mTelephoneCityBufSize:I

    .line 106
    const-wide/16 v8, 0x81

    sget-wide v10, Lcom/android/providers/contacts/util/ChnPNLUtils;->mNumofMobileCity:J

    mul-long/2addr v8, v10

    long-to-int v7, v8

    sput v7, Lcom/android/providers/contacts/util/ChnPNLUtils;->mMobileCityBufSize:I

    .line 107
    sget-wide v8, Lcom/android/providers/contacts/util/ChnPNLUtils;->mNumofPrefix:J

    const-wide/16 v10, 0x2

    mul-long/2addr v8, v10

    long-to-int v7, v8

    sput v7, Lcom/android/providers/contacts/util/ChnPNLUtils;->mHeaderBufSize:I

    .line 108
    const/16 v7, 0x4e20

    sput v7, Lcom/android/providers/contacts/util/ChnPNLUtils;->mMobileBufSize:I

    .line 109
    const/16 v7, 0x84

    sput v7, Lcom/android/providers/contacts/util/ChnPNLUtils;->mStartPos:I
    :try_end_0
    .catch Ljava/lang/OutOfMemoryError; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_3
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 116
    if-eqz v4, :cond_0

    .line 117
    :try_start_1
    invoke-virtual {v4}, Ljava/io/RandomAccessFile;->close()V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_0

    .line 123
    .end local v3    # "myFile":Ljava/io/File;
    :cond_0
    :goto_1
    return v6

    .line 80
    .restart local v3    # "myFile":Ljava/io/File;
    :cond_1
    :try_start_2
    new-instance v5, Ljava/io/RandomAccessFile;

    const-string v7, "/system/etc/HomeLocationDB.bin"

    const-string v8, "r"

    invoke-direct {v5, v7, v8}, Ljava/io/RandomAccessFile;-><init>(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_2
    .catch Ljava/lang/OutOfMemoryError; {:try_start_2 .. :try_end_2} :catch_1
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_3
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .end local v4    # "ois":Ljava/io/RandomAccessFile;
    .restart local v5    # "ois":Ljava/io/RandomAccessFile;
    move-object v4, v5

    .end local v5    # "ois":Ljava/io/RandomAccessFile;
    .restart local v4    # "ois":Ljava/io/RandomAccessFile;
    goto/16 :goto_0

    .line 119
    :catch_0
    move-exception v2

    .line 120
    .local v2, "e":Ljava/io/IOException;
    invoke-virtual {v2}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_1

    .line 110
    .end local v2    # "e":Ljava/io/IOException;
    .end local v3    # "myFile":Ljava/io/File;
    :catch_1
    move-exception v2

    .line 111
    .local v2, "e":Ljava/lang/OutOfMemoryError;
    :try_start_3
    invoke-virtual {v2}, Ljava/lang/OutOfMemoryError;->printStackTrace()V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 116
    if-eqz v4, :cond_0

    .line 117
    :try_start_4
    invoke-virtual {v4}, Ljava/io/RandomAccessFile;->close()V
    :try_end_4
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_2

    goto :goto_1

    .line 119
    :catch_2
    move-exception v2

    .line 120
    .local v2, "e":Ljava/io/IOException;
    invoke-virtual {v2}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_1

    .line 112
    .end local v2    # "e":Ljava/io/IOException;
    :catch_3
    move-exception v2

    .line 113
    .local v2, "e":Ljava/lang/Exception;
    :try_start_5
    invoke-virtual {v2}, Ljava/lang/Exception;->printStackTrace()V
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    .line 116
    if-eqz v4, :cond_0

    .line 117
    :try_start_6
    invoke-virtual {v4}, Ljava/io/RandomAccessFile;->close()V
    :try_end_6
    .catch Ljava/io/IOException; {:try_start_6 .. :try_end_6} :catch_4

    goto :goto_1

    .line 119
    :catch_4
    move-exception v2

    .line 120
    .local v2, "e":Ljava/io/IOException;
    invoke-virtual {v2}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_1

    .line 115
    .end local v2    # "e":Ljava/io/IOException;
    :catchall_0
    move-exception v7

    .line 116
    if-eqz v4, :cond_2

    .line 117
    :try_start_7
    invoke-virtual {v4}, Ljava/io/RandomAccessFile;->close()V
    :try_end_7
    .catch Ljava/io/IOException; {:try_start_7 .. :try_end_7} :catch_5

    .line 121
    :cond_2
    :goto_2
    throw v7

    .line 119
    :catch_5
    move-exception v2

    .line 120
    .restart local v2    # "e":Ljava/io/IOException;
    invoke-virtual {v2}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_2
.end method

.method private static readUnsignedInt([B)J
    .locals 11
    .param p0, "bytes"    # [B

    .prologue
    .line 127
    const/4 v8, 0x0

    aget-byte v8, p0, v8

    and-int/lit16 v8, v8, 0xff

    int-to-long v0, v8

    .line 128
    .local v0, "b0":J
    const/4 v8, 0x1

    aget-byte v8, p0, v8

    and-int/lit16 v8, v8, 0xff

    int-to-long v8, v8

    const/16 v10, 0x8

    shl-long v2, v8, v10

    .line 129
    .local v2, "b1":J
    const/4 v8, 0x2

    aget-byte v8, p0, v8

    and-int/lit16 v8, v8, 0xff

    int-to-long v8, v8

    const/16 v10, 0x10

    shl-long v4, v8, v10

    .line 130
    .local v4, "b2":J
    const/4 v8, 0x3

    aget-byte v8, p0, v8

    and-int/lit16 v8, v8, 0xff

    int-to-long v8, v8

    const/16 v10, 0x18

    shl-long v6, v8, v10

    .line 131
    .local v6, "b3":J
    or-long v8, v0, v2

    or-long/2addr v8, v4

    or-long/2addr v8, v6

    return-wide v8
.end method

.method private readUnsignedShort([B)I
    .locals 3
    .param p1, "bytes"    # [B

    .prologue
    .line 135
    const/4 v2, 0x0

    aget-byte v2, p1, v2

    and-int/lit16 v0, v2, 0xff

    .line 136
    .local v0, "b0":I
    const/4 v2, 0x1

    aget-byte v2, p1, v2

    and-int/lit16 v2, v2, 0xff

    shl-int/lit8 v1, v2, 0x8

    .line 137
    .local v1, "b1":I
    or-int v2, v0, v1

    return v2
.end method

.method private teleBinarySearch([BII)I
    .locals 5
    .param p1, "telephoneCityProvince"    # [B
    .param p2, "iNum"    # I
    .param p3, "count"    # I

    .prologue
    .line 141
    const/4 v2, 0x0

    .line 142
    .local v2, "i":I
    const/4 v3, 0x2

    new-array v1, v3, [B

    .line 144
    .local v1, "buffer2":[B
    const/4 v2, 0x0

    :goto_0
    if-ge v2, p3, :cond_1

    .line 145
    const/4 v3, 0x0

    mul-int/lit16 v4, v2, 0x83

    add-int/lit16 v4, v4, 0x83

    add-int/lit8 v4, v4, -0x2

    aget-byte v4, p1, v4

    aput-byte v4, v1, v3

    .line 146
    const/4 v3, 0x1

    mul-int/lit16 v4, v2, 0x83

    add-int/lit16 v4, v4, 0x83

    add-int/lit8 v4, v4, -0x1

    aget-byte v4, p1, v4

    aput-byte v4, v1, v3

    .line 147
    invoke-direct {p0, v1}, Lcom/android/providers/contacts/util/ChnPNLUtils;->readUnsignedShort([B)I

    move-result v0

    .line 148
    .local v0, "areaCode":I
    if-ne v0, p2, :cond_0

    .line 152
    .end local v0    # "areaCode":I
    .end local v2    # "i":I
    :goto_1
    return v2

    .line 144
    .restart local v0    # "areaCode":I
    .restart local v2    # "i":I
    :cond_0
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 152
    .end local v0    # "areaCode":I
    :cond_1
    const/4 v2, -0x1

    goto :goto_1
.end method


# virtual methods
.method public getPNL(Ljava/lang/String;)Ljava/lang/String;
    .locals 36
    .param p1, "number"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 250
    const/16 v25, 0x0

    .line 252
    .local v25, "pnl":Ljava/lang/String;
    const-string v29, ""

    .line 253
    .local v29, "strProvince":Ljava/lang/String;
    const-string v28, ""

    .line 254
    .local v28, "strCity":Ljava/lang/String;
    const/16 v32, 0x2

    move/from16 v0, v32

    new-array v6, v0, [B

    .line 255
    .local v6, "buffer2":[B
    const/16 v27, 0x0

    .line 256
    .local v27, "province":Ljava/lang/String;
    const/4 v10, 0x0

    .line 257
    .local v10, "city":Ljava/lang/String;
    const/16 v16, 0x0

    .line 259
    .local v16, "isMobileNumber":Z
    const/4 v9, 0x0

    .line 260
    .local v9, "callNumPrefixInt":I
    const/16 v23, 0x0

    .line 263
    .local v23, "ois":Ljava/io/RandomAccessFile;
    invoke-direct/range {p0 .. p1}, Lcom/android/providers/contacts/util/ChnPNLUtils;->getCallNumPrefix(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v8

    .line 264
    .local v8, "callNumPrefix":Ljava/lang/String;
    if-nez v8, :cond_1

    .line 265
    const/16 v32, 0x0

    .line 375
    :cond_0
    :goto_0
    return-object v32

    .line 267
    :cond_1
    const/16 v32, 0x0

    move/from16 v0, v32

    invoke-virtual {v8, v0}, Ljava/lang/String;->charAt(I)C

    move-result v32

    const/16 v33, 0x30

    move/from16 v0, v32

    move/from16 v1, v33

    if-ne v0, v1, :cond_2

    const/16 v16, 0x0

    .line 268
    :goto_1
    invoke-static {v8}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v9

    .line 271
    :try_start_0
    new-instance v21, Ljava/io/File;

    const-string v32, "/data/data/com.android.phone/HomeLocationDB.bin"

    move-object/from16 v0, v21

    move-object/from16 v1, v32

    invoke-direct {v0, v1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 272
    .local v21, "myFile":Ljava/io/File;
    invoke-virtual/range {v21 .. v21}, Ljava/io/File;->exists()Z

    move-result v32

    if-eqz v32, :cond_3

    .line 273
    new-instance v24, Ljava/io/RandomAccessFile;

    const-string v32, "/data/data/com.android.phone/HomeLocationDB.bin"

    const-string v33, "r"

    move-object/from16 v0, v24

    move-object/from16 v1, v32

    move-object/from16 v2, v33

    invoke-direct {v0, v1, v2}, Ljava/io/RandomAccessFile;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .end local v23    # "ois":Ljava/io/RandomAccessFile;
    .local v24, "ois":Ljava/io/RandomAccessFile;
    move-object/from16 v23, v24

    .line 277
    .end local v24    # "ois":Ljava/io/RandomAccessFile;
    .restart local v23    # "ois":Ljava/io/RandomAccessFile;
    :goto_2
    sget v32, Lcom/android/providers/contacts/util/ChnPNLUtils;->mStartPos:I

    move/from16 v0, v32

    int-to-long v0, v0

    move-wide/from16 v32, v0

    move-object/from16 v0, v23

    move-wide/from16 v1, v32

    invoke-virtual {v0, v1, v2}, Ljava/io/RandomAccessFile;->seek(J)V

    .line 280
    sget v32, Lcom/android/providers/contacts/util/ChnPNLUtils;->mProvinceBufSize:I

    move/from16 v0, v32

    new-array v7, v0, [B

    .line 281
    .local v7, "bufferProvince":[B
    const/16 v32, 0x0

    sget v33, Lcom/android/providers/contacts/util/ChnPNLUtils;->mProvinceBufSize:I

    move-object/from16 v0, v23

    move/from16 v1, v32

    move/from16 v2, v33

    invoke-virtual {v0, v7, v1, v2}, Ljava/io/RandomAccessFile;->read([BII)I

    .line 283
    if-nez v16, :cond_9

    .line 284
    sget v32, Lcom/android/providers/contacts/util/ChnPNLUtils;->mTelephoneCityBufSize:I

    move/from16 v0, v32

    new-array v0, v0, [B

    move-object/from16 v30, v0

    .line 285
    .local v30, "telephoneCityProvince":[B
    const/16 v32, 0x0

    sget v33, Lcom/android/providers/contacts/util/ChnPNLUtils;->mTelephoneCityBufSize:I

    move-object/from16 v0, v23

    move-object/from16 v1, v30

    move/from16 v2, v32

    move/from16 v3, v33

    invoke-virtual {v0, v1, v2, v3}, Ljava/io/RandomAccessFile;->read([BII)I

    .line 287
    sget-wide v32, Lcom/android/providers/contacts/util/ChnPNLUtils;->mNumofTelephoneCity:J

    move-wide/from16 v0, v32

    long-to-int v0, v0

    move/from16 v32, v0

    move-object/from16 v0, p0

    move-object/from16 v1, v30

    move/from16 v2, v32

    invoke-direct {v0, v1, v9, v2}, Lcom/android/providers/contacts/util/ChnPNLUtils;->teleBinarySearch([BII)I

    move-result v31

    .line 289
    .local v31, "telret":I
    const/16 v32, -0x1

    move/from16 v0, v31

    move/from16 v1, v32

    if-ne v0, v1, :cond_4

    .line 290
    invoke-virtual/range {v23 .. v23}, Ljava/io/RandomAccessFile;->close()V
    :try_end_0
    .catch Ljava/lang/OutOfMemoryError; {:try_start_0 .. :try_end_0} :catch_6
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_8
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 291
    const/16 v32, 0x0

    .line 357
    if-eqz v23, :cond_0

    .line 358
    :try_start_1
    invoke-virtual/range {v23 .. v23}, Ljava/io/RandomAccessFile;->close()V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_0

    goto/16 :goto_0

    .line 359
    :catch_0
    move-exception v11

    .line 360
    .local v11, "e":Ljava/io/IOException;
    invoke-virtual {v11}, Ljava/io/IOException;->printStackTrace()V

    goto/16 :goto_0

    .line 267
    .end local v7    # "bufferProvince":[B
    .end local v11    # "e":Ljava/io/IOException;
    .end local v21    # "myFile":Ljava/io/File;
    .end local v30    # "telephoneCityProvince":[B
    .end local v31    # "telret":I
    :cond_2
    const/16 v16, 0x1

    goto/16 :goto_1

    .line 275
    .restart local v21    # "myFile":Ljava/io/File;
    :cond_3
    :try_start_2
    new-instance v24, Ljava/io/RandomAccessFile;

    const-string v32, "/system/etc/HomeLocationDB.bin"

    const-string v33, "r"

    move-object/from16 v0, v24

    move-object/from16 v1, v32

    move-object/from16 v2, v33

    invoke-direct {v0, v1, v2}, Ljava/io/RandomAccessFile;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .end local v23    # "ois":Ljava/io/RandomAccessFile;
    .restart local v24    # "ois":Ljava/io/RandomAccessFile;
    move-object/from16 v23, v24

    .end local v24    # "ois":Ljava/io/RandomAccessFile;
    .restart local v23    # "ois":Ljava/io/RandomAccessFile;
    goto :goto_2

    .line 293
    .restart local v7    # "bufferProvince":[B
    .restart local v30    # "telephoneCityProvince":[B
    .restart local v31    # "telret":I
    :cond_4
    const/4 v5, 0x0

    .line 294
    .local v5, "buffer1":B
    move/from16 v0, v31

    mul-int/lit16 v0, v0, 0x83

    move/from16 v32, v0

    aget-byte v5, v30, v32

    .line 295
    move/from16 v26, v5

    .line 296
    .local v26, "proindex":I
    if-gez v26, :cond_5

    .line 297
    invoke-virtual/range {v23 .. v23}, Ljava/io/RandomAccessFile;->close()V
    :try_end_2
    .catch Ljava/lang/OutOfMemoryError; {:try_start_2 .. :try_end_2} :catch_6
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_8
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 298
    const/16 v32, 0x0

    .line 357
    if-eqz v23, :cond_0

    .line 358
    :try_start_3
    invoke-virtual/range {v23 .. v23}, Ljava/io/RandomAccessFile;->close()V
    :try_end_3
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_1

    goto/16 :goto_0

    .line 359
    :catch_1
    move-exception v11

    .line 360
    .restart local v11    # "e":Ljava/io/IOException;
    invoke-virtual {v11}, Ljava/io/IOException;->printStackTrace()V

    goto/16 :goto_0

    .line 300
    .end local v11    # "e":Ljava/io/IOException;
    :cond_5
    if-eqz v26, :cond_6

    .line 301
    add-int/lit8 v32, v26, -0x1

    mul-int/lit8 v32, v32, 0x60

    const/16 v33, 0x20

    :try_start_4
    move-object/from16 v0, p0

    move/from16 v1, v32

    move/from16 v2, v33

    invoke-direct {v0, v7, v1, v2}, Lcom/android/providers/contacts/util/ChnPNLUtils;->getBufferUnicodeString([BII)Ljava/lang/String;

    move-result-object v27

    .line 304
    :cond_6
    move/from16 v0, v31

    mul-int/lit16 v0, v0, 0x83

    move/from16 v32, v0

    add-int/lit8 v32, v32, 0x1

    const/16 v33, 0x20

    move-object/from16 v0, p0

    move-object/from16 v1, v30

    move/from16 v2, v32

    move/from16 v3, v33

    invoke-direct {v0, v1, v2, v3}, Lcom/android/providers/contacts/util/ChnPNLUtils;->getBufferUnicodeString([BII)Ljava/lang/String;
    :try_end_4
    .catch Ljava/lang/OutOfMemoryError; {:try_start_4 .. :try_end_4} :catch_6
    .catch Ljava/lang/Exception; {:try_start_4 .. :try_end_4} :catch_8
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    move-result-object v10

    .line 357
    .end local v30    # "telephoneCityProvince":[B
    .end local v31    # "telret":I
    :goto_3
    if-eqz v23, :cond_7

    .line 358
    :try_start_5
    invoke-virtual/range {v23 .. v23}, Ljava/io/RandomAccessFile;->close()V
    :try_end_5
    .catch Ljava/io/IOException; {:try_start_5 .. :try_end_5} :catch_5

    .line 363
    :cond_7
    :goto_4
    move-object/from16 v29, v27

    .line 364
    move-object/from16 v28, v10

    .line 366
    const-string v32, "ChnPNLUtils"

    new-instance v33, Ljava/lang/StringBuilder;

    invoke-direct/range {v33 .. v33}, Ljava/lang/StringBuilder;-><init>()V

    const-string v34, "strProvince: "

    invoke-virtual/range {v33 .. v34}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v33

    move-object/from16 v0, v33

    move-object/from16 v1, v29

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v33

    const-string v34, ", strCity: "

    invoke-virtual/range {v33 .. v34}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v33

    move-object/from16 v0, v33

    move-object/from16 v1, v28

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v33

    invoke-virtual/range {v33 .. v33}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v33

    invoke-static/range {v32 .. v33}, Landroid/util/secutil/Log;->secW(Ljava/lang/String;Ljava/lang/String;)I

    .line 367
    if-eqz v23, :cond_8

    .line 368
    invoke-virtual/range {v23 .. v23}, Ljava/io/RandomAccessFile;->close()V

    .line 369
    :cond_8
    if-eqz v29, :cond_f

    if-eqz v28, :cond_f

    .line 370
    new-instance v32, Ljava/lang/StringBuilder;

    invoke-direct/range {v32 .. v32}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, v32

    move-object/from16 v1, v29

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v32

    move-object/from16 v0, v32

    move-object/from16 v1, v28

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v32

    invoke-virtual/range {v32 .. v32}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v25

    :goto_5
    move-object/from16 v32, v25

    .line 375
    goto/16 :goto_0

    .line 307
    .end local v5    # "buffer1":B
    .end local v26    # "proindex":I
    :cond_9
    :try_start_6
    invoke-virtual/range {v23 .. v23}, Ljava/io/RandomAccessFile;->getFilePointer()J

    move-result-wide v12

    .line 308
    .local v12, "currentPos":J
    const-wide/16 v32, 0x83

    sget-wide v34, Lcom/android/providers/contacts/util/ChnPNLUtils;->mNumofTelephoneCity:J

    mul-long v32, v32, v34

    add-long v32, v32, v12

    move-object/from16 v0, v23

    move-wide/from16 v1, v32

    invoke-virtual {v0, v1, v2}, Ljava/io/RandomAccessFile;->seek(J)V

    .line 310
    sget v32, Lcom/android/providers/contacts/util/ChnPNLUtils;->mMobileCityBufSize:I

    move/from16 v0, v32

    new-array v0, v0, [B

    move-object/from16 v18, v0

    .line 311
    .local v18, "mobileCityProvince":[B
    const/16 v32, 0x0

    sget v33, Lcom/android/providers/contacts/util/ChnPNLUtils;->mMobileCityBufSize:I

    move-object/from16 v0, v23

    move-object/from16 v1, v18

    move/from16 v2, v32

    move/from16 v3, v33

    invoke-virtual {v0, v1, v2, v3}, Ljava/io/RandomAccessFile;->read([BII)I

    .line 314
    sget v32, Lcom/android/providers/contacts/util/ChnPNLUtils;->mHeaderBufSize:I

    move/from16 v0, v32

    new-array v15, v0, [B

    .line 315
    .local v15, "headerbuf":[B
    const/16 v32, 0x0

    sget v33, Lcom/android/providers/contacts/util/ChnPNLUtils;->mHeaderBufSize:I

    move-object/from16 v0, v23

    move/from16 v1, v32

    move/from16 v2, v33

    invoke-virtual {v0, v15, v1, v2}, Ljava/io/RandomAccessFile;->read([BII)I

    .line 316
    const/16 v32, 0x0

    const/16 v33, 0x3

    move/from16 v0, v32

    move/from16 v1, v33

    invoke-virtual {v8, v0, v1}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v19

    .line 317
    .local v19, "mobileHeader":Ljava/lang/String;
    invoke-static/range {v19 .. v19}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v22

    .line 318
    .local v22, "nMobileHeader":I
    sget-wide v32, Lcom/android/providers/contacts/util/ChnPNLUtils;->mNumofPrefix:J

    move-wide/from16 v0, v32

    long-to-int v0, v0

    move/from16 v32, v0

    move-object/from16 v0, p0

    move/from16 v1, v22

    move/from16 v2, v32

    invoke-direct {v0, v15, v1, v2}, Lcom/android/providers/contacts/util/ChnPNLUtils;->mobileHeaderSearch([BII)I

    move-result v14

    .line 319
    .local v14, "headerIndex":I
    if-gez v14, :cond_a

    .line 320
    invoke-virtual/range {v23 .. v23}, Ljava/io/RandomAccessFile;->close()V
    :try_end_6
    .catch Ljava/lang/OutOfMemoryError; {:try_start_6 .. :try_end_6} :catch_6
    .catch Ljava/lang/Exception; {:try_start_6 .. :try_end_6} :catch_8
    .catchall {:try_start_6 .. :try_end_6} :catchall_0

    .line 321
    const/16 v32, 0x0

    .line 357
    if-eqz v23, :cond_0

    .line 358
    :try_start_7
    invoke-virtual/range {v23 .. v23}, Ljava/io/RandomAccessFile;->close()V
    :try_end_7
    .catch Ljava/io/IOException; {:try_start_7 .. :try_end_7} :catch_2

    goto/16 :goto_0

    .line 359
    :catch_2
    move-exception v11

    .line 360
    .restart local v11    # "e":Ljava/io/IOException;
    invoke-virtual {v11}, Ljava/io/IOException;->printStackTrace()V

    goto/16 :goto_0

    .line 323
    .end local v11    # "e":Ljava/io/IOException;
    :cond_a
    :try_start_8
    sget v32, Lcom/android/providers/contacts/util/ChnPNLUtils;->mMobileBufSize:I

    move/from16 v0, v32

    new-array v0, v0, [B

    move-object/from16 v20, v0

    .line 325
    .local v20, "mobilebuf":[B
    invoke-virtual/range {v23 .. v23}, Ljava/io/RandomAccessFile;->getFilePointer()J

    move-result-wide v12

    .line 326
    sget v32, Lcom/android/providers/contacts/util/ChnPNLUtils;->mMobileBufSize:I

    mul-int v32, v32, v14

    move/from16 v0, v32

    int-to-long v0, v0

    move-wide/from16 v32, v0

    add-long v32, v32, v12

    move-object/from16 v0, v23

    move-wide/from16 v1, v32

    invoke-virtual {v0, v1, v2}, Ljava/io/RandomAccessFile;->seek(J)V

    .line 328
    const/16 v32, 0x0

    sget v33, Lcom/android/providers/contacts/util/ChnPNLUtils;->mMobileBufSize:I

    move-object/from16 v0, v23

    move-object/from16 v1, v20

    move/from16 v2, v32

    move/from16 v3, v33

    invoke-virtual {v0, v1, v2, v3}, Ljava/io/RandomAccessFile;->read([BII)I

    .line 329
    rem-int/lit16 v0, v9, 0x2710

    move/from16 v17, v0

    .line 330
    .local v17, "lowMobile":I
    const/16 v32, 0x0

    mul-int/lit8 v33, v17, 0x2

    aget-byte v33, v20, v33

    aput-byte v33, v6, v32

    .line 331
    const/16 v32, 0x1

    mul-int/lit8 v33, v17, 0x2

    add-int/lit8 v33, v33, 0x1

    aget-byte v33, v20, v33

    aput-byte v33, v6, v32

    .line 332
    move-object/from16 v0, p0

    invoke-direct {v0, v6}, Lcom/android/providers/contacts/util/ChnPNLUtils;->readUnsignedShort([B)I

    move-result v4

    .line 333
    .local v4, "CityIndex":I
    if-gtz v4, :cond_b

    .line 334
    invoke-virtual/range {v23 .. v23}, Ljava/io/RandomAccessFile;->close()V
    :try_end_8
    .catch Ljava/lang/OutOfMemoryError; {:try_start_8 .. :try_end_8} :catch_6
    .catch Ljava/lang/Exception; {:try_start_8 .. :try_end_8} :catch_8
    .catchall {:try_start_8 .. :try_end_8} :catchall_0

    .line 335
    const/16 v32, 0x0

    .line 357
    if-eqz v23, :cond_0

    .line 358
    :try_start_9
    invoke-virtual/range {v23 .. v23}, Ljava/io/RandomAccessFile;->close()V
    :try_end_9
    .catch Ljava/io/IOException; {:try_start_9 .. :try_end_9} :catch_3

    goto/16 :goto_0

    .line 359
    :catch_3
    move-exception v11

    .line 360
    .restart local v11    # "e":Ljava/io/IOException;
    invoke-virtual {v11}, Ljava/io/IOException;->printStackTrace()V

    goto/16 :goto_0

    .line 337
    .end local v11    # "e":Ljava/io/IOException;
    :cond_b
    const/4 v5, 0x0

    .line 338
    .restart local v5    # "buffer1":B
    add-int/lit8 v32, v4, -0x1

    move/from16 v0, v32

    mul-int/lit16 v0, v0, 0x81

    move/from16 v32, v0

    :try_start_a
    aget-byte v5, v18, v32

    .line 339
    move/from16 v26, v5

    .line 340
    .restart local v26    # "proindex":I
    if-gez v26, :cond_c

    .line 341
    invoke-virtual/range {v23 .. v23}, Ljava/io/RandomAccessFile;->close()V
    :try_end_a
    .catch Ljava/lang/OutOfMemoryError; {:try_start_a .. :try_end_a} :catch_6
    .catch Ljava/lang/Exception; {:try_start_a .. :try_end_a} :catch_8
    .catchall {:try_start_a .. :try_end_a} :catchall_0

    .line 342
    const/16 v32, 0x0

    .line 357
    if-eqz v23, :cond_0

    .line 358
    :try_start_b
    invoke-virtual/range {v23 .. v23}, Ljava/io/RandomAccessFile;->close()V
    :try_end_b
    .catch Ljava/io/IOException; {:try_start_b .. :try_end_b} :catch_4

    goto/16 :goto_0

    .line 359
    :catch_4
    move-exception v11

    .line 360
    .restart local v11    # "e":Ljava/io/IOException;
    invoke-virtual {v11}, Ljava/io/IOException;->printStackTrace()V

    goto/16 :goto_0

    .line 344
    .end local v11    # "e":Ljava/io/IOException;
    :cond_c
    if-eqz v26, :cond_d

    .line 345
    add-int/lit8 v32, v26, -0x1

    mul-int/lit8 v32, v32, 0x60

    const/16 v33, 0x20

    :try_start_c
    move-object/from16 v0, p0

    move/from16 v1, v32

    move/from16 v2, v33

    invoke-direct {v0, v7, v1, v2}, Lcom/android/providers/contacts/util/ChnPNLUtils;->getBufferUnicodeString([BII)Ljava/lang/String;

    move-result-object v27

    .line 348
    :cond_d
    add-int/lit8 v32, v4, -0x1

    move/from16 v0, v32

    mul-int/lit16 v0, v0, 0x81

    move/from16 v32, v0

    add-int/lit8 v32, v32, 0x1

    const/16 v33, 0x20

    move-object/from16 v0, p0

    move-object/from16 v1, v18

    move/from16 v2, v32

    move/from16 v3, v33

    invoke-direct {v0, v1, v2, v3}, Lcom/android/providers/contacts/util/ChnPNLUtils;->getBufferUnicodeString([BII)Ljava/lang/String;
    :try_end_c
    .catch Ljava/lang/OutOfMemoryError; {:try_start_c .. :try_end_c} :catch_6
    .catch Ljava/lang/Exception; {:try_start_c .. :try_end_c} :catch_8
    .catchall {:try_start_c .. :try_end_c} :catchall_0

    move-result-object v10

    goto/16 :goto_3

    .line 359
    .end local v4    # "CityIndex":I
    .end local v12    # "currentPos":J
    .end local v14    # "headerIndex":I
    .end local v15    # "headerbuf":[B
    .end local v17    # "lowMobile":I
    .end local v18    # "mobileCityProvince":[B
    .end local v19    # "mobileHeader":Ljava/lang/String;
    .end local v20    # "mobilebuf":[B
    .end local v22    # "nMobileHeader":I
    :catch_5
    move-exception v11

    .line 360
    .restart local v11    # "e":Ljava/io/IOException;
    invoke-virtual {v11}, Ljava/io/IOException;->printStackTrace()V

    goto/16 :goto_4

    .line 351
    .end local v5    # "buffer1":B
    .end local v7    # "bufferProvince":[B
    .end local v11    # "e":Ljava/io/IOException;
    .end local v21    # "myFile":Ljava/io/File;
    .end local v26    # "proindex":I
    :catch_6
    move-exception v11

    .line 352
    .local v11, "e":Ljava/lang/OutOfMemoryError;
    :try_start_d
    const-string v32, ""
    :try_end_d
    .catchall {:try_start_d .. :try_end_d} :catchall_0

    .line 357
    if-eqz v23, :cond_0

    .line 358
    :try_start_e
    invoke-virtual/range {v23 .. v23}, Ljava/io/RandomAccessFile;->close()V
    :try_end_e
    .catch Ljava/io/IOException; {:try_start_e .. :try_end_e} :catch_7

    goto/16 :goto_0

    .line 359
    :catch_7
    move-exception v11

    .line 360
    .local v11, "e":Ljava/io/IOException;
    invoke-virtual {v11}, Ljava/io/IOException;->printStackTrace()V

    goto/16 :goto_0

    .line 353
    .end local v11    # "e":Ljava/io/IOException;
    :catch_8
    move-exception v11

    .line 354
    .local v11, "e":Ljava/lang/Exception;
    :try_start_f
    const-string v32, ""
    :try_end_f
    .catchall {:try_start_f .. :try_end_f} :catchall_0

    .line 357
    if-eqz v23, :cond_0

    .line 358
    :try_start_10
    invoke-virtual/range {v23 .. v23}, Ljava/io/RandomAccessFile;->close()V
    :try_end_10
    .catch Ljava/io/IOException; {:try_start_10 .. :try_end_10} :catch_9

    goto/16 :goto_0

    .line 359
    :catch_9
    move-exception v11

    .line 360
    .local v11, "e":Ljava/io/IOException;
    invoke-virtual {v11}, Ljava/io/IOException;->printStackTrace()V

    goto/16 :goto_0

    .line 356
    .end local v11    # "e":Ljava/io/IOException;
    :catchall_0
    move-exception v32

    .line 357
    if-eqz v23, :cond_e

    .line 358
    :try_start_11
    invoke-virtual/range {v23 .. v23}, Ljava/io/RandomAccessFile;->close()V
    :try_end_11
    .catch Ljava/io/IOException; {:try_start_11 .. :try_end_11} :catch_a

    .line 361
    :cond_e
    :goto_6
    throw v32

    .line 359
    :catch_a
    move-exception v11

    .line 360
    .restart local v11    # "e":Ljava/io/IOException;
    invoke-virtual {v11}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_6

    .line 371
    .end local v11    # "e":Ljava/io/IOException;
    .restart local v5    # "buffer1":B
    .restart local v7    # "bufferProvince":[B
    .restart local v21    # "myFile":Ljava/io/File;
    .restart local v26    # "proindex":I
    :cond_f
    if-eqz v28, :cond_10

    .line 372
    move-object/from16 v25, v28

    goto/16 :goto_5

    .line 374
    :cond_10
    const-string v25, ""

    goto/16 :goto_5
.end method
