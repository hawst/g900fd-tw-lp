.class public Lcom/android/providers/contacts/util/CursorIndexer;
.super Landroid/database/CursorWrapper;
.source "CursorIndexer.java"


# instance fields
.field private mVirtureCurrentPos:I

.field private mVirturePosArray:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Landroid/database/Cursor;Ljava/lang/String;)V
    .locals 23
    .param p1, "cursor"    # Landroid/database/Cursor;
    .param p2, "filter"    # Ljava/lang/String;

    .prologue
    .line 21
    invoke-direct/range {p0 .. p1}, Landroid/database/CursorWrapper;-><init>(Landroid/database/Cursor;)V

    .line 16
    new-instance v21, Ljava/util/ArrayList;

    invoke-direct/range {v21 .. v21}, Ljava/util/ArrayList;-><init>()V

    move-object/from16 v0, v21

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/android/providers/contacts/util/CursorIndexer;->mVirturePosArray:Ljava/util/ArrayList;

    .line 25
    invoke-virtual/range {p2 .. p2}, Ljava/lang/String;->isEmpty()Z

    move-result v21

    if-eqz v21, :cond_0

    .line 26
    invoke-interface/range {p1 .. p1}, Landroid/database/Cursor;->getCount()I

    move-result v4

    .line 27
    .local v4, "cursorCnt":I
    const/4 v6, 0x0

    .local v6, "i":I
    :goto_0
    if-ge v6, v4, :cond_b

    .line 28
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/providers/contacts/util/CursorIndexer;->mVirturePosArray:Ljava/util/ArrayList;

    move-object/from16 v21, v0

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v22

    invoke-virtual/range {v21 .. v22}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 27
    add-int/lit8 v6, v6, 0x1

    goto :goto_0

    .line 34
    .end local v4    # "cursorCnt":I
    .end local v6    # "i":I
    :cond_0
    const/16 v21, 0x2

    move/from16 v0, v21

    new-array v8, v0, [I

    .line 37
    .local v8, "indexes":[I
    new-instance v12, Ljava/util/ArrayList;

    invoke-direct {v12}, Ljava/util/ArrayList;-><init>()V

    .line 38
    .local v12, "result0":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Integer;>;"
    new-instance v13, Ljava/util/ArrayList;

    invoke-direct {v13}, Ljava/util/ArrayList;-><init>()V

    .line 39
    .local v13, "result1":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Integer;>;"
    new-instance v14, Ljava/util/ArrayList;

    invoke-direct {v14}, Ljava/util/ArrayList;-><init>()V

    .line 40
    .local v14, "result2":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Integer;>;"
    new-instance v15, Ljava/util/ArrayList;

    invoke-direct {v15}, Ljava/util/ArrayList;-><init>()V

    .line 41
    .local v15, "result3":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Integer;>;"
    new-instance v16, Ljava/util/ArrayList;

    invoke-direct/range {v16 .. v16}, Ljava/util/ArrayList;-><init>()V

    .line 42
    .local v16, "result4":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Integer;>;"
    new-instance v17, Ljava/util/ArrayList;

    invoke-direct/range {v17 .. v17}, Ljava/util/ArrayList;-><init>()V

    .line 43
    .local v17, "result5":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Integer;>;"
    new-instance v18, Ljava/util/ArrayList;

    invoke-direct/range {v18 .. v18}, Ljava/util/ArrayList;-><init>()V

    .line 44
    .local v18, "result6":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Integer;>;"
    new-instance v20, Ljava/util/ArrayList;

    invoke-direct/range {v20 .. v20}, Ljava/util/ArrayList;-><init>()V

    .line 46
    .local v20, "result_none":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Integer;>;"
    const/16 v21, 0x8

    move/from16 v0, v21

    new-array v0, v0, [Ljava/util/ArrayList;

    move-object/from16 v19, v0

    .line 48
    .local v19, "resultList":[Ljava/util/ArrayList;
    const/16 v21, 0x0

    aput-object v12, v19, v21

    .line 49
    const/16 v21, 0x1

    aput-object v13, v19, v21

    .line 50
    const/16 v21, 0x2

    aput-object v14, v19, v21

    .line 51
    const/16 v21, 0x3

    aput-object v15, v19, v21

    .line 52
    const/16 v21, 0x4

    aput-object v16, v19, v21

    .line 53
    const/16 v21, 0x5

    aput-object v17, v19, v21

    .line 54
    const/16 v21, 0x6

    aput-object v18, v19, v21

    .line 55
    const/16 v21, 0x7

    aput-object v20, v19, v21

    .line 58
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    .line 60
    .local v2, "candidate":Ljava/lang/StringBuilder;
    sget-object v21, Ljava/util/Locale;->KOREAN:Ljava/util/Locale;

    invoke-virtual/range {v21 .. v21}, Ljava/util/Locale;->getLanguage()Ljava/lang/String;

    move-result-object v21

    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v22

    invoke-virtual/range {v22 .. v22}, Ljava/util/Locale;->getLanguage()Ljava/lang/String;

    move-result-object v22

    invoke-virtual/range {v21 .. v22}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v21

    if-eqz v21, :cond_2

    .line 61
    const-string v21, "display_name_alt"

    move-object/from16 v0, p1

    move-object/from16 v1, v21

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v3

    .line 62
    .local v3, "columnIndex":I
    const/16 v21, -0x1

    move/from16 v0, v21

    if-ne v3, v0, :cond_1

    .line 63
    const-string v21, "display_name"

    move-object/from16 v0, p1

    move-object/from16 v1, v21

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v3

    .line 69
    :cond_1
    :goto_1
    const/16 v21, -0x1

    move/from16 v0, v21

    if-ne v3, v0, :cond_3

    .line 70
    const/4 v7, 0x0

    .local v7, "idx":I
    :goto_2
    invoke-interface/range {p1 .. p1}, Landroid/database/Cursor;->getCount()I

    move-result v21

    move/from16 v0, v21

    if-ge v7, v0, :cond_a

    .line 71
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/providers/contacts/util/CursorIndexer;->mVirturePosArray:Ljava/util/ArrayList;

    move-object/from16 v21, v0

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v22

    invoke-virtual/range {v21 .. v22}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 70
    add-int/lit8 v7, v7, 0x1

    goto :goto_2

    .line 66
    .end local v3    # "columnIndex":I
    .end local v7    # "idx":I
    :cond_2
    const-string v21, "display_name"

    move-object/from16 v0, p1

    move-object/from16 v1, v21

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v3

    .restart local v3    # "columnIndex":I
    goto :goto_1

    .line 74
    :cond_3
    invoke-virtual/range {p2 .. p2}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v11

    .line 75
    .local v11, "reg":Ljava/lang/String;
    const/4 v6, 0x0

    .restart local v6    # "i":I
    :goto_3
    invoke-virtual {v11}, Ljava/lang/String;->length()I

    move-result v21

    move/from16 v0, v21

    if-ge v6, v0, :cond_5

    .line 76
    invoke-virtual {v11, v6}, Ljava/lang/String;->charAt(I)C

    move-result v21

    invoke-static/range {v21 .. v21}, Ljava/lang/Character;->isLetterOrDigit(C)Z

    move-result v21

    if-eqz v21, :cond_4

    .line 77
    invoke-virtual {v11, v6}, Ljava/lang/String;->charAt(I)C

    move-result v21

    move/from16 v0, v21

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 75
    :goto_4
    add-int/lit8 v6, v6, 0x1

    goto :goto_3

    .line 79
    :cond_4
    const-string v21, "\\"

    move-object/from16 v0, v21

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v21

    invoke-virtual {v11, v6}, Ljava/lang/String;->charAt(I)C

    move-result v22

    invoke-virtual/range {v21 .. v22}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    goto :goto_4

    .line 82
    :cond_5
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v21

    invoke-static/range {v21 .. v21}, Lcom/android/providers/contacts/util/KoreanPatternHelper;->getPattern(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v10

    .line 83
    .local v10, "patternStr":Ljava/lang/String;
    new-instance v21, Ljava/lang/StringBuilder;

    invoke-direct/range {v21 .. v21}, Ljava/lang/StringBuilder;-><init>()V

    const-string v22, "("

    invoke-virtual/range {v21 .. v22}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v21

    move-object/from16 v0, v21

    invoke-virtual {v0, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v21

    const-string v22, ")"

    invoke-virtual/range {v21 .. v22}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v21

    invoke-virtual/range {v21 .. v21}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v21

    invoke-static/range {v21 .. v21}, Ljava/util/regex/Pattern;->compile(Ljava/lang/String;)Ljava/util/regex/Pattern;

    move-result-object v9

    .line 85
    .local v9, "pattern":Ljava/util/regex/Pattern;
    invoke-interface/range {p1 .. p1}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v21

    if-eqz v21, :cond_8

    .line 86
    const/4 v7, 0x0

    .line 88
    .restart local v7    # "idx":I
    :cond_6
    move-object/from16 v0, p1

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v5

    .line 89
    .local v5, "display_name":Ljava/lang/String;
    if-eqz v5, :cond_9

    move-object/from16 v0, p0

    invoke-virtual {v0, v5, v9, v8}, Lcom/android/providers/contacts/util/CursorIndexer;->findPos(Ljava/lang/String;Ljava/util/regex/Pattern;[I)Z

    move-result v21

    if-eqz v21, :cond_9

    .line 90
    const/16 v21, 0x0

    aget v21, v8, v21

    const/16 v22, 0x7

    move/from16 v0, v21

    move/from16 v1, v22

    if-le v0, v1, :cond_7

    .line 91
    const/16 v21, 0x0

    const/16 v22, 0x7

    aput v22, v8, v21

    .line 96
    :cond_7
    :goto_5
    const/16 v21, 0x0

    aget v21, v8, v21

    aget-object v21, v19, v21

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v22

    invoke-virtual/range {v21 .. v22}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 97
    add-int/lit8 v7, v7, 0x1

    .line 98
    invoke-interface/range {p1 .. p1}, Landroid/database/Cursor;->moveToNext()Z

    move-result v21

    if-nez v21, :cond_6

    .line 101
    .end local v5    # "display_name":Ljava/lang/String;
    .end local v7    # "idx":I
    :cond_8
    const/4 v6, 0x0

    :goto_6
    const/16 v21, 0x8

    move/from16 v0, v21

    if-ge v6, v0, :cond_a

    .line 102
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/providers/contacts/util/CursorIndexer;->mVirturePosArray:Ljava/util/ArrayList;

    move-object/from16 v21, v0

    aget-object v22, v19, v6

    invoke-virtual/range {v21 .. v22}, Ljava/util/ArrayList;->addAll(Ljava/util/Collection;)Z

    .line 101
    add-int/lit8 v6, v6, 0x1

    goto :goto_6

    .line 94
    .restart local v5    # "display_name":Ljava/lang/String;
    .restart local v7    # "idx":I
    :cond_9
    const/16 v21, 0x0

    const/16 v22, 0x7

    aput v22, v8, v21

    goto :goto_5

    .line 106
    .end local v5    # "display_name":Ljava/lang/String;
    .end local v6    # "i":I
    .end local v7    # "idx":I
    .end local v9    # "pattern":Ljava/util/regex/Pattern;
    .end local v10    # "patternStr":Ljava/lang/String;
    .end local v11    # "reg":Ljava/lang/String;
    :cond_a
    const/16 v21, -0x1

    move/from16 v0, v21

    move-object/from16 v1, p0

    iput v0, v1, Lcom/android/providers/contacts/util/CursorIndexer;->mVirtureCurrentPos:I

    .line 107
    .end local v2    # "candidate":Ljava/lang/StringBuilder;
    .end local v3    # "columnIndex":I
    .end local v8    # "indexes":[I
    .end local v12    # "result0":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Integer;>;"
    .end local v13    # "result1":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Integer;>;"
    .end local v14    # "result2":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Integer;>;"
    .end local v15    # "result3":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Integer;>;"
    .end local v16    # "result4":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Integer;>;"
    .end local v17    # "result5":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Integer;>;"
    .end local v18    # "result6":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Integer;>;"
    .end local v19    # "resultList":[Ljava/util/ArrayList;
    .end local v20    # "result_none":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Integer;>;"
    :cond_b
    return-void
.end method


# virtual methods
.method public findPos(Ljava/lang/String;Ljava/util/regex/Pattern;[I)Z
    .locals 6
    .param p1, "target"    # Ljava/lang/String;
    .param p2, "pattern"    # Ljava/util/regex/Pattern;
    .param p3, "indexes"    # [I

    .prologue
    const/4 v3, 0x0

    .line 111
    const/4 v2, 0x0

    .line 112
    .local v2, "result":Z
    invoke-virtual {p1}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object p1

    .line 115
    :try_start_0
    invoke-virtual {p2, p1}, Ljava/util/regex/Pattern;->matcher(Ljava/lang/CharSequence;)Ljava/util/regex/Matcher;

    move-result-object v1

    .line 116
    .local v1, "m":Ljava/util/regex/Matcher;
    invoke-virtual {v1}, Ljava/util/regex/Matcher;->find()Z

    move-result v4

    if-eqz v4, :cond_1

    .line 117
    invoke-virtual {v1}, Ljava/util/regex/Matcher;->start()I

    move-result v4

    invoke-virtual {v1}, Ljava/util/regex/Matcher;->end()I

    move-result v5

    if-ne v4, v5, :cond_0

    .line 127
    .end local v1    # "m":Ljava/util/regex/Matcher;
    :goto_0
    return v3

    .line 120
    .restart local v1    # "m":Ljava/util/regex/Matcher;
    :cond_0
    const/4 v3, 0x0

    invoke-virtual {v1}, Ljava/util/regex/Matcher;->start()I

    move-result v4

    aput v4, p3, v3

    .line 121
    const/4 v3, 0x1

    invoke-virtual {v1}, Ljava/util/regex/Matcher;->end()I

    move-result v4

    aput v4, p3, v3
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 122
    const/4 v2, 0x1

    .end local v1    # "m":Ljava/util/regex/Matcher;
    :cond_1
    :goto_1
    move v3, v2

    .line 127
    goto :goto_0

    .line 124
    :catch_0
    move-exception v0

    .line 125
    .local v0, "e":Ljava/lang/Exception;
    const-string v3, "CursorIndexer"

    const-string v4, "search highlight exception!"

    invoke-static {v3, v4}, Landroid/util/secutil/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1
.end method

.method public getPosition()I
    .locals 1

    .prologue
    .line 188
    iget v0, p0, Lcom/android/providers/contacts/util/CursorIndexer;->mVirtureCurrentPos:I

    return v0
.end method

.method public isAfterLast()Z
    .locals 2

    .prologue
    .line 194
    iget v0, p0, Lcom/android/providers/contacts/util/CursorIndexer;->mVirtureCurrentPos:I

    add-int/lit8 v0, v0, 0x2

    iget-object v1, p0, Lcom/android/providers/contacts/util/CursorIndexer;->mVirturePosArray:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    if-ne v0, v1, :cond_0

    .line 195
    const/4 v0, 0x1

    .line 197
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isBeforeFirst()Z
    .locals 1

    .prologue
    .line 205
    iget v0, p0, Lcom/android/providers/contacts/util/CursorIndexer;->mVirtureCurrentPos:I

    add-int/lit8 v0, v0, -0x1

    if-nez v0, :cond_0

    .line 206
    const/4 v0, 0x1

    .line 208
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isFirst()Z
    .locals 1

    .prologue
    .line 215
    iget v0, p0, Lcom/android/providers/contacts/util/CursorIndexer;->mVirtureCurrentPos:I

    if-nez v0, :cond_0

    .line 216
    const/4 v0, 0x1

    .line 218
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isLast()Z
    .locals 2

    .prologue
    .line 226
    iget v0, p0, Lcom/android/providers/contacts/util/CursorIndexer;->mVirtureCurrentPos:I

    add-int/lit8 v0, v0, -0x1

    iget-object v1, p0, Lcom/android/providers/contacts/util/CursorIndexer;->mVirturePosArray:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    if-ne v0, v1, :cond_0

    .line 227
    const/4 v0, 0x1

    .line 229
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public move(I)Z
    .locals 2
    .param p1, "offset"    # I

    .prologue
    .line 132
    iget v0, p0, Lcom/android/providers/contacts/util/CursorIndexer;->mVirtureCurrentPos:I

    add-int/2addr v0, p1

    iget-object v1, p0, Lcom/android/providers/contacts/util/CursorIndexer;->mVirturePosArray:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    if-lt v0, v1, :cond_0

    .line 133
    const/4 v0, 0x0

    .line 137
    :goto_0
    return v0

    .line 135
    :cond_0
    iget v0, p0, Lcom/android/providers/contacts/util/CursorIndexer;->mVirtureCurrentPos:I

    add-int/2addr v0, p1

    iput v0, p0, Lcom/android/providers/contacts/util/CursorIndexer;->mVirtureCurrentPos:I

    .line 137
    iget-object v0, p0, Lcom/android/providers/contacts/util/CursorIndexer;->mVirturePosArray:Ljava/util/ArrayList;

    iget v1, p0, Lcom/android/providers/contacts/util/CursorIndexer;->mVirtureCurrentPos:I

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    invoke-super {p0, v0}, Landroid/database/CursorWrapper;->moveToPosition(I)Z

    move-result v0

    goto :goto_0
.end method

.method public moveToFirst()Z
    .locals 2

    .prologue
    const/4 v0, 0x0

    .line 142
    iget-object v1, p0, Lcom/android/providers/contacts/util/CursorIndexer;->mVirturePosArray:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    if-nez v1, :cond_0

    .line 146
    :goto_0
    return v0

    .line 145
    :cond_0
    iput v0, p0, Lcom/android/providers/contacts/util/CursorIndexer;->mVirtureCurrentPos:I

    .line 146
    iget-object v0, p0, Lcom/android/providers/contacts/util/CursorIndexer;->mVirturePosArray:Ljava/util/ArrayList;

    iget v1, p0, Lcom/android/providers/contacts/util/CursorIndexer;->mVirtureCurrentPos:I

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    invoke-super {p0, v0}, Landroid/database/CursorWrapper;->moveToPosition(I)Z

    move-result v0

    goto :goto_0
.end method

.method public moveToLast()Z
    .locals 2

    .prologue
    .line 151
    iget-object v0, p0, Lcom/android/providers/contacts/util/CursorIndexer;->mVirturePosArray:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-nez v0, :cond_0

    .line 152
    const/4 v0, 0x0

    .line 155
    :goto_0
    return v0

    .line 154
    :cond_0
    iget-object v0, p0, Lcom/android/providers/contacts/util/CursorIndexer;->mVirturePosArray:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    iput v0, p0, Lcom/android/providers/contacts/util/CursorIndexer;->mVirtureCurrentPos:I

    .line 155
    iget-object v0, p0, Lcom/android/providers/contacts/util/CursorIndexer;->mVirturePosArray:Ljava/util/ArrayList;

    iget v1, p0, Lcom/android/providers/contacts/util/CursorIndexer;->mVirtureCurrentPos:I

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    invoke-super {p0, v0}, Landroid/database/CursorWrapper;->moveToPosition(I)Z

    move-result v0

    goto :goto_0
.end method

.method public moveToNext()Z
    .locals 2

    .prologue
    .line 160
    iget v0, p0, Lcom/android/providers/contacts/util/CursorIndexer;->mVirtureCurrentPos:I

    add-int/lit8 v0, v0, 0x1

    iget-object v1, p0, Lcom/android/providers/contacts/util/CursorIndexer;->mVirturePosArray:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    if-lt v0, v1, :cond_0

    .line 161
    const/4 v0, 0x0

    .line 164
    :goto_0
    return v0

    .line 163
    :cond_0
    iget v0, p0, Lcom/android/providers/contacts/util/CursorIndexer;->mVirtureCurrentPos:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/android/providers/contacts/util/CursorIndexer;->mVirtureCurrentPos:I

    .line 164
    iget-object v0, p0, Lcom/android/providers/contacts/util/CursorIndexer;->mVirturePosArray:Ljava/util/ArrayList;

    iget v1, p0, Lcom/android/providers/contacts/util/CursorIndexer;->mVirtureCurrentPos:I

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    invoke-super {p0, v0}, Landroid/database/CursorWrapper;->moveToPosition(I)Z

    move-result v0

    goto :goto_0
.end method

.method public moveToPosition(I)Z
    .locals 2
    .param p1, "position"    # I

    .prologue
    .line 169
    iget-object v0, p0, Lcom/android/providers/contacts/util/CursorIndexer;->mVirturePosArray:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-lt p1, v0, :cond_0

    .line 170
    const/4 v0, 0x0

    .line 173
    :goto_0
    return v0

    .line 172
    :cond_0
    iput p1, p0, Lcom/android/providers/contacts/util/CursorIndexer;->mVirtureCurrentPos:I

    .line 173
    iget-object v0, p0, Lcom/android/providers/contacts/util/CursorIndexer;->mVirturePosArray:Ljava/util/ArrayList;

    iget v1, p0, Lcom/android/providers/contacts/util/CursorIndexer;->mVirtureCurrentPos:I

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    invoke-super {p0, v0}, Landroid/database/CursorWrapper;->moveToPosition(I)Z

    move-result v0

    goto :goto_0
.end method

.method public moveToPrevious()Z
    .locals 2

    .prologue
    .line 178
    iget v0, p0, Lcom/android/providers/contacts/util/CursorIndexer;->mVirtureCurrentPos:I

    if-nez v0, :cond_0

    .line 179
    const/4 v0, 0x0

    .line 182
    :goto_0
    return v0

    .line 181
    :cond_0
    iget v0, p0, Lcom/android/providers/contacts/util/CursorIndexer;->mVirtureCurrentPos:I

    add-int/lit8 v0, v0, -0x1

    iput v0, p0, Lcom/android/providers/contacts/util/CursorIndexer;->mVirtureCurrentPos:I

    .line 182
    iget-object v0, p0, Lcom/android/providers/contacts/util/CursorIndexer;->mVirturePosArray:Ljava/util/ArrayList;

    iget v1, p0, Lcom/android/providers/contacts/util/CursorIndexer;->mVirtureCurrentPos:I

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    invoke-super {p0, v0}, Landroid/database/CursorWrapper;->moveToPosition(I)Z

    move-result v0

    goto :goto_0
.end method
