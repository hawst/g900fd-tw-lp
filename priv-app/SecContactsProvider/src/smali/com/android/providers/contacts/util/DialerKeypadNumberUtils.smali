.class public Lcom/android/providers/contacts/util/DialerKeypadNumberUtils;
.super Ljava/lang/Object;
.source "DialerKeypadNumberUtils.java"


# static fields
.field private static final DEBUG_LOGGING:Z

.field private static final LATIN_EXTENDED_KEYPAD_MAP:Landroid/util/SparseIntArray;


# direct methods
.method static constructor <clinit>()V
    .locals 8

    .prologue
    const/16 v7, 0x38

    const/16 v6, 0x36

    const/16 v5, 0x34

    const/16 v4, 0x33

    const/16 v3, 0x32

    .line 33
    const-string v0, "DialerKeypadNumberUtils"

    const/4 v1, 0x3

    invoke-static {v0, v1}, Landroid/util/secutil/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    sput-boolean v0, Lcom/android/providers/contacts/util/DialerKeypadNumberUtils;->DEBUG_LOGGING:Z

    .line 38
    new-instance v0, Landroid/util/SparseIntArray;

    invoke-direct {v0}, Landroid/util/SparseIntArray;-><init>()V

    sput-object v0, Lcom/android/providers/contacts/util/DialerKeypadNumberUtils;->LATIN_EXTENDED_KEYPAD_MAP:Landroid/util/SparseIntArray;

    .line 40
    sget-object v0, Lcom/android/providers/contacts/util/DialerKeypadNumberUtils;->LATIN_EXTENDED_KEYPAD_MAP:Landroid/util/SparseIntArray;

    const/16 v1, 0x30

    const/16 v2, 0x30

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseIntArray;->put(II)V

    .line 42
    sget-object v0, Lcom/android/providers/contacts/util/DialerKeypadNumberUtils;->LATIN_EXTENDED_KEYPAD_MAP:Landroid/util/SparseIntArray;

    const/16 v1, 0x31

    const/16 v2, 0x31

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseIntArray;->put(II)V

    .line 44
    sget-object v0, Lcom/android/providers/contacts/util/DialerKeypadNumberUtils;->LATIN_EXTENDED_KEYPAD_MAP:Landroid/util/SparseIntArray;

    invoke-virtual {v0, v3, v3}, Landroid/util/SparseIntArray;->put(II)V

    sget-object v0, Lcom/android/providers/contacts/util/DialerKeypadNumberUtils;->LATIN_EXTENDED_KEYPAD_MAP:Landroid/util/SparseIntArray;

    const/16 v1, 0x61

    invoke-virtual {v0, v1, v3}, Landroid/util/SparseIntArray;->put(II)V

    .line 45
    sget-object v0, Lcom/android/providers/contacts/util/DialerKeypadNumberUtils;->LATIN_EXTENDED_KEYPAD_MAP:Landroid/util/SparseIntArray;

    const/16 v1, 0x62

    invoke-virtual {v0, v1, v3}, Landroid/util/SparseIntArray;->put(II)V

    sget-object v0, Lcom/android/providers/contacts/util/DialerKeypadNumberUtils;->LATIN_EXTENDED_KEYPAD_MAP:Landroid/util/SparseIntArray;

    const/16 v1, 0x63

    invoke-virtual {v0, v1, v3}, Landroid/util/SparseIntArray;->put(II)V

    .line 47
    sget-object v0, Lcom/android/providers/contacts/util/DialerKeypadNumberUtils;->LATIN_EXTENDED_KEYPAD_MAP:Landroid/util/SparseIntArray;

    const/16 v1, 0xe0

    invoke-virtual {v0, v1, v3}, Landroid/util/SparseIntArray;->put(II)V

    sget-object v0, Lcom/android/providers/contacts/util/DialerKeypadNumberUtils;->LATIN_EXTENDED_KEYPAD_MAP:Landroid/util/SparseIntArray;

    const/16 v1, 0xe1

    invoke-virtual {v0, v1, v3}, Landroid/util/SparseIntArray;->put(II)V

    .line 48
    sget-object v0, Lcom/android/providers/contacts/util/DialerKeypadNumberUtils;->LATIN_EXTENDED_KEYPAD_MAP:Landroid/util/SparseIntArray;

    const/16 v1, 0xe2

    invoke-virtual {v0, v1, v3}, Landroid/util/SparseIntArray;->put(II)V

    sget-object v0, Lcom/android/providers/contacts/util/DialerKeypadNumberUtils;->LATIN_EXTENDED_KEYPAD_MAP:Landroid/util/SparseIntArray;

    const/16 v1, 0xe3

    invoke-virtual {v0, v1, v3}, Landroid/util/SparseIntArray;->put(II)V

    .line 49
    sget-object v0, Lcom/android/providers/contacts/util/DialerKeypadNumberUtils;->LATIN_EXTENDED_KEYPAD_MAP:Landroid/util/SparseIntArray;

    const/16 v1, 0xe4

    invoke-virtual {v0, v1, v3}, Landroid/util/SparseIntArray;->put(II)V

    sget-object v0, Lcom/android/providers/contacts/util/DialerKeypadNumberUtils;->LATIN_EXTENDED_KEYPAD_MAP:Landroid/util/SparseIntArray;

    const/16 v1, 0xe5

    invoke-virtual {v0, v1, v3}, Landroid/util/SparseIntArray;->put(II)V

    .line 50
    sget-object v0, Lcom/android/providers/contacts/util/DialerKeypadNumberUtils;->LATIN_EXTENDED_KEYPAD_MAP:Landroid/util/SparseIntArray;

    const/16 v1, 0xe6

    invoke-virtual {v0, v1, v3}, Landroid/util/SparseIntArray;->put(II)V

    .line 52
    sget-object v0, Lcom/android/providers/contacts/util/DialerKeypadNumberUtils;->LATIN_EXTENDED_KEYPAD_MAP:Landroid/util/SparseIntArray;

    const/16 v1, 0x101

    invoke-virtual {v0, v1, v3}, Landroid/util/SparseIntArray;->put(II)V

    sget-object v0, Lcom/android/providers/contacts/util/DialerKeypadNumberUtils;->LATIN_EXTENDED_KEYPAD_MAP:Landroid/util/SparseIntArray;

    const/16 v1, 0x103

    invoke-virtual {v0, v1, v3}, Landroid/util/SparseIntArray;->put(II)V

    .line 53
    sget-object v0, Lcom/android/providers/contacts/util/DialerKeypadNumberUtils;->LATIN_EXTENDED_KEYPAD_MAP:Landroid/util/SparseIntArray;

    const/16 v1, 0x105

    invoke-virtual {v0, v1, v3}, Landroid/util/SparseIntArray;->put(II)V

    .line 54
    sget-object v0, Lcom/android/providers/contacts/util/DialerKeypadNumberUtils;->LATIN_EXTENDED_KEYPAD_MAP:Landroid/util/SparseIntArray;

    const/16 v1, 0xe7

    invoke-virtual {v0, v1, v3}, Landroid/util/SparseIntArray;->put(II)V

    .line 56
    sget-object v0, Lcom/android/providers/contacts/util/DialerKeypadNumberUtils;->LATIN_EXTENDED_KEYPAD_MAP:Landroid/util/SparseIntArray;

    const/16 v1, 0x107

    invoke-virtual {v0, v1, v3}, Landroid/util/SparseIntArray;->put(II)V

    sget-object v0, Lcom/android/providers/contacts/util/DialerKeypadNumberUtils;->LATIN_EXTENDED_KEYPAD_MAP:Landroid/util/SparseIntArray;

    const/16 v1, 0x10d

    invoke-virtual {v0, v1, v3}, Landroid/util/SparseIntArray;->put(II)V

    .line 58
    sget-object v0, Lcom/android/providers/contacts/util/DialerKeypadNumberUtils;->LATIN_EXTENDED_KEYPAD_MAP:Landroid/util/SparseIntArray;

    invoke-virtual {v0, v4, v4}, Landroid/util/SparseIntArray;->put(II)V

    sget-object v0, Lcom/android/providers/contacts/util/DialerKeypadNumberUtils;->LATIN_EXTENDED_KEYPAD_MAP:Landroid/util/SparseIntArray;

    const/16 v1, 0x64

    invoke-virtual {v0, v1, v4}, Landroid/util/SparseIntArray;->put(II)V

    .line 59
    sget-object v0, Lcom/android/providers/contacts/util/DialerKeypadNumberUtils;->LATIN_EXTENDED_KEYPAD_MAP:Landroid/util/SparseIntArray;

    const/16 v1, 0x65

    invoke-virtual {v0, v1, v4}, Landroid/util/SparseIntArray;->put(II)V

    sget-object v0, Lcom/android/providers/contacts/util/DialerKeypadNumberUtils;->LATIN_EXTENDED_KEYPAD_MAP:Landroid/util/SparseIntArray;

    const/16 v1, 0x66

    invoke-virtual {v0, v1, v4}, Landroid/util/SparseIntArray;->put(II)V

    .line 61
    sget-object v0, Lcom/android/providers/contacts/util/DialerKeypadNumberUtils;->LATIN_EXTENDED_KEYPAD_MAP:Landroid/util/SparseIntArray;

    const/16 v1, 0x18b

    invoke-virtual {v0, v1, v4}, Landroid/util/SparseIntArray;->put(II)V

    sget-object v0, Lcom/android/providers/contacts/util/DialerKeypadNumberUtils;->LATIN_EXTENDED_KEYPAD_MAP:Landroid/util/SparseIntArray;

    const/16 v1, 0x18c

    invoke-virtual {v0, v1, v4}, Landroid/util/SparseIntArray;->put(II)V

    .line 63
    sget-object v0, Lcom/android/providers/contacts/util/DialerKeypadNumberUtils;->LATIN_EXTENDED_KEYPAD_MAP:Landroid/util/SparseIntArray;

    const/16 v1, 0xe8

    invoke-virtual {v0, v1, v4}, Landroid/util/SparseIntArray;->put(II)V

    sget-object v0, Lcom/android/providers/contacts/util/DialerKeypadNumberUtils;->LATIN_EXTENDED_KEYPAD_MAP:Landroid/util/SparseIntArray;

    const/16 v1, 0xe9

    invoke-virtual {v0, v1, v4}, Landroid/util/SparseIntArray;->put(II)V

    .line 64
    sget-object v0, Lcom/android/providers/contacts/util/DialerKeypadNumberUtils;->LATIN_EXTENDED_KEYPAD_MAP:Landroid/util/SparseIntArray;

    const/16 v1, 0xea

    invoke-virtual {v0, v1, v4}, Landroid/util/SparseIntArray;->put(II)V

    sget-object v0, Lcom/android/providers/contacts/util/DialerKeypadNumberUtils;->LATIN_EXTENDED_KEYPAD_MAP:Landroid/util/SparseIntArray;

    const/16 v1, 0xea

    invoke-virtual {v0, v1, v4}, Landroid/util/SparseIntArray;->put(II)V

    .line 65
    sget-object v0, Lcom/android/providers/contacts/util/DialerKeypadNumberUtils;->LATIN_EXTENDED_KEYPAD_MAP:Landroid/util/SparseIntArray;

    const/16 v1, 0xeb

    invoke-virtual {v0, v1, v4}, Landroid/util/SparseIntArray;->put(II)V

    .line 67
    sget-object v0, Lcom/android/providers/contacts/util/DialerKeypadNumberUtils;->LATIN_EXTENDED_KEYPAD_MAP:Landroid/util/SparseIntArray;

    const/16 v1, 0x113

    invoke-virtual {v0, v1, v4}, Landroid/util/SparseIntArray;->put(II)V

    sget-object v0, Lcom/android/providers/contacts/util/DialerKeypadNumberUtils;->LATIN_EXTENDED_KEYPAD_MAP:Landroid/util/SparseIntArray;

    const/16 v1, 0x115

    invoke-virtual {v0, v1, v4}, Landroid/util/SparseIntArray;->put(II)V

    .line 68
    sget-object v0, Lcom/android/providers/contacts/util/DialerKeypadNumberUtils;->LATIN_EXTENDED_KEYPAD_MAP:Landroid/util/SparseIntArray;

    const/16 v1, 0x119

    invoke-virtual {v0, v1, v4}, Landroid/util/SparseIntArray;->put(II)V

    .line 69
    sget-object v0, Lcom/android/providers/contacts/util/DialerKeypadNumberUtils;->LATIN_EXTENDED_KEYPAD_MAP:Landroid/util/SparseIntArray;

    const/16 v1, 0x1dd

    invoke-virtual {v0, v1, v4}, Landroid/util/SparseIntArray;->put(II)V

    .line 71
    sget-object v0, Lcom/android/providers/contacts/util/DialerKeypadNumberUtils;->LATIN_EXTENDED_KEYPAD_MAP:Landroid/util/SparseIntArray;

    invoke-virtual {v0, v5, v5}, Landroid/util/SparseIntArray;->put(II)V

    sget-object v0, Lcom/android/providers/contacts/util/DialerKeypadNumberUtils;->LATIN_EXTENDED_KEYPAD_MAP:Landroid/util/SparseIntArray;

    const/16 v1, 0x67

    invoke-virtual {v0, v1, v5}, Landroid/util/SparseIntArray;->put(II)V

    .line 72
    sget-object v0, Lcom/android/providers/contacts/util/DialerKeypadNumberUtils;->LATIN_EXTENDED_KEYPAD_MAP:Landroid/util/SparseIntArray;

    const/16 v1, 0x68

    invoke-virtual {v0, v1, v5}, Landroid/util/SparseIntArray;->put(II)V

    sget-object v0, Lcom/android/providers/contacts/util/DialerKeypadNumberUtils;->LATIN_EXTENDED_KEYPAD_MAP:Landroid/util/SparseIntArray;

    const/16 v1, 0x69

    invoke-virtual {v0, v1, v5}, Landroid/util/SparseIntArray;->put(II)V

    .line 73
    sget-object v0, Lcom/android/providers/contacts/util/DialerKeypadNumberUtils;->LATIN_EXTENDED_KEYPAD_MAP:Landroid/util/SparseIntArray;

    const/16 v1, 0x121

    invoke-virtual {v0, v1, v5}, Landroid/util/SparseIntArray;->put(II)V

    .line 74
    sget-object v0, Lcom/android/providers/contacts/util/DialerKeypadNumberUtils;->LATIN_EXTENDED_KEYPAD_MAP:Landroid/util/SparseIntArray;

    const/16 v1, 0x1e7

    invoke-virtual {v0, v1, v5}, Landroid/util/SparseIntArray;->put(II)V

    .line 76
    sget-object v0, Lcom/android/providers/contacts/util/DialerKeypadNumberUtils;->LATIN_EXTENDED_KEYPAD_MAP:Landroid/util/SparseIntArray;

    const/16 v1, 0xec

    invoke-virtual {v0, v1, v5}, Landroid/util/SparseIntArray;->put(II)V

    sget-object v0, Lcom/android/providers/contacts/util/DialerKeypadNumberUtils;->LATIN_EXTENDED_KEYPAD_MAP:Landroid/util/SparseIntArray;

    const/16 v1, 0xed

    invoke-virtual {v0, v1, v5}, Landroid/util/SparseIntArray;->put(II)V

    .line 77
    sget-object v0, Lcom/android/providers/contacts/util/DialerKeypadNumberUtils;->LATIN_EXTENDED_KEYPAD_MAP:Landroid/util/SparseIntArray;

    const/16 v1, 0xee

    invoke-virtual {v0, v1, v5}, Landroid/util/SparseIntArray;->put(II)V

    sget-object v0, Lcom/android/providers/contacts/util/DialerKeypadNumberUtils;->LATIN_EXTENDED_KEYPAD_MAP:Landroid/util/SparseIntArray;

    const/16 v1, 0xef

    invoke-virtual {v0, v1, v5}, Landroid/util/SparseIntArray;->put(II)V

    .line 79
    sget-object v0, Lcom/android/providers/contacts/util/DialerKeypadNumberUtils;->LATIN_EXTENDED_KEYPAD_MAP:Landroid/util/SparseIntArray;

    const/16 v1, 0x129

    invoke-virtual {v0, v1, v5}, Landroid/util/SparseIntArray;->put(II)V

    sget-object v0, Lcom/android/providers/contacts/util/DialerKeypadNumberUtils;->LATIN_EXTENDED_KEYPAD_MAP:Landroid/util/SparseIntArray;

    const/16 v1, 0x12f

    invoke-virtual {v0, v1, v5}, Landroid/util/SparseIntArray;->put(II)V

    .line 80
    sget-object v0, Lcom/android/providers/contacts/util/DialerKeypadNumberUtils;->LATIN_EXTENDED_KEYPAD_MAP:Landroid/util/SparseIntArray;

    const/16 v1, 0x131

    invoke-virtual {v0, v1, v5}, Landroid/util/SparseIntArray;->put(II)V

    .line 82
    sget-object v0, Lcom/android/providers/contacts/util/DialerKeypadNumberUtils;->LATIN_EXTENDED_KEYPAD_MAP:Landroid/util/SparseIntArray;

    const/16 v1, 0x35

    const/16 v2, 0x35

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseIntArray;->put(II)V

    sget-object v0, Lcom/android/providers/contacts/util/DialerKeypadNumberUtils;->LATIN_EXTENDED_KEYPAD_MAP:Landroid/util/SparseIntArray;

    const/16 v1, 0x6a

    const/16 v2, 0x35

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseIntArray;->put(II)V

    .line 83
    sget-object v0, Lcom/android/providers/contacts/util/DialerKeypadNumberUtils;->LATIN_EXTENDED_KEYPAD_MAP:Landroid/util/SparseIntArray;

    const/16 v1, 0x6b

    const/16 v2, 0x35

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseIntArray;->put(II)V

    sget-object v0, Lcom/android/providers/contacts/util/DialerKeypadNumberUtils;->LATIN_EXTENDED_KEYPAD_MAP:Landroid/util/SparseIntArray;

    const/16 v1, 0x6c

    const/16 v2, 0x35

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseIntArray;->put(II)V

    .line 84
    sget-object v0, Lcom/android/providers/contacts/util/DialerKeypadNumberUtils;->LATIN_EXTENDED_KEYPAD_MAP:Landroid/util/SparseIntArray;

    const/16 v1, 0x137

    const/16 v2, 0x35

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseIntArray;->put(II)V

    .line 86
    sget-object v0, Lcom/android/providers/contacts/util/DialerKeypadNumberUtils;->LATIN_EXTENDED_KEYPAD_MAP:Landroid/util/SparseIntArray;

    const/16 v1, 0x13c

    const/16 v2, 0x35

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseIntArray;->put(II)V

    sget-object v0, Lcom/android/providers/contacts/util/DialerKeypadNumberUtils;->LATIN_EXTENDED_KEYPAD_MAP:Landroid/util/SparseIntArray;

    const/16 v1, 0x142

    const/16 v2, 0x35

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseIntArray;->put(II)V

    .line 88
    sget-object v0, Lcom/android/providers/contacts/util/DialerKeypadNumberUtils;->LATIN_EXTENDED_KEYPAD_MAP:Landroid/util/SparseIntArray;

    invoke-virtual {v0, v6, v6}, Landroid/util/SparseIntArray;->put(II)V

    sget-object v0, Lcom/android/providers/contacts/util/DialerKeypadNumberUtils;->LATIN_EXTENDED_KEYPAD_MAP:Landroid/util/SparseIntArray;

    const/16 v1, 0x6d

    invoke-virtual {v0, v1, v6}, Landroid/util/SparseIntArray;->put(II)V

    .line 89
    sget-object v0, Lcom/android/providers/contacts/util/DialerKeypadNumberUtils;->LATIN_EXTENDED_KEYPAD_MAP:Landroid/util/SparseIntArray;

    const/16 v1, 0x6e

    invoke-virtual {v0, v1, v6}, Landroid/util/SparseIntArray;->put(II)V

    sget-object v0, Lcom/android/providers/contacts/util/DialerKeypadNumberUtils;->LATIN_EXTENDED_KEYPAD_MAP:Landroid/util/SparseIntArray;

    const/16 v1, 0x6f

    invoke-virtual {v0, v1, v6}, Landroid/util/SparseIntArray;->put(II)V

    .line 90
    sget-object v0, Lcom/android/providers/contacts/util/DialerKeypadNumberUtils;->LATIN_EXTENDED_KEYPAD_MAP:Landroid/util/SparseIntArray;

    const/16 v1, 0xf1

    invoke-virtual {v0, v1, v6}, Landroid/util/SparseIntArray;->put(II)V

    .line 92
    sget-object v0, Lcom/android/providers/contacts/util/DialerKeypadNumberUtils;->LATIN_EXTENDED_KEYPAD_MAP:Landroid/util/SparseIntArray;

    const/16 v1, 0x144

    invoke-virtual {v0, v1, v6}, Landroid/util/SparseIntArray;->put(II)V

    sget-object v0, Lcom/android/providers/contacts/util/DialerKeypadNumberUtils;->LATIN_EXTENDED_KEYPAD_MAP:Landroid/util/SparseIntArray;

    const/16 v1, 0x146

    invoke-virtual {v0, v1, v6}, Landroid/util/SparseIntArray;->put(II)V

    .line 94
    sget-object v0, Lcom/android/providers/contacts/util/DialerKeypadNumberUtils;->LATIN_EXTENDED_KEYPAD_MAP:Landroid/util/SparseIntArray;

    const/16 v1, 0xf2

    invoke-virtual {v0, v1, v6}, Landroid/util/SparseIntArray;->put(II)V

    sget-object v0, Lcom/android/providers/contacts/util/DialerKeypadNumberUtils;->LATIN_EXTENDED_KEYPAD_MAP:Landroid/util/SparseIntArray;

    const/16 v1, 0xf3

    invoke-virtual {v0, v1, v6}, Landroid/util/SparseIntArray;->put(II)V

    .line 95
    sget-object v0, Lcom/android/providers/contacts/util/DialerKeypadNumberUtils;->LATIN_EXTENDED_KEYPAD_MAP:Landroid/util/SparseIntArray;

    const/16 v1, 0xf4

    invoke-virtual {v0, v1, v6}, Landroid/util/SparseIntArray;->put(II)V

    sget-object v0, Lcom/android/providers/contacts/util/DialerKeypadNumberUtils;->LATIN_EXTENDED_KEYPAD_MAP:Landroid/util/SparseIntArray;

    const/16 v1, 0xf5

    invoke-virtual {v0, v1, v6}, Landroid/util/SparseIntArray;->put(II)V

    .line 96
    sget-object v0, Lcom/android/providers/contacts/util/DialerKeypadNumberUtils;->LATIN_EXTENDED_KEYPAD_MAP:Landroid/util/SparseIntArray;

    const/16 v1, 0xf6

    invoke-virtual {v0, v1, v6}, Landroid/util/SparseIntArray;->put(II)V

    sget-object v0, Lcom/android/providers/contacts/util/DialerKeypadNumberUtils;->LATIN_EXTENDED_KEYPAD_MAP:Landroid/util/SparseIntArray;

    const/16 v1, 0xf8

    invoke-virtual {v0, v1, v6}, Landroid/util/SparseIntArray;->put(II)V

    .line 98
    sget-object v0, Lcom/android/providers/contacts/util/DialerKeypadNumberUtils;->LATIN_EXTENDED_KEYPAD_MAP:Landroid/util/SparseIntArray;

    const/16 v1, 0x37

    const/16 v2, 0x37

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseIntArray;->put(II)V

    sget-object v0, Lcom/android/providers/contacts/util/DialerKeypadNumberUtils;->LATIN_EXTENDED_KEYPAD_MAP:Landroid/util/SparseIntArray;

    const/16 v1, 0x70

    const/16 v2, 0x37

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseIntArray;->put(II)V

    .line 99
    sget-object v0, Lcom/android/providers/contacts/util/DialerKeypadNumberUtils;->LATIN_EXTENDED_KEYPAD_MAP:Landroid/util/SparseIntArray;

    const/16 v1, 0x71

    const/16 v2, 0x37

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseIntArray;->put(II)V

    sget-object v0, Lcom/android/providers/contacts/util/DialerKeypadNumberUtils;->LATIN_EXTENDED_KEYPAD_MAP:Landroid/util/SparseIntArray;

    const/16 v1, 0x72

    const/16 v2, 0x37

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseIntArray;->put(II)V

    .line 100
    sget-object v0, Lcom/android/providers/contacts/util/DialerKeypadNumberUtils;->LATIN_EXTENDED_KEYPAD_MAP:Landroid/util/SparseIntArray;

    const/16 v1, 0x73

    const/16 v2, 0x37

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseIntArray;->put(II)V

    .line 102
    sget-object v0, Lcom/android/providers/contacts/util/DialerKeypadNumberUtils;->LATIN_EXTENDED_KEYPAD_MAP:Landroid/util/SparseIntArray;

    const/16 v1, 0x155

    const/16 v2, 0x37

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseIntArray;->put(II)V

    sget-object v0, Lcom/android/providers/contacts/util/DialerKeypadNumberUtils;->LATIN_EXTENDED_KEYPAD_MAP:Landroid/util/SparseIntArray;

    const/16 v1, 0x159

    const/16 v2, 0x37

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseIntArray;->put(II)V

    .line 104
    sget-object v0, Lcom/android/providers/contacts/util/DialerKeypadNumberUtils;->LATIN_EXTENDED_KEYPAD_MAP:Landroid/util/SparseIntArray;

    const/16 v1, 0x160

    const/16 v2, 0x37

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseIntArray;->put(II)V

    sget-object v0, Lcom/android/providers/contacts/util/DialerKeypadNumberUtils;->LATIN_EXTENDED_KEYPAD_MAP:Landroid/util/SparseIntArray;

    const/16 v1, 0x161

    const/16 v2, 0x37

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseIntArray;->put(II)V

    .line 106
    sget-object v0, Lcom/android/providers/contacts/util/DialerKeypadNumberUtils;->LATIN_EXTENDED_KEYPAD_MAP:Landroid/util/SparseIntArray;

    invoke-virtual {v0, v7, v7}, Landroid/util/SparseIntArray;->put(II)V

    sget-object v0, Lcom/android/providers/contacts/util/DialerKeypadNumberUtils;->LATIN_EXTENDED_KEYPAD_MAP:Landroid/util/SparseIntArray;

    const/16 v1, 0x74

    invoke-virtual {v0, v1, v7}, Landroid/util/SparseIntArray;->put(II)V

    .line 107
    sget-object v0, Lcom/android/providers/contacts/util/DialerKeypadNumberUtils;->LATIN_EXTENDED_KEYPAD_MAP:Landroid/util/SparseIntArray;

    const/16 v1, 0x75

    invoke-virtual {v0, v1, v7}, Landroid/util/SparseIntArray;->put(II)V

    sget-object v0, Lcom/android/providers/contacts/util/DialerKeypadNumberUtils;->LATIN_EXTENDED_KEYPAD_MAP:Landroid/util/SparseIntArray;

    const/16 v1, 0x76

    invoke-virtual {v0, v1, v7}, Landroid/util/SparseIntArray;->put(II)V

    .line 109
    sget-object v0, Lcom/android/providers/contacts/util/DialerKeypadNumberUtils;->LATIN_EXTENDED_KEYPAD_MAP:Landroid/util/SparseIntArray;

    const/16 v1, 0x163

    invoke-virtual {v0, v1, v7}, Landroid/util/SparseIntArray;->put(II)V

    sget-object v0, Lcom/android/providers/contacts/util/DialerKeypadNumberUtils;->LATIN_EXTENDED_KEYPAD_MAP:Landroid/util/SparseIntArray;

    const/16 v1, 0x165

    invoke-virtual {v0, v1, v7}, Landroid/util/SparseIntArray;->put(II)V

    .line 111
    sget-object v0, Lcom/android/providers/contacts/util/DialerKeypadNumberUtils;->LATIN_EXTENDED_KEYPAD_MAP:Landroid/util/SparseIntArray;

    const/16 v1, 0xf9

    invoke-virtual {v0, v1, v7}, Landroid/util/SparseIntArray;->put(II)V

    sget-object v0, Lcom/android/providers/contacts/util/DialerKeypadNumberUtils;->LATIN_EXTENDED_KEYPAD_MAP:Landroid/util/SparseIntArray;

    const/16 v1, 0xfa

    invoke-virtual {v0, v1, v7}, Landroid/util/SparseIntArray;->put(II)V

    .line 112
    sget-object v0, Lcom/android/providers/contacts/util/DialerKeypadNumberUtils;->LATIN_EXTENDED_KEYPAD_MAP:Landroid/util/SparseIntArray;

    const/16 v1, 0xfb

    invoke-virtual {v0, v1, v7}, Landroid/util/SparseIntArray;->put(II)V

    sget-object v0, Lcom/android/providers/contacts/util/DialerKeypadNumberUtils;->LATIN_EXTENDED_KEYPAD_MAP:Landroid/util/SparseIntArray;

    const/16 v1, 0xfc

    invoke-virtual {v0, v1, v7}, Landroid/util/SparseIntArray;->put(II)V

    .line 114
    sget-object v0, Lcom/android/providers/contacts/util/DialerKeypadNumberUtils;->LATIN_EXTENDED_KEYPAD_MAP:Landroid/util/SparseIntArray;

    const/16 v1, 0x16b

    invoke-virtual {v0, v1, v7}, Landroid/util/SparseIntArray;->put(II)V

    sget-object v0, Lcom/android/providers/contacts/util/DialerKeypadNumberUtils;->LATIN_EXTENDED_KEYPAD_MAP:Landroid/util/SparseIntArray;

    const/16 v1, 0x16f

    invoke-virtual {v0, v1, v7}, Landroid/util/SparseIntArray;->put(II)V

    .line 115
    sget-object v0, Lcom/android/providers/contacts/util/DialerKeypadNumberUtils;->LATIN_EXTENDED_KEYPAD_MAP:Landroid/util/SparseIntArray;

    const/16 v1, 0x173

    invoke-virtual {v0, v1, v7}, Landroid/util/SparseIntArray;->put(II)V

    .line 117
    sget-object v0, Lcom/android/providers/contacts/util/DialerKeypadNumberUtils;->LATIN_EXTENDED_KEYPAD_MAP:Landroid/util/SparseIntArray;

    const/16 v1, 0x39

    const/16 v2, 0x39

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseIntArray;->put(II)V

    sget-object v0, Lcom/android/providers/contacts/util/DialerKeypadNumberUtils;->LATIN_EXTENDED_KEYPAD_MAP:Landroid/util/SparseIntArray;

    const/16 v1, 0x77

    const/16 v2, 0x39

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseIntArray;->put(II)V

    .line 118
    sget-object v0, Lcom/android/providers/contacts/util/DialerKeypadNumberUtils;->LATIN_EXTENDED_KEYPAD_MAP:Landroid/util/SparseIntArray;

    const/16 v1, 0x78

    const/16 v2, 0x39

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseIntArray;->put(II)V

    sget-object v0, Lcom/android/providers/contacts/util/DialerKeypadNumberUtils;->LATIN_EXTENDED_KEYPAD_MAP:Landroid/util/SparseIntArray;

    const/16 v1, 0x79

    const/16 v2, 0x39

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseIntArray;->put(II)V

    .line 119
    sget-object v0, Lcom/android/providers/contacts/util/DialerKeypadNumberUtils;->LATIN_EXTENDED_KEYPAD_MAP:Landroid/util/SparseIntArray;

    const/16 v1, 0x7a

    const/16 v2, 0x39

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseIntArray;->put(II)V

    .line 121
    sget-object v0, Lcom/android/providers/contacts/util/DialerKeypadNumberUtils;->LATIN_EXTENDED_KEYPAD_MAP:Landroid/util/SparseIntArray;

    const/16 v1, 0xfd

    const/16 v2, 0x39

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseIntArray;->put(II)V

    sget-object v0, Lcom/android/providers/contacts/util/DialerKeypadNumberUtils;->LATIN_EXTENDED_KEYPAD_MAP:Landroid/util/SparseIntArray;

    const/16 v1, 0xff

    const/16 v2, 0x39

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseIntArray;->put(II)V

    .line 123
    sget-object v0, Lcom/android/providers/contacts/util/DialerKeypadNumberUtils;->LATIN_EXTENDED_KEYPAD_MAP:Landroid/util/SparseIntArray;

    const/16 v1, 0x17a

    const/16 v2, 0x39

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseIntArray;->put(II)V

    sget-object v0, Lcom/android/providers/contacts/util/DialerKeypadNumberUtils;->LATIN_EXTENDED_KEYPAD_MAP:Landroid/util/SparseIntArray;

    const/16 v1, 0x17c

    const/16 v2, 0x39

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseIntArray;->put(II)V

    .line 124
    sget-object v0, Lcom/android/providers/contacts/util/DialerKeypadNumberUtils;->LATIN_EXTENDED_KEYPAD_MAP:Landroid/util/SparseIntArray;

    const/16 v1, 0x17e

    const/16 v2, 0x39

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseIntArray;->put(II)V

    .line 126
    sget-object v0, Lcom/android/providers/contacts/util/DialerKeypadNumberUtils;->LATIN_EXTENDED_KEYPAD_MAP:Landroid/util/SparseIntArray;

    const/16 v1, 0x2a

    const/16 v2, 0x2a

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseIntArray;->put(II)V

    sget-object v0, Lcom/android/providers/contacts/util/DialerKeypadNumberUtils;->LATIN_EXTENDED_KEYPAD_MAP:Landroid/util/SparseIntArray;

    const/16 v1, 0x23

    const/16 v2, 0x23

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseIntArray;->put(II)V

    .line 127
    sget-object v0, Lcom/android/providers/contacts/util/DialerKeypadNumberUtils;->LATIN_EXTENDED_KEYPAD_MAP:Landroid/util/SparseIntArray;

    const/16 v1, 0x20

    const/16 v2, 0x5e

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseIntArray;->put(II)V

    .line 128
    return-void
.end method

.method private constructor <init>()V
    .locals 0

    .prologue
    .line 29
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 30
    return-void
.end method

.method private static changeActionChar(Ljava/lang/Character;)Ljava/lang/String;
    .locals 8
    .param p0, "code"    # Ljava/lang/Character;

    .prologue
    const v7, 0xac00

    .line 180
    const/4 v2, 0x0

    .line 184
    .local v2, "action_char":Ljava/lang/String;
    invoke-virtual {p0}, Ljava/lang/Character;->charValue()C

    move-result v6

    invoke-static {v6}, Ljava/lang/Character$UnicodeBlock;->of(C)Ljava/lang/Character$UnicodeBlock;

    move-result-object v5

    .line 185
    .local v5, "unicodeBlock":Ljava/lang/Character$UnicodeBlock;
    const/16 v3, 0x2a

    .line 192
    .local v3, "defaultChar":C
    invoke-static {v7}, Ljava/lang/Character;->valueOf(C)Ljava/lang/Character;

    move-result-object v6

    invoke-virtual {p0, v6}, Ljava/lang/Character;->compareTo(Ljava/lang/Character;)I

    move-result v6

    if-ltz v6, :cond_2

    const v6, 0xd7a3

    invoke-static {v6}, Ljava/lang/Character;->valueOf(C)Ljava/lang/Character;

    move-result-object v6

    invoke-virtual {p0, v6}, Ljava/lang/Character;->compareTo(Ljava/lang/Character;)I

    move-result v6

    if-gtz v6, :cond_2

    .line 193
    invoke-virtual {p0}, Ljava/lang/Character;->charValue()C

    move-result v6

    sub-int v1, v6, v7

    .line 195
    .local v1, "UniValue":I
    rem-int/lit8 v0, v1, 0x1c

    .line 196
    .local v0, "Jong":I
    sub-int v6, v1, v0

    div-int/lit8 v6, v6, 0x1c

    div-int/lit8 v4, v6, 0x15

    .line 197
    .local v4, "nChosung":I
    invoke-static {v4}, Lcom/android/providers/contacts/util/DialerKeypadNumberUtils;->convertKoreaKeypadLettersToDigits(I)Ljava/lang/String;

    move-result-object v2

    .line 214
    .end local v0    # "Jong":I
    .end local v1    # "UniValue":I
    .end local v4    # "nChosung":I
    :cond_0
    :goto_0
    if-nez v2, :cond_1

    .line 215
    invoke-static {v3}, Ljava/lang/Character;->toString(C)Ljava/lang/String;

    move-result-object v2

    .line 217
    :cond_1
    return-object v2

    .line 199
    :cond_2
    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v6

    invoke-virtual {v6}, Ljava/util/Locale;->getLanguage()Ljava/lang/String;

    move-result-object v6

    const-string v7, "iw"

    invoke-virtual {v6, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_3

    const/16 v6, 0x5d0

    invoke-static {v6}, Ljava/lang/Character;->valueOf(C)Ljava/lang/Character;

    move-result-object v6

    invoke-virtual {p0, v6}, Ljava/lang/Character;->compareTo(Ljava/lang/Character;)I

    move-result v6

    if-ltz v6, :cond_3

    const/16 v6, 0x5ea

    invoke-static {v6}, Ljava/lang/Character;->valueOf(C)Ljava/lang/Character;

    move-result-object v6

    invoke-virtual {p0, v6}, Ljava/lang/Character;->compareTo(Ljava/lang/Character;)I

    move-result v6

    if-gtz v6, :cond_3

    .line 201
    invoke-virtual {p0}, Ljava/lang/Character;->charValue()C

    move-result v6

    invoke-static {v6}, Lcom/android/providers/contacts/util/DialerKeypadNumberUtils;->convertHebrewKeypadLettersToDigits(C)Ljava/lang/String;

    move-result-object v2

    goto :goto_0

    .line 203
    :cond_3
    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v6

    invoke-virtual {v6}, Ljava/util/Locale;->getLanguage()Ljava/lang/String;

    move-result-object v6

    const-string v7, "ru"

    invoke-virtual {v6, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_4

    const/16 v6, 0x401

    invoke-static {v6}, Ljava/lang/Character;->valueOf(C)Ljava/lang/Character;

    move-result-object v6

    invoke-virtual {p0, v6}, Ljava/lang/Character;->compareTo(Ljava/lang/Character;)I

    move-result v6

    if-ltz v6, :cond_4

    const/16 v6, 0x44f

    invoke-static {v6}, Ljava/lang/Character;->valueOf(C)Ljava/lang/Character;

    move-result-object v6

    invoke-virtual {p0, v6}, Ljava/lang/Character;->compareTo(Ljava/lang/Character;)I

    move-result v6

    if-gtz v6, :cond_4

    .line 205
    invoke-virtual {p0}, Ljava/lang/Character;->charValue()C

    move-result v6

    invoke-static {v6}, Lcom/android/providers/contacts/util/DialerKeypadNumberUtils;->convertRussiaKeypadLettersToDigits(C)Ljava/lang/String;

    move-result-object v2

    goto :goto_0

    .line 208
    :cond_4
    invoke-static {v5}, Lcom/android/providers/contacts/util/DialerKeypadNumberUtils;->isLatinUnicodeBlock(Ljava/lang/Character$UnicodeBlock;)Z

    move-result v6

    if-eqz v6, :cond_0

    .line 210
    invoke-virtual {p0}, Ljava/lang/Character;->charValue()C

    move-result v6

    invoke-static {v6}, Ljava/lang/Character;->toLowerCase(C)C

    move-result v6

    invoke-static {v6, v3}, Lcom/android/providers/contacts/util/DialerKeypadNumberUtils;->convertKeypadLettersToDigits(CC)Ljava/lang/String;

    move-result-object v2

    goto :goto_0
.end method

.method private static convertHebrewKeypadLettersToDigits(C)Ljava/lang/String;
    .locals 1
    .param p0, "code"    # C

    .prologue
    .line 288
    const/4 v0, 0x0

    .line 290
    .local v0, "action_char":Ljava/lang/String;
    packed-switch p0, :pswitch_data_0

    .line 343
    :goto_0
    return-object v0

    .line 294
    :pswitch_0
    const-string v0, "2"

    .line 295
    goto :goto_0

    .line 300
    :pswitch_1
    const-string v0, "3"

    .line 301
    goto :goto_0

    .line 307
    :pswitch_2
    const-string v0, "4"

    .line 308
    goto :goto_0

    .line 314
    :pswitch_3
    const-string v0, "5"

    .line 315
    goto :goto_0

    .line 320
    :pswitch_4
    const-string v0, "6"

    .line 321
    goto :goto_0

    .line 326
    :pswitch_5
    const-string v0, "7"

    .line 327
    goto :goto_0

    .line 332
    :pswitch_6
    const-string v0, "8"

    .line 333
    goto :goto_0

    .line 339
    :pswitch_7
    const-string v0, "9"

    goto :goto_0

    .line 290
    nop

    :pswitch_data_0
    .packed-switch 0x5d0
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_4
        :pswitch_4
        :pswitch_4
        :pswitch_3
        :pswitch_3
        :pswitch_3
        :pswitch_3
        :pswitch_2
        :pswitch_2
        :pswitch_2
        :pswitch_2
        :pswitch_7
        :pswitch_7
        :pswitch_7
        :pswitch_7
        :pswitch_6
        :pswitch_6
        :pswitch_6
        :pswitch_5
        :pswitch_5
        :pswitch_5
    .end packed-switch
.end method

.method public static convertKeypadLettersToDigits(CC)Ljava/lang/String;
    .locals 4
    .param p0, "input"    # C
    .param p1, "defaultChar"    # C

    .prologue
    .line 143
    sget-object v1, Lcom/android/providers/contacts/util/DialerKeypadNumberUtils;->LATIN_EXTENDED_KEYPAD_MAP:Landroid/util/SparseIntArray;

    invoke-virtual {v1, p0, p1}, Landroid/util/SparseIntArray;->get(II)I

    move-result v1

    int-to-char v0, v1

    .line 144
    .local v0, "ret":C
    sget-boolean v1, Lcom/android/providers/contacts/util/DialerKeypadNumberUtils;->DEBUG_LOGGING:Z

    if-eqz v1, :cond_0

    .line 145
    const-string v1, "DialerKeypadNumberUtils"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "@@ converted String : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/secutil/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I

    .line 147
    :cond_0
    invoke-static {v0}, Ljava/lang/String;->valueOf(C)Ljava/lang/String;

    move-result-object v1

    return-object v1
.end method

.method private static convertKoreaKeypadLettersToDigits(I)Ljava/lang/String;
    .locals 1
    .param p0, "nChosung"    # I

    .prologue
    .line 240
    const/4 v0, 0x0

    .line 242
    .local v0, "action_char":Ljava/lang/String;
    packed-switch p0, :pswitch_data_0

    .line 284
    :goto_0
    return-object v0

    .line 247
    :pswitch_0
    const-string v0, "4"

    .line 248
    goto :goto_0

    .line 252
    :pswitch_1
    const-string v0, "5"

    .line 253
    goto :goto_0

    .line 258
    :pswitch_2
    const-string v0, "6"

    .line 259
    goto :goto_0

    .line 264
    :pswitch_3
    const-string v0, "7"

    .line 265
    goto :goto_0

    .line 270
    :pswitch_4
    const-string v0, "8"

    .line 271
    goto :goto_0

    .line 276
    :pswitch_5
    const-string v0, "9"

    .line 277
    goto :goto_0

    .line 281
    :pswitch_6
    const-string v0, "0"

    goto :goto_0

    .line 242
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_2
        :pswitch_1
        :pswitch_6
        :pswitch_3
        :pswitch_3
        :pswitch_4
        :pswitch_4
        :pswitch_6
        :pswitch_5
        :pswitch_5
        :pswitch_5
        :pswitch_0
        :pswitch_2
        :pswitch_3
        :pswitch_4
    .end packed-switch
.end method

.method private static convertRussiaKeypadLettersToDigits(C)Ljava/lang/String;
    .locals 1
    .param p0, "code"    # C

    .prologue
    .line 348
    const/4 v0, 0x0

    .line 349
    .local v0, "action_char":Ljava/lang/String;
    packed-switch p0, :pswitch_data_0

    .line 441
    :pswitch_0
    const-string v0, "*"

    .line 445
    :goto_0
    return-object v0

    .line 358
    :pswitch_1
    const-string v0, "2"

    .line 359
    goto :goto_0

    .line 371
    :pswitch_2
    const-string v0, "3"

    .line 372
    goto :goto_0

    .line 382
    :pswitch_3
    const-string v0, "4"

    .line 383
    goto :goto_0

    .line 393
    :pswitch_4
    const-string v0, "5"

    .line 394
    goto :goto_0

    .line 404
    :pswitch_5
    const-string v0, "6"

    .line 405
    goto :goto_0

    .line 415
    :pswitch_6
    const-string v0, "7"

    .line 416
    goto :goto_0

    .line 426
    :pswitch_7
    const-string v0, "8"

    .line 427
    goto :goto_0

    .line 437
    :pswitch_8
    const-string v0, "9"

    .line 438
    goto :goto_0

    .line 349
    nop

    :pswitch_data_0
    .packed-switch 0x401
        :pswitch_2
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_2
        :pswitch_2
        :pswitch_2
        :pswitch_2
        :pswitch_3
        :pswitch_3
        :pswitch_3
        :pswitch_3
        :pswitch_4
        :pswitch_4
        :pswitch_4
        :pswitch_4
        :pswitch_5
        :pswitch_5
        :pswitch_5
        :pswitch_5
        :pswitch_6
        :pswitch_6
        :pswitch_6
        :pswitch_6
        :pswitch_7
        :pswitch_7
        :pswitch_7
        :pswitch_7
        :pswitch_8
        :pswitch_8
        :pswitch_8
        :pswitch_8
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_2
        :pswitch_2
        :pswitch_2
        :pswitch_2
        :pswitch_3
        :pswitch_3
        :pswitch_3
        :pswitch_3
        :pswitch_4
        :pswitch_4
        :pswitch_4
        :pswitch_4
        :pswitch_5
        :pswitch_5
        :pswitch_5
        :pswitch_5
        :pswitch_6
        :pswitch_6
        :pswitch_6
        :pswitch_6
        :pswitch_7
        :pswitch_7
        :pswitch_7
        :pswitch_7
        :pswitch_8
        :pswitch_8
        :pswitch_8
        :pswitch_8
        :pswitch_0
        :pswitch_2
    .end packed-switch
.end method

.method public static convertTraditionalChineseKeypadLettersToDigits(C)C
    .locals 3
    .param p0, "code"    # C

    .prologue
    .line 449
    const/16 v0, 0x2a

    .line 451
    .local v0, "action_char":C
    invoke-static {p0}, Ljava/lang/Character$UnicodeBlock;->of(C)Ljava/lang/Character$UnicodeBlock;

    move-result-object v1

    invoke-static {v1}, Lcom/android/providers/contacts/util/DialerKeypadNumberUtils;->isLatinUnicodeBlock(Ljava/lang/Character$UnicodeBlock;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 452
    sget-object v1, Lcom/android/providers/contacts/util/DialerKeypadNumberUtils;->LATIN_EXTENDED_KEYPAD_MAP:Landroid/util/SparseIntArray;

    invoke-static {p0}, Ljava/lang/Character;->toLowerCase(C)C

    move-result v2

    invoke-virtual {v1, v2, v0}, Landroid/util/SparseIntArray;->get(II)I

    move-result v1

    int-to-char p0, v1

    .line 455
    :cond_0
    sparse-switch p0, :sswitch_data_0

    .line 541
    :goto_0
    return v0

    .line 462
    :sswitch_0
    const/16 v0, 0x401

    .line 463
    goto :goto_0

    .line 471
    :sswitch_1
    const/16 v0, 0x402

    .line 472
    goto :goto_0

    .line 479
    :sswitch_2
    const/16 v0, 0x403

    .line 480
    goto :goto_0

    .line 487
    :sswitch_3
    const/16 v0, 0x404

    .line 488
    goto :goto_0

    .line 496
    :sswitch_4
    const/16 v0, 0x405

    .line 497
    goto :goto_0

    .line 503
    :sswitch_5
    const/16 v0, 0x406

    .line 504
    goto :goto_0

    .line 511
    :sswitch_6
    const/16 v0, 0x407

    .line 512
    goto :goto_0

    .line 519
    :sswitch_7
    const/16 v0, 0x408

    .line 520
    goto :goto_0

    .line 528
    :sswitch_8
    const/16 v0, 0x409

    .line 529
    goto :goto_0

    .line 535
    :sswitch_9
    const/16 v0, 0x42d

    .line 536
    goto :goto_0

    .line 455
    nop

    :sswitch_data_0
    .sparse-switch
        0x30 -> :sswitch_9
        0x31 -> :sswitch_0
        0x32 -> :sswitch_1
        0x33 -> :sswitch_2
        0x34 -> :sswitch_3
        0x35 -> :sswitch_4
        0x36 -> :sswitch_5
        0x37 -> :sswitch_6
        0x38 -> :sswitch_7
        0x39 -> :sswitch_8
        0x3105 -> :sswitch_0
        0x3106 -> :sswitch_0
        0x3107 -> :sswitch_0
        0x3108 -> :sswitch_0
        0x3109 -> :sswitch_1
        0x310a -> :sswitch_1
        0x310b -> :sswitch_1
        0x310c -> :sswitch_1
        0x310d -> :sswitch_2
        0x310e -> :sswitch_2
        0x310f -> :sswitch_2
        0x3110 -> :sswitch_3
        0x3111 -> :sswitch_3
        0x3112 -> :sswitch_3
        0x3113 -> :sswitch_4
        0x3114 -> :sswitch_4
        0x3115 -> :sswitch_4
        0x3116 -> :sswitch_4
        0x3117 -> :sswitch_5
        0x3118 -> :sswitch_5
        0x3119 -> :sswitch_5
        0x311a -> :sswitch_6
        0x311b -> :sswitch_6
        0x311c -> :sswitch_6
        0x311d -> :sswitch_6
        0x311e -> :sswitch_7
        0x311f -> :sswitch_7
        0x3120 -> :sswitch_7
        0x3121 -> :sswitch_7
        0x3122 -> :sswitch_8
        0x3123 -> :sswitch_8
        0x3124 -> :sswitch_8
        0x3125 -> :sswitch_8
        0x3126 -> :sswitch_8
        0x3127 -> :sswitch_9
        0x3128 -> :sswitch_9
        0x3129 -> :sswitch_9
        0x4e00 -> :sswitch_0
        0x4e28 -> :sswitch_1
        0x4e36 -> :sswitch_3
        0x4e3f -> :sswitch_2
        0x4e5b -> :sswitch_4
    .end sparse-switch
.end method

.method public static isLatinUnicodeBlock(Ljava/lang/Character$UnicodeBlock;)Z
    .locals 1
    .param p0, "unicodeBlock"    # Ljava/lang/Character$UnicodeBlock;

    .prologue
    .line 151
    sget-object v0, Ljava/lang/Character$UnicodeBlock;->BASIC_LATIN:Ljava/lang/Character$UnicodeBlock;

    if-eq p0, v0, :cond_0

    sget-object v0, Ljava/lang/Character$UnicodeBlock;->LATIN_1_SUPPLEMENT:Ljava/lang/Character$UnicodeBlock;

    if-eq p0, v0, :cond_0

    sget-object v0, Ljava/lang/Character$UnicodeBlock;->LATIN_EXTENDED_A:Ljava/lang/Character$UnicodeBlock;

    if-eq p0, v0, :cond_0

    sget-object v0, Ljava/lang/Character$UnicodeBlock;->LATIN_EXTENDED_B:Ljava/lang/Character$UnicodeBlock;

    if-eq p0, v0, :cond_0

    sget-object v0, Ljava/lang/Character$UnicodeBlock;->LATIN_EXTENDED_ADDITIONAL:Ljava/lang/Character$UnicodeBlock;

    if-ne p0, v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static makeActionCode(Ljava/lang/String;)Ljava/lang/String;
    .locals 5
    .param p0, "Display_name"    # Ljava/lang/String;

    .prologue
    .line 161
    invoke-static {p0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 162
    const/4 v4, 0x0

    .line 175
    :goto_0
    return-object v4

    .line 164
    :cond_0
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 165
    .local v0, "action_code":Ljava/lang/StringBuilder;
    const/4 v3, 0x0

    .line 166
    .local v3, "size":I
    const/4 v2, 0x0

    .line 167
    .local v2, "idx":I
    invoke-virtual {p0}, Ljava/lang/String;->length()I

    move-result v3

    .line 170
    const/4 v2, 0x0

    :goto_1
    if-ge v2, v3, :cond_1

    .line 172
    invoke-virtual {p0, v2}, Ljava/lang/String;->charAt(I)C

    move-result v4

    invoke-static {v4}, Ljava/lang/Character;->valueOf(C)Ljava/lang/Character;

    move-result-object v1

    .line 173
    .local v1, "code":Ljava/lang/Character;
    invoke-static {v1}, Lcom/android/providers/contacts/util/DialerKeypadNumberUtils;->changeActionChar(Ljava/lang/Character;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 170
    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    .line 175
    .end local v1    # "code":Ljava/lang/Character;
    :cond_1
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    goto :goto_0
.end method

.method public static makeActionCodeHKTW(Ljava/lang/String;)Ljava/lang/String;
    .locals 5
    .param p0, "Display_name"    # Ljava/lang/String;

    .prologue
    .line 222
    invoke-static {p0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 223
    const/4 v4, 0x0

    .line 235
    :goto_0
    return-object v4

    .line 224
    :cond_0
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 225
    .local v0, "action_code":Ljava/lang/StringBuilder;
    const/4 v3, 0x0

    .line 226
    .local v3, "size":I
    const/4 v2, 0x0

    .line 227
    .local v2, "idx":I
    invoke-virtual {p0}, Ljava/lang/String;->length()I

    move-result v3

    .line 230
    const/4 v2, 0x0

    :goto_1
    if-ge v2, v3, :cond_1

    .line 231
    invoke-virtual {p0, v2}, Ljava/lang/String;->charAt(I)C

    move-result v1

    .line 232
    .local v1, "code":C
    invoke-static {v1}, Lcom/android/providers/contacts/util/DialerKeypadNumberUtils;->convertTraditionalChineseKeypadLettersToDigits(C)C

    move-result v4

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 230
    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    .line 235
    .end local v1    # "code":C
    :cond_1
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    goto :goto_0
.end method
