.class Lcom/android/providers/contacts/util/FbHighQualityImageDownload$FacebookIdQueue;
.super Ljava/lang/Object;
.source "FbHighQualityImageDownload.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/providers/contacts/util/FbHighQualityImageDownload;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "FacebookIdQueue"
.end annotation


# instance fields
.field private final indexItem:I

.field private mutex:Ljava/lang/Object;

.field private queue:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/android/providers/contacts/util/FbHighQualityImageDownload$QueueItem;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method private constructor <init>()V
    .locals 1

    .prologue
    .line 536
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 537
    const/4 v0, 0x0

    iput v0, p0, Lcom/android/providers/contacts/util/FbHighQualityImageDownload$FacebookIdQueue;->indexItem:I

    .line 539
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Lcom/android/providers/contacts/util/FbHighQualityImageDownload$FacebookIdQueue;->mutex:Ljava/lang/Object;

    .line 541
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    invoke-static {v0}, Ljava/util/Collections;->synchronizedList(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/android/providers/contacts/util/FbHighQualityImageDownload$FacebookIdQueue;->queue:Ljava/util/List;

    return-void
.end method

.method synthetic constructor <init>(Lcom/android/providers/contacts/util/FbHighQualityImageDownload$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/android/providers/contacts/util/FbHighQualityImageDownload$1;

    .prologue
    .line 536
    invoke-direct {p0}, Lcom/android/providers/contacts/util/FbHighQualityImageDownload$FacebookIdQueue;-><init>()V

    return-void
.end method

.method static synthetic access$200(Lcom/android/providers/contacts/util/FbHighQualityImageDownload$FacebookIdQueue;)Z
    .locals 1
    .param p0, "x0"    # Lcom/android/providers/contacts/util/FbHighQualityImageDownload$FacebookIdQueue;

    .prologue
    .line 536
    invoke-direct {p0}, Lcom/android/providers/contacts/util/FbHighQualityImageDownload$FacebookIdQueue;->isEmptyQueue()Z

    move-result v0

    return v0
.end method

.method static synthetic access$300(Lcom/android/providers/contacts/util/FbHighQualityImageDownload$FacebookIdQueue;)Lcom/android/providers/contacts/util/FbHighQualityImageDownload$QueueItem;
    .locals 1
    .param p0, "x0"    # Lcom/android/providers/contacts/util/FbHighQualityImageDownload$FacebookIdQueue;

    .prologue
    .line 536
    invoke-direct {p0}, Lcom/android/providers/contacts/util/FbHighQualityImageDownload$FacebookIdQueue;->getFacebookIdFromQueue()Lcom/android/providers/contacts/util/FbHighQualityImageDownload$QueueItem;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$600(Lcom/android/providers/contacts/util/FbHighQualityImageDownload$FacebookIdQueue;)I
    .locals 1
    .param p0, "x0"    # Lcom/android/providers/contacts/util/FbHighQualityImageDownload$FacebookIdQueue;

    .prologue
    .line 536
    invoke-direct {p0}, Lcom/android/providers/contacts/util/FbHighQualityImageDownload$FacebookIdQueue;->removeAll()I

    move-result v0

    return v0
.end method

.method static synthetic access$700(Lcom/android/providers/contacts/util/FbHighQualityImageDownload$FacebookIdQueue;Lcom/android/providers/contacts/util/FbHighQualityImageDownload$QueueItem;)Z
    .locals 1
    .param p0, "x0"    # Lcom/android/providers/contacts/util/FbHighQualityImageDownload$FacebookIdQueue;
    .param p1, "x1"    # Lcom/android/providers/contacts/util/FbHighQualityImageDownload$QueueItem;

    .prologue
    .line 536
    invoke-direct {p0, p1}, Lcom/android/providers/contacts/util/FbHighQualityImageDownload$FacebookIdQueue;->addFacebookIdToQueue(Lcom/android/providers/contacts/util/FbHighQualityImageDownload$QueueItem;)Z

    move-result v0

    return v0
.end method

.method private addFacebookIdToQueue(Lcom/android/providers/contacts/util/FbHighQualityImageDownload$QueueItem;)Z
    .locals 2
    .param p1, "fbId"    # Lcom/android/providers/contacts/util/FbHighQualityImageDownload$QueueItem;

    .prologue
    .line 544
    iget-object v1, p0, Lcom/android/providers/contacts/util/FbHighQualityImageDownload$FacebookIdQueue;->mutex:Ljava/lang/Object;

    monitor-enter v1

    .line 545
    :try_start_0
    iget-object v0, p0, Lcom/android/providers/contacts/util/FbHighQualityImageDownload$FacebookIdQueue;->queue:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 546
    const/4 v0, 0x0

    monitor-exit v1

    .line 549
    :goto_0
    return v0

    .line 548
    :cond_0
    iget-object v0, p0, Lcom/android/providers/contacts/util/FbHighQualityImageDownload$FacebookIdQueue;->queue:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 549
    const/4 v0, 0x1

    monitor-exit v1

    goto :goto_0

    .line 550
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method private getFacebookIdFromQueue()Lcom/android/providers/contacts/util/FbHighQualityImageDownload$QueueItem;
    .locals 5

    .prologue
    .line 554
    iget-object v2, p0, Lcom/android/providers/contacts/util/FbHighQualityImageDownload$FacebookIdQueue;->mutex:Ljava/lang/Object;

    monitor-enter v2

    .line 555
    :try_start_0
    iget-object v1, p0, Lcom/android/providers/contacts/util/FbHighQualityImageDownload$FacebookIdQueue;->queue:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    if-lez v1, :cond_0

    .line 556
    const-string v1, "FB_LOGS"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "queue size = "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p0, Lcom/android/providers/contacts/util/FbHighQualityImageDownload$FacebookIdQueue;->queue:Ljava/util/List;

    invoke-interface {v4}, Ljava/util/List;->size()I

    move-result v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v1, v3}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 558
    iget-object v1, p0, Lcom/android/providers/contacts/util/FbHighQualityImageDownload$FacebookIdQueue;->queue:Ljava/util/List;

    const/4 v3, 0x0

    invoke-interface {v1, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/providers/contacts/util/FbHighQualityImageDownload$QueueItem;

    .line 559
    .local v0, "item":Lcom/android/providers/contacts/util/FbHighQualityImageDownload$QueueItem;
    iget-object v1, p0, Lcom/android/providers/contacts/util/FbHighQualityImageDownload$FacebookIdQueue;->queue:Ljava/util/List;

    const/4 v3, 0x0

    invoke-interface {v1, v3}, Ljava/util/List;->remove(I)Ljava/lang/Object;

    .line 560
    monitor-exit v2

    .line 562
    .end local v0    # "item":Lcom/android/providers/contacts/util/FbHighQualityImageDownload$QueueItem;
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    monitor-exit v2

    goto :goto_0

    .line 563
    :catchall_0
    move-exception v1

    monitor-exit v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1
.end method

.method private isEmptyQueue()Z
    .locals 2

    .prologue
    .line 575
    iget-object v1, p0, Lcom/android/providers/contacts/util/FbHighQualityImageDownload$FacebookIdQueue;->mutex:Ljava/lang/Object;

    monitor-enter v1

    .line 576
    :try_start_0
    iget-object v0, p0, Lcom/android/providers/contacts/util/FbHighQualityImageDownload$FacebookIdQueue;->queue:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    monitor-exit v1

    return v0

    .line 577
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method private removeAll()I
    .locals 4

    .prologue
    .line 567
    iget-object v2, p0, Lcom/android/providers/contacts/util/FbHighQualityImageDownload$FacebookIdQueue;->mutex:Ljava/lang/Object;

    monitor-enter v2

    .line 568
    :try_start_0
    iget-object v1, p0, Lcom/android/providers/contacts/util/FbHighQualityImageDownload$FacebookIdQueue;->queue:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v0

    .line 569
    .local v0, "size":I
    iget-object v1, p0, Lcom/android/providers/contacts/util/FbHighQualityImageDownload$FacebookIdQueue;->queue:Ljava/util/List;

    iget-object v3, p0, Lcom/android/providers/contacts/util/FbHighQualityImageDownload$FacebookIdQueue;->queue:Ljava/util/List;

    invoke-interface {v1, v3}, Ljava/util/List;->removeAll(Ljava/util/Collection;)Z

    .line 570
    monitor-exit v2

    return v0

    .line 571
    .end local v0    # "size":I
    :catchall_0
    move-exception v1

    monitor-exit v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1
.end method


# virtual methods
.method public getCount()I
    .locals 2

    .prologue
    .line 581
    iget-object v1, p0, Lcom/android/providers/contacts/util/FbHighQualityImageDownload$FacebookIdQueue;->mutex:Ljava/lang/Object;

    monitor-enter v1

    .line 582
    :try_start_0
    iget-object v0, p0, Lcom/android/providers/contacts/util/FbHighQualityImageDownload$FacebookIdQueue;->queue:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    monitor-exit v1

    return v0

    .line 583
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method
