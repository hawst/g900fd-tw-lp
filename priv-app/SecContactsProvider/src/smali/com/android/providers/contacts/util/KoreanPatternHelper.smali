.class public Lcom/android/providers/contacts/util/KoreanPatternHelper;
.super Ljava/lang/Object;
.source "KoreanPatternHelper.java"


# static fields
.field private static KOREAN_RANGE_PATTERN:[Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    .line 4
    const/16 v0, 0x1e

    new-array v0, v0, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string v2, "[\\uAC00-\\uAE4A]"

    aput-object v2, v0, v1

    const/4 v1, 0x1

    const-string v2, "[\\uAE4C-\\uB091]"

    aput-object v2, v0, v1

    const/4 v1, 0x2

    const-string v2, ""

    aput-object v2, v0, v1

    const/4 v1, 0x3

    const-string v2, "[\\uB098-\\uB2E2]"

    aput-object v2, v0, v1

    const/4 v1, 0x4

    const-string v2, ""

    aput-object v2, v0, v1

    const/4 v1, 0x5

    const-string v2, ""

    aput-object v2, v0, v1

    const/4 v1, 0x6

    const-string v2, "[\\uB2E4-\\uB52A]"

    aput-object v2, v0, v1

    const/4 v1, 0x7

    const-string v2, "[\\uB530-\\uB775]"

    aput-object v2, v0, v1

    const/16 v1, 0x8

    const-string v2, "[\\uB77C-\\uB9C1]"

    aput-object v2, v0, v1

    const/16 v1, 0x9

    const-string v2, ""

    aput-object v2, v0, v1

    const/16 v1, 0xa

    const-string v2, ""

    aput-object v2, v0, v1

    const/16 v1, 0xb

    const-string v2, ""

    aput-object v2, v0, v1

    const/16 v1, 0xc

    const-string v2, ""

    aput-object v2, v0, v1

    const/16 v1, 0xd

    const-string v2, ""

    aput-object v2, v0, v1

    const/16 v1, 0xe

    const-string v2, ""

    aput-object v2, v0, v1

    const/16 v1, 0xf

    const-string v2, ""

    aput-object v2, v0, v1

    const/16 v1, 0x10

    const-string v2, "[\\uB9C8-\\uBC11]"

    aput-object v2, v0, v1

    const/16 v1, 0x11

    const-string v2, "[\\uBC14-\\uBE5B]"

    aput-object v2, v0, v1

    const/16 v1, 0x12

    const-string v2, "[\\uBE60-\\uC0A5]"

    aput-object v2, v0, v1

    const/16 v1, 0x13

    const-string v2, ""

    aput-object v2, v0, v1

    const/16 v1, 0x14

    const-string v2, "[\\uC0AC-\\uC2F6]"

    aput-object v2, v0, v1

    const/16 v1, 0x15

    const-string v2, "[\\uC2F8-\\uC53D]"

    aput-object v2, v0, v1

    const/16 v1, 0x16

    const-string v2, "[\\uC544-\\uC78E]"

    aput-object v2, v0, v1

    const/16 v1, 0x17

    const-string v2, "[\\uC790-\\uC9DA]"

    aput-object v2, v0, v1

    const/16 v1, 0x18

    const-string v2, "[\\uC9DC-\\uCC27]"

    aput-object v2, v0, v1

    const/16 v1, 0x19

    const-string v2, "[\\uCC28-\\uCE6D]"

    aput-object v2, v0, v1

    const/16 v1, 0x1a

    const-string v2, "[\\uCE74-\\uD0B9]"

    aput-object v2, v0, v1

    const/16 v1, 0x1b

    const-string v2, "[\\uD0C0-\\uD305]"

    aput-object v2, v0, v1

    const/16 v1, 0x1c

    const-string v2, "[\\uD30C-\\uD551]"

    aput-object v2, v0, v1

    const/16 v1, 0x1d

    const-string v2, "[\\uD558-\\uD79D]"

    aput-object v2, v0, v1

    sput-object v0, Lcom/android/providers/contacts/util/KoreanPatternHelper;->KOREAN_RANGE_PATTERN:[Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 3
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static getPattern(Ljava/lang/String;)Ljava/lang/String;
    .locals 7
    .param p0, "s"    # Ljava/lang/String;

    .prologue
    .line 48
    const/4 v1, 0x0

    .line 50
    .local v1, "position":I
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    .line 52
    .local v3, "sb":Ljava/lang/StringBuilder;
    invoke-virtual {p0}, Ljava/lang/String;->length()I

    move-result v4

    .line 53
    .local v4, "stringLength":I
    const/4 v5, 0x0

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->setLength(I)V

    .line 55
    :goto_0
    add-int/lit8 v2, v1, 0x1

    .end local v1    # "position":I
    .local v2, "position":I
    invoke-virtual {p0, v1}, Ljava/lang/String;->codePointAt(I)I

    move-result v0

    .line 59
    .local v0, "character":I
    const/16 v5, 0x1100

    if-lt v0, v5, :cond_2

    const/16 v5, 0x1112

    if-le v0, v5, :cond_0

    const/16 v5, 0x3131

    if-lt v0, v5, :cond_2

    :cond_0
    const/16 v5, 0x314e

    if-le v0, v5, :cond_1

    const v5, 0xac00

    if-lt v0, v5, :cond_2

    :cond_1
    const v5, 0xd7a3

    if-le v0, v5, :cond_4

    .line 61
    :cond_2
    const/16 v5, 0x2e

    if-ne v0, v5, :cond_3

    .line 62
    const/16 v5, 0x5c

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 63
    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->appendCodePoint(I)Ljava/lang/StringBuilder;

    .line 75
    :goto_1
    if-lt v2, v4, :cond_6

    .line 77
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    return-object v5

    .line 65
    :cond_3
    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->appendCodePoint(I)Ljava/lang/StringBuilder;

    goto :goto_1

    .line 70
    :cond_4
    invoke-static {v0}, Lcom/android/providers/contacts/util/KoreanPatternHelper;->isConsonant(I)Z

    move-result v5

    if-eqz v5, :cond_5

    .line 71
    sget-object v5, Lcom/android/providers/contacts/util/KoreanPatternHelper;->KOREAN_RANGE_PATTERN:[Ljava/lang/String;

    add-int/lit16 v6, v0, -0x3131

    aget-object v5, v5, v6

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_1

    .line 73
    :cond_5
    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->appendCodePoint(I)Ljava/lang/StringBuilder;

    goto :goto_1

    :cond_6
    move v1, v2

    .end local v2    # "position":I
    .restart local v1    # "position":I
    goto :goto_0
.end method

.method private static isConsonant(I)Z
    .locals 1
    .param p0, "character"    # I

    .prologue
    .line 40
    const/16 v0, 0x3130

    if-le p0, v0, :cond_0

    const/16 v0, 0x314e

    if-gt p0, v0, :cond_0

    .line 41
    const/4 v0, 0x1

    .line 44
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
