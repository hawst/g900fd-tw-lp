.class public Lcom/android/providers/contacts/VoicemailPermissions;
.super Ljava/lang/Object;
.source "VoicemailPermissions.java"


# instance fields
.field private final mContext:Landroid/content/Context;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 30
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 31
    iput-object p1, p0, Lcom/android/providers/contacts/VoicemailPermissions;->mContext:Landroid/content/Context;

    .line 32
    return-void
.end method

.method private callerHasPermission(Ljava/lang/String;)Z
    .locals 1
    .param p1, "permission"    # Ljava/lang/String;

    .prologue
    .line 105
    iget-object v0, p0, Lcom/android/providers/contacts/VoicemailPermissions;->mContext:Landroid/content/Context;

    invoke-virtual {v0, p1}, Landroid/content/Context;->checkCallingOrSelfPermission(Ljava/lang/String;)I

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private packageHasPermission(Ljava/lang/String;Ljava/lang/String;)Z
    .locals 1
    .param p1, "packageName"    # Ljava/lang/String;
    .param p2, "permission"    # Ljava/lang/String;

    .prologue
    .line 99
    iget-object v0, p0, Lcom/android/providers/contacts/VoicemailPermissions;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v0

    invoke-virtual {v0, p2, p1}, Landroid/content/pm/PackageManager;->checkPermission(Ljava/lang/String;Ljava/lang/String;)I

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public callerHasOwnVoicemailAccess()Z
    .locals 1

    .prologue
    .line 36
    const-string v0, "com.android.voicemail.permission.ADD_VOICEMAIL"

    invoke-direct {p0, v0}, Lcom/android/providers/contacts/VoicemailPermissions;->callerHasPermission(Ljava/lang/String;)Z

    move-result v0

    return v0
.end method

.method public callerHasReadAccess()Z
    .locals 1

    .prologue
    .line 41
    const-string v0, "com.android.voicemail.permission.READ_VOICEMAIL"

    invoke-direct {p0, v0}, Lcom/android/providers/contacts/VoicemailPermissions;->callerHasPermission(Ljava/lang/String;)Z

    move-result v0

    return v0
.end method

.method public callerHasWriteAccess()Z
    .locals 1

    .prologue
    .line 47
    const-string v0, "com.android.voicemail.permission.WRITE_VOICEMAIL"

    invoke-direct {p0, v0}, Lcom/android/providers/contacts/VoicemailPermissions;->callerHasPermission(Ljava/lang/String;)Z

    move-result v0

    return v0
.end method

.method public checkCallerHasOwnVoicemailAccess()V
    .locals 2

    .prologue
    .line 56
    invoke-virtual {p0}, Lcom/android/providers/contacts/VoicemailPermissions;->callerHasOwnVoicemailAccess()Z

    move-result v0

    if-nez v0, :cond_0

    .line 57
    new-instance v0, Ljava/lang/SecurityException;

    const-string v1, "The caller must have permission: com.android.voicemail.permission.ADD_VOICEMAIL"

    invoke-direct {v0, v1}, Ljava/lang/SecurityException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 60
    :cond_0
    return-void
.end method

.method public checkCallerHasReadAccess()V
    .locals 5

    .prologue
    .line 68
    invoke-virtual {p0}, Lcom/android/providers/contacts/VoicemailPermissions;->callerHasReadAccess()Z

    move-result v0

    if-nez v0, :cond_0

    .line 69
    new-instance v0, Ljava/lang/SecurityException;

    const-string v1, "The caller must have %s permission: "

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    const-string v4, "com.android.voicemail.permission.READ_VOICEMAIL"

    aput-object v4, v2, v3

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/SecurityException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 72
    :cond_0
    return-void
.end method

.method public checkCallerHasWriteAccess()V
    .locals 5

    .prologue
    .line 75
    invoke-virtual {p0}, Lcom/android/providers/contacts/VoicemailPermissions;->callerHasWriteAccess()Z

    move-result v0

    if-nez v0, :cond_0

    .line 76
    new-instance v0, Ljava/lang/SecurityException;

    const-string v1, "The caller must have %s permission: "

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    const-string v4, "com.android.voicemail.permission.WRITE_VOICEMAIL"

    aput-object v4, v2, v3

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/SecurityException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 79
    :cond_0
    return-void
.end method

.method public packageHasOwnVoicemailAccess(Ljava/lang/String;)Z
    .locals 1
    .param p1, "packageName"    # Ljava/lang/String;

    .prologue
    .line 83
    const-string v0, "com.android.voicemail.permission.ADD_VOICEMAIL"

    invoke-direct {p0, p1, v0}, Lcom/android/providers/contacts/VoicemailPermissions;->packageHasPermission(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v0

    return v0
.end method

.method public packageHasReadAccess(Ljava/lang/String;)Z
    .locals 1
    .param p1, "packageName"    # Ljava/lang/String;

    .prologue
    .line 89
    const-string v0, "com.android.voicemail.permission.READ_VOICEMAIL"

    invoke-direct {p0, p1, v0}, Lcom/android/providers/contacts/VoicemailPermissions;->packageHasPermission(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v0

    return v0
.end method

.method public packageHasWriteAccess(Ljava/lang/String;)Z
    .locals 1
    .param p1, "packageName"    # Ljava/lang/String;

    .prologue
    .line 94
    const-string v0, "com.android.voicemail.permission.WRITE_VOICEMAIL"

    invoke-direct {p0, p1, v0}, Lcom/android/providers/contacts/VoicemailPermissions;->packageHasPermission(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v0

    return v0
.end method
