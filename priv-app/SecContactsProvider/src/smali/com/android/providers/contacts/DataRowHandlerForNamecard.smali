.class public Lcom/android/providers/contacts/DataRowHandlerForNamecard;
.super Lcom/android/providers/contacts/DataRowHandler;
.source "DataRowHandlerForNamecard.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/android/providers/contacts/DataRowHandlerForNamecard$Namecard;
    }
.end annotation


# instance fields
.field private final mMaxDisplayPhotoDim:I

.field private final mMaxThumbnailPhotoDim:I

.field private final mPhotoStore:Lcom/android/providers/contacts/PhotoStore;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/android/providers/contacts/ContactsDatabaseHelper;Lcom/android/providers/contacts/aggregation/ContactAggregator;Lcom/android/providers/contacts/PhotoStore;II)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "dbHelper"    # Lcom/android/providers/contacts/ContactsDatabaseHelper;
    .param p3, "aggregator"    # Lcom/android/providers/contacts/aggregation/ContactAggregator;
    .param p4, "photoStore"    # Lcom/android/providers/contacts/PhotoStore;
    .param p5, "maxDisplayPhotoDim"    # I
    .param p6, "maxThumbnailPhotoDim"    # I

    .prologue
    .line 63
    const-string v0, "vnd.sec.cursor.item/name_card"

    invoke-direct {p0, p1, p2, p3, v0}, Lcom/android/providers/contacts/DataRowHandler;-><init>(Landroid/content/Context;Lcom/android/providers/contacts/ContactsDatabaseHelper;Lcom/android/providers/contacts/aggregation/ContactAggregator;Ljava/lang/String;)V

    .line 64
    iput-object p4, p0, Lcom/android/providers/contacts/DataRowHandlerForNamecard;->mPhotoStore:Lcom/android/providers/contacts/PhotoStore;

    .line 65
    iput p5, p0, Lcom/android/providers/contacts/DataRowHandlerForNamecard;->mMaxDisplayPhotoDim:I

    .line 66
    iput p6, p0, Lcom/android/providers/contacts/DataRowHandlerForNamecard;->mMaxThumbnailPhotoDim:I

    .line 67
    return-void
.end method

.method private hasNonNullPhoto(Landroid/content/ContentValues;)Z
    .locals 2
    .param p1, "values"    # Landroid/content/ContentValues;

    .prologue
    .line 134
    const-string v1, "data15"

    invoke-virtual {p1, v1}, Landroid/content/ContentValues;->getAsByteArray(Ljava/lang/String;)[B

    move-result-object v0

    .line 135
    .local v0, "photoBytes":[B
    if-eqz v0, :cond_0

    array-length v1, v0

    if-lez v1, :cond_0

    const/4 v1, 0x1

    :goto_0
    return v1

    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method private preProcessPhoto(Landroid/content/ContentValues;)Z
    .locals 2
    .param p1, "values"    # Landroid/content/ContentValues;

    .prologue
    .line 116
    const-string v1, "data15"

    invoke-virtual {p1, v1}, Landroid/content/ContentValues;->containsKey(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 117
    invoke-direct {p0, p1}, Lcom/android/providers/contacts/DataRowHandlerForNamecard;->hasNonNullPhoto(Landroid/content/ContentValues;)Z

    move-result v0

    .line 118
    .local v0, "photoExists":Z
    if-eqz v0, :cond_0

    .line 119
    invoke-direct {p0, p1}, Lcom/android/providers/contacts/DataRowHandlerForNamecard;->processPhoto(Landroid/content/ContentValues;)Z

    move-result v1

    if-nez v1, :cond_1

    .line 121
    const/4 v1, 0x0

    .line 130
    .end local v0    # "photoExists":Z
    :goto_0
    return v1

    .line 126
    .restart local v0    # "photoExists":Z
    :cond_0
    const-string v1, "data15"

    invoke-virtual {p1, v1}, Landroid/content/ContentValues;->putNull(Ljava/lang/String;)V

    .line 127
    const-string v1, "data14"

    invoke-virtual {p1, v1}, Landroid/content/ContentValues;->putNull(Ljava/lang/String;)V

    .line 130
    .end local v0    # "photoExists":Z
    :cond_1
    const/4 v1, 0x1

    goto :goto_0
.end method

.method private processPhoto(Landroid/content/ContentValues;)Z
    .locals 8
    .param p1, "values"    # Landroid/content/ContentValues;

    .prologue
    .line 147
    const-string v5, "data15"

    invoke-virtual {p1, v5}, Landroid/content/ContentValues;->getAsByteArray(Ljava/lang/String;)[B

    move-result-object v1

    .line 148
    .local v1, "originalPhoto":[B
    if-eqz v1, :cond_1

    .line 150
    :try_start_0
    new-instance v4, Lcom/android/providers/contacts/PhotoProcessor;

    iget v5, p0, Lcom/android/providers/contacts/DataRowHandlerForNamecard;->mMaxDisplayPhotoDim:I

    iget v6, p0, Lcom/android/providers/contacts/DataRowHandlerForNamecard;->mMaxThumbnailPhotoDim:I

    invoke-direct {v4, v1, v5, v6}, Lcom/android/providers/contacts/PhotoProcessor;-><init>([BII)V

    .line 152
    .local v4, "processor":Lcom/android/providers/contacts/PhotoProcessor;
    iget-object v5, p0, Lcom/android/providers/contacts/DataRowHandlerForNamecard;->mPhotoStore:Lcom/android/providers/contacts/PhotoStore;

    invoke-virtual {v5, v4}, Lcom/android/providers/contacts/PhotoStore;->insert(Lcom/android/providers/contacts/PhotoProcessor;)J

    move-result-wide v2

    .line 153
    .local v2, "photoFileId":J
    const-wide/16 v6, 0x0

    cmp-long v5, v2, v6

    if-eqz v5, :cond_0

    .line 154
    const-string v5, "data14"

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v6

    invoke-virtual {p1, v5, v6}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 158
    :goto_0
    const-string v5, "data15"

    invoke-virtual {v4}, Lcom/android/providers/contacts/PhotoProcessor;->getThumbnailPhotoBytes()[B

    move-result-object v6

    invoke-virtual {p1, v5, v6}, Landroid/content/ContentValues;->put(Ljava/lang/String;[B)V

    .line 159
    const/4 v5, 0x1

    .line 164
    .end local v2    # "photoFileId":J
    .end local v4    # "processor":Lcom/android/providers/contacts/PhotoProcessor;
    :goto_1
    return v5

    .line 156
    .restart local v2    # "photoFileId":J
    .restart local v4    # "processor":Lcom/android/providers/contacts/PhotoProcessor;
    :cond_0
    const-string v5, "data14"

    invoke-virtual {p1, v5}, Landroid/content/ContentValues;->putNull(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 160
    .end local v2    # "photoFileId":J
    .end local v4    # "processor":Lcom/android/providers/contacts/PhotoProcessor;
    :catch_0
    move-exception v0

    .line 161
    .local v0, "ioe":Ljava/io/IOException;
    const-string v5, "DataRowHandlerForNamecard"

    const-string v6, "Could not process Namecard for insert or update"

    invoke-static {v5, v6, v0}, Landroid/util/secutil/Log;->secE(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 164
    .end local v0    # "ioe":Ljava/io/IOException;
    :cond_1
    const/4 v5, 0x0

    goto :goto_1
.end method


# virtual methods
.method public delete(Landroid/database/sqlite/SQLiteDatabase;Lcom/android/providers/contacts/TransactionContext;Landroid/database/Cursor;)I
    .locals 4
    .param p1, "db"    # Landroid/database/sqlite/SQLiteDatabase;
    .param p2, "txContext"    # Lcom/android/providers/contacts/TransactionContext;
    .param p3, "c"    # Landroid/database/Cursor;

    .prologue
    .line 140
    const/4 v1, 0x2

    invoke-interface {p3, v1}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v2

    .line 141
    .local v2, "rawContactId":J
    invoke-super {p0, p1, p2, p3}, Lcom/android/providers/contacts/DataRowHandler;->delete(Landroid/database/sqlite/SQLiteDatabase;Lcom/android/providers/contacts/TransactionContext;Landroid/database/Cursor;)I

    move-result v0

    .line 142
    .local v0, "count":I
    iget-object v1, p0, Lcom/android/providers/contacts/DataRowHandlerForNamecard;->mContactAggregator:Lcom/android/providers/contacts/aggregation/ContactAggregator;

    invoke-virtual {v1, p1, v2, v3}, Lcom/android/providers/contacts/aggregation/ContactAggregator;->updateNamecardId(Landroid/database/sqlite/SQLiteDatabase;J)V

    .line 143
    return v0
.end method

.method public insert(Landroid/database/sqlite/SQLiteDatabase;Lcom/android/providers/contacts/TransactionContext;JLandroid/content/ContentValues;)J
    .locals 9
    .param p1, "db"    # Landroid/database/sqlite/SQLiteDatabase;
    .param p2, "txContext"    # Lcom/android/providers/contacts/TransactionContext;
    .param p3, "rawContactId"    # J
    .param p5, "values"    # Landroid/content/ContentValues;

    .prologue
    .line 73
    const-string v3, "skip_processing"

    invoke-virtual {p5, v3}, Landroid/content/ContentValues;->containsKey(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_2

    .line 74
    const-string v3, "skip_processing"

    invoke-virtual {p5, v3}, Landroid/content/ContentValues;->remove(Ljava/lang/String;)V

    .line 81
    :cond_0
    invoke-super/range {p0 .. p5}, Lcom/android/providers/contacts/DataRowHandler;->insert(Landroid/database/sqlite/SQLiteDatabase;Lcom/android/providers/contacts/TransactionContext;JLandroid/content/ContentValues;)J

    move-result-wide v0

    .line 82
    .local v0, "dataId":J
    invoke-virtual {p2, p3, p4}, Lcom/android/providers/contacts/TransactionContext;->isNewRawContact(J)Z

    move-result v3

    if-nez v3, :cond_1

    .line 83
    iget-object v3, p0, Lcom/android/providers/contacts/DataRowHandlerForNamecard;->mContactAggregator:Lcom/android/providers/contacts/aggregation/ContactAggregator;

    invoke-virtual {v3, p1, p3, p4}, Lcom/android/providers/contacts/aggregation/ContactAggregator;->updateNamecardId(Landroid/database/sqlite/SQLiteDatabase;J)V

    .line 86
    :cond_1
    new-instance v2, Landroid/content/ContentValues;

    invoke-direct {v2}, Landroid/content/ContentValues;-><init>()V

    .line 87
    .local v2, "v":Landroid/content/ContentValues;
    const-string v3, "is_private"

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 88
    const-string v3, "raw_contacts"

    const-string v4, "_id = ?"

    const/4 v5, 0x1

    new-array v5, v5, [Ljava/lang/String;

    const/4 v6, 0x0

    invoke-static {p3, p4}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v7

    aput-object v7, v5, v6

    invoke-virtual {p1, v3, v2, v4, v5}, Landroid/database/sqlite/SQLiteDatabase;->update(Ljava/lang/String;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    .line 90
    .end local v0    # "dataId":J
    .end local v2    # "v":Landroid/content/ContentValues;
    :goto_0
    return-wide v0

    .line 77
    :cond_2
    invoke-direct {p0, p5}, Lcom/android/providers/contacts/DataRowHandlerForNamecard;->preProcessPhoto(Landroid/content/ContentValues;)Z

    move-result v3

    if-nez v3, :cond_0

    .line 78
    const-wide/16 v0, 0x0

    goto :goto_0
.end method

.method public update(Landroid/database/sqlite/SQLiteDatabase;Lcom/android/providers/contacts/TransactionContext;Landroid/content/ContentValues;Landroid/database/Cursor;Z)Z
    .locals 5
    .param p1, "db"    # Landroid/database/sqlite/SQLiteDatabase;
    .param p2, "txContext"    # Lcom/android/providers/contacts/TransactionContext;
    .param p3, "values"    # Landroid/content/ContentValues;
    .param p4, "c"    # Landroid/database/Cursor;
    .param p5, "callerIsSyncAdapter"    # Z

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 96
    invoke-interface {p4, v3}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v0

    .line 98
    .local v0, "rawContactId":J
    const-string v4, "skip_processing"

    invoke-virtual {p3, v4}, Landroid/content/ContentValues;->containsKey(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_1

    .line 99
    const-string v4, "skip_processing"

    invoke-virtual {p3, v4}, Landroid/content/ContentValues;->remove(Ljava/lang/String;)V

    .line 108
    :cond_0
    invoke-super/range {p0 .. p5}, Lcom/android/providers/contacts/DataRowHandler;->update(Landroid/database/sqlite/SQLiteDatabase;Lcom/android/providers/contacts/TransactionContext;Landroid/content/ContentValues;Landroid/database/Cursor;Z)Z

    move-result v4

    if-nez v4, :cond_2

    .line 112
    :goto_0
    return v2

    .line 102
    :cond_1
    invoke-direct {p0, p3}, Lcom/android/providers/contacts/DataRowHandlerForNamecard;->preProcessPhoto(Landroid/content/ContentValues;)Z

    move-result v4

    if-nez v4, :cond_0

    goto :goto_0

    .line 111
    :cond_2
    iget-object v2, p0, Lcom/android/providers/contacts/DataRowHandlerForNamecard;->mContactAggregator:Lcom/android/providers/contacts/aggregation/ContactAggregator;

    invoke-virtual {v2, p1, v0, v1}, Lcom/android/providers/contacts/aggregation/ContactAggregator;->updateNamecardId(Landroid/database/sqlite/SQLiteDatabase;J)V

    move v2, v3

    .line 112
    goto :goto_0
.end method
