.class Lcom/android/providers/contacts/LocaleSet$LocaleWrapper;
.super Ljava/lang/Object;
.source "LocaleSet.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/providers/contacts/LocaleSet;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "LocaleWrapper"
.end annotation


# instance fields
.field private final mLanguage:Ljava/lang/String;

.field private final mLocale:Ljava/util/Locale;

.field private final mLocaleIsCJK:Z


# direct methods
.method public constructor <init>(Ljava/util/Locale;)V
    .locals 1
    .param p1, "locale"    # Ljava/util/Locale;

    .prologue
    .line 39
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 40
    iput-object p1, p0, Lcom/android/providers/contacts/LocaleSet$LocaleWrapper;->mLocale:Ljava/util/Locale;

    .line 41
    iget-object v0, p0, Lcom/android/providers/contacts/LocaleSet$LocaleWrapper;->mLocale:Ljava/util/Locale;

    if-eqz v0, :cond_0

    .line 42
    iget-object v0, p0, Lcom/android/providers/contacts/LocaleSet$LocaleWrapper;->mLocale:Ljava/util/Locale;

    invoke-virtual {v0}, Ljava/util/Locale;->getLanguage()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/android/providers/contacts/LocaleSet$LocaleWrapper;->mLanguage:Ljava/lang/String;

    .line 43
    iget-object v0, p0, Lcom/android/providers/contacts/LocaleSet$LocaleWrapper;->mLanguage:Ljava/lang/String;

    invoke-static {v0}, Lcom/android/providers/contacts/LocaleSet$LocaleWrapper;->isLanguageCJK(Ljava/lang/String;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/android/providers/contacts/LocaleSet$LocaleWrapper;->mLocaleIsCJK:Z

    .line 48
    :goto_0
    return-void

    .line 45
    :cond_0
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/android/providers/contacts/LocaleSet$LocaleWrapper;->mLanguage:Ljava/lang/String;

    .line 46
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/android/providers/contacts/LocaleSet$LocaleWrapper;->mLocaleIsCJK:Z

    goto :goto_0
.end method

.method private static isLanguageCJK(Ljava/lang/String;)Z
    .locals 1
    .param p0, "language"    # Ljava/lang/String;

    .prologue
    .line 34
    # getter for: Lcom/android/providers/contacts/LocaleSet;->CHINESE_LANGUAGE:Ljava/lang/String;
    invoke-static {}, Lcom/android/providers/contacts/LocaleSet;->access$000()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    # getter for: Lcom/android/providers/contacts/LocaleSet;->JAPANESE_LANGUAGE:Ljava/lang/String;
    invoke-static {}, Lcom/android/providers/contacts/LocaleSet;->access$100()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    # getter for: Lcom/android/providers/contacts/LocaleSet;->KOREAN_LANGUAGE:Ljava/lang/String;
    invoke-static {}, Lcom/android/providers/contacts/LocaleSet;->access$200()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public getLocale()Ljava/util/Locale;
    .locals 1

    .prologue
    .line 55
    iget-object v0, p0, Lcom/android/providers/contacts/LocaleSet$LocaleWrapper;->mLocale:Ljava/util/Locale;

    return-object v0
.end method

.method public hasLocale()Z
    .locals 1

    .prologue
    .line 51
    iget-object v0, p0, Lcom/android/providers/contacts/LocaleSet$LocaleWrapper;->mLocale:Ljava/util/Locale;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isLanguage(Ljava/lang/String;)Z
    .locals 1
    .param p1, "language"    # Ljava/lang/String;

    .prologue
    .line 67
    iget-object v0, p0, Lcom/android/providers/contacts/LocaleSet$LocaleWrapper;->mLanguage:Ljava/lang/String;

    if-nez v0, :cond_1

    if-nez p1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    :cond_1
    iget-object v0, p0, Lcom/android/providers/contacts/LocaleSet$LocaleWrapper;->mLanguage:Ljava/lang/String;

    invoke-virtual {v0, p1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    goto :goto_0
.end method

.method public isLocale(Ljava/util/Locale;)Z
    .locals 1
    .param p1, "locale"    # Ljava/util/Locale;

    .prologue
    .line 59
    iget-object v0, p0, Lcom/android/providers/contacts/LocaleSet$LocaleWrapper;->mLocale:Ljava/util/Locale;

    if-nez v0, :cond_1

    if-nez p1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    :cond_1
    iget-object v0, p0, Lcom/android/providers/contacts/LocaleSet$LocaleWrapper;->mLocale:Ljava/util/Locale;

    invoke-virtual {v0, p1}, Ljava/util/Locale;->equals(Ljava/lang/Object;)Z

    move-result v0

    goto :goto_0
.end method

.method public isLocaleCJK()Z
    .locals 1

    .prologue
    .line 63
    iget-boolean v0, p0, Lcom/android/providers/contacts/LocaleSet$LocaleWrapper;->mLocaleIsCJK:Z

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 1

    .prologue
    .line 72
    iget-object v0, p0, Lcom/android/providers/contacts/LocaleSet$LocaleWrapper;->mLocale:Ljava/util/Locale;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/providers/contacts/LocaleSet$LocaleWrapper;->mLocale:Ljava/util/Locale;

    invoke-virtual {v0}, Ljava/util/Locale;->toString()Ljava/lang/String;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    const-string v0, "(null)"

    goto :goto_0
.end method
