.class public Lcom/android/providers/contacts/CscContacts;
.super Ljava/lang/Object;
.source "CscContacts.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/android/providers/contacts/CscContacts$PhoneData;,
        Lcom/android/providers/contacts/CscContacts$Contact;
    }
.end annotation


# static fields
.field private static mParser:Lcom/android/providers/contacts/util/CscParser;


# instance fields
.field private context:Landroid/content/Context;

.field private numberOfContacts:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 46
    const/4 v0, 0x0

    sput-object v0, Lcom/android/providers/contacts/CscContacts;->mParser:Lcom/android/providers/contacts/util/CscParser;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 0
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 55
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 56
    iput-object p1, p0, Lcom/android/providers/contacts/CscContacts;->context:Landroid/content/Context;

    .line 57
    return-void
.end method

.method private getContacts()Ljava/util/ArrayList;
    .locals 21
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/android/providers/contacts/CscContacts$Contact;",
            ">;"
        }
    .end annotation

    .prologue
    .line 233
    new-instance v6, Ljava/util/ArrayList;

    invoke-direct {v6}, Ljava/util/ArrayList;-><init>()V

    .line 234
    .local v6, "contacts":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/android/providers/contacts/CscContacts$Contact;>;"
    sget-object v18, Lcom/android/providers/contacts/CscContacts;->mParser:Lcom/android/providers/contacts/util/CscParser;

    const-string v19, "ContactsData_Multi.Information"

    invoke-virtual/range {v18 .. v19}, Lcom/android/providers/contacts/util/CscParser;->search(Ljava/lang/String;)Lorg/w3c/dom/Node;

    move-result-object v3

    .line 235
    .local v3, "contactNode":Lorg/w3c/dom/Node;
    sget-object v18, Lcom/android/providers/contacts/CscContacts;->mParser:Lcom/android/providers/contacts/util/CscParser;

    const-string v19, "Contacts"

    move-object/from16 v0, v18

    move-object/from16 v1, v19

    invoke-virtual {v0, v3, v1}, Lcom/android/providers/contacts/util/CscParser;->searchList(Lorg/w3c/dom/Node;Ljava/lang/String;)Lorg/w3c/dom/NodeList;

    move-result-object v4

    .line 244
    .local v4, "contactNodeList":Lorg/w3c/dom/NodeList;
    if-eqz v4, :cond_3

    .line 245
    invoke-interface {v4}, Lorg/w3c/dom/NodeList;->getLength()I

    move-result v18

    move/from16 v0, v18

    move-object/from16 v1, p0

    iput v0, v1, Lcom/android/providers/contacts/CscContacts;->numberOfContacts:I

    .line 246
    const/4 v7, 0x0

    .local v7, "i":I
    :goto_0
    move-object/from16 v0, p0

    iget v0, v0, Lcom/android/providers/contacts/CscContacts;->numberOfContacts:I

    move/from16 v18, v0

    move/from16 v0, v18

    if-ge v7, v0, :cond_3

    .line 247
    invoke-interface {v4, v7}, Lorg/w3c/dom/NodeList;->item(I)Lorg/w3c/dom/Node;

    move-result-object v5

    .line 248
    .local v5, "contactNodeListChild":Lorg/w3c/dom/Node;
    new-instance v14, Ljava/util/ArrayList;

    invoke-direct {v14}, Ljava/util/ArrayList;-><init>()V

    .line 250
    .local v14, "phoneDataList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/android/providers/contacts/CscContacts$PhoneData;>;"
    sget-object v18, Lcom/android/providers/contacts/CscContacts;->mParser:Lcom/android/providers/contacts/util/CscParser;

    const-string v19, "Name"

    move-object/from16 v0, v18

    move-object/from16 v1, v19

    invoke-virtual {v0, v5, v1}, Lcom/android/providers/contacts/util/CscParser;->search(Lorg/w3c/dom/Node;Ljava/lang/String;)Lorg/w3c/dom/Node;

    move-result-object v10

    .line 251
    .local v10, "nameNode":Lorg/w3c/dom/Node;
    sget-object v18, Lcom/android/providers/contacts/CscContacts;->mParser:Lcom/android/providers/contacts/util/CscParser;

    const-string v19, "Number"

    move-object/from16 v0, v18

    move-object/from16 v1, v19

    invoke-virtual {v0, v5, v1}, Lcom/android/providers/contacts/util/CscParser;->searchList(Lorg/w3c/dom/Node;Ljava/lang/String;)Lorg/w3c/dom/NodeList;

    move-result-object v12

    .line 253
    .local v12, "numberNodeList":Lorg/w3c/dom/NodeList;
    sget-object v18, Lcom/android/providers/contacts/CscContacts;->mParser:Lcom/android/providers/contacts/util/CscParser;

    const-string v19, "Type"

    move-object/from16 v0, v18

    move-object/from16 v1, v19

    invoke-virtual {v0, v5, v1}, Lcom/android/providers/contacts/util/CscParser;->searchList(Lorg/w3c/dom/Node;Ljava/lang/String;)Lorg/w3c/dom/NodeList;

    move-result-object v17

    .line 255
    .local v17, "typeNodeList":Lorg/w3c/dom/NodeList;
    sget-object v18, Lcom/android/providers/contacts/CscContacts;->mParser:Lcom/android/providers/contacts/util/CscParser;

    move-object/from16 v0, v18

    invoke-virtual {v0, v10}, Lcom/android/providers/contacts/util/CscParser;->getValue(Lorg/w3c/dom/Node;)Ljava/lang/String;

    move-result-object v9

    .line 256
    .local v9, "name":Ljava/lang/String;
    const-string v18, "CscContacts"

    new-instance v19, Ljava/lang/StringBuilder;

    invoke-direct/range {v19 .. v19}, Ljava/lang/StringBuilder;-><init>()V

    const-string v20, "CSCContacts: getContacts() #"

    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v19

    move-object/from16 v0, v19

    invoke-virtual {v0, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v19

    invoke-virtual/range {v19 .. v19}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v19

    invoke-static/range {v18 .. v19}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 257
    const-string v18, "CscContacts"

    new-instance v19, Ljava/lang/StringBuilder;

    invoke-direct/range {v19 .. v19}, Ljava/lang/StringBuilder;-><init>()V

    const-string v20, "CSCContacts: Name: "

    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v19

    move-object/from16 v0, v19

    invoke-virtual {v0, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v19

    invoke-virtual/range {v19 .. v19}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v19

    invoke-static/range {v18 .. v19}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 258
    invoke-interface {v12}, Lorg/w3c/dom/NodeList;->getLength()I

    move-result v15

    .line 259
    .local v15, "sizeOfNumberNodeList":I
    const/4 v8, 0x0

    .local v8, "j":I
    :goto_1
    if-ge v8, v15, :cond_1

    .line 260
    const/4 v11, 0x0

    .line 261
    .local v11, "number":Ljava/lang/String;
    const/16 v16, 0x0

    .line 262
    .local v16, "type":Ljava/lang/String;
    sget-object v18, Lcom/android/providers/contacts/CscContacts;->mParser:Lcom/android/providers/contacts/util/CscParser;

    invoke-interface {v12, v8}, Lorg/w3c/dom/NodeList;->item(I)Lorg/w3c/dom/Node;

    move-result-object v19

    invoke-virtual/range {v18 .. v19}, Lcom/android/providers/contacts/util/CscParser;->getValue(Lorg/w3c/dom/Node;)Ljava/lang/String;

    move-result-object v11

    .line 263
    if-eqz v17, :cond_0

    invoke-interface/range {v17 .. v17}, Lorg/w3c/dom/NodeList;->getLength()I

    move-result v18

    move/from16 v0, v18

    if-le v0, v8, :cond_0

    .line 264
    sget-object v18, Lcom/android/providers/contacts/CscContacts;->mParser:Lcom/android/providers/contacts/util/CscParser;

    move-object/from16 v0, v17

    invoke-interface {v0, v8}, Lorg/w3c/dom/NodeList;->item(I)Lorg/w3c/dom/Node;

    move-result-object v19

    invoke-virtual/range {v18 .. v19}, Lcom/android/providers/contacts/util/CscParser;->getValue(Lorg/w3c/dom/Node;)Ljava/lang/String;

    move-result-object v16

    .line 265
    :cond_0
    new-instance v13, Lcom/android/providers/contacts/CscContacts$PhoneData;

    move-object/from16 v0, v16

    invoke-direct {v13, v11, v0}, Lcom/android/providers/contacts/CscContacts$PhoneData;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 266
    .local v13, "phoneData":Lcom/android/providers/contacts/CscContacts$PhoneData;
    invoke-virtual {v14, v13}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 267
    const-string v18, "CSCContacts"

    new-instance v19, Ljava/lang/StringBuilder;

    invoke-direct/range {v19 .. v19}, Ljava/lang/StringBuilder;-><init>()V

    const-string v20, "Number: "

    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v19

    move-object/from16 v0, v19

    invoke-virtual {v0, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v19

    const-string v20, ", Type: "

    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v19

    move-object/from16 v0, v19

    move-object/from16 v1, v16

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v19

    invoke-virtual/range {v19 .. v19}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v19

    invoke-static/range {v18 .. v19}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 259
    add-int/lit8 v8, v8, 0x1

    goto :goto_1

    .line 270
    .end local v11    # "number":Ljava/lang/String;
    .end local v13    # "phoneData":Lcom/android/providers/contacts/CscContacts$PhoneData;
    .end local v16    # "type":Ljava/lang/String;
    :cond_1
    if-eqz v9, :cond_2

    .line 271
    new-instance v2, Lcom/android/providers/contacts/CscContacts$Contact;

    move-object/from16 v0, p0

    invoke-direct {v2, v0, v9, v14}, Lcom/android/providers/contacts/CscContacts$Contact;-><init>(Lcom/android/providers/contacts/CscContacts;Ljava/lang/String;Ljava/util/ArrayList;)V

    .line 272
    .local v2, "contact":Lcom/android/providers/contacts/CscContacts$Contact;
    invoke-virtual {v6, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 246
    .end local v2    # "contact":Lcom/android/providers/contacts/CscContacts$Contact;
    :goto_2
    add-int/lit8 v7, v7, 0x1

    goto/16 :goto_0

    .line 274
    :cond_2
    const-string v18, "CscContacts"

    new-instance v19, Ljava/lang/StringBuilder;

    invoke-direct/range {v19 .. v19}, Ljava/lang/StringBuilder;-><init>()V

    const-string v20, "CSCContacts: ** getContacts ("

    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v19

    move-object/from16 v0, v19

    invoke-virtual {v0, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v19

    const-string v20, ") : contact name is null. Skip.."

    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v19

    invoke-virtual/range {v19 .. v19}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v19

    invoke-static/range {v18 .. v19}, Landroid/util/secutil/Log;->secI(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_2

    .line 279
    .end local v5    # "contactNodeListChild":Lorg/w3c/dom/Node;
    .end local v7    # "i":I
    .end local v8    # "j":I
    .end local v9    # "name":Ljava/lang/String;
    .end local v10    # "nameNode":Lorg/w3c/dom/Node;
    .end local v12    # "numberNodeList":Lorg/w3c/dom/NodeList;
    .end local v14    # "phoneDataList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/android/providers/contacts/CscContacts$PhoneData;>;"
    .end local v15    # "sizeOfNumberNodeList":I
    .end local v17    # "typeNodeList":Lorg/w3c/dom/NodeList;
    :cond_3
    return-object v6
.end method


# virtual methods
.method public setCscContacts()V
    .locals 8

    .prologue
    .line 60
    const/4 v6, 0x0

    .line 61
    .local v6, "cursor":Landroid/database/Cursor;
    iget-object v1, p0, Lcom/android/providers/contacts/CscContacts;->context:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    .line 63
    .local v0, "cr":Landroid/content/ContentResolver;
    :try_start_0
    sget-object v1, Landroid/provider/ContactsContract$RawContacts;->CONTENT_URI:Landroid/net/Uri;

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/String;

    const/4 v3, 0x0

    const-string v4, "display_name"

    aput-object v4, v2, v3

    const-string v3, "account_type=? "

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/String;

    const/4 v5, 0x0

    const-string v7, "vnd.sec.contact.phone"

    aput-object v7, v4, v5

    const/4 v5, 0x0

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v6

    .line 67
    if-eqz v6, :cond_0

    invoke-interface {v6}, Landroid/database/Cursor;->getCount()I

    move-result v1

    if-nez v1, :cond_1

    .line 68
    :cond_0
    invoke-virtual {p0}, Lcom/android/providers/contacts/CscContacts;->updateOthers()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 71
    :cond_1
    if-eqz v6, :cond_2

    .line 72
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    .line 74
    :cond_2
    return-void

    .line 71
    :catchall_0
    move-exception v1

    if-eqz v6, :cond_3

    .line 72
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    :cond_3
    throw v1
.end method

.method public updateOthers()V
    .locals 25

    .prologue
    .line 77
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/providers/contacts/CscContacts;->context:Landroid/content/Context;

    move-object/from16 v22, v0

    invoke-static/range {v22 .. v22}, Landroid/preference/PreferenceManager;->getDefaultSharedPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v17

    .line 79
    .local v17, "prefs":Landroid/content/SharedPreferences;
    invoke-static {}, Lcom/android/providers/contacts/util/CscParser;->getOthersPath()Ljava/lang/String;

    move-result-object v4

    .line 80
    .local v4, "OTHERS_CSC_FILE":Ljava/lang/String;
    const-string v5, "ContactsData.Information"

    .line 81
    .local v5, "PATH_CONTACTS_INFORMATION":Ljava/lang/String;
    new-instance v22, Lcom/android/providers/contacts/util/CscParser;

    move-object/from16 v0, v22

    invoke-direct {v0, v4}, Lcom/android/providers/contacts/util/CscParser;-><init>(Ljava/lang/String;)V

    sput-object v22, Lcom/android/providers/contacts/CscContacts;->mParser:Lcom/android/providers/contacts/util/CscParser;

    .line 83
    sget-object v22, Lcom/android/providers/contacts/CscContacts;->mParser:Lcom/android/providers/contacts/util/CscParser;

    const-string v23, "ContactsData.Information.Name"

    invoke-virtual/range {v22 .. v23}, Lcom/android/providers/contacts/util/CscParser;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 85
    .local v2, "InformationName":Ljava/lang/String;
    sget-object v22, Lcom/android/providers/contacts/CscContacts;->mParser:Lcom/android/providers/contacts/util/CscParser;

    const-string v23, "ContactsData.Information.Number"

    invoke-virtual/range {v22 .. v23}, Lcom/android/providers/contacts/util/CscParser;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 88
    .local v3, "InformationNumber":Ljava/lang/String;
    invoke-direct/range {p0 .. p0}, Lcom/android/providers/contacts/CscContacts;->getContacts()Ljava/util/ArrayList;

    move-result-object v9

    .line 89
    .local v9, "contacts":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/android/providers/contacts/CscContacts$Contact;>;"
    invoke-virtual {v9}, Ljava/util/ArrayList;->size()I

    move-result v20

    .line 93
    .local v20, "size":I
    if-lez v20, :cond_6

    .line 94
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/providers/contacts/CscContacts;->context:Landroid/content/Context;

    move-object/from16 v22, v0

    invoke-virtual/range {v22 .. v22}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v22

    sget-object v23, Landroid/provider/ContactsContract;->AUTHORITY_URI:Landroid/net/Uri;

    invoke-virtual/range {v22 .. v23}, Landroid/content/ContentResolver;->acquireContentProviderClient(Landroid/net/Uri;)Landroid/content/ContentProviderClient;

    move-result-object v8

    .line 97
    .local v8, "client":Landroid/content/ContentProviderClient;
    const/16 v19, 0x0

    .line 98
    .local v19, "results":[Landroid/content/ContentProviderResult;
    new-instance v6, Ljava/util/ArrayList;

    invoke-direct {v6}, Ljava/util/ArrayList;-><init>()V

    .line 101
    .local v6, "batch":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Landroid/content/ContentProviderOperation;>;"
    const/4 v11, 0x0

    .local v11, "i":I
    :goto_0
    move/from16 v0, v20

    if-ge v11, v0, :cond_4

    .line 107
    invoke-virtual {v6}, Ljava/util/ArrayList;->size()I

    move-result v18

    .line 109
    .local v18, "rawContactIdIndex":I
    invoke-virtual {v9, v11}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v22

    check-cast v22, Lcom/android/providers/contacts/CscContacts$Contact;

    invoke-virtual/range {v22 .. v22}, Lcom/android/providers/contacts/CscContacts$Contact;->getName()Ljava/lang/String;

    move-result-object v13

    .line 110
    .local v13, "name":Ljava/lang/String;
    sget-object v22, Landroid/provider/ContactsContract$RawContacts;->CONTENT_URI:Landroid/net/Uri;

    invoke-static/range {v22 .. v22}, Landroid/content/ContentProviderOperation;->newInsert(Landroid/net/Uri;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v7

    .line 112
    .local v7, "builder":Landroid/content/ContentProviderOperation$Builder;
    const-string v22, "account_name"

    const-string v23, "vnd.sec.contact.phone"

    move-object/from16 v0, v22

    move-object/from16 v1, v23

    invoke-virtual {v7, v0, v1}, Landroid/content/ContentProviderOperation$Builder;->withValue(Ljava/lang/String;Ljava/lang/Object;)Landroid/content/ContentProviderOperation$Builder;

    .line 114
    const-string v22, "account_type"

    const-string v23, "vnd.sec.contact.phone"

    move-object/from16 v0, v22

    move-object/from16 v1, v23

    invoke-virtual {v7, v0, v1}, Landroid/content/ContentProviderOperation$Builder;->withValue(Ljava/lang/String;Ljava/lang/Object;)Landroid/content/ContentProviderOperation$Builder;

    .line 118
    invoke-virtual {v7}, Landroid/content/ContentProviderOperation$Builder;->build()Landroid/content/ContentProviderOperation;

    move-result-object v22

    move-object/from16 v0, v22

    invoke-virtual {v6, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 119
    const-string v22, "CscContacts"

    new-instance v23, Ljava/lang/StringBuilder;

    invoke-direct/range {v23 .. v23}, Ljava/lang/StringBuilder;-><init>()V

    const-string v24, "CSCContacts: UpdateOthers() name: "

    invoke-virtual/range {v23 .. v24}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v23

    move-object/from16 v0, v23

    invoke-virtual {v0, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v23

    invoke-virtual/range {v23 .. v23}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v23

    invoke-static/range {v22 .. v23}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 120
    invoke-virtual {v13}, Ljava/lang/String;->length()I

    move-result v22

    if-lez v22, :cond_0

    .line 121
    sget-object v22, Landroid/provider/ContactsContract$Data;->CONTENT_URI:Landroid/net/Uri;

    invoke-static/range {v22 .. v22}, Landroid/content/ContentProviderOperation;->newInsert(Landroid/net/Uri;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v7

    .line 123
    const-string v22, "raw_contact_id"

    move-object/from16 v0, v22

    move/from16 v1, v18

    invoke-virtual {v7, v0, v1}, Landroid/content/ContentProviderOperation$Builder;->withValueBackReference(Ljava/lang/String;I)Landroid/content/ContentProviderOperation$Builder;

    .line 125
    const-string v22, "mimetype"

    const-string v23, "vnd.android.cursor.item/name"

    move-object/from16 v0, v22

    move-object/from16 v1, v23

    invoke-virtual {v7, v0, v1}, Landroid/content/ContentProviderOperation$Builder;->withValue(Ljava/lang/String;Ljava/lang/Object;)Landroid/content/ContentProviderOperation$Builder;

    .line 127
    const-string v22, "data1"

    move-object/from16 v0, v22

    invoke-virtual {v7, v0, v13}, Landroid/content/ContentProviderOperation$Builder;->withValue(Ljava/lang/String;Ljava/lang/Object;)Landroid/content/ContentProviderOperation$Builder;

    .line 128
    invoke-virtual {v7}, Landroid/content/ContentProviderOperation$Builder;->build()Landroid/content/ContentProviderOperation;

    move-result-object v22

    move-object/from16 v0, v22

    invoke-virtual {v6, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 133
    :cond_0
    new-instance v16, Ljava/util/ArrayList;

    invoke-direct/range {v16 .. v16}, Ljava/util/ArrayList;-><init>()V

    .line 134
    .local v16, "phoneDataList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/android/providers/contacts/CscContacts$PhoneData;>;"
    invoke-virtual {v9, v11}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v22

    check-cast v22, Lcom/android/providers/contacts/CscContacts$Contact;

    invoke-virtual/range {v22 .. v22}, Lcom/android/providers/contacts/CscContacts$Contact;->getPhoneData()Ljava/util/ArrayList;

    move-result-object v16

    .line 135
    invoke-virtual/range {v16 .. v16}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v12

    .local v12, "i$":Ljava/util/Iterator;
    :cond_1
    :goto_1
    invoke-interface {v12}, Ljava/util/Iterator;->hasNext()Z

    move-result v22

    if-eqz v22, :cond_3

    invoke-interface {v12}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v15

    check-cast v15, Lcom/android/providers/contacts/CscContacts$PhoneData;

    .line 136
    .local v15, "phoneData":Lcom/android/providers/contacts/CscContacts$PhoneData;
    invoke-virtual {v15}, Lcom/android/providers/contacts/CscContacts$PhoneData;->getNumber()Ljava/lang/String;

    move-result-object v14

    .line 137
    .local v14, "number":Ljava/lang/String;
    invoke-virtual {v15}, Lcom/android/providers/contacts/CscContacts$PhoneData;->getType()Ljava/lang/String;

    move-result-object v21

    .line 138
    .local v21, "type":Ljava/lang/String;
    const-string v22, "CscContacts"

    new-instance v23, Ljava/lang/StringBuilder;

    invoke-direct/range {v23 .. v23}, Ljava/lang/StringBuilder;-><init>()V

    const-string v24, "CSCContacts: number: "

    invoke-virtual/range {v23 .. v24}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v23

    move-object/from16 v0, v23

    invoke-virtual {v0, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v23

    const-string v24, ", type: "

    invoke-virtual/range {v23 .. v24}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v23

    move-object/from16 v0, v23

    move-object/from16 v1, v21

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v23

    invoke-virtual/range {v23 .. v23}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v23

    invoke-static/range {v22 .. v23}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 140
    invoke-virtual {v14}, Ljava/lang/String;->length()I

    move-result v22

    if-lez v22, :cond_1

    .line 141
    sget-object v22, Landroid/provider/ContactsContract$Data;->CONTENT_URI:Landroid/net/Uri;

    invoke-static/range {v22 .. v22}, Landroid/content/ContentProviderOperation;->newInsert(Landroid/net/Uri;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v7

    .line 143
    const-string v22, "raw_contact_id"

    move-object/from16 v0, v22

    move/from16 v1, v18

    invoke-virtual {v7, v0, v1}, Landroid/content/ContentProviderOperation$Builder;->withValueBackReference(Ljava/lang/String;I)Landroid/content/ContentProviderOperation$Builder;

    .line 145
    const-string v22, "mimetype"

    const-string v23, "vnd.android.cursor.item/phone_v2"

    move-object/from16 v0, v22

    move-object/from16 v1, v23

    invoke-virtual {v7, v0, v1}, Landroid/content/ContentProviderOperation$Builder;->withValue(Ljava/lang/String;Ljava/lang/Object;)Landroid/content/ContentProviderOperation$Builder;

    .line 147
    invoke-static/range {v21 .. v21}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v22

    if-nez v22, :cond_2

    .line 148
    const-string v22, "data2"

    const/16 v23, 0x0

    invoke-static/range {v23 .. v23}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v23

    move-object/from16 v0, v22

    move-object/from16 v1, v23

    invoke-virtual {v7, v0, v1}, Landroid/content/ContentProviderOperation$Builder;->withValue(Ljava/lang/String;Ljava/lang/Object;)Landroid/content/ContentProviderOperation$Builder;

    .line 149
    const-string v22, "data3"

    move-object/from16 v0, v22

    move-object/from16 v1, v21

    invoke-virtual {v7, v0, v1}, Landroid/content/ContentProviderOperation$Builder;->withValue(Ljava/lang/String;Ljava/lang/Object;)Landroid/content/ContentProviderOperation$Builder;

    .line 153
    :goto_2
    const-string v22, "data1"

    move-object/from16 v0, v22

    invoke-virtual {v7, v0, v14}, Landroid/content/ContentProviderOperation$Builder;->withValue(Ljava/lang/String;Ljava/lang/Object;)Landroid/content/ContentProviderOperation$Builder;

    .line 154
    const-string v22, "is_primary"

    const/16 v23, 0x1

    invoke-static/range {v23 .. v23}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v23

    move-object/from16 v0, v22

    move-object/from16 v1, v23

    invoke-virtual {v7, v0, v1}, Landroid/content/ContentProviderOperation$Builder;->withValue(Ljava/lang/String;Ljava/lang/Object;)Landroid/content/ContentProviderOperation$Builder;

    .line 155
    invoke-virtual {v7}, Landroid/content/ContentProviderOperation$Builder;->build()Landroid/content/ContentProviderOperation;

    move-result-object v22

    move-object/from16 v0, v22

    invoke-virtual {v6, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto/16 :goto_1

    .line 151
    :cond_2
    const-string v22, "data2"

    const/16 v23, 0x2

    invoke-static/range {v23 .. v23}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v23

    move-object/from16 v0, v22

    move-object/from16 v1, v23

    invoke-virtual {v7, v0, v1}, Landroid/content/ContentProviderOperation$Builder;->withValue(Ljava/lang/String;Ljava/lang/Object;)Landroid/content/ContentProviderOperation$Builder;

    goto :goto_2

    .line 101
    .end local v14    # "number":Ljava/lang/String;
    .end local v15    # "phoneData":Lcom/android/providers/contacts/CscContacts$PhoneData;
    .end local v21    # "type":Ljava/lang/String;
    :cond_3
    add-int/lit8 v11, v11, 0x1

    goto/16 :goto_0

    .line 160
    .end local v7    # "builder":Landroid/content/ContentProviderOperation$Builder;
    .end local v12    # "i$":Ljava/util/Iterator;
    .end local v13    # "name":Ljava/lang/String;
    .end local v16    # "phoneDataList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/android/providers/contacts/CscContacts$PhoneData;>;"
    .end local v18    # "rawContactIdIndex":I
    :cond_4
    :try_start_0
    invoke-virtual {v8, v6}, Landroid/content/ContentProviderClient;->applyBatch(Ljava/util/ArrayList;)[Landroid/content/ContentProviderResult;

    .line 161
    if-eqz v17, :cond_5

    .line 162
    invoke-interface/range {v17 .. v17}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v22

    const-string v23, "setCscContactsAlready"

    const/16 v24, 0x1

    invoke-interface/range {v22 .. v24}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    move-result-object v22

    invoke-interface/range {v22 .. v22}, Landroid/content/SharedPreferences$Editor;->apply()V

    .line 164
    :cond_5
    invoke-virtual {v8}, Landroid/content/ContentProviderClient;->release()Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_3
    .catch Landroid/content/OperationApplicationException; {:try_start_0 .. :try_end_0} :catch_0

    .line 229
    .end local v6    # "batch":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Landroid/content/ContentProviderOperation;>;"
    .end local v8    # "client":Landroid/content/ContentProviderClient;
    .end local v11    # "i":I
    .end local v19    # "results":[Landroid/content/ContentProviderResult;
    :goto_3
    return-void

    .line 167
    .restart local v6    # "batch":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Landroid/content/ContentProviderOperation;>;"
    .restart local v8    # "client":Landroid/content/ContentProviderClient;
    .restart local v11    # "i":I
    .restart local v19    # "results":[Landroid/content/ContentProviderResult;
    :catch_0
    move-exception v10

    .line 168
    .local v10, "e":Landroid/content/OperationApplicationException;
    invoke-virtual {v10}, Landroid/content/OperationApplicationException;->printStackTrace()V

    goto :goto_3

    .line 170
    .end local v6    # "batch":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Landroid/content/ContentProviderOperation;>;"
    .end local v8    # "client":Landroid/content/ContentProviderClient;
    .end local v10    # "e":Landroid/content/OperationApplicationException;
    .end local v11    # "i":I
    .end local v19    # "results":[Landroid/content/ContentProviderResult;
    :cond_6
    if-eqz v2, :cond_a

    .line 175
    const/16 v19, 0x0

    .line 176
    .restart local v19    # "results":[Landroid/content/ContentProviderResult;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/providers/contacts/CscContacts;->context:Landroid/content/Context;

    move-object/from16 v22, v0

    invoke-virtual/range {v22 .. v22}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v22

    sget-object v23, Landroid/provider/ContactsContract;->AUTHORITY_URI:Landroid/net/Uri;

    invoke-virtual/range {v22 .. v23}, Landroid/content/ContentResolver;->acquireContentProviderClient(Landroid/net/Uri;)Landroid/content/ContentProviderClient;

    move-result-object v8

    .line 179
    .restart local v8    # "client":Landroid/content/ContentProviderClient;
    new-instance v6, Ljava/util/ArrayList;

    invoke-direct {v6}, Ljava/util/ArrayList;-><init>()V

    .line 181
    .restart local v6    # "batch":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Landroid/content/ContentProviderOperation;>;"
    invoke-virtual {v6}, Ljava/util/ArrayList;->size()I

    move-result v18

    .line 183
    .restart local v18    # "rawContactIdIndex":I
    sget-object v22, Landroid/provider/ContactsContract$RawContacts;->CONTENT_URI:Landroid/net/Uri;

    invoke-static/range {v22 .. v22}, Landroid/content/ContentProviderOperation;->newInsert(Landroid/net/Uri;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v7

    .line 185
    .restart local v7    # "builder":Landroid/content/ContentProviderOperation$Builder;
    const-string v22, "account_name"

    const-string v23, "vnd.sec.contact.phone"

    move-object/from16 v0, v22

    move-object/from16 v1, v23

    invoke-virtual {v7, v0, v1}, Landroid/content/ContentProviderOperation$Builder;->withValue(Ljava/lang/String;Ljava/lang/Object;)Landroid/content/ContentProviderOperation$Builder;

    .line 187
    const-string v22, "account_type"

    const-string v23, "vnd.sec.contact.phone"

    move-object/from16 v0, v22

    move-object/from16 v1, v23

    invoke-virtual {v7, v0, v1}, Landroid/content/ContentProviderOperation$Builder;->withValue(Ljava/lang/String;Ljava/lang/Object;)Landroid/content/ContentProviderOperation$Builder;

    .line 191
    invoke-virtual {v7}, Landroid/content/ContentProviderOperation$Builder;->build()Landroid/content/ContentProviderOperation;

    move-result-object v22

    move-object/from16 v0, v22

    invoke-virtual {v6, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 193
    if-eqz v2, :cond_7

    .line 194
    sget-object v22, Landroid/provider/ContactsContract$Data;->CONTENT_URI:Landroid/net/Uri;

    invoke-static/range {v22 .. v22}, Landroid/content/ContentProviderOperation;->newInsert(Landroid/net/Uri;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v7

    .line 196
    const-string v22, "raw_contact_id"

    move-object/from16 v0, v22

    move/from16 v1, v18

    invoke-virtual {v7, v0, v1}, Landroid/content/ContentProviderOperation$Builder;->withValueBackReference(Ljava/lang/String;I)Landroid/content/ContentProviderOperation$Builder;

    .line 198
    const-string v22, "mimetype"

    const-string v23, "vnd.android.cursor.item/name"

    move-object/from16 v0, v22

    move-object/from16 v1, v23

    invoke-virtual {v7, v0, v1}, Landroid/content/ContentProviderOperation$Builder;->withValue(Ljava/lang/String;Ljava/lang/Object;)Landroid/content/ContentProviderOperation$Builder;

    .line 200
    const-string v22, "data1"

    move-object/from16 v0, v22

    invoke-virtual {v7, v0, v2}, Landroid/content/ContentProviderOperation$Builder;->withValue(Ljava/lang/String;Ljava/lang/Object;)Landroid/content/ContentProviderOperation$Builder;

    .line 201
    invoke-virtual {v7}, Landroid/content/ContentProviderOperation$Builder;->build()Landroid/content/ContentProviderOperation;

    move-result-object v22

    move-object/from16 v0, v22

    invoke-virtual {v6, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 203
    :cond_7
    if-eqz v3, :cond_8

    .line 204
    sget-object v22, Landroid/provider/ContactsContract$Data;->CONTENT_URI:Landroid/net/Uri;

    invoke-static/range {v22 .. v22}, Landroid/content/ContentProviderOperation;->newInsert(Landroid/net/Uri;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v7

    .line 206
    const-string v22, "raw_contact_id"

    move-object/from16 v0, v22

    move/from16 v1, v18

    invoke-virtual {v7, v0, v1}, Landroid/content/ContentProviderOperation$Builder;->withValueBackReference(Ljava/lang/String;I)Landroid/content/ContentProviderOperation$Builder;

    .line 208
    const-string v22, "mimetype"

    const-string v23, "vnd.android.cursor.item/phone_v2"

    move-object/from16 v0, v22

    move-object/from16 v1, v23

    invoke-virtual {v7, v0, v1}, Landroid/content/ContentProviderOperation$Builder;->withValue(Ljava/lang/String;Ljava/lang/Object;)Landroid/content/ContentProviderOperation$Builder;

    .line 209
    const-string v22, "data2"

    const/16 v23, 0x2

    invoke-static/range {v23 .. v23}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v23

    move-object/from16 v0, v22

    move-object/from16 v1, v23

    invoke-virtual {v7, v0, v1}, Landroid/content/ContentProviderOperation$Builder;->withValue(Ljava/lang/String;Ljava/lang/Object;)Landroid/content/ContentProviderOperation$Builder;

    .line 210
    const-string v22, "data1"

    move-object/from16 v0, v22

    invoke-virtual {v7, v0, v3}, Landroid/content/ContentProviderOperation$Builder;->withValue(Ljava/lang/String;Ljava/lang/Object;)Landroid/content/ContentProviderOperation$Builder;

    .line 211
    const-string v22, "is_primary"

    const/16 v23, 0x1

    invoke-static/range {v23 .. v23}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v23

    move-object/from16 v0, v22

    move-object/from16 v1, v23

    invoke-virtual {v7, v0, v1}, Landroid/content/ContentProviderOperation$Builder;->withValue(Ljava/lang/String;Ljava/lang/Object;)Landroid/content/ContentProviderOperation$Builder;

    .line 212
    invoke-virtual {v7}, Landroid/content/ContentProviderOperation$Builder;->build()Landroid/content/ContentProviderOperation;

    move-result-object v22

    move-object/from16 v0, v22

    invoke-virtual {v6, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 215
    :cond_8
    :try_start_1
    invoke-virtual {v8, v6}, Landroid/content/ContentProviderClient;->applyBatch(Ljava/util/ArrayList;)[Landroid/content/ContentProviderResult;

    move-result-object v19

    .line 216
    if-eqz v17, :cond_9

    .line 217
    invoke-interface/range {v17 .. v17}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v22

    const-string v23, "setCscContactsAlready"

    const/16 v24, 0x1

    invoke-interface/range {v22 .. v24}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    move-result-object v22

    invoke-interface/range {v22 .. v22}, Landroid/content/SharedPreferences$Editor;->apply()V
    :try_end_1
    .catch Landroid/os/RemoteException; {:try_start_1 .. :try_end_1} :catch_1
    .catch Landroid/content/OperationApplicationException; {:try_start_1 .. :try_end_1} :catch_2

    .line 224
    :cond_9
    :goto_4
    const-string v22, "CscContacts"

    new-instance v23, Ljava/lang/StringBuilder;

    invoke-direct/range {v23 .. v23}, Ljava/lang/StringBuilder;-><init>()V

    const-string v24, "CSCContacts: ** Information Contact "

    invoke-virtual/range {v23 .. v24}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v23

    move-object/from16 v0, v23

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v23

    const-string v24, " : "

    invoke-virtual/range {v23 .. v24}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v23

    move-object/from16 v0, v23

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v23

    invoke-virtual/range {v23 .. v23}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v23

    invoke-static/range {v22 .. v23}, Landroid/util/secutil/Log;->secI(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_3

    .line 219
    :catch_1
    move-exception v10

    .line 220
    .local v10, "e":Landroid/os/RemoteException;
    invoke-virtual {v10}, Landroid/os/RemoteException;->printStackTrace()V

    goto :goto_4

    .line 221
    .end local v10    # "e":Landroid/os/RemoteException;
    :catch_2
    move-exception v10

    .line 222
    .local v10, "e":Landroid/content/OperationApplicationException;
    invoke-virtual {v10}, Landroid/content/OperationApplicationException;->printStackTrace()V

    goto :goto_4

    .line 227
    .end local v6    # "batch":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Landroid/content/ContentProviderOperation;>;"
    .end local v7    # "builder":Landroid/content/ContentProviderOperation$Builder;
    .end local v8    # "client":Landroid/content/ContentProviderClient;
    .end local v10    # "e":Landroid/content/OperationApplicationException;
    .end local v18    # "rawContactIdIndex":I
    .end local v19    # "results":[Landroid/content/ContentProviderResult;
    :cond_a
    const-string v22, "CscContacts"

    const-string v23, "CSCContacts: ** Information Contact is not existed "

    invoke-static/range {v22 .. v23}, Landroid/util/secutil/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_3

    .line 165
    .restart local v6    # "batch":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Landroid/content/ContentProviderOperation;>;"
    .restart local v8    # "client":Landroid/content/ContentProviderClient;
    .restart local v11    # "i":I
    .restart local v19    # "results":[Landroid/content/ContentProviderResult;
    :catch_3
    move-exception v22

    goto/16 :goto_3
.end method
