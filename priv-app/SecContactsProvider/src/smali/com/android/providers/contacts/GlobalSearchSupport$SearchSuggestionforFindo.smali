.class Lcom/android/providers/contacts/GlobalSearchSupport$SearchSuggestionforFindo;
.super Ljava/lang/Object;
.source "GlobalSearchSupport.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/providers/contacts/GlobalSearchSupport;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "SearchSuggestionforFindo"
.end annotation


# instance fields
.field DEFAULT_IMAGES:[Ljava/lang/String;

.field address:J

.field colorMap:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field contactId:J

.field contactUri:Landroid/net/Uri;

.field context:Landroid/content/Context;

.field email:J

.field event:J

.field icon1:Ljava/lang/String;

.field icon2:Ljava/lang/String;

.field im:J

.field intentData:Ljava/lang/String;

.field lookupKey:Ljava/lang/String;

.field mimeTypeId:J

.field name:J

.field nickname:J

.field note:J

.field numOfBgColor:I

.field final numOfDefaultImgBeforeK:I

.field organization:J

.field pakageNameOfContacts:Ljava/lang/String;

.field phone:J

.field photoId:Ljava/lang/String;

.field res:Landroid/content/res/Resources;

.field resIdOfDefaultImg:I

.field text1:Ljava/lang/String;

.field text2:Ljava/lang/String;

.field website:J


# direct methods
.method public constructor <init>(Lcom/android/providers/contacts/ContactsProvider2;)V
    .locals 7
    .param p1, "contactsProvider"    # Lcom/android/providers/contacts/ContactsProvider2;

    .prologue
    const/4 v6, 0x3

    const/4 v5, 0x0

    .line 262
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 247
    const/4 v2, 0x5

    new-array v2, v2, [Ljava/lang/String;

    const-string v3, "contacts_default_image_small_05"

    aput-object v3, v2, v5

    const/4 v3, 0x1

    const-string v4, "contacts_default_image_small_01"

    aput-object v4, v2, v3

    const/4 v3, 0x2

    const-string v4, "contacts_default_image_small_02"

    aput-object v4, v2, v3

    const-string v3, "contacts_default_image_small_03"

    aput-object v3, v2, v6

    const/4 v3, 0x4

    const-string v4, "contacts_default_image_small_04"

    aput-object v4, v2, v3

    iput-object v2, p0, Lcom/android/providers/contacts/GlobalSearchSupport$SearchSuggestionforFindo;->DEFAULT_IMAGES:[Ljava/lang/String;

    .line 255
    iget-object v2, p0, Lcom/android/providers/contacts/GlobalSearchSupport$SearchSuggestionforFindo;->DEFAULT_IMAGES:[Ljava/lang/String;

    array-length v2, v2

    iput v2, p0, Lcom/android/providers/contacts/GlobalSearchSupport$SearchSuggestionforFindo;->numOfDefaultImgBeforeK:I

    .line 256
    iput v6, p0, Lcom/android/providers/contacts/GlobalSearchSupport$SearchSuggestionforFindo;->numOfBgColor:I

    .line 257
    iput v5, p0, Lcom/android/providers/contacts/GlobalSearchSupport$SearchSuggestionforFindo;->resIdOfDefaultImg:I

    .line 260
    const/4 v2, 0x0

    iput-object v2, p0, Lcom/android/providers/contacts/GlobalSearchSupport$SearchSuggestionforFindo;->res:Landroid/content/res/Resources;

    .line 263
    invoke-virtual {p1}, Lcom/android/providers/contacts/ContactsProvider2;->getContext()Landroid/content/Context;

    move-result-object v2

    iput-object v2, p0, Lcom/android/providers/contacts/GlobalSearchSupport$SearchSuggestionforFindo;->context:Landroid/content/Context;

    .line 264
    iget-object v2, p0, Lcom/android/providers/contacts/GlobalSearchSupport$SearchSuggestionforFindo;->context:Landroid/content/Context;

    invoke-virtual {p1, v2}, Lcom/android/providers/contacts/ContactsProvider2;->getDatabaseHelper(Landroid/content/Context;)Lcom/android/providers/contacts/ContactsDatabaseHelper;

    move-result-object v0

    .line 266
    .local v0, "dbHelper":Lcom/android/providers/contacts/ContactsDatabaseHelper;
    invoke-virtual {v0}, Lcom/android/providers/contacts/ContactsDatabaseHelper;->getMimeTypeIdForStructuredName()J

    move-result-wide v2

    iput-wide v2, p0, Lcom/android/providers/contacts/GlobalSearchSupport$SearchSuggestionforFindo;->name:J

    .line 267
    invoke-virtual {v0}, Lcom/android/providers/contacts/ContactsDatabaseHelper;->getMimeTypeIdForPhone()J

    move-result-wide v2

    iput-wide v2, p0, Lcom/android/providers/contacts/GlobalSearchSupport$SearchSuggestionforFindo;->phone:J

    .line 268
    invoke-virtual {v0}, Lcom/android/providers/contacts/ContactsDatabaseHelper;->getMimeTypeIdForEmail()J

    move-result-wide v2

    iput-wide v2, p0, Lcom/android/providers/contacts/GlobalSearchSupport$SearchSuggestionforFindo;->email:J

    .line 269
    invoke-virtual {v0}, Lcom/android/providers/contacts/ContactsDatabaseHelper;->getMimeTypeIdForWebSite()J

    move-result-wide v2

    iput-wide v2, p0, Lcom/android/providers/contacts/GlobalSearchSupport$SearchSuggestionforFindo;->website:J

    .line 270
    invoke-virtual {v0}, Lcom/android/providers/contacts/ContactsDatabaseHelper;->getMimeTypeIdForStructuredPostal()J

    move-result-wide v2

    iput-wide v2, p0, Lcom/android/providers/contacts/GlobalSearchSupport$SearchSuggestionforFindo;->address:J

    .line 271
    invoke-virtual {v0}, Lcom/android/providers/contacts/ContactsDatabaseHelper;->getMimeTypeIdForIm()J

    move-result-wide v2

    iput-wide v2, p0, Lcom/android/providers/contacts/GlobalSearchSupport$SearchSuggestionforFindo;->im:J

    .line 272
    invoke-virtual {v0}, Lcom/android/providers/contacts/ContactsDatabaseHelper;->getMimeTypeIdForNote()J

    move-result-wide v2

    iput-wide v2, p0, Lcom/android/providers/contacts/GlobalSearchSupport$SearchSuggestionforFindo;->note:J

    .line 273
    invoke-virtual {v0}, Lcom/android/providers/contacts/ContactsDatabaseHelper;->getMimeTypeIdForNickname()J

    move-result-wide v2

    iput-wide v2, p0, Lcom/android/providers/contacts/GlobalSearchSupport$SearchSuggestionforFindo;->nickname:J

    .line 274
    invoke-virtual {v0}, Lcom/android/providers/contacts/ContactsDatabaseHelper;->getMimeTypeIdForOrganization()J

    move-result-wide v2

    iput-wide v2, p0, Lcom/android/providers/contacts/GlobalSearchSupport$SearchSuggestionforFindo;->organization:J

    .line 275
    invoke-virtual {v0}, Lcom/android/providers/contacts/ContactsDatabaseHelper;->getMimeTypeIdForEvent()J

    move-result-wide v2

    iput-wide v2, p0, Lcom/android/providers/contacts/GlobalSearchSupport$SearchSuggestionforFindo;->event:J

    .line 277
    invoke-static {}, Lcom/sec/android/app/CscFeature;->getInstance()Lcom/sec/android/app/CscFeature;

    move-result-object v2

    const-string v3, "CscFeature_Contact_ReplacePackageAs"

    invoke-virtual {v2, v3}, Lcom/sec/android/app/CscFeature;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    const-string v3, ""

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    const-string v2, "com.android.contacts"

    :goto_0
    iput-object v2, p0, Lcom/android/providers/contacts/GlobalSearchSupport$SearchSuggestionforFindo;->pakageNameOfContacts:Ljava/lang/String;

    .line 285
    :try_start_0
    iget-object v2, p0, Lcom/android/providers/contacts/GlobalSearchSupport$SearchSuggestionforFindo;->context:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v2

    iget-object v3, p0, Lcom/android/providers/contacts/GlobalSearchSupport$SearchSuggestionforFindo;->pakageNameOfContacts:Ljava/lang/String;

    invoke-virtual {v2, v3}, Landroid/content/pm/PackageManager;->getResourcesForApplication(Ljava/lang/String;)Landroid/content/res/Resources;

    move-result-object v2

    iput-object v2, p0, Lcom/android/providers/contacts/GlobalSearchSupport$SearchSuggestionforFindo;->res:Landroid/content/res/Resources;

    .line 286
    invoke-direct {p0}, Lcom/android/providers/contacts/GlobalSearchSupport$SearchSuggestionforFindo;->setResIdOfDefaultImage()V

    .line 287
    iget v2, p0, Lcom/android/providers/contacts/GlobalSearchSupport$SearchSuggestionforFindo;->resIdOfDefaultImg:I

    if-eqz v2, :cond_0

    .line 288
    new-instance v2, Ljava/util/HashMap;

    invoke-direct {v2}, Ljava/util/HashMap;-><init>()V

    iput-object v2, p0, Lcom/android/providers/contacts/GlobalSearchSupport$SearchSuggestionforFindo;->colorMap:Ljava/util/HashMap;

    .line 289
    invoke-direct {p0}, Lcom/android/providers/contacts/GlobalSearchSupport$SearchSuggestionforFindo;->setDefaultPhotoBackgroundColor()V
    :try_end_0
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    .line 296
    :cond_0
    :goto_1
    return-void

    .line 277
    :cond_1
    invoke-static {}, Lcom/sec/android/app/CscFeature;->getInstance()Lcom/sec/android/app/CscFeature;

    move-result-object v2

    const-string v3, "CscFeature_Contact_ReplacePackageAs"

    invoke-virtual {v2, v3}, Lcom/sec/android/app/CscFeature;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    goto :goto_0

    .line 291
    :catch_0
    move-exception v1

    .line 294
    .local v1, "e":Landroid/content/pm/PackageManager$NameNotFoundException;
    invoke-virtual {v1}, Landroid/content/pm/PackageManager$NameNotFoundException;->printStackTrace()V

    goto :goto_1
.end method

.method private addColumnValue(Ljava/util/ArrayList;Ljava/lang/String;)V
    .locals 4
    .param p2, "column"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/Object;",
            ">;",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    .line 382
    .local p1, "list":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Object;>;"
    const-string v0, "suggest_text_1"

    invoke-virtual {v0, p2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 383
    iget-object v0, p0, Lcom/android/providers/contacts/GlobalSearchSupport$SearchSuggestionforFindo;->text1:Ljava/lang/String;

    invoke-virtual {p1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 407
    :goto_0
    return-void

    .line 384
    :cond_0
    const-string v0, "suggest_text_2"

    invoke-virtual {v0, p2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 385
    iget-object v0, p0, Lcom/android/providers/contacts/GlobalSearchSupport$SearchSuggestionforFindo;->text2:Ljava/lang/String;

    invoke-virtual {p1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 386
    :cond_1
    const-string v0, "suggest_icon_1"

    invoke-virtual {v0, p2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 387
    iget-object v0, p0, Lcom/android/providers/contacts/GlobalSearchSupport$SearchSuggestionforFindo;->icon1:Ljava/lang/String;

    invoke-virtual {p1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 388
    :cond_2
    const-string v0, "suggest_icon_2"

    invoke-virtual {v0, p2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 389
    iget-object v0, p0, Lcom/android/providers/contacts/GlobalSearchSupport$SearchSuggestionforFindo;->icon2:Ljava/lang/String;

    invoke-virtual {p1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 390
    :cond_3
    const-string v0, "suggest_intent_data"

    invoke-virtual {v0, p2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 391
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "content://com.android.contacts/contacts/lookup/"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/android/providers/contacts/GlobalSearchSupport$SearchSuggestionforFindo;->lookupKey:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "/"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-wide v2, p0, Lcom/android/providers/contacts/GlobalSearchSupport$SearchSuggestionforFindo;->contactId:J

    invoke-virtual {v0, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/android/providers/contacts/GlobalSearchSupport$SearchSuggestionforFindo;->intentData:Ljava/lang/String;

    .line 392
    iget-object v0, p0, Lcom/android/providers/contacts/GlobalSearchSupport$SearchSuggestionforFindo;->intentData:Ljava/lang/String;

    invoke-virtual {p1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 393
    :cond_4
    const-string v0, "suggest_shortcut_id"

    invoke-virtual {v0, p2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_5

    .line 394
    iget-object v0, p0, Lcom/android/providers/contacts/GlobalSearchSupport$SearchSuggestionforFindo;->lookupKey:Ljava/lang/String;

    invoke-virtual {p1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 395
    :cond_5
    const-string v0, "suggest_uri"

    invoke-virtual {v0, p2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_6

    .line 397
    sget-object v0, Landroid/provider/ContactsContract$Contacts;->CONTENT_VCARD_URI:Landroid/net/Uri;

    iget-object v1, p0, Lcom/android/providers/contacts/GlobalSearchSupport$SearchSuggestionforFindo;->lookupKey:Ljava/lang/String;

    invoke-static {v0, v1}, Landroid/net/Uri;->withAppendedPath(Landroid/net/Uri;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    iput-object v0, p0, Lcom/android/providers/contacts/GlobalSearchSupport$SearchSuggestionforFindo;->contactUri:Landroid/net/Uri;

    .line 398
    iget-object v0, p0, Lcom/android/providers/contacts/GlobalSearchSupport$SearchSuggestionforFindo;->contactUri:Landroid/net/Uri;

    invoke-virtual {p1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto/16 :goto_0

    .line 399
    :cond_6
    const-string v0, "suggest_target_type"

    invoke-virtual {v0, p2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_7

    .line 400
    iget-wide v0, p0, Lcom/android/providers/contacts/GlobalSearchSupport$SearchSuggestionforFindo;->mimeTypeId:J

    invoke-direct {p0, v0, v1}, Lcom/android/providers/contacts/GlobalSearchSupport$SearchSuggestionforFindo;->getTargetType(J)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto/16 :goto_0

    .line 401
    :cond_7
    const-string v0, "suggest_mime_type"

    invoke-virtual {v0, p2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_8

    .line 402
    const-string v0, "text/x-vcard"

    invoke-virtual {p1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto/16 :goto_0

    .line 405
    :cond_8
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Invalid column name: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method private getTargetType(J)Ljava/lang/String;
    .locals 5
    .param p1, "mimeTypeId"    # J

    .prologue
    .line 410
    const-string v0, "0"

    .line 411
    .local v0, "res":Ljava/lang/String;
    iget-wide v2, p0, Lcom/android/providers/contacts/GlobalSearchSupport$SearchSuggestionforFindo;->phone:J

    cmp-long v1, v2, p1

    if-nez v1, :cond_1

    .line 412
    const-string v0, "1"

    .line 430
    :cond_0
    :goto_0
    return-object v0

    .line 413
    :cond_1
    iget-wide v2, p0, Lcom/android/providers/contacts/GlobalSearchSupport$SearchSuggestionforFindo;->email:J

    cmp-long v1, v2, p1

    if-nez v1, :cond_2

    .line 414
    const-string v0, "2"

    goto :goto_0

    .line 415
    :cond_2
    iget-wide v2, p0, Lcom/android/providers/contacts/GlobalSearchSupport$SearchSuggestionforFindo;->website:J

    cmp-long v1, v2, p1

    if-nez v1, :cond_3

    .line 416
    const-string v0, "3"

    goto :goto_0

    .line 417
    :cond_3
    iget-wide v2, p0, Lcom/android/providers/contacts/GlobalSearchSupport$SearchSuggestionforFindo;->address:J

    cmp-long v1, v2, p1

    if-nez v1, :cond_4

    .line 418
    const-string v0, "4"

    goto :goto_0

    .line 419
    :cond_4
    iget-wide v2, p0, Lcom/android/providers/contacts/GlobalSearchSupport$SearchSuggestionforFindo;->event:J

    cmp-long v1, v2, p1

    if-nez v1, :cond_5

    .line 420
    const-string v0, "5"

    goto :goto_0

    .line 421
    :cond_5
    iget-wide v2, p0, Lcom/android/providers/contacts/GlobalSearchSupport$SearchSuggestionforFindo;->organization:J

    cmp-long v1, v2, p1

    if-nez v1, :cond_6

    .line 422
    const-string v0, "6"

    goto :goto_0

    .line 423
    :cond_6
    iget-wide v2, p0, Lcom/android/providers/contacts/GlobalSearchSupport$SearchSuggestionforFindo;->im:J

    cmp-long v1, v2, p1

    if-nez v1, :cond_7

    .line 424
    const-string v0, "7"

    goto :goto_0

    .line 425
    :cond_7
    iget-wide v2, p0, Lcom/android/providers/contacts/GlobalSearchSupport$SearchSuggestionforFindo;->note:J

    cmp-long v1, v2, p1

    if-nez v1, :cond_8

    .line 426
    const-string v0, "8"

    goto :goto_0

    .line 427
    :cond_8
    iget-wide v2, p0, Lcom/android/providers/contacts/GlobalSearchSupport$SearchSuggestionforFindo;->nickname:J

    cmp-long v1, v2, p1

    if-nez v1, :cond_0

    .line 428
    const-string v0, "9"

    goto :goto_0
.end method

.method private setDefaultPhotoBackgroundColor()V
    .locals 9

    .prologue
    .line 299
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    iget v2, p0, Lcom/android/providers/contacts/GlobalSearchSupport$SearchSuggestionforFindo;->numOfBgColor:I

    if-ge v1, v2, :cond_0

    .line 301
    :try_start_0
    iget-object v2, p0, Lcom/android/providers/contacts/GlobalSearchSupport$SearchSuggestionforFindo;->colorMap:Ljava/util/HashMap;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "color"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    add-int/lit8 v4, v1, 0x1

    invoke-static {v4}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    iget-object v4, p0, Lcom/android/providers/contacts/GlobalSearchSupport$SearchSuggestionforFindo;->res:Landroid/content/res/Resources;

    iget-object v5, p0, Lcom/android/providers/contacts/GlobalSearchSupport$SearchSuggestionforFindo;->res:Landroid/content/res/Resources;

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "default_caller_id_bg_color_"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    add-int/lit8 v7, v1, 0x1

    invoke-static {v7}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    const-string v7, "color"

    iget-object v8, p0, Lcom/android/providers/contacts/GlobalSearchSupport$SearchSuggestionforFindo;->pakageNameOfContacts:Ljava/lang/String;

    invoke-virtual {v5, v6, v7, v8}, Landroid/content/res/Resources;->getIdentifier(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)I

    move-result v5

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getColor(I)I

    move-result v4

    invoke-static {v4}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 299
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 304
    :catch_0
    move-exception v0

    .line 308
    :cond_0
    return-void
.end method

.method private setResIdOfDefaultImage()V
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 311
    iget-object v0, p0, Lcom/android/providers/contacts/GlobalSearchSupport$SearchSuggestionforFindo;->res:Landroid/content/res/Resources;

    iget-object v1, p0, Lcom/android/providers/contacts/GlobalSearchSupport$SearchSuggestionforFindo;->DEFAULT_IMAGES:[Ljava/lang/String;

    aget-object v1, v1, v4

    const-string v2, "drawable"

    iget-object v3, p0, Lcom/android/providers/contacts/GlobalSearchSupport$SearchSuggestionforFindo;->pakageNameOfContacts:Ljava/lang/String;

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/res/Resources;->getIdentifier(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lcom/android/providers/contacts/GlobalSearchSupport$SearchSuggestionforFindo;->resIdOfDefaultImg:I

    .line 313
    iget v0, p0, Lcom/android/providers/contacts/GlobalSearchSupport$SearchSuggestionforFindo;->resIdOfDefaultImg:I

    if-nez v0, :cond_0

    .line 314
    iget-object v0, p0, Lcom/android/providers/contacts/GlobalSearchSupport$SearchSuggestionforFindo;->res:Landroid/content/res/Resources;

    const-string v1, "contacts_default_caller_id_list"

    const-string v2, "drawable"

    iget-object v3, p0, Lcom/android/providers/contacts/GlobalSearchSupport$SearchSuggestionforFindo;->pakageNameOfContacts:Ljava/lang/String;

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/res/Resources;->getIdentifier(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lcom/android/providers/contacts/GlobalSearchSupport$SearchSuggestionforFindo;->resIdOfDefaultImg:I

    .line 319
    :goto_0
    return-void

    .line 318
    :cond_0
    iput v4, p0, Lcom/android/providers/contacts/GlobalSearchSupport$SearchSuggestionforFindo;->resIdOfDefaultImg:I

    goto :goto_0
.end method


# virtual methods
.method public asList([Ljava/lang/String;)Ljava/util/ArrayList;
    .locals 8
    .param p1, "projection"    # [Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "([",
            "Ljava/lang/String;",
            ")",
            "Ljava/util/ArrayList",
            "<*>;"
        }
    .end annotation

    .prologue
    .line 324
    iget-object v4, p0, Lcom/android/providers/contacts/GlobalSearchSupport$SearchSuggestionforFindo;->icon1:Ljava/lang/String;

    if-nez v4, :cond_0

    .line 325
    iget-object v4, p0, Lcom/android/providers/contacts/GlobalSearchSupport$SearchSuggestionforFindo;->photoId:Ljava/lang/String;

    if-eqz v4, :cond_2

    .line 326
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "content://com.android.contacts/contacts/"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-wide v6, p0, Lcom/android/providers/contacts/GlobalSearchSupport$SearchSuggestionforFindo;->contactId:J

    invoke-virtual {v4, v6, v7}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "/photo"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    iput-object v4, p0, Lcom/android/providers/contacts/GlobalSearchSupport$SearchSuggestionforFindo;->icon1:Ljava/lang/String;

    .line 347
    :cond_0
    :goto_0
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    .line 348
    .local v2, "list":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Object;>;"
    if-nez p1, :cond_6

    .line 349
    iget-object v4, p0, Lcom/android/providers/contacts/GlobalSearchSupport$SearchSuggestionforFindo;->text1:Ljava/lang/String;

    invoke-virtual {v2, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 350
    iget-object v4, p0, Lcom/android/providers/contacts/GlobalSearchSupport$SearchSuggestionforFindo;->text2:Ljava/lang/String;

    invoke-virtual {v2, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 351
    iget-object v4, p0, Lcom/android/providers/contacts/GlobalSearchSupport$SearchSuggestionforFindo;->icon1:Ljava/lang/String;

    invoke-virtual {v2, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 352
    iget-object v4, p0, Lcom/android/providers/contacts/GlobalSearchSupport$SearchSuggestionforFindo;->icon2:Ljava/lang/String;

    invoke-virtual {v2, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 353
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "content://com.android.contacts/contacts/lookup/"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-object v5, p0, Lcom/android/providers/contacts/GlobalSearchSupport$SearchSuggestionforFindo;->lookupKey:Ljava/lang/String;

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "/"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-wide v6, p0, Lcom/android/providers/contacts/GlobalSearchSupport$SearchSuggestionforFindo;->contactId:J

    invoke-virtual {v4, v6, v7}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    iput-object v4, p0, Lcom/android/providers/contacts/GlobalSearchSupport$SearchSuggestionforFindo;->intentData:Ljava/lang/String;

    .line 354
    iget-object v4, p0, Lcom/android/providers/contacts/GlobalSearchSupport$SearchSuggestionforFindo;->intentData:Ljava/lang/String;

    invoke-virtual {v2, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 355
    iget-object v4, p0, Lcom/android/providers/contacts/GlobalSearchSupport$SearchSuggestionforFindo;->lookupKey:Ljava/lang/String;

    invoke-virtual {v2, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 356
    iget-object v4, p0, Lcom/android/providers/contacts/GlobalSearchSupport$SearchSuggestionforFindo;->contactUri:Landroid/net/Uri;

    invoke-virtual {v2, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 357
    iget-wide v4, p0, Lcom/android/providers/contacts/GlobalSearchSupport$SearchSuggestionforFindo;->mimeTypeId:J

    invoke-direct {p0, v4, v5}, Lcom/android/providers/contacts/GlobalSearchSupport$SearchSuggestionforFindo;->getTargetType(J)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 358
    const-string v4, "text/x-vcard"

    invoke-virtual {v2, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 364
    :cond_1
    return-object v2

    .line 328
    .end local v2    # "list":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Object;>;"
    :cond_2
    iget-wide v4, p0, Lcom/android/providers/contacts/GlobalSearchSupport$SearchSuggestionforFindo;->contactId:J

    const-wide/16 v6, 0x0

    cmp-long v4, v4, v6

    if-ltz v4, :cond_4

    iget-wide v4, p0, Lcom/android/providers/contacts/GlobalSearchSupport$SearchSuggestionforFindo;->contactId:J

    iget v6, p0, Lcom/android/providers/contacts/GlobalSearchSupport$SearchSuggestionforFindo;->numOfDefaultImgBeforeK:I

    int-to-long v6, v6

    rem-long/2addr v4, v6

    long-to-int v1, v4

    .line 329
    .local v1, "iconNum":I
    :goto_1
    const-string v4, "0"

    iput-object v4, p0, Lcom/android/providers/contacts/GlobalSearchSupport$SearchSuggestionforFindo;->icon2:Ljava/lang/String;

    .line 330
    const/4 v3, 0x0

    .line 331
    .local v3, "resourceId":I
    iget v4, p0, Lcom/android/providers/contacts/GlobalSearchSupport$SearchSuggestionforFindo;->resIdOfDefaultImg:I

    if-nez v4, :cond_5

    .line 332
    iget-object v4, p0, Lcom/android/providers/contacts/GlobalSearchSupport$SearchSuggestionforFindo;->res:Landroid/content/res/Resources;

    if-eqz v4, :cond_3

    .line 333
    iget-object v4, p0, Lcom/android/providers/contacts/GlobalSearchSupport$SearchSuggestionforFindo;->res:Landroid/content/res/Resources;

    iget-object v5, p0, Lcom/android/providers/contacts/GlobalSearchSupport$SearchSuggestionforFindo;->DEFAULT_IMAGES:[Ljava/lang/String;

    aget-object v5, v5, v1

    const-string v6, "drawable"

    iget-object v7, p0, Lcom/android/providers/contacts/GlobalSearchSupport$SearchSuggestionforFindo;->pakageNameOfContacts:Ljava/lang/String;

    invoke-virtual {v4, v5, v6, v7}, Landroid/content/res/Resources;->getIdentifier(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)I

    move-result v3

    .line 341
    :cond_3
    :goto_2
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "android.resource://"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-object v5, p0, Lcom/android/providers/contacts/GlobalSearchSupport$SearchSuggestionforFindo;->pakageNameOfContacts:Ljava/lang/String;

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "/"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-static {v3}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v4

    invoke-virtual {v4}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v4

    iput-object v4, p0, Lcom/android/providers/contacts/GlobalSearchSupport$SearchSuggestionforFindo;->icon1:Ljava/lang/String;

    goto/16 :goto_0

    .line 328
    .end local v1    # "iconNum":I
    .end local v3    # "resourceId":I
    :cond_4
    const/4 v1, 0x0

    goto :goto_1

    .line 337
    .restart local v1    # "iconNum":I
    .restart local v3    # "resourceId":I
    :cond_5
    iget v3, p0, Lcom/android/providers/contacts/GlobalSearchSupport$SearchSuggestionforFindo;->resIdOfDefaultImg:I

    .line 338
    iget-object v4, p0, Lcom/android/providers/contacts/GlobalSearchSupport$SearchSuggestionforFindo;->colorMap:Ljava/util/HashMap;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "color"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget-wide v6, p0, Lcom/android/providers/contacts/GlobalSearchSupport$SearchSuggestionforFindo;->contactId:J

    invoke-virtual {p0, v6, v7}, Lcom/android/providers/contacts/GlobalSearchSupport$SearchSuggestionforFindo;->getDefaultPhotoBackgroundColor(J)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/String;

    iput-object v4, p0, Lcom/android/providers/contacts/GlobalSearchSupport$SearchSuggestionforFindo;->icon2:Ljava/lang/String;

    goto :goto_2

    .line 360
    .end local v1    # "iconNum":I
    .end local v3    # "resourceId":I
    .restart local v2    # "list":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Object;>;"
    :cond_6
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_3
    array-length v4, p1

    if-ge v0, v4, :cond_1

    .line 361
    aget-object v4, p1, v0

    invoke-direct {p0, v2, v4}, Lcom/android/providers/contacts/GlobalSearchSupport$SearchSuggestionforFindo;->addColumnValue(Ljava/util/ArrayList;Ljava/lang/String;)V

    .line 360
    add-int/lit8 v0, v0, 0x1

    goto :goto_3
.end method

.method public getDefaultPhotoBackgroundColor(J)Ljava/lang/String;
    .locals 3
    .param p1, "contactId"    # J

    .prologue
    .line 367
    const-wide/16 v0, 0x1

    cmp-long v0, p1, v0

    if-gez v0, :cond_0

    .line 368
    const-string v0, "1"

    .line 378
    :goto_0
    return-object v0

    .line 370
    :cond_0
    const-wide/16 v0, 0x4

    rem-long v0, p1, v0

    long-to-int v0, v0

    packed-switch v0, :pswitch_data_0

    .line 378
    const-string v0, "1"

    goto :goto_0

    .line 372
    :pswitch_0
    const-string v0, "1"

    goto :goto_0

    .line 374
    :pswitch_1
    const-string v0, "2"

    goto :goto_0

    .line 376
    :pswitch_2
    const-string v0, "3"

    goto :goto_0

    .line 370
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method public reset()V
    .locals 4

    .prologue
    const-wide/16 v2, 0x0

    const/4 v0, 0x0

    .line 434
    iput-wide v2, p0, Lcom/android/providers/contacts/GlobalSearchSupport$SearchSuggestionforFindo;->contactId:J

    .line 435
    iput-object v0, p0, Lcom/android/providers/contacts/GlobalSearchSupport$SearchSuggestionforFindo;->photoId:Ljava/lang/String;

    .line 436
    iput-object v0, p0, Lcom/android/providers/contacts/GlobalSearchSupport$SearchSuggestionforFindo;->lookupKey:Ljava/lang/String;

    .line 437
    iput-object v0, p0, Lcom/android/providers/contacts/GlobalSearchSupport$SearchSuggestionforFindo;->text1:Ljava/lang/String;

    .line 438
    iput-object v0, p0, Lcom/android/providers/contacts/GlobalSearchSupport$SearchSuggestionforFindo;->text2:Ljava/lang/String;

    .line 439
    iput-object v0, p0, Lcom/android/providers/contacts/GlobalSearchSupport$SearchSuggestionforFindo;->icon1:Ljava/lang/String;

    .line 440
    iput-object v0, p0, Lcom/android/providers/contacts/GlobalSearchSupport$SearchSuggestionforFindo;->icon2:Ljava/lang/String;

    .line 441
    iput-object v0, p0, Lcom/android/providers/contacts/GlobalSearchSupport$SearchSuggestionforFindo;->intentData:Ljava/lang/String;

    .line 442
    iput-wide v2, p0, Lcom/android/providers/contacts/GlobalSearchSupport$SearchSuggestionforFindo;->mimeTypeId:J

    .line 443
    iput-object v0, p0, Lcom/android/providers/contacts/GlobalSearchSupport$SearchSuggestionforFindo;->contactUri:Landroid/net/Uri;

    .line 444
    return-void
.end method
