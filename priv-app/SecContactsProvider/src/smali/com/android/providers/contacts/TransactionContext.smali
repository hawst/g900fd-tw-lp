.class public Lcom/android/providers/contacts/TransactionContext;
.super Ljava/lang/Object;
.source "TransactionContext.java"


# instance fields
.field private mChangedRawContacts:Ljava/util/HashSet;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashSet",
            "<",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation
.end field

.field private mDeletedDataEvents:Ljava/util/HashSet;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashSet",
            "<",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation
.end field

.field private mDeletedRawContacts:Ljava/util/HashSet;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashSet",
            "<",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation
.end field

.field private mDirtyRawContacts:Ljava/util/HashSet;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashSet",
            "<",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation
.end field

.field private final mForProfile:Z

.field private mInsertedDataEvents:Ljava/util/HashSet;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashSet",
            "<",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation
.end field

.field private mInsertedRawContactsAccounts:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/Long;",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation
.end field

.field private mLastModifiedGroups:Ljava/util/HashSet;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashSet",
            "<",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation
.end field

.field private mLastModifiedRawContacts:Ljava/util/HashSet;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashSet",
            "<",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation
.end field

.field private mStaleSearchIndexContacts:Ljava/util/HashSet;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashSet",
            "<",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation
.end field

.field private mStaleSearchIndexRawContacts:Ljava/util/HashSet;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashSet",
            "<",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation
.end field

.field private mUpdatedDataEvents:Ljava/util/HashSet;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashSet",
            "<",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation
.end field

.field private mUpdatedRawContacts:Ljava/util/HashSet;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashSet",
            "<",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation
.end field

.field private mUpdatedSyncStates:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/Long;",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Z)V
    .locals 0
    .param p1, "forProfile"    # Z

    .prologue
    .line 55
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 56
    iput-boolean p1, p0, Lcom/android/providers/contacts/TransactionContext;->mForProfile:Z

    .line 57
    return-void
.end method


# virtual methods
.method public clearAll()V
    .locals 0

    .prologue
    .line 256
    invoke-virtual {p0}, Lcom/android/providers/contacts/TransactionContext;->clearExceptSearchIndexUpdates()V

    .line 257
    invoke-virtual {p0}, Lcom/android/providers/contacts/TransactionContext;->clearEventsUpdates()V

    .line 258
    invoke-virtual {p0}, Lcom/android/providers/contacts/TransactionContext;->clearSearchIndexUpdates()V

    .line 259
    return-void
.end method

.method public clearEventsUpdates()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 245
    iput-object v0, p0, Lcom/android/providers/contacts/TransactionContext;->mDeletedDataEvents:Ljava/util/HashSet;

    .line 246
    iput-object v0, p0, Lcom/android/providers/contacts/TransactionContext;->mInsertedDataEvents:Ljava/util/HashSet;

    .line 247
    iput-object v0, p0, Lcom/android/providers/contacts/TransactionContext;->mUpdatedDataEvents:Ljava/util/HashSet;

    .line 248
    return-void
.end method

.method public clearExceptSearchIndexUpdates()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 232
    iput-object v0, p0, Lcom/android/providers/contacts/TransactionContext;->mInsertedRawContactsAccounts:Ljava/util/HashMap;

    .line 233
    iput-object v0, p0, Lcom/android/providers/contacts/TransactionContext;->mUpdatedRawContacts:Ljava/util/HashSet;

    .line 234
    iput-object v0, p0, Lcom/android/providers/contacts/TransactionContext;->mUpdatedSyncStates:Ljava/util/HashMap;

    .line 235
    iput-object v0, p0, Lcom/android/providers/contacts/TransactionContext;->mDirtyRawContacts:Ljava/util/HashSet;

    .line 236
    iput-object v0, p0, Lcom/android/providers/contacts/TransactionContext;->mChangedRawContacts:Ljava/util/HashSet;

    .line 238
    iput-object v0, p0, Lcom/android/providers/contacts/TransactionContext;->mDeletedRawContacts:Ljava/util/HashSet;

    .line 239
    iput-object v0, p0, Lcom/android/providers/contacts/TransactionContext;->mLastModifiedRawContacts:Ljava/util/HashSet;

    .line 241
    iput-object v0, p0, Lcom/android/providers/contacts/TransactionContext;->mLastModifiedGroups:Ljava/util/HashSet;

    .line 242
    return-void
.end method

.method public clearSearchIndexUpdates()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 251
    iput-object v0, p0, Lcom/android/providers/contacts/TransactionContext;->mStaleSearchIndexRawContacts:Ljava/util/HashSet;

    .line 252
    iput-object v0, p0, Lcom/android/providers/contacts/TransactionContext;->mStaleSearchIndexContacts:Ljava/util/HashSet;

    .line 253
    return-void
.end method

.method public dataEventDeleted(J)V
    .locals 3
    .param p1, "dataId"    # J

    .prologue
    .line 128
    iget-object v0, p0, Lcom/android/providers/contacts/TransactionContext;->mDeletedDataEvents:Ljava/util/HashSet;

    if-nez v0, :cond_0

    invoke-static {}, Lcom/google/android/collect/Sets;->newHashSet()Ljava/util/HashSet;

    move-result-object v0

    iput-object v0, p0, Lcom/android/providers/contacts/TransactionContext;->mDeletedDataEvents:Ljava/util/HashSet;

    .line 129
    :cond_0
    iget-object v0, p0, Lcom/android/providers/contacts/TransactionContext;->mDeletedDataEvents:Ljava/util/HashSet;

    invoke-static {p1, p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    .line 130
    return-void
.end method

.method public dataEventInserted(J)V
    .locals 3
    .param p1, "dataId"    # J

    .prologue
    .line 133
    iget-object v0, p0, Lcom/android/providers/contacts/TransactionContext;->mInsertedDataEvents:Ljava/util/HashSet;

    if-nez v0, :cond_0

    invoke-static {}, Lcom/google/android/collect/Sets;->newHashSet()Ljava/util/HashSet;

    move-result-object v0

    iput-object v0, p0, Lcom/android/providers/contacts/TransactionContext;->mInsertedDataEvents:Ljava/util/HashSet;

    .line 134
    :cond_0
    iget-object v0, p0, Lcom/android/providers/contacts/TransactionContext;->mInsertedDataEvents:Ljava/util/HashSet;

    invoke-static {p1, p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    .line 135
    return-void
.end method

.method public dataEventUpdated(J)V
    .locals 3
    .param p1, "dataId"    # J

    .prologue
    .line 138
    iget-object v0, p0, Lcom/android/providers/contacts/TransactionContext;->mUpdatedDataEvents:Ljava/util/HashSet;

    if-nez v0, :cond_0

    invoke-static {}, Lcom/google/android/collect/Sets;->newHashSet()Ljava/util/HashSet;

    move-result-object v0

    iput-object v0, p0, Lcom/android/providers/contacts/TransactionContext;->mUpdatedDataEvents:Ljava/util/HashSet;

    .line 139
    :cond_0
    iget-object v0, p0, Lcom/android/providers/contacts/TransactionContext;->mUpdatedDataEvents:Ljava/util/HashSet;

    invoke-static {p1, p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    .line 140
    return-void
.end method

.method public getAccountIdOrNullForRawContact(J)Ljava/lang/Long;
    .locals 3
    .param p1, "rawContactId"    # J

    .prologue
    .line 190
    iget-object v0, p0, Lcom/android/providers/contacts/TransactionContext;->mInsertedRawContactsAccounts:Ljava/util/HashMap;

    if-nez v0, :cond_0

    invoke-static {}, Lcom/google/android/collect/Maps;->newHashMap()Ljava/util/HashMap;

    move-result-object v0

    iput-object v0, p0, Lcom/android/providers/contacts/TransactionContext;->mInsertedRawContactsAccounts:Ljava/util/HashMap;

    .line 191
    :cond_0
    iget-object v0, p0, Lcom/android/providers/contacts/TransactionContext;->mInsertedRawContactsAccounts:Ljava/util/HashMap;

    invoke-static {p1, p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Long;

    return-object v0
.end method

.method public getChangedRawContactIds()Ljava/util/Set;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Set",
            "<",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation

    .prologue
    .line 170
    iget-object v0, p0, Lcom/android/providers/contacts/TransactionContext;->mChangedRawContacts:Ljava/util/HashSet;

    if-nez v0, :cond_0

    invoke-static {}, Lcom/google/android/collect/Sets;->newHashSet()Ljava/util/HashSet;

    move-result-object v0

    iput-object v0, p0, Lcom/android/providers/contacts/TransactionContext;->mChangedRawContacts:Ljava/util/HashSet;

    .line 171
    :cond_0
    iget-object v0, p0, Lcom/android/providers/contacts/TransactionContext;->mChangedRawContacts:Ljava/util/HashSet;

    return-object v0
.end method

.method public getDeletedDataEventIds()Ljava/util/Set;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Set",
            "<",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation

    .prologue
    .line 200
    iget-object v0, p0, Lcom/android/providers/contacts/TransactionContext;->mDeletedDataEvents:Ljava/util/HashSet;

    if-nez v0, :cond_0

    invoke-static {}, Lcom/google/android/collect/Sets;->newHashSet()Ljava/util/HashSet;

    move-result-object v0

    iput-object v0, p0, Lcom/android/providers/contacts/TransactionContext;->mDeletedDataEvents:Ljava/util/HashSet;

    .line 201
    :cond_0
    iget-object v0, p0, Lcom/android/providers/contacts/TransactionContext;->mDeletedDataEvents:Ljava/util/HashSet;

    return-object v0
.end method

.method public getDeletedRawContactIds()Ljava/util/Set;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Set",
            "<",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation

    .prologue
    .line 195
    iget-object v0, p0, Lcom/android/providers/contacts/TransactionContext;->mDeletedRawContacts:Ljava/util/HashSet;

    if-nez v0, :cond_0

    invoke-static {}, Lcom/google/android/collect/Sets;->newHashSet()Ljava/util/HashSet;

    move-result-object v0

    iput-object v0, p0, Lcom/android/providers/contacts/TransactionContext;->mDeletedRawContacts:Ljava/util/HashSet;

    .line 196
    :cond_0
    iget-object v0, p0, Lcom/android/providers/contacts/TransactionContext;->mDeletedRawContacts:Ljava/util/HashSet;

    return-object v0
.end method

.method public getDirtyRawContactIds()Ljava/util/Set;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Set",
            "<",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation

    .prologue
    .line 165
    iget-object v0, p0, Lcom/android/providers/contacts/TransactionContext;->mDirtyRawContacts:Ljava/util/HashSet;

    if-nez v0, :cond_0

    invoke-static {}, Lcom/google/android/collect/Sets;->newHashSet()Ljava/util/HashSet;

    move-result-object v0

    iput-object v0, p0, Lcom/android/providers/contacts/TransactionContext;->mDirtyRawContacts:Ljava/util/HashSet;

    .line 166
    :cond_0
    iget-object v0, p0, Lcom/android/providers/contacts/TransactionContext;->mDirtyRawContacts:Ljava/util/HashSet;

    return-object v0
.end method

.method public getInsertedDataEventIds()Ljava/util/Set;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Set",
            "<",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation

    .prologue
    .line 205
    iget-object v0, p0, Lcom/android/providers/contacts/TransactionContext;->mInsertedDataEvents:Ljava/util/HashSet;

    if-nez v0, :cond_0

    invoke-static {}, Lcom/google/android/collect/Sets;->newHashSet()Ljava/util/HashSet;

    move-result-object v0

    iput-object v0, p0, Lcom/android/providers/contacts/TransactionContext;->mInsertedDataEvents:Ljava/util/HashSet;

    .line 206
    :cond_0
    iget-object v0, p0, Lcom/android/providers/contacts/TransactionContext;->mInsertedDataEvents:Ljava/util/HashSet;

    return-object v0
.end method

.method public getInsertedRawContactIds()Ljava/util/Set;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Set",
            "<",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation

    .prologue
    .line 155
    iget-object v0, p0, Lcom/android/providers/contacts/TransactionContext;->mInsertedRawContactsAccounts:Ljava/util/HashMap;

    if-nez v0, :cond_0

    invoke-static {}, Lcom/google/android/collect/Maps;->newHashMap()Ljava/util/HashMap;

    move-result-object v0

    iput-object v0, p0, Lcom/android/providers/contacts/TransactionContext;->mInsertedRawContactsAccounts:Ljava/util/HashMap;

    .line 156
    :cond_0
    iget-object v0, p0, Lcom/android/providers/contacts/TransactionContext;->mInsertedRawContactsAccounts:Ljava/util/HashMap;

    invoke-virtual {v0}, Ljava/util/HashMap;->keySet()Ljava/util/Set;

    move-result-object v0

    return-object v0
.end method

.method public getLastModifiedGroupIds()Ljava/util/Set;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Set",
            "<",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation

    .prologue
    .line 221
    iget-object v0, p0, Lcom/android/providers/contacts/TransactionContext;->mLastModifiedGroups:Ljava/util/HashSet;

    if-nez v0, :cond_0

    invoke-static {}, Lcom/google/android/collect/Sets;->newHashSet()Ljava/util/HashSet;

    move-result-object v0

    iput-object v0, p0, Lcom/android/providers/contacts/TransactionContext;->mLastModifiedGroups:Ljava/util/HashSet;

    .line 222
    :cond_0
    iget-object v0, p0, Lcom/android/providers/contacts/TransactionContext;->mLastModifiedGroups:Ljava/util/HashSet;

    return-object v0
.end method

.method public getLastModifiedRawContactIds()Ljava/util/Set;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Set",
            "<",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation

    .prologue
    .line 215
    iget-object v0, p0, Lcom/android/providers/contacts/TransactionContext;->mLastModifiedRawContacts:Ljava/util/HashSet;

    if-nez v0, :cond_0

    invoke-static {}, Lcom/google/android/collect/Sets;->newHashSet()Ljava/util/HashSet;

    move-result-object v0

    iput-object v0, p0, Lcom/android/providers/contacts/TransactionContext;->mLastModifiedRawContacts:Ljava/util/HashSet;

    .line 216
    :cond_0
    iget-object v0, p0, Lcom/android/providers/contacts/TransactionContext;->mLastModifiedRawContacts:Ljava/util/HashSet;

    return-object v0
.end method

.method public getStaleSearchIndexContactIds()Ljava/util/Set;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Set",
            "<",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation

    .prologue
    .line 180
    iget-object v0, p0, Lcom/android/providers/contacts/TransactionContext;->mStaleSearchIndexContacts:Ljava/util/HashSet;

    if-nez v0, :cond_0

    invoke-static {}, Lcom/google/android/collect/Sets;->newHashSet()Ljava/util/HashSet;

    move-result-object v0

    iput-object v0, p0, Lcom/android/providers/contacts/TransactionContext;->mStaleSearchIndexContacts:Ljava/util/HashSet;

    .line 181
    :cond_0
    iget-object v0, p0, Lcom/android/providers/contacts/TransactionContext;->mStaleSearchIndexContacts:Ljava/util/HashSet;

    return-object v0
.end method

.method public getStaleSearchIndexRawContactIds()Ljava/util/Set;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Set",
            "<",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation

    .prologue
    .line 175
    iget-object v0, p0, Lcom/android/providers/contacts/TransactionContext;->mStaleSearchIndexRawContacts:Ljava/util/HashSet;

    if-nez v0, :cond_0

    invoke-static {}, Lcom/google/android/collect/Sets;->newHashSet()Ljava/util/HashSet;

    move-result-object v0

    iput-object v0, p0, Lcom/android/providers/contacts/TransactionContext;->mStaleSearchIndexRawContacts:Ljava/util/HashSet;

    .line 176
    :cond_0
    iget-object v0, p0, Lcom/android/providers/contacts/TransactionContext;->mStaleSearchIndexRawContacts:Ljava/util/HashSet;

    return-object v0
.end method

.method public getUpdatedDataEventIds()Ljava/util/Set;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Set",
            "<",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation

    .prologue
    .line 210
    iget-object v0, p0, Lcom/android/providers/contacts/TransactionContext;->mUpdatedDataEvents:Ljava/util/HashSet;

    if-nez v0, :cond_0

    invoke-static {}, Lcom/google/android/collect/Sets;->newHashSet()Ljava/util/HashSet;

    move-result-object v0

    iput-object v0, p0, Lcom/android/providers/contacts/TransactionContext;->mUpdatedDataEvents:Ljava/util/HashSet;

    .line 211
    :cond_0
    iget-object v0, p0, Lcom/android/providers/contacts/TransactionContext;->mUpdatedDataEvents:Ljava/util/HashSet;

    return-object v0
.end method

.method public getUpdatedRawContactIds()Ljava/util/Set;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Set",
            "<",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation

    .prologue
    .line 160
    iget-object v0, p0, Lcom/android/providers/contacts/TransactionContext;->mUpdatedRawContacts:Ljava/util/HashSet;

    if-nez v0, :cond_0

    invoke-static {}, Lcom/google/android/collect/Sets;->newHashSet()Ljava/util/HashSet;

    move-result-object v0

    iput-object v0, p0, Lcom/android/providers/contacts/TransactionContext;->mUpdatedRawContacts:Ljava/util/HashSet;

    .line 161
    :cond_0
    iget-object v0, p0, Lcom/android/providers/contacts/TransactionContext;->mUpdatedRawContacts:Ljava/util/HashSet;

    return-object v0
.end method

.method public getUpdatedSyncStates()Ljava/util/Set;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Set",
            "<",
            "Ljava/util/Map$Entry",
            "<",
            "Ljava/lang/Long;",
            "Ljava/lang/Object;",
            ">;>;"
        }
    .end annotation

    .prologue
    .line 185
    iget-object v0, p0, Lcom/android/providers/contacts/TransactionContext;->mUpdatedSyncStates:Ljava/util/HashMap;

    if-nez v0, :cond_0

    invoke-static {}, Lcom/google/android/collect/Maps;->newHashMap()Ljava/util/HashMap;

    move-result-object v0

    iput-object v0, p0, Lcom/android/providers/contacts/TransactionContext;->mUpdatedSyncStates:Ljava/util/HashMap;

    .line 186
    :cond_0
    iget-object v0, p0, Lcom/android/providers/contacts/TransactionContext;->mUpdatedSyncStates:Ljava/util/HashMap;

    invoke-virtual {v0}, Ljava/util/HashMap;->entrySet()Ljava/util/Set;

    move-result-object v0

    return-object v0
.end method

.method public groupsLastModified(J)V
    .locals 3
    .param p1, "groupId"    # J

    .prologue
    .line 149
    iget-object v0, p0, Lcom/android/providers/contacts/TransactionContext;->mLastModifiedGroups:Ljava/util/HashSet;

    if-nez v0, :cond_0

    invoke-static {}, Lcom/google/android/collect/Sets;->newHashSet()Ljava/util/HashSet;

    move-result-object v0

    iput-object v0, p0, Lcom/android/providers/contacts/TransactionContext;->mLastModifiedGroups:Ljava/util/HashSet;

    .line 150
    :cond_0
    iget-object v0, p0, Lcom/android/providers/contacts/TransactionContext;->mLastModifiedGroups:Ljava/util/HashSet;

    invoke-static {p1, p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    .line 151
    return-void
.end method

.method public invalidateSearchIndexForContact(J)V
    .locals 3
    .param p1, "contactId"    # J

    .prologue
    .line 112
    iget-object v0, p0, Lcom/android/providers/contacts/TransactionContext;->mStaleSearchIndexContacts:Ljava/util/HashSet;

    if-nez v0, :cond_0

    invoke-static {}, Lcom/google/android/collect/Sets;->newHashSet()Ljava/util/HashSet;

    move-result-object v0

    iput-object v0, p0, Lcom/android/providers/contacts/TransactionContext;->mStaleSearchIndexContacts:Ljava/util/HashSet;

    .line 113
    :cond_0
    iget-object v0, p0, Lcom/android/providers/contacts/TransactionContext;->mStaleSearchIndexContacts:Ljava/util/HashSet;

    invoke-static {p1, p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    .line 114
    return-void
.end method

.method public invalidateSearchIndexForRawContact(J)V
    .locals 3
    .param p1, "rawContactId"    # J

    .prologue
    .line 107
    iget-object v0, p0, Lcom/android/providers/contacts/TransactionContext;->mStaleSearchIndexRawContacts:Ljava/util/HashSet;

    if-nez v0, :cond_0

    invoke-static {}, Lcom/google/android/collect/Sets;->newHashSet()Ljava/util/HashSet;

    move-result-object v0

    iput-object v0, p0, Lcom/android/providers/contacts/TransactionContext;->mStaleSearchIndexRawContacts:Ljava/util/HashSet;

    .line 108
    :cond_0
    iget-object v0, p0, Lcom/android/providers/contacts/TransactionContext;->mStaleSearchIndexRawContacts:Ljava/util/HashSet;

    invoke-static {p1, p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    .line 109
    return-void
.end method

.method public isNewRawContact(J)Z
    .locals 3
    .param p1, "rawContactId"    # J

    .prologue
    .line 227
    iget-object v0, p0, Lcom/android/providers/contacts/TransactionContext;->mInsertedRawContactsAccounts:Ljava/util/HashMap;

    if-nez v0, :cond_0

    invoke-static {}, Lcom/google/android/collect/Maps;->newHashMap()Ljava/util/HashMap;

    move-result-object v0

    iput-object v0, p0, Lcom/android/providers/contacts/TransactionContext;->mInsertedRawContactsAccounts:Ljava/util/HashMap;

    .line 228
    :cond_0
    iget-object v0, p0, Lcom/android/providers/contacts/TransactionContext;->mInsertedRawContactsAccounts:Ljava/util/HashMap;

    invoke-static {p1, p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method public markRawContactChangedOrDeletedOrInserted(J)V
    .locals 3
    .param p1, "rawContactId"    # J

    .prologue
    .line 87
    iget-object v0, p0, Lcom/android/providers/contacts/TransactionContext;->mChangedRawContacts:Ljava/util/HashSet;

    if-nez v0, :cond_0

    .line 88
    invoke-static {}, Lcom/google/android/collect/Sets;->newHashSet()Ljava/util/HashSet;

    move-result-object v0

    iput-object v0, p0, Lcom/android/providers/contacts/TransactionContext;->mChangedRawContacts:Ljava/util/HashSet;

    .line 90
    :cond_0
    iget-object v0, p0, Lcom/android/providers/contacts/TransactionContext;->mChangedRawContacts:Ljava/util/HashSet;

    invoke-static {p1, p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    .line 91
    return-void
.end method

.method public markRawContactChangedOrDeletedOrInsertedSet(Ljava/util/HashSet;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/HashSet",
            "<",
            "Ljava/lang/Long;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 95
    .local p1, "rawContactIdsSet":Ljava/util/HashSet;, "Ljava/util/HashSet<Ljava/lang/Long;>;"
    iget-object v0, p0, Lcom/android/providers/contacts/TransactionContext;->mChangedRawContacts:Ljava/util/HashSet;

    if-nez v0, :cond_0

    .line 96
    invoke-static {}, Lcom/google/android/collect/Sets;->newHashSet()Ljava/util/HashSet;

    move-result-object v0

    iput-object v0, p0, Lcom/android/providers/contacts/TransactionContext;->mChangedRawContacts:Ljava/util/HashSet;

    .line 98
    :cond_0
    iget-object v0, p0, Lcom/android/providers/contacts/TransactionContext;->mChangedRawContacts:Ljava/util/HashSet;

    invoke-virtual {v0, p1}, Ljava/util/HashSet;->addAll(Ljava/util/Collection;)Z

    .line 99
    return-void
.end method

.method public markRawContactDirtyAndChanged(JZ)V
    .locals 3
    .param p1, "rawContactId"    # J
    .param p3, "isSyncAdapter"    # Z

    .prologue
    .line 76
    if-nez p3, :cond_1

    .line 77
    iget-object v0, p0, Lcom/android/providers/contacts/TransactionContext;->mDirtyRawContacts:Ljava/util/HashSet;

    if-nez v0, :cond_0

    .line 78
    invoke-static {}, Lcom/google/android/collect/Sets;->newHashSet()Ljava/util/HashSet;

    move-result-object v0

    iput-object v0, p0, Lcom/android/providers/contacts/TransactionContext;->mDirtyRawContacts:Ljava/util/HashSet;

    .line 80
    :cond_0
    iget-object v0, p0, Lcom/android/providers/contacts/TransactionContext;->mDirtyRawContacts:Ljava/util/HashSet;

    invoke-static {p1, p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    .line 83
    :cond_1
    invoke-virtual {p0, p1, p2}, Lcom/android/providers/contacts/TransactionContext;->markRawContactChangedOrDeletedOrInserted(J)V

    .line 84
    return-void
.end method

.method public rawContactDeleted(J)V
    .locals 3
    .param p1, "rawContactId"    # J

    .prologue
    .line 117
    iget-object v0, p0, Lcom/android/providers/contacts/TransactionContext;->mDeletedRawContacts:Ljava/util/HashSet;

    if-nez v0, :cond_0

    invoke-static {}, Lcom/google/android/collect/Sets;->newHashSet()Ljava/util/HashSet;

    move-result-object v0

    iput-object v0, p0, Lcom/android/providers/contacts/TransactionContext;->mDeletedRawContacts:Ljava/util/HashSet;

    .line 118
    :cond_0
    iget-object v0, p0, Lcom/android/providers/contacts/TransactionContext;->mDeletedRawContacts:Ljava/util/HashSet;

    invoke-static {p1, p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    .line 119
    return-void
.end method

.method public rawContactDeletedSet(Ljava/util/HashSet;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/HashSet",
            "<",
            "Ljava/lang/Long;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 123
    .local p1, "rawContactIdsSet":Ljava/util/HashSet;, "Ljava/util/HashSet<Ljava/lang/Long;>;"
    iget-object v0, p0, Lcom/android/providers/contacts/TransactionContext;->mDeletedRawContacts:Ljava/util/HashSet;

    if-nez v0, :cond_0

    invoke-static {}, Lcom/google/android/collect/Sets;->newHashSet()Ljava/util/HashSet;

    move-result-object v0

    iput-object v0, p0, Lcom/android/providers/contacts/TransactionContext;->mDeletedRawContacts:Ljava/util/HashSet;

    .line 124
    :cond_0
    iget-object v0, p0, Lcom/android/providers/contacts/TransactionContext;->mDeletedRawContacts:Ljava/util/HashSet;

    invoke-virtual {v0, p1}, Ljava/util/HashSet;->addAll(Ljava/util/Collection;)Z

    .line 125
    return-void
.end method

.method public rawContactInserted(JJ)V
    .locals 3
    .param p1, "rawContactId"    # J
    .param p3, "accountId"    # J

    .prologue
    .line 64
    iget-object v0, p0, Lcom/android/providers/contacts/TransactionContext;->mInsertedRawContactsAccounts:Ljava/util/HashMap;

    if-nez v0, :cond_0

    invoke-static {}, Lcom/google/android/collect/Maps;->newHashMap()Ljava/util/HashMap;

    move-result-object v0

    iput-object v0, p0, Lcom/android/providers/contacts/TransactionContext;->mInsertedRawContactsAccounts:Ljava/util/HashMap;

    .line 65
    :cond_0
    iget-object v0, p0, Lcom/android/providers/contacts/TransactionContext;->mInsertedRawContactsAccounts:Ljava/util/HashMap;

    invoke-static {p1, p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-static {p3, p4}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 67
    invoke-virtual {p0, p1, p2}, Lcom/android/providers/contacts/TransactionContext;->markRawContactChangedOrDeletedOrInserted(J)V

    .line 68
    return-void
.end method

.method public rawContactLastModified(J)V
    .locals 3
    .param p1, "rawContactId"    # J

    .prologue
    .line 143
    iget-object v0, p0, Lcom/android/providers/contacts/TransactionContext;->mLastModifiedRawContacts:Ljava/util/HashSet;

    if-nez v0, :cond_0

    invoke-static {}, Lcom/google/android/collect/Sets;->newHashSet()Ljava/util/HashSet;

    move-result-object v0

    iput-object v0, p0, Lcom/android/providers/contacts/TransactionContext;->mLastModifiedRawContacts:Ljava/util/HashSet;

    .line 144
    :cond_0
    iget-object v0, p0, Lcom/android/providers/contacts/TransactionContext;->mLastModifiedRawContacts:Ljava/util/HashSet;

    invoke-static {p1, p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    .line 145
    return-void
.end method

.method public rawContactUpdated(J)V
    .locals 3
    .param p1, "rawContactId"    # J

    .prologue
    .line 71
    iget-object v0, p0, Lcom/android/providers/contacts/TransactionContext;->mUpdatedRawContacts:Ljava/util/HashSet;

    if-nez v0, :cond_0

    invoke-static {}, Lcom/google/android/collect/Sets;->newHashSet()Ljava/util/HashSet;

    move-result-object v0

    iput-object v0, p0, Lcom/android/providers/contacts/TransactionContext;->mUpdatedRawContacts:Ljava/util/HashSet;

    .line 72
    :cond_0
    iget-object v0, p0, Lcom/android/providers/contacts/TransactionContext;->mUpdatedRawContacts:Ljava/util/HashSet;

    invoke-static {p1, p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    .line 73
    return-void
.end method

.method public syncStateUpdated(JLjava/lang/Object;)V
    .locals 3
    .param p1, "rowId"    # J
    .param p3, "data"    # Ljava/lang/Object;

    .prologue
    .line 102
    iget-object v0, p0, Lcom/android/providers/contacts/TransactionContext;->mUpdatedSyncStates:Ljava/util/HashMap;

    if-nez v0, :cond_0

    invoke-static {}, Lcom/google/android/collect/Maps;->newHashMap()Ljava/util/HashMap;

    move-result-object v0

    iput-object v0, p0, Lcom/android/providers/contacts/TransactionContext;->mUpdatedSyncStates:Ljava/util/HashMap;

    .line 103
    :cond_0
    iget-object v0, p0, Lcom/android/providers/contacts/TransactionContext;->mUpdatedSyncStates:Ljava/util/HashMap;

    invoke-static {p1, p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-virtual {v0, v1, p3}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 104
    return-void
.end method
