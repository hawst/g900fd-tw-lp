.class Lcom/android/providers/contacts/ContactLocaleUtils$TraditionalChineseTWContactUtils;
.super Lcom/android/providers/contacts/ContactLocaleUtils$ContactLocaleUtilsBase;
.source "ContactLocaleUtils.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/providers/contacts/ContactLocaleUtils;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "TraditionalChineseTWContactUtils"
.end annotation


# direct methods
.method public constructor <init>(Lcom/android/providers/contacts/LocaleSet;)V
    .locals 0
    .param p1, "locale"    # Lcom/android/providers/contacts/LocaleSet;

    .prologue
    .line 691
    invoke-direct {p0, p1}, Lcom/android/providers/contacts/ContactLocaleUtils$ContactLocaleUtilsBase;-><init>(Lcom/android/providers/contacts/LocaleSet;)V

    .line 692
    return-void
.end method

.method public static getBPMFNameLookupKeys(Ljava/lang/String;)Ljava/util/Iterator;
    .locals 15
    .param p0, "name"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "Ljava/util/Iterator",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    const/16 v14, 0x20

    const/4 v13, 0x0

    .line 705
    new-instance v6, Ljava/util/HashSet;

    invoke-direct {v6}, Ljava/util/HashSet;-><init>()V

    .line 706
    .local v6, "keys":Ljava/util/HashSet;, "Ljava/util/HashSet<Ljava/lang/String;>;"
    invoke-virtual {p0}, Ljava/lang/String;->length()I

    move-result v7

    .line 707
    .local v7, "maxNameLength":I
    const/16 v11, 0x1e

    if-le v7, v11, :cond_0

    .line 708
    const/16 v7, 0x1e

    .line 710
    :cond_0
    invoke-static {}, Lcom/android/providers/contacts/HanziToBPMF;->getIntance()Lcom/android/providers/contacts/HanziToBPMF;

    move-result-object v11

    invoke-virtual {p0, v13, v7}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v12

    invoke-virtual {v11, v12}, Lcom/android/providers/contacts/HanziToBPMF;->get(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v10

    .line 711
    .local v10, "tokens":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/android/providers/contacts/HanziToBPMF$Token;>;"
    if-nez v10, :cond_1

    .line 712
    const/4 v11, 0x0

    .line 763
    :goto_0
    return-object v11

    .line 715
    :cond_1
    invoke-virtual {v10}, Ljava/util/ArrayList;->size()I

    move-result v9

    .line 716
    .local v9, "tokenCount":I
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    .line 717
    .local v1, "keyBpmf":Ljava/lang/StringBuilder;
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    .line 721
    .local v3, "keyInitial":Ljava/lang/StringBuilder;
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    .line 722
    .local v5, "keyOrignal":Ljava/lang/StringBuilder;
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    .line 723
    .local v2, "keyBpmfNotMap":Ljava/lang/StringBuilder;
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    .line 725
    .local v4, "keyInitialNotMap":Ljava/lang/StringBuilder;
    add-int/lit8 v0, v9, -0x1

    .local v0, "i":I
    :goto_1
    if-ltz v0, :cond_7

    .line 726
    invoke-virtual {v10, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Lcom/android/providers/contacts/HanziToBPMF$Token;

    .line 727
    .local v8, "token":Lcom/android/providers/contacts/HanziToBPMF$Token;
    const/4 v11, 0x2

    iget v12, v8, Lcom/android/providers/contacts/HanziToBPMF$Token;->type:I

    if-ne v11, v12, :cond_3

    .line 728
    iget-object v11, v8, Lcom/android/providers/contacts/HanziToBPMF$Token;->target:Ljava/lang/String;

    invoke-static {v11}, Lcom/android/providers/contacts/util/DialerKeypadNumberUtils;->makeActionCodeHKTW(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v1, v13, v11}, Ljava/lang/StringBuilder;->insert(ILjava/lang/String;)Ljava/lang/StringBuilder;

    .line 729
    iget-object v11, v8, Lcom/android/providers/contacts/HanziToBPMF$Token;->target:Ljava/lang/String;

    invoke-virtual {v11, v13}, Ljava/lang/String;->charAt(I)C

    move-result v11

    invoke-static {v11}, Lcom/android/providers/contacts/util/DialerKeypadNumberUtils;->convertTraditionalChineseKeypadLettersToDigits(C)C

    move-result v11

    invoke-virtual {v3, v13, v11}, Ljava/lang/StringBuilder;->insert(IC)Ljava/lang/StringBuilder;

    .line 730
    iget-object v11, v8, Lcom/android/providers/contacts/HanziToBPMF$Token;->target:Ljava/lang/String;

    invoke-virtual {v2, v13, v11}, Ljava/lang/StringBuilder;->insert(ILjava/lang/String;)Ljava/lang/StringBuilder;

    .line 731
    iget-object v11, v8, Lcom/android/providers/contacts/HanziToBPMF$Token;->target:Ljava/lang/String;

    invoke-virtual {v11, v13}, Ljava/lang/String;->charAt(I)C

    move-result v11

    invoke-virtual {v4, v13, v11}, Ljava/lang/StringBuilder;->insert(IC)Ljava/lang/StringBuilder;

    .line 753
    :goto_2
    iget-object v11, v8, Lcom/android/providers/contacts/HanziToBPMF$Token;->source:Ljava/lang/String;

    invoke-virtual {v5, v13, v11}, Ljava/lang/StringBuilder;->insert(ILjava/lang/String;)Ljava/lang/StringBuilder;

    .line 754
    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v6, v11}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    .line 755
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v6, v11}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    .line 756
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v6, v11}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    .line 758
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v12

    invoke-virtual {v11, v12}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v11

    if-nez v11, :cond_2

    .line 759
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v6, v11}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    .line 760
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v6, v11}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    .line 725
    :cond_2
    add-int/lit8 v0, v0, -0x1

    goto :goto_1

    .line 732
    :cond_3
    const/4 v11, 0x1

    iget v12, v8, Lcom/android/providers/contacts/HanziToBPMF$Token;->type:I

    if-ne v11, v12, :cond_6

    .line 734
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->length()I

    move-result v11

    if-lez v11, :cond_4

    .line 735
    invoke-virtual {v1, v13, v14}, Ljava/lang/StringBuilder;->insert(IC)Ljava/lang/StringBuilder;

    .line 736
    invoke-virtual {v2, v13, v14}, Ljava/lang/StringBuilder;->insert(IC)Ljava/lang/StringBuilder;

    .line 739
    :cond_4
    invoke-virtual {v5}, Ljava/lang/StringBuilder;->length()I

    move-result v11

    if-lez v11, :cond_5

    .line 740
    invoke-virtual {v5, v13, v14}, Ljava/lang/StringBuilder;->insert(IC)Ljava/lang/StringBuilder;

    .line 743
    :cond_5
    iget-object v11, v8, Lcom/android/providers/contacts/HanziToBPMF$Token;->source:Ljava/lang/String;

    invoke-static {v11}, Lcom/android/providers/contacts/util/DialerKeypadNumberUtils;->makeActionCodeHKTW(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v1, v13, v11}, Ljava/lang/StringBuilder;->insert(ILjava/lang/String;)Ljava/lang/StringBuilder;

    .line 744
    iget-object v11, v8, Lcom/android/providers/contacts/HanziToBPMF$Token;->source:Ljava/lang/String;

    invoke-virtual {v11, v13}, Ljava/lang/String;->charAt(I)C

    move-result v11

    invoke-static {v11}, Lcom/android/providers/contacts/util/DialerKeypadNumberUtils;->convertTraditionalChineseKeypadLettersToDigits(C)C

    move-result v11

    invoke-virtual {v3, v13, v11}, Ljava/lang/StringBuilder;->insert(IC)Ljava/lang/StringBuilder;

    .line 745
    iget-object v11, v8, Lcom/android/providers/contacts/HanziToBPMF$Token;->source:Ljava/lang/String;

    invoke-virtual {v2, v13, v11}, Ljava/lang/StringBuilder;->insert(ILjava/lang/String;)Ljava/lang/StringBuilder;

    .line 746
    iget-object v11, v8, Lcom/android/providers/contacts/HanziToBPMF$Token;->source:Ljava/lang/String;

    invoke-virtual {v11, v13}, Ljava/lang/String;->charAt(I)C

    move-result v11

    invoke-virtual {v4, v13, v11}, Ljava/lang/StringBuilder;->insert(IC)Ljava/lang/StringBuilder;

    goto :goto_2

    .line 748
    :cond_6
    iget-object v11, v8, Lcom/android/providers/contacts/HanziToBPMF$Token;->source:Ljava/lang/String;

    invoke-static {v11}, Lcom/android/providers/contacts/util/DialerKeypadNumberUtils;->makeActionCodeHKTW(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v1, v13, v11}, Ljava/lang/StringBuilder;->insert(ILjava/lang/String;)Ljava/lang/StringBuilder;

    .line 749
    iget-object v11, v8, Lcom/android/providers/contacts/HanziToBPMF$Token;->source:Ljava/lang/String;

    invoke-virtual {v11, v13}, Ljava/lang/String;->charAt(I)C

    move-result v11

    invoke-static {v11}, Lcom/android/providers/contacts/util/DialerKeypadNumberUtils;->convertTraditionalChineseKeypadLettersToDigits(C)C

    move-result v11

    invoke-virtual {v3, v13, v11}, Ljava/lang/StringBuilder;->insert(IC)Ljava/lang/StringBuilder;

    .line 750
    iget-object v11, v8, Lcom/android/providers/contacts/HanziToBPMF$Token;->source:Ljava/lang/String;

    invoke-virtual {v2, v13, v11}, Ljava/lang/StringBuilder;->insert(ILjava/lang/String;)Ljava/lang/StringBuilder;

    .line 751
    iget-object v11, v8, Lcom/android/providers/contacts/HanziToBPMF$Token;->source:Ljava/lang/String;

    invoke-virtual {v11, v13}, Ljava/lang/String;->charAt(I)C

    move-result v11

    invoke-virtual {v4, v13, v11}, Ljava/lang/StringBuilder;->insert(IC)Ljava/lang/StringBuilder;

    goto/16 :goto_2

    .line 763
    .end local v8    # "token":Lcom/android/providers/contacts/HanziToBPMF$Token;
    :cond_7
    invoke-virtual {v6}, Ljava/util/HashSet;->iterator()Ljava/util/Iterator;

    move-result-object v11

    goto/16 :goto_0
.end method


# virtual methods
.method public getNameLookupKeys(Ljava/lang/String;I)Ljava/util/Iterator;
    .locals 1
    .param p1, "name"    # Ljava/lang/String;
    .param p2, "nameStyle"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "I)",
            "Ljava/util/Iterator",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 696
    const/4 v0, 0x4

    if-eq p2, v0, :cond_0

    const/4 v0, 0x5

    if-eq p2, v0, :cond_0

    .line 698
    invoke-static {p1}, Lcom/android/providers/contacts/ContactLocaleUtils$TraditionalChineseTWContactUtils;->getBPMFNameLookupKeys(Ljava/lang/String;)Ljava/util/Iterator;

    move-result-object v0

    .line 700
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
