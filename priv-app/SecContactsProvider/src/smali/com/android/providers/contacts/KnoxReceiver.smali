.class public Lcom/android/providers/contacts/KnoxReceiver;
.super Landroid/content/BroadcastReceiver;
.source "KnoxReceiver.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/android/providers/contacts/KnoxReceiver$UserSwitchReceiver;
    }
.end annotation


# static fields
.field private static mUserSwitchReceiver:Lcom/android/providers/contacts/KnoxReceiver$UserSwitchReceiver;

.field private static mbIsContainerOnlyMode:Z


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 70
    const/4 v0, 0x0

    sput-object v0, Lcom/android/providers/contacts/KnoxReceiver;->mUserSwitchReceiver:Lcom/android/providers/contacts/KnoxReceiver$UserSwitchReceiver;

    .line 71
    const/4 v0, 0x0

    sput-boolean v0, Lcom/android/providers/contacts/KnoxReceiver;->mbIsContainerOnlyMode:Z

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 16
    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    .line 73
    return-void
.end method

.method static synthetic access$000()Z
    .locals 1

    .prologue
    .line 16
    sget-boolean v0, Lcom/android/providers/contacts/KnoxReceiver;->mbIsContainerOnlyMode:Z

    return v0
.end method

.method static synthetic access$002(Z)Z
    .locals 0
    .param p0, "x0"    # Z

    .prologue
    .line 16
    sput-boolean p0, Lcom/android/providers/contacts/KnoxReceiver;->mbIsContainerOnlyMode:Z

    return p0
.end method

.method public static isKnoxContainerOnlyMode(Landroid/content/Context;)Z
    .locals 6
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    const/4 v4, 0x0

    .line 42
    sget-object v5, Lcom/android/providers/contacts/KnoxReceiver;->mUserSwitchReceiver:Lcom/android/providers/contacts/KnoxReceiver$UserSwitchReceiver;

    if-nez v5, :cond_2

    .line 43
    new-instance v5, Lcom/android/providers/contacts/KnoxReceiver$UserSwitchReceiver;

    invoke-direct {v5}, Lcom/android/providers/contacts/KnoxReceiver$UserSwitchReceiver;-><init>()V

    sput-object v5, Lcom/android/providers/contacts/KnoxReceiver;->mUserSwitchReceiver:Lcom/android/providers/contacts/KnoxReceiver$UserSwitchReceiver;

    .line 45
    new-instance v0, Landroid/content/IntentFilter;

    invoke-direct {v0}, Landroid/content/IntentFilter;-><init>()V

    .line 46
    .local v0, "filter":Landroid/content/IntentFilter;
    const-string v5, "android.intent.action.USER_ADDED"

    invoke-virtual {v0, v5}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 47
    const-string v5, "android.intent.action.USER_REMOVED"

    invoke-virtual {v0, v5}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 48
    sget-object v5, Lcom/android/providers/contacts/KnoxReceiver;->mUserSwitchReceiver:Lcom/android/providers/contacts/KnoxReceiver$UserSwitchReceiver;

    invoke-virtual {p0, v5, v0}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    .line 50
    const-string v5, "persona"

    invoke-virtual {p0, v5}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/os/PersonaManager;

    .line 51
    .local v3, "pm":Landroid/os/PersonaManager;
    invoke-static {p0}, Landroid/os/PersonaManager;->isKioskModeEnabled(Landroid/content/Context;)Z

    move-result v5

    sput-boolean v5, Lcom/android/providers/contacts/KnoxReceiver;->mbIsContainerOnlyMode:Z

    .line 54
    if-eqz v3, :cond_0

    sget-boolean v5, Lcom/android/providers/contacts/KnoxReceiver;->mbIsContainerOnlyMode:Z

    if-nez v5, :cond_1

    .line 67
    .end local v0    # "filter":Landroid/content/IntentFilter;
    .end local v3    # "pm":Landroid/os/PersonaManager;
    :cond_0
    :goto_0
    return v4

    .line 57
    .restart local v0    # "filter":Landroid/content/IntentFilter;
    .restart local v3    # "pm":Landroid/os/PersonaManager;
    :cond_1
    const/4 v5, 0x1

    invoke-virtual {v3, v5}, Landroid/os/PersonaManager;->getPersonas(Z)Ljava/util/List;

    move-result-object v2

    .line 58
    .local v2, "personas":Ljava/util/List;, "Ljava/util/List<Landroid/content/pm/PersonaInfo;>;"
    sput-boolean v4, Lcom/android/providers/contacts/KnoxReceiver;->mbIsContainerOnlyMode:Z

    .line 59
    if-eqz v2, :cond_2

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v5

    if-lez v5, :cond_2

    .line 60
    invoke-interface {v2, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/content/pm/PersonaInfo;

    .line 61
    .local v1, "perInfo":Landroid/content/pm/PersonaInfo;
    if-eqz v1, :cond_2

    .line 62
    iget-boolean v4, v1, Landroid/content/pm/PersonaInfo;->isKioskModeEnabled:Z

    sput-boolean v4, Lcom/android/providers/contacts/KnoxReceiver;->mbIsContainerOnlyMode:Z

    .line 67
    .end local v0    # "filter":Landroid/content/IntentFilter;
    .end local v1    # "perInfo":Landroid/content/pm/PersonaInfo;
    .end local v2    # "personas":Ljava/util/List;, "Ljava/util/List<Landroid/content/pm/PersonaInfo;>;"
    .end local v3    # "pm":Landroid/os/PersonaManager;
    :cond_2
    sget-boolean v4, Lcom/android/providers/contacts/KnoxReceiver;->mbIsContainerOnlyMode:Z

    goto :goto_0
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 6
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "intent"    # Landroid/content/Intent;

    .prologue
    .line 22
    if-eqz p2, :cond_0

    .line 23
    invoke-static {p1}, Landroid/preference/PreferenceManager;->getDefaultSharedPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v2

    .line 24
    .local v2, "prefs":Landroid/content/SharedPreferences;
    const/4 v0, 0x0

    .line 25
    .local v0, "bundle":Landroid/os/Bundle;
    const-string v4, "version"

    invoke-static {p1, v4}, Landroid/os/PersonaManager;->getKnoxInfoForApp(Landroid/content/Context;Ljava/lang/String;)Landroid/os/Bundle;

    move-result-object v0

    .line 26
    const-string v4, "TwoWayFlag"

    const/4 v5, 0x0

    invoke-virtual {p2, v4, v5}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v3

    .line 27
    .local v3, "syncable":Z
    const-string v4, "2.0"

    const-string v5, "version"

    invoke-virtual {v0, v5}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_2

    .line 28
    const-string v4, "name"

    invoke-virtual {p2, v4}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 29
    .local v1, "mode":Ljava/lang/String;
    const-string v4, "KNOX"

    invoke-virtual {v4, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_1

    .line 30
    invoke-interface {v2}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v4

    const-string v5, "2wayflag"

    invoke-interface {v4, v5, v3}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    move-result-object v4

    invoke-interface {v4}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 38
    .end local v0    # "bundle":Landroid/os/Bundle;
    .end local v1    # "mode":Ljava/lang/String;
    .end local v2    # "prefs":Landroid/content/SharedPreferences;
    .end local v3    # "syncable":Z
    :cond_0
    :goto_0
    return-void

    .line 32
    .restart local v0    # "bundle":Landroid/os/Bundle;
    .restart local v1    # "mode":Ljava/lang/String;
    .restart local v2    # "prefs":Landroid/content/SharedPreferences;
    .restart local v3    # "syncable":Z
    :cond_1
    invoke-interface {v2}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v4

    const-string v5, "2wayflag2"

    invoke-interface {v4, v5, v3}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    move-result-object v4

    invoke-interface {v4}, Landroid/content/SharedPreferences$Editor;->commit()Z

    goto :goto_0

    .line 34
    .end local v1    # "mode":Ljava/lang/String;
    :cond_2
    const-string v4, "1.0"

    const-string v5, "version"

    invoke-virtual {v0, v5}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 35
    invoke-interface {v2}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v4

    const-string v5, "2wayflag"

    invoke-interface {v4, v5, v3}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    move-result-object v4

    invoke-interface {v4}, Landroid/content/SharedPreferences$Editor;->commit()Z

    goto :goto_0
.end method
