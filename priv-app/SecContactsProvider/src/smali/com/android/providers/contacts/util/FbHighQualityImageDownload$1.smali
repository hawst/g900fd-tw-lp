.class Lcom/android/providers/contacts/util/FbHighQualityImageDownload$1;
.super Landroid/os/Handler;
.source "FbHighQualityImageDownload.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/providers/contacts/util/FbHighQualityImageDownload;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/providers/contacts/util/FbHighQualityImageDownload;


# direct methods
.method constructor <init>(Lcom/android/providers/contacts/util/FbHighQualityImageDownload;)V
    .locals 0

    .prologue
    .line 104
    iput-object p1, p0, Lcom/android/providers/contacts/util/FbHighQualityImageDownload$1;->this$0:Lcom/android/providers/contacts/util/FbHighQualityImageDownload;

    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 5
    .param p1, "msg"    # Landroid/os/Message;

    .prologue
    .line 106
    iget v3, p1, Landroid/os/Message;->what:I

    packed-switch v3, :pswitch_data_0

    .line 152
    const-string v3, "FB_LOGS"

    const-string v4, "Invalid message"

    invoke-static {v3, v4}, Landroid/util/secutil/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I

    .line 155
    :cond_0
    :goto_0
    return-void

    .line 108
    :pswitch_0
    const-string v3, "FB_LOGS"

    const-string v4, "FB_IMAGE_DOWNLOAD_START"

    invoke-static {v3, v4}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 110
    iget-object v3, p0, Lcom/android/providers/contacts/util/FbHighQualityImageDownload$1;->this$0:Lcom/android/providers/contacts/util/FbHighQualityImageDownload;

    iget-boolean v3, v3, Lcom/android/providers/contacts/util/FbHighQualityImageDownload;->mIsFbHighQualitySyncing:Z

    if-eqz v3, :cond_0

    .line 111
    iget-object v3, p0, Lcom/android/providers/contacts/util/FbHighQualityImageDownload$1;->this$0:Lcom/android/providers/contacts/util/FbHighQualityImageDownload;

    # getter for: Lcom/android/providers/contacts/util/FbHighQualityImageDownload;->mQueue:Lcom/android/providers/contacts/util/FbHighQualityImageDownload$FacebookIdQueue;
    invoke-static {v3}, Lcom/android/providers/contacts/util/FbHighQualityImageDownload;->access$100(Lcom/android/providers/contacts/util/FbHighQualityImageDownload;)Lcom/android/providers/contacts/util/FbHighQualityImageDownload$FacebookIdQueue;

    move-result-object v3

    # invokes: Lcom/android/providers/contacts/util/FbHighQualityImageDownload$FacebookIdQueue;->isEmptyQueue()Z
    invoke-static {v3}, Lcom/android/providers/contacts/util/FbHighQualityImageDownload$FacebookIdQueue;->access$200(Lcom/android/providers/contacts/util/FbHighQualityImageDownload$FacebookIdQueue;)Z

    move-result v3

    if-nez v3, :cond_1

    .line 112
    iget-object v3, p0, Lcom/android/providers/contacts/util/FbHighQualityImageDownload$1;->this$0:Lcom/android/providers/contacts/util/FbHighQualityImageDownload;

    # getter for: Lcom/android/providers/contacts/util/FbHighQualityImageDownload;->mQueue:Lcom/android/providers/contacts/util/FbHighQualityImageDownload$FacebookIdQueue;
    invoke-static {v3}, Lcom/android/providers/contacts/util/FbHighQualityImageDownload;->access$100(Lcom/android/providers/contacts/util/FbHighQualityImageDownload;)Lcom/android/providers/contacts/util/FbHighQualityImageDownload$FacebookIdQueue;

    move-result-object v3

    # invokes: Lcom/android/providers/contacts/util/FbHighQualityImageDownload$FacebookIdQueue;->getFacebookIdFromQueue()Lcom/android/providers/contacts/util/FbHighQualityImageDownload$QueueItem;
    invoke-static {v3}, Lcom/android/providers/contacts/util/FbHighQualityImageDownload$FacebookIdQueue;->access$300(Lcom/android/providers/contacts/util/FbHighQualityImageDownload$FacebookIdQueue;)Lcom/android/providers/contacts/util/FbHighQualityImageDownload$QueueItem;

    move-result-object v1

    .line 113
    .local v1, "item":Lcom/android/providers/contacts/util/FbHighQualityImageDownload$QueueItem;
    if-eqz v1, :cond_0

    .line 114
    invoke-virtual {v1}, Lcom/android/providers/contacts/util/FbHighQualityImageDownload$QueueItem;->getRawContactId()Ljava/lang/String;

    move-result-object v2

    .line 115
    .local v2, "rawContactId":Ljava/lang/String;
    invoke-virtual {v1}, Lcom/android/providers/contacts/util/FbHighQualityImageDownload$QueueItem;->getFbId()Ljava/lang/String;

    move-result-object v0

    .line 117
    .local v0, "fbId":Ljava/lang/String;
    iget-object v3, p0, Lcom/android/providers/contacts/util/FbHighQualityImageDownload$1;->this$0:Lcom/android/providers/contacts/util/FbHighQualityImageDownload;

    # invokes: Lcom/android/providers/contacts/util/FbHighQualityImageDownload;->fetchFbImageFromId(Ljava/lang/String;Ljava/lang/String;)V
    invoke-static {v3, v2, v0}, Lcom/android/providers/contacts/util/FbHighQualityImageDownload;->access$400(Lcom/android/providers/contacts/util/FbHighQualityImageDownload;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 120
    .end local v0    # "fbId":Ljava/lang/String;
    .end local v1    # "item":Lcom/android/providers/contacts/util/FbHighQualityImageDownload$QueueItem;
    .end local v2    # "rawContactId":Ljava/lang/String;
    :cond_1
    iget-object v3, p0, Lcom/android/providers/contacts/util/FbHighQualityImageDownload$1;->this$0:Lcom/android/providers/contacts/util/FbHighQualityImageDownload;

    invoke-virtual {v3}, Lcom/android/providers/contacts/util/FbHighQualityImageDownload;->stop()V

    goto :goto_0

    .line 127
    :pswitch_1
    const-string v3, "FB_LOGS"

    const-string v4, "FB_IMAGE_DOWNLOAD_NEXT"

    invoke-static {v3, v4}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 129
    iget-object v3, p0, Lcom/android/providers/contacts/util/FbHighQualityImageDownload$1;->this$0:Lcom/android/providers/contacts/util/FbHighQualityImageDownload;

    iget-boolean v3, v3, Lcom/android/providers/contacts/util/FbHighQualityImageDownload;->mIsFbHighQualitySyncing:Z

    if-eqz v3, :cond_0

    .line 130
    iget-object v3, p0, Lcom/android/providers/contacts/util/FbHighQualityImageDownload$1;->this$0:Lcom/android/providers/contacts/util/FbHighQualityImageDownload;

    # getter for: Lcom/android/providers/contacts/util/FbHighQualityImageDownload;->mQueue:Lcom/android/providers/contacts/util/FbHighQualityImageDownload$FacebookIdQueue;
    invoke-static {v3}, Lcom/android/providers/contacts/util/FbHighQualityImageDownload;->access$100(Lcom/android/providers/contacts/util/FbHighQualityImageDownload;)Lcom/android/providers/contacts/util/FbHighQualityImageDownload$FacebookIdQueue;

    move-result-object v3

    # invokes: Lcom/android/providers/contacts/util/FbHighQualityImageDownload$FacebookIdQueue;->isEmptyQueue()Z
    invoke-static {v3}, Lcom/android/providers/contacts/util/FbHighQualityImageDownload$FacebookIdQueue;->access$200(Lcom/android/providers/contacts/util/FbHighQualityImageDownload$FacebookIdQueue;)Z

    move-result v3

    if-nez v3, :cond_2

    .line 131
    iget-object v3, p0, Lcom/android/providers/contacts/util/FbHighQualityImageDownload$1;->this$0:Lcom/android/providers/contacts/util/FbHighQualityImageDownload;

    # getter for: Lcom/android/providers/contacts/util/FbHighQualityImageDownload;->mQueue:Lcom/android/providers/contacts/util/FbHighQualityImageDownload$FacebookIdQueue;
    invoke-static {v3}, Lcom/android/providers/contacts/util/FbHighQualityImageDownload;->access$100(Lcom/android/providers/contacts/util/FbHighQualityImageDownload;)Lcom/android/providers/contacts/util/FbHighQualityImageDownload$FacebookIdQueue;

    move-result-object v3

    # invokes: Lcom/android/providers/contacts/util/FbHighQualityImageDownload$FacebookIdQueue;->getFacebookIdFromQueue()Lcom/android/providers/contacts/util/FbHighQualityImageDownload$QueueItem;
    invoke-static {v3}, Lcom/android/providers/contacts/util/FbHighQualityImageDownload$FacebookIdQueue;->access$300(Lcom/android/providers/contacts/util/FbHighQualityImageDownload$FacebookIdQueue;)Lcom/android/providers/contacts/util/FbHighQualityImageDownload$QueueItem;

    move-result-object v1

    .line 132
    .restart local v1    # "item":Lcom/android/providers/contacts/util/FbHighQualityImageDownload$QueueItem;
    if-eqz v1, :cond_0

    .line 133
    invoke-virtual {v1}, Lcom/android/providers/contacts/util/FbHighQualityImageDownload$QueueItem;->getRawContactId()Ljava/lang/String;

    move-result-object v2

    .line 134
    .restart local v2    # "rawContactId":Ljava/lang/String;
    invoke-virtual {v1}, Lcom/android/providers/contacts/util/FbHighQualityImageDownload$QueueItem;->getFbId()Ljava/lang/String;

    move-result-object v0

    .line 136
    .restart local v0    # "fbId":Ljava/lang/String;
    iget-object v3, p0, Lcom/android/providers/contacts/util/FbHighQualityImageDownload$1;->this$0:Lcom/android/providers/contacts/util/FbHighQualityImageDownload;

    # invokes: Lcom/android/providers/contacts/util/FbHighQualityImageDownload;->fetchFbImageFromId(Ljava/lang/String;Ljava/lang/String;)V
    invoke-static {v3, v2, v0}, Lcom/android/providers/contacts/util/FbHighQualityImageDownload;->access$400(Lcom/android/providers/contacts/util/FbHighQualityImageDownload;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 139
    .end local v0    # "fbId":Ljava/lang/String;
    .end local v1    # "item":Lcom/android/providers/contacts/util/FbHighQualityImageDownload$QueueItem;
    .end local v2    # "rawContactId":Ljava/lang/String;
    :cond_2
    const-string v3, "FB_LOGS"

    const-string v4, "FB_IMAGE_DOWNLOAD_SUCCESSFULLY_END"

    invoke-static {v3, v4}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 143
    iget-object v3, p0, Lcom/android/providers/contacts/util/FbHighQualityImageDownload$1;->this$0:Lcom/android/providers/contacts/util/FbHighQualityImageDownload;

    const/4 v4, 0x0

    # setter for: Lcom/android/providers/contacts/util/FbHighQualityImageDownload;->mCountOfTryFbAccountSync:I
    invoke-static {v3, v4}, Lcom/android/providers/contacts/util/FbHighQualityImageDownload;->access$502(Lcom/android/providers/contacts/util/FbHighQualityImageDownload;I)I

    .line 145
    iget-object v3, p0, Lcom/android/providers/contacts/util/FbHighQualityImageDownload$1;->this$0:Lcom/android/providers/contacts/util/FbHighQualityImageDownload;

    invoke-virtual {v3}, Lcom/android/providers/contacts/util/FbHighQualityImageDownload;->stop()V

    goto :goto_0

    .line 106
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method
