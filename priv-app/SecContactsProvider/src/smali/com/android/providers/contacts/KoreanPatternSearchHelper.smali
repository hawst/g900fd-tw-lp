.class public Lcom/android/providers/contacts/KoreanPatternSearchHelper;
.super Ljava/lang/Object;
.source "KoreanPatternSearchHelper.java"


# static fields
.field private static KOREAN_JAUM_CONVERT_MAP:[I

.field private static KOREAN_JAUM_CONVERT_MAP_COUNT:I

.field private static KOREAN_RANGE_MAP:[I


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    const/16 v1, 0x1e

    .line 23
    new-array v0, v1, [I

    fill-array-data v0, :array_0

    sput-object v0, Lcom/android/providers/contacts/KoreanPatternSearchHelper;->KOREAN_JAUM_CONVERT_MAP:[I

    .line 58
    sput v1, Lcom/android/providers/contacts/KoreanPatternSearchHelper;->KOREAN_JAUM_CONVERT_MAP_COUNT:I

    .line 60
    new-array v0, v1, [I

    fill-array-data v0, :array_1

    sput-object v0, Lcom/android/providers/contacts/KoreanPatternSearchHelper;->KOREAN_RANGE_MAP:[I

    return-void

    .line 23
    nop

    :array_0
    .array-data 4
        0x1100
        0x1101
        0x0
        0x1102
        0x0
        0x0
        0x1103
        0x1104
        0x1105
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x1106
        0x1107
        0x1108
        0x0
        0x1109
        0x110a
        0x110b
        0x110c
        0x110d
        0x110e
        0x110f
        0x1110
        0x1111
        0x1112
    .end array-data

    .line 60
    :array_1
    .array-data 4
        0xac00
        0xae4c
        0x0
        0xb098
        0x0
        0x0
        0xb2e4
        0xb530
        0xb77c
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0xb9c8
        0xbc14
        0xbe60
        0x0
        0xc0ac
        0xc2f8
        0xc544
        0xc790
        0xc9dc
        0xcc28
        0xce74
        0xd0c0
        0xd30c
        0xd558
    .end array-data
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 22
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method static findConsonant(Ljava/lang/String;)I
    .locals 3
    .param p0, "filter"    # Ljava/lang/String;

    .prologue
    .line 287
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    invoke-virtual {p0}, Ljava/lang/String;->length()I

    move-result v2

    if-ge v1, v2, :cond_1

    .line 288
    invoke-virtual {p0, v1}, Ljava/lang/String;->codePointAt(I)I

    move-result v0

    .line 289
    .local v0, "character":I
    invoke-static {v0}, Lcom/android/providers/contacts/KoreanPatternSearchHelper;->isConsonant(I)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 294
    .end local v0    # "character":I
    .end local v1    # "i":I
    :goto_1
    return v1

    .line 287
    .restart local v0    # "character":I
    .restart local v1    # "i":I
    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 294
    .end local v0    # "character":I
    :cond_1
    const/4 v1, -0x1

    goto :goto_1
.end method

.method static getConsonantsOfName(Ljava/lang/String;)Ljava/lang/String;
    .locals 9
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    const/16 v8, 0x3131

    const v7, 0xac00

    .line 158
    const/4 v1, 0x0

    .line 161
    .local v1, "position":I
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    .line 163
    .local v3, "sb":Ljava/lang/StringBuilder;
    invoke-virtual {p0}, Ljava/lang/String;->length()I

    move-result v4

    .line 164
    .local v4, "stringLength":I
    const/4 v5, 0x0

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->setLength(I)V

    .line 166
    :goto_0
    add-int/lit8 v2, v1, 0x1

    .end local v1    # "position":I
    .local v2, "position":I
    invoke-virtual {p0, v1}, Ljava/lang/String;->codePointAt(I)I

    move-result v0

    .line 167
    .local v0, "character":I
    const/16 v5, 0x20

    if-ne v0, v5, :cond_1

    .line 199
    :goto_1
    if-lt v2, v4, :cond_6

    .line 201
    :cond_0
    :goto_2
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    return-object v5

    .line 173
    :cond_1
    const/16 v5, 0x1100

    if-lt v0, v5, :cond_0

    const/16 v5, 0x1112

    if-le v0, v5, :cond_2

    if-lt v0, v8, :cond_0

    :cond_2
    const/16 v5, 0x314e

    if-le v0, v5, :cond_3

    if-lt v0, v7, :cond_0

    :cond_3
    const v5, 0xd7a3

    if-gt v0, v5, :cond_0

    .line 179
    if-lt v0, v7, :cond_5

    .line 183
    sub-int v5, v0, v7

    div-int/lit16 v5, v5, 0x24c

    add-int/lit16 v0, v5, 0x1100

    .line 197
    :cond_4
    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->appendCodePoint(I)Ljava/lang/StringBuilder;

    goto :goto_1

    .line 184
    :cond_5
    if-lt v0, v8, :cond_4

    .line 187
    add-int/lit16 v5, v0, -0x3131

    sget v6, Lcom/android/providers/contacts/KoreanPatternSearchHelper;->KOREAN_JAUM_CONVERT_MAP_COUNT:I

    if-ge v5, v6, :cond_0

    .line 191
    sget-object v5, Lcom/android/providers/contacts/KoreanPatternSearchHelper;->KOREAN_JAUM_CONVERT_MAP:[I

    add-int/lit16 v6, v0, -0x3131

    aget v0, v5, v6

    .line 192
    if-nez v0, :cond_4

    goto :goto_2

    :cond_6
    move v1, v2

    .end local v2    # "position":I
    .restart local v1    # "position":I
    goto :goto_0
.end method

.method static getPatternFromFilter(Ljava/lang/String;)Ljava/lang/String;
    .locals 4
    .param p0, "filter"    # Ljava/lang/String;

    .prologue
    .line 206
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    .line 209
    .local v2, "sb":Ljava/lang/StringBuilder;
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    invoke-virtual {p0}, Ljava/lang/String;->length()I

    move-result v3

    if-ge v1, v3, :cond_1

    .line 210
    invoke-virtual {p0, v1}, Ljava/lang/String;->codePointAt(I)I

    move-result v0

    .line 211
    .local v0, "character":I
    invoke-static {v0}, Lcom/android/providers/contacts/KoreanPatternSearchHelper;->isConsonant(I)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 214
    const-string v3, "?"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 209
    :goto_1
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 216
    :cond_0
    add-int/lit8 v3, v1, 0x1

    invoke-virtual {p0, v1, v3}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_1

    .line 221
    .end local v0    # "character":I
    :cond_1
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    return-object v3
.end method

.method static hasConsonantAndComplete(Ljava/lang/String;)Z
    .locals 9
    .param p0, "filter"    # Ljava/lang/String;

    .prologue
    const v8, 0xd7a3

    const v7, 0xac00

    const/4 v5, 0x0

    const/4 v4, 0x1

    .line 226
    const/4 v1, 0x0

    .line 227
    .local v1, "hasComplete":Z
    const/4 v2, 0x0

    .line 229
    .local v2, "hasConsonant":Z
    const/4 v3, 0x0

    .local v3, "i":I
    :goto_0
    invoke-virtual {p0}, Ljava/lang/String;->length()I

    move-result v6

    if-ge v3, v6, :cond_7

    .line 230
    invoke-virtual {p0, v3}, Ljava/lang/String;->codePointAt(I)I

    move-result v0

    .line 233
    .local v0, "character":I
    const/16 v6, 0x1100

    if-lt v0, v6, :cond_2

    const/16 v6, 0x1112

    if-le v0, v6, :cond_0

    const/16 v6, 0x3131

    if-lt v0, v6, :cond_2

    :cond_0
    const/16 v6, 0x314e

    if-le v0, v6, :cond_1

    if-lt v0, v7, :cond_2

    :cond_1
    if-le v0, v8, :cond_4

    :cond_2
    move v4, v5

    .line 253
    .end local v0    # "character":I
    :cond_3
    :goto_1
    return v4

    .line 240
    .restart local v0    # "character":I
    :cond_4
    if-lt v0, v7, :cond_6

    if-gt v0, v8, :cond_6

    .line 241
    const/4 v1, 0x1

    .line 242
    if-nez v2, :cond_3

    .line 229
    :cond_5
    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    .line 246
    :cond_6
    const/4 v2, 0x1

    .line 247
    if-eqz v1, :cond_5

    goto :goto_1

    .line 253
    .end local v0    # "character":I
    :cond_7
    if-eqz v1, :cond_8

    if-nez v2, :cond_3

    :cond_8
    move v4, v5

    goto :goto_1
.end method

.method private static isConsonant(I)Z
    .locals 1
    .param p0, "character"    # I

    .prologue
    .line 94
    const/16 v0, 0x3130

    if-le p0, v0, :cond_0

    const/16 v0, 0x314e

    if-gt p0, v0, :cond_0

    .line 95
    const/4 v0, 0x1

    .line 98
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
