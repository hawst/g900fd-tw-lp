.class public Lcom/android/providers/contacts/util/SimContactUtils;
.super Ljava/lang/Object;
.source "SimContactUtils.java"


# static fields
.field private static sSimContactUtils:Lcom/android/providers/contacts/util/SimContactUtils;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 30
    const/4 v0, 0x0

    sput-object v0, Lcom/android/providers/contacts/util/SimContactUtils;->sSimContactUtils:Lcom/android/providers/contacts/util/SimContactUtils;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 26
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static getInstance()Lcom/android/providers/contacts/util/SimContactUtils;
    .locals 1

    .prologue
    .line 43
    sget-object v0, Lcom/android/providers/contacts/util/SimContactUtils;->sSimContactUtils:Lcom/android/providers/contacts/util/SimContactUtils;

    if-nez v0, :cond_0

    .line 44
    new-instance v0, Lcom/android/providers/contacts/util/SimContactUtils;

    invoke-direct {v0}, Lcom/android/providers/contacts/util/SimContactUtils;-><init>()V

    sput-object v0, Lcom/android/providers/contacts/util/SimContactUtils;->sSimContactUtils:Lcom/android/providers/contacts/util/SimContactUtils;

    .line 46
    :cond_0
    sget-object v0, Lcom/android/providers/contacts/util/SimContactUtils;->sSimContactUtils:Lcom/android/providers/contacts/util/SimContactUtils;

    return-object v0
.end method


# virtual methods
.method public getAnrConfigValue()I
    .locals 1

    .prologue
    .line 61
    invoke-static {}, Lcom/android/providers/contacts/util/LoadCscFeatureUtils;->getInstance()Lcom/android/providers/contacts/util/LoadCscFeatureUtils;

    move-result-object v0

    invoke-virtual {v0}, Lcom/android/providers/contacts/util/LoadCscFeatureUtils;->getAnrConfigValue()I

    move-result v0

    return v0
.end method

.method public isMultiSimSupport()Z
    .locals 2

    .prologue
    const/4 v0, 0x1

    .line 79
    invoke-static {}, Lcom/samsung/android/telephony/MultiSimManager;->getSimSlotCount()I

    move-result v1

    if-le v1, v0, :cond_0

    .line 82
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isSim2DbSupport()Z
    .locals 1

    .prologue
    .line 57
    invoke-static {}, Lcom/android/providers/contacts/util/LoadCscFeatureUtils;->getInstance()Lcom/android/providers/contacts/util/LoadCscFeatureUtils;

    move-result-object v0

    invoke-virtual {v0}, Lcom/android/providers/contacts/util/LoadCscFeatureUtils;->isSim2DbEnabled()Z

    move-result v0

    return v0
.end method

.method public isSimDbSupport()Z
    .locals 1

    .prologue
    .line 53
    invoke-static {}, Lcom/android/providers/contacts/util/LoadCscFeatureUtils;->getInstance()Lcom/android/providers/contacts/util/LoadCscFeatureUtils;

    move-result-object v0

    invoke-virtual {v0}, Lcom/android/providers/contacts/util/LoadCscFeatureUtils;->isSimDbEnabled()Z

    move-result v0

    return v0
.end method
