.class Lcom/android/providers/contacts/ContactLocaleUtils$ContactLocaleUtilsBase;
.super Ljava/lang/Object;
.source "ContactLocaleUtils.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/providers/contacts/ContactLocaleUtils;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "ContactLocaleUtilsBase"
.end annotation


# instance fields
.field protected final mAlphabeticIndex:Llibcore/icu/AlphabeticIndex$ImmutableIndex;

.field private final mAlphabeticIndexBucketCount:I

.field private final mEnableSecondaryLocalePinyin:Z

.field private final mNumberBucketIndex:I


# direct methods
.method public constructor <init>(Lcom/android/providers/contacts/LocaleSet;)V
    .locals 9
    .param p1, "locales"    # Lcom/android/providers/contacts/LocaleSet;

    .prologue
    const/16 v8, 0x110a

    const/16 v7, 0x1108

    const/16 v6, 0x1104

    const/16 v5, 0x1101

    const/16 v4, 0xae0

    .line 84
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 97
    invoke-virtual {p1}, Lcom/android/providers/contacts/LocaleSet;->getSecondaryLocale()Ljava/util/Locale;

    move-result-object v1

    .line 98
    .local v1, "secondaryLocale":Ljava/util/Locale;
    invoke-virtual {p1}, Lcom/android/providers/contacts/LocaleSet;->isSecondaryLocaleSimplifiedChinese()Z

    move-result v2

    iput-boolean v2, p0, Lcom/android/providers/contacts/ContactLocaleUtils$ContactLocaleUtilsBase;->mEnableSecondaryLocalePinyin:Z

    .line 99
    new-instance v2, Llibcore/icu/AlphabeticIndex;

    invoke-virtual {p1}, Lcom/android/providers/contacts/LocaleSet;->getPrimaryLocale()Ljava/util/Locale;

    move-result-object v3

    invoke-direct {v2, v3}, Llibcore/icu/AlphabeticIndex;-><init>(Ljava/util/Locale;)V

    const/16 v3, 0x190

    invoke-virtual {v2, v3}, Llibcore/icu/AlphabeticIndex;->setMaxLabelCount(I)Llibcore/icu/AlphabeticIndex;

    move-result-object v0

    .line 101
    .local v0, "ai":Llibcore/icu/AlphabeticIndex;
    if-eqz v1, :cond_0

    .line 102
    invoke-virtual {v0, v1}, Llibcore/icu/AlphabeticIndex;->addLabels(Ljava/util/Locale;)Llibcore/icu/AlphabeticIndex;

    .line 104
    :cond_0
    sget-object v2, Ljava/util/Locale;->ENGLISH:Ljava/util/Locale;

    invoke-virtual {v0, v2}, Llibcore/icu/AlphabeticIndex;->addLabels(Ljava/util/Locale;)Llibcore/icu/AlphabeticIndex;

    move-result-object v2

    sget-object v3, Ljava/util/Locale;->JAPANESE:Ljava/util/Locale;

    invoke-virtual {v2, v3}, Llibcore/icu/AlphabeticIndex;->addLabels(Ljava/util/Locale;)Llibcore/icu/AlphabeticIndex;

    move-result-object v2

    sget-object v3, Ljava/util/Locale;->KOREAN:Ljava/util/Locale;

    invoke-virtual {v2, v3}, Llibcore/icu/AlphabeticIndex;->addLabels(Ljava/util/Locale;)Llibcore/icu/AlphabeticIndex;

    move-result-object v2

    sget-object v3, Lcom/android/providers/contacts/ContactLocaleUtils;->LOCALE_THAI:Ljava/util/Locale;

    invoke-virtual {v2, v3}, Llibcore/icu/AlphabeticIndex;->addLabels(Ljava/util/Locale;)Llibcore/icu/AlphabeticIndex;

    move-result-object v2

    sget-object v3, Lcom/android/providers/contacts/ContactLocaleUtils;->LOCALE_ARABIC:Ljava/util/Locale;

    invoke-virtual {v2, v3}, Llibcore/icu/AlphabeticIndex;->addLabels(Ljava/util/Locale;)Llibcore/icu/AlphabeticIndex;

    move-result-object v2

    sget-object v3, Lcom/android/providers/contacts/ContactLocaleUtils;->LOCALE_HEBREW:Ljava/util/Locale;

    invoke-virtual {v2, v3}, Llibcore/icu/AlphabeticIndex;->addLabels(Ljava/util/Locale;)Llibcore/icu/AlphabeticIndex;

    move-result-object v2

    sget-object v3, Lcom/android/providers/contacts/ContactLocaleUtils;->LOCALE_GREEK:Ljava/util/Locale;

    invoke-virtual {v2, v3}, Llibcore/icu/AlphabeticIndex;->addLabels(Ljava/util/Locale;)Llibcore/icu/AlphabeticIndex;

    move-result-object v2

    sget-object v3, Lcom/android/providers/contacts/ContactLocaleUtils;->LOCALE_UKRAINIAN:Ljava/util/Locale;

    invoke-virtual {v2, v3}, Llibcore/icu/AlphabeticIndex;->addLabels(Ljava/util/Locale;)Llibcore/icu/AlphabeticIndex;

    move-result-object v2

    sget-object v3, Lcom/android/providers/contacts/ContactLocaleUtils;->LOCALE_HINDI:Ljava/util/Locale;

    invoke-virtual {v2, v3}, Llibcore/icu/AlphabeticIndex;->addLabels(Ljava/util/Locale;)Llibcore/icu/AlphabeticIndex;

    move-result-object v2

    invoke-virtual {v2, v4, v4}, Llibcore/icu/AlphabeticIndex;->addLabelRange(II)Llibcore/icu/AlphabeticIndex;

    move-result-object v2

    const/16 v3, 0xe40

    const/16 v4, 0xe45

    invoke-virtual {v2, v3, v4}, Llibcore/icu/AlphabeticIndex;->addLabelRange(II)Llibcore/icu/AlphabeticIndex;

    move-result-object v2

    const/16 v3, 0x621

    const/16 v4, 0x626

    invoke-virtual {v2, v3, v4}, Llibcore/icu/AlphabeticIndex;->addLabelRange(II)Llibcore/icu/AlphabeticIndex;

    move-result-object v2

    invoke-virtual {v2, v5, v5}, Llibcore/icu/AlphabeticIndex;->addLabelRange(II)Llibcore/icu/AlphabeticIndex;

    move-result-object v2

    invoke-virtual {v2, v6, v6}, Llibcore/icu/AlphabeticIndex;->addLabelRange(II)Llibcore/icu/AlphabeticIndex;

    move-result-object v2

    invoke-virtual {v2, v7, v7}, Llibcore/icu/AlphabeticIndex;->addLabelRange(II)Llibcore/icu/AlphabeticIndex;

    move-result-object v2

    invoke-virtual {v2, v8, v8}, Llibcore/icu/AlphabeticIndex;->addLabelRange(II)Llibcore/icu/AlphabeticIndex;

    move-result-object v2

    const/16 v3, 0x110d

    const/16 v4, 0x110d

    invoke-virtual {v2, v3, v4}, Llibcore/icu/AlphabeticIndex;->addLabelRange(II)Llibcore/icu/AlphabeticIndex;

    move-result-object v2

    const/16 v3, 0x1780

    const/16 v4, 0x17dd

    invoke-virtual {v2, v3, v4}, Llibcore/icu/AlphabeticIndex;->addLabelRange(II)Llibcore/icu/AlphabeticIndex;

    move-result-object v2

    const/16 v3, 0x17e0

    const/16 v4, 0x17e9

    invoke-virtual {v2, v3, v4}, Llibcore/icu/AlphabeticIndex;->addLabelRange(II)Llibcore/icu/AlphabeticIndex;

    move-result-object v2

    const/16 v3, 0x17f0

    const/16 v4, 0x17f9

    invoke-virtual {v2, v3, v4}, Llibcore/icu/AlphabeticIndex;->addLabelRange(II)Llibcore/icu/AlphabeticIndex;

    move-result-object v2

    const/16 v3, 0x3131

    const/16 v4, 0x3131

    invoke-virtual {v2, v3, v4}, Llibcore/icu/AlphabeticIndex;->addLabelRange(II)Llibcore/icu/AlphabeticIndex;

    move-result-object v2

    const/16 v3, 0x3134

    const/16 v4, 0x3134

    invoke-virtual {v2, v3, v4}, Llibcore/icu/AlphabeticIndex;->addLabelRange(II)Llibcore/icu/AlphabeticIndex;

    move-result-object v2

    const/16 v3, 0x3137

    const/16 v4, 0x3137

    invoke-virtual {v2, v3, v4}, Llibcore/icu/AlphabeticIndex;->addLabelRange(II)Llibcore/icu/AlphabeticIndex;

    move-result-object v2

    const/16 v3, 0x3139

    const/16 v4, 0x3139

    invoke-virtual {v2, v3, v4}, Llibcore/icu/AlphabeticIndex;->addLabelRange(II)Llibcore/icu/AlphabeticIndex;

    move-result-object v2

    const/16 v3, 0x3141

    const/16 v4, 0x3142

    invoke-virtual {v2, v3, v4}, Llibcore/icu/AlphabeticIndex;->addLabelRange(II)Llibcore/icu/AlphabeticIndex;

    move-result-object v2

    const/16 v3, 0x3145

    const/16 v4, 0x3145

    invoke-virtual {v2, v3, v4}, Llibcore/icu/AlphabeticIndex;->addLabelRange(II)Llibcore/icu/AlphabeticIndex;

    move-result-object v2

    const/16 v3, 0x3147

    const/16 v4, 0x3148

    invoke-virtual {v2, v3, v4}, Llibcore/icu/AlphabeticIndex;->addLabelRange(II)Llibcore/icu/AlphabeticIndex;

    move-result-object v2

    const/16 v3, 0x314a

    const/16 v4, 0x314e

    invoke-virtual {v2, v3, v4}, Llibcore/icu/AlphabeticIndex;->addLabelRange(II)Llibcore/icu/AlphabeticIndex;

    move-result-object v2

    sget-object v3, Lcom/android/providers/contacts/ContactLocaleUtils;->LOCALE_SERBIAN:Ljava/util/Locale;

    invoke-virtual {v2, v3}, Llibcore/icu/AlphabeticIndex;->addLabels(Ljava/util/Locale;)Llibcore/icu/AlphabeticIndex;

    move-result-object v2

    invoke-virtual {v2}, Llibcore/icu/AlphabeticIndex;->getImmutableIndex()Llibcore/icu/AlphabeticIndex$ImmutableIndex;

    move-result-object v2

    iput-object v2, p0, Lcom/android/providers/contacts/ContactLocaleUtils$ContactLocaleUtilsBase;->mAlphabeticIndex:Llibcore/icu/AlphabeticIndex$ImmutableIndex;

    .line 134
    iget-object v2, p0, Lcom/android/providers/contacts/ContactLocaleUtils$ContactLocaleUtilsBase;->mAlphabeticIndex:Llibcore/icu/AlphabeticIndex$ImmutableIndex;

    invoke-virtual {v2}, Llibcore/icu/AlphabeticIndex$ImmutableIndex;->getBucketCount()I

    move-result v2

    iput v2, p0, Lcom/android/providers/contacts/ContactLocaleUtils$ContactLocaleUtilsBase;->mAlphabeticIndexBucketCount:I

    .line 135
    iget v2, p0, Lcom/android/providers/contacts/ContactLocaleUtils$ContactLocaleUtilsBase;->mAlphabeticIndexBucketCount:I

    add-int/lit8 v2, v2, -0x1

    iput v2, p0, Lcom/android/providers/contacts/ContactLocaleUtils$ContactLocaleUtilsBase;->mNumberBucketIndex:I

    .line 136
    return-void
.end method


# virtual methods
.method public getBucketCount()I
    .locals 1

    .prologue
    .line 196
    iget v0, p0, Lcom/android/providers/contacts/ContactLocaleUtils$ContactLocaleUtilsBase;->mAlphabeticIndexBucketCount:I

    add-int/lit8 v0, v0, 0x1

    return v0
.end method

.method public getBucketIndex(Ljava/lang/String;)I
    .locals 6
    .param p1, "name"    # Ljava/lang/String;

    .prologue
    .line 152
    const/4 v4, 0x0

    .line 153
    .local v4, "prefixIsNumeric":Z
    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v2

    .line 154
    .local v2, "length":I
    const/4 v3, 0x0

    .line 155
    .local v3, "offset":I
    :goto_0
    if-ge v3, v2, :cond_0

    .line 156
    invoke-static {p1, v3}, Ljava/lang/Character;->codePointAt(Ljava/lang/CharSequence;I)I

    move-result v1

    .line 159
    .local v1, "codePoint":I
    invoke-static {v1}, Ljava/lang/Character;->isDigit(I)Z

    move-result v5

    if-eqz v5, :cond_2

    .line 160
    const/4 v4, 0x1

    .line 170
    .end local v1    # "codePoint":I
    :cond_0
    if-eqz v4, :cond_4

    .line 171
    iget v0, p0, Lcom/android/providers/contacts/ContactLocaleUtils$ContactLocaleUtilsBase;->mNumberBucketIndex:I

    .line 188
    :cond_1
    :goto_1
    return v0

    .line 162
    .restart local v1    # "codePoint":I
    :cond_2
    invoke-static {v1}, Ljava/lang/Character;->isSpaceChar(I)Z

    move-result v5

    if-nez v5, :cond_3

    const/16 v5, 0x2b

    if-eq v1, v5, :cond_3

    const/16 v5, 0x28

    if-eq v1, v5, :cond_3

    const/16 v5, 0x29

    if-eq v1, v5, :cond_3

    const/16 v5, 0x2e

    if-eq v1, v5, :cond_3

    const/16 v5, 0x2d

    if-eq v1, v5, :cond_3

    const/16 v5, 0x23

    if-ne v1, v5, :cond_0

    .line 168
    :cond_3
    invoke-static {v1}, Ljava/lang/Character;->charCount(I)I

    move-result v5

    add-int/2addr v3, v5

    .line 169
    goto :goto_0

    .line 178
    .end local v1    # "codePoint":I
    :cond_4
    iget-boolean v5, p0, Lcom/android/providers/contacts/ContactLocaleUtils$ContactLocaleUtilsBase;->mEnableSecondaryLocalePinyin:Z

    if-eqz v5, :cond_5

    .line 179
    invoke-static {}, Lcom/android/providers/contacts/HanziToPinyin;->getInstance()Lcom/android/providers/contacts/HanziToPinyin;

    move-result-object v5

    invoke-virtual {v5, p1}, Lcom/android/providers/contacts/HanziToPinyin;->transliterate(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    .line 181
    :cond_5
    iget-object v5, p0, Lcom/android/providers/contacts/ContactLocaleUtils$ContactLocaleUtilsBase;->mAlphabeticIndex:Llibcore/icu/AlphabeticIndex$ImmutableIndex;

    invoke-virtual {v5, p1}, Llibcore/icu/AlphabeticIndex$ImmutableIndex;->getBucketIndex(Ljava/lang/String;)I

    move-result v0

    .line 182
    .local v0, "bucket":I
    if-gez v0, :cond_6

    .line 183
    const/4 v0, -0x1

    goto :goto_1

    .line 185
    :cond_6
    iget v5, p0, Lcom/android/providers/contacts/ContactLocaleUtils$ContactLocaleUtilsBase;->mNumberBucketIndex:I

    if-lt v0, v5, :cond_1

    .line 186
    add-int/lit8 v0, v0, 0x1

    goto :goto_1
.end method

.method public getBucketLabel(I)Ljava/lang/String;
    .locals 1
    .param p1, "bucketIndex"    # I

    .prologue
    .line 205
    if-ltz p1, :cond_0

    invoke-virtual {p0}, Lcom/android/providers/contacts/ContactLocaleUtils$ContactLocaleUtilsBase;->getBucketCount()I

    move-result v0

    if-lt p1, v0, :cond_1

    .line 206
    :cond_0
    const-string v0, ""

    .line 212
    :goto_0
    return-object v0

    .line 207
    :cond_1
    iget v0, p0, Lcom/android/providers/contacts/ContactLocaleUtils$ContactLocaleUtilsBase;->mNumberBucketIndex:I

    if-ne p1, v0, :cond_2

    .line 208
    const-string v0, "#"

    goto :goto_0

    .line 209
    :cond_2
    iget v0, p0, Lcom/android/providers/contacts/ContactLocaleUtils$ContactLocaleUtilsBase;->mNumberBucketIndex:I

    if-le p1, v0, :cond_3

    .line 210
    add-int/lit8 p1, p1, -0x1

    .line 212
    :cond_3
    iget-object v0, p0, Lcom/android/providers/contacts/ContactLocaleUtils$ContactLocaleUtilsBase;->mAlphabeticIndex:Llibcore/icu/AlphabeticIndex$ImmutableIndex;

    invoke-virtual {v0, p1}, Llibcore/icu/AlphabeticIndex$ImmutableIndex;->getBucketLabel(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public getLabels()Ljava/util/ArrayList;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 221
    invoke-virtual {p0}, Lcom/android/providers/contacts/ContactLocaleUtils$ContactLocaleUtilsBase;->getBucketCount()I

    move-result v0

    .line 222
    .local v0, "bucketCount":I
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2, v0}, Ljava/util/ArrayList;-><init>(I)V

    .line 223
    .local v2, "labels":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    if-ge v1, v0, :cond_0

    .line 224
    invoke-virtual {p0, v1}, Lcom/android/providers/contacts/ContactLocaleUtils$ContactLocaleUtilsBase;->getBucketLabel(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 223
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 226
    :cond_0
    return-object v2
.end method

.method public getNameLookupKeys(Ljava/lang/String;I)Ljava/util/Iterator;
    .locals 1
    .param p1, "name"    # Ljava/lang/String;
    .param p2, "nameStyle"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "I)",
            "Ljava/util/Iterator",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 217
    const/4 v0, 0x0

    return-object v0
.end method

.method public getSortKey(Ljava/lang/String;)Ljava/lang/String;
    .locals 0
    .param p1, "name"    # Ljava/lang/String;

    .prologue
    .line 139
    return-object p1
.end method
