.class public Lcom/android/providers/contacts/PostalSplitter;
.super Ljava/lang/Object;
.source "PostalSplitter.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/android/providers/contacts/PostalSplitter$Postal;
    }
.end annotation


# static fields
.field private static final JAPANESE_LANGUAGE:Ljava/lang/String;


# instance fields
.field private final mLocale:Ljava/util/Locale;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 28
    sget-object v0, Ljava/util/Locale;->JAPANESE:Ljava/util/Locale;

    invoke-virtual {v0}, Ljava/util/Locale;->getLanguage()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/android/providers/contacts/PostalSplitter;->JAPANESE_LANGUAGE:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Ljava/util/Locale;)V
    .locals 0
    .param p1, "locale"    # Ljava/util/Locale;

    .prologue
    .line 62
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 63
    iput-object p1, p0, Lcom/android/providers/contacts/PostalSplitter;->mLocale:Ljava/util/Locale;

    .line 64
    return-void
.end method

.method private static arePrintableAsciiOnly([Ljava/lang/String;)Z
    .locals 6
    .param p0, "values"    # [Ljava/lang/String;

    .prologue
    const/4 v4, 0x1

    .line 242
    if-nez p0, :cond_1

    .line 253
    :cond_0
    :goto_0
    return v4

    .line 245
    :cond_1
    move-object v0, p0

    .local v0, "arr$":[Ljava/lang/String;
    array-length v2, v0

    .local v2, "len$":I
    const/4 v1, 0x0

    .local v1, "i$":I
    :goto_1
    if-ge v1, v2, :cond_0

    aget-object v3, v0, v1

    .line 246
    .local v3, "value":Ljava/lang/String;
    invoke-static {v3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v5

    if-eqz v5, :cond_3

    .line 245
    :cond_2
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 249
    :cond_3
    invoke-static {v3}, Landroid/text/TextUtils;->isPrintableAsciiOnly(Ljava/lang/CharSequence;)Z

    move-result v5

    if-nez v5, :cond_2

    .line 250
    const/4 v4, 0x0

    goto :goto_0
.end method

.method private joinEnUs(Lcom/android/providers/contacts/PostalSplitter$Postal;)Ljava/lang/String;
    .locals 13
    .param p1, "postal"    # Lcom/android/providers/contacts/PostalSplitter$Postal;

    .prologue
    .line 167
    iget-object v12, p1, Lcom/android/providers/contacts/PostalSplitter$Postal;->street:Ljava/lang/String;

    invoke-static {v12}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v12

    if-nez v12, :cond_13

    const/4 v10, 0x1

    .line 168
    .local v10, "hasStreet":Z
    :goto_0
    iget-object v12, p1, Lcom/android/providers/contacts/PostalSplitter$Postal;->pobox:Ljava/lang/String;

    invoke-static {v12}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v12

    if-nez v12, :cond_14

    const/4 v6, 0x1

    .line 169
    .local v6, "hasPobox":Z
    :goto_1
    iget-object v12, p1, Lcom/android/providers/contacts/PostalSplitter$Postal;->neighborhood:Ljava/lang/String;

    invoke-static {v12}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v12

    if-nez v12, :cond_15

    const/4 v5, 0x1

    .line 170
    .local v5, "hasNeighborhood":Z
    :goto_2
    iget-object v12, p1, Lcom/android/providers/contacts/PostalSplitter$Postal;->city:Ljava/lang/String;

    invoke-static {v12}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v12

    if-nez v12, :cond_16

    const/4 v2, 0x1

    .line 171
    .local v2, "hasCity":Z
    :goto_3
    iget-object v12, p1, Lcom/android/providers/contacts/PostalSplitter$Postal;->region:Ljava/lang/String;

    invoke-static {v12}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v12

    if-nez v12, :cond_17

    const/4 v8, 0x1

    .line 172
    .local v8, "hasRegion":Z
    :goto_4
    iget-object v12, p1, Lcom/android/providers/contacts/PostalSplitter$Postal;->postcode:Ljava/lang/String;

    invoke-static {v12}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v12

    if-nez v12, :cond_18

    const/4 v7, 0x1

    .line 173
    .local v7, "hasPostcode":Z
    :goto_5
    iget-object v12, p1, Lcom/android/providers/contacts/PostalSplitter$Postal;->country:Ljava/lang/String;

    invoke-static {v12}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v12

    if-nez v12, :cond_19

    const/4 v3, 0x1

    .line 179
    .local v3, "hasCountry":Z
    :goto_6
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 181
    .local v0, "builder":Ljava/lang/StringBuilder;
    if-nez v10, :cond_0

    if-nez v6, :cond_0

    if-eqz v5, :cond_1a

    :cond_0
    const/4 v4, 0x1

    .line 182
    .local v4, "hasFirstBlock":Z
    :goto_7
    if-nez v2, :cond_1

    if-nez v8, :cond_1

    if-eqz v7, :cond_1b

    :cond_1
    const/4 v9, 0x1

    .line 183
    .local v9, "hasSecondBlock":Z
    :goto_8
    move v11, v3

    .line 185
    .local v11, "hasThirdBlock":Z
    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v12

    invoke-virtual {v12}, Ljava/util/Locale;->getLanguage()Ljava/lang/String;

    move-result-object v1

    .line 187
    .local v1, "currentLang":Ljava/lang/String;
    if-eqz v4, :cond_7

    .line 188
    if-eqz v10, :cond_2

    .line 189
    iget-object v12, p1, Lcom/android/providers/contacts/PostalSplitter$Postal;->street:Ljava/lang/String;

    invoke-virtual {v0, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 191
    :cond_2
    if-eqz v6, :cond_4

    .line 192
    if-eqz v10, :cond_3

    const-string v12, "\n"

    invoke-virtual {v0, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 193
    :cond_3
    iget-object v12, p1, Lcom/android/providers/contacts/PostalSplitter$Postal;->pobox:Ljava/lang/String;

    invoke-virtual {v0, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 195
    :cond_4
    if-eqz v5, :cond_7

    .line 196
    if-nez v10, :cond_5

    if-eqz v6, :cond_6

    :cond_5
    const-string v12, "\n"

    invoke-virtual {v0, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 197
    :cond_6
    iget-object v12, p1, Lcom/android/providers/contacts/PostalSplitter$Postal;->neighborhood:Ljava/lang/String;

    invoke-virtual {v0, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 201
    :cond_7
    if-eqz v9, :cond_f

    .line 202
    if-eqz v4, :cond_8

    .line 203
    const-string v12, "\n"

    invoke-virtual {v0, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 205
    :cond_8
    if-eqz v2, :cond_9

    .line 206
    iget-object v12, p1, Lcom/android/providers/contacts/PostalSplitter$Postal;->city:Ljava/lang/String;

    invoke-virtual {v0, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 208
    :cond_9
    if-eqz v8, :cond_c

    .line 209
    if-eqz v2, :cond_b

    .line 210
    const-string v12, "ar"

    invoke-virtual {v1, v12}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v12

    if-nez v12, :cond_a

    const-string v12, "fa"

    invoke-virtual {v1, v12}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v12

    if-nez v12, :cond_a

    const-string v12, "ur"

    invoke-virtual {v1, v12}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v12

    if-eqz v12, :cond_1c

    .line 212
    :cond_a
    const-string v12, "\u060c "

    invoke-virtual {v0, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 217
    :cond_b
    :goto_9
    iget-object v12, p1, Lcom/android/providers/contacts/PostalSplitter$Postal;->region:Ljava/lang/String;

    invoke-virtual {v0, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 219
    :cond_c
    if-eqz v7, :cond_f

    .line 220
    if-nez v2, :cond_d

    if-eqz v8, :cond_e

    :cond_d
    const-string v12, " "

    invoke-virtual {v0, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 221
    :cond_e
    iget-object v12, p1, Lcom/android/providers/contacts/PostalSplitter$Postal;->postcode:Ljava/lang/String;

    invoke-virtual {v0, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 225
    :cond_f
    if-eqz v11, :cond_12

    .line 226
    if-nez v4, :cond_10

    if-eqz v9, :cond_11

    .line 227
    :cond_10
    const-string v12, "\n"

    invoke-virtual {v0, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 229
    :cond_11
    if-eqz v3, :cond_12

    .line 230
    iget-object v12, p1, Lcom/android/providers/contacts/PostalSplitter$Postal;->country:Ljava/lang/String;

    invoke-virtual {v0, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 234
    :cond_12
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->length()I

    move-result v12

    if-lez v12, :cond_1d

    .line 235
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v12

    .line 237
    :goto_a
    return-object v12

    .line 167
    .end local v0    # "builder":Ljava/lang/StringBuilder;
    .end local v1    # "currentLang":Ljava/lang/String;
    .end local v2    # "hasCity":Z
    .end local v3    # "hasCountry":Z
    .end local v4    # "hasFirstBlock":Z
    .end local v5    # "hasNeighborhood":Z
    .end local v6    # "hasPobox":Z
    .end local v7    # "hasPostcode":Z
    .end local v8    # "hasRegion":Z
    .end local v9    # "hasSecondBlock":Z
    .end local v10    # "hasStreet":Z
    .end local v11    # "hasThirdBlock":Z
    :cond_13
    const/4 v10, 0x0

    goto/16 :goto_0

    .line 168
    .restart local v10    # "hasStreet":Z
    :cond_14
    const/4 v6, 0x0

    goto/16 :goto_1

    .line 169
    .restart local v6    # "hasPobox":Z
    :cond_15
    const/4 v5, 0x0

    goto/16 :goto_2

    .line 170
    .restart local v5    # "hasNeighborhood":Z
    :cond_16
    const/4 v2, 0x0

    goto/16 :goto_3

    .line 171
    .restart local v2    # "hasCity":Z
    :cond_17
    const/4 v8, 0x0

    goto/16 :goto_4

    .line 172
    .restart local v8    # "hasRegion":Z
    :cond_18
    const/4 v7, 0x0

    goto/16 :goto_5

    .line 173
    .restart local v7    # "hasPostcode":Z
    :cond_19
    const/4 v3, 0x0

    goto/16 :goto_6

    .line 181
    .restart local v0    # "builder":Ljava/lang/StringBuilder;
    .restart local v3    # "hasCountry":Z
    :cond_1a
    const/4 v4, 0x0

    goto/16 :goto_7

    .line 182
    .restart local v4    # "hasFirstBlock":Z
    :cond_1b
    const/4 v9, 0x0

    goto/16 :goto_8

    .line 214
    .restart local v1    # "currentLang":Ljava/lang/String;
    .restart local v9    # "hasSecondBlock":Z
    .restart local v11    # "hasThirdBlock":Z
    :cond_1c
    const-string v12, ", "

    invoke-virtual {v0, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_9

    .line 237
    :cond_1d
    const/4 v12, 0x0

    goto :goto_a
.end method

.method private joinJaJp(Lcom/android/providers/contacts/PostalSplitter$Postal;)Ljava/lang/String;
    .locals 14
    .param p1, "postal"    # Lcom/android/providers/contacts/PostalSplitter$Postal;

    .prologue
    const/4 v11, 0x1

    const/4 v12, 0x0

    .line 101
    iget-object v13, p1, Lcom/android/providers/contacts/PostalSplitter$Postal;->street:Ljava/lang/String;

    invoke-static {v13}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v13

    if-nez v13, :cond_12

    move v9, v11

    .line 102
    .local v9, "hasStreet":Z
    :goto_0
    iget-object v13, p1, Lcom/android/providers/contacts/PostalSplitter$Postal;->pobox:Ljava/lang/String;

    invoke-static {v13}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v13

    if-nez v13, :cond_13

    move v5, v11

    .line 103
    .local v5, "hasPobox":Z
    :goto_1
    iget-object v13, p1, Lcom/android/providers/contacts/PostalSplitter$Postal;->neighborhood:Ljava/lang/String;

    invoke-static {v13}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v13

    if-nez v13, :cond_14

    move v4, v11

    .line 104
    .local v4, "hasNeighborhood":Z
    :goto_2
    iget-object v13, p1, Lcom/android/providers/contacts/PostalSplitter$Postal;->city:Ljava/lang/String;

    invoke-static {v13}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v13

    if-nez v13, :cond_15

    move v1, v11

    .line 105
    .local v1, "hasCity":Z
    :goto_3
    iget-object v13, p1, Lcom/android/providers/contacts/PostalSplitter$Postal;->region:Ljava/lang/String;

    invoke-static {v13}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v13

    if-nez v13, :cond_16

    move v7, v11

    .line 106
    .local v7, "hasRegion":Z
    :goto_4
    iget-object v13, p1, Lcom/android/providers/contacts/PostalSplitter$Postal;->postcode:Ljava/lang/String;

    invoke-static {v13}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v13

    if-nez v13, :cond_17

    move v6, v11

    .line 107
    .local v6, "hasPostcode":Z
    :goto_5
    iget-object v13, p1, Lcom/android/providers/contacts/PostalSplitter$Postal;->country:Ljava/lang/String;

    invoke-static {v13}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v13

    if-nez v13, :cond_18

    move v2, v11

    .line 113
    .local v2, "hasCountry":Z
    :goto_6
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 115
    .local v0, "builder":Ljava/lang/StringBuilder;
    if-nez v2, :cond_0

    if-eqz v6, :cond_19

    :cond_0
    move v3, v11

    .line 116
    .local v3, "hasFirstBlock":Z
    :goto_7
    if-nez v7, :cond_1

    if-nez v1, :cond_1

    if-eqz v4, :cond_1a

    :cond_1
    move v8, v11

    .line 117
    .local v8, "hasSecondBlock":Z
    :goto_8
    if-nez v9, :cond_2

    if-eqz v5, :cond_1b

    :cond_2
    move v10, v11

    .line 119
    .local v10, "hasThirdBlock":Z
    :goto_9
    if-eqz v3, :cond_5

    .line 120
    if-eqz v2, :cond_3

    .line 121
    iget-object v11, p1, Lcom/android/providers/contacts/PostalSplitter$Postal;->country:Ljava/lang/String;

    invoke-virtual {v0, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 123
    :cond_3
    if-eqz v6, :cond_5

    .line 124
    if-eqz v2, :cond_4

    const-string v11, " "

    invoke-virtual {v0, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 125
    :cond_4
    iget-object v11, p1, Lcom/android/providers/contacts/PostalSplitter$Postal;->postcode:Ljava/lang/String;

    invoke-virtual {v0, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 129
    :cond_5
    if-eqz v8, :cond_c

    .line 130
    if-eqz v3, :cond_6

    .line 131
    const-string v11, "\n"

    invoke-virtual {v0, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 133
    :cond_6
    if-eqz v7, :cond_7

    .line 134
    iget-object v11, p1, Lcom/android/providers/contacts/PostalSplitter$Postal;->region:Ljava/lang/String;

    invoke-virtual {v0, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 136
    :cond_7
    if-eqz v1, :cond_9

    .line 137
    if-eqz v7, :cond_8

    const-string v11, " "

    invoke-virtual {v0, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 138
    :cond_8
    iget-object v11, p1, Lcom/android/providers/contacts/PostalSplitter$Postal;->city:Ljava/lang/String;

    invoke-virtual {v0, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 140
    :cond_9
    if-eqz v4, :cond_c

    .line 141
    if-nez v7, :cond_a

    if-eqz v1, :cond_b

    :cond_a
    const-string v11, " "

    invoke-virtual {v0, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 142
    :cond_b
    iget-object v11, p1, Lcom/android/providers/contacts/PostalSplitter$Postal;->neighborhood:Ljava/lang/String;

    invoke-virtual {v0, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 146
    :cond_c
    if-eqz v10, :cond_11

    .line 147
    if-nez v3, :cond_d

    if-eqz v8, :cond_e

    .line 148
    :cond_d
    const-string v11, "\n"

    invoke-virtual {v0, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 150
    :cond_e
    if-eqz v9, :cond_f

    .line 151
    iget-object v11, p1, Lcom/android/providers/contacts/PostalSplitter$Postal;->street:Ljava/lang/String;

    invoke-virtual {v0, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 153
    :cond_f
    if-eqz v5, :cond_11

    .line 154
    if-eqz v9, :cond_10

    const-string v11, " "

    invoke-virtual {v0, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 155
    :cond_10
    iget-object v11, p1, Lcom/android/providers/contacts/PostalSplitter$Postal;->pobox:Ljava/lang/String;

    invoke-virtual {v0, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 159
    :cond_11
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->length()I

    move-result v11

    if-lez v11, :cond_1c

    .line 160
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    .line 162
    :goto_a
    return-object v11

    .end local v0    # "builder":Ljava/lang/StringBuilder;
    .end local v1    # "hasCity":Z
    .end local v2    # "hasCountry":Z
    .end local v3    # "hasFirstBlock":Z
    .end local v4    # "hasNeighborhood":Z
    .end local v5    # "hasPobox":Z
    .end local v6    # "hasPostcode":Z
    .end local v7    # "hasRegion":Z
    .end local v8    # "hasSecondBlock":Z
    .end local v9    # "hasStreet":Z
    .end local v10    # "hasThirdBlock":Z
    :cond_12
    move v9, v12

    .line 101
    goto/16 :goto_0

    .restart local v9    # "hasStreet":Z
    :cond_13
    move v5, v12

    .line 102
    goto/16 :goto_1

    .restart local v5    # "hasPobox":Z
    :cond_14
    move v4, v12

    .line 103
    goto/16 :goto_2

    .restart local v4    # "hasNeighborhood":Z
    :cond_15
    move v1, v12

    .line 104
    goto/16 :goto_3

    .restart local v1    # "hasCity":Z
    :cond_16
    move v7, v12

    .line 105
    goto/16 :goto_4

    .restart local v7    # "hasRegion":Z
    :cond_17
    move v6, v12

    .line 106
    goto/16 :goto_5

    .restart local v6    # "hasPostcode":Z
    :cond_18
    move v2, v12

    .line 107
    goto/16 :goto_6

    .restart local v0    # "builder":Ljava/lang/StringBuilder;
    .restart local v2    # "hasCountry":Z
    :cond_19
    move v3, v12

    .line 115
    goto/16 :goto_7

    .restart local v3    # "hasFirstBlock":Z
    :cond_1a
    move v8, v12

    .line 116
    goto/16 :goto_8

    .restart local v8    # "hasSecondBlock":Z
    :cond_1b
    move v10, v12

    .line 117
    goto/16 :goto_9

    .line 162
    .restart local v10    # "hasThirdBlock":Z
    :cond_1c
    const/4 v11, 0x0

    goto :goto_a
.end method


# virtual methods
.method public join(Lcom/android/providers/contacts/PostalSplitter$Postal;)Ljava/lang/String;
    .locals 3
    .param p1, "postal"    # Lcom/android/providers/contacts/PostalSplitter$Postal;

    .prologue
    .line 86
    const/4 v1, 0x7

    new-array v0, v1, [Ljava/lang/String;

    const/4 v1, 0x0

    iget-object v2, p1, Lcom/android/providers/contacts/PostalSplitter$Postal;->street:Ljava/lang/String;

    aput-object v2, v0, v1

    const/4 v1, 0x1

    iget-object v2, p1, Lcom/android/providers/contacts/PostalSplitter$Postal;->pobox:Ljava/lang/String;

    aput-object v2, v0, v1

    const/4 v1, 0x2

    iget-object v2, p1, Lcom/android/providers/contacts/PostalSplitter$Postal;->neighborhood:Ljava/lang/String;

    aput-object v2, v0, v1

    const/4 v1, 0x3

    iget-object v2, p1, Lcom/android/providers/contacts/PostalSplitter$Postal;->city:Ljava/lang/String;

    aput-object v2, v0, v1

    const/4 v1, 0x4

    iget-object v2, p1, Lcom/android/providers/contacts/PostalSplitter$Postal;->region:Ljava/lang/String;

    aput-object v2, v0, v1

    const/4 v1, 0x5

    iget-object v2, p1, Lcom/android/providers/contacts/PostalSplitter$Postal;->postcode:Ljava/lang/String;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    iget-object v2, p1, Lcom/android/providers/contacts/PostalSplitter$Postal;->country:Ljava/lang/String;

    aput-object v2, v0, v1

    .line 91
    .local v0, "values":[Ljava/lang/String;
    iget-object v1, p0, Lcom/android/providers/contacts/PostalSplitter;->mLocale:Ljava/util/Locale;

    if-eqz v1, :cond_0

    sget-object v1, Lcom/android/providers/contacts/PostalSplitter;->JAPANESE_LANGUAGE:Ljava/lang/String;

    iget-object v2, p0, Lcom/android/providers/contacts/PostalSplitter;->mLocale:Ljava/util/Locale;

    invoke-virtual {v2}, Ljava/util/Locale;->getLanguage()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-static {v0}, Lcom/android/providers/contacts/PostalSplitter;->arePrintableAsciiOnly([Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 94
    invoke-direct {p0, p1}, Lcom/android/providers/contacts/PostalSplitter;->joinJaJp(Lcom/android/providers/contacts/PostalSplitter$Postal;)Ljava/lang/String;

    move-result-object v1

    .line 96
    :goto_0
    return-object v1

    :cond_0
    invoke-direct {p0, p1}, Lcom/android/providers/contacts/PostalSplitter;->joinEnUs(Lcom/android/providers/contacts/PostalSplitter$Postal;)Ljava/lang/String;

    move-result-object v1

    goto :goto_0
.end method

.method public split(Lcom/android/providers/contacts/PostalSplitter$Postal;Ljava/lang/String;)V
    .locals 1
    .param p1, "postal"    # Lcom/android/providers/contacts/PostalSplitter$Postal;
    .param p2, "formattedAddress"    # Ljava/lang/String;

    .prologue
    .line 71
    invoke-static {p2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 72
    iput-object p2, p1, Lcom/android/providers/contacts/PostalSplitter$Postal;->street:Ljava/lang/String;

    .line 74
    :cond_0
    return-void
.end method
