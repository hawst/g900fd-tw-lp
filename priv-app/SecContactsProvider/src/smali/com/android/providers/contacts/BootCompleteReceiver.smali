.class public Lcom/android/providers/contacts/BootCompleteReceiver;
.super Landroid/content/BroadcastReceiver;
.source "BootCompleteReceiver.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 13
    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    return-void
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 4
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "intent"    # Landroid/content/Intent;

    .prologue
    .line 19
    invoke-static {}, Lcom/android/providers/contacts/util/LoadCscFeatureUtils;->getInstance()Lcom/android/providers/contacts/util/LoadCscFeatureUtils;

    move-result-object v2

    invoke-virtual {v2}, Lcom/android/providers/contacts/util/LoadCscFeatureUtils;->getEnableSupportAAB()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 20
    invoke-static {p1}, Landroid/preference/PreferenceManager;->getDefaultSharedPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v1

    .line 21
    .local v1, "prefs":Landroid/content/SharedPreferences;
    if-eqz v1, :cond_0

    const-string v2, "setCscContactsAlready"

    const/4 v3, 0x0

    invoke-interface {v1, v2, v3}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v2

    if-nez v2, :cond_0

    .line 22
    new-instance v0, Lcom/android/providers/contacts/CscContacts;

    invoke-direct {v0, p1}, Lcom/android/providers/contacts/CscContacts;-><init>(Landroid/content/Context;)V

    .line 23
    .local v0, "CscContacts":Lcom/android/providers/contacts/CscContacts;
    invoke-virtual {v0}, Lcom/android/providers/contacts/CscContacts;->setCscContacts()V

    .line 26
    .end local v0    # "CscContacts":Lcom/android/providers/contacts/CscContacts;
    .end local v1    # "prefs":Landroid/content/SharedPreferences;
    :cond_0
    return-void
.end method
