.class Lcom/android/providers/contacts/ContactLocaleUtils$TraditionalChineseHKContactUtils;
.super Lcom/android/providers/contacts/ContactLocaleUtils$ContactLocaleUtilsBase;
.source "ContactLocaleUtils.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/providers/contacts/ContactLocaleUtils;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "TraditionalChineseHKContactUtils"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/providers/contacts/ContactLocaleUtils;


# direct methods
.method public constructor <init>(Lcom/android/providers/contacts/ContactLocaleUtils;Lcom/android/providers/contacts/LocaleSet;)V
    .locals 0
    .param p2, "locale"    # Lcom/android/providers/contacts/LocaleSet;

    .prologue
    .line 467
    iput-object p1, p0, Lcom/android/providers/contacts/ContactLocaleUtils$TraditionalChineseHKContactUtils;->this$0:Lcom/android/providers/contacts/ContactLocaleUtils;

    .line 468
    invoke-direct {p0, p2}, Lcom/android/providers/contacts/ContactLocaleUtils$ContactLocaleUtilsBase;-><init>(Lcom/android/providers/contacts/LocaleSet;)V

    .line 469
    return-void
.end method


# virtual methods
.method public getNameLookupKeys(Ljava/lang/String;I)Ljava/util/Iterator;
    .locals 1
    .param p1, "name"    # Ljava/lang/String;
    .param p2, "nameStyle"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "I)",
            "Ljava/util/Iterator",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 526
    const/4 v0, 0x4

    if-eq p2, v0, :cond_0

    const/4 v0, 0x5

    if-eq p2, v0, :cond_0

    .line 528
    invoke-virtual {p0, p1}, Lcom/android/providers/contacts/ContactLocaleUtils$TraditionalChineseHKContactUtils;->getStrokeNameLookupKeys(Ljava/lang/String;)Ljava/util/Iterator;

    move-result-object v0

    .line 530
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getSortKey(Ljava/lang/String;)Ljava/lang/String;
    .locals 10
    .param p1, "name"    # Ljava/lang/String;

    .prologue
    const/4 v9, 0x2

    const/16 v8, 0x20

    .line 474
    iget-object v6, p0, Lcom/android/providers/contacts/ContactLocaleUtils$TraditionalChineseHKContactUtils;->this$0:Lcom/android/providers/contacts/ContactLocaleUtils;

    # getter for: Lcom/android/providers/contacts/ContactLocaleUtils;->mLocales:Lcom/android/providers/contacts/LocaleSet;
    invoke-static {v6}, Lcom/android/providers/contacts/ContactLocaleUtils;->access$000(Lcom/android/providers/contacts/ContactLocaleUtils;)Lcom/android/providers/contacts/LocaleSet;

    move-result-object v6

    invoke-virtual {v6}, Lcom/android/providers/contacts/LocaleSet;->toString()Ljava/lang/String;

    move-result-object v6

    const-string v7, "zh_CN"

    invoke-virtual {v6, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_5

    .line 475
    invoke-static {}, Lcom/android/providers/contacts/HanziToPinyin;->getInstance()Lcom/android/providers/contacts/HanziToPinyin;

    move-result-object v6

    invoke-virtual {v6, p1}, Lcom/android/providers/contacts/HanziToPinyin;->getTokens(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v4

    .line 476
    .local v4, "tokens":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/android/providers/contacts/HanziToPinyin$Token;>;"
    if-eqz v4, :cond_4

    invoke-virtual {v4}, Ljava/util/ArrayList;->size()I

    move-result v6

    if-lez v6, :cond_4

    .line 477
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    .line 478
    .local v1, "sb":Ljava/lang/StringBuilder;
    invoke-virtual {v4}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v6

    if-eqz v6, :cond_3

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/android/providers/contacts/HanziToPinyin$Token;

    .line 481
    .local v3, "token":Lcom/android/providers/contacts/HanziToPinyin$Token;
    iget v6, v3, Lcom/android/providers/contacts/HanziToPinyin$Token;->type:I

    if-ne v9, v6, :cond_1

    .line 482
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->length()I

    move-result v6

    if-lez v6, :cond_0

    .line 483
    invoke-virtual {v1, v8}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 485
    :cond_0
    iget-object v6, v3, Lcom/android/providers/contacts/HanziToPinyin$Token;->target:Ljava/lang/String;

    invoke-virtual {v1, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 486
    invoke-virtual {v1, v8}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 487
    iget-object v6, v3, Lcom/android/providers/contacts/HanziToPinyin$Token;->source:Ljava/lang/String;

    invoke-virtual {v1, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_0

    .line 489
    :cond_1
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->length()I

    move-result v6

    if-lez v6, :cond_2

    .line 490
    invoke-virtual {v1, v8}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 492
    :cond_2
    iget-object v6, v3, Lcom/android/providers/contacts/HanziToPinyin$Token;->source:Ljava/lang/String;

    invoke-virtual {v1, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_0

    .line 495
    .end local v3    # "token":Lcom/android/providers/contacts/HanziToPinyin$Token;
    :cond_3
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    .line 521
    .end local v0    # "i$":Ljava/util/Iterator;
    .end local v1    # "sb":Ljava/lang/StringBuilder;
    .end local v4    # "tokens":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/android/providers/contacts/HanziToPinyin$Token;>;"
    :goto_1
    return-object v6

    .line 497
    .restart local v4    # "tokens":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/android/providers/contacts/HanziToPinyin$Token;>;"
    :cond_4
    invoke-super {p0, p1}, Lcom/android/providers/contacts/ContactLocaleUtils$ContactLocaleUtilsBase;->getSortKey(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    goto :goto_1

    .line 498
    .end local v4    # "tokens":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/android/providers/contacts/HanziToPinyin$Token;>;"
    :cond_5
    iget-object v6, p0, Lcom/android/providers/contacts/ContactLocaleUtils$TraditionalChineseHKContactUtils;->this$0:Lcom/android/providers/contacts/ContactLocaleUtils;

    # getter for: Lcom/android/providers/contacts/ContactLocaleUtils;->mLocales:Lcom/android/providers/contacts/LocaleSet;
    invoke-static {v6}, Lcom/android/providers/contacts/ContactLocaleUtils;->access$000(Lcom/android/providers/contacts/ContactLocaleUtils;)Lcom/android/providers/contacts/LocaleSet;

    move-result-object v6

    invoke-virtual {v6}, Lcom/android/providers/contacts/LocaleSet;->toString()Ljava/lang/String;

    move-result-object v6

    const-string v7, "zh_HK"

    invoke-virtual {v6, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_a

    .line 499
    move-object v2, p1

    .line 500
    .local v2, "tmpDisplayName":Ljava/lang/String;
    if-eqz p1, :cond_6

    .line 501
    const/4 v6, 0x0

    const/4 v7, 0x1

    invoke-virtual {p1, v6, v7}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v2

    .line 503
    :cond_6
    invoke-static {}, Lcom/android/providers/contacts/HanziToStroke;->getIntance()Lcom/android/providers/contacts/HanziToStroke;

    move-result-object v6

    invoke-virtual {v6, v2}, Lcom/android/providers/contacts/HanziToStroke;->getNew(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v5

    .line 504
    .local v5, "tokens":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/android/providers/contacts/HanziToStroke$Token;>;"
    if-eqz v5, :cond_9

    invoke-virtual {v5}, Ljava/util/ArrayList;->size()I

    move-result v6

    if-lez v6, :cond_9

    .line 505
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    .line 506
    .restart local v1    # "sb":Ljava/lang/StringBuilder;
    invoke-virtual {v5}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .restart local v0    # "i$":Ljava/util/Iterator;
    :goto_2
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v6

    if-eqz v6, :cond_8

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/android/providers/contacts/HanziToStroke$Token;

    .line 507
    .local v3, "token":Lcom/android/providers/contacts/HanziToStroke$Token;
    iget v6, v3, Lcom/android/providers/contacts/HanziToStroke$Token;->type:I

    if-ne v9, v6, :cond_7

    .line 508
    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 509
    invoke-virtual {v1, v8}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 510
    const/16 v6, 0xa4

    invoke-virtual {v1, v6}, Ljava/lang/StringBuilder;->appendCodePoint(I)Ljava/lang/StringBuilder;

    .line 511
    iget-object v6, v3, Lcom/android/providers/contacts/HanziToStroke$Token;->source:Ljava/lang/String;

    invoke-virtual {v1, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 512
    iget-object v6, v3, Lcom/android/providers/contacts/HanziToStroke$Token;->target:Ljava/lang/String;

    invoke-virtual {v1, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_2

    .line 514
    :cond_7
    invoke-super {p0, p1}, Lcom/android/providers/contacts/ContactLocaleUtils$ContactLocaleUtilsBase;->getSortKey(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    goto :goto_1

    .line 517
    .end local v3    # "token":Lcom/android/providers/contacts/HanziToStroke$Token;
    :cond_8
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    goto :goto_1

    .line 519
    .end local v0    # "i$":Ljava/util/Iterator;
    .end local v1    # "sb":Ljava/lang/StringBuilder;
    :cond_9
    invoke-super {p0, p1}, Lcom/android/providers/contacts/ContactLocaleUtils$ContactLocaleUtilsBase;->getSortKey(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    goto :goto_1

    .line 521
    .end local v2    # "tmpDisplayName":Ljava/lang/String;
    .end local v5    # "tokens":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/android/providers/contacts/HanziToStroke$Token;>;"
    :cond_a
    invoke-super {p0, p1}, Lcom/android/providers/contacts/ContactLocaleUtils$ContactLocaleUtilsBase;->getSortKey(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    goto :goto_1
.end method

.method public getStrokeNameLookupKeys(Ljava/lang/String;)Ljava/util/Iterator;
    .locals 25
    .param p1, "name"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "Ljava/util/Iterator",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 535
    new-instance v13, Ljava/util/HashSet;

    invoke-direct {v13}, Ljava/util/HashSet;-><init>()V

    .line 536
    .local v13, "keys":Ljava/util/HashSet;, "Ljava/util/HashSet<Ljava/lang/String;>;"
    invoke-virtual/range {p1 .. p1}, Ljava/lang/String;->length()I

    move-result v14

    .line 537
    .local v14, "maxNameLength":I
    const/16 v22, 0x1e

    move/from16 v0, v22

    if-le v14, v0, :cond_0

    .line 538
    const/16 v14, 0x1e

    .line 541
    :cond_0
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/providers/contacts/ContactLocaleUtils$TraditionalChineseHKContactUtils;->this$0:Lcom/android/providers/contacts/ContactLocaleUtils;

    move-object/from16 v22, v0

    # getter for: Lcom/android/providers/contacts/ContactLocaleUtils;->mLocales:Lcom/android/providers/contacts/LocaleSet;
    invoke-static/range {v22 .. v22}, Lcom/android/providers/contacts/ContactLocaleUtils;->access$000(Lcom/android/providers/contacts/ContactLocaleUtils;)Lcom/android/providers/contacts/LocaleSet;

    move-result-object v22

    invoke-virtual/range {v22 .. v22}, Lcom/android/providers/contacts/LocaleSet;->toString()Ljava/lang/String;

    move-result-object v22

    const-string v23, "zh_HK"

    invoke-virtual/range {v22 .. v23}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v22

    if-eqz v22, :cond_a

    .line 542
    invoke-static {}, Lcom/android/providers/contacts/HanziToStroke;->getIntance()Lcom/android/providers/contacts/HanziToStroke;

    move-result-object v22

    const/16 v23, 0x0

    move-object/from16 v0, p1

    move/from16 v1, v23

    invoke-virtual {v0, v1, v14}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v23

    invoke-virtual/range {v22 .. v23}, Lcom/android/providers/contacts/HanziToStroke;->get(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v20

    .line 543
    .local v20, "tokens":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/android/providers/contacts/HanziToStroke$Token;>;"
    if-nez v20, :cond_1

    .line 544
    const/16 v22, 0x0

    .line 683
    .end local v20    # "tokens":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/android/providers/contacts/HanziToStroke$Token;>;"
    :goto_0
    return-object v22

    .line 547
    .restart local v20    # "tokens":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/android/providers/contacts/HanziToStroke$Token;>;"
    :cond_1
    invoke-virtual/range {v20 .. v20}, Ljava/util/ArrayList;->size()I

    move-result v17

    .line 548
    .local v17, "tokenCount":I
    new-instance v11, Ljava/lang/StringBuilder;

    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    .line 549
    .local v11, "keyStroke":Ljava/lang/StringBuilder;
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    .line 550
    .local v6, "keyInitial":Ljava/lang/StringBuilder;
    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    .line 551
    .local v8, "keyOrignal":Ljava/lang/StringBuilder;
    new-instance v12, Ljava/lang/StringBuilder;

    invoke-direct {v12}, Ljava/lang/StringBuilder;-><init>()V

    .line 552
    .local v12, "keyStrokeNotMap":Ljava/lang/StringBuilder;
    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    .line 554
    .local v7, "keyInitialNotMap":Ljava/lang/StringBuilder;
    add-int/lit8 v2, v17, -0x1

    .local v2, "i":I
    :goto_1
    if-ltz v2, :cond_14

    .line 555
    move-object/from16 v0, v20

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v16

    check-cast v16, Lcom/android/providers/contacts/HanziToStroke$Token;

    .line 556
    .local v16, "token":Lcom/android/providers/contacts/HanziToStroke$Token;
    const/16 v22, 0x2

    move-object/from16 v0, v16

    iget v0, v0, Lcom/android/providers/contacts/HanziToStroke$Token;->type:I

    move/from16 v23, v0

    move/from16 v0, v22

    move/from16 v1, v23

    if-ne v0, v1, :cond_4

    .line 557
    if-nez v2, :cond_3

    .line 558
    const/16 v22, 0x0

    move-object/from16 v0, v16

    iget-object v0, v0, Lcom/android/providers/contacts/HanziToStroke$Token;->target:Ljava/lang/String;

    move-object/from16 v23, v0

    invoke-static/range {v23 .. v23}, Lcom/android/providers/contacts/util/DialerKeypadNumberUtils;->makeActionCodeHKTW(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v23

    move/from16 v0, v22

    move-object/from16 v1, v23

    invoke-virtual {v11, v0, v1}, Ljava/lang/StringBuilder;->insert(ILjava/lang/String;)Ljava/lang/StringBuilder;

    .line 559
    const/16 v22, 0x0

    move-object/from16 v0, v16

    iget-object v0, v0, Lcom/android/providers/contacts/HanziToStroke$Token;->target:Ljava/lang/String;

    move-object/from16 v23, v0

    move/from16 v0, v22

    move-object/from16 v1, v23

    invoke-virtual {v12, v0, v1}, Ljava/lang/StringBuilder;->insert(ILjava/lang/String;)Ljava/lang/StringBuilder;

    .line 564
    :goto_2
    const/16 v22, 0x0

    move-object/from16 v0, v16

    iget-object v0, v0, Lcom/android/providers/contacts/HanziToStroke$Token;->target:Ljava/lang/String;

    move-object/from16 v23, v0

    const/16 v24, 0x0

    invoke-virtual/range {v23 .. v24}, Ljava/lang/String;->charAt(I)C

    move-result v23

    invoke-static/range {v23 .. v23}, Lcom/android/providers/contacts/util/DialerKeypadNumberUtils;->convertTraditionalChineseKeypadLettersToDigits(C)C

    move-result v23

    move/from16 v0, v22

    move/from16 v1, v23

    invoke-virtual {v6, v0, v1}, Ljava/lang/StringBuilder;->insert(IC)Ljava/lang/StringBuilder;

    .line 565
    const/16 v22, 0x0

    move-object/from16 v0, v16

    iget-object v0, v0, Lcom/android/providers/contacts/HanziToStroke$Token;->target:Ljava/lang/String;

    move-object/from16 v23, v0

    const/16 v24, 0x0

    invoke-virtual/range {v23 .. v24}, Ljava/lang/String;->charAt(I)C

    move-result v23

    move/from16 v0, v22

    move/from16 v1, v23

    invoke-virtual {v7, v0, v1}, Ljava/lang/StringBuilder;->insert(IC)Ljava/lang/StringBuilder;

    .line 600
    :goto_3
    const/16 v22, 0x0

    move-object/from16 v0, v16

    iget-object v0, v0, Lcom/android/providers/contacts/HanziToStroke$Token;->source:Ljava/lang/String;

    move-object/from16 v23, v0

    move/from16 v0, v22

    move-object/from16 v1, v23

    invoke-virtual {v8, v0, v1}, Ljava/lang/StringBuilder;->insert(ILjava/lang/String;)Ljava/lang/StringBuilder;

    .line 601
    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v22

    move-object/from16 v0, v22

    invoke-virtual {v13, v0}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    .line 602
    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v22

    move-object/from16 v0, v22

    invoke-virtual {v13, v0}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    .line 603
    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v22

    move-object/from16 v0, v22

    invoke-virtual {v13, v0}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    .line 605
    if-nez v2, :cond_2

    .line 606
    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v22

    move-object/from16 v0, v22

    invoke-virtual {v13, v0}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    .line 607
    invoke-virtual {v12}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v22

    move-object/from16 v0, v22

    invoke-virtual {v13, v0}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    .line 554
    :cond_2
    add-int/lit8 v2, v2, -0x1

    goto/16 :goto_1

    .line 561
    :cond_3
    const/16 v22, 0x0

    move-object/from16 v0, v16

    iget-object v0, v0, Lcom/android/providers/contacts/HanziToStroke$Token;->target:Ljava/lang/String;

    move-object/from16 v23, v0

    const/16 v24, 0x0

    invoke-virtual/range {v23 .. v24}, Ljava/lang/String;->charAt(I)C

    move-result v23

    invoke-static/range {v23 .. v23}, Lcom/android/providers/contacts/util/DialerKeypadNumberUtils;->convertTraditionalChineseKeypadLettersToDigits(C)C

    move-result v23

    move/from16 v0, v22

    move/from16 v1, v23

    invoke-virtual {v11, v0, v1}, Ljava/lang/StringBuilder;->insert(IC)Ljava/lang/StringBuilder;

    .line 562
    const/16 v22, 0x0

    move-object/from16 v0, v16

    iget-object v0, v0, Lcom/android/providers/contacts/HanziToStroke$Token;->target:Ljava/lang/String;

    move-object/from16 v23, v0

    const/16 v24, 0x0

    invoke-virtual/range {v23 .. v24}, Ljava/lang/String;->charAt(I)C

    move-result v23

    move/from16 v0, v22

    move/from16 v1, v23

    invoke-virtual {v12, v0, v1}, Ljava/lang/StringBuilder;->insert(IC)Ljava/lang/StringBuilder;

    goto/16 :goto_2

    .line 566
    :cond_4
    const/16 v22, 0x1

    move-object/from16 v0, v16

    iget v0, v0, Lcom/android/providers/contacts/HanziToStroke$Token;->type:I

    move/from16 v23, v0

    move/from16 v0, v22

    move/from16 v1, v23

    if-ne v0, v1, :cond_8

    .line 568
    invoke-virtual {v11}, Ljava/lang/StringBuilder;->length()I

    move-result v22

    if-lez v22, :cond_5

    .line 569
    const/16 v22, 0x0

    const/16 v23, 0x20

    move/from16 v0, v22

    move/from16 v1, v23

    invoke-virtual {v11, v0, v1}, Ljava/lang/StringBuilder;->insert(IC)Ljava/lang/StringBuilder;

    .line 570
    const/16 v22, 0x0

    const/16 v23, 0x20

    move/from16 v0, v22

    move/from16 v1, v23

    invoke-virtual {v12, v0, v1}, Ljava/lang/StringBuilder;->insert(IC)Ljava/lang/StringBuilder;

    .line 573
    :cond_5
    invoke-virtual {v8}, Ljava/lang/StringBuilder;->length()I

    move-result v22

    if-lez v22, :cond_6

    .line 574
    const/16 v22, 0x0

    const/16 v23, 0x20

    move/from16 v0, v22

    move/from16 v1, v23

    invoke-virtual {v8, v0, v1}, Ljava/lang/StringBuilder;->insert(IC)Ljava/lang/StringBuilder;

    .line 577
    :cond_6
    if-nez v2, :cond_7

    .line 578
    const/16 v22, 0x0

    move-object/from16 v0, v16

    iget-object v0, v0, Lcom/android/providers/contacts/HanziToStroke$Token;->source:Ljava/lang/String;

    move-object/from16 v23, v0

    invoke-static/range {v23 .. v23}, Lcom/android/providers/contacts/util/DialerKeypadNumberUtils;->makeActionCodeHKTW(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v23

    move/from16 v0, v22

    move-object/from16 v1, v23

    invoke-virtual {v11, v0, v1}, Ljava/lang/StringBuilder;->insert(ILjava/lang/String;)Ljava/lang/StringBuilder;

    .line 579
    const/16 v22, 0x0

    move-object/from16 v0, v16

    iget-object v0, v0, Lcom/android/providers/contacts/HanziToStroke$Token;->source:Ljava/lang/String;

    move-object/from16 v23, v0

    move/from16 v0, v22

    move-object/from16 v1, v23

    invoke-virtual {v12, v0, v1}, Ljava/lang/StringBuilder;->insert(ILjava/lang/String;)Ljava/lang/StringBuilder;

    .line 585
    :goto_4
    const/16 v22, 0x0

    move-object/from16 v0, v16

    iget-object v0, v0, Lcom/android/providers/contacts/HanziToStroke$Token;->source:Ljava/lang/String;

    move-object/from16 v23, v0

    const/16 v24, 0x0

    invoke-virtual/range {v23 .. v24}, Ljava/lang/String;->charAt(I)C

    move-result v23

    invoke-static/range {v23 .. v23}, Lcom/android/providers/contacts/util/DialerKeypadNumberUtils;->convertTraditionalChineseKeypadLettersToDigits(C)C

    move-result v23

    move/from16 v0, v22

    move/from16 v1, v23

    invoke-virtual {v6, v0, v1}, Ljava/lang/StringBuilder;->insert(IC)Ljava/lang/StringBuilder;

    .line 586
    const/16 v22, 0x0

    move-object/from16 v0, v16

    iget-object v0, v0, Lcom/android/providers/contacts/HanziToStroke$Token;->source:Ljava/lang/String;

    move-object/from16 v23, v0

    const/16 v24, 0x0

    invoke-virtual/range {v23 .. v24}, Ljava/lang/String;->charAt(I)C

    move-result v23

    move/from16 v0, v22

    move/from16 v1, v23

    invoke-virtual {v7, v0, v1}, Ljava/lang/StringBuilder;->insert(IC)Ljava/lang/StringBuilder;

    goto/16 :goto_3

    .line 581
    :cond_7
    const/16 v22, 0x0

    move-object/from16 v0, v16

    iget-object v0, v0, Lcom/android/providers/contacts/HanziToStroke$Token;->source:Ljava/lang/String;

    move-object/from16 v23, v0

    const/16 v24, 0x0

    invoke-virtual/range {v23 .. v24}, Ljava/lang/String;->charAt(I)C

    move-result v23

    invoke-static/range {v23 .. v23}, Lcom/android/providers/contacts/util/DialerKeypadNumberUtils;->convertTraditionalChineseKeypadLettersToDigits(C)C

    move-result v23

    move/from16 v0, v22

    move/from16 v1, v23

    invoke-virtual {v11, v0, v1}, Ljava/lang/StringBuilder;->insert(IC)Ljava/lang/StringBuilder;

    .line 582
    const/16 v22, 0x0

    move-object/from16 v0, v16

    iget-object v0, v0, Lcom/android/providers/contacts/HanziToStroke$Token;->source:Ljava/lang/String;

    move-object/from16 v23, v0

    const/16 v24, 0x0

    invoke-virtual/range {v23 .. v24}, Ljava/lang/String;->charAt(I)C

    move-result v23

    move/from16 v0, v22

    move/from16 v1, v23

    invoke-virtual {v12, v0, v1}, Ljava/lang/StringBuilder;->insert(IC)Ljava/lang/StringBuilder;

    goto :goto_4

    .line 588
    :cond_8
    if-nez v2, :cond_9

    .line 589
    const/16 v22, 0x0

    move-object/from16 v0, v16

    iget-object v0, v0, Lcom/android/providers/contacts/HanziToStroke$Token;->source:Ljava/lang/String;

    move-object/from16 v23, v0

    invoke-static/range {v23 .. v23}, Lcom/android/providers/contacts/util/DialerKeypadNumberUtils;->makeActionCodeHKTW(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v23

    move/from16 v0, v22

    move-object/from16 v1, v23

    invoke-virtual {v11, v0, v1}, Ljava/lang/StringBuilder;->insert(ILjava/lang/String;)Ljava/lang/StringBuilder;

    .line 590
    const/16 v22, 0x0

    move-object/from16 v0, v16

    iget-object v0, v0, Lcom/android/providers/contacts/HanziToStroke$Token;->source:Ljava/lang/String;

    move-object/from16 v23, v0

    move/from16 v0, v22

    move-object/from16 v1, v23

    invoke-virtual {v12, v0, v1}, Ljava/lang/StringBuilder;->insert(ILjava/lang/String;)Ljava/lang/StringBuilder;

    .line 596
    :goto_5
    const/16 v22, 0x0

    move-object/from16 v0, v16

    iget-object v0, v0, Lcom/android/providers/contacts/HanziToStroke$Token;->source:Ljava/lang/String;

    move-object/from16 v23, v0

    const/16 v24, 0x0

    invoke-virtual/range {v23 .. v24}, Ljava/lang/String;->charAt(I)C

    move-result v23

    invoke-static/range {v23 .. v23}, Lcom/android/providers/contacts/util/DialerKeypadNumberUtils;->convertTraditionalChineseKeypadLettersToDigits(C)C

    move-result v23

    move/from16 v0, v22

    move/from16 v1, v23

    invoke-virtual {v6, v0, v1}, Ljava/lang/StringBuilder;->insert(IC)Ljava/lang/StringBuilder;

    .line 597
    const/16 v22, 0x0

    move-object/from16 v0, v16

    iget-object v0, v0, Lcom/android/providers/contacts/HanziToStroke$Token;->source:Ljava/lang/String;

    move-object/from16 v23, v0

    const/16 v24, 0x0

    invoke-virtual/range {v23 .. v24}, Ljava/lang/String;->charAt(I)C

    move-result v23

    move/from16 v0, v22

    move/from16 v1, v23

    invoke-virtual {v7, v0, v1}, Ljava/lang/StringBuilder;->insert(IC)Ljava/lang/StringBuilder;

    goto/16 :goto_3

    .line 592
    :cond_9
    const/16 v22, 0x0

    move-object/from16 v0, v16

    iget-object v0, v0, Lcom/android/providers/contacts/HanziToStroke$Token;->source:Ljava/lang/String;

    move-object/from16 v23, v0

    const/16 v24, 0x0

    invoke-virtual/range {v23 .. v24}, Ljava/lang/String;->charAt(I)C

    move-result v23

    invoke-static/range {v23 .. v23}, Lcom/android/providers/contacts/util/DialerKeypadNumberUtils;->convertTraditionalChineseKeypadLettersToDigits(C)C

    move-result v23

    move/from16 v0, v22

    move/from16 v1, v23

    invoke-virtual {v11, v0, v1}, Ljava/lang/StringBuilder;->insert(IC)Ljava/lang/StringBuilder;

    .line 593
    const/16 v22, 0x0

    move-object/from16 v0, v16

    iget-object v0, v0, Lcom/android/providers/contacts/HanziToStroke$Token;->source:Ljava/lang/String;

    move-object/from16 v23, v0

    const/16 v24, 0x0

    invoke-virtual/range {v23 .. v24}, Ljava/lang/String;->charAt(I)C

    move-result v23

    move/from16 v0, v22

    move/from16 v1, v23

    invoke-virtual {v12, v0, v1}, Ljava/lang/StringBuilder;->insert(IC)Ljava/lang/StringBuilder;

    goto :goto_5

    .line 612
    .end local v2    # "i":I
    .end local v6    # "keyInitial":Ljava/lang/StringBuilder;
    .end local v7    # "keyInitialNotMap":Ljava/lang/StringBuilder;
    .end local v8    # "keyOrignal":Ljava/lang/StringBuilder;
    .end local v11    # "keyStroke":Ljava/lang/StringBuilder;
    .end local v12    # "keyStrokeNotMap":Ljava/lang/StringBuilder;
    .end local v16    # "token":Lcom/android/providers/contacts/HanziToStroke$Token;
    .end local v17    # "tokenCount":I
    .end local v20    # "tokens":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/android/providers/contacts/HanziToStroke$Token;>;"
    :cond_a
    invoke-static {}, Lcom/android/providers/contacts/HanziToPinyin;->getInstance()Lcom/android/providers/contacts/HanziToPinyin;

    move-result-object v22

    const/16 v23, 0x0

    move-object/from16 v0, p1

    move/from16 v1, v23

    invoke-virtual {v0, v1, v14}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v23

    invoke-virtual/range {v22 .. v23}, Lcom/android/providers/contacts/HanziToPinyin;->getTokens(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v4

    .line 616
    .local v4, "intokens":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/android/providers/contacts/HanziToPinyin$Token;>;"
    new-instance v19, Ljava/util/ArrayList;

    invoke-direct/range {v19 .. v19}, Ljava/util/ArrayList;-><init>()V

    .line 617
    .local v19, "tokens":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/android/providers/contacts/HanziToPinyin$Token;>;"
    invoke-virtual {v4}, Ljava/util/ArrayList;->size()I

    move-result v3

    .line 618
    .local v3, "intokenCount":I
    const/4 v2, 0x0

    .restart local v2    # "i":I
    :goto_6
    add-int/lit8 v22, v3, -0x1

    move/from16 v0, v22

    if-gt v2, v0, :cond_e

    .line 619
    invoke-virtual {v4, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v21

    check-cast v21, Lcom/android/providers/contacts/HanziToPinyin$Token;

    .line 620
    .local v21, "tokentmp":Lcom/android/providers/contacts/HanziToPinyin$Token;
    const/16 v22, 0x2

    move-object/from16 v0, v21

    iget v0, v0, Lcom/android/providers/contacts/HanziToPinyin$Token;->type:I

    move/from16 v23, v0

    move/from16 v0, v22

    move/from16 v1, v23

    if-ne v0, v1, :cond_c

    .line 621
    move-object/from16 v0, v19

    move-object/from16 v1, v21

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 618
    :cond_b
    :goto_7
    add-int/lit8 v2, v2, 0x1

    goto :goto_6

    .line 622
    :cond_c
    const/16 v22, 0x1

    move-object/from16 v0, v21

    iget v0, v0, Lcom/android/providers/contacts/HanziToPinyin$Token;->type:I

    move/from16 v23, v0

    move/from16 v0, v22

    move/from16 v1, v23

    if-ne v0, v1, :cond_d

    .line 623
    move-object/from16 v0, v21

    iget-object v0, v0, Lcom/android/providers/contacts/HanziToPinyin$Token;->source:Ljava/lang/String;

    move-object/from16 v22, v0

    invoke-virtual/range {v22 .. v22}, Ljava/lang/String;->length()I

    move-result v15

    .line 624
    .local v15, "size":I
    const/4 v5, 0x0

    .local v5, "j":I
    :goto_8
    add-int/lit8 v22, v15, -0x1

    move/from16 v0, v22

    if-gt v5, v0, :cond_b

    .line 625
    new-instance v18, Lcom/android/providers/contacts/HanziToPinyin$Token;

    invoke-direct/range {v18 .. v18}, Lcom/android/providers/contacts/HanziToPinyin$Token;-><init>()V

    .line 626
    .local v18, "tokenbpmftmp":Lcom/android/providers/contacts/HanziToPinyin$Token;
    move-object/from16 v0, v21

    iget-object v0, v0, Lcom/android/providers/contacts/HanziToPinyin$Token;->source:Ljava/lang/String;

    move-object/from16 v22, v0

    add-int/lit8 v23, v5, 0x1

    move-object/from16 v0, v22

    move/from16 v1, v23

    invoke-virtual {v0, v5, v1}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v22

    move-object/from16 v0, v22

    move-object/from16 v1, v18

    iput-object v0, v1, Lcom/android/providers/contacts/HanziToPinyin$Token;->source:Ljava/lang/String;

    .line 627
    move-object/from16 v0, v21

    iget-object v0, v0, Lcom/android/providers/contacts/HanziToPinyin$Token;->target:Ljava/lang/String;

    move-object/from16 v22, v0

    add-int/lit8 v23, v5, 0x1

    move-object/from16 v0, v22

    move/from16 v1, v23

    invoke-virtual {v0, v5, v1}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v22

    move-object/from16 v0, v22

    move-object/from16 v1, v18

    iput-object v0, v1, Lcom/android/providers/contacts/HanziToPinyin$Token;->target:Ljava/lang/String;

    .line 628
    move-object/from16 v0, v21

    iget v0, v0, Lcom/android/providers/contacts/HanziToPinyin$Token;->type:I

    move/from16 v22, v0

    move/from16 v0, v22

    move-object/from16 v1, v18

    iput v0, v1, Lcom/android/providers/contacts/HanziToPinyin$Token;->type:I

    .line 629
    move-object/from16 v0, v19

    move-object/from16 v1, v18

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 624
    add-int/lit8 v5, v5, 0x1

    goto :goto_8

    .line 632
    .end local v5    # "j":I
    .end local v15    # "size":I
    .end local v18    # "tokenbpmftmp":Lcom/android/providers/contacts/HanziToPinyin$Token;
    :cond_d
    move-object/from16 v0, v19

    move-object/from16 v1, v21

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_7

    .line 637
    .end local v21    # "tokentmp":Lcom/android/providers/contacts/HanziToPinyin$Token;
    :cond_e
    invoke-virtual/range {v19 .. v19}, Ljava/util/ArrayList;->size()I

    move-result v17

    .line 638
    .restart local v17    # "tokenCount":I
    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    .line 639
    .local v9, "keyPinyin":Ljava/lang/StringBuilder;
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    .line 643
    .restart local v6    # "keyInitial":Ljava/lang/StringBuilder;
    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    .line 644
    .restart local v8    # "keyOrignal":Ljava/lang/StringBuilder;
    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    .line 645
    .local v10, "keyPinyinNotMap":Ljava/lang/StringBuilder;
    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    .line 647
    .restart local v7    # "keyInitialNotMap":Ljava/lang/StringBuilder;
    add-int/lit8 v2, v17, -0x1

    :goto_9
    if-ltz v2, :cond_14

    .line 648
    move-object/from16 v0, v19

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v16

    check-cast v16, Lcom/android/providers/contacts/HanziToPinyin$Token;

    .line 649
    .local v16, "token":Lcom/android/providers/contacts/HanziToPinyin$Token;
    const/16 v22, 0x2

    move-object/from16 v0, v16

    iget v0, v0, Lcom/android/providers/contacts/HanziToPinyin$Token;->type:I

    move/from16 v23, v0

    move/from16 v0, v22

    move/from16 v1, v23

    if-ne v0, v1, :cond_10

    .line 650
    const/16 v22, 0x0

    move-object/from16 v0, v16

    iget-object v0, v0, Lcom/android/providers/contacts/HanziToPinyin$Token;->target:Ljava/lang/String;

    move-object/from16 v23, v0

    invoke-static/range {v23 .. v23}, Lcom/android/providers/contacts/util/DialerKeypadNumberUtils;->makeActionCodeHKTW(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v23

    move/from16 v0, v22

    move-object/from16 v1, v23

    invoke-virtual {v9, v0, v1}, Ljava/lang/StringBuilder;->insert(ILjava/lang/String;)Ljava/lang/StringBuilder;

    .line 651
    const/16 v22, 0x0

    move-object/from16 v0, v16

    iget-object v0, v0, Lcom/android/providers/contacts/HanziToPinyin$Token;->target:Ljava/lang/String;

    move-object/from16 v23, v0

    const/16 v24, 0x0

    invoke-virtual/range {v23 .. v24}, Ljava/lang/String;->charAt(I)C

    move-result v23

    invoke-static/range {v23 .. v23}, Lcom/android/providers/contacts/util/DialerKeypadNumberUtils;->convertTraditionalChineseKeypadLettersToDigits(C)C

    move-result v23

    move/from16 v0, v22

    move/from16 v1, v23

    invoke-virtual {v6, v0, v1}, Ljava/lang/StringBuilder;->insert(IC)Ljava/lang/StringBuilder;

    .line 652
    const/16 v22, 0x0

    move-object/from16 v0, v16

    iget-object v0, v0, Lcom/android/providers/contacts/HanziToPinyin$Token;->target:Ljava/lang/String;

    move-object/from16 v23, v0

    move/from16 v0, v22

    move-object/from16 v1, v23

    invoke-virtual {v10, v0, v1}, Ljava/lang/StringBuilder;->insert(ILjava/lang/String;)Ljava/lang/StringBuilder;

    .line 653
    const/16 v22, 0x0

    move-object/from16 v0, v16

    iget-object v0, v0, Lcom/android/providers/contacts/HanziToPinyin$Token;->target:Ljava/lang/String;

    move-object/from16 v23, v0

    const/16 v24, 0x0

    invoke-virtual/range {v23 .. v24}, Ljava/lang/String;->charAt(I)C

    move-result v23

    move/from16 v0, v22

    move/from16 v1, v23

    invoke-virtual {v7, v0, v1}, Ljava/lang/StringBuilder;->insert(IC)Ljava/lang/StringBuilder;

    .line 672
    :goto_a
    const/16 v22, 0x0

    move-object/from16 v0, v16

    iget-object v0, v0, Lcom/android/providers/contacts/HanziToPinyin$Token;->source:Ljava/lang/String;

    move-object/from16 v23, v0

    move/from16 v0, v22

    move-object/from16 v1, v23

    invoke-virtual {v8, v0, v1}, Ljava/lang/StringBuilder;->insert(ILjava/lang/String;)Ljava/lang/StringBuilder;

    .line 673
    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v22

    move-object/from16 v0, v22

    invoke-virtual {v13, v0}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    .line 674
    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v22

    move-object/from16 v0, v22

    invoke-virtual {v13, v0}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    .line 675
    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v22

    move-object/from16 v0, v22

    invoke-virtual {v13, v0}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    .line 676
    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v22

    move-object/from16 v0, v22

    invoke-virtual {v13, v0}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    .line 677
    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v22

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v23

    invoke-virtual/range {v22 .. v23}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v22

    if-nez v22, :cond_f

    .line 678
    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v22

    move-object/from16 v0, v22

    invoke-virtual {v13, v0}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    .line 679
    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v22

    move-object/from16 v0, v22

    invoke-virtual {v13, v0}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    .line 647
    :cond_f
    add-int/lit8 v2, v2, -0x1

    goto/16 :goto_9

    .line 654
    :cond_10
    const/16 v22, 0x1

    move-object/from16 v0, v16

    iget v0, v0, Lcom/android/providers/contacts/HanziToPinyin$Token;->type:I

    move/from16 v23, v0

    move/from16 v0, v22

    move/from16 v1, v23

    if-ne v0, v1, :cond_13

    .line 656
    invoke-virtual {v9}, Ljava/lang/StringBuilder;->length()I

    move-result v22

    if-lez v22, :cond_11

    .line 657
    const/16 v22, 0x0

    const/16 v23, 0x20

    move/from16 v0, v22

    move/from16 v1, v23

    invoke-virtual {v9, v0, v1}, Ljava/lang/StringBuilder;->insert(IC)Ljava/lang/StringBuilder;

    .line 659
    :cond_11
    invoke-virtual {v8}, Ljava/lang/StringBuilder;->length()I

    move-result v22

    if-lez v22, :cond_12

    .line 660
    const/16 v22, 0x0

    const/16 v23, 0x20

    move/from16 v0, v22

    move/from16 v1, v23

    invoke-virtual {v8, v0, v1}, Ljava/lang/StringBuilder;->insert(IC)Ljava/lang/StringBuilder;

    .line 662
    :cond_12
    const/16 v22, 0x0

    move-object/from16 v0, v16

    iget-object v0, v0, Lcom/android/providers/contacts/HanziToPinyin$Token;->source:Ljava/lang/String;

    move-object/from16 v23, v0

    invoke-static/range {v23 .. v23}, Lcom/android/providers/contacts/util/DialerKeypadNumberUtils;->makeActionCodeHKTW(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v23

    move/from16 v0, v22

    move-object/from16 v1, v23

    invoke-virtual {v9, v0, v1}, Ljava/lang/StringBuilder;->insert(ILjava/lang/String;)Ljava/lang/StringBuilder;

    .line 663
    const/16 v22, 0x0

    move-object/from16 v0, v16

    iget-object v0, v0, Lcom/android/providers/contacts/HanziToPinyin$Token;->source:Ljava/lang/String;

    move-object/from16 v23, v0

    const/16 v24, 0x0

    invoke-virtual/range {v23 .. v24}, Ljava/lang/String;->charAt(I)C

    move-result v23

    invoke-static/range {v23 .. v23}, Lcom/android/providers/contacts/util/DialerKeypadNumberUtils;->convertTraditionalChineseKeypadLettersToDigits(C)C

    move-result v23

    move/from16 v0, v22

    move/from16 v1, v23

    invoke-virtual {v6, v0, v1}, Ljava/lang/StringBuilder;->insert(IC)Ljava/lang/StringBuilder;

    .line 664
    const/16 v22, 0x0

    move-object/from16 v0, v16

    iget-object v0, v0, Lcom/android/providers/contacts/HanziToPinyin$Token;->source:Ljava/lang/String;

    move-object/from16 v23, v0

    move/from16 v0, v22

    move-object/from16 v1, v23

    invoke-virtual {v10, v0, v1}, Ljava/lang/StringBuilder;->insert(ILjava/lang/String;)Ljava/lang/StringBuilder;

    .line 665
    const/16 v22, 0x0

    move-object/from16 v0, v16

    iget-object v0, v0, Lcom/android/providers/contacts/HanziToPinyin$Token;->source:Ljava/lang/String;

    move-object/from16 v23, v0

    const/16 v24, 0x0

    invoke-virtual/range {v23 .. v24}, Ljava/lang/String;->charAt(I)C

    move-result v23

    move/from16 v0, v22

    move/from16 v1, v23

    invoke-virtual {v7, v0, v1}, Ljava/lang/StringBuilder;->insert(IC)Ljava/lang/StringBuilder;

    goto/16 :goto_a

    .line 667
    :cond_13
    const/16 v22, 0x0

    move-object/from16 v0, v16

    iget-object v0, v0, Lcom/android/providers/contacts/HanziToPinyin$Token;->source:Ljava/lang/String;

    move-object/from16 v23, v0

    invoke-static/range {v23 .. v23}, Lcom/android/providers/contacts/util/DialerKeypadNumberUtils;->makeActionCodeHKTW(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v23

    move/from16 v0, v22

    move-object/from16 v1, v23

    invoke-virtual {v9, v0, v1}, Ljava/lang/StringBuilder;->insert(ILjava/lang/String;)Ljava/lang/StringBuilder;

    .line 668
    const/16 v22, 0x0

    move-object/from16 v0, v16

    iget-object v0, v0, Lcom/android/providers/contacts/HanziToPinyin$Token;->source:Ljava/lang/String;

    move-object/from16 v23, v0

    const/16 v24, 0x0

    invoke-virtual/range {v23 .. v24}, Ljava/lang/String;->charAt(I)C

    move-result v23

    invoke-static/range {v23 .. v23}, Lcom/android/providers/contacts/util/DialerKeypadNumberUtils;->convertTraditionalChineseKeypadLettersToDigits(C)C

    move-result v23

    move/from16 v0, v22

    move/from16 v1, v23

    invoke-virtual {v6, v0, v1}, Ljava/lang/StringBuilder;->insert(IC)Ljava/lang/StringBuilder;

    .line 669
    const/16 v22, 0x0

    move-object/from16 v0, v16

    iget-object v0, v0, Lcom/android/providers/contacts/HanziToPinyin$Token;->source:Ljava/lang/String;

    move-object/from16 v23, v0

    move/from16 v0, v22

    move-object/from16 v1, v23

    invoke-virtual {v10, v0, v1}, Ljava/lang/StringBuilder;->insert(ILjava/lang/String;)Ljava/lang/StringBuilder;

    .line 670
    const/16 v22, 0x0

    move-object/from16 v0, v16

    iget-object v0, v0, Lcom/android/providers/contacts/HanziToPinyin$Token;->source:Ljava/lang/String;

    move-object/from16 v23, v0

    const/16 v24, 0x0

    invoke-virtual/range {v23 .. v24}, Ljava/lang/String;->charAt(I)C

    move-result v23

    move/from16 v0, v22

    move/from16 v1, v23

    invoke-virtual {v7, v0, v1}, Ljava/lang/StringBuilder;->insert(IC)Ljava/lang/StringBuilder;

    goto/16 :goto_a

    .line 683
    .end local v3    # "intokenCount":I
    .end local v4    # "intokens":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/android/providers/contacts/HanziToPinyin$Token;>;"
    .end local v9    # "keyPinyin":Ljava/lang/StringBuilder;
    .end local v10    # "keyPinyinNotMap":Ljava/lang/StringBuilder;
    .end local v16    # "token":Lcom/android/providers/contacts/HanziToPinyin$Token;
    .end local v19    # "tokens":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/android/providers/contacts/HanziToPinyin$Token;>;"
    :cond_14
    invoke-virtual {v13}, Ljava/util/HashSet;->iterator()Ljava/util/Iterator;

    move-result-object v22

    goto/16 :goto_0
.end method
