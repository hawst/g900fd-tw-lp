.class public Lcom/android/providers/contacts/ContactsUpgradeReceiver;
.super Landroid/content/BroadcastReceiver;
.source "ContactsUpgradeReceiver.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 42
    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    return-void
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 14
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "intent"    # Landroid/content/Intent;

    .prologue
    .line 57
    :try_start_0
    const-string v10, "ContactsUpgradeReceiver"

    const/4 v11, 0x0

    invoke-virtual {p1, v10, v11}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v7

    .line 58
    .local v7, "prefs":Landroid/content/SharedPreferences;
    const-string v10, "db_version"

    const/4 v11, 0x0

    invoke-interface {v7, v10, v11}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v5

    .line 59
    .local v5, "prefDbVersion":I
    invoke-static {}, Llibcore/icu/ICU;->getIcuVersion()Ljava/lang/String;

    move-result-object v0

    .line 60
    .local v0, "curIcuVersion":Ljava/lang/String;
    const-string v10, "icu_version"

    const-string v11, ""

    invoke-interface {v7, v10, v11}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    .line 62
    .local v6, "prefIcuVersion":Ljava/lang/String;
    const-string v10, "min_match"

    const/4 v11, 0x7

    invoke-interface {v7, v10, v11}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v4

    .line 63
    .local v4, "min_match":I
    invoke-static {p1}, Lcom/android/providers/contacts/ContactsDatabaseHelper;->getInstance(Landroid/content/Context;)Lcom/android/providers/contacts/ContactsDatabaseHelper;

    move-result-object v3

    .line 66
    .local v3, "helper":Lcom/android/providers/contacts/ContactsDatabaseHelper;
    const/16 v10, 0x38f

    if-ne v5, v10, :cond_0

    invoke-virtual {v6, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-nez v10, :cond_1

    .line 72
    :cond_0
    invoke-interface {v7}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v2

    .line 73
    .local v2, "editor":Landroid/content/SharedPreferences$Editor;
    const-string v10, "db_version"

    const/16 v11, 0x38f

    invoke-interface {v2, v10, v11}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    .line 74
    const-string v10, "icu_version"

    invoke-interface {v2, v10, v0}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    .line 75
    invoke-interface {v2}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 81
    invoke-static {p1}, Lcom/android/providers/contacts/ProfileDatabaseHelper;->getInstance(Landroid/content/Context;)Lcom/android/providers/contacts/ProfileDatabaseHelper;

    move-result-object v8

    .line 82
    .local v8, "profileHelper":Lcom/android/providers/contacts/ProfileDatabaseHelper;
    const-string v10, "ContactsUpgradeReceiver"

    const-string v11, "Creating or opening contacts database"

    invoke-static {v10, v11}, Landroid/util/secutil/Log;->secI(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_1

    .line 84
    :try_start_1
    invoke-static {}, Landroid/app/ActivityManagerNative;->getDefault()Landroid/app/IActivityManager;

    move-result-object v10

    const v11, 0x7f050003

    invoke-virtual {p1, v11}, Landroid/content/Context;->getText(I)Ljava/lang/CharSequence;

    move-result-object v11

    const/4 v12, 0x1

    invoke-interface {v10, v11, v12}, Landroid/app/IActivityManager;->showBootMessage(Ljava/lang/CharSequence;Z)V
    :try_end_1
    .catch Landroid/os/RemoteException; {:try_start_1 .. :try_end_1} :catch_0
    .catch Ljava/lang/Throwable; {:try_start_1 .. :try_end_1} :catch_1

    .line 89
    :goto_0
    :try_start_2
    invoke-virtual {v3}, Lcom/android/providers/contacts/ContactsDatabaseHelper;->getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    .line 91
    invoke-virtual {v8}, Lcom/android/providers/contacts/ProfileDatabaseHelper;->getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    .line 92
    invoke-static {p1, v3, v8}, Lcom/android/providers/contacts/ContactsProvider2;->updateLocaleOffline(Landroid/content/Context;Lcom/android/providers/contacts/ContactsDatabaseHelper;Lcom/android/providers/contacts/ProfileDatabaseHelper;)V

    .line 98
    .end local v2    # "editor":Landroid/content/SharedPreferences$Editor;
    .end local v8    # "profileHelper":Lcom/android/providers/contacts/ProfileDatabaseHelper;
    :cond_1
    invoke-static {}, Lcom/android/providers/contacts/util/LoadCscFeatureUtils;->getInstance()Lcom/android/providers/contacts/util/LoadCscFeatureUtils;

    move-result-object v10

    invoke-virtual {v10}, Lcom/android/providers/contacts/util/LoadCscFeatureUtils;->getMinMatch()I

    move-result v10

    if-eq v4, v10, :cond_2

    .line 99
    const-string v10, "ContactsUpgradeReceiver"

    new-instance v11, Ljava/lang/StringBuilder;

    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    const-string v12, "Update CallerIdMatchingDigit "

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v11

    const-string v12, " -> "

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-static {}, Lcom/android/providers/contacts/util/LoadCscFeatureUtils;->getInstance()Lcom/android/providers/contacts/util/LoadCscFeatureUtils;

    move-result-object v12

    invoke-virtual {v12}, Lcom/android/providers/contacts/util/LoadCscFeatureUtils;->getMinMatch()I

    move-result v12

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-static {v10, v11}, Landroid/util/secutil/Log;->secI(Ljava/lang/String;Ljava/lang/String;)I

    .line 100
    const-string v10, "phone_lookup"

    const-string v11, "0"

    invoke-virtual {v3, v10, v11}, Lcom/android/providers/contacts/ContactsDatabaseHelper;->setProperty(Ljava/lang/String;Ljava/lang/String;)V

    .line 101
    invoke-interface {v7}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v10

    const-string v11, "min_match"

    invoke-static {}, Lcom/android/providers/contacts/util/LoadCscFeatureUtils;->getInstance()Lcom/android/providers/contacts/util/LoadCscFeatureUtils;

    move-result-object v12

    invoke-virtual {v12}, Lcom/android/providers/contacts/util/LoadCscFeatureUtils;->getMinMatch()I

    move-result v12

    invoke-interface {v10, v11, v12}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    move-result-object v10

    invoke-interface {v10}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 115
    .end local v0    # "curIcuVersion":Ljava/lang/String;
    .end local v3    # "helper":Lcom/android/providers/contacts/ContactsDatabaseHelper;
    .end local v4    # "min_match":I
    .end local v5    # "prefDbVersion":I
    .end local v6    # "prefIcuVersion":Ljava/lang/String;
    .end local v7    # "prefs":Landroid/content/SharedPreferences;
    :cond_2
    :goto_1
    return-void

    .line 86
    .restart local v0    # "curIcuVersion":Ljava/lang/String;
    .restart local v2    # "editor":Landroid/content/SharedPreferences$Editor;
    .restart local v3    # "helper":Lcom/android/providers/contacts/ContactsDatabaseHelper;
    .restart local v4    # "min_match":I
    .restart local v5    # "prefDbVersion":I
    .restart local v6    # "prefIcuVersion":Ljava/lang/String;
    .restart local v7    # "prefs":Landroid/content/SharedPreferences;
    .restart local v8    # "profileHelper":Lcom/android/providers/contacts/ProfileDatabaseHelper;
    :catch_0
    move-exception v1

    .line 87
    .local v1, "e":Landroid/os/RemoteException;
    invoke-virtual {v1}, Landroid/os/RemoteException;->printStackTrace()V
    :try_end_2
    .catch Ljava/lang/Throwable; {:try_start_2 .. :try_end_2} :catch_1

    goto :goto_0

    .line 106
    .end local v0    # "curIcuVersion":Ljava/lang/String;
    .end local v1    # "e":Landroid/os/RemoteException;
    .end local v2    # "editor":Landroid/content/SharedPreferences$Editor;
    .end local v3    # "helper":Lcom/android/providers/contacts/ContactsDatabaseHelper;
    .end local v4    # "min_match":I
    .end local v5    # "prefDbVersion":I
    .end local v6    # "prefIcuVersion":Ljava/lang/String;
    .end local v7    # "prefs":Landroid/content/SharedPreferences;
    .end local v8    # "profileHelper":Lcom/android/providers/contacts/ProfileDatabaseHelper;
    :catch_1
    move-exception v9

    .line 109
    .local v9, "t":Ljava/lang/Throwable;
    const-string v10, "ContactsUpgradeReceiver"

    const-string v11, "Error during upgrade attempt. Disabling receiver."

    invoke-static {v10, v11, v9}, Landroid/util/secutil/Log;->wtf(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 110
    invoke-virtual {p1}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v10

    new-instance v11, Landroid/content/ComponentName;

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v12

    invoke-direct {v11, p1, v12}, Landroid/content/ComponentName;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const/4 v12, 0x2

    const/4 v13, 0x1

    invoke-virtual {v10, v11, v12, v13}, Landroid/content/pm/PackageManager;->setComponentEnabledSetting(Landroid/content/ComponentName;II)V

    goto :goto_1
.end method
