.class public Lcom/android/providers/contacts/util/CursorCacheManager;
.super Ljava/lang/Object;
.source "CursorCacheManager.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/android/providers/contacts/util/CursorCacheManager$UniqCacheKey;
    }
.end annotation


# static fields
.field private static final SEPARATOR_PATTERN:Ljava/util/regex/Pattern;

.field private static mContext:Landroid/content/Context;

.field private static mDisabled:Z

.field private static sSingleton:Lcom/android/providers/contacts/util/CursorCacheManager;


# instance fields
.field private mCache:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Lcom/android/providers/contacts/util/CursorCacheManager$UniqCacheKey;",
            "Landroid/database/Cursor;",
            ">;"
        }
    .end annotation
.end field

.field private mCachedQueryUri:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/Integer;",
            "Ljava/util/HashSet",
            "<",
            "Ljava/lang/Integer;",
            ">;>;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 65
    const-string v0, "\u0001"

    invoke-static {v0}, Ljava/util/regex/Pattern;->compile(Ljava/lang/String;)Ljava/util/regex/Pattern;

    move-result-object v0

    sput-object v0, Lcom/android/providers/contacts/util/CursorCacheManager;->SEPARATOR_PATTERN:Ljava/util/regex/Pattern;

    .line 67
    const/4 v0, 0x0

    sput-object v0, Lcom/android/providers/contacts/util/CursorCacheManager;->mContext:Landroid/content/Context;

    .line 69
    const/4 v0, 0x0

    sput-boolean v0, Lcom/android/providers/contacts/util/CursorCacheManager;->mDisabled:Z

    return-void
.end method

.method private constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 71
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 57
    invoke-static {}, Lcom/google/android/collect/Maps;->newHashMap()Ljava/util/HashMap;

    move-result-object v0

    iput-object v0, p0, Lcom/android/providers/contacts/util/CursorCacheManager;->mCache:Ljava/util/Map;

    .line 59
    invoke-static {}, Lcom/google/android/collect/Maps;->newHashMap()Ljava/util/HashMap;

    move-result-object v0

    iput-object v0, p0, Lcom/android/providers/contacts/util/CursorCacheManager;->mCachedQueryUri:Ljava/util/Map;

    .line 72
    sput-object p1, Lcom/android/providers/contacts/util/CursorCacheManager;->mContext:Landroid/content/Context;

    .line 73
    return-void
.end method

.method private appendIfNotNull(Ljava/lang/StringBuilder;Ljava/lang/Object;)V
    .locals 1
    .param p1, "sb"    # Ljava/lang/StringBuilder;
    .param p2, "value"    # Ljava/lang/Object;

    .prologue
    .line 325
    if-eqz p2, :cond_0

    .line 326
    invoke-virtual {p2}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 328
    :cond_0
    return-void
.end method

.method private appendStringArray(Ljava/lang/StringBuilder;[Ljava/lang/String;)V
    .locals 2
    .param p1, "sb"    # Ljava/lang/StringBuilder;
    .param p2, "array"    # [Ljava/lang/String;

    .prologue
    .line 355
    if-eqz p2, :cond_0

    .line 356
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    array-length v1, p2

    if-ge v0, v1, :cond_0

    .line 357
    const-string v1, "\u0001"

    invoke-direct {p0, p1, v1}, Lcom/android/providers/contacts/util/CursorCacheManager;->appendIfNotNull(Ljava/lang/StringBuilder;Ljava/lang/Object;)V

    .line 358
    aget-object v1, p2, v0

    invoke-direct {p0, p1, v1}, Lcom/android/providers/contacts/util/CursorCacheManager;->appendIfNotNull(Ljava/lang/StringBuilder;Ljava/lang/Object;)V

    .line 356
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 361
    .end local v0    # "i":I
    :cond_0
    return-void
.end method

.method private buildCacheKey(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    .locals 2
    .param p1, "queryUri"    # Landroid/net/Uri;
    .param p2, "projection"    # [Ljava/lang/String;
    .param p3, "selection"    # Ljava/lang/String;
    .param p4, "selectionArgs"    # [Ljava/lang/String;
    .param p5, "sortOrder"    # Ljava/lang/String;

    .prologue
    .line 338
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 340
    .local v0, "sb":Ljava/lang/StringBuilder;
    invoke-direct {p0, v0, p1}, Lcom/android/providers/contacts/util/CursorCacheManager;->appendIfNotNull(Ljava/lang/StringBuilder;Ljava/lang/Object;)V

    .line 341
    const-string v1, "\u0001"

    invoke-direct {p0, v0, v1}, Lcom/android/providers/contacts/util/CursorCacheManager;->appendIfNotNull(Ljava/lang/StringBuilder;Ljava/lang/Object;)V

    .line 342
    invoke-direct {p0, v0, p3}, Lcom/android/providers/contacts/util/CursorCacheManager;->appendIfNotNull(Ljava/lang/StringBuilder;Ljava/lang/Object;)V

    .line 343
    const-string v1, "\u0001"

    invoke-direct {p0, v0, v1}, Lcom/android/providers/contacts/util/CursorCacheManager;->appendIfNotNull(Ljava/lang/StringBuilder;Ljava/lang/Object;)V

    .line 344
    invoke-direct {p0, v0, p5}, Lcom/android/providers/contacts/util/CursorCacheManager;->appendIfNotNull(Ljava/lang/StringBuilder;Ljava/lang/Object;)V

    .line 345
    const-string v1, "\u0001"

    invoke-direct {p0, v0, v1}, Lcom/android/providers/contacts/util/CursorCacheManager;->appendIfNotNull(Ljava/lang/StringBuilder;Ljava/lang/Object;)V

    .line 346
    invoke-direct {p0}, Lcom/android/providers/contacts/util/CursorCacheManager;->getPackageName()Ljava/lang/String;

    move-result-object v1

    invoke-direct {p0, v0, v1}, Lcom/android/providers/contacts/util/CursorCacheManager;->appendIfNotNull(Ljava/lang/StringBuilder;Ljava/lang/Object;)V

    .line 347
    const-string v1, "\u0001"

    invoke-direct {p0, v0, v1}, Lcom/android/providers/contacts/util/CursorCacheManager;->appendIfNotNull(Ljava/lang/StringBuilder;Ljava/lang/Object;)V

    .line 348
    invoke-direct {p0, v0, p2}, Lcom/android/providers/contacts/util/CursorCacheManager;->appendStringArray(Ljava/lang/StringBuilder;[Ljava/lang/String;)V

    .line 349
    invoke-direct {p0, v0, p4}, Lcom/android/providers/contacts/util/CursorCacheManager;->appendStringArray(Ljava/lang/StringBuilder;[Ljava/lang/String;)V

    .line 351
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    return-object v1
.end method

.method private cloneCursor(Landroid/database/Cursor;)Landroid/database/MatrixCursor;
    .locals 9
    .param p1, "source"    # Landroid/database/Cursor;

    .prologue
    .line 288
    invoke-interface {p1}, Landroid/database/Cursor;->getColumnCount()I

    move-result v4

    .line 290
    .local v4, "i":I
    const/4 v0, 0x1

    .local v0, "FIELD_TYPE_INTEGER":I
    const/4 v2, 0x3

    .local v2, "FIELD_TYPE_STRING":I
    const/4 v1, 0x0

    .line 291
    .local v1, "FIELD_TYPE_NULL":I
    new-instance v6, Landroid/database/MatrixCursor;

    invoke-interface {p1}, Landroid/database/Cursor;->getColumnNames()[Ljava/lang/String;

    move-result-object v7

    invoke-interface {p1}, Landroid/database/Cursor;->getCount()I

    move-result v8

    invoke-direct {v6, v7, v8}, Landroid/database/MatrixCursor;-><init>([Ljava/lang/String;I)V

    .line 292
    .local v6, "result":Landroid/database/MatrixCursor;
    invoke-interface {p1}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v7

    if-eqz v7, :cond_2

    .line 294
    :cond_0
    new-array v3, v4, [Ljava/lang/Object;

    .line 295
    .local v3, "currRow":[Ljava/lang/Object;
    const/4 v5, 0x0

    .local v5, "j":I
    :goto_0
    if-ge v5, v4, :cond_1

    .line 296
    invoke-interface {p1, v5}, Landroid/database/Cursor;->getType(I)I

    move-result v7

    packed-switch v7, :pswitch_data_0

    .line 307
    :pswitch_0
    invoke-interface {p1, v5}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v7

    aput-object v7, v3, v5

    .line 295
    :goto_1
    add-int/lit8 v5, v5, 0x1

    goto :goto_0

    .line 298
    :pswitch_1
    invoke-interface {p1, v5}, Landroid/database/Cursor;->getInt(I)I

    move-result v7

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    aput-object v7, v3, v5

    goto :goto_1

    .line 301
    :pswitch_2
    invoke-interface {p1, v5}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v7

    aput-object v7, v3, v5

    goto :goto_1

    .line 304
    :pswitch_3
    const/4 v7, 0x0

    aput-object v7, v3, v5

    goto :goto_1

    .line 312
    :cond_1
    invoke-virtual {v6, v3}, Landroid/database/MatrixCursor;->addRow([Ljava/lang/Object;)V

    .line 313
    invoke-interface {p1}, Landroid/database/Cursor;->moveToNext()Z

    move-result v7

    if-nez v7, :cond_0

    .line 315
    .end local v3    # "currRow":[Ljava/lang/Object;
    .end local v5    # "j":I
    :cond_2
    sget-object v7, Lcom/android/providers/contacts/util/CursorCacheManager;->mContext:Landroid/content/Context;

    invoke-virtual {v7}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v7

    sget-object v8, Landroid/provider/ContactsContract;->AUTHORITY_URI:Landroid/net/Uri;

    invoke-virtual {v6, v7, v8}, Landroid/database/MatrixCursor;->setNotificationUri(Landroid/content/ContentResolver;Landroid/net/Uri;)V

    .line 316
    return-object v6

    .line 296
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_3
        :pswitch_1
        :pswitch_0
        :pswitch_2
    .end packed-switch
.end method

.method public static getInstance()Lcom/android/providers/contacts/util/CursorCacheManager;
    .locals 1

    .prologue
    .line 83
    sget-object v0, Lcom/android/providers/contacts/util/CursorCacheManager;->sSingleton:Lcom/android/providers/contacts/util/CursorCacheManager;

    if-nez v0, :cond_0

    .line 84
    const/4 v0, 0x0

    .line 85
    :goto_0
    return-object v0

    :cond_0
    sget-object v0, Lcom/android/providers/contacts/util/CursorCacheManager;->sSingleton:Lcom/android/providers/contacts/util/CursorCacheManager;

    goto :goto_0
.end method

.method public static getInstance(Landroid/content/Context;)Lcom/android/providers/contacts/util/CursorCacheManager;
    .locals 1
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 107
    sget-object v0, Lcom/android/providers/contacts/util/CursorCacheManager;->sSingleton:Lcom/android/providers/contacts/util/CursorCacheManager;

    if-nez v0, :cond_0

    .line 108
    new-instance v0, Lcom/android/providers/contacts/util/CursorCacheManager;

    invoke-direct {v0, p0}, Lcom/android/providers/contacts/util/CursorCacheManager;-><init>(Landroid/content/Context;)V

    sput-object v0, Lcom/android/providers/contacts/util/CursorCacheManager;->sSingleton:Lcom/android/providers/contacts/util/CursorCacheManager;

    .line 110
    :cond_0
    sget-object v0, Lcom/android/providers/contacts/util/CursorCacheManager;->sSingleton:Lcom/android/providers/contacts/util/CursorCacheManager;

    return-object v0
.end method

.method private getPackageName()Ljava/lang/String;
    .locals 2

    .prologue
    .line 248
    invoke-static {}, Landroid/os/Binder;->getCallingPid()I

    move-result v1

    .line 250
    .local v1, "pid":I
    invoke-direct {p0, v1}, Lcom/android/providers/contacts/util/CursorCacheManager;->getProcessNameFromPid(I)Ljava/lang/String;

    move-result-object v0

    .line 252
    .local v0, "packageName":Ljava/lang/String;
    return-object v0
.end method

.method private getProcessNameFromPid(I)Ljava/lang/String;
    .locals 6
    .param p1, "givenPid"    # I

    .prologue
    .line 270
    sget-object v4, Lcom/android/providers/contacts/util/CursorCacheManager;->mContext:Landroid/content/Context;

    const-string v5, "activity"

    invoke-virtual {v4, v5}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/app/ActivityManager;

    .line 272
    .local v1, "am":Landroid/app/ActivityManager;
    invoke-virtual {v1}, Landroid/app/ActivityManager;->getRunningAppProcesses()Ljava/util/List;

    move-result-object v3

    .line 273
    .local v3, "lstAppInfo":Ljava/util/List;, "Ljava/util/List<Landroid/app/ActivityManager$RunningAppProcessInfo;>;"
    if-eqz v3, :cond_1

    .line 274
    invoke-interface {v3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    .local v2, "i$":Ljava/util/Iterator;
    :cond_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/ActivityManager$RunningAppProcessInfo;

    .line 275
    .local v0, "ai":Landroid/app/ActivityManager$RunningAppProcessInfo;
    if-eqz v0, :cond_0

    iget v4, v0, Landroid/app/ActivityManager$RunningAppProcessInfo;->pid:I

    if-ne v4, p1, :cond_0

    .line 276
    iget-object v4, v0, Landroid/app/ActivityManager$RunningAppProcessInfo;->processName:Ljava/lang/String;

    .line 280
    .end local v0    # "ai":Landroid/app/ActivityManager$RunningAppProcessInfo;
    .end local v2    # "i$":Ljava/util/Iterator;
    :goto_0
    return-object v4

    :cond_1
    const/4 v4, 0x0

    goto :goto_0
.end method

.method private isCachedUri(Ljava/lang/Integer;)Z
    .locals 2
    .param p1, "uriCode"    # Ljava/lang/Integer;

    .prologue
    .line 256
    if-eqz p1, :cond_0

    iget-object v0, p0, Lcom/android/providers/contacts/util/CursorCacheManager;->mCachedQueryUri:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/android/providers/contacts/util/CursorCacheManager;->mDisabled:Z

    if-nez v0, :cond_0

    invoke-virtual {p1}, Ljava/lang/Integer;->intValue()I

    move-result v0

    const/4 v1, -0x1

    if-ne v0, v1, :cond_1

    .line 258
    :cond_0
    const/4 v0, 0x0

    .line 259
    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x1

    goto :goto_0
.end method


# virtual methods
.method public disable()V
    .locals 1

    .prologue
    .line 364
    const/4 v0, 0x1

    sput-boolean v0, Lcom/android/providers/contacts/util/CursorCacheManager;->mDisabled:Z

    .line 365
    return-void
.end method

.method public get(Ljava/lang/Integer;Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    .locals 10
    .param p1, "uriCode"    # Ljava/lang/Integer;
    .param p2, "queryUri"    # Landroid/net/Uri;
    .param p3, "projection"    # [Ljava/lang/String;
    .param p4, "selection"    # Ljava/lang/String;
    .param p5, "selectionArgs"    # [Ljava/lang/String;
    .param p6, "sortOrder"    # Ljava/lang/String;

    .prologue
    .line 125
    invoke-direct {p0, p1}, Lcom/android/providers/contacts/util/CursorCacheManager;->isCachedUri(Ljava/lang/Integer;)Z

    move-result v0

    if-eqz v0, :cond_0

    if-nez p4, :cond_0

    if-nez p5, :cond_0

    if-eqz p6, :cond_1

    .line 127
    :cond_0
    const/4 v6, 0x0

    .line 141
    :goto_0
    return-object v6

    .line 129
    :cond_1
    iget-object v9, p0, Lcom/android/providers/contacts/util/CursorCacheManager;->mCache:Ljava/util/Map;

    monitor-enter v9

    move-object v0, p0

    move-object v1, p2

    move-object v2, p3

    move-object v3, p4

    move-object v4, p5

    move-object/from16 v5, p6

    .line 130
    :try_start_0
    invoke-direct/range {v0 .. v5}, Lcom/android/providers/contacts/util/CursorCacheManager;->buildCacheKey(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    .line 132
    .local v7, "key":Ljava/lang/String;
    invoke-static {p1, v7}, Lcom/android/providers/contacts/util/CursorCacheManager$UniqCacheKey;->getInstanceWithKey(Ljava/lang/Integer;Ljava/lang/String;)Lcom/android/providers/contacts/util/CursorCacheManager$UniqCacheKey;

    move-result-object v8

    .line 134
    .local v8, "uKey":Lcom/android/providers/contacts/util/CursorCacheManager$UniqCacheKey;
    const/4 v6, 0x0

    .line 135
    .local v6, "c":Landroid/database/Cursor;
    iget-object v0, p0, Lcom/android/providers/contacts/util/CursorCacheManager;->mCache:Ljava/util/Map;

    invoke-interface {v0, v8}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 136
    iget-object v0, p0, Lcom/android/providers/contacts/util/CursorCacheManager;->mCache:Ljava/util/Map;

    invoke-interface {v0, v8}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v6

    .end local v6    # "c":Landroid/database/Cursor;
    check-cast v6, Landroid/database/Cursor;

    .line 137
    .restart local v6    # "c":Landroid/database/Cursor;
    invoke-direct {p0, v6}, Lcom/android/providers/contacts/util/CursorCacheManager;->cloneCursor(Landroid/database/Cursor;)Landroid/database/MatrixCursor;

    move-result-object v6

    .line 138
    sget-object v0, Lcom/android/providers/contacts/util/CursorCacheManager;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    sget-object v1, Landroid/provider/ContactsContract;->AUTHORITY_URI:Landroid/net/Uri;

    invoke-interface {v6, v0, v1}, Landroid/database/Cursor;->setNotificationUri(Landroid/content/ContentResolver;Landroid/net/Uri;)V

    .line 141
    :cond_2
    monitor-exit v9

    goto :goto_0

    .line 142
    .end local v6    # "c":Landroid/database/Cursor;
    .end local v7    # "key":Ljava/lang/String;
    .end local v8    # "uKey":Lcom/android/providers/contacts/util/CursorCacheManager$UniqCacheKey;
    :catchall_0
    move-exception v0

    monitor-exit v9
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public invalidate(Ljava/lang/Integer;)V
    .locals 13
    .param p1, "uriCodeChanged"    # Ljava/lang/Integer;

    .prologue
    .line 221
    iget-object v11, p0, Lcom/android/providers/contacts/util/CursorCacheManager;->mCache:Ljava/util/Map;

    monitor-enter v11

    .line 222
    :try_start_0
    invoke-static {}, Lcom/google/android/collect/Sets;->newHashSet()Ljava/util/HashSet;

    move-result-object v1

    .line 223
    .local v1, "flushTarget":Ljava/util/HashSet;, "Ljava/util/HashSet<Ljava/lang/Integer;>;"
    iget-object v10, p0, Lcom/android/providers/contacts/util/CursorCacheManager;->mCachedQueryUri:Ljava/util/Map;

    invoke-interface {v10}, Ljava/util/Map;->keySet()Ljava/util/Set;

    move-result-object v7

    .line 224
    .local v7, "targetCacheSet":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/Integer;>;"
    invoke-interface {v7}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v3

    .local v3, "i$":Ljava/util/Iterator;
    :cond_0
    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v10

    if-eqz v10, :cond_1

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Ljava/lang/Integer;

    .line 225
    .local v8, "targetUriCode":Ljava/lang/Integer;
    const/4 v4, 0x0

    .line 226
    .local v4, "ignoreSetFromTargetUriCode":Ljava/util/HashSet;, "Ljava/util/HashSet<Ljava/lang/Integer;>;"
    iget-object v10, p0, Lcom/android/providers/contacts/util/CursorCacheManager;->mCachedQueryUri:Ljava/util/Map;

    invoke-interface {v10, v8}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v4

    .end local v4    # "ignoreSetFromTargetUriCode":Ljava/util/HashSet;, "Ljava/util/HashSet<Ljava/lang/Integer;>;"
    check-cast v4, Ljava/util/HashSet;

    .line 227
    .restart local v4    # "ignoreSetFromTargetUriCode":Ljava/util/HashSet;, "Ljava/util/HashSet<Ljava/lang/Integer;>;"
    invoke-virtual {v4, p1}, Ljava/util/HashSet;->contains(Ljava/lang/Object;)Z

    move-result v10

    if-nez v10, :cond_0

    .line 228
    invoke-virtual {v1, v8}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 244
    .end local v1    # "flushTarget":Ljava/util/HashSet;, "Ljava/util/HashSet<Ljava/lang/Integer;>;"
    .end local v3    # "i$":Ljava/util/Iterator;
    .end local v4    # "ignoreSetFromTargetUriCode":Ljava/util/HashSet;, "Ljava/util/HashSet<Ljava/lang/Integer;>;"
    .end local v7    # "targetCacheSet":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/Integer;>;"
    .end local v8    # "targetUriCode":Ljava/lang/Integer;
    :catchall_0
    move-exception v10

    monitor-exit v11
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v10

    .line 231
    .restart local v1    # "flushTarget":Ljava/util/HashSet;, "Ljava/util/HashSet<Ljava/lang/Integer;>;"
    .restart local v3    # "i$":Ljava/util/Iterator;
    .restart local v7    # "targetCacheSet":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/Integer;>;"
    :cond_1
    :try_start_1
    iget-object v10, p0, Lcom/android/providers/contacts/util/CursorCacheManager;->mCache:Ljava/util/Map;

    invoke-interface {v10}, Ljava/util/Map;->keySet()Ljava/util/Set;

    move-result-object v6

    .line 232
    .local v6, "setKeys":Ljava/util/Set;, "Ljava/util/Set<Lcom/android/providers/contacts/util/CursorCacheManager$UniqCacheKey;>;"
    invoke-static {}, Lcom/google/android/collect/Lists;->newArrayList()Ljava/util/ArrayList;

    move-result-object v5

    .line 233
    .local v5, "removeTarget":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/android/providers/contacts/util/CursorCacheManager$UniqCacheKey;>;"
    invoke-interface {v6}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_2
    :goto_1
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v10

    if-eqz v10, :cond_4

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v9

    check-cast v9, Lcom/android/providers/contacts/util/CursorCacheManager$UniqCacheKey;

    .line 234
    .local v9, "uKey":Lcom/android/providers/contacts/util/CursorCacheManager$UniqCacheKey;
    # getter for: Lcom/android/providers/contacts/util/CursorCacheManager$UniqCacheKey;->mUriCode:Ljava/lang/Integer;
    invoke-static {v9}, Lcom/android/providers/contacts/util/CursorCacheManager$UniqCacheKey;->access$000(Lcom/android/providers/contacts/util/CursorCacheManager$UniqCacheKey;)Ljava/lang/Integer;

    move-result-object v10

    invoke-virtual {v1, v10}, Ljava/util/HashSet;->contains(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_2

    .line 235
    iget-object v10, p0, Lcom/android/providers/contacts/util/CursorCacheManager;->mCache:Ljava/util/Map;

    invoke-interface {v10, v9}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/database/Cursor;

    .line 236
    .local v0, "c":Landroid/database/Cursor;
    if-eqz v0, :cond_3

    invoke-interface {v0}, Landroid/database/Cursor;->isClosed()Z

    move-result v10

    if-nez v10, :cond_3

    .line 237
    invoke-interface {v0}, Landroid/database/Cursor;->close()V

    .line 238
    :cond_3
    invoke-virtual {v5, v9}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 241
    .end local v0    # "c":Landroid/database/Cursor;
    .end local v9    # "uKey":Lcom/android/providers/contacts/util/CursorCacheManager$UniqCacheKey;
    :cond_4
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_2
    invoke-virtual {v5}, Ljava/util/ArrayList;->size()I

    move-result v10

    if-ge v2, v10, :cond_5

    .line 242
    iget-object v10, p0, Lcom/android/providers/contacts/util/CursorCacheManager;->mCache:Ljava/util/Map;

    invoke-virtual {v5, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v12

    invoke-interface {v10, v12}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 241
    add-int/lit8 v2, v2, 0x1

    goto :goto_2

    .line 244
    :cond_5
    monitor-exit v11
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 245
    return-void
.end method

.method public invalidateAll()V
    .locals 9

    .prologue
    .line 193
    iget-object v7, p0, Lcom/android/providers/contacts/util/CursorCacheManager;->mCache:Ljava/util/Map;

    monitor-enter v7

    .line 194
    :try_start_0
    iget-object v6, p0, Lcom/android/providers/contacts/util/CursorCacheManager;->mCache:Ljava/util/Map;

    invoke-interface {v6}, Ljava/util/Map;->keySet()Ljava/util/Set;

    move-result-object v5

    .line 196
    .local v5, "setKeys":Ljava/util/Set;, "Ljava/util/Set<Lcom/android/providers/contacts/util/CursorCacheManager$UniqCacheKey;>;"
    invoke-static {}, Lcom/google/android/collect/Lists;->newArrayList()Ljava/util/ArrayList;

    move-result-object v4

    .line 197
    .local v4, "removeTarget":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/android/providers/contacts/util/CursorCacheManager$UniqCacheKey;>;"
    invoke-interface {v5}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v3

    .local v3, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v6

    if-eqz v6, :cond_1

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/android/providers/contacts/util/CursorCacheManager$UniqCacheKey;

    .line 198
    .local v1, "code":Lcom/android/providers/contacts/util/CursorCacheManager$UniqCacheKey;
    iget-object v6, p0, Lcom/android/providers/contacts/util/CursorCacheManager;->mCache:Ljava/util/Map;

    invoke-interface {v6, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/database/Cursor;

    .line 199
    .local v0, "c":Landroid/database/Cursor;
    if-eqz v0, :cond_0

    invoke-interface {v0}, Landroid/database/Cursor;->isClosed()Z

    move-result v6

    if-nez v6, :cond_0

    .line 200
    invoke-interface {v0}, Landroid/database/Cursor;->close()V

    .line 201
    :cond_0
    invoke-virtual {v4, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 209
    .end local v0    # "c":Landroid/database/Cursor;
    .end local v1    # "code":Lcom/android/providers/contacts/util/CursorCacheManager$UniqCacheKey;
    .end local v3    # "i$":Ljava/util/Iterator;
    .end local v4    # "removeTarget":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/android/providers/contacts/util/CursorCacheManager$UniqCacheKey;>;"
    .end local v5    # "setKeys":Ljava/util/Set;, "Ljava/util/Set<Lcom/android/providers/contacts/util/CursorCacheManager$UniqCacheKey;>;"
    :catchall_0
    move-exception v6

    monitor-exit v7
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v6

    .line 204
    .restart local v3    # "i$":Ljava/util/Iterator;
    .restart local v4    # "removeTarget":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/android/providers/contacts/util/CursorCacheManager$UniqCacheKey;>;"
    .restart local v5    # "setKeys":Ljava/util/Set;, "Ljava/util/Set<Lcom/android/providers/contacts/util/CursorCacheManager$UniqCacheKey;>;"
    :cond_1
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_1
    :try_start_1
    invoke-virtual {v4}, Ljava/util/ArrayList;->size()I

    move-result v6

    if-ge v2, v6, :cond_2

    .line 205
    iget-object v6, p0, Lcom/android/providers/contacts/util/CursorCacheManager;->mCache:Ljava/util/Map;

    invoke-virtual {v4, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v8

    invoke-interface {v6, v8}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 204
    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    .line 207
    :cond_2
    invoke-virtual {v4}, Ljava/util/ArrayList;->size()I

    move-result v6

    if-lez v6, :cond_3

    .line 208
    const-string v6, "ContactsProvider_CursorCache"

    const-string v8, "Invalidated All"

    invoke-static {v6, v8}, Landroid/util/secutil/Log;->secI(Ljava/lang/String;Ljava/lang/String;)I

    .line 209
    :cond_3
    monitor-exit v7
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 211
    return-void
.end method

.method public put(Ljava/lang/Integer;Landroid/database/Cursor;Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)V
    .locals 11
    .param p1, "uriCode"    # Ljava/lang/Integer;
    .param p2, "c"    # Landroid/database/Cursor;
    .param p3, "queryUri"    # Landroid/net/Uri;
    .param p4, "projection"    # [Ljava/lang/String;
    .param p5, "selection"    # Ljava/lang/String;
    .param p6, "selectionArgs"    # [Ljava/lang/String;
    .param p7, "sortOrder"    # Ljava/lang/String;

    .prologue
    .line 155
    if-nez p5, :cond_0

    invoke-direct {p0, p1}, Lcom/android/providers/contacts/util/CursorCacheManager;->isCachedUri(Ljava/lang/Integer;)Z

    move-result v0

    if-eqz v0, :cond_0

    if-nez p6, :cond_0

    if-nez p7, :cond_0

    instance-of v0, p2, Landroid/database/MatrixCursor;

    if-eqz v0, :cond_1

    .line 187
    :cond_0
    :goto_0
    return-void

    .line 159
    :cond_1
    const/4 v8, 0x0

    .line 162
    .local v8, "needToBuildMatrixCursor":Z
    iget-object v10, p0, Lcom/android/providers/contacts/util/CursorCacheManager;->mCache:Ljava/util/Map;

    monitor-enter v10

    move-object v0, p0

    move-object v1, p3

    move-object v2, p4

    move-object/from16 v3, p5

    move-object/from16 v4, p6

    move-object/from16 v5, p7

    .line 163
    :try_start_0
    invoke-direct/range {v0 .. v5}, Lcom/android/providers/contacts/util/CursorCacheManager;->buildCacheKey(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    .line 164
    .local v6, "key":Ljava/lang/String;
    invoke-static {p1, v6}, Lcom/android/providers/contacts/util/CursorCacheManager$UniqCacheKey;->getInstanceWithKey(Ljava/lang/Integer;Ljava/lang/String;)Lcom/android/providers/contacts/util/CursorCacheManager$UniqCacheKey;

    move-result-object v9

    .line 165
    .local v9, "uKey":Lcom/android/providers/contacts/util/CursorCacheManager$UniqCacheKey;
    iget-object v0, p0, Lcom/android/providers/contacts/util/CursorCacheManager;->mCache:Ljava/util/Map;

    invoke-interface {v0, v9}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_5

    const/4 v8, 0x1

    .line 166
    :goto_1
    monitor-exit v10
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 167
    const/4 v7, 0x0

    .line 168
    .local v7, "mc":Landroid/database/MatrixCursor;
    if-eqz v8, :cond_2

    .line 169
    invoke-direct {p0, p2}, Lcom/android/providers/contacts/util/CursorCacheManager;->cloneCursor(Landroid/database/Cursor;)Landroid/database/MatrixCursor;

    move-result-object v7

    .line 170
    :cond_2
    iget-object v1, p0, Lcom/android/providers/contacts/util/CursorCacheManager;->mCache:Ljava/util/Map;

    monitor-enter v1

    .line 171
    if-eqz v7, :cond_3

    :try_start_1
    iget-object v0, p0, Lcom/android/providers/contacts/util/CursorCacheManager;->mCache:Ljava/util/Map;

    invoke-interface {v0, v9}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_3

    .line 172
    iget-object v0, p0, Lcom/android/providers/contacts/util/CursorCacheManager;->mCache:Ljava/util/Map;

    invoke-interface {v0, v9, v7}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 180
    :cond_3
    iget-object v0, p0, Lcom/android/providers/contacts/util/CursorCacheManager;->mCache:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->size()I

    move-result v0

    const/4 v2, 0x6

    if-le v0, v2, :cond_4

    .line 181
    invoke-virtual {p0}, Lcom/android/providers/contacts/util/CursorCacheManager;->invalidateAll()V

    .line 182
    invoke-virtual {p0}, Lcom/android/providers/contacts/util/CursorCacheManager;->disable()V

    .line 183
    const-string v0, "ContactsProvider_CursorCache"

    const-string v2, "ERROR : Due to mCache size() > 6 ,So CursorCacheManager is disabled. FATAL EXCEPTION"

    invoke-static {v0, v2}, Landroid/util/secutil/Log;->secW(Ljava/lang/String;Ljava/lang/String;)I

    .line 186
    :cond_4
    monitor-exit v1

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0

    .line 165
    .end local v7    # "mc":Landroid/database/MatrixCursor;
    :cond_5
    const/4 v8, 0x0

    goto :goto_1

    .line 166
    .end local v6    # "key":Ljava/lang/String;
    .end local v9    # "uKey":Lcom/android/providers/contacts/util/CursorCacheManager$UniqCacheKey;
    :catchall_1
    move-exception v0

    :try_start_2
    monitor-exit v10
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method

.method public registerUriCache(Ljava/lang/Integer;Ljava/util/HashSet;)V
    .locals 1
    .param p1, "allowCacheUri"    # Ljava/lang/Integer;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Integer;",
            "Ljava/util/HashSet",
            "<",
            "Ljava/lang/Integer;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 96
    .local p2, "ignoreUpdateUri":Ljava/util/HashSet;, "Ljava/util/HashSet<Ljava/lang/Integer;>;"
    if-eqz p1, :cond_0

    if-nez p2, :cond_1

    .line 97
    :cond_0
    new-instance v0, Ljava/lang/IllegalArgumentException;

    invoke-direct {v0}, Ljava/lang/IllegalArgumentException;-><init>()V

    throw v0

    .line 99
    :cond_1
    sget-boolean v0, Lcom/android/providers/contacts/util/CursorCacheManager;->mDisabled:Z

    if-eqz v0, :cond_3

    .line 104
    :cond_2
    :goto_0
    return-void

    .line 102
    :cond_3
    iget-object v0, p0, Lcom/android/providers/contacts/util/CursorCacheManager;->mCachedQueryUri:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_2

    .line 103
    iget-object v0, p0, Lcom/android/providers/contacts/util/CursorCacheManager;->mCachedQueryUri:Ljava/util/Map;

    invoke-interface {v0, p1, p2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0
.end method
