.class Lcom/android/providers/contacts/util/FbHighQualityImageDownload$QueueItem;
.super Ljava/lang/Object;
.source "FbHighQualityImageDownload.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/providers/contacts/util/FbHighQualityImageDownload;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "QueueItem"
.end annotation


# instance fields
.field protected mFacbookId:Ljava/lang/String;

.field protected rawContactId:Ljava/lang/String;


# direct methods
.method public constructor <init>(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)V
    .locals 0
    .param p1, "c"    # Landroid/content/Context;
    .param p2, "rawContctId"    # Ljava/lang/String;
    .param p3, "fbId"    # Ljava/lang/String;

    .prologue
    .line 592
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 593
    iput-object p3, p0, Lcom/android/providers/contacts/util/FbHighQualityImageDownload$QueueItem;->mFacbookId:Ljava/lang/String;

    .line 594
    iput-object p2, p0, Lcom/android/providers/contacts/util/FbHighQualityImageDownload$QueueItem;->rawContactId:Ljava/lang/String;

    .line 595
    return-void
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 4
    .param p1, "obj"    # Ljava/lang/Object;

    .prologue
    const/4 v1, 0x0

    .line 598
    if-nez p1, :cond_1

    .line 608
    :cond_0
    :goto_0
    return v1

    .line 601
    :cond_1
    if-ne p1, p0, :cond_2

    .line 602
    const/4 v1, 0x1

    goto :goto_0

    .line 604
    :cond_2
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    if-ne v2, v3, :cond_0

    move-object v0, p1

    .line 607
    check-cast v0, Lcom/android/providers/contacts/util/FbHighQualityImageDownload$QueueItem;

    .line 608
    .local v0, "tmp":Lcom/android/providers/contacts/util/FbHighQualityImageDownload$QueueItem;
    invoke-virtual {v0}, Lcom/android/providers/contacts/util/FbHighQualityImageDownload$QueueItem;->getFbId()Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/android/providers/contacts/util/FbHighQualityImageDownload$QueueItem;->mFacbookId:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    goto :goto_0
.end method

.method public getFbId()Ljava/lang/String;
    .locals 1

    .prologue
    .line 617
    iget-object v0, p0, Lcom/android/providers/contacts/util/FbHighQualityImageDownload$QueueItem;->mFacbookId:Ljava/lang/String;

    return-object v0
.end method

.method public getRawContactId()Ljava/lang/String;
    .locals 1

    .prologue
    .line 621
    iget-object v0, p0, Lcom/android/providers/contacts/util/FbHighQualityImageDownload$QueueItem;->rawContactId:Ljava/lang/String;

    return-object v0
.end method

.method public hashCode()I
    .locals 1

    .prologue
    .line 613
    iget-object v0, p0, Lcom/android/providers/contacts/util/FbHighQualityImageDownload$QueueItem;->mFacbookId:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    return v0
.end method
