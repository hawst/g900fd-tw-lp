.class public Lcom/android/providers/contacts/ContactLocaleUtils;
.super Ljava/lang/Object;
.source "ContactLocaleUtils.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/android/providers/contacts/ContactLocaleUtils$TraditionalChineseTWContactUtils;,
        Lcom/android/providers/contacts/ContactLocaleUtils$TraditionalChineseHKContactUtils;,
        Lcom/android/providers/contacts/ContactLocaleUtils$SimplifiedChineseContactUtils;,
        Lcom/android/providers/contacts/ContactLocaleUtils$JapaneseContactUtils;,
        Lcom/android/providers/contacts/ContactLocaleUtils$ContactLocaleUtilsBase;
    }
.end annotation


# static fields
.field private static final JAPANESE_LANGUAGE:Ljava/lang/String;

.field private static final KOREAN_LANGUAGE:Ljava/lang/String;

.field public static final LOCALE_ARABIC:Ljava/util/Locale;

.field public static final LOCALE_GREEK:Ljava/util/Locale;

.field public static final LOCALE_HEBREW:Ljava/util/Locale;

.field public static final LOCALE_HINDI:Ljava/util/Locale;

.field public static final LOCALE_SERBIAN:Ljava/util/Locale;

.field public static final LOCALE_THAI:Ljava/util/Locale;

.field public static final LOCALE_UKRAINIAN:Ljava/util/Locale;

.field private static sSingleton:Lcom/android/providers/contacts/ContactLocaleUtils;


# instance fields
.field private final mLocales:Lcom/android/providers/contacts/LocaleSet;

.field private final mUtils:Lcom/android/providers/contacts/ContactLocaleUtils$ContactLocaleUtilsBase;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 57
    new-instance v0, Ljava/util/Locale;

    const-string v1, "ar"

    invoke-direct {v0, v1}, Ljava/util/Locale;-><init>(Ljava/lang/String;)V

    sput-object v0, Lcom/android/providers/contacts/ContactLocaleUtils;->LOCALE_ARABIC:Ljava/util/Locale;

    .line 58
    new-instance v0, Ljava/util/Locale;

    const-string v1, "el"

    invoke-direct {v0, v1}, Ljava/util/Locale;-><init>(Ljava/lang/String;)V

    sput-object v0, Lcom/android/providers/contacts/ContactLocaleUtils;->LOCALE_GREEK:Ljava/util/Locale;

    .line 59
    new-instance v0, Ljava/util/Locale;

    const-string v1, "he"

    invoke-direct {v0, v1}, Ljava/util/Locale;-><init>(Ljava/lang/String;)V

    sput-object v0, Lcom/android/providers/contacts/ContactLocaleUtils;->LOCALE_HEBREW:Ljava/util/Locale;

    .line 61
    new-instance v0, Ljava/util/Locale;

    const-string v1, "sr"

    invoke-direct {v0, v1}, Ljava/util/Locale;-><init>(Ljava/lang/String;)V

    sput-object v0, Lcom/android/providers/contacts/ContactLocaleUtils;->LOCALE_SERBIAN:Ljava/util/Locale;

    .line 62
    new-instance v0, Ljava/util/Locale;

    const-string v1, "uk"

    invoke-direct {v0, v1}, Ljava/util/Locale;-><init>(Ljava/lang/String;)V

    sput-object v0, Lcom/android/providers/contacts/ContactLocaleUtils;->LOCALE_UKRAINIAN:Ljava/util/Locale;

    .line 63
    new-instance v0, Ljava/util/Locale;

    const-string v1, "th"

    invoke-direct {v0, v1}, Ljava/util/Locale;-><init>(Ljava/lang/String;)V

    sput-object v0, Lcom/android/providers/contacts/ContactLocaleUtils;->LOCALE_THAI:Ljava/util/Locale;

    .line 64
    new-instance v0, Ljava/util/Locale;

    const-string v1, "hi"

    invoke-direct {v0, v1}, Ljava/util/Locale;-><init>(Ljava/lang/String;)V

    sput-object v0, Lcom/android/providers/contacts/ContactLocaleUtils;->LOCALE_HINDI:Ljava/util/Locale;

    .line 766
    sget-object v0, Ljava/util/Locale;->JAPANESE:Ljava/util/Locale;

    invoke-virtual {v0}, Ljava/util/Locale;->getLanguage()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/android/providers/contacts/ContactLocaleUtils;->JAPANESE_LANGUAGE:Ljava/lang/String;

    .line 767
    sget-object v0, Ljava/util/Locale;->KOREAN:Ljava/util/Locale;

    invoke-virtual {v0}, Ljava/util/Locale;->getLanguage()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/android/providers/contacts/ContactLocaleUtils;->KOREAN_LANGUAGE:Ljava/lang/String;

    return-void
.end method

.method private constructor <init>(Lcom/android/providers/contacts/LocaleSet;)V
    .locals 3
    .param p1, "locales"    # Lcom/android/providers/contacts/LocaleSet;

    .prologue
    .line 778
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 779
    if-nez p1, :cond_0

    .line 780
    invoke-static {}, Lcom/android/providers/contacts/LocaleSet;->getDefault()Lcom/android/providers/contacts/LocaleSet;

    move-result-object v0

    iput-object v0, p0, Lcom/android/providers/contacts/ContactLocaleUtils;->mLocales:Lcom/android/providers/contacts/LocaleSet;

    .line 786
    :goto_0
    invoke-static {}, Lcom/android/providers/contacts/util/LoadCscFeatureUtils;->getInstance()Lcom/android/providers/contacts/util/LoadCscFeatureUtils;

    move-result-object v0

    invoke-virtual {v0}, Lcom/android/providers/contacts/util/LoadCscFeatureUtils;->getEnableSupportStrokeBPMF()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 787
    iget-object v0, p0, Lcom/android/providers/contacts/ContactLocaleUtils;->mLocales:Lcom/android/providers/contacts/LocaleSet;

    invoke-virtual {v0}, Lcom/android/providers/contacts/LocaleSet;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v1, "zh_TW"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 788
    new-instance v0, Lcom/android/providers/contacts/ContactLocaleUtils$TraditionalChineseTWContactUtils;

    iget-object v1, p0, Lcom/android/providers/contacts/ContactLocaleUtils;->mLocales:Lcom/android/providers/contacts/LocaleSet;

    invoke-direct {v0, v1}, Lcom/android/providers/contacts/ContactLocaleUtils$TraditionalChineseTWContactUtils;-><init>(Lcom/android/providers/contacts/LocaleSet;)V

    iput-object v0, p0, Lcom/android/providers/contacts/ContactLocaleUtils;->mUtils:Lcom/android/providers/contacts/ContactLocaleUtils$ContactLocaleUtilsBase;

    .line 806
    :goto_1
    const-string v0, "ContactLocale"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "AddressBook Labels ["

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/android/providers/contacts/ContactLocaleUtils;->mLocales:Lcom/android/providers/contacts/LocaleSet;

    invoke-virtual {v2}, Lcom/android/providers/contacts/LocaleSet;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "]: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Lcom/android/providers/contacts/ContactLocaleUtils;->getLabels()Ljava/util/ArrayList;

    move-result-object v2

    invoke-virtual {v2}, Ljava/util/ArrayList;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->secI(Ljava/lang/String;Ljava/lang/String;)I

    .line 808
    return-void

    .line 782
    :cond_0
    iput-object p1, p0, Lcom/android/providers/contacts/ContactLocaleUtils;->mLocales:Lcom/android/providers/contacts/LocaleSet;

    goto :goto_0

    .line 790
    :cond_1
    new-instance v0, Lcom/android/providers/contacts/ContactLocaleUtils$TraditionalChineseHKContactUtils;

    iget-object v1, p0, Lcom/android/providers/contacts/ContactLocaleUtils;->mLocales:Lcom/android/providers/contacts/LocaleSet;

    invoke-direct {v0, p0, v1}, Lcom/android/providers/contacts/ContactLocaleUtils$TraditionalChineseHKContactUtils;-><init>(Lcom/android/providers/contacts/ContactLocaleUtils;Lcom/android/providers/contacts/LocaleSet;)V

    iput-object v0, p0, Lcom/android/providers/contacts/ContactLocaleUtils;->mUtils:Lcom/android/providers/contacts/ContactLocaleUtils$ContactLocaleUtilsBase;

    goto :goto_1

    .line 792
    :cond_2
    iget-object v0, p0, Lcom/android/providers/contacts/ContactLocaleUtils;->mLocales:Lcom/android/providers/contacts/LocaleSet;

    invoke-virtual {v0}, Lcom/android/providers/contacts/LocaleSet;->isPrimaryLocaleSimplifiedChinese()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 793
    new-instance v0, Lcom/android/providers/contacts/ContactLocaleUtils$SimplifiedChineseContactUtils;

    iget-object v1, p0, Lcom/android/providers/contacts/ContactLocaleUtils;->mLocales:Lcom/android/providers/contacts/LocaleSet;

    invoke-direct {v0, v1}, Lcom/android/providers/contacts/ContactLocaleUtils$SimplifiedChineseContactUtils;-><init>(Lcom/android/providers/contacts/LocaleSet;)V

    iput-object v0, p0, Lcom/android/providers/contacts/ContactLocaleUtils;->mUtils:Lcom/android/providers/contacts/ContactLocaleUtils$ContactLocaleUtilsBase;

    goto :goto_1

    .line 794
    :cond_3
    invoke-static {}, Lcom/android/providers/contacts/util/LoadCscFeatureUtils;->getInstance()Lcom/android/providers/contacts/util/LoadCscFeatureUtils;

    move-result-object v0

    invoke-virtual {v0}, Lcom/android/providers/contacts/util/LoadCscFeatureUtils;->getEnableSupportFuzzySearch()Z

    move-result v0

    if-eqz v0, :cond_4

    .line 797
    new-instance v0, Lcom/android/providers/contacts/ContactLocaleUtils$SimplifiedChineseContactUtils;

    iget-object v1, p0, Lcom/android/providers/contacts/ContactLocaleUtils;->mLocales:Lcom/android/providers/contacts/LocaleSet;

    invoke-direct {v0, v1}, Lcom/android/providers/contacts/ContactLocaleUtils$SimplifiedChineseContactUtils;-><init>(Lcom/android/providers/contacts/LocaleSet;)V

    iput-object v0, p0, Lcom/android/providers/contacts/ContactLocaleUtils;->mUtils:Lcom/android/providers/contacts/ContactLocaleUtils$ContactLocaleUtilsBase;

    goto :goto_1

    .line 798
    :cond_4
    iget-object v0, p0, Lcom/android/providers/contacts/ContactLocaleUtils;->mLocales:Lcom/android/providers/contacts/LocaleSet;

    sget-object v1, Lcom/android/providers/contacts/ContactLocaleUtils;->JAPANESE_LANGUAGE:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/android/providers/contacts/LocaleSet;->isPrimaryLanguage(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_5

    iget-object v0, p0, Lcom/android/providers/contacts/ContactLocaleUtils;->mLocales:Lcom/android/providers/contacts/LocaleSet;

    sget-object v1, Lcom/android/providers/contacts/ContactLocaleUtils;->KOREAN_LANGUAGE:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/android/providers/contacts/LocaleSet;->isPrimaryLanguage(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_6

    invoke-static {}, Lcom/sec/android/app/CscFeature;->getInstance()Lcom/sec/android/app/CscFeature;

    move-result-object v0

    const-string v1, "CscFeature_Contact_EnableDocomoAccountAsDefault"

    invoke-virtual {v0, v1}, Lcom/sec/android/app/CscFeature;->getEnableStatus(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_5

    invoke-static {}, Lcom/sec/android/app/CscFeature;->getInstance()Lcom/sec/android/app/CscFeature;

    move-result-object v0

    const-string v1, "CscFeature_Contact_EnableUIM"

    invoke-virtual {v0, v1}, Lcom/sec/android/app/CscFeature;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    const-string v1, "KDDI"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_6

    .line 802
    :cond_5
    new-instance v0, Lcom/android/providers/contacts/ContactLocaleUtils$JapaneseContactUtils;

    iget-object v1, p0, Lcom/android/providers/contacts/ContactLocaleUtils;->mLocales:Lcom/android/providers/contacts/LocaleSet;

    invoke-direct {v0, v1}, Lcom/android/providers/contacts/ContactLocaleUtils$JapaneseContactUtils;-><init>(Lcom/android/providers/contacts/LocaleSet;)V

    iput-object v0, p0, Lcom/android/providers/contacts/ContactLocaleUtils;->mUtils:Lcom/android/providers/contacts/ContactLocaleUtils$ContactLocaleUtilsBase;

    goto/16 :goto_1

    .line 804
    :cond_6
    new-instance v0, Lcom/android/providers/contacts/ContactLocaleUtils$ContactLocaleUtilsBase;

    iget-object v1, p0, Lcom/android/providers/contacts/ContactLocaleUtils;->mLocales:Lcom/android/providers/contacts/LocaleSet;

    invoke-direct {v0, v1}, Lcom/android/providers/contacts/ContactLocaleUtils$ContactLocaleUtilsBase;-><init>(Lcom/android/providers/contacts/LocaleSet;)V

    iput-object v0, p0, Lcom/android/providers/contacts/ContactLocaleUtils;->mUtils:Lcom/android/providers/contacts/ContactLocaleUtils$ContactLocaleUtilsBase;

    goto/16 :goto_1
.end method

.method static synthetic access$000(Lcom/android/providers/contacts/ContactLocaleUtils;)Lcom/android/providers/contacts/LocaleSet;
    .locals 1
    .param p0, "x0"    # Lcom/android/providers/contacts/ContactLocaleUtils;

    .prologue
    .line 54
    iget-object v0, p0, Lcom/android/providers/contacts/ContactLocaleUtils;->mLocales:Lcom/android/providers/contacts/LocaleSet;

    return-object v0
.end method

.method private buildMultiPinyinArrayList([[Ljava/lang/String;I)Ljava/util/ArrayList;
    .locals 3
    .param p1, "array"    # [[Ljava/lang/String;
    .param p2, "arrayLength"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "([[",
            "Ljava/lang/String;",
            "I)",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 944
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 945
    .local v1, "mArray":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    add-int/lit8 v0, p2, -0x1

    .local v0, "i":I
    :goto_0
    if-ltz v0, :cond_0

    .line 946
    aget-object v2, p1, v0

    invoke-virtual {p0, v2, v1}, Lcom/android/providers/contacts/ContactLocaleUtils;->aggregateMultiPinyins([Ljava/lang/String;Ljava/util/ArrayList;)Ljava/util/ArrayList;

    move-result-object v1

    .line 945
    add-int/lit8 v0, v0, -0x1

    goto :goto_0

    .line 948
    :cond_0
    return-object v1
.end method

.method private getArrayListWithLastSuffix(Ljava/util/ArrayList;Ljava/lang/String;)Ljava/util/ArrayList;
    .locals 3
    .param p2, "suffix"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;",
            "Ljava/lang/String;",
            ")",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 973
    .local p1, "array":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    invoke-virtual {p1}, Ljava/util/ArrayList;->size()I

    move-result v1

    if-ge v0, v1, :cond_0

    .line 974
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p1, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Ljava/util/ArrayList;->set(ILjava/lang/Object;)Ljava/lang/Object;

    .line 973
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 976
    :cond_0
    return-object p1
.end method

.method public static declared-synchronized getInstance()Lcom/android/providers/contacts/ContactLocaleUtils;
    .locals 3

    .prologue
    .line 816
    const-class v1, Lcom/android/providers/contacts/ContactLocaleUtils;

    monitor-enter v1

    :try_start_0
    sget-object v0, Lcom/android/providers/contacts/ContactLocaleUtils;->sSingleton:Lcom/android/providers/contacts/ContactLocaleUtils;

    if-nez v0, :cond_0

    .line 817
    new-instance v0, Lcom/android/providers/contacts/ContactLocaleUtils;

    invoke-static {}, Lcom/android/providers/contacts/LocaleSet;->getDefault()Lcom/android/providers/contacts/LocaleSet;

    move-result-object v2

    invoke-direct {v0, v2}, Lcom/android/providers/contacts/ContactLocaleUtils;-><init>(Lcom/android/providers/contacts/LocaleSet;)V

    sput-object v0, Lcom/android/providers/contacts/ContactLocaleUtils;->sSingleton:Lcom/android/providers/contacts/ContactLocaleUtils;

    .line 819
    :cond_0
    sget-object v0, Lcom/android/providers/contacts/ContactLocaleUtils;->sSingleton:Lcom/android/providers/contacts/ContactLocaleUtils;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit v1

    return-object v0

    .line 816
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method private getMultiPinYinWithPrefixWithoutTokenSource([Ljava/lang/String;Ljava/lang/String;)[Ljava/lang/String;
    .locals 4
    .param p1, "array"    # [Ljava/lang/String;
    .param p2, "prefix"    # Ljava/lang/String;

    .prologue
    .line 957
    array-length v0, p1

    .line 958
    .local v0, "arraySize":I
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    if-ge v1, v0, :cond_0

    .line 959
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    .line 960
    .local v2, "sb":Ljava/lang/StringBuilder;
    invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 961
    const/16 v3, 0x7c

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 962
    aget-object v3, p1, v1

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 963
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    aput-object v3, p1, v1

    .line 958
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 965
    .end local v2    # "sb":Ljava/lang/StringBuilder;
    :cond_0
    return-object p1
.end method

.method public static declared-synchronized setLocale(Ljava/util/Locale;)V
    .locals 2
    .param p0, "locale"    # Ljava/util/Locale;
    .annotation build Lcom/google/common/annotations/VisibleForTesting;
    .end annotation

    .prologue
    .line 824
    const-class v1, Lcom/android/providers/contacts/ContactLocaleUtils;

    monitor-enter v1

    :try_start_0
    new-instance v0, Lcom/android/providers/contacts/LocaleSet;

    invoke-direct {v0, p0}, Lcom/android/providers/contacts/LocaleSet;-><init>(Ljava/util/Locale;)V

    invoke-static {v0}, Lcom/android/providers/contacts/ContactLocaleUtils;->setLocales(Lcom/android/providers/contacts/LocaleSet;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 825
    monitor-exit v1

    return-void

    .line 824
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method public static declared-synchronized setLocales(Lcom/android/providers/contacts/LocaleSet;)V
    .locals 2
    .param p0, "locales"    # Lcom/android/providers/contacts/LocaleSet;

    .prologue
    .line 828
    const-class v1, Lcom/android/providers/contacts/ContactLocaleUtils;

    monitor-enter v1

    :try_start_0
    sget-object v0, Lcom/android/providers/contacts/ContactLocaleUtils;->sSingleton:Lcom/android/providers/contacts/ContactLocaleUtils;

    if-eqz v0, :cond_0

    sget-object v0, Lcom/android/providers/contacts/ContactLocaleUtils;->sSingleton:Lcom/android/providers/contacts/ContactLocaleUtils;

    invoke-virtual {v0, p0}, Lcom/android/providers/contacts/ContactLocaleUtils;->isLocale(Lcom/android/providers/contacts/LocaleSet;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 829
    :cond_0
    new-instance v0, Lcom/android/providers/contacts/ContactLocaleUtils;

    invoke-direct {v0, p0}, Lcom/android/providers/contacts/ContactLocaleUtils;-><init>(Lcom/android/providers/contacts/LocaleSet;)V

    sput-object v0, Lcom/android/providers/contacts/ContactLocaleUtils;->sSingleton:Lcom/android/providers/contacts/ContactLocaleUtils;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 831
    :cond_1
    monitor-exit v1

    return-void

    .line 828
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method


# virtual methods
.method public aggregateMultiPinyins([Ljava/lang/String;Ljava/util/ArrayList;)Ljava/util/ArrayList;
    .locals 8
    .param p1, "arrayA"    # [Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "([",
            "Ljava/lang/String;",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;)",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 918
    .local p2, "arrayB":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    array-length v3, p1

    .line 919
    .local v3, "lengthAA":I
    invoke-virtual {p2}, Ljava/util/ArrayList;->size()I

    move-result v4

    .line 921
    .local v4, "lengthBB":I
    new-instance v5, Ljava/util/ArrayList;

    invoke-direct {v5}, Ljava/util/ArrayList;-><init>()V

    .line 922
    .local v5, "mArray":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    if-lez v3, :cond_2

    .line 923
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    if-ge v1, v3, :cond_2

    .line 924
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 925
    .local v0, "builder":Ljava/lang/StringBuilder;
    aget-object v6, p1, v1

    invoke-virtual {v0, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 926
    const/16 v6, 0x7c

    invoke-virtual {v0, v6}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 927
    if-lez v4, :cond_0

    .line 928
    const/4 v2, 0x0

    .local v2, "j":I
    :goto_1
    if-ge v2, v4, :cond_1

    .line 929
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {p2, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Ljava/lang/String;

    invoke-virtual {v7, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 928
    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    .line 932
    .end local v2    # "j":I
    :cond_0
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 923
    :cond_1
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 936
    .end local v0    # "builder":Ljava/lang/StringBuilder;
    .end local v1    # "i":I
    :cond_2
    return-object v5
.end method

.method public getBucketIndex(Ljava/lang/String;)I
    .locals 1
    .param p1, "name"    # Ljava/lang/String;

    .prologue
    .line 864
    iget-object v0, p0, Lcom/android/providers/contacts/ContactLocaleUtils;->mUtils:Lcom/android/providers/contacts/ContactLocaleUtils$ContactLocaleUtilsBase;

    invoke-virtual {v0, p1}, Lcom/android/providers/contacts/ContactLocaleUtils$ContactLocaleUtilsBase;->getBucketIndex(Ljava/lang/String;)I

    move-result v0

    return v0
.end method

.method public getBucketLabel(I)Ljava/lang/String;
    .locals 1
    .param p1, "bucketIndex"    # I

    .prologue
    .line 872
    iget-object v0, p0, Lcom/android/providers/contacts/ContactLocaleUtils;->mUtils:Lcom/android/providers/contacts/ContactLocaleUtils$ContactLocaleUtilsBase;

    invoke-virtual {v0, p1}, Lcom/android/providers/contacts/ContactLocaleUtils$ContactLocaleUtilsBase;->getBucketLabel(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getLabels()Ljava/util/ArrayList;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 880
    iget-object v0, p0, Lcom/android/providers/contacts/ContactLocaleUtils;->mUtils:Lcom/android/providers/contacts/ContactLocaleUtils$ContactLocaleUtilsBase;

    invoke-virtual {v0}, Lcom/android/providers/contacts/ContactLocaleUtils$ContactLocaleUtilsBase;->getLabels()Ljava/util/ArrayList;

    move-result-object v0

    return-object v0
.end method

.method public getMultiPinyinsForName(Ljava/lang/String;)Ljava/util/ArrayList;
    .locals 12
    .param p1, "name"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 984
    const/4 v0, 0x3

    .line 986
    .local v0, "MAX_MUTI_PINYIN_COUNT":I
    invoke-static {}, Lcom/android/providers/contacts/HanziToMultiPinyin;->getInstance()Lcom/android/providers/contacts/HanziToMultiPinyin;

    move-result-object v10

    invoke-virtual {v10, p1}, Lcom/android/providers/contacts/HanziToMultiPinyin;->get(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v9

    .line 987
    .local v9, "tokens":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/android/providers/contacts/HanziToPinyin$Token;>;"
    const/4 v10, 0x3

    new-array v2, v10, [[Ljava/lang/String;

    .line 989
    .local v2, "arrayB":[[Ljava/lang/String;
    if-eqz v9, :cond_7

    invoke-virtual {v9}, Ljava/util/ArrayList;->size()I

    move-result v10

    if-lez v10, :cond_7

    .line 990
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    .line 991
    .local v6, "sb":Ljava/lang/StringBuilder;
    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    .line 993
    .local v7, "sb2":Ljava/lang/StringBuilder;
    const/4 v4, 0x0

    .line 994
    .local v4, "count":I
    invoke-virtual {v9}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v5

    .local v5, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v10

    if-eqz v10, :cond_5

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Lcom/android/providers/contacts/HanziToPinyin$Token;

    .line 995
    .local v8, "token":Lcom/android/providers/contacts/HanziToPinyin$Token;
    const/4 v10, 0x2

    iget v11, v8, Lcom/android/providers/contacts/HanziToPinyin$Token;->type:I

    if-ne v10, v11, :cond_3

    .line 996
    invoke-virtual {v6}, Ljava/lang/StringBuilder;->length()I

    move-result v10

    if-lez v10, :cond_0

    .line 997
    const/16 v10, 0x7c

    invoke-virtual {v6, v10}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 998
    const/16 v10, 0x7c

    invoke-virtual {v7, v10}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 1001
    :cond_0
    iget-object v10, v8, Lcom/android/providers/contacts/HanziToPinyin$Token;->target:Ljava/lang/String;

    const-string v11, ","

    invoke-virtual {v10, v11}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v1

    .line 1002
    .local v1, "arrayA":[Ljava/lang/String;
    array-length v10, v1

    const/4 v11, 0x1

    if-gt v10, v11, :cond_1

    .line 1003
    iget-object v10, v8, Lcom/android/providers/contacts/HanziToPinyin$Token;->target:Ljava/lang/String;

    invoke-virtual {v6, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1004
    iget-object v10, v8, Lcom/android/providers/contacts/HanziToPinyin$Token;->target:Ljava/lang/String;

    invoke-virtual {v7, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1016
    :goto_1
    const/16 v10, 0x7c

    invoke-virtual {v6, v10}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 1017
    iget-object v10, v8, Lcom/android/providers/contacts/HanziToPinyin$Token;->source:Ljava/lang/String;

    invoke-virtual {v6, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_0

    .line 1006
    :cond_1
    const/4 v10, 0x3

    if-ge v4, v10, :cond_2

    .line 1007
    const/4 v10, 0x0

    aget-object v10, v1, v10

    invoke-virtual {v6, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1008
    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-direct {p0, v1, v10}, Lcom/android/providers/contacts/ContactLocaleUtils;->getMultiPinYinWithPrefixWithoutTokenSource([Ljava/lang/String;Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v10

    aput-object v10, v2, v4

    .line 1009
    new-instance v7, Ljava/lang/StringBuilder;

    .end local v7    # "sb2":Ljava/lang/StringBuilder;
    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    .line 1010
    .restart local v7    # "sb2":Ljava/lang/StringBuilder;
    add-int/lit8 v4, v4, 0x1

    goto :goto_1

    .line 1012
    :cond_2
    const/4 v10, 0x0

    aget-object v10, v1, v10

    invoke-virtual {v6, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1013
    const/4 v10, 0x0

    aget-object v10, v1, v10

    invoke-virtual {v7, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_1

    .line 1019
    .end local v1    # "arrayA":[Ljava/lang/String;
    :cond_3
    invoke-virtual {v6}, Ljava/lang/StringBuilder;->length()I

    move-result v10

    if-lez v10, :cond_4

    .line 1020
    const/16 v10, 0x7c

    invoke-virtual {v6, v10}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 1022
    :cond_4
    iget-object v10, v8, Lcom/android/providers/contacts/HanziToPinyin$Token;->source:Ljava/lang/String;

    invoke-virtual {v6, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_0

    .line 1026
    .end local v8    # "token":Lcom/android/providers/contacts/HanziToPinyin$Token;
    :cond_5
    array-length v10, v2

    if-lez v10, :cond_6

    .line 1027
    invoke-direct {p0, v2, v4}, Lcom/android/providers/contacts/ContactLocaleUtils;->buildMultiPinyinArrayList([[Ljava/lang/String;I)Ljava/util/ArrayList;

    move-result-object v3

    .line 1028
    .local v3, "arrayC":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-direct {p0, v3, v10}, Lcom/android/providers/contacts/ContactLocaleUtils;->getArrayListWithLastSuffix(Ljava/util/ArrayList;Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v3

    .line 1036
    .end local v4    # "count":I
    .end local v5    # "i$":Ljava/util/Iterator;
    .end local v6    # "sb":Ljava/lang/StringBuilder;
    .end local v7    # "sb2":Ljava/lang/StringBuilder;
    :goto_2
    return-object v3

    .line 1031
    .end local v3    # "arrayC":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    .restart local v4    # "count":I
    .restart local v5    # "i$":Ljava/util/Iterator;
    .restart local v6    # "sb":Ljava/lang/StringBuilder;
    .restart local v7    # "sb2":Ljava/lang/StringBuilder;
    :cond_6
    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    .line 1032
    .restart local v3    # "arrayC":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v3, v10}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_2

    .line 1035
    .end local v3    # "arrayC":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    .end local v4    # "count":I
    .end local v5    # "i$":Ljava/util/Iterator;
    .end local v6    # "sb":Ljava/lang/StringBuilder;
    .end local v7    # "sb2":Ljava/lang/StringBuilder;
    :cond_7
    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    .line 1036
    .restart local v3    # "arrayC":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    goto :goto_2
.end method

.method public getNameLookupKeys(Ljava/lang/String;I)Ljava/util/Iterator;
    .locals 1
    .param p1, "name"    # Ljava/lang/String;
    .param p2, "nameStyle"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "I)",
            "Ljava/util/Iterator",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 896
    iget-object v0, p0, Lcom/android/providers/contacts/ContactLocaleUtils;->mLocales:Lcom/android/providers/contacts/LocaleSet;

    invoke-virtual {v0}, Lcom/android/providers/contacts/LocaleSet;->isPrimaryLocaleCJK()Z

    move-result v0

    if-nez v0, :cond_2

    .line 897
    iget-object v0, p0, Lcom/android/providers/contacts/ContactLocaleUtils;->mLocales:Lcom/android/providers/contacts/LocaleSet;

    invoke-virtual {v0}, Lcom/android/providers/contacts/LocaleSet;->isSecondaryLocaleSimplifiedChinese()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 898
    const/4 v0, 0x3

    if-eq p2, v0, :cond_0

    const/4 v0, 0x2

    if-ne p2, v0, :cond_2

    .line 900
    :cond_0
    invoke-static {p1}, Lcom/android/providers/contacts/ContactLocaleUtils$SimplifiedChineseContactUtils;->getPinyinNameLookupKeys(Ljava/lang/String;)Ljava/util/Iterator;

    move-result-object v0

    .line 908
    :goto_0
    return-object v0

    .line 903
    :cond_1
    const/4 v0, 0x4

    if-ne p2, v0, :cond_2

    invoke-static {}, Lcom/android/providers/contacts/util/LoadCscFeatureUtils;->getInstance()Lcom/android/providers/contacts/util/LoadCscFeatureUtils;

    move-result-object v0

    invoke-virtual {v0}, Lcom/android/providers/contacts/util/LoadCscFeatureUtils;->getEnableSupportStrokeBPMF()Z

    move-result v0

    if-nez v0, :cond_2

    .line 904
    invoke-static {p1}, Lcom/android/providers/contacts/ContactLocaleUtils$JapaneseContactUtils;->getRomajiNameLookupKeys(Ljava/lang/String;)Ljava/util/Iterator;

    move-result-object v0

    goto :goto_0

    .line 908
    :cond_2
    iget-object v0, p0, Lcom/android/providers/contacts/ContactLocaleUtils;->mUtils:Lcom/android/providers/contacts/ContactLocaleUtils$ContactLocaleUtilsBase;

    invoke-virtual {v0, p1, p2}, Lcom/android/providers/contacts/ContactLocaleUtils$ContactLocaleUtilsBase;->getNameLookupKeys(Ljava/lang/String;I)Ljava/util/Iterator;

    move-result-object v0

    goto :goto_0
.end method

.method public getSortKey(Ljava/lang/String;I)Ljava/lang/String;
    .locals 7
    .param p1, "name"    # Ljava/lang/String;
    .param p2, "nameStyle"    # I

    .prologue
    const/16 v6, 0x20

    .line 834
    const/4 v4, 0x3

    if-ne p2, v4, :cond_5

    iget-object v4, p0, Lcom/android/providers/contacts/ContactLocaleUtils;->mLocales:Lcom/android/providers/contacts/LocaleSet;

    invoke-virtual {v4}, Lcom/android/providers/contacts/LocaleSet;->toString()Ljava/lang/String;

    move-result-object v4

    const-string v5, "zh_HK"

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_5

    iget-object v4, p0, Lcom/android/providers/contacts/ContactLocaleUtils;->mLocales:Lcom/android/providers/contacts/LocaleSet;

    invoke-virtual {v4}, Lcom/android/providers/contacts/LocaleSet;->toString()Ljava/lang/String;

    move-result-object v4

    const-string v5, "zh_TW"

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_5

    .line 836
    invoke-static {}, Lcom/android/providers/contacts/HanziToPinyin;->getInstance()Lcom/android/providers/contacts/HanziToPinyin;

    move-result-object v4

    invoke-virtual {v4, p1}, Lcom/android/providers/contacts/HanziToPinyin;->getTokens(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v3

    .line 837
    .local v3, "tokens":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/android/providers/contacts/HanziToPinyin$Token;>;"
    if-eqz v3, :cond_4

    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    move-result v4

    if-lez v4, :cond_4

    .line 838
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    .line 839
    .local v1, "sb":Ljava/lang/StringBuilder;
    invoke-virtual {v3}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_3

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/android/providers/contacts/HanziToPinyin$Token;

    .line 842
    .local v2, "token":Lcom/android/providers/contacts/HanziToPinyin$Token;
    const/4 v4, 0x2

    iget v5, v2, Lcom/android/providers/contacts/HanziToPinyin$Token;->type:I

    if-ne v4, v5, :cond_1

    .line 843
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->length()I

    move-result v4

    if-lez v4, :cond_0

    .line 844
    invoke-virtual {v1, v6}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 846
    :cond_0
    iget-object v4, v2, Lcom/android/providers/contacts/HanziToPinyin$Token;->target:Ljava/lang/String;

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 847
    invoke-virtual {v1, v6}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 848
    iget-object v4, v2, Lcom/android/providers/contacts/HanziToPinyin$Token;->source:Ljava/lang/String;

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_0

    .line 850
    :cond_1
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->length()I

    move-result v4

    if-lez v4, :cond_2

    .line 851
    invoke-virtual {v1, v6}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 853
    :cond_2
    iget-object v4, v2, Lcom/android/providers/contacts/HanziToPinyin$Token;->source:Ljava/lang/String;

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_0

    .line 856
    .end local v2    # "token":Lcom/android/providers/contacts/HanziToPinyin$Token;
    :cond_3
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    .end local v0    # "i$":Ljava/util/Iterator;
    .end local v1    # "sb":Ljava/lang/StringBuilder;
    :cond_4
    move-object v4, p1

    .line 860
    .end local v3    # "tokens":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/android/providers/contacts/HanziToPinyin$Token;>;"
    :goto_1
    return-object v4

    :cond_5
    iget-object v4, p0, Lcom/android/providers/contacts/ContactLocaleUtils;->mUtils:Lcom/android/providers/contacts/ContactLocaleUtils$ContactLocaleUtilsBase;

    invoke-virtual {v4, p1}, Lcom/android/providers/contacts/ContactLocaleUtils$ContactLocaleUtilsBase;->getSortKey(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    goto :goto_1
.end method

.method public isLocale(Lcom/android/providers/contacts/LocaleSet;)Z
    .locals 1
    .param p1, "locales"    # Lcom/android/providers/contacts/LocaleSet;

    .prologue
    .line 812
    iget-object v0, p0, Lcom/android/providers/contacts/ContactLocaleUtils;->mLocales:Lcom/android/providers/contacts/LocaleSet;

    invoke-virtual {v0, p1}, Lcom/android/providers/contacts/LocaleSet;->equals(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method
