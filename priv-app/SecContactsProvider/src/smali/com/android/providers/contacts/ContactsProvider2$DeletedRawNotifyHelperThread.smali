.class Lcom/android/providers/contacts/ContactsProvider2$DeletedRawNotifyHelperThread;
.super Ljava/lang/Thread;
.source "ContactsProvider2.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/providers/contacts/ContactsProvider2;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "DeletedRawNotifyHelperThread"
.end annotation


# static fields
.field private static MAX_CALENDAR_PROVIDER_OPERATION_RETRY:I

.field private static isRunning:Z

.field private static mBackService:Lcom/android/providers/contacts/ContactsProvider2$DeletedRawNotifyHelperThread;


# instance fields
.field private mContext:Landroid/content/Context;

.field private mQueue:Ljava/util/concurrent/ArrayBlockingQueue;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/concurrent/ArrayBlockingQueue",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private mTotalWaitingTime:J


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 3910
    const/4 v0, 0x0

    sput-object v0, Lcom/android/providers/contacts/ContactsProvider2$DeletedRawNotifyHelperThread;->mBackService:Lcom/android/providers/contacts/ContactsProvider2$DeletedRawNotifyHelperThread;

    .line 3912
    const/4 v0, 0x0

    sput-boolean v0, Lcom/android/providers/contacts/ContactsProvider2$DeletedRawNotifyHelperThread;->isRunning:Z

    .line 3920
    const/4 v0, 0x5

    sput v0, Lcom/android/providers/contacts/ContactsProvider2$DeletedRawNotifyHelperThread;->MAX_CALENDAR_PROVIDER_OPERATION_RETRY:I

    return-void
.end method

.method private constructor <init>(Landroid/content/Context;)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 3930
    const-string v0, "DeletedRawNotifyHelperThread"

    invoke-direct {p0, v0}, Ljava/lang/Thread;-><init>(Ljava/lang/String;)V

    .line 3922
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/android/providers/contacts/ContactsProvider2$DeletedRawNotifyHelperThread;->mContext:Landroid/content/Context;

    .line 3924
    new-instance v0, Ljava/util/concurrent/ArrayBlockingQueue;

    const/16 v1, 0x64

    invoke-direct {v0, v1}, Ljava/util/concurrent/ArrayBlockingQueue;-><init>(I)V

    iput-object v0, p0, Lcom/android/providers/contacts/ContactsProvider2$DeletedRawNotifyHelperThread;->mQueue:Ljava/util/concurrent/ArrayBlockingQueue;

    .line 3926
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/android/providers/contacts/ContactsProvider2$DeletedRawNotifyHelperThread;->mTotalWaitingTime:J

    .line 3932
    iput-object p1, p0, Lcom/android/providers/contacts/ContactsProvider2$DeletedRawNotifyHelperThread;->mContext:Landroid/content/Context;

    .line 3933
    return-void
.end method

.method private declared-synchronized putIds(Ljava/lang/String;)V
    .locals 2
    .param p1, "ids"    # Ljava/lang/String;

    .prologue
    .line 3956
    monitor-enter p0

    :try_start_0
    iget-object v1, p0, Lcom/android/providers/contacts/ContactsProvider2$DeletedRawNotifyHelperThread;->mQueue:Ljava/util/concurrent/ArrayBlockingQueue;

    invoke-virtual {v1, p1}, Ljava/util/concurrent/ArrayBlockingQueue;->put(Ljava/lang/Object;)V
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 3960
    :goto_0
    monitor-exit p0

    return-void

    .line 3957
    :catch_0
    move-exception v0

    .line 3958
    .local v0, "e":Ljava/lang/InterruptedException;
    :try_start_1
    invoke-virtual {v0}, Ljava/lang/InterruptedException;->printStackTrace()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 3956
    .end local v0    # "e":Ljava/lang/InterruptedException;
    :catchall_0
    move-exception v1

    monitor-exit p0

    throw v1
.end method

.method public static declared-synchronized runBackThread(Landroid/content/Context;Ljava/lang/String;)V
    .locals 3
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "ids"    # Ljava/lang/String;

    .prologue
    .line 3936
    const-class v1, Lcom/android/providers/contacts/ContactsProvider2$DeletedRawNotifyHelperThread;

    monitor-enter v1

    :try_start_0
    sget-boolean v0, Lcom/android/providers/contacts/ContactsProvider2$DeletedRawNotifyHelperThread;->isRunning:Z

    if-nez v0, :cond_0

    .line 3937
    const-string v0, "ContactsProvider"

    const-string v2, "Create DeletedRawNotifyHelperThread"

    invoke-static {v0, v2}, Landroid/util/secutil/Log;->secI(Ljava/lang/String;Ljava/lang/String;)I

    .line 3938
    new-instance v0, Lcom/android/providers/contacts/ContactsProvider2$DeletedRawNotifyHelperThread;

    invoke-direct {v0, p0}, Lcom/android/providers/contacts/ContactsProvider2$DeletedRawNotifyHelperThread;-><init>(Landroid/content/Context;)V

    sput-object v0, Lcom/android/providers/contacts/ContactsProvider2$DeletedRawNotifyHelperThread;->mBackService:Lcom/android/providers/contacts/ContactsProvider2$DeletedRawNotifyHelperThread;

    .line 3940
    :cond_0
    sget-object v0, Lcom/android/providers/contacts/ContactsProvider2$DeletedRawNotifyHelperThread;->mBackService:Lcom/android/providers/contacts/ContactsProvider2$DeletedRawNotifyHelperThread;

    invoke-direct {v0, p0}, Lcom/android/providers/contacts/ContactsProvider2$DeletedRawNotifyHelperThread;->setContext(Landroid/content/Context;)V

    .line 3941
    sget-object v0, Lcom/android/providers/contacts/ContactsProvider2$DeletedRawNotifyHelperThread;->mBackService:Lcom/android/providers/contacts/ContactsProvider2$DeletedRawNotifyHelperThread;

    invoke-direct {v0, p1}, Lcom/android/providers/contacts/ContactsProvider2$DeletedRawNotifyHelperThread;->putIds(Ljava/lang/String;)V

    .line 3943
    sget-boolean v0, Lcom/android/providers/contacts/ContactsProvider2$DeletedRawNotifyHelperThread;->isRunning:Z

    if-nez v0, :cond_1

    .line 3944
    const-string v0, "ContactsProvider"

    const-string v2, "Start DeletedRawNotifyHelperThread"

    invoke-static {v0, v2}, Landroid/util/secutil/Log;->secI(Ljava/lang/String;Ljava/lang/String;)I

    .line 3945
    const/4 v0, 0x1

    sput-boolean v0, Lcom/android/providers/contacts/ContactsProvider2$DeletedRawNotifyHelperThread;->isRunning:Z

    .line 3946
    sget-object v0, Lcom/android/providers/contacts/ContactsProvider2$DeletedRawNotifyHelperThread;->mBackService:Lcom/android/providers/contacts/ContactsProvider2$DeletedRawNotifyHelperThread;

    invoke-virtual {v0}, Lcom/android/providers/contacts/ContactsProvider2$DeletedRawNotifyHelperThread;->start()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 3948
    :cond_1
    monitor-exit v1

    return-void

    .line 3936
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method private setContext(Landroid/content/Context;)V
    .locals 0
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 3951
    iput-object p1, p0, Lcom/android/providers/contacts/ContactsProvider2$DeletedRawNotifyHelperThread;->mContext:Landroid/content/Context;

    .line 3952
    return-void
.end method


# virtual methods
.method public run()V
    .locals 14

    .prologue
    const-wide/16 v12, 0x258

    const-wide/16 v10, 0x64

    .line 3973
    sget-object v5, Lcom/android/providers/contacts/ContactsProvider2$DeletedRawNotifyHelperThread;->mBackService:Lcom/android/providers/contacts/ContactsProvider2$DeletedRawNotifyHelperThread;

    if-nez v5, :cond_0

    .line 3974
    const-string v5, "ContactsProvider"

    const-string v6, "Don\'t run this thread without call method \'mBackService()\'"

    invoke-static {v5, v6}, Landroid/util/secutil/Log;->secW(Ljava/lang/String;Ljava/lang/String;)I

    .line 4019
    :goto_0
    return-void

    .line 3977
    :cond_0
    const-string v4, ""

    .line 3978
    .local v4, "temp":Ljava/lang/String;
    const/4 v3, 0x0

    .line 3981
    .local v3, "retry":I
    :cond_1
    :goto_1
    :try_start_0
    iget-object v5, p0, Lcom/android/providers/contacts/ContactsProvider2$DeletedRawNotifyHelperThread;->mQueue:Ljava/util/concurrent/ArrayBlockingQueue;

    invoke-virtual {v5}, Ljava/util/concurrent/ArrayBlockingQueue;->size()I

    move-result v5

    if-eqz v5, :cond_2

    .line 3982
    iget-object v5, p0, Lcom/android/providers/contacts/ContactsProvider2$DeletedRawNotifyHelperThread;->mQueue:Ljava/util/concurrent/ArrayBlockingQueue;

    invoke-virtual {v5}, Ljava/util/concurrent/ArrayBlockingQueue;->take()Ljava/lang/Object;

    move-result-object v5

    move-object v0, v5

    check-cast v0, Ljava/lang/String;

    move-object v4, v0

    .line 3983
    iget-object v5, p0, Lcom/android/providers/contacts/ContactsProvider2$DeletedRawNotifyHelperThread;->mContext:Landroid/content/Context;

    invoke-virtual {v5}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v5

    sget-object v6, Landroid/provider/CalendarContract$Events;->CONTENT_URI:Landroid/net/Uri;

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "contact_data_id IS NOT NULL AND contact_id in "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    const/4 v8, 0x0

    invoke-virtual {v5, v6, v7, v8}, Landroid/content/ContentResolver;->delete(Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;)I

    .line 3985
    const-string v4, ""

    .line 3986
    const/4 v3, 0x0

    .line 3987
    const-wide/16 v6, 0x0

    iput-wide v6, p0, Lcom/android/providers/contacts/ContactsProvider2$DeletedRawNotifyHelperThread;->mTotalWaitingTime:J
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/NullPointerException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_2
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_1

    .line 3998
    :catch_0
    move-exception v2

    .line 3999
    .local v2, "e":Ljava/lang/InterruptedException;
    :try_start_1
    invoke-virtual {v2}, Ljava/lang/InterruptedException;->printStackTrace()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 4005
    if-eqz v4, :cond_1

    invoke-virtual {v4}, Ljava/lang/String;->length()I

    move-result v5

    if-eqz v5, :cond_1

    sget v5, Lcom/android/providers/contacts/ContactsProvider2$DeletedRawNotifyHelperThread;->MAX_CALENDAR_PROVIDER_OPERATION_RETRY:I

    if-ge v3, v5, :cond_1

    .line 4007
    const-string v5, "ContactsProvider"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "Calendar Provider is broken (DeletedRawContacts Thread) retry:"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, " failed ids = "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 4010
    invoke-direct {p0, v4}, Lcom/android/providers/contacts/ContactsProvider2$DeletedRawNotifyHelperThread;->putIds(Ljava/lang/String;)V

    .line 4011
    invoke-virtual {p0, v10, v11}, Lcom/android/providers/contacts/ContactsProvider2$DeletedRawNotifyHelperThread;->sleepSelf(J)V

    .line 4012
    const-string v4, ""

    .line 4013
    add-int/lit8 v3, v3, 0x1

    goto :goto_1

    .line 3989
    .end local v2    # "e":Ljava/lang/InterruptedException;
    :cond_2
    :goto_2
    :try_start_2
    iget-object v5, p0, Lcom/android/providers/contacts/ContactsProvider2$DeletedRawNotifyHelperThread;->mQueue:Ljava/util/concurrent/ArrayBlockingQueue;

    invoke-virtual {v5}, Ljava/util/concurrent/ArrayBlockingQueue;->size()I

    move-result v5

    if-nez v5, :cond_3

    iget-wide v6, p0, Lcom/android/providers/contacts/ContactsProvider2$DeletedRawNotifyHelperThread;->mTotalWaitingTime:J

    cmp-long v5, v12, v6

    if-lez v5, :cond_3

    .line 3990
    const-wide/16 v6, 0x64

    invoke-static {v6, v7}, Ljava/lang/Thread;->sleep(J)V

    .line 3991
    iget-wide v6, p0, Lcom/android/providers/contacts/ContactsProvider2$DeletedRawNotifyHelperThread;->mTotalWaitingTime:J

    const-wide/16 v8, 0x1

    add-long/2addr v6, v8

    iput-wide v6, p0, Lcom/android/providers/contacts/ContactsProvider2$DeletedRawNotifyHelperThread;->mTotalWaitingTime:J
    :try_end_2
    .catch Ljava/lang/InterruptedException; {:try_start_2 .. :try_end_2} :catch_0
    .catch Ljava/lang/NullPointerException; {:try_start_2 .. :try_end_2} :catch_1
    .catch Ljava/lang/IllegalArgumentException; {:try_start_2 .. :try_end_2} :catch_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_2

    .line 4000
    :catch_1
    move-exception v2

    .line 4001
    .local v2, "e":Ljava/lang/NullPointerException;
    :try_start_3
    invoke-virtual {v2}, Ljava/lang/NullPointerException;->printStackTrace()V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 4005
    if-eqz v4, :cond_1

    invoke-virtual {v4}, Ljava/lang/String;->length()I

    move-result v5

    if-eqz v5, :cond_1

    sget v5, Lcom/android/providers/contacts/ContactsProvider2$DeletedRawNotifyHelperThread;->MAX_CALENDAR_PROVIDER_OPERATION_RETRY:I

    if-ge v3, v5, :cond_1

    .line 4007
    const-string v5, "ContactsProvider"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "Calendar Provider is broken (DeletedRawContacts Thread) retry:"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, " failed ids = "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 4010
    invoke-direct {p0, v4}, Lcom/android/providers/contacts/ContactsProvider2$DeletedRawNotifyHelperThread;->putIds(Ljava/lang/String;)V

    .line 4011
    invoke-virtual {p0, v10, v11}, Lcom/android/providers/contacts/ContactsProvider2$DeletedRawNotifyHelperThread;->sleepSelf(J)V

    .line 4012
    const-string v4, ""

    .line 4013
    add-int/lit8 v3, v3, 0x1

    goto/16 :goto_1

    .line 3993
    .end local v2    # "e":Ljava/lang/NullPointerException;
    :cond_3
    :try_start_4
    iget-wide v6, p0, Lcom/android/providers/contacts/ContactsProvider2$DeletedRawNotifyHelperThread;->mTotalWaitingTime:J

    cmp-long v5, v12, v6

    if-gtz v5, :cond_5

    iget-object v5, p0, Lcom/android/providers/contacts/ContactsProvider2$DeletedRawNotifyHelperThread;->mQueue:Ljava/util/concurrent/ArrayBlockingQueue;

    invoke-virtual {v5}, Ljava/util/concurrent/ArrayBlockingQueue;->size()I

    move-result v5

    if-nez v5, :cond_5

    .line 3994
    const-string v5, "ContactsProvider"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "Stoped DeletedRawNotifyHelperThread time : "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    iget-wide v8, p0, Lcom/android/providers/contacts/ContactsProvider2$DeletedRawNotifyHelperThread;->mTotalWaitingTime:J

    invoke-virtual {v6, v8, v9}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Landroid/util/secutil/Log;->secI(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_4
    .catch Ljava/lang/InterruptedException; {:try_start_4 .. :try_end_4} :catch_0
    .catch Ljava/lang/NullPointerException; {:try_start_4 .. :try_end_4} :catch_1
    .catch Ljava/lang/IllegalArgumentException; {:try_start_4 .. :try_end_4} :catch_2
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    .line 4005
    if-eqz v4, :cond_4

    invoke-virtual {v4}, Ljava/lang/String;->length()I

    move-result v5

    if-eqz v5, :cond_4

    sget v5, Lcom/android/providers/contacts/ContactsProvider2$DeletedRawNotifyHelperThread;->MAX_CALENDAR_PROVIDER_OPERATION_RETRY:I

    if-ge v3, v5, :cond_4

    .line 4007
    const-string v5, "ContactsProvider"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "Calendar Provider is broken (DeletedRawContacts Thread) retry:"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, " failed ids = "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 4010
    invoke-direct {p0, v4}, Lcom/android/providers/contacts/ContactsProvider2$DeletedRawNotifyHelperThread;->putIds(Ljava/lang/String;)V

    .line 4011
    invoke-virtual {p0, v10, v11}, Lcom/android/providers/contacts/ContactsProvider2$DeletedRawNotifyHelperThread;->sleepSelf(J)V

    .line 4012
    const-string v4, ""

    .line 4013
    add-int/lit8 v3, v3, 0x1

    .line 4017
    :cond_4
    const/4 v5, 0x0

    sput-boolean v5, Lcom/android/providers/contacts/ContactsProvider2$DeletedRawNotifyHelperThread;->isRunning:Z

    .line 4018
    const-string v5, "ContactsProvider"

    const-string v6, "End DeletedRawNotifyHelperThread"

    invoke-static {v5, v6}, Landroid/util/secutil/Log;->secI(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0

    .line 4005
    :cond_5
    if-eqz v4, :cond_1

    invoke-virtual {v4}, Ljava/lang/String;->length()I

    move-result v5

    if-eqz v5, :cond_1

    sget v5, Lcom/android/providers/contacts/ContactsProvider2$DeletedRawNotifyHelperThread;->MAX_CALENDAR_PROVIDER_OPERATION_RETRY:I

    if-ge v3, v5, :cond_1

    .line 4007
    const-string v5, "ContactsProvider"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "Calendar Provider is broken (DeletedRawContacts Thread) retry:"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, " failed ids = "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 4010
    invoke-direct {p0, v4}, Lcom/android/providers/contacts/ContactsProvider2$DeletedRawNotifyHelperThread;->putIds(Ljava/lang/String;)V

    .line 4011
    invoke-virtual {p0, v10, v11}, Lcom/android/providers/contacts/ContactsProvider2$DeletedRawNotifyHelperThread;->sleepSelf(J)V

    .line 4012
    const-string v4, ""

    .line 4013
    add-int/lit8 v3, v3, 0x1

    goto/16 :goto_1

    .line 4002
    :catch_2
    move-exception v2

    .line 4003
    .local v2, "e":Ljava/lang/IllegalArgumentException;
    :try_start_5
    const-string v5, "ContactsProvider"

    const-string v6, "Ignoring unexpected illegal argument exception"

    invoke-static {v5, v6, v2}, Landroid/util/secutil/Log;->secW(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    .line 4005
    if-eqz v4, :cond_1

    invoke-virtual {v4}, Ljava/lang/String;->length()I

    move-result v5

    if-eqz v5, :cond_1

    sget v5, Lcom/android/providers/contacts/ContactsProvider2$DeletedRawNotifyHelperThread;->MAX_CALENDAR_PROVIDER_OPERATION_RETRY:I

    if-ge v3, v5, :cond_1

    .line 4007
    const-string v5, "ContactsProvider"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "Calendar Provider is broken (DeletedRawContacts Thread) retry:"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, " failed ids = "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 4010
    invoke-direct {p0, v4}, Lcom/android/providers/contacts/ContactsProvider2$DeletedRawNotifyHelperThread;->putIds(Ljava/lang/String;)V

    .line 4011
    invoke-virtual {p0, v10, v11}, Lcom/android/providers/contacts/ContactsProvider2$DeletedRawNotifyHelperThread;->sleepSelf(J)V

    .line 4012
    const-string v4, ""

    .line 4013
    add-int/lit8 v3, v3, 0x1

    goto/16 :goto_1

    .line 4005
    .end local v2    # "e":Ljava/lang/IllegalArgumentException;
    :catchall_0
    move-exception v5

    if-eqz v4, :cond_6

    invoke-virtual {v4}, Ljava/lang/String;->length()I

    move-result v6

    if-eqz v6, :cond_6

    sget v6, Lcom/android/providers/contacts/ContactsProvider2$DeletedRawNotifyHelperThread;->MAX_CALENDAR_PROVIDER_OPERATION_RETRY:I

    if-ge v3, v6, :cond_6

    .line 4007
    const-string v6, "ContactsProvider"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "Calendar Provider is broken (DeletedRawContacts Thread) retry:"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, " failed ids = "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 4010
    invoke-direct {p0, v4}, Lcom/android/providers/contacts/ContactsProvider2$DeletedRawNotifyHelperThread;->putIds(Ljava/lang/String;)V

    .line 4011
    invoke-virtual {p0, v10, v11}, Lcom/android/providers/contacts/ContactsProvider2$DeletedRawNotifyHelperThread;->sleepSelf(J)V

    .line 4012
    const-string v4, ""

    .line 4013
    add-int/lit8 v3, v3, 0x1

    :cond_6
    throw v5
.end method

.method public sleepSelf(J)V
    .locals 1
    .param p1, "time"    # J

    .prologue
    .line 3964
    :try_start_0
    invoke-static {p1, p2}, Ljava/lang/Thread;->sleep(J)V
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0

    .line 3969
    :goto_0
    return-void

    .line 3965
    :catch_0
    move-exception v0

    .line 3967
    .local v0, "e":Ljava/lang/InterruptedException;
    invoke-virtual {v0}, Ljava/lang/InterruptedException;->printStackTrace()V

    goto :goto_0
.end method
