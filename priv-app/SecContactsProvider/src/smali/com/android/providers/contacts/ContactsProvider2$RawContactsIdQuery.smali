.class interface abstract Lcom/android/providers/contacts/ContactsProvider2$RawContactsIdQuery;
.super Ljava/lang/Object;
.source "ContactsProvider2.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/providers/contacts/ContactsProvider2;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x60a
    name = "RawContactsIdQuery"
.end annotation


# static fields
.field public static final COLUMNS:[Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    .line 6086
    const/4 v0, 0x4

    new-array v0, v0, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string v2, "raw_contacts._id AS _id "

    aput-object v2, v0, v1

    const/4 v1, 0x1

    const-string v2, "accounts.account_name AS account_name"

    aput-object v2, v0, v1

    const/4 v1, 0x2

    const-string v2, "accounts.account_type AS account_type"

    aput-object v2, v0, v1

    const/4 v1, 0x3

    const-string v2, "accounts.data_set AS data_set"

    aput-object v2, v0, v1

    sput-object v0, Lcom/android/providers/contacts/ContactsProvider2$RawContactsIdQuery;->COLUMNS:[Ljava/lang/String;

    return-void
.end method
