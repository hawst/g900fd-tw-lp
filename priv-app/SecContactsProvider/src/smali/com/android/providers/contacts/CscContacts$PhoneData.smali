.class Lcom/android/providers/contacts/CscContacts$PhoneData;
.super Ljava/lang/Object;
.source "CscContacts.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/providers/contacts/CscContacts;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "PhoneData"
.end annotation


# instance fields
.field private mNumber:Ljava/lang/String;

.field private mType:Ljava/lang/String;


# direct methods
.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;)V
    .locals 0
    .param p1, "number"    # Ljava/lang/String;
    .param p2, "type"    # Ljava/lang/String;

    .prologue
    .line 306
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 307
    iput-object p1, p0, Lcom/android/providers/contacts/CscContacts$PhoneData;->mNumber:Ljava/lang/String;

    .line 308
    iput-object p2, p0, Lcom/android/providers/contacts/CscContacts$PhoneData;->mType:Ljava/lang/String;

    .line 309
    return-void
.end method


# virtual methods
.method public getNumber()Ljava/lang/String;
    .locals 1

    .prologue
    .line 312
    iget-object v0, p0, Lcom/android/providers/contacts/CscContacts$PhoneData;->mNumber:Ljava/lang/String;

    return-object v0
.end method

.method public getType()Ljava/lang/String;
    .locals 1

    .prologue
    .line 316
    iget-object v0, p0, Lcom/android/providers/contacts/CscContacts$PhoneData;->mType:Ljava/lang/String;

    return-object v0
.end method
