.class public Lcom/android/providers/contacts/LocaleSet;
.super Ljava/lang/Object;
.source "LocaleSet.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/android/providers/contacts/LocaleSet$LocaleWrapper;
    }
.end annotation


# static fields
.field private static final CHINESE_LANGUAGE:Ljava/lang/String;

.field private static final JAPANESE_LANGUAGE:Ljava/lang/String;

.field private static final KOREAN_LANGUAGE:Ljava/lang/String;


# instance fields
.field private final mPrimaryLocale:Lcom/android/providers/contacts/LocaleSet$LocaleWrapper;

.field private final mSecondaryLocale:Lcom/android/providers/contacts/LocaleSet$LocaleWrapper;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 24
    sget-object v0, Ljava/util/Locale;->CHINESE:Ljava/util/Locale;

    invoke-virtual {v0}, Ljava/util/Locale;->getLanguage()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/android/providers/contacts/LocaleSet;->CHINESE_LANGUAGE:Ljava/lang/String;

    .line 25
    sget-object v0, Ljava/util/Locale;->JAPANESE:Ljava/util/Locale;

    invoke-virtual {v0}, Ljava/util/Locale;->getLanguage()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/android/providers/contacts/LocaleSet;->JAPANESE_LANGUAGE:Ljava/lang/String;

    .line 26
    sget-object v0, Ljava/util/Locale;->KOREAN:Ljava/util/Locale;

    invoke-virtual {v0}, Ljava/util/Locale;->getLanguage()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/android/providers/contacts/LocaleSet;->KOREAN_LANGUAGE:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Ljava/util/Locale;)V
    .locals 1
    .param p1, "locale"    # Ljava/util/Locale;

    .prologue
    .line 81
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/android/providers/contacts/LocaleSet;-><init>(Ljava/util/Locale;Ljava/util/Locale;)V

    .line 82
    return-void
.end method

.method public constructor <init>(Ljava/util/Locale;Ljava/util/Locale;)V
    .locals 2
    .param p1, "primaryLocale"    # Ljava/util/Locale;
    .param p2, "secondaryLocale"    # Ljava/util/Locale;

    .prologue
    .line 121
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 122
    new-instance v0, Lcom/android/providers/contacts/LocaleSet$LocaleWrapper;

    invoke-direct {v0, p1}, Lcom/android/providers/contacts/LocaleSet$LocaleWrapper;-><init>(Ljava/util/Locale;)V

    iput-object v0, p0, Lcom/android/providers/contacts/LocaleSet;->mPrimaryLocale:Lcom/android/providers/contacts/LocaleSet$LocaleWrapper;

    .line 123
    new-instance v0, Lcom/android/providers/contacts/LocaleSet$LocaleWrapper;

    iget-object v1, p0, Lcom/android/providers/contacts/LocaleSet;->mPrimaryLocale:Lcom/android/providers/contacts/LocaleSet$LocaleWrapper;

    invoke-virtual {v1, p2}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 p2, 0x0

    .end local p2    # "secondaryLocale":Ljava/util/Locale;
    :cond_0
    invoke-direct {v0, p2}, Lcom/android/providers/contacts/LocaleSet$LocaleWrapper;-><init>(Ljava/util/Locale;)V

    iput-object v0, p0, Lcom/android/providers/contacts/LocaleSet;->mSecondaryLocale:Lcom/android/providers/contacts/LocaleSet$LocaleWrapper;

    .line 125
    return-void
.end method

.method static synthetic access$000()Ljava/lang/String;
    .locals 1

    .prologue
    .line 23
    sget-object v0, Lcom/android/providers/contacts/LocaleSet;->CHINESE_LANGUAGE:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$100()Ljava/lang/String;
    .locals 1

    .prologue
    .line 23
    sget-object v0, Lcom/android/providers/contacts/LocaleSet;->JAPANESE_LANGUAGE:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$200()Ljava/lang/String;
    .locals 1

    .prologue
    .line 23
    sget-object v0, Lcom/android/providers/contacts/LocaleSet;->KOREAN_LANGUAGE:Ljava/lang/String;

    return-object v0
.end method

.method public static getDefault()Lcom/android/providers/contacts/LocaleSet;
    .locals 2

    .prologue
    .line 77
    new-instance v0, Lcom/android/providers/contacts/LocaleSet;

    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/android/providers/contacts/LocaleSet;-><init>(Ljava/util/Locale;)V

    return-object v0
.end method

.method public static getLocaleSet(Ljava/lang/String;)Lcom/android/providers/contacts/LocaleSet;
    .locals 6
    .param p0, "localeString"    # Ljava/lang/String;

    .prologue
    const/4 v5, 0x1

    .line 99
    if-eqz p0, :cond_1

    const/16 v3, 0x5f

    invoke-virtual {p0, v3}, Ljava/lang/String;->indexOf(I)I

    move-result v3

    const/4 v4, -0x1

    if-ne v3, v4, :cond_1

    .line 100
    const-string v3, ";"

    invoke-virtual {p0, v3}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v0

    .line 101
    .local v0, "locales":[Ljava/lang/String;
    const/4 v3, 0x0

    aget-object v3, v0, v3

    invoke-static {v3}, Ljava/util/Locale;->forLanguageTag(Ljava/lang/String;)Ljava/util/Locale;

    move-result-object v1

    .line 103
    .local v1, "primaryLocale":Ljava/util/Locale;
    if-eqz v1, :cond_1

    invoke-virtual {v1}, Ljava/util/Locale;->toLanguageTag()Ljava/lang/String;

    move-result-object v3

    const-string v4, "und"

    invoke-static {v3, v4}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_1

    .line 105
    array-length v3, v0

    if-le v3, v5, :cond_0

    aget-object v3, v0, v5

    if-eqz v3, :cond_0

    .line 106
    aget-object v3, v0, v5

    invoke-static {v3}, Ljava/util/Locale;->forLanguageTag(Ljava/lang/String;)Ljava/util/Locale;

    move-result-object v2

    .line 107
    .local v2, "secondaryLocale":Ljava/util/Locale;
    if-eqz v2, :cond_0

    invoke-virtual {v2}, Ljava/util/Locale;->toLanguageTag()Ljava/lang/String;

    move-result-object v3

    const-string v4, "und"

    invoke-static {v3, v4}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_0

    .line 109
    new-instance v3, Lcom/android/providers/contacts/LocaleSet;

    invoke-direct {v3, v1, v2}, Lcom/android/providers/contacts/LocaleSet;-><init>(Ljava/util/Locale;Ljava/util/Locale;)V

    .line 115
    .end local v0    # "locales":[Ljava/lang/String;
    .end local v1    # "primaryLocale":Ljava/util/Locale;
    .end local v2    # "secondaryLocale":Ljava/util/Locale;
    :goto_0
    return-object v3

    .line 112
    .restart local v0    # "locales":[Ljava/lang/String;
    .restart local v1    # "primaryLocale":Ljava/util/Locale;
    :cond_0
    new-instance v3, Lcom/android/providers/contacts/LocaleSet;

    invoke-direct {v3, v1}, Lcom/android/providers/contacts/LocaleSet;-><init>(Ljava/util/Locale;)V

    goto :goto_0

    .line 115
    .end local v0    # "locales":[Ljava/lang/String;
    .end local v1    # "primaryLocale":Ljava/util/Locale;
    :cond_1
    invoke-static {}, Lcom/android/providers/contacts/LocaleSet;->getDefault()Lcom/android/providers/contacts/LocaleSet;

    move-result-object v3

    goto :goto_0
.end method

.method public static isLocaleSimplifiedChinese(Ljava/util/Locale;)Z
    .locals 2
    .param p0, "locale"    # Ljava/util/Locale;
    .annotation build Lcom/google/common/annotations/VisibleForTesting;
    .end annotation

    .prologue
    .line 173
    if-eqz p0, :cond_0

    invoke-virtual {p0}, Ljava/util/Locale;->getLanguage()Ljava/lang/String;

    move-result-object v0

    sget-object v1, Lcom/android/providers/contacts/LocaleSet;->CHINESE_LANGUAGE:Ljava/lang/String;

    invoke-static {v0, v1}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 174
    :cond_0
    const/4 v0, 0x0

    .line 181
    :goto_0
    return v0

    .line 177
    :cond_1
    invoke-virtual {p0}, Ljava/util/Locale;->getScript()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_2

    .line 178
    invoke-virtual {p0}, Ljava/util/Locale;->getScript()Ljava/lang/String;

    move-result-object v0

    const-string v1, "Hans"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    goto :goto_0

    .line 181
    :cond_2
    sget-object v0, Ljava/util/Locale;->SIMPLIFIED_CHINESE:Ljava/util/Locale;

    invoke-virtual {p0, v0}, Ljava/util/Locale;->equals(Ljava/lang/Object;)Z

    move-result v0

    goto :goto_0
.end method

.method public static isLocaleTraditionalChinese(Ljava/util/Locale;)Z
    .locals 2
    .param p0, "locale"    # Ljava/util/Locale;
    .annotation build Lcom/google/common/annotations/VisibleForTesting;
    .end annotation

    .prologue
    .line 195
    if-eqz p0, :cond_0

    invoke-virtual {p0}, Ljava/util/Locale;->getLanguage()Ljava/lang/String;

    move-result-object v0

    sget-object v1, Lcom/android/providers/contacts/LocaleSet;->CHINESE_LANGUAGE:Ljava/lang/String;

    invoke-static {v0, v1}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 196
    :cond_0
    const/4 v0, 0x0

    .line 203
    :goto_0
    return v0

    .line 199
    :cond_1
    invoke-virtual {p0}, Ljava/util/Locale;->getScript()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_2

    .line 200
    invoke-virtual {p0}, Ljava/util/Locale;->getScript()Ljava/lang/String;

    move-result-object v0

    const-string v1, "Hant"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    goto :goto_0

    .line 203
    :cond_2
    sget-object v0, Ljava/util/Locale;->TRADITIONAL_CHINESE:Ljava/util/Locale;

    invoke-virtual {p0, v0}, Ljava/util/Locale;->equals(Ljava/lang/Object;)Z

    move-result v0

    goto :goto_0
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 4
    .param p1, "object"    # Ljava/lang/Object;

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 232
    if-ne p1, p0, :cond_1

    .line 240
    :cond_0
    :goto_0
    return v1

    .line 235
    :cond_1
    instance-of v3, p1, Lcom/android/providers/contacts/LocaleSet;

    if-eqz v3, :cond_3

    move-object v0, p1

    .line 236
    check-cast v0, Lcom/android/providers/contacts/LocaleSet;

    .line 237
    .local v0, "other":Lcom/android/providers/contacts/LocaleSet;
    iget-object v3, p0, Lcom/android/providers/contacts/LocaleSet;->mPrimaryLocale:Lcom/android/providers/contacts/LocaleSet$LocaleWrapper;

    invoke-virtual {v3}, Lcom/android/providers/contacts/LocaleSet$LocaleWrapper;->getLocale()Ljava/util/Locale;

    move-result-object v3

    invoke-virtual {v0, v3}, Lcom/android/providers/contacts/LocaleSet;->isPrimaryLocale(Ljava/util/Locale;)Z

    move-result v3

    if-eqz v3, :cond_2

    iget-object v3, p0, Lcom/android/providers/contacts/LocaleSet;->mSecondaryLocale:Lcom/android/providers/contacts/LocaleSet$LocaleWrapper;

    invoke-virtual {v3}, Lcom/android/providers/contacts/LocaleSet$LocaleWrapper;->getLocale()Ljava/util/Locale;

    move-result-object v3

    invoke-virtual {v0, v3}, Lcom/android/providers/contacts/LocaleSet;->isSecondaryLocale(Ljava/util/Locale;)Z

    move-result v3

    if-nez v3, :cond_0

    :cond_2
    move v1, v2

    goto :goto_0

    .end local v0    # "other":Lcom/android/providers/contacts/LocaleSet;
    :cond_3
    move v1, v2

    .line 240
    goto :goto_0
.end method

.method public getPrimaryLocale()Ljava/util/Locale;
    .locals 1

    .prologue
    .line 152
    iget-object v0, p0, Lcom/android/providers/contacts/LocaleSet;->mPrimaryLocale:Lcom/android/providers/contacts/LocaleSet$LocaleWrapper;

    invoke-virtual {v0}, Lcom/android/providers/contacts/LocaleSet$LocaleWrapper;->getLocale()Ljava/util/Locale;

    move-result-object v0

    return-object v0
.end method

.method public getSecondaryLocale()Ljava/util/Locale;
    .locals 1

    .prologue
    .line 156
    iget-object v0, p0, Lcom/android/providers/contacts/LocaleSet;->mSecondaryLocale:Lcom/android/providers/contacts/LocaleSet$LocaleWrapper;

    invoke-virtual {v0}, Lcom/android/providers/contacts/LocaleSet$LocaleWrapper;->getLocale()Ljava/util/Locale;

    move-result-object v0

    return-object v0
.end method

.method public hasSecondaryLocale()Z
    .locals 1

    .prologue
    .line 148
    iget-object v0, p0, Lcom/android/providers/contacts/LocaleSet;->mSecondaryLocale:Lcom/android/providers/contacts/LocaleSet$LocaleWrapper;

    invoke-virtual {v0}, Lcom/android/providers/contacts/LocaleSet$LocaleWrapper;->hasLocale()Z

    move-result v0

    return v0
.end method

.method public isPrimaryLanguage(Ljava/lang/String;)Z
    .locals 1
    .param p1, "language"    # Ljava/lang/String;

    .prologue
    .line 223
    iget-object v0, p0, Lcom/android/providers/contacts/LocaleSet;->mPrimaryLocale:Lcom/android/providers/contacts/LocaleSet$LocaleWrapper;

    invoke-virtual {v0, p1}, Lcom/android/providers/contacts/LocaleSet$LocaleWrapper;->isLanguage(Ljava/lang/String;)Z

    move-result v0

    return v0
.end method

.method public isPrimaryLocale(Ljava/util/Locale;)Z
    .locals 1
    .param p1, "locale"    # Ljava/util/Locale;

    .prologue
    .line 160
    iget-object v0, p0, Lcom/android/providers/contacts/LocaleSet;->mPrimaryLocale:Lcom/android/providers/contacts/LocaleSet$LocaleWrapper;

    invoke-virtual {v0, p1}, Lcom/android/providers/contacts/LocaleSet$LocaleWrapper;->isLocale(Ljava/util/Locale;)Z

    move-result v0

    return v0
.end method

.method public isPrimaryLocaleCJK()Z
    .locals 1

    .prologue
    .line 215
    iget-object v0, p0, Lcom/android/providers/contacts/LocaleSet;->mPrimaryLocale:Lcom/android/providers/contacts/LocaleSet$LocaleWrapper;

    invoke-virtual {v0}, Lcom/android/providers/contacts/LocaleSet$LocaleWrapper;->isLocaleCJK()Z

    move-result v0

    return v0
.end method

.method public isPrimaryLocaleSimplifiedChinese()Z
    .locals 1

    .prologue
    .line 185
    invoke-virtual {p0}, Lcom/android/providers/contacts/LocaleSet;->getPrimaryLocale()Ljava/util/Locale;

    move-result-object v0

    invoke-static {v0}, Lcom/android/providers/contacts/LocaleSet;->isLocaleSimplifiedChinese(Ljava/util/Locale;)Z

    move-result v0

    return v0
.end method

.method public isSecondaryLanguage(Ljava/lang/String;)Z
    .locals 1
    .param p1, "language"    # Ljava/lang/String;

    .prologue
    .line 227
    iget-object v0, p0, Lcom/android/providers/contacts/LocaleSet;->mSecondaryLocale:Lcom/android/providers/contacts/LocaleSet$LocaleWrapper;

    invoke-virtual {v0, p1}, Lcom/android/providers/contacts/LocaleSet$LocaleWrapper;->isLanguage(Ljava/lang/String;)Z

    move-result v0

    return v0
.end method

.method public isSecondaryLocale(Ljava/util/Locale;)Z
    .locals 1
    .param p1, "locale"    # Ljava/util/Locale;

    .prologue
    .line 164
    iget-object v0, p0, Lcom/android/providers/contacts/LocaleSet;->mSecondaryLocale:Lcom/android/providers/contacts/LocaleSet$LocaleWrapper;

    invoke-virtual {v0, p1}, Lcom/android/providers/contacts/LocaleSet$LocaleWrapper;->isLocale(Ljava/util/Locale;)Z

    move-result v0

    return v0
.end method

.method public isSecondaryLocaleCJK()Z
    .locals 1

    .prologue
    .line 219
    iget-object v0, p0, Lcom/android/providers/contacts/LocaleSet;->mSecondaryLocale:Lcom/android/providers/contacts/LocaleSet$LocaleWrapper;

    invoke-virtual {v0}, Lcom/android/providers/contacts/LocaleSet$LocaleWrapper;->isLocaleCJK()Z

    move-result v0

    return v0
.end method

.method public isSecondaryLocaleSimplifiedChinese()Z
    .locals 1

    .prologue
    .line 189
    invoke-virtual {p0}, Lcom/android/providers/contacts/LocaleSet;->getSecondaryLocale()Ljava/util/Locale;

    move-result-object v0

    invoke-static {v0}, Lcom/android/providers/contacts/LocaleSet;->isLocaleSimplifiedChinese(Ljava/util/Locale;)Z

    move-result v0

    return v0
.end method

.method public normalize()Lcom/android/providers/contacts/LocaleSet;
    .locals 3

    .prologue
    .line 128
    invoke-virtual {p0}, Lcom/android/providers/contacts/LocaleSet;->getPrimaryLocale()Ljava/util/Locale;

    move-result-object v0

    .line 129
    .local v0, "primaryLocale":Ljava/util/Locale;
    if-nez v0, :cond_1

    .line 130
    invoke-static {}, Lcom/android/providers/contacts/LocaleSet;->getDefault()Lcom/android/providers/contacts/LocaleSet;

    move-result-object p0

    .line 144
    .end local p0    # "this":Lcom/android/providers/contacts/LocaleSet;
    :cond_0
    :goto_0
    return-object p0

    .line 132
    .restart local p0    # "this":Lcom/android/providers/contacts/LocaleSet;
    :cond_1
    invoke-virtual {p0}, Lcom/android/providers/contacts/LocaleSet;->getSecondaryLocale()Ljava/util/Locale;

    move-result-object v1

    .line 135
    .local v1, "secondaryLocale":Ljava/util/Locale;
    if-eqz v1, :cond_2

    invoke-virtual {v1}, Ljava/util/Locale;->getLanguage()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0, v2}, Lcom/android/providers/contacts/LocaleSet;->isPrimaryLanguage(Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_2

    invoke-virtual {p0}, Lcom/android/providers/contacts/LocaleSet;->isPrimaryLocaleCJK()Z

    move-result v2

    if-eqz v2, :cond_3

    invoke-virtual {p0}, Lcom/android/providers/contacts/LocaleSet;->isSecondaryLocaleCJK()Z

    move-result v2

    if-eqz v2, :cond_3

    .line 138
    :cond_2
    new-instance p0, Lcom/android/providers/contacts/LocaleSet;

    .end local p0    # "this":Lcom/android/providers/contacts/LocaleSet;
    invoke-direct {p0, v0}, Lcom/android/providers/contacts/LocaleSet;-><init>(Ljava/util/Locale;)V

    goto :goto_0

    .line 141
    .restart local p0    # "this":Lcom/android/providers/contacts/LocaleSet;
    :cond_3
    sget-object v2, Ljava/util/Locale;->ENGLISH:Ljava/util/Locale;

    invoke-virtual {v2}, Ljava/util/Locale;->getLanguage()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0, v2}, Lcom/android/providers/contacts/LocaleSet;->isSecondaryLanguage(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 142
    new-instance p0, Lcom/android/providers/contacts/LocaleSet;

    .end local p0    # "this":Lcom/android/providers/contacts/LocaleSet;
    invoke-direct {p0, v0}, Lcom/android/providers/contacts/LocaleSet;-><init>(Ljava/util/Locale;)V

    goto :goto_0
.end method

.method public final toString()Ljava/lang/String;
    .locals 2

    .prologue
    .line 245
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 246
    .local v0, "builder":Ljava/lang/StringBuilder;
    iget-object v1, p0, Lcom/android/providers/contacts/LocaleSet;->mPrimaryLocale:Lcom/android/providers/contacts/LocaleSet$LocaleWrapper;

    invoke-virtual {v1}, Lcom/android/providers/contacts/LocaleSet$LocaleWrapper;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 247
    invoke-virtual {p0}, Lcom/android/providers/contacts/LocaleSet;->hasSecondaryLocale()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 248
    const-string v1, ";"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 249
    iget-object v1, p0, Lcom/android/providers/contacts/LocaleSet;->mSecondaryLocale:Lcom/android/providers/contacts/LocaleSet$LocaleWrapper;

    invoke-virtual {v1}, Lcom/android/providers/contacts/LocaleSet$LocaleWrapper;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 251
    :cond_0
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    return-object v1
.end method
