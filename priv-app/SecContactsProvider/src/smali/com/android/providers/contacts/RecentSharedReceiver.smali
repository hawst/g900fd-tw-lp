.class public Lcom/android/providers/contacts/RecentSharedReceiver;
.super Landroid/content/BroadcastReceiver;
.source "RecentSharedReceiver.java"


# instance fields
.field private mPrefs:Landroid/content/SharedPreferences;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 12
    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    .line 17
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/android/providers/contacts/RecentSharedReceiver;->mPrefs:Landroid/content/SharedPreferences;

    return-void
.end method

.method private getDuplicatedIndex(Ljava/lang/String;)I
    .locals 9
    .param p1, "list"    # Ljava/lang/String;

    .prologue
    const/4 v8, 0x0

    const/4 v4, -0x1

    .line 87
    iget-object v5, p0, Lcom/android/providers/contacts/RecentSharedReceiver;->mPrefs:Landroid/content/SharedPreferences;

    const-string v6, "COUNT"

    invoke-interface {v5, v6, v8}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v3

    .line 88
    .local v3, "sharedCount":I
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    if-ge v0, v3, :cond_2

    .line 89
    new-instance v1, Ljava/lang/StringBuilder;

    const-string v5, "POSITON_"

    invoke-direct {v1, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 90
    .local v1, "key":Ljava/lang/StringBuilder;
    const-string v5, "%d"

    const/4 v6, 0x1

    new-array v6, v6, [Ljava/lang/Object;

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    aput-object v7, v6, v8

    invoke-static {v5, v6}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v1, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 91
    iget-object v5, p0, Lcom/android/providers/contacts/RecentSharedReceiver;->mPrefs:Landroid/content/SharedPreferences;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    const-string v7, ""

    invoke-interface {v5, v6, v7}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 93
    .local v2, "oldData":Ljava/lang/String;
    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v5

    if-eqz v5, :cond_1

    move v0, v4

    .line 102
    .end local v0    # "i":I
    .end local v1    # "key":Ljava/lang/StringBuilder;
    .end local v2    # "oldData":Ljava/lang/String;
    :cond_0
    :goto_1
    return v0

    .line 97
    .restart local v0    # "i":I
    .restart local v1    # "key":Ljava/lang/StringBuilder;
    .restart local v2    # "oldData":Ljava/lang/String;
    :cond_1
    invoke-virtual {p1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-nez v5, :cond_0

    .line 88
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .end local v1    # "key":Ljava/lang/StringBuilder;
    .end local v2    # "oldData":Ljava/lang/String;
    :cond_2
    move v0, v4

    .line 102
    goto :goto_1
.end method

.method private getIdArray(Ljava/util/ArrayList;)[J
    .locals 10
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;)[J"
        }
    .end annotation

    .prologue
    .line 55
    .local p1, "list":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    invoke-virtual {p1}, Ljava/util/ArrayList;->size()I

    move-result v3

    new-array v1, v3, [J

    .line 57
    .local v1, "idList":[J
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    invoke-virtual {p1}, Ljava/util/ArrayList;->size()I

    move-result v3

    if-ge v0, v3, :cond_0

    .line 58
    invoke-virtual {p1, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    invoke-static {v3}, Ljava/lang/Long;->valueOf(Ljava/lang/String;)Ljava/lang/Long;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Long;->longValue()J

    move-result-wide v6

    aput-wide v6, v1, v0

    .line 57
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 62
    :cond_0
    const/4 v0, 0x0

    :goto_1
    array-length v3, v1

    if-ge v0, v3, :cond_3

    .line 63
    const/4 v2, 0x0

    .local v2, "j":I
    :goto_2
    array-length v3, v1

    add-int/lit8 v3, v3, -0x1

    if-ge v2, v3, :cond_2

    .line 64
    aget-wide v6, v1, v2

    add-int/lit8 v3, v2, 0x1

    aget-wide v8, v1, v3

    cmp-long v3, v6, v8

    if-lez v3, :cond_1

    .line 65
    aget-wide v4, v1, v2

    .line 66
    .local v4, "temp":J
    add-int/lit8 v3, v2, 0x1

    aget-wide v6, v1, v3

    aput-wide v6, v1, v2

    .line 67
    add-int/lit8 v3, v2, 0x1

    aput-wide v4, v1, v3

    .line 63
    .end local v4    # "temp":J
    :cond_1
    add-int/lit8 v2, v2, 0x1

    goto :goto_2

    .line 62
    :cond_2
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 71
    .end local v2    # "j":I
    :cond_3
    return-object v1
.end method

.method private makeListString([J)Ljava/lang/String;
    .locals 4
    .param p1, "list"    # [J

    .prologue
    .line 75
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    .line 77
    .local v1, "sb":Ljava/lang/StringBuilder;
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    array-length v2, p1

    if-ge v0, v2, :cond_1

    .line 78
    aget-wide v2, p1, v0

    invoke-static {v2, v3}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 79
    add-int/lit8 v2, v0, 0x1

    array-length v3, p1

    if-ge v2, v3, :cond_0

    .line 80
    const-string v2, ","

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 77
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 83
    :cond_1
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    return-object v2
.end method

.method private reOrderRecentDatas(ILjava/lang/String;)V
    .locals 13
    .param p1, "index"    # I
    .param p2, "list"    # Ljava/lang/String;

    .prologue
    const/4 v12, 0x1

    const/4 v11, 0x0

    .line 106
    if-gez p1, :cond_1

    const/16 v7, 0x8

    .line 107
    .local v7, "reOrderedCount":I
    :goto_0
    iget-object v8, p0, Lcom/android/providers/contacts/RecentSharedReceiver;->mPrefs:Landroid/content/SharedPreferences;

    invoke-interface {v8}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v6

    .line 109
    .local v6, "prefsEditor":Landroid/content/SharedPreferences$Editor;
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_1
    if-ge v0, v7, :cond_0

    .line 110
    if-ne v0, p1, :cond_2

    .line 127
    :cond_0
    new-instance v1, Ljava/lang/String;

    const-string v8, "POSITON_0"

    invoke-direct {v1, v8}, Ljava/lang/String;-><init>(Ljava/lang/String;)V

    .line 128
    .local v1, "key":Ljava/lang/String;
    invoke-interface {v6, v1, p2}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    .line 130
    invoke-interface {v6}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 131
    return-void

    .end local v0    # "i":I
    .end local v1    # "key":Ljava/lang/String;
    .end local v6    # "prefsEditor":Landroid/content/SharedPreferences$Editor;
    .end local v7    # "reOrderedCount":I
    :cond_1
    move v7, p1

    .line 106
    goto :goto_0

    .line 113
    .restart local v0    # "i":I
    .restart local v6    # "prefsEditor":Landroid/content/SharedPreferences$Editor;
    .restart local v7    # "reOrderedCount":I
    :cond_2
    add-int/lit8 v2, v0, 0x1

    .line 115
    .local v2, "newIndex":I
    new-instance v5, Ljava/lang/StringBuilder;

    const-string v8, "POSITON_"

    invoke-direct {v5, v8}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 116
    .local v5, "oldKey":Ljava/lang/StringBuilder;
    const-string v8, "%d"

    new-array v9, v12, [Ljava/lang/Object;

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v10

    aput-object v10, v9, v11

    invoke-static {v8, v9}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v5, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 117
    iget-object v8, p0, Lcom/android/providers/contacts/RecentSharedReceiver;->mPrefs:Landroid/content/SharedPreferences;

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    const-string v10, ""

    invoke-interface {v8, v9, v10}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 119
    .local v4, "oldData":Ljava/lang/String;
    invoke-static {v4}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v8

    if-nez v8, :cond_0

    .line 122
    new-instance v3, Ljava/lang/StringBuilder;

    const-string v8, "POSITON_"

    invoke-direct {v3, v8}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 123
    .local v3, "newKey":Ljava/lang/StringBuilder;
    const-string v8, "%d"

    new-array v9, v12, [Ljava/lang/Object;

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v10

    aput-object v10, v9, v11

    invoke-static {v8, v9}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v3, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 124
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-interface {v6, v8, v4}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    .line 109
    add-int/lit8 v0, v0, 0x1

    goto :goto_1
.end method

.method private saveRecentContacts(Ljava/util/ArrayList;)V
    .locals 9
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .prologue
    .local p1, "list":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    const/16 v5, 0x9

    .line 34
    iget-object v6, p0, Lcom/android/providers/contacts/RecentSharedReceiver;->mPrefs:Landroid/content/SharedPreferences;

    invoke-interface {v6}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v4

    .line 36
    .local v4, "prefsEditor":Landroid/content/SharedPreferences$Editor;
    invoke-direct {p0, p1}, Lcom/android/providers/contacts/RecentSharedReceiver;->getIdArray(Ljava/util/ArrayList;)[J

    move-result-object v0

    .line 37
    .local v0, "idList":[J
    invoke-direct {p0, v0}, Lcom/android/providers/contacts/RecentSharedReceiver;->makeListString([J)Ljava/lang/String;

    move-result-object v2

    .line 40
    .local v2, "listString":Ljava/lang/String;
    invoke-direct {p0, v2}, Lcom/android/providers/contacts/RecentSharedReceiver;->getDuplicatedIndex(Ljava/lang/String;)I

    move-result v1

    .line 43
    .local v1, "index":I
    invoke-direct {p0, v1, v2}, Lcom/android/providers/contacts/RecentSharedReceiver;->reOrderRecentDatas(ILjava/lang/String;)V

    .line 46
    if-gez v1, :cond_1

    .line 47
    iget-object v6, p0, Lcom/android/providers/contacts/RecentSharedReceiver;->mPrefs:Landroid/content/SharedPreferences;

    const-string v7, "COUNT"

    const/4 v8, 0x0

    invoke-interface {v6, v7, v8}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v6

    add-int/lit8 v3, v6, 0x1

    .line 48
    .local v3, "newCount":I
    const-string v6, "COUNT"

    if-lt v3, v5, :cond_0

    move v3, v5

    .end local v3    # "newCount":I
    :cond_0
    invoke-interface {v4, v6, v3}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    .line 50
    invoke-interface {v4}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 52
    :cond_1
    return-void
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 3
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "intent"    # Landroid/content/Intent;

    .prologue
    .line 22
    if-eqz p2, :cond_1

    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_1

    const-string v1, "com.samsung.contacts.UpdateRecentSharedContacts"

    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 24
    const-string v1, "SharedContacts"

    invoke-virtual {p2, v1}, Landroid/content/Intent;->getStringArrayListExtra(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v0

    .line 25
    .local v0, "list":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    iget-object v1, p0, Lcom/android/providers/contacts/RecentSharedReceiver;->mPrefs:Landroid/content/SharedPreferences;

    if-nez v1, :cond_0

    .line 26
    const-string v1, "RecentSharedReceiver"

    const/4 v2, 0x0

    invoke-virtual {p1, v1, v2}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v1

    iput-object v1, p0, Lcom/android/providers/contacts/RecentSharedReceiver;->mPrefs:Landroid/content/SharedPreferences;

    .line 29
    :cond_0
    invoke-direct {p0, v0}, Lcom/android/providers/contacts/RecentSharedReceiver;->saveRecentContacts(Ljava/util/ArrayList;)V

    .line 31
    .end local v0    # "list":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    :cond_1
    return-void
.end method
