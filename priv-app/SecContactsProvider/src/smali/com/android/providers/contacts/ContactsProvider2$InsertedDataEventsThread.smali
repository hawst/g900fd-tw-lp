.class Lcom/android/providers/contacts/ContactsProvider2$InsertedDataEventsThread;
.super Ljava/lang/Thread;
.source "ContactsProvider2.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/providers/contacts/ContactsProvider2;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "InsertedDataEventsThread"
.end annotation


# instance fields
.field db:Landroid/database/sqlite/SQLiteDatabase;

.field private ids:Ljava/lang/String;

.field final synthetic this$0:Lcom/android/providers/contacts/ContactsProvider2;


# direct methods
.method public constructor <init>(Lcom/android/providers/contacts/ContactsProvider2;Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;)V
    .locals 1
    .param p2, "activeDb"    # Landroid/database/sqlite/SQLiteDatabase;
    .param p3, "ids"    # Ljava/lang/String;

    .prologue
    .line 4079
    iput-object p1, p0, Lcom/android/providers/contacts/ContactsProvider2$InsertedDataEventsThread;->this$0:Lcom/android/providers/contacts/ContactsProvider2;

    .line 4080
    const-string v0, "InsertedDataEventsThread"

    invoke-direct {p0, v0}, Ljava/lang/Thread;-><init>(Ljava/lang/String;)V

    .line 4076
    const-string v0, ""

    iput-object v0, p0, Lcom/android/providers/contacts/ContactsProvider2$InsertedDataEventsThread;->ids:Ljava/lang/String;

    .line 4077
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/android/providers/contacts/ContactsProvider2$InsertedDataEventsThread;->db:Landroid/database/sqlite/SQLiteDatabase;

    .line 4081
    iput-object p3, p0, Lcom/android/providers/contacts/ContactsProvider2$InsertedDataEventsThread;->ids:Ljava/lang/String;

    .line 4082
    iput-object p2, p0, Lcom/android/providers/contacts/ContactsProvider2$InsertedDataEventsThread;->db:Landroid/database/sqlite/SQLiteDatabase;

    .line 4083
    return-void
.end method


# virtual methods
.method public run()V
    .locals 35

    .prologue
    .line 4086
    const/4 v12, 0x0

    .line 4087
    .local v12, "cursor":Landroid/database/Cursor;
    const-string v32, "UTC"

    .line 4088
    .local v32, "timezone":Ljava/lang/String;
    new-instance v28, Ljava/util/ArrayList;

    invoke-direct/range {v28 .. v28}, Ljava/util/ArrayList;-><init>()V

    .line 4090
    .local v28, "ops":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Landroid/content/ContentProviderOperation;>;"
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/android/providers/contacts/ContactsProvider2$InsertedDataEventsThread;->this$0:Lcom/android/providers/contacts/ContactsProvider2;

    # invokes: Lcom/android/providers/contacts/ContactsProvider2;->getDisplayOrder()I
    invoke-static {v2}, Lcom/android/providers/contacts/ContactsProvider2;->access$000(Lcom/android/providers/contacts/ContactsProvider2;)I

    move-result v29

    .line 4093
    .local v29, "orderOf":I
    :try_start_0
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/android/providers/contacts/ContactsProvider2$InsertedDataEventsThread;->db:Landroid/database/sqlite/SQLiteDatabase;

    const-string v3, "view_data"

    const/16 v4, 0xb

    new-array v4, v4, [Ljava/lang/String;

    const/4 v5, 0x0

    const-string v6, "_id"

    aput-object v6, v4, v5

    const/4 v5, 0x1

    const-string v6, "data1"

    aput-object v6, v4, v5

    const/4 v5, 0x2

    const-string v6, "data2"

    aput-object v6, v4, v5

    const/4 v5, 0x3

    const-string v6, "data3"

    aput-object v6, v4, v5

    const/4 v5, 0x4

    const-string v6, "raw_contact_id"

    aput-object v6, v4, v5

    const/4 v5, 0x5

    const-string v6, "account_name"

    aput-object v6, v4, v5

    const/4 v5, 0x6

    const-string v6, "account_type"

    aput-object v6, v4, v5

    const/4 v5, 0x7

    const-string v6, "display_name"

    aput-object v6, v4, v5

    const/16 v5, 0x8

    const-string v6, "display_name_alt"

    aput-object v6, v4, v5

    const/16 v5, 0x9

    const-string v6, "data14"

    aput-object v6, v4, v5

    const/16 v5, 0xa

    const-string v6, "data15"

    aput-object v6, v4, v5

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "mimetype = ? AND _id in "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/android/providers/contacts/ContactsProvider2$InsertedDataEventsThread;->ids:Ljava/lang/String;

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    const/4 v6, 0x1

    new-array v6, v6, [Ljava/lang/String;

    const/4 v7, 0x0

    const-string v8, "vnd.android.cursor.item/contact_event"

    aput-object v8, v6, v7

    const/4 v7, 0x0

    const/4 v8, 0x0

    const/4 v9, 0x0

    invoke-virtual/range {v2 .. v9}, Landroid/database/sqlite/SQLiteDatabase;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v12

    .line 4100
    if-eqz v12, :cond_d

    .line 4101
    const-string v2, "_id"

    invoke-interface {v12, v2}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v22

    .line 4102
    .local v22, "indexId":I
    const-string v2, "data1"

    invoke-interface {v12, v2}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v26

    .line 4103
    .local v26, "indexStartDate":I
    const-string v2, "data2"

    invoke-interface {v12, v2}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v27

    .line 4105
    .local v27, "indexType":I
    const-string v2, "data3"

    invoke-interface {v12, v2}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v23

    .line 4106
    .local v23, "indexLabel":I
    const-string v2, "raw_contact_id"

    invoke-interface {v12, v2}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v24

    .line 4107
    .local v24, "indexRawContactsId":I
    const-string v2, "account_name"

    invoke-interface {v12, v2}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v17

    .line 4108
    .local v17, "indexAccountName":I
    const-string v2, "account_type"

    invoke-interface {v12, v2}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v18

    .line 4109
    .local v18, "indexAccountType":I
    const-string v2, "display_name"

    invoke-interface {v12, v2}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v20

    .line 4110
    .local v20, "indexDisplayName":I
    const-string v2, "display_name_alt"

    invoke-interface {v12, v2}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v21

    .line 4111
    .local v21, "indexDisplayNameReverse":I
    const-string v2, "data15"

    invoke-interface {v12, v2}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v19

    .line 4112
    .local v19, "indexDateType":I
    const-string v2, "data14"

    invoke-interface {v12, v2}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v25

    .line 4113
    .local v25, "indexSolarDate":I
    const/16 v33, 0x0

    .line 4115
    .local v33, "titleBuilder":Ljava/lang/StringBuilder;
    :cond_0
    :goto_0
    invoke-interface {v12}, Landroid/database/Cursor;->moveToNext()Z

    move-result v2

    if-eqz v2, :cond_d

    .line 4117
    move/from16 v0, v19

    invoke-interface {v12, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v14

    .line 4118
    .local v14, "dateType":I
    invoke-static {}, Lcom/sec/android/app/CscFeature;->getInstance()Lcom/sec/android/app/CscFeature;

    move-result-object v2

    const-string v3, "CscFeature_Calendar_EnableLunar"

    invoke-virtual {v2, v3}, Lcom/sec/android/app/CscFeature;->getEnableStatus(Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_7

    .line 4120
    move/from16 v0, v26

    invoke-interface {v12, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v13

    .line 4132
    .local v13, "date":Ljava/lang/String;
    :cond_1
    :goto_1
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/android/providers/contacts/ContactsProvider2$InsertedDataEventsThread;->this$0:Lcom/android/providers/contacts/ContactsProvider2;

    # invokes: Lcom/android/providers/contacts/ContactsProvider2;->convertDate(Ljava/lang/String;)J
    invoke-static {v2, v13}, Lcom/android/providers/contacts/ContactsProvider2;->access$100(Lcom/android/providers/contacts/ContactsProvider2;Ljava/lang/String;)J

    move-result-wide v30

    .line 4133
    .local v30, "startMillis":J
    new-instance v33, Ljava/lang/StringBuilder;

    .end local v33    # "titleBuilder":Ljava/lang/StringBuilder;
    invoke-direct/range {v33 .. v33}, Ljava/lang/StringBuilder;-><init>()V

    .line 4134
    .restart local v33    # "titleBuilder":Ljava/lang/StringBuilder;
    const/4 v2, 0x1

    move/from16 v0, v29

    if-ne v0, v2, :cond_9

    .line 4135
    move/from16 v0, v20

    invoke-interface {v12, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v2

    if-eqz v2, :cond_2

    .line 4136
    move/from16 v0, v20

    invoke-interface {v12, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, v33

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 4137
    const-string v2, ". "

    move-object/from16 v0, v33

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 4147
    :cond_2
    :goto_2
    move/from16 v0, v27

    invoke-interface {v12, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v2

    if-nez v2, :cond_b

    .line 4148
    move/from16 v0, v23

    invoke-interface {v12, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v2

    if-eqz v2, :cond_3

    .line 4149
    move/from16 v0, v23

    invoke-interface {v12, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, v33

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 4157
    :cond_3
    :goto_3
    new-instance v34, Landroid/content/ContentValues;

    invoke-direct/range {v34 .. v34}, Landroid/content/ContentValues;-><init>()V

    .line 4160
    .local v34, "values":Landroid/content/ContentValues;
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    move/from16 v0, v18

    invoke-interface {v12, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "#"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    move/from16 v0, v17

    invoke-interface {v12, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "#"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "1"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    .line 4162
    .local v11, "contactAccountType":Ljava/lang/String;
    invoke-static {}, Lcom/android/providers/contacts/util/LoadCscFeatureUtils;->getInstance()Lcom/android/providers/contacts/util/LoadCscFeatureUtils;

    move-result-object v2

    invoke-virtual {v2}, Lcom/android/providers/contacts/util/LoadCscFeatureUtils;->getEnableMultiPinyinSearch()Z

    move-result v2

    if-eqz v2, :cond_4

    const/16 v2, 0x22

    invoke-virtual {v11, v2}, Ljava/lang/String;->indexOf(I)I

    move-result v2

    const/4 v3, -0x1

    if-eq v2, v3, :cond_4

    .line 4166
    const-string v2, "ContactsProvider"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "contact_account_type"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/secutil/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I

    .line 4167
    const-string v2, "\""

    const-string v3, ""

    invoke-virtual {v11, v2, v3}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v11

    .line 4169
    :cond_4
    const-string v2, "contact_account_type"

    move-object/from16 v0, v34

    invoke-virtual {v0, v2, v11}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 4173
    const-string v2, "calendar_id"

    const/4 v3, 0x1

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    move-object/from16 v0, v34

    invoke-virtual {v0, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 4174
    const-string v2, "title"

    invoke-virtual/range {v33 .. v33}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    move-object/from16 v0, v34

    invoke-virtual {v0, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 4175
    const-string v2, "allDay"

    const/4 v3, 0x1

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    move-object/from16 v0, v34

    invoke-virtual {v0, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 4177
    const-string v2, "dtstart"

    invoke-static/range {v30 .. v31}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    move-object/from16 v0, v34

    invoke-virtual {v0, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 4178
    const-string v2, "eventTimezone"

    move-object/from16 v0, v34

    move-object/from16 v1, v32

    invoke-virtual {v0, v2, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 4180
    const-string v2, "duration"

    const-string v3, "P1D"

    move-object/from16 v0, v34

    invoke-virtual {v0, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 4182
    const-string v2, "rrule"

    const-string v3, "FREQ=YEARLY;UNTIL=20361231T000000Z;WKST=SU"

    move-object/from16 v0, v34

    invoke-virtual {v0, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 4184
    const-string v2, "description"

    const-string v3, ""

    move-object/from16 v0, v34

    invoke-virtual {v0, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 4185
    const-string v2, "eventLocation"

    const-string v3, ""

    move-object/from16 v0, v34

    invoke-virtual {v0, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 4186
    const-string v2, "accessLevel"

    const/4 v3, 0x3

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    move-object/from16 v0, v34

    invoke-virtual {v0, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 4187
    const-string v2, "contact_data_id"

    move/from16 v0, v22

    invoke-interface {v12, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v3

    move-object/from16 v0, v34

    invoke-virtual {v0, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 4188
    const-string v2, "contact_id"

    move/from16 v0, v24

    invoke-interface {v12, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v3

    move-object/from16 v0, v34

    invoke-virtual {v0, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 4190
    move/from16 v0, v27

    invoke-interface {v12, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v16

    .line 4191
    .local v16, "eventType":Ljava/lang/String;
    if-eqz v16, :cond_0

    .line 4193
    const-string v2, "contactEventType"

    move-object/from16 v0, v34

    move-object/from16 v1, v16

    invoke-virtual {v0, v2, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 4194
    invoke-static {}, Lcom/sec/android/app/CscFeature;->getInstance()Lcom/sec/android/app/CscFeature;

    move-result-object v2

    const-string v3, "CscFeature_Calendar_EnableLunar"

    invoke-virtual {v2, v3}, Lcom/sec/android/app/CscFeature;->getEnableStatus(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_5

    .line 4195
    const-string v3, "setLunar"

    if-nez v14, :cond_c

    const-string v2, "0"

    :goto_4
    move-object/from16 v0, v34

    invoke-virtual {v0, v3, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 4198
    :cond_5
    sget-object v2, Landroid/provider/CalendarContract$Events;->CONTENT_URI:Landroid/net/Uri;

    invoke-static {v2}, Landroid/content/ContentProviderOperation;->newInsert(Landroid/net/Uri;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v2

    move-object/from16 v0, v34

    invoke-virtual {v2, v0}, Landroid/content/ContentProviderOperation$Builder;->withValues(Landroid/content/ContentValues;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v10

    .line 4199
    .local v10, "b":Landroid/content/ContentProviderOperation$Builder;
    invoke-virtual {v10}, Landroid/content/ContentProviderOperation$Builder;->build()Landroid/content/ContentProviderOperation;

    move-result-object v2

    move-object/from16 v0, v28

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z
    :try_end_0
    .catch Ljava/lang/OutOfMemoryError; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto/16 :goto_0

    .line 4202
    .end local v10    # "b":Landroid/content/ContentProviderOperation$Builder;
    .end local v11    # "contactAccountType":Ljava/lang/String;
    .end local v13    # "date":Ljava/lang/String;
    .end local v14    # "dateType":I
    .end local v16    # "eventType":Ljava/lang/String;
    .end local v17    # "indexAccountName":I
    .end local v18    # "indexAccountType":I
    .end local v19    # "indexDateType":I
    .end local v20    # "indexDisplayName":I
    .end local v21    # "indexDisplayNameReverse":I
    .end local v22    # "indexId":I
    .end local v23    # "indexLabel":I
    .end local v24    # "indexRawContactsId":I
    .end local v25    # "indexSolarDate":I
    .end local v26    # "indexStartDate":I
    .end local v27    # "indexType":I
    .end local v30    # "startMillis":J
    .end local v33    # "titleBuilder":Ljava/lang/StringBuilder;
    .end local v34    # "values":Landroid/content/ContentValues;
    :catch_0
    move-exception v15

    .line 4203
    .local v15, "e":Ljava/lang/OutOfMemoryError;
    :try_start_1
    const-string v2, "ContactsProvider"

    const-string v3, "Ignoring unexpected outofmemory exception in InsertedDataEventsThread"

    invoke-static {v2, v3, v15}, Landroid/util/secutil/Log;->secW(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 4206
    if-eqz v12, :cond_6

    .line 4207
    invoke-interface {v12}, Landroid/database/Cursor;->close()V

    .line 4212
    .end local v15    # "e":Ljava/lang/OutOfMemoryError;
    :cond_6
    :goto_5
    :try_start_2
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/android/providers/contacts/ContactsProvider2$InsertedDataEventsThread;->this$0:Lcom/android/providers/contacts/ContactsProvider2;

    invoke-virtual {v2}, Lcom/android/providers/contacts/ContactsProvider2;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    const-string v3, "com.android.calendar"

    move-object/from16 v0, v28

    invoke-virtual {v2, v3, v0}, Landroid/content/ContentResolver;->applyBatch(Ljava/lang/String;Ljava/util/ArrayList;)[Landroid/content/ContentProviderResult;
    :try_end_2
    .catch Landroid/os/RemoteException; {:try_start_2 .. :try_end_2} :catch_1
    .catch Landroid/content/OperationApplicationException; {:try_start_2 .. :try_end_2} :catch_2
    .catch Ljava/lang/IllegalArgumentException; {:try_start_2 .. :try_end_2} :catch_3

    .line 4220
    :goto_6
    return-void

    .line 4123
    .restart local v14    # "dateType":I
    .restart local v17    # "indexAccountName":I
    .restart local v18    # "indexAccountType":I
    .restart local v19    # "indexDateType":I
    .restart local v20    # "indexDisplayName":I
    .restart local v21    # "indexDisplayNameReverse":I
    .restart local v22    # "indexId":I
    .restart local v23    # "indexLabel":I
    .restart local v24    # "indexRawContactsId":I
    .restart local v25    # "indexSolarDate":I
    .restart local v26    # "indexStartDate":I
    .restart local v27    # "indexType":I
    .restart local v33    # "titleBuilder":Ljava/lang/StringBuilder;
    :cond_7
    if-nez v14, :cond_8

    .line 4124
    :try_start_3
    move/from16 v0, v26

    invoke-interface {v12, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v13

    .restart local v13    # "date":Ljava/lang/String;
    goto/16 :goto_1

    .line 4126
    .end local v13    # "date":Ljava/lang/String;
    :cond_8
    move/from16 v0, v25

    invoke-interface {v12, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v13

    .line 4127
    .restart local v13    # "date":Ljava/lang/String;
    invoke-static {v13}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_1

    goto/16 :goto_0

    .line 4140
    .restart local v30    # "startMillis":J
    :cond_9
    move/from16 v0, v21

    invoke-interface {v12, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v2

    if-eqz v2, :cond_2

    .line 4141
    move/from16 v0, v21

    invoke-interface {v12, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, v33

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 4142
    const-string v2, ". "

    move-object/from16 v0, v33

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
    :try_end_3
    .catch Ljava/lang/OutOfMemoryError; {:try_start_3 .. :try_end_3} :catch_0
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    goto/16 :goto_2

    .line 4206
    .end local v13    # "date":Ljava/lang/String;
    .end local v14    # "dateType":I
    .end local v17    # "indexAccountName":I
    .end local v18    # "indexAccountType":I
    .end local v19    # "indexDateType":I
    .end local v20    # "indexDisplayName":I
    .end local v21    # "indexDisplayNameReverse":I
    .end local v22    # "indexId":I
    .end local v23    # "indexLabel":I
    .end local v24    # "indexRawContactsId":I
    .end local v25    # "indexSolarDate":I
    .end local v26    # "indexStartDate":I
    .end local v27    # "indexType":I
    .end local v30    # "startMillis":J
    .end local v33    # "titleBuilder":Ljava/lang/StringBuilder;
    :catchall_0
    move-exception v2

    if-eqz v12, :cond_a

    .line 4207
    invoke-interface {v12}, Landroid/database/Cursor;->close()V

    :cond_a
    throw v2

    .line 4152
    .restart local v13    # "date":Ljava/lang/String;
    .restart local v14    # "dateType":I
    .restart local v17    # "indexAccountName":I
    .restart local v18    # "indexAccountType":I
    .restart local v19    # "indexDateType":I
    .restart local v20    # "indexDisplayName":I
    .restart local v21    # "indexDisplayNameReverse":I
    .restart local v22    # "indexId":I
    .restart local v23    # "indexLabel":I
    .restart local v24    # "indexRawContactsId":I
    .restart local v25    # "indexSolarDate":I
    .restart local v26    # "indexStartDate":I
    .restart local v27    # "indexType":I
    .restart local v30    # "startMillis":J
    .restart local v33    # "titleBuilder":Ljava/lang/StringBuilder;
    :cond_b
    :try_start_4
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/android/providers/contacts/ContactsProvider2$InsertedDataEventsThread;->this$0:Lcom/android/providers/contacts/ContactsProvider2;

    invoke-virtual {v2}, Lcom/android/providers/contacts/ContactsProvider2;->getContext()Landroid/content/Context;

    move-result-object v2

    move/from16 v0, v27

    invoke-interface {v12, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v3

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-static {v3}, Landroid/provider/ContactsContract$CommonDataKinds$Event;->getTypeResource(Ljava/lang/Integer;)I

    move-result v3

    invoke-virtual {v2, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    if-eqz v2, :cond_3

    .line 4153
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/android/providers/contacts/ContactsProvider2$InsertedDataEventsThread;->this$0:Lcom/android/providers/contacts/ContactsProvider2;

    invoke-virtual {v2}, Lcom/android/providers/contacts/ContactsProvider2;->getContext()Landroid/content/Context;

    move-result-object v2

    move/from16 v0, v27

    invoke-interface {v12, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v3

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-static {v3}, Landroid/provider/ContactsContract$CommonDataKinds$Event;->getTypeResource(Ljava/lang/Integer;)I

    move-result v3

    invoke-virtual {v2, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, v33

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto/16 :goto_3

    .line 4195
    .restart local v11    # "contactAccountType":Ljava/lang/String;
    .restart local v16    # "eventType":Ljava/lang/String;
    .restart local v34    # "values":Landroid/content/ContentValues;
    :cond_c
    const-string v2, "1"
    :try_end_4
    .catch Ljava/lang/OutOfMemoryError; {:try_start_4 .. :try_end_4} :catch_0
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    goto/16 :goto_4

    .line 4206
    .end local v11    # "contactAccountType":Ljava/lang/String;
    .end local v13    # "date":Ljava/lang/String;
    .end local v14    # "dateType":I
    .end local v16    # "eventType":Ljava/lang/String;
    .end local v17    # "indexAccountName":I
    .end local v18    # "indexAccountType":I
    .end local v19    # "indexDateType":I
    .end local v20    # "indexDisplayName":I
    .end local v21    # "indexDisplayNameReverse":I
    .end local v22    # "indexId":I
    .end local v23    # "indexLabel":I
    .end local v24    # "indexRawContactsId":I
    .end local v25    # "indexSolarDate":I
    .end local v26    # "indexStartDate":I
    .end local v27    # "indexType":I
    .end local v30    # "startMillis":J
    .end local v33    # "titleBuilder":Ljava/lang/StringBuilder;
    .end local v34    # "values":Landroid/content/ContentValues;
    :cond_d
    if-eqz v12, :cond_6

    .line 4207
    invoke-interface {v12}, Landroid/database/Cursor;->close()V

    goto/16 :goto_5

    .line 4213
    :catch_1
    move-exception v15

    .line 4214
    .local v15, "e":Landroid/os/RemoteException;
    const-string v2, "ContactsProvider"

    const-string v3, "Ignoring unexpected remote exception"

    invoke-static {v2, v3, v15}, Landroid/util/secutil/Log;->secW(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto/16 :goto_6

    .line 4215
    .end local v15    # "e":Landroid/os/RemoteException;
    :catch_2
    move-exception v15

    .line 4216
    .local v15, "e":Landroid/content/OperationApplicationException;
    const-string v2, "ContactsProvider"

    const-string v3, "Ignoring unexpected exception"

    invoke-static {v2, v3, v15}, Landroid/util/secutil/Log;->secW(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto/16 :goto_6

    .line 4217
    .end local v15    # "e":Landroid/content/OperationApplicationException;
    :catch_3
    move-exception v15

    .line 4218
    .local v15, "e":Ljava/lang/IllegalArgumentException;
    const-string v2, "ContactsProvider"

    const-string v3, "Ignoring unexpected illegal argument exception"

    invoke-static {v2, v3, v15}, Landroid/util/secutil/Log;->secW(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto/16 :goto_6
.end method
