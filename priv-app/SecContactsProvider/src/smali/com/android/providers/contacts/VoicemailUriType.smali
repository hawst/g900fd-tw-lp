.class final enum Lcom/android/providers/contacts/VoicemailUriType;
.super Ljava/lang/Enum;
.source "VoicemailUriType.java"

# interfaces
.implements Lcom/android/providers/contacts/util/UriType;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/android/providers/contacts/VoicemailUriType;",
        ">;",
        "Lcom/android/providers/contacts/util/UriType;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/android/providers/contacts/VoicemailUriType;

.field public static final enum FROM_CALLLOG:Lcom/android/providers/contacts/VoicemailUriType;

.field public static final enum FROM_CALLLOG_ID:Lcom/android/providers/contacts/VoicemailUriType;

.field public static final enum NO_MATCH:Lcom/android/providers/contacts/VoicemailUriType;

.field public static final enum STATUS:Lcom/android/providers/contacts/VoicemailUriType;

.field public static final enum STATUS_ID:Lcom/android/providers/contacts/VoicemailUriType;

.field public static final enum VOICEMAILS:Lcom/android/providers/contacts/VoicemailUriType;

.field public static final enum VOICEMAILS_ID:Lcom/android/providers/contacts/VoicemailUriType;


# instance fields
.field private final path:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 9

    .prologue
    const/4 v8, 0x4

    const/4 v7, 0x3

    const/4 v6, 0x2

    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 24
    new-instance v0, Lcom/android/providers/contacts/VoicemailUriType;

    const-string v1, "NO_MATCH"

    const/4 v2, 0x0

    invoke-direct {v0, v1, v4, v2}, Lcom/android/providers/contacts/VoicemailUriType;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/android/providers/contacts/VoicemailUriType;->NO_MATCH:Lcom/android/providers/contacts/VoicemailUriType;

    .line 25
    new-instance v0, Lcom/android/providers/contacts/VoicemailUriType;

    const-string v1, "VOICEMAILS"

    const-string v2, "voicemail"

    invoke-direct {v0, v1, v5, v2}, Lcom/android/providers/contacts/VoicemailUriType;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/android/providers/contacts/VoicemailUriType;->VOICEMAILS:Lcom/android/providers/contacts/VoicemailUriType;

    .line 26
    new-instance v0, Lcom/android/providers/contacts/VoicemailUriType;

    const-string v1, "VOICEMAILS_ID"

    const-string v2, "voicemail/#"

    invoke-direct {v0, v1, v6, v2}, Lcom/android/providers/contacts/VoicemailUriType;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/android/providers/contacts/VoicemailUriType;->VOICEMAILS_ID:Lcom/android/providers/contacts/VoicemailUriType;

    .line 27
    new-instance v0, Lcom/android/providers/contacts/VoicemailUriType;

    const-string v1, "STATUS"

    const-string v2, "status"

    invoke-direct {v0, v1, v7, v2}, Lcom/android/providers/contacts/VoicemailUriType;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/android/providers/contacts/VoicemailUriType;->STATUS:Lcom/android/providers/contacts/VoicemailUriType;

    .line 28
    new-instance v0, Lcom/android/providers/contacts/VoicemailUriType;

    const-string v1, "STATUS_ID"

    const-string v2, "status/#"

    invoke-direct {v0, v1, v8, v2}, Lcom/android/providers/contacts/VoicemailUriType;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/android/providers/contacts/VoicemailUriType;->STATUS_ID:Lcom/android/providers/contacts/VoicemailUriType;

    .line 29
    new-instance v0, Lcom/android/providers/contacts/VoicemailUriType;

    const-string v1, "FROM_CALLLOG"

    const/4 v2, 0x5

    const-string v3, "from_calllog"

    invoke-direct {v0, v1, v2, v3}, Lcom/android/providers/contacts/VoicemailUriType;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/android/providers/contacts/VoicemailUriType;->FROM_CALLLOG:Lcom/android/providers/contacts/VoicemailUriType;

    .line 30
    new-instance v0, Lcom/android/providers/contacts/VoicemailUriType;

    const-string v1, "FROM_CALLLOG_ID"

    const/4 v2, 0x6

    const-string v3, "from_calllog/#"

    invoke-direct {v0, v1, v2, v3}, Lcom/android/providers/contacts/VoicemailUriType;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/android/providers/contacts/VoicemailUriType;->FROM_CALLLOG_ID:Lcom/android/providers/contacts/VoicemailUriType;

    .line 23
    const/4 v0, 0x7

    new-array v0, v0, [Lcom/android/providers/contacts/VoicemailUriType;

    sget-object v1, Lcom/android/providers/contacts/VoicemailUriType;->NO_MATCH:Lcom/android/providers/contacts/VoicemailUriType;

    aput-object v1, v0, v4

    sget-object v1, Lcom/android/providers/contacts/VoicemailUriType;->VOICEMAILS:Lcom/android/providers/contacts/VoicemailUriType;

    aput-object v1, v0, v5

    sget-object v1, Lcom/android/providers/contacts/VoicemailUriType;->VOICEMAILS_ID:Lcom/android/providers/contacts/VoicemailUriType;

    aput-object v1, v0, v6

    sget-object v1, Lcom/android/providers/contacts/VoicemailUriType;->STATUS:Lcom/android/providers/contacts/VoicemailUriType;

    aput-object v1, v0, v7

    sget-object v1, Lcom/android/providers/contacts/VoicemailUriType;->STATUS_ID:Lcom/android/providers/contacts/VoicemailUriType;

    aput-object v1, v0, v8

    const/4 v1, 0x5

    sget-object v2, Lcom/android/providers/contacts/VoicemailUriType;->FROM_CALLLOG:Lcom/android/providers/contacts/VoicemailUriType;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, Lcom/android/providers/contacts/VoicemailUriType;->FROM_CALLLOG_ID:Lcom/android/providers/contacts/VoicemailUriType;

    aput-object v2, v0, v1

    sput-object v0, Lcom/android/providers/contacts/VoicemailUriType;->$VALUES:[Lcom/android/providers/contacts/VoicemailUriType;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;ILjava/lang/String;)V
    .locals 0
    .param p3, "path"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    .line 34
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 35
    iput-object p3, p0, Lcom/android/providers/contacts/VoicemailUriType;->path:Ljava/lang/String;

    .line 36
    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/android/providers/contacts/VoicemailUriType;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 23
    const-class v0, Lcom/android/providers/contacts/VoicemailUriType;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/android/providers/contacts/VoicemailUriType;

    return-object v0
.end method

.method public static values()[Lcom/android/providers/contacts/VoicemailUriType;
    .locals 1

    .prologue
    .line 23
    sget-object v0, Lcom/android/providers/contacts/VoicemailUriType;->$VALUES:[Lcom/android/providers/contacts/VoicemailUriType;

    invoke-virtual {v0}, [Lcom/android/providers/contacts/VoicemailUriType;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/android/providers/contacts/VoicemailUriType;

    return-object v0
.end method


# virtual methods
.method public path()Ljava/lang/String;
    .locals 1

    .prologue
    .line 40
    iget-object v0, p0, Lcom/android/providers/contacts/VoicemailUriType;->path:Ljava/lang/String;

    return-object v0
.end method
