.class Lcom/android/providers/contacts/ContactsProvider2$DeletedDataEventsThread;
.super Ljava/lang/Thread;
.source "ContactsProvider2.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/providers/contacts/ContactsProvider2;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "DeletedDataEventsThread"
.end annotation


# instance fields
.field private ids:Ljava/lang/String;

.field final synthetic this$0:Lcom/android/providers/contacts/ContactsProvider2;


# direct methods
.method public constructor <init>(Lcom/android/providers/contacts/ContactsProvider2;Ljava/lang/String;)V
    .locals 1
    .param p2, "ids"    # Ljava/lang/String;

    .prologue
    .line 4050
    iput-object p1, p0, Lcom/android/providers/contacts/ContactsProvider2$DeletedDataEventsThread;->this$0:Lcom/android/providers/contacts/ContactsProvider2;

    .line 4051
    const-string v0, "DeletedDataEventsThread"

    invoke-direct {p0, v0}, Ljava/lang/Thread;-><init>(Ljava/lang/String;)V

    .line 4048
    const-string v0, ""

    iput-object v0, p0, Lcom/android/providers/contacts/ContactsProvider2$DeletedDataEventsThread;->ids:Ljava/lang/String;

    .line 4052
    iput-object p2, p0, Lcom/android/providers/contacts/ContactsProvider2$DeletedDataEventsThread;->ids:Ljava/lang/String;

    .line 4053
    return-void
.end method


# virtual methods
.method public run()V
    .locals 5

    .prologue
    .line 4059
    :try_start_0
    iget-object v1, p0, Lcom/android/providers/contacts/ContactsProvider2$DeletedDataEventsThread;->this$0:Lcom/android/providers/contacts/ContactsProvider2;

    invoke-virtual {v1}, Lcom/android/providers/contacts/ContactsProvider2;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    sget-object v2, Landroid/provider/CalendarContract$Events;->CONTENT_URI:Landroid/net/Uri;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "contact_id IS NOT NULL AND contact_data_id in "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p0, Lcom/android/providers/contacts/ContactsProvider2$DeletedDataEventsThread;->ids:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    const/4 v4, 0x0

    invoke-virtual {v1, v2, v3, v4}, Landroid/content/ContentResolver;->delete(Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0

    .line 4065
    :goto_0
    return-void

    .line 4061
    :catch_0
    move-exception v0

    .line 4062
    .local v0, "e":Ljava/lang/IllegalArgumentException;
    const-string v1, "ContactsProvider"

    const-string v2, "Ignoring unexpected illegal argument exception"

    invoke-static {v1, v2, v0}, Landroid/util/secutil/Log;->secW(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_0
.end method
