.class public Lcom/android/providers/contacts/Constants;
.super Ljava/lang/Object;
.source "Constants.java"


# static fields
.field public static final ADN_ANR_EXPANSION_URI:Landroid/net/Uri;

.field public static final ADN_FROM_CONTACTS_URI:Landroid/net/Uri;

.field public static final ADN_SUBID_ANR_EXPANSION_URI:Landroid/net/Uri;

.field public static final ADN_SUBID_FROM_CONTACTS_URI:Landroid/net/Uri;

.field public static final ADN_SUBID_URI:Landroid/net/Uri;

.field public static final ADN_URI:Landroid/net/Uri;

.field public static final LINKED_CONTACT_COUNT_LIMIT:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 70
    invoke-static {}, Lcom/android/providers/contacts/util/LoadCscFeatureUtils;->getInstance()Lcom/android/providers/contacts/util/LoadCscFeatureUtils;

    move-result-object v0

    invoke-virtual {v0}, Lcom/android/providers/contacts/util/LoadCscFeatureUtils;->getMaxLinkCount()I

    move-result v0

    sput v0, Lcom/android/providers/contacts/Constants;->LINKED_CONTACT_COUNT_LIMIT:I

    .line 72
    const-string v0, "content://icc/adn"

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    sput-object v0, Lcom/android/providers/contacts/Constants;->ADN_URI:Landroid/net/Uri;

    .line 74
    const-string v0, "content://icc/adn/subId"

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    sput-object v0, Lcom/android/providers/contacts/Constants;->ADN_SUBID_URI:Landroid/net/Uri;

    .line 76
    const-string v0, "content://icc/adn/from_contacts"

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    sput-object v0, Lcom/android/providers/contacts/Constants;->ADN_FROM_CONTACTS_URI:Landroid/net/Uri;

    .line 78
    const-string v0, "content://icc/adn/from_contacts/subId"

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    sput-object v0, Lcom/android/providers/contacts/Constants;->ADN_SUBID_FROM_CONTACTS_URI:Landroid/net/Uri;

    .line 80
    const-string v0, "content://icc/adn/expansion"

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    sput-object v0, Lcom/android/providers/contacts/Constants;->ADN_ANR_EXPANSION_URI:Landroid/net/Uri;

    .line 82
    const-string v0, "content://icc/adn/expansion/subId"

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    sput-object v0, Lcom/android/providers/contacts/Constants;->ADN_SUBID_ANR_EXPANSION_URI:Landroid/net/Uri;

    return-void
.end method

.method private constructor <init>()V
    .locals 0

    .prologue
    .line 24
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static final getSubIdFromSlotId()J
    .locals 2

    .prologue
    .line 141
    const/4 v0, 0x1

    invoke-static {v0}, Landroid/telephony/SubscriptionManager;->getSubId(I)[J

    move-result-object v0

    const/4 v1, 0x0

    aget-wide v0, v0, v1

    return-wide v0
.end method
