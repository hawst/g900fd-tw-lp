.class final Lcom/android/providers/contacts/PhoneLookupWithStarPrefix;
.super Ljava/lang/Object;
.source "PhoneLookupWithStarPrefix.java"


# direct methods
.method constructor <init>()V
    .locals 0

    .prologue
    .line 30
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private static cursorValue(Landroid/database/Cursor;I)Ljava/lang/Object;
    .locals 4
    .param p0, "cursor"    # Landroid/database/Cursor;
    .param p1, "column"    # I

    .prologue
    const/4 v0, 0x0

    .line 135
    invoke-interface {p0, p1}, Landroid/database/Cursor;->getType(I)I

    move-result v1

    packed-switch v1, :pswitch_data_0

    .line 147
    const-string v1, "PhoneLookupWSP"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Invalid value in cursor: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-interface {p0, p1}, Landroid/database/Cursor;->getType(I)I

    move-result v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 148
    :goto_0
    :pswitch_0
    return-object v0

    .line 137
    :pswitch_1
    invoke-interface {p0, p1}, Landroid/database/Cursor;->getBlob(I)[B

    move-result-object v0

    goto :goto_0

    .line 139
    :pswitch_2
    invoke-interface {p0, p1}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    goto :goto_0

    .line 141
    :pswitch_3
    invoke-interface {p0, p1}, Landroid/database/Cursor;->getFloat(I)F

    move-result v0

    invoke-static {v0}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v0

    goto :goto_0

    .line 143
    :pswitch_4
    invoke-interface {p0, p1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 135
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_1
    .end packed-switch
.end method

.method private static matchingNumberStartsWithStar(Landroid/database/Cursor;)Z
    .locals 3
    .param p0, "cursor"    # Landroid/database/Cursor;

    .prologue
    .line 123
    const/4 v2, -0x1

    invoke-interface {p0, v2}, Landroid/database/Cursor;->moveToPosition(I)Z

    .line 124
    :cond_0
    invoke-interface {p0}, Landroid/database/Cursor;->moveToNext()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 125
    const-string v2, "number"

    invoke-interface {p0, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    .line 126
    .local v0, "numberIndex":I
    invoke-interface {p0, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/android/providers/contacts/PhoneLookupWithStarPrefix;->normalizeNumberWithStar(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 127
    .local v1, "phoneNumber":Ljava/lang/String;
    const-string v2, "*"

    invoke-virtual {v1, v2}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 128
    const/4 v2, 0x1

    .line 131
    .end local v0    # "numberIndex":I
    .end local v1    # "phoneNumber":Ljava/lang/String;
    :goto_0
    return v2

    :cond_1
    const/4 v2, 0x0

    goto :goto_0
.end method

.method static normalizeNumberWithStar(Ljava/lang/String;)Ljava/lang/String;
    .locals 4
    .param p0, "phoneNumber"    # Ljava/lang/String;

    .prologue
    .line 105
    invoke-static {p0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 116
    .end local p0    # "phoneNumber":Ljava/lang/String;
    :goto_0
    return-object p0

    .line 108
    .restart local p0    # "phoneNumber":Ljava/lang/String;
    :cond_0
    const-string v0, "*"

    invoke-virtual {p0, v0}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 113
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "*"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const/4 v1, 0x1

    invoke-virtual {p0, v1}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v1

    const-string v2, "+"

    const-string v3, ""

    invoke-virtual {v1, v2, v3}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Landroid/telephony/PhoneNumberUtils;->normalizeNumber(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p0

    goto :goto_0

    .line 116
    :cond_1
    invoke-static {p0}, Landroid/telephony/PhoneNumberUtils;->normalizeNumber(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p0

    goto :goto_0
.end method

.method public static removeNonStarMatchesFromCursor(Ljava/lang/String;Landroid/database/Cursor;)Landroid/database/Cursor;
    .locals 10
    .param p0, "number"    # Ljava/lang/String;
    .param p1, "cursor"    # Landroid/database/Cursor;

    .prologue
    .line 52
    move-object v6, p1

    .line 55
    .local v6, "unreturnedCursor":Landroid/database/Cursor;
    :try_start_0
    invoke-static {p0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result v8

    if-eqz v8, :cond_1

    .line 56
    const/4 v6, 0x0

    .line 97
    if-eqz v6, :cond_0

    .line 98
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    .end local p1    # "cursor":Landroid/database/Cursor;
    :cond_0
    :goto_0
    return-object p1

    .line 60
    .restart local p1    # "cursor":Landroid/database/Cursor;
    :cond_1
    :try_start_1
    invoke-static {p0}, Lcom/android/providers/contacts/PhoneLookupWithStarPrefix;->normalizeNumberWithStar(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    .line 61
    .local v5, "queryPhoneNumberNormalized":Ljava/lang/String;
    const-string v8, "*"

    invoke-virtual {v5, v8}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v8

    if-nez v8, :cond_2

    invoke-static {p1}, Lcom/android/providers/contacts/PhoneLookupWithStarPrefix;->matchingNumberStartsWithStar(Landroid/database/Cursor;)Z

    move-result v8

    if-nez v8, :cond_2

    .line 63
    const/4 v8, -0x1

    invoke-interface {p1, v8}, Landroid/database/Cursor;->moveToPosition(I)Z
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 64
    const/4 v6, 0x0

    .line 97
    if-eqz v6, :cond_0

    .line 98
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    goto :goto_0

    .line 68
    :cond_2
    :try_start_2
    new-instance v3, Landroid/database/MatrixCursor;

    invoke-interface {p1}, Landroid/database/Cursor;->getColumnNames()[Ljava/lang/String;

    move-result-object v8

    invoke-direct {v3, v8}, Landroid/database/MatrixCursor;-><init>([Ljava/lang/String;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 71
    .local v3, "matrixCursor":Landroid/database/MatrixCursor;
    move-object v7, v3

    .line 74
    .local v7, "unreturnedMatrixCursor":Landroid/database/Cursor;
    const/4 v8, -0x1

    :try_start_3
    invoke-interface {p1, v8}, Landroid/database/Cursor;->moveToPosition(I)Z

    .line 75
    :cond_3
    invoke-interface {p1}, Landroid/database/Cursor;->moveToNext()Z

    move-result v8

    if-eqz v8, :cond_6

    .line 76
    const-string v8, "number"

    invoke-interface {p1, v8}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v4

    .line 77
    .local v4, "numberIndex":I
    invoke-interface {p1, v4}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v8

    invoke-static {v8}, Lcom/android/providers/contacts/PhoneLookupWithStarPrefix;->normalizeNumberWithStar(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 79
    .local v2, "matchingNumberNormalized":Ljava/lang/String;
    const-string v8, "*"

    invoke-virtual {v2, v8}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v8

    if-nez v8, :cond_4

    const-string v8, "*"

    invoke-virtual {v5, v8}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v8

    if-eqz v8, :cond_5

    :cond_4
    invoke-virtual {v2, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_3

    .line 83
    :cond_5
    invoke-virtual {v3}, Landroid/database/MatrixCursor;->newRow()Landroid/database/MatrixCursor$RowBuilder;

    move-result-object v0

    .line 84
    .local v0, "b":Landroid/database/MatrixCursor$RowBuilder;
    const/4 v1, 0x0

    .local v1, "column":I
    :goto_1
    invoke-interface {p1}, Landroid/database/Cursor;->getColumnCount()I

    move-result v8

    if-ge v1, v8, :cond_3

    .line 85
    invoke-interface {p1, v1}, Landroid/database/Cursor;->getColumnName(I)Ljava/lang/String;

    move-result-object v8

    invoke-static {p1, v1}, Lcom/android/providers/contacts/PhoneLookupWithStarPrefix;->cursorValue(Landroid/database/Cursor;I)Ljava/lang/Object;

    move-result-object v9

    invoke-virtual {v0, v8, v9}, Landroid/database/MatrixCursor$RowBuilder;->add(Ljava/lang/String;Ljava/lang/Object;)Landroid/database/MatrixCursor$RowBuilder;
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 84
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 89
    .end local v0    # "b":Landroid/database/MatrixCursor$RowBuilder;
    .end local v1    # "column":I
    .end local v2    # "matchingNumberNormalized":Ljava/lang/String;
    .end local v4    # "numberIndex":I
    :cond_6
    const/4 v7, 0x0

    .line 92
    if-eqz v7, :cond_7

    .line 93
    :try_start_4
    invoke-interface {v7}, Landroid/database/Cursor;->close()V
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    .line 97
    :cond_7
    if-eqz v6, :cond_8

    .line 98
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    :cond_8
    move-object p1, v3

    goto :goto_0

    .line 92
    :catchall_0
    move-exception v8

    if-eqz v7, :cond_9

    .line 93
    :try_start_5
    invoke-interface {v7}, Landroid/database/Cursor;->close()V

    :cond_9
    throw v8
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_1

    .line 97
    .end local v3    # "matrixCursor":Landroid/database/MatrixCursor;
    .end local v5    # "queryPhoneNumberNormalized":Ljava/lang/String;
    .end local v7    # "unreturnedMatrixCursor":Landroid/database/Cursor;
    :catchall_1
    move-exception v8

    if-eqz v6, :cond_a

    .line 98
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    :cond_a
    throw v8
.end method
