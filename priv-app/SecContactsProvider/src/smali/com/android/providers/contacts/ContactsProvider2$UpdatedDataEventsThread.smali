.class Lcom/android/providers/contacts/ContactsProvider2$UpdatedDataEventsThread;
.super Ljava/lang/Thread;
.source "ContactsProvider2.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/providers/contacts/ContactsProvider2;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "UpdatedDataEventsThread"
.end annotation


# instance fields
.field db:Landroid/database/sqlite/SQLiteDatabase;

.field private ids:Ljava/lang/String;

.field final synthetic this$0:Lcom/android/providers/contacts/ContactsProvider2;


# direct methods
.method public constructor <init>(Lcom/android/providers/contacts/ContactsProvider2;Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;)V
    .locals 1
    .param p2, "activeDb"    # Landroid/database/sqlite/SQLiteDatabase;
    .param p3, "ids"    # Ljava/lang/String;

    .prologue
    .line 4231
    iput-object p1, p0, Lcom/android/providers/contacts/ContactsProvider2$UpdatedDataEventsThread;->this$0:Lcom/android/providers/contacts/ContactsProvider2;

    .line 4232
    const-string v0, "UpdatedDataEventsThread"

    invoke-direct {p0, v0}, Ljava/lang/Thread;-><init>(Ljava/lang/String;)V

    .line 4228
    const-string v0, ""

    iput-object v0, p0, Lcom/android/providers/contacts/ContactsProvider2$UpdatedDataEventsThread;->ids:Ljava/lang/String;

    .line 4229
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/android/providers/contacts/ContactsProvider2$UpdatedDataEventsThread;->db:Landroid/database/sqlite/SQLiteDatabase;

    .line 4233
    iput-object p3, p0, Lcom/android/providers/contacts/ContactsProvider2$UpdatedDataEventsThread;->ids:Ljava/lang/String;

    .line 4234
    iput-object p2, p0, Lcom/android/providers/contacts/ContactsProvider2$UpdatedDataEventsThread;->db:Landroid/database/sqlite/SQLiteDatabase;

    .line 4235
    return-void
.end method


# virtual methods
.method public run()V
    .locals 33

    .prologue
    .line 4238
    const/4 v12, 0x0

    .line 4239
    .local v12, "cursor":Landroid/database/Cursor;
    const-string v30, "UTC"

    .line 4240
    .local v30, "timezone":Ljava/lang/String;
    new-instance v26, Ljava/util/ArrayList;

    invoke-direct/range {v26 .. v26}, Ljava/util/ArrayList;-><init>()V

    .line 4242
    .local v26, "ops":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Landroid/content/ContentProviderOperation;>;"
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/android/providers/contacts/ContactsProvider2$UpdatedDataEventsThread;->this$0:Lcom/android/providers/contacts/ContactsProvider2;

    # invokes: Lcom/android/providers/contacts/ContactsProvider2;->getDisplayOrder()I
    invoke-static {v2}, Lcom/android/providers/contacts/ContactsProvider2;->access$000(Lcom/android/providers/contacts/ContactsProvider2;)I

    move-result v27

    .line 4245
    .local v27, "orderOf":I
    :try_start_0
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/android/providers/contacts/ContactsProvider2$UpdatedDataEventsThread;->db:Landroid/database/sqlite/SQLiteDatabase;

    const-string v3, "view_data"

    const/16 v4, 0xb

    new-array v4, v4, [Ljava/lang/String;

    const/4 v5, 0x0

    const-string v6, "_id"

    aput-object v6, v4, v5

    const/4 v5, 0x1

    const-string v6, "data1"

    aput-object v6, v4, v5

    const/4 v5, 0x2

    const-string v6, "data2"

    aput-object v6, v4, v5

    const/4 v5, 0x3

    const-string v6, "data3"

    aput-object v6, v4, v5

    const/4 v5, 0x4

    const-string v6, "raw_contact_id"

    aput-object v6, v4, v5

    const/4 v5, 0x5

    const-string v6, "account_name"

    aput-object v6, v4, v5

    const/4 v5, 0x6

    const-string v6, "account_type"

    aput-object v6, v4, v5

    const/4 v5, 0x7

    const-string v6, "display_name"

    aput-object v6, v4, v5

    const/16 v5, 0x8

    const-string v6, "display_name_alt"

    aput-object v6, v4, v5

    const/16 v5, 0x9

    const-string v6, "data14"

    aput-object v6, v4, v5

    const/16 v5, 0xa

    const-string v6, "data15"

    aput-object v6, v4, v5

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "mimetype = ? AND _id in "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/android/providers/contacts/ContactsProvider2$UpdatedDataEventsThread;->ids:Ljava/lang/String;

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    const/4 v6, 0x1

    new-array v6, v6, [Ljava/lang/String;

    const/4 v7, 0x0

    const-string v8, "vnd.android.cursor.item/contact_event"

    aput-object v8, v6, v7

    const/4 v7, 0x0

    const/4 v8, 0x0

    const/4 v9, 0x0

    invoke-virtual/range {v2 .. v9}, Landroid/database/sqlite/SQLiteDatabase;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v12

    .line 4249
    if-eqz v12, :cond_b

    .line 4250
    const-string v2, "_id"

    invoke-interface {v12, v2}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v21

    .line 4251
    .local v21, "indexId":I
    const-string v2, "data1"

    invoke-interface {v12, v2}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v24

    .line 4252
    .local v24, "indexStartDate":I
    const-string v2, "data2"

    invoke-interface {v12, v2}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v25

    .line 4254
    .local v25, "indexType":I
    const-string v2, "data3"

    invoke-interface {v12, v2}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v22

    .line 4255
    .local v22, "indexLabel":I
    const-string v2, "display_name"

    invoke-interface {v12, v2}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v19

    .line 4256
    .local v19, "indexDisplayName":I
    const-string v2, "display_name_alt"

    invoke-interface {v12, v2}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v20

    .line 4257
    .local v20, "indexDisplayNameReverse":I
    const-string v2, "data15"

    invoke-interface {v12, v2}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v18

    .line 4258
    .local v18, "indexDateType":I
    const-string v2, "data14"

    invoke-interface {v12, v2}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v23

    .line 4259
    .local v23, "indexSolarDate":I
    const/16 v31, 0x0

    .line 4260
    .local v31, "titleBuilder":Ljava/lang/StringBuilder;
    const/16 v17, 0x0

    .line 4262
    .local v17, "idBuilder":Ljava/lang/StringBuilder;
    :cond_0
    :goto_0
    invoke-interface {v12}, Landroid/database/Cursor;->moveToNext()Z

    move-result v2

    if-eqz v2, :cond_b

    .line 4265
    move/from16 v0, v18

    invoke-interface {v12, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v14

    .line 4266
    .local v14, "dateType":I
    invoke-static {}, Lcom/sec/android/app/CscFeature;->getInstance()Lcom/sec/android/app/CscFeature;

    move-result-object v2

    const-string v3, "CscFeature_Calendar_EnableLunar"

    invoke-virtual {v2, v3}, Lcom/sec/android/app/CscFeature;->getEnableStatus(Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_6

    .line 4268
    move/from16 v0, v24

    invoke-interface {v12, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v13

    .line 4279
    .local v13, "date":Ljava/lang/String;
    :cond_1
    :goto_1
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/android/providers/contacts/ContactsProvider2$UpdatedDataEventsThread;->this$0:Lcom/android/providers/contacts/ContactsProvider2;

    # invokes: Lcom/android/providers/contacts/ContactsProvider2;->convertDate(Ljava/lang/String;)J
    invoke-static {v2, v13}, Lcom/android/providers/contacts/ContactsProvider2;->access$100(Lcom/android/providers/contacts/ContactsProvider2;Ljava/lang/String;)J

    move-result-wide v28

    .line 4280
    .local v28, "startMillis":J
    new-instance v31, Ljava/lang/StringBuilder;

    .end local v31    # "titleBuilder":Ljava/lang/StringBuilder;
    invoke-direct/range {v31 .. v31}, Ljava/lang/StringBuilder;-><init>()V

    .line 4281
    .restart local v31    # "titleBuilder":Ljava/lang/StringBuilder;
    const/4 v2, 0x1

    move/from16 v0, v27

    if-ne v0, v2, :cond_8

    .line 4282
    move/from16 v0, v19

    invoke-interface {v12, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v2

    if-eqz v2, :cond_2

    .line 4283
    move/from16 v0, v19

    invoke-interface {v12, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, v31

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 4285
    const-string v2, ". "

    move-object/from16 v0, v31

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 4295
    :cond_2
    :goto_2
    move/from16 v0, v25

    invoke-interface {v12, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v2

    if-nez v2, :cond_9

    .line 4296
    move/from16 v0, v22

    invoke-interface {v12, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v2

    if-eqz v2, :cond_3

    .line 4297
    move/from16 v0, v22

    invoke-interface {v12, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, v31

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 4305
    :cond_3
    :goto_3
    new-instance v32, Landroid/content/ContentValues;

    invoke-direct/range {v32 .. v32}, Landroid/content/ContentValues;-><init>()V

    .line 4306
    .local v32, "values":Landroid/content/ContentValues;
    const-string v2, "calendar_id"

    const/4 v3, 0x1

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    move-object/from16 v0, v32

    invoke-virtual {v0, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 4307
    const-string v2, "title"

    invoke-virtual/range {v31 .. v31}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    move-object/from16 v0, v32

    invoke-virtual {v0, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 4308
    const-string v2, "dtstart"

    invoke-static/range {v28 .. v29}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    move-object/from16 v0, v32

    invoke-virtual {v0, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 4309
    const-string v2, "eventTimezone"

    move-object/from16 v0, v32

    move-object/from16 v1, v30

    invoke-virtual {v0, v2, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 4311
    const-string v2, "duration"

    const-string v3, "P1D"

    move-object/from16 v0, v32

    invoke-virtual {v0, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 4313
    const-string v2, "rrule"

    const-string v3, "FREQ=YEARLY;UNTIL=20361231T000000Z;WKST=SU"

    move-object/from16 v0, v32

    invoke-virtual {v0, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 4315
    const-string v2, "accessLevel"

    const/4 v3, 0x3

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    move-object/from16 v0, v32

    invoke-virtual {v0, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 4317
    move/from16 v0, v25

    invoke-interface {v12, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v16

    .line 4318
    .local v16, "eventType":Ljava/lang/String;
    if-eqz v16, :cond_0

    .line 4320
    const-string v2, "contactEventType"

    move-object/from16 v0, v32

    move-object/from16 v1, v16

    invoke-virtual {v0, v2, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 4321
    new-instance v17, Ljava/lang/StringBuilder;

    .end local v17    # "idBuilder":Ljava/lang/StringBuilder;
    invoke-direct/range {v17 .. v17}, Ljava/lang/StringBuilder;-><init>()V

    .line 4322
    .restart local v17    # "idBuilder":Ljava/lang/StringBuilder;
    sget-object v2, Landroid/provider/CalendarContract$Events;->CONTENT_URI:Landroid/net/Uri;

    move-object/from16 v0, v17

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 4324
    invoke-static {}, Lcom/sec/android/app/CscFeature;->getInstance()Lcom/sec/android/app/CscFeature;

    move-result-object v2

    const-string v3, "CscFeature_Calendar_EnableLunar"

    invoke-virtual {v2, v3}, Lcom/sec/android/app/CscFeature;->getEnableStatus(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_4

    .line 4326
    const-string v3, "setLunar"

    if-nez v14, :cond_a

    const-string v2, "0"

    :goto_4
    move-object/from16 v0, v32

    invoke-virtual {v0, v3, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 4329
    :cond_4
    invoke-virtual/range {v17 .. v17}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v2

    invoke-static {v2}, Landroid/content/ContentProviderOperation;->newUpdate(Landroid/net/Uri;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v2

    move-object/from16 v0, v32

    invoke-virtual {v2, v0}, Landroid/content/ContentProviderOperation$Builder;->withValues(Landroid/content/ContentValues;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v11

    .line 4331
    .local v11, "b":Landroid/content/ContentProviderOperation$Builder;
    const/4 v2, 0x1

    new-array v10, v2, [Ljava/lang/String;

    const/4 v2, 0x0

    move/from16 v0, v21

    invoke-interface {v12, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v3

    aput-object v3, v10, v2

    .line 4332
    .local v10, "args":[Ljava/lang/String;
    const-string v2, "contact_data_id=?"

    invoke-virtual {v11, v2, v10}, Landroid/content/ContentProviderOperation$Builder;->withSelection(Ljava/lang/String;[Ljava/lang/String;)Landroid/content/ContentProviderOperation$Builder;

    .line 4334
    invoke-virtual {v11}, Landroid/content/ContentProviderOperation$Builder;->build()Landroid/content/ContentProviderOperation;

    move-result-object v2

    move-object/from16 v0, v26

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto/16 :goto_0

    .line 4338
    .end local v10    # "args":[Ljava/lang/String;
    .end local v11    # "b":Landroid/content/ContentProviderOperation$Builder;
    .end local v13    # "date":Ljava/lang/String;
    .end local v14    # "dateType":I
    .end local v16    # "eventType":Ljava/lang/String;
    .end local v17    # "idBuilder":Ljava/lang/StringBuilder;
    .end local v18    # "indexDateType":I
    .end local v19    # "indexDisplayName":I
    .end local v20    # "indexDisplayNameReverse":I
    .end local v21    # "indexId":I
    .end local v22    # "indexLabel":I
    .end local v23    # "indexSolarDate":I
    .end local v24    # "indexStartDate":I
    .end local v25    # "indexType":I
    .end local v28    # "startMillis":J
    .end local v31    # "titleBuilder":Ljava/lang/StringBuilder;
    .end local v32    # "values":Landroid/content/ContentValues;
    :catchall_0
    move-exception v2

    if-eqz v12, :cond_5

    invoke-interface {v12}, Landroid/database/Cursor;->close()V

    :cond_5
    throw v2

    .line 4270
    .restart local v14    # "dateType":I
    .restart local v17    # "idBuilder":Ljava/lang/StringBuilder;
    .restart local v18    # "indexDateType":I
    .restart local v19    # "indexDisplayName":I
    .restart local v20    # "indexDisplayNameReverse":I
    .restart local v21    # "indexId":I
    .restart local v22    # "indexLabel":I
    .restart local v23    # "indexSolarDate":I
    .restart local v24    # "indexStartDate":I
    .restart local v25    # "indexType":I
    .restart local v31    # "titleBuilder":Ljava/lang/StringBuilder;
    :cond_6
    if-nez v14, :cond_7

    .line 4271
    :try_start_1
    move/from16 v0, v24

    invoke-interface {v12, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v13

    .restart local v13    # "date":Ljava/lang/String;
    goto/16 :goto_1

    .line 4273
    .end local v13    # "date":Ljava/lang/String;
    :cond_7
    move/from16 v0, v23

    invoke-interface {v12, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v13

    .line 4274
    .restart local v13    # "date":Ljava/lang/String;
    invoke-static {v13}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_1

    goto/16 :goto_0

    .line 4288
    .restart local v28    # "startMillis":J
    :cond_8
    move/from16 v0, v20

    invoke-interface {v12, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v2

    if-eqz v2, :cond_2

    .line 4289
    move/from16 v0, v20

    invoke-interface {v12, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, v31

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 4290
    const-string v2, ". "

    move-object/from16 v0, v31

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto/16 :goto_2

    .line 4300
    :cond_9
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/android/providers/contacts/ContactsProvider2$UpdatedDataEventsThread;->this$0:Lcom/android/providers/contacts/ContactsProvider2;

    invoke-virtual {v2}, Lcom/android/providers/contacts/ContactsProvider2;->getContext()Landroid/content/Context;

    move-result-object v2

    move/from16 v0, v25

    invoke-interface {v12, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v3

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-static {v3}, Landroid/provider/ContactsContract$CommonDataKinds$Event;->getTypeResource(Ljava/lang/Integer;)I

    move-result v3

    invoke-virtual {v2, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    if-eqz v2, :cond_3

    .line 4301
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/android/providers/contacts/ContactsProvider2$UpdatedDataEventsThread;->this$0:Lcom/android/providers/contacts/ContactsProvider2;

    invoke-virtual {v2}, Lcom/android/providers/contacts/ContactsProvider2;->getContext()Landroid/content/Context;

    move-result-object v2

    move/from16 v0, v25

    invoke-interface {v12, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v3

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-static {v3}, Landroid/provider/ContactsContract$CommonDataKinds$Event;->getTypeResource(Ljava/lang/Integer;)I

    move-result v3

    invoke-virtual {v2, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, v31

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto/16 :goto_3

    .line 4326
    .restart local v16    # "eventType":Ljava/lang/String;
    .restart local v32    # "values":Landroid/content/ContentValues;
    :cond_a
    const-string v2, "1"
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto/16 :goto_4

    .line 4338
    .end local v13    # "date":Ljava/lang/String;
    .end local v14    # "dateType":I
    .end local v16    # "eventType":Ljava/lang/String;
    .end local v17    # "idBuilder":Ljava/lang/StringBuilder;
    .end local v18    # "indexDateType":I
    .end local v19    # "indexDisplayName":I
    .end local v20    # "indexDisplayNameReverse":I
    .end local v21    # "indexId":I
    .end local v22    # "indexLabel":I
    .end local v23    # "indexSolarDate":I
    .end local v24    # "indexStartDate":I
    .end local v25    # "indexType":I
    .end local v28    # "startMillis":J
    .end local v31    # "titleBuilder":Ljava/lang/StringBuilder;
    .end local v32    # "values":Landroid/content/ContentValues;
    :cond_b
    if-eqz v12, :cond_c

    invoke-interface {v12}, Landroid/database/Cursor;->close()V

    .line 4343
    :cond_c
    :try_start_2
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/android/providers/contacts/ContactsProvider2$UpdatedDataEventsThread;->this$0:Lcom/android/providers/contacts/ContactsProvider2;

    invoke-virtual {v2}, Lcom/android/providers/contacts/ContactsProvider2;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    const-string v3, "com.android.calendar"

    move-object/from16 v0, v26

    invoke-virtual {v2, v3, v0}, Landroid/content/ContentResolver;->applyBatch(Ljava/lang/String;Ljava/util/ArrayList;)[Landroid/content/ContentProviderResult;
    :try_end_2
    .catch Landroid/os/RemoteException; {:try_start_2 .. :try_end_2} :catch_0
    .catch Landroid/content/OperationApplicationException; {:try_start_2 .. :try_end_2} :catch_1
    .catch Ljava/lang/IllegalArgumentException; {:try_start_2 .. :try_end_2} :catch_2

    .line 4351
    :goto_5
    return-void

    .line 4344
    :catch_0
    move-exception v15

    .line 4345
    .local v15, "e":Landroid/os/RemoteException;
    const-string v2, "ContactsProvider"

    const-string v3, "Ignoring unexpected remote exception"

    invoke-static {v2, v3, v15}, Landroid/util/secutil/Log;->secW(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_5

    .line 4346
    .end local v15    # "e":Landroid/os/RemoteException;
    :catch_1
    move-exception v15

    .line 4347
    .local v15, "e":Landroid/content/OperationApplicationException;
    const-string v2, "ContactsProvider"

    const-string v3, "Ignoring unexpected exception"

    invoke-static {v2, v3, v15}, Landroid/util/secutil/Log;->secW(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_5

    .line 4348
    .end local v15    # "e":Landroid/content/OperationApplicationException;
    :catch_2
    move-exception v15

    .line 4349
    .local v15, "e":Ljava/lang/IllegalArgumentException;
    const-string v2, "ContactsProvider"

    const-string v3, "Ignoring unexpected illegal argument exception"

    invoke-static {v2, v3, v15}, Landroid/util/secutil/Log;->secW(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_5
.end method
