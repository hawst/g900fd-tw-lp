.class Lcom/android/providers/contacts/util/CursorCacheManager$UniqCacheKey;
.super Ljava/lang/Object;
.source "CursorCacheManager.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/providers/contacts/util/CursorCacheManager;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "UniqCacheKey"
.end annotation


# instance fields
.field private final mBuiltHashKey:Ljava/lang/String;

.field private final mUriCode:Ljava/lang/Integer;


# direct methods
.method private constructor <init>(Ljava/lang/Integer;Ljava/lang/String;)V
    .locals 0
    .param p1, "code"    # Ljava/lang/Integer;
    .param p2, "uniqKey"    # Ljava/lang/String;

    .prologue
    .line 379
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 380
    iput-object p1, p0, Lcom/android/providers/contacts/util/CursorCacheManager$UniqCacheKey;->mUriCode:Ljava/lang/Integer;

    .line 382
    iput-object p2, p0, Lcom/android/providers/contacts/util/CursorCacheManager$UniqCacheKey;->mBuiltHashKey:Ljava/lang/String;

    .line 383
    return-void
.end method

.method static synthetic access$000(Lcom/android/providers/contacts/util/CursorCacheManager$UniqCacheKey;)Ljava/lang/Integer;
    .locals 1
    .param p0, "x0"    # Lcom/android/providers/contacts/util/CursorCacheManager$UniqCacheKey;

    .prologue
    .line 374
    iget-object v0, p0, Lcom/android/providers/contacts/util/CursorCacheManager$UniqCacheKey;->mUriCode:Ljava/lang/Integer;

    return-object v0
.end method

.method public static getInstanceWithKey(Ljava/lang/Integer;Ljava/lang/String;)Lcom/android/providers/contacts/util/CursorCacheManager$UniqCacheKey;
    .locals 1
    .param p0, "uriCode"    # Ljava/lang/Integer;
    .param p1, "key"    # Ljava/lang/String;

    .prologue
    .line 386
    new-instance v0, Lcom/android/providers/contacts/util/CursorCacheManager$UniqCacheKey;

    invoke-direct {v0, p0, p1}, Lcom/android/providers/contacts/util/CursorCacheManager$UniqCacheKey;-><init>(Ljava/lang/Integer;Ljava/lang/String;)V

    return-object v0
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 2
    .param p1, "o"    # Ljava/lang/Object;

    .prologue
    .line 401
    invoke-virtual {p1}, Ljava/lang/Object;->hashCode()I

    move-result v0

    invoke-virtual {p0}, Lcom/android/providers/contacts/util/CursorCacheManager$UniqCacheKey;->hashCode()I

    move-result v1

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hashCode()I
    .locals 1

    .prologue
    .line 396
    iget-object v0, p0, Lcom/android/providers/contacts/util/CursorCacheManager$UniqCacheKey;->mBuiltHashKey:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 1

    .prologue
    .line 391
    iget-object v0, p0, Lcom/android/providers/contacts/util/CursorCacheManager$UniqCacheKey;->mBuiltHashKey:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
