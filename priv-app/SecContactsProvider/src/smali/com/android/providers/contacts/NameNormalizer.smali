.class public Lcom/android/providers/contacts/NameNormalizer;
.super Ljava/lang/Object;
.source "NameNormalizer.java"


# static fields
.field private static final ABNORMAL_COLLATION_LOCALE:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private static sCachedComplexityCollator:Ljava/text/RuleBasedCollator;

.field private static sCachedCompressingCollator:Ljava/text/RuleBasedCollator;

.field private static sCollatorLocale:Ljava/util/Locale;

.field private static final sCollatorLock:Ljava/lang/Object;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    .line 37
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    sput-object v0, Lcom/android/providers/contacts/NameNormalizer;->sCollatorLock:Ljava/lang/Object;

    .line 46
    const/16 v0, 0x19

    new-array v0, v0, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string v2, "ga_IE"

    aput-object v2, v0, v1

    const/4 v1, 0x1

    const-string v2, "et_EE"

    aput-object v2, v0, v1

    const/4 v1, 0x2

    const-string v2, "mk_MK"

    aput-object v2, v0, v1

    const/4 v1, 0x3

    const-string v2, "ms_MY"

    aput-object v2, v0, v1

    const/4 v1, 0x4

    const-string v2, "is_IS"

    aput-object v2, v0, v1

    const/4 v1, 0x5

    const-string v2, "kk_KZ"

    aput-object v2, v0, v1

    const/4 v1, 0x6

    const-string v2, "uz_UZ"

    aput-object v2, v0, v1

    const/4 v1, 0x7

    const-string v2, "gl_ES"

    aput-object v2, v0, v1

    const/16 v1, 0x8

    const-string v2, "eu_ES"

    aput-object v2, v0, v1

    const/16 v1, 0x9

    const-string v2, "eu_FR"

    aput-object v2, v0, v1

    const/16 v1, 0xa

    const-string v2, "km_KH"

    aput-object v2, v0, v1

    const/16 v1, 0xb

    const-string v2, "my_MM"

    aput-object v2, v0, v1

    const/16 v1, 0xc

    const-string v2, "lo_LA"

    aput-object v2, v0, v1

    const/16 v1, 0xd

    const-string v2, "kn_IN"

    aput-object v2, v0, v1

    const/16 v1, 0xe

    const-string v2, "ml_IN"

    aput-object v2, v0, v1

    const/16 v1, 0xf

    const-string v2, "ne_IN"

    aput-object v2, v0, v1

    const/16 v1, 0x10

    const-string v2, "ka_GE"

    aput-object v2, v0, v1

    const/16 v1, 0x11

    const-string v2, "hy_AM"

    aput-object v2, v0, v1

    const/16 v1, 0x12

    const-string v2, "pa_IN"

    aput-object v2, v0, v1

    const/16 v1, 0x13

    const-string v2, "gu_IN"

    aput-object v2, v0, v1

    const/16 v1, 0x14

    const-string v2, "te_IN"

    aput-object v2, v0, v1

    const/16 v1, 0x15

    const-string v2, "si_IN"

    aput-object v2, v0, v1

    const/16 v1, 0x16

    const-string v2, "or_IN"

    aput-object v2, v0, v1

    const/16 v1, 0x17

    const-string v2, "tl_PH"

    aput-object v2, v0, v1

    const/16 v1, 0x18

    const-string v2, "mn_MN"

    aput-object v2, v0, v1

    invoke-static {v0}, Lcom/google/android/collect/Lists;->newArrayList([Ljava/lang/Object;)Ljava/util/ArrayList;

    move-result-object v0

    sput-object v0, Lcom/android/providers/contacts/NameNormalizer;->ABNORMAL_COLLATION_LOCALE:Ljava/util/List;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 35
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static compareComplexity(Ljava/lang/String;Ljava/lang/String;)I
    .locals 5
    .param p0, "name1"    # Ljava/lang/String;
    .param p1, "name2"    # Ljava/lang/String;

    .prologue
    .line 131
    invoke-static {p0}, Lcom/android/providers/contacts/NameNormalizer;->lettersAndDigitsOnly(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 132
    .local v0, "clean1":Ljava/lang/String;
    invoke-static {p1}, Lcom/android/providers/contacts/NameNormalizer;->lettersAndDigitsOnly(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 133
    .local v1, "clean2":Ljava/lang/String;
    invoke-static {}, Lcom/android/providers/contacts/NameNormalizer;->getComplexityCollator()Ljava/text/RuleBasedCollator;

    move-result-object v3

    invoke-virtual {v3, v0, v1}, Ljava/text/RuleBasedCollator;->compare(Ljava/lang/String;Ljava/lang/String;)I

    move-result v2

    .line 134
    .local v2, "diff":I
    if-eqz v2, :cond_0

    move v3, v2

    .line 144
    :goto_0
    return v3

    .line 140
    :cond_0
    invoke-virtual {v0, v1}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v3

    neg-int v2, v3

    .line 141
    if-eqz v2, :cond_1

    move v3, v2

    .line 142
    goto :goto_0

    .line 144
    :cond_1
    invoke-virtual {p0}, Ljava/lang/String;->length()I

    move-result v3

    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v4

    sub-int/2addr v3, v4

    goto :goto_0
.end method

.method private static ensureCollators()V
    .locals 5

    .prologue
    const/4 v4, 0x1

    .line 57
    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v1

    .line 58
    .local v1, "locale":Ljava/util/Locale;
    sget-object v2, Lcom/android/providers/contacts/NameNormalizer;->sCollatorLocale:Ljava/util/Locale;

    invoke-virtual {v1, v2}, Ljava/util/Locale;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 71
    :goto_0
    return-void

    .line 61
    :cond_0
    sput-object v1, Lcom/android/providers/contacts/NameNormalizer;->sCollatorLocale:Ljava/util/Locale;

    .line 63
    invoke-static {v1}, Lcom/android/providers/contacts/NameNormalizer;->getCollatorLocale(Ljava/util/Locale;)Ljava/util/Locale;

    move-result-object v0

    .line 65
    .local v0, "collatorLocale":Ljava/util/Locale;
    invoke-static {v0}, Ljava/text/Collator;->getInstance(Ljava/util/Locale;)Ljava/text/Collator;

    move-result-object v2

    check-cast v2, Ljava/text/RuleBasedCollator;

    sput-object v2, Lcom/android/providers/contacts/NameNormalizer;->sCachedCompressingCollator:Ljava/text/RuleBasedCollator;

    .line 66
    sget-object v2, Lcom/android/providers/contacts/NameNormalizer;->sCachedCompressingCollator:Ljava/text/RuleBasedCollator;

    const/4 v3, 0x0

    invoke-virtual {v2, v3}, Ljava/text/RuleBasedCollator;->setStrength(I)V

    .line 67
    sget-object v2, Lcom/android/providers/contacts/NameNormalizer;->sCachedCompressingCollator:Ljava/text/RuleBasedCollator;

    invoke-virtual {v2, v4}, Ljava/text/RuleBasedCollator;->setDecomposition(I)V

    .line 69
    invoke-static {v0}, Ljava/text/Collator;->getInstance(Ljava/util/Locale;)Ljava/text/Collator;

    move-result-object v2

    check-cast v2, Ljava/text/RuleBasedCollator;

    sput-object v2, Lcom/android/providers/contacts/NameNormalizer;->sCachedComplexityCollator:Ljava/text/RuleBasedCollator;

    .line 70
    sget-object v2, Lcom/android/providers/contacts/NameNormalizer;->sCachedComplexityCollator:Ljava/text/RuleBasedCollator;

    invoke-virtual {v2, v4}, Ljava/text/RuleBasedCollator;->setStrength(I)V

    goto :goto_0
.end method

.method static getCollatorLocale(Ljava/util/Locale;)Ljava/util/Locale;
    .locals 4
    .param p0, "locale"    # Ljava/util/Locale;

    .prologue
    .line 90
    move-object v0, p0

    .line 95
    .local v0, "collatorLocale":Ljava/util/Locale;
    invoke-virtual {v0}, Ljava/util/Locale;->toString()Ljava/lang/String;

    move-result-object v1

    const-string v2, "fr"

    invoke-virtual {v1, v2}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_0

    sget-object v1, Lcom/android/providers/contacts/NameNormalizer;->ABNORMAL_COLLATION_LOCALE:Ljava/util/List;

    invoke-virtual {v0}, Ljava/util/Locale;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v1, v2}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    invoke-static {}, Lcom/android/providers/contacts/util/LoadCscFeatureUtils;->getInstance()Lcom/android/providers/contacts/util/LoadCscFeatureUtils;

    move-result-object v1

    invoke-virtual {v1}, Lcom/android/providers/contacts/util/LoadCscFeatureUtils;->getEnableSupportFuzzySearch()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 98
    :cond_0
    const-string v1, "NameNormalizer"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "@@ collator is selected from( "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " ) to U S"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 99
    sget-object v0, Ljava/util/Locale;->US:Ljava/util/Locale;

    .line 102
    :cond_1
    return-object v0
.end method

.method static getComplexityCollator()Ljava/text/RuleBasedCollator;
    .locals 2
    .annotation build Lcom/google/common/annotations/VisibleForTesting;
    .end annotation

    .prologue
    .line 83
    sget-object v1, Lcom/android/providers/contacts/NameNormalizer;->sCollatorLock:Ljava/lang/Object;

    monitor-enter v1

    .line 84
    :try_start_0
    invoke-static {}, Lcom/android/providers/contacts/NameNormalizer;->ensureCollators()V

    .line 85
    sget-object v0, Lcom/android/providers/contacts/NameNormalizer;->sCachedComplexityCollator:Ljava/text/RuleBasedCollator;

    monitor-exit v1

    return-object v0

    .line 86
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method static getCompressingCollator()Ljava/text/RuleBasedCollator;
    .locals 2
    .annotation build Lcom/google/common/annotations/VisibleForTesting;
    .end annotation

    .prologue
    .line 75
    sget-object v1, Lcom/android/providers/contacts/NameNormalizer;->sCollatorLock:Ljava/lang/Object;

    monitor-enter v1

    .line 76
    :try_start_0
    invoke-static {}, Lcom/android/providers/contacts/NameNormalizer;->ensureCollators()V

    .line 77
    sget-object v0, Lcom/android/providers/contacts/NameNormalizer;->sCachedCompressingCollator:Ljava/text/RuleBasedCollator;

    monitor-exit v1

    return-object v0

    .line 78
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method private static isAdditionalChar(C)Z
    .locals 1
    .param p0, "c"    # C

    .prologue
    .line 173
    const/16 v0, 0x17b4

    if-lt p0, v0, :cond_0

    const/16 v0, 0x17d6

    if-le p0, v0, :cond_3

    :cond_0
    const/16 v0, 0x17d8

    if-lt p0, v0, :cond_1

    const/16 v0, 0x17db

    if-le p0, v0, :cond_3

    :cond_1
    const/16 v0, 0x17dd

    if-lt p0, v0, :cond_2

    const/16 v0, 0x17df

    if-le p0, v0, :cond_3

    :cond_2
    const/16 v0, 0x17ea

    if-lt p0, v0, :cond_4

    const/16 v0, 0x17ff

    if-gt p0, v0, :cond_4

    .line 175
    :cond_3
    const/4 v0, 0x1

    .line 177
    :goto_0
    return v0

    :cond_4
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static lettersAndDigitsOnly(Ljava/lang/String;)Ljava/lang/String;
    .locals 6
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 152
    if-nez p0, :cond_1

    .line 153
    const-string p0, ""

    .line 168
    .end local p0    # "name":Ljava/lang/String;
    .local v1, "i":I
    .local v2, "length":I
    .local v4, "letters":[C
    :cond_0
    :goto_0
    return-object p0

    .line 155
    .end local v1    # "i":I
    .end local v2    # "length":I
    .end local v4    # "letters":[C
    .restart local p0    # "name":Ljava/lang/String;
    :cond_1
    invoke-virtual {p0}, Ljava/lang/String;->toCharArray()[C

    move-result-object v4

    .line 156
    .restart local v4    # "letters":[C
    const/4 v2, 0x0

    .line 157
    .restart local v2    # "length":I
    const/4 v1, 0x0

    .restart local v1    # "i":I
    :goto_1
    array-length v5, v4

    if-ge v1, v5, :cond_4

    .line 158
    aget-char v0, v4, v1

    .line 159
    .local v0, "c":C
    invoke-static {v0}, Ljava/lang/Character;->isLetterOrDigit(C)Z

    move-result v5

    if-nez v5, :cond_2

    invoke-static {v0}, Lcom/android/providers/contacts/NameNormalizer;->isAdditionalChar(C)Z

    move-result v5

    if-eqz v5, :cond_3

    .line 160
    :cond_2
    add-int/lit8 v3, v2, 0x1

    .end local v2    # "length":I
    .local v3, "length":I
    aput-char v0, v4, v2

    move v2, v3

    .line 157
    .end local v3    # "length":I
    .restart local v2    # "length":I
    :cond_3
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 164
    .end local v0    # "c":C
    :cond_4
    array-length v5, v4

    if-eq v2, v5, :cond_0

    .line 165
    new-instance p0, Ljava/lang/String;

    .end local p0    # "name":Ljava/lang/String;
    const/4 v5, 0x0

    invoke-direct {p0, v4, v5, v2}, Ljava/lang/String;-><init>([CII)V

    goto :goto_0
.end method

.method public static normalize(Ljava/lang/String;)Ljava/lang/String;
    .locals 3
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 112
    invoke-static {}, Lcom/sec/android/app/CscFeature;->getInstance()Lcom/sec/android/app/CscFeature;

    move-result-object v1

    const-string v2, "CscFeature_Contact_EnableCallerIdSearch4Korea"

    invoke-virtual {v1, v2}, Lcom/sec/android/app/CscFeature;->getEnableStatus(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_0

    invoke-static {}, Lcom/sec/android/app/CscFeature;->getInstance()Lcom/sec/android/app/CscFeature;

    move-result-object v1

    const-string v2, "CscFeature_Contact_EnableDocomoAccountAsDefault"

    invoke-virtual {v1, v2}, Lcom/sec/android/app/CscFeature;->getEnableStatus(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_0

    invoke-static {}, Lcom/sec/android/app/CscFeature;->getInstance()Lcom/sec/android/app/CscFeature;

    move-result-object v1

    const-string v2, "CscFeature_Contact_EnableUIM"

    invoke-virtual {v1, v2}, Lcom/sec/android/app/CscFeature;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    const-string v2, "KDDI"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 115
    :cond_0
    invoke-static {}, Lcom/android/providers/contacts/NameNormalizer;->getCompressingCollator()Ljava/text/RuleBasedCollator;

    move-result-object v1

    invoke-virtual {v1, p0}, Ljava/text/RuleBasedCollator;->getCollationKey(Ljava/lang/String;)Ljava/text/CollationKey;

    move-result-object v0

    .line 120
    .local v0, "key":Ljava/text/CollationKey;
    :goto_0
    invoke-virtual {v0}, Ljava/text/CollationKey;->toByteArray()[B

    move-result-object v1

    if-nez v1, :cond_2

    .line 121
    const-string v1, ""

    .line 123
    :goto_1
    return-object v1

    .line 117
    .end local v0    # "key":Ljava/text/CollationKey;
    :cond_1
    invoke-static {}, Lcom/android/providers/contacts/NameNormalizer;->getCompressingCollator()Ljava/text/RuleBasedCollator;

    move-result-object v1

    invoke-static {p0}, Lcom/android/providers/contacts/NameNormalizer;->lettersAndDigitsOnly(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/text/RuleBasedCollator;->getCollationKey(Ljava/lang/String;)Ljava/text/CollationKey;

    move-result-object v0

    .restart local v0    # "key":Ljava/text/CollationKey;
    goto :goto_0

    .line 123
    :cond_2
    invoke-virtual {v0}, Ljava/text/CollationKey;->toByteArray()[B

    move-result-object v1

    const/4 v2, 0x1

    invoke-static {v1, v2}, Lcom/android/providers/contacts/util/Hex;->encodeHex([BZ)Ljava/lang/String;

    move-result-object v1

    goto :goto_1
.end method
