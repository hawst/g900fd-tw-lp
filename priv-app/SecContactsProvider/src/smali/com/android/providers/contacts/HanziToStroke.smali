.class public Lcom/android/providers/contacts/HanziToStroke;
.super Ljava/lang/Object;
.source "HanziToStroke.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/android/providers/contacts/HanziToStroke$Token;
    }
.end annotation


# static fields
.field private static final OFFSET_PRIOR_STROKE:[I

.field private static final PRIOR_STROKE:[C

.field private static devider:[I

.field private static sSingleton:Lcom/android/providers/contacts/HanziToStroke;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    const/4 v1, 0x2

    .line 58
    const/16 v0, 0x2b

    new-array v0, v0, [I

    fill-array-data v0, :array_0

    sput-object v0, Lcom/android/providers/contacts/HanziToStroke;->devider:[I

    .line 81
    new-array v0, v1, [C

    fill-array-data v0, :array_1

    sput-object v0, Lcom/android/providers/contacts/HanziToStroke;->PRIOR_STROKE:[C

    .line 85
    new-array v0, v1, [I

    fill-array-data v0, :array_2

    sput-object v0, Lcom/android/providers/contacts/HanziToStroke;->OFFSET_PRIOR_STROKE:[I

    return-void

    .line 58
    nop

    :array_0
    .array-data 4
        0x4e00
        0x4ff4
        0x51e8
        0x53dc
        0x55d0
        0x57c4
        0x59b8
        0x5bac
        0x5da0
        0x5f94
        0x6188
        0x637c
        0x6570
        0x6764
        0x6958
        0x6b4c
        0x6d40
        0x6f34
        0x7128
        0x731c
        0x7510
        0x7704
        0x78f8
        0x7aec
        0x7ce0
        0x7ed4
        0x80c8
        0x82bc
        0x84b0
        0x86a4
        0x8898    # 4.9E-41f
        0x8a8c
        0x8c80
        0x8e74
        0x9068
        0x925c
        0x9450
        0x9644
        0x9838
        0x9a2c
        0x9c20
        0x9e14
        0x9fa5
    .end array-data

    .line 81
    :array_1
    .array-data 2
        -0x7f4es
        -0x76dfs
    .end array-data

    .line 85
    :array_2
    .array-data 4
        0x8
        0x10
    .end array-data
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 11
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 560
    return-void
.end method

.method private checkTemp(Ljava/lang/String;)I
    .locals 5
    .param p1, "letter"    # Ljava/lang/String;

    .prologue
    .line 516
    const/4 v2, -0x1

    .line 517
    .local v2, "offset":I
    sget-object v4, Lcom/android/providers/contacts/HanziToStroke;->PRIOR_STROKE:[C

    array-length v0, v4

    .line 519
    .local v0, "arrayLen":I
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    if-ge v1, v0, :cond_0

    .line 520
    sget-object v4, Lcom/android/providers/contacts/HanziToStroke;->PRIOR_STROKE:[C

    aget-char v4, v4, v1

    invoke-static {v4}, Ljava/lang/Character;->toString(C)Ljava/lang/String;

    move-result-object v3

    .line 521
    .local v3, "sample":Ljava/lang/String;
    invoke-virtual {p1, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_1

    .line 522
    sget-object v4, Lcom/android/providers/contacts/HanziToStroke;->OFFSET_PRIOR_STROKE:[I

    aget v2, v4, v1

    .line 526
    .end local v3    # "sample":Ljava/lang/String;
    :cond_0
    return v2

    .line 519
    .restart local v3    # "sample":Ljava/lang/String;
    :cond_1
    add-int/lit8 v1, v1, 0x1

    goto :goto_0
.end method

.method private static find([[[II)[I
    .locals 6
    .param p0, "values"    # [[[I
    .param p1, "key"    # I

    .prologue
    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 541
    const/4 v1, 0x0

    .line 542
    .local v1, "lowerBound":I
    array-length v3, p0

    add-int/lit8 v2, v3, -0x1

    .line 545
    .local v2, "upperBound":I
    :goto_0
    add-int v3, v1, v2

    div-int/lit8 v0, v3, 0x2

    .line 546
    .local v0, "curIn":I
    aget-object v3, p0, v0

    aget-object v3, v3, v4

    aget v3, v3, v4

    if-ne v3, p1, :cond_0

    .line 547
    aget-object v3, p0, v0

    aget-object v3, v3, v5

    .line 549
    :goto_1
    return-object v3

    .line 548
    :cond_0
    if-le v1, v2, :cond_1

    .line 549
    new-array v3, v5, [I

    aput v4, v3, v4

    goto :goto_1

    .line 551
    :cond_1
    aget-object v3, p0, v0

    aget-object v3, v3, v4

    aget v3, v3, v4

    if-ge v3, p1, :cond_2

    .line 552
    add-int/lit8 v1, v0, 0x1

    goto :goto_0

    .line 554
    :cond_2
    add-int/lit8 v2, v0, -0x1

    goto :goto_0
.end method

.method private static findMapper(I)I
    .locals 3
    .param p0, "c"    # I

    .prologue
    .line 532
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    sget-object v1, Lcom/android/providers/contacts/HanziToStroke;->devider:[I

    array-length v1, v1

    if-ge v0, v1, :cond_1

    .line 533
    sget-object v1, Lcom/android/providers/contacts/HanziToStroke;->devider:[I

    aget v1, v1, v0

    if-lt p0, v1, :cond_0

    sget-object v1, Lcom/android/providers/contacts/HanziToStroke;->devider:[I

    add-int/lit8 v2, v0, 0x1

    aget v1, v1, v2

    if-ge p0, v1, :cond_0

    .line 534
    add-int/lit8 v1, v0, 0x1

    .line 537
    :goto_1
    return v1

    .line 532
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 537
    :cond_1
    const/4 v1, -0x1

    goto :goto_1
.end method

.method public static declared-synchronized getIntance()Lcom/android/providers/contacts/HanziToStroke;
    .locals 2

    .prologue
    .line 92
    const-class v1, Lcom/android/providers/contacts/HanziToStroke;

    monitor-enter v1

    :try_start_0
    sget-object v0, Lcom/android/providers/contacts/HanziToStroke;->sSingleton:Lcom/android/providers/contacts/HanziToStroke;

    if-nez v0, :cond_0

    .line 93
    new-instance v0, Lcom/android/providers/contacts/HanziToStroke;

    invoke-direct {v0}, Lcom/android/providers/contacts/HanziToStroke;-><init>()V

    sput-object v0, Lcom/android/providers/contacts/HanziToStroke;->sSingleton:Lcom/android/providers/contacts/HanziToStroke;

    .line 95
    :cond_0
    sget-object v0, Lcom/android/providers/contacts/HanziToStroke;->sSingleton:Lcom/android/providers/contacts/HanziToStroke;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit v1

    return-object v0

    .line 92
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method private getToken(C)Lcom/android/providers/contacts/HanziToStroke$Token;
    .locals 8
    .param p1, "character"    # C

    .prologue
    const/4 v7, 0x3

    .line 129
    new-instance v5, Lcom/android/providers/contacts/HanziToStroke$Token;

    invoke-direct {v5}, Lcom/android/providers/contacts/HanziToStroke$Token;-><init>()V

    .line 130
    .local v5, "token":Lcom/android/providers/contacts/HanziToStroke$Token;
    invoke-static {p1}, Ljava/lang/Character;->toString(C)Ljava/lang/String;

    move-result-object v2

    .line 131
    .local v2, "letter":Ljava/lang/String;
    iput-object v2, v5, Lcom/android/providers/contacts/HanziToStroke$Token;->source:Ljava/lang/String;

    .line 132
    const/4 v6, 0x2

    iput v6, v5, Lcom/android/providers/contacts/HanziToStroke$Token;->type:I

    .line 134
    const/16 v6, 0x100

    if-ge p1, v6, :cond_0

    .line 135
    const/4 v6, 0x1

    iput v6, v5, Lcom/android/providers/contacts/HanziToStroke$Token;->type:I

    .line 136
    iput-object v2, v5, Lcom/android/providers/contacts/HanziToStroke$Token;->target:Ljava/lang/String;

    .line 297
    :goto_0
    return-object v5

    .line 138
    :cond_0
    const/16 v6, 0x4e00

    if-lt p1, v6, :cond_1

    const v6, 0x9fa5

    if-le p1, v6, :cond_2

    .line 140
    :cond_1
    iput v7, v5, Lcom/android/providers/contacts/HanziToStroke$Token;->type:I

    .line 141
    iput-object v2, v5, Lcom/android/providers/contacts/HanziToStroke$Token;->target:Ljava/lang/String;

    goto :goto_0

    .line 145
    :cond_2
    invoke-static {p1}, Lcom/android/providers/contacts/HanziToStroke;->findMapper(I)I

    move-result v3

    .line 148
    .local v3, "method":I
    packed-switch v3, :pswitch_data_0

    .line 277
    iput v7, v5, Lcom/android/providers/contacts/HanziToStroke$Token;->type:I

    .line 278
    iput-object v2, v5, Lcom/android/providers/contacts/HanziToStroke$Token;->target:Ljava/lang/String;

    goto :goto_0

    .line 150
    :pswitch_0
    sget-object v6, Lcom/android/providers/contacts/MapStroke1;->HANZI_TO_STROKE_MAP_1:[[[I

    invoke-static {v6, p1}, Lcom/android/providers/contacts/HanziToStroke;->find([[[II)[I

    move-result-object v0

    .line 282
    .local v0, "array":[I
    :goto_1
    const/4 v6, 0x0

    aget v6, v0, v6

    if-nez v6, :cond_3

    .line 283
    iput v7, v5, Lcom/android/providers/contacts/HanziToStroke$Token;->type:I

    .line 284
    iput-object v2, v5, Lcom/android/providers/contacts/HanziToStroke$Token;->target:Ljava/lang/String;

    goto :goto_0

    .line 153
    .end local v0    # "array":[I
    :pswitch_1
    sget-object v6, Lcom/android/providers/contacts/MapStroke2;->HANZI_TO_STROKE_MAP_2:[[[I

    invoke-static {v6, p1}, Lcom/android/providers/contacts/HanziToStroke;->find([[[II)[I

    move-result-object v0

    .line 154
    .restart local v0    # "array":[I
    goto :goto_1

    .line 156
    .end local v0    # "array":[I
    :pswitch_2
    sget-object v6, Lcom/android/providers/contacts/MapStroke3;->HANZI_TO_STROKE_MAP_3:[[[I

    invoke-static {v6, p1}, Lcom/android/providers/contacts/HanziToStroke;->find([[[II)[I

    move-result-object v0

    .line 157
    .restart local v0    # "array":[I
    goto :goto_1

    .line 159
    .end local v0    # "array":[I
    :pswitch_3
    sget-object v6, Lcom/android/providers/contacts/MapStroke4;->HANZI_TO_STROKE_MAP_4:[[[I

    invoke-static {v6, p1}, Lcom/android/providers/contacts/HanziToStroke;->find([[[II)[I

    move-result-object v0

    .line 160
    .restart local v0    # "array":[I
    goto :goto_1

    .line 162
    .end local v0    # "array":[I
    :pswitch_4
    sget-object v6, Lcom/android/providers/contacts/MapStroke5;->HANZI_TO_STROKE_MAP_5:[[[I

    invoke-static {v6, p1}, Lcom/android/providers/contacts/HanziToStroke;->find([[[II)[I

    move-result-object v0

    .line 163
    .restart local v0    # "array":[I
    goto :goto_1

    .line 165
    .end local v0    # "array":[I
    :pswitch_5
    sget-object v6, Lcom/android/providers/contacts/MapStroke6;->HANZI_TO_STROKE_MAP_6:[[[I

    invoke-static {v6, p1}, Lcom/android/providers/contacts/HanziToStroke;->find([[[II)[I

    move-result-object v0

    .line 166
    .restart local v0    # "array":[I
    goto :goto_1

    .line 168
    .end local v0    # "array":[I
    :pswitch_6
    sget-object v6, Lcom/android/providers/contacts/MapStroke7;->HANZI_TO_STROKE_MAP_7:[[[I

    invoke-static {v6, p1}, Lcom/android/providers/contacts/HanziToStroke;->find([[[II)[I

    move-result-object v0

    .line 169
    .restart local v0    # "array":[I
    goto :goto_1

    .line 171
    .end local v0    # "array":[I
    :pswitch_7
    sget-object v6, Lcom/android/providers/contacts/MapStroke8;->HANZI_TO_STROKE_MAP_8:[[[I

    invoke-static {v6, p1}, Lcom/android/providers/contacts/HanziToStroke;->find([[[II)[I

    move-result-object v0

    .line 172
    .restart local v0    # "array":[I
    goto :goto_1

    .line 174
    .end local v0    # "array":[I
    :pswitch_8
    sget-object v6, Lcom/android/providers/contacts/MapStroke9;->HANZI_TO_STROKE_MAP_9:[[[I

    invoke-static {v6, p1}, Lcom/android/providers/contacts/HanziToStroke;->find([[[II)[I

    move-result-object v0

    .line 175
    .restart local v0    # "array":[I
    goto :goto_1

    .line 177
    .end local v0    # "array":[I
    :pswitch_9
    sget-object v6, Lcom/android/providers/contacts/MapStroke10;->HANZI_TO_STROKE_MAP_10:[[[I

    invoke-static {v6, p1}, Lcom/android/providers/contacts/HanziToStroke;->find([[[II)[I

    move-result-object v0

    .line 178
    .restart local v0    # "array":[I
    goto :goto_1

    .line 180
    .end local v0    # "array":[I
    :pswitch_a
    sget-object v6, Lcom/android/providers/contacts/MapStroke11;->HANZI_TO_STROKE_MAP_11:[[[I

    invoke-static {v6, p1}, Lcom/android/providers/contacts/HanziToStroke;->find([[[II)[I

    move-result-object v0

    .line 181
    .restart local v0    # "array":[I
    goto :goto_1

    .line 183
    .end local v0    # "array":[I
    :pswitch_b
    sget-object v6, Lcom/android/providers/contacts/MapStroke12;->HANZI_TO_STROKE_MAP_12:[[[I

    invoke-static {v6, p1}, Lcom/android/providers/contacts/HanziToStroke;->find([[[II)[I

    move-result-object v0

    .line 184
    .restart local v0    # "array":[I
    goto :goto_1

    .line 186
    .end local v0    # "array":[I
    :pswitch_c
    sget-object v6, Lcom/android/providers/contacts/MapStroke13;->HANZI_TO_STROKE_MAP_13:[[[I

    invoke-static {v6, p1}, Lcom/android/providers/contacts/HanziToStroke;->find([[[II)[I

    move-result-object v0

    .line 187
    .restart local v0    # "array":[I
    goto :goto_1

    .line 189
    .end local v0    # "array":[I
    :pswitch_d
    sget-object v6, Lcom/android/providers/contacts/MapStroke14;->HANZI_TO_STROKE_MAP_14:[[[I

    invoke-static {v6, p1}, Lcom/android/providers/contacts/HanziToStroke;->find([[[II)[I

    move-result-object v0

    .line 190
    .restart local v0    # "array":[I
    goto :goto_1

    .line 192
    .end local v0    # "array":[I
    :pswitch_e
    sget-object v6, Lcom/android/providers/contacts/MapStroke15;->HANZI_TO_STROKE_MAP_15:[[[I

    invoke-static {v6, p1}, Lcom/android/providers/contacts/HanziToStroke;->find([[[II)[I

    move-result-object v0

    .line 193
    .restart local v0    # "array":[I
    goto :goto_1

    .line 195
    .end local v0    # "array":[I
    :pswitch_f
    sget-object v6, Lcom/android/providers/contacts/MapStroke16;->HANZI_TO_STROKE_MAP_16:[[[I

    invoke-static {v6, p1}, Lcom/android/providers/contacts/HanziToStroke;->find([[[II)[I

    move-result-object v0

    .line 196
    .restart local v0    # "array":[I
    goto :goto_1

    .line 198
    .end local v0    # "array":[I
    :pswitch_10
    sget-object v6, Lcom/android/providers/contacts/MapStroke17;->HANZI_TO_STROKE_MAP_17:[[[I

    invoke-static {v6, p1}, Lcom/android/providers/contacts/HanziToStroke;->find([[[II)[I

    move-result-object v0

    .line 199
    .restart local v0    # "array":[I
    goto :goto_1

    .line 201
    .end local v0    # "array":[I
    :pswitch_11
    sget-object v6, Lcom/android/providers/contacts/MapStroke18;->HANZI_TO_STROKE_MAP_18:[[[I

    invoke-static {v6, p1}, Lcom/android/providers/contacts/HanziToStroke;->find([[[II)[I

    move-result-object v0

    .line 202
    .restart local v0    # "array":[I
    goto :goto_1

    .line 204
    .end local v0    # "array":[I
    :pswitch_12
    sget-object v6, Lcom/android/providers/contacts/MapStroke19;->HANZI_TO_STROKE_MAP_19:[[[I

    invoke-static {v6, p1}, Lcom/android/providers/contacts/HanziToStroke;->find([[[II)[I

    move-result-object v0

    .line 205
    .restart local v0    # "array":[I
    goto/16 :goto_1

    .line 207
    .end local v0    # "array":[I
    :pswitch_13
    sget-object v6, Lcom/android/providers/contacts/MapStroke20;->HANZI_TO_STROKE_MAP_20:[[[I

    invoke-static {v6, p1}, Lcom/android/providers/contacts/HanziToStroke;->find([[[II)[I

    move-result-object v0

    .line 208
    .restart local v0    # "array":[I
    goto/16 :goto_1

    .line 210
    .end local v0    # "array":[I
    :pswitch_14
    sget-object v6, Lcom/android/providers/contacts/MapStroke21;->HANZI_TO_STROKE_MAP_21:[[[I

    invoke-static {v6, p1}, Lcom/android/providers/contacts/HanziToStroke;->find([[[II)[I

    move-result-object v0

    .line 211
    .restart local v0    # "array":[I
    goto/16 :goto_1

    .line 213
    .end local v0    # "array":[I
    :pswitch_15
    sget-object v6, Lcom/android/providers/contacts/MapStroke22;->HANZI_TO_STROKE_MAP_22:[[[I

    invoke-static {v6, p1}, Lcom/android/providers/contacts/HanziToStroke;->find([[[II)[I

    move-result-object v0

    .line 214
    .restart local v0    # "array":[I
    goto/16 :goto_1

    .line 216
    .end local v0    # "array":[I
    :pswitch_16
    sget-object v6, Lcom/android/providers/contacts/MapStroke23;->HANZI_TO_STROKE_MAP_23:[[[I

    invoke-static {v6, p1}, Lcom/android/providers/contacts/HanziToStroke;->find([[[II)[I

    move-result-object v0

    .line 217
    .restart local v0    # "array":[I
    goto/16 :goto_1

    .line 219
    .end local v0    # "array":[I
    :pswitch_17
    sget-object v6, Lcom/android/providers/contacts/MapStroke24;->HANZI_TO_STROKE_MAP_24:[[[I

    invoke-static {v6, p1}, Lcom/android/providers/contacts/HanziToStroke;->find([[[II)[I

    move-result-object v0

    .line 220
    .restart local v0    # "array":[I
    goto/16 :goto_1

    .line 222
    .end local v0    # "array":[I
    :pswitch_18
    sget-object v6, Lcom/android/providers/contacts/MapStroke25;->HANZI_TO_STROKE_MAP_25:[[[I

    invoke-static {v6, p1}, Lcom/android/providers/contacts/HanziToStroke;->find([[[II)[I

    move-result-object v0

    .line 223
    .restart local v0    # "array":[I
    goto/16 :goto_1

    .line 225
    .end local v0    # "array":[I
    :pswitch_19
    sget-object v6, Lcom/android/providers/contacts/MapStroke26;->HANZI_TO_STROKE_MAP_26:[[[I

    invoke-static {v6, p1}, Lcom/android/providers/contacts/HanziToStroke;->find([[[II)[I

    move-result-object v0

    .line 226
    .restart local v0    # "array":[I
    goto/16 :goto_1

    .line 228
    .end local v0    # "array":[I
    :pswitch_1a
    sget-object v6, Lcom/android/providers/contacts/MapStroke27;->HANZI_TO_STROKE_MAP_27:[[[I

    invoke-static {v6, p1}, Lcom/android/providers/contacts/HanziToStroke;->find([[[II)[I

    move-result-object v0

    .line 229
    .restart local v0    # "array":[I
    goto/16 :goto_1

    .line 231
    .end local v0    # "array":[I
    :pswitch_1b
    sget-object v6, Lcom/android/providers/contacts/MapStroke28;->HANZI_TO_STROKE_MAP_28:[[[I

    invoke-static {v6, p1}, Lcom/android/providers/contacts/HanziToStroke;->find([[[II)[I

    move-result-object v0

    .line 232
    .restart local v0    # "array":[I
    goto/16 :goto_1

    .line 234
    .end local v0    # "array":[I
    :pswitch_1c
    sget-object v6, Lcom/android/providers/contacts/MapStroke29;->HANZI_TO_STROKE_MAP_29:[[[I

    invoke-static {v6, p1}, Lcom/android/providers/contacts/HanziToStroke;->find([[[II)[I

    move-result-object v0

    .line 235
    .restart local v0    # "array":[I
    goto/16 :goto_1

    .line 237
    .end local v0    # "array":[I
    :pswitch_1d
    sget-object v6, Lcom/android/providers/contacts/MapStroke30;->HANZI_TO_STROKE_MAP_30:[[[I

    invoke-static {v6, p1}, Lcom/android/providers/contacts/HanziToStroke;->find([[[II)[I

    move-result-object v0

    .line 238
    .restart local v0    # "array":[I
    goto/16 :goto_1

    .line 240
    .end local v0    # "array":[I
    :pswitch_1e
    sget-object v6, Lcom/android/providers/contacts/MapStroke31;->HANZI_TO_STROKE_MAP_31:[[[I

    invoke-static {v6, p1}, Lcom/android/providers/contacts/HanziToStroke;->find([[[II)[I

    move-result-object v0

    .line 241
    .restart local v0    # "array":[I
    goto/16 :goto_1

    .line 243
    .end local v0    # "array":[I
    :pswitch_1f
    sget-object v6, Lcom/android/providers/contacts/MapStroke32;->HANZI_TO_STROKE_MAP_32:[[[I

    invoke-static {v6, p1}, Lcom/android/providers/contacts/HanziToStroke;->find([[[II)[I

    move-result-object v0

    .line 244
    .restart local v0    # "array":[I
    goto/16 :goto_1

    .line 246
    .end local v0    # "array":[I
    :pswitch_20
    sget-object v6, Lcom/android/providers/contacts/MapStroke33;->HANZI_TO_STROKE_MAP_33:[[[I

    invoke-static {v6, p1}, Lcom/android/providers/contacts/HanziToStroke;->find([[[II)[I

    move-result-object v0

    .line 247
    .restart local v0    # "array":[I
    goto/16 :goto_1

    .line 249
    .end local v0    # "array":[I
    :pswitch_21
    sget-object v6, Lcom/android/providers/contacts/MapStroke34;->HANZI_TO_STROKE_MAP_34:[[[I

    invoke-static {v6, p1}, Lcom/android/providers/contacts/HanziToStroke;->find([[[II)[I

    move-result-object v0

    .line 250
    .restart local v0    # "array":[I
    goto/16 :goto_1

    .line 252
    .end local v0    # "array":[I
    :pswitch_22
    sget-object v6, Lcom/android/providers/contacts/MapStroke35;->HANZI_TO_STROKE_MAP_35:[[[I

    invoke-static {v6, p1}, Lcom/android/providers/contacts/HanziToStroke;->find([[[II)[I

    move-result-object v0

    .line 253
    .restart local v0    # "array":[I
    goto/16 :goto_1

    .line 255
    .end local v0    # "array":[I
    :pswitch_23
    sget-object v6, Lcom/android/providers/contacts/MapStroke36;->HANZI_TO_STROKE_MAP_36:[[[I

    invoke-static {v6, p1}, Lcom/android/providers/contacts/HanziToStroke;->find([[[II)[I

    move-result-object v0

    .line 256
    .restart local v0    # "array":[I
    goto/16 :goto_1

    .line 258
    .end local v0    # "array":[I
    :pswitch_24
    sget-object v6, Lcom/android/providers/contacts/MapStroke37;->HANZI_TO_STROKE_MAP_37:[[[I

    invoke-static {v6, p1}, Lcom/android/providers/contacts/HanziToStroke;->find([[[II)[I

    move-result-object v0

    .line 259
    .restart local v0    # "array":[I
    goto/16 :goto_1

    .line 261
    .end local v0    # "array":[I
    :pswitch_25
    sget-object v6, Lcom/android/providers/contacts/MapStroke38;->HANZI_TO_STROKE_MAP_38:[[[I

    invoke-static {v6, p1}, Lcom/android/providers/contacts/HanziToStroke;->find([[[II)[I

    move-result-object v0

    .line 262
    .restart local v0    # "array":[I
    goto/16 :goto_1

    .line 264
    .end local v0    # "array":[I
    :pswitch_26
    sget-object v6, Lcom/android/providers/contacts/MapStroke39;->HANZI_TO_STROKE_MAP_39:[[[I

    invoke-static {v6, p1}, Lcom/android/providers/contacts/HanziToStroke;->find([[[II)[I

    move-result-object v0

    .line 265
    .restart local v0    # "array":[I
    goto/16 :goto_1

    .line 267
    .end local v0    # "array":[I
    :pswitch_27
    sget-object v6, Lcom/android/providers/contacts/MapStroke40;->HANZI_TO_STROKE_MAP_40:[[[I

    invoke-static {v6, p1}, Lcom/android/providers/contacts/HanziToStroke;->find([[[II)[I

    move-result-object v0

    .line 268
    .restart local v0    # "array":[I
    goto/16 :goto_1

    .line 270
    .end local v0    # "array":[I
    :pswitch_28
    sget-object v6, Lcom/android/providers/contacts/MapStroke41;->HANZI_TO_STROKE_MAP_41:[[[I

    invoke-static {v6, p1}, Lcom/android/providers/contacts/HanziToStroke;->find([[[II)[I

    move-result-object v0

    .line 271
    .restart local v0    # "array":[I
    goto/16 :goto_1

    .line 273
    .end local v0    # "array":[I
    :pswitch_29
    sget-object v6, Lcom/android/providers/contacts/MapStroke42;->HANZI_TO_STROKE_MAP_42:[[[I

    invoke-static {v6, p1}, Lcom/android/providers/contacts/HanziToStroke;->find([[[II)[I

    move-result-object v0

    .line 274
    .restart local v0    # "array":[I
    goto/16 :goto_1

    .line 291
    :cond_3
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    .line 292
    .local v4, "strokes":Ljava/lang/StringBuilder;
    const/4 v1, 0x0

    .local v1, "j":I
    :goto_2
    array-length v6, v0

    if-ge v1, v6, :cond_4

    .line 293
    aget v6, v0, v1

    int-to-char v6, v6

    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 292
    add-int/lit8 v1, v1, 0x1

    goto :goto_2

    .line 295
    :cond_4
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    iput-object v6, v5, Lcom/android/providers/contacts/HanziToStroke$Token;->target:Ljava/lang/String;

    goto/16 :goto_0

    .line 148
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_7
        :pswitch_8
        :pswitch_9
        :pswitch_a
        :pswitch_b
        :pswitch_c
        :pswitch_d
        :pswitch_e
        :pswitch_f
        :pswitch_10
        :pswitch_11
        :pswitch_12
        :pswitch_13
        :pswitch_14
        :pswitch_15
        :pswitch_16
        :pswitch_17
        :pswitch_18
        :pswitch_19
        :pswitch_1a
        :pswitch_1b
        :pswitch_1c
        :pswitch_1d
        :pswitch_1e
        :pswitch_1f
        :pswitch_20
        :pswitch_21
        :pswitch_22
        :pswitch_23
        :pswitch_24
        :pswitch_25
        :pswitch_26
        :pswitch_27
        :pswitch_28
        :pswitch_29
    .end packed-switch
.end method

.method private getTokenForStroke(C)Lcom/android/providers/contacts/HanziToStroke$Token;
    .locals 10
    .param p1, "character"    # C

    .prologue
    const/4 v8, 0x3

    .line 331
    new-instance v6, Lcom/android/providers/contacts/HanziToStroke$Token;

    invoke-direct {v6}, Lcom/android/providers/contacts/HanziToStroke$Token;-><init>()V

    .line 332
    .local v6, "token":Lcom/android/providers/contacts/HanziToStroke$Token;
    invoke-static {p1}, Ljava/lang/Character;->toString(C)Ljava/lang/String;

    move-result-object v3

    .line 333
    .local v3, "letter":Ljava/lang/String;
    iput-object v3, v6, Lcom/android/providers/contacts/HanziToStroke$Token;->source:Ljava/lang/String;

    .line 334
    const/4 v7, 0x2

    iput v7, v6, Lcom/android/providers/contacts/HanziToStroke$Token;->type:I

    .line 336
    const/16 v7, 0x100

    if-ge p1, v7, :cond_0

    .line 337
    const/4 v7, 0x1

    iput v7, v6, Lcom/android/providers/contacts/HanziToStroke$Token;->type:I

    .line 338
    iput-object v3, v6, Lcom/android/providers/contacts/HanziToStroke$Token;->target:Ljava/lang/String;

    .line 512
    :goto_0
    return-object v6

    .line 340
    :cond_0
    const/16 v7, 0x4e00

    if-lt p1, v7, :cond_1

    const v7, 0x9fa5

    if-le p1, v7, :cond_2

    .line 342
    :cond_1
    iput v8, v6, Lcom/android/providers/contacts/HanziToStroke$Token;->type:I

    .line 343
    iput-object v3, v6, Lcom/android/providers/contacts/HanziToStroke$Token;->target:Ljava/lang/String;

    goto :goto_0

    .line 347
    :cond_2
    invoke-static {p1}, Lcom/android/providers/contacts/HanziToStroke;->findMapper(I)I

    move-result v4

    .line 350
    .local v4, "method":I
    packed-switch v4, :pswitch_data_0

    .line 479
    iput v8, v6, Lcom/android/providers/contacts/HanziToStroke$Token;->type:I

    .line 480
    iput-object v3, v6, Lcom/android/providers/contacts/HanziToStroke$Token;->target:Ljava/lang/String;

    goto :goto_0

    .line 352
    :pswitch_0
    sget-object v7, Lcom/android/providers/contacts/MapStroke1;->HANZI_TO_STROKE_MAP_1:[[[I

    invoke-static {v7, p1}, Lcom/android/providers/contacts/HanziToStroke;->find([[[II)[I

    move-result-object v0

    .line 484
    .local v0, "array":[I
    :goto_1
    const/4 v7, 0x0

    aget v7, v0, v7

    if-nez v7, :cond_3

    .line 485
    iput v8, v6, Lcom/android/providers/contacts/HanziToStroke$Token;->type:I

    .line 486
    iput-object v3, v6, Lcom/android/providers/contacts/HanziToStroke$Token;->target:Ljava/lang/String;

    goto :goto_0

    .line 355
    .end local v0    # "array":[I
    :pswitch_1
    sget-object v7, Lcom/android/providers/contacts/MapStroke2;->HANZI_TO_STROKE_MAP_2:[[[I

    invoke-static {v7, p1}, Lcom/android/providers/contacts/HanziToStroke;->find([[[II)[I

    move-result-object v0

    .line 356
    .restart local v0    # "array":[I
    goto :goto_1

    .line 358
    .end local v0    # "array":[I
    :pswitch_2
    sget-object v7, Lcom/android/providers/contacts/MapStroke3;->HANZI_TO_STROKE_MAP_3:[[[I

    invoke-static {v7, p1}, Lcom/android/providers/contacts/HanziToStroke;->find([[[II)[I

    move-result-object v0

    .line 359
    .restart local v0    # "array":[I
    goto :goto_1

    .line 361
    .end local v0    # "array":[I
    :pswitch_3
    sget-object v7, Lcom/android/providers/contacts/MapStroke4;->HANZI_TO_STROKE_MAP_4:[[[I

    invoke-static {v7, p1}, Lcom/android/providers/contacts/HanziToStroke;->find([[[II)[I

    move-result-object v0

    .line 362
    .restart local v0    # "array":[I
    goto :goto_1

    .line 364
    .end local v0    # "array":[I
    :pswitch_4
    sget-object v7, Lcom/android/providers/contacts/MapStroke5;->HANZI_TO_STROKE_MAP_5:[[[I

    invoke-static {v7, p1}, Lcom/android/providers/contacts/HanziToStroke;->find([[[II)[I

    move-result-object v0

    .line 365
    .restart local v0    # "array":[I
    goto :goto_1

    .line 367
    .end local v0    # "array":[I
    :pswitch_5
    sget-object v7, Lcom/android/providers/contacts/MapStroke6;->HANZI_TO_STROKE_MAP_6:[[[I

    invoke-static {v7, p1}, Lcom/android/providers/contacts/HanziToStroke;->find([[[II)[I

    move-result-object v0

    .line 368
    .restart local v0    # "array":[I
    goto :goto_1

    .line 370
    .end local v0    # "array":[I
    :pswitch_6
    sget-object v7, Lcom/android/providers/contacts/MapStroke7;->HANZI_TO_STROKE_MAP_7:[[[I

    invoke-static {v7, p1}, Lcom/android/providers/contacts/HanziToStroke;->find([[[II)[I

    move-result-object v0

    .line 371
    .restart local v0    # "array":[I
    goto :goto_1

    .line 373
    .end local v0    # "array":[I
    :pswitch_7
    sget-object v7, Lcom/android/providers/contacts/MapStroke8;->HANZI_TO_STROKE_MAP_8:[[[I

    invoke-static {v7, p1}, Lcom/android/providers/contacts/HanziToStroke;->find([[[II)[I

    move-result-object v0

    .line 374
    .restart local v0    # "array":[I
    goto :goto_1

    .line 376
    .end local v0    # "array":[I
    :pswitch_8
    sget-object v7, Lcom/android/providers/contacts/MapStroke9;->HANZI_TO_STROKE_MAP_9:[[[I

    invoke-static {v7, p1}, Lcom/android/providers/contacts/HanziToStroke;->find([[[II)[I

    move-result-object v0

    .line 377
    .restart local v0    # "array":[I
    goto :goto_1

    .line 379
    .end local v0    # "array":[I
    :pswitch_9
    sget-object v7, Lcom/android/providers/contacts/MapStroke10;->HANZI_TO_STROKE_MAP_10:[[[I

    invoke-static {v7, p1}, Lcom/android/providers/contacts/HanziToStroke;->find([[[II)[I

    move-result-object v0

    .line 380
    .restart local v0    # "array":[I
    goto :goto_1

    .line 382
    .end local v0    # "array":[I
    :pswitch_a
    sget-object v7, Lcom/android/providers/contacts/MapStroke11;->HANZI_TO_STROKE_MAP_11:[[[I

    invoke-static {v7, p1}, Lcom/android/providers/contacts/HanziToStroke;->find([[[II)[I

    move-result-object v0

    .line 383
    .restart local v0    # "array":[I
    goto :goto_1

    .line 385
    .end local v0    # "array":[I
    :pswitch_b
    sget-object v7, Lcom/android/providers/contacts/MapStroke12;->HANZI_TO_STROKE_MAP_12:[[[I

    invoke-static {v7, p1}, Lcom/android/providers/contacts/HanziToStroke;->find([[[II)[I

    move-result-object v0

    .line 386
    .restart local v0    # "array":[I
    goto :goto_1

    .line 388
    .end local v0    # "array":[I
    :pswitch_c
    sget-object v7, Lcom/android/providers/contacts/MapStroke13;->HANZI_TO_STROKE_MAP_13:[[[I

    invoke-static {v7, p1}, Lcom/android/providers/contacts/HanziToStroke;->find([[[II)[I

    move-result-object v0

    .line 389
    .restart local v0    # "array":[I
    goto :goto_1

    .line 391
    .end local v0    # "array":[I
    :pswitch_d
    sget-object v7, Lcom/android/providers/contacts/MapStroke14;->HANZI_TO_STROKE_MAP_14:[[[I

    invoke-static {v7, p1}, Lcom/android/providers/contacts/HanziToStroke;->find([[[II)[I

    move-result-object v0

    .line 392
    .restart local v0    # "array":[I
    goto :goto_1

    .line 394
    .end local v0    # "array":[I
    :pswitch_e
    sget-object v7, Lcom/android/providers/contacts/MapStroke15;->HANZI_TO_STROKE_MAP_15:[[[I

    invoke-static {v7, p1}, Lcom/android/providers/contacts/HanziToStroke;->find([[[II)[I

    move-result-object v0

    .line 395
    .restart local v0    # "array":[I
    goto :goto_1

    .line 397
    .end local v0    # "array":[I
    :pswitch_f
    sget-object v7, Lcom/android/providers/contacts/MapStroke16;->HANZI_TO_STROKE_MAP_16:[[[I

    invoke-static {v7, p1}, Lcom/android/providers/contacts/HanziToStroke;->find([[[II)[I

    move-result-object v0

    .line 398
    .restart local v0    # "array":[I
    goto :goto_1

    .line 400
    .end local v0    # "array":[I
    :pswitch_10
    sget-object v7, Lcom/android/providers/contacts/MapStroke17;->HANZI_TO_STROKE_MAP_17:[[[I

    invoke-static {v7, p1}, Lcom/android/providers/contacts/HanziToStroke;->find([[[II)[I

    move-result-object v0

    .line 401
    .restart local v0    # "array":[I
    goto :goto_1

    .line 403
    .end local v0    # "array":[I
    :pswitch_11
    sget-object v7, Lcom/android/providers/contacts/MapStroke18;->HANZI_TO_STROKE_MAP_18:[[[I

    invoke-static {v7, p1}, Lcom/android/providers/contacts/HanziToStroke;->find([[[II)[I

    move-result-object v0

    .line 404
    .restart local v0    # "array":[I
    goto :goto_1

    .line 406
    .end local v0    # "array":[I
    :pswitch_12
    sget-object v7, Lcom/android/providers/contacts/MapStroke19;->HANZI_TO_STROKE_MAP_19:[[[I

    invoke-static {v7, p1}, Lcom/android/providers/contacts/HanziToStroke;->find([[[II)[I

    move-result-object v0

    .line 407
    .restart local v0    # "array":[I
    goto/16 :goto_1

    .line 409
    .end local v0    # "array":[I
    :pswitch_13
    sget-object v7, Lcom/android/providers/contacts/MapStroke20;->HANZI_TO_STROKE_MAP_20:[[[I

    invoke-static {v7, p1}, Lcom/android/providers/contacts/HanziToStroke;->find([[[II)[I

    move-result-object v0

    .line 410
    .restart local v0    # "array":[I
    goto/16 :goto_1

    .line 412
    .end local v0    # "array":[I
    :pswitch_14
    sget-object v7, Lcom/android/providers/contacts/MapStroke21;->HANZI_TO_STROKE_MAP_21:[[[I

    invoke-static {v7, p1}, Lcom/android/providers/contacts/HanziToStroke;->find([[[II)[I

    move-result-object v0

    .line 413
    .restart local v0    # "array":[I
    goto/16 :goto_1

    .line 415
    .end local v0    # "array":[I
    :pswitch_15
    sget-object v7, Lcom/android/providers/contacts/MapStroke22;->HANZI_TO_STROKE_MAP_22:[[[I

    invoke-static {v7, p1}, Lcom/android/providers/contacts/HanziToStroke;->find([[[II)[I

    move-result-object v0

    .line 416
    .restart local v0    # "array":[I
    goto/16 :goto_1

    .line 418
    .end local v0    # "array":[I
    :pswitch_16
    sget-object v7, Lcom/android/providers/contacts/MapStroke23;->HANZI_TO_STROKE_MAP_23:[[[I

    invoke-static {v7, p1}, Lcom/android/providers/contacts/HanziToStroke;->find([[[II)[I

    move-result-object v0

    .line 419
    .restart local v0    # "array":[I
    goto/16 :goto_1

    .line 421
    .end local v0    # "array":[I
    :pswitch_17
    sget-object v7, Lcom/android/providers/contacts/MapStroke24;->HANZI_TO_STROKE_MAP_24:[[[I

    invoke-static {v7, p1}, Lcom/android/providers/contacts/HanziToStroke;->find([[[II)[I

    move-result-object v0

    .line 422
    .restart local v0    # "array":[I
    goto/16 :goto_1

    .line 424
    .end local v0    # "array":[I
    :pswitch_18
    sget-object v7, Lcom/android/providers/contacts/MapStroke25;->HANZI_TO_STROKE_MAP_25:[[[I

    invoke-static {v7, p1}, Lcom/android/providers/contacts/HanziToStroke;->find([[[II)[I

    move-result-object v0

    .line 425
    .restart local v0    # "array":[I
    goto/16 :goto_1

    .line 427
    .end local v0    # "array":[I
    :pswitch_19
    sget-object v7, Lcom/android/providers/contacts/MapStroke26;->HANZI_TO_STROKE_MAP_26:[[[I

    invoke-static {v7, p1}, Lcom/android/providers/contacts/HanziToStroke;->find([[[II)[I

    move-result-object v0

    .line 428
    .restart local v0    # "array":[I
    goto/16 :goto_1

    .line 430
    .end local v0    # "array":[I
    :pswitch_1a
    sget-object v7, Lcom/android/providers/contacts/MapStroke27;->HANZI_TO_STROKE_MAP_27:[[[I

    invoke-static {v7, p1}, Lcom/android/providers/contacts/HanziToStroke;->find([[[II)[I

    move-result-object v0

    .line 431
    .restart local v0    # "array":[I
    goto/16 :goto_1

    .line 433
    .end local v0    # "array":[I
    :pswitch_1b
    sget-object v7, Lcom/android/providers/contacts/MapStroke28;->HANZI_TO_STROKE_MAP_28:[[[I

    invoke-static {v7, p1}, Lcom/android/providers/contacts/HanziToStroke;->find([[[II)[I

    move-result-object v0

    .line 434
    .restart local v0    # "array":[I
    goto/16 :goto_1

    .line 436
    .end local v0    # "array":[I
    :pswitch_1c
    sget-object v7, Lcom/android/providers/contacts/MapStroke29;->HANZI_TO_STROKE_MAP_29:[[[I

    invoke-static {v7, p1}, Lcom/android/providers/contacts/HanziToStroke;->find([[[II)[I

    move-result-object v0

    .line 437
    .restart local v0    # "array":[I
    goto/16 :goto_1

    .line 439
    .end local v0    # "array":[I
    :pswitch_1d
    sget-object v7, Lcom/android/providers/contacts/MapStroke30;->HANZI_TO_STROKE_MAP_30:[[[I

    invoke-static {v7, p1}, Lcom/android/providers/contacts/HanziToStroke;->find([[[II)[I

    move-result-object v0

    .line 440
    .restart local v0    # "array":[I
    goto/16 :goto_1

    .line 442
    .end local v0    # "array":[I
    :pswitch_1e
    sget-object v7, Lcom/android/providers/contacts/MapStroke31;->HANZI_TO_STROKE_MAP_31:[[[I

    invoke-static {v7, p1}, Lcom/android/providers/contacts/HanziToStroke;->find([[[II)[I

    move-result-object v0

    .line 443
    .restart local v0    # "array":[I
    goto/16 :goto_1

    .line 445
    .end local v0    # "array":[I
    :pswitch_1f
    sget-object v7, Lcom/android/providers/contacts/MapStroke32;->HANZI_TO_STROKE_MAP_32:[[[I

    invoke-static {v7, p1}, Lcom/android/providers/contacts/HanziToStroke;->find([[[II)[I

    move-result-object v0

    .line 446
    .restart local v0    # "array":[I
    goto/16 :goto_1

    .line 448
    .end local v0    # "array":[I
    :pswitch_20
    sget-object v7, Lcom/android/providers/contacts/MapStroke33;->HANZI_TO_STROKE_MAP_33:[[[I

    invoke-static {v7, p1}, Lcom/android/providers/contacts/HanziToStroke;->find([[[II)[I

    move-result-object v0

    .line 449
    .restart local v0    # "array":[I
    goto/16 :goto_1

    .line 451
    .end local v0    # "array":[I
    :pswitch_21
    sget-object v7, Lcom/android/providers/contacts/MapStroke34;->HANZI_TO_STROKE_MAP_34:[[[I

    invoke-static {v7, p1}, Lcom/android/providers/contacts/HanziToStroke;->find([[[II)[I

    move-result-object v0

    .line 452
    .restart local v0    # "array":[I
    goto/16 :goto_1

    .line 454
    .end local v0    # "array":[I
    :pswitch_22
    sget-object v7, Lcom/android/providers/contacts/MapStroke35;->HANZI_TO_STROKE_MAP_35:[[[I

    invoke-static {v7, p1}, Lcom/android/providers/contacts/HanziToStroke;->find([[[II)[I

    move-result-object v0

    .line 455
    .restart local v0    # "array":[I
    goto/16 :goto_1

    .line 457
    .end local v0    # "array":[I
    :pswitch_23
    sget-object v7, Lcom/android/providers/contacts/MapStroke36;->HANZI_TO_STROKE_MAP_36:[[[I

    invoke-static {v7, p1}, Lcom/android/providers/contacts/HanziToStroke;->find([[[II)[I

    move-result-object v0

    .line 458
    .restart local v0    # "array":[I
    goto/16 :goto_1

    .line 460
    .end local v0    # "array":[I
    :pswitch_24
    sget-object v7, Lcom/android/providers/contacts/MapStroke37;->HANZI_TO_STROKE_MAP_37:[[[I

    invoke-static {v7, p1}, Lcom/android/providers/contacts/HanziToStroke;->find([[[II)[I

    move-result-object v0

    .line 461
    .restart local v0    # "array":[I
    goto/16 :goto_1

    .line 463
    .end local v0    # "array":[I
    :pswitch_25
    sget-object v7, Lcom/android/providers/contacts/MapStroke38;->HANZI_TO_STROKE_MAP_38:[[[I

    invoke-static {v7, p1}, Lcom/android/providers/contacts/HanziToStroke;->find([[[II)[I

    move-result-object v0

    .line 464
    .restart local v0    # "array":[I
    goto/16 :goto_1

    .line 466
    .end local v0    # "array":[I
    :pswitch_26
    sget-object v7, Lcom/android/providers/contacts/MapStroke39;->HANZI_TO_STROKE_MAP_39:[[[I

    invoke-static {v7, p1}, Lcom/android/providers/contacts/HanziToStroke;->find([[[II)[I

    move-result-object v0

    .line 467
    .restart local v0    # "array":[I
    goto/16 :goto_1

    .line 469
    .end local v0    # "array":[I
    :pswitch_27
    sget-object v7, Lcom/android/providers/contacts/MapStroke40;->HANZI_TO_STROKE_MAP_40:[[[I

    invoke-static {v7, p1}, Lcom/android/providers/contacts/HanziToStroke;->find([[[II)[I

    move-result-object v0

    .line 470
    .restart local v0    # "array":[I
    goto/16 :goto_1

    .line 472
    .end local v0    # "array":[I
    :pswitch_28
    sget-object v7, Lcom/android/providers/contacts/MapStroke41;->HANZI_TO_STROKE_MAP_41:[[[I

    invoke-static {v7, p1}, Lcom/android/providers/contacts/HanziToStroke;->find([[[II)[I

    move-result-object v0

    .line 473
    .restart local v0    # "array":[I
    goto/16 :goto_1

    .line 475
    .end local v0    # "array":[I
    :pswitch_29
    sget-object v7, Lcom/android/providers/contacts/MapStroke42;->HANZI_TO_STROKE_MAP_42:[[[I

    invoke-static {v7, p1}, Lcom/android/providers/contacts/HanziToStroke;->find([[[II)[I

    move-result-object v0

    .line 476
    .restart local v0    # "array":[I
    goto/16 :goto_1

    .line 492
    :cond_3
    const-string v7, "**HanziToStroke**"

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "getTokenForStroke the number of stroke = "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    array-length v9, v0

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v7, v8}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 494
    const/16 v2, 0x41

    .line 495
    .local v2, "ch_U":I
    const/16 v1, 0x61

    .line 496
    .local v1, "ch_L":I
    const/4 v5, -0x1

    .line 498
    .local v5, "offset":I
    invoke-static {p1}, Ljava/lang/Character;->toString(C)Ljava/lang/String;

    move-result-object v7

    invoke-direct {p0, v7}, Lcom/android/providers/contacts/HanziToStroke;->checkTemp(Ljava/lang/String;)I

    move-result v5

    .line 499
    if-gez v5, :cond_4

    .line 500
    array-length v5, v0

    .line 503
    :cond_4
    const/16 v7, 0x1a

    if-le v5, v7, :cond_5

    .line 504
    add-int/lit8 v1, v5, 0x60

    .line 505
    int-to-char v7, v1

    invoke-static {v7}, Ljava/lang/Character;->toString(C)Ljava/lang/String;

    move-result-object v7

    iput-object v7, v6, Lcom/android/providers/contacts/HanziToStroke$Token;->target:Ljava/lang/String;

    .line 510
    :goto_2
    const-string v7, "**HanziToStroke**"

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "getTokenForStroke the number of stroke TOKEN TARGET IS = "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    iget-object v9, v6, Lcom/android/providers/contacts/HanziToStroke$Token;->target:Ljava/lang/String;

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v7, v8}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0

    .line 507
    :cond_5
    add-int/lit8 v2, v5, 0x40

    .line 508
    int-to-char v7, v2

    invoke-static {v7}, Ljava/lang/Character;->toString(C)Ljava/lang/String;

    move-result-object v7

    iput-object v7, v6, Lcom/android/providers/contacts/HanziToStroke$Token;->target:Ljava/lang/String;

    goto :goto_2

    .line 350
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_7
        :pswitch_8
        :pswitch_9
        :pswitch_a
        :pswitch_b
        :pswitch_c
        :pswitch_d
        :pswitch_e
        :pswitch_f
        :pswitch_10
        :pswitch_11
        :pswitch_12
        :pswitch_13
        :pswitch_14
        :pswitch_15
        :pswitch_16
        :pswitch_17
        :pswitch_18
        :pswitch_19
        :pswitch_1a
        :pswitch_1b
        :pswitch_1c
        :pswitch_1d
        :pswitch_1e
        :pswitch_1f
        :pswitch_20
        :pswitch_21
        :pswitch_22
        :pswitch_23
        :pswitch_24
        :pswitch_25
        :pswitch_26
        :pswitch_27
        :pswitch_28
        :pswitch_29
    .end packed-switch
.end method


# virtual methods
.method public get(Ljava/lang/String;)Ljava/util/ArrayList;
    .locals 7
    .param p1, "input"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/android/providers/contacts/HanziToStroke$Token;",
            ">;"
        }
    .end annotation

    .prologue
    .line 99
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v6

    if-eqz v6, :cond_1

    .line 100
    const/4 v5, 0x0

    .line 125
    :cond_0
    return-object v5

    .line 103
    :cond_1
    new-instance v5, Ljava/util/ArrayList;

    invoke-direct {v5}, Ljava/util/ArrayList;-><init>()V

    .line 106
    .local v5, "tokens":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/android/providers/contacts/HanziToStroke$Token;>;"
    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v3

    .line 108
    .local v3, "inputLength":I
    const/4 v6, 0x0

    invoke-virtual {p1, v6}, Ljava/lang/String;->charAt(I)C

    move-result v6

    invoke-direct {p0, v6}, Lcom/android/providers/contacts/HanziToStroke;->getToken(C)Lcom/android/providers/contacts/HanziToStroke$Token;

    move-result-object v1

    .line 110
    .local v1, "currentToken":Lcom/android/providers/contacts/HanziToStroke$Token;
    invoke-virtual {v5, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 112
    const/4 v2, 0x1

    .local v2, "i":I
    :goto_0
    if-ge v2, v3, :cond_0

    .line 113
    invoke-virtual {p1, v2}, Ljava/lang/String;->charAt(I)C

    move-result v0

    .line 114
    .local v0, "character":C
    invoke-direct {p0, v0}, Lcom/android/providers/contacts/HanziToStroke;->getToken(C)Lcom/android/providers/contacts/HanziToStroke$Token;

    move-result-object v4

    .line 115
    .local v4, "token":Lcom/android/providers/contacts/HanziToStroke$Token;
    iget v6, v4, Lcom/android/providers/contacts/HanziToStroke$Token;->type:I

    packed-switch v6, :pswitch_data_0

    .line 112
    :goto_1
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 118
    :pswitch_0
    invoke-virtual {v5, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 121
    :pswitch_1
    invoke-virtual {v5, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 115
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method public getNew(Ljava/lang/String;)Ljava/util/ArrayList;
    .locals 7
    .param p1, "input"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/android/providers/contacts/HanziToStroke$Token;",
            ">;"
        }
    .end annotation

    .prologue
    .line 301
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v6

    if-eqz v6, :cond_1

    .line 302
    const/4 v5, 0x0

    .line 327
    :cond_0
    return-object v5

    .line 305
    :cond_1
    new-instance v5, Ljava/util/ArrayList;

    invoke-direct {v5}, Ljava/util/ArrayList;-><init>()V

    .line 308
    .local v5, "tokens":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/android/providers/contacts/HanziToStroke$Token;>;"
    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v3

    .line 310
    .local v3, "inputLength":I
    const/4 v6, 0x0

    invoke-virtual {p1, v6}, Ljava/lang/String;->charAt(I)C

    move-result v6

    invoke-direct {p0, v6}, Lcom/android/providers/contacts/HanziToStroke;->getTokenForStroke(C)Lcom/android/providers/contacts/HanziToStroke$Token;

    move-result-object v1

    .line 312
    .local v1, "currentToken":Lcom/android/providers/contacts/HanziToStroke$Token;
    invoke-virtual {v5, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 314
    const/4 v2, 0x1

    .local v2, "i":I
    :goto_0
    if-ge v2, v3, :cond_0

    .line 315
    invoke-virtual {p1, v2}, Ljava/lang/String;->charAt(I)C

    move-result v0

    .line 316
    .local v0, "character":C
    invoke-direct {p0, v0}, Lcom/android/providers/contacts/HanziToStroke;->getTokenForStroke(C)Lcom/android/providers/contacts/HanziToStroke$Token;

    move-result-object v4

    .line 317
    .local v4, "token":Lcom/android/providers/contacts/HanziToStroke$Token;
    iget v6, v4, Lcom/android/providers/contacts/HanziToStroke$Token;->type:I

    packed-switch v6, :pswitch_data_0

    .line 314
    :goto_1
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 320
    :pswitch_0
    invoke-virtual {v5, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 323
    :pswitch_1
    invoke-virtual {v5, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 317
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method
