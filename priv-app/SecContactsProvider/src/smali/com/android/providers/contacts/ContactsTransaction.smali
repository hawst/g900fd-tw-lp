.class public Lcom/android/providers/contacts/ContactsTransaction;
.super Ljava/lang/Object;
.source "ContactsTransaction.java"


# instance fields
.field private final mBatch:Z

.field private final mDatabaseTagMap:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Landroid/database/sqlite/SQLiteDatabase;",
            ">;"
        }
    .end annotation
.end field

.field private final mDatabasesForTransaction:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Landroid/database/sqlite/SQLiteDatabase;",
            ">;"
        }
    .end annotation
.end field

.field private mIsDirty:Z

.field private mYieldFailed:Z


# direct methods
.method public constructor <init>(Z)V
    .locals 1
    .param p1, "batch"    # Z

    .prologue
    .line 72
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 73
    iput-boolean p1, p0, Lcom/android/providers/contacts/ContactsTransaction;->mBatch:Z

    .line 74
    invoke-static {}, Lcom/google/android/collect/Lists;->newArrayList()Ljava/util/ArrayList;

    move-result-object v0

    iput-object v0, p0, Lcom/android/providers/contacts/ContactsTransaction;->mDatabasesForTransaction:Ljava/util/List;

    .line 75
    invoke-static {}, Lcom/google/android/collect/Maps;->newHashMap()Ljava/util/HashMap;

    move-result-object v0

    iput-object v0, p0, Lcom/android/providers/contacts/ContactsTransaction;->mDatabaseTagMap:Ljava/util/Map;

    .line 76
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/android/providers/contacts/ContactsTransaction;->mIsDirty:Z

    .line 77
    return-void
.end method


# virtual methods
.method public finish(Z)V
    .locals 5
    .param p1, "callerIsBatch"    # Z

    .prologue
    .line 191
    iget-boolean v3, p0, Lcom/android/providers/contacts/ContactsTransaction;->mBatch:Z

    if-eqz v3, :cond_0

    if-eqz p1, :cond_4

    .line 192
    :cond_0
    iget-object v3, p0, Lcom/android/providers/contacts/ContactsTransaction;->mDatabasesForTransaction:Ljava/util/List;

    invoke-interface {v3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    .local v2, "i$":Ljava/util/Iterator;
    :cond_1
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_3

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/database/sqlite/SQLiteDatabase;

    .line 201
    .local v0, "db":Landroid/database/sqlite/SQLiteDatabase;
    iget-boolean v3, p0, Lcom/android/providers/contacts/ContactsTransaction;->mYieldFailed:Z

    if-eqz v3, :cond_2

    invoke-virtual {v0}, Landroid/database/sqlite/SQLiteDatabase;->isDbLockedByCurrentThread()Z

    move-result v3

    if-eqz v3, :cond_1

    .line 206
    :cond_2
    :try_start_0
    invoke-virtual {v0}, Landroid/database/sqlite/SQLiteDatabase;->endTransaction()V
    :try_end_0
    .catch Landroid/database/sqlite/SQLiteDiskIOException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 207
    :catch_0
    move-exception v1

    .line 208
    .local v1, "e":Landroid/database/sqlite/SQLiteDiskIOException;
    const-string v3, "ContactsProvider"

    const-string v4, "Writing exception to parcel. SQLiteDiskIOException"

    invoke-static {v3, v4}, Landroid/util/secutil/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 211
    .end local v0    # "db":Landroid/database/sqlite/SQLiteDatabase;
    .end local v1    # "e":Landroid/database/sqlite/SQLiteDiskIOException;
    :cond_3
    iget-object v3, p0, Lcom/android/providers/contacts/ContactsTransaction;->mDatabasesForTransaction:Ljava/util/List;

    invoke-interface {v3}, Ljava/util/List;->clear()V

    .line 212
    iget-object v3, p0, Lcom/android/providers/contacts/ContactsTransaction;->mDatabaseTagMap:Ljava/util/Map;

    invoke-interface {v3}, Ljava/util/Map;->clear()V

    .line 213
    const/4 v3, 0x0

    iput-boolean v3, p0, Lcom/android/providers/contacts/ContactsTransaction;->mIsDirty:Z

    .line 215
    .end local v2    # "i$":Ljava/util/Iterator;
    :cond_4
    return-void
.end method

.method public getDbForTag(Ljava/lang/String;)Landroid/database/sqlite/SQLiteDatabase;
    .locals 1
    .param p1, "tag"    # Ljava/lang/String;

    .prologue
    .line 138
    iget-object v0, p0, Lcom/android/providers/contacts/ContactsTransaction;->mDatabaseTagMap:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/database/sqlite/SQLiteDatabase;

    return-object v0
.end method

.method public hasDbInTransaction(Ljava/lang/String;)Z
    .locals 1
    .param p1, "tag"    # Ljava/lang/String;

    .prologue
    .line 128
    iget-object v0, p0, Lcom/android/providers/contacts/ContactsTransaction;->mDatabaseTagMap:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method public isBatch()Z
    .locals 1

    .prologue
    .line 80
    iget-boolean v0, p0, Lcom/android/providers/contacts/ContactsTransaction;->mBatch:Z

    return v0
.end method

.method public isDirty()Z
    .locals 1

    .prologue
    .line 84
    iget-boolean v0, p0, Lcom/android/providers/contacts/ContactsTransaction;->mIsDirty:Z

    return v0
.end method

.method public markDirty()V
    .locals 1

    .prologue
    .line 88
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/android/providers/contacts/ContactsTransaction;->mIsDirty:Z

    .line 89
    return-void
.end method

.method public markSuccessful(Z)V
    .locals 3
    .param p1, "callerIsBatch"    # Z

    .prologue
    .line 162
    iget-boolean v2, p0, Lcom/android/providers/contacts/ContactsTransaction;->mBatch:Z

    if-eqz v2, :cond_0

    if-eqz p1, :cond_1

    .line 163
    :cond_0
    iget-object v2, p0, Lcom/android/providers/contacts/ContactsTransaction;->mDatabasesForTransaction:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/database/sqlite/SQLiteDatabase;

    .line 164
    .local v0, "db":Landroid/database/sqlite/SQLiteDatabase;
    invoke-virtual {v0}, Landroid/database/sqlite/SQLiteDatabase;->setTransactionSuccessful()V

    goto :goto_0

    .line 167
    .end local v0    # "db":Landroid/database/sqlite/SQLiteDatabase;
    .end local v1    # "i$":Ljava/util/Iterator;
    :cond_1
    return-void
.end method

.method public markYieldFailed()V
    .locals 1

    .prologue
    .line 92
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/android/providers/contacts/ContactsTransaction;->mYieldFailed:Z

    .line 93
    return-void
.end method

.method public removeDbForTag(Ljava/lang/String;)Landroid/database/sqlite/SQLiteDatabase;
    .locals 2
    .param p1, "tag"    # Ljava/lang/String;

    .prologue
    .line 150
    iget-object v1, p0, Lcom/android/providers/contacts/ContactsTransaction;->mDatabaseTagMap:Ljava/util/Map;

    invoke-interface {v1, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/database/sqlite/SQLiteDatabase;

    .line 151
    .local v0, "db":Landroid/database/sqlite/SQLiteDatabase;
    iget-object v1, p0, Lcom/android/providers/contacts/ContactsTransaction;->mDatabaseTagMap:Ljava/util/Map;

    invoke-interface {v1, p1}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 152
    iget-object v1, p0, Lcom/android/providers/contacts/ContactsTransaction;->mDatabasesForTransaction:Ljava/util/List;

    invoke-interface {v1, v0}, Ljava/util/List;->remove(Ljava/lang/Object;)Z

    .line 153
    return-object v0
.end method

.method public startTransactionForDb(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;Landroid/database/sqlite/SQLiteTransactionListener;)V
    .locals 2
    .param p1, "db"    # Landroid/database/sqlite/SQLiteDatabase;
    .param p2, "tag"    # Ljava/lang/String;
    .param p3, "listener"    # Landroid/database/sqlite/SQLiteTransactionListener;

    .prologue
    .line 111
    invoke-virtual {p0, p2}, Lcom/android/providers/contacts/ContactsTransaction;->hasDbInTransaction(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 114
    iget-object v0, p0, Lcom/android/providers/contacts/ContactsTransaction;->mDatabasesForTransaction:Ljava/util/List;

    const/4 v1, 0x0

    invoke-interface {v0, v1, p1}, Ljava/util/List;->add(ILjava/lang/Object;)V

    .line 115
    iget-object v0, p0, Lcom/android/providers/contacts/ContactsTransaction;->mDatabaseTagMap:Ljava/util/Map;

    invoke-interface {v0, p2, p1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 116
    if-eqz p3, :cond_1

    .line 117
    invoke-virtual {p1, p3}, Landroid/database/sqlite/SQLiteDatabase;->beginTransactionWithListenerNonExclusive(Landroid/database/sqlite/SQLiteTransactionListener;)V

    .line 122
    :cond_0
    :goto_0
    return-void

    .line 119
    :cond_1
    invoke-virtual {p1}, Landroid/database/sqlite/SQLiteDatabase;->beginTransactionNonExclusive()V

    goto :goto_0
.end method
