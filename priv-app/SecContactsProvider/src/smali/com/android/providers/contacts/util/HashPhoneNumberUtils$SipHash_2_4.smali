.class public Lcom/android/providers/contacts/util/HashPhoneNumberUtils$SipHash_2_4;
.super Ljava/lang/Object;
.source "HashPhoneNumberUtils.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/providers/contacts/util/HashPhoneNumberUtils;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "SipHash_2_4"
.end annotation


# static fields
.field static LONG_BYTES:I

.field static final key:[B

.field private static m:J

.field private static m_idx:I

.field private static msg_byte_counter:B

.field private static v0:J

.field private static final v0_init:J

.field private static v1:J

.field private static final v1_init:J

.field private static v2:J

.field private static final v2_init:J

.field private static v3:J

.field private static final v3_init:J


# direct methods
.method static constructor <clinit>()V
    .locals 6

    .prologue
    const/16 v5, 0x8

    const/4 v4, 0x0

    .line 80
    sput v5, Lcom/android/providers/contacts/util/HashPhoneNumberUtils$SipHash_2_4;->LONG_BYTES:I

    .line 82
    sput-byte v4, Lcom/android/providers/contacts/util/HashPhoneNumberUtils$SipHash_2_4;->msg_byte_counter:B

    .line 85
    sput v4, Lcom/android/providers/contacts/util/HashPhoneNumberUtils$SipHash_2_4;->m_idx:I

    .line 89
    const-string v0, "2309851Cdgewlk3E"

    invoke-virtual {v0}, Ljava/lang/String;->getBytes()[B

    move-result-object v0

    sput-object v0, Lcom/android/providers/contacts/util/HashPhoneNumberUtils$SipHash_2_4;->key:[B

    .line 91
    const-wide v0, 0x736f6d6570736575L    # 1.0986868386607877E248

    sget-object v2, Lcom/android/providers/contacts/util/HashPhoneNumberUtils$SipHash_2_4;->key:[B

    invoke-static {v2, v4}, Lcom/android/providers/contacts/util/HashPhoneNumberUtils$SipHash_2_4;->bytesLEtoLong([BI)J

    move-result-wide v2

    xor-long/2addr v0, v2

    sput-wide v0, Lcom/android/providers/contacts/util/HashPhoneNumberUtils$SipHash_2_4;->v0_init:J

    .line 93
    const-wide v0, 0x646f72616e646f6dL    # 6.222199573468475E175

    sget-object v2, Lcom/android/providers/contacts/util/HashPhoneNumberUtils$SipHash_2_4;->key:[B

    invoke-static {v2, v5}, Lcom/android/providers/contacts/util/HashPhoneNumberUtils$SipHash_2_4;->bytesLEtoLong([BI)J

    move-result-wide v2

    xor-long/2addr v0, v2

    sput-wide v0, Lcom/android/providers/contacts/util/HashPhoneNumberUtils$SipHash_2_4;->v1_init:J

    .line 95
    const-wide v0, 0x6c7967656e657261L    # 3.4208747916531402E214

    sget-object v2, Lcom/android/providers/contacts/util/HashPhoneNumberUtils$SipHash_2_4;->key:[B

    invoke-static {v2, v4}, Lcom/android/providers/contacts/util/HashPhoneNumberUtils$SipHash_2_4;->bytesLEtoLong([BI)J

    move-result-wide v2

    xor-long/2addr v0, v2

    sput-wide v0, Lcom/android/providers/contacts/util/HashPhoneNumberUtils$SipHash_2_4;->v2_init:J

    .line 97
    const-wide v0, 0x7465646279746573L    # 4.901176695720602E252

    sget-object v2, Lcom/android/providers/contacts/util/HashPhoneNumberUtils$SipHash_2_4;->key:[B

    invoke-static {v2, v5}, Lcom/android/providers/contacts/util/HashPhoneNumberUtils$SipHash_2_4;->bytesLEtoLong([BI)J

    move-result-wide v2

    xor-long/2addr v0, v2

    sput-wide v0, Lcom/android/providers/contacts/util/HashPhoneNumberUtils$SipHash_2_4;->v3_init:J

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 78
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private static bytesLEtoLong([BI)J
    .locals 9
    .param p0, "b"    # [B
    .param p1, "offset"    # I

    .prologue
    const/16 v8, 0x8

    .line 244
    array-length v1, p0

    sub-int/2addr v1, p1

    if-ge v1, v8, :cond_0

    .line 245
    new-instance v1, Ljava/lang/IllegalArgumentException;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Less then 8 bytes starting from offset:"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-direct {v1, v4}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 248
    :cond_0
    const-wide/16 v2, 0x0

    .line 249
    .local v2, "m":J
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    if-ge v0, v8, :cond_1

    .line 250
    add-int v1, v0, p1

    aget-byte v1, p0, v1

    int-to-long v4, v1

    const-wide/16 v6, 0xff

    and-long/2addr v4, v6

    mul-int/lit8 v1, v0, 0x8

    shl-long/2addr v4, v1

    or-long/2addr v2, v4

    .line 249
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 252
    :cond_1
    return-wide v2
.end method

.method private static finish()J
    .locals 6

    .prologue
    .line 162
    sget-byte v0, Lcom/android/providers/contacts/util/HashPhoneNumberUtils$SipHash_2_4;->msg_byte_counter:B

    .line 165
    .local v0, "msgLenMod256":B
    :goto_0
    sget v1, Lcom/android/providers/contacts/util/HashPhoneNumberUtils$SipHash_2_4;->m_idx:I

    sget v2, Lcom/android/providers/contacts/util/HashPhoneNumberUtils$SipHash_2_4;->LONG_BYTES:I

    add-int/lit8 v2, v2, -0x1

    if-ge v1, v2, :cond_0

    .line 166
    const/4 v1, 0x0

    invoke-static {v1}, Lcom/android/providers/contacts/util/HashPhoneNumberUtils$SipHash_2_4;->updateHash(B)V

    goto :goto_0

    .line 170
    :cond_0
    invoke-static {v0}, Lcom/android/providers/contacts/util/HashPhoneNumberUtils$SipHash_2_4;->updateHash(B)V

    .line 173
    sget-wide v2, Lcom/android/providers/contacts/util/HashPhoneNumberUtils$SipHash_2_4;->v2:J

    const-wide/16 v4, 0xff

    xor-long/2addr v2, v4

    sput-wide v2, Lcom/android/providers/contacts/util/HashPhoneNumberUtils$SipHash_2_4;->v2:J

    .line 174
    invoke-static {}, Lcom/android/providers/contacts/util/HashPhoneNumberUtils$SipHash_2_4;->siphash_round()V

    .line 175
    invoke-static {}, Lcom/android/providers/contacts/util/HashPhoneNumberUtils$SipHash_2_4;->siphash_round()V

    .line 176
    invoke-static {}, Lcom/android/providers/contacts/util/HashPhoneNumberUtils$SipHash_2_4;->siphash_round()V

    .line 177
    invoke-static {}, Lcom/android/providers/contacts/util/HashPhoneNumberUtils$SipHash_2_4;->siphash_round()V

    .line 180
    sget-wide v2, Lcom/android/providers/contacts/util/HashPhoneNumberUtils$SipHash_2_4;->v0:J

    sget-wide v4, Lcom/android/providers/contacts/util/HashPhoneNumberUtils$SipHash_2_4;->v1:J

    xor-long/2addr v2, v4

    sget-wide v4, Lcom/android/providers/contacts/util/HashPhoneNumberUtils$SipHash_2_4;->v2:J

    xor-long/2addr v2, v4

    sget-wide v4, Lcom/android/providers/contacts/util/HashPhoneNumberUtils$SipHash_2_4;->v3:J

    xor-long/2addr v2, v4

    sput-wide v2, Lcom/android/providers/contacts/util/HashPhoneNumberUtils$SipHash_2_4;->v0:J

    .line 181
    sget-wide v2, Lcom/android/providers/contacts/util/HashPhoneNumberUtils$SipHash_2_4;->v0:J

    return-wide v2
.end method

.method public static getSipHash(Ljava/lang/String;)Ljava/lang/String;
    .locals 2
    .param p0, "value"    # Ljava/lang/String;

    .prologue
    .line 264
    invoke-virtual {p0}, Ljava/lang/String;->getBytes()[B

    move-result-object v0

    invoke-static {v0}, Lcom/android/providers/contacts/util/HashPhoneNumberUtils$SipHash_2_4;->hash([B)J

    move-result-wide v0

    invoke-static {v0, v1}, Lcom/android/providers/contacts/util/HashPhoneNumberUtils$SipHash_2_4;->longToString(J)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private static hash([B)J
    .locals 4
    .param p0, "data"    # [B

    .prologue
    .line 192
    invoke-static {}, Lcom/android/providers/contacts/util/HashPhoneNumberUtils$SipHash_2_4;->initialize()V

    .line 193
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    array-length v1, p0

    if-ge v0, v1, :cond_0

    .line 194
    aget-byte v1, p0, v0

    invoke-static {v1}, Lcom/android/providers/contacts/util/HashPhoneNumberUtils$SipHash_2_4;->updateHash(B)V

    .line 193
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 196
    :cond_0
    invoke-static {}, Lcom/android/providers/contacts/util/HashPhoneNumberUtils$SipHash_2_4;->finish()J

    move-result-wide v2

    return-wide v2
.end method

.method private static initialize()V
    .locals 2

    .prologue
    .line 116
    sget-wide v0, Lcom/android/providers/contacts/util/HashPhoneNumberUtils$SipHash_2_4;->v0_init:J

    sput-wide v0, Lcom/android/providers/contacts/util/HashPhoneNumberUtils$SipHash_2_4;->v0:J

    .line 117
    sget-wide v0, Lcom/android/providers/contacts/util/HashPhoneNumberUtils$SipHash_2_4;->v1_init:J

    sput-wide v0, Lcom/android/providers/contacts/util/HashPhoneNumberUtils$SipHash_2_4;->v1:J

    .line 118
    sget-wide v0, Lcom/android/providers/contacts/util/HashPhoneNumberUtils$SipHash_2_4;->v2_init:J

    sput-wide v0, Lcom/android/providers/contacts/util/HashPhoneNumberUtils$SipHash_2_4;->v2:J

    .line 119
    sget-wide v0, Lcom/android/providers/contacts/util/HashPhoneNumberUtils$SipHash_2_4;->v3_init:J

    sput-wide v0, Lcom/android/providers/contacts/util/HashPhoneNumberUtils$SipHash_2_4;->v3:J

    .line 121
    const/4 v0, 0x0

    sput-byte v0, Lcom/android/providers/contacts/util/HashPhoneNumberUtils$SipHash_2_4;->msg_byte_counter:B

    .line 122
    return-void
.end method

.method private static longToString(J)Ljava/lang/String;
    .locals 10
    .param p0, "m"    # J

    .prologue
    .line 256
    new-instance v1, Ljava/lang/StringBuilder;

    const/16 v2, 0x12

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(I)V

    .line 257
    .local v1, "sb":Ljava/lang/StringBuilder;
    const/4 v0, 0x7

    .local v0, "i":I
    :goto_0
    if-ltz v0, :cond_0

    .line 258
    const-string v2, "%02x"

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    mul-int/lit8 v5, v0, 0x8

    ushr-long v6, p0, v5

    const-wide/16 v8, 0xff

    and-long/2addr v6, v8

    long-to-int v5, v6

    int-to-byte v5, v5

    invoke-static {v5}, Ljava/lang/Byte;->valueOf(B)Ljava/lang/Byte;

    move-result-object v5

    aput-object v5, v3, v4

    invoke-static {v2, v3}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 257
    add-int/lit8 v0, v0, -0x1

    goto :goto_0

    .line 260
    :cond_0
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    sget-object v3, Ljava/util/Locale;->ENGLISH:Ljava/util/Locale;

    invoke-virtual {v2, v3}, Ljava/lang/String;->toLowerCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v2

    return-object v2
.end method

.method private static rotateLeft(JI)J
    .locals 4
    .param p0, "l"    # J
    .param p2, "shift"    # I

    .prologue
    .line 208
    shl-long v0, p0, p2

    rsub-int/lit8 v2, p2, 0x40

    ushr-long v2, p0, v2

    or-long/2addr v0, v2

    return-wide v0
.end method

.method private static siphash_round()V
    .locals 5

    .prologue
    const/16 v4, 0x20

    .line 215
    sget-wide v0, Lcom/android/providers/contacts/util/HashPhoneNumberUtils$SipHash_2_4;->v0:J

    sget-wide v2, Lcom/android/providers/contacts/util/HashPhoneNumberUtils$SipHash_2_4;->v1:J

    add-long/2addr v0, v2

    sput-wide v0, Lcom/android/providers/contacts/util/HashPhoneNumberUtils$SipHash_2_4;->v0:J

    .line 216
    sget-wide v0, Lcom/android/providers/contacts/util/HashPhoneNumberUtils$SipHash_2_4;->v2:J

    sget-wide v2, Lcom/android/providers/contacts/util/HashPhoneNumberUtils$SipHash_2_4;->v3:J

    add-long/2addr v0, v2

    sput-wide v0, Lcom/android/providers/contacts/util/HashPhoneNumberUtils$SipHash_2_4;->v2:J

    .line 217
    sget-wide v0, Lcom/android/providers/contacts/util/HashPhoneNumberUtils$SipHash_2_4;->v1:J

    const/16 v2, 0xd

    invoke-static {v0, v1, v2}, Lcom/android/providers/contacts/util/HashPhoneNumberUtils$SipHash_2_4;->rotateLeft(JI)J

    move-result-wide v0

    sput-wide v0, Lcom/android/providers/contacts/util/HashPhoneNumberUtils$SipHash_2_4;->v1:J

    .line 218
    sget-wide v0, Lcom/android/providers/contacts/util/HashPhoneNumberUtils$SipHash_2_4;->v3:J

    const/16 v2, 0x10

    invoke-static {v0, v1, v2}, Lcom/android/providers/contacts/util/HashPhoneNumberUtils$SipHash_2_4;->rotateLeft(JI)J

    move-result-wide v0

    sput-wide v0, Lcom/android/providers/contacts/util/HashPhoneNumberUtils$SipHash_2_4;->v3:J

    .line 220
    sget-wide v0, Lcom/android/providers/contacts/util/HashPhoneNumberUtils$SipHash_2_4;->v1:J

    sget-wide v2, Lcom/android/providers/contacts/util/HashPhoneNumberUtils$SipHash_2_4;->v0:J

    xor-long/2addr v0, v2

    sput-wide v0, Lcom/android/providers/contacts/util/HashPhoneNumberUtils$SipHash_2_4;->v1:J

    .line 221
    sget-wide v0, Lcom/android/providers/contacts/util/HashPhoneNumberUtils$SipHash_2_4;->v3:J

    sget-wide v2, Lcom/android/providers/contacts/util/HashPhoneNumberUtils$SipHash_2_4;->v2:J

    xor-long/2addr v0, v2

    sput-wide v0, Lcom/android/providers/contacts/util/HashPhoneNumberUtils$SipHash_2_4;->v3:J

    .line 222
    sget-wide v0, Lcom/android/providers/contacts/util/HashPhoneNumberUtils$SipHash_2_4;->v0:J

    invoke-static {v0, v1, v4}, Lcom/android/providers/contacts/util/HashPhoneNumberUtils$SipHash_2_4;->rotateLeft(JI)J

    move-result-wide v0

    sput-wide v0, Lcom/android/providers/contacts/util/HashPhoneNumberUtils$SipHash_2_4;->v0:J

    .line 224
    sget-wide v0, Lcom/android/providers/contacts/util/HashPhoneNumberUtils$SipHash_2_4;->v2:J

    sget-wide v2, Lcom/android/providers/contacts/util/HashPhoneNumberUtils$SipHash_2_4;->v1:J

    add-long/2addr v0, v2

    sput-wide v0, Lcom/android/providers/contacts/util/HashPhoneNumberUtils$SipHash_2_4;->v2:J

    .line 225
    sget-wide v0, Lcom/android/providers/contacts/util/HashPhoneNumberUtils$SipHash_2_4;->v0:J

    sget-wide v2, Lcom/android/providers/contacts/util/HashPhoneNumberUtils$SipHash_2_4;->v3:J

    add-long/2addr v0, v2

    sput-wide v0, Lcom/android/providers/contacts/util/HashPhoneNumberUtils$SipHash_2_4;->v0:J

    .line 226
    sget-wide v0, Lcom/android/providers/contacts/util/HashPhoneNumberUtils$SipHash_2_4;->v1:J

    const/16 v2, 0x11

    invoke-static {v0, v1, v2}, Lcom/android/providers/contacts/util/HashPhoneNumberUtils$SipHash_2_4;->rotateLeft(JI)J

    move-result-wide v0

    sput-wide v0, Lcom/android/providers/contacts/util/HashPhoneNumberUtils$SipHash_2_4;->v1:J

    .line 227
    sget-wide v0, Lcom/android/providers/contacts/util/HashPhoneNumberUtils$SipHash_2_4;->v3:J

    const/16 v2, 0x15

    invoke-static {v0, v1, v2}, Lcom/android/providers/contacts/util/HashPhoneNumberUtils$SipHash_2_4;->rotateLeft(JI)J

    move-result-wide v0

    sput-wide v0, Lcom/android/providers/contacts/util/HashPhoneNumberUtils$SipHash_2_4;->v3:J

    .line 229
    sget-wide v0, Lcom/android/providers/contacts/util/HashPhoneNumberUtils$SipHash_2_4;->v1:J

    sget-wide v2, Lcom/android/providers/contacts/util/HashPhoneNumberUtils$SipHash_2_4;->v2:J

    xor-long/2addr v0, v2

    sput-wide v0, Lcom/android/providers/contacts/util/HashPhoneNumberUtils$SipHash_2_4;->v1:J

    .line 230
    sget-wide v0, Lcom/android/providers/contacts/util/HashPhoneNumberUtils$SipHash_2_4;->v3:J

    sget-wide v2, Lcom/android/providers/contacts/util/HashPhoneNumberUtils$SipHash_2_4;->v0:J

    xor-long/2addr v0, v2

    sput-wide v0, Lcom/android/providers/contacts/util/HashPhoneNumberUtils$SipHash_2_4;->v3:J

    .line 231
    sget-wide v0, Lcom/android/providers/contacts/util/HashPhoneNumberUtils$SipHash_2_4;->v2:J

    invoke-static {v0, v1, v4}, Lcom/android/providers/contacts/util/HashPhoneNumberUtils$SipHash_2_4;->rotateLeft(JI)J

    move-result-wide v0

    sput-wide v0, Lcom/android/providers/contacts/util/HashPhoneNumberUtils$SipHash_2_4;->v2:J

    .line 232
    return-void
.end method

.method private static updateHash(B)V
    .locals 6
    .param p0, "b"    # B

    .prologue
    .line 134
    sget-byte v0, Lcom/android/providers/contacts/util/HashPhoneNumberUtils$SipHash_2_4;->msg_byte_counter:B

    add-int/lit8 v0, v0, 0x1

    int-to-byte v0, v0

    sput-byte v0, Lcom/android/providers/contacts/util/HashPhoneNumberUtils$SipHash_2_4;->msg_byte_counter:B

    .line 135
    sget-wide v0, Lcom/android/providers/contacts/util/HashPhoneNumberUtils$SipHash_2_4;->m:J

    int-to-long v2, p0

    const-wide/16 v4, 0xff

    and-long/2addr v2, v4

    sget v4, Lcom/android/providers/contacts/util/HashPhoneNumberUtils$SipHash_2_4;->m_idx:I

    mul-int/lit8 v4, v4, 0x8

    shl-long/2addr v2, v4

    or-long/2addr v0, v2

    sput-wide v0, Lcom/android/providers/contacts/util/HashPhoneNumberUtils$SipHash_2_4;->m:J

    .line 136
    sget v0, Lcom/android/providers/contacts/util/HashPhoneNumberUtils$SipHash_2_4;->m_idx:I

    add-int/lit8 v0, v0, 0x1

    sput v0, Lcom/android/providers/contacts/util/HashPhoneNumberUtils$SipHash_2_4;->m_idx:I

    .line 137
    sget v0, Lcom/android/providers/contacts/util/HashPhoneNumberUtils$SipHash_2_4;->m_idx:I

    sget v1, Lcom/android/providers/contacts/util/HashPhoneNumberUtils$SipHash_2_4;->LONG_BYTES:I

    if-lt v0, v1, :cond_0

    .line 139
    sget-wide v0, Lcom/android/providers/contacts/util/HashPhoneNumberUtils$SipHash_2_4;->v3:J

    sget-wide v2, Lcom/android/providers/contacts/util/HashPhoneNumberUtils$SipHash_2_4;->m:J

    xor-long/2addr v0, v2

    sput-wide v0, Lcom/android/providers/contacts/util/HashPhoneNumberUtils$SipHash_2_4;->v3:J

    .line 140
    invoke-static {}, Lcom/android/providers/contacts/util/HashPhoneNumberUtils$SipHash_2_4;->siphash_round()V

    .line 141
    invoke-static {}, Lcom/android/providers/contacts/util/HashPhoneNumberUtils$SipHash_2_4;->siphash_round()V

    .line 142
    sget-wide v0, Lcom/android/providers/contacts/util/HashPhoneNumberUtils$SipHash_2_4;->v0:J

    sget-wide v2, Lcom/android/providers/contacts/util/HashPhoneNumberUtils$SipHash_2_4;->m:J

    xor-long/2addr v0, v2

    sput-wide v0, Lcom/android/providers/contacts/util/HashPhoneNumberUtils$SipHash_2_4;->v0:J

    .line 144
    const/4 v0, 0x0

    sput v0, Lcom/android/providers/contacts/util/HashPhoneNumberUtils$SipHash_2_4;->m_idx:I

    .line 145
    const-wide/16 v0, 0x0

    sput-wide v0, Lcom/android/providers/contacts/util/HashPhoneNumberUtils$SipHash_2_4;->m:J

    .line 147
    :cond_0
    return-void
.end method
