.class interface abstract Lcom/android/providers/contacts/ContactsProvider2$ContactsLocationInfoQuery;
.super Ljava/lang/Object;
.source "ContactsProvider2.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/providers/contacts/ContactsProvider2;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x60a
    name = "ContactsLocationInfoQuery"
.end annotation


# static fields
.field public static final COLUMNS:[Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    .line 6110
    const/4 v0, 0x7

    new-array v0, v0, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string v2, "view_raw_contacts.contact_id as contact_id"

    aput-object v2, v0, v1

    const/4 v1, 0x1

    const-string v2, "location.raw_contact_id as raw_contact_id"

    aput-object v2, v0, v1

    const/4 v1, 0x2

    const-string v2, "location.data_id as data_id"

    aput-object v2, v0, v1

    const/4 v1, 0x3

    const-string v2, "view_contacts.display_name as name"

    aput-object v2, v0, v1

    const/4 v1, 0x4

    const-string v2, "location.number as number"

    aput-object v2, v0, v1

    const/4 v1, 0x5

    const-string v2, "location.pnl as location"

    aput-object v2, v0, v1

    const/4 v1, 0x6

    const-string v2, "view_contacts.sort_key as sort_key"

    aput-object v2, v0, v1

    sput-object v0, Lcom/android/providers/contacts/ContactsProvider2$ContactsLocationInfoQuery;->COLUMNS:[Ljava/lang/String;

    return-void
.end method
