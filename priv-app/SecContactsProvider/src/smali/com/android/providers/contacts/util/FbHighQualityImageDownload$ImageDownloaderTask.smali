.class Lcom/android/providers/contacts/util/FbHighQualityImageDownload$ImageDownloaderTask;
.super Landroid/os/AsyncTask;
.source "FbHighQualityImageDownload.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/providers/contacts/util/FbHighQualityImageDownload;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "ImageDownloaderTask"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/os/AsyncTask",
        "<",
        "Ljava/lang/String;",
        "Ljava/lang/Integer;",
        "Ljava/lang/String;",
        ">;"
    }
.end annotation


# instance fields
.field private final mRawContactId:J

.field private final mSourceId:Ljava/lang/String;

.field final synthetic this$0:Lcom/android/providers/contacts/util/FbHighQualityImageDownload;


# direct methods
.method private constructor <init>(Lcom/android/providers/contacts/util/FbHighQualityImageDownload;Ljava/lang/String;Ljava/lang/String;)V
    .locals 2
    .param p2, "rawContactId"    # Ljava/lang/String;
    .param p3, "sourceId"    # Ljava/lang/String;

    .prologue
    .line 439
    iput-object p1, p0, Lcom/android/providers/contacts/util/FbHighQualityImageDownload$ImageDownloaderTask;->this$0:Lcom/android/providers/contacts/util/FbHighQualityImageDownload;

    invoke-direct {p0}, Landroid/os/AsyncTask;-><init>()V

    .line 440
    invoke-static {p2}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v0

    iput-wide v0, p0, Lcom/android/providers/contacts/util/FbHighQualityImageDownload$ImageDownloaderTask;->mRawContactId:J

    .line 441
    iput-object p3, p0, Lcom/android/providers/contacts/util/FbHighQualityImageDownload$ImageDownloaderTask;->mSourceId:Ljava/lang/String;

    .line 442
    return-void
.end method

.method synthetic constructor <init>(Lcom/android/providers/contacts/util/FbHighQualityImageDownload;Ljava/lang/String;Ljava/lang/String;Lcom/android/providers/contacts/util/FbHighQualityImageDownload$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/android/providers/contacts/util/FbHighQualityImageDownload;
    .param p2, "x1"    # Ljava/lang/String;
    .param p3, "x2"    # Ljava/lang/String;
    .param p4, "x3"    # Lcom/android/providers/contacts/util/FbHighQualityImageDownload$1;

    .prologue
    .line 434
    invoke-direct {p0, p1, p2, p3}, Lcom/android/providers/contacts/util/FbHighQualityImageDownload$ImageDownloaderTask;-><init>(Lcom/android/providers/contacts/util/FbHighQualityImageDownload;Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method


# virtual methods
.method protected bridge synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1
    .param p1, "x0"    # [Ljava/lang/Object;

    .prologue
    .line 434
    check-cast p1, [Ljava/lang/String;

    .end local p1    # "x0":[Ljava/lang/Object;
    invoke-virtual {p0, p1}, Lcom/android/providers/contacts/util/FbHighQualityImageDownload$ImageDownloaderTask;->doInBackground([Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method protected varargs doInBackground([Ljava/lang/String;)Ljava/lang/String;
    .locals 4
    .param p1, "params"    # [Ljava/lang/String;

    .prologue
    .line 446
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "https://graph.facebook.com/"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/android/providers/contacts/util/FbHighQualityImageDownload$ImageDownloaderTask;->mSourceId:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "/picture?width=240&height=240"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 447
    .local v0, "fbURl":Ljava/lang/String;
    iget-object v1, p0, Lcom/android/providers/contacts/util/FbHighQualityImageDownload$ImageDownloaderTask;->this$0:Lcom/android/providers/contacts/util/FbHighQualityImageDownload;

    # getter for: Lcom/android/providers/contacts/util/FbHighQualityImageDownload;->mFbImageDownloader:Lcom/android/providers/contacts/util/FbHighQualityImageDownload$FbImageDownloader;
    invoke-static {v1}, Lcom/android/providers/contacts/util/FbHighQualityImageDownload;->access$900(Lcom/android/providers/contacts/util/FbHighQualityImageDownload;)Lcom/android/providers/contacts/util/FbHighQualityImageDownload$FbImageDownloader;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 448
    iget-object v1, p0, Lcom/android/providers/contacts/util/FbHighQualityImageDownload$ImageDownloaderTask;->this$0:Lcom/android/providers/contacts/util/FbHighQualityImageDownload;

    # getter for: Lcom/android/providers/contacts/util/FbHighQualityImageDownload;->mFbImageDownloader:Lcom/android/providers/contacts/util/FbHighQualityImageDownload$FbImageDownloader;
    invoke-static {v1}, Lcom/android/providers/contacts/util/FbHighQualityImageDownload;->access$900(Lcom/android/providers/contacts/util/FbHighQualityImageDownload;)Lcom/android/providers/contacts/util/FbHighQualityImageDownload$FbImageDownloader;

    move-result-object v1

    iget-wide v2, p0, Lcom/android/providers/contacts/util/FbHighQualityImageDownload$ImageDownloaderTask;->mRawContactId:J

    invoke-virtual {v1, v0, v2, v3}, Lcom/android/providers/contacts/util/FbHighQualityImageDownload$FbImageDownloader;->getBitmapFromURL(Ljava/lang/String;J)V

    .line 450
    :cond_0
    const-string v1, "success"

    return-object v1
.end method
