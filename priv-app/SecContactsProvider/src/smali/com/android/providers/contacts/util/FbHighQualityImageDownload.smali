.class public Lcom/android/providers/contacts/util/FbHighQualityImageDownload;
.super Ljava/lang/Object;
.source "FbHighQualityImageDownload.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/android/providers/contacts/util/FbHighQualityImageDownload$QueueItem;,
        Lcom/android/providers/contacts/util/FbHighQualityImageDownload$FacebookIdQueue;,
        Lcom/android/providers/contacts/util/FbHighQualityImageDownload$FbImageDownloader;,
        Lcom/android/providers/contacts/util/FbHighQualityImageDownload$ImageDownloaderTask;
    }
.end annotation


# instance fields
.field private mContext:Landroid/content/Context;

.field private mCountOfFailByNotSynced:I

.field private mCountOfTryFbAccountSync:I

.field mFBImageDownloadHandler:Landroid/os/Handler;

.field private mFbImageDownloader:Lcom/android/providers/contacts/util/FbHighQualityImageDownload$FbImageDownloader;

.field mIsFbHighQualitySyncing:Z

.field private mMaxCountOfFailByNotSynced:I

.field private mQueue:Lcom/android/providers/contacts/util/FbHighQualityImageDownload$FacebookIdQueue;

.field private mSizeOfQueueToCheckNotSynced:I


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 3
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    const/4 v2, 0x0

    .line 159
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 79
    new-instance v0, Lcom/android/providers/contacts/util/FbHighQualityImageDownload$FacebookIdQueue;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/android/providers/contacts/util/FbHighQualityImageDownload$FacebookIdQueue;-><init>(Lcom/android/providers/contacts/util/FbHighQualityImageDownload$1;)V

    iput-object v0, p0, Lcom/android/providers/contacts/util/FbHighQualityImageDownload;->mQueue:Lcom/android/providers/contacts/util/FbHighQualityImageDownload$FacebookIdQueue;

    .line 81
    iput-boolean v2, p0, Lcom/android/providers/contacts/util/FbHighQualityImageDownload;->mIsFbHighQualitySyncing:Z

    .line 87
    iput v2, p0, Lcom/android/providers/contacts/util/FbHighQualityImageDownload;->mCountOfFailByNotSynced:I

    .line 91
    iput v2, p0, Lcom/android/providers/contacts/util/FbHighQualityImageDownload;->mCountOfTryFbAccountSync:I

    .line 104
    new-instance v0, Lcom/android/providers/contacts/util/FbHighQualityImageDownload$1;

    invoke-direct {v0, p0}, Lcom/android/providers/contacts/util/FbHighQualityImageDownload$1;-><init>(Lcom/android/providers/contacts/util/FbHighQualityImageDownload;)V

    iput-object v0, p0, Lcom/android/providers/contacts/util/FbHighQualityImageDownload;->mFBImageDownloadHandler:Landroid/os/Handler;

    .line 160
    iput-object p1, p0, Lcom/android/providers/contacts/util/FbHighQualityImageDownload;->mContext:Landroid/content/Context;

    .line 162
    new-instance v0, Lcom/android/providers/contacts/util/FbHighQualityImageDownload$FbImageDownloader;

    invoke-direct {v0, p0}, Lcom/android/providers/contacts/util/FbHighQualityImageDownload$FbImageDownloader;-><init>(Lcom/android/providers/contacts/util/FbHighQualityImageDownload;)V

    iput-object v0, p0, Lcom/android/providers/contacts/util/FbHighQualityImageDownload;->mFbImageDownloader:Lcom/android/providers/contacts/util/FbHighQualityImageDownload$FbImageDownloader;

    .line 163
    return-void
.end method

.method static synthetic access$100(Lcom/android/providers/contacts/util/FbHighQualityImageDownload;)Lcom/android/providers/contacts/util/FbHighQualityImageDownload$FacebookIdQueue;
    .locals 1
    .param p0, "x0"    # Lcom/android/providers/contacts/util/FbHighQualityImageDownload;

    .prologue
    .line 59
    iget-object v0, p0, Lcom/android/providers/contacts/util/FbHighQualityImageDownload;->mQueue:Lcom/android/providers/contacts/util/FbHighQualityImageDownload$FacebookIdQueue;

    return-object v0
.end method

.method static synthetic access$1000(Lcom/android/providers/contacts/util/FbHighQualityImageDownload;IJ)V
    .locals 0
    .param p0, "x0"    # Lcom/android/providers/contacts/util/FbHighQualityImageDownload;
    .param p1, "x1"    # I
    .param p2, "x2"    # J

    .prologue
    .line 59
    invoke-direct {p0, p1, p2, p3}, Lcom/android/providers/contacts/util/FbHighQualityImageDownload;->sendMessageToFbImageDownloadHandler(IJ)V

    return-void
.end method

.method static synthetic access$400(Lcom/android/providers/contacts/util/FbHighQualityImageDownload;Ljava/lang/String;Ljava/lang/String;)V
    .locals 0
    .param p0, "x0"    # Lcom/android/providers/contacts/util/FbHighQualityImageDownload;
    .param p1, "x1"    # Ljava/lang/String;
    .param p2, "x2"    # Ljava/lang/String;

    .prologue
    .line 59
    invoke-direct {p0, p1, p2}, Lcom/android/providers/contacts/util/FbHighQualityImageDownload;->fetchFbImageFromId(Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method

.method static synthetic access$502(Lcom/android/providers/contacts/util/FbHighQualityImageDownload;I)I
    .locals 0
    .param p0, "x0"    # Lcom/android/providers/contacts/util/FbHighQualityImageDownload;
    .param p1, "x1"    # I

    .prologue
    .line 59
    iput p1, p0, Lcom/android/providers/contacts/util/FbHighQualityImageDownload;->mCountOfTryFbAccountSync:I

    return p1
.end method

.method static synthetic access$900(Lcom/android/providers/contacts/util/FbHighQualityImageDownload;)Lcom/android/providers/contacts/util/FbHighQualityImageDownload$FbImageDownloader;
    .locals 1
    .param p0, "x0"    # Lcom/android/providers/contacts/util/FbHighQualityImageDownload;

    .prologue
    .line 59
    iget-object v0, p0, Lcom/android/providers/contacts/util/FbHighQualityImageDownload;->mFbImageDownloader:Lcom/android/providers/contacts/util/FbHighQualityImageDownload$FbImageDownloader;

    return-object v0
.end method

.method private fetchFbImageFromId(Ljava/lang/String;Ljava/lang/String;)V
    .locals 20
    .param p1, "rawContactId"    # Ljava/lang/String;
    .param p2, "fbId"    # Ljava/lang/String;

    .prologue
    .line 354
    invoke-virtual/range {p0 .. p0}, Lcom/android/providers/contacts/util/FbHighQualityImageDownload;->getDatabaseHelper()Lcom/android/providers/contacts/ContactsDatabaseHelper;

    move-result-object v5

    invoke-virtual {v5}, Lcom/android/providers/contacts/ContactsDatabaseHelper;->getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v4

    .line 356
    .local v4, "db":Landroid/database/sqlite/SQLiteDatabase;
    const/16 v19, -0x1

    .line 357
    .local v19, "photoRow":I
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "raw_contact_id = "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    move-object/from16 v0, p1

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, " AND "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "mimetype"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "==\'"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "vnd.android.cursor.item/photo"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "\'"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    .line 359
    .local v7, "where":Ljava/lang/String;
    const-string v5, "view_data"

    const/4 v6, 0x0

    const/4 v8, 0x0

    const/4 v9, 0x0

    const/4 v10, 0x0

    const/4 v11, 0x0

    invoke-virtual/range {v4 .. v11}, Landroid/database/sqlite/SQLiteDatabase;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v12

    .line 361
    .local v12, "cursor":Landroid/database/Cursor;
    :try_start_0
    const-string v5, "_id"

    invoke-interface {v12, v5}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v17

    .line 362
    .local v17, "idIdx":I
    invoke-interface {v12}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v5

    if-eqz v5, :cond_0

    .line 363
    move/from16 v0, v17

    invoke-interface {v12, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v19

    .line 365
    :cond_0
    if-ltz v19, :cond_6

    .line 372
    const/4 v5, 0x0

    move-object/from16 v0, p0

    iput v5, v0, Lcom/android/providers/contacts/util/FbHighQualityImageDownload;->mSizeOfQueueToCheckNotSynced:I

    .line 373
    const/4 v5, 0x0

    move-object/from16 v0, p0

    iput v5, v0, Lcom/android/providers/contacts/util/FbHighQualityImageDownload;->mCountOfFailByNotSynced:I

    .line 375
    const-string v5, "data_version"

    invoke-interface {v12, v5}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v16

    .line 376
    .local v16, "dataVersionIndex":I
    const-string v5, "data13"

    invoke-interface {v12, v5}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v14

    .line 377
    .local v14, "data13Index":I
    const/4 v15, -0x1

    .line 378
    .local v15, "dataVersion":I
    const/4 v13, -0x1

    .line 379
    .local v13, "data13":I
    invoke-interface {v12}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v5

    if-eqz v5, :cond_1

    .line 380
    move/from16 v0, v16

    invoke-interface {v12, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v15

    .line 381
    invoke-interface {v12, v14}, Landroid/database/Cursor;->getInt(I)I

    move-result v13

    .line 384
    :cond_1
    const-string v5, "FB_LOGS"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "In fetchFbImageFromId dataVersion = "

    invoke-virtual {v6, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v15}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v8, " data13 = "

    invoke-virtual {v6, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v13}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 386
    if-eqz v15, :cond_2

    if-eq v13, v15, :cond_4

    .line 387
    :cond_2
    const-string v5, "FB_LOGS"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "In fetchFbImageFromId Profile picture updated for RawConatct ID = "

    invoke-virtual {v6, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    move-object/from16 v0, p1

    invoke-virtual {v6, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 390
    new-instance v18, Lcom/android/providers/contacts/util/FbHighQualityImageDownload$ImageDownloaderTask;

    const/4 v5, 0x0

    move-object/from16 v0, v18

    move-object/from16 v1, p0

    move-object/from16 v2, p1

    move-object/from16 v3, p2

    invoke-direct {v0, v1, v2, v3, v5}, Lcom/android/providers/contacts/util/FbHighQualityImageDownload$ImageDownloaderTask;-><init>(Lcom/android/providers/contacts/util/FbHighQualityImageDownload;Ljava/lang/String;Ljava/lang/String;Lcom/android/providers/contacts/util/FbHighQualityImageDownload$1;)V

    .line 392
    .local v18, "imageDownloader":Lcom/android/providers/contacts/util/FbHighQualityImageDownload$ImageDownloaderTask;
    const/4 v5, 0x0

    new-array v5, v5, [Ljava/lang/String;

    move-object/from16 v0, v18

    invoke-virtual {v0, v5}, Lcom/android/providers/contacts/util/FbHighQualityImageDownload$ImageDownloaderTask;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 426
    .end local v13    # "data13":I
    .end local v14    # "data13Index":I
    .end local v15    # "dataVersion":I
    .end local v16    # "dataVersionIndex":I
    .end local v18    # "imageDownloader":Lcom/android/providers/contacts/util/FbHighQualityImageDownload$ImageDownloaderTask;
    :goto_0
    if-eqz v12, :cond_3

    .line 427
    invoke-interface {v12}, Landroid/database/Cursor;->close()V

    .line 429
    :cond_3
    return-void

    .line 394
    .restart local v13    # "data13":I
    .restart local v14    # "data13Index":I
    .restart local v15    # "dataVersion":I
    .restart local v16    # "dataVersionIndex":I
    :cond_4
    :try_start_1
    const-string v5, "FB_LOGS"

    const-string v6, "In fetchFbImageFromId Don\'t update as profile picture is not changed"

    invoke-static {v5, v6}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 396
    const/4 v5, 0x1

    move-object/from16 v0, p0

    invoke-direct {v0, v5}, Lcom/android/providers/contacts/util/FbHighQualityImageDownload;->sendMessageToFbImageDownloadHandler(I)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 426
    .end local v13    # "data13":I
    .end local v14    # "data13Index":I
    .end local v15    # "dataVersion":I
    .end local v16    # "dataVersionIndex":I
    .end local v17    # "idIdx":I
    :catchall_0
    move-exception v5

    if-eqz v12, :cond_5

    .line 427
    invoke-interface {v12}, Landroid/database/Cursor;->close()V

    :cond_5
    throw v5

    .line 399
    .restart local v17    # "idIdx":I
    :cond_6
    :try_start_2
    const-string v5, "FB_LOGS"

    const-string v6, "In fetchFbImageFromId Photo field is not synced in contact Db yet Put the same id back to queue"

    invoke-static {v5, v6}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 402
    move-object/from16 v0, p0

    iget v5, v0, Lcom/android/providers/contacts/util/FbHighQualityImageDownload;->mCountOfFailByNotSynced:I

    if-nez v5, :cond_7

    .line 403
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/android/providers/contacts/util/FbHighQualityImageDownload;->mQueue:Lcom/android/providers/contacts/util/FbHighQualityImageDownload$FacebookIdQueue;

    invoke-virtual {v5}, Lcom/android/providers/contacts/util/FbHighQualityImageDownload$FacebookIdQueue;->getCount()I

    move-result v5

    move-object/from16 v0, p0

    iput v5, v0, Lcom/android/providers/contacts/util/FbHighQualityImageDownload;->mSizeOfQueueToCheckNotSynced:I

    .line 405
    :cond_7
    move-object/from16 v0, p0

    iget v5, v0, Lcom/android/providers/contacts/util/FbHighQualityImageDownload;->mSizeOfQueueToCheckNotSynced:I

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/android/providers/contacts/util/FbHighQualityImageDownload;->mQueue:Lcom/android/providers/contacts/util/FbHighQualityImageDownload$FacebookIdQueue;

    invoke-virtual {v6}, Lcom/android/providers/contacts/util/FbHighQualityImageDownload$FacebookIdQueue;->getCount()I

    move-result v6

    if-ne v5, v6, :cond_8

    .line 406
    move-object/from16 v0, p0

    iget v5, v0, Lcom/android/providers/contacts/util/FbHighQualityImageDownload;->mCountOfFailByNotSynced:I

    add-int/lit8 v5, v5, 0x1

    move-object/from16 v0, p0

    iput v5, v0, Lcom/android/providers/contacts/util/FbHighQualityImageDownload;->mCountOfFailByNotSynced:I

    .line 408
    :cond_8
    const-string v5, "FB_LOGS"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "mCountOfSyncFail = "

    invoke-virtual {v6, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    move-object/from16 v0, p0

    iget v8, v0, Lcom/android/providers/contacts/util/FbHighQualityImageDownload;->mCountOfFailByNotSynced:I

    invoke-virtual {v6, v8}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 410
    move-object/from16 v0, p0

    iget v5, v0, Lcom/android/providers/contacts/util/FbHighQualityImageDownload;->mCountOfFailByNotSynced:I

    move-object/from16 v0, p0

    iget v6, v0, Lcom/android/providers/contacts/util/FbHighQualityImageDownload;->mMaxCountOfFailByNotSynced:I

    if-ne v5, v6, :cond_a

    .line 412
    const-string v5, "FB_LOGS"

    const-string v6, "Stopping the FB high sync as FB application has not updated the Photo data in contact database"

    invoke-static {v5, v6}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 414
    if-eqz v12, :cond_9

    .line 415
    invoke-interface {v12}, Landroid/database/Cursor;->close()V

    .line 418
    :cond_9
    invoke-direct/range {p0 .. p0}, Lcom/android/providers/contacts/util/FbHighQualityImageDownload;->stopBySyncFail()V

    .line 421
    :cond_a
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/android/providers/contacts/util/FbHighQualityImageDownload;->mQueue:Lcom/android/providers/contacts/util/FbHighQualityImageDownload$FacebookIdQueue;

    new-instance v6, Lcom/android/providers/contacts/util/FbHighQualityImageDownload$QueueItem;

    move-object/from16 v0, p0

    iget-object v8, v0, Lcom/android/providers/contacts/util/FbHighQualityImageDownload;->mContext:Landroid/content/Context;

    move-object/from16 v0, p1

    move-object/from16 v1, p2

    invoke-direct {v6, v8, v0, v1}, Lcom/android/providers/contacts/util/FbHighQualityImageDownload$QueueItem;-><init>(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)V

    # invokes: Lcom/android/providers/contacts/util/FbHighQualityImageDownload$FacebookIdQueue;->addFacebookIdToQueue(Lcom/android/providers/contacts/util/FbHighQualityImageDownload$QueueItem;)Z
    invoke-static {v5, v6}, Lcom/android/providers/contacts/util/FbHighQualityImageDownload$FacebookIdQueue;->access$700(Lcom/android/providers/contacts/util/FbHighQualityImageDownload$FacebookIdQueue;Lcom/android/providers/contacts/util/FbHighQualityImageDownload$QueueItem;)Z

    .line 423
    const/4 v5, 0x1

    const-wide/16 v8, 0x64

    move-object/from16 v0, p0

    invoke-direct {v0, v5, v8, v9}, Lcom/android/providers/contacts/util/FbHighQualityImageDownload;->sendMessageToFbImageDownloadHandler(IJ)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto/16 :goto_0
.end method

.method private getFbAccount()Landroid/accounts/Account;
    .locals 7

    .prologue
    .line 240
    iget-object v5, p0, Lcom/android/providers/contacts/util/FbHighQualityImageDownload;->mContext:Landroid/content/Context;

    invoke-static {v5}, Landroid/accounts/AccountManager;->get(Landroid/content/Context;)Landroid/accounts/AccountManager;

    move-result-object v5

    invoke-virtual {v5}, Landroid/accounts/AccountManager;->getAccounts()[Landroid/accounts/Account;

    move-result-object v1

    .line 241
    .local v1, "accounts":[Landroid/accounts/Account;
    move-object v2, v1

    .local v2, "arr$":[Landroid/accounts/Account;
    array-length v4, v2

    .local v4, "len$":I
    const/4 v3, 0x0

    .local v3, "i$":I
    :goto_0
    if-ge v3, v4, :cond_1

    aget-object v0, v2, v3

    .line 242
    .local v0, "account":Landroid/accounts/Account;
    const-string v5, "com.facebook.auth.login"

    iget-object v6, v0, Landroid/accounts/Account;->type:Ljava/lang/String;

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_0

    .line 248
    .end local v0    # "account":Landroid/accounts/Account;
    :goto_1
    return-object v0

    .line 241
    .restart local v0    # "account":Landroid/accounts/Account;
    :cond_0
    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    .line 247
    .end local v0    # "account":Landroid/accounts/Account;
    :cond_1
    const-string v5, "FB_LOGS"

    const-string v6, "getFbAccount : can\'t find FB account"

    invoke-static {v5, v6}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 248
    const/4 v0, 0x0

    goto :goto_1
.end method

.method private sendMessageToFbImageDownloadHandler(I)V
    .locals 2
    .param p1, "what"    # I

    .prologue
    .line 266
    const-wide/16 v0, 0x0

    invoke-direct {p0, p1, v0, v1}, Lcom/android/providers/contacts/util/FbHighQualityImageDownload;->sendMessageToFbImageDownloadHandler(IJ)V

    .line 267
    return-void
.end method

.method private sendMessageToFbImageDownloadHandler(IJ)V
    .locals 2
    .param p1, "what"    # I
    .param p2, "delayMillis"    # J

    .prologue
    .line 270
    invoke-static {}, Landroid/os/Message;->obtain()Landroid/os/Message;

    move-result-object v0

    .line 271
    .local v0, "msg":Landroid/os/Message;
    iput p1, v0, Landroid/os/Message;->what:I

    .line 272
    iget-object v1, p0, Lcom/android/providers/contacts/util/FbHighQualityImageDownload;->mFBImageDownloadHandler:Landroid/os/Handler;

    invoke-virtual {v1, v0, p2, p3}, Landroid/os/Handler;->sendMessageDelayed(Landroid/os/Message;J)Z

    .line 273
    return-void
.end method

.method private stopBySyncFail()V
    .locals 3

    .prologue
    .line 194
    const-string v0, "FB_LOGS"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "stopBySyncFail, sync count : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/android/providers/contacts/util/FbHighQualityImageDownload;->mCountOfTryFbAccountSync:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 196
    invoke-virtual {p0}, Lcom/android/providers/contacts/util/FbHighQualityImageDownload;->stop()V

    .line 198
    iget v0, p0, Lcom/android/providers/contacts/util/FbHighQualityImageDownload;->mCountOfTryFbAccountSync:I

    const/4 v1, 0x5

    if-ge v0, v1, :cond_0

    .line 199
    iget v0, p0, Lcom/android/providers/contacts/util/FbHighQualityImageDownload;->mCountOfTryFbAccountSync:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/android/providers/contacts/util/FbHighQualityImageDownload;->mCountOfTryFbAccountSync:I

    .line 200
    invoke-direct {p0}, Lcom/android/providers/contacts/util/FbHighQualityImageDownload;->tryFbAccountSync()V

    .line 205
    :goto_0
    return-void

    .line 202
    :cond_0
    const-string v0, "FB_LOGS"

    const-string v1, "stopBySyncFail, sync count is over. we don\'t try account sync any more"

    invoke-static {v0, v1}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method private tryFbAccountSync()V
    .locals 6

    .prologue
    .line 212
    const-string v2, "FB_LOGS"

    const-string v3, "tryFbAccountSync"

    invoke-static {v2, v3}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 214
    invoke-direct {p0}, Lcom/android/providers/contacts/util/FbHighQualityImageDownload;->getFbAccount()Landroid/accounts/Account;

    move-result-object v0

    .line 216
    .local v0, "account":Landroid/accounts/Account;
    if-eqz v0, :cond_0

    .line 217
    const-string v2, "com.android.contacts"

    invoke-static {v0, v2}, Landroid/content/ContentResolver;->getSyncAutomatically(Landroid/accounts/Account;Ljava/lang/String;)Z

    move-result v1

    .line 219
    .local v1, "syncEnabled":Z
    if-eqz v1, :cond_1

    .line 220
    const-string v2, "FB_LOGS"

    const-string v3, "tryFbAccountSync : disable FB account"

    invoke-static {v2, v3}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 221
    const-string v2, "com.android.contacts"

    const/4 v3, 0x0

    invoke-static {v0, v2, v3}, Landroid/content/ContentResolver;->setSyncAutomatically(Landroid/accounts/Account;Ljava/lang/String;Z)V

    .line 223
    new-instance v2, Landroid/os/Handler;

    invoke-direct {v2}, Landroid/os/Handler;-><init>()V

    new-instance v3, Lcom/android/providers/contacts/util/FbHighQualityImageDownload$2;

    invoke-direct {v3, p0, v0}, Lcom/android/providers/contacts/util/FbHighQualityImageDownload$2;-><init>(Lcom/android/providers/contacts/util/FbHighQualityImageDownload;Landroid/accounts/Account;)V

    const-wide/16 v4, 0x3e8

    invoke-virtual {v2, v3, v4, v5}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 234
    .end local v1    # "syncEnabled":Z
    :cond_0
    :goto_0
    return-void

    .line 231
    .restart local v1    # "syncEnabled":Z
    :cond_1
    const-string v2, "FB_LOGS"

    const-string v3, "tryFbAccountSync : FB account is diabled"

    invoke-static {v2, v3}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method


# virtual methods
.method public getDatabaseHelper()Lcom/android/providers/contacts/ContactsDatabaseHelper;
    .locals 1

    .prologue
    .line 255
    iget-object v0, p0, Lcom/android/providers/contacts/util/FbHighQualityImageDownload;->mContext:Landroid/content/Context;

    invoke-static {v0}, Lcom/android/providers/contacts/ContactsDatabaseHelper;->getInstance(Landroid/content/Context;)Lcom/android/providers/contacts/ContactsDatabaseHelper;

    move-result-object v0

    return-object v0
.end method

.method public getFacebookProfileIdFromDb()V
    .locals 19

    .prologue
    .line 281
    const/4 v9, 0x0

    .line 282
    .local v9, "c":Landroid/database/Cursor;
    const-string v4, "account_type IS \'com.facebook.auth.login\'"

    .line 284
    .local v4, "selection":Ljava/lang/String;
    invoke-virtual/range {p0 .. p0}, Lcom/android/providers/contacts/util/FbHighQualityImageDownload;->getDatabaseHelper()Lcom/android/providers/contacts/ContactsDatabaseHelper;

    move-result-object v2

    invoke-virtual {v2}, Lcom/android/providers/contacts/ContactsDatabaseHelper;->getReadableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v1

    .line 286
    .local v1, "db":Landroid/database/sqlite/SQLiteDatabase;
    new-instance v12, Ljava/util/HashMap;

    invoke-direct {v12}, Ljava/util/HashMap;-><init>()V

    .line 289
    .local v12, "fbIdMap":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/String;Ljava/lang/String;>;"
    :try_start_0
    const-string v2, "view_raw_contacts"

    const/4 v3, 0x2

    new-array v3, v3, [Ljava/lang/String;

    const/4 v5, 0x0

    const-string v6, "sourceid"

    aput-object v6, v3, v5

    const/4 v5, 0x1

    const-string v6, "_id"

    aput-object v6, v3, v5

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    const/4 v8, 0x0

    invoke-virtual/range {v1 .. v8}, Landroid/database/sqlite/SQLiteDatabase;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v9

    .line 292
    if-eqz v9, :cond_2

    .line 293
    invoke-interface {v9}, Landroid/database/Cursor;->moveToFirst()Z

    .line 294
    invoke-interface {v9}, Landroid/database/Cursor;->getCount()I

    move-result v2

    if-lez v2, :cond_2

    .line 295
    invoke-virtual/range {p0 .. p0}, Lcom/android/providers/contacts/util/FbHighQualityImageDownload;->stop()V

    .line 297
    const/4 v2, 0x0

    move-object/from16 v0, p0

    iput v2, v0, Lcom/android/providers/contacts/util/FbHighQualityImageDownload;->mCountOfFailByNotSynced:I

    .line 298
    const/4 v2, 0x0

    move-object/from16 v0, p0

    iput v2, v0, Lcom/android/providers/contacts/util/FbHighQualityImageDownload;->mSizeOfQueueToCheckNotSynced:I

    .line 300
    :goto_0
    invoke-interface {v9}, Landroid/database/Cursor;->isAfterLast()Z

    move-result v2

    if-nez v2, :cond_2

    .line 301
    const-string v2, "sourceid"

    invoke-interface {v9, v2}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v18

    .line 302
    .local v18, "sourceIdIndex":I
    const-string v2, "_id"

    invoke-interface {v9, v2}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v16

    .line 303
    .local v16, "rawContactIndex":I
    move/from16 v0, v18

    invoke-interface {v9, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v17

    .line 304
    .local v17, "sourceId":Ljava/lang/String;
    move/from16 v0, v16

    invoke-interface {v9, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v15

    .line 305
    .local v15, "rawContactId":Ljava/lang/String;
    if-eqz v17, :cond_0

    const-string v2, ""

    move-object/from16 v0, v17

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 306
    move-object/from16 v0, v17

    invoke-virtual {v12, v15, v0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 308
    :cond_0
    invoke-interface {v9}, Landroid/database/Cursor;->moveToNext()Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 313
    .end local v15    # "rawContactId":Ljava/lang/String;
    .end local v16    # "rawContactIndex":I
    .end local v17    # "sourceId":Ljava/lang/String;
    .end local v18    # "sourceIdIndex":I
    :catchall_0
    move-exception v2

    if-eqz v9, :cond_1

    .line 314
    invoke-interface {v9}, Landroid/database/Cursor;->close()V

    :cond_1
    throw v2

    .line 313
    :cond_2
    if-eqz v9, :cond_3

    .line 314
    invoke-interface {v9}, Landroid/database/Cursor;->close()V

    .line 316
    :cond_3
    const/4 v10, 0x0

    .line 320
    .local v10, "fbContactsCount":I
    invoke-virtual {v12}, Ljava/util/HashMap;->entrySet()Ljava/util/Set;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v13

    .line 322
    .local v13, "iterator":Ljava/util/Iterator;, "Ljava/util/Iterator<Ljava/util/Map$Entry<Ljava/lang/String;Ljava/lang/String;>;>;"
    :goto_1
    invoke-interface {v13}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_4

    .line 323
    invoke-interface {v13}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v14

    check-cast v14, Ljava/util/Map$Entry;

    .line 324
    .local v14, "mapEntry":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/String;Ljava/lang/String;>;"
    invoke-interface {v14}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v15

    check-cast v15, Ljava/lang/String;

    .line 325
    .restart local v15    # "rawContactId":Ljava/lang/String;
    invoke-interface {v14}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v11

    check-cast v11, Ljava/lang/String;

    .line 327
    .local v11, "fbId":Ljava/lang/String;
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/android/providers/contacts/util/FbHighQualityImageDownload;->mQueue:Lcom/android/providers/contacts/util/FbHighQualityImageDownload$FacebookIdQueue;

    new-instance v3, Lcom/android/providers/contacts/util/FbHighQualityImageDownload$QueueItem;

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/android/providers/contacts/util/FbHighQualityImageDownload;->mContext:Landroid/content/Context;

    invoke-direct {v3, v5, v15, v11}, Lcom/android/providers/contacts/util/FbHighQualityImageDownload$QueueItem;-><init>(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)V

    # invokes: Lcom/android/providers/contacts/util/FbHighQualityImageDownload$FacebookIdQueue;->addFacebookIdToQueue(Lcom/android/providers/contacts/util/FbHighQualityImageDownload$QueueItem;)Z
    invoke-static {v2, v3}, Lcom/android/providers/contacts/util/FbHighQualityImageDownload$FacebookIdQueue;->access$700(Lcom/android/providers/contacts/util/FbHighQualityImageDownload$FacebookIdQueue;Lcom/android/providers/contacts/util/FbHighQualityImageDownload$QueueItem;)Z

    .line 329
    add-int/lit8 v10, v10, 0x1

    .line 330
    goto :goto_1

    .line 331
    .end local v11    # "fbId":Ljava/lang/String;
    .end local v14    # "mapEntry":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/String;Ljava/lang/String;>;"
    .end local v15    # "rawContactId":Ljava/lang/String;
    :cond_4
    const-string v2, "FB_LOGS"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Number of facebook contacts : "

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v10}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 333
    if-lez v10, :cond_6

    .line 334
    const/4 v2, 0x1

    move-object/from16 v0, p0

    iput-boolean v2, v0, Lcom/android/providers/contacts/util/FbHighQualityImageDownload;->mIsFbHighQualitySyncing:Z

    .line 336
    const/16 v2, 0xfa

    if-le v10, v2, :cond_5

    mul-int/lit8 v2, v10, 0x2

    :goto_2
    move-object/from16 v0, p0

    iput v2, v0, Lcom/android/providers/contacts/util/FbHighQualityImageDownload;->mMaxCountOfFailByNotSynced:I

    .line 339
    const/4 v2, 0x0

    move-object/from16 v0, p0

    invoke-direct {v0, v2}, Lcom/android/providers/contacts/util/FbHighQualityImageDownload;->sendMessageToFbImageDownloadHandler(I)V

    .line 343
    :goto_3
    return-void

    .line 336
    :cond_5
    const/16 v2, 0x1f4

    goto :goto_2

    .line 341
    :cond_6
    invoke-virtual/range {p0 .. p0}, Lcom/android/providers/contacts/util/FbHighQualityImageDownload;->stop()V

    goto :goto_3
.end method

.method public init()V
    .locals 3

    .prologue
    .line 171
    const-string v0, "FB_LOGS"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "In FbHighQualityImageDownload fbHighQualitySyncStarted = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-boolean v2, p0, Lcom/android/providers/contacts/util/FbHighQualityImageDownload;->mIsFbHighQualitySyncing:Z

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 174
    iget-boolean v0, p0, Lcom/android/providers/contacts/util/FbHighQualityImageDownload;->mIsFbHighQualitySyncing:Z

    if-nez v0, :cond_0

    .line 175
    invoke-virtual {p0}, Lcom/android/providers/contacts/util/FbHighQualityImageDownload;->getFacebookProfileIdFromDb()V

    .line 177
    :cond_0
    return-void
.end method

.method public stop()V
    .locals 2

    .prologue
    .line 183
    const-string v0, "FB_LOGS"

    const-string v1, "In FbHighQualityImageDownload STOP"

    invoke-static {v0, v1}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 185
    iget-object v0, p0, Lcom/android/providers/contacts/util/FbHighQualityImageDownload;->mQueue:Lcom/android/providers/contacts/util/FbHighQualityImageDownload$FacebookIdQueue;

    # invokes: Lcom/android/providers/contacts/util/FbHighQualityImageDownload$FacebookIdQueue;->removeAll()I
    invoke-static {v0}, Lcom/android/providers/contacts/util/FbHighQualityImageDownload$FacebookIdQueue;->access$600(Lcom/android/providers/contacts/util/FbHighQualityImageDownload$FacebookIdQueue;)I

    .line 187
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/android/providers/contacts/util/FbHighQualityImageDownload;->mIsFbHighQualitySyncing:Z

    .line 188
    return-void
.end method
