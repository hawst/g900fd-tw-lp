.class public Lcom/android/providers/contacts/KnoxReceiver$UserSwitchReceiver;
.super Landroid/content/BroadcastReceiver;
.source "KnoxReceiver.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/providers/contacts/KnoxReceiver;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "UserSwitchReceiver"
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 73
    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    return-void
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 6
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "intent"    # Landroid/content/Intent;

    .prologue
    const/4 v5, 0x0

    .line 77
    if-eqz p2, :cond_1

    .line 78
    const-string v3, "android.intent.action.USER_ADDED"

    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_0

    const-string v3, "android.intent.action.USER_REMOVED"

    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 80
    :cond_0
    const-string v3, "persona"

    invoke-virtual {p1, v3}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/os/PersonaManager;

    .line 82
    .local v2, "pm":Landroid/os/PersonaManager;
    invoke-static {p1}, Landroid/os/PersonaManager;->isKioskModeEnabled(Landroid/content/Context;)Z

    move-result v3

    # setter for: Lcom/android/providers/contacts/KnoxReceiver;->mbIsContainerOnlyMode:Z
    invoke-static {v3}, Lcom/android/providers/contacts/KnoxReceiver;->access$002(Z)Z

    .line 85
    if-eqz v2, :cond_1

    # getter for: Lcom/android/providers/contacts/KnoxReceiver;->mbIsContainerOnlyMode:Z
    invoke-static {}, Lcom/android/providers/contacts/KnoxReceiver;->access$000()Z

    move-result v3

    if-nez v3, :cond_2

    .line 100
    .end local v2    # "pm":Landroid/os/PersonaManager;
    :cond_1
    :goto_0
    return-void

    .line 88
    .restart local v2    # "pm":Landroid/os/PersonaManager;
    :cond_2
    const/4 v3, 0x1

    invoke-virtual {v2, v3}, Landroid/os/PersonaManager;->getPersonas(Z)Ljava/util/List;

    move-result-object v1

    .line 89
    .local v1, "personas":Ljava/util/List;, "Ljava/util/List<Landroid/content/pm/PersonaInfo;>;"
    # setter for: Lcom/android/providers/contacts/KnoxReceiver;->mbIsContainerOnlyMode:Z
    invoke-static {v5}, Lcom/android/providers/contacts/KnoxReceiver;->access$002(Z)Z

    .line 90
    if-eqz v1, :cond_3

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v3

    if-lez v3, :cond_3

    .line 91
    invoke-interface {v1, v5}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/pm/PersonaInfo;

    .line 92
    .local v0, "perInfo":Landroid/content/pm/PersonaInfo;
    if-eqz v0, :cond_3

    .line 93
    iget-boolean v3, v0, Landroid/content/pm/PersonaInfo;->isKioskModeEnabled:Z

    # setter for: Lcom/android/providers/contacts/KnoxReceiver;->mbIsContainerOnlyMode:Z
    invoke-static {v3}, Lcom/android/providers/contacts/KnoxReceiver;->access$002(Z)Z

    .line 97
    .end local v0    # "perInfo":Landroid/content/pm/PersonaInfo;
    :cond_3
    const-string v3, "KnoxReceiver"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "/"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    # getter for: Lcom/android/providers/contacts/KnoxReceiver;->mbIsContainerOnlyMode:Z
    invoke-static {}, Lcom/android/providers/contacts/KnoxReceiver;->access$000()Z

    move-result v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method
