.class public Lcom/android/providers/contacts/DataRowHandlerForKidsPhoto;
.super Lcom/android/providers/contacts/DataRowHandler;
.source "DataRowHandlerForKidsPhoto.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/android/providers/contacts/DataRowHandlerForKidsPhoto$KidsPhoto;
    }
.end annotation


# instance fields
.field private final mMaxDisplayPhotoDim:I

.field private final mMaxThumbnailPhotoDim:I

.field private final mPhotoStore:Lcom/android/providers/contacts/PhotoStore;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/android/providers/contacts/ContactsDatabaseHelper;Lcom/android/providers/contacts/aggregation/ContactAggregator;Lcom/android/providers/contacts/PhotoStore;II)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "dbHelper"    # Lcom/android/providers/contacts/ContactsDatabaseHelper;
    .param p3, "aggregator"    # Lcom/android/providers/contacts/aggregation/ContactAggregator;
    .param p4, "photoStore"    # Lcom/android/providers/contacts/PhotoStore;
    .param p5, "maxDisplayPhotoDim"    # I
    .param p6, "maxThumbnailPhotoDim"    # I

    .prologue
    .line 65
    const-string v0, "vnd.sec.cursor.item/kids_photo"

    invoke-direct {p0, p1, p2, p3, v0}, Lcom/android/providers/contacts/DataRowHandler;-><init>(Landroid/content/Context;Lcom/android/providers/contacts/ContactsDatabaseHelper;Lcom/android/providers/contacts/aggregation/ContactAggregator;Ljava/lang/String;)V

    .line 66
    iput-object p4, p0, Lcom/android/providers/contacts/DataRowHandlerForKidsPhoto;->mPhotoStore:Lcom/android/providers/contacts/PhotoStore;

    .line 67
    iput p5, p0, Lcom/android/providers/contacts/DataRowHandlerForKidsPhoto;->mMaxDisplayPhotoDim:I

    .line 68
    iput p6, p0, Lcom/android/providers/contacts/DataRowHandlerForKidsPhoto;->mMaxThumbnailPhotoDim:I

    .line 69
    return-void
.end method

.method private hasNonNullPhoto(Landroid/content/ContentValues;)Z
    .locals 2
    .param p1, "values"    # Landroid/content/ContentValues;

    .prologue
    .line 150
    const-string v1, "data15"

    invoke-virtual {p1, v1}, Landroid/content/ContentValues;->getAsByteArray(Ljava/lang/String;)[B

    move-result-object v0

    .line 151
    .local v0, "photoBytes":[B
    if-eqz v0, :cond_0

    array-length v1, v0

    if-lez v1, :cond_0

    const/4 v1, 0x1

    :goto_0
    return v1

    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method private preProcessPhoto(Landroid/content/ContentValues;)Z
    .locals 2
    .param p1, "values"    # Landroid/content/ContentValues;

    .prologue
    .line 130
    const-string v1, "data15"

    invoke-virtual {p1, v1}, Landroid/content/ContentValues;->containsKey(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 131
    invoke-direct {p0, p1}, Lcom/android/providers/contacts/DataRowHandlerForKidsPhoto;->hasNonNullPhoto(Landroid/content/ContentValues;)Z

    move-result v0

    .line 132
    .local v0, "photoExists":Z
    if-eqz v0, :cond_0

    .line 133
    invoke-direct {p0, p1}, Lcom/android/providers/contacts/DataRowHandlerForKidsPhoto;->processPhoto(Landroid/content/ContentValues;)Z

    move-result v1

    if-nez v1, :cond_1

    .line 135
    const/4 v1, 0x0

    .line 146
    .end local v0    # "photoExists":Z
    :goto_0
    return v1

    .line 142
    .restart local v0    # "photoExists":Z
    :cond_0
    const-string v1, "data15"

    invoke-virtual {p1, v1}, Landroid/content/ContentValues;->putNull(Ljava/lang/String;)V

    .line 143
    const-string v1, "data14"

    invoke-virtual {p1, v1}, Landroid/content/ContentValues;->putNull(Ljava/lang/String;)V

    .line 146
    .end local v0    # "photoExists":Z
    :cond_1
    const/4 v1, 0x1

    goto :goto_0
.end method

.method private processPhoto(Landroid/content/ContentValues;)Z
    .locals 8
    .param p1, "values"    # Landroid/content/ContentValues;

    .prologue
    .line 155
    const-string v5, "data15"

    invoke-virtual {p1, v5}, Landroid/content/ContentValues;->getAsByteArray(Ljava/lang/String;)[B

    move-result-object v1

    .line 156
    .local v1, "originalPhoto":[B
    if-eqz v1, :cond_1

    .line 158
    :try_start_0
    new-instance v4, Lcom/android/providers/contacts/PhotoProcessor;

    iget v5, p0, Lcom/android/providers/contacts/DataRowHandlerForKidsPhoto;->mMaxDisplayPhotoDim:I

    iget v6, p0, Lcom/android/providers/contacts/DataRowHandlerForKidsPhoto;->mMaxThumbnailPhotoDim:I

    invoke-direct {v4, v1, v5, v6}, Lcom/android/providers/contacts/PhotoProcessor;-><init>([BII)V

    .line 160
    .local v4, "processor":Lcom/android/providers/contacts/PhotoProcessor;
    iget-object v5, p0, Lcom/android/providers/contacts/DataRowHandlerForKidsPhoto;->mPhotoStore:Lcom/android/providers/contacts/PhotoStore;

    invoke-virtual {v5, v4}, Lcom/android/providers/contacts/PhotoStore;->insert(Lcom/android/providers/contacts/PhotoProcessor;)J

    move-result-wide v2

    .line 161
    .local v2, "photoFileId":J
    const-wide/16 v6, 0x0

    cmp-long v5, v2, v6

    if-eqz v5, :cond_0

    .line 162
    const-string v5, "data14"

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v6

    invoke-virtual {p1, v5, v6}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 166
    :goto_0
    const-string v5, "data15"

    invoke-virtual {v4}, Lcom/android/providers/contacts/PhotoProcessor;->getThumbnailPhotoBytes()[B

    move-result-object v6

    invoke-virtual {p1, v5, v6}, Landroid/content/ContentValues;->put(Ljava/lang/String;[B)V

    .line 167
    const/4 v5, 0x1

    .line 172
    .end local v2    # "photoFileId":J
    .end local v4    # "processor":Lcom/android/providers/contacts/PhotoProcessor;
    :goto_1
    return v5

    .line 164
    .restart local v2    # "photoFileId":J
    .restart local v4    # "processor":Lcom/android/providers/contacts/PhotoProcessor;
    :cond_0
    const-string v5, "data14"

    invoke-virtual {p1, v5}, Landroid/content/ContentValues;->putNull(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 168
    .end local v2    # "photoFileId":J
    .end local v4    # "processor":Lcom/android/providers/contacts/PhotoProcessor;
    :catch_0
    move-exception v0

    .line 169
    .local v0, "ioe":Ljava/io/IOException;
    const-string v5, "DataRowHandlerForKidsPhoto"

    const-string v6, "Could not process KidsPhoto for insert or update"

    invoke-static {v5, v6, v0}, Landroid/util/secutil/Log;->secE(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 172
    .end local v0    # "ioe":Ljava/io/IOException;
    :cond_1
    const/4 v5, 0x0

    goto :goto_1
.end method


# virtual methods
.method public insert(Landroid/database/sqlite/SQLiteDatabase;Lcom/android/providers/contacts/TransactionContext;JLandroid/content/ContentValues;)J
    .locals 9
    .param p1, "db"    # Landroid/database/sqlite/SQLiteDatabase;
    .param p2, "txContext"    # Lcom/android/providers/contacts/TransactionContext;
    .param p3, "rawContactId"    # J
    .param p5, "values"    # Landroid/content/ContentValues;

    .prologue
    const-wide/16 v0, 0x0

    .line 75
    const-string v4, "skip_processing"

    invoke-virtual {p5, v4}, Landroid/content/ContentValues;->containsKey(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_2

    .line 76
    const-string v4, "skip_processing"

    invoke-virtual {p5, v4}, Landroid/content/ContentValues;->remove(Ljava/lang/String;)V

    .line 84
    :cond_0
    cmp-long v4, p3, v0

    if-eqz v4, :cond_1

    .line 85
    const-string v4, "DataRowHandlerForKidsPhoto"

    const-string v5, "Ignore to set the rawContactId for Kids Photo"

    invoke-static {v4, v5}, Landroid/util/secutil/Log;->secI(Ljava/lang/String;Ljava/lang/String;)I

    .line 88
    :cond_1
    const-string v4, "data"

    const/4 v5, 0x0

    invoke-virtual {p1, v4, v5, p5}, Landroid/database/sqlite/SQLiteDatabase;->insert(Ljava/lang/String;Ljava/lang/String;Landroid/content/ContentValues;)J

    move-result-wide v0

    .line 90
    .local v0, "dataId":J
    const-string v4, "data13"

    invoke-virtual {p5, v4}, Landroid/content/ContentValues;->getAsString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 91
    .local v2, "kidsId":Ljava/lang/String;
    new-instance v3, Landroid/content/ContentValues;

    invoke-direct {v3}, Landroid/content/ContentValues;-><init>()V

    .line 92
    .local v3, "v":Landroid/content/ContentValues;
    const-string v4, "photo_id"

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v5

    invoke-virtual {v3, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 93
    const-string v4, "kids"

    const-string v5, "_id =?"

    const/4 v6, 0x1

    new-array v6, v6, [Ljava/lang/String;

    const/4 v7, 0x0

    aput-object v2, v6, v7

    invoke-virtual {p1, v4, v3, v5, v6}, Landroid/database/sqlite/SQLiteDatabase;->update(Ljava/lang/String;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    .line 97
    .end local v0    # "dataId":J
    .end local v2    # "kidsId":Ljava/lang/String;
    .end local v3    # "v":Landroid/content/ContentValues;
    :goto_0
    return-wide v0

    .line 79
    :cond_2
    invoke-direct {p0, p5}, Lcom/android/providers/contacts/DataRowHandlerForKidsPhoto;->preProcessPhoto(Landroid/content/ContentValues;)Z

    move-result v4

    if-nez v4, :cond_0

    goto :goto_0
.end method

.method public update(Landroid/database/sqlite/SQLiteDatabase;Lcom/android/providers/contacts/TransactionContext;Landroid/content/ContentValues;Landroid/database/Cursor;Z)Z
    .locals 9
    .param p1, "db"    # Landroid/database/sqlite/SQLiteDatabase;
    .param p2, "txContext"    # Lcom/android/providers/contacts/TransactionContext;
    .param p3, "values"    # Landroid/content/ContentValues;
    .param p4, "c"    # Landroid/database/Cursor;
    .param p5, "callerIsSyncAdapter"    # Z

    .prologue
    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 103
    invoke-interface {p4, v4}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v0

    .line 105
    .local v0, "dataId":J
    const-string v6, "skip_processing"

    invoke-virtual {p3, v6}, Landroid/content/ContentValues;->containsKey(Ljava/lang/String;)Z

    move-result v6

    if-eqz v6, :cond_2

    .line 106
    const-string v6, "skip_processing"

    invoke-virtual {p3, v6}, Landroid/content/ContentValues;->remove(Ljava/lang/String;)V

    .line 115
    :cond_0
    invoke-virtual {p3}, Landroid/content/ContentValues;->size()I

    move-result v6

    if-lez v6, :cond_1

    .line 116
    iget-object v6, p0, Lcom/android/providers/contacts/DataRowHandlerForKidsPhoto;->mSelectionArgs1:[Ljava/lang/String;

    invoke-static {v0, v1}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v7

    aput-object v7, v6, v4

    .line 117
    const-string v6, "data"

    const-string v7, "_id =?"

    iget-object v8, p0, Lcom/android/providers/contacts/DataRowHandlerForKidsPhoto;->mSelectionArgs1:[Ljava/lang/String;

    invoke-virtual {p1, v6, p3, v7, v8}, Landroid/database/sqlite/SQLiteDatabase;->update(Ljava/lang/String;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    .line 118
    new-instance v3, Landroid/content/ContentValues;

    invoke-direct {v3}, Landroid/content/ContentValues;-><init>()V

    .line 119
    .local v3, "v":Landroid/content/ContentValues;
    const-string v6, "data13"

    invoke-virtual {p3, v6}, Landroid/content/ContentValues;->getAsString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 120
    .local v2, "kidsId":Ljava/lang/String;
    const-string v6, "photo_id"

    invoke-virtual {v3, v6, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 121
    const-string v6, "kids"

    const-string v7, "_id =?"

    new-array v8, v5, [Ljava/lang/String;

    aput-object v2, v8, v4

    invoke-virtual {p1, v6, v3, v7, v8}, Landroid/database/sqlite/SQLiteDatabase;->update(Ljava/lang/String;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    move v4, v5

    .line 126
    .end local v2    # "kidsId":Ljava/lang/String;
    .end local v3    # "v":Landroid/content/ContentValues;
    :cond_1
    :goto_0
    return v4

    .line 109
    :cond_2
    invoke-direct {p0, p3}, Lcom/android/providers/contacts/DataRowHandlerForKidsPhoto;->preProcessPhoto(Landroid/content/ContentValues;)Z

    move-result v6

    if-nez v6, :cond_0

    goto :goto_0
.end method
