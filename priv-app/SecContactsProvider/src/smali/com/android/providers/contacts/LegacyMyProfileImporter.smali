.class public Lcom/android/providers/contacts/LegacyMyProfileImporter;
.super Ljava/lang/Object;
.source "LegacyMyProfileImporter.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/android/providers/contacts/LegacyMyProfileImporter$ContactLinkInfoUpdate;,
        Lcom/android/providers/contacts/LegacyMyProfileImporter$HasPhoneHasEmailUpdate;,
        Lcom/android/providers/contacts/LegacyMyProfileImporter$PhotoIdUpdate;,
        Lcom/android/providers/contacts/LegacyMyProfileImporter$PhoneLookupInsert;,
        Lcom/android/providers/contacts/LegacyMyProfileImporter$DataInsert;,
        Lcom/android/providers/contacts/LegacyMyProfileImporter$ContactsUpdate;,
        Lcom/android/providers/contacts/LegacyMyProfileImporter$RawContactsUpdate;,
        Lcom/android/providers/contacts/LegacyMyProfileImporter$RawContactsInsert;
    }
.end annotation


# static fields
.field private static RAW_CONTACT_ENTITIES_PROJECTION:[Ljava/lang/String;

.field private static mRawContactsAccountId:J


# instance fields
.field private mContactsHelper:Lcom/android/providers/contacts/ContactsDatabaseHelper;

.field private final mContactsProvider:Lcom/android/providers/contacts/ContactsProvider2;

.field private mProfileHelper:Lcom/android/providers/contacts/ProfileDatabaseHelper;

.field private final mProfileProvider:Lcom/android/providers/contacts/ProfileProvider;

.field private mSourceDb:Landroid/database/sqlite/SQLiteDatabase;

.field private mTargetDb:Landroid/database/sqlite/SQLiteDatabase;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    .line 61
    const/16 v0, 0x12

    new-array v0, v0, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string v2, "data1"

    aput-object v2, v0, v1

    const/4 v1, 0x1

    const-string v2, "data2"

    aput-object v2, v0, v1

    const/4 v1, 0x2

    const-string v2, "data3"

    aput-object v2, v0, v1

    const/4 v1, 0x3

    const-string v2, "data4"

    aput-object v2, v0, v1

    const/4 v1, 0x4

    const-string v2, "data5"

    aput-object v2, v0, v1

    const/4 v1, 0x5

    const-string v2, "data6"

    aput-object v2, v0, v1

    const/4 v1, 0x6

    const-string v2, "data7"

    aput-object v2, v0, v1

    const/4 v1, 0x7

    const-string v2, "data8"

    aput-object v2, v0, v1

    const/16 v1, 0x8

    const-string v2, "data9"

    aput-object v2, v0, v1

    const/16 v1, 0x9

    const-string v2, "data10"

    aput-object v2, v0, v1

    const/16 v1, 0xa

    const-string v2, "data11"

    aput-object v2, v0, v1

    const/16 v1, 0xb

    const-string v2, "data12"

    aput-object v2, v0, v1

    const/16 v1, 0xc

    const-string v2, "data13"

    aput-object v2, v0, v1

    const/16 v1, 0xd

    const-string v2, "data14"

    aput-object v2, v0, v1

    const/16 v1, 0xe

    const-string v2, "data15"

    aput-object v2, v0, v1

    const/16 v1, 0xf

    const-string v2, "mimetype"

    aput-object v2, v0, v1

    const/16 v1, 0x10

    const-string v2, "is_primary"

    aput-object v2, v0, v1

    const/16 v1, 0x11

    const-string v2, "is_super_primary"

    aput-object v2, v0, v1

    sput-object v0, Lcom/android/providers/contacts/LegacyMyProfileImporter;->RAW_CONTACT_ENTITIES_PROJECTION:[Ljava/lang/String;

    .line 101
    const-wide/16 v0, -0x1

    sput-wide v0, Lcom/android/providers/contacts/LegacyMyProfileImporter;->mRawContactsAccountId:J

    return-void
.end method

.method public constructor <init>(Lcom/android/providers/contacts/ContactsProvider2;Lcom/android/providers/contacts/ProfileProvider;)V
    .locals 0
    .param p1, "contactsProvider"    # Lcom/android/providers/contacts/ContactsProvider2;
    .param p2, "profileProvider"    # Lcom/android/providers/contacts/ProfileProvider;

    .prologue
    .line 105
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 106
    iput-object p1, p0, Lcom/android/providers/contacts/LegacyMyProfileImporter;->mContactsProvider:Lcom/android/providers/contacts/ContactsProvider2;

    .line 107
    iput-object p2, p0, Lcom/android/providers/contacts/LegacyMyProfileImporter;->mProfileProvider:Lcom/android/providers/contacts/ProfileProvider;

    .line 108
    return-void
.end method

.method private bindPhoneNumberData(Landroid/database/sqlite/SQLiteStatement;Landroid/database/Cursor;Ljava/lang/String;Ljava/lang/String;)V
    .locals 4
    .param p1, "dataInsert"    # Landroid/database/sqlite/SQLiteStatement;
    .param p2, "c"    # Landroid/database/Cursor;
    .param p3, "number"    # Ljava/lang/String;
    .param p4, "numberE164"    # Ljava/lang/String;

    .prologue
    const/4 v3, 0x4

    const/4 v2, 0x2

    const/4 v1, 0x1

    .line 288
    invoke-virtual {p1, v1, p3}, Landroid/database/sqlite/SQLiteStatement;->bindString(ILjava/lang/String;)V

    .line 289
    invoke-interface {p2, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-direct {p0, p1, v2, v1}, Lcom/android/providers/contacts/LegacyMyProfileImporter;->bindString(Landroid/database/sqlite/SQLiteStatement;ILjava/lang/String;)V

    .line 290
    const/4 v1, 0x3

    invoke-interface {p2, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-direct {p0, p1, v1, v2}, Lcom/android/providers/contacts/LegacyMyProfileImporter;->bindString(Landroid/database/sqlite/SQLiteStatement;ILjava/lang/String;)V

    .line 291
    if-eqz p4, :cond_0

    .line 292
    invoke-virtual {p1, v3, p4}, Landroid/database/sqlite/SQLiteStatement;->bindString(ILjava/lang/String;)V

    .line 296
    :goto_0
    const/4 v0, 0x5

    .local v0, "i":I
    :goto_1
    const/16 v1, 0x10

    if-ge v0, v1, :cond_1

    .line 297
    add-int/lit8 v1, v0, -0x1

    invoke-interface {p2, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-direct {p0, p1, v0, v1}, Lcom/android/providers/contacts/LegacyMyProfileImporter;->bindString(Landroid/database/sqlite/SQLiteStatement;ILjava/lang/String;)V

    .line 296
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 294
    .end local v0    # "i":I
    :cond_0
    invoke-virtual {p1, v3}, Landroid/database/sqlite/SQLiteStatement;->bindNull(I)V

    goto :goto_0

    .line 300
    .restart local v0    # "i":I
    :cond_1
    return-void
.end method

.method private bindString(Landroid/database/sqlite/SQLiteStatement;ILjava/lang/String;)V
    .locals 0
    .param p1, "insert"    # Landroid/database/sqlite/SQLiteStatement;
    .param p2, "index"    # I
    .param p3, "string"    # Ljava/lang/String;

    .prologue
    .line 551
    if-nez p3, :cond_0

    .line 552
    invoke-virtual {p1, p2}, Landroid/database/sqlite/SQLiteStatement;->bindNull(I)V

    .line 556
    :goto_0
    return-void

    .line 554
    :cond_0
    invoke-virtual {p1, p2, p3}, Landroid/database/sqlite/SQLiteStatement;->bindString(ILjava/lang/String;)V

    goto :goto_0
.end method

.method private checkForImportMyProfileData()Z
    .locals 5

    .prologue
    .line 368
    iget-object v2, p0, Lcom/android/providers/contacts/LegacyMyProfileImporter;->mSourceDb:Landroid/database/sqlite/SQLiteDatabase;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "SELECT COUNT(*) FROM raw_contacts JOIN accounts ON (accounts._id=raw_contacts.account_id) WHERE account_name= "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "vnd.sec.contact.my_profile"

    invoke-static {v4}, Landroid/database/DatabaseUtils;->sqlEscapeString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " AND "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "account_type"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "="

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "vnd.sec.contact.my_profile"

    invoke-static {v4}, Landroid/database/DatabaseUtils;->sqlEscapeString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    const/4 v4, 0x0

    invoke-static {v2, v3, v4}, Landroid/database/DatabaseUtils;->longForQuery(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;[Ljava/lang/String;)J

    move-result-wide v0

    .line 375
    .local v0, "count":J
    const-wide/16 v2, 0x0

    cmp-long v2, v0, v2

    if-eqz v2, :cond_0

    const/4 v2, 0x1

    :goto_0
    return v2

    :cond_0
    const/4 v2, 0x0

    goto :goto_0
.end method

.method private importMyProfielDataFromContactsDb()V
    .locals 2

    .prologue
    .line 137
    iget-object v0, p0, Lcom/android/providers/contacts/LegacyMyProfileImporter;->mContactsProvider:Lcom/android/providers/contacts/ContactsProvider2;

    invoke-virtual {v0}, Lcom/android/providers/contacts/ContactsProvider2;->getDatabaseHelper()Landroid/database/sqlite/SQLiteOpenHelper;

    move-result-object v0

    check-cast v0, Lcom/android/providers/contacts/ContactsDatabaseHelper;

    iput-object v0, p0, Lcom/android/providers/contacts/LegacyMyProfileImporter;->mContactsHelper:Lcom/android/providers/contacts/ContactsDatabaseHelper;

    .line 138
    iget-object v0, p0, Lcom/android/providers/contacts/LegacyMyProfileImporter;->mContactsHelper:Lcom/android/providers/contacts/ContactsDatabaseHelper;

    invoke-virtual {v0}, Lcom/android/providers/contacts/ContactsDatabaseHelper;->getReadableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    iput-object v0, p0, Lcom/android/providers/contacts/LegacyMyProfileImporter;->mSourceDb:Landroid/database/sqlite/SQLiteDatabase;

    .line 139
    invoke-direct {p0}, Lcom/android/providers/contacts/LegacyMyProfileImporter;->checkForImportMyProfileData()Z

    move-result v0

    if-nez v0, :cond_0

    .line 153
    :goto_0
    return-void

    .line 143
    :cond_0
    iget-object v0, p0, Lcom/android/providers/contacts/LegacyMyProfileImporter;->mProfileProvider:Lcom/android/providers/contacts/ProfileProvider;

    invoke-virtual {v0}, Lcom/android/providers/contacts/ProfileProvider;->getDatabaseHelper()Landroid/database/sqlite/SQLiteOpenHelper;

    move-result-object v0

    check-cast v0, Lcom/android/providers/contacts/ProfileDatabaseHelper;

    iput-object v0, p0, Lcom/android/providers/contacts/LegacyMyProfileImporter;->mProfileHelper:Lcom/android/providers/contacts/ProfileDatabaseHelper;

    .line 144
    iget-object v0, p0, Lcom/android/providers/contacts/LegacyMyProfileImporter;->mProfileHelper:Lcom/android/providers/contacts/ProfileDatabaseHelper;

    invoke-virtual {v0}, Lcom/android/providers/contacts/ProfileDatabaseHelper;->getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    iput-object v0, p0, Lcom/android/providers/contacts/LegacyMyProfileImporter;->mTargetDb:Landroid/database/sqlite/SQLiteDatabase;

    .line 146
    iget-object v0, p0, Lcom/android/providers/contacts/LegacyMyProfileImporter;->mTargetDb:Landroid/database/sqlite/SQLiteDatabase;

    invoke-virtual {v0}, Landroid/database/sqlite/SQLiteDatabase;->beginTransactionNonExclusive()V

    .line 148
    :try_start_0
    invoke-direct {p0}, Lcom/android/providers/contacts/LegacyMyProfileImporter;->importMyprofileContacts()V

    .line 149
    iget-object v0, p0, Lcom/android/providers/contacts/LegacyMyProfileImporter;->mTargetDb:Landroid/database/sqlite/SQLiteDatabase;

    invoke-virtual {v0}, Landroid/database/sqlite/SQLiteDatabase;->setTransactionSuccessful()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 151
    iget-object v0, p0, Lcom/android/providers/contacts/LegacyMyProfileImporter;->mTargetDb:Landroid/database/sqlite/SQLiteDatabase;

    invoke-virtual {v0}, Landroid/database/sqlite/SQLiteDatabase;->endTransaction()V

    goto :goto_0

    :catchall_0
    move-exception v0

    iget-object v1, p0, Lcom/android/providers/contacts/LegacyMyProfileImporter;->mTargetDb:Landroid/database/sqlite/SQLiteDatabase;

    invoke-virtual {v1}, Landroid/database/sqlite/SQLiteDatabase;->endTransaction()V

    throw v0
.end method

.method private importMyprofileContacts()V
    .locals 13

    .prologue
    const/4 v5, 0x0

    .line 156
    const-string v0, "LegacyMyProfileImporter"

    const-string v1, "importMyprofileContacts"

    invoke-static {v0, v1}, Landroid/util/secutil/Log;->secV(Ljava/lang/String;Ljava/lang/String;)I

    .line 157
    const-wide/16 v8, 0x0

    .line 158
    .local v8, "raw_contact_id":J
    const-wide/16 v10, 0x0

    .line 160
    .local v10, "contact_id":J
    const-string v12, "account_name=? AND account_type=?"

    .line 162
    .local v12, "selection":Ljava/lang/String;
    const/4 v0, 0x2

    new-array v4, v0, [Ljava/lang/String;

    const/4 v0, 0x0

    const-string v1, "vnd.sec.contact.my_profile"

    aput-object v1, v4, v0

    const/4 v0, 0x1

    const-string v1, "vnd.sec.contact.my_profile"

    aput-object v1, v4, v0

    .line 165
    .local v4, "selectionArgs":[Ljava/lang/String;
    iget-object v0, p0, Lcom/android/providers/contacts/LegacyMyProfileImporter;->mSourceDb:Landroid/database/sqlite/SQLiteDatabase;

    const-string v1, "view_raw_entities"

    sget-object v2, Lcom/android/providers/contacts/LegacyMyProfileImporter;->RAW_CONTACT_ENTITIES_PROJECTION:[Ljava/lang/String;

    const-string v3, "account_name=? AND account_type=?"

    move-object v6, v5

    move-object v7, v5

    invoke-virtual/range {v0 .. v7}, Landroid/database/sqlite/SQLiteDatabase;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v7

    .line 167
    .local v7, "c":Landroid/database/Cursor;
    if-eqz v7, :cond_0

    invoke-interface {v7}, Landroid/database/Cursor;->getCount()I

    move-result v0

    if-lez v0, :cond_0

    .line 168
    invoke-direct {p0}, Lcom/android/providers/contacts/LegacyMyProfileImporter;->insertRawContacts()J

    move-result-wide v8

    .line 170
    :cond_0
    iget-object v0, p0, Lcom/android/providers/contacts/LegacyMyProfileImporter;->mTargetDb:Landroid/database/sqlite/SQLiteDatabase;

    const-string v1, "INSERT INTO data(data1,data2,data3,data4,data5,data6,data7,data8,data9,data10,data11,data12,data13,data14,data15,mimetype_id,raw_contact_id,is_primary,is_super_primary) VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)"

    invoke-virtual {v0, v1}, Landroid/database/sqlite/SQLiteDatabase;->compileStatement(Ljava/lang/String;)Landroid/database/sqlite/SQLiteStatement;

    move-result-object v6

    .line 171
    .local v6, "dataInsert":Landroid/database/sqlite/SQLiteStatement;
    const-wide/16 v0, 0x0

    cmp-long v0, v8, v0

    if-lez v0, :cond_2

    .line 173
    invoke-direct {p0, v8, v9}, Lcom/android/providers/contacts/LegacyMyProfileImporter;->insertContacts(J)J

    move-result-wide v10

    .line 174
    :goto_0
    invoke-interface {v7}, Landroid/database/Cursor;->moveToNext()Z

    move-result v0

    if-eqz v0, :cond_1

    move-object v5, p0

    .line 175
    invoke-direct/range {v5 .. v11}, Lcom/android/providers/contacts/LegacyMyProfileImporter;->insertMyProfileData(Landroid/database/sqlite/SQLiteStatement;Landroid/database/Cursor;JJ)V

    goto :goto_0

    .line 177
    :cond_1
    invoke-direct {p0, v8, v9, v10, v11}, Lcom/android/providers/contacts/LegacyMyProfileImporter;->updateContactHasPhoneHasEmailLinkTypes(JJ)V

    .line 179
    :cond_2
    if-eqz v7, :cond_3

    .line 180
    invoke-interface {v7}, Landroid/database/Cursor;->close()V

    .line 182
    :cond_3
    if-eqz v6, :cond_4

    .line 183
    invoke-virtual {v6}, Landroid/database/sqlite/SQLiteStatement;->close()V

    .line 186
    :cond_4
    invoke-direct {p0, v8, v9}, Lcom/android/providers/contacts/LegacyMyProfileImporter;->updateRawContactDisplayName(J)V

    .line 187
    invoke-direct {p0, v10, v11}, Lcom/android/providers/contacts/LegacyMyProfileImporter;->updateLookupKeyForContact(J)V

    .line 189
    return-void
.end method

.method private insert(Landroid/database/sqlite/SQLiteStatement;)J
    .locals 4
    .param p1, "insertStatement"    # Landroid/database/sqlite/SQLiteStatement;

    .prologue
    .line 559
    invoke-virtual {p1}, Landroid/database/sqlite/SQLiteStatement;->executeInsert()J

    move-result-wide v0

    .line 560
    .local v0, "rowId":J
    const-wide/16 v2, 0x0

    cmp-long v2, v0, v2

    if-nez v2, :cond_0

    .line 561
    new-instance v2, Ljava/lang/RuntimeException;

    const-string v3, "Insert failed"

    invoke-direct {v2, v3}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v2

    .line 563
    :cond_0
    return-wide v0
.end method

.method private insertContacts(J)J
    .locals 19
    .param p1, "rawContactId"    # J

    .prologue
    .line 192
    const-string v4, "LegacyMyProfileImporter"

    const-string v5, "insertContacts"

    invoke-static {v4, v5}, Landroid/util/secutil/Log;->secV(Ljava/lang/String;Ljava/lang/String;)I

    .line 193
    const-wide/16 v14, 0x0

    .line 194
    .local v14, "contact_id":J
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/providers/contacts/LegacyMyProfileImporter;->mTargetDb:Landroid/database/sqlite/SQLiteDatabase;

    const-string v5, "contacts"

    const/4 v6, 0x1

    new-array v6, v6, [Ljava/lang/String;

    const/4 v7, 0x0

    const-string v8, "_id"

    aput-object v8, v6, v7

    const/4 v7, 0x0

    const/4 v8, 0x0

    const/4 v9, 0x0

    const/4 v10, 0x0

    const-string v11, "_id limit 1"

    invoke-virtual/range {v4 .. v11}, Landroid/database/sqlite/SQLiteDatabase;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v13

    .line 197
    .local v13, "cur":Landroid/database/Cursor;
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/providers/contacts/LegacyMyProfileImporter;->mTargetDb:Landroid/database/sqlite/SQLiteDatabase;

    const-string v5, "UPDATE raw_contacts SET contact_id=? WHERE _id=?"

    invoke-virtual {v4, v5}, Landroid/database/sqlite/SQLiteDatabase;->compileStatement(Ljava/lang/String;)Landroid/database/sqlite/SQLiteStatement;

    move-result-object v18

    .line 199
    .local v18, "rawcontactUpdate":Landroid/database/sqlite/SQLiteStatement;
    :try_start_0
    invoke-interface {v13}, Landroid/database/Cursor;->moveToNext()Z

    move-result v4

    if-eqz v4, :cond_0

    .line 200
    const/4 v4, 0x0

    invoke-interface {v13, v4}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v14

    .line 201
    const/4 v4, 0x1

    move-object/from16 v0, v18

    invoke-virtual {v0, v4, v14, v15}, Landroid/database/sqlite/SQLiteStatement;->bindLong(IJ)V

    .line 202
    const/4 v4, 0x2

    move-object/from16 v0, v18

    move-wide/from16 v1, p1

    invoke-virtual {v0, v4, v1, v2}, Landroid/database/sqlite/SQLiteStatement;->bindLong(IJ)V

    .line 203
    invoke-virtual/range {v18 .. v18}, Landroid/database/sqlite/SQLiteStatement;->execute()V

    .line 204
    invoke-virtual/range {v18 .. v18}, Landroid/database/sqlite/SQLiteStatement;->close()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 208
    invoke-interface {v13}, Landroid/database/Cursor;->close()V

    move-wide/from16 v16, v14

    .line 225
    .end local v14    # "contact_id":J
    .local v16, "contact_id":J
    :goto_0
    return-wide v16

    .line 208
    .end local v16    # "contact_id":J
    .restart local v14    # "contact_id":J
    :cond_0
    invoke-interface {v13}, Landroid/database/Cursor;->close()V

    .line 212
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/providers/contacts/LegacyMyProfileImporter;->mTargetDb:Landroid/database/sqlite/SQLiteDatabase;

    const-string v5, "INSERT INTO contacts(name_raw_contact_id) VALUES(?)"

    invoke-virtual {v4, v5}, Landroid/database/sqlite/SQLiteDatabase;->compileStatement(Ljava/lang/String;)Landroid/database/sqlite/SQLiteStatement;

    move-result-object v12

    .line 214
    .local v12, "contactInsert":Landroid/database/sqlite/SQLiteStatement;
    const/4 v4, 0x1

    :try_start_1
    move-wide/from16 v0, p1

    invoke-virtual {v12, v4, v0, v1}, Landroid/database/sqlite/SQLiteStatement;->bindLong(IJ)V

    .line 215
    move-object/from16 v0, p0

    invoke-direct {v0, v12}, Lcom/android/providers/contacts/LegacyMyProfileImporter;->insert(Landroid/database/sqlite/SQLiteStatement;)J

    move-result-wide v14

    .line 216
    const/4 v4, 0x1

    move-object/from16 v0, v18

    invoke-virtual {v0, v4, v14, v15}, Landroid/database/sqlite/SQLiteStatement;->bindLong(IJ)V

    .line 217
    const/4 v4, 0x2

    move-object/from16 v0, v18

    move-wide/from16 v1, p1

    invoke-virtual {v0, v4, v1, v2}, Landroid/database/sqlite/SQLiteStatement;->bindLong(IJ)V

    .line 218
    invoke-virtual/range {v18 .. v18}, Landroid/database/sqlite/SQLiteStatement;->execute()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 220
    invoke-virtual {v12}, Landroid/database/sqlite/SQLiteStatement;->close()V

    .line 221
    invoke-virtual/range {v18 .. v18}, Landroid/database/sqlite/SQLiteStatement;->close()V

    move-wide/from16 v16, v14

    .line 225
    .end local v14    # "contact_id":J
    .restart local v16    # "contact_id":J
    goto :goto_0

    .line 208
    .end local v12    # "contactInsert":Landroid/database/sqlite/SQLiteStatement;
    .end local v16    # "contact_id":J
    .restart local v14    # "contact_id":J
    :catchall_0
    move-exception v4

    invoke-interface {v13}, Landroid/database/Cursor;->close()V

    throw v4

    .line 220
    .restart local v12    # "contactInsert":Landroid/database/sqlite/SQLiteStatement;
    :catchall_1
    move-exception v4

    invoke-virtual {v12}, Landroid/database/sqlite/SQLiteStatement;->close()V

    .line 221
    invoke-virtual/range {v18 .. v18}, Landroid/database/sqlite/SQLiteStatement;->close()V

    throw v4
.end method

.method private insertMyProfileData(Landroid/database/sqlite/SQLiteStatement;Landroid/database/Cursor;JJ)V
    .locals 23
    .param p1, "dataInsert"    # Landroid/database/sqlite/SQLiteStatement;
    .param p2, "c"    # Landroid/database/Cursor;
    .param p3, "rawContactId"    # J
    .param p5, "contactId"    # J

    .prologue
    .line 234
    const-string v4, "LegacyMyProfileImporter"

    const-string v5, "insertMyProfileData"

    invoke-static {v4, v5}, Landroid/util/secutil/Log;->secV(Ljava/lang/String;Ljava/lang/String;)I

    .line 235
    const/4 v10, 0x0

    .line 236
    .local v10, "number":Ljava/lang/String;
    const/4 v11, 0x0

    .line 237
    .local v11, "numberE164":Ljava/lang/String;
    const/16 v4, 0xf

    move-object/from16 v0, p2

    invoke-interface {v0, v4}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v19

    .line 238
    .local v19, "mimetype":Ljava/lang/String;
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/providers/contacts/LegacyMyProfileImporter;->mProfileHelper:Lcom/android/providers/contacts/ProfileDatabaseHelper;

    move-object/from16 v0, v19

    invoke-virtual {v4, v0}, Lcom/android/providers/contacts/ProfileDatabaseHelper;->getMimeTypeId(Ljava/lang/String;)J

    move-result-wide v20

    .line 239
    .local v20, "mimetype_id":J
    const/16 v4, 0x10

    move-object/from16 v0, p1

    move-wide/from16 v1, v20

    invoke-virtual {v0, v4, v1, v2}, Landroid/database/sqlite/SQLiteStatement;->bindLong(IJ)V

    .line 240
    const/16 v4, 0x11

    move-object/from16 v0, p1

    move-wide/from16 v1, p3

    invoke-virtual {v0, v4, v1, v2}, Landroid/database/sqlite/SQLiteStatement;->bindLong(IJ)V

    .line 241
    const/16 v4, 0x12

    const/16 v5, 0x10

    move-object/from16 v0, p2

    invoke-interface {v0, v5}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v6

    move-object/from16 v0, p1

    invoke-virtual {v0, v4, v6, v7}, Landroid/database/sqlite/SQLiteStatement;->bindLong(IJ)V

    .line 242
    const/16 v4, 0x13

    const/16 v5, 0x11

    move-object/from16 v0, p2

    invoke-interface {v0, v5}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v6

    move-object/from16 v0, p1

    invoke-virtual {v0, v4, v6, v7}, Landroid/database/sqlite/SQLiteStatement;->bindLong(IJ)V

    .line 244
    const-string v4, "vnd.android.cursor.item/phone_v2"

    move-object/from16 v0, v19

    invoke-virtual {v4, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_2

    .line 247
    const/4 v4, 0x0

    move-object/from16 v0, p2

    invoke-interface {v0, v4}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v10

    .line 248
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/providers/contacts/LegacyMyProfileImporter;->mProfileHelper:Lcom/android/providers/contacts/ProfileDatabaseHelper;

    invoke-virtual {v4}, Lcom/android/providers/contacts/ProfileDatabaseHelper;->getCurrentCountryIso()Ljava/lang/String;

    move-result-object v4

    invoke-static {v10, v4}, Landroid/telephony/PhoneNumberUtils;->formatNumberToE164(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v11

    .line 250
    move-object/from16 v0, p0

    move-object/from16 v1, p1

    move-object/from16 v2, p2

    invoke-direct {v0, v1, v2, v10, v11}, Lcom/android/providers/contacts/LegacyMyProfileImporter;->bindPhoneNumberData(Landroid/database/sqlite/SQLiteStatement;Landroid/database/Cursor;Ljava/lang/String;Ljava/lang/String;)V

    .line 265
    :cond_0
    :goto_0
    invoke-direct/range {p0 .. p1}, Lcom/android/providers/contacts/LegacyMyProfileImporter;->insert(Landroid/database/sqlite/SQLiteStatement;)J

    move-result-wide v8

    .line 268
    .local v8, "dataId":J
    const-string v4, "vnd.android.cursor.item/name"

    move-object/from16 v0, v19

    invoke-virtual {v4, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_5

    move-object/from16 v4, p0

    move-object/from16 v5, p2

    move-wide/from16 v6, p3

    .line 269
    invoke-direct/range {v4 .. v9}, Lcom/android/providers/contacts/LegacyMyProfileImporter;->insertProfileNameLookup(Landroid/database/Cursor;JJ)V

    .line 284
    :cond_1
    :goto_1
    return-void

    .line 251
    .end local v8    # "dataId":J
    :cond_2
    const-string v4, "vnd.android.cursor.item/photo"

    move-object/from16 v0, v19

    invoke-virtual {v4, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_4

    .line 252
    const/4 v12, 0x1

    .local v12, "i":I
    :goto_2
    const/16 v4, 0xf

    if-ge v12, v4, :cond_3

    .line 253
    add-int/lit8 v4, v12, -0x1

    move-object/from16 v0, p2

    invoke-interface {v0, v4}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v4

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    invoke-direct {v0, v1, v12, v4}, Lcom/android/providers/contacts/LegacyMyProfileImporter;->bindString(Landroid/database/sqlite/SQLiteStatement;ILjava/lang/String;)V

    .line 252
    add-int/lit8 v12, v12, 0x1

    goto :goto_2

    .line 255
    :cond_3
    const/16 v4, 0xf

    const/16 v5, 0xe

    move-object/from16 v0, p2

    invoke-interface {v0, v5}, Landroid/database/Cursor;->getBlob(I)[B

    move-result-object v5

    move-object/from16 v0, p1

    invoke-virtual {v0, v4, v5}, Landroid/database/sqlite/SQLiteStatement;->bindBlob(I[B)V

    goto :goto_0

    .line 260
    .end local v12    # "i":I
    :cond_4
    const/4 v12, 0x1

    .restart local v12    # "i":I
    :goto_3
    const/16 v4, 0x10

    if-ge v12, v4, :cond_0

    .line 261
    add-int/lit8 v4, v12, -0x1

    move-object/from16 v0, p2

    invoke-interface {v0, v4}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v4

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    invoke-direct {v0, v1, v12, v4}, Lcom/android/providers/contacts/LegacyMyProfileImporter;->bindString(Landroid/database/sqlite/SQLiteStatement;ILjava/lang/String;)V

    .line 260
    add-int/lit8 v12, v12, 0x1

    goto :goto_3

    .line 270
    .end local v12    # "i":I
    .restart local v8    # "dataId":J
    :cond_5
    const-string v4, "vnd.android.cursor.item/phone_v2"

    move-object/from16 v0, v19

    invoke-virtual {v4, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_6

    move-object/from16 v4, p0

    move-object/from16 v5, p2

    move-wide/from16 v6, p3

    .line 271
    invoke-direct/range {v4 .. v11}, Lcom/android/providers/contacts/LegacyMyProfileImporter;->insertPhoneNumberLookup(Landroid/database/Cursor;JJLjava/lang/String;Ljava/lang/String;)V

    goto :goto_1

    .line 272
    :cond_6
    const-string v4, "vnd.android.cursor.item/email_v2"

    move-object/from16 v0, v19

    invoke-virtual {v4, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_7

    .line 274
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/android/providers/contacts/LegacyMyProfileImporter;->mProfileHelper:Lcom/android/providers/contacts/ProfileDatabaseHelper;

    const/4 v4, 0x0

    move-object/from16 v0, p2

    invoke-interface {v0, v4}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v18

    move-wide/from16 v14, p3

    move-wide/from16 v16, v8

    invoke-virtual/range {v13 .. v18}, Lcom/android/providers/contacts/ProfileDatabaseHelper;->insertNameLookupForEmail(JJLjava/lang/String;)Ljava/lang/String;

    goto :goto_1

    .line 276
    :cond_7
    const-string v4, "vnd.android.cursor.item/photo"

    move-object/from16 v0, v19

    invoke-virtual {v4, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_1

    .line 277
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/providers/contacts/LegacyMyProfileImporter;->mTargetDb:Landroid/database/sqlite/SQLiteDatabase;

    const-string v5, "UPDATE contacts SET photo_id=? WHERE _id=?"

    invoke-virtual {v4, v5}, Landroid/database/sqlite/SQLiteDatabase;->compileStatement(Ljava/lang/String;)Landroid/database/sqlite/SQLiteStatement;

    move-result-object v22

    .line 278
    .local v22, "photoIdUpdate":Landroid/database/sqlite/SQLiteStatement;
    const/4 v4, 0x1

    move-object/from16 v0, v22

    invoke-virtual {v0, v4, v8, v9}, Landroid/database/sqlite/SQLiteStatement;->bindLong(IJ)V

    .line 279
    const/4 v4, 0x2

    move-object/from16 v0, v22

    move-wide/from16 v1, p5

    invoke-virtual {v0, v4, v1, v2}, Landroid/database/sqlite/SQLiteStatement;->bindLong(IJ)V

    .line 280
    invoke-virtual/range {v22 .. v22}, Landroid/database/sqlite/SQLiteStatement;->execute()V

    .line 281
    invoke-virtual/range {v22 .. v22}, Landroid/database/sqlite/SQLiteStatement;->close()V

    goto/16 :goto_1
.end method

.method private insertPhoneNumberLookup(Landroid/database/Cursor;JJLjava/lang/String;Ljava/lang/String;)V
    .locals 8
    .param p1, "c"    # Landroid/database/Cursor;
    .param p2, "rawContactId"    # J
    .param p4, "dataId"    # J
    .param p6, "number"    # Ljava/lang/String;
    .param p7, "numberE164"    # Ljava/lang/String;

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    .line 322
    if-eqz p6, :cond_1

    .line 323
    iget-object v2, p0, Lcom/android/providers/contacts/LegacyMyProfileImporter;->mTargetDb:Landroid/database/sqlite/SQLiteDatabase;

    const-string v3, "INSERT INTO phone_lookup(raw_contact_id,data_id,normalized_number,min_match) VALUES (?,?,?,?)"

    invoke-virtual {v2, v3}, Landroid/database/sqlite/SQLiteDatabase;->compileStatement(Ljava/lang/String;)Landroid/database/sqlite/SQLiteStatement;

    move-result-object v1

    .line 325
    .local v1, "phonelookupInsert":Landroid/database/sqlite/SQLiteStatement;
    invoke-static {p6}, Landroid/telephony/PhoneNumberUtils;->normalizeNumber(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 326
    .local v0, "normalizedNumber":Ljava/lang/String;
    if-eqz v1, :cond_1

    .line 327
    invoke-virtual {v1, v4, p2, p3}, Landroid/database/sqlite/SQLiteStatement;->bindLong(IJ)V

    .line 328
    invoke-virtual {v1, v5, p4, p5}, Landroid/database/sqlite/SQLiteStatement;->bindLong(IJ)V

    .line 329
    invoke-virtual {v1, v6, v0}, Landroid/database/sqlite/SQLiteStatement;->bindString(ILjava/lang/String;)V

    .line 330
    invoke-static {p6}, Landroid/telephony/PhoneNumberUtils;->toCallerIDMinMatch(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v7, v2}, Landroid/database/sqlite/SQLiteStatement;->bindString(ILjava/lang/String;)V

    .line 332
    invoke-direct {p0, v1}, Lcom/android/providers/contacts/LegacyMyProfileImporter;->insert(Landroid/database/sqlite/SQLiteStatement;)J

    .line 334
    if-eqz p7, :cond_0

    .line 335
    invoke-virtual {v1}, Landroid/database/sqlite/SQLiteStatement;->clearBindings()V

    .line 336
    invoke-virtual {v1, v4, p2, p3}, Landroid/database/sqlite/SQLiteStatement;->bindLong(IJ)V

    .line 337
    invoke-virtual {v1, v5, p4, p5}, Landroid/database/sqlite/SQLiteStatement;->bindLong(IJ)V

    .line 338
    invoke-virtual {v1, v6, v0}, Landroid/database/sqlite/SQLiteStatement;->bindString(ILjava/lang/String;)V

    .line 339
    invoke-static {p6}, Landroid/telephony/PhoneNumberUtils;->toCallerIDMinMatch(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v7, v2}, Landroid/database/sqlite/SQLiteStatement;->bindString(ILjava/lang/String;)V

    .line 341
    invoke-direct {p0, v1}, Lcom/android/providers/contacts/LegacyMyProfileImporter;->insert(Landroid/database/sqlite/SQLiteStatement;)J

    .line 344
    :cond_0
    invoke-virtual {v1}, Landroid/database/sqlite/SQLiteStatement;->close()V

    .line 348
    .end local v0    # "normalizedNumber":Ljava/lang/String;
    .end local v1    # "phonelookupInsert":Landroid/database/sqlite/SQLiteStatement;
    :cond_1
    return-void
.end method

.method private insertProfileNameLookup(Landroid/database/Cursor;JJ)V
    .locals 8
    .param p1, "c"    # Landroid/database/Cursor;
    .param p2, "rawContactId"    # J
    .param p4, "dataId"    # J

    .prologue
    .line 307
    const/4 v0, 0x0

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v6

    .line 312
    .local v6, "displayName":Ljava/lang/String;
    iget-object v0, p0, Lcom/android/providers/contacts/LegacyMyProfileImporter;->mProfileHelper:Lcom/android/providers/contacts/ProfileDatabaseHelper;

    iget-object v1, p0, Lcom/android/providers/contacts/LegacyMyProfileImporter;->mTargetDb:Landroid/database/sqlite/SQLiteDatabase;

    move-wide v2, p2

    move-wide v4, p4

    invoke-virtual/range {v0 .. v6}, Lcom/android/providers/contacts/ProfileDatabaseHelper;->insertStructuredNameLookup(Landroid/database/sqlite/SQLiteDatabase;JJLjava/lang/String;)V

    .line 318
    return-void
.end method

.method private insertRawContacts()J
    .locals 5

    .prologue
    .line 356
    iget-object v3, p0, Lcom/android/providers/contacts/LegacyMyProfileImporter;->mTargetDb:Landroid/database/sqlite/SQLiteDatabase;

    const-string v4, "INSERT INTO raw_contacts(account_id) VALUES (?)"

    invoke-virtual {v3, v4}, Landroid/database/sqlite/SQLiteDatabase;->compileStatement(Ljava/lang/String;)Landroid/database/sqlite/SQLiteStatement;

    move-result-object v2

    .line 357
    .local v2, "rawContactInsert":Landroid/database/sqlite/SQLiteStatement;
    const/4 v3, 0x1

    invoke-direct {p0}, Lcom/android/providers/contacts/LegacyMyProfileImporter;->resovleAccountId()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, Landroid/database/sqlite/SQLiteStatement;->bindString(ILjava/lang/String;)V

    .line 358
    invoke-direct {p0, v2}, Lcom/android/providers/contacts/LegacyMyProfileImporter;->insert(Landroid/database/sqlite/SQLiteStatement;)J

    move-result-wide v0

    .line 359
    .local v0, "rawContactId":J
    invoke-virtual {v2}, Landroid/database/sqlite/SQLiteStatement;->close()V

    .line 360
    return-wide v0
.end method

.method private resovleAccountId()Ljava/lang/String;
    .locals 6

    .prologue
    .line 567
    sget-wide v2, Lcom/android/providers/contacts/LegacyMyProfileImporter;->mRawContactsAccountId:J

    const-wide/16 v4, 0x0

    cmp-long v1, v2, v4

    if-gez v1, :cond_0

    .line 568
    const-string v1, "vnd.sec.contact.phone"

    const-string v2, "vnd.sec.contact.phone"

    const/4 v3, 0x0

    invoke-static {v1, v2, v3}, Lcom/android/providers/contacts/AccountWithDataSet;->get(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/android/providers/contacts/AccountWithDataSet;

    move-result-object v0

    .line 569
    .local v0, "accountWithDataSet":Lcom/android/providers/contacts/AccountWithDataSet;
    iget-object v1, p0, Lcom/android/providers/contacts/LegacyMyProfileImporter;->mProfileHelper:Lcom/android/providers/contacts/ProfileDatabaseHelper;

    invoke-virtual {v1, v0}, Lcom/android/providers/contacts/ProfileDatabaseHelper;->getOrCreateAccountIdInTransaction(Lcom/android/providers/contacts/AccountWithDataSet;)J

    move-result-wide v2

    sput-wide v2, Lcom/android/providers/contacts/LegacyMyProfileImporter;->mRawContactsAccountId:J

    .line 571
    .end local v0    # "accountWithDataSet":Lcom/android/providers/contacts/AccountWithDataSet;
    :cond_0
    sget-wide v2, Lcom/android/providers/contacts/LegacyMyProfileImporter;->mRawContactsAccountId:J

    invoke-static {v2, v3}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v1

    return-object v1
.end method

.method private updateContactHasPhoneHasEmailLinkTypes(JJ)V
    .locals 27
    .param p1, "rawContactId"    # J
    .param p3, "contactId"    # J

    .prologue
    .line 497
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/providers/contacts/LegacyMyProfileImporter;->mTargetDb:Landroid/database/sqlite/SQLiteDatabase;

    const-string v5, "UPDATE contacts SET has_phone_number =  (CASE WHEN (SELECT COUNT(*) FROM data  JOIN mimetypes ON (data.mimetype_id = mimetypes._id AND mimetype = \'vnd.android.cursor.item/phone_v2\')  JOIN raw_contacts ON (data.raw_contact_id = raw_contacts._id AND raw_contacts.contact_id = contacts._id)) > 0 THEN 1 ELSE 0 END) ,has_email =  (CASE WHEN (SELECT COUNT(*) FROM data  JOIN mimetypes ON (data.mimetype_id = mimetypes._id AND mimetype = \'vnd.android.cursor.item/email_v2\')  JOIN raw_contacts ON (data.raw_contact_id = raw_contacts._id AND raw_contacts.contact_id = contacts._id)) > 0 THEN 1 ELSE 0 END) WHERE _id=?"

    invoke-virtual {v4, v5}, Landroid/database/sqlite/SQLiteDatabase;->compileStatement(Ljava/lang/String;)Landroid/database/sqlite/SQLiteStatement;

    move-result-object v25

    .line 499
    .local v25, "updateHasPhoneHasEmailContact":Landroid/database/sqlite/SQLiteStatement;
    const/4 v4, 0x1

    move-object/from16 v0, v25

    move-wide/from16 v1, p3

    invoke-virtual {v0, v4, v1, v2}, Landroid/database/sqlite/SQLiteStatement;->bindLong(IJ)V

    .line 500
    invoke-virtual/range {v25 .. v25}, Landroid/database/sqlite/SQLiteStatement;->execute()V

    .line 501
    invoke-virtual/range {v25 .. v25}, Landroid/database/sqlite/SQLiteStatement;->close()V

    .line 504
    const-string v23, "contact_id=? "

    .line 505
    .local v23, "selection":Ljava/lang/String;
    const/4 v4, 0x1

    new-array v8, v4, [Ljava/lang/String;

    const/4 v4, 0x0

    invoke-static/range {p3 .. p4}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v5

    aput-object v5, v8, v4

    .line 508
    .local v8, "selectionArgs":[Ljava/lang/String;
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/providers/contacts/LegacyMyProfileImporter;->mTargetDb:Landroid/database/sqlite/SQLiteDatabase;

    const-string v5, "view_raw_contacts"

    const/4 v6, 0x3

    new-array v6, v6, [Ljava/lang/String;

    const/4 v7, 0x0

    const-string v9, "_id"

    aput-object v9, v6, v7

    const/4 v7, 0x1

    const-string v9, "account_type"

    aput-object v9, v6, v7

    const/4 v7, 0x2

    const-string v9, "data_set"

    aput-object v9, v6, v7

    const-string v7, "contact_id=? "

    const/4 v9, 0x0

    const/4 v10, 0x0

    const/4 v11, 0x0

    invoke-virtual/range {v4 .. v11}, Landroid/database/sqlite/SQLiteDatabase;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v14

    .line 511
    .local v14, "c":Landroid/database/Cursor;
    const/16 v22, 0x5

    .line 512
    .local v22, "max_link_count":I
    const/16 v20, 0x0

    .line 513
    .local v20, "link_count":I
    const/4 v13, 0x0

    .line 514
    .local v13, "account_type":Ljava/lang/String;
    const/4 v15, 0x0

    .line 515
    .local v15, "data_set":Ljava/lang/String;
    const/4 v4, 0x5

    new-array v0, v4, [J

    move-object/from16 v21, v0

    .line 516
    .local v21, "link_raw_contact_id":[J
    const/4 v4, 0x5

    new-array v0, v4, [Ljava/lang/String;

    move-object/from16 v19, v0

    .line 519
    .local v19, "link_account_type_and_data_set":[Ljava/lang/String;
    :goto_0
    :try_start_0
    invoke-interface {v14}, Landroid/database/Cursor;->moveToNext()Z

    move-result v4

    if-eqz v4, :cond_1

    .line 520
    const/4 v4, 0x0

    invoke-interface {v14, v4}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v4

    aput-wide v4, v21, v20

    .line 521
    const/4 v4, 0x1

    invoke-interface {v14, v4}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v13

    .line 522
    const/4 v4, 0x2

    invoke-interface {v14, v4}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v15

    .line 523
    invoke-static {v15}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-nez v4, :cond_0

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v4, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "/"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v12

    .line 526
    .local v12, "accountWithDataSet":Ljava/lang/String;
    :goto_1
    aput-object v12, v19, v20
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 527
    add-int/lit8 v20, v20, 0x1

    .line 528
    goto :goto_0

    .end local v12    # "accountWithDataSet":Ljava/lang/String;
    :cond_0
    move-object v12, v13

    .line 523
    goto :goto_1

    .line 530
    :cond_1
    invoke-interface {v14}, Landroid/database/Cursor;->close()V

    .line 533
    if-lez v20, :cond_3

    .line 534
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/providers/contacts/LegacyMyProfileImporter;->mTargetDb:Landroid/database/sqlite/SQLiteDatabase;

    const-string v5, "UPDATE contacts SET link_count = ?, link_type1 = ?, raw_contact_linkpriority1 = ? ,link_type2 = ?, raw_contact_linkpriority2 = ? ,link_type3 = ?, raw_contact_linkpriority3 = ? ,link_type4 = ?, raw_contact_linkpriority4 = ? ,link_type5 = ?, raw_contact_linkpriority5 = ?  WHERE _id=?"

    invoke-virtual {v4, v5}, Landroid/database/sqlite/SQLiteDatabase;->compileStatement(Ljava/lang/String;)Landroid/database/sqlite/SQLiteStatement;

    move-result-object v24

    .line 536
    .local v24, "updateContactLink":Landroid/database/sqlite/SQLiteStatement;
    const/4 v4, 0x1

    move/from16 v0, v20

    int-to-long v6, v0

    move-object/from16 v0, v24

    invoke-virtual {v0, v4, v6, v7}, Landroid/database/sqlite/SQLiteStatement;->bindLong(IJ)V

    .line 537
    const/16 v16, 0x0

    .local v16, "i":I
    const/16 v17, 0x2

    .local v17, "j":I
    move/from16 v18, v17

    .end local v17    # "j":I
    .local v18, "j":I
    :goto_2
    const/4 v4, 0x5

    move/from16 v0, v16

    if-ge v0, v4, :cond_2

    .line 538
    add-int/lit8 v17, v18, 0x1

    .end local v18    # "j":I
    .restart local v17    # "j":I
    aget-object v4, v19, v16

    move-object/from16 v0, p0

    move-object/from16 v1, v24

    move/from16 v2, v18

    invoke-direct {v0, v1, v2, v4}, Lcom/android/providers/contacts/LegacyMyProfileImporter;->bindString(Landroid/database/sqlite/SQLiteStatement;ILjava/lang/String;)V

    .line 539
    add-int/lit8 v18, v17, 0x1

    .end local v17    # "j":I
    .restart local v18    # "j":I
    aget-wide v4, v21, v16

    move-object/from16 v0, v24

    move/from16 v1, v17

    invoke-virtual {v0, v1, v4, v5}, Landroid/database/sqlite/SQLiteStatement;->bindLong(IJ)V

    .line 537
    add-int/lit8 v16, v16, 0x1

    goto :goto_2

    .line 530
    .end local v16    # "i":I
    .end local v18    # "j":I
    .end local v24    # "updateContactLink":Landroid/database/sqlite/SQLiteStatement;
    :catchall_0
    move-exception v4

    invoke-interface {v14}, Landroid/database/Cursor;->close()V

    throw v4

    .line 542
    .restart local v16    # "i":I
    .restart local v18    # "j":I
    .restart local v24    # "updateContactLink":Landroid/database/sqlite/SQLiteStatement;
    :cond_2
    const/16 v4, 0xc

    move-object/from16 v0, v24

    move-wide/from16 v1, p3

    invoke-virtual {v0, v4, v1, v2}, Landroid/database/sqlite/SQLiteStatement;->bindLong(IJ)V

    .line 543
    invoke-virtual/range {v24 .. v24}, Landroid/database/sqlite/SQLiteStatement;->execute()V

    .line 544
    invoke-virtual/range {v24 .. v24}, Landroid/database/sqlite/SQLiteStatement;->close()V

    .line 548
    .end local v16    # "i":I
    .end local v18    # "j":I
    .end local v24    # "updateContactLink":Landroid/database/sqlite/SQLiteStatement;
    :cond_3
    return-void
.end method

.method private updateLookupKeyForContact(J)V
    .locals 3
    .param p1, "contactId"    # J

    .prologue
    .line 480
    iget-object v1, p0, Lcom/android/providers/contacts/LegacyMyProfileImporter;->mTargetDb:Landroid/database/sqlite/SQLiteDatabase;

    const-string v2, "UPDATE contacts SET lookup=?  WHERE _id=?"

    invoke-virtual {v1, v2}, Landroid/database/sqlite/SQLiteDatabase;->compileStatement(Ljava/lang/String;)Landroid/database/sqlite/SQLiteStatement;

    move-result-object v0

    .line 484
    .local v0, "lookupKeyUpdate":Landroid/database/sqlite/SQLiteStatement;
    const/4 v1, 0x1

    const-string v2, "profile"

    invoke-virtual {v0, v1, v2}, Landroid/database/sqlite/SQLiteStatement;->bindString(ILjava/lang/String;)V

    .line 485
    const/4 v1, 0x2

    invoke-virtual {v0, v1, p1, p2}, Landroid/database/sqlite/SQLiteStatement;->bindLong(IJ)V

    .line 486
    invoke-virtual {v0}, Landroid/database/sqlite/SQLiteStatement;->execute()V

    .line 487
    invoke-virtual {v0}, Landroid/database/sqlite/SQLiteStatement;->close()V

    .line 488
    return-void
.end method

.method private updateRawContactDisplayName(J)V
    .locals 3
    .param p1, "rawContactId"    # J

    .prologue
    .line 493
    iget-object v0, p0, Lcom/android/providers/contacts/LegacyMyProfileImporter;->mProfileHelper:Lcom/android/providers/contacts/ProfileDatabaseHelper;

    iget-object v1, p0, Lcom/android/providers/contacts/LegacyMyProfileImporter;->mTargetDb:Landroid/database/sqlite/SQLiteDatabase;

    invoke-virtual {v0, v1, p1, p2}, Lcom/android/providers/contacts/ProfileDatabaseHelper;->updateRawContactDisplayName(Landroid/database/sqlite/SQLiteDatabase;J)V

    .line 494
    return-void
.end method


# virtual methods
.method public importLegacyMyProfileData()Z
    .locals 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 112
    const-string v2, "LegacyMyProfileImporter"

    const-string v3, "Importing my profile contacts"

    invoke-static {v2, v3}, Landroid/util/secutil/Log;->secW(Ljava/lang/String;Ljava/lang/String;)I

    .line 114
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    const/4 v2, 0x5

    if-ge v1, v2, :cond_0

    .line 116
    :try_start_0
    invoke-direct {p0}, Lcom/android/providers/contacts/LegacyMyProfileImporter;->importMyProfielDataFromContactsDb()V
    :try_end_0
    .catch Landroid/database/sqlite/SQLiteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 118
    const/4 v2, 0x1

    .line 131
    :goto_1
    return v2

    .line 120
    :catch_0
    move-exception v0

    .line 121
    .local v0, "e":Landroid/database/sqlite/SQLiteException;
    const-string v2, "LegacyMyProfileImporter"

    const-string v3, "Database import exception. Will retry in 2000ms"

    invoke-static {v2, v3, v0}, Landroid/util/secutil/Log;->secE(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 126
    const-wide/16 v2, 0x7d0

    invoke-static {v2, v3}, Ljava/lang/Thread;->sleep(J)V

    .line 114
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 131
    .end local v0    # "e":Landroid/database/sqlite/SQLiteException;
    :cond_0
    const/4 v2, 0x0

    goto :goto_1
.end method
