.class public Lcom/sec/android/soagent/RegisterReceiver;
.super Landroid/content/BroadcastReceiver;
.source "llIIIIlllllIIllIIllI"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 19
    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    return-void
.end method

.method private llIIIIlllllIIllIIllI(Landroid/content/Intent;)LIIlIIIIIIllllIIlIIlI;
    .locals 2

    .prologue
    .line 133
    new-instance v0, LIIlIIIIIIllllIIlIIlI;

    invoke-direct {v0}, LIIlIIIIIIllllIIlIIlI;-><init>()V

    .line 134
    const-string v1, "type"

    invoke-virtual {p1, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, LIIlIIIIIIllllIIlIIlI;->llllIIIllIlIIIIllllI(Ljava/lang/String;)V

    .line 135
    const-string v1, "serialNumber"

    invoke-virtual {p1, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, LIIlIIIIIIllllIIlIIlI;->lllIlIlIIIllIIlIllIl(Ljava/lang/String;)V

    .line 136
    const-string v1, "modelName"

    invoke-virtual {p1, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, LIIlIIIIIIllllIIlIIlI;->llIlIIIIlIIIIIlIlIII(Ljava/lang/String;)V

    .line 137
    const-string v1, "mcc"

    invoke-virtual {p1, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, LIIlIIIIIIllllIIlIIlI;->IllIlIIIIlIIlIIIllIl(Ljava/lang/String;)V

    .line 138
    return-object v0
.end method

.method private llIIIIlllllIIllIIllI(Landroid/content/Context;Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 177
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/sec/android/soagent/RegisterService;

    invoke-direct {v0, p1, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 178
    const-string v1, "type"

    invoke-virtual {v0, v1, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 179
    const/16 v1, 0x20

    invoke-virtual {v0, v1}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    .line 180
    invoke-virtual {p1, v0}, Landroid/content/Context;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;

    .line 181
    return-void
.end method

.method private llIIIIlllllIIllIIllI(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 146
    const-string v0, "PHONE COVER"

    invoke-virtual {v0, p3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-virtual {p0, p1}, Lcom/sec/android/soagent/RegisterReceiver;->llIIIIlllllIIllIIllI(Landroid/content/Context;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 147
    const-string v0, "Wait cover reistering until Eula agreememnt"

    invoke-static {v0}, LllIlIIIIlIIIIIlIlIII;->lllIlIlIIIllIIlIllIl(Ljava/lang/String;)V

    .line 155
    :cond_0
    :goto_0
    return-void

    .line 151
    :cond_1
    const-string v0, "com.samsung.android.intent.action.ACCESSORY_SETUPWIZARD_COMPLETE"

    invoke-virtual {v0, p2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_2

    const-string v0, "sec.soagent.action.ACCESSORY_TIME"

    invoke-virtual {v0, p2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_2

    const-string v0, "android.net.conn.CONNECTIVITY_CHANGE"

    invoke-virtual {v0, p2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 153
    :cond_2
    invoke-direct {p0, p1, p3}, Lcom/sec/android/soagent/RegisterReceiver;->llIIIIlllllIIllIIllI(Landroid/content/Context;Ljava/lang/String;)V

    goto :goto_0
.end method


# virtual methods
.method protected llIIIIlllllIIllIIllI(Landroid/content/Context;)Z
    .locals 2

    .prologue
    const/4 v0, 0x0

    .line 161
    invoke-static {p1}, Lcom/sec/android/soagent/db/llllIIIllIlIIIIllllI;->llllIIIllIlIIIIllllI(Landroid/content/Context;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 162
    const-string v1, "Setup Wizard is not completed"

    invoke-static {v1}, LllIlIIIIlIIIIIlIlIII;->lllIlIlIIIllIIlIllIl(Ljava/lang/String;)V

    .line 170
    :goto_0
    return v0

    .line 166
    :cond_0
    invoke-static {p1}, Lcom/sec/android/soagent/db/llllIIIllIlIIIIllllI;->llIIIIlllllIIllIIllI(Landroid/content/Context;)Z

    move-result v1

    if-nez v1, :cond_1

    .line 167
    const-string v1, "EULA is not agreed"

    invoke-static {v1}, LllIlIIIIlIIIIIlIlIII;->lllIlIlIIIllIIlIllIl(Ljava/lang/String;)V

    goto :goto_0

    .line 170
    :cond_1
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 6

    .prologue
    .line 52
    if-nez p1, :cond_1

    .line 53
    const-string v0, "Content is null!!"

    invoke-static {v0}, LllIlIIIIlIIIIIlIlIII;->llIlIIIIlIIIIIlIlIII(Ljava/lang/String;)V

    .line 124
    :cond_0
    :goto_0
    return-void

    .line 57
    :cond_1
    if-nez p2, :cond_2

    .line 58
    const-string v0, "Intent is null!!"

    invoke-static {v0}, LllIlIIIIlIIIIIlIlIII;->llIlIIIIlIIIIIlIlIII(Ljava/lang/String;)V

    goto :goto_0

    .line 62
    :cond_2
    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v1

    .line 63
    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 64
    const-string v0, "Action is null!!"

    invoke-static {v0}, LllIlIIIIlIIIIIlIlIII;->llIlIIIIlIIIIIlIlIII(Ljava/lang/String;)V

    goto :goto_0

    .line 68
    :cond_3
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Receive broadcast meassage:"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, LllIlIIIIlIIIIIlIlIII;->llllIIIllIlIIIIllllI(Ljava/lang/String;)V

    .line 71
    const-string v0, "com.samsung.android.intent.action.ACCESSORY_SETUPWIZARD_COMPLETE"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_5

    .line 72
    const-string v0, "eula_agree"

    const/4 v2, 0x0

    invoke-virtual {p2, v0, v2}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v0

    if-nez v0, :cond_4

    .line 73
    const-string v0, "Accessory EULA is not agreed"

    invoke-static {v0}, LllIlIIIIlIIIIIlIlIII;->lllIlIlIIIllIIlIllIl(Ljava/lang/String;)V

    goto :goto_0

    .line 77
    :cond_4
    invoke-direct {p0, p2}, Lcom/sec/android/soagent/RegisterReceiver;->llIIIIlllllIIllIIllI(Landroid/content/Intent;)LIIlIIIIIIllllIIlIIlI;

    move-result-object v0

    .line 78
    invoke-virtual {v0}, LIIlIIIIIIllllIIlIIlI;->llIlIIIIlIIIIIlIlIII()V

    .line 80
    invoke-static {p1, v0}, LlllIIIIllIlIlllllllI;->lllIlIlIIIllIIlIllIl(Landroid/content/Context;LIIlIIIIIIllllIIlIIlI;)Z

    move-result v2

    if-eqz v2, :cond_5

    .line 81
    const-string v2, "Update Accessory DB"

    invoke-static {v2}, LllIlIIIIlIIIIIlIlIII;->lllIlIlIIIllIIlIllIl(Ljava/lang/String;)V

    .line 82
    invoke-static {p1, v0}, LlllIIIIllIlIlllllllI;->llIIIIlllllIIllIIllI(Landroid/content/Context;LIIlIIIIIIllllIIlIIlI;)Z

    .line 84
    invoke-static {p1}, Lcom/sec/android/soagent/db/llIIIIlllllIIllIIllI;->llIIIIlllllIIllIIllI(Landroid/content/Context;)J

    move-result-wide v2

    const-wide/16 v4, 0x0

    cmp-long v0, v2, v4

    if-nez v0, :cond_5

    .line 85
    invoke-static {p1}, LlllIIIIllIlIlllllllI;->llllIIIllIlIIIIllllI(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_5

    .line 86
    invoke-static {p1}, LlllIIIIllIlIlllllllI;->lllIlIlIIIllIIlIllIl(Landroid/content/Context;)J

    move-result-wide v2

    invoke-static {p1, v2, v3}, LlllIIIIllIlIlllllllI;->llIIIIlllllIIllIIllI(Landroid/content/Context;J)V

    .line 94
    :cond_5
    const-string v0, "android.net.conn.CONNECTIVITY_CHANGE"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_7

    .line 95
    const-string v0, "networkInfo"

    invoke-virtual {p2, v0}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Landroid/net/NetworkInfo;

    .line 96
    if-eqz v0, :cond_6

    sget-object v2, Landroid/net/NetworkInfo$DetailedState;->CONNECTED:Landroid/net/NetworkInfo$DetailedState;

    invoke-virtual {v0}, Landroid/net/NetworkInfo;->getDetailedState()Landroid/net/NetworkInfo$DetailedState;

    move-result-object v0

    invoke-virtual {v2, v0}, Landroid/net/NetworkInfo$DetailedState;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_7

    .line 98
    :cond_6
    const-string v0, "Network is not available"

    invoke-static {v0}, LllIlIIIIlIIIIIlIlIII;->lllIlIlIIIllIIlIllIl(Ljava/lang/String;)V

    goto/16 :goto_0

    .line 104
    :cond_7
    const-string v0, "Get Accessory List"

    invoke-static {v0}, LllIlIIIIlIIIIIlIlIII;->lllIlIlIIIllIIlIllIl(Ljava/lang/String;)V

    .line 105
    invoke-static {p1}, LlllIIIIllIlIlllllllI;->llIIIIlllllIIllIIllI(Landroid/content/Context;)Ljava/util/ArrayList;

    move-result-object v0

    .line 107
    invoke-static {v0}, LlllIIIIllIlIlllllllI;->llIIIIlllllIIllIIllI(Ljava/util/ArrayList;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 108
    const-string v2, "Need to device registering..."

    invoke-static {v2}, LllIlIIIIlIIIIIlIlIII;->lllIlIlIIIllIIlIllIl(Ljava/lang/String;)V

    .line 109
    invoke-static {p1}, LlllIIIIllIlIlllllllI;->llIlIIIIlIIIIIlIlIII(Landroid/content/Context;)Z

    move-result v2

    if-eqz v2, :cond_8

    .line 110
    const-string v0, "Wait until registration time is passed"

    invoke-static {v0}, LllIlIIIIlIIIIIlIlIII;->lllIlIlIIIllIIlIllIl(Ljava/lang/String;)V

    .line 111
    const-string v0, "android.intent.action.BOOT_COMPLETED"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 112
    invoke-static {p1}, LlllIIIIllIlIlllllllI;->lllIlIlIIIllIIlIllIl(Landroid/content/Context;)J

    move-result-wide v0

    invoke-static {p1, v0, v1}, LlllIIIIllIlIlllllllI;->llIIIIlllllIIllIIllI(Landroid/content/Context;J)V

    goto/16 :goto_0

    .line 116
    :cond_8
    const-string v2, "Start to register accessory"

    invoke-static {v2}, LllIlIIIIlIIIIIlIlIII;->lllIlIlIIIllIIlIllIl(Ljava/lang/String;)V

    .line 117
    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_9
    :goto_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LIIlIIIIIIllllIIlIIlI;

    .line 118
    invoke-virtual {v0}, LIIlIIIIIIllllIIlIIlI;->lllIlIlIIIllIIlIllIl()Z

    move-result v3

    if-eqz v3, :cond_9

    .line 119
    invoke-virtual {v0}, LIIlIIIIIIllllIIlIIlI;->IIIIllIlIIlIIIIlllIl()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, p1, v1, v0}, Lcom/sec/android/soagent/RegisterReceiver;->llIIIIlllllIIllIIllI(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1
.end method
