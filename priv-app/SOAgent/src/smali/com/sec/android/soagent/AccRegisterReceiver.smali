.class public Lcom/sec/android/soagent/AccRegisterReceiver;
.super Landroid/content/BroadcastReceiver;
.source "llIIIIlllllIIllIIllI"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 14
    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    return-void
.end method

.method private llIIIIlllllIIllIIllI(Landroid/content/Context;Ljava/lang/String;)V
    .locals 3

    .prologue
    .line 87
    new-instance v0, Landroid/content/Intent;

    const-string v1, "com.samsung.android.intent.action.ACCESSORY_SETUPWIZARD_COMPLETE"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 88
    const-string v1, "eula_agree"

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 89
    const-string v1, "type"

    const-string v2, "PHONE COVER"

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 90
    const-string v1, "serialNumber"

    invoke-virtual {v0, v1, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 91
    const/16 v1, 0x20

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    .line 92
    invoke-virtual {p1, v0}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V

    .line 93
    return-void
.end method


# virtual methods
.method protected llIIIIlllllIIllIIllI(Landroid/content/Context;LIIlIIIIIIllllIIlIIlI;)V
    .locals 2

    .prologue
    .line 65
    invoke-static {p1}, Lcom/sec/android/soagent/db/llIIIIlllllIIllIIllI;->llllIIIllIlIIIIllllI(Landroid/content/Context;)I

    move-result v0

    const/16 v1, 0xc8

    if-ne v0, v1, :cond_0

    .line 66
    const-string v0, "Update previous Cover preference to DB"

    invoke-static {v0}, LllIlIIIIlIIIIIlIlIII;->lllIlIlIIIllIIlIllIl(Ljava/lang/String;)V

    .line 68
    new-instance v0, LIIlIIIIIIllllIIlIIlI;

    invoke-direct {v0}, LIIlIIIIIIllllIIlIIlI;-><init>()V

    .line 69
    invoke-virtual {v0}, LIIlIIIIIIllllIIlIIlI;->IllIlIIIIlIIlIIIllIl()V

    .line 70
    const-string v1, "PHONE COVER"

    invoke-virtual {v0, v1}, LIIlIIIIIIllllIIlIIlI;->llllIIIllIlIIIIllllI(Ljava/lang/String;)V

    .line 71
    invoke-static {p1}, Lcom/sec/android/soagent/db/llIIIIlllllIIllIIllI;->lllIlIlIIIllIIlIllIl(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, LIIlIIIIIIllllIIlIIlI;->lllIlIlIIIllIIlIllIl(Ljava/lang/String;)V

    .line 72
    invoke-static {p1, v0}, LlllIIIIllIlIlllllllI;->llIIIIlllllIIllIIllI(Landroid/content/Context;LIIlIIIIIIllllIIlIIlI;)Z

    .line 75
    const/4 v0, 0x0

    invoke-static {p1, v0}, Lcom/sec/android/soagent/db/llIIIIlllllIIllIIllI;->llIIIIlllllIIllIIllI(Landroid/content/Context;I)Z

    .line 76
    const-string v0, ""

    invoke-static {p1, v0}, Lcom/sec/android/soagent/db/llIIIIlllllIIllIIllI;->llIIIIlllllIIllIIllI(Landroid/content/Context;Ljava/lang/String;)Z

    .line 78
    :cond_0
    return-void
.end method

.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 4

    .prologue
    .line 24
    if-nez p1, :cond_1

    .line 25
    const-string v0, "Content is null!!"

    invoke-static {v0}, LllIlIIIIlIIIIIlIlIII;->llIlIIIIlIIIIIlIlIII(Ljava/lang/String;)V

    .line 58
    :cond_0
    :goto_0
    return-void

    .line 29
    :cond_1
    if-nez p2, :cond_2

    .line 30
    const-string v0, "Intent is null!!"

    invoke-static {v0}, LllIlIIIIlIIIIIlIlIII;->llIlIIIIlIIIIIlIlIII(Ljava/lang/String;)V

    goto :goto_0

    .line 34
    :cond_2
    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    .line 35
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_3

    .line 36
    const-string v0, "Action is null!!"

    invoke-static {v0}, LllIlIIIIlIIIIIlIlIII;->llIlIIIIlIIIIIlIlIII(Ljava/lang/String;)V

    goto :goto_0

    .line 40
    :cond_3
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Receive broadcast meassage:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, LllIlIIIIlIIIIIlIlIII;->llllIIIllIlIIIIllllI(Ljava/lang/String;)V

    .line 42
    const-string v1, "com.samsung.android.intent.action.COVER_ATTACHED"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 44
    const-string v0, "serialNumber"

    invoke-virtual {p2, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 45
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_4

    .line 46
    const-string v0, "Cover Serial Number is null!!"

    invoke-static {v0}, LllIlIIIIlIIIIIlIlIII;->llIlIIIIlIIIIIlIlIII(Ljava/lang/String;)V

    goto :goto_0

    .line 51
    :cond_4
    const-string v1, "PHONE COVER"

    invoke-static {p1, v1}, LlllIIIIllIlIlllllllI;->llIIIIlllllIIllIIllI(Landroid/content/Context;Ljava/lang/String;)LIIlIIIIIIllllIIlIIlI;

    move-result-object v1

    .line 52
    if-nez v1, :cond_5

    const-string v2, "isBoot"

    const/4 v3, 0x0

    invoke-virtual {p2, v2, v3}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v2

    if-eqz v2, :cond_5

    .line 53
    invoke-virtual {p0, p1, v1}, Lcom/sec/android/soagent/AccRegisterReceiver;->llIIIIlllllIIllIIllI(Landroid/content/Context;LIIlIIIIIIllllIIlIIlI;)V

    .line 56
    :cond_5
    invoke-direct {p0, p1, v0}, Lcom/sec/android/soagent/AccRegisterReceiver;->llIIIIlllllIIllIIllI(Landroid/content/Context;Ljava/lang/String;)V

    goto :goto_0
.end method
