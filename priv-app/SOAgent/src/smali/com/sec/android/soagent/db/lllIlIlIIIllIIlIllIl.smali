.class Lcom/sec/android/soagent/db/lllIlIlIIIllIIlIllIl;
.super Landroid/database/sqlite/SQLiteOpenHelper;
.source "llIIIIlllllIIllIIllI"


# direct methods
.method constructor <init>(Landroid/content/Context;)V
    .locals 3

    .prologue
    .line 54
    const-string v0, "soagent.db"

    const/4 v1, 0x0

    const/4 v2, 0x1

    invoke-direct {p0, p1, v0, v1, v2}, Landroid/database/sqlite/SQLiteOpenHelper;-><init>(Landroid/content/Context;Ljava/lang/String;Landroid/database/sqlite/SQLiteDatabase$CursorFactory;I)V

    .line 55
    return-void
.end method


# virtual methods
.method public onCreate(Landroid/database/sqlite/SQLiteDatabase;)V
    .locals 4

    .prologue
    .line 59
    monitor-enter p1

    .line 61
    :try_start_0
    const-string v0, "CREATE TABLE IF NOT EXISTS %s(%s)"

    const/4 v1, 0x2

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    const-string v3, "SOAgent"

    aput-object v3, v1, v2

    const/4 v2, 0x1

    const-string v3, "_id INTEGER PRIMARY KEY,List STR8(100), Info STR8(100)"

    aput-object v3, v1, v2

    invoke-static {v0, v1}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 63
    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V
    :try_end_0
    .catch Landroid/database/SQLException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/NullPointerException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_2
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 74
    :goto_0
    :try_start_1
    monitor-exit p1

    .line 75
    return-void

    .line 64
    :catch_0
    move-exception v0

    .line 65
    const-string v1, "Failed to create the table. : SOAgent"

    invoke-static {v1}, LllIlIIIIlIIIIIlIlIII;->IllIlIIIIlIIlIIIllIl(Ljava/lang/String;)V

    .line 66
    invoke-static {v0}, LllIlIIIIlIIIIIlIlIII;->llIIIIlllllIIllIIllI(Ljava/lang/Throwable;)V

    goto :goto_0

    .line 74
    :catchall_0
    move-exception v0

    monitor-exit p1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0

    .line 67
    :catch_1
    move-exception v0

    .line 68
    :try_start_2
    const-string v1, "Failed to create the table."

    invoke-static {v1}, LllIlIIIIlIIIIIlIlIII;->IllIlIIIIlIIlIIIllIl(Ljava/lang/String;)V

    .line 69
    invoke-static {v0}, LllIlIIIIlIIIIIlIlIII;->llIIIIlllllIIllIIllI(Ljava/lang/Throwable;)V

    goto :goto_0

    .line 70
    :catch_2
    move-exception v0

    .line 71
    const-string v1, "Failed to create the table."

    invoke-static {v1}, LllIlIIIIlIIIIIlIlIII;->IllIlIIIIlIIlIIIllIl(Ljava/lang/String;)V

    .line 72
    invoke-static {v0}, LllIlIIIIlIIIIIlIlIII;->llIIIIlllllIIllIIllI(Ljava/lang/Throwable;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0
.end method

.method public onUpgrade(Landroid/database/sqlite/SQLiteDatabase;II)V
    .locals 4

    .prologue
    .line 79
    monitor-enter p1

    .line 81
    :try_start_0
    const-string v0, "DROP TABLE IF EXISTS %s"

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    const-string v3, "SOAgent"

    aput-object v3, v1, v2

    invoke-static {v0, v1}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 82
    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 83
    invoke-virtual {p0, p1}, Lcom/sec/android/soagent/db/lllIlIlIIIllIIlIllIl;->onCreate(Landroid/database/sqlite/SQLiteDatabase;)V
    :try_end_0
    .catch Landroid/database/SQLException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/NullPointerException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_2
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 94
    :goto_0
    :try_start_1
    monitor-exit p1

    .line 95
    return-void

    .line 84
    :catch_0
    move-exception v0

    .line 85
    const-string v1, "Failed to upgrade the table. : SOAgent"

    invoke-static {v1}, LllIlIIIIlIIIIIlIlIII;->IllIlIIIIlIIlIIIllIl(Ljava/lang/String;)V

    .line 86
    invoke-static {v0}, LllIlIIIIlIIIIIlIlIII;->llIIIIlllllIIllIIllI(Ljava/lang/Throwable;)V

    goto :goto_0

    .line 94
    :catchall_0
    move-exception v0

    monitor-exit p1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0

    .line 87
    :catch_1
    move-exception v0

    .line 88
    :try_start_2
    const-string v1, "Failed to upgrade the table."

    invoke-static {v1}, LllIlIIIIlIIIIIlIlIII;->IllIlIIIIlIIlIIIllIl(Ljava/lang/String;)V

    .line 89
    invoke-static {v0}, LllIlIIIIlIIIIIlIlIII;->llIIIIlllllIIllIIllI(Ljava/lang/Throwable;)V

    goto :goto_0

    .line 90
    :catch_2
    move-exception v0

    .line 91
    const-string v1, "Failed to upgrade the table."

    invoke-static {v1}, LllIlIIIIlIIIIIlIlIII;->IllIlIIIIlIIlIIIllIl(Ljava/lang/String;)V

    .line 92
    invoke-static {v0}, LllIlIIIIlIIIIIlIlIII;->llIIIIlllllIIllIIllI(Ljava/lang/Throwable;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0
.end method
