.class public Lcom/sec/android/soagent/db/SoContentProvider;
.super Landroid/content/ContentProvider;
.source "llIIIIlllllIIllIIllI"


# static fields
.field public static final llIIIIlllllIIllIIllI:Landroid/net/Uri;

.field private static llllIIIllIlIIIIllllI:Ljava/util/HashMap;


# instance fields
.field private lllIlIlIIIllIIlIllIl:Lcom/sec/android/soagent/db/lllIlIlIIIllIIlIllIl;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 36
    const-string v0, "content://com.sec.android.soagent.provider.contentprovider/SOAgent"

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    sput-object v0, Lcom/sec/android/soagent/db/SoContentProvider;->llIIIIlllllIIllIIllI:Landroid/net/Uri;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 20
    invoke-direct {p0}, Landroid/content/ContentProvider;-><init>()V

    .line 52
    return-void
.end method


# virtual methods
.method public delete(Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;)I
    .locals 5

    .prologue
    .line 268
    iget-object v2, p0, Lcom/sec/android/soagent/db/SoContentProvider;->lllIlIlIIIllIIlIllIl:Lcom/sec/android/soagent/db/lllIlIlIIIllIIlIllIl;

    monitor-enter v2

    .line 269
    :try_start_0
    iget-object v0, p0, Lcom/sec/android/soagent/db/SoContentProvider;->lllIlIlIIIllIIlIllIl:Lcom/sec/android/soagent/db/lllIlIlIIIllIIlIllIl;

    invoke-virtual {v0}, Lcom/sec/android/soagent/db/lllIlIlIIIllIIlIllIl;->getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v0

    .line 270
    const/4 v1, 0x0

    .line 272
    :try_start_1
    const-string v3, "SOAgent"

    invoke-virtual {v0, v3, p2, p3}, Landroid/database/sqlite/SQLiteDatabase;->delete(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v1

    .line 273
    invoke-virtual {p0}, Lcom/sec/android/soagent/db/SoContentProvider;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const/4 v3, 0x0

    invoke-virtual {v0, p1, v3}, Landroid/content/ContentResolver;->notifyChange(Landroid/net/Uri;Landroid/database/ContentObserver;)V
    :try_end_1
    .catch Landroid/database/SQLException; {:try_start_1 .. :try_end_1} :catch_0
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 281
    :goto_0
    :try_start_2
    monitor-exit v2

    return v1

    .line 274
    :catch_0
    move-exception v0

    .line 275
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Failed to delete row. : "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, LllIlIIIIlIIIIIlIlIII;->llIIIIlllllIIllIIllI(Ljava/lang/String;)V

    .line 276
    invoke-static {v0}, LllIlIIIIlIIIIIlIlIII;->llIIIIlllllIIllIIllI(Ljava/lang/Throwable;)V

    goto :goto_0

    .line 282
    :catchall_0
    move-exception v0

    monitor-exit v2
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    throw v0

    .line 277
    :catch_1
    move-exception v0

    .line 278
    :try_start_3
    const-string v3, "Failed to delete row."

    invoke-static {v3}, LllIlIIIIlIIIIIlIlIII;->IllIlIIIIlIIlIIIllIl(Ljava/lang/String;)V

    .line 279
    invoke-static {v0}, LllIlIIIIlIIIIIlIlIII;->llIIIIlllllIIllIIllI(Ljava/lang/Throwable;)V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    goto :goto_0
.end method

.method public getType(Landroid/net/Uri;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 171
    const-string v0, "com.sec.android.soagentclient.provider.contentprovider/soagent"

    return-object v0
.end method

.method public insert(Landroid/net/Uri;Landroid/content/ContentValues;)Landroid/net/Uri;
    .locals 9

    .prologue
    const/4 v7, 0x1

    const/4 v0, 0x0

    .line 185
    iget-object v4, p0, Lcom/sec/android/soagent/db/SoContentProvider;->lllIlIlIIIllIIlIllIl:Lcom/sec/android/soagent/db/lllIlIlIIIllIIlIllIl;

    monitor-enter v4

    .line 186
    :try_start_0
    iget-object v1, p0, Lcom/sec/android/soagent/db/SoContentProvider;->lllIlIlIIIllIIlIllIl:Lcom/sec/android/soagent/db/lllIlIlIIIllIIlIllIl;

    invoke-virtual {v1}, Lcom/sec/android/soagent/db/lllIlIlIIIllIIlIllIl;->getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v5

    .line 189
    :try_start_1
    new-instance v1, Landroid/content/ContentValues;

    invoke-direct {v1, p2}, Landroid/content/ContentValues;-><init>(Landroid/content/ContentValues;)V

    .line 190
    if-nez p2, :cond_0

    .line 191
    new-instance v1, Landroid/content/ContentValues;

    invoke-direct {v1}, Landroid/content/ContentValues;-><init>()V

    .line 193
    :cond_0
    const-wide/16 v2, -0x1

    .line 194
    const-string v6, "List"

    invoke-virtual {v1, v6}, Landroid/content/ContentValues;->containsKey(Ljava/lang/String;)Z

    move-result v6

    if-ne v6, v7, :cond_1

    const-string v6, "Info"

    invoke-virtual {v1, v6}, Landroid/content/ContentValues;->containsKey(Ljava/lang/String;)Z

    move-result v6

    if-ne v6, v7, :cond_1

    .line 196
    const-string v2, "SOAgent"

    const/4 v3, 0x0

    invoke-virtual {v5, v2, v3, v1}, Landroid/database/sqlite/SQLiteDatabase;->insert(Ljava/lang/String;Ljava/lang/String;Landroid/content/ContentValues;)J

    move-result-wide v2

    .line 198
    :cond_1
    const-wide/16 v6, 0x0

    cmp-long v1, v2, v6

    if-lez v1, :cond_2

    .line 199
    sget-object v1, Lcom/sec/android/soagent/db/SoContentProvider;->llIIIIlllllIIllIIllI:Landroid/net/Uri;

    invoke-static {v1, v2, v3}, Landroid/content/ContentUris;->withAppendedId(Landroid/net/Uri;J)Landroid/net/Uri;
    :try_end_1
    .catch Landroid/database/SQLException; {:try_start_1 .. :try_end_1} :catch_0
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result-object v0

    .line 200
    :try_start_2
    invoke-virtual {p0}, Lcom/sec/android/soagent/db/SoContentProvider;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v1, v0, v2}, Landroid/content/ContentResolver;->notifyChange(Landroid/net/Uri;Landroid/database/ContentObserver;)V
    :try_end_2
    .catch Landroid/database/SQLException; {:try_start_2 .. :try_end_2} :catch_3
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    :goto_0
    move-object v1, v0

    .line 211
    :goto_1
    :try_start_3
    monitor-exit v4
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    return-object v1

    .line 202
    :cond_2
    :try_start_4
    const-string v1, "rowId invalidate"

    invoke-static {v1}, LllIlIIIIlIIIIIlIlIII;->IllIlIIIIlIIlIIIllIl(Ljava/lang/String;)V
    :try_end_4
    .catch Landroid/database/SQLException; {:try_start_4 .. :try_end_4} :catch_0
    .catch Ljava/lang/Exception; {:try_start_4 .. :try_end_4} :catch_1
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    goto :goto_0

    .line 204
    :catch_0
    move-exception v1

    move-object v8, v1

    move-object v1, v0

    move-object v0, v8

    .line 205
    :goto_2
    :try_start_5
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Failed to insert row. : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, LllIlIIIIlIIIIIlIlIII;->llIIIIlllllIIllIIllI(Ljava/lang/String;)V

    .line 206
    invoke-static {v0}, LllIlIIIIlIIIIIlIlIII;->llIIIIlllllIIllIIllI(Ljava/lang/Throwable;)V

    goto :goto_1

    .line 212
    :catchall_0
    move-exception v0

    monitor-exit v4
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    throw v0

    .line 207
    :catch_1
    move-exception v1

    move-object v8, v1

    move-object v1, v0

    move-object v0, v8

    .line 208
    :goto_3
    :try_start_6
    const-string v2, "Failed to insert row."

    invoke-static {v2}, LllIlIIIIlIIIIIlIlIII;->IllIlIIIIlIIlIIIllIl(Ljava/lang/String;)V

    .line 209
    invoke-static {v0}, LllIlIIIIlIIIIIlIlIII;->llIIIIlllllIIllIIllI(Ljava/lang/Throwable;)V
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_0

    goto :goto_1

    .line 207
    :catch_2
    move-exception v1

    move-object v8, v1

    move-object v1, v0

    move-object v0, v8

    goto :goto_3

    .line 204
    :catch_3
    move-exception v1

    move-object v8, v1

    move-object v1, v0

    move-object v0, v8

    goto :goto_2
.end method

.method public onCreate()Z
    .locals 2

    .prologue
    .line 107
    new-instance v0, Lcom/sec/android/soagent/db/lllIlIlIIIllIIlIllIl;

    invoke-virtual {p0}, Lcom/sec/android/soagent/db/SoContentProvider;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/sec/android/soagent/db/lllIlIlIIIllIIlIllIl;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/sec/android/soagent/db/SoContentProvider;->lllIlIlIIIllIIlIllIl:Lcom/sec/android/soagent/db/lllIlIlIIIllIIlIllIl;

    .line 108
    const/4 v0, 0x1

    return v0
.end method

.method public query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    .locals 10

    .prologue
    const/4 v8, 0x0

    .line 136
    iget-object v9, p0, Lcom/sec/android/soagent/db/SoContentProvider;->lllIlIlIIIllIIlIllIl:Lcom/sec/android/soagent/db/lllIlIlIIIllIIlIllIl;

    monitor-enter v9

    .line 137
    :try_start_0
    iget-object v0, p0, Lcom/sec/android/soagent/db/SoContentProvider;->lllIlIlIIIllIIlIllIl:Lcom/sec/android/soagent/db/lllIlIlIIIllIIlIllIl;

    invoke-virtual {v0}, Lcom/sec/android/soagent/db/lllIlIlIIIllIIlIllIl;->getReadableDatabase()Landroid/database/sqlite/SQLiteDatabase;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v1

    .line 140
    :try_start_1
    new-instance v0, Landroid/database/sqlite/SQLiteQueryBuilder;

    invoke-direct {v0}, Landroid/database/sqlite/SQLiteQueryBuilder;-><init>()V

    .line 142
    const-string v2, "SOAgent"

    invoke-virtual {v0, v2}, Landroid/database/sqlite/SQLiteQueryBuilder;->setTables(Ljava/lang/String;)V

    .line 143
    sget-object v2, Lcom/sec/android/soagent/db/SoContentProvider;->llllIIIllIlIIIIllllI:Ljava/util/HashMap;

    invoke-virtual {v0, v2}, Landroid/database/sqlite/SQLiteQueryBuilder;->setProjectionMap(Ljava/util/Map;)V

    .line 145
    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    invoke-virtual/range {v0 .. v7}, Landroid/database/sqlite/SQLiteQueryBuilder;->query(Landroid/database/sqlite/SQLiteDatabase;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    :try_end_1
    .catch Landroid/database/SQLException; {:try_start_1 .. :try_end_1} :catch_0
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result-object v1

    .line 146
    if-eqz v1, :cond_0

    .line 147
    :try_start_2
    invoke-virtual {p0}, Lcom/sec/android/soagent/db/SoContentProvider;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    invoke-interface {v1, v0, p1}, Landroid/database/Cursor;->setNotificationUri(Landroid/content/ContentResolver;Landroid/net/Uri;)V
    :try_end_2
    .catch Landroid/database/SQLException; {:try_start_2 .. :try_end_2} :catch_3
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 156
    :cond_0
    :goto_0
    :try_start_3
    monitor-exit v9

    return-object v1

    .line 149
    :catch_0
    move-exception v0

    move-object v1, v8

    .line 150
    :goto_1
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Failed to query. : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, LllIlIIIIlIIIIIlIlIII;->llIIIIlllllIIllIIllI(Ljava/lang/String;)V

    .line 151
    invoke-static {v0}, LllIlIIIIlIIIIIlIlIII;->llIIIIlllllIIllIIllI(Ljava/lang/Throwable;)V

    goto :goto_0

    .line 157
    :catchall_0
    move-exception v0

    monitor-exit v9
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    throw v0

    .line 152
    :catch_1
    move-exception v0

    .line 153
    :goto_2
    :try_start_4
    const-string v1, "Failed to query."

    invoke-static {v1}, LllIlIIIIlIIIIIlIlIII;->IllIlIIIIlIIlIIIllIl(Ljava/lang/String;)V

    .line 154
    invoke-static {v0}, LllIlIIIIlIIIIIlIlIII;->llIIIIlllllIIllIIllI(Ljava/lang/Throwable;)V
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    move-object v1, v8

    goto :goto_0

    .line 152
    :catch_2
    move-exception v0

    move-object v8, v1

    goto :goto_2

    .line 149
    :catch_3
    move-exception v0

    goto :goto_1
.end method

.method public update(Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I
    .locals 5

    .prologue
    .line 231
    iget-object v2, p0, Lcom/sec/android/soagent/db/SoContentProvider;->lllIlIlIIIllIIlIllIl:Lcom/sec/android/soagent/db/lllIlIlIIIllIIlIllIl;

    monitor-enter v2

    .line 232
    :try_start_0
    iget-object v0, p0, Lcom/sec/android/soagent/db/SoContentProvider;->lllIlIlIIIllIIlIllIl:Lcom/sec/android/soagent/db/lllIlIlIIIllIIlIllIl;

    invoke-virtual {v0}, Lcom/sec/android/soagent/db/lllIlIlIIIllIIlIllIl;->getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v0

    .line 233
    const/4 v1, 0x0

    .line 236
    :try_start_1
    const-string v3, "SOAgent"

    invoke-virtual {v0, v3, p2, p3, p4}, Landroid/database/sqlite/SQLiteDatabase;->update(Ljava/lang/String;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v1

    .line 237
    invoke-virtual {p0}, Lcom/sec/android/soagent/db/SoContentProvider;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const/4 v3, 0x0

    invoke-virtual {v0, p1, v3}, Landroid/content/ContentResolver;->notifyChange(Landroid/net/Uri;Landroid/database/ContentObserver;)V
    :try_end_1
    .catch Landroid/database/SQLException; {:try_start_1 .. :try_end_1} :catch_0
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 245
    :goto_0
    :try_start_2
    monitor-exit v2

    return v1

    .line 238
    :catch_0
    move-exception v0

    .line 239
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Failed to update row. : "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, LllIlIIIIlIIIIIlIlIII;->llIIIIlllllIIllIIllI(Ljava/lang/String;)V

    .line 240
    invoke-static {v0}, LllIlIIIIlIIIIIlIlIII;->llIIIIlllllIIllIIllI(Ljava/lang/Throwable;)V

    goto :goto_0

    .line 246
    :catchall_0
    move-exception v0

    monitor-exit v2
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    throw v0

    .line 241
    :catch_1
    move-exception v0

    .line 242
    :try_start_3
    const-string v3, "Failed to update row."

    invoke-static {v3}, LllIlIIIIlIIIIIlIlIII;->IllIlIIIIlIIlIIIllIl(Ljava/lang/String;)V

    .line 243
    invoke-static {v0}, LllIlIIIIlIIIIIlIlIII;->llIIIIlllllIIllIIllI(Ljava/lang/Throwable;)V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    goto :goto_0
.end method
