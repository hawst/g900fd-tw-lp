.class public Lcom/sec/android/soagent/RegisterService;
.super Landroid/app/IntentService;
.source "llIIIIlllllIIllIIllI"


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 22
    const-string v0, "Service[AccRegisterService]"

    invoke-direct {p0, v0}, Landroid/app/IntentService;-><init>(Ljava/lang/String;)V

    .line 23
    return-void
.end method

.method private llIIIIlllllIIllIIllI(LIIlIIIIIIllllIIlIIlI;LIlIlIlllIlllIlIIllII;)V
    .locals 3

    .prologue
    .line 64
    new-instance v0, LlIllIlllIIllllIlIlIl;

    invoke-direct {v0, p1, p2}, LlIllIlllIIllllIlIlIl;-><init>(LIIlIIIIIIllllIIlIIlI;LIlIlIlllIlllIlIIllII;)V

    .line 66
    const-string v1, "Request NetActionRegisterDevice"

    invoke-static {v1}, LllIlIIIIlIIIIIlIlIII;->lllIlIlIIIllIIlIllIl(Ljava/lang/String;)V

    .line 67
    new-instance v1, LlIlIIlllllIllllllllI;

    invoke-direct {v1}, LlIlIIlllllIllllllllI;-><init>()V

    .line 68
    invoke-virtual {v1, p0, v0}, LlIlIIlllllIllllllllI;->llIIIIlllllIIllIIllI(Landroid/content/Context;LlllIIIlllIllIlIllIIl;)LIlIlIlIIIIIllllIlIIl;

    move-result-object v0

    .line 70
    if-nez v0, :cond_0

    .line 71
    const-string v0, "Result is Null"

    invoke-static {v0}, LllIlIIIIlIIIIIlIlIII;->llIlIIIIlIIIIIlIlIII(Ljava/lang/String;)V

    .line 92
    :goto_0
    return-void

    .line 74
    :cond_0
    invoke-virtual {v0}, LIlIlIlIIIIIllllIlIIl;->llIlIIIIlIIIIIlIlIII()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 75
    const-string v0, "Receive result: success in NetActionRegisterDevice"

    invoke-static {v0}, LllIlIIIIlIIIIIlIlIII;->lllIlIlIIIllIIlIllIl(Ljava/lang/String;)V

    .line 76
    invoke-virtual {p1}, LIIlIIIIIIllllIIlIIlI;->IllIlIIIIlIIlIIIllIl()V

    .line 77
    invoke-static {p0, p1}, LlllIIIIllIlIlllllllI;->llllIIIllIlIIIIllllI(Landroid/content/Context;LIIlIIIIIIllllIIlIIlI;)Z

    goto :goto_0

    .line 80
    :cond_1
    invoke-virtual {v0}, LIlIlIlIIIIIllllIlIIl;->lllIlIlIIIllIIlIllIl()LlIlIIIlllIIlIIIlIlII;

    move-result-object v0

    .line 81
    if-eqz v0, :cond_3

    .line 82
    const/16 v1, 0x1b8

    invoke-virtual {v0}, LlIlIIIlllIIlIIIlIlII;->llIIIIlllllIIllIIllI()I

    move-result v2

    if-eq v1, v2, :cond_2

    const/16 v1, 0x64

    invoke-virtual {v0}, LlIlIIIlllIIlIIIlIlII;->llIIIIlllllIIllIIllI()I

    move-result v0

    if-ne v1, v0, :cond_3

    .line 84
    :cond_2
    const-string v0, "Accessory is invalid!!!"

    invoke-static {v0}, LllIlIIIIlIIIIIlIlIII;->llIlIIIIlIIIIIlIlIII(Ljava/lang/String;)V

    .line 85
    invoke-virtual {p1}, LIIlIIIIIIllllIIlIIlI;->llIIllllIIlllIIIIlll()V

    .line 86
    invoke-static {p0, p1}, LlllIIIIllIlIlllllllI;->llllIIIllIlIIIIllllI(Landroid/content/Context;LIIlIIIIIIllllIIlIIlI;)Z

    .line 89
    :cond_3
    const-string v0, "Receive result: fail in NetActionRegisterDevice"

    invoke-static {v0}, LllIlIIIIlIIIIIlIlIII;->lllIlIlIIIllIIlIllIl(Ljava/lang/String;)V

    goto :goto_0
.end method


# virtual methods
.method protected onHandleIntent(Landroid/content/Intent;)V
    .locals 4

    .prologue
    .line 28
    if-nez p1, :cond_0

    .line 29
    const-string v0, "Intent is null!!"

    invoke-static {v0}, LllIlIIIIlIIIIIlIlIII;->llIlIIIIlIIIIIlIlIII(Ljava/lang/String;)V

    .line 57
    :goto_0
    return-void

    .line 33
    :cond_0
    invoke-static {p0}, LIllIIlllIllIIIIIllII;->llIIIIlllllIIllIIllI(Landroid/content/Context;)LlIlIIIlllIIlIIIlIlII;

    move-result-object v0

    .line 34
    if-eqz v0, :cond_1

    .line 35
    const-string v0, "Network is not available"

    invoke-static {v0}, LllIlIIIIlIIIIIlIlIII;->llIlIIIIlIIIIIlIlIII(Ljava/lang/String;)V

    goto :goto_0

    .line 39
    :cond_1
    const-string v0, "type"

    invoke-virtual {p1, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 40
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 41
    const-string v0, "Extra type is null"

    invoke-static {v0}, LllIlIIIIlIIIIIlIlIII;->llIlIIIIlIIIIIlIlIII(Ljava/lang/String;)V

    goto :goto_0

    .line 45
    :cond_2
    invoke-static {p0, v0}, LlllIIIIllIlIlllllllI;->llIIIIlllllIIllIIllI(Landroid/content/Context;Ljava/lang/String;)LIIlIIIIIIllllIIlIIlI;

    move-result-object v1

    .line 46
    if-nez v1, :cond_3

    .line 47
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Can\'t find Accessory DB:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, LllIlIIIIlIIIIIlIlIII;->llIlIIIIlIIIIIlIlIII(Ljava/lang/String;)V

    goto :goto_0

    .line 51
    :cond_3
    new-instance v0, LIlIlIlllIlllIlIIllII;

    invoke-direct {v0}, LIlIlIlllIlllIlIIllII;-><init>()V

    .line 52
    invoke-static {p0}, LlllIlIlIIIllIIlIllIl;->llIIIIlllllIIllIIllI(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, LIlIlIlllIlllIlIIllII;->llIIIIlllllIIllIIllI(Ljava/lang/String;)V

    .line 53
    invoke-static {p0}, LlllIlIlIIIllIIlIllIl;->llllIIIllIlIIIIllllI(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, LIlIlIlllIlllIlIIllII;->llllIIIllIlIIIIllllI(Ljava/lang/String;)V

    .line 55
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Start to register accessory: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v1}, LIIlIIIIIIllllIIlIIlI;->IIIIllIlIIlIIIIlllIl()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, LllIlIIIIlIIIIIlIlIII;->lllIlIlIIIllIIlIllIl(Ljava/lang/String;)V

    .line 56
    invoke-direct {p0, v1, v0}, Lcom/sec/android/soagent/RegisterService;->llIIIIlllllIIllIIllI(LIIlIIIIIIllllIIlIIlI;LIlIlIlllIlllIlIIllII;)V

    goto :goto_0
.end method
