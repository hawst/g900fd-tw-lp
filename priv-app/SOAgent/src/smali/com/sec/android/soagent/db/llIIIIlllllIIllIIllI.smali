.class public Lcom/sec/android/soagent/db/llIIIIlllllIIllIIllI;
.super Ljava/lang/Object;
.source "llIIIIlllllIIllIIllI"


# static fields
.field private static llIIIIlllllIIllIIllI:Ljava/lang/String;

.field private static llllIIIllIlIIIIllllI:Ljava/lang/String;
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 18
    const-string v0, "ACCESSORY_REGISTRAION_TIME"

    sput-object v0, Lcom/sec/android/soagent/db/llIIIIlllllIIllIIllI;->llIIIIlllllIIllIIllI:Ljava/lang/String;

    .line 80
    const-string v0, "ACCESSORY_SERIAL_NUMBER"

    sput-object v0, Lcom/sec/android/soagent/db/llIIIIlllllIIllIIllI;->llllIIIllIlIIIIllllI:Ljava/lang/String;

    return-void
.end method

.method public static llIIIIlllllIIllIIllI(Landroid/content/Context;)J
    .locals 4

    .prologue
    const-wide/16 v0, 0x0

    .line 24
    .line 25
    if-nez p0, :cond_1

    .line 33
    :cond_0
    :goto_0
    return-wide v0

    .line 28
    :cond_1
    const-string v2, "ACC_PREFERENCE"

    const/4 v3, 0x0

    invoke-virtual {p0, v2, v3}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v2

    .line 30
    if-eqz v2, :cond_0

    .line 31
    sget-object v3, Lcom/sec/android/soagent/db/llIIIIlllllIIllIIllI;->llIIIIlllllIIllIIllI:Ljava/lang/String;

    invoke-interface {v2, v3, v0, v1}, Landroid/content/SharedPreferences;->getLong(Ljava/lang/String;J)J

    move-result-wide v0

    goto :goto_0
.end method

.method public static llIIIIlllllIIllIIllI(Landroid/content/Context;I)Z
    .locals 2
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    const/4 v0, 0x0

    .line 104
    if-nez p0, :cond_1

    .line 117
    :cond_0
    :goto_0
    return v0

    .line 107
    :cond_1
    const-string v1, "INNOVATION_PREFERENCE"

    invoke-virtual {p0, v1, v0}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v1

    .line 109
    if-eqz v1, :cond_0

    .line 110
    invoke-interface {v1}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    .line 111
    const-string v1, "ACCESSORY_REGISTRAION_STATE"

    invoke-interface {v0, v1}, Landroid/content/SharedPreferences$Editor;->remove(Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    .line 112
    const-string v1, "ACCESSORY_REGISTRAION_STATE"

    invoke-interface {v0, v1, p1}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    .line 113
    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->apply()V

    .line 114
    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    move-result v0

    goto :goto_0
.end method

.method public static llIIIIlllllIIllIIllI(Landroid/content/Context;J)Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 40
    if-nez p0, :cond_1

    .line 53
    :cond_0
    :goto_0
    return v0

    .line 43
    :cond_1
    const-string v1, "ACC_PREFERENCE"

    invoke-virtual {p0, v1, v0}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v1

    .line 45
    if-eqz v1, :cond_0

    .line 46
    invoke-interface {v1}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    .line 47
    sget-object v1, Lcom/sec/android/soagent/db/llIIIIlllllIIllIIllI;->llIIIIlllllIIllIIllI:Ljava/lang/String;

    invoke-interface {v0, v1}, Landroid/content/SharedPreferences$Editor;->remove(Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    .line 48
    sget-object v1, Lcom/sec/android/soagent/db/llIIIIlllllIIllIIllI;->llIIIIlllllIIllIIllI:Ljava/lang/String;

    invoke-interface {v0, v1, p1, p2}, Landroid/content/SharedPreferences$Editor;->putLong(Ljava/lang/String;J)Landroid/content/SharedPreferences$Editor;

    .line 49
    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->apply()V

    .line 50
    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    move-result v0

    goto :goto_0
.end method

.method public static llIIIIlllllIIllIIllI(Landroid/content/Context;Ljava/lang/String;)Z
    .locals 3
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    const/4 v0, 0x0

    .line 142
    if-nez p0, :cond_1

    .line 159
    :cond_0
    :goto_0
    return v0

    .line 145
    :cond_1
    if-eqz p1, :cond_0

    .line 148
    const-string v1, "INNOVATION_PREFERENCE"

    invoke-virtual {p0, v1, v0}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v1

    .line 150
    if-eqz v1, :cond_0

    .line 151
    invoke-interface {v1}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    .line 152
    sget-object v1, Lcom/sec/android/soagent/db/llIIIIlllllIIllIIllI;->llllIIIllIlIIIIllllI:Ljava/lang/String;

    invoke-interface {v0, v1}, Landroid/content/SharedPreferences$Editor;->remove(Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    .line 153
    sget-object v1, Lcom/sec/android/soagent/db/llIIIIlllllIIllIIllI;->llllIIIllIlIIIIllllI:Ljava/lang/String;

    invoke-static {p1}, LllIIIIlllllIIllIIllI;->lllIlIlIIIllIIlIllIl(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    .line 154
    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->apply()V

    .line 155
    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    move-result v0

    goto :goto_0
.end method

.method public static lllIlIlIIIllIIlIllIl(Landroid/content/Context;)Ljava/lang/String;
    .locals 3
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 125
    const-string v0, ""

    .line 126
    if-nez p0, :cond_1

    .line 134
    :cond_0
    :goto_0
    return-object v0

    .line 129
    :cond_1
    const-string v1, "INNOVATION_PREFERENCE"

    const/4 v2, 0x0

    invoke-virtual {p0, v1, v2}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v1

    .line 131
    if-eqz v1, :cond_0

    .line 132
    sget-object v0, Lcom/sec/android/soagent/db/llIIIIlllllIIllIIllI;->llllIIIllIlIIIIllllI:Ljava/lang/String;

    const-string v2, ""

    invoke-interface {v1, v0, v2}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, LllIIIIlllllIIllIIllI;->llIlIIIIlIIIIIlIlIII(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public static llllIIIllIlIIIIllllI(Landroid/content/Context;)I
    .locals 3
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    const/4 v0, 0x0

    .line 87
    .line 88
    if-nez p0, :cond_1

    .line 96
    :cond_0
    :goto_0
    return v0

    .line 91
    :cond_1
    const-string v1, "INNOVATION_PREFERENCE"

    invoke-virtual {p0, v1, v0}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v1

    .line 93
    if-eqz v1, :cond_0

    .line 94
    const-string v2, "ACCESSORY_REGISTRAION_STATE"

    invoke-interface {v1, v2, v0}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v0

    goto :goto_0
.end method
