.class public Lcom/sec/android/fota/osp/time/ServerTimeReceiver;
.super Landroid/content/BroadcastReceiver;
.source "llIIIIlllllIIllIIllI"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 11
    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    return-void
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 6

    .prologue
    .line 28
    if-nez p1, :cond_1

    .line 29
    sget-object v0, LllIlIIIIlIIIIIlIlIII;->llIIIIlllllIIllIIllI:LllllIllIIIIIlIIllIII;

    const-string v1, "Content is null!!"

    invoke-virtual {v0, v1}, LllllIllIIIIIlIIllIII;->llIlIIIIlIIIIIlIlIII(Ljava/lang/String;)V

    .line 61
    :cond_0
    :goto_0
    return-void

    .line 33
    :cond_1
    if-nez p2, :cond_2

    .line 34
    sget-object v0, LllIlIIIIlIIIIIlIlIII;->llIIIIlllllIIllIIllI:LllllIllIIIIIlIIllIII;

    const-string v1, "Intent is null!!"

    invoke-virtual {v0, v1}, LllllIllIIIIIlIIllIII;->llIlIIIIlIIIIIlIlIII(Ljava/lang/String;)V

    goto :goto_0

    .line 38
    :cond_2
    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    .line 39
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_3

    .line 40
    sget-object v0, LllIlIIIIlIIIIIlIlIII;->llIIIIlllllIIllIIllI:LllllIllIIIIIlIIllIII;

    const-string v1, "Action is null!!"

    invoke-virtual {v0, v1}, LllllIllIIIIIlIIllIII;->llIlIIIIlIIIIIlIlIII(Ljava/lang/String;)V

    goto :goto_0

    .line 44
    :cond_3
    sget-object v1, LllIlIIIIlIIIIIlIlIII;->llIIIIlllllIIllIIllI:LllllIllIIIIIlIIllIII;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Receive broadcast meassage:"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, LllllIllIIIIIlIIllIII;->llllIIIllIlIIIIllllI(Ljava/lang/String;)V

    .line 46
    invoke-static {p1}, Lcom/sec/android/fota/osp/time/llllIIIllIlIIIIllllI;->llIIIIlllllIIllIIllI(Landroid/content/Context;)J

    move-result-wide v2

    const-wide/16 v4, 0x0

    cmp-long v1, v2, v4

    if-nez v1, :cond_4

    .line 47
    sget-object v0, LllIlIIIIlIIIIIlIlIII;->llIIIIlllllIIllIIllI:LllllIllIIIIIlIIllIII;

    const-string v1, "No action is available before server time settings"

    invoke-virtual {v0, v1}, LllllIllIIIIIlIIllIII;->lllIlIlIIIllIIlIllIl(Ljava/lang/String;)V

    goto :goto_0

    .line 52
    :cond_4
    const-string v1, "android.intent.action.TIME_SET"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_5

    const-string v1, "android.intent.action.TIMEZONE_CHANGED"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 55
    :cond_5
    :try_start_0
    invoke-static {p1}, Lcom/sec/android/fota/osp/time/llllIIIllIlIIIIllllI;->lllIlIlIIIllIIlIllIl(Landroid/content/Context;)Z
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 56
    :catch_0
    move-exception v0

    .line 57
    invoke-static {v0}, LllIlIIIIlIIIIIlIlIII;->llIIIIlllllIIllIIllI(Ljava/lang/Throwable;)V

    goto :goto_0
.end method
