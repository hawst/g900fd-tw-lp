.class public Lcom/sec/android/fota/osp/http/ErrorResultException;
.super Ljava/lang/Exception;
.source "llIIIIlllllIIllIIllI"


# static fields
.field private static final serialVersionUID:J = -0x3616ee28603b7f42L


# instance fields
.field private faultCode:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 29
    invoke-direct {p0}, Ljava/lang/Exception;-><init>()V

    .line 30
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 71
    invoke-direct {p0, p1}, Ljava/lang/Exception;-><init>(Ljava/lang/String;)V

    .line 72
    invoke-virtual {p0, p2}, Lcom/sec/android/fota/osp/http/ErrorResultException;->llIIIIlllllIIllIIllI(Ljava/lang/String;)V

    .line 74
    const-string v0, "ErrorResult.code"

    invoke-static {v0, p1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 75
    const-string v0, "ErrorResult.message"

    invoke-static {v0, p2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 76
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/lang/Throwable;)V
    .locals 0

    .prologue
    .line 60
    invoke-direct {p0, p1, p2}, Ljava/lang/Exception;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 61
    return-void
.end method


# virtual methods
.method public llIIIIlllllIIllIIllI(Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 110
    iput-object p1, p0, Lcom/sec/android/fota/osp/http/ErrorResultException;->faultCode:Ljava/lang/String;

    .line 111
    return-void
.end method
