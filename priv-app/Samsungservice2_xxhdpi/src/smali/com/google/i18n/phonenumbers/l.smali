.class public abstract enum Lcom/google/i18n/phonenumbers/l;
.super Ljava/lang/Enum;
.source "PhoneNumberUtil.java"


# static fields
.field public static final enum a:Lcom/google/i18n/phonenumbers/l;

.field public static final enum b:Lcom/google/i18n/phonenumbers/l;

.field public static final enum c:Lcom/google/i18n/phonenumbers/l;

.field public static final enum d:Lcom/google/i18n/phonenumbers/l;

.field private static final synthetic e:[Lcom/google/i18n/phonenumbers/l;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    .line 445
    new-instance v0, Lcom/google/i18n/phonenumbers/m;

    const-string v1, "POSSIBLE"

    invoke-direct {v0, v1}, Lcom/google/i18n/phonenumbers/m;-><init>(Ljava/lang/String;)V

    sput-object v0, Lcom/google/i18n/phonenumbers/l;->a:Lcom/google/i18n/phonenumbers/l;

    .line 457
    new-instance v0, Lcom/google/i18n/phonenumbers/n;

    const-string v1, "VALID"

    invoke-direct {v0, v1}, Lcom/google/i18n/phonenumbers/n;-><init>(Ljava/lang/String;)V

    sput-object v0, Lcom/google/i18n/phonenumbers/l;->b:Lcom/google/i18n/phonenumbers/l;

    .line 479
    new-instance v0, Lcom/google/i18n/phonenumbers/o;

    const-string v1, "STRICT_GROUPING"

    invoke-direct {v0, v1}, Lcom/google/i18n/phonenumbers/o;-><init>(Ljava/lang/String;)V

    sput-object v0, Lcom/google/i18n/phonenumbers/l;->c:Lcom/google/i18n/phonenumbers/l;

    .line 510
    new-instance v0, Lcom/google/i18n/phonenumbers/q;

    const-string v1, "EXACT_GROUPING"

    invoke-direct {v0, v1}, Lcom/google/i18n/phonenumbers/q;-><init>(Ljava/lang/String;)V

    sput-object v0, Lcom/google/i18n/phonenumbers/l;->d:Lcom/google/i18n/phonenumbers/l;

    .line 440
    const/4 v0, 0x4

    new-array v0, v0, [Lcom/google/i18n/phonenumbers/l;

    const/4 v1, 0x0

    sget-object v2, Lcom/google/i18n/phonenumbers/l;->a:Lcom/google/i18n/phonenumbers/l;

    aput-object v2, v0, v1

    const/4 v1, 0x1

    sget-object v2, Lcom/google/i18n/phonenumbers/l;->b:Lcom/google/i18n/phonenumbers/l;

    aput-object v2, v0, v1

    const/4 v1, 0x2

    sget-object v2, Lcom/google/i18n/phonenumbers/l;->c:Lcom/google/i18n/phonenumbers/l;

    aput-object v2, v0, v1

    const/4 v1, 0x3

    sget-object v2, Lcom/google/i18n/phonenumbers/l;->d:Lcom/google/i18n/phonenumbers/l;

    aput-object v2, v0, v1

    sput-object v0, Lcom/google/i18n/phonenumbers/l;->e:[Lcom/google/i18n/phonenumbers/l;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0

    .prologue
    .line 440
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method synthetic constructor <init>(Ljava/lang/String;IB)V
    .locals 0

    .prologue
    .line 440
    invoke-direct {p0, p1, p2}, Lcom/google/i18n/phonenumbers/l;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/google/i18n/phonenumbers/l;
    .locals 1

    .prologue
    .line 440
    const-class v0, Lcom/google/i18n/phonenumbers/l;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/google/i18n/phonenumbers/l;

    return-object v0
.end method

.method public static values()[Lcom/google/i18n/phonenumbers/l;
    .locals 1

    .prologue
    .line 440
    sget-object v0, Lcom/google/i18n/phonenumbers/l;->e:[Lcom/google/i18n/phonenumbers/l;

    invoke-virtual {v0}, [Lcom/google/i18n/phonenumbers/l;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/google/i18n/phonenumbers/l;

    return-object v0
.end method


# virtual methods
.method abstract a(Lcom/google/i18n/phonenumbers/ac;Ljava/lang/String;Lcom/google/i18n/phonenumbers/h;)Z
.end method
