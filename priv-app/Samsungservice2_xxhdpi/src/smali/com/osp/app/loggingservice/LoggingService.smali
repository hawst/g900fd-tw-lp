.class public Lcom/osp/app/loggingservice/LoggingService;
.super Lcom/osp/app/util/AbstractBaseService;
.source "LoggingService.java"


# static fields
.field private static b:Lcom/osp/app/loggingservice/c;


# instance fields
.field private a:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 67
    sget-object v0, Lcom/osp/app/loggingservice/c;->a:Lcom/osp/app/loggingservice/c;

    sput-object v0, Lcom/osp/app/loggingservice/LoggingService;->b:Lcom/osp/app/loggingservice/c;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 21
    invoke-direct {p0}, Lcom/osp/app/util/AbstractBaseService;-><init>()V

    .line 62
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/osp/app/loggingservice/LoggingService;->a:Ljava/lang/String;

    .line 183
    return-void
.end method

.method static synthetic a(Lcom/osp/app/loggingservice/LoggingService;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 21
    iget-object v0, p0, Lcom/osp/app/loggingservice/LoggingService;->a:Ljava/lang/String;

    return-object v0
.end method

.method public static a(Lcom/osp/app/loggingservice/c;)V
    .locals 3

    .prologue
    .line 74
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v0, "LS"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "updateLoggingStep incoming step : "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0}, Lcom/osp/app/loggingservice/c;->ordinal()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 75
    sget-object v0, Lcom/osp/app/loggingservice/c;->a:Lcom/osp/app/loggingservice/c;

    if-eq p0, v0, :cond_0

    sget-object v0, Lcom/osp/app/loggingservice/LoggingService;->b:Lcom/osp/app/loggingservice/c;

    invoke-virtual {v0}, Lcom/osp/app/loggingservice/c;->ordinal()I

    move-result v0

    invoke-virtual {p0}, Lcom/osp/app/loggingservice/c;->ordinal()I

    move-result v1

    if-ge v0, v1, :cond_1

    .line 77
    :cond_0
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v0, "LS"

    const-string v1, "Logging step updated!"

    invoke-static {v0, v1}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 78
    sput-object p0, Lcom/osp/app/loggingservice/LoggingService;->b:Lcom/osp/app/loggingservice/c;

    .line 80
    :cond_1
    return-void
.end method


# virtual methods
.method public onBind(Landroid/content/Intent;)Landroid/os/IBinder;
    .locals 1

    .prologue
    .line 118
    const/4 v0, 0x0

    return-object v0
.end method

.method public onDestroy()V
    .locals 2

    .prologue
    .line 173
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v0, "LS"

    const-string v1, "onDestroy"

    invoke-static {v0, v1}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 174
    sget-object v0, Lcom/osp/app/loggingservice/c;->a:Lcom/osp/app/loggingservice/c;

    sput-object v0, Lcom/osp/app/loggingservice/LoggingService;->b:Lcom/osp/app/loggingservice/c;

    .line 175
    invoke-super {p0}, Lcom/osp/app/util/AbstractBaseService;->onDestroy()V

    .line 176
    return-void
.end method

.method public onStartCommand(Landroid/content/Intent;II)I
    .locals 4

    .prologue
    const/4 v0, -0x1

    const/4 v3, 0x2

    .line 123
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v1, "LS"

    const-string v2, "onStartCommand"

    invoke-static {v1, v2}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 132
    invoke-static {p0}, Lcom/osp/app/util/ad;->a(Landroid/content/Context;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 134
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v0, "LS"

    const-string v1, "Unable to send log due to network disconnected."

    invoke-static {v0, v1}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 135
    invoke-virtual {p0}, Lcom/osp/app/loggingservice/LoggingService;->stopSelf()V

    .line 168
    :goto_0
    return v3

    .line 141
    :cond_0
    if-eqz p1, :cond_1

    .line 143
    const-string v1, "logging_service_mode"

    invoke-virtual {p1, v1, v0}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v0

    .line 146
    :cond_1
    packed-switch v0, :pswitch_data_0

    .line 162
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v0, "LS"

    const-string v1, "SERVICE LOGGING MODE is Invalid"

    invoke-static {v0, v1}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 163
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v0, "SERVICE LOGGING MODE is Invalid"

    invoke-static {v0}, Lcom/osp/app/util/aq;->b(Ljava/lang/String;)V

    .line 164
    invoke-virtual {p0}, Lcom/osp/app/loggingservice/LoggingService;->stopSelf()V

    goto :goto_0

    .line 149
    :pswitch_0
    const-string v0, ""

    sget-object v1, Lcom/osp/app/loggingservice/a;->a:[I

    sget-object v2, Lcom/osp/app/loggingservice/LoggingService;->b:Lcom/osp/app/loggingservice/c;

    invoke-virtual {v2}, Lcom/osp/app/loggingservice/c;->ordinal()I

    move-result v2

    aget v1, v1, v2

    packed-switch v1, :pswitch_data_1

    :goto_1
    :pswitch_1
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "getCurrLoggingStep string : "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;)V

    iput-object v0, p0, Lcom/osp/app/loggingservice/LoggingService;->a:Ljava/lang/String;

    .line 150
    iget-object v0, p0, Lcom/osp/app/loggingservice/LoggingService;->a:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 152
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v0, "LS"

    const-string v1, "Failed due to invalid step"

    invoke-static {v0, v1}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 153
    invoke-virtual {p0}, Lcom/osp/app/loggingservice/LoggingService;->stopSelf()V

    goto :goto_0

    .line 149
    :pswitch_2
    const-string v0, "0000"

    goto :goto_1

    :pswitch_3
    const-string v0, "0101"

    goto :goto_1

    :pswitch_4
    const-string v0, "0102"

    goto :goto_1

    :pswitch_5
    const-string v0, "0103"

    goto :goto_1

    :pswitch_6
    const-string v0, "0104"

    goto :goto_1

    .line 157
    :cond_2
    new-instance v0, Lcom/osp/app/loggingservice/d;

    invoke-direct {v0, p0, p3}, Lcom/osp/app/loggingservice/d;-><init>(Lcom/osp/app/loggingservice/LoggingService;I)V

    .line 158
    invoke-virtual {v0}, Lcom/osp/app/loggingservice/d;->b()V

    goto :goto_0

    .line 146
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
    .end packed-switch

    .line 149
    :pswitch_data_1
    .packed-switch 0x1
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
    .end packed-switch
.end method
