.class public final enum Lcom/osp/app/loggingservice/c;
.super Ljava/lang/Enum;
.source "LoggingService.java"


# static fields
.field public static final enum a:Lcom/osp/app/loggingservice/c;

.field public static final enum b:Lcom/osp/app/loggingservice/c;

.field public static final enum c:Lcom/osp/app/loggingservice/c;

.field public static final enum d:Lcom/osp/app/loggingservice/c;

.field public static final enum e:Lcom/osp/app/loggingservice/c;

.field public static final enum f:Lcom/osp/app/loggingservice/c;

.field private static final synthetic g:[Lcom/osp/app/loggingservice/c;


# direct methods
.method static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 40
    new-instance v0, Lcom/osp/app/loggingservice/c;

    const-string v1, "INVALID"

    invoke-direct {v0, v1, v3}, Lcom/osp/app/loggingservice/c;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/osp/app/loggingservice/c;->a:Lcom/osp/app/loggingservice/c;

    new-instance v0, Lcom/osp/app/loggingservice/c;

    const-string v1, "MAIN_PAGE"

    invoke-direct {v0, v1, v4}, Lcom/osp/app/loggingservice/c;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/osp/app/loggingservice/c;->b:Lcom/osp/app/loggingservice/c;

    new-instance v0, Lcom/osp/app/loggingservice/c;

    const-string v1, "NAME_VALIDATION_PAGE"

    invoke-direct {v0, v1, v5}, Lcom/osp/app/loggingservice/c;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/osp/app/loggingservice/c;->c:Lcom/osp/app/loggingservice/c;

    new-instance v0, Lcom/osp/app/loggingservice/c;

    const-string v1, "SIGNUP_PAGE"

    invoke-direct {v0, v1, v6}, Lcom/osp/app/loggingservice/c;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/osp/app/loggingservice/c;->d:Lcom/osp/app/loggingservice/c;

    new-instance v0, Lcom/osp/app/loggingservice/c;

    const-string v1, "SMS_VALIDATION_PAGE"

    invoke-direct {v0, v1, v7}, Lcom/osp/app/loggingservice/c;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/osp/app/loggingservice/c;->e:Lcom/osp/app/loggingservice/c;

    new-instance v0, Lcom/osp/app/loggingservice/c;

    const-string v1, "TNC_PAGE"

    const/4 v2, 0x5

    invoke-direct {v0, v1, v2}, Lcom/osp/app/loggingservice/c;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/osp/app/loggingservice/c;->f:Lcom/osp/app/loggingservice/c;

    .line 38
    const/4 v0, 0x6

    new-array v0, v0, [Lcom/osp/app/loggingservice/c;

    sget-object v1, Lcom/osp/app/loggingservice/c;->a:Lcom/osp/app/loggingservice/c;

    aput-object v1, v0, v3

    sget-object v1, Lcom/osp/app/loggingservice/c;->b:Lcom/osp/app/loggingservice/c;

    aput-object v1, v0, v4

    sget-object v1, Lcom/osp/app/loggingservice/c;->c:Lcom/osp/app/loggingservice/c;

    aput-object v1, v0, v5

    sget-object v1, Lcom/osp/app/loggingservice/c;->d:Lcom/osp/app/loggingservice/c;

    aput-object v1, v0, v6

    sget-object v1, Lcom/osp/app/loggingservice/c;->e:Lcom/osp/app/loggingservice/c;

    aput-object v1, v0, v7

    const/4 v1, 0x5

    sget-object v2, Lcom/osp/app/loggingservice/c;->f:Lcom/osp/app/loggingservice/c;

    aput-object v2, v0, v1

    sput-object v0, Lcom/osp/app/loggingservice/c;->g:[Lcom/osp/app/loggingservice/c;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0

    .prologue
    .line 38
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/osp/app/loggingservice/c;
    .locals 1

    .prologue
    .line 38
    const-class v0, Lcom/osp/app/loggingservice/c;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/osp/app/loggingservice/c;

    return-object v0
.end method

.method public static values()[Lcom/osp/app/loggingservice/c;
    .locals 1

    .prologue
    .line 38
    sget-object v0, Lcom/osp/app/loggingservice/c;->g:[Lcom/osp/app/loggingservice/c;

    invoke-virtual {v0}, [Lcom/osp/app/loggingservice/c;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/osp/app/loggingservice/c;

    return-object v0
.end method
