.class final Lcom/osp/app/loggingservice/d;
.super Lcom/msc/c/b;
.source "LoggingService.java"


# instance fields
.field final synthetic c:Lcom/osp/app/loggingservice/LoggingService;

.field private d:J

.field private final e:I


# direct methods
.method public constructor <init>(Lcom/osp/app/loggingservice/LoggingService;I)V
    .locals 0

    .prologue
    .line 188
    iput-object p1, p0, Lcom/osp/app/loggingservice/d;->c:Lcom/osp/app/loggingservice/LoggingService;

    .line 189
    invoke-direct {p0, p1}, Lcom/msc/c/b;-><init>(Landroid/content/Context;)V

    .line 190
    invoke-virtual {p0}, Lcom/osp/app/loggingservice/d;->c()V

    .line 192
    iput p2, p0, Lcom/osp/app/loggingservice/d;->e:I

    .line 193
    return-void
.end method


# virtual methods
.method protected final varargs a()Ljava/lang/Boolean;
    .locals 3

    .prologue
    .line 197
    invoke-static {}, Lcom/msc/c/g;->a()Lcom/msc/c/g;

    iget-object v0, p0, Lcom/osp/app/loggingservice/d;->c:Lcom/osp/app/loggingservice/LoggingService;

    iget-object v1, p0, Lcom/osp/app/loggingservice/d;->c:Lcom/osp/app/loggingservice/LoggingService;

    invoke-static {v1}, Lcom/osp/app/loggingservice/LoggingService;->a(Lcom/osp/app/loggingservice/LoggingService;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1, p0}, Lcom/msc/c/g;->k(Landroid/content/Context;Ljava/lang/String;Lcom/msc/b/h;)J

    move-result-wide v0

    iput-wide v0, p0, Lcom/osp/app/loggingservice/d;->d:J

    iget-wide v0, p0, Lcom/osp/app/loggingservice/d;->d:J

    invoke-virtual {p0, v0, v1}, Lcom/osp/app/loggingservice/d;->b(J)V

    iget-wide v0, p0, Lcom/osp/app/loggingservice/d;->d:J

    const-string v2, "from_xml"

    invoke-virtual {p0, v0, v1, v2}, Lcom/osp/app/loggingservice/d;->a(JLjava/lang/String;)V

    iget-wide v0, p0, Lcom/osp/app/loggingservice/d;->d:J

    sget-object v2, Lcom/msc/b/g;->b:Lcom/msc/b/g;

    invoke-static {v0, v1, v2}, Lcom/msc/c/g;->a(JLcom/msc/b/g;)V

    .line 198
    const/4 v0, 0x0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    return-object v0
.end method

.method public final declared-synchronized a(Lcom/msc/b/c;)V
    .locals 4

    .prologue
    .line 210
    monitor-enter p0

    :try_start_0
    invoke-super {p0, p1}, Lcom/msc/c/b;->a(Lcom/msc/b/c;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 212
    if-nez p1, :cond_1

    .line 223
    :cond_0
    :goto_0
    monitor-exit p0

    return-void

    .line 217
    :cond_1
    :try_start_1
    invoke-virtual {p1}, Lcom/msc/b/c;->a()J

    move-result-wide v0

    .line 219
    iget-wide v2, p0, Lcom/osp/app/loggingservice/d;->d:J

    cmp-long v0, v0, v2

    if-nez v0, :cond_0

    .line 221
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v0, "LS"

    const-string v1, "requestLoggingCanceledStep success"

    invoke-static {v0, v1}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 210
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method protected final a(Ljava/lang/Boolean;)V
    .locals 2

    .prologue
    .line 203
    invoke-super {p0, p1}, Lcom/msc/c/b;->a(Ljava/lang/Boolean;)V

    .line 205
    iget-object v0, p0, Lcom/osp/app/loggingservice/d;->c:Lcom/osp/app/loggingservice/LoggingService;

    iget v1, p0, Lcom/osp/app/loggingservice/d;->e:I

    invoke-virtual {v0, v1}, Lcom/osp/app/loggingservice/LoggingService;->stopSelf(I)V

    .line 206
    return-void
.end method

.method public final declared-synchronized b(Lcom/msc/b/c;)V
    .locals 4

    .prologue
    .line 227
    monitor-enter p0

    :try_start_0
    invoke-super {p0, p1}, Lcom/msc/c/b;->b(Lcom/msc/b/c;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 229
    if-nez p1, :cond_1

    .line 240
    :cond_0
    :goto_0
    monitor-exit p0

    return-void

    .line 234
    :cond_1
    :try_start_1
    invoke-virtual {p1}, Lcom/msc/b/c;->a()J

    move-result-wide v0

    .line 236
    iget-wide v2, p0, Lcom/osp/app/loggingservice/d;->d:J

    cmp-long v0, v0, v2

    if-nez v0, :cond_0

    .line 238
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v0, "LS"

    const-string v1, "requestLoggingCanceledStep failed"

    invoke-static {v0, v1}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 227
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method protected final synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 183
    invoke-virtual {p0}, Lcom/osp/app/loggingservice/d;->a()Ljava/lang/Boolean;

    move-result-object v0

    return-object v0
.end method

.method protected final synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 183
    check-cast p1, Ljava/lang/Boolean;

    invoke-virtual {p0, p1}, Lcom/osp/app/loggingservice/d;->a(Ljava/lang/Boolean;)V

    return-void
.end method
