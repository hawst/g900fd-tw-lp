.class public final Lcom/osp/app/pushmarketing/a;
.super Ljava/lang/Object;
.source "PushMarketingNotification.java"


# instance fields
.field private final a:Ljava/lang/String;

.field private b:I

.field private c:Ljava/lang/String;

.field private d:Lcom/osp/app/pushmarketing/e;

.field private e:Landroid/graphics/Bitmap;

.field private f:Landroid/graphics/Bitmap;

.field private g:Z

.field private h:Z

.field private final i:Ljava/lang/String;

.field private final j:Ljava/lang/String;

.field private final k:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x0

    .line 90
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 30
    const-string v0, "PMN"

    iput-object v0, p0, Lcom/osp/app/pushmarketing/a;->a:Ljava/lang/String;

    .line 46
    iput-object v1, p0, Lcom/osp/app/pushmarketing/a;->d:Lcom/osp/app/pushmarketing/e;

    .line 51
    iput-object v1, p0, Lcom/osp/app/pushmarketing/a;->e:Landroid/graphics/Bitmap;

    .line 56
    iput-object v1, p0, Lcom/osp/app/pushmarketing/a;->f:Landroid/graphics/Bitmap;

    .line 61
    iput-boolean v2, p0, Lcom/osp/app/pushmarketing/a;->g:Z

    .line 66
    iput-boolean v2, p0, Lcom/osp/app/pushmarketing/a;->h:Z

    .line 71
    const-string v0, "00"

    iput-object v0, p0, Lcom/osp/app/pushmarketing/a;->i:Ljava/lang/String;

    .line 76
    const-string v0, "01"

    iput-object v0, p0, Lcom/osp/app/pushmarketing/a;->j:Ljava/lang/String;

    .line 81
    const-string v0, "02"

    iput-object v0, p0, Lcom/osp/app/pushmarketing/a;->k:Ljava/lang/String;

    .line 92
    return-void
.end method

.method static synthetic a(Lcom/osp/app/pushmarketing/a;Landroid/graphics/Bitmap;)Landroid/graphics/Bitmap;
    .locals 0

    .prologue
    .line 25
    iput-object p1, p0, Lcom/osp/app/pushmarketing/a;->e:Landroid/graphics/Bitmap;

    return-object p1
.end method

.method private a(Landroid/content/Context;)V
    .locals 9

    .prologue
    const/high16 v8, 0x8000000

    const/4 v7, 0x4

    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 179
    iget-boolean v0, p0, Lcom/osp/app/pushmarketing/a;->g:Z

    if-eqz v0, :cond_2

    iget-boolean v0, p0, Lcom/osp/app/pushmarketing/a;->h:Z

    if-eqz v0, :cond_2

    .line 182
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v0, "PMN"

    const-string v3, "buildNotification"

    invoke-static {v0, v3}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 184
    new-instance v3, Lcom/msc/sa/c/g;

    invoke-direct {v3}, Lcom/msc/sa/c/g;-><init>()V

    .line 187
    sget-object v0, Lcom/osp/app/pushmarketing/l;->a:Lcom/osp/app/pushmarketing/k;

    iget-object v0, p0, Lcom/osp/app/pushmarketing/a;->d:Lcom/osp/app/pushmarketing/e;

    invoke-virtual {v0}, Lcom/osp/app/pushmarketing/e;->a()[Ljava/lang/String;

    move-result-object v0

    invoke-static {p1, v0, v1}, Lcom/osp/app/pushmarketing/k;->a(Landroid/content/Context;[Ljava/lang/String;I)I

    move-result v0

    invoke-virtual {v3, v0}, Lcom/msc/sa/c/g;->a(I)V

    .line 190
    iget-object v0, p0, Lcom/osp/app/pushmarketing/a;->d:Lcom/osp/app/pushmarketing/e;

    invoke-virtual {v0}, Lcom/osp/app/pushmarketing/e;->b()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v3, v0}, Lcom/msc/sa/c/g;->a(Ljava/lang/String;)V

    .line 193
    iget-object v0, p0, Lcom/osp/app/pushmarketing/a;->d:Lcom/osp/app/pushmarketing/e;

    invoke-virtual {v0}, Lcom/osp/app/pushmarketing/e;->c()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v3, v0}, Lcom/msc/sa/c/g;->b(Ljava/lang/String;)V

    .line 196
    iget-object v0, p0, Lcom/osp/app/pushmarketing/a;->d:Lcom/osp/app/pushmarketing/e;

    invoke-virtual {v0}, Lcom/osp/app/pushmarketing/e;->l()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v3, v0}, Lcom/msc/sa/c/g;->c(Ljava/lang/String;)V

    .line 199
    iget-object v0, p0, Lcom/osp/app/pushmarketing/a;->e:Landroid/graphics/Bitmap;

    invoke-virtual {v3, v0}, Lcom/msc/sa/c/g;->b(Landroid/graphics/Bitmap;)V

    .line 202
    invoke-virtual {v3}, Lcom/msc/sa/c/g;->a()V

    .line 205
    iget-object v0, p0, Lcom/osp/app/pushmarketing/a;->f:Landroid/graphics/Bitmap;

    invoke-virtual {v3, v0}, Lcom/msc/sa/c/g;->a(Landroid/graphics/Bitmap;)V

    .line 208
    iget-object v0, p0, Lcom/osp/app/pushmarketing/a;->d:Lcom/osp/app/pushmarketing/e;

    invoke-virtual {v0}, Lcom/osp/app/pushmarketing/e;->k()Ljava/lang/String;

    move-result-object v0

    .line 210
    const-string v4, "bigtext"

    invoke-virtual {v4, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_3

    .line 212
    sget-object v0, Lcom/msc/sa/c/i;->b:Lcom/msc/sa/c/i;

    .line 225
    :goto_0
    invoke-virtual {v3, v0}, Lcom/msc/sa/c/g;->a(Lcom/msc/sa/c/i;)V

    .line 226
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v4, "PMN Noti ID(PMS) : "

    invoke-direct {v0, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v4, p0, Lcom/osp/app/pushmarketing/a;->d:Lcom/osp/app/pushmarketing/e;

    invoke-virtual {v4}, Lcom/osp/app/pushmarketing/e;->d()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;)V

    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v4, "PMN Noti ID(Notification Manager) : "

    invoke-direct {v0, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v4, p0, Lcom/osp/app/pushmarketing/a;->b:I

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;)V

    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v4, "PMN Campaign ID : "

    invoke-direct {v0, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v4, p0, Lcom/osp/app/pushmarketing/a;->d:Lcom/osp/app/pushmarketing/e;

    invoke-virtual {v4}, Lcom/osp/app/pushmarketing/e;->e()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;)V

    sget-object v0, Lcom/osp/app/pushmarketing/l;->a:Lcom/osp/app/pushmarketing/k;

    iget-object v0, p0, Lcom/osp/app/pushmarketing/a;->d:Lcom/osp/app/pushmarketing/e;

    invoke-static {v0, v2}, Lcom/osp/app/pushmarketing/k;->a(Lcom/osp/app/pushmarketing/e;I)Z

    move-result v0

    if-nez v0, :cond_a

    new-instance v0, Landroid/content/Intent;

    const-string v4, "com.osp.app.pushmarketing.PushMarketingService"

    invoke-direct {v0, v4}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    sget-object v4, Lcom/osp/app/pushmarketing/l;->a:Lcom/osp/app/pushmarketing/k;

    iget-object v4, p0, Lcom/osp/app/pushmarketing/a;->d:Lcom/osp/app/pushmarketing/e;

    if-nez v4, :cond_6

    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v2, "PMV"

    const-string v4, "Content Target Enabled : payload is null"

    invoke-static {v2, v4}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;)V

    :goto_1
    if-eqz v1, :cond_9

    const-string v1, "push_marketing_service_mode"

    invoke-virtual {v0, v1, v7}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    const-string v1, "pref_notification_id"

    iget v2, p0, Lcom/osp/app/pushmarketing/a;->b:I

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    const-string v1, "notification_id"

    iget-object v2, p0, Lcom/osp/app/pushmarketing/a;->d:Lcom/osp/app/pushmarketing/e;

    invoke-virtual {v2}, Lcom/osp/app/pushmarketing/e;->d()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    const-string v1, "target_package"

    iget-object v2, p0, Lcom/osp/app/pushmarketing/a;->d:Lcom/osp/app/pushmarketing/e;

    invoke-virtual {v2}, Lcom/osp/app/pushmarketing/e;->f()[Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;[Ljava/lang/String;)Landroid/content/Intent;

    const-string v1, "link_type"

    iget-object v2, p0, Lcom/osp/app/pushmarketing/a;->d:Lcom/osp/app/pushmarketing/e;

    invoke-virtual {v2}, Lcom/osp/app/pushmarketing/e;->i()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    const-string v1, "target_link"

    iget-object v2, p0, Lcom/osp/app/pushmarketing/a;->d:Lcom/osp/app/pushmarketing/e;

    invoke-virtual {v2}, Lcom/osp/app/pushmarketing/e;->g()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    const-string v1, "error_link"

    iget-object v2, p0, Lcom/osp/app/pushmarketing/a;->d:Lcom/osp/app/pushmarketing/e;

    invoke-virtual {v2}, Lcom/osp/app/pushmarketing/e;->j()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    const-string v1, "campaign_id"

    iget-object v2, p0, Lcom/osp/app/pushmarketing/a;->d:Lcom/osp/app/pushmarketing/e;

    invoke-virtual {v2}, Lcom/osp/app/pushmarketing/e;->e()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    const-string v1, "reg_id"

    iget-object v2, p0, Lcom/osp/app/pushmarketing/a;->c:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    :goto_2
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    iget v2, p0, Lcom/osp/app/pushmarketing/a;->b:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "00"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-static {p1, v1, v0, v8}, Landroid/app/PendingIntent;->getService(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v0

    invoke-virtual {v3, v0}, Lcom/msc/sa/c/g;->a(Landroid/app/PendingIntent;)V

    .line 228
    :cond_0
    :goto_3
    invoke-virtual {v3, p1}, Lcom/msc/sa/c/g;->a(Landroid/content/Context;)Landroid/app/Notification;

    move-result-object v1

    .line 230
    if-eqz v1, :cond_1

    .line 232
    iget v0, v1, Landroid/app/Notification;->flags:I

    or-int/lit8 v0, v0, 0x10

    iput v0, v1, Landroid/app/Notification;->flags:I

    .line 235
    :cond_1
    const-string v0, "notification"

    invoke-virtual {p1, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/NotificationManager;

    .line 237
    iget v2, p0, Lcom/osp/app/pushmarketing/a;->b:I

    invoke-virtual {v0, v2, v1}, Landroid/app/NotificationManager;->notify(ILandroid/app/Notification;)V

    .line 239
    :cond_2
    return-void

    .line 214
    :cond_3
    const-string v4, "action"

    invoke-virtual {v4, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_4

    .line 216
    sget-object v0, Lcom/msc/sa/c/i;->c:Lcom/msc/sa/c/i;

    goto/16 :goto_0

    .line 217
    :cond_4
    const-string v4, "bigpicture"

    invoke-virtual {v4, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_5

    .line 219
    sget-object v0, Lcom/msc/sa/c/i;->a:Lcom/msc/sa/c/i;

    goto/16 :goto_0

    .line 222
    :cond_5
    sget-object v0, Lcom/msc/sa/c/i;->d:Lcom/msc/sa/c/i;

    goto/16 :goto_0

    .line 226
    :cond_6
    const-string v5, "deeplink"

    invoke-virtual {v4}, Lcom/osp/app/pushmarketing/e;->i()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-nez v5, :cond_7

    const-string v5, "weblink"

    invoke-virtual {v4}, Lcom/osp/app/pushmarketing/e;->i()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-nez v5, :cond_7

    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v2, "PMV"

    const-string v4, "Content Target Enabled : link type is invalid"

    invoke-static {v2, v4}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_1

    :cond_7
    invoke-virtual {v4}, Lcom/osp/app/pushmarketing/e;->g()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Lcom/osp/app/pushmarketing/k;->a(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_8

    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v2, "PMV"

    const-string v4, "Content Target Enabled : target link is null"

    invoke-static {v2, v4}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_1

    :cond_8
    move v1, v2

    goto/16 :goto_1

    :cond_9
    const-string v1, "push_marketing_service_mode"

    const/4 v2, -0x1

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    goto/16 :goto_2

    :cond_a
    new-instance v0, Landroid/content/Intent;

    const-string v4, "com.osp.app.pushmarketing.PushMarketingService"

    invoke-direct {v0, v4}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    const-string v4, "push_marketing_service_mode"

    invoke-virtual {v0, v4, v7}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    const-string v4, "pref_notification_id"

    iget v5, p0, Lcom/osp/app/pushmarketing/a;->b:I

    invoke-virtual {v0, v4, v5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    const-string v4, "notification_id"

    iget-object v5, p0, Lcom/osp/app/pushmarketing/a;->d:Lcom/osp/app/pushmarketing/e;

    invoke-virtual {v5}, Lcom/osp/app/pushmarketing/e;->d()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v0, v4, v5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    const-string v4, "link_type"

    iget-object v5, p0, Lcom/osp/app/pushmarketing/a;->d:Lcom/osp/app/pushmarketing/e;

    invoke-virtual {v5}, Lcom/osp/app/pushmarketing/e;->t()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v0, v4, v5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    const-string v4, "target_package"

    iget-object v5, p0, Lcom/osp/app/pushmarketing/a;->d:Lcom/osp/app/pushmarketing/e;

    invoke-virtual {v5}, Lcom/osp/app/pushmarketing/e;->r()[Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v0, v4, v5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;[Ljava/lang/String;)Landroid/content/Intent;

    const-string v4, "target_link"

    iget-object v5, p0, Lcom/osp/app/pushmarketing/a;->d:Lcom/osp/app/pushmarketing/e;

    invoke-virtual {v5}, Lcom/osp/app/pushmarketing/e;->w()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v0, v4, v5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    const-string v4, "error_link"

    iget-object v5, p0, Lcom/osp/app/pushmarketing/a;->d:Lcom/osp/app/pushmarketing/e;

    invoke-virtual {v5}, Lcom/osp/app/pushmarketing/e;->z()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v0, v4, v5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    const-string v4, "campaign_id"

    iget-object v5, p0, Lcom/osp/app/pushmarketing/a;->d:Lcom/osp/app/pushmarketing/e;

    invoke-virtual {v5}, Lcom/osp/app/pushmarketing/e;->e()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v0, v4, v5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    const-string v4, "reg_id"

    iget-object v5, p0, Lcom/osp/app/pushmarketing/a;->c:Ljava/lang/String;

    invoke-virtual {v0, v4, v5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    iget v5, p0, Lcom/osp/app/pushmarketing/a;->b:I

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "01"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/Integer;->intValue()I

    move-result v4

    invoke-static {p1, v4, v0, v8}, Landroid/app/PendingIntent;->getService(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v4

    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v5, "PMN addAction 1 prefNotiID : "

    invoke-direct {v0, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v5, p0, Lcom/osp/app/pushmarketing/a;->b:I

    invoke-virtual {v0, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/osp/app/pushmarketing/a;->d:Lcom/osp/app/pushmarketing/e;

    invoke-virtual {v0}, Lcom/osp/app/pushmarketing/e;->B()Z

    move-result v0

    if-eqz v0, :cond_c

    sget-object v0, Lcom/osp/app/pushmarketing/l;->a:Lcom/osp/app/pushmarketing/k;

    iget-object v0, p0, Lcom/osp/app/pushmarketing/a;->d:Lcom/osp/app/pushmarketing/e;

    invoke-virtual {v0}, Lcom/osp/app/pushmarketing/e;->r()[Ljava/lang/String;

    move-result-object v0

    invoke-static {p1, v0, v2}, Lcom/osp/app/pushmarketing/k;->a(Landroid/content/Context;[Ljava/lang/String;I)I

    move-result v0

    :goto_4
    iget-object v5, p0, Lcom/osp/app/pushmarketing/a;->d:Lcom/osp/app/pushmarketing/e;

    invoke-virtual {v5}, Lcom/osp/app/pushmarketing/e;->o()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v3, v0, v5, v4}, Lcom/msc/sa/c/g;->a(ILjava/lang/String;Landroid/app/PendingIntent;)V

    invoke-virtual {v3, v4}, Lcom/msc/sa/c/g;->a(Landroid/app/PendingIntent;)V

    sget-object v0, Lcom/osp/app/pushmarketing/l;->a:Lcom/osp/app/pushmarketing/k;

    iget-object v0, p0, Lcom/osp/app/pushmarketing/a;->d:Lcom/osp/app/pushmarketing/e;

    const/4 v4, 0x2

    invoke-static {v0, v4}, Lcom/osp/app/pushmarketing/k;->a(Lcom/osp/app/pushmarketing/e;I)Z

    move-result v0

    if-eqz v0, :cond_0

    new-instance v0, Landroid/content/Intent;

    const-string v4, "com.osp.app.pushmarketing.PushMarketingService"

    invoke-direct {v0, v4}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    const-string v4, "push_marketing_service_mode"

    invoke-virtual {v0, v4, v7}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    const-string v4, "pref_notification_id"

    iget v5, p0, Lcom/osp/app/pushmarketing/a;->b:I

    invoke-virtual {v0, v4, v5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    const-string v4, "notification_id"

    iget-object v5, p0, Lcom/osp/app/pushmarketing/a;->d:Lcom/osp/app/pushmarketing/e;

    invoke-virtual {v5}, Lcom/osp/app/pushmarketing/e;->d()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v0, v4, v5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    const-string v4, "target_package"

    iget-object v5, p0, Lcom/osp/app/pushmarketing/a;->d:Lcom/osp/app/pushmarketing/e;

    invoke-virtual {v5}, Lcom/osp/app/pushmarketing/e;->s()[Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v0, v4, v5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;[Ljava/lang/String;)Landroid/content/Intent;

    const-string v4, "link_type"

    iget-object v5, p0, Lcom/osp/app/pushmarketing/a;->d:Lcom/osp/app/pushmarketing/e;

    invoke-virtual {v5}, Lcom/osp/app/pushmarketing/e;->u()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v0, v4, v5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    const-string v4, "target_link"

    iget-object v5, p0, Lcom/osp/app/pushmarketing/a;->d:Lcom/osp/app/pushmarketing/e;

    invoke-virtual {v5}, Lcom/osp/app/pushmarketing/e;->x()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v0, v4, v5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    const-string v4, "error_link"

    iget-object v5, p0, Lcom/osp/app/pushmarketing/a;->d:Lcom/osp/app/pushmarketing/e;

    invoke-virtual {v5}, Lcom/osp/app/pushmarketing/e;->A()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v0, v4, v5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    const-string v4, "campaign_id"

    iget-object v5, p0, Lcom/osp/app/pushmarketing/a;->d:Lcom/osp/app/pushmarketing/e;

    invoke-virtual {v5}, Lcom/osp/app/pushmarketing/e;->e()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v0, v4, v5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "PMN regId : "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v5, p0, Lcom/osp/app/pushmarketing/a;->c:Ljava/lang/String;

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;)V

    const-string v4, "reg_id"

    iget-object v5, p0, Lcom/osp/app/pushmarketing/a;->c:Ljava/lang/String;

    invoke-virtual {v0, v4, v5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    iget v5, p0, Lcom/osp/app/pushmarketing/a;->b:I

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "02"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/Integer;->intValue()I

    move-result v4

    invoke-static {p1, v4, v0, v8}, Landroid/app/PendingIntent;->getService(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v0

    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "PMN addAction 2 prefNotiID : "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v5, p0, Lcom/osp/app/pushmarketing/a;->b:I

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;)V

    iget-object v4, p0, Lcom/osp/app/pushmarketing/a;->d:Lcom/osp/app/pushmarketing/e;

    invoke-virtual {v4}, Lcom/osp/app/pushmarketing/e;->C()Z

    move-result v4

    if-eqz v4, :cond_b

    sget-object v1, Lcom/osp/app/pushmarketing/l;->a:Lcom/osp/app/pushmarketing/k;

    iget-object v1, p0, Lcom/osp/app/pushmarketing/a;->d:Lcom/osp/app/pushmarketing/e;

    invoke-virtual {v1}, Lcom/osp/app/pushmarketing/e;->s()[Ljava/lang/String;

    move-result-object v1

    invoke-static {p1, v1, v2}, Lcom/osp/app/pushmarketing/k;->a(Landroid/content/Context;[Ljava/lang/String;I)I

    move-result v1

    :cond_b
    iget-object v2, p0, Lcom/osp/app/pushmarketing/a;->d:Lcom/osp/app/pushmarketing/e;

    invoke-virtual {v2}, Lcom/osp/app/pushmarketing/e;->p()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v3, v1, v2, v0}, Lcom/msc/sa/c/g;->a(ILjava/lang/String;Landroid/app/PendingIntent;)V

    goto/16 :goto_3

    :cond_c
    move v0, v1

    goto/16 :goto_4
.end method

.method static synthetic a(Lcom/osp/app/pushmarketing/a;Landroid/content/Context;)V
    .locals 0

    .prologue
    .line 25
    invoke-direct {p0, p1}, Lcom/osp/app/pushmarketing/a;->a(Landroid/content/Context;)V

    return-void
.end method

.method static synthetic a(Lcom/osp/app/pushmarketing/a;)Z
    .locals 1

    .prologue
    .line 25
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/osp/app/pushmarketing/a;->g:Z

    return v0
.end method

.method static synthetic b(Lcom/osp/app/pushmarketing/a;Landroid/graphics/Bitmap;)Landroid/graphics/Bitmap;
    .locals 0

    .prologue
    .line 25
    iput-object p1, p0, Lcom/osp/app/pushmarketing/a;->f:Landroid/graphics/Bitmap;

    return-object p1
.end method

.method static synthetic b(Lcom/osp/app/pushmarketing/a;)Lcom/osp/app/pushmarketing/e;
    .locals 1

    .prologue
    .line 25
    iget-object v0, p0, Lcom/osp/app/pushmarketing/a;->d:Lcom/osp/app/pushmarketing/e;

    return-object v0
.end method

.method static synthetic c(Lcom/osp/app/pushmarketing/a;)Z
    .locals 1

    .prologue
    .line 25
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/osp/app/pushmarketing/a;->h:Z

    return v0
.end method


# virtual methods
.method public final a(Landroid/content/Context;Ljava/lang/String;Lcom/osp/app/pushmarketing/e;Ljava/lang/String;)V
    .locals 5

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 104
    sget-object v0, Lcom/osp/app/pushmarketing/l;->a:Lcom/osp/app/pushmarketing/k;

    invoke-virtual {v0, p1}, Lcom/osp/app/pushmarketing/k;->a(Landroid/content/Context;)I

    move-result v0

    iput v0, p0, Lcom/osp/app/pushmarketing/a;->b:I

    .line 106
    iput-object p3, p0, Lcom/osp/app/pushmarketing/a;->d:Lcom/osp/app/pushmarketing/e;

    .line 107
    iput-object p4, p0, Lcom/osp/app/pushmarketing/a;->c:Ljava/lang/String;

    .line 109
    sget-object v0, Lcom/osp/app/pushmarketing/l;->a:Lcom/osp/app/pushmarketing/k;

    invoke-static {p4}, Lcom/osp/app/pushmarketing/k;->a(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 111
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v0, "PMN"

    const-string v1, "regId is Null"

    invoke-static {v0, v1}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 133
    :goto_0
    return-void

    .line 115
    :cond_0
    sget-object v0, Lcom/osp/app/pushmarketing/l;->a:Lcom/osp/app/pushmarketing/k;

    if-nez p3, :cond_1

    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v0, "PMV"

    const-string v3, "Mandatory Check : payload is null"

    invoke-static {v0, v3}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;)V

    move v0, v1

    :goto_1
    if-eqz v0, :cond_6

    sget-object v0, Lcom/osp/app/pushmarketing/l;->a:Lcom/osp/app/pushmarketing/k;

    const-string v0, "bigpicture"

    invoke-virtual {p3}, Lcom/osp/app/pushmarketing/e;->k()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    .line 121
    sget-object v0, Lcom/osp/app/pushmarketing/l;->a:Lcom/osp/app/pushmarketing/k;

    invoke-static {p1, p3}, Lcom/osp/app/pushmarketing/k;->a(Landroid/content/Context;Lcom/osp/app/pushmarketing/e;)Z

    move-result v0

    if-nez v0, :cond_7

    .line 123
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v0, "PMN"

    const-string v1, "DUID is not valid"

    invoke-static {v0, v1}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 115
    :cond_1
    invoke-virtual {p3}, Lcom/osp/app/pushmarketing/e;->b()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/osp/app/pushmarketing/k;->a(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v0, "PMV"

    const-string v3, "Mandatory Check : Content Title is null"

    invoke-static {v0, v3}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;)V

    move v0, v1

    goto :goto_1

    :cond_2
    invoke-virtual {p3}, Lcom/osp/app/pushmarketing/e;->c()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/osp/app/pushmarketing/k;->a(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v0, "PMV"

    const-string v3, "Mandatory Check : Content Text is null"

    invoke-static {v0, v3}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;)V

    move v0, v1

    goto :goto_1

    :cond_3
    invoke-virtual {p3}, Lcom/osp/app/pushmarketing/e;->d()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/osp/app/pushmarketing/k;->a(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_4

    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v0, "PMV"

    const-string v3, "Mandatory Check : NotiId is null"

    invoke-static {v0, v3}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;)V

    move v0, v1

    goto :goto_1

    :cond_4
    invoke-virtual {p3}, Lcom/osp/app/pushmarketing/e;->e()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/osp/app/pushmarketing/k;->a(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_5

    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v0, "PMV"

    const-string v3, "Mandatory Check : CampaignId is null"

    invoke-static {v0, v3}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;)V

    move v0, v1

    goto :goto_1

    :cond_5
    move v0, v2

    goto :goto_1

    .line 117
    :cond_6
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v0, "PMN"

    const-string v1, "Mandatory parameter is not valid"

    invoke-static {v0, v1}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 127
    :cond_7
    const-string v0, "bigpicture"

    iget-object v3, p0, Lcom/osp/app/pushmarketing/a;->d:Lcom/osp/app/pushmarketing/e;

    invoke-virtual {v3}, Lcom/osp/app/pushmarketing/e;->k()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_9

    iget-object v0, p0, Lcom/osp/app/pushmarketing/a;->d:Lcom/osp/app/pushmarketing/e;

    invoke-virtual {v0}, Lcom/osp/app/pushmarketing/e;->n()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_8

    iget-object v0, p0, Lcom/osp/app/pushmarketing/a;->d:Lcom/osp/app/pushmarketing/e;

    invoke-virtual {v0}, Lcom/osp/app/pushmarketing/e;->n()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_8

    new-instance v0, Lcom/osp/app/pushmarketing/c;

    iget-object v3, p0, Lcom/osp/app/pushmarketing/a;->d:Lcom/osp/app/pushmarketing/e;

    invoke-virtual {v3}, Lcom/osp/app/pushmarketing/e;->n()Ljava/lang/String;

    move-result-object v3

    sget-object v4, Lcom/osp/app/pushmarketing/d;->b:Lcom/osp/app/pushmarketing/d;

    invoke-direct {v0, p0, p1, v3, v4}, Lcom/osp/app/pushmarketing/c;-><init>(Lcom/osp/app/pushmarketing/a;Landroid/content/Context;Ljava/lang/String;Lcom/osp/app/pushmarketing/d;)V

    new-array v3, v1, [Ljava/lang/Void;

    invoke-virtual {v0, v3}, Lcom/osp/app/pushmarketing/c;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    :goto_2
    iget-object v0, p0, Lcom/osp/app/pushmarketing/a;->d:Lcom/osp/app/pushmarketing/e;

    invoke-virtual {v0}, Lcom/osp/app/pushmarketing/e;->m()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_a

    iget-object v0, p0, Lcom/osp/app/pushmarketing/a;->d:Lcom/osp/app/pushmarketing/e;

    invoke-virtual {v0}, Lcom/osp/app/pushmarketing/e;->m()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_a

    new-instance v0, Lcom/osp/app/pushmarketing/c;

    iget-object v2, p0, Lcom/osp/app/pushmarketing/a;->d:Lcom/osp/app/pushmarketing/e;

    invoke-virtual {v2}, Lcom/osp/app/pushmarketing/e;->m()Ljava/lang/String;

    move-result-object v2

    sget-object v3, Lcom/osp/app/pushmarketing/d;->a:Lcom/osp/app/pushmarketing/d;

    invoke-direct {v0, p0, p1, v2, v3}, Lcom/osp/app/pushmarketing/c;-><init>(Lcom/osp/app/pushmarketing/a;Landroid/content/Context;Ljava/lang/String;Lcom/osp/app/pushmarketing/d;)V

    new-array v1, v1, [Ljava/lang/Void;

    invoke-virtual {v0, v1}, Lcom/osp/app/pushmarketing/c;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    .line 129
    :goto_3
    invoke-direct {p0, p1}, Lcom/osp/app/pushmarketing/a;->a(Landroid/content/Context;)V

    .line 131
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "input : "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", parse : "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p3}, Lcom/osp/app/pushmarketing/e;->d()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;)V

    goto/16 :goto_0

    .line 127
    :cond_8
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v0, "PMN"

    const-string v3, "BigPicture url is empty, Change Style to default"

    invoke-static {v0, v3}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/osp/app/pushmarketing/a;->d:Lcom/osp/app/pushmarketing/e;

    const-string v3, "normal"

    invoke-virtual {v0, v3}, Lcom/osp/app/pushmarketing/e;->a(Ljava/lang/String;)V

    :cond_9
    iput-boolean v2, p0, Lcom/osp/app/pushmarketing/a;->h:Z

    goto :goto_2

    :cond_a
    iput-boolean v2, p0, Lcom/osp/app/pushmarketing/a;->g:Z

    goto :goto_3
.end method
