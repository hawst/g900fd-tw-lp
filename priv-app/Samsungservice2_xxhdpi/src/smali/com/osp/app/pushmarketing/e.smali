.class public final Lcom/osp/app/pushmarketing/e;
.super Ljava/lang/Object;
.source "PushMarketingPayload.java"


# instance fields
.field private A:Ljava/lang/String;

.field private B:Ljava/lang/String;

.field private C:Ljava/lang/String;

.field private D:Ljava/lang/String;

.field private E:Ljava/lang/String;

.field private F:Ljava/lang/String;

.field private G:Ljava/lang/String;

.field private H:Ljava/lang/String;

.field private I:Ljava/lang/String;

.field private J:Ljava/lang/String;

.field private K:Ljava/lang/String;

.field private L:[Ljava/lang/String;

.field private M:[Ljava/lang/String;

.field private N:[Ljava/lang/String;

.field private O:Ljava/lang/String;

.field private P:Ljava/lang/String;

.field private Q:Ljava/lang/String;

.field private R:Ljava/lang/String;

.field private S:Ljava/lang/String;

.field private T:Ljava/lang/String;

.field private U:Ljava/lang/String;

.field private V:Ljava/lang/String;

.field private W:Ljava/lang/String;

.field private X:Z

.field private Y:Z

.field private Z:Z

.field private final a:Ljava/lang/String;

.field private final b:Ljava/lang/String;

.field private final c:Ljava/lang/String;

.field private final d:Ljava/lang/String;

.field private final e:Ljava/lang/String;

.field private final f:Ljava/lang/String;

.field private final g:Ljava/lang/String;

.field private final h:Ljava/lang/String;

.field private final i:Ljava/lang/String;

.field private final j:Ljava/lang/String;

.field private final k:Ljava/lang/String;

.field private final l:Ljava/lang/String;

.field private final m:Ljava/lang/String;

.field private final n:Ljava/lang/String;

.field private final o:Ljava/lang/String;

.field private final p:Ljava/lang/String;

.field private final q:Ljava/lang/String;

.field private final r:Ljava/lang/String;

.field private final s:Ljava/lang/String;

.field private final t:Ljava/lang/String;

.field private u:[Ljava/lang/String;

.field private v:Ljava/lang/String;

.field private w:Ljava/lang/String;

.field private x:Ljava/lang/String;

.field private y:Ljava/lang/String;

.field private z:[Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x1

    .line 19
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 21
    const-string v0, "PMP"

    iput-object v0, p0, Lcom/osp/app/pushmarketing/e;->a:Ljava/lang/String;

    .line 24
    const-string v0, "request_package"

    iput-object v0, p0, Lcom/osp/app/pushmarketing/e;->b:Ljava/lang/String;

    .line 25
    const-string v0, "title"

    iput-object v0, p0, Lcom/osp/app/pushmarketing/e;->c:Ljava/lang/String;

    .line 26
    const-string v0, "content"

    iput-object v0, p0, Lcom/osp/app/pushmarketing/e;->d:Ljava/lang/String;

    .line 27
    const-string v0, "notiId"

    iput-object v0, p0, Lcom/osp/app/pushmarketing/e;->e:Ljava/lang/String;

    .line 28
    const-string v0, "cmpiId"

    iput-object v0, p0, Lcom/osp/app/pushmarketing/e;->f:Ljava/lang/String;

    .line 31
    const-string v0, "link_type"

    iput-object v0, p0, Lcom/osp/app/pushmarketing/e;->g:Ljava/lang/String;

    .line 32
    const-string v0, "target_link"

    iput-object v0, p0, Lcom/osp/app/pushmarketing/e;->h:Ljava/lang/String;

    .line 33
    const-string v0, "error_link"

    iput-object v0, p0, Lcom/osp/app/pushmarketing/e;->i:Ljava/lang/String;

    .line 34
    const-string v0, "duid"

    iput-object v0, p0, Lcom/osp/app/pushmarketing/e;->j:Ljava/lang/String;

    .line 35
    const-string v0, "style"

    iput-object v0, p0, Lcom/osp/app/pushmarketing/e;->k:Ljava/lang/String;

    .line 36
    const-string v0, "ticker"

    iput-object v0, p0, Lcom/osp/app/pushmarketing/e;->l:Ljava/lang/String;

    .line 37
    const-string v0, "large_icon"

    iput-object v0, p0, Lcom/osp/app/pushmarketing/e;->m:Ljava/lang/String;

    .line 38
    const-string v0, "bigpicture"

    iput-object v0, p0, Lcom/osp/app/pushmarketing/e;->n:Ljava/lang/String;

    .line 41
    const-string v0, "button1"

    iput-object v0, p0, Lcom/osp/app/pushmarketing/e;->o:Ljava/lang/String;

    .line 42
    const-string v0, "button2"

    iput-object v0, p0, Lcom/osp/app/pushmarketing/e;->p:Ljava/lang/String;

    .line 43
    const-string v0, "button3"

    iput-object v0, p0, Lcom/osp/app/pushmarketing/e;->q:Ljava/lang/String;

    .line 44
    const-string v0, "target_package"

    iput-object v0, p0, Lcom/osp/app/pushmarketing/e;->r:Ljava/lang/String;

    .line 45
    const-string v0, "text"

    iput-object v0, p0, Lcom/osp/app/pushmarketing/e;->s:Ljava/lang/String;

    .line 46
    const-string v0, "icon_visibility"

    iput-object v0, p0, Lcom/osp/app/pushmarketing/e;->t:Ljava/lang/String;

    .line 60
    const-string v0, "normal"

    iput-object v0, p0, Lcom/osp/app/pushmarketing/e;->E:Ljava/lang/String;

    .line 80
    iput-boolean v1, p0, Lcom/osp/app/pushmarketing/e;->X:Z

    .line 81
    iput-boolean v1, p0, Lcom/osp/app/pushmarketing/e;->Y:Z

    .line 82
    iput-boolean v1, p0, Lcom/osp/app/pushmarketing/e;->Z:Z

    return-void
.end method

.method private b(Lorg/json/JSONObject;)V
    .locals 7

    .prologue
    const/4 v0, 0x0

    .line 194
    const-string v1, "button1"

    invoke-virtual {p1, v1}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_5

    .line 196
    const-string v1, "button1"

    invoke-virtual {p1, v1}, Lorg/json/JSONObject;->getJSONObject(Ljava/lang/String;)Lorg/json/JSONObject;

    move-result-object v2

    .line 197
    const-string v1, "target_package"

    invoke-virtual {v2, v1}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 199
    const-string v1, "target_package"

    invoke-virtual {v2, v1}, Lorg/json/JSONObject;->getJSONArray(Ljava/lang/String;)Lorg/json/JSONArray;

    move-result-object v3

    .line 200
    invoke-virtual {v3}, Lorg/json/JSONArray;->length()I

    move-result v4

    .line 201
    new-array v1, v4, [Ljava/lang/String;

    iput-object v1, p0, Lcom/osp/app/pushmarketing/e;->L:[Ljava/lang/String;

    move v1, v0

    .line 202
    :goto_0
    if-ge v1, v4, :cond_0

    .line 204
    iget-object v5, p0, Lcom/osp/app/pushmarketing/e;->L:[Ljava/lang/String;

    invoke-virtual {v3, v1}, Lorg/json/JSONArray;->getString(I)Ljava/lang/String;

    move-result-object v6

    aput-object v6, v5, v1

    .line 202
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 208
    :cond_0
    const-string v1, "text"

    invoke-virtual {v2, v1}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 210
    const-string v1, "text"

    invoke-virtual {v2, v1}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/osp/app/pushmarketing/e;->I:Ljava/lang/String;

    .line 212
    :cond_1
    const-string v1, "link_type"

    invoke-virtual {v2, v1}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 214
    const-string v1, "link_type"

    invoke-virtual {v2, v1}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/osp/app/pushmarketing/e;->O:Ljava/lang/String;

    .line 216
    :cond_2
    const-string v1, "target_link"

    invoke-virtual {v2, v1}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_3

    .line 218
    const-string v1, "target_link"

    invoke-virtual {v2, v1}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/osp/app/pushmarketing/e;->R:Ljava/lang/String;

    .line 220
    :cond_3
    const-string v1, "error_link"

    invoke-virtual {v2, v1}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_4

    .line 222
    const-string v1, "error_link"

    invoke-virtual {v2, v1}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/osp/app/pushmarketing/e;->U:Ljava/lang/String;

    .line 224
    :cond_4
    const-string v1, "icon_visibility"

    invoke-virtual {v2, v1}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_5

    .line 226
    const-string v1, "icon_visibility"

    invoke-virtual {v2, v1}, Lorg/json/JSONObject;->getBoolean(Ljava/lang/String;)Z

    move-result v1

    iput-boolean v1, p0, Lcom/osp/app/pushmarketing/e;->X:Z

    .line 230
    :cond_5
    const-string v1, "button2"

    invoke-virtual {p1, v1}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_b

    .line 232
    const-string v1, "button2"

    invoke-virtual {p1, v1}, Lorg/json/JSONObject;->getJSONObject(Ljava/lang/String;)Lorg/json/JSONObject;

    move-result-object v2

    .line 233
    const-string v1, "target_package"

    invoke-virtual {v2, v1}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_6

    .line 235
    const-string v1, "target_package"

    invoke-virtual {v2, v1}, Lorg/json/JSONObject;->getJSONArray(Ljava/lang/String;)Lorg/json/JSONArray;

    move-result-object v3

    .line 236
    invoke-virtual {v3}, Lorg/json/JSONArray;->length()I

    move-result v4

    .line 237
    new-array v1, v4, [Ljava/lang/String;

    iput-object v1, p0, Lcom/osp/app/pushmarketing/e;->M:[Ljava/lang/String;

    move v1, v0

    .line 238
    :goto_1
    if-ge v1, v4, :cond_6

    .line 240
    iget-object v5, p0, Lcom/osp/app/pushmarketing/e;->M:[Ljava/lang/String;

    invoke-virtual {v3, v1}, Lorg/json/JSONArray;->getString(I)Ljava/lang/String;

    move-result-object v6

    aput-object v6, v5, v1

    .line 238
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 244
    :cond_6
    const-string v1, "text"

    invoke-virtual {v2, v1}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_7

    .line 246
    const-string v1, "text"

    invoke-virtual {v2, v1}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/osp/app/pushmarketing/e;->J:Ljava/lang/String;

    .line 248
    :cond_7
    const-string v1, "link_type"

    invoke-virtual {v2, v1}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_8

    .line 250
    const-string v1, "link_type"

    invoke-virtual {v2, v1}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/osp/app/pushmarketing/e;->P:Ljava/lang/String;

    .line 252
    :cond_8
    const-string v1, "target_link"

    invoke-virtual {v2, v1}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_9

    .line 254
    const-string v1, "target_link"

    invoke-virtual {v2, v1}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/osp/app/pushmarketing/e;->S:Ljava/lang/String;

    .line 256
    :cond_9
    const-string v1, "error_link"

    invoke-virtual {v2, v1}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_a

    .line 258
    const-string v1, "error_link"

    invoke-virtual {v2, v1}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/osp/app/pushmarketing/e;->V:Ljava/lang/String;

    .line 260
    :cond_a
    const-string v1, "icon_visibility"

    invoke-virtual {v2, v1}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_b

    .line 262
    const-string v1, "icon_visibility"

    invoke-virtual {v2, v1}, Lorg/json/JSONObject;->getBoolean(Ljava/lang/String;)Z

    move-result v1

    iput-boolean v1, p0, Lcom/osp/app/pushmarketing/e;->Y:Z

    .line 266
    :cond_b
    const-string v1, "button3"

    invoke-virtual {p1, v1}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_11

    .line 268
    const-string v1, "button3"

    invoke-virtual {p1, v1}, Lorg/json/JSONObject;->getJSONObject(Ljava/lang/String;)Lorg/json/JSONObject;

    move-result-object v1

    .line 269
    const-string v2, "target_package"

    invoke-virtual {v1, v2}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_c

    .line 271
    const-string v2, "target_package"

    invoke-virtual {v1, v2}, Lorg/json/JSONObject;->getJSONArray(Ljava/lang/String;)Lorg/json/JSONArray;

    move-result-object v2

    .line 272
    invoke-virtual {v2}, Lorg/json/JSONArray;->length()I

    move-result v3

    .line 273
    new-array v4, v3, [Ljava/lang/String;

    iput-object v4, p0, Lcom/osp/app/pushmarketing/e;->N:[Ljava/lang/String;

    .line 274
    :goto_2
    if-ge v0, v3, :cond_c

    .line 276
    iget-object v4, p0, Lcom/osp/app/pushmarketing/e;->N:[Ljava/lang/String;

    invoke-virtual {v2, v0}, Lorg/json/JSONArray;->getString(I)Ljava/lang/String;

    move-result-object v5

    aput-object v5, v4, v0

    .line 274
    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    .line 280
    :cond_c
    const-string v0, "text"

    invoke-virtual {v1, v0}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_d

    .line 282
    const-string v0, "text"

    invoke-virtual {v1, v0}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/osp/app/pushmarketing/e;->K:Ljava/lang/String;

    .line 284
    :cond_d
    const-string v0, "link_type"

    invoke-virtual {v1, v0}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_e

    .line 286
    const-string v0, "link_type"

    invoke-virtual {v1, v0}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/osp/app/pushmarketing/e;->Q:Ljava/lang/String;

    .line 288
    :cond_e
    const-string v0, "target_link"

    invoke-virtual {v1, v0}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_f

    .line 290
    const-string v0, "target_link"

    invoke-virtual {v1, v0}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/osp/app/pushmarketing/e;->T:Ljava/lang/String;

    .line 292
    :cond_f
    const-string v0, "error_link"

    invoke-virtual {v1, v0}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_10

    .line 294
    const-string v0, "error_link"

    invoke-virtual {v1, v0}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/osp/app/pushmarketing/e;->W:Ljava/lang/String;

    .line 296
    :cond_10
    const-string v0, "icon_visibility"

    invoke-virtual {v1, v0}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_11

    .line 298
    const-string v0, "icon_visibility"

    invoke-virtual {v1, v0}, Lorg/json/JSONObject;->getBoolean(Ljava/lang/String;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/osp/app/pushmarketing/e;->Z:Z

    .line 302
    :cond_11
    return-void
.end method


# virtual methods
.method public final A()Ljava/lang/String;
    .locals 1

    .prologue
    .line 584
    sget-object v0, Lcom/osp/app/pushmarketing/l;->a:Lcom/osp/app/pushmarketing/k;

    iget-object v0, p0, Lcom/osp/app/pushmarketing/e;->V:Ljava/lang/String;

    invoke-static {v0}, Lcom/osp/app/pushmarketing/k;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final B()Z
    .locals 1

    .prologue
    .line 603
    iget-boolean v0, p0, Lcom/osp/app/pushmarketing/e;->X:Z

    return v0
.end method

.method public final C()Z
    .locals 1

    .prologue
    .line 612
    iget-boolean v0, p0, Lcom/osp/app/pushmarketing/e;->Y:Z

    return v0
.end method

.method public final a(Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 90
    iput-object p1, p0, Lcom/osp/app/pushmarketing/e;->E:Ljava/lang/String;

    .line 91
    return-void
.end method

.method public final a(Lorg/json/JSONObject;)V
    .locals 9

    .prologue
    const/4 v1, 0x0

    const/4 v6, 0x1

    .line 101
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v2, "PMP"

    invoke-direct {v0, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p1}, Lorg/json/JSONObject;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;)V

    .line 103
    const-string v0, "request_package"

    invoke-virtual {p1, v0}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 105
    const-string v0, "request_package"

    invoke-virtual {p1, v0}, Lorg/json/JSONObject;->getJSONArray(Ljava/lang/String;)Lorg/json/JSONArray;

    move-result-object v2

    .line 106
    invoke-virtual {v2}, Lorg/json/JSONArray;->length()I

    move-result v3

    .line 107
    new-array v0, v3, [Ljava/lang/String;

    iput-object v0, p0, Lcom/osp/app/pushmarketing/e;->u:[Ljava/lang/String;

    move v0, v1

    .line 108
    :goto_0
    if-ge v0, v3, :cond_0

    .line 110
    iget-object v4, p0, Lcom/osp/app/pushmarketing/e;->u:[Ljava/lang/String;

    invoke-virtual {v2, v0}, Lorg/json/JSONArray;->getString(I)Ljava/lang/String;

    move-result-object v5

    aput-object v5, v4, v0

    .line 108
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 113
    :cond_0
    const-string v0, "title"

    invoke-virtual {p1, v0}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 115
    const-string v0, "title"

    invoke-virtual {p1, v0}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/osp/app/pushmarketing/e;->v:Ljava/lang/String;

    .line 117
    :cond_1
    const-string v0, "content"

    invoke-virtual {p1, v0}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 119
    const-string v0, "content"

    invoke-virtual {p1, v0}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/osp/app/pushmarketing/e;->w:Ljava/lang/String;

    .line 120
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v2, "PMP Contents convert to BYTES : "

    invoke-direct {v0, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v2, "%x"

    new-array v3, v6, [Ljava/lang/Object;

    new-instance v4, Ljava/math/BigInteger;

    iget-object v5, p0, Lcom/osp/app/pushmarketing/e;->w:Ljava/lang/String;

    invoke-virtual {v5}, Ljava/lang/String;->getBytes()[B

    move-result-object v5

    invoke-direct {v4, v6, v5}, Ljava/math/BigInteger;-><init>(I[B)V

    aput-object v4, v3, v1

    invoke-static {v2, v3}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;)V

    .line 123
    :try_start_0
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v2, "PMP Contents convert to BYTES(UTF-8) : "

    invoke-direct {v0, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v2, "%x"

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    new-instance v5, Ljava/math/BigInteger;

    const/4 v6, 0x1

    iget-object v7, p0, Lcom/osp/app/pushmarketing/e;->w:Ljava/lang/String;

    const-string v8, "UTF-8"

    invoke-virtual {v7, v8}, Ljava/lang/String;->getBytes(Ljava/lang/String;)[B

    move-result-object v7

    invoke-direct {v5, v6, v7}, Ljava/math/BigInteger;-><init>(I[B)V

    aput-object v5, v3, v4

    invoke-static {v2, v3}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;)V

    .line 124
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v2, "PMP Contents convert to BYTES(MS949) : "

    invoke-direct {v0, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v2, "%x"

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    new-instance v5, Ljava/math/BigInteger;

    const/4 v6, 0x1

    iget-object v7, p0, Lcom/osp/app/pushmarketing/e;->w:Ljava/lang/String;

    const-string v8, "MS949"

    invoke-virtual {v7, v8}, Ljava/lang/String;->getBytes(Ljava/lang/String;)[B

    move-result-object v7

    invoke-direct {v5, v6, v7}, Ljava/math/BigInteger;-><init>(I[B)V

    aput-object v5, v3, v4

    invoke-static {v2, v3}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;)V

    .line 125
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v2, "PMP Contents convert to BYTES(ISO-8859-1) : "

    invoke-direct {v0, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v2, "%x"

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    new-instance v5, Ljava/math/BigInteger;

    const/4 v6, 0x1

    iget-object v7, p0, Lcom/osp/app/pushmarketing/e;->w:Ljava/lang/String;

    const-string v8, "ISO-8859-1"

    invoke-virtual {v7, v8}, Ljava/lang/String;->getBytes(Ljava/lang/String;)[B

    move-result-object v7

    invoke-direct {v5, v6, v7}, Ljava/math/BigInteger;-><init>(I[B)V

    aput-object v5, v3, v4

    invoke-static {v2, v3}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/io/UnsupportedEncodingException; {:try_start_0 .. :try_end_0} :catch_0

    .line 132
    :cond_2
    :goto_1
    const-string v0, "notiId"

    invoke-virtual {p1, v0}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 134
    const-string v0, "notiId"

    invoke-virtual {p1, v0}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/osp/app/pushmarketing/e;->x:Ljava/lang/String;

    .line 136
    :cond_3
    const-string v0, "cmpiId"

    invoke-virtual {p1, v0}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 138
    const-string v0, "cmpiId"

    invoke-virtual {p1, v0}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/osp/app/pushmarketing/e;->y:Ljava/lang/String;

    .line 141
    :cond_4
    const-string v0, "link_type"

    invoke-virtual {p1, v0}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_5

    .line 143
    const-string v0, "link_type"

    invoke-virtual {p1, v0}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/osp/app/pushmarketing/e;->C:Ljava/lang/String;

    .line 145
    :cond_5
    const-string v0, "target_package"

    invoke-virtual {p1, v0}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_6

    .line 147
    const-string v0, "target_package"

    invoke-virtual {p1, v0}, Lorg/json/JSONObject;->getJSONArray(Ljava/lang/String;)Lorg/json/JSONArray;

    move-result-object v2

    .line 148
    invoke-virtual {v2}, Lorg/json/JSONArray;->length()I

    move-result v3

    .line 149
    new-array v0, v3, [Ljava/lang/String;

    iput-object v0, p0, Lcom/osp/app/pushmarketing/e;->z:[Ljava/lang/String;

    move v0, v1

    .line 150
    :goto_2
    if-ge v0, v3, :cond_6

    .line 152
    iget-object v1, p0, Lcom/osp/app/pushmarketing/e;->z:[Ljava/lang/String;

    invoke-virtual {v2, v0}, Lorg/json/JSONArray;->getString(I)Ljava/lang/String;

    move-result-object v4

    aput-object v4, v1, v0

    .line 150
    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    .line 127
    :catch_0
    move-exception v0

    invoke-virtual {v0}, Ljava/io/UnsupportedEncodingException;->printStackTrace()V

    goto :goto_1

    .line 155
    :cond_6
    const-string v0, "target_link"

    invoke-virtual {p1, v0}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_7

    .line 157
    const-string v0, "target_link"

    invoke-virtual {p1, v0}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/osp/app/pushmarketing/e;->A:Ljava/lang/String;

    .line 159
    :cond_7
    const-string v0, "duid"

    invoke-virtual {p1, v0}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_8

    .line 161
    const-string v0, "duid"

    invoke-virtual {p1, v0}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/osp/app/pushmarketing/e;->B:Ljava/lang/String;

    .line 163
    :cond_8
    const-string v0, "style"

    invoke-virtual {p1, v0}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_9

    .line 165
    const-string v0, "style"

    invoke-virtual {p1, v0}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/osp/app/pushmarketing/e;->E:Ljava/lang/String;

    .line 167
    :cond_9
    const-string v0, "ticker"

    invoke-virtual {p1, v0}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_a

    .line 169
    const-string v0, "ticker"

    invoke-virtual {p1, v0}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/osp/app/pushmarketing/e;->F:Ljava/lang/String;

    .line 171
    :cond_a
    const-string v0, "large_icon"

    invoke-virtual {p1, v0}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_b

    .line 173
    const-string v0, "large_icon"

    invoke-virtual {p1, v0}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/osp/app/pushmarketing/e;->G:Ljava/lang/String;

    .line 175
    :cond_b
    const-string v0, "bigpicture"

    invoke-virtual {p1, v0}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_c

    .line 177
    const-string v0, "bigpicture"

    invoke-virtual {p1, v0}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/osp/app/pushmarketing/e;->H:Ljava/lang/String;

    .line 179
    :cond_c
    const-string v0, "error_link"

    invoke-virtual {p1, v0}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_d

    .line 181
    const-string v0, "error_link"

    invoke-virtual {p1, v0}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/osp/app/pushmarketing/e;->D:Ljava/lang/String;

    .line 184
    :cond_d
    invoke-direct {p0, p1}, Lcom/osp/app/pushmarketing/e;->b(Lorg/json/JSONObject;)V

    .line 185
    return-void
.end method

.method public final a()[Ljava/lang/String;
    .locals 1

    .prologue
    .line 310
    iget-object v0, p0, Lcom/osp/app/pushmarketing/e;->u:[Ljava/lang/String;

    return-object v0
.end method

.method public final b()Ljava/lang/String;
    .locals 1

    .prologue
    .line 319
    iget-object v0, p0, Lcom/osp/app/pushmarketing/e;->v:Ljava/lang/String;

    return-object v0
.end method

.method public final c()Ljava/lang/String;
    .locals 1

    .prologue
    .line 328
    iget-object v0, p0, Lcom/osp/app/pushmarketing/e;->w:Ljava/lang/String;

    return-object v0
.end method

.method public final d()Ljava/lang/String;
    .locals 1

    .prologue
    .line 337
    iget-object v0, p0, Lcom/osp/app/pushmarketing/e;->x:Ljava/lang/String;

    return-object v0
.end method

.method public final e()Ljava/lang/String;
    .locals 1

    .prologue
    .line 346
    iget-object v0, p0, Lcom/osp/app/pushmarketing/e;->y:Ljava/lang/String;

    return-object v0
.end method

.method public final f()[Ljava/lang/String;
    .locals 1

    .prologue
    .line 355
    iget-object v0, p0, Lcom/osp/app/pushmarketing/e;->z:[Ljava/lang/String;

    return-object v0
.end method

.method public final g()Ljava/lang/String;
    .locals 2

    .prologue
    .line 365
    const-string v0, "weblink"

    iget-object v1, p0, Lcom/osp/app/pushmarketing/e;->C:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 367
    sget-object v0, Lcom/osp/app/pushmarketing/l;->a:Lcom/osp/app/pushmarketing/k;

    iget-object v0, p0, Lcom/osp/app/pushmarketing/e;->A:Ljava/lang/String;

    invoke-static {v0}, Lcom/osp/app/pushmarketing/k;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 370
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/osp/app/pushmarketing/e;->A:Ljava/lang/String;

    goto :goto_0
.end method

.method public final h()Ljava/lang/String;
    .locals 1

    .prologue
    .line 380
    iget-object v0, p0, Lcom/osp/app/pushmarketing/e;->B:Ljava/lang/String;

    return-object v0
.end method

.method public final i()Ljava/lang/String;
    .locals 1

    .prologue
    .line 389
    iget-object v0, p0, Lcom/osp/app/pushmarketing/e;->C:Ljava/lang/String;

    return-object v0
.end method

.method public final j()Ljava/lang/String;
    .locals 1

    .prologue
    .line 398
    sget-object v0, Lcom/osp/app/pushmarketing/l;->a:Lcom/osp/app/pushmarketing/k;

    iget-object v0, p0, Lcom/osp/app/pushmarketing/e;->D:Ljava/lang/String;

    invoke-static {v0}, Lcom/osp/app/pushmarketing/k;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final k()Ljava/lang/String;
    .locals 1

    .prologue
    .line 407
    iget-object v0, p0, Lcom/osp/app/pushmarketing/e;->E:Ljava/lang/String;

    return-object v0
.end method

.method public final l()Ljava/lang/String;
    .locals 1

    .prologue
    .line 416
    iget-object v0, p0, Lcom/osp/app/pushmarketing/e;->F:Ljava/lang/String;

    return-object v0
.end method

.method public final m()Ljava/lang/String;
    .locals 1

    .prologue
    .line 425
    iget-object v0, p0, Lcom/osp/app/pushmarketing/e;->G:Ljava/lang/String;

    return-object v0
.end method

.method public final n()Ljava/lang/String;
    .locals 1

    .prologue
    .line 434
    iget-object v0, p0, Lcom/osp/app/pushmarketing/e;->H:Ljava/lang/String;

    return-object v0
.end method

.method public final o()Ljava/lang/String;
    .locals 1

    .prologue
    .line 443
    iget-object v0, p0, Lcom/osp/app/pushmarketing/e;->I:Ljava/lang/String;

    return-object v0
.end method

.method public final p()Ljava/lang/String;
    .locals 1

    .prologue
    .line 452
    iget-object v0, p0, Lcom/osp/app/pushmarketing/e;->J:Ljava/lang/String;

    return-object v0
.end method

.method public final q()Ljava/lang/String;
    .locals 1
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 462
    iget-object v0, p0, Lcom/osp/app/pushmarketing/e;->K:Ljava/lang/String;

    return-object v0
.end method

.method public final r()[Ljava/lang/String;
    .locals 1

    .prologue
    .line 471
    iget-object v0, p0, Lcom/osp/app/pushmarketing/e;->L:[Ljava/lang/String;

    return-object v0
.end method

.method public final s()[Ljava/lang/String;
    .locals 1

    .prologue
    .line 480
    iget-object v0, p0, Lcom/osp/app/pushmarketing/e;->M:[Ljava/lang/String;

    return-object v0
.end method

.method public final t()Ljava/lang/String;
    .locals 1

    .prologue
    .line 499
    iget-object v0, p0, Lcom/osp/app/pushmarketing/e;->O:Ljava/lang/String;

    return-object v0
.end method

.method public final u()Ljava/lang/String;
    .locals 1

    .prologue
    .line 508
    iget-object v0, p0, Lcom/osp/app/pushmarketing/e;->P:Ljava/lang/String;

    return-object v0
.end method

.method public final v()Ljava/lang/String;
    .locals 1
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 518
    iget-object v0, p0, Lcom/osp/app/pushmarketing/e;->Q:Ljava/lang/String;

    return-object v0
.end method

.method public final w()Ljava/lang/String;
    .locals 2

    .prologue
    .line 528
    const-string v0, "weblink"

    iget-object v1, p0, Lcom/osp/app/pushmarketing/e;->O:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 530
    sget-object v0, Lcom/osp/app/pushmarketing/l;->a:Lcom/osp/app/pushmarketing/k;

    iget-object v0, p0, Lcom/osp/app/pushmarketing/e;->R:Ljava/lang/String;

    invoke-static {v0}, Lcom/osp/app/pushmarketing/k;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 533
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/osp/app/pushmarketing/e;->R:Ljava/lang/String;

    goto :goto_0
.end method

.method public final x()Ljava/lang/String;
    .locals 2

    .prologue
    .line 544
    const-string v0, "weblink"

    iget-object v1, p0, Lcom/osp/app/pushmarketing/e;->P:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 546
    sget-object v0, Lcom/osp/app/pushmarketing/l;->a:Lcom/osp/app/pushmarketing/k;

    iget-object v0, p0, Lcom/osp/app/pushmarketing/e;->S:Ljava/lang/String;

    invoke-static {v0}, Lcom/osp/app/pushmarketing/k;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 549
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/osp/app/pushmarketing/e;->S:Ljava/lang/String;

    goto :goto_0
.end method

.method public final y()Ljava/lang/String;
    .locals 2
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 560
    const-string v0, "weblink"

    iget-object v1, p0, Lcom/osp/app/pushmarketing/e;->Q:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 562
    sget-object v0, Lcom/osp/app/pushmarketing/l;->a:Lcom/osp/app/pushmarketing/k;

    iget-object v0, p0, Lcom/osp/app/pushmarketing/e;->T:Ljava/lang/String;

    invoke-static {v0}, Lcom/osp/app/pushmarketing/k;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 565
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/osp/app/pushmarketing/e;->T:Ljava/lang/String;

    goto :goto_0
.end method

.method public final z()Ljava/lang/String;
    .locals 1

    .prologue
    .line 575
    sget-object v0, Lcom/osp/app/pushmarketing/l;->a:Lcom/osp/app/pushmarketing/k;

    iget-object v0, p0, Lcom/osp/app/pushmarketing/e;->U:Ljava/lang/String;

    invoke-static {v0}, Lcom/osp/app/pushmarketing/k;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
