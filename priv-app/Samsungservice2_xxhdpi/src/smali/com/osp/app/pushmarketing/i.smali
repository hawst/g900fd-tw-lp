.class final Lcom/osp/app/pushmarketing/i;
.super Lcom/msc/c/b;
.source "PushMarketingService.java"


# instance fields
.field final synthetic c:Lcom/osp/app/pushmarketing/PushMarketingService;

.field private final d:Landroid/content/Intent;

.field private e:J

.field private f:J

.field private g:Z

.field private h:Ljava/lang/String;


# direct methods
.method public constructor <init>(Lcom/osp/app/pushmarketing/PushMarketingService;Landroid/content/Intent;)V
    .locals 1

    .prologue
    .line 885
    iput-object p1, p0, Lcom/osp/app/pushmarketing/i;->c:Lcom/osp/app/pushmarketing/PushMarketingService;

    .line 886
    invoke-direct {p0, p1}, Lcom/msc/c/b;-><init>(Landroid/content/Context;)V

    .line 882
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/osp/app/pushmarketing/i;->g:Z

    .line 883
    const-string v0, "Fail"

    iput-object v0, p0, Lcom/osp/app/pushmarketing/i;->h:Ljava/lang/String;

    .line 887
    invoke-virtual {p0}, Lcom/osp/app/pushmarketing/i;->c()V

    .line 888
    iput-object p2, p0, Lcom/osp/app/pushmarketing/i;->d:Landroid/content/Intent;

    .line 889
    return-void
.end method

.method private b(Ljava/lang/String;)V
    .locals 10

    .prologue
    .line 1050
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v0, "PMS requestClickFeedBack called"

    invoke-static {v0}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;)V

    .line 1052
    iget-object v0, p0, Lcom/osp/app/pushmarketing/i;->d:Landroid/content/Intent;

    const-string v1, "notification_id"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    .line 1053
    iget-object v0, p0, Lcom/osp/app/pushmarketing/i;->d:Landroid/content/Intent;

    const-string v1, "campaign_id"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    .line 1055
    new-instance v0, Lcom/osp/app/pushmarketing/g;

    invoke-direct {v0}, Lcom/osp/app/pushmarketing/g;-><init>()V

    .line 1056
    iget-object v0, p0, Lcom/osp/app/pushmarketing/i;->c:Lcom/osp/app/pushmarketing/PushMarketingService;

    invoke-static {v0}, Lcom/msc/c/e;->h(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v8

    iget-object v0, p0, Lcom/osp/app/pushmarketing/i;->d:Landroid/content/Intent;

    const-string v1, "reg_id"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v9

    invoke-static {}, Lcom/osp/app/util/ah;->a()Lcom/osp/app/util/ah;

    move-result-object v0

    invoke-virtual {v0}, Lcom/osp/app/util/ah;->g()Z

    move-result v4

    invoke-static {}, Lcom/msc/b/i;->a()Lcom/msc/b/i;

    move-result-object v0

    const-string v1, "https://eu-crm.samsungosp.com/v2/pms/application/clickFeedback"

    invoke-static {}, Lcom/osp/app/signin/SamsungService;->j()Z

    move-result v2

    const/4 v3, 0x0

    move-object v5, p0

    invoke-virtual/range {v0 .. v5}, Lcom/msc/b/i;->a(Ljava/lang/String;ZZZLcom/msc/b/h;)Lcom/msc/b/d;

    move-result-object v0

    const-string v1, "content-type"

    const-string v2, "application/x-www-form-urlencoded;charset=UTF-8"

    invoke-virtual {v0, v1, v2}, Lcom/msc/b/d;->a(Ljava/lang/String;Ljava/lang/String;)V

    const-string v1, "accept"

    const-string v2, "application/json"

    invoke-virtual {v0, v1, v2}, Lcom/msc/b/d;->a(Ljava/lang/String;Ljava/lang/String;)V

    const-string v1, "x-osp-authToken"

    invoke-virtual {v0, v1, p1}, Lcom/msc/b/d;->a(Ljava/lang/String;Ljava/lang/String;)V

    const-string v1, "x-osp-appId"

    const-string v2, "j5p7ll8g33"

    invoke-virtual {v0, v1, v2}, Lcom/msc/b/d;->a(Ljava/lang/String;Ljava/lang/String;)V

    const-string v1, "x-osp-userId"

    invoke-virtual {v0, v1, v8}, Lcom/msc/b/d;->a(Ljava/lang/String;Ljava/lang/String;)V

    const-string v1, "regId"

    invoke-virtual {v0, v1, v9}, Lcom/msc/b/d;->b(Ljava/lang/String;Ljava/lang/String;)V

    const-string v1, "notiId"

    invoke-virtual {v0, v1, v6}, Lcom/msc/b/d;->b(Ljava/lang/String;Ljava/lang/String;)V

    const-string v1, "cmpiId"

    invoke-virtual {v0, v1, v7}, Lcom/msc/b/d;->b(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v0}, Lcom/msc/b/d;->a()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/osp/app/pushmarketing/i;->e:J

    .line 1059
    iget-wide v0, p0, Lcom/osp/app/pushmarketing/i;->e:J

    invoke-virtual {p0, v0, v1}, Lcom/osp/app/pushmarketing/i;->a(J)V

    .line 1060
    iget-wide v0, p0, Lcom/osp/app/pushmarketing/i;->e:J

    const-string v2, "from_json"

    invoke-virtual {p0, v0, v1, v2}, Lcom/osp/app/pushmarketing/i;->a(JLjava/lang/String;)V

    .line 1061
    iget-wide v0, p0, Lcom/osp/app/pushmarketing/i;->e:J

    sget-object v2, Lcom/msc/b/g;->c:Lcom/msc/b/g;

    invoke-static {v0, v1, v2}, Lcom/osp/app/pushmarketing/g;->a(JLcom/msc/b/g;)V

    .line 1062
    return-void
.end method

.method private h()V
    .locals 8

    .prologue
    const/4 v3, 0x0

    .line 1031
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v0, "PMS requestAuthenticationV02 called"

    invoke-static {v0}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;)V

    .line 1033
    invoke-static {}, Lcom/msc/c/g;->a()Lcom/msc/c/g;

    .line 1034
    iget-object v0, p0, Lcom/osp/app/pushmarketing/i;->c:Lcom/osp/app/pushmarketing/PushMarketingService;

    const-string v1, "j5p7ll8g33"

    const-string v2, "5763D0052DC1462E13751F753384E9A9"

    iget-object v4, p0, Lcom/osp/app/pushmarketing/i;->c:Lcom/osp/app/pushmarketing/PushMarketingService;

    invoke-static {v4}, Lcom/msc/c/e;->g(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v5

    move-object v4, v3

    move-object v6, v3

    move-object v7, p0

    invoke-static/range {v0 .. v7}, Lcom/msc/c/g;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/osp/app/signin/cn;Lcom/msc/b/h;)J

    move-result-wide v0

    iput-wide v0, p0, Lcom/osp/app/pushmarketing/i;->f:J

    .line 1037
    iget-wide v0, p0, Lcom/osp/app/pushmarketing/i;->f:J

    invoke-virtual {p0, v0, v1}, Lcom/osp/app/pushmarketing/i;->b(J)V

    .line 1038
    iget-wide v0, p0, Lcom/osp/app/pushmarketing/i;->f:J

    const-string v2, "from_json"

    invoke-virtual {p0, v0, v1, v2}, Lcom/osp/app/pushmarketing/i;->a(JLjava/lang/String;)V

    .line 1040
    iget-wide v0, p0, Lcom/osp/app/pushmarketing/i;->f:J

    sget-object v2, Lcom/msc/b/g;->c:Lcom/msc/b/g;

    invoke-static {v0, v1, v2}, Lcom/msc/c/g;->a(JLcom/msc/b/g;)V

    .line 1041
    return-void
.end method


# virtual methods
.method protected final varargs a()Ljava/lang/Boolean;
    .locals 2

    .prologue
    .line 895
    iget-object v0, p0, Lcom/osp/app/pushmarketing/i;->c:Lcom/osp/app/pushmarketing/PushMarketingService;

    invoke-static {v0}, Lcom/msc/c/e;->b(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v0

    .line 896
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v1

    if-lez v1, :cond_0

    .line 898
    invoke-direct {p0, v0}, Lcom/osp/app/pushmarketing/i;->b(Ljava/lang/String;)V

    .line 904
    :goto_0
    const/4 v0, 0x0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    return-object v0

    .line 901
    :cond_0
    invoke-direct {p0}, Lcom/osp/app/pushmarketing/i;->h()V

    goto :goto_0
.end method

.method public final declared-synchronized a(Lcom/msc/b/c;)V
    .locals 6

    .prologue
    .line 930
    monitor-enter p0

    :try_start_0
    invoke-super {p0, p1}, Lcom/msc/c/b;->a(Lcom/msc/b/c;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 932
    if-nez p1, :cond_1

    .line 970
    :cond_0
    :goto_0
    monitor-exit p0

    return-void

    .line 937
    :cond_1
    :try_start_1
    invoke-virtual {p1}, Lcom/msc/b/c;->a()J

    move-result-wide v0

    .line 938
    invoke-virtual {p1}, Lcom/msc/b/c;->e()Ljava/lang/String;

    move-result-object v2

    .line 940
    iget-wide v4, p0, Lcom/osp/app/pushmarketing/i;->e:J

    cmp-long v3, v0, v4

    if-nez v3, :cond_2

    .line 942
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v0, "PMS"

    const-string v1, "PMS feedback success"

    invoke-static {v0, v1}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 943
    const-string v0, "Success"

    iput-object v0, p0, Lcom/osp/app/pushmarketing/i;->h:Ljava/lang/String;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 930
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 945
    :cond_2
    :try_start_2
    iget-wide v4, p0, Lcom/osp/app/pushmarketing/i;->f:J
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    cmp-long v0, v0, v4

    if-nez v0, :cond_0

    .line 949
    :try_start_3
    new-instance v0, Lcom/msc/a/d;

    invoke-direct {v0}, Lcom/msc/a/d;-><init>()V

    .line 950
    invoke-static {}, Lcom/msc/c/h;->a()Lcom/msc/c/h;

    invoke-static {v2, v0}, Lcom/msc/c/h;->a(Ljava/lang/String;Lcom/msc/a/d;)V

    .line 951
    invoke-virtual {v0}, Lcom/msc/a/d;->g()Ljava/lang/String;

    move-result-object v0

    .line 952
    if-eqz v0, :cond_3

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v1

    if-lez v1, :cond_3

    .line 954
    iget-object v1, p0, Lcom/osp/app/pushmarketing/i;->c:Lcom/osp/app/pushmarketing/PushMarketingService;

    invoke-static {v1, v0}, Lcom/msc/c/e;->b(Landroid/content/Context;Ljava/lang/String;)V

    .line 955
    invoke-direct {p0, v0}, Lcom/osp/app/pushmarketing/i;->b(Ljava/lang/String;)V
    :try_end_3
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_0
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    goto :goto_0

    .line 962
    :catch_0
    move-exception v0

    .line 964
    :try_start_4
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    .line 965
    new-instance v1, Lcom/msc/c/f;

    invoke-direct {v1, v0}, Lcom/msc/c/f;-><init>(Ljava/lang/Exception;)V

    iput-object v1, p0, Lcom/osp/app/pushmarketing/i;->b:Lcom/msc/c/f;

    .line 966
    const-string v0, "Fail"

    iput-object v0, p0, Lcom/osp/app/pushmarketing/i;->h:Ljava/lang/String;
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    goto :goto_0

    .line 958
    :cond_3
    :try_start_5
    const-string v0, "Fail"

    iput-object v0, p0, Lcom/osp/app/pushmarketing/i;->h:Ljava/lang/String;

    .line 959
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v0, "PMS"

    const-string v1, "Fail to get accessToken"

    invoke-static {v0, v1}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 960
    new-instance v0, Lcom/msc/c/f;

    invoke-direct {v0}, Lcom/msc/c/f;-><init>()V

    iput-object v0, p0, Lcom/osp/app/pushmarketing/i;->b:Lcom/msc/c/f;
    :try_end_5
    .catch Ljava/lang/Exception; {:try_start_5 .. :try_end_5} :catch_0
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    goto :goto_0
.end method

.method protected final a(Ljava/lang/Boolean;)V
    .locals 2

    .prologue
    .line 910
    invoke-super {p0, p1}, Lcom/msc/c/b;->a(Ljava/lang/Boolean;)V

    .line 912
    iget-object v0, p0, Lcom/osp/app/pushmarketing/i;->h:Ljava/lang/String;

    const-string v1, "Success"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 914
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v0, "PMS"

    const-string v1, "PMS click feedback is complete"

    invoke-static {v0, v1}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 926
    :goto_0
    return-void

    .line 915
    :cond_0
    iget-object v0, p0, Lcom/osp/app/pushmarketing/i;->h:Ljava/lang/String;

    const-string v1, "require re-SignIn"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 917
    iget-object v0, p0, Lcom/osp/app/pushmarketing/i;->c:Lcom/osp/app/pushmarketing/PushMarketingService;

    invoke-static {v0}, Lcom/osp/app/pushmarketing/PushMarketingService;->h(Lcom/osp/app/pushmarketing/PushMarketingService;)V

    goto :goto_0

    .line 918
    :cond_1
    iget-object v0, p0, Lcom/osp/app/pushmarketing/i;->h:Ljava/lang/String;

    const-string v1, "require re-SignIn with signout"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 920
    iget-object v0, p0, Lcom/osp/app/pushmarketing/i;->c:Lcom/osp/app/pushmarketing/PushMarketingService;

    invoke-static {v0}, Lcom/osp/app/pushmarketing/PushMarketingService;->i(Lcom/osp/app/pushmarketing/PushMarketingService;)V

    goto :goto_0

    .line 923
    :cond_2
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v0, "PMS"

    const-string v1, "PMS click feedback error occured"

    invoke-static {v0, v1}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public final b(Lcom/msc/b/c;)V
    .locals 6

    .prologue
    .line 974
    invoke-super {p0, p1}, Lcom/msc/c/b;->b(Lcom/msc/b/c;)V

    .line 976
    if-nez p1, :cond_1

    .line 1024
    :cond_0
    :goto_0
    return-void

    .line 981
    :cond_1
    invoke-virtual {p1}, Lcom/msc/b/c;->a()J

    move-result-wide v0

    .line 982
    invoke-virtual {p1}, Lcom/msc/b/c;->e()Ljava/lang/String;

    move-result-object v2

    .line 984
    iget-wide v4, p0, Lcom/osp/app/pushmarketing/i;->e:J

    cmp-long v3, v0, v4

    if-nez v3, :cond_4

    .line 986
    if-eqz v2, :cond_0

    .line 989
    const-string v0, "PMS_5001"

    iget-object v1, p0, Lcom/osp/app/pushmarketing/i;->b:Lcom/msc/c/f;

    invoke-virtual {v1}, Lcom/msc/c/f;->a()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 991
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v0, "PMS"

    const-string v1, "PMS internal server error occured"

    invoke-static {v0, v1}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 992
    const-string v0, "Newtork Error"

    iput-object v0, p0, Lcom/osp/app/pushmarketing/i;->h:Ljava/lang/String;

    goto :goto_0

    .line 993
    :cond_2
    const-string v0, "ACF_0403"

    iget-object v1, p0, Lcom/osp/app/pushmarketing/i;->b:Lcom/msc/c/f;

    invoke-virtual {v1}, Lcom/msc/c/f;->a()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 995
    iget-boolean v0, p0, Lcom/osp/app/pushmarketing/i;->g:Z

    if-nez v0, :cond_3

    .line 997
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/osp/app/pushmarketing/i;->g:Z

    .line 998
    iget-object v0, p0, Lcom/osp/app/pushmarketing/i;->c:Lcom/osp/app/pushmarketing/PushMarketingService;

    const-string v1, ""

    invoke-static {v0, v1}, Lcom/msc/c/e;->b(Landroid/content/Context;Ljava/lang/String;)V

    .line 999
    invoke-direct {p0}, Lcom/osp/app/pushmarketing/i;->h()V

    goto :goto_0

    .line 1002
    :cond_3
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v0, "PMS"

    const-string v1, "Fail to get Accesstoken"

    invoke-static {v0, v1}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 1003
    const-string v0, "Fail"

    iput-object v0, p0, Lcom/osp/app/pushmarketing/i;->h:Ljava/lang/String;

    goto :goto_0

    .line 1007
    :cond_4
    iget-wide v4, p0, Lcom/osp/app/pushmarketing/i;->f:J

    cmp-long v0, v0, v4

    if-nez v0, :cond_0

    .line 1009
    if-eqz v2, :cond_0

    .line 1011
    const-string v0, "AUT_1302"

    iget-object v1, p0, Lcom/osp/app/pushmarketing/i;->b:Lcom/msc/c/f;

    invoke-virtual {v1}, Lcom/msc/c/f;->a()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_5

    const-string v0, "AUT_1830"

    iget-object v1, p0, Lcom/osp/app/pushmarketing/i;->b:Lcom/msc/c/f;

    invoke-virtual {v1}, Lcom/msc/c/f;->a()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_6

    .line 1013
    :cond_5
    iget-object v0, p0, Lcom/osp/app/pushmarketing/i;->c:Lcom/osp/app/pushmarketing/PushMarketingService;

    invoke-static {v0}, Lcom/msc/c/c;->g(Landroid/content/Context;)V

    .line 1014
    iget-object v0, p0, Lcom/osp/app/pushmarketing/i;->c:Lcom/osp/app/pushmarketing/PushMarketingService;

    invoke-static {v0}, Lcom/msc/c/e;->a(Landroid/content/Context;)Z

    .line 1015
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v0, "PMS"

    const-string v1, "Require resign-in process"

    invoke-static {v0, v1}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 1016
    const-string v0, "require re-SignIn"

    iput-object v0, p0, Lcom/osp/app/pushmarketing/i;->h:Ljava/lang/String;

    goto/16 :goto_0

    .line 1017
    :cond_6
    invoke-static {}, Lcom/osp/device/b;->b()Lcom/osp/device/b;

    move-result-object v0

    invoke-virtual {v0}, Lcom/osp/device/b;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    const-string v0, "AUT_1094"

    iget-object v1, p0, Lcom/osp/app/pushmarketing/i;->b:Lcom/msc/c/f;

    invoke-virtual {v1}, Lcom/msc/c/f;->a()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1019
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v0, "PMS"

    const-string v1, "Require resign-in with sign out process"

    invoke-static {v0, v1}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 1020
    const-string v0, "require re-SignIn with signout"

    iput-object v0, p0, Lcom/osp/app/pushmarketing/i;->h:Ljava/lang/String;

    goto/16 :goto_0
.end method

.method protected final d()V
    .locals 1

    .prologue
    .line 1067
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v0, "PMS ClickFeedBackTask is canceled"

    invoke-static {v0}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;)V

    .line 1068
    const-string v0, "Cancel"

    iput-object v0, p0, Lcom/osp/app/pushmarketing/i;->h:Ljava/lang/String;

    .line 1069
    invoke-super {p0}, Lcom/msc/c/b;->d()V

    .line 1070
    return-void
.end method

.method protected final synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 874
    invoke-virtual {p0}, Lcom/osp/app/pushmarketing/i;->a()Ljava/lang/Boolean;

    move-result-object v0

    return-object v0
.end method

.method protected final synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 874
    check-cast p1, Ljava/lang/Boolean;

    invoke-virtual {p0, p1}, Lcom/osp/app/pushmarketing/i;->a(Ljava/lang/Boolean;)V

    return-void
.end method
