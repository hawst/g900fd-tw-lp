.class public final Lcom/osp/app/pushmarketing/g;
.super Ljava/lang/Object;
.source "PushMarketingRequestSet.java"


# instance fields
.field private final a:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 19
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 21
    const-string v0, "PMRQ"

    iput-object v0, p0, Lcom/osp/app/pushmarketing/g;->a:Ljava/lang/String;

    return-void
.end method

.method public static a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/msc/b/h;)J
    .locals 15

    .prologue
    .line 56
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v2, "PMRQ"

    const-string v3, "prepareRegistrationPMS()"

    invoke-static {v2, v3}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 62
    const-string v2, ""

    .line 64
    :try_start_0
    invoke-static {}, Lcom/osp/common/util/i;->a()Lcom/osp/common/util/i;

    invoke-static {p0}, Lcom/osp/common/util/i;->f(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v9

    .line 69
    invoke-static {}, Lcom/osp/common/util/i;->a()Lcom/osp/common/util/i;

    invoke-static {p0}, Lcom/osp/common/util/i;->k(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v10

    .line 70
    invoke-static {}, Lcom/osp/common/util/i;->a()Lcom/osp/common/util/i;

    invoke-static {}, Lcom/osp/common/util/i;->b()Ljava/lang/String;

    move-result-object v11

    .line 71
    invoke-static {}, Lcom/osp/common/util/i;->a()Lcom/osp/common/util/i;

    invoke-static {}, Lcom/osp/common/util/i;->c()Ljava/lang/String;

    move-result-object v3

    sget-object v4, Ljava/util/Locale;->ENGLISH:Ljava/util/Locale;

    invoke-virtual {v3, v4}, Ljava/lang/String;->toUpperCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v12

    .line 77
    invoke-static {p0}, Lcom/osp/device/d;->a(Landroid/content/Context;)Lcom/osp/device/a;

    move-result-object v3

    .line 78
    if-eqz v3, :cond_0

    .line 82
    invoke-virtual {v3}, Lcom/osp/device/a;->d()Ljava/lang/String;

    move-result-object v2

    .line 83
    const-string v3, ":"

    invoke-virtual {v2, v3}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v3

    add-int/lit8 v3, v3, 0x1

    invoke-virtual {v2, v3}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v2

    move-object v8, v2

    .line 86
    :goto_0
    sget v2, Landroid/os/Build$VERSION;->SDK_INT:I

    invoke-static {v2}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v13

    .line 87
    invoke-virtual {p0}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v2

    invoke-virtual {p0}, Landroid/content/Context;->getApplicationInfo()Landroid/content/pm/ApplicationInfo;

    move-result-object v3

    iget-object v3, v3, Landroid/content/pm/ApplicationInfo;->packageName:Ljava/lang/String;

    const/4 v4, 0x0

    invoke-virtual {v2, v3, v4}, Landroid/content/pm/PackageManager;->getPackageInfo(Ljava/lang/String;I)Landroid/content/pm/PackageInfo;

    move-result-object v2

    iget v2, v2, Landroid/content/pm/PackageInfo;->versionCode:I

    invoke-static {v2}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v14

    .line 95
    invoke-static {}, Lcom/osp/app/util/ah;->a()Lcom/osp/app/util/ah;

    move-result-object v2

    invoke-virtual {v2}, Lcom/osp/app/util/ah;->g()Z

    move-result v6

    .line 97
    invoke-static {}, Lcom/msc/b/i;->a()Lcom/msc/b/i;

    move-result-object v2

    const-string v3, "https://eu-crm.samsungosp.com/v2/pms/application/regitDevice"

    invoke-static {}, Lcom/osp/app/signin/SamsungService;->j()Z

    move-result v4

    const/4 v5, 0x0

    move-object/from16 v7, p5

    invoke-virtual/range {v2 .. v7}, Lcom/msc/b/i;->a(Ljava/lang/String;ZZZLcom/msc/b/h;)Lcom/msc/b/d;

    move-result-object v2

    .line 100
    const-string v3, "content-type"

    const-string v4, "application/x-www-form-urlencoded;charset=UTF-8"

    invoke-virtual {v2, v3, v4}, Lcom/msc/b/d;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 101
    const-string v3, "accept"

    const-string v4, "application/json"

    invoke-virtual {v2, v3, v4}, Lcom/msc/b/d;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 102
    const-string v3, "x-osp-authToken"

    move-object/from16 v0, p3

    invoke-virtual {v2, v3, v0}, Lcom/msc/b/d;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 103
    const-string v3, "x-osp-appId"

    const-string v4, "j5p7ll8g33"

    invoke-virtual {v2, v3, v4}, Lcom/msc/b/d;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 104
    const-string v3, "x-osp-userId"

    move-object/from16 v0, p2

    invoke-virtual {v2, v3, v0}, Lcom/msc/b/d;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 133
    const-string v3, "guId"

    move-object/from16 v0, p2

    invoke-virtual {v2, v3, v0}, Lcom/msc/b/d;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 134
    const-string v3, "userId"

    move-object/from16 v0, p1

    invoke-virtual {v2, v3, v0}, Lcom/msc/b/d;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 135
    const-string v3, "regId"

    move-object/from16 v0, p4

    invoke-virtual {v2, v3, v0}, Lcom/msc/b/d;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 136
    const-string v3, "dvceModelNm"

    invoke-virtual {v2, v3, v12}, Lcom/msc/b/d;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 137
    const-string v3, "mblCntyCd"

    invoke-virtual {v2, v3, v9}, Lcom/msc/b/d;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 138
    const-string v3, "mblNtwkCd"

    invoke-virtual {v2, v3, v10}, Lcom/msc/b/d;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 139
    const-string v3, "dvceCustCd"

    invoke-virtual {v2, v3, v11}, Lcom/msc/b/d;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 140
    const-string v3, "dvceUnqId"

    invoke-virtual {v2, v3, v8}, Lcom/msc/b/d;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 141
    const-string v3, "osVersion"

    invoke-virtual {v2, v3, v13}, Lcom/msc/b/d;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 142
    const-string v3, "versionCode"

    invoke-virtual {v2, v3, v14}, Lcom/msc/b/d;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 144
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "PMRQ osVersion : "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;)V

    .line 145
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "PMRQ versionCode : "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;)V

    .line 147
    invoke-virtual {v2}, Lcom/msc/b/d;->a()J

    move-result-wide v2

    :goto_1
    return-wide v2

    .line 89
    :catch_0
    move-exception v2

    invoke-virtual {v2}, Ljava/lang/Exception;->printStackTrace()V

    .line 92
    const-wide/16 v2, -0x1

    goto :goto_1

    :cond_0
    move-object v8, v2

    goto/16 :goto_0
.end method

.method public static a(JLcom/msc/b/g;)V
    .locals 2

    .prologue
    .line 202
    invoke-static {}, Lcom/msc/b/i;->a()Lcom/msc/b/i;

    move-result-object v0

    invoke-virtual {v0, p0, p1, p2}, Lcom/msc/b/i;->a(JLcom/msc/b/g;)V

    .line 203
    return-void
.end method
