.class final Lcom/osp/app/pushmarketing/c;
.super Landroid/os/AsyncTask;
.source "PushMarketingNotification.java"


# instance fields
.field final synthetic a:Lcom/osp/app/pushmarketing/a;

.field private final b:Lcom/osp/app/pushmarketing/d;

.field private final c:Ljava/lang/String;

.field private final d:Landroid/content/Context;


# direct methods
.method public constructor <init>(Lcom/osp/app/pushmarketing/a;Landroid/content/Context;Ljava/lang/String;Lcom/osp/app/pushmarketing/d;)V
    .locals 0

    .prologue
    .line 436
    iput-object p1, p0, Lcom/osp/app/pushmarketing/c;->a:Lcom/osp/app/pushmarketing/a;

    invoke-direct {p0}, Landroid/os/AsyncTask;-><init>()V

    .line 437
    iput-object p3, p0, Lcom/osp/app/pushmarketing/c;->c:Ljava/lang/String;

    .line 438
    iput-object p4, p0, Lcom/osp/app/pushmarketing/c;->b:Lcom/osp/app/pushmarketing/d;

    .line 439
    iput-object p2, p0, Lcom/osp/app/pushmarketing/c;->d:Landroid/content/Context;

    .line 440
    return-void
.end method

.method private varargs a()Landroid/graphics/Bitmap;
    .locals 2

    .prologue
    .line 445
    :try_start_0
    new-instance v0, Ljava/net/URL;

    iget-object v1, p0, Lcom/osp/app/pushmarketing/c;->c:Ljava/lang/String;

    invoke-direct {v0, v1}, Ljava/net/URL;-><init>(Ljava/lang/String;)V

    .line 450
    invoke-virtual {v0}, Ljava/net/URL;->openStream()Ljava/io/InputStream;

    move-result-object v0

    .line 451
    invoke-static {v0}, Landroid/graphics/BitmapFactory;->decodeStream(Ljava/io/InputStream;)Landroid/graphics/Bitmap;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 460
    :goto_0
    return-object v0

    .line 453
    :catch_0
    move-exception v0

    move-object v1, v0

    .line 455
    const/4 v0, 0x0

    .line 457
    invoke-virtual {v1}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_0
.end method


# virtual methods
.method protected final synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 430
    invoke-direct {p0}, Lcom/osp/app/pushmarketing/c;->a()Landroid/graphics/Bitmap;

    move-result-object v0

    return-object v0
.end method

.method protected final synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 3

    .prologue
    .line 430
    check-cast p1, Landroid/graphics/Bitmap;

    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v0, "PMN"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "GetImageFromUrlTask onPostExecute : "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, Lcom/osp/app/pushmarketing/c;->b:Lcom/osp/app/pushmarketing/d;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;)V

    sget-object v0, Lcom/osp/app/pushmarketing/b;->a:[I

    iget-object v1, p0, Lcom/osp/app/pushmarketing/c;->b:Lcom/osp/app/pushmarketing/d;

    invoke-virtual {v1}, Lcom/osp/app/pushmarketing/d;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    :goto_0
    iget-object v0, p0, Lcom/osp/app/pushmarketing/c;->a:Lcom/osp/app/pushmarketing/a;

    iget-object v1, p0, Lcom/osp/app/pushmarketing/c;->d:Landroid/content/Context;

    invoke-static {v0, v1}, Lcom/osp/app/pushmarketing/a;->a(Lcom/osp/app/pushmarketing/a;Landroid/content/Context;)V

    return-void

    :pswitch_0
    iget-object v0, p0, Lcom/osp/app/pushmarketing/c;->a:Lcom/osp/app/pushmarketing/a;

    invoke-static {v0, p1}, Lcom/osp/app/pushmarketing/a;->a(Lcom/osp/app/pushmarketing/a;Landroid/graphics/Bitmap;)Landroid/graphics/Bitmap;

    iget-object v0, p0, Lcom/osp/app/pushmarketing/c;->a:Lcom/osp/app/pushmarketing/a;

    invoke-static {v0}, Lcom/osp/app/pushmarketing/a;->a(Lcom/osp/app/pushmarketing/a;)Z

    goto :goto_0

    :pswitch_1
    if-nez p1, :cond_0

    iget-object v0, p0, Lcom/osp/app/pushmarketing/c;->a:Lcom/osp/app/pushmarketing/a;

    invoke-static {v0}, Lcom/osp/app/pushmarketing/a;->b(Lcom/osp/app/pushmarketing/a;)Lcom/osp/app/pushmarketing/e;

    move-result-object v0

    const-string v1, "normal"

    invoke-virtual {v0, v1}, Lcom/osp/app/pushmarketing/e;->a(Ljava/lang/String;)V

    :goto_1
    iget-object v0, p0, Lcom/osp/app/pushmarketing/c;->a:Lcom/osp/app/pushmarketing/a;

    invoke-static {v0}, Lcom/osp/app/pushmarketing/a;->c(Lcom/osp/app/pushmarketing/a;)Z

    goto :goto_0

    :cond_0
    iget-object v0, p0, Lcom/osp/app/pushmarketing/c;->a:Lcom/osp/app/pushmarketing/a;

    invoke-static {v0, p1}, Lcom/osp/app/pushmarketing/a;->b(Lcom/osp/app/pushmarketing/a;Landroid/graphics/Bitmap;)Landroid/graphics/Bitmap;

    goto :goto_1

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method
