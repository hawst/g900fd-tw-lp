.class final Lcom/osp/app/pushmarketing/j;
.super Lcom/msc/c/b;
.source "PushMarketingService.java"


# instance fields
.field final synthetic c:Lcom/osp/app/pushmarketing/PushMarketingService;

.field private d:J

.field private e:J

.field private f:Z

.field private g:Ljava/lang/String;


# direct methods
.method public constructor <init>(Lcom/osp/app/pushmarketing/PushMarketingService;)V
    .locals 1

    .prologue
    .line 1088
    iput-object p1, p0, Lcom/osp/app/pushmarketing/j;->c:Lcom/osp/app/pushmarketing/PushMarketingService;

    .line 1089
    invoke-direct {p0, p1}, Lcom/msc/c/b;-><init>(Landroid/content/Context;)V

    .line 1085
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/osp/app/pushmarketing/j;->f:Z

    .line 1086
    const-string v0, "Fail"

    iput-object v0, p0, Lcom/osp/app/pushmarketing/j;->g:Ljava/lang/String;

    .line 1090
    invoke-virtual {p0}, Lcom/osp/app/pushmarketing/j;->c()V

    .line 1091
    return-void
.end method

.method private b(Ljava/lang/String;)V
    .locals 6

    .prologue
    .line 1253
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v0, "PMS requestRegistPMS called"

    invoke-static {v0}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;)V

    .line 1255
    sget-object v0, Lcom/osp/app/pushmarketing/l;->a:Lcom/osp/app/pushmarketing/k;

    iget-object v0, p0, Lcom/osp/app/pushmarketing/j;->c:Lcom/osp/app/pushmarketing/PushMarketingService;

    invoke-static {v0}, Lcom/osp/app/pushmarketing/k;->b(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v4

    .line 1256
    sget-object v0, Lcom/osp/app/pushmarketing/l;->a:Lcom/osp/app/pushmarketing/k;

    invoke-static {v4}, Lcom/osp/app/pushmarketing/k;->a(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1258
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v0, "PMS"

    const-string v1, "regID is null"

    invoke-static {v0, v1}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 1271
    :goto_0
    return-void

    .line 1265
    :cond_0
    new-instance v0, Lcom/osp/app/pushmarketing/g;

    invoke-direct {v0}, Lcom/osp/app/pushmarketing/g;-><init>()V

    .line 1266
    iget-object v0, p0, Lcom/osp/app/pushmarketing/j;->c:Lcom/osp/app/pushmarketing/PushMarketingService;

    iget-object v1, p0, Lcom/osp/app/pushmarketing/j;->c:Lcom/osp/app/pushmarketing/PushMarketingService;

    invoke-static {v1}, Lcom/osp/app/util/ad;->h(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/osp/app/pushmarketing/j;->c:Lcom/osp/app/pushmarketing/PushMarketingService;

    invoke-static {v2}, Lcom/msc/c/e;->h(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v2

    move-object v3, p1

    move-object v5, p0

    invoke-static/range {v0 .. v5}, Lcom/osp/app/pushmarketing/g;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/msc/b/h;)J

    move-result-wide v0

    iput-wide v0, p0, Lcom/osp/app/pushmarketing/j;->d:J

    .line 1268
    iget-wide v0, p0, Lcom/osp/app/pushmarketing/j;->d:J

    invoke-virtual {p0, v0, v1}, Lcom/osp/app/pushmarketing/j;->a(J)V

    .line 1269
    iget-wide v0, p0, Lcom/osp/app/pushmarketing/j;->d:J

    const-string v2, "from_json"

    invoke-virtual {p0, v0, v1, v2}, Lcom/osp/app/pushmarketing/j;->a(JLjava/lang/String;)V

    .line 1270
    iget-wide v0, p0, Lcom/osp/app/pushmarketing/j;->d:J

    sget-object v2, Lcom/msc/b/g;->c:Lcom/msc/b/g;

    invoke-static {v0, v1, v2}, Lcom/osp/app/pushmarketing/g;->a(JLcom/msc/b/g;)V

    goto :goto_0
.end method

.method private h()V
    .locals 8

    .prologue
    const/4 v3, 0x0

    .line 1234
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v0, "PMS requestAuthenticationV02 called"

    invoke-static {v0}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;)V

    .line 1236
    invoke-static {}, Lcom/msc/c/g;->a()Lcom/msc/c/g;

    .line 1237
    iget-object v0, p0, Lcom/osp/app/pushmarketing/j;->c:Lcom/osp/app/pushmarketing/PushMarketingService;

    const-string v1, "j5p7ll8g33"

    const-string v2, "5763D0052DC1462E13751F753384E9A9"

    iget-object v4, p0, Lcom/osp/app/pushmarketing/j;->c:Lcom/osp/app/pushmarketing/PushMarketingService;

    invoke-static {v4}, Lcom/msc/c/e;->g(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v5

    move-object v4, v3

    move-object v6, v3

    move-object v7, p0

    invoke-static/range {v0 .. v7}, Lcom/msc/c/g;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/osp/app/signin/cn;Lcom/msc/b/h;)J

    move-result-wide v0

    iput-wide v0, p0, Lcom/osp/app/pushmarketing/j;->e:J

    .line 1240
    iget-wide v0, p0, Lcom/osp/app/pushmarketing/j;->e:J

    invoke-virtual {p0, v0, v1}, Lcom/osp/app/pushmarketing/j;->b(J)V

    .line 1241
    iget-wide v0, p0, Lcom/osp/app/pushmarketing/j;->e:J

    const-string v2, "from_json"

    invoke-virtual {p0, v0, v1, v2}, Lcom/osp/app/pushmarketing/j;->a(JLjava/lang/String;)V

    .line 1243
    iget-wide v0, p0, Lcom/osp/app/pushmarketing/j;->e:J

    sget-object v2, Lcom/msc/b/g;->c:Lcom/msc/b/g;

    invoke-static {v0, v1, v2}, Lcom/msc/c/g;->a(JLcom/msc/b/g;)V

    .line 1244
    return-void
.end method


# virtual methods
.method protected final varargs a()Ljava/lang/Boolean;
    .locals 2

    .prologue
    .line 1097
    iget-object v0, p0, Lcom/osp/app/pushmarketing/j;->c:Lcom/osp/app/pushmarketing/PushMarketingService;

    invoke-static {v0}, Lcom/msc/c/e;->b(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v0

    .line 1098
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v1

    if-lez v1, :cond_0

    .line 1100
    invoke-direct {p0, v0}, Lcom/osp/app/pushmarketing/j;->b(Ljava/lang/String;)V

    .line 1106
    :goto_0
    const/4 v0, 0x0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    return-object v0

    .line 1103
    :cond_0
    invoke-direct {p0}, Lcom/osp/app/pushmarketing/j;->h()V

    goto :goto_0
.end method

.method public final declared-synchronized a(Lcom/msc/b/c;)V
    .locals 6

    .prologue
    .line 1133
    monitor-enter p0

    :try_start_0
    invoke-super {p0, p1}, Lcom/msc/c/b;->a(Lcom/msc/b/c;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1135
    if-nez p1, :cond_1

    .line 1173
    :cond_0
    :goto_0
    monitor-exit p0

    return-void

    .line 1140
    :cond_1
    :try_start_1
    invoke-virtual {p1}, Lcom/msc/b/c;->a()J

    move-result-wide v0

    .line 1141
    invoke-virtual {p1}, Lcom/msc/b/c;->e()Ljava/lang/String;

    move-result-object v2

    .line 1143
    iget-wide v4, p0, Lcom/osp/app/pushmarketing/j;->d:J

    cmp-long v3, v0, v4

    if-nez v3, :cond_2

    .line 1145
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v0, "PMS"

    const-string v1, "PMS Device Registration success"

    invoke-static {v0, v1}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 1146
    const-string v0, "Success"

    iput-object v0, p0, Lcom/osp/app/pushmarketing/j;->g:Ljava/lang/String;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 1133
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 1148
    :cond_2
    :try_start_2
    iget-wide v4, p0, Lcom/osp/app/pushmarketing/j;->e:J
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    cmp-long v0, v0, v4

    if-nez v0, :cond_0

    .line 1152
    :try_start_3
    new-instance v0, Lcom/msc/a/d;

    invoke-direct {v0}, Lcom/msc/a/d;-><init>()V

    .line 1153
    invoke-static {}, Lcom/msc/c/h;->a()Lcom/msc/c/h;

    invoke-static {v2, v0}, Lcom/msc/c/h;->a(Ljava/lang/String;Lcom/msc/a/d;)V

    .line 1154
    invoke-virtual {v0}, Lcom/msc/a/d;->g()Ljava/lang/String;

    move-result-object v0

    .line 1155
    if-eqz v0, :cond_3

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v1

    if-lez v1, :cond_3

    .line 1157
    iget-object v1, p0, Lcom/osp/app/pushmarketing/j;->c:Lcom/osp/app/pushmarketing/PushMarketingService;

    invoke-static {v1, v0}, Lcom/msc/c/e;->b(Landroid/content/Context;Ljava/lang/String;)V

    .line 1158
    invoke-direct {p0, v0}, Lcom/osp/app/pushmarketing/j;->b(Ljava/lang/String;)V
    :try_end_3
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_0
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    goto :goto_0

    .line 1165
    :catch_0
    move-exception v0

    .line 1167
    :try_start_4
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    .line 1168
    new-instance v1, Lcom/msc/c/f;

    invoke-direct {v1, v0}, Lcom/msc/c/f;-><init>(Ljava/lang/Exception;)V

    iput-object v1, p0, Lcom/osp/app/pushmarketing/j;->b:Lcom/msc/c/f;

    .line 1169
    const-string v0, "Fail"

    iput-object v0, p0, Lcom/osp/app/pushmarketing/j;->g:Ljava/lang/String;
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    goto :goto_0

    .line 1161
    :cond_3
    :try_start_5
    const-string v0, "Fail"

    iput-object v0, p0, Lcom/osp/app/pushmarketing/j;->g:Ljava/lang/String;

    .line 1162
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v0, "PMS"

    const-string v1, "Fail to get accessToken"

    invoke-static {v0, v1}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 1163
    new-instance v0, Lcom/msc/c/f;

    invoke-direct {v0}, Lcom/msc/c/f;-><init>()V

    iput-object v0, p0, Lcom/osp/app/pushmarketing/j;->b:Lcom/msc/c/f;
    :try_end_5
    .catch Ljava/lang/Exception; {:try_start_5 .. :try_end_5} :catch_0
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    goto :goto_0
.end method

.method protected final a(Ljava/lang/Boolean;)V
    .locals 2

    .prologue
    .line 1112
    invoke-super {p0, p1}, Lcom/msc/c/b;->a(Ljava/lang/Boolean;)V

    .line 1114
    iget-object v0, p0, Lcom/osp/app/pushmarketing/j;->g:Ljava/lang/String;

    const-string v1, "Success"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1116
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v0, "PMS"

    const-string v1, "PMS device registration is complete"

    invoke-static {v0, v1}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 1129
    :goto_0
    return-void

    .line 1118
    :cond_0
    iget-object v0, p0, Lcom/osp/app/pushmarketing/j;->g:Ljava/lang/String;

    const-string v1, "require re-SignIn"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1120
    iget-object v0, p0, Lcom/osp/app/pushmarketing/j;->c:Lcom/osp/app/pushmarketing/PushMarketingService;

    invoke-static {v0}, Lcom/osp/app/pushmarketing/PushMarketingService;->h(Lcom/osp/app/pushmarketing/PushMarketingService;)V

    goto :goto_0

    .line 1121
    :cond_1
    iget-object v0, p0, Lcom/osp/app/pushmarketing/j;->g:Ljava/lang/String;

    const-string v1, "require re-SignIn with signout"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 1123
    iget-object v0, p0, Lcom/osp/app/pushmarketing/j;->c:Lcom/osp/app/pushmarketing/PushMarketingService;

    invoke-static {v0}, Lcom/osp/app/pushmarketing/PushMarketingService;->i(Lcom/osp/app/pushmarketing/PushMarketingService;)V

    goto :goto_0

    .line 1126
    :cond_2
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v0, "PMS"

    const-string v1, "PMS device registration error occured"

    invoke-static {v0, v1}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public final b(Lcom/msc/b/c;)V
    .locals 6

    .prologue
    .line 1177
    invoke-super {p0, p1}, Lcom/msc/c/b;->b(Lcom/msc/b/c;)V

    .line 1179
    if-nez p1, :cond_1

    .line 1227
    :cond_0
    :goto_0
    return-void

    .line 1184
    :cond_1
    invoke-virtual {p1}, Lcom/msc/b/c;->a()J

    move-result-wide v0

    .line 1185
    invoke-virtual {p1}, Lcom/msc/b/c;->e()Ljava/lang/String;

    move-result-object v2

    .line 1187
    iget-wide v4, p0, Lcom/osp/app/pushmarketing/j;->d:J

    cmp-long v3, v0, v4

    if-nez v3, :cond_4

    .line 1189
    if-eqz v2, :cond_0

    .line 1192
    const-string v0, "PMS_5001"

    iget-object v1, p0, Lcom/osp/app/pushmarketing/j;->b:Lcom/msc/c/f;

    invoke-virtual {v1}, Lcom/msc/c/f;->a()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 1194
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v0, "PMS"

    const-string v1, "PMS internal server error occured"

    invoke-static {v0, v1}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 1195
    const-string v0, "Newtork Error"

    iput-object v0, p0, Lcom/osp/app/pushmarketing/j;->g:Ljava/lang/String;

    goto :goto_0

    .line 1196
    :cond_2
    const-string v0, "ACF_0403"

    iget-object v1, p0, Lcom/osp/app/pushmarketing/j;->b:Lcom/msc/c/f;

    invoke-virtual {v1}, Lcom/msc/c/f;->a()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1198
    iget-boolean v0, p0, Lcom/osp/app/pushmarketing/j;->f:Z

    if-nez v0, :cond_3

    .line 1200
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/osp/app/pushmarketing/j;->f:Z

    .line 1201
    iget-object v0, p0, Lcom/osp/app/pushmarketing/j;->c:Lcom/osp/app/pushmarketing/PushMarketingService;

    const-string v1, ""

    invoke-static {v0, v1}, Lcom/msc/c/e;->b(Landroid/content/Context;Ljava/lang/String;)V

    .line 1202
    invoke-direct {p0}, Lcom/osp/app/pushmarketing/j;->h()V

    goto :goto_0

    .line 1205
    :cond_3
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v0, "PMS"

    const-string v1, "Fail to get Accesstoken"

    invoke-static {v0, v1}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 1206
    const-string v0, "Fail"

    iput-object v0, p0, Lcom/osp/app/pushmarketing/j;->g:Ljava/lang/String;

    goto :goto_0

    .line 1210
    :cond_4
    iget-wide v4, p0, Lcom/osp/app/pushmarketing/j;->e:J

    cmp-long v0, v0, v4

    if-nez v0, :cond_0

    .line 1212
    if-eqz v2, :cond_0

    .line 1214
    const-string v0, "AUT_1302"

    iget-object v1, p0, Lcom/osp/app/pushmarketing/j;->b:Lcom/msc/c/f;

    invoke-virtual {v1}, Lcom/msc/c/f;->a()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_5

    const-string v0, "AUT_1830"

    iget-object v1, p0, Lcom/osp/app/pushmarketing/j;->b:Lcom/msc/c/f;

    invoke-virtual {v1}, Lcom/msc/c/f;->a()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_6

    .line 1216
    :cond_5
    iget-object v0, p0, Lcom/osp/app/pushmarketing/j;->c:Lcom/osp/app/pushmarketing/PushMarketingService;

    invoke-static {v0}, Lcom/msc/c/c;->g(Landroid/content/Context;)V

    .line 1217
    iget-object v0, p0, Lcom/osp/app/pushmarketing/j;->c:Lcom/osp/app/pushmarketing/PushMarketingService;

    invoke-static {v0}, Lcom/msc/c/e;->a(Landroid/content/Context;)Z

    .line 1218
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v0, "PMS"

    const-string v1, "Require resign-in process"

    invoke-static {v0, v1}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 1219
    const-string v0, "require re-SignIn"

    iput-object v0, p0, Lcom/osp/app/pushmarketing/j;->g:Ljava/lang/String;

    goto/16 :goto_0

    .line 1220
    :cond_6
    invoke-static {}, Lcom/osp/device/b;->b()Lcom/osp/device/b;

    move-result-object v0

    invoke-virtual {v0}, Lcom/osp/device/b;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    const-string v0, "AUT_1094"

    iget-object v1, p0, Lcom/osp/app/pushmarketing/j;->b:Lcom/msc/c/f;

    invoke-virtual {v1}, Lcom/msc/c/f;->a()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1222
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v0, "PMS"

    const-string v1, "Require resign-in with sign out process"

    invoke-static {v0, v1}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 1223
    const-string v0, "require re-SignIn with signout"

    iput-object v0, p0, Lcom/osp/app/pushmarketing/j;->g:Ljava/lang/String;

    goto/16 :goto_0
.end method

.method protected final d()V
    .locals 1

    .prologue
    .line 1276
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v0, "PMS PushMarketingDeviceRegitTask is canceled"

    invoke-static {v0}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;)V

    .line 1277
    const-string v0, "Cancel"

    iput-object v0, p0, Lcom/osp/app/pushmarketing/j;->g:Ljava/lang/String;

    .line 1278
    invoke-super {p0}, Lcom/msc/c/b;->d()V

    .line 1279
    return-void
.end method

.method protected final synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 1078
    invoke-virtual {p0}, Lcom/osp/app/pushmarketing/j;->a()Ljava/lang/Boolean;

    move-result-object v0

    return-object v0
.end method

.method protected final synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 1078
    check-cast p1, Ljava/lang/Boolean;

    invoke-virtual {p0, p1}, Lcom/osp/app/pushmarketing/j;->a(Ljava/lang/Boolean;)V

    return-void
.end method
