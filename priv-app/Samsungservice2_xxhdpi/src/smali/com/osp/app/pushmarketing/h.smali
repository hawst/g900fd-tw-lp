.class final Lcom/osp/app/pushmarketing/h;
.super Ljava/lang/Object;
.source "PushMarketingService.java"

# interfaces
.implements Landroid/content/ServiceConnection;


# instance fields
.field final synthetic a:Lcom/osp/app/pushmarketing/PushMarketingService;


# direct methods
.method constructor <init>(Lcom/osp/app/pushmarketing/PushMarketingService;)V
    .locals 0

    .prologue
    .line 590
    iput-object p1, p0, Lcom/osp/app/pushmarketing/h;->a:Lcom/osp/app/pushmarketing/PushMarketingService;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onServiceConnected(Landroid/content/ComponentName;Landroid/os/IBinder;)V
    .locals 2

    .prologue
    .line 601
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v0, "PMS"

    const-string v1, "IPushClientService is connected"

    invoke-static {v0, v1}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 603
    iget-object v0, p0, Lcom/osp/app/pushmarketing/h;->a:Lcom/osp/app/pushmarketing/PushMarketingService;

    invoke-static {p2}, Lcom/sec/b/a/b;->a(Landroid/os/IBinder;)Lcom/sec/b/a/a;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/osp/app/pushmarketing/PushMarketingService;->a(Lcom/osp/app/pushmarketing/PushMarketingService;Lcom/sec/b/a/a;)Lcom/sec/b/a/a;

    .line 604
    iget-object v0, p0, Lcom/osp/app/pushmarketing/h;->a:Lcom/osp/app/pushmarketing/PushMarketingService;

    invoke-static {v0}, Lcom/osp/app/pushmarketing/PushMarketingService;->b(Lcom/osp/app/pushmarketing/PushMarketingService;)Lcom/sec/b/a/a;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 606
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v0, "PMS"

    const-string v1, "IPushClientService is OK"

    invoke-static {v0, v1}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 607
    iget-object v0, p0, Lcom/osp/app/pushmarketing/h;->a:Lcom/osp/app/pushmarketing/PushMarketingService;

    invoke-static {v0}, Lcom/osp/app/pushmarketing/PushMarketingService;->c(Lcom/osp/app/pushmarketing/PushMarketingService;)I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    .line 631
    :goto_0
    return-void

    .line 610
    :sswitch_0
    iget-object v0, p0, Lcom/osp/app/pushmarketing/h;->a:Lcom/osp/app/pushmarketing/PushMarketingService;

    invoke-static {v0}, Lcom/osp/app/pushmarketing/PushMarketingService;->d(Lcom/osp/app/pushmarketing/PushMarketingService;)V

    goto :goto_0

    .line 613
    :sswitch_1
    iget-object v0, p0, Lcom/osp/app/pushmarketing/h;->a:Lcom/osp/app/pushmarketing/PushMarketingService;

    invoke-static {v0}, Lcom/osp/app/pushmarketing/PushMarketingService;->e(Lcom/osp/app/pushmarketing/PushMarketingService;)V

    goto :goto_0

    .line 616
    :sswitch_2
    iget-object v0, p0, Lcom/osp/app/pushmarketing/h;->a:Lcom/osp/app/pushmarketing/PushMarketingService;

    invoke-static {v0}, Lcom/osp/app/pushmarketing/PushMarketingService;->f(Lcom/osp/app/pushmarketing/PushMarketingService;)V

    goto :goto_0

    .line 619
    :sswitch_3
    iget-object v0, p0, Lcom/osp/app/pushmarketing/h;->a:Lcom/osp/app/pushmarketing/PushMarketingService;

    invoke-static {v0}, Lcom/osp/app/pushmarketing/PushMarketingService;->g(Lcom/osp/app/pushmarketing/PushMarketingService;)V

    goto :goto_0

    .line 628
    :cond_0
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v0, "PMS"

    const-string v1, "IPushClientService is NULL"

    invoke-static {v0, v1}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 607
    :sswitch_data_0
    .sparse-switch
        0x1 -> :sswitch_0
        0x2 -> :sswitch_3
        0x3 -> :sswitch_2
        0x63 -> :sswitch_1
    .end sparse-switch
.end method

.method public final onServiceDisconnected(Landroid/content/ComponentName;)V
    .locals 2

    .prologue
    .line 594
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v0, "PMS"

    const-string v1, "IPushClientService is disconnected"

    invoke-static {v0, v1}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 595
    iget-object v0, p0, Lcom/osp/app/pushmarketing/h;->a:Lcom/osp/app/pushmarketing/PushMarketingService;

    invoke-static {v0}, Lcom/osp/app/pushmarketing/PushMarketingService;->a(Lcom/osp/app/pushmarketing/PushMarketingService;)Z

    .line 596
    iget-object v0, p0, Lcom/osp/app/pushmarketing/h;->a:Lcom/osp/app/pushmarketing/PushMarketingService;

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lcom/osp/app/pushmarketing/PushMarketingService;->a(Lcom/osp/app/pushmarketing/PushMarketingService;Lcom/sec/b/a/a;)Lcom/sec/b/a/a;

    .line 597
    return-void
.end method
