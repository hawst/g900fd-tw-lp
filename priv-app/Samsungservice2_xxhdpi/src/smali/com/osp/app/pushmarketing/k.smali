.class public final Lcom/osp/app/pushmarketing/k;
.super Ljava/lang/Object;
.source "PushMarketingUtil.java"


# instance fields
.field private final a:Ljava/lang/String;

.field private final b:Ljava/lang/String;

.field private final c:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 23
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 28
    const-string v0, "PMV"

    iput-object v0, p0, Lcom/osp/app/pushmarketing/k;->a:Ljava/lang/String;

    .line 33
    const-string v0, "notification_id"

    iput-object v0, p0, Lcom/osp/app/pushmarketing/k;->b:Ljava/lang/String;

    .line 38
    const-string v0, "reg_id"

    iput-object v0, p0, Lcom/osp/app/pushmarketing/k;->c:Ljava/lang/String;

    .line 46
    return-void
.end method

.method public static a(Landroid/content/Context;[Ljava/lang/String;I)I
    .locals 5

    .prologue
    const/16 v4, 0x13

    .line 341
    const/4 v0, 0x0

    :goto_0
    array-length v1, p1

    if-ge v0, v1, :cond_11

    .line 343
    aget-object v1, p1, v0

    .line 345
    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const/high16 v3, 0x7f090000

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 347
    packed-switch p2, :pswitch_data_0

    .line 353
    const v0, 0x7f0200bc

    .line 546
    :goto_1
    return v0

    .line 350
    :pswitch_0
    const v0, 0x7f0200ab

    goto :goto_1

    .line 356
    :cond_0
    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f090001

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 358
    packed-switch p2, :pswitch_data_1

    .line 364
    const v0, 0x7f0200b7

    goto :goto_1

    .line 361
    :pswitch_1
    const v0, 0x7f0200a6

    goto :goto_1

    .line 367
    :cond_1
    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f090002

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 369
    packed-switch p2, :pswitch_data_2

    .line 375
    const v0, 0x7f0200bd

    goto :goto_1

    .line 372
    :pswitch_2
    const v0, 0x7f0200ac

    goto :goto_1

    .line 378
    :cond_2
    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f090003

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    .line 380
    packed-switch p2, :pswitch_data_3

    .line 386
    const v0, 0x7f0200c2

    goto :goto_1

    .line 383
    :pswitch_3
    const v0, 0x7f0200b1

    goto :goto_1

    .line 389
    :cond_3
    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f090004

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_4

    .line 391
    packed-switch p2, :pswitch_data_4

    .line 397
    const v0, 0x7f0200bf

    goto :goto_1

    .line 394
    :pswitch_4
    const v0, 0x7f0200ae

    goto :goto_1

    .line 400
    :cond_4
    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f090005

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_5

    .line 402
    packed-switch p2, :pswitch_data_5

    .line 408
    const v0, 0x7f0200bb

    goto/16 :goto_1

    .line 405
    :pswitch_5
    const v0, 0x7f0200aa

    goto/16 :goto_1

    .line 411
    :cond_5
    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f090006

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_6

    .line 413
    packed-switch p2, :pswitch_data_6

    .line 419
    const v0, 0x7f0200b6

    goto/16 :goto_1

    .line 416
    :pswitch_6
    const v0, 0x7f0200a5

    goto/16 :goto_1

    .line 422
    :cond_6
    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f090007

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_7

    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f090008

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_9

    .line 425
    :cond_7
    invoke-static {}, Lcom/osp/app/util/r;->j()Z

    move-result v0

    if-eqz v0, :cond_8

    .line 427
    packed-switch p2, :pswitch_data_7

    .line 433
    const v0, 0x7f0200b9

    goto/16 :goto_1

    .line 430
    :pswitch_7
    const v0, 0x7f0200a8

    goto/16 :goto_1

    .line 437
    :cond_8
    packed-switch p2, :pswitch_data_8

    .line 443
    const v0, 0x7f0200b8

    goto/16 :goto_1

    .line 440
    :pswitch_8
    const v0, 0x7f0200a7

    goto/16 :goto_1

    .line 446
    :cond_9
    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f090009

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_a

    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f09000a

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_b

    .line 449
    :cond_a
    packed-switch p2, :pswitch_data_9

    .line 455
    const v0, 0x7f0200c4

    goto/16 :goto_1

    .line 452
    :pswitch_9
    const v0, 0x7f0200b3

    goto/16 :goto_1

    .line 458
    :cond_b
    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f09000b

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_c

    .line 460
    packed-switch p2, :pswitch_data_a

    .line 466
    const v0, 0x7f0200c0

    goto/16 :goto_1

    .line 463
    :pswitch_a
    const v0, 0x7f0200af

    goto/16 :goto_1

    .line 469
    :cond_c
    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f09000c

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_d

    .line 471
    packed-switch p2, :pswitch_data_b

    .line 477
    const v0, 0x7f0200c3

    goto/16 :goto_1

    .line 474
    :pswitch_b
    const v0, 0x7f0200b2

    goto/16 :goto_1

    .line 480
    :cond_d
    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f09000d

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_e

    .line 482
    packed-switch p2, :pswitch_data_c

    .line 488
    const v0, 0x7f0200c1

    goto/16 :goto_1

    .line 485
    :pswitch_c
    const v0, 0x7f0200b0

    goto/16 :goto_1

    .line 500
    :cond_e
    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f090010

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_f

    .line 502
    packed-switch p2, :pswitch_data_d

    .line 508
    const v0, 0x7f0200be

    goto/16 :goto_1

    .line 505
    :pswitch_d
    const v0, 0x7f0200ad

    goto/16 :goto_1

    .line 517
    :cond_f
    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f090013

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_10

    .line 519
    packed-switch p2, :pswitch_data_e

    .line 525
    const v0, 0x7f0200ba

    goto/16 :goto_1

    .line 522
    :pswitch_e
    const v0, 0x7f0200a9

    goto/16 :goto_1

    .line 341
    :cond_10
    add-int/lit8 v0, v0, 0x1

    goto/16 :goto_0

    .line 532
    :cond_11
    packed-switch p2, :pswitch_data_f

    .line 542
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    if-lt v0, v4, :cond_13

    .line 544
    const v0, 0x7f0200b5

    goto/16 :goto_1

    .line 535
    :pswitch_f
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    if-lt v0, v4, :cond_12

    .line 537
    const v0, 0x7f0200a4

    goto/16 :goto_1

    .line 539
    :cond_12
    const v0, 0x7f0200a3

    goto/16 :goto_1

    .line 546
    :cond_13
    const v0, 0x7f0200b4

    goto/16 :goto_1

    .line 347
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
    .end packed-switch

    .line 358
    :pswitch_data_1
    .packed-switch 0x1
        :pswitch_1
    .end packed-switch

    .line 369
    :pswitch_data_2
    .packed-switch 0x1
        :pswitch_2
    .end packed-switch

    .line 380
    :pswitch_data_3
    .packed-switch 0x1
        :pswitch_3
    .end packed-switch

    .line 391
    :pswitch_data_4
    .packed-switch 0x1
        :pswitch_4
    .end packed-switch

    .line 402
    :pswitch_data_5
    .packed-switch 0x1
        :pswitch_5
    .end packed-switch

    .line 413
    :pswitch_data_6
    .packed-switch 0x1
        :pswitch_6
    .end packed-switch

    .line 427
    :pswitch_data_7
    .packed-switch 0x1
        :pswitch_7
    .end packed-switch

    .line 437
    :pswitch_data_8
    .packed-switch 0x1
        :pswitch_8
    .end packed-switch

    .line 449
    :pswitch_data_9
    .packed-switch 0x1
        :pswitch_9
    .end packed-switch

    .line 460
    :pswitch_data_a
    .packed-switch 0x1
        :pswitch_a
    .end packed-switch

    .line 471
    :pswitch_data_b
    .packed-switch 0x1
        :pswitch_b
    .end packed-switch

    .line 482
    :pswitch_data_c
    .packed-switch 0x1
        :pswitch_c
    .end packed-switch

    .line 502
    :pswitch_data_d
    .packed-switch 0x1
        :pswitch_d
    .end packed-switch

    .line 519
    :pswitch_data_e
    .packed-switch 0x1
        :pswitch_e
    .end packed-switch

    .line 532
    :pswitch_data_f
    .packed-switch 0x1
        :pswitch_f
    .end packed-switch
.end method

.method public static a()Lcom/osp/app/pushmarketing/k;
    .locals 1

    .prologue
    .line 57
    sget-object v0, Lcom/osp/app/pushmarketing/l;->a:Lcom/osp/app/pushmarketing/k;

    return-object v0
.end method

.method public static a(Landroid/content/Context;Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 582
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "PMV saveRegID : "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;)V

    .line 584
    invoke-static {p0}, Landroid/preference/PreferenceManager;->getDefaultSharedPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v0

    .line 585
    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    .line 587
    const-string v1, "reg_id"

    invoke-interface {v0, v1, p1}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    .line 588
    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 589
    return-void
.end method

.method public static a(Landroid/content/Context;Lcom/osp/app/pushmarketing/e;)Z
    .locals 4

    .prologue
    const/4 v1, 0x1

    const/4 v0, 0x0

    .line 118
    invoke-virtual {p1}, Lcom/osp/app/pushmarketing/e;->h()Ljava/lang/String;

    move-result-object v2

    if-eqz v2, :cond_0

    invoke-virtual {p1}, Lcom/osp/app/pushmarketing/e;->h()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/String;->isEmpty()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 120
    :cond_0
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v0, "PMV"

    const-string v2, "DUID is NULL : Pass checking duid procedure"

    invoke-static {v0, v2}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;)V

    move v0, v1

    .line 152
    :goto_0
    return v0

    .line 127
    :cond_1
    :try_start_0
    invoke-static {p0}, Lcom/osp/device/d;->a(Landroid/content/Context;)Lcom/osp/device/a;

    move-result-object v2

    .line 128
    if-nez v2, :cond_2

    .line 130
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v1, "PMV"

    const-string v2, "DeviceInfo is null"

    invoke-static {v1, v2}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 148
    :catch_0
    move-exception v1

    invoke-virtual {v1}, Ljava/lang/Exception;->printStackTrace()V

    .line 151
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v1, "PMV"

    const-string v2, "Error Obtaining DUID, Notification process is canceled"

    invoke-static {v1, v2}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 136
    :cond_2
    :try_start_1
    invoke-virtual {v2}, Lcom/osp/device/a;->d()Ljava/lang/String;

    move-result-object v2

    .line 137
    const-string v3, ":"

    invoke-virtual {v2, v3}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v3

    add-int/lit8 v3, v3, 0x1

    invoke-virtual {v2, v3}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v2

    .line 139
    invoke-virtual {p1}, Lcom/osp/app/pushmarketing/e;->h()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    .line 141
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v2, "PMV"

    const-string v3, "DUID CHECK SUCCESS"

    invoke-static {v2, v3}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;)V

    move v0, v1

    .line 142
    goto :goto_0

    .line 145
    :cond_3
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v1, "PMV"

    const-string v2, "Different DUID, Notification process is canceled"

    invoke-static {v1, v2}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_0
.end method

.method public static a(Lcom/osp/app/pushmarketing/e;I)Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 252
    if-nez p0, :cond_0

    .line 256
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v1, "PMV"

    const-string v2, "Button Enabled : payload is null"

    invoke-static {v1, v2}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 314
    :goto_0
    return v0

    .line 260
    :cond_0
    packed-switch p1, :pswitch_data_0

    .line 313
    :cond_1
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v0, "PMV"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Button Enabled : button"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " parameter check success"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 314
    const/4 v0, 0x1

    goto :goto_0

    .line 264
    :pswitch_0
    invoke-virtual {p0}, Lcom/osp/app/pushmarketing/e;->o()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/osp/app/pushmarketing/k;->a(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 266
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v1, "PMV"

    const-string v2, "Button1 Text is null"

    invoke-static {v1, v2}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 268
    :cond_2
    const-string v1, "deeplink"

    invoke-virtual {p0}, Lcom/osp/app/pushmarketing/e;->t()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_3

    const-string v1, "weblink"

    invoke-virtual {p0}, Lcom/osp/app/pushmarketing/e;->t()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_3

    .line 270
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v1, "PMV"

    const-string v2, "Button1 link type parameter is invalid"

    invoke-static {v1, v2}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 272
    :cond_3
    invoke-virtual {p0}, Lcom/osp/app/pushmarketing/e;->w()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/osp/app/pushmarketing/k;->a(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 274
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v1, "PMV"

    const-string v2, "Button1 target link is null"

    invoke-static {v1, v2}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 280
    :pswitch_1
    invoke-virtual {p0}, Lcom/osp/app/pushmarketing/e;->p()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/osp/app/pushmarketing/k;->a(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_4

    .line 282
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v1, "PMV"

    const-string v2, "Button2 Text is null"

    invoke-static {v1, v2}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 284
    :cond_4
    const-string v1, "deeplink"

    invoke-virtual {p0}, Lcom/osp/app/pushmarketing/e;->u()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_5

    const-string v1, "weblink"

    invoke-virtual {p0}, Lcom/osp/app/pushmarketing/e;->u()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_5

    .line 286
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v1, "PMV"

    const-string v2, "Button2 link type parameter is invalid"

    invoke-static {v1, v2}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 288
    :cond_5
    invoke-virtual {p0}, Lcom/osp/app/pushmarketing/e;->x()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/osp/app/pushmarketing/k;->a(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 290
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v1, "PMV"

    const-string v2, "Button2 target link is null"

    invoke-static {v1, v2}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 295
    :pswitch_2
    invoke-virtual {p0}, Lcom/osp/app/pushmarketing/e;->q()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/osp/app/pushmarketing/k;->a(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_6

    .line 297
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v1, "PMV"

    const-string v2, "Button3 Text is null"

    invoke-static {v1, v2}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 299
    :cond_6
    const-string v1, "deeplink"

    invoke-virtual {p0}, Lcom/osp/app/pushmarketing/e;->v()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_7

    const-string v1, "weblink"

    invoke-virtual {p0}, Lcom/osp/app/pushmarketing/e;->v()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_7

    .line 301
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v1, "PMV"

    const-string v2, "Button3 link type parameter is invalid"

    invoke-static {v1, v2}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 303
    :cond_7
    invoke-virtual {p0}, Lcom/osp/app/pushmarketing/e;->y()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/osp/app/pushmarketing/k;->a(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 305
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v1, "PMV"

    const-string v2, "Button3 target link is null"

    invoke-static {v1, v2}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 260
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method public static a(Ljava/lang/String;)Z
    .locals 1

    .prologue
    .line 324
    if-eqz p0, :cond_0

    invoke-virtual {p0}, Ljava/lang/String;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 326
    :cond_0
    const/4 v0, 0x1

    .line 328
    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static b(Landroid/content/Context;)Ljava/lang/String;
    .locals 3

    .prologue
    .line 599
    invoke-static {p0}, Landroid/preference/PreferenceManager;->getDefaultSharedPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v0

    .line 600
    const-string v1, "reg_id"

    const-string v2, ""

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 601
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "PMV getRegID : "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;)V

    .line 602
    return-object v0
.end method

.method public static b(Ljava/lang/String;)Ljava/lang/String;
    .locals 2

    .prologue
    .line 613
    invoke-static {p0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    const-string v0, "http://"

    invoke-virtual {p0, v0}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    const-string v0, "https://"

    invoke-virtual {p0, v0}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 615
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "http://"

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p0

    .line 618
    :cond_0
    return-object p0
.end method

.method public static c(Landroid/content/Context;)V
    .locals 3

    .prologue
    .line 628
    new-instance v0, Landroid/content/Intent;

    const-string v1, "com.osp.app.pushmarketing.PushMarketingService"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 629
    const-string v1, "push_marketing_service_mode"

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 630
    invoke-virtual {p0, v0}, Landroid/content/Context;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;

    .line 631
    return-void
.end method


# virtual methods
.method public final declared-synchronized a(Landroid/content/Context;)I
    .locals 4

    .prologue
    .line 558
    monitor-enter p0

    :try_start_0
    invoke-static {p1}, Landroid/preference/PreferenceManager;->getDefaultSharedPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v0

    .line 559
    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v1

    .line 561
    const-string v2, "notification_id"

    const/4 v3, -0x1

    invoke-interface {v0, v2, v3}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v2

    .line 563
    add-int/lit8 v0, v2, 0x1

    const/16 v3, 0x3e8

    if-le v2, v3, :cond_0

    .line 565
    const/4 v0, 0x0

    .line 568
    :cond_0
    const-string v2, "notification_id"

    invoke-interface {v1, v2, v0}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    .line 569
    invoke-interface {v1}, Landroid/content/SharedPreferences$Editor;->commit()Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 571
    monitor-exit p0

    return v0

    .line 558
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method
