.class public Lcom/osp/app/pushmarketing/PushMarketingService;
.super Lcom/osp/app/util/AbstractBaseService;
.source "PushMarketingService.java"


# instance fields
.field private final a:Ljava/lang/String;

.field private b:Lcom/sec/b/a/a;

.field private c:Z

.field private d:Ljava/lang/String;

.field private e:I

.field private final f:Ljava/lang/String;

.field private final g:Ljava/lang/String;

.field private final h:Ljava/lang/String;

.field private final i:Ljava/lang/String;

.field private final j:Ljava/lang/String;

.field private final k:Landroid/content/ServiceConnection;


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 48
    invoke-direct {p0}, Lcom/osp/app/util/AbstractBaseService;-><init>()V

    .line 53
    const-string v0, "PMS"

    iput-object v0, p0, Lcom/osp/app/pushmarketing/PushMarketingService;->a:Ljava/lang/String;

    .line 58
    iput-object v1, p0, Lcom/osp/app/pushmarketing/PushMarketingService;->b:Lcom/sec/b/a/a;

    .line 63
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/osp/app/pushmarketing/PushMarketingService;->c:Z

    .line 68
    iput-object v1, p0, Lcom/osp/app/pushmarketing/PushMarketingService;->d:Ljava/lang/String;

    .line 73
    const/4 v0, -0x1

    iput v0, p0, Lcom/osp/app/pushmarketing/PushMarketingService;->e:I

    .line 255
    const-string v0, "AUT_1302"

    iput-object v0, p0, Lcom/osp/app/pushmarketing/PushMarketingService;->f:Ljava/lang/String;

    .line 260
    const-string v0, "AUT_1094"

    iput-object v0, p0, Lcom/osp/app/pushmarketing/PushMarketingService;->g:Ljava/lang/String;

    .line 265
    const-string v0, "ACF_0403"

    iput-object v0, p0, Lcom/osp/app/pushmarketing/PushMarketingService;->h:Ljava/lang/String;

    .line 270
    const-string v0, "PMS_5001"

    iput-object v0, p0, Lcom/osp/app/pushmarketing/PushMarketingService;->i:Ljava/lang/String;

    .line 275
    const-string v0, "AUT_1830"

    iput-object v0, p0, Lcom/osp/app/pushmarketing/PushMarketingService;->j:Ljava/lang/String;

    .line 590
    new-instance v0, Lcom/osp/app/pushmarketing/h;

    invoke-direct {v0, p0}, Lcom/osp/app/pushmarketing/h;-><init>(Lcom/osp/app/pushmarketing/PushMarketingService;)V

    iput-object v0, p0, Lcom/osp/app/pushmarketing/PushMarketingService;->k:Landroid/content/ServiceConnection;

    .line 1078
    return-void
.end method

.method static synthetic a(Lcom/osp/app/pushmarketing/PushMarketingService;Lcom/sec/b/a/a;)Lcom/sec/b/a/a;
    .locals 0

    .prologue
    .line 48
    iput-object p1, p0, Lcom/osp/app/pushmarketing/PushMarketingService;->b:Lcom/sec/b/a/a;

    return-object p1
.end method

.method private a(Ljava/lang/String;)Ljava/util/List;
    .locals 7

    .prologue
    .line 668
    new-instance v0, Landroid/content/Intent;

    const-string v1, "android.intent.action.VIEW"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 670
    invoke-static {p1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setData(Landroid/net/Uri;)Landroid/content/Intent;

    .line 672
    invoke-virtual {p0}, Lcom/osp/app/pushmarketing/PushMarketingService;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v1

    const/16 v2, 0x40

    invoke-virtual {v1, v0, v2}, Landroid/content/pm/PackageManager;->queryIntentActivities(Landroid/content/Intent;I)Ljava/util/List;

    move-result-object v2

    .line 673
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v0, "PMS"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v3, "Query Intent size : "

    invoke-direct {v1, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v3

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 675
    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/pm/ResolveInfo;

    .line 677
    iget-object v1, v0, Landroid/content/pm/ResolveInfo;->filter:Landroid/content/IntentFilter;

    if-eqz v1, :cond_0

    .line 680
    const/4 v1, 0x0

    :goto_0
    iget-object v4, v0, Landroid/content/pm/ResolveInfo;->filter:Landroid/content/IntentFilter;

    invoke-virtual {v4}, Landroid/content/IntentFilter;->countDataSchemes()I

    move-result v4

    if-ge v1, v4, :cond_0

    .line 682
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v4, "PMS"

    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "Data scheme : "

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v6, v0, Landroid/content/pm/ResolveInfo;->filter:Landroid/content/IntentFilter;

    invoke-virtual {v6, v1}, Landroid/content/IntentFilter;->getDataScheme(I)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 680
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 687
    :cond_1
    return-object v2
.end method

.method private a()V
    .locals 2

    .prologue
    .line 425
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v0, "PMS"

    const-string v1, "startService"

    invoke-static {v0, v1}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 426
    new-instance v0, Landroid/content/Intent;

    const-string v1, "com.sec.spp.push.PUSH_CLIENT_SERVICE_ACTION"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0, v0}, Lcom/osp/app/pushmarketing/PushMarketingService;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;

    .line 427
    return-void
.end method

.method static synthetic a(Lcom/osp/app/pushmarketing/PushMarketingService;)Z
    .locals 1

    .prologue
    .line 48
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/osp/app/pushmarketing/PushMarketingService;->c:Z

    return v0
.end method

.method static synthetic b(Lcom/osp/app/pushmarketing/PushMarketingService;)Lcom/sec/b/a/a;
    .locals 1

    .prologue
    .line 48
    iget-object v0, p0, Lcom/osp/app/pushmarketing/PushMarketingService;->b:Lcom/sec/b/a/a;

    return-object v0
.end method

.method static synthetic c(Lcom/osp/app/pushmarketing/PushMarketingService;)I
    .locals 1

    .prologue
    .line 48
    iget v0, p0, Lcom/osp/app/pushmarketing/PushMarketingService;->e:I

    return v0
.end method

.method static synthetic d(Lcom/osp/app/pushmarketing/PushMarketingService;)V
    .locals 0

    .prologue
    .line 48
    invoke-direct {p0}, Lcom/osp/app/pushmarketing/PushMarketingService;->h()V

    return-void
.end method

.method private e()V
    .locals 3

    .prologue
    .line 435
    iget-boolean v0, p0, Lcom/osp/app/pushmarketing/PushMarketingService;->c:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/osp/app/pushmarketing/PushMarketingService;->b:Lcom/sec/b/a/a;

    if-eqz v0, :cond_0

    .line 437
    iget v0, p0, Lcom/osp/app/pushmarketing/PushMarketingService;->e:I

    sparse-switch v0, :sswitch_data_0

    .line 462
    :goto_0
    return-void

    .line 440
    :sswitch_0
    invoke-direct {p0}, Lcom/osp/app/pushmarketing/PushMarketingService;->h()V

    goto :goto_0

    .line 443
    :sswitch_1
    invoke-direct {p0}, Lcom/osp/app/pushmarketing/PushMarketingService;->i()V

    goto :goto_0

    .line 446
    :sswitch_2
    invoke-direct {p0}, Lcom/osp/app/pushmarketing/PushMarketingService;->g()V

    goto :goto_0

    .line 449
    :sswitch_3
    invoke-direct {p0}, Lcom/osp/app/pushmarketing/PushMarketingService;->f()V

    goto :goto_0

    .line 458
    :cond_0
    new-instance v0, Landroid/content/Intent;

    const-string v1, "com.sec.spp.push.PUSH_CLIENT_SERVICE_ACTION"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 459
    iget-object v1, p0, Lcom/osp/app/pushmarketing/PushMarketingService;->k:Landroid/content/ServiceConnection;

    const/4 v2, 0x1

    invoke-virtual {p0, v0, v1, v2}, Lcom/osp/app/pushmarketing/PushMarketingService;->bindService(Landroid/content/Intent;Landroid/content/ServiceConnection;I)Z

    move-result v0

    iput-boolean v0, p0, Lcom/osp/app/pushmarketing/PushMarketingService;->c:Z

    goto :goto_0

    .line 437
    nop

    :sswitch_data_0
    .sparse-switch
        0x1 -> :sswitch_0
        0x2 -> :sswitch_3
        0x3 -> :sswitch_2
        0x63 -> :sswitch_1
    .end sparse-switch
.end method

.method static synthetic e(Lcom/osp/app/pushmarketing/PushMarketingService;)V
    .locals 0

    .prologue
    .line 48
    invoke-direct {p0}, Lcom/osp/app/pushmarketing/PushMarketingService;->i()V

    return-void
.end method

.method private f()V
    .locals 4

    .prologue
    .line 469
    sget-object v0, Lcom/osp/app/pushmarketing/l;->a:Lcom/osp/app/pushmarketing/k;

    sget-object v0, Lcom/osp/app/pushmarketing/l;->a:Lcom/osp/app/pushmarketing/k;

    invoke-static {p0}, Lcom/osp/app/pushmarketing/k;->b(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/osp/app/pushmarketing/k;->a(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 471
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v0, "PMS"

    const-string v1, "Duplicate Request to Registration-PMS"

    invoke-static {v0, v1}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 502
    :goto_0
    return-void

    .line 475
    :cond_0
    const/4 v0, 0x0

    .line 477
    iget-object v1, p0, Lcom/osp/app/pushmarketing/PushMarketingService;->b:Lcom/sec/b/a/a;

    if-eqz v1, :cond_2

    .line 481
    :try_start_0
    iget-object v1, p0, Lcom/osp/app/pushmarketing/PushMarketingService;->b:Lcom/sec/b/a/a;

    const-string v2, "af06dccdddc4c3b0"

    invoke-interface {v1, v2}, Lcom/sec/b/a/a;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 482
    sget-object v1, Lcom/osp/app/pushmarketing/l;->a:Lcom/osp/app/pushmarketing/k;

    invoke-static {p0, v0}, Lcom/osp/app/pushmarketing/k;->a(Landroid/content/Context;Ljava/lang/String;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 493
    :goto_1
    sget-object v1, Lcom/osp/app/pushmarketing/l;->a:Lcom/osp/app/pushmarketing/k;

    invoke-static {v0}, Lcom/osp/app/pushmarketing/k;->a(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_1

    .line 495
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "PMS regId : "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;)V

    .line 497
    new-instance v0, Lcom/osp/app/pushmarketing/j;

    invoke-direct {v0, p0}, Lcom/osp/app/pushmarketing/j;-><init>(Lcom/osp/app/pushmarketing/PushMarketingService;)V

    .line 498
    invoke-virtual {v0}, Lcom/osp/app/pushmarketing/j;->b()V

    .line 501
    :cond_1
    invoke-virtual {p0}, Lcom/osp/app/pushmarketing/PushMarketingService;->stopSelf()V

    goto :goto_0

    .line 483
    :catch_0
    move-exception v1

    .line 485
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v2, "PMS"

    const-string v3, "Get regID is failed"

    invoke-static {v2, v3}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 486
    invoke-virtual {v1}, Landroid/os/RemoteException;->printStackTrace()V

    goto :goto_1

    .line 490
    :cond_2
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v1, "PMS"

    const-string v2, "Service is not connected"

    invoke-static {v1, v2}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1
.end method

.method static synthetic f(Lcom/osp/app/pushmarketing/PushMarketingService;)V
    .locals 0

    .prologue
    .line 48
    invoke-direct {p0}, Lcom/osp/app/pushmarketing/PushMarketingService;->g()V

    return-void
.end method

.method private g()V
    .locals 2

    .prologue
    .line 508
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v1, p0, Lcom/osp/app/pushmarketing/PushMarketingService;->d:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ";af06dccdddc4c3b0"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 511
    :try_start_0
    iget-object v1, p0, Lcom/osp/app/pushmarketing/PushMarketingService;->b:Lcom/sec/b/a/a;

    invoke-interface {v1, v0}, Lcom/sec/b/a/a;->c(Ljava/lang/String;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 517
    :goto_0
    invoke-virtual {p0}, Lcom/osp/app/pushmarketing/PushMarketingService;->stopSelf()V

    .line 518
    return-void

    .line 512
    :catch_0
    move-exception v0

    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    goto :goto_0
.end method

.method static synthetic g(Lcom/osp/app/pushmarketing/PushMarketingService;)V
    .locals 0

    .prologue
    .line 48
    invoke-direct {p0}, Lcom/osp/app/pushmarketing/PushMarketingService;->f()V

    return-void
.end method

.method private h()V
    .locals 4

    .prologue
    .line 525
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v0, "PMS"

    const-string v1, "register SPP"

    invoke-static {v0, v1}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 526
    sget-object v0, Lcom/osp/app/pushmarketing/l;->a:Lcom/osp/app/pushmarketing/k;

    invoke-static {p0}, Lcom/osp/app/pushmarketing/k;->b(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v2

    .line 527
    const-string v0, ""

    .line 530
    :try_start_0
    iget-object v1, p0, Lcom/osp/app/pushmarketing/PushMarketingService;->b:Lcom/sec/b/a/a;

    const-string v3, "af06dccdddc4c3b0"

    invoke-interface {v1, v3}, Lcom/sec/b/a/a;->b(Ljava/lang/String;)Ljava/lang/String;
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 536
    :goto_0
    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_0

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 538
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v0, "PMS"

    const-string v1, "Already registrated to SPP"

    invoke-static {v0, v1}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 554
    :goto_1
    return-void

    .line 531
    :catch_0
    move-exception v1

    invoke-virtual {v1}, Landroid/os/RemoteException;->printStackTrace()V

    goto :goto_0

    .line 543
    :cond_0
    sget-object v0, Lcom/osp/app/pushmarketing/l;->a:Lcom/osp/app/pushmarketing/k;

    const-string v0, ""

    invoke-static {p0, v0}, Lcom/osp/app/pushmarketing/k;->a(Landroid/content/Context;Ljava/lang/String;)V

    .line 547
    :try_start_1
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "PMS getPackageName : "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0}, Lcom/osp/app/pushmarketing/PushMarketingService;->getPackageName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;)V

    .line 548
    iget-object v0, p0, Lcom/osp/app/pushmarketing/PushMarketingService;->b:Lcom/sec/b/a/a;

    const-string v1, "af06dccdddc4c3b0"

    invoke-virtual {p0}, Lcom/osp/app/pushmarketing/PushMarketingService;->getPackageName()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Lcom/sec/b/a/a;->a(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_1
    .catch Landroid/os/RemoteException; {:try_start_1 .. :try_end_1} :catch_1

    goto :goto_1

    .line 549
    :catch_1
    move-exception v0

    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    goto :goto_1
.end method

.method static synthetic h(Lcom/osp/app/pushmarketing/PushMarketingService;)V
    .locals 7

    .prologue
    .line 48
    invoke-static {p0}, Lcom/osp/app/util/ad;->d(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_0

    new-instance v0, Landroid/content/Intent;

    const-string v1, "com.msc.action.samsungaccount.resignin"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    const/high16 v1, 0x10000000

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    const-string v1, "OSP_VER"

    const-string v2, "OSP_02"

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    const-string v1, "email_id"

    invoke-static {p0}, Lcom/osp/app/util/ad;->h(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    const-string v1, "from_notification"

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    const/4 v1, 0x0

    const/high16 v2, 0x8000000

    invoke-static {p0, v1, v0, v2}, Landroid/app/PendingIntent;->getActivity(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v1

    const/4 v2, 0x0

    const v0, 0x7f09004f

    invoke-virtual {p0, v0}, Lcom/osp/app/pushmarketing/PushMarketingService;->getString(I)Ljava/lang/String;

    move-result-object v3

    const v0, 0x7f09008d

    invoke-virtual {p0, v0}, Lcom/osp/app/pushmarketing/PushMarketingService;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-static {}, Lcom/osp/app/signin/SamsungService;->i()I

    move-result v5

    const v6, 0x132df82

    move-object v0, p0

    invoke-static/range {v0 .. v6}, Lcom/msc/sa/c/d;->a(Landroid/content/Context;Landroid/app/PendingIntent;Landroid/app/PendingIntent;Ljava/lang/String;Ljava/lang/String;II)V

    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v0, "PMS"

    const-string v1, "Show resign-in Notification"

    invoke-static {v0, v1}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;)V

    :cond_0
    return-void
.end method

.method private i()V
    .locals 2

    .prologue
    .line 560
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v0, "PMS"

    const-string v1, "deregister SPP"

    invoke-static {v0, v1}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 562
    sget-object v0, Lcom/osp/app/pushmarketing/l;->a:Lcom/osp/app/pushmarketing/k;

    const-string v0, ""

    invoke-static {p0, v0}, Lcom/osp/app/pushmarketing/k;->a(Landroid/content/Context;Ljava/lang/String;)V

    .line 566
    :try_start_0
    iget-object v0, p0, Lcom/osp/app/pushmarketing/PushMarketingService;->b:Lcom/sec/b/a/a;

    const-string v1, "af06dccdddc4c3b0"

    invoke-interface {v0, v1}, Lcom/sec/b/a/a;->a(Ljava/lang/String;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 572
    :goto_0
    invoke-virtual {p0}, Lcom/osp/app/pushmarketing/PushMarketingService;->stopSelf()V

    .line 573
    return-void

    .line 567
    :catch_0
    move-exception v0

    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    goto :goto_0
.end method

.method static synthetic i(Lcom/osp/app/pushmarketing/PushMarketingService;)V
    .locals 7

    .prologue
    .line 48
    invoke-static {p0}, Lcom/osp/app/util/ad;->d(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_0

    new-instance v0, Landroid/content/Intent;

    const-string v1, "com.msc.action.samsungaccount.resignin_with_signout"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    const/high16 v1, 0x10000000

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    const-string v1, "OSP_VER"

    const-string v2, "OSP_02"

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    const-string v1, "from_notification"

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    const/4 v1, 0x0

    const/high16 v2, 0x8000000

    invoke-static {p0, v1, v0, v2}, Landroid/app/PendingIntent;->getActivity(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v1

    const/4 v2, 0x0

    const v0, 0x7f09004f

    invoke-virtual {p0, v0}, Lcom/osp/app/pushmarketing/PushMarketingService;->getString(I)Ljava/lang/String;

    move-result-object v3

    const v0, 0x7f09008d

    invoke-virtual {p0, v0}, Lcom/osp/app/pushmarketing/PushMarketingService;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-static {}, Lcom/osp/app/signin/SamsungService;->i()I

    move-result v5

    const v6, 0x132df82

    move-object v0, p0

    invoke-static/range {v0 .. v6}, Lcom/msc/sa/c/d;->a(Landroid/content/Context;Landroid/app/PendingIntent;Landroid/app/PendingIntent;Ljava/lang/String;Ljava/lang/String;II)V

    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v0, "PMS"

    const-string v1, "Show resign-in with sign out Notification"

    invoke-static {v0, v1}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;)V

    :cond_0
    return-void
.end method


# virtual methods
.method public onBind(Landroid/content/Intent;)Landroid/os/IBinder;
    .locals 1

    .prologue
    .line 290
    const/4 v0, 0x0

    return-object v0
.end method

.method public onDestroy()V
    .locals 2

    .prologue
    .line 405
    iget-boolean v0, p0, Lcom/osp/app/pushmarketing/PushMarketingService;->c:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/osp/app/pushmarketing/PushMarketingService;->k:Landroid/content/ServiceConnection;

    invoke-virtual {p0, v0}, Lcom/osp/app/pushmarketing/PushMarketingService;->unbindService(Landroid/content/ServiceConnection;)V

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/osp/app/pushmarketing/PushMarketingService;->c:Z

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/osp/app/pushmarketing/PushMarketingService;->b:Lcom/sec/b/a/a;

    .line 408
    :cond_0
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v0, "PMS"

    const-string v1, "onDestroy"

    invoke-static {v0, v1}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 418
    invoke-super {p0}, Lcom/osp/app/util/AbstractBaseService;->onDestroy()V

    .line 419
    return-void
.end method

.method public onStartCommand(Landroid/content/Intent;II)I
    .locals 5

    .prologue
    const/4 v3, 0x0

    const/4 v2, -0x1

    .line 304
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x10

    if-ge v0, v1, :cond_0

    .line 306
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v0, "PMS"

    const-string v1, "VERSION IS UNDER JELLY_BEAN"

    invoke-static {v0, v1}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 311
    :cond_0
    iput v2, p0, Lcom/osp/app/pushmarketing/PushMarketingService;->e:I

    .line 313
    if-eqz p1, :cond_1

    .line 315
    const-string v0, "push_marketing_service_mode"

    invoke-virtual {p1, v0, v2}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v0

    iput v0, p0, Lcom/osp/app/pushmarketing/PushMarketingService;->e:I

    .line 318
    :cond_1
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v0, "PMS"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "PMS Service Start : "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v2, p0, Lcom/osp/app/pushmarketing/PushMarketingService;->e:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 320
    iget v0, p0, Lcom/osp/app/pushmarketing/PushMarketingService;->e:I

    sparse-switch v0, :sswitch_data_0

    .line 387
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v0, "PMS"

    const-string v1, "SERVICE MODE is Invalid"

    invoke-static {v0, v1}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 388
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v0, "SERVICE MODE is Invalid"

    invoke-static {v0}, Lcom/osp/app/util/aq;->b(Ljava/lang/String;)V

    .line 389
    invoke-virtual {p0}, Lcom/osp/app/pushmarketing/PushMarketingService;->stopSelf()V

    .line 393
    :cond_2
    :goto_0
    const/4 v0, 0x2

    return v0

    .line 323
    :sswitch_0
    invoke-direct {p0}, Lcom/osp/app/pushmarketing/PushMarketingService;->a()V

    goto :goto_0

    .line 326
    :sswitch_1
    invoke-direct {p0}, Lcom/osp/app/pushmarketing/PushMarketingService;->a()V

    .line 329
    invoke-static {p0}, Lcom/osp/app/util/ad;->d(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 331
    const/4 v0, 0x1

    iput v0, p0, Lcom/osp/app/pushmarketing/PushMarketingService;->e:I

    .line 332
    invoke-direct {p0}, Lcom/osp/app/pushmarketing/PushMarketingService;->e()V

    goto :goto_0

    .line 337
    :sswitch_2
    invoke-direct {p0}, Lcom/osp/app/pushmarketing/PushMarketingService;->e()V

    goto :goto_0

    .line 341
    :sswitch_3
    const-string v0, "ack"

    invoke-virtual {p1, v0, v3}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v1

    .line 342
    const-string v0, "msg"

    invoke-virtual {p1, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 343
    const-string v2, "notificationId"

    invoke-virtual {p1, v2}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/osp/app/pushmarketing/PushMarketingService;->d:Ljava/lang/String;

    .line 344
    sget-object v2, Lcom/osp/app/pushmarketing/l;->a:Lcom/osp/app/pushmarketing/k;

    invoke-static {p0}, Lcom/osp/app/pushmarketing/k;->b(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v2

    sget-object v3, Lcom/osp/app/pushmarketing/l;->a:Lcom/osp/app/pushmarketing/k;

    invoke-static {v2}, Lcom/osp/app/pushmarketing/k;->a(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_3

    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v0, "PMS"

    const-string v2, "regID is null"

    invoke-static {v0, v2}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 345
    :goto_1
    if-eqz v1, :cond_4

    .line 347
    invoke-direct {p0}, Lcom/osp/app/pushmarketing/PushMarketingService;->e()V

    goto :goto_0

    .line 344
    :cond_3
    :try_start_0
    new-instance v3, Lorg/json/JSONObject;

    invoke-direct {v3, v0}, Lorg/json/JSONObject;-><init>(Ljava/lang/String;)V

    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v4, "PMSCONTENT : "

    invoke-direct {v0, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;)V

    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v4, "PMSJSONOBJ : "

    invoke-direct {v0, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;)V

    new-instance v0, Lcom/osp/app/pushmarketing/e;

    invoke-direct {v0}, Lcom/osp/app/pushmarketing/e;-><init>()V

    invoke-virtual {v0, v3}, Lcom/osp/app/pushmarketing/e;->a(Lorg/json/JSONObject;)V

    new-instance v3, Lcom/osp/app/pushmarketing/a;

    invoke-direct {v3}, Lcom/osp/app/pushmarketing/a;-><init>()V

    iget-object v4, p0, Lcom/osp/app/pushmarketing/PushMarketingService;->d:Ljava/lang/String;

    invoke-virtual {v3, p0, v4, v0, v2}, Lcom/osp/app/pushmarketing/a;->a(Landroid/content/Context;Ljava/lang/String;Lcom/osp/app/pushmarketing/e;Ljava/lang/String;)V
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_1

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Lorg/json/JSONException;->printStackTrace()V

    goto :goto_1

    .line 350
    :cond_4
    invoke-virtual {p0}, Lcom/osp/app/pushmarketing/PushMarketingService;->stopSelf()V

    goto/16 :goto_0

    .line 355
    :sswitch_4
    invoke-direct {p0}, Lcom/osp/app/pushmarketing/PushMarketingService;->e()V

    goto/16 :goto_0

    .line 361
    :sswitch_5
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "Notification ID : "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v1, "notification_id"

    invoke-virtual {p1, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;)V

    .line 362
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "PREF Notification ID : "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v1, "pref_notification_id"

    const/16 v2, -0x63

    invoke-virtual {p1, v1, v2}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;)V

    .line 363
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "Link Type : "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v1, "link_type"

    invoke-virtual {p1, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;)V

    .line 365
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "Target Link : "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v1, "target_link"

    invoke-virtual {p1, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;)V

    .line 366
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "Error Link : "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v1, "error_link"

    invoke-virtual {p1, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;)V

    .line 367
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "Campaign ID : "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v1, "campaign_id"

    invoke-virtual {p1, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;)V

    .line 368
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "regId : "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v1, "reg_id"

    invoke-virtual {p1, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;)V

    .line 370
    :try_start_1
    const-string v0, "pref_notification_id"

    const/4 v1, -0x1

    invoke-virtual {p1, v0, v1}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v1

    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v2, "PMS pref NOTI ID : "

    invoke-direct {v0, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;)V

    const-string v0, "notification"

    invoke-virtual {p0, v0}, Lcom/osp/app/pushmarketing/PushMarketingService;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/NotificationManager;

    invoke-virtual {v0, v1}, Landroid/app/NotificationManager;->cancel(I)V

    const-string v0, "collapsePanels"

    invoke-static {}, Lcom/osp/device/b;->b()Lcom/osp/device/b;

    move-result-object v1

    invoke-virtual {v1}, Lcom/osp/device/b;->l()I

    move-result v1

    const/16 v2, 0x11

    if-ge v1, v2, :cond_5

    const-string v0, "collapse"

    :cond_5
    const-string v1, "statusbar"

    invoke-virtual {p0, v1}, Lcom/osp/app/pushmarketing/PushMarketingService;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_2

    move-result-object v1

    :try_start_2
    const-string v2, "android.app.StatusBarManager"

    invoke-static {v2}, Ljava/lang/Class;->forName(Ljava/lang/String;)Ljava/lang/Class;

    move-result-object v2

    const/4 v3, 0x0

    new-array v3, v3, [Ljava/lang/Class;

    invoke-virtual {v2, v0, v3}, Ljava/lang/Class;->getMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v0

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Object;

    invoke-virtual {v0, v1, v2}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_1

    :goto_2
    :try_start_3
    new-instance v1, Landroid/content/Intent;

    const-string v0, "android.intent.action.VIEW"

    invoke-direct {v1, v0}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    const-string v0, "deeplink"

    const-string v2, "link_type"

    invoke-virtual {p1, v2}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_8

    const-string v0, "target_link"

    invoke-virtual {p1, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    const-string v0, "error_link"

    invoke-virtual {p1, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    sget-object v3, Lcom/osp/app/pushmarketing/l;->a:Lcom/osp/app/pushmarketing/k;

    invoke-static {v0}, Lcom/osp/app/pushmarketing/k;->a(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_6

    invoke-static {}, Lcom/msc/c/k;->b()Ljava/lang/String;

    move-result-object v0

    :cond_6
    invoke-direct {p0, v2}, Lcom/osp/app/pushmarketing/PushMarketingService;->a(Ljava/lang/String;)Ljava/util/List;

    move-result-object v3

    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v3

    if-lez v3, :cond_7

    invoke-static {v2}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    invoke-virtual {v1, v0}, Landroid/content/Intent;->setData(Landroid/net/Uri;)Landroid/content/Intent;

    :goto_3
    const v0, 0x14000020

    invoke-virtual {v1, v0}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    invoke-virtual {p0, v1}, Lcom/osp/app/pushmarketing/PushMarketingService;->startActivity(Landroid/content/Intent;)V
    :try_end_3
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_2

    .line 377
    :goto_4
    new-instance v0, Lcom/osp/app/pushmarketing/i;

    invoke-direct {v0, p0, p1}, Lcom/osp/app/pushmarketing/i;-><init>(Lcom/osp/app/pushmarketing/PushMarketingService;Landroid/content/Intent;)V

    .line 378
    invoke-virtual {v0}, Lcom/osp/app/pushmarketing/i;->b()V

    goto/16 :goto_0

    .line 370
    :catch_1
    move-exception v0

    :try_start_4
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V
    :try_end_4
    .catch Ljava/lang/Exception; {:try_start_4 .. :try_end_4} :catch_2

    goto :goto_2

    :catch_2
    move-exception v0

    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_4

    :cond_7
    :try_start_5
    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    invoke-virtual {v1, v0}, Landroid/content/Intent;->setData(Landroid/net/Uri;)Landroid/content/Intent;

    goto :goto_3

    :cond_8
    const-string v0, "weblink"

    const-string v2, "link_type"

    invoke-virtual {p1, v2}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_9

    const-string v0, "target_link"

    invoke-virtual {p1, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    invoke-virtual {v1, v0}, Landroid/content/Intent;->setData(Landroid/net/Uri;)Landroid/content/Intent;

    goto :goto_3

    :cond_9
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v0, "PMS"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Invalid Link Type : "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v3, "link_type"

    invoke-virtual {p1, v3}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_5
    .catch Ljava/lang/Exception; {:try_start_5 .. :try_end_5} :catch_2

    goto :goto_3

    .line 384
    :sswitch_6
    invoke-virtual {p0}, Lcom/osp/app/pushmarketing/PushMarketingService;->stopSelf()V

    goto/16 :goto_0

    .line 320
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x1 -> :sswitch_1
        0x2 -> :sswitch_4
        0x3 -> :sswitch_3
        0x4 -> :sswitch_5
        0x63 -> :sswitch_2
        0x64 -> :sswitch_6
    .end sparse-switch
.end method
