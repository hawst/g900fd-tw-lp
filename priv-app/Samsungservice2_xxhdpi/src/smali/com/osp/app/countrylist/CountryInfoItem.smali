.class public Lcom/osp/app/countrylist/CountryInfoItem;
.super Ljava/lang/Object;
.source "CountryInfoItem.java"

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;


# instance fields
.field private a:Ljava/lang/String;

.field private b:Ljava/lang/String;

.field private c:Ljava/lang/String;

.field private d:Ljava/lang/String;

.field private e:Ljava/lang/String;

.field private f:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 18
    new-instance v0, Lcom/osp/app/countrylist/a;

    invoke-direct {v0}, Lcom/osp/app/countrylist/a;-><init>()V

    sput-object v0, Lcom/osp/app/countrylist/CountryInfoItem;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 71
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 72
    const-string v0, ""

    iput-object v0, p0, Lcom/osp/app/countrylist/CountryInfoItem;->b:Ljava/lang/String;

    .line 73
    const-string v0, ""

    iput-object v0, p0, Lcom/osp/app/countrylist/CountryInfoItem;->d:Ljava/lang/String;

    .line 74
    const-string v0, ""

    iput-object v0, p0, Lcom/osp/app/countrylist/CountryInfoItem;->a:Ljava/lang/String;

    .line 75
    const-string v0, ""

    iput-object v0, p0, Lcom/osp/app/countrylist/CountryInfoItem;->c:Ljava/lang/String;

    .line 76
    const-string v0, ""

    iput-object v0, p0, Lcom/osp/app/countrylist/CountryInfoItem;->e:Ljava/lang/String;

    .line 77
    const-string v0, ""

    iput-object v0, p0, Lcom/osp/app/countrylist/CountryInfoItem;->f:Ljava/lang/String;

    .line 78
    return-void
.end method

.method static synthetic a(Lcom/osp/app/countrylist/CountryInfoItem;Ljava/lang/String;)Ljava/lang/String;
    .locals 0

    .prologue
    .line 11
    iput-object p1, p0, Lcom/osp/app/countrylist/CountryInfoItem;->b:Ljava/lang/String;

    return-object p1
.end method

.method static synthetic b(Lcom/osp/app/countrylist/CountryInfoItem;Ljava/lang/String;)Ljava/lang/String;
    .locals 0

    .prologue
    .line 11
    iput-object p1, p0, Lcom/osp/app/countrylist/CountryInfoItem;->d:Ljava/lang/String;

    return-object p1
.end method

.method static synthetic c(Lcom/osp/app/countrylist/CountryInfoItem;Ljava/lang/String;)Ljava/lang/String;
    .locals 0

    .prologue
    .line 11
    iput-object p1, p0, Lcom/osp/app/countrylist/CountryInfoItem;->a:Ljava/lang/String;

    return-object p1
.end method

.method static synthetic d(Lcom/osp/app/countrylist/CountryInfoItem;Ljava/lang/String;)Ljava/lang/String;
    .locals 0

    .prologue
    .line 11
    iput-object p1, p0, Lcom/osp/app/countrylist/CountryInfoItem;->c:Ljava/lang/String;

    return-object p1
.end method


# virtual methods
.method public final a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 91
    iget-object v0, p0, Lcom/osp/app/countrylist/CountryInfoItem;->a:Ljava/lang/String;

    return-object v0
.end method

.method public final a(Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 127
    iput-object p1, p0, Lcom/osp/app/countrylist/CountryInfoItem;->e:Ljava/lang/String;

    .line 128
    return-void
.end method

.method public final b()Ljava/lang/String;
    .locals 1

    .prologue
    .line 100
    iget-object v0, p0, Lcom/osp/app/countrylist/CountryInfoItem;->b:Ljava/lang/String;

    return-object v0
.end method

.method public final b(Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 136
    iput-object p1, p0, Lcom/osp/app/countrylist/CountryInfoItem;->a:Ljava/lang/String;

    .line 137
    return-void
.end method

.method public final c()Ljava/lang/String;
    .locals 1

    .prologue
    .line 109
    iget-object v0, p0, Lcom/osp/app/countrylist/CountryInfoItem;->c:Ljava/lang/String;

    return-object v0
.end method

.method public final c(Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 145
    iput-object p1, p0, Lcom/osp/app/countrylist/CountryInfoItem;->b:Ljava/lang/String;

    .line 146
    return-void
.end method

.method public final d()Ljava/lang/String;
    .locals 1

    .prologue
    .line 118
    iget-object v0, p0, Lcom/osp/app/countrylist/CountryInfoItem;->d:Ljava/lang/String;

    return-object v0
.end method

.method public final d(Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 154
    iput-object p1, p0, Lcom/osp/app/countrylist/CountryInfoItem;->c:Ljava/lang/String;

    .line 155
    return-void
.end method

.method public describeContents()I
    .locals 1

    .prologue
    .line 82
    const/4 v0, 0x0

    return v0
.end method

.method public final e()Ljava/lang/String;
    .locals 1

    .prologue
    .line 176
    iget-object v0, p0, Lcom/osp/app/countrylist/CountryInfoItem;->e:Ljava/lang/String;

    return-object v0
.end method

.method public final e(Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 163
    iput-object p1, p0, Lcom/osp/app/countrylist/CountryInfoItem;->d:Ljava/lang/String;

    .line 164
    return-void
.end method

.method public final f()Ljava/lang/String;
    .locals 1

    .prologue
    .line 194
    iget-object v0, p0, Lcom/osp/app/countrylist/CountryInfoItem;->f:Ljava/lang/String;

    return-object v0
.end method

.method public final f(Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 167
    iput-object p1, p0, Lcom/osp/app/countrylist/CountryInfoItem;->f:Ljava/lang/String;

    .line 168
    return-void
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 1

    .prologue
    .line 181
    iget-object v0, p0, Lcom/osp/app/countrylist/CountryInfoItem;->b:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 182
    iget-object v0, p0, Lcom/osp/app/countrylist/CountryInfoItem;->d:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 183
    iget-object v0, p0, Lcom/osp/app/countrylist/CountryInfoItem;->a:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 184
    iget-object v0, p0, Lcom/osp/app/countrylist/CountryInfoItem;->c:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 185
    iget-object v0, p0, Lcom/osp/app/countrylist/CountryInfoItem;->e:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 186
    return-void
.end method
