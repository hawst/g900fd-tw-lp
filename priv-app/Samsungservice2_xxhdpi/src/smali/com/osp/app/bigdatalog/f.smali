.class public final Lcom/osp/app/bigdatalog/f;
.super Landroid/os/Handler;
.source "BigDataLogManager.java"


# instance fields
.field final synthetic a:Lcom/osp/app/bigdatalog/e;


# direct methods
.method public constructor <init>(Lcom/osp/app/bigdatalog/e;)V
    .locals 0

    .prologue
    .line 317
    iput-object p1, p0, Lcom/osp/app/bigdatalog/f;->a:Lcom/osp/app/bigdatalog/e;

    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    return-void
.end method


# virtual methods
.method public final handleMessage(Landroid/os/Message;)V
    .locals 7

    .prologue
    const/4 v6, -0x6

    const/4 v2, 0x1

    .line 321
    iget v0, p1, Landroid/os/Message;->what:I

    packed-switch v0, :pswitch_data_0

    .line 371
    :cond_0
    :goto_0
    return-void

    .line 326
    :pswitch_0
    const/4 v0, 0x0

    .line 328
    iget-object v1, p0, Lcom/osp/app/bigdatalog/f;->a:Lcom/osp/app/bigdatalog/e;

    invoke-static {v1}, Lcom/osp/app/bigdatalog/e;->a(Lcom/osp/app/bigdatalog/e;)Ljava/util/Queue;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/Queue;->size()I

    move-result v1

    if-lez v1, :cond_3

    .line 330
    iget-object v1, p0, Lcom/osp/app/bigdatalog/f;->a:Lcom/osp/app/bigdatalog/e;

    invoke-static {v1}, Lcom/osp/app/bigdatalog/e;->a(Lcom/osp/app/bigdatalog/e;)Ljava/util/Queue;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/Queue;->iterator()Ljava/util/Iterator;

    move-result-object v3

    move v1, v0

    :goto_1
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/osp/app/bigdatalog/c;

    .line 332
    iget-object v4, p0, Lcom/osp/app/bigdatalog/f;->a:Lcom/osp/app/bigdatalog/e;

    invoke-static {v4, v0}, Lcom/osp/app/bigdatalog/e;->a(Lcom/osp/app/bigdatalog/e;Lcom/osp/app/bigdatalog/c;)Lcom/osp/app/bigdatalog/c;

    .line 333
    iget-object v0, p0, Lcom/osp/app/bigdatalog/f;->a:Lcom/osp/app/bigdatalog/e;

    invoke-static {v0}, Lcom/osp/app/bigdatalog/e;->b(Lcom/osp/app/bigdatalog/e;)I

    move-result v0

    .line 334
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "sendLog result code : "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " result msg : "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-static {v0}, Lcom/osp/app/bigdatalog/d;->a(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;)V

    .line 335
    if-ne v0, v6, :cond_5

    move v0, v2

    :goto_2
    move v1, v0

    .line 339
    goto :goto_1

    .line 340
    :cond_1
    iget-object v0, p0, Lcom/osp/app/bigdatalog/f;->a:Lcom/osp/app/bigdatalog/e;

    invoke-static {v0}, Lcom/osp/app/bigdatalog/e;->a(Lcom/osp/app/bigdatalog/e;)Ljava/util/Queue;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Queue;->clear()V

    .line 352
    :goto_3
    if-ne v1, v2, :cond_2

    .line 354
    invoke-static {}, Lcom/osp/app/bigdatalog/g;->a()Lcom/osp/app/bigdatalog/e;

    move-result-object v1

    iget-object v0, p0, Lcom/osp/app/bigdatalog/f;->a:Lcom/osp/app/bigdatalog/e;

    invoke-static {v0}, Lcom/osp/app/bigdatalog/e;->c(Lcom/osp/app/bigdatalog/e;)Ljava/lang/ref/WeakReference;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/Context;

    invoke-virtual {v1, v0}, Lcom/osp/app/bigdatalog/e;->a(Landroid/content/Context;)V

    .line 356
    invoke-static {}, Lcom/osp/app/bigdatalog/g;->a()Lcom/osp/app/bigdatalog/e;

    move-result-object v0

    invoke-virtual {v0}, Lcom/osp/app/bigdatalog/e;->c()Z

    move-result v0

    if-nez v0, :cond_2

    .line 358
    invoke-static {}, Lcom/osp/app/bigdatalog/g;->a()Lcom/osp/app/bigdatalog/e;

    move-result-object v1

    iget-object v0, p0, Lcom/osp/app/bigdatalog/f;->a:Lcom/osp/app/bigdatalog/e;

    invoke-static {v0}, Lcom/osp/app/bigdatalog/e;->c(Lcom/osp/app/bigdatalog/e;)Ljava/lang/ref/WeakReference;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/Context;

    invoke-virtual {v1, v0}, Lcom/osp/app/bigdatalog/e;->b(Landroid/content/Context;)V

    .line 362
    :cond_2
    iget-object v0, p0, Lcom/osp/app/bigdatalog/f;->a:Lcom/osp/app/bigdatalog/e;

    invoke-static {v0}, Lcom/osp/app/bigdatalog/e;->c(Lcom/osp/app/bigdatalog/e;)Ljava/lang/ref/WeakReference;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 364
    iget-object v0, p0, Lcom/osp/app/bigdatalog/f;->a:Lcom/osp/app/bigdatalog/e;

    invoke-static {v0}, Lcom/osp/app/bigdatalog/e;->d(Lcom/osp/app/bigdatalog/e;)Lcom/osp/app/bigdatalog/a;

    move-result-object v1

    iget-object v0, p0, Lcom/osp/app/bigdatalog/f;->a:Lcom/osp/app/bigdatalog/e;

    invoke-static {v0}, Lcom/osp/app/bigdatalog/e;->c(Lcom/osp/app/bigdatalog/e;)Ljava/lang/ref/WeakReference;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/Context;

    invoke-virtual {v1, v0}, Lcom/osp/app/bigdatalog/a;->a(Landroid/content/Context;)V

    goto/16 :goto_0

    .line 344
    :cond_3
    iget-object v1, p0, Lcom/osp/app/bigdatalog/f;->a:Lcom/osp/app/bigdatalog/e;

    invoke-static {v1}, Lcom/osp/app/bigdatalog/e;->b(Lcom/osp/app/bigdatalog/e;)I

    move-result v1

    .line 345
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, " sendLog result code : "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " result msg : "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-static {v1}, Lcom/osp/app/bigdatalog/d;->a(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;)V

    .line 346
    if-ne v1, v6, :cond_4

    move v1, v2

    .line 348
    goto/16 :goto_3

    :cond_4
    move v1, v0

    goto/16 :goto_3

    :cond_5
    move v0, v1

    goto/16 :goto_2

    .line 321
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
    .end packed-switch
.end method
