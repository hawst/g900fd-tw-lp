.class public Lcom/osp/app/bigdatalog/BigDataLogReceiver;
.super Landroid/content/BroadcastReceiver;
.source "BigDataLogReceiver.java"


# instance fields
.field private a:Lcom/osp/app/bigdatalog/h;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 17
    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    return-void
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 6

    .prologue
    .line 37
    if-eqz p2, :cond_1

    const-string v0, "com.osp.app.signin.REGISTER_FILTER"

    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 39
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v0, "BDLR"

    const-string v1, "registration intent received"

    invoke-static {v0, v1}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 40
    invoke-virtual {p2}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v0

    .line 41
    if-eqz v0, :cond_1

    .line 43
    const-string v1, "EXTRA_STR_ACTION"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 44
    const-string v2, "EXTRA_STR"

    invoke-virtual {v0, v2}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 45
    const-string v3, "EXTRA_RESULT_CODE"

    invoke-virtual {v0, v3}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v0

    .line 46
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v3, "BDLR"

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "SPP Push Client registration result - EXTRA_STR_ACTION : "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v4, " //  EXTRA_STR : "

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "  //  EXTRA_RESULT_CODE : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v3, v1}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 48
    new-instance v1, Lcom/osp/app/bigdatalog/h;

    invoke-direct {v1, p1}, Lcom/osp/app/bigdatalog/h;-><init>(Landroid/content/Context;)V

    iput-object v1, p0, Lcom/osp/app/bigdatalog/BigDataLogReceiver;->a:Lcom/osp/app/bigdatalog/h;

    .line 51
    const/16 v1, 0x64

    if-eq v0, v1, :cond_0

    const/16 v1, 0xc8

    if-ne v0, v1, :cond_2

    .line 53
    :cond_0
    iget-object v0, p0, Lcom/osp/app/bigdatalog/BigDataLogReceiver;->a:Lcom/osp/app/bigdatalog/h;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/osp/app/bigdatalog/h;->a(Z)V

    .line 55
    invoke-static {}, Lcom/osp/app/bigdatalog/g;->a()Lcom/osp/app/bigdatalog/e;

    move-result-object v0

    invoke-virtual {v0}, Lcom/osp/app/bigdatalog/e;->b()V

    .line 56
    iget-object v0, p0, Lcom/osp/app/bigdatalog/BigDataLogReceiver;->a:Lcom/osp/app/bigdatalog/h;

    invoke-static {}, Lcom/osp/app/bigdatalog/g;->a()Lcom/osp/app/bigdatalog/e;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    const-string v1, "REGISTER_SPP_FAIL_WAIT_TIME"

    invoke-virtual {v0, v1}, Lcom/osp/app/bigdatalog/h;->c(Ljava/lang/String;)V

    .line 63
    :cond_1
    :goto_0
    return-void

    .line 59
    :cond_2
    iget-object v0, p0, Lcom/osp/app/bigdatalog/BigDataLogReceiver;->a:Lcom/osp/app/bigdatalog/h;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/osp/app/bigdatalog/h;->a(Z)V

    goto :goto_0
.end method
