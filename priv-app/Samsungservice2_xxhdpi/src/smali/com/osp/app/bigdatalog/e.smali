.class public final Lcom/osp/app/bigdatalog/e;
.super Ljava/lang/Object;
.source "BigDataLogManager.java"


# instance fields
.field public final a:I

.field public final b:J

.field public final c:Ljava/lang/String;

.field private d:Ljava/lang/ref/WeakReference;

.field private e:Lcom/osp/app/bigdatalog/h;

.field private f:Lcom/osp/app/bigdatalog/c;

.field private g:Ljava/util/Queue;

.field private h:Lcom/osp/app/bigdatalog/a;

.field private i:Z

.field private j:I


# direct methods
.method private constructor <init>()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 95
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 72
    iput v2, p0, Lcom/osp/app/bigdatalog/e;->j:I

    .line 77
    const/4 v0, 0x5

    iput v0, p0, Lcom/osp/app/bigdatalog/e;->a:I

    .line 82
    const-wide/32 v0, 0x2932e00

    iput-wide v0, p0, Lcom/osp/app/bigdatalog/e;->b:J

    .line 88
    const-string v0, "REGISTER_SPP_FAIL_WAIT_TIME"

    iput-object v0, p0, Lcom/osp/app/bigdatalog/e;->c:Ljava/lang/String;

    .line 96
    iput-boolean v2, p0, Lcom/osp/app/bigdatalog/e;->i:Z

    .line 97
    return-void
.end method

.method synthetic constructor <init>(B)V
    .locals 0

    .prologue
    .line 21
    invoke-direct {p0}, Lcom/osp/app/bigdatalog/e;-><init>()V

    return-void
.end method

.method static synthetic a(Lcom/osp/app/bigdatalog/e;Lcom/osp/app/bigdatalog/c;)Lcom/osp/app/bigdatalog/c;
    .locals 0

    .prologue
    .line 21
    iput-object p1, p0, Lcom/osp/app/bigdatalog/e;->f:Lcom/osp/app/bigdatalog/c;

    return-object p1
.end method

.method public static a()Lcom/osp/app/bigdatalog/e;
    .locals 1

    .prologue
    .line 105
    invoke-static {}, Lcom/osp/app/bigdatalog/g;->a()Lcom/osp/app/bigdatalog/e;

    move-result-object v0

    return-object v0
.end method

.method static synthetic a(Lcom/osp/app/bigdatalog/e;)Ljava/util/Queue;
    .locals 1

    .prologue
    .line 21
    iget-object v0, p0, Lcom/osp/app/bigdatalog/e;->g:Ljava/util/Queue;

    return-object v0
.end method

.method static synthetic b(Lcom/osp/app/bigdatalog/e;)I
    .locals 1

    .prologue
    .line 21
    invoke-direct {p0}, Lcom/osp/app/bigdatalog/e;->e()I

    move-result v0

    return v0
.end method

.method static synthetic c(Lcom/osp/app/bigdatalog/e;)Ljava/lang/ref/WeakReference;
    .locals 1

    .prologue
    .line 21
    iget-object v0, p0, Lcom/osp/app/bigdatalog/e;->d:Ljava/lang/ref/WeakReference;

    return-object v0
.end method

.method static synthetic d(Lcom/osp/app/bigdatalog/e;)Lcom/osp/app/bigdatalog/a;
    .locals 1

    .prologue
    .line 21
    iget-object v0, p0, Lcom/osp/app/bigdatalog/e;->h:Lcom/osp/app/bigdatalog/a;

    return-object v0
.end method

.method private d()V
    .locals 1

    .prologue
    .line 127
    iget v0, p0, Lcom/osp/app/bigdatalog/e;->j:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/osp/app/bigdatalog/e;->j:I

    .line 128
    return-void
.end method

.method private e()I
    .locals 11

    .prologue
    .line 293
    iget-object v0, p0, Lcom/osp/app/bigdatalog/e;->f:Lcom/osp/app/bigdatalog/c;

    if-eqz v0, :cond_1

    iget-boolean v0, p0, Lcom/osp/app/bigdatalog/e;->i:Z

    const/4 v1, 0x1

    if-ne v0, v1, :cond_1

    .line 295
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "sendLog : "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v1, p0, Lcom/osp/app/bigdatalog/e;->f:Lcom/osp/app/bigdatalog/c;

    invoke-virtual {v1}, Lcom/osp/app/bigdatalog/c;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;)V

    .line 296
    iget-object v0, p0, Lcom/osp/app/bigdatalog/e;->h:Lcom/osp/app/bigdatalog/a;

    invoke-virtual {v0}, Lcom/osp/app/bigdatalog/a;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 300
    :try_start_0
    iget-object v0, p0, Lcom/osp/app/bigdatalog/e;->h:Lcom/osp/app/bigdatalog/a;

    invoke-virtual {v0}, Lcom/osp/app/bigdatalog/a;->b()Lcom/sec/b/a/a/a/a;

    move-result-object v1

    iget-object v0, p0, Lcom/osp/app/bigdatalog/e;->f:Lcom/osp/app/bigdatalog/c;

    invoke-virtual {v0}, Lcom/osp/app/bigdatalog/c;->a()Ljava/lang/String;

    move-result-object v2

    iget-object v0, p0, Lcom/osp/app/bigdatalog/e;->f:Lcom/osp/app/bigdatalog/c;

    invoke-virtual {v0}, Lcom/osp/app/bigdatalog/c;->b()Ljava/lang/String;

    move-result-object v3

    iget-object v0, p0, Lcom/osp/app/bigdatalog/e;->f:Lcom/osp/app/bigdatalog/c;

    invoke-virtual {v0}, Lcom/osp/app/bigdatalog/c;->c()J

    move-result-wide v4

    iget-object v0, p0, Lcom/osp/app/bigdatalog/e;->f:Lcom/osp/app/bigdatalog/c;

    invoke-virtual {v0}, Lcom/osp/app/bigdatalog/c;->e()Ljava/lang/String;

    move-result-object v6

    const/4 v7, 0x0

    iget-object v0, p0, Lcom/osp/app/bigdatalog/e;->f:Lcom/osp/app/bigdatalog/c;

    invoke-virtual {v0}, Lcom/osp/app/bigdatalog/c;->f()Ljava/lang/String;

    move-result-object v8

    iget-object v0, p0, Lcom/osp/app/bigdatalog/e;->f:Lcom/osp/app/bigdatalog/c;

    invoke-virtual {v0}, Lcom/osp/app/bigdatalog/c;->d()Ljava/lang/String;

    move-result-object v9

    iget-object v0, p0, Lcom/osp/app/bigdatalog/e;->f:Lcom/osp/app/bigdatalog/c;

    invoke-virtual {v0}, Lcom/osp/app/bigdatalog/c;->g()Ljava/lang/String;

    move-result-object v10

    invoke-interface/range {v1 .. v10}, Lcom/sec/b/a/a/a/a;->a(Ljava/lang/String;Ljava/lang/String;JLjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result v0

    .line 309
    :goto_0
    return v0

    .line 304
    :catch_0
    move-exception v0

    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v0, "BDLM"

    const-string v1, "send log exception occurred"

    invoke-static {v0, v1}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 307
    :cond_0
    const/16 v0, -0x3e8

    goto :goto_0

    .line 309
    :cond_1
    const/16 v0, -0x7d0

    goto :goto_0
.end method


# virtual methods
.method public final a(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 115
    new-instance v0, Lcom/osp/app/bigdatalog/h;

    invoke-direct {v0, p1}, Lcom/osp/app/bigdatalog/h;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/osp/app/bigdatalog/e;->e:Lcom/osp/app/bigdatalog/h;

    .line 116
    new-instance v0, Lcom/osp/app/bigdatalog/a;

    invoke-direct {v0}, Lcom/osp/app/bigdatalog/a;-><init>()V

    iput-object v0, p0, Lcom/osp/app/bigdatalog/e;->h:Lcom/osp/app/bigdatalog/a;

    .line 117
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/osp/app/bigdatalog/e;->i:Z

    .line 118
    new-instance v0, Ljava/util/LinkedList;

    invoke-direct {v0}, Ljava/util/LinkedList;-><init>()V

    iput-object v0, p0, Lcom/osp/app/bigdatalog/e;->g:Ljava/util/Queue;

    .line 119
    new-instance v0, Ljava/lang/ref/WeakReference;

    invoke-direct {v0, p1}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    iput-object v0, p0, Lcom/osp/app/bigdatalog/e;->d:Ljava/lang/ref/WeakReference;

    .line 120
    return-void
.end method

.method public final a(Landroid/content/Context;Lcom/osp/app/bigdatalog/c;)V
    .locals 2

    .prologue
    .line 233
    iget-boolean v0, p0, Lcom/osp/app/bigdatalog/e;->i:Z

    if-nez v0, :cond_0

    .line 235
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v0, "BDLM"

    const-string v1, "sendLog denied because BigDataLogManager not initialized"

    invoke-static {v0, v1}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 253
    :goto_0
    return-void

    .line 239
    :cond_0
    invoke-static {p1}, Lcom/osp/app/util/ad;->s(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 241
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v0, "BDLM"

    const-string v1, "SendLog Do not use data in Chinese Model"

    invoke-static {v0, v1}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 245
    :cond_1
    if-eqz p2, :cond_2

    .line 247
    iput-object p2, p0, Lcom/osp/app/bigdatalog/e;->f:Lcom/osp/app/bigdatalog/c;

    .line 248
    iget-object v0, p0, Lcom/osp/app/bigdatalog/e;->h:Lcom/osp/app/bigdatalog/a;

    new-instance v1, Lcom/osp/app/bigdatalog/f;

    invoke-direct {v1, p0}, Lcom/osp/app/bigdatalog/f;-><init>(Lcom/osp/app/bigdatalog/e;)V

    invoke-virtual {v0, p1, v1}, Lcom/osp/app/bigdatalog/a;->a(Landroid/content/Context;Lcom/osp/app/bigdatalog/f;)V

    goto :goto_0

    .line 251
    :cond_2
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v0, "BDLM"

    const-string v1, "data to be sent is null"

    invoke-static {v0, v1}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public final a(Landroid/content/Context;Ljava/util/Queue;)V
    .locals 2

    .prologue
    .line 263
    iget-boolean v0, p0, Lcom/osp/app/bigdatalog/e;->i:Z

    if-nez v0, :cond_0

    .line 265
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v0, "BDLM"

    const-string v1, "sendLog denied because BigDataLogManager not initialized"

    invoke-static {v0, v1}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 283
    :goto_0
    return-void

    .line 269
    :cond_0
    invoke-static {p1}, Lcom/osp/app/util/ad;->s(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 271
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v0, "BDLM"

    const-string v1, "SendLog Do not use data in Chinese Model"

    invoke-static {v0, v1}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 275
    :cond_1
    if-eqz p2, :cond_2

    .line 277
    iput-object p2, p0, Lcom/osp/app/bigdatalog/e;->g:Ljava/util/Queue;

    .line 278
    iget-object v0, p0, Lcom/osp/app/bigdatalog/e;->h:Lcom/osp/app/bigdatalog/a;

    new-instance v1, Lcom/osp/app/bigdatalog/f;

    invoke-direct {v1, p0}, Lcom/osp/app/bigdatalog/f;-><init>(Lcom/osp/app/bigdatalog/e;)V

    invoke-virtual {v0, p1, v1}, Lcom/osp/app/bigdatalog/a;->a(Landroid/content/Context;Lcom/osp/app/bigdatalog/f;)V

    goto :goto_0

    .line 281
    :cond_2
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v0, "BDLM"

    const-string v1, "data to be sent is null"

    invoke-static {v0, v1}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public final b()V
    .locals 1

    .prologue
    .line 135
    const/4 v0, 0x0

    iput v0, p0, Lcom/osp/app/bigdatalog/e;->j:I

    .line 136
    return-void
.end method

.method public final b(Landroid/content/Context;)V
    .locals 3

    .prologue
    .line 179
    iget-boolean v0, p0, Lcom/osp/app/bigdatalog/e;->i:Z

    if-nez v0, :cond_0

    .line 181
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v0, "BDLM"

    const-string v1, "Registration denied because BigDataLogManager not initialized"

    invoke-static {v0, v1}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 223
    :goto_0
    return-void

    .line 185
    :cond_0
    invoke-static {p1}, Lcom/osp/app/util/ad;->s(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 187
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v0, "BDLM"

    const-string v1, "REGISTER SPP Do not use data in Chinese Model"

    invoke-static {v0, v1}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 191
    :cond_1
    new-instance v0, Landroid/content/Intent;

    const-string v1, "com.sec.spp.push.REQUEST_REGISTER"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 192
    const-string v1, "EXTRA_PACKAGENAME"

    const-string v2, "com.osp.app.signin"

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 193
    const-string v1, "EXTRA_INTENTFILTER"

    const-string v2, "com.osp.app.signin.REGISTER_FILTER"

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 195
    invoke-direct {p0}, Lcom/osp/app/bigdatalog/e;->d()V

    .line 197
    iget v1, p0, Lcom/osp/app/bigdatalog/e;->j:I

    const/4 v2, 0x5

    if-gt v1, v2, :cond_2

    .line 199
    invoke-virtual {p1, v0}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V

    .line 200
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v0, "BDLM"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Registration intent sent to spp push client. Count = "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v2, p0, Lcom/osp/app/bigdatalog/e;->j:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 204
    :cond_2
    iget v1, p0, Lcom/osp/app/bigdatalog/e;->j:I

    const/4 v2, 0x6

    if-ne v1, v2, :cond_3

    .line 206
    iget-object v0, p0, Lcom/osp/app/bigdatalog/e;->e:Lcom/osp/app/bigdatalog/h;

    const-string v1, "REGISTER_SPP_FAIL_WAIT_TIME"

    invoke-virtual {v0, v1}, Lcom/osp/app/bigdatalog/h;->a(Ljava/lang/String;)V

    .line 207
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v0, "BDLM"

    const-string v1, "SPP Register fail more than 5 times. Start 5min timer"

    invoke-static {v0, v1}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 211
    :cond_3
    iget-object v1, p0, Lcom/osp/app/bigdatalog/e;->e:Lcom/osp/app/bigdatalog/h;

    const-string v2, "REGISTER_SPP_FAIL_WAIT_TIME"

    invoke-virtual {v1, v2}, Lcom/osp/app/bigdatalog/h;->b(Ljava/lang/String;)Ljava/lang/Boolean;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    const/4 v2, 0x1

    if-ne v1, v2, :cond_4

    .line 213
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v1, "BDLM"

    const-string v2, "Waiting 12 hour is ended, Reset timer and count"

    invoke-static {v1, v2}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 214
    iget-object v1, p0, Lcom/osp/app/bigdatalog/e;->e:Lcom/osp/app/bigdatalog/h;

    const-string v2, "REGISTER_SPP_FAIL_WAIT_TIME"

    invoke-virtual {v1, v2}, Lcom/osp/app/bigdatalog/h;->c(Ljava/lang/String;)V

    .line 215
    const/4 v1, 0x0

    iput v1, p0, Lcom/osp/app/bigdatalog/e;->j:I

    .line 217
    invoke-virtual {p1, v0}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V

    .line 218
    invoke-direct {p0}, Lcom/osp/app/bigdatalog/e;->d()V

    goto/16 :goto_0

    .line 222
    :cond_4
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v0, "BDLM"

    const-string v1, "Registration intent didn\'t send to spp push client. Waiting 5min "

    invoke-static {v0, v1}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0
.end method

.method public final c()Z
    .locals 3

    .prologue
    .line 155
    const/4 v0, 0x0

    .line 156
    iget-boolean v1, p0, Lcom/osp/app/bigdatalog/e;->i:Z

    if-eqz v1, :cond_1

    .line 158
    iget-object v0, p0, Lcom/osp/app/bigdatalog/e;->e:Lcom/osp/app/bigdatalog/h;

    invoke-virtual {v0}, Lcom/osp/app/bigdatalog/h;->a()Ljava/lang/Boolean;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    .line 159
    if-eqz v0, :cond_0

    .line 161
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v1, "BDLM"

    const-string v2, "already registered in spp"

    invoke-static {v1, v2}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 170
    :goto_0
    return v0

    .line 164
    :cond_0
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v1, "BDLM"

    const-string v2, "not registered in spp yet"

    invoke-static {v1, v2}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 168
    :cond_1
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v1, "BDLM"

    const-string v2, "isSppRegistered failed because BigDataLogManager not initialized"

    invoke-static {v1, v2}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method
