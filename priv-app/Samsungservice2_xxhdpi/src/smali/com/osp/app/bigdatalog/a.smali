.class public final Lcom/osp/app/bigdatalog/a;
.super Ljava/lang/Object;
.source "BigDataLogBinder.java"


# instance fields
.field private a:Landroid/os/Handler;

.field private b:Lcom/sec/b/a/a/a/a;

.field private final c:Landroid/content/ServiceConnection;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 18
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 43
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/osp/app/bigdatalog/a;->b:Lcom/sec/b/a/a/a/a;

    .line 45
    new-instance v0, Lcom/osp/app/bigdatalog/b;

    invoke-direct {v0, p0}, Lcom/osp/app/bigdatalog/b;-><init>(Lcom/osp/app/bigdatalog/a;)V

    iput-object v0, p0, Lcom/osp/app/bigdatalog/a;->c:Landroid/content/ServiceConnection;

    return-void
.end method

.method static synthetic a(Lcom/osp/app/bigdatalog/a;)Landroid/os/Handler;
    .locals 1

    .prologue
    .line 18
    iget-object v0, p0, Lcom/osp/app/bigdatalog/a;->a:Landroid/os/Handler;

    return-object v0
.end method

.method static synthetic a(Lcom/osp/app/bigdatalog/a;Lcom/sec/b/a/a/a/a;)Lcom/sec/b/a/a/a/a;
    .locals 0

    .prologue
    .line 18
    iput-object p1, p0, Lcom/osp/app/bigdatalog/a;->b:Lcom/sec/b/a/a/a/a;

    return-object p1
.end method


# virtual methods
.method public final a(Landroid/content/Context;)V
    .locals 2

    .prologue
    .line 90
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v0, "BDLB"

    const-string v1, "SPP service unbinding triggered"

    invoke-static {v0, v1}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 93
    iget-object v0, p0, Lcom/osp/app/bigdatalog/a;->b:Lcom/sec/b/a/a/a/a;

    if-eqz v0, :cond_0

    .line 95
    iget-object v0, p0, Lcom/osp/app/bigdatalog/a;->c:Landroid/content/ServiceConnection;

    invoke-virtual {p1, v0}, Landroid/content/Context;->unbindService(Landroid/content/ServiceConnection;)V

    .line 96
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/osp/app/bigdatalog/a;->b:Lcom/sec/b/a/a/a/a;

    .line 98
    :cond_0
    return-void
.end method

.method public final a(Landroid/content/Context;Lcom/osp/app/bigdatalog/f;)V
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 68
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v0, "BDLB"

    const-string v1, "SPP service binding triggered"

    invoke-static {v0, v1}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 69
    iput-object p2, p0, Lcom/osp/app/bigdatalog/a;->a:Landroid/os/Handler;

    .line 71
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    .line 72
    const-string v1, "com.sec.spp.push.dlc.writer.WriterService"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 73
    new-instance v1, Landroid/content/ComponentName;

    const-string v2, "com.sec.spp.push"

    const-string v3, "com.sec.spp.push.dlc.writer.WriterService"

    invoke-direct {v1, v2, v3}, Landroid/content/ComponentName;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setComponent(Landroid/content/ComponentName;)Landroid/content/Intent;

    .line 75
    iget-object v1, p0, Lcom/osp/app/bigdatalog/a;->c:Landroid/content/ServiceConnection;

    const/4 v2, 0x1

    invoke-virtual {p1, v0, v1, v2}, Landroid/content/Context;->bindService(Landroid/content/Intent;Landroid/content/ServiceConnection;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 77
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v0, "BDLB"

    const-string v1, "SPP service bind failed"

    invoke-static {v0, v1}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 78
    iput-object v4, p0, Lcom/osp/app/bigdatalog/a;->b:Lcom/sec/b/a/a/a/a;

    .line 79
    iput-object v4, p0, Lcom/osp/app/bigdatalog/a;->a:Landroid/os/Handler;

    .line 81
    :cond_0
    return-void
.end method

.method public final a()Z
    .locals 2

    .prologue
    .line 107
    iget-object v0, p0, Lcom/osp/app/bigdatalog/a;->b:Lcom/sec/b/a/a/a/a;

    if-nez v0, :cond_0

    .line 109
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v0, "BDLB"

    const-string v1, "SPP service in disconnected status"

    invoke-static {v0, v1}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 110
    const/4 v0, 0x0

    .line 114
    :goto_0
    return v0

    .line 113
    :cond_0
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v0, "BDLB"

    const-string v1, "SPP service in connected status"

    invoke-static {v0, v1}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 114
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public final b()Lcom/sec/b/a/a/a/a;
    .locals 1

    .prologue
    .line 125
    iget-object v0, p0, Lcom/osp/app/bigdatalog/a;->b:Lcom/sec/b/a/a/a/a;

    return-object v0
.end method
