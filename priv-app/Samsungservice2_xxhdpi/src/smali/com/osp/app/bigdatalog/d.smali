.class public final Lcom/osp/app/bigdatalog/d;
.super Ljava/lang/Object;
.source "BigDataLogDlcApi.java"


# direct methods
.method public static a(I)Ljava/lang/String;
    .locals 1

    .prologue
    .line 124
    const-string v0, ""

    .line 125
    sparse-switch p0, :sswitch_data_0

    .line 153
    :goto_0
    return-object v0

    .line 128
    :sswitch_0
    const-string v0, "Success"

    goto :goto_0

    .line 131
    :sswitch_1
    const-string v0, "Invalid parameter"

    goto :goto_0

    .line 134
    :sswitch_2
    const-string v0, "Service is unavailable"

    goto :goto_0

    .line 137
    :sswitch_3
    const-string v0, "Application is blocked"

    goto :goto_0

    .line 140
    :sswitch_4
    const-string v0, "Permission error"

    goto :goto_0

    .line 143
    :sswitch_5
    const-string v0, "App not registered yet"

    goto :goto_0

    .line 146
    :sswitch_6
    const-string v0, "Service is not connected"

    goto :goto_0

    .line 149
    :sswitch_7
    const-string v0, "Log data is empty"

    goto :goto_0

    .line 152
    :sswitch_8
    const-string v0, "Log data is not sent yet"

    goto :goto_0

    .line 125
    nop

    :sswitch_data_0
    .sparse-switch
        -0xbb8 -> :sswitch_8
        -0x7d0 -> :sswitch_7
        -0x3e8 -> :sswitch_6
        -0x6 -> :sswitch_5
        -0x4 -> :sswitch_4
        -0x3 -> :sswitch_3
        -0x2 -> :sswitch_2
        -0x1 -> :sswitch_1
        0x0 -> :sswitch_0
    .end sparse-switch
.end method
