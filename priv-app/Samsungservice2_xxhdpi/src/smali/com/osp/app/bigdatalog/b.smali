.class final Lcom/osp/app/bigdatalog/b;
.super Ljava/lang/Object;
.source "BigDataLogBinder.java"

# interfaces
.implements Landroid/content/ServiceConnection;


# instance fields
.field final synthetic a:Lcom/osp/app/bigdatalog/a;


# direct methods
.method constructor <init>(Lcom/osp/app/bigdatalog/a;)V
    .locals 0

    .prologue
    .line 45
    iput-object p1, p0, Lcom/osp/app/bigdatalog/b;->a:Lcom/osp/app/bigdatalog/a;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onServiceConnected(Landroid/content/ComponentName;Landroid/os/IBinder;)V
    .locals 2

    .prologue
    .line 49
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v0, "BDLB"

    const-string v1, "SPP service connection completed"

    invoke-static {v0, v1}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 50
    iget-object v0, p0, Lcom/osp/app/bigdatalog/b;->a:Lcom/osp/app/bigdatalog/a;

    invoke-static {p2}, Lcom/sec/b/a/a/a/b;->a(Landroid/os/IBinder;)Lcom/sec/b/a/a/a/a;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/osp/app/bigdatalog/a;->a(Lcom/osp/app/bigdatalog/a;Lcom/sec/b/a/a/a/a;)Lcom/sec/b/a/a/a/a;

    .line 51
    iget-object v0, p0, Lcom/osp/app/bigdatalog/b;->a:Lcom/osp/app/bigdatalog/a;

    invoke-static {v0}, Lcom/osp/app/bigdatalog/a;->a(Lcom/osp/app/bigdatalog/a;)Landroid/os/Handler;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    .line 52
    return-void
.end method

.method public final onServiceDisconnected(Landroid/content/ComponentName;)V
    .locals 2

    .prologue
    .line 56
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v0, "BDLB"

    const-string v1, "SPP service disconnected"

    invoke-static {v0, v1}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 57
    iget-object v0, p0, Lcom/osp/app/bigdatalog/b;->a:Lcom/osp/app/bigdatalog/a;

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lcom/osp/app/bigdatalog/a;->a(Lcom/osp/app/bigdatalog/a;Lcom/sec/b/a/a/a/a;)Lcom/sec/b/a/a/a/a;

    .line 58
    return-void
.end method
