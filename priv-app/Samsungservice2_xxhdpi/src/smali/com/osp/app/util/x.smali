.class public final Lcom/osp/app/util/x;
.super Ljava/lang/Object;
.source "ScreenSpecificationUtil.java"


# direct methods
.method public static a(Landroid/app/Activity;)Z
    .locals 3

    .prologue
    .line 254
    const/16 v0, 0xf0

    const/16 v1, 0x140

    const/16 v2, 0x78

    invoke-static {p0, v0, v1, v2}, Lcom/osp/app/util/x;->a(Landroid/content/Context;III)Z

    move-result v0

    return v0
.end method

.method public static a(Landroid/content/Context;)Z
    .locals 3

    .prologue
    .line 134
    const/16 v0, 0x2d0

    const/16 v1, 0x500

    const/16 v2, 0x140

    invoke-static {p0, v0, v1, v2}, Lcom/osp/app/util/x;->a(Landroid/content/Context;III)Z

    move-result v0

    return v0
.end method

.method private static a(Landroid/content/Context;II)Z
    .locals 2

    .prologue
    .line 108
    invoke-static {p0}, Lcom/msc/sa/c/d;->b(Landroid/content/Context;)Landroid/util/DisplayMetrics;

    move-result-object v0

    .line 115
    iget v1, v0, Landroid/util/DisplayMetrics;->widthPixels:I

    .line 116
    iget v0, v0, Landroid/util/DisplayMetrics;->heightPixels:I

    .line 120
    if-gt v1, p1, :cond_0

    if-le v0, p2, :cond_1

    :cond_0
    if-gt v1, p2, :cond_2

    if-gt v0, p1, :cond_2

    .line 122
    :cond_1
    const/4 v0, 0x1

    .line 124
    :goto_0
    return v0

    :cond_2
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private static a(Landroid/content/Context;III)Z
    .locals 3

    .prologue
    .line 78
    invoke-static {p0}, Lcom/msc/sa/c/d;->b(Landroid/content/Context;)Landroid/util/DisplayMetrics;

    move-result-object v0

    .line 85
    iget v1, v0, Landroid/util/DisplayMetrics;->widthPixels:I

    .line 86
    iget v2, v0, Landroid/util/DisplayMetrics;->heightPixels:I

    .line 87
    iget v0, v0, Landroid/util/DisplayMetrics;->densityDpi:I

    .line 89
    if-ne v0, p3, :cond_2

    if-ne v1, p1, :cond_0

    if-eq v2, p2, :cond_1

    :cond_0
    if-ne v1, p2, :cond_2

    if-ne v2, p1, :cond_2

    .line 91
    :cond_1
    const/4 v0, 0x1

    .line 94
    :goto_0
    return v0

    :cond_2
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static b(Landroid/content/Context;)Z
    .locals 3

    .prologue
    .line 144
    const/16 v0, 0x300

    const/16 v1, 0x500

    const/16 v2, 0x140

    invoke-static {p0, v0, v1, v2}, Lcom/osp/app/util/x;->a(Landroid/content/Context;III)Z

    move-result v0

    return v0
.end method

.method public static c(Landroid/content/Context;)Z
    .locals 3

    .prologue
    .line 154
    const/16 v0, 0x21c

    const/16 v1, 0x3c0

    const/16 v2, 0xf0

    invoke-static {p0, v0, v1, v2}, Lcom/osp/app/util/x;->a(Landroid/content/Context;III)Z

    move-result v0

    return v0
.end method

.method public static d(Landroid/content/Context;)Z
    .locals 3

    .prologue
    .line 174
    const/16 v0, 0xa00

    const/16 v1, 0x640

    const/16 v2, 0x140

    invoke-static {p0, v0, v1, v2}, Lcom/osp/app/util/x;->a(Landroid/content/Context;III)Z

    move-result v0

    return v0
.end method

.method public static e(Landroid/content/Context;)Z
    .locals 3

    .prologue
    .line 184
    const/16 v0, 0x400

    const/16 v1, 0x300

    const/16 v2, 0xa0

    invoke-static {p0, v0, v1, v2}, Lcom/osp/app/util/x;->a(Landroid/content/Context;III)Z

    move-result v0

    return v0
.end method

.method public static f(Landroid/content/Context;)Z
    .locals 3

    .prologue
    .line 194
    const/16 v0, 0x800

    const/16 v1, 0x600

    const/16 v2, 0x140

    invoke-static {p0, v0, v1, v2}, Lcom/osp/app/util/x;->a(Landroid/content/Context;III)Z

    move-result v0

    return v0
.end method

.method public static g(Landroid/content/Context;)Z
    .locals 3

    .prologue
    .line 204
    const/16 v0, 0x500

    const/16 v1, 0x320

    const/16 v2, 0xa0

    invoke-static {p0, v0, v1, v2}, Lcom/osp/app/util/x;->a(Landroid/content/Context;III)Z

    move-result v0

    return v0
.end method

.method public static h(Landroid/content/Context;)Z
    .locals 3

    .prologue
    .line 214
    const/16 v0, 0x500

    const/16 v1, 0x320

    const/16 v2, 0xd5

    invoke-static {p0, v0, v1, v2}, Lcom/osp/app/util/x;->a(Landroid/content/Context;III)Z

    move-result v0

    return v0
.end method

.method public static i(Landroid/content/Context;)Z
    .locals 3

    .prologue
    .line 224
    const/16 v0, 0x2d0

    const/16 v1, 0x500

    const/16 v2, 0xf0

    invoke-static {p0, v0, v1, v2}, Lcom/osp/app/util/x;->a(Landroid/content/Context;III)Z

    move-result v0

    return v0
.end method

.method public static j(Landroid/content/Context;)Z
    .locals 3

    .prologue
    .line 234
    const/16 v0, 0x1e0

    const/16 v1, 0x320

    const/16 v2, 0xf0

    invoke-static {p0, v0, v1, v2}, Lcom/osp/app/util/x;->a(Landroid/content/Context;III)Z

    move-result v0

    return v0
.end method

.method public static k(Landroid/content/Context;)Z
    .locals 3

    .prologue
    .line 244
    const/16 v0, 0x140

    const/16 v1, 0x1e0

    const/16 v2, 0xa0

    invoke-static {p0, v0, v1, v2}, Lcom/osp/app/util/x;->a(Landroid/content/Context;III)Z

    move-result v0

    return v0
.end method

.method public static l(Landroid/content/Context;)Z
    .locals 3

    .prologue
    .line 264
    const/16 v0, 0x5a0

    const/16 v1, 0xa00

    const/16 v2, 0x280

    invoke-static {p0, v0, v1, v2}, Lcom/osp/app/util/x;->a(Landroid/content/Context;III)Z

    move-result v0

    return v0
.end method

.method public static m(Landroid/content/Context;)Z
    .locals 2

    .prologue
    .line 274
    const/16 v0, 0x21b

    const/16 v1, 0x3bf

    invoke-static {p0, v0, v1}, Lcom/osp/app/util/x;->a(Landroid/content/Context;II)Z

    move-result v0

    return v0
.end method

.method public static n(Landroid/content/Context;)Z
    .locals 2

    .prologue
    .line 286
    const/16 v0, 0x21c

    const/16 v1, 0x3c0

    invoke-static {p0, v0, v1}, Lcom/osp/app/util/x;->a(Landroid/content/Context;II)Z

    move-result v0

    return v0
.end method

.method public static o(Landroid/content/Context;)Z
    .locals 2

    .prologue
    .line 296
    const/16 v0, 0x1df

    const/16 v1, 0x31f

    invoke-static {p0, v0, v1}, Lcom/osp/app/util/x;->a(Landroid/content/Context;II)Z

    move-result v0

    return v0
.end method

.method public static p(Landroid/content/Context;)Z
    .locals 2

    .prologue
    .line 316
    const/16 v0, 0x13f

    const/16 v1, 0x1df

    invoke-static {p0, v0, v1}, Lcom/osp/app/util/x;->a(Landroid/content/Context;II)Z

    move-result v0

    return v0
.end method

.method public static q(Landroid/content/Context;)Z
    .locals 2

    .prologue
    .line 336
    const/16 v0, 0x2cf

    const/16 v1, 0x4ff

    invoke-static {p0, v0, v1}, Lcom/osp/app/util/x;->a(Landroid/content/Context;II)Z

    move-result v0

    return v0
.end method

.method public static r(Landroid/content/Context;)Z
    .locals 2

    .prologue
    .line 346
    const/16 v0, 0x437

    const/16 v1, 0x77f

    invoke-static {p0, v0, v1}, Lcom/osp/app/util/x;->a(Landroid/content/Context;II)Z

    move-result v0

    return v0
.end method
