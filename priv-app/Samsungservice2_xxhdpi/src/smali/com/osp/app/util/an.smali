.class public final Lcom/osp/app/util/an;
.super Ljava/lang/Object;
.source "ThemeResourceManager.java"


# static fields
.field private static a:Lcom/osp/app/util/an;

.field private static b:Ljava/lang/ref/WeakReference;


# direct methods
.method private constructor <init>()V
    .locals 0

    .prologue
    .line 134
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 136
    return-void
.end method

.method public static a(I)I
    .locals 2

    .prologue
    .line 164
    .line 166
    sget-object v0, Lcom/osp/app/util/an;->b:Ljava/lang/ref/WeakReference;

    if-eqz v0, :cond_0

    .line 168
    sget-object v0, Lcom/osp/app/util/an;->b:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/osp/app/util/BaseActivity;

    .line 169
    if-eqz v0, :cond_0

    .line 171
    invoke-virtual {v0}, Lcom/osp/app/util/BaseActivity;->h()Z

    move-result v1

    invoke-static {v0, p0, v1}, Lcom/osp/app/util/an;->a(Lcom/osp/app/util/BaseActivity;IZ)I

    move-result p0

    .line 175
    :cond_0
    return p0
.end method

.method private static a(Lcom/osp/app/util/BaseActivity;I)I
    .locals 6

    .prologue
    .line 366
    .line 368
    if-eqz p0, :cond_1

    .line 370
    invoke-virtual {p0}, Lcom/osp/app/util/BaseActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    .line 371
    invoke-virtual {v2, p1}, Landroid/content/res/Resources;->getResourceName(I)Ljava/lang/String;

    move-result-object v0

    .line 372
    invoke-virtual {v2, p1}, Landroid/content/res/Resources;->getResourceTypeName(I)Ljava/lang/String;

    move-result-object v3

    .line 373
    invoke-virtual {v2, p1}, Landroid/content/res/Resources;->getResourcePackageName(I)Ljava/lang/String;

    move-result-object v4

    .line 375
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v5, "res name="

    invoke-direct {v1, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;)V

    .line 376
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v5, "res type="

    invoke-direct {v1, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;)V

    .line 377
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v5, "res pkg="

    invoke-direct {v1, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;)V

    .line 379
    invoke-static {p0}, Lcom/osp/app/util/x;->d(Landroid/content/Context;)Z

    move-result v1

    if-nez v1, :cond_0

    invoke-static {p0}, Lcom/osp/app/util/x;->f(Landroid/content/Context;)Z

    move-result v1

    if-nez v1, :cond_0

    invoke-static {p0}, Lcom/osp/app/util/x;->e(Landroid/content/Context;)Z

    move-result v1

    if-eqz v1, :cond_3

    .line 382
    :cond_0
    invoke-static {p0}, Lcom/osp/app/util/r;->o(Landroid/content/Context;)Lcom/osp/app/util/s;

    move-result-object v1

    sget-object v5, Lcom/osp/app/util/s;->c:Lcom/osp/app/util/s;

    if-ne v1, v5, :cond_2

    .line 384
    const-string v1, "wqxga12"

    invoke-virtual {v0, v1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_a

    .line 386
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "_wqxga12"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    move-object v1, v0

    .line 409
    :goto_0
    :try_start_0
    invoke-virtual {v2, v1, v3, v4}, Landroid/content/res/Resources;->getIdentifier(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    .line 415
    if-gtz v0, :cond_7

    .line 426
    :cond_1
    :goto_1
    return p1

    .line 389
    :cond_2
    invoke-static {p0}, Lcom/osp/app/util/r;->o(Landroid/content/Context;)Lcom/osp/app/util/s;

    move-result-object v1

    sget-object v5, Lcom/osp/app/util/s;->a:Lcom/osp/app/util/s;

    if-ne v1, v5, :cond_a

    .line 391
    const-string v1, "wqxga8"

    invoke-virtual {v0, v1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_a

    .line 393
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "_wqxga8"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    move-object v1, v0

    goto :goto_0

    .line 396
    :cond_3
    invoke-static {p0}, Lcom/osp/app/util/x;->l(Landroid/content/Context;)Z

    move-result v1

    if-eqz v1, :cond_4

    .line 398
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "_t"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    move-object v1, v0

    goto :goto_0

    .line 399
    :cond_4
    sget-object v1, Lcom/osp/app/util/u;->u:[Ljava/lang/String;

    invoke-static {v1}, Lcom/osp/app/util/u;->b([Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_5

    .line 401
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "_hdtft"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    move-object v1, v0

    goto :goto_0

    .line 402
    :cond_5
    invoke-static {p0}, Lcom/osp/app/util/r;->h(Landroid/content/Context;)Z

    move-result v1

    if-eqz v1, :cond_a

    .line 404
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "_wxgavzw"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    move-object v1, v0

    goto :goto_0

    :cond_6
    move v0, p1

    .line 421
    :cond_7
    if-eq v0, p1, :cond_8

    .line 423
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "convert res name="

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;)V

    :cond_8
    move p1, v0

    goto/16 :goto_1

    .line 415
    :cond_9
    throw v0

    :catch_0
    move-exception v0

    if-gtz p1, :cond_6

    goto/16 :goto_1

    :catchall_0
    move-exception v0

    if-gtz p1, :cond_9

    goto/16 :goto_1

    :cond_a
    move-object v1, v0

    goto/16 :goto_0
.end method

.method private static a(Lcom/osp/app/util/BaseActivity;IZ)I
    .locals 7

    .prologue
    .line 230
    .line 233
    if-eqz p0, :cond_0

    .line 235
    invoke-virtual {p0}, Lcom/osp/app/util/BaseActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    .line 236
    invoke-virtual {v4, p1}, Landroid/content/res/Resources;->getResourceName(I)Ljava/lang/String;

    move-result-object v0

    .line 237
    invoke-virtual {v4, p1}, Landroid/content/res/Resources;->getResourceTypeName(I)Ljava/lang/String;

    move-result-object v5

    .line 238
    invoke-virtual {v4, p1}, Landroid/content/res/Resources;->getResourcePackageName(I)Ljava/lang/String;

    move-result-object v6

    .line 240
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "res name="

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;)V

    .line 241
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "res type="

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;)V

    .line 242
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "res pkg="

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;)V

    .line 244
    const-string v1, "dark"

    invoke-virtual {v0, v1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_1b

    if-nez p2, :cond_1b

    .line 246
    const-string v1, "dark"

    const-string v2, "light"

    invoke-virtual {v0, v1, v2}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v0

    .line 250
    :try_start_0
    invoke-virtual {v4, v0, v5, v6}, Landroid/content/res/Resources;->getIdentifier(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result v1

    .line 256
    if-gtz p1, :cond_2

    .line 355
    :cond_0
    :goto_0
    return p1

    :cond_1
    move v1, p1

    .line 263
    :cond_2
    :goto_1
    const-string v2, "setupwizard_account_button_"

    invoke-virtual {v0, v2}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_3

    const-string v2, "setupwizardaccount_divider_"

    invoke-virtual {v0, v2}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_4

    :cond_3
    invoke-static {}, Lcom/osp/app/signin/SamsungService;->a()Z

    move-result v2

    if-eqz v2, :cond_4

    invoke-static {p0}, Lcom/osp/app/util/r;->n(Landroid/content/Context;)Z

    move-result v2

    if-nez v2, :cond_4

    .line 268
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "_setup"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 273
    :try_start_1
    const-string v3, "light_setup"

    invoke-virtual {v2, v3}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v3

    if-eqz v3, :cond_8

    .line 275
    invoke-static {p0}, Lcom/osp/app/util/r;->m(Landroid/content/Context;)Z

    move-result v3

    if-eqz v3, :cond_7

    if-nez p2, :cond_7

    .line 277
    invoke-virtual {v4, v2, v5, v6}, Landroid/content/res/Resources;->getIdentifier(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)I
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_2
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result v3

    .line 290
    :goto_2
    if-lez v3, :cond_4

    move-object v0, v2

    move v1, v3

    .line 298
    :cond_4
    :goto_3
    invoke-static {}, Lcom/osp/app/util/r;->f()Z

    move-result v2

    if-nez v2, :cond_5

    invoke-static {}, Lcom/osp/app/util/r;->l()Z

    move-result v2

    if-eqz v2, :cond_a

    :cond_5
    invoke-static {}, Lcom/osp/app/util/r;->h()Z

    move-result v2

    if-eqz v2, :cond_a

    .line 300
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, "_vzw"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 340
    :goto_4
    :try_start_2
    invoke-virtual {v4, v0, v5, v6}, Landroid/content/res/Resources;->getIdentifier(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)I
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_1
    .catchall {:try_start_2 .. :try_end_2} :catchall_2

    move-result p1

    .line 346
    if-gtz p1, :cond_19

    :goto_5
    move p1, v1

    .line 348
    goto :goto_0

    .line 256
    :cond_6
    throw v0

    .line 278
    :cond_7
    :try_start_3
    invoke-static {p0}, Lcom/osp/app/util/r;->l(Landroid/content/Context;)Z

    move-result v3

    if-eqz v3, :cond_1a

    .line 280
    invoke-virtual {v4, v2, v5, v6}, Landroid/content/res/Resources;->getIdentifier(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)I

    move-result v3

    goto :goto_2

    .line 284
    :cond_8
    invoke-virtual {v4, v2, v5, v6}, Landroid/content/res/Resources;->getIdentifier(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)I
    :try_end_3
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_2
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    move-result v3

    goto :goto_2

    .line 290
    :catchall_0
    move-exception v0

    if-lez v1, :cond_9

    :cond_9
    throw v0

    .line 301
    :cond_a
    invoke-static {}, Lcom/osp/app/util/r;->k()Z

    move-result v2

    if-eqz v2, :cond_b

    .line 303
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, "_lt01"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_4

    .line 304
    :cond_b
    sget-object v2, Lcom/osp/app/util/u;->d:[Ljava/lang/String;

    invoke-static {v2}, Lcom/osp/app/util/u;->a([Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_c

    .line 306
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, "_cane"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_4

    .line 307
    :cond_c
    sget-object v2, Lcom/osp/app/util/u;->b:[Ljava/lang/String;

    invoke-static {v2}, Lcom/osp/app/util/u;->a([Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_d

    invoke-static {p0}, Lcom/osp/app/util/r;->m(Landroid/content/Context;)Z

    move-result v2

    if-nez v2, :cond_d

    .line 309
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, "_crater"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_4

    .line 310
    :cond_d
    invoke-static {p0}, Lcom/osp/app/util/x;->c(Landroid/content/Context;)Z

    move-result v2

    if-eqz v2, :cond_e

    invoke-static {p0}, Lcom/osp/app/util/r;->m(Landroid/content/Context;)Z

    move-result v2

    if-nez v2, :cond_e

    .line 312
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, "_serrano"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_4

    .line 313
    :cond_e
    sget-object v2, Lcom/osp/app/util/u;->s:[Ljava/lang/String;

    invoke-static {v2}, Lcom/osp/app/util/u;->b([Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_f

    .line 315
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, "_kk"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_4

    .line 316
    :cond_f
    sget-object v2, Lcom/osp/app/util/u;->e:[Ljava/lang/String;

    invoke-static {v2}, Lcom/osp/app/util/u;->a([Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_10

    sget-object v2, Lcom/osp/app/util/u;->a:[Ljava/lang/String;

    invoke-static {v2}, Lcom/osp/app/util/u;->a([Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_10

    sget-object v2, Lcom/osp/app/util/u;->c:[Ljava/lang/String;

    invoke-static {v2}, Lcom/osp/app/util/u;->a([Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_11

    .line 318
    :cond_10
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, "_ms01"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_4

    .line 319
    :cond_11
    sget-object v2, Lcom/osp/app/util/u;->i:[Ljava/lang/String;

    invoke-static {v2}, Lcom/osp/app/util/u;->a([Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_12

    .line 321
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, "_h"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_4

    .line 322
    :cond_12
    sget-object v2, Lcom/osp/app/util/u;->f:[Ljava/lang/String;

    invoke-static {v2}, Lcom/osp/app/util/u;->a([Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_13

    .line 324
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, "_q7"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_4

    .line 325
    :cond_13
    invoke-static {}, Lcom/osp/app/util/r;->m()Z

    move-result v2

    if-eqz v2, :cond_14

    .line 327
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, "_goya"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_4

    .line 328
    :cond_14
    sget-object v2, Lcom/osp/app/util/u;->p:[Ljava/lang/String;

    invoke-static {v2}, Lcom/osp/app/util/u;->b([Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_15

    sget-object v2, Lcom/osp/app/util/u;->r:[Ljava/lang/String;

    invoke-static {v2}, Lcom/osp/app/util/u;->a([Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_16

    invoke-static {p0}, Lcom/osp/app/util/r;->m(Landroid/content/Context;)Z

    move-result v2

    if-eqz v2, :cond_16

    invoke-static {p0}, Lcom/osp/app/util/x;->j(Landroid/content/Context;)Z

    move-result v2

    if-eqz v2, :cond_16

    const/4 v2, 0x1

    :goto_6
    if-nez v2, :cond_15

    sget-object v2, Lcom/osp/app/util/u;->t:[Ljava/lang/String;

    invoke-static {v2}, Lcom/osp/app/util/u;->b([Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_15

    sget-object v2, Lcom/osp/app/util/u;->u:[Ljava/lang/String;

    invoke-static {v2}, Lcom/osp/app/util/u;->b([Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_17

    .line 331
    :cond_15
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, "_tft"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_4

    .line 328
    :cond_16
    const/4 v2, 0x0

    goto :goto_6

    .line 334
    :cond_17
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "convert res name="

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;)V

    move p1, v1

    .line 335
    goto/16 :goto_0

    .line 346
    :cond_18
    throw v0

    .line 352
    :cond_19
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "convert res name="

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;)V

    goto/16 :goto_0

    :catch_0
    move-exception v1

    .line 256
    if-gtz p1, :cond_1

    goto/16 :goto_0

    :catchall_1
    move-exception v0

    if-gtz p1, :cond_6

    goto/16 :goto_0

    :catch_1
    move-exception v2

    .line 346
    if-gtz p1, :cond_19

    goto/16 :goto_5

    :catchall_2
    move-exception v0

    if-gtz p1, :cond_18

    goto/16 :goto_5

    :catch_2
    move-exception v3

    .line 290
    if-lez v1, :cond_4

    move-object v0, v2

    .line 293
    goto/16 :goto_3

    :cond_1a
    move v3, v1

    goto/16 :goto_2

    :cond_1b
    move v1, p1

    goto/16 :goto_1
.end method

.method public static declared-synchronized a(Lcom/osp/app/util/BaseActivity;)Lcom/osp/app/util/an;
    .locals 2

    .prologue
    .line 145
    const-class v1, Lcom/osp/app/util/an;

    monitor-enter v1

    :try_start_0
    sget-object v0, Lcom/osp/app/util/an;->a:Lcom/osp/app/util/an;

    if-nez v0, :cond_0

    .line 147
    new-instance v0, Lcom/osp/app/util/an;

    invoke-direct {v0}, Lcom/osp/app/util/an;-><init>()V

    sput-object v0, Lcom/osp/app/util/an;->a:Lcom/osp/app/util/an;

    .line 149
    :cond_0
    sget-object v0, Lcom/osp/app/util/an;->b:Ljava/lang/ref/WeakReference;

    if-eqz v0, :cond_1

    sget-object v0, Lcom/osp/app/util/an;->b:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    if-eqz v0, :cond_1

    sget-object v0, Lcom/osp/app/util/an;->b:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/osp/app/util/BaseActivity;

    invoke-virtual {v0, p0}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_2

    .line 151
    :cond_1
    new-instance v0, Ljava/lang/ref/WeakReference;

    invoke-direct {v0, p0}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    sput-object v0, Lcom/osp/app/util/an;->b:Ljava/lang/ref/WeakReference;

    .line 154
    :cond_2
    sget-object v0, Lcom/osp/app/util/an;->a:Lcom/osp/app/util/an;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit v1

    return-object v0

    .line 145
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method public static b(I)I
    .locals 1

    .prologue
    .line 185
    .line 187
    sget-object v0, Lcom/osp/app/util/an;->b:Ljava/lang/ref/WeakReference;

    if-eqz v0, :cond_0

    .line 189
    sget-object v0, Lcom/osp/app/util/an;->b:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/osp/app/util/BaseActivity;

    .line 190
    if-eqz v0, :cond_0

    .line 192
    invoke-static {v0, p0}, Lcom/osp/app/util/an;->a(Lcom/osp/app/util/BaseActivity;I)I

    move-result p0

    .line 196
    :cond_0
    return p0
.end method
