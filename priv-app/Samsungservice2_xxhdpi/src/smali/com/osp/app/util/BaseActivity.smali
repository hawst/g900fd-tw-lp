.class public abstract Lcom/osp/app/util/BaseActivity;
.super Landroid/app/Activity;
.source "BaseActivity.java"


# instance fields
.field private A:Z

.field private B:Z

.field private C:Ljava/lang/String;

.field private D:I

.field private E:Ljava/lang/String;

.field private final a:Ljava/lang/String;

.field private final b:I

.field private final c:I

.field private final d:I

.field private final e:I

.field private final f:I

.field protected final g:Ljava/lang/String;

.field protected final h:Ljava/lang/String;

.field protected final i:Ljava/lang/String;

.field protected final j:Ljava/lang/String;

.field protected final k:Ljava/lang/String;

.field protected final l:Ljava/lang/String;

.field protected final m:Ljava/lang/String;

.field protected final n:Ljava/lang/String;

.field protected final o:Ljava/lang/String;

.field protected final p:Ljava/lang/String;

.field protected final q:Ljava/lang/String;

.field protected r:Lcom/msc/sa/c/f;

.field protected final s:I

.field protected final t:I

.field protected final u:I

.field protected final v:I

.field protected final w:I

.field protected x:Landroid/view/Menu;

.field private final y:I

.field private final z:Lcom/osp/app/util/am;


# direct methods
.method public constructor <init>()V
    .locals 4

    .prologue
    const/4 v3, 0x5

    const/4 v2, 0x0

    const/16 v1, 0x32

    .line 82
    invoke-direct {p0}, Landroid/app/Activity;-><init>()V

    .line 84
    const-string v0, "ViewComponentAttribute"

    iput-object v0, p0, Lcom/osp/app/util/BaseActivity;->a:Ljava/lang/String;

    .line 89
    iput v1, p0, Lcom/osp/app/util/BaseActivity;->b:I

    .line 94
    iput v1, p0, Lcom/osp/app/util/BaseActivity;->c:I

    .line 99
    const/16 v0, 0xc8

    iput v0, p0, Lcom/osp/app/util/BaseActivity;->d:I

    .line 104
    iput v1, p0, Lcom/osp/app/util/BaseActivity;->e:I

    .line 109
    const/16 v0, 0xf

    iput v0, p0, Lcom/osp/app/util/BaseActivity;->f:I

    .line 114
    iput v3, p0, Lcom/osp/app/util/BaseActivity;->y:I

    .line 119
    const-string v0, "done_cancel"

    iput-object v0, p0, Lcom/osp/app/util/BaseActivity;->g:Ljava/lang/String;

    .line 124
    const-string v0, "signin_cancel"

    iput-object v0, p0, Lcom/osp/app/util/BaseActivity;->h:Ljava/lang/String;

    .line 129
    const-string v0, "signup_cancel"

    iput-object v0, p0, Lcom/osp/app/util/BaseActivity;->i:Ljava/lang/String;

    .line 134
    const-string v0, "save_cancel"

    iput-object v0, p0, Lcom/osp/app/util/BaseActivity;->j:Ljava/lang/String;

    .line 139
    const-string v0, "agree_cancel"

    iput-object v0, p0, Lcom/osp/app/util/BaseActivity;->k:Ljava/lang/String;

    .line 144
    const-string v0, "agree_decliene"

    iput-object v0, p0, Lcom/osp/app/util/BaseActivity;->l:Ljava/lang/String;

    .line 149
    const-string v0, "confirm_cancel"

    iput-object v0, p0, Lcom/osp/app/util/BaseActivity;->m:Ljava/lang/String;

    .line 154
    const-string v0, "edit"

    iput-object v0, p0, Lcom/osp/app/util/BaseActivity;->n:Ljava/lang/String;

    .line 159
    const-string v0, "next"

    iput-object v0, p0, Lcom/osp/app/util/BaseActivity;->o:Ljava/lang/String;

    .line 164
    const-string v0, "finish"

    iput-object v0, p0, Lcom/osp/app/util/BaseActivity;->p:Ljava/lang/String;

    .line 169
    const-string v0, "verify"

    iput-object v0, p0, Lcom/osp/app/util/BaseActivity;->q:Ljava/lang/String;

    .line 181
    iput v2, p0, Lcom/osp/app/util/BaseActivity;->s:I

    .line 186
    const/4 v0, 0x1

    iput v0, p0, Lcom/osp/app/util/BaseActivity;->t:I

    .line 192
    const/4 v0, 0x2

    iput v0, p0, Lcom/osp/app/util/BaseActivity;->u:I

    .line 198
    const/4 v0, 0x3

    iput v0, p0, Lcom/osp/app/util/BaseActivity;->v:I

    .line 204
    iput v3, p0, Lcom/osp/app/util/BaseActivity;->w:I

    .line 209
    new-instance v0, Lcom/osp/app/util/am;

    invoke-direct {v0, p0}, Lcom/osp/app/util/am;-><init>(Landroid/app/Activity;)V

    iput-object v0, p0, Lcom/osp/app/util/BaseActivity;->z:Lcom/osp/app/util/am;

    .line 307
    iput v2, p0, Lcom/osp/app/util/BaseActivity;->D:I

    .line 355
    return-void
.end method

.method private a(I)V
    .locals 11

    .prologue
    const v10, 0x7f0c015f

    const/16 v9, 0x8

    const/4 v8, 0x4

    const v7, 0x106000d

    const/4 v2, 0x0

    .line 1875
    invoke-static {}, Lcom/osp/app/signin/SamsungService;->a()Z

    move-result v0

    if-nez v0, :cond_b

    .line 1877
    const v0, 0x7f0c015e

    invoke-virtual {p0, v0}, Lcom/osp/app/util/BaseActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    .line 1878
    if-eqz v0, :cond_b

    .line 1880
    invoke-static {}, Lcom/osp/app/util/q;->a()Lcom/osp/app/util/q;

    if-eqz v0, :cond_b

    instance-of v1, p0, Lcom/osp/app/util/BaseActivity;

    if-eqz v1, :cond_b

    move-object v1, p0

    check-cast v1, Lcom/osp/app/util/BaseActivity;

    invoke-static {v1}, Lcom/osp/app/util/an;->a(Lcom/osp/app/util/BaseActivity;)Lcom/osp/app/util/an;

    const/4 v1, 0x2

    if-ne p1, v1, :cond_7

    move v1, v2

    :goto_0
    invoke-virtual {v0}, Landroid/view/ViewGroup;->getChildCount()I

    move-result v2

    if-ge v1, v2, :cond_b

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->getChildAt(I)Landroid/view/View;

    move-result-object v2

    invoke-virtual {v2}, Landroid/view/View;->getId()I

    move-result v2

    const v3, 0x7f0c0160

    if-ne v2, v3, :cond_0

    invoke-static {p0}, Lcom/osp/app/util/r;->l(Landroid/content/Context;)Z

    move-result v2

    if-nez v2, :cond_3

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->getChildAt(I)Landroid/view/View;

    move-result-object v2

    const v3, 0x7f02012e

    invoke-static {v3}, Lcom/osp/app/util/an;->a(I)I

    move-result v3

    invoke-virtual {v2, v3}, Landroid/view/View;->setBackgroundResource(I)V

    :cond_0
    :goto_1
    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->getChildAt(I)Landroid/view/View;

    move-result-object v2

    invoke-virtual {v2}, Landroid/view/View;->getId()I

    move-result v2

    if-eq v2, v10, :cond_1

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->getChildAt(I)Landroid/view/View;

    move-result-object v2

    invoke-virtual {v2}, Landroid/view/View;->getId()I

    move-result v2

    const v3, 0x7f0c0162

    if-ne v2, v3, :cond_2

    :cond_1
    invoke-static {p0}, Lcom/osp/app/util/r;->e(Landroid/content/Context;)Z

    move-result v2

    if-eqz v2, :cond_4

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->getChildAt(I)Landroid/view/View;

    move-result-object v2

    invoke-virtual {v2, v8}, Landroid/view/View;->setVisibility(I)V

    :cond_2
    :goto_2
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :cond_3
    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->getChildAt(I)Landroid/view/View;

    move-result-object v2

    invoke-virtual {v2, v7}, Landroid/view/View;->setBackgroundResource(I)V

    goto :goto_1

    :cond_4
    const v2, 0x7f070024

    invoke-static {v2}, Lcom/osp/app/util/an;->a(I)I

    move-result v2

    if-lez v2, :cond_6

    :try_start_0
    invoke-static {p0}, Lcom/osp/app/util/r;->l(Landroid/content/Context;)Z

    move-result v3

    if-nez v3, :cond_5

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->getChildAt(I)Landroid/view/View;

    move-result-object v3

    invoke-virtual {v3, v2}, Landroid/view/View;->setBackgroundResource(I)V

    :goto_3
    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->getChildAt(I)Landroid/view/View;

    move-result-object v2

    const/4 v3, 0x0

    invoke-virtual {v2, v3}, Landroid/view/View;->setVisibility(I)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_2

    :catch_0
    move-exception v2

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->getChildAt(I)Landroid/view/View;

    move-result-object v2

    invoke-virtual {v2, v9}, Landroid/view/View;->setVisibility(I)V

    goto :goto_2

    :cond_5
    :try_start_1
    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->getChildAt(I)Landroid/view/View;

    move-result-object v2

    invoke-virtual {v2}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v3

    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const v5, 0x7f08011f

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v4

    iput v4, v3, Landroid/view/ViewGroup$LayoutParams;->width:I

    iget v4, v3, Landroid/view/ViewGroup$LayoutParams;->width:I

    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    const v6, 0x7f08011e

    invoke-virtual {v5, v6}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v5

    sub-int/2addr v4, v5

    iput v4, v3, Landroid/view/ViewGroup$LayoutParams;->width:I

    invoke-virtual {v2, v3}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->getChildAt(I)Landroid/view/View;

    move-result-object v2

    const v3, 0x106000d

    invoke-virtual {v2, v3}, Landroid/view/View;->setBackgroundResource(I)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_3

    :cond_6
    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->getChildAt(I)Landroid/view/View;

    move-result-object v2

    invoke-virtual {v2, v8}, Landroid/view/View;->setVisibility(I)V

    goto :goto_2

    :cond_7
    :goto_4
    invoke-virtual {v0}, Landroid/view/ViewGroup;->getChildCount()I

    move-result v1

    if-ge v2, v1, :cond_b

    invoke-virtual {v0, v2}, Landroid/view/ViewGroup;->getChildAt(I)Landroid/view/View;

    move-result-object v1

    invoke-virtual {v1}, Landroid/view/View;->getId()I

    move-result v1

    const v3, 0x7f0c0160

    if-ne v1, v3, :cond_8

    invoke-virtual {v0, v2}, Landroid/view/ViewGroup;->getChildAt(I)Landroid/view/View;

    move-result-object v1

    invoke-virtual {v1, v7}, Landroid/view/View;->setBackgroundResource(I)V

    :cond_8
    invoke-virtual {v0, v2}, Landroid/view/ViewGroup;->getChildAt(I)Landroid/view/View;

    move-result-object v1

    invoke-virtual {v1}, Landroid/view/View;->getId()I

    move-result v1

    if-eq v1, v10, :cond_9

    invoke-virtual {v0, v2}, Landroid/view/ViewGroup;->getChildAt(I)Landroid/view/View;

    move-result-object v1

    invoke-virtual {v1}, Landroid/view/View;->getId()I

    move-result v1

    const v3, 0x7f0c0162

    if-ne v1, v3, :cond_a

    :cond_9
    invoke-virtual {v0, v2}, Landroid/view/ViewGroup;->getChildAt(I)Landroid/view/View;

    move-result-object v1

    invoke-virtual {v1, v9}, Landroid/view/View;->setVisibility(I)V

    :cond_a
    add-int/lit8 v2, v2, 0x1

    goto :goto_4

    .line 1883
    :cond_b
    return-void
.end method

.method public static a(Landroid/app/Activity;)V
    .locals 3

    .prologue
    .line 3070
    invoke-virtual {p0}, Landroid/app/Activity;->getWindow()Landroid/view/Window;

    move-result-object v0

    .line 3071
    const/4 v1, 0x2

    invoke-virtual {v0, v1}, Landroid/view/Window;->addFlags(I)V

    .line 3072
    invoke-virtual {v0}, Landroid/view/Window;->getAttributes()Landroid/view/WindowManager$LayoutParams;

    move-result-object v1

    .line 3074
    const/high16 v2, 0x3f800000    # 1.0f

    iput v2, v1, Landroid/view/WindowManager$LayoutParams;->alpha:F

    .line 3075
    const v2, 0x3f19999a    # 0.6f

    iput v2, v1, Landroid/view/WindowManager$LayoutParams;->dimAmount:F

    .line 3077
    invoke-virtual {v0, v1}, Landroid/view/Window;->setAttributes(Landroid/view/WindowManager$LayoutParams;)V

    .line 3078
    return-void
.end method

.method private static a(Landroid/widget/EditText;)V
    .locals 4

    .prologue
    const/4 v0, 0x1

    .line 1215
    invoke-virtual {p0, v0}, Landroid/widget/EditText;->setInputType(I)V

    .line 1216
    new-array v0, v0, [Landroid/text/InputFilter;

    const/4 v1, 0x0

    new-instance v2, Landroid/text/InputFilter$LengthFilter;

    const/16 v3, 0xc8

    invoke-direct {v2, v3}, Landroid/text/InputFilter$LengthFilter;-><init>(I)V

    aput-object v2, v0, v1

    invoke-virtual {p0, v0}, Landroid/widget/EditText;->setFilters([Landroid/text/InputFilter;)V

    .line 1217
    return-void
.end method

.method protected static a(Landroid/widget/TextView;)V
    .locals 1

    .prologue
    .line 2273
    if-eqz p0, :cond_0

    .line 2275
    invoke-virtual {p0}, Landroid/widget/TextView;->getTextColors()Landroid/content/res/ColorStateList;

    move-result-object v0

    .line 2276
    if-eqz v0, :cond_0

    .line 2278
    invoke-virtual {v0}, Landroid/content/res/ColorStateList;->getDefaultColor()I

    move-result v0

    invoke-virtual {p0, v0}, Landroid/widget/TextView;->setTextColor(I)V

    .line 2281
    :cond_0
    return-void
.end method

.method private a(Z)V
    .locals 5

    .prologue
    const/16 v4, 0xd

    const/16 v3, 0x8

    const/4 v2, 0x2

    .line 1063
    iget-object v0, p0, Lcom/osp/app/util/BaseActivity;->z:Lcom/osp/app/util/am;

    iget v0, v0, Lcom/osp/app/util/am;->a:I

    const/4 v1, -0x1

    if-eq v0, v1, :cond_0

    if-nez p1, :cond_1

    .line 1066
    :cond_0
    iget v0, p0, Lcom/osp/app/util/BaseActivity;->D:I

    if-ne v0, v2, :cond_3

    .line 1068
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "["

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "]  THEME_EXCEPTIONAL_TYPE_TRANSLUCENT"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/osp/app/util/aq;->c(Ljava/lang/String;)V

    .line 1069
    if-eqz p1, :cond_1

    .line 1071
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0xb

    if-ge v0, v1, :cond_2

    .line 1073
    const v0, 0x103000f

    invoke-virtual {p0, v0}, Lcom/osp/app/util/BaseActivity;->setTheme(I)V

    .line 1074
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "["

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "] setTheme=[android.R.style.Theme_Translucent]"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/osp/app/util/aq;->c(Ljava/lang/String;)V

    .line 1146
    :cond_1
    :goto_0
    return-void

    .line 1077
    :cond_2
    invoke-virtual {p0}, Lcom/osp/app/util/BaseActivity;->k()V

    goto :goto_0

    .line 1080
    :cond_3
    iget v0, p0, Lcom/osp/app/util/BaseActivity;->D:I

    const/4 v1, 0x3

    if-ne v0, v1, :cond_4

    .line 1082
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "["

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "]  THEME_EXCEPTIONAL_TYPE_TRANSLUCENT_NOTITLE"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/osp/app/util/aq;->c(Ljava/lang/String;)V

    .line 1083
    const v0, 0x1030010

    invoke-virtual {p0, v0}, Lcom/osp/app/util/BaseActivity;->setTheme(I)V

    goto :goto_0

    .line 1086
    :cond_4
    iget v0, p0, Lcom/osp/app/util/BaseActivity;->D:I

    if-nez v0, :cond_d

    .line 1088
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "["

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "]  THEME_EXCEPTIONAL_TYPE_NONE"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/osp/app/util/aq;->c(Ljava/lang/String;)V

    .line 1089
    if-eqz p1, :cond_5

    .line 1091
    invoke-virtual {p0}, Lcom/osp/app/util/BaseActivity;->h()Z

    move-result v0

    if-eqz v0, :cond_8

    .line 1093
    iget-object v0, p0, Lcom/osp/app/util/BaseActivity;->z:Lcom/osp/app/util/am;

    invoke-virtual {v0, v4}, Lcom/osp/app/util/am;->a(I)Z

    .line 1098
    :goto_1
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "["

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "] setTheme=["

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/osp/app/util/BaseActivity;->z:Lcom/osp/app/util/am;

    iget-object v1, p0, Lcom/osp/app/util/BaseActivity;->z:Lcom/osp/app/util/am;

    iget v1, v1, Lcom/osp/app/util/am;->a:I

    invoke-static {v1}, Lcom/osp/app/util/am;->c(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " ]"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/osp/app/util/aq;->c(Ljava/lang/String;)V

    .line 1101
    :cond_5
    invoke-static {}, Lcom/osp/app/signin/SamsungService;->a()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1103
    invoke-static {p0}, Lcom/osp/app/util/r;->g(Landroid/content/Context;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 1108
    invoke-static {p0}, Lcom/osp/app/util/r;->m(Landroid/content/Context;)Z

    move-result v0

    if-nez v0, :cond_7

    invoke-static {}, Lcom/osp/app/signin/SamsungService;->a()Z

    move-result v0

    if-eqz v0, :cond_9

    sget-object v0, Lcom/osp/app/util/u;->h:[Ljava/lang/String;

    invoke-static {v0}, Lcom/osp/app/util/u;->a([Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_6

    invoke-static {}, Lcom/osp/app/util/r;->m()Z

    move-result v0

    if-nez v0, :cond_6

    invoke-static {p0}, Lcom/osp/app/util/r;->k(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_9

    :cond_6
    const/4 v0, 0x1

    :goto_2
    if-eqz v0, :cond_b

    .line 1110
    :cond_7
    invoke-virtual {p0}, Lcom/osp/app/util/BaseActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v0

    iget v0, v0, Landroid/content/res/Configuration;->orientation:I

    if-ne v0, v2, :cond_a

    .line 1112
    invoke-virtual {p0}, Lcom/osp/app/util/BaseActivity;->getWindow()Landroid/view/Window;

    move-result-object v0

    invoke-static {p0}, Lcom/osp/app/util/an;->a(Lcom/osp/app/util/BaseActivity;)Lcom/osp/app/util/an;

    const v1, 0x7f0200c9

    invoke-static {v1}, Lcom/osp/app/util/an;->a(I)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/view/Window;->setBackgroundDrawableResource(I)V

    goto/16 :goto_0

    .line 1096
    :cond_8
    iget-object v0, p0, Lcom/osp/app/util/BaseActivity;->z:Lcom/osp/app/util/am;

    invoke-virtual {v0, v3}, Lcom/osp/app/util/am;->a(I)Z

    goto/16 :goto_1

    .line 1108
    :cond_9
    const/4 v0, 0x0

    goto :goto_2

    .line 1115
    :cond_a
    invoke-virtual {p0}, Lcom/osp/app/util/BaseActivity;->getWindow()Landroid/view/Window;

    move-result-object v0

    invoke-static {p0}, Lcom/osp/app/util/an;->a(Lcom/osp/app/util/BaseActivity;)Lcom/osp/app/util/an;

    const v1, 0x7f0200c8

    invoke-static {v1}, Lcom/osp/app/util/an;->a(I)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/view/Window;->setBackgroundDrawableResource(I)V

    goto/16 :goto_0

    .line 1117
    :cond_b
    invoke-virtual {p0}, Lcom/osp/app/util/BaseActivity;->h()Z

    move-result v0

    if-nez v0, :cond_1

    .line 1121
    invoke-virtual {p0}, Lcom/osp/app/util/BaseActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v0

    iget v0, v0, Landroid/content/res/Configuration;->orientation:I

    if-ne v0, v2, :cond_c

    .line 1123
    invoke-virtual {p0}, Lcom/osp/app/util/BaseActivity;->getWindow()Landroid/view/Window;

    move-result-object v0

    invoke-static {p0}, Lcom/osp/app/util/an;->a(Lcom/osp/app/util/BaseActivity;)Lcom/osp/app/util/an;

    const v1, 0x7f0200ec

    invoke-static {v1}, Lcom/osp/app/util/an;->a(I)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/view/Window;->setBackgroundDrawableResource(I)V

    goto/16 :goto_0

    .line 1126
    :cond_c
    invoke-virtual {p0}, Lcom/osp/app/util/BaseActivity;->getWindow()Landroid/view/Window;

    move-result-object v0

    invoke-static {p0}, Lcom/osp/app/util/an;->a(Lcom/osp/app/util/BaseActivity;)Lcom/osp/app/util/an;

    const v1, 0x7f0200eb

    invoke-static {v1}, Lcom/osp/app/util/an;->a(I)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/view/Window;->setBackgroundDrawableResource(I)V

    goto/16 :goto_0

    .line 1132
    :cond_d
    iget v0, p0, Lcom/osp/app/util/BaseActivity;->D:I

    const/4 v1, 0x5

    if-ne v0, v1, :cond_1

    .line 1134
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "["

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "]  THEME_EXCEPTIONAL_TYPE_NONE_FOR_ACTIVITY_INTERFACE"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/osp/app/util/aq;->c(Ljava/lang/String;)V

    .line 1135
    invoke-virtual {p0}, Lcom/osp/app/util/BaseActivity;->h()Z

    move-result v0

    if-eqz v0, :cond_e

    .line 1137
    iget-object v0, p0, Lcom/osp/app/util/BaseActivity;->z:Lcom/osp/app/util/am;

    invoke-virtual {v0, v4}, Lcom/osp/app/util/am;->a(I)Z

    .line 1142
    :goto_3
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "["

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "] setTheme=["

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/osp/app/util/BaseActivity;->z:Lcom/osp/app/util/am;

    iget-object v1, p0, Lcom/osp/app/util/BaseActivity;->z:Lcom/osp/app/util/am;

    iget v1, v1, Lcom/osp/app/util/am;->a:I

    invoke-static {v1}, Lcom/osp/app/util/am;->c(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " ]"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/osp/app/util/aq;->c(Ljava/lang/String;)V

    goto/16 :goto_0

    .line 1140
    :cond_e
    iget-object v0, p0, Lcom/osp/app/util/BaseActivity;->z:Lcom/osp/app/util/am;

    invoke-virtual {v0, v3}, Lcom/osp/app/util/am;->a(I)Z

    goto :goto_3
.end method

.method private static a(Landroid/content/Intent;)Z
    .locals 3

    .prologue
    .line 1440
    invoke-virtual {p0}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    .line 1441
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_0

    const-string v1, "android.intent.action."

    invoke-virtual {v0, v1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 1443
    const-string v1, "android.intent.action."

    const-string v2, "com.samsung.account."

    invoke-virtual {v0, v1, v2}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 1444
    const/4 v0, 0x1

    .line 1446
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private b(Landroid/content/Intent;)V
    .locals 3

    .prologue
    .line 1455
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    invoke-static {p1}, Lcom/osp/app/util/aq;->a(Landroid/content/Intent;)Ljava/lang/String;

    move-result-object v0

    .line 1456
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "["

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "] sendBroadcast Intent=["

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " ]"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/osp/app/util/aq;->c(Ljava/lang/String;)V

    .line 1458
    return-void
.end method

.method private b(Landroid/widget/EditText;)V
    .locals 4

    .prologue
    .line 1317
    const/16 v0, 0x81

    invoke-virtual {p1, v0}, Landroid/widget/EditText;->setInputType(I)V

    .line 1318
    const/4 v0, 0x1

    new-array v0, v0, [Landroid/text/InputFilter;

    const/4 v1, 0x0

    new-instance v2, Landroid/text/InputFilter$LengthFilter;

    const/16 v3, 0xf

    invoke-direct {v2, v3}, Landroid/text/InputFilter$LengthFilter;-><init>(I)V

    aput-object v2, v0, v1

    invoke-virtual {p1, v0}, Landroid/widget/EditText;->setFilters([Landroid/text/InputFilter;)V

    .line 1319
    new-instance v0, Landroid/text/method/PasswordTransformationMethod;

    invoke-direct {v0}, Landroid/text/method/PasswordTransformationMethod;-><init>()V

    invoke-virtual {p1, v0}, Landroid/widget/EditText;->setTransformationMethod(Landroid/text/method/TransformationMethod;)V

    .line 1320
    const v0, 0x7f090170

    invoke-virtual {p0, v0}, Lcom/osp/app/util/BaseActivity;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/widget/EditText;->setHint(Ljava/lang/CharSequence;)V

    .line 1321
    return-void
.end method

.method private b(Z)V
    .locals 6

    .prologue
    const/4 v5, 0x0

    const v4, 0x7f0c0197

    const v3, 0x7f0c018e

    .line 1764
    invoke-static {}, Lcom/osp/device/b;->b()Lcom/osp/device/b;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/osp/device/b;->l(Landroid/content/Context;)Z

    move-result v0

    .line 1765
    if-eqz p1, :cond_3

    .line 1767
    if-eqz v0, :cond_2

    .line 1769
    invoke-virtual {p0}, Lcom/osp/app/util/BaseActivity;->getMenuInflater()Landroid/view/MenuInflater;

    move-result-object v0

    const v1, 0x7f0b000c

    iget-object v2, p0, Lcom/osp/app/util/BaseActivity;->x:Landroid/view/Menu;

    invoke-virtual {v0, v1, v2}, Landroid/view/MenuInflater;->inflate(ILandroid/view/Menu;)V

    .line 1784
    :goto_0
    invoke-static {}, Lcom/osp/device/b;->b()Lcom/osp/device/b;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/osp/device/b;->i(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1786
    invoke-direct {p0}, Lcom/osp/app/util/BaseActivity;->c()Z

    move-result v0

    if-eqz v0, :cond_5

    .line 1788
    iget-object v0, p0, Lcom/osp/app/util/BaseActivity;->x:Landroid/view/Menu;

    invoke-interface {v0, v4}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v0

    invoke-virtual {p0}, Lcom/osp/app/util/BaseActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f020047

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    invoke-interface {v0, v1}, Landroid/view/MenuItem;->setIcon(Landroid/graphics/drawable/Drawable;)Landroid/view/MenuItem;

    .line 1789
    iget-object v0, p0, Lcom/osp/app/util/BaseActivity;->x:Landroid/view/Menu;

    invoke-interface {v0, v3}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v0

    invoke-virtual {p0}, Lcom/osp/app/util/BaseActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f020041

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    invoke-interface {v0, v1}, Landroid/view/MenuItem;->setIcon(Landroid/graphics/drawable/Drawable;)Landroid/view/MenuItem;

    .line 1798
    :cond_0
    :goto_1
    invoke-static {p0}, Lcom/osp/app/util/r;->k(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1800
    iget-object v0, p0, Lcom/osp/app/util/BaseActivity;->x:Landroid/view/Menu;

    invoke-interface {v0, v4}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v0

    invoke-interface {v0, v5}, Landroid/view/MenuItem;->setIcon(Landroid/graphics/drawable/Drawable;)Landroid/view/MenuItem;

    .line 1801
    iget-object v0, p0, Lcom/osp/app/util/BaseActivity;->x:Landroid/view/Menu;

    invoke-interface {v0, v3}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v0

    invoke-interface {v0, v5}, Landroid/view/MenuItem;->setIcon(Landroid/graphics/drawable/Drawable;)Landroid/view/MenuItem;

    .line 1804
    :cond_1
    return-void

    .line 1772
    :cond_2
    invoke-virtual {p0}, Lcom/osp/app/util/BaseActivity;->getMenuInflater()Landroid/view/MenuInflater;

    move-result-object v0

    const v1, 0x7f0b000d

    iget-object v2, p0, Lcom/osp/app/util/BaseActivity;->x:Landroid/view/Menu;

    invoke-virtual {v0, v1, v2}, Landroid/view/MenuInflater;->inflate(ILandroid/view/Menu;)V

    goto :goto_0

    .line 1776
    :cond_3
    if-eqz v0, :cond_4

    .line 1778
    invoke-virtual {p0}, Lcom/osp/app/util/BaseActivity;->getMenuInflater()Landroid/view/MenuInflater;

    move-result-object v0

    const v1, 0x7f0b0012

    iget-object v2, p0, Lcom/osp/app/util/BaseActivity;->x:Landroid/view/Menu;

    invoke-virtual {v0, v1, v2}, Landroid/view/MenuInflater;->inflate(ILandroid/view/Menu;)V

    goto :goto_0

    .line 1781
    :cond_4
    invoke-virtual {p0}, Lcom/osp/app/util/BaseActivity;->getMenuInflater()Landroid/view/MenuInflater;

    move-result-object v0

    const v1, 0x7f0b0013

    iget-object v2, p0, Lcom/osp/app/util/BaseActivity;->x:Landroid/view/Menu;

    invoke-virtual {v0, v1, v2}, Landroid/view/MenuInflater;->inflate(ILandroid/view/Menu;)V

    goto :goto_0

    .line 1792
    :cond_5
    iget-object v0, p0, Lcom/osp/app/util/BaseActivity;->x:Landroid/view/Menu;

    invoke-interface {v0, v4}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v0

    invoke-virtual {p0}, Lcom/osp/app/util/BaseActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f020048

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    invoke-interface {v0, v1}, Landroid/view/MenuItem;->setIcon(Landroid/graphics/drawable/Drawable;)Landroid/view/MenuItem;

    .line 1793
    iget-object v0, p0, Lcom/osp/app/util/BaseActivity;->x:Landroid/view/Menu;

    invoke-interface {v0, v3}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v0

    invoke-virtual {p0}, Lcom/osp/app/util/BaseActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f020042

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    invoke-interface {v0, v1}, Landroid/view/MenuItem;->setIcon(Landroid/graphics/drawable/Drawable;)Landroid/view/MenuItem;

    goto :goto_1
.end method

.method private c()Z
    .locals 6

    .prologue
    const/4 v1, 0x1

    const/4 v0, 0x0

    .line 847
    .line 848
    invoke-static {p0}, Lcom/osp/app/util/r;->a(Landroid/app/Activity;)Z

    move-result v3

    .line 850
    sget-object v2, Lcom/osp/app/util/h;->a:Lcom/osp/app/util/g;

    invoke-static {}, Lcom/osp/app/util/ah;->a()Lcom/osp/app/util/ah;

    move-result-object v2

    invoke-virtual {v2}, Lcom/osp/app/util/ah;->k()Ljava/lang/String;

    move-result-object v2

    const-string v4, "dark"

    invoke-virtual {v4, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_3

    move v2, v1

    :goto_0
    if-eq v2, v3, :cond_0

    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v3, "CAU"

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "change test actionbar theme - isblack?"

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 852
    :cond_0
    invoke-virtual {p0}, Lcom/osp/app/util/BaseActivity;->h()Z

    move-result v3

    if-nez v3, :cond_1

    if-eqz v2, :cond_2

    :cond_1
    move v0, v1

    .line 856
    :cond_2
    return v0

    .line 850
    :cond_3
    const-string v4, "light"

    invoke-virtual {v4, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_4

    move v2, v0

    goto :goto_0

    :cond_4
    move v2, v3

    goto :goto_0
.end method

.method protected static f(Ljava/lang/String;)Z
    .locals 3

    .prologue
    .line 1960
    const/4 v0, 0x0

    .line 1967
    const-string v1, "470"

    invoke-virtual {v1, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    const-string v1, "402"

    invoke-virtual {v1, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    const-string v1, "636"

    invoke-virtual {v1, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    const-string v1, "542"

    invoke-virtual {v1, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    const-string v1, "282"

    invoke-virtual {v1, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    const-string v1, "404"

    invoke-virtual {v1, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    const-string v1, "432"

    invoke-virtual {v1, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    const-string v1, "472"

    invoke-virtual {v1, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    const-string v1, "429"

    invoke-virtual {v1, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    const-string v1, "420"

    invoke-virtual {v1, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    const-string v1, "413"

    invoke-virtual {v1, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    const-string v1, "424"

    invoke-virtual {v1, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    const-string v1, "430"

    invoke-virtual {v1, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    const-string v1, "431"

    invoke-virtual {v1, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 1970
    :cond_0
    const/4 v0, 0x1

    .line 1973
    :cond_1
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "CountryListDialogManager::exculdeNativeCountryName mcc : "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " exclude : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;)V

    .line 1975
    return v0
.end method


# virtual methods
.method protected final a(Ljava/lang/String;Landroid/widget/EditText;)Landroid/widget/EditText;
    .locals 7

    .prologue
    const/16 v6, 0x32

    const/4 v5, 0x0

    const/16 v4, 0x2000

    const/4 v3, 0x1

    const v2, 0x7f090017

    .line 483
    if-nez p1, :cond_0

    .line 485
    const-string v0, "ViewComponentAttributesetEditViewComponent::"

    const-string v1, "fieldTitle is null"

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 486
    const/4 p2, 0x0

    .line 584
    :goto_0
    return-object p2

    .line 494
    :cond_0
    invoke-virtual {p2, v3}, Landroid/widget/EditText;->setEnabled(Z)V

    .line 495
    invoke-virtual {p2, v3}, Landroid/widget/EditText;->setFocusable(Z)V

    .line 496
    invoke-virtual {p2, v3}, Landroid/widget/EditText;->setClickable(Z)V

    .line 498
    const v0, 0x7f090039

    invoke-virtual {p0, v0}, Lcom/osp/app/util/BaseActivity;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 500
    invoke-static {p2}, Lcom/osp/app/util/BaseActivity;->a(Landroid/widget/EditText;)V

    .line 501
    const v0, 0x12000

    invoke-virtual {p2, v0}, Landroid/widget/EditText;->setInputType(I)V

    .line 502
    const v0, 0x7f090039

    invoke-virtual {p2, v0}, Landroid/widget/EditText;->setHint(I)V

    .line 503
    const v0, 0x7f090039

    invoke-virtual {p0, v0}, Lcom/osp/app/util/BaseActivity;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p2, v0}, Landroid/widget/EditText;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 504
    new-array v0, v3, [Landroid/text/InputFilter;

    new-instance v1, Landroid/text/InputFilter$LengthFilter;

    invoke-direct {v1, v6}, Landroid/text/InputFilter$LengthFilter;-><init>(I)V

    aput-object v1, v0, v5

    invoke-virtual {p2, v0}, Landroid/widget/EditText;->setFilters([Landroid/text/InputFilter;)V

    goto :goto_0

    .line 505
    :cond_1
    const v0, 0x7f09002a

    invoke-virtual {p0, v0}, Lcom/osp/app/util/BaseActivity;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 507
    invoke-static {p2}, Lcom/osp/app/util/BaseActivity;->a(Landroid/widget/EditText;)V

    .line 508
    const v0, 0x12000

    invoke-virtual {p2, v0}, Landroid/widget/EditText;->setInputType(I)V

    .line 509
    const v0, 0x7f09002a

    invoke-virtual {p2, v0}, Landroid/widget/EditText;->setHint(I)V

    .line 510
    const v0, 0x7f09002a

    invoke-virtual {p0, v0}, Lcom/osp/app/util/BaseActivity;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p2, v0}, Landroid/widget/EditText;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 511
    new-array v0, v3, [Landroid/text/InputFilter;

    new-instance v1, Landroid/text/InputFilter$LengthFilter;

    invoke-direct {v1, v6}, Landroid/text/InputFilter$LengthFilter;-><init>(I)V

    aput-object v1, v0, v5

    invoke-virtual {p2, v0}, Landroid/widget/EditText;->setFilters([Landroid/text/InputFilter;)V

    goto :goto_0

    .line 512
    :cond_2
    const v0, 0x7f09003d

    invoke-virtual {p0, v0}, Lcom/osp/app/util/BaseActivity;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 514
    invoke-static {p2}, Lcom/osp/app/util/BaseActivity;->a(Landroid/widget/EditText;)V

    .line 515
    const v0, 0x7f09003d

    invoke-virtual {p2, v0}, Landroid/widget/EditText;->setHint(I)V

    .line 516
    const v0, 0x7f09003d

    invoke-virtual {p0, v0}, Lcom/osp/app/util/BaseActivity;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p2, v0}, Landroid/widget/EditText;->setContentDescription(Ljava/lang/CharSequence;)V

    goto/16 :goto_0

    .line 517
    :cond_3
    const v0, 0x7f09001a

    invoke-virtual {p0, v0}, Lcom/osp/app/util/BaseActivity;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 519
    invoke-static {p2}, Lcom/osp/app/util/BaseActivity;->a(Landroid/widget/EditText;)V

    .line 520
    invoke-virtual {p2, v4}, Landroid/widget/EditText;->setInputType(I)V

    .line 521
    const v0, 0x7f09001a

    invoke-virtual {p2, v0}, Landroid/widget/EditText;->setHint(I)V

    .line 522
    const v0, 0x7f09001a

    invoke-virtual {p0, v0}, Lcom/osp/app/util/BaseActivity;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p2, v0}, Landroid/widget/EditText;->setContentDescription(Ljava/lang/CharSequence;)V

    goto/16 :goto_0

    .line 523
    :cond_4
    invoke-virtual {p0, v2}, Lcom/osp/app/util/BaseActivity;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_5

    .line 525
    invoke-static {p2}, Lcom/osp/app/util/BaseActivity;->a(Landroid/widget/EditText;)V

    .line 526
    invoke-virtual {p2, v4}, Landroid/widget/EditText;->setInputType(I)V

    .line 527
    invoke-virtual {p2, v2}, Landroid/widget/EditText;->setHint(I)V

    .line 528
    invoke-virtual {p0, v2}, Lcom/osp/app/util/BaseActivity;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p2, v0}, Landroid/widget/EditText;->setContentDescription(Ljava/lang/CharSequence;)V

    goto/16 :goto_0

    .line 529
    :cond_5
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0, v2}, Lcom/osp/app/util/BaseActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "2"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_6

    .line 531
    invoke-static {p2}, Lcom/osp/app/util/BaseActivity;->a(Landroid/widget/EditText;)V

    .line 532
    invoke-virtual {p2, v4}, Landroid/widget/EditText;->setInputType(I)V

    .line 533
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0, v2}, Lcom/osp/app/util/BaseActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "2"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p2, v0}, Landroid/widget/EditText;->setHint(Ljava/lang/CharSequence;)V

    .line 534
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0, v2}, Lcom/osp/app/util/BaseActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "2"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p2, v0}, Landroid/widget/EditText;->setContentDescription(Ljava/lang/CharSequence;)V

    goto/16 :goto_0

    .line 535
    :cond_6
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0, v2}, Lcom/osp/app/util/BaseActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "3"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_7

    .line 537
    invoke-static {p2}, Lcom/osp/app/util/BaseActivity;->a(Landroid/widget/EditText;)V

    .line 538
    invoke-virtual {p2, v4}, Landroid/widget/EditText;->setInputType(I)V

    .line 539
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0, v2}, Lcom/osp/app/util/BaseActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "3"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p2, v0}, Landroid/widget/EditText;->setHint(Ljava/lang/CharSequence;)V

    .line 540
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0, v2}, Lcom/osp/app/util/BaseActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "3"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p2, v0}, Landroid/widget/EditText;->setContentDescription(Ljava/lang/CharSequence;)V

    goto/16 :goto_0

    .line 541
    :cond_7
    const v0, 0x7f09004b

    invoke-virtual {p0, v0}, Lcom/osp/app/util/BaseActivity;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_8

    .line 543
    invoke-static {p2}, Lcom/osp/app/util/BaseActivity;->a(Landroid/widget/EditText;)V

    .line 544
    invoke-virtual {p2, v4}, Landroid/widget/EditText;->setInputType(I)V

    .line 545
    const v0, 0x7f09004b

    invoke-virtual {p2, v0}, Landroid/widget/EditText;->setHint(I)V

    .line 546
    const v0, 0x7f09004b

    invoke-virtual {p0, v0}, Lcom/osp/app/util/BaseActivity;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p2, v0}, Landroid/widget/EditText;->setContentDescription(Ljava/lang/CharSequence;)V

    goto/16 :goto_0

    .line 547
    :cond_8
    const v0, 0x7f090049

    invoke-virtual {p0, v0}, Lcom/osp/app/util/BaseActivity;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_9

    .line 549
    invoke-static {p2}, Lcom/osp/app/util/BaseActivity;->a(Landroid/widget/EditText;)V

    .line 550
    const/4 v0, 0x2

    invoke-virtual {p2, v0}, Landroid/widget/EditText;->setInputType(I)V

    .line 551
    new-array v0, v3, [Landroid/text/InputFilter;

    new-instance v1, Landroid/text/InputFilter$LengthFilter;

    const/4 v2, 0x5

    invoke-direct {v1, v2}, Landroid/text/InputFilter$LengthFilter;-><init>(I)V

    aput-object v1, v0, v5

    invoke-virtual {p2, v0}, Landroid/widget/EditText;->setFilters([Landroid/text/InputFilter;)V

    .line 552
    const v0, 0x7f090049

    invoke-virtual {p2, v0}, Landroid/widget/EditText;->setHint(I)V

    .line 553
    const v0, 0x7f090049

    invoke-virtual {p0, v0}, Lcom/osp/app/util/BaseActivity;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p2, v0}, Landroid/widget/EditText;->setContentDescription(Ljava/lang/CharSequence;)V

    goto/16 :goto_0

    .line 554
    :cond_9
    const v0, 0x7f090048

    invoke-virtual {p0, v0}, Lcom/osp/app/util/BaseActivity;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_a

    .line 556
    const/4 v0, 0x3

    invoke-virtual {p2, v0}, Landroid/widget/EditText;->setInputType(I)V

    new-array v0, v3, [Landroid/text/InputFilter;

    new-instance v1, Landroid/text/InputFilter$LengthFilter;

    invoke-direct {v1, v6}, Landroid/text/InputFilter$LengthFilter;-><init>(I)V

    aput-object v1, v0, v5

    invoke-virtual {p2, v0}, Landroid/widget/EditText;->setFilters([Landroid/text/InputFilter;)V

    .line 557
    const v0, 0x7f090048

    invoke-virtual {p2, v0}, Landroid/widget/EditText;->setHint(I)V

    .line 558
    const v0, 0x7f090048

    invoke-virtual {p0, v0}, Lcom/osp/app/util/BaseActivity;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p2, v0}, Landroid/widget/EditText;->setContentDescription(Ljava/lang/CharSequence;)V

    goto/16 :goto_0

    .line 559
    :cond_a
    const v0, 0x7f090040

    invoke-virtual {p0, v0}, Lcom/osp/app/util/BaseActivity;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_b

    .line 561
    invoke-static {p2}, Lcom/osp/app/util/BaseActivity;->a(Landroid/widget/EditText;)V

    .line 562
    invoke-virtual {p2, v4}, Landroid/widget/EditText;->setInputType(I)V

    .line 563
    const v0, 0x7f090040

    invoke-virtual {p2, v0}, Landroid/widget/EditText;->setHint(I)V

    .line 564
    const v0, 0x7f090040

    invoke-virtual {p0, v0}, Lcom/osp/app/util/BaseActivity;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p2, v0}, Landroid/widget/EditText;->setContentDescription(Ljava/lang/CharSequence;)V

    goto/16 :goto_0

    .line 565
    :cond_b
    const v0, 0x7f090023

    invoke-virtual {p0, v0}, Lcom/osp/app/util/BaseActivity;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_c

    .line 567
    const/16 v0, 0x21

    invoke-virtual {p2, v0}, Landroid/widget/EditText;->setInputType(I)V

    new-array v0, v3, [Landroid/text/InputFilter;

    new-instance v1, Landroid/text/InputFilter$LengthFilter;

    invoke-direct {v1, v6}, Landroid/text/InputFilter$LengthFilter;-><init>(I)V

    aput-object v1, v0, v5

    invoke-virtual {p2, v0}, Landroid/widget/EditText;->setFilters([Landroid/text/InputFilter;)V

    const v0, 0x7f090025

    invoke-virtual {p0, v0}, Lcom/osp/app/util/BaseActivity;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p2, v0}, Landroid/widget/EditText;->setHint(Ljava/lang/CharSequence;)V

    .line 568
    const v0, 0x7f090112

    invoke-virtual {p2, v0}, Landroid/widget/EditText;->setHint(I)V

    .line 569
    const v0, 0x7f090112

    invoke-virtual {p0, v0}, Lcom/osp/app/util/BaseActivity;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p2, v0}, Landroid/widget/EditText;->setContentDescription(Ljava/lang/CharSequence;)V

    goto/16 :goto_0

    .line 570
    :cond_c
    const v0, 0x7f090045

    invoke-virtual {p0, v0}, Lcom/osp/app/util/BaseActivity;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_d

    .line 572
    invoke-direct {p0, p2}, Lcom/osp/app/util/BaseActivity;->b(Landroid/widget/EditText;)V

    goto/16 :goto_0

    .line 574
    :cond_d
    const v0, 0x7f09001b

    invoke-virtual {p0, v0}, Lcom/osp/app/util/BaseActivity;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_e

    .line 576
    invoke-direct {p0, p2}, Lcom/osp/app/util/BaseActivity;->b(Landroid/widget/EditText;)V

    goto/16 :goto_0

    .line 580
    :cond_e
    const-string v0, "ViewComponentAttributesetEditViewComponent::"

    const-string v1, "Cannot find same fieldTitle"

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 581
    invoke-static {p2}, Lcom/osp/app/util/BaseActivity;->a(Landroid/widget/EditText;)V

    goto/16 :goto_0
.end method

.method protected abstract a()V
.end method

.method public final a(ILandroid/content/DialogInterface$OnDismissListener;)V
    .locals 1

    .prologue
    .line 2362
    const/4 v0, 0x0

    invoke-virtual {p0, p1, p2, v0}, Lcom/osp/app/util/BaseActivity;->a(ILandroid/content/DialogInterface$OnDismissListener;Landroid/content/DialogInterface$OnCancelListener;)V

    .line 2363
    return-void
.end method

.method public final a(ILandroid/content/DialogInterface$OnDismissListener;Landroid/content/DialogInterface$OnCancelListener;)V
    .locals 1

    .prologue
    .line 2399
    iget-object v0, p0, Lcom/osp/app/util/BaseActivity;->r:Lcom/msc/sa/c/f;

    invoke-static {p0, p1, v0, p2, p3}, Lcom/msc/sa/c/d;->a(Landroid/app/Activity;ILcom/msc/sa/c/f;Landroid/content/DialogInterface$OnDismissListener;Landroid/content/DialogInterface$OnCancelListener;)Lcom/msc/sa/c/f;

    move-result-object v0

    iput-object v0, p0, Lcom/osp/app/util/BaseActivity;->r:Lcom/msc/sa/c/f;

    .line 2401
    return-void
.end method

.method public final a(ILandroid/content/Intent;)V
    .locals 4

    .prologue
    const/16 v3, 0x5d

    .line 1939
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "["

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 1940
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " setResultWithLog resultCode=["

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/osp/app/util/aq;->c(Ljava/lang/String;)V

    .line 1942
    if-eqz p2, :cond_0

    const-string v1, "error_code"

    invoke-virtual {p2, v1}, Landroid/content/Intent;->hasExtra(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 1944
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " errorCode=["

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "error_code"

    invoke-virtual {p2, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "], errorMsg["

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "error_message"

    invoke-virtual {p2, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/osp/app/util/aq;->c(Ljava/lang/String;)V

    .line 1949
    :cond_0
    invoke-virtual {p0, p1, p2}, Lcom/osp/app/util/BaseActivity;->setResult(ILandroid/content/Intent;)V

    .line 1950
    return-void
.end method

.method public final a(IZ)V
    .locals 4

    .prologue
    const/4 v3, -0x1

    .line 2694
    if-eqz p2, :cond_0

    .line 2696
    const v0, 0x7f030040

    invoke-virtual {p0, v0}, Lcom/osp/app/util/BaseActivity;->setContentView(I)V

    .line 2698
    const v0, 0x7f0c0160

    invoke-virtual {p0, v0}, Lcom/osp/app/util/BaseActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    .line 2699
    const-string v1, "layout_inflater"

    invoke-virtual {p0, v1}, Lcom/osp/app/util/BaseActivity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/view/LayoutInflater;

    .line 2700
    const/4 v2, 0x0

    invoke-virtual {v1, p1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v1

    .line 2701
    new-instance v2, Landroid/view/ViewGroup$LayoutParams;

    invoke-direct {v2, v3, v3}, Landroid/view/ViewGroup$LayoutParams;-><init>(II)V

    invoke-virtual {v0, v1, v2}, Landroid/view/ViewGroup;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    .line 2703
    invoke-virtual {p0}, Lcom/osp/app/util/BaseActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v0

    iget v0, v0, Landroid/content/res/Configuration;->orientation:I

    invoke-direct {p0, v0}, Lcom/osp/app/util/BaseActivity;->a(I)V

    .line 2708
    :goto_0
    return-void

    .line 2706
    :cond_0
    invoke-virtual {p0, p1}, Lcom/osp/app/util/BaseActivity;->setContentView(I)V

    goto :goto_0
.end method

.method public final a(Landroid/content/Intent;J)V
    .locals 2

    .prologue
    .line 1436
    invoke-static {}, Lcom/osp/app/util/e;->a()Lcom/osp/app/util/e;

    move-result-object v0

    invoke-virtual {v0, p0, p1, p2, p3}, Lcom/osp/app/util/e;->a(Landroid/content/Context;Landroid/content/Intent;J)V

    .line 1437
    return-void
.end method

.method protected final a(Landroid/view/View;)V
    .locals 3

    .prologue
    const/4 v2, -0x1

    .line 2752
    if-eqz p1, :cond_0

    .line 2754
    invoke-virtual {p0}, Lcom/osp/app/util/BaseActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v0

    iget v0, v0, Landroid/content/res/Configuration;->orientation:I

    const/4 v1, 0x2

    if-ne v0, v1, :cond_1

    .line 2756
    invoke-static {p0}, Lcom/osp/app/util/an;->a(Lcom/osp/app/util/BaseActivity;)Lcom/osp/app/util/an;

    const v0, 0x7f0200c9

    invoke-static {v0}, Lcom/osp/app/util/an;->a(I)I

    move-result v0

    invoke-virtual {p1, v0}, Landroid/view/View;->setBackgroundResource(I)V

    .line 2761
    :goto_0
    invoke-virtual {p0}, Lcom/osp/app/util/BaseActivity;->getWindow()Landroid/view/Window;

    move-result-object v0

    invoke-virtual {v0, v2, v2}, Landroid/view/Window;->setLayout(II)V

    .line 2763
    :cond_0
    return-void

    .line 2759
    :cond_1
    invoke-static {p0}, Lcom/osp/app/util/an;->a(Lcom/osp/app/util/BaseActivity;)Lcom/osp/app/util/an;

    const v0, 0x7f0200c8

    invoke-static {v0}, Lcom/osp/app/util/an;->a(I)I

    move-result v0

    invoke-virtual {p1, v0}, Landroid/view/View;->setBackgroundResource(I)V

    goto :goto_0
.end method

.method protected final a(Landroid/content/ComponentName;)Z
    .locals 10

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 928
    .line 929
    const/4 v2, 0x2

    new-array v3, v2, [Ljava/lang/String;

    const-string v2, "com.sec.everglades"

    aput-object v2, v3, v1

    const-string v2, "com.android.settings"

    aput-object v2, v3, v0

    .line 931
    if-eqz p1, :cond_1

    .line 934
    invoke-virtual {p1}, Landroid/content/ComponentName;->getPackageName()Ljava/lang/String;

    move-result-object v4

    .line 936
    array-length v5, v3

    move v2, v1

    :goto_0
    if-ge v2, v5, :cond_1

    aget-object v6, v3, v2

    .line 938
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v7

    new-instance v8, Ljava/lang/StringBuilder;

    const-string v9, "calling package - "

    invoke-direct {v8, v9}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v8, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v7, v8}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 939
    invoke-virtual {v6, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_0

    .line 948
    :goto_1
    return v0

    .line 936
    :cond_0
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    :cond_1
    move v0, v1

    goto :goto_1
.end method

.method protected final a([Ljava/lang/String;)Z
    .locals 8

    .prologue
    const/16 v7, 0x2c

    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 963
    new-instance v0, Lcom/osp/app/util/t;

    invoke-direct {v0, p1}, Lcom/osp/app/util/t;-><init>([Ljava/lang/String;)V

    .line 965
    invoke-virtual {p0}, Lcom/osp/app/util/BaseActivity;->getIntent()Landroid/content/Intent;

    move-result-object v3

    invoke-virtual {v3}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v3

    invoke-virtual {v0, v3}, Lcom/osp/app/util/t;->a(Landroid/os/Bundle;)[Ljava/lang/String;

    move-result-object v3

    .line 967
    if-eqz v3, :cond_2

    array-length v0, v3

    if-lez v0, :cond_2

    .line 969
    new-instance v4, Ljava/lang/StringBuffer;

    const-string v0, ""

    invoke-direct {v4, v0}, Ljava/lang/StringBuffer;-><init>(Ljava/lang/String;)V

    .line 970
    array-length v5, v3

    move v0, v1

    :goto_0
    if-ge v0, v5, :cond_0

    aget-object v6, v3, v0

    .line 972
    invoke-virtual {v4, v6}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v6

    invoke-virtual {v6, v7}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    .line 970
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 975
    :cond_0
    invoke-virtual {v4}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v0

    .line 976
    invoke-virtual {v0, v7}, Ljava/lang/String;->lastIndexOf(I)I

    move-result v3

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v4

    add-int/lit8 v4, v4, -0x1

    if-ne v3, v4, :cond_1

    .line 978
    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v3

    add-int/lit8 v3, v3, -0x1

    invoke-virtual {v0, v1, v3}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v0

    .line 981
    :cond_1
    const-string v3, "Param [%s] must not be null"

    new-array v4, v2, [Ljava/lang/Object;

    aput-object v0, v4, v1

    invoke-static {v3, v4}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 983
    new-instance v3, Landroid/content/Intent;

    invoke-direct {v3}, Landroid/content/Intent;-><init>()V

    .line 984
    const-string v4, "error_code"

    const-string v5, "SAC_0101"

    invoke-virtual {v3, v4, v5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 985
    const-string v4, "error_message"

    invoke-virtual {v3, v4, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 986
    invoke-virtual {p0, v2, v3}, Lcom/osp/app/util/BaseActivity;->a(ILandroid/content/Intent;)V

    .line 987
    invoke-virtual {p0}, Lcom/osp/app/util/BaseActivity;->finish()V

    move v0, v1

    .line 1006
    :goto_1
    return v0

    .line 993
    :cond_2
    invoke-static {p0}, Lcom/osp/app/util/ad;->d(Landroid/content/Context;)Z

    move-result v0

    .line 995
    if-nez v0, :cond_3

    .line 997
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    .line 998
    const-string v3, "error_code"

    const-string v4, "SAC_0102"

    invoke-virtual {v0, v3, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 999
    const-string v3, "error_message"

    const-string v4, "Samsung account does not exist"

    invoke-virtual {v0, v3, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 1000
    invoke-virtual {p0, v2, v0}, Lcom/osp/app/util/BaseActivity;->a(ILandroid/content/Intent;)V

    .line 1001
    invoke-virtual {p0}, Lcom/osp/app/util/BaseActivity;->finish()V

    move v0, v1

    .line 1003
    goto :goto_1

    :cond_3
    move v0, v2

    .line 1006
    goto :goto_1
.end method

.method protected abstract b()V
.end method

.method public final b(I)V
    .locals 2

    .prologue
    .line 1928
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "["

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "] setResultWithLog resultCode=["

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const/16 v1, 0x5d

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/osp/app/util/aq;->c(Ljava/lang/String;)V

    .line 1929
    invoke-virtual {p0, p1}, Lcom/osp/app/util/BaseActivity;->setResult(I)V

    .line 1930
    return-void
.end method

.method public final c(I)V
    .locals 6

    .prologue
    const/4 v5, 0x1

    .line 1984
    invoke-static {p0}, Lcom/osp/app/util/ad;->a(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/16 v0, 0xe0

    if-ne p1, v0, :cond_4

    .line 1986
    :cond_0
    new-instance v2, Landroid/content/Intent;

    invoke-direct {v2}, Landroid/content/Intent;-><init>()V

    .line 1988
    const-string v0, "android.net.wifi.PICK_WIFI_NETWORK"

    .line 1994
    invoke-static {}, Lcom/osp/device/b;->b()Lcom/osp/device/b;

    move-result-object v1

    invoke-virtual {v1}, Lcom/osp/device/b;->l()I

    move-result v1

    const/16 v3, 0x13

    if-ge v1, v3, :cond_1

    invoke-static {}, Lcom/osp/app/util/r;->f()Z

    move-result v1

    if-nez v1, :cond_1

    sget-object v1, Lcom/osp/app/util/u;->j:[Ljava/lang/String;

    invoke-static {v1}, Lcom/osp/app/util/u;->a([Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_1

    sget-object v1, Lcom/osp/app/util/u;->h:[Ljava/lang/String;

    invoke-static {v1}, Lcom/osp/app/util/u;->a([Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_6

    :cond_1
    invoke-static {}, Lcom/osp/app/signin/SamsungService;->a()Z

    move-result v1

    if-eqz v1, :cond_6

    .line 1997
    const-string v0, "com.android.net.wifi.SECSETUP_WIFI_NETWORK"

    .line 1999
    invoke-virtual {v2, v0}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 2000
    invoke-virtual {p0}, Lcom/osp/app/util/BaseActivity;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v1

    .line 2001
    const/4 v3, 0x0

    invoke-virtual {v1, v2, v3}, Landroid/content/pm/PackageManager;->queryIntentActivities(Landroid/content/Intent;I)Ljava/util/List;

    move-result-object v1

    .line 2002
    if-eqz v1, :cond_2

    invoke-interface {v1}, Ljava/util/List;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_5

    .line 2004
    :cond_2
    const-string v0, "android.net.wifi.PICK_WIFI_NETWORK"

    move-object v1, v0

    .line 2011
    :goto_0
    const-string v0, "android.net.wifi.PICK_WIFI_NETWORK"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 2014
    const-string v0, "wifi"

    invoke-virtual {p0, v0}, Lcom/osp/app/util/BaseActivity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/net/wifi/WifiManager;

    .line 2016
    invoke-virtual {v0}, Landroid/net/wifi/WifiManager;->getWifiState()I

    move-result v3

    const/4 v4, 0x3

    if-ne v3, v4, :cond_7

    .line 2019
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v0, "wifi ok"

    invoke-static {v0}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;)V

    .line 2026
    :goto_1
    const-string v0, "only_access_points"

    invoke-virtual {v2, v0, v5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 2027
    const-string v0, "extra_prefs_show_button_bar"

    invoke-virtual {v2, v0, v5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 2028
    const-string v0, "extra_prefs_set_back_text"

    const v3, 0x7f090092

    invoke-virtual {p0, v3}, Lcom/osp/app/util/BaseActivity;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v0, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 2029
    const-string v0, "wifi_enable_next_on_connect"

    invoke-virtual {v2, v0, v5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 2033
    :cond_3
    invoke-virtual {v2, v1}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 2035
    invoke-virtual {p0, v2, p1}, Lcom/osp/app/util/BaseActivity;->startActivityForResult(Landroid/content/Intent;I)V

    .line 2039
    :cond_4
    return-void

    .line 2007
    :cond_5
    const-string v1, "extra_samsungaccount_for_wifisetupwizard"

    const-string v3, "true"

    invoke-virtual {v2, v1, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    :cond_6
    move-object v1, v0

    goto :goto_0

    .line 2023
    :cond_7
    invoke-virtual {v0, v5}, Landroid/net/wifi/WifiManager;->setWifiEnabled(Z)Z

    goto :goto_1
.end method

.method public final d(I)V
    .locals 0

    .prologue
    .line 2732
    iput p1, p0, Lcom/osp/app/util/BaseActivity;->D:I

    .line 2733
    return-void
.end method

.method protected final d(Ljava/lang/String;)V
    .locals 4

    .prologue
    const v3, 0x103013b

    const v2, 0x103013a

    .line 1272
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0xe

    if-ge v0, v1, :cond_0

    .line 1274
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/osp/app/util/BaseActivity;->requestWindowFeature(I)Z

    .line 1299
    :goto_0
    return-void

    .line 1277
    :cond_0
    const-string v0, "dark"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1279
    invoke-virtual {p0, v2}, Lcom/osp/app/util/BaseActivity;->setTheme(I)V

    .line 1280
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "["

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "] setTheme=[android.R.style.Theme_DeviceDefault_Panel]"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/osp/app/util/aq;->c(Ljava/lang/String;)V

    goto :goto_0

    .line 1281
    :cond_1
    const-string v0, "light"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 1283
    invoke-virtual {p0, v3}, Lcom/osp/app/util/BaseActivity;->setTheme(I)V

    .line 1284
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "["

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "] setTheme=[android.R.style.Theme_DeviceDefault_Light_Panel]"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/osp/app/util/aq;->c(Ljava/lang/String;)V

    goto :goto_0

    .line 1287
    :cond_2
    invoke-virtual {p0}, Lcom/osp/app/util/BaseActivity;->h()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 1289
    invoke-virtual {p0, v2}, Lcom/osp/app/util/BaseActivity;->setTheme(I)V

    .line 1290
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "["

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "] setTheme=[android.R.style.Theme_DeviceDefault_Panel]"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/osp/app/util/aq;->c(Ljava/lang/String;)V

    goto/16 :goto_0

    .line 1293
    :cond_3
    invoke-virtual {p0, v3}, Lcom/osp/app/util/BaseActivity;->setTheme(I)V

    .line 1294
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "["

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "] setTheme=[android.R.style.Theme_DeviceDefault_Light_Panel]"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/osp/app/util/aq;->c(Ljava/lang/String;)V

    goto/16 :goto_0
.end method

.method protected final e(Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 1891
    iput-object p1, p0, Lcom/osp/app/util/BaseActivity;->C:Ljava/lang/String;

    .line 1892
    return-void
.end method

.method public finish()V
    .locals 3

    .prologue
    .line 2713
    const-string v0, "input_method"

    invoke-virtual {p0, v0}, Lcom/osp/app/util/BaseActivity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/inputmethod/InputMethodManager;

    .line 2714
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Landroid/view/inputmethod/InputMethodManager;->isActive()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 2716
    invoke-virtual {p0}, Lcom/osp/app/util/BaseActivity;->getCurrentFocus()Landroid/view/View;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 2718
    invoke-virtual {p0}, Lcom/osp/app/util/BaseActivity;->getCurrentFocus()Landroid/view/View;

    move-result-object v1

    invoke-virtual {v1}, Landroid/view/View;->getWindowToken()Landroid/os/IBinder;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/view/inputmethod/InputMethodManager;->hideSoftInputFromWindow(Landroid/os/IBinder;I)Z

    .line 2722
    :cond_0
    invoke-super {p0}, Landroid/app/Activity;->finish()V

    .line 2723
    return-void
.end method

.method public final g(Ljava/lang/String;)Ljava/lang/String;
    .locals 5

    .prologue
    const/4 v0, 0x0

    .line 2229
    .line 2234
    :try_start_0
    invoke-virtual {p0}, Lcom/osp/app/util/BaseActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    .line 2235
    invoke-virtual {v1}, Landroid/content/res/Resources;->getAssets()Landroid/content/res/AssetManager;

    move-result-object v1

    .line 2236
    invoke-virtual {v1, p1}, Landroid/content/res/AssetManager;->open(Ljava/lang/String;)Ljava/io/InputStream;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_4
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v2

    .line 2237
    :try_start_1
    new-instance v1, Ljava/io/BufferedReader;

    new-instance v3, Ljava/io/InputStreamReader;

    invoke-direct {v3, v2}, Ljava/io/InputStreamReader;-><init>(Ljava/io/InputStream;)V

    invoke-direct {v1, v3}, Ljava/io/BufferedReader;-><init>(Ljava/io/Reader;)V

    .line 2238
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    .line 2240
    :goto_0
    invoke-virtual {v1}, Ljava/io/BufferedReader;->readLine()Ljava/lang/String;

    move-result-object v4

    if-eqz v4, :cond_1

    .line 2242
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    goto :goto_0

    .line 2247
    :catch_0
    move-exception v1

    :goto_1
    :try_start_2
    invoke-virtual {v1}, Ljava/lang/Exception;->printStackTrace()V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 2252
    if-eqz v2, :cond_0

    .line 2256
    :try_start_3
    invoke-virtual {v2}, Ljava/io/InputStream;->close()V
    :try_end_3
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_2

    .line 2262
    :cond_0
    :goto_2
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v1, "onCreate make country array part finish"

    invoke-static {v1}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;)V

    .line 2264
    :goto_3
    return-object v0

    .line 2245
    :cond_1
    :try_start_4
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
    :try_end_4
    .catch Ljava/lang/Exception; {:try_start_4 .. :try_end_4} :catch_0
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    move-result-object v0

    .line 2252
    if-eqz v2, :cond_2

    .line 2256
    :try_start_5
    invoke-virtual {v2}, Ljava/io/InputStream;->close()V
    :try_end_5
    .catch Ljava/io/IOException; {:try_start_5 .. :try_end_5} :catch_1

    .line 2262
    :cond_2
    :goto_4
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v1, "onCreate make country array part finish"

    invoke-static {v1}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;)V

    goto :goto_3

    .line 2257
    :catch_1
    move-exception v1

    invoke-virtual {v1}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_4

    :catch_2
    move-exception v1

    invoke-virtual {v1}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_2

    .line 2252
    :catchall_0
    move-exception v1

    move-object v2, v0

    move-object v0, v1

    :goto_5
    if-eqz v2, :cond_3

    .line 2256
    :try_start_6
    invoke-virtual {v2}, Ljava/io/InputStream;->close()V
    :try_end_6
    .catch Ljava/io/IOException; {:try_start_6 .. :try_end_6} :catch_3

    .line 2262
    :cond_3
    :goto_6
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v1, "onCreate make country array part finish"

    invoke-static {v1}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;)V

    throw v0

    .line 2257
    :catch_3
    move-exception v1

    invoke-virtual {v1}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_6

    .line 2252
    :catchall_1
    move-exception v0

    goto :goto_5

    .line 2247
    :catch_4
    move-exception v1

    move-object v2, v0

    goto :goto_1
.end method

.method public final h(Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 2742
    iput-object p1, p0, Lcom/osp/app/util/BaseActivity;->E:Ljava/lang/String;

    .line 2743
    return-void
.end method

.method protected final h()Z
    .locals 4

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x1

    .line 881
    iget-boolean v0, p0, Lcom/osp/app/util/BaseActivity;->B:Z

    if-nez v0, :cond_4

    .line 883
    const-string v0, "dark"

    iget-object v3, p0, Lcom/osp/app/util/BaseActivity;->E:Ljava/lang/String;

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    move-object v0, p0

    :goto_0
    move-object v3, v0

    move v0, v2

    .line 893
    :goto_1
    iput-boolean v0, v3, Lcom/osp/app/util/BaseActivity;->A:Z

    .line 900
    iput-boolean v1, p0, Lcom/osp/app/util/BaseActivity;->B:Z

    .line 916
    :cond_0
    :goto_2
    iget-boolean v0, p0, Lcom/osp/app/util/BaseActivity;->A:Z

    if-nez v0, :cond_6

    :goto_3
    return v1

    .line 886
    :cond_1
    const-string v0, "light"

    iget-object v3, p0, Lcom/osp/app/util/BaseActivity;->E:Ljava/lang/String;

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    move v0, v1

    move-object v3, p0

    .line 888
    goto :goto_1

    .line 891
    :cond_2
    iget-object v0, p0, Lcom/osp/app/util/BaseActivity;->z:Lcom/osp/app/util/am;

    const-string v3, "com.android.settings"

    invoke-virtual {v0, v3}, Lcom/osp/app/util/am;->a(Ljava/lang/String;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/osp/app/util/BaseActivity;->A:Z

    .line 893
    sget-object v0, Lcom/osp/app/util/h;->a:Lcom/osp/app/util/g;

    iget-boolean v0, p0, Lcom/osp/app/util/BaseActivity;->A:Z

    if-nez v0, :cond_3

    move v0, v1

    :goto_4
    invoke-static {v0}, Lcom/osp/app/util/g;->a(Z)Z

    move-result v0

    if-nez v0, :cond_7

    move v0, v1

    move-object v3, p0

    goto :goto_1

    :cond_3
    move v0, v2

    goto :goto_4

    .line 903
    :cond_4
    iget-object v0, p0, Lcom/osp/app/util/BaseActivity;->E:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 905
    const-string v0, "dark"

    iget-object v3, p0, Lcom/osp/app/util/BaseActivity;->E:Ljava/lang/String;

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_5

    .line 907
    iput-boolean v2, p0, Lcom/osp/app/util/BaseActivity;->A:Z

    .line 912
    :goto_5
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/osp/app/util/BaseActivity;->E:Ljava/lang/String;

    goto :goto_2

    .line 910
    :cond_5
    iput-boolean v1, p0, Lcom/osp/app/util/BaseActivity;->A:Z

    goto :goto_5

    :cond_6
    move v1, v2

    .line 916
    goto :goto_3

    :cond_7
    move-object v0, p0

    goto :goto_0
.end method

.method protected final i()V
    .locals 2

    .prologue
    .line 1223
    iget-object v0, p0, Lcom/osp/app/util/BaseActivity;->z:Lcom/osp/app/util/am;

    const/16 v1, 0x10

    invoke-virtual {v0, v1}, Lcom/osp/app/util/am;->a(I)Z

    .line 1224
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "["

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "] setTheme=["

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/osp/app/util/BaseActivity;->z:Lcom/osp/app/util/am;

    iget-object v1, p0, Lcom/osp/app/util/BaseActivity;->z:Lcom/osp/app/util/am;

    iget v1, v1, Lcom/osp/app/util/am;->a:I

    invoke-static {v1}, Lcom/osp/app/util/am;->c(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " ]"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/osp/app/util/aq;->c(Ljava/lang/String;)V

    .line 1225
    return-void
.end method

.method protected final j()V
    .locals 2

    .prologue
    .line 1239
    iget-object v0, p0, Lcom/osp/app/util/BaseActivity;->z:Lcom/osp/app/util/am;

    const/16 v1, 0xb

    invoke-virtual {v0, v1}, Lcom/osp/app/util/am;->a(I)Z

    .line 1240
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "["

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "] setTheme=["

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/osp/app/util/BaseActivity;->z:Lcom/osp/app/util/am;

    iget-object v1, p0, Lcom/osp/app/util/BaseActivity;->z:Lcom/osp/app/util/am;

    iget v1, v1, Lcom/osp/app/util/am;->a:I

    invoke-static {v1}, Lcom/osp/app/util/am;->c(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " ]"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/osp/app/util/aq;->c(Ljava/lang/String;)V

    .line 1241
    return-void
.end method

.method protected final k()V
    .locals 2

    .prologue
    .line 1247
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0xe

    if-ge v0, v1, :cond_0

    .line 1249
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/osp/app/util/BaseActivity;->requestWindowFeature(I)Z

    .line 1263
    :goto_0
    return-void

    .line 1252
    :cond_0
    invoke-virtual {p0}, Lcom/osp/app/util/BaseActivity;->h()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1254
    const v0, 0x103013a

    invoke-virtual {p0, v0}, Lcom/osp/app/util/BaseActivity;->setTheme(I)V

    .line 1255
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "["

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "] setTheme=[android.R.style.Theme_DeviceDefault_Panel]"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/osp/app/util/aq;->c(Ljava/lang/String;)V

    goto :goto_0

    .line 1258
    :cond_1
    const v0, 0x103013b

    invoke-virtual {p0, v0}, Lcom/osp/app/util/BaseActivity;->setTheme(I)V

    .line 1259
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "["

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "] setTheme=[android.R.style.Theme_DeviceDefault_Light_Panel]"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/osp/app/util/aq;->c(Ljava/lang/String;)V

    goto :goto_0
.end method

.method protected final l()V
    .locals 8

    .prologue
    const v7, 0x7f0c018f

    const v6, 0x7f020048

    const v5, 0x7f020042

    const v4, 0x7f0c018e

    const/4 v3, 0x0

    .line 1516
    iget-object v0, p0, Lcom/osp/app/util/BaseActivity;->C:Ljava/lang/String;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/osp/app/util/BaseActivity;->x:Landroid/view/Menu;

    if-eqz v0, :cond_1

    .line 1518
    const-string v0, "done_cancel"

    iget-object v1, p0, Lcom/osp/app/util/BaseActivity;->C:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 1520
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/osp/app/util/BaseActivity;->b(Z)V

    .line 1562
    :cond_0
    :goto_0
    invoke-virtual {p0}, Lcom/osp/app/util/BaseActivity;->a()V

    .line 1565
    :cond_1
    return-void

    .line 1522
    :cond_2
    const-string v0, "signin_cancel"

    iget-object v1, p0, Lcom/osp/app/util/BaseActivity;->C:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 1524
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/osp/app/util/BaseActivity;->b(Z)V

    goto :goto_0

    .line 1526
    :cond_3
    const-string v0, "signup_cancel"

    iget-object v1, p0, Lcom/osp/app/util/BaseActivity;->C:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_6

    .line 1528
    invoke-static {}, Lcom/osp/device/b;->b()Lcom/osp/device/b;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/osp/device/b;->l(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_5

    invoke-virtual {p0}, Lcom/osp/app/util/BaseActivity;->getMenuInflater()Landroid/view/MenuInflater;

    move-result-object v0

    const v1, 0x7f0b000e

    iget-object v2, p0, Lcom/osp/app/util/BaseActivity;->x:Landroid/view/Menu;

    invoke-virtual {v0, v1, v2}, Landroid/view/MenuInflater;->inflate(ILandroid/view/Menu;)V

    :goto_1
    invoke-direct {p0}, Lcom/osp/app/util/BaseActivity;->c()Z

    move-result v0

    if-nez v0, :cond_4

    iget-object v0, p0, Lcom/osp/app/util/BaseActivity;->x:Landroid/view/Menu;

    const v1, 0x7f0c0198

    invoke-interface {v0, v1}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v0

    invoke-virtual {p0}, Lcom/osp/app/util/BaseActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1, v6}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    invoke-interface {v0, v1}, Landroid/view/MenuItem;->setIcon(Landroid/graphics/drawable/Drawable;)Landroid/view/MenuItem;

    iget-object v0, p0, Lcom/osp/app/util/BaseActivity;->x:Landroid/view/Menu;

    invoke-interface {v0, v4}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v0

    invoke-virtual {p0}, Lcom/osp/app/util/BaseActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1, v5}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    invoke-interface {v0, v1}, Landroid/view/MenuItem;->setIcon(Landroid/graphics/drawable/Drawable;)Landroid/view/MenuItem;

    :cond_4
    invoke-static {p0}, Lcom/osp/app/util/r;->k(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/osp/app/util/BaseActivity;->x:Landroid/view/Menu;

    const v1, 0x7f0c0198

    invoke-interface {v0, v1}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v0

    invoke-interface {v0, v3}, Landroid/view/MenuItem;->setIcon(Landroid/graphics/drawable/Drawable;)Landroid/view/MenuItem;

    iget-object v0, p0, Lcom/osp/app/util/BaseActivity;->x:Landroid/view/Menu;

    invoke-interface {v0, v4}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v0

    invoke-interface {v0, v3}, Landroid/view/MenuItem;->setIcon(Landroid/graphics/drawable/Drawable;)Landroid/view/MenuItem;

    goto :goto_0

    :cond_5
    invoke-virtual {p0}, Lcom/osp/app/util/BaseActivity;->getMenuInflater()Landroid/view/MenuInflater;

    move-result-object v0

    const v1, 0x7f0b000f

    iget-object v2, p0, Lcom/osp/app/util/BaseActivity;->x:Landroid/view/Menu;

    invoke-virtual {v0, v1, v2}, Landroid/view/MenuInflater;->inflate(ILandroid/view/Menu;)V

    goto :goto_1

    .line 1530
    :cond_6
    const-string v0, "save_cancel"

    iget-object v1, p0, Lcom/osp/app/util/BaseActivity;->C:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_b

    .line 1532
    invoke-static {}, Lcom/osp/device/b;->b()Lcom/osp/device/b;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/osp/device/b;->l(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_8

    invoke-virtual {p0}, Lcom/osp/app/util/BaseActivity;->getMenuInflater()Landroid/view/MenuInflater;

    move-result-object v0

    const v1, 0x7f0b000b

    iget-object v2, p0, Lcom/osp/app/util/BaseActivity;->x:Landroid/view/Menu;

    invoke-virtual {v0, v1, v2}, Landroid/view/MenuInflater;->inflate(ILandroid/view/Menu;)V

    :goto_2
    invoke-static {}, Lcom/osp/device/b;->b()Lcom/osp/device/b;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/osp/device/b;->i(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_a

    invoke-direct {p0}, Lcom/osp/app/util/BaseActivity;->c()Z

    move-result v0

    if-eqz v0, :cond_9

    iget-object v0, p0, Lcom/osp/app/util/BaseActivity;->x:Landroid/view/Menu;

    invoke-interface {v0, v7}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v0

    invoke-virtual {p0}, Lcom/osp/app/util/BaseActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f020047

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    invoke-interface {v0, v1}, Landroid/view/MenuItem;->setIcon(Landroid/graphics/drawable/Drawable;)Landroid/view/MenuItem;

    iget-object v0, p0, Lcom/osp/app/util/BaseActivity;->x:Landroid/view/Menu;

    invoke-interface {v0, v4}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v0

    invoke-virtual {p0}, Lcom/osp/app/util/BaseActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f020041

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    invoke-interface {v0, v1}, Landroid/view/MenuItem;->setIcon(Landroid/graphics/drawable/Drawable;)Landroid/view/MenuItem;

    :cond_7
    :goto_3
    invoke-static {p0}, Lcom/osp/app/util/r;->k(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/osp/app/util/BaseActivity;->x:Landroid/view/Menu;

    invoke-interface {v0, v7}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v0

    invoke-interface {v0, v3}, Landroid/view/MenuItem;->setIcon(Landroid/graphics/drawable/Drawable;)Landroid/view/MenuItem;

    iget-object v0, p0, Lcom/osp/app/util/BaseActivity;->x:Landroid/view/Menu;

    invoke-interface {v0, v4}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v0

    invoke-interface {v0, v3}, Landroid/view/MenuItem;->setIcon(Landroid/graphics/drawable/Drawable;)Landroid/view/MenuItem;

    goto/16 :goto_0

    :cond_8
    invoke-virtual {p0}, Lcom/osp/app/util/BaseActivity;->getMenuInflater()Landroid/view/MenuInflater;

    move-result-object v0

    const v1, 0x7f0b0003

    iget-object v2, p0, Lcom/osp/app/util/BaseActivity;->x:Landroid/view/Menu;

    invoke-virtual {v0, v1, v2}, Landroid/view/MenuInflater;->inflate(ILandroid/view/Menu;)V

    goto :goto_2

    :cond_9
    iget-object v0, p0, Lcom/osp/app/util/BaseActivity;->x:Landroid/view/Menu;

    invoke-interface {v0, v7}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v0

    invoke-virtual {p0}, Lcom/osp/app/util/BaseActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1, v6}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    invoke-interface {v0, v1}, Landroid/view/MenuItem;->setIcon(Landroid/graphics/drawable/Drawable;)Landroid/view/MenuItem;

    iget-object v0, p0, Lcom/osp/app/util/BaseActivity;->x:Landroid/view/Menu;

    invoke-interface {v0, v4}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v0

    invoke-virtual {p0}, Lcom/osp/app/util/BaseActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1, v5}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    invoke-interface {v0, v1}, Landroid/view/MenuItem;->setIcon(Landroid/graphics/drawable/Drawable;)Landroid/view/MenuItem;

    goto :goto_3

    :cond_a
    invoke-static {}, Lcom/osp/device/b;->b()Lcom/osp/device/b;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/osp/device/b;->e(Landroid/content/Context;)I

    move-result v0

    const/16 v1, 0x78

    if-gt v0, v1, :cond_7

    iget-object v0, p0, Lcom/osp/app/util/BaseActivity;->x:Landroid/view/Menu;

    invoke-interface {v0, v7}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v0

    invoke-interface {v0, v3}, Landroid/view/MenuItem;->setIcon(Landroid/graphics/drawable/Drawable;)Landroid/view/MenuItem;

    iget-object v0, p0, Lcom/osp/app/util/BaseActivity;->x:Landroid/view/Menu;

    invoke-interface {v0, v4}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v0

    invoke-interface {v0, v3}, Landroid/view/MenuItem;->setIcon(Landroid/graphics/drawable/Drawable;)Landroid/view/MenuItem;

    goto :goto_3

    .line 1534
    :cond_b
    const-string v0, "agree_cancel"

    iget-object v1, p0, Lcom/osp/app/util/BaseActivity;->C:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_e

    .line 1536
    invoke-static {}, Lcom/osp/device/b;->b()Lcom/osp/device/b;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/osp/device/b;->l(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_d

    invoke-virtual {p0}, Lcom/osp/app/util/BaseActivity;->getMenuInflater()Landroid/view/MenuInflater;

    move-result-object v0

    const v1, 0x7f0b0010

    iget-object v2, p0, Lcom/osp/app/util/BaseActivity;->x:Landroid/view/Menu;

    invoke-virtual {v0, v1, v2}, Landroid/view/MenuInflater;->inflate(ILandroid/view/Menu;)V

    :goto_4
    invoke-direct {p0}, Lcom/osp/app/util/BaseActivity;->c()Z

    move-result v0

    if-nez v0, :cond_c

    iget-object v0, p0, Lcom/osp/app/util/BaseActivity;->x:Landroid/view/Menu;

    const v1, 0x7f0c0199

    invoke-interface {v0, v1}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v0

    invoke-virtual {p0}, Lcom/osp/app/util/BaseActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1, v6}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    invoke-interface {v0, v1}, Landroid/view/MenuItem;->setIcon(Landroid/graphics/drawable/Drawable;)Landroid/view/MenuItem;

    iget-object v0, p0, Lcom/osp/app/util/BaseActivity;->x:Landroid/view/Menu;

    const v1, 0x7f0c019a

    invoke-interface {v0, v1}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v0

    invoke-virtual {p0}, Lcom/osp/app/util/BaseActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1, v5}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    invoke-interface {v0, v1}, Landroid/view/MenuItem;->setIcon(Landroid/graphics/drawable/Drawable;)Landroid/view/MenuItem;

    :cond_c
    invoke-static {p0}, Lcom/osp/app/util/r;->k(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/osp/app/util/BaseActivity;->x:Landroid/view/Menu;

    const v1, 0x7f0c0199

    invoke-interface {v0, v1}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v0

    invoke-interface {v0, v3}, Landroid/view/MenuItem;->setIcon(Landroid/graphics/drawable/Drawable;)Landroid/view/MenuItem;

    iget-object v0, p0, Lcom/osp/app/util/BaseActivity;->x:Landroid/view/Menu;

    const v1, 0x7f0c019a

    invoke-interface {v0, v1}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v0

    invoke-interface {v0, v3}, Landroid/view/MenuItem;->setIcon(Landroid/graphics/drawable/Drawable;)Landroid/view/MenuItem;

    goto/16 :goto_0

    :cond_d
    invoke-virtual {p0}, Lcom/osp/app/util/BaseActivity;->getMenuInflater()Landroid/view/MenuInflater;

    move-result-object v0

    const v1, 0x7f0b0011

    iget-object v2, p0, Lcom/osp/app/util/BaseActivity;->x:Landroid/view/Menu;

    invoke-virtual {v0, v1, v2}, Landroid/view/MenuInflater;->inflate(ILandroid/view/Menu;)V

    goto :goto_4

    .line 1538
    :cond_e
    const-string v0, "agree_decliene"

    iget-object v1, p0, Lcom/osp/app/util/BaseActivity;->C:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_10

    .line 1540
    invoke-static {}, Lcom/osp/device/b;->b()Lcom/osp/device/b;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/osp/device/b;->l(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_f

    invoke-virtual {p0}, Lcom/osp/app/util/BaseActivity;->getMenuInflater()Landroid/view/MenuInflater;

    move-result-object v0

    const v1, 0x7f0b0001

    iget-object v2, p0, Lcom/osp/app/util/BaseActivity;->x:Landroid/view/Menu;

    invoke-virtual {v0, v1, v2}, Landroid/view/MenuInflater;->inflate(ILandroid/view/Menu;)V

    :goto_5
    invoke-static {p0}, Lcom/osp/app/util/r;->k(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/osp/app/util/BaseActivity;->x:Landroid/view/Menu;

    const v1, 0x7f0c018d

    invoke-interface {v0, v1}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v0

    invoke-interface {v0, v3}, Landroid/view/MenuItem;->setIcon(Landroid/graphics/drawable/Drawable;)Landroid/view/MenuItem;

    iget-object v0, p0, Lcom/osp/app/util/BaseActivity;->x:Landroid/view/Menu;

    invoke-interface {v0, v4}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v0

    invoke-interface {v0, v3}, Landroid/view/MenuItem;->setIcon(Landroid/graphics/drawable/Drawable;)Landroid/view/MenuItem;

    goto/16 :goto_0

    :cond_f
    invoke-virtual {p0}, Lcom/osp/app/util/BaseActivity;->getMenuInflater()Landroid/view/MenuInflater;

    move-result-object v0

    const v1, 0x7f0b0002

    iget-object v2, p0, Lcom/osp/app/util/BaseActivity;->x:Landroid/view/Menu;

    invoke-virtual {v0, v1, v2}, Landroid/view/MenuInflater;->inflate(ILandroid/view/Menu;)V

    goto :goto_5

    .line 1542
    :cond_10
    const-string v0, "edit"

    iget-object v1, p0, Lcom/osp/app/util/BaseActivity;->C:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_13

    .line 1544
    invoke-virtual {p0}, Lcom/osp/app/util/BaseActivity;->getMenuInflater()Landroid/view/MenuInflater;

    move-result-object v0

    const/high16 v1, 0x7f0b0000

    iget-object v2, p0, Lcom/osp/app/util/BaseActivity;->x:Landroid/view/Menu;

    invoke-virtual {v0, v1, v2}, Landroid/view/MenuInflater;->inflate(ILandroid/view/Menu;)V

    invoke-direct {p0}, Lcom/osp/app/util/BaseActivity;->c()Z

    move-result v0

    if-nez v0, :cond_11

    iget-object v0, p0, Lcom/osp/app/util/BaseActivity;->x:Landroid/view/Menu;

    const v1, 0x7f0c018c

    invoke-interface {v0, v1}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v0

    invoke-virtual {p0}, Lcom/osp/app/util/BaseActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f02004e

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    invoke-interface {v0, v1}, Landroid/view/MenuItem;->setIcon(Landroid/graphics/drawable/Drawable;)Landroid/view/MenuItem;

    :cond_11
    invoke-static {}, Lcom/osp/device/b;->b()Lcom/osp/device/b;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/osp/device/b;->i(Landroid/content/Context;)Z

    move-result v0

    if-nez v0, :cond_12

    invoke-static {}, Lcom/osp/device/b;->b()Lcom/osp/device/b;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/osp/device/b;->e(Landroid/content/Context;)I

    move-result v0

    const/16 v1, 0x78

    if-gt v0, v1, :cond_12

    iget-object v0, p0, Lcom/osp/app/util/BaseActivity;->x:Landroid/view/Menu;

    const v1, 0x7f0c018c

    invoke-interface {v0, v1}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v0

    invoke-interface {v0, v3}, Landroid/view/MenuItem;->setTitle(Ljava/lang/CharSequence;)Landroid/view/MenuItem;

    :cond_12
    invoke-static {p0}, Lcom/osp/app/util/r;->k(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/osp/app/util/BaseActivity;->x:Landroid/view/Menu;

    const v1, 0x7f0c018c

    invoke-interface {v0, v1}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v0

    invoke-interface {v0, v3}, Landroid/view/MenuItem;->setIcon(Landroid/graphics/drawable/Drawable;)Landroid/view/MenuItem;

    goto/16 :goto_0

    .line 1546
    :cond_13
    const-string v0, "next"

    iget-object v1, p0, Lcom/osp/app/util/BaseActivity;->C:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_15

    .line 1548
    invoke-virtual {p0}, Lcom/osp/app/util/BaseActivity;->getMenuInflater()Landroid/view/MenuInflater;

    move-result-object v0

    const v1, 0x7f0b000a

    iget-object v2, p0, Lcom/osp/app/util/BaseActivity;->x:Landroid/view/Menu;

    invoke-virtual {v0, v1, v2}, Landroid/view/MenuInflater;->inflate(ILandroid/view/Menu;)V

    invoke-direct {p0}, Lcom/osp/app/util/BaseActivity;->c()Z

    move-result v0

    if-nez v0, :cond_14

    iget-object v0, p0, Lcom/osp/app/util/BaseActivity;->x:Landroid/view/Menu;

    const v1, 0x7f0c0196

    invoke-interface {v0, v1}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v0

    invoke-virtual {p0}, Lcom/osp/app/util/BaseActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f020054

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    invoke-interface {v0, v1}, Landroid/view/MenuItem;->setIcon(Landroid/graphics/drawable/Drawable;)Landroid/view/MenuItem;

    :cond_14
    invoke-static {p0}, Lcom/osp/app/util/r;->k(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/osp/app/util/BaseActivity;->x:Landroid/view/Menu;

    const v1, 0x7f0c0196

    invoke-interface {v0, v1}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v0

    invoke-interface {v0, v3}, Landroid/view/MenuItem;->setIcon(Landroid/graphics/drawable/Drawable;)Landroid/view/MenuItem;

    goto/16 :goto_0

    .line 1550
    :cond_15
    const-string v0, "verify"

    iget-object v1, p0, Lcom/osp/app/util/BaseActivity;->C:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_17

    .line 1552
    invoke-virtual {p0}, Lcom/osp/app/util/BaseActivity;->getMenuInflater()Landroid/view/MenuInflater;

    move-result-object v0

    const v1, 0x7f0b0014

    iget-object v2, p0, Lcom/osp/app/util/BaseActivity;->x:Landroid/view/Menu;

    invoke-virtual {v0, v1, v2}, Landroid/view/MenuInflater;->inflate(ILandroid/view/Menu;)V

    invoke-direct {p0}, Lcom/osp/app/util/BaseActivity;->c()Z

    move-result v0

    if-nez v0, :cond_16

    iget-object v0, p0, Lcom/osp/app/util/BaseActivity;->x:Landroid/view/Menu;

    const v1, 0x7f0c019b

    invoke-interface {v0, v1}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v0

    invoke-virtual {p0}, Lcom/osp/app/util/BaseActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f020054

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    invoke-interface {v0, v1}, Landroid/view/MenuItem;->setIcon(Landroid/graphics/drawable/Drawable;)Landroid/view/MenuItem;

    :cond_16
    iget-object v0, p0, Lcom/osp/app/util/BaseActivity;->x:Landroid/view/Menu;

    const v1, 0x7f0c019b

    invoke-interface {v0, v1}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v0

    invoke-interface {v0, v3}, Landroid/view/MenuItem;->setIcon(Landroid/graphics/drawable/Drawable;)Landroid/view/MenuItem;

    goto/16 :goto_0

    .line 1554
    :cond_17
    const-string v0, "confirm_cancel"

    iget-object v1, p0, Lcom/osp/app/util/BaseActivity;->C:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1a

    .line 1556
    invoke-static {}, Lcom/osp/device/b;->b()Lcom/osp/device/b;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/osp/device/b;->l(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_19

    invoke-virtual {p0}, Lcom/osp/app/util/BaseActivity;->getMenuInflater()Landroid/view/MenuInflater;

    move-result-object v0

    const v1, 0x7f0b0004

    iget-object v2, p0, Lcom/osp/app/util/BaseActivity;->x:Landroid/view/Menu;

    invoke-virtual {v0, v1, v2}, Landroid/view/MenuInflater;->inflate(ILandroid/view/Menu;)V

    :goto_6
    invoke-direct {p0}, Lcom/osp/app/util/BaseActivity;->c()Z

    move-result v0

    if-nez v0, :cond_18

    iget-object v0, p0, Lcom/osp/app/util/BaseActivity;->x:Landroid/view/Menu;

    const v1, 0x7f0c0190

    invoke-interface {v0, v1}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v0

    invoke-virtual {p0}, Lcom/osp/app/util/BaseActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1, v6}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    invoke-interface {v0, v1}, Landroid/view/MenuItem;->setIcon(Landroid/graphics/drawable/Drawable;)Landroid/view/MenuItem;

    iget-object v0, p0, Lcom/osp/app/util/BaseActivity;->x:Landroid/view/Menu;

    invoke-interface {v0, v4}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v0

    invoke-virtual {p0}, Lcom/osp/app/util/BaseActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1, v5}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    invoke-interface {v0, v1}, Landroid/view/MenuItem;->setIcon(Landroid/graphics/drawable/Drawable;)Landroid/view/MenuItem;

    :cond_18
    invoke-static {p0}, Lcom/osp/app/util/r;->k(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/osp/app/util/BaseActivity;->x:Landroid/view/Menu;

    const v1, 0x7f0c0190

    invoke-interface {v0, v1}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v0

    invoke-interface {v0, v3}, Landroid/view/MenuItem;->setIcon(Landroid/graphics/drawable/Drawable;)Landroid/view/MenuItem;

    iget-object v0, p0, Lcom/osp/app/util/BaseActivity;->x:Landroid/view/Menu;

    invoke-interface {v0, v4}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v0

    invoke-interface {v0, v3}, Landroid/view/MenuItem;->setIcon(Landroid/graphics/drawable/Drawable;)Landroid/view/MenuItem;

    goto/16 :goto_0

    :cond_19
    invoke-virtual {p0}, Lcom/osp/app/util/BaseActivity;->getMenuInflater()Landroid/view/MenuInflater;

    move-result-object v0

    const v1, 0x7f0b0005

    iget-object v2, p0, Lcom/osp/app/util/BaseActivity;->x:Landroid/view/Menu;

    invoke-virtual {v0, v1, v2}, Landroid/view/MenuInflater;->inflate(ILandroid/view/Menu;)V

    goto :goto_6

    .line 1557
    :cond_1a
    const-string v0, "finish"

    iget-object v1, p0, Lcom/osp/app/util/BaseActivity;->C:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1559
    invoke-virtual {p0}, Lcom/osp/app/util/BaseActivity;->getMenuInflater()Landroid/view/MenuInflater;

    move-result-object v0

    const v1, 0x7f0b0007

    iget-object v2, p0, Lcom/osp/app/util/BaseActivity;->x:Landroid/view/Menu;

    invoke-virtual {v0, v1, v2}, Landroid/view/MenuInflater;->inflate(ILandroid/view/Menu;)V

    invoke-direct {p0}, Lcom/osp/app/util/BaseActivity;->c()Z

    move-result v0

    if-nez v0, :cond_1b

    iget-object v0, p0, Lcom/osp/app/util/BaseActivity;->x:Landroid/view/Menu;

    const v1, 0x7f0c0192

    invoke-interface {v0, v1}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v0

    invoke-virtual {p0}, Lcom/osp/app/util/BaseActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f020054

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    invoke-interface {v0, v1}, Landroid/view/MenuItem;->setIcon(Landroid/graphics/drawable/Drawable;)Landroid/view/MenuItem;

    :cond_1b
    invoke-static {p0}, Lcom/osp/app/util/r;->k(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/osp/app/util/BaseActivity;->x:Landroid/view/Menu;

    const v1, 0x7f0c0192

    invoke-interface {v0, v1}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v0

    invoke-interface {v0, v3}, Landroid/view/MenuItem;->setIcon(Landroid/graphics/drawable/Drawable;)Landroid/view/MenuItem;

    goto/16 :goto_0
.end method

.method protected final m()V
    .locals 10

    .prologue
    const-wide/32 v8, 0xea60

    const-wide/16 v6, 0x0

    .line 2169
    invoke-virtual {p0}, Lcom/osp/app/util/BaseActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Landroid/preference/PreferenceManager;->getDefaultSharedPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v1

    .line 2171
    const-string v0, "SIGN_OUT_START_TIME"

    invoke-interface {v1, v0, v6, v7}, Landroid/content/SharedPreferences;->getLong(Ljava/lang/String;J)J

    move-result-wide v2

    .line 2173
    cmp-long v0, v2, v6

    if-lez v0, :cond_1

    .line 2175
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v4

    .line 2177
    sub-long v2, v4, v2

    .line 2178
    cmp-long v0, v2, v6

    if-lez v0, :cond_0

    cmp-long v0, v2, v8

    if-gez v0, :cond_0

    .line 2186
    sub-long v2, v8, v2

    .line 2187
    const-wide/16 v4, 0x1388

    :try_start_0
    div-long/2addr v2, v4

    long-to-int v2, v2

    .line 2191
    const/4 v0, 0x0

    :goto_0
    if-ge v0, v2, :cond_0

    .line 2194
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v3

    const-string v4, "Wait.. 5000"

    invoke-static {v3, v4}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 2196
    const-wide/16 v4, 0x1388

    invoke-static {v4, v5}, Ljava/lang/Thread;->sleep(J)V

    .line 2198
    const-string v3, "SIGN_OUT_START_TIME"

    const-wide/16 v4, 0x0

    invoke-interface {v1, v3, v4, v5}, Landroid/content/SharedPreferences;->getLong(Ljava/lang/String;J)J
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-wide v4

    .line 2199
    cmp-long v3, v4, v6

    if-eqz v3, :cond_0

    .line 2201
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 2212
    :cond_0
    invoke-interface {v1}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    .line 2213
    const-string v1, "SIGN_OUT_START_TIME"

    invoke-interface {v0, v1}, Landroid/content/SharedPreferences$Editor;->remove(Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    .line 2214
    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 2219
    :cond_1
    :goto_1
    return-void

    .line 2207
    :catch_0
    move-exception v0

    :try_start_1
    invoke-virtual {v0}, Ljava/lang/InterruptedException;->printStackTrace()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2212
    invoke-interface {v1}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    .line 2213
    const-string v1, "SIGN_OUT_START_TIME"

    invoke-interface {v0, v1}, Landroid/content/SharedPreferences$Editor;->remove(Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    .line 2214
    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    goto :goto_1

    .line 2212
    :catchall_0
    move-exception v0

    invoke-interface {v1}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v1

    .line 2213
    const-string v2, "SIGN_OUT_START_TIME"

    invoke-interface {v1, v2}, Landroid/content/SharedPreferences$Editor;->remove(Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    .line 2214
    invoke-interface {v1}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 2215
    throw v0
.end method

.method protected final n()Ljava/lang/String;
    .locals 2

    .prologue
    .line 2490
    const/4 v0, 0x0

    .line 2491
    invoke-static {}, Lcom/osp/common/util/i;->a()Lcom/osp/common/util/i;

    invoke-static {p0}, Lcom/osp/common/util/i;->e(Landroid/content/Context;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 2495
    :try_start_0
    invoke-static {}, Lcom/osp/common/util/i;->a()Lcom/osp/common/util/i;

    invoke-static {p0}, Lcom/osp/common/util/i;->c(Landroid/content/Context;)Ljava/lang/String;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 2504
    :goto_0
    return-object v0

    .line 2496
    :catch_0
    move-exception v1

    invoke-virtual {v1}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_0

    .line 2502
    :cond_0
    invoke-static {}, Lcom/osp/common/util/i;->a()Lcom/osp/common/util/i;

    invoke-static {p0}, Lcom/osp/common/util/i;->m(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public final o()I
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 2772
    .line 2775
    :try_start_0
    const-string v0, "com.android.internal.R$dimen"

    invoke-static {v0}, Ljava/lang/Class;->forName(Ljava/lang/String;)Ljava/lang/Class;

    move-result-object v0

    .line 2776
    const-string v2, "status_bar_height"

    invoke-virtual {v0, v2}, Ljava/lang/Class;->getDeclaredField(Ljava/lang/String;)Ljava/lang/reflect/Field;

    move-result-object v0

    const/4 v2, 0x0

    invoke-virtual {v0, v2}, Ljava/lang/reflect/Field;->getInt(Ljava/lang/Object;)I

    move-result v0

    .line 2777
    if-lez v0, :cond_1

    .line 2779
    invoke-virtual {p0}, Lcom/osp/app/util/BaseActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-virtual {v2, v0}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result v0

    .line 2786
    :goto_0
    invoke-virtual {p0}, Lcom/osp/app/util/BaseActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v2

    .line 2787
    invoke-static {}, Lcom/osp/device/b;->b()Lcom/osp/device/b;

    move-result-object v3

    invoke-virtual {v3, p0}, Lcom/osp/device/b;->i(Landroid/content/Context;)Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-static {p0}, Lcom/osp/app/util/r;->k(Landroid/content/Context;)Z

    move-result v3

    if-nez v3, :cond_0

    iget v2, v2, Landroid/util/DisplayMetrics;->densityDpi:I

    const/16 v3, 0xd5

    if-eq v2, v3, :cond_0

    move v0, v1

    .line 2792
    :cond_0
    return v0

    .line 2781
    :catch_0
    move-exception v0

    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    :cond_1
    move v0, v1

    goto :goto_0
.end method

.method public onBackPressed()V
    .locals 1

    .prologue
    .line 1181
    :try_start_0
    invoke-super {p0}, Landroid/app/Activity;->onBackPressed()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 1188
    :goto_0
    return-void

    .line 1182
    :catch_0
    move-exception v0

    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    .line 1186
    invoke-virtual {p0}, Lcom/osp/app/util/BaseActivity;->finish()V

    goto :goto_0
.end method

.method public onConfigurationChanged(Landroid/content/res/Configuration;)V
    .locals 2

    .prologue
    .line 1833
    invoke-super {p0, p1}, Landroid/app/Activity;->onConfigurationChanged(Landroid/content/res/Configuration;)V

    .line 1835
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0xb

    if-lt v0, v1, :cond_0

    iget-object v0, p0, Lcom/osp/app/util/BaseActivity;->x:Landroid/view/Menu;

    if-eqz v0, :cond_0

    .line 1837
    iget-object v0, p0, Lcom/osp/app/util/BaseActivity;->x:Landroid/view/Menu;

    invoke-interface {v0}, Landroid/view/Menu;->clear()V

    .line 1839
    invoke-virtual {p0}, Lcom/osp/app/util/BaseActivity;->l()V

    .line 1841
    :cond_0
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/osp/app/util/BaseActivity;->a(Z)V

    .line 1843
    invoke-static {}, Lcom/osp/device/b;->b()Lcom/osp/device/b;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/osp/device/b;->i(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1845
    iget v0, p1, Landroid/content/res/Configuration;->orientation:I

    invoke-direct {p0, v0}, Lcom/osp/app/util/BaseActivity;->a(I)V

    .line 1847
    :cond_1
    return-void
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 5

    .prologue
    const/4 v4, 0x1

    .line 1022
    sget-object v0, Lcom/osp/app/util/h;->a:Lcom/osp/app/util/g;

    invoke-static {p0}, Lcom/osp/app/util/g;->a(Landroid/content/Context;)V

    .line 1025
    invoke-direct {p0, v4}, Lcom/osp/app/util/BaseActivity;->a(Z)V

    .line 1026
    invoke-super {p0, p1}, Landroid/app/Activity;->onCreate(Landroid/os/Bundle;)V

    .line 1027
    invoke-virtual {p0}, Lcom/osp/app/util/BaseActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    .line 1028
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    invoke-static {v0}, Lcom/osp/app/util/aq;->a(Landroid/content/Intent;)Ljava/lang/String;

    move-result-object v1

    .line 1030
    invoke-virtual {p0}, Lcom/osp/app/util/BaseActivity;->getCallingPackage()Ljava/lang/String;

    move-result-object v0

    .line 1032
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 1034
    const-string v0, "Unknown"

    .line 1039
    :goto_0
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "["

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "] onCreate Intent=["

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " ] CallingPackage =[ "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " ] "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/osp/app/util/aq;->c(Ljava/lang/String;)V

    .line 1043
    :try_start_0
    invoke-virtual {p0}, Lcom/osp/app/util/BaseActivity;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v0

    invoke-virtual {p0}, Lcom/osp/app/util/BaseActivity;->getComponentName()Landroid/content/ComponentName;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/content/pm/PackageManager;->getActivityInfo(Landroid/content/ComponentName;I)Landroid/content/pm/ActivityInfo;

    move-result-object v0

    .line 1044
    if-eqz v0, :cond_0

    iget-boolean v0, v0, Landroid/content/pm/ActivityInfo;->exported:Z

    if-ne v0, v4, :cond_0

    .line 1046
    invoke-static {}, Lcom/osp/device/b;->b()Lcom/osp/device/b;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/osp/device/b;->f(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v0

    .line 1047
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "["

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "] device=["

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " ]"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/osp/app/util/aq;->c(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 1054
    :cond_0
    :goto_1
    return-void

    .line 1037
    :cond_1
    invoke-static {}, Lcom/osp/app/util/v;->a()Lcom/osp/app/util/v;

    move-result-object v2

    invoke-virtual {v2, v0}, Lcom/osp/app/util/v;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_0

    .line 1049
    :catch_0
    move-exception v0

    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_1
.end method

.method public onCreateDialog(I)Landroid/app/Dialog;
    .locals 1

    .prologue
    .line 2352
    invoke-super {p0, p1}, Landroid/app/Activity;->onCreateDialog(I)Landroid/app/Dialog;

    move-result-object v0

    return-object v0
.end method

.method public onCreateOptionsMenu(Landroid/view/Menu;)Z
    .locals 2

    .prologue
    .line 1491
    iget-object v0, p0, Lcom/osp/app/util/BaseActivity;->C:Ljava/lang/String;

    if-eqz v0, :cond_0

    if-eqz p1, :cond_0

    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0xb

    if-lt v0, v1, :cond_0

    .line 1493
    iput-object p1, p0, Lcom/osp/app/util/BaseActivity;->x:Landroid/view/Menu;

    .line 1495
    invoke-virtual {p0}, Lcom/osp/app/util/BaseActivity;->l()V

    .line 1496
    const/4 v0, 0x1

    .line 1499
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected onDestroy()V
    .locals 0

    .prologue
    .line 2935
    invoke-super {p0}, Landroid/app/Activity;->onDestroy()V

    .line 2938
    return-void
.end method

.method protected onNewIntent(Landroid/content/Intent;)V
    .locals 3

    .prologue
    .line 1155
    invoke-super {p0, p1}, Landroid/app/Activity;->onNewIntent(Landroid/content/Intent;)V

    .line 1156
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    invoke-static {p1}, Lcom/osp/app/util/aq;->a(Landroid/content/Intent;)Ljava/lang/String;

    move-result-object v0

    .line 1157
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "["

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "] onNewIntent Intent=["

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " ]"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/osp/app/util/aq;->c(Ljava/lang/String;)V

    .line 1158
    return-void
.end method

.method public onPrepareOptionsMenu(Landroid/view/Menu;)Z
    .locals 1

    .prologue
    .line 1505
    iget-object v0, p0, Lcom/osp/app/util/BaseActivity;->x:Landroid/view/Menu;

    if-eqz v0, :cond_0

    .line 1507
    const/4 v0, 0x1

    .line 1509
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected onRestoreInstanceState(Landroid/os/Bundle;)V
    .locals 1

    .prologue
    .line 1164
    :try_start_0
    invoke-super {p0, p1}, Landroid/app/Activity;->onRestoreInstanceState(Landroid/os/Bundle;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 1169
    :goto_0
    return-void

    .line 1165
    :catch_0
    move-exception v0

    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_0
.end method

.method protected onResume()V
    .locals 6

    .prologue
    const/4 v1, 0x1

    .line 1851
    invoke-super {p0}, Landroid/app/Activity;->onResume()V

    .line 1853
    invoke-static {}, Lcom/osp/app/signin/SamsungService;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1855
    invoke-virtual {p0}, Lcom/osp/app/util/BaseActivity;->getWindow()Landroid/view/Window;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/Window;->getAttributes()Landroid/view/WindowManager$LayoutParams;

    move-result-object v0

    iget v0, v0, Landroid/view/WindowManager$LayoutParams;->flags:I

    and-int/lit16 v0, v0, 0x400

    if-nez v0, :cond_1

    .line 1857
    invoke-static {}, Lcom/osp/app/util/r;->n()Z

    move-result v0

    if-nez v0, :cond_0

    invoke-static {}, Lcom/osp/device/b;->b()Lcom/osp/device/b;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/osp/device/b;->i(Landroid/content/Context;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 1859
    invoke-static {p0, v1}, Lcom/msc/sa/c/d;->a(Landroid/content/Context;Z)V

    .line 1860
    invoke-static {}, Lcom/osp/device/b;->b()Lcom/osp/device/b;

    move-result-object v0

    invoke-virtual {v0}, Lcom/osp/device/b;->l()I

    move-result v0

    const/16 v1, 0xf

    if-le v0, v1, :cond_0

    invoke-static {p0}, Lcom/osp/app/util/r;->u(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_0

    :try_start_0
    const-string v0, "statusbar"

    invoke-virtual {p0, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    const-string v1, "android.app.StatusBarManager"

    invoke-static {v1}, Ljava/lang/Class;->forName(Ljava/lang/String;)Ljava/lang/Class;

    move-result-object v1

    const-string v2, "setTransGradationModeColor"

    const/4 v3, 0x2

    new-array v3, v3, [Ljava/lang/Class;

    const/4 v4, 0x0

    sget-object v5, Ljava/lang/Boolean;->TYPE:Ljava/lang/Class;

    aput-object v5, v3, v4

    const/4 v4, 0x1

    sget-object v5, Ljava/lang/Integer;->TYPE:Ljava/lang/Class;

    aput-object v5, v3, v4

    invoke-virtual {v1, v2, v3}, Ljava/lang/Class;->getMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v1

    const/4 v2, 0x2

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    const/4 v4, 0x0

    invoke-static {v4}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v4

    aput-object v4, v2, v3

    const/4 v3, 0x1

    const/4 v4, 0x0

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-virtual {v1, v0, v2}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;

    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v0, "setTransGradationModeColor = false, Color = 0"

    invoke-static {v0}, Lcom/osp/app/util/aq;->c(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/ClassNotFoundException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_1

    .line 1867
    :cond_0
    :goto_0
    return-void

    .line 1860
    :catch_0
    move-exception v0

    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v0, "setTransGradationModeColor() method not found"

    invoke-static {v0}, Lcom/osp/app/util/aq;->b(Ljava/lang/String;)V

    goto :goto_0

    :catch_1
    move-exception v0

    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v0, "setTransGradationModeColor() exception error"

    invoke-static {v0}, Lcom/osp/app/util/aq;->b(Ljava/lang/String;)V

    goto :goto_0

    .line 1864
    :cond_1
    invoke-static {p0, v1}, Lcom/msc/sa/c/d;->a(Landroid/content/Context;Z)V

    goto :goto_0
.end method

.method public onWindowFocusChanged(Z)V
    .locals 1

    .prologue
    .line 2475
    invoke-super {p0, p1}, Landroid/app/Activity;->onWindowFocusChanged(Z)V

    .line 2477
    const/4 v0, 0x1

    if-ne p1, v0, :cond_0

    invoke-static {}, Lcom/osp/app/signin/SamsungService;->a()Z

    move-result v0

    if-nez v0, :cond_0

    .line 2479
    invoke-virtual {p0}, Lcom/osp/app/util/BaseActivity;->b()V

    .line 2481
    :cond_0
    return-void
.end method

.method protected final p()Z
    .locals 3

    .prologue
    .line 3018
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v0, "ViewComponentAttribute"

    const-string v1, "isAvailableSignature()"

    invoke-static {v0, v1}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 3019
    invoke-virtual {p0}, Lcom/osp/app/util/BaseActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    .line 3020
    const-string v1, "client_id"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 3021
    const-string v2, "package_name"

    invoke-virtual {v0, v2}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 3022
    invoke-static {p0, v1, v0}, Lcom/msc/sa/c/j;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 3024
    const/4 v0, 0x0

    .line 3026
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public sendBroadcast(Landroid/content/Intent;)V
    .locals 1

    .prologue
    .line 1399
    invoke-direct {p0, p1}, Lcom/osp/app/util/BaseActivity;->b(Landroid/content/Intent;)V

    .line 1401
    invoke-super {p0, p1}, Landroid/app/Activity;->sendBroadcast(Landroid/content/Intent;)V

    .line 1405
    invoke-static {p1}, Lcom/osp/app/util/BaseActivity;->a(Landroid/content/Intent;)Z

    move-result v0

    .line 1406
    if-eqz v0, :cond_0

    .line 1408
    invoke-super {p0, p1}, Landroid/app/Activity;->sendBroadcast(Landroid/content/Intent;)V

    .line 1410
    :cond_0
    return-void
.end method

.method public sendBroadcast(Landroid/content/Intent;Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 1414
    invoke-direct {p0, p1}, Lcom/osp/app/util/BaseActivity;->b(Landroid/content/Intent;)V

    .line 1416
    invoke-super {p0, p1, p2}, Landroid/app/Activity;->sendBroadcast(Landroid/content/Intent;Ljava/lang/String;)V

    .line 1420
    invoke-static {p1}, Lcom/osp/app/util/BaseActivity;->a(Landroid/content/Intent;)Z

    move-result v0

    .line 1421
    if-eqz v0, :cond_0

    .line 1423
    invoke-super {p0, p1, p2}, Landroid/app/Activity;->sendBroadcast(Landroid/content/Intent;Ljava/lang/String;)V

    .line 1425
    :cond_0
    return-void
.end method

.method public setContentView(I)V
    .locals 5

    .prologue
    const/16 v1, 0xa

    const/4 v4, -0x1

    const/4 v2, 0x1

    .line 2607
    invoke-static {p0}, Lcom/osp/app/util/r;->k(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2610
    invoke-static {p0}, Lcom/msc/sa/c/d;->c(Landroid/app/Activity;)V

    .line 2614
    :cond_0
    invoke-static {}, Lcom/osp/app/signin/SamsungService;->a()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-static {p0}, Lcom/osp/app/util/r;->m(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 2616
    instance-of v0, p0, Lcom/osp/app/signin/SignUpWithFacebookWebView;

    if-nez v0, :cond_1

    instance-of v0, p0, Lcom/osp/app/signin/WebContentView;

    if-nez v0, :cond_1

    instance-of v0, p0, Lcom/osp/app/signin/WeiboInfoWebView;

    if-nez v0, :cond_1

    instance-of v0, p0, Lcom/osp/app/signin/NameValidationWebView;

    if-nez v0, :cond_1

    instance-of v0, p0, Lcom/msc/sa/myprofile/MyProfileWebView;

    if-eqz v0, :cond_d

    .line 2619
    :cond_1
    invoke-static {p0}, Lcom/msc/sa/c/d;->d(Landroid/app/Activity;)V

    .line 2626
    :cond_2
    :goto_0
    invoke-static {p0}, Lcom/osp/app/util/x;->o(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-static {p0}, Lcom/osp/app/util/r;->m(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_e

    :cond_3
    instance-of v0, p0, Lcom/osp/app/signin/SignUpCompleteView;

    if-eqz v0, :cond_4

    invoke-static {p0}, Lcom/osp/app/util/r;->m(Landroid/content/Context;)Z

    move-result v0

    if-nez v0, :cond_4

    invoke-static {p0}, Lcom/osp/app/util/r;->k(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_e

    :cond_4
    invoke-static {}, Lcom/osp/device/b;->b()Lcom/osp/device/b;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/osp/device/b;->i(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_5

    instance-of v0, p0, Lcom/osp/app/signin/SelectCountryView;

    if-nez v0, :cond_e

    :cond_5
    invoke-static {}, Lcom/osp/app/signin/SamsungService;->a()Z

    move-result v0

    if-eqz v0, :cond_e

    instance-of v0, p0, Lcom/osp/app/signin/AccountView;

    if-nez v0, :cond_e

    instance-of v0, p0, Lcom/osp/app/signin/SignUpWithFacebookWebView;

    if-nez v0, :cond_e

    instance-of v0, p0, Lcom/osp/app/signin/WebContentView;

    if-nez v0, :cond_e

    instance-of v0, p0, Lcom/osp/app/signin/WeiboInfoWebView;

    if-nez v0, :cond_e

    instance-of v0, p0, Lcom/osp/app/signin/NameValidationWebView;

    if-nez v0, :cond_e

    instance-of v0, p0, Lcom/msc/sa/myprofile/MyProfileWebView;

    if-nez v0, :cond_e

    move v0, v2

    :goto_1
    if-eqz v0, :cond_f

    .line 2628
    invoke-static {}, Lcom/osp/device/b;->b()Lcom/osp/device/b;

    move-result-object v0

    invoke-virtual {v0}, Lcom/osp/device/b;->l()I

    move-result v0

    if-le v0, v1, :cond_6

    invoke-virtual {p0}, Landroid/app/Activity;->getActionBar()Landroid/app/ActionBar;

    move-result-object v0

    if-eqz v0, :cond_6

    invoke-virtual {v0}, Landroid/app/ActionBar;->hide()V

    .line 2629
    :cond_6
    invoke-static {}, Lcom/osp/device/b;->b()Lcom/osp/device/b;

    move-result-object v0

    invoke-virtual {v0}, Lcom/osp/device/b;->l()I

    move-result v0

    if-gt v0, v1, :cond_7

    .line 2631
    invoke-virtual {p0, v2}, Lcom/osp/app/util/BaseActivity;->requestWindowFeature(I)Z

    .line 2634
    :cond_7
    const v0, 0x7f030006

    invoke-super {p0, v0}, Landroid/app/Activity;->setContentView(I)V

    .line 2636
    const v0, 0x7f0c003b

    invoke-virtual {p0, v0}, Lcom/osp/app/util/BaseActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    .line 2637
    const-string v1, "layout_inflater"

    invoke-virtual {p0, v1}, Lcom/osp/app/util/BaseActivity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/view/LayoutInflater;

    .line 2638
    const/4 v3, 0x0

    invoke-virtual {v1, p1, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v1

    .line 2639
    new-instance v3, Landroid/view/ViewGroup$LayoutParams;

    invoke-direct {v3, v4, v4}, Landroid/view/ViewGroup$LayoutParams;-><init>(II)V

    invoke-virtual {v0, v1, v3}, Landroid/view/ViewGroup;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    .line 2641
    const v0, 0x7f0c003a

    invoke-virtual {p0, v0}, Lcom/osp/app/util/BaseActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/osp/app/signin/AccountTitleLinearLayout;

    .line 2642
    if-eqz v0, :cond_9

    .line 2644
    invoke-virtual {p0}, Lcom/osp/app/util/BaseActivity;->h()Z

    move-result v1

    invoke-virtual {v0, p0, v1}, Lcom/osp/app/signin/AccountTitleLinearLayout;->a(Landroid/content/Context;Z)V

    .line 2646
    invoke-virtual {p0}, Lcom/osp/app/util/BaseActivity;->getWindow()Landroid/view/Window;

    move-result-object v1

    invoke-virtual {v1}, Landroid/view/Window;->getAttributes()Landroid/view/WindowManager$LayoutParams;

    move-result-object v1

    .line 2647
    invoke-static {p0}, Lcom/osp/app/util/r;->g(Landroid/content/Context;)Z

    move-result v3

    if-nez v3, :cond_8

    .line 2649
    iget v1, v1, Landroid/view/WindowManager$LayoutParams;->flags:I

    and-int/lit16 v1, v1, 0x400

    if-eqz v1, :cond_8

    .line 2651
    invoke-virtual {v0}, Lcom/osp/app/signin/AccountTitleLinearLayout;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v1

    check-cast v1, Landroid/widget/LinearLayout$LayoutParams;

    .line 2652
    invoke-virtual {p0}, Lcom/osp/app/util/BaseActivity;->o()I

    move-result v3

    iput v3, v1, Landroid/widget/LinearLayout$LayoutParams;->topMargin:I

    .line 2653
    invoke-virtual {v0, v1}, Lcom/osp/app/signin/AccountTitleLinearLayout;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 2657
    :cond_8
    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/osp/app/signin/AccountTitleLinearLayout;->a(Ljava/lang/Boolean;)V

    .line 2658
    instance-of v1, p0, Lcom/osp/app/signin/SelectCountryView;

    if-eqz v1, :cond_9

    .line 2660
    const v1, 0x7f09001f

    invoke-virtual {p0, v1}, Lcom/osp/app/util/BaseActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/osp/app/signin/AccountTitleLinearLayout;->a(Ljava/lang/String;)V

    .line 2668
    :cond_9
    :goto_2
    invoke-static {}, Lcom/osp/app/signin/SamsungService;->a()Z

    move-result v0

    if-nez v0, :cond_b

    .line 2670
    invoke-static {p0}, Lcom/osp/app/util/r;->m(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_a

    instance-of v0, p0, Lcom/osp/app/signin/AccountView;

    if-nez v0, :cond_a

    .line 2672
    invoke-static {p0}, Lcom/msc/sa/c/d;->d(Landroid/app/Activity;)V

    .line 2674
    :cond_a
    invoke-virtual {p0}, Lcom/osp/app/util/BaseActivity;->b()V

    .line 2676
    :cond_b
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v0, "blockMultiTouch_start"

    invoke-static {v0}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;)V

    .line 2677
    sget-object v0, Lcom/osp/app/util/h;->a:Lcom/osp/app/util/g;

    invoke-virtual {p0}, Lcom/osp/app/util/BaseActivity;->getWindow()Landroid/view/Window;

    move-result-object v1

    invoke-virtual {v1}, Landroid/view/Window;->getDecorView()Landroid/view/View;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/osp/app/util/g;->a(Landroid/view/View;)V

    .line 2678
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v0, "blockMultiTouch_end"

    invoke-static {v0}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;)V

    .line 2680
    invoke-static {}, Lcom/osp/app/signin/SamsungService;->a()Z

    move-result v0

    if-eqz v0, :cond_c

    invoke-static {p0}, Lcom/osp/app/util/r;->k(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_c

    sget-object v0, Lcom/osp/app/util/u;->o:[Ljava/lang/String;

    invoke-static {v0}, Lcom/osp/app/util/u;->a([Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_c

    .line 2682
    invoke-virtual {p0}, Lcom/osp/app/util/BaseActivity;->getWindow()Landroid/view/Window;

    move-result-object v0

    .line 2683
    invoke-virtual {v0}, Landroid/view/Window;->getAttributes()Landroid/view/WindowManager$LayoutParams;

    move-result-object v0

    .line 2685
    iget v1, v0, Landroid/view/WindowManager$LayoutParams;->flags:I

    const v2, -0x4000001

    and-int/2addr v1, v2

    iput v1, v0, Landroid/view/WindowManager$LayoutParams;->flags:I

    .line 2687
    :cond_c
    return-void

    .line 2622
    :cond_d
    invoke-static {p0}, Lcom/msc/sa/c/d;->c(Landroid/app/Activity;)V

    goto/16 :goto_0

    .line 2626
    :cond_e
    const/4 v0, 0x0

    goto/16 :goto_1

    .line 2665
    :cond_f
    invoke-super {p0, p1}, Landroid/app/Activity;->setContentView(I)V

    goto :goto_2
.end method

.method public setInputMethodEmoticonDisabled(Landroid/view/View;)V
    .locals 3

    .prologue
    .line 3038
    if-eqz p1, :cond_1

    .line 3040
    :try_start_0
    instance-of v0, p1, Landroid/view/ViewGroup;

    if-eqz v0, :cond_0

    .line 3043
    check-cast p1, Landroid/view/ViewGroup;

    .line 3045
    invoke-virtual {p1}, Landroid/view/ViewGroup;->getChildCount()I

    move-result v1

    .line 3046
    const/4 v0, 0x0

    :goto_0
    if-ge v0, v1, :cond_1

    .line 3048
    invoke-virtual {p1, v0}, Landroid/view/ViewGroup;->getChildAt(I)Landroid/view/View;

    move-result-object v2

    invoke-virtual {p0, v2}, Lcom/osp/app/util/BaseActivity;->setInputMethodEmoticonDisabled(Landroid/view/View;)V

    .line 3046
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 3052
    :cond_0
    instance-of v0, p1, Landroid/widget/EditText;

    if-eqz v0, :cond_1

    .line 3054
    check-cast p1, Landroid/widget/EditText;

    .line 3055
    const-string v0, "disableEmoticonInput=true"

    invoke-virtual {p1, v0}, Landroid/widget/EditText;->setPrivateImeOptions(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 3063
    :cond_1
    :goto_1
    return-void

    .line 3059
    :catch_0
    move-exception v0

    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_1
.end method

.method public startService(Landroid/content/Intent;)Landroid/content/ComponentName;
    .locals 3

    .prologue
    .line 1484
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    invoke-static {p1}, Lcom/osp/app/util/aq;->a(Landroid/content/Intent;)Ljava/lang/String;

    move-result-object v0

    .line 1485
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "["

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "] startService Intent=["

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " ]"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/osp/app/util/aq;->c(Ljava/lang/String;)V

    .line 1486
    invoke-super {p0, p1}, Landroid/app/Activity;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;

    move-result-object v0

    return-object v0
.end method
