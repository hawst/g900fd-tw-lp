.class final Lcom/osp/app/util/am;
.super Ljava/lang/Object;
.source "Theme.java"


# instance fields
.field a:I

.field private b:Ljava/lang/ref/WeakReference;

.field private c:Ljava/lang/ref/WeakReference;


# direct methods
.method public constructor <init>(Landroid/app/Activity;)V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 95
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 81
    iput-object v0, p0, Lcom/osp/app/util/am;->b:Ljava/lang/ref/WeakReference;

    .line 82
    iput-object v0, p0, Lcom/osp/app/util/am;->c:Ljava/lang/ref/WeakReference;

    .line 84
    const/4 v0, -0x1

    iput v0, p0, Lcom/osp/app/util/am;->a:I

    .line 96
    new-instance v0, Ljava/lang/ref/WeakReference;

    invoke-direct {v0, p1}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    iput-object v0, p0, Lcom/osp/app/util/am;->b:Ljava/lang/ref/WeakReference;

    .line 97
    return-void
.end method

.method public constructor <init>(Landroid/app/Service;)V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 99
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 81
    iput-object v0, p0, Lcom/osp/app/util/am;->b:Ljava/lang/ref/WeakReference;

    .line 82
    iput-object v0, p0, Lcom/osp/app/util/am;->c:Ljava/lang/ref/WeakReference;

    .line 84
    const/4 v0, -0x1

    iput v0, p0, Lcom/osp/app/util/am;->a:I

    .line 100
    new-instance v0, Ljava/lang/ref/WeakReference;

    invoke-direct {v0, p1}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    iput-object v0, p0, Lcom/osp/app/util/am;->c:Ljava/lang/ref/WeakReference;

    .line 101
    return-void
.end method

.method public static c(I)Ljava/lang/String;
    .locals 1

    .prologue
    .line 1532
    sparse-switch p0, :sswitch_data_0

    .line 1645
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, p0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 1648
    :goto_0
    return-object v0

    .line 1537
    :sswitch_0
    const-string v0, "R.style.Theme_ICS_Black"

    goto :goto_0

    .line 1540
    :sswitch_1
    const-string v0, "R.style.Theme_ICS_White_Dialog"

    goto :goto_0

    .line 1543
    :sswitch_2
    const-string v0, "R.style.Theme_ICS_White_Dialog_No_Background"

    goto :goto_0

    .line 1546
    :sswitch_3
    const-string v0, "R.style.Theme_ICS_White"

    goto :goto_0

    .line 1549
    :sswitch_4
    const-string v0, "R.style.Theme_ICS_Black_Dialog"

    goto :goto_0

    .line 1552
    :sswitch_5
    const-string v0, "android.R.style.Theme"

    goto :goto_0

    .line 1555
    :sswitch_6
    const-string v0, "android.R.style.Theme_Dialog"

    goto :goto_0

    .line 1558
    :sswitch_7
    const-string v0, "android.R.style.Theme_NoTitleBar"

    goto :goto_0

    .line 1561
    :sswitch_8
    const-string v0, "android.R.style.Theme_Translucent"

    goto :goto_0

    .line 1564
    :sswitch_9
    const-string v0, "android.R.style.Theme_Translucent_NoTitleBar"

    goto :goto_0

    .line 1567
    :sswitch_a
    const-string v0, "android.R.style.Theme_Light"

    goto :goto_0

    .line 1570
    :sswitch_b
    const-string v0, "android.R.style.Theme_Holo_Light_DialogWhenLarge"

    goto :goto_0

    .line 1573
    :sswitch_c
    const-string v0, "android.R.style.Theme_Light_NoTitleBar"

    goto :goto_0

    .line 1576
    :sswitch_d
    const-string v0, "android.R.style.Theme_Black"

    goto :goto_0

    .line 1579
    :sswitch_e
    const-string v0, "android.R.style.Theme_Holo_DialogWhenLarge"

    goto :goto_0

    .line 1582
    :sswitch_f
    const-string v0, "android.R.style.Theme_Black_NoTitleBar"

    goto :goto_0

    .line 1585
    :sswitch_10
    const-string v0, "android.R.style.Theme_Holo"

    goto :goto_0

    .line 1588
    :sswitch_11
    const-string v0, "android.R.style.Theme_DeviceDefault"

    goto :goto_0

    .line 1591
    :sswitch_12
    const-string v0, "android.R.style.Theme_Holo_Dialog"

    goto :goto_0

    .line 1594
    :sswitch_13
    const-string v0, "android.R.style.Theme_DeviceDefault_Dialog"

    goto :goto_0

    .line 1597
    :sswitch_14
    const-string v0, "android.R.style.Theme_Holo_Dialog_NoActionBar"

    goto :goto_0

    .line 1600
    :sswitch_15
    const-string v0, "android.R.style.Theme_DeviceDefault_NoActionBar"

    goto :goto_0

    .line 1603
    :sswitch_16
    const-string v0, "android.R.style.Theme_Holo_NoActionBar"

    goto :goto_0

    .line 1606
    :sswitch_17
    const-string v0, "android.R.style.Theme_Holo_Wallpaper"

    goto :goto_0

    .line 1609
    :sswitch_18
    const-string v0, "android.R.style.Theme_DeviceDefault_Wallpaper"

    goto :goto_0

    .line 1612
    :sswitch_19
    const-string v0, "android.R.style.Theme_DeviceDefault_Dialog_NoActionBar"

    goto :goto_0

    .line 1615
    :sswitch_1a
    const-string v0, "android.R.style.Theme_Holo_Wallpaper_NoTitleBar"

    goto :goto_0

    .line 1618
    :sswitch_1b
    const-string v0, "android.R.style.Theme_DeviceDefault_Wallpaper_NoTitleBar"

    goto :goto_0

    .line 1621
    :sswitch_1c
    const-string v0, "android.R.style.Theme_Holo_Light"

    goto :goto_0

    .line 1624
    :sswitch_1d
    const-string v0, "android.R.style.Theme_DeviceDefault_Light"

    goto :goto_0

    .line 1627
    :sswitch_1e
    const-string v0, "android.R.style.Theme_Holo_Light_Dialog"

    goto :goto_0

    .line 1630
    :sswitch_1f
    const-string v0, "android.R.style.Theme_DeviceDefault_Light_Dialog"

    goto :goto_0

    .line 1633
    :sswitch_20
    const-string v0, "android.R.style.Theme_Holo_Light_Dialog_NoActionBar"

    goto :goto_0

    .line 1636
    :sswitch_21
    const-string v0, "android.R.style.Theme_DeviceDefault_Light_Dialog_NoActionBar"

    goto :goto_0

    .line 1639
    :sswitch_22
    const-string v0, "android.R.style.Theme_Holo_Light_NoActionBar"

    goto :goto_0

    .line 1642
    :sswitch_23
    const-string v0, "android.R.style.Theme_DeviceDefault_Light_NoActionBar"

    goto :goto_0

    .line 1532
    nop

    :sswitch_data_0
    .sparse-switch
        0x1030005 -> :sswitch_5
        0x1030006 -> :sswitch_7
        0x1030008 -> :sswitch_d
        0x1030009 -> :sswitch_f
        0x103000b -> :sswitch_6
        0x103000c -> :sswitch_a
        0x103000d -> :sswitch_c
        0x103000f -> :sswitch_8
        0x1030010 -> :sswitch_9
        0x103006b -> :sswitch_10
        0x103006c -> :sswitch_16
        0x103006e -> :sswitch_1c
        0x103006f -> :sswitch_12
        0x1030071 -> :sswitch_14
        0x1030073 -> :sswitch_1e
        0x1030075 -> :sswitch_20
        0x1030077 -> :sswitch_e
        0x1030079 -> :sswitch_b
        0x103007d -> :sswitch_17
        0x103007e -> :sswitch_1a
        0x10300f0 -> :sswitch_22
        0x1030128 -> :sswitch_11
        0x1030129 -> :sswitch_15
        0x103012b -> :sswitch_1d
        0x103012c -> :sswitch_23
        0x103012e -> :sswitch_13
        0x1030130 -> :sswitch_19
        0x1030132 -> :sswitch_1f
        0x1030134 -> :sswitch_21
        0x103013c -> :sswitch_18
        0x103013d -> :sswitch_1b
        0x7f0a0015 -> :sswitch_0
        0x7f0a0016 -> :sswitch_3
        0x7f0a0017 -> :sswitch_4
        0x7f0a0018 -> :sswitch_1
        0x7f0a0019 -> :sswitch_2
    .end sparse-switch
.end method


# virtual methods
.method public final a(I)Z
    .locals 11

    .prologue
    const v3, 0x1030129

    const v2, 0x103000b

    const/16 v10, 0xe

    const/16 v9, 0xd

    const/16 v8, 0xa

    .line 1431
    const/4 v4, 0x0

    .line 1440
    if-gtz p1, :cond_0

    .line 1442
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "Theme::setTheme::aThemeType is wrong value, "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/osp/app/util/aq;->d(Ljava/lang/String;)V

    move v0, v4

    .line 1471
    :goto_0
    return v0

    .line 1446
    :cond_0
    sget v6, Landroid/os/Build$VERSION;->SDK_INT:I

    .line 1451
    const/4 v1, -0x1

    iget-object v0, p0, Lcom/osp/app/util/am;->b:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/Activity;

    if-nez v0, :cond_1

    const/4 v0, -0x1

    move v1, v0

    .line 1452
    :goto_1
    const/4 v0, -0x1

    if-ne v1, v0, :cond_47

    .line 1454
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "Theme::setTheme::aThemeType is wrong value, "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "Api :"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/osp/app/util/aq;->d(Ljava/lang/String;)V

    move v0, v4

    .line 1455
    goto :goto_0

    .line 1451
    :cond_1
    const/4 v5, 0x0

    instance-of v7, v0, Lcom/osp/app/util/BaseActivity;

    if-eqz v7, :cond_4

    invoke-static {}, Lcom/osp/device/b;->b()Lcom/osp/device/b;

    move-result-object v5

    invoke-virtual {v5, v0}, Lcom/osp/device/b;->i(Landroid/content/Context;)Z

    move-result v5

    :cond_2
    :goto_2
    packed-switch p1, :pswitch_data_0

    :cond_3
    move v0, v1

    :goto_3
    move v1, v0

    goto :goto_1

    :cond_4
    instance-of v7, v0, Lcom/osp/app/util/BasePreferenceActivity;

    if-eqz v7, :cond_2

    invoke-static {}, Lcom/osp/device/b;->b()Lcom/osp/device/b;

    move-result-object v5

    invoke-virtual {v5, v0}, Lcom/osp/device/b;->i(Landroid/content/Context;)Z

    move-result v5

    goto :goto_2

    :pswitch_0
    if-gt v6, v8, :cond_5

    const v0, 0x1030005

    goto :goto_3

    :cond_5
    if-gt v6, v9, :cond_6

    const v0, 0x103006b

    goto :goto_3

    :cond_6
    if-lt v6, v10, :cond_3

    const v0, 0x7f0a0015

    goto :goto_3

    :pswitch_1
    if-gt v6, v8, :cond_7

    const v0, 0x103000f

    goto :goto_3

    :cond_7
    if-gt v6, v9, :cond_8

    const v0, 0x103007d

    goto :goto_3

    :cond_8
    if-lt v6, v10, :cond_3

    const v0, 0x103013c

    goto :goto_3

    :pswitch_2
    if-gt v6, v8, :cond_9

    move v0, v2

    goto :goto_3

    :cond_9
    if-gt v6, v9, :cond_a

    const v0, 0x103006f

    goto :goto_3

    :cond_a
    if-lt v6, v10, :cond_3

    const v0, 0x103012e

    goto :goto_3

    :pswitch_3
    if-gt v6, v8, :cond_b

    move v0, v2

    goto :goto_3

    :cond_b
    if-gt v6, v9, :cond_c

    const v0, 0x1030071

    goto :goto_3

    :cond_c
    if-lt v6, v10, :cond_3

    const v0, 0x1030130

    goto :goto_3

    :pswitch_4
    if-gt v6, v8, :cond_d

    const v0, 0x1030006

    goto :goto_3

    :cond_d
    if-gt v6, v9, :cond_e

    const v0, 0x103006c

    goto :goto_3

    :cond_e
    if-lt v6, v10, :cond_3

    move v0, v3

    goto :goto_3

    :pswitch_5
    if-gt v6, v8, :cond_f

    const v0, 0x1030010

    goto :goto_3

    :cond_f
    if-gt v6, v9, :cond_10

    const v0, 0x103007e

    goto :goto_3

    :cond_10
    if-lt v6, v10, :cond_3

    const v0, 0x103013d

    goto :goto_3

    :pswitch_6
    if-gt v6, v8, :cond_11

    const v0, 0x1030006

    goto :goto_3

    :cond_11
    if-gt v6, v9, :cond_12

    const v0, 0x103006c

    goto :goto_3

    :cond_12
    if-lt v6, v10, :cond_3

    move v0, v3

    goto/16 :goto_3

    :pswitch_7
    if-gt v6, v8, :cond_13

    const v0, 0x103000c

    goto/16 :goto_3

    :cond_13
    if-gt v6, v9, :cond_16

    instance-of v1, v0, Lcom/osp/app/signin/SelectCountryView;

    if-nez v1, :cond_14

    instance-of v0, v0, Lcom/msc/sa/selfupdate/SelfUpgradePopup;

    if-eqz v0, :cond_15

    :cond_14
    const v0, 0x1030079

    goto/16 :goto_3

    :cond_15
    const v0, 0x103006e

    goto/16 :goto_3

    :cond_16
    if-lt v6, v10, :cond_3

    invoke-static {}, Lcom/osp/device/b;->b()Lcom/osp/device/b;

    move-result-object v1

    invoke-virtual {v1}, Lcom/osp/device/b;->e()Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_1c

    const-string v2, "SHV-E150S"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_17

    const-string v2, "GT-6800"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1c

    :cond_17
    instance-of v1, v0, Lcom/osp/app/signin/SelectCountryView;

    if-nez v1, :cond_18

    instance-of v1, v0, Lcom/osp/app/signin/PasswordChangeView;

    if-nez v1, :cond_18

    instance-of v1, v0, Lcom/msc/sa/selfupdate/SelfUpgradePopup;

    if-eqz v1, :cond_1b

    :cond_18
    instance-of v0, v0, Lcom/osp/app/signin/SelectCountryView;

    if-nez v0, :cond_19

    const/16 v0, 0x10

    if-lt v6, v0, :cond_1a

    :cond_19
    const v0, 0x7f0a0017

    goto/16 :goto_3

    :cond_1a
    const v0, 0x7f0a0017

    goto/16 :goto_3

    :cond_1b
    const v0, 0x7f0a0015

    goto/16 :goto_3

    :cond_1c
    invoke-static {}, Lcom/osp/app/signin/SamsungService;->a()Z

    move-result v1

    if-eqz v1, :cond_1e

    instance-of v1, v0, Lcom/osp/app/signin/SignUpWithFacebookWebView;

    if-nez v1, :cond_1e

    instance-of v1, v0, Lcom/osp/app/signin/WeiboInfoWebView;

    if-nez v1, :cond_1e

    instance-of v1, v0, Lcom/osp/app/signin/WebContentView;

    if-nez v1, :cond_1e

    instance-of v1, v0, Lcom/osp/app/signin/SelectCountryView;

    if-nez v1, :cond_1e

    invoke-static {v0}, Lcom/osp/app/util/x;->o(Landroid/content/Context;)Z

    move-result v1

    if-eqz v1, :cond_1d

    invoke-static {v0}, Lcom/osp/app/util/r;->m(Landroid/content/Context;)Z

    move-result v1

    if-eqz v1, :cond_1e

    :cond_1d
    const v0, 0x7f0a000f

    goto/16 :goto_3

    :cond_1e
    instance-of v1, v0, Lcom/osp/app/signin/SelectCountryView;

    if-nez v1, :cond_1f

    instance-of v1, v0, Lcom/msc/sa/selfupdate/SelfUpgradePopup;

    if-eqz v1, :cond_24

    :cond_1f
    if-eqz v5, :cond_22

    instance-of v0, v0, Lcom/osp/app/signin/SelectCountryView;

    if-nez v0, :cond_20

    const/16 v0, 0x10

    if-lt v6, v0, :cond_21

    :cond_20
    const v0, 0x7f0a0019

    goto/16 :goto_3

    :cond_21
    const v0, 0x7f0a0018

    goto/16 :goto_3

    :cond_22
    const v1, 0x7f0a0016

    invoke-static {}, Lcom/osp/app/signin/SamsungService;->a()Z

    move-result v2

    if-eqz v2, :cond_3

    invoke-static {v0}, Lcom/osp/app/util/x;->o(Landroid/content/Context;)Z

    move-result v2

    if-eqz v2, :cond_23

    invoke-static {v0}, Lcom/osp/app/util/r;->m(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_3

    :cond_23
    const v0, 0x7f0a000f

    goto/16 :goto_3

    :cond_24
    const v0, 0x7f0a0016

    goto/16 :goto_3

    :pswitch_8
    if-gt v6, v8, :cond_25

    move v0, v2

    goto/16 :goto_3

    :cond_25
    if-gt v6, v9, :cond_26

    const v0, 0x1030073

    goto/16 :goto_3

    :cond_26
    if-lt v6, v10, :cond_3

    invoke-static {}, Lcom/osp/device/b;->b()Lcom/osp/device/b;

    move-result-object v0

    invoke-virtual {v0}, Lcom/osp/device/b;->e()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_3

    const-string v1, "SHV-E150S"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_27

    const-string v1, "GT-6800"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_28

    :cond_27
    const v0, 0x103012e

    goto/16 :goto_3

    :cond_28
    const v0, 0x1030132

    goto/16 :goto_3

    :pswitch_9
    if-gt v6, v8, :cond_29

    move v0, v2

    goto/16 :goto_3

    :cond_29
    if-gt v6, v9, :cond_2a

    const v0, 0x1030075

    goto/16 :goto_3

    :cond_2a
    if-lt v6, v10, :cond_3

    invoke-static {}, Lcom/osp/device/b;->b()Lcom/osp/device/b;

    move-result-object v0

    invoke-virtual {v0}, Lcom/osp/device/b;->e()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_3

    const-string v1, "SHV-E150S"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_2b

    const-string v1, "GT-6800"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2c

    :cond_2b
    const v0, 0x1030130

    goto/16 :goto_3

    :cond_2c
    const v0, 0x1030134

    goto/16 :goto_3

    :pswitch_a
    if-gt v6, v8, :cond_2d

    const v0, 0x103000d

    goto/16 :goto_3

    :cond_2d
    if-gt v6, v9, :cond_2e

    const v0, 0x10300f0

    goto/16 :goto_3

    :cond_2e
    if-lt v6, v10, :cond_3

    invoke-static {}, Lcom/osp/device/b;->b()Lcom/osp/device/b;

    move-result-object v0

    invoke-virtual {v0}, Lcom/osp/device/b;->e()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_3

    const-string v1, "SHV-E150S"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_2f

    const-string v1, "GT-6800"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_30

    :cond_2f
    move v0, v3

    goto/16 :goto_3

    :cond_30
    const v0, 0x103012c

    goto/16 :goto_3

    :pswitch_b
    if-gt v6, v8, :cond_31

    const v0, 0x103000d

    goto/16 :goto_3

    :cond_31
    if-gt v6, v9, :cond_32

    const v0, 0x10300f0

    goto/16 :goto_3

    :cond_32
    if-lt v6, v10, :cond_3

    invoke-static {}, Lcom/osp/device/b;->b()Lcom/osp/device/b;

    move-result-object v0

    invoke-virtual {v0}, Lcom/osp/device/b;->e()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_3

    const-string v1, "SHV-E150S"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_33

    const-string v1, "GT-6800"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_34

    :cond_33
    move v0, v3

    goto/16 :goto_3

    :cond_34
    const v0, 0x103012c

    goto/16 :goto_3

    :pswitch_c
    if-gt v6, v8, :cond_35

    const v0, 0x1030008

    goto/16 :goto_3

    :cond_35
    if-gt v6, v9, :cond_38

    instance-of v1, v0, Lcom/osp/app/signin/SelectCountryView;

    if-nez v1, :cond_36

    instance-of v0, v0, Lcom/msc/sa/selfupdate/SelfUpgradePopup;

    if-eqz v0, :cond_37

    :cond_36
    const v0, 0x1030077

    goto/16 :goto_3

    :cond_37
    const v0, 0x103006b

    goto/16 :goto_3

    :cond_38
    if-lt v6, v10, :cond_3

    invoke-static {}, Lcom/osp/app/signin/SamsungService;->a()Z

    move-result v1

    if-eqz v1, :cond_3a

    instance-of v1, v0, Lcom/osp/app/signin/SignUpWithFacebookWebView;

    if-nez v1, :cond_3a

    instance-of v1, v0, Lcom/osp/app/signin/WeiboInfoWebView;

    if-nez v1, :cond_3a

    instance-of v1, v0, Lcom/osp/app/signin/WebContentView;

    if-nez v1, :cond_3a

    instance-of v1, v0, Lcom/osp/app/signin/SelectCountryView;

    if-nez v1, :cond_3a

    invoke-static {v0}, Lcom/osp/app/util/x;->o(Landroid/content/Context;)Z

    move-result v1

    if-eqz v1, :cond_39

    invoke-static {v0}, Lcom/osp/app/util/r;->m(Landroid/content/Context;)Z

    move-result v1

    if-eqz v1, :cond_3a

    :cond_39
    const v0, 0x7f0a0011

    goto/16 :goto_3

    :cond_3a
    instance-of v1, v0, Lcom/osp/app/signin/SelectCountryView;

    if-nez v1, :cond_3b

    instance-of v1, v0, Lcom/msc/sa/selfupdate/SelfUpgradePopup;

    if-eqz v1, :cond_3e

    :cond_3b
    if-eqz v5, :cond_3c

    const v0, 0x7f0a0017

    goto/16 :goto_3

    :cond_3c
    const v1, 0x7f0a0015

    invoke-static {}, Lcom/osp/app/signin/SamsungService;->a()Z

    move-result v2

    if-eqz v2, :cond_3

    invoke-static {v0}, Lcom/osp/app/util/x;->o(Landroid/content/Context;)Z

    move-result v2

    if-eqz v2, :cond_3d

    invoke-static {v0}, Lcom/osp/app/util/r;->m(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_3

    :cond_3d
    const v0, 0x7f0a0011

    goto/16 :goto_3

    :cond_3e
    const v0, 0x7f0a0015

    goto/16 :goto_3

    :pswitch_d
    if-gt v6, v8, :cond_3f

    move v0, v2

    goto/16 :goto_3

    :cond_3f
    if-gt v6, v9, :cond_40

    const v0, 0x103006f

    goto/16 :goto_3

    :cond_40
    if-lt v6, v10, :cond_3

    const v0, 0x103012e

    goto/16 :goto_3

    :pswitch_e
    if-gt v6, v8, :cond_41

    move v0, v2

    goto/16 :goto_3

    :cond_41
    if-gt v6, v9, :cond_42

    const v0, 0x1030071

    goto/16 :goto_3

    :cond_42
    if-lt v6, v10, :cond_3

    const v0, 0x1030130

    goto/16 :goto_3

    :pswitch_f
    if-gt v6, v8, :cond_43

    const v0, 0x1030009

    goto/16 :goto_3

    :cond_43
    if-gt v6, v9, :cond_44

    const v0, 0x103006c

    goto/16 :goto_3

    :cond_44
    if-lt v6, v10, :cond_3

    move v0, v3

    goto/16 :goto_3

    :pswitch_10
    if-gt v6, v8, :cond_45

    const v0, 0x1030009

    goto/16 :goto_3

    :cond_45
    if-gt v6, v9, :cond_46

    const v0, 0x103006c

    goto/16 :goto_3

    :cond_46
    if-lt v6, v10, :cond_3

    move v0, v3

    goto/16 :goto_3

    .line 1458
    :cond_47
    iput v1, p0, Lcom/osp/app/util/am;->a:I

    .line 1460
    iget-object v0, p0, Lcom/osp/app/util/am;->b:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/Activity;

    .line 1461
    if-nez v0, :cond_48

    .line 1463
    const/4 v0, 0x0

    goto/16 :goto_0

    .line 1465
    :cond_48
    invoke-virtual {v0, v1}, Landroid/app/Activity;->setTheme(I)V

    .line 1467
    const/4 v0, 0x1

    goto/16 :goto_0

    .line 1451
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_7
        :pswitch_8
        :pswitch_9
        :pswitch_a
        :pswitch_b
        :pswitch_c
        :pswitch_d
        :pswitch_e
        :pswitch_f
        :pswitch_10
    .end packed-switch
.end method

.method public final a(Ljava/lang/String;)Z
    .locals 9

    .prologue
    const/16 v8, 0x15

    const/4 v3, 0x0

    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 1281
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    if-lt v0, v8, :cond_0

    .line 1283
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v0, "Theme::L OS light theme"

    invoke-static {v0}, Lcom/osp/app/util/aq;->c(Ljava/lang/String;)V

    move v0, v1

    .line 1397
    :goto_0
    return v0

    .line 1296
    :cond_0
    if-eqz p1, :cond_1

    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-ne v0, v1, :cond_2

    .line 1298
    :cond_1
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v0, "Theme::getTheme::pkgName is null"

    invoke-static {v0}, Lcom/osp/app/util/aq;->d(Ljava/lang/String;)V

    move v0, v2

    .line 1299
    goto :goto_0

    .line 1306
    :cond_2
    :try_start_0
    iget-object v0, p0, Lcom/osp/app/util/am;->b:Ljava/lang/ref/WeakReference;

    if-eqz v0, :cond_3

    .line 1308
    iget-object v0, p0, Lcom/osp/app/util/am;->b:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/Activity;

    .line 1309
    if-eqz v0, :cond_11

    move-object v4, v0

    move v5, v1

    move-object v0, v3

    .line 1321
    :goto_1
    if-nez v4, :cond_4

    if-nez v0, :cond_4

    move v0, v2

    .line 1323
    goto :goto_0

    .line 1313
    :cond_3
    iget-object v0, p0, Lcom/osp/app/util/am;->c:Ljava/lang/ref/WeakReference;

    if-eqz v0, :cond_10

    .line 1315
    iget-object v0, p0, Lcom/osp/app/util/am;->c:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/Service;

    .line 1316
    if-eqz v0, :cond_f

    move-object v4, v3

    move v5, v2

    .line 1318
    goto :goto_1

    .line 1326
    :cond_4
    if-eqz v5, :cond_5

    .line 1328
    invoke-virtual {v4}, Landroid/app/Activity;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v0

    .line 1334
    :goto_2
    const/4 v4, 0x0

    invoke-virtual {v0, p1, v4}, Landroid/content/pm/PackageManager;->getPackageInfo(Ljava/lang/String;I)Landroid/content/pm/PackageInfo;

    move-result-object v4

    .line 1335
    if-nez v4, :cond_6

    .line 1337
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v0, "Theme::getTheme::pkgInfo is null"

    invoke-static {v0}, Lcom/osp/app/util/aq;->d(Ljava/lang/String;)V

    move v0, v2

    .line 1338
    goto :goto_0

    .line 1331
    :cond_5
    invoke-virtual {v0}, Landroid/app/Service;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v0

    goto :goto_2

    .line 1341
    :cond_6
    iget-object v4, v4, Landroid/content/pm/PackageInfo;->applicationInfo:Landroid/content/pm/ApplicationInfo;

    iget v4, v4, Landroid/content/pm/ApplicationInfo;->theme:I

    .line 1343
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "settings theme : "

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v5, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Lcom/osp/app/util/aq;->b(Ljava/lang/String;)V

    .line 1344
    if-lez v4, :cond_a

    .line 1346
    invoke-virtual {v0, p1}, Landroid/content/pm/PackageManager;->getResourcesForApplication(Ljava/lang/String;)Landroid/content/res/Resources;

    move-result-object v5

    .line 1347
    invoke-virtual {v5, v4}, Landroid/content/res/Resources;->getResourceEntryName(I)Ljava/lang/String;

    move-result-object v0

    .line 1349
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    new-instance v6, Ljava/lang/StringBuilder;

    const-string v7, "settings theme : "

    invoke-direct {v6, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v6, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/osp/app/util/aq;->b(Ljava/lang/String;)V

    .line 1351
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    if-lt v0, v8, :cond_8

    .line 1353
    invoke-virtual {v5}, Landroid/content/res/Resources;->newTheme()Landroid/content/res/Resources$Theme;

    move-result-object v0

    const/4 v6, 0x2

    new-array v6, v6, [I

    fill-array-data v6, :array_0

    invoke-virtual {v0, v4, v6}, Landroid/content/res/Resources$Theme;->obtainStyledAttributes(I[I)Landroid/content/res/TypedArray;
    :try_end_0
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v0

    .line 1359
    :goto_3
    if-eqz v0, :cond_b

    .line 1361
    const/4 v3, 0x0

    const/4 v4, 0x0

    :try_start_1
    invoke-virtual {v0, v3, v4}, Landroid/content/res/TypedArray;->getResourceId(II)I

    move-result v3

    .line 1362
    invoke-virtual {v5, v3}, Landroid/content/res/Resources;->getColor(I)I

    move-result v3

    .line 1364
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v4, "Theme"

    new-instance v6, Ljava/lang/StringBuilder;

    const-string v7, "settings text color : "

    invoke-direct {v6, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-static {v3}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v4, v6}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_1
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_1 .. :try_end_1} :catch_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 1368
    const/4 v4, 0x1

    const/4 v6, 0x0

    :try_start_2
    invoke-virtual {v0, v4, v6}, Landroid/content/res/TypedArray;->getResourceId(II)I

    move-result v4

    .line 1369
    invoke-virtual {v5, v4}, Landroid/content/res/Resources;->getResourceEntryName(I)Ljava/lang/String;

    move-result-object v4

    .line 1371
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v5, "Theme"

    new-instance v6, Ljava/lang/StringBuilder;

    const-string v7, "settings theme bg : "

    invoke-direct {v6, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v6, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v5, v4}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_2
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_2 .. :try_end_2} :catch_1
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 1377
    :goto_4
    shr-int/lit8 v4, v3, 0x10

    and-int/lit16 v4, v4, 0xff

    shr-int/lit8 v5, v3, 0x8

    and-int/lit16 v5, v5, 0xff

    and-int/lit16 v6, v3, 0xff

    :try_start_3
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    new-instance v7, Ljava/lang/StringBuilder;

    const-string v8, "isDarkTextColor : "

    invoke-direct {v7, v8}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v7, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, " / "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-static {v3}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v7, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v7, " / "

    invoke-virtual {v3, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v7, ", "

    invoke-virtual {v3, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v7, ", "

    invoke-virtual {v3, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Lcom/osp/app/util/aq;->b(Ljava/lang/String;)V

    add-int v3, v4, v5

    add-int/2addr v3, v6

    div-int/lit8 v3, v3, 0x3
    :try_end_3
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_3 .. :try_end_3} :catch_1
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    const/16 v4, 0x80

    if-ge v3, v4, :cond_9

    move v3, v1

    :goto_5
    if-eqz v3, :cond_b

    .line 1379
    if-eqz v0, :cond_7

    .line 1393
    invoke-virtual {v0}, Landroid/content/res/TypedArray;->recycle()V

    :cond_7
    move v0, v1

    goto/16 :goto_0

    .line 1356
    :cond_8
    :try_start_4
    invoke-virtual {v5}, Landroid/content/res/Resources;->newTheme()Landroid/content/res/Resources$Theme;

    move-result-object v0

    const/4 v6, 0x2

    new-array v6, v6, [I

    fill-array-data v6, :array_1

    invoke-virtual {v0, v4, v6}, Landroid/content/res/Resources$Theme;->obtainStyledAttributes(I[I)Landroid/content/res/TypedArray;
    :try_end_4
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_4 .. :try_end_4} :catch_0
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    move-result-object v0

    goto/16 :goto_3

    :cond_9
    move v3, v2

    .line 1377
    goto :goto_5

    :cond_a
    move-object v0, v3

    .line 1391
    :cond_b
    if-eqz v0, :cond_c

    .line 1393
    invoke-virtual {v0}, Landroid/content/res/TypedArray;->recycle()V

    :cond_c
    move v0, v2

    .line 1397
    goto/16 :goto_0

    .line 1387
    :catch_0
    move-exception v0

    move-object v0, v3

    :goto_6
    :try_start_5
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v3, "Theme::getTheme::NameNotFoundException,"

    invoke-direct {v1, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/osp/app/util/aq;->d(Ljava/lang/String;)V
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_1

    .line 1388
    if-eqz v0, :cond_d

    .line 1393
    invoke-virtual {v0}, Landroid/content/res/TypedArray;->recycle()V

    :cond_d
    move v0, v2

    goto/16 :goto_0

    .line 1391
    :catchall_0
    move-exception v0

    :goto_7
    if-eqz v3, :cond_e

    .line 1393
    invoke-virtual {v3}, Landroid/content/res/TypedArray;->recycle()V

    :cond_e
    throw v0

    .line 1391
    :catchall_1
    move-exception v1

    move-object v3, v0

    move-object v0, v1

    goto :goto_7

    .line 1387
    :catch_1
    move-exception v1

    goto :goto_6

    :catch_2
    move-exception v4

    goto/16 :goto_4

    :cond_f
    move-object v4, v3

    move v5, v2

    goto/16 :goto_1

    :cond_10
    move-object v0, v3

    move-object v4, v3

    move v5, v2

    goto/16 :goto_1

    :cond_11
    move-object v4, v0

    move v5, v2

    move-object v0, v3

    goto/16 :goto_1

    .line 1353
    :array_0
    .array-data 4
        0x1010036
        0x1010054
    .end array-data

    .line 1356
    :array_1
    .array-data 4
        0x1010038
        0x1010054
    .end array-data
.end method

.method public final b(I)Z
    .locals 11

    .prologue
    const v3, 0x1030129

    const v2, 0x103000b

    const/16 v10, 0xe

    const/16 v9, 0xd

    const/16 v8, 0xa

    .line 1482
    const/4 v4, 0x0

    .line 1491
    if-gtz p1, :cond_0

    .line 1493
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "Theme::setTheme::aThemeType is wrong value, "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/osp/app/util/aq;->d(Ljava/lang/String;)V

    move v0, v4

    .line 1522
    :goto_0
    return v0

    .line 1497
    :cond_0
    sget v5, Landroid/os/Build$VERSION;->SDK_INT:I

    .line 1502
    const/4 v1, -0x1

    iget-object v0, p0, Lcom/osp/app/util/am;->c:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/Service;

    if-nez v0, :cond_1

    const/4 v0, -0x1

    move v1, v0

    .line 1503
    :goto_1
    const/4 v0, -0x1

    if-ne v1, v0, :cond_30

    .line 1505
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "Theme::setTheme::aThemeType is wrong value, "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "Api :"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/osp/app/util/aq;->d(Ljava/lang/String;)V

    move v0, v4

    .line 1506
    goto :goto_0

    .line 1502
    :cond_1
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    new-instance v6, Ljava/lang/StringBuilder;

    const-string v7, "Theme isUnderQHD? : "

    invoke-direct {v6, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-static {v0}, Lcom/osp/app/util/x;->n(Landroid/content/Context;)Z

    move-result v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v6}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;)V

    invoke-static {}, Lcom/osp/device/b;->b()Lcom/osp/device/b;

    move-result-object v6

    invoke-virtual {v6, v0}, Lcom/osp/device/b;->i(Landroid/content/Context;)Z

    move-result v6

    packed-switch p1, :pswitch_data_0

    :cond_2
    move v0, v1

    :goto_2
    move v1, v0

    goto :goto_1

    :pswitch_0
    if-gt v5, v8, :cond_3

    const v0, 0x1030005

    goto :goto_2

    :cond_3
    if-gt v5, v9, :cond_4

    const v0, 0x103006b

    goto :goto_2

    :cond_4
    if-lt v5, v10, :cond_2

    const v0, 0x7f0a0015

    goto :goto_2

    :pswitch_1
    if-gt v5, v8, :cond_5

    const v0, 0x103000f

    goto :goto_2

    :cond_5
    if-gt v5, v9, :cond_6

    const v0, 0x103007d

    goto :goto_2

    :cond_6
    if-lt v5, v10, :cond_2

    const v0, 0x103013c

    goto :goto_2

    :pswitch_2
    if-gt v5, v8, :cond_7

    move v0, v2

    goto :goto_2

    :cond_7
    if-gt v5, v9, :cond_8

    const v0, 0x103006f

    goto :goto_2

    :cond_8
    if-lt v5, v10, :cond_2

    const v0, 0x103012e

    goto :goto_2

    :pswitch_3
    if-gt v5, v8, :cond_9

    move v0, v2

    goto :goto_2

    :cond_9
    if-gt v5, v9, :cond_a

    const v0, 0x1030071

    goto :goto_2

    :cond_a
    if-lt v5, v10, :cond_2

    const v0, 0x1030130

    goto :goto_2

    :pswitch_4
    if-gt v5, v8, :cond_b

    const v0, 0x1030006

    goto :goto_2

    :cond_b
    if-gt v5, v9, :cond_c

    const v0, 0x103006c

    goto :goto_2

    :cond_c
    if-lt v5, v10, :cond_2

    move v0, v3

    goto :goto_2

    :pswitch_5
    if-gt v5, v8, :cond_d

    const v0, 0x1030010

    goto :goto_2

    :cond_d
    if-gt v5, v9, :cond_e

    const v0, 0x103007e

    goto :goto_2

    :cond_e
    if-lt v5, v10, :cond_2

    const v0, 0x103013d

    goto :goto_2

    :pswitch_6
    if-gt v5, v8, :cond_f

    const v0, 0x1030006

    goto :goto_2

    :cond_f
    if-gt v5, v9, :cond_10

    const v0, 0x103006c

    goto :goto_2

    :cond_10
    if-lt v5, v10, :cond_2

    move v0, v3

    goto :goto_2

    :pswitch_7
    if-gt v5, v8, :cond_11

    const v0, 0x103000c

    goto :goto_2

    :cond_11
    if-gt v5, v9, :cond_12

    const v0, 0x103006e

    goto/16 :goto_2

    :cond_12
    if-lt v5, v10, :cond_2

    invoke-static {}, Lcom/osp/device/b;->b()Lcom/osp/device/b;

    move-result-object v2

    invoke-virtual {v2}, Lcom/osp/device/b;->e()Ljava/lang/String;

    move-result-object v2

    if-eqz v2, :cond_14

    const-string v3, "SHV-E150S"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_13

    const-string v3, "GT-6800"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_14

    :cond_13
    const v0, 0x7f0a0015

    goto/16 :goto_2

    :cond_14
    invoke-static {}, Lcom/osp/app/signin/SamsungService;->a()Z

    move-result v2

    if-eqz v2, :cond_2

    if-nez v6, :cond_2

    invoke-static {v0}, Lcom/osp/app/util/x;->n(Landroid/content/Context;)Z

    move-result v0

    if-nez v0, :cond_2

    const v0, 0x7f0a000f

    goto/16 :goto_2

    :pswitch_8
    if-gt v5, v8, :cond_15

    move v0, v2

    goto/16 :goto_2

    :cond_15
    if-gt v5, v9, :cond_16

    const v0, 0x1030073

    goto/16 :goto_2

    :cond_16
    if-lt v5, v10, :cond_2

    invoke-static {}, Lcom/osp/device/b;->b()Lcom/osp/device/b;

    move-result-object v0

    invoke-virtual {v0}, Lcom/osp/device/b;->e()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_2

    const-string v1, "SHV-E150S"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_17

    const-string v1, "GT-6800"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_18

    :cond_17
    const v0, 0x103012e

    goto/16 :goto_2

    :cond_18
    const v0, 0x1030132

    goto/16 :goto_2

    :pswitch_9
    if-gt v5, v8, :cond_19

    move v0, v2

    goto/16 :goto_2

    :cond_19
    if-gt v5, v9, :cond_1a

    const v0, 0x1030075

    goto/16 :goto_2

    :cond_1a
    if-lt v5, v10, :cond_2

    invoke-static {}, Lcom/osp/device/b;->b()Lcom/osp/device/b;

    move-result-object v0

    invoke-virtual {v0}, Lcom/osp/device/b;->e()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_2

    const-string v1, "SHV-E150S"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_1b

    const-string v1, "GT-6800"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1c

    :cond_1b
    const v0, 0x1030130

    goto/16 :goto_2

    :cond_1c
    const v0, 0x1030134

    goto/16 :goto_2

    :pswitch_a
    if-gt v5, v8, :cond_1d

    const v0, 0x103000d

    goto/16 :goto_2

    :cond_1d
    if-gt v5, v9, :cond_1e

    const v0, 0x10300f0

    goto/16 :goto_2

    :cond_1e
    if-lt v5, v10, :cond_2

    invoke-static {}, Lcom/osp/device/b;->b()Lcom/osp/device/b;

    move-result-object v0

    invoke-virtual {v0}, Lcom/osp/device/b;->e()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_2

    const-string v1, "SHV-E150S"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_1f

    const-string v1, "GT-6800"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_20

    :cond_1f
    move v0, v3

    goto/16 :goto_2

    :cond_20
    const v0, 0x103012c

    goto/16 :goto_2

    :pswitch_b
    if-gt v5, v8, :cond_21

    const v0, 0x103000d

    goto/16 :goto_2

    :cond_21
    if-gt v5, v9, :cond_22

    const v0, 0x10300f0

    goto/16 :goto_2

    :cond_22
    if-lt v5, v10, :cond_2

    invoke-static {}, Lcom/osp/device/b;->b()Lcom/osp/device/b;

    move-result-object v0

    invoke-virtual {v0}, Lcom/osp/device/b;->e()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_2

    const-string v1, "SHV-E150S"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_23

    const-string v1, "GT-6800"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_24

    :cond_23
    move v0, v3

    goto/16 :goto_2

    :cond_24
    const v0, 0x103012c

    goto/16 :goto_2

    :pswitch_c
    if-gt v5, v8, :cond_25

    const v0, 0x1030008

    goto/16 :goto_2

    :cond_25
    if-gt v5, v9, :cond_26

    const v0, 0x103006b

    goto/16 :goto_2

    :cond_26
    if-lt v5, v10, :cond_2

    invoke-static {}, Lcom/osp/app/signin/SamsungService;->a()Z

    move-result v1

    if-eqz v1, :cond_27

    if-nez v6, :cond_27

    invoke-static {v0}, Lcom/osp/app/util/x;->n(Landroid/content/Context;)Z

    move-result v0

    if-nez v0, :cond_27

    const v0, 0x7f0a0011

    goto/16 :goto_2

    :cond_27
    const v0, 0x7f0a0015

    goto/16 :goto_2

    :pswitch_d
    if-gt v5, v8, :cond_28

    move v0, v2

    goto/16 :goto_2

    :cond_28
    if-gt v5, v9, :cond_29

    const v0, 0x103006f

    goto/16 :goto_2

    :cond_29
    if-lt v5, v10, :cond_2

    const v0, 0x103012e

    goto/16 :goto_2

    :pswitch_e
    if-gt v5, v8, :cond_2a

    move v0, v2

    goto/16 :goto_2

    :cond_2a
    if-gt v5, v9, :cond_2b

    const v0, 0x1030071

    goto/16 :goto_2

    :cond_2b
    if-lt v5, v10, :cond_2

    const v0, 0x1030130

    goto/16 :goto_2

    :pswitch_f
    if-gt v5, v8, :cond_2c

    const v0, 0x1030009

    goto/16 :goto_2

    :cond_2c
    if-gt v5, v9, :cond_2d

    const v0, 0x103006c

    goto/16 :goto_2

    :cond_2d
    if-lt v5, v10, :cond_2

    move v0, v3

    goto/16 :goto_2

    :pswitch_10
    if-gt v5, v8, :cond_2e

    const v0, 0x1030009

    goto/16 :goto_2

    :cond_2e
    if-gt v5, v9, :cond_2f

    const v0, 0x103006c

    goto/16 :goto_2

    :cond_2f
    if-lt v5, v10, :cond_2

    move v0, v3

    goto/16 :goto_2

    .line 1509
    :cond_30
    iput v1, p0, Lcom/osp/app/util/am;->a:I

    .line 1511
    iget-object v0, p0, Lcom/osp/app/util/am;->c:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/Service;

    .line 1512
    if-nez v0, :cond_31

    .line 1514
    const/4 v0, 0x0

    goto/16 :goto_0

    .line 1516
    :cond_31
    invoke-virtual {v0, v1}, Landroid/app/Service;->setTheme(I)V

    .line 1518
    const/4 v0, 0x1

    goto/16 :goto_0

    .line 1502
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_7
        :pswitch_8
        :pswitch_9
        :pswitch_a
        :pswitch_b
        :pswitch_c
        :pswitch_d
        :pswitch_e
        :pswitch_f
        :pswitch_10
    .end packed-switch
.end method
