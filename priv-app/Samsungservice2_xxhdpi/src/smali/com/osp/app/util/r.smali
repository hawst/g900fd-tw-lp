.class public final Lcom/osp/app/util/r;
.super Ljava/lang/Object;
.source "LocalBusinessException.java"


# direct methods
.method public static a()Z
    .locals 2

    .prologue
    .line 246
    invoke-static {}, Lcom/osp/device/b;->b()Lcom/osp/device/b;

    move-result-object v0

    invoke-virtual {v0}, Lcom/osp/device/b;->c()Ljava/lang/String;

    move-result-object v0

    .line 250
    if-eqz v0, :cond_0

    const-string v1, "CHM"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static a(Landroid/app/Activity;)Z
    .locals 9

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 125
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v3, 0x15

    if-lt v0, v3, :cond_0

    .line 127
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v0, "ActionBar Theme::L OS light theme"

    invoke-static {v0}, Lcom/osp/app/util/aq;->c(Ljava/lang/String;)V

    move v0, v1

    .line 176
    :goto_0
    return v0

    .line 131
    :cond_0
    invoke-static {p0}, Lcom/osp/app/util/r;->b(Landroid/app/Activity;)Z

    move-result v0

    if-eqz v0, :cond_1

    move v0, v2

    .line 133
    goto :goto_0

    .line 138
    :cond_1
    :try_start_0
    invoke-static {}, Landroid/content/res/Resources;->getSystem()Landroid/content/res/Resources;

    move-result-object v0

    const-string v3, "action_bar_title"

    const-string v4, "id"

    const-string v5, "android"

    invoke-virtual {v0, v3, v4, v5}, Landroid/content/res/Resources;->getIdentifier(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)I

    move-result v0

    .line 140
    if-lez v0, :cond_3

    .line 142
    invoke-virtual {p0, v0}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 143
    invoke-virtual {v0}, Landroid/widget/TextView;->getTextColors()Landroid/content/res/ColorStateList;

    move-result-object v0

    .line 144
    invoke-virtual {v0}, Landroid/content/res/ColorStateList;->getDefaultColor()I

    move-result v5

    .line 146
    invoke-static {v5}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    move-result-object v6

    .line 152
    invoke-virtual {v6}, Ljava/lang/String;->length()I

    move-result v0

    const/16 v3, 0x8

    if-ne v0, v3, :cond_2

    .line 154
    const/4 v0, 0x2

    const/4 v3, 0x4

    invoke-virtual {v6, v0, v3}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v0

    const/16 v3, 0x10

    invoke-static {v0, v3}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;I)I

    move-result v4

    .line 155
    const/4 v0, 0x4

    const/4 v3, 0x6

    invoke-virtual {v6, v0, v3}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v0

    const/16 v3, 0x10

    invoke-static {v0, v3}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;I)I

    move-result v3

    .line 156
    const/4 v0, 0x6

    invoke-virtual {v6, v0}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v0

    const/16 v7, 0x10

    invoke-static {v0, v7}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;I)I

    move-result v0

    .line 164
    :goto_1
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    new-instance v7, Ljava/lang/StringBuilder;

    const-string v8, "ActionBar Text Color : "

    invoke-direct {v7, v8}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v7, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v7, " / "

    invoke-virtual {v5, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, " / "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, ", "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, ", "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Lcom/osp/app/util/aq;->b(Ljava/lang/String;)V

    .line 166
    add-int/2addr v3, v4

    add-int/2addr v0, v3

    div-int/lit8 v0, v0, 0x3

    const/16 v3, 0x80

    if-lt v0, v3, :cond_3

    move v0, v2

    .line 168
    goto/16 :goto_0

    .line 159
    :cond_2
    const/4 v0, 0x0

    const/4 v3, 0x2

    invoke-virtual {v6, v0, v3}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v0

    const/16 v3, 0x10

    invoke-static {v0, v3}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;I)I

    move-result v4

    .line 160
    const/4 v0, 0x2

    const/4 v3, 0x4

    invoke-virtual {v6, v0, v3}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v0

    const/16 v3, 0x10

    invoke-static {v0, v3}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;I)I

    move-result v3

    .line 161
    const/4 v0, 0x4

    invoke-virtual {v6, v0}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v0

    const/16 v7, 0x10

    invoke-static {v0, v7}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;I)I
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result v0

    goto :goto_1

    .line 173
    :catch_0
    move-exception v0

    move v0, v1

    goto/16 :goto_0

    :cond_3
    move v0, v1

    .line 176
    goto/16 :goto_0
.end method

.method public static a(Landroid/content/Context;)Z
    .locals 5

    .prologue
    const/4 v0, 0x0

    .line 91
    .line 92
    invoke-virtual {p0}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v1

    .line 97
    :try_start_0
    const-string v2, "com.osp.app.signin"

    const/16 v3, 0x1000

    invoke-virtual {v1, v2, v3}, Landroid/content/pm/PackageManager;->getPackageInfo(Ljava/lang/String;I)Landroid/content/pm/PackageInfo;

    move-result-object v2

    .line 98
    if-eqz v2, :cond_0

    iget-object v1, v2, Landroid/content/pm/PackageInfo;->permissions:[Landroid/content/pm/PermissionInfo;

    if-eqz v1, :cond_0

    move v1, v0

    .line 100
    :goto_0
    iget-object v3, v2, Landroid/content/pm/PackageInfo;->permissions:[Landroid/content/pm/PermissionInfo;

    array-length v3, v3

    if-ge v1, v3, :cond_0

    .line 102
    if-eqz v2, :cond_1

    iget-object v3, v2, Landroid/content/pm/PackageInfo;->permissions:[Landroid/content/pm/PermissionInfo;

    aget-object v3, v3, v1

    if-eqz v3, :cond_1

    const-string v3, "com.osp.contentprovider.OSPContentProvider.READ_CONTENT"

    iget-object v4, v2, Landroid/content/pm/PackageInfo;->permissions:[Landroid/content/pm/PermissionInfo;

    aget-object v4, v4, v1

    iget-object v4, v4, Landroid/content/pm/PermissionInfo;->name:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
    :try_end_0
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v3

    if-eqz v3, :cond_1

    .line 104
    const/4 v0, 0x1

    .line 113
    :cond_0
    :goto_1
    return v0

    .line 100
    :cond_1
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 109
    :catch_0
    move-exception v1

    invoke-virtual {v1}, Landroid/content/pm/PackageManager$NameNotFoundException;->printStackTrace()V

    goto :goto_1
.end method

.method public static a(Ljava/lang/String;)Z
    .locals 6

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 1943
    const/4 v2, 0x7

    new-array v3, v2, [Ljava/lang/String;

    const-string v2, "310"

    aput-object v2, v3, v1

    const-string v2, "311"

    aput-object v2, v3, v0

    const/4 v2, 0x2

    const-string v4, "312"

    aput-object v4, v3, v2

    const/4 v2, 0x3

    const-string v4, "313"

    aput-object v4, v3, v2

    const/4 v2, 0x4

    const-string v4, "314"

    aput-object v4, v3, v2

    const/4 v2, 0x5

    const-string v4, "315"

    aput-object v4, v3, v2

    const/4 v2, 0x6

    const-string v4, "316"

    aput-object v4, v3, v2

    .line 1945
    array-length v4, v3

    move v2, v1

    :goto_0
    if-ge v2, v4, :cond_1

    aget-object v5, v3, v2

    .line 1947
    invoke-virtual {v5, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_0

    .line 1953
    :goto_1
    return v0

    .line 1945
    :cond_0
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    :cond_1
    move v0, v1

    .line 1953
    goto :goto_1
.end method

.method public static b()Z
    .locals 3

    .prologue
    .line 259
    invoke-static {}, Lcom/osp/device/b;->b()Lcom/osp/device/b;

    move-result-object v0

    invoke-virtual {v0}, Lcom/osp/device/b;->n()Ljava/lang/String;

    move-result-object v0

    .line 260
    const-string v1, "CN"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 263
    const/4 v0, 0x1

    .line 268
    :goto_0
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "isChinaCountryCode : "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/osp/app/util/aq;->c(Ljava/lang/String;)V

    .line 269
    return v0

    .line 266
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private static b(Landroid/app/Activity;)Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 198
    invoke-static {}, Lcom/osp/device/b;->b()Lcom/osp/device/b;

    move-result-object v1

    invoke-virtual {v1, p0}, Lcom/osp/device/b;->i(Landroid/content/Context;)Z

    move-result v1

    if-nez v1, :cond_1

    .line 227
    :cond_0
    :goto_0
    return v0

    .line 212
    :cond_1
    :try_start_0
    invoke-static {}, Lcom/osp/common/util/i;->a()Lcom/osp/common/util/i;

    invoke-static {}, Lcom/osp/common/util/i;->c()Ljava/lang/String;

    move-result-object v1

    .line 216
    if-eqz v1, :cond_0

    .line 218
    const-string v2, "SHV-E150S"

    invoke-virtual {v2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_2

    const-string v2, "GT-P6800"

    invoke-virtual {v2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    if-eqz v1, :cond_0

    .line 220
    :cond_2
    const/4 v0, 0x1

    goto :goto_0

    .line 223
    :catch_0
    move-exception v1

    invoke-virtual {v1}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_0
.end method

.method public static b(Landroid/content/Context;)Z
    .locals 6

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 278
    invoke-static {}, Lcom/osp/common/util/i;->a()Lcom/osp/common/util/i;

    invoke-static {p0}, Lcom/osp/app/util/ad;->p(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/osp/common/util/i;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 285
    if-eqz v2, :cond_2

    .line 287
    const-string v3, "CN"

    sget-object v4, Ljava/util/Locale;->ENGLISH:Ljava/util/Locale;

    invoke-virtual {v2, v4}, Ljava/lang/String;->toUpperCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    .line 290
    const-string v4, "KR"

    sget-object v5, Ljava/util/Locale;->ENGLISH:Ljava/util/Locale;

    invoke-virtual {v2, v5}, Ljava/lang/String;->toUpperCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v4, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    or-int/2addr v2, v3

    .line 293
    if-eqz v2, :cond_1

    .line 320
    :cond_0
    :goto_0
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "isCountryCode : "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/osp/app/util/aq;->c(Ljava/lang/String;)V

    .line 321
    return v0

    :cond_1
    move v0, v1

    .line 298
    goto :goto_0

    .line 302
    :cond_2
    invoke-static {}, Lcom/osp/device/b;->b()Lcom/osp/device/b;

    move-result-object v2

    invoke-virtual {v2}, Lcom/osp/device/b;->n()Ljava/lang/String;

    move-result-object v2

    .line 304
    const-string v3, "CN"

    sget-object v4, Ljava/util/Locale;->ENGLISH:Ljava/util/Locale;

    invoke-virtual {v2, v4}, Ljava/lang/String;->toUpperCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    .line 307
    const-string v4, "KR"

    sget-object v5, Ljava/util/Locale;->ENGLISH:Ljava/util/Locale;

    invoke-virtual {v2, v5}, Ljava/lang/String;->toUpperCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v4, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    or-int/2addr v2, v3

    .line 310
    if-nez v2, :cond_0

    move v0, v1

    .line 315
    goto :goto_0
.end method

.method public static c()Z
    .locals 2

    .prologue
    .line 338
    invoke-static {}, Lcom/osp/device/b;->b()Lcom/osp/device/b;

    move-result-object v0

    invoke-virtual {v0}, Lcom/osp/device/b;->c()Ljava/lang/String;

    move-result-object v0

    .line 341
    const-string v1, "CTC"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 343
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v0, "LBE"

    const-string v1, "Block Service After Boot"

    invoke-static {v0, v1}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 344
    const/4 v0, 0x1

    .line 347
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static c(Landroid/content/Context;)Z
    .locals 8

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x1

    .line 452
    .line 455
    :try_start_0
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    .line 459
    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    invoke-virtual {v3}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v3

    iget-object v3, v3, Landroid/content/res/Configuration;->locale:Ljava/util/Locale;

    .line 460
    if-eqz v3, :cond_1

    .line 462
    invoke-virtual {v3}, Ljava/util/Locale;->getLanguage()Ljava/lang/String;

    move-result-object v4

    .line 466
    const/16 v5, 0xe

    if-gt v5, v0, :cond_2

    .line 468
    const/4 v0, -0x1

    .line 469
    const-string v4, "android.util.LocaleUtil"

    invoke-static {v4}, Ljava/lang/Class;->forName(Ljava/lang/String;)Ljava/lang/Class;

    move-result-object v4

    .line 470
    if-eqz v4, :cond_0

    .line 475
    const-string v0, "getLayoutDirectionFromLocale"

    const/4 v5, 0x1

    new-array v5, v5, [Ljava/lang/Class;

    const/4 v6, 0x0

    const-class v7, Ljava/util/Locale;

    aput-object v7, v5, v6

    invoke-virtual {v4, v0, v5}, Ljava/lang/Class;->getMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v0

    const/4 v5, 0x1

    new-array v5, v5, [Ljava/lang/Object;

    const/4 v6, 0x0

    aput-object v3, v5, v6

    invoke-virtual {v0, v4, v5}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result v0

    .line 479
    :cond_0
    if-ne v0, v1, :cond_5

    move v0, v1

    :goto_0
    move v2, v0

    .line 495
    :cond_1
    :goto_1
    if-eqz v2, :cond_4

    .line 497
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v0, "LBE"

    const-string v1, "layout direction is rtl"

    invoke-static {v0, v1}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 502
    :goto_2
    return v2

    .line 485
    :cond_2
    :try_start_1
    const-string v0, "ar"

    invoke-virtual {v0, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_3

    const-string v0, "he"

    invoke-virtual {v0, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_3

    const-string v0, "iw"

    invoke-virtual {v0, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    move-result v0

    if-eqz v0, :cond_1

    :cond_3
    move v2, v1

    .line 487
    goto :goto_1

    .line 491
    :catch_0
    move-exception v0

    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_1

    .line 500
    :cond_4
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v0, "LBE"

    const-string v1, "layout direction is ltr"

    invoke-static {v0, v1}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_2

    :cond_5
    move v0, v2

    goto :goto_0
.end method

.method public static d()Z
    .locals 6

    .prologue
    const/4 v0, 0x1

    .line 367
    invoke-static {}, Lcom/osp/device/b;->b()Lcom/osp/device/b;

    move-result-object v1

    invoke-virtual {v1}, Lcom/osp/device/b;->g()Ljava/lang/String;

    move-result-object v2

    .line 374
    invoke-static {}, Lcom/osp/device/b;->b()Lcom/osp/device/b;

    move-result-object v1

    invoke-virtual {v1}, Lcom/osp/device/b;->e()Ljava/lang/String;

    move-result-object v3

    .line 377
    invoke-static {}, Lcom/osp/device/b;->b()Lcom/osp/device/b;

    move-result-object v1

    invoke-virtual {v1}, Lcom/osp/device/b;->c()Ljava/lang/String;

    move-result-object v4

    .line 380
    const/4 v1, 0x0

    .line 386
    const-string v5, "VZW"

    invoke-virtual {v5, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_1

    .line 435
    :cond_0
    :goto_0
    if-eqz v0, :cond_3

    .line 437
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v1, "LBE"

    const-string v2, "exculde Notification icon !!! "

    invoke-static {v1, v2}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 442
    :goto_1
    return v0

    .line 389
    :cond_1
    if-eqz v3, :cond_2

    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v4

    if-lez v4, :cond_2

    const-string v4, "unknown"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_2

    .line 393
    const-string v4, "-"

    invoke-virtual {v3, v4}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v4

    .line 394
    if-lez v4, :cond_2

    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v5

    add-int/lit8 v5, v5, -0x1

    if-ge v4, v5, :cond_2

    .line 396
    add-int/lit8 v4, v4, 0x1

    invoke-virtual {v3, v4}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v3

    .line 398
    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-nez v4, :cond_2

    .line 402
    invoke-virtual {v2, v3}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v4

    if-nez v4, :cond_2

    .line 404
    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v3

    .line 405
    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v4

    add-int/lit8 v5, v3, 0x2

    if-le v4, v5, :cond_2

    .line 407
    add-int/lit8 v4, v3, 0x2

    invoke-virtual {v2, v3, v4}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v2

    .line 411
    const-string v3, "BV"

    invoke-virtual {v3, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    :cond_2
    move v0, v1

    goto :goto_0

    .line 440
    :cond_3
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v1, "LBE"

    const-string v2, "include Notification icon !!!"

    invoke-static {v1, v2}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1
.end method

.method public static d(Landroid/content/Context;)Z
    .locals 3

    .prologue
    .line 585
    const/4 v0, 0x0

    .line 588
    :try_start_0
    invoke-static {}, Lcom/osp/common/util/i;->a()Lcom/osp/common/util/i;

    .line 591
    invoke-static {p0}, Lcom/osp/common/util/i;->e(Landroid/content/Context;)Z

    move-result v1

    if-eqz v1, :cond_3

    .line 594
    invoke-static {p0}, Lcom/osp/common/util/i;->h(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v1

    .line 595
    if-eqz v1, :cond_2

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v2

    if-lez v2, :cond_2

    .line 597
    invoke-static {p0, v1}, Lcom/osp/app/util/ad;->e(Landroid/content/Context;Ljava/lang/String;)V

    .line 606
    :goto_0
    const-string v2, "460"

    invoke-virtual {v2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    const-string v2, "461"

    invoke-virtual {v2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 608
    :cond_0
    const/4 v0, 0x1

    .line 633
    :cond_1
    :goto_1
    return v0

    .line 600
    :cond_2
    invoke-static {p0}, Lcom/osp/app/util/ad;->o(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v1

    goto :goto_0

    .line 604
    :cond_3
    invoke-static {p0}, Lcom/osp/app/util/ad;->o(Landroid/content/Context;)Ljava/lang/String;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    goto :goto_0

    .line 629
    :catch_0
    move-exception v1

    invoke-virtual {v1}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_1
.end method

.method public static e()Z
    .locals 7

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 646
    invoke-static {}, Lcom/osp/device/b;->b()Lcom/osp/device/b;

    move-result-object v2

    invoke-virtual {v2}, Lcom/osp/device/b;->e()Ljava/lang/String;

    move-result-object v3

    .line 657
    const/16 v2, 0xa

    new-array v4, v2, [Ljava/lang/String;

    const-string v2, "SHV-E120L"

    aput-object v2, v4, v1

    const-string v2, "E120L"

    aput-object v2, v4, v0

    const/4 v2, 0x2

    const-string v5, "SHV-E140L"

    aput-object v5, v4, v2

    const/4 v2, 0x3

    const-string v5, "E140L"

    aput-object v5, v4, v2

    const/4 v2, 0x4

    const-string v5, "SHV-E160L"

    aput-object v5, v4, v2

    const/4 v2, 0x5

    const-string v5, "E160L"

    aput-object v5, v4, v2

    const/4 v2, 0x6

    const-string v5, "EK-KC120L"

    aput-object v5, v4, v2

    const/4 v2, 0x7

    const-string v5, "KC120L"

    aput-object v5, v4, v2

    const/16 v2, 0x8

    const-string v5, "SHV-E270L"

    aput-object v5, v4, v2

    const/16 v2, 0x9

    const-string v5, "E270L"

    aput-object v5, v4, v2

    .line 659
    array-length v5, v4

    move v2, v1

    :goto_0
    if-ge v2, v5, :cond_1

    aget-object v6, v4, v2

    .line 661
    invoke-virtual {v6, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_0

    .line 668
    :goto_1
    return v0

    .line 659
    :cond_0
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    :cond_1
    move v0, v1

    goto :goto_1
.end method

.method public static e(Landroid/content/Context;)Z
    .locals 1

    .prologue
    .line 703
    invoke-static {}, Lcom/osp/app/signin/SamsungService;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 705
    invoke-static {}, Lcom/osp/device/b;->b()Lcom/osp/device/b;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/osp/device/b;->i(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 707
    const/4 v0, 0x1

    .line 711
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static f()Z
    .locals 2

    .prologue
    .line 677
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x13

    if-gt v0, v1, :cond_1

    .line 679
    sget-object v0, Lcom/osp/app/util/u;->k:[Ljava/lang/String;

    invoke-static {v0}, Lcom/osp/app/util/u;->a([Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    sget-object v0, Lcom/osp/app/util/u;->l:[Ljava/lang/String;

    invoke-static {v0}, Lcom/osp/app/util/u;->a([Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 681
    :cond_0
    const/4 v0, 0x1

    .line 684
    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static f(Landroid/content/Context;)Z
    .locals 7

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 973
    invoke-static {p0}, Lcom/osp/app/util/ad;->p(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/osp/common/util/i;->d(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 975
    const/16 v2, 0x8

    new-array v4, v2, [Ljava/lang/String;

    const-string v2, "KOR"

    aput-object v2, v4, v1

    const-string v2, "DEU"

    aput-object v2, v4, v0

    const/4 v2, 0x2

    const-string v5, "ZAF"

    aput-object v5, v4, v2

    const/4 v2, 0x3

    const-string v5, "NLD"

    aput-object v5, v4, v2

    const/4 v2, 0x4

    const-string v5, "BEL"

    aput-object v5, v4, v2

    const/4 v2, 0x5

    const-string v5, "CAN"

    aput-object v5, v4, v2

    const/4 v2, 0x6

    const-string v5, "AUS"

    aput-object v5, v4, v2

    const/4 v2, 0x7

    const-string v5, "ITA"

    aput-object v5, v4, v2

    .line 977
    array-length v5, v4

    move v2, v1

    :goto_0
    if-ge v2, v5, :cond_1

    aget-object v6, v4, v2

    .line 979
    invoke-virtual {v6, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_0

    .line 981
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "isRestrictedAutoCheck CountryName : true | "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;)V

    .line 987
    :goto_1
    return v0

    .line 977
    :cond_0
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 986
    :cond_1
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v2, "isRestrictedAutoCheck CountryName : false | "

    invoke-direct {v0, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;)V

    move v0, v1

    .line 987
    goto :goto_1
.end method

.method public static g()Z
    .locals 4

    .prologue
    const/4 v0, 0x0

    .line 734
    :try_start_0
    invoke-static {}, Lcom/osp/common/util/i;->a()Lcom/osp/common/util/i;

    invoke-static {}, Lcom/osp/common/util/i;->c()Ljava/lang/String;

    move-result-object v1

    sget-object v2, Ljava/util/Locale;->ENGLISH:Ljava/util/Locale;

    invoke-virtual {v1, v2}, Ljava/lang/String;->toUpperCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v1

    .line 735
    const-string v2, "SM-P907A"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    const-string v2, "SM-T537A"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    const-string v2, "SM-T337A"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 737
    :cond_0
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v1, "LBE"

    const-string v2, "This model is located LastInSetupWizard"

    invoke-static {v1, v2}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 738
    const/4 v0, 0x1

    .line 749
    :goto_0
    return v0

    .line 741
    :cond_1
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v1, "LBE"

    const-string v2, "This model is NOT located LastInSetupWizard"

    invoke-static {v1, v2}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 745
    :catch_0
    move-exception v1

    .line 747
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v2, "LBE"

    const-string v3, "Fail to get model ID (LastInSetupWizard)"

    invoke-static {v2, v3}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 748
    invoke-virtual {v1}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_0
.end method

.method public static g(Landroid/content/Context;)Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 1259
    sget v1, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v2, 0x15

    if-lt v1, v2, :cond_0

    .line 1263
    :try_start_0
    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f09029d

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 1265
    if-eqz v1, :cond_0

    const-string v2, "ZERO_UI"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    if-eqz v1, :cond_0

    .line 1267
    const/4 v0, 0x1

    .line 1274
    :cond_0
    :goto_0
    return v0

    .line 1271
    :catch_0
    move-exception v1

    goto :goto_0
.end method

.method public static h()Z
    .locals 2

    .prologue
    .line 778
    invoke-static {}, Lcom/osp/device/b;->b()Lcom/osp/device/b;

    move-result-object v0

    invoke-virtual {v0}, Lcom/osp/device/b;->c()Ljava/lang/String;

    move-result-object v0

    .line 781
    const-string v1, "VZW"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 783
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v0, "isCheckVZW :true"

    invoke-static {v0}, Lcom/osp/app/util/aq;->c(Ljava/lang/String;)V

    .line 784
    const/4 v0, 0x1

    .line 787
    :goto_0
    return v0

    .line 786
    :cond_0
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v0, "isCheckVZW :false"

    invoke-static {v0}, Lcom/osp/app/util/aq;->c(Ljava/lang/String;)V

    .line 787
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static h(Landroid/content/Context;)Z
    .locals 1

    .prologue
    .line 1283
    invoke-static {p0}, Lcom/osp/app/util/x;->h(Landroid/content/Context;)Z

    move-result v0

    if-nez v0, :cond_0

    invoke-static {p0}, Lcom/osp/app/util/x;->g(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    invoke-static {p0}, Lcom/osp/app/util/r;->k(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-static {}, Lcom/osp/app/util/r;->h()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1285
    const/4 v0, 0x1

    .line 1287
    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static i()Z
    .locals 2

    .prologue
    .line 796
    invoke-static {}, Lcom/osp/device/b;->b()Lcom/osp/device/b;

    move-result-object v0

    invoke-virtual {v0}, Lcom/osp/device/b;->c()Ljava/lang/String;

    move-result-object v0

    .line 799
    const-string v1, "ATT"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 801
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v0, "isCheckATT :true"

    invoke-static {v0}, Lcom/osp/app/util/aq;->c(Ljava/lang/String;)V

    .line 802
    const/4 v0, 0x1

    .line 805
    :goto_0
    return v0

    .line 804
    :cond_0
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v0, "isCheckATT :false"

    invoke-static {v0}, Lcom/osp/app/util/aq;->c(Ljava/lang/String;)V

    .line 805
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static i(Landroid/content/Context;)Z
    .locals 2

    .prologue
    const/4 v0, 0x1

    .line 1308
    invoke-static {p0}, Lcom/osp/app/util/r;->m(Landroid/content/Context;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 1321
    :cond_0
    :goto_0
    return v0

    .line 1311
    :cond_1
    invoke-static {}, Lcom/osp/app/signin/SamsungService;->a()Z

    move-result v1

    if-eqz v1, :cond_3

    .line 1313
    sget-object v1, Lcom/osp/app/util/u;->i:[Ljava/lang/String;

    invoke-static {v1}, Lcom/osp/app/util/u;->a([Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_2

    sget-object v1, Lcom/osp/app/util/u;->h:[Ljava/lang/String;

    invoke-static {v1}, Lcom/osp/app/util/u;->a([Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_2

    sget-object v1, Lcom/osp/app/util/u;->e:[Ljava/lang/String;

    invoke-static {v1}, Lcom/osp/app/util/u;->a([Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_2

    sget-object v1, Lcom/osp/app/util/u;->d:[Ljava/lang/String;

    invoke-static {v1}, Lcom/osp/app/util/u;->a([Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_2

    sget-object v1, Lcom/osp/app/util/u;->b:[Ljava/lang/String;

    invoke-static {v1}, Lcom/osp/app/util/u;->a([Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_2

    sget-object v1, Lcom/osp/app/util/u;->c:[Ljava/lang/String;

    invoke-static {v1}, Lcom/osp/app/util/u;->a([Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_2

    invoke-static {}, Lcom/osp/app/util/r;->m()Z

    move-result v1

    if-nez v1, :cond_2

    sget-object v1, Lcom/osp/app/util/u;->a:[Ljava/lang/String;

    invoke-static {v1}, Lcom/osp/app/util/u;->a([Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_2

    invoke-static {p0}, Lcom/osp/app/util/r;->k(Landroid/content/Context;)Z

    move-result v1

    if-eqz v1, :cond_3

    .line 1315
    :cond_2
    sget-object v1, Lcom/osp/app/util/u;->o:[Ljava/lang/String;

    invoke-static {v1}, Lcom/osp/app/util/u;->a([Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 1321
    :cond_3
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static j()Z
    .locals 2

    .prologue
    .line 867
    invoke-static {}, Lcom/osp/device/b;->b()Lcom/osp/device/b;

    move-result-object v0

    invoke-virtual {v0}, Lcom/osp/device/b;->n()Ljava/lang/String;

    move-result-object v0

    .line 870
    const-string v1, "CA"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 872
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v0, "isCanadaModel : true"

    invoke-static {v0}, Lcom/osp/app/util/aq;->c(Ljava/lang/String;)V

    .line 873
    const/4 v0, 0x1

    .line 877
    :goto_0
    return v0

    .line 876
    :cond_0
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v0, "isCanadaModel : false"

    invoke-static {v0}, Lcom/osp/app/util/aq;->c(Ljava/lang/String;)V

    .line 877
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static j(Landroid/content/Context;)Z
    .locals 1

    .prologue
    .line 1332
    invoke-static {}, Lcom/osp/device/b;->b()Lcom/osp/device/b;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/osp/device/b;->i(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1334
    sget-object v0, Lcom/osp/app/util/u;->h:[Ljava/lang/String;

    invoke-static {v0}, Lcom/osp/app/util/u;->a([Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    invoke-static {p0}, Lcom/osp/app/util/r;->k(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1336
    :cond_0
    const/4 v0, 0x1

    .line 1339
    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static k()Z
    .locals 7

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 930
    sget v2, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v3, 0x13

    if-lt v2, v3, :cond_1

    move v0, v1

    .line 956
    :cond_0
    :goto_0
    return v0

    .line 936
    :cond_1
    invoke-static {}, Lcom/osp/device/b;->b()Lcom/osp/device/b;

    move-result-object v2

    invoke-virtual {v2}, Lcom/osp/device/b;->e()Ljava/lang/String;

    move-result-object v3

    .line 937
    const/16 v2, 0x8

    new-array v4, v2, [Ljava/lang/String;

    const-string v2, "SM-T310"

    aput-object v2, v4, v1

    const-string v2, "SM-T311"

    aput-object v2, v4, v0

    const/4 v2, 0x2

    const-string v5, "SM-T312"

    aput-object v5, v4, v2

    const/4 v2, 0x3

    const-string v5, "SM-T315"

    aput-object v5, v4, v2

    const/4 v2, 0x4

    const-string v5, "GT-P5200"

    aput-object v5, v4, v2

    const/4 v2, 0x5

    const-string v5, "GT-P5205"

    aput-object v5, v4, v2

    const/4 v2, 0x6

    const-string v5, "GT-P5210"

    aput-object v5, v4, v2

    const/4 v2, 0x7

    const-string v5, "GT-P5220"

    aput-object v5, v4, v2

    .line 942
    sget-object v2, Lcom/osp/app/util/u;->j:[Ljava/lang/String;

    invoke-static {v2}, Lcom/osp/app/util/u;->a([Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 947
    array-length v5, v4

    move v2, v1

    :goto_1
    if-ge v2, v5, :cond_2

    aget-object v6, v4, v2

    .line 949
    invoke-virtual {v6, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-nez v6, :cond_0

    .line 947
    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    :cond_2
    move v0, v1

    goto :goto_0
.end method

.method public static k(Landroid/content/Context;)Z
    .locals 2

    .prologue
    .line 1368
    invoke-static {}, Lcom/osp/device/b;->b()Lcom/osp/device/b;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/osp/device/b;->i(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_0

    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x13

    if-lt v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static l()Z
    .locals 4

    .prologue
    const/4 v0, 0x0

    .line 999
    :try_start_0
    invoke-static {}, Lcom/osp/common/util/i;->a()Lcom/osp/common/util/i;

    invoke-static {}, Lcom/osp/common/util/i;->c()Ljava/lang/String;

    move-result-object v1

    sget-object v2, Ljava/util/Locale;->ENGLISH:Ljava/util/Locale;

    invoke-virtual {v1, v2}, Ljava/lang/String;->toUpperCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v1

    .line 1000
    const-string v2, "SCH-I605"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 1002
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v1, "LBE"

    const-string v2, "This model is VZW resources file."

    invoke-static {v1, v2}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 1003
    const/4 v0, 0x1

    .line 1013
    :cond_0
    :goto_0
    return v0

    .line 1009
    :catch_0
    move-exception v1

    .line 1011
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v2, "LBE"

    const-string v3, "Fail to get model ID (isHDmodel)"

    invoke-static {v2, v3}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 1012
    invoke-virtual {v1}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_0
.end method

.method public static l(Landroid/content/Context;)Z
    .locals 2

    .prologue
    .line 1379
    invoke-static {}, Lcom/osp/device/b;->b()Lcom/osp/device/b;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/osp/device/b;->i(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_0

    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x15

    if-lt v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static m()Z
    .locals 2

    .prologue
    .line 1102
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x13

    if-ge v0, v1, :cond_0

    .line 1104
    sget-object v0, Lcom/osp/app/util/u;->g:[Ljava/lang/String;

    invoke-static {v0}, Lcom/osp/app/util/u;->a([Ljava/lang/String;)Z

    move-result v0

    .line 1107
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static m(Landroid/content/Context;)Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 1390
    invoke-static {}, Lcom/osp/device/b;->b()Lcom/osp/device/b;

    move-result-object v1

    invoke-virtual {v1, p0}, Lcom/osp/device/b;->i(Landroid/content/Context;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 1407
    :cond_0
    :goto_0
    return v0

    .line 1397
    :cond_1
    :try_start_0
    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f09029d

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 1399
    if-eqz v1, :cond_0

    const-string v2, "KITKAT_UI"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_2

    const-string v2, "ZERO_UI"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    :cond_2
    sget v1, Landroid/os/Build$VERSION;->SDK_INT:I
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    const/16 v2, 0x13

    if-lt v1, v2, :cond_0

    .line 1401
    const/4 v0, 0x1

    goto :goto_0

    .line 1405
    :catch_0
    move-exception v1

    goto :goto_0
.end method

.method public static n()Z
    .locals 3

    .prologue
    .line 1240
    const/4 v0, 0x1

    new-array v0, v0, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string v2, "SM-A300"

    aput-object v2, v0, v1

    invoke-static {v0}, Lcom/osp/app/util/u;->b([Ljava/lang/String;)Z

    move-result v0

    return v0
.end method

.method public static n(Landroid/content/Context;)Z
    .locals 2

    .prologue
    .line 1418
    invoke-static {p0}, Lcom/osp/app/util/x;->o(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_0

    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x13

    if-ge v0, v1, :cond_0

    .line 1420
    const/4 v0, 0x1

    .line 1423
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static o(Landroid/content/Context;)Lcom/osp/app/util/s;
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 1434
    invoke-static {p0}, Lcom/osp/app/util/r;->k(Landroid/content/Context;)Z

    move-result v1

    if-nez v1, :cond_1

    .line 1462
    :cond_0
    :goto_0
    return-object v0

    .line 1439
    :cond_1
    invoke-static {}, Lcom/osp/device/b;->b()Lcom/osp/device/b;

    move-result-object v1

    invoke-virtual {v1, p0}, Lcom/osp/device/b;->m(Landroid/content/Context;)I

    move-result v1

    .line 1441
    packed-switch v1, :pswitch_data_0

    .line 1453
    :pswitch_0
    invoke-static {}, Lcom/osp/device/b;->b()Lcom/osp/device/b;

    move-result-object v1

    invoke-virtual {v1, p0}, Lcom/osp/device/b;->n(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v1

    .line 1455
    const-string v2, "vienna"

    invoke-virtual {v1, v2}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_2

    const-string v2, "v1a"

    invoke-virtual {v1, v2}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 1457
    :cond_2
    sget-object v0, Lcom/osp/app/util/s;->c:Lcom/osp/app/util/s;

    goto :goto_0

    .line 1444
    :pswitch_1
    sget-object v0, Lcom/osp/app/util/s;->a:Lcom/osp/app/util/s;

    goto :goto_0

    .line 1447
    :pswitch_2
    sget-object v0, Lcom/osp/app/util/s;->b:Lcom/osp/app/util/s;

    goto :goto_0

    .line 1450
    :pswitch_3
    sget-object v0, Lcom/osp/app/util/s;->c:Lcom/osp/app/util/s;

    goto :goto_0

    .line 1441
    nop

    :pswitch_data_0
    .packed-switch 0x8
        :pswitch_1
        :pswitch_0
        :pswitch_2
        :pswitch_0
        :pswitch_3
    .end packed-switch
.end method

.method public static o()Z
    .locals 7

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 1556
    invoke-static {}, Lcom/osp/device/b;->b()Lcom/osp/device/b;

    move-result-object v2

    invoke-virtual {v2}, Lcom/osp/device/b;->e()Ljava/lang/String;

    move-result-object v3

    .line 1557
    const/4 v2, 0x4

    new-array v4, v2, [Ljava/lang/String;

    const-string v2, "SM-G3819D"

    aput-object v2, v4, v1

    const-string v2, "SM-G3819"

    aput-object v2, v4, v0

    const/4 v2, 0x2

    const-string v5, "SM-N9009"

    aput-object v5, v4, v2

    const/4 v2, 0x3

    const-string v5, "SM-G3556D"

    aput-object v5, v4, v2

    .line 1559
    array-length v5, v4

    move v2, v1

    :goto_0
    if-ge v2, v5, :cond_1

    aget-object v6, v4, v2

    .line 1561
    invoke-virtual {v6, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_0

    .line 1563
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v1, "isMSimExcpetedModel : true"

    invoke-static {v1}, Lcom/osp/app/util/aq;->c(Ljava/lang/String;)V

    .line 1567
    :goto_1
    return v0

    .line 1559
    :cond_0
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    :cond_1
    move v0, v1

    .line 1567
    goto :goto_1
.end method

.method public static p()Z
    .locals 1

    .prologue
    .line 1655
    invoke-static {}, Lcom/osp/app/util/r;->s()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1657
    const/4 v0, 0x1

    .line 1660
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static p(Landroid/content/Context;)Z
    .locals 2

    .prologue
    .line 1621
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x15

    if-lt v0, v1, :cond_0

    .line 1625
    invoke-static {}, Lcom/osp/app/util/m;->a()Lcom/osp/app/util/m;

    invoke-static {p0}, Lcom/osp/app/util/m;->a(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-static {}, Lcom/osp/common/util/i;->a()Lcom/osp/common/util/i;

    invoke-static {p0}, Lcom/osp/common/util/i;->e(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1627
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v0, "LBE"

    const-string v1, "isSupportEasySignUpModel : true"

    invoke-static {v0, v1}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 1628
    const/4 v0, 0x1

    .line 1633
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static q()Z
    .locals 1

    .prologue
    .line 1669
    invoke-static {}, Lcom/osp/app/util/r;->s()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1671
    const/4 v0, 0x1

    .line 1674
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static q(Landroid/content/Context;)Z
    .locals 1

    .prologue
    .line 1641
    invoke-static {}, Lcom/osp/device/b;->b()Lcom/osp/device/b;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/osp/device/b;->i(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-static {}, Lcom/osp/app/signin/SamsungService;->a()Z

    move-result v0

    if-nez v0, :cond_0

    .line 1643
    const/4 v0, 0x1

    .line 1646
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static r()Z
    .locals 1

    .prologue
    .line 1693
    invoke-static {}, Lcom/osp/app/signin/SamsungService;->a()Z

    move-result v0

    if-nez v0, :cond_0

    .line 1695
    const/4 v0, 0x1

    .line 1698
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static r(Landroid/content/Context;)Z
    .locals 1

    .prologue
    .line 1727
    invoke-static {}, Lcom/osp/device/b;->b()Lcom/osp/device/b;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/osp/device/b;->i(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1729
    const/4 v0, 0x1

    .line 1731
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static s()Z
    .locals 2

    .prologue
    .line 1708
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x13

    if-lt v0, v1, :cond_0

    .line 1714
    const/4 v0, 0x1

    .line 1717
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static s(Landroid/content/Context;)Z
    .locals 5

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 1742
    invoke-static {}, Lcom/osp/common/util/i;->a()Lcom/osp/common/util/i;

    invoke-static {p0}, Lcom/osp/app/util/ad;->p(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/osp/common/util/i;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 1747
    if-eqz v2, :cond_2

    .line 1749
    const-string v3, "CN"

    sget-object v4, Ljava/util/Locale;->ENGLISH:Ljava/util/Locale;

    invoke-virtual {v2, v4}, Ljava/lang/String;->toUpperCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v3, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 1770
    :cond_0
    :goto_0
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "isCountryCode : "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/osp/app/util/aq;->c(Ljava/lang/String;)V

    .line 1771
    return v0

    :cond_1
    move v0, v1

    .line 1754
    goto :goto_0

    .line 1758
    :cond_2
    invoke-static {}, Lcom/osp/device/b;->b()Lcom/osp/device/b;

    move-result-object v2

    invoke-virtual {v2}, Lcom/osp/device/b;->n()Ljava/lang/String;

    move-result-object v2

    .line 1760
    const-string v3, "CN"

    sget-object v4, Ljava/util/Locale;->ENGLISH:Ljava/util/Locale;

    invoke-virtual {v2, v4}, Ljava/lang/String;->toUpperCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v3, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    move v0, v1

    .line 1765
    goto :goto_0
.end method

.method public static t()Z
    .locals 1

    .prologue
    .line 1846
    invoke-static {}, Lcom/osp/app/util/r;->i()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1848
    const/4 v0, 0x1

    .line 1851
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static t(Landroid/content/Context;)Z
    .locals 1

    .prologue
    .line 1781
    invoke-static {p0}, Lcom/osp/app/util/r;->b(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1783
    const/4 v0, 0x1

    .line 1786
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static u()Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 2029
    invoke-static {}, Lcom/osp/app/util/ah;->a()Lcom/osp/app/util/ah;

    move-result-object v1

    invoke-virtual {v1}, Lcom/osp/app/util/ah;->s()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 2039
    :cond_0
    :goto_0
    return v0

    .line 2034
    :cond_1
    sget v1, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v2, 0xa

    if-le v1, v2, :cond_0

    .line 2036
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public static u(Landroid/content/Context;)Z
    .locals 5

    .prologue
    const/4 v1, 0x0

    const/4 v0, 0x1

    .line 1860
    .line 1862
    invoke-static {p0}, Lcom/osp/app/util/r;->m(Landroid/content/Context;)Z

    move-result v2

    if-eqz v2, :cond_3

    invoke-static {}, Lcom/osp/app/util/r;->w()Z

    move-result v2

    if-nez v2, :cond_1

    const/4 v2, 0x3

    new-array v2, v2, [Ljava/lang/String;

    const-string v3, "SM-G850"

    aput-object v3, v2, v1

    const-string v3, "GT-I9060"

    aput-object v3, v2, v0

    const/4 v3, 0x2

    const-string v4, "SM-J100"

    aput-object v4, v2, v3

    invoke-static {v2}, Lcom/osp/app/util/u;->b([Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_0

    sget-object v2, Lcom/osp/app/util/u;->u:[Ljava/lang/String;

    invoke-static {v2}, Lcom/osp/app/util/u;->b([Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_0

    sget-object v2, Lcom/osp/app/util/u;->t:[Ljava/lang/String;

    invoke-static {v2}, Lcom/osp/app/util/u;->b([Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_0

    invoke-static {}, Lcom/osp/app/util/r;->n()Z

    move-result v2

    if-eqz v2, :cond_2

    :cond_0
    move v2, v0

    :goto_0
    if-eqz v2, :cond_3

    .line 1869
    :cond_1
    :goto_1
    return v0

    :cond_2
    move v2, v1

    .line 1862
    goto :goto_0

    .line 1865
    :cond_3
    invoke-static {p0}, Lcom/osp/app/util/r;->k(Landroid/content/Context;)Z

    move-result v2

    if-eqz v2, :cond_4

    sget-object v2, Lcom/osp/app/util/u;->v:[Ljava/lang/String;

    invoke-static {v2}, Lcom/osp/app/util/u;->a([Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_1

    :cond_4
    move v0, v1

    goto :goto_1
.end method

.method public static v()Z
    .locals 2

    .prologue
    .line 2045
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x15

    if-lt v0, v1, :cond_0

    .line 2047
    const/4 v0, 0x1

    .line 2050
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static v(Landroid/content/Context;)Z
    .locals 12

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 1907
    .line 1909
    invoke-static {p0}, Lcom/osp/app/util/ad;->h(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v5

    .line 1911
    invoke-static {v5}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1913
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v0, "LBE"

    const-string v1, "compareLoginIdWithServiceAccounts : loginId is empty"

    invoke-static {v0, v1}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;)V

    move v0, v2

    .line 1932
    :goto_0
    return v0

    .line 1918
    :cond_0
    const/4 v0, 0x4

    new-array v6, v0, [Ljava/lang/String;

    const-string v0, "com.google"

    aput-object v0, v6, v2

    const-string v0, "com.facebook.auth.login"

    aput-object v0, v6, v1

    const/4 v0, 0x2

    const-string v3, "com.android.email"

    aput-object v3, v6, v0

    const/4 v0, 0x3

    const-string v3, "com.android.exchange"

    aput-object v3, v6, v0

    .line 1919
    invoke-static {p0}, Landroid/accounts/AccountManager;->get(Landroid/content/Context;)Landroid/accounts/AccountManager;

    move-result-object v7

    .line 1921
    array-length v8, v6

    move v4, v2

    move v0, v2

    :goto_1
    if-ge v4, v8, :cond_3

    aget-object v3, v6, v4

    .line 1923
    invoke-virtual {v7, v3}, Landroid/accounts/AccountManager;->getAccountsByType(Ljava/lang/String;)[Landroid/accounts/Account;

    move-result-object v9

    array-length v10, v9

    move v3, v2

    :goto_2
    if-ge v3, v10, :cond_2

    aget-object v11, v9, v3

    .line 1925
    iget-object v11, v11, Landroid/accounts/Account;->name:Ljava/lang/String;

    invoke-virtual {v5, v11}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v11

    if-eqz v11, :cond_1

    move v0, v1

    .line 1923
    :cond_1
    add-int/lit8 v3, v3, 0x1

    goto :goto_2

    .line 1921
    :cond_2
    add-int/lit8 v3, v4, 0x1

    move v4, v3

    goto :goto_1

    .line 1931
    :cond_3
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v1, "LBE"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "compareLoginIdWithServiceAccounts : "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method private static w()Z
    .locals 8

    .prologue
    const/4 v0, 0x0

    .line 1877
    const-string v1, "default"

    .line 1881
    :try_start_0
    const-string v2, "com.samsung.android.feature.FloatingFeature"

    invoke-static {v2}, Ljava/lang/Class;->forName(Ljava/lang/String;)Ljava/lang/Class;

    move-result-object v2

    .line 1883
    const-string v3, "getInstance"

    const/4 v4, 0x0

    new-array v4, v4, [Ljava/lang/Class;

    invoke-virtual {v2, v3, v4}, Ljava/lang/Class;->getMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v2

    const/4 v3, 0x0

    const/4 v4, 0x0

    new-array v4, v4, [Ljava/lang/Object;

    invoke-virtual {v2, v3, v4}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    .line 1884
    invoke-virtual {v2}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    const-string v4, "getString"

    const/4 v5, 0x2

    new-array v5, v5, [Ljava/lang/Class;

    const/4 v6, 0x0

    const-class v7, Ljava/lang/String;

    aput-object v7, v5, v6

    const/4 v6, 0x1

    const-class v7, Ljava/lang/String;

    aput-object v7, v5, v6

    invoke-virtual {v3, v4, v5}, Ljava/lang/Class;->getMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v3

    .line 1885
    const/4 v4, 0x2

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    const-string v6, "SEC_FLOATING_FEATURE_SETUPWIZARD_STEP_SEQUENCE_TYPE"

    aput-object v6, v4, v5

    const/4 v5, 0x1

    const-string v6, "default"

    aput-object v6, v4, v5

    invoke-virtual {v3, v2, v4}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    .line 1887
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "isSkipDropBoxModel() "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/osp/app/util/aq;->c(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/ClassNotFoundException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_1

    .line 1898
    :goto_0
    if-eqz v1, :cond_0

    const-string v0, "skipdropbox"

    invoke-virtual {v1, v0}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v0

    :cond_0
    return v0

    .line 1888
    :catch_0
    move-exception v2

    .line 1891
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v2, "isSkipDropBoxModel() Class not found"

    invoke-static {v2}, Lcom/osp/app/util/aq;->b(Ljava/lang/String;)V

    goto :goto_0

    .line 1892
    :catch_1
    move-exception v2

    .line 1895
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v2, "isSkipDropBoxModel() exception"

    invoke-static {v2}, Lcom/osp/app/util/aq;->b(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public static w(Landroid/content/Context;)Z
    .locals 2

    .prologue
    .line 2002
    const/4 v1, 0x0

    .line 2004
    invoke-static {p0}, Lcom/osp/app/util/r;->m(Landroid/content/Context;)Z

    move-result v0

    if-nez v0, :cond_0

    invoke-static {p0}, Lcom/osp/app/util/r;->g(Landroid/content/Context;)Z

    move-result v0

    if-nez v0, :cond_0

    invoke-static {p0}, Lcom/osp/app/util/r;->l(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 2006
    :cond_0
    instance-of v0, p0, Lcom/osp/app/util/BaseActivity;

    if-eqz v0, :cond_2

    move-object v0, p0

    check-cast v0, Lcom/osp/app/util/BaseActivity;

    invoke-virtual {v0}, Lcom/osp/app/util/BaseActivity;->h()Z

    move-result v0

    if-nez v0, :cond_2

    .line 2011
    invoke-static {p0}, Lcom/osp/app/util/r;->g(Landroid/content/Context;)Z

    move-result v0

    if-nez v0, :cond_1

    invoke-static {}, Lcom/osp/app/util/r;->h()Z

    move-result v0

    if-nez v0, :cond_2

    .line 2013
    :cond_1
    const/4 v0, 0x1

    .line 2018
    :goto_0
    return v0

    :cond_2
    move v0, v1

    goto :goto_0
.end method
