.class public final Lcom/osp/app/util/b;
.super Ljava/lang/Object;
.source "AnimationUtils.java"

# interfaces
.implements Landroid/view/animation/Interpolator;


# static fields
.field private static final a:[[F

.field private static final b:[[F

.field private static final c:[[F


# instance fields
.field private d:[[F


# direct methods
.method static constructor <clinit>()V
    .locals 7

    .prologue
    const/4 v6, 0x2

    const/4 v5, 0x1

    const/4 v4, 0x0

    const/4 v3, 0x3

    .line 60
    const/4 v0, 0x5

    new-array v0, v0, [[F

    new-array v1, v3, [F

    fill-array-data v1, :array_0

    aput-object v1, v0, v4

    new-array v1, v3, [F

    fill-array-data v1, :array_1

    aput-object v1, v0, v5

    new-array v1, v3, [F

    fill-array-data v1, :array_2

    aput-object v1, v0, v6

    new-array v1, v3, [F

    fill-array-data v1, :array_3

    aput-object v1, v0, v3

    const/4 v1, 0x4

    new-array v2, v3, [F

    fill-array-data v2, :array_4

    aput-object v2, v0, v1

    sput-object v0, Lcom/osp/app/util/b;->a:[[F

    .line 63
    new-array v0, v6, [[F

    new-array v1, v3, [F

    fill-array-data v1, :array_5

    aput-object v1, v0, v4

    new-array v1, v3, [F

    fill-array-data v1, :array_6

    aput-object v1, v0, v5

    sput-object v0, Lcom/osp/app/util/b;->b:[[F

    .line 65
    new-array v0, v3, [[F

    new-array v1, v3, [F

    fill-array-data v1, :array_7

    aput-object v1, v0, v4

    new-array v1, v3, [F

    fill-array-data v1, :array_8

    aput-object v1, v0, v5

    new-array v1, v3, [F

    fill-array-data v1, :array_9

    aput-object v1, v0, v6

    sput-object v0, Lcom/osp/app/util/b;->c:[[F

    return-void

    .line 60
    nop

    :array_0
    .array-data 4
        0x0
        0x0
        0x3e7ced91    # 0.247f
    .end array-data

    :array_1
    .array-data 4
        0x3e7ced91    # 0.247f
        0x3ef5c28f    # 0.48f
        0x3f3851ec    # 0.72f
    .end array-data

    :array_2
    .array-data 4
        0x3f333333    # 0.7f
        0x3f55c28f    # 0.835f
        0x3f67ae14    # 0.905f
    .end array-data

    :array_3
    .array-data 4
        0x3f68f5c3    # 0.91f
        0x3f747ae1    # 0.955f
        0x3f7a5e35    # 0.978f
    .end array-data

    :array_4
    .array-data 4
        0x3f7a5e35    # 0.978f
        0x3f7ff972    # 0.9999f
        0x3f800000    # 1.0f
    .end array-data

    .line 63
    :array_5
    .array-data 4
        0x0
        0x3d4ccccd    # 0.05f
        0x3efd70a4    # 0.495f
    .end array-data

    :array_6
    .array-data 4
        0x3efd70a4    # 0.495f
        0x3f70a3d7    # 0.94f
        0x3f800000    # 1.0f
    .end array-data

    .line 65
    :array_7
    .array-data 4
        0x0
        0x3c23d70a    # 0.01f
        0x3ee66666    # 0.45f
    .end array-data

    :array_8
    .array-data 4
        0x3ee66666    # 0.45f
        0x3f4ccccd    # 0.8f
        0x3f6872b0    # 0.908f
    .end array-data

    :array_9
    .array-data 4
        0x3f6872b0    # 0.908f
        0x3f7ff972    # 0.9999f
        0x3f800000    # 1.0f
    .end array-data
.end method

.method public constructor <init>(Lcom/osp/app/util/c;)V
    .locals 1

    .prologue
    .line 69
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 70
    sget-object v0, Lcom/osp/app/util/c;->a:Lcom/osp/app/util/c;

    if-ne p1, v0, :cond_0

    .line 72
    sget-object v0, Lcom/osp/app/util/b;->b:[[F

    iput-object v0, p0, Lcom/osp/app/util/b;->d:[[F

    .line 80
    :goto_0
    return-void

    .line 73
    :cond_0
    sget-object v0, Lcom/osp/app/util/c;->b:Lcom/osp/app/util/c;

    if-ne p1, v0, :cond_1

    .line 75
    sget-object v0, Lcom/osp/app/util/b;->c:[[F

    iput-object v0, p0, Lcom/osp/app/util/b;->d:[[F

    goto :goto_0

    .line 78
    :cond_1
    sget-object v0, Lcom/osp/app/util/b;->a:[[F

    iput-object v0, p0, Lcom/osp/app/util/b;->d:[[F

    goto :goto_0
.end method


# virtual methods
.method public final getInterpolation(F)F
    .locals 9

    .prologue
    const/4 v8, 0x0

    const/high16 v7, 0x3f800000    # 1.0f

    .line 85
    div-float v1, p1, v7

    .line 86
    iget-object v0, p0, Lcom/osp/app/util/b;->d:[[F

    array-length v2, v0

    .line 87
    int-to-float v0, v2

    mul-float/2addr v0, v1

    float-to-double v4, v0

    invoke-static {v4, v5}, Ljava/lang/Math;->floor(D)D

    move-result-wide v4

    double-to-int v0, v4

    .line 88
    iget-object v3, p0, Lcom/osp/app/util/b;->d:[[F

    array-length v3, v3

    if-lt v0, v3, :cond_0

    .line 90
    iget-object v0, p0, Lcom/osp/app/util/b;->d:[[F

    array-length v0, v0

    add-int/lit8 v0, v0, -0x1

    .line 93
    :cond_0
    int-to-float v3, v0

    int-to-float v4, v2

    div-float v4, v7, v4

    mul-float/2addr v3, v4

    sub-float/2addr v1, v3

    int-to-float v2, v2

    mul-float/2addr v1, v2

    .line 94
    iget-object v2, p0, Lcom/osp/app/util/b;->d:[[F

    aget-object v0, v2, v0

    .line 95
    const/4 v2, 0x0

    aget v3, v0, v8

    const/high16 v4, 0x40000000    # 2.0f

    sub-float v5, v7, v1

    mul-float/2addr v4, v5

    const/4 v5, 0x1

    aget v5, v0, v5

    aget v6, v0, v8

    sub-float/2addr v5, v6

    mul-float/2addr v4, v5

    const/4 v5, 0x2

    aget v5, v0, v5

    aget v0, v0, v8

    sub-float v0, v5, v0

    mul-float/2addr v0, v1

    add-float/2addr v0, v4

    mul-float/2addr v0, v1

    add-float/2addr v0, v3

    mul-float/2addr v0, v7

    add-float/2addr v0, v2

    .line 97
    return v0
.end method
