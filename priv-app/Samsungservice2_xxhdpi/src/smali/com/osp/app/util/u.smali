.class public abstract Lcom/osp/app/util/u;
.super Ljava/lang/Object;
.source "ModelUtil.java"


# static fields
.field public static final a:[Ljava/lang/String;

.field public static final b:[Ljava/lang/String;

.field public static final c:[Ljava/lang/String;

.field public static final d:[Ljava/lang/String;

.field public static final e:[Ljava/lang/String;

.field public static final f:[Ljava/lang/String;

.field public static final g:[Ljava/lang/String;

.field public static final h:[Ljava/lang/String;

.field public static final i:[Ljava/lang/String;

.field public static final j:[Ljava/lang/String;

.field public static final k:[Ljava/lang/String;

.field public static final l:[Ljava/lang/String;

.field public static final m:[Ljava/lang/String;

.field public static final n:[Ljava/lang/String;

.field public static final o:[Ljava/lang/String;

.field public static final p:[Ljava/lang/String;

.field public static final q:[Ljava/lang/String;

.field public static final r:[Ljava/lang/String;

.field public static final s:[Ljava/lang/String;

.field public static final t:[Ljava/lang/String;

.field public static final u:[Ljava/lang/String;

.field public static final v:[Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 87
    const/16 v0, 0xe

    new-array v0, v0, [Ljava/lang/String;

    const-string v1, "GT-I9301I"

    aput-object v1, v0, v3

    const-string v1, "GT-I9300I"

    aput-object v1, v0, v4

    const-string v1, "SPH-L710"

    aput-object v1, v0, v5

    const-string v1, "EF-C1930B"

    aput-object v1, v0, v6

    const-string v1, "GT-I9300I"

    aput-object v1, v0, v7

    const/4 v1, 0x5

    const-string v2, "EF-AI930B"

    aput-object v2, v0, v1

    const/4 v1, 0x6

    const-string v2, "SCH-I535PP"

    aput-object v2, v0, v1

    const/4 v1, 0x7

    const-string v2, "SGH-N035"

    aput-object v2, v0, v1

    const/16 v1, 0x8

    const-string v2, "GT-I9300T"

    aput-object v2, v0, v1

    const/16 v1, 0x9

    const-string v2, "GT-I939I"

    aput-object v2, v0, v1

    const/16 v1, 0xa

    const-string v2, "GT-I9308I"

    aput-object v2, v0, v1

    const/16 v1, 0xb

    const-string v2, "GT-I9308"

    aput-object v2, v0, v1

    const/16 v1, 0xc

    const-string v2, "SCH-R530M"

    aput-object v2, v0, v1

    const/16 v1, 0xd

    const-string v2, "GT-I9305T"

    aput-object v2, v0, v1

    sput-object v0, Lcom/osp/app/util/u;->a:[Ljava/lang/String;

    .line 95
    const/16 v0, 0x11

    new-array v0, v0, [Ljava/lang/String;

    const-string v1, "GT-I9150D1"

    aput-object v1, v0, v3

    const-string v1, "GT-I9152"

    aput-object v1, v0, v4

    const-string v1, "GT-I9158"

    aput-object v1, v0, v5

    const-string v1, "GT-I9150D1"

    aput-object v1, v0, v6

    const-string v1, "GT-I9152"

    aput-object v1, v0, v7

    const/4 v1, 0x5

    const-string v2, "GT-I9150"

    aput-object v2, v0, v1

    const/4 v1, 0x6

    const-string v2, "GT-I9152D1"

    aput-object v2, v0, v1

    const/4 v1, 0x7

    const-string v2, "GT-I9150D1"

    aput-object v2, v0, v1

    const/16 v1, 0x8

    const-string v2, "SCH-P709D2"

    aput-object v2, v0, v1

    const/16 v1, 0x9

    const-string v2, "SCH-P709"

    aput-object v2, v0, v1

    const/16 v1, 0xa

    const-string v2, "GT-I9152X"

    aput-object v2, v0, v1

    const/16 v1, 0xb

    const-string v2, "GT-I9150"

    aput-object v2, v0, v1

    const/16 v1, 0xc

    const-string v2, "GT-I9152P"

    aput-object v2, v0, v1

    const/16 v1, 0xd

    const-string v2, "SCH-P709E"

    aput-object v2, v0, v1

    const/16 v1, 0xe

    const-string v2, "GT-I9158D1"

    aput-object v2, v0, v1

    const/16 v1, 0xf

    const-string v2, "GT-I9158P"

    aput-object v2, v0, v1

    const/16 v1, 0x10

    const-string v2, "SCH-I709E"

    aput-object v2, v0, v1

    sput-object v0, Lcom/osp/app/util/u;->b:[Ljava/lang/String;

    .line 103
    const/16 v0, 0xc

    new-array v0, v0, [Ljava/lang/String;

    const-string v1, "SM-N7507"

    aput-object v1, v0, v3

    const-string v1, "SM-N7502"

    aput-object v1, v0, v4

    const-string v1, "SM-N750K"

    aput-object v1, v0, v5

    const-string v1, "SM-N750L"

    aput-object v1, v0, v6

    const-string v1, "SM-N750S"

    aput-object v1, v0, v7

    const/4 v1, 0x5

    const-string v2, "SM-N7500Q"

    aput-object v2, v0, v1

    const/4 v1, 0x6

    const-string v2, "SM-N7508V"

    aput-object v2, v0, v1

    const/4 v1, 0x7

    const-string v2, "SM-N7509V"

    aput-object v2, v0, v1

    const/16 v1, 0x8

    const-string v2, "SM-N7506V"

    aput-object v2, v0, v1

    const/16 v1, 0x9

    const-string v2, "ET-FN750C"

    aput-object v2, v0, v1

    const/16 v1, 0xa

    const-string v2, "SM-N7505L"

    aput-object v2, v0, v1

    const/16 v1, 0xb

    const-string v2, "SM-N750"

    aput-object v2, v0, v1

    sput-object v0, Lcom/osp/app/util/u;->c:[Ljava/lang/String;

    .line 111
    new-array v0, v5, [Ljava/lang/String;

    const-string v1, "GT-I8580"

    aput-object v1, v0, v3

    const-string v1, "SHW-M570S"

    aput-object v1, v0, v4

    sput-object v0, Lcom/osp/app/util/u;->d:[Ljava/lang/String;

    .line 118
    const/16 v0, 0xd

    new-array v0, v0, [Ljava/lang/String;

    const-string v1, "EF-CG710B"

    aput-object v1, v0, v3

    const-string v1, "EF-FG710B"

    aput-object v1, v0, v4

    const-string v1, "SM-G7102"

    aput-object v1, v0, v5

    const-string v1, "SM-G7105"

    aput-object v1, v0, v6

    const-string v1, "SM-G7105L"

    aput-object v1, v0, v7

    const/4 v1, 0x5

    const-string v2, "SM-G7106"

    aput-object v2, v0, v1

    const/4 v1, 0x6

    const-string v2, "SM-G7108"

    aput-object v2, v0, v1

    const/4 v1, 0x7

    const-string v2, "SM-G7109"

    aput-object v2, v0, v1

    const/16 v1, 0x8

    const-string v2, "SM-G710L"

    aput-object v2, v0, v1

    const/16 v1, 0x9

    const-string v2, "SM-G710K"

    aput-object v2, v0, v1

    const/16 v1, 0xa

    const-string v2, "SM-G710S"

    aput-object v2, v0, v1

    const/16 v1, 0xb

    const-string v2, "SM-G7108V"

    aput-object v2, v0, v1

    const/16 v1, 0xc

    const-string v2, "SM-G7108U"

    aput-object v2, v0, v1

    sput-object v0, Lcom/osp/app/util/u;->e:[Ljava/lang/String;

    .line 127
    const/4 v0, 0x6

    new-array v0, v0, [Ljava/lang/String;

    const-string v1, "SM-T2558"

    aput-object v1, v0, v3

    const-string v1, "SM-T255"

    aput-object v1, v0, v4

    const-string v1, "SM-T251"

    aput-object v1, v0, v5

    const-string v1, "SM-T255S"

    aput-object v1, v0, v6

    const-string v1, "SM-T2556"

    aput-object v1, v0, v7

    const/4 v1, 0x5

    const-string v2, "SM-T2519"

    aput-object v2, v0, v1

    sput-object v0, Lcom/osp/app/util/u;->f:[Ljava/lang/String;

    .line 134
    new-array v0, v7, [Ljava/lang/String;

    const-string v1, "SM-T110"

    aput-object v1, v0, v3

    const-string v1, "SM-T111"

    aput-object v1, v0, v4

    const-string v1, "SM-T111M"

    aput-object v1, v0, v5

    const-string v1, "SM-T111NQ"

    aput-object v1, v0, v6

    sput-object v0, Lcom/osp/app/util/u;->g:[Ljava/lang/String;

    .line 141
    const/16 v0, 0xc

    new-array v0, v0, [Ljava/lang/String;

    const-string v1, "SM-P600"

    aput-object v1, v0, v3

    const-string v1, "SM-P600X"

    aput-object v1, v0, v4

    const-string v1, "SM-P601"

    aput-object v1, v0, v5

    const-string v1, "SM-P601D1"

    aput-object v1, v0, v6

    const-string v1, "SM-P602"

    aput-object v1, v0, v7

    const/4 v1, 0x5

    const-string v2, "SM-P605"

    aput-object v2, v0, v1

    const/4 v1, 0x6

    const-string v2, "SM-P605S"

    aput-object v2, v0, v1

    const/4 v1, 0x7

    const-string v2, "SM-P605M"

    aput-object v2, v0, v1

    const/16 v1, 0x8

    const-string v2, "SM-P605L"

    aput-object v2, v0, v1

    const/16 v1, 0x9

    const-string v2, "SM-P605K"

    aput-object v2, v0, v1

    const/16 v1, 0xa

    const-string v2, "SM-P605V"

    aput-object v2, v0, v1

    const/16 v1, 0xb

    const-string v2, "SM-P605VD1"

    aput-object v2, v0, v1

    sput-object v0, Lcom/osp/app/util/u;->h:[Ljava/lang/String;

    .line 149
    const/16 v0, 0x1e

    new-array v0, v0, [Ljava/lang/String;

    const-string v1, "SM-N900"

    aput-object v1, v0, v3

    const-string v1, "SM-N9002"

    aput-object v1, v0, v4

    const-string v1, "SM-N9005"

    aput-object v1, v0, v5

    const-string v1, "SM-N9006"

    aput-object v1, v0, v6

    const-string v1, "SM-N900A"

    aput-object v1, v0, v7

    const/4 v1, 0x5

    const-string v2, "SM-N900P"

    aput-object v2, v0, v1

    const/4 v1, 0x6

    const-string v2, "SM-N900S"

    aput-object v2, v0, v1

    const/4 v1, 0x7

    const-string v2, "SM-N900T"

    aput-object v2, v0, v1

    const/16 v1, 0x8

    const-string v2, "SM-N900V"

    aput-object v2, v0, v1

    const/16 v1, 0x9

    const-string v2, "SM-N900W8"

    aput-object v2, v0, v1

    const/16 v1, 0xa

    const-string v2, "SM-N9008"

    aput-object v2, v0, v1

    const/16 v1, 0xb

    const-string v2, "SM-N9009"

    aput-object v2, v0, v1

    const/16 v1, 0xc

    const-string v2, "SM-N900D"

    aput-object v2, v0, v1

    const/16 v1, 0xd

    const-string v2, "SM-N900J"

    aput-object v2, v0, v1

    const/16 v1, 0xe

    const-string v2, "SM-N900K"

    aput-object v2, v0, v1

    const/16 v1, 0xf

    const-string v2, "SM-N900L"

    aput-object v2, v0, v1

    const/16 v1, 0x10

    const-string v2, "SM-N900Q"

    aput-object v2, v0, v1

    const/16 v1, 0x11

    const-string v2, "SM-N900R4"

    aput-object v2, v0, v1

    const/16 v1, 0x12

    const-string v2, "SM-N900W9"

    aput-object v2, v0, v1

    const/16 v1, 0x13

    const-string v2, "SM-N900X"

    aput-object v2, v0, v1

    const/16 v1, 0x14

    const-string v2, "SM-N9000Q"

    aput-object v2, v0, v1

    const/16 v1, 0x15

    const-string v2, "SC-01F"

    aput-object v2, v0, v1

    const/16 v1, 0x16

    const-string v2, "SC-02F"

    aput-object v2, v0, v1

    const/16 v1, 0x17

    const-string v2, "SCL22"

    aput-object v2, v0, v1

    const/16 v1, 0x18

    const-string v2, "SM-N9007"

    aput-object v2, v0, v1

    const/16 v1, 0x19

    const-string v2, "SM-N9009V"

    aput-object v2, v0, v1

    const/16 v1, 0x1a

    const-string v2, "SM-N9008S"

    aput-object v2, v0, v1

    const/16 v1, 0x1b

    const-string v2, "SM-N9008V"

    aput-object v2, v0, v1

    const/16 v1, 0x1c

    const-string v2, "GT-I9508V"

    aput-object v2, v0, v1

    const/16 v1, 0x1d

    const-string v2, "SM-G910S"

    aput-object v2, v0, v1

    sput-object v0, Lcom/osp/app/util/u;->i:[Ljava/lang/String;

    .line 159
    const/4 v0, 0x7

    new-array v0, v0, [Ljava/lang/String;

    const-string v1, "GT-N5120"

    aput-object v1, v0, v3

    const-string v1, "SHW-M500W"

    aput-object v1, v0, v4

    const-string v1, "SHW-M500"

    aput-object v1, v0, v5

    const-string v1, "GT-N5105"

    aput-object v1, v0, v6

    const-string v1, "GT-N5100"

    aput-object v1, v0, v7

    const/4 v1, 0x5

    const-string v2, "SGH-I467"

    aput-object v2, v0, v1

    const/4 v1, 0x6

    const-string v2, "GT-N5110"

    aput-object v2, v0, v1

    sput-object v0, Lcom/osp/app/util/u;->j:[Ljava/lang/String;

    .line 173
    const/16 v0, 0x27

    new-array v0, v0, [Ljava/lang/String;

    const-string v1, "SCH-R970C"

    aput-object v1, v0, v3

    const-string v1, "GT-I9505"

    aput-object v1, v0, v4

    const-string v1, "SGH-N045"

    aput-object v1, v0, v5

    const-string v1, "GT-I9500"

    aput-object v1, v0, v6

    const-string v1, "SHV-E300L"

    aput-object v1, v0, v7

    const/4 v1, 0x5

    const-string v2, "SHV-E300K"

    aput-object v2, v0, v1

    const/4 v1, 0x6

    const-string v2, "SCH-I959"

    aput-object v2, v0, v1

    const/4 v1, 0x7

    const-string v2, "GT-I9502"

    aput-object v2, v0, v1

    const/16 v1, 0x8

    const-string v2, "GT-I9508"

    aput-object v2, v0, v1

    const/16 v1, 0x9

    const-string v2, "DGT-I9500"

    aput-object v2, v0, v1

    const/16 v1, 0xa

    const-string v2, "BGT-I9500"

    aput-object v2, v0, v1

    const/16 v1, 0xb

    const-string v2, "GT-I9500X"

    aput-object v2, v0, v1

    const/16 v1, 0xc

    const-string v2, "EF-PI950B"

    aput-object v2, v0, v1

    const/16 v1, 0xd

    const-string v2, "EF-LI950B"

    aput-object v2, v0, v1

    const/16 v1, 0xe

    const-string v2, "EF-CI950B"

    aput-object v2, v0, v1

    const/16 v1, 0xf

    const-string v2, "EF-FI950B"

    aput-object v2, v0, v1

    const/16 v1, 0x10

    const-string v2, "EB-K600B"

    aput-object v2, v0, v1

    const/16 v1, 0x11

    const-string v2, "EO-EH3303"

    aput-object v2, v0, v1

    const/16 v1, 0x12

    const-string v2, "SGH-M919"

    aput-object v2, v0, v1

    const/16 v1, 0x13

    const-string v2, "SGH-M919Z"

    aput-object v2, v0, v1

    const/16 v1, 0x14

    const-string v2, "SGH-M919ZWATMB"

    aput-object v2, v0, v1

    const/16 v1, 0x15

    const-string v2, "SGH-M919V"

    aput-object v2, v0, v1

    const/16 v1, 0x16

    const-string v2, "SGH-I337"

    aput-object v2, v0, v1

    const/16 v1, 0x17

    const-string v2, "SGH-I337M"

    aput-object v2, v0, v1

    const/16 v1, 0x18

    const-string v2, "SGH-I337ZKLTCE"

    aput-object v2, v0, v1

    const/16 v1, 0x19

    const-string v2, "SGH-I337Z"

    aput-object v2, v0, v1

    const/16 v1, 0x1a

    const-string v2, "SCH-I545"

    aput-object v2, v0, v1

    const/16 v1, 0x1b

    const-string v2, "SCH-R970"

    aput-object v2, v0, v1

    const/16 v1, 0x1c

    const-string v2, "SPH-L720"

    aput-object v2, v0, v1

    const/16 v1, 0x1d

    const-string v2, "DPH-L720"

    aput-object v2, v0, v1

    const/16 v1, 0x1e

    const-string v2, "DGH-I337"

    aput-object v2, v0, v1

    const/16 v1, 0x1f

    const-string v2, "SGH-I337DSAATT"

    aput-object v2, v0, v1

    const/16 v1, 0x20

    const-string v2, "SGH-I337D"

    aput-object v2, v0, v1

    const/16 v1, 0x21

    const-string v2, "DGT-I9505"

    aput-object v2, v0, v1

    const/16 v1, 0x22

    const-string v2, "GT-I9505X"

    aput-object v2, v0, v1

    const/16 v1, 0x23

    const-string v2, "SHV-E300S"

    aput-object v2, v0, v1

    const/16 v1, 0x24

    const-string v2, "SHV-E300SZW6SO"

    aput-object v2, v0, v1

    const/16 v1, 0x25

    const-string v2, "SGH-I537"

    aput-object v2, v0, v1

    const/16 v1, 0x26

    const-string v2, "SAMSUNG-SGH-I337"

    aput-object v2, v0, v1

    sput-object v0, Lcom/osp/app/util/u;->k:[Ljava/lang/String;

    .line 183
    const/4 v0, 0x6

    new-array v0, v0, [Ljava/lang/String;

    const-string v1, "GT-I9192"

    aput-object v1, v0, v3

    const-string v1, "GT-I9190"

    aput-object v1, v0, v4

    const-string v1, "GT-I9195"

    aput-object v1, v0, v5

    const-string v1, "GT-I257"

    aput-object v1, v0, v6

    const-string v1, "SGH-I257"

    aput-object v1, v0, v7

    const/4 v1, 0x5

    const-string v2, "SPH-L520"

    aput-object v2, v0, v1

    sput-object v0, Lcom/osp/app/util/u;->l:[Ljava/lang/String;

    .line 201
    new-array v0, v5, [Ljava/lang/String;

    const-string v1, "SC-04F"

    aput-object v1, v0, v3

    const-string v1, "SCL23"

    aput-object v1, v0, v4

    sput-object v0, Lcom/osp/app/util/u;->m:[Ljava/lang/String;

    .line 230
    new-array v0, v4, [Ljava/lang/String;

    const-string v1, "SM-G900"

    aput-object v1, v0, v3

    sput-object v0, Lcom/osp/app/util/u;->n:[Ljava/lang/String;

    .line 238
    new-array v0, v7, [Ljava/lang/String;

    const-string v1, "SM-T310"

    aput-object v1, v0, v3

    const-string v1, "SM-T311"

    aput-object v1, v0, v4

    const-string v1, "SM-T312"

    aput-object v1, v0, v5

    const-string v1, "SM-T315"

    aput-object v1, v0, v6

    sput-object v0, Lcom/osp/app/util/u;->o:[Ljava/lang/String;

    .line 245
    new-array v0, v4, [Ljava/lang/String;

    const-string v1, "SM-G750"

    aput-object v1, v0, v3

    sput-object v0, Lcom/osp/app/util/u;->p:[Ljava/lang/String;

    .line 250
    new-array v0, v5, [Ljava/lang/String;

    const-string v1, "SM-N910"

    aput-object v1, v0, v3

    const-string v1, "SM-N915"

    aput-object v1, v0, v4

    sput-object v0, Lcom/osp/app/util/u;->q:[Ljava/lang/String;

    .line 257
    new-array v0, v4, [Ljava/lang/String;

    const-string v1, "SM-G357FZ"

    aput-object v1, v0, v3

    sput-object v0, Lcom/osp/app/util/u;->r:[Ljava/lang/String;

    .line 264
    new-array v0, v5, [Ljava/lang/String;

    const-string v1, "SM-G386"

    aput-object v1, v0, v3

    const-string v1, "SM-A300"

    aput-object v1, v0, v4

    sput-object v0, Lcom/osp/app/util/u;->s:[Ljava/lang/String;

    .line 271
    new-array v0, v4, [Ljava/lang/String;

    const-string v1, "SM-G530"

    aput-object v1, v0, v3

    sput-object v0, Lcom/osp/app/util/u;->t:[Ljava/lang/String;

    .line 278
    new-array v0, v4, [Ljava/lang/String;

    const-string v1, "SM-G720"

    aput-object v1, v0, v3

    sput-object v0, Lcom/osp/app/util/u;->u:[Ljava/lang/String;

    .line 280
    new-array v0, v5, [Ljava/lang/String;

    const-string v1, "SM-T365"

    aput-object v1, v0, v3

    const-string v1, "SM-T360"

    aput-object v1, v0, v4

    sput-object v0, Lcom/osp/app/util/u;->v:[Ljava/lang/String;

    return-void
.end method

.method public static a([Ljava/lang/String;)Z
    .locals 5

    .prologue
    const/4 v0, 0x0

    .line 289
    invoke-static {}, Lcom/osp/device/b;->b()Lcom/osp/device/b;

    move-result-object v1

    invoke-virtual {v1}, Lcom/osp/device/b;->e()Ljava/lang/String;

    move-result-object v1

    sget-object v2, Ljava/util/Locale;->ENGLISH:Ljava/util/Locale;

    invoke-virtual {v1, v2}, Ljava/lang/String;->toUpperCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v2

    .line 291
    array-length v3, p0

    move v1, v0

    :goto_0
    if-ge v1, v3, :cond_0

    aget-object v4, p0, v1

    .line 293
    invoke-virtual {v4, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_1

    .line 295
    const/4 v0, 0x1

    .line 298
    :cond_0
    return v0

    .line 291
    :cond_1
    add-int/lit8 v1, v1, 0x1

    goto :goto_0
.end method

.method public static b([Ljava/lang/String;)Z
    .locals 5

    .prologue
    const/4 v0, 0x0

    .line 308
    invoke-static {}, Lcom/osp/device/b;->b()Lcom/osp/device/b;

    move-result-object v1

    invoke-virtual {v1}, Lcom/osp/device/b;->e()Ljava/lang/String;

    move-result-object v1

    sget-object v2, Ljava/util/Locale;->ENGLISH:Ljava/util/Locale;

    invoke-virtual {v1, v2}, Ljava/lang/String;->toUpperCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v2

    .line 310
    if-eqz v2, :cond_0

    .line 312
    array-length v3, p0

    move v1, v0

    :goto_0
    if-ge v1, v3, :cond_0

    aget-object v4, p0, v1

    .line 314
    invoke-virtual {v2, v4}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v4

    if-eqz v4, :cond_1

    .line 316
    const/4 v0, 0x1

    .line 320
    :cond_0
    return v0

    .line 312
    :cond_1
    add-int/lit8 v1, v1, 0x1

    goto :goto_0
.end method
