.class public final Lcom/osp/app/util/ab;
.super Landroid/widget/ArrayAdapter;
.source "SecurityQuestionUtil.java"


# instance fields
.field private final a:Lcom/osp/app/util/BaseActivity;

.field private b:Ljava/util/ArrayList;


# direct methods
.method public constructor <init>(Landroid/content/Context;Ljava/util/ArrayList;)V
    .locals 3

    .prologue
    .line 138
    const v0, 0x7f03002c

    invoke-direct {p0, p1, v0}, Landroid/widget/ArrayAdapter;-><init>(Landroid/content/Context;I)V

    .line 135
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/osp/app/util/ab;->b:Ljava/util/ArrayList;

    move-object v0, p1

    .line 140
    check-cast v0, Lcom/osp/app/util/BaseActivity;

    iput-object v0, p0, Lcom/osp/app/util/ab;->a:Lcom/osp/app/util/BaseActivity;

    .line 142
    iput-object p2, p0, Lcom/osp/app/util/ab;->b:Ljava/util/ArrayList;

    .line 144
    invoke-virtual {p0}, Lcom/osp/app/util/ab;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    .line 146
    invoke-virtual {p0}, Lcom/osp/app/util/ab;->clear()V

    .line 149
    :cond_0
    iget-object v0, p0, Lcom/osp/app/util/ab;->b:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v2

    .line 150
    const/4 v0, 0x0

    move v1, v0

    :goto_0
    if-ge v1, v2, :cond_1

    .line 152
    iget-object v0, p0, Lcom/osp/app/util/ab;->b:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    invoke-virtual {p1, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/osp/app/util/ab;->add(Ljava/lang/Object;)V

    .line 150
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 154
    :cond_1
    return-void
.end method


# virtual methods
.method public final getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 8

    .prologue
    const v7, 0x7f0c011a

    const v6, 0x7f0800e0

    const/16 v5, 0x15

    const/4 v4, -0x1

    const/4 v3, 0x0

    .line 158
    .line 159
    if-nez p2, :cond_1

    .line 161
    invoke-virtual {p0}, Lcom/osp/app/util/ab;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    .line 162
    const v1, 0x7f03002c

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object p2

    .line 164
    invoke-virtual {p2, v7}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 165
    invoke-virtual {p0, p1}, Lcom/osp/app/util/ab;->getItem(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/CharSequence;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 167
    iget-object v1, p0, Lcom/osp/app/util/ab;->a:Lcom/osp/app/util/BaseActivity;

    invoke-static {v1}, Lcom/osp/app/util/an;->a(Lcom/osp/app/util/BaseActivity;)Lcom/osp/app/util/an;

    invoke-static {v6}, Lcom/osp/app/util/an;->b(I)I

    move-result v1

    .line 168
    iget-object v2, p0, Lcom/osp/app/util/ab;->a:Lcom/osp/app/util/BaseActivity;

    invoke-virtual {v2}, Lcom/osp/app/util/BaseActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-virtual {v2, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    int-to-float v1, v1

    invoke-virtual {v0, v3, v1}, Landroid/widget/TextView;->setTextSize(IF)V

    .line 170
    sget v1, Landroid/os/Build$VERSION;->SDK_INT:I

    if-lt v1, v5, :cond_0

    .line 172
    invoke-static {v0, v3, v4, v3, v4}, Lcom/msc/sa/c/d;->a(Landroid/view/View;IIII)V

    .line 188
    :cond_0
    :goto_0
    return-object p2

    .line 176
    :cond_1
    invoke-virtual {p2, v7}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 177
    invoke-virtual {p0, p1}, Lcom/osp/app/util/ab;->getItem(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/CharSequence;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 179
    iget-object v1, p0, Lcom/osp/app/util/ab;->a:Lcom/osp/app/util/BaseActivity;

    invoke-static {v1}, Lcom/osp/app/util/an;->a(Lcom/osp/app/util/BaseActivity;)Lcom/osp/app/util/an;

    invoke-static {v6}, Lcom/osp/app/util/an;->b(I)I

    move-result v1

    .line 180
    iget-object v2, p0, Lcom/osp/app/util/ab;->a:Lcom/osp/app/util/BaseActivity;

    invoke-virtual {v2}, Lcom/osp/app/util/BaseActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-virtual {v2, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    int-to-float v1, v1

    invoke-virtual {v0, v3, v1}, Landroid/widget/TextView;->setTextSize(IF)V

    .line 182
    sget v1, Landroid/os/Build$VERSION;->SDK_INT:I

    if-lt v1, v5, :cond_0

    .line 184
    invoke-static {v0, v3, v4, v3, v4}, Lcom/msc/sa/c/d;->a(Landroid/view/View;IIII)V

    goto :goto_0
.end method
