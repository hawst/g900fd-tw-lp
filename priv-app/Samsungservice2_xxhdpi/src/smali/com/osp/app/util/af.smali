.class public final enum Lcom/osp/app/util/af;
.super Ljava/lang/Enum;
.source "StateCheckUtil.java"


# static fields
.field public static final enum a:Lcom/osp/app/util/af;

.field public static final enum b:Lcom/osp/app/util/af;

.field public static final enum c:Lcom/osp/app/util/af;

.field public static final enum d:Lcom/osp/app/util/af;

.field private static final synthetic e:[Lcom/osp/app/util/af;


# direct methods
.method static constructor <clinit>()V
    .locals 6

    .prologue
    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 61
    new-instance v0, Lcom/osp/app/util/af;

    const-string v1, "Notification"

    invoke-direct {v0, v1, v2}, Lcom/osp/app/util/af;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/osp/app/util/af;->a:Lcom/osp/app/util/af;

    new-instance v0, Lcom/osp/app/util/af;

    const-string v1, "TncRequest"

    invoke-direct {v0, v1, v3}, Lcom/osp/app/util/af;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/osp/app/util/af;->b:Lcom/osp/app/util/af;

    new-instance v0, Lcom/osp/app/util/af;

    const-string v1, "AuthWithTncMandatory"

    invoke-direct {v0, v1, v4}, Lcom/osp/app/util/af;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/osp/app/util/af;->c:Lcom/osp/app/util/af;

    new-instance v0, Lcom/osp/app/util/af;

    const-string v1, "NotiForTncRequest"

    invoke-direct {v0, v1, v5}, Lcom/osp/app/util/af;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/osp/app/util/af;->d:Lcom/osp/app/util/af;

    .line 59
    const/4 v0, 0x4

    new-array v0, v0, [Lcom/osp/app/util/af;

    sget-object v1, Lcom/osp/app/util/af;->a:Lcom/osp/app/util/af;

    aput-object v1, v0, v2

    sget-object v1, Lcom/osp/app/util/af;->b:Lcom/osp/app/util/af;

    aput-object v1, v0, v3

    sget-object v1, Lcom/osp/app/util/af;->c:Lcom/osp/app/util/af;

    aput-object v1, v0, v4

    sget-object v1, Lcom/osp/app/util/af;->d:Lcom/osp/app/util/af;

    aput-object v1, v0, v5

    sput-object v0, Lcom/osp/app/util/af;->e:[Lcom/osp/app/util/af;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0

    .prologue
    .line 59
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/osp/app/util/af;
    .locals 1

    .prologue
    .line 59
    const-class v0, Lcom/osp/app/util/af;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/osp/app/util/af;

    return-object v0
.end method

.method public static values()[Lcom/osp/app/util/af;
    .locals 1

    .prologue
    .line 59
    sget-object v0, Lcom/osp/app/util/af;->e:[Lcom/osp/app/util/af;

    invoke-virtual {v0}, [Lcom/osp/app/util/af;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/osp/app/util/af;

    return-object v0
.end method
