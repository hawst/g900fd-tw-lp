.class public final Lcom/osp/app/util/q;
.super Ljava/lang/Object;
.source "LayoutParamsChangeUtil.java"


# static fields
.field private static a:Lcom/osp/app/util/q;


# direct methods
.method private constructor <init>()V
    .locals 0

    .prologue
    .line 48
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 49
    return-void
.end method

.method public static a()Lcom/osp/app/util/q;
    .locals 1

    .prologue
    .line 57
    sget-object v0, Lcom/osp/app/util/q;->a:Lcom/osp/app/util/q;

    if-eqz v0, :cond_0

    .line 59
    sget-object v0, Lcom/osp/app/util/q;->a:Lcom/osp/app/util/q;

    .line 63
    :goto_0
    return-object v0

    .line 62
    :cond_0
    new-instance v0, Lcom/osp/app/util/q;

    invoke-direct {v0}, Lcom/osp/app/util/q;-><init>()V

    .line 63
    sput-object v0, Lcom/osp/app/util/q;->a:Lcom/osp/app/util/q;

    goto :goto_0
.end method

.method public static a(Landroid/content/Context;Landroid/view/View;III)V
    .locals 2

    .prologue
    const/4 v0, 0x0

    .line 248
    invoke-static {p0}, Lcom/osp/app/util/r;->g(Landroid/content/Context;)Z

    move-result v1

    if-eqz v1, :cond_1

    move p3, v0

    move p2, v0

    .line 259
    :cond_0
    :goto_0
    invoke-static {p1, p2, p3, p4}, Lcom/osp/app/util/q;->b(Landroid/view/View;III)V

    .line 260
    return-void

    .line 254
    :cond_1
    invoke-static {p0}, Lcom/osp/app/util/r;->l(Landroid/content/Context;)Z

    move-result v1

    if-eqz v1, :cond_0

    move p3, v0

    .line 256
    goto :goto_0
.end method

.method public static a(Landroid/view/View;III)V
    .locals 3

    .prologue
    .line 76
    if-nez p0, :cond_1

    .line 106
    :cond_0
    :goto_0
    return-void

    .line 81
    :cond_1
    const/4 v0, 0x1

    if-ne p3, v0, :cond_3

    .line 98
    :goto_1
    invoke-virtual {p0}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    .line 100
    iget v1, v0, Landroid/view/ViewGroup$LayoutParams;->width:I

    if-ne v1, p2, :cond_2

    .line 103
    iput p1, v0, Landroid/view/ViewGroup$LayoutParams;->width:I

    .line 105
    :cond_2
    invoke-virtual {p0, v0}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    goto :goto_0

    .line 87
    :cond_3
    const/4 v0, 0x2

    if-ne p3, v0, :cond_0

    move v2, p1

    move p1, p2

    move p2, v2

    .line 90
    goto :goto_1
.end method

.method public static b(Landroid/view/View;III)V
    .locals 2

    .prologue
    .line 212
    if-nez p0, :cond_1

    .line 244
    :cond_0
    :goto_0
    return-void

    .line 217
    :cond_1
    const/4 v0, 0x1

    if-ne p3, v0, :cond_4

    .line 229
    :goto_1
    invoke-virtual {p0}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v1

    .line 230
    instance-of v0, v1, Landroid/view/ViewGroup$MarginLayoutParams;

    if-eqz v0, :cond_0

    move-object v0, v1

    .line 232
    check-cast v0, Landroid/view/ViewGroup$MarginLayoutParams;

    iget v0, v0, Landroid/view/ViewGroup$MarginLayoutParams;->leftMargin:I

    if-eq v0, p2, :cond_2

    move-object v0, v1

    .line 234
    check-cast v0, Landroid/view/ViewGroup$MarginLayoutParams;

    iput p2, v0, Landroid/view/ViewGroup$MarginLayoutParams;->leftMargin:I

    :cond_2
    move-object v0, v1

    .line 237
    check-cast v0, Landroid/view/ViewGroup$MarginLayoutParams;

    iget v0, v0, Landroid/view/ViewGroup$MarginLayoutParams;->rightMargin:I

    if-eq v0, p2, :cond_3

    move-object v0, v1

    .line 239
    check-cast v0, Landroid/view/ViewGroup$MarginLayoutParams;

    iput p2, v0, Landroid/view/ViewGroup$MarginLayoutParams;->rightMargin:I

    .line 242
    :cond_3
    invoke-static {p0, v1}, Lcom/msc/sa/c/d;->a(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    goto :goto_0

    .line 221
    :cond_4
    const/4 v0, 0x2

    if-ne p3, v0, :cond_0

    move p2, p1

    goto :goto_1
.end method

.method public static c(Landroid/view/View;III)V
    .locals 3

    .prologue
    .line 315
    if-nez p0, :cond_1

    .line 348
    :cond_0
    :goto_0
    return-void

    .line 320
    :cond_1
    const/4 v0, 0x1

    if-ne p3, v0, :cond_3

    .line 337
    :goto_1
    invoke-virtual {p0}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v1

    .line 338
    instance-of v0, v1, Landroid/view/ViewGroup$MarginLayoutParams;

    if-eqz v0, :cond_0

    move-object v0, v1

    .line 340
    check-cast v0, Landroid/view/ViewGroup$MarginLayoutParams;

    iget v0, v0, Landroid/view/ViewGroup$MarginLayoutParams;->rightMargin:I

    if-ne v0, p2, :cond_2

    move-object v0, v1

    .line 344
    check-cast v0, Landroid/view/ViewGroup$MarginLayoutParams;

    iput p1, v0, Landroid/view/ViewGroup$MarginLayoutParams;->rightMargin:I

    .line 346
    :cond_2
    invoke-virtual {p0, v1}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    goto :goto_0

    .line 326
    :cond_3
    const/4 v0, 0x2

    if-ne p3, v0, :cond_0

    move v2, p1

    move p1, p2

    move p2, v2

    .line 329
    goto :goto_1
.end method
