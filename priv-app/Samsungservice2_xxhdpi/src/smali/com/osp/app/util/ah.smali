.class public final Lcom/osp/app/util/ah;
.super Ljava/lang/Object;
.source "TestPropertyManager.java"


# static fields
.field private static final a:Ljava/lang/String;


# instance fields
.field private b:Lcom/osp/app/util/aj;

.field private c:Z


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 29
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-static {}, Landroid/os/Environment;->getExternalStorageDirectory()Ljava/io/File;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "/msc_service.prop"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/osp/app/util/ah;->a:Ljava/lang/String;

    return-void
.end method

.method private constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 99
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 76
    new-instance v0, Lcom/osp/app/util/aj;

    invoke-direct {v0, v1}, Lcom/osp/app/util/aj;-><init>(B)V

    iput-object v0, p0, Lcom/osp/app/util/ah;->b:Lcom/osp/app/util/aj;

    .line 94
    iput-boolean v1, p0, Lcom/osp/app/util/ah;->c:Z

    .line 101
    return-void
.end method

.method synthetic constructor <init>(B)V
    .locals 0

    .prologue
    .line 24
    invoke-direct {p0}, Lcom/osp/app/util/ah;-><init>()V

    return-void
.end method

.method public static a()Lcom/osp/app/util/ah;
    .locals 1

    .prologue
    .line 109
    sget-object v0, Lcom/osp/app/util/al;->a:Lcom/osp/app/util/ah;

    return-object v0
.end method

.method public static b(Ljava/lang/String;)Z
    .locals 1

    .prologue
    .line 978
    const-string v0, "54.208.185.179"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 980
    const/4 v0, 0x1

    .line 982
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private c(Ljava/lang/String;)V
    .locals 5

    .prologue
    const/4 v4, 0x1

    .line 542
    const/4 v2, 0x0

    .line 545
    :try_start_0
    invoke-static {}, Landroid/util/Xml;->newPullParser()Lorg/xmlpull/v1/XmlPullParser;

    move-result-object v0

    .line 546
    new-instance v1, Ljava/io/StringReader;

    invoke-direct {v1, p1}, Ljava/io/StringReader;-><init>(Ljava/lang/String;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 548
    :try_start_1
    invoke-interface {v0, v1}, Lorg/xmlpull/v1/XmlPullParser;->setInput(Ljava/io/Reader;)V

    .line 550
    :cond_0
    :goto_0
    invoke-interface {v0}, Lorg/xmlpull/v1/XmlPullParser;->next()I

    move-result v2

    if-eq v2, v4, :cond_2

    .line 552
    invoke-interface {v0}, Lorg/xmlpull/v1/XmlPullParser;->getEventType()I

    move-result v2

    const/4 v3, 0x2

    if-ne v2, v3, :cond_0

    .line 554
    invoke-interface {v0}, Lorg/xmlpull/v1/XmlPullParser;->getName()Ljava/lang/String;

    move-result-object v2

    .line 557
    const-string v3, "property"

    invoke-virtual {v3, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 559
    new-instance v2, Lcom/osp/app/util/aj;

    const/4 v3, 0x0

    invoke-direct {v2, v3}, Lcom/osp/app/util/aj;-><init>(B)V

    iput-object v2, p0, Lcom/osp/app/util/ah;->b:Lcom/osp/app/util/aj;

    .line 560
    iget-object v2, p0, Lcom/osp/app/util/ah;->b:Lcom/osp/app/util/aj;

    invoke-virtual {v2, v0}, Lcom/osp/app/util/aj;->a(Lorg/xmlpull/v1/XmlPullParser;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 578
    :catchall_0
    move-exception v0

    :goto_1
    if-eqz v1, :cond_1

    .line 580
    invoke-virtual {v1}, Ljava/io/StringReader;->close()V

    .line 578
    :cond_1
    throw v0

    .line 563
    :cond_2
    :try_start_2
    const-string v0, "STG1"

    invoke-virtual {p0}, Lcom/osp/app/util/ah;->f()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_3

    const-string v0, "STG2"

    invoke-virtual {p0}, Lcom/osp/app/util/ah;->f()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 565
    :cond_3
    invoke-static {}, Lcom/osp/app/signin/SamsungService;->d()V

    .line 567
    :cond_4
    iget-object v0, p0, Lcom/osp/app/util/ah;->b:Lcom/osp/app/util/aj;

    iget-object v0, v0, Lcom/osp/app/util/aj;->a:Lcom/osp/app/util/ai;

    iget-object v0, v0, Lcom/osp/app/util/ai;->b:Ljava/lang/String;

    if-eqz v0, :cond_5

    iget-object v0, p0, Lcom/osp/app/util/ah;->b:Lcom/osp/app/util/aj;

    iget-object v0, v0, Lcom/osp/app/util/aj;->a:Lcom/osp/app/util/ai;

    iget-object v0, v0, Lcom/osp/app/util/ai;->b:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_5

    .line 569
    iget-object v0, p0, Lcom/osp/app/util/ah;->b:Lcom/osp/app/util/aj;

    iget-object v0, v0, Lcom/osp/app/util/aj;->a:Lcom/osp/app/util/ai;

    iget-object v0, v0, Lcom/osp/app/util/ai;->b:Ljava/lang/String;

    invoke-static {v0}, Lcom/osp/app/signin/SamsungService;->a(Ljava/lang/String;)V

    .line 570
    const/4 v0, 0x1

    invoke-static {v0}, Lcom/osp/app/signin/SamsungService;->d(Z)V

    .line 572
    :cond_5
    iget-object v0, p0, Lcom/osp/app/util/ah;->b:Lcom/osp/app/util/aj;

    iget-object v0, v0, Lcom/osp/app/util/aj;->a:Lcom/osp/app/util/ai;

    iget-object v0, v0, Lcom/osp/app/util/ai;->c:Ljava/lang/String;

    if-eqz v0, :cond_6

    iget-object v0, p0, Lcom/osp/app/util/ah;->b:Lcom/osp/app/util/aj;

    iget-object v0, v0, Lcom/osp/app/util/aj;->a:Lcom/osp/app/util/ai;

    iget-object v0, v0, Lcom/osp/app/util/ai;->c:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_6

    .line 574
    iget-object v0, p0, Lcom/osp/app/util/ah;->b:Lcom/osp/app/util/aj;

    iget-object v0, v0, Lcom/osp/app/util/aj;->a:Lcom/osp/app/util/ai;

    iget-object v0, v0, Lcom/osp/app/util/ai;->c:Ljava/lang/String;

    invoke-static {v0}, Lcom/osp/app/signin/SamsungService;->b(Ljava/lang/String;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 578
    :cond_6
    if-eqz v1, :cond_7

    .line 580
    invoke-virtual {v1}, Ljava/io/StringReader;->close()V

    .line 582
    :cond_7
    return-void

    .line 578
    :catchall_1
    move-exception v0

    move-object v1, v2

    goto :goto_1
.end method


# virtual methods
.method public final a(Ljava/lang/String;)Ljava/lang/String;
    .locals 2

    .prologue
    .line 432
    invoke-virtual {p0}, Lcom/osp/app/util/ah;->f()Ljava/lang/String;

    move-result-object v0

    .line 433
    const-string v1, "STG1"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_6

    .line 435
    const-string v0, "auth.samsungosp.com"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 437
    const-string p1, "54.208.185.179"

    .line 470
    :cond_0
    :goto_0
    return-object p1

    .line 438
    :cond_1
    const-string v0, "account.samsung.com"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 440
    const-string p1, "stg-account.samsung.com"

    goto :goto_0

    .line 441
    :cond_2
    const-string v0, "api.samsungosp.com"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 443
    const-string p1, "54.208.185.179"

    goto :goto_0

    .line 444
    :cond_3
    const-string v0, "www.ospserver.net"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 446
    const-string p1, "stg-www.ospserver.net"

    goto :goto_0

    .line 447
    :cond_4
    const-string v0, "us.account.samsung.com"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_5

    const-string v0, "chn.account.samsung.com"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 449
    :cond_5
    const-string p1, "stg-account.samsung.com"

    goto :goto_0

    .line 451
    :cond_6
    const-string v1, "STG2"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 453
    const-string v0, "auth.samsungosp.com"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_7

    .line 455
    const-string p1, "stg-auth.samsungosp.com"

    goto :goto_0

    .line 456
    :cond_7
    const-string v0, "account.samsung.com"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_8

    .line 458
    const-string p1, "account.samsung.com"

    goto :goto_0

    .line 459
    :cond_8
    const-string v0, "api.samsungosp.com"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_9

    .line 461
    const-string p1, "stg-api.samsungosp.com"

    goto :goto_0

    .line 462
    :cond_9
    const-string v0, "www.ospserver.net"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_a

    .line 464
    const-string p1, "www.ospserver.net"

    goto :goto_0

    .line 465
    :cond_a
    const-string v0, "us.account.samsung.com"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_b

    const-string v0, "chn.account.samsung.com"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 467
    :cond_b
    const-string p1, "stg-us.account.samsung.com"

    goto :goto_0
.end method

.method public final b()Ljava/lang/String;
    .locals 3

    .prologue
    .line 138
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v0, "TPM"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "getTestIdProperty : "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, Lcom/osp/app/util/ah;->b:Lcom/osp/app/util/aj;

    iget-object v2, v2, Lcom/osp/app/util/aj;->a:Lcom/osp/app/util/ai;

    iget-object v2, v2, Lcom/osp/app/util/ai;->f:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 139
    iget-object v0, p0, Lcom/osp/app/util/ah;->b:Lcom/osp/app/util/aj;

    iget-object v0, v0, Lcom/osp/app/util/aj;->a:Lcom/osp/app/util/ai;

    iget-object v0, v0, Lcom/osp/app/util/ai;->f:Ljava/lang/String;

    return-object v0
.end method

.method public final c()Z
    .locals 1

    .prologue
    .line 148
    iget-object v0, p0, Lcom/osp/app/util/ah;->b:Lcom/osp/app/util/aj;

    iget-object v0, v0, Lcom/osp/app/util/aj;->a:Lcom/osp/app/util/ai;

    iget-object v0, v0, Lcom/osp/app/util/ai;->f:Ljava/lang/String;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/osp/app/util/ah;->b:Lcom/osp/app/util/aj;

    iget-object v0, v0, Lcom/osp/app/util/aj;->a:Lcom/osp/app/util/ai;

    iget-object v0, v0, Lcom/osp/app/util/ai;->f:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    .line 149
    :goto_0
    return v0

    .line 148
    :cond_0
    const/4 v0, 0x0

    .line 149
    goto :goto_0
.end method

.method public final d()Ljava/lang/String;
    .locals 3

    .prologue
    .line 158
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v0, "TPM"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "getCSCProperty : "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, Lcom/osp/app/util/ah;->b:Lcom/osp/app/util/aj;

    iget-object v2, v2, Lcom/osp/app/util/aj;->a:Lcom/osp/app/util/ai;

    iget-object v2, v2, Lcom/osp/app/util/ai;->d:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 159
    iget-object v0, p0, Lcom/osp/app/util/ah;->b:Lcom/osp/app/util/aj;

    iget-object v0, v0, Lcom/osp/app/util/aj;->a:Lcom/osp/app/util/ai;

    iget-object v0, v0, Lcom/osp/app/util/ai;->d:Ljava/lang/String;

    return-object v0
.end method

.method public final e()Ljava/lang/String;
    .locals 3

    .prologue
    .line 168
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v0, "TPM"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "getModelProperty : "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, Lcom/osp/app/util/ah;->b:Lcom/osp/app/util/aj;

    iget-object v2, v2, Lcom/osp/app/util/aj;->a:Lcom/osp/app/util/ai;

    iget-object v2, v2, Lcom/osp/app/util/ai;->e:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 169
    iget-object v0, p0, Lcom/osp/app/util/ah;->b:Lcom/osp/app/util/aj;

    iget-object v0, v0, Lcom/osp/app/util/aj;->a:Lcom/osp/app/util/ai;

    iget-object v0, v0, Lcom/osp/app/util/ai;->e:Ljava/lang/String;

    return-object v0
.end method

.method public final f()Ljava/lang/String;
    .locals 1

    .prologue
    .line 178
    iget-object v0, p0, Lcom/osp/app/util/ah;->b:Lcom/osp/app/util/aj;

    iget-object v0, v0, Lcom/osp/app/util/aj;->b:Lcom/osp/app/util/ak;

    iget-object v0, v0, Lcom/osp/app/util/ak;->b:Ljava/lang/String;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/osp/app/util/ah;->b:Lcom/osp/app/util/aj;

    iget-object v0, v0, Lcom/osp/app/util/aj;->b:Lcom/osp/app/util/ak;

    iget-object v0, v0, Lcom/osp/app/util/ak;->b:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    .line 180
    iget-object v0, p0, Lcom/osp/app/util/ah;->b:Lcom/osp/app/util/aj;

    iget-object v0, v0, Lcom/osp/app/util/aj;->b:Lcom/osp/app/util/ak;

    iget-object v0, v0, Lcom/osp/app/util/ak;->b:Ljava/lang/String;

    .line 186
    :goto_0
    return-object v0

    .line 181
    :cond_0
    iget-object v0, p0, Lcom/osp/app/util/ah;->b:Lcom/osp/app/util/aj;

    iget-object v0, v0, Lcom/osp/app/util/aj;->a:Lcom/osp/app/util/ai;

    iget-object v0, v0, Lcom/osp/app/util/ai;->a:Ljava/lang/String;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/osp/app/util/ah;->b:Lcom/osp/app/util/aj;

    iget-object v0, v0, Lcom/osp/app/util/aj;->a:Lcom/osp/app/util/ai;

    iget-object v0, v0, Lcom/osp/app/util/ai;->a:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_1

    .line 183
    iget-object v0, p0, Lcom/osp/app/util/ah;->b:Lcom/osp/app/util/aj;

    iget-object v0, v0, Lcom/osp/app/util/aj;->a:Lcom/osp/app/util/ai;

    iget-object v0, v0, Lcom/osp/app/util/ai;->a:Ljava/lang/String;

    goto :goto_0

    .line 186
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final g()Z
    .locals 4

    .prologue
    const/4 v0, 0x0

    .line 196
    iget-object v1, p0, Lcom/osp/app/util/ah;->b:Lcom/osp/app/util/aj;

    iget-object v1, v1, Lcom/osp/app/util/aj;->a:Lcom/osp/app/util/ai;

    iget-object v1, v1, Lcom/osp/app/util/ai;->g:Ljava/lang/String;

    if-eqz v1, :cond_1

    iget-object v1, p0, Lcom/osp/app/util/ah;->b:Lcom/osp/app/util/aj;

    iget-object v1, v1, Lcom/osp/app/util/aj;->a:Lcom/osp/app/util/ai;

    iget-object v1, v1, Lcom/osp/app/util/ai;->g:Ljava/lang/String;

    invoke-virtual {v1}, Ljava/lang/String;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_1

    .line 198
    const-string v1, "false"

    iget-object v2, p0, Lcom/osp/app/util/ah;->b:Lcom/osp/app/util/aj;

    iget-object v2, v2, Lcom/osp/app/util/aj;->a:Lcom/osp/app/util/ai;

    iget-object v2, v2, Lcom/osp/app/util/ai;->g:Ljava/lang/String;

    sget-object v3, Ljava/util/Locale;->ENGLISH:Ljava/util/Locale;

    invoke-virtual {v2, v3}, Ljava/lang/String;->toLowerCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 201
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v1, "TPM"

    const-string v2, "isNoProxy : false"

    invoke-static {v1, v2}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 211
    :cond_0
    :goto_0
    return v0

    .line 204
    :cond_1
    invoke-static {}, Lcom/osp/app/signin/SamsungService;->j()Z

    move-result v1

    if-nez v1, :cond_0

    .line 210
    :cond_2
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v0, "TPM"

    const-string v1, "isNoProxy(default) : true"

    invoke-static {v0, v1}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 211
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public final h()Ljava/lang/String;
    .locals 1

    .prologue
    .line 220
    iget-object v0, p0, Lcom/osp/app/util/ah;->b:Lcom/osp/app/util/aj;

    iget-object v0, v0, Lcom/osp/app/util/aj;->b:Lcom/osp/app/util/ak;

    iget-object v0, v0, Lcom/osp/app/util/ak;->c:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 222
    const/4 v0, 0x0

    .line 225
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/osp/app/util/ah;->b:Lcom/osp/app/util/aj;

    iget-object v0, v0, Lcom/osp/app/util/aj;->b:Lcom/osp/app/util/ak;

    iget-object v0, v0, Lcom/osp/app/util/ak;->c:Ljava/lang/String;

    goto :goto_0
.end method

.method public final i()Ljava/lang/String;
    .locals 1

    .prologue
    .line 235
    iget-object v0, p0, Lcom/osp/app/util/ah;->b:Lcom/osp/app/util/aj;

    iget-object v0, v0, Lcom/osp/app/util/aj;->b:Lcom/osp/app/util/ak;

    iget-object v0, v0, Lcom/osp/app/util/ak;->d:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 237
    const/4 v0, 0x0

    .line 240
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/osp/app/util/ah;->b:Lcom/osp/app/util/aj;

    iget-object v0, v0, Lcom/osp/app/util/aj;->b:Lcom/osp/app/util/ak;

    iget-object v0, v0, Lcom/osp/app/util/ak;->d:Ljava/lang/String;

    goto :goto_0
.end method

.method public final j()Ljava/lang/String;
    .locals 1

    .prologue
    .line 250
    iget-object v0, p0, Lcom/osp/app/util/ah;->b:Lcom/osp/app/util/aj;

    iget-object v0, v0, Lcom/osp/app/util/aj;->b:Lcom/osp/app/util/ak;

    iget-object v0, v0, Lcom/osp/app/util/ak;->e:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 252
    const/4 v0, 0x0

    .line 255
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/osp/app/util/ah;->b:Lcom/osp/app/util/aj;

    iget-object v0, v0, Lcom/osp/app/util/aj;->b:Lcom/osp/app/util/ak;

    iget-object v0, v0, Lcom/osp/app/util/ak;->e:Ljava/lang/String;

    goto :goto_0
.end method

.method public final k()Ljava/lang/String;
    .locals 1

    .prologue
    .line 265
    iget-object v0, p0, Lcom/osp/app/util/ah;->b:Lcom/osp/app/util/aj;

    iget-object v0, v0, Lcom/osp/app/util/aj;->b:Lcom/osp/app/util/ak;

    iget-object v0, v0, Lcom/osp/app/util/ak;->f:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 267
    const/4 v0, 0x0

    .line 270
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/osp/app/util/ah;->b:Lcom/osp/app/util/aj;

    iget-object v0, v0, Lcom/osp/app/util/aj;->b:Lcom/osp/app/util/ak;

    iget-object v0, v0, Lcom/osp/app/util/ak;->f:Ljava/lang/String;

    goto :goto_0
.end method

.method public final l()Ljava/lang/String;
    .locals 1

    .prologue
    .line 280
    iget-object v0, p0, Lcom/osp/app/util/ah;->b:Lcom/osp/app/util/aj;

    iget-object v0, v0, Lcom/osp/app/util/aj;->b:Lcom/osp/app/util/ak;

    iget-object v0, v0, Lcom/osp/app/util/ak;->g:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 282
    const/4 v0, 0x0

    .line 285
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/osp/app/util/ah;->b:Lcom/osp/app/util/aj;

    iget-object v0, v0, Lcom/osp/app/util/aj;->b:Lcom/osp/app/util/ak;

    iget-object v0, v0, Lcom/osp/app/util/ak;->g:Ljava/lang/String;

    goto :goto_0
.end method

.method public final m()Z
    .locals 2

    .prologue
    .line 293
    iget-object v0, p0, Lcom/osp/app/util/ah;->b:Lcom/osp/app/util/aj;

    iget-object v0, v0, Lcom/osp/app/util/aj;->b:Lcom/osp/app/util/ak;

    iget-object v0, v0, Lcom/osp/app/util/ak;->h:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 295
    const-string v0, "yes"

    iget-object v1, p0, Lcom/osp/app/util/ah;->b:Lcom/osp/app/util/aj;

    iget-object v1, v1, Lcom/osp/app/util/aj;->b:Lcom/osp/app/util/ak;

    iget-object v1, v1, Lcom/osp/app/util/ak;->h:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 297
    const/4 v0, 0x1

    .line 301
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final n()Ljava/lang/String;
    .locals 1

    .prologue
    .line 310
    iget-object v0, p0, Lcom/osp/app/util/ah;->b:Lcom/osp/app/util/aj;

    iget-object v0, v0, Lcom/osp/app/util/aj;->b:Lcom/osp/app/util/ak;

    iget-object v0, v0, Lcom/osp/app/util/ak;->i:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 312
    const/4 v0, 0x0

    .line 315
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/osp/app/util/ah;->b:Lcom/osp/app/util/aj;

    iget-object v0, v0, Lcom/osp/app/util/aj;->b:Lcom/osp/app/util/ak;

    iget-object v0, v0, Lcom/osp/app/util/ak;->i:Ljava/lang/String;

    goto :goto_0
.end method

.method public final o()Ljava/lang/String;
    .locals 2

    .prologue
    const/4 v0, 0x0

    .line 325
    iget-object v1, p0, Lcom/osp/app/util/ah;->b:Lcom/osp/app/util/aj;

    iget-object v1, v1, Lcom/osp/app/util/aj;->b:Lcom/osp/app/util/ak;

    iget-object v1, v1, Lcom/osp/app/util/ak;->j:Ljava/lang/String;

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 342
    :cond_0
    :goto_0
    return-object v0

    .line 332
    :cond_1
    :try_start_0
    iget-object v1, p0, Lcom/osp/app/util/ah;->b:Lcom/osp/app/util/aj;

    iget-object v1, v1, Lcom/osp/app/util/aj;->b:Lcom/osp/app/util/ak;

    iget-object v1, v1, Lcom/osp/app/util/ak;->j:Ljava/lang/String;

    invoke-static {v1}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v1

    if-lez v1, :cond_0

    .line 334
    iget-object v1, p0, Lcom/osp/app/util/ah;->b:Lcom/osp/app/util/aj;

    iget-object v1, v1, Lcom/osp/app/util/aj;->b:Lcom/osp/app/util/ak;

    iget-object v0, v1, Lcom/osp/app/util/ak;->j:Ljava/lang/String;
    :try_end_0
    .catch Ljava/lang/NumberFormatException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 339
    :catch_0
    move-exception v1

    invoke-virtual {v1}, Ljava/lang/NumberFormatException;->printStackTrace()V

    goto :goto_0
.end method

.method public final p()Z
    .locals 2

    .prologue
    .line 351
    iget-object v0, p0, Lcom/osp/app/util/ah;->b:Lcom/osp/app/util/aj;

    iget-object v0, v0, Lcom/osp/app/util/aj;->b:Lcom/osp/app/util/ak;

    iget-object v0, v0, Lcom/osp/app/util/ak;->k:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 353
    const-string v0, "yes"

    iget-object v1, p0, Lcom/osp/app/util/ah;->b:Lcom/osp/app/util/aj;

    iget-object v1, v1, Lcom/osp/app/util/aj;->b:Lcom/osp/app/util/ak;

    iget-object v1, v1, Lcom/osp/app/util/ak;->k:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 355
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v0, "TPM"

    const-string v1, "enabled supportPhoneNumberID"

    invoke-static {v0, v1}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 356
    const/4 v0, 0x1

    .line 360
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final q()Z
    .locals 1

    .prologue
    .line 364
    iget-object v0, p0, Lcom/osp/app/util/ah;->b:Lcom/osp/app/util/aj;

    iget-object v0, v0, Lcom/osp/app/util/aj;->b:Lcom/osp/app/util/ak;

    invoke-static {v0}, Lcom/osp/app/util/ak;->a(Lcom/osp/app/util/ak;)Z

    move-result v0

    return v0
.end method

.method public final r()Ljava/lang/String;
    .locals 2

    .prologue
    const/4 v0, 0x0

    .line 371
    iget-object v1, p0, Lcom/osp/app/util/ah;->b:Lcom/osp/app/util/aj;

    iget-object v1, v1, Lcom/osp/app/util/aj;->b:Lcom/osp/app/util/ak;

    iget-object v1, v1, Lcom/osp/app/util/ak;->l:Ljava/lang/String;

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 388
    :cond_0
    :goto_0
    return-object v0

    .line 378
    :cond_1
    :try_start_0
    iget-object v1, p0, Lcom/osp/app/util/ah;->b:Lcom/osp/app/util/aj;

    iget-object v1, v1, Lcom/osp/app/util/aj;->b:Lcom/osp/app/util/ak;

    iget-object v1, v1, Lcom/osp/app/util/ak;->l:Ljava/lang/String;

    invoke-static {v1}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v1

    if-lez v1, :cond_0

    .line 380
    iget-object v1, p0, Lcom/osp/app/util/ah;->b:Lcom/osp/app/util/aj;

    iget-object v1, v1, Lcom/osp/app/util/aj;->b:Lcom/osp/app/util/ak;

    iget-object v0, v1, Lcom/osp/app/util/ak;->l:Ljava/lang/String;
    :try_end_0
    .catch Ljava/lang/NumberFormatException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 385
    :catch_0
    move-exception v1

    invoke-virtual {v1}, Ljava/lang/NumberFormatException;->printStackTrace()V

    goto :goto_0
.end method

.method public final s()Z
    .locals 2

    .prologue
    .line 398
    iget-object v0, p0, Lcom/osp/app/util/ah;->b:Lcom/osp/app/util/aj;

    iget-object v0, v0, Lcom/osp/app/util/aj;->b:Lcom/osp/app/util/ak;

    iget-object v0, v0, Lcom/osp/app/util/ak;->m:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 400
    const-string v0, "yes"

    iget-object v1, p0, Lcom/osp/app/util/ah;->b:Lcom/osp/app/util/aj;

    iget-object v1, v1, Lcom/osp/app/util/aj;->b:Lcom/osp/app/util/ak;

    iget-object v1, v1, Lcom/osp/app/util/ak;->m:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 402
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v0, "TPM"

    const-string v1, "set ignore My Profile Web"

    invoke-static {v0, v1}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 403
    const/4 v0, 0x1

    .line 407
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final t()Z
    .locals 2

    .prologue
    .line 414
    iget-object v0, p0, Lcom/osp/app/util/ah;->b:Lcom/osp/app/util/aj;

    iget-object v0, v0, Lcom/osp/app/util/aj;->b:Lcom/osp/app/util/ak;

    iget-object v0, v0, Lcom/osp/app/util/ak;->n:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 416
    const-string v0, "yes"

    iget-object v1, p0, Lcom/osp/app/util/ah;->b:Lcom/osp/app/util/aj;

    iget-object v1, v1, Lcom/osp/app/util/aj;->b:Lcom/osp/app/util/ak;

    iget-object v1, v1, Lcom/osp/app/util/ak;->n:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 418
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v0, "TPM"

    const-string v1, "set enabled displayDefaultLaunchUI"

    invoke-static {v0, v1}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 419
    const/4 v0, 0x1

    .line 423
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final u()Z
    .locals 1

    .prologue
    .line 479
    invoke-virtual {p0}, Lcom/osp/app/util/ah;->f()Ljava/lang/String;

    move-result-object v0

    .line 480
    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final v()Z
    .locals 1

    .prologue
    .line 489
    iget-boolean v0, p0, Lcom/osp/app/util/ah;->c:Z

    return v0
.end method

.method public final w()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 496
    const/4 v1, 0x0

    .line 499
    :try_start_0
    new-instance v0, Ljava/io/File;

    sget-object v2, Lcom/osp/app/util/ah;->a:Ljava/lang/String;

    invoke-direct {v0, v2}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 500
    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 502
    sget-object v0, Lcom/osp/app/util/ah;->a:Ljava/lang/String;

    invoke-static {v0}, Lcom/sec/a/a/a;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 503
    if-eqz v1, :cond_0

    invoke-virtual {v1}, Ljava/lang/String;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    .line 505
    invoke-direct {p0, v1}, Lcom/osp/app/util/ah;->c(Ljava/lang/String;)V

    .line 506
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/osp/app/util/ah;->c:Z

    .line 507
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v0, "TPM"

    const-string v2, "Property set"

    invoke-static {v0, v2}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 508
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v2, "Property set : "

    invoke-direct {v0, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/osp/app/util/aq;->c(Ljava/lang/String;)V

    .line 531
    :goto_0
    return-void

    .line 511
    :cond_0
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v0, "TPM"

    const-string v2, "Property is empty"

    invoke-static {v0, v2}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catch Lorg/xmlpull/v1/XmlPullParserException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_2

    goto :goto_0

    .line 517
    :catch_0
    move-exception v0

    .line 519
    new-instance v2, Lcom/osp/app/util/aj;

    invoke-direct {v2, v3}, Lcom/osp/app/util/aj;-><init>(B)V

    iput-object v2, p0, Lcom/osp/app/util/ah;->b:Lcom/osp/app/util/aj;

    .line 520
    invoke-virtual {v0}, Lorg/xmlpull/v1/XmlPullParserException;->printStackTrace()V

    .line 521
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v0, "TPM"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Property : "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 515
    :cond_1
    :try_start_1
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v0, "TPM"

    const-string v2, "There is no property file"

    invoke-static {v0, v2}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_1
    .catch Lorg/xmlpull/v1/XmlPullParserException; {:try_start_1 .. :try_end_1} :catch_0
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_2

    goto :goto_0

    .line 522
    :catch_1
    move-exception v0

    .line 524
    new-instance v2, Lcom/osp/app/util/aj;

    invoke-direct {v2, v3}, Lcom/osp/app/util/aj;-><init>(B)V

    iput-object v2, p0, Lcom/osp/app/util/ah;->b:Lcom/osp/app/util/aj;

    .line 525
    invoke-virtual {v0}, Ljava/io/IOException;->printStackTrace()V

    .line 526
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v0, "TPM"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Property : "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 527
    :catch_2
    move-exception v0

    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_0
.end method
