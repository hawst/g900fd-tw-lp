.class public final enum Lcom/osp/app/util/s;
.super Ljava/lang/Enum;
.source "LocalBusinessException.java"


# static fields
.field public static final enum a:Lcom/osp/app/util/s;

.field public static final enum b:Lcom/osp/app/util/s;

.field public static final enum c:Lcom/osp/app/util/s;

.field private static final synthetic d:[Lcom/osp/app/util/s;


# direct methods
.method static constructor <clinit>()V
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 51
    new-instance v0, Lcom/osp/app/util/s;

    const-string v1, "SCREEN_SIZE_8"

    invoke-direct {v0, v1, v2}, Lcom/osp/app/util/s;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/osp/app/util/s;->a:Lcom/osp/app/util/s;

    new-instance v0, Lcom/osp/app/util/s;

    const-string v1, "SCREEN_SIZE_10"

    invoke-direct {v0, v1, v3}, Lcom/osp/app/util/s;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/osp/app/util/s;->b:Lcom/osp/app/util/s;

    new-instance v0, Lcom/osp/app/util/s;

    const-string v1, "SCREEN_SIZE_12"

    invoke-direct {v0, v1, v4}, Lcom/osp/app/util/s;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/osp/app/util/s;->c:Lcom/osp/app/util/s;

    .line 49
    const/4 v0, 0x3

    new-array v0, v0, [Lcom/osp/app/util/s;

    sget-object v1, Lcom/osp/app/util/s;->a:Lcom/osp/app/util/s;

    aput-object v1, v0, v2

    sget-object v1, Lcom/osp/app/util/s;->b:Lcom/osp/app/util/s;

    aput-object v1, v0, v3

    sget-object v1, Lcom/osp/app/util/s;->c:Lcom/osp/app/util/s;

    aput-object v1, v0, v4

    sput-object v0, Lcom/osp/app/util/s;->d:[Lcom/osp/app/util/s;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0

    .prologue
    .line 49
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/osp/app/util/s;
    .locals 1

    .prologue
    .line 49
    const-class v0, Lcom/osp/app/util/s;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/osp/app/util/s;

    return-object v0
.end method

.method public static values()[Lcom/osp/app/util/s;
    .locals 1

    .prologue
    .line 49
    sget-object v0, Lcom/osp/app/util/s;->d:[Lcom/osp/app/util/s;

    invoke-virtual {v0}, [Lcom/osp/app/util/s;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/osp/app/util/s;

    return-object v0
.end method
