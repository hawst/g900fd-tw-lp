.class public final enum Lcom/osp/app/util/c;
.super Ljava/lang/Enum;
.source "AnimationUtils.java"


# static fields
.field public static final enum a:Lcom/osp/app/util/c;

.field public static final enum b:Lcom/osp/app/util/c;

.field public static final enum c:Lcom/osp/app/util/c;

.field private static final synthetic d:[Lcom/osp/app/util/c;


# direct methods
.method static constructor <clinit>()V
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 53
    new-instance v0, Lcom/osp/app/util/c;

    const-string v1, "SEGMENTS_33"

    invoke-direct {v0, v1, v2}, Lcom/osp/app/util/c;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/osp/app/util/c;->a:Lcom/osp/app/util/c;

    new-instance v0, Lcom/osp/app/util/c;

    const-string v1, "SEGMENTS_70"

    invoke-direct {v0, v1, v3}, Lcom/osp/app/util/c;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/osp/app/util/c;->b:Lcom/osp/app/util/c;

    new-instance v0, Lcom/osp/app/util/c;

    const-string v1, "SEGMENTS_90"

    invoke-direct {v0, v1, v4}, Lcom/osp/app/util/c;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/osp/app/util/c;->c:Lcom/osp/app/util/c;

    .line 51
    const/4 v0, 0x3

    new-array v0, v0, [Lcom/osp/app/util/c;

    sget-object v1, Lcom/osp/app/util/c;->a:Lcom/osp/app/util/c;

    aput-object v1, v0, v2

    sget-object v1, Lcom/osp/app/util/c;->b:Lcom/osp/app/util/c;

    aput-object v1, v0, v3

    sget-object v1, Lcom/osp/app/util/c;->c:Lcom/osp/app/util/c;

    aput-object v1, v0, v4

    sput-object v0, Lcom/osp/app/util/c;->d:[Lcom/osp/app/util/c;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0

    .prologue
    .line 51
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/osp/app/util/c;
    .locals 1

    .prologue
    .line 51
    const-class v0, Lcom/osp/app/util/c;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/osp/app/util/c;

    return-object v0
.end method

.method public static values()[Lcom/osp/app/util/c;
    .locals 1

    .prologue
    .line 51
    sget-object v0, Lcom/osp/app/util/c;->d:[Lcom/osp/app/util/c;

    invoke-virtual {v0}, [Lcom/osp/app/util/c;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/osp/app/util/c;

    return-object v0
.end method
