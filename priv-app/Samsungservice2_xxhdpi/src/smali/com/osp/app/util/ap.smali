.class public final Lcom/osp/app/util/ap;
.super Ljava/lang/Object;
.source "Toast.java"


# static fields
.field private static a:Landroid/widget/Toast;

.field private static b:Lcom/osp/app/util/ap;


# direct methods
.method private constructor <init>()V
    .locals 0

    .prologue
    .line 18
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 19
    return-void
.end method

.method public static a(Landroid/content/Context;II)Landroid/widget/Toast;
    .locals 1

    .prologue
    .line 74
    sget-object v0, Lcom/osp/app/util/ap;->a:Landroid/widget/Toast;

    if-nez v0, :cond_0

    .line 76
    invoke-static {p0, p1, p2}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    sput-object v0, Lcom/osp/app/util/ap;->a:Landroid/widget/Toast;

    .line 78
    :cond_0
    sget-object v0, Lcom/osp/app/util/ap;->a:Landroid/widget/Toast;

    invoke-virtual {v0, p1}, Landroid/widget/Toast;->setText(I)V

    .line 79
    sget-object v0, Lcom/osp/app/util/ap;->a:Landroid/widget/Toast;

    return-object v0
.end method

.method public static a(Landroid/content/Context;Ljava/lang/CharSequence;)Landroid/widget/Toast;
    .locals 1

    .prologue
    .line 57
    sget-object v0, Lcom/osp/app/util/ap;->a:Landroid/widget/Toast;

    if-nez v0, :cond_0

    .line 59
    const/4 v0, 0x0

    invoke-static {p0, p1, v0}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v0

    sput-object v0, Lcom/osp/app/util/ap;->a:Landroid/widget/Toast;

    .line 61
    :cond_0
    sget-object v0, Lcom/osp/app/util/ap;->a:Landroid/widget/Toast;

    invoke-virtual {v0, p1}, Landroid/widget/Toast;->setText(Ljava/lang/CharSequence;)V

    .line 62
    sget-object v0, Lcom/osp/app/util/ap;->a:Landroid/widget/Toast;

    return-object v0
.end method

.method public static a(Landroid/content/Context;Ljava/lang/String;I)Landroid/widget/Toast;
    .locals 1

    .prologue
    .line 40
    sget-object v0, Lcom/osp/app/util/ap;->a:Landroid/widget/Toast;

    if-nez v0, :cond_0

    .line 42
    invoke-static {p0, p1, p2}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v0

    sput-object v0, Lcom/osp/app/util/ap;->a:Landroid/widget/Toast;

    .line 44
    :cond_0
    sget-object v0, Lcom/osp/app/util/ap;->a:Landroid/widget/Toast;

    invoke-virtual {v0, p1}, Landroid/widget/Toast;->setText(Ljava/lang/CharSequence;)V

    .line 45
    sget-object v0, Lcom/osp/app/util/ap;->a:Landroid/widget/Toast;

    return-object v0
.end method

.method public static a()Lcom/osp/app/util/ap;
    .locals 1

    .prologue
    .line 22
    sget-object v0, Lcom/osp/app/util/ap;->b:Lcom/osp/app/util/ap;

    if-eqz v0, :cond_0

    .line 24
    sget-object v0, Lcom/osp/app/util/ap;->b:Lcom/osp/app/util/ap;

    .line 28
    :goto_0
    return-object v0

    .line 27
    :cond_0
    new-instance v0, Lcom/osp/app/util/ap;

    invoke-direct {v0}, Lcom/osp/app/util/ap;-><init>()V

    .line 28
    sput-object v0, Lcom/osp/app/util/ap;->b:Lcom/osp/app/util/ap;

    goto :goto_0
.end method
