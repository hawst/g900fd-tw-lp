.class public final Lcom/osp/app/util/v;
.super Ljava/lang/Object;
.source "RestrictionStringRemovalUtil.java"


# static fields
.field private static final a:[Ljava/lang/String;


# instance fields
.field private final b:Ljava/util/HashMap;

.field private final c:Ljava/util/HashMap;

.field private d:Ljava/util/HashMap;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    .line 21
    const/16 v0, 0x14

    new-array v0, v0, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string v2, "extra_prefs_set_back_text"

    aput-object v2, v0, v1

    const/4 v1, 0x1

    const-string v2, "extra_prefs_show_button_bar"

    aput-object v2, v0, v1

    const/4 v1, 0x2

    const-string v2, "extra_prefs_show_skip"

    aput-object v2, v0, v1

    const/4 v1, 0x3

    const-string v2, "only_access_points"

    aput-object v2, v0, v1

    const/4 v1, 0x4

    const-string v2, "wifi_enable_next_on_connect"

    aput-object v2, v0, v1

    const/4 v1, 0x5

    const-string v2, "mypackage"

    aput-object v2, v0, v1

    const/4 v1, 0x6

    const-string v2, "BG_WhoareU"

    aput-object v2, v0, v1

    const/4 v1, 0x7

    const-string v2, "MobileCountryCode"

    aput-object v2, v0, v1

    const/16 v1, 0x8

    const-string v2, "SIZE"

    aput-object v2, v0, v1

    const/16 v1, 0x9

    const-string v2, "key_name_check_result_key"

    aput-object v2, v0, v1

    const/16 v1, 0xa

    const-string v2, "key_name_check_method"

    aput-object v2, v0, v1

    const/16 v1, 0xb

    const-string v2, "is_show_email_validation"

    aput-object v2, v0, v1

    const/16 v1, 0xc

    const-string v2, "tncChanged"

    aput-object v2, v0, v1

    const/16 v1, 0xd

    const-string v2, "account_mode"

    aput-object v2, v0, v1

    const/16 v1, 0xe

    const-string v2, "OSP_VER"

    aput-object v2, v0, v1

    const/16 v1, 0xf

    const-string v2, "MODE"

    aput-object v2, v0, v1

    const/16 v1, 0x10

    const-string v2, "error_message"

    aput-object v2, v0, v1

    const/16 v1, 0x11

    const-string v2, "mcc"

    aput-object v2, v0, v1

    const/16 v1, 0x12

    const-string v2, "progress_theme"

    aput-object v2, v0, v1

    const/16 v1, 0x13

    const-string v2, "requestMode"

    aput-object v2, v0, v1

    sput-object v0, Lcom/osp/app/util/v;->a:[Ljava/lang/String;

    return-void
.end method

.method private constructor <init>()V
    .locals 3

    .prologue
    .line 66
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 67
    invoke-direct {p0}, Lcom/osp/app/util/v;->b()V

    .line 68
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/osp/app/util/v;->b:Ljava/util/HashMap;

    .line 69
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/osp/app/util/v;->c:Ljava/util/HashMap;

    .line 72
    iget-object v0, p0, Lcom/osp/app/util/v;->b:Ljava/util/HashMap;

    const-string v1, "login_id"

    const-string v2, "external_l_d"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 73
    iget-object v0, p0, Lcom/osp/app/util/v;->b:Ljava/util/HashMap;

    const-string v1, "passwordBlock"

    const-string v2, "external_p_block"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 74
    iget-object v0, p0, Lcom/osp/app/util/v;->b:Ljava/util/HashMap;

    const-string v1, "signUpInfo"

    const-string v2, "external_s_info"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 75
    iget-object v0, p0, Lcom/osp/app/util/v;->b:Ljava/util/HashMap;

    const-string v1, "apps_password"

    const-string v2, "external_a_p"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 76
    iget-object v0, p0, Lcom/osp/app/util/v;->b:Ljava/util/HashMap;

    const-string v1, "apps_birthdate"

    const-string v2, "external_a_b"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 77
    iget-object v0, p0, Lcom/osp/app/util/v;->b:Ljava/util/HashMap;

    const-string v1, "apps_id"

    const-string v2, "external_a_i"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 78
    iget-object v0, p0, Lcom/osp/app/util/v;->b:Ljava/util/HashMap;

    const-string v1, "apps_userid"

    const-string v2, "external_a_u"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 79
    iget-object v0, p0, Lcom/osp/app/util/v;->b:Ljava/util/HashMap;

    const-string v1, "mypackage"

    const-string v2, "external_m_p"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 80
    iget-object v0, p0, Lcom/osp/app/util/v;->b:Ljava/util/HashMap;

    const-string v1, "EmailID"

    const-string v2, "external_em"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 81
    iget-object v0, p0, Lcom/osp/app/util/v;->b:Ljava/util/HashMap;

    const-string v1, "Password"

    const-string v2, "external_pw"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 82
    iget-object v0, p0, Lcom/osp/app/util/v;->b:Ljava/util/HashMap;

    const-string v1, "URI"

    const-string v2, "internal_u_i"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 83
    iget-object v0, p0, Lcom/osp/app/util/v;->b:Ljava/util/HashMap;

    const-string v1, "com.msc.action.EMAIL_VALIDATION_COMPLETE_NOTIFICATION"

    const-string v2, "internal_em_validation_complete_noti"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 84
    iget-object v0, p0, Lcom/osp/app/util/v;->b:Ljava/util/HashMap;

    const-string v1, "email_id"

    const-string v2, "internal_internal_em_i"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 91
    iget-object v0, p0, Lcom/osp/app/util/v;->b:Ljava/util/HashMap;

    const-string v1, "NEED_AUTHCODE"

    const-string v2, "internal_need_a_code"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 92
    iget-object v0, p0, Lcom/osp/app/util/v;->b:Ljava/util/HashMap;

    const-string v1, "key_user_id"

    const-string v2, "internal_k_u_i"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 93
    iget-object v0, p0, Lcom/osp/app/util/v;->b:Ljava/util/HashMap;

    const-string v1, "com.msc.action.SETUPWIZARD_COMPLETE_SELFUPGRADE_NOTIFICATION"

    const-string v2, "internal_setupwizard_complete_selfupgrade_noti"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 94
    iget-object v0, p0, Lcom/osp/app/util/v;->b:Ljava/util/HashMap;

    const-string v1, "is_return_email_validation_check"

    const-string v2, "internal_is_return_em_validation_check"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 95
    iget-object v0, p0, Lcom/osp/app/util/v;->b:Ljava/util/HashMap;

    const-string v1, "key_is_email_verified"

    const-string v2, "internal_key_is_em_verified"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 96
    iget-object v0, p0, Lcom/osp/app/util/v;->b:Ljava/util/HashMap;

    const-string v1, "authcode"

    const-string v2, "common_a_code"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 97
    iget-object v0, p0, Lcom/osp/app/util/v;->b:Ljava/util/HashMap;

    const-string v1, "APPID"

    const-string v2, "common_a_i"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 98
    iget-object v0, p0, Lcom/osp/app/util/v;->b:Ljava/util/HashMap;

    const-string v1, "APPSECRET"

    const-string v2, "common_a_s"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 99
    iget-object v0, p0, Lcom/osp/app/util/v;->b:Ljava/util/HashMap;

    const-string v1, "client_id"

    const-string v2, "common_c_i"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 100
    iget-object v0, p0, Lcom/osp/app/util/v;->b:Ljava/util/HashMap;

    const-string v1, "client_secret"

    const-string v2, "common_c_s"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 101
    iget-object v0, p0, Lcom/osp/app/util/v;->b:Ljava/util/HashMap;

    const-string v1, "service_name"

    const-string v2, "common_s_n"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 102
    iget-object v0, p0, Lcom/osp/app/util/v;->b:Ljava/util/HashMap;

    const-string v1, "com.sec.android.app.secsetupwizard.SETUPWIZARD_COMPLETE"

    const-string v2, "common_action_sec_setupwizard_complete"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 103
    iget-object v0, p0, Lcom/osp/app/util/v;->b:Ljava/util/HashMap;

    const-string v1, "com.sec.android.app.phonesetupwizard.action.SETUPWIZARD_COMPLETE"

    const-string v2, "common_action_phone_setupwizard_complete"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 107
    iget-object v0, p0, Lcom/osp/app/util/v;->b:Ljava/util/HashMap;

    const-string v1, "com.osp.app.signin.action.EMAIL_VALIDATION_COMPLETED"

    const-string v2, "external_action_email_validation_completed"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 108
    iget-object v0, p0, Lcom/osp/app/util/v;->b:Ljava/util/HashMap;

    const-string v1, "com.msc.action.samsungaccount.request.BackGroundSignin"

    const-string v2, "external_action_request_BackGroundSignin"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 109
    iget-object v0, p0, Lcom/osp/app/util/v;->b:Ljava/util/HashMap;

    const-string v1, "com.msc.action.samsungaccount.response.BackGroundSignin"

    const-string v2, "external_action_response_BackGroundSignin"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 110
    iget-object v0, p0, Lcom/osp/app/util/v;->b:Ljava/util/HashMap;

    const-string v1, "com.msc.action.samsungaccount.Update_NewTerms"

    const-string v2, "action_sa_Update_NewTerms"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 111
    iget-object v0, p0, Lcom/osp/app/util/v;->b:Ljava/util/HashMap;

    const-string v1, "com.msc.action.samsungaccount.changepassword"

    const-string v2, "action_sa_change_p_w"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 112
    iget-object v0, p0, Lcom/osp/app/util/v;->b:Ljava/util/HashMap;

    const-string v1, "com.msc.action.samsungaccount.accountinfo"

    const-string v2, "action_sa_a_i"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 113
    iget-object v0, p0, Lcom/osp/app/util/v;->b:Ljava/util/HashMap;

    const-string v1, "com.msc.action.samsungaccount.changecreditcard"

    const-string v2, "action_sa_change_c_c"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 114
    iget-object v0, p0, Lcom/osp/app/util/v;->b:Ljava/util/HashMap;

    const-string v1, "android.intent.action.SAMSUNGACCOUNT_SIGNIN_COMPLETED"

    const-string v2, "action_sa_s_i_COMPLETED"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 115
    iget-object v0, p0, Lcom/osp/app/util/v;->b:Ljava/util/HashMap;

    const-string v1, "android.intent.action.SAMSUNGACCOUNT_SIGNOUT_COMPLETED"

    const-string v2, "action_sa_s_o_COMPLETED"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 116
    iget-object v0, p0, Lcom/osp/app/util/v;->b:Ljava/util/HashMap;

    const-string v1, "android.intent.action.SAMSUNGACCOUNT_SIGNUPINFO_CHANGED"

    const-string v2, "action_sa_s_i_CHANGED"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 117
    iget-object v0, p0, Lcom/osp/app/util/v;->b:Ljava/util/HashMap;

    const-string v1, "com.osp.app.signin.action.SAMSUNG_ACCOUNT_SETUPWIZARD"

    const-string v2, "action_sa_SETUPWIZARD"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 118
    iget-object v0, p0, Lcom/osp/app/util/v;->b:Ljava/util/HashMap;

    const-string v1, "com.osp.app.signin.SetupWizardActivity"

    const-string v2, "action_sa_SETUPWIZARD_NEW_NAMING"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 119
    iget-object v0, p0, Lcom/osp/app/util/v;->b:Ljava/util/HashMap;

    const-string v1, "osp.signin.SAMSUNG_ACCOUNT_SIGNOUT"

    const-string v2, "action_sa_SIGNOUT"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 120
    iget-object v0, p0, Lcom/osp/app/util/v;->b:Ljava/util/HashMap;

    const-string v1, "android.intent.action.REGISTRATION_CANCELED"

    const-string v2, "action_REGISTRATION_CANCELED"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 121
    iget-object v0, p0, Lcom/osp/app/util/v;->b:Ljava/util/HashMap;

    const-string v1, "android.intent.action.REGISTRATION_COMPLETED"

    const-string v2, "action_REGISTRATION_COMPLETED"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 122
    iget-object v0, p0, Lcom/osp/app/util/v;->b:Ljava/util/HashMap;

    const-string v1, "com.osp.ACCESSTOKEN.RESPONSE"

    const-string v2, "action_ACCESSTOKEN.RESPONSE"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 123
    iget-object v0, p0, Lcom/osp/app/util/v;->b:Ljava/util/HashMap;

    const-string v1, "com.osp.ACCESSTOKEN.FAIL"

    const-string v2, "action_ACCESSTOKEN.FAIL"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 124
    iget-object v0, p0, Lcom/osp/app/util/v;->b:Ljava/util/HashMap;

    const-string v1, "android.intent.action.DM_WIPEOUT_SUCCESS"

    const-string v2, "action_DM_WIPEOUT_SUCCESS"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 125
    iget-object v0, p0, Lcom/osp/app/util/v;->b:Ljava/util/HashMap;

    const-string v1, "android.intent.action.DM_WIPEOUT_FAILED"

    const-string v2, "action_DM_WIPEOUT_FAILED"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 126
    iget-object v0, p0, Lcom/osp/app/util/v;->b:Ljava/util/HashMap;

    const-string v1, "android.intent.action.SAMSUNG_ACCOUNT_SIGNOUT_SUCCESS"

    const-string v2, "action_SAMSUNG_ACCOUNT_SIGNOUT_SUCCESS"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 127
    iget-object v0, p0, Lcom/osp/app/util/v;->b:Ljava/util/HashMap;

    const-string v1, "android.intent.action.SAMSUNG_ACCOUNT_SIGNOUT_FAILED"

    const-string v2, "action_SAMSUNG_ACCOUNT_SIGNOUT_FAILED"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 128
    iget-object v0, p0, Lcom/osp/app/util/v;->b:Ljava/util/HashMap;

    const-string v1, "android.net.wifi.PICK_WIFI_NETWORK"

    const-string v2, "action_PICK_WIFI_NETWORK"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 129
    iget-object v0, p0, Lcom/osp/app/util/v;->b:Ljava/util/HashMap;

    const-string v1, "com.samsung.wipe.MTDATA"

    const-string v2, "action_wipe_MTDATA"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 130
    iget-object v0, p0, Lcom/osp/app/util/v;->b:Ljava/util/HashMap;

    const-string v1, "android.intent.action.DM_FACTORY_DATA_RESET"

    const-string v2, "action_DM_FACTORY_DATA_RESET"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 131
    iget-object v0, p0, Lcom/osp/app/util/v;->b:Ljava/util/HashMap;

    const-string v1, "android.intent.action.DM_ACCOUNT_SIGNIN_CHECK"

    const-string v2, "action_DM_ACCOUNT_SIGNIN_CHECK"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 132
    iget-object v0, p0, Lcom/osp/app/util/v;->b:Ljava/util/HashMap;

    const-string v1, "com.osp.EXTERNAL.ACCESSTOKEN"

    const-string v2, "action_EXTERNAL_ACCESSTOKEN"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 133
    iget-object v0, p0, Lcom/osp/app/util/v;->b:Ljava/util/HashMap;

    const-string v1, "android.intent.action.BOOT_COMPLETED"

    const-string v2, "action_BOOT_COMPLETED"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 134
    iget-object v0, p0, Lcom/osp/app/util/v;->b:Ljava/util/HashMap;

    const-string v1, "samsungaccount_clear_notification"

    const-string v2, "sa_clear_noti"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 135
    iget-object v0, p0, Lcom/osp/app/util/v;->b:Ljava/util/HashMap;

    const-string v1, "com.msc.action.DO_REQUEST_GLD"

    const-string v2, "action_DO_REQUEST_GLD"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 136
    iget-object v0, p0, Lcom/osp/app/util/v;->b:Ljava/util/HashMap;

    const-string v1, "com.msc.action.samsungaccount.tncs"

    const-string v2, "action_sa_tncs"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 137
    iget-object v0, p0, Lcom/osp/app/util/v;->b:Ljava/util/HashMap;

    const-string v1, "android.intent.action.PACKAGE_REPLACED"

    const-string v2, "action_PACKAGE_REPLACED"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 138
    iget-object v0, p0, Lcom/osp/app/util/v;->b:Ljava/util/HashMap;

    const-string v1, "android.intent.action.PACKAGE_REMOVED"

    const-string v2, "action_PACKAGE_REMOVED"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 139
    iget-object v0, p0, Lcom/osp/app/util/v;->b:Ljava/util/HashMap;

    const-string v1, "com.msc.action.samsungaccount.web_no_button"

    const-string v2, "action_web_n_b"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 140
    iget-object v0, p0, Lcom/osp/app/util/v;->b:Ljava/util/HashMap;

    const-string v1, "com.msc.action.samsungaccount.web_with_sign_in_screen_button"

    const-string v2, "action_web_w_s_i_s_b"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 141
    iget-object v0, p0, Lcom/osp/app/util/v;->b:Ljava/util/HashMap;

    const-string v1, "com.msc.action.samsungaccount.web_dialog"

    const-string v2, "action_web_d"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 142
    iget-object v0, p0, Lcom/osp/app/util/v;->b:Ljava/util/HashMap;

    const-string v1, "com.msc.action.samsungaccount.web_contact_us"

    const-string v2, "action_web_c_u"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 143
    iget-object v0, p0, Lcom/osp/app/util/v;->b:Ljava/util/HashMap;

    const-string v1, "com.msc.action.samsungaccount.web_customer_support"

    const-string v2, "action_web_c_s"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 144
    iget-object v0, p0, Lcom/osp/app/util/v;->b:Ljava/util/HashMap;

    const-string v1, "com.msc.action.samsungaccount.web_with_close_button"

    const-string v2, "action_web_w_c_b"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 147
    iget-object v0, p0, Lcom/osp/app/util/v;->b:Ljava/util/HashMap;

    const-string v1, "com.sec.android.app.samsungapps"

    const-string v2, "sa_apps"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 148
    iget-object v0, p0, Lcom/osp/app/util/v;->b:Ljava/util/HashMap;

    const-string v1, "com.mspot.android.aria"

    const-string v2, "musichub"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 149
    iget-object v0, p0, Lcom/osp/app/util/v;->b:Ljava/util/HashMap;

    const-string v1, "com.sec.android.app.gamehub"

    const-string v2, "gamehub"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 150
    iget-object v0, p0, Lcom/osp/app/util/v;->b:Ljava/util/HashMap;

    const-string v1, "com.sec.android.app.familystory"

    const-string v2, "familystory"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 151
    iget-object v0, p0, Lcom/osp/app/util/v;->b:Ljava/util/HashMap;

    const-string v1, "com.samsung.videohub"

    const-string v2, "videohub"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 152
    iget-object v0, p0, Lcom/osp/app/util/v;->b:Ljava/util/HashMap;

    const-string v1, "com.sec.msc.learninghub"

    const-string v2, "learninghub"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 153
    iget-object v0, p0, Lcom/osp/app/util/v;->b:Ljava/util/HashMap;

    const-string v1, "com.sec.android.sCloudRelayData"

    const-string v2, "sCloudRelayData"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 154
    iget-object v0, p0, Lcom/osp/app/util/v;->b:Ljava/util/HashMap;

    const-string v1, "com.coolots.chaton"

    const-string v2, "coolots_chaton_v"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 155
    iget-object v0, p0, Lcom/osp/app/util/v;->b:Ljava/util/HashMap;

    const-string v1, "com.sec.chaton"

    const-string v2, "chaton"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 156
    iget-object v0, p0, Lcom/osp/app/util/v;->b:Ljava/util/HashMap;

    const-string v1, "com.sec.readershub2.store"

    const-string v2, "readershub2_store"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 157
    iget-object v0, p0, Lcom/osp/app/util/v;->b:Ljava/util/HashMap;

    const-string v1, "com.samsung.android.app.directsharesettings"

    const-string v2, "directshare"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 158
    iget-object v0, p0, Lcom/osp/app/util/v;->b:Ljava/util/HashMap;

    const-string v1, "com.samsung.android.providers.always"

    const-string v2, "always"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 159
    iget-object v0, p0, Lcom/osp/app/util/v;->b:Ljava/util/HashMap;

    const-string v1, "com.sec.everglades"

    const-string v2, "everglades"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 160
    iget-object v0, p0, Lcom/osp/app/util/v;->b:Ljava/util/HashMap;

    const-string v1, "com.samsung.scloud.scloudstarter"

    const-string v2, "sCloudStarter"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 161
    iget-object v0, p0, Lcom/osp/app/util/v;->b:Ljava/util/HashMap;

    const-string v1, "com.sec.chatonforcanada"

    const-string v2, "chaton_canada"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 162
    iget-object v0, p0, Lcom/osp/app/util/v;->b:Ljava/util/HashMap;

    const-string v1, "com.tripadvisor.tripadvisor"

    const-string v2, "tripadvisor"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 163
    iget-object v0, p0, Lcom/osp/app/util/v;->b:Ljava/util/HashMap;

    const-string v1, "com.rharham.RunningPro"

    const-string v2, "running_pro"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 164
    iget-object v0, p0, Lcom/osp/app/util/v;->b:Ljava/util/HashMap;

    const-string v1, "com.sec.android.wallet"

    const-string v2, "sWallet"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 165
    iget-object v0, p0, Lcom/osp/app/util/v;->b:Ljava/util/HashMap;

    const-string v1, "com.sec.pcw"

    const-string v2, "sLink"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 168
    iget-object v0, p0, Lcom/osp/app/util/v;->c:Ljava/util/HashMap;

    const-string v1, "EmailID"

    const-string v2, "exist"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 169
    iget-object v0, p0, Lcom/osp/app/util/v;->c:Ljava/util/HashMap;

    const-string v1, "Password"

    const-string v2, "exist"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 170
    iget-object v0, p0, Lcom/osp/app/util/v;->c:Ljava/util/HashMap;

    const-string v1, "email_id"

    const-string v2, "exist"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 171
    iget-object v0, p0, Lcom/osp/app/util/v;->c:Ljava/util/HashMap;

    const-string v1, "URI"

    const-string v2, "exist"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 173
    return-void
.end method

.method synthetic constructor <init>(B)V
    .locals 0

    .prologue
    .line 10
    invoke-direct {p0}, Lcom/osp/app/util/v;-><init>()V

    return-void
.end method

.method public static a()Lcom/osp/app/util/v;
    .locals 1

    .prologue
    .line 182
    sget-object v0, Lcom/osp/app/util/w;->a:Lcom/osp/app/util/v;

    return-object v0
.end method

.method private b()V
    .locals 6

    .prologue
    .line 46
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/osp/app/util/v;->d:Ljava/util/HashMap;

    .line 47
    sget-object v1, Lcom/osp/app/util/v;->a:[Ljava/lang/String;

    array-length v2, v1

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v2, :cond_0

    aget-object v3, v1, v0

    .line 49
    iget-object v4, p0, Lcom/osp/app/util/v;->d:Ljava/util/HashMap;

    const/4 v5, 0x1

    invoke-static {v5}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v5

    invoke-virtual {v4, v3, v5}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 47
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 51
    :cond_0
    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    .locals 3

    .prologue
    .line 266
    const-string v0, "null"

    .line 268
    if-eqz p1, :cond_0

    if-eqz p2, :cond_0

    .line 270
    iget-object v0, p0, Lcom/osp/app/util/v;->c:Ljava/util/HashMap;

    invoke-virtual {v0, p1}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    const/4 v1, 0x1

    if-ne v0, v1, :cond_1

    .line 273
    iget-object v0, p0, Lcom/osp/app/util/v;->c:Ljava/util/HashMap;

    invoke-virtual {v0, p1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 274
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "inputValueString is : "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;)V

    .line 287
    :cond_0
    :goto_0
    return-object v0

    .line 277
    :cond_1
    const-string v0, "mypackage"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 279
    invoke-virtual {p0, p2}, Lcom/osp/app/util/v;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 284
    :goto_1
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "inputValueString is : "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;)V

    goto :goto_0

    :cond_2
    move-object v0, p2

    .line 282
    goto :goto_1
.end method

.method public final a(Ljava/lang/String;)Z
    .locals 1

    .prologue
    .line 60
    iget-object v0, p0, Lcom/osp/app/util/v;->d:Ljava/util/HashMap;

    invoke-virtual {v0, p1}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method public final b(Ljava/lang/String;)Ljava/lang/String;
    .locals 8

    .prologue
    const/4 v2, 0x2

    const/4 v1, 0x0

    .line 192
    if-eqz p1, :cond_1

    iget-object v0, p0, Lcom/osp/app/util/v;->b:Ljava/util/HashMap;

    invoke-virtual {v0, p1}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    const/4 v3, 0x1

    if-ne v0, v3, :cond_1

    .line 196
    iget-object v0, p0, Lcom/osp/app/util/v;->b:Ljava/util/HashMap;

    invoke-virtual {v0, p1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 197
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "inputKeyString is : "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;)V

    :goto_0
    move-object p1, v0

    .line 255
    :cond_0
    return-object p1

    .line 200
    :cond_1
    const-string v0, ""

    .line 203
    if-eqz p1, :cond_8

    .line 205
    :try_start_0
    const-string v3, "."

    invoke-virtual {p1, v3}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v3

    if-eqz v3, :cond_4

    .line 207
    const-string v0, "\\."

    invoke-virtual {p1, v0}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v1

    .line 208
    array-length v3, v1

    .line 210
    if-lt v3, v2, :cond_0

    .line 216
    new-instance v2, Ljava/lang/StringBuffer;

    const-string v0, ""

    invoke-direct {v2, v0}, Ljava/lang/StringBuffer;-><init>(Ljava/lang/String;)V

    .line 217
    add-int/lit8 v0, v3, -0x2

    :goto_1
    if-ge v0, v3, :cond_2

    .line 219
    aget-object v4, v1, v0

    invoke-virtual {v2, v4}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v4

    const/16 v5, 0x2e

    invoke-virtual {v4, v5}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    .line 217
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 221
    :cond_2
    invoke-virtual {v2}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 253
    :cond_3
    :goto_2
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "inputKeyString is : "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;)V

    goto :goto_0

    .line 222
    :cond_4
    :try_start_1
    const-string v3, "_"

    invoke-virtual {p1, v3}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v3

    if-eqz v3, :cond_9

    .line 224
    const-string v3, "_"

    invoke-virtual {p1, v3}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v4

    .line 225
    array-length v5, v4

    .line 227
    if-ge v5, v2, :cond_5

    .line 229
    const/4 v0, 0x0

    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v1

    div-int/lit8 v1, v1, 0x2

    add-int/lit8 v1, v1, 0x1

    invoke-virtual {p1, v0, v1}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v0

    goto :goto_2

    :cond_5
    move v3, v1

    .line 232
    :goto_3
    if-ge v3, v5, :cond_3

    .line 234
    aget-object v1, v4, v3

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v1

    .line 235
    if-lez v1, :cond_6

    .line 237
    const/4 v6, 0x3

    if-le v1, v6, :cond_7

    move v1, v2

    .line 238
    :goto_4
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v6, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    aget-object v6, v4, v3

    const/4 v7, 0x0

    invoke-virtual {v6, v7, v1}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "_"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    move-result-object v0

    .line 232
    :cond_6
    add-int/lit8 v1, v3, 0x1

    move v3, v1

    goto :goto_3

    .line 237
    :cond_7
    add-int/lit8 v1, v1, -0x1

    goto :goto_4

    :cond_8
    move-object p1, v0

    :cond_9
    move-object v0, p1

    .line 251
    goto :goto_2

    .line 247
    :catch_0
    move-exception v0

    move-object v1, v0

    .line 249
    const-string v0, "Unknown"

    .line 250
    invoke-virtual {v1}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_2
.end method
