.class public Lcom/osp/app/util/BasePreferenceActivity;
.super Landroid/preference/PreferenceActivity;
.source "BasePreferenceActivity.java"

# interfaces
.implements Landroid/view/View$OnFocusChangeListener;


# instance fields
.field private final a:Ljava/lang/String;

.field private final b:I

.field private final c:I

.field private final d:I

.field private final e:I

.field private final f:I

.field private final g:I

.field private final h:Lcom/osp/app/util/am;

.field private i:Z

.field private j:Z


# direct methods
.method public constructor <init>()V
    .locals 3

    .prologue
    const/16 v2, 0xc8

    const/16 v1, 0x32

    .line 32
    invoke-direct {p0}, Landroid/preference/PreferenceActivity;-><init>()V

    .line 34
    const-string v0, "ViewComponentAttribute"

    iput-object v0, p0, Lcom/osp/app/util/BasePreferenceActivity;->a:Ljava/lang/String;

    .line 39
    iput v1, p0, Lcom/osp/app/util/BasePreferenceActivity;->b:I

    .line 44
    iput v1, p0, Lcom/osp/app/util/BasePreferenceActivity;->c:I

    .line 49
    iput v2, p0, Lcom/osp/app/util/BasePreferenceActivity;->d:I

    .line 54
    iput v2, p0, Lcom/osp/app/util/BasePreferenceActivity;->e:I

    .line 59
    const/16 v0, 0xf

    iput v0, p0, Lcom/osp/app/util/BasePreferenceActivity;->f:I

    .line 64
    const/4 v0, 0x5

    iput v0, p0, Lcom/osp/app/util/BasePreferenceActivity;->g:I

    .line 66
    new-instance v0, Lcom/osp/app/util/am;

    invoke-direct {v0, p0}, Lcom/osp/app/util/am;-><init>(Landroid/app/Activity;)V

    iput-object v0, p0, Lcom/osp/app/util/BasePreferenceActivity;->h:Lcom/osp/app/util/am;

    return-void
.end method


# virtual methods
.method public final a(Landroid/view/ViewGroup;I)V
    .locals 12

    .prologue
    const v11, 0x7f0c0160

    const v10, 0x7f0c015f

    const v9, 0x106000d

    const/16 v8, 0x8

    const/4 v0, 0x0

    .line 632
    if-eqz p1, :cond_0

    invoke-static {}, Lcom/osp/device/b;->b()Lcom/osp/device/b;

    move-result-object v1

    invoke-virtual {v1, p0}, Lcom/osp/device/b;->i(Landroid/content/Context;)Z

    move-result v1

    if-nez v1, :cond_1

    .line 721
    :cond_0
    return-void

    .line 637
    :cond_1
    invoke-static {}, Lcom/osp/app/signin/SamsungService;->a()Z

    move-result v2

    .line 639
    const/4 v1, 0x2

    if-ne p2, v1, :cond_a

    .line 641
    invoke-virtual {p1}, Landroid/view/ViewGroup;->getChildCount()I

    move-result v3

    move v1, v0

    .line 642
    :goto_0
    if-ge v1, v3, :cond_0

    .line 645
    invoke-virtual {p1, v1}, Landroid/view/ViewGroup;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/View;->getId()I

    move-result v0

    if-ne v0, v11, :cond_2

    .line 647
    invoke-static {p0}, Lcom/osp/app/util/r;->l(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_5

    .line 649
    invoke-virtual {p1, v1}, Landroid/view/ViewGroup;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v9}, Landroid/view/View;->setBackgroundResource(I)V

    .line 663
    :cond_2
    :goto_1
    invoke-virtual {p1, v1}, Landroid/view/ViewGroup;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/View;->getId()I

    move-result v0

    if-eq v0, v10, :cond_3

    invoke-virtual {p1, v1}, Landroid/view/ViewGroup;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/View;->getId()I

    move-result v0

    const v4, 0x7f0c0162

    if-ne v0, v4, :cond_4

    .line 665
    :cond_3
    if-eqz v2, :cond_7

    .line 667
    invoke-virtual {p1, v1}, Landroid/view/ViewGroup;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v8}, Landroid/view/View;->setVisibility(I)V

    .line 642
    :cond_4
    :goto_2
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 652
    :cond_5
    invoke-virtual {p0}, Lcom/osp/app/util/BasePreferenceActivity;->a()Z

    move-result v0

    if-eqz v0, :cond_6

    .line 654
    invoke-virtual {p1, v1}, Landroid/view/ViewGroup;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    const v4, 0x7f02012e

    invoke-virtual {v0, v4}, Landroid/view/View;->setBackgroundResource(I)V

    goto :goto_1

    .line 657
    :cond_6
    invoke-virtual {p1, v1}, Landroid/view/ViewGroup;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    const v4, 0x7f02012f

    invoke-virtual {v0, v4}, Landroid/view/View;->setBackgroundResource(I)V

    goto :goto_1

    .line 670
    :cond_7
    invoke-virtual {p0}, Lcom/osp/app/util/BasePreferenceActivity;->a()Z

    move-result v0

    if-eqz v0, :cond_8

    .line 674
    const v0, 0x7f070024

    .line 682
    :goto_3
    :try_start_0
    invoke-static {p0}, Lcom/osp/app/util/r;->l(Landroid/content/Context;)Z

    move-result v4

    if-nez v4, :cond_9

    .line 684
    invoke-virtual {p1, v1}, Landroid/view/ViewGroup;->getChildAt(I)Landroid/view/View;

    move-result-object v4

    invoke-virtual {v4, v0}, Landroid/view/View;->setBackgroundResource(I)V

    .line 695
    :goto_4
    invoke-virtual {p1, v1}, Landroid/view/ViewGroup;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    const/4 v4, 0x0

    invoke-virtual {v0, v4}, Landroid/view/View;->setVisibility(I)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_2

    .line 698
    :catch_0
    move-exception v0

    invoke-virtual {p1, v1}, Landroid/view/ViewGroup;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v8}, Landroid/view/View;->setVisibility(I)V

    goto :goto_2

    .line 677
    :cond_8
    const v0, 0x7f070025

    goto :goto_3

    .line 687
    :cond_9
    :try_start_1
    invoke-virtual {p1, v1}, Landroid/view/ViewGroup;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    .line 688
    invoke-virtual {v0}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v4

    .line 689
    invoke-virtual {p0}, Lcom/osp/app/util/BasePreferenceActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    const v6, 0x7f08011f

    invoke-virtual {v5, v6}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v5

    iput v5, v4, Landroid/view/ViewGroup$LayoutParams;->width:I

    .line 690
    iget v5, v4, Landroid/view/ViewGroup$LayoutParams;->width:I

    invoke-virtual {p0}, Lcom/osp/app/util/BasePreferenceActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v6

    const v7, 0x7f08011e

    invoke-virtual {v6, v7}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v6

    sub-int/2addr v5, v6

    iput v5, v4, Landroid/view/ViewGroup$LayoutParams;->width:I

    .line 691
    invoke-virtual {v0, v4}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 693
    invoke-virtual {p1, v1}, Landroid/view/ViewGroup;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    const v4, 0x106000d

    invoke-virtual {v0, v4}, Landroid/view/View;->setBackgroundResource(I)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_4

    .line 706
    :cond_a
    invoke-virtual {p1}, Landroid/view/ViewGroup;->getChildCount()I

    move-result v1

    .line 707
    :goto_5
    if-ge v0, v1, :cond_0

    .line 709
    invoke-virtual {p1, v0}, Landroid/view/ViewGroup;->getChildAt(I)Landroid/view/View;

    move-result-object v2

    invoke-virtual {v2}, Landroid/view/View;->getId()I

    move-result v2

    if-ne v2, v11, :cond_b

    .line 711
    invoke-virtual {p1, v0}, Landroid/view/ViewGroup;->getChildAt(I)Landroid/view/View;

    move-result-object v2

    invoke-virtual {v2, v9}, Landroid/view/View;->setBackgroundResource(I)V

    .line 714
    :cond_b
    invoke-virtual {p1, v0}, Landroid/view/ViewGroup;->getChildAt(I)Landroid/view/View;

    move-result-object v2

    invoke-virtual {v2}, Landroid/view/View;->getId()I

    move-result v2

    if-eq v2, v10, :cond_c

    invoke-virtual {p1, v0}, Landroid/view/ViewGroup;->getChildAt(I)Landroid/view/View;

    move-result-object v2

    invoke-virtual {v2}, Landroid/view/View;->getId()I

    move-result v2

    const v3, 0x7f0c0162

    if-ne v2, v3, :cond_d

    .line 716
    :cond_c
    invoke-virtual {p1, v0}, Landroid/view/ViewGroup;->getChildAt(I)Landroid/view/View;

    move-result-object v2

    invoke-virtual {v2, v8}, Landroid/view/View;->setVisibility(I)V

    .line 707
    :cond_d
    add-int/lit8 v0, v0, 0x1

    goto :goto_5
.end method

.method protected final a()Z
    .locals 4

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x1

    .line 340
    iget-boolean v0, p0, Lcom/osp/app/util/BasePreferenceActivity;->j:Z

    if-nez v0, :cond_0

    .line 342
    iget-object v0, p0, Lcom/osp/app/util/BasePreferenceActivity;->h:Lcom/osp/app/util/am;

    const-string v3, "com.android.settings"

    invoke-virtual {v0, v3}, Lcom/osp/app/util/am;->a(Ljava/lang/String;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/osp/app/util/BasePreferenceActivity;->i:Z

    .line 343
    iput-boolean v1, p0, Lcom/osp/app/util/BasePreferenceActivity;->j:Z

    .line 345
    sget-object v0, Lcom/osp/app/util/h;->a:Lcom/osp/app/util/g;

    iget-boolean v0, p0, Lcom/osp/app/util/BasePreferenceActivity;->i:Z

    if-nez v0, :cond_1

    move v0, v1

    :goto_0
    invoke-static {v0}, Lcom/osp/app/util/g;->a(Z)Z

    move-result v0

    if-nez v0, :cond_2

    move v0, v1

    :goto_1
    iput-boolean v0, p0, Lcom/osp/app/util/BasePreferenceActivity;->i:Z

    .line 348
    :cond_0
    iget-boolean v0, p0, Lcom/osp/app/util/BasePreferenceActivity;->i:Z

    if-nez v0, :cond_3

    :goto_2
    return v1

    :cond_1
    move v0, v2

    .line 345
    goto :goto_0

    :cond_2
    move v0, v2

    goto :goto_1

    :cond_3
    move v1, v2

    .line 348
    goto :goto_2
.end method

.method protected final b()V
    .locals 2

    .prologue
    .line 413
    iget-object v0, p0, Lcom/osp/app/util/BasePreferenceActivity;->h:Lcom/osp/app/util/am;

    const/16 v1, 0xd

    invoke-virtual {v0, v1}, Lcom/osp/app/util/am;->a(I)Z

    .line 414
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "["

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "] setTheme=["

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/osp/app/util/BasePreferenceActivity;->h:Lcom/osp/app/util/am;

    iget-object v1, p0, Lcom/osp/app/util/BasePreferenceActivity;->h:Lcom/osp/app/util/am;

    iget v1, v1, Lcom/osp/app/util/am;->a:I

    invoke-static {v1}, Lcom/osp/app/util/am;->c(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " ]"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/osp/app/util/aq;->c(Ljava/lang/String;)V

    .line 415
    return-void
.end method

.method protected final c()V
    .locals 2

    .prologue
    .line 499
    iget-object v0, p0, Lcom/osp/app/util/BasePreferenceActivity;->h:Lcom/osp/app/util/am;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Lcom/osp/app/util/am;->a(I)Z

    .line 500
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "["

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "] setTheme=["

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/osp/app/util/BasePreferenceActivity;->h:Lcom/osp/app/util/am;

    iget-object v1, p0, Lcom/osp/app/util/BasePreferenceActivity;->h:Lcom/osp/app/util/am;

    iget v1, v1, Lcom/osp/app/util/am;->a:I

    invoke-static {v1}, Lcom/osp/app/util/am;->c(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " ]"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/osp/app/util/aq;->c(Ljava/lang/String;)V

    .line 501
    return-void
.end method

.method public onBackPressed()V
    .locals 1

    .prologue
    .line 607
    :try_start_0
    invoke-super {p0}, Landroid/preference/PreferenceActivity;->onBackPressed()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 614
    :goto_0
    return-void

    .line 608
    :catch_0
    move-exception v0

    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    .line 612
    invoke-virtual {p0}, Lcom/osp/app/util/BasePreferenceActivity;->finish()V

    goto :goto_0
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 354
    sget-object v0, Lcom/osp/app/util/h;->a:Lcom/osp/app/util/g;

    invoke-static {p0}, Lcom/osp/app/util/g;->a(Landroid/content/Context;)V

    .line 357
    iget-object v0, p0, Lcom/osp/app/util/BasePreferenceActivity;->h:Lcom/osp/app/util/am;

    iget v0, v0, Lcom/osp/app/util/am;->a:I

    const/4 v1, -0x1

    if-ne v0, v1, :cond_0

    .line 368
    invoke-virtual {p0}, Lcom/osp/app/util/BasePreferenceActivity;->a()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 370
    iget-object v0, p0, Lcom/osp/app/util/BasePreferenceActivity;->h:Lcom/osp/app/util/am;

    const/16 v1, 0xd

    invoke-virtual {v0, v1}, Lcom/osp/app/util/am;->a(I)Z

    .line 371
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "["

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "] setTheme=["

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/osp/app/util/BasePreferenceActivity;->h:Lcom/osp/app/util/am;

    iget-object v1, p0, Lcom/osp/app/util/BasePreferenceActivity;->h:Lcom/osp/app/util/am;

    iget v1, v1, Lcom/osp/app/util/am;->a:I

    invoke-static {v1}, Lcom/osp/app/util/am;->c(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " ]"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/osp/app/util/aq;->c(Ljava/lang/String;)V

    .line 379
    :cond_0
    :goto_0
    invoke-static {}, Lcom/osp/app/signin/SamsungService;->a()Z

    move-result v0

    if-nez v0, :cond_1

    .line 381
    invoke-static {p0}, Lcom/osp/app/util/r;->k(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 383
    invoke-virtual {p0}, Lcom/osp/app/util/BasePreferenceActivity;->getWindow()Landroid/view/Window;

    move-result-object v0

    const/16 v1, 0x400

    invoke-virtual {v0, v1}, Landroid/view/Window;->addFlags(I)V

    .line 390
    :cond_1
    :goto_1
    invoke-super {p0, p1}, Landroid/preference/PreferenceActivity;->onCreate(Landroid/os/Bundle;)V

    .line 391
    return-void

    .line 374
    :cond_2
    iget-object v0, p0, Lcom/osp/app/util/BasePreferenceActivity;->h:Lcom/osp/app/util/am;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Lcom/osp/app/util/am;->a(I)Z

    .line 375
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "["

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "] setTheme=["

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/osp/app/util/BasePreferenceActivity;->h:Lcom/osp/app/util/am;

    iget-object v1, p0, Lcom/osp/app/util/BasePreferenceActivity;->h:Lcom/osp/app/util/am;

    iget v1, v1, Lcom/osp/app/util/am;->a:I

    invoke-static {v1}, Lcom/osp/app/util/am;->c(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " ]"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/osp/app/util/aq;->c(Ljava/lang/String;)V

    goto :goto_0

    .line 384
    :cond_3
    invoke-static {p0}, Lcom/osp/app/util/r;->m(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 386
    invoke-static {p0}, Lcom/msc/sa/c/d;->d(Landroid/app/Activity;)V

    goto :goto_1
.end method

.method public onFocusChange(Landroid/view/View;Z)V
    .locals 0
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 558
    return-void
.end method

.method protected onRestoreInstanceState(Landroid/os/Bundle;)V
    .locals 1

    .prologue
    .line 397
    :try_start_0
    invoke-super {p0, p1}, Landroid/preference/PreferenceActivity;->onRestoreInstanceState(Landroid/os/Bundle;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 402
    :goto_0
    return-void

    .line 398
    :catch_0
    move-exception v0

    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_0
.end method

.method protected onResume()V
    .locals 0

    .prologue
    .line 406
    invoke-super {p0}, Landroid/preference/PreferenceActivity;->onResume()V

    .line 407
    return-void
.end method

.method public sendBroadcast(Landroid/content/Intent;)V
    .locals 3

    .prologue
    .line 562
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    invoke-static {p1}, Lcom/osp/app/util/aq;->a(Landroid/content/Intent;)Ljava/lang/String;

    move-result-object v0

    .line 563
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "["

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "] sendBroadcast Intent=["

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " ]"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/osp/app/util/aq;->c(Ljava/lang/String;)V

    .line 564
    invoke-super {p0, p1}, Landroid/preference/PreferenceActivity;->sendBroadcast(Landroid/content/Intent;)V

    .line 565
    return-void
.end method

.method public sendBroadcast(Landroid/content/Intent;Ljava/lang/String;)V
    .locals 3

    .prologue
    .line 569
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    invoke-static {p1}, Lcom/osp/app/util/aq;->a(Landroid/content/Intent;)Ljava/lang/String;

    move-result-object v0

    .line 570
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "["

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "] sendBroadcast Intent=["

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " ]"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/osp/app/util/aq;->c(Ljava/lang/String;)V

    .line 571
    invoke-super {p0, p1, p2}, Landroid/preference/PreferenceActivity;->sendBroadcast(Landroid/content/Intent;Ljava/lang/String;)V

    .line 572
    return-void
.end method

.method public startService(Landroid/content/Intent;)Landroid/content/ComponentName;
    .locals 3

    .prologue
    .line 598
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    invoke-static {p1}, Lcom/osp/app/util/aq;->a(Landroid/content/Intent;)Ljava/lang/String;

    move-result-object v0

    .line 599
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "["

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "] startService Intent=["

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " ]"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/osp/app/util/aq;->c(Ljava/lang/String;)V

    .line 600
    invoke-super {p0, p1}, Landroid/preference/PreferenceActivity;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;

    move-result-object v0

    return-object v0
.end method
