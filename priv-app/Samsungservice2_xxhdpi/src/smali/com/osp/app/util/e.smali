.class public final Lcom/osp/app/util/e;
.super Ljava/lang/Object;
.source "BroadcastInterfaceManager.java"


# static fields
.field private static a:Lcom/osp/app/util/e;


# instance fields
.field private final b:Ljava/util/HashMap;

.field private final c:Ljava/util/HashMap;

.field private d:J

.field private e:J


# direct methods
.method private constructor <init>()V
    .locals 2

    .prologue
    const-wide/16 v0, 0x0

    .line 26
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 23
    iput-wide v0, p0, Lcom/osp/app/util/e;->d:J

    .line 24
    iput-wide v0, p0, Lcom/osp/app/util/e;->e:J

    .line 28
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/osp/app/util/e;->b:Ljava/util/HashMap;

    .line 29
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/osp/app/util/e;->c:Ljava/util/HashMap;

    .line 31
    return-void
.end method

.method public static a()Lcom/osp/app/util/e;
    .locals 1

    .prologue
    .line 39
    sget-object v0, Lcom/osp/app/util/e;->a:Lcom/osp/app/util/e;

    if-eqz v0, :cond_0

    .line 41
    sget-object v0, Lcom/osp/app/util/e;->a:Lcom/osp/app/util/e;

    .line 44
    :goto_0
    return-object v0

    .line 43
    :cond_0
    new-instance v0, Lcom/osp/app/util/e;

    invoke-direct {v0}, Lcom/osp/app/util/e;-><init>()V

    .line 44
    sput-object v0, Lcom/osp/app/util/e;->a:Lcom/osp/app/util/e;

    goto :goto_0
.end method


# virtual methods
.method public final a(Landroid/content/Intent;)J
    .locals 8

    .prologue
    const-wide/16 v6, 0x1

    .line 56
    const-wide/16 v0, -0x1

    .line 58
    new-instance v2, Lcom/osp/app/util/f;

    invoke-direct {v2}, Lcom/osp/app/util/f;-><init>()V

    .line 60
    invoke-virtual {p1}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/osp/app/util/f;->a(Lcom/osp/app/util/f;Ljava/lang/String;)Ljava/lang/String;

    .line 61
    const-string v3, "mypackage"

    invoke-virtual {p1, v3}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/osp/app/util/f;->b(Lcom/osp/app/util/f;Ljava/lang/String;)Ljava/lang/String;

    .line 63
    const-string v3, "com.msc.action.ACCESSTOKEN_V02_REQUEST"

    invoke-static {v2}, Lcom/osp/app/util/f;->a(Lcom/osp/app/util/f;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 65
    iget-object v0, p0, Lcom/osp/app/util/e;->c:Ljava/util/HashMap;

    iget-wide v4, p0, Lcom/osp/app/util/e;->e:J

    add-long/2addr v4, v6

    iput-wide v4, p0, Lcom/osp/app/util/e;->e:J

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 66
    iget-wide v0, p0, Lcom/osp/app/util/e;->e:J

    .line 67
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v2, "BIM"

    const-string v3, "Register broadcast ATV02"

    invoke-static {v2, v3}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 91
    :goto_0
    return-wide v0

    .line 69
    :cond_0
    const-string v3, "com.msc.action.samsungaccount.request.BackGroundSignin"

    invoke-static {v2}, Lcom/osp/app/util/f;->a(Lcom/osp/app/util/f;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_2

    .line 72
    const-string v0, "REQUEST_ACCESSTOKEN"

    const-string v1, "account_mode"

    invoke-virtual {p1, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 74
    invoke-static {v2}, Lcom/osp/app/util/f;->b(Lcom/osp/app/util/f;)Z

    .line 77
    const-string v0, "request_code"

    const/4 v1, 0x0

    invoke-virtual {p1, v0, v1}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v0

    invoke-static {v2, v0}, Lcom/osp/app/util/f;->a(Lcom/osp/app/util/f;I)I

    .line 80
    :cond_1
    iget-object v0, p0, Lcom/osp/app/util/e;->b:Ljava/util/HashMap;

    iget-wide v4, p0, Lcom/osp/app/util/e;->d:J

    add-long/2addr v4, v6

    iput-wide v4, p0, Lcom/osp/app/util/e;->d:J

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 81
    iget-wide v0, p0, Lcom/osp/app/util/e;->d:J

    .line 82
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v2, "BIM"

    const-string v3, "Register broadcast BSI"

    invoke-static {v2, v3}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 86
    :cond_2
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v2, "BIM : No support this broadcast"

    invoke-static {v2}, Lcom/osp/app/util/aq;->b(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public final a(Landroid/content/Context;)V
    .locals 9

    .prologue
    .line 216
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v0, "BIM"

    const-string v1, "sendRemainResponse"

    invoke-static {v0, v1}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 222
    iget-object v0, p0, Lcom/osp/app/util/e;->b:Ljava/util/HashMap;

    invoke-virtual {v0}, Ljava/util/HashMap;->keySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->toArray()[Ljava/lang/Object;

    move-result-object v2

    .line 224
    array-length v3, v2

    const/4 v0, 0x0

    move v1, v0

    :goto_0
    if-ge v1, v3, :cond_2

    aget-object v4, v2, v1

    .line 226
    iget-object v0, p0, Lcom/osp/app/util/e;->b:Ljava/util/HashMap;

    invoke-virtual {v0, v4}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/osp/app/util/f;

    .line 227
    invoke-static {v0}, Lcom/osp/app/util/f;->a(Lcom/osp/app/util/f;)Ljava/lang/String;

    move-result-object v5

    .line 228
    invoke-static {v0}, Lcom/osp/app/util/f;->c(Lcom/osp/app/util/f;)Ljava/lang/String;

    move-result-object v6

    .line 230
    const-string v7, "com.msc.action.samsungaccount.request.BackGroundSignin"

    invoke-virtual {v7, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_1

    .line 232
    new-instance v5, Landroid/content/Intent;

    const-string v7, "com.msc.action.samsungaccount.response.BackGroundSignin"

    invoke-direct {v5, v7}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 233
    const-string v7, "bg_result"

    const/4 v8, 0x1

    invoke-virtual {v5, v7, v8}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 235
    if-eqz v6, :cond_1

    .line 238
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    new-instance v7, Ljava/lang/StringBuilder;

    const-string v8, "BIMBackgroundSignIn Return Fail : "

    invoke-direct {v7, v8}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v7, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v7}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;)V

    .line 240
    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v7, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, ":"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v7}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v7

    invoke-virtual {v5, v7}, Landroid/content/Intent;->setData(Landroid/net/Uri;)Landroid/content/Intent;

    .line 241
    invoke-virtual {v5, v6}, Landroid/content/Intent;->setPackage(Ljava/lang/String;)Landroid/content/Intent;

    .line 243
    invoke-static {v0}, Lcom/osp/app/util/f;->d(Lcom/osp/app/util/f;)Z

    move-result v6

    if-eqz v6, :cond_0

    .line 247
    const-string v6, "request_code"

    invoke-static {v0}, Lcom/osp/app/util/f;->e(Lcom/osp/app/util/f;)I

    move-result v0

    invoke-virtual {v5, v6, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 249
    :cond_0
    invoke-virtual {p1, v5}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V

    .line 251
    iget-object v0, p0, Lcom/osp/app/util/e;->b:Ljava/util/HashMap;

    invoke-virtual {v0, v4}, Ljava/util/HashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 224
    :cond_1
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 255
    :cond_2
    return-void
.end method

.method public final a(Landroid/content/Context;Landroid/content/Intent;J)V
    .locals 5

    .prologue
    .line 102
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v0, "BIM"

    const-string v1, "sendResponseBroadcast"

    invoke-static {v0, v1}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 104
    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v1

    .line 106
    const-string v0, "com.msc.action.ACCESSTOKEN_V02_RESPONSE"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 110
    iget-object v0, p0, Lcom/osp/app/util/e;->c:Ljava/util/HashMap;

    invoke-static {p3, p4}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/osp/app/util/f;

    .line 116
    :goto_0
    if-eqz v0, :cond_0

    .line 118
    invoke-static {v0}, Lcom/osp/app/util/f;->a(Lcom/osp/app/util/f;)Ljava/lang/String;

    move-result-object v2

    .line 119
    invoke-static {v0}, Lcom/osp/app/util/f;->c(Lcom/osp/app/util/f;)Ljava/lang/String;

    move-result-object v0

    .line 121
    invoke-virtual {p2}, Landroid/content/Intent;->getPackage()Ljava/lang/String;

    move-result-object v3

    .line 123
    const-string v4, "com.msc.action.ACCESSTOKEN_V02_REQUEST"

    invoke-virtual {v4, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_2

    const-string v4, "com.msc.action.ACCESSTOKEN_V02_RESPONSE"

    invoke-virtual {v4, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_2

    if-eqz v0, :cond_2

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_2

    .line 131
    invoke-virtual {p1, p2}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V

    .line 132
    iget-object v0, p0, Lcom/osp/app/util/e;->c:Ljava/util/HashMap;

    invoke-static {p3, p4}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 148
    :cond_0
    :goto_1
    return-void

    .line 113
    :cond_1
    iget-object v0, p0, Lcom/osp/app/util/e;->b:Ljava/util/HashMap;

    invoke-static {p3, p4}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/osp/app/util/f;

    goto :goto_0

    .line 134
    :cond_2
    const-string v4, "com.msc.action.samsungaccount.request.BackGroundSignin"

    invoke-virtual {v4, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    const-string v2, "com.msc.action.samsungaccount.response.BackGroundSignin"

    invoke-virtual {v2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    if-eqz v0, :cond_0

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 143
    invoke-virtual {p1, p2}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V

    .line 144
    iget-object v0, p0, Lcom/osp/app/util/e;->b:Ljava/util/HashMap;

    invoke-static {p3, p4}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_1
.end method

.method public final a(Landroid/content/Context;Ljava/lang/String;)V
    .locals 8

    .prologue
    .line 173
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v0, "BIM"

    const-string v1, "sendRemainResponseV02"

    invoke-static {v0, v1}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 179
    iget-object v0, p0, Lcom/osp/app/util/e;->c:Ljava/util/HashMap;

    invoke-virtual {v0}, Ljava/util/HashMap;->keySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->toArray()[Ljava/lang/Object;

    move-result-object v2

    .line 181
    array-length v3, v2

    const/4 v0, 0x0

    move v1, v0

    :goto_0
    if-ge v1, v3, :cond_1

    aget-object v4, v2, v1

    .line 183
    iget-object v0, p0, Lcom/osp/app/util/e;->c:Ljava/util/HashMap;

    invoke-virtual {v0, v4}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/osp/app/util/f;

    .line 184
    invoke-static {v0}, Lcom/osp/app/util/f;->a(Lcom/osp/app/util/f;)Ljava/lang/String;

    move-result-object v5

    .line 185
    invoke-static {v0}, Lcom/osp/app/util/f;->c(Lcom/osp/app/util/f;)Ljava/lang/String;

    move-result-object v0

    .line 187
    const-string v6, "com.msc.action.ACCESSTOKEN_V02_REQUEST"

    invoke-virtual {v6, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_0

    .line 190
    new-instance v5, Landroid/content/Intent;

    const-string v6, "com.msc.action.ACCESSTOKEN_V02_RESPONSE"

    invoke-direct {v5, v6}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 191
    const-string v6, "result_code"

    const/4 v7, 0x1

    invoke-virtual {v5, v6, v7}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 192
    const-string v6, "error_message"

    invoke-virtual {v5, v6, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 194
    if-eqz v0, :cond_0

    .line 196
    invoke-virtual {v5, v0}, Landroid/content/Intent;->setPackage(Ljava/lang/String;)Landroid/content/Intent;

    .line 198
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v6, "BIM V02 Return Fail : "

    invoke-direct {v0, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;)V

    .line 200
    invoke-virtual {p1, v5}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V

    .line 202
    iget-object v0, p0, Lcom/osp/app/util/e;->c:Ljava/util/HashMap;

    invoke-virtual {v0, v4}, Ljava/util/HashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 181
    :cond_0
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 207
    :cond_1
    return-void
.end method
