.class public final Lcom/osp/app/util/m;
.super Ljava/lang/Object;
.source "EasySignupUtil.java"


# static fields
.field private static a:Lcom/osp/app/util/m;


# direct methods
.method private constructor <init>()V
    .locals 0

    .prologue
    .line 149
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 150
    return-void
.end method

.method public static a()Lcom/osp/app/util/m;
    .locals 1

    .prologue
    .line 158
    sget-object v0, Lcom/osp/app/util/m;->a:Lcom/osp/app/util/m;

    if-eqz v0, :cond_0

    .line 160
    sget-object v0, Lcom/osp/app/util/m;->a:Lcom/osp/app/util/m;

    .line 163
    :goto_0
    return-object v0

    .line 162
    :cond_0
    new-instance v0, Lcom/osp/app/util/m;

    invoke-direct {v0}, Lcom/osp/app/util/m;-><init>()V

    .line 163
    sput-object v0, Lcom/osp/app/util/m;->a:Lcom/osp/app/util/m;

    goto :goto_0
.end method

.method public static a(Landroid/app/Activity;Z)V
    .locals 3

    .prologue
    .line 210
    new-instance v0, Landroid/content/Intent;

    const-string v1, "com.samsung.android.coreapps.easysignup.ACTION_REQ_AUTH"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 211
    const/16 v1, 0x20

    invoke-virtual {v0, v1}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    .line 212
    const-string v1, "AuthRequestFrom"

    const-string v2, "samsungAccount"

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 213
    const-string v1, "agreeMarketing"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 214
    const/16 v1, 0xee

    invoke-virtual {p0, v0, v1}, Landroid/app/Activity;->startActivityForResult(Landroid/content/Intent;I)V

    .line 215
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v0, "ESUU"

    const-string v1, "requestActivityForReqAuth start"

    invoke-static {v0, v1}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 216
    return-void
.end method

.method public static a(Landroid/content/Context;)Z
    .locals 3

    .prologue
    .line 175
    :try_start_0
    invoke-virtual {p0}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v0

    .line 176
    const-string v1, "com.samsung.android.coreapps"

    const/16 v2, 0x80

    invoke-virtual {v0, v1, v2}, Landroid/content/pm/PackageManager;->getPackageInfo(Ljava/lang/String;I)Landroid/content/pm/PackageInfo;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 183
    const/4 v0, 0x1

    :goto_0
    return v0

    .line 180
    :catch_0
    move-exception v0

    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v0, "ESUU"

    const-string v1, "not support easySignup"

    invoke-static {v0, v1}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 181
    const/4 v0, 0x0

    goto :goto_0
.end method
