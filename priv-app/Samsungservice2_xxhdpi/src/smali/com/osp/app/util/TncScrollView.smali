.class public Lcom/osp/app/util/TncScrollView;
.super Landroid/widget/ScrollView;
.source "TncScrollView.java"


# instance fields
.field private a:F

.field private b:I

.field private c:I

.field private d:I


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0

    .prologue
    .line 26
    invoke-direct {p0, p1, p2}, Landroid/widget/ScrollView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 27
    return-void
.end method


# virtual methods
.method protected onConfigurationChanged(Landroid/content/res/Configuration;)V
    .locals 2

    .prologue
    .line 94
    invoke-super {p0, p1}, Landroid/widget/ScrollView;->onConfigurationChanged(Landroid/content/res/Configuration;)V

    .line 95
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v0, "TncScrollView"

    const-string v1, "onConfigurationChanged"

    invoke-static {v0, v1}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 97
    new-instance v0, Lcom/osp/app/util/ao;

    invoke-direct {v0, p0, p1}, Lcom/osp/app/util/ao;-><init>(Lcom/osp/app/util/TncScrollView;Landroid/content/res/Configuration;)V

    invoke-virtual {p0, v0}, Lcom/osp/app/util/TncScrollView;->post(Ljava/lang/Runnable;)Z

    .line 117
    return-void
.end method

.method protected onScrollChanged(IIII)V
    .locals 6

    .prologue
    const/4 v5, 0x2

    const/4 v4, 0x1

    .line 31
    invoke-super {p0, p1, p2, p3, p4}, Landroid/widget/ScrollView;->onScrollChanged(IIII)V

    .line 33
    invoke-virtual {p0}, Lcom/osp/app/util/TncScrollView;->computeVerticalScrollRange()I

    move-result v0

    .line 34
    invoke-virtual {p0}, Lcom/osp/app/util/TncScrollView;->getScrollY()I

    move-result v1

    .line 36
    if-lez v0, :cond_3

    .line 38
    invoke-virtual {p0}, Lcom/osp/app/util/TncScrollView;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v2

    iget v2, v2, Landroid/content/res/Configuration;->orientation:I

    .line 40
    iget v3, p0, Lcom/osp/app/util/TncScrollView;->d:I

    if-gtz v3, :cond_0

    .line 42
    iput v0, p0, Lcom/osp/app/util/TncScrollView;->d:I

    .line 44
    :cond_0
    iget v3, p0, Lcom/osp/app/util/TncScrollView;->d:I

    if-ne v3, v0, :cond_5

    .line 46
    iget v3, p0, Lcom/osp/app/util/TncScrollView;->b:I

    if-gtz v3, :cond_4

    if-ne v2, v4, :cond_4

    .line 48
    iput v0, p0, Lcom/osp/app/util/TncScrollView;->b:I

    .line 54
    :cond_1
    :goto_0
    iget v2, p0, Lcom/osp/app/util/TncScrollView;->b:I

    if-eq v2, v0, :cond_2

    iget v2, p0, Lcom/osp/app/util/TncScrollView;->c:I

    if-ne v2, v0, :cond_3

    .line 57
    :cond_2
    int-to-float v1, v1

    int-to-float v0, v0

    div-float v0, v1, v0

    iput v0, p0, Lcom/osp/app/util/TncScrollView;->a:F

    .line 73
    :cond_3
    :goto_1
    return-void

    .line 49
    :cond_4
    iget v3, p0, Lcom/osp/app/util/TncScrollView;->c:I

    if-gtz v3, :cond_1

    if-ne v2, v5, :cond_1

    .line 51
    iput v0, p0, Lcom/osp/app/util/TncScrollView;->c:I

    goto :goto_0

    .line 61
    :cond_5
    if-ne v2, v4, :cond_6

    iget v3, p0, Lcom/osp/app/util/TncScrollView;->d:I

    if-lt v3, v0, :cond_7

    :cond_6
    if-ne v2, v5, :cond_3

    iget v2, p0, Lcom/osp/app/util/TncScrollView;->d:I

    if-le v2, v0, :cond_3

    .line 64
    :cond_7
    iput v0, p0, Lcom/osp/app/util/TncScrollView;->d:I

    .line 66
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v2, "TncScrollView"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "onScrollChanged - height:"

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ", scrollY:"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v3, ", ratio:"

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v3, p0, Lcom/osp/app/util/TncScrollView;->a:F

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v2, v1}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 68
    if-lez v0, :cond_3

    int-to-float v1, v0

    iget v2, p0, Lcom/osp/app/util/TncScrollView;->a:F

    mul-float/2addr v1, v2

    const/high16 v2, 0x3f000000    # 0.5f

    add-float/2addr v1, v2

    float-to-int v1, v1

    const/4 v2, 0x0

    invoke-virtual {p0, v2, v1}, Lcom/osp/app/util/TncScrollView;->scrollTo(II)V

    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v2, "TncScrollView"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "adjustScrollY - height:"

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v3, ", scrollY:"

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", ratio:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lcom/osp/app/util/TncScrollView;->a:F

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v2, v0}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1
.end method
