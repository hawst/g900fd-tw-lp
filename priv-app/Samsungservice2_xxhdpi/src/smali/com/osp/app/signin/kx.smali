.class final Lcom/osp/app/signin/kx;
.super Ljava/lang/Object;
.source "SignUpView.java"

# interfaces
.implements Landroid/text/TextWatcher;


# instance fields
.field final synthetic a:Landroid/widget/Button;

.field final synthetic b:Lcom/osp/app/signin/SignUpView;


# direct methods
.method constructor <init>(Lcom/osp/app/signin/SignUpView;Landroid/widget/Button;)V
    .locals 0

    .prologue
    .line 2449
    iput-object p1, p0, Lcom/osp/app/signin/kx;->b:Lcom/osp/app/signin/SignUpView;

    iput-object p2, p0, Lcom/osp/app/signin/kx;->a:Landroid/widget/Button;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final afterTextChanged(Landroid/text/Editable;)V
    .locals 0

    .prologue
    .line 2453
    return-void
.end method

.method public final beforeTextChanged(Ljava/lang/CharSequence;III)V
    .locals 0

    .prologue
    .line 2457
    return-void
.end method

.method public final onTextChanged(Ljava/lang/CharSequence;III)V
    .locals 3

    .prologue
    .line 2461
    iget-object v0, p0, Lcom/osp/app/signin/kx;->b:Lcom/osp/app/signin/SignUpView;

    invoke-static {v0}, Lcom/osp/app/signin/SignUpView;->v(Lcom/osp/app/signin/SignUpView;)Lcom/msc/a/j;

    move-result-object v0

    if-nez v0, :cond_0

    .line 2463
    iget-object v0, p0, Lcom/osp/app/signin/kx;->b:Lcom/osp/app/signin/SignUpView;

    new-instance v1, Lcom/msc/a/j;

    invoke-direct {v1}, Lcom/msc/a/j;-><init>()V

    invoke-static {v0, v1}, Lcom/osp/app/signin/SignUpView;->a(Lcom/osp/app/signin/SignUpView;Lcom/msc/a/j;)Lcom/msc/a/j;

    .line 2465
    :cond_0
    iget-object v0, p0, Lcom/osp/app/signin/kx;->b:Lcom/osp/app/signin/SignUpView;

    invoke-static {v0}, Lcom/osp/app/signin/SignUpView;->v(Lcom/osp/app/signin/SignUpView;)Lcom/msc/a/j;

    move-result-object v0

    invoke-static {}, Lcom/osp/app/util/y;->a()Lcom/osp/app/util/y;

    move-result-object v1

    invoke-virtual {v1}, Lcom/osp/app/util/y;->c()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/msc/a/j;->a(Ljava/lang/String;)V

    .line 2466
    iget-object v0, p0, Lcom/osp/app/signin/kx;->b:Lcom/osp/app/signin/SignUpView;

    invoke-static {v0}, Lcom/osp/app/signin/SignUpView;->v(Lcom/osp/app/signin/SignUpView;)Lcom/msc/a/j;

    move-result-object v0

    iget-object v1, p0, Lcom/osp/app/signin/kx;->a:Landroid/widget/Button;

    invoke-virtual {v1}, Landroid/widget/Button;->getText()Ljava/lang/CharSequence;

    move-result-object v1

    invoke-interface {v1}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/msc/a/j;->b(Ljava/lang/String;)V

    .line 2468
    iget-object v0, p0, Lcom/osp/app/signin/kx;->b:Lcom/osp/app/signin/SignUpView;

    invoke-static {v0}, Lcom/osp/app/signin/SignUpView;->w(Lcom/osp/app/signin/SignUpView;)Landroid/widget/EditText;

    move-result-object v0

    if-nez v0, :cond_1

    .line 2470
    iget-object v1, p0, Lcom/osp/app/signin/kx;->b:Lcom/osp/app/signin/SignUpView;

    iget-object v0, p0, Lcom/osp/app/signin/kx;->b:Lcom/osp/app/signin/SignUpView;

    const v2, 0x7f0c0150

    invoke-virtual {v0, v2}, Lcom/osp/app/signin/SignUpView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/EditText;

    invoke-static {v1, v0}, Lcom/osp/app/signin/SignUpView;->a(Lcom/osp/app/signin/SignUpView;Landroid/widget/EditText;)Landroid/widget/EditText;

    .line 2472
    :cond_1
    iget-object v0, p0, Lcom/osp/app/signin/kx;->b:Lcom/osp/app/signin/SignUpView;

    invoke-static {v0}, Lcom/osp/app/signin/SignUpView;->v(Lcom/osp/app/signin/SignUpView;)Lcom/msc/a/j;

    move-result-object v0

    invoke-virtual {v0}, Lcom/msc/a/j;->b()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 2474
    iget-object v0, p0, Lcom/osp/app/signin/kx;->b:Lcom/osp/app/signin/SignUpView;

    invoke-static {v0}, Lcom/osp/app/signin/SignUpView;->w(Lcom/osp/app/signin/SignUpView;)Landroid/widget/EditText;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setEnabled(Z)V

    .line 2479
    :goto_0
    iget-object v0, p0, Lcom/osp/app/signin/kx;->b:Lcom/osp/app/signin/SignUpView;

    invoke-static {v0}, Lcom/osp/app/signin/SignUpView;->w(Lcom/osp/app/signin/SignUpView;)Landroid/widget/EditText;

    move-result-object v0

    const-string v1, ""

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    .line 2481
    iget-object v0, p0, Lcom/osp/app/signin/kx;->b:Lcom/osp/app/signin/SignUpView;

    invoke-virtual {v0}, Lcom/osp/app/signin/SignUpView;->a()V

    .line 2482
    return-void

    .line 2477
    :cond_2
    iget-object v0, p0, Lcom/osp/app/signin/kx;->b:Lcom/osp/app/signin/SignUpView;

    invoke-static {v0}, Lcom/osp/app/signin/SignUpView;->w(Lcom/osp/app/signin/SignUpView;)Landroid/widget/EditText;

    move-result-object v0

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setEnabled(Z)V

    goto :goto_0
.end method
