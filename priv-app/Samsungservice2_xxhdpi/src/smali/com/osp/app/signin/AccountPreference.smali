.class public Lcom/osp/app/signin/AccountPreference;
.super Lcom/osp/app/util/BasePreferenceActivity;
.source "AccountPreference.java"


# instance fields
.field private a:Lcom/msc/a/g;

.field private b:Z

.field private c:Lcom/osp/app/signin/g;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 40
    invoke-direct {p0}, Lcom/osp/app/util/BasePreferenceActivity;-><init>()V

    .line 62
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/osp/app/signin/AccountPreference;->b:Z

    .line 359
    return-void
.end method

.method static synthetic a(Lcom/osp/app/signin/AccountPreference;)Lcom/msc/a/g;
    .locals 1

    .prologue
    .line 40
    iget-object v0, p0, Lcom/osp/app/signin/AccountPreference;->a:Lcom/msc/a/g;

    return-object v0
.end method

.method static synthetic a(Lcom/osp/app/signin/AccountPreference;Lcom/msc/a/g;)Lcom/msc/a/g;
    .locals 0

    .prologue
    .line 40
    iput-object p1, p0, Lcom/osp/app/signin/AccountPreference;->a:Lcom/msc/a/g;

    return-object p1
.end method

.method private a(I)V
    .locals 4

    .prologue
    const v3, 0x7f08011e

    .line 256
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "AccountView::initLayoutParams orientation : "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;)V

    .line 258
    invoke-static {}, Lcom/osp/device/b;->b()Lcom/osp/device/b;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/osp/device/b;->i(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 260
    invoke-virtual {p0}, Lcom/osp/app/signin/AccountPreference;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f08011f

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v0

    float-to-int v0, v0

    .line 261
    invoke-virtual {p0}, Lcom/osp/app/signin/AccountPreference;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1, v3}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v1

    float-to-int v1, v1

    .line 262
    invoke-static {}, Lcom/osp/app/signin/SamsungService;->a()Z

    move-result v2

    if-nez v2, :cond_0

    .line 264
    const v0, 0x7f0c015e

    invoke-virtual {p0, v0}, Lcom/osp/app/signin/AccountPreference;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    .line 265
    invoke-virtual {p0, v0, p1}, Lcom/osp/app/signin/AccountPreference;->a(Landroid/view/ViewGroup;I)V

    .line 267
    invoke-virtual {p0}, Lcom/osp/app/signin/AccountPreference;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0, v3}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v0

    float-to-int v0, v0

    .line 269
    :cond_0
    const v2, 0x7f0c0161

    invoke-virtual {p0, v2}, Lcom/osp/app/signin/AccountPreference;->findViewById(I)Landroid/view/View;

    move-result-object v2

    .line 270
    invoke-static {}, Lcom/osp/app/util/q;->a()Lcom/osp/app/util/q;

    invoke-static {v2, v0, v1, p1}, Lcom/osp/app/util/q;->b(Landroid/view/View;III)V

    .line 272
    :cond_1
    return-void
.end method

.method static synthetic a(Lcom/osp/app/signin/AccountPreference;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 40
    invoke-direct {p0, p1}, Lcom/osp/app/signin/AccountPreference;->a(Ljava/lang/String;)V

    return-void
.end method

.method private a(Ljava/lang/String;)V
    .locals 3

    .prologue
    .line 90
    new-instance v0, Landroid/content/Intent;

    const-string v1, "com.msc.action.samsungaccount.resignin"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 91
    const-string v1, "client_id"

    const-string v2, "j5p7ll8g33"

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 92
    const-string v1, "client_secret"

    const-string v2, "5763D0052DC1462E13751F753384E9A9"

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 93
    const-string v1, "OSP_VER"

    const-string v2, "OSP_02"

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 95
    if-eqz p1, :cond_0

    .line 97
    const-string v1, "email_id"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 106
    :cond_0
    invoke-virtual {p0, v0}, Lcom/osp/app/signin/AccountPreference;->startActivity(Landroid/content/Intent;)V

    .line 108
    return-void
.end method

.method static synthetic b(Lcom/osp/app/signin/AccountPreference;Ljava/lang/String;)V
    .locals 3

    .prologue
    .line 40
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/osp/app/signin/EmailValidationView;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const-string v1, "email_id"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    const-string v1, "is_resend"

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    const-string v1, "is_bgmode"

    const/16 v2, 0xca

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    const/16 v1, 0x64

    invoke-virtual {p0, v0, v1}, Lcom/osp/app/signin/AccountPreference;->startActivityForResult(Landroid/content/Intent;I)V

    return-void
.end method


# virtual methods
.method protected onActivityResult(IILandroid/content/Intent;)V
    .locals 1

    .prologue
    .line 120
    invoke-super {p0, p1, p2, p3}, Lcom/osp/app/util/BasePreferenceActivity;->onActivityResult(IILandroid/content/Intent;)V

    .line 121
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/osp/app/signin/AccountPreference;->b:Z

    .line 122
    const/16 v0, 0x64

    if-ne p1, v0, :cond_0

    .line 124
    const/16 v0, 0xc

    if-ne p2, v0, :cond_0

    .line 126
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/osp/app/signin/AccountPreference;->b:Z

    .line 129
    :cond_0
    return-void
.end method

.method public onBackPressed()V
    .locals 1

    .prologue
    .line 113
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/osp/app/signin/AccountPreference;->setResult(I)V

    .line 115
    invoke-super {p0}, Lcom/osp/app/util/BasePreferenceActivity;->onBackPressed()V

    .line 116
    return-void
.end method

.method public onConfigurationChanged(Landroid/content/res/Configuration;)V
    .locals 2

    .prologue
    .line 241
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v0, "AP"

    const-string v1, "onConfigurationChanged()"

    invoke-static {v0, v1}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 243
    iget v0, p1, Landroid/content/res/Configuration;->orientation:I

    invoke-direct {p0, v0}, Lcom/osp/app/signin/AccountPreference;->a(I)V

    .line 245
    invoke-super {p0, p1}, Lcom/osp/app/util/BasePreferenceActivity;->onConfigurationChanged(Landroid/content/res/Configuration;)V

    .line 246
    return-void
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 7

    .prologue
    const v6, 0x7f0800e7

    const v5, 0x7f070006

    const/4 v4, 0x1

    .line 137
    invoke-super {p0, p1}, Lcom/osp/app/util/BasePreferenceActivity;->onCreate(Landroid/os/Bundle;)V

    .line 139
    const/4 v0, 0x0

    invoke-static {v0}, Lcom/osp/app/signin/SamsungService;->a(Z)V

    .line 140
    invoke-static {p0}, Lcom/osp/app/util/ad;->d(Landroid/content/Context;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 142
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v0, "Samsung account does not exist"

    invoke-static {v0}, Lcom/osp/app/util/aq;->c(Ljava/lang/String;)V

    .line 143
    invoke-virtual {p0}, Lcom/osp/app/signin/AccountPreference;->finish()V

    .line 235
    :goto_0
    return-void

    .line 147
    :cond_0
    invoke-static {p0}, Lcom/osp/app/util/ad;->a(Landroid/content/Context;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 149
    invoke-static {}, Lcom/osp/app/util/ap;->a()Lcom/osp/app/util/ap;

    const v0, 0x7f09003e

    invoke-virtual {p0, v0}, Lcom/osp/app/signin/AccountPreference;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-static {p0, v0, v4}, Lcom/osp/app/util/ap;->a(Landroid/content/Context;Ljava/lang/String;I)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    .line 150
    invoke-virtual {p0}, Lcom/osp/app/signin/AccountPreference;->finish()V

    goto :goto_0

    .line 161
    :cond_1
    const v0, 0x7f030005

    invoke-virtual {p0, v0}, Lcom/osp/app/signin/AccountPreference;->setContentView(I)V

    .line 162
    const v0, 0x7f040004

    invoke-virtual {p0, v0}, Lcom/osp/app/signin/AccountPreference;->addPreferencesFromResource(I)V

    .line 167
    const v0, 0x7f0c0037

    invoke-virtual {p0, v0}, Lcom/osp/app/signin/AccountPreference;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 168
    const v1, 0x7f0c0036

    invoke-virtual {p0, v1}, Lcom/osp/app/signin/AccountPreference;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/LinearLayout;

    .line 169
    invoke-virtual {v0}, Landroid/widget/TextView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v2

    check-cast v2, Landroid/view/ViewGroup$MarginLayoutParams;

    .line 170
    invoke-virtual {p0}, Lcom/osp/app/signin/AccountPreference;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    invoke-virtual {v3, v6}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v3

    iput v3, v2, Landroid/view/ViewGroup$MarginLayoutParams;->leftMargin:I

    .line 171
    invoke-virtual {p0}, Lcom/osp/app/signin/AccountPreference;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    invoke-virtual {v3, v6}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v3

    iput v3, v2, Landroid/view/ViewGroup$MarginLayoutParams;->rightMargin:I

    .line 172
    invoke-static {v0, v2}, Lcom/msc/sa/c/d;->a(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    .line 174
    invoke-virtual {p0}, Lcom/osp/app/signin/AccountPreference;->a()Z

    move-result v2

    if-eqz v2, :cond_5

    .line 176
    sget-object v2, Lcom/osp/app/util/u;->p:[Ljava/lang/String;

    invoke-static {v2}, Lcom/osp/app/util/u;->b([Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_4

    .line 178
    invoke-virtual {p0}, Lcom/osp/app/signin/AccountPreference;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f070028

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getColor(I)I

    move-result v2

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setTextColor(I)V

    .line 179
    const v0, 0x7f020143

    invoke-virtual {v1, v0}, Landroid/widget/LinearLayout;->setBackgroundResource(I)V

    .line 199
    :goto_1
    invoke-static {}, Lcom/osp/device/b;->b()Lcom/osp/device/b;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/osp/device/b;->i(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 201
    const/16 v0, 0x8

    invoke-virtual {v1, v0}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 203
    invoke-virtual {p0}, Lcom/osp/app/signin/AccountPreference;->a()Z

    move-result v0

    if-eqz v0, :cond_7

    .line 205
    invoke-virtual {p0}, Lcom/osp/app/signin/AccountPreference;->b()V

    .line 212
    :cond_2
    :goto_2
    invoke-virtual {p0}, Lcom/osp/app/signin/AccountPreference;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v0

    iget v0, v0, Landroid/content/res/Configuration;->orientation:I

    invoke-direct {p0, v0}, Lcom/osp/app/signin/AccountPreference;->a(I)V

    .line 214
    invoke-static {p0}, Lcom/osp/app/util/ad;->d(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_8

    invoke-static {p0, v4}, Lcom/msc/c/c;->a(Landroid/content/Context;Z)Z

    move-result v0

    if-nez v0, :cond_8

    .line 216
    invoke-static {p0}, Lcom/osp/app/util/ad;->h(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/osp/app/signin/AccountPreference;->a(Ljava/lang/String;)V

    .line 217
    invoke-virtual {p0}, Lcom/osp/app/signin/AccountPreference;->finish()V

    .line 233
    :cond_3
    :goto_3
    invoke-static {p0}, Lcom/msc/sa/c/d;->a(Landroid/app/Activity;)V

    goto/16 :goto_0

    .line 182
    :cond_4
    invoke-virtual {p0}, Lcom/osp/app/signin/AccountPreference;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-virtual {v2, v5}, Landroid/content/res/Resources;->getColor(I)I

    move-result v2

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setTextColor(I)V

    .line 183
    const v0, 0x7f020142

    invoke-virtual {v1, v0}, Landroid/widget/LinearLayout;->setBackgroundResource(I)V

    goto :goto_1

    .line 189
    :cond_5
    invoke-virtual {p0}, Lcom/osp/app/signin/AccountPreference;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f070007

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getColor(I)I

    move-result v2

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setTextColor(I)V

    .line 192
    invoke-static {p0}, Lcom/osp/app/util/r;->m(Landroid/content/Context;)Z

    move-result v2

    if-eqz v2, :cond_6

    .line 194
    invoke-virtual {p0}, Lcom/osp/app/signin/AccountPreference;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-virtual {v2, v5}, Landroid/content/res/Resources;->getColorStateList(I)Landroid/content/res/ColorStateList;

    move-result-object v2

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setTextColor(Landroid/content/res/ColorStateList;)V

    .line 197
    :cond_6
    const v0, 0x7f020144

    invoke-virtual {v1, v0}, Landroid/widget/LinearLayout;->setBackgroundResource(I)V

    goto :goto_1

    .line 208
    :cond_7
    invoke-virtual {p0}, Lcom/osp/app/signin/AccountPreference;->c()V

    goto :goto_2

    .line 218
    :cond_8
    invoke-static {p0}, Lcom/osp/app/util/ad;->k(Landroid/content/Context;)Z

    move-result v0

    if-nez v0, :cond_3

    .line 220
    new-instance v0, Lcom/osp/app/signin/g;

    invoke-direct {v0, p0}, Lcom/osp/app/signin/g;-><init>(Lcom/osp/app/signin/AccountPreference;)V

    iput-object v0, p0, Lcom/osp/app/signin/AccountPreference;->c:Lcom/osp/app/signin/g;

    .line 221
    iget-object v0, p0, Lcom/osp/app/signin/AccountPreference;->c:Lcom/osp/app/signin/g;

    invoke-virtual {v0}, Lcom/osp/app/signin/g;->b()V

    goto :goto_3
.end method

.method public onOptionsItemSelected(Landroid/view/MenuItem;)Z
    .locals 2

    .prologue
    .line 470
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0xb

    if-lt v0, v1, :cond_0

    .line 472
    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    .line 476
    :cond_0
    :goto_0
    invoke-super {p0, p1}, Lcom/osp/app/util/BasePreferenceActivity;->onOptionsItemSelected(Landroid/view/MenuItem;)Z

    move-result v0

    return v0

    .line 475
    :pswitch_0
    invoke-virtual {p0}, Lcom/osp/app/signin/AccountPreference;->onBackPressed()V

    goto :goto_0

    .line 472
    :pswitch_data_0
    .packed-switch 0x102002c
        :pswitch_0
    .end packed-switch
.end method

.method protected onResume()V
    .locals 2

    .prologue
    .line 276
    invoke-super {p0}, Lcom/osp/app/util/BasePreferenceActivity;->onResume()V

    .line 281
    invoke-static {p0}, Lcom/osp/app/util/ad;->h(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v1

    .line 282
    const v0, 0x7f0c0037

    invoke-virtual {p0, v0}, Lcom/osp/app/signin/AccountPreference;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 283
    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 284
    invoke-static {}, Lcom/osp/device/b;->b()Lcom/osp/device/b;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/osp/device/b;->i(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 286
    invoke-virtual {p0, v1}, Lcom/osp/app/signin/AccountPreference;->setTitle(Ljava/lang/CharSequence;)V

    .line 288
    :cond_0
    iget-boolean v0, p0, Lcom/osp/app/signin/AccountPreference;->b:Z

    if-eqz v0, :cond_2

    .line 290
    invoke-virtual {p0}, Lcom/osp/app/signin/AccountPreference;->finish()V

    .line 298
    :cond_1
    :goto_0
    return-void

    .line 291
    :cond_2
    invoke-static {p0}, Lcom/osp/app/util/ad;->d(Landroid/content/Context;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 293
    invoke-virtual {p0}, Lcom/osp/app/signin/AccountPreference;->finish()V

    goto :goto_0
.end method
