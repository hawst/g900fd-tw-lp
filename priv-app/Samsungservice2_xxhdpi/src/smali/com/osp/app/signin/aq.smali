.class final Lcom/osp/app/signin/aq;
.super Lcom/msc/c/b;
.source "AccountView.java"


# instance fields
.field final synthetic c:Ljava/lang/String;

.field final synthetic d:Lcom/osp/app/signin/AccountView;

.field private e:J

.field private f:Z


# direct methods
.method constructor <init>(Lcom/osp/app/signin/AccountView;Landroid/content/Context;Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 1779
    iput-object p1, p0, Lcom/osp/app/signin/aq;->d:Lcom/osp/app/signin/AccountView;

    iput-object p3, p0, Lcom/osp/app/signin/aq;->c:Ljava/lang/String;

    invoke-direct {p0, p2}, Lcom/msc/c/b;-><init>(Landroid/content/Context;)V

    .line 1788
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/osp/app/signin/aq;->f:Z

    return-void
.end method


# virtual methods
.method protected final varargs a()Ljava/lang/Boolean;
    .locals 3

    .prologue
    .line 1792
    invoke-static {}, Lcom/msc/c/g;->a()Lcom/msc/c/g;

    .line 1793
    iget-object v0, p0, Lcom/osp/app/signin/aq;->d:Lcom/osp/app/signin/AccountView;

    invoke-static {v0, p0}, Lcom/msc/c/g;->c(Landroid/content/Context;Lcom/msc/b/h;)J

    move-result-wide v0

    iput-wide v0, p0, Lcom/osp/app/signin/aq;->e:J

    .line 1795
    iget-wide v0, p0, Lcom/osp/app/signin/aq;->e:J

    invoke-virtual {p0, v0, v1}, Lcom/osp/app/signin/aq;->a(J)V

    .line 1799
    iget-wide v0, p0, Lcom/osp/app/signin/aq;->e:J

    sget-object v2, Lcom/msc/b/g;->b:Lcom/msc/b/g;

    invoke-static {v0, v1, v2}, Lcom/msc/c/g;->a(JLcom/msc/b/g;)V

    .line 1801
    const/4 v0, 0x0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    return-object v0
.end method

.method public final declared-synchronized a(Lcom/msc/b/c;)V
    .locals 4

    .prologue
    .line 1806
    monitor-enter p0

    :try_start_0
    invoke-super {p0, p1}, Lcom/msc/c/b;->a(Lcom/msc/b/c;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1808
    if-nez p1, :cond_0

    .line 1832
    :goto_0
    monitor-exit p0

    return-void

    .line 1813
    :cond_0
    :try_start_1
    invoke-virtual {p1}, Lcom/msc/b/c;->e()Ljava/lang/String;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result-object v0

    .line 1820
    :try_start_2
    iget-object v1, p0, Lcom/osp/app/signin/aq;->d:Lcom/osp/app/signin/AccountView;

    invoke-static {v1, v0}, Lcom/osp/app/signin/SamsungService;->a(Landroid/content/Context;Ljava/lang/String;)V

    .line 1821
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v1, "saved signatrue"

    invoke-static {v1}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;)V

    .line 1824
    invoke-static {}, Lcom/msc/c/h;->a()Lcom/msc/c/h;

    invoke-static {v0}, Lcom/msc/c/h;->A(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v0

    .line 1826
    invoke-static {}, Lcom/msc/c/h;->a()Lcom/msc/c/h;

    iget-object v1, p0, Lcom/osp/app/signin/aq;->d:Lcom/osp/app/signin/AccountView;

    iget-object v2, p0, Lcom/osp/app/signin/aq;->d:Lcom/osp/app/signin/AccountView;

    invoke-static {v2}, Lcom/osp/app/signin/AccountView;->l(Lcom/osp/app/signin/AccountView;)Ljava/lang/String;

    move-result-object v2

    iget-object v3, p0, Lcom/osp/app/signin/aq;->c:Ljava/lang/String;

    invoke-static {v1, v0, v2, v3}, Lcom/msc/c/h;->a(Landroid/content/Context;Ljava/util/ArrayList;Ljava/lang/String;Ljava/lang/String;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/osp/app/signin/aq;->f:Z
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_0
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0

    .line 1828
    :catch_0
    move-exception v0

    :try_start_3
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    goto :goto_0

    .line 1806
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method protected final a(Ljava/lang/Boolean;)V
    .locals 5

    .prologue
    .line 1843
    invoke-super {p0, p1}, Lcom/msc/c/b;->a(Ljava/lang/Boolean;)V

    .line 1847
    :try_start_0
    iget-boolean v0, p0, Lcom/osp/app/signin/aq;->f:Z

    if-eqz v0, :cond_0

    .line 1851
    iget-object v0, p0, Lcom/osp/app/signin/aq;->d:Lcom/osp/app/signin/AccountView;

    invoke-static {v0}, Lcom/osp/app/signin/AccountView;->k(Lcom/osp/app/signin/AccountView;)V

    .line 1865
    :goto_0
    return-void

    .line 1858
    :cond_0
    iget-object v0, p0, Lcom/osp/app/signin/aq;->d:Lcom/osp/app/signin/AccountView;

    const/4 v1, 0x1

    iget-object v2, p0, Lcom/osp/app/signin/aq;->d:Lcom/osp/app/signin/AccountView;

    invoke-static {v2}, Lcom/osp/app/signin/AccountView;->m(Lcom/osp/app/signin/AccountView;)Landroid/content/Intent;

    move-result-object v2

    new-instance v3, Landroid/content/Intent;

    const-string v4, "com.msc.action.samsungaccount.response.BackGroundSignin"

    invoke-direct {v3, v4}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    invoke-static {v0, v1, v2, v3}, Lcom/osp/app/signin/AccountView;->a(Lcom/osp/app/signin/AccountView;ILandroid/content/Intent;Landroid/content/Intent;)V

    .line 1859
    iget-object v0, p0, Lcom/osp/app/signin/aq;->d:Lcom/osp/app/signin/AccountView;

    invoke-virtual {v0}, Lcom/osp/app/signin/AccountView;->finish()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 1861
    :catch_0
    move-exception v0

    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_0
.end method

.method protected final d()V
    .locals 2

    .prologue
    .line 1836
    invoke-super {p0}, Lcom/msc/c/b;->d()V

    .line 1837
    iget-object v0, p0, Lcom/osp/app/signin/aq;->d:Lcom/osp/app/signin/AccountView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/osp/app/signin/AccountView;->b(I)V

    .line 1838
    iget-object v0, p0, Lcom/osp/app/signin/aq;->d:Lcom/osp/app/signin/AccountView;

    invoke-virtual {v0}, Lcom/osp/app/signin/AccountView;->finish()V

    .line 1839
    return-void
.end method

.method protected final synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 1779
    invoke-virtual {p0}, Lcom/osp/app/signin/aq;->a()Ljava/lang/Boolean;

    move-result-object v0

    return-object v0
.end method

.method protected final synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 1779
    check-cast p1, Ljava/lang/Boolean;

    invoke-virtual {p0, p1}, Lcom/osp/app/signin/aq;->a(Ljava/lang/Boolean;)V

    return-void
.end method
