.class public Lcom/osp/app/signin/SignUpinfo;
.super Ljava/lang/Object;
.source "SignUpinfo.java"

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;


# instance fields
.field private A:Ljava/lang/String;

.field private B:Ljava/lang/String;

.field private C:Ljava/lang/String;

.field private D:Ljava/lang/String;

.field private E:Ljava/lang/String;

.field private F:Ljava/lang/String;

.field private G:Ljava/lang/String;

.field private H:Ljava/lang/String;

.field private I:Ljava/lang/String;

.field private J:Ljava/lang/String;

.field private K:Ljava/lang/String;

.field private L:Ljava/lang/String;

.field private M:Ljava/lang/String;

.field private N:Ljava/lang/String;

.field private O:Z

.field private P:Ljava/lang/String;

.field private Q:Ljava/lang/String;

.field private a:Ljava/lang/String;

.field private b:Ljava/lang/String;

.field private c:Ljava/lang/String;

.field private d:Ljava/lang/String;

.field private e:Ljava/lang/String;

.field private f:Ljava/lang/String;

.field private g:Ljava/lang/String;

.field private h:Ljava/lang/String;

.field private i:Z

.field private j:Z

.field private k:Ljava/lang/String;

.field private l:Ljava/lang/String;

.field private m:Z

.field private n:Z

.field private o:Ljava/lang/String;

.field private p:Z

.field private q:Z

.field private r:Ljava/lang/String;

.field private s:Ljava/lang/String;

.field private t:Ljava/lang/String;

.field private u:Ljava/lang/String;

.field private v:Ljava/lang/String;

.field private w:Ljava/lang/String;

.field private x:Ljava/lang/String;

.field private y:Ljava/lang/String;

.field private z:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 449
    new-instance v0, Lcom/osp/app/signin/mo;

    invoke-direct {v0}, Lcom/osp/app/signin/mo;-><init>()V

    sput-object v0, Lcom/osp/app/signin/SignUpinfo;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 323
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 89
    const-string v0, ""

    iput-object v0, p0, Lcom/osp/app/signin/SignUpinfo;->o:Ljava/lang/String;

    .line 325
    const-string v0, ""

    iput-object v0, p0, Lcom/osp/app/signin/SignUpinfo;->b:Ljava/lang/String;

    .line 326
    const-string v0, ""

    iput-object v0, p0, Lcom/osp/app/signin/SignUpinfo;->v:Ljava/lang/String;

    .line 327
    const-string v0, ""

    iput-object v0, p0, Lcom/osp/app/signin/SignUpinfo;->w:Ljava/lang/String;

    .line 328
    const-string v0, ""

    iput-object v0, p0, Lcom/osp/app/signin/SignUpinfo;->u:Ljava/lang/String;

    .line 329
    const-string v0, ""

    iput-object v0, p0, Lcom/osp/app/signin/SignUpinfo;->c:Ljava/lang/String;

    .line 330
    const-string v0, ""

    iput-object v0, p0, Lcom/osp/app/signin/SignUpinfo;->d:Ljava/lang/String;

    .line 331
    const-string v0, ""

    iput-object v0, p0, Lcom/osp/app/signin/SignUpinfo;->e:Ljava/lang/String;

    .line 332
    const-string v0, ""

    iput-object v0, p0, Lcom/osp/app/signin/SignUpinfo;->f:Ljava/lang/String;

    .line 333
    const-string v0, ""

    iput-object v0, p0, Lcom/osp/app/signin/SignUpinfo;->g:Ljava/lang/String;

    .line 334
    iput-boolean v1, p0, Lcom/osp/app/signin/SignUpinfo;->i:Z

    .line 335
    iput-boolean v1, p0, Lcom/osp/app/signin/SignUpinfo;->j:Z

    .line 336
    const-string v0, ""

    iput-object v0, p0, Lcom/osp/app/signin/SignUpinfo;->h:Ljava/lang/String;

    .line 339
    const-string v0, ""

    iput-object v0, p0, Lcom/osp/app/signin/SignUpinfo;->z:Ljava/lang/String;

    .line 340
    const-string v0, ""

    iput-object v0, p0, Lcom/osp/app/signin/SignUpinfo;->A:Ljava/lang/String;

    .line 341
    const-string v0, ""

    iput-object v0, p0, Lcom/osp/app/signin/SignUpinfo;->B:Ljava/lang/String;

    .line 342
    const-string v0, ""

    iput-object v0, p0, Lcom/osp/app/signin/SignUpinfo;->C:Ljava/lang/String;

    .line 343
    const-string v0, ""

    iput-object v0, p0, Lcom/osp/app/signin/SignUpinfo;->D:Ljava/lang/String;

    .line 344
    const-string v0, ""

    iput-object v0, p0, Lcom/osp/app/signin/SignUpinfo;->E:Ljava/lang/String;

    .line 346
    const-string v0, ""

    iput-object v0, p0, Lcom/osp/app/signin/SignUpinfo;->F:Ljava/lang/String;

    .line 347
    const-string v0, ""

    iput-object v0, p0, Lcom/osp/app/signin/SignUpinfo;->G:Ljava/lang/String;

    .line 348
    const-string v0, ""

    iput-object v0, p0, Lcom/osp/app/signin/SignUpinfo;->H:Ljava/lang/String;

    .line 349
    const-string v0, ""

    iput-object v0, p0, Lcom/osp/app/signin/SignUpinfo;->I:Ljava/lang/String;

    .line 350
    const-string v0, ""

    iput-object v0, p0, Lcom/osp/app/signin/SignUpinfo;->J:Ljava/lang/String;

    .line 351
    const-string v0, ""

    iput-object v0, p0, Lcom/osp/app/signin/SignUpinfo;->K:Ljava/lang/String;

    .line 352
    const-string v0, ""

    iput-object v0, p0, Lcom/osp/app/signin/SignUpinfo;->L:Ljava/lang/String;

    .line 353
    const-string v0, ""

    iput-object v0, p0, Lcom/osp/app/signin/SignUpinfo;->M:Ljava/lang/String;

    .line 355
    const-string v0, ""

    iput-object v0, p0, Lcom/osp/app/signin/SignUpinfo;->N:Ljava/lang/String;

    .line 356
    iput-boolean v1, p0, Lcom/osp/app/signin/SignUpinfo;->O:Z

    .line 358
    const-string v0, ""

    iput-object v0, p0, Lcom/osp/app/signin/SignUpinfo;->k:Ljava/lang/String;

    .line 359
    const-string v0, ""

    iput-object v0, p0, Lcom/osp/app/signin/SignUpinfo;->l:Ljava/lang/String;

    .line 361
    const-string v0, ""

    iput-object v0, p0, Lcom/osp/app/signin/SignUpinfo;->x:Ljava/lang/String;

    .line 362
    const-string v0, ""

    iput-object v0, p0, Lcom/osp/app/signin/SignUpinfo;->y:Ljava/lang/String;

    .line 364
    invoke-static {}, Lcom/osp/device/b;->b()Lcom/osp/device/b;

    move-result-object v0

    invoke-virtual {v0}, Lcom/osp/device/b;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 366
    const-string v0, ""

    iput-object v0, p0, Lcom/osp/app/signin/SignUpinfo;->P:Ljava/lang/String;

    .line 367
    const-string v0, ""

    iput-object v0, p0, Lcom/osp/app/signin/SignUpinfo;->Q:Ljava/lang/String;

    .line 369
    iput-boolean v1, p0, Lcom/osp/app/signin/SignUpinfo;->p:Z

    .line 370
    iput-boolean v1, p0, Lcom/osp/app/signin/SignUpinfo;->q:Z

    .line 372
    const-string v0, ""

    iput-object v0, p0, Lcom/osp/app/signin/SignUpinfo;->r:Ljava/lang/String;

    .line 373
    const-string v0, ""

    iput-object v0, p0, Lcom/osp/app/signin/SignUpinfo;->t:Ljava/lang/String;

    .line 374
    const-string v0, ""

    iput-object v0, p0, Lcom/osp/app/signin/SignUpinfo;->s:Ljava/lang/String;

    .line 376
    :cond_0
    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 2

    .prologue
    .line 383
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 89
    const-string v0, ""

    iput-object v0, p0, Lcom/osp/app/signin/SignUpinfo;->o:Ljava/lang/String;

    .line 385
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Ljava/lang/String;)Ljava/lang/Boolean;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    iput-boolean v0, p0, Lcom/osp/app/signin/SignUpinfo;->i:Z

    .line 386
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Ljava/lang/String;)Ljava/lang/Boolean;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    iput-boolean v0, p0, Lcom/osp/app/signin/SignUpinfo;->j:Z

    .line 387
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Ljava/lang/String;)Ljava/lang/Boolean;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    iput-boolean v0, p0, Lcom/osp/app/signin/SignUpinfo;->m:Z

    .line 388
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Ljava/lang/String;)Ljava/lang/Boolean;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    iput-boolean v0, p0, Lcom/osp/app/signin/SignUpinfo;->O:Z

    .line 390
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "mShowPwd : "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-boolean v1, p0, Lcom/osp/app/signin/SignUpinfo;->i:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;)V

    .line 391
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "mEmailReceiveYNFlag : "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-boolean v1, p0, Lcom/osp/app/signin/SignUpinfo;->j:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;)V

    .line 392
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "mCheckEmailValidation : "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-boolean v1, p0, Lcom/osp/app/signin/SignUpinfo;->m:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;)V

    .line 393
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "mTncAccepted : "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-boolean v1, p0, Lcom/osp/app/signin/SignUpinfo;->O:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;)V

    .line 395
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Ljava/lang/String;)Ljava/lang/Boolean;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    iput-boolean v0, p0, Lcom/osp/app/signin/SignUpinfo;->n:Z

    .line 397
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/osp/app/signin/SignUpinfo;->a:Ljava/lang/String;

    .line 398
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/osp/app/signin/SignUpinfo;->b:Ljava/lang/String;

    .line 399
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/osp/app/signin/SignUpinfo;->v:Ljava/lang/String;

    .line 400
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/osp/app/signin/SignUpinfo;->w:Ljava/lang/String;

    .line 401
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/osp/app/signin/SignUpinfo;->u:Ljava/lang/String;

    .line 402
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/osp/app/signin/SignUpinfo;->c:Ljava/lang/String;

    .line 403
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/osp/app/signin/SignUpinfo;->d:Ljava/lang/String;

    .line 404
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/osp/app/signin/SignUpinfo;->e:Ljava/lang/String;

    .line 405
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/osp/app/signin/SignUpinfo;->f:Ljava/lang/String;

    .line 406
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/osp/app/signin/SignUpinfo;->g:Ljava/lang/String;

    .line 407
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/osp/app/signin/SignUpinfo;->h:Ljava/lang/String;

    .line 409
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/osp/app/signin/SignUpinfo;->o:Ljava/lang/String;

    .line 410
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/osp/app/signin/SignUpinfo;->z:Ljava/lang/String;

    .line 411
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/osp/app/signin/SignUpinfo;->A:Ljava/lang/String;

    .line 412
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/osp/app/signin/SignUpinfo;->B:Ljava/lang/String;

    .line 413
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/osp/app/signin/SignUpinfo;->C:Ljava/lang/String;

    .line 414
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/osp/app/signin/SignUpinfo;->D:Ljava/lang/String;

    .line 415
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/osp/app/signin/SignUpinfo;->E:Ljava/lang/String;

    .line 417
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/osp/app/signin/SignUpinfo;->F:Ljava/lang/String;

    .line 418
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/osp/app/signin/SignUpinfo;->G:Ljava/lang/String;

    .line 419
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/osp/app/signin/SignUpinfo;->H:Ljava/lang/String;

    .line 420
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/osp/app/signin/SignUpinfo;->I:Ljava/lang/String;

    .line 421
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/osp/app/signin/SignUpinfo;->J:Ljava/lang/String;

    .line 422
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/osp/app/signin/SignUpinfo;->K:Ljava/lang/String;

    .line 423
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/osp/app/signin/SignUpinfo;->L:Ljava/lang/String;

    .line 424
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/osp/app/signin/SignUpinfo;->M:Ljava/lang/String;

    .line 426
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/osp/app/signin/SignUpinfo;->k:Ljava/lang/String;

    .line 427
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/osp/app/signin/SignUpinfo;->l:Ljava/lang/String;

    .line 429
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/osp/app/signin/SignUpinfo;->x:Ljava/lang/String;

    .line 430
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/osp/app/signin/SignUpinfo;->y:Ljava/lang/String;

    .line 432
    invoke-static {}, Lcom/osp/device/b;->b()Lcom/osp/device/b;

    move-result-object v0

    invoke-virtual {v0}, Lcom/osp/device/b;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 434
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/osp/app/signin/SignUpinfo;->P:Ljava/lang/String;

    .line 435
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/osp/app/signin/SignUpinfo;->Q:Ljava/lang/String;

    .line 437
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Ljava/lang/String;)Ljava/lang/Boolean;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    iput-boolean v0, p0, Lcom/osp/app/signin/SignUpinfo;->p:Z

    .line 438
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Ljava/lang/String;)Ljava/lang/Boolean;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    iput-boolean v0, p0, Lcom/osp/app/signin/SignUpinfo;->q:Z

    .line 440
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/osp/app/signin/SignUpinfo;->r:Ljava/lang/String;

    .line 441
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/osp/app/signin/SignUpinfo;->t:Ljava/lang/String;

    .line 442
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/osp/app/signin/SignUpinfo;->s:Ljava/lang/String;

    .line 444
    :cond_0
    return-void
.end method


# virtual methods
.method public final A()Ljava/lang/String;
    .locals 1

    .prologue
    .line 722
    iget-object v0, p0, Lcom/osp/app/signin/SignUpinfo;->h:Ljava/lang/String;

    return-object v0
.end method

.method public final A(Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 1082
    iput-object p1, p0, Lcom/osp/app/signin/SignUpinfo;->Q:Ljava/lang/String;

    .line 1083
    return-void
.end method

.method public final B()Ljava/lang/String;
    .locals 1

    .prologue
    .line 731
    iget-object v0, p0, Lcom/osp/app/signin/SignUpinfo;->N:Ljava/lang/String;

    return-object v0
.end method

.method public final B(Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 1133
    iput-object p1, p0, Lcom/osp/app/signin/SignUpinfo;->r:Ljava/lang/String;

    .line 1134
    return-void
.end method

.method public final C()Ljava/lang/String;
    .locals 1

    .prologue
    .line 740
    iget-object v0, p0, Lcom/osp/app/signin/SignUpinfo;->k:Ljava/lang/String;

    return-object v0
.end method

.method public final C(Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 1151
    iput-object p1, p0, Lcom/osp/app/signin/SignUpinfo;->s:Ljava/lang/String;

    .line 1152
    return-void
.end method

.method public final D()Ljava/lang/String;
    .locals 1

    .prologue
    .line 749
    iget-object v0, p0, Lcom/osp/app/signin/SignUpinfo;->l:Ljava/lang/String;

    return-object v0
.end method

.method public final D(Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 1169
    iput-object p1, p0, Lcom/osp/app/signin/SignUpinfo;->t:Ljava/lang/String;

    .line 1170
    return-void
.end method

.method public final E()Ljava/lang/String;
    .locals 1

    .prologue
    .line 758
    iget-object v0, p0, Lcom/osp/app/signin/SignUpinfo;->P:Ljava/lang/String;

    return-object v0
.end method

.method public final E(Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 1182
    iput-object p1, p0, Lcom/osp/app/signin/SignUpinfo;->x:Ljava/lang/String;

    .line 1183
    return-void
.end method

.method public final F()Ljava/lang/String;
    .locals 1

    .prologue
    .line 767
    iget-object v0, p0, Lcom/osp/app/signin/SignUpinfo;->Q:Ljava/lang/String;

    return-object v0
.end method

.method public final F(Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 1190
    iput-object p1, p0, Lcom/osp/app/signin/SignUpinfo;->y:Ljava/lang/String;

    .line 1191
    return-void
.end method

.method public final G()V
    .locals 1

    .prologue
    .line 1097
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/osp/app/signin/SignUpinfo;->p:Z

    .line 1098
    return-void
.end method

.method public final H()Z
    .locals 1

    .prologue
    .line 1106
    iget-boolean v0, p0, Lcom/osp/app/signin/SignUpinfo;->p:Z

    return v0
.end method

.method public final I()V
    .locals 1

    .prologue
    .line 1115
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/osp/app/signin/SignUpinfo;->q:Z

    .line 1116
    return-void
.end method

.method public final J()Z
    .locals 1

    .prologue
    .line 1124
    iget-boolean v0, p0, Lcom/osp/app/signin/SignUpinfo;->q:Z

    return v0
.end method

.method public final K()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1142
    iget-object v0, p0, Lcom/osp/app/signin/SignUpinfo;->r:Ljava/lang/String;

    return-object v0
.end method

.method public final L()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1160
    iget-object v0, p0, Lcom/osp/app/signin/SignUpinfo;->s:Ljava/lang/String;

    return-object v0
.end method

.method public final M()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1178
    iget-object v0, p0, Lcom/osp/app/signin/SignUpinfo;->t:Ljava/lang/String;

    return-object v0
.end method

.method public final N()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1186
    iget-object v0, p0, Lcom/osp/app/signin/SignUpinfo;->x:Ljava/lang/String;

    return-object v0
.end method

.method public final O()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1194
    iget-object v0, p0, Lcom/osp/app/signin/SignUpinfo;->y:Ljava/lang/String;

    return-object v0
.end method

.method public final a()V
    .locals 1

    .prologue
    .line 144
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/osp/app/signin/SignUpinfo;->n:Z

    .line 145
    return-void
.end method

.method public final a(Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 776
    iput-object p1, p0, Lcom/osp/app/signin/SignUpinfo;->a:Ljava/lang/String;

    .line 777
    return-void
.end method

.method public final a(Z)V
    .locals 0

    .prologue
    .line 875
    iput-boolean p1, p0, Lcom/osp/app/signin/SignUpinfo;->i:Z

    .line 876
    return-void
.end method

.method public final b(Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 785
    iput-object p1, p0, Lcom/osp/app/signin/SignUpinfo;->b:Ljava/lang/String;

    .line 787
    iget-object v0, p0, Lcom/osp/app/signin/SignUpinfo;->u:Ljava/lang/String;

    const-string v1, "003"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 789
    iput-object p1, p0, Lcom/osp/app/signin/SignUpinfo;->v:Ljava/lang/String;

    .line 791
    :cond_0
    iget-object v0, p0, Lcom/osp/app/signin/SignUpinfo;->u:Ljava/lang/String;

    const-string v1, "001"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 793
    iput-object p1, p0, Lcom/osp/app/signin/SignUpinfo;->w:Ljava/lang/String;

    .line 795
    :cond_1
    return-void
.end method

.method public final b(Z)V
    .locals 0

    .prologue
    .line 884
    iput-boolean p1, p0, Lcom/osp/app/signin/SignUpinfo;->j:Z

    .line 885
    return-void
.end method

.method public final b()Z
    .locals 1

    .prologue
    .line 153
    iget-boolean v0, p0, Lcom/osp/app/signin/SignUpinfo;->n:Z

    return v0
.end method

.method public final c()Ljava/lang/String;
    .locals 1

    .prologue
    .line 162
    iget-object v0, p0, Lcom/osp/app/signin/SignUpinfo;->o:Ljava/lang/String;

    return-object v0
.end method

.method public final c(Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 821
    iput-object p1, p0, Lcom/osp/app/signin/SignUpinfo;->u:Ljava/lang/String;

    .line 822
    return-void
.end method

.method public final c(Z)V
    .locals 0

    .prologue
    .line 893
    iput-boolean p1, p0, Lcom/osp/app/signin/SignUpinfo;->m:Z

    .line 894
    return-void
.end method

.method public final d()Ljava/lang/String;
    .locals 1

    .prologue
    .line 470
    iget-object v0, p0, Lcom/osp/app/signin/SignUpinfo;->a:Ljava/lang/String;

    return-object v0
.end method

.method public final d(Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 830
    iput-object p1, p0, Lcom/osp/app/signin/SignUpinfo;->c:Ljava/lang/String;

    .line 831
    return-void
.end method

.method public describeContents()I
    .locals 1

    .prologue
    .line 1088
    const/4 v0, 0x0

    return v0
.end method

.method public final e()Ljava/lang/String;
    .locals 1

    .prologue
    .line 479
    iget-object v0, p0, Lcom/osp/app/signin/SignUpinfo;->b:Ljava/lang/String;

    return-object v0
.end method

.method public final e(Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 839
    iput-object p1, p0, Lcom/osp/app/signin/SignUpinfo;->d:Ljava/lang/String;

    .line 840
    return-void
.end method

.method public final f()Ljava/lang/String;
    .locals 1

    .prologue
    .line 488
    iget-object v0, p0, Lcom/osp/app/signin/SignUpinfo;->v:Ljava/lang/String;

    return-object v0
.end method

.method public final f(Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 848
    iput-object p1, p0, Lcom/osp/app/signin/SignUpinfo;->e:Ljava/lang/String;

    .line 849
    return-void
.end method

.method public final g()Ljava/lang/String;
    .locals 1

    .prologue
    .line 497
    iget-object v0, p0, Lcom/osp/app/signin/SignUpinfo;->w:Ljava/lang/String;

    return-object v0
.end method

.method public final g(Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 857
    iput-object p1, p0, Lcom/osp/app/signin/SignUpinfo;->f:Ljava/lang/String;

    .line 858
    return-void
.end method

.method public final h()Ljava/lang/String;
    .locals 1

    .prologue
    .line 515
    iget-object v0, p0, Lcom/osp/app/signin/SignUpinfo;->c:Ljava/lang/String;

    return-object v0
.end method

.method public final h(Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 866
    iput-object p1, p0, Lcom/osp/app/signin/SignUpinfo;->g:Ljava/lang/String;

    .line 867
    return-void
.end method

.method public final i()Ljava/lang/String;
    .locals 1

    .prologue
    .line 533
    iget-object v0, p0, Lcom/osp/app/signin/SignUpinfo;->e:Ljava/lang/String;

    return-object v0
.end method

.method public final i(Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 902
    iput-object p1, p0, Lcom/osp/app/signin/SignUpinfo;->z:Ljava/lang/String;

    .line 903
    return-void
.end method

.method public final j()Ljava/lang/String;
    .locals 1

    .prologue
    .line 542
    iget-object v0, p0, Lcom/osp/app/signin/SignUpinfo;->f:Ljava/lang/String;

    return-object v0
.end method

.method public final j(Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 911
    iput-object p1, p0, Lcom/osp/app/signin/SignUpinfo;->A:Ljava/lang/String;

    .line 912
    return-void
.end method

.method public final k()Ljava/lang/String;
    .locals 1

    .prologue
    .line 551
    iget-object v0, p0, Lcom/osp/app/signin/SignUpinfo;->g:Ljava/lang/String;

    return-object v0
.end method

.method public final k(Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 920
    iput-object p1, p0, Lcom/osp/app/signin/SignUpinfo;->B:Ljava/lang/String;

    .line 921
    return-void
.end method

.method public final l(Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 929
    iput-object p1, p0, Lcom/osp/app/signin/SignUpinfo;->C:Ljava/lang/String;

    .line 930
    return-void
.end method

.method public final l()Z
    .locals 1

    .prologue
    .line 578
    iget-boolean v0, p0, Lcom/osp/app/signin/SignUpinfo;->j:Z

    return v0
.end method

.method public final m(Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 938
    iput-object p1, p0, Lcom/osp/app/signin/SignUpinfo;->D:Ljava/lang/String;

    .line 939
    return-void
.end method

.method public final m()Z
    .locals 1

    .prologue
    .line 587
    iget-boolean v0, p0, Lcom/osp/app/signin/SignUpinfo;->m:Z

    return v0
.end method

.method public final n()Ljava/lang/String;
    .locals 1

    .prologue
    .line 596
    iget-object v0, p0, Lcom/osp/app/signin/SignUpinfo;->z:Ljava/lang/String;

    return-object v0
.end method

.method public final n(Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 947
    iput-object p1, p0, Lcom/osp/app/signin/SignUpinfo;->E:Ljava/lang/String;

    .line 948
    return-void
.end method

.method public final o()Ljava/lang/String;
    .locals 1

    .prologue
    .line 605
    iget-object v0, p0, Lcom/osp/app/signin/SignUpinfo;->A:Ljava/lang/String;

    return-object v0
.end method

.method public final o(Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 956
    iput-object p1, p0, Lcom/osp/app/signin/SignUpinfo;->F:Ljava/lang/String;

    .line 957
    return-void
.end method

.method public final p()Ljava/lang/String;
    .locals 1

    .prologue
    .line 614
    iget-object v0, p0, Lcom/osp/app/signin/SignUpinfo;->B:Ljava/lang/String;

    return-object v0
.end method

.method public final p(Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 965
    iput-object p1, p0, Lcom/osp/app/signin/SignUpinfo;->G:Ljava/lang/String;

    .line 966
    return-void
.end method

.method public final q()Ljava/lang/String;
    .locals 1

    .prologue
    .line 623
    iget-object v0, p0, Lcom/osp/app/signin/SignUpinfo;->C:Ljava/lang/String;

    return-object v0
.end method

.method public final q(Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 974
    iput-object p1, p0, Lcom/osp/app/signin/SignUpinfo;->H:Ljava/lang/String;

    .line 975
    return-void
.end method

.method public final r()Ljava/lang/String;
    .locals 1

    .prologue
    .line 632
    iget-object v0, p0, Lcom/osp/app/signin/SignUpinfo;->D:Ljava/lang/String;

    return-object v0
.end method

.method public final r(Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 983
    iput-object p1, p0, Lcom/osp/app/signin/SignUpinfo;->I:Ljava/lang/String;

    .line 984
    return-void
.end method

.method public final s()Ljava/lang/String;
    .locals 1

    .prologue
    .line 641
    iget-object v0, p0, Lcom/osp/app/signin/SignUpinfo;->E:Ljava/lang/String;

    return-object v0
.end method

.method public final s(Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 992
    iput-object p1, p0, Lcom/osp/app/signin/SignUpinfo;->J:Ljava/lang/String;

    .line 993
    return-void
.end method

.method public final t()Ljava/lang/String;
    .locals 1

    .prologue
    .line 650
    iget-object v0, p0, Lcom/osp/app/signin/SignUpinfo;->F:Ljava/lang/String;

    return-object v0
.end method

.method public final t(Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 1010
    iput-object p1, p0, Lcom/osp/app/signin/SignUpinfo;->L:Ljava/lang/String;

    .line 1011
    return-void
.end method

.method public final u()Ljava/lang/String;
    .locals 1

    .prologue
    .line 659
    iget-object v0, p0, Lcom/osp/app/signin/SignUpinfo;->G:Ljava/lang/String;

    return-object v0
.end method

.method public final u(Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 1019
    iput-object p1, p0, Lcom/osp/app/signin/SignUpinfo;->M:Ljava/lang/String;

    .line 1020
    return-void
.end method

.method public final v()Ljava/lang/String;
    .locals 1

    .prologue
    .line 668
    iget-object v0, p0, Lcom/osp/app/signin/SignUpinfo;->H:Ljava/lang/String;

    return-object v0
.end method

.method public final v(Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 1028
    iput-object p1, p0, Lcom/osp/app/signin/SignUpinfo;->h:Ljava/lang/String;

    .line 1029
    return-void
.end method

.method public final w()Ljava/lang/String;
    .locals 1

    .prologue
    .line 677
    iget-object v0, p0, Lcom/osp/app/signin/SignUpinfo;->I:Ljava/lang/String;

    return-object v0
.end method

.method public final w(Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 1037
    iput-object p1, p0, Lcom/osp/app/signin/SignUpinfo;->N:Ljava/lang/String;

    .line 1038
    return-void
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 1

    .prologue
    .line 266
    iget-boolean v0, p0, Lcom/osp/app/signin/SignUpinfo;->i:Z

    invoke-static {v0}, Ljava/lang/Boolean;->toString(Z)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 267
    iget-boolean v0, p0, Lcom/osp/app/signin/SignUpinfo;->j:Z

    invoke-static {v0}, Ljava/lang/Boolean;->toString(Z)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 268
    iget-boolean v0, p0, Lcom/osp/app/signin/SignUpinfo;->m:Z

    invoke-static {v0}, Ljava/lang/Boolean;->toString(Z)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 269
    iget-boolean v0, p0, Lcom/osp/app/signin/SignUpinfo;->O:Z

    invoke-static {v0}, Ljava/lang/Boolean;->toString(Z)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 270
    iget-boolean v0, p0, Lcom/osp/app/signin/SignUpinfo;->n:Z

    invoke-static {v0}, Ljava/lang/Boolean;->toString(Z)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 272
    iget-object v0, p0, Lcom/osp/app/signin/SignUpinfo;->a:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 273
    iget-object v0, p0, Lcom/osp/app/signin/SignUpinfo;->b:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 274
    iget-object v0, p0, Lcom/osp/app/signin/SignUpinfo;->v:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 275
    iget-object v0, p0, Lcom/osp/app/signin/SignUpinfo;->w:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 276
    iget-object v0, p0, Lcom/osp/app/signin/SignUpinfo;->u:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 277
    iget-object v0, p0, Lcom/osp/app/signin/SignUpinfo;->c:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 278
    iget-object v0, p0, Lcom/osp/app/signin/SignUpinfo;->d:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 279
    iget-object v0, p0, Lcom/osp/app/signin/SignUpinfo;->e:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 280
    iget-object v0, p0, Lcom/osp/app/signin/SignUpinfo;->f:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 281
    iget-object v0, p0, Lcom/osp/app/signin/SignUpinfo;->g:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 282
    iget-object v0, p0, Lcom/osp/app/signin/SignUpinfo;->h:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 284
    iget-object v0, p0, Lcom/osp/app/signin/SignUpinfo;->o:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 285
    iget-object v0, p0, Lcom/osp/app/signin/SignUpinfo;->z:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 286
    iget-object v0, p0, Lcom/osp/app/signin/SignUpinfo;->A:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 287
    iget-object v0, p0, Lcom/osp/app/signin/SignUpinfo;->B:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 288
    iget-object v0, p0, Lcom/osp/app/signin/SignUpinfo;->C:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 289
    iget-object v0, p0, Lcom/osp/app/signin/SignUpinfo;->D:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 290
    iget-object v0, p0, Lcom/osp/app/signin/SignUpinfo;->E:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 292
    iget-object v0, p0, Lcom/osp/app/signin/SignUpinfo;->F:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 293
    iget-object v0, p0, Lcom/osp/app/signin/SignUpinfo;->G:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 294
    iget-object v0, p0, Lcom/osp/app/signin/SignUpinfo;->H:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 295
    iget-object v0, p0, Lcom/osp/app/signin/SignUpinfo;->I:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 296
    iget-object v0, p0, Lcom/osp/app/signin/SignUpinfo;->J:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 297
    iget-object v0, p0, Lcom/osp/app/signin/SignUpinfo;->K:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 298
    iget-object v0, p0, Lcom/osp/app/signin/SignUpinfo;->L:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 299
    iget-object v0, p0, Lcom/osp/app/signin/SignUpinfo;->M:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 301
    iget-object v0, p0, Lcom/osp/app/signin/SignUpinfo;->k:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 302
    iget-object v0, p0, Lcom/osp/app/signin/SignUpinfo;->l:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 304
    iget-object v0, p0, Lcom/osp/app/signin/SignUpinfo;->x:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 305
    iget-object v0, p0, Lcom/osp/app/signin/SignUpinfo;->y:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 307
    invoke-static {}, Lcom/osp/device/b;->b()Lcom/osp/device/b;

    move-result-object v0

    invoke-virtual {v0}, Lcom/osp/device/b;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 309
    iget-object v0, p0, Lcom/osp/app/signin/SignUpinfo;->P:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 310
    iget-object v0, p0, Lcom/osp/app/signin/SignUpinfo;->Q:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 311
    iget-boolean v0, p0, Lcom/osp/app/signin/SignUpinfo;->p:Z

    invoke-static {v0}, Ljava/lang/Boolean;->toString(Z)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 312
    iget-boolean v0, p0, Lcom/osp/app/signin/SignUpinfo;->q:Z

    invoke-static {v0}, Ljava/lang/Boolean;->toString(Z)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 313
    iget-object v0, p0, Lcom/osp/app/signin/SignUpinfo;->r:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 314
    iget-object v0, p0, Lcom/osp/app/signin/SignUpinfo;->t:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 315
    iget-object v0, p0, Lcom/osp/app/signin/SignUpinfo;->s:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 318
    :cond_0
    return-void
.end method

.method public final x()Ljava/lang/String;
    .locals 1

    .prologue
    .line 686
    iget-object v0, p0, Lcom/osp/app/signin/SignUpinfo;->J:Ljava/lang/String;

    return-object v0
.end method

.method public final x(Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 1055
    iput-object p1, p0, Lcom/osp/app/signin/SignUpinfo;->k:Ljava/lang/String;

    .line 1056
    return-void
.end method

.method public final y()Ljava/lang/String;
    .locals 1

    .prologue
    .line 704
    iget-object v0, p0, Lcom/osp/app/signin/SignUpinfo;->L:Ljava/lang/String;

    return-object v0
.end method

.method public final y(Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 1064
    iput-object p1, p0, Lcom/osp/app/signin/SignUpinfo;->l:Ljava/lang/String;

    .line 1065
    return-void
.end method

.method public final z()Ljava/lang/String;
    .locals 1

    .prologue
    .line 713
    iget-object v0, p0, Lcom/osp/app/signin/SignUpinfo;->M:Ljava/lang/String;

    return-object v0
.end method

.method public final z(Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 1073
    iput-object p1, p0, Lcom/osp/app/signin/SignUpinfo;->P:Ljava/lang/String;

    .line 1074
    return-void
.end method
