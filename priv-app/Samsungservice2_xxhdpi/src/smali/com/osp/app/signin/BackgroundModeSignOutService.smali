.class public Lcom/osp/app/signin/BackgroundModeSignOutService;
.super Lcom/osp/app/util/AbstractBaseService;
.source "BackgroundModeSignOutService.java"


# instance fields
.field private a:Z

.field private b:Z

.field private c:Ljava/lang/String;

.field private d:I

.field private e:Z

.field private f:Z

.field private g:Lcom/osp/app/signin/cv;


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 44
    invoke-direct {p0}, Lcom/osp/app/util/AbstractBaseService;-><init>()V

    .line 49
    iput-boolean v1, p0, Lcom/osp/app/signin/BackgroundModeSignOutService;->a:Z

    .line 50
    iput-boolean v1, p0, Lcom/osp/app/signin/BackgroundModeSignOutService;->b:Z

    .line 57
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/osp/app/signin/BackgroundModeSignOutService;->c:Ljava/lang/String;

    .line 70
    iput-boolean v1, p0, Lcom/osp/app/signin/BackgroundModeSignOutService;->e:Z

    .line 71
    iput-boolean v1, p0, Lcom/osp/app/signin/BackgroundModeSignOutService;->f:Z

    .line 266
    return-void
.end method

.method static synthetic a(Lcom/osp/app/signin/BackgroundModeSignOutService;Ljava/lang/String;)Ljava/lang/String;
    .locals 0

    .prologue
    .line 44
    iput-object p1, p0, Lcom/osp/app/signin/BackgroundModeSignOutService;->c:Ljava/lang/String;

    return-object p1
.end method

.method static synthetic a(Lcom/osp/app/signin/BackgroundModeSignOutService;Z)V
    .locals 6

    .prologue
    const/4 v5, 0x1

    const/4 v3, 0x0

    .line 44
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v0, "BSOP"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "SignOuted byAbort=["

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "]"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    const-string v2, "START"

    invoke-static {v0, v1, v2}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    iget-boolean v0, p0, Lcom/osp/app/signin/BackgroundModeSignOutService;->a:Z

    if-ne v0, v5, :cond_1

    iput-boolean v3, p0, Lcom/osp/app/signin/BackgroundModeSignOutService;->a:Z

    :cond_0
    :goto_0
    return-void

    :cond_1
    invoke-static {}, Lcom/osp/app/util/e;->a()Lcom/osp/app/util/e;

    move-result-object v0

    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v1, "BIM"

    const-string v2, "sendRemainAtSignOut"

    invoke-static {v1, v2}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v0, p0}, Lcom/osp/app/util/e;->a(Landroid/content/Context;)V

    const-string v1, "Samsung account is signed out"

    invoke-virtual {v0, p0, v1}, Lcom/osp/app/util/e;->a(Landroid/content/Context;Ljava/lang/String;)V

    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    const-class v1, Lcom/osp/app/signin/BackgroundModeService;

    invoke-virtual {v0, p0, v1}, Landroid/content/Intent;->setClass(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;

    invoke-virtual {p0, v0}, Lcom/osp/app/signin/BackgroundModeSignOutService;->stopService(Landroid/content/Intent;)Z

    const-class v1, Lcom/osp/app/signin/InterfaceService;

    invoke-virtual {v0, p0, v1}, Landroid/content/Intent;->setClass(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;

    invoke-virtual {p0, v0}, Lcom/osp/app/signin/BackgroundModeSignOutService;->stopService(Landroid/content/Intent;)Z

    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v0, "BSOP"

    const-string v1, "SignOuted run"

    const-string v2, "START"

    invoke-static {v0, v1, v2}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    iput-boolean v5, p0, Lcom/osp/app/signin/BackgroundModeSignOutService;->b:Z

    invoke-static {p0}, Lcom/osp/app/util/ad;->r(Landroid/content/Context;)V

    invoke-virtual {p0}, Lcom/osp/app/signin/BackgroundModeSignOutService;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    const-string v1, "BOOT_NOTI"

    invoke-virtual {v0, v1, v3}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    invoke-static {}, Lcom/osp/app/util/r;->d()Z

    move-result v1

    if-nez v1, :cond_2

    const-string v1, "NOTI_DISPLAY"

    invoke-interface {v0, v1, v3}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    :cond_2
    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v0, "DBMV2"

    const-string v1, "deleteDBV02"

    invoke-static {v0, v1}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    sget-object v1, Lcom/msc/contentprovider/SamsungServiceProvider;->a:Landroid/net/Uri;

    const-string v2, "_id = 1"

    const/4 v3, 0x0

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/ContentResolver;->delete(Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;)I

    invoke-static {}, Lcom/osp/app/signin/SamsungService;->p()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-static {p0}, Lcom/msc/openprovider/b;->e(Landroid/content/Context;)V

    :cond_3
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v0, "BSOP"

    const-string v1, "removeData"

    const-string v2, "START"

    invoke-static {v0, v1, v2}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    :try_start_0
    new-instance v0, Lcom/osp/common/property/a;

    invoke-virtual {p0}, Lcom/osp/app/signin/BackgroundModeSignOutService;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/osp/common/property/a;-><init>(Landroid/content/Context;)V

    const-string v1, "SignOut"

    const-string v2, "true"

    invoke-virtual {v0, v1, v2}, Lcom/osp/common/property/a;->b(Ljava/lang/String;Ljava/lang/String;)V

    new-instance v1, Lcom/osp/security/identity/d;

    invoke-virtual {p0}, Lcom/osp/app/signin/BackgroundModeSignOutService;->getApplicationContext()Landroid/content/Context;

    move-result-object v2

    invoke-direct {v1, v2}, Lcom/osp/security/identity/d;-><init>(Landroid/content/Context;)V

    new-instance v2, Lcom/osp/security/credential/a;

    invoke-virtual {p0}, Lcom/osp/app/signin/BackgroundModeSignOutService;->getApplicationContext()Landroid/content/Context;

    move-result-object v3

    invoke-direct {v2, v3}, Lcom/osp/security/credential/a;-><init>(Landroid/content/Context;)V

    new-instance v3, Lcom/osp/social/member/d;

    invoke-virtual {p0}, Lcom/osp/app/signin/BackgroundModeSignOutService;->getApplicationContext()Landroid/content/Context;

    move-result-object v4

    invoke-direct {v3, v4}, Lcom/osp/social/member/d;-><init>(Landroid/content/Context;)V

    invoke-virtual {v1}, Lcom/osp/security/identity/d;->k()V

    invoke-virtual {v2}, Lcom/osp/security/credential/a;->a()V

    invoke-virtual {v3}, Lcom/osp/social/member/d;->a()V

    const-string v1, "SignOut"

    invoke-virtual {v0, v1}, Lcom/osp/common/property/a;->c(Ljava/lang/String;)V

    sget-object v0, Lcom/msc/sa/selfupdate/j;->d:Lcom/msc/sa/selfupdate/j;

    const/4 v1, -0x1

    invoke-static {p0, v0, v1}, Lcom/msc/sa/selfupdate/c;->a(Landroid/content/Context;Lcom/msc/sa/selfupdate/j;I)I
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    :goto_1
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v0, "BSOP"

    const-string v1, "removeData"

    const-string v2, "END"

    invoke-static {v0, v1, v2}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    iget v0, p0, Lcom/osp/app/signin/BackgroundModeSignOutService;->d:I

    invoke-virtual {p0, v0}, Lcom/osp/app/signin/BackgroundModeSignOutService;->stopSelf(I)V

    invoke-virtual {p0}, Lcom/osp/app/signin/BackgroundModeSignOutService;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Landroid/preference/PreferenceManager;->getDefaultSharedPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    const-string v1, "SIGN_OUT_START_TIME"

    invoke-interface {v0, v1}, Landroid/content/SharedPreferences$Editor;->remove(Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v0, "BSOP"

    const-string v1, "SignOuted run"

    const-string v2, "END"

    invoke-static {v0, v1, v2}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    if-eqz p1, :cond_0

    iput-boolean v5, p0, Lcom/osp/app/signin/BackgroundModeSignOutService;->a:Z

    goto/16 :goto_0

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v0, "BSOP"

    const-string v1, "removeData Exception"

    invoke-static {v0, v1}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1
.end method

.method static synthetic a(Lcom/osp/app/signin/BackgroundModeSignOutService;)Z
    .locals 1

    .prologue
    .line 44
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/osp/app/signin/BackgroundModeSignOutService;->e:Z

    return v0
.end method

.method static synthetic b(Lcom/osp/app/signin/BackgroundModeSignOutService;)Z
    .locals 1

    .prologue
    .line 44
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/osp/app/signin/BackgroundModeSignOutService;->f:Z

    return v0
.end method

.method static synthetic c(Lcom/osp/app/signin/BackgroundModeSignOutService;)Z
    .locals 1

    .prologue
    .line 44
    iget-boolean v0, p0, Lcom/osp/app/signin/BackgroundModeSignOutService;->e:Z

    return v0
.end method

.method static synthetic d(Lcom/osp/app/signin/BackgroundModeSignOutService;)Z
    .locals 1

    .prologue
    .line 44
    iget-boolean v0, p0, Lcom/osp/app/signin/BackgroundModeSignOutService;->f:Z

    return v0
.end method

.method static synthetic e(Lcom/osp/app/signin/BackgroundModeSignOutService;)Z
    .locals 1

    .prologue
    .line 44
    iget-boolean v0, p0, Lcom/osp/app/signin/BackgroundModeSignOutService;->b:Z

    return v0
.end method

.method static synthetic f(Lcom/osp/app/signin/BackgroundModeSignOutService;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 44
    iget-object v0, p0, Lcom/osp/app/signin/BackgroundModeSignOutService;->c:Ljava/lang/String;

    return-object v0
.end method


# virtual methods
.method public onBind(Landroid/content/Intent;)Landroid/os/IBinder;
    .locals 1

    .prologue
    .line 563
    const/4 v0, 0x0

    return-object v0
.end method

.method public onDestroy()V
    .locals 2

    .prologue
    .line 257
    invoke-super {p0}, Lcom/osp/app/util/AbstractBaseService;->onDestroy()V

    .line 259
    iget-object v0, p0, Lcom/osp/app/signin/BackgroundModeSignOutService;->g:Lcom/osp/app/signin/cv;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/osp/app/signin/BackgroundModeSignOutService;->g:Lcom/osp/app/signin/cv;

    invoke-virtual {v0}, Lcom/osp/app/signin/cv;->getStatus()Landroid/os/AsyncTask$Status;

    move-result-object v0

    sget-object v1, Landroid/os/AsyncTask$Status;->RUNNING:Landroid/os/AsyncTask$Status;

    if-ne v0, v1, :cond_0

    .line 261
    iget-object v0, p0, Lcom/osp/app/signin/BackgroundModeSignOutService;->g:Lcom/osp/app/signin/cv;

    invoke-virtual {v0}, Lcom/osp/app/signin/cv;->d()V

    .line 262
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/osp/app/signin/BackgroundModeSignOutService;->g:Lcom/osp/app/signin/cv;

    .line 264
    :cond_0
    return-void
.end method

.method public onStartCommand(Landroid/content/Intent;II)I
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 162
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v0, "BSOP"

    const-string v1, "onCreate"

    const-string v2, "START"

    invoke-static {v0, v1, v2}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 164
    invoke-static {}, Lcom/osp/app/pushmarketing/k;->a()Lcom/osp/app/pushmarketing/k;

    new-instance v0, Landroid/content/Intent;

    const-string v1, "com.osp.app.pushmarketing.PushMarketingService"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    const-string v1, "push_marketing_service_mode"

    const/16 v2, 0x63

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    invoke-virtual {p0, v0}, Landroid/content/Context;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;

    .line 166
    invoke-static {}, Lcom/osp/app/util/p;->a()Lcom/osp/app/util/p;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/osp/app/util/p;->c(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 168
    invoke-static {}, Lcom/osp/app/util/p;->a()Lcom/osp/app/util/p;

    invoke-static {p0, v4}, Lcom/osp/app/util/p;->b(Landroid/content/Context;I)Z

    .line 169
    invoke-static {}, Lcom/osp/app/util/p;->a()Lcom/osp/app/util/p;

    invoke-static {p0, v4}, Lcom/osp/app/util/p;->a(Landroid/content/Context;I)Z

    .line 172
    :cond_0
    invoke-virtual {p0}, Lcom/osp/app/signin/BackgroundModeSignOutService;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Landroid/preference/PreferenceManager;->getDefaultSharedPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    const-string v1, "SIGN_OUT_START_TIME"

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    invoke-interface {v0, v1, v2, v3}, Landroid/content/SharedPreferences$Editor;->putLong(Ljava/lang/String;J)Landroid/content/SharedPreferences$Editor;

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 174
    iput-boolean v4, p0, Lcom/osp/app/signin/BackgroundModeSignOutService;->a:Z

    .line 175
    iput-boolean v4, p0, Lcom/osp/app/signin/BackgroundModeSignOutService;->b:Z

    .line 176
    iput-boolean v4, p0, Lcom/osp/app/signin/BackgroundModeSignOutService;->e:Z

    .line 177
    iput-boolean v4, p0, Lcom/osp/app/signin/BackgroundModeSignOutService;->f:Z

    .line 200
    iput p3, p0, Lcom/osp/app/signin/BackgroundModeSignOutService;->d:I

    .line 202
    const-string v0, "notification"

    invoke-virtual {p0, v0}, Lcom/osp/app/signin/BackgroundModeSignOutService;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/NotificationManager;

    .line 203
    const v1, 0x132df82

    invoke-virtual {v0, v1}, Landroid/app/NotificationManager;->cancel(I)V

    .line 204
    const v1, 0x132df83

    invoke-virtual {v0, v1}, Landroid/app/NotificationManager;->cancel(I)V

    .line 206
    new-instance v0, Landroid/content/Intent;

    const-string v1, "android.intent.action.SAMSUNGACCOUNT_SIGNOUT_COMPLETED"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 207
    invoke-static {}, Lcom/osp/device/b;->b()Lcom/osp/device/b;

    move-result-object v1

    invoke-virtual {v1}, Lcom/osp/device/b;->l()I

    move-result v1

    const/16 v2, 0xb

    if-le v1, v2, :cond_1

    .line 209
    const/16 v1, 0x20

    invoke-virtual {v0, v1}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    .line 211
    :cond_1
    invoke-virtual {p0, v0}, Lcom/osp/app/signin/BackgroundModeSignOutService;->sendBroadcast(Landroid/content/Intent;)V

    .line 213
    new-instance v0, Landroid/content/Intent;

    const-string v1, "android.intent.action.REGISTRATION_CANCELED"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 216
    const-string v1, "login_id"

    invoke-static {p0}, Lcom/msc/c/e;->e(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 217
    invoke-static {p0}, Lcom/msc/c/e;->e(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Landroid/telephony/PhoneNumberUtils;->isGlobalPhoneNumber(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_3

    .line 219
    const-string v1, "login_id_type"

    const-string v2, "001"

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 224
    :goto_0
    invoke-virtual {p0, v0}, Lcom/osp/app/signin/BackgroundModeSignOutService;->sendBroadcast(Landroid/content/Intent;)V

    .line 226
    new-instance v0, Landroid/content/Intent;

    const-string v1, "osp.signin.SAMSUNG_ACCOUNT_SIGNOUT"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 227
    invoke-virtual {p0, v0}, Lcom/osp/app/signin/BackgroundModeSignOutService;->sendBroadcast(Landroid/content/Intent;)V

    .line 231
    const/4 v0, 0x1

    invoke-static {p0, v0}, Lcom/msc/c/c;->a(Landroid/content/Context;Z)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 233
    new-instance v0, Lcom/osp/app/signin/cv;

    invoke-direct {v0, p0}, Lcom/osp/app/signin/cv;-><init>(Lcom/osp/app/signin/BackgroundModeSignOutService;)V

    iput-object v0, p0, Lcom/osp/app/signin/BackgroundModeSignOutService;->g:Lcom/osp/app/signin/cv;

    .line 234
    iget-object v0, p0, Lcom/osp/app/signin/BackgroundModeSignOutService;->g:Lcom/osp/app/signin/cv;

    invoke-virtual {v0}, Lcom/osp/app/signin/cv;->c()V

    .line 235
    iget-object v0, p0, Lcom/osp/app/signin/BackgroundModeSignOutService;->g:Lcom/osp/app/signin/cv;

    new-array v1, v4, [Ljava/lang/Void;

    invoke-virtual {v0, v1}, Lcom/osp/app/signin/cv;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    .line 238
    :cond_2
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v0, "BSOP"

    const-string v1, "onCreate"

    const-string v2, "END"

    invoke-static {v0, v1, v2}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 239
    return p3

    .line 222
    :cond_3
    const-string v1, "login_id_type"

    const-string v2, "003"

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    goto :goto_0
.end method
