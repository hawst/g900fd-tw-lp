.class final Lcom/osp/app/signin/iy;
.super Ljava/lang/Object;
.source "SignInView.java"

# interfaces
.implements Landroid/content/DialogInterface$OnKeyListener;


# instance fields
.field final synthetic a:Lcom/osp/app/signin/SignInView;


# direct methods
.method constructor <init>(Lcom/osp/app/signin/SignInView;)V
    .locals 0

    .prologue
    .line 7918
    iput-object p1, p0, Lcom/osp/app/signin/iy;->a:Lcom/osp/app/signin/SignInView;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onKey(Landroid/content/DialogInterface;ILandroid/view/KeyEvent;)Z
    .locals 6

    .prologue
    const/4 v5, 0x4

    const/4 v4, 0x0

    .line 7922
    if-ne p2, v5, :cond_1

    .line 7926
    new-instance v0, Lcom/osp/app/bigdatalog/c;

    iget-object v1, p0, Lcom/osp/app/signin/iy;->a:Lcom/osp/app/signin/SignInView;

    invoke-virtual {v1}, Lcom/osp/app/signin/SignInView;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    const/4 v2, 0x3

    iget-object v3, p0, Lcom/osp/app/signin/iy;->a:Lcom/osp/app/signin/SignInView;

    invoke-static {v3}, Lcom/osp/app/signin/SignInView;->c(Lcom/osp/app/signin/SignInView;)Landroid/content/Intent;

    move-result-object v3

    invoke-direct {v0, v1, v5, v2, v3}, Lcom/osp/app/bigdatalog/c;-><init>(Landroid/content/Context;IILandroid/content/Intent;)V

    .line 7928
    invoke-static {}, Lcom/osp/app/bigdatalog/e;->a()Lcom/osp/app/bigdatalog/e;

    move-result-object v1

    iget-object v2, p0, Lcom/osp/app/signin/iy;->a:Lcom/osp/app/signin/SignInView;

    invoke-virtual {v2}, Lcom/osp/app/signin/SignInView;->getApplicationContext()Landroid/content/Context;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/osp/app/bigdatalog/e;->a(Landroid/content/Context;)V

    .line 7929
    invoke-static {}, Lcom/osp/app/bigdatalog/e;->a()Lcom/osp/app/bigdatalog/e;

    move-result-object v1

    iget-object v2, p0, Lcom/osp/app/signin/iy;->a:Lcom/osp/app/signin/SignInView;

    invoke-virtual {v2}, Lcom/osp/app/signin/SignInView;->getApplicationContext()Landroid/content/Context;

    move-result-object v2

    invoke-virtual {v1, v2, v0}, Lcom/osp/app/bigdatalog/e;->a(Landroid/content/Context;Lcom/osp/app/bigdatalog/c;)V

    .line 7932
    iget-object v0, p0, Lcom/osp/app/signin/iy;->a:Lcom/osp/app/signin/SignInView;

    invoke-static {v0}, Lcom/osp/app/signin/SignInView;->d(Lcom/osp/app/signin/SignInView;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 7934
    iget-object v0, p0, Lcom/osp/app/signin/iy;->a:Lcom/osp/app/signin/SignInView;

    const/4 v1, 0x7

    iget-object v2, p0, Lcom/osp/app/signin/iy;->a:Lcom/osp/app/signin/SignInView;

    invoke-static {v2}, Lcom/osp/app/signin/SignInView;->c(Lcom/osp/app/signin/SignInView;)Landroid/content/Intent;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/osp/app/signin/SignInView;->a(ILandroid/content/Intent;)V

    .line 7948
    :cond_0
    :goto_0
    iget-object v0, p0, Lcom/osp/app/signin/iy;->a:Lcom/osp/app/signin/SignInView;

    invoke-virtual {v0}, Lcom/osp/app/signin/SignInView;->finish()V

    .line 7951
    :cond_1
    return v4

    .line 7938
    :cond_2
    iget-object v0, p0, Lcom/osp/app/signin/iy;->a:Lcom/osp/app/signin/SignInView;

    invoke-static {v0}, Lcom/osp/app/signin/SignInView;->c(Lcom/osp/app/signin/SignInView;)Landroid/content/Intent;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 7940
    iget-object v0, p0, Lcom/osp/app/signin/iy;->a:Lcom/osp/app/signin/SignInView;

    invoke-static {v0}, Lcom/osp/app/signin/SignInView;->c(Lcom/osp/app/signin/SignInView;)Landroid/content/Intent;

    move-result-object v0

    const-string v1, "is_cancelable_just_one_activity"

    invoke-virtual {v0, v1, v4}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 7942
    iget-object v0, p0, Lcom/osp/app/signin/iy;->a:Lcom/osp/app/signin/SignInView;

    const/16 v1, 0xe

    iget-object v2, p0, Lcom/osp/app/signin/iy;->a:Lcom/osp/app/signin/SignInView;

    invoke-static {v2}, Lcom/osp/app/signin/SignInView;->c(Lcom/osp/app/signin/SignInView;)Landroid/content/Intent;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/osp/app/signin/SignInView;->a(ILandroid/content/Intent;)V

    goto :goto_0

    .line 7945
    :cond_3
    iget-object v0, p0, Lcom/osp/app/signin/iy;->a:Lcom/osp/app/signin/SignInView;

    iget-object v1, p0, Lcom/osp/app/signin/iy;->a:Lcom/osp/app/signin/SignInView;

    invoke-static {v1}, Lcom/osp/app/signin/SignInView;->c(Lcom/osp/app/signin/SignInView;)Landroid/content/Intent;

    move-result-object v1

    invoke-virtual {v0, v4, v1}, Lcom/osp/app/signin/SignInView;->a(ILandroid/content/Intent;)V

    goto :goto_0
.end method
