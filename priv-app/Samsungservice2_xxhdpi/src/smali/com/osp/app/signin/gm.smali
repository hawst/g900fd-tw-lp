.class final Lcom/osp/app/signin/gm;
.super Ljava/lang/Object;
.source "PasswordChangeView.java"

# interfaces
.implements Landroid/widget/CompoundButton$OnCheckedChangeListener;


# instance fields
.field final synthetic a:Landroid/widget/CheckBox;

.field final synthetic b:Lcom/osp/app/signin/PasswordChangeView;


# direct methods
.method constructor <init>(Lcom/osp/app/signin/PasswordChangeView;Landroid/widget/CheckBox;)V
    .locals 0

    .prologue
    .line 331
    iput-object p1, p0, Lcom/osp/app/signin/gm;->b:Lcom/osp/app/signin/PasswordChangeView;

    iput-object p2, p0, Lcom/osp/app/signin/gm;->a:Landroid/widget/CheckBox;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onCheckedChanged(Landroid/widget/CompoundButton;Z)V
    .locals 7

    .prologue
    const v6, 0x7f0c010c

    const v5, 0x7f0c0108

    const v4, 0x7f0c0104

    const/16 v3, 0x91

    const/16 v2, 0x81

    .line 337
    iget-object v0, p0, Lcom/osp/app/signin/gm;->a:Landroid/widget/CheckBox;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/CheckBox;->playSoundEffect(I)V

    .line 339
    const/4 v0, 0x1

    if-ne p2, v0, :cond_3

    .line 341
    iget-object v0, p0, Lcom/osp/app/signin/gm;->b:Lcom/osp/app/signin/PasswordChangeView;

    invoke-virtual {v0, v4}, Lcom/osp/app/signin/PasswordChangeView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/EditText;

    .line 342
    if-eqz v0, :cond_0

    .line 344
    invoke-virtual {v0}, Landroid/widget/EditText;->getSelectionEnd()I

    move-result v1

    .line 345
    invoke-virtual {v0, v3}, Landroid/widget/EditText;->setInputType(I)V

    .line 346
    if-ltz v1, :cond_0

    .line 348
    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setSelection(I)V

    .line 352
    :cond_0
    iget-object v0, p0, Lcom/osp/app/signin/gm;->b:Lcom/osp/app/signin/PasswordChangeView;

    invoke-virtual {v0, v5}, Lcom/osp/app/signin/PasswordChangeView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/EditText;

    .line 353
    if-eqz v0, :cond_1

    .line 355
    invoke-virtual {v0}, Landroid/widget/EditText;->getSelectionEnd()I

    move-result v1

    .line 356
    invoke-virtual {v0, v3}, Landroid/widget/EditText;->setInputType(I)V

    .line 357
    if-ltz v1, :cond_1

    .line 359
    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setSelection(I)V

    .line 362
    :cond_1
    iget-object v0, p0, Lcom/osp/app/signin/gm;->b:Lcom/osp/app/signin/PasswordChangeView;

    invoke-virtual {v0, v6}, Lcom/osp/app/signin/PasswordChangeView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/EditText;

    .line 363
    if-eqz v0, :cond_2

    .line 365
    invoke-virtual {v0}, Landroid/widget/EditText;->getSelectionEnd()I

    move-result v1

    .line 366
    invoke-virtual {v0, v3}, Landroid/widget/EditText;->setInputType(I)V

    .line 367
    if-ltz v1, :cond_2

    .line 369
    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setSelection(I)V

    .line 405
    :cond_2
    :goto_0
    return-void

    .line 374
    :cond_3
    iget-object v0, p0, Lcom/osp/app/signin/gm;->b:Lcom/osp/app/signin/PasswordChangeView;

    invoke-virtual {v0, v4}, Lcom/osp/app/signin/PasswordChangeView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/EditText;

    .line 375
    if-eqz v0, :cond_4

    .line 377
    invoke-virtual {v0}, Landroid/widget/EditText;->getSelectionEnd()I

    move-result v1

    .line 378
    invoke-virtual {v0, v2}, Landroid/widget/EditText;->setInputType(I)V

    .line 379
    if-ltz v1, :cond_4

    .line 381
    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setSelection(I)V

    .line 384
    :cond_4
    iget-object v0, p0, Lcom/osp/app/signin/gm;->b:Lcom/osp/app/signin/PasswordChangeView;

    invoke-virtual {v0, v5}, Lcom/osp/app/signin/PasswordChangeView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/EditText;

    .line 385
    if-eqz v0, :cond_5

    .line 387
    invoke-virtual {v0}, Landroid/widget/EditText;->getSelectionEnd()I

    move-result v1

    .line 388
    invoke-virtual {v0, v2}, Landroid/widget/EditText;->setInputType(I)V

    .line 389
    if-ltz v1, :cond_5

    .line 391
    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setSelection(I)V

    .line 394
    :cond_5
    iget-object v0, p0, Lcom/osp/app/signin/gm;->b:Lcom/osp/app/signin/PasswordChangeView;

    invoke-virtual {v0, v6}, Lcom/osp/app/signin/PasswordChangeView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/EditText;

    .line 395
    if-eqz v0, :cond_2

    .line 397
    invoke-virtual {v0}, Landroid/widget/EditText;->getSelectionEnd()I

    move-result v1

    .line 398
    invoke-virtual {v0, v2}, Landroid/widget/EditText;->setInputType(I)V

    .line 399
    if-ltz v1, :cond_2

    .line 401
    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setSelection(I)V

    goto :goto_0
.end method
