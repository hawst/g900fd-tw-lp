.class final Lcom/osp/app/signin/oe;
.super Lcom/msc/c/b;
.source "TnCView.java"


# instance fields
.field protected c:Ljava/lang/Thread;

.field protected d:Ljava/lang/Thread;

.field protected e:Ljava/lang/Runnable;

.field protected f:Ljava/lang/Runnable;

.field final synthetic g:Lcom/osp/app/signin/TnCView;

.field private h:J

.field private i:J

.field private j:Z

.field private k:Z

.field private l:Ljava/lang/String;

.field private m:Ljava/lang/String;

.field private n:Ljava/lang/String;

.field private o:Z

.field private p:Z


# direct methods
.method public constructor <init>(Lcom/osp/app/signin/TnCView;)V
    .locals 2

    .prologue
    const/4 v0, 0x0

    .line 5408
    iput-object p1, p0, Lcom/osp/app/signin/oe;->g:Lcom/osp/app/signin/TnCView;

    .line 5409
    invoke-direct {p0, p1}, Lcom/msc/c/b;-><init>(Landroid/content/Context;)V

    .line 5393
    iput-boolean v0, p0, Lcom/osp/app/signin/oe;->j:Z

    .line 5394
    iput-boolean v0, p0, Lcom/osp/app/signin/oe;->k:Z

    .line 5402
    iput-boolean v0, p0, Lcom/osp/app/signin/oe;->o:Z

    .line 5403
    iput-boolean v0, p0, Lcom/osp/app/signin/oe;->p:Z

    .line 5411
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v0, "TNC"

    const-string v1, "GetUserid_SpecialTermsListTask"

    invoke-static {v0, v1}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 5418
    invoke-static {p1}, Lcom/osp/app/signin/TnCView;->q(Lcom/osp/app/signin/TnCView;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/osp/common/util/i;->d(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/osp/app/signin/oe;->m:Ljava/lang/String;

    .line 5419
    iget-object v0, p0, Lcom/osp/app/signin/oe;->m:Ljava/lang/String;

    if-nez v0, :cond_0

    .line 5421
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v0, "TNC"

    const-string v1, "GetSpecialTermsListTask - country is null"

    invoke-static {v0, v1}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 5424
    const-string v0, ""

    iput-object v0, p0, Lcom/osp/app/signin/oe;->m:Ljava/lang/String;

    .line 5427
    :cond_0
    invoke-static {}, Lcom/osp/common/util/i;->a()Lcom/osp/common/util/i;

    invoke-static {p1}, Lcom/osp/common/util/i;->n(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/osp/app/signin/oe;->n:Ljava/lang/String;

    .line 5428
    iget-object v0, p0, Lcom/osp/app/signin/oe;->n:Ljava/lang/String;

    if-nez v0, :cond_1

    .line 5430
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v0, "TNC"

    const-string v1, "GetSpecialTermsListTask - language is null"

    invoke-static {v0, v1}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 5431
    const-string v0, ""

    iput-object v0, p0, Lcom/osp/app/signin/oe;->n:Ljava/lang/String;

    .line 5434
    :cond_1
    new-instance v0, Lcom/osp/app/signin/of;

    invoke-direct {v0, p0, p1}, Lcom/osp/app/signin/of;-><init>(Lcom/osp/app/signin/oe;Lcom/osp/app/signin/TnCView;)V

    iput-object v0, p0, Lcom/osp/app/signin/oe;->e:Ljava/lang/Runnable;

    .line 5444
    new-instance v0, Lcom/osp/app/signin/og;

    invoke-direct {v0, p0, p1}, Lcom/osp/app/signin/og;-><init>(Lcom/osp/app/signin/oe;Lcom/osp/app/signin/TnCView;)V

    iput-object v0, p0, Lcom/osp/app/signin/oe;->f:Ljava/lang/Runnable;

    .line 5455
    new-instance v0, Ljava/lang/Thread;

    iget-object v1, p0, Lcom/osp/app/signin/oe;->e:Ljava/lang/Runnable;

    invoke-direct {v0, v1}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;)V

    iput-object v0, p0, Lcom/osp/app/signin/oe;->c:Ljava/lang/Thread;

    .line 5456
    new-instance v0, Ljava/lang/Thread;

    iget-object v1, p0, Lcom/osp/app/signin/oe;->f:Ljava/lang/Runnable;

    invoke-direct {v0, v1}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;)V

    iput-object v0, p0, Lcom/osp/app/signin/oe;->d:Ljava/lang/Thread;

    .line 5459
    return-void
.end method

.method static synthetic a(Lcom/osp/app/signin/oe;)V
    .locals 3

    .prologue
    .line 5388
    const-string v0, ""

    iget-object v1, p0, Lcom/osp/app/signin/oe;->g:Lcom/osp/app/signin/TnCView;

    invoke-static {v1}, Lcom/osp/app/signin/TnCView;->S(Lcom/osp/app/signin/TnCView;)Lcom/osp/app/signin/SignUpinfo;

    move-result-object v1

    if-eqz v1, :cond_0

    iget-object v0, p0, Lcom/osp/app/signin/oe;->g:Lcom/osp/app/signin/TnCView;

    invoke-static {v0}, Lcom/osp/app/signin/TnCView;->S(Lcom/osp/app/signin/TnCView;)Lcom/osp/app/signin/SignUpinfo;

    move-result-object v0

    invoke-virtual {v0}, Lcom/osp/app/signin/SignUpinfo;->e()Ljava/lang/String;

    move-result-object v0

    :cond_0
    invoke-static {}, Lcom/msc/c/g;->a()Lcom/msc/c/g;

    iget-object v1, p0, Lcom/osp/app/signin/oe;->g:Lcom/osp/app/signin/TnCView;

    invoke-static {v1, v0, p0}, Lcom/msc/c/g;->i(Landroid/content/Context;Ljava/lang/String;Lcom/msc/b/h;)J

    move-result-wide v0

    iput-wide v0, p0, Lcom/osp/app/signin/oe;->i:J

    iget-wide v0, p0, Lcom/osp/app/signin/oe;->i:J

    invoke-virtual {p0, v0, v1}, Lcom/osp/app/signin/oe;->a(J)V

    iget-wide v0, p0, Lcom/osp/app/signin/oe;->i:J

    const-string v2, "from_xml"

    invoke-virtual {p0, v0, v1, v2}, Lcom/osp/app/signin/oe;->a(JLjava/lang/String;)V

    iget-wide v0, p0, Lcom/osp/app/signin/oe;->i:J

    sget-object v2, Lcom/msc/b/g;->c:Lcom/msc/b/g;

    invoke-static {v0, v1, v2}, Lcom/msc/c/g;->a(JLcom/msc/b/g;)V

    return-void
.end method

.method private static a(Ljava/lang/Thread;)V
    .locals 2

    .prologue
    .line 5497
    if-eqz p0, :cond_0

    invoke-virtual {p0}, Ljava/lang/Thread;->isAlive()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 5501
    const-wide/16 v0, 0x12c

    :try_start_0
    invoke-virtual {p0, v0, v1}, Ljava/lang/Thread;->join(J)V
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0

    .line 5506
    :cond_0
    :goto_0
    return-void

    .line 5502
    :catch_0
    move-exception v0

    invoke-virtual {v0}, Ljava/lang/InterruptedException;->printStackTrace()V

    goto :goto_0
.end method

.method static synthetic b(Lcom/osp/app/signin/oe;)Z
    .locals 1

    .prologue
    .line 5388
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/osp/app/signin/oe;->o:Z

    return v0
.end method

.method static synthetic c(Lcom/osp/app/signin/oe;)V
    .locals 3

    .prologue
    .line 5388
    invoke-static {}, Lcom/msc/c/g;->a()Lcom/msc/c/g;

    iget-object v0, p0, Lcom/osp/app/signin/oe;->g:Lcom/osp/app/signin/TnCView;

    iget-object v1, p0, Lcom/osp/app/signin/oe;->m:Ljava/lang/String;

    iget-object v2, p0, Lcom/osp/app/signin/oe;->n:Ljava/lang/String;

    invoke-static {v0, v1, v2, p0}, Lcom/msc/c/g;->g(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Lcom/msc/b/h;)J

    move-result-wide v0

    iput-wide v0, p0, Lcom/osp/app/signin/oe;->h:J

    iget-wide v0, p0, Lcom/osp/app/signin/oe;->h:J

    invoke-virtual {p0, v0, v1}, Lcom/osp/app/signin/oe;->a(J)V

    iget-wide v0, p0, Lcom/osp/app/signin/oe;->h:J

    sget-object v2, Lcom/msc/b/g;->c:Lcom/msc/b/g;

    invoke-static {v0, v1, v2}, Lcom/msc/c/g;->a(JLcom/msc/b/g;)V

    return-void
.end method

.method static synthetic d(Lcom/osp/app/signin/oe;)Z
    .locals 1

    .prologue
    .line 5388
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/osp/app/signin/oe;->p:Z

    return v0
.end method


# virtual methods
.method protected final varargs a()Ljava/lang/Boolean;
    .locals 3

    .prologue
    .line 5512
    iget-object v0, p0, Lcom/osp/app/signin/oe;->g:Lcom/osp/app/signin/TnCView;

    invoke-static {v0}, Lcom/osp/app/signin/TnCView;->R(Lcom/osp/app/signin/TnCView;)V

    .line 5516
    iget-object v0, p0, Lcom/osp/app/signin/oe;->c:Ljava/lang/Thread;

    invoke-virtual {v0}, Ljava/lang/Thread;->start()V

    .line 5517
    iget-object v0, p0, Lcom/osp/app/signin/oe;->d:Ljava/lang/Thread;

    invoke-virtual {v0}, Ljava/lang/Thread;->start()V

    .line 5519
    :cond_0
    iget-boolean v0, p0, Lcom/osp/app/signin/oe;->o:Z

    if-eqz v0, :cond_1

    iget-boolean v0, p0, Lcom/osp/app/signin/oe;->p:Z

    if-nez v0, :cond_2

    .line 5521
    :cond_1
    invoke-virtual {p0}, Lcom/osp/app/signin/oe;->isCancelled()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 5523
    :cond_2
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v0, "TNC"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "mResultUserid : "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-boolean v2, p0, Lcom/osp/app/signin/oe;->j:Z

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "| mResultSpecialTermsList : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-boolean v2, p0, Lcom/osp/app/signin/oe;->k:Z

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 5528
    iget-boolean v0, p0, Lcom/osp/app/signin/oe;->j:Z

    if-eqz v0, :cond_3

    iget-boolean v0, p0, Lcom/osp/app/signin/oe;->k:Z

    if-eqz v0, :cond_3

    .line 5530
    const-string v0, "Success"

    iput-object v0, p0, Lcom/osp/app/signin/oe;->l:Ljava/lang/String;

    .line 5536
    :goto_0
    const/4 v0, 0x0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    return-object v0

    .line 5533
    :cond_3
    const-string v0, "Fail"

    iput-object v0, p0, Lcom/osp/app/signin/oe;->l:Ljava/lang/String;

    goto :goto_0
.end method

.method public final declared-synchronized a(Lcom/msc/b/c;)V
    .locals 6

    .prologue
    .line 5610
    monitor-enter p0

    :try_start_0
    invoke-super {p0, p1}, Lcom/msc/c/b;->a(Lcom/msc/b/c;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 5612
    if-nez p1, :cond_1

    .line 5658
    :cond_0
    :goto_0
    monitor-exit p0

    return-void

    .line 5617
    :cond_1
    :try_start_1
    invoke-virtual {p1}, Lcom/msc/b/c;->a()J

    move-result-wide v0

    .line 5618
    invoke-virtual {p1}, Lcom/msc/b/c;->e()Ljava/lang/String;

    move-result-object v2

    .line 5620
    iget-wide v4, p0, Lcom/osp/app/signin/oe;->h:J
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    cmp-long v3, v0, v4

    if-nez v3, :cond_2

    .line 5624
    :try_start_2
    iget-object v0, p0, Lcom/osp/app/signin/oe;->g:Lcom/osp/app/signin/TnCView;

    invoke-static {}, Lcom/msc/c/h;->a()Lcom/msc/c/h;

    invoke-static {v2}, Lcom/msc/c/h;->x(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/osp/app/signin/TnCView;->a(Lcom/osp/app/signin/TnCView;Ljava/util/ArrayList;)Ljava/util/ArrayList;

    .line 5625
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v0, "TNC"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "GetSpecialTermsListTask termsList size = "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, Lcom/osp/app/signin/oe;->g:Lcom/osp/app/signin/TnCView;

    invoke-static {v2}, Lcom/osp/app/signin/TnCView;->p(Lcom/osp/app/signin/TnCView;)Ljava/util/ArrayList;

    move-result-object v2

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 5626
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/osp/app/signin/oe;->k:Z
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_0
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0

    .line 5629
    :catch_0
    move-exception v0

    .line 5631
    :try_start_3
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    .line 5632
    const/4 v1, 0x0

    iput-boolean v1, p0, Lcom/osp/app/signin/oe;->k:Z

    .line 5633
    const-string v1, "Fail"

    iput-object v1, p0, Lcom/osp/app/signin/oe;->l:Ljava/lang/String;

    .line 5634
    new-instance v1, Lcom/msc/c/f;

    invoke-direct {v1, v0}, Lcom/msc/c/f;-><init>(Ljava/lang/Exception;)V

    iput-object v1, p0, Lcom/osp/app/signin/oe;->b:Lcom/msc/c/f;
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    goto :goto_0

    .line 5610
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 5636
    :cond_2
    :try_start_4
    iget-wide v4, p0, Lcom/osp/app/signin/oe;->i:J
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    cmp-long v0, v0, v4

    if-nez v0, :cond_0

    .line 5640
    :try_start_5
    invoke-static {}, Lcom/msc/c/h;->a()Lcom/msc/c/h;

    invoke-static {v2}, Lcom/msc/c/h;->h(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 5641
    if-eqz v0, :cond_3

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v0

    if-nez v0, :cond_3

    .line 5644
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/osp/app/signin/oe;->j:Z
    :try_end_5
    .catch Ljava/lang/Exception; {:try_start_5 .. :try_end_5} :catch_1
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    goto :goto_0

    .line 5650
    :catch_1
    move-exception v0

    .line 5652
    :try_start_6
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    .line 5653
    const/4 v1, 0x0

    iput-boolean v1, p0, Lcom/osp/app/signin/oe;->j:Z

    .line 5654
    const-string v1, "Fail"

    iput-object v1, p0, Lcom/osp/app/signin/oe;->l:Ljava/lang/String;

    .line 5655
    new-instance v1, Lcom/msc/c/f;

    invoke-direct {v1, v0}, Lcom/msc/c/f;-><init>(Ljava/lang/Exception;)V

    iput-object v1, p0, Lcom/osp/app/signin/oe;->b:Lcom/msc/c/f;
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_0

    goto/16 :goto_0

    .line 5647
    :cond_3
    :try_start_7
    const-string v0, "Fail"

    iput-object v0, p0, Lcom/osp/app/signin/oe;->l:Ljava/lang/String;

    .line 5648
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/osp/app/signin/oe;->j:Z
    :try_end_7
    .catch Ljava/lang/Exception; {:try_start_7 .. :try_end_7} :catch_1
    .catchall {:try_start_7 .. :try_end_7} :catchall_0

    goto/16 :goto_0
.end method

.method protected final a(Ljava/lang/Boolean;)V
    .locals 9

    .prologue
    const/16 v4, 0xe

    const/4 v8, 0x3

    const/4 v7, 0x1

    const/4 v2, 0x0

    .line 5541
    invoke-super {p0, p1}, Lcom/msc/c/b;->a(Ljava/lang/Boolean;)V

    .line 5543
    iget-object v0, p0, Lcom/osp/app/signin/oe;->g:Lcom/osp/app/signin/TnCView;

    invoke-static {v0}, Lcom/osp/app/signin/TnCView;->P(Lcom/osp/app/signin/TnCView;)Landroid/app/ProgressDialog;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 5545
    iget-object v0, p0, Lcom/osp/app/signin/oe;->g:Lcom/osp/app/signin/TnCView;

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lcom/osp/app/signin/TnCView;->a(Lcom/osp/app/signin/TnCView;Landroid/app/ProgressDialog;)Landroid/app/ProgressDialog;

    .line 5548
    :cond_0
    const-string v0, "Success"

    iget-object v1, p0, Lcom/osp/app/signin/oe;->l:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_5

    iget-object v0, p0, Lcom/osp/app/signin/oe;->g:Lcom/osp/app/signin/TnCView;

    invoke-static {v0}, Lcom/osp/app/signin/TnCView;->p(Lcom/osp/app/signin/TnCView;)Ljava/util/ArrayList;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_5

    .line 5550
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v0, "TNC"

    const-string v1, "GetSpecialTermsListTask Success"

    invoke-static {v0, v1}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 5552
    iget-object v0, p0, Lcom/osp/app/signin/oe;->g:Lcom/osp/app/signin/TnCView;

    invoke-static {v0}, Lcom/osp/app/signin/TnCView;->p(Lcom/osp/app/signin/TnCView;)Ljava/util/ArrayList;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v3

    move v1, v2

    .line 5553
    :goto_0
    if-ge v1, v3, :cond_1

    .line 5555
    iget-object v0, p0, Lcom/osp/app/signin/oe;->g:Lcom/osp/app/signin/TnCView;

    invoke-static {v0}, Lcom/osp/app/signin/TnCView;->p(Lcom/osp/app/signin/TnCView;)Ljava/util/ArrayList;

    move-result-object v0

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Ljava/lang/String;

    .line 5556
    aget-object v4, v0, v2

    const-string v5, "/"

    invoke-virtual {v4, v5}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v4

    .line 5557
    if-eqz v4, :cond_4

    array-length v5, v4

    const/4 v6, 0x4

    if-lt v5, v6, :cond_4

    aget-object v5, v4, v8

    if-eqz v5, :cond_4

    aget-object v4, v4, v8

    const-string v5, "general.txt"

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_4

    .line 5559
    iget-object v1, p0, Lcom/osp/app/signin/oe;->g:Lcom/osp/app/signin/TnCView;

    const v3, 0x7f090059

    invoke-virtual {v1, v3}, Lcom/osp/app/signin/TnCView;->getString(I)Ljava/lang/String;

    move-result-object v1

    aput-object v1, v0, v7

    .line 5564
    :cond_1
    iget-object v0, p0, Lcom/osp/app/signin/oe;->g:Lcom/osp/app/signin/TnCView;

    new-instance v1, Lcom/osp/app/signin/op;

    iget-object v3, p0, Lcom/osp/app/signin/oe;->g:Lcom/osp/app/signin/TnCView;

    invoke-direct {v1, v3}, Lcom/osp/app/signin/op;-><init>(Lcom/osp/app/signin/TnCView;)V

    invoke-static {v0, v1}, Lcom/osp/app/signin/TnCView;->a(Lcom/osp/app/signin/TnCView;Lcom/osp/app/signin/op;)Lcom/osp/app/signin/op;

    .line 5565
    iget-object v0, p0, Lcom/osp/app/signin/oe;->g:Lcom/osp/app/signin/TnCView;

    invoke-static {v0}, Lcom/osp/app/signin/TnCView;->B(Lcom/osp/app/signin/TnCView;)Landroid/widget/ListView;

    move-result-object v0

    iget-object v1, p0, Lcom/osp/app/signin/oe;->g:Lcom/osp/app/signin/TnCView;

    invoke-static {v1}, Lcom/osp/app/signin/TnCView;->w(Lcom/osp/app/signin/TnCView;)Lcom/osp/app/signin/op;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 5566
    iget-object v0, p0, Lcom/osp/app/signin/oe;->g:Lcom/osp/app/signin/TnCView;

    invoke-static {v0}, Lcom/osp/app/signin/TnCView;->B(Lcom/osp/app/signin/TnCView;)Landroid/widget/ListView;

    move-result-object v0

    new-instance v1, Lcom/osp/app/signin/oi;

    iget-object v3, p0, Lcom/osp/app/signin/oe;->g:Lcom/osp/app/signin/TnCView;

    invoke-direct {v1, v3, v2}, Lcom/osp/app/signin/oi;-><init>(Lcom/osp/app/signin/TnCView;B)V

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V

    .line 5567
    iget-object v0, p0, Lcom/osp/app/signin/oe;->g:Lcom/osp/app/signin/TnCView;

    new-instance v1, Lcom/osp/app/signin/oj;

    iget-object v2, p0, Lcom/osp/app/signin/oe;->g:Lcom/osp/app/signin/TnCView;

    invoke-direct {v1, v2}, Lcom/osp/app/signin/oj;-><init>(Lcom/osp/app/signin/TnCView;)V

    invoke-static {v0, v1}, Lcom/osp/app/signin/TnCView;->a(Lcom/osp/app/signin/TnCView;Lcom/osp/app/signin/oj;)Lcom/osp/app/signin/oj;

    .line 5569
    iget-object v0, p0, Lcom/osp/app/signin/oe;->g:Lcom/osp/app/signin/TnCView;

    iget-object v1, p0, Lcom/osp/app/signin/oe;->g:Lcom/osp/app/signin/TnCView;

    invoke-static {v1}, Lcom/osp/app/signin/TnCView;->p(Lcom/osp/app/signin/TnCView;)Ljava/util/ArrayList;

    move-result-object v1

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    new-array v1, v1, [Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/osp/app/signin/TnCView;->a(Lcom/osp/app/signin/TnCView;[Ljava/lang/String;)[Ljava/lang/String;

    .line 5571
    iget-object v0, p0, Lcom/osp/app/signin/oe;->g:Lcom/osp/app/signin/TnCView;

    invoke-static {v0}, Lcom/osp/app/signin/TnCView;->K(Lcom/osp/app/signin/TnCView;)Z

    move-result v0

    if-nez v0, :cond_3

    iget-object v0, p0, Lcom/osp/app/signin/oe;->g:Lcom/osp/app/signin/TnCView;

    invoke-static {v0}, Lcom/osp/app/signin/TnCView;->D(Lcom/osp/app/signin/TnCView;)Z

    move-result v0

    if-nez v0, :cond_2

    iget-object v0, p0, Lcom/osp/app/signin/oe;->g:Lcom/osp/app/signin/TnCView;

    invoke-static {v0}, Lcom/osp/app/signin/TnCView;->L(Lcom/osp/app/signin/TnCView;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 5573
    :cond_2
    iget-object v0, p0, Lcom/osp/app/signin/oe;->g:Lcom/osp/app/signin/TnCView;

    invoke-static {v0}, Lcom/osp/app/signin/TnCView;->M(Lcom/osp/app/signin/TnCView;)V

    .line 5606
    :cond_3
    :goto_1
    return-void

    .line 5553
    :cond_4
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto/16 :goto_0

    .line 5575
    :cond_5
    const-string v0, "Fail"

    iget-object v1, p0, Lcom/osp/app/signin/oe;->l:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_8

    iget-boolean v0, p0, Lcom/osp/app/signin/oe;->j:Z

    if-nez v0, :cond_8

    .line 5578
    iget-object v0, p0, Lcom/osp/app/signin/oe;->b:Lcom/msc/c/f;

    if-eqz v0, :cond_7

    .line 5580
    invoke-virtual {p0, v2}, Lcom/osp/app/signin/oe;->a(Z)V

    .line 5586
    :goto_2
    iget-object v0, p0, Lcom/osp/app/signin/oe;->g:Lcom/osp/app/signin/TnCView;

    invoke-virtual {v0}, Lcom/osp/app/signin/TnCView;->getIntent()Landroid/content/Intent;

    move-result-object v0

    .line 5587
    if-eqz v0, :cond_6

    const-string v1, "is_cancelable_just_one_activity"

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v0

    if-eqz v0, :cond_6

    .line 5589
    iget-object v0, p0, Lcom/osp/app/signin/oe;->g:Lcom/osp/app/signin/TnCView;

    iget-object v1, p0, Lcom/osp/app/signin/oe;->g:Lcom/osp/app/signin/TnCView;

    invoke-static {v1}, Lcom/osp/app/signin/TnCView;->c(Lcom/osp/app/signin/TnCView;)Landroid/content/Intent;

    move-result-object v1

    invoke-virtual {v0, v4, v1}, Lcom/osp/app/signin/TnCView;->a(ILandroid/content/Intent;)V

    .line 5591
    :cond_6
    iget-object v0, p0, Lcom/osp/app/signin/oe;->g:Lcom/osp/app/signin/TnCView;

    invoke-virtual {v0}, Lcom/osp/app/signin/TnCView;->finish()V

    goto :goto_1

    .line 5583
    :cond_7
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v0, "TNC"

    const-string v1, "GetUserIDTask - Account already exists."

    invoke-static {v0, v1}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 5584
    invoke-static {}, Lcom/osp/app/util/ap;->a()Lcom/osp/app/util/ap;

    iget-object v0, p0, Lcom/osp/app/signin/oe;->g:Lcom/osp/app/signin/TnCView;

    iget-object v1, p0, Lcom/osp/app/signin/oe;->g:Lcom/osp/app/signin/TnCView;

    const v3, 0x7f090015

    invoke-virtual {v1, v3}, Lcom/osp/app/signin/TnCView;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1, v7}, Lcom/osp/app/util/ap;->a(Landroid/content/Context;Ljava/lang/String;I)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    goto :goto_2

    .line 5593
    :cond_8
    const-string v0, "Fail"

    iget-object v1, p0, Lcom/osp/app/signin/oe;->l:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    iget-boolean v0, p0, Lcom/osp/app/signin/oe;->k:Z

    if-nez v0, :cond_3

    .line 5595
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v0, "TNC"

    const-string v1, "GetSpecialTermsListTask Fail"

    invoke-static {v0, v1}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 5597
    invoke-virtual {p0, v2}, Lcom/osp/app/signin/oe;->a(Z)V

    .line 5599
    iget-object v0, p0, Lcom/osp/app/signin/oe;->g:Lcom/osp/app/signin/TnCView;

    invoke-virtual {v0}, Lcom/osp/app/signin/TnCView;->getIntent()Landroid/content/Intent;

    move-result-object v0

    .line 5600
    if-eqz v0, :cond_9

    const-string v1, "is_cancelable_just_one_activity"

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v0

    if-eqz v0, :cond_9

    .line 5602
    iget-object v0, p0, Lcom/osp/app/signin/oe;->g:Lcom/osp/app/signin/TnCView;

    iget-object v1, p0, Lcom/osp/app/signin/oe;->g:Lcom/osp/app/signin/TnCView;

    invoke-static {v1}, Lcom/osp/app/signin/TnCView;->c(Lcom/osp/app/signin/TnCView;)Landroid/content/Intent;

    move-result-object v1

    invoke-virtual {v0, v4, v1}, Lcom/osp/app/signin/TnCView;->a(ILandroid/content/Intent;)V

    .line 5604
    :cond_9
    iget-object v0, p0, Lcom/osp/app/signin/oe;->g:Lcom/osp/app/signin/TnCView;

    invoke-virtual {v0}, Lcom/osp/app/signin/TnCView;->finish()V

    goto/16 :goto_1
.end method

.method public final declared-synchronized b(Lcom/msc/b/c;)V
    .locals 4

    .prologue
    .line 5662
    monitor-enter p0

    :try_start_0
    invoke-super {p0, p1}, Lcom/msc/c/b;->b(Lcom/msc/b/c;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 5664
    if-nez p1, :cond_1

    .line 5683
    :cond_0
    :goto_0
    monitor-exit p0

    return-void

    .line 5669
    :cond_1
    :try_start_1
    invoke-virtual {p1}, Lcom/msc/b/c;->a()J

    move-result-wide v0

    .line 5671
    iget-wide v2, p0, Lcom/osp/app/signin/oe;->i:J

    cmp-long v2, v0, v2

    if-nez v2, :cond_2

    .line 5674
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/osp/app/signin/oe;->j:Z

    .line 5675
    const-string v0, "Fail"

    iput-object v0, p0, Lcom/osp/app/signin/oe;->l:Ljava/lang/String;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 5662
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 5677
    :cond_2
    :try_start_2
    iget-wide v2, p0, Lcom/osp/app/signin/oe;->h:J

    cmp-long v0, v0, v2

    if-nez v0, :cond_0

    .line 5680
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/osp/app/signin/oe;->k:Z

    .line 5681
    const-string v0, "Fail"

    iput-object v0, p0, Lcom/osp/app/signin/oe;->l:Ljava/lang/String;
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0
.end method

.method protected final d()V
    .locals 3

    .prologue
    .line 5475
    invoke-super {p0}, Lcom/msc/c/b;->d()V

    .line 5480
    iget-object v0, p0, Lcom/osp/app/signin/oe;->c:Ljava/lang/Thread;

    invoke-static {v0}, Lcom/osp/app/signin/oe;->a(Ljava/lang/Thread;)V

    .line 5481
    iget-object v0, p0, Lcom/osp/app/signin/oe;->d:Ljava/lang/Thread;

    invoke-static {v0}, Lcom/osp/app/signin/oe;->a(Ljava/lang/Thread;)V

    .line 5483
    iget-object v0, p0, Lcom/osp/app/signin/oe;->g:Lcom/osp/app/signin/TnCView;

    invoke-virtual {v0}, Lcom/osp/app/signin/TnCView;->getIntent()Landroid/content/Intent;

    move-result-object v0

    .line 5484
    if-eqz v0, :cond_0

    const-string v1, "is_cancelable_just_one_activity"

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 5486
    iget-object v0, p0, Lcom/osp/app/signin/oe;->g:Lcom/osp/app/signin/TnCView;

    const/16 v1, 0xe

    iget-object v2, p0, Lcom/osp/app/signin/oe;->g:Lcom/osp/app/signin/TnCView;

    invoke-static {v2}, Lcom/osp/app/signin/TnCView;->c(Lcom/osp/app/signin/TnCView;)Landroid/content/Intent;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/osp/app/signin/TnCView;->a(ILandroid/content/Intent;)V

    .line 5488
    :cond_0
    iget-object v0, p0, Lcom/osp/app/signin/oe;->g:Lcom/osp/app/signin/TnCView;

    invoke-virtual {v0}, Lcom/osp/app/signin/TnCView;->finish()V

    .line 5489
    return-void
.end method

.method protected final synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 5388
    invoke-virtual {p0}, Lcom/osp/app/signin/oe;->a()Ljava/lang/Boolean;

    move-result-object v0

    return-object v0
.end method

.method protected final synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 5388
    check-cast p1, Ljava/lang/Boolean;

    invoke-virtual {p0, p1}, Lcom/osp/app/signin/oe;->a(Ljava/lang/Boolean;)V

    return-void
.end method

.method protected final onPreExecute()V
    .locals 1

    .prologue
    .line 5464
    iget-object v0, p0, Lcom/osp/app/signin/oe;->g:Lcom/osp/app/signin/TnCView;

    invoke-static {v0}, Lcom/osp/app/signin/TnCView;->P(Lcom/osp/app/signin/TnCView;)Landroid/app/ProgressDialog;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/osp/app/signin/oe;->g:Lcom/osp/app/signin/TnCView;

    invoke-static {v0}, Lcom/osp/app/signin/TnCView;->P(Lcom/osp/app/signin/TnCView;)Landroid/app/ProgressDialog;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/ProgressDialog;->isShowing()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 5466
    iget-object v0, p0, Lcom/osp/app/signin/oe;->g:Lcom/osp/app/signin/TnCView;

    invoke-static {v0}, Lcom/osp/app/signin/TnCView;->P(Lcom/osp/app/signin/TnCView;)Landroid/app/ProgressDialog;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/osp/app/signin/oe;->a(Landroid/app/ProgressDialog;)V

    .line 5471
    :goto_0
    return-void

    .line 5469
    :cond_0
    invoke-super {p0}, Lcom/msc/c/b;->onPreExecute()V

    goto :goto_0
.end method
