.class final Lcom/osp/app/signin/ax;
.super Lcom/msc/c/b;
.source "AccountView.java"


# instance fields
.field final synthetic c:Lcom/osp/app/signin/AccountView;

.field private d:I

.field private final e:I


# direct methods
.method public constructor <init>(Lcom/osp/app/signin/AccountView;I)V
    .locals 1

    .prologue
    .line 8823
    iput-object p1, p0, Lcom/osp/app/signin/ax;->c:Lcom/osp/app/signin/AccountView;

    .line 8824
    invoke-direct {p0, p1}, Lcom/msc/c/b;-><init>(Landroid/content/Context;)V

    .line 8812
    const/4 v0, 0x0

    iput v0, p0, Lcom/osp/app/signin/ax;->d:I

    .line 8826
    iput p2, p0, Lcom/osp/app/signin/ax;->e:I

    .line 8827
    return-void
.end method


# virtual methods
.method protected final varargs a()Ljava/lang/Boolean;
    .locals 4

    .prologue
    .line 8836
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v0, "AV"

    const-string v1, "AdditionalNetworkCheckTask start"

    invoke-static {v0, v1}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 8840
    :try_start_0
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v0, "AV"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "AdditionalNetworkCheckTask START : "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    invoke-virtual {v1, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 8842
    new-instance v0, Ljava/net/URL;

    invoke-static {}, Lcom/msc/c/k;->a()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/net/URL;-><init>(Ljava/lang/String;)V

    .line 8843
    invoke-virtual {v0}, Ljava/net/URL;->openConnection()Ljava/net/URLConnection;

    move-result-object v0

    check-cast v0, Ljava/net/HttpURLConnection;

    .line 8845
    if-eqz v0, :cond_0

    .line 8847
    const/16 v1, 0x7d0

    invoke-virtual {v0, v1}, Ljava/net/HttpURLConnection;->setReadTimeout(I)V

    .line 8848
    const/16 v1, 0x7d0

    invoke-virtual {v0, v1}, Ljava/net/HttpURLConnection;->setConnectTimeout(I)V

    .line 8850
    invoke-virtual {v0}, Ljava/net/HttpURLConnection;->getResponseCode()I

    move-result v1

    iput v1, p0, Lcom/osp/app/signin/ax;->d:I

    .line 8851
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v1, "AV"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "AdditionalNetworkCheckTask responseCode = ["

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v3, p0, Lcom/osp/app/signin/ax;->d:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "]"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 8853
    invoke-virtual {v0}, Ljava/net/HttpURLConnection;->disconnect()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 8861
    :cond_0
    :goto_0
    const/4 v0, 0x0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    return-object v0

    .line 8856
    :catch_0
    move-exception v0

    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_0
.end method

.method protected final a(Ljava/lang/Boolean;)V
    .locals 4

    .prologue
    .line 8866
    invoke-super {p0, p1}, Lcom/msc/c/b;->a(Ljava/lang/Boolean;)V

    .line 8868
    iget v0, p0, Lcom/osp/app/signin/ax;->d:I

    const/16 v1, 0xc8

    if-eq v0, v1, :cond_0

    .line 8871
    iget-object v0, p0, Lcom/osp/app/signin/ax;->c:Lcom/osp/app/signin/AccountView;

    const/16 v1, 0xe0

    invoke-virtual {v0, v1}, Lcom/osp/app/signin/AccountView;->c(I)V

    .line 8902
    :goto_0
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v0, "AV"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "AdditionalNetworkCheckTask Result  : "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v2, p0, Lcom/osp/app/signin/ax;->d:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 8903
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v0, "AV"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "AdditionalNetworkCheckTask END : "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    invoke-virtual {v1, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 8905
    return-void

    .line 8878
    :cond_0
    iget-object v0, p0, Lcom/osp/app/signin/ax;->c:Lcom/osp/app/signin/AccountView;

    invoke-static {v0}, Lcom/osp/app/signin/AccountView;->Y(Lcom/osp/app/signin/AccountView;)Lcom/osp/app/signin/bc;

    move-result-object v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/osp/app/signin/ax;->c:Lcom/osp/app/signin/AccountView;

    invoke-static {v0}, Lcom/osp/app/signin/AccountView;->Y(Lcom/osp/app/signin/AccountView;)Lcom/osp/app/signin/bc;

    move-result-object v0

    invoke-virtual {v0}, Lcom/osp/app/signin/bc;->getStatus()Landroid/os/AsyncTask$Status;

    move-result-object v0

    sget-object v1, Landroid/os/AsyncTask$Status;->RUNNING:Landroid/os/AsyncTask$Status;

    if-ne v0, v1, :cond_1

    .line 8880
    iget-object v0, p0, Lcom/osp/app/signin/ax;->c:Lcom/osp/app/signin/AccountView;

    invoke-static {v0}, Lcom/osp/app/signin/AccountView;->Y(Lcom/osp/app/signin/AccountView;)Lcom/osp/app/signin/bc;

    move-result-object v0

    invoke-virtual {v0}, Lcom/osp/app/signin/bc;->d()V

    .line 8881
    iget-object v0, p0, Lcom/osp/app/signin/ax;->c:Lcom/osp/app/signin/AccountView;

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lcom/osp/app/signin/AccountView;->a(Lcom/osp/app/signin/AccountView;Lcom/osp/app/signin/bc;)Lcom/osp/app/signin/bc;

    .line 8884
    :cond_1
    iget-object v0, p0, Lcom/osp/app/signin/ax;->c:Lcom/osp/app/signin/AccountView;

    new-instance v1, Lcom/osp/app/signin/bc;

    iget-object v2, p0, Lcom/osp/app/signin/ax;->c:Lcom/osp/app/signin/AccountView;

    iget v3, p0, Lcom/osp/app/signin/ax;->e:I

    invoke-direct {v1, v2, v3}, Lcom/osp/app/signin/bc;-><init>(Lcom/osp/app/signin/AccountView;I)V

    invoke-static {v0, v1}, Lcom/osp/app/signin/AccountView;->a(Lcom/osp/app/signin/AccountView;Lcom/osp/app/signin/bc;)Lcom/osp/app/signin/bc;

    .line 8885
    iget-object v0, p0, Lcom/osp/app/signin/ax;->c:Lcom/osp/app/signin/AccountView;

    invoke-static {v0}, Lcom/osp/app/signin/AccountView;->Y(Lcom/osp/app/signin/AccountView;)Lcom/osp/app/signin/bc;

    move-result-object v0

    invoke-static {v0}, Lcom/msc/sa/c/d;->a(Landroid/os/AsyncTask;)V

    goto :goto_0
.end method

.method protected final d()V
    .locals 0

    .prologue
    .line 8831
    invoke-super {p0}, Lcom/msc/c/b;->d()V

    .line 8832
    return-void
.end method

.method protected final synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 8807
    invoke-virtual {p0}, Lcom/osp/app/signin/ax;->a()Ljava/lang/Boolean;

    move-result-object v0

    return-object v0
.end method

.method protected final synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 8807
    check-cast p1, Ljava/lang/Boolean;

    invoke-virtual {p0, p1}, Lcom/osp/app/signin/ax;->a(Ljava/lang/Boolean;)V

    return-void
.end method
