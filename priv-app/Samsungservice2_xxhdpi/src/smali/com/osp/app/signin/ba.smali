.class final Lcom/osp/app/signin/ba;
.super Lcom/msc/c/b;
.source "AccountView.java"


# instance fields
.field final synthetic c:Lcom/osp/app/signin/AccountView;

.field private d:J

.field private e:J


# direct methods
.method public constructor <init>(Lcom/osp/app/signin/AccountView;Z)V
    .locals 0

    .prologue
    .line 6152
    iput-object p1, p0, Lcom/osp/app/signin/ba;->c:Lcom/osp/app/signin/AccountView;

    .line 6153
    invoke-direct {p0, p1}, Lcom/msc/c/b;-><init>(Landroid/content/Context;)V

    .line 6155
    if-eqz p2, :cond_0

    .line 6157
    invoke-virtual {p0}, Lcom/osp/app/signin/ba;->c()V

    .line 6162
    :cond_0
    return-void
.end method

.method private h()V
    .locals 4

    .prologue
    .line 6225
    new-instance v0, Lcom/msc/a/f;

    invoke-direct {v0}, Lcom/msc/a/f;-><init>()V

    .line 6227
    iget-object v1, p0, Lcom/osp/app/signin/ba;->c:Lcom/osp/app/signin/AccountView;

    invoke-static {v1}, Lcom/osp/app/util/ad;->h(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/msc/a/f;->d(Ljava/lang/String;)V

    .line 6228
    invoke-virtual {v0}, Lcom/msc/a/f;->e()V

    .line 6229
    iget-object v1, p0, Lcom/osp/app/signin/ba;->c:Lcom/osp/app/signin/AccountView;

    invoke-static {v1}, Lcom/osp/app/signin/AccountView;->w(Lcom/osp/app/signin/AccountView;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/msc/a/f;->h(Ljava/lang/String;)V

    .line 6232
    iget-object v1, p0, Lcom/osp/app/signin/ba;->c:Lcom/osp/app/signin/AccountView;

    invoke-static {v1}, Lcom/osp/app/signin/AccountView;->l(Lcom/osp/app/signin/AccountView;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/msc/a/f;->f(Ljava/lang/String;)V

    .line 6233
    iget-object v1, p0, Lcom/osp/app/signin/ba;->c:Lcom/osp/app/signin/AccountView;

    invoke-static {v1}, Lcom/osp/app/signin/AccountView;->x(Lcom/osp/app/signin/AccountView;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/msc/a/f;->g(Ljava/lang/String;)V

    .line 6234
    invoke-static {}, Lcom/osp/device/b;->b()Lcom/osp/device/b;

    move-result-object v1

    invoke-virtual {v1}, Lcom/osp/device/b;->a()Z

    move-result v1

    if-eqz v1, :cond_1

    iget-object v1, p0, Lcom/osp/app/signin/ba;->c:Lcom/osp/app/signin/AccountView;

    invoke-static {v1}, Lcom/osp/app/util/ad;->h(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Landroid/telephony/PhoneNumberUtils;->isGlobalPhoneNumber(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 6236
    iget-object v1, p0, Lcom/osp/app/signin/ba;->c:Lcom/osp/app/signin/AccountView;

    invoke-static {v1}, Lcom/msc/c/e;->h(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v1

    .line 6237
    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 6239
    invoke-virtual {v0, v1}, Lcom/msc/a/f;->c(Ljava/lang/String;)V

    .line 6242
    :cond_0
    const-string v1, "Y"

    invoke-virtual {v0, v1}, Lcom/msc/a/f;->b(Ljava/lang/String;)V

    .line 6244
    :cond_1
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v1, "AV"

    const-string v2, "RequestCheckListInfo"

    const-string v3, "START"

    invoke-static {v1, v2, v3}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    invoke-static {}, Lcom/msc/c/g;->a()Lcom/msc/c/g;

    iget-object v1, p0, Lcom/osp/app/signin/ba;->c:Lcom/osp/app/signin/AccountView;

    invoke-static {v1, v0, p0}, Lcom/msc/c/g;->a(Landroid/content/Context;Lcom/msc/a/f;Lcom/msc/b/h;)J

    move-result-wide v0

    iput-wide v0, p0, Lcom/osp/app/signin/ba;->d:J

    iget-wide v0, p0, Lcom/osp/app/signin/ba;->d:J

    invoke-virtual {p0, v0, v1}, Lcom/osp/app/signin/ba;->a(J)V

    iget-wide v0, p0, Lcom/osp/app/signin/ba;->d:J

    const-string v2, "from_xml"

    invoke-virtual {p0, v0, v1, v2}, Lcom/osp/app/signin/ba;->a(JLjava/lang/String;)V

    iget-wide v0, p0, Lcom/osp/app/signin/ba;->d:J

    sget-object v2, Lcom/msc/b/g;->c:Lcom/msc/b/g;

    invoke-static {v0, v1, v2}, Lcom/msc/c/g;->a(JLcom/msc/b/g;)V

    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v0, "AV"

    const-string v1, "RequestCheckListInfo"

    const-string v2, "END"

    invoke-static {v0, v1, v2}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 6246
    return-void
.end method


# virtual methods
.method protected final varargs a()Ljava/lang/Boolean;
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 6177
    iget-object v0, p0, Lcom/osp/app/signin/ba;->c:Lcom/osp/app/signin/AccountView;

    iget-object v1, p0, Lcom/osp/app/signin/ba;->c:Lcom/osp/app/signin/AccountView;

    invoke-static {v1}, Lcom/osp/app/util/ad;->p(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/osp/app/signin/AccountView;->b(Lcom/osp/app/signin/AccountView;Ljava/lang/String;)Ljava/lang/String;

    .line 6179
    invoke-static {}, Lcom/osp/device/b;->b()Lcom/osp/device/b;

    move-result-object v0

    invoke-virtual {v0}, Lcom/osp/device/b;->l()I

    move-result v0

    const/16 v1, 0x10

    if-le v0, v1, :cond_2

    iget-object v0, p0, Lcom/osp/app/signin/ba;->c:Lcom/osp/app/signin/AccountView;

    invoke-static {v0}, Lcom/osp/app/signin/AccountView;->v(Lcom/osp/app/signin/AccountView;)I

    move-result v0

    const/4 v1, 0x3

    if-ne v0, v1, :cond_2

    .line 6184
    iget-object v0, p0, Lcom/osp/app/signin/ba;->c:Lcom/osp/app/signin/AccountView;

    invoke-static {v0}, Lcom/osp/app/signin/SamsungService;->a(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v0

    .line 6186
    if-eqz v0, :cond_1

    .line 6190
    :try_start_0
    invoke-static {}, Lcom/msc/c/h;->a()Lcom/msc/c/h;

    invoke-static {v0}, Lcom/msc/c/h;->A(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v0

    .line 6192
    invoke-static {}, Lcom/msc/c/h;->a()Lcom/msc/c/h;

    iget-object v1, p0, Lcom/osp/app/signin/ba;->c:Lcom/osp/app/signin/AccountView;

    iget-object v2, p0, Lcom/osp/app/signin/ba;->c:Lcom/osp/app/signin/AccountView;

    invoke-static {v2}, Lcom/osp/app/signin/AccountView;->l(Lcom/osp/app/signin/AccountView;)Ljava/lang/String;

    move-result-object v2

    iget-object v3, p0, Lcom/osp/app/signin/ba;->c:Lcom/osp/app/signin/AccountView;

    invoke-virtual {v3}, Lcom/osp/app/signin/AccountView;->getCallingPackage()Ljava/lang/String;

    move-result-object v3

    invoke-static {v1, v0, v2, v3}, Lcom/msc/c/h;->a(Landroid/content/Context;Ljava/util/ArrayList;Ljava/lang/String;Ljava/lang/String;)Z

    move-result v0

    .line 6194
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v1, "AV"

    const-string v2, "used cache signature"

    invoke-static {v1, v2}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 6196
    if-eqz v0, :cond_0

    .line 6198
    invoke-direct {p0}, Lcom/osp/app/signin/ba;->h()V

    .line 6199
    const/4 v0, 0x0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    .line 6218
    :goto_0
    return-object v0

    .line 6201
    :cond_0
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v0, "AV"

    const-string v1, "Not matched cache data of signature"

    invoke-static {v0, v1}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 6211
    :cond_1
    :goto_1
    invoke-static {}, Lcom/msc/c/g;->a()Lcom/msc/c/g;

    iget-object v0, p0, Lcom/osp/app/signin/ba;->c:Lcom/osp/app/signin/AccountView;

    invoke-static {v0, p0}, Lcom/msc/c/g;->c(Landroid/content/Context;Lcom/msc/b/h;)J

    move-result-wide v0

    iput-wide v0, p0, Lcom/osp/app/signin/ba;->e:J

    iget-wide v0, p0, Lcom/osp/app/signin/ba;->e:J

    invoke-virtual {p0, v0, v1}, Lcom/osp/app/signin/ba;->a(J)V

    iget-wide v0, p0, Lcom/osp/app/signin/ba;->e:J

    sget-object v2, Lcom/msc/b/g;->b:Lcom/msc/b/g;

    invoke-static {v0, v1, v2}, Lcom/msc/c/g;->a(JLcom/msc/b/g;)V

    .line 6218
    :goto_2
    invoke-static {v4}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    goto :goto_0

    .line 6203
    :catch_0
    move-exception v0

    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_1

    .line 6215
    :cond_2
    invoke-direct {p0}, Lcom/osp/app/signin/ba;->h()V

    goto :goto_2
.end method

.method public final declared-synchronized a(Lcom/msc/b/c;)V
    .locals 6

    .prologue
    .line 6297
    monitor-enter p0

    :try_start_0
    invoke-super {p0, p1}, Lcom/msc/c/b;->a(Lcom/msc/b/c;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 6299
    if-nez p1, :cond_1

    .line 6348
    :cond_0
    :goto_0
    monitor-exit p0

    return-void

    .line 6304
    :cond_1
    :try_start_1
    invoke-virtual {p1}, Lcom/msc/b/c;->a()J

    move-result-wide v0

    .line 6305
    invoke-virtual {p1}, Lcom/msc/b/c;->e()Ljava/lang/String;

    move-result-object v2

    .line 6307
    iget-wide v4, p0, Lcom/osp/app/signin/ba;->d:J
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    cmp-long v3, v0, v4

    if-nez v3, :cond_2

    .line 6312
    :try_start_2
    iget-object v0, p0, Lcom/osp/app/signin/ba;->c:Lcom/osp/app/signin/AccountView;

    invoke-static {}, Lcom/msc/c/h;->a()Lcom/msc/c/h;

    iget-object v1, p0, Lcom/osp/app/signin/ba;->c:Lcom/osp/app/signin/AccountView;

    invoke-static {v1, v2}, Lcom/msc/c/h;->b(Landroid/content/Context;Ljava/lang/String;)Lcom/msc/a/g;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/osp/app/signin/AccountView;->b(Lcom/osp/app/signin/AccountView;Lcom/msc/a/g;)Lcom/msc/a/g;
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_0
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0

    .line 6313
    :catch_0
    move-exception v0

    .line 6315
    :try_start_3
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    .line 6316
    new-instance v1, Lcom/msc/c/f;

    invoke-direct {v1, v0}, Lcom/msc/c/f;-><init>(Ljava/lang/Exception;)V

    iput-object v1, p0, Lcom/osp/app/signin/ba;->b:Lcom/msc/c/f;
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    goto :goto_0

    .line 6297
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 6318
    :cond_2
    :try_start_4
    iget-wide v4, p0, Lcom/osp/app/signin/ba;->e:J
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    cmp-long v0, v0, v4

    if-nez v0, :cond_0

    .line 6325
    :try_start_5
    iget-object v0, p0, Lcom/osp/app/signin/ba;->c:Lcom/osp/app/signin/AccountView;

    invoke-static {v0, v2}, Lcom/osp/app/signin/SamsungService;->a(Landroid/content/Context;Ljava/lang/String;)V

    .line 6326
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v0, "AV"

    const-string v1, "Save Signature"

    invoke-static {v0, v1}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 6329
    invoke-static {}, Lcom/msc/c/h;->a()Lcom/msc/c/h;

    invoke-static {v2}, Lcom/msc/c/h;->A(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v0

    .line 6331
    invoke-static {}, Lcom/msc/c/h;->a()Lcom/msc/c/h;

    iget-object v1, p0, Lcom/osp/app/signin/ba;->c:Lcom/osp/app/signin/AccountView;

    iget-object v2, p0, Lcom/osp/app/signin/ba;->c:Lcom/osp/app/signin/AccountView;

    invoke-static {v2}, Lcom/osp/app/signin/AccountView;->l(Lcom/osp/app/signin/AccountView;)Ljava/lang/String;

    move-result-object v2

    iget-object v3, p0, Lcom/osp/app/signin/ba;->c:Lcom/osp/app/signin/AccountView;

    invoke-virtual {v3}, Lcom/osp/app/signin/AccountView;->getCallingPackage()Ljava/lang/String;

    move-result-object v3

    invoke-static {v1, v0, v2, v3}, Lcom/msc/c/h;->a(Landroid/content/Context;Ljava/util/ArrayList;Ljava/lang/String;Ljava/lang/String;)Z

    move-result v0

    .line 6333
    if-eqz v0, :cond_3

    .line 6335
    invoke-direct {p0}, Lcom/osp/app/signin/ba;->h()V
    :try_end_5
    .catch Ljava/lang/Exception; {:try_start_5 .. :try_end_5} :catch_1
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    goto :goto_0

    .line 6341
    :catch_1
    move-exception v0

    :try_start_6
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    .line 6345
    invoke-direct {p0}, Lcom/osp/app/signin/ba;->h()V
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_0

    goto :goto_0

    .line 6338
    :cond_3
    :try_start_7
    new-instance v0, Lcom/msc/c/f;

    const-string v1, "The signature of this application is not registered with the server."

    invoke-direct {v0, v1}, Lcom/msc/c/f;-><init>(Ljava/lang/String;)V

    iput-object v0, p0, Lcom/osp/app/signin/ba;->b:Lcom/msc/c/f;
    :try_end_7
    .catch Ljava/lang/Exception; {:try_start_7 .. :try_end_7} :catch_1
    .catchall {:try_start_7 .. :try_end_7} :catchall_0

    goto :goto_0
.end method

.method protected final a(Ljava/lang/Boolean;)V
    .locals 5

    .prologue
    const/4 v4, 0x0

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 6265
    invoke-super {p0}, Lcom/msc/c/b;->e()V

    .line 6266
    iget-object v0, p0, Lcom/osp/app/signin/ba;->b:Lcom/msc/c/f;

    if-eqz v0, :cond_1

    .line 6268
    invoke-static {}, Lcom/osp/device/b;->b()Lcom/osp/device/b;

    move-result-object v0

    invoke-virtual {v0}, Lcom/osp/device/b;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    const-string v0, "USR_3174"

    iget-object v1, p0, Lcom/osp/app/signin/ba;->b:Lcom/msc/c/f;

    invoke-virtual {v1}, Lcom/msc/c/f;->a()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 6270
    iget-object v0, p0, Lcom/osp/app/signin/ba;->c:Lcom/osp/app/signin/AccountView;

    iget-object v1, p0, Lcom/osp/app/signin/ba;->c:Lcom/osp/app/signin/AccountView;

    invoke-static {v1}, Lcom/osp/app/util/ad;->h(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/osp/app/signin/AccountView;->c(Lcom/osp/app/signin/AccountView;Ljava/lang/String;)V

    .line 6271
    iget-object v0, p0, Lcom/osp/app/signin/ba;->c:Lcom/osp/app/signin/AccountView;

    invoke-virtual {v0}, Lcom/osp/app/signin/AccountView;->finish()V

    .line 6293
    :goto_0
    return-void

    .line 6275
    :cond_0
    invoke-virtual {p0, v2}, Lcom/osp/app/signin/ba;->a(Z)V

    .line 6276
    iget-object v0, p0, Lcom/osp/app/signin/ba;->c:Lcom/osp/app/signin/AccountView;

    iget-object v1, p0, Lcom/osp/app/signin/ba;->c:Lcom/osp/app/signin/AccountView;

    invoke-static {v1}, Lcom/osp/app/signin/AccountView;->m(Lcom/osp/app/signin/AccountView;)Landroid/content/Intent;

    move-result-object v1

    invoke-static {v0, v3, v1, v4}, Lcom/osp/app/signin/AccountView;->a(Lcom/osp/app/signin/AccountView;ILandroid/content/Intent;Landroid/content/Intent;)V

    .line 6277
    iget-object v0, p0, Lcom/osp/app/signin/ba;->c:Lcom/osp/app/signin/AccountView;

    invoke-virtual {v0}, Lcom/osp/app/signin/AccountView;->finish()V

    goto :goto_0

    .line 6281
    :cond_1
    iget-object v0, p0, Lcom/osp/app/signin/ba;->c:Lcom/osp/app/signin/AccountView;

    invoke-static {v0}, Lcom/osp/app/signin/AccountView;->y(Lcom/osp/app/signin/AccountView;)Lcom/msc/a/g;

    move-result-object v0

    if-eqz v0, :cond_2

    .line 6283
    iget-object v0, p0, Lcom/osp/app/signin/ba;->c:Lcom/osp/app/signin/AccountView;

    iget-object v1, p0, Lcom/osp/app/signin/ba;->c:Lcom/osp/app/signin/AccountView;

    invoke-static {v1}, Lcom/osp/app/signin/AccountView;->y(Lcom/osp/app/signin/AccountView;)Lcom/msc/a/g;

    move-result-object v1

    invoke-virtual {v1}, Lcom/msc/a/g;->f()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/msc/c/e;->d(Landroid/content/Context;Ljava/lang/String;)V

    .line 6285
    iget-object v0, p0, Lcom/osp/app/signin/ba;->c:Lcom/osp/app/signin/AccountView;

    invoke-virtual {p0}, Lcom/osp/app/signin/ba;->g()Landroid/app/ProgressDialog;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/osp/app/signin/AccountView;->a(Lcom/osp/app/signin/AccountView;Landroid/app/ProgressDialog;)Landroid/app/ProgressDialog;

    .line 6286
    iget-object v0, p0, Lcom/osp/app/signin/ba;->c:Lcom/osp/app/signin/AccountView;

    iget-object v1, p0, Lcom/osp/app/signin/ba;->c:Lcom/osp/app/signin/AccountView;

    invoke-static {v1}, Lcom/osp/app/signin/AccountView;->y(Lcom/osp/app/signin/AccountView;)Lcom/msc/a/g;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/osp/app/signin/AccountView;->a(Lcom/osp/app/signin/AccountView;Lcom/msc/a/g;)V

    goto :goto_0

    .line 6289
    :cond_2
    invoke-virtual {p0, v2}, Lcom/osp/app/signin/ba;->a(Z)V

    .line 6290
    iget-object v0, p0, Lcom/osp/app/signin/ba;->c:Lcom/osp/app/signin/AccountView;

    iget-object v1, p0, Lcom/osp/app/signin/ba;->c:Lcom/osp/app/signin/AccountView;

    invoke-static {v1}, Lcom/osp/app/signin/AccountView;->m(Lcom/osp/app/signin/AccountView;)Landroid/content/Intent;

    move-result-object v1

    invoke-static {v0, v3, v1, v4}, Lcom/osp/app/signin/AccountView;->a(Lcom/osp/app/signin/AccountView;ILandroid/content/Intent;Landroid/content/Intent;)V

    .line 6291
    iget-object v0, p0, Lcom/osp/app/signin/ba;->c:Lcom/osp/app/signin/AccountView;

    invoke-virtual {v0}, Lcom/osp/app/signin/AccountView;->finish()V

    goto :goto_0
.end method

.method public final b(Lcom/msc/b/c;)V
    .locals 4

    .prologue
    .line 6352
    invoke-super {p0, p1}, Lcom/msc/c/b;->b(Lcom/msc/b/c;)V

    .line 6354
    if-nez p1, :cond_1

    .line 6374
    :cond_0
    :goto_0
    return-void

    .line 6359
    :cond_1
    invoke-virtual {p1}, Lcom/msc/b/c;->a()J

    move-result-wide v0

    .line 6361
    iget-wide v2, p0, Lcom/osp/app/signin/ba;->d:J

    cmp-long v2, v0, v2

    if-eqz v2, :cond_0

    .line 6364
    iget-wide v2, p0, Lcom/osp/app/signin/ba;->e:J

    cmp-long v0, v0, v2

    if-nez v0, :cond_0

    .line 6371
    invoke-direct {p0}, Lcom/osp/app/signin/ba;->h()V

    goto :goto_0
.end method

.method protected final d()V
    .locals 2

    .prologue
    .line 6166
    invoke-super {p0}, Lcom/msc/c/b;->d()V

    .line 6167
    iget-object v0, p0, Lcom/osp/app/signin/ba;->c:Lcom/osp/app/signin/AccountView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/osp/app/signin/AccountView;->b(I)V

    .line 6168
    iget-object v0, p0, Lcom/osp/app/signin/ba;->c:Lcom/osp/app/signin/AccountView;

    invoke-virtual {v0}, Lcom/osp/app/signin/AccountView;->finish()V

    .line 6169
    return-void
.end method

.method protected final synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 6136
    invoke-virtual {p0}, Lcom/osp/app/signin/ba;->a()Ljava/lang/Boolean;

    move-result-object v0

    return-object v0
.end method

.method protected final synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 6136
    check-cast p1, Ljava/lang/Boolean;

    invoke-virtual {p0, p1}, Lcom/osp/app/signin/ba;->a(Ljava/lang/Boolean;)V

    return-void
.end method
