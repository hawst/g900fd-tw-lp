.class final Lcom/osp/app/signin/ho;
.super Ljava/lang/Object;
.source "SelectCountryView.java"

# interfaces
.implements Landroid/widget/AdapterView$OnItemClickListener;


# instance fields
.field final synthetic a:Lcom/osp/app/signin/hl;


# direct methods
.method constructor <init>(Lcom/osp/app/signin/hl;)V
    .locals 0

    .prologue
    .line 1263
    iput-object p1, p0, Lcom/osp/app/signin/ho;->a:Lcom/osp/app/signin/hl;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onItemClick(Landroid/widget/AdapterView;Landroid/view/View;IJ)V
    .locals 7

    .prologue
    const/4 v6, -0x1

    .line 1268
    iget-object v0, p0, Lcom/osp/app/signin/ho;->a:Lcom/osp/app/signin/hl;

    invoke-static {v0}, Lcom/osp/app/signin/hl;->a(Lcom/osp/app/signin/hl;)[Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/osp/app/signin/ho;->a:Lcom/osp/app/signin/hl;

    invoke-static {v0}, Lcom/osp/app/signin/hl;->a(Lcom/osp/app/signin/hl;)[Ljava/lang/String;

    move-result-object v0

    array-length v0, v0

    add-int/lit8 v1, p3, 0x1

    if-ge v0, v1, :cond_1

    .line 1362
    :cond_0
    return-void

    .line 1272
    :cond_1
    iget-object v0, p0, Lcom/osp/app/signin/ho;->a:Lcom/osp/app/signin/hl;

    invoke-static {v0}, Lcom/osp/app/signin/hl;->a(Lcom/osp/app/signin/hl;)[Ljava/lang/String;

    move-result-object v0

    aget-object v0, v0, p3

    .line 1273
    iget-object v1, p0, Lcom/osp/app/signin/ho;->a:Lcom/osp/app/signin/hl;

    iget-object v1, v1, Lcom/osp/app/signin/hl;->d:Lcom/osp/app/signin/SelectCountryView;

    invoke-static {v1}, Lcom/osp/app/signin/SelectCountryView;->e(Lcom/osp/app/signin/SelectCountryView;)Landroid/content/Intent;

    move-result-object v1

    const-string v2, "SelectCountry"

    invoke-virtual {v1, v2, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 1275
    iget-object v1, p0, Lcom/osp/app/signin/ho;->a:Lcom/osp/app/signin/hl;

    iget-object v1, v1, Lcom/osp/app/signin/hl;->d:Lcom/osp/app/signin/SelectCountryView;

    invoke-static {v1}, Lcom/osp/app/signin/SelectCountryView;->o(Lcom/osp/app/signin/SelectCountryView;)Lcom/osp/app/signin/ej;

    move-result-object v1

    if-eqz v1, :cond_2

    .line 1277
    iget-object v1, p0, Lcom/osp/app/signin/ho;->a:Lcom/osp/app/signin/hl;

    iget-object v1, v1, Lcom/osp/app/signin/hl;->d:Lcom/osp/app/signin/SelectCountryView;

    invoke-static {v1}, Lcom/osp/app/signin/SelectCountryView;->o(Lcom/osp/app/signin/SelectCountryView;)Lcom/osp/app/signin/ej;

    move-result-object v1

    invoke-virtual {v1}, Lcom/osp/app/signin/ej;->a()V

    .line 1284
    :cond_2
    const-string v1, "country_calling_code_list"

    iget-object v2, p0, Lcom/osp/app/signin/ho;->a:Lcom/osp/app/signin/hl;

    iget-object v2, v2, Lcom/osp/app/signin/hl;->d:Lcom/osp/app/signin/SelectCountryView;

    invoke-static {v2}, Lcom/osp/app/signin/SelectCountryView;->a(Lcom/osp/app/signin/SelectCountryView;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_c

    const-string v1, "language_list"

    iget-object v2, p0, Lcom/osp/app/signin/ho;->a:Lcom/osp/app/signin/hl;

    iget-object v2, v2, Lcom/osp/app/signin/hl;->d:Lcom/osp/app/signin/SelectCountryView;

    invoke-static {v2}, Lcom/osp/app/signin/SelectCountryView;->a(Lcom/osp/app/signin/SelectCountryView;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_c

    .line 1286
    const-string v1, "("

    invoke-virtual {v0, v1}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v1

    .line 1287
    if-lez v1, :cond_c

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v2

    const/4 v3, 0x1

    if-eq v2, v3, :cond_c

    .line 1289
    const/4 v2, 0x0

    add-int/lit8 v1, v1, -0x1

    invoke-virtual {v0, v2, v1}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v0

    move-object v1, v0

    .line 1293
    :goto_0
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v0, "SCV"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "select country:"

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 1295
    iget-object v0, p0, Lcom/osp/app/signin/ho;->a:Lcom/osp/app/signin/hl;

    iget-object v0, v0, Lcom/osp/app/signin/hl;->d:Lcom/osp/app/signin/SelectCountryView;

    invoke-static {v0, p3}, Lcom/osp/app/signin/SelectCountryView;->a(Lcom/osp/app/signin/SelectCountryView;I)I

    .line 1296
    iget-object v0, p0, Lcom/osp/app/signin/ho;->a:Lcom/osp/app/signin/hl;

    iget-object v0, v0, Lcom/osp/app/signin/hl;->c:Ljava/util/ArrayList;

    if-eqz v0, :cond_0

    .line 1298
    iget-object v0, p0, Lcom/osp/app/signin/ho;->a:Lcom/osp/app/signin/hl;

    iget-object v0, v0, Lcom/osp/app/signin/hl;->c:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_3
    :goto_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/osp/app/countrylist/CountryInfoItem;

    .line 1305
    iget-object v3, p0, Lcom/osp/app/signin/ho;->a:Lcom/osp/app/signin/hl;

    iget-object v3, v3, Lcom/osp/app/signin/hl;->d:Lcom/osp/app/signin/SelectCountryView;

    invoke-static {v3}, Lcom/msc/c/e;->f(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v3

    .line 1306
    const-string v4, "Hong kong"

    invoke-virtual {v1, v4}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v4

    if-nez v4, :cond_4

    const-string v4, "Macao"

    invoke-virtual {v1, v4}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_8

    .line 1308
    :cond_4
    invoke-virtual {v0}, Lcom/osp/app/countrylist/CountryInfoItem;->b()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v4, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_3

    .line 1311
    const-string v4, "country_calling_code_list"

    iget-object v5, p0, Lcom/osp/app/signin/ho;->a:Lcom/osp/app/signin/hl;

    iget-object v5, v5, Lcom/osp/app/signin/hl;->d:Lcom/osp/app/signin/SelectCountryView;

    invoke-static {v5}, Lcom/osp/app/signin/SelectCountryView;->a(Lcom/osp/app/signin/SelectCountryView;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_5

    .line 1313
    iget-object v3, p0, Lcom/osp/app/signin/ho;->a:Lcom/osp/app/signin/hl;

    iget-object v3, v3, Lcom/osp/app/signin/hl;->d:Lcom/osp/app/signin/SelectCountryView;

    invoke-static {v3}, Lcom/osp/app/signin/SelectCountryView;->e(Lcom/osp/app/signin/SelectCountryView;)Landroid/content/Intent;

    move-result-object v3

    const-string v4, "key_country_calling_code"

    invoke-virtual {v0}, Lcom/osp/app/countrylist/CountryInfoItem;->e()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v3, v4, v5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 1314
    iget-object v3, p0, Lcom/osp/app/signin/ho;->a:Lcom/osp/app/signin/hl;

    iget-object v3, v3, Lcom/osp/app/signin/hl;->d:Lcom/osp/app/signin/SelectCountryView;

    invoke-static {v3}, Lcom/osp/app/signin/SelectCountryView;->e(Lcom/osp/app/signin/SelectCountryView;)Landroid/content/Intent;

    move-result-object v3

    const-string v4, "key_country_code"

    invoke-virtual {v0}, Lcom/osp/app/countrylist/CountryInfoItem;->a()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v3, v4, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 1326
    :goto_2
    iget-object v0, p0, Lcom/osp/app/signin/ho;->a:Lcom/osp/app/signin/hl;

    iget-object v0, v0, Lcom/osp/app/signin/hl;->d:Lcom/osp/app/signin/SelectCountryView;

    invoke-static {v0}, Lcom/osp/app/signin/SelectCountryView;->u(Lcom/osp/app/signin/SelectCountryView;)V

    .line 1328
    iget-object v0, p0, Lcom/osp/app/signin/ho;->a:Lcom/osp/app/signin/hl;

    iget-object v0, v0, Lcom/osp/app/signin/hl;->d:Lcom/osp/app/signin/SelectCountryView;

    iget-object v3, p0, Lcom/osp/app/signin/ho;->a:Lcom/osp/app/signin/hl;

    iget-object v3, v3, Lcom/osp/app/signin/hl;->d:Lcom/osp/app/signin/SelectCountryView;

    invoke-static {v3}, Lcom/osp/app/signin/SelectCountryView;->e(Lcom/osp/app/signin/SelectCountryView;)Landroid/content/Intent;

    move-result-object v3

    invoke-virtual {v0, v6, v3}, Lcom/osp/app/signin/SelectCountryView;->a(ILandroid/content/Intent;)V

    .line 1329
    iget-object v0, p0, Lcom/osp/app/signin/ho;->a:Lcom/osp/app/signin/hl;

    iget-object v0, v0, Lcom/osp/app/signin/hl;->d:Lcom/osp/app/signin/SelectCountryView;

    invoke-virtual {v0}, Lcom/osp/app/signin/SelectCountryView;->finish()V

    goto :goto_1

    .line 1315
    :cond_5
    const-string v4, "language_list"

    iget-object v5, p0, Lcom/osp/app/signin/ho;->a:Lcom/osp/app/signin/hl;

    iget-object v5, v5, Lcom/osp/app/signin/hl;->d:Lcom/osp/app/signin/SelectCountryView;

    invoke-static {v5}, Lcom/osp/app/signin/SelectCountryView;->a(Lcom/osp/app/signin/SelectCountryView;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_6

    .line 1317
    iget-object v3, p0, Lcom/osp/app/signin/ho;->a:Lcom/osp/app/signin/hl;

    iget-object v3, v3, Lcom/osp/app/signin/hl;->d:Lcom/osp/app/signin/SelectCountryView;

    invoke-static {v3}, Lcom/osp/app/signin/SelectCountryView;->e(Lcom/osp/app/signin/SelectCountryView;)Landroid/content/Intent;

    move-result-object v3

    const-string v4, "key_language_code"

    invoke-virtual {v0}, Lcom/osp/app/countrylist/CountryInfoItem;->a()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v3, v4, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    goto :goto_2

    .line 1320
    :cond_6
    if-eqz v3, :cond_7

    invoke-virtual {v0}, Lcom/osp/app/countrylist/CountryInfoItem;->c()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_7

    .line 1322
    iget-object v4, p0, Lcom/osp/app/signin/ho;->a:Lcom/osp/app/signin/hl;

    iget-object v4, v4, Lcom/osp/app/signin/hl;->d:Lcom/osp/app/signin/SelectCountryView;

    invoke-static {v4}, Lcom/osp/app/signin/SelectCountryView;->e(Lcom/osp/app/signin/SelectCountryView;)Landroid/content/Intent;

    move-result-object v4

    const-string v5, "ChangeMcc"

    invoke-virtual {v4, v5, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 1324
    :cond_7
    iget-object v3, p0, Lcom/osp/app/signin/ho;->a:Lcom/osp/app/signin/hl;

    iget-object v3, v3, Lcom/osp/app/signin/hl;->d:Lcom/osp/app/signin/SelectCountryView;

    invoke-virtual {v0}, Lcom/osp/app/countrylist/CountryInfoItem;->c()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v0}, Lcom/osp/app/countrylist/CountryInfoItem;->a()Ljava/lang/String;

    move-result-object v0

    invoke-static {v3, v4, v0}, Lcom/msc/c/e;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_2

    .line 1334
    :cond_8
    invoke-virtual {v0}, Lcom/osp/app/countrylist/CountryInfoItem;->b()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v4, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_3

    .line 1337
    const-string v4, "country_calling_code_list"

    iget-object v5, p0, Lcom/osp/app/signin/ho;->a:Lcom/osp/app/signin/hl;

    iget-object v5, v5, Lcom/osp/app/signin/hl;->d:Lcom/osp/app/signin/SelectCountryView;

    invoke-static {v5}, Lcom/osp/app/signin/SelectCountryView;->a(Lcom/osp/app/signin/SelectCountryView;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_9

    .line 1339
    iget-object v3, p0, Lcom/osp/app/signin/ho;->a:Lcom/osp/app/signin/hl;

    iget-object v3, v3, Lcom/osp/app/signin/hl;->d:Lcom/osp/app/signin/SelectCountryView;

    invoke-static {v3}, Lcom/osp/app/signin/SelectCountryView;->e(Lcom/osp/app/signin/SelectCountryView;)Landroid/content/Intent;

    move-result-object v3

    const-string v4, "key_country_calling_code"

    invoke-virtual {v0}, Lcom/osp/app/countrylist/CountryInfoItem;->e()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v3, v4, v5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 1340
    iget-object v3, p0, Lcom/osp/app/signin/ho;->a:Lcom/osp/app/signin/hl;

    iget-object v3, v3, Lcom/osp/app/signin/hl;->d:Lcom/osp/app/signin/SelectCountryView;

    invoke-static {v3}, Lcom/osp/app/signin/SelectCountryView;->e(Lcom/osp/app/signin/SelectCountryView;)Landroid/content/Intent;

    move-result-object v3

    const-string v4, "key_country_code"

    invoke-virtual {v0}, Lcom/osp/app/countrylist/CountryInfoItem;->a()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v3, v4, v5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 1341
    iget-object v3, p0, Lcom/osp/app/signin/ho;->a:Lcom/osp/app/signin/hl;

    iget-object v3, v3, Lcom/osp/app/signin/hl;->d:Lcom/osp/app/signin/SelectCountryView;

    invoke-static {v3}, Lcom/osp/app/signin/SelectCountryView;->e(Lcom/osp/app/signin/SelectCountryView;)Landroid/content/Intent;

    move-result-object v3

    const-string v4, "key_alpha2_country_code"

    invoke-virtual {v0}, Lcom/osp/app/countrylist/CountryInfoItem;->f()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v3, v4, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 1353
    :goto_3
    iget-object v0, p0, Lcom/osp/app/signin/ho;->a:Lcom/osp/app/signin/hl;

    iget-object v0, v0, Lcom/osp/app/signin/hl;->d:Lcom/osp/app/signin/SelectCountryView;

    invoke-static {v0}, Lcom/osp/app/signin/SelectCountryView;->u(Lcom/osp/app/signin/SelectCountryView;)V

    .line 1355
    iget-object v0, p0, Lcom/osp/app/signin/ho;->a:Lcom/osp/app/signin/hl;

    iget-object v0, v0, Lcom/osp/app/signin/hl;->d:Lcom/osp/app/signin/SelectCountryView;

    iget-object v3, p0, Lcom/osp/app/signin/ho;->a:Lcom/osp/app/signin/hl;

    iget-object v3, v3, Lcom/osp/app/signin/hl;->d:Lcom/osp/app/signin/SelectCountryView;

    invoke-static {v3}, Lcom/osp/app/signin/SelectCountryView;->e(Lcom/osp/app/signin/SelectCountryView;)Landroid/content/Intent;

    move-result-object v3

    invoke-virtual {v0, v6, v3}, Lcom/osp/app/signin/SelectCountryView;->a(ILandroid/content/Intent;)V

    .line 1356
    iget-object v0, p0, Lcom/osp/app/signin/ho;->a:Lcom/osp/app/signin/hl;

    iget-object v0, v0, Lcom/osp/app/signin/hl;->d:Lcom/osp/app/signin/SelectCountryView;

    invoke-virtual {v0}, Lcom/osp/app/signin/SelectCountryView;->finish()V

    goto/16 :goto_1

    .line 1342
    :cond_9
    const-string v4, "language_list"

    iget-object v5, p0, Lcom/osp/app/signin/ho;->a:Lcom/osp/app/signin/hl;

    iget-object v5, v5, Lcom/osp/app/signin/hl;->d:Lcom/osp/app/signin/SelectCountryView;

    invoke-static {v5}, Lcom/osp/app/signin/SelectCountryView;->a(Lcom/osp/app/signin/SelectCountryView;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_a

    .line 1344
    iget-object v3, p0, Lcom/osp/app/signin/ho;->a:Lcom/osp/app/signin/hl;

    iget-object v3, v3, Lcom/osp/app/signin/hl;->d:Lcom/osp/app/signin/SelectCountryView;

    invoke-static {v3}, Lcom/osp/app/signin/SelectCountryView;->e(Lcom/osp/app/signin/SelectCountryView;)Landroid/content/Intent;

    move-result-object v3

    const-string v4, "key_language_code"

    invoke-virtual {v0}, Lcom/osp/app/countrylist/CountryInfoItem;->c()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v3, v4, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    goto :goto_3

    .line 1347
    :cond_a
    if-eqz v3, :cond_b

    invoke-virtual {v0}, Lcom/osp/app/countrylist/CountryInfoItem;->a()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_b

    .line 1349
    iget-object v4, p0, Lcom/osp/app/signin/ho;->a:Lcom/osp/app/signin/hl;

    iget-object v4, v4, Lcom/osp/app/signin/hl;->d:Lcom/osp/app/signin/SelectCountryView;

    invoke-static {v4}, Lcom/osp/app/signin/SelectCountryView;->e(Lcom/osp/app/signin/SelectCountryView;)Landroid/content/Intent;

    move-result-object v4

    const-string v5, "ChangeMcc"

    invoke-virtual {v4, v5, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 1351
    :cond_b
    iget-object v3, p0, Lcom/osp/app/signin/ho;->a:Lcom/osp/app/signin/hl;

    iget-object v3, v3, Lcom/osp/app/signin/hl;->d:Lcom/osp/app/signin/SelectCountryView;

    invoke-virtual {v0}, Lcom/osp/app/countrylist/CountryInfoItem;->c()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v0}, Lcom/osp/app/countrylist/CountryInfoItem;->a()Ljava/lang/String;

    move-result-object v0

    invoke-static {v3, v4, v0}, Lcom/msc/c/e;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_3

    :cond_c
    move-object v1, v0

    goto/16 :goto_0
.end method
