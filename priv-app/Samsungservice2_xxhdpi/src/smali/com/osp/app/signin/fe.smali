.class final Lcom/osp/app/signin/fe;
.super Ljava/lang/Object;
.source "EmailValidationView.java"

# interfaces
.implements Landroid/content/DialogInterface$OnClickListener;


# instance fields
.field final synthetic a:Lcom/osp/app/signin/EmailValidationView;


# direct methods
.method constructor <init>(Lcom/osp/app/signin/EmailValidationView;)V
    .locals 0

    .prologue
    .line 652
    iput-object p1, p0, Lcom/osp/app/signin/fe;->a:Lcom/osp/app/signin/EmailValidationView;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onClick(Landroid/content/DialogInterface;I)V
    .locals 7

    .prologue
    .line 656
    iget-object v0, p0, Lcom/osp/app/signin/fe;->a:Lcom/osp/app/signin/EmailValidationView;

    invoke-static {v0}, Lcom/osp/app/signin/EmailValidationView;->k(Lcom/osp/app/signin/EmailValidationView;)I

    move-result v0

    const/16 v1, 0xca

    if-ne v0, v1, :cond_1

    .line 658
    iget-object v0, p0, Lcom/osp/app/signin/fe;->a:Lcom/osp/app/signin/EmailValidationView;

    invoke-static {v0}, Lcom/osp/app/signin/EmailValidationView;->l(Lcom/osp/app/signin/EmailValidationView;)V

    .line 670
    :goto_0
    iget-object v0, p0, Lcom/osp/app/signin/fe;->a:Lcom/osp/app/signin/EmailValidationView;

    iget v0, v0, Lcom/osp/app/signin/EmailValidationView;->a:I

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/osp/app/signin/fe;->a:Lcom/osp/app/signin/EmailValidationView;

    invoke-static {v0}, Lcom/osp/app/util/r;->t(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 672
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v0, "EVV"

    const-string v1, "Show MybenefitNoti"

    invoke-static {v0, v1}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 676
    new-instance v0, Landroid/content/Intent;

    const-string v1, "com.msc.action.samsungaccount.mybenefitwebview_internal"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 689
    iget-object v1, p0, Lcom/osp/app/signin/fe;->a:Lcom/osp/app/signin/EmailValidationView;

    const/4 v2, 0x0

    const/high16 v3, 0x8000000

    invoke-static {v1, v2, v0, v3}, Landroid/app/PendingIntent;->getActivity(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v1

    .line 691
    iget-object v0, p0, Lcom/osp/app/signin/fe;->a:Lcom/osp/app/signin/EmailValidationView;

    const/4 v2, 0x0

    iget-object v3, p0, Lcom/osp/app/signin/fe;->a:Lcom/osp/app/signin/EmailValidationView;

    const v4, 0x7f09004f

    invoke-virtual {v3, v4}, Lcom/osp/app/signin/EmailValidationView;->getString(I)Ljava/lang/String;

    move-result-object v3

    iget-object v4, p0, Lcom/osp/app/signin/fe;->a:Lcom/osp/app/signin/EmailValidationView;

    const v5, 0x7f090152

    invoke-virtual {v4, v5}, Lcom/osp/app/signin/EmailValidationView;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-static {}, Lcom/osp/app/signin/SamsungService;->i()I

    move-result v5

    const v6, 0x132df82

    invoke-static/range {v0 .. v6}, Lcom/msc/sa/c/d;->a(Landroid/content/Context;Landroid/app/PendingIntent;Landroid/app/PendingIntent;Ljava/lang/String;Ljava/lang/String;II)V

    .line 696
    :cond_0
    return-void

    .line 659
    :cond_1
    iget-object v0, p0, Lcom/osp/app/signin/fe;->a:Lcom/osp/app/signin/EmailValidationView;

    invoke-static {v0}, Lcom/osp/app/signin/EmailValidationView;->k(Lcom/osp/app/signin/EmailValidationView;)I

    move-result v0

    const/16 v1, 0xc9

    if-ne v0, v1, :cond_2

    .line 661
    iget-object v0, p0, Lcom/osp/app/signin/fe;->a:Lcom/osp/app/signin/EmailValidationView;

    invoke-virtual {v0}, Lcom/osp/app/signin/EmailValidationView;->finish()V

    goto :goto_0

    .line 662
    :cond_2
    iget-object v0, p0, Lcom/osp/app/signin/fe;->a:Lcom/osp/app/signin/EmailValidationView;

    invoke-static {v0}, Lcom/osp/app/signin/EmailValidationView;->k(Lcom/osp/app/signin/EmailValidationView;)I

    move-result v0

    const/16 v1, 0xcb

    if-ne v0, v1, :cond_3

    .line 664
    iget-object v0, p0, Lcom/osp/app/signin/fe;->a:Lcom/osp/app/signin/EmailValidationView;

    invoke-virtual {v0}, Lcom/osp/app/signin/EmailValidationView;->finish()V

    goto :goto_0

    .line 667
    :cond_3
    iget-object v0, p0, Lcom/osp/app/signin/fe;->a:Lcom/osp/app/signin/EmailValidationView;

    invoke-static {v0}, Lcom/osp/app/signin/EmailValidationView;->m(Lcom/osp/app/signin/EmailValidationView;)V

    goto :goto_0
.end method
