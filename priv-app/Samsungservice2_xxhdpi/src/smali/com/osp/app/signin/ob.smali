.class final Lcom/osp/app/signin/ob;
.super Lcom/msc/c/b;
.source "TnCView.java"


# instance fields
.field final synthetic c:Lcom/osp/app/signin/TnCView;

.field private d:J

.field private e:Ljava/lang/String;


# direct methods
.method public constructor <init>(Lcom/osp/app/signin/TnCView;)V
    .locals 0

    .prologue
    .line 4410
    iput-object p1, p0, Lcom/osp/app/signin/ob;->c:Lcom/osp/app/signin/TnCView;

    .line 4411
    invoke-direct {p0, p1}, Lcom/msc/c/b;-><init>(Landroid/content/Context;)V

    .line 4415
    return-void
.end method


# virtual methods
.method protected final varargs a()Ljava/lang/Boolean;
    .locals 3

    .prologue
    .line 4424
    iget-object v0, p0, Lcom/osp/app/signin/ob;->c:Lcom/osp/app/signin/TnCView;

    invoke-static {v0}, Lcom/osp/app/util/ad;->h(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v0

    .line 4425
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v1

    if-lez v1, :cond_0

    iget-object v1, p0, Lcom/osp/app/signin/ob;->c:Lcom/osp/app/signin/TnCView;

    invoke-static {v1}, Lcom/osp/app/signin/TnCView;->C(Lcom/osp/app/signin/TnCView;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 4427
    :cond_0
    iget-object v0, p0, Lcom/osp/app/signin/ob;->c:Lcom/osp/app/signin/TnCView;

    invoke-static {v0}, Lcom/osp/app/signin/TnCView;->c(Lcom/osp/app/signin/TnCView;)Landroid/content/Intent;

    move-result-object v0

    const-string v1, "email_id"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 4429
    :cond_1
    new-instance v1, Lcom/msc/a/f;

    invoke-direct {v1}, Lcom/msc/a/f;-><init>()V

    .line 4430
    invoke-virtual {v1, v0}, Lcom/msc/a/f;->d(Ljava/lang/String;)V

    .line 4431
    iget-object v0, p0, Lcom/osp/app/signin/ob;->c:Lcom/osp/app/signin/TnCView;

    invoke-static {v0}, Lcom/osp/app/signin/TnCView;->D(Lcom/osp/app/signin/TnCView;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 4433
    invoke-virtual {v1}, Lcom/msc/a/f;->b()V

    .line 4434
    invoke-virtual {v1}, Lcom/msc/a/f;->d()V

    .line 4437
    iget-object v0, p0, Lcom/osp/app/signin/ob;->c:Lcom/osp/app/signin/TnCView;

    invoke-static {v0}, Lcom/osp/app/signin/TnCView;->m(Lcom/osp/app/signin/TnCView;)Z

    move-result v0

    invoke-virtual {v1, v0}, Lcom/msc/a/f;->a(Z)V

    .line 4440
    iget-object v0, p0, Lcom/osp/app/signin/ob;->c:Lcom/osp/app/signin/TnCView;

    invoke-static {v0}, Lcom/osp/app/util/r;->b(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 4447
    iget-object v0, p0, Lcom/osp/app/signin/ob;->c:Lcom/osp/app/signin/TnCView;

    invoke-static {v0}, Lcom/osp/app/signin/TnCView;->n(Lcom/osp/app/signin/TnCView;)Z

    move-result v0

    invoke-virtual {v1, v0}, Lcom/msc/a/f;->b(Z)V

    .line 4449
    :cond_2
    iget-object v0, p0, Lcom/osp/app/signin/ob;->c:Lcom/osp/app/signin/TnCView;

    invoke-static {v0}, Lcom/osp/app/signin/TnCView;->q(Lcom/osp/app/signin/TnCView;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Lcom/msc/a/f;->h(Ljava/lang/String;)V

    .line 4451
    :cond_3
    iget-object v0, p0, Lcom/osp/app/signin/ob;->c:Lcom/osp/app/signin/TnCView;

    invoke-static {v0}, Lcom/osp/app/signin/TnCView;->y(Lcom/osp/app/signin/TnCView;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Lcom/msc/a/f;->f(Ljava/lang/String;)V

    .line 4452
    iget-object v0, p0, Lcom/osp/app/signin/ob;->c:Lcom/osp/app/signin/TnCView;

    invoke-static {v0}, Lcom/osp/app/signin/TnCView;->E(Lcom/osp/app/signin/TnCView;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Lcom/msc/a/f;->g(Ljava/lang/String;)V

    .line 4453
    invoke-static {}, Lcom/osp/device/b;->b()Lcom/osp/device/b;

    move-result-object v0

    invoke-virtual {v0}, Lcom/osp/device/b;->a()Z

    move-result v0

    if-eqz v0, :cond_4

    iget-object v0, p0, Lcom/osp/app/signin/ob;->c:Lcom/osp/app/signin/TnCView;

    invoke-static {v0}, Lcom/osp/app/util/ad;->h(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/telephony/PhoneNumberUtils;->isGlobalPhoneNumber(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 4455
    iget-object v0, p0, Lcom/osp/app/signin/ob;->c:Lcom/osp/app/signin/TnCView;

    invoke-static {v0}, Lcom/msc/c/e;->h(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v0

    .line 4456
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_4

    .line 4458
    const-string v2, "Y"

    invoke-virtual {v1, v2}, Lcom/msc/a/f;->b(Ljava/lang/String;)V

    .line 4459
    invoke-virtual {v1, v0}, Lcom/msc/a/f;->c(Ljava/lang/String;)V

    .line 4464
    :cond_4
    invoke-static {}, Lcom/msc/c/g;->a()Lcom/msc/c/g;

    iget-object v0, p0, Lcom/osp/app/signin/ob;->c:Lcom/osp/app/signin/TnCView;

    invoke-static {v0, v1, p0}, Lcom/msc/c/g;->a(Landroid/content/Context;Lcom/msc/a/f;Lcom/msc/b/h;)J

    move-result-wide v0

    iput-wide v0, p0, Lcom/osp/app/signin/ob;->d:J

    iget-wide v0, p0, Lcom/osp/app/signin/ob;->d:J

    invoke-virtual {p0, v0, v1}, Lcom/osp/app/signin/ob;->a(J)V

    iget-wide v0, p0, Lcom/osp/app/signin/ob;->d:J

    const-string v2, "from_xml"

    invoke-virtual {p0, v0, v1, v2}, Lcom/osp/app/signin/ob;->a(JLjava/lang/String;)V

    iget-wide v0, p0, Lcom/osp/app/signin/ob;->d:J

    sget-object v2, Lcom/msc/b/g;->c:Lcom/msc/b/g;

    invoke-static {v0, v1, v2}, Lcom/msc/c/g;->a(JLcom/msc/b/g;)V

    .line 4465
    const/4 v0, 0x0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    return-object v0
.end method

.method public final declared-synchronized a(Lcom/msc/b/c;)V
    .locals 8

    .prologue
    const/4 v1, 0x0

    .line 4513
    monitor-enter p0

    :try_start_0
    invoke-super {p0, p1}, Lcom/msc/c/b;->a(Lcom/msc/b/c;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 4515
    if-nez p1, :cond_1

    .line 4565
    :cond_0
    :goto_0
    monitor-exit p0

    return-void

    .line 4520
    :cond_1
    :try_start_1
    invoke-virtual {p1}, Lcom/msc/b/c;->a()J

    move-result-wide v2

    .line 4521
    invoke-virtual {p1}, Lcom/msc/b/c;->e()Ljava/lang/String;

    move-result-object v0

    .line 4523
    iget-wide v4, p0, Lcom/osp/app/signin/ob;->d:J
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    cmp-long v2, v2, v4

    if-nez v2, :cond_0

    .line 4529
    :try_start_2
    invoke-static {}, Lcom/msc/c/h;->a()Lcom/msc/c/h;

    iget-object v2, p0, Lcom/osp/app/signin/ob;->c:Lcom/osp/app/signin/TnCView;

    invoke-static {v2, v0}, Lcom/msc/c/h;->b(Landroid/content/Context;Ljava/lang/String;)Lcom/msc/a/g;
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_0
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    move-result-object v0

    .line 4537
    :goto_1
    if-eqz v0, :cond_4

    :try_start_3
    invoke-virtual {v0}, Lcom/msc/a/g;->d()Z

    move-result v0

    if-nez v0, :cond_4

    .line 4539
    const-string v0, "Success"

    iput-object v0, p0, Lcom/osp/app/signin/ob;->e:Ljava/lang/String;

    .line 4540
    iget-object v0, p0, Lcom/osp/app/signin/ob;->c:Lcom/osp/app/signin/TnCView;

    invoke-static {v0}, Lcom/osp/app/signin/TnCView;->G(Lcom/osp/app/signin/TnCView;)I

    move-result v0

    and-int/lit8 v0, v0, 0x2

    const/4 v1, 0x2

    if-ne v0, v1, :cond_2

    .line 4542
    iget-object v0, p0, Lcom/osp/app/signin/ob;->c:Lcom/osp/app/signin/TnCView;

    invoke-static {v0}, Lcom/osp/app/signin/TnCView;->H(Lcom/osp/app/signin/TnCView;)I

    .line 4544
    :cond_2
    iget-object v0, p0, Lcom/osp/app/signin/ob;->c:Lcom/osp/app/signin/TnCView;

    invoke-static {v0}, Lcom/osp/app/signin/TnCView;->G(Lcom/osp/app/signin/TnCView;)I

    move-result v0

    if-lez v0, :cond_0

    .line 4548
    iget-object v0, p0, Lcom/osp/app/signin/ob;->c:Lcom/osp/app/signin/TnCView;

    iget-object v1, p0, Lcom/osp/app/signin/ob;->c:Lcom/osp/app/signin/TnCView;

    invoke-static {v1}, Lcom/osp/app/signin/TnCView;->G(Lcom/osp/app/signin/TnCView;)I

    move-result v1

    iget-object v2, p0, Lcom/osp/app/signin/ob;->c:Lcom/osp/app/signin/TnCView;

    invoke-static {v2}, Lcom/msc/c/e;->h(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v2

    iget-object v3, p0, Lcom/osp/app/signin/ob;->c:Lcom/osp/app/signin/TnCView;

    invoke-static {v3}, Lcom/osp/app/signin/TnCView;->y(Lcom/osp/app/signin/TnCView;)Ljava/lang/String;

    move-result-object v3

    iget-object v4, p0, Lcom/osp/app/signin/ob;->c:Lcom/osp/app/signin/TnCView;

    invoke-static {v4}, Lcom/osp/app/signin/TnCView;->I(Lcom/osp/app/signin/TnCView;)Ljava/lang/String;

    move-result-object v4

    iget-object v5, p0, Lcom/osp/app/signin/ob;->c:Lcom/osp/app/signin/TnCView;

    invoke-static {v5}, Lcom/osp/app/signin/TnCView;->J(Lcom/osp/app/signin/TnCView;)Z

    move-result v5

    const/4 v6, 0x0

    const/4 v7, 0x0

    invoke-static/range {v0 .. v7}, Lcom/osp/app/util/ad;->a(Landroid/content/Context;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;ZZLjava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    .line 4551
    iget-object v1, p0, Lcom/osp/app/signin/ob;->c:Lcom/osp/app/signin/TnCView;

    invoke-static {v1}, Lcom/osp/app/signin/TnCView;->F(Lcom/osp/app/signin/TnCView;)Z

    move-result v1

    if-eqz v1, :cond_3

    .line 4553
    const-string v1, "key_return_result"

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 4554
    iget-object v1, p0, Lcom/osp/app/signin/ob;->c:Lcom/osp/app/signin/TnCView;

    const/16 v2, 0xdc

    invoke-virtual {v1, v0, v2}, Lcom/osp/app/signin/TnCView;->startActivityForResult(Landroid/content/Intent;I)V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    goto :goto_0

    .line 4513
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 4531
    :catch_0
    move-exception v0

    .line 4533
    :try_start_4
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    .line 4534
    new-instance v2, Lcom/msc/c/f;

    invoke-direct {v2, v0}, Lcom/msc/c/f;-><init>(Ljava/lang/Exception;)V

    iput-object v2, p0, Lcom/osp/app/signin/ob;->b:Lcom/msc/c/f;

    move-object v0, v1

    goto :goto_1

    .line 4557
    :cond_3
    iget-object v1, p0, Lcom/osp/app/signin/ob;->c:Lcom/osp/app/signin/TnCView;

    invoke-virtual {v1, v0}, Lcom/osp/app/signin/TnCView;->startActivity(Landroid/content/Intent;)V

    goto/16 :goto_0

    .line 4562
    :cond_4
    const-string v0, "Fail"

    iput-object v0, p0, Lcom/osp/app/signin/ob;->e:Ljava/lang/String;
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    goto/16 :goto_0
.end method

.method protected final a(Ljava/lang/Boolean;)V
    .locals 4

    .prologue
    const/4 v2, 0x0

    const/4 v3, -0x1

    .line 4470
    invoke-super {p0, p1}, Lcom/msc/c/b;->a(Ljava/lang/Boolean;)V

    .line 4471
    iget-object v0, p0, Lcom/osp/app/signin/ob;->b:Lcom/msc/c/f;

    if-eqz v0, :cond_1

    .line 4473
    invoke-static {}, Lcom/osp/device/b;->b()Lcom/osp/device/b;

    move-result-object v0

    invoke-virtual {v0}, Lcom/osp/device/b;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    const-string v0, "USR_3174"

    iget-object v1, p0, Lcom/osp/app/signin/ob;->b:Lcom/msc/c/f;

    invoke-virtual {v1}, Lcom/msc/c/f;->a()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 4475
    iget-object v0, p0, Lcom/osp/app/signin/ob;->c:Lcom/osp/app/signin/TnCView;

    iget-object v1, p0, Lcom/osp/app/signin/ob;->c:Lcom/osp/app/signin/TnCView;

    invoke-static {v1}, Lcom/osp/app/util/ad;->h(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/osp/app/signin/TnCView;->a(Lcom/osp/app/signin/TnCView;Ljava/lang/String;)V

    .line 4476
    iget-object v0, p0, Lcom/osp/app/signin/ob;->c:Lcom/osp/app/signin/TnCView;

    invoke-virtual {v0}, Lcom/osp/app/signin/TnCView;->finish()V

    .line 4509
    :goto_0
    return-void

    .line 4479
    :cond_0
    invoke-virtual {p0, v2}, Lcom/osp/app/signin/ob;->a(Z)V

    goto :goto_0

    .line 4483
    :cond_1
    const-string v0, "Success"

    iget-object v1, p0, Lcom/osp/app/signin/ob;->e:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 4485
    iget-object v0, p0, Lcom/osp/app/signin/ob;->c:Lcom/osp/app/signin/TnCView;

    invoke-static {v0}, Lcom/osp/app/signin/TnCView;->F(Lcom/osp/app/signin/TnCView;)Z

    move-result v0

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/osp/app/signin/ob;->c:Lcom/osp/app/signin/TnCView;

    invoke-static {v0}, Lcom/osp/app/signin/TnCView;->G(Lcom/osp/app/signin/TnCView;)I

    move-result v0

    if-lez v0, :cond_2

    .line 4487
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v0, "TNC"

    const-string v1, "don\'t finish this activity now"

    invoke-static {v0, v1}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 4490
    :cond_2
    iget-object v0, p0, Lcom/osp/app/signin/ob;->c:Lcom/osp/app/signin/TnCView;

    const-string v1, "notification"

    invoke-virtual {v0, v1}, Lcom/osp/app/signin/TnCView;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/NotificationManager;

    .line 4491
    const v1, 0x132df82

    invoke-virtual {v0, v1}, Landroid/app/NotificationManager;->cancel(I)V

    .line 4493
    iget-object v0, p0, Lcom/osp/app/signin/ob;->c:Lcom/osp/app/signin/TnCView;

    invoke-static {v0}, Lcom/osp/app/signin/TnCView;->i(Lcom/osp/app/signin/TnCView;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 4495
    iget-object v0, p0, Lcom/osp/app/signin/ob;->c:Lcom/osp/app/signin/TnCView;

    invoke-virtual {v0}, Lcom/osp/app/signin/TnCView;->getIntent()Landroid/content/Intent;

    move-result-object v0

    .line 4496
    const-string v1, "key_easy_signup_mo_auth_mode"

    iget-object v2, p0, Lcom/osp/app/signin/ob;->c:Lcom/osp/app/signin/TnCView;

    invoke-static {v2}, Lcom/osp/app/signin/TnCView;->a(Lcom/osp/app/signin/TnCView;)Z

    move-result v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 4497
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "PROCESSING_SUCCESS mEasySignupMoAuthMode - mIsEsuTncAcceptedChecked : "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, Lcom/osp/app/signin/ob;->c:Lcom/osp/app/signin/TnCView;

    invoke-static {v2}, Lcom/osp/app/signin/TnCView;->a(Lcom/osp/app/signin/TnCView;)Z

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;)V

    .line 4498
    iget-object v1, p0, Lcom/osp/app/signin/ob;->c:Lcom/osp/app/signin/TnCView;

    invoke-virtual {v1, v3, v0}, Lcom/osp/app/signin/TnCView;->a(ILandroid/content/Intent;)V

    .line 4503
    :goto_1
    iget-object v0, p0, Lcom/osp/app/signin/ob;->c:Lcom/osp/app/signin/TnCView;

    invoke-virtual {v0}, Lcom/osp/app/signin/TnCView;->finish()V

    goto :goto_0

    .line 4501
    :cond_3
    iget-object v0, p0, Lcom/osp/app/signin/ob;->c:Lcom/osp/app/signin/TnCView;

    invoke-virtual {v0, v3}, Lcom/osp/app/signin/TnCView;->b(I)V

    goto :goto_1

    .line 4507
    :cond_4
    invoke-virtual {p0, v2}, Lcom/osp/app/signin/ob;->a(Z)V

    goto/16 :goto_0
.end method

.method public final b(Lcom/msc/b/c;)V
    .locals 4

    .prologue
    .line 4569
    invoke-super {p0, p1}, Lcom/msc/c/b;->b(Lcom/msc/b/c;)V

    .line 4571
    if-nez p1, :cond_1

    .line 4582
    :cond_0
    :goto_0
    return-void

    .line 4576
    :cond_1
    invoke-virtual {p1}, Lcom/msc/b/c;->a()J

    move-result-wide v0

    .line 4578
    iget-wide v2, p0, Lcom/osp/app/signin/ob;->d:J

    cmp-long v0, v0, v2

    if-nez v0, :cond_0

    .line 4580
    const-string v0, "Fail"

    iput-object v0, p0, Lcom/osp/app/signin/ob;->e:Ljava/lang/String;

    goto :goto_0
.end method

.method protected final synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 4405
    invoke-virtual {p0}, Lcom/osp/app/signin/ob;->a()Ljava/lang/Boolean;

    move-result-object v0

    return-object v0
.end method

.method protected final synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 4405
    check-cast p1, Ljava/lang/Boolean;

    invoke-virtual {p0, p1}, Lcom/osp/app/signin/ob;->a(Ljava/lang/Boolean;)V

    return-void
.end method
