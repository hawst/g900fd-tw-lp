.class final Lcom/osp/app/signin/lw;
.super Ljava/lang/Object;
.source "SignUpView.java"

# interfaces
.implements Landroid/widget/CompoundButton$OnCheckedChangeListener;


# instance fields
.field final synthetic a:Lcom/osp/app/signin/SignUpView;


# direct methods
.method constructor <init>(Lcom/osp/app/signin/SignUpView;)V
    .locals 0

    .prologue
    .line 1688
    iput-object p1, p0, Lcom/osp/app/signin/lw;->a:Lcom/osp/app/signin/SignUpView;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onCheckedChanged(Landroid/widget/CompoundButton;Z)V
    .locals 4

    .prologue
    const/16 v3, 0x91

    const/16 v2, 0x81

    .line 1694
    iget-object v0, p0, Lcom/osp/app/signin/lw;->a:Lcom/osp/app/signin/SignUpView;

    invoke-static {v0}, Lcom/osp/app/signin/SignUpView;->l(Lcom/osp/app/signin/SignUpView;)Landroid/widget/CheckBox;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/CheckBox;->playSoundEffect(I)V

    .line 1696
    const/4 v0, 0x1

    if-ne p2, v0, :cond_2

    .line 1698
    iget-object v0, p0, Lcom/osp/app/signin/lw;->a:Lcom/osp/app/signin/SignUpView;

    invoke-static {v0}, Lcom/osp/app/signin/SignUpView;->m(Lcom/osp/app/signin/SignUpView;)Landroid/widget/EditText;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/EditText;->getSelectionEnd()I

    move-result v0

    .line 1699
    iget-object v1, p0, Lcom/osp/app/signin/lw;->a:Lcom/osp/app/signin/SignUpView;

    invoke-static {v1}, Lcom/osp/app/signin/SignUpView;->m(Lcom/osp/app/signin/SignUpView;)Landroid/widget/EditText;

    move-result-object v1

    invoke-virtual {v1, v3}, Landroid/widget/EditText;->setInputType(I)V

    .line 1700
    if-ltz v0, :cond_0

    .line 1702
    iget-object v1, p0, Lcom/osp/app/signin/lw;->a:Lcom/osp/app/signin/SignUpView;

    invoke-static {v1}, Lcom/osp/app/signin/SignUpView;->m(Lcom/osp/app/signin/SignUpView;)Landroid/widget/EditText;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/widget/EditText;->setSelection(I)V

    .line 1704
    :cond_0
    iget-object v0, p0, Lcom/osp/app/signin/lw;->a:Lcom/osp/app/signin/SignUpView;

    invoke-static {v0}, Lcom/osp/app/signin/SignUpView;->n(Lcom/osp/app/signin/SignUpView;)Landroid/widget/EditText;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/EditText;->getSelectionEnd()I

    move-result v0

    .line 1705
    iget-object v1, p0, Lcom/osp/app/signin/lw;->a:Lcom/osp/app/signin/SignUpView;

    invoke-static {v1}, Lcom/osp/app/signin/SignUpView;->n(Lcom/osp/app/signin/SignUpView;)Landroid/widget/EditText;

    move-result-object v1

    invoke-virtual {v1, v3}, Landroid/widget/EditText;->setInputType(I)V

    .line 1706
    if-ltz v0, :cond_1

    .line 1708
    iget-object v1, p0, Lcom/osp/app/signin/lw;->a:Lcom/osp/app/signin/SignUpView;

    invoke-static {v1}, Lcom/osp/app/signin/SignUpView;->n(Lcom/osp/app/signin/SignUpView;)Landroid/widget/EditText;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/widget/EditText;->setSelection(I)V

    .line 1725
    :cond_1
    :goto_0
    return-void

    .line 1712
    :cond_2
    iget-object v0, p0, Lcom/osp/app/signin/lw;->a:Lcom/osp/app/signin/SignUpView;

    invoke-static {v0}, Lcom/osp/app/signin/SignUpView;->m(Lcom/osp/app/signin/SignUpView;)Landroid/widget/EditText;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/EditText;->getSelectionEnd()I

    move-result v0

    .line 1713
    iget-object v1, p0, Lcom/osp/app/signin/lw;->a:Lcom/osp/app/signin/SignUpView;

    invoke-static {v1}, Lcom/osp/app/signin/SignUpView;->m(Lcom/osp/app/signin/SignUpView;)Landroid/widget/EditText;

    move-result-object v1

    invoke-virtual {v1, v2}, Landroid/widget/EditText;->setInputType(I)V

    .line 1714
    if-ltz v0, :cond_3

    .line 1716
    iget-object v1, p0, Lcom/osp/app/signin/lw;->a:Lcom/osp/app/signin/SignUpView;

    invoke-static {v1}, Lcom/osp/app/signin/SignUpView;->m(Lcom/osp/app/signin/SignUpView;)Landroid/widget/EditText;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/widget/EditText;->setSelection(I)V

    .line 1718
    :cond_3
    iget-object v0, p0, Lcom/osp/app/signin/lw;->a:Lcom/osp/app/signin/SignUpView;

    invoke-static {v0}, Lcom/osp/app/signin/SignUpView;->n(Lcom/osp/app/signin/SignUpView;)Landroid/widget/EditText;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/EditText;->getSelectionEnd()I

    move-result v0

    .line 1719
    iget-object v1, p0, Lcom/osp/app/signin/lw;->a:Lcom/osp/app/signin/SignUpView;

    invoke-static {v1}, Lcom/osp/app/signin/SignUpView;->n(Lcom/osp/app/signin/SignUpView;)Landroid/widget/EditText;

    move-result-object v1

    invoke-virtual {v1, v2}, Landroid/widget/EditText;->setInputType(I)V

    .line 1720
    if-ltz v0, :cond_1

    .line 1722
    iget-object v1, p0, Lcom/osp/app/signin/lw;->a:Lcom/osp/app/signin/SignUpView;

    invoke-static {v1}, Lcom/osp/app/signin/SignUpView;->n(Lcom/osp/app/signin/SignUpView;)Landroid/widget/EditText;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/widget/EditText;->setSelection(I)V

    goto :goto_0
.end method
