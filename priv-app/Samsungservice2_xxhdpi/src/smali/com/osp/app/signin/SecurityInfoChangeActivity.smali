.class public Lcom/osp/app/signin/SecurityInfoChangeActivity;
.super Lcom/osp/app/util/BaseActivity;
.source "SecurityInfoChangeActivity.java"


# instance fields
.field private A:Ljava/lang/String;

.field private B:Ljava/lang/String;

.field private a:Landroid/widget/Button;

.field private b:Landroid/widget/EditText;

.field private c:Landroid/widget/Button;

.field private d:Landroid/widget/Button;

.field private e:Landroid/app/AlertDialog;

.field private f:Lcom/msc/a/j;

.field private y:Lcom/osp/app/signin/hh;

.field private z:Lcom/osp/app/signin/hg;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 52
    invoke-direct {p0}, Lcom/osp/app/util/BaseActivity;-><init>()V

    .line 104
    iput-object v0, p0, Lcom/osp/app/signin/SecurityInfoChangeActivity;->f:Lcom/msc/a/j;

    .line 109
    iput-object v0, p0, Lcom/osp/app/signin/SecurityInfoChangeActivity;->y:Lcom/osp/app/signin/hh;

    .line 114
    iput-object v0, p0, Lcom/osp/app/signin/SecurityInfoChangeActivity;->z:Lcom/osp/app/signin/hg;

    .line 119
    iput-object v0, p0, Lcom/osp/app/signin/SecurityInfoChangeActivity;->A:Ljava/lang/String;

    .line 124
    iput-object v0, p0, Lcom/osp/app/signin/SecurityInfoChangeActivity;->B:Ljava/lang/String;

    .line 926
    return-void
.end method

.method static synthetic a(Lcom/osp/app/signin/SecurityInfoChangeActivity;)Lcom/msc/a/j;
    .locals 1

    .prologue
    .line 52
    iget-object v0, p0, Lcom/osp/app/signin/SecurityInfoChangeActivity;->f:Lcom/msc/a/j;

    return-object v0
.end method

.method static synthetic a(Lcom/osp/app/signin/SecurityInfoChangeActivity;Lcom/msc/a/j;)Lcom/msc/a/j;
    .locals 0

    .prologue
    .line 52
    iput-object p1, p0, Lcom/osp/app/signin/SecurityInfoChangeActivity;->f:Lcom/msc/a/j;

    return-object p1
.end method

.method static synthetic a(Lcom/osp/app/signin/SecurityInfoChangeActivity;Lcom/osp/app/signin/hg;)Lcom/osp/app/signin/hg;
    .locals 0

    .prologue
    .line 52
    iput-object p1, p0, Lcom/osp/app/signin/SecurityInfoChangeActivity;->z:Lcom/osp/app/signin/hg;

    return-object p1
.end method

.method private a(I)V
    .locals 4

    .prologue
    const v3, 0x7f08011e

    .line 492
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v0, "SICA"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "initLayoutParams orientation=["

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "]"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    const-string v2, "START"

    invoke-static {v0, v1, v2}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 494
    invoke-static {}, Lcom/osp/device/b;->b()Lcom/osp/device/b;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/osp/device/b;->i(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 496
    invoke-virtual {p0}, Lcom/osp/app/signin/SecurityInfoChangeActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f08011f

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v0

    float-to-int v0, v0

    .line 497
    invoke-virtual {p0}, Lcom/osp/app/signin/SecurityInfoChangeActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1, v3}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v1

    float-to-int v1, v1

    .line 498
    invoke-static {}, Lcom/osp/app/signin/SamsungService;->a()Z

    move-result v2

    if-nez v2, :cond_0

    .line 500
    invoke-virtual {p0}, Lcom/osp/app/signin/SecurityInfoChangeActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0, v3}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v0

    float-to-int v0, v0

    .line 503
    :cond_0
    const v2, 0x7f0c0173

    invoke-virtual {p0, v2}, Lcom/osp/app/signin/SecurityInfoChangeActivity;->findViewById(I)Landroid/view/View;

    move-result-object v2

    .line 504
    invoke-static {}, Lcom/osp/app/util/q;->a()Lcom/osp/app/util/q;

    invoke-static {v2, v0, v1, p1}, Lcom/osp/app/util/q;->b(Landroid/view/View;III)V

    .line 507
    :cond_1
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v0, "SICA"

    const-string v1, "initLayoutParams"

    const-string v2, "END"

    invoke-static {v0, v1, v2}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 508
    return-void
.end method

.method static synthetic a(Lcom/osp/app/signin/SecurityInfoChangeActivity;Ljava/lang/String;)V
    .locals 3

    .prologue
    .line 52
    new-instance v0, Landroid/content/Intent;

    const-string v1, "com.msc.action.samsungaccount.resignin"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    const-string v1, "client_id"

    iget-object v2, p0, Lcom/osp/app/signin/SecurityInfoChangeActivity;->A:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    const-string v1, "client_secret"

    iget-object v2, p0, Lcom/osp/app/signin/SecurityInfoChangeActivity;->B:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    const-string v1, "OSP_VER"

    const-string v2, "OSP_02"

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    if-eqz p1, :cond_0

    const-string v1, "email_id"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    :cond_0
    invoke-virtual {p0, v0}, Lcom/osp/app/signin/SecurityInfoChangeActivity;->startActivity(Landroid/content/Intent;)V

    return-void
.end method

.method static synthetic b(Lcom/osp/app/signin/SecurityInfoChangeActivity;)Landroid/widget/Button;
    .locals 1

    .prologue
    .line 52
    iget-object v0, p0, Lcom/osp/app/signin/SecurityInfoChangeActivity;->a:Landroid/widget/Button;

    return-object v0
.end method

.method static synthetic c(Lcom/osp/app/signin/SecurityInfoChangeActivity;)Landroid/widget/EditText;
    .locals 1

    .prologue
    .line 52
    iget-object v0, p0, Lcom/osp/app/signin/SecurityInfoChangeActivity;->b:Landroid/widget/EditText;

    return-object v0
.end method

.method private c()V
    .locals 3

    .prologue
    .line 130
    const-string v0, "input_method"

    invoke-virtual {p0, v0}, Lcom/osp/app/signin/SecurityInfoChangeActivity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/inputmethod/InputMethodManager;

    .line 132
    iget-object v1, p0, Lcom/osp/app/signin/SecurityInfoChangeActivity;->b:Landroid/widget/EditText;

    invoke-virtual {v1}, Landroid/widget/EditText;->hasFocus()Z

    move-result v1

    const/4 v2, 0x1

    if-ne v1, v2, :cond_0

    .line 134
    iget-object v1, p0, Lcom/osp/app/signin/SecurityInfoChangeActivity;->b:Landroid/widget/EditText;

    invoke-virtual {v1}, Landroid/widget/EditText;->getWindowToken()Landroid/os/IBinder;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/view/inputmethod/InputMethodManager;->hideSoftInputFromWindow(Landroid/os/IBinder;I)Z

    .line 136
    :cond_0
    return-void
.end method

.method static synthetic d(Lcom/osp/app/signin/SecurityInfoChangeActivity;)V
    .locals 0

    .prologue
    .line 52
    invoke-direct {p0}, Lcom/osp/app/signin/SecurityInfoChangeActivity;->c()V

    return-void
.end method

.method static synthetic e(Lcom/osp/app/signin/SecurityInfoChangeActivity;)Lcom/osp/app/signin/hg;
    .locals 1

    .prologue
    .line 52
    iget-object v0, p0, Lcom/osp/app/signin/SecurityInfoChangeActivity;->z:Lcom/osp/app/signin/hg;

    return-object v0
.end method

.method static synthetic f(Lcom/osp/app/signin/SecurityInfoChangeActivity;)V
    .locals 3

    .prologue
    .line 52
    new-instance v0, Landroid/content/Intent;

    const-string v1, "com.msc.action.samsungaccount.resignin_with_signout"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    const-string v1, "client_id"

    iget-object v2, p0, Lcom/osp/app/signin/SecurityInfoChangeActivity;->A:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    const-string v1, "client_secret"

    iget-object v2, p0, Lcom/osp/app/signin/SecurityInfoChangeActivity;->B:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    const-string v1, "OSP_VER"

    const-string v2, "OSP_02"

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    invoke-virtual {p0, v0}, Lcom/osp/app/signin/SecurityInfoChangeActivity;->startActivity(Landroid/content/Intent;)V

    return-void
.end method


# virtual methods
.method public final a()V
    .locals 4

    .prologue
    const v3, 0x7f0c018f

    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 306
    iget-object v0, p0, Lcom/osp/app/signin/SecurityInfoChangeActivity;->f:Lcom/msc/a/j;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/osp/app/signin/SecurityInfoChangeActivity;->f:Lcom/msc/a/j;

    invoke-virtual {v0}, Lcom/msc/a/j;->b()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/osp/app/signin/SecurityInfoChangeActivity;->b:Landroid/widget/EditText;

    invoke-virtual {v0}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-interface {v0}, Landroid/text/Editable;->length()I

    move-result v0

    if-nez v0, :cond_3

    .line 308
    :cond_0
    iget-object v0, p0, Lcom/osp/app/signin/SecurityInfoChangeActivity;->c:Landroid/widget/Button;

    if-eqz v0, :cond_1

    .line 310
    iget-object v0, p0, Lcom/osp/app/signin/SecurityInfoChangeActivity;->c:Landroid/widget/Button;

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setEnabled(Z)V

    .line 312
    :cond_1
    iget-object v0, p0, Lcom/osp/app/signin/SecurityInfoChangeActivity;->x:Landroid/view/Menu;

    if-eqz v0, :cond_2

    .line 314
    iget-object v0, p0, Lcom/osp/app/signin/SecurityInfoChangeActivity;->x:Landroid/view/Menu;

    invoke-interface {v0, v3}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v0

    invoke-interface {v0, v1}, Landroid/view/MenuItem;->setEnabled(Z)Landroid/view/MenuItem;

    .line 327
    :cond_2
    :goto_0
    return-void

    .line 318
    :cond_3
    iget-object v0, p0, Lcom/osp/app/signin/SecurityInfoChangeActivity;->c:Landroid/widget/Button;

    if-eqz v0, :cond_4

    .line 320
    iget-object v0, p0, Lcom/osp/app/signin/SecurityInfoChangeActivity;->c:Landroid/widget/Button;

    invoke-virtual {v0, v2}, Landroid/widget/Button;->setEnabled(Z)V

    .line 322
    :cond_4
    iget-object v0, p0, Lcom/osp/app/signin/SecurityInfoChangeActivity;->x:Landroid/view/Menu;

    if-eqz v0, :cond_2

    .line 324
    iget-object v0, p0, Lcom/osp/app/signin/SecurityInfoChangeActivity;->x:Landroid/view/Menu;

    invoke-interface {v0, v3}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v0

    invoke-interface {v0, v2}, Landroid/view/MenuItem;->setEnabled(Z)Landroid/view/MenuItem;

    goto :goto_0
.end method

.method protected final b()V
    .locals 0

    .prologue
    .line 540
    return-void
.end method

.method protected onActivityResult(IILandroid/content/Intent;)V
    .locals 4

    .prologue
    const/16 v3, 0x10

    .line 461
    invoke-super {p0, p1, p2, p3}, Lcom/osp/app/util/BaseActivity;->onActivityResult(IILandroid/content/Intent;)V

    .line 463
    if-ne p2, v3, :cond_0

    .line 465
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v0, "SICA"

    const-string v1, "PasswordChangeView - onActivityResult - requestCode :"

    const-string v2, "RESULT_EXPIRED_TOKEN"

    invoke-static {v0, v1, v2}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 466
    invoke-virtual {p0, v3}, Lcom/osp/app/signin/SecurityInfoChangeActivity;->b(I)V

    .line 467
    invoke-virtual {p0}, Lcom/osp/app/signin/SecurityInfoChangeActivity;->finish()V

    .line 472
    :goto_0
    return-void

    .line 470
    :cond_0
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "PasswordChangeView - onActivityResult - requestCode :"

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;)V

    .line 471
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "PasswordChangeView - onActivityResult - resultCode :"

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public onConfigurationChanged(Landroid/content/res/Configuration;)V
    .locals 3

    .prologue
    .line 476
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v0, "SICA"

    const-string v1, "onConfigurationChanged"

    const-string v2, "START"

    invoke-static {v0, v1, v2}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 478
    invoke-super {p0, p1}, Lcom/osp/app/util/BaseActivity;->onConfigurationChanged(Landroid/content/res/Configuration;)V

    .line 481
    iget v0, p1, Landroid/content/res/Configuration;->orientation:I

    invoke-direct {p0, v0}, Lcom/osp/app/signin/SecurityInfoChangeActivity;->a(I)V

    .line 483
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v0, "SICA"

    const-string v1, "onConfigurationChanged"

    const-string v2, "END"

    invoke-static {v0, v1, v2}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 484
    return-void
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 7

    .prologue
    const v6, 0x7f0c0115

    const v5, 0x7f070006

    const v4, 0x7f0c0174

    const v3, 0x7f0c0119

    .line 341
    invoke-super {p0, p1}, Lcom/osp/app/util/BaseActivity;->onCreate(Landroid/os/Bundle;)V

    .line 342
    invoke-virtual {p0}, Lcom/osp/app/signin/SecurityInfoChangeActivity;->getApplication()Landroid/app/Application;

    move-result-object v0

    check-cast v0, Lcom/osp/app/signin/SamsungService;

    invoke-virtual {v0, p0}, Lcom/osp/app/signin/SamsungService;->a(Landroid/app/Activity;)V

    .line 344
    const v0, 0x7f03002b

    invoke-static {p0}, Lcom/osp/app/util/r;->q(Landroid/content/Context;)Z

    move-result v1

    invoke-virtual {p0, v0, v1}, Lcom/osp/app/signin/SecurityInfoChangeActivity;->a(IZ)V

    .line 347
    invoke-static {p0}, Lcom/osp/app/util/ad;->d(Landroid/content/Context;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 349
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/osp/app/signin/SecurityInfoChangeActivity;->b(I)V

    .line 350
    invoke-virtual {p0}, Lcom/osp/app/signin/SecurityInfoChangeActivity;->finish()V

    .line 386
    :goto_0
    return-void

    .line 354
    :cond_0
    invoke-virtual {p0}, Lcom/osp/app/signin/SecurityInfoChangeActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string v1, "client_id"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/osp/app/signin/SecurityInfoChangeActivity;->A:Ljava/lang/String;

    const-string v1, "client_secret"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/osp/app/signin/SecurityInfoChangeActivity;->B:Ljava/lang/String;

    .line 355
    invoke-virtual {p0}, Lcom/osp/app/signin/SecurityInfoChangeActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v0, 0x7f0c0112

    invoke-virtual {p0, v0}, Lcom/osp/app/signin/SecurityInfoChangeActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    if-eqz v0, :cond_1

    invoke-static {p0}, Lcom/osp/app/util/an;->a(Lcom/osp/app/util/BaseActivity;)Lcom/osp/app/util/an;

    invoke-static {v5}, Lcom/osp/app/util/an;->a(I)I

    move-result v2

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v2

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setTextColor(I)V

    :cond_1
    const v0, 0x7f0c0117

    invoke-virtual {p0, v0}, Lcom/osp/app/signin/SecurityInfoChangeActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    if-eqz v0, :cond_2

    invoke-static {p0}, Lcom/osp/app/util/an;->a(Lcom/osp/app/util/BaseActivity;)Lcom/osp/app/util/an;

    invoke-static {v5}, Lcom/osp/app/util/an;->a(I)I

    move-result v2

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setTextColor(I)V

    :cond_2
    iget-object v0, p0, Lcom/osp/app/signin/SecurityInfoChangeActivity;->a:Landroid/widget/Button;

    if-nez v0, :cond_3

    const v0, 0x7f0c0114

    invoke-virtual {p0, v0}, Lcom/osp/app/signin/SecurityInfoChangeActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lcom/osp/app/signin/SecurityInfoChangeActivity;->a:Landroid/widget/Button;

    :cond_3
    iget-object v0, p0, Lcom/osp/app/signin/SecurityInfoChangeActivity;->b:Landroid/widget/EditText;

    if-nez v0, :cond_4

    const v0, 0x7f0c00b1

    invoke-virtual {p0, v0}, Lcom/osp/app/signin/SecurityInfoChangeActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/EditText;

    iput-object v0, p0, Lcom/osp/app/signin/SecurityInfoChangeActivity;->b:Landroid/widget/EditText;

    :cond_4
    invoke-virtual {p0, v6}, Lcom/osp/app/signin/SecurityInfoChangeActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    if-eqz v0, :cond_5

    invoke-static {p0}, Lcom/osp/app/util/an;->a(Lcom/osp/app/util/BaseActivity;)Lcom/osp/app/util/an;

    const v1, 0x7f02003b

    invoke-static {v1}, Lcom/osp/app/util/an;->a(I)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageResource(I)V

    invoke-static {v0}, Lcom/msc/sa/c/d;->a(Landroid/view/View;)V

    :cond_5
    iget-object v0, p0, Lcom/osp/app/signin/SecurityInfoChangeActivity;->c:Landroid/widget/Button;

    if-nez v0, :cond_6

    invoke-virtual {p0, v3}, Lcom/osp/app/signin/SecurityInfoChangeActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lcom/osp/app/signin/SecurityInfoChangeActivity;->c:Landroid/widget/Button;

    :cond_6
    iget-object v0, p0, Lcom/osp/app/signin/SecurityInfoChangeActivity;->d:Landroid/widget/Button;

    if-nez v0, :cond_7

    invoke-virtual {p0, v4}, Lcom/osp/app/signin/SecurityInfoChangeActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lcom/osp/app/signin/SecurityInfoChangeActivity;->d:Landroid/widget/Button;

    .line 357
    :cond_7
    invoke-virtual {p0}, Lcom/osp/app/signin/SecurityInfoChangeActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v0

    iget v0, v0, Landroid/content/res/Configuration;->orientation:I

    invoke-direct {p0, v0}, Lcom/osp/app/signin/SecurityInfoChangeActivity;->a(I)V

    .line 377
    invoke-static {p0}, Lcom/msc/sa/c/d;->a(Landroid/app/Activity;)V

    .line 379
    iget-object v0, p0, Lcom/osp/app/signin/SecurityInfoChangeActivity;->a:Landroid/widget/Button;

    new-instance v1, Lcom/osp/app/signin/ha;

    invoke-direct {v1, p0}, Lcom/osp/app/signin/ha;-><init>(Lcom/osp/app/signin/SecurityInfoChangeActivity;)V

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object v0, p0, Lcom/osp/app/signin/SecurityInfoChangeActivity;->a:Landroid/widget/Button;

    new-instance v1, Lcom/osp/app/signin/hb;

    invoke-direct {v1, p0}, Lcom/osp/app/signin/hb;-><init>(Lcom/osp/app/signin/SecurityInfoChangeActivity;)V

    invoke-virtual {v0, v1}, Landroid/widget/Button;->addTextChangedListener(Landroid/text/TextWatcher;)V

    iget-object v0, p0, Lcom/osp/app/signin/SecurityInfoChangeActivity;->f:Lcom/msc/a/j;

    if-eqz v0, :cond_9

    invoke-static {}, Lcom/osp/app/util/y;->a()Lcom/osp/app/util/y;

    move-result-object v0

    iget-object v1, p0, Lcom/osp/app/signin/SecurityInfoChangeActivity;->f:Lcom/msc/a/j;

    invoke-virtual {v1}, Lcom/msc/a/j;->a()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/osp/app/util/y;->a(Ljava/lang/String;)I

    move-result v0

    if-lez v0, :cond_e

    iget-object v1, p0, Lcom/osp/app/signin/SecurityInfoChangeActivity;->a:Landroid/widget/Button;

    invoke-virtual {v1, v0}, Landroid/widget/Button;->setText(I)V

    :cond_8
    :goto_1
    iget-object v0, p0, Lcom/osp/app/signin/SecurityInfoChangeActivity;->b:Landroid/widget/EditText;

    iget-object v1, p0, Lcom/osp/app/signin/SecurityInfoChangeActivity;->f:Lcom/msc/a/j;

    invoke-virtual {v1}, Lcom/msc/a/j;->c()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    iget-object v0, p0, Lcom/osp/app/signin/SecurityInfoChangeActivity;->b:Landroid/widget/EditText;

    iget-object v1, p0, Lcom/osp/app/signin/SecurityInfoChangeActivity;->b:Landroid/widget/EditText;

    invoke-virtual {v1}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v1

    invoke-interface {v1}, Landroid/text/Editable;->length()I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setSelection(I)V

    :cond_9
    iget-object v0, p0, Lcom/osp/app/signin/SecurityInfoChangeActivity;->b:Landroid/widget/EditText;

    new-instance v1, Lcom/osp/app/signin/hc;

    invoke-direct {v1, p0}, Lcom/osp/app/signin/hc;-><init>(Lcom/osp/app/signin/SecurityInfoChangeActivity;)V

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->addTextChangedListener(Landroid/text/TextWatcher;)V

    iget-object v0, p0, Lcom/osp/app/signin/SecurityInfoChangeActivity;->d:Landroid/widget/Button;

    if-eqz v0, :cond_a

    invoke-static {}, Lcom/osp/device/b;->b()Lcom/osp/device/b;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/osp/device/b;->l(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_f

    invoke-virtual {p0, v3}, Lcom/osp/app/signin/SecurityInfoChangeActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lcom/osp/app/signin/SecurityInfoChangeActivity;->c:Landroid/widget/Button;

    invoke-virtual {p0, v4}, Lcom/osp/app/signin/SecurityInfoChangeActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lcom/osp/app/signin/SecurityInfoChangeActivity;->d:Landroid/widget/Button;

    :cond_a
    :goto_2
    iget-object v0, p0, Lcom/osp/app/signin/SecurityInfoChangeActivity;->c:Landroid/widget/Button;

    new-instance v1, Lcom/osp/app/signin/hd;

    invoke-direct {v1, p0}, Lcom/osp/app/signin/hd;-><init>(Lcom/osp/app/signin/SecurityInfoChangeActivity;)V

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object v0, p0, Lcom/osp/app/signin/SecurityInfoChangeActivity;->d:Landroid/widget/Button;

    if-eqz v0, :cond_b

    iget-object v0, p0, Lcom/osp/app/signin/SecurityInfoChangeActivity;->d:Landroid/widget/Button;

    new-instance v1, Lcom/osp/app/signin/he;

    invoke-direct {v1, p0}, Lcom/osp/app/signin/he;-><init>(Lcom/osp/app/signin/SecurityInfoChangeActivity;)V

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    :cond_b
    invoke-virtual {p0, v6}, Lcom/osp/app/signin/SecurityInfoChangeActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    const v1, 0x7f0c0113

    invoke-virtual {p0, v1}, Lcom/osp/app/signin/SecurityInfoChangeActivity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/LinearLayout;

    if-eqz v0, :cond_c

    if-eqz v1, :cond_c

    new-instance v1, Lcom/osp/app/signin/hf;

    invoke-direct {v1, p0}, Lcom/osp/app/signin/hf;-><init>(Lcom/osp/app/signin/SecurityInfoChangeActivity;)V

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    :cond_c
    const v0, 0x7f0c0028

    invoke-virtual {p0, v0}, Lcom/osp/app/signin/SecurityInfoChangeActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    if-eqz v0, :cond_d

    sget v1, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v2, 0xb

    if-ge v1, v2, :cond_10

    const v1, 0x108009a

    invoke-virtual {v0, v1}, Landroid/view/View;->setBackgroundResource(I)V

    .line 380
    :cond_d
    :goto_3
    invoke-virtual {p0}, Lcom/osp/app/signin/SecurityInfoChangeActivity;->a()V

    .line 382
    new-instance v0, Lcom/osp/app/signin/hh;

    invoke-direct {v0, p0}, Lcom/osp/app/signin/hh;-><init>(Lcom/osp/app/signin/SecurityInfoChangeActivity;)V

    iput-object v0, p0, Lcom/osp/app/signin/SecurityInfoChangeActivity;->y:Lcom/osp/app/signin/hh;

    .line 383
    iget-object v0, p0, Lcom/osp/app/signin/SecurityInfoChangeActivity;->y:Lcom/osp/app/signin/hh;

    invoke-virtual {v0}, Lcom/osp/app/signin/hh;->b()V

    .line 385
    const-string v0, "save_cancel"

    invoke-virtual {p0, v0}, Lcom/osp/app/signin/SecurityInfoChangeActivity;->e(Ljava/lang/String;)V

    goto/16 :goto_0

    .line 379
    :cond_e
    iget-object v0, p0, Lcom/osp/app/signin/SecurityInfoChangeActivity;->f:Lcom/msc/a/j;

    invoke-virtual {v0}, Lcom/msc/a/j;->b()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_8

    iget-object v0, p0, Lcom/osp/app/signin/SecurityInfoChangeActivity;->a:Landroid/widget/Button;

    iget-object v1, p0, Lcom/osp/app/signin/SecurityInfoChangeActivity;->f:Lcom/msc/a/j;

    invoke-virtual {v1}, Lcom/msc/a/j;->b()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setText(Ljava/lang/CharSequence;)V

    goto/16 :goto_1

    :cond_f
    invoke-virtual {p0, v4}, Lcom/osp/app/signin/SecurityInfoChangeActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lcom/osp/app/signin/SecurityInfoChangeActivity;->c:Landroid/widget/Button;

    invoke-virtual {p0, v3}, Lcom/osp/app/signin/SecurityInfoChangeActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lcom/osp/app/signin/SecurityInfoChangeActivity;->d:Landroid/widget/Button;

    iget-object v0, p0, Lcom/osp/app/signin/SecurityInfoChangeActivity;->c:Landroid/widget/Button;

    const v1, 0x7f090052

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setText(I)V

    iget-object v0, p0, Lcom/osp/app/signin/SecurityInfoChangeActivity;->d:Landroid/widget/Button;

    const v1, 0x7f090019

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setText(I)V

    goto/16 :goto_2

    :cond_10
    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    goto :goto_3
.end method

.method public onCreateDialog(I)Landroid/app/Dialog;
    .locals 2

    .prologue
    .line 390
    const/4 v0, 0x0

    .line 391
    packed-switch p1, :pswitch_data_0

    .line 398
    :goto_0
    return-object v0

    .line 394
    :pswitch_0
    invoke-static {}, Lcom/osp/app/util/y;->a()Lcom/osp/app/util/y;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/osp/app/util/y;->a(Landroid/content/Context;)Landroid/app/AlertDialog;

    move-result-object v0

    goto :goto_0

    .line 397
    :pswitch_1
    invoke-static {}, Lcom/osp/app/util/y;->a()Lcom/osp/app/util/y;

    move-result-object v0

    iget-object v1, p0, Lcom/osp/app/signin/SecurityInfoChangeActivity;->a:Landroid/widget/Button;

    invoke-virtual {v0, p0, v1}, Lcom/osp/app/util/y;->a(Landroid/content/Context;Landroid/widget/Button;)Landroid/app/AlertDialog;

    move-result-object v0

    goto :goto_0

    .line 391
    nop

    :pswitch_data_0
    .packed-switch 0x12c
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method protected onDestroy()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 407
    invoke-super {p0}, Lcom/osp/app/util/BaseActivity;->onDestroy()V

    .line 409
    iget-object v0, p0, Lcom/osp/app/signin/SecurityInfoChangeActivity;->e:Landroid/app/AlertDialog;

    if-eqz v0, :cond_1

    .line 413
    :try_start_0
    invoke-virtual {p0}, Lcom/osp/app/signin/SecurityInfoChangeActivity;->isFinishing()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/osp/app/signin/SecurityInfoChangeActivity;->e:Landroid/app/AlertDialog;

    invoke-virtual {v0}, Landroid/app/AlertDialog;->isShowing()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 415
    iget-object v0, p0, Lcom/osp/app/signin/SecurityInfoChangeActivity;->e:Landroid/app/AlertDialog;

    invoke-virtual {v0}, Landroid/app/AlertDialog;->dismiss()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 422
    :cond_0
    :goto_0
    iput-object v2, p0, Lcom/osp/app/signin/SecurityInfoChangeActivity;->e:Landroid/app/AlertDialog;

    .line 426
    :cond_1
    iget-object v0, p0, Lcom/osp/app/signin/SecurityInfoChangeActivity;->y:Lcom/osp/app/signin/hh;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/osp/app/signin/SecurityInfoChangeActivity;->y:Lcom/osp/app/signin/hh;

    invoke-virtual {v0}, Lcom/osp/app/signin/hh;->getStatus()Landroid/os/AsyncTask$Status;

    move-result-object v0

    sget-object v1, Landroid/os/AsyncTask$Status;->RUNNING:Landroid/os/AsyncTask$Status;

    if-ne v0, v1, :cond_2

    .line 428
    iget-object v0, p0, Lcom/osp/app/signin/SecurityInfoChangeActivity;->y:Lcom/osp/app/signin/hh;

    invoke-virtual {v0}, Lcom/osp/app/signin/hh;->d()V

    .line 429
    iput-object v2, p0, Lcom/osp/app/signin/SecurityInfoChangeActivity;->y:Lcom/osp/app/signin/hh;

    .line 432
    :cond_2
    iget-object v0, p0, Lcom/osp/app/signin/SecurityInfoChangeActivity;->z:Lcom/osp/app/signin/hg;

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/osp/app/signin/SecurityInfoChangeActivity;->z:Lcom/osp/app/signin/hg;

    invoke-virtual {v0}, Lcom/osp/app/signin/hg;->getStatus()Landroid/os/AsyncTask$Status;

    move-result-object v0

    sget-object v1, Landroid/os/AsyncTask$Status;->RUNNING:Landroid/os/AsyncTask$Status;

    if-ne v0, v1, :cond_3

    .line 434
    iget-object v0, p0, Lcom/osp/app/signin/SecurityInfoChangeActivity;->z:Lcom/osp/app/signin/hg;

    invoke-virtual {v0}, Lcom/osp/app/signin/hg;->d()V

    .line 435
    iput-object v2, p0, Lcom/osp/app/signin/SecurityInfoChangeActivity;->z:Lcom/osp/app/signin/hg;

    .line 437
    :cond_3
    return-void

    .line 417
    :catch_0
    move-exception v0

    :try_start_1
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 422
    :catchall_0
    move-exception v0

    iput-object v2, p0, Lcom/osp/app/signin/SecurityInfoChangeActivity;->e:Landroid/app/AlertDialog;

    throw v0
.end method

.method public onOptionsItemSelected(Landroid/view/MenuItem;)Z
    .locals 2

    .prologue
    .line 512
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0xb

    if-lt v0, v1, :cond_0

    .line 514
    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    .line 529
    :cond_0
    :goto_0
    invoke-super {p0, p1}, Lcom/osp/app/util/BaseActivity;->onOptionsItemSelected(Landroid/view/MenuItem;)Z

    move-result v0

    return v0

    .line 518
    :sswitch_0
    invoke-virtual {p0}, Lcom/osp/app/signin/SecurityInfoChangeActivity;->onBackPressed()V

    goto :goto_0

    .line 522
    :sswitch_1
    invoke-direct {p0}, Lcom/osp/app/signin/SecurityInfoChangeActivity;->c()V

    .line 524
    new-instance v0, Lcom/osp/app/signin/hg;

    invoke-direct {v0, p0}, Lcom/osp/app/signin/hg;-><init>(Lcom/osp/app/signin/SecurityInfoChangeActivity;)V

    iput-object v0, p0, Lcom/osp/app/signin/SecurityInfoChangeActivity;->z:Lcom/osp/app/signin/hg;

    .line 525
    iget-object v0, p0, Lcom/osp/app/signin/SecurityInfoChangeActivity;->z:Lcom/osp/app/signin/hg;

    invoke-virtual {v0}, Lcom/osp/app/signin/hg;->b()V

    goto :goto_0

    .line 528
    :sswitch_2
    invoke-virtual {p0}, Lcom/osp/app/signin/SecurityInfoChangeActivity;->onBackPressed()V

    goto :goto_0

    .line 514
    :sswitch_data_0
    .sparse-switch
        0x102002c -> :sswitch_0
        0x7f0c018e -> :sswitch_2
        0x7f0c018f -> :sswitch_1
    .end sparse-switch
.end method

.method protected onRestoreInstanceState(Landroid/os/Bundle;)V
    .locals 0

    .prologue
    .line 456
    invoke-super {p0, p1}, Lcom/osp/app/util/BaseActivity;->onRestoreInstanceState(Landroid/os/Bundle;)V

    .line 457
    return-void
.end method

.method protected onResume()V
    .locals 1

    .prologue
    .line 441
    invoke-super {p0}, Lcom/osp/app/util/BaseActivity;->onResume()V

    .line 446
    invoke-static {p0}, Lcom/osp/app/util/ad;->d(Landroid/content/Context;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 448
    const/16 v0, 0x10

    invoke-virtual {p0, v0}, Lcom/osp/app/signin/SecurityInfoChangeActivity;->b(I)V

    .line 449
    invoke-virtual {p0}, Lcom/osp/app/signin/SecurityInfoChangeActivity;->finish()V

    .line 452
    :cond_0
    return-void
.end method
