.class final Lcom/osp/app/signin/nm;
.super Lcom/msc/c/b;
.source "SmsVerificationActivity.java"


# instance fields
.field final synthetic c:Lcom/osp/app/signin/SmsVerificationActivity;

.field private d:I

.field private e:J

.field private f:J

.field private g:J

.field private h:J

.field private i:J

.field private j:J

.field private k:J

.field private l:Z

.field private m:Lcom/msc/sa/d/f;


# direct methods
.method public constructor <init>(Lcom/osp/app/signin/SmsVerificationActivity;)V
    .locals 1

    .prologue
    .line 2318
    iput-object p1, p0, Lcom/osp/app/signin/nm;->c:Lcom/osp/app/signin/SmsVerificationActivity;

    .line 2319
    invoke-direct {p0, p1}, Lcom/msc/c/b;-><init>(Landroid/content/Context;)V

    .line 2269
    const/4 v0, -0x1

    iput v0, p0, Lcom/osp/app/signin/nm;->d:I

    .line 2308
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/osp/app/signin/nm;->l:Z

    .line 2313
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/osp/app/signin/nm;->m:Lcom/msc/sa/d/f;

    .line 2320
    return-void
.end method

.method private h()V
    .locals 4

    .prologue
    .line 2891
    iget-object v0, p0, Lcom/osp/app/signin/nm;->c:Lcom/osp/app/signin/SmsVerificationActivity;

    invoke-static {v0}, Lcom/osp/app/signin/SmsVerificationActivity;->L(Lcom/osp/app/signin/SmsVerificationActivity;)Z

    move-result v0

    const/4 v1, 0x1

    if-ne v0, v1, :cond_0

    .line 2893
    iget-object v0, p0, Lcom/osp/app/signin/nm;->c:Lcom/osp/app/signin/SmsVerificationActivity;

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lcom/osp/app/signin/SmsVerificationActivity;->b(Lcom/osp/app/signin/SmsVerificationActivity;Z)Z

    .line 2895
    :cond_0
    invoke-static {}, Lcom/msc/c/g;->a()Lcom/msc/c/g;

    .line 2896
    iget-object v0, p0, Lcom/osp/app/signin/nm;->c:Lcom/osp/app/signin/SmsVerificationActivity;

    iget-object v1, p0, Lcom/osp/app/signin/nm;->c:Lcom/osp/app/signin/SmsVerificationActivity;

    invoke-static {v1}, Lcom/osp/app/signin/SmsVerificationActivity;->j(Lcom/osp/app/signin/SmsVerificationActivity;)Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/osp/app/signin/nm;->c:Lcom/osp/app/signin/SmsVerificationActivity;

    invoke-static {v2}, Lcom/osp/app/signin/SmsVerificationActivity;->D(Lcom/osp/app/signin/SmsVerificationActivity;)Ljava/lang/String;

    move-result-object v2

    iget-object v3, p0, Lcom/osp/app/signin/nm;->c:Lcom/osp/app/signin/SmsVerificationActivity;

    invoke-virtual {v3}, Lcom/osp/app/signin/SmsVerificationActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    invoke-virtual {v3}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v3

    iget-object v3, v3, Landroid/content/res/Configuration;->locale:Ljava/util/Locale;

    invoke-virtual {v3}, Ljava/util/Locale;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v0, v1, v2, v3, p0}, Lcom/msc/c/g;->g(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/msc/b/h;)J

    move-result-wide v0

    iput-wide v0, p0, Lcom/osp/app/signin/nm;->e:J

    .line 2898
    iget-wide v0, p0, Lcom/osp/app/signin/nm;->e:J

    const-string v2, "from_xml"

    invoke-virtual {p0, v0, v1, v2}, Lcom/osp/app/signin/nm;->a(JLjava/lang/String;)V

    .line 2899
    iget-wide v0, p0, Lcom/osp/app/signin/nm;->e:J

    sget-object v2, Lcom/msc/b/g;->c:Lcom/msc/b/g;

    invoke-static {v0, v1, v2}, Lcom/msc/c/g;->a(JLcom/msc/b/g;)V

    .line 2900
    return-void
.end method


# virtual methods
.method protected final varargs a()Ljava/lang/Boolean;
    .locals 7

    .prologue
    const/4 v0, 0x1

    const/4 v6, 0x0

    .line 2495
    iget v1, p0, Lcom/osp/app/signin/nm;->d:I

    packed-switch v1, :pswitch_data_0

    .line 2555
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v0, "SMSV"

    const-string v1, "doInBackground : undefined SMSAuthenticateTask Mode!!!"

    invoke-static {v0, v1}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 2559
    :goto_0
    :pswitch_0
    invoke-static {v6}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    return-object v0

    .line 2498
    :pswitch_1
    invoke-static {}, Lcom/osp/device/b;->b()Lcom/osp/device/b;

    move-result-object v1

    invoke-virtual {v1}, Lcom/osp/device/b;->a()Z

    move-result v1

    if-eqz v1, :cond_3

    .line 2500
    iget-object v1, p0, Lcom/osp/app/signin/nm;->c:Lcom/osp/app/signin/SmsVerificationActivity;

    invoke-static {v1}, Lcom/osp/app/signin/SmsVerificationActivity;->L(Lcom/osp/app/signin/SmsVerificationActivity;)Z

    move-result v1

    if-eq v1, v0, :cond_0

    const-string v1, "FROM_SIGN_IN_FLOW"

    iget-object v2, p0, Lcom/osp/app/signin/nm;->c:Lcom/osp/app/signin/SmsVerificationActivity;

    invoke-static {v2}, Lcom/osp/app/signin/SmsVerificationActivity;->M(Lcom/osp/app/signin/SmsVerificationActivity;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/osp/app/signin/nm;->c:Lcom/osp/app/signin/SmsVerificationActivity;

    invoke-static {v1}, Lcom/osp/app/signin/SmsVerificationActivity;->j(Lcom/osp/app/signin/SmsVerificationActivity;)Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/osp/app/signin/nm;->c:Lcom/osp/app/signin/SmsVerificationActivity;

    invoke-static {v2}, Lcom/osp/app/signin/SmsVerificationActivity;->D(Lcom/osp/app/signin/SmsVerificationActivity;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_1

    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_1

    iget-object v3, p0, Lcom/osp/app/signin/nm;->c:Lcom/osp/app/signin/SmsVerificationActivity;

    invoke-static {v3}, Lcom/osp/app/signin/SmsVerificationActivity;->J(Lcom/osp/app/signin/SmsVerificationActivity;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    iget-object v1, p0, Lcom/osp/app/signin/nm;->c:Lcom/osp/app/signin/SmsVerificationActivity;

    invoke-static {v1}, Lcom/osp/app/signin/SmsVerificationActivity;->K(Lcom/osp/app/signin/SmsVerificationActivity;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v1, "SMSV"

    const-string v2, "Identical number"

    invoke-static {v1, v2}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;)V

    :goto_1
    if-eqz v0, :cond_2

    .line 2508
    :cond_0
    invoke-direct {p0}, Lcom/osp/app/signin/nm;->h()V

    goto :goto_0

    :cond_1
    move v0, v6

    .line 2500
    goto :goto_1

    .line 2513
    :cond_2
    invoke-static {}, Lcom/msc/c/g;->a()Lcom/msc/c/g;

    iget-object v0, p0, Lcom/osp/app/signin/nm;->c:Lcom/osp/app/signin/SmsVerificationActivity;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v2, p0, Lcom/osp/app/signin/nm;->c:Lcom/osp/app/signin/SmsVerificationActivity;

    invoke-static {v2}, Lcom/osp/app/signin/SmsVerificationActivity;->j(Lcom/osp/app/signin/SmsVerificationActivity;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/osp/app/signin/nm;->c:Lcom/osp/app/signin/SmsVerificationActivity;

    invoke-static {v2}, Lcom/osp/app/signin/SmsVerificationActivity;->D(Lcom/osp/app/signin/SmsVerificationActivity;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/osp/app/signin/nm;->c:Lcom/osp/app/signin/SmsVerificationActivity;

    invoke-static {v2}, Lcom/osp/app/signin/SmsVerificationActivity;->j(Lcom/osp/app/signin/SmsVerificationActivity;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v1, v2, p0}, Lcom/msc/c/g;->i(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Lcom/msc/b/h;)J

    move-result-wide v0

    iput-wide v0, p0, Lcom/osp/app/signin/nm;->k:J

    iget-wide v0, p0, Lcom/osp/app/signin/nm;->k:J

    invoke-virtual {p0, v0, v1}, Lcom/osp/app/signin/nm;->a(J)V

    iget-wide v0, p0, Lcom/osp/app/signin/nm;->k:J

    sget-object v2, Lcom/msc/b/g;->c:Lcom/msc/b/g;

    invoke-static {v0, v1, v2}, Lcom/msc/c/g;->a(JLcom/msc/b/g;)V

    goto/16 :goto_0

    .line 2518
    :cond_3
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v1, p0, Lcom/osp/app/signin/nm;->c:Lcom/osp/app/signin/SmsVerificationActivity;

    invoke-static {v1}, Lcom/osp/app/signin/SmsVerificationActivity;->j(Lcom/osp/app/signin/SmsVerificationActivity;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/osp/app/signin/nm;->c:Lcom/osp/app/signin/SmsVerificationActivity;

    invoke-static {v1}, Lcom/osp/app/signin/SmsVerificationActivity;->D(Lcom/osp/app/signin/SmsVerificationActivity;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {}, Lcom/msc/c/g;->a()Lcom/msc/c/g;

    iget-object v1, p0, Lcom/osp/app/signin/nm;->c:Lcom/osp/app/signin/SmsVerificationActivity;

    invoke-static {v1, v0, p0}, Lcom/msc/c/g;->i(Landroid/content/Context;Ljava/lang/String;Lcom/msc/b/h;)J

    move-result-wide v0

    iput-wide v0, p0, Lcom/osp/app/signin/nm;->j:J

    iget-wide v0, p0, Lcom/osp/app/signin/nm;->j:J

    invoke-virtual {p0, v0, v1}, Lcom/osp/app/signin/nm;->a(J)V

    iget-wide v0, p0, Lcom/osp/app/signin/nm;->j:J

    sget-object v2, Lcom/msc/b/g;->c:Lcom/msc/b/g;

    invoke-static {v0, v1, v2}, Lcom/msc/c/g;->a(JLcom/msc/b/g;)V

    goto/16 :goto_0

    .line 2524
    :pswitch_2
    iget-object v0, p0, Lcom/osp/app/signin/nm;->c:Lcom/osp/app/signin/SmsVerificationActivity;

    invoke-static {v0}, Lcom/osp/app/signin/SmsVerificationActivity;->N(Lcom/osp/app/signin/SmsVerificationActivity;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 2526
    new-instance v0, Lcom/msc/c/f;

    invoke-direct {v0}, Lcom/msc/c/f;-><init>()V

    iput-object v0, p0, Lcom/osp/app/signin/nm;->b:Lcom/msc/c/f;

    goto/16 :goto_0

    .line 2534
    :cond_4
    invoke-static {}, Lcom/msc/c/g;->a()Lcom/msc/c/g;

    iget-object v0, p0, Lcom/osp/app/signin/nm;->c:Lcom/osp/app/signin/SmsVerificationActivity;

    iget-object v1, p0, Lcom/osp/app/signin/nm;->c:Lcom/osp/app/signin/SmsVerificationActivity;

    invoke-static {v1}, Lcom/osp/app/signin/SmsVerificationActivity;->j(Lcom/osp/app/signin/SmsVerificationActivity;)Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/osp/app/signin/nm;->c:Lcom/osp/app/signin/SmsVerificationActivity;

    invoke-static {v2}, Lcom/osp/app/signin/SmsVerificationActivity;->D(Lcom/osp/app/signin/SmsVerificationActivity;)Ljava/lang/String;

    move-result-object v2

    iget-object v3, p0, Lcom/osp/app/signin/nm;->c:Lcom/osp/app/signin/SmsVerificationActivity;

    invoke-static {v3}, Lcom/osp/app/signin/SmsVerificationActivity;->N(Lcom/osp/app/signin/SmsVerificationActivity;)Ljava/lang/String;

    move-result-object v3

    iget-object v4, p0, Lcom/osp/app/signin/nm;->c:Lcom/osp/app/signin/SmsVerificationActivity;

    invoke-static {v4}, Lcom/osp/app/signin/SmsVerificationActivity;->R(Lcom/osp/app/signin/SmsVerificationActivity;)Ljava/lang/String;

    move-result-object v4

    move-object v5, p0

    invoke-static/range {v0 .. v5}, Lcom/msc/c/g;->c(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/msc/b/h;)J

    move-result-wide v0

    iput-wide v0, p0, Lcom/osp/app/signin/nm;->f:J

    iget-wide v0, p0, Lcom/osp/app/signin/nm;->f:J

    const-string v2, "from_xml"

    invoke-virtual {p0, v0, v1, v2}, Lcom/osp/app/signin/nm;->a(JLjava/lang/String;)V

    iget-wide v0, p0, Lcom/osp/app/signin/nm;->f:J

    sget-object v2, Lcom/msc/b/g;->c:Lcom/msc/b/g;

    invoke-static {v0, v1, v2}, Lcom/msc/c/g;->a(JLcom/msc/b/g;)V

    goto/16 :goto_0

    .line 2539
    :pswitch_3
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v0, "SMSV"

    const-string v1, "doInBackground : SMS_PUBLIC_VERIFY_REQUEST_MODE"

    invoke-static {v0, v1}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 2495
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_2
        :pswitch_2
        :pswitch_0
        :pswitch_3
    .end packed-switch
.end method

.method protected final a(I)V
    .locals 0

    .prologue
    .line 2328
    iput p1, p0, Lcom/osp/app/signin/nm;->d:I

    .line 2329
    return-void
.end method

.method public final declared-synchronized a(Lcom/msc/b/c;)V
    .locals 13

    .prologue
    const/4 v2, 0x0

    const/4 v8, 0x1

    const/4 v1, 0x0

    .line 2564
    monitor-enter p0

    :try_start_0
    invoke-super {p0, p1}, Lcom/msc/c/b;->a(Lcom/msc/b/c;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 2566
    if-nez p1, :cond_1

    .line 2769
    :cond_0
    :goto_0
    monitor-exit p0

    return-void

    .line 2571
    :cond_1
    :try_start_1
    invoke-virtual {p1}, Lcom/msc/b/c;->a()J

    move-result-wide v4

    .line 2572
    invoke-virtual {p1}, Lcom/msc/b/c;->e()Ljava/lang/String;

    move-result-object v3

    .line 2574
    iget-wide v6, p0, Lcom/osp/app/signin/nm;->j:J
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    cmp-long v0, v4, v6

    if-nez v0, :cond_3

    .line 2578
    :try_start_2
    invoke-static {}, Lcom/msc/c/h;->a()Lcom/msc/c/h;

    invoke-static {v3}, Lcom/msc/c/h;->h(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 2579
    if-eqz v0, :cond_2

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v0

    if-nez v0, :cond_2

    .line 2586
    invoke-direct {p0}, Lcom/osp/app/signin/nm;->h()V
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_0
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0

    .line 2593
    :catch_0
    move-exception v0

    .line 2595
    :try_start_3
    new-instance v1, Lcom/msc/c/f;

    invoke-direct {v1, v0}, Lcom/msc/c/f;-><init>(Ljava/lang/Exception;)V

    iput-object v1, p0, Lcom/osp/app/signin/nm;->b:Lcom/msc/c/f;
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    goto :goto_0

    .line 2564
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 2590
    :cond_2
    :try_start_4
    new-instance v0, Lcom/msc/c/f;

    invoke-direct {v0}, Lcom/msc/c/f;-><init>()V

    iput-object v0, p0, Lcom/osp/app/signin/nm;->b:Lcom/msc/c/f;

    .line 2591
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/osp/app/signin/nm;->l:Z
    :try_end_4
    .catch Ljava/lang/Exception; {:try_start_4 .. :try_end_4} :catch_0
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    goto :goto_0

    .line 2597
    :cond_3
    :try_start_5
    iget-wide v6, p0, Lcom/osp/app/signin/nm;->e:J

    cmp-long v0, v4, v6

    if-nez v0, :cond_6

    .line 2599
    iget-object v0, p0, Lcom/osp/app/signin/nm;->c:Lcom/osp/app/signin/SmsVerificationActivity;

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lcom/osp/app/signin/SmsVerificationActivity;->h(Lcom/osp/app/signin/SmsVerificationActivity;Ljava/lang/String;)Ljava/lang/String;
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    .line 2602
    :try_start_6
    invoke-static {}, Lcom/msc/c/h;->a()Lcom/msc/c/h;

    invoke-static {v3}, Lcom/msc/c/h;->E(Ljava/lang/String;)Ljava/util/HashMap;

    move-result-object v1

    .line 2603
    const-string v0, "authenticateToken"

    invoke-virtual {v1, v0}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 2605
    iget-object v2, p0, Lcom/osp/app/signin/nm;->c:Lcom/osp/app/signin/SmsVerificationActivity;

    const-string v0, "authenticateToken"

    invoke-virtual {v1, v0}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-static {v2, v0}, Lcom/osp/app/signin/SmsVerificationActivity;->h(Lcom/osp/app/signin/SmsVerificationActivity;Ljava/lang/String;)Ljava/lang/String;

    .line 2607
    :cond_4
    const-string v0, "prefix"

    invoke-virtual {v1, v0}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_5

    .line 2609
    iget-object v2, p0, Lcom/osp/app/signin/nm;->c:Lcom/osp/app/signin/SmsVerificationActivity;

    const-string v0, "prefix"

    invoke-virtual {v1, v0}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-static {v2, v0}, Lcom/osp/app/signin/SmsVerificationActivity;->i(Lcom/osp/app/signin/SmsVerificationActivity;Ljava/lang/String;)Ljava/lang/String;

    .line 2611
    :cond_5
    const-string v0, "limitExpireTime"

    invoke-virtual {v1, v0}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    .line 2616
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "SMSV authenticateToken : "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v1, p0, Lcom/osp/app/signin/nm;->c:Lcom/osp/app/signin/SmsVerificationActivity;

    invoke-static {v1}, Lcom/osp/app/signin/SmsVerificationActivity;->N(Lcom/osp/app/signin/SmsVerificationActivity;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;)V
    :try_end_6
    .catch Ljava/lang/Exception; {:try_start_6 .. :try_end_6} :catch_1
    .catchall {:try_start_6 .. :try_end_6} :catchall_0

    .line 2623
    :goto_1
    :try_start_7
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "SMSV onSuccess RequestSMSAuthenticate : "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v1, p0, Lcom/osp/app/signin/nm;->c:Lcom/osp/app/signin/SmsVerificationActivity;

    invoke-static {v1}, Lcom/osp/app/signin/SmsVerificationActivity;->N(Lcom/osp/app/signin/SmsVerificationActivity;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;)V

    .line 2624
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v0, "SMSV"

    const-string v1, "RequestSMSAuthenticate onRequestSuccess"

    invoke-static {v0, v1}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 2626
    iget-object v0, p0, Lcom/osp/app/signin/nm;->c:Lcom/osp/app/signin/SmsVerificationActivity;

    invoke-static {v0}, Lcom/osp/app/signin/SmsVerificationActivity;->N(Lcom/osp/app/signin/SmsVerificationActivity;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2628
    new-instance v0, Lcom/msc/c/f;

    invoke-direct {v0}, Lcom/msc/c/f;-><init>()V

    iput-object v0, p0, Lcom/osp/app/signin/nm;->b:Lcom/msc/c/f;

    goto/16 :goto_0

    .line 2618
    :catch_1
    move-exception v0

    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_1

    .line 2631
    :cond_6
    iget-wide v6, p0, Lcom/osp/app/signin/nm;->f:J
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_0

    cmp-long v0, v4, v6

    if-nez v0, :cond_9

    .line 2636
    :try_start_8
    invoke-static {}, Lcom/msc/c/h;->a()Lcom/msc/c/h;

    invoke-static {v3}, Lcom/msc/c/h;->G(Ljava/lang/String;)Ljava/util/HashMap;

    move-result-object v2

    .line 2637
    const-string v0, "validationTime"

    invoke-virtual {v2, v0}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_7

    .line 2639
    iget-object v3, p0, Lcom/osp/app/signin/nm;->c:Lcom/osp/app/signin/SmsVerificationActivity;

    const-string v0, "validationTime"

    invoke-virtual {v2, v0}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-static {v3, v0}, Lcom/osp/app/signin/SmsVerificationActivity;->j(Lcom/osp/app/signin/SmsVerificationActivity;Ljava/lang/String;)Ljava/lang/String;

    .line 2641
    :cond_7
    const-string v0, "boolean"

    invoke-virtual {v2, v0}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_8

    .line 2643
    const-string v0, "boolean"

    invoke-virtual {v2, v0}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Ljava/lang/Boolean;->parseBoolean(Ljava/lang/String;)Z
    :try_end_8
    .catch Ljava/lang/Exception; {:try_start_8 .. :try_end_8} :catch_2
    .catchall {:try_start_8 .. :try_end_8} :catchall_0

    move-result v0

    .line 2644
    :try_start_9
    iget-object v1, p0, Lcom/osp/app/signin/nm;->c:Lcom/osp/app/signin/SmsVerificationActivity;

    invoke-static {v1, v0}, Lcom/osp/app/signin/SmsVerificationActivity;->d(Lcom/osp/app/signin/SmsVerificationActivity;Z)Z
    :try_end_9
    .catch Ljava/lang/Exception; {:try_start_9 .. :try_end_9} :catch_5
    .catchall {:try_start_9 .. :try_end_9} :catchall_0

    .line 2652
    :goto_2
    :try_start_a
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "SMSV onSuccess ValidateSMSAuthenticate : "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, Lcom/osp/app/signin/nm;->c:Lcom/osp/app/signin/SmsVerificationActivity;

    invoke-static {v2}, Lcom/osp/app/signin/SmsVerificationActivity;->O(Lcom/osp/app/signin/SmsVerificationActivity;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;)V

    .line 2653
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v1, "SMSV"

    const-string v2, "ValidateSMSAuthenticate onRequestSuccess"

    invoke-static {v1, v2}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 2655
    if-nez v0, :cond_0

    .line 2657
    new-instance v0, Lcom/msc/c/f;

    invoke-direct {v0}, Lcom/msc/c/f;-><init>()V

    iput-object v0, p0, Lcom/osp/app/signin/nm;->b:Lcom/msc/c/f;

    goto/16 :goto_0

    .line 2647
    :catch_2
    move-exception v0

    :goto_3
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    :cond_8
    move v0, v1

    goto :goto_2

    .line 2659
    :cond_9
    iget-wide v0, p0, Lcom/osp/app/signin/nm;->k:J
    :try_end_a
    .catchall {:try_start_a .. :try_end_a} :catchall_0

    cmp-long v0, v4, v0

    if-nez v0, :cond_d

    .line 2665
    :try_start_b
    invoke-static {}, Lcom/msc/c/h;->a()Lcom/msc/c/h;

    invoke-static {v3}, Lcom/msc/c/h;->f(Ljava/lang/String;)Lcom/msc/sa/d/f;
    :try_end_b
    .catch Ljava/lang/Exception; {:try_start_b .. :try_end_b} :catch_3
    .catchall {:try_start_b .. :try_end_b} :catchall_0

    move-result-object v0

    .line 2673
    :goto_4
    if-eqz v0, :cond_0

    .line 2675
    :try_start_c
    invoke-virtual {v0}, Lcom/msc/sa/d/f;->a()Z

    move-result v1

    if-nez v1, :cond_a

    invoke-virtual {v0}, Lcom/msc/sa/d/f;->b()Z

    move-result v1

    if-eqz v1, :cond_a

    .line 2677
    iput-object v0, p0, Lcom/osp/app/signin/nm;->m:Lcom/msc/sa/d/f;

    .line 2678
    new-instance v0, Lcom/msc/c/f;

    invoke-direct {v0}, Lcom/msc/c/f;-><init>()V

    iput-object v0, p0, Lcom/osp/app/signin/nm;->b:Lcom/msc/c/f;

    goto/16 :goto_0

    .line 2667
    :catch_3
    move-exception v0

    .line 2669
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    .line 2670
    new-instance v1, Lcom/msc/c/f;

    invoke-direct {v1, v0}, Lcom/msc/c/f;-><init>(Ljava/lang/Exception;)V

    iput-object v1, p0, Lcom/osp/app/signin/nm;->b:Lcom/msc/c/f;

    move-object v0, v2

    goto :goto_4

    .line 2681
    :cond_a
    const-string v1, "FROM_EDIT_PROFILE"

    iget-object v2, p0, Lcom/osp/app/signin/nm;->c:Lcom/osp/app/signin/SmsVerificationActivity;

    invoke-static {v2}, Lcom/osp/app/signin/SmsVerificationActivity;->M(Lcom/osp/app/signin/SmsVerificationActivity;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_c

    const-string v1, "FROM_SIGN_IN_FLOW"

    iget-object v2, p0, Lcom/osp/app/signin/nm;->c:Lcom/osp/app/signin/SmsVerificationActivity;

    invoke-static {v2}, Lcom/osp/app/signin/SmsVerificationActivity;->M(Lcom/osp/app/signin/SmsVerificationActivity;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_c

    .line 2683
    invoke-virtual {v0}, Lcom/msc/sa/d/f;->a()Z

    move-result v1

    if-nez v1, :cond_b

    invoke-virtual {v0}, Lcom/msc/sa/d/f;->b()Z

    move-result v1

    if-nez v1, :cond_b

    .line 2685
    iget-object v1, p0, Lcom/osp/app/signin/nm;->c:Lcom/osp/app/signin/SmsVerificationActivity;

    invoke-static {v1}, Lcom/osp/app/signin/SmsVerificationActivity;->P(Lcom/osp/app/signin/SmsVerificationActivity;)Z

    .line 2686
    iput-object v0, p0, Lcom/osp/app/signin/nm;->m:Lcom/msc/sa/d/f;

    .line 2687
    new-instance v0, Lcom/msc/c/f;

    invoke-direct {v0}, Lcom/msc/c/f;-><init>()V

    iput-object v0, p0, Lcom/osp/app/signin/nm;->b:Lcom/msc/c/f;

    goto/16 :goto_0

    .line 2696
    :cond_b
    invoke-direct {p0}, Lcom/osp/app/signin/nm;->h()V

    goto/16 :goto_0

    .line 2706
    :cond_c
    invoke-direct {p0}, Lcom/osp/app/signin/nm;->h()V

    goto/16 :goto_0

    .line 2711
    :cond_d
    iget-wide v0, p0, Lcom/osp/app/signin/nm;->g:J

    cmp-long v0, v4, v0

    if-nez v0, :cond_e

    .line 2714
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v0, "SMSV"

    const-string v1, "RequestSMSVerifyCodeForDP onRequestSuccess"

    invoke-static {v0, v1}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 2716
    :cond_e
    iget-wide v0, p0, Lcom/osp/app/signin/nm;->h:J

    cmp-long v0, v4, v0

    if-nez v0, :cond_f

    .line 2733
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v0, "SMSV"

    const-string v1, "RequestSMSValidationForDP onRequestSuccess"

    invoke-static {v0, v1}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 2734
    :cond_f
    iget-wide v0, p0, Lcom/osp/app/signin/nm;->i:J

    cmp-long v0, v4, v0

    if-nez v0, :cond_0

    .line 2736
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v0, "SMSV"

    const-string v1, "RequestIsAuthForDPId onRequestSuccess"

    invoke-static {v0, v1}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_c
    .catchall {:try_start_c .. :try_end_c} :catchall_0

    .line 2739
    :try_start_d
    const-string v0, ""

    .line 2740
    const-string v1, ""

    .line 2741
    invoke-static {}, Lcom/msc/c/h;->a()Lcom/msc/c/h;

    invoke-static {v3}, Lcom/msc/c/h;->F(Ljava/lang/String;)Ljava/util/HashMap;

    move-result-object v2

    .line 2742
    const-string v3, "auth_status"

    invoke-virtual {v2, v3}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_10

    .line 2744
    const-string v0, "auth_status"

    invoke-virtual {v2, v0}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 2746
    :cond_10
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "mEasySignup Authentication : "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;)V

    .line 2748
    const-string v3, "1"

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_14

    .line 2750
    const-string v0, "msisdn"

    invoke-virtual {v2, v0}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 2751
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "mEasySignupPhoneNumber : "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;)V

    .line 2753
    :goto_5
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "mCountryCallingCode + mPhoneNumber : "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, Lcom/osp/app/signin/nm;->c:Lcom/osp/app/signin/SmsVerificationActivity;

    invoke-static {v2}, Lcom/osp/app/signin/SmsVerificationActivity;->j(Lcom/osp/app/signin/SmsVerificationActivity;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/osp/app/signin/nm;->c:Lcom/osp/app/signin/SmsVerificationActivity;

    invoke-static {v2}, Lcom/osp/app/signin/SmsVerificationActivity;->D(Lcom/osp/app/signin/SmsVerificationActivity;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;)V

    .line 2754
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v2, p0, Lcom/osp/app/signin/nm;->c:Lcom/osp/app/signin/SmsVerificationActivity;

    invoke-static {v2}, Lcom/osp/app/signin/SmsVerificationActivity;->j(Lcom/osp/app/signin/SmsVerificationActivity;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/osp/app/signin/nm;->c:Lcom/osp/app/signin/SmsVerificationActivity;

    invoke-static {v2}, Lcom/osp/app/signin/SmsVerificationActivity;->D(Lcom/osp/app/signin/SmsVerificationActivity;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_11

    .line 2756
    iget-object v0, p0, Lcom/osp/app/signin/nm;->c:Lcom/osp/app/signin/SmsVerificationActivity;

    const/4 v1, 0x1

    invoke-static {v0, v1}, Lcom/osp/app/signin/SmsVerificationActivity;->e(Lcom/osp/app/signin/SmsVerificationActivity;Z)Z
    :try_end_d
    .catch Ljava/lang/Exception; {:try_start_d .. :try_end_d} :catch_4
    .catchall {:try_start_d .. :try_end_d} :catchall_0

    goto/16 :goto_0

    .line 2762
    :catch_4
    move-exception v0

    :try_start_e
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V
    :try_end_e
    .catchall {:try_start_e .. :try_end_e} :catchall_0

    goto/16 :goto_0

    .line 2759
    :cond_11
    :try_start_f
    iget-object v0, p0, Lcom/osp/app/signin/nm;->c:Lcom/osp/app/signin/SmsVerificationActivity;

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lcom/osp/app/signin/SmsVerificationActivity;->e(Lcom/osp/app/signin/SmsVerificationActivity;Z)Z

    .line 2760
    iget-object v0, p0, Lcom/osp/app/signin/nm;->c:Lcom/osp/app/signin/SmsVerificationActivity;

    invoke-static {v0}, Lcom/osp/app/signin/SmsVerificationActivity;->L(Lcom/osp/app/signin/SmsVerificationActivity;)Z

    move-result v0

    if-ne v0, v8, :cond_12

    iget-object v0, p0, Lcom/osp/app/signin/nm;->c:Lcom/osp/app/signin/SmsVerificationActivity;

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lcom/osp/app/signin/SmsVerificationActivity;->b(Lcom/osp/app/signin/SmsVerificationActivity;Z)Z

    :cond_12
    invoke-static {}, Lcom/msc/c/g;->a()Lcom/msc/c/g;

    iget-object v6, p0, Lcom/osp/app/signin/nm;->c:Lcom/osp/app/signin/SmsVerificationActivity;

    iget-object v0, p0, Lcom/osp/app/signin/nm;->c:Lcom/osp/app/signin/SmsVerificationActivity;

    invoke-static {v0}, Lcom/osp/app/signin/SmsVerificationActivity;->E(Lcom/osp/app/signin/SmsVerificationActivity;)Ljava/lang/String;

    move-result-object v7

    iget-object v0, p0, Lcom/osp/app/signin/nm;->c:Lcom/osp/app/signin/SmsVerificationActivity;

    invoke-static {v0}, Lcom/osp/app/signin/SmsVerificationActivity;->F(Lcom/osp/app/signin/SmsVerificationActivity;)Ljava/lang/String;

    move-result-object v8

    iget-object v0, p0, Lcom/osp/app/signin/nm;->c:Lcom/osp/app/signin/SmsVerificationActivity;

    invoke-static {v0}, Lcom/osp/app/signin/SmsVerificationActivity;->j(Lcom/osp/app/signin/SmsVerificationActivity;)Ljava/lang/String;

    move-result-object v9

    iget-object v0, p0, Lcom/osp/app/signin/nm;->c:Lcom/osp/app/signin/SmsVerificationActivity;

    invoke-static {v0}, Lcom/osp/app/signin/SmsVerificationActivity;->D(Lcom/osp/app/signin/SmsVerificationActivity;)Ljava/lang/String;

    move-result-object v10

    iget-object v0, p0, Lcom/osp/app/signin/nm;->c:Lcom/osp/app/signin/SmsVerificationActivity;

    invoke-virtual {v0}, Lcom/osp/app/signin/SmsVerificationActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v0

    iget-object v0, v0, Landroid/content/res/Configuration;->locale:Ljava/util/Locale;

    invoke-virtual {v0}, Ljava/util/Locale;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v0, "SRS"

    const-string v1, "prepareRequestSMSVerifyCodeForDP()"

    invoke-static {v0, v1}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;)V

    invoke-static {v7}, Lcom/msc/c/k;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    invoke-static {v1}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;)V

    invoke-static {}, Lcom/osp/app/util/ah;->a()Lcom/osp/app/util/ah;

    move-result-object v0

    invoke-virtual {v0}, Lcom/osp/app/util/ah;->g()Z

    move-result v4

    invoke-static {}, Lcom/msc/b/i;->a()Lcom/msc/b/i;

    move-result-object v0

    invoke-static {}, Lcom/osp/app/signin/SamsungService;->j()Z

    move-result v2

    const/4 v3, 0x0

    move-object v5, p0

    invoke-virtual/range {v0 .. v5}, Lcom/msc/b/i;->a(Ljava/lang/String;ZZZLcom/msc/b/h;)Lcom/msc/b/d;

    move-result-object v0

    invoke-static {}, Lcom/osp/app/util/ah;->a()Lcom/osp/app/util/ah;

    move-result-object v1

    invoke-virtual {v1}, Lcom/osp/app/util/ah;->c()Z

    move-result v1

    if-eqz v1, :cond_13

    const-string v1, "SQELOG"

    invoke-static {}, Lcom/osp/app/util/ah;->a()Lcom/osp/app/util/ah;

    move-result-object v2

    invoke-virtual {v2}, Lcom/osp/app/util/ah;->b()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/msc/b/d;->a(Ljava/lang/String;Ljava/lang/String;)V

    :cond_13
    const-string v1, "j5p7ll8g33:5763D0052DC1462E13751F753384E9A9"

    invoke-virtual {v1}, Ljava/lang/String;->getBytes()[B

    move-result-object v1

    const/4 v2, 0x2

    invoke-static {v1, v2}, Landroid/util/Base64;->encodeToString([BI)Ljava/lang/String;

    move-result-object v1

    invoke-static {}, Lcom/osp/app/util/ac;->a()Lcom/osp/app/util/ac;

    invoke-static {v6}, Lcom/osp/app/util/ac;->b(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v2

    const-string v3, "User-Agent"

    invoke-virtual {v0, v3, v2}, Lcom/msc/b/d;->a(Ljava/lang/String;Ljava/lang/String;)V

    const-string v2, "Authorization"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "Basic "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v2, v1}, Lcom/msc/b/d;->a(Ljava/lang/String;Ljava/lang/String;)V

    const-string v1, "x-osp-appId"

    const-string v2, "j5p7ll8g33"

    invoke-virtual {v0, v1, v2}, Lcom/msc/b/d;->a(Ljava/lang/String;Ljava/lang/String;)V

    const-string v1, "Content-Type"

    const-string v2, "application/json"

    invoke-virtual {v0, v1, v2}, Lcom/msc/b/d;->a(Ljava/lang/String;Ljava/lang/String;)V

    invoke-static {v7, v9, v10, v8, v11}, Lcom/msc/c/g;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lorg/json/JSONObject;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/msc/b/d;->a(Lorg/json/JSONObject;)V

    invoke-virtual {v0}, Lcom/msc/b/d;->a()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/osp/app/signin/nm;->g:J

    iget-wide v0, p0, Lcom/osp/app/signin/nm;->g:J

    invoke-virtual {p0, v0, v1}, Lcom/osp/app/signin/nm;->a(J)V

    iget-wide v0, p0, Lcom/osp/app/signin/nm;->g:J

    const-string v2, "from_json"

    invoke-virtual {p0, v0, v1, v2}, Lcom/osp/app/signin/nm;->a(JLjava/lang/String;)V

    iget-wide v0, p0, Lcom/osp/app/signin/nm;->g:J

    sget-object v2, Lcom/msc/b/g;->c:Lcom/msc/b/g;

    invoke-static {v0, v1, v2}, Lcom/msc/c/g;->a(JLcom/msc/b/g;)V
    :try_end_f
    .catch Ljava/lang/Exception; {:try_start_f .. :try_end_f} :catch_4
    .catchall {:try_start_f .. :try_end_f} :catchall_0

    goto/16 :goto_0

    .line 2647
    :catch_5
    move-exception v1

    move-object v12, v1

    move v1, v0

    move-object v0, v12

    goto/16 :goto_3

    :cond_14
    move-object v0, v1

    goto/16 :goto_5
.end method

.method protected final a(Ljava/lang/Boolean;)V
    .locals 7

    .prologue
    const v2, 0x7f0900ec

    const/4 v3, 0x2

    const/4 v6, 0x4

    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 2333
    invoke-super {p0, p1}, Lcom/msc/c/b;->a(Ljava/lang/Boolean;)V

    .line 2335
    iget-object v0, p0, Lcom/osp/app/signin/nm;->b:Lcom/msc/c/f;

    if-eqz v0, :cond_9

    .line 2337
    iget v0, p0, Lcom/osp/app/signin/nm;->d:I

    if-nez v0, :cond_0

    .line 2339
    iget-object v0, p0, Lcom/osp/app/signin/nm;->c:Lcom/osp/app/signin/SmsVerificationActivity;

    const v1, 0x7f0c00c7

    invoke-virtual {v0, v1}, Lcom/osp/app/signin/SmsVerificationActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v5}, Landroid/view/View;->setEnabled(Z)V

    .line 2340
    iget-object v0, p0, Lcom/osp/app/signin/nm;->c:Lcom/osp/app/signin/SmsVerificationActivity;

    invoke-static {v0}, Lcom/osp/app/signin/SmsVerificationActivity;->r(Lcom/osp/app/signin/SmsVerificationActivity;)V

    .line 2343
    :cond_0
    iget-boolean v0, p0, Lcom/osp/app/signin/nm;->l:Z

    if-eqz v0, :cond_2

    .line 2345
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v0, "SMSV"

    const-string v1, "GetUserIDTask - Account already exists."

    invoke-static {v0, v1}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 2346
    invoke-static {}, Lcom/osp/app/util/ap;->a()Lcom/osp/app/util/ap;

    iget-object v0, p0, Lcom/osp/app/signin/nm;->c:Lcom/osp/app/signin/SmsVerificationActivity;

    iget-object v1, p0, Lcom/osp/app/signin/nm;->c:Lcom/osp/app/signin/SmsVerificationActivity;

    const v2, 0x7f090015

    invoke-virtual {v1, v2}, Lcom/osp/app/signin/SmsVerificationActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1, v5}, Lcom/osp/app/util/ap;->a(Landroid/content/Context;Ljava/lang/String;I)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    .line 2476
    :cond_1
    :goto_0
    return-void

    .line 2347
    :cond_2
    iget-object v0, p0, Lcom/osp/app/signin/nm;->b:Lcom/msc/c/f;

    invoke-virtual {v0}, Lcom/msc/c/f;->b()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_5

    .line 2349
    iget-object v0, p0, Lcom/osp/app/signin/nm;->m:Lcom/msc/sa/d/f;

    if-eqz v0, :cond_4

    .line 2351
    iget-object v0, p0, Lcom/osp/app/signin/nm;->m:Lcom/msc/sa/d/f;

    invoke-virtual {v0}, Lcom/msc/sa/d/f;->a()Z

    move-result v0

    if-nez v0, :cond_3

    iget-object v0, p0, Lcom/osp/app/signin/nm;->m:Lcom/msc/sa/d/f;

    invoke-virtual {v0}, Lcom/msc/sa/d/f;->b()Z

    move-result v0

    if-nez v0, :cond_3

    .line 2353
    iget-object v0, p0, Lcom/osp/app/signin/nm;->c:Lcom/osp/app/signin/SmsVerificationActivity;

    invoke-static {v0}, Lcom/osp/app/signin/SmsVerificationActivity;->s(Lcom/osp/app/signin/SmsVerificationActivity;)V

    .line 2354
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/osp/app/signin/nm;->m:Lcom/msc/sa/d/f;

    goto :goto_0

    .line 2357
    :cond_3
    iget-object v0, p0, Lcom/osp/app/signin/nm;->c:Lcom/osp/app/signin/SmsVerificationActivity;

    invoke-static {v0}, Lcom/osp/app/signin/SmsVerificationActivity;->t(Lcom/osp/app/signin/SmsVerificationActivity;)V

    .line 2358
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/osp/app/signin/nm;->m:Lcom/msc/sa/d/f;

    goto :goto_0

    .line 2363
    :cond_4
    invoke-virtual {p0, v4}, Lcom/osp/app/signin/nm;->a(Z)V

    goto :goto_0

    .line 2367
    :cond_5
    const-string v0, "USR_3166"

    iget-object v1, p0, Lcom/osp/app/signin/nm;->b:Lcom/msc/c/f;

    invoke-virtual {v1}, Lcom/msc/c/f;->a()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_6

    .line 2369
    invoke-static {}, Lcom/osp/app/util/ap;->a()Lcom/osp/app/util/ap;

    iget-object v0, p0, Lcom/osp/app/signin/nm;->c:Lcom/osp/app/signin/SmsVerificationActivity;

    iget-object v1, p0, Lcom/osp/app/signin/nm;->b:Lcom/msc/c/f;

    invoke-virtual {v1}, Lcom/msc/c/f;->b()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1, v5}, Lcom/osp/app/util/ap;->a(Landroid/content/Context;Ljava/lang/String;I)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    goto :goto_0

    .line 2370
    :cond_6
    const-string v0, "USR_3156"

    iget-object v1, p0, Lcom/osp/app/signin/nm;->b:Lcom/msc/c/f;

    invoke-virtual {v1}, Lcom/msc/c/f;->a()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_7

    const-string v0, "4000401123"

    iget-object v1, p0, Lcom/osp/app/signin/nm;->b:Lcom/msc/c/f;

    invoke-virtual {v1}, Lcom/msc/c/f;->a()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_8

    .line 2372
    :cond_7
    iget-object v0, p0, Lcom/osp/app/signin/nm;->c:Lcom/osp/app/signin/SmsVerificationActivity;

    invoke-virtual {v0, v2}, Lcom/osp/app/signin/SmsVerificationActivity;->getString(I)Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/osp/app/signin/nm;->b:Lcom/msc/c/f;

    invoke-virtual {v1}, Lcom/msc/c/f;->b()Ljava/lang/String;

    move-result-object v1

    new-instance v2, Lcom/osp/app/signin/nn;

    invoke-direct {v2, p0}, Lcom/osp/app/signin/nn;-><init>(Lcom/osp/app/signin/nm;)V

    invoke-virtual {p0, v0, v1, v2}, Lcom/osp/app/signin/nm;->a(Ljava/lang/String;Ljava/lang/String;Landroid/content/DialogInterface$OnClickListener;)V

    goto/16 :goto_0

    .line 2383
    :cond_8
    iget-object v0, p0, Lcom/osp/app/signin/nm;->c:Lcom/osp/app/signin/SmsVerificationActivity;

    invoke-virtual {v0, v2}, Lcom/osp/app/signin/SmsVerificationActivity;->getString(I)Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/osp/app/signin/nm;->b:Lcom/msc/c/f;

    invoke-virtual {v1}, Lcom/msc/c/f;->b()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lcom/osp/app/signin/nm;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 2388
    :cond_9
    iget-object v0, p0, Lcom/osp/app/signin/nm;->c:Lcom/osp/app/signin/SmsVerificationActivity;

    const v1, 0x7f0c00c6

    invoke-virtual {v0, v1}, Lcom/osp/app/signin/SmsVerificationActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 2389
    iget v1, p0, Lcom/osp/app/signin/nm;->d:I

    if-eq v1, v5, :cond_a

    iget v1, p0, Lcom/osp/app/signin/nm;->d:I

    if-eq v1, v3, :cond_a

    iget v1, p0, Lcom/osp/app/signin/nm;->d:I

    const/4 v2, 0x3

    if-ne v1, v2, :cond_b

    .line 2391
    :cond_a
    iget-object v1, p0, Lcom/osp/app/signin/nm;->c:Lcom/osp/app/signin/SmsVerificationActivity;

    const-string v2, "notification"

    invoke-virtual {v1, v2}, Lcom/osp/app/signin/SmsVerificationActivity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/app/NotificationManager;

    .line 2392
    const v2, 0x132df82

    invoke-virtual {v1, v2}, Landroid/app/NotificationManager;->cancel(I)V

    .line 2394
    :cond_b
    iget v1, p0, Lcom/osp/app/signin/nm;->d:I

    packed-switch v1, :pswitch_data_0

    .line 2453
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v1, "SMSV"

    const-string v2, "onPostExecute : undefined SMSAuthenticateTask Mode!!!"

    invoke-static {v1, v2}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 2457
    :goto_1
    iget-object v1, p0, Lcom/osp/app/signin/nm;->c:Lcom/osp/app/signin/SmsVerificationActivity;

    invoke-static {v1}, Lcom/osp/app/signin/SmsVerificationActivity;->G(Lcom/osp/app/signin/SmsVerificationActivity;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 2459
    iget-object v1, p0, Lcom/osp/app/signin/nm;->c:Lcom/osp/app/signin/SmsVerificationActivity;

    invoke-static {v1}, Lcom/osp/app/signin/SmsVerificationActivity;->z(Lcom/osp/app/signin/SmsVerificationActivity;)Landroid/os/Handler;

    move-result-object v1

    if-eqz v1, :cond_c

    .line 2461
    iget-object v1, p0, Lcom/osp/app/signin/nm;->c:Lcom/osp/app/signin/SmsVerificationActivity;

    invoke-static {v1}, Lcom/osp/app/signin/SmsVerificationActivity;->z(Lcom/osp/app/signin/SmsVerificationActivity;)Landroid/os/Handler;

    move-result-object v1

    invoke-virtual {v1, v4}, Landroid/os/Handler;->removeMessages(I)V

    .line 2464
    :cond_c
    iget-object v1, p0, Lcom/osp/app/signin/nm;->c:Lcom/osp/app/signin/SmsVerificationActivity;

    invoke-static {v1}, Lcom/osp/app/signin/SmsVerificationActivity;->A(Lcom/osp/app/signin/SmsVerificationActivity;)V

    .line 2465
    iget-object v1, p0, Lcom/osp/app/signin/nm;->c:Lcom/osp/app/signin/SmsVerificationActivity;

    invoke-static {v1}, Lcom/osp/app/signin/SmsVerificationActivity;->B(Lcom/osp/app/signin/SmsVerificationActivity;)V

    .line 2466
    iget-object v1, p0, Lcom/osp/app/signin/nm;->c:Lcom/osp/app/signin/SmsVerificationActivity;

    invoke-static {v1}, Lcom/osp/app/signin/SmsVerificationActivity;->H(Lcom/osp/app/signin/SmsVerificationActivity;)V

    .line 2467
    iget-object v1, p0, Lcom/osp/app/signin/nm;->c:Lcom/osp/app/signin/SmsVerificationActivity;

    invoke-static {v1}, Lcom/osp/app/signin/SmsVerificationActivity;->r(Lcom/osp/app/signin/SmsVerificationActivity;)V

    .line 2468
    iget-object v1, p0, Lcom/osp/app/signin/nm;->c:Lcom/osp/app/signin/SmsVerificationActivity;

    const v2, 0x7f0c00c7

    invoke-virtual {v1, v2}, Lcom/osp/app/signin/SmsVerificationActivity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    invoke-virtual {v1, v5}, Landroid/view/View;->setEnabled(Z)V

    .line 2470
    invoke-virtual {v0, v6}, Landroid/widget/TextView;->setVisibility(I)V

    .line 2472
    iget-object v0, p0, Lcom/osp/app/signin/nm;->c:Lcom/osp/app/signin/SmsVerificationActivity;

    invoke-static {v0}, Lcom/osp/app/signin/SmsVerificationActivity;->I(Lcom/osp/app/signin/SmsVerificationActivity;)V

    goto/16 :goto_0

    .line 2398
    :pswitch_0
    iget-object v1, p0, Lcom/osp/app/signin/nm;->c:Lcom/osp/app/signin/SmsVerificationActivity;

    invoke-static {v1}, Lcom/osp/app/signin/SmsVerificationActivity;->u(Lcom/osp/app/signin/SmsVerificationActivity;)Z

    move-result v1

    if-nez v1, :cond_d

    .line 2400
    invoke-static {}, Lcom/osp/app/util/ap;->a()Lcom/osp/app/util/ap;

    iget-object v1, p0, Lcom/osp/app/signin/nm;->c:Lcom/osp/app/signin/SmsVerificationActivity;

    const v2, 0x7f0900d4

    invoke-static {v1, v2, v4}, Lcom/osp/app/util/ap;->a(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v1

    invoke-virtual {v1}, Landroid/widget/Toast;->show()V

    .line 2402
    :cond_d
    iget-object v1, p0, Lcom/osp/app/signin/nm;->c:Lcom/osp/app/signin/SmsVerificationActivity;

    invoke-static {v1, v4}, Lcom/osp/app/signin/SmsVerificationActivity;->c(Lcom/osp/app/signin/SmsVerificationActivity;Z)Z

    .line 2403
    iget-object v1, p0, Lcom/osp/app/signin/nm;->c:Lcom/osp/app/signin/SmsVerificationActivity;

    invoke-static {v1}, Lcom/osp/app/signin/SmsVerificationActivity;->v(Lcom/osp/app/signin/SmsVerificationActivity;)V

    .line 2404
    iget-object v1, p0, Lcom/osp/app/signin/nm;->c:Lcom/osp/app/signin/SmsVerificationActivity;

    invoke-static {v1}, Lcom/osp/app/signin/SmsVerificationActivity;->w(Lcom/osp/app/signin/SmsVerificationActivity;)V

    .line 2405
    iget-object v1, p0, Lcom/osp/app/signin/nm;->c:Lcom/osp/app/signin/SmsVerificationActivity;

    invoke-static {v1}, Lcom/osp/app/signin/SmsVerificationActivity;->x(Lcom/osp/app/signin/SmsVerificationActivity;)V

    .line 2406
    iget-object v1, p0, Lcom/osp/app/signin/nm;->c:Lcom/osp/app/signin/SmsVerificationActivity;

    const v2, 0x7f0c00c4

    invoke-virtual {v1, v2}, Lcom/osp/app/signin/SmsVerificationActivity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/EditText;

    .line 2407
    invoke-virtual {v1}, Landroid/widget/EditText;->requestFocus()Z

    .line 2408
    iget-object v1, p0, Lcom/osp/app/signin/nm;->c:Lcom/osp/app/signin/SmsVerificationActivity;

    invoke-static {v1}, Lcom/osp/app/signin/SmsVerificationActivity;->y(Lcom/osp/app/signin/SmsVerificationActivity;)V

    goto/16 :goto_1

    .line 2412
    :pswitch_1
    iget-object v1, p0, Lcom/osp/app/signin/nm;->c:Lcom/osp/app/signin/SmsVerificationActivity;

    invoke-static {v1}, Lcom/osp/app/signin/SmsVerificationActivity;->z(Lcom/osp/app/signin/SmsVerificationActivity;)Landroid/os/Handler;

    move-result-object v1

    if-eqz v1, :cond_e

    .line 2414
    iget-object v1, p0, Lcom/osp/app/signin/nm;->c:Lcom/osp/app/signin/SmsVerificationActivity;

    invoke-static {v1}, Lcom/osp/app/signin/SmsVerificationActivity;->z(Lcom/osp/app/signin/SmsVerificationActivity;)Landroid/os/Handler;

    move-result-object v1

    invoke-virtual {v1, v4}, Landroid/os/Handler;->removeMessages(I)V

    .line 2416
    :cond_e
    iget-object v1, p0, Lcom/osp/app/signin/nm;->c:Lcom/osp/app/signin/SmsVerificationActivity;

    invoke-static {v1}, Lcom/osp/app/signin/SmsVerificationActivity;->A(Lcom/osp/app/signin/SmsVerificationActivity;)V

    .line 2417
    iget-object v1, p0, Lcom/osp/app/signin/nm;->c:Lcom/osp/app/signin/SmsVerificationActivity;

    invoke-static {v1}, Lcom/osp/app/signin/SmsVerificationActivity;->B(Lcom/osp/app/signin/SmsVerificationActivity;)V

    .line 2418
    invoke-virtual {v0, v6}, Landroid/widget/TextView;->setVisibility(I)V

    .line 2419
    iget-object v1, p0, Lcom/osp/app/signin/nm;->c:Lcom/osp/app/signin/SmsVerificationActivity;

    const-string v2, ""

    invoke-static {v1, v2}, Lcom/osp/app/signin/SmsVerificationActivity;->b(Lcom/osp/app/signin/SmsVerificationActivity;Ljava/lang/String;)V

    .line 2421
    iget v1, p0, Lcom/osp/app/signin/nm;->d:I

    if-ne v1, v3, :cond_f

    .line 2423
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v1, "SMSV"

    const-string v2, "Start UpdateUserLoginIDTask"

    invoke-static {v1, v2}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 2424
    iget-object v1, p0, Lcom/osp/app/signin/nm;->c:Lcom/osp/app/signin/SmsVerificationActivity;

    new-instance v2, Lcom/osp/app/signin/ns;

    iget-object v3, p0, Lcom/osp/app/signin/nm;->c:Lcom/osp/app/signin/SmsVerificationActivity;

    invoke-direct {v2, v3}, Lcom/osp/app/signin/ns;-><init>(Lcom/osp/app/signin/SmsVerificationActivity;)V

    invoke-static {v1, v2}, Lcom/osp/app/signin/SmsVerificationActivity;->a(Lcom/osp/app/signin/SmsVerificationActivity;Lcom/osp/app/signin/ns;)Lcom/osp/app/signin/ns;

    .line 2425
    iget-object v1, p0, Lcom/osp/app/signin/nm;->c:Lcom/osp/app/signin/SmsVerificationActivity;

    invoke-static {v1}, Lcom/osp/app/signin/SmsVerificationActivity;->C(Lcom/osp/app/signin/SmsVerificationActivity;)Lcom/osp/app/signin/ns;

    move-result-object v1

    invoke-static {v1}, Lcom/msc/sa/c/d;->a(Landroid/os/AsyncTask;)V

    goto/16 :goto_1

    .line 2428
    :cond_f
    iget-object v1, p0, Lcom/osp/app/signin/nm;->c:Lcom/osp/app/signin/SmsVerificationActivity;

    invoke-virtual {v1}, Lcom/osp/app/signin/SmsVerificationActivity;->c()V

    goto/16 :goto_1

    .line 2434
    :pswitch_2
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v1, "SMSV"

    const-string v2, "onPostExecute : Success Public SMS Verification"

    invoke-static {v1, v2}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 2435
    iget-object v1, p0, Lcom/osp/app/signin/nm;->c:Lcom/osp/app/signin/SmsVerificationActivity;

    invoke-static {v1}, Lcom/osp/app/signin/SmsVerificationActivity;->z(Lcom/osp/app/signin/SmsVerificationActivity;)Landroid/os/Handler;

    move-result-object v1

    if-eqz v1, :cond_10

    .line 2437
    iget-object v1, p0, Lcom/osp/app/signin/nm;->c:Lcom/osp/app/signin/SmsVerificationActivity;

    invoke-static {v1}, Lcom/osp/app/signin/SmsVerificationActivity;->z(Lcom/osp/app/signin/SmsVerificationActivity;)Landroid/os/Handler;

    move-result-object v1

    invoke-virtual {v1, v4}, Landroid/os/Handler;->removeMessages(I)V

    .line 2439
    :cond_10
    iget-object v1, p0, Lcom/osp/app/signin/nm;->c:Lcom/osp/app/signin/SmsVerificationActivity;

    invoke-static {v1}, Lcom/osp/app/signin/SmsVerificationActivity;->A(Lcom/osp/app/signin/SmsVerificationActivity;)V

    .line 2440
    iget-object v1, p0, Lcom/osp/app/signin/nm;->c:Lcom/osp/app/signin/SmsVerificationActivity;

    invoke-static {v1}, Lcom/osp/app/signin/SmsVerificationActivity;->B(Lcom/osp/app/signin/SmsVerificationActivity;)V

    .line 2441
    invoke-virtual {v0, v6}, Landroid/widget/TextView;->setVisibility(I)V

    .line 2442
    iget-object v1, p0, Lcom/osp/app/signin/nm;->c:Lcom/osp/app/signin/SmsVerificationActivity;

    const-string v2, ""

    invoke-static {v1, v2}, Lcom/osp/app/signin/SmsVerificationActivity;->b(Lcom/osp/app/signin/SmsVerificationActivity;Ljava/lang/String;)V

    .line 2444
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "mCountryCallingCode : "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, Lcom/osp/app/signin/nm;->c:Lcom/osp/app/signin/SmsVerificationActivity;

    invoke-static {v2}, Lcom/osp/app/signin/SmsVerificationActivity;->j(Lcom/osp/app/signin/SmsVerificationActivity;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "\tmPhoneNumber"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/osp/app/signin/nm;->c:Lcom/osp/app/signin/SmsVerificationActivity;

    invoke-static {v2}, Lcom/osp/app/signin/SmsVerificationActivity;->D(Lcom/osp/app/signin/SmsVerificationActivity;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;)V

    .line 2445
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "mDeviceIdWithSHA1 : "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, Lcom/osp/app/signin/nm;->c:Lcom/osp/app/signin/SmsVerificationActivity;

    invoke-static {v2}, Lcom/osp/app/signin/SmsVerificationActivity;->E(Lcom/osp/app/signin/SmsVerificationActivity;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "\tmImsi"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/osp/app/signin/nm;->c:Lcom/osp/app/signin/SmsVerificationActivity;

    invoke-static {v2}, Lcom/osp/app/signin/SmsVerificationActivity;->F(Lcom/osp/app/signin/SmsVerificationActivity;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;)V

    .line 2447
    iget-object v1, p0, Lcom/osp/app/signin/nm;->c:Lcom/osp/app/signin/SmsVerificationActivity;

    invoke-static {v1}, Lcom/osp/app/signin/SmsVerificationActivity;->g(Lcom/osp/app/signin/SmsVerificationActivity;)Landroid/content/Intent;

    move-result-object v1

    const-string v2, "did"

    iget-object v3, p0, Lcom/osp/app/signin/nm;->c:Lcom/osp/app/signin/SmsVerificationActivity;

    invoke-static {v3}, Lcom/osp/app/signin/SmsVerificationActivity;->E(Lcom/osp/app/signin/SmsVerificationActivity;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 2448
    iget-object v1, p0, Lcom/osp/app/signin/nm;->c:Lcom/osp/app/signin/SmsVerificationActivity;

    invoke-static {v1}, Lcom/osp/app/signin/SmsVerificationActivity;->g(Lcom/osp/app/signin/SmsVerificationActivity;)Landroid/content/Intent;

    move-result-object v1

    const-string v2, "imsi"

    iget-object v3, p0, Lcom/osp/app/signin/nm;->c:Lcom/osp/app/signin/SmsVerificationActivity;

    invoke-static {v3}, Lcom/osp/app/signin/SmsVerificationActivity;->F(Lcom/osp/app/signin/SmsVerificationActivity;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 2449
    iget-object v1, p0, Lcom/osp/app/signin/nm;->c:Lcom/osp/app/signin/SmsVerificationActivity;

    const/4 v2, -0x1

    iget-object v3, p0, Lcom/osp/app/signin/nm;->c:Lcom/osp/app/signin/SmsVerificationActivity;

    invoke-static {v3}, Lcom/osp/app/signin/SmsVerificationActivity;->g(Lcom/osp/app/signin/SmsVerificationActivity;)Landroid/content/Intent;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Lcom/osp/app/signin/SmsVerificationActivity;->a(ILandroid/content/Intent;)V

    .line 2450
    iget-object v1, p0, Lcom/osp/app/signin/nm;->c:Lcom/osp/app/signin/SmsVerificationActivity;

    invoke-virtual {v1}, Lcom/osp/app/signin/SmsVerificationActivity;->finish()V

    goto/16 :goto_1

    .line 2394
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_1
        :pswitch_2
        :pswitch_0
    .end packed-switch
.end method

.method public final b(Lcom/msc/b/c;)V
    .locals 11

    .prologue
    const v10, 0x7f0900b0

    const/16 v9, 0x18

    const v8, 0x7f0900b1

    const/4 v7, 0x0

    const/4 v6, 0x1

    .line 2773
    invoke-super {p0, p1}, Lcom/msc/c/b;->b(Lcom/msc/b/c;)V

    .line 2775
    if-nez p1, :cond_1

    .line 2832
    :cond_0
    :goto_0
    return-void

    .line 2780
    :cond_1
    invoke-virtual {p1}, Lcom/msc/b/c;->a()J

    move-result-wide v0

    .line 2781
    invoke-virtual {p1}, Lcom/msc/b/c;->e()Ljava/lang/String;

    move-result-object v2

    .line 2783
    iget-wide v4, p0, Lcom/osp/app/signin/nm;->e:J

    cmp-long v3, v0, v4

    if-nez v3, :cond_4

    .line 2785
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v0, "SMSV"

    const-string v1, "RequestSMSAuthenticate failed"

    invoke-static {v0, v1}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 2786
    if-eqz v2, :cond_3

    .line 2788
    const-string v0, "USR_3156"

    iget-object v1, p0, Lcom/osp/app/signin/nm;->b:Lcom/msc/c/f;

    invoke-virtual {v1}, Lcom/msc/c/f;->a()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 2790
    iget-object v0, p0, Lcom/osp/app/signin/nm;->b:Lcom/msc/c/f;

    iget-object v1, p0, Lcom/osp/app/signin/nm;->c:Lcom/osp/app/signin/SmsVerificationActivity;

    const v2, 0x7f0900d3

    invoke-virtual {v1, v2}, Lcom/osp/app/signin/SmsVerificationActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    new-array v2, v6, [Ljava/lang/Object;

    invoke-static {v9}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v2, v7

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/msc/c/f;->b(Ljava/lang/String;)V

    goto :goto_0

    .line 2793
    :cond_2
    const-string v0, "USR_3166"

    iget-object v1, p0, Lcom/osp/app/signin/nm;->b:Lcom/msc/c/f;

    invoke-virtual {v1}, Lcom/msc/c/f;->a()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 2795
    iget-object v0, p0, Lcom/osp/app/signin/nm;->b:Lcom/msc/c/f;

    iget-object v1, p0, Lcom/osp/app/signin/nm;->c:Lcom/osp/app/signin/SmsVerificationActivity;

    const v2, 0x7f090134

    invoke-virtual {v1, v2}, Lcom/osp/app/signin/SmsVerificationActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    new-array v2, v6, [Ljava/lang/Object;

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v2, v7

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/msc/c/f;->b(Ljava/lang/String;)V

    goto :goto_0

    .line 2799
    :cond_3
    iget-object v0, p0, Lcom/osp/app/signin/nm;->b:Lcom/msc/c/f;

    iget-object v1, p0, Lcom/osp/app/signin/nm;->c:Lcom/osp/app/signin/SmsVerificationActivity;

    invoke-virtual {v1, v8}, Lcom/osp/app/signin/SmsVerificationActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/msc/c/f;->b(Ljava/lang/String;)V

    goto :goto_0

    .line 2801
    :cond_4
    iget-wide v2, p0, Lcom/osp/app/signin/nm;->f:J

    cmp-long v2, v0, v2

    if-nez v2, :cond_5

    .line 2803
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v0, "SMSV"

    const-string v1, "ValidateSMSAuthenticate failed"

    invoke-static {v0, v1}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 2804
    iget-object v0, p0, Lcom/osp/app/signin/nm;->b:Lcom/msc/c/f;

    iget-object v1, p0, Lcom/osp/app/signin/nm;->c:Lcom/osp/app/signin/SmsVerificationActivity;

    invoke-virtual {v1, v10}, Lcom/osp/app/signin/SmsVerificationActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/msc/c/f;->b(Ljava/lang/String;)V

    goto/16 :goto_0

    .line 2806
    :cond_5
    iget-wide v2, p0, Lcom/osp/app/signin/nm;->g:J

    cmp-long v2, v0, v2

    if-nez v2, :cond_7

    .line 2808
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v0, "SMSV"

    const-string v1, "RequestNewSMSVerifyCodeWithDPServer failed"

    invoke-static {v0, v1}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 2810
    const-string v0, "4000401123"

    iget-object v1, p0, Lcom/osp/app/signin/nm;->b:Lcom/msc/c/f;

    invoke-virtual {v1}, Lcom/msc/c/f;->a()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_6

    .line 2812
    iget-object v0, p0, Lcom/osp/app/signin/nm;->b:Lcom/msc/c/f;

    iget-object v1, p0, Lcom/osp/app/signin/nm;->c:Lcom/osp/app/signin/SmsVerificationActivity;

    const v2, 0x7f0900d3

    invoke-virtual {v1, v2}, Lcom/osp/app/signin/SmsVerificationActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    new-array v2, v6, [Ljava/lang/Object;

    invoke-static {v9}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v2, v7

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/msc/c/f;->b(Ljava/lang/String;)V

    goto/16 :goto_0

    .line 2816
    :cond_6
    iget-object v0, p0, Lcom/osp/app/signin/nm;->b:Lcom/msc/c/f;

    iget-object v1, p0, Lcom/osp/app/signin/nm;->c:Lcom/osp/app/signin/SmsVerificationActivity;

    invoke-virtual {v1, v8}, Lcom/osp/app/signin/SmsVerificationActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/msc/c/f;->b(Ljava/lang/String;)V

    goto/16 :goto_0

    .line 2818
    :cond_7
    iget-wide v2, p0, Lcom/osp/app/signin/nm;->h:J

    cmp-long v2, v0, v2

    if-nez v2, :cond_9

    .line 2820
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v0, "SMSV"

    const-string v1, "RequestNewValidate failed"

    invoke-static {v0, v1}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 2821
    const-string v0, "USR_3177"

    iget-object v1, p0, Lcom/osp/app/signin/nm;->b:Lcom/msc/c/f;

    invoke-virtual {v1}, Lcom/msc/c/f;->a()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_8

    .line 2823
    iget-object v0, p0, Lcom/osp/app/signin/nm;->b:Lcom/msc/c/f;

    iget-object v1, p0, Lcom/osp/app/signin/nm;->c:Lcom/osp/app/signin/SmsVerificationActivity;

    invoke-virtual {v1, v10}, Lcom/osp/app/signin/SmsVerificationActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/msc/c/f;->b(Ljava/lang/String;)V

    goto/16 :goto_0

    .line 2826
    :cond_8
    iget-object v0, p0, Lcom/osp/app/signin/nm;->b:Lcom/msc/c/f;

    iget-object v1, p0, Lcom/osp/app/signin/nm;->c:Lcom/osp/app/signin/SmsVerificationActivity;

    invoke-virtual {v1, v8}, Lcom/osp/app/signin/SmsVerificationActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/msc/c/f;->b(Ljava/lang/String;)V

    goto/16 :goto_0

    .line 2828
    :cond_9
    iget-wide v2, p0, Lcom/osp/app/signin/nm;->i:J

    cmp-long v0, v0, v2

    if-nez v0, :cond_0

    .line 2830
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v0, "SMSV"

    const-string v1, "RequestIsAuthForDPId failed"

    invoke-static {v0, v1}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0
.end method

.method protected final d()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 2836
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v0, "SMSV"

    const-string v1, "SMSAuthenticateTask canceled"

    invoke-static {v0, v1}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 2838
    iget-object v0, p0, Lcom/osp/app/signin/nm;->c:Lcom/osp/app/signin/SmsVerificationActivity;

    invoke-static {v0, v2}, Lcom/osp/app/signin/SmsVerificationActivity;->d(Lcom/osp/app/signin/SmsVerificationActivity;Z)Z

    .line 2839
    iget-object v0, p0, Lcom/osp/app/signin/nm;->c:Lcom/osp/app/signin/SmsVerificationActivity;

    const v1, 0x7f0c00c7

    invoke-virtual {v0, v1}, Lcom/osp/app/signin/SmsVerificationActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/view/View;->setEnabled(Z)V

    .line 2840
    iget-object v0, p0, Lcom/osp/app/signin/nm;->c:Lcom/osp/app/signin/SmsVerificationActivity;

    invoke-static {v0}, Lcom/osp/app/signin/SmsVerificationActivity;->Q(Lcom/osp/app/signin/SmsVerificationActivity;)Lcom/osp/app/signin/BottomSoftkeyLinearLayout;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 2842
    invoke-static {}, Lcom/osp/device/b;->b()Lcom/osp/device/b;

    move-result-object v0

    iget-object v1, p0, Lcom/osp/app/signin/nm;->c:Lcom/osp/app/signin/SmsVerificationActivity;

    invoke-virtual {v0, v1}, Lcom/osp/device/b;->l(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 2844
    iget-object v0, p0, Lcom/osp/app/signin/nm;->c:Lcom/osp/app/signin/SmsVerificationActivity;

    invoke-static {v0}, Lcom/osp/app/signin/SmsVerificationActivity;->Q(Lcom/osp/app/signin/SmsVerificationActivity;)Lcom/osp/app/signin/BottomSoftkeyLinearLayout;

    move-result-object v0

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/osp/app/signin/BottomSoftkeyLinearLayout;->a(Ljava/lang/Boolean;)V

    .line 2851
    :cond_0
    :goto_0
    iget-object v0, p0, Lcom/osp/app/signin/nm;->c:Lcom/osp/app/signin/SmsVerificationActivity;

    invoke-static {v0}, Lcom/osp/app/signin/SmsVerificationActivity;->A(Lcom/osp/app/signin/SmsVerificationActivity;)V

    .line 2852
    iget-object v0, p0, Lcom/osp/app/signin/nm;->c:Lcom/osp/app/signin/SmsVerificationActivity;

    invoke-static {v0}, Lcom/osp/app/signin/SmsVerificationActivity;->B(Lcom/osp/app/signin/SmsVerificationActivity;)V

    .line 2853
    iget-object v0, p0, Lcom/osp/app/signin/nm;->c:Lcom/osp/app/signin/SmsVerificationActivity;

    invoke-static {v0}, Lcom/osp/app/signin/SmsVerificationActivity;->H(Lcom/osp/app/signin/SmsVerificationActivity;)V

    .line 2854
    iget-object v0, p0, Lcom/osp/app/signin/nm;->c:Lcom/osp/app/signin/SmsVerificationActivity;

    invoke-static {v0}, Lcom/osp/app/signin/SmsVerificationActivity;->r(Lcom/osp/app/signin/SmsVerificationActivity;)V

    .line 2856
    iget-object v0, p0, Lcom/osp/app/signin/nm;->c:Lcom/osp/app/signin/SmsVerificationActivity;

    invoke-static {v0}, Lcom/osp/app/signin/SmsVerificationActivity;->z(Lcom/osp/app/signin/SmsVerificationActivity;)Landroid/os/Handler;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 2858
    iget-object v0, p0, Lcom/osp/app/signin/nm;->c:Lcom/osp/app/signin/SmsVerificationActivity;

    invoke-static {v0}, Lcom/osp/app/signin/SmsVerificationActivity;->z(Lcom/osp/app/signin/SmsVerificationActivity;)Landroid/os/Handler;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/os/Handler;->removeMessages(I)V

    .line 2861
    :cond_1
    iget-object v0, p0, Lcom/osp/app/signin/nm;->c:Lcom/osp/app/signin/SmsVerificationActivity;

    const v1, 0x7f0c00c6

    invoke-virtual {v0, v1}, Lcom/osp/app/signin/SmsVerificationActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 2862
    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    .line 2864
    invoke-super {p0}, Lcom/msc/c/b;->d()V

    .line 2865
    return-void

    .line 2847
    :cond_2
    iget-object v0, p0, Lcom/osp/app/signin/nm;->c:Lcom/osp/app/signin/SmsVerificationActivity;

    invoke-static {v0}, Lcom/osp/app/signin/SmsVerificationActivity;->Q(Lcom/osp/app/signin/SmsVerificationActivity;)Lcom/osp/app/signin/BottomSoftkeyLinearLayout;

    move-result-object v0

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/osp/app/signin/BottomSoftkeyLinearLayout;->b(Ljava/lang/Boolean;)V

    goto :goto_0
.end method

.method protected final synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 2244
    invoke-virtual {p0}, Lcom/osp/app/signin/nm;->a()Ljava/lang/Boolean;

    move-result-object v0

    return-object v0
.end method

.method protected final synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 2244
    check-cast p1, Ljava/lang/Boolean;

    invoke-virtual {p0, p1}, Lcom/osp/app/signin/nm;->a(Ljava/lang/Boolean;)V

    return-void
.end method
