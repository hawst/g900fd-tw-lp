.class final Lcom/osp/app/signin/j;
.super Landroid/widget/BaseAdapter;
.source "AccountServiceList.java"


# instance fields
.field final synthetic a:Lcom/osp/app/signin/h;

.field private final b:Landroid/view/LayoutInflater;

.field private c:Ljava/util/ArrayList;


# direct methods
.method public constructor <init>(Lcom/osp/app/signin/h;Landroid/content/Context;Ljava/util/ArrayList;)V
    .locals 2

    .prologue
    .line 769
    iput-object p1, p0, Lcom/osp/app/signin/j;->a:Lcom/osp/app/signin/h;

    invoke-direct {p0}, Landroid/widget/BaseAdapter;-><init>()V

    .line 767
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/osp/app/signin/j;->c:Ljava/util/ArrayList;

    .line 770
    invoke-static {p2}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    iput-object v0, p0, Lcom/osp/app/signin/j;->b:Landroid/view/LayoutInflater;

    .line 772
    if-eqz p3, :cond_0

    invoke-virtual {p3}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-eqz v0, :cond_0

    .line 774
    iput-object p3, p0, Lcom/osp/app/signin/j;->c:Ljava/util/ArrayList;

    .line 779
    :goto_0
    return-void

    .line 777
    :cond_0
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v0, "ASL"

    const-string v1, "ServiceList is null."

    invoke-static {v0, v1}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method private a(I)Lcom/osp/app/signin/i;
    .locals 1

    .prologue
    .line 807
    iget-object v0, p0, Lcom/osp/app/signin/j;->c:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/osp/app/signin/i;

    return-object v0
.end method


# virtual methods
.method public final a(Ljava/lang/CharSequence;Landroid/view/View$OnClickListener;)V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 788
    iget-object v0, p0, Lcom/osp/app/signin/j;->c:Ljava/util/ArrayList;

    invoke-virtual {v0, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/osp/app/signin/i;

    iget v0, v0, Lcom/osp/app/signin/i;->c:I

    const v1, 0x8000

    if-eq v0, v1, :cond_0

    .line 790
    iget-object v0, p0, Lcom/osp/app/signin/j;->c:Ljava/util/ArrayList;

    new-instance v1, Lcom/osp/app/signin/i;

    iget-object v2, p0, Lcom/osp/app/signin/j;->a:Lcom/osp/app/signin/h;

    invoke-direct {v1, v2, p1, p2}, Lcom/osp/app/signin/i;-><init>(Lcom/osp/app/signin/h;Ljava/lang/CharSequence;Landroid/view/View$OnClickListener;)V

    invoke-virtual {v0, v3, v1}, Ljava/util/ArrayList;->add(ILjava/lang/Object;)V

    .line 791
    invoke-virtual {p0}, Lcom/osp/app/signin/j;->notifyDataSetChanged()V

    .line 793
    :cond_0
    return-void
.end method

.method public final getCount()I
    .locals 1

    .prologue
    .line 802
    iget-object v0, p0, Lcom/osp/app/signin/j;->c:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    return v0
.end method

.method public final synthetic getItem(I)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 763
    invoke-direct {p0, p1}, Lcom/osp/app/signin/j;->a(I)Lcom/osp/app/signin/i;

    move-result-object v0

    return-object v0
.end method

.method public final getItemId(I)J
    .locals 2

    .prologue
    .line 812
    int-to-long v0, p1

    return-wide v0
.end method

.method public final getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 11

    .prologue
    const v10, 0x8000

    const/4 v4, 0x1

    const/16 v9, 0x15

    const/16 v8, 0x8

    const/4 v5, 0x0

    .line 818
    if-nez p2, :cond_1

    .line 821
    new-instance v1, Lcom/osp/app/signin/k;

    invoke-direct {v1, p0, v5}, Lcom/osp/app/signin/k;-><init>(Lcom/osp/app/signin/j;B)V

    .line 822
    iget-object v0, p0, Lcom/osp/app/signin/j;->b:Landroid/view/LayoutInflater;

    const v2, 0x7f03000a

    const/4 v3, 0x0

    invoke-virtual {v0, v2, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object p2

    .line 823
    const v0, 0x7f0c0073

    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, v1, Lcom/osp/app/signin/k;->a:Landroid/widget/TextView;

    .line 824
    const v0, 0x7f0c0074

    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, v1, Lcom/osp/app/signin/k;->b:Landroid/widget/TextView;

    .line 825
    const v0, 0x7f0c0078

    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, v1, Lcom/osp/app/signin/k;->c:Landroid/widget/TextView;

    .line 826
    const v0, 0x7f0c0075

    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, v1, Lcom/osp/app/signin/k;->d:Landroid/widget/LinearLayout;

    .line 827
    const v0, 0x7f0c0077

    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, v1, Lcom/osp/app/signin/k;->e:Landroid/widget/TextView;

    .line 828
    const v0, 0x7f0c0076

    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, v1, Lcom/osp/app/signin/k;->f:Landroid/widget/ImageView;

    .line 830
    invoke-virtual {p2, v1}, Landroid/view/View;->setTag(Ljava/lang/Object;)V

    move-object v2, v1

    .line 837
    :goto_0
    iget-object v0, v2, Lcom/osp/app/signin/k;->d:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v8}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 838
    iget-object v0, v2, Lcom/osp/app/signin/k;->a:Landroid/widget/TextView;

    invoke-virtual {v0, v8}, Landroid/widget/TextView;->setVisibility(I)V

    .line 839
    iget-object v0, v2, Lcom/osp/app/signin/k;->b:Landroid/widget/TextView;

    invoke-virtual {v0, v8}, Landroid/widget/TextView;->setVisibility(I)V

    .line 840
    iget-object v0, v2, Lcom/osp/app/signin/k;->c:Landroid/widget/TextView;

    invoke-virtual {v0, v8}, Landroid/widget/TextView;->setVisibility(I)V

    .line 842
    invoke-direct {p0, p1}, Lcom/osp/app/signin/j;->a(I)Lcom/osp/app/signin/i;

    move-result-object v1

    .line 843
    if-eqz v1, :cond_0

    .line 845
    iget-object v0, p0, Lcom/osp/app/signin/j;->a:Lcom/osp/app/signin/h;

    invoke-static {v0}, Lcom/osp/app/signin/h;->a(Lcom/osp/app/signin/h;)Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v0

    iget v0, v0, Landroid/content/res/Configuration;->orientation:I

    const/4 v3, 0x2

    if-ne v0, v3, :cond_2

    move v3, v4

    .line 846
    :goto_1
    invoke-static {}, Lcom/osp/device/b;->b()Lcom/osp/device/b;

    move-result-object v0

    iget-object v6, p0, Lcom/osp/app/signin/j;->a:Lcom/osp/app/signin/h;

    invoke-static {v6}, Lcom/osp/app/signin/h;->a(Lcom/osp/app/signin/h;)Landroid/content/Context;

    move-result-object v6

    invoke-virtual {v0, v6}, Lcom/osp/device/b;->i(Landroid/content/Context;)Z

    move-result v6

    .line 848
    iget v0, v1, Lcom/osp/app/signin/i;->c:I

    and-int/2addr v0, v10

    if-ne v0, v10, :cond_5

    .line 850
    iget-object v0, v2, Lcom/osp/app/signin/k;->a:Landroid/widget/TextView;

    invoke-virtual {v0, v5}, Landroid/widget/TextView;->setVisibility(I)V

    .line 853
    iget-object v0, v2, Lcom/osp/app/signin/k;->a:Landroid/widget/TextView;

    iget-object v4, v1, Lcom/osp/app/signin/i;->d:Ljava/lang/CharSequence;

    invoke-virtual {v0, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 854
    iget-object v0, v2, Lcom/osp/app/signin/k;->a:Landroid/widget/TextView;

    iget-object v1, v1, Lcom/osp/app/signin/i;->e:Landroid/view/View$OnClickListener;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 855
    iget-object v1, v2, Lcom/osp/app/signin/k;->a:Landroid/widget/TextView;

    iget-object v0, p0, Lcom/osp/app/signin/j;->a:Lcom/osp/app/signin/h;

    invoke-static {v0}, Lcom/osp/app/signin/h;->a(Lcom/osp/app/signin/h;)Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    iget-object v0, p0, Lcom/osp/app/signin/j;->a:Lcom/osp/app/signin/h;

    invoke-static {v0}, Lcom/osp/app/signin/h;->a(Lcom/osp/app/signin/h;)Landroid/content/Context;

    move-result-object v0

    check-cast v0, Lcom/osp/app/util/BaseActivity;

    invoke-static {v0}, Lcom/osp/app/util/an;->a(Lcom/osp/app/util/BaseActivity;)Lcom/osp/app/util/an;

    const v0, 0x7f07001e

    invoke-static {v0}, Lcom/osp/app/util/an;->a(I)I

    move-result v0

    invoke-virtual {v4, v0}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setTextColor(I)V

    .line 857
    iget-object v1, v2, Lcom/osp/app/signin/k;->a:Landroid/widget/TextView;

    iget-object v0, p0, Lcom/osp/app/signin/j;->a:Lcom/osp/app/signin/h;

    invoke-static {v0}, Lcom/osp/app/signin/h;->a(Lcom/osp/app/signin/h;)Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    iget-object v0, p0, Lcom/osp/app/signin/j;->a:Lcom/osp/app/signin/h;

    invoke-static {v0}, Lcom/osp/app/signin/h;->a(Lcom/osp/app/signin/h;)Landroid/content/Context;

    move-result-object v0

    check-cast v0, Lcom/osp/app/util/BaseActivity;

    invoke-static {v0}, Lcom/osp/app/util/an;->a(Lcom/osp/app/util/BaseActivity;)Lcom/osp/app/util/an;

    const v0, 0x7f07001e

    invoke-static {v0}, Lcom/osp/app/util/an;->a(I)I

    move-result v0

    invoke-virtual {v4, v0}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setLinkTextColor(I)V

    .line 860
    iget-object v0, p0, Lcom/osp/app/signin/j;->a:Lcom/osp/app/signin/h;

    invoke-static {v0}, Lcom/osp/app/signin/h;->a(Lcom/osp/app/signin/h;)Landroid/content/Context;

    move-result-object v0

    check-cast v0, Lcom/osp/app/util/BaseActivity;

    invoke-static {v0}, Lcom/osp/app/util/an;->a(Lcom/osp/app/util/BaseActivity;)Lcom/osp/app/util/an;

    const v0, 0x7f08004d

    invoke-static {v0}, Lcom/osp/app/util/an;->b(I)I

    move-result v0

    .line 862
    iget-object v1, v2, Lcom/osp/app/signin/k;->a:Landroid/widget/TextView;

    iget-object v4, p0, Lcom/osp/app/signin/j;->a:Lcom/osp/app/signin/h;

    invoke-static {v4}, Lcom/osp/app/signin/h;->a(Lcom/osp/app/signin/h;)Landroid/content/Context;

    move-result-object v4

    invoke-virtual {v4}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    invoke-virtual {v4, v0}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v0

    invoke-virtual {v1, v5, v0}, Landroid/widget/TextView;->setTextSize(IF)V

    .line 865
    iget-object v0, v2, Lcom/osp/app/signin/k;->a:Landroid/widget/TextView;

    const/4 v1, 0x2

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setMaxLines(I)V

    .line 867
    iget-object v0, v2, Lcom/osp/app/signin/k;->a:Landroid/widget/TextView;

    invoke-virtual {v0}, Landroid/widget/TextView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup$MarginLayoutParams;

    .line 868
    if-eqz v3, :cond_4

    .line 871
    if-nez v6, :cond_3

    .line 873
    iget-object v1, v2, Lcom/osp/app/signin/k;->a:Landroid/widget/TextView;

    invoke-virtual {v1, v8}, Landroid/widget/TextView;->setVisibility(I)V

    .line 890
    :goto_2
    iget-object v1, v2, Lcom/osp/app/signin/k;->a:Landroid/widget/TextView;

    invoke-static {v1, v0}, Lcom/msc/sa/c/d;->a(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    .line 892
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    if-lt v0, v9, :cond_0

    .line 894
    iget-object v0, v2, Lcom/osp/app/signin/k;->a:Landroid/widget/TextView;

    const-string v1, "sec-roboto-light"

    invoke-static {v1, v5}, Landroid/graphics/Typeface;->create(Ljava/lang/String;I)Landroid/graphics/Typeface;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setTypeface(Landroid/graphics/Typeface;)V

    .line 1074
    :cond_0
    :goto_3
    return-object p2

    .line 834
    :cond_1
    invoke-virtual {p2}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/osp/app/signin/k;

    move-object v2, v0

    goto/16 :goto_0

    :cond_2
    move v3, v5

    .line 845
    goto/16 :goto_1

    .line 876
    :cond_3
    iget-object v1, v2, Lcom/osp/app/signin/k;->a:Landroid/widget/TextView;

    const/4 v3, 0x3

    invoke-static {v1, v3}, Lcom/msc/sa/c/d;->a(Landroid/view/View;I)V

    .line 878
    iget-object v1, p0, Lcom/osp/app/signin/j;->a:Lcom/osp/app/signin/h;

    invoke-static {v1}, Lcom/osp/app/signin/h;->a(Lcom/osp/app/signin/h;)Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v3, 0x7f080064

    invoke-virtual {v1, v3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    iput v1, v0, Landroid/view/ViewGroup$MarginLayoutParams;->topMargin:I

    .line 879
    iget-object v1, p0, Lcom/osp/app/signin/j;->a:Lcom/osp/app/signin/h;

    invoke-static {v1}, Lcom/osp/app/signin/h;->a(Lcom/osp/app/signin/h;)Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v3, 0x7f080064

    invoke-virtual {v1, v3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    iput v1, v0, Landroid/view/ViewGroup$MarginLayoutParams;->bottomMargin:I

    goto :goto_2

    .line 885
    :cond_4
    iget-object v1, v2, Lcom/osp/app/signin/k;->a:Landroid/widget/TextView;

    const/4 v3, 0x3

    invoke-static {v1, v3}, Lcom/msc/sa/c/d;->a(Landroid/view/View;I)V

    .line 887
    iget-object v1, p0, Lcom/osp/app/signin/j;->a:Lcom/osp/app/signin/h;

    invoke-static {v1}, Lcom/osp/app/signin/h;->a(Lcom/osp/app/signin/h;)Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v3, 0x7f08004e

    invoke-virtual {v1, v3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    iput v1, v0, Landroid/view/ViewGroup$MarginLayoutParams;->topMargin:I

    .line 888
    iget-object v1, p0, Lcom/osp/app/signin/j;->a:Lcom/osp/app/signin/h;

    invoke-static {v1}, Lcom/osp/app/signin/h;->a(Lcom/osp/app/signin/h;)Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v3, 0x7f08004f

    invoke-virtual {v1, v3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    iput v1, v0, Landroid/view/ViewGroup$MarginLayoutParams;->bottomMargin:I

    goto :goto_2

    .line 897
    :cond_5
    iget v0, v1, Lcom/osp/app/signin/i;->c:I

    and-int/lit16 v0, v0, 0x1000

    const/16 v7, 0x1000

    if-ne v0, v7, :cond_6

    .line 899
    iget-object v0, v2, Lcom/osp/app/signin/k;->a:Landroid/widget/TextView;

    invoke-virtual {v0, v5}, Landroid/widget/TextView;->setVisibility(I)V

    .line 902
    iget-object v0, v2, Lcom/osp/app/signin/k;->a:Landroid/widget/TextView;

    iget v1, v1, Lcom/osp/app/signin/i;->b:I

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    .line 903
    iget-object v1, v2, Lcom/osp/app/signin/k;->a:Landroid/widget/TextView;

    iget-object v0, p0, Lcom/osp/app/signin/j;->a:Lcom/osp/app/signin/h;

    invoke-static {v0}, Lcom/osp/app/signin/h;->a(Lcom/osp/app/signin/h;)Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    iget-object v0, p0, Lcom/osp/app/signin/j;->a:Lcom/osp/app/signin/h;

    invoke-static {v0}, Lcom/osp/app/signin/h;->a(Lcom/osp/app/signin/h;)Landroid/content/Context;

    move-result-object v0

    check-cast v0, Lcom/osp/app/util/BaseActivity;

    invoke-static {v0}, Lcom/osp/app/util/an;->a(Lcom/osp/app/util/BaseActivity;)Lcom/osp/app/util/an;

    const v0, 0x7f070004

    invoke-static {v0}, Lcom/osp/app/util/an;->a(I)I

    move-result v0

    invoke-virtual {v3, v0}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setTextColor(I)V

    .line 906
    iget-object v0, v2, Lcom/osp/app/signin/k;->a:Landroid/widget/TextView;

    invoke-virtual {v0}, Landroid/widget/TextView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup$MarginLayoutParams;

    .line 907
    iget-object v1, p0, Lcom/osp/app/signin/j;->a:Lcom/osp/app/signin/h;

    invoke-static {v1}, Lcom/osp/app/signin/h;->a(Lcom/osp/app/signin/h;)Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v3, 0x7f080053

    invoke-virtual {v1, v3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    iput v1, v0, Landroid/view/ViewGroup$MarginLayoutParams;->topMargin:I

    .line 908
    iget-object v1, p0, Lcom/osp/app/signin/j;->a:Lcom/osp/app/signin/h;

    invoke-static {v1}, Lcom/osp/app/signin/h;->a(Lcom/osp/app/signin/h;)Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v3, 0x7f080054

    invoke-virtual {v1, v3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    iput v1, v0, Landroid/view/ViewGroup$MarginLayoutParams;->bottomMargin:I

    .line 909
    iget-object v1, v2, Lcom/osp/app/signin/k;->a:Landroid/widget/TextView;

    invoke-static {v1, v0}, Lcom/msc/sa/c/d;->a(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    .line 911
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    if-lt v0, v9, :cond_0

    .line 913
    iget-object v0, v2, Lcom/osp/app/signin/k;->a:Landroid/widget/TextView;

    const-string v1, "sec-roboto-light"

    invoke-static {v1, v5}, Landroid/graphics/Typeface;->create(Ljava/lang/String;I)Landroid/graphics/Typeface;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setTypeface(Landroid/graphics/Typeface;)V

    goto/16 :goto_3

    .line 916
    :cond_6
    iget v0, v1, Lcom/osp/app/signin/i;->c:I

    and-int/lit16 v0, v0, 0x2000

    const/16 v7, 0x2000

    if-ne v0, v7, :cond_c

    .line 918
    iget-object v0, v2, Lcom/osp/app/signin/k;->b:Landroid/widget/TextView;

    invoke-virtual {v0, v5}, Landroid/widget/TextView;->setVisibility(I)V

    .line 921
    iget-object v0, v2, Lcom/osp/app/signin/k;->b:Landroid/widget/TextView;

    iget v1, v1, Lcom/osp/app/signin/i;->b:I

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    .line 922
    iget-object v1, v2, Lcom/osp/app/signin/k;->b:Landroid/widget/TextView;

    iget-object v0, p0, Lcom/osp/app/signin/j;->a:Lcom/osp/app/signin/h;

    invoke-static {v0}, Lcom/osp/app/signin/h;->a(Lcom/osp/app/signin/h;)Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v7

    iget-object v0, p0, Lcom/osp/app/signin/j;->a:Lcom/osp/app/signin/h;

    invoke-static {v0}, Lcom/osp/app/signin/h;->a(Lcom/osp/app/signin/h;)Landroid/content/Context;

    move-result-object v0

    check-cast v0, Lcom/osp/app/util/BaseActivity;

    invoke-static {v0}, Lcom/osp/app/util/an;->a(Lcom/osp/app/util/BaseActivity;)Lcom/osp/app/util/an;

    const v0, 0x7f07000a

    invoke-static {v0}, Lcom/osp/app/util/an;->a(I)I

    move-result v0

    invoke-virtual {v7, v0}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setTextColor(I)V

    .line 925
    iget-object v0, p0, Lcom/osp/app/signin/j;->c:Ljava/util/ArrayList;

    invoke-virtual {v0, v5}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/osp/app/signin/i;

    iget v0, v0, Lcom/osp/app/signin/i;->c:I

    if-ne v0, v10, :cond_7

    move v1, v4

    .line 927
    :goto_4
    iget-object v0, v2, Lcom/osp/app/signin/k;->b:Landroid/widget/TextView;

    invoke-virtual {v0}, Landroid/widget/TextView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup$MarginLayoutParams;

    .line 928
    if-eqz v3, :cond_a

    .line 931
    if-nez v6, :cond_8

    .line 933
    iget-object v1, v2, Lcom/osp/app/signin/k;->b:Landroid/widget/TextView;

    invoke-virtual {v1, v8}, Landroid/widget/TextView;->setVisibility(I)V

    .line 956
    :goto_5
    iget-object v1, v2, Lcom/osp/app/signin/k;->b:Landroid/widget/TextView;

    invoke-static {v1, v0}, Lcom/msc/sa/c/d;->a(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    .line 958
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    if-lt v0, v9, :cond_0

    .line 960
    iget-object v0, v2, Lcom/osp/app/signin/k;->b:Landroid/widget/TextView;

    const-string v1, "sec-roboto-light"

    invoke-static {v1, v4}, Landroid/graphics/Typeface;->create(Ljava/lang/String;I)Landroid/graphics/Typeface;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setTypeface(Landroid/graphics/Typeface;)V

    goto/16 :goto_3

    :cond_7
    move v1, v5

    .line 925
    goto :goto_4

    .line 936
    :cond_8
    if-eqz v1, :cond_9

    .line 938
    iput v5, v0, Landroid/view/ViewGroup$MarginLayoutParams;->topMargin:I

    .line 943
    :goto_6
    iget-object v1, p0, Lcom/osp/app/signin/j;->a:Lcom/osp/app/signin/h;

    invoke-static {v1}, Lcom/osp/app/signin/h;->a(Lcom/osp/app/signin/h;)Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v3, 0x7f080068

    invoke-virtual {v1, v3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    iput v1, v0, Landroid/view/ViewGroup$MarginLayoutParams;->bottomMargin:I

    goto :goto_5

    .line 941
    :cond_9
    iget-object v1, p0, Lcom/osp/app/signin/j;->a:Lcom/osp/app/signin/h;

    invoke-static {v1}, Lcom/osp/app/signin/h;->a(Lcom/osp/app/signin/h;)Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v3, 0x7f080067

    invoke-virtual {v1, v3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    iput v1, v0, Landroid/view/ViewGroup$MarginLayoutParams;->topMargin:I

    goto :goto_6

    .line 947
    :cond_a
    if-eqz v1, :cond_b

    .line 949
    iput v5, v0, Landroid/view/ViewGroup$MarginLayoutParams;->topMargin:I

    .line 954
    :goto_7
    iget-object v1, p0, Lcom/osp/app/signin/j;->a:Lcom/osp/app/signin/h;

    invoke-static {v1}, Lcom/osp/app/signin/h;->a(Lcom/osp/app/signin/h;)Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v3, 0x7f08005f

    invoke-virtual {v1, v3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    iput v1, v0, Landroid/view/ViewGroup$MarginLayoutParams;->bottomMargin:I

    goto :goto_5

    .line 952
    :cond_b
    iget-object v1, p0, Lcom/osp/app/signin/j;->a:Lcom/osp/app/signin/h;

    invoke-static {v1}, Lcom/osp/app/signin/h;->a(Lcom/osp/app/signin/h;)Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v3, 0x7f08005e

    invoke-virtual {v1, v3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    iput v1, v0, Landroid/view/ViewGroup$MarginLayoutParams;->topMargin:I

    goto :goto_7

    .line 963
    :cond_c
    iget v0, v1, Lcom/osp/app/signin/i;->c:I

    and-int/lit16 v0, v0, 0x4000

    const/16 v6, 0x4000

    if-ne v0, v6, :cond_f

    .line 965
    iget-object v0, v2, Lcom/osp/app/signin/k;->c:Landroid/widget/TextView;

    invoke-virtual {v0, v5}, Landroid/widget/TextView;->setVisibility(I)V

    .line 968
    iget-object v0, v2, Lcom/osp/app/signin/k;->c:Landroid/widget/TextView;

    new-instance v6, Ljava/lang/StringBuilder;

    const-string v7, "* "

    invoke-direct {v6, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v7, p0, Lcom/osp/app/signin/j;->a:Lcom/osp/app/signin/h;

    invoke-static {v7}, Lcom/osp/app/signin/h;->a(Lcom/osp/app/signin/h;)Landroid/content/Context;

    move-result-object v7

    iget v8, v1, Lcom/osp/app/signin/i;->b:I

    invoke-virtual {v7, v8}, Landroid/content/Context;->getText(I)Ljava/lang/CharSequence;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v0, v6}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 969
    iget-object v0, v2, Lcom/osp/app/signin/k;->c:Landroid/widget/TextView;

    iget-object v6, p0, Lcom/osp/app/signin/j;->a:Lcom/osp/app/signin/h;

    invoke-static {v6}, Lcom/osp/app/signin/h;->a(Lcom/osp/app/signin/h;)Landroid/content/Context;

    move-result-object v6

    iget v1, v1, Lcom/osp/app/signin/i;->b:I

    invoke-virtual {v6, v1}, Landroid/content/Context;->getText(I)Ljava/lang/CharSequence;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 970
    iget-object v1, v2, Lcom/osp/app/signin/k;->c:Landroid/widget/TextView;

    iget-object v0, p0, Lcom/osp/app/signin/j;->a:Lcom/osp/app/signin/h;

    invoke-static {v0}, Lcom/osp/app/signin/h;->a(Lcom/osp/app/signin/h;)Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v6

    iget-object v0, p0, Lcom/osp/app/signin/j;->a:Lcom/osp/app/signin/h;

    invoke-static {v0}, Lcom/osp/app/signin/h;->a(Lcom/osp/app/signin/h;)Landroid/content/Context;

    move-result-object v0

    check-cast v0, Lcom/osp/app/util/BaseActivity;

    invoke-static {v0}, Lcom/osp/app/util/an;->a(Lcom/osp/app/util/BaseActivity;)Lcom/osp/app/util/an;

    const v0, 0x7f07001c

    invoke-static {v0}, Lcom/osp/app/util/an;->a(I)I

    move-result v0

    invoke-virtual {v6, v0}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setTextColor(I)V

    .line 973
    iget-object v0, v2, Lcom/osp/app/signin/k;->b:Landroid/widget/TextView;

    invoke-virtual {v0}, Landroid/widget/TextView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup$MarginLayoutParams;

    .line 974
    if-eqz v3, :cond_d

    .line 976
    iget-object v1, p0, Lcom/osp/app/signin/j;->a:Lcom/osp/app/signin/h;

    invoke-static {v1}, Lcom/osp/app/signin/h;->a(Lcom/osp/app/signin/h;)Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v3, 0x7f080069

    invoke-virtual {v1, v3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    iput v1, v0, Landroid/view/ViewGroup$MarginLayoutParams;->topMargin:I

    .line 977
    iget-object v1, p0, Lcom/osp/app/signin/j;->a:Lcom/osp/app/signin/h;

    invoke-static {v1}, Lcom/osp/app/signin/h;->a(Lcom/osp/app/signin/h;)Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v3, 0x7f08006a

    invoke-virtual {v1, v3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    iput v1, v0, Landroid/view/ViewGroup$MarginLayoutParams;->bottomMargin:I

    .line 983
    :goto_8
    iget-object v1, v2, Lcom/osp/app/signin/k;->c:Landroid/widget/TextView;

    invoke-static {v1, v0}, Lcom/msc/sa/c/d;->a(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    .line 985
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    if-lt v0, v9, :cond_0

    .line 987
    iget-object v0, p0, Lcom/osp/app/signin/j;->a:Lcom/osp/app/signin/h;

    invoke-static {v0}, Lcom/osp/app/signin/h;->a(Lcom/osp/app/signin/h;)Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/osp/app/util/r;->g(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_e

    .line 989
    iget-object v0, v2, Lcom/osp/app/signin/k;->c:Landroid/widget/TextView;

    const-string v1, "sec-roboto-light"

    invoke-static {v1, v4}, Landroid/graphics/Typeface;->create(Ljava/lang/String;I)Landroid/graphics/Typeface;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setTypeface(Landroid/graphics/Typeface;)V

    goto/16 :goto_3

    .line 980
    :cond_d
    iget-object v1, p0, Lcom/osp/app/signin/j;->a:Lcom/osp/app/signin/h;

    invoke-static {v1}, Lcom/osp/app/signin/h;->a(Lcom/osp/app/signin/h;)Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v3, 0x7f080061

    invoke-virtual {v1, v3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    iput v1, v0, Landroid/view/ViewGroup$MarginLayoutParams;->topMargin:I

    .line 981
    iget-object v1, p0, Lcom/osp/app/signin/j;->a:Lcom/osp/app/signin/h;

    invoke-static {v1}, Lcom/osp/app/signin/h;->a(Lcom/osp/app/signin/h;)Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v3, 0x7f080062

    invoke-virtual {v1, v3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    iput v1, v0, Landroid/view/ViewGroup$MarginLayoutParams;->bottomMargin:I

    goto :goto_8

    .line 992
    :cond_e
    iget-object v0, v2, Lcom/osp/app/signin/k;->c:Landroid/widget/TextView;

    const-string v1, "sec-roboto-light"

    invoke-static {v1, v5}, Landroid/graphics/Typeface;->create(Ljava/lang/String;I)Landroid/graphics/Typeface;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setTypeface(Landroid/graphics/Typeface;)V

    goto/16 :goto_3

    .line 998
    :cond_f
    iget-object v0, v2, Lcom/osp/app/signin/k;->d:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v5}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 1000
    iget-object v0, v1, Lcom/osp/app/signin/i;->f:Landroid/graphics/drawable/Drawable;

    if-eqz v0, :cond_11

    .line 1003
    iget-object v0, v2, Lcom/osp/app/signin/k;->f:Landroid/widget/ImageView;

    iget-object v4, v1, Lcom/osp/app/signin/i;->f:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v0, v4}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 1026
    :goto_9
    iget-object v0, v2, Lcom/osp/app/signin/k;->e:Landroid/widget/TextView;

    iget v4, v1, Lcom/osp/app/signin/i;->b:I

    invoke-virtual {v0, v4}, Landroid/widget/TextView;->setText(I)V

    .line 1029
    iget-object v0, v2, Lcom/osp/app/signin/k;->d:Landroid/widget/LinearLayout;

    invoke-virtual {v0}, Landroid/widget/LinearLayout;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout$LayoutParams;

    .line 1030
    iget v4, v1, Lcom/osp/app/signin/i;->c:I

    const/high16 v6, 0x300000

    and-int/2addr v4, v6

    const/high16 v6, 0x200000

    if-ne v4, v6, :cond_13

    .line 1032
    iget-object v4, v2, Lcom/osp/app/signin/k;->e:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/osp/app/signin/j;->a:Lcom/osp/app/signin/h;

    invoke-static {v1}, Lcom/osp/app/signin/h;->a(Lcom/osp/app/signin/h;)Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v6

    iget-object v1, p0, Lcom/osp/app/signin/j;->a:Lcom/osp/app/signin/h;

    invoke-static {v1}, Lcom/osp/app/signin/h;->a(Lcom/osp/app/signin/h;)Landroid/content/Context;

    move-result-object v1

    check-cast v1, Lcom/osp/app/util/BaseActivity;

    invoke-static {v1}, Lcom/osp/app/util/an;->a(Lcom/osp/app/util/BaseActivity;)Lcom/osp/app/util/an;

    const v1, 0x7f07000c

    invoke-static {v1}, Lcom/osp/app/util/an;->a(I)I

    move-result v1

    invoke-virtual {v6, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    invoke-virtual {v4, v1}, Landroid/widget/TextView;->setTextColor(I)V

    .line 1035
    if-eqz v3, :cond_12

    .line 1037
    iget-object v1, p0, Lcom/osp/app/signin/j;->a:Lcom/osp/app/signin/h;

    invoke-static {v1}, Lcom/osp/app/signin/h;->a(Lcom/osp/app/signin/h;)Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v3, 0x7f080065

    invoke-virtual {v1, v3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    iput v1, v0, Landroid/widget/LinearLayout$LayoutParams;->topMargin:I

    .line 1038
    iget-object v1, p0, Lcom/osp/app/signin/j;->a:Lcom/osp/app/signin/h;

    invoke-static {v1}, Lcom/osp/app/signin/h;->a(Lcom/osp/app/signin/h;)Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v3, 0x7f080066

    invoke-virtual {v1, v3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    iput v1, v0, Landroid/widget/LinearLayout$LayoutParams;->bottomMargin:I

    .line 1064
    :cond_10
    :goto_a
    iget-object v1, v2, Lcom/osp/app/signin/k;->d:Landroid/widget/LinearLayout;

    invoke-static {v1, v0}, Lcom/msc/sa/c/d;->a(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    .line 1066
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    if-lt v0, v9, :cond_0

    .line 1068
    iget-object v0, v2, Lcom/osp/app/signin/k;->e:Landroid/widget/TextView;

    const-string v1, "sec-roboto-light"

    invoke-static {v1, v5}, Landroid/graphics/Typeface;->create(Ljava/lang/String;I)Landroid/graphics/Typeface;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setTypeface(Landroid/graphics/Typeface;)V

    goto/16 :goto_3

    .line 1007
    :cond_11
    iget-object v0, v2, Lcom/osp/app/signin/k;->f:Landroid/widget/ImageView;

    iget v4, v1, Lcom/osp/app/signin/i;->a:I

    invoke-virtual {v0, v4}, Landroid/widget/ImageView;->setImageResource(I)V

    goto/16 :goto_9

    .line 1041
    :cond_12
    iget-object v1, p0, Lcom/osp/app/signin/j;->a:Lcom/osp/app/signin/h;

    invoke-static {v1}, Lcom/osp/app/signin/h;->a(Lcom/osp/app/signin/h;)Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v3, 0x7f080057

    invoke-virtual {v1, v3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    iput v1, v0, Landroid/widget/LinearLayout$LayoutParams;->topMargin:I

    .line 1042
    iget-object v1, p0, Lcom/osp/app/signin/j;->a:Lcom/osp/app/signin/h;

    invoke-static {v1}, Lcom/osp/app/signin/h;->a(Lcom/osp/app/signin/h;)Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v3, 0x7f080058

    invoke-virtual {v1, v3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    iput v1, v0, Landroid/widget/LinearLayout$LayoutParams;->bottomMargin:I

    goto :goto_a

    .line 1044
    :cond_13
    iget v1, v1, Lcom/osp/app/signin/i;->c:I

    const/high16 v3, 0x300000

    and-int/2addr v1, v3

    const/high16 v3, 0x100000

    if-ne v1, v3, :cond_10

    .line 1046
    iget-object v3, v2, Lcom/osp/app/signin/k;->e:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/osp/app/signin/j;->a:Lcom/osp/app/signin/h;

    invoke-static {v1}, Lcom/osp/app/signin/h;->a(Lcom/osp/app/signin/h;)Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    iget-object v1, p0, Lcom/osp/app/signin/j;->a:Lcom/osp/app/signin/h;

    invoke-static {v1}, Lcom/osp/app/signin/h;->a(Lcom/osp/app/signin/h;)Landroid/content/Context;

    move-result-object v1

    check-cast v1, Lcom/osp/app/util/BaseActivity;

    invoke-static {v1}, Lcom/osp/app/util/an;->a(Lcom/osp/app/util/BaseActivity;)Lcom/osp/app/util/an;

    const v1, 0x7f070004

    invoke-static {v1}, Lcom/osp/app/util/an;->a(I)I

    move-result v1

    invoke-virtual {v4, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    invoke-virtual {v3, v1}, Landroid/widget/TextView;->setTextColor(I)V

    .line 1050
    iget-object v1, p0, Lcom/osp/app/signin/j;->a:Lcom/osp/app/signin/h;

    invoke-static {v1}, Lcom/osp/app/signin/h;->a(Lcom/osp/app/signin/h;)Landroid/content/Context;

    move-result-object v1

    check-cast v1, Lcom/osp/app/util/BaseActivity;

    invoke-static {v1}, Lcom/osp/app/util/an;->a(Lcom/osp/app/util/BaseActivity;)Lcom/osp/app/util/an;

    const v1, 0x7f080056

    invoke-static {v1}, Lcom/osp/app/util/an;->b(I)I

    move-result v1

    .line 1052
    iget-object v3, v2, Lcom/osp/app/signin/k;->e:Landroid/widget/TextView;

    iget-object v4, p0, Lcom/osp/app/signin/j;->a:Lcom/osp/app/signin/h;

    invoke-static {v4}, Lcom/osp/app/signin/h;->a(Lcom/osp/app/signin/h;)Landroid/content/Context;

    move-result-object v4

    invoke-virtual {v4}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    invoke-virtual {v4, v1}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v1

    invoke-virtual {v3, v5, v1}, Landroid/widget/TextView;->setTextSize(IF)V

    .line 1054
    iget-object v1, p0, Lcom/osp/app/signin/j;->a:Lcom/osp/app/signin/h;

    invoke-static {v1}, Lcom/osp/app/signin/h;->a(Lcom/osp/app/signin/h;)Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v3, 0x7f080050

    invoke-virtual {v1, v3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    iput v1, v0, Landroid/widget/LinearLayout$LayoutParams;->topMargin:I

    .line 1056
    invoke-virtual {p0}, Lcom/osp/app/signin/j;->getCount()I

    move-result v1

    add-int/lit8 v1, v1, -0x1

    if-ne p1, v1, :cond_14

    .line 1058
    iget-object v1, p0, Lcom/osp/app/signin/j;->a:Lcom/osp/app/signin/h;

    invoke-static {v1}, Lcom/osp/app/signin/h;->a(Lcom/osp/app/signin/h;)Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v3, 0x7f080052

    invoke-virtual {v1, v3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    iput v1, v0, Landroid/widget/LinearLayout$LayoutParams;->bottomMargin:I

    goto/16 :goto_a

    .line 1061
    :cond_14
    iget-object v1, p0, Lcom/osp/app/signin/j;->a:Lcom/osp/app/signin/h;

    invoke-static {v1}, Lcom/osp/app/signin/h;->a(Lcom/osp/app/signin/h;)Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v3, 0x7f080051

    invoke-virtual {v1, v3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    iput v1, v0, Landroid/widget/LinearLayout$LayoutParams;->bottomMargin:I

    goto/16 :goto_a
.end method

.method public final isEnabled(I)Z
    .locals 1

    .prologue
    .line 797
    const/4 v0, 0x0

    return v0
.end method
