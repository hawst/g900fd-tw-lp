.class public final Lcom/osp/app/signin/kd;
.super Ljava/lang/Object;
.source "SignUpFieldInfo.java"


# instance fields
.field a:Z

.field b:Z

.field c:Z

.field d:Z

.field e:Z

.field f:Z

.field g:Z

.field h:Z

.field i:Z

.field j:Z

.field k:Z

.field l:Z

.field m:Z

.field n:Z

.field o:Z

.field p:Z

.field q:Z

.field r:Z

.field s:Ljava/util/ArrayList;

.field t:Ljava/util/ArrayList;

.field u:Ljava/util/ArrayList;

.field v:Ljava/util/ArrayList;

.field private w:Z

.field private final x:[Ljava/lang/String;

.field private final y:Lcom/osp/app/signin/ke;


# direct methods
.method public constructor <init>()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 234
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 162
    iput-boolean v3, p0, Lcom/osp/app/signin/kd;->w:Z

    .line 175
    const/16 v0, 0x10

    new-array v0, v0, [Ljava/lang/String;

    const-string v1, "birthDate"

    aput-object v1, v0, v3

    const/4 v1, 0x1

    const-string v2, "countryCode"

    aput-object v2, v0, v1

    const/4 v1, 0x2

    const-string v2, "prefixName"

    aput-object v2, v0, v1

    const/4 v1, 0x3

    const-string v2, "givenName"

    aput-object v2, v0, v1

    const/4 v1, 0x4

    const-string v2, "familyName"

    aput-object v2, v0, v1

    const/4 v1, 0x5

    const-string v2, "streetText"

    aput-object v2, v0, v1

    const/4 v1, 0x6

    const-string v2, "extendedText"

    aput-object v2, v0, v1

    const/4 v1, 0x7

    const-string v2, "postOfficeBoxNumberText"

    aput-object v2, v0, v1

    const/16 v1, 0x8

    const-string v2, "postalCodeText"

    aput-object v2, v0, v1

    const/16 v1, 0x9

    const-string v2, "receiveSMSPhoneNumberText"

    aput-object v2, v0, v1

    const/16 v1, 0xa

    const-string v2, "userDisplayName"

    aput-object v2, v0, v1

    const/16 v1, 0xb

    const-string v2, "localityText"

    aput-object v2, v0, v1

    const/16 v1, 0xc

    const-string v2, "regionText"

    aput-object v2, v0, v1

    const/16 v1, 0xd

    const-string v2, "genderTypeCode"

    aput-object v2, v0, v1

    const/16 v1, 0xe

    const-string v2, "relationshipStatusCode"

    aput-object v2, v0, v1

    const/16 v1, 0xf

    const-string v2, "emailReceiveYNFlag"

    aput-object v2, v0, v1

    iput-object v0, p0, Lcom/osp/app/signin/kd;->x:[Ljava/lang/String;

    .line 236
    iput-boolean v3, p0, Lcom/osp/app/signin/kd;->a:Z

    .line 237
    iput-boolean v3, p0, Lcom/osp/app/signin/kd;->b:Z

    .line 239
    iput-boolean v3, p0, Lcom/osp/app/signin/kd;->c:Z

    .line 240
    iput-boolean v3, p0, Lcom/osp/app/signin/kd;->d:Z

    .line 241
    iput-boolean v3, p0, Lcom/osp/app/signin/kd;->e:Z

    .line 242
    iput-boolean v3, p0, Lcom/osp/app/signin/kd;->f:Z

    .line 244
    iput-boolean v3, p0, Lcom/osp/app/signin/kd;->j:Z

    .line 245
    iput-boolean v3, p0, Lcom/osp/app/signin/kd;->k:Z

    .line 246
    iput-boolean v3, p0, Lcom/osp/app/signin/kd;->l:Z

    .line 247
    iput-boolean v3, p0, Lcom/osp/app/signin/kd;->m:Z

    .line 249
    iput-boolean v3, p0, Lcom/osp/app/signin/kd;->g:Z

    .line 250
    iput-boolean v3, p0, Lcom/osp/app/signin/kd;->h:Z

    .line 251
    iput-boolean v3, p0, Lcom/osp/app/signin/kd;->i:Z

    .line 253
    iput-boolean v3, p0, Lcom/osp/app/signin/kd;->n:Z

    .line 254
    iput-boolean v3, p0, Lcom/osp/app/signin/kd;->o:Z

    .line 255
    iput-boolean v3, p0, Lcom/osp/app/signin/kd;->p:Z

    .line 257
    iput-boolean v3, p0, Lcom/osp/app/signin/kd;->q:Z

    .line 258
    iput-boolean v3, p0, Lcom/osp/app/signin/kd;->r:Z

    .line 260
    new-instance v0, Lcom/osp/app/signin/ke;

    invoke-direct {v0, v3}, Lcom/osp/app/signin/ke;-><init>(B)V

    iput-object v0, p0, Lcom/osp/app/signin/kd;->y:Lcom/osp/app/signin/ke;

    .line 262
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/osp/app/signin/kd;->t:Ljava/util/ArrayList;

    .line 263
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/osp/app/signin/kd;->u:Ljava/util/ArrayList;

    .line 264
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/osp/app/signin/kd;->v:Ljava/util/ArrayList;

    .line 265
    return-void
.end method

.method private Y()V
    .locals 4

    .prologue
    .line 815
    iget-object v0, p0, Lcom/osp/app/signin/kd;->x:[Ljava/lang/String;

    array-length v1, v0

    .line 817
    const/4 v0, 0x0

    :goto_0
    if-ge v0, v1, :cond_1

    .line 819
    iget-object v2, p0, Lcom/osp/app/signin/kd;->t:Ljava/util/ArrayList;

    iget-object v3, p0, Lcom/osp/app/signin/kd;->x:[Ljava/lang/String;

    aget-object v3, v3, v0

    invoke-virtual {v2, v3}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 821
    iget-object v2, p0, Lcom/osp/app/signin/kd;->s:Ljava/util/ArrayList;

    iget-object v3, p0, Lcom/osp/app/signin/kd;->x:[Ljava/lang/String;

    aget-object v3, v3, v0

    invoke-virtual {v2, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 817
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 825
    :cond_1
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/osp/app/signin/kd;->w:Z

    .line 826
    return-void
.end method

.method public static a(Landroid/os/Bundle;)Lcom/osp/app/signin/kd;
    .locals 2

    .prologue
    .line 739
    new-instance v0, Lcom/osp/app/signin/kd;

    invoke-direct {v0}, Lcom/osp/app/signin/kd;-><init>()V

    .line 740
    if-eqz p0, :cond_0

    .line 742
    const-string v1, "countryCode"

    invoke-virtual {p0, v1}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v1

    iput-boolean v1, v0, Lcom/osp/app/signin/kd;->a:Z

    .line 743
    const-string v1, "birthDate"

    invoke-virtual {p0, v1}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v1

    iput-boolean v1, v0, Lcom/osp/app/signin/kd;->b:Z

    .line 745
    const-string v1, "prefixName"

    invoke-virtual {p0, v1}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v1

    iput-boolean v1, v0, Lcom/osp/app/signin/kd;->c:Z

    .line 746
    const-string v1, "givenName"

    invoke-virtual {p0, v1}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v1

    iput-boolean v1, v0, Lcom/osp/app/signin/kd;->d:Z

    .line 747
    const-string v1, "familyName"

    invoke-virtual {p0, v1}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v1

    iput-boolean v1, v0, Lcom/osp/app/signin/kd;->e:Z

    .line 748
    const-string v1, "localityText"

    invoke-virtual {p0, v1}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v1

    iput-boolean v1, v0, Lcom/osp/app/signin/kd;->f:Z

    .line 750
    const-string v1, "streetText"

    invoke-virtual {p0, v1}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v1

    iput-boolean v1, v0, Lcom/osp/app/signin/kd;->j:Z

    .line 751
    const-string v1, "extendedText"

    invoke-virtual {p0, v1}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v1

    iput-boolean v1, v0, Lcom/osp/app/signin/kd;->k:Z

    .line 752
    const-string v1, "postOfficeBoxNumberText"

    invoke-virtual {p0, v1}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v1

    iput-boolean v1, v0, Lcom/osp/app/signin/kd;->l:Z

    .line 753
    const-string v1, "regionText"

    invoke-virtual {p0, v1}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v1

    iput-boolean v1, v0, Lcom/osp/app/signin/kd;->m:Z

    .line 755
    const-string v1, "postalCodeText"

    invoke-virtual {p0, v1}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v1

    iput-boolean v1, v0, Lcom/osp/app/signin/kd;->g:Z

    .line 756
    const-string v1, "receiveSMSPhoneNumberText"

    invoke-virtual {p0, v1}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v1

    iput-boolean v1, v0, Lcom/osp/app/signin/kd;->h:Z

    .line 757
    const-string v1, "emailReceiveYNFlag"

    invoke-virtual {p0, v1}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v1

    iput-boolean v1, v0, Lcom/osp/app/signin/kd;->i:Z

    .line 759
    const-string v1, "genderTypeCode"

    invoke-virtual {p0, v1}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v1

    iput-boolean v1, v0, Lcom/osp/app/signin/kd;->n:Z

    .line 760
    const-string v1, "userDisplayName"

    invoke-virtual {p0, v1}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v1

    iput-boolean v1, v0, Lcom/osp/app/signin/kd;->o:Z

    .line 761
    const-string v1, "relationshipStatusCode"

    invoke-virtual {p0, v1}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v1

    iput-boolean v1, v0, Lcom/osp/app/signin/kd;->p:Z

    .line 763
    const-string v1, "LIST_ORDER"

    invoke-virtual {p0, v1}, Landroid/os/Bundle;->getStringArrayList(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v1

    iput-object v1, v0, Lcom/osp/app/signin/kd;->t:Ljava/util/ArrayList;

    .line 764
    const-string v1, "PREFIX_NAME_ELEMENT_LIST"

    invoke-virtual {p0, v1}, Landroid/os/Bundle;->getStringArrayList(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v1

    iput-object v1, v0, Lcom/osp/app/signin/kd;->u:Ljava/util/ArrayList;

    .line 765
    const-string v1, "PREFIX_NAME_TITLE_ELEMENT_LIST"

    invoke-virtual {p0, v1}, Landroid/os/Bundle;->getStringArrayList(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v1

    iput-object v1, v0, Lcom/osp/app/signin/kd;->v:Ljava/util/ArrayList;

    .line 767
    const-string v1, "securityAnswer"

    invoke-virtual {p0, v1}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v1

    iput-boolean v1, v0, Lcom/osp/app/signin/kd;->q:Z

    .line 768
    const-string v1, "securityQuestion"

    invoke-virtual {p0, v1}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v1

    iput-boolean v1, v0, Lcom/osp/app/signin/kd;->r:Z

    .line 770
    :cond_0
    return-object v0
.end method


# virtual methods
.method public final A()V
    .locals 1

    .prologue
    .line 613
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/osp/app/signin/kd;->f:Z

    .line 614
    return-void
.end method

.method public final B()V
    .locals 2

    .prologue
    .line 617
    iget-object v0, p0, Lcom/osp/app/signin/kd;->y:Lcom/osp/app/signin/ke;

    const/4 v1, 0x1

    iput-boolean v1, v0, Lcom/osp/app/signin/ke;->e:Z

    .line 618
    return-void
.end method

.method public final C()V
    .locals 1

    .prologue
    .line 621
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/osp/app/signin/kd;->j:Z

    .line 622
    return-void
.end method

.method public final D()V
    .locals 2

    .prologue
    .line 625
    iget-object v0, p0, Lcom/osp/app/signin/kd;->y:Lcom/osp/app/signin/ke;

    const/4 v1, 0x1

    iput-boolean v1, v0, Lcom/osp/app/signin/ke;->i:Z

    .line 626
    return-void
.end method

.method public final E()V
    .locals 1

    .prologue
    .line 629
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/osp/app/signin/kd;->k:Z

    .line 630
    return-void
.end method

.method public final F()V
    .locals 2

    .prologue
    .line 633
    iget-object v0, p0, Lcom/osp/app/signin/kd;->y:Lcom/osp/app/signin/ke;

    const/4 v1, 0x1

    iput-boolean v1, v0, Lcom/osp/app/signin/ke;->j:Z

    .line 634
    return-void
.end method

.method public final G()V
    .locals 1

    .prologue
    .line 637
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/osp/app/signin/kd;->l:Z

    .line 638
    return-void
.end method

.method public final H()V
    .locals 2

    .prologue
    .line 641
    iget-object v0, p0, Lcom/osp/app/signin/kd;->y:Lcom/osp/app/signin/ke;

    const/4 v1, 0x1

    iput-boolean v1, v0, Lcom/osp/app/signin/ke;->k:Z

    .line 642
    return-void
.end method

.method public final I()V
    .locals 1

    .prologue
    .line 645
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/osp/app/signin/kd;->m:Z

    .line 646
    return-void
.end method

.method public final J()V
    .locals 2

    .prologue
    .line 649
    iget-object v0, p0, Lcom/osp/app/signin/kd;->y:Lcom/osp/app/signin/ke;

    const/4 v1, 0x1

    iput-boolean v1, v0, Lcom/osp/app/signin/ke;->l:Z

    .line 650
    return-void
.end method

.method public final K()V
    .locals 1

    .prologue
    .line 653
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/osp/app/signin/kd;->g:Z

    .line 654
    return-void
.end method

.method public final L()V
    .locals 2

    .prologue
    .line 657
    iget-object v0, p0, Lcom/osp/app/signin/kd;->y:Lcom/osp/app/signin/ke;

    const/4 v1, 0x1

    iput-boolean v1, v0, Lcom/osp/app/signin/ke;->f:Z

    .line 658
    return-void
.end method

.method public final M()V
    .locals 1

    .prologue
    .line 661
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/osp/app/signin/kd;->h:Z

    .line 662
    return-void
.end method

.method public final N()V
    .locals 2

    .prologue
    .line 665
    iget-object v0, p0, Lcom/osp/app/signin/kd;->y:Lcom/osp/app/signin/ke;

    const/4 v1, 0x1

    iput-boolean v1, v0, Lcom/osp/app/signin/ke;->g:Z

    .line 666
    return-void
.end method

.method public final O()V
    .locals 1

    .prologue
    .line 669
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/osp/app/signin/kd;->i:Z

    .line 670
    return-void
.end method

.method public final P()V
    .locals 1

    .prologue
    .line 673
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/osp/app/signin/kd;->n:Z

    .line 674
    return-void
.end method

.method public final Q()V
    .locals 2

    .prologue
    .line 677
    iget-object v0, p0, Lcom/osp/app/signin/kd;->y:Lcom/osp/app/signin/ke;

    const/4 v1, 0x1

    iput-boolean v1, v0, Lcom/osp/app/signin/ke;->m:Z

    .line 678
    return-void
.end method

.method public final R()V
    .locals 1

    .prologue
    .line 681
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/osp/app/signin/kd;->o:Z

    .line 682
    return-void
.end method

.method public final S()V
    .locals 2

    .prologue
    .line 685
    iget-object v0, p0, Lcom/osp/app/signin/kd;->y:Lcom/osp/app/signin/ke;

    const/4 v1, 0x1

    iput-boolean v1, v0, Lcom/osp/app/signin/ke;->n:Z

    .line 686
    return-void
.end method

.method public final T()V
    .locals 1

    .prologue
    .line 689
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/osp/app/signin/kd;->p:Z

    .line 690
    return-void
.end method

.method public final U()V
    .locals 2

    .prologue
    .line 693
    iget-object v0, p0, Lcom/osp/app/signin/kd;->y:Lcom/osp/app/signin/ke;

    const/4 v1, 0x1

    iput-boolean v1, v0, Lcom/osp/app/signin/ke;->o:Z

    .line 694
    return-void
.end method

.method public final V()V
    .locals 2

    .prologue
    .line 711
    iget-object v0, p0, Lcom/osp/app/signin/kd;->y:Lcom/osp/app/signin/ke;

    const/4 v1, 0x1

    iput-boolean v1, v0, Lcom/osp/app/signin/ke;->p:Z

    .line 712
    return-void
.end method

.method public final W()V
    .locals 2

    .prologue
    .line 729
    iget-object v0, p0, Lcom/osp/app/signin/kd;->y:Lcom/osp/app/signin/ke;

    const/4 v1, 0x1

    iput-boolean v1, v0, Lcom/osp/app/signin/ke;->q:Z

    .line 730
    return-void
.end method

.method public final X()Landroid/os/Bundle;
    .locals 3

    .prologue
    .line 779
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 781
    const-string v1, "countryCode"

    iget-boolean v2, p0, Lcom/osp/app/signin/kd;->a:Z

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 782
    const-string v1, "birthDate"

    iget-boolean v2, p0, Lcom/osp/app/signin/kd;->b:Z

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 784
    const-string v1, "prefixName"

    iget-boolean v2, p0, Lcom/osp/app/signin/kd;->c:Z

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 785
    const-string v1, "givenName"

    iget-boolean v2, p0, Lcom/osp/app/signin/kd;->d:Z

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 786
    const-string v1, "familyName"

    iget-boolean v2, p0, Lcom/osp/app/signin/kd;->e:Z

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 787
    const-string v1, "localityText"

    iget-boolean v2, p0, Lcom/osp/app/signin/kd;->f:Z

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 789
    const-string v1, "streetText"

    iget-boolean v2, p0, Lcom/osp/app/signin/kd;->j:Z

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 790
    const-string v1, "extendedText"

    iget-boolean v2, p0, Lcom/osp/app/signin/kd;->k:Z

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 791
    const-string v1, "postOfficeBoxNumberText"

    iget-boolean v2, p0, Lcom/osp/app/signin/kd;->l:Z

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 792
    const-string v1, "regionText"

    iget-boolean v2, p0, Lcom/osp/app/signin/kd;->m:Z

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 794
    const-string v1, "postalCodeText"

    iget-boolean v2, p0, Lcom/osp/app/signin/kd;->g:Z

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 795
    const-string v1, "receiveSMSPhoneNumberText"

    iget-boolean v2, p0, Lcom/osp/app/signin/kd;->h:Z

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 796
    const-string v1, "emailReceiveYNFlag"

    iget-boolean v2, p0, Lcom/osp/app/signin/kd;->i:Z

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 798
    const-string v1, "genderTypeCode"

    iget-boolean v2, p0, Lcom/osp/app/signin/kd;->n:Z

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 799
    const-string v1, "userDisplayName"

    iget-boolean v2, p0, Lcom/osp/app/signin/kd;->o:Z

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 800
    const-string v1, "relationshipStatusCode"

    iget-boolean v2, p0, Lcom/osp/app/signin/kd;->p:Z

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 802
    const-string v1, "LIST_ORDER"

    iget-object v2, p0, Lcom/osp/app/signin/kd;->t:Ljava/util/ArrayList;

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putStringArrayList(Ljava/lang/String;Ljava/util/ArrayList;)V

    .line 803
    const-string v1, "PREFIX_NAME_ELEMENT_LIST"

    iget-object v2, p0, Lcom/osp/app/signin/kd;->u:Ljava/util/ArrayList;

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putStringArrayList(Ljava/lang/String;Ljava/util/ArrayList;)V

    .line 804
    const-string v1, "PREFIX_NAME_TITLE_ELEMENT_LIST"

    iget-object v2, p0, Lcom/osp/app/signin/kd;->v:Ljava/util/ArrayList;

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putStringArrayList(Ljava/lang/String;Ljava/util/ArrayList;)V

    .line 806
    const-string v1, "securityAnswer"

    iget-boolean v2, p0, Lcom/osp/app/signin/kd;->q:Z

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 807
    const-string v1, "securityQuestion"

    iget-boolean v2, p0, Lcom/osp/app/signin/kd;->r:Z

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 808
    return-object v0
.end method

.method public final a()Ljava/util/ArrayList;
    .locals 1

    .prologue
    .line 268
    iget-boolean v0, p0, Lcom/osp/app/signin/kd;->w:Z

    if-nez v0, :cond_0

    .line 270
    iget-object v0, p0, Lcom/osp/app/signin/kd;->s:Ljava/util/ArrayList;

    if-nez v0, :cond_1

    .line 272
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/osp/app/signin/kd;->s:Ljava/util/ArrayList;

    .line 277
    :goto_0
    invoke-direct {p0}, Lcom/osp/app/signin/kd;->Y()V

    .line 280
    :cond_0
    iget-object v0, p0, Lcom/osp/app/signin/kd;->s:Ljava/util/ArrayList;

    return-object v0

    .line 275
    :cond_1
    iget-object v0, p0, Lcom/osp/app/signin/kd;->s:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    goto :goto_0
.end method

.method public final a(Landroid/content/Context;Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 323
    invoke-static {}, Lcom/osp/app/signin/SamsungService;->p()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 326
    invoke-virtual {p0}, Lcom/osp/app/signin/kd;->b()I

    move-result v0

    if-lez v0, :cond_1

    .line 328
    const/4 v0, 0x1

    invoke-static {p1, p2, v0}, Lcom/msc/openprovider/b;->a(Landroid/content/Context;Ljava/lang/String;Z)V

    .line 336
    :cond_0
    :goto_0
    return-void

    .line 331
    :cond_1
    const/4 v0, 0x0

    invoke-static {p1, p2, v0}, Lcom/msc/openprovider/b;->a(Landroid/content/Context;Ljava/lang/String;Z)V

    goto :goto_0
.end method

.method public final a(Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 284
    iget-object v0, p0, Lcom/osp/app/signin/kd;->t:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 285
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/osp/app/signin/kd;->w:Z

    .line 286
    return-void
.end method

.method public final b()I
    .locals 3

    .prologue
    const/4 v1, 0x1

    .line 344
    const/4 v0, 0x0

    .line 345
    iget-boolean v2, p0, Lcom/osp/app/signin/kd;->a:Z

    if-ne v2, v1, :cond_0

    move v0, v1

    .line 349
    :cond_0
    iget-boolean v2, p0, Lcom/osp/app/signin/kd;->b:Z

    if-ne v2, v1, :cond_1

    .line 351
    add-int/lit8 v0, v0, 0x1

    .line 353
    :cond_1
    iget-boolean v2, p0, Lcom/osp/app/signin/kd;->c:Z

    if-ne v2, v1, :cond_2

    .line 355
    add-int/lit8 v0, v0, 0x1

    .line 357
    :cond_2
    iget-boolean v2, p0, Lcom/osp/app/signin/kd;->d:Z

    if-ne v2, v1, :cond_3

    .line 359
    add-int/lit8 v0, v0, 0x1

    .line 361
    :cond_3
    iget-boolean v2, p0, Lcom/osp/app/signin/kd;->e:Z

    if-ne v2, v1, :cond_4

    .line 363
    add-int/lit8 v0, v0, 0x1

    .line 365
    :cond_4
    iget-boolean v2, p0, Lcom/osp/app/signin/kd;->f:Z

    if-ne v2, v1, :cond_5

    .line 367
    add-int/lit8 v0, v0, 0x1

    .line 369
    :cond_5
    iget-boolean v2, p0, Lcom/osp/app/signin/kd;->j:Z

    if-ne v2, v1, :cond_6

    .line 371
    add-int/lit8 v0, v0, 0x1

    .line 373
    :cond_6
    iget-boolean v2, p0, Lcom/osp/app/signin/kd;->k:Z

    if-ne v2, v1, :cond_7

    .line 375
    add-int/lit8 v0, v0, 0x1

    .line 377
    :cond_7
    iget-boolean v2, p0, Lcom/osp/app/signin/kd;->l:Z

    if-ne v2, v1, :cond_8

    .line 379
    add-int/lit8 v0, v0, 0x1

    .line 381
    :cond_8
    iget-boolean v2, p0, Lcom/osp/app/signin/kd;->m:Z

    if-ne v2, v1, :cond_9

    .line 383
    add-int/lit8 v0, v0, 0x1

    .line 385
    :cond_9
    iget-boolean v2, p0, Lcom/osp/app/signin/kd;->g:Z

    if-ne v2, v1, :cond_a

    .line 387
    add-int/lit8 v0, v0, 0x1

    .line 389
    :cond_a
    iget-boolean v2, p0, Lcom/osp/app/signin/kd;->h:Z

    if-ne v2, v1, :cond_b

    .line 391
    add-int/lit8 v0, v0, 0x1

    .line 393
    :cond_b
    iget-boolean v2, p0, Lcom/osp/app/signin/kd;->i:Z

    if-ne v2, v1, :cond_c

    .line 395
    add-int/lit8 v0, v0, 0x1

    .line 397
    :cond_c
    iget-boolean v2, p0, Lcom/osp/app/signin/kd;->n:Z

    if-ne v2, v1, :cond_d

    .line 399
    add-int/lit8 v0, v0, 0x1

    .line 401
    :cond_d
    iget-boolean v2, p0, Lcom/osp/app/signin/kd;->o:Z

    if-ne v2, v1, :cond_e

    .line 403
    add-int/lit8 v0, v0, 0x1

    .line 405
    :cond_e
    iget-boolean v2, p0, Lcom/osp/app/signin/kd;->p:Z

    if-ne v2, v1, :cond_f

    .line 407
    add-int/lit8 v0, v0, 0x1

    .line 409
    :cond_f
    iget-boolean v2, p0, Lcom/osp/app/signin/kd;->q:Z

    if-ne v2, v1, :cond_10

    .line 411
    add-int/lit8 v0, v0, 0x1

    .line 413
    :cond_10
    iget-boolean v2, p0, Lcom/osp/app/signin/kd;->r:Z

    if-ne v2, v1, :cond_11

    .line 415
    add-int/lit8 v0, v0, 0x1

    .line 417
    :cond_11
    return v0
.end method

.method public final b(Ljava/lang/String;)V
    .locals 4

    .prologue
    .line 299
    const-string v0, "|"

    invoke-virtual {p1, v0}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v0

    .line 304
    const/4 v1, 0x0

    const/4 v2, 0x3

    invoke-virtual {p1, v1, v2}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v1

    .line 306
    const/4 v2, 0x4

    invoke-virtual {p1, v2, v0}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v2

    .line 307
    add-int/lit8 v0, v0, 0x5

    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v3

    invoke-virtual {p1, v0, v3}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v0

    .line 308
    iget-object v3, p0, Lcom/osp/app/signin/kd;->u:Ljava/util/ArrayList;

    invoke-virtual {v3, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 309
    iget-object v2, p0, Lcom/osp/app/signin/kd;->u:Ljava/util/ArrayList;

    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 310
    iget-object v0, p0, Lcom/osp/app/signin/kd;->v:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 311
    iget-object v0, p0, Lcom/osp/app/signin/kd;->v:Ljava/util/ArrayList;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 312
    return-void
.end method

.method public final c()Z
    .locals 1

    .prologue
    .line 425
    iget-boolean v0, p0, Lcom/osp/app/signin/kd;->b:Z

    return v0
.end method

.method public final d()Z
    .locals 1

    .prologue
    .line 437
    iget-object v0, p0, Lcom/osp/app/signin/kd;->y:Lcom/osp/app/signin/ke;

    iget-boolean v0, v0, Lcom/osp/app/signin/ke;->b:Z

    return v0
.end method

.method public final e()Z
    .locals 1

    .prologue
    .line 441
    iget-boolean v0, p0, Lcom/osp/app/signin/kd;->d:Z

    return v0
.end method

.method public final f()Z
    .locals 1

    .prologue
    .line 445
    iget-object v0, p0, Lcom/osp/app/signin/kd;->y:Lcom/osp/app/signin/ke;

    iget-boolean v0, v0, Lcom/osp/app/signin/ke;->c:Z

    return v0
.end method

.method public final g()Z
    .locals 1

    .prologue
    .line 449
    iget-boolean v0, p0, Lcom/osp/app/signin/kd;->e:Z

    return v0
.end method

.method public final h()Z
    .locals 1

    .prologue
    .line 453
    iget-object v0, p0, Lcom/osp/app/signin/kd;->y:Lcom/osp/app/signin/ke;

    iget-boolean v0, v0, Lcom/osp/app/signin/ke;->d:Z

    return v0
.end method

.method public final i()Z
    .locals 1

    .prologue
    .line 461
    iget-object v0, p0, Lcom/osp/app/signin/kd;->y:Lcom/osp/app/signin/ke;

    iget-boolean v0, v0, Lcom/osp/app/signin/ke;->e:Z

    return v0
.end method

.method public final j()Z
    .locals 1

    .prologue
    .line 469
    iget-object v0, p0, Lcom/osp/app/signin/kd;->y:Lcom/osp/app/signin/ke;

    iget-boolean v0, v0, Lcom/osp/app/signin/ke;->i:Z

    return v0
.end method

.method public final k()Z
    .locals 1

    .prologue
    .line 477
    iget-object v0, p0, Lcom/osp/app/signin/kd;->y:Lcom/osp/app/signin/ke;

    iget-boolean v0, v0, Lcom/osp/app/signin/ke;->j:Z

    return v0
.end method

.method public final l()Z
    .locals 1

    .prologue
    .line 485
    iget-object v0, p0, Lcom/osp/app/signin/kd;->y:Lcom/osp/app/signin/ke;

    iget-boolean v0, v0, Lcom/osp/app/signin/ke;->k:Z

    return v0
.end method

.method public final m()Z
    .locals 1

    .prologue
    .line 493
    iget-object v0, p0, Lcom/osp/app/signin/kd;->y:Lcom/osp/app/signin/ke;

    iget-boolean v0, v0, Lcom/osp/app/signin/ke;->l:Z

    return v0
.end method

.method public final n()Z
    .locals 1

    .prologue
    .line 501
    iget-object v0, p0, Lcom/osp/app/signin/kd;->y:Lcom/osp/app/signin/ke;

    iget-boolean v0, v0, Lcom/osp/app/signin/ke;->f:Z

    return v0
.end method

.method public final o()Z
    .locals 1

    .prologue
    .line 509
    iget-object v0, p0, Lcom/osp/app/signin/kd;->y:Lcom/osp/app/signin/ke;

    iget-boolean v0, v0, Lcom/osp/app/signin/ke;->g:Z

    return v0
.end method

.method public final p()Z
    .locals 1

    .prologue
    .line 521
    iget-object v0, p0, Lcom/osp/app/signin/kd;->y:Lcom/osp/app/signin/ke;

    iget-boolean v0, v0, Lcom/osp/app/signin/ke;->m:Z

    return v0
.end method

.method public final q()Z
    .locals 1

    .prologue
    .line 529
    iget-object v0, p0, Lcom/osp/app/signin/kd;->y:Lcom/osp/app/signin/ke;

    iget-boolean v0, v0, Lcom/osp/app/signin/ke;->n:Z

    return v0
.end method

.method public final r()Z
    .locals 1

    .prologue
    .line 537
    iget-object v0, p0, Lcom/osp/app/signin/kd;->y:Lcom/osp/app/signin/ke;

    iget-boolean v0, v0, Lcom/osp/app/signin/ke;->o:Z

    return v0
.end method

.method public final s()V
    .locals 1

    .prologue
    .line 577
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/osp/app/signin/kd;->a:Z

    .line 578
    return-void
.end method

.method public final t()V
    .locals 1

    .prologue
    .line 581
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/osp/app/signin/kd;->b:Z

    .line 582
    return-void
.end method

.method public final u()V
    .locals 1

    .prologue
    .line 589
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/osp/app/signin/kd;->c:Z

    .line 590
    return-void
.end method

.method public final v()V
    .locals 2

    .prologue
    .line 593
    iget-object v0, p0, Lcom/osp/app/signin/kd;->y:Lcom/osp/app/signin/ke;

    const/4 v1, 0x1

    iput-boolean v1, v0, Lcom/osp/app/signin/ke;->b:Z

    .line 594
    return-void
.end method

.method public final w()V
    .locals 1

    .prologue
    .line 597
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/osp/app/signin/kd;->d:Z

    .line 598
    return-void
.end method

.method public final x()V
    .locals 2

    .prologue
    .line 601
    iget-object v0, p0, Lcom/osp/app/signin/kd;->y:Lcom/osp/app/signin/ke;

    const/4 v1, 0x1

    iput-boolean v1, v0, Lcom/osp/app/signin/ke;->c:Z

    .line 602
    return-void
.end method

.method public final y()V
    .locals 1

    .prologue
    .line 605
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/osp/app/signin/kd;->e:Z

    .line 606
    return-void
.end method

.method public final z()V
    .locals 2

    .prologue
    .line 609
    iget-object v0, p0, Lcom/osp/app/signin/kd;->y:Lcom/osp/app/signin/ke;

    const/4 v1, 0x1

    iput-boolean v1, v0, Lcom/osp/app/signin/ke;->d:Z

    .line 610
    return-void
.end method
