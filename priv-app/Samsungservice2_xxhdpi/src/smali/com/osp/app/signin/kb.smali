.class public final enum Lcom/osp/app/signin/kb;
.super Ljava/lang/Enum;
.source "SignIn_Ver01.java"


# static fields
.field public static final enum a:Lcom/osp/app/signin/kb;

.field public static final enum b:Lcom/osp/app/signin/kb;

.field public static final enum c:Lcom/osp/app/signin/kb;

.field public static final enum d:Lcom/osp/app/signin/kb;

.field public static final enum e:Lcom/osp/app/signin/kb;

.field public static final enum f:Lcom/osp/app/signin/kb;

.field public static final enum g:Lcom/osp/app/signin/kb;

.field public static final enum h:Lcom/osp/app/signin/kb;

.field public static final enum i:Lcom/osp/app/signin/kb;

.field public static final enum j:Lcom/osp/app/signin/kb;

.field public static final enum k:Lcom/osp/app/signin/kb;

.field private static final synthetic l:[Lcom/osp/app/signin/kb;


# direct methods
.method static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 48
    new-instance v0, Lcom/osp/app/signin/kb;

    const-string v1, "Canceled"

    invoke-direct {v0, v1, v3}, Lcom/osp/app/signin/kb;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/osp/app/signin/kb;->a:Lcom/osp/app/signin/kb;

    new-instance v0, Lcom/osp/app/signin/kb;

    const-string v1, "ContainsAccessToken"

    invoke-direct {v0, v1, v4}, Lcom/osp/app/signin/kb;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/osp/app/signin/kb;->b:Lcom/osp/app/signin/kb;

    new-instance v0, Lcom/osp/app/signin/kb;

    const-string v1, "ContainsAuthToken"

    invoke-direct {v0, v1, v5}, Lcom/osp/app/signin/kb;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/osp/app/signin/kb;->c:Lcom/osp/app/signin/kb;

    new-instance v0, Lcom/osp/app/signin/kb;

    const-string v1, "Failed"

    invoke-direct {v0, v1, v6}, Lcom/osp/app/signin/kb;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/osp/app/signin/kb;->d:Lcom/osp/app/signin/kb;

    new-instance v0, Lcom/osp/app/signin/kb;

    const-string v1, "IsAppRegistered"

    invoke-direct {v0, v1, v7}, Lcom/osp/app/signin/kb;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/osp/app/signin/kb;->e:Lcom/osp/app/signin/kb;

    new-instance v0, Lcom/osp/app/signin/kb;

    const-string v1, "IsNotAppRegistered"

    const/4 v2, 0x5

    invoke-direct {v0, v1, v2}, Lcom/osp/app/signin/kb;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/osp/app/signin/kb;->f:Lcom/osp/app/signin/kb;

    new-instance v0, Lcom/osp/app/signin/kb;

    const-string v1, "NotContainsAccessToken"

    const/4 v2, 0x6

    invoke-direct {v0, v1, v2}, Lcom/osp/app/signin/kb;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/osp/app/signin/kb;->g:Lcom/osp/app/signin/kb;

    new-instance v0, Lcom/osp/app/signin/kb;

    const-string v1, "NotContainsAuthToken"

    const/4 v2, 0x7

    invoke-direct {v0, v1, v2}, Lcom/osp/app/signin/kb;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/osp/app/signin/kb;->h:Lcom/osp/app/signin/kb;

    new-instance v0, Lcom/osp/app/signin/kb;

    const-string v1, "Start"

    const/16 v2, 0x8

    invoke-direct {v0, v1, v2}, Lcom/osp/app/signin/kb;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/osp/app/signin/kb;->i:Lcom/osp/app/signin/kb;

    new-instance v0, Lcom/osp/app/signin/kb;

    const-string v1, "Success"

    const/16 v2, 0x9

    invoke-direct {v0, v1, v2}, Lcom/osp/app/signin/kb;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/osp/app/signin/kb;->j:Lcom/osp/app/signin/kb;

    new-instance v0, Lcom/osp/app/signin/kb;

    const-string v1, "UserAuthentication"

    const/16 v2, 0xa

    invoke-direct {v0, v1, v2}, Lcom/osp/app/signin/kb;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/osp/app/signin/kb;->k:Lcom/osp/app/signin/kb;

    .line 46
    const/16 v0, 0xb

    new-array v0, v0, [Lcom/osp/app/signin/kb;

    sget-object v1, Lcom/osp/app/signin/kb;->a:Lcom/osp/app/signin/kb;

    aput-object v1, v0, v3

    sget-object v1, Lcom/osp/app/signin/kb;->b:Lcom/osp/app/signin/kb;

    aput-object v1, v0, v4

    sget-object v1, Lcom/osp/app/signin/kb;->c:Lcom/osp/app/signin/kb;

    aput-object v1, v0, v5

    sget-object v1, Lcom/osp/app/signin/kb;->d:Lcom/osp/app/signin/kb;

    aput-object v1, v0, v6

    sget-object v1, Lcom/osp/app/signin/kb;->e:Lcom/osp/app/signin/kb;

    aput-object v1, v0, v7

    const/4 v1, 0x5

    sget-object v2, Lcom/osp/app/signin/kb;->f:Lcom/osp/app/signin/kb;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, Lcom/osp/app/signin/kb;->g:Lcom/osp/app/signin/kb;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, Lcom/osp/app/signin/kb;->h:Lcom/osp/app/signin/kb;

    aput-object v2, v0, v1

    const/16 v1, 0x8

    sget-object v2, Lcom/osp/app/signin/kb;->i:Lcom/osp/app/signin/kb;

    aput-object v2, v0, v1

    const/16 v1, 0x9

    sget-object v2, Lcom/osp/app/signin/kb;->j:Lcom/osp/app/signin/kb;

    aput-object v2, v0, v1

    const/16 v1, 0xa

    sget-object v2, Lcom/osp/app/signin/kb;->k:Lcom/osp/app/signin/kb;

    aput-object v2, v0, v1

    sput-object v0, Lcom/osp/app/signin/kb;->l:[Lcom/osp/app/signin/kb;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0

    .prologue
    .line 46
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/osp/app/signin/kb;
    .locals 1

    .prologue
    .line 46
    const-class v0, Lcom/osp/app/signin/kb;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/osp/app/signin/kb;

    return-object v0
.end method

.method public static values()[Lcom/osp/app/signin/kb;
    .locals 1

    .prologue
    .line 46
    sget-object v0, Lcom/osp/app/signin/kb;->l:[Lcom/osp/app/signin/kb;

    invoke-virtual {v0}, [Lcom/osp/app/signin/kb;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/osp/app/signin/kb;

    return-object v0
.end method
