.class final Lcom/osp/app/signin/be;
.super Lcom/msc/c/b;
.source "AccountView.java"


# instance fields
.field final synthetic c:Lcom/osp/app/signin/AccountView;

.field private d:J

.field private e:Ljava/lang/String;

.field private final f:I


# direct methods
.method public constructor <init>(Lcom/osp/app/signin/AccountView;I)V
    .locals 2

    .prologue
    .line 8510
    iput-object p1, p0, Lcom/osp/app/signin/be;->c:Lcom/osp/app/signin/AccountView;

    .line 8511
    invoke-direct {p0, p1}, Lcom/msc/c/b;-><init>(Landroid/content/Context;)V

    .line 8513
    iput p2, p0, Lcom/osp/app/signin/be;->f:I

    .line 8517
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v0, "AV"

    const-string v1, "GetMyCountryZoneTask"

    invoke-static {v0, v1}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 8520
    return-void
.end method


# virtual methods
.method protected final varargs a()Ljava/lang/Boolean;
    .locals 3

    .prologue
    .line 8532
    invoke-static {}, Lcom/msc/c/g;->a()Lcom/msc/c/g;

    iget-object v0, p0, Lcom/osp/app/signin/be;->c:Lcom/osp/app/signin/AccountView;

    invoke-static {v0, p0}, Lcom/msc/c/g;->b(Landroid/content/Context;Lcom/msc/b/h;)J

    move-result-wide v0

    iput-wide v0, p0, Lcom/osp/app/signin/be;->d:J

    iget-wide v0, p0, Lcom/osp/app/signin/be;->d:J

    invoke-virtual {p0, v0, v1}, Lcom/osp/app/signin/be;->a(J)V

    iget-wide v0, p0, Lcom/osp/app/signin/be;->d:J

    sget-object v2, Lcom/msc/b/g;->b:Lcom/msc/b/g;

    invoke-static {v0, v1, v2}, Lcom/msc/c/g;->a(JLcom/msc/b/g;)V

    .line 8533
    const/4 v0, 0x0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    return-object v0
.end method

.method public final declared-synchronized a(Lcom/msc/b/c;)V
    .locals 6

    .prologue
    .line 8621
    monitor-enter p0

    :try_start_0
    invoke-super {p0, p1}, Lcom/msc/c/b;->a(Lcom/msc/b/c;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 8623
    if-nez p1, :cond_1

    .line 8668
    :cond_0
    :goto_0
    monitor-exit p0

    return-void

    .line 8628
    :cond_1
    :try_start_1
    invoke-virtual {p1}, Lcom/msc/b/c;->a()J

    move-result-wide v0

    .line 8629
    invoke-virtual {p1}, Lcom/msc/b/c;->e()Ljava/lang/String;

    move-result-object v2

    .line 8631
    iget-wide v4, p0, Lcom/osp/app/signin/be;->d:J
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    cmp-long v0, v0, v4

    if-nez v0, :cond_0

    .line 8635
    :try_start_2
    invoke-static {}, Lcom/msc/c/h;->a()Lcom/msc/c/h;

    invoke-static {v2}, Lcom/msc/c/h;->w(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 8637
    if-eqz v0, :cond_3

    .line 8639
    invoke-static {}, Lcom/osp/common/util/i;->a()Lcom/osp/common/util/i;

    iget-object v1, p0, Lcom/osp/app/signin/be;->c:Lcom/osp/app/signin/AccountView;

    invoke-static {v1}, Lcom/osp/common/util/i;->o(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v1

    .line 8640
    invoke-static {}, Lcom/osp/common/util/i;->a()Lcom/osp/common/util/i;

    iget-object v2, p0, Lcom/osp/app/signin/be;->c:Lcom/osp/app/signin/AccountView;

    invoke-static {v1}, Lcom/osp/common/util/i;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 8641
    if-eqz v2, :cond_2

    invoke-virtual {v2, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_2

    .line 8643
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v3, "GetMyCountryZoneTask countryCodeFromNetworkMcc = ["

    invoke-direct {v0, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, "]"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;)V

    .line 8644
    iget-object v0, p0, Lcom/osp/app/signin/be;->c:Lcom/osp/app/signin/AccountView;

    invoke-static {v0, v1}, Lcom/osp/app/signin/AccountView;->g(Lcom/osp/app/signin/AccountView;Ljava/lang/String;)Ljava/lang/String;

    .line 8654
    :goto_1
    iget-object v0, p0, Lcom/osp/app/signin/be;->c:Lcom/osp/app/signin/AccountView;

    invoke-static {v0}, Lcom/osp/app/signin/AccountView;->R(Lcom/osp/app/signin/AccountView;)Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_4

    iget-object v0, p0, Lcom/osp/app/signin/be;->c:Lcom/osp/app/signin/AccountView;

    invoke-static {v0}, Lcom/osp/app/signin/AccountView;->R(Lcom/osp/app/signin/AccountView;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v0

    if-lez v0, :cond_4

    .line 8656
    iget-object v0, p0, Lcom/osp/app/signin/be;->c:Lcom/osp/app/signin/AccountView;

    iget-object v1, p0, Lcom/osp/app/signin/be;->c:Lcom/osp/app/signin/AccountView;

    invoke-static {v1}, Lcom/osp/app/signin/AccountView;->R(Lcom/osp/app/signin/AccountView;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/osp/app/util/ad;->e(Landroid/content/Context;Ljava/lang/String;)V

    .line 8657
    const-string v0, "Success"

    iput-object v0, p0, Lcom/osp/app/signin/be;->e:Ljava/lang/String;
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_0
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0

    .line 8662
    :catch_0
    move-exception v0

    :try_start_3
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    .line 8665
    const-string v0, "Fail"

    iput-object v0, p0, Lcom/osp/app/signin/be;->e:Ljava/lang/String;
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    goto :goto_0

    .line 8621
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 8647
    :cond_2
    :try_start_4
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "GetMyCountryZoneTask countryCode = ["

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "]"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;)V

    .line 8648
    iget-object v1, p0, Lcom/osp/app/signin/be;->c:Lcom/osp/app/signin/AccountView;

    invoke-static {}, Lcom/osp/common/util/i;->a()Lcom/osp/common/util/i;

    iget-object v2, p0, Lcom/osp/app/signin/be;->c:Lcom/osp/app/signin/AccountView;

    sget-object v2, Ljava/util/Locale;->ENGLISH:Ljava/util/Locale;

    invoke-virtual {v0, v2}, Ljava/lang/String;->toLowerCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/osp/common/util/i;->c(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Lcom/osp/app/signin/AccountView;->g(Lcom/osp/app/signin/AccountView;Ljava/lang/String;)Ljava/lang/String;

    goto :goto_1

    .line 8652
    :cond_3
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v0, "AV"

    const-string v1, "GetMyCountryZoneTask Success. but countryCode is null!!"

    invoke-static {v0, v1}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1

    .line 8660
    :cond_4
    const-string v0, "Fail"

    iput-object v0, p0, Lcom/osp/app/signin/be;->e:Ljava/lang/String;
    :try_end_4
    .catch Ljava/lang/Exception; {:try_start_4 .. :try_end_4} :catch_0
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    goto/16 :goto_0
.end method

.method protected final a(Ljava/lang/Boolean;)V
    .locals 4

    .prologue
    .line 8545
    invoke-super {p0, p1}, Lcom/msc/c/b;->a(Ljava/lang/Boolean;)V

    .line 8547
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v0, "AV"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "GetMyCountryZoneTask mcc = "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, Lcom/osp/app/signin/be;->c:Lcom/osp/app/signin/AccountView;

    invoke-static {v2}, Lcom/osp/app/signin/AccountView;->R(Lcom/osp/app/signin/AccountView;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 8549
    iget v0, p0, Lcom/osp/app/signin/be;->f:I

    packed-switch v0, :pswitch_data_0

    .line 8613
    :goto_0
    return-void

    .line 8552
    :pswitch_0
    const-string v0, "Success"

    iget-object v1, p0, Lcom/osp/app/signin/be;->e:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/osp/app/signin/be;->c:Lcom/osp/app/signin/AccountView;

    invoke-static {v0}, Lcom/osp/app/signin/AccountView;->R(Lcom/osp/app/signin/AccountView;)Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_3

    .line 8554
    iget-object v0, p0, Lcom/osp/app/signin/be;->c:Lcom/osp/app/signin/AccountView;

    iget-object v1, p0, Lcom/osp/app/signin/be;->c:Lcom/osp/app/signin/AccountView;

    invoke-static {v1}, Lcom/osp/app/signin/AccountView;->R(Lcom/osp/app/signin/AccountView;)Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/osp/app/signin/be;->c:Lcom/osp/app/signin/AccountView;

    invoke-virtual {v2}, Lcom/osp/app/signin/AccountView;->getApplicationContext()Landroid/content/Context;

    iget-object v2, p0, Lcom/osp/app/signin/be;->c:Lcom/osp/app/signin/AccountView;

    invoke-static {v2}, Lcom/osp/app/signin/AccountView;->R(Lcom/osp/app/signin/AccountView;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/osp/common/util/i;->d(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v1, v2}, Lcom/msc/c/e;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)V

    .line 8555
    invoke-static {}, Lcom/osp/app/signin/SamsungService;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 8557
    iget-object v0, p0, Lcom/osp/app/signin/be;->c:Lcom/osp/app/signin/AccountView;

    invoke-static {v0}, Lcom/osp/app/signin/AccountView;->S(Lcom/osp/app/signin/AccountView;)V

    goto :goto_0

    .line 8560
    :cond_0
    const-string v0, "450"

    iget-object v1, p0, Lcom/osp/app/signin/be;->c:Lcom/osp/app/signin/AccountView;

    invoke-static {v1}, Lcom/osp/app/signin/AccountView;->R(Lcom/osp/app/signin/AccountView;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/osp/app/signin/be;->c:Lcom/osp/app/signin/AccountView;

    invoke-static {v0}, Lcom/osp/app/util/r;->s(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_2

    :cond_1
    iget-object v0, p0, Lcom/osp/app/signin/be;->c:Lcom/osp/app/signin/AccountView;

    invoke-static {v0}, Lcom/osp/app/signin/AccountView;->T(Lcom/osp/app/signin/AccountView;)Z

    move-result v0

    if-nez v0, :cond_2

    .line 8562
    iget-object v0, p0, Lcom/osp/app/signin/be;->c:Lcom/osp/app/signin/AccountView;

    invoke-static {v0}, Lcom/osp/app/signin/AccountView;->U(Lcom/osp/app/signin/AccountView;)V

    goto :goto_0

    .line 8565
    :cond_2
    iget-object v0, p0, Lcom/osp/app/signin/be;->c:Lcom/osp/app/signin/AccountView;

    invoke-static {v0}, Lcom/osp/app/signin/AccountView;->S(Lcom/osp/app/signin/AccountView;)V

    goto :goto_0

    .line 8570
    :cond_3
    iget-object v0, p0, Lcom/osp/app/signin/be;->c:Lcom/osp/app/signin/AccountView;

    const/16 v1, 0xd0

    invoke-static {v0, v1}, Lcom/osp/app/signin/AccountView;->c(Lcom/osp/app/signin/AccountView;I)V

    .line 8571
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v0, "AV"

    const-string v1, "GetMyCountryZoneTask Fail"

    invoke-static {v0, v1}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 8575
    :pswitch_1
    const-string v0, "Success"

    iget-object v1, p0, Lcom/osp/app/signin/be;->e:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_5

    iget-object v0, p0, Lcom/osp/app/signin/be;->c:Lcom/osp/app/signin/AccountView;

    invoke-static {v0}, Lcom/osp/app/signin/AccountView;->R(Lcom/osp/app/signin/AccountView;)Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_5

    .line 8577
    iget-object v0, p0, Lcom/osp/app/signin/be;->c:Lcom/osp/app/signin/AccountView;

    iget-object v1, p0, Lcom/osp/app/signin/be;->c:Lcom/osp/app/signin/AccountView;

    invoke-static {v1}, Lcom/osp/app/signin/AccountView;->R(Lcom/osp/app/signin/AccountView;)Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/osp/app/signin/be;->c:Lcom/osp/app/signin/AccountView;

    invoke-virtual {v2}, Lcom/osp/app/signin/AccountView;->getApplicationContext()Landroid/content/Context;

    iget-object v2, p0, Lcom/osp/app/signin/be;->c:Lcom/osp/app/signin/AccountView;

    invoke-static {v2}, Lcom/osp/app/signin/AccountView;->R(Lcom/osp/app/signin/AccountView;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/osp/common/util/i;->d(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v1, v2}, Lcom/msc/c/e;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)V

    .line 8579
    const/4 v0, 0x0

    .line 8581
    iget-object v1, p0, Lcom/osp/app/signin/be;->c:Lcom/osp/app/signin/AccountView;

    invoke-static {v1}, Lcom/osp/app/signin/AccountView;->V(Lcom/osp/app/signin/AccountView;)Z

    move-result v1

    if-eqz v1, :cond_4

    .line 8583
    iget-object v0, p0, Lcom/osp/app/signin/be;->c:Lcom/osp/app/signin/AccountView;

    invoke-static {v0}, Lcom/osp/app/signin/AccountView;->W(Lcom/osp/app/signin/AccountView;)Ljava/lang/String;

    move-result-object v0

    .line 8586
    :cond_4
    iget-object v1, p0, Lcom/osp/app/signin/be;->c:Lcom/osp/app/signin/AccountView;

    invoke-static {v1, v0}, Lcom/osp/app/signin/AccountView;->f(Lcom/osp/app/signin/AccountView;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 8589
    :cond_5
    iget-object v0, p0, Lcom/osp/app/signin/be;->c:Lcom/osp/app/signin/AccountView;

    const/16 v1, 0xdf

    invoke-static {v0, v1}, Lcom/osp/app/signin/AccountView;->c(Lcom/osp/app/signin/AccountView;I)V

    .line 8590
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v0, "AV"

    const-string v1, "GetMyCountryZoneTask Fail"

    invoke-static {v0, v1}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 8596
    :pswitch_2
    :try_start_0
    iget-object v0, p0, Lcom/osp/app/signin/be;->c:Lcom/osp/app/signin/AccountView;

    iget-object v1, p0, Lcom/osp/app/signin/be;->c:Lcom/osp/app/signin/AccountView;

    invoke-static {v1}, Lcom/osp/app/signin/AccountView;->X(Lcom/osp/app/signin/AccountView;)Z

    move-result v1

    invoke-static {v0, v1}, Lcom/msc/c/n;->a(Landroid/content/Context;Z)Ljava/lang/String;

    move-result-object v0

    .line 8597
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v1, "AV"

    const-string v2, "GetMyCountryZoneTask Fail"

    invoke-static {v1, v2}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 8598
    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    .line 8599
    new-instance v2, Landroid/content/Intent;

    const-string v3, "android.intent.action.VIEW"

    invoke-direct {v2, v3, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;Landroid/net/Uri;)V

    .line 8600
    const-string v1, "com.android.browser.application_id"

    iget-object v3, p0, Lcom/osp/app/signin/be;->c:Lcom/osp/app/signin/AccountView;

    invoke-virtual {v3}, Lcom/osp/app/signin/AccountView;->getPackageName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v1, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 8602
    invoke-static {}, Lcom/osp/app/signin/SamsungService;->a()Z

    move-result v1

    if-nez v1, :cond_6

    iget-object v1, p0, Lcom/osp/app/signin/be;->c:Lcom/osp/app/signin/AccountView;

    invoke-static {v2, v1}, Lcom/osp/app/util/ad;->a(Landroid/content/Intent;Landroid/content/Context;)Z

    move-result v1

    const/4 v3, 0x1

    if-ne v1, v3, :cond_6

    .line 8604
    iget-object v0, p0, Lcom/osp/app/signin/be;->c:Lcom/osp/app/signin/AccountView;

    invoke-virtual {v0, v2}, Lcom/osp/app/signin/AccountView;->startActivity(Landroid/content/Intent;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto/16 :goto_0

    .line 8609
    :catch_0
    move-exception v0

    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    goto/16 :goto_0

    .line 8607
    :cond_6
    :try_start_1
    iget-object v1, p0, Lcom/osp/app/signin/be;->c:Lcom/osp/app/signin/AccountView;

    invoke-static {v1, v0}, Lcom/osp/app/signin/AccountView;->a(Lcom/osp/app/signin/AccountView;Ljava/lang/String;)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    goto/16 :goto_0

    .line 8549
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method public final b(Lcom/msc/b/c;)V
    .locals 4

    .prologue
    .line 8672
    invoke-super {p0, p1}, Lcom/msc/c/b;->b(Lcom/msc/b/c;)V

    .line 8674
    if-nez p1, :cond_1

    .line 8686
    :cond_0
    :goto_0
    return-void

    .line 8679
    :cond_1
    invoke-virtual {p1}, Lcom/msc/b/c;->a()J

    move-result-wide v0

    .line 8681
    iget-wide v2, p0, Lcom/osp/app/signin/be;->d:J

    cmp-long v0, v0, v2

    if-nez v0, :cond_0

    .line 8684
    const-string v0, "Fail"

    iput-object v0, p0, Lcom/osp/app/signin/be;->e:Ljava/lang/String;

    goto :goto_0
.end method

.method protected final d()V
    .locals 2

    .prologue
    .line 8538
    invoke-super {p0}, Lcom/msc/c/b;->d()V

    .line 8539
    iget-object v0, p0, Lcom/osp/app/signin/be;->c:Lcom/osp/app/signin/AccountView;

    const/16 v1, 0xe

    invoke-virtual {v0, v1}, Lcom/osp/app/signin/AccountView;->setResult(I)V

    .line 8540
    iget-object v0, p0, Lcom/osp/app/signin/be;->c:Lcom/osp/app/signin/AccountView;

    invoke-virtual {v0}, Lcom/osp/app/signin/AccountView;->finish()V

    .line 8541
    return-void
.end method

.method protected final synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 8479
    invoke-virtual {p0}, Lcom/osp/app/signin/be;->a()Ljava/lang/Boolean;

    move-result-object v0

    return-object v0
.end method

.method protected final synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 8479
    check-cast p1, Ljava/lang/Boolean;

    invoke-virtual {p0, p1}, Lcom/osp/app/signin/be;->a(Ljava/lang/Boolean;)V

    return-void
.end method

.method protected final onPreExecute()V
    .locals 0

    .prologue
    .line 8527
    invoke-super {p0}, Lcom/msc/c/b;->onPreExecute()V

    .line 8528
    return-void
.end method
