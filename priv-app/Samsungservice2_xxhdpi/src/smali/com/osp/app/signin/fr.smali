.class final Lcom/osp/app/signin/fr;
.super Lcom/msc/c/b;
.source "InterfaceService.java"


# instance fields
.field final synthetic c:Lcom/osp/app/signin/InterfaceService;

.field private final d:Ljava/lang/String;

.field private final e:Ljava/lang/String;

.field private final f:Ljava/lang/String;

.field private final g:Ljava/lang/String;

.field private final h:[Ljava/lang/String;

.field private i:Ljava/lang/String;

.field private j:Z

.field private final k:I

.field private l:Ljava/lang/String;

.field private m:Ljava/lang/String;

.field private n:J

.field private o:J

.field private p:J

.field private q:J

.field private r:J

.field private s:J

.field private t:J

.field private u:Z

.field private v:Z

.field private w:Ljava/lang/String;

.field private x:Landroid/content/Intent;

.field private final y:J


# direct methods
.method public constructor <init>(Lcom/osp/app/signin/InterfaceService;Landroid/content/Intent;I)V
    .locals 7

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 545
    iput-object p1, p0, Lcom/osp/app/signin/fr;->c:Lcom/osp/app/signin/InterfaceService;

    .line 546
    invoke-direct {p0, p1}, Lcom/msc/c/b;-><init>(Landroid/content/Context;)V

    .line 547
    invoke-virtual {p0}, Lcom/osp/app/signin/fr;->c()V

    .line 550
    iput p3, p0, Lcom/osp/app/signin/fr;->k:I

    .line 551
    const-string v2, "client_id"

    invoke-virtual {p2, v2}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/osp/app/signin/fr;->d:Ljava/lang/String;

    .line 552
    const-string v2, "client_secret"

    invoke-virtual {p2, v2}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/osp/app/signin/fr;->e:Ljava/lang/String;

    .line 553
    const-string v2, "mypackage"

    invoke-virtual {p2, v2}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/osp/app/signin/fr;->f:Ljava/lang/String;

    .line 554
    const-string v2, "expired_access_token"

    invoke-virtual {p2, v2}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/osp/app/signin/fr;->g:Ljava/lang/String;

    .line 555
    const-string v2, "additional"

    invoke-virtual {p2, v2}, Landroid/content/Intent;->getStringArrayExtra(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/osp/app/signin/fr;->h:[Ljava/lang/String;

    .line 556
    const-string v2, "MODE"

    invoke-virtual {p2, v2}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/osp/app/signin/fr;->i:Ljava/lang/String;

    .line 557
    const-string v2, "key_request_id"

    const-wide/16 v4, 0x0

    invoke-virtual {p2, v2, v4, v5}, Landroid/content/Intent;->getLongExtra(Ljava/lang/String;J)J

    move-result-wide v2

    iput-wide v2, p0, Lcom/osp/app/signin/fr;->y:J

    .line 559
    invoke-direct {p0}, Lcom/osp/app/signin/fr;->h()Z

    move-result v2

    if-nez v2, :cond_0

    .line 563
    const/4 v2, 0x3

    new-array v3, v2, [Ljava/lang/String;

    const-string v2, "com.sec.readershub"

    aput-object v2, v3, v1

    const-string v2, "com.sec.readershub2.store"

    aput-object v2, v3, v0

    const/4 v2, 0x2

    const-string v4, "com.sec.pcw"

    aput-object v4, v3, v2

    array-length v4, v3

    move v2, v1

    :goto_0
    if-ge v2, v4, :cond_3

    aget-object v5, v3, v2

    iget-object v6, p0, Lcom/osp/app/signin/fr;->f:Ljava/lang/String;

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_1

    :goto_1
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v1, "SIS"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "hide noti? "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;)V

    if-eqz v0, :cond_2

    .line 565
    const-string v0, "SHOW_NOTIFICATION_WITH_NO_UI"

    iput-object v0, p0, Lcom/osp/app/signin/fr;->i:Ljava/lang/String;

    .line 571
    :goto_2
    invoke-direct {p0}, Lcom/osp/app/signin/fr;->h()Z

    .line 576
    :cond_0
    return-void

    .line 563
    :cond_1
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 568
    :cond_2
    const-string v0, "SHOW_NOTIFICATION_WITH_POPUP"

    iput-object v0, p0, Lcom/osp/app/signin/fr;->i:Ljava/lang/String;

    goto :goto_2

    :cond_3
    move v0, v1

    goto :goto_1
.end method

.method private a(Lcom/msc/a/i;)V
    .locals 8

    .prologue
    const/4 v6, 0x1

    .line 1189
    iget-object v0, p0, Lcom/osp/app/signin/fr;->c:Lcom/osp/app/signin/InterfaceService;

    invoke-static {p1}, Lcom/osp/app/signin/InterfaceService;->a(Lcom/msc/a/i;)I

    move-result v1

    .line 1191
    iget-boolean v0, p0, Lcom/osp/app/signin/fr;->j:Z

    if-eqz v0, :cond_2

    .line 1193
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v0, "SIS"

    const-string v2, "no notification"

    invoke-static {v0, v2}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 1205
    :cond_0
    :goto_0
    const-string v0, "HIDE_NOTIFICATION_WITH_RESULT"

    iget-object v2, p0, Lcom/osp/app/signin/fr;->i:Ljava/lang/String;

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 1207
    iget-object v0, p0, Lcom/osp/app/signin/fr;->c:Lcom/osp/app/signin/InterfaceService;

    iget-object v2, p0, Lcom/osp/app/signin/fr;->c:Lcom/osp/app/signin/InterfaceService;

    invoke-static {v2}, Lcom/msc/c/e;->h(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v2

    iget-object v3, p0, Lcom/osp/app/signin/fr;->d:Ljava/lang/String;

    iget-object v4, p0, Lcom/osp/app/signin/fr;->e:Ljava/lang/String;

    iget-boolean v5, p0, Lcom/osp/app/signin/fr;->j:Z

    iget-object v7, p0, Lcom/osp/app/signin/fr;->w:Ljava/lang/String;

    invoke-static/range {v0 .. v7}, Lcom/osp/app/util/ad;->a(Landroid/content/Context;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;ZZLjava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    iput-object v0, p0, Lcom/osp/app/signin/fr;->x:Landroid/content/Intent;

    .line 1216
    :cond_1
    :goto_1
    return-void

    .line 1197
    :cond_2
    iget-object v0, p0, Lcom/osp/app/signin/fr;->c:Lcom/osp/app/signin/InterfaceService;

    iget-object v2, p0, Lcom/osp/app/signin/fr;->d:Ljava/lang/String;

    iget-object v3, p0, Lcom/osp/app/signin/fr;->e:Ljava/lang/String;

    iget-object v4, p0, Lcom/osp/app/signin/fr;->w:Ljava/lang/String;

    invoke-static {v0, v1, v2, v3, v4}, Lcom/osp/app/signin/InterfaceService;->a(Lcom/osp/app/signin/InterfaceService;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 1199
    const-string v0, "SHOW_NOTIFICATION_WITH_POPUP"

    iget-object v2, p0, Lcom/osp/app/signin/fr;->i:Ljava/lang/String;

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1201
    iget-object v0, p0, Lcom/osp/app/signin/fr;->c:Lcom/osp/app/signin/InterfaceService;

    invoke-static {v0, p1}, Lcom/osp/app/signin/InterfaceService;->b(Lcom/osp/app/signin/InterfaceService;Lcom/msc/a/i;)V

    goto :goto_0

    .line 1209
    :cond_3
    const-string v0, "HIDE_NOTIFICATION_WITH_CHECKLIST_PROCESS"

    iget-object v2, p0, Lcom/osp/app/signin/fr;->i:Ljava/lang/String;

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1211
    iget-object v0, p0, Lcom/osp/app/signin/fr;->c:Lcom/osp/app/signin/InterfaceService;

    iget-object v2, p0, Lcom/osp/app/signin/fr;->c:Lcom/osp/app/signin/InterfaceService;

    invoke-static {v2}, Lcom/msc/c/e;->h(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v2

    iget-object v3, p0, Lcom/osp/app/signin/fr;->d:Ljava/lang/String;

    iget-object v4, p0, Lcom/osp/app/signin/fr;->e:Ljava/lang/String;

    iget-boolean v5, p0, Lcom/osp/app/signin/fr;->j:Z

    iget-object v7, p0, Lcom/osp/app/signin/fr;->w:Ljava/lang/String;

    invoke-static/range {v0 .. v7}, Lcom/osp/app/util/ad;->a(Landroid/content/Context;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;ZZLjava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    .line 1213
    iget-object v1, p0, Lcom/osp/app/signin/fr;->c:Lcom/osp/app/signin/InterfaceService;

    invoke-virtual {v1, v0}, Lcom/osp/app/signin/InterfaceService;->startActivity(Landroid/content/Intent;)V

    goto :goto_1
.end method

.method private a([Ljava/lang/String;Landroid/content/Intent;)V
    .locals 8

    .prologue
    const/4 v3, 0x0

    .line 1027
    if-eqz p1, :cond_10

    .line 1029
    array-length v4, p1

    move v2, v3

    :goto_0
    if-ge v2, v4, :cond_10

    aget-object v0, p1, v2

    .line 1031
    const-string v1, "user_id"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 1033
    const-string v0, "user_id"

    iget-object v1, p0, Lcom/osp/app/signin/fr;->c:Lcom/osp/app/signin/InterfaceService;

    invoke-static {v1}, Lcom/msc/c/e;->h(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p2, v0, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 1029
    :cond_0
    :goto_1
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_0

    .line 1034
    :cond_1
    const-string v1, "birthday"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 1036
    const-string v0, "birthday"

    iget-object v1, p0, Lcom/osp/app/signin/fr;->c:Lcom/osp/app/signin/InterfaceService;

    invoke-static {v1}, Lcom/msc/c/e;->c(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p2, v0, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    goto :goto_1

    .line 1037
    :cond_2
    const-string v1, "email_id"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_3

    .line 1039
    const-string v0, "email_id"

    iget-object v1, p0, Lcom/osp/app/signin/fr;->c:Lcom/osp/app/signin/InterfaceService;

    invoke-static {v1}, Lcom/osp/app/util/ad;->h(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p2, v0, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    goto :goto_1

    .line 1040
    :cond_3
    const-string v1, "mcc"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_4

    .line 1042
    const-string v0, "mcc"

    iget-object v1, p0, Lcom/osp/app/signin/fr;->c:Lcom/osp/app/signin/InterfaceService;

    invoke-static {v1}, Lcom/msc/c/e;->f(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p2, v0, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    goto :goto_1

    .line 1043
    :cond_4
    const-string v1, "server_url"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_5

    .line 1045
    const-string v0, "server_url"

    iget-object v1, p0, Lcom/osp/app/signin/fr;->c:Lcom/osp/app/signin/InterfaceService;

    invoke-static {v1}, Lcom/msc/c/c;->f(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p2, v0, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    goto :goto_1

    .line 1067
    :cond_5
    const-string v1, "api_server_url"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_6

    .line 1069
    iget-object v0, p0, Lcom/osp/app/signin/fr;->c:Lcom/osp/app/signin/InterfaceService;

    const-string v1, "API_SERVER"

    invoke-static {v0, v1}, Lcom/osp/app/util/ad;->g(Landroid/content/Context;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 1070
    const-string v1, "api_server_url"

    invoke-virtual {p2, v1, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    goto :goto_1

    .line 1102
    :cond_6
    const-string v1, "auth_server_url"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_7

    .line 1104
    iget-object v0, p0, Lcom/osp/app/signin/fr;->c:Lcom/osp/app/signin/InterfaceService;

    const-string v1, "AUTH_SERVER"

    invoke-static {v0, v1}, Lcom/osp/app/util/ad;->g(Landroid/content/Context;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 1105
    const-string v1, "auth_server_url"

    invoke-virtual {p2, v1, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    goto :goto_1

    .line 1135
    :cond_7
    const-string v1, "cc"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_8

    .line 1137
    iget-object v0, p0, Lcom/osp/app/signin/fr;->c:Lcom/osp/app/signin/InterfaceService;

    invoke-static {v0}, Lcom/osp/app/util/ad;->n(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v0

    .line 1138
    const-string v1, "cc"

    invoke-virtual {p2, v1, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    goto/16 :goto_1

    .line 1140
    :cond_8
    const-string v1, "device_physical_address_text"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_a

    .line 1142
    iget-object v0, p0, Lcom/osp/app/signin/fr;->c:Lcom/osp/app/signin/InterfaceService;

    const-string v1, "DEVICE_PHYSICAL_ADDRESS_TEXT"

    invoke-virtual {v0, v1, v3}, Lcom/osp/app/signin/InterfaceService;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v0

    .line 1143
    const-string v1, "DEVICE_PHYSICAL_ADDRESS_TEXT"

    const-string v5, ""

    invoke-interface {v0, v1, v5}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 1144
    const/4 v0, 0x0

    .line 1147
    if-eqz v1, :cond_9

    :try_start_0
    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v5

    if-lez v5, :cond_9

    .line 1149
    invoke-static {}, Lcom/msc/c/a;->a()Lcom/msc/c/a;

    move-result-object v5

    invoke-static {}, Lcom/msc/c/a;->a()Lcom/msc/c/a;

    move-result-object v6

    invoke-virtual {v6}, Lcom/msc/c/a;->b()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6, v1}, Lcom/msc/c/a;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 1160
    :goto_2
    const-string v1, "device_physical_address_text"

    invoke-virtual {p2, v1, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    goto/16 :goto_1

    .line 1152
    :cond_9
    :try_start_1
    iget-object v1, p0, Lcom/osp/app/signin/fr;->c:Lcom/osp/app/signin/InterfaceService;

    invoke-static {v1}, Lcom/osp/device/d;->a(Landroid/content/Context;)Lcom/osp/device/a;

    move-result-object v1

    .line 1153
    invoke-virtual {v1}, Lcom/osp/device/a;->d()Ljava/lang/String;
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    move-result-object v0

    goto :goto_2

    .line 1155
    :catch_0
    move-exception v1

    invoke-virtual {v1}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_2

    .line 1161
    :cond_a
    const-string v1, "access_token_expires_in"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_b

    .line 1163
    const-string v0, "access_token_expires_in"

    iget-wide v6, p0, Lcom/osp/app/signin/fr;->n:J

    invoke-virtual {p2, v0, v6, v7}, Landroid/content/Intent;->putExtra(Ljava/lang/String;J)Landroid/content/Intent;

    goto/16 :goto_1

    .line 1164
    :cond_b
    const-string v1, "refresh_token"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_c

    .line 1166
    const-string v0, "refresh_token"

    iget-object v1, p0, Lcom/osp/app/signin/fr;->m:Ljava/lang/String;

    invoke-virtual {p2, v0, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    goto/16 :goto_1

    .line 1167
    :cond_c
    const-string v1, "refresh_token_expires_in"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_d

    .line 1169
    const-string v0, "refresh_token_expires_in"

    iget-wide v6, p0, Lcom/osp/app/signin/fr;->o:J

    invoke-virtual {p2, v0, v6, v7}, Landroid/content/Intent;->putExtra(Ljava/lang/String;J)Landroid/content/Intent;

    goto/16 :goto_1

    .line 1170
    :cond_d
    const-string v1, "login_id"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_e

    .line 1172
    const-string v0, "login_id"

    iget-object v1, p0, Lcom/osp/app/signin/fr;->c:Lcom/osp/app/signin/InterfaceService;

    invoke-static {v1}, Lcom/osp/app/util/ad;->h(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p2, v0, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    goto/16 :goto_1

    .line 1173
    :cond_e
    const-string v1, "login_id_type"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1175
    iget-object v0, p0, Lcom/osp/app/signin/fr;->c:Lcom/osp/app/signin/InterfaceService;

    invoke-static {v0}, Lcom/osp/app/util/ad;->h(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/telephony/PhoneNumberUtils;->isGlobalPhoneNumber(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_f

    .line 1177
    const-string v0, "login_id_type"

    const-string v1, "001"

    invoke-virtual {p2, v0, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    goto/16 :goto_1

    .line 1180
    :cond_f
    const-string v0, "login_id_type"

    const-string v1, "003"

    invoke-virtual {p2, v0, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    goto/16 :goto_1

    .line 1185
    :cond_10
    return-void
.end method

.method private h()Z
    .locals 4

    .prologue
    const/4 v0, 0x0

    const/4 v1, 0x1

    .line 580
    const-string v2, "SHOW_NOTIFICATION_WITH_POPUP"

    iget-object v3, p0, Lcom/osp/app/signin/fr;->i:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    const-string v2, "SHOW_NOTIFICATION_WITH_NO_UI"

    iget-object v3, p0, Lcom/osp/app/signin/fr;->i:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 582
    :cond_0
    iput-boolean v0, p0, Lcom/osp/app/signin/fr;->j:Z

    move v0, v1

    .line 589
    :cond_1
    :goto_0
    return v0

    .line 584
    :cond_2
    const-string v2, "HIDE_NOTIFICATION_WITH_RESULT"

    iget-object v3, p0, Lcom/osp/app/signin/fr;->i:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_3

    const-string v2, "HIDE_NOTIFICATION_WITH_CHECKLIST_PROCESS"

    iget-object v3, p0, Lcom/osp/app/signin/fr;->i:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 586
    :cond_3
    iput-boolean v1, p0, Lcom/osp/app/signin/fr;->j:Z

    move v0, v1

    .line 587
    goto :goto_0
.end method

.method private i()V
    .locals 8

    .prologue
    const/4 v3, 0x0

    const/4 v4, 0x1

    .line 656
    iget-object v0, p0, Lcom/osp/app/signin/fr;->c:Lcom/osp/app/signin/InterfaceService;

    invoke-static {v0}, Lcom/osp/app/util/ad;->p(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/osp/app/signin/fr;->w:Ljava/lang/String;

    .line 658
    const-string v0, "dlw5n54c92"

    iget-object v1, p0, Lcom/osp/app/signin/fr;->d:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 665
    invoke-direct {p0}, Lcom/osp/app/signin/fr;->j()Z

    move-result v0

    iput-boolean v0, p0, Lcom/osp/app/signin/fr;->u:Z

    .line 666
    iget-boolean v0, p0, Lcom/osp/app/signin/fr;->u:Z

    if-ne v0, v4, :cond_0

    .line 668
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v0, "SIS"

    const-string v1, "reuse FMM accessToken from DB"

    invoke-static {v0, v1}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 732
    :goto_0
    return-void

    .line 672
    :cond_0
    iget-object v1, p0, Lcom/osp/app/signin/fr;->d:Ljava/lang/String;

    iget-object v2, p0, Lcom/osp/app/signin/fr;->e:Ljava/lang/String;

    invoke-static {}, Lcom/msc/c/g;->a()Lcom/msc/c/g;

    iget-object v0, p0, Lcom/osp/app/signin/fr;->c:Lcom/osp/app/signin/InterfaceService;

    iget-object v4, p0, Lcom/osp/app/signin/fr;->c:Lcom/osp/app/signin/InterfaceService;

    invoke-static {v4}, Lcom/msc/c/e;->g(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v5

    move-object v4, v3

    move-object v6, v3

    move-object v7, p0

    invoke-static/range {v0 .. v7}, Lcom/msc/c/g;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/osp/app/signin/cn;Lcom/msc/b/h;)J

    move-result-wide v0

    iput-wide v0, p0, Lcom/osp/app/signin/fr;->r:J

    iget-wide v0, p0, Lcom/osp/app/signin/fr;->r:J

    invoke-virtual {p0, v0, v1}, Lcom/osp/app/signin/fr;->a(J)V

    iget-wide v0, p0, Lcom/osp/app/signin/fr;->r:J

    const-string v2, "from_json"

    invoke-virtual {p0, v0, v1, v2}, Lcom/osp/app/signin/fr;->a(JLjava/lang/String;)V

    iget-wide v0, p0, Lcom/osp/app/signin/fr;->r:J

    sget-object v2, Lcom/msc/b/g;->c:Lcom/msc/b/g;

    invoke-static {v0, v1, v2}, Lcom/msc/c/g;->a(JLcom/msc/b/g;)V

    goto :goto_0

    .line 676
    :cond_1
    invoke-direct {p0}, Lcom/osp/app/signin/fr;->j()Z

    move-result v0

    iput-boolean v0, p0, Lcom/osp/app/signin/fr;->u:Z

    .line 677
    iget-boolean v0, p0, Lcom/osp/app/signin/fr;->u:Z

    if-eqz v0, :cond_2

    .line 680
    iget-object v0, p0, Lcom/osp/app/signin/fr;->c:Lcom/osp/app/signin/InterfaceService;

    iget-object v1, p0, Lcom/osp/app/signin/fr;->d:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/msc/openprovider/b;->a(Landroid/content/Context;Ljava/lang/String;)I

    move-result v0

    invoke-static {v0}, Lcom/osp/app/util/ad;->a(I)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 682
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v0, "SIS"

    const-string v1, "reuse accessToken from DB"

    invoke-static {v0, v1}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 705
    :cond_2
    iget-boolean v0, p0, Lcom/osp/app/signin/fr;->u:Z

    if-eqz v0, :cond_4

    .line 707
    invoke-static {}, Lcom/osp/common/util/i;->a()Lcom/osp/common/util/i;

    iget-object v0, p0, Lcom/osp/app/signin/fr;->c:Lcom/osp/app/signin/InterfaceService;

    invoke-static {v0}, Lcom/osp/common/util/i;->l(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v0

    sget-object v1, Ljava/util/Locale;->ENGLISH:Ljava/util/Locale;

    invoke-virtual {v0, v1}, Ljava/lang/String;->toUpperCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v0

    new-instance v1, Lcom/msc/a/h;

    invoke-direct {v1}, Lcom/msc/a/h;-><init>()V

    iget-object v2, p0, Lcom/osp/app/signin/fr;->c:Lcom/osp/app/signin/InterfaceService;

    invoke-static {v2}, Lcom/osp/app/util/ad;->h(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/msc/a/h;->d(Ljava/lang/String;)V

    invoke-virtual {v1}, Lcom/msc/a/h;->b()V

    iget-object v2, p0, Lcom/osp/app/signin/fr;->d:Ljava/lang/String;

    invoke-virtual {v1, v2}, Lcom/msc/a/h;->h(Ljava/lang/String;)V

    iget-object v2, p0, Lcom/osp/app/signin/fr;->f:Ljava/lang/String;

    invoke-virtual {v1, v2}, Lcom/msc/a/h;->i(Ljava/lang/String;)V

    iget-object v2, p0, Lcom/osp/app/signin/fr;->d:Ljava/lang/String;

    invoke-virtual {v1, v2}, Lcom/msc/a/h;->j(Ljava/lang/String;)V

    invoke-virtual {v1, v0}, Lcom/msc/a/h;->e(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/osp/app/signin/fr;->w:Ljava/lang/String;

    invoke-virtual {v1, v0}, Lcom/msc/a/h;->k(Ljava/lang/String;)V

    invoke-static {}, Lcom/osp/device/b;->b()Lcom/osp/device/b;

    move-result-object v0

    invoke-virtual {v0}, Lcom/osp/device/b;->a()Z

    move-result v0

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/osp/app/signin/fr;->c:Lcom/osp/app/signin/InterfaceService;

    invoke-static {v0}, Lcom/osp/app/util/ad;->h(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/telephony/PhoneNumberUtils;->isGlobalPhoneNumber(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/osp/app/signin/fr;->c:Lcom/osp/app/signin/InterfaceService;

    invoke-static {v0}, Lcom/msc/c/e;->h(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_3

    const-string v2, "Y"

    invoke-virtual {v1, v2}, Lcom/msc/a/h;->b(Ljava/lang/String;)V

    invoke-virtual {v1, v0}, Lcom/msc/a/h;->c(Ljava/lang/String;)V

    :cond_3
    invoke-static {}, Lcom/msc/c/g;->a()Lcom/msc/c/g;

    iget-object v0, p0, Lcom/osp/app/signin/fr;->c:Lcom/osp/app/signin/InterfaceService;

    invoke-static {v0, v1, p0}, Lcom/msc/c/g;->a(Landroid/content/Context;Lcom/msc/a/h;Lcom/msc/b/h;)J

    move-result-wide v0

    iput-wide v0, p0, Lcom/osp/app/signin/fr;->q:J

    iget-wide v0, p0, Lcom/osp/app/signin/fr;->q:J

    invoke-virtual {p0, v0, v1}, Lcom/osp/app/signin/fr;->a(J)V

    iget-wide v0, p0, Lcom/osp/app/signin/fr;->q:J

    const-string v2, "from_xml"

    invoke-virtual {p0, v0, v1, v2}, Lcom/osp/app/signin/fr;->a(JLjava/lang/String;)V

    iget-wide v0, p0, Lcom/osp/app/signin/fr;->q:J

    sget-object v2, Lcom/msc/b/g;->c:Lcom/msc/b/g;

    invoke-static {v0, v1, v2}, Lcom/msc/c/g;->a(JLcom/msc/b/g;)V

    goto/16 :goto_0

    .line 713
    :cond_4
    iget-object v0, p0, Lcom/osp/app/signin/fr;->c:Lcom/osp/app/signin/InterfaceService;

    invoke-static {v0}, Lcom/osp/app/util/ad;->a(Landroid/content/Context;)Z

    move-result v0

    if-ne v0, v4, :cond_5

    invoke-static {}, Lcom/osp/app/signin/SamsungService;->a()Z

    move-result v0

    if-nez v0, :cond_5

    .line 715
    invoke-static {}, Lcom/msc/sa/selfupdate/c;->b()Lcom/msc/sa/selfupdate/c;

    move-result-object v0

    iget-object v1, p0, Lcom/osp/app/signin/fr;->c:Lcom/osp/app/signin/InterfaceService;

    invoke-virtual {v1}, Lcom/osp/app/signin/InterfaceService;->getApplication()Landroid/app/Application;

    move-result-object v1

    iget-object v2, p0, Lcom/osp/app/signin/fr;->f:Ljava/lang/String;

    invoke-virtual {v0, v1, v4, v4, v2}, Lcom/msc/sa/selfupdate/c;->a(Landroid/app/Application;ZZLjava/lang/String;)V

    .line 719
    :cond_5
    invoke-static {}, Lcom/msc/sa/selfupdate/c;->b()Lcom/msc/sa/selfupdate/c;

    move-result-object v0

    invoke-virtual {v0}, Lcom/msc/sa/selfupdate/c;->f()Z

    move-result v0

    if-ne v0, v4, :cond_6

    .line 721
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v0, "Self upgrade is necessary after startSelfUpgrade"

    invoke-static {v0}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;)V

    .line 722
    invoke-static {}, Lcom/osp/app/util/e;->a()Lcom/osp/app/util/e;

    move-result-object v0

    iget-object v1, p0, Lcom/osp/app/signin/fr;->c:Lcom/osp/app/signin/InterfaceService;

    const-string v2, "The upgrade process must be completed if you use Samsung account"

    invoke-virtual {v0, v1, v2}, Lcom/osp/app/util/e;->a(Landroid/content/Context;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 728
    :cond_6
    new-instance v1, Lcom/msc/a/c;

    invoke-direct {v1}, Lcom/msc/a/c;-><init>()V

    invoke-static {}, Lcom/osp/device/b;->b()Lcom/osp/device/b;

    move-result-object v0

    invoke-virtual {v0}, Lcom/osp/device/b;->a()Z

    move-result v0

    if-eqz v0, :cond_7

    iget-object v0, p0, Lcom/osp/app/signin/fr;->c:Lcom/osp/app/signin/InterfaceService;

    invoke-static {v0}, Lcom/osp/app/util/ad;->h(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/telephony/PhoneNumberUtils;->isGlobalPhoneNumber(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_7

    iget-object v0, p0, Lcom/osp/app/signin/fr;->c:Lcom/osp/app/signin/InterfaceService;

    invoke-static {v0}, Lcom/msc/c/e;->h(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_7

    invoke-virtual {v1}, Lcom/msc/a/c;->g()V

    invoke-virtual {v1, v0}, Lcom/msc/a/c;->b(Ljava/lang/String;)V

    :cond_7
    invoke-virtual {v1, v4}, Lcom/msc/a/c;->a(Z)V

    invoke-virtual {v1, v4}, Lcom/msc/a/c;->b(Z)V

    iget-object v0, p0, Lcom/osp/app/signin/fr;->d:Ljava/lang/String;

    invoke-virtual {v1, v0}, Lcom/msc/a/c;->d(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/osp/app/signin/fr;->e:Ljava/lang/String;

    invoke-virtual {v1, v0}, Lcom/msc/a/c;->f(Ljava/lang/String;)V

    invoke-virtual {v1}, Lcom/msc/a/c;->b()V

    invoke-virtual {v1}, Lcom/msc/a/c;->a()V

    invoke-virtual {v1}, Lcom/msc/a/c;->f()V

    invoke-virtual {v1}, Lcom/msc/a/c;->e()V

    iget-object v0, p0, Lcom/osp/app/signin/fr;->c:Lcom/osp/app/signin/InterfaceService;

    invoke-static {v0}, Lcom/osp/app/util/ad;->h(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Lcom/msc/a/c;->a(Ljava/lang/String;)V

    invoke-static {}, Lcom/osp/common/util/i;->a()Lcom/osp/common/util/i;

    iget-object v0, p0, Lcom/osp/app/signin/fr;->c:Lcom/osp/app/signin/InterfaceService;

    invoke-static {v0}, Lcom/osp/common/util/i;->l(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v0

    sget-object v2, Ljava/util/Locale;->ENGLISH:Ljava/util/Locale;

    invoke-virtual {v0, v2}, Ljava/lang/String;->toUpperCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Lcom/msc/a/c;->c(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/osp/app/signin/fr;->d:Ljava/lang/String;

    invoke-virtual {v1, v0}, Lcom/msc/a/c;->e(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/osp/app/signin/fr;->w:Ljava/lang/String;

    invoke-virtual {v1, v0}, Lcom/msc/a/c;->j(Ljava/lang/String;)V

    invoke-virtual {v1}, Lcom/msc/a/c;->c()V

    invoke-virtual {v1}, Lcom/msc/a/c;->d()V

    iget-object v0, p0, Lcom/osp/app/signin/fr;->f:Ljava/lang/String;

    invoke-virtual {v1, v0}, Lcom/msc/a/c;->k(Ljava/lang/String;)V

    :try_start_0
    iget-object v0, p0, Lcom/osp/app/signin/fr;->c:Lcom/osp/app/signin/InterfaceService;

    invoke-static {v0}, Lcom/osp/device/d;->a(Landroid/content/Context;)Lcom/osp/device/a;

    move-result-object v0

    invoke-virtual {v0}, Lcom/osp/device/a;->c()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Lcom/msc/a/c;->g(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    :goto_1
    const-string v0, "M"

    invoke-virtual {v1, v0}, Lcom/msc/a/c;->h(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/osp/app/signin/fr;->c:Lcom/osp/app/signin/InterfaceService;

    invoke-static {v0}, Lcom/msc/c/e;->g(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Lcom/msc/a/c;->i(Ljava/lang/String;)V

    invoke-static {}, Lcom/msc/c/g;->a()Lcom/msc/c/g;

    iget-object v0, p0, Lcom/osp/app/signin/fr;->c:Lcom/osp/app/signin/InterfaceService;

    invoke-static {v0, v1, p0}, Lcom/msc/c/g;->a(Landroid/content/Context;Lcom/msc/a/c;Lcom/msc/b/h;)J

    move-result-wide v0

    iput-wide v0, p0, Lcom/osp/app/signin/fr;->t:J

    iget-wide v0, p0, Lcom/osp/app/signin/fr;->t:J

    invoke-virtual {p0, v0, v1}, Lcom/osp/app/signin/fr;->a(J)V

    iget-wide v0, p0, Lcom/osp/app/signin/fr;->t:J

    const-string v2, "from_json"

    invoke-virtual {p0, v0, v1, v2}, Lcom/osp/app/signin/fr;->a(JLjava/lang/String;)V

    iget-wide v0, p0, Lcom/osp/app/signin/fr;->t:J

    sget-object v2, Lcom/msc/b/g;->c:Lcom/msc/b/g;

    invoke-static {v0, v1, v2}, Lcom/msc/c/g;->a(JLcom/msc/b/g;)V

    goto/16 :goto_0

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_1
.end method

.method private j()Z
    .locals 13

    .prologue
    const/4 v1, 0x1

    const/4 v12, 0x0

    const/4 v0, 0x0

    const-wide/16 v10, 0x3e8

    const-wide/16 v8, 0x0

    .line 972
    iget-object v2, p0, Lcom/osp/app/signin/fr;->c:Lcom/osp/app/signin/InterfaceService;

    sget-object v3, Lcom/osp/app/util/af;->c:Lcom/osp/app/util/af;

    invoke-static {v2, v3, v0}, Lcom/osp/app/util/ad;->a(Landroid/content/Context;Lcom/osp/app/util/af;Z)Z

    move-result v2

    if-nez v2, :cond_3

    iget-object v2, p0, Lcom/osp/app/signin/fr;->c:Lcom/osp/app/signin/InterfaceService;

    iget-object v3, p0, Lcom/osp/app/signin/fr;->d:Ljava/lang/String;

    invoke-static {v2, v3}, Lcom/osp/app/util/ad;->c(Landroid/content/Context;Ljava/lang/String;)Landroid/os/Bundle;

    move-result-object v2

    iget-object v3, p0, Lcom/osp/app/signin/fr;->c:Lcom/osp/app/signin/InterfaceService;

    invoke-static {v3}, Lcom/osp/app/util/ad;->k(Landroid/content/Context;)Z

    move-result v3

    iput-boolean v3, p0, Lcom/osp/app/signin/fr;->v:Z

    if-eqz v2, :cond_1

    const-string v3, "key_accesstoken"

    invoke-virtual {v2, v3}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    iput-object v3, p0, Lcom/osp/app/signin/fr;->l:Ljava/lang/String;

    const-string v3, "key_accesstoken_expires_in"

    invoke-virtual {v2, v3, v8, v9}, Landroid/os/Bundle;->getLong(Ljava/lang/String;J)J

    move-result-wide v4

    iput-wide v4, p0, Lcom/osp/app/signin/fr;->n:J

    const-string v3, "key_refreshtoken"

    invoke-virtual {v2, v3}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    iput-object v3, p0, Lcom/osp/app/signin/fr;->m:Ljava/lang/String;

    const-string v3, "key_refreshtoken_expires_in"

    invoke-virtual {v2, v3, v8, v9}, Landroid/os/Bundle;->getLong(Ljava/lang/String;J)J

    move-result-wide v4

    iput-wide v4, p0, Lcom/osp/app/signin/fr;->o:J

    const-string v3, "key_accesstoken_issued_time"

    invoke-virtual {v2, v3, v8, v9}, Landroid/os/Bundle;->getLong(Ljava/lang/String;J)J

    move-result-wide v2

    iput-wide v2, p0, Lcom/osp/app/signin/fr;->p:J

    move v2, v1

    :goto_0
    if-eqz v2, :cond_3

    .line 974
    iget-object v2, p0, Lcom/osp/app/signin/fr;->l:Ljava/lang/String;

    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_3

    iget-object v2, p0, Lcom/osp/app/signin/fr;->m:Ljava/lang/String;

    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_3

    .line 976
    iget-object v2, p0, Lcom/osp/app/signin/fr;->g:Ljava/lang/String;

    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 978
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    div-long/2addr v2, v10

    .line 980
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "curTimeSec = "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;)V

    .line 981
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "accessTokenIssuedTime =  "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-wide v6, p0, Lcom/osp/app/signin/fr;->p:J

    invoke-virtual {v4, v6, v7}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;)V

    .line 982
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "accessTokenIssuedTime/1000 =  "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-wide v6, p0, Lcom/osp/app/signin/fr;->p:J

    div-long/2addr v6, v10

    invoke-virtual {v4, v6, v7}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;)V

    .line 983
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "AccessTokenExpiresIn = "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-wide v6, p0, Lcom/osp/app/signin/fr;->n:J

    invoke-virtual {v4, v6, v7}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;)V

    .line 984
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "TimeGap = "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-wide v6, p0, Lcom/osp/app/signin/fr;->p:J

    div-long/2addr v6, v10

    sub-long v6, v2, v6

    invoke-virtual {v4, v6, v7}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;)V

    .line 985
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v4, "SIS"

    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "expiredAccessToken is Empty. Check Token issuedTime. TimeGap = "

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-wide v6, p0, Lcom/osp/app/signin/fr;->p:J

    div-long/2addr v6, v10

    sub-long v6, v2, v6

    invoke-virtual {v5, v6, v7}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 987
    iget-wide v4, p0, Lcom/osp/app/signin/fr;->p:J

    cmp-long v4, v4, v8

    if-eqz v4, :cond_0

    iget-wide v4, p0, Lcom/osp/app/signin/fr;->p:J

    div-long/2addr v4, v10

    sub-long/2addr v2, v4

    iget-wide v4, p0, Lcom/osp/app/signin/fr;->n:J

    cmp-long v2, v2, v4

    if-lez v2, :cond_2

    .line 989
    :cond_0
    iget-object v1, p0, Lcom/osp/app/signin/fr;->c:Lcom/osp/app/signin/InterfaceService;

    iget-object v2, p0, Lcom/osp/app/signin/fr;->d:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/osp/app/util/ad;->d(Landroid/content/Context;Ljava/lang/String;)V

    .line 990
    iput-object v12, p0, Lcom/osp/app/signin/fr;->l:Ljava/lang/String;

    .line 991
    iput-object v12, p0, Lcom/osp/app/signin/fr;->m:Ljava/lang/String;

    .line 992
    iput-wide v8, p0, Lcom/osp/app/signin/fr;->n:J

    .line 993
    iput-wide v8, p0, Lcom/osp/app/signin/fr;->o:J

    .line 994
    iput-wide v8, p0, Lcom/osp/app/signin/fr;->p:J

    .line 1017
    :goto_1
    return v0

    :cond_1
    move v2, v0

    .line 972
    goto/16 :goto_0

    .line 999
    :cond_2
    iget-object v2, p0, Lcom/osp/app/signin/fr;->l:Ljava/lang/String;

    iget-object v3, p0, Lcom/osp/app/signin/fr;->g:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_3

    iget-boolean v2, p0, Lcom/osp/app/signin/fr;->v:Z

    if-eqz v2, :cond_3

    move v0, v1

    .line 1004
    goto :goto_1

    .line 1010
    :cond_3
    iget-object v1, p0, Lcom/osp/app/signin/fr;->c:Lcom/osp/app/signin/InterfaceService;

    iget-object v2, p0, Lcom/osp/app/signin/fr;->d:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/osp/app/util/ad;->d(Landroid/content/Context;Ljava/lang/String;)V

    .line 1011
    iput-object v12, p0, Lcom/osp/app/signin/fr;->l:Ljava/lang/String;

    .line 1012
    iput-object v12, p0, Lcom/osp/app/signin/fr;->m:Ljava/lang/String;

    .line 1013
    iput-wide v8, p0, Lcom/osp/app/signin/fr;->n:J

    .line 1014
    iput-wide v8, p0, Lcom/osp/app/signin/fr;->o:J

    .line 1015
    iput-wide v8, p0, Lcom/osp/app/signin/fr;->p:J

    goto :goto_1
.end method


# virtual methods
.method protected final varargs a()Ljava/lang/Boolean;
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 613
    invoke-static {}, Lcom/osp/device/b;->b()Lcom/osp/device/b;

    move-result-object v0

    invoke-virtual {v0}, Lcom/osp/device/b;->l()I

    move-result v0

    const/16 v1, 0x10

    if-le v0, v1, :cond_2

    .line 618
    iget-object v0, p0, Lcom/osp/app/signin/fr;->c:Lcom/osp/app/signin/InterfaceService;

    invoke-static {v0}, Lcom/osp/app/signin/SamsungService;->a(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v0

    .line 619
    if-eqz v0, :cond_1

    .line 623
    :try_start_0
    invoke-static {}, Lcom/msc/c/h;->a()Lcom/msc/c/h;

    invoke-static {v0}, Lcom/msc/c/h;->A(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v0

    .line 625
    invoke-static {}, Lcom/msc/c/h;->a()Lcom/msc/c/h;

    iget-object v1, p0, Lcom/osp/app/signin/fr;->c:Lcom/osp/app/signin/InterfaceService;

    iget-object v2, p0, Lcom/osp/app/signin/fr;->d:Ljava/lang/String;

    iget-object v3, p0, Lcom/osp/app/signin/fr;->f:Ljava/lang/String;

    invoke-static {v1, v0, v2, v3}, Lcom/msc/c/h;->a(Landroid/content/Context;Ljava/util/ArrayList;Ljava/lang/String;Ljava/lang/String;)Z

    move-result v0

    .line 626
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v1, "SIS"

    const-string v2, "Use cached signature"

    invoke-static {v1, v2}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 628
    if-eqz v0, :cond_0

    .line 630
    invoke-direct {p0}, Lcom/osp/app/signin/fr;->i()V

    .line 631
    const/4 v0, 0x0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    .line 649
    :goto_0
    return-object v0

    .line 633
    :cond_0
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v0, "SIS"

    const-string v1, "Not matched cache data of signature"

    invoke-static {v0, v1}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 642
    :cond_1
    :goto_1
    invoke-static {}, Lcom/msc/c/g;->a()Lcom/msc/c/g;

    iget-object v0, p0, Lcom/osp/app/signin/fr;->c:Lcom/osp/app/signin/InterfaceService;

    invoke-static {v0, p0}, Lcom/msc/c/g;->c(Landroid/content/Context;Lcom/msc/b/h;)J

    move-result-wide v0

    iput-wide v0, p0, Lcom/osp/app/signin/fr;->s:J

    iget-wide v0, p0, Lcom/osp/app/signin/fr;->s:J

    invoke-virtual {p0, v0, v1}, Lcom/osp/app/signin/fr;->a(J)V

    iget-wide v0, p0, Lcom/osp/app/signin/fr;->s:J

    sget-object v2, Lcom/msc/b/g;->b:Lcom/msc/b/g;

    invoke-static {v0, v1, v2}, Lcom/msc/c/g;->a(JLcom/msc/b/g;)V

    .line 649
    :goto_2
    invoke-static {v4}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    goto :goto_0

    .line 634
    :catch_0
    move-exception v0

    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_1

    .line 646
    :cond_2
    invoke-direct {p0}, Lcom/osp/app/signin/fr;->i()V

    goto :goto_2
.end method

.method public final declared-synchronized a(Lcom/msc/b/c;)V
    .locals 6

    .prologue
    .line 838
    monitor-enter p0

    :try_start_0
    invoke-super {p0, p1}, Lcom/msc/c/b;->a(Lcom/msc/b/c;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 840
    if-nez p1, :cond_1

    .line 941
    :cond_0
    :goto_0
    monitor-exit p0

    return-void

    .line 845
    :cond_1
    :try_start_1
    invoke-virtual {p1}, Lcom/msc/b/c;->a()J

    move-result-wide v0

    .line 846
    invoke-virtual {p1}, Lcom/msc/b/c;->e()Ljava/lang/String;

    move-result-object v2

    .line 848
    iget-wide v4, p0, Lcom/osp/app/signin/fr;->q:J
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    cmp-long v3, v0, v4

    if-nez v3, :cond_3

    .line 852
    :try_start_2
    iget-object v0, p0, Lcom/osp/app/signin/fr;->c:Lcom/osp/app/signin/InterfaceService;

    new-instance v1, Lcom/msc/a/i;

    invoke-direct {v1}, Lcom/msc/a/i;-><init>()V

    invoke-static {v0, v1}, Lcom/osp/app/signin/InterfaceService;->a(Lcom/osp/app/signin/InterfaceService;Lcom/msc/a/i;)Lcom/msc/a/i;

    .line 853
    invoke-static {}, Lcom/msc/c/h;->a()Lcom/msc/c/h;

    iget-object v0, p0, Lcom/osp/app/signin/fr;->c:Lcom/osp/app/signin/InterfaceService;

    iget-object v1, p0, Lcom/osp/app/signin/fr;->d:Ljava/lang/String;

    iget-object v3, p0, Lcom/osp/app/signin/fr;->c:Lcom/osp/app/signin/InterfaceService;

    invoke-static {v3}, Lcom/osp/app/signin/InterfaceService;->a(Lcom/osp/app/signin/InterfaceService;)Lcom/msc/a/i;

    move-result-object v3

    invoke-static {v0, v1, v2, v3}, Lcom/msc/c/h;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Lcom/msc/a/i;)V

    .line 855
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v0, "requestAuthenticationV02 called"

    invoke-static {v0}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/osp/app/signin/fr;->c:Lcom/osp/app/signin/InterfaceService;

    invoke-static {v0}, Lcom/osp/app/signin/InterfaceService;->a(Lcom/osp/app/signin/InterfaceService;)Lcom/msc/a/i;

    move-result-object v0

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/osp/app/signin/fr;->c:Lcom/osp/app/signin/InterfaceService;

    iget-object v0, p0, Lcom/osp/app/signin/fr;->c:Lcom/osp/app/signin/InterfaceService;

    invoke-static {v0}, Lcom/osp/app/signin/InterfaceService;->a(Lcom/osp/app/signin/InterfaceService;)Lcom/msc/a/i;

    move-result-object v0

    invoke-static {v0}, Lcom/osp/app/signin/InterfaceService;->a(Lcom/msc/a/i;)I

    move-result v0

    if-lez v0, :cond_2

    iget-object v0, p0, Lcom/osp/app/signin/fr;->c:Lcom/osp/app/signin/InterfaceService;

    invoke-static {v0}, Lcom/osp/app/signin/InterfaceService;->a(Lcom/osp/app/signin/InterfaceService;)Lcom/msc/a/i;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/osp/app/signin/fr;->a(Lcom/msc/a/i;)V

    new-instance v0, Ljava/lang/Exception;

    const-string v1, "The certification process must be completed before you can use your Samsung account"

    invoke-direct {v0, v1}, Ljava/lang/Exception;-><init>(Ljava/lang/String;)V

    throw v0
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_0
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 857
    :catch_0
    move-exception v0

    .line 859
    :try_start_3
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    .line 860
    new-instance v1, Lcom/msc/c/f;

    invoke-direct {v1, v0}, Lcom/msc/c/f;-><init>(Ljava/lang/Exception;)V

    iput-object v1, p0, Lcom/osp/app/signin/fr;->b:Lcom/msc/c/f;
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    goto :goto_0

    .line 838
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 855
    :cond_2
    :try_start_4
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v0, "SIS"

    const-string v1, "reuse accessToken from DB"

    invoke-static {v0, v1}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_4
    .catch Ljava/lang/Exception; {:try_start_4 .. :try_end_4} :catch_0
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    goto :goto_0

    .line 862
    :cond_3
    :try_start_5
    iget-wide v4, p0, Lcom/osp/app/signin/fr;->r:J
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    cmp-long v3, v0, v4

    if-nez v3, :cond_4

    .line 866
    :try_start_6
    new-instance v0, Lcom/msc/a/d;

    invoke-direct {v0}, Lcom/msc/a/d;-><init>()V

    .line 867
    invoke-static {}, Lcom/msc/c/h;->a()Lcom/msc/c/h;

    invoke-static {v2, v0}, Lcom/msc/c/h;->a(Ljava/lang/String;Lcom/msc/a/d;)V

    .line 868
    invoke-virtual {v0}, Lcom/msc/a/d;->g()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/osp/app/signin/fr;->l:Ljava/lang/String;

    .line 869
    invoke-virtual {v0}, Lcom/msc/a/d;->j()J

    move-result-wide v2

    iput-wide v2, p0, Lcom/osp/app/signin/fr;->n:J

    .line 870
    invoke-virtual {v0}, Lcom/msc/a/d;->h()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/osp/app/signin/fr;->m:Ljava/lang/String;

    .line 871
    invoke-virtual {v0}, Lcom/msc/a/d;->k()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/osp/app/signin/fr;->o:J
    :try_end_6
    .catch Ljava/lang/Exception; {:try_start_6 .. :try_end_6} :catch_1
    .catchall {:try_start_6 .. :try_end_6} :catchall_0

    goto/16 :goto_0

    .line 873
    :catch_1
    move-exception v0

    .line 875
    :try_start_7
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    .line 876
    new-instance v1, Lcom/msc/c/f;

    invoke-direct {v1, v0}, Lcom/msc/c/f;-><init>(Ljava/lang/Exception;)V

    iput-object v1, p0, Lcom/osp/app/signin/fr;->b:Lcom/msc/c/f;

    goto/16 :goto_0

    .line 878
    :cond_4
    iget-wide v4, p0, Lcom/osp/app/signin/fr;->s:J
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_0

    cmp-long v3, v0, v4

    if-nez v3, :cond_6

    .line 885
    :try_start_8
    iget-object v0, p0, Lcom/osp/app/signin/fr;->c:Lcom/osp/app/signin/InterfaceService;

    invoke-static {v0, v2}, Lcom/osp/app/signin/SamsungService;->a(Landroid/content/Context;Ljava/lang/String;)V

    .line 886
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v0, "SIS"

    const-string v1, "Save Signature"

    invoke-static {v0, v1}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 889
    invoke-static {}, Lcom/msc/c/h;->a()Lcom/msc/c/h;

    invoke-static {v2}, Lcom/msc/c/h;->A(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v0

    .line 891
    invoke-static {}, Lcom/msc/c/h;->a()Lcom/msc/c/h;

    iget-object v1, p0, Lcom/osp/app/signin/fr;->c:Lcom/osp/app/signin/InterfaceService;

    iget-object v2, p0, Lcom/osp/app/signin/fr;->d:Ljava/lang/String;

    iget-object v3, p0, Lcom/osp/app/signin/fr;->f:Ljava/lang/String;

    invoke-static {v1, v0, v2, v3}, Lcom/msc/c/h;->a(Landroid/content/Context;Ljava/util/ArrayList;Ljava/lang/String;Ljava/lang/String;)Z

    move-result v0

    .line 893
    if-eqz v0, :cond_5

    .line 895
    invoke-direct {p0}, Lcom/osp/app/signin/fr;->i()V
    :try_end_8
    .catch Ljava/lang/Exception; {:try_start_8 .. :try_end_8} :catch_2
    .catchall {:try_start_8 .. :try_end_8} :catchall_0

    goto/16 :goto_0

    .line 901
    :catch_2
    move-exception v0

    :try_start_9
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    .line 905
    invoke-direct {p0}, Lcom/osp/app/signin/fr;->i()V
    :try_end_9
    .catchall {:try_start_9 .. :try_end_9} :catchall_0

    goto/16 :goto_0

    .line 898
    :cond_5
    :try_start_a
    new-instance v0, Lcom/msc/c/f;

    const-string v1, "The signature of this application is not registered with the server."

    invoke-direct {v0, v1}, Lcom/msc/c/f;-><init>(Ljava/lang/String;)V

    iput-object v0, p0, Lcom/osp/app/signin/fr;->b:Lcom/msc/c/f;
    :try_end_a
    .catch Ljava/lang/Exception; {:try_start_a .. :try_end_a} :catch_2
    .catchall {:try_start_a .. :try_end_a} :catchall_0

    goto/16 :goto_0

    .line 907
    :cond_6
    :try_start_b
    iget-wide v4, p0, Lcom/osp/app/signin/fr;->t:J
    :try_end_b
    .catchall {:try_start_b .. :try_end_b} :catchall_0

    cmp-long v0, v0, v4

    if-nez v0, :cond_0

    .line 912
    :try_start_c
    new-instance v0, Lcom/msc/a/d;

    invoke-direct {v0}, Lcom/msc/a/d;-><init>()V

    .line 913
    new-instance v1, Lcom/msc/a/i;

    invoke-direct {v1}, Lcom/msc/a/i;-><init>()V

    .line 914
    invoke-static {}, Lcom/msc/c/h;->a()Lcom/msc/c/h;

    iget-object v3, p0, Lcom/osp/app/signin/fr;->c:Lcom/osp/app/signin/InterfaceService;

    iget-object v4, p0, Lcom/osp/app/signin/fr;->d:Ljava/lang/String;

    invoke-static {v3, v4, v2, v0, v1}, Lcom/msc/c/h;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Lcom/msc/a/d;Lcom/msc/a/i;)V

    .line 916
    iget-object v2, p0, Lcom/osp/app/signin/fr;->c:Lcom/osp/app/signin/InterfaceService;

    invoke-static {v1}, Lcom/osp/app/signin/InterfaceService;->a(Lcom/msc/a/i;)I

    move-result v2

    if-lez v2, :cond_7

    .line 918
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v0, "SIS"

    const-string v2, "preprocess is not completed!"

    invoke-static {v0, v2}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 919
    invoke-direct {p0, v1}, Lcom/osp/app/signin/fr;->a(Lcom/msc/a/i;)V

    .line 921
    new-instance v0, Ljava/lang/Exception;

    const-string v1, "The certification process must be completed before you can use your Samsung account"

    invoke-direct {v0, v1}, Ljava/lang/Exception;-><init>(Ljava/lang/String;)V

    throw v0
    :try_end_c
    .catch Ljava/lang/Exception; {:try_start_c .. :try_end_c} :catch_3
    .catchall {:try_start_c .. :try_end_c} :catchall_0

    .line 935
    :catch_3
    move-exception v0

    .line 937
    :try_start_d
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    .line 938
    new-instance v1, Lcom/msc/c/f;

    invoke-direct {v1, v0}, Lcom/msc/c/f;-><init>(Ljava/lang/Exception;)V

    iput-object v1, p0, Lcom/osp/app/signin/fr;->b:Lcom/msc/c/f;
    :try_end_d
    .catchall {:try_start_d .. :try_end_d} :catchall_0

    goto/16 :goto_0

    .line 924
    :cond_7
    :try_start_e
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v1, "SIS"

    const-string v2, "preprocess is completed!"

    invoke-static {v1, v2}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 926
    invoke-virtual {v0}, Lcom/msc/a/d;->g()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/osp/app/signin/fr;->l:Ljava/lang/String;

    .line 927
    invoke-virtual {v0}, Lcom/msc/a/d;->j()J

    move-result-wide v2

    iput-wide v2, p0, Lcom/osp/app/signin/fr;->n:J

    .line 928
    invoke-virtual {v0}, Lcom/msc/a/d;->h()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/osp/app/signin/fr;->m:Ljava/lang/String;

    .line 929
    invoke-virtual {v0}, Lcom/msc/a/d;->k()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/osp/app/signin/fr;->o:J

    .line 933
    iget-object v0, p0, Lcom/osp/app/signin/fr;->c:Lcom/osp/app/signin/InterfaceService;

    sget-object v1, Lcom/osp/app/util/af;->c:Lcom/osp/app/util/af;

    invoke-static {v0, v1}, Lcom/osp/app/util/ad;->a(Landroid/content/Context;Lcom/osp/app/util/af;)V
    :try_end_e
    .catch Ljava/lang/Exception; {:try_start_e .. :try_end_e} :catch_3
    .catchall {:try_start_e .. :try_end_e} :catchall_0

    goto/16 :goto_0
.end method

.method protected final a(Ljava/lang/Boolean;)V
    .locals 8

    .prologue
    const/4 v4, 0x1

    .line 736
    invoke-super {p0, p1}, Lcom/msc/c/b;->a(Ljava/lang/Boolean;)V

    .line 738
    iget-object v0, p0, Lcom/osp/app/signin/fr;->b:Lcom/msc/c/f;

    if-eqz v0, :cond_3

    .line 740
    invoke-static {}, Lcom/osp/device/b;->b()Lcom/osp/device/b;

    move-result-object v0

    invoke-virtual {v0}, Lcom/osp/device/b;->a()Z

    move-result v0

    if-eqz v0, :cond_2

    const-string v0, "USR_3174"

    iget-object v1, p0, Lcom/osp/app/signin/fr;->b:Lcom/msc/c/f;

    invoke-virtual {v1}, Lcom/msc/c/f;->a()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 742
    iget-object v0, p0, Lcom/osp/app/signin/fr;->c:Lcom/osp/app/signin/InterfaceService;

    iget-object v1, p0, Lcom/osp/app/signin/fr;->d:Ljava/lang/String;

    iget-object v2, p0, Lcom/osp/app/signin/fr;->e:Ljava/lang/String;

    const-string v3, "OSP_02"

    invoke-static {v0, v1, v2, v3}, Lcom/osp/app/signin/InterfaceService;->a(Lcom/osp/app/signin/InterfaceService;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 761
    :cond_0
    :goto_0
    iget-object v0, p0, Lcom/osp/app/signin/fr;->b:Lcom/msc/c/f;

    invoke-virtual {v0}, Lcom/msc/c/f;->b()Ljava/lang/String;

    move-result-object v0

    .line 763
    new-instance v1, Landroid/content/Intent;

    const-string v2, "com.msc.action.ACCESSTOKEN_V02_RESPONSE"

    invoke-direct {v1, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 764
    const-string v2, "result_code"

    invoke-virtual {v1, v2, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 765
    const-string v2, "error_message"

    invoke-virtual {v1, v2, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 766
    iget-object v2, p0, Lcom/osp/app/signin/fr;->f:Ljava/lang/String;

    invoke-virtual {v1, v2}, Landroid/content/Intent;->setPackage(Ljava/lang/String;)Landroid/content/Intent;

    .line 769
    const-string v2, "client_id"

    iget-object v3, p0, Lcom/osp/app/signin/fr;->d:Ljava/lang/String;

    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 771
    const-string v2, "The certification process must be completed before you can use your Samsung account"

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/osp/app/signin/fr;->x:Landroid/content/Intent;

    if-eqz v0, :cond_1

    .line 773
    iget-object v0, p0, Lcom/osp/app/signin/fr;->x:Landroid/content/Intent;

    invoke-virtual {v0}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    .line 775
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_1

    .line 777
    iget-object v2, p0, Lcom/osp/app/signin/fr;->x:Landroid/content/Intent;

    invoke-virtual {v2}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v2

    .line 779
    const-string v3, "REQUIRED_PROCESS_ACTION"

    invoke-virtual {v1, v3, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 780
    const-string v0, "key_return_result"

    invoke-virtual {v1, v0, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 781
    if-eqz v2, :cond_1

    .line 783
    invoke-virtual {v1, v2}, Landroid/content/Intent;->putExtras(Landroid/os/Bundle;)Landroid/content/Intent;

    .line 789
    :cond_1
    invoke-static {}, Lcom/osp/app/util/e;->a()Lcom/osp/app/util/e;

    move-result-object v0

    iget-object v2, p0, Lcom/osp/app/signin/fr;->c:Lcom/osp/app/signin/InterfaceService;

    iget-wide v4, p0, Lcom/osp/app/signin/fr;->y:J

    invoke-virtual {v0, v2, v1, v4, v5}, Lcom/osp/app/util/e;->a(Landroid/content/Context;Landroid/content/Intent;J)V

    .line 791
    iget-object v0, p0, Lcom/osp/app/signin/fr;->c:Lcom/osp/app/signin/InterfaceService;

    iget v1, p0, Lcom/osp/app/signin/fr;->k:I

    invoke-virtual {v0, v1}, Lcom/osp/app/signin/InterfaceService;->stopSelf(I)V

    .line 834
    :goto_1
    return-void

    .line 743
    :cond_2
    const-string v0, "USR_3113"

    iget-object v1, p0, Lcom/osp/app/signin/fr;->b:Lcom/msc/c/f;

    invoke-virtual {v1}, Lcom/msc/c/f;->a()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-static {}, Lcom/osp/device/b;->b()Lcom/osp/device/b;

    move-result-object v0

    invoke-virtual {v0}, Lcom/osp/device/b;->l()I

    move-result v0

    const/16 v1, 0x10

    if-le v0, v1, :cond_0

    .line 745
    iget-object v0, p0, Lcom/osp/app/signin/fr;->c:Lcom/osp/app/signin/InterfaceService;

    invoke-static {v0}, Lcom/osp/app/util/ad;->i(Landroid/content/Context;)V

    .line 751
    new-instance v0, Lcom/osp/app/signin/fs;

    invoke-direct {v0, p0}, Lcom/osp/app/signin/fs;-><init>(Lcom/osp/app/signin/fr;)V

    const/4 v1, 0x0

    const-wide/16 v2, 0x3e8

    invoke-virtual {v0, v1, v2, v3}, Lcom/osp/app/signin/fs;->sendEmptyMessageDelayed(IJ)Z

    goto/16 :goto_0

    .line 795
    :cond_3
    iget-object v0, p0, Lcom/osp/app/signin/fr;->l:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_5

    iget-object v0, p0, Lcom/osp/app/signin/fr;->m:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_5

    .line 799
    iget-boolean v0, p0, Lcom/osp/app/signin/fr;->u:Z

    if-nez v0, :cond_4

    .line 801
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v0, "SIS"

    const-string v1, "Token cannot reusable. Save token"

    invoke-static {v0, v1}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 802
    iget-object v0, p0, Lcom/osp/app/signin/fr;->c:Lcom/osp/app/signin/InterfaceService;

    iget-object v1, p0, Lcom/osp/app/signin/fr;->d:Ljava/lang/String;

    iget-object v2, p0, Lcom/osp/app/signin/fr;->l:Ljava/lang/String;

    iget-wide v3, p0, Lcom/osp/app/signin/fr;->n:J

    iget-object v5, p0, Lcom/osp/app/signin/fr;->m:Ljava/lang/String;

    iget-wide v6, p0, Lcom/osp/app/signin/fr;->o:J

    invoke-static/range {v0 .. v7}, Lcom/osp/app/util/ad;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;JLjava/lang/String;J)V

    .line 808
    :cond_4
    new-instance v0, Landroid/content/Intent;

    const-string v1, "com.msc.action.ACCESSTOKEN_V02_RESPONSE"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 809
    const-string v1, "result_code"

    const/4 v2, -0x1

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 810
    const-string v1, "access_token"

    iget-object v2, p0, Lcom/osp/app/signin/fr;->l:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 811
    iget-object v1, p0, Lcom/osp/app/signin/fr;->h:[Ljava/lang/String;

    invoke-direct {p0, v1, v0}, Lcom/osp/app/signin/fr;->a([Ljava/lang/String;Landroid/content/Intent;)V

    .line 812
    iget-object v1, p0, Lcom/osp/app/signin/fr;->f:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setPackage(Ljava/lang/String;)Landroid/content/Intent;

    .line 815
    const-string v1, "client_id"

    iget-object v2, p0, Lcom/osp/app/signin/fr;->d:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 818
    invoke-static {}, Lcom/osp/app/util/e;->a()Lcom/osp/app/util/e;

    move-result-object v1

    iget-object v2, p0, Lcom/osp/app/signin/fr;->c:Lcom/osp/app/signin/InterfaceService;

    iget-wide v4, p0, Lcom/osp/app/signin/fr;->y:J

    invoke-virtual {v1, v2, v0, v4, v5}, Lcom/osp/app/util/e;->a(Landroid/content/Context;Landroid/content/Intent;J)V

    .line 833
    :goto_2
    iget-object v0, p0, Lcom/osp/app/signin/fr;->c:Lcom/osp/app/signin/InterfaceService;

    iget v1, p0, Lcom/osp/app/signin/fr;->k:I

    invoke-virtual {v0, v1}, Lcom/osp/app/signin/InterfaceService;->stopSelf(I)V

    goto/16 :goto_1

    .line 822
    :cond_5
    new-instance v0, Landroid/content/Intent;

    const-string v1, "com.msc.action.ACCESSTOKEN_V02_RESPONSE"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 823
    const-string v1, "result_code"

    invoke-virtual {v0, v1, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 826
    iget-object v1, p0, Lcom/osp/app/signin/fr;->f:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setPackage(Ljava/lang/String;)Landroid/content/Intent;

    .line 829
    const-string v1, "client_id"

    iget-object v2, p0, Lcom/osp/app/signin/fr;->d:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 831
    invoke-static {}, Lcom/osp/app/util/e;->a()Lcom/osp/app/util/e;

    move-result-object v1

    iget-object v2, p0, Lcom/osp/app/signin/fr;->c:Lcom/osp/app/signin/InterfaceService;

    iget-wide v4, p0, Lcom/osp/app/signin/fr;->y:J

    invoke-virtual {v1, v2, v0, v4, v5}, Lcom/osp/app/util/e;->a(Landroid/content/Context;Landroid/content/Intent;J)V

    goto :goto_2
.end method

.method public final b(Lcom/msc/b/c;)V
    .locals 6

    .prologue
    .line 1220
    invoke-super {p0, p1}, Lcom/msc/c/b;->b(Lcom/msc/b/c;)V

    .line 1222
    if-nez p1, :cond_1

    .line 1257
    :cond_0
    :goto_0
    return-void

    .line 1227
    :cond_1
    invoke-virtual {p1}, Lcom/msc/b/c;->a()J

    move-result-wide v0

    .line 1228
    invoke-virtual {p1}, Lcom/msc/b/c;->e()Ljava/lang/String;

    move-result-object v2

    .line 1233
    iget-wide v4, p0, Lcom/osp/app/signin/fr;->r:J

    cmp-long v3, v0, v4

    if-eqz v3, :cond_2

    iget-wide v4, p0, Lcom/osp/app/signin/fr;->t:J

    cmp-long v3, v0, v4

    if-nez v3, :cond_5

    .line 1235
    :cond_2
    if-eqz v2, :cond_0

    iget-object v0, p0, Lcom/osp/app/signin/fr;->b:Lcom/msc/c/f;

    if-eqz v0, :cond_0

    .line 1237
    const-string v0, "AUT_1302"

    iget-object v1, p0, Lcom/osp/app/signin/fr;->b:Lcom/msc/c/f;

    invoke-virtual {v1}, Lcom/msc/c/f;->a()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_3

    const-string v0, "AUT_1830"

    iget-object v1, p0, Lcom/osp/app/signin/fr;->b:Lcom/msc/c/f;

    invoke-virtual {v1}, Lcom/msc/c/f;->a()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 1239
    :cond_3
    iget-object v0, p0, Lcom/osp/app/signin/fr;->c:Lcom/osp/app/signin/InterfaceService;

    invoke-static {v0}, Lcom/msc/c/c;->g(Landroid/content/Context;)V

    .line 1240
    iget-object v0, p0, Lcom/osp/app/signin/fr;->c:Lcom/osp/app/signin/InterfaceService;

    invoke-static {v0}, Lcom/msc/c/e;->a(Landroid/content/Context;)Z

    .line 1241
    iget-object v0, p0, Lcom/osp/app/signin/fr;->c:Lcom/osp/app/signin/InterfaceService;

    iget-object v1, p0, Lcom/osp/app/signin/fr;->d:Ljava/lang/String;

    iget-object v2, p0, Lcom/osp/app/signin/fr;->e:Ljava/lang/String;

    const-string v3, "OSP_02"

    invoke-static {v0, v1, v2, v3}, Lcom/osp/app/signin/InterfaceService;->a(Lcom/osp/app/signin/InterfaceService;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 1242
    :cond_4
    invoke-static {}, Lcom/osp/device/b;->b()Lcom/osp/device/b;

    move-result-object v0

    invoke-virtual {v0}, Lcom/osp/device/b;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    const-string v0, "AUT_1094"

    iget-object v1, p0, Lcom/osp/app/signin/fr;->b:Lcom/msc/c/f;

    invoke-virtual {v1}, Lcom/msc/c/f;->a()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1244
    iget-object v0, p0, Lcom/osp/app/signin/fr;->c:Lcom/osp/app/signin/InterfaceService;

    iget-object v1, p0, Lcom/osp/app/signin/fr;->d:Ljava/lang/String;

    iget-object v2, p0, Lcom/osp/app/signin/fr;->e:Ljava/lang/String;

    invoke-static {v0, v1, v2}, Lcom/osp/app/signin/InterfaceService;->a(Lcom/osp/app/signin/InterfaceService;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 1247
    :cond_5
    iget-wide v2, p0, Lcom/osp/app/signin/fr;->s:J

    cmp-long v0, v0, v2

    if-nez v0, :cond_0

    .line 1254
    invoke-direct {p0}, Lcom/osp/app/signin/fr;->i()V

    goto :goto_0
.end method

.method protected final synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 509
    invoke-virtual {p0}, Lcom/osp/app/signin/fr;->a()Ljava/lang/Boolean;

    move-result-object v0

    return-object v0
.end method

.method protected final synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 509
    check-cast p1, Ljava/lang/Boolean;

    invoke-virtual {p0, p1}, Lcom/osp/app/signin/fr;->a(Ljava/lang/Boolean;)V

    return-void
.end method
