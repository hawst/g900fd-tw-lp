.class public final Lcom/osp/app/signin/c;
.super Ljava/lang/Object;
.source "AccountGLD.java"


# static fields
.field private static volatile a:Lcom/osp/app/signin/c;


# instance fields
.field private b:Ljava/lang/String;

.field private c:J

.field private d:Ljava/lang/String;

.field private e:Ljava/lang/String;

.field private f:I

.field private g:Ljava/lang/String;

.field private h:Ljava/lang/String;

.field private i:I

.field private j:Ljava/lang/String;

.field private k:Ljava/lang/String;

.field private l:I

.field private m:Ljava/lang/String;

.field private n:Ljava/lang/String;

.field private o:I

.field private p:Ljava/lang/String;


# direct methods
.method private constructor <init>()V
    .locals 0

    .prologue
    .line 98
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 99
    invoke-direct {p0}, Lcom/osp/app/signin/c;->b()V

    .line 100
    return-void
.end method

.method static synthetic a()Lcom/osp/app/signin/c;
    .locals 1

    .prologue
    .line 73
    invoke-static {}, Lcom/osp/app/signin/c;->c()Lcom/osp/app/signin/c;

    move-result-object v0

    return-object v0
.end method

.method static synthetic a(Lcom/osp/app/signin/c;)V
    .locals 0

    .prologue
    .line 73
    invoke-direct {p0}, Lcom/osp/app/signin/c;->b()V

    return-void
.end method

.method static synthetic b(Lcom/osp/app/signin/c;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 73
    iget-object v0, p0, Lcom/osp/app/signin/c;->b:Ljava/lang/String;

    return-object v0
.end method

.method private b()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 103
    const-string v0, "false"

    iput-object v0, p0, Lcom/osp/app/signin/c;->b:Ljava/lang/String;

    .line 105
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/osp/app/signin/c;->c:J

    .line 106
    const-string v0, ""

    iput-object v0, p0, Lcom/osp/app/signin/c;->d:Ljava/lang/String;

    .line 108
    const-string v0, ""

    iput-object v0, p0, Lcom/osp/app/signin/c;->e:Ljava/lang/String;

    .line 109
    iput v2, p0, Lcom/osp/app/signin/c;->f:I

    .line 110
    const-string v0, ""

    iput-object v0, p0, Lcom/osp/app/signin/c;->g:Ljava/lang/String;

    .line 112
    const-string v0, ""

    iput-object v0, p0, Lcom/osp/app/signin/c;->h:Ljava/lang/String;

    .line 113
    iput v2, p0, Lcom/osp/app/signin/c;->i:I

    .line 114
    const-string v0, ""

    iput-object v0, p0, Lcom/osp/app/signin/c;->j:Ljava/lang/String;

    .line 116
    const-string v0, ""

    iput-object v0, p0, Lcom/osp/app/signin/c;->k:Ljava/lang/String;

    .line 117
    iput v2, p0, Lcom/osp/app/signin/c;->l:I

    .line 118
    const-string v0, ""

    iput-object v0, p0, Lcom/osp/app/signin/c;->m:Ljava/lang/String;

    .line 120
    const-string v0, ""

    iput-object v0, p0, Lcom/osp/app/signin/c;->n:Ljava/lang/String;

    .line 121
    iput v2, p0, Lcom/osp/app/signin/c;->o:I

    .line 122
    const-string v0, ""

    iput-object v0, p0, Lcom/osp/app/signin/c;->p:Ljava/lang/String;

    .line 124
    return-void
.end method

.method static synthetic c(Lcom/osp/app/signin/c;)J
    .locals 2

    .prologue
    .line 73
    iget-wide v0, p0, Lcom/osp/app/signin/c;->c:J

    return-wide v0
.end method

.method private static declared-synchronized c()Lcom/osp/app/signin/c;
    .locals 2

    .prologue
    .line 127
    const-class v1, Lcom/osp/app/signin/c;

    monitor-enter v1

    :try_start_0
    sget-object v0, Lcom/osp/app/signin/c;->a:Lcom/osp/app/signin/c;

    if-nez v0, :cond_0

    .line 129
    new-instance v0, Lcom/osp/app/signin/c;

    invoke-direct {v0}, Lcom/osp/app/signin/c;-><init>()V

    sput-object v0, Lcom/osp/app/signin/c;->a:Lcom/osp/app/signin/c;

    .line 131
    :cond_0
    sget-object v0, Lcom/osp/app/signin/c;->a:Lcom/osp/app/signin/c;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit v1

    return-object v0

    .line 127
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method static synthetic d(Lcom/osp/app/signin/c;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 73
    iget-object v0, p0, Lcom/osp/app/signin/c;->d:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic e(Lcom/osp/app/signin/c;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 73
    iget-object v0, p0, Lcom/osp/app/signin/c;->e:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic f(Lcom/osp/app/signin/c;)I
    .locals 1

    .prologue
    .line 73
    iget v0, p0, Lcom/osp/app/signin/c;->f:I

    return v0
.end method

.method static synthetic g(Lcom/osp/app/signin/c;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 73
    iget-object v0, p0, Lcom/osp/app/signin/c;->g:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic h(Lcom/osp/app/signin/c;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 73
    iget-object v0, p0, Lcom/osp/app/signin/c;->k:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic i(Lcom/osp/app/signin/c;)I
    .locals 1

    .prologue
    .line 73
    iget v0, p0, Lcom/osp/app/signin/c;->l:I

    return v0
.end method

.method static synthetic j(Lcom/osp/app/signin/c;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 73
    iget-object v0, p0, Lcom/osp/app/signin/c;->m:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic k(Lcom/osp/app/signin/c;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 73
    iget-object v0, p0, Lcom/osp/app/signin/c;->h:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic l(Lcom/osp/app/signin/c;)I
    .locals 1

    .prologue
    .line 73
    iget v0, p0, Lcom/osp/app/signin/c;->i:I

    return v0
.end method

.method static synthetic m(Lcom/osp/app/signin/c;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 73
    iget-object v0, p0, Lcom/osp/app/signin/c;->j:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic n(Lcom/osp/app/signin/c;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 73
    iget-object v0, p0, Lcom/osp/app/signin/c;->n:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic o(Lcom/osp/app/signin/c;)I
    .locals 1

    .prologue
    .line 73
    iget v0, p0, Lcom/osp/app/signin/c;->o:I

    return v0
.end method

.method static synthetic p(Lcom/osp/app/signin/c;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 73
    iget-object v0, p0, Lcom/osp/app/signin/c;->p:Ljava/lang/String;

    return-object v0
.end method
