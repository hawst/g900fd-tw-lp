.class final Lcom/osp/app/signin/ia;
.super Ljava/lang/Object;
.source "SignInView.java"

# interfaces
.implements Landroid/content/DialogInterface$OnClickListener;


# instance fields
.field final synthetic a:Lcom/osp/app/signin/SignInView;


# direct methods
.method constructor <init>(Lcom/osp/app/signin/SignInView;)V
    .locals 0

    .prologue
    .line 1736
    iput-object p1, p0, Lcom/osp/app/signin/ia;->a:Lcom/osp/app/signin/SignInView;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onClick(Landroid/content/DialogInterface;I)V
    .locals 5

    .prologue
    const/4 v4, 0x1

    .line 1741
    const-string v0, "BG_mode"

    iget-object v1, p0, Lcom/osp/app/signin/ia;->a:Lcom/osp/app/signin/SignInView;

    invoke-static {v1}, Lcom/osp/app/signin/SignInView;->i(Lcom/osp/app/signin/SignInView;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1743
    iget-object v0, p0, Lcom/osp/app/signin/ia;->a:Lcom/osp/app/signin/SignInView;

    iget-object v1, p0, Lcom/osp/app/signin/ia;->a:Lcom/osp/app/signin/SignInView;

    invoke-virtual {v1}, Lcom/osp/app/signin/SignInView;->getIntent()Landroid/content/Intent;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/osp/app/signin/SignInView;->a(Lcom/osp/app/signin/SignInView;Landroid/content/Intent;)Landroid/content/Intent;

    .line 1744
    iget-object v0, p0, Lcom/osp/app/signin/ia;->a:Lcom/osp/app/signin/SignInView;

    invoke-static {v0}, Lcom/osp/app/signin/SignInView;->c(Lcom/osp/app/signin/SignInView;)Landroid/content/Intent;

    move-result-object v0

    const-string v1, "wait_more_info"

    invoke-virtual {v0, v1, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 1745
    iget-object v0, p0, Lcom/osp/app/signin/ia;->a:Lcom/osp/app/signin/SignInView;

    const/4 v1, -0x1

    iget-object v2, p0, Lcom/osp/app/signin/ia;->a:Lcom/osp/app/signin/SignInView;

    invoke-static {v2}, Lcom/osp/app/signin/SignInView;->c(Lcom/osp/app/signin/SignInView;)Landroid/content/Intent;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/osp/app/signin/SignInView;->a(ILandroid/content/Intent;)V

    .line 1746
    iget-object v0, p0, Lcom/osp/app/signin/ia;->a:Lcom/osp/app/signin/SignInView;

    invoke-virtual {v0}, Lcom/osp/app/signin/SignInView;->finish()V

    .line 1747
    iget-object v0, p0, Lcom/osp/app/signin/ia;->a:Lcom/osp/app/signin/SignInView;

    invoke-static {v0}, Lcom/osp/app/signin/SignInView;->r(Lcom/osp/app/signin/SignInView;)Landroid/os/Handler;

    move-result-object v0

    new-instance v1, Lcom/osp/app/signin/ib;

    invoke-direct {v1, p0}, Lcom/osp/app/signin/ib;-><init>(Lcom/osp/app/signin/ia;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 1797
    :goto_0
    invoke-static {}, Lcom/osp/app/util/ap;->a()Lcom/osp/app/util/ap;

    iget-object v0, p0, Lcom/osp/app/signin/ia;->a:Lcom/osp/app/signin/SignInView;

    iget-object v1, p0, Lcom/osp/app/signin/ia;->a:Lcom/osp/app/signin/SignInView;

    const v2, 0x7f090024

    invoke-virtual {v1, v2}, Lcom/osp/app/signin/SignInView;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1, v4}, Lcom/osp/app/util/ap;->a(Landroid/content/Context;Ljava/lang/String;I)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    .line 1798
    return-void

    .line 1778
    :cond_0
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    .line 1779
    const-string v1, "com.osp.app.signin"

    const-string v2, "com.osp.app.signin.AccountinfoView"

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->setClassName(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 1780
    const-string v1, "client_id"

    iget-object v2, p0, Lcom/osp/app/signin/ia;->a:Lcom/osp/app/signin/SignInView;

    invoke-static {v2}, Lcom/osp/app/signin/SignInView;->j(Lcom/osp/app/signin/SignInView;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 1781
    const-string v1, "client_secret"

    iget-object v2, p0, Lcom/osp/app/signin/ia;->a:Lcom/osp/app/signin/SignInView;

    invoke-static {v2}, Lcom/osp/app/signin/SignInView;->k(Lcom/osp/app/signin/SignInView;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 1782
    const-string v1, "account_mode"

    iget-object v2, p0, Lcom/osp/app/signin/ia;->a:Lcom/osp/app/signin/SignInView;

    invoke-static {v2}, Lcom/osp/app/signin/SignInView;->l(Lcom/osp/app/signin/SignInView;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 1783
    const-string v1, "service_name"

    iget-object v2, p0, Lcom/osp/app/signin/ia;->a:Lcom/osp/app/signin/SignInView;

    invoke-static {v2}, Lcom/osp/app/signin/SignInView;->m(Lcom/osp/app/signin/SignInView;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 1784
    const-string v1, "OSP_VER"

    iget-object v2, p0, Lcom/osp/app/signin/ia;->a:Lcom/osp/app/signin/SignInView;

    invoke-static {v2}, Lcom/osp/app/signin/SignInView;->n(Lcom/osp/app/signin/SignInView;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 1785
    const-string v1, "direct_modify"

    invoke-virtual {v0, v1, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 1786
    const-string v1, "authcode"

    iget-object v2, p0, Lcom/osp/app/signin/ia;->a:Lcom/osp/app/signin/SignInView;

    invoke-static {v2}, Lcom/osp/app/signin/SignInView;->o(Lcom/osp/app/signin/SignInView;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 1787
    const-string v1, "BG_WhoareU"

    iget-object v2, p0, Lcom/osp/app/signin/ia;->a:Lcom/osp/app/signin/SignInView;

    invoke-static {v2}, Lcom/osp/app/signin/SignInView;->i(Lcom/osp/app/signin/SignInView;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 1788
    const-string v1, "mypackage"

    iget-object v2, p0, Lcom/osp/app/signin/ia;->a:Lcom/osp/app/signin/SignInView;

    invoke-static {v2}, Lcom/osp/app/signin/SignInView;->p(Lcom/osp/app/signin/SignInView;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 1789
    const-string v1, "key_request_id"

    iget-object v2, p0, Lcom/osp/app/signin/ia;->a:Lcom/osp/app/signin/SignInView;

    invoke-static {v2}, Lcom/osp/app/signin/SignInView;->q(Lcom/osp/app/signin/SignInView;)J

    move-result-wide v2

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;J)Landroid/content/Intent;

    .line 1790
    const-string v1, "is_authorized_internal_action_for_accountinfo"

    const-string v2, "TIME_PATTERN"

    invoke-static {v2}, Lcom/osp/app/util/ad;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 1795
    iget-object v1, p0, Lcom/osp/app/signin/ia;->a:Lcom/osp/app/signin/SignInView;

    const/16 v2, 0xd2

    invoke-virtual {v1, v0, v2}, Lcom/osp/app/signin/SignInView;->startActivityForResult(Landroid/content/Intent;I)V

    goto/16 :goto_0
.end method
