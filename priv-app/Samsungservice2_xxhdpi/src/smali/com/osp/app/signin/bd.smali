.class final Lcom/osp/app/signin/bd;
.super Lcom/msc/c/b;
.source "AccountView.java"


# instance fields
.field final synthetic c:Lcom/osp/app/signin/AccountView;

.field private d:J

.field private e:J

.field private f:J

.field private g:Z

.field private h:Ljava/lang/String;

.field private i:Lcom/osp/app/signin/kd;

.field private j:Z

.field private k:Ljava/lang/String;


# direct methods
.method public constructor <init>(Lcom/osp/app/signin/AccountView;)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    const/4 v0, 0x0

    .line 6607
    iput-object p1, p0, Lcom/osp/app/signin/bd;->c:Lcom/osp/app/signin/AccountView;

    .line 6608
    invoke-direct {p0, p1}, Lcom/msc/c/b;-><init>(Landroid/content/Context;)V

    .line 6586
    iput-boolean v1, p0, Lcom/osp/app/signin/bd;->g:Z

    .line 6590
    iput-object v0, p0, Lcom/osp/app/signin/bd;->h:Ljava/lang/String;

    .line 6594
    iput-object v0, p0, Lcom/osp/app/signin/bd;->i:Lcom/osp/app/signin/kd;

    .line 6598
    iput-boolean v1, p0, Lcom/osp/app/signin/bd;->j:Z

    .line 6602
    iput-object v0, p0, Lcom/osp/app/signin/bd;->k:Ljava/lang/String;

    .line 6612
    return-void
.end method

.method private b(Ljava/lang/String;)V
    .locals 3

    .prologue
    .line 6950
    invoke-static {}, Lcom/msc/c/g;->a()Lcom/msc/c/g;

    .line 6951
    iget-object v0, p0, Lcom/osp/app/signin/bd;->c:Lcom/osp/app/signin/AccountView;

    iget-object v1, p0, Lcom/osp/app/signin/bd;->c:Lcom/osp/app/signin/AccountView;

    invoke-static {v1}, Lcom/osp/app/signin/AccountView;->l(Lcom/osp/app/signin/AccountView;)Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/osp/app/signin/bd;->c:Lcom/osp/app/signin/AccountView;

    invoke-static {v2}, Lcom/osp/app/signin/AccountView;->G(Lcom/osp/app/signin/AccountView;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v1, p1, v2, p0}, Lcom/msc/c/g;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/msc/b/h;)J

    move-result-wide v0

    iput-wide v0, p0, Lcom/osp/app/signin/bd;->e:J

    .line 6952
    iget-wide v0, p0, Lcom/osp/app/signin/bd;->e:J

    invoke-virtual {p0, v0, v1}, Lcom/osp/app/signin/bd;->a(J)V

    .line 6953
    iget-wide v0, p0, Lcom/osp/app/signin/bd;->e:J

    const-string v2, "from_json"

    invoke-virtual {p0, v0, v1, v2}, Lcom/osp/app/signin/bd;->a(JLjava/lang/String;)V

    .line 6954
    iget-wide v0, p0, Lcom/osp/app/signin/bd;->e:J

    sget-object v2, Lcom/msc/b/g;->b:Lcom/msc/b/g;

    invoke-static {v0, v1, v2}, Lcom/msc/c/g;->a(JLcom/msc/b/g;)V

    .line 6955
    return-void
.end method

.method private h()V
    .locals 3

    .prologue
    .line 6936
    invoke-static {}, Lcom/msc/c/g;->a()Lcom/msc/c/g;

    .line 6937
    iget-object v0, p0, Lcom/osp/app/signin/bd;->c:Lcom/osp/app/signin/AccountView;

    iget-object v1, p0, Lcom/osp/app/signin/bd;->c:Lcom/osp/app/signin/AccountView;

    invoke-static {v1}, Lcom/osp/app/signin/AccountView;->l(Lcom/osp/app/signin/AccountView;)Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/osp/app/signin/bd;->c:Lcom/osp/app/signin/AccountView;

    invoke-static {v2}, Lcom/osp/app/util/ad;->h(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v1, v2, p0}, Lcom/msc/c/g;->c(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Lcom/msc/b/h;)J

    move-result-wide v0

    iput-wide v0, p0, Lcom/osp/app/signin/bd;->d:J

    .line 6938
    iget-wide v0, p0, Lcom/osp/app/signin/bd;->d:J

    invoke-virtual {p0, v0, v1}, Lcom/osp/app/signin/bd;->a(J)V

    .line 6939
    iget-wide v0, p0, Lcom/osp/app/signin/bd;->d:J

    const-string v2, "none"

    invoke-virtual {p0, v0, v1, v2}, Lcom/osp/app/signin/bd;->a(JLjava/lang/String;)V

    .line 6941
    iget-wide v0, p0, Lcom/osp/app/signin/bd;->d:J

    sget-object v2, Lcom/msc/b/g;->c:Lcom/msc/b/g;

    invoke-static {v0, v1, v2}, Lcom/msc/c/g;->a(JLcom/msc/b/g;)V

    .line 6942
    return-void
.end method


# virtual methods
.method protected final varargs a()Ljava/lang/Boolean;
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 6637
    iget-object v0, p0, Lcom/osp/app/signin/bd;->c:Lcom/osp/app/signin/AccountView;

    invoke-static {v0}, Lcom/msc/c/e;->g(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v0

    if-nez v0, :cond_0

    .line 6639
    iput-boolean v2, p0, Lcom/osp/app/signin/bd;->g:Z

    .line 6641
    iget-object v0, p0, Lcom/osp/app/signin/bd;->c:Lcom/osp/app/signin/AccountView;

    iget-object v1, p0, Lcom/osp/app/signin/bd;->c:Lcom/osp/app/signin/AccountView;

    invoke-static {v1}, Lcom/osp/app/util/ad;->h(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/osp/app/signin/AccountView;->c(Lcom/osp/app/signin/AccountView;Ljava/lang/String;)V

    .line 6646
    :goto_0
    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    return-object v0

    .line 6644
    :cond_0
    invoke-direct {p0}, Lcom/osp/app/signin/bd;->h()V

    goto :goto_0
.end method

.method public final declared-synchronized a(Lcom/msc/b/c;)V
    .locals 6

    .prologue
    .line 6748
    monitor-enter p0

    :try_start_0
    invoke-super {p0, p1}, Lcom/msc/c/b;->a(Lcom/msc/b/c;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 6750
    if-nez p1, :cond_1

    .line 6825
    :cond_0
    :goto_0
    monitor-exit p0

    return-void

    .line 6755
    :cond_1
    :try_start_1
    invoke-virtual {p1}, Lcom/msc/b/c;->a()J

    move-result-wide v0

    .line 6756
    invoke-virtual {p1}, Lcom/msc/b/c;->e()Ljava/lang/String;

    move-result-object v2

    .line 6758
    iget-wide v4, p0, Lcom/osp/app/signin/bd;->d:J
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    cmp-long v3, v0, v4

    if-nez v3, :cond_4

    .line 6763
    :try_start_2
    invoke-static {}, Lcom/msc/c/h;->a()Lcom/msc/c/h;

    move-result-object v0

    iget-object v1, p0, Lcom/osp/app/signin/bd;->c:Lcom/osp/app/signin/AccountView;

    iget-object v3, p0, Lcom/osp/app/signin/bd;->c:Lcom/osp/app/signin/AccountView;

    invoke-static {v3}, Lcom/osp/app/signin/AccountView;->l(Lcom/osp/app/signin/AccountView;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v1, v3, v2}, Lcom/msc/c/h;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)Lcom/osp/app/signin/kd;

    move-result-object v0

    iput-object v0, p0, Lcom/osp/app/signin/bd;->i:Lcom/osp/app/signin/kd;
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_1
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 6771
    :goto_1
    :try_start_3
    const-string v0, "OSP_02"

    iget-object v1, p0, Lcom/osp/app/signin/bd;->c:Lcom/osp/app/signin/AccountView;

    invoke-static {v1}, Lcom/osp/app/signin/AccountView;->I(Lcom/osp/app/signin/AccountView;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/osp/app/signin/bd;->c:Lcom/osp/app/signin/AccountView;

    invoke-static {v0}, Lcom/osp/app/signin/AccountView;->l(Lcom/osp/app/signin/AccountView;)Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/osp/app/signin/bd;->c:Lcom/osp/app/signin/AccountView;

    invoke-static {v0}, Lcom/osp/app/signin/AccountView;->J(Lcom/osp/app/signin/AccountView;)Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_2

    const-string v0, "BG_mode"

    iget-object v1, p0, Lcom/osp/app/signin/bd;->c:Lcom/osp/app/signin/AccountView;

    invoke-static {v1}, Lcom/osp/app/signin/AccountView;->D(Lcom/osp/app/signin/AccountView;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 6773
    :cond_2
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/osp/app/signin/bd;->g:Z

    .line 6774
    iget-object v0, p0, Lcom/osp/app/signin/bd;->c:Lcom/osp/app/signin/AccountView;

    invoke-static {v0}, Lcom/msc/c/e;->g(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/osp/app/signin/bd;->b(Ljava/lang/String;)V
    :try_end_3
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_0
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    goto :goto_0

    .line 6779
    :catch_0
    move-exception v0

    .line 6781
    :try_start_4
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    .line 6782
    new-instance v1, Lcom/msc/c/f;

    invoke-direct {v1, v0}, Lcom/msc/c/f;-><init>(Ljava/lang/Exception;)V

    iput-object v1, p0, Lcom/osp/app/signin/bd;->b:Lcom/msc/c/f;
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    goto :goto_0

    .line 6748
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 6765
    :catch_1
    move-exception v0

    :try_start_5
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    goto :goto_1

    .line 6777
    :cond_3
    :try_start_6
    new-instance v0, Lcom/msc/c/f;

    invoke-direct {v0}, Lcom/msc/c/f;-><init>()V

    iput-object v0, p0, Lcom/osp/app/signin/bd;->b:Lcom/msc/c/f;
    :try_end_6
    .catch Ljava/lang/Exception; {:try_start_6 .. :try_end_6} :catch_0
    .catchall {:try_start_6 .. :try_end_6} :catchall_0

    goto :goto_0

    .line 6784
    :cond_4
    :try_start_7
    iget-wide v4, p0, Lcom/osp/app/signin/bd;->e:J
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_0

    cmp-long v3, v0, v4

    if-nez v3, :cond_5

    .line 6788
    :try_start_8
    invoke-static {}, Lcom/msc/c/h;->a()Lcom/msc/c/h;

    invoke-static {v2}, Lcom/msc/c/h;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/osp/app/signin/bd;->h:Ljava/lang/String;
    :try_end_8
    .catch Ljava/lang/Exception; {:try_start_8 .. :try_end_8} :catch_2
    .catchall {:try_start_8 .. :try_end_8} :catchall_0

    goto/16 :goto_0

    .line 6789
    :catch_2
    move-exception v0

    .line 6791
    :try_start_9
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    .line 6792
    new-instance v1, Lcom/msc/c/f;

    invoke-direct {v1, v0}, Lcom/msc/c/f;-><init>(Ljava/lang/Exception;)V

    iput-object v1, p0, Lcom/osp/app/signin/bd;->b:Lcom/msc/c/f;

    goto/16 :goto_0

    .line 6794
    :cond_5
    iget-wide v4, p0, Lcom/osp/app/signin/bd;->f:J
    :try_end_9
    .catchall {:try_start_9 .. :try_end_9} :catchall_0

    cmp-long v0, v0, v4

    if-nez v0, :cond_0

    .line 6798
    :try_start_a
    invoke-static {}, Lcom/msc/c/h;->a()Lcom/msc/c/h;

    invoke-static {v2}, Lcom/msc/c/h;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/osp/app/signin/bd;->k:Ljava/lang/String;

    .line 6799
    iget-object v0, p0, Lcom/osp/app/signin/bd;->k:Ljava/lang/String;

    if-eqz v0, :cond_7

    iget-object v0, p0, Lcom/osp/app/signin/bd;->k:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v0

    if-lez v0, :cond_7

    .line 6801
    iget-object v0, p0, Lcom/osp/app/signin/bd;->c:Lcom/osp/app/signin/AccountView;

    iget-object v1, p0, Lcom/osp/app/signin/bd;->k:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/msc/c/e;->e(Landroid/content/Context;Ljava/lang/String;)V

    .line 6802
    invoke-virtual {p0}, Lcom/osp/app/signin/bd;->isCancelled()Z

    move-result v0

    if-nez v0, :cond_0

    .line 6806
    iget-boolean v0, p0, Lcom/osp/app/signin/bd;->g:Z

    if-eqz v0, :cond_6

    .line 6808
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/osp/app/signin/bd;->g:Z

    .line 6809
    iget-object v0, p0, Lcom/osp/app/signin/bd;->k:Ljava/lang/String;

    invoke-direct {p0, v0}, Lcom/osp/app/signin/bd;->b(Ljava/lang/String;)V
    :try_end_a
    .catch Ljava/lang/Exception; {:try_start_a .. :try_end_a} :catch_3
    .catchall {:try_start_a .. :try_end_a} :catchall_0

    goto/16 :goto_0

    .line 6819
    :catch_3
    move-exception v0

    .line 6821
    :try_start_b
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    .line 6822
    new-instance v1, Lcom/msc/c/f;

    invoke-direct {v1, v0}, Lcom/msc/c/f;-><init>(Ljava/lang/Exception;)V

    iput-object v1, p0, Lcom/osp/app/signin/bd;->b:Lcom/msc/c/f;
    :try_end_b
    .catchall {:try_start_b .. :try_end_b} :catchall_0

    goto/16 :goto_0

    .line 6812
    :cond_6
    :try_start_c
    invoke-direct {p0}, Lcom/osp/app/signin/bd;->h()V

    goto/16 :goto_0

    .line 6817
    :cond_7
    new-instance v0, Lcom/msc/c/f;

    invoke-direct {v0}, Lcom/msc/c/f;-><init>()V

    iput-object v0, p0, Lcom/osp/app/signin/bd;->b:Lcom/msc/c/f;
    :try_end_c
    .catch Ljava/lang/Exception; {:try_start_c .. :try_end_c} :catch_3
    .catchall {:try_start_c .. :try_end_c} :catchall_0

    goto/16 :goto_0
.end method

.method protected final a(Ljava/lang/Boolean;)V
    .locals 9

    .prologue
    const/4 v8, 0x0

    const/4 v7, -0x1

    const/4 v6, 0x1

    .line 6651
    invoke-super {p0, p1}, Lcom/msc/c/b;->a(Ljava/lang/Boolean;)V

    .line 6652
    iget-object v0, p0, Lcom/osp/app/signin/bd;->b:Lcom/msc/c/f;

    if-eqz v0, :cond_5

    .line 6654
    const-string v0, "SOCKET_TIMEOUT_EXCEPTION"

    iget-object v1, p0, Lcom/osp/app/signin/bd;->b:Lcom/msc/c/f;

    invoke-virtual {v1}, Lcom/msc/c/f;->b()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    const-string v0, "CONNECT_TIMEOUT_EXCEPTION"

    iget-object v1, p0, Lcom/osp/app/signin/bd;->b:Lcom/msc/c/f;

    invoke-virtual {v1}, Lcom/msc/c/f;->b()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    const-string v0, "HTTP_HOST_CONNECT_EXCEPTION"

    iget-object v1, p0, Lcom/osp/app/signin/bd;->b:Lcom/msc/c/f;

    invoke-virtual {v1}, Lcom/msc/c/f;->b()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 6657
    :cond_0
    new-instance v0, Landroid/content/Intent;

    const-string v1, "com.msc.action.samsungaccount.response.BackGroundSignin"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 6658
    iget-object v1, p0, Lcom/osp/app/signin/bd;->c:Lcom/osp/app/signin/AccountView;

    const/4 v2, 0x3

    iget-object v3, p0, Lcom/osp/app/signin/bd;->c:Lcom/osp/app/signin/AccountView;

    invoke-static {v3}, Lcom/osp/app/signin/AccountView;->m(Lcom/osp/app/signin/AccountView;)Landroid/content/Intent;

    move-result-object v3

    invoke-static {v1, v2, v3, v0}, Lcom/osp/app/signin/AccountView;->a(Lcom/osp/app/signin/AccountView;ILandroid/content/Intent;Landroid/content/Intent;)V

    .line 6659
    iget-object v0, p0, Lcom/osp/app/signin/bd;->c:Lcom/osp/app/signin/AccountView;

    invoke-virtual {v0}, Lcom/osp/app/signin/AccountView;->finish()V

    .line 6744
    :goto_0
    return-void

    .line 6662
    :cond_1
    iget-boolean v0, p0, Lcom/osp/app/signin/bd;->j:Z

    if-eqz v0, :cond_2

    .line 6664
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/osp/app/signin/bd;->j:Z

    .line 6665
    iget-object v0, p0, Lcom/osp/app/signin/bd;->c:Lcom/osp/app/signin/AccountView;

    invoke-static {v0}, Lcom/osp/app/signin/AccountView;->C(Lcom/osp/app/signin/AccountView;)V

    goto :goto_0

    .line 6667
    :cond_2
    const-string v0, "BG_mode"

    iget-object v1, p0, Lcom/osp/app/signin/bd;->c:Lcom/osp/app/signin/AccountView;

    invoke-static {v1}, Lcom/osp/app/signin/AccountView;->D(Lcom/osp/app/signin/AccountView;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 6669
    new-instance v0, Landroid/content/Intent;

    const-string v1, "com.msc.action.samsungaccount.response.BackGroundSignin"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 6670
    const-string v1, "bg_result"

    invoke-virtual {v0, v1, v6}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 6672
    iget-object v1, p0, Lcom/osp/app/signin/bd;->c:Lcom/osp/app/signin/AccountView;

    invoke-static {v1}, Lcom/osp/app/signin/AccountView;->x(Lcom/osp/app/signin/AccountView;)Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_3

    .line 6674
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v2, p0, Lcom/osp/app/signin/bd;->c:Lcom/osp/app/signin/AccountView;

    invoke-static {v2}, Lcom/osp/app/signin/AccountView;->x(Lcom/osp/app/signin/AccountView;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ":"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setData(Landroid/net/Uri;)Landroid/content/Intent;

    .line 6675
    iget-object v1, p0, Lcom/osp/app/signin/bd;->c:Lcom/osp/app/signin/AccountView;

    invoke-static {v1}, Lcom/osp/app/signin/AccountView;->x(Lcom/osp/app/signin/AccountView;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setPackage(Ljava/lang/String;)Landroid/content/Intent;

    .line 6678
    const-string v1, "client_id"

    iget-object v2, p0, Lcom/osp/app/signin/bd;->c:Lcom/osp/app/signin/AccountView;

    invoke-static {v2}, Lcom/osp/app/signin/AccountView;->l(Lcom/osp/app/signin/AccountView;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 6679
    iget-object v1, p0, Lcom/osp/app/signin/bd;->c:Lcom/osp/app/signin/AccountView;

    iget-object v2, p0, Lcom/osp/app/signin/bd;->c:Lcom/osp/app/signin/AccountView;

    invoke-static {v2}, Lcom/osp/app/signin/AccountView;->E(Lcom/osp/app/signin/AccountView;)J

    move-result-wide v2

    invoke-virtual {v1, v0, v2, v3}, Lcom/osp/app/signin/AccountView;->a(Landroid/content/Intent;J)V

    .line 6686
    :cond_3
    :goto_1
    iget-object v0, p0, Lcom/osp/app/signin/bd;->c:Lcom/osp/app/signin/AccountView;

    invoke-virtual {v0}, Lcom/osp/app/signin/AccountView;->finish()V

    goto :goto_0

    .line 6684
    :cond_4
    iget-object v0, p0, Lcom/osp/app/signin/bd;->c:Lcom/osp/app/signin/AccountView;

    iget-object v1, p0, Lcom/osp/app/signin/bd;->c:Lcom/osp/app/signin/AccountView;

    invoke-static {v1}, Lcom/osp/app/signin/AccountView;->m(Lcom/osp/app/signin/AccountView;)Landroid/content/Intent;

    move-result-object v1

    invoke-static {v0, v6, v1, v8}, Lcom/osp/app/signin/AccountView;->a(Lcom/osp/app/signin/AccountView;ILandroid/content/Intent;Landroid/content/Intent;)V

    goto :goto_1

    .line 6689
    :cond_5
    new-instance v0, Landroid/content/Intent;

    const-string v1, "com.msc.action.samsungaccount.response.BackGroundSignin"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 6690
    iget-object v1, p0, Lcom/osp/app/signin/bd;->i:Lcom/osp/app/signin/kd;

    if-eqz v1, :cond_8

    iget-object v1, p0, Lcom/osp/app/signin/bd;->i:Lcom/osp/app/signin/kd;

    invoke-virtual {v1}, Lcom/osp/app/signin/kd;->b()I

    move-result v1

    if-lez v1, :cond_8

    .line 6692
    const-string v1, "BG_mode"

    iget-object v2, p0, Lcom/osp/app/signin/bd;->c:Lcom/osp/app/signin/AccountView;

    invoke-static {v2}, Lcom/osp/app/signin/AccountView;->D(Lcom/osp/app/signin/AccountView;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_6

    .line 6694
    iget-object v1, p0, Lcom/osp/app/signin/bd;->c:Lcom/osp/app/signin/AccountView;

    invoke-static {v1}, Lcom/osp/app/signin/AccountView;->m(Lcom/osp/app/signin/AccountView;)Landroid/content/Intent;

    move-result-object v1

    const-string v2, "wait_more_info"

    invoke-virtual {v1, v2, v6}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 6700
    :goto_2
    new-instance v1, Landroid/content/Intent;

    invoke-direct {v1}, Landroid/content/Intent;-><init>()V

    .line 6701
    const-string v2, "com.osp.app.signin"

    const-string v3, "com.osp.app.signin.AccountinfoView"

    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->setClassName(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 6702
    const-string v2, "client_id"

    iget-object v3, p0, Lcom/osp/app/signin/bd;->c:Lcom/osp/app/signin/AccountView;

    invoke-static {v3}, Lcom/osp/app/signin/AccountView;->l(Lcom/osp/app/signin/AccountView;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 6703
    const-string v2, "client_secret"

    iget-object v3, p0, Lcom/osp/app/signin/bd;->c:Lcom/osp/app/signin/AccountView;

    invoke-static {v3}, Lcom/osp/app/signin/AccountView;->F(Lcom/osp/app/signin/AccountView;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 6704
    const-string v2, "account_mode"

    iget-object v3, p0, Lcom/osp/app/signin/bd;->c:Lcom/osp/app/signin/AccountView;

    invoke-static {v3}, Lcom/osp/app/signin/AccountView;->G(Lcom/osp/app/signin/AccountView;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 6705
    const-string v2, "service_name"

    iget-object v3, p0, Lcom/osp/app/signin/bd;->c:Lcom/osp/app/signin/AccountView;

    invoke-static {v3}, Lcom/osp/app/signin/AccountView;->H(Lcom/osp/app/signin/AccountView;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 6706
    const-string v2, "OSP_VER"

    iget-object v3, p0, Lcom/osp/app/signin/bd;->c:Lcom/osp/app/signin/AccountView;

    invoke-static {v3}, Lcom/osp/app/signin/AccountView;->I(Lcom/osp/app/signin/AccountView;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 6707
    const-string v2, "direct_modify"

    invoke-virtual {v1, v2, v6}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 6708
    const-string v2, "authcode"

    iget-object v3, p0, Lcom/osp/app/signin/bd;->h:Ljava/lang/String;

    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 6709
    const-string v2, "is_authorized_internal_action_for_accountinfo"

    const-string v3, "TIME_PATTERN"

    invoke-static {v3}, Lcom/osp/app/util/ad;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 6711
    const-string v2, "BG_mode"

    iget-object v3, p0, Lcom/osp/app/signin/bd;->c:Lcom/osp/app/signin/AccountView;

    invoke-static {v3}, Lcom/osp/app/signin/AccountView;->D(Lcom/osp/app/signin/AccountView;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_7

    .line 6713
    const/high16 v2, 0x10000000

    invoke-virtual {v1, v2}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    .line 6714
    const-string v2, "BG_WhoareU"

    iget-object v3, p0, Lcom/osp/app/signin/bd;->c:Lcom/osp/app/signin/AccountView;

    invoke-static {v3}, Lcom/osp/app/signin/AccountView;->D(Lcom/osp/app/signin/AccountView;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 6715
    const-string v2, "mypackage"

    iget-object v3, p0, Lcom/osp/app/signin/bd;->c:Lcom/osp/app/signin/AccountView;

    invoke-static {v3}, Lcom/osp/app/signin/AccountView;->x(Lcom/osp/app/signin/AccountView;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 6716
    const-string v2, "key_request_id"

    iget-object v3, p0, Lcom/osp/app/signin/bd;->c:Lcom/osp/app/signin/AccountView;

    invoke-static {v3}, Lcom/osp/app/signin/AccountView;->E(Lcom/osp/app/signin/AccountView;)J

    move-result-wide v4

    invoke-virtual {v1, v2, v4, v5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;J)Landroid/content/Intent;

    .line 6717
    iget-object v2, p0, Lcom/osp/app/signin/bd;->c:Lcom/osp/app/signin/AccountView;

    invoke-virtual {v2, v1}, Lcom/osp/app/signin/AccountView;->startActivity(Landroid/content/Intent;)V

    .line 6719
    iget-object v1, p0, Lcom/osp/app/signin/bd;->c:Lcom/osp/app/signin/AccountView;

    iget-object v2, p0, Lcom/osp/app/signin/bd;->c:Lcom/osp/app/signin/AccountView;

    invoke-static {v2}, Lcom/osp/app/signin/AccountView;->m(Lcom/osp/app/signin/AccountView;)Landroid/content/Intent;

    move-result-object v2

    invoke-static {v1, v7, v2, v0}, Lcom/osp/app/signin/AccountView;->a(Lcom/osp/app/signin/AccountView;ILandroid/content/Intent;Landroid/content/Intent;)V

    .line 6720
    iget-object v0, p0, Lcom/osp/app/signin/bd;->c:Lcom/osp/app/signin/AccountView;

    invoke-virtual {v0}, Lcom/osp/app/signin/AccountView;->finish()V

    .line 6728
    :goto_3
    invoke-static {}, Lcom/osp/app/util/ap;->a()Lcom/osp/app/util/ap;

    iget-object v0, p0, Lcom/osp/app/signin/bd;->c:Lcom/osp/app/signin/AccountView;

    iget-object v1, p0, Lcom/osp/app/signin/bd;->c:Lcom/osp/app/signin/AccountView;

    const v2, 0x7f090024

    invoke-virtual {v1, v2}, Lcom/osp/app/signin/AccountView;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1, v6}, Lcom/osp/app/util/ap;->a(Landroid/content/Context;Ljava/lang/String;I)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    .line 6729
    iput-object v8, p0, Lcom/osp/app/signin/bd;->i:Lcom/osp/app/signin/kd;

    goto/16 :goto_0

    .line 6697
    :cond_6
    const-string v1, "wait_more_info"

    invoke-virtual {v0, v1, v6}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    goto/16 :goto_2

    .line 6725
    :cond_7
    iget-object v0, p0, Lcom/osp/app/signin/bd;->c:Lcom/osp/app/signin/AccountView;

    const/16 v2, 0xd2

    invoke-virtual {v0, v1, v2}, Lcom/osp/app/signin/AccountView;->startActivityForResult(Landroid/content/Intent;I)V

    goto :goto_3

    .line 6732
    :cond_8
    const-string v1, "BG_mode"

    iget-object v2, p0, Lcom/osp/app/signin/bd;->c:Lcom/osp/app/signin/AccountView;

    invoke-static {v2}, Lcom/osp/app/signin/AccountView;->D(Lcom/osp/app/signin/AccountView;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_a

    .line 6734
    const-string v1, "authcode"

    iget-object v2, p0, Lcom/osp/app/signin/bd;->h:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 6735
    const-string v1, "ServerUrl"

    iget-object v2, p0, Lcom/osp/app/signin/bd;->c:Lcom/osp/app/signin/AccountView;

    invoke-static {v2}, Lcom/msc/c/c;->f(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 6741
    :cond_9
    :goto_4
    iget-object v1, p0, Lcom/osp/app/signin/bd;->c:Lcom/osp/app/signin/AccountView;

    iget-object v2, p0, Lcom/osp/app/signin/bd;->c:Lcom/osp/app/signin/AccountView;

    invoke-static {v2}, Lcom/osp/app/signin/AccountView;->m(Lcom/osp/app/signin/AccountView;)Landroid/content/Intent;

    move-result-object v2

    invoke-static {v1, v7, v2, v0}, Lcom/osp/app/signin/AccountView;->a(Lcom/osp/app/signin/AccountView;ILandroid/content/Intent;Landroid/content/Intent;)V

    .line 6742
    iget-object v0, p0, Lcom/osp/app/signin/bd;->c:Lcom/osp/app/signin/AccountView;

    invoke-virtual {v0}, Lcom/osp/app/signin/AccountView;->finish()V

    goto/16 :goto_0

    .line 6736
    :cond_a
    iget-object v1, p0, Lcom/osp/app/signin/bd;->c:Lcom/osp/app/signin/AccountView;

    invoke-static {v1}, Lcom/osp/app/signin/AccountView;->I(Lcom/osp/app/signin/AccountView;)Ljava/lang/String;

    move-result-object v1

    const-string v2, "OSP_02"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_9

    .line 6738
    iget-object v1, p0, Lcom/osp/app/signin/bd;->c:Lcom/osp/app/signin/AccountView;

    invoke-static {v1}, Lcom/osp/app/signin/AccountView;->m(Lcom/osp/app/signin/AccountView;)Landroid/content/Intent;

    move-result-object v1

    const-string v2, "authcode"

    iget-object v3, p0, Lcom/osp/app/signin/bd;->h:Ljava/lang/String;

    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 6739
    iget-object v1, p0, Lcom/osp/app/signin/bd;->c:Lcom/osp/app/signin/AccountView;

    invoke-static {v1}, Lcom/osp/app/signin/AccountView;->m(Lcom/osp/app/signin/AccountView;)Landroid/content/Intent;

    move-result-object v1

    const-string v2, "ServerUrl"

    iget-object v3, p0, Lcom/osp/app/signin/bd;->c:Lcom/osp/app/signin/AccountView;

    invoke-static {v3}, Lcom/msc/c/c;->f(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    goto :goto_4
.end method

.method public final b(Lcom/msc/b/c;)V
    .locals 7

    .prologue
    const/4 v6, 0x1

    .line 6829
    invoke-super {p0, p1}, Lcom/msc/c/b;->b(Lcom/msc/b/c;)V

    .line 6831
    if-nez p1, :cond_1

    .line 6925
    :cond_0
    :goto_0
    return-void

    .line 6836
    :cond_1
    invoke-virtual {p1}, Lcom/msc/b/c;->a()J

    move-result-wide v0

    .line 6837
    invoke-virtual {p1}, Lcom/msc/b/c;->e()Ljava/lang/String;

    move-result-object v2

    .line 6839
    iget-wide v4, p0, Lcom/osp/app/signin/bd;->d:J

    cmp-long v3, v0, v4

    if-nez v3, :cond_4

    .line 6843
    :try_start_0
    const-string v0, "OSP_02"

    iget-object v1, p0, Lcom/osp/app/signin/bd;->c:Lcom/osp/app/signin/AccountView;

    invoke-static {v1}, Lcom/osp/app/signin/AccountView;->I(Lcom/osp/app/signin/AccountView;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/osp/app/signin/bd;->c:Lcom/osp/app/signin/AccountView;

    invoke-static {v0}, Lcom/osp/app/signin/AccountView;->l(Lcom/osp/app/signin/AccountView;)Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/osp/app/signin/bd;->c:Lcom/osp/app/signin/AccountView;

    invoke-static {v0}, Lcom/osp/app/signin/AccountView;->J(Lcom/osp/app/signin/AccountView;)Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_2

    const-string v0, "BG_mode"

    iget-object v1, p0, Lcom/osp/app/signin/bd;->c:Lcom/osp/app/signin/AccountView;

    invoke-static {v1}, Lcom/osp/app/signin/AccountView;->D(Lcom/osp/app/signin/AccountView;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 6845
    :cond_2
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/osp/app/signin/bd;->g:Z

    .line 6846
    iget-object v0, p0, Lcom/osp/app/signin/bd;->c:Lcom/osp/app/signin/AccountView;

    invoke-static {v0}, Lcom/msc/c/e;->g(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/osp/app/signin/bd;->b(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 6851
    :catch_0
    move-exception v0

    .line 6853
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    .line 6854
    new-instance v1, Lcom/msc/c/f;

    invoke-direct {v1, v0}, Lcom/msc/c/f;-><init>(Ljava/lang/Exception;)V

    iput-object v1, p0, Lcom/osp/app/signin/bd;->b:Lcom/msc/c/f;

    goto :goto_0

    .line 6849
    :cond_3
    :try_start_1
    new-instance v0, Lcom/msc/c/f;

    invoke-direct {v0}, Lcom/msc/c/f;-><init>()V

    iput-object v0, p0, Lcom/osp/app/signin/bd;->b:Lcom/msc/c/f;
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_0

    .line 6856
    :cond_4
    iget-wide v4, p0, Lcom/osp/app/signin/bd;->e:J

    cmp-long v3, v0, v4

    if-nez v3, :cond_a

    .line 6858
    if-eqz v2, :cond_0

    .line 6862
    :try_start_2
    iget-boolean v0, p0, Lcom/osp/app/signin/bd;->g:Z

    if-nez v0, :cond_5

    const-string v0, "AUT_1805"

    iget-object v1, p0, Lcom/osp/app/signin/bd;->b:Lcom/msc/c/f;

    invoke-virtual {v1}, Lcom/msc/c/f;->a()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_6

    :cond_5
    const-string v0, "AUT_1004"

    iget-object v1, p0, Lcom/osp/app/signin/bd;->b:Lcom/msc/c/f;

    invoke-virtual {v1}, Lcom/msc/c/f;->a()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_7

    .line 6866
    :cond_6
    iget-object v0, p0, Lcom/osp/app/signin/bd;->c:Lcom/osp/app/signin/AccountView;

    invoke-static {v0}, Lcom/msc/c/c;->e(Landroid/content/Context;)V

    .line 6870
    iget-object v0, p0, Lcom/osp/app/signin/bd;->c:Lcom/osp/app/signin/AccountView;

    invoke-static {v0}, Lcom/msc/c/e;->i(Landroid/content/Context;)V

    .line 6872
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/osp/app/signin/bd;->j:Z
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_1

    goto/16 :goto_0

    .line 6901
    :catch_1
    move-exception v0

    .line 6903
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    .line 6904
    new-instance v1, Lcom/msc/c/f;

    invoke-direct {v1, v0}, Lcom/msc/c/f;-><init>(Ljava/lang/Exception;)V

    iput-object v1, p0, Lcom/osp/app/signin/bd;->b:Lcom/msc/c/f;

    goto/16 :goto_0

    .line 6873
    :cond_7
    :try_start_3
    iget-boolean v0, p0, Lcom/osp/app/signin/bd;->g:Z

    if-nez v0, :cond_9

    const-string v0, "AUT_1302"

    iget-object v1, p0, Lcom/osp/app/signin/bd;->b:Lcom/msc/c/f;

    invoke-virtual {v1}, Lcom/msc/c/f;->a()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_8

    const-string v0, "AUT_1830"

    iget-object v1, p0, Lcom/osp/app/signin/bd;->b:Lcom/msc/c/f;

    invoke-virtual {v1}, Lcom/msc/c/f;->a()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_9

    .line 6884
    :cond_8
    iget-object v0, p0, Lcom/osp/app/signin/bd;->c:Lcom/osp/app/signin/AccountView;

    invoke-static {v0}, Lcom/msc/c/c;->g(Landroid/content/Context;)V

    .line 6885
    iget-object v0, p0, Lcom/osp/app/signin/bd;->c:Lcom/osp/app/signin/AccountView;

    invoke-static {v0}, Lcom/msc/c/e;->a(Landroid/content/Context;)Z

    .line 6886
    iget-object v0, p0, Lcom/osp/app/signin/bd;->c:Lcom/osp/app/signin/AccountView;

    iget-object v1, p0, Lcom/osp/app/signin/bd;->c:Lcom/osp/app/signin/AccountView;

    invoke-static {v1}, Lcom/osp/app/util/ad;->h(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/osp/app/signin/AccountView;->c(Lcom/osp/app/signin/AccountView;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 6897
    :cond_9
    invoke-static {}, Lcom/osp/device/b;->b()Lcom/osp/device/b;

    move-result-object v0

    invoke-virtual {v0}, Lcom/osp/device/b;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/osp/app/signin/bd;->g:Z

    if-nez v0, :cond_0

    const-string v0, "AUT_1094"

    iget-object v1, p0, Lcom/osp/app/signin/bd;->b:Lcom/msc/c/f;

    invoke-virtual {v1}, Lcom/msc/c/f;->a()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 6899
    iget-object v0, p0, Lcom/osp/app/signin/bd;->c:Lcom/osp/app/signin/AccountView;

    invoke-static {v0}, Lcom/osp/app/signin/AccountView;->K(Lcom/osp/app/signin/AccountView;)V
    :try_end_3
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_1

    goto/16 :goto_0

    .line 6907
    :cond_a
    iget-wide v4, p0, Lcom/osp/app/signin/bd;->f:J

    cmp-long v0, v0, v4

    if-nez v0, :cond_0

    .line 6909
    if-eqz v2, :cond_0

    .line 6911
    const-string v0, "AUT_1805"

    iget-object v1, p0, Lcom/osp/app/signin/bd;->b:Lcom/msc/c/f;

    invoke-virtual {v1}, Lcom/msc/c/f;->a()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_b

    const-string v0, "AUT_1004"

    iget-object v1, p0, Lcom/osp/app/signin/bd;->b:Lcom/msc/c/f;

    invoke-virtual {v1}, Lcom/msc/c/f;->a()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 6915
    :cond_b
    iget-object v0, p0, Lcom/osp/app/signin/bd;->c:Lcom/osp/app/signin/AccountView;

    invoke-static {v0}, Lcom/msc/c/c;->e(Landroid/content/Context;)V

    .line 6919
    iget-object v0, p0, Lcom/osp/app/signin/bd;->c:Lcom/osp/app/signin/AccountView;

    invoke-static {v0}, Lcom/msc/c/e;->i(Landroid/content/Context;)V

    .line 6921
    iput-boolean v6, p0, Lcom/osp/app/signin/bd;->j:Z

    goto/16 :goto_0
.end method

.method protected final d()V
    .locals 2

    .prologue
    .line 6616
    invoke-super {p0}, Lcom/msc/c/b;->d()V

    .line 6617
    iget-object v0, p0, Lcom/osp/app/signin/bd;->c:Lcom/osp/app/signin/AccountView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/osp/app/signin/AccountView;->b(I)V

    .line 6618
    iget-object v0, p0, Lcom/osp/app/signin/bd;->c:Lcom/osp/app/signin/AccountView;

    invoke-virtual {v0}, Lcom/osp/app/signin/AccountView;->finish()V

    .line 6619
    return-void
.end method

.method protected final synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 6569
    invoke-virtual {p0}, Lcom/osp/app/signin/bd;->a()Ljava/lang/Boolean;

    move-result-object v0

    return-object v0
.end method

.method protected final synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 6569
    check-cast p1, Ljava/lang/Boolean;

    invoke-virtual {p0, p1}, Lcom/osp/app/signin/bd;->a(Ljava/lang/Boolean;)V

    return-void
.end method

.method protected final onPreExecute()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 6623
    iget-object v0, p0, Lcom/osp/app/signin/bd;->c:Lcom/osp/app/signin/AccountView;

    invoke-static {v0}, Lcom/osp/app/signin/AccountView;->A(Lcom/osp/app/signin/AccountView;)Lcom/osp/app/signin/ba;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/osp/app/signin/bd;->c:Lcom/osp/app/signin/AccountView;

    invoke-static {v0}, Lcom/osp/app/signin/AccountView;->B(Lcom/osp/app/signin/AccountView;)Landroid/app/ProgressDialog;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/osp/app/signin/bd;->c:Lcom/osp/app/signin/AccountView;

    invoke-static {v0}, Lcom/osp/app/signin/AccountView;->B(Lcom/osp/app/signin/AccountView;)Landroid/app/ProgressDialog;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/ProgressDialog;->isShowing()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 6625
    iget-object v0, p0, Lcom/osp/app/signin/bd;->c:Lcom/osp/app/signin/AccountView;

    invoke-static {v0}, Lcom/osp/app/signin/AccountView;->B(Lcom/osp/app/signin/AccountView;)Landroid/app/ProgressDialog;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/osp/app/signin/bd;->a(Landroid/app/ProgressDialog;)V

    .line 6627
    iget-object v0, p0, Lcom/osp/app/signin/bd;->c:Lcom/osp/app/signin/AccountView;

    invoke-static {v0, v1}, Lcom/osp/app/signin/AccountView;->a(Lcom/osp/app/signin/AccountView;Landroid/app/ProgressDialog;)Landroid/app/ProgressDialog;

    .line 6628
    iget-object v0, p0, Lcom/osp/app/signin/bd;->c:Lcom/osp/app/signin/AccountView;

    invoke-static {v0}, Lcom/osp/app/signin/AccountView;->A(Lcom/osp/app/signin/AccountView;)Lcom/osp/app/signin/ba;

    move-result-object v0

    invoke-virtual {v0, v1}, Lcom/osp/app/signin/ba;->a(Landroid/app/ProgressDialog;)V

    .line 6633
    :goto_0
    return-void

    .line 6631
    :cond_0
    invoke-super {p0}, Lcom/msc/c/b;->onPreExecute()V

    goto :goto_0
.end method
