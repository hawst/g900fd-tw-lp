.class final Lcom/osp/app/signin/fw;
.super Ljava/lang/Object;
.source "NameValidationWebView.java"

# interfaces
.implements Landroid/content/DialogInterface$OnKeyListener;


# instance fields
.field final synthetic a:Lcom/osp/app/signin/NameValidationWebView;


# direct methods
.method constructor <init>(Lcom/osp/app/signin/NameValidationWebView;)V
    .locals 0

    .prologue
    .line 165
    iput-object p1, p0, Lcom/osp/app/signin/fw;->a:Lcom/osp/app/signin/NameValidationWebView;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onKey(Landroid/content/DialogInterface;ILandroid/view/KeyEvent;)Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 169
    const/4 v1, 0x4

    invoke-virtual {p3}, Landroid/view/KeyEvent;->getKeyCode()I

    move-result v2

    if-ne v1, v2, :cond_2

    .line 171
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v1, "NVWV"

    const-string v2, "ProgressDialog - back key pressed."

    invoke-static {v1, v2}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 175
    :try_start_0
    iget-object v1, p0, Lcom/osp/app/signin/fw;->a:Lcom/osp/app/signin/NameValidationWebView;

    invoke-virtual {v1}, Lcom/osp/app/signin/NameValidationWebView;->isFinishing()Z

    move-result v1

    if-nez v1, :cond_0

    iget-object v1, p0, Lcom/osp/app/signin/fw;->a:Lcom/osp/app/signin/NameValidationWebView;

    invoke-static {v1}, Lcom/osp/app/signin/NameValidationWebView;->a(Lcom/osp/app/signin/NameValidationWebView;)Landroid/app/ProgressDialog;

    move-result-object v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/osp/app/signin/fw;->a:Lcom/osp/app/signin/NameValidationWebView;

    invoke-static {v1}, Lcom/osp/app/signin/NameValidationWebView;->a(Lcom/osp/app/signin/NameValidationWebView;)Landroid/app/ProgressDialog;

    move-result-object v1

    invoke-virtual {v1}, Landroid/app/ProgressDialog;->isShowing()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 177
    iget-object v1, p0, Lcom/osp/app/signin/fw;->a:Lcom/osp/app/signin/NameValidationWebView;

    invoke-static {v1}, Lcom/osp/app/signin/NameValidationWebView;->a(Lcom/osp/app/signin/NameValidationWebView;)Landroid/app/ProgressDialog;

    move-result-object v1

    invoke-virtual {v1}, Landroid/app/ProgressDialog;->dismiss()V

    .line 178
    iget-object v1, p0, Lcom/osp/app/signin/fw;->a:Lcom/osp/app/signin/NameValidationWebView;

    const/4 v2, 0x0

    invoke-static {v1, v2}, Lcom/osp/app/signin/NameValidationWebView;->a(Lcom/osp/app/signin/NameValidationWebView;Landroid/app/ProgressDialog;)Landroid/app/ProgressDialog;

    .line 179
    iget-object v1, p0, Lcom/osp/app/signin/fw;->a:Lcom/osp/app/signin/NameValidationWebView;

    invoke-static {v1}, Lcom/osp/app/signin/NameValidationWebView;->b(Lcom/osp/app/signin/NameValidationWebView;)Landroid/webkit/WebView;

    move-result-object v1

    invoke-virtual {v1}, Landroid/webkit/WebView;->stopLoading()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 186
    :cond_0
    :goto_0
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v1, "NVWV"

    const-string v2, "does not go back."

    invoke-static {v1, v2}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 187
    iget-object v1, p0, Lcom/osp/app/signin/fw;->a:Lcom/osp/app/signin/NameValidationWebView;

    invoke-virtual {v1}, Lcom/osp/app/signin/NameValidationWebView;->getIntent()Landroid/content/Intent;

    move-result-object v1

    .line 188
    if-eqz v1, :cond_1

    const-string v2, "is_cancelable_just_one_activity"

    invoke-virtual {v1, v2, v0}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 190
    iget-object v0, p0, Lcom/osp/app/signin/fw;->a:Lcom/osp/app/signin/NameValidationWebView;

    const/16 v2, 0xe

    invoke-virtual {v0, v2, v1}, Lcom/osp/app/signin/NameValidationWebView;->a(ILandroid/content/Intent;)V

    .line 192
    :cond_1
    iget-object v0, p0, Lcom/osp/app/signin/fw;->a:Lcom/osp/app/signin/NameValidationWebView;

    invoke-virtual {v0}, Lcom/osp/app/signin/NameValidationWebView;->finish()V

    .line 193
    const/4 v0, 0x1

    .line 196
    :cond_2
    return v0

    .line 181
    :catch_0
    move-exception v1

    invoke-virtual {v1}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_0
.end method
