.class final Lcom/osp/app/signin/g;
.super Lcom/msc/c/b;
.source "AccountPreference.java"


# instance fields
.field final synthetic c:Lcom/osp/app/signin/AccountPreference;

.field private d:J


# direct methods
.method public constructor <init>(Lcom/osp/app/signin/AccountPreference;)V
    .locals 0

    .prologue
    .line 363
    iput-object p1, p0, Lcom/osp/app/signin/g;->c:Lcom/osp/app/signin/AccountPreference;

    .line 364
    invoke-direct {p0, p1}, Lcom/msc/c/b;-><init>(Landroid/content/Context;)V

    .line 368
    return-void
.end method


# virtual methods
.method protected final varargs a()Ljava/lang/Boolean;
    .locals 3

    .prologue
    .line 383
    new-instance v0, Lcom/msc/a/f;

    invoke-direct {v0}, Lcom/msc/a/f;-><init>()V

    .line 384
    iget-object v1, p0, Lcom/osp/app/signin/g;->c:Lcom/osp/app/signin/AccountPreference;

    invoke-static {v1}, Lcom/osp/app/util/ad;->h(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/msc/a/f;->d(Ljava/lang/String;)V

    .line 386
    invoke-static {}, Lcom/osp/device/b;->b()Lcom/osp/device/b;

    move-result-object v1

    invoke-virtual {v1}, Lcom/osp/device/b;->a()Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/osp/app/signin/g;->c:Lcom/osp/app/signin/AccountPreference;

    invoke-static {v1}, Lcom/osp/app/util/ad;->h(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Landroid/telephony/PhoneNumberUtils;->isGlobalPhoneNumber(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 388
    iget-object v1, p0, Lcom/osp/app/signin/g;->c:Lcom/osp/app/signin/AccountPreference;

    invoke-static {v1}, Lcom/msc/c/e;->h(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v1

    .line 389
    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 391
    const-string v2, "Y"

    invoke-virtual {v0, v2}, Lcom/msc/a/f;->b(Ljava/lang/String;)V

    .line 392
    invoke-virtual {v0, v1}, Lcom/msc/a/f;->c(Ljava/lang/String;)V

    .line 397
    :cond_0
    invoke-static {}, Lcom/msc/c/g;->a()Lcom/msc/c/g;

    iget-object v1, p0, Lcom/osp/app/signin/g;->c:Lcom/osp/app/signin/AccountPreference;

    invoke-static {v1, v0, p0}, Lcom/msc/c/g;->a(Landroid/content/Context;Lcom/msc/a/f;Lcom/msc/b/h;)J

    move-result-wide v0

    iput-wide v0, p0, Lcom/osp/app/signin/g;->d:J

    iget-wide v0, p0, Lcom/osp/app/signin/g;->d:J

    invoke-virtual {p0, v0, v1}, Lcom/osp/app/signin/g;->a(J)V

    iget-wide v0, p0, Lcom/osp/app/signin/g;->d:J

    const-string v2, "from_xml"

    invoke-virtual {p0, v0, v1, v2}, Lcom/osp/app/signin/g;->a(JLjava/lang/String;)V

    iget-wide v0, p0, Lcom/osp/app/signin/g;->d:J

    sget-object v2, Lcom/msc/b/g;->c:Lcom/msc/b/g;

    invoke-static {v0, v1, v2}, Lcom/msc/c/g;->a(JLcom/msc/b/g;)V

    .line 399
    const/4 v0, 0x0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    return-object v0
.end method

.method public final declared-synchronized a(Lcom/msc/b/c;)V
    .locals 6

    .prologue
    .line 434
    monitor-enter p0

    :try_start_0
    invoke-super {p0, p1}, Lcom/msc/c/b;->a(Lcom/msc/b/c;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 436
    if-nez p1, :cond_1

    .line 456
    :cond_0
    :goto_0
    monitor-exit p0

    return-void

    .line 441
    :cond_1
    :try_start_1
    invoke-virtual {p1}, Lcom/msc/b/c;->a()J

    move-result-wide v0

    .line 442
    invoke-virtual {p1}, Lcom/msc/b/c;->e()Ljava/lang/String;

    move-result-object v2

    .line 444
    iget-wide v4, p0, Lcom/osp/app/signin/g;->d:J
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    cmp-long v0, v0, v4

    if-nez v0, :cond_0

    .line 448
    :try_start_2
    iget-object v0, p0, Lcom/osp/app/signin/g;->c:Lcom/osp/app/signin/AccountPreference;

    invoke-static {}, Lcom/msc/c/h;->a()Lcom/msc/c/h;

    iget-object v1, p0, Lcom/osp/app/signin/g;->c:Lcom/osp/app/signin/AccountPreference;

    invoke-static {v1, v2}, Lcom/msc/c/h;->b(Landroid/content/Context;Ljava/lang/String;)Lcom/msc/a/g;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/osp/app/signin/AccountPreference;->a(Lcom/osp/app/signin/AccountPreference;Lcom/msc/a/g;)Lcom/msc/a/g;
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_0
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0

    .line 450
    :catch_0
    move-exception v0

    .line 452
    :try_start_3
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    .line 453
    new-instance v1, Lcom/msc/c/f;

    invoke-direct {v1, v0}, Lcom/msc/c/f;-><init>(Ljava/lang/Exception;)V

    iput-object v1, p0, Lcom/osp/app/signin/g;->b:Lcom/msc/c/f;
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    goto :goto_0

    .line 434
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method protected final a(Ljava/lang/Boolean;)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 404
    invoke-super {p0}, Lcom/msc/c/b;->e()V

    .line 405
    iget-object v0, p0, Lcom/osp/app/signin/g;->b:Lcom/msc/c/f;

    if-eqz v0, :cond_2

    .line 407
    invoke-static {}, Lcom/osp/device/b;->b()Lcom/osp/device/b;

    move-result-object v0

    invoke-virtual {v0}, Lcom/osp/device/b;->a()Z

    move-result v0

    if-eqz v0, :cond_1

    const-string v0, "USR_3174"

    iget-object v1, p0, Lcom/osp/app/signin/g;->b:Lcom/msc/c/f;

    invoke-virtual {v1}, Lcom/msc/c/f;->a()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    const/4 v1, 0x1

    if-ne v0, v1, :cond_1

    .line 409
    iget-object v0, p0, Lcom/osp/app/signin/g;->c:Lcom/osp/app/signin/AccountPreference;

    iget-object v1, p0, Lcom/osp/app/signin/g;->c:Lcom/osp/app/signin/AccountPreference;

    invoke-static {v1}, Lcom/osp/app/util/ad;->h(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/osp/app/signin/AccountPreference;->a(Lcom/osp/app/signin/AccountPreference;Ljava/lang/String;)V

    .line 410
    iget-object v0, p0, Lcom/osp/app/signin/g;->c:Lcom/osp/app/signin/AccountPreference;

    invoke-virtual {v0}, Lcom/osp/app/signin/AccountPreference;->finish()V

    .line 430
    :cond_0
    :goto_0
    return-void

    .line 414
    :cond_1
    invoke-virtual {p0, v2}, Lcom/osp/app/signin/g;->a(Z)V

    .line 415
    iget-object v0, p0, Lcom/osp/app/signin/g;->c:Lcom/osp/app/signin/AccountPreference;

    invoke-virtual {v0}, Lcom/osp/app/signin/AccountPreference;->finish()V

    goto :goto_0

    .line 419
    :cond_2
    iget-object v0, p0, Lcom/osp/app/signin/g;->c:Lcom/osp/app/signin/AccountPreference;

    invoke-static {v0}, Lcom/osp/app/signin/AccountPreference;->a(Lcom/osp/app/signin/AccountPreference;)Lcom/msc/a/g;

    move-result-object v0

    if-eqz v0, :cond_3

    .line 421
    iget-object v0, p0, Lcom/osp/app/signin/g;->c:Lcom/osp/app/signin/AccountPreference;

    invoke-static {v0}, Lcom/osp/app/signin/AccountPreference;->a(Lcom/osp/app/signin/AccountPreference;)Lcom/msc/a/g;

    move-result-object v0

    invoke-virtual {v0}, Lcom/msc/a/g;->h()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 423
    iget-object v0, p0, Lcom/osp/app/signin/g;->c:Lcom/osp/app/signin/AccountPreference;

    iget-object v1, p0, Lcom/osp/app/signin/g;->c:Lcom/osp/app/signin/AccountPreference;

    invoke-static {v1}, Lcom/osp/app/util/ad;->g(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/osp/app/signin/AccountPreference;->b(Lcom/osp/app/signin/AccountPreference;Ljava/lang/String;)V

    goto :goto_0

    .line 427
    :cond_3
    invoke-virtual {p0, v2}, Lcom/osp/app/signin/g;->a(Z)V

    .line 428
    iget-object v0, p0, Lcom/osp/app/signin/g;->c:Lcom/osp/app/signin/AccountPreference;

    invoke-virtual {v0}, Lcom/osp/app/signin/AccountPreference;->finish()V

    goto :goto_0
.end method

.method protected final d()V
    .locals 1

    .prologue
    .line 372
    invoke-super {p0}, Lcom/msc/c/b;->d()V

    .line 373
    iget-object v0, p0, Lcom/osp/app/signin/g;->c:Lcom/osp/app/signin/AccountPreference;

    invoke-virtual {v0}, Lcom/osp/app/signin/AccountPreference;->finish()V

    .line 374
    return-void
.end method

.method protected final synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 359
    invoke-virtual {p0}, Lcom/osp/app/signin/g;->a()Ljava/lang/Boolean;

    move-result-object v0

    return-object v0
.end method

.method protected final synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 359
    check-cast p1, Ljava/lang/Boolean;

    invoke-virtual {p0, p1}, Lcom/osp/app/signin/g;->a(Ljava/lang/Boolean;)V

    return-void
.end method
