.class final Lcom/osp/app/signin/jy;
.super Lcom/msc/c/b;
.source "SignInView.java"


# instance fields
.field final synthetic c:Lcom/osp/app/signin/SignInView;

.field private d:J

.field private e:J

.field private f:J

.field private g:Z

.field private h:Ljava/lang/String;


# direct methods
.method public constructor <init>(Lcom/osp/app/signin/SignInView;Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 8330
    iput-object p1, p0, Lcom/osp/app/signin/jy;->c:Lcom/osp/app/signin/SignInView;

    .line 8331
    const/4 v0, 0x1

    invoke-direct {p0, p2, v0}, Lcom/msc/c/b;-><init>(Landroid/content/Context;Z)V

    .line 8318
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/osp/app/signin/jy;->g:Z

    .line 8332
    invoke-virtual {p0}, Lcom/osp/app/signin/jy;->c()V

    .line 8333
    return-void
.end method

.method private h()V
    .locals 4

    .prologue
    .line 8490
    invoke-static {}, Lcom/msc/c/g;->a()Lcom/msc/c/g;

    .line 8491
    iget-object v0, p0, Lcom/osp/app/signin/jy;->c:Lcom/osp/app/signin/SignInView;

    const-string v1, "j5p7ll8g33"

    iget-object v2, p0, Lcom/osp/app/signin/jy;->c:Lcom/osp/app/signin/SignInView;

    invoke-static {v2}, Lcom/msc/c/e;->h(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v2

    iget-object v3, p0, Lcom/osp/app/signin/jy;->c:Lcom/osp/app/signin/SignInView;

    invoke-static {v3}, Lcom/msc/c/e;->b(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v3

    invoke-static {v0, v1, v2, v3, p0}, Lcom/msc/c/g;->i(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/msc/b/h;)J

    move-result-wide v0

    iput-wide v0, p0, Lcom/osp/app/signin/jy;->d:J

    .line 8493
    iget-wide v0, p0, Lcom/osp/app/signin/jy;->d:J

    invoke-virtual {p0, v0, v1}, Lcom/osp/app/signin/jy;->a(J)V

    .line 8494
    iget-wide v0, p0, Lcom/osp/app/signin/jy;->d:J

    const-string v2, "from_xml"

    invoke-virtual {p0, v0, v1, v2}, Lcom/osp/app/signin/jy;->a(JLjava/lang/String;)V

    .line 8495
    iget-wide v0, p0, Lcom/osp/app/signin/jy;->d:J

    sget-object v2, Lcom/msc/b/g;->c:Lcom/msc/b/g;

    invoke-static {v0, v1, v2}, Lcom/msc/c/g;->a(JLcom/msc/b/g;)V

    .line 8496
    return-void
.end method


# virtual methods
.method protected final varargs a()Ljava/lang/Boolean;
    .locals 1

    .prologue
    .line 8354
    invoke-direct {p0}, Lcom/osp/app/signin/jy;->h()V

    .line 8355
    const/4 v0, 0x0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    return-object v0
.end method

.method public final declared-synchronized a(Lcom/msc/b/c;)V
    .locals 6

    .prologue
    .line 8368
    monitor-enter p0

    :try_start_0
    invoke-super {p0, p1}, Lcom/msc/c/b;->a(Lcom/msc/b/c;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 8370
    if-nez p1, :cond_1

    .line 8431
    :cond_0
    :goto_0
    monitor-exit p0

    return-void

    .line 8375
    :cond_1
    :try_start_1
    invoke-virtual {p1}, Lcom/msc/b/c;->a()J

    move-result-wide v0

    .line 8376
    invoke-virtual {p1}, Lcom/msc/b/c;->e()Ljava/lang/String;

    move-result-object v2

    .line 8378
    iget-wide v4, p0, Lcom/osp/app/signin/jy;->d:J
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    cmp-long v3, v0, v4

    if-nez v3, :cond_3

    .line 8382
    :try_start_2
    invoke-static {}, Lcom/msc/c/h;->a()Lcom/msc/c/h;

    invoke-static {v2}, Lcom/msc/c/h;->O(Ljava/lang/String;)Z

    move-result v0

    .line 8384
    const/4 v1, 0x1

    if-ne v0, v1, :cond_2

    .line 8386
    const-string v0, "Success"

    iput-object v0, p0, Lcom/osp/app/signin/jy;->h:Ljava/lang/String;
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_0
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0

    .line 8391
    :catch_0
    move-exception v0

    :try_start_3
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    .line 8394
    const-string v0, "Fail"

    iput-object v0, p0, Lcom/osp/app/signin/jy;->h:Ljava/lang/String;
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    goto :goto_0

    .line 8368
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 8389
    :cond_2
    :try_start_4
    const-string v0, "Fail"

    iput-object v0, p0, Lcom/osp/app/signin/jy;->h:Ljava/lang/String;
    :try_end_4
    .catch Ljava/lang/Exception; {:try_start_4 .. :try_end_4} :catch_0
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    goto :goto_0

    .line 8397
    :cond_3
    :try_start_5
    iget-wide v4, p0, Lcom/osp/app/signin/jy;->e:J
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    cmp-long v3, v0, v4

    if-nez v3, :cond_4

    .line 8401
    :try_start_6
    invoke-static {}, Lcom/msc/c/h;->a()Lcom/msc/c/h;

    invoke-static {v2}, Lcom/msc/c/h;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 8402
    invoke-virtual {p0}, Lcom/osp/app/signin/jy;->isCancelled()Z

    move-result v0

    if-nez v0, :cond_0

    .line 8406
    invoke-static {}, Lcom/msc/c/g;->a()Lcom/msc/c/g;

    iget-object v0, p0, Lcom/osp/app/signin/jy;->c:Lcom/osp/app/signin/SignInView;

    const-string v1, "authorization_code"

    const-string v3, "j5p7ll8g33"

    const-string v4, "5763D0052DC1462E13751F753384E9A9"

    move-object v5, p0

    invoke-static/range {v0 .. v5}, Lcom/msc/c/g;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/msc/b/h;)J

    move-result-wide v0

    iput-wide v0, p0, Lcom/osp/app/signin/jy;->f:J

    iget-wide v0, p0, Lcom/osp/app/signin/jy;->f:J

    invoke-virtual {p0, v0, v1}, Lcom/osp/app/signin/jy;->a(J)V

    iget-wide v0, p0, Lcom/osp/app/signin/jy;->f:J

    const-string v2, "from_json"

    invoke-virtual {p0, v0, v1, v2}, Lcom/osp/app/signin/jy;->a(JLjava/lang/String;)V

    iget-wide v0, p0, Lcom/osp/app/signin/jy;->f:J

    sget-object v2, Lcom/msc/b/g;->c:Lcom/msc/b/g;

    invoke-static {v0, v1, v2}, Lcom/msc/c/g;->a(JLcom/msc/b/g;)V
    :try_end_6
    .catch Ljava/lang/Exception; {:try_start_6 .. :try_end_6} :catch_1
    .catchall {:try_start_6 .. :try_end_6} :catchall_0

    goto :goto_0

    .line 8407
    :catch_1
    move-exception v0

    .line 8409
    :try_start_7
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    .line 8410
    new-instance v1, Lcom/msc/c/f;

    invoke-direct {v1, v0}, Lcom/msc/c/f;-><init>(Ljava/lang/Exception;)V

    iput-object v1, p0, Lcom/osp/app/signin/jy;->b:Lcom/msc/c/f;

    .line 8411
    const-string v0, "Fail"

    iput-object v0, p0, Lcom/osp/app/signin/jy;->h:Ljava/lang/String;

    goto :goto_0

    .line 8413
    :cond_4
    iget-wide v4, p0, Lcom/osp/app/signin/jy;->f:J
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_0

    cmp-long v0, v0, v4

    if-nez v0, :cond_0

    .line 8417
    :try_start_8
    invoke-static {}, Lcom/msc/c/h;->a()Lcom/msc/c/h;

    iget-object v0, p0, Lcom/osp/app/signin/jy;->c:Lcom/osp/app/signin/SignInView;

    invoke-static {v0, v2}, Lcom/msc/c/h;->a(Landroid/content/Context;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 8418
    iget-object v1, p0, Lcom/osp/app/signin/jy;->c:Lcom/osp/app/signin/SignInView;

    invoke-static {v1, v0}, Lcom/msc/c/e;->b(Landroid/content/Context;Ljava/lang/String;)V

    .line 8419
    invoke-virtual {p0}, Lcom/osp/app/signin/jy;->isCancelled()Z

    move-result v0

    if-nez v0, :cond_0

    .line 8423
    invoke-direct {p0}, Lcom/osp/app/signin/jy;->h()V
    :try_end_8
    .catch Ljava/lang/Exception; {:try_start_8 .. :try_end_8} :catch_2
    .catchall {:try_start_8 .. :try_end_8} :catchall_0

    goto/16 :goto_0

    .line 8424
    :catch_2
    move-exception v0

    .line 8426
    :try_start_9
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    .line 8427
    new-instance v1, Lcom/msc/c/f;

    invoke-direct {v1, v0}, Lcom/msc/c/f;-><init>(Ljava/lang/Exception;)V

    iput-object v1, p0, Lcom/osp/app/signin/jy;->b:Lcom/msc/c/f;

    .line 8428
    const-string v0, "Fail"

    iput-object v0, p0, Lcom/osp/app/signin/jy;->h:Ljava/lang/String;
    :try_end_9
    .catchall {:try_start_9 .. :try_end_9} :catchall_0

    goto/16 :goto_0
.end method

.method protected final a(Ljava/lang/Boolean;)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 8337
    invoke-super {p0, p1}, Lcom/msc/c/b;->a(Ljava/lang/Boolean;)V

    .line 8339
    const-string v0, "Fail"

    iget-object v1, p0, Lcom/osp/app/signin/jy;->h:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 8341
    invoke-static {}, Lcom/osp/app/signin/SamsungService;->a()Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/osp/app/signin/jy;->c:Lcom/osp/app/signin/SignInView;

    invoke-static {v0, v2}, Lcom/osp/app/signin/SignInView;->c(Lcom/osp/app/signin/SignInView;Z)V

    .line 8349
    :cond_0
    :goto_0
    return-void

    .line 8341
    :cond_1
    iget-object v0, p0, Lcom/osp/app/signin/jy;->c:Lcom/osp/app/signin/SignInView;

    invoke-static {v0}, Lcom/osp/app/signin/SignInView;->e(Lcom/osp/app/signin/SignInView;)Z

    move-result v0

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/osp/app/signin/jy;->c:Lcom/osp/app/signin/SignInView;

    invoke-static {v0}, Lcom/osp/app/signin/SignInView;->s(Lcom/osp/app/signin/SignInView;)Landroid/view/View;

    move-result-object v0

    const v1, 0x7f0c0129

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/EditText;

    :goto_1
    iget-object v1, p0, Lcom/osp/app/signin/jy;->c:Lcom/osp/app/signin/SignInView;

    invoke-virtual {v0}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    sget-object v2, Ljava/util/Locale;->ENGLISH:Ljava/util/Locale;

    invoke-virtual {v0, v2}, Ljava/lang/String;->toLowerCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Lcom/osp/app/signin/SignInView;->g(Lcom/osp/app/signin/SignInView;Ljava/lang/String;)V

    goto :goto_0

    :cond_2
    iget-object v0, p0, Lcom/osp/app/signin/jy;->c:Lcom/osp/app/signin/SignInView;

    const v1, 0x7f0c0121

    invoke-virtual {v0, v1}, Lcom/osp/app/signin/SignInView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/EditText;

    goto :goto_1

    .line 8343
    :cond_3
    const-string v0, "Success"

    iget-object v1, p0, Lcom/osp/app/signin/jy;->h:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 8345
    iget-object v0, p0, Lcom/osp/app/signin/jy;->c:Lcom/osp/app/signin/SignInView;

    invoke-static {v0}, Lcom/osp/app/util/r;->p(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_4

    iget-object v0, p0, Lcom/osp/app/signin/jy;->c:Lcom/osp/app/signin/SignInView;

    invoke-static {v0}, Lcom/osp/app/signin/SignInView;->X(Lcom/osp/app/signin/SignInView;)Z

    move-result v0

    if-eqz v0, :cond_4

    invoke-static {}, Lcom/osp/app/signin/SamsungService;->a()Z

    move-result v0

    if-eqz v0, :cond_4

    invoke-static {}, Lcom/osp/app/util/m;->a()Lcom/osp/app/util/m;

    iget-object v0, p0, Lcom/osp/app/signin/jy;->c:Lcom/osp/app/signin/SignInView;

    invoke-static {v0, v2}, Lcom/osp/app/util/m;->a(Landroid/app/Activity;Z)V

    :goto_2
    iget-object v0, p0, Lcom/osp/app/signin/jy;->c:Lcom/osp/app/signin/SignInView;

    invoke-static {v0}, Landroid/preference/PreferenceManager;->getDefaultSharedPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    const-string v1, "EMAIL_VALIDATION_KEY"

    iget-object v2, p0, Lcom/osp/app/signin/jy;->c:Lcom/osp/app/signin/SignInView;

    invoke-static {v2}, Lcom/osp/app/util/ad;->g(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    goto :goto_0

    :cond_4
    iget-object v0, p0, Lcom/osp/app/signin/jy;->c:Lcom/osp/app/signin/SignInView;

    const/4 v1, 0x1

    invoke-static {v0, v1}, Lcom/osp/app/signin/SignInView;->c(Lcom/osp/app/signin/SignInView;Z)V

    goto :goto_2
.end method

.method public final b(Lcom/msc/b/c;)V
    .locals 4

    .prologue
    .line 8435
    invoke-super {p0, p1}, Lcom/msc/c/b;->b(Lcom/msc/b/c;)V

    .line 8437
    if-nez p1, :cond_1

    .line 8484
    :cond_0
    :goto_0
    return-void

    .line 8442
    :cond_1
    invoke-virtual {p1}, Lcom/msc/b/c;->a()J

    move-result-wide v0

    .line 8445
    iget-wide v2, p0, Lcom/osp/app/signin/jy;->d:J

    cmp-long v2, v0, v2

    if-nez v2, :cond_3

    .line 8447
    iget-boolean v0, p0, Lcom/osp/app/signin/jy;->g:Z

    if-nez v0, :cond_2

    const-string v0, "ACF_0403"

    iget-object v1, p0, Lcom/osp/app/signin/jy;->b:Lcom/msc/c/f;

    invoke-virtual {v1}, Lcom/msc/c/f;->a()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 8451
    const/4 v0, 0x0

    :try_start_0
    iput-object v0, p0, Lcom/osp/app/signin/jy;->b:Lcom/msc/c/f;

    .line 8452
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/osp/app/signin/jy;->g:Z

    .line 8453
    iget-object v0, p0, Lcom/osp/app/signin/jy;->c:Lcom/osp/app/signin/SignInView;

    const-string v1, ""

    invoke-static {v0, v1}, Lcom/msc/c/e;->b(Landroid/content/Context;Ljava/lang/String;)V

    .line 8454
    invoke-virtual {p0}, Lcom/osp/app/signin/jy;->isCancelled()Z

    move-result v0

    if-nez v0, :cond_0

    .line 8458
    invoke-static {}, Lcom/msc/c/g;->a()Lcom/msc/c/g;

    iget-object v0, p0, Lcom/osp/app/signin/jy;->c:Lcom/osp/app/signin/SignInView;

    const-string v1, "j5p7ll8g33"

    iget-object v2, p0, Lcom/osp/app/signin/jy;->c:Lcom/osp/app/signin/SignInView;

    invoke-static {v2}, Lcom/msc/c/e;->g(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x0

    invoke-static {v0, v1, v2, v3, p0}, Lcom/msc/c/g;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/msc/b/h;)J

    move-result-wide v0

    iput-wide v0, p0, Lcom/osp/app/signin/jy;->e:J

    iget-wide v0, p0, Lcom/osp/app/signin/jy;->e:J

    invoke-virtual {p0, v0, v1}, Lcom/osp/app/signin/jy;->a(J)V

    iget-wide v0, p0, Lcom/osp/app/signin/jy;->e:J

    const-string v2, "from_json"

    invoke-virtual {p0, v0, v1, v2}, Lcom/osp/app/signin/jy;->a(JLjava/lang/String;)V

    iget-wide v0, p0, Lcom/osp/app/signin/jy;->e:J

    sget-object v2, Lcom/msc/b/g;->b:Lcom/msc/b/g;

    invoke-static {v0, v1, v2}, Lcom/msc/c/g;->a(JLcom/msc/b/g;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 8459
    :catch_0
    move-exception v0

    .line 8461
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    .line 8462
    new-instance v1, Lcom/msc/c/f;

    invoke-direct {v1, v0}, Lcom/msc/c/f;-><init>(Ljava/lang/Exception;)V

    iput-object v1, p0, Lcom/osp/app/signin/jy;->b:Lcom/msc/c/f;

    .line 8463
    const-string v0, "Fail"

    iput-object v0, p0, Lcom/osp/app/signin/jy;->h:Ljava/lang/String;

    goto :goto_0

    .line 8467
    :cond_2
    const-string v0, "Fail"

    iput-object v0, p0, Lcom/osp/app/signin/jy;->h:Ljava/lang/String;

    goto :goto_0

    .line 8470
    :cond_3
    iget-wide v2, p0, Lcom/osp/app/signin/jy;->e:J

    cmp-long v2, v0, v2

    if-nez v2, :cond_4

    .line 8472
    const-string v0, "Fail"

    iput-object v0, p0, Lcom/osp/app/signin/jy;->h:Ljava/lang/String;

    goto :goto_0

    .line 8480
    :cond_4
    iget-wide v2, p0, Lcom/osp/app/signin/jy;->f:J

    cmp-long v0, v0, v2

    if-nez v0, :cond_0

    .line 8482
    const-string v0, "Fail"

    iput-object v0, p0, Lcom/osp/app/signin/jy;->h:Ljava/lang/String;

    goto/16 :goto_0
.end method

.method protected final d()V
    .locals 3

    .prologue
    .line 8360
    invoke-super {p0}, Lcom/msc/c/b;->d()V

    .line 8361
    iget-object v0, p0, Lcom/osp/app/signin/jy;->c:Lcom/osp/app/signin/SignInView;

    const/4 v1, 0x0

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Lcom/osp/app/signin/SignInView;->a(ILandroid/content/Intent;)V

    .line 8362
    iget-object v0, p0, Lcom/osp/app/signin/jy;->c:Lcom/osp/app/signin/SignInView;

    invoke-virtual {v0}, Lcom/osp/app/signin/SignInView;->finish()V

    .line 8364
    return-void
.end method

.method protected final synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 8301
    invoke-virtual {p0}, Lcom/osp/app/signin/jy;->a()Ljava/lang/Boolean;

    move-result-object v0

    return-object v0
.end method

.method protected final synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 8301
    check-cast p1, Ljava/lang/Boolean;

    invoke-virtual {p0, p1}, Lcom/osp/app/signin/jy;->a(Ljava/lang/Boolean;)V

    return-void
.end method
