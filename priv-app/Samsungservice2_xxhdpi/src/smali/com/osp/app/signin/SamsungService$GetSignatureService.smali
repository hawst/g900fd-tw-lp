.class public Lcom/osp/app/signin/SamsungService$GetSignatureService;
.super Lcom/osp/app/util/AbstractBaseService;
.source "SamsungService.java"


# instance fields
.field private a:Lcom/osp/app/signin/gy;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1206
    invoke-direct {p0}, Lcom/osp/app/util/AbstractBaseService;-><init>()V

    .line 1278
    return-void
.end method


# virtual methods
.method public onBind(Landroid/content/Intent;)Landroid/os/IBinder;
    .locals 1

    .prologue
    .line 1270
    const/4 v0, 0x0

    return-object v0
.end method

.method public onCreate()V
    .locals 0

    .prologue
    .line 1228
    invoke-super {p0}, Lcom/osp/app/util/AbstractBaseService;->onCreate()V

    .line 1229
    return-void
.end method

.method public onDestroy()V
    .locals 2

    .prologue
    .line 1235
    invoke-super {p0}, Lcom/osp/app/util/AbstractBaseService;->onDestroy()V

    .line 1237
    iget-object v0, p0, Lcom/osp/app/signin/SamsungService$GetSignatureService;->a:Lcom/osp/app/signin/gy;

    if-eqz v0, :cond_1

    .line 1239
    iget-object v0, p0, Lcom/osp/app/signin/SamsungService$GetSignatureService;->a:Lcom/osp/app/signin/gy;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/osp/app/signin/SamsungService$GetSignatureService;->a:Lcom/osp/app/signin/gy;

    invoke-virtual {v0}, Lcom/osp/app/signin/gy;->getStatus()Landroid/os/AsyncTask$Status;

    move-result-object v0

    sget-object v1, Landroid/os/AsyncTask$Status;->FINISHED:Landroid/os/AsyncTask$Status;

    if-eq v0, v1, :cond_0

    .line 1241
    iget-object v0, p0, Lcom/osp/app/signin/SamsungService$GetSignatureService;->a:Lcom/osp/app/signin/gy;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/osp/app/signin/gy;->cancel(Z)Z

    .line 1243
    :cond_0
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/osp/app/signin/SamsungService$GetSignatureService;->a:Lcom/osp/app/signin/gy;

    .line 1245
    :cond_1
    return-void
.end method

.method public onStartCommand(Landroid/content/Intent;II)I
    .locals 1

    .prologue
    .line 1250
    if-eqz p1, :cond_0

    .line 1254
    :try_start_0
    new-instance v0, Lcom/osp/app/signin/gy;

    invoke-direct {v0, p0}, Lcom/osp/app/signin/gy;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/osp/app/signin/SamsungService$GetSignatureService;->a:Lcom/osp/app/signin/gy;

    .line 1255
    iget-object v0, p0, Lcom/osp/app/signin/SamsungService$GetSignatureService;->a:Lcom/osp/app/signin/gy;

    invoke-virtual {v0}, Lcom/osp/app/signin/gy;->b()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 1261
    :goto_0
    invoke-virtual {p0}, Lcom/osp/app/signin/SamsungService$GetSignatureService;->stopSelf()V

    .line 1262
    invoke-super {p0, p1, p2, p3}, Lcom/osp/app/util/AbstractBaseService;->onStartCommand(Landroid/content/Intent;II)I

    move-result v0

    .line 1264
    :goto_1
    return v0

    .line 1256
    :catch_0
    move-exception v0

    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_0

    .line 1264
    :cond_0
    const/4 v0, 0x2

    goto :goto_1
.end method
