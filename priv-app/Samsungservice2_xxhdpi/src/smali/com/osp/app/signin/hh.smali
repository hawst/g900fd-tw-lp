.class final Lcom/osp/app/signin/hh;
.super Lcom/msc/c/b;
.source "SecurityInfoChangeActivity.java"


# instance fields
.field final synthetic c:Lcom/osp/app/signin/SecurityInfoChangeActivity;

.field private d:J

.field private e:J

.field private f:J

.field private g:J

.field private h:Z


# direct methods
.method public constructor <init>(Lcom/osp/app/signin/SecurityInfoChangeActivity;)V
    .locals 1

    .prologue
    .line 678
    iput-object p1, p0, Lcom/osp/app/signin/hh;->c:Lcom/osp/app/signin/SecurityInfoChangeActivity;

    .line 679
    invoke-direct {p0, p1}, Lcom/msc/c/b;-><init>(Landroid/content/Context;)V

    .line 673
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/osp/app/signin/hh;->h:Z

    .line 687
    return-void
.end method

.method private b(Ljava/lang/String;)V
    .locals 3

    .prologue
    .line 879
    iget-object v0, p0, Lcom/osp/app/signin/hh;->c:Lcom/osp/app/signin/SecurityInfoChangeActivity;

    invoke-static {v0}, Lcom/osp/app/util/ad;->h(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v0

    .line 880
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_0

    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 890
    :cond_0
    :goto_0
    return-void

    .line 885
    :cond_1
    invoke-static {}, Lcom/msc/c/g;->a()Lcom/msc/c/g;

    .line 886
    iget-object v1, p0, Lcom/osp/app/signin/hh;->c:Lcom/osp/app/signin/SecurityInfoChangeActivity;

    invoke-static {v1, p1, v0, p0}, Lcom/msc/c/g;->j(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Lcom/msc/b/h;)J

    move-result-wide v0

    iput-wide v0, p0, Lcom/osp/app/signin/hh;->d:J

    .line 887
    iget-wide v0, p0, Lcom/osp/app/signin/hh;->d:J

    invoke-virtual {p0, v0, v1}, Lcom/osp/app/signin/hh;->a(J)V

    .line 888
    iget-wide v0, p0, Lcom/osp/app/signin/hh;->d:J

    const-string v2, "from_xml"

    invoke-virtual {p0, v0, v1, v2}, Lcom/osp/app/signin/hh;->a(JLjava/lang/String;)V

    .line 889
    iget-wide v0, p0, Lcom/osp/app/signin/hh;->d:J

    sget-object v2, Lcom/msc/b/g;->b:Lcom/msc/b/g;

    invoke-static {v0, v1, v2}, Lcom/msc/c/g;->a(JLcom/msc/b/g;)V

    goto :goto_0
.end method

.method private h()V
    .locals 4

    .prologue
    .line 896
    invoke-static {}, Lcom/msc/c/g;->a()Lcom/msc/c/g;

    .line 897
    iget-object v0, p0, Lcom/osp/app/signin/hh;->c:Lcom/osp/app/signin/SecurityInfoChangeActivity;

    const-string v1, "j5p7ll8g33"

    iget-object v2, p0, Lcom/osp/app/signin/hh;->c:Lcom/osp/app/signin/SecurityInfoChangeActivity;

    invoke-static {v2}, Lcom/msc/c/e;->g(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x0

    invoke-static {v0, v1, v2, v3, p0}, Lcom/msc/c/g;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/msc/b/h;)J

    move-result-wide v0

    iput-wide v0, p0, Lcom/osp/app/signin/hh;->f:J

    .line 899
    iget-wide v0, p0, Lcom/osp/app/signin/hh;->f:J

    invoke-virtual {p0, v0, v1}, Lcom/osp/app/signin/hh;->a(J)V

    .line 900
    iget-wide v0, p0, Lcom/osp/app/signin/hh;->f:J

    const-string v2, "from_json"

    invoke-virtual {p0, v0, v1, v2}, Lcom/osp/app/signin/hh;->a(JLjava/lang/String;)V

    .line 901
    iget-wide v0, p0, Lcom/osp/app/signin/hh;->f:J

    sget-object v2, Lcom/msc/b/g;->b:Lcom/msc/b/g;

    invoke-static {v0, v1, v2}, Lcom/msc/c/g;->a(JLcom/msc/b/g;)V

    .line 902
    return-void
.end method


# virtual methods
.method protected final varargs a()Ljava/lang/Boolean;
    .locals 3

    .prologue
    .line 698
    iget-object v0, p0, Lcom/osp/app/signin/hh;->c:Lcom/osp/app/signin/SecurityInfoChangeActivity;

    invoke-static {v0}, Lcom/msc/c/e;->b(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v0

    .line 699
    if-eqz v0, :cond_0

    .line 701
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v1, "SICA"

    const-string v2, "AccessToken Exist requestSecurityQuestion"

    invoke-static {v1, v2}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 702
    invoke-direct {p0, v0}, Lcom/osp/app/signin/hh;->b(Ljava/lang/String;)V

    .line 709
    :goto_0
    const/4 v0, 0x0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    return-object v0

    .line 705
    :cond_0
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v0, "SICA"

    const-string v1, "AccessToken NOT Exist requestAuthCode"

    invoke-static {v0, v1}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 706
    invoke-direct {p0}, Lcom/osp/app/signin/hh;->h()V

    goto :goto_0
.end method

.method public final declared-synchronized a(Lcom/msc/b/c;)V
    .locals 6

    .prologue
    .line 760
    monitor-enter p0

    :try_start_0
    invoke-super {p0, p1}, Lcom/msc/c/b;->a(Lcom/msc/b/c;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 762
    if-nez p1, :cond_1

    .line 835
    :cond_0
    :goto_0
    monitor-exit p0

    return-void

    .line 767
    :cond_1
    :try_start_1
    invoke-virtual {p1}, Lcom/msc/b/c;->a()J

    move-result-wide v0

    .line 768
    invoke-virtual {p1}, Lcom/msc/b/c;->e()Ljava/lang/String;

    move-result-object v2

    .line 770
    iget-wide v4, p0, Lcom/osp/app/signin/hh;->d:J
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    cmp-long v3, v0, v4

    if-nez v3, :cond_2

    .line 774
    :try_start_2
    iget-object v0, p0, Lcom/osp/app/signin/hh;->c:Lcom/osp/app/signin/SecurityInfoChangeActivity;

    invoke-static {}, Lcom/msc/c/h;->a()Lcom/msc/c/h;

    invoke-static {v2}, Lcom/msc/c/h;->I(Ljava/lang/String;)Lcom/msc/a/j;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/osp/app/signin/SecurityInfoChangeActivity;->a(Lcom/osp/app/signin/SecurityInfoChangeActivity;Lcom/msc/a/j;)Lcom/msc/a/j;
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_0
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0

    .line 775
    :catch_0
    move-exception v0

    .line 777
    :try_start_3
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    .line 778
    new-instance v1, Lcom/msc/c/f;

    invoke-direct {v1, v0}, Lcom/msc/c/f;-><init>(Ljava/lang/Exception;)V

    iput-object v1, p0, Lcom/osp/app/signin/hh;->b:Lcom/msc/c/f;
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    goto :goto_0

    .line 760
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 780
    :cond_2
    :try_start_4
    iget-wide v4, p0, Lcom/osp/app/signin/hh;->g:J
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    cmp-long v3, v0, v4

    if-nez v3, :cond_4

    .line 784
    :try_start_5
    invoke-static {}, Lcom/msc/c/h;->a()Lcom/msc/c/h;

    invoke-static {v2}, Lcom/msc/c/h;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 785
    if-eqz v0, :cond_3

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v1

    if-lez v1, :cond_3

    .line 787
    iget-object v1, p0, Lcom/osp/app/signin/hh;->c:Lcom/osp/app/signin/SecurityInfoChangeActivity;

    invoke-static {v1, v0}, Lcom/msc/c/e;->e(Landroid/content/Context;Ljava/lang/String;)V

    .line 788
    invoke-virtual {p0}, Lcom/osp/app/signin/hh;->isCancelled()Z

    move-result v0

    if-nez v0, :cond_0

    .line 792
    invoke-direct {p0}, Lcom/osp/app/signin/hh;->h()V
    :try_end_5
    .catch Ljava/lang/Exception; {:try_start_5 .. :try_end_5} :catch_1
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    goto :goto_0

    .line 798
    :catch_1
    move-exception v0

    .line 800
    :try_start_6
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    .line 801
    new-instance v1, Lcom/msc/c/f;

    invoke-direct {v1, v0}, Lcom/msc/c/f;-><init>(Ljava/lang/Exception;)V

    iput-object v1, p0, Lcom/osp/app/signin/hh;->b:Lcom/msc/c/f;
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_0

    goto :goto_0

    .line 796
    :cond_3
    :try_start_7
    new-instance v0, Lcom/msc/c/f;

    invoke-direct {v0}, Lcom/msc/c/f;-><init>()V

    iput-object v0, p0, Lcom/osp/app/signin/hh;->b:Lcom/msc/c/f;
    :try_end_7
    .catch Ljava/lang/Exception; {:try_start_7 .. :try_end_7} :catch_1
    .catchall {:try_start_7 .. :try_end_7} :catchall_0

    goto :goto_0

    .line 803
    :cond_4
    :try_start_8
    iget-wide v4, p0, Lcom/osp/app/signin/hh;->f:J
    :try_end_8
    .catchall {:try_start_8 .. :try_end_8} :catchall_0

    cmp-long v3, v0, v4

    if-nez v3, :cond_5

    .line 807
    :try_start_9
    invoke-static {}, Lcom/msc/c/h;->a()Lcom/msc/c/h;

    invoke-static {v2}, Lcom/msc/c/h;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 808
    invoke-virtual {p0}, Lcom/osp/app/signin/hh;->isCancelled()Z

    move-result v0

    if-nez v0, :cond_0

    .line 812
    invoke-static {}, Lcom/msc/c/g;->a()Lcom/msc/c/g;

    iget-object v0, p0, Lcom/osp/app/signin/hh;->c:Lcom/osp/app/signin/SecurityInfoChangeActivity;

    const-string v1, "authorization_code"

    const-string v3, "j5p7ll8g33"

    const-string v4, "5763D0052DC1462E13751F753384E9A9"

    move-object v5, p0

    invoke-static/range {v0 .. v5}, Lcom/msc/c/g;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/msc/b/h;)J

    move-result-wide v0

    iput-wide v0, p0, Lcom/osp/app/signin/hh;->e:J

    iget-wide v0, p0, Lcom/osp/app/signin/hh;->e:J

    invoke-virtual {p0, v0, v1}, Lcom/osp/app/signin/hh;->a(J)V

    iget-wide v0, p0, Lcom/osp/app/signin/hh;->e:J

    const-string v2, "from_json"

    invoke-virtual {p0, v0, v1, v2}, Lcom/osp/app/signin/hh;->a(JLjava/lang/String;)V

    iget-wide v0, p0, Lcom/osp/app/signin/hh;->e:J

    sget-object v2, Lcom/msc/b/g;->c:Lcom/msc/b/g;

    invoke-static {v0, v1, v2}, Lcom/msc/c/g;->a(JLcom/msc/b/g;)V
    :try_end_9
    .catch Ljava/lang/Exception; {:try_start_9 .. :try_end_9} :catch_2
    .catchall {:try_start_9 .. :try_end_9} :catchall_0

    goto/16 :goto_0

    .line 813
    :catch_2
    move-exception v0

    .line 815
    :try_start_a
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    .line 816
    new-instance v1, Lcom/msc/c/f;

    invoke-direct {v1, v0}, Lcom/msc/c/f;-><init>(Ljava/lang/Exception;)V

    iput-object v1, p0, Lcom/osp/app/signin/hh;->b:Lcom/msc/c/f;

    goto/16 :goto_0

    .line 818
    :cond_5
    iget-wide v4, p0, Lcom/osp/app/signin/hh;->e:J
    :try_end_a
    .catchall {:try_start_a .. :try_end_a} :catchall_0

    cmp-long v0, v0, v4

    if-nez v0, :cond_0

    .line 822
    :try_start_b
    invoke-static {}, Lcom/msc/c/h;->a()Lcom/msc/c/h;

    iget-object v0, p0, Lcom/osp/app/signin/hh;->c:Lcom/osp/app/signin/SecurityInfoChangeActivity;

    invoke-static {v0, v2}, Lcom/msc/c/h;->a(Landroid/content/Context;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 823
    iget-object v1, p0, Lcom/osp/app/signin/hh;->c:Lcom/osp/app/signin/SecurityInfoChangeActivity;

    invoke-static {v1, v0}, Lcom/msc/c/e;->b(Landroid/content/Context;Ljava/lang/String;)V

    .line 824
    invoke-virtual {p0}, Lcom/osp/app/signin/hh;->isCancelled()Z

    move-result v1

    if-nez v1, :cond_0

    .line 828
    invoke-direct {p0, v0}, Lcom/osp/app/signin/hh;->b(Ljava/lang/String;)V
    :try_end_b
    .catch Ljava/lang/Exception; {:try_start_b .. :try_end_b} :catch_3
    .catchall {:try_start_b .. :try_end_b} :catchall_0

    goto/16 :goto_0

    .line 829
    :catch_3
    move-exception v0

    .line 831
    :try_start_c
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    .line 832
    new-instance v1, Lcom/msc/c/f;

    invoke-direct {v1, v0}, Lcom/msc/c/f;-><init>(Ljava/lang/Exception;)V

    iput-object v1, p0, Lcom/osp/app/signin/hh;->b:Lcom/msc/c/f;
    :try_end_c
    .catchall {:try_start_c .. :try_end_c} :catchall_0

    goto/16 :goto_0
.end method

.method protected final a(Ljava/lang/Boolean;)V
    .locals 5

    .prologue
    const/16 v4, 0x13

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 714
    invoke-super {p0, p1}, Lcom/msc/c/b;->a(Ljava/lang/Boolean;)V

    .line 715
    iget-object v0, p0, Lcom/osp/app/signin/hh;->b:Lcom/msc/c/f;

    if-eqz v0, :cond_3

    .line 717
    const-string v0, "AUT_1302"

    iget-object v1, p0, Lcom/osp/app/signin/hh;->b:Lcom/msc/c/f;

    invoke-virtual {v1}, Lcom/msc/c/f;->a()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    const-string v0, "AUT_1830"

    iget-object v1, p0, Lcom/osp/app/signin/hh;->b:Lcom/msc/c/f;

    invoke-virtual {v1}, Lcom/msc/c/f;->a()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 719
    :cond_0
    iget-object v0, p0, Lcom/osp/app/signin/hh;->c:Lcom/osp/app/signin/SecurityInfoChangeActivity;

    invoke-static {v0}, Lcom/msc/c/c;->g(Landroid/content/Context;)V

    .line 720
    iget-object v0, p0, Lcom/osp/app/signin/hh;->c:Lcom/osp/app/signin/SecurityInfoChangeActivity;

    invoke-static {v0}, Lcom/msc/c/e;->a(Landroid/content/Context;)Z

    .line 721
    iget-object v0, p0, Lcom/osp/app/signin/hh;->c:Lcom/osp/app/signin/SecurityInfoChangeActivity;

    iget-object v1, p0, Lcom/osp/app/signin/hh;->c:Lcom/osp/app/signin/SecurityInfoChangeActivity;

    invoke-static {v1}, Lcom/osp/app/util/ad;->h(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/osp/app/signin/SecurityInfoChangeActivity;->a(Lcom/osp/app/signin/SecurityInfoChangeActivity;Ljava/lang/String;)V

    .line 722
    iget-object v0, p0, Lcom/osp/app/signin/hh;->c:Lcom/osp/app/signin/SecurityInfoChangeActivity;

    invoke-virtual {v0, v4}, Lcom/osp/app/signin/SecurityInfoChangeActivity;->b(I)V

    .line 732
    :goto_0
    iget-object v0, p0, Lcom/osp/app/signin/hh;->c:Lcom/osp/app/signin/SecurityInfoChangeActivity;

    invoke-virtual {v0}, Lcom/osp/app/signin/SecurityInfoChangeActivity;->finish()V

    .line 756
    :goto_1
    return-void

    .line 723
    :cond_1
    invoke-static {}, Lcom/osp/device/b;->b()Lcom/osp/device/b;

    move-result-object v0

    invoke-virtual {v0}, Lcom/osp/device/b;->a()Z

    move-result v0

    if-eqz v0, :cond_2

    const-string v0, "AUT_1094"

    iget-object v1, p0, Lcom/osp/app/signin/hh;->b:Lcom/msc/c/f;

    invoke-virtual {v1}, Lcom/msc/c/f;->a()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 725
    iget-object v0, p0, Lcom/osp/app/signin/hh;->c:Lcom/osp/app/signin/SecurityInfoChangeActivity;

    invoke-static {v0}, Lcom/osp/app/signin/SecurityInfoChangeActivity;->f(Lcom/osp/app/signin/SecurityInfoChangeActivity;)V

    .line 726
    iget-object v0, p0, Lcom/osp/app/signin/hh;->c:Lcom/osp/app/signin/SecurityInfoChangeActivity;

    invoke-virtual {v0, v4}, Lcom/osp/app/signin/SecurityInfoChangeActivity;->b(I)V

    goto :goto_0

    .line 729
    :cond_2
    invoke-virtual {p0, v2}, Lcom/osp/app/signin/hh;->a(Z)V

    .line 730
    iget-object v0, p0, Lcom/osp/app/signin/hh;->c:Lcom/osp/app/signin/SecurityInfoChangeActivity;

    invoke-virtual {v0, v3}, Lcom/osp/app/signin/SecurityInfoChangeActivity;->b(I)V

    goto :goto_0

    .line 735
    :cond_3
    iget-object v0, p0, Lcom/osp/app/signin/hh;->c:Lcom/osp/app/signin/SecurityInfoChangeActivity;

    invoke-static {v0}, Lcom/osp/app/signin/SecurityInfoChangeActivity;->a(Lcom/osp/app/signin/SecurityInfoChangeActivity;)Lcom/msc/a/j;

    move-result-object v0

    if-eqz v0, :cond_6

    .line 737
    invoke-static {}, Lcom/osp/app/util/y;->a()Lcom/osp/app/util/y;

    move-result-object v0

    iget-object v1, p0, Lcom/osp/app/signin/hh;->c:Lcom/osp/app/signin/SecurityInfoChangeActivity;

    invoke-static {v1}, Lcom/osp/app/signin/SecurityInfoChangeActivity;->a(Lcom/osp/app/signin/SecurityInfoChangeActivity;)Lcom/msc/a/j;

    move-result-object v1

    invoke-virtual {v1}, Lcom/msc/a/j;->a()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/osp/app/util/y;->a(Ljava/lang/String;)I

    move-result v0

    .line 738
    if-lez v0, :cond_5

    .line 740
    iget-object v1, p0, Lcom/osp/app/signin/hh;->c:Lcom/osp/app/signin/SecurityInfoChangeActivity;

    invoke-static {v1}, Lcom/osp/app/signin/SecurityInfoChangeActivity;->b(Lcom/osp/app/signin/SecurityInfoChangeActivity;)Landroid/widget/Button;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/widget/Button;->setText(I)V

    .line 748
    :cond_4
    :goto_2
    iget-object v0, p0, Lcom/osp/app/signin/hh;->c:Lcom/osp/app/signin/SecurityInfoChangeActivity;

    invoke-static {v0}, Lcom/osp/app/signin/SecurityInfoChangeActivity;->c(Lcom/osp/app/signin/SecurityInfoChangeActivity;)Landroid/widget/EditText;

    move-result-object v0

    iget-object v1, p0, Lcom/osp/app/signin/hh;->c:Lcom/osp/app/signin/SecurityInfoChangeActivity;

    invoke-static {v1}, Lcom/osp/app/signin/SecurityInfoChangeActivity;->a(Lcom/osp/app/signin/SecurityInfoChangeActivity;)Lcom/msc/a/j;

    move-result-object v1

    invoke-virtual {v1}, Lcom/msc/a/j;->c()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    .line 749
    iget-object v0, p0, Lcom/osp/app/signin/hh;->c:Lcom/osp/app/signin/SecurityInfoChangeActivity;

    invoke-static {v0}, Lcom/osp/app/signin/SecurityInfoChangeActivity;->c(Lcom/osp/app/signin/SecurityInfoChangeActivity;)Landroid/widget/EditText;

    move-result-object v0

    iget-object v1, p0, Lcom/osp/app/signin/hh;->c:Lcom/osp/app/signin/SecurityInfoChangeActivity;

    invoke-static {v1}, Lcom/osp/app/signin/SecurityInfoChangeActivity;->c(Lcom/osp/app/signin/SecurityInfoChangeActivity;)Landroid/widget/EditText;

    move-result-object v1

    invoke-virtual {v1}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v1

    invoke-interface {v1}, Landroid/text/Editable;->length()I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setSelection(I)V

    goto :goto_1

    .line 743
    :cond_5
    iget-object v0, p0, Lcom/osp/app/signin/hh;->c:Lcom/osp/app/signin/SecurityInfoChangeActivity;

    invoke-static {v0}, Lcom/osp/app/signin/SecurityInfoChangeActivity;->a(Lcom/osp/app/signin/SecurityInfoChangeActivity;)Lcom/msc/a/j;

    move-result-object v0

    invoke-virtual {v0}, Lcom/msc/a/j;->b()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_4

    .line 745
    iget-object v0, p0, Lcom/osp/app/signin/hh;->c:Lcom/osp/app/signin/SecurityInfoChangeActivity;

    invoke-static {v0}, Lcom/osp/app/signin/SecurityInfoChangeActivity;->b(Lcom/osp/app/signin/SecurityInfoChangeActivity;)Landroid/widget/Button;

    move-result-object v0

    iget-object v1, p0, Lcom/osp/app/signin/hh;->c:Lcom/osp/app/signin/SecurityInfoChangeActivity;

    invoke-static {v1}, Lcom/osp/app/signin/SecurityInfoChangeActivity;->a(Lcom/osp/app/signin/SecurityInfoChangeActivity;)Lcom/msc/a/j;

    move-result-object v1

    invoke-virtual {v1}, Lcom/msc/a/j;->b()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setText(Ljava/lang/CharSequence;)V

    goto :goto_2

    .line 752
    :cond_6
    invoke-virtual {p0, v2}, Lcom/osp/app/signin/hh;->a(Z)V

    .line 753
    iget-object v0, p0, Lcom/osp/app/signin/hh;->c:Lcom/osp/app/signin/SecurityInfoChangeActivity;

    invoke-virtual {v0, v3}, Lcom/osp/app/signin/SecurityInfoChangeActivity;->b(I)V

    .line 754
    iget-object v0, p0, Lcom/osp/app/signin/hh;->c:Lcom/osp/app/signin/SecurityInfoChangeActivity;

    invoke-virtual {v0}, Lcom/osp/app/signin/SecurityInfoChangeActivity;->finish()V

    goto/16 :goto_1
.end method

.method public final b(Lcom/msc/b/c;)V
    .locals 6

    .prologue
    .line 839
    invoke-super {p0, p1}, Lcom/msc/c/b;->b(Lcom/msc/b/c;)V

    .line 841
    if-nez p1, :cond_1

    .line 873
    :cond_0
    :goto_0
    return-void

    .line 846
    :cond_1
    invoke-virtual {p1}, Lcom/msc/b/c;->a()J

    move-result-wide v0

    .line 847
    invoke-virtual {p1}, Lcom/msc/b/c;->e()Ljava/lang/String;

    move-result-object v2

    .line 849
    iget-wide v4, p0, Lcom/osp/app/signin/hh;->d:J

    cmp-long v0, v0, v4

    if-nez v0, :cond_0

    .line 851
    if-eqz v2, :cond_0

    .line 853
    iget-boolean v0, p0, Lcom/osp/app/signin/hh;->h:Z

    if-nez v0, :cond_0

    const-string v0, "ACF_0403"

    iget-object v1, p0, Lcom/osp/app/signin/hh;->b:Lcom/msc/c/f;

    invoke-virtual {v1}, Lcom/msc/c/f;->a()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 857
    const/4 v0, 0x0

    :try_start_0
    iput-object v0, p0, Lcom/osp/app/signin/hh;->b:Lcom/msc/c/f;

    .line 858
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/osp/app/signin/hh;->h:Z

    .line 859
    iget-object v0, p0, Lcom/osp/app/signin/hh;->c:Lcom/osp/app/signin/SecurityInfoChangeActivity;

    const-string v1, ""

    invoke-static {v0, v1}, Lcom/msc/c/e;->b(Landroid/content/Context;Ljava/lang/String;)V

    .line 860
    invoke-virtual {p0}, Lcom/osp/app/signin/hh;->isCancelled()Z

    move-result v0

    if-nez v0, :cond_0

    .line 864
    invoke-direct {p0}, Lcom/osp/app/signin/hh;->h()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 865
    :catch_0
    move-exception v0

    .line 867
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    .line 868
    new-instance v1, Lcom/msc/c/f;

    invoke-direct {v1, v0}, Lcom/msc/c/f;-><init>(Ljava/lang/Exception;)V

    iput-object v1, p0, Lcom/osp/app/signin/hh;->b:Lcom/msc/c/f;

    goto :goto_0
.end method

.method protected final d()V
    .locals 2

    .prologue
    .line 691
    invoke-super {p0}, Lcom/msc/c/b;->d()V

    .line 692
    iget-object v0, p0, Lcom/osp/app/signin/hh;->c:Lcom/osp/app/signin/SecurityInfoChangeActivity;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/osp/app/signin/SecurityInfoChangeActivity;->b(I)V

    .line 693
    iget-object v0, p0, Lcom/osp/app/signin/hh;->c:Lcom/osp/app/signin/SecurityInfoChangeActivity;

    invoke-virtual {v0}, Lcom/osp/app/signin/SecurityInfoChangeActivity;->finish()V

    .line 694
    return-void
.end method

.method protected final synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 648
    invoke-virtual {p0}, Lcom/osp/app/signin/hh;->a()Ljava/lang/Boolean;

    move-result-object v0

    return-object v0
.end method

.method protected final synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 648
    check-cast p1, Ljava/lang/Boolean;

    invoke-virtual {p0, p1}, Lcom/osp/app/signin/hh;->a(Ljava/lang/Boolean;)V

    return-void
.end method
