.class public Lcom/osp/app/signin/ShowDisclaimerView;
.super Lcom/osp/app/util/BaseActivity;
.source "ShowDisclaimerView.java"


# instance fields
.field private a:Landroid/widget/TextView;

.field private b:Landroid/widget/TextView;

.field private c:Landroid/widget/LinearLayout;

.field private d:Ljava/lang/String;

.field private e:Ljava/lang/String;

.field private f:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 25
    invoke-direct {p0}, Lcom/osp/app/util/BaseActivity;-><init>()V

    .line 30
    iput-object v0, p0, Lcom/osp/app/signin/ShowDisclaimerView;->d:Ljava/lang/String;

    .line 31
    iput-object v0, p0, Lcom/osp/app/signin/ShowDisclaimerView;->e:Ljava/lang/String;

    .line 32
    iput-object v0, p0, Lcom/osp/app/signin/ShowDisclaimerView;->f:Ljava/lang/String;

    .line 95
    return-void
.end method

.method static synthetic a(Lcom/osp/app/signin/ShowDisclaimerView;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 25
    iget-object v0, p0, Lcom/osp/app/signin/ShowDisclaimerView;->f:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic b(Lcom/osp/app/signin/ShowDisclaimerView;)Landroid/widget/TextView;
    .locals 1

    .prologue
    .line 25
    iget-object v0, p0, Lcom/osp/app/signin/ShowDisclaimerView;->a:Landroid/widget/TextView;

    return-object v0
.end method


# virtual methods
.method protected final a()V
    .locals 0

    .prologue
    .line 189
    return-void
.end method

.method protected final b()V
    .locals 0

    .prologue
    .line 221
    return-void
.end method

.method public onConfigurationChanged(Landroid/content/res/Configuration;)V
    .locals 1

    .prologue
    .line 194
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v0, "onConfigurationChanged()"

    invoke-static {v0}, Lcom/osp/app/util/aq;->c(Ljava/lang/String;)V

    .line 196
    invoke-super {p0, p1}, Lcom/osp/app/util/BaseActivity;->onConfigurationChanged(Landroid/content/res/Configuration;)V

    .line 197
    return-void
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 36
    invoke-super {p0, p1}, Lcom/osp/app/util/BaseActivity;->onCreate(Landroid/os/Bundle;)V

    .line 37
    invoke-virtual {p0}, Lcom/osp/app/signin/ShowDisclaimerView;->getApplication()Landroid/app/Application;

    move-result-object v0

    check-cast v0, Lcom/osp/app/signin/SamsungService;

    invoke-virtual {v0, p0}, Lcom/osp/app/signin/SamsungService;->a(Landroid/app/Activity;)V

    .line 38
    const v0, 0x7f03002d

    invoke-virtual {p0, v0}, Lcom/osp/app/signin/ShowDisclaimerView;->setContentView(I)V

    .line 39
    invoke-virtual {p0}, Lcom/osp/app/signin/ShowDisclaimerView;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string v1, "pp_data"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/osp/app/signin/ShowDisclaimerView;->e:Ljava/lang/String;

    const-string v1, "tnc_data"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/osp/app/signin/ShowDisclaimerView;->d:Ljava/lang/String;

    .line 50
    invoke-static {p0}, Lcom/msc/sa/c/d;->a(Landroid/app/Activity;)V

    .line 51
    iget-object v0, p0, Lcom/osp/app/signin/ShowDisclaimerView;->c:Landroid/widget/LinearLayout;

    if-nez v0, :cond_0

    const v0, 0x7f0c0089

    invoke-virtual {p0, v0}, Lcom/osp/app/signin/ShowDisclaimerView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcom/osp/app/signin/ShowDisclaimerView;->c:Landroid/widget/LinearLayout;

    :cond_0
    iget-object v0, p0, Lcom/osp/app/signin/ShowDisclaimerView;->b:Landroid/widget/TextView;

    if-nez v0, :cond_1

    const v0, 0x7f0c011b

    invoke-virtual {p0, v0}, Lcom/osp/app/signin/ShowDisclaimerView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/osp/app/signin/ShowDisclaimerView;->b:Landroid/widget/TextView;

    :cond_1
    iget-object v0, p0, Lcom/osp/app/signin/ShowDisclaimerView;->a:Landroid/widget/TextView;

    if-nez v0, :cond_2

    const v0, 0x7f0c011c

    invoke-virtual {p0, v0}, Lcom/osp/app/signin/ShowDisclaimerView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/osp/app/signin/ShowDisclaimerView;->a:Landroid/widget/TextView;

    .line 53
    :cond_2
    iget-object v0, p0, Lcom/osp/app/signin/ShowDisclaimerView;->e:Ljava/lang/String;

    if-eqz v0, :cond_4

    .line 55
    iget-object v0, p0, Lcom/osp/app/signin/ShowDisclaimerView;->b:Landroid/widget/TextView;

    const v1, 0x7f090068

    invoke-virtual {p0, v1}, Lcom/osp/app/signin/ShowDisclaimerView;->getText(I)Ljava/lang/CharSequence;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 56
    iget-object v0, p0, Lcom/osp/app/signin/ShowDisclaimerView;->e:Ljava/lang/String;

    iput-object v0, p0, Lcom/osp/app/signin/ShowDisclaimerView;->f:Ljava/lang/String;

    .line 62
    :cond_3
    :goto_0
    new-instance v0, Lcom/osp/app/signin/hr;

    invoke-direct {v0, p0}, Lcom/osp/app/signin/hr;-><init>(Lcom/osp/app/signin/ShowDisclaimerView;)V

    .line 63
    invoke-virtual {v0}, Lcom/osp/app/signin/hr;->b()V

    .line 65
    return-void

    .line 57
    :cond_4
    iget-object v0, p0, Lcom/osp/app/signin/ShowDisclaimerView;->d:Ljava/lang/String;

    if-eqz v0, :cond_3

    .line 59
    iget-object v0, p0, Lcom/osp/app/signin/ShowDisclaimerView;->b:Landroid/widget/TextView;

    const v1, 0x7f090059

    invoke-virtual {p0, v1}, Lcom/osp/app/signin/ShowDisclaimerView;->getText(I)Ljava/lang/CharSequence;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 60
    iget-object v0, p0, Lcom/osp/app/signin/ShowDisclaimerView;->d:Ljava/lang/String;

    iput-object v0, p0, Lcom/osp/app/signin/ShowDisclaimerView;->f:Ljava/lang/String;

    goto :goto_0
.end method

.method public onOptionsItemSelected(Landroid/view/MenuItem;)Z
    .locals 2

    .prologue
    .line 202
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0xb

    if-lt v0, v1, :cond_0

    .line 204
    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    .line 208
    :cond_0
    :goto_0
    invoke-super {p0, p1}, Lcom/osp/app/util/BaseActivity;->onOptionsItemSelected(Landroid/view/MenuItem;)Z

    move-result v0

    return v0

    .line 207
    :pswitch_0
    invoke-virtual {p0}, Lcom/osp/app/signin/ShowDisclaimerView;->onBackPressed()V

    goto :goto_0

    .line 204
    :pswitch_data_0
    .packed-switch 0x102002c
        :pswitch_0
    .end packed-switch
.end method
