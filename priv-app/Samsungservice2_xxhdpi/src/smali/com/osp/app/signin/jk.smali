.class final Lcom/osp/app/signin/jk;
.super Lcom/msc/c/b;
.source "SignInView.java"


# instance fields
.field final synthetic c:Lcom/osp/app/signin/SignInView;

.field private d:J

.field private e:J

.field private f:J


# direct methods
.method public constructor <init>(Lcom/osp/app/signin/SignInView;)V
    .locals 0

    .prologue
    .line 4660
    iput-object p1, p0, Lcom/osp/app/signin/jk;->c:Lcom/osp/app/signin/SignInView;

    .line 4661
    invoke-direct {p0, p1}, Lcom/msc/c/b;-><init>(Landroid/content/Context;)V

    .line 4665
    return-void
.end method

.method private h()V
    .locals 3

    .prologue
    .line 5070
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v0, "SIV"

    const-string v1, "requestMyCountryZone"

    invoke-static {v0, v1}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 5072
    invoke-static {}, Lcom/msc/c/g;->a()Lcom/msc/c/g;

    .line 5073
    iget-object v0, p0, Lcom/osp/app/signin/jk;->c:Lcom/osp/app/signin/SignInView;

    invoke-static {v0, p0}, Lcom/msc/c/g;->b(Landroid/content/Context;Lcom/msc/b/h;)J

    move-result-wide v0

    iput-wide v0, p0, Lcom/osp/app/signin/jk;->f:J

    .line 5074
    iget-wide v0, p0, Lcom/osp/app/signin/jk;->f:J

    invoke-virtual {p0, v0, v1}, Lcom/osp/app/signin/jk;->a(J)V

    .line 5076
    iget-wide v0, p0, Lcom/osp/app/signin/jk;->f:J

    sget-object v2, Lcom/msc/b/g;->b:Lcom/msc/b/g;

    invoke-static {v0, v1, v2}, Lcom/msc/c/g;->a(JLcom/msc/b/g;)V

    .line 5077
    return-void
.end method


# virtual methods
.method protected final varargs a()Ljava/lang/Boolean;
    .locals 9

    .prologue
    const/4 v3, 0x0

    const/4 v8, 0x0

    .line 4675
    iget-object v0, p0, Lcom/osp/app/signin/jk;->c:Lcom/osp/app/signin/SignInView;

    invoke-static {v0}, Lcom/osp/app/signin/SignInView;->e(Lcom/osp/app/signin/SignInView;)Z

    move-result v0

    if-eqz v0, :cond_6

    .line 4680
    iget-object v0, p0, Lcom/osp/app/signin/jk;->c:Lcom/osp/app/signin/SignInView;

    invoke-static {v0}, Lcom/osp/app/signin/SignInView;->s(Lcom/osp/app/signin/SignInView;)Landroid/view/View;

    move-result-object v0

    const v1, 0x7f0c0129

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/EditText;

    .line 4681
    iget-object v1, p0, Lcom/osp/app/signin/jk;->c:Lcom/osp/app/signin/SignInView;

    invoke-static {v1}, Lcom/osp/app/signin/SignInView;->s(Lcom/osp/app/signin/SignInView;)Landroid/view/View;

    move-result-object v1

    const v2, 0x7f0c012a

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/EditText;

    .line 4688
    :goto_0
    iget-object v2, p0, Lcom/osp/app/signin/jk;->c:Lcom/osp/app/signin/SignInView;

    invoke-static {v2}, Lcom/osp/app/signin/SignInView;->L(Lcom/osp/app/signin/SignInView;)V

    .line 4690
    iget-object v2, p0, Lcom/osp/app/signin/jk;->c:Lcom/osp/app/signin/SignInView;

    invoke-static {v2, v8}, Lcom/osp/app/signin/SignInView;->b(Lcom/osp/app/signin/SignInView;Z)Z

    .line 4691
    iget-object v2, p0, Lcom/osp/app/signin/jk;->c:Lcom/osp/app/signin/SignInView;

    invoke-static {v2, v3}, Lcom/osp/app/signin/SignInView;->d(Lcom/osp/app/signin/SignInView;Ljava/lang/String;)Ljava/lang/String;

    .line 4693
    iget-object v2, p0, Lcom/osp/app/signin/jk;->c:Lcom/osp/app/signin/SignInView;

    invoke-static {v2, v3}, Lcom/osp/app/signin/SignInView;->b(Lcom/osp/app/signin/SignInView;Ljava/lang/String;)Ljava/lang/String;

    .line 4695
    iget-object v2, p0, Lcom/osp/app/signin/jk;->c:Lcom/osp/app/signin/SignInView;

    invoke-static {v2}, Lcom/osp/app/signin/SignInView;->j(Lcom/osp/app/signin/SignInView;)Ljava/lang/String;

    move-result-object v3

    .line 4696
    iget-object v2, p0, Lcom/osp/app/signin/jk;->c:Lcom/osp/app/signin/SignInView;

    invoke-static {v2}, Lcom/osp/app/signin/SignInView;->k(Lcom/osp/app/signin/SignInView;)Ljava/lang/String;

    move-result-object v4

    .line 4697
    iget-object v2, p0, Lcom/osp/app/signin/jk;->c:Lcom/osp/app/signin/SignInView;

    invoke-static {v2}, Lcom/osp/app/signin/SignInView;->p(Lcom/osp/app/signin/SignInView;)Ljava/lang/String;

    move-result-object v2

    .line 4698
    if-eqz v3, :cond_0

    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v5

    if-lez v5, :cond_0

    if-eqz v4, :cond_0

    invoke-virtual {v4}, Ljava/lang/String;->length()I

    move-result v4

    if-gtz v4, :cond_2

    .line 4700
    :cond_0
    const-string v3, "j5p7ll8g33"

    .line 4701
    if-eqz v2, :cond_1

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v4

    if-gtz v4, :cond_2

    .line 4704
    :cond_1
    const-string v2, "com.osp.app.signin"

    .line 4708
    :cond_2
    iget-object v4, p0, Lcom/osp/app/signin/jk;->c:Lcom/osp/app/signin/SignInView;

    iget-object v5, p0, Lcom/osp/app/signin/jk;->c:Lcom/osp/app/signin/SignInView;

    invoke-static {v5}, Lcom/osp/app/util/ad;->p(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/osp/app/signin/SignInView;->c(Lcom/osp/app/signin/SignInView;Ljava/lang/String;)Ljava/lang/String;

    .line 4709
    iget-object v4, p0, Lcom/osp/app/signin/jk;->c:Lcom/osp/app/signin/SignInView;

    invoke-static {v4}, Lcom/osp/app/signin/SignInView;->A(Lcom/osp/app/signin/SignInView;)Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-eqz v4, :cond_3

    .line 4711
    invoke-direct {p0}, Lcom/osp/app/signin/jk;->h()V

    .line 4714
    :cond_3
    invoke-static {}, Lcom/osp/common/util/i;->a()Lcom/osp/common/util/i;

    iget-object v4, p0, Lcom/osp/app/signin/jk;->c:Lcom/osp/app/signin/SignInView;

    invoke-static {v4}, Lcom/osp/common/util/i;->l(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v4

    sget-object v5, Ljava/util/Locale;->ENGLISH:Ljava/util/Locale;

    invoke-virtual {v4, v5}, Ljava/lang/String;->toUpperCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v4

    .line 4715
    new-instance v5, Lcom/msc/a/h;

    invoke-direct {v5}, Lcom/msc/a/h;-><init>()V

    .line 4716
    invoke-virtual {v0}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v6

    sget-object v7, Ljava/util/Locale;->ENGLISH:Ljava/util/Locale;

    invoke-virtual {v6, v7}, Ljava/lang/String;->toLowerCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Lcom/msc/a/h;->d(Ljava/lang/String;)V

    .line 4718
    iget-object v6, p0, Lcom/osp/app/signin/jk;->c:Lcom/osp/app/signin/SignInView;

    invoke-static {v6}, Lcom/osp/app/signin/SignInView;->C(Lcom/osp/app/signin/SignInView;)Z

    move-result v6

    if-eqz v6, :cond_4

    iget-object v6, p0, Lcom/osp/app/signin/jk;->c:Lcom/osp/app/signin/SignInView;

    invoke-static {v6}, Lcom/osp/app/signin/SignInView;->D(Lcom/osp/app/signin/SignInView;)Z

    move-result v6

    if-nez v6, :cond_4

    .line 4720
    iget-object v6, p0, Lcom/osp/app/signin/jk;->c:Lcom/osp/app/signin/SignInView;

    invoke-static {v6}, Lcom/osp/app/util/ad;->t(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v6

    .line 4721
    invoke-virtual {v5, v6}, Lcom/msc/a/h;->c(Ljava/lang/String;)V

    .line 4722
    const-string v6, "Y"

    invoke-virtual {v5, v6}, Lcom/msc/a/h;->a(Ljava/lang/String;)V

    .line 4727
    :cond_4
    invoke-virtual {v5}, Lcom/msc/a/h;->b()V

    .line 4730
    invoke-virtual {v5, v3}, Lcom/msc/a/h;->h(Ljava/lang/String;)V

    .line 4731
    invoke-virtual {v5, v2}, Lcom/msc/a/h;->i(Ljava/lang/String;)V

    .line 4732
    invoke-virtual {v5, v3}, Lcom/msc/a/h;->j(Ljava/lang/String;)V

    .line 4733
    invoke-virtual {v5, v4}, Lcom/msc/a/h;->e(Ljava/lang/String;)V

    .line 4734
    iget-object v2, p0, Lcom/osp/app/signin/jk;->c:Lcom/osp/app/signin/SignInView;

    invoke-static {v2}, Lcom/osp/app/signin/SignInView;->A(Lcom/osp/app/signin/SignInView;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v5, v2}, Lcom/msc/a/h;->k(Ljava/lang/String;)V

    .line 4735
    invoke-virtual {v1}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v5, v1}, Lcom/msc/a/h;->f(Ljava/lang/String;)V

    .line 4736
    invoke-static {}, Lcom/osp/device/b;->b()Lcom/osp/device/b;

    move-result-object v1

    invoke-virtual {v1}, Lcom/osp/device/b;->a()Z

    move-result v1

    if-eqz v1, :cond_5

    invoke-virtual {v0}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    sget-object v1, Ljava/util/Locale;->ENGLISH:Ljava/util/Locale;

    invoke-virtual {v0, v1}, Ljava/lang/String;->toLowerCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/telephony/PhoneNumberUtils;->isGlobalPhoneNumber(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_5

    .line 4738
    const-string v0, "Y"

    invoke-virtual {v5, v0}, Lcom/msc/a/h;->b(Ljava/lang/String;)V

    .line 4740
    :cond_5
    invoke-static {}, Lcom/msc/c/g;->a()Lcom/msc/c/g;

    iget-object v0, p0, Lcom/osp/app/signin/jk;->c:Lcom/osp/app/signin/SignInView;

    invoke-static {v0, v5, p0}, Lcom/msc/c/g;->a(Landroid/content/Context;Lcom/msc/a/h;Lcom/msc/b/h;)J

    move-result-wide v0

    iput-wide v0, p0, Lcom/osp/app/signin/jk;->d:J

    iget-wide v0, p0, Lcom/osp/app/signin/jk;->d:J

    invoke-virtual {p0, v0, v1}, Lcom/osp/app/signin/jk;->a(J)V

    iget-wide v0, p0, Lcom/osp/app/signin/jk;->d:J

    const-string v2, "from_xml"

    invoke-virtual {p0, v0, v1, v2}, Lcom/osp/app/signin/jk;->a(JLjava/lang/String;)V

    iget-wide v0, p0, Lcom/osp/app/signin/jk;->d:J

    sget-object v2, Lcom/msc/b/g;->c:Lcom/msc/b/g;

    invoke-static {v0, v1, v2}, Lcom/msc/c/g;->a(JLcom/msc/b/g;)V

    .line 4741
    invoke-static {v8}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    return-object v0

    .line 4684
    :cond_6
    iget-object v0, p0, Lcom/osp/app/signin/jk;->c:Lcom/osp/app/signin/SignInView;

    const v1, 0x7f0c0121

    invoke-virtual {v0, v1}, Lcom/osp/app/signin/SignInView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/EditText;

    .line 4685
    iget-object v1, p0, Lcom/osp/app/signin/jk;->c:Lcom/osp/app/signin/SignInView;

    const v2, 0x7f0c0122

    invoke-virtual {v1, v2}, Lcom/osp/app/signin/SignInView;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/EditText;

    goto/16 :goto_0
.end method

.method public final declared-synchronized a(Lcom/msc/b/c;)V
    .locals 6

    .prologue
    .line 4853
    monitor-enter p0

    :try_start_0
    invoke-super {p0, p1}, Lcom/msc/c/b;->a(Lcom/msc/b/c;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 4855
    if-nez p1, :cond_1

    .line 4957
    :cond_0
    :goto_0
    monitor-exit p0

    return-void

    .line 4860
    :cond_1
    :try_start_1
    invoke-virtual {p1}, Lcom/msc/b/c;->a()J

    move-result-wide v0

    .line 4861
    invoke-virtual {p1}, Lcom/msc/b/c;->e()Ljava/lang/String;

    move-result-object v2

    .line 4863
    iget-wide v4, p0, Lcom/osp/app/signin/jk;->d:J
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    cmp-long v3, v0, v4

    if-nez v3, :cond_3

    .line 4868
    :try_start_2
    new-instance v0, Lcom/msc/a/i;

    invoke-direct {v0}, Lcom/msc/a/i;-><init>()V

    .line 4869
    invoke-static {}, Lcom/msc/c/h;->a()Lcom/msc/c/h;

    iget-object v1, p0, Lcom/osp/app/signin/jk;->c:Lcom/osp/app/signin/SignInView;

    iget-object v3, p0, Lcom/osp/app/signin/jk;->c:Lcom/osp/app/signin/SignInView;

    invoke-static {v3}, Lcom/osp/app/signin/SignInView;->j(Lcom/osp/app/signin/SignInView;)Ljava/lang/String;

    move-result-object v3

    invoke-static {v1, v3, v2, v0}, Lcom/msc/c/h;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Lcom/msc/a/i;)V

    .line 4870
    invoke-static {}, Lcom/osp/device/b;->b()Lcom/osp/device/b;

    move-result-object v1

    invoke-virtual {v1}, Lcom/osp/device/b;->a()Z

    move-result v1

    if-eqz v1, :cond_2

    invoke-virtual {v0}, Lcom/msc/a/i;->a()Lcom/msc/c/f;

    move-result-object v1

    if-eqz v1, :cond_2

    .line 4872
    iget-object v1, p0, Lcom/osp/app/signin/jk;->c:Lcom/osp/app/signin/SignInView;

    iget-object v2, p0, Lcom/osp/app/signin/jk;->c:Lcom/osp/app/signin/SignInView;

    invoke-virtual {v0}, Lcom/msc/a/i;->b()Lcom/msc/a/g;

    move-result-object v2

    invoke-virtual {v2}, Lcom/msc/a/g;->c()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/osp/app/signin/SignInView;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/osp/app/signin/SignInView;->e(Lcom/osp/app/signin/SignInView;Ljava/lang/String;)Ljava/lang/String;

    .line 4873
    iget-object v1, p0, Lcom/osp/app/signin/jk;->c:Lcom/osp/app/signin/SignInView;

    invoke-virtual {v0}, Lcom/msc/a/i;->b()Lcom/msc/a/g;

    move-result-object v2

    invoke-virtual {v2}, Lcom/msc/a/g;->b()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/osp/app/signin/SignInView;->b(Lcom/osp/app/signin/SignInView;Ljava/lang/String;)Ljava/lang/String;

    .line 4874
    invoke-virtual {v0}, Lcom/msc/a/i;->a()Lcom/msc/c/f;

    move-result-object v0

    iput-object v0, p0, Lcom/osp/app/signin/jk;->b:Lcom/msc/c/f;
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_0
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0

    .line 4879
    :catch_0
    move-exception v0

    .line 4881
    :try_start_3
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    .line 4882
    new-instance v1, Lcom/msc/c/f;

    invoke-direct {v1, v0}, Lcom/msc/c/f;-><init>(Ljava/lang/Exception;)V

    iput-object v1, p0, Lcom/osp/app/signin/jk;->b:Lcom/msc/c/f;
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    goto :goto_0

    .line 4853
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 4877
    :cond_2
    :try_start_4
    iget-object v1, p0, Lcom/osp/app/signin/jk;->c:Lcom/osp/app/signin/SignInView;

    invoke-virtual {v0}, Lcom/msc/a/i;->b()Lcom/msc/a/g;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/osp/app/signin/SignInView;->b(Lcom/osp/app/signin/SignInView;Lcom/msc/a/g;)Lcom/msc/a/g;

    .line 4878
    iget-object v1, p0, Lcom/osp/app/signin/jk;->c:Lcom/osp/app/signin/SignInView;

    invoke-virtual {v0}, Lcom/msc/a/i;->c()Lcom/osp/app/signin/kd;

    move-result-object v0

    invoke-static {v1, v0}, Lcom/osp/app/signin/SignInView;->a(Lcom/osp/app/signin/SignInView;Lcom/osp/app/signin/kd;)Lcom/osp/app/signin/kd;
    :try_end_4
    .catch Ljava/lang/Exception; {:try_start_4 .. :try_end_4} :catch_0
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    goto :goto_0

    .line 4884
    :cond_3
    :try_start_5
    iget-wide v4, p0, Lcom/osp/app/signin/jk;->e:J
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    cmp-long v3, v0, v4

    if-nez v3, :cond_6

    .line 4888
    :try_start_6
    iget-object v0, p0, Lcom/osp/app/signin/jk;->c:Lcom/osp/app/signin/SignInView;

    invoke-static {}, Lcom/msc/c/h;->a()Lcom/msc/c/h;

    invoke-static {v2}, Lcom/msc/c/h;->e(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/osp/app/signin/SignInView;->b(Lcom/osp/app/signin/SignInView;Ljava/lang/String;)Ljava/lang/String;

    .line 4889
    iget-object v0, p0, Lcom/osp/app/signin/jk;->c:Lcom/osp/app/signin/SignInView;

    invoke-static {v0}, Lcom/osp/app/signin/SignInView;->y(Lcom/osp/app/signin/SignInView;)Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/osp/app/signin/jk;->c:Lcom/osp/app/signin/SignInView;

    invoke-static {v0}, Lcom/osp/app/signin/SignInView;->y(Lcom/osp/app/signin/SignInView;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v0

    if-lez v0, :cond_0

    .line 4894
    iget-object v0, p0, Lcom/osp/app/signin/jk;->c:Lcom/osp/app/signin/SignInView;

    const/4 v1, 0x1

    invoke-static {v0, v1}, Lcom/osp/app/signin/SignInView;->b(Lcom/osp/app/signin/SignInView;Z)Z

    .line 4895
    invoke-static {}, Lcom/osp/common/util/i;->a()Lcom/osp/common/util/i;

    iget-object v0, p0, Lcom/osp/app/signin/jk;->c:Lcom/osp/app/signin/SignInView;

    invoke-static {v0}, Lcom/osp/common/util/i;->e(Landroid/content/Context;)Z
    :try_end_6
    .catch Ljava/lang/Exception; {:try_start_6 .. :try_end_6} :catch_1
    .catchall {:try_start_6 .. :try_end_6} :catchall_0

    move-result v0

    if-eqz v0, :cond_4

    .line 4900
    :try_start_7
    iget-object v0, p0, Lcom/osp/app/signin/jk;->c:Lcom/osp/app/signin/SignInView;

    invoke-static {}, Lcom/osp/common/util/i;->a()Lcom/osp/common/util/i;

    iget-object v1, p0, Lcom/osp/app/signin/jk;->c:Lcom/osp/app/signin/SignInView;

    invoke-static {v1}, Lcom/osp/common/util/i;->h(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/osp/app/signin/SignInView;->d(Lcom/osp/app/signin/SignInView;Ljava/lang/String;)Ljava/lang/String;

    .line 4901
    iget-object v0, p0, Lcom/osp/app/signin/jk;->c:Lcom/osp/app/signin/SignInView;

    iget-object v1, p0, Lcom/osp/app/signin/jk;->c:Lcom/osp/app/signin/SignInView;

    invoke-static {v1}, Lcom/osp/app/signin/SignInView;->G(Lcom/osp/app/signin/SignInView;)Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/osp/app/signin/jk;->c:Lcom/osp/app/signin/SignInView;

    invoke-virtual {v2}, Lcom/osp/app/signin/SignInView;->getApplicationContext()Landroid/content/Context;

    iget-object v2, p0, Lcom/osp/app/signin/jk;->c:Lcom/osp/app/signin/SignInView;

    invoke-static {v2}, Lcom/osp/app/signin/SignInView;->G(Lcom/osp/app/signin/SignInView;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/osp/common/util/i;->d(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v1, v2}, Lcom/msc/c/e;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)V
    :try_end_7
    .catch Ljava/lang/Exception; {:try_start_7 .. :try_end_7} :catch_2
    .catchall {:try_start_7 .. :try_end_7} :catchall_1

    .line 4907
    :try_start_8
    iget-object v0, p0, Lcom/osp/app/signin/jk;->c:Lcom/osp/app/signin/SignInView;

    invoke-static {v0}, Lcom/osp/app/signin/SignInView;->G(Lcom/osp/app/signin/SignInView;)Ljava/lang/String;

    move-result-object v0

    if-nez v0, :cond_4

    .line 4909
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v0, "SIV"

    const-string v1, "The sim state is ready but mcc is null!!"

    invoke-static {v0, v1}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 4914
    :cond_4
    :goto_1
    iget-object v0, p0, Lcom/osp/app/signin/jk;->c:Lcom/osp/app/signin/SignInView;

    invoke-static {v0}, Lcom/osp/app/signin/SignInView;->G(Lcom/osp/app/signin/SignInView;)Ljava/lang/String;

    move-result-object v0

    if-nez v0, :cond_0

    .line 4916
    invoke-direct {p0}, Lcom/osp/app/signin/jk;->h()V
    :try_end_8
    .catch Ljava/lang/Exception; {:try_start_8 .. :try_end_8} :catch_1
    .catchall {:try_start_8 .. :try_end_8} :catchall_0

    goto/16 :goto_0

    .line 4919
    :catch_1
    move-exception v0

    :try_start_9
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V
    :try_end_9
    .catchall {:try_start_9 .. :try_end_9} :catchall_0

    goto/16 :goto_0

    .line 4902
    :catch_2
    move-exception v0

    :try_start_a
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V
    :try_end_a
    .catchall {:try_start_a .. :try_end_a} :catchall_1

    .line 4907
    :try_start_b
    iget-object v0, p0, Lcom/osp/app/signin/jk;->c:Lcom/osp/app/signin/SignInView;

    invoke-static {v0}, Lcom/osp/app/signin/SignInView;->G(Lcom/osp/app/signin/SignInView;)Ljava/lang/String;

    move-result-object v0

    if-nez v0, :cond_4

    .line 4909
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v0, "SIV"

    const-string v1, "The sim state is ready but mcc is null!!"

    invoke-static {v0, v1}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1

    .line 4907
    :catchall_1
    move-exception v0

    iget-object v1, p0, Lcom/osp/app/signin/jk;->c:Lcom/osp/app/signin/SignInView;

    invoke-static {v1}, Lcom/osp/app/signin/SignInView;->G(Lcom/osp/app/signin/SignInView;)Ljava/lang/String;

    move-result-object v1

    if-nez v1, :cond_5

    .line 4909
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v1, "SIV"

    const-string v2, "The sim state is ready but mcc is null!!"

    invoke-static {v1, v2}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;)V

    :cond_5
    throw v0
    :try_end_b
    .catch Ljava/lang/Exception; {:try_start_b .. :try_end_b} :catch_1
    .catchall {:try_start_b .. :try_end_b} :catchall_0

    .line 4923
    :cond_6
    :try_start_c
    iget-wide v4, p0, Lcom/osp/app/signin/jk;->f:J
    :try_end_c
    .catchall {:try_start_c .. :try_end_c} :catchall_0

    cmp-long v0, v0, v4

    if-nez v0, :cond_0

    .line 4927
    :try_start_d
    invoke-static {}, Lcom/msc/c/h;->a()Lcom/msc/c/h;

    invoke-static {v2}, Lcom/msc/c/h;->w(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 4928
    if-eqz v0, :cond_8

    .line 4930
    invoke-static {}, Lcom/osp/common/util/i;->a()Lcom/osp/common/util/i;

    iget-object v1, p0, Lcom/osp/app/signin/jk;->c:Lcom/osp/app/signin/SignInView;

    invoke-static {v1}, Lcom/osp/common/util/i;->o(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v1

    .line 4931
    invoke-static {}, Lcom/osp/common/util/i;->a()Lcom/osp/common/util/i;

    iget-object v2, p0, Lcom/osp/app/signin/jk;->c:Lcom/osp/app/signin/SignInView;

    invoke-static {v1}, Lcom/osp/common/util/i;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 4932
    if-eqz v2, :cond_7

    invoke-virtual {v2, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_7

    .line 4934
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v3, "GetMyCountryZoneTask countryCodeFromNetworkMcc = ["

    invoke-direct {v0, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, "]"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;)V

    .line 4935
    iget-object v0, p0, Lcom/osp/app/signin/jk;->c:Lcom/osp/app/signin/SignInView;

    invoke-static {v0, v1}, Lcom/osp/app/signin/SignInView;->d(Lcom/osp/app/signin/SignInView;Ljava/lang/String;)Ljava/lang/String;

    .line 4942
    :goto_2
    iget-object v0, p0, Lcom/osp/app/signin/jk;->c:Lcom/osp/app/signin/SignInView;

    invoke-static {v0}, Lcom/osp/app/signin/SignInView;->G(Lcom/osp/app/signin/SignInView;)Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/osp/app/signin/jk;->c:Lcom/osp/app/signin/SignInView;

    invoke-static {v0}, Lcom/osp/app/signin/SignInView;->G(Lcom/osp/app/signin/SignInView;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v0

    if-lez v0, :cond_0

    .line 4944
    iget-object v0, p0, Lcom/osp/app/signin/jk;->c:Lcom/osp/app/signin/SignInView;

    iget-object v1, p0, Lcom/osp/app/signin/jk;->c:Lcom/osp/app/signin/SignInView;

    invoke-static {v1}, Lcom/osp/app/signin/SignInView;->G(Lcom/osp/app/signin/SignInView;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/osp/app/util/ad;->e(Landroid/content/Context;Ljava/lang/String;)V
    :try_end_d
    .catch Ljava/lang/Exception; {:try_start_d .. :try_end_d} :catch_3
    .catchall {:try_start_d .. :try_end_d} :catchall_0

    goto/16 :goto_0

    .line 4950
    :catch_3
    move-exception v0

    .line 4952
    :try_start_e
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v1, "GetMyCountryZoneTask fail"

    invoke-static {v1}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;)V

    .line 4953
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    .line 4954
    iget-object v0, p0, Lcom/osp/app/signin/jk;->c:Lcom/osp/app/signin/SignInView;

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lcom/osp/app/signin/SignInView;->d(Lcom/osp/app/signin/SignInView;Ljava/lang/String;)Ljava/lang/String;
    :try_end_e
    .catchall {:try_start_e .. :try_end_e} :catchall_0

    goto/16 :goto_0

    .line 4938
    :cond_7
    :try_start_f
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "GetMyCountryZoneTask countryCode = ["

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "]"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;)V

    .line 4939
    iget-object v1, p0, Lcom/osp/app/signin/jk;->c:Lcom/osp/app/signin/SignInView;

    invoke-static {}, Lcom/osp/common/util/i;->a()Lcom/osp/common/util/i;

    iget-object v2, p0, Lcom/osp/app/signin/jk;->c:Lcom/osp/app/signin/SignInView;

    sget-object v2, Ljava/util/Locale;->ENGLISH:Ljava/util/Locale;

    invoke-virtual {v0, v2}, Ljava/lang/String;->toLowerCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/osp/common/util/i;->c(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Lcom/osp/app/signin/SignInView;->d(Lcom/osp/app/signin/SignInView;Ljava/lang/String;)Ljava/lang/String;

    goto :goto_2

    .line 4948
    :cond_8
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v0, "SIV"

    const-string v1, "GetMyCountryZoneTask Success. but countryCode is null!!"

    invoke-static {v0, v1}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_f
    .catch Ljava/lang/Exception; {:try_start_f .. :try_end_f} :catch_3
    .catchall {:try_start_f .. :try_end_f} :catchall_0

    goto/16 :goto_0
.end method

.method protected final a(Ljava/lang/Boolean;)V
    .locals 9

    .prologue
    const/4 v8, 0x4

    const/4 v7, 0x0

    const/4 v6, 0x0

    .line 4746
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x15

    if-lt v0, v1, :cond_1

    .line 4748
    invoke-super {p0, p1}, Lcom/msc/c/b;->a(Ljava/lang/Boolean;)V

    .line 4754
    :goto_0
    iget-object v0, p0, Lcom/osp/app/signin/jk;->b:Lcom/msc/c/f;

    if-eqz v0, :cond_d

    .line 4761
    iget-object v0, p0, Lcom/osp/app/signin/jk;->c:Lcom/osp/app/signin/SignInView;

    invoke-static {v0}, Lcom/osp/app/signin/SignInView;->c(Lcom/osp/app/signin/SignInView;)Landroid/content/Intent;

    move-result-object v0

    const-string v1, "signin_duration"

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    iget-object v4, p0, Lcom/osp/app/signin/jk;->c:Lcom/osp/app/signin/SignInView;

    invoke-static {v4}, Lcom/osp/app/signin/SignInView;->B(Lcom/osp/app/signin/SignInView;)J

    move-result-wide v4

    sub-long/2addr v2, v4

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;J)Landroid/content/Intent;

    .line 4762
    iget-object v0, p0, Lcom/osp/app/signin/jk;->c:Lcom/osp/app/signin/SignInView;

    invoke-static {v0}, Lcom/osp/app/signin/SignInView;->c(Lcom/osp/app/signin/SignInView;)Landroid/content/Intent;

    move-result-object v0

    const-string v1, "signin_status"

    invoke-virtual {v0, v1, v6}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 4763
    iget-object v0, p0, Lcom/osp/app/signin/jk;->c:Lcom/osp/app/signin/SignInView;

    invoke-static {v0}, Lcom/osp/app/signin/SignInView;->c(Lcom/osp/app/signin/SignInView;)Landroid/content/Intent;

    move-result-object v0

    const-string v1, "error_code"

    iget-object v2, p0, Lcom/osp/app/signin/jk;->b:Lcom/msc/c/f;

    invoke-virtual {v2}, Lcom/msc/c/f;->a()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 4765
    new-instance v0, Lcom/osp/app/bigdatalog/c;

    iget-object v1, p0, Lcom/osp/app/signin/jk;->c:Lcom/osp/app/signin/SignInView;

    invoke-virtual {v1}, Lcom/osp/app/signin/SignInView;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    const/16 v2, 0x9

    iget-object v3, p0, Lcom/osp/app/signin/jk;->c:Lcom/osp/app/signin/SignInView;

    invoke-static {v3}, Lcom/osp/app/signin/SignInView;->c(Lcom/osp/app/signin/SignInView;)Landroid/content/Intent;

    move-result-object v3

    invoke-direct {v0, v1, v8, v2, v3}, Lcom/osp/app/bigdatalog/c;-><init>(Landroid/content/Context;IILandroid/content/Intent;)V

    .line 4768
    invoke-static {}, Lcom/osp/app/bigdatalog/e;->a()Lcom/osp/app/bigdatalog/e;

    move-result-object v1

    iget-object v2, p0, Lcom/osp/app/signin/jk;->c:Lcom/osp/app/signin/SignInView;

    invoke-virtual {v2}, Lcom/osp/app/signin/SignInView;->getApplicationContext()Landroid/content/Context;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/osp/app/bigdatalog/e;->a(Landroid/content/Context;)V

    .line 4769
    invoke-static {}, Lcom/osp/app/bigdatalog/e;->a()Lcom/osp/app/bigdatalog/e;

    move-result-object v1

    iget-object v2, p0, Lcom/osp/app/signin/jk;->c:Lcom/osp/app/signin/SignInView;

    invoke-virtual {v2}, Lcom/osp/app/signin/SignInView;->getApplicationContext()Landroid/content/Context;

    move-result-object v2

    invoke-virtual {v1, v2, v0}, Lcom/osp/app/bigdatalog/e;->a(Landroid/content/Context;Lcom/osp/app/bigdatalog/c;)V

    .line 4773
    iget-object v0, p0, Lcom/osp/app/signin/jk;->c:Lcom/osp/app/signin/SignInView;

    invoke-static {v0}, Lcom/osp/app/signin/SignInView;->C(Lcom/osp/app/signin/SignInView;)Z

    move-result v0

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/osp/app/signin/jk;->c:Lcom/osp/app/signin/SignInView;

    invoke-static {v0}, Lcom/osp/app/signin/SignInView;->D(Lcom/osp/app/signin/SignInView;)Z

    move-result v0

    if-nez v0, :cond_2

    const-string v0, "USR_3113"

    iget-object v1, p0, Lcom/osp/app/signin/jk;->b:Lcom/msc/c/f;

    invoke-virtual {v1}, Lcom/msc/c/f;->a()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 4775
    iget-object v0, p0, Lcom/osp/app/signin/jk;->c:Lcom/osp/app/signin/SignInView;

    iget-object v1, p0, Lcom/osp/app/signin/jk;->c:Lcom/osp/app/signin/SignInView;

    invoke-static {v0, v1}, Lcom/osp/app/signin/SignInView;->a(Lcom/osp/app/signin/SignInView;Landroid/content/Context;)V

    .line 4849
    :cond_0
    :goto_1
    return-void

    .line 4751
    :cond_1
    invoke-super {p0}, Lcom/msc/c/b;->e()V

    goto/16 :goto_0

    .line 4776
    :cond_2
    iget-object v0, p0, Lcom/osp/app/signin/jk;->c:Lcom/osp/app/signin/SignInView;

    invoke-static {v0}, Lcom/osp/app/signin/SignInView;->C(Lcom/osp/app/signin/SignInView;)Z

    move-result v0

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/osp/app/signin/jk;->c:Lcom/osp/app/signin/SignInView;

    invoke-static {v0}, Lcom/osp/app/signin/SignInView;->D(Lcom/osp/app/signin/SignInView;)Z

    move-result v0

    if-nez v0, :cond_3

    const-string v0, "USR_3192"

    iget-object v1, p0, Lcom/osp/app/signin/jk;->b:Lcom/msc/c/f;

    invoke-virtual {v1}, Lcom/msc/c/f;->a()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 4778
    iget-object v0, p0, Lcom/osp/app/signin/jk;->c:Lcom/osp/app/signin/SignInView;

    iget-object v1, p0, Lcom/osp/app/signin/jk;->c:Lcom/osp/app/signin/SignInView;

    invoke-static {v0, v1}, Lcom/osp/app/signin/SignInView;->b(Lcom/osp/app/signin/SignInView;Landroid/content/Context;)V

    goto :goto_1

    .line 4779
    :cond_3
    invoke-static {}, Lcom/osp/device/b;->b()Lcom/osp/device/b;

    move-result-object v0

    invoke-virtual {v0}, Lcom/osp/device/b;->a()Z

    move-result v0

    if-eqz v0, :cond_4

    const-string v0, "USR_3174"

    iget-object v1, p0, Lcom/osp/app/signin/jk;->b:Lcom/msc/c/f;

    invoke-virtual {v1}, Lcom/msc/c/f;->a()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 4783
    iget-object v0, p0, Lcom/osp/app/signin/jk;->c:Lcom/osp/app/signin/SignInView;

    invoke-static {v0}, Lcom/osp/app/signin/SignInView;->M(Lcom/osp/app/signin/SignInView;)V

    goto :goto_1

    .line 4784
    :cond_4
    invoke-static {}, Lcom/osp/device/b;->b()Lcom/osp/device/b;

    move-result-object v0

    invoke-virtual {v0}, Lcom/osp/device/b;->a()Z

    move-result v0

    if-eqz v0, :cond_5

    const-string v0, "DUPLICATED_ID"

    iget-object v1, p0, Lcom/osp/app/signin/jk;->b:Lcom/msc/c/f;

    invoke-virtual {v1}, Lcom/msc/c/f;->a()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_5

    .line 4787
    iget-object v0, p0, Lcom/osp/app/signin/jk;->c:Lcom/osp/app/signin/SignInView;

    const/4 v1, 0x2

    iget-object v2, p0, Lcom/osp/app/signin/jk;->c:Lcom/osp/app/signin/SignInView;

    iget-object v2, v2, Lcom/osp/app/signin/SignInView;->d:Landroid/content/DialogInterface$OnCancelListener;

    invoke-virtual {v0, v1, v7, v2}, Lcom/osp/app/signin/SignInView;->a(ILandroid/content/DialogInterface$OnDismissListener;Landroid/content/DialogInterface$OnCancelListener;)V

    goto :goto_1

    .line 4788
    :cond_5
    iget-object v0, p0, Lcom/osp/app/signin/jk;->c:Lcom/osp/app/signin/SignInView;

    invoke-static {v0}, Lcom/osp/app/signin/SignInView;->F(Lcom/osp/app/signin/SignInView;)Z

    move-result v0

    if-eqz v0, :cond_8

    .line 4790
    iget-object v0, p0, Lcom/osp/app/signin/jk;->c:Lcom/osp/app/signin/SignInView;

    invoke-static {v0, v6}, Lcom/osp/app/signin/SignInView;->b(Lcom/osp/app/signin/SignInView;Z)Z

    .line 4791
    iget-object v0, p0, Lcom/osp/app/signin/jk;->c:Lcom/osp/app/signin/SignInView;

    invoke-static {v0}, Lcom/osp/app/signin/SignInView;->G(Lcom/osp/app/signin/SignInView;)Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_6

    .line 4793
    iget-object v0, p0, Lcom/osp/app/signin/jk;->c:Lcom/osp/app/signin/SignInView;

    iget-object v1, p0, Lcom/osp/app/signin/jk;->c:Lcom/osp/app/signin/SignInView;

    invoke-static {v1}, Lcom/osp/app/signin/SignInView;->G(Lcom/osp/app/signin/SignInView;)Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/osp/app/signin/jk;->c:Lcom/osp/app/signin/SignInView;

    invoke-virtual {v2}, Lcom/osp/app/signin/SignInView;->getApplicationContext()Landroid/content/Context;

    iget-object v2, p0, Lcom/osp/app/signin/jk;->c:Lcom/osp/app/signin/SignInView;

    invoke-static {v2}, Lcom/osp/app/signin/SignInView;->G(Lcom/osp/app/signin/SignInView;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/osp/common/util/i;->d(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v1, v2}, Lcom/msc/c/e;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)V

    .line 4794
    iget-object v0, p0, Lcom/osp/app/signin/jk;->c:Lcom/osp/app/signin/SignInView;

    new-instance v1, Lcom/osp/app/signin/jj;

    iget-object v2, p0, Lcom/osp/app/signin/jk;->c:Lcom/osp/app/signin/SignInView;

    invoke-direct {v1, v2}, Lcom/osp/app/signin/jj;-><init>(Lcom/osp/app/signin/SignInView;)V

    invoke-static {v0, v1}, Lcom/osp/app/signin/SignInView;->a(Lcom/osp/app/signin/SignInView;Lcom/osp/app/signin/jj;)Lcom/osp/app/signin/jj;

    .line 4795
    iget-object v0, p0, Lcom/osp/app/signin/jk;->c:Lcom/osp/app/signin/SignInView;

    invoke-static {v0}, Lcom/osp/app/signin/SignInView;->H(Lcom/osp/app/signin/SignInView;)Lcom/osp/app/signin/jj;

    move-result-object v0

    invoke-static {v0}, Lcom/msc/sa/c/d;->a(Landroid/os/AsyncTask;)V

    goto/16 :goto_1

    .line 4798
    :cond_6
    invoke-static {}, Lcom/osp/device/b;->b()Lcom/osp/device/b;

    move-result-object v0

    iget-object v1, p0, Lcom/osp/app/signin/jk;->c:Lcom/osp/app/signin/SignInView;

    invoke-virtual {v0, v1}, Lcom/osp/device/b;->i(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_7

    .line 4800
    iget-object v0, p0, Lcom/osp/app/signin/jk;->c:Lcom/osp/app/signin/SignInView;

    new-instance v1, Lcom/osp/app/signin/jm;

    iget-object v2, p0, Lcom/osp/app/signin/jk;->c:Lcom/osp/app/signin/SignInView;

    invoke-direct {v1, v2}, Lcom/osp/app/signin/jm;-><init>(Lcom/osp/app/signin/SignInView;)V

    invoke-static {v0, v1}, Lcom/osp/app/signin/SignInView;->a(Lcom/osp/app/signin/SignInView;Lcom/osp/app/signin/jm;)Lcom/osp/app/signin/jm;

    .line 4801
    iget-object v0, p0, Lcom/osp/app/signin/jk;->c:Lcom/osp/app/signin/SignInView;

    invoke-static {v0}, Lcom/osp/app/signin/SignInView;->I(Lcom/osp/app/signin/SignInView;)Lcom/osp/app/signin/jm;

    move-result-object v0

    new-array v1, v6, [Ljava/lang/Void;

    invoke-virtual {v0, v1}, Lcom/osp/app/signin/jm;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    goto/16 :goto_1

    .line 4804
    :cond_7
    iget-object v0, p0, Lcom/osp/app/signin/jk;->c:Lcom/osp/app/signin/SignInView;

    invoke-static {v0}, Lcom/osp/app/signin/SignInView;->J(Lcom/osp/app/signin/SignInView;)V

    goto/16 :goto_1

    .line 4807
    :cond_8
    iget-object v0, p0, Lcom/osp/app/signin/jk;->c:Lcom/osp/app/signin/SignInView;

    invoke-static {v0}, Lcom/osp/app/signin/SignInView;->N(Lcom/osp/app/signin/SignInView;)Z

    move-result v0

    if-eqz v0, :cond_9

    invoke-static {}, Lcom/osp/app/signin/gv;->a()Lcom/osp/app/signin/gs;

    move-result-object v0

    iget-object v1, p0, Lcom/osp/app/signin/jk;->c:Lcom/osp/app/signin/SignInView;

    invoke-virtual {v0, v1, v7}, Lcom/osp/app/signin/gs;->a(Landroid/content/Context;Lcom/osp/app/signin/gu;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 4811
    :cond_9
    iget-object v0, p0, Lcom/osp/app/signin/jk;->c:Lcom/osp/app/signin/SignInView;

    invoke-static {v0}, Lcom/osp/app/signin/SignInView;->N(Lcom/osp/app/signin/SignInView;)Z

    move-result v0

    if-nez v0, :cond_a

    const-string v0, "AUT_1885"

    iget-object v1, p0, Lcom/osp/app/signin/jk;->b:Lcom/msc/c/f;

    invoke-virtual {v1}, Lcom/msc/c/f;->a()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_a

    .line 4813
    iget-object v0, p0, Lcom/osp/app/signin/jk;->c:Lcom/osp/app/signin/SignInView;

    iget-object v1, p0, Lcom/osp/app/signin/jk;->c:Lcom/osp/app/signin/SignInView;

    iget-object v1, v1, Lcom/osp/app/signin/SignInView;->f:Landroid/content/DialogInterface$OnCancelListener;

    invoke-virtual {v0, v8, v7, v1}, Lcom/osp/app/signin/SignInView;->a(ILandroid/content/DialogInterface$OnDismissListener;Landroid/content/DialogInterface$OnCancelListener;)V

    goto/16 :goto_1

    .line 4817
    :cond_a
    const-string v0, "USR_3113"

    iget-object v1, p0, Lcom/osp/app/signin/jk;->b:Lcom/msc/c/f;

    invoke-virtual {v1}, Lcom/msc/c/f;->a()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_b

    const-string v0, "USR_3121"

    iget-object v1, p0, Lcom/osp/app/signin/jk;->b:Lcom/msc/c/f;

    invoke-virtual {v1}, Lcom/msc/c/f;->a()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_c

    .line 4819
    :cond_b
    iget-object v0, p0, Lcom/osp/app/signin/jk;->b:Lcom/msc/c/f;

    const-string v1, "IVIP"

    invoke-virtual {v0, v1}, Lcom/msc/c/f;->a(Ljava/lang/String;)V

    .line 4821
    :cond_c
    invoke-virtual {p0, v6}, Lcom/osp/app/signin/jk;->a(Z)V

    .line 4822
    iget-object v0, p0, Lcom/osp/app/signin/jk;->c:Lcom/osp/app/signin/SignInView;

    invoke-static {v0}, Lcom/osp/app/signin/SignInView;->a(Lcom/osp/app/signin/SignInView;)V

    goto/16 :goto_1

    .line 4826
    :cond_d
    iget-object v0, p0, Lcom/osp/app/signin/jk;->c:Lcom/osp/app/signin/SignInView;

    invoke-static {v0}, Lcom/osp/app/signin/SignInView;->K(Lcom/osp/app/signin/SignInView;)Lcom/msc/a/g;

    move-result-object v0

    if-eqz v0, :cond_10

    .line 4828
    iget-object v0, p0, Lcom/osp/app/signin/jk;->c:Lcom/osp/app/signin/SignInView;

    iget-object v1, p0, Lcom/osp/app/signin/jk;->c:Lcom/osp/app/signin/SignInView;

    invoke-static {v1}, Lcom/osp/app/signin/SignInView;->K(Lcom/osp/app/signin/SignInView;)Lcom/msc/a/g;

    move-result-object v1

    invoke-virtual {v1}, Lcom/msc/a/g;->f()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/msc/c/e;->d(Landroid/content/Context;Ljava/lang/String;)V

    .line 4829
    invoke-static {}, Lcom/osp/device/b;->b()Lcom/osp/device/b;

    move-result-object v0

    invoke-virtual {v0}, Lcom/osp/device/b;->a()Z

    move-result v0

    if-eqz v0, :cond_e

    .line 4831
    iget-object v0, p0, Lcom/osp/app/signin/jk;->c:Lcom/osp/app/signin/SignInView;

    invoke-static {v0}, Lcom/osp/app/signin/SignInView;->K(Lcom/osp/app/signin/SignInView;)Lcom/msc/a/g;

    move-result-object v0

    invoke-virtual {v0}, Lcom/msc/a/g;->c()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_e

    iget-object v0, p0, Lcom/osp/app/signin/jk;->c:Lcom/osp/app/signin/SignInView;

    invoke-static {v0}, Lcom/osp/app/signin/SignInView;->K(Lcom/osp/app/signin/SignInView;)Lcom/msc/a/g;

    move-result-object v0

    invoke-virtual {v0}, Lcom/msc/a/g;->c()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/telephony/PhoneNumberUtils;->isGlobalPhoneNumber(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_e

    .line 4834
    iget-object v0, p0, Lcom/osp/app/signin/jk;->c:Lcom/osp/app/signin/SignInView;

    invoke-static {v0}, Lcom/osp/app/signin/SignInView;->e(Lcom/osp/app/signin/SignInView;)Z

    move-result v0

    if-eqz v0, :cond_f

    iget-object v0, p0, Lcom/osp/app/signin/jk;->c:Lcom/osp/app/signin/SignInView;

    iget-object v0, v0, Lcom/osp/app/signin/SignInView;->a:Landroid/app/AlertDialog;

    if-eqz v0, :cond_f

    .line 4836
    iget-object v0, p0, Lcom/osp/app/signin/jk;->c:Lcom/osp/app/signin/SignInView;

    invoke-static {v0}, Lcom/osp/app/signin/SignInView;->s(Lcom/osp/app/signin/SignInView;)Landroid/view/View;

    move-result-object v0

    const v1, 0x7f0c0129

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/EditText;

    iget-object v1, p0, Lcom/osp/app/signin/jk;->c:Lcom/osp/app/signin/SignInView;

    invoke-static {v1}, Lcom/osp/app/signin/SignInView;->K(Lcom/osp/app/signin/SignInView;)Lcom/msc/a/g;

    move-result-object v1

    invoke-virtual {v1}, Lcom/msc/a/g;->c()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    .line 4843
    :cond_e
    :goto_2
    iget-object v0, p0, Lcom/osp/app/signin/jk;->c:Lcom/osp/app/signin/SignInView;

    iget-object v1, p0, Lcom/osp/app/signin/jk;->c:Lcom/osp/app/signin/SignInView;

    invoke-static {v1}, Lcom/osp/app/signin/SignInView;->K(Lcom/osp/app/signin/SignInView;)Lcom/msc/a/g;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/osp/app/signin/SignInView;->a(Lcom/osp/app/signin/SignInView;Lcom/msc/a/g;)V

    goto/16 :goto_1

    .line 4839
    :cond_f
    iget-object v0, p0, Lcom/osp/app/signin/jk;->c:Lcom/osp/app/signin/SignInView;

    const v1, 0x7f0c0121

    invoke-virtual {v0, v1}, Lcom/osp/app/signin/SignInView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/EditText;

    iget-object v1, p0, Lcom/osp/app/signin/jk;->c:Lcom/osp/app/signin/SignInView;

    invoke-static {v1}, Lcom/osp/app/signin/SignInView;->K(Lcom/osp/app/signin/SignInView;)Lcom/msc/a/g;

    move-result-object v1

    invoke-virtual {v1}, Lcom/msc/a/g;->c()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    goto :goto_2

    .line 4846
    :cond_10
    invoke-virtual {p0, v6}, Lcom/osp/app/signin/jk;->a(Z)V

    .line 4847
    iget-object v0, p0, Lcom/osp/app/signin/jk;->c:Lcom/osp/app/signin/SignInView;

    invoke-static {v0}, Lcom/osp/app/signin/SignInView;->a(Lcom/osp/app/signin/SignInView;)V

    goto/16 :goto_1
.end method

.method public final b(Lcom/msc/b/c;)V
    .locals 6

    .prologue
    .line 4961
    invoke-super {p0, p1}, Lcom/msc/c/b;->b(Lcom/msc/b/c;)V

    .line 4963
    if-nez p1, :cond_1

    .line 5012
    :cond_0
    :goto_0
    return-void

    .line 4968
    :cond_1
    invoke-virtual {p1}, Lcom/msc/b/c;->a()J

    move-result-wide v0

    .line 4969
    invoke-virtual {p1}, Lcom/msc/b/c;->e()Ljava/lang/String;

    move-result-object v2

    .line 4971
    iget-wide v4, p0, Lcom/osp/app/signin/jk;->d:J

    cmp-long v3, v0, v4

    if-nez v3, :cond_6

    .line 4973
    if-eqz v2, :cond_0

    .line 4977
    :try_start_0
    iget-object v0, p0, Lcom/osp/app/signin/jk;->c:Lcom/osp/app/signin/SignInView;

    invoke-static {v0}, Lcom/osp/app/signin/SignInView;->N(Lcom/osp/app/signin/SignInView;)Z

    move-result v0

    if-eqz v0, :cond_3

    const-string v0, "USR_3121"

    iget-object v1, p0, Lcom/osp/app/signin/jk;->b:Lcom/msc/c/f;

    invoke-virtual {v1}, Lcom/msc/c/f;->a()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_2

    const-string v0, "USR_3113"

    iget-object v1, p0, Lcom/osp/app/signin/jk;->b:Lcom/msc/c/f;

    invoke-virtual {v1}, Lcom/msc/c/f;->a()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_2

    const-string v0, "USR_1464"

    iget-object v1, p0, Lcom/osp/app/signin/jk;->b:Lcom/msc/c/f;

    invoke-virtual {v1}, Lcom/msc/c/f;->a()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 4982
    :cond_2
    invoke-static {}, Lcom/osp/app/signin/gv;->a()Lcom/osp/app/signin/gs;

    move-result-object v0

    invoke-virtual {v0}, Lcom/osp/app/signin/gs;->a()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 4998
    :catch_0
    move-exception v0

    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_0

    .line 4986
    :cond_3
    :try_start_1
    const-string v0, "SSO_8005"

    iget-object v1, p0, Lcom/osp/app/signin/jk;->b:Lcom/msc/c/f;

    invoke-virtual {v1}, Lcom/msc/c/f;->a()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    const/4 v1, 0x1

    if-ne v0, v1, :cond_4

    .line 4988
    new-instance v0, Lcom/osp/security/time/a;

    iget-object v1, p0, Lcom/osp/app/signin/jk;->c:Lcom/osp/app/signin/SignInView;

    invoke-direct {v0, v1}, Lcom/osp/security/time/a;-><init>(Landroid/content/Context;)V

    .line 4989
    invoke-virtual {v0}, Lcom/osp/security/time/a;->b()V

    goto :goto_0

    .line 4990
    :cond_4
    const-string v0, "USR_1573"

    iget-object v1, p0, Lcom/osp/app/signin/jk;->b:Lcom/msc/c/f;

    invoke-virtual {v1}, Lcom/msc/c/f;->a()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 4992
    invoke-virtual {p0}, Lcom/osp/app/signin/jk;->isCancelled()Z

    move-result v0

    if-nez v0, :cond_0

    .line 4996
    iget-object v0, p0, Lcom/osp/app/signin/jk;->c:Lcom/osp/app/signin/SignInView;

    invoke-static {v0}, Lcom/osp/app/signin/SignInView;->e(Lcom/osp/app/signin/SignInView;)Z

    move-result v0

    if-eqz v0, :cond_5

    iget-object v0, p0, Lcom/osp/app/signin/jk;->c:Lcom/osp/app/signin/SignInView;

    invoke-static {v0}, Lcom/osp/app/signin/SignInView;->s(Lcom/osp/app/signin/SignInView;)Landroid/view/View;

    move-result-object v0

    const v1, 0x7f0c0129

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/EditText;

    iget-object v1, p0, Lcom/osp/app/signin/jk;->c:Lcom/osp/app/signin/SignInView;

    invoke-static {v1}, Lcom/osp/app/signin/SignInView;->s(Lcom/osp/app/signin/SignInView;)Landroid/view/View;

    move-result-object v1

    const v2, 0x7f0c012a

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/EditText;

    :goto_1
    invoke-static {}, Lcom/msc/c/g;->a()Lcom/msc/c/g;

    iget-object v2, p0, Lcom/osp/app/signin/jk;->c:Lcom/osp/app/signin/SignInView;

    invoke-virtual {v0}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v2, v0, v1, p0}, Lcom/msc/c/g;->b(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Lcom/msc/b/h;)J

    move-result-wide v0

    iput-wide v0, p0, Lcom/osp/app/signin/jk;->e:J

    iget-wide v0, p0, Lcom/osp/app/signin/jk;->e:J

    invoke-virtual {p0, v0, v1}, Lcom/osp/app/signin/jk;->a(J)V

    iget-wide v0, p0, Lcom/osp/app/signin/jk;->e:J

    const-string v2, "verify_xml"

    invoke-virtual {p0, v0, v1, v2}, Lcom/osp/app/signin/jk;->a(JLjava/lang/String;)V

    iget-wide v0, p0, Lcom/osp/app/signin/jk;->e:J

    sget-object v2, Lcom/msc/b/g;->c:Lcom/msc/b/g;

    invoke-static {v0, v1, v2}, Lcom/msc/c/g;->a(JLcom/msc/b/g;)V

    goto/16 :goto_0

    :cond_5
    iget-object v0, p0, Lcom/osp/app/signin/jk;->c:Lcom/osp/app/signin/SignInView;

    const v1, 0x7f0c0121

    invoke-virtual {v0, v1}, Lcom/osp/app/signin/SignInView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/EditText;

    iget-object v1, p0, Lcom/osp/app/signin/jk;->c:Lcom/osp/app/signin/SignInView;

    const v2, 0x7f0c0122

    invoke-virtual {v1, v2}, Lcom/osp/app/signin/SignInView;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/EditText;
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_1

    .line 5003
    :cond_6
    iget-wide v2, p0, Lcom/osp/app/signin/jk;->f:J

    cmp-long v0, v0, v2

    if-nez v0, :cond_0

    .line 5005
    iget-object v0, p0, Lcom/osp/app/signin/jk;->c:Lcom/osp/app/signin/SignInView;

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lcom/osp/app/signin/SignInView;->d(Lcom/osp/app/signin/SignInView;Ljava/lang/String;)Ljava/lang/String;

    goto/16 :goto_0
.end method

.method protected final d()V
    .locals 1

    .prologue
    .line 4669
    invoke-super {p0}, Lcom/msc/c/b;->d()V

    .line 4670
    iget-object v0, p0, Lcom/osp/app/signin/jk;->c:Lcom/osp/app/signin/SignInView;

    invoke-static {v0}, Lcom/osp/app/signin/SignInView;->a(Lcom/osp/app/signin/SignInView;)V

    .line 4671
    return-void
.end method

.method protected final synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 4640
    invoke-virtual {p0}, Lcom/osp/app/signin/jk;->a()Ljava/lang/Boolean;

    move-result-object v0

    return-object v0
.end method

.method protected final synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 4640
    check-cast p1, Ljava/lang/Boolean;

    invoke-virtual {p0, p1}, Lcom/osp/app/signin/jk;->a(Ljava/lang/Boolean;)V

    return-void
.end method
