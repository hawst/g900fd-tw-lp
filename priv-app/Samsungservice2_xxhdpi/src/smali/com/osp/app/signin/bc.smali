.class final Lcom/osp/app/signin/bc;
.super Lcom/msc/c/b;
.source "AccountView.java"


# instance fields
.field final synthetic c:Lcom/osp/app/signin/AccountView;

.field private d:J

.field private e:J

.field private f:Ljava/lang/String;

.field private g:I

.field private h:Ljava/lang/String;


# direct methods
.method public constructor <init>(Lcom/osp/app/signin/AccountView;I)V
    .locals 2

    .prologue
    .line 10047
    iput-object p1, p0, Lcom/osp/app/signin/bc;->c:Lcom/osp/app/signin/AccountView;

    .line 10048
    invoke-direct {p0, p1}, Lcom/msc/c/b;-><init>(Landroid/content/Context;)V

    .line 10043
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/osp/app/signin/bc;->f:Ljava/lang/String;

    .line 10044
    const/4 v0, 0x0

    iput v0, p0, Lcom/osp/app/signin/bc;->g:I

    .line 10049
    invoke-static {}, Lcom/osp/device/b;->b()Lcom/osp/device/b;

    move-result-object v0

    invoke-virtual {v0}, Lcom/osp/device/b;->n()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/osp/app/signin/bc;->f:Ljava/lang/String;

    .line 10050
    iput p2, p0, Lcom/osp/app/signin/bc;->g:I

    .line 10052
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "EnablePhoneIDCheckTask mDeviceCountryCode = ["

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v1, p0, Lcom/osp/app/signin/bc;->f:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "]"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;)V

    .line 10056
    return-void
.end method


# virtual methods
.method protected final varargs a()Ljava/lang/Boolean;
    .locals 3

    .prologue
    .line 10061
    iget-object v0, p0, Lcom/osp/app/signin/bc;->f:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    const-string v0, "US"

    .line 10063
    :goto_0
    invoke-static {}, Lcom/msc/c/g;->a()Lcom/msc/c/g;

    iget-object v1, p0, Lcom/osp/app/signin/bc;->c:Lcom/osp/app/signin/AccountView;

    invoke-static {v1, v0, p0}, Lcom/msc/c/g;->e(Landroid/content/Context;Ljava/lang/String;Lcom/msc/b/h;)J

    move-result-wide v0

    iput-wide v0, p0, Lcom/osp/app/signin/bc;->d:J

    iget-wide v0, p0, Lcom/osp/app/signin/bc;->d:J

    const-string v2, "none"

    invoke-virtual {p0, v0, v1, v2}, Lcom/osp/app/signin/bc;->a(JLjava/lang/String;)V

    iget-wide v0, p0, Lcom/osp/app/signin/bc;->d:J

    invoke-virtual {p0, v0, v1}, Lcom/osp/app/signin/bc;->a(J)V

    iget-wide v0, p0, Lcom/osp/app/signin/bc;->d:J

    sget-object v2, Lcom/msc/b/g;->b:Lcom/msc/b/g;

    invoke-static {v0, v1, v2}, Lcom/msc/c/g;->a(JLcom/msc/b/g;)V

    .line 10065
    const/4 v0, 0x0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    return-object v0

    .line 10061
    :cond_0
    iget-object v0, p0, Lcom/osp/app/signin/bc;->f:Ljava/lang/String;

    goto :goto_0
.end method

.method public final declared-synchronized a(Lcom/msc/b/c;)V
    .locals 6

    .prologue
    .line 10117
    monitor-enter p0

    :try_start_0
    invoke-super {p0, p1}, Lcom/msc/c/b;->a(Lcom/msc/b/c;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 10119
    if-nez p1, :cond_1

    .line 10183
    :cond_0
    :goto_0
    monitor-exit p0

    return-void

    .line 10124
    :cond_1
    :try_start_1
    invoke-virtual {p1}, Lcom/msc/b/c;->a()J

    move-result-wide v0

    .line 10125
    invoke-virtual {p1}, Lcom/msc/b/c;->e()Ljava/lang/String;

    move-result-object v2

    .line 10127
    iget-wide v4, p0, Lcom/osp/app/signin/bc;->d:J
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    cmp-long v3, v0, v4

    if-nez v3, :cond_7

    .line 10131
    :try_start_2
    invoke-static {}, Lcom/msc/c/h;->a()Lcom/msc/c/h;

    iget-object v0, p0, Lcom/osp/app/signin/bc;->c:Lcom/osp/app/signin/AccountView;

    invoke-static {v2}, Lcom/msc/c/h;->K(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 10132
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "support phone number id from server : "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;)V

    .line 10134
    iget-object v2, p0, Lcom/osp/app/signin/bc;->c:Lcom/osp/app/signin/AccountView;

    const-string v0, "false"

    invoke-static {v2}, Lcom/osp/app/util/ad;->p(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-eqz v4, :cond_2

    invoke-static {}, Lcom/msc/c/g;->a()Lcom/msc/c/g;

    iget-object v1, p0, Lcom/osp/app/signin/bc;->c:Lcom/osp/app/signin/AccountView;

    invoke-static {v1, p0}, Lcom/msc/c/g;->b(Landroid/content/Context;Lcom/msc/b/h;)J

    move-result-wide v4

    iput-wide v4, p0, Lcom/osp/app/signin/bc;->e:J

    iget-wide v4, p0, Lcom/osp/app/signin/bc;->e:J

    invoke-virtual {p0, v4, v5}, Lcom/osp/app/signin/bc;->a(J)V

    iget-wide v4, p0, Lcom/osp/app/signin/bc;->e:J

    sget-object v1, Lcom/msc/b/g;->b:Lcom/msc/b/g;

    invoke-static {v4, v5, v1}, Lcom/msc/c/g;->a(JLcom/msc/b/g;)V

    invoke-static {v2}, Lcom/osp/app/util/ad;->p(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v1

    :cond_2
    invoke-static {}, Lcom/osp/common/util/i;->a()Lcom/osp/common/util/i;

    invoke-static {v1}, Lcom/osp/common/util/i;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-static {}, Lcom/osp/device/b;->b()Lcom/osp/device/b;

    move-result-object v1

    invoke-virtual {v1}, Lcom/osp/device/b;->n()Ljava/lang/String;

    move-result-object v1

    if-eqz v2, :cond_3

    sget-object v4, Ljava/util/Locale;->ENGLISH:Ljava/util/Locale;

    invoke-virtual {v2, v4}, Ljava/lang/String;->toUpperCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v2

    :cond_3
    if-eqz v1, :cond_4

    sget-object v4, Ljava/util/Locale;->ENGLISH:Ljava/util/Locale;

    invoke-virtual {v1, v4}, Ljava/lang/String;->toUpperCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v1

    :cond_4
    invoke-static {v3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-nez v4, :cond_5

    const-string v4, "true"

    invoke-virtual {v4, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_5

    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v3, "countryCodeFromMcc : "

    invoke-direct {v0, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v3, "\t countryCodeFromCSC : "

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;)V

    if-eqz v1, :cond_6

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_6

    const-string v0, "true"

    .line 10135
    :cond_5
    :goto_1
    invoke-static {}, Lcom/osp/device/b;->b()Lcom/osp/device/b;

    move-result-object v1

    iget-object v2, p0, Lcom/osp/app/signin/bc;->c:Lcom/osp/app/signin/AccountView;

    invoke-virtual {v1, v2, v0}, Lcom/osp/device/b;->a(Landroid/content/Context;Ljava/lang/String;)V

    .line 10136
    invoke-static {}, Lcom/osp/device/b;->b()Lcom/osp/device/b;

    iget-object v1, p0, Lcom/osp/app/signin/bc;->c:Lcom/osp/app/signin/AccountView;

    invoke-static {v1, v0}, Lcom/osp/device/b;->b(Landroid/content/Context;Ljava/lang/String;)V

    .line 10137
    const-string v0, "Success"

    iput-object v0, p0, Lcom/osp/app/signin/bc;->h:Ljava/lang/String;
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_0
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto/16 :goto_0

    .line 10139
    :catch_0
    move-exception v0

    :try_start_3
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    .line 10144
    invoke-static {}, Lcom/osp/device/b;->b()Lcom/osp/device/b;

    move-result-object v0

    iget-object v1, p0, Lcom/osp/app/signin/bc;->c:Lcom/osp/app/signin/AccountView;

    const-string v2, "false"

    invoke-virtual {v0, v1, v2}, Lcom/osp/device/b;->a(Landroid/content/Context;Ljava/lang/String;)V

    .line 10145
    invoke-static {}, Lcom/osp/device/b;->b()Lcom/osp/device/b;

    iget-object v0, p0, Lcom/osp/app/signin/bc;->c:Lcom/osp/app/signin/AccountView;

    const-string v1, "false"

    invoke-static {v0, v1}, Lcom/osp/device/b;->b(Landroid/content/Context;Ljava/lang/String;)V

    .line 10146
    const-string v0, "Fail"

    iput-object v0, p0, Lcom/osp/app/signin/bc;->h:Ljava/lang/String;
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    goto/16 :goto_0

    .line 10117
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 10134
    :cond_6
    :try_start_4
    const-string v0, "false"
    :try_end_4
    .catch Ljava/lang/Exception; {:try_start_4 .. :try_end_4} :catch_0
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    goto :goto_1

    .line 10149
    :cond_7
    :try_start_5
    iget-wide v4, p0, Lcom/osp/app/signin/bc;->e:J
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    cmp-long v0, v0, v4

    if-nez v0, :cond_0

    .line 10153
    :try_start_6
    invoke-static {}, Lcom/msc/c/h;->a()Lcom/msc/c/h;

    invoke-static {v2}, Lcom/msc/c/h;->w(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 10155
    if-eqz v0, :cond_9

    .line 10157
    invoke-static {}, Lcom/osp/common/util/i;->a()Lcom/osp/common/util/i;

    iget-object v1, p0, Lcom/osp/app/signin/bc;->c:Lcom/osp/app/signin/AccountView;

    invoke-static {v1}, Lcom/osp/common/util/i;->o(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v1

    .line 10158
    invoke-static {}, Lcom/osp/common/util/i;->a()Lcom/osp/common/util/i;

    iget-object v2, p0, Lcom/osp/app/signin/bc;->c:Lcom/osp/app/signin/AccountView;

    invoke-static {v1}, Lcom/osp/common/util/i;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 10159
    if-eqz v2, :cond_8

    invoke-virtual {v2, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_8

    .line 10161
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v3, "GetMyCountryZoneTask countryCodeFromNetworkMcc = ["

    invoke-direct {v0, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, "]"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;)V

    .line 10162
    iget-object v0, p0, Lcom/osp/app/signin/bc;->c:Lcom/osp/app/signin/AccountView;

    invoke-static {v0, v1}, Lcom/osp/app/signin/AccountView;->g(Lcom/osp/app/signin/AccountView;Ljava/lang/String;)Ljava/lang/String;

    .line 10172
    :goto_2
    iget-object v0, p0, Lcom/osp/app/signin/bc;->c:Lcom/osp/app/signin/AccountView;

    invoke-static {v0}, Lcom/osp/app/signin/AccountView;->R(Lcom/osp/app/signin/AccountView;)Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/osp/app/signin/bc;->c:Lcom/osp/app/signin/AccountView;

    invoke-static {v0}, Lcom/osp/app/signin/AccountView;->R(Lcom/osp/app/signin/AccountView;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v0

    if-lez v0, :cond_0

    .line 10174
    iget-object v0, p0, Lcom/osp/app/signin/bc;->c:Lcom/osp/app/signin/AccountView;

    iget-object v1, p0, Lcom/osp/app/signin/bc;->c:Lcom/osp/app/signin/AccountView;

    invoke-static {v1}, Lcom/osp/app/signin/AccountView;->R(Lcom/osp/app/signin/AccountView;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/osp/app/util/ad;->e(Landroid/content/Context;Ljava/lang/String;)V
    :try_end_6
    .catch Ljava/lang/Exception; {:try_start_6 .. :try_end_6} :catch_1
    .catchall {:try_start_6 .. :try_end_6} :catchall_0

    goto/16 :goto_0

    .line 10177
    :catch_1
    move-exception v0

    :try_start_7
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_0

    goto/16 :goto_0

    .line 10165
    :cond_8
    :try_start_8
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "GetMyCountryZoneTask countryCode = ["

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "]"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;)V

    .line 10166
    iget-object v1, p0, Lcom/osp/app/signin/bc;->c:Lcom/osp/app/signin/AccountView;

    invoke-static {}, Lcom/osp/common/util/i;->a()Lcom/osp/common/util/i;

    iget-object v2, p0, Lcom/osp/app/signin/bc;->c:Lcom/osp/app/signin/AccountView;

    sget-object v2, Ljava/util/Locale;->ENGLISH:Ljava/util/Locale;

    invoke-virtual {v0, v2}, Ljava/lang/String;->toLowerCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/osp/common/util/i;->c(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Lcom/osp/app/signin/AccountView;->g(Lcom/osp/app/signin/AccountView;Ljava/lang/String;)Ljava/lang/String;

    goto :goto_2

    .line 10170
    :cond_9
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v0, "AV"

    const-string v1, "GetMyCountryZoneTask Success. but countryCode is null!!"

    invoke-static {v0, v1}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_8
    .catch Ljava/lang/Exception; {:try_start_8 .. :try_end_8} :catch_1
    .catchall {:try_start_8 .. :try_end_8} :catchall_0

    goto :goto_2
.end method

.method protected final a(Ljava/lang/Boolean;)V
    .locals 4

    .prologue
    const/4 v3, 0x1

    .line 10070
    invoke-super {p0, p1}, Lcom/msc/c/b;->a(Ljava/lang/Boolean;)V

    .line 10072
    const-string v0, "Success"

    iget-object v1, p0, Lcom/osp/app/signin/bc;->h:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 10074
    iget-object v0, p0, Lcom/osp/app/signin/bc;->c:Lcom/osp/app/signin/AccountView;

    iget-object v1, p0, Lcom/osp/app/signin/bc;->c:Lcom/osp/app/signin/AccountView;

    invoke-static {v1}, Lcom/osp/app/signin/AccountView;->R(Lcom/osp/app/signin/AccountView;)Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/osp/app/signin/bc;->c:Lcom/osp/app/signin/AccountView;

    invoke-virtual {v2}, Lcom/osp/app/signin/AccountView;->getApplicationContext()Landroid/content/Context;

    iget-object v2, p0, Lcom/osp/app/signin/bc;->c:Lcom/osp/app/signin/AccountView;

    invoke-static {v2}, Lcom/osp/app/signin/AccountView;->R(Lcom/osp/app/signin/AccountView;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/osp/common/util/i;->d(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v1, v2}, Lcom/msc/c/e;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)V

    .line 10075
    iget-object v0, p0, Lcom/osp/app/signin/bc;->c:Lcom/osp/app/signin/AccountView;

    invoke-static {v0}, Lcom/osp/app/util/r;->p(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 10077
    iget-object v0, p0, Lcom/osp/app/signin/bc;->c:Lcom/osp/app/signin/AccountView;

    iget v1, p0, Lcom/osp/app/signin/bc;->g:I

    invoke-static {v0, v1}, Lcom/osp/app/signin/AccountView;->a(Lcom/osp/app/signin/AccountView;I)V

    .line 10101
    :cond_0
    :goto_0
    return-void

    .line 10080
    :cond_1
    iget v0, p0, Lcom/osp/app/signin/bc;->g:I

    if-nez v0, :cond_2

    .line 10082
    iget-object v0, p0, Lcom/osp/app/signin/bc;->c:Lcom/osp/app/signin/AccountView;

    invoke-static {v0}, Lcom/osp/app/signin/AccountView;->N(Lcom/osp/app/signin/AccountView;)V

    goto :goto_0

    .line 10083
    :cond_2
    iget v0, p0, Lcom/osp/app/signin/bc;->g:I

    if-ne v0, v3, :cond_0

    .line 10085
    iget-object v0, p0, Lcom/osp/app/signin/bc;->c:Lcom/osp/app/signin/AccountView;

    invoke-static {v0}, Lcom/osp/app/signin/AccountView;->ag(Lcom/osp/app/signin/AccountView;)V

    goto :goto_0

    .line 10088
    :cond_3
    const-string v0, "Fail"

    iget-object v1, p0, Lcom/osp/app/signin/bc;->h:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 10090
    invoke-static {}, Lcom/osp/app/util/ap;->a()Lcom/osp/app/util/ap;

    iget-object v0, p0, Lcom/osp/app/signin/bc;->c:Lcom/osp/app/signin/AccountView;

    iget-object v1, p0, Lcom/osp/app/signin/bc;->c:Lcom/osp/app/signin/AccountView;

    const v2, 0x7f09004a

    invoke-virtual {v1, v2}, Lcom/osp/app/signin/AccountView;->getString(I)Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x0

    invoke-static {v0, v1, v2}, Lcom/osp/app/util/ap;->a(Landroid/content/Context;Ljava/lang/String;I)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    .line 10093
    iget-object v0, p0, Lcom/osp/app/signin/bc;->c:Lcom/osp/app/signin/AccountView;

    invoke-static {v0}, Lcom/osp/app/signin/AccountView;->v(Lcom/osp/app/signin/AccountView;)I

    move-result v0

    const/16 v1, 0x9

    if-ne v0, v1, :cond_0

    .line 10095
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    .line 10096
    const-string v1, "error_message"

    const-string v2, "Internal network error"

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 10097
    iget-object v1, p0, Lcom/osp/app/signin/bc;->c:Lcom/osp/app/signin/AccountView;

    invoke-virtual {v1, v3, v0}, Lcom/osp/app/signin/AccountView;->a(ILandroid/content/Intent;)V

    .line 10098
    iget-object v0, p0, Lcom/osp/app/signin/bc;->c:Lcom/osp/app/signin/AccountView;

    invoke-virtual {v0}, Lcom/osp/app/signin/AccountView;->finish()V

    goto :goto_0
.end method

.method public final declared-synchronized b(Lcom/msc/b/c;)V
    .locals 4

    .prologue
    .line 10188
    monitor-enter p0

    if-nez p1, :cond_1

    .line 10203
    :cond_0
    :goto_0
    monitor-exit p0

    return-void

    .line 10193
    :cond_1
    :try_start_0
    invoke-virtual {p1}, Lcom/msc/b/c;->a()J

    move-result-wide v0

    .line 10194
    iget-wide v2, p0, Lcom/osp/app/signin/bc;->d:J

    cmp-long v2, v0, v2

    if-nez v2, :cond_2

    .line 10196
    invoke-static {}, Lcom/osp/device/b;->b()Lcom/osp/device/b;

    move-result-object v0

    iget-object v1, p0, Lcom/osp/app/signin/bc;->c:Lcom/osp/app/signin/AccountView;

    const-string v2, "false"

    invoke-virtual {v0, v1, v2}, Lcom/osp/device/b;->a(Landroid/content/Context;Ljava/lang/String;)V

    .line 10197
    invoke-static {}, Lcom/osp/device/b;->b()Lcom/osp/device/b;

    iget-object v0, p0, Lcom/osp/app/signin/bc;->c:Lcom/osp/app/signin/AccountView;

    const-string v1, "false"

    invoke-static {v0, v1}, Lcom/osp/device/b;->b(Landroid/content/Context;Ljava/lang/String;)V

    .line 10198
    const-string v0, "Fail"

    iput-object v0, p0, Lcom/osp/app/signin/bc;->h:Ljava/lang/String;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 10188
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 10199
    :cond_2
    :try_start_1
    iget-wide v2, p0, Lcom/osp/app/signin/bc;->e:J

    cmp-long v0, v0, v2

    if-nez v0, :cond_0

    .line 10201
    const-string v0, "Fail"

    iput-object v0, p0, Lcom/osp/app/signin/bc;->h:Ljava/lang/String;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0
.end method

.method protected final d()V
    .locals 2

    .prologue
    .line 10106
    iget-object v0, p0, Lcom/osp/app/signin/bc;->c:Lcom/osp/app/signin/AccountView;

    invoke-static {v0}, Lcom/osp/app/signin/AccountView;->v(Lcom/osp/app/signin/AccountView;)I

    move-result v0

    const/16 v1, 0x9

    if-ne v0, v1, :cond_0

    .line 10108
    iget-object v0, p0, Lcom/osp/app/signin/bc;->c:Lcom/osp/app/signin/AccountView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/osp/app/signin/AccountView;->b(I)V

    .line 10109
    iget-object v0, p0, Lcom/osp/app/signin/bc;->c:Lcom/osp/app/signin/AccountView;

    invoke-virtual {v0}, Lcom/osp/app/signin/AccountView;->finish()V

    .line 10111
    :cond_0
    invoke-super {p0}, Lcom/msc/c/b;->d()V

    .line 10113
    return-void
.end method

.method protected final synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 10031
    invoke-virtual {p0}, Lcom/osp/app/signin/bc;->a()Ljava/lang/Boolean;

    move-result-object v0

    return-object v0
.end method

.method protected final synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 10031
    check-cast p1, Ljava/lang/Boolean;

    invoke-virtual {p0, p1}, Lcom/osp/app/signin/bc;->a(Ljava/lang/Boolean;)V

    return-void
.end method
