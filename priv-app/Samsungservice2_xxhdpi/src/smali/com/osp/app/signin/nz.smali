.class final Lcom/osp/app/signin/nz;
.super Ljava/lang/Object;
.source "TnCView.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field final synthetic a:Lcom/osp/app/signin/TnCView;


# direct methods
.method constructor <init>(Lcom/osp/app/signin/TnCView;)V
    .locals 0

    .prologue
    .line 2032
    iput-object p1, p0, Lcom/osp/app/signin/nz;->a:Lcom/osp/app/signin/TnCView;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onClick(Landroid/view/View;)V
    .locals 5

    .prologue
    .line 2036
    iget-object v0, p0, Lcom/osp/app/signin/nz;->a:Lcom/osp/app/signin/TnCView;

    invoke-virtual {v0}, Lcom/osp/app/signin/TnCView;->getIntent()Landroid/content/Intent;

    move-result-object v0

    .line 2037
    if-eqz v0, :cond_1

    const-string v1, "is_cancelable_just_one_activity"

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 2039
    iget-object v0, p0, Lcom/osp/app/signin/nz;->a:Lcom/osp/app/signin/TnCView;

    invoke-static {v0}, Lcom/osp/app/signin/TnCView;->c(Lcom/osp/app/signin/TnCView;)Landroid/content/Intent;

    move-result-object v0

    const-string v1, "key_is_name_verified"

    iget-object v2, p0, Lcom/osp/app/signin/nz;->a:Lcom/osp/app/signin/TnCView;

    invoke-static {v2}, Lcom/osp/app/signin/TnCView;->d(Lcom/osp/app/signin/TnCView;)Z

    move-result v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 2040
    iget-object v0, p0, Lcom/osp/app/signin/nz;->a:Lcom/osp/app/signin/TnCView;

    invoke-static {v0}, Lcom/osp/app/signin/TnCView;->d(Lcom/osp/app/signin/TnCView;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/osp/app/signin/nz;->a:Lcom/osp/app/signin/TnCView;

    invoke-static {v0}, Lcom/osp/app/signin/TnCView;->e(Lcom/osp/app/signin/TnCView;)Landroid/os/Bundle;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 2042
    iget-object v0, p0, Lcom/osp/app/signin/nz;->a:Lcom/osp/app/signin/TnCView;

    invoke-static {v0}, Lcom/osp/app/signin/TnCView;->c(Lcom/osp/app/signin/TnCView;)Landroid/content/Intent;

    move-result-object v0

    const-string v1, "information_after_name_validation"

    iget-object v2, p0, Lcom/osp/app/signin/nz;->a:Lcom/osp/app/signin/TnCView;

    invoke-static {v2}, Lcom/osp/app/signin/TnCView;->e(Lcom/osp/app/signin/TnCView;)Landroid/os/Bundle;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Bundle;)Landroid/content/Intent;

    .line 2044
    :cond_0
    iget-object v0, p0, Lcom/osp/app/signin/nz;->a:Lcom/osp/app/signin/TnCView;

    const/16 v1, 0xe

    iget-object v2, p0, Lcom/osp/app/signin/nz;->a:Lcom/osp/app/signin/TnCView;

    invoke-static {v2}, Lcom/osp/app/signin/TnCView;->c(Lcom/osp/app/signin/TnCView;)Landroid/content/Intent;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/osp/app/signin/TnCView;->a(ILandroid/content/Intent;)V

    .line 2046
    :cond_1
    invoke-static {}, Lcom/osp/device/b;->b()Lcom/osp/device/b;

    move-result-object v0

    iget-object v1, p0, Lcom/osp/app/signin/nz;->a:Lcom/osp/app/signin/TnCView;

    invoke-virtual {v0, v1}, Lcom/osp/device/b;->i(Landroid/content/Context;)Z

    move-result v0

    if-nez v0, :cond_2

    .line 2052
    new-instance v0, Lcom/osp/app/bigdatalog/c;

    iget-object v1, p0, Lcom/osp/app/signin/nz;->a:Lcom/osp/app/signin/TnCView;

    invoke-virtual {v1}, Lcom/osp/app/signin/TnCView;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    const/4 v2, 0x2

    const/4 v3, 0x3

    iget-object v4, p0, Lcom/osp/app/signin/nz;->a:Lcom/osp/app/signin/TnCView;

    invoke-static {v4}, Lcom/osp/app/signin/TnCView;->c(Lcom/osp/app/signin/TnCView;)Landroid/content/Intent;

    move-result-object v4

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/osp/app/bigdatalog/c;-><init>(Landroid/content/Context;IILandroid/content/Intent;)V

    .line 2054
    invoke-static {}, Lcom/osp/app/bigdatalog/e;->a()Lcom/osp/app/bigdatalog/e;

    move-result-object v1

    iget-object v2, p0, Lcom/osp/app/signin/nz;->a:Lcom/osp/app/signin/TnCView;

    invoke-virtual {v2}, Lcom/osp/app/signin/TnCView;->getApplicationContext()Landroid/content/Context;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/osp/app/bigdatalog/e;->a(Landroid/content/Context;)V

    .line 2055
    invoke-static {}, Lcom/osp/app/bigdatalog/e;->a()Lcom/osp/app/bigdatalog/e;

    move-result-object v1

    iget-object v2, p0, Lcom/osp/app/signin/nz;->a:Lcom/osp/app/signin/TnCView;

    invoke-virtual {v2}, Lcom/osp/app/signin/TnCView;->getApplicationContext()Landroid/content/Context;

    move-result-object v2

    invoke-virtual {v1, v2, v0}, Lcom/osp/app/bigdatalog/e;->a(Landroid/content/Context;Lcom/osp/app/bigdatalog/c;)V

    .line 2058
    :cond_2
    iget-object v0, p0, Lcom/osp/app/signin/nz;->a:Lcom/osp/app/signin/TnCView;

    invoke-virtual {v0}, Lcom/osp/app/signin/TnCView;->finish()V

    .line 2059
    return-void
.end method
