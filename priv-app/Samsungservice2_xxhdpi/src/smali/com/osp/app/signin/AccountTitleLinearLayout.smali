.class public Lcom/osp/app/signin/AccountTitleLinearLayout;
.super Landroid/widget/LinearLayout;
.source "AccountTitleLinearLayout.java"


# instance fields
.field private a:Landroid/content/Context;

.field private b:Landroid/widget/LinearLayout;

.field private c:Landroid/widget/TextView;

.field private d:Z


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 31
    invoke-direct {p0, p1}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;)V

    .line 28
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/osp/app/signin/AccountTitleLinearLayout;->d:Z

    .line 32
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .prologue
    .line 35
    invoke-direct {p0, p1, p2}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 28
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/osp/app/signin/AccountTitleLinearLayout;->d:Z

    .line 36
    return-void
.end method

.method private a(Landroid/content/res/Configuration;)Z
    .locals 7

    .prologue
    const/4 v4, 0x1

    const/4 v2, -0x1

    const/4 v3, 0x0

    .line 206
    iget-object v0, p0, Lcom/osp/app/signin/AccountTitleLinearLayout;->a:Landroid/content/Context;

    instance-of v0, v0, Lcom/osp/app/signin/AccountView;

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/osp/app/signin/AccountTitleLinearLayout;->a:Landroid/content/Context;

    invoke-static {v0}, Lcom/osp/app/util/r;->g(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 208
    invoke-virtual {p0}, Lcom/osp/app/signin/AccountTitleLinearLayout;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    .line 210
    const v0, 0x7f080117

    invoke-virtual {v5, v0}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    .line 211
    iget v1, p1, Landroid/content/res/Configuration;->orientation:I

    const/4 v6, 0x2

    if-ne v1, v6, :cond_4

    .line 213
    const v0, 0x7f080118

    invoke-virtual {v5, v0}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    move v1, v0

    .line 216
    :goto_0
    iget-object v0, p0, Lcom/osp/app/signin/AccountTitleLinearLayout;->b:Landroid/widget/LinearLayout;

    invoke-virtual {v0}, Landroid/widget/LinearLayout;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout$LayoutParams;

    .line 218
    iget-object v6, p0, Lcom/osp/app/signin/AccountTitleLinearLayout;->a:Landroid/content/Context;

    instance-of v6, v6, Lcom/osp/app/signin/SignUpCompleteView;

    if-eqz v6, :cond_1

    .line 220
    const v1, 0x7f080119

    invoke-virtual {v5, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    iput v1, v0, Landroid/widget/LinearLayout$LayoutParams;->height:I

    .line 221
    const v1, 0x7f08011a

    invoke-virtual {v5, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    .line 229
    :goto_1
    iget-object v3, p0, Lcom/osp/app/signin/AccountTitleLinearLayout;->b:Landroid/widget/LinearLayout;

    const/16 v5, 0x10

    invoke-virtual {v3, v5}, Landroid/widget/LinearLayout;->setGravity(I)V

    .line 230
    invoke-virtual {p0}, Lcom/osp/app/signin/AccountTitleLinearLayout;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v5, 0x7f080116

    invoke-virtual {v3, v5}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v3

    float-to-int v3, v3

    .line 232
    invoke-static {}, Lcom/osp/device/b;->b()Lcom/osp/device/b;

    move-result-object v5

    iget-object v6, p0, Lcom/osp/app/signin/AccountTitleLinearLayout;->a:Landroid/content/Context;

    invoke-virtual {v5, v6}, Lcom/osp/device/b;->i(Landroid/content/Context;)Z

    move-result v5

    if-eqz v5, :cond_0

    .line 234
    iget v5, p1, Landroid/content/res/Configuration;->orientation:I

    if-ne v5, v4, :cond_2

    .line 236
    invoke-virtual {p0}, Lcom/osp/app/signin/AccountTitleLinearLayout;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    const v6, 0x7f08011e

    invoke-virtual {v5, v6}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v5

    float-to-int v5, v5

    add-int/2addr v3, v5

    .line 243
    :cond_0
    :goto_2
    iget-object v5, p0, Lcom/osp/app/signin/AccountTitleLinearLayout;->b:Landroid/widget/LinearLayout;

    neg-int v6, v3

    invoke-static {v5, v3, v1, v6, v2}, Lcom/msc/sa/c/d;->a(Landroid/view/View;IIII)V

    .line 245
    iget-object v1, p0, Lcom/osp/app/signin/AccountTitleLinearLayout;->b:Landroid/widget/LinearLayout;

    const v2, 0x7f07003b

    invoke-virtual {v1, v2}, Landroid/widget/LinearLayout;->setBackgroundResource(I)V

    .line 246
    iget-object v1, p0, Lcom/osp/app/signin/AccountTitleLinearLayout;->b:Landroid/widget/LinearLayout;

    invoke-virtual {v1, v0}, Landroid/widget/LinearLayout;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    move v0, v4

    .line 250
    :goto_3
    return v0

    .line 224
    :cond_1
    iput v1, v0, Landroid/widget/LinearLayout$LayoutParams;->height:I

    .line 225
    iput v3, v0, Landroid/widget/LinearLayout$LayoutParams;->topMargin:I

    .line 226
    iput v3, v0, Landroid/widget/LinearLayout$LayoutParams;->bottomMargin:I

    move v1, v2

    goto :goto_1

    .line 239
    :cond_2
    invoke-virtual {p0}, Lcom/osp/app/signin/AccountTitleLinearLayout;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    const v6, 0x7f08011f

    invoke-virtual {v5, v6}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v5

    float-to-int v5, v5

    add-int/2addr v3, v5

    goto :goto_2

    :cond_3
    move v0, v3

    .line 250
    goto :goto_3

    :cond_4
    move v1, v0

    goto :goto_0
.end method

.method private b()V
    .locals 5

    .prologue
    const v4, 0x7f080004

    const v3, 0x7f080003

    .line 90
    iget-object v0, p0, Lcom/osp/app/signin/AccountTitleLinearLayout;->a:Landroid/content/Context;

    invoke-static {v0}, Lcom/osp/app/util/r;->g(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 92
    const/4 v0, 0x0

    .line 94
    invoke-static {}, Lcom/osp/device/b;->b()Lcom/osp/device/b;

    move-result-object v1

    iget-object v2, p0, Lcom/osp/app/signin/AccountTitleLinearLayout;->a:Landroid/content/Context;

    invoke-virtual {v1, v2}, Lcom/osp/device/b;->i(Landroid/content/Context;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 96
    invoke-virtual {p0}, Lcom/osp/app/signin/AccountTitleLinearLayout;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1, v3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    .line 103
    :goto_0
    iget-object v2, p0, Lcom/osp/app/signin/AccountTitleLinearLayout;->b:Landroid/widget/LinearLayout;

    const/16 v3, 0x10

    invoke-virtual {v2, v3}, Landroid/widget/LinearLayout;->setGravity(I)V

    .line 104
    invoke-virtual {p0}, Lcom/osp/app/signin/AccountTitleLinearLayout;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f080116

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v2

    float-to-int v2, v2

    .line 105
    invoke-static {}, Lcom/osp/device/b;->b()Lcom/osp/device/b;

    move-result-object v3

    iget-object v4, p0, Lcom/osp/app/signin/AccountTitleLinearLayout;->a:Landroid/content/Context;

    invoke-virtual {v3, v4}, Lcom/osp/device/b;->i(Landroid/content/Context;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 107
    invoke-virtual {p0}, Lcom/osp/app/signin/AccountTitleLinearLayout;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    invoke-virtual {v3}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v3

    iget v3, v3, Landroid/content/res/Configuration;->orientation:I

    const/4 v4, 0x1

    if-ne v3, v4, :cond_3

    .line 109
    invoke-virtual {p0}, Lcom/osp/app/signin/AccountTitleLinearLayout;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f08011e

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v3

    float-to-int v3, v3

    add-int/2addr v2, v3

    .line 115
    :cond_0
    :goto_1
    iget-object v3, p0, Lcom/osp/app/signin/AccountTitleLinearLayout;->b:Landroid/widget/LinearLayout;

    invoke-static {v3, v2, v1, v2, v0}, Lcom/msc/sa/c/d;->a(Landroid/view/View;IIII)V

    .line 116
    iget-object v0, p0, Lcom/osp/app/signin/AccountTitleLinearLayout;->b:Landroid/widget/LinearLayout;

    const v1, 0x7f07003b

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setBackgroundResource(I)V

    .line 138
    :cond_1
    :goto_2
    return-void

    .line 99
    :cond_2
    invoke-virtual {p0}, Lcom/osp/app/signin/AccountTitleLinearLayout;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0, v3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    iget-object v0, p0, Lcom/osp/app/signin/AccountTitleLinearLayout;->a:Landroid/content/Context;

    check-cast v0, Lcom/osp/app/util/BaseActivity;

    invoke-virtual {v0}, Lcom/osp/app/util/BaseActivity;->o()I

    move-result v0

    add-int/2addr v1, v0

    .line 100
    invoke-virtual {p0}, Lcom/osp/app/signin/AccountTitleLinearLayout;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0, v4}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    goto :goto_0

    .line 112
    :cond_3
    invoke-virtual {p0}, Lcom/osp/app/signin/AccountTitleLinearLayout;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f08011f

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v3

    float-to-int v3, v3

    add-int/2addr v2, v3

    goto :goto_1

    .line 117
    :cond_4
    iget-object v0, p0, Lcom/osp/app/signin/AccountTitleLinearLayout;->a:Landroid/content/Context;

    invoke-static {v0}, Lcom/osp/app/util/r;->m(Landroid/content/Context;)Z

    move-result v0

    if-nez v0, :cond_5

    iget-object v0, p0, Lcom/osp/app/signin/AccountTitleLinearLayout;->a:Landroid/content/Context;

    invoke-static {v0}, Lcom/osp/app/util/r;->k(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 119
    :cond_5
    invoke-virtual {p0}, Lcom/osp/app/signin/AccountTitleLinearLayout;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    .line 120
    iget-object v0, p0, Lcom/osp/app/signin/AccountTitleLinearLayout;->b:Landroid/widget/LinearLayout;

    const v2, 0x106000d

    invoke-virtual {v0, v2}, Landroid/widget/LinearLayout;->setBackgroundResource(I)V

    .line 122
    invoke-static {}, Lcom/osp/device/b;->b()Lcom/osp/device/b;

    move-result-object v0

    iget-object v2, p0, Lcom/osp/app/signin/AccountTitleLinearLayout;->a:Landroid/content/Context;

    invoke-virtual {v0, v2}, Lcom/osp/device/b;->i(Landroid/content/Context;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 124
    iget-object v0, p0, Lcom/osp/app/signin/AccountTitleLinearLayout;->b:Landroid/widget/LinearLayout;

    invoke-virtual {v0}, Landroid/widget/LinearLayout;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout$LayoutParams;

    .line 125
    invoke-virtual {v1}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v1

    iget v1, v1, Landroid/content/res/Configuration;->orientation:I

    const/4 v2, 0x2

    if-ne v1, v2, :cond_6

    .line 127
    invoke-virtual {p0}, Lcom/osp/app/signin/AccountTitleLinearLayout;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f08001a

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    iput v1, v0, Landroid/widget/LinearLayout$LayoutParams;->topMargin:I

    .line 128
    invoke-virtual {p0}, Lcom/osp/app/signin/AccountTitleLinearLayout;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1, v4}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    iput v1, v0, Landroid/widget/LinearLayout$LayoutParams;->bottomMargin:I

    .line 135
    :goto_3
    iget-object v1, p0, Lcom/osp/app/signin/AccountTitleLinearLayout;->b:Landroid/widget/LinearLayout;

    invoke-virtual {v1, v0}, Landroid/widget/LinearLayout;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    goto :goto_2

    .line 131
    :cond_6
    invoke-virtual {p0}, Lcom/osp/app/signin/AccountTitleLinearLayout;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1, v3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    iput v1, v0, Landroid/widget/LinearLayout$LayoutParams;->topMargin:I

    .line 132
    invoke-virtual {p0}, Lcom/osp/app/signin/AccountTitleLinearLayout;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1, v4}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    iput v1, v0, Landroid/widget/LinearLayout$LayoutParams;->bottomMargin:I

    goto :goto_3
.end method


# virtual methods
.method public final a()Landroid/widget/TextView;
    .locals 1

    .prologue
    .line 254
    iget-object v0, p0, Lcom/osp/app/signin/AccountTitleLinearLayout;->c:Landroid/widget/TextView;

    return-object v0
.end method

.method public final a(Landroid/content/Context;Z)V
    .locals 9

    .prologue
    const/4 v2, 0x0

    const/16 v8, 0xbf

    const/high16 v7, 0x3f800000    # 1.0f

    const/4 v6, 0x0

    .line 157
    iput-boolean p2, p0, Lcom/osp/app/signin/AccountTitleLinearLayout;->d:Z

    .line 159
    iput-object p1, p0, Lcom/osp/app/signin/AccountTitleLinearLayout;->a:Landroid/content/Context;

    invoke-virtual {p0}, Lcom/osp/app/signin/AccountTitleLinearLayout;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    iget-object v0, p0, Lcom/osp/app/signin/AccountTitleLinearLayout;->a:Landroid/content/Context;

    instance-of v0, v0, Lcom/osp/app/util/BaseActivity;

    if-eqz v0, :cond_7

    iget-object v0, p0, Lcom/osp/app/signin/AccountTitleLinearLayout;->a:Landroid/content/Context;

    check-cast v0, Lcom/osp/app/util/BaseActivity;

    invoke-static {v0}, Lcom/osp/app/util/an;->a(Lcom/osp/app/util/BaseActivity;)Lcom/osp/app/util/an;

    move-result-object v0

    move-object v1, v0

    :goto_0
    const-string v0, "layout_inflater"

    invoke-virtual {p1, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/LayoutInflater;

    const v4, 0x7f030007

    invoke-virtual {v0, v4, p0}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v4

    const v0, 0x7f0c003c

    invoke-virtual {v4, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcom/osp/app/signin/AccountTitleLinearLayout;->b:Landroid/widget/LinearLayout;

    const v0, 0x7f0c003d

    invoke-virtual {v4, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/osp/app/signin/AccountTitleLinearLayout;->c:Landroid/widget/TextView;

    invoke-virtual {v3}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/osp/app/signin/AccountTitleLinearLayout;->a(Landroid/content/res/Configuration;)Z

    move-result v0

    if-nez v0, :cond_1

    iget-boolean v0, p0, Lcom/osp/app/signin/AccountTitleLinearLayout;->d:Z

    if-eqz v0, :cond_5

    iget-object v0, p0, Lcom/osp/app/signin/AccountTitleLinearLayout;->b:Landroid/widget/LinearLayout;

    const v4, 0x7f0200e4

    invoke-virtual {v0, v4}, Landroid/widget/LinearLayout;->setBackgroundResource(I)V

    :cond_0
    :goto_1
    invoke-direct {p0}, Lcom/osp/app/signin/AccountTitleLinearLayout;->b()V

    :cond_1
    if-eqz v1, :cond_2

    const v0, 0x7f0800f5

    invoke-static {v0}, Lcom/osp/app/util/an;->b(I)I

    move-result v0

    iget-object v1, p0, Lcom/osp/app/signin/AccountTitleLinearLayout;->c:Landroid/widget/TextView;

    invoke-virtual {v3, v0}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v0

    invoke-virtual {v1, v6, v0}, Landroid/widget/TextView;->setTextSize(IF)V

    iget-object v0, p0, Lcom/osp/app/signin/AccountTitleLinearLayout;->c:Landroid/widget/TextView;

    invoke-virtual {v0, v6}, Landroid/widget/TextView;->setIncludeFontPadding(Z)V

    iget-object v0, p0, Lcom/osp/app/signin/AccountTitleLinearLayout;->c:Landroid/widget/TextView;

    const v1, 0x7f07001a

    invoke-static {v1}, Lcom/osp/app/util/an;->a(I)I

    move-result v1

    invoke-virtual {v3, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setTextColor(I)V

    iget-object v0, p0, Lcom/osp/app/signin/AccountTitleLinearLayout;->c:Landroid/widget/TextView;

    invoke-static {}, Lcom/osp/device/b;->b()Lcom/osp/device/b;

    move-result-object v1

    iget-object v3, p0, Lcom/osp/app/signin/AccountTitleLinearLayout;->a:Landroid/content/Context;

    invoke-virtual {v1, v3}, Lcom/osp/device/b;->i(Landroid/content/Context;)Z

    move-result v1

    if-nez v1, :cond_2

    iget-object v1, p0, Lcom/osp/app/signin/AccountTitleLinearLayout;->a:Landroid/content/Context;

    invoke-static {v1}, Lcom/osp/app/util/r;->g(Landroid/content/Context;)Z

    move-result v1

    if-nez v1, :cond_2

    iget-object v1, p0, Lcom/osp/app/signin/AccountTitleLinearLayout;->a:Landroid/content/Context;

    invoke-static {v1}, Lcom/osp/app/util/x;->m(Landroid/content/Context;)Z

    move-result v1

    if-eqz v1, :cond_6

    invoke-static {v8, v6, v6, v6}, Landroid/graphics/Color;->argb(IIII)I

    move-result v1

    invoke-virtual {v0, v7, v7, v7, v1}, Landroid/widget/TextView;->setShadowLayer(FFFI)V

    :cond_2
    :goto_2
    iget-object v0, p0, Lcom/osp/app/signin/AccountTitleLinearLayout;->a:Landroid/content/Context;

    invoke-static {v0}, Lcom/osp/app/util/x;->l(Landroid/content/Context;)Z

    move-result v0

    if-nez v0, :cond_3

    iget-object v0, p0, Lcom/osp/app/signin/AccountTitleLinearLayout;->a:Landroid/content/Context;

    invoke-static {v0}, Lcom/osp/app/util/x;->a(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_4

    :cond_3
    iget-object v0, p0, Lcom/osp/app/signin/AccountTitleLinearLayout;->c:Landroid/widget/TextView;

    invoke-virtual {v0, v2, v6}, Landroid/widget/TextView;->setTypeface(Landroid/graphics/Typeface;I)V

    .line 160
    :cond_4
    return-void

    .line 159
    :cond_5
    iget-boolean v0, p0, Lcom/osp/app/signin/AccountTitleLinearLayout;->d:Z

    if-nez v0, :cond_0

    invoke-static {}, Lcom/osp/device/b;->b()Lcom/osp/device/b;

    move-result-object v0

    iget-object v4, p0, Lcom/osp/app/signin/AccountTitleLinearLayout;->a:Landroid/content/Context;

    invoke-virtual {v0, v4}, Lcom/osp/device/b;->i(Landroid/content/Context;)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/osp/app/signin/AccountTitleLinearLayout;->a:Landroid/content/Context;

    invoke-static {v0}, Lcom/osp/app/util/r;->m(Landroid/content/Context;)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/osp/app/signin/AccountTitleLinearLayout;->b:Landroid/widget/LinearLayout;

    invoke-virtual {v0}, Landroid/widget/LinearLayout;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout$LayoutParams;

    invoke-virtual {p0}, Lcom/osp/app/signin/AccountTitleLinearLayout;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const v5, 0x7f0800f9

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v4

    iput v4, v0, Landroid/widget/LinearLayout$LayoutParams;->height:I

    iget-object v4, p0, Lcom/osp/app/signin/AccountTitleLinearLayout;->b:Landroid/widget/LinearLayout;

    invoke-virtual {v4, v0}, Landroid/widget/LinearLayout;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    goto/16 :goto_1

    :cond_6
    const/4 v1, 0x0

    const/high16 v3, 0x40000000    # 2.0f

    invoke-static {v8, v6, v6, v6}, Landroid/graphics/Color;->argb(IIII)I

    move-result v4

    invoke-virtual {v0, v7, v1, v3, v4}, Landroid/widget/TextView;->setShadowLayer(FFFI)V

    goto :goto_2

    :cond_7
    move-object v1, v2

    goto/16 :goto_0
.end method

.method public final a(Ljava/lang/Boolean;)V
    .locals 2

    .prologue
    .line 166
    invoke-virtual {p1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 168
    iget-object v0, p0, Lcom/osp/app/signin/AccountTitleLinearLayout;->b:Landroid/widget/LinearLayout;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 173
    :goto_0
    return-void

    .line 171
    :cond_0
    iget-object v0, p0, Lcom/osp/app/signin/AccountTitleLinearLayout;->b:Landroid/widget/LinearLayout;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setVisibility(I)V

    goto :goto_0
.end method

.method public final a(Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 196
    iget-object v0, p0, Lcom/osp/app/signin/AccountTitleLinearLayout;->c:Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 197
    return-void
.end method

.method protected onConfigurationChanged(Landroid/content/res/Configuration;)V
    .locals 1

    .prologue
    .line 142
    iget-object v0, p0, Lcom/osp/app/signin/AccountTitleLinearLayout;->b:Landroid/widget/LinearLayout;

    if-eqz v0, :cond_0

    .line 144
    invoke-direct {p0, p1}, Lcom/osp/app/signin/AccountTitleLinearLayout;->a(Landroid/content/res/Configuration;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 146
    invoke-direct {p0}, Lcom/osp/app/signin/AccountTitleLinearLayout;->b()V

    .line 149
    :cond_0
    invoke-super {p0, p1}, Landroid/widget/LinearLayout;->onConfigurationChanged(Landroid/content/res/Configuration;)V

    .line 150
    return-void
.end method
