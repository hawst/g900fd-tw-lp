.class public Lcom/osp/app/signin/CheckNameBirthdateActivity;
.super Lcom/osp/app/util/BaseActivity;
.source "CheckNameBirthdateActivity.java"


# instance fields
.field private A:I

.field private B:I

.field private C:I

.field private D:I

.field private E:Ljava/lang/String;

.field private F:Ljava/lang/String;

.field private G:Ljava/lang/String;

.field private H:Ljava/lang/String;

.field private I:Z

.field private J:Lcom/osp/app/signin/dj;

.field private K:Ljava/lang/String;

.field private L:Z

.field private M:Z

.field private N:Lcom/osp/app/signin/BottomSoftkeyLinearLayout;

.field private final O:Landroid/app/DatePickerDialog$OnDateSetListener;

.field final a:Landroid/view/View$OnClickListener;

.field final b:Landroid/view/View$OnClickListener;

.field final c:Landroid/content/DialogInterface$OnCancelListener;

.field d:Landroid/text/TextWatcher;

.field private final e:Ljava/lang/String;

.field private f:Landroid/app/Dialog;

.field private y:I

.field private z:I


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 81
    invoke-direct {p0}, Lcom/osp/app/util/BaseActivity;-><init>()V

    .line 83
    const-string v0, "CNB"

    iput-object v0, p0, Lcom/osp/app/signin/CheckNameBirthdateActivity;->e:Ljava/lang/String;

    .line 166
    iput-boolean v1, p0, Lcom/osp/app/signin/CheckNameBirthdateActivity;->I:Z

    .line 181
    iput-boolean v1, p0, Lcom/osp/app/signin/CheckNameBirthdateActivity;->L:Z

    .line 186
    iput-boolean v1, p0, Lcom/osp/app/signin/CheckNameBirthdateActivity;->M:Z

    .line 189
    new-instance v0, Lcom/osp/app/signin/cy;

    invoke-direct {v0, p0}, Lcom/osp/app/signin/cy;-><init>(Lcom/osp/app/signin/CheckNameBirthdateActivity;)V

    iput-object v0, p0, Lcom/osp/app/signin/CheckNameBirthdateActivity;->a:Landroid/view/View$OnClickListener;

    .line 196
    new-instance v0, Lcom/osp/app/signin/db;

    invoke-direct {v0, p0}, Lcom/osp/app/signin/db;-><init>(Lcom/osp/app/signin/CheckNameBirthdateActivity;)V

    iput-object v0, p0, Lcom/osp/app/signin/CheckNameBirthdateActivity;->b:Landroid/view/View$OnClickListener;

    .line 204
    new-instance v0, Lcom/osp/app/signin/dc;

    invoke-direct {v0, p0}, Lcom/osp/app/signin/dc;-><init>(Lcom/osp/app/signin/CheckNameBirthdateActivity;)V

    iput-object v0, p0, Lcom/osp/app/signin/CheckNameBirthdateActivity;->c:Landroid/content/DialogInterface$OnCancelListener;

    .line 217
    new-instance v0, Lcom/osp/app/signin/dd;

    invoke-direct {v0, p0}, Lcom/osp/app/signin/dd;-><init>(Lcom/osp/app/signin/CheckNameBirthdateActivity;)V

    iput-object v0, p0, Lcom/osp/app/signin/CheckNameBirthdateActivity;->O:Landroid/app/DatePickerDialog$OnDateSetListener;

    .line 231
    new-instance v0, Lcom/osp/app/signin/de;

    invoke-direct {v0, p0}, Lcom/osp/app/signin/de;-><init>(Lcom/osp/app/signin/CheckNameBirthdateActivity;)V

    iput-object v0, p0, Lcom/osp/app/signin/CheckNameBirthdateActivity;->d:Landroid/text/TextWatcher;

    .line 945
    return-void
.end method

.method static synthetic a(Lcom/osp/app/signin/CheckNameBirthdateActivity;I)I
    .locals 0

    .prologue
    .line 81
    iput p1, p0, Lcom/osp/app/signin/CheckNameBirthdateActivity;->z:I

    return p1
.end method

.method static synthetic a(Lcom/osp/app/signin/CheckNameBirthdateActivity;Ljava/lang/String;)Ljava/lang/String;
    .locals 0

    .prologue
    .line 81
    iput-object p1, p0, Lcom/osp/app/signin/CheckNameBirthdateActivity;->K:Ljava/lang/String;

    return-object p1
.end method

.method private a(I)V
    .locals 6

    .prologue
    const v4, 0x7f08011e

    .line 486
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "CNB::initLayoutParams orientation : "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;)V

    .line 488
    invoke-static {}, Lcom/osp/device/b;->b()Lcom/osp/device/b;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/osp/device/b;->i(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 490
    const v0, 0x7f0c0165

    invoke-virtual {p0, v0}, Lcom/osp/app/signin/CheckNameBirthdateActivity;->findViewById(I)Landroid/view/View;

    move-result-object v2

    .line 494
    invoke-virtual {p0}, Lcom/osp/app/signin/CheckNameBirthdateActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f08011f

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v0

    float-to-int v0, v0

    .line 495
    invoke-virtual {p0}, Lcom/osp/app/signin/CheckNameBirthdateActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1, v4}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v1

    float-to-int v1, v1

    .line 496
    if-eqz v2, :cond_1

    .line 498
    invoke-static {}, Lcom/osp/app/signin/SamsungService;->a()Z

    move-result v3

    if-nez v3, :cond_0

    .line 500
    invoke-virtual {p0}, Lcom/osp/app/signin/CheckNameBirthdateActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0, v4}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v0

    float-to-int v0, v0

    .line 503
    :cond_0
    invoke-static {}, Lcom/osp/app/util/q;->a()Lcom/osp/app/util/q;

    invoke-static {v2, v0, v1, p1}, Lcom/osp/app/util/q;->b(Landroid/view/View;III)V

    .line 509
    :cond_1
    invoke-static {p0}, Lcom/osp/app/util/r;->k(Landroid/content/Context;)Z

    move-result v2

    if-nez v2, :cond_4

    .line 511
    invoke-virtual {p0}, Lcom/osp/app/signin/CheckNameBirthdateActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f08013d

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v0

    float-to-int v1, v0

    .line 512
    invoke-virtual {p0}, Lcom/osp/app/signin/CheckNameBirthdateActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v2, 0x7f08013c

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v0

    float-to-int v0, v0

    .line 515
    :goto_0
    iget-object v2, p0, Lcom/osp/app/signin/CheckNameBirthdateActivity;->N:Lcom/osp/app/signin/BottomSoftkeyLinearLayout;

    if-eqz v2, :cond_3

    .line 517
    invoke-static {}, Lcom/osp/app/signin/SamsungService;->a()Z

    move-result v2

    if-eqz v2, :cond_2

    sget-object v2, Lcom/osp/app/util/u;->o:[Ljava/lang/String;

    invoke-static {v2}, Lcom/osp/app/util/u;->a([Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 519
    const/4 v1, 0x0

    .line 521
    :cond_2
    invoke-static {}, Lcom/osp/app/util/q;->a()Lcom/osp/app/util/q;

    iget-object v2, p0, Lcom/osp/app/signin/CheckNameBirthdateActivity;->N:Lcom/osp/app/signin/BottomSoftkeyLinearLayout;

    invoke-static {p0, v2, v1, v0, p1}, Lcom/osp/app/util/q;->a(Landroid/content/Context;Landroid/view/View;III)V

    .line 524
    :cond_3
    return-void

    :cond_4
    move v5, v1

    move v1, v0

    move v0, v5

    goto :goto_0
.end method

.method static synthetic a(Lcom/osp/app/signin/CheckNameBirthdateActivity;)V
    .locals 0

    .prologue
    .line 81
    invoke-direct {p0}, Lcom/osp/app/signin/CheckNameBirthdateActivity;->g()V

    return-void
.end method

.method static synthetic a(Lcom/osp/app/signin/CheckNameBirthdateActivity;Lcom/msc/a/d;)V
    .locals 6

    .prologue
    .line 81
    invoke-virtual {p1}, Lcom/msc/a/d;->d()Ljava/util/ArrayList;

    move-result-object v2

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v0

    new-array v3, v0, [Ljava/lang/String;

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v4

    const/4 v0, 0x0

    move v1, v0

    :goto_0
    if-ge v1, v4, :cond_0

    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    new-instance v5, Ljava/lang/StringBuilder;

    const-string v0, "MAKE DATA FOR SECURITY INFO CHECK : "

    invoke-direct {v5, v0}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/msc/a/e;

    invoke-virtual {v0}, Lcom/msc/a/e;->a()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v5, "|"

    invoke-virtual {v0, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v2, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/msc/a/e;

    invoke-virtual {v0}, Lcom/msc/a/e;->b()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;)V

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/msc/a/e;

    invoke-virtual {v0}, Lcom/msc/a/e;->a()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v5, "_"

    invoke-virtual {v0, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v2, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/msc/a/e;

    invoke-virtual {v0}, Lcom/msc/a/e;->b()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    aput-object v0, v3, v1

    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    :cond_0
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/osp/app/signin/CheckSecurityInfoActivity;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const-string v1, "client_id"

    iget-object v2, p0, Lcom/osp/app/signin/CheckNameBirthdateActivity;->E:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    const-string v1, "client_secret"

    iget-object v2, p0, Lcom/osp/app/signin/CheckNameBirthdateActivity;->F:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    const-string v1, "key_login_id"

    iget-object v2, p0, Lcom/osp/app/signin/CheckNameBirthdateActivity;->G:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    const-string v1, "key_password"

    iget-object v2, p0, Lcom/osp/app/signin/CheckNameBirthdateActivity;->H:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    const-string v1, "required_auth"

    iget-boolean v2, p0, Lcom/osp/app/signin/CheckNameBirthdateActivity;->M:Z

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    const-string v1, "key_duplicated_user_ids"

    invoke-virtual {v0, v1, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;[Ljava/lang/String;)Landroid/content/Intent;

    const-string v1, "key_internal_is_resign_in"

    iget-boolean v2, p0, Lcom/osp/app/signin/CheckNameBirthdateActivity;->L:Z

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    const-string v1, "is_cancelable_just_one_activity"

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    const/16 v1, 0xe7

    invoke-virtual {p0, v0, v1}, Lcom/osp/app/signin/CheckNameBirthdateActivity;->startActivityForResult(Landroid/content/Intent;I)V

    return-void
.end method

.method static synthetic b(Lcom/osp/app/signin/CheckNameBirthdateActivity;I)I
    .locals 0

    .prologue
    .line 81
    iput p1, p0, Lcom/osp/app/signin/CheckNameBirthdateActivity;->y:I

    return p1
.end method

.method static synthetic b(Lcom/osp/app/signin/CheckNameBirthdateActivity;)V
    .locals 9

    .prologue
    const/4 v8, 0x3

    const/16 v7, 0xa

    const/4 v6, 0x0

    const/4 v5, 0x1

    const/4 v4, 0x2

    .line 81
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v0, "CheckNameBirthdateActivity::updateDisplay"

    invoke-static {v0}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;)V

    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object v2

    iget v0, p0, Lcom/osp/app/signin/CheckNameBirthdateActivity;->z:I

    invoke-static {v0}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v5}, Ljava/util/Calendar;->get(I)I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v1

    iget v3, p0, Lcom/osp/app/signin/CheckNameBirthdateActivity;->y:I

    if-ge v3, v7, :cond_1

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v3, "0"

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v3, p0, Lcom/osp/app/signin/CheckNameBirthdateActivity;->y:I

    invoke-static {v3}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    :goto_0
    iget v3, p0, Lcom/osp/app/signin/CheckNameBirthdateActivity;->A:I

    if-ge v3, v7, :cond_2

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v3, "0"

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v3, p0, Lcom/osp/app/signin/CheckNameBirthdateActivity;->A:I

    invoke-static {v3}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    :goto_1
    invoke-virtual {v2, v4}, Ljava/util/Calendar;->get(I)I

    move-result v3

    if-ge v3, v7, :cond_3

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v3, "0"

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v2, v4}, Ljava/util/Calendar;->get(I)I

    move-result v3

    invoke-static {v3}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    :goto_2
    const/4 v3, 0x5

    invoke-virtual {v2, v3}, Ljava/util/Calendar;->get(I)I

    move-result v3

    if-ge v3, v7, :cond_4

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v3, "0"

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const/4 v3, 0x5

    invoke-virtual {v2, v3}, Ljava/util/Calendar;->get(I)I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    :goto_3
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    if-ge v0, v1, :cond_0

    iget v0, p0, Lcom/osp/app/signin/CheckNameBirthdateActivity;->z:I

    const/16 v1, 0x76d

    if-ge v0, v1, :cond_5

    :cond_0
    const v0, 0x7f090031

    invoke-virtual {p0, v0}, Lcom/osp/app/signin/CheckNameBirthdateActivity;->getText(I)Ljava/lang/CharSequence;

    move-result-object v0

    invoke-interface {v0}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {}, Lcom/osp/app/util/ap;->a()Lcom/osp/app/util/ap;

    invoke-static {p0, v0, v6}, Lcom/osp/app/util/ap;->a(Landroid/content/Context;Ljava/lang/String;I)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    iget v0, p0, Lcom/osp/app/signin/CheckNameBirthdateActivity;->D:I

    iput v0, p0, Lcom/osp/app/signin/CheckNameBirthdateActivity;->z:I

    iget v0, p0, Lcom/osp/app/signin/CheckNameBirthdateActivity;->C:I

    iput v0, p0, Lcom/osp/app/signin/CheckNameBirthdateActivity;->y:I

    iget v0, p0, Lcom/osp/app/signin/CheckNameBirthdateActivity;->B:I

    iput v0, p0, Lcom/osp/app/signin/CheckNameBirthdateActivity;->A:I

    iget-object v0, p0, Lcom/osp/app/signin/CheckNameBirthdateActivity;->f:Landroid/app/Dialog;

    check-cast v0, Landroid/app/DatePickerDialog;

    iget v1, p0, Lcom/osp/app/signin/CheckNameBirthdateActivity;->D:I

    iget v2, p0, Lcom/osp/app/signin/CheckNameBirthdateActivity;->C:I

    iget v3, p0, Lcom/osp/app/signin/CheckNameBirthdateActivity;->B:I

    invoke-virtual {v0, v1, v2, v3}, Landroid/app/DatePickerDialog;->updateDate(III)V

    :goto_4
    return-void

    :cond_1
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v3, p0, Lcom/osp/app/signin/CheckNameBirthdateActivity;->y:I

    invoke-static {v3}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_0

    :cond_2
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v3, p0, Lcom/osp/app/signin/CheckNameBirthdateActivity;->A:I

    invoke-static {v3}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_1

    :cond_3
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v2, v4}, Ljava/util/Calendar;->get(I)I

    move-result v3

    invoke-static {v3}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    goto/16 :goto_2

    :cond_4
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const/4 v3, 0x5

    invoke-virtual {v2, v3}, Ljava/util/Calendar;->get(I)I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    goto/16 :goto_3

    :cond_5
    invoke-virtual {p0}, Lcom/osp/app/signin/CheckNameBirthdateActivity;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const-string v1, "date_format"

    invoke-static {v0, v1}, Landroid/provider/Settings$System;->getString(Landroid/content/ContentResolver;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    const-string v1, "MM-dd-yyyy"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_6

    const-string v0, "%02d/%02d/%04d"

    new-array v1, v8, [Ljava/lang/Object;

    iget v2, p0, Lcom/osp/app/signin/CheckNameBirthdateActivity;->y:I

    add-int/lit8 v2, v2, 0x1

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v1, v6

    iget v2, p0, Lcom/osp/app/signin/CheckNameBirthdateActivity;->A:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v1, v5

    iget v2, p0, Lcom/osp/app/signin/CheckNameBirthdateActivity;->z:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v1, v4

    invoke-static {v0, v1}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    move-object v1, v0

    :goto_5
    const v0, 0x7f0c0017

    invoke-virtual {p0, v0}, Lcom/osp/app/signin/CheckNameBirthdateActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setContentDescription(Ljava/lang/CharSequence;)V

    iput-boolean v5, p0, Lcom/osp/app/signin/CheckNameBirthdateActivity;->I:Z

    invoke-virtual {p0}, Lcom/osp/app/signin/CheckNameBirthdateActivity;->a()V

    goto/16 :goto_4

    :cond_6
    const-string v1, "dd-MM-yyyy"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_7

    const-string v0, "%02d/%02d/%04d"

    new-array v1, v8, [Ljava/lang/Object;

    iget v2, p0, Lcom/osp/app/signin/CheckNameBirthdateActivity;->A:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v1, v6

    iget v2, p0, Lcom/osp/app/signin/CheckNameBirthdateActivity;->y:I

    add-int/lit8 v2, v2, 0x1

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v1, v5

    iget v2, p0, Lcom/osp/app/signin/CheckNameBirthdateActivity;->z:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v1, v4

    invoke-static {v0, v1}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    move-object v1, v0

    goto :goto_5

    :cond_7
    const-string v0, "%04d/%02d/%02d"

    new-array v1, v8, [Ljava/lang/Object;

    iget v2, p0, Lcom/osp/app/signin/CheckNameBirthdateActivity;->z:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v1, v6

    iget v2, p0, Lcom/osp/app/signin/CheckNameBirthdateActivity;->y:I

    add-int/lit8 v2, v2, 0x1

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v1, v5

    iget v2, p0, Lcom/osp/app/signin/CheckNameBirthdateActivity;->A:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v1, v4

    invoke-static {v0, v1}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    move-object v1, v0

    goto :goto_5
.end method

.method static synthetic c(Lcom/osp/app/signin/CheckNameBirthdateActivity;I)I
    .locals 0

    .prologue
    .line 81
    iput p1, p0, Lcom/osp/app/signin/CheckNameBirthdateActivity;->A:I

    return p1
.end method

.method static synthetic c(Lcom/osp/app/signin/CheckNameBirthdateActivity;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 81
    iget-object v0, p0, Lcom/osp/app/signin/CheckNameBirthdateActivity;->G:Ljava/lang/String;

    return-object v0
.end method

.method private d()Ljava/lang/String;
    .locals 1

    .prologue
    .line 362
    const v0, 0x7f0c00a7

    invoke-virtual {p0, v0}, Lcom/osp/app/signin/CheckNameBirthdateActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/EditText;

    .line 363
    invoke-virtual {v0}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method static synthetic d(Lcom/osp/app/signin/CheckNameBirthdateActivity;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 81
    iget-object v0, p0, Lcom/osp/app/signin/CheckNameBirthdateActivity;->H:Ljava/lang/String;

    return-object v0
.end method

.method private e()Ljava/lang/String;
    .locals 1

    .prologue
    .line 372
    const v0, 0x7f0c00aa

    invoke-virtual {p0, v0}, Lcom/osp/app/signin/CheckNameBirthdateActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/EditText;

    .line 373
    invoke-virtual {v0}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method static synthetic e(Lcom/osp/app/signin/CheckNameBirthdateActivity;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 81
    iget-object v0, p0, Lcom/osp/app/signin/CheckNameBirthdateActivity;->E:Ljava/lang/String;

    return-object v0
.end method

.method private f()Ljava/lang/String;
    .locals 5

    .prologue
    .line 382
    const/4 v0, 0x0

    const-string v1, "%04d%02d%02d"

    const/4 v2, 0x3

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    iget v4, p0, Lcom/osp/app/signin/CheckNameBirthdateActivity;->z:I

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v2, v3

    const/4 v3, 0x1

    iget v4, p0, Lcom/osp/app/signin/CheckNameBirthdateActivity;->y:I

    add-int/lit8 v4, v4, 0x1

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v2, v3

    const/4 v3, 0x2

    iget v4, p0, Lcom/osp/app/signin/CheckNameBirthdateActivity;->A:I

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-static {v0, v1, v2}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 384
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v1, "CNB"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "BirthDate : "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 385
    return-object v0
.end method

.method static synthetic f(Lcom/osp/app/signin/CheckNameBirthdateActivity;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 81
    iget-object v0, p0, Lcom/osp/app/signin/CheckNameBirthdateActivity;->F:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic g(Lcom/osp/app/signin/CheckNameBirthdateActivity;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 81
    invoke-direct {p0}, Lcom/osp/app/signin/CheckNameBirthdateActivity;->f()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private g()V
    .locals 1

    .prologue
    .line 834
    new-instance v0, Lcom/osp/app/signin/dj;

    invoke-direct {v0, p0, p0}, Lcom/osp/app/signin/dj;-><init>(Lcom/osp/app/signin/CheckNameBirthdateActivity;Landroid/content/Context;)V

    iput-object v0, p0, Lcom/osp/app/signin/CheckNameBirthdateActivity;->J:Lcom/osp/app/signin/dj;

    .line 835
    iget-object v0, p0, Lcom/osp/app/signin/CheckNameBirthdateActivity;->J:Lcom/osp/app/signin/dj;

    invoke-virtual {v0}, Lcom/osp/app/signin/dj;->b()V

    .line 836
    return-void
.end method

.method static synthetic h(Lcom/osp/app/signin/CheckNameBirthdateActivity;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 81
    invoke-direct {p0}, Lcom/osp/app/signin/CheckNameBirthdateActivity;->d()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method static synthetic i(Lcom/osp/app/signin/CheckNameBirthdateActivity;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 81
    invoke-direct {p0}, Lcom/osp/app/signin/CheckNameBirthdateActivity;->e()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method static synthetic j(Lcom/osp/app/signin/CheckNameBirthdateActivity;)Z
    .locals 1

    .prologue
    .line 81
    iget-boolean v0, p0, Lcom/osp/app/signin/CheckNameBirthdateActivity;->M:Z

    return v0
.end method

.method static synthetic k(Lcom/osp/app/signin/CheckNameBirthdateActivity;)V
    .locals 5

    .prologue
    .line 81
    invoke-static {}, Lcom/osp/device/b;->b()Lcom/osp/device/b;

    move-result-object v0

    invoke-virtual {v0}, Lcom/osp/device/b;->a()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v0, "Login try with blocked id"

    invoke-static {v0}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;)V

    new-instance v2, Landroid/content/Intent;

    const-string v0, "com.msc.action.samsungaccount.web_dialog"

    invoke-direct {v2, v0}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    const v0, 0x7f0c0121

    invoke-virtual {p0, v0}, Lcom/osp/app/signin/CheckNameBirthdateActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/EditText;

    const-string v1, "j5p7ll8g33"

    iget-object v3, p0, Lcom/osp/app/signin/CheckNameBirthdateActivity;->E:Ljava/lang/String;

    invoke-static {v3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_0

    iget-object v1, p0, Lcom/osp/app/signin/CheckNameBirthdateActivity;->E:Ljava/lang/String;

    :cond_0
    invoke-static {p0}, Lcom/osp/app/util/ad;->p(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    sget-object v4, Ljava/util/Locale;->ENGLISH:Ljava/util/Locale;

    invoke-virtual {v0, v4}, Ljava/lang/String;->toLowerCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v0

    invoke-static {p0, v1, v0, v3}, Lcom/msc/c/n;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    const-string v1, "ServerUrl"

    invoke-virtual {v2, v1, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    invoke-virtual {p0, v2}, Lcom/osp/app/signin/CheckNameBirthdateActivity;->startActivity(Landroid/content/Intent;)V

    :cond_1
    return-void
.end method

.method static synthetic l(Lcom/osp/app/signin/CheckNameBirthdateActivity;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 81
    iget-object v0, p0, Lcom/osp/app/signin/CheckNameBirthdateActivity;->K:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic m(Lcom/osp/app/signin/CheckNameBirthdateActivity;)V
    .locals 3

    .prologue
    .line 81
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v0, "CNB"

    const-string v1, "sendSignInCompleteBroadcast"

    invoke-static {v0, v1}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;)V

    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    iget-boolean v1, p0, Lcom/osp/app/signin/CheckNameBirthdateActivity;->L:Z

    if-eqz v1, :cond_2

    const-string v1, "android.intent.action.SAMSUNGACCOUNT_RESIGNIN_COMPLETED"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    :goto_0
    invoke-static {}, Lcom/osp/device/b;->b()Lcom/osp/device/b;

    move-result-object v1

    invoke-virtual {v1}, Lcom/osp/device/b;->l()I

    move-result v1

    const/16 v2, 0xb

    if-le v1, v2, :cond_0

    const/16 v1, 0x20

    invoke-virtual {v0, v1}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    :cond_0
    invoke-virtual {p0, v0}, Lcom/osp/app/signin/CheckNameBirthdateActivity;->sendBroadcast(Landroid/content/Intent;)V

    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v0, "CNB"

    const-string v1, "send signInCompleteIntentV02"

    invoke-static {v0, v1}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;)V

    new-instance v0, Landroid/content/Intent;

    const-string v1, "android.intent.action.REGISTRATION_COMPLETED"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    const-string v1, "login_id"

    invoke-static {p0}, Lcom/osp/app/util/ad;->h(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    invoke-virtual {p0, v0}, Lcom/osp/app/signin/CheckNameBirthdateActivity;->sendBroadcast(Landroid/content/Intent;)V

    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v0, "CNB"

    const-string v1, "send signInCompleteIntentV01"

    invoke-static {v0, v1}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;)V

    invoke-static {}, Lcom/osp/app/signin/SamsungService;->a()Z

    move-result v0

    if-nez v0, :cond_1

    invoke-virtual {p0}, Lcom/osp/app/signin/CheckNameBirthdateActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string v1, "com.msc.action.MARKETING_POPUP_NOTIFICATION"

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v0

    if-eqz v0, :cond_1

    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    const-string v1, "com.msc.action.MARKETING_POPUP_NOTIFICATION"

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    const-string v1, "com.osp.app.signin"

    const-string v2, "com.osp.app.signin.BackgroundModeService"

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->setClassName(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    invoke-virtual {p0, v0}, Lcom/osp/app/signin/CheckNameBirthdateActivity;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;

    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v0, "CNB"

    const-string v1, "startService MarketingPopupNotiIntent"

    invoke-static {v0, v1}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;)V

    :cond_1
    return-void

    :cond_2
    const-string v1, "android.intent.action.SAMSUNGACCOUNT_SIGNIN_COMPLETED"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    goto :goto_0
.end method

.method static synthetic n(Lcom/osp/app/signin/CheckNameBirthdateActivity;)V
    .locals 6

    .prologue
    .line 81
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/osp/app/signin/SmsVerificationActivity;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const-string v1, "is_phnumber_verification_mode"

    const-string v2, "FROM_SIGN_IN_FLOW"

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    const-string v1, "client_id"

    iget-object v2, p0, Lcom/osp/app/signin/CheckNameBirthdateActivity;->E:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    const-string v1, "client_secret"

    iget-object v2, p0, Lcom/osp/app/signin/CheckNameBirthdateActivity;->F:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    const-string v1, "key_password"

    iget-object v2, p0, Lcom/osp/app/signin/CheckNameBirthdateActivity;->H:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    const-string v1, "key_user_id"

    iget-object v2, p0, Lcom/osp/app/signin/CheckNameBirthdateActivity;->K:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    const-string v1, "key_internal_is_resign_in"

    iget-boolean v2, p0, Lcom/osp/app/signin/CheckNameBirthdateActivity;->L:Z

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    const-string v1, "required_auth"

    iget-boolean v2, p0, Lcom/osp/app/signin/CheckNameBirthdateActivity;->M:Z

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    const-string v1, "key_duplicated_id_password"

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "+"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, Lcom/osp/app/signin/CheckNameBirthdateActivity;->G:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {p0}, Lcom/osp/app/util/ad;->p(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/osp/app/util/ad;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/google/i18n/phonenumbers/ac;

    move-result-object v1

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1}, Lcom/google/i18n/phonenumbers/ac;->b()J

    move-result-wide v4

    invoke-virtual {v2, v4, v5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1}, Lcom/google/i18n/phonenumbers/ac;->a()I

    move-result v1

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    const-string v3, "key_phonenumber"

    invoke-virtual {v0, v3, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    const-string v2, "key_country_calling_code"

    invoke-virtual {v0, v2, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    const-string v1, "is_cancelable_just_one_activity"

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    const/16 v1, 0xe5

    invoke-virtual {p0, v0, v1}, Lcom/osp/app/signin/CheckNameBirthdateActivity;->startActivityForResult(Landroid/content/Intent;I)V

    return-void
.end method

.method static synthetic o(Lcom/osp/app/signin/CheckNameBirthdateActivity;)V
    .locals 2

    .prologue
    .line 81
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/osp/app/signin/PasswordChangeView;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const/16 v1, 0xef

    invoke-virtual {p0, v0, v1}, Lcom/osp/app/signin/CheckNameBirthdateActivity;->startActivityForResult(Landroid/content/Intent;I)V

    return-void
.end method

.method static synthetic p(Lcom/osp/app/signin/CheckNameBirthdateActivity;)V
    .locals 2

    .prologue
    .line 81
    new-instance v0, Landroid/content/Intent;

    const-string v1, "com.msc.action.samsungaccount.web_with_sign_in_screen_button"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    const/16 v1, 0xf4

    invoke-virtual {p0, v0, v1}, Lcom/osp/app/signin/CheckNameBirthdateActivity;->startActivityForResult(Landroid/content/Intent;I)V

    return-void
.end method


# virtual methods
.method protected final a()V
    .locals 7

    .prologue
    const v6, 0x7f0c0190

    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 613
    invoke-direct {p0}, Lcom/osp/app/signin/CheckNameBirthdateActivity;->f()Ljava/lang/String;

    move-result-object v0

    .line 614
    invoke-direct {p0}, Lcom/osp/app/signin/CheckNameBirthdateActivity;->d()Ljava/lang/String;

    move-result-object v1

    .line 615
    invoke-direct {p0}, Lcom/osp/app/signin/CheckNameBirthdateActivity;->e()Ljava/lang/String;

    move-result-object v2

    .line 617
    iget-object v3, p0, Lcom/osp/app/signin/CheckNameBirthdateActivity;->x:Landroid/view/Menu;

    if-eqz v3, :cond_0

    .line 619
    iget-object v3, p0, Lcom/osp/app/signin/CheckNameBirthdateActivity;->x:Landroid/view/Menu;

    invoke-interface {v3, v6}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v3

    invoke-interface {v3, v4}, Landroid/view/MenuItem;->setEnabled(Z)Landroid/view/MenuItem;

    .line 621
    :cond_0
    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_4

    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_4

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_4

    iget-boolean v0, p0, Lcom/osp/app/signin/CheckNameBirthdateActivity;->I:Z

    if-ne v0, v5, :cond_4

    .line 623
    iget-object v0, p0, Lcom/osp/app/signin/CheckNameBirthdateActivity;->x:Landroid/view/Menu;

    if-eqz v0, :cond_1

    .line 625
    iget-object v0, p0, Lcom/osp/app/signin/CheckNameBirthdateActivity;->x:Landroid/view/Menu;

    invoke-interface {v0, v6}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v0

    invoke-interface {v0, v5}, Landroid/view/MenuItem;->setEnabled(Z)Landroid/view/MenuItem;

    .line 627
    :cond_1
    iget-object v0, p0, Lcom/osp/app/signin/CheckNameBirthdateActivity;->N:Lcom/osp/app/signin/BottomSoftkeyLinearLayout;

    if-eqz v0, :cond_2

    .line 629
    invoke-static {}, Lcom/osp/device/b;->b()Lcom/osp/device/b;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/osp/device/b;->l(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 631
    iget-object v0, p0, Lcom/osp/app/signin/CheckNameBirthdateActivity;->N:Lcom/osp/app/signin/BottomSoftkeyLinearLayout;

    invoke-static {v5}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/osp/app/signin/BottomSoftkeyLinearLayout;->a(Ljava/lang/Boolean;)V

    .line 655
    :cond_2
    :goto_0
    return-void

    .line 634
    :cond_3
    iget-object v0, p0, Lcom/osp/app/signin/CheckNameBirthdateActivity;->N:Lcom/osp/app/signin/BottomSoftkeyLinearLayout;

    invoke-static {v5}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/osp/app/signin/BottomSoftkeyLinearLayout;->b(Ljava/lang/Boolean;)V

    goto :goto_0

    .line 639
    :cond_4
    iget-object v0, p0, Lcom/osp/app/signin/CheckNameBirthdateActivity;->x:Landroid/view/Menu;

    if-eqz v0, :cond_5

    .line 641
    iget-object v0, p0, Lcom/osp/app/signin/CheckNameBirthdateActivity;->x:Landroid/view/Menu;

    invoke-interface {v0, v6}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v0

    invoke-interface {v0, v4}, Landroid/view/MenuItem;->setEnabled(Z)Landroid/view/MenuItem;

    .line 643
    :cond_5
    iget-object v0, p0, Lcom/osp/app/signin/CheckNameBirthdateActivity;->N:Lcom/osp/app/signin/BottomSoftkeyLinearLayout;

    if-eqz v0, :cond_2

    .line 645
    invoke-static {}, Lcom/osp/device/b;->b()Lcom/osp/device/b;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/osp/device/b;->l(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_6

    .line 647
    iget-object v0, p0, Lcom/osp/app/signin/CheckNameBirthdateActivity;->N:Lcom/osp/app/signin/BottomSoftkeyLinearLayout;

    invoke-static {v4}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/osp/app/signin/BottomSoftkeyLinearLayout;->a(Ljava/lang/Boolean;)V

    goto :goto_0

    .line 650
    :cond_6
    iget-object v0, p0, Lcom/osp/app/signin/CheckNameBirthdateActivity;->N:Lcom/osp/app/signin/BottomSoftkeyLinearLayout;

    invoke-static {v4}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/osp/app/signin/BottomSoftkeyLinearLayout;->b(Ljava/lang/Boolean;)V

    goto :goto_0
.end method

.method protected final b()V
    .locals 0

    .prologue
    .line 660
    return-void
.end method

.method public final c()V
    .locals 3

    .prologue
    .line 1839
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v0, "CNB"

    const-string v1, "CancelSignInDual_DB"

    const-string v2, "START"

    invoke-static {v0, v1, v2}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 1841
    invoke-static {p0}, Lcom/osp/app/util/ad;->d(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1843
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v0, "CNB"

    const-string v1, "cancel - CancelSignInDual_DB"

    invoke-static {v0, v1}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 1877
    :goto_0
    return-void

    .line 1849
    :cond_0
    const-string v0, ""

    const-string v1, ""

    const-string v2, ""

    invoke-static {p0, v0, v1, v2}, Lcom/msc/c/e;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 1854
    :try_start_0
    new-instance v0, Lcom/osp/security/identity/d;

    invoke-direct {v0, p0}, Lcom/osp/security/identity/d;-><init>(Landroid/content/Context;)V

    .line 1855
    invoke-virtual {v0}, Lcom/osp/security/identity/d;->k()V
    :try_end_0
    .catch Lcom/osp/security/identity/IdentityException; {:try_start_0 .. :try_end_0} :catch_0

    .line 1862
    :goto_1
    :try_start_1
    new-instance v0, Lcom/osp/security/credential/a;

    invoke-direct {v0, p0}, Lcom/osp/security/credential/a;-><init>(Landroid/content/Context;)V

    .line 1863
    invoke-virtual {v0}, Lcom/osp/security/credential/a;->a()V
    :try_end_1
    .catch Lcom/osp/security/credential/CredentialException; {:try_start_1 .. :try_end_1} :catch_1

    .line 1870
    :goto_2
    :try_start_2
    new-instance v0, Lcom/osp/social/member/d;

    invoke-direct {v0, p0}, Lcom/osp/social/member/d;-><init>(Landroid/content/Context;)V

    .line 1871
    invoke-virtual {v0}, Lcom/osp/social/member/d;->a()V
    :try_end_2
    .catch Lcom/osp/social/member/MemberServiceException; {:try_start_2 .. :try_end_2} :catch_2

    .line 1876
    :goto_3
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v0, "CNB"

    const-string v1, "CancelSignInDual_DB"

    const-string v2, "END"

    invoke-static {v0, v1, v2}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 1856
    :catch_0
    move-exception v0

    invoke-virtual {v0}, Lcom/osp/security/identity/IdentityException;->printStackTrace()V

    goto :goto_1

    .line 1864
    :catch_1
    move-exception v0

    invoke-virtual {v0}, Lcom/osp/security/credential/CredentialException;->printStackTrace()V

    goto :goto_2

    .line 1872
    :catch_2
    move-exception v0

    invoke-virtual {v0}, Lcom/osp/social/member/MemberServiceException;->printStackTrace()V

    goto :goto_3
.end method

.method protected onActivityResult(IILandroid/content/Intent;)V
    .locals 3

    .prologue
    const/16 v2, 0xe

    .line 1993
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "onActivityResult RequestCode : "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;)V

    .line 1994
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "onActivityResult resultCode : "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;)V

    .line 1995
    invoke-super {p0, p1, p2, p3}, Lcom/osp/app/util/BaseActivity;->onActivityResult(IILandroid/content/Intent;)V

    .line 1997
    const/16 v0, 0xef

    if-ne p1, v0, :cond_0

    .line 1999
    const/4 v0, -0x1

    invoke-virtual {p0, v0}, Lcom/osp/app/signin/CheckNameBirthdateActivity;->b(I)V

    .line 2000
    invoke-virtual {p0}, Lcom/osp/app/signin/CheckNameBirthdateActivity;->finish()V

    .line 2038
    :goto_0
    return-void

    .line 2004
    :cond_0
    const/16 v0, 0xe7

    if-ne p1, v0, :cond_1

    .line 2006
    const/16 v0, 0x18

    if-ne p2, v0, :cond_1

    .line 2008
    invoke-virtual {p0, v2}, Lcom/osp/app/signin/CheckNameBirthdateActivity;->b(I)V

    .line 2009
    invoke-virtual {p0}, Lcom/osp/app/signin/CheckNameBirthdateActivity;->finish()V

    goto :goto_0

    .line 2014
    :cond_1
    const/16 v0, 0xf4

    if-ne p1, v0, :cond_2

    .line 2016
    invoke-virtual {p0, v2}, Lcom/osp/app/signin/CheckNameBirthdateActivity;->b(I)V

    .line 2017
    invoke-virtual {p0}, Lcom/osp/app/signin/CheckNameBirthdateActivity;->finish()V

    goto :goto_0

    .line 2021
    :cond_2
    sparse-switch p2, :sswitch_data_0

    goto :goto_0

    .line 2024
    :sswitch_0
    invoke-virtual {p0, p2}, Lcom/osp/app/signin/CheckNameBirthdateActivity;->b(I)V

    .line 2025
    invoke-virtual {p0}, Lcom/osp/app/signin/CheckNameBirthdateActivity;->finish()V

    goto :goto_0

    .line 2028
    :sswitch_1
    invoke-virtual {p0, p2}, Lcom/osp/app/signin/CheckNameBirthdateActivity;->b(I)V

    .line 2029
    invoke-virtual {p0}, Lcom/osp/app/signin/CheckNameBirthdateActivity;->finish()V

    goto :goto_0

    .line 2032
    :sswitch_2
    invoke-virtual {p0, p2}, Lcom/osp/app/signin/CheckNameBirthdateActivity;->b(I)V

    .line 2033
    invoke-virtual {p0}, Lcom/osp/app/signin/CheckNameBirthdateActivity;->finish()V

    goto :goto_0

    .line 2036
    :sswitch_3
    invoke-virtual {p0, p2}, Lcom/osp/app/signin/CheckNameBirthdateActivity;->b(I)V

    .line 2037
    invoke-virtual {p0}, Lcom/osp/app/signin/CheckNameBirthdateActivity;->finish()V

    goto :goto_0

    .line 2021
    nop

    :sswitch_data_0
    .sparse-switch
        -0x1 -> :sswitch_0
        0x0 -> :sswitch_2
        0x1 -> :sswitch_1
        0x15 -> :sswitch_3
    .end sparse-switch
.end method

.method public onBackPressed()V
    .locals 3

    .prologue
    .line 909
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v0, "CNB"

    const-string v1, "onBackPressed"

    invoke-static {v0, v1}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 911
    invoke-virtual {p0}, Lcom/osp/app/signin/CheckNameBirthdateActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string v1, "is_cancelable_just_one_activity"

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 913
    const/16 v0, 0xe

    invoke-virtual {p0, v0}, Lcom/osp/app/signin/CheckNameBirthdateActivity;->b(I)V

    .line 916
    :cond_0
    invoke-super {p0}, Lcom/osp/app/util/BaseActivity;->onBackPressed()V

    .line 917
    return-void
.end method

.method public onConfigurationChanged(Landroid/content/res/Configuration;)V
    .locals 1

    .prologue
    .line 474
    invoke-super {p0, p1}, Lcom/osp/app/util/BaseActivity;->onConfigurationChanged(Landroid/content/res/Configuration;)V

    .line 476
    iget v0, p1, Landroid/content/res/Configuration;->orientation:I

    invoke-direct {p0, v0}, Lcom/osp/app/signin/CheckNameBirthdateActivity;->a(I)V

    .line 477
    return-void
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 8

    .prologue
    const/4 v1, 0x1

    const v7, 0x7f090064

    const v6, 0x7f090019

    const v5, 0x7f070006

    const/4 v2, 0x0

    .line 252
    invoke-super {p0, p1}, Lcom/osp/app/util/BaseActivity;->onCreate(Landroid/os/Bundle;)V

    .line 254
    invoke-static {}, Lcom/osp/device/b;->b()Lcom/osp/device/b;

    move-result-object v0

    invoke-virtual {v0}, Lcom/osp/device/b;->a()Z

    move-result v0

    if-nez v0, :cond_0

    .line 256
    invoke-virtual {p0}, Lcom/osp/app/signin/CheckNameBirthdateActivity;->finish()V

    .line 259
    :cond_0
    invoke-virtual {p0}, Lcom/osp/app/signin/CheckNameBirthdateActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string v3, "client_id"

    invoke-virtual {v0, v3}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/osp/app/signin/CheckNameBirthdateActivity;->E:Ljava/lang/String;

    invoke-virtual {p0}, Lcom/osp/app/signin/CheckNameBirthdateActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string v3, "client_secret"

    invoke-virtual {v0, v3}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/osp/app/signin/CheckNameBirthdateActivity;->F:Ljava/lang/String;

    invoke-virtual {p0}, Lcom/osp/app/signin/CheckNameBirthdateActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string v3, "key_login_id"

    invoke-virtual {v0, v3}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/osp/app/signin/CheckNameBirthdateActivity;->G:Ljava/lang/String;

    invoke-virtual {p0}, Lcom/osp/app/signin/CheckNameBirthdateActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string v3, "key_password"

    invoke-virtual {v0, v3}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/osp/app/signin/CheckNameBirthdateActivity;->H:Ljava/lang/String;

    invoke-virtual {p0}, Lcom/osp/app/signin/CheckNameBirthdateActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string v3, "required_auth"

    invoke-virtual {v0, v3, v2}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v0

    iput-boolean v0, p0, Lcom/osp/app/signin/CheckNameBirthdateActivity;->M:Z

    invoke-virtual {p0}, Lcom/osp/app/signin/CheckNameBirthdateActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string v3, "key_internal_is_resign_in"

    invoke-virtual {v0, v3, v2}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v0

    iput-boolean v0, p0, Lcom/osp/app/signin/CheckNameBirthdateActivity;->L:Z

    iget-object v0, p0, Lcom/osp/app/signin/CheckNameBirthdateActivity;->E:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/osp/app/signin/CheckNameBirthdateActivity;->F:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_2

    :cond_1
    const-string v0, "j5p7ll8g33"

    iput-object v0, p0, Lcom/osp/app/signin/CheckNameBirthdateActivity;->E:Ljava/lang/String;

    const-string v0, "5763D0052DC1462E13751F753384E9A9"

    iput-object v0, p0, Lcom/osp/app/signin/CheckNameBirthdateActivity;->F:Ljava/lang/String;

    :cond_2
    iget-object v0, p0, Lcom/osp/app/signin/CheckNameBirthdateActivity;->G:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_3

    iget-object v0, p0, Lcom/osp/app/signin/CheckNameBirthdateActivity;->H:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_b

    :cond_3
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v0, "CNB"

    const-string v3, "Account Info(ID or password) is missing!!!"

    invoke-static {v0, v3}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;)V

    move v0, v2

    :goto_0
    if-nez v0, :cond_4

    .line 261
    invoke-virtual {p0}, Lcom/osp/app/signin/CheckNameBirthdateActivity;->finish()V

    .line 264
    :cond_4
    const v0, 0x7f030014

    invoke-static {p0}, Lcom/osp/app/util/r;->q(Landroid/content/Context;)Z

    move-result v3

    invoke-virtual {p0, v0, v3}, Lcom/osp/app/signin/CheckNameBirthdateActivity;->a(IZ)V

    .line 266
    invoke-static {p0}, Lcom/osp/app/util/r;->i(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_5

    .line 268
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v3, 0x13

    if-ge v0, v3, :cond_5

    .line 270
    invoke-static {}, Lcom/osp/device/b;->b()Lcom/osp/device/b;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/osp/device/b;->i(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_5

    .line 272
    invoke-static {p0}, Lcom/msc/sa/c/d;->c(Landroid/app/Activity;)V

    .line 289
    :cond_5
    invoke-static {p0}, Lcom/msc/sa/c/d;->a(Landroid/app/Activity;)V

    .line 291
    const v0, 0x7f0c00a4

    invoke-virtual {p0, v0}, Lcom/osp/app/signin/CheckNameBirthdateActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    if-eqz v0, :cond_6

    invoke-virtual {p0}, Lcom/osp/app/signin/CheckNameBirthdateActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    invoke-static {p0}, Lcom/osp/app/util/an;->a(Lcom/osp/app/util/BaseActivity;)Lcom/osp/app/util/an;

    invoke-static {v5}, Lcom/osp/app/util/an;->a(I)I

    move-result v4

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getColor(I)I

    move-result v3

    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setTextColor(I)V

    :cond_6
    const v0, 0x7f0c00a6

    invoke-virtual {p0, v0}, Lcom/osp/app/signin/CheckNameBirthdateActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    if-eqz v0, :cond_7

    invoke-virtual {p0}, Lcom/osp/app/signin/CheckNameBirthdateActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    invoke-static {p0}, Lcom/osp/app/util/an;->a(Lcom/osp/app/util/BaseActivity;)Lcom/osp/app/util/an;

    invoke-static {v5}, Lcom/osp/app/util/an;->a(I)I

    move-result v4

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getColor(I)I

    move-result v3

    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setTextColor(I)V

    :cond_7
    const v0, 0x7f0c00a9

    invoke-virtual {p0, v0}, Lcom/osp/app/signin/CheckNameBirthdateActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    if-eqz v0, :cond_8

    invoke-virtual {p0}, Lcom/osp/app/signin/CheckNameBirthdateActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    invoke-static {p0}, Lcom/osp/app/util/an;->a(Lcom/osp/app/util/BaseActivity;)Lcom/osp/app/util/an;

    invoke-static {v5}, Lcom/osp/app/util/an;->a(I)I

    move-result v4

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getColor(I)I

    move-result v3

    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setTextColor(I)V

    :cond_8
    const v0, 0x7f0c00ad

    invoke-virtual {p0, v0}, Lcom/osp/app/signin/CheckNameBirthdateActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    if-eqz v0, :cond_9

    invoke-virtual {p0}, Lcom/osp/app/signin/CheckNameBirthdateActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    invoke-static {p0}, Lcom/osp/app/util/an;->a(Lcom/osp/app/util/BaseActivity;)Lcom/osp/app/util/an;

    invoke-static {v5}, Lcom/osp/app/util/an;->a(I)I

    move-result v4

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getColor(I)I

    move-result v3

    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setTextColor(I)V

    :cond_9
    invoke-static {}, Lcom/osp/app/util/r;->v()Z

    move-result v0

    if-eqz v0, :cond_c

    const/4 v0, 0x4

    new-array v3, v0, [I

    fill-array-data v3, :array_0

    move v0, v2

    :goto_1
    array-length v4, v3

    if-ge v0, v4, :cond_c

    aget v4, v3, v0

    invoke-virtual {p0, v4}, Lcom/osp/app/signin/CheckNameBirthdateActivity;->findViewById(I)Landroid/view/View;

    move-result-object v4

    if-eqz v4, :cond_a

    const/16 v5, 0x8

    invoke-virtual {v4, v5}, Landroid/view/View;->setVisibility(I)V

    :cond_a
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    :cond_b
    move v0, v1

    .line 259
    goto/16 :goto_0

    .line 291
    :cond_c
    const v0, 0x7f0c0015

    invoke-virtual {p0, v0}, Lcom/osp/app/signin/CheckNameBirthdateActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    new-instance v3, Lcom/osp/app/signin/df;

    invoke-direct {v3, p0}, Lcom/osp/app/signin/df;-><init>(Lcom/osp/app/signin/CheckNameBirthdateActivity;)V

    invoke-virtual {v0, v3}, Landroid/widget/LinearLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    const v0, 0x7f0c0016

    invoke-virtual {p0, v0}, Lcom/osp/app/signin/CheckNameBirthdateActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    if-eqz v0, :cond_d

    invoke-static {p0}, Lcom/osp/app/util/an;->a(Lcom/osp/app/util/BaseActivity;)Lcom/osp/app/util/an;

    const v3, 0x7f020057

    invoke-static {v3}, Lcom/osp/app/util/an;->a(I)I

    move-result v3

    invoke-virtual {v0, v3}, Landroid/widget/ImageView;->setImageResource(I)V

    :cond_d
    const v0, 0x7f0c0065

    invoke-virtual {p0, v0}, Lcom/osp/app/signin/CheckNameBirthdateActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/osp/app/signin/BottomSoftkeyLinearLayout;

    iput-object v0, p0, Lcom/osp/app/signin/CheckNameBirthdateActivity;->N:Lcom/osp/app/signin/BottomSoftkeyLinearLayout;

    iget-object v0, p0, Lcom/osp/app/signin/CheckNameBirthdateActivity;->N:Lcom/osp/app/signin/BottomSoftkeyLinearLayout;

    if-eqz v0, :cond_e

    iget-object v0, p0, Lcom/osp/app/signin/CheckNameBirthdateActivity;->N:Lcom/osp/app/signin/BottomSoftkeyLinearLayout;

    invoke-virtual {p0}, Lcom/osp/app/signin/CheckNameBirthdateActivity;->h()Z

    move-result v3

    invoke-static {v3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v3

    invoke-virtual {v0, p0, v3}, Lcom/osp/app/signin/BottomSoftkeyLinearLayout;->a(Landroid/content/Context;Ljava/lang/Boolean;)V

    invoke-static {}, Lcom/osp/device/b;->b()Lcom/osp/device/b;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/osp/device/b;->l(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_12

    iget-object v0, p0, Lcom/osp/app/signin/CheckNameBirthdateActivity;->N:Lcom/osp/app/signin/BottomSoftkeyLinearLayout;

    invoke-virtual {p0, v7}, Lcom/osp/app/signin/CheckNameBirthdateActivity;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Lcom/osp/app/signin/BottomSoftkeyLinearLayout;->b(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/osp/app/signin/CheckNameBirthdateActivity;->N:Lcom/osp/app/signin/BottomSoftkeyLinearLayout;

    invoke-virtual {p0, v7}, Lcom/osp/app/signin/CheckNameBirthdateActivity;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Lcom/osp/app/signin/BottomSoftkeyLinearLayout;->d(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/osp/app/signin/CheckNameBirthdateActivity;->N:Lcom/osp/app/signin/BottomSoftkeyLinearLayout;

    invoke-virtual {p0, v6}, Lcom/osp/app/signin/CheckNameBirthdateActivity;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Lcom/osp/app/signin/BottomSoftkeyLinearLayout;->c(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/osp/app/signin/CheckNameBirthdateActivity;->N:Lcom/osp/app/signin/BottomSoftkeyLinearLayout;

    invoke-virtual {p0, v6}, Lcom/osp/app/signin/CheckNameBirthdateActivity;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Lcom/osp/app/signin/BottomSoftkeyLinearLayout;->e(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/osp/app/signin/CheckNameBirthdateActivity;->N:Lcom/osp/app/signin/BottomSoftkeyLinearLayout;

    iget-object v3, p0, Lcom/osp/app/signin/CheckNameBirthdateActivity;->a:Landroid/view/View$OnClickListener;

    invoke-virtual {v0, v3}, Lcom/osp/app/signin/BottomSoftkeyLinearLayout;->a(Landroid/view/View$OnClickListener;)V

    iget-object v0, p0, Lcom/osp/app/signin/CheckNameBirthdateActivity;->N:Lcom/osp/app/signin/BottomSoftkeyLinearLayout;

    iget-object v3, p0, Lcom/osp/app/signin/CheckNameBirthdateActivity;->b:Landroid/view/View$OnClickListener;

    invoke-virtual {v0, v3}, Lcom/osp/app/signin/BottomSoftkeyLinearLayout;->b(Landroid/view/View$OnClickListener;)V

    .line 292
    :cond_e
    :goto_2
    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object v0

    invoke-virtual {v0, v1}, Ljava/util/Calendar;->get(I)I

    move-result v1

    iput v1, p0, Lcom/osp/app/signin/CheckNameBirthdateActivity;->z:I

    const/4 v1, 0x2

    invoke-virtual {v0, v1}, Ljava/util/Calendar;->get(I)I

    move-result v1

    iput v1, p0, Lcom/osp/app/signin/CheckNameBirthdateActivity;->y:I

    const/4 v1, 0x5

    invoke-virtual {v0, v1}, Ljava/util/Calendar;->get(I)I

    move-result v0

    iput v0, p0, Lcom/osp/app/signin/CheckNameBirthdateActivity;->A:I

    const v0, 0x7f0c00a7

    invoke-virtual {p0, v0}, Lcom/osp/app/signin/CheckNameBirthdateActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/EditText;

    const v1, 0x7f0c00aa

    invoke-virtual {p0, v1}, Lcom/osp/app/signin/CheckNameBirthdateActivity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/EditText;

    iget-object v3, p0, Lcom/osp/app/signin/CheckNameBirthdateActivity;->d:Landroid/text/TextWatcher;

    invoke-virtual {v0, v3}, Landroid/widget/EditText;->addTextChangedListener(Landroid/text/TextWatcher;)V

    iget-object v0, p0, Lcom/osp/app/signin/CheckNameBirthdateActivity;->d:Landroid/text/TextWatcher;

    invoke-virtual {v1, v0}, Landroid/widget/EditText;->addTextChangedListener(Landroid/text/TextWatcher;)V

    invoke-static {}, Lcom/osp/device/b;->b()Lcom/osp/device/b;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/osp/device/b;->i(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_11

    const v0, 0x7f0c0166

    invoke-virtual {p0, v0}, Lcom/osp/app/signin/CheckNameBirthdateActivity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    invoke-static {}, Lcom/osp/app/signin/SamsungService;->a()Z

    move-result v0

    if-eqz v0, :cond_13

    iget-object v0, p0, Lcom/osp/app/signin/CheckNameBirthdateActivity;->N:Lcom/osp/app/signin/BottomSoftkeyLinearLayout;

    if-eqz v0, :cond_f

    iget-object v0, p0, Lcom/osp/app/signin/CheckNameBirthdateActivity;->N:Lcom/osp/app/signin/BottomSoftkeyLinearLayout;

    invoke-virtual {v0, v2}, Lcom/osp/app/signin/BottomSoftkeyLinearLayout;->setVisibility(I)V

    :cond_f
    if-eqz v1, :cond_10

    invoke-virtual {v1}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup$MarginLayoutParams;

    invoke-virtual {p0}, Lcom/osp/app/signin/CheckNameBirthdateActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f080143

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v2

    float-to-int v2, v2

    iput v2, v0, Landroid/view/ViewGroup$MarginLayoutParams;->topMargin:I

    invoke-virtual {v1, v0}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    :cond_10
    :goto_3
    invoke-static {}, Lcom/osp/app/util/r;->v()Z

    move-result v0

    if-eqz v0, :cond_11

    const/4 v0, 0x4

    invoke-virtual {v1, v0}, Landroid/view/View;->setVisibility(I)V

    :cond_11
    invoke-virtual {p0}, Lcom/osp/app/signin/CheckNameBirthdateActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v0

    iget v0, v0, Landroid/content/res/Configuration;->orientation:I

    invoke-direct {p0, v0}, Lcom/osp/app/signin/CheckNameBirthdateActivity;->a(I)V

    .line 294
    invoke-virtual {p0}, Lcom/osp/app/signin/CheckNameBirthdateActivity;->a()V

    .line 296
    return-void

    .line 291
    :cond_12
    iget-object v0, p0, Lcom/osp/app/signin/CheckNameBirthdateActivity;->N:Lcom/osp/app/signin/BottomSoftkeyLinearLayout;

    invoke-virtual {p0, v6}, Lcom/osp/app/signin/CheckNameBirthdateActivity;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Lcom/osp/app/signin/BottomSoftkeyLinearLayout;->b(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/osp/app/signin/CheckNameBirthdateActivity;->N:Lcom/osp/app/signin/BottomSoftkeyLinearLayout;

    invoke-virtual {p0, v6}, Lcom/osp/app/signin/CheckNameBirthdateActivity;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Lcom/osp/app/signin/BottomSoftkeyLinearLayout;->d(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/osp/app/signin/CheckNameBirthdateActivity;->N:Lcom/osp/app/signin/BottomSoftkeyLinearLayout;

    invoke-virtual {p0, v7}, Lcom/osp/app/signin/CheckNameBirthdateActivity;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Lcom/osp/app/signin/BottomSoftkeyLinearLayout;->c(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/osp/app/signin/CheckNameBirthdateActivity;->N:Lcom/osp/app/signin/BottomSoftkeyLinearLayout;

    invoke-virtual {p0, v7}, Lcom/osp/app/signin/CheckNameBirthdateActivity;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Lcom/osp/app/signin/BottomSoftkeyLinearLayout;->e(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/osp/app/signin/CheckNameBirthdateActivity;->N:Lcom/osp/app/signin/BottomSoftkeyLinearLayout;

    iget-object v3, p0, Lcom/osp/app/signin/CheckNameBirthdateActivity;->a:Landroid/view/View$OnClickListener;

    invoke-virtual {v0, v3}, Lcom/osp/app/signin/BottomSoftkeyLinearLayout;->b(Landroid/view/View$OnClickListener;)V

    iget-object v0, p0, Lcom/osp/app/signin/CheckNameBirthdateActivity;->N:Lcom/osp/app/signin/BottomSoftkeyLinearLayout;

    iget-object v3, p0, Lcom/osp/app/signin/CheckNameBirthdateActivity;->b:Landroid/view/View$OnClickListener;

    invoke-virtual {v0, v3}, Lcom/osp/app/signin/BottomSoftkeyLinearLayout;->a(Landroid/view/View$OnClickListener;)V

    goto/16 :goto_2

    .line 292
    :cond_13
    const-string v0, "confirm_cancel"

    invoke-virtual {p0, v0}, Lcom/osp/app/signin/CheckNameBirthdateActivity;->e(Ljava/lang/String;)V

    invoke-static {p0}, Lcom/msc/sa/c/d;->e(Landroid/app/Activity;)V

    iget-object v0, p0, Lcom/osp/app/signin/CheckNameBirthdateActivity;->N:Lcom/osp/app/signin/BottomSoftkeyLinearLayout;

    if-eqz v0, :cond_10

    iget-object v0, p0, Lcom/osp/app/signin/CheckNameBirthdateActivity;->N:Lcom/osp/app/signin/BottomSoftkeyLinearLayout;

    const/16 v2, 0x8

    invoke-virtual {v0, v2}, Lcom/osp/app/signin/BottomSoftkeyLinearLayout;->setVisibility(I)V

    goto :goto_3

    .line 291
    nop

    :array_0
    .array-data 4
        0x7f0c00a5
        0x7f0c00a8
        0x7f0c00ac
        0x7f0c00ae
    .end array-data
.end method

.method public onCreateDialog(I)Landroid/app/Dialog;
    .locals 6

    .prologue
    const/4 v0, 0x0

    const v4, 0x7f090107

    const v2, 0x7f09004f

    const v3, 0x7f090019

    .line 754
    packed-switch p1, :pswitch_data_0

    .line 785
    :goto_0
    return-object v0

    .line 758
    :pswitch_0
    new-instance v0, Landroid/app/DatePickerDialog;

    iget-object v2, p0, Lcom/osp/app/signin/CheckNameBirthdateActivity;->O:Landroid/app/DatePickerDialog$OnDateSetListener;

    iget v3, p0, Lcom/osp/app/signin/CheckNameBirthdateActivity;->z:I

    iget v4, p0, Lcom/osp/app/signin/CheckNameBirthdateActivity;->y:I

    iget v5, p0, Lcom/osp/app/signin/CheckNameBirthdateActivity;->A:I

    move-object v1, p0

    invoke-direct/range {v0 .. v5}, Landroid/app/DatePickerDialog;-><init>(Landroid/content/Context;Landroid/app/DatePickerDialog$OnDateSetListener;III)V

    iput-object v0, p0, Lcom/osp/app/signin/CheckNameBirthdateActivity;->f:Landroid/app/Dialog;

    .line 759
    iget v0, p0, Lcom/osp/app/signin/CheckNameBirthdateActivity;->z:I

    iput v0, p0, Lcom/osp/app/signin/CheckNameBirthdateActivity;->D:I

    .line 760
    iget v0, p0, Lcom/osp/app/signin/CheckNameBirthdateActivity;->y:I

    iput v0, p0, Lcom/osp/app/signin/CheckNameBirthdateActivity;->C:I

    .line 761
    iget v0, p0, Lcom/osp/app/signin/CheckNameBirthdateActivity;->A:I

    iput v0, p0, Lcom/osp/app/signin/CheckNameBirthdateActivity;->B:I

    .line 763
    iget-object v0, p0, Lcom/osp/app/signin/CheckNameBirthdateActivity;->f:Landroid/app/Dialog;

    check-cast v0, Landroid/app/DatePickerDialog;

    invoke-static {v0}, Lcom/msc/sa/c/d;->a(Landroid/app/DatePickerDialog;)V

    .line 764
    iget-object v0, p0, Lcom/osp/app/signin/CheckNameBirthdateActivity;->f:Landroid/app/Dialog;

    new-instance v1, Lcom/osp/app/signin/dg;

    invoke-direct {v1, p0}, Lcom/osp/app/signin/dg;-><init>(Lcom/osp/app/signin/CheckNameBirthdateActivity;)V

    invoke-virtual {v0, v1}, Landroid/app/Dialog;->setOnDismissListener(Landroid/content/DialogInterface$OnDismissListener;)V

    .line 772
    iget-object v0, p0, Lcom/osp/app/signin/CheckNameBirthdateActivity;->f:Landroid/app/Dialog;

    goto :goto_0

    .line 774
    :pswitch_1
    new-instance v0, Lcom/osp/app/signin/dh;

    invoke-direct {v0, p0}, Lcom/osp/app/signin/dh;-><init>(Lcom/osp/app/signin/CheckNameBirthdateActivity;)V

    new-instance v1, Landroid/app/AlertDialog$Builder;

    invoke-direct {v1, p0}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    invoke-virtual {v1, v4}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    const v2, 0x7f090108

    invoke-virtual {v1, v2}, Landroid/app/AlertDialog$Builder;->setMessage(I)Landroid/app/AlertDialog$Builder;

    invoke-virtual {v1, v4, v0}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    invoke-virtual {v1}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v0

    goto :goto_0

    .line 776
    :pswitch_2
    new-instance v0, Lcom/osp/app/signin/di;

    invoke-direct {v0, p0}, Lcom/osp/app/signin/di;-><init>(Lcom/osp/app/signin/CheckNameBirthdateActivity;)V

    new-instance v1, Landroid/app/AlertDialog$Builder;

    invoke-direct {v1, p0}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    invoke-virtual {v1, v2}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    const v2, 0x7f090110

    invoke-virtual {v1, v2}, Landroid/app/AlertDialog$Builder;->setMessage(I)Landroid/app/AlertDialog$Builder;

    const v2, 0x7f090063

    invoke-virtual {v1, v2, v0}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    iget-object v0, p0, Lcom/osp/app/signin/CheckNameBirthdateActivity;->c:Landroid/content/DialogInterface$OnCancelListener;

    invoke-virtual {v1, v0}, Landroid/app/AlertDialog$Builder;->setOnCancelListener(Landroid/content/DialogInterface$OnCancelListener;)Landroid/app/AlertDialog$Builder;

    invoke-virtual {v1}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v0

    goto :goto_0

    .line 778
    :pswitch_3
    new-instance v1, Landroid/app/AlertDialog$Builder;

    invoke-direct {v1, p0}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    invoke-virtual {v1, v2}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    const v2, 0x7f09018c

    invoke-virtual {v1, v2}, Landroid/app/AlertDialog$Builder;->setMessage(I)Landroid/app/AlertDialog$Builder;

    const v2, 0x7f090064

    invoke-virtual {v1, v2, v0}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    new-instance v0, Lcom/osp/app/signin/cz;

    invoke-direct {v0, p0}, Lcom/osp/app/signin/cz;-><init>(Lcom/osp/app/signin/CheckNameBirthdateActivity;)V

    invoke-virtual {v1, v3, v0}, Landroid/app/AlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    invoke-virtual {v1}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v0

    goto/16 :goto_0

    .line 780
    :pswitch_4
    new-instance v0, Lcom/osp/app/signin/da;

    invoke-direct {v0, p0}, Lcom/osp/app/signin/da;-><init>(Lcom/osp/app/signin/CheckNameBirthdateActivity;)V

    new-instance v1, Landroid/app/AlertDialog$Builder;

    invoke-direct {v1, p0}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    const v2, 0x7f090116

    invoke-virtual {v1, v2}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    const v2, 0x7f09019a

    invoke-virtual {v1, v2}, Landroid/app/AlertDialog$Builder;->setMessage(I)Landroid/app/AlertDialog$Builder;

    invoke-virtual {v1, v3, v0}, Landroid/app/AlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    const v2, 0x7f090042

    invoke-virtual {v1, v2, v0}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    invoke-virtual {v1}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v0

    goto/16 :goto_0

    .line 754
    nop

    :pswitch_data_0
    .packed-switch 0x64
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
    .end packed-switch
.end method

.method protected onDestroy()V
    .locals 2

    .prologue
    .line 821
    iget-object v0, p0, Lcom/osp/app/signin/CheckNameBirthdateActivity;->J:Lcom/osp/app/signin/dj;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/osp/app/signin/CheckNameBirthdateActivity;->J:Lcom/osp/app/signin/dj;

    invoke-virtual {v0}, Lcom/osp/app/signin/dj;->getStatus()Landroid/os/AsyncTask$Status;

    move-result-object v0

    sget-object v1, Landroid/os/AsyncTask$Status;->RUNNING:Landroid/os/AsyncTask$Status;

    if-ne v0, v1, :cond_0

    .line 823
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v0, "CNB"

    const-string v1, "SignInNewDualTask cancelTask"

    invoke-static {v0, v1}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 824
    iget-object v0, p0, Lcom/osp/app/signin/CheckNameBirthdateActivity;->J:Lcom/osp/app/signin/dj;

    invoke-virtual {v0}, Lcom/osp/app/signin/dj;->d()V

    .line 825
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/osp/app/signin/CheckNameBirthdateActivity;->J:Lcom/osp/app/signin/dj;

    .line 827
    :cond_0
    invoke-super {p0}, Lcom/osp/app/util/BaseActivity;->onDestroy()V

    .line 828
    return-void
.end method

.method public onOptionsItemSelected(Landroid/view/MenuItem;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    .line 790
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v1, "CNB"

    const-string v2, "onOptionsItemSelected"

    const-string v3, "START"

    invoke-static {v1, v2, v3}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 792
    invoke-static {}, Lcom/osp/device/b;->b()Lcom/osp/device/b;

    move-result-object v1

    invoke-virtual {v1, p0}, Lcom/osp/device/b;->i(Landroid/content/Context;)Z

    move-result v1

    if-nez v1, :cond_0

    sget v1, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v2, 0xb

    if-ge v1, v2, :cond_0

    .line 794
    const/4 v0, 0x0

    .line 816
    :goto_0
    return v0

    .line 797
    :cond_0
    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v1

    sparse-switch v1, :sswitch_data_0

    .line 811
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v0, "CNB"

    const-string v1, "onOptionsItemSelected"

    const-string v2, "END"

    invoke-static {v0, v1, v2}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 812
    invoke-super {p0, p1}, Lcom/osp/app/util/BaseActivity;->onOptionsItemSelected(Landroid/view/MenuItem;)Z

    move-result v0

    goto :goto_0

    .line 800
    :sswitch_0
    invoke-virtual {p0}, Lcom/osp/app/signin/CheckNameBirthdateActivity;->onBackPressed()V

    .line 801
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v1, "CNB"

    const-string v2, "onOptionsItemSelected"

    const-string v3, "END"

    invoke-static {v1, v2, v3}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 804
    :sswitch_1
    invoke-direct {p0}, Lcom/osp/app/signin/CheckNameBirthdateActivity;->g()V

    .line 815
    :goto_1
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v1, "CNB"

    const-string v2, "onOptionsItemSelected"

    const-string v3, "END"

    invoke-static {v1, v2, v3}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 807
    :sswitch_2
    invoke-virtual {p0}, Lcom/osp/app/signin/CheckNameBirthdateActivity;->onBackPressed()V

    .line 808
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v1, "CNB"

    const-string v2, "onOptionsItemSelected"

    const-string v3, "END"

    invoke-static {v1, v2, v3}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1

    .line 797
    nop

    :sswitch_data_0
    .sparse-switch
        0x102002c -> :sswitch_0
        0x7f0c018e -> :sswitch_2
        0x7f0c0190 -> :sswitch_1
    .end sparse-switch
.end method
