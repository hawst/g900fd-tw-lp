.class final Lcom/osp/app/signin/d;
.super Ljava/lang/Object;
.source "AccountHelpPreference.java"

# interfaces
.implements Landroid/preference/Preference$OnPreferenceClickListener;


# instance fields
.field final synthetic a:Lcom/osp/app/signin/AccountHelpPreference;


# direct methods
.method constructor <init>(Lcom/osp/app/signin/AccountHelpPreference;)V
    .locals 0

    .prologue
    .line 142
    iput-object p1, p0, Lcom/osp/app/signin/d;->a:Lcom/osp/app/signin/AccountHelpPreference;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onPreferenceClick(Landroid/preference/Preference;)Z
    .locals 5

    .prologue
    const/4 v4, 0x1

    .line 146
    iget-object v0, p0, Lcom/osp/app/signin/d;->a:Lcom/osp/app/signin/AccountHelpPreference;

    invoke-virtual {v0}, Lcom/osp/app/signin/AccountHelpPreference;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    iget-object v1, p0, Lcom/osp/app/signin/d;->a:Lcom/osp/app/signin/AccountHelpPreference;

    invoke-static {v1}, Lcom/osp/app/signin/AccountHelpPreference;->a(Lcom/osp/app/signin/AccountHelpPreference;)Z

    move-result v1

    invoke-static {v0, v1}, Lcom/msc/c/g;->a(Landroid/content/Context;Z)Ljava/lang/String;

    move-result-object v0

    .line 148
    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    .line 149
    new-instance v2, Landroid/content/Intent;

    const-string v3, "android.intent.action.VIEW"

    invoke-direct {v2, v3, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;Landroid/net/Uri;)V

    .line 150
    const-string v1, "com.android.browser.application_id"

    iget-object v3, p0, Lcom/osp/app/signin/d;->a:Lcom/osp/app/signin/AccountHelpPreference;

    invoke-virtual {v3}, Lcom/osp/app/signin/AccountHelpPreference;->getPackageName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v1, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 151
    iget-object v1, p0, Lcom/osp/app/signin/d;->a:Lcom/osp/app/signin/AccountHelpPreference;

    invoke-static {v2, v1}, Lcom/osp/app/util/ad;->a(Landroid/content/Intent;Landroid/content/Context;)Z

    move-result v1

    if-ne v1, v4, :cond_0

    .line 153
    iget-object v0, p0, Lcom/osp/app/signin/d;->a:Lcom/osp/app/signin/AccountHelpPreference;

    invoke-virtual {v0, v2}, Lcom/osp/app/signin/AccountHelpPreference;->startActivity(Landroid/content/Intent;)V

    .line 159
    :goto_0
    return v4

    .line 156
    :cond_0
    iget-object v1, p0, Lcom/osp/app/signin/d;->a:Lcom/osp/app/signin/AccountHelpPreference;

    invoke-static {v1, v0}, Lcom/osp/app/signin/AccountHelpPreference;->a(Lcom/osp/app/signin/AccountHelpPreference;Ljava/lang/String;)V

    goto :goto_0
.end method
