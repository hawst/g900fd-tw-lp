.class final Lcom/osp/app/signin/oc;
.super Lcom/msc/c/b;
.source "TnCView.java"


# instance fields
.field final synthetic c:Lcom/osp/app/signin/TnCView;

.field private d:J

.field private e:Ljava/lang/String;


# direct methods
.method public constructor <init>(Lcom/osp/app/signin/TnCView;)V
    .locals 2

    .prologue
    .line 5066
    iput-object p1, p0, Lcom/osp/app/signin/oc;->c:Lcom/osp/app/signin/TnCView;

    .line 5067
    invoke-direct {p0, p1}, Lcom/msc/c/b;-><init>(Landroid/content/Context;)V

    .line 5069
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v0, "TNC"

    const-string v1, "GetMyCountryZoneTask"

    invoke-static {v0, v1}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 5072
    return-void
.end method


# virtual methods
.method protected final varargs a()Ljava/lang/Boolean;
    .locals 3

    .prologue
    .line 5091
    invoke-static {}, Lcom/msc/c/g;->a()Lcom/msc/c/g;

    iget-object v0, p0, Lcom/osp/app/signin/oc;->c:Lcom/osp/app/signin/TnCView;

    invoke-static {v0, p0}, Lcom/msc/c/g;->b(Landroid/content/Context;Lcom/msc/b/h;)J

    move-result-wide v0

    iput-wide v0, p0, Lcom/osp/app/signin/oc;->d:J

    iget-wide v0, p0, Lcom/osp/app/signin/oc;->d:J

    invoke-virtual {p0, v0, v1}, Lcom/osp/app/signin/oc;->a(J)V

    iget-wide v0, p0, Lcom/osp/app/signin/oc;->d:J

    sget-object v2, Lcom/msc/b/g;->b:Lcom/msc/b/g;

    invoke-static {v0, v1, v2}, Lcom/msc/c/g;->a(JLcom/msc/b/g;)V

    .line 5092
    const/4 v0, 0x0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    return-object v0
.end method

.method public final declared-synchronized a(Lcom/msc/b/c;)V
    .locals 6

    .prologue
    .line 5125
    monitor-enter p0

    :try_start_0
    invoke-super {p0, p1}, Lcom/msc/c/b;->a(Lcom/msc/b/c;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 5127
    if-nez p1, :cond_1

    .line 5171
    :cond_0
    :goto_0
    monitor-exit p0

    return-void

    .line 5132
    :cond_1
    :try_start_1
    invoke-virtual {p1}, Lcom/msc/b/c;->a()J

    move-result-wide v0

    .line 5133
    invoke-virtual {p1}, Lcom/msc/b/c;->e()Ljava/lang/String;

    move-result-object v2

    .line 5135
    iget-wide v4, p0, Lcom/osp/app/signin/oc;->d:J
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    cmp-long v0, v0, v4

    if-nez v0, :cond_0

    .line 5139
    :try_start_2
    invoke-static {}, Lcom/msc/c/h;->a()Lcom/msc/c/h;

    invoke-static {v2}, Lcom/msc/c/h;->w(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 5140
    if-eqz v0, :cond_3

    .line 5142
    invoke-static {}, Lcom/osp/common/util/i;->a()Lcom/osp/common/util/i;

    iget-object v1, p0, Lcom/osp/app/signin/oc;->c:Lcom/osp/app/signin/TnCView;

    invoke-static {v1}, Lcom/osp/common/util/i;->o(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v1

    .line 5143
    invoke-static {}, Lcom/osp/common/util/i;->a()Lcom/osp/common/util/i;

    iget-object v2, p0, Lcom/osp/app/signin/oc;->c:Lcom/osp/app/signin/TnCView;

    invoke-static {v1}, Lcom/osp/common/util/i;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 5144
    if-eqz v2, :cond_2

    invoke-virtual {v2, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_2

    .line 5146
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v3, "GetMyCountryZoneTask countryCodeFromNetworkMcc = ["

    invoke-direct {v0, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, "]"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;)V

    .line 5147
    iget-object v0, p0, Lcom/osp/app/signin/oc;->c:Lcom/osp/app/signin/TnCView;

    invoke-static {v0, v1}, Lcom/osp/app/signin/TnCView;->b(Lcom/osp/app/signin/TnCView;Ljava/lang/String;)Ljava/lang/String;

    .line 5157
    :goto_1
    iget-object v0, p0, Lcom/osp/app/signin/oc;->c:Lcom/osp/app/signin/TnCView;

    invoke-static {v0}, Lcom/osp/app/signin/TnCView;->q(Lcom/osp/app/signin/TnCView;)Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_4

    iget-object v0, p0, Lcom/osp/app/signin/oc;->c:Lcom/osp/app/signin/TnCView;

    invoke-static {v0}, Lcom/osp/app/signin/TnCView;->q(Lcom/osp/app/signin/TnCView;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v0

    if-lez v0, :cond_4

    .line 5159
    iget-object v0, p0, Lcom/osp/app/signin/oc;->c:Lcom/osp/app/signin/TnCView;

    iget-object v1, p0, Lcom/osp/app/signin/oc;->c:Lcom/osp/app/signin/TnCView;

    invoke-static {v1}, Lcom/osp/app/signin/TnCView;->q(Lcom/osp/app/signin/TnCView;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/osp/app/util/ad;->e(Landroid/content/Context;Ljava/lang/String;)V

    .line 5160
    const-string v0, "Success"

    iput-object v0, p0, Lcom/osp/app/signin/oc;->e:Ljava/lang/String;
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_0
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0

    .line 5165
    :catch_0
    move-exception v0

    :try_start_3
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    .line 5168
    const-string v0, "Fail"

    iput-object v0, p0, Lcom/osp/app/signin/oc;->e:Ljava/lang/String;
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    goto :goto_0

    .line 5125
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 5150
    :cond_2
    :try_start_4
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "GetMyCountryZoneTask countryCode = ["

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "]"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;)V

    .line 5151
    iget-object v1, p0, Lcom/osp/app/signin/oc;->c:Lcom/osp/app/signin/TnCView;

    invoke-static {}, Lcom/osp/common/util/i;->a()Lcom/osp/common/util/i;

    iget-object v2, p0, Lcom/osp/app/signin/oc;->c:Lcom/osp/app/signin/TnCView;

    sget-object v2, Ljava/util/Locale;->ENGLISH:Ljava/util/Locale;

    invoke-virtual {v0, v2}, Ljava/lang/String;->toLowerCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/osp/common/util/i;->c(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Lcom/osp/app/signin/TnCView;->b(Lcom/osp/app/signin/TnCView;Ljava/lang/String;)Ljava/lang/String;

    goto :goto_1

    .line 5155
    :cond_3
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v0, "TNC"

    const-string v1, "GetMyCountryZoneTask Success. but countryCode is null!!"

    invoke-static {v0, v1}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1

    .line 5163
    :cond_4
    const-string v0, "Fail"

    iput-object v0, p0, Lcom/osp/app/signin/oc;->e:Ljava/lang/String;
    :try_end_4
    .catch Ljava/lang/Exception; {:try_start_4 .. :try_end_4} :catch_0
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    goto/16 :goto_0
.end method

.method protected final a(Ljava/lang/Boolean;)V
    .locals 3

    .prologue
    .line 5098
    iget-object v0, p0, Lcom/osp/app/signin/oc;->c:Lcom/osp/app/signin/TnCView;

    invoke-virtual {p0}, Lcom/osp/app/signin/oc;->g()Landroid/app/ProgressDialog;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/osp/app/signin/TnCView;->a(Lcom/osp/app/signin/TnCView;Landroid/app/ProgressDialog;)Landroid/app/ProgressDialog;

    .line 5100
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v0, "TNC"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "GetMyCountryZoneTask mcc = "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, Lcom/osp/app/signin/oc;->c:Lcom/osp/app/signin/TnCView;

    invoke-static {v2}, Lcom/osp/app/signin/TnCView;->q(Lcom/osp/app/signin/TnCView;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 5101
    const-string v0, "Success"

    iget-object v1, p0, Lcom/osp/app/signin/oc;->e:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/osp/app/signin/oc;->c:Lcom/osp/app/signin/TnCView;

    invoke-static {v0}, Lcom/osp/app/signin/TnCView;->q(Lcom/osp/app/signin/TnCView;)Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 5103
    iget-object v0, p0, Lcom/osp/app/signin/oc;->c:Lcom/osp/app/signin/TnCView;

    iget-object v0, p0, Lcom/osp/app/signin/oc;->c:Lcom/osp/app/signin/TnCView;

    invoke-static {v0}, Lcom/osp/app/signin/TnCView;->q(Lcom/osp/app/signin/TnCView;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/osp/common/util/i;->d(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    if-nez v0, :cond_0

    .line 5106
    iget-object v0, p0, Lcom/osp/app/signin/oc;->c:Lcom/osp/app/signin/TnCView;

    invoke-static {v0}, Lcom/osp/app/signin/TnCView;->N(Lcom/osp/app/signin/TnCView;)V

    .line 5121
    :goto_0
    return-void

    .line 5112
    :cond_0
    iget-object v0, p0, Lcom/osp/app/signin/oc;->c:Lcom/osp/app/signin/TnCView;

    invoke-static {v0}, Lcom/osp/app/signin/TnCView;->O(Lcom/osp/app/signin/TnCView;)V

    goto :goto_0

    .line 5116
    :cond_1
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v0, "TNC"

    const-string v1, "GetMyCountryZoneTask Fail"

    invoke-static {v0, v1}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 5119
    iget-object v0, p0, Lcom/osp/app/signin/oc;->c:Lcom/osp/app/signin/TnCView;

    invoke-static {v0}, Lcom/osp/app/signin/TnCView;->N(Lcom/osp/app/signin/TnCView;)V

    goto :goto_0
.end method

.method public final b(Lcom/msc/b/c;)V
    .locals 4

    .prologue
    .line 5175
    invoke-super {p0, p1}, Lcom/msc/c/b;->b(Lcom/msc/b/c;)V

    .line 5177
    if-nez p1, :cond_1

    .line 5189
    :cond_0
    :goto_0
    return-void

    .line 5182
    :cond_1
    invoke-virtual {p1}, Lcom/msc/b/c;->a()J

    move-result-wide v0

    .line 5184
    iget-wide v2, p0, Lcom/osp/app/signin/oc;->d:J

    cmp-long v0, v0, v2

    if-nez v0, :cond_0

    .line 5187
    const-string v0, "Fail"

    iput-object v0, p0, Lcom/osp/app/signin/oc;->e:Ljava/lang/String;

    goto :goto_0
.end method

.method protected final d()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 5076
    invoke-super {p0}, Lcom/msc/c/b;->d()V

    .line 5079
    invoke-virtual {p0, v2}, Lcom/osp/app/signin/oc;->a(Z)V

    .line 5081
    iget-object v0, p0, Lcom/osp/app/signin/oc;->c:Lcom/osp/app/signin/TnCView;

    invoke-virtual {v0}, Lcom/osp/app/signin/TnCView;->getIntent()Landroid/content/Intent;

    move-result-object v0

    .line 5082
    if-eqz v0, :cond_0

    const-string v1, "is_cancelable_just_one_activity"

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 5084
    iget-object v0, p0, Lcom/osp/app/signin/oc;->c:Lcom/osp/app/signin/TnCView;

    const/16 v1, 0xe

    iget-object v2, p0, Lcom/osp/app/signin/oc;->c:Lcom/osp/app/signin/TnCView;

    invoke-static {v2}, Lcom/osp/app/signin/TnCView;->c(Lcom/osp/app/signin/TnCView;)Landroid/content/Intent;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/osp/app/signin/TnCView;->a(ILandroid/content/Intent;)V

    .line 5086
    :cond_0
    iget-object v0, p0, Lcom/osp/app/signin/oc;->c:Lcom/osp/app/signin/TnCView;

    invoke-virtual {v0}, Lcom/osp/app/signin/TnCView;->finish()V

    .line 5087
    return-void
.end method

.method protected final synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 5061
    invoke-virtual {p0}, Lcom/osp/app/signin/oc;->a()Ljava/lang/Boolean;

    move-result-object v0

    return-object v0
.end method

.method protected final synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 5061
    check-cast p1, Ljava/lang/Boolean;

    invoke-virtual {p0, p1}, Lcom/osp/app/signin/oc;->a(Ljava/lang/Boolean;)V

    return-void
.end method
