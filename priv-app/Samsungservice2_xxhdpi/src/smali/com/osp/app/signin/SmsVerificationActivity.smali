.class public Lcom/osp/app/signin/SmsVerificationActivity;
.super Lcom/osp/app/util/BaseActivity;
.source "SmsVerificationActivity.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field private A:Z

.field private B:Ljava/lang/String;

.field private final C:Ljava/lang/String;

.field private D:Ljava/lang/String;

.field private E:Ljava/lang/String;

.field private F:Ljava/lang/String;

.field private G:Ljava/lang/String;

.field private H:Ljava/lang/String;

.field private I:Ljava/lang/String;

.field private J:Ljava/lang/String;

.field private K:Z

.field private L:Z

.field private M:Ljava/lang/String;

.field private N:Ljava/lang/String;

.field private O:Ljava/lang/String;

.field private P:Ljava/lang/String;

.field private Q:Ljava/lang/String;

.field private R:Lcom/osp/app/signin/no;

.field private S:Ljava/lang/String;

.field private T:I

.field private U:Z

.field private V:Ljava/lang/String;

.field private W:Ljava/lang/String;

.field private X:Lcom/osp/app/signin/nm;

.field private Y:Lcom/osp/app/signin/nl;

.field private Z:Lcom/osp/app/signin/ns;

.field final a:Ljava/lang/String;

.field private aa:Landroid/app/AlertDialog;

.field private ab:Landroid/app/AlertDialog;

.field private ac:Landroid/app/AlertDialog;

.field private ad:Z

.field private final ae:I

.field private final af:I

.field private ag:Z

.field private ah:Landroid/content/Intent;

.field private ai:Z

.field private aj:Z

.field private ak:Z

.field private al:Ljava/lang/String;

.field private am:Ljava/lang/String;

.field private an:Ljava/lang/String;

.field private final ao:Ljava/lang/String;

.field private ap:Ljava/lang/String;

.field private aq:Ljava/lang/String;

.field private ar:Lcom/osp/app/signin/BottomSoftkeyLinearLayout;

.field private final as:Landroid/content/BroadcastReceiver;

.field private final at:Landroid/os/Handler;

.field final b:Ljava/lang/String;

.field final c:Landroid/content/DialogInterface$OnCancelListener;

.field final d:Landroid/view/View$OnClickListener;

.field final e:Landroid/view/View$OnClickListener;

.field private final f:Ljava/lang/String;

.field private final y:I

.field private z:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x0

    .line 101
    invoke-direct {p0}, Lcom/osp/app/util/BaseActivity;-><init>()V

    .line 103
    const-string v0, "SMSV"

    iput-object v0, p0, Lcom/osp/app/signin/SmsVerificationActivity;->f:Ljava/lang/String;

    .line 105
    const/16 v0, 0x3c

    iput v0, p0, Lcom/osp/app/signin/SmsVerificationActivity;->y:I

    .line 107
    const-string v0, ""

    iput-object v0, p0, Lcom/osp/app/signin/SmsVerificationActivity;->z:Ljava/lang/String;

    .line 112
    iput-boolean v1, p0, Lcom/osp/app/signin/SmsVerificationActivity;->A:Z

    .line 117
    const-string v0, "/link/link.do"

    iput-object v0, p0, Lcom/osp/app/signin/SmsVerificationActivity;->a:Ljava/lang/String;

    .line 121
    const-string v0, "/main/main.do"

    iput-object v0, p0, Lcom/osp/app/signin/SmsVerificationActivity;->b:Ljava/lang/String;

    .line 162
    const-string v0, "Account:"

    iput-object v0, p0, Lcom/osp/app/signin/SmsVerificationActivity;->B:Ljava/lang/String;

    .line 167
    const-string v0, ":"

    iput-object v0, p0, Lcom/osp/app/signin/SmsVerificationActivity;->C:Ljava/lang/String;

    .line 204
    iput-boolean v1, p0, Lcom/osp/app/signin/SmsVerificationActivity;->K:Z

    .line 205
    iput-boolean v1, p0, Lcom/osp/app/signin/SmsVerificationActivity;->L:Z

    .line 210
    const-string v0, ""

    iput-object v0, p0, Lcom/osp/app/signin/SmsVerificationActivity;->M:Ljava/lang/String;

    .line 245
    const/4 v0, -0x1

    iput v0, p0, Lcom/osp/app/signin/SmsVerificationActivity;->T:I

    .line 250
    iput-boolean v1, p0, Lcom/osp/app/signin/SmsVerificationActivity;->U:Z

    .line 257
    iput-object v2, p0, Lcom/osp/app/signin/SmsVerificationActivity;->V:Ljava/lang/String;

    .line 262
    iput-object v2, p0, Lcom/osp/app/signin/SmsVerificationActivity;->W:Ljava/lang/String;

    .line 287
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/osp/app/signin/SmsVerificationActivity;->ad:Z

    .line 292
    const/4 v0, 0x4

    iput v0, p0, Lcom/osp/app/signin/SmsVerificationActivity;->ae:I

    .line 297
    const/4 v0, 0x6

    iput v0, p0, Lcom/osp/app/signin/SmsVerificationActivity;->af:I

    .line 299
    iput-boolean v1, p0, Lcom/osp/app/signin/SmsVerificationActivity;->ag:Z

    .line 309
    iput-boolean v1, p0, Lcom/osp/app/signin/SmsVerificationActivity;->ai:Z

    .line 311
    iput-boolean v1, p0, Lcom/osp/app/signin/SmsVerificationActivity;->aj:Z

    .line 313
    iput-boolean v1, p0, Lcom/osp/app/signin/SmsVerificationActivity;->ak:Z

    .line 317
    new-instance v0, Lcom/osp/app/signin/mp;

    invoke-direct {v0, p0}, Lcom/osp/app/signin/mp;-><init>(Lcom/osp/app/signin/SmsVerificationActivity;)V

    iput-object v0, p0, Lcom/osp/app/signin/SmsVerificationActivity;->c:Landroid/content/DialogInterface$OnCancelListener;

    .line 328
    iput-object v2, p0, Lcom/osp/app/signin/SmsVerificationActivity;->al:Ljava/lang/String;

    .line 330
    iput-object v2, p0, Lcom/osp/app/signin/SmsVerificationActivity;->am:Ljava/lang/String;

    .line 331
    iput-object v2, p0, Lcom/osp/app/signin/SmsVerificationActivity;->an:Ljava/lang/String;

    .line 335
    const-string v0, "310"

    iput-object v0, p0, Lcom/osp/app/signin/SmsVerificationActivity;->ao:Ljava/lang/String;

    .line 339
    iput-object v2, p0, Lcom/osp/app/signin/SmsVerificationActivity;->ap:Ljava/lang/String;

    .line 343
    iput-object v2, p0, Lcom/osp/app/signin/SmsVerificationActivity;->aq:Ljava/lang/String;

    .line 347
    new-instance v0, Lcom/osp/app/signin/na;

    invoke-direct {v0, p0}, Lcom/osp/app/signin/na;-><init>(Lcom/osp/app/signin/SmsVerificationActivity;)V

    iput-object v0, p0, Lcom/osp/app/signin/SmsVerificationActivity;->d:Landroid/view/View$OnClickListener;

    .line 354
    new-instance v0, Lcom/osp/app/signin/ne;

    invoke-direct {v0, p0}, Lcom/osp/app/signin/ne;-><init>(Lcom/osp/app/signin/SmsVerificationActivity;)V

    iput-object v0, p0, Lcom/osp/app/signin/SmsVerificationActivity;->e:Landroid/view/View$OnClickListener;

    .line 364
    new-instance v0, Lcom/osp/app/signin/nf;

    invoke-direct {v0, p0}, Lcom/osp/app/signin/nf;-><init>(Lcom/osp/app/signin/SmsVerificationActivity;)V

    iput-object v0, p0, Lcom/osp/app/signin/SmsVerificationActivity;->as:Landroid/content/BroadcastReceiver;

    .line 534
    new-instance v0, Lcom/osp/app/signin/ng;

    invoke-direct {v0, p0}, Lcom/osp/app/signin/ng;-><init>(Lcom/osp/app/signin/SmsVerificationActivity;)V

    iput-object v0, p0, Lcom/osp/app/signin/SmsVerificationActivity;->at:Landroid/os/Handler;

    .line 4202
    return-void
.end method

.method static synthetic A(Lcom/osp/app/signin/SmsVerificationActivity;)V
    .locals 0

    .prologue
    .line 101
    invoke-direct {p0}, Lcom/osp/app/signin/SmsVerificationActivity;->g()V

    return-void
.end method

.method static synthetic B(Lcom/osp/app/signin/SmsVerificationActivity;)V
    .locals 0

    .prologue
    .line 101
    invoke-direct {p0}, Lcom/osp/app/signin/SmsVerificationActivity;->f()V

    return-void
.end method

.method static synthetic C(Lcom/osp/app/signin/SmsVerificationActivity;)Lcom/osp/app/signin/ns;
    .locals 1

    .prologue
    .line 101
    iget-object v0, p0, Lcom/osp/app/signin/SmsVerificationActivity;->Z:Lcom/osp/app/signin/ns;

    return-object v0
.end method

.method static synthetic D(Lcom/osp/app/signin/SmsVerificationActivity;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 101
    iget-object v0, p0, Lcom/osp/app/signin/SmsVerificationActivity;->E:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic E(Lcom/osp/app/signin/SmsVerificationActivity;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 101
    iget-object v0, p0, Lcom/osp/app/signin/SmsVerificationActivity;->ap:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic F(Lcom/osp/app/signin/SmsVerificationActivity;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 101
    iget-object v0, p0, Lcom/osp/app/signin/SmsVerificationActivity;->aq:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic G(Lcom/osp/app/signin/SmsVerificationActivity;)Z
    .locals 1

    .prologue
    .line 101
    iget-boolean v0, p0, Lcom/osp/app/signin/SmsVerificationActivity;->ak:Z

    return v0
.end method

.method static synthetic H(Lcom/osp/app/signin/SmsVerificationActivity;)V
    .locals 2

    .prologue
    .line 101
    const v0, 0x7f0c00c4

    invoke-virtual {p0, v0}, Lcom/osp/app/signin/SmsVerificationActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/EditText;

    const-string v1, ""

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    return-void
.end method

.method static synthetic I(Lcom/osp/app/signin/SmsVerificationActivity;)V
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 101
    iget-object v0, p0, Lcom/osp/app/signin/SmsVerificationActivity;->ac:Landroid/app/AlertDialog;

    if-eqz v0, :cond_1

    :try_start_0
    iget-object v0, p0, Lcom/osp/app/signin/SmsVerificationActivity;->ac:Landroid/app/AlertDialog;

    invoke-virtual {v0}, Landroid/app/AlertDialog;->isShowing()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/osp/app/signin/SmsVerificationActivity;->ac:Landroid/app/AlertDialog;

    invoke-virtual {v0}, Landroid/app/AlertDialog;->dismiss()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :cond_0
    :goto_0
    iput-object v1, p0, Lcom/osp/app/signin/SmsVerificationActivity;->ac:Landroid/app/AlertDialog;

    :cond_1
    iget-object v0, p0, Lcom/osp/app/signin/SmsVerificationActivity;->ac:Landroid/app/AlertDialog;

    if-nez v0, :cond_2

    :try_start_1
    new-instance v0, Landroid/app/AlertDialog$Builder;

    invoke-direct {v0, p0}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    const v1, 0x7f09004f

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    const v1, 0x7f090198

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setMessage(I)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    const v1, 0x7f090041

    new-instance v2, Lcom/osp/app/signin/nd;

    invoke-direct {v2, p0}, Lcom/osp/app/signin/nd;-><init>(Lcom/osp/app/signin/SmsVerificationActivity;)V

    invoke-virtual {v0, v1, v2}, Landroid/app/AlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    const v1, 0x7f09005c

    new-instance v2, Lcom/osp/app/signin/nc;

    invoke-direct {v2, p0}, Lcom/osp/app/signin/nc;-><init>(Lcom/osp/app/signin/SmsVerificationActivity;)V

    invoke-virtual {v0, v1, v2}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    new-instance v1, Lcom/osp/app/signin/nb;

    invoke-direct {v1, p0}, Lcom/osp/app/signin/nb;-><init>(Lcom/osp/app/signin/SmsVerificationActivity;)V

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setOnCancelListener(Landroid/content/DialogInterface$OnCancelListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->show()Landroid/app/AlertDialog;

    move-result-object v0

    iput-object v0, p0, Lcom/osp/app/signin/SmsVerificationActivity;->ac:Landroid/app/AlertDialog;
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1

    :cond_2
    :goto_1
    return-void

    :catch_0
    move-exception v0

    :try_start_2
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v0

    iput-object v1, p0, Lcom/osp/app/signin/SmsVerificationActivity;->ac:Landroid/app/AlertDialog;

    throw v0

    :catch_1
    move-exception v0

    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_1
.end method

.method static synthetic J(Lcom/osp/app/signin/SmsVerificationActivity;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 101
    iget-object v0, p0, Lcom/osp/app/signin/SmsVerificationActivity;->Q:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic K(Lcom/osp/app/signin/SmsVerificationActivity;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 101
    iget-object v0, p0, Lcom/osp/app/signin/SmsVerificationActivity;->P:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic L(Lcom/osp/app/signin/SmsVerificationActivity;)Z
    .locals 1

    .prologue
    .line 101
    iget-boolean v0, p0, Lcom/osp/app/signin/SmsVerificationActivity;->K:Z

    return v0
.end method

.method static synthetic M(Lcom/osp/app/signin/SmsVerificationActivity;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 101
    iget-object v0, p0, Lcom/osp/app/signin/SmsVerificationActivity;->al:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic N(Lcom/osp/app/signin/SmsVerificationActivity;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 101
    iget-object v0, p0, Lcom/osp/app/signin/SmsVerificationActivity;->V:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic O(Lcom/osp/app/signin/SmsVerificationActivity;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 101
    iget-object v0, p0, Lcom/osp/app/signin/SmsVerificationActivity;->W:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic P(Lcom/osp/app/signin/SmsVerificationActivity;)Z
    .locals 1

    .prologue
    .line 101
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/osp/app/signin/SmsVerificationActivity;->L:Z

    return v0
.end method

.method static synthetic Q(Lcom/osp/app/signin/SmsVerificationActivity;)Lcom/osp/app/signin/BottomSoftkeyLinearLayout;
    .locals 1

    .prologue
    .line 101
    iget-object v0, p0, Lcom/osp/app/signin/SmsVerificationActivity;->ar:Lcom/osp/app/signin/BottomSoftkeyLinearLayout;

    return-object v0
.end method

.method static synthetic R(Lcom/osp/app/signin/SmsVerificationActivity;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 101
    iget-object v0, p0, Lcom/osp/app/signin/SmsVerificationActivity;->F:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic S(Lcom/osp/app/signin/SmsVerificationActivity;)V
    .locals 1

    .prologue
    .line 101
    new-instance v0, Lcom/osp/app/signin/no;

    invoke-direct {v0, p0, p0}, Lcom/osp/app/signin/no;-><init>(Lcom/osp/app/signin/SmsVerificationActivity;Landroid/content/Context;)V

    iput-object v0, p0, Lcom/osp/app/signin/SmsVerificationActivity;->R:Lcom/osp/app/signin/no;

    iget-object v0, p0, Lcom/osp/app/signin/SmsVerificationActivity;->R:Lcom/osp/app/signin/no;

    invoke-virtual {v0}, Lcom/osp/app/signin/no;->b()V

    return-void
.end method

.method static synthetic T(Lcom/osp/app/signin/SmsVerificationActivity;)V
    .locals 0

    .prologue
    .line 101
    invoke-direct {p0}, Lcom/osp/app/signin/SmsVerificationActivity;->z()V

    return-void
.end method

.method static synthetic U(Lcom/osp/app/signin/SmsVerificationActivity;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 101
    iget-object v0, p0, Lcom/osp/app/signin/SmsVerificationActivity;->z:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic V(Lcom/osp/app/signin/SmsVerificationActivity;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 101
    iget-object v0, p0, Lcom/osp/app/signin/SmsVerificationActivity;->M:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic W(Lcom/osp/app/signin/SmsVerificationActivity;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 101
    iget-object v0, p0, Lcom/osp/app/signin/SmsVerificationActivity;->I:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic X(Lcom/osp/app/signin/SmsVerificationActivity;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 101
    iget-object v0, p0, Lcom/osp/app/signin/SmsVerificationActivity;->J:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic Y(Lcom/osp/app/signin/SmsVerificationActivity;)Z
    .locals 1

    .prologue
    .line 101
    iget-boolean v0, p0, Lcom/osp/app/signin/SmsVerificationActivity;->A:Z

    return v0
.end method

.method static synthetic Z(Lcom/osp/app/signin/SmsVerificationActivity;)V
    .locals 5

    .prologue
    .line 101
    invoke-static {}, Lcom/osp/device/b;->b()Lcom/osp/device/b;

    move-result-object v0

    invoke-virtual {v0}, Lcom/osp/device/b;->a()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v0, "Login try with blocked id"

    invoke-static {v0}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;)V

    new-instance v2, Landroid/content/Intent;

    const-string v0, "com.msc.action.samsungaccount.web_dialog"

    invoke-direct {v2, v0}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    const v0, 0x7f0c0121

    invoke-virtual {p0, v0}, Lcom/osp/app/signin/SmsVerificationActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/EditText;

    const-string v1, "j5p7ll8g33"

    iget-object v3, p0, Lcom/osp/app/signin/SmsVerificationActivity;->I:Ljava/lang/String;

    invoke-static {v3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_0

    iget-object v1, p0, Lcom/osp/app/signin/SmsVerificationActivity;->I:Ljava/lang/String;

    :cond_0
    invoke-static {p0}, Lcom/osp/app/util/ad;->p(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    sget-object v4, Ljava/util/Locale;->ENGLISH:Ljava/util/Locale;

    invoke-virtual {v0, v4}, Ljava/lang/String;->toLowerCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v0

    invoke-static {p0, v1, v0, v3}, Lcom/msc/c/n;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    const-string v1, "ServerUrl"

    invoke-virtual {v2, v1, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    invoke-virtual {p0, v2}, Lcom/osp/app/signin/SmsVerificationActivity;->startActivity(Landroid/content/Intent;)V

    :cond_1
    return-void
.end method

.method static synthetic a(Lcom/osp/app/signin/SmsVerificationActivity;Lcom/osp/app/signin/nm;)Lcom/osp/app/signin/nm;
    .locals 0

    .prologue
    .line 101
    iput-object p1, p0, Lcom/osp/app/signin/SmsVerificationActivity;->X:Lcom/osp/app/signin/nm;

    return-object p1
.end method

.method static synthetic a(Lcom/osp/app/signin/SmsVerificationActivity;Lcom/osp/app/signin/ns;)Lcom/osp/app/signin/ns;
    .locals 0

    .prologue
    .line 101
    iput-object p1, p0, Lcom/osp/app/signin/SmsVerificationActivity;->Z:Lcom/osp/app/signin/ns;

    return-object p1
.end method

.method static synthetic a(Lcom/osp/app/signin/SmsVerificationActivity;Ljava/lang/String;)Ljava/lang/String;
    .locals 6

    .prologue
    .line 101
    const-string v0, ""

    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_0

    iget-object v1, p0, Lcom/osp/app/signin/SmsVerificationActivity;->B:Ljava/lang/String;

    invoke-virtual {p1, v1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_1

    :cond_0
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v1, "SMSV"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "SMS msg is not contain prefix("

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v3, p0, Lcom/osp/app/signin/SmsVerificationActivity;->B:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ") !!!"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;)V

    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v1, "SMSV"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "SMS MESSAGE : "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;)V

    :goto_0
    return-object v0

    :cond_1
    iget-object v1, p0, Lcom/osp/app/signin/SmsVerificationActivity;->B:Ljava/lang/String;

    invoke-virtual {p1, v1}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v1

    iget-object v2, p0, Lcom/osp/app/signin/SmsVerificationActivity;->B:Ljava/lang/String;

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v2

    add-int/2addr v1, v2

    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v2

    invoke-virtual {p1, v1, v2}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v2

    new-instance v3, Ljava/lang/StringBuffer;

    invoke-direct {v3}, Ljava/lang/StringBuffer;-><init>()V

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v4

    const/4 v1, 0x0

    :goto_1
    if-ge v1, v4, :cond_3

    invoke-virtual {v2, v1}, Ljava/lang/String;->charAt(I)C

    move-result v5

    invoke-static {v5}, Ljava/lang/Character;->isDigit(C)Z

    move-result v5

    if-eqz v5, :cond_2

    invoke-virtual {v2, v1}, Ljava/lang/String;->charAt(I)C

    move-result v5

    invoke-virtual {v3, v5}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    :cond_2
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v2, "SMSV"

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "VerifyCode length is "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v2, v1}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;)V

    :cond_3
    invoke-virtual {v3}, Ljava/lang/StringBuffer;->length()I

    move-result v1

    const/4 v2, 0x4

    if-lt v1, v2, :cond_4

    invoke-virtual {v3}, Ljava/lang/StringBuffer;->length()I

    move-result v1

    const/4 v2, 0x6

    if-gt v1, v2, :cond_4

    invoke-virtual {v3}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    :cond_4
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v1, "SMSV"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v4, "The length of verifyCode("

    invoke-direct {v2, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ") is wrong!!!!"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0
.end method

.method private a(I)V
    .locals 8

    .prologue
    const v6, 0x7f08011e

    .line 1173
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "SMSV::initLayoutParams orientation : "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;)V

    .line 1175
    invoke-static {}, Lcom/osp/device/b;->b()Lcom/osp/device/b;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/osp/device/b;->i(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 1177
    const v0, 0x7f0c00c0

    invoke-virtual {p0, v0}, Lcom/osp/app/signin/SmsVerificationActivity;->findViewById(I)Landroid/view/View;

    move-result-object v2

    .line 1178
    const v0, 0x7f0c016b

    invoke-virtual {p0, v0}, Lcom/osp/app/signin/SmsVerificationActivity;->findViewById(I)Landroid/view/View;

    move-result-object v3

    .line 1179
    const v0, 0x7f0c016c

    invoke-virtual {p0, v0}, Lcom/osp/app/signin/SmsVerificationActivity;->findViewById(I)Landroid/view/View;

    move-result-object v4

    .line 1181
    invoke-virtual {p0}, Lcom/osp/app/signin/SmsVerificationActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f08011f

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v0

    float-to-int v0, v0

    .line 1182
    invoke-virtual {p0}, Lcom/osp/app/signin/SmsVerificationActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1, v6}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v1

    float-to-int v1, v1

    .line 1183
    if-eqz v2, :cond_1

    if-eqz v3, :cond_1

    .line 1185
    invoke-static {}, Lcom/osp/app/signin/SamsungService;->a()Z

    move-result v5

    if-nez v5, :cond_0

    .line 1187
    invoke-virtual {p0}, Lcom/osp/app/signin/SmsVerificationActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0, v6}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v0

    float-to-int v0, v0

    .line 1190
    :cond_0
    invoke-static {}, Lcom/osp/app/util/q;->a()Lcom/osp/app/util/q;

    invoke-static {v2, v0, v1, p1}, Lcom/osp/app/util/q;->b(Landroid/view/View;III)V

    .line 1191
    invoke-static {}, Lcom/osp/app/util/q;->a()Lcom/osp/app/util/q;

    invoke-static {v3, v0, v1, p1}, Lcom/osp/app/util/q;->b(Landroid/view/View;III)V

    .line 1195
    :cond_1
    invoke-static {p0}, Lcom/osp/app/util/r;->k(Landroid/content/Context;)Z

    move-result v2

    if-nez v2, :cond_5

    .line 1197
    invoke-virtual {p0}, Lcom/osp/app/signin/SmsVerificationActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f08013d

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v0

    float-to-int v1, v0

    .line 1198
    invoke-virtual {p0}, Lcom/osp/app/signin/SmsVerificationActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v2, 0x7f08013c

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v0

    float-to-int v0, v0

    .line 1201
    :goto_0
    invoke-static {}, Lcom/osp/app/util/r;->v()Z

    move-result v2

    if-eqz v2, :cond_2

    .line 1203
    if-eqz v3, :cond_2

    if-eqz v4, :cond_2

    .line 1205
    const/4 v2, 0x4

    invoke-virtual {v3, v2}, Landroid/view/View;->setVisibility(I)V

    .line 1206
    const/16 v2, 0x8

    invoke-virtual {v4, v2}, Landroid/view/View;->setVisibility(I)V

    .line 1210
    :cond_2
    iget-object v2, p0, Lcom/osp/app/signin/SmsVerificationActivity;->ar:Lcom/osp/app/signin/BottomSoftkeyLinearLayout;

    if-eqz v2, :cond_4

    .line 1212
    invoke-static {}, Lcom/osp/app/signin/SamsungService;->a()Z

    move-result v2

    if-eqz v2, :cond_3

    sget-object v2, Lcom/osp/app/util/u;->o:[Ljava/lang/String;

    invoke-static {v2}, Lcom/osp/app/util/u;->a([Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_3

    .line 1214
    const/4 v1, 0x0

    .line 1216
    :cond_3
    invoke-static {}, Lcom/osp/app/util/q;->a()Lcom/osp/app/util/q;

    iget-object v2, p0, Lcom/osp/app/signin/SmsVerificationActivity;->ar:Lcom/osp/app/signin/BottomSoftkeyLinearLayout;

    invoke-static {p0, v2, v1, v0, p1}, Lcom/osp/app/util/q;->a(Landroid/content/Context;Landroid/view/View;III)V

    .line 1219
    :cond_4
    return-void

    :cond_5
    move v7, v1

    move v1, v0

    move v0, v7

    goto :goto_0
.end method

.method static synthetic a(Lcom/osp/app/signin/SmsVerificationActivity;)V
    .locals 0

    .prologue
    .line 101
    invoke-direct {p0}, Lcom/osp/app/signin/SmsVerificationActivity;->w()V

    return-void
.end method

.method static synthetic a(Lcom/osp/app/signin/SmsVerificationActivity;Z)V
    .locals 0

    .prologue
    .line 101
    invoke-direct {p0, p1}, Lcom/osp/app/signin/SmsVerificationActivity;->a(Z)V

    return-void
.end method

.method private a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Landroid/content/DialogInterface$OnClickListener;Landroid/content/DialogInterface$OnCancelListener;)V
    .locals 3

    .prologue
    .line 2095
    invoke-virtual {p0}, Landroid/app/Activity;->isFinishing()Z

    move-result v0

    if-nez v0, :cond_0

    .line 2097
    new-instance v0, Ljava/lang/StringBuffer;

    invoke-direct {v0}, Ljava/lang/StringBuffer;-><init>()V

    .line 2098
    const-string v1, "<br><b>"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 2099
    invoke-virtual {v0, p2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 2100
    const-string v1, "</b><br>"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 2102
    new-instance v1, Landroid/app/AlertDialog$Builder;

    invoke-direct {v1, p0}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    invoke-virtual {v1, p1}, Landroid/app/AlertDialog$Builder;->setTitle(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/Html;->fromHtml(Ljava/lang/String;)Landroid/text/Spanned;

    move-result-object v0

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, "\n"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Landroid/app/AlertDialog$Builder;->setMessage(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    invoke-virtual {v0, p4, p5}, Landroid/app/AlertDialog$Builder;->setNeutralButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    invoke-virtual {v0, p6}, Landroid/app/AlertDialog$Builder;->setOnCancelListener(Landroid/content/DialogInterface$OnCancelListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    .line 2107
    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->show()Landroid/app/AlertDialog;

    .line 2109
    :cond_0
    return-void
.end method

.method private a(Z)V
    .locals 3

    .prologue
    const v2, 0x7f0c0196

    .line 1451
    iget-object v0, p0, Lcom/osp/app/signin/SmsVerificationActivity;->ar:Lcom/osp/app/signin/BottomSoftkeyLinearLayout;

    if-eqz v0, :cond_0

    .line 1453
    invoke-static {}, Lcom/osp/device/b;->b()Lcom/osp/device/b;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/osp/device/b;->l(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 1455
    iget-object v0, p0, Lcom/osp/app/signin/SmsVerificationActivity;->ar:Lcom/osp/app/signin/BottomSoftkeyLinearLayout;

    invoke-static {p1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/osp/app/signin/BottomSoftkeyLinearLayout;->a(Ljava/lang/Boolean;)V

    .line 1461
    :cond_0
    :goto_0
    iget-object v0, p0, Lcom/osp/app/signin/SmsVerificationActivity;->x:Landroid/view/Menu;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/osp/app/signin/SmsVerificationActivity;->x:Landroid/view/Menu;

    invoke-interface {v0, v2}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 1463
    iget-object v0, p0, Lcom/osp/app/signin/SmsVerificationActivity;->x:Landroid/view/Menu;

    invoke-interface {v0, v2}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v0

    invoke-interface {v0, p1}, Landroid/view/MenuItem;->setEnabled(Z)Landroid/view/MenuItem;

    .line 1465
    :cond_1
    return-void

    .line 1458
    :cond_2
    iget-object v0, p0, Lcom/osp/app/signin/SmsVerificationActivity;->ar:Lcom/osp/app/signin/BottomSoftkeyLinearLayout;

    invoke-static {p1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/osp/app/signin/BottomSoftkeyLinearLayout;->b(Ljava/lang/Boolean;)V

    goto :goto_0
.end method

.method static synthetic aa(Lcom/osp/app/signin/SmsVerificationActivity;)V
    .locals 3

    .prologue
    .line 101
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v0, "SMSV"

    const-string v1, "sendSignInCompleteBroadcast"

    invoke-static {v0, v1}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;)V

    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    iget-boolean v1, p0, Lcom/osp/app/signin/SmsVerificationActivity;->ag:Z

    if-eqz v1, :cond_2

    const-string v1, "android.intent.action.SAMSUNGACCOUNT_RESIGNIN_COMPLETED"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    :goto_0
    invoke-static {}, Lcom/osp/device/b;->b()Lcom/osp/device/b;

    move-result-object v1

    invoke-virtual {v1}, Lcom/osp/device/b;->l()I

    move-result v1

    const/16 v2, 0xb

    if-le v1, v2, :cond_0

    const/16 v1, 0x20

    invoke-virtual {v0, v1}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    :cond_0
    invoke-virtual {p0, v0}, Lcom/osp/app/signin/SmsVerificationActivity;->sendBroadcast(Landroid/content/Intent;)V

    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v0, "SMSV"

    const-string v1, "send signInCompleteIntentV02"

    invoke-static {v0, v1}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;)V

    new-instance v0, Landroid/content/Intent;

    const-string v1, "android.intent.action.REGISTRATION_COMPLETED"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    const-string v1, "login_id"

    invoke-static {p0}, Lcom/osp/app/util/ad;->h(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    invoke-virtual {p0, v0}, Lcom/osp/app/signin/SmsVerificationActivity;->sendBroadcast(Landroid/content/Intent;)V

    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v0, "SMSV"

    const-string v1, "send signInCompleteIntentV01"

    invoke-static {v0, v1}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;)V

    invoke-static {}, Lcom/osp/app/signin/SamsungService;->a()Z

    move-result v0

    if-nez v0, :cond_1

    invoke-virtual {p0}, Lcom/osp/app/signin/SmsVerificationActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string v1, "com.msc.action.MARKETING_POPUP_NOTIFICATION"

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v0

    if-eqz v0, :cond_1

    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    const-string v1, "com.msc.action.MARKETING_POPUP_NOTIFICATION"

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    const-string v1, "com.osp.app.signin"

    const-string v2, "com.osp.app.signin.BackgroundModeService"

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->setClassName(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    invoke-virtual {p0, v0}, Lcom/osp/app/signin/SmsVerificationActivity;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;

    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v0, "SMSV"

    const-string v1, "startService MarketingPopupNotiIntent"

    invoke-static {v0, v1}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;)V

    :cond_1
    return-void

    :cond_2
    const-string v1, "android.intent.action.SAMSUNGACCOUNT_SIGNIN_COMPLETED"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    goto :goto_0
.end method

.method static synthetic ab(Lcom/osp/app/signin/SmsVerificationActivity;)V
    .locals 3

    .prologue
    .line 101
    invoke-static {}, Lcom/osp/app/util/ap;->a()Lcom/osp/app/util/ap;

    const v0, 0x7f090054

    invoke-virtual {p0, v0}, Lcom/osp/app/signin/SmsVerificationActivity;->getString(I)Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x1

    invoke-static {p0, v0, v1}, Lcom/osp/app/util/ap;->a(Landroid/content/Context;Ljava/lang/String;I)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    const/4 v0, -0x1

    invoke-virtual {p0, v0}, Lcom/osp/app/signin/SmsVerificationActivity;->b(I)V

    iget-boolean v0, p0, Lcom/osp/app/signin/SmsVerificationActivity;->aj:Z

    if-eqz v0, :cond_0

    const/16 v0, 0x66

    const/4 v1, 0x0

    iget-object v2, p0, Lcom/osp/app/signin/SmsVerificationActivity;->c:Landroid/content/DialogInterface$OnCancelListener;

    invoke-virtual {p0, v0, v1, v2}, Lcom/osp/app/signin/SmsVerificationActivity;->a(ILandroid/content/DialogInterface$OnDismissListener;Landroid/content/DialogInterface$OnCancelListener;)V

    :goto_0
    return-void

    :cond_0
    invoke-direct {p0}, Lcom/osp/app/signin/SmsVerificationActivity;->z()V

    goto :goto_0
.end method

.method static synthetic b(Lcom/osp/app/signin/SmsVerificationActivity;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 101
    iget-object v0, p0, Lcom/osp/app/signin/SmsVerificationActivity;->B:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic b(Lcom/osp/app/signin/SmsVerificationActivity;Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 101
    const v0, 0x7f0c00c4

    invoke-virtual {p0, v0}, Lcom/osp/app/signin/SmsVerificationActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/EditText;

    invoke-virtual {v0, p1}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    return-void
.end method

.method static synthetic b(Lcom/osp/app/signin/SmsVerificationActivity;Z)Z
    .locals 0

    .prologue
    .line 101
    iput-boolean p1, p0, Lcom/osp/app/signin/SmsVerificationActivity;->K:Z

    return p1
.end method

.method private b(Ljava/lang/String;)Z
    .locals 2

    .prologue
    .line 1628
    iget-boolean v0, p0, Lcom/osp/app/signin/SmsVerificationActivity;->U:Z

    if-eqz v0, :cond_0

    .line 1630
    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v0

    const/4 v1, 0x4

    if-lt v0, v1, :cond_0

    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v0

    const/4 v1, 0x6

    if-gt v0, v1, :cond_0

    .line 1632
    const/4 v0, 0x1

    .line 1635
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method static synthetic c(Lcom/osp/app/signin/SmsVerificationActivity;)I
    .locals 2

    .prologue
    .line 101
    iget v0, p0, Lcom/osp/app/signin/SmsVerificationActivity;->T:I

    add-int/lit8 v1, v0, -0x1

    iput v1, p0, Lcom/osp/app/signin/SmsVerificationActivity;->T:I

    return v0
.end method

.method static synthetic c(Lcom/osp/app/signin/SmsVerificationActivity;Ljava/lang/String;)Ljava/lang/String;
    .locals 0

    .prologue
    .line 101
    iput-object p1, p0, Lcom/osp/app/signin/SmsVerificationActivity;->S:Ljava/lang/String;

    return-object p1
.end method

.method static synthetic c(Lcom/osp/app/signin/SmsVerificationActivity;Z)Z
    .locals 0

    .prologue
    .line 101
    iput-boolean p1, p0, Lcom/osp/app/signin/SmsVerificationActivity;->ad:Z

    return p1
.end method

.method static synthetic d(Lcom/osp/app/signin/SmsVerificationActivity;Ljava/lang/String;)Ljava/lang/String;
    .locals 0

    .prologue
    .line 101
    iput-object p1, p0, Lcom/osp/app/signin/SmsVerificationActivity;->G:Ljava/lang/String;

    return-object p1
.end method

.method static synthetic d(Lcom/osp/app/signin/SmsVerificationActivity;)V
    .locals 0

    .prologue
    .line 101
    invoke-direct {p0}, Lcom/osp/app/signin/SmsVerificationActivity;->t()V

    return-void
.end method

.method static synthetic d(Lcom/osp/app/signin/SmsVerificationActivity;Z)Z
    .locals 0

    .prologue
    .line 101
    iput-boolean p1, p0, Lcom/osp/app/signin/SmsVerificationActivity;->ai:Z

    return p1
.end method

.method static synthetic e(Lcom/osp/app/signin/SmsVerificationActivity;Ljava/lang/String;)Ljava/lang/String;
    .locals 0

    .prologue
    .line 101
    iput-object p1, p0, Lcom/osp/app/signin/SmsVerificationActivity;->H:Ljava/lang/String;

    return-object p1
.end method

.method private e()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 860
    const v0, 0x7f0c00c4

    invoke-virtual {p0, v0}, Lcom/osp/app/signin/SmsVerificationActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/EditText;

    .line 861
    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setEnabled(Z)V

    .line 862
    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setFocusable(Z)V

    .line 863
    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setFocusableInTouchMode(Z)V

    .line 864
    return-void
.end method

.method static synthetic e(Lcom/osp/app/signin/SmsVerificationActivity;)V
    .locals 3

    .prologue
    .line 101
    const v0, 0x7f0c015a

    invoke-virtual {p0, v0}, Lcom/osp/app/signin/SmsVerificationActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v2, p0, Lcom/osp/app/signin/SmsVerificationActivity;->G:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " (+"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/osp/app/signin/SmsVerificationActivity;->H:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const/16 v2, 0x29

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setText(Ljava/lang/CharSequence;)V

    return-void
.end method

.method static synthetic e(Lcom/osp/app/signin/SmsVerificationActivity;Z)Z
    .locals 0

    .prologue
    .line 101
    iput-boolean p1, p0, Lcom/osp/app/signin/SmsVerificationActivity;->ak:Z

    return p1
.end method

.method private f()V
    .locals 2

    .prologue
    .line 896
    const v0, 0x7f0c00c3

    invoke-virtual {p0, v0}, Lcom/osp/app/signin/SmsVerificationActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/EditText;

    .line 897
    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setEnabled(Z)V

    .line 898
    return-void
.end method

.method static synthetic f(Lcom/osp/app/signin/SmsVerificationActivity;)V
    .locals 0

    .prologue
    .line 101
    invoke-direct {p0}, Lcom/osp/app/signin/SmsVerificationActivity;->q()V

    return-void
.end method

.method static synthetic f(Lcom/osp/app/signin/SmsVerificationActivity;Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 101
    const v0, 0x7f0c00c7

    invoke-virtual {p0, v0}, Lcom/osp/app/signin/SmsVerificationActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-static {p1}, Landroid/telephony/PhoneNumberUtils;->isGlobalPhoneNumber(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/view/View;->setEnabled(Z)V

    :goto_0
    return-void

    :cond_0
    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/View;->setEnabled(Z)V

    goto :goto_0
.end method

.method static synthetic g(Lcom/osp/app/signin/SmsVerificationActivity;)Landroid/content/Intent;
    .locals 1

    .prologue
    .line 101
    iget-object v0, p0, Lcom/osp/app/signin/SmsVerificationActivity;->ah:Landroid/content/Intent;

    return-object v0
.end method

.method private g()V
    .locals 2

    .prologue
    .line 918
    const v0, 0x7f0c015a

    invoke-virtual {p0, v0}, Lcom/osp/app/signin/SmsVerificationActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    .line 919
    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setEnabled(Z)V

    .line 920
    return-void
.end method

.method static synthetic g(Lcom/osp/app/signin/SmsVerificationActivity;Ljava/lang/String;)Z
    .locals 1

    .prologue
    .line 101
    invoke-direct {p0, p1}, Lcom/osp/app/signin/SmsVerificationActivity;->b(Ljava/lang/String;)Z

    move-result v0

    return v0
.end method

.method static synthetic h(Lcom/osp/app/signin/SmsVerificationActivity;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 101
    iget-object v0, p0, Lcom/osp/app/signin/SmsVerificationActivity;->S:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic h(Lcom/osp/app/signin/SmsVerificationActivity;Ljava/lang/String;)Ljava/lang/String;
    .locals 0

    .prologue
    .line 101
    iput-object p1, p0, Lcom/osp/app/signin/SmsVerificationActivity;->V:Ljava/lang/String;

    return-object p1
.end method

.method static synthetic i(Lcom/osp/app/signin/SmsVerificationActivity;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 101
    iget-object v0, p0, Lcom/osp/app/signin/SmsVerificationActivity;->D:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic i(Lcom/osp/app/signin/SmsVerificationActivity;Ljava/lang/String;)Ljava/lang/String;
    .locals 0

    .prologue
    .line 101
    iput-object p1, p0, Lcom/osp/app/signin/SmsVerificationActivity;->B:Ljava/lang/String;

    return-object p1
.end method

.method static synthetic j(Lcom/osp/app/signin/SmsVerificationActivity;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 101
    iget-object v0, p0, Lcom/osp/app/signin/SmsVerificationActivity;->H:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic j(Lcom/osp/app/signin/SmsVerificationActivity;Ljava/lang/String;)Ljava/lang/String;
    .locals 0

    .prologue
    .line 101
    iput-object p1, p0, Lcom/osp/app/signin/SmsVerificationActivity;->W:Ljava/lang/String;

    return-object p1
.end method

.method static synthetic k(Lcom/osp/app/signin/SmsVerificationActivity;Ljava/lang/String;)Ljava/lang/String;
    .locals 0

    .prologue
    .line 101
    iput-object p1, p0, Lcom/osp/app/signin/SmsVerificationActivity;->z:Ljava/lang/String;

    return-object p1
.end method

.method static synthetic k(Lcom/osp/app/signin/SmsVerificationActivity;)V
    .locals 2

    .prologue
    .line 101
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/osp/app/signin/PasswordChangeView;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const/16 v1, 0xef

    invoke-virtual {p0, v0, v1}, Lcom/osp/app/signin/SmsVerificationActivity;->startActivityForResult(Landroid/content/Intent;I)V

    return-void
.end method

.method static synthetic l(Lcom/osp/app/signin/SmsVerificationActivity;)V
    .locals 0

    .prologue
    .line 101
    invoke-direct {p0}, Lcom/osp/app/signin/SmsVerificationActivity;->x()V

    return-void
.end method

.method static synthetic l(Lcom/osp/app/signin/SmsVerificationActivity;Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 101
    new-instance v0, Landroid/content/Intent;

    const-string v1, "com.msc.action.samsungaccount.web_with_close_button"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    const-string v1, "ServerUrl"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    invoke-virtual {p0, v0}, Lcom/osp/app/signin/SmsVerificationActivity;->startActivity(Landroid/content/Intent;)V

    return-void
.end method

.method static synthetic m(Lcom/osp/app/signin/SmsVerificationActivity;)V
    .locals 0

    .prologue
    .line 101
    invoke-direct {p0}, Lcom/osp/app/signin/SmsVerificationActivity;->v()V

    return-void
.end method

.method static synthetic n(Lcom/osp/app/signin/SmsVerificationActivity;)V
    .locals 0

    .prologue
    .line 101
    invoke-direct {p0}, Lcom/osp/app/signin/SmsVerificationActivity;->u()V

    return-void
.end method

.method static synthetic o(Lcom/osp/app/signin/SmsVerificationActivity;)Lcom/osp/app/signin/nm;
    .locals 1

    .prologue
    .line 101
    iget-object v0, p0, Lcom/osp/app/signin/SmsVerificationActivity;->X:Lcom/osp/app/signin/nm;

    return-object v0
.end method

.method static synthetic p(Lcom/osp/app/signin/SmsVerificationActivity;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 101
    invoke-direct {p0}, Lcom/osp/app/signin/SmsVerificationActivity;->r()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private q()V
    .locals 2

    .prologue
    .line 950
    const v0, 0x7f0c00c3

    invoke-virtual {p0, v0}, Lcom/osp/app/signin/SmsVerificationActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/EditText;

    .line 951
    iget-object v1, p0, Lcom/osp/app/signin/SmsVerificationActivity;->E:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    .line 953
    return-void
.end method

.method static synthetic q(Lcom/osp/app/signin/SmsVerificationActivity;)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 101
    iget-object v0, p0, Lcom/osp/app/signin/SmsVerificationActivity;->ah:Landroid/content/Intent;

    const-string v1, "key_internal_sign_up_inforamtion"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/osp/app/signin/SignUpinfo;

    iget-object v1, p0, Lcom/osp/app/signin/SmsVerificationActivity;->ab:Landroid/app/AlertDialog;

    if-eqz v1, :cond_1

    :try_start_0
    iget-object v1, p0, Lcom/osp/app/signin/SmsVerificationActivity;->ab:Landroid/app/AlertDialog;

    invoke-virtual {v1}, Landroid/app/AlertDialog;->isShowing()Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/osp/app/signin/SmsVerificationActivity;->ab:Landroid/app/AlertDialog;

    invoke-virtual {v1}, Landroid/app/AlertDialog;->dismiss()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :cond_0
    :goto_0
    iput-object v2, p0, Lcom/osp/app/signin/SmsVerificationActivity;->ab:Landroid/app/AlertDialog;

    :cond_1
    iget-object v1, p0, Lcom/osp/app/signin/SmsVerificationActivity;->ab:Landroid/app/AlertDialog;

    if-nez v1, :cond_2

    const-string v1, ""

    if-eqz v0, :cond_3

    invoke-virtual {v0}, Lcom/osp/app/signin/SignUpinfo;->e()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_3

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "+"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/osp/app/signin/SmsVerificationActivity;->H:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-direct {p0}, Lcom/osp/app/signin/SmsVerificationActivity;->r()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "\r\n\r\n"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    :goto_1
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const v1, 0x7f090141

    invoke-virtual {p0, v1}, Lcom/osp/app/signin/SmsVerificationActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    :try_start_1
    new-instance v1, Landroid/app/AlertDialog$Builder;

    invoke-direct {v1, p0}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    const v2, 0x7f090109

    invoke-virtual {v1, v2}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/app/AlertDialog$Builder;->setMessage(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    const v1, 0x7f090064

    new-instance v2, Lcom/osp/app/signin/mt;

    invoke-direct {v2, p0}, Lcom/osp/app/signin/mt;-><init>(Lcom/osp/app/signin/SmsVerificationActivity;)V

    invoke-virtual {v0, v1, v2}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->show()Landroid/app/AlertDialog;

    move-result-object v0

    iput-object v0, p0, Lcom/osp/app/signin/SmsVerificationActivity;->ab:Landroid/app/AlertDialog;
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1

    :cond_2
    :goto_2
    return-void

    :catch_0
    move-exception v1

    :try_start_2
    invoke-virtual {v1}, Ljava/lang/Exception;->printStackTrace()V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v0

    iput-object v2, p0, Lcom/osp/app/signin/SmsVerificationActivity;->ab:Landroid/app/AlertDialog;

    throw v0

    :catch_1
    move-exception v0

    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_2

    :cond_3
    move-object v0, v1

    goto :goto_1
.end method

.method private r()Ljava/lang/String;
    .locals 1

    .prologue
    .line 961
    const v0, 0x7f0c00c3

    invoke-virtual {p0, v0}, Lcom/osp/app/signin/SmsVerificationActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/EditText;

    .line 962
    invoke-virtual {v0}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method static synthetic r(Lcom/osp/app/signin/SmsVerificationActivity;)V
    .locals 0

    .prologue
    .line 101
    invoke-direct {p0}, Lcom/osp/app/signin/SmsVerificationActivity;->e()V

    return-void
.end method

.method private s()Ljava/lang/String;
    .locals 1

    .prologue
    .line 981
    const v0, 0x7f0c00c4

    invoke-virtual {p0, v0}, Lcom/osp/app/signin/SmsVerificationActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/EditText;

    .line 982
    invoke-virtual {v0}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method static synthetic s(Lcom/osp/app/signin/SmsVerificationActivity;)V
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 101
    iget-object v0, p0, Lcom/osp/app/signin/SmsVerificationActivity;->aa:Landroid/app/AlertDialog;

    if-eqz v0, :cond_1

    :try_start_0
    iget-object v0, p0, Lcom/osp/app/signin/SmsVerificationActivity;->aa:Landroid/app/AlertDialog;

    invoke-virtual {v0}, Landroid/app/AlertDialog;->isShowing()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/osp/app/signin/SmsVerificationActivity;->aa:Landroid/app/AlertDialog;

    invoke-virtual {v0}, Landroid/app/AlertDialog;->dismiss()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :cond_0
    :goto_0
    iput-object v1, p0, Lcom/osp/app/signin/SmsVerificationActivity;->aa:Landroid/app/AlertDialog;

    :cond_1
    iget-object v0, p0, Lcom/osp/app/signin/SmsVerificationActivity;->aa:Landroid/app/AlertDialog;

    if-nez v0, :cond_2

    iget-object v0, p0, Lcom/osp/app/signin/SmsVerificationActivity;->ah:Landroid/content/Intent;

    const-string v1, "key_internal_sign_up_inforamtion"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/osp/app/signin/SignUpinfo;

    const-string v1, ""

    if-eqz v0, :cond_3

    invoke-virtual {v0}, Lcom/osp/app/signin/SignUpinfo;->e()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_3

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "+"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/osp/app/signin/SmsVerificationActivity;->H:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-direct {p0}, Lcom/osp/app/signin/SmsVerificationActivity;->r()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "\r\n\r\n"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    :goto_1
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const v1, 0x7f090140

    invoke-virtual {p0, v1}, Lcom/osp/app/signin/SmsVerificationActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    :try_start_1
    new-instance v1, Landroid/app/AlertDialog$Builder;

    invoke-direct {v1, p0}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    const v2, 0x7f090109

    invoke-virtual {v1, v2}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/app/AlertDialog$Builder;->setMessage(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    const v1, 0x7f090138

    new-instance v2, Lcom/osp/app/signin/mw;

    invoke-direct {v2, p0}, Lcom/osp/app/signin/mw;-><init>(Lcom/osp/app/signin/SmsVerificationActivity;)V

    invoke-virtual {v0, v1, v2}, Landroid/app/AlertDialog$Builder;->setNeutralButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    const v1, 0x7f090041

    new-instance v2, Lcom/osp/app/signin/mv;

    invoke-direct {v2, p0}, Lcom/osp/app/signin/mv;-><init>(Lcom/osp/app/signin/SmsVerificationActivity;)V

    invoke-virtual {v0, v1, v2}, Landroid/app/AlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    const v1, 0x7f09005c

    new-instance v2, Lcom/osp/app/signin/mu;

    invoke-direct {v2, p0}, Lcom/osp/app/signin/mu;-><init>(Lcom/osp/app/signin/SmsVerificationActivity;)V

    invoke-virtual {v0, v1, v2}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->show()Landroid/app/AlertDialog;

    move-result-object v0

    iput-object v0, p0, Lcom/osp/app/signin/SmsVerificationActivity;->aa:Landroid/app/AlertDialog;
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1

    :cond_2
    :goto_2
    return-void

    :catch_0
    move-exception v0

    :try_start_2
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto/16 :goto_0

    :catchall_0
    move-exception v0

    iput-object v1, p0, Lcom/osp/app/signin/SmsVerificationActivity;->aa:Landroid/app/AlertDialog;

    throw v0

    :catch_1
    move-exception v0

    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_2

    :cond_3
    move-object v0, v1

    goto :goto_1
.end method

.method private t()V
    .locals 6

    .prologue
    const v5, 0x7f0c00c6

    const/4 v3, 0x1

    const/4 v4, 0x0

    .line 1700
    const-string v0, "%02d:%02d"

    const/4 v1, 0x2

    new-array v1, v1, [Ljava/lang/Object;

    iget v2, p0, Lcom/osp/app/signin/SmsVerificationActivity;->T:I

    div-int/lit8 v2, v2, 0x3c

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v1, v4

    iget v2, p0, Lcom/osp/app/signin/SmsVerificationActivity;->T:I

    rem-int/lit8 v2, v2, 0x3c

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v1, v3

    invoke-static {v0, v1}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    .line 1703
    invoke-virtual {p0, v5}, Lcom/osp/app/signin/SmsVerificationActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 1704
    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 1706
    iget v0, p0, Lcom/osp/app/signin/SmsVerificationActivity;->T:I

    if-nez v0, :cond_0

    .line 1708
    const v0, 0x7f0c00c7

    invoke-virtual {p0, v0}, Lcom/osp/app/signin/SmsVerificationActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v3}, Landroid/view/View;->setEnabled(Z)V

    .line 1710
    iput-boolean v4, p0, Lcom/osp/app/signin/SmsVerificationActivity;->U:Z

    .line 1711
    invoke-direct {p0, v4}, Lcom/osp/app/signin/SmsVerificationActivity;->a(Z)V

    .line 1712
    invoke-direct {p0}, Lcom/osp/app/signin/SmsVerificationActivity;->g()V

    .line 1713
    invoke-direct {p0}, Lcom/osp/app/signin/SmsVerificationActivity;->f()V

    .line 1714
    invoke-direct {p0}, Lcom/osp/app/signin/SmsVerificationActivity;->e()V

    .line 1715
    const/4 v0, -0x1

    iput v0, p0, Lcom/osp/app/signin/SmsVerificationActivity;->T:I

    .line 1717
    invoke-virtual {p0, v5}, Lcom/osp/app/signin/SmsVerificationActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 1718
    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    .line 1725
    :goto_0
    return-void

    .line 1722
    :cond_0
    iget-object v0, p0, Lcom/osp/app/signin/SmsVerificationActivity;->at:Landroid/os/Handler;

    const-wide/16 v2, 0x3e8

    invoke-virtual {v0, v4, v2, v3}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z

    goto :goto_0
.end method

.method static synthetic t(Lcom/osp/app/signin/SmsVerificationActivity;)V
    .locals 7

    .prologue
    .line 101
    const-string v0, "FROM_EDIT_PROFILE"

    iget-object v1, p0, Lcom/osp/app/signin/SmsVerificationActivity;->al:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    const-string v0, "FROM_SIGN_IN_FLOW"

    iget-object v1, p0, Lcom/osp/app/signin/SmsVerificationActivity;->al:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    const v0, 0x7f09010b

    invoke-virtual {p0, v0}, Lcom/osp/app/signin/SmsVerificationActivity;->getString(I)Ljava/lang/String;

    move-result-object v4

    :goto_0
    const v0, 0x7f090103

    invoke-virtual {p0, v0}, Lcom/osp/app/signin/SmsVerificationActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v2, "+"

    invoke-direct {v0, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, Lcom/osp/app/signin/SmsVerificationActivity;->H:Ljava/lang/String;

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v2, p0, Lcom/osp/app/signin/SmsVerificationActivity;->E:Ljava/lang/String;

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    const v0, 0x7f090106

    invoke-virtual {p0, v0}, Lcom/osp/app/signin/SmsVerificationActivity;->getString(I)Ljava/lang/String;

    move-result-object v3

    new-instance v5, Lcom/osp/app/signin/mx;

    invoke-direct {v5, p0}, Lcom/osp/app/signin/mx;-><init>(Lcom/osp/app/signin/SmsVerificationActivity;)V

    const/4 v6, 0x0

    move-object v0, p0

    invoke-direct/range {v0 .. v6}, Lcom/osp/app/signin/SmsVerificationActivity;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Landroid/content/DialogInterface$OnClickListener;Landroid/content/DialogInterface$OnCancelListener;)V

    return-void

    :cond_0
    const v0, 0x7f090104

    invoke-virtual {p0, v0}, Lcom/osp/app/signin/SmsVerificationActivity;->getString(I)Ljava/lang/String;

    move-result-object v4

    goto :goto_0
.end method

.method private u()V
    .locals 7

    .prologue
    const/4 v5, 0x0

    const/4 v4, 0x0

    .line 1731
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v0, "SMSVstartSMSAuthenticate()"

    invoke-static {v0}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;)V

    .line 1733
    const v0, 0x7f0c00c4

    invoke-virtual {p0, v0}, Lcom/osp/app/signin/SmsVerificationActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/EditText;

    .line 1734
    const-string v1, ""

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    .line 1736
    iput-boolean v4, p0, Lcom/osp/app/signin/SmsVerificationActivity;->ai:Z

    .line 1737
    invoke-direct {p0}, Lcom/osp/app/signin/SmsVerificationActivity;->r()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/osp/app/signin/SmsVerificationActivity;->E:Ljava/lang/String;

    .line 1740
    :try_start_0
    invoke-static {}, Lcom/google/i18n/phonenumbers/h;->a()Lcom/google/i18n/phonenumbers/h;

    move-result-object v0

    iget-object v1, p0, Lcom/osp/app/signin/SmsVerificationActivity;->E:Ljava/lang/String;

    iget-object v2, p0, Lcom/osp/app/signin/SmsVerificationActivity;->S:Ljava/lang/String;

    sget-object v3, Ljava/util/Locale;->ENGLISH:Ljava/util/Locale;

    invoke-virtual {v2, v3}, Ljava/lang/String;->toUpperCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/google/i18n/phonenumbers/h;->a(Ljava/lang/CharSequence;Ljava/lang/String;)Ljava/lang/Iterable;

    move-result-object v0

    invoke-interface {v0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/i18n/phonenumbers/d;

    .line 1741
    invoke-virtual {v0}, Lcom/google/i18n/phonenumbers/d;->a()Lcom/google/i18n/phonenumbers/ac;

    move-result-object v0

    .line 1742
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0}, Lcom/google/i18n/phonenumbers/ac;->b()J

    move-result-wide v2

    invoke-virtual {v1, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/osp/app/signin/SmsVerificationActivity;->E:Ljava/lang/String;

    .line 1743
    invoke-direct {p0}, Lcom/osp/app/signin/SmsVerificationActivity;->q()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 1750
    :goto_0
    const-string v0, "FROM_EDIT_PROFILE"

    iget-object v1, p0, Lcom/osp/app/signin/SmsVerificationActivity;->al:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1758
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v1, p0, Lcom/osp/app/signin/SmsVerificationActivity;->H:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/osp/app/signin/SmsVerificationActivity;->E:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 1759
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v2, p0, Lcom/osp/app/signin/SmsVerificationActivity;->Q:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/osp/app/signin/SmsVerificationActivity;->P:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1761
    const v0, 0x7f090109

    invoke-virtual {p0, v0}, Lcom/osp/app/signin/SmsVerificationActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v2, "+"

    invoke-direct {v0, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, Lcom/osp/app/signin/SmsVerificationActivity;->H:Ljava/lang/String;

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-direct {p0}, Lcom/osp/app/signin/SmsVerificationActivity;->r()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    const v0, 0x7f090105

    invoke-virtual {p0, v0}, Lcom/osp/app/signin/SmsVerificationActivity;->getString(I)Ljava/lang/String;

    move-result-object v3

    const v0, 0x7f090042

    invoke-virtual {p0, v0}, Lcom/osp/app/signin/SmsVerificationActivity;->getString(I)Ljava/lang/String;

    move-result-object v4

    move-object v0, p0

    move-object v6, v5

    invoke-direct/range {v0 .. v6}, Lcom/osp/app/signin/SmsVerificationActivity;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Landroid/content/DialogInterface$OnClickListener;Landroid/content/DialogInterface$OnCancelListener;)V

    .line 1805
    :goto_1
    return-void

    .line 1745
    :catch_0
    move-exception v0

    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_0

    .line 1767
    :cond_0
    new-instance v0, Lcom/osp/security/identity/g;

    invoke-direct {v0, p0}, Lcom/osp/security/identity/g;-><init>(Landroid/content/Context;)V

    .line 1768
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "+"

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v1, p0, Lcom/osp/app/signin/SmsVerificationActivity;->H:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/osp/app/signin/SmsVerificationActivity;->E:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/osp/app/signin/SmsVerificationActivity;->S:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/osp/security/identity/g;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/osp/security/identity/h;

    move-result-object v0

    .line 1769
    sget-object v1, Lcom/osp/security/identity/h;->a:Lcom/osp/security/identity/h;

    if-ne v0, v1, :cond_1

    .line 1786
    invoke-direct {p0}, Lcom/osp/app/signin/SmsVerificationActivity;->y()V

    .line 1791
    new-instance v0, Lcom/osp/app/signin/nm;

    invoke-direct {v0, p0}, Lcom/osp/app/signin/nm;-><init>(Lcom/osp/app/signin/SmsVerificationActivity;)V

    iput-object v0, p0, Lcom/osp/app/signin/SmsVerificationActivity;->X:Lcom/osp/app/signin/nm;

    .line 1792
    iget-object v0, p0, Lcom/osp/app/signin/SmsVerificationActivity;->X:Lcom/osp/app/signin/nm;

    invoke-virtual {v0, v4}, Lcom/osp/app/signin/nm;->a(I)V

    .line 1793
    iget-object v0, p0, Lcom/osp/app/signin/SmsVerificationActivity;->X:Lcom/osp/app/signin/nm;

    invoke-virtual {v0}, Lcom/osp/app/signin/nm;->b()V

    goto :goto_1

    .line 1801
    :cond_1
    invoke-static {}, Lcom/osp/app/util/ap;->a()Lcom/osp/app/util/ap;

    const v0, 0x7f0900ed

    const/4 v1, 0x1

    invoke-static {p0, v0, v1}, Lcom/osp/app/util/ap;->a(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    goto :goto_1
.end method

.method static synthetic u(Lcom/osp/app/signin/SmsVerificationActivity;)Z
    .locals 1

    .prologue
    .line 101
    iget-boolean v0, p0, Lcom/osp/app/signin/SmsVerificationActivity;->ad:Z

    return v0
.end method

.method private v()V
    .locals 2

    .prologue
    .line 1812
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v0, "SMSVstartSMSValidate()"

    invoke-static {v0}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;)V

    .line 1814
    invoke-direct {p0}, Lcom/osp/app/signin/SmsVerificationActivity;->y()V

    .line 1815
    invoke-direct {p0}, Lcom/osp/app/signin/SmsVerificationActivity;->s()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/osp/app/signin/SmsVerificationActivity;->F:Ljava/lang/String;

    .line 1816
    invoke-direct {p0}, Lcom/osp/app/signin/SmsVerificationActivity;->r()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/osp/app/signin/SmsVerificationActivity;->E:Ljava/lang/String;

    .line 1818
    iget-boolean v0, p0, Lcom/osp/app/signin/SmsVerificationActivity;->ai:Z

    if-eqz v0, :cond_0

    .line 1820
    invoke-direct {p0}, Lcom/osp/app/signin/SmsVerificationActivity;->g()V

    .line 1821
    invoke-direct {p0}, Lcom/osp/app/signin/SmsVerificationActivity;->f()V

    .line 1822
    invoke-virtual {p0}, Lcom/osp/app/signin/SmsVerificationActivity;->c()V

    .line 1841
    :goto_0
    return-void

    .line 1826
    :cond_0
    const-string v0, "PUBLIC_SMS_FLOW"

    iget-object v1, p0, Lcom/osp/app/signin/SmsVerificationActivity;->al:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1828
    const/4 v0, 0x3

    .line 1837
    :goto_1
    new-instance v1, Lcom/osp/app/signin/nm;

    invoke-direct {v1, p0}, Lcom/osp/app/signin/nm;-><init>(Lcom/osp/app/signin/SmsVerificationActivity;)V

    iput-object v1, p0, Lcom/osp/app/signin/SmsVerificationActivity;->X:Lcom/osp/app/signin/nm;

    .line 1838
    iget-object v1, p0, Lcom/osp/app/signin/SmsVerificationActivity;->X:Lcom/osp/app/signin/nm;

    invoke-virtual {v1, v0}, Lcom/osp/app/signin/nm;->a(I)V

    .line 1839
    iget-object v0, p0, Lcom/osp/app/signin/SmsVerificationActivity;->X:Lcom/osp/app/signin/nm;

    invoke-virtual {v0}, Lcom/osp/app/signin/nm;->b()V

    goto :goto_0

    .line 1829
    :cond_1
    const-string v0, "FROM_EDIT_PROFILE"

    iget-object v1, p0, Lcom/osp/app/signin/SmsVerificationActivity;->al:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_2

    const-string v0, "FROM_SIGN_IN_FLOW"

    iget-object v1, p0, Lcom/osp/app/signin/SmsVerificationActivity;->al:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 1831
    :cond_2
    const/4 v0, 0x2

    goto :goto_1

    .line 1834
    :cond_3
    const/4 v0, 0x1

    goto :goto_1
.end method

.method static synthetic v(Lcom/osp/app/signin/SmsVerificationActivity;)V
    .locals 2

    .prologue
    const/4 v1, 0x1

    .line 101
    const v0, 0x7f0c00c4

    invoke-virtual {p0, v0}, Lcom/osp/app/signin/SmsVerificationActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/EditText;

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setFocusable(Z)V

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setFocusableInTouchMode(Z)V

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setEnabled(Z)V

    return-void
.end method

.method private w()V
    .locals 1

    .prologue
    .line 1852
    invoke-static {p0}, Lcom/osp/app/util/ad;->a(Landroid/content/Context;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 1854
    const/16 v0, 0xdb

    invoke-virtual {p0, v0}, Lcom/osp/app/signin/SmsVerificationActivity;->c(I)V

    .line 1859
    :goto_0
    return-void

    .line 1857
    :cond_0
    invoke-direct {p0}, Lcom/osp/app/signin/SmsVerificationActivity;->v()V

    goto :goto_0
.end method

.method static synthetic w(Lcom/osp/app/signin/SmsVerificationActivity;)V
    .locals 2

    .prologue
    .line 101
    const v0, 0x7f0c015a

    invoke-virtual {p0, v0}, Lcom/osp/app/signin/SmsVerificationActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setEnabled(Z)V

    return-void
.end method

.method private x()V
    .locals 5

    .prologue
    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 1938
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "SMSVmSuccessVerified = "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-boolean v1, p0, Lcom/osp/app/signin/SmsVerificationActivity;->ai:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;)V

    .line 1940
    iget-boolean v0, p0, Lcom/osp/app/signin/SmsVerificationActivity;->ai:Z

    if-eqz v0, :cond_0

    .line 1942
    const v0, 0x7f0c00c3

    invoke-virtual {p0, v0}, Lcom/osp/app/signin/SmsVerificationActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/EditText;

    .line 1943
    const v1, 0x7f0c00c7

    invoke-virtual {p0, v1}, Lcom/osp/app/signin/SmsVerificationActivity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    .line 1945
    if-eqz v1, :cond_0

    if-eqz v0, :cond_0

    .line 1947
    iget-object v2, p0, Lcom/osp/app/signin/SmsVerificationActivity;->E:Ljava/lang/String;

    invoke-virtual {v0}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1949
    invoke-virtual {v1, v3}, Landroid/view/View;->setEnabled(Z)V

    .line 1950
    invoke-direct {p0, v4}, Lcom/osp/app/signin/SmsVerificationActivity;->a(Z)V

    .line 1958
    :cond_0
    :goto_0
    return-void

    .line 1953
    :cond_1
    invoke-virtual {v1, v4}, Landroid/view/View;->setEnabled(Z)V

    .line 1954
    invoke-direct {p0, v3}, Lcom/osp/app/signin/SmsVerificationActivity;->a(Z)V

    goto :goto_0
.end method

.method static synthetic x(Lcom/osp/app/signin/SmsVerificationActivity;)V
    .locals 2

    .prologue
    .line 101
    const v0, 0x7f0c00c3

    invoke-virtual {p0, v0}, Lcom/osp/app/signin/SmsVerificationActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/EditText;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setEnabled(Z)V

    return-void
.end method

.method private y()V
    .locals 8

    .prologue
    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 2195
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v0, "SmsVerificationActivity::CloseIME"

    invoke-static {v0}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;)V

    .line 2197
    const-string v0, "input_method"

    invoke-virtual {p0, v0}, Lcom/osp/app/signin/SmsVerificationActivity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/inputmethod/InputMethodManager;

    .line 2200
    const v1, 0x7f0c00c3

    invoke-virtual {p0, v1}, Lcom/osp/app/signin/SmsVerificationActivity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/EditText;

    .line 2201
    const v2, 0x7f0c00c4

    invoke-virtual {p0, v2}, Lcom/osp/app/signin/SmsVerificationActivity;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/EditText;

    .line 2203
    const/4 v5, 0x2

    new-array v5, v5, [Landroid/widget/EditText;

    aput-object v1, v5, v3

    aput-object v2, v5, v4

    move v1, v3

    .line 2205
    :goto_0
    array-length v2, v5

    if-ge v1, v2, :cond_0

    .line 2207
    aget-object v2, v5, v1

    if-eqz v2, :cond_1

    invoke-virtual {v2}, Landroid/widget/EditText;->hasFocus()Z

    move-result v6

    if-ne v6, v4, :cond_1

    invoke-virtual {v2}, Landroid/widget/EditText;->getWindowToken()Landroid/os/IBinder;

    move-result-object v2

    invoke-virtual {v0, v2, v3}, Landroid/view/inputmethod/InputMethodManager;->hideSoftInputFromWindow(Landroid/os/IBinder;I)Z

    move v2, v4

    :goto_1
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    new-instance v6, Ljava/lang/StringBuilder;

    const-string v7, "AccountinfoView::CheckCloseIME skip : "

    invoke-direct {v6, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v6, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v6}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;)V

    if-nez v2, :cond_0

    .line 2209
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 2212
    :cond_0
    return-void

    :cond_1
    move v2, v3

    goto :goto_1
.end method

.method static synthetic y(Lcom/osp/app/signin/SmsVerificationActivity;)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 101
    const v0, 0x7f0c00c7

    invoke-virtual {p0, v0}, Lcom/osp/app/signin/SmsVerificationActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v1}, Landroid/view/View;->setEnabled(Z)V

    iget-object v0, p0, Lcom/osp/app/signin/SmsVerificationActivity;->at:Landroid/os/Handler;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeMessages(I)V

    const v0, 0x7f0c00c6

    invoke-virtual {p0, v0}, Lcom/osp/app/signin/SmsVerificationActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/osp/app/signin/SmsVerificationActivity;->U:Z

    const/16 v0, 0x3c

    iput v0, p0, Lcom/osp/app/signin/SmsVerificationActivity;->T:I

    invoke-direct {p0}, Lcom/osp/app/signin/SmsVerificationActivity;->t()V

    return-void
.end method

.method static synthetic z(Lcom/osp/app/signin/SmsVerificationActivity;)Landroid/os/Handler;
    .locals 1

    .prologue
    .line 101
    iget-object v0, p0, Lcom/osp/app/signin/SmsVerificationActivity;->at:Landroid/os/Handler;

    return-object v0
.end method

.method private z()V
    .locals 7

    .prologue
    .line 4425
    const v0, 0x7f090119

    invoke-virtual {p0, v0}, Lcom/osp/app/signin/SmsVerificationActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v2, "+"

    invoke-direct {v0, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, Lcom/osp/app/signin/SmsVerificationActivity;->H:Ljava/lang/String;

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v2, p0, Lcom/osp/app/signin/SmsVerificationActivity;->E:Ljava/lang/String;

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    const v0, 0x7f090186

    invoke-virtual {p0, v0}, Lcom/osp/app/signin/SmsVerificationActivity;->getString(I)Ljava/lang/String;

    move-result-object v3

    const v0, 0x7f0900c7

    invoke-virtual {p0, v0}, Lcom/osp/app/signin/SmsVerificationActivity;->getString(I)Ljava/lang/String;

    move-result-object v4

    new-instance v5, Lcom/osp/app/signin/my;

    invoke-direct {v5, p0}, Lcom/osp/app/signin/my;-><init>(Lcom/osp/app/signin/SmsVerificationActivity;)V

    new-instance v6, Lcom/osp/app/signin/mz;

    invoke-direct {v6, p0}, Lcom/osp/app/signin/mz;-><init>(Lcom/osp/app/signin/SmsVerificationActivity;)V

    move-object v0, p0

    invoke-direct/range {v0 .. v6}, Lcom/osp/app/signin/SmsVerificationActivity;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Landroid/content/DialogInterface$OnClickListener;Landroid/content/DialogInterface$OnCancelListener;)V

    .line 4491
    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/String;)Ljava/lang/String;
    .locals 4

    .prologue
    .line 2168
    invoke-static {}, Lcom/osp/device/b;->b()Lcom/osp/device/b;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/osp/device/b;->i(Landroid/content/Context;)Z

    move-result v0

    .line 2170
    invoke-static {}, Lcom/msc/c/g;->a()Lcom/msc/c/g;

    invoke-static {v0}, Lcom/msc/c/k;->a(Z)Ljava/lang/String;

    move-result-object v1

    .line 2174
    invoke-static {}, Lcom/msc/c/g;->a()Lcom/msc/c/g;

    invoke-virtual {p0}, Lcom/osp/app/signin/SmsVerificationActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v2

    invoke-static {p0}, Lcom/osp/app/util/ad;->g(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3, p1, v1, v0}, Lcom/msc/c/g;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Z)Ljava/lang/String;

    move-result-object v0

    .line 2176
    return-object v0
.end method

.method protected final a()V
    .locals 1

    .prologue
    .line 1311
    iget v0, p0, Lcom/osp/app/signin/SmsVerificationActivity;->T:I

    if-lez v0, :cond_0

    invoke-direct {p0}, Lcom/osp/app/signin/SmsVerificationActivity;->s()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/osp/app/signin/SmsVerificationActivity;->b(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-direct {p0, v0}, Lcom/osp/app/signin/SmsVerificationActivity;->a(Z)V

    .line 1312
    return-void

    .line 1311
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected final b()V
    .locals 0

    .prologue
    .line 1317
    return-void
.end method

.method public final c()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    .line 2117
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v0, "SMSVshowTncActivity()"

    invoke-static {v0}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;)V

    .line 2119
    iget-object v0, p0, Lcom/osp/app/signin/SmsVerificationActivity;->ah:Landroid/content/Intent;

    const-class v1, Lcom/osp/app/signin/TnCView;

    invoke-virtual {v0, p0, v1}, Landroid/content/Intent;->setClass(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;

    .line 2120
    iget-object v0, p0, Lcom/osp/app/signin/SmsVerificationActivity;->ah:Landroid/content/Intent;

    const-string v1, "key_internal_sign_up_inforamtion"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/osp/app/signin/SignUpinfo;

    .line 2127
    if-eqz v0, :cond_1

    .line 2129
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v2, p0, Lcom/osp/app/signin/SmsVerificationActivity;->H:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/osp/app/signin/SmsVerificationActivity;->E:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/osp/app/signin/SignUpinfo;->a(Ljava/lang/String;)V

    .line 2130
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v2, p0, Lcom/osp/app/signin/SmsVerificationActivity;->H:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/osp/app/signin/SmsVerificationActivity;->E:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/osp/app/signin/SignUpinfo;->b(Ljava/lang/String;)V

    .line 2131
    iget-object v1, p0, Lcom/osp/app/signin/SmsVerificationActivity;->E:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/osp/app/signin/SignUpinfo;->A(Ljava/lang/String;)V

    .line 2132
    iget-object v1, p0, Lcom/osp/app/signin/SmsVerificationActivity;->H:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/osp/app/signin/SignUpinfo;->z(Ljava/lang/String;)V

    .line 2133
    invoke-virtual {v0}, Lcom/osp/app/signin/SignUpinfo;->G()V

    .line 2134
    invoke-virtual {v0}, Lcom/osp/app/signin/SignUpinfo;->I()V

    .line 2135
    iget-object v1, p0, Lcom/osp/app/signin/SmsVerificationActivity;->V:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/osp/app/signin/SignUpinfo;->E(Ljava/lang/String;)V

    .line 2136
    iget-object v1, p0, Lcom/osp/app/signin/SmsVerificationActivity;->F:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/osp/app/signin/SignUpinfo;->F(Ljava/lang/String;)V

    .line 2138
    iget-boolean v1, p0, Lcom/osp/app/signin/SmsVerificationActivity;->L:Z

    if-eqz v1, :cond_0

    .line 2140
    invoke-virtual {v0}, Lcom/osp/app/signin/SignUpinfo;->a()V

    .line 2141
    const/4 v1, 0x0

    iput-boolean v1, p0, Lcom/osp/app/signin/SmsVerificationActivity;->L:Z

    .line 2146
    :cond_0
    iget-object v1, p0, Lcom/osp/app/signin/SmsVerificationActivity;->ah:Landroid/content/Intent;

    const-string v2, "key_internal_sign_up_inforamtion"

    invoke-virtual {v1, v2, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 2149
    :cond_1
    iget-object v0, p0, Lcom/osp/app/signin/SmsVerificationActivity;->ah:Landroid/content/Intent;

    const-string v1, "is_cancelable_just_one_activity"

    invoke-virtual {v0, v1, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 2152
    iget-object v0, p0, Lcom/osp/app/signin/SmsVerificationActivity;->ah:Landroid/content/Intent;

    const-string v1, "is_signup_flow"

    invoke-virtual {v0, v1, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 2155
    invoke-direct {p0}, Lcom/osp/app/signin/SmsVerificationActivity;->e()V

    .line 2157
    iget-object v0, p0, Lcom/osp/app/signin/SmsVerificationActivity;->ah:Landroid/content/Intent;

    const/16 v1, 0xce

    invoke-virtual {p0, v0, v1}, Lcom/osp/app/signin/SmsVerificationActivity;->startActivityForResult(Landroid/content/Intent;I)V

    .line 2158
    return-void
.end method

.method public final d()V
    .locals 3

    .prologue
    .line 4157
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v0, "SMSV"

    const-string v1, "CancelSignInDual_DB"

    const-string v2, "START"

    invoke-static {v0, v1, v2}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 4159
    invoke-static {p0}, Lcom/osp/app/util/ad;->d(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 4161
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v0, "SMSV"

    const-string v1, "cancel - CancelSignInDual_DB"

    invoke-static {v0, v1}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 4195
    :goto_0
    return-void

    .line 4167
    :cond_0
    const-string v0, ""

    const-string v1, ""

    const-string v2, ""

    invoke-static {p0, v0, v1, v2}, Lcom/msc/c/e;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 4172
    :try_start_0
    new-instance v0, Lcom/osp/security/identity/d;

    invoke-direct {v0, p0}, Lcom/osp/security/identity/d;-><init>(Landroid/content/Context;)V

    .line 4173
    invoke-virtual {v0}, Lcom/osp/security/identity/d;->k()V
    :try_end_0
    .catch Lcom/osp/security/identity/IdentityException; {:try_start_0 .. :try_end_0} :catch_0

    .line 4180
    :goto_1
    :try_start_1
    new-instance v0, Lcom/osp/security/credential/a;

    invoke-direct {v0, p0}, Lcom/osp/security/credential/a;-><init>(Landroid/content/Context;)V

    .line 4181
    invoke-virtual {v0}, Lcom/osp/security/credential/a;->a()V
    :try_end_1
    .catch Lcom/osp/security/credential/CredentialException; {:try_start_1 .. :try_end_1} :catch_1

    .line 4188
    :goto_2
    :try_start_2
    new-instance v0, Lcom/osp/social/member/d;

    invoke-direct {v0, p0}, Lcom/osp/social/member/d;-><init>(Landroid/content/Context;)V

    .line 4189
    invoke-virtual {v0}, Lcom/osp/social/member/d;->a()V
    :try_end_2
    .catch Lcom/osp/social/member/MemberServiceException; {:try_start_2 .. :try_end_2} :catch_2

    .line 4194
    :goto_3
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v0, "SMSV"

    const-string v1, "CancelSignInDual_DB"

    const-string v2, "END"

    invoke-static {v0, v1, v2}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 4174
    :catch_0
    move-exception v0

    invoke-virtual {v0}, Lcom/osp/security/identity/IdentityException;->printStackTrace()V

    goto :goto_1

    .line 4182
    :catch_1
    move-exception v0

    invoke-virtual {v0}, Lcom/osp/security/credential/CredentialException;->printStackTrace()V

    goto :goto_2

    .line 4190
    :catch_2
    move-exception v0

    invoke-virtual {v0}, Lcom/osp/social/member/MemberServiceException;->printStackTrace()V

    goto :goto_3
.end method

.method protected onActivityResult(IILandroid/content/Intent;)V
    .locals 2

    .prologue
    .line 1469
    invoke-super {p0, p1, p2, p3}, Lcom/osp/app/util/BaseActivity;->onActivityResult(IILandroid/content/Intent;)V

    .line 1471
    const/16 v0, 0xd0

    if-ne p1, v0, :cond_1

    .line 1473
    packed-switch p2, :pswitch_data_0

    .line 1542
    :cond_0
    :goto_0
    return-void

    .line 1476
    :pswitch_0
    const-string v0, "key_country_calling_code"

    invoke-virtual {p3, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/osp/app/signin/SmsVerificationActivity;->H:Ljava/lang/String;

    .line 1477
    const-string v0, "key_alpha2_country_code"

    invoke-virtual {p3, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/osp/app/signin/SmsVerificationActivity;->S:Ljava/lang/String;

    .line 1478
    const-string v0, "SelectCountry"

    invoke-virtual {p3, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 1482
    const v0, 0x7f0c015a

    invoke-virtual {p0, v0}, Lcom/osp/app/signin/SmsVerificationActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    .line 1483
    invoke-virtual {v0, v1}, Landroid/widget/Button;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0

    .line 1490
    :cond_1
    const/16 v0, 0xce

    if-ne p1, v0, :cond_2

    .line 1492
    sparse-switch p2, :sswitch_data_0

    .line 1509
    invoke-virtual {p0, p2, p3}, Lcom/osp/app/signin/SmsVerificationActivity;->a(ILandroid/content/Intent;)V

    .line 1510
    invoke-virtual {p0}, Lcom/osp/app/signin/SmsVerificationActivity;->finish()V

    goto :goto_0

    .line 1495
    :sswitch_0
    invoke-virtual {p0, p2, p3}, Lcom/osp/app/signin/SmsVerificationActivity;->a(ILandroid/content/Intent;)V

    .line 1496
    invoke-virtual {p0}, Lcom/osp/app/signin/SmsVerificationActivity;->finish()V

    goto :goto_0

    .line 1499
    :sswitch_1
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v0, "SMSV RESULT_FAIL_ACTIVATING_EMAIL_VALIDATION"

    invoke-static {v0}, Lcom/osp/app/util/aq;->b(Ljava/lang/String;)V

    .line 1500
    invoke-virtual {p0, p2}, Lcom/osp/app/signin/SmsVerificationActivity;->b(I)V

    .line 1501
    invoke-virtual {p0}, Lcom/osp/app/signin/SmsVerificationActivity;->finish()V

    goto :goto_0

    .line 1504
    :sswitch_2
    invoke-virtual {p0}, Lcom/osp/app/signin/SmsVerificationActivity;->getApplication()Landroid/app/Application;

    move-result-object v0

    check-cast v0, Lcom/osp/app/signin/SamsungService;

    invoke-virtual {v0, p0}, Lcom/osp/app/signin/SamsungService;->a(Landroid/app/Activity;)V

    .line 1505
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v0, "SMSV TnC Canceled"

    invoke-static {v0}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;)V

    goto :goto_0

    .line 1513
    :cond_2
    const/16 v0, 0xe2

    if-ne p1, v0, :cond_3

    .line 1515
    packed-switch p2, :pswitch_data_1

    goto :goto_0

    .line 1518
    :pswitch_1
    invoke-direct {p0}, Lcom/osp/app/signin/SmsVerificationActivity;->u()V

    goto :goto_0

    .line 1524
    :cond_3
    const/16 v0, 0xdb

    if-ne p1, v0, :cond_4

    .line 1526
    packed-switch p2, :pswitch_data_2

    goto :goto_0

    .line 1529
    :pswitch_2
    invoke-direct {p0}, Lcom/osp/app/signin/SmsVerificationActivity;->v()V

    goto :goto_0

    .line 1536
    :cond_4
    const/16 v0, 0xef

    if-ne p1, v0, :cond_0

    .line 1539
    const/4 v0, -0x1

    invoke-virtual {p0, v0}, Lcom/osp/app/signin/SmsVerificationActivity;->b(I)V

    .line 1540
    invoke-virtual {p0}, Lcom/osp/app/signin/SmsVerificationActivity;->finish()V

    goto :goto_0

    .line 1473
    :pswitch_data_0
    .packed-switch -0x1
        :pswitch_0
    .end packed-switch

    .line 1492
    :sswitch_data_0
    .sparse-switch
        -0x1 -> :sswitch_0
        0xd -> :sswitch_1
        0xe -> :sswitch_2
    .end sparse-switch

    .line 1515
    :pswitch_data_1
    .packed-switch -0x1
        :pswitch_1
    .end packed-switch

    .line 1526
    :pswitch_data_2
    .packed-switch -0x1
        :pswitch_2
    .end packed-switch
.end method

.method public onBackPressed()V
    .locals 3

    .prologue
    .line 763
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v0, "SMSV"

    const-string v1, "onBackPressed"

    invoke-static {v0, v1}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 765
    iget-object v0, p0, Lcom/osp/app/signin/SmsVerificationActivity;->ah:Landroid/content/Intent;

    const-string v1, "is_cancelable_just_one_activity"

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 767
    const/16 v0, 0xe

    invoke-virtual {p0, v0}, Lcom/osp/app/signin/SmsVerificationActivity;->b(I)V

    .line 770
    :cond_0
    invoke-super {p0}, Lcom/osp/app/util/BaseActivity;->onBackPressed()V

    .line 771
    return-void
.end method

.method public onClick(Landroid/view/View;)V
    .locals 4

    .prologue
    .line 1863
    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    .line 1925
    :goto_0
    return-void

    .line 1871
    :sswitch_0
    invoke-static {p0}, Lcom/osp/app/util/ad;->a(Landroid/content/Context;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 1873
    const/16 v0, 0xe2

    invoke-virtual {p0, v0}, Lcom/osp/app/signin/SmsVerificationActivity;->c(I)V

    goto :goto_0

    .line 1880
    :cond_0
    invoke-direct {p0}, Lcom/osp/app/signin/SmsVerificationActivity;->r()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/osp/app/signin/SmsVerificationActivity;->E:Ljava/lang/String;

    .line 1882
    const-string v0, "FROM_EDIT_PROFILE"

    iget-object v1, p0, Lcom/osp/app/signin/SmsVerificationActivity;->al:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_2

    const-string v0, "FROM_SIGN_IN_FLOW"

    iget-object v1, p0, Lcom/osp/app/signin/SmsVerificationActivity;->al:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_2

    const-string v0, "PUBLIC_SMS_FLOW"

    iget-object v1, p0, Lcom/osp/app/signin/SmsVerificationActivity;->al:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_2

    iget-object v0, p0, Lcom/osp/app/signin/SmsVerificationActivity;->E:Ljava/lang/String;

    iget-object v1, p0, Lcom/osp/app/signin/SmsVerificationActivity;->N:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/osp/app/signin/SmsVerificationActivity;->H:Ljava/lang/String;

    iget-object v1, p0, Lcom/osp/app/signin/SmsVerificationActivity;->O:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_2

    .line 1887
    :cond_1
    new-instance v0, Landroid/app/AlertDialog$Builder;

    invoke-direct {v0, p0}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    const v1, 0x7f09011b

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    const v1, 0x7f09011a

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setMessage(I)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    const v1, 0x7f090064

    new-instance v2, Lcom/osp/app/signin/ms;

    invoke-direct {v2, p0}, Lcom/osp/app/signin/ms;-><init>(Lcom/osp/app/signin/SmsVerificationActivity;)V

    invoke-virtual {v0, v1, v2}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    const v1, 0x7f090019

    new-instance v2, Lcom/osp/app/signin/mr;

    invoke-direct {v2, p0}, Lcom/osp/app/signin/mr;-><init>(Lcom/osp/app/signin/SmsVerificationActivity;)V

    invoke-virtual {v0, v1, v2}, Landroid/app/AlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    new-instance v1, Lcom/osp/app/signin/mq;

    invoke-direct {v1, p0}, Lcom/osp/app/signin/mq;-><init>(Lcom/osp/app/signin/SmsVerificationActivity;)V

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setOnCancelListener(Landroid/content/DialogInterface$OnCancelListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v0

    .line 1914
    invoke-virtual {v0}, Landroid/app/AlertDialog;->show()V

    goto :goto_0

    .line 1918
    :cond_2
    invoke-direct {p0}, Lcom/osp/app/signin/SmsVerificationActivity;->u()V

    goto/16 :goto_0

    .line 1924
    :sswitch_1
    const v0, 0x7f0c015a

    invoke-virtual {p0, v0}, Lcom/osp/app/signin/SmsVerificationActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    invoke-virtual {v0}, Landroid/widget/Button;->getText()Ljava/lang/CharSequence;

    move-result-object v0

    invoke-interface {v0}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v0

    new-instance v1, Landroid/content/Intent;

    invoke-direct {v1}, Landroid/content/Intent;-><init>()V

    const-class v2, Lcom/osp/app/signin/SelectCountryView;

    invoke-virtual {v1, p0, v2}, Landroid/content/Intent;->setClass(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;

    const-string v2, "key_country_list_mode"

    const-string v3, "country_calling_code_list"

    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    const-string v2, "key_default_country_name"

    invoke-virtual {v1, v2, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    const/16 v0, 0xd0

    invoke-virtual {p0, v1, v0}, Lcom/osp/app/signin/SmsVerificationActivity;->startActivityForResult(Landroid/content/Intent;I)V

    goto/16 :goto_0

    .line 1863
    nop

    :sswitch_data_0
    .sparse-switch
        0x7f0c00c7 -> :sswitch_0
        0x7f0c015a -> :sswitch_1
    .end sparse-switch
.end method

.method public onConfigurationChanged(Landroid/content/res/Configuration;)V
    .locals 1

    .prologue
    .line 1161
    invoke-super {p0, p1}, Lcom/osp/app/util/BaseActivity;->onConfigurationChanged(Landroid/content/res/Configuration;)V

    .line 1163
    iget v0, p1, Landroid/content/res/Configuration;->orientation:I

    invoke-direct {p0, v0}, Lcom/osp/app/signin/SmsVerificationActivity;->a(I)V

    .line 1164
    return-void
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 8

    .prologue
    const v7, 0x7f09006b

    const/4 v6, 0x2

    const v3, 0x7f070006

    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 651
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v0, "SMSV"

    const-string v1, "onCreate"

    invoke-static {v0, v1}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 653
    invoke-super {p0, p1}, Lcom/osp/app/util/BaseActivity;->onCreate(Landroid/os/Bundle;)V

    .line 655
    invoke-virtual {p0}, Lcom/osp/app/signin/SmsVerificationActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    iput-object v0, p0, Lcom/osp/app/signin/SmsVerificationActivity;->ah:Landroid/content/Intent;

    .line 657
    invoke-static {p0}, Lcom/osp/app/util/ad;->d(Landroid/content/Context;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 659
    iget-object v0, p0, Lcom/osp/app/signin/SmsVerificationActivity;->ah:Landroid/content/Intent;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/osp/app/signin/SmsVerificationActivity;->ah:Landroid/content/Intent;

    const-string v1, "is_signup_flow"

    invoke-virtual {v0, v1, v4}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 661
    sget-object v0, Lcom/osp/app/loggingservice/c;->e:Lcom/osp/app/loggingservice/c;

    invoke-static {v0}, Lcom/osp/app/loggingservice/LoggingService;->a(Lcom/osp/app/loggingservice/c;)V

    .line 665
    :cond_0
    const v0, 0x7f030034

    invoke-static {p0}, Lcom/osp/app/util/r;->q(Landroid/content/Context;)Z

    move-result v1

    invoke-virtual {p0, v0, v1}, Lcom/osp/app/signin/SmsVerificationActivity;->a(IZ)V

    .line 667
    invoke-static {p0}, Lcom/osp/app/util/r;->i(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 669
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x13

    if-ge v0, v1, :cond_1

    .line 671
    invoke-static {}, Lcom/osp/device/b;->b()Lcom/osp/device/b;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/osp/device/b;->i(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 673
    invoke-static {p0}, Lcom/msc/sa/c/d;->c(Landroid/app/Activity;)V

    .line 699
    :cond_1
    invoke-static {p0}, Lcom/msc/sa/c/d;->a(Landroid/app/Activity;)V

    .line 701
    invoke-virtual {p0}, Lcom/osp/app/signin/SmsVerificationActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v0, 0x7f0c00c1

    invoke-virtual {p0, v0}, Lcom/osp/app/signin/SmsVerificationActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    if-eqz v0, :cond_2

    invoke-static {p0}, Lcom/osp/app/util/an;->a(Lcom/osp/app/util/BaseActivity;)Lcom/osp/app/util/an;

    invoke-static {v3}, Lcom/osp/app/util/an;->a(I)I

    move-result v2

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v2

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setTextColor(I)V

    :cond_2
    const v0, 0x7f0c00c2

    invoke-virtual {p0, v0}, Lcom/osp/app/signin/SmsVerificationActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    if-eqz v0, :cond_3

    invoke-static {p0}, Lcom/osp/app/util/an;->a(Lcom/osp/app/util/BaseActivity;)Lcom/osp/app/util/an;

    invoke-static {v3}, Lcom/osp/app/util/an;->a(I)I

    move-result v2

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v2

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setTextColor(I)V

    :cond_3
    const v0, 0x7f0c00c5

    invoke-virtual {p0, v0}, Lcom/osp/app/signin/SmsVerificationActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    if-eqz v0, :cond_4

    invoke-static {p0}, Lcom/osp/app/util/an;->a(Lcom/osp/app/util/BaseActivity;)Lcom/osp/app/util/an;

    invoke-static {v3}, Lcom/osp/app/util/an;->a(I)I

    move-result v2

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setTextColor(I)V

    :cond_4
    const v0, 0x7f0c0065

    invoke-virtual {p0, v0}, Lcom/osp/app/signin/SmsVerificationActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/osp/app/signin/BottomSoftkeyLinearLayout;

    iput-object v0, p0, Lcom/osp/app/signin/SmsVerificationActivity;->ar:Lcom/osp/app/signin/BottomSoftkeyLinearLayout;

    iget-object v0, p0, Lcom/osp/app/signin/SmsVerificationActivity;->ar:Lcom/osp/app/signin/BottomSoftkeyLinearLayout;

    if-eqz v0, :cond_5

    iget-object v0, p0, Lcom/osp/app/signin/SmsVerificationActivity;->ar:Lcom/osp/app/signin/BottomSoftkeyLinearLayout;

    invoke-virtual {p0}, Lcom/osp/app/signin/SmsVerificationActivity;->h()Z

    move-result v1

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-virtual {v0, p0, v1}, Lcom/osp/app/signin/BottomSoftkeyLinearLayout;->a(Landroid/content/Context;Ljava/lang/Boolean;)V

    iget-object v0, p0, Lcom/osp/app/signin/SmsVerificationActivity;->ar:Lcom/osp/app/signin/BottomSoftkeyLinearLayout;

    iget-object v1, p0, Lcom/osp/app/signin/SmsVerificationActivity;->ar:Lcom/osp/app/signin/BottomSoftkeyLinearLayout;

    const-string v1, "one_button"

    invoke-virtual {v0, v1}, Lcom/osp/app/signin/BottomSoftkeyLinearLayout;->a(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/osp/app/signin/SmsVerificationActivity;->ar:Lcom/osp/app/signin/BottomSoftkeyLinearLayout;

    invoke-virtual {p0, v7}, Lcom/osp/app/signin/SmsVerificationActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/osp/app/signin/BottomSoftkeyLinearLayout;->c(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/osp/app/signin/SmsVerificationActivity;->ar:Lcom/osp/app/signin/BottomSoftkeyLinearLayout;

    invoke-virtual {p0, v7}, Lcom/osp/app/signin/SmsVerificationActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/osp/app/signin/BottomSoftkeyLinearLayout;->e(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/osp/app/signin/SmsVerificationActivity;->ar:Lcom/osp/app/signin/BottomSoftkeyLinearLayout;

    iget-object v1, p0, Lcom/osp/app/signin/SmsVerificationActivity;->d:Landroid/view/View$OnClickListener;

    invoke-virtual {v0, v1}, Lcom/osp/app/signin/BottomSoftkeyLinearLayout;->b(Landroid/view/View$OnClickListener;)V

    :cond_5
    invoke-static {}, Lcom/osp/app/signin/SamsungService;->a()Z

    move-result v0

    if-nez v0, :cond_6

    invoke-static {}, Lcom/osp/device/b;->b()Lcom/osp/device/b;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/osp/device/b;->i(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_6

    iget-object v0, p0, Lcom/osp/app/signin/SmsVerificationActivity;->ar:Lcom/osp/app/signin/BottomSoftkeyLinearLayout;

    if-eqz v0, :cond_6

    iget-object v0, p0, Lcom/osp/app/signin/SmsVerificationActivity;->ar:Lcom/osp/app/signin/BottomSoftkeyLinearLayout;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Lcom/osp/app/signin/BottomSoftkeyLinearLayout;->setVisibility(I)V

    :cond_6
    const v0, 0x7f0c00c7

    invoke-virtual {p0, v0}, Lcom/osp/app/signin/SmsVerificationActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    if-eqz v0, :cond_7

    invoke-static {p0}, Lcom/osp/app/util/an;->a(Lcom/osp/app/util/BaseActivity;)Lcom/osp/app/util/an;

    const v1, 0x7f0200c5

    invoke-static {v1}, Lcom/osp/app/util/an;->a(I)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageResource(I)V

    invoke-static {p0, v0}, Lcom/msc/sa/c/d;->a(Landroid/content/Context;Landroid/view/View;)V

    invoke-static {v0}, Lcom/msc/sa/c/d;->a(Landroid/view/View;)V

    .line 702
    :cond_7
    invoke-virtual {p0}, Lcom/osp/app/signin/SmsVerificationActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v0

    iget v0, v0, Landroid/content/res/Configuration;->orientation:I

    invoke-direct {p0, v0}, Lcom/osp/app/signin/SmsVerificationActivity;->a(I)V

    const-string v0, "FROM_EDIT_PROFILE"

    invoke-virtual {p0}, Lcom/osp/app/signin/SmsVerificationActivity;->getIntent()Landroid/content/Intent;

    move-result-object v1

    const-string v2, "is_phnumber_verification_mode"

    invoke-virtual {v1, v2}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_8

    const-string v0, "FROM_SIGN_IN_FLOW"

    invoke-virtual {p0}, Lcom/osp/app/signin/SmsVerificationActivity;->getIntent()Landroid/content/Intent;

    move-result-object v1

    const-string v2, "is_phnumber_verification_mode"

    invoke-virtual {v1, v2}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_9

    :cond_8
    const v0, 0x7f0c00c1

    invoke-virtual {p0, v0}, Lcom/osp/app/signin/SmsVerificationActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    const v1, 0x7f090102

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    :cond_9
    const v0, 0x7f0c00c5

    invoke-virtual {p0, v0}, Lcom/osp/app/signin/SmsVerificationActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    if-eqz v0, :cond_a

    const v1, 0x7f090134

    invoke-virtual {p0, v1}, Lcom/osp/app/signin/SmsVerificationActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    new-array v2, v5, [Ljava/lang/Object;

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v2, v4

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    :cond_a
    const v0, 0x7f0c00c3

    invoke-virtual {p0, v0}, Lcom/osp/app/signin/SmsVerificationActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/EditText;

    if-eqz v0, :cond_b

    invoke-virtual {v0, v6}, Landroid/widget/EditText;->setInputType(I)V

    new-instance v1, Lcom/osp/app/signin/ni;

    invoke-direct {v1, p0}, Lcom/osp/app/signin/ni;-><init>(Lcom/osp/app/signin/SmsVerificationActivity;)V

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->addTextChangedListener(Landroid/text/TextWatcher;)V

    :cond_b
    const v0, 0x7f0c00c4

    invoke-virtual {p0, v0}, Lcom/osp/app/signin/SmsVerificationActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/EditText;

    if-eqz v0, :cond_c

    invoke-virtual {v0, v6}, Landroid/widget/EditText;->setInputType(I)V

    new-array v1, v5, [Landroid/text/InputFilter;

    new-instance v2, Landroid/text/InputFilter$LengthFilter;

    const/4 v3, 0x6

    invoke-direct {v2, v3}, Landroid/text/InputFilter$LengthFilter;-><init>(I)V

    aput-object v2, v1, v4

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setFilters([Landroid/text/InputFilter;)V

    new-instance v1, Lcom/osp/app/signin/nj;

    invoke-direct {v1, p0}, Lcom/osp/app/signin/nj;-><init>(Lcom/osp/app/signin/SmsVerificationActivity;)V

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->addTextChangedListener(Landroid/text/TextWatcher;)V

    new-instance v1, Lcom/osp/app/signin/nk;

    invoke-direct {v1, p0, v0}, Lcom/osp/app/signin/nk;-><init>(Lcom/osp/app/signin/SmsVerificationActivity;Landroid/widget/EditText;)V

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setOnEditorActionListener(Landroid/widget/TextView$OnEditorActionListener;)V

    :cond_c
    invoke-direct {p0}, Lcom/osp/app/signin/SmsVerificationActivity;->e()V

    const v0, 0x7f0c015a

    invoke-virtual {p0, v0}, Lcom/osp/app/signin/SmsVerificationActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    if-eqz v0, :cond_d

    invoke-virtual {v0, p0}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    :cond_d
    const v0, 0x7f0c00c7

    invoke-virtual {p0, v0}, Lcom/osp/app/signin/SmsVerificationActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    if-eqz v0, :cond_e

    invoke-virtual {v0, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    :cond_e
    const v0, 0x7f0c00c6

    invoke-virtual {p0, v0}, Lcom/osp/app/signin/SmsVerificationActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    if-eqz v0, :cond_f

    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    :cond_f
    invoke-direct {p0, v4}, Lcom/osp/app/signin/SmsVerificationActivity;->a(Z)V

    .line 703
    iget-object v0, p0, Lcom/osp/app/signin/SmsVerificationActivity;->ah:Landroid/content/Intent;

    const-string v1, "country_code_mcc"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/osp/app/signin/SmsVerificationActivity;->D:Ljava/lang/String;

    iget-object v0, p0, Lcom/osp/app/signin/SmsVerificationActivity;->ah:Landroid/content/Intent;

    const-string v1, "is_phnumber_verification_mode"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/osp/app/signin/SmsVerificationActivity;->al:Ljava/lang/String;

    invoke-virtual {p0}, Lcom/osp/app/signin/SmsVerificationActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string v1, "required_auth"

    invoke-virtual {v0, v1, v4}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v0

    iput-boolean v0, p0, Lcom/osp/app/signin/SmsVerificationActivity;->A:Z

    invoke-virtual {p0}, Lcom/osp/app/signin/SmsVerificationActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string v1, "key_internal_is_resign_in"

    invoke-virtual {v0, v1, v4}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v0

    iput-boolean v0, p0, Lcom/osp/app/signin/SmsVerificationActivity;->ag:Z

    invoke-static {}, Lcom/osp/app/util/ac;->a()Lcom/osp/app/util/ac;

    invoke-static {p0}, Lcom/osp/app/util/ac;->a(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/osp/app/signin/SmsVerificationActivity;->aq:Ljava/lang/String;

    invoke-static {}, Lcom/osp/app/util/ac;->a()Lcom/osp/app/util/ac;

    invoke-static {p0}, Lcom/osp/app/util/ac;->c(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/osp/app/signin/SmsVerificationActivity;->ap:Ljava/lang/String;

    invoke-static {}, Lcom/osp/device/b;->b()Lcom/osp/device/b;

    move-result-object v0

    invoke-virtual {v0}, Lcom/osp/device/b;->a()Z

    move-result v0

    if-nez v0, :cond_10

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/osp/app/signin/SmsVerificationActivity;->al:Ljava/lang/String;

    :cond_10
    const-string v0, "FROM_EDIT_PROFILE"

    iget-object v1, p0, Lcom/osp/app/signin/SmsVerificationActivity;->al:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_15

    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v0, "SMSVCalled SMS get Intent"

    invoke-static {v0}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/osp/app/signin/SmsVerificationActivity;->ah:Landroid/content/Intent;

    const-string v1, "key_country_calling_code"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/osp/app/signin/SmsVerificationActivity;->H:Ljava/lang/String;

    iput-object v0, p0, Lcom/osp/app/signin/SmsVerificationActivity;->Q:Ljava/lang/String;

    iget-object v0, p0, Lcom/osp/app/signin/SmsVerificationActivity;->ah:Landroid/content/Intent;

    const-string v1, "key_phonenumber"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/osp/app/signin/SmsVerificationActivity;->E:Ljava/lang/String;

    iput-object v0, p0, Lcom/osp/app/signin/SmsVerificationActivity;->P:Ljava/lang/String;

    iget-object v0, p0, Lcom/osp/app/signin/SmsVerificationActivity;->H:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_11

    iget-object v0, p0, Lcom/osp/app/signin/SmsVerificationActivity;->E:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_13

    :cond_11
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v0, "SMSVPhone number is empty && Country Calling Code is empty"

    invoke-static {v0}, Lcom/osp/app/util/aq;->c(Ljava/lang/String;)V

    :cond_12
    :goto_0
    invoke-virtual {p0, v5}, Lcom/osp/app/signin/SmsVerificationActivity;->b(I)V

    invoke-virtual {p0}, Lcom/osp/app/signin/SmsVerificationActivity;->finish()V

    .line 705
    :cond_13
    :goto_1
    invoke-static {}, Lcom/osp/app/signin/SamsungService;->a()Z

    move-result v0

    if-nez v0, :cond_14

    invoke-static {}, Lcom/osp/device/b;->b()Lcom/osp/device/b;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/osp/device/b;->i(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_14

    .line 707
    const-string v0, "next"

    invoke-virtual {p0, v0}, Lcom/osp/app/signin/SmsVerificationActivity;->e(Ljava/lang/String;)V

    .line 708
    invoke-static {p0}, Lcom/msc/sa/c/d;->e(Landroid/app/Activity;)V

    .line 711
    :cond_14
    new-instance v0, Lcom/osp/app/signin/nl;

    invoke-direct {v0, p0}, Lcom/osp/app/signin/nl;-><init>(Lcom/osp/app/signin/SmsVerificationActivity;)V

    iput-object v0, p0, Lcom/osp/app/signin/SmsVerificationActivity;->Y:Lcom/osp/app/signin/nl;

    .line 712
    iget-object v0, p0, Lcom/osp/app/signin/SmsVerificationActivity;->Y:Lcom/osp/app/signin/nl;

    new-array v1, v4, [Ljava/lang/Void;

    invoke-virtual {v0, v1}, Lcom/osp/app/signin/nl;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    .line 713
    return-void

    .line 703
    :cond_15
    const-string v0, "FROM_SIGN_IN_FLOW"

    iget-object v1, p0, Lcom/osp/app/signin/SmsVerificationActivity;->al:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_18

    invoke-virtual {p0}, Lcom/osp/app/signin/SmsVerificationActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string v1, "key_country_calling_code"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/osp/app/signin/SmsVerificationActivity;->H:Ljava/lang/String;

    iput-object v0, p0, Lcom/osp/app/signin/SmsVerificationActivity;->Q:Ljava/lang/String;

    invoke-virtual {p0}, Lcom/osp/app/signin/SmsVerificationActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string v1, "key_phonenumber"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/osp/app/signin/SmsVerificationActivity;->E:Ljava/lang/String;

    iput-object v0, p0, Lcom/osp/app/signin/SmsVerificationActivity;->P:Ljava/lang/String;

    invoke-virtual {p0}, Lcom/osp/app/signin/SmsVerificationActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string v1, "key_duplicated_id_password"

    invoke-virtual {v0, v1, v4}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v0

    iput-boolean v0, p0, Lcom/osp/app/signin/SmsVerificationActivity;->aj:Z

    invoke-virtual {p0}, Lcom/osp/app/signin/SmsVerificationActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string v1, "client_id"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/osp/app/signin/SmsVerificationActivity;->I:Ljava/lang/String;

    invoke-virtual {p0}, Lcom/osp/app/signin/SmsVerificationActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string v1, "client_secret"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/osp/app/signin/SmsVerificationActivity;->J:Ljava/lang/String;

    invoke-virtual {p0}, Lcom/osp/app/signin/SmsVerificationActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string v1, "key_user_id"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/osp/app/signin/SmsVerificationActivity;->z:Ljava/lang/String;

    iget-object v0, p0, Lcom/osp/app/signin/SmsVerificationActivity;->I:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_16

    iget-object v0, p0, Lcom/osp/app/signin/SmsVerificationActivity;->J:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_17

    :cond_16
    const-string v0, "j5p7ll8g33"

    iput-object v0, p0, Lcom/osp/app/signin/SmsVerificationActivity;->I:Ljava/lang/String;

    const-string v0, "5763D0052DC1462E13751F753384E9A9"

    iput-object v0, p0, Lcom/osp/app/signin/SmsVerificationActivity;->J:Ljava/lang/String;

    :cond_17
    invoke-virtual {p0}, Lcom/osp/app/signin/SmsVerificationActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string v1, "key_password"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/osp/app/signin/SmsVerificationActivity;->M:Ljava/lang/String;

    iget-object v0, p0, Lcom/osp/app/signin/SmsVerificationActivity;->M:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_13

    goto/16 :goto_0

    :cond_18
    iget-object v0, p0, Lcom/osp/app/signin/SmsVerificationActivity;->ah:Landroid/content/Intent;

    const-string v1, "key_internal_sign_up_inforamtion"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/osp/app/signin/SignUpinfo;

    if-eqz v0, :cond_12

    invoke-virtual {v0}, Lcom/osp/app/signin/SignUpinfo;->F()Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_13

    invoke-virtual {v0}, Lcom/osp/app/signin/SignUpinfo;->F()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/osp/app/signin/SmsVerificationActivity;->E:Ljava/lang/String;

    invoke-virtual {v0}, Lcom/osp/app/signin/SignUpinfo;->F()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/osp/app/signin/SmsVerificationActivity;->N:Ljava/lang/String;

    invoke-virtual {v0}, Lcom/osp/app/signin/SignUpinfo;->E()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/osp/app/signin/SmsVerificationActivity;->O:Ljava/lang/String;

    invoke-virtual {v0}, Lcom/osp/app/signin/SignUpinfo;->E()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/osp/app/signin/SmsVerificationActivity;->H:Ljava/lang/String;

    goto/16 :goto_1
.end method

.method public onCreateDialog(I)Landroid/app/Dialog;
    .locals 3

    .prologue
    .line 834
    packed-switch p1, :pswitch_data_0

    .line 843
    invoke-super {p0, p1}, Lcom/osp/app/util/BaseActivity;->onCreateDialog(I)Landroid/app/Dialog;

    move-result-object v0

    :goto_0
    return-object v0

    .line 838
    :pswitch_0
    new-instance v0, Lcom/osp/app/signin/nh;

    invoke-direct {v0, p0}, Lcom/osp/app/signin/nh;-><init>(Lcom/osp/app/signin/SmsVerificationActivity;)V

    new-instance v1, Landroid/app/AlertDialog$Builder;

    invoke-direct {v1, p0}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    const v2, 0x7f09004f

    invoke-virtual {v1, v2}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    const v2, 0x7f090110

    invoke-virtual {v1, v2}, Landroid/app/AlertDialog$Builder;->setMessage(I)Landroid/app/AlertDialog$Builder;

    const v2, 0x7f090063

    invoke-virtual {v1, v2, v0}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    iget-object v0, p0, Lcom/osp/app/signin/SmsVerificationActivity;->c:Landroid/content/DialogInterface$OnCancelListener;

    invoke-virtual {v1, v0}, Landroid/app/AlertDialog$Builder;->setOnCancelListener(Landroid/content/DialogInterface$OnCancelListener;)Landroid/app/AlertDialog$Builder;

    invoke-virtual {v1}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v0

    goto :goto_0

    .line 834
    :pswitch_data_0
    .packed-switch 0x66
        :pswitch_0
    .end packed-switch
.end method

.method protected onDestroy()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 775
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v0, "SMSV"

    const-string v1, "onDestroy"

    invoke-static {v0, v1}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 776
    iget-object v0, p0, Lcom/osp/app/signin/SmsVerificationActivity;->Y:Lcom/osp/app/signin/nl;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/osp/app/signin/SmsVerificationActivity;->Y:Lcom/osp/app/signin/nl;

    invoke-virtual {v0}, Lcom/osp/app/signin/nl;->getStatus()Landroid/os/AsyncTask$Status;

    move-result-object v0

    sget-object v1, Landroid/os/AsyncTask$Status;->RUNNING:Landroid/os/AsyncTask$Status;

    if-ne v0, v1, :cond_0

    .line 778
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v0, "SMSV"

    const-string v1, "PreCountryListTask cancelTask"

    invoke-static {v0, v1}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 779
    iget-object v0, p0, Lcom/osp/app/signin/SmsVerificationActivity;->Y:Lcom/osp/app/signin/nl;

    invoke-virtual {v0}, Lcom/osp/app/signin/nl;->d()V

    .line 780
    iput-object v2, p0, Lcom/osp/app/signin/SmsVerificationActivity;->Y:Lcom/osp/app/signin/nl;

    .line 782
    :cond_0
    iget-object v0, p0, Lcom/osp/app/signin/SmsVerificationActivity;->X:Lcom/osp/app/signin/nm;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/osp/app/signin/SmsVerificationActivity;->X:Lcom/osp/app/signin/nm;

    invoke-virtual {v0}, Lcom/osp/app/signin/nm;->getStatus()Landroid/os/AsyncTask$Status;

    move-result-object v0

    sget-object v1, Landroid/os/AsyncTask$Status;->RUNNING:Landroid/os/AsyncTask$Status;

    if-ne v0, v1, :cond_1

    .line 784
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v0, "SMSV"

    const-string v1, "SMSAuthenticateTask cancelTask"

    invoke-static {v0, v1}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 785
    iget-object v0, p0, Lcom/osp/app/signin/SmsVerificationActivity;->X:Lcom/osp/app/signin/nm;

    invoke-virtual {v0}, Lcom/osp/app/signin/nm;->d()V

    .line 786
    iput-object v2, p0, Lcom/osp/app/signin/SmsVerificationActivity;->X:Lcom/osp/app/signin/nm;

    .line 788
    :cond_1
    iget-object v0, p0, Lcom/osp/app/signin/SmsVerificationActivity;->Z:Lcom/osp/app/signin/ns;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/osp/app/signin/SmsVerificationActivity;->Z:Lcom/osp/app/signin/ns;

    invoke-virtual {v0}, Lcom/osp/app/signin/ns;->getStatus()Landroid/os/AsyncTask$Status;

    move-result-object v0

    sget-object v1, Landroid/os/AsyncTask$Status;->RUNNING:Landroid/os/AsyncTask$Status;

    if-ne v0, v1, :cond_2

    .line 790
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v0, "SMSV"

    const-string v1, "mUpdateUserLoginIDTask cancelTask"

    invoke-static {v0, v1}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 791
    iget-object v0, p0, Lcom/osp/app/signin/SmsVerificationActivity;->Z:Lcom/osp/app/signin/ns;

    invoke-virtual {v0}, Lcom/osp/app/signin/ns;->d()V

    .line 792
    iput-object v2, p0, Lcom/osp/app/signin/SmsVerificationActivity;->Z:Lcom/osp/app/signin/ns;

    .line 795
    :cond_2
    invoke-super {p0}, Lcom/osp/app/util/BaseActivity;->onDestroy()V

    .line 796
    return-void
.end method

.method public onOptionsItemSelected(Landroid/view/MenuItem;)Z
    .locals 2

    .prologue
    .line 1658
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0xb

    if-lt v0, v1, :cond_0

    .line 1660
    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    .line 1671
    const/4 v0, 0x0

    .line 1674
    :goto_0
    return v0

    .line 1663
    :sswitch_0
    invoke-virtual {p0}, Lcom/osp/app/signin/SmsVerificationActivity;->onBackPressed()V

    .line 1674
    :cond_0
    :goto_1
    const/4 v0, 0x1

    goto :goto_0

    .line 1667
    :sswitch_1
    invoke-direct {p0}, Lcom/osp/app/signin/SmsVerificationActivity;->w()V

    goto :goto_1

    .line 1660
    :sswitch_data_0
    .sparse-switch
        0x102002c -> :sswitch_0
        0x7f0c0196 -> :sswitch_1
    .end sparse-switch
.end method

.method protected onPause()V
    .locals 2

    .prologue
    .line 748
    iget-object v0, p0, Lcom/osp/app/signin/SmsVerificationActivity;->as:Landroid/content/BroadcastReceiver;

    invoke-virtual {p0, v0}, Lcom/osp/app/signin/SmsVerificationActivity;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    .line 750
    iget-object v0, p0, Lcom/osp/app/signin/SmsVerificationActivity;->R:Lcom/osp/app/signin/no;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/osp/app/signin/SmsVerificationActivity;->R:Lcom/osp/app/signin/no;

    invoke-virtual {v0}, Lcom/osp/app/signin/no;->getStatus()Landroid/os/AsyncTask$Status;

    move-result-object v0

    sget-object v1, Landroid/os/AsyncTask$Status;->RUNNING:Landroid/os/AsyncTask$Status;

    if-ne v0, v1, :cond_0

    .line 752
    invoke-static {}, Lcom/osp/app/util/ap;->a()Lcom/osp/app/util/ap;

    const v0, 0x7f09004a

    invoke-virtual {p0, v0}, Lcom/osp/app/signin/SmsVerificationActivity;->getString(I)Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x1

    invoke-static {p0, v0, v1}, Lcom/osp/app/util/ap;->a(Landroid/content/Context;Ljava/lang/String;I)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    .line 753
    iget-object v0, p0, Lcom/osp/app/signin/SmsVerificationActivity;->R:Lcom/osp/app/signin/no;

    invoke-virtual {v0}, Lcom/osp/app/signin/no;->d()V

    .line 754
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/osp/app/signin/SmsVerificationActivity;->R:Lcom/osp/app/signin/no;

    .line 757
    :cond_0
    invoke-super {p0}, Lcom/osp/app/util/BaseActivity;->onPause()V

    .line 758
    return-void
.end method

.method protected onResume()V
    .locals 2

    .prologue
    .line 718
    new-instance v0, Landroid/content/IntentFilter;

    const-string v1, "android.provider.Telephony.SMS_RECEIVED"

    invoke-direct {v0, v1}, Landroid/content/IntentFilter;-><init>(Ljava/lang/String;)V

    .line 719
    iget-object v1, p0, Lcom/osp/app/signin/SmsVerificationActivity;->as:Landroid/content/BroadcastReceiver;

    invoke-virtual {p0, v1, v0}, Lcom/osp/app/signin/SmsVerificationActivity;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    .line 723
    iget-object v0, p0, Lcom/osp/app/signin/SmsVerificationActivity;->al:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-static {p0}, Lcom/osp/app/util/ad;->d(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 735
    invoke-virtual {p0}, Lcom/osp/app/signin/SmsVerificationActivity;->finish()V

    .line 738
    :cond_0
    invoke-virtual {p0}, Lcom/osp/app/signin/SmsVerificationActivity;->isFinishing()Z

    move-result v0

    if-nez v0, :cond_1

    .line 740
    invoke-direct {p0}, Lcom/osp/app/signin/SmsVerificationActivity;->x()V

    .line 742
    :cond_1
    invoke-super {p0}, Lcom/osp/app/util/BaseActivity;->onResume()V

    .line 743
    return-void
.end method
