.class final Lcom/osp/app/signin/ml;
.super Ljava/lang/Object;
.source "SignUpWithFacebookWebView.java"


# instance fields
.field final synthetic a:Lcom/osp/app/signin/SignUpWithFacebookWebView;


# direct methods
.method private constructor <init>(Lcom/osp/app/signin/SignUpWithFacebookWebView;)V
    .locals 0

    .prologue
    .line 353
    iput-object p1, p0, Lcom/osp/app/signin/ml;->a:Lcom/osp/app/signin/SignUpWithFacebookWebView;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/osp/app/signin/SignUpWithFacebookWebView;B)V
    .locals 0

    .prologue
    .line 353
    invoke-direct {p0, p1}, Lcom/osp/app/signin/ml;-><init>(Lcom/osp/app/signin/SignUpWithFacebookWebView;)V

    return-void
.end method


# virtual methods
.method public final processJson(Ljava/lang/String;)V
    .locals 4
    .annotation runtime Landroid/webkit/JavascriptInterface;
    .end annotation

    .prologue
    .line 357
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "JSON content : "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;)V

    .line 360
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    .line 363
    :try_start_0
    new-instance v1, Lorg/json/JSONObject;

    invoke-direct {v1, p1}, Lorg/json/JSONObject;-><init>(Ljava/lang/String;)V

    const-string v2, "result"

    invoke-virtual {v1, v2}, Lorg/json/JSONObject;->getJSONObject(Ljava/lang/String;)Lorg/json/JSONObject;

    move-result-object v1

    .line 365
    const-string v2, "email"

    invoke-virtual {v1, v2}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 367
    const-string v2, "email"

    const-string v3, "email"

    invoke-virtual {v1, v3}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 369
    :cond_0
    const-string v2, "birthday"

    invoke-virtual {v1, v2}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 371
    const-string v2, "birthday"

    const-string v3, "birthday"

    invoke-virtual {v1, v3}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 373
    :cond_1
    const-string v2, "last_name"

    invoke-virtual {v1, v2}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 375
    const-string v2, "family_name"

    const-string v3, "last_name"

    invoke-virtual {v1, v3}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 377
    :cond_2
    const-string v2, "first_name"

    invoke-virtual {v1, v2}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_3

    .line 379
    const-string v2, "given_name"

    const-string v3, "first_name"

    invoke-virtual {v1, v3}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 383
    :cond_3
    const-string v2, "phone_number"

    invoke-virtual {v1, v2}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_4

    .line 385
    const-string v2, "phone_number"

    const-string v3, "phone_number"

    invoke-virtual {v1, v3}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v2, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 390
    :cond_4
    iget-object v1, p0, Lcom/osp/app/signin/ml;->a:Lcom/osp/app/signin/SignUpWithFacebookWebView;

    const/4 v2, 0x1

    invoke-virtual {v1, v2, v0}, Lcom/osp/app/signin/SignUpWithFacebookWebView;->setResult(ILandroid/content/Intent;)V
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    .line 397
    :goto_0
    iget-object v0, p0, Lcom/osp/app/signin/ml;->a:Lcom/osp/app/signin/SignUpWithFacebookWebView;

    invoke-virtual {v0}, Lcom/osp/app/signin/SignUpWithFacebookWebView;->finish()V

    .line 398
    return-void

    .line 391
    :catch_0
    move-exception v0

    .line 393
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "JSON parsing error message : "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Lorg/json/JSONException;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/osp/app/util/aq;->b(Ljava/lang/String;)V

    .line 394
    iget-object v0, p0, Lcom/osp/app/signin/ml;->a:Lcom/osp/app/signin/SignUpWithFacebookWebView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/osp/app/signin/SignUpWithFacebookWebView;->setResult(I)V

    goto :goto_0
.end method
