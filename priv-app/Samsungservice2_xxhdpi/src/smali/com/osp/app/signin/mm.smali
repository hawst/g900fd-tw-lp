.class final Lcom/osp/app/signin/mm;
.super Landroid/webkit/WebViewClient;
.source "SignUpWithFacebookWebView.java"


# instance fields
.field final synthetic a:Lcom/osp/app/signin/SignUpWithFacebookWebView;


# direct methods
.method private constructor <init>(Lcom/osp/app/signin/SignUpWithFacebookWebView;)V
    .locals 0

    .prologue
    .line 185
    iput-object p1, p0, Lcom/osp/app/signin/mm;->a:Lcom/osp/app/signin/SignUpWithFacebookWebView;

    invoke-direct {p0}, Landroid/webkit/WebViewClient;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/osp/app/signin/SignUpWithFacebookWebView;B)V
    .locals 0

    .prologue
    .line 185
    invoke-direct {p0, p1}, Lcom/osp/app/signin/mm;-><init>(Lcom/osp/app/signin/SignUpWithFacebookWebView;)V

    return-void
.end method


# virtual methods
.method public final onPageFinished(Landroid/webkit/WebView;Ljava/lang/String;)V
    .locals 3

    .prologue
    .line 285
    :try_start_0
    const-string v0, "/mobile/account/facebookUserInfoResult.do?"

    invoke-virtual {p2, v0}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 287
    iget-object v0, p0, Lcom/osp/app/signin/mm;->a:Lcom/osp/app/signin/SignUpWithFacebookWebView;

    invoke-static {v0}, Lcom/osp/app/signin/SignUpWithFacebookWebView;->c(Lcom/osp/app/signin/SignUpWithFacebookWebView;)Landroid/webkit/WebView;

    move-result-object v0

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/webkit/WebView;->setVisibility(I)V

    .line 288
    const-string v0, "javascript:window.JSONOUT.processJson(document.getElementsByTagName(\'body\')[0].innerHTML);"

    invoke-virtual {p1, v0}, Landroid/webkit/WebView;->loadUrl(Ljava/lang/String;)V

    .line 289
    iget-object v0, p0, Lcom/osp/app/signin/mm;->a:Lcom/osp/app/signin/SignUpWithFacebookWebView;

    invoke-static {v0}, Lcom/osp/app/signin/SignUpWithFacebookWebView;->a(Lcom/osp/app/signin/SignUpWithFacebookWebView;)Landroid/app/ProgressDialog;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/ProgressDialog;->dismiss()V

    .line 290
    iget-object v0, p0, Lcom/osp/app/signin/mm;->a:Lcom/osp/app/signin/SignUpWithFacebookWebView;

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lcom/osp/app/signin/SignUpWithFacebookWebView;->a(Lcom/osp/app/signin/SignUpWithFacebookWebView;Landroid/app/ProgressDialog;)Landroid/app/ProgressDialog;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 313
    :cond_0
    :goto_0
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "onPageFinished url : "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;)V

    .line 315
    invoke-super {p0, p1, p2}, Landroid/webkit/WebViewClient;->onPageFinished(Landroid/webkit/WebView;Ljava/lang/String;)V

    .line 316
    return-void

    .line 293
    :cond_1
    :try_start_1
    iget-object v0, p0, Lcom/osp/app/signin/mm;->a:Lcom/osp/app/signin/SignUpWithFacebookWebView;

    invoke-virtual {v0}, Lcom/osp/app/signin/SignUpWithFacebookWebView;->isFinishing()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/osp/app/signin/mm;->a:Lcom/osp/app/signin/SignUpWithFacebookWebView;

    invoke-static {v0}, Lcom/osp/app/signin/SignUpWithFacebookWebView;->a(Lcom/osp/app/signin/SignUpWithFacebookWebView;)Landroid/app/ProgressDialog;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/osp/app/signin/mm;->a:Lcom/osp/app/signin/SignUpWithFacebookWebView;

    invoke-static {v0}, Lcom/osp/app/signin/SignUpWithFacebookWebView;->a(Lcom/osp/app/signin/SignUpWithFacebookWebView;)Landroid/app/ProgressDialog;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/ProgressDialog;->isShowing()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 295
    iget-object v0, p0, Lcom/osp/app/signin/mm;->a:Lcom/osp/app/signin/SignUpWithFacebookWebView;

    invoke-static {v0}, Lcom/osp/app/signin/SignUpWithFacebookWebView;->c(Lcom/osp/app/signin/SignUpWithFacebookWebView;)Landroid/webkit/WebView;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/webkit/WebView;->setVisibility(I)V

    .line 296
    iget-object v0, p0, Lcom/osp/app/signin/mm;->a:Lcom/osp/app/signin/SignUpWithFacebookWebView;

    invoke-static {v0}, Lcom/osp/app/signin/SignUpWithFacebookWebView;->a(Lcom/osp/app/signin/SignUpWithFacebookWebView;)Landroid/app/ProgressDialog;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/ProgressDialog;->dismiss()V

    .line 297
    iget-object v0, p0, Lcom/osp/app/signin/mm;->a:Lcom/osp/app/signin/SignUpWithFacebookWebView;

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lcom/osp/app/signin/SignUpWithFacebookWebView;->a(Lcom/osp/app/signin/SignUpWithFacebookWebView;Landroid/app/ProgressDialog;)Landroid/app/ProgressDialog;

    .line 298
    const-string v0, "facebook.com/login.php?"

    invoke-virtual {p2, v0}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 300
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v0, "SUWFWV"

    const-string v1, "Display facebook login page"

    invoke-static {v0, v1}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 301
    iget-object v0, p0, Lcom/osp/app/signin/mm;->a:Lcom/osp/app/signin/SignUpWithFacebookWebView;

    invoke-static {v0}, Lcom/osp/app/signin/SignUpWithFacebookWebView;->c(Lcom/osp/app/signin/SignUpWithFacebookWebView;)Landroid/webkit/WebView;

    move-result-object v0

    invoke-virtual {v0}, Landroid/webkit/WebView;->clearHistory()V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_0

    .line 309
    :catch_0
    move-exception v0

    .line 311
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "ProgressDialog error message : "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/osp/app/util/aq;->b(Ljava/lang/String;)V

    goto/16 :goto_0

    .line 302
    :cond_2
    :try_start_2
    const-string v0, "facebook.com/dialog/oauth?"

    invoke-virtual {p2, v0}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 304
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v0, "SUWFWV"

    const-string v1, "go redirect url to oauth"

    invoke-static {v0, v1}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_0

    goto/16 :goto_0
.end method

.method public final onPageStarted(Landroid/webkit/WebView;Ljava/lang/String;Landroid/graphics/Bitmap;)V
    .locals 4

    .prologue
    .line 211
    iget-object v0, p0, Lcom/osp/app/signin/mm;->a:Lcom/osp/app/signin/SignUpWithFacebookWebView;

    invoke-static {v0}, Lcom/osp/app/signin/SignUpWithFacebookWebView;->a(Lcom/osp/app/signin/SignUpWithFacebookWebView;)Landroid/app/ProgressDialog;

    move-result-object v0

    if-nez v0, :cond_1

    .line 213
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0xd

    if-le v0, v1, :cond_2

    .line 215
    const/4 v0, 0x4

    .line 216
    iget-object v1, p0, Lcom/osp/app/signin/mm;->a:Lcom/osp/app/signin/SignUpWithFacebookWebView;

    invoke-static {v1}, Lcom/osp/app/signin/SignUpWithFacebookWebView;->b(Lcom/osp/app/signin/SignUpWithFacebookWebView;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 218
    const/4 v0, 0x5

    .line 220
    :cond_0
    iget-object v1, p0, Lcom/osp/app/signin/mm;->a:Lcom/osp/app/signin/SignUpWithFacebookWebView;

    new-instance v2, Landroid/app/ProgressDialog;

    iget-object v3, p0, Lcom/osp/app/signin/mm;->a:Lcom/osp/app/signin/SignUpWithFacebookWebView;

    invoke-direct {v2, v3, v0}, Landroid/app/ProgressDialog;-><init>(Landroid/content/Context;I)V

    invoke-static {v1, v2}, Lcom/osp/app/signin/SignUpWithFacebookWebView;->a(Lcom/osp/app/signin/SignUpWithFacebookWebView;Landroid/app/ProgressDialog;)Landroid/app/ProgressDialog;

    .line 225
    :goto_0
    iget-object v0, p0, Lcom/osp/app/signin/mm;->a:Lcom/osp/app/signin/SignUpWithFacebookWebView;

    invoke-static {v0}, Lcom/osp/app/signin/SignUpWithFacebookWebView;->a(Lcom/osp/app/signin/SignUpWithFacebookWebView;)Landroid/app/ProgressDialog;

    move-result-object v0

    iget-object v1, p0, Lcom/osp/app/signin/mm;->a:Lcom/osp/app/signin/SignUpWithFacebookWebView;

    const v2, 0x7f090036

    invoke-static {v1, v2}, Lcom/msc/sa/c/d;->a(Landroid/content/Context;I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/app/ProgressDialog;->setMessage(Ljava/lang/CharSequence;)V

    .line 226
    iget-object v0, p0, Lcom/osp/app/signin/mm;->a:Lcom/osp/app/signin/SignUpWithFacebookWebView;

    invoke-static {v0}, Lcom/osp/app/signin/SignUpWithFacebookWebView;->a(Lcom/osp/app/signin/SignUpWithFacebookWebView;)Landroid/app/ProgressDialog;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/app/ProgressDialog;->setCancelable(Z)V

    .line 227
    iget-object v0, p0, Lcom/osp/app/signin/mm;->a:Lcom/osp/app/signin/SignUpWithFacebookWebView;

    invoke-static {v0}, Lcom/osp/app/signin/SignUpWithFacebookWebView;->a(Lcom/osp/app/signin/SignUpWithFacebookWebView;)Landroid/app/ProgressDialog;

    move-result-object v0

    new-instance v1, Lcom/osp/app/signin/mn;

    invoke-direct {v1, p0}, Lcom/osp/app/signin/mn;-><init>(Lcom/osp/app/signin/mm;)V

    invoke-virtual {v0, v1}, Landroid/app/ProgressDialog;->setOnKeyListener(Landroid/content/DialogInterface$OnKeyListener;)V

    .line 263
    :try_start_0
    iget-object v0, p0, Lcom/osp/app/signin/mm;->a:Lcom/osp/app/signin/SignUpWithFacebookWebView;

    invoke-virtual {v0}, Lcom/osp/app/signin/SignUpWithFacebookWebView;->isFinishing()Z

    move-result v0

    if-nez v0, :cond_1

    .line 265
    iget-object v0, p0, Lcom/osp/app/signin/mm;->a:Lcom/osp/app/signin/SignUpWithFacebookWebView;

    invoke-static {v0}, Lcom/osp/app/signin/SignUpWithFacebookWebView;->a(Lcom/osp/app/signin/SignUpWithFacebookWebView;)Landroid/app/ProgressDialog;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/ProgressDialog;->show()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 273
    :cond_1
    :goto_1
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "onPageStarted url : "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;)V

    .line 274
    const-string v0, "/mobile/account/facebookUserInfoResult.do?"

    invoke-virtual {p2, v0}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 279
    :goto_2
    return-void

    .line 223
    :cond_2
    iget-object v0, p0, Lcom/osp/app/signin/mm;->a:Lcom/osp/app/signin/SignUpWithFacebookWebView;

    new-instance v1, Landroid/app/ProgressDialog;

    iget-object v2, p0, Lcom/osp/app/signin/mm;->a:Lcom/osp/app/signin/SignUpWithFacebookWebView;

    invoke-direct {v1, v2}, Landroid/app/ProgressDialog;-><init>(Landroid/content/Context;)V

    invoke-static {v0, v1}, Lcom/osp/app/signin/SignUpWithFacebookWebView;->a(Lcom/osp/app/signin/SignUpWithFacebookWebView;Landroid/app/ProgressDialog;)Landroid/app/ProgressDialog;

    goto :goto_0

    .line 267
    :catch_0
    move-exception v0

    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_1

    .line 278
    :cond_3
    invoke-super {p0, p1, p2, p3}, Landroid/webkit/WebViewClient;->onPageStarted(Landroid/webkit/WebView;Ljava/lang/String;Landroid/graphics/Bitmap;)V

    goto :goto_2
.end method

.method public final onReceivedError(Landroid/webkit/WebView;ILjava/lang/String;Ljava/lang/String;)V
    .locals 3

    .prologue
    .line 322
    :try_start_0
    iget-object v0, p0, Lcom/osp/app/signin/mm;->a:Lcom/osp/app/signin/SignUpWithFacebookWebView;

    invoke-virtual {v0}, Lcom/osp/app/signin/SignUpWithFacebookWebView;->isFinishing()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/osp/app/signin/mm;->a:Lcom/osp/app/signin/SignUpWithFacebookWebView;

    invoke-static {v0}, Lcom/osp/app/signin/SignUpWithFacebookWebView;->a(Lcom/osp/app/signin/SignUpWithFacebookWebView;)Landroid/app/ProgressDialog;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/osp/app/signin/mm;->a:Lcom/osp/app/signin/SignUpWithFacebookWebView;

    invoke-static {v0}, Lcom/osp/app/signin/SignUpWithFacebookWebView;->a(Lcom/osp/app/signin/SignUpWithFacebookWebView;)Landroid/app/ProgressDialog;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/ProgressDialog;->isShowing()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 324
    iget-object v0, p0, Lcom/osp/app/signin/mm;->a:Lcom/osp/app/signin/SignUpWithFacebookWebView;

    invoke-static {v0}, Lcom/osp/app/signin/SignUpWithFacebookWebView;->a(Lcom/osp/app/signin/SignUpWithFacebookWebView;)Landroid/app/ProgressDialog;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/ProgressDialog;->dismiss()V

    .line 325
    iget-object v0, p0, Lcom/osp/app/signin/mm;->a:Lcom/osp/app/signin/SignUpWithFacebookWebView;

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lcom/osp/app/signin/SignUpWithFacebookWebView;->a(Lcom/osp/app/signin/SignUpWithFacebookWebView;Landroid/app/ProgressDialog;)Landroid/app/ProgressDialog;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 331
    :cond_0
    :goto_0
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "Error code : "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/osp/app/util/aq;->b(Ljava/lang/String;)V

    .line 332
    invoke-super {p0, p1, p2, p3, p4}, Landroid/webkit/WebViewClient;->onReceivedError(Landroid/webkit/WebView;ILjava/lang/String;Ljava/lang/String;)V

    .line 333
    return-void

    .line 327
    :catch_0
    move-exception v0

    .line 329
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "ProgressDialog error message : "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/osp/app/util/aq;->b(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public final shouldOverrideUrlLoading(Landroid/webkit/WebView;Ljava/lang/String;)Z
    .locals 3

    .prologue
    .line 190
    const-string v0, "http://"

    invoke-virtual {p2, v0}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    const-string v0, "https://"

    invoke-virtual {p2, v0}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 194
    :try_start_0
    new-instance v0, Landroid/content/Intent;

    const-string v1, "android.intent.action.VIEW"

    invoke-static {p2}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;Landroid/net/Uri;)V

    .line 195
    iget-object v1, p0, Lcom/osp/app/signin/mm;->a:Lcom/osp/app/signin/SignUpWithFacebookWebView;

    invoke-virtual {v1, v0}, Lcom/osp/app/signin/SignUpWithFacebookWebView;->startActivity(Landroid/content/Intent;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 205
    const/4 v0, 0x1

    :goto_0
    return v0

    .line 196
    :catch_0
    move-exception v0

    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    .line 199
    invoke-super {p0, p1, p2}, Landroid/webkit/WebViewClient;->shouldOverrideUrlLoading(Landroid/webkit/WebView;Ljava/lang/String;)Z

    move-result v0

    goto :goto_0

    .line 203
    :cond_0
    invoke-super {p0, p1, p2}, Landroid/webkit/WebViewClient;->shouldOverrideUrlLoading(Landroid/webkit/WebView;Ljava/lang/String;)Z

    move-result v0

    goto :goto_0
.end method
