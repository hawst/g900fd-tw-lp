.class public Lcom/osp/app/signin/CheckSecurityInfoActivity;
.super Lcom/osp/app/util/BaseActivity;
.source "CheckSecurityInfoActivity.java"


# instance fields
.field private A:Z

.field private B:Lcom/osp/app/signin/dv;

.field private C:Lcom/osp/app/signin/du;

.field private final D:I

.field private E:Ljava/util/ArrayList;

.field private F:Ljava/lang/String;

.field private G:Ljava/lang/String;

.field private H:Ljava/lang/String;

.field private I:Ljava/util/ArrayList;

.field private J:Ljava/util/ArrayList;

.field private K:Ljava/lang/String;

.field private L:Lcom/osp/app/signin/dx;

.field private M:Z

.field private N:Lcom/osp/app/signin/BottomSoftkeyLinearLayout;

.field final a:Ljava/lang/String;

.field final b:Ljava/lang/String;

.field final c:Landroid/view/View$OnClickListener;

.field final d:Landroid/view/View$OnClickListener;

.field final e:Landroid/content/DialogInterface$OnCancelListener;

.field private final f:Ljava/lang/String;

.field private final y:Ljava/lang/String;

.field private z:Lcom/osp/app/signin/dw;


# direct methods
.method public constructor <init>()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x0

    .line 85
    invoke-direct {p0}, Lcom/osp/app/util/BaseActivity;-><init>()V

    .line 87
    const-string v0, "CSI"

    iput-object v0, p0, Lcom/osp/app/signin/CheckSecurityInfoActivity;->f:Ljava/lang/String;

    .line 92
    const-string v0, "status_impossible_distinct"

    iput-object v0, p0, Lcom/osp/app/signin/CheckSecurityInfoActivity;->y:Ljava/lang/String;

    .line 102
    iput-boolean v1, p0, Lcom/osp/app/signin/CheckSecurityInfoActivity;->A:Z

    .line 127
    const-string v0, "/link/link.do"

    iput-object v0, p0, Lcom/osp/app/signin/CheckSecurityInfoActivity;->a:Ljava/lang/String;

    .line 131
    const-string v0, "/main/main.do"

    iput-object v0, p0, Lcom/osp/app/signin/CheckSecurityInfoActivity;->b:Ljava/lang/String;

    .line 146
    const/4 v0, 0x5

    iput v0, p0, Lcom/osp/app/signin/CheckSecurityInfoActivity;->D:I

    .line 178
    iput-object v2, p0, Lcom/osp/app/signin/CheckSecurityInfoActivity;->I:Ljava/util/ArrayList;

    .line 184
    iput-object v2, p0, Lcom/osp/app/signin/CheckSecurityInfoActivity;->J:Ljava/util/ArrayList;

    .line 200
    iput-boolean v1, p0, Lcom/osp/app/signin/CheckSecurityInfoActivity;->M:Z

    .line 203
    new-instance v0, Lcom/osp/app/signin/dm;

    invoke-direct {v0, p0}, Lcom/osp/app/signin/dm;-><init>(Lcom/osp/app/signin/CheckSecurityInfoActivity;)V

    iput-object v0, p0, Lcom/osp/app/signin/CheckSecurityInfoActivity;->c:Landroid/view/View$OnClickListener;

    .line 250
    new-instance v0, Lcom/osp/app/signin/dn;

    invoke-direct {v0, p0}, Lcom/osp/app/signin/dn;-><init>(Lcom/osp/app/signin/CheckSecurityInfoActivity;)V

    iput-object v0, p0, Lcom/osp/app/signin/CheckSecurityInfoActivity;->d:Landroid/view/View$OnClickListener;

    .line 335
    new-instance v0, Lcom/osp/app/signin/do;

    invoke-direct {v0, p0}, Lcom/osp/app/signin/do;-><init>(Lcom/osp/app/signin/CheckSecurityInfoActivity;)V

    iput-object v0, p0, Lcom/osp/app/signin/CheckSecurityInfoActivity;->e:Landroid/content/DialogInterface$OnCancelListener;

    .line 1629
    return-void
.end method

.method static synthetic a(Lcom/osp/app/signin/CheckSecurityInfoActivity;Lcom/osp/app/signin/du;)Lcom/osp/app/signin/du;
    .locals 0

    .prologue
    .line 85
    iput-object p1, p0, Lcom/osp/app/signin/CheckSecurityInfoActivity;->C:Lcom/osp/app/signin/du;

    return-object p1
.end method

.method static synthetic a(Lcom/osp/app/signin/CheckSecurityInfoActivity;Lcom/osp/app/signin/dw;)Lcom/osp/app/signin/dw;
    .locals 0

    .prologue
    .line 85
    iput-object p1, p0, Lcom/osp/app/signin/CheckSecurityInfoActivity;->z:Lcom/osp/app/signin/dw;

    return-object p1
.end method

.method static synthetic a(Lcom/osp/app/signin/CheckSecurityInfoActivity;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 85
    invoke-direct {p0}, Lcom/osp/app/signin/CheckSecurityInfoActivity;->e()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method static synthetic a(Lcom/osp/app/signin/CheckSecurityInfoActivity;Ljava/util/ArrayList;)Ljava/util/ArrayList;
    .locals 0

    .prologue
    .line 85
    iput-object p1, p0, Lcom/osp/app/signin/CheckSecurityInfoActivity;->I:Ljava/util/ArrayList;

    return-object p1
.end method

.method private a(I)V
    .locals 6

    .prologue
    const v4, 0x7f08011e

    .line 2642
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "CSI::initLayoutParams orientation : "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;)V

    .line 2644
    invoke-static {}, Lcom/osp/device/b;->b()Lcom/osp/device/b;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/osp/device/b;->i(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 2646
    const v0, 0x7f0c0168

    invoke-virtual {p0, v0}, Lcom/osp/app/signin/CheckSecurityInfoActivity;->findViewById(I)Landroid/view/View;

    move-result-object v2

    .line 2650
    invoke-virtual {p0}, Lcom/osp/app/signin/CheckSecurityInfoActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f08011f

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v0

    float-to-int v0, v0

    .line 2651
    invoke-virtual {p0}, Lcom/osp/app/signin/CheckSecurityInfoActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1, v4}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v1

    float-to-int v1, v1

    .line 2652
    if-eqz v2, :cond_1

    .line 2656
    invoke-static {}, Lcom/osp/app/signin/SamsungService;->a()Z

    move-result v3

    if-nez v3, :cond_0

    .line 2658
    invoke-virtual {p0}, Lcom/osp/app/signin/CheckSecurityInfoActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0, v4}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v0

    float-to-int v0, v0

    .line 2661
    :cond_0
    invoke-static {}, Lcom/osp/app/util/q;->a()Lcom/osp/app/util/q;

    invoke-static {v2, v0, v1, p1}, Lcom/osp/app/util/q;->b(Landroid/view/View;III)V

    .line 2667
    :cond_1
    invoke-static {p0}, Lcom/osp/app/util/r;->k(Landroid/content/Context;)Z

    move-result v2

    if-nez v2, :cond_4

    .line 2669
    invoke-virtual {p0}, Lcom/osp/app/signin/CheckSecurityInfoActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f08013d

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v0

    float-to-int v1, v0

    .line 2670
    invoke-virtual {p0}, Lcom/osp/app/signin/CheckSecurityInfoActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v2, 0x7f08013c

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v0

    float-to-int v0, v0

    .line 2673
    :goto_0
    iget-object v2, p0, Lcom/osp/app/signin/CheckSecurityInfoActivity;->N:Lcom/osp/app/signin/BottomSoftkeyLinearLayout;

    if-eqz v2, :cond_3

    .line 2675
    invoke-static {}, Lcom/osp/app/signin/SamsungService;->a()Z

    move-result v2

    if-eqz v2, :cond_2

    sget-object v2, Lcom/osp/app/util/u;->o:[Ljava/lang/String;

    invoke-static {v2}, Lcom/osp/app/util/u;->a([Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 2677
    const/4 v1, 0x0

    .line 2679
    :cond_2
    invoke-static {}, Lcom/osp/app/util/q;->a()Lcom/osp/app/util/q;

    iget-object v2, p0, Lcom/osp/app/signin/CheckSecurityInfoActivity;->N:Lcom/osp/app/signin/BottomSoftkeyLinearLayout;

    invoke-static {p0, v2, v1, v0, p1}, Lcom/osp/app/util/q;->a(Landroid/content/Context;Landroid/view/View;III)V

    .line 2683
    :cond_3
    return-void

    :cond_4
    move v5, v1

    move v1, v0

    move v0, v5

    goto :goto_0
.end method

.method static synthetic b(Lcom/osp/app/signin/CheckSecurityInfoActivity;)Lcom/osp/app/signin/du;
    .locals 1

    .prologue
    .line 85
    iget-object v0, p0, Lcom/osp/app/signin/CheckSecurityInfoActivity;->C:Lcom/osp/app/signin/du;

    return-object v0
.end method

.method static synthetic b(Lcom/osp/app/signin/CheckSecurityInfoActivity;Ljava/util/ArrayList;)V
    .locals 10

    .prologue
    const/4 v9, 0x5

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 85
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/osp/app/signin/CheckSecurityInfoActivity;->E:Ljava/util/ArrayList;

    invoke-virtual {p1}, Ljava/util/ArrayList;->size()I

    move-result v5

    move v4, v2

    :goto_0
    if-ge v4, v5, :cond_2

    invoke-virtual {p1, v4}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/osp/app/signin/dw;

    invoke-virtual {v0}, Lcom/osp/app/signin/dw;->b()Ljava/lang/String;

    move-result-object v6

    iget-object v0, p0, Lcom/osp/app/signin/CheckSecurityInfoActivity;->E:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v7

    move v1, v2

    :goto_1
    if-ge v1, v7, :cond_7

    iget-object v0, p0, Lcom/osp/app/signin/CheckSecurityInfoActivity;->E:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/util/Pair;

    iget-object v0, v0, Landroid/util/Pair;->first:Ljava/lang/Object;

    check-cast v0, Ljava/lang/String;

    invoke-virtual {v0, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    move v0, v3

    :goto_2
    if-nez v0, :cond_0

    invoke-static {}, Lcom/osp/app/util/y;->a()Lcom/osp/app/util/y;

    move-result-object v1

    invoke-virtual {p1, v4}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/osp/app/signin/dw;

    invoke-virtual {v0}, Lcom/osp/app/signin/dw;->b()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Lcom/osp/app/util/y;->a(Ljava/lang/String;)I

    move-result v1

    iget-object v6, p0, Lcom/osp/app/signin/CheckSecurityInfoActivity;->E:Ljava/util/ArrayList;

    new-instance v7, Landroid/util/Pair;

    invoke-virtual {p1, v4}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/osp/app/signin/dw;

    invoke-virtual {v0}, Lcom/osp/app/signin/dw;->b()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-direct {v7, v0, v1}, Landroid/util/Pair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    invoke-virtual {v6, v7}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    :cond_0
    add-int/lit8 v0, v4, 0x1

    move v4, v0

    goto :goto_0

    :cond_1
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_1

    :cond_2
    iget-object v0, p0, Lcom/osp/app/signin/CheckSecurityInfoActivity;->E:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-ge v0, v9, :cond_5

    invoke-static {}, Lcom/osp/app/util/y;->a()Lcom/osp/app/util/y;

    move-result-object v0

    invoke-virtual {v0}, Lcom/osp/app/util/y;->b()Ljava/util/ArrayList;

    move-result-object v5

    invoke-virtual {v5}, Ljava/util/ArrayList;->size()I

    move-result v6

    move v1, v2

    :goto_3
    if-ge v1, v6, :cond_5

    iget-object v0, p0, Lcom/osp/app/signin/CheckSecurityInfoActivity;->E:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v7

    move v4, v2

    :goto_4
    if-ge v4, v7, :cond_6

    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    new-instance v8, Ljava/lang/StringBuilder;

    const-string v0, "makeQSet qRId|rRId = "

    invoke-direct {v8, v0}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/osp/app/signin/CheckSecurityInfoActivity;->E:Ljava/util/ArrayList;

    invoke-virtual {v0, v4}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/util/Pair;

    iget-object v0, v0, Landroid/util/Pair;->second:Ljava/lang/Object;

    invoke-virtual {v8, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v8, "|"

    invoke-virtual {v0, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v5, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v8

    invoke-virtual {v0, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/osp/app/signin/CheckSecurityInfoActivity;->E:Ljava/util/ArrayList;

    invoke-virtual {v0, v4}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/util/Pair;

    iget-object v0, v0, Landroid/util/Pair;->second:Ljava/lang/Object;

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v5, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v8

    invoke-virtual {v0, v8}, Ljava/lang/Integer;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_4

    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v0, "makeQSet isExist"

    invoke-static {v0}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;)V

    move v0, v3

    :goto_5
    if-nez v0, :cond_3

    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v4, "makeQSet add : "

    invoke-direct {v0, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v5, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v4

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/osp/app/signin/CheckSecurityInfoActivity;->E:Ljava/util/ArrayList;

    new-instance v4, Landroid/util/Pair;

    const-string v7, ""

    invoke-virtual {v5, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v8

    invoke-direct {v4, v7, v8}, Landroid/util/Pair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    invoke-virtual {v0, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    :cond_3
    iget-object v0, p0, Lcom/osp/app/signin/CheckSecurityInfoActivity;->E:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-eq v0, v9, :cond_5

    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto/16 :goto_3

    :cond_4
    add-int/lit8 v0, v4, 0x1

    move v4, v0

    goto/16 :goto_4

    :cond_5
    return-void

    :cond_6
    move v0, v2

    goto :goto_5

    :cond_7
    move v0, v2

    goto/16 :goto_2
.end method

.method static synthetic c(Lcom/osp/app/signin/CheckSecurityInfoActivity;)Ljava/util/ArrayList;
    .locals 1

    .prologue
    .line 85
    iget-object v0, p0, Lcom/osp/app/signin/CheckSecurityInfoActivity;->I:Ljava/util/ArrayList;

    return-object v0
.end method

.method static synthetic d(Lcom/osp/app/signin/CheckSecurityInfoActivity;)Lcom/osp/app/signin/dw;
    .locals 1

    .prologue
    .line 85
    iget-object v0, p0, Lcom/osp/app/signin/CheckSecurityInfoActivity;->z:Lcom/osp/app/signin/dw;

    return-object v0
.end method

.method private d()V
    .locals 9

    .prologue
    const/16 v8, 0x8

    const/4 v7, 0x2

    const/4 v1, 0x0

    const v6, 0x7f090064

    const v5, 0x7f090019

    .line 578
    const v0, 0x7f0c00a4

    invoke-virtual {p0, v0}, Lcom/osp/app/signin/CheckSecurityInfoActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 579
    if-eqz v0, :cond_0

    .line 581
    invoke-virtual {p0}, Lcom/osp/app/signin/CheckSecurityInfoActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-static {p0}, Lcom/osp/app/util/an;->a(Lcom/osp/app/util/BaseActivity;)Lcom/osp/app/util/an;

    const v3, 0x7f070006

    invoke-static {v3}, Lcom/osp/app/util/an;->a(I)I

    move-result v3

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getColor(I)I

    move-result v2

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setTextColor(I)V

    .line 584
    :cond_0
    const v0, 0x7f0c00b0

    invoke-virtual {p0, v0}, Lcom/osp/app/signin/CheckSecurityInfoActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/RadioGroup;

    .line 586
    const/4 v2, 0x1

    new-array v2, v2, [I

    const v3, 0x1010214

    aput v3, v2, v1

    .line 587
    invoke-virtual {p0, v2}, Lcom/osp/app/signin/CheckSecurityInfoActivity;->obtainStyledAttributes([I)Landroid/content/res/TypedArray;

    move-result-object v2

    .line 588
    invoke-virtual {v2, v1}, Landroid/content/res/TypedArray;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v2

    .line 590
    if-eqz v0, :cond_1

    .line 592
    sget v3, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v4, 0xb

    if-lt v3, v4, :cond_1

    invoke-virtual {v0, v7}, Landroid/widget/LinearLayout;->setShowDividers(I)V

    invoke-virtual {v0, v2}, Landroid/widget/LinearLayout;->setDividerDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 595
    :cond_1
    invoke-static {}, Lcom/osp/app/util/r;->v()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 597
    new-array v2, v7, [I

    fill-array-data v2, :array_0

    move v0, v1

    .line 598
    :goto_0
    array-length v1, v2

    if-ge v0, v1, :cond_3

    .line 600
    aget v1, v2, v0

    invoke-virtual {p0, v1}, Lcom/osp/app/signin/CheckSecurityInfoActivity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    .line 601
    if-eqz v1, :cond_2

    .line 603
    invoke-virtual {v1, v8}, Landroid/view/View;->setVisibility(I)V

    .line 598
    :cond_2
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 609
    :cond_3
    const v0, 0x7f0c0065

    invoke-virtual {p0, v0}, Lcom/osp/app/signin/CheckSecurityInfoActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/osp/app/signin/BottomSoftkeyLinearLayout;

    iput-object v0, p0, Lcom/osp/app/signin/CheckSecurityInfoActivity;->N:Lcom/osp/app/signin/BottomSoftkeyLinearLayout;

    .line 610
    iget-object v0, p0, Lcom/osp/app/signin/CheckSecurityInfoActivity;->N:Lcom/osp/app/signin/BottomSoftkeyLinearLayout;

    if-eqz v0, :cond_4

    .line 612
    iget-object v0, p0, Lcom/osp/app/signin/CheckSecurityInfoActivity;->N:Lcom/osp/app/signin/BottomSoftkeyLinearLayout;

    invoke-virtual {p0}, Lcom/osp/app/signin/CheckSecurityInfoActivity;->h()Z

    move-result v1

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-virtual {v0, p0, v1}, Lcom/osp/app/signin/BottomSoftkeyLinearLayout;->a(Landroid/content/Context;Ljava/lang/Boolean;)V

    .line 613
    invoke-static {}, Lcom/osp/device/b;->b()Lcom/osp/device/b;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/osp/device/b;->l(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_6

    .line 615
    iget-object v0, p0, Lcom/osp/app/signin/CheckSecurityInfoActivity;->N:Lcom/osp/app/signin/BottomSoftkeyLinearLayout;

    invoke-virtual {p0, v6}, Lcom/osp/app/signin/CheckSecurityInfoActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/osp/app/signin/BottomSoftkeyLinearLayout;->b(Ljava/lang/String;)V

    .line 616
    iget-object v0, p0, Lcom/osp/app/signin/CheckSecurityInfoActivity;->N:Lcom/osp/app/signin/BottomSoftkeyLinearLayout;

    invoke-virtual {p0, v6}, Lcom/osp/app/signin/CheckSecurityInfoActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/osp/app/signin/BottomSoftkeyLinearLayout;->d(Ljava/lang/String;)V

    .line 617
    iget-object v0, p0, Lcom/osp/app/signin/CheckSecurityInfoActivity;->N:Lcom/osp/app/signin/BottomSoftkeyLinearLayout;

    invoke-virtual {p0, v5}, Lcom/osp/app/signin/CheckSecurityInfoActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/osp/app/signin/BottomSoftkeyLinearLayout;->c(Ljava/lang/String;)V

    .line 618
    iget-object v0, p0, Lcom/osp/app/signin/CheckSecurityInfoActivity;->N:Lcom/osp/app/signin/BottomSoftkeyLinearLayout;

    invoke-virtual {p0, v5}, Lcom/osp/app/signin/CheckSecurityInfoActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/osp/app/signin/BottomSoftkeyLinearLayout;->e(Ljava/lang/String;)V

    .line 619
    iget-object v0, p0, Lcom/osp/app/signin/CheckSecurityInfoActivity;->N:Lcom/osp/app/signin/BottomSoftkeyLinearLayout;

    iget-object v1, p0, Lcom/osp/app/signin/CheckSecurityInfoActivity;->c:Landroid/view/View$OnClickListener;

    invoke-virtual {v0, v1}, Lcom/osp/app/signin/BottomSoftkeyLinearLayout;->a(Landroid/view/View$OnClickListener;)V

    .line 620
    iget-object v0, p0, Lcom/osp/app/signin/CheckSecurityInfoActivity;->N:Lcom/osp/app/signin/BottomSoftkeyLinearLayout;

    iget-object v1, p0, Lcom/osp/app/signin/CheckSecurityInfoActivity;->d:Landroid/view/View$OnClickListener;

    invoke-virtual {v0, v1}, Lcom/osp/app/signin/BottomSoftkeyLinearLayout;->b(Landroid/view/View$OnClickListener;)V

    .line 633
    :cond_4
    :goto_1
    invoke-static {}, Lcom/osp/app/signin/SamsungService;->a()Z

    move-result v0

    if-nez v0, :cond_5

    invoke-static {}, Lcom/osp/device/b;->b()Lcom/osp/device/b;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/osp/device/b;->i(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_5

    .line 635
    iget-object v0, p0, Lcom/osp/app/signin/CheckSecurityInfoActivity;->N:Lcom/osp/app/signin/BottomSoftkeyLinearLayout;

    if-eqz v0, :cond_5

    .line 637
    iget-object v0, p0, Lcom/osp/app/signin/CheckSecurityInfoActivity;->N:Lcom/osp/app/signin/BottomSoftkeyLinearLayout;

    invoke-virtual {v0, v8}, Lcom/osp/app/signin/BottomSoftkeyLinearLayout;->setVisibility(I)V

    .line 642
    :cond_5
    return-void

    .line 624
    :cond_6
    iget-object v0, p0, Lcom/osp/app/signin/CheckSecurityInfoActivity;->N:Lcom/osp/app/signin/BottomSoftkeyLinearLayout;

    invoke-virtual {p0, v5}, Lcom/osp/app/signin/CheckSecurityInfoActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/osp/app/signin/BottomSoftkeyLinearLayout;->b(Ljava/lang/String;)V

    .line 625
    iget-object v0, p0, Lcom/osp/app/signin/CheckSecurityInfoActivity;->N:Lcom/osp/app/signin/BottomSoftkeyLinearLayout;

    invoke-virtual {p0, v5}, Lcom/osp/app/signin/CheckSecurityInfoActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/osp/app/signin/BottomSoftkeyLinearLayout;->d(Ljava/lang/String;)V

    .line 626
    iget-object v0, p0, Lcom/osp/app/signin/CheckSecurityInfoActivity;->N:Lcom/osp/app/signin/BottomSoftkeyLinearLayout;

    invoke-virtual {p0, v6}, Lcom/osp/app/signin/CheckSecurityInfoActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/osp/app/signin/BottomSoftkeyLinearLayout;->c(Ljava/lang/String;)V

    .line 627
    iget-object v0, p0, Lcom/osp/app/signin/CheckSecurityInfoActivity;->N:Lcom/osp/app/signin/BottomSoftkeyLinearLayout;

    invoke-virtual {p0, v6}, Lcom/osp/app/signin/CheckSecurityInfoActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/osp/app/signin/BottomSoftkeyLinearLayout;->e(Ljava/lang/String;)V

    .line 628
    iget-object v0, p0, Lcom/osp/app/signin/CheckSecurityInfoActivity;->N:Lcom/osp/app/signin/BottomSoftkeyLinearLayout;

    iget-object v1, p0, Lcom/osp/app/signin/CheckSecurityInfoActivity;->c:Landroid/view/View$OnClickListener;

    invoke-virtual {v0, v1}, Lcom/osp/app/signin/BottomSoftkeyLinearLayout;->b(Landroid/view/View$OnClickListener;)V

    .line 629
    iget-object v0, p0, Lcom/osp/app/signin/CheckSecurityInfoActivity;->N:Lcom/osp/app/signin/BottomSoftkeyLinearLayout;

    iget-object v1, p0, Lcom/osp/app/signin/CheckSecurityInfoActivity;->d:Landroid/view/View$OnClickListener;

    invoke-virtual {v0, v1}, Lcom/osp/app/signin/BottomSoftkeyLinearLayout;->a(Landroid/view/View$OnClickListener;)V

    goto :goto_1

    .line 597
    nop

    :array_0
    .array-data 4
        0x7f0c00a5
        0x7f0c00a8
    .end array-data
.end method

.method private e()Ljava/lang/String;
    .locals 3

    .prologue
    .line 763
    const/4 v1, 0x0

    .line 765
    const v0, 0x7f0c00b0

    invoke-virtual {p0, v0}, Lcom/osp/app/signin/CheckSecurityInfoActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/RadioGroup;

    .line 768
    invoke-virtual {v0}, Landroid/widget/RadioGroup;->getCheckedRadioButtonId()I

    move-result v2

    .line 769
    invoke-virtual {p0, v2}, Lcom/osp/app/signin/CheckSecurityInfoActivity;->findViewById(I)Landroid/view/View;

    move-result-object v2

    invoke-virtual {v0, v2}, Landroid/widget/RadioGroup;->indexOfChild(Landroid/view/View;)I

    move-result v0

    .line 772
    iget-object v2, p0, Lcom/osp/app/signin/CheckSecurityInfoActivity;->E:Ljava/util/ArrayList;

    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/util/Pair;

    iget-object v0, v0, Landroid/util/Pair;->second:Ljava/lang/Object;

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    .line 773
    if-lez v0, :cond_0

    .line 775
    invoke-static {}, Lcom/osp/app/util/y;->a()Lcom/osp/app/util/y;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/osp/app/util/y;->a(I)Ljava/lang/String;

    move-result-object v0

    move-object v1, v0

    .line 779
    :cond_0
    const v0, 0x7f0c00b1

    invoke-virtual {p0, v0}, Lcom/osp/app/signin/CheckSecurityInfoActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/EditText;

    .line 780
    invoke-virtual {v0}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    .line 782
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "/"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method static synthetic e(Lcom/osp/app/signin/CheckSecurityInfoActivity;)V
    .locals 1

    .prologue
    .line 85
    new-instance v0, Lcom/osp/app/signin/dx;

    invoke-direct {v0, p0, p0}, Lcom/osp/app/signin/dx;-><init>(Lcom/osp/app/signin/CheckSecurityInfoActivity;Landroid/content/Context;)V

    iput-object v0, p0, Lcom/osp/app/signin/CheckSecurityInfoActivity;->L:Lcom/osp/app/signin/dx;

    iget-object v0, p0, Lcom/osp/app/signin/CheckSecurityInfoActivity;->L:Lcom/osp/app/signin/dx;

    invoke-virtual {v0}, Lcom/osp/app/signin/dx;->b()V

    return-void
.end method

.method static synthetic f(Lcom/osp/app/signin/CheckSecurityInfoActivity;)Ljava/util/ArrayList;
    .locals 1

    .prologue
    .line 85
    iget-object v0, p0, Lcom/osp/app/signin/CheckSecurityInfoActivity;->J:Ljava/util/ArrayList;

    return-object v0
.end method

.method static synthetic g(Lcom/osp/app/signin/CheckSecurityInfoActivity;)V
    .locals 12

    .prologue
    const v11, 0x7f0800e7

    const v10, 0x7f0800dd

    const/16 v9, 0x10

    const/4 v8, 0x1

    const/4 v3, 0x0

    .line 85
    iget-object v0, p0, Lcom/osp/app/signin/CheckSecurityInfoActivity;->E:Ljava/util/ArrayList;

    invoke-static {v0}, Ljava/util/Collections;->shuffle(Ljava/util/List;)V

    const v0, 0x7f0c00b0

    invoke-virtual {p0, v0}, Lcom/osp/app/signin/CheckSecurityInfoActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/RadioGroup;

    iget-object v1, p0, Lcom/osp/app/signin/CheckSecurityInfoActivity;->E:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v4

    move v2, v3

    :goto_0
    if-ge v2, v4, :cond_3

    new-instance v5, Landroid/widget/RadioButton;

    invoke-direct {v5, p0}, Landroid/widget/RadioButton;-><init>(Landroid/content/Context;)V

    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    new-instance v6, Ljava/lang/StringBuilder;

    const-string v1, "RID : "

    invoke-direct {v6, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v1, p0, Lcom/osp/app/signin/CheckSecurityInfoActivity;->E:Ljava/util/ArrayList;

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/util/Pair;

    iget-object v1, v1, Landroid/util/Pair;->first:Ljava/lang/Object;

    check-cast v1, Ljava/lang/String;

    invoke-virtual {v6, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v6, "|"

    invoke-virtual {v1, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    iget-object v1, p0, Lcom/osp/app/signin/CheckSecurityInfoActivity;->E:Ljava/util/ArrayList;

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/util/Pair;

    iget-object v1, v1, Landroid/util/Pair;->second:Ljava/lang/Object;

    invoke-virtual {v6, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;)V

    iget-object v1, p0, Lcom/osp/app/signin/CheckSecurityInfoActivity;->E:Ljava/util/ArrayList;

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/util/Pair;

    iget-object v1, v1, Landroid/util/Pair;->second:Ljava/lang/Object;

    check-cast v1, Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    if-lez v1, :cond_2

    iget-object v1, p0, Lcom/osp/app/signin/CheckSecurityInfoActivity;->E:Ljava/util/ArrayList;

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/util/Pair;

    iget-object v1, v1, Landroid/util/Pair;->second:Ljava/lang/Object;

    check-cast v1, Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p0, v1}, Lcom/osp/app/signin/CheckSecurityInfoActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v5, v1}, Landroid/widget/RadioButton;->setText(Ljava/lang/CharSequence;)V

    :goto_1
    sget v1, Landroid/os/Build$VERSION;->SDK_INT:I

    if-le v1, v9, :cond_1

    const/high16 v1, 0x41200000    # 10.0f

    invoke-virtual {p0}, Lcom/osp/app/signin/CheckSecurityInfoActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v6

    invoke-virtual {v6}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v6

    invoke-static {v8, v1, v6}, Landroid/util/TypedValue;->applyDimension(IFLandroid/util/DisplayMetrics;)F

    move-result v1

    float-to-int v1, v1

    invoke-static {}, Lcom/osp/device/b;->b()Lcom/osp/device/b;

    move-result-object v6

    invoke-virtual {v6, p0}, Lcom/osp/device/b;->i(Landroid/content/Context;)Z

    move-result v6

    if-eqz v6, :cond_0

    invoke-virtual {p0}, Lcom/osp/app/signin/CheckSecurityInfoActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1, v10}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v1

    float-to-int v1, v1

    :cond_0
    invoke-static {v5, v1, v3, v3, v3}, Lcom/msc/sa/c/d;->a(Landroid/view/View;IIII)V

    :cond_1
    invoke-virtual {v5, v3}, Landroid/widget/RadioButton;->setChecked(Z)V

    new-instance v1, Landroid/widget/RadioGroup$LayoutParams;

    const/4 v6, -0x1

    const/4 v7, -0x2

    invoke-direct {v1, v6, v7}, Landroid/widget/RadioGroup$LayoutParams;-><init>(II)V

    invoke-virtual {p0}, Lcom/osp/app/signin/CheckSecurityInfoActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v6

    invoke-virtual {v6, v11}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v6

    iput v6, v1, Landroid/widget/RadioGroup$LayoutParams;->leftMargin:I

    invoke-virtual {p0}, Lcom/osp/app/signin/CheckSecurityInfoActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v6

    invoke-virtual {v6, v11}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v6

    iput v6, v1, Landroid/widget/RadioGroup$LayoutParams;->rightMargin:I

    invoke-virtual {p0}, Lcom/osp/app/signin/CheckSecurityInfoActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v6

    const v7, 0x7f0800dc

    invoke-virtual {v6, v7}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v6

    iput v6, v1, Landroid/widget/RadioGroup$LayoutParams;->height:I

    invoke-virtual {p0}, Lcom/osp/app/signin/CheckSecurityInfoActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v6

    invoke-virtual {v6, v10}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v6

    invoke-virtual {v5, v6}, Landroid/widget/RadioButton;->setCompoundDrawablePadding(I)V

    invoke-virtual {v5, v9}, Landroid/widget/RadioButton;->setGravity(I)V

    invoke-static {v5, v1}, Lcom/msc/sa/c/d;->a(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    invoke-virtual {v0, v5, v2}, Landroid/widget/RadioGroup;->addView(Landroid/view/View;I)V

    add-int/lit8 v1, v2, 0x1

    move v2, v1

    goto/16 :goto_0

    :cond_2
    iget-object v1, p0, Lcom/osp/app/signin/CheckSecurityInfoActivity;->E:Ljava/util/ArrayList;

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/util/Pair;

    iget-object v1, v1, Landroid/util/Pair;->first:Ljava/lang/Object;

    check-cast v1, Ljava/lang/CharSequence;

    invoke-virtual {v5, v1}, Landroid/widget/RadioButton;->setText(Ljava/lang/CharSequence;)V

    goto :goto_1

    :cond_3
    invoke-virtual {v0, v3}, Landroid/widget/RadioGroup;->getChildAt(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/RadioButton;

    invoke-virtual {v1, v8}, Landroid/widget/RadioButton;->setChecked(Z)V

    invoke-virtual {v0}, Landroid/widget/RadioGroup;->invalidate()V

    return-void
.end method

.method static synthetic h(Lcom/osp/app/signin/CheckSecurityInfoActivity;)V
    .locals 2

    .prologue
    .line 85
    new-instance v0, Landroid/content/Intent;

    const-string v1, "com.msc.action.samsungaccount.web_with_sign_in_screen_button"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    const/16 v1, 0xf4

    invoke-virtual {p0, v0, v1}, Lcom/osp/app/signin/CheckSecurityInfoActivity;->startActivityForResult(Landroid/content/Intent;I)V

    return-void
.end method

.method static synthetic i(Lcom/osp/app/signin/CheckSecurityInfoActivity;)V
    .locals 2

    .prologue
    .line 85
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/osp/app/signin/PasswordChangeView;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const/16 v1, 0xef

    invoke-virtual {p0, v0, v1}, Lcom/osp/app/signin/CheckSecurityInfoActivity;->startActivityForResult(Landroid/content/Intent;I)V

    return-void
.end method

.method static synthetic j(Lcom/osp/app/signin/CheckSecurityInfoActivity;)V
    .locals 5

    .prologue
    .line 85
    invoke-static {}, Lcom/osp/device/b;->b()Lcom/osp/device/b;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/osp/device/b;->i(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_1

    :try_start_0
    const-string v0, "/main/main.do"

    :goto_0
    invoke-static {}, Lcom/osp/device/b;->b()Lcom/osp/device/b;

    move-result-object v1

    invoke-virtual {v1, p0}, Lcom/osp/device/b;->i(Landroid/content/Context;)Z

    move-result v1

    invoke-static {}, Lcom/msc/c/g;->a()Lcom/msc/c/g;

    invoke-static {v1}, Lcom/msc/c/k;->a(Z)Ljava/lang/String;

    move-result-object v2

    invoke-static {}, Lcom/msc/c/g;->a()Lcom/msc/c/g;

    invoke-virtual {p0}, Lcom/osp/app/signin/CheckSecurityInfoActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v3

    invoke-static {p0}, Lcom/osp/app/util/ad;->g(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4, v0, v2, v1}, Lcom/msc/c/g;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Z)Ljava/lang/String;

    move-result-object v0

    new-instance v1, Landroid/content/Intent;

    invoke-direct {v1}, Landroid/content/Intent;-><init>()V

    const-string v2, "android.intent.action.VIEW"

    invoke-virtual {v1, v2}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/content/Intent;->setData(Landroid/net/Uri;)Landroid/content/Intent;

    invoke-static {}, Lcom/osp/app/signin/SamsungService;->a()Z

    move-result v2

    if-nez v2, :cond_2

    invoke-static {v1, p0}, Lcom/osp/app/util/ad;->a(Landroid/content/Intent;Landroid/content/Context;)Z

    move-result v2

    const/4 v3, 0x1

    if-ne v2, v3, :cond_2

    invoke-virtual {p0, v1}, Lcom/osp/app/signin/CheckSecurityInfoActivity;->startActivity(Landroid/content/Intent;)V

    :cond_0
    :goto_1
    return-void

    :cond_1
    const-string v0, "/link/link.do"

    goto :goto_0

    :cond_2
    if-eqz v0, :cond_0

    new-instance v1, Landroid/content/Intent;

    const-string v2, "com.msc.action.samsungaccount.web_with_close_button"

    invoke-direct {v1, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    const-string v2, "ServerUrl"

    invoke-virtual {v1, v2, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    invoke-virtual {p0, v1}, Lcom/osp/app/signin/CheckSecurityInfoActivity;->startActivity(Landroid/content/Intent;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_1

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_1
.end method

.method static synthetic k(Lcom/osp/app/signin/CheckSecurityInfoActivity;)V
    .locals 6

    .prologue
    .line 85
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/osp/app/signin/SmsVerificationActivity;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const-string v1, "is_phnumber_verification_mode"

    const-string v2, "FROM_SIGN_IN_FLOW"

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    const-string v1, "client_id"

    iget-object v2, p0, Lcom/osp/app/signin/CheckSecurityInfoActivity;->F:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    const-string v1, "client_secret"

    iget-object v2, p0, Lcom/osp/app/signin/CheckSecurityInfoActivity;->G:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    const-string v1, "key_duplicated_id_password"

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    const-string v1, "key_password"

    iget-object v2, p0, Lcom/osp/app/signin/CheckSecurityInfoActivity;->H:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    const-string v1, "key_user_id"

    iget-object v2, p0, Lcom/osp/app/signin/CheckSecurityInfoActivity;->z:Lcom/osp/app/signin/dw;

    invoke-virtual {v2}, Lcom/osp/app/signin/dw;->a()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    const-string v1, "required_auth"

    iget-boolean v2, p0, Lcom/osp/app/signin/CheckSecurityInfoActivity;->M:Z

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    const-string v1, "key_internal_is_resign_in"

    iget-boolean v2, p0, Lcom/osp/app/signin/CheckSecurityInfoActivity;->A:Z

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "+"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, Lcom/osp/app/signin/CheckSecurityInfoActivity;->K:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {p0}, Lcom/osp/app/util/ad;->p(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/osp/app/util/ad;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/google/i18n/phonenumbers/ac;

    move-result-object v1

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1}, Lcom/google/i18n/phonenumbers/ac;->b()J

    move-result-wide v4

    invoke-virtual {v2, v4, v5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1}, Lcom/google/i18n/phonenumbers/ac;->a()I

    move-result v1

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    const-string v3, "key_phonenumber"

    invoke-virtual {v0, v3, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    const-string v2, "key_country_calling_code"

    invoke-virtual {v0, v2, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    const-string v1, "is_cancelable_just_one_activity"

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    const/16 v1, 0xe5

    invoke-virtual {p0, v0, v1}, Lcom/osp/app/signin/CheckSecurityInfoActivity;->startActivityForResult(Landroid/content/Intent;I)V

    return-void
.end method

.method static synthetic l(Lcom/osp/app/signin/CheckSecurityInfoActivity;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 85
    iget-object v0, p0, Lcom/osp/app/signin/CheckSecurityInfoActivity;->K:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic m(Lcom/osp/app/signin/CheckSecurityInfoActivity;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 85
    iget-object v0, p0, Lcom/osp/app/signin/CheckSecurityInfoActivity;->H:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic n(Lcom/osp/app/signin/CheckSecurityInfoActivity;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 85
    iget-object v0, p0, Lcom/osp/app/signin/CheckSecurityInfoActivity;->F:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic o(Lcom/osp/app/signin/CheckSecurityInfoActivity;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 85
    iget-object v0, p0, Lcom/osp/app/signin/CheckSecurityInfoActivity;->G:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic p(Lcom/osp/app/signin/CheckSecurityInfoActivity;)Z
    .locals 1

    .prologue
    .line 85
    iget-boolean v0, p0, Lcom/osp/app/signin/CheckSecurityInfoActivity;->M:Z

    return v0
.end method

.method static synthetic q(Lcom/osp/app/signin/CheckSecurityInfoActivity;)V
    .locals 5

    .prologue
    .line 85
    invoke-static {}, Lcom/osp/device/b;->b()Lcom/osp/device/b;

    move-result-object v0

    invoke-virtual {v0}, Lcom/osp/device/b;->a()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v0, "Login try with blocked id"

    invoke-static {v0}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;)V

    new-instance v2, Landroid/content/Intent;

    const-string v0, "com.msc.action.samsungaccount.web_dialog"

    invoke-direct {v2, v0}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    const v0, 0x7f0c0121

    invoke-virtual {p0, v0}, Lcom/osp/app/signin/CheckSecurityInfoActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/EditText;

    const-string v1, "j5p7ll8g33"

    iget-object v3, p0, Lcom/osp/app/signin/CheckSecurityInfoActivity;->F:Ljava/lang/String;

    invoke-static {v3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_0

    iget-object v1, p0, Lcom/osp/app/signin/CheckSecurityInfoActivity;->F:Ljava/lang/String;

    :cond_0
    invoke-static {p0}, Lcom/osp/app/util/ad;->p(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    sget-object v4, Ljava/util/Locale;->ENGLISH:Ljava/util/Locale;

    invoke-virtual {v0, v4}, Ljava/lang/String;->toLowerCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v0

    invoke-static {p0, v1, v0, v3}, Lcom/msc/c/n;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    const-string v1, "ServerUrl"

    invoke-virtual {v2, v1, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    invoke-virtual {p0, v2}, Lcom/osp/app/signin/CheckSecurityInfoActivity;->startActivity(Landroid/content/Intent;)V

    :cond_1
    return-void
.end method

.method static synthetic r(Lcom/osp/app/signin/CheckSecurityInfoActivity;)V
    .locals 3

    .prologue
    .line 85
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v0, "CSI"

    const-string v1, "sendSignInCompleteBroadcast"

    invoke-static {v0, v1}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;)V

    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    iget-boolean v1, p0, Lcom/osp/app/signin/CheckSecurityInfoActivity;->A:Z

    if-eqz v1, :cond_2

    const-string v1, "android.intent.action.SAMSUNGACCOUNT_RESIGNIN_COMPLETED"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    :goto_0
    invoke-static {}, Lcom/osp/device/b;->b()Lcom/osp/device/b;

    move-result-object v1

    invoke-virtual {v1}, Lcom/osp/device/b;->l()I

    move-result v1

    const/16 v2, 0xb

    if-le v1, v2, :cond_0

    const/16 v1, 0x20

    invoke-virtual {v0, v1}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    :cond_0
    invoke-virtual {p0, v0}, Lcom/osp/app/signin/CheckSecurityInfoActivity;->sendBroadcast(Landroid/content/Intent;)V

    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v0, "CSI"

    const-string v1, "send signInCompleteIntentV02"

    invoke-static {v0, v1}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;)V

    new-instance v0, Landroid/content/Intent;

    const-string v1, "android.intent.action.REGISTRATION_COMPLETED"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    const-string v1, "login_id"

    invoke-static {p0}, Lcom/osp/app/util/ad;->h(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    invoke-virtual {p0, v0}, Lcom/osp/app/signin/CheckSecurityInfoActivity;->sendBroadcast(Landroid/content/Intent;)V

    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v0, "CSI"

    const-string v1, "send signInCompleteIntentV01"

    invoke-static {v0, v1}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;)V

    invoke-static {}, Lcom/osp/app/signin/SamsungService;->a()Z

    move-result v0

    if-nez v0, :cond_1

    invoke-virtual {p0}, Lcom/osp/app/signin/CheckSecurityInfoActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string v1, "com.msc.action.MARKETING_POPUP_NOTIFICATION"

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v0

    if-eqz v0, :cond_1

    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    const-string v1, "com.msc.action.MARKETING_POPUP_NOTIFICATION"

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    const-string v1, "com.osp.app.signin"

    const-string v2, "com.osp.app.signin.BackgroundModeService"

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->setClassName(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    invoke-virtual {p0, v0}, Lcom/osp/app/signin/CheckSecurityInfoActivity;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;

    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v0, "CSI"

    const-string v1, "startService MarketingPopupNotiIntent"

    invoke-static {v0, v1}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;)V

    :cond_1
    return-void

    :cond_2
    const-string v1, "android.intent.action.SAMSUNGACCOUNT_SIGNIN_COMPLETED"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    goto :goto_0
.end method


# virtual methods
.method protected final a()V
    .locals 5

    .prologue
    const v4, 0x7f0c0190

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 511
    const v0, 0x7f0c00b1

    invoke-virtual {p0, v0}, Lcom/osp/app/signin/CheckSecurityInfoActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/EditText;

    .line 513
    if-eqz v0, :cond_2

    .line 515
    invoke-virtual {v0}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    .line 517
    iget-object v1, p0, Lcom/osp/app/signin/CheckSecurityInfoActivity;->x:Landroid/view/Menu;

    if-eqz v1, :cond_0

    .line 519
    iget-object v1, p0, Lcom/osp/app/signin/CheckSecurityInfoActivity;->x:Landroid/view/Menu;

    invoke-interface {v1, v4}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v1

    invoke-interface {v1, v2}, Landroid/view/MenuItem;->setEnabled(Z)Landroid/view/MenuItem;

    .line 521
    :cond_0
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_4

    .line 523
    iget-object v0, p0, Lcom/osp/app/signin/CheckSecurityInfoActivity;->x:Landroid/view/Menu;

    if-eqz v0, :cond_1

    .line 525
    iget-object v0, p0, Lcom/osp/app/signin/CheckSecurityInfoActivity;->x:Landroid/view/Menu;

    invoke-interface {v0, v4}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v0

    invoke-interface {v0, v3}, Landroid/view/MenuItem;->setEnabled(Z)Landroid/view/MenuItem;

    .line 527
    :cond_1
    iget-object v0, p0, Lcom/osp/app/signin/CheckSecurityInfoActivity;->N:Lcom/osp/app/signin/BottomSoftkeyLinearLayout;

    if-eqz v0, :cond_2

    .line 529
    invoke-static {}, Lcom/osp/device/b;->b()Lcom/osp/device/b;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/osp/device/b;->l(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 531
    iget-object v0, p0, Lcom/osp/app/signin/CheckSecurityInfoActivity;->N:Lcom/osp/app/signin/BottomSoftkeyLinearLayout;

    invoke-static {v3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/osp/app/signin/BottomSoftkeyLinearLayout;->a(Ljava/lang/Boolean;)V

    .line 555
    :cond_2
    :goto_0
    return-void

    .line 534
    :cond_3
    iget-object v0, p0, Lcom/osp/app/signin/CheckSecurityInfoActivity;->N:Lcom/osp/app/signin/BottomSoftkeyLinearLayout;

    invoke-static {v3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/osp/app/signin/BottomSoftkeyLinearLayout;->b(Ljava/lang/Boolean;)V

    goto :goto_0

    .line 539
    :cond_4
    iget-object v0, p0, Lcom/osp/app/signin/CheckSecurityInfoActivity;->x:Landroid/view/Menu;

    if-eqz v0, :cond_5

    .line 541
    iget-object v0, p0, Lcom/osp/app/signin/CheckSecurityInfoActivity;->x:Landroid/view/Menu;

    invoke-interface {v0, v4}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v0

    invoke-interface {v0, v2}, Landroid/view/MenuItem;->setEnabled(Z)Landroid/view/MenuItem;

    .line 543
    :cond_5
    iget-object v0, p0, Lcom/osp/app/signin/CheckSecurityInfoActivity;->N:Lcom/osp/app/signin/BottomSoftkeyLinearLayout;

    if-eqz v0, :cond_2

    .line 545
    invoke-static {}, Lcom/osp/device/b;->b()Lcom/osp/device/b;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/osp/device/b;->l(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_6

    .line 547
    iget-object v0, p0, Lcom/osp/app/signin/CheckSecurityInfoActivity;->N:Lcom/osp/app/signin/BottomSoftkeyLinearLayout;

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/osp/app/signin/BottomSoftkeyLinearLayout;->a(Ljava/lang/Boolean;)V

    goto :goto_0

    .line 550
    :cond_6
    iget-object v0, p0, Lcom/osp/app/signin/CheckSecurityInfoActivity;->N:Lcom/osp/app/signin/BottomSoftkeyLinearLayout;

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/osp/app/signin/BottomSoftkeyLinearLayout;->b(Ljava/lang/Boolean;)V

    goto :goto_0
.end method

.method protected final b()V
    .locals 0

    .prologue
    .line 560
    return-void
.end method

.method public final c()V
    .locals 3

    .prologue
    .line 2587
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v0, "CSI"

    const-string v1, "CancelSignInDual_DB"

    const-string v2, "START"

    invoke-static {v0, v1, v2}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 2589
    invoke-static {p0}, Lcom/osp/app/util/ad;->d(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2591
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v0, "CSI"

    const-string v1, "cancel - CancelSignInDual_DB"

    invoke-static {v0, v1}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 2625
    :goto_0
    return-void

    .line 2597
    :cond_0
    const-string v0, ""

    const-string v1, ""

    const-string v2, ""

    invoke-static {p0, v0, v1, v2}, Lcom/msc/c/e;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 2602
    :try_start_0
    new-instance v0, Lcom/osp/security/identity/d;

    invoke-direct {v0, p0}, Lcom/osp/security/identity/d;-><init>(Landroid/content/Context;)V

    .line 2603
    invoke-virtual {v0}, Lcom/osp/security/identity/d;->k()V
    :try_end_0
    .catch Lcom/osp/security/identity/IdentityException; {:try_start_0 .. :try_end_0} :catch_0

    .line 2610
    :goto_1
    :try_start_1
    new-instance v0, Lcom/osp/security/credential/a;

    invoke-direct {v0, p0}, Lcom/osp/security/credential/a;-><init>(Landroid/content/Context;)V

    .line 2611
    invoke-virtual {v0}, Lcom/osp/security/credential/a;->a()V
    :try_end_1
    .catch Lcom/osp/security/credential/CredentialException; {:try_start_1 .. :try_end_1} :catch_1

    .line 2618
    :goto_2
    :try_start_2
    new-instance v0, Lcom/osp/social/member/d;

    invoke-direct {v0, p0}, Lcom/osp/social/member/d;-><init>(Landroid/content/Context;)V

    .line 2619
    invoke-virtual {v0}, Lcom/osp/social/member/d;->a()V
    :try_end_2
    .catch Lcom/osp/social/member/MemberServiceException; {:try_start_2 .. :try_end_2} :catch_2

    .line 2624
    :goto_3
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v0, "CSI"

    const-string v1, "CancelSignInDual_DB"

    const-string v2, "END"

    invoke-static {v0, v1, v2}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 2604
    :catch_0
    move-exception v0

    invoke-virtual {v0}, Lcom/osp/security/identity/IdentityException;->printStackTrace()V

    goto :goto_1

    .line 2612
    :catch_1
    move-exception v0

    invoke-virtual {v0}, Lcom/osp/security/credential/CredentialException;->printStackTrace()V

    goto :goto_2

    .line 2620
    :catch_2
    move-exception v0

    invoke-virtual {v0}, Lcom/osp/social/member/MemberServiceException;->printStackTrace()V

    goto :goto_3
.end method

.method protected onActivityResult(IILandroid/content/Intent;)V
    .locals 2

    .prologue
    .line 1577
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "onActivityResult RequestCode : "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;)V

    .line 1578
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "onActivityResult resultCode : "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;)V

    .line 1579
    invoke-super {p0, p1, p2, p3}, Lcom/osp/app/util/BaseActivity;->onActivityResult(IILandroid/content/Intent;)V

    .line 1581
    const/16 v0, 0xef

    if-ne p1, v0, :cond_0

    .line 1583
    const/4 v0, -0x1

    invoke-virtual {p0, v0}, Lcom/osp/app/signin/CheckSecurityInfoActivity;->b(I)V

    .line 1584
    invoke-virtual {p0}, Lcom/osp/app/signin/CheckSecurityInfoActivity;->finish()V

    .line 1612
    :goto_0
    return-void

    .line 1588
    :cond_0
    const/16 v0, 0xf4

    if-ne p1, v0, :cond_1

    .line 1590
    const/16 v0, 0x18

    invoke-virtual {p0, v0}, Lcom/osp/app/signin/CheckSecurityInfoActivity;->b(I)V

    .line 1591
    invoke-virtual {p0}, Lcom/osp/app/signin/CheckSecurityInfoActivity;->finish()V

    goto :goto_0

    .line 1595
    :cond_1
    sparse-switch p2, :sswitch_data_0

    goto :goto_0

    .line 1598
    :sswitch_0
    invoke-virtual {p0, p2}, Lcom/osp/app/signin/CheckSecurityInfoActivity;->b(I)V

    .line 1599
    invoke-virtual {p0}, Lcom/osp/app/signin/CheckSecurityInfoActivity;->finish()V

    goto :goto_0

    .line 1602
    :sswitch_1
    invoke-virtual {p0, p2}, Lcom/osp/app/signin/CheckSecurityInfoActivity;->b(I)V

    .line 1603
    invoke-virtual {p0}, Lcom/osp/app/signin/CheckSecurityInfoActivity;->finish()V

    goto :goto_0

    .line 1606
    :sswitch_2
    invoke-virtual {p0, p2}, Lcom/osp/app/signin/CheckSecurityInfoActivity;->b(I)V

    .line 1607
    invoke-virtual {p0}, Lcom/osp/app/signin/CheckSecurityInfoActivity;->finish()V

    goto :goto_0

    .line 1610
    :sswitch_3
    invoke-virtual {p0, p2}, Lcom/osp/app/signin/CheckSecurityInfoActivity;->b(I)V

    .line 1611
    invoke-virtual {p0}, Lcom/osp/app/signin/CheckSecurityInfoActivity;->finish()V

    goto :goto_0

    .line 1595
    :sswitch_data_0
    .sparse-switch
        -0x1 -> :sswitch_0
        0x0 -> :sswitch_2
        0x1 -> :sswitch_1
        0x15 -> :sswitch_3
    .end sparse-switch
.end method

.method public onBackPressed()V
    .locals 3

    .prologue
    .line 565
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v0, "CSI"

    const-string v1, "onBackPressed"

    invoke-static {v0, v1}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 567
    invoke-virtual {p0}, Lcom/osp/app/signin/CheckSecurityInfoActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string v1, "is_cancelable_just_one_activity"

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 569
    const/16 v0, 0xe

    invoke-virtual {p0, v0}, Lcom/osp/app/signin/CheckSecurityInfoActivity;->b(I)V

    .line 572
    :cond_0
    invoke-super {p0}, Lcom/osp/app/util/BaseActivity;->onBackPressed()V

    .line 573
    return-void
.end method

.method public onConfigurationChanged(Landroid/content/res/Configuration;)V
    .locals 1

    .prologue
    .line 2630
    invoke-super {p0, p1}, Lcom/osp/app/util/BaseActivity;->onConfigurationChanged(Landroid/content/res/Configuration;)V

    .line 2632
    iget v0, p1, Landroid/content/res/Configuration;->orientation:I

    invoke-direct {p0, v0}, Lcom/osp/app/signin/CheckSecurityInfoActivity;->a(I)V

    .line 2633
    return-void
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 8

    .prologue
    const/4 v7, 0x4

    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 370
    invoke-super {p0, p1}, Lcom/osp/app/util/BaseActivity;->onCreate(Landroid/os/Bundle;)V

    .line 372
    invoke-static {}, Lcom/osp/device/b;->b()Lcom/osp/device/b;

    move-result-object v0

    invoke-virtual {v0}, Lcom/osp/device/b;->a()Z

    move-result v0

    if-nez v0, :cond_0

    .line 374
    invoke-virtual {p0}, Lcom/osp/app/signin/CheckSecurityInfoActivity;->finish()V

    .line 377
    :cond_0
    invoke-virtual {p0}, Lcom/osp/app/signin/CheckSecurityInfoActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string v3, "client_id"

    invoke-virtual {v0, v3}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/osp/app/signin/CheckSecurityInfoActivity;->F:Ljava/lang/String;

    invoke-virtual {p0}, Lcom/osp/app/signin/CheckSecurityInfoActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string v3, "client_secret"

    invoke-virtual {v0, v3}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/osp/app/signin/CheckSecurityInfoActivity;->G:Ljava/lang/String;

    invoke-virtual {p0}, Lcom/osp/app/signin/CheckSecurityInfoActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string v3, "key_login_id"

    invoke-virtual {v0, v3}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/osp/app/signin/CheckSecurityInfoActivity;->K:Ljava/lang/String;

    invoke-virtual {p0}, Lcom/osp/app/signin/CheckSecurityInfoActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string v3, "key_password"

    invoke-virtual {v0, v3}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/osp/app/signin/CheckSecurityInfoActivity;->H:Ljava/lang/String;

    invoke-virtual {p0}, Lcom/osp/app/signin/CheckSecurityInfoActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string v3, "required_auth"

    invoke-virtual {v0, v3, v1}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v0

    iput-boolean v0, p0, Lcom/osp/app/signin/CheckSecurityInfoActivity;->M:Z

    invoke-virtual {p0}, Lcom/osp/app/signin/CheckSecurityInfoActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string v3, "key_internal_is_resign_in"

    invoke-virtual {v0, v3, v1}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v0

    iput-boolean v0, p0, Lcom/osp/app/signin/CheckSecurityInfoActivity;->A:Z

    iget-object v0, p0, Lcom/osp/app/signin/CheckSecurityInfoActivity;->F:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/osp/app/signin/CheckSecurityInfoActivity;->G:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_2

    :cond_1
    const-string v0, "j5p7ll8g33"

    iput-object v0, p0, Lcom/osp/app/signin/CheckSecurityInfoActivity;->F:Ljava/lang/String;

    const-string v0, "5763D0052DC1462E13751F753384E9A9"

    iput-object v0, p0, Lcom/osp/app/signin/CheckSecurityInfoActivity;->G:Ljava/lang/String;

    :cond_2
    iget-object v0, p0, Lcom/osp/app/signin/CheckSecurityInfoActivity;->H:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_3

    move v0, v1

    :goto_0
    if-nez v0, :cond_7

    .line 379
    invoke-virtual {p0}, Lcom/osp/app/signin/CheckSecurityInfoActivity;->finish()V

    .line 417
    :goto_1
    return-void

    .line 377
    :cond_3
    invoke-virtual {p0}, Lcom/osp/app/signin/CheckSecurityInfoActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string v3, "key_duplicated_user_ids"

    invoke-virtual {v0, v3}, Landroid/content/Intent;->getStringArrayExtra(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v3

    if-eqz v3, :cond_4

    array-length v0, v3

    if-gt v0, v2, :cond_5

    :cond_4
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v0, "CSI"

    const-string v2, "userId array is missing!!"

    invoke-static {v0, v2}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;)V

    move v0, v1

    goto :goto_0

    :cond_5
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/osp/app/signin/CheckSecurityInfoActivity;->J:Ljava/util/ArrayList;

    move v0, v1

    :goto_2
    array-length v4, v3

    if-ge v0, v4, :cond_6

    aget-object v4, v3, v0

    const-string v5, "_"

    invoke-virtual {v4, v5}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v4

    iget-object v5, p0, Lcom/osp/app/signin/CheckSecurityInfoActivity;->J:Ljava/util/ArrayList;

    invoke-virtual {v5, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "passed data : "

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    aget-object v6, v3, v0

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;)V

    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "User ID List : "

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    aget-object v6, v4, v1

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "/"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    aget-object v4, v4, v2

    invoke-virtual {v5, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;)V

    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    :cond_6
    move v0, v2

    goto :goto_0

    .line 383
    :cond_7
    const v0, 0x7f030015

    invoke-static {p0}, Lcom/osp/app/util/r;->q(Landroid/content/Context;)Z

    move-result v2

    invoke-virtual {p0, v0, v2}, Lcom/osp/app/signin/CheckSecurityInfoActivity;->a(IZ)V

    .line 385
    invoke-static {p0}, Lcom/osp/app/util/r;->i(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_8

    .line 387
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v2, 0x13

    if-ge v0, v2, :cond_8

    .line 389
    invoke-static {}, Lcom/osp/device/b;->b()Lcom/osp/device/b;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/osp/device/b;->i(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_8

    .line 391
    invoke-static {p0}, Lcom/msc/sa/c/d;->c(Landroid/app/Activity;)V

    .line 408
    :cond_8
    invoke-static {p0}, Lcom/msc/sa/c/d;->a(Landroid/app/Activity;)V

    .line 410
    invoke-direct {p0}, Lcom/osp/app/signin/CheckSecurityInfoActivity;->d()V

    .line 411
    const v0, 0x7f0c00af

    invoke-virtual {p0, v0}, Lcom/osp/app/signin/CheckSecurityInfoActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v7}, Landroid/view/View;->setVisibility(I)V

    const v0, 0x7f0c00b1

    invoke-virtual {p0, v0}, Lcom/osp/app/signin/CheckSecurityInfoActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/EditText;

    new-instance v2, Lcom/osp/app/signin/dp;

    invoke-direct {v2, p0}, Lcom/osp/app/signin/dp;-><init>(Lcom/osp/app/signin/CheckSecurityInfoActivity;)V

    invoke-virtual {v0, v2}, Landroid/widget/EditText;->addTextChangedListener(Landroid/text/TextWatcher;)V

    invoke-static {}, Lcom/osp/device/b;->b()Lcom/osp/device/b;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/osp/device/b;->i(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_b

    const v0, 0x7f0c0169

    invoke-virtual {p0, v0}, Lcom/osp/app/signin/CheckSecurityInfoActivity;->findViewById(I)Landroid/view/View;

    move-result-object v2

    invoke-static {}, Lcom/osp/app/signin/SamsungService;->a()Z

    move-result v0

    if-eqz v0, :cond_c

    iget-object v0, p0, Lcom/osp/app/signin/CheckSecurityInfoActivity;->N:Lcom/osp/app/signin/BottomSoftkeyLinearLayout;

    if-eqz v0, :cond_9

    iget-object v0, p0, Lcom/osp/app/signin/CheckSecurityInfoActivity;->N:Lcom/osp/app/signin/BottomSoftkeyLinearLayout;

    invoke-virtual {v0, v1}, Lcom/osp/app/signin/BottomSoftkeyLinearLayout;->setVisibility(I)V

    :cond_9
    if-eqz v2, :cond_a

    invoke-virtual {v2}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup$MarginLayoutParams;

    invoke-virtual {p0}, Lcom/osp/app/signin/CheckSecurityInfoActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v3, 0x7f080143

    invoke-virtual {v1, v3}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v1

    float-to-int v1, v1

    iput v1, v0, Landroid/view/ViewGroup$MarginLayoutParams;->topMargin:I

    invoke-virtual {v2, v0}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    :cond_a
    :goto_3
    invoke-static {}, Lcom/osp/app/util/r;->v()Z

    move-result v0

    if-eqz v0, :cond_b

    invoke-virtual {v2, v7}, Landroid/view/View;->setVisibility(I)V

    :cond_b
    invoke-virtual {p0}, Lcom/osp/app/signin/CheckSecurityInfoActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v0

    iget v0, v0, Landroid/content/res/Configuration;->orientation:I

    invoke-direct {p0, v0}, Lcom/osp/app/signin/CheckSecurityInfoActivity;->a(I)V

    .line 413
    invoke-virtual {p0}, Lcom/osp/app/signin/CheckSecurityInfoActivity;->a()V

    .line 415
    new-instance v0, Lcom/osp/app/signin/dv;

    invoke-direct {v0, p0}, Lcom/osp/app/signin/dv;-><init>(Lcom/osp/app/signin/CheckSecurityInfoActivity;)V

    iput-object v0, p0, Lcom/osp/app/signin/CheckSecurityInfoActivity;->B:Lcom/osp/app/signin/dv;

    iget-object v0, p0, Lcom/osp/app/signin/CheckSecurityInfoActivity;->B:Lcom/osp/app/signin/dv;

    invoke-virtual {v0}, Lcom/osp/app/signin/dv;->b()V

    goto/16 :goto_1

    .line 411
    :cond_c
    const-string v0, "confirm_cancel"

    invoke-virtual {p0, v0}, Lcom/osp/app/signin/CheckSecurityInfoActivity;->e(Ljava/lang/String;)V

    invoke-static {p0}, Lcom/msc/sa/c/d;->e(Landroid/app/Activity;)V

    iget-object v0, p0, Lcom/osp/app/signin/CheckSecurityInfoActivity;->N:Lcom/osp/app/signin/BottomSoftkeyLinearLayout;

    if-eqz v0, :cond_a

    iget-object v0, p0, Lcom/osp/app/signin/CheckSecurityInfoActivity;->N:Lcom/osp/app/signin/BottomSoftkeyLinearLayout;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Lcom/osp/app/signin/BottomSoftkeyLinearLayout;->setVisibility(I)V

    goto :goto_3
.end method

.method public onCreateDialog(I)Landroid/app/Dialog;
    .locals 5

    .prologue
    const v4, 0x7f090107

    const v2, 0x7f09004f

    const v3, 0x7f090019

    .line 1372
    packed-switch p1, :pswitch_data_0

    .line 1389
    invoke-super {p0, p1}, Lcom/osp/app/util/BaseActivity;->onCreateDialog(I)Landroid/app/Dialog;

    move-result-object v0

    :goto_0
    return-object v0

    .line 1376
    :pswitch_0
    new-instance v0, Lcom/osp/app/signin/dt;

    invoke-direct {v0, p0}, Lcom/osp/app/signin/dt;-><init>(Lcom/osp/app/signin/CheckSecurityInfoActivity;)V

    new-instance v1, Landroid/app/AlertDialog$Builder;

    invoke-direct {v1, p0}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    invoke-virtual {v1, v4}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    const v2, 0x7f090108

    invoke-virtual {v1, v2}, Landroid/app/AlertDialog$Builder;->setMessage(I)Landroid/app/AlertDialog$Builder;

    invoke-virtual {v1, v4, v0}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    invoke-virtual {v1}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v0

    goto :goto_0

    .line 1378
    :pswitch_1
    new-instance v0, Lcom/osp/app/signin/ds;

    invoke-direct {v0, p0}, Lcom/osp/app/signin/ds;-><init>(Lcom/osp/app/signin/CheckSecurityInfoActivity;)V

    new-instance v1, Landroid/app/AlertDialog$Builder;

    invoke-direct {v1, p0}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    invoke-virtual {p0, v2}, Lcom/osp/app/signin/CheckSecurityInfoActivity;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/app/AlertDialog$Builder;->setTitle(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    const v2, 0x7f09013f

    invoke-virtual {v1, v2}, Landroid/app/AlertDialog$Builder;->setMessage(I)Landroid/app/AlertDialog$Builder;

    const v2, 0x7f090104

    invoke-virtual {p0, v2}, Lcom/osp/app/signin/CheckSecurityInfoActivity;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2, v0}, Landroid/app/AlertDialog$Builder;->setPositiveButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    invoke-virtual {p0, v3}, Lcom/osp/app/signin/CheckSecurityInfoActivity;->getString(I)Ljava/lang/String;

    move-result-object v0

    const/4 v2, 0x0

    invoke-virtual {v1, v0, v2}, Landroid/app/AlertDialog$Builder;->setNegativeButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    invoke-virtual {v1}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v0

    goto :goto_0

    .line 1381
    :pswitch_2
    new-instance v0, Lcom/osp/app/signin/dr;

    invoke-direct {v0, p0}, Lcom/osp/app/signin/dr;-><init>(Lcom/osp/app/signin/CheckSecurityInfoActivity;)V

    new-instance v1, Landroid/app/AlertDialog$Builder;

    invoke-direct {v1, p0}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    invoke-virtual {v1, v2}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    const v2, 0x7f090110

    invoke-virtual {v1, v2}, Landroid/app/AlertDialog$Builder;->setMessage(I)Landroid/app/AlertDialog$Builder;

    const v2, 0x7f090063

    invoke-virtual {v1, v2, v0}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    iget-object v0, p0, Lcom/osp/app/signin/CheckSecurityInfoActivity;->e:Landroid/content/DialogInterface$OnCancelListener;

    invoke-virtual {v1, v0}, Landroid/app/AlertDialog$Builder;->setOnCancelListener(Landroid/content/DialogInterface$OnCancelListener;)Landroid/app/AlertDialog$Builder;

    invoke-virtual {v1}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v0

    goto :goto_0

    .line 1384
    :pswitch_3
    new-instance v0, Lcom/osp/app/signin/dq;

    invoke-direct {v0, p0}, Lcom/osp/app/signin/dq;-><init>(Lcom/osp/app/signin/CheckSecurityInfoActivity;)V

    new-instance v1, Landroid/app/AlertDialog$Builder;

    invoke-direct {v1, p0}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    const v2, 0x7f090116

    invoke-virtual {v1, v2}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    const v2, 0x7f09019a

    invoke-virtual {v1, v2}, Landroid/app/AlertDialog$Builder;->setMessage(I)Landroid/app/AlertDialog$Builder;

    invoke-virtual {v1, v3, v0}, Landroid/app/AlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    const v2, 0x7f090042

    invoke-virtual {v1, v2, v0}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    invoke-virtual {v1}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v0

    goto/16 :goto_0

    .line 1372
    :pswitch_data_0
    .packed-switch 0x64
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method protected onDestroy()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 1344
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v0, "CSI"

    const-string v1, "onDestroy"

    invoke-static {v0, v1}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 1346
    iget-object v0, p0, Lcom/osp/app/signin/CheckSecurityInfoActivity;->B:Lcom/osp/app/signin/dv;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/osp/app/signin/CheckSecurityInfoActivity;->B:Lcom/osp/app/signin/dv;

    invoke-virtual {v0}, Lcom/osp/app/signin/dv;->getStatus()Landroid/os/AsyncTask$Status;

    move-result-object v0

    sget-object v1, Landroid/os/AsyncTask$Status;->RUNNING:Landroid/os/AsyncTask$Status;

    if-ne v0, v1, :cond_0

    .line 1348
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v0, "CSI"

    const-string v1, "GetSecurityInfoTask cancelTask"

    invoke-static {v0, v1}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 1349
    iget-object v0, p0, Lcom/osp/app/signin/CheckSecurityInfoActivity;->B:Lcom/osp/app/signin/dv;

    invoke-virtual {v0}, Lcom/osp/app/signin/dv;->d()V

    .line 1350
    iput-object v2, p0, Lcom/osp/app/signin/CheckSecurityInfoActivity;->B:Lcom/osp/app/signin/dv;

    .line 1353
    :cond_0
    iget-object v0, p0, Lcom/osp/app/signin/CheckSecurityInfoActivity;->C:Lcom/osp/app/signin/du;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/osp/app/signin/CheckSecurityInfoActivity;->C:Lcom/osp/app/signin/du;

    invoke-virtual {v0}, Lcom/osp/app/signin/du;->getStatus()Landroid/os/AsyncTask$Status;

    move-result-object v0

    sget-object v1, Landroid/os/AsyncTask$Status;->RUNNING:Landroid/os/AsyncTask$Status;

    if-ne v0, v1, :cond_1

    .line 1355
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v0, "CSI"

    const-string v1, "mCheckSecurityAnswerTask cancelTask"

    invoke-static {v0, v1}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 1356
    iget-object v0, p0, Lcom/osp/app/signin/CheckSecurityInfoActivity;->C:Lcom/osp/app/signin/du;

    invoke-virtual {v0}, Lcom/osp/app/signin/du;->d()V

    .line 1357
    iput-object v2, p0, Lcom/osp/app/signin/CheckSecurityInfoActivity;->C:Lcom/osp/app/signin/du;

    .line 1360
    :cond_1
    iget-object v0, p0, Lcom/osp/app/signin/CheckSecurityInfoActivity;->L:Lcom/osp/app/signin/dx;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/osp/app/signin/CheckSecurityInfoActivity;->L:Lcom/osp/app/signin/dx;

    invoke-virtual {v0}, Lcom/osp/app/signin/dx;->getStatus()Landroid/os/AsyncTask$Status;

    move-result-object v0

    sget-object v1, Landroid/os/AsyncTask$Status;->RUNNING:Landroid/os/AsyncTask$Status;

    if-ne v0, v1, :cond_2

    .line 1362
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v0, "CSI"

    const-string v1, "mSignInNewDualTask cancelTask"

    invoke-static {v0, v1}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 1363
    iget-object v0, p0, Lcom/osp/app/signin/CheckSecurityInfoActivity;->L:Lcom/osp/app/signin/dx;

    invoke-virtual {v0}, Lcom/osp/app/signin/dx;->d()V

    .line 1364
    iput-object v2, p0, Lcom/osp/app/signin/CheckSecurityInfoActivity;->L:Lcom/osp/app/signin/dx;

    .line 1367
    :cond_2
    invoke-super {p0}, Lcom/osp/app/util/BaseActivity;->onDestroy()V

    .line 1368
    return-void
.end method

.method public onOptionsItemSelected(Landroid/view/MenuItem;)Z
    .locals 5

    .prologue
    const/4 v0, 0x0

    const/4 v1, 0x1

    .line 1272
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v2, "CSI"

    const-string v3, "onOptionsItemSelected"

    const-string v4, "START"

    invoke-static {v2, v3, v4}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 1274
    invoke-static {}, Lcom/osp/device/b;->b()Lcom/osp/device/b;

    move-result-object v2

    invoke-virtual {v2, p0}, Lcom/osp/device/b;->i(Landroid/content/Context;)Z

    move-result v2

    if-nez v2, :cond_0

    sget v2, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v3, 0xb

    if-ge v2, v3, :cond_0

    .line 1338
    :goto_0
    return v0

    .line 1279
    :cond_0
    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v2

    sparse-switch v2, :sswitch_data_0

    .line 1333
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v0, "CSI"

    const-string v1, "onOptionsItemSelected"

    const-string v2, "END"

    invoke-static {v0, v1, v2}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 1334
    invoke-super {p0, p1}, Lcom/osp/app/util/BaseActivity;->onOptionsItemSelected(Landroid/view/MenuItem;)Z

    move-result v0

    goto :goto_0

    .line 1282
    :sswitch_0
    invoke-virtual {p0}, Lcom/osp/app/signin/CheckSecurityInfoActivity;->onBackPressed()V

    .line 1283
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v0, "CSI"

    const-string v2, "onOptionsItemSelected"

    const-string v3, "END"

    invoke-static {v0, v2, v3}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    move v0, v1

    .line 1284
    goto :goto_0

    .line 1288
    :sswitch_1
    invoke-direct {p0}, Lcom/osp/app/signin/CheckSecurityInfoActivity;->e()Ljava/lang/String;

    move-result-object v2

    .line 1289
    const-string v3, "/"

    invoke-virtual {v2, v3}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v2

    .line 1292
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "SelectedAnswerId = "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    aget-object v4, v2, v0

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "InputAnswer"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    aget-object v4, v2, v1

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;)V

    .line 1294
    aget-object v3, v2, v0

    invoke-static {v3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_1

    aget-object v3, v2, v1

    invoke-static {v3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_1

    .line 1296
    new-instance v3, Lcom/osp/app/signin/du;

    aget-object v0, v2, v0

    aget-object v2, v2, v1

    invoke-direct {v3, p0, v0, v2}, Lcom/osp/app/signin/du;-><init>(Lcom/osp/app/signin/CheckSecurityInfoActivity;Ljava/lang/String;Ljava/lang/String;)V

    iput-object v3, p0, Lcom/osp/app/signin/CheckSecurityInfoActivity;->C:Lcom/osp/app/signin/du;

    .line 1297
    iget-object v0, p0, Lcom/osp/app/signin/CheckSecurityInfoActivity;->C:Lcom/osp/app/signin/du;

    invoke-virtual {v0}, Lcom/osp/app/signin/du;->b()V

    .line 1337
    :goto_1
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v0, "CSI"

    const-string v2, "onOptionsItemSelected"

    const-string v3, "END"

    invoke-static {v0, v2, v3}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    move v0, v1

    .line 1338
    goto/16 :goto_0

    .line 1300
    :cond_1
    invoke-static {}, Lcom/osp/app/util/ap;->a()Lcom/osp/app/util/ap;

    const v0, 0x7f09004a

    invoke-static {p0, v0, v1}, Lcom/osp/app/util/ap;->a(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    goto :goto_1

    .line 1329
    :sswitch_2
    invoke-virtual {p0}, Lcom/osp/app/signin/CheckSecurityInfoActivity;->onBackPressed()V

    .line 1330
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v0, "CSI"

    const-string v2, "onOptionsItemSelected"

    const-string v3, "END"

    invoke-static {v0, v2, v3}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1

    .line 1279
    :sswitch_data_0
    .sparse-switch
        0x102002c -> :sswitch_0
        0x7f0c018e -> :sswitch_2
        0x7f0c0190 -> :sswitch_1
    .end sparse-switch
.end method
