.class public Lcom/osp/app/signin/WeiboInfoWebView;
.super Lcom/osp/app/util/BaseActivity;
.source "WeiboInfoWebView.java"


# instance fields
.field private final A:Ljava/lang/String;

.field private final B:Ljava/lang/String;

.field private final C:Ljava/lang/String;

.field private final D:Ljava/lang/String;

.field private final E:Ljava/lang/String;

.field private final F:Ljava/lang/String;

.field private final G:Ljava/lang/String;

.field private final H:Ljava/lang/String;

.field private I:I

.field private J:Landroid/content/Context;

.field private K:Landroid/content/Intent;

.field private L:Landroid/app/AlertDialog;

.field private M:Landroid/app/ProgressDialog;

.field private N:Landroid/webkit/WebView;

.field private O:Landroid/widget/LinearLayout;

.field private P:Landroid/widget/Button;

.field private Q:Z

.field private R:Lcom/osp/app/signin/pz;

.field private final a:Ljava/lang/String;

.field private final b:Ljava/lang/String;

.field private final c:Ljava/lang/String;

.field private final d:Ljava/lang/String;

.field private final e:Ljava/lang/String;

.field private final f:Ljava/lang/String;

.field private final y:Ljava/lang/String;

.field private final z:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 54
    invoke-direct {p0}, Lcom/osp/app/util/BaseActivity;-><init>()V

    .line 58
    const-string v0, "https://api.weibo.com/oauth2/authorize"

    iput-object v0, p0, Lcom/osp/app/signin/WeiboInfoWebView;->a:Ljava/lang/String;

    .line 59
    const-string v0, "https://api.weibo.com/2/proxy/account/username.json"

    iput-object v0, p0, Lcom/osp/app/signin/WeiboInfoWebView;->b:Ljava/lang/String;

    .line 61
    const-string v0, "2262907817"

    iput-object v0, p0, Lcom/osp/app/signin/WeiboInfoWebView;->c:Ljava/lang/String;

    .line 63
    const-string v0, "0d6bb20d354792ad509315426a5e70d0"

    iput-object v0, p0, Lcom/osp/app/signin/WeiboInfoWebView;->d:Ljava/lang/String;

    .line 64
    const-string v0, "www.samsung.com/cn/"

    iput-object v0, p0, Lcom/osp/app/signin/WeiboInfoWebView;->e:Ljava/lang/String;

    .line 65
    const-string v0, ""

    iput-object v0, p0, Lcom/osp/app/signin/WeiboInfoWebView;->f:Ljava/lang/String;

    .line 67
    const-string v0, "client_id"

    iput-object v0, p0, Lcom/osp/app/signin/WeiboInfoWebView;->y:Ljava/lang/String;

    .line 68
    const-string v0, "response_type"

    iput-object v0, p0, Lcom/osp/app/signin/WeiboInfoWebView;->z:Ljava/lang/String;

    .line 69
    const-string v0, "redirect_uri"

    iput-object v0, p0, Lcom/osp/app/signin/WeiboInfoWebView;->A:Ljava/lang/String;

    .line 70
    const-string v0, "display"

    iput-object v0, p0, Lcom/osp/app/signin/WeiboInfoWebView;->B:Ljava/lang/String;

    .line 71
    const-string v0, "scope"

    iput-object v0, p0, Lcom/osp/app/signin/WeiboInfoWebView;->C:Ljava/lang/String;

    .line 72
    const-string v0, "packagename"

    iput-object v0, p0, Lcom/osp/app/signin/WeiboInfoWebView;->D:Ljava/lang/String;

    .line 73
    const-string v0, "key_hash"

    iput-object v0, p0, Lcom/osp/app/signin/WeiboInfoWebView;->E:Ljava/lang/String;

    .line 74
    const-string v0, "source"

    iput-object v0, p0, Lcom/osp/app/signin/WeiboInfoWebView;->F:Ljava/lang/String;

    .line 75
    const-string v0, "ip"

    iput-object v0, p0, Lcom/osp/app/signin/WeiboInfoWebView;->G:Ljava/lang/String;

    .line 76
    const-string v0, "1"

    iput-object v0, p0, Lcom/osp/app/signin/WeiboInfoWebView;->H:Ljava/lang/String;

    .line 78
    const/4 v0, 0x0

    iput v0, p0, Lcom/osp/app/signin/WeiboInfoWebView;->I:I

    .line 80
    iput-object v1, p0, Lcom/osp/app/signin/WeiboInfoWebView;->J:Landroid/content/Context;

    .line 81
    iput-object v1, p0, Lcom/osp/app/signin/WeiboInfoWebView;->K:Landroid/content/Intent;

    .line 83
    iput-object v1, p0, Lcom/osp/app/signin/WeiboInfoWebView;->L:Landroid/app/AlertDialog;

    .line 84
    iput-object v1, p0, Lcom/osp/app/signin/WeiboInfoWebView;->M:Landroid/app/ProgressDialog;

    .line 86
    iput-object v1, p0, Lcom/osp/app/signin/WeiboInfoWebView;->N:Landroid/webkit/WebView;

    .line 571
    return-void
.end method

.method static synthetic a(Lcom/osp/app/signin/WeiboInfoWebView;Landroid/app/ProgressDialog;)Landroid/app/ProgressDialog;
    .locals 0

    .prologue
    .line 54
    iput-object p1, p0, Lcom/osp/app/signin/WeiboInfoWebView;->M:Landroid/app/ProgressDialog;

    return-object p1
.end method

.method static synthetic a(Lcom/osp/app/signin/WeiboInfoWebView;)Lcom/osp/app/signin/pz;
    .locals 1

    .prologue
    .line 54
    iget-object v0, p0, Lcom/osp/app/signin/WeiboInfoWebView;->R:Lcom/osp/app/signin/pz;

    return-object v0
.end method

.method static synthetic a(Lcom/osp/app/signin/WeiboInfoWebView;Lcom/osp/app/signin/pz;)Lcom/osp/app/signin/pz;
    .locals 0

    .prologue
    .line 54
    iput-object p1, p0, Lcom/osp/app/signin/WeiboInfoWebView;->R:Lcom/osp/app/signin/pz;

    return-object p1
.end method

.method public static a(Ljava/lang/String;)Lcom/osp/app/signin/pz;
    .locals 3

    .prologue
    .line 820
    new-instance v1, Lcom/osp/app/signin/pz;

    invoke-direct {v1}, Lcom/osp/app/signin/pz;-><init>()V

    .line 824
    :try_start_0
    new-instance v0, Lorg/json/JSONObject;

    invoke-direct {v0, p0}, Lorg/json/JSONObject;-><init>(Ljava/lang/String;)V

    .line 825
    const-string v2, "email"

    invoke-virtual {v0, v2}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 827
    const-string v2, "email"

    invoke-virtual {v0, v2}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/osp/app/signin/pz;->c(Ljava/lang/String;)V

    .line 828
    invoke-virtual {v1}, Lcom/osp/app/signin/pz;->c()Ljava/lang/String;

    move-result-object v2

    if-eqz v2, :cond_0

    invoke-virtual {v1}, Lcom/osp/app/signin/pz;->c()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/String;->isEmpty()Z

    move-result v2

    if-nez v2, :cond_0

    .line 830
    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Lcom/osp/app/signin/pz;->a(I)V

    .line 833
    :cond_0
    const-string v2, "mobile"

    invoke-virtual {v0, v2}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 835
    const-string v2, "mobile"

    invoke-virtual {v0, v2}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/osp/app/signin/pz;->f(Ljava/lang/String;)V

    .line 836
    invoke-virtual {v1}, Lcom/osp/app/signin/pz;->f()Ljava/lang/String;

    move-result-object v2

    if-eqz v2, :cond_1

    invoke-virtual {v1}, Lcom/osp/app/signin/pz;->f()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/String;->isEmpty()Z

    move-result v2

    if-nez v2, :cond_1

    .line 838
    const/4 v2, 0x2

    invoke-virtual {v1, v2}, Lcom/osp/app/signin/pz;->a(I)V

    .line 841
    :cond_1
    const-string v2, "error_code"

    invoke-virtual {v0, v2}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 843
    const-string v2, "error_code"

    invoke-virtual {v0, v2}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/osp/app/signin/pz;->g(Ljava/lang/String;)V

    .line 845
    :cond_2
    const-string v2, "state"

    invoke-virtual {v0, v2}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_3

    .line 847
    const-string v2, "state"

    invoke-virtual {v0, v2}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 848
    const/4 v2, 0x0

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/osp/app/signin/pz;->a(Ljava/lang/Boolean;)V

    .line 849
    const-string v2, "1"

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 851
    const/4 v0, 0x1

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    invoke-virtual {v1, v0}, Lcom/osp/app/signin/pz;->a(Ljava/lang/Boolean;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 858
    :cond_3
    :goto_0
    return-object v1

    .line 854
    :catch_0
    move-exception v0

    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_0
.end method

.method private static a(Lcom/msc/d/a/a;)Ljava/lang/String;
    .locals 8

    .prologue
    const/4 v1, 0x0

    .line 344
    if-nez p0, :cond_0

    .line 346
    const-string v0, ""

    .line 375
    :goto_0
    return-object v0

    .line 349
    :cond_0
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    .line 350
    const/4 v0, 0x1

    .line 351
    invoke-virtual {p0}, Lcom/msc/d/a/a;->a()I

    move-result v4

    move v2, v1

    .line 352
    :goto_1
    if-ge v2, v4, :cond_4

    .line 354
    if-eqz v0, :cond_2

    move v0, v1

    .line 361
    :goto_2
    invoke-virtual {p0, v2}, Lcom/msc/d/a/a;->a(I)Ljava/lang/String;

    move-result-object v5

    .line 362
    invoke-virtual {p0, v5}, Lcom/msc/d/a/a;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    .line 363
    if-nez v6, :cond_3

    .line 365
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    new-instance v6, Ljava/lang/StringBuilder;

    const-string v7, "encodeUrl key:"

    invoke-direct {v6, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v6, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, " \'s value is null"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;)V

    .line 373
    :cond_1
    :goto_3
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "encodeUrl : "

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;)V

    .line 352
    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    .line 359
    :cond_2
    const/16 v5, 0x26

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    goto :goto_2

    .line 368
    :cond_3
    invoke-virtual {p0, v2}, Lcom/msc/d/a/a;->b(I)Ljava/lang/String;

    move-result-object v5

    if-eqz v5, :cond_1

    .line 370
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0, v2}, Lcom/msc/d/a/a;->a(I)Ljava/lang/String;

    move-result-object v6

    invoke-static {v6}, Ljava/net/URLEncoder;->encode(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "="

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {p0, v2}, Lcom/msc/d/a/a;->b(I)Ljava/lang/String;

    move-result-object v6

    invoke-static {v6}, Ljava/net/URLEncoder;->encode(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_3

    .line 375
    :cond_4
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_0
.end method

.method static synthetic b(Lcom/osp/app/signin/WeiboInfoWebView;)Landroid/content/Context;
    .locals 1

    .prologue
    .line 54
    iget-object v0, p0, Lcom/osp/app/signin/WeiboInfoWebView;->J:Landroid/content/Context;

    return-object v0
.end method

.method static synthetic b(Ljava/lang/String;)Lcom/osp/app/signin/pz;
    .locals 8

    .prologue
    const/4 v7, 0x1

    const/4 v1, 0x0

    .line 54
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v2, "parseFromResult params = "

    invoke-direct {v0, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;)V

    new-instance v2, Lcom/osp/app/signin/pz;

    invoke-direct {v2}, Lcom/osp/app/signin/pz;-><init>()V

    if-eqz p0, :cond_3

    invoke-virtual {p0}, Ljava/lang/String;->length()I

    move-result v0

    if-lez v0, :cond_3

    const-string v0, "&"

    invoke-virtual {p0, v0}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v3

    if-eqz v3, :cond_3

    array-length v0, v3

    if-lez v0, :cond_3

    move v0, v1

    :goto_0
    array-length v4, v3

    if-ge v0, v4, :cond_3

    aget-object v4, v3, v0

    const-string v5, "="

    const/4 v6, 0x2

    invoke-virtual {v4, v5, v6}, Ljava/lang/String;->split(Ljava/lang/String;I)[Ljava/lang/String;

    move-result-object v4

    if-eqz v4, :cond_0

    array-length v5, v4

    if-lez v5, :cond_0

    aget-object v5, v4, v1

    const-string v6, "access_token"

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_1

    aget-object v4, v4, v7

    invoke-virtual {v2, v4}, Lcom/osp/app/signin/pz;->a(Ljava/lang/String;)V

    :cond_0
    :goto_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_1
    aget-object v5, v4, v1

    const-string v6, "uid"

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_2

    aget-object v4, v4, v7

    invoke-virtual {v2, v4}, Lcom/osp/app/signin/pz;->b(Ljava/lang/String;)V

    goto :goto_1

    :cond_2
    aget-object v5, v4, v1

    const-string v6, "error_code"

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_0

    aget-object v4, v4, v7

    invoke-virtual {v2, v4}, Lcom/osp/app/signin/pz;->g(Ljava/lang/String;)V

    goto :goto_1

    :cond_3
    return-object v2
.end method

.method static synthetic c(Lcom/osp/app/signin/WeiboInfoWebView;)I
    .locals 1

    .prologue
    .line 54
    iget v0, p0, Lcom/osp/app/signin/WeiboInfoWebView;->I:I

    return v0
.end method

.method static synthetic d(Lcom/osp/app/signin/WeiboInfoWebView;)Landroid/webkit/WebView;
    .locals 1

    .prologue
    .line 54
    iget-object v0, p0, Lcom/osp/app/signin/WeiboInfoWebView;->N:Landroid/webkit/WebView;

    return-object v0
.end method

.method static synthetic d()Ljava/lang/String;
    .locals 3

    .prologue
    .line 54
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v0, "WIWV"

    const-string v1, "makeWeiboUsernameURL()"

    invoke-static {v0, v1}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;)V

    new-instance v0, Lcom/msc/d/a/a;

    invoke-direct {v0}, Lcom/msc/d/a/a;-><init>()V

    const-string v1, "source"

    const-string v2, "2262907817"

    invoke-virtual {v0, v1, v2}, Lcom/msc/d/a/a;->a(Ljava/lang/String;Ljava/lang/String;)V

    const-string v1, "ip"

    invoke-static {}, Lcom/osp/app/signin/WeiboInfoWebView;->e()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/msc/d/a/a;->a(Ljava/lang/String;Ljava/lang/String;)V

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "https://api.weibo.com/2/proxy/account/username.json?"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-static {v0}, Lcom/osp/app/signin/WeiboInfoWebView;->a(Lcom/msc/d/a/a;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method static synthetic e(Lcom/osp/app/signin/WeiboInfoWebView;)Landroid/content/Intent;
    .locals 1

    .prologue
    .line 54
    iget-object v0, p0, Lcom/osp/app/signin/WeiboInfoWebView;->K:Landroid/content/Intent;

    return-object v0
.end method

.method private static e()Ljava/lang/String;
    .locals 5

    .prologue
    .line 784
    const-string v1, "1.1.1.1"

    .line 787
    :try_start_0
    invoke-static {}, Ljava/net/NetworkInterface;->getNetworkInterfaces()Ljava/util/Enumeration;

    move-result-object v2

    .line 788
    if-eqz v2, :cond_2

    .line 790
    :cond_0
    invoke-interface {v2}, Ljava/util/Enumeration;->hasMoreElements()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 792
    invoke-interface {v2}, Ljava/util/Enumeration;->nextElement()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/net/NetworkInterface;

    .line 793
    invoke-virtual {v0}, Ljava/net/NetworkInterface;->getInetAddresses()Ljava/util/Enumeration;

    move-result-object v3

    :cond_1
    invoke-interface {v3}, Ljava/util/Enumeration;->hasMoreElements()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 795
    invoke-interface {v3}, Ljava/util/Enumeration;->nextElement()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/net/InetAddress;

    .line 796
    invoke-virtual {v0}, Ljava/net/InetAddress;->isLoopbackAddress()Z

    move-result v4

    if-nez v4, :cond_1

    .line 798
    invoke-virtual {v0}, Ljava/net/InetAddress;->hashCode()I

    move-result v0

    invoke-static {v0}, Landroid/text/format/Formatter;->formatIpAddress(I)Ljava/lang/String;
    :try_end_0
    .catch Ljava/net/SocketException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 799
    :try_start_1
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    invoke-static {v0}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;)V
    :try_end_1
    .catch Ljava/net/SocketException; {:try_start_1 .. :try_end_1} :catch_1

    .line 809
    :goto_0
    return-object v0

    :cond_2
    move-object v0, v1

    .line 808
    goto :goto_0

    .line 805
    :catch_0
    move-exception v0

    move-object v0, v1

    goto :goto_0

    :catch_1
    move-exception v1

    goto :goto_0
.end method

.method static synthetic f(Lcom/osp/app/signin/WeiboInfoWebView;)Landroid/app/ProgressDialog;
    .locals 1

    .prologue
    .line 54
    iget-object v0, p0, Lcom/osp/app/signin/WeiboInfoWebView;->M:Landroid/app/ProgressDialog;

    return-object v0
.end method


# virtual methods
.method protected final a()V
    .locals 0

    .prologue
    .line 744
    return-void
.end method

.method public final a(I)V
    .locals 0

    .prologue
    .line 867
    iput p1, p0, Lcom/osp/app/signin/WeiboInfoWebView;->I:I

    .line 868
    return-void
.end method

.method protected final b()V
    .locals 0

    .prologue
    .line 750
    return-void
.end method

.method public final c()I
    .locals 1

    .prologue
    .line 863
    iget v0, p0, Lcom/osp/app/signin/WeiboInfoWebView;->I:I

    return v0
.end method

.method public onBackPressed()V
    .locals 3

    .prologue
    .line 561
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v0, "WIWV"

    const-string v1, "onBackPressed"

    invoke-static {v0, v1}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 563
    invoke-virtual {p0}, Lcom/osp/app/signin/WeiboInfoWebView;->getIntent()Landroid/content/Intent;

    move-result-object v0

    .line 564
    if-eqz v0, :cond_0

    const-string v1, "is_cancelable_just_one_activity"

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 566
    const/16 v1, 0xe

    invoke-virtual {p0, v1, v0}, Lcom/osp/app/signin/WeiboInfoWebView;->a(ILandroid/content/Intent;)V

    .line 568
    :cond_0
    invoke-super {p0}, Lcom/osp/app/util/BaseActivity;->onBackPressed()V

    .line 569
    return-void
.end method

.method public onConfigurationChanged(Landroid/content/res/Configuration;)V
    .locals 2

    .prologue
    .line 97
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v0, "WIWV"

    const-string v1, "onConfigurationChanged()"

    invoke-static {v0, v1}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 99
    invoke-super {p0, p1}, Lcom/osp/app/util/BaseActivity;->onConfigurationChanged(Landroid/content/res/Configuration;)V

    .line 100
    return-void
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 5

    .prologue
    const/4 v4, 0x1

    const/16 v2, 0xe

    const/4 v3, 0x0

    .line 104
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    if-lt v0, v2, :cond_4

    .line 106
    const/4 v0, 0x5

    invoke-virtual {p0, v0}, Lcom/osp/app/signin/WeiboInfoWebView;->d(I)V

    .line 112
    :goto_0
    invoke-super {p0, p1}, Lcom/osp/app/util/BaseActivity;->onCreate(Landroid/os/Bundle;)V

    .line 114
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v0, "WIWV"

    const-string v1, "onCreate()"

    invoke-static {v0, v1}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 117
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    if-ge v0, v2, :cond_0

    .line 119
    invoke-virtual {p0, v4}, Lcom/osp/app/signin/WeiboInfoWebView;->requestWindowFeature(I)Z

    .line 138
    :cond_0
    invoke-static {p0}, Lcom/msc/sa/c/d;->b(Landroid/app/Activity;)V

    .line 140
    const v0, 0x7f03005d

    invoke-virtual {p0, v0}, Lcom/osp/app/signin/WeiboInfoWebView;->setContentView(I)V

    .line 142
    invoke-virtual {p0}, Lcom/osp/app/signin/WeiboInfoWebView;->getIntent()Landroid/content/Intent;

    move-result-object v0

    iput-object v0, p0, Lcom/osp/app/signin/WeiboInfoWebView;->K:Landroid/content/Intent;

    .line 143
    iget-object v0, p0, Lcom/osp/app/signin/WeiboInfoWebView;->K:Landroid/content/Intent;

    invoke-virtual {v0}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    .line 144
    if-nez v0, :cond_1

    .line 146
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v0, "WIWV"

    const-string v1, "onCreate() - intent Action is null."

    invoke-static {v0, v1}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 147
    iget-object v0, p0, Lcom/osp/app/signin/WeiboInfoWebView;->K:Landroid/content/Intent;

    invoke-virtual {p0, v3, v0}, Lcom/osp/app/signin/WeiboInfoWebView;->a(ILandroid/content/Intent;)V

    .line 148
    invoke-virtual {p0}, Lcom/osp/app/signin/WeiboInfoWebView;->finish()V

    .line 151
    :cond_1
    iget-object v0, p0, Lcom/osp/app/signin/WeiboInfoWebView;->O:Landroid/widget/LinearLayout;

    if-nez v0, :cond_2

    const v0, 0x7f0c0028

    invoke-virtual {p0, v0}, Lcom/osp/app/signin/WeiboInfoWebView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcom/osp/app/signin/WeiboInfoWebView;->O:Landroid/widget/LinearLayout;

    iget-object v0, p0, Lcom/osp/app/signin/WeiboInfoWebView;->O:Landroid/widget/LinearLayout;

    invoke-static {p0}, Lcom/osp/app/util/an;->a(Lcom/osp/app/util/BaseActivity;)Lcom/osp/app/util/an;

    const v1, 0x7f0200ff

    invoke-static {v1}, Lcom/osp/app/util/an;->a(I)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setBackgroundResource(I)V

    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    if-lt v0, v2, :cond_2

    iget-object v0, p0, Lcom/osp/app/signin/WeiboInfoWebView;->O:Landroid/widget/LinearLayout;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setVisibility(I)V

    :cond_2
    iget-object v0, p0, Lcom/osp/app/signin/WeiboInfoWebView;->P:Landroid/widget/Button;

    if-nez v0, :cond_3

    const v0, 0x7f0c0155

    invoke-virtual {p0, v0}, Lcom/osp/app/signin/WeiboInfoWebView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lcom/osp/app/signin/WeiboInfoWebView;->P:Landroid/widget/Button;

    iget-object v0, p0, Lcom/osp/app/signin/WeiboInfoWebView;->P:Landroid/widget/Button;

    invoke-static {p0}, Lcom/osp/app/util/an;->a(Lcom/osp/app/util/BaseActivity;)Lcom/osp/app/util/an;

    const v1, 0x7f020020

    invoke-static {v1}, Lcom/osp/app/util/an;->a(I)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setBackgroundResource(I)V

    iget-object v0, p0, Lcom/osp/app/signin/WeiboInfoWebView;->P:Landroid/widget/Button;

    invoke-virtual {p0}, Lcom/osp/app/signin/WeiboInfoWebView;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-static {p0}, Lcom/osp/app/util/an;->a(Lcom/osp/app/util/BaseActivity;)Lcom/osp/app/util/an;

    const v2, 0x7f020030

    invoke-static {v2}, Lcom/osp/app/util/an;->a(I)I

    move-result v2

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getColorStateList(I)Landroid/content/res/ColorStateList;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setTextColor(Landroid/content/res/ColorStateList;)V

    iget-object v0, p0, Lcom/osp/app/signin/WeiboInfoWebView;->P:Landroid/widget/Button;

    const v1, 0x7f0900c7

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setText(I)V

    iget-object v0, p0, Lcom/osp/app/signin/WeiboInfoWebView;->P:Landroid/widget/Button;

    invoke-virtual {p0}, Lcom/osp/app/signin/WeiboInfoWebView;->h()Z

    move-result v1

    invoke-static {p0, v0, v1}, Lcom/msc/sa/c/d;->a(Landroid/content/Context;Landroid/view/View;Z)V

    .line 153
    :cond_3
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v0, "WIWV"

    const-string v1, "initComponent"

    const-string v2, "START"

    invoke-static {v0, v1, v2}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    iput-object p0, p0, Lcom/osp/app/signin/WeiboInfoWebView;->J:Landroid/content/Context;

    const v0, 0x7f0c00fd

    invoke-virtual {p0, v0}, Lcom/osp/app/signin/WeiboInfoWebView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/webkit/WebView;

    iput-object v0, p0, Lcom/osp/app/signin/WeiboInfoWebView;->N:Landroid/webkit/WebView;

    iget-object v0, p0, Lcom/osp/app/signin/WeiboInfoWebView;->P:Landroid/widget/Button;

    new-instance v1, Lcom/osp/app/signin/qa;

    invoke-direct {v1, p0}, Lcom/osp/app/signin/qa;-><init>(Lcom/osp/app/signin/WeiboInfoWebView;)V

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v0, "WIWV"

    const-string v1, "initComponent"

    const-string v2, "END"

    invoke-static {v0, v1, v2}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 155
    iget-object v0, p0, Lcom/osp/app/signin/WeiboInfoWebView;->N:Landroid/webkit/WebView;

    invoke-virtual {v0}, Landroid/webkit/WebView;->getSettings()Landroid/webkit/WebSettings;

    move-result-object v0

    invoke-virtual {v0, v4}, Landroid/webkit/WebSettings;->setJavaScriptEnabled(Z)V

    .line 156
    iget-object v0, p0, Lcom/osp/app/signin/WeiboInfoWebView;->N:Landroid/webkit/WebView;

    invoke-virtual {v0}, Landroid/webkit/WebView;->getSettings()Landroid/webkit/WebSettings;

    move-result-object v0

    invoke-virtual {v0, v3}, Landroid/webkit/WebSettings;->setSavePassword(Z)V

    .line 157
    iget-object v0, p0, Lcom/osp/app/signin/WeiboInfoWebView;->N:Landroid/webkit/WebView;

    new-instance v1, Lcom/osp/app/signin/qb;

    invoke-direct {v1, p0, v3}, Lcom/osp/app/signin/qb;-><init>(Lcom/osp/app/signin/WeiboInfoWebView;B)V

    const-string v2, "JSONOUT"

    invoke-virtual {v0, v1, v2}, Landroid/webkit/WebView;->addJavascriptInterface(Ljava/lang/Object;Ljava/lang/String;)V

    .line 158
    iget-object v0, p0, Lcom/osp/app/signin/WeiboInfoWebView;->N:Landroid/webkit/WebView;

    new-instance v1, Lcom/osp/app/signin/qc;

    invoke-direct {v1, p0}, Lcom/osp/app/signin/qc;-><init>(Lcom/osp/app/signin/WeiboInfoWebView;)V

    invoke-virtual {v0, v1}, Landroid/webkit/WebView;->setWebViewClient(Landroid/webkit/WebViewClient;)V

    .line 160
    iget-object v0, p0, Lcom/osp/app/signin/WeiboInfoWebView;->N:Landroid/webkit/WebView;

    invoke-virtual {v0}, Landroid/webkit/WebView;->clearHistory()V

    .line 161
    iget-object v0, p0, Lcom/osp/app/signin/WeiboInfoWebView;->N:Landroid/webkit/WebView;

    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v1, "WIWV"

    const-string v2, "makeWeiboAuthURL()"

    invoke-static {v1, v2}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;)V

    new-instance v1, Lcom/msc/d/a/a;

    invoke-direct {v1}, Lcom/msc/d/a/a;-><init>()V

    const-string v2, "client_id"

    const-string v3, "2262907817"

    invoke-virtual {v1, v2, v3}, Lcom/msc/d/a/a;->a(Ljava/lang/String;Ljava/lang/String;)V

    const-string v2, "response_type"

    const-string v3, "code"

    invoke-virtual {v1, v2, v3}, Lcom/msc/d/a/a;->a(Ljava/lang/String;Ljava/lang/String;)V

    const-string v2, "redirect_uri"

    const-string v3, "www.samsung.com/cn/"

    invoke-virtual {v1, v2, v3}, Lcom/msc/d/a/a;->a(Ljava/lang/String;Ljava/lang/String;)V

    const-string v2, "display"

    const-string v3, "mobile"

    invoke-virtual {v1, v2, v3}, Lcom/msc/d/a/a;->a(Ljava/lang/String;Ljava/lang/String;)V

    const-string v2, "scope"

    const-string v3, ""

    invoke-virtual {v1, v2, v3}, Lcom/msc/d/a/a;->a(Ljava/lang/String;Ljava/lang/String;)V

    const-string v2, "packagename"

    invoke-virtual {p0}, Lcom/osp/app/signin/WeiboInfoWebView;->getPackageName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Lcom/msc/d/a/a;->a(Ljava/lang/String;Ljava/lang/String;)V

    const-string v2, "key_hash"

    const-string v3, "0d6bb20d354792ad509315426a5e70d0"

    invoke-virtual {v1, v2, v3}, Lcom/msc/d/a/a;->a(Ljava/lang/String;Ljava/lang/String;)V

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "https://api.weibo.com/oauth2/authorize?"

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-static {v1}, Lcom/osp/app/signin/WeiboInfoWebView;->a(Lcom/msc/d/a/a;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/webkit/WebView;->loadUrl(Ljava/lang/String;)V

    .line 162
    return-void

    .line 109
    :cond_4
    const/4 v0, 0x3

    invoke-virtual {p0, v0}, Lcom/osp/app/signin/WeiboInfoWebView;->d(I)V

    goto/16 :goto_0
.end method

.method protected onDestroy()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 503
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v0, "WIWV"

    const-string v1, "onDestroy"

    invoke-static {v0, v1}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 505
    iget-object v0, p0, Lcom/osp/app/signin/WeiboInfoWebView;->N:Landroid/webkit/WebView;

    if-eqz v0, :cond_0

    .line 507
    iget-object v0, p0, Lcom/osp/app/signin/WeiboInfoWebView;->N:Landroid/webkit/WebView;

    invoke-virtual {v0}, Landroid/webkit/WebView;->clearHistory()V

    .line 508
    iget-object v0, p0, Lcom/osp/app/signin/WeiboInfoWebView;->N:Landroid/webkit/WebView;

    invoke-virtual {v0}, Landroid/webkit/WebView;->destroy()V

    .line 509
    iput-object v2, p0, Lcom/osp/app/signin/WeiboInfoWebView;->N:Landroid/webkit/WebView;

    .line 512
    :cond_0
    :try_start_0
    invoke-virtual {p0}, Lcom/osp/app/signin/WeiboInfoWebView;->isFinishing()Z

    move-result v0

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/osp/app/signin/WeiboInfoWebView;->L:Landroid/app/AlertDialog;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/osp/app/signin/WeiboInfoWebView;->L:Landroid/app/AlertDialog;

    invoke-virtual {v0}, Landroid/app/AlertDialog;->isShowing()Z

    move-result v0

    const/4 v1, 0x1

    if-ne v0, v1, :cond_1

    iget-object v0, p0, Lcom/osp/app/signin/WeiboInfoWebView;->L:Landroid/app/AlertDialog;

    invoke-virtual {v0}, Landroid/app/AlertDialog;->dismiss()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :cond_1
    :goto_0
    iput-object v2, p0, Lcom/osp/app/signin/WeiboInfoWebView;->L:Landroid/app/AlertDialog;

    .line 514
    iget-object v0, p0, Lcom/osp/app/signin/WeiboInfoWebView;->M:Landroid/app/ProgressDialog;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/osp/app/signin/WeiboInfoWebView;->M:Landroid/app/ProgressDialog;

    invoke-virtual {v0}, Landroid/app/ProgressDialog;->isShowing()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 516
    iget-object v0, p0, Lcom/osp/app/signin/WeiboInfoWebView;->M:Landroid/app/ProgressDialog;

    invoke-virtual {v0}, Landroid/app/ProgressDialog;->dismiss()V

    .line 517
    iput-object v2, p0, Lcom/osp/app/signin/WeiboInfoWebView;->M:Landroid/app/ProgressDialog;

    .line 520
    :cond_2
    invoke-super {p0}, Lcom/osp/app/util/BaseActivity;->onDestroy()V

    .line 521
    return-void

    .line 512
    :catch_0
    move-exception v0

    :try_start_1
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v0

    iput-object v2, p0, Lcom/osp/app/signin/WeiboInfoWebView;->L:Landroid/app/AlertDialog;

    throw v0
.end method

.method public onOptionsItemSelected(Landroid/view/MenuItem;)Z
    .locals 2

    .prologue
    .line 166
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0xb

    if-lt v0, v1, :cond_0

    .line 168
    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    .line 176
    const/4 v0, 0x0

    .line 179
    :goto_0
    return v0

    .line 171
    :pswitch_0
    iget-object v0, p0, Lcom/osp/app/signin/WeiboInfoWebView;->N:Landroid/webkit/WebView;

    invoke-virtual {v0}, Landroid/webkit/WebView;->clearHistory()V

    .line 172
    invoke-virtual {p0}, Lcom/osp/app/signin/WeiboInfoWebView;->finish()V

    .line 179
    :cond_0
    const/4 v0, 0x1

    goto :goto_0

    .line 168
    nop

    :pswitch_data_0
    .packed-switch 0x102002c
        :pswitch_0
    .end packed-switch
.end method

.method protected onPause()V
    .locals 2

    .prologue
    .line 525
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v0, "WIWV"

    const-string v1, "onPause"

    invoke-static {v0, v1}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 527
    invoke-super {p0}, Lcom/osp/app/util/BaseActivity;->onPause()V

    .line 528
    return-void
.end method

.method protected onResume()V
    .locals 2

    .prologue
    .line 532
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v0, "WIWV"

    const-string v1, "onResume"

    invoke-static {v0, v1}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 534
    iget-boolean v0, p0, Lcom/osp/app/signin/WeiboInfoWebView;->Q:Z

    if-eqz v0, :cond_0

    invoke-static {p0}, Lcom/osp/app/util/ad;->d(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 546
    invoke-virtual {p0}, Lcom/osp/app/signin/WeiboInfoWebView;->finish()V

    .line 549
    :cond_0
    invoke-super {p0}, Lcom/osp/app/util/BaseActivity;->onResume()V

    .line 550
    return-void
.end method

.method protected onStop()V
    .locals 2

    .prologue
    .line 554
    invoke-super {p0}, Lcom/osp/app/util/BaseActivity;->onStop()V

    .line 556
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v0, "WIWV"

    const-string v1, "onStop"

    invoke-static {v0, v1}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 557
    return-void
.end method
