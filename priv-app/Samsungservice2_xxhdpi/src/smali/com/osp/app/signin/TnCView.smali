.class public Lcom/osp/app/signin/TnCView;
.super Lcom/osp/app/util/BaseActivity;
.source "TnCView.java"


# instance fields
.field private A:I

.field private final B:Ljava/lang/String;

.field private final C:Ljava/lang/String;

.field private D:Z

.field private E:Z

.field private F:Z

.field private G:Z

.field private H:Z

.field private I:Z

.field private J:Landroid/app/AlertDialog;

.field private K:Landroid/widget/ListView;

.field private L:Lcom/osp/app/signin/op;

.field private M:Lcom/osp/app/signin/oj;

.field private N:Z

.field private O:Z

.field private P:Lcom/osp/app/signin/ob;

.field private Q:Lcom/osp/app/signin/oc;

.field private R:Lcom/osp/app/signin/od;

.field private S:Lcom/osp/app/signin/oe;

.field private final T:Lcom/osp/app/signin/kd;

.field private U:Z

.field private V:Z

.field private W:Z

.field private X:Z

.field private Y:Ljava/lang/String;

.field private Z:Ljava/lang/String;

.field private a:Landroid/content/Intent;

.field private aA:Z

.field private aB:Z

.field private aC:Z

.field private aD:Z

.field private aE:Z

.field private aF:Z

.field private aG:Ljava/lang/String;

.field private aH:Z

.field private aI:J

.field private aJ:Ljava/lang/String;

.field private aK:Lcom/osp/app/signin/BottomSoftkeyLinearLayout;

.field private aL:Z

.field private aM:Z

.field private aN:Landroid/widget/CheckBox;

.field private aO:Landroid/widget/CheckBox;

.field private aP:Landroid/widget/CheckBox;

.field private aQ:Landroid/widget/CheckBox;

.field private aR:Landroid/widget/CheckBox;

.field private aS:Landroid/widget/CheckBox;

.field private aT:Z

.field private aU:Z

.field private aV:Landroid/os/Bundle;

.field private aW:J

.field private aX:Z

.field private aY:Z

.field private aZ:Lcom/osp/app/signin/ok;

.field private aa:Ljava/lang/String;

.field private ab:Ljava/lang/String;

.field private ac:[Ljava/lang/String;

.field private ad:I

.field private ae:Z

.field private af:I

.field private ag:Z

.field private ah:Ljava/util/ArrayList;

.field private ai:Landroid/app/ProgressDialog;

.field private aj:Ljava/lang/String;

.field private ak:Ljava/lang/String;

.field private al:Z

.field private am:Lcom/osp/app/signin/SignUpinfo;

.field private an:Lcom/osp/app/util/n;

.field private ao:Ljava/lang/String;

.field private ap:Ljava/lang/String;

.field private aq:Ljava/lang/String;

.field private ar:Ljava/lang/String;

.field private as:Ljava/lang/String;

.field private at:I

.field private au:I

.field private av:I

.field private aw:Ljava/lang/String;

.field private ax:Ljava/lang/String;

.field private ay:Ljava/lang/String;

.field private az:Landroid/widget/TextView;

.field private b:Ljava/lang/String;

.field private ba:Lcom/osp/app/signin/jz;

.field private bb:Ljava/lang/String;

.field private bc:Lcom/msc/a/d;

.field private bd:Ljava/lang/String;

.field private be:Lcom/osp/security/identity/f;

.field private bf:Landroid/app/AlertDialog;

.field private c:Ljava/lang/String;

.field private d:Ljava/lang/String;

.field private e:Ljava/lang/String;

.field private f:Ljava/lang/String;

.field private y:Ljava/lang/String;

.field private z:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 3

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    const/4 v0, 0x0

    .line 108
    invoke-direct {p0}, Lcom/osp/app/util/BaseActivity;-><init>()V

    .line 145
    iput-object v0, p0, Lcom/osp/app/signin/TnCView;->a:Landroid/content/Intent;

    .line 150
    iput-object v0, p0, Lcom/osp/app/signin/TnCView;->b:Ljava/lang/String;

    .line 155
    iput-object v0, p0, Lcom/osp/app/signin/TnCView;->c:Ljava/lang/String;

    .line 160
    iput-object v0, p0, Lcom/osp/app/signin/TnCView;->d:Ljava/lang/String;

    .line 165
    iput-object v0, p0, Lcom/osp/app/signin/TnCView;->e:Ljava/lang/String;

    .line 170
    iput-object v0, p0, Lcom/osp/app/signin/TnCView;->f:Ljava/lang/String;

    .line 175
    iput-object v0, p0, Lcom/osp/app/signin/TnCView;->y:Ljava/lang/String;

    .line 180
    iput-object v0, p0, Lcom/osp/app/signin/TnCView;->z:Ljava/lang/String;

    .line 190
    iput-object v0, p0, Lcom/osp/app/signin/TnCView;->B:Ljava/lang/String;

    .line 196
    iput-object v0, p0, Lcom/osp/app/signin/TnCView;->C:Ljava/lang/String;

    .line 251
    iput-boolean v1, p0, Lcom/osp/app/signin/TnCView;->N:Z

    .line 256
    iput-boolean v1, p0, Lcom/osp/app/signin/TnCView;->O:Z

    .line 288
    iput-object v0, p0, Lcom/osp/app/signin/TnCView;->T:Lcom/osp/app/signin/kd;

    .line 303
    iput-boolean v1, p0, Lcom/osp/app/signin/TnCView;->U:Z

    .line 309
    iput-boolean v1, p0, Lcom/osp/app/signin/TnCView;->V:Z

    .line 314
    iput-boolean v1, p0, Lcom/osp/app/signin/TnCView;->W:Z

    .line 319
    iput-boolean v1, p0, Lcom/osp/app/signin/TnCView;->X:Z

    .line 330
    iput-object v0, p0, Lcom/osp/app/signin/TnCView;->Y:Ljava/lang/String;

    .line 335
    iput-object v0, p0, Lcom/osp/app/signin/TnCView;->Z:Ljava/lang/String;

    .line 340
    iput-object v0, p0, Lcom/osp/app/signin/TnCView;->aa:Ljava/lang/String;

    .line 345
    iput-object v0, p0, Lcom/osp/app/signin/TnCView;->ab:Ljava/lang/String;

    .line 350
    iput-object v0, p0, Lcom/osp/app/signin/TnCView;->ac:[Ljava/lang/String;

    .line 361
    iput-boolean v1, p0, Lcom/osp/app/signin/TnCView;->ae:Z

    .line 380
    iput-object v0, p0, Lcom/osp/app/signin/TnCView;->ah:Ljava/util/ArrayList;

    .line 385
    iput-object v0, p0, Lcom/osp/app/signin/TnCView;->ai:Landroid/app/ProgressDialog;

    .line 391
    iput-object v0, p0, Lcom/osp/app/signin/TnCView;->aj:Ljava/lang/String;

    .line 396
    iput-object v0, p0, Lcom/osp/app/signin/TnCView;->ak:Ljava/lang/String;

    .line 422
    iput-boolean v1, p0, Lcom/osp/app/signin/TnCView;->al:Z

    .line 462
    iput v2, p0, Lcom/osp/app/signin/TnCView;->at:I

    .line 467
    iput v2, p0, Lcom/osp/app/signin/TnCView;->au:I

    .line 472
    iput v2, p0, Lcom/osp/app/signin/TnCView;->av:I

    .line 583
    iput-boolean v1, p0, Lcom/osp/app/signin/TnCView;->aM:Z

    .line 588
    iput-object v0, p0, Lcom/osp/app/signin/TnCView;->aN:Landroid/widget/CheckBox;

    .line 592
    iput-object v0, p0, Lcom/osp/app/signin/TnCView;->aO:Landroid/widget/CheckBox;

    .line 596
    iput-object v0, p0, Lcom/osp/app/signin/TnCView;->aP:Landroid/widget/CheckBox;

    .line 600
    iput-object v0, p0, Lcom/osp/app/signin/TnCView;->aQ:Landroid/widget/CheckBox;

    .line 604
    iput-object v0, p0, Lcom/osp/app/signin/TnCView;->aR:Landroid/widget/CheckBox;

    .line 608
    iput-object v0, p0, Lcom/osp/app/signin/TnCView;->aS:Landroid/widget/CheckBox;

    .line 613
    iput-boolean v1, p0, Lcom/osp/app/signin/TnCView;->aT:Z

    .line 618
    iput-boolean v1, p0, Lcom/osp/app/signin/TnCView;->aU:Z

    .line 5744
    return-void
.end method

.method static synthetic A(Lcom/osp/app/signin/TnCView;)[Ljava/lang/String;
    .locals 1

    .prologue
    .line 108
    iget-object v0, p0, Lcom/osp/app/signin/TnCView;->ac:[Ljava/lang/String;

    return-object v0
.end method

.method static synthetic B(Lcom/osp/app/signin/TnCView;)Landroid/widget/ListView;
    .locals 1

    .prologue
    .line 108
    iget-object v0, p0, Lcom/osp/app/signin/TnCView;->K:Landroid/widget/ListView;

    return-object v0
.end method

.method static synthetic C(Lcom/osp/app/signin/TnCView;)Z
    .locals 1

    .prologue
    .line 108
    iget-boolean v0, p0, Lcom/osp/app/signin/TnCView;->aL:Z

    return v0
.end method

.method static synthetic D(Lcom/osp/app/signin/TnCView;)Z
    .locals 1

    .prologue
    .line 108
    iget-boolean v0, p0, Lcom/osp/app/signin/TnCView;->V:Z

    return v0
.end method

.method static synthetic E(Lcom/osp/app/signin/TnCView;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 108
    iget-object v0, p0, Lcom/osp/app/signin/TnCView;->z:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic F(Lcom/osp/app/signin/TnCView;)Z
    .locals 1

    .prologue
    .line 108
    iget-boolean v0, p0, Lcom/osp/app/signin/TnCView;->aX:Z

    return v0
.end method

.method static synthetic G(Lcom/osp/app/signin/TnCView;)I
    .locals 1

    .prologue
    .line 108
    iget v0, p0, Lcom/osp/app/signin/TnCView;->af:I

    return v0
.end method

.method static synthetic H(Lcom/osp/app/signin/TnCView;)I
    .locals 1

    .prologue
    .line 108
    iget v0, p0, Lcom/osp/app/signin/TnCView;->af:I

    xor-int/lit8 v0, v0, 0x2

    iput v0, p0, Lcom/osp/app/signin/TnCView;->af:I

    return v0
.end method

.method static synthetic I(Lcom/osp/app/signin/TnCView;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 108
    iget-object v0, p0, Lcom/osp/app/signin/TnCView;->c:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic J(Lcom/osp/app/signin/TnCView;)Z
    .locals 1

    .prologue
    .line 108
    iget-boolean v0, p0, Lcom/osp/app/signin/TnCView;->aY:Z

    return v0
.end method

.method static synthetic K(Lcom/osp/app/signin/TnCView;)Z
    .locals 1

    .prologue
    .line 108
    iget-boolean v0, p0, Lcom/osp/app/signin/TnCView;->aM:Z

    return v0
.end method

.method static synthetic L(Lcom/osp/app/signin/TnCView;)Z
    .locals 1

    .prologue
    .line 108
    iget-boolean v0, p0, Lcom/osp/app/signin/TnCView;->ae:Z

    return v0
.end method

.method static synthetic M(Lcom/osp/app/signin/TnCView;)V
    .locals 10

    .prologue
    const/4 v3, 0x0

    .line 108
    new-instance v0, Landroid/app/AlertDialog$Builder;

    invoke-direct {v0, p0}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    const v1, 0x7f09004f

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    const v1, 0x7f090169

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setMessage(I)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    const v1, 0x7f090042

    new-instance v2, Lcom/osp/app/signin/oa;

    invoke-direct {v2, p0}, Lcom/osp/app/signin/oa;-><init>(Lcom/osp/app/signin/TnCView;)V

    invoke-virtual {v0, v1, v2}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/AlertDialog;->show()V

    const v1, 0x102000b

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    if-eqz v0, :cond_1

    new-instance v4, Landroid/text/SpannableString;

    invoke-virtual {v0}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v1

    invoke-direct {v4, v1}, Landroid/text/SpannableString;-><init>(Ljava/lang/CharSequence;)V

    invoke-interface {v4}, Landroid/text/Spannable;->length()I

    move-result v1

    const-class v2, Landroid/text/style/URLSpan;

    invoke-interface {v4, v3, v1, v2}, Landroid/text/Spannable;->getSpans(IILjava/lang/Class;)[Ljava/lang/Object;

    move-result-object v1

    check-cast v1, [Landroid/text/style/URLSpan;

    array-length v5, v1

    move v2, v3

    :goto_0
    if-ge v2, v5, :cond_0

    aget-object v6, v1, v2

    invoke-interface {v4, v6}, Landroid/text/Spannable;->getSpanStart(Ljava/lang/Object;)I

    move-result v7

    invoke-interface {v4, v6}, Landroid/text/Spannable;->getSpanEnd(Ljava/lang/Object;)I

    move-result v8

    invoke-interface {v4, v6}, Landroid/text/Spannable;->removeSpan(Ljava/lang/Object;)V

    invoke-static {p0}, Lcom/msc/c/n;->c(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v6

    new-instance v9, Lcom/osp/app/signin/TnCView$8;

    invoke-direct {v9, p0, v6}, Lcom/osp/app/signin/TnCView$8;-><init>(Lcom/osp/app/signin/TnCView;Ljava/lang/String;)V

    invoke-interface {v4, v9, v7, v8, v3}, Landroid/text/Spannable;->setSpan(Ljava/lang/Object;III)V

    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    :cond_0
    invoke-virtual {v0, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    invoke-static {}, Landroid/text/method/LinkMovementMethod;->getInstance()Landroid/text/method/MovementMethod;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setMovementMethod(Landroid/text/method/MovementMethod;)V

    :cond_1
    return-void
.end method

.method static synthetic N(Lcom/osp/app/signin/TnCView;)V
    .locals 4

    .prologue
    .line 108
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    const-class v1, Lcom/osp/app/signin/SelectCountryView;

    invoke-virtual {v0, p0, v1}, Landroid/content/Intent;->setClass(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;

    const-string v1, "MODE"

    iget-object v2, p0, Lcom/osp/app/signin/TnCView;->B:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    const-string v1, "BG_WhoareU"

    iget-object v2, p0, Lcom/osp/app/signin/TnCView;->C:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    const-string v1, "com.osp.app.signin.action.SAMSUNG_ACCOUNT_SETUPWIZARD"

    invoke-static {}, Lcom/osp/app/signin/SamsungService;->a()Z

    move-result v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    const-string v1, "BG_mode"

    iget-object v2, p0, Lcom/osp/app/signin/TnCView;->C:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/osp/app/signin/TnCView;->z:Ljava/lang/String;

    if-eqz v1, :cond_0

    const-string v1, "mypackage"

    iget-object v2, p0, Lcom/osp/app/signin/TnCView;->z:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    const-string v1, "key_request_id"

    iget-wide v2, p0, Lcom/osp/app/signin/TnCView;->aW:J

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;J)Landroid/content/Intent;

    :cond_0
    const/16 v1, 0xd0

    invoke-virtual {p0, v0, v1}, Lcom/osp/app/signin/TnCView;->startActivityForResult(Landroid/content/Intent;I)V

    return-void
.end method

.method static synthetic O(Lcom/osp/app/signin/TnCView;)V
    .locals 0

    .prologue
    .line 108
    invoke-direct {p0}, Lcom/osp/app/signin/TnCView;->e()V

    return-void
.end method

.method static synthetic P(Lcom/osp/app/signin/TnCView;)Landroid/app/ProgressDialog;
    .locals 1

    .prologue
    .line 108
    iget-object v0, p0, Lcom/osp/app/signin/TnCView;->ai:Landroid/app/ProgressDialog;

    return-object v0
.end method

.method static synthetic Q(Lcom/osp/app/signin/TnCView;)V
    .locals 0

    .prologue
    .line 108
    invoke-virtual {p0}, Lcom/osp/app/signin/TnCView;->m()V

    return-void
.end method

.method static synthetic R(Lcom/osp/app/signin/TnCView;)V
    .locals 0

    .prologue
    .line 108
    invoke-virtual {p0}, Lcom/osp/app/signin/TnCView;->m()V

    return-void
.end method

.method static synthetic S(Lcom/osp/app/signin/TnCView;)Lcom/osp/app/signin/SignUpinfo;
    .locals 1

    .prologue
    .line 108
    iget-object v0, p0, Lcom/osp/app/signin/TnCView;->am:Lcom/osp/app/signin/SignUpinfo;

    return-object v0
.end method

.method static synthetic T(Lcom/osp/app/signin/TnCView;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 108
    iget-object v0, p0, Lcom/osp/app/signin/TnCView;->e:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic U(Lcom/osp/app/signin/TnCView;)Z
    .locals 1

    .prologue
    .line 108
    iget-boolean v0, p0, Lcom/osp/app/signin/TnCView;->I:Z

    return v0
.end method

.method static synthetic V(Lcom/osp/app/signin/TnCView;)Z
    .locals 1

    .prologue
    .line 108
    iget-boolean v0, p0, Lcom/osp/app/signin/TnCView;->aF:Z

    return v0
.end method

.method static synthetic W(Lcom/osp/app/signin/TnCView;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 108
    iget-object v0, p0, Lcom/osp/app/signin/TnCView;->bd:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic X(Lcom/osp/app/signin/TnCView;)Lcom/osp/security/identity/f;
    .locals 1

    .prologue
    .line 108
    iget-object v0, p0, Lcom/osp/app/signin/TnCView;->be:Lcom/osp/security/identity/f;

    return-object v0
.end method

.method static synthetic Y(Lcom/osp/app/signin/TnCView;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 108
    iget-object v0, p0, Lcom/osp/app/signin/TnCView;->ap:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic Z(Lcom/osp/app/signin/TnCView;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 108
    iget-object v0, p0, Lcom/osp/app/signin/TnCView;->aq:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic a(Lcom/osp/app/signin/TnCView;Landroid/app/ProgressDialog;)Landroid/app/ProgressDialog;
    .locals 0

    .prologue
    .line 108
    iput-object p1, p0, Lcom/osp/app/signin/TnCView;->ai:Landroid/app/ProgressDialog;

    return-object p1
.end method

.method static synthetic a(Lcom/osp/app/signin/TnCView;Landroid/widget/CheckBox;)Landroid/widget/CheckBox;
    .locals 0

    .prologue
    .line 108
    iput-object p1, p0, Lcom/osp/app/signin/TnCView;->aN:Landroid/widget/CheckBox;

    return-object p1
.end method

.method static synthetic a(Lcom/osp/app/signin/TnCView;Lcom/msc/a/d;)Lcom/msc/a/d;
    .locals 0

    .prologue
    .line 108
    iput-object p1, p0, Lcom/osp/app/signin/TnCView;->bc:Lcom/msc/a/d;

    return-object p1
.end method

.method static synthetic a(Lcom/osp/app/signin/TnCView;Lcom/osp/app/signin/oj;)Lcom/osp/app/signin/oj;
    .locals 0

    .prologue
    .line 108
    iput-object p1, p0, Lcom/osp/app/signin/TnCView;->M:Lcom/osp/app/signin/oj;

    return-object p1
.end method

.method static synthetic a(Lcom/osp/app/signin/TnCView;Lcom/osp/app/signin/op;)Lcom/osp/app/signin/op;
    .locals 0

    .prologue
    .line 108
    iput-object p1, p0, Lcom/osp/app/signin/TnCView;->L:Lcom/osp/app/signin/op;

    return-object p1
.end method

.method static synthetic a(Lcom/osp/app/signin/TnCView;Ljava/util/ArrayList;)Ljava/util/ArrayList;
    .locals 0

    .prologue
    .line 108
    iput-object p1, p0, Lcom/osp/app/signin/TnCView;->ah:Ljava/util/ArrayList;

    return-object p1
.end method

.method private a(I)V
    .locals 7

    .prologue
    const v3, 0x7f08011e

    const/4 v4, 0x0

    .line 1174
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "AccountView::initLayoutParams orientation : "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;)V

    .line 1176
    invoke-static {}, Lcom/osp/device/b;->b()Lcom/osp/device/b;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/osp/device/b;->i(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_5

    .line 1178
    invoke-virtual {p0}, Lcom/osp/app/signin/TnCView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f08011f

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v0

    float-to-int v0, v0

    .line 1179
    invoke-virtual {p0}, Lcom/osp/app/signin/TnCView;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1, v3}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v1

    float-to-int v2, v1

    .line 1180
    invoke-static {}, Lcom/osp/app/signin/SamsungService;->a()Z

    move-result v1

    if-nez v1, :cond_0

    .line 1182
    invoke-virtual {p0}, Lcom/osp/app/signin/TnCView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0, v3}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v0

    float-to-int v0, v0

    .line 1185
    :cond_0
    const v1, 0x7f0c0184

    invoke-virtual {p0, v1}, Lcom/osp/app/signin/TnCView;->findViewById(I)Landroid/view/View;

    move-result-object v5

    .line 1186
    invoke-static {}, Lcom/osp/app/util/q;->a()Lcom/osp/app/util/q;

    if-eqz v5, :cond_2

    const/4 v1, 0x1

    if-ne p1, v1, :cond_6

    move v1, v0

    move v3, v2

    :goto_0
    invoke-virtual {v5}, Landroid/view/View;->getPaddingRight()I

    move-result v6

    if-eq v6, v1, :cond_1

    invoke-virtual {v5}, Landroid/view/View;->getPaddingRight()I

    move-result v1

    if-nez v1, :cond_2

    :cond_1
    invoke-static {v5, v3, v4, v3, v4}, Lcom/msc/sa/c/d;->a(Landroid/view/View;IIII)V

    .line 1188
    :cond_2
    iget-object v1, p0, Lcom/osp/app/signin/TnCView;->K:Landroid/widget/ListView;

    invoke-virtual {v1}, Landroid/widget/ListView;->invalidate()V

    .line 1190
    const v1, 0x7f0c0181

    invoke-virtual {p0, v1}, Lcom/osp/app/signin/TnCView;->findViewById(I)Landroid/view/View;

    move-result-object v1

    .line 1191
    invoke-static {}, Lcom/osp/app/util/q;->a()Lcom/osp/app/util/q;

    invoke-static {v1, v0, v2, p1}, Lcom/osp/app/util/q;->b(Landroid/view/View;III)V

    .line 1194
    invoke-static {p0}, Lcom/osp/app/util/r;->k(Landroid/content/Context;)Z

    move-result v1

    if-nez v1, :cond_3

    .line 1196
    invoke-virtual {p0}, Lcom/osp/app/signin/TnCView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f08013d

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v0

    float-to-int v0, v0

    .line 1197
    invoke-virtual {p0}, Lcom/osp/app/signin/TnCView;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f08013c

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v1

    float-to-int v2, v1

    .line 1200
    :cond_3
    iget-object v1, p0, Lcom/osp/app/signin/TnCView;->aK:Lcom/osp/app/signin/BottomSoftkeyLinearLayout;

    if-eqz v1, :cond_5

    .line 1202
    invoke-static {}, Lcom/osp/app/signin/SamsungService;->a()Z

    move-result v1

    if-eqz v1, :cond_4

    sget-object v1, Lcom/osp/app/util/u;->o:[Ljava/lang/String;

    invoke-static {v1}, Lcom/osp/app/util/u;->a([Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_4

    move v0, v4

    .line 1206
    :cond_4
    invoke-static {}, Lcom/osp/app/util/q;->a()Lcom/osp/app/util/q;

    iget-object v1, p0, Lcom/osp/app/signin/TnCView;->aK:Lcom/osp/app/signin/BottomSoftkeyLinearLayout;

    invoke-static {p0, v1, v0, v2, p1}, Lcom/osp/app/util/q;->a(Landroid/content/Context;Landroid/view/View;III)V

    .line 1225
    :cond_5
    return-void

    .line 1186
    :cond_6
    const/4 v1, 0x2

    if-ne p1, v1, :cond_2

    move v1, v2

    move v3, v0

    goto :goto_0
.end method

.method static synthetic a(Lcom/osp/app/signin/TnCView;Landroid/app/AlertDialog;)V
    .locals 1

    .prologue
    .line 108
    if-eqz p1, :cond_0

    :try_start_0
    invoke-virtual {p0}, Lcom/osp/app/signin/TnCView;->isFinishing()Z

    move-result v0

    if-nez v0, :cond_0

    invoke-virtual {p1}, Landroid/app/AlertDialog;->isShowing()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p1}, Landroid/app/AlertDialog;->dismiss()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :cond_0
    :goto_0
    return-void

    :catch_0
    move-exception v0

    :try_start_1
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v0

    throw v0
.end method

.method static synthetic a(Lcom/osp/app/signin/TnCView;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 108
    invoke-direct {p0, p1}, Lcom/osp/app/signin/TnCView;->b(Ljava/lang/String;)V

    return-void
.end method

.method static synthetic a(Lcom/osp/app/signin/TnCView;Z)V
    .locals 0

    .prologue
    .line 108
    invoke-direct {p0, p1}, Lcom/osp/app/signin/TnCView;->a(Z)V

    return-void
.end method

.method private a(Z)V
    .locals 4

    .prologue
    const/4 v3, -0x1

    .line 1720
    invoke-static {p0}, Lcom/osp/app/util/ad;->a(Landroid/content/Context;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 1722
    const/16 v0, 0xde

    invoke-virtual {p0, v0}, Lcom/osp/app/signin/TnCView;->c(I)V

    .line 1779
    :cond_0
    :goto_0
    return-void

    .line 1726
    :cond_1
    if-eqz p1, :cond_2

    .line 1728
    new-instance v0, Lcom/osp/app/signin/ob;

    invoke-direct {v0, p0}, Lcom/osp/app/signin/ob;-><init>(Lcom/osp/app/signin/TnCView;)V

    iput-object v0, p0, Lcom/osp/app/signin/TnCView;->P:Lcom/osp/app/signin/ob;

    .line 1729
    iget-object v0, p0, Lcom/osp/app/signin/TnCView;->P:Lcom/osp/app/signin/ob;

    invoke-virtual {v0}, Lcom/osp/app/signin/ob;->b()V

    goto :goto_0

    .line 1732
    :cond_2
    const-string v0, "450"

    iget-object v1, p0, Lcom/osp/app/signin/TnCView;->Y:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_3

    invoke-static {p0}, Lcom/osp/app/util/r;->s(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_6

    .line 1734
    :cond_3
    iget-boolean v0, p0, Lcom/osp/app/signin/TnCView;->D:Z

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/osp/app/signin/TnCView;->E:Z

    if-eqz v0, :cond_0

    .line 1736
    iget-boolean v0, p0, Lcom/osp/app/signin/TnCView;->V:Z

    if-eqz v0, :cond_5

    .line 1738
    iget-boolean v0, p0, Lcom/osp/app/signin/TnCView;->aT:Z

    if-eqz v0, :cond_4

    .line 1740
    invoke-virtual {p0}, Lcom/osp/app/signin/TnCView;->getIntent()Landroid/content/Intent;

    move-result-object v0

    .line 1741
    const-string v1, "key_easy_signup_mo_auth_mode"

    iget-boolean v2, p0, Lcom/osp/app/signin/TnCView;->H:Z

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 1742
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "mEasySignupMoAuthMode - mIsEsuTncAcceptedChecked : "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-boolean v2, p0, Lcom/osp/app/signin/TnCView;->H:Z

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;)V

    .line 1743
    invoke-virtual {p0, v3, v0}, Lcom/osp/app/signin/TnCView;->a(ILandroid/content/Intent;)V

    .line 1748
    :goto_1
    invoke-virtual {p0}, Lcom/osp/app/signin/TnCView;->finish()V

    goto :goto_0

    .line 1746
    :cond_4
    invoke-virtual {p0, v3}, Lcom/osp/app/signin/TnCView;->b(I)V

    goto :goto_1

    .line 1751
    :cond_5
    invoke-direct {p0}, Lcom/osp/app/signin/TnCView;->f()V

    goto :goto_0

    .line 1756
    :cond_6
    iget-boolean v0, p0, Lcom/osp/app/signin/TnCView;->D:Z

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/osp/app/signin/TnCView;->E:Z

    if-eqz v0, :cond_0

    .line 1758
    iget-boolean v0, p0, Lcom/osp/app/signin/TnCView;->V:Z

    if-eqz v0, :cond_8

    .line 1760
    iget-boolean v0, p0, Lcom/osp/app/signin/TnCView;->aT:Z

    if-eqz v0, :cond_7

    .line 1762
    invoke-virtual {p0}, Lcom/osp/app/signin/TnCView;->getIntent()Landroid/content/Intent;

    move-result-object v0

    .line 1763
    const-string v1, "key_easy_signup_mo_auth_mode"

    iget-boolean v2, p0, Lcom/osp/app/signin/TnCView;->H:Z

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 1764
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "mEasySignupMoAuthMode - mIsEsuTncAcceptedChecked : "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-boolean v2, p0, Lcom/osp/app/signin/TnCView;->H:Z

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;)V

    .line 1765
    invoke-virtual {p0, v3, v0}, Lcom/osp/app/signin/TnCView;->a(ILandroid/content/Intent;)V

    .line 1770
    :goto_2
    invoke-virtual {p0}, Lcom/osp/app/signin/TnCView;->finish()V

    goto/16 :goto_0

    .line 1768
    :cond_7
    invoke-virtual {p0, v3}, Lcom/osp/app/signin/TnCView;->b(I)V

    goto :goto_2

    .line 1773
    :cond_8
    invoke-direct {p0}, Lcom/osp/app/signin/TnCView;->f()V

    goto/16 :goto_0
.end method

.method static synthetic a(Lcom/osp/app/signin/TnCView;)Z
    .locals 1

    .prologue
    .line 108
    iget-boolean v0, p0, Lcom/osp/app/signin/TnCView;->H:Z

    return v0
.end method

.method static synthetic a(Lcom/osp/app/signin/TnCView;[Ljava/lang/String;)[Ljava/lang/String;
    .locals 0

    .prologue
    .line 108
    iput-object p1, p0, Lcom/osp/app/signin/TnCView;->ac:[Ljava/lang/String;

    return-object p1
.end method

.method static synthetic aa(Lcom/osp/app/signin/TnCView;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 108
    iget-object v0, p0, Lcom/osp/app/signin/TnCView;->ar:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic ab(Lcom/osp/app/signin/TnCView;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 108
    iget-object v0, p0, Lcom/osp/app/signin/TnCView;->ay:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic ac(Lcom/osp/app/signin/TnCView;)Lcom/osp/app/signin/jz;
    .locals 1

    .prologue
    .line 108
    iget-object v0, p0, Lcom/osp/app/signin/TnCView;->ba:Lcom/osp/app/signin/jz;

    return-object v0
.end method

.method static synthetic ad(Lcom/osp/app/signin/TnCView;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 108
    iget-object v0, p0, Lcom/osp/app/signin/TnCView;->bb:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic ae(Lcom/osp/app/signin/TnCView;)Lcom/msc/a/d;
    .locals 1

    .prologue
    .line 108
    iget-object v0, p0, Lcom/osp/app/signin/TnCView;->bc:Lcom/msc/a/d;

    return-object v0
.end method

.method static synthetic af(Lcom/osp/app/signin/TnCView;)V
    .locals 3

    .prologue
    .line 108
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v0, "TNC"

    const-string v1, "sendSignInCompleteBroadcast"

    invoke-static {v0, v1}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;)V

    new-instance v0, Landroid/content/Intent;

    const-string v1, "android.intent.action.SAMSUNGACCOUNT_SIGNIN_COMPLETED"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    invoke-static {}, Lcom/osp/device/b;->b()Lcom/osp/device/b;

    move-result-object v1

    invoke-virtual {v1}, Lcom/osp/device/b;->l()I

    move-result v1

    const/16 v2, 0xb

    if-le v1, v2, :cond_0

    const/16 v1, 0x20

    invoke-virtual {v0, v1}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    :cond_0
    const-string v1, "signUpInfo"

    const-string v2, "SA_EMAIL_;SA_COUNTRY_;SA_BIRTHDATE_;SA_EMAILRECEIVE_;"

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    invoke-virtual {p0, v0}, Lcom/osp/app/signin/TnCView;->sendBroadcast(Landroid/content/Intent;)V

    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v0, "TNC"

    const-string v1, "send signInCompleteIntentV02"

    invoke-static {v0, v1}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;)V

    new-instance v0, Landroid/content/Intent;

    const-string v1, "android.intent.action.REGISTRATION_COMPLETED"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    const-string v1, "login_id"

    invoke-static {p0}, Lcom/osp/app/util/ad;->h(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    invoke-virtual {p0, v0}, Lcom/osp/app/signin/TnCView;->sendBroadcast(Landroid/content/Intent;)V

    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v0, "TNC"

    const-string v1, "send signInCompleteIntentV01"

    invoke-static {v0, v1}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;)V

    invoke-static {}, Lcom/osp/app/signin/SamsungService;->a()Z

    move-result v0

    if-nez v0, :cond_1

    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    const-string v1, "com.msc.action.MARKETING_POPUP_NOTIFICATION"

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    const-string v1, "com.osp.app.signin"

    const-string v2, "com.osp.app.signin.BackgroundModeService"

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->setClassName(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    invoke-virtual {p0, v0}, Lcom/osp/app/signin/TnCView;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;

    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v0, "TNC"

    const-string v1, "startService MarketingPopupNotiIntent"

    invoke-static {v0, v1}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;)V

    :cond_1
    return-void
.end method

.method static synthetic ag(Lcom/osp/app/signin/TnCView;)Lcom/osp/app/util/n;
    .locals 1

    .prologue
    .line 108
    iget-object v0, p0, Lcom/osp/app/signin/TnCView;->an:Lcom/osp/app/util/n;

    return-object v0
.end method

.method static synthetic ah(Lcom/osp/app/signin/TnCView;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 108
    iget-object v0, p0, Lcom/osp/app/signin/TnCView;->d:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic ai(Lcom/osp/app/signin/TnCView;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 108
    iget-object v0, p0, Lcom/osp/app/signin/TnCView;->f:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic aj(Lcom/osp/app/signin/TnCView;)Z
    .locals 1

    .prologue
    .line 108
    iget-boolean v0, p0, Lcom/osp/app/signin/TnCView;->ag:Z

    return v0
.end method

.method static synthetic ak(Lcom/osp/app/signin/TnCView;)Landroid/app/AlertDialog;
    .locals 1

    .prologue
    .line 108
    iget-object v0, p0, Lcom/osp/app/signin/TnCView;->bf:Landroid/app/AlertDialog;

    return-object v0
.end method

.method static synthetic b(Lcom/osp/app/signin/TnCView;Landroid/widget/CheckBox;)Landroid/widget/CheckBox;
    .locals 0

    .prologue
    .line 108
    iput-object p1, p0, Lcom/osp/app/signin/TnCView;->aO:Landroid/widget/CheckBox;

    return-object p1
.end method

.method static synthetic b(Lcom/osp/app/signin/TnCView;Ljava/lang/String;)Ljava/lang/String;
    .locals 0

    .prologue
    .line 108
    iput-object p1, p0, Lcom/osp/app/signin/TnCView;->Y:Ljava/lang/String;

    return-object p1
.end method

.method private b(Ljava/lang/String;)V
    .locals 4

    .prologue
    .line 1113
    new-instance v0, Landroid/content/Intent;

    const-string v1, "com.msc.action.samsungaccount.resignin"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 1114
    const-string v1, "client_id"

    iget-object v2, p0, Lcom/osp/app/signin/TnCView;->b:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 1115
    const-string v1, "client_secret"

    iget-object v2, p0, Lcom/osp/app/signin/TnCView;->c:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 1116
    const-string v1, "account_mode"

    iget-object v2, p0, Lcom/osp/app/signin/TnCView;->d:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 1117
    const-string v1, "OSP_VER"

    iget-object v2, p0, Lcom/osp/app/signin/TnCView;->e:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 1118
    const-string v1, "MODE"

    iget-object v2, p0, Lcom/osp/app/signin/TnCView;->f:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 1119
    const-string v1, "key_request_id"

    iget-wide v2, p0, Lcom/osp/app/signin/TnCView;->aW:J

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;J)Landroid/content/Intent;

    .line 1121
    if-eqz p1, :cond_0

    .line 1123
    const-string v1, "email_id"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 1132
    :cond_0
    invoke-virtual {p0, v0}, Lcom/osp/app/signin/TnCView;->startActivity(Landroid/content/Intent;)V

    .line 1134
    return-void
.end method

.method private b(Z)V
    .locals 7

    .prologue
    .line 7621
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v0, "=========================================== SignInDual_End PROCESSING_SUCCESS==========================================="

    invoke-static {v0}, Lcom/osp/app/util/aq;->c(Ljava/lang/String;)V

    .line 7628
    const-string v0, "OSP_02"

    iget-object v1, p0, Lcom/osp/app/signin/TnCView;->e:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 7630
    const-string v0, "ADD_ACCOUNT"

    iget-object v1, p0, Lcom/osp/app/signin/TnCView;->f:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 7632
    iget-object v0, p0, Lcom/osp/app/signin/TnCView;->a:Landroid/content/Intent;

    const-string v1, "authcode"

    iget-object v2, p0, Lcom/osp/app/signin/TnCView;->ak:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 7636
    :cond_0
    iget-boolean v0, p0, Lcom/osp/app/signin/TnCView;->U:Z

    if-eqz v0, :cond_1

    .line 7638
    iget-object v0, p0, Lcom/osp/app/signin/TnCView;->a:Landroid/content/Intent;

    const-string v1, "signUpInfo"

    iget-object v2, p0, Lcom/osp/app/signin/TnCView;->aj:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 7639
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "SignUpProcessEnd  PROCESS_STATE_AUTHORIZATION_COMPLETED\tsignUpInfo\t\t:\t"

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v1, p0, Lcom/osp/app/signin/TnCView;->aj:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/osp/app/util/aq;->b(Ljava/lang/String;)V

    .line 7643
    :cond_1
    iget v0, p0, Lcom/osp/app/signin/TnCView;->af:I

    if-nez v0, :cond_2

    if-eqz p1, :cond_2

    iget-boolean v0, p0, Lcom/osp/app/signin/TnCView;->I:Z

    if-eqz v0, :cond_2

    invoke-static {p0}, Lcom/osp/app/util/r;->t(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 7645
    invoke-static {}, Lcom/osp/app/signin/SamsungService;->a()Z

    move-result v0

    if-nez v0, :cond_2

    .line 7647
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v0, "TNC"

    const-string v1, "Show MybenefitNoti"

    invoke-static {v0, v1}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 7651
    new-instance v0, Landroid/content/Intent;

    const-string v1, "com.msc.action.samsungaccount.mybenefitwebview_internal"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 7663
    const/4 v1, 0x0

    const/high16 v2, 0x8000000

    invoke-static {p0, v1, v0, v2}, Landroid/app/PendingIntent;->getActivity(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v1

    .line 7665
    const/4 v2, 0x0

    const v0, 0x7f09004f

    invoke-virtual {p0, v0}, Lcom/osp/app/signin/TnCView;->getString(I)Ljava/lang/String;

    move-result-object v3

    const v0, 0x7f090152

    invoke-virtual {p0, v0}, Lcom/osp/app/signin/TnCView;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-static {}, Lcom/osp/app/signin/SamsungService;->i()I

    move-result v5

    const v6, 0x132df82

    move-object v0, p0

    invoke-static/range {v0 .. v6}, Lcom/msc/sa/c/d;->a(Landroid/content/Context;Landroid/app/PendingIntent;Landroid/app/PendingIntent;Ljava/lang/String;Ljava/lang/String;II)V

    .line 7669
    :cond_2
    return-void
.end method

.method static synthetic b(Lcom/osp/app/signin/TnCView;)Z
    .locals 1

    .prologue
    .line 108
    iget-boolean v0, p0, Lcom/osp/app/signin/TnCView;->aU:Z

    return v0
.end method

.method static synthetic b(Lcom/osp/app/signin/TnCView;Z)Z
    .locals 0

    .prologue
    .line 108
    iput-boolean p1, p0, Lcom/osp/app/signin/TnCView;->D:Z

    return p1
.end method

.method static synthetic c(Lcom/osp/app/signin/TnCView;)Landroid/content/Intent;
    .locals 1

    .prologue
    .line 108
    iget-object v0, p0, Lcom/osp/app/signin/TnCView;->a:Landroid/content/Intent;

    return-object v0
.end method

.method static synthetic c(Lcom/osp/app/signin/TnCView;Landroid/widget/CheckBox;)Landroid/widget/CheckBox;
    .locals 0

    .prologue
    .line 108
    iput-object p1, p0, Lcom/osp/app/signin/TnCView;->aP:Landroid/widget/CheckBox;

    return-object p1
.end method

.method static synthetic c(Lcom/osp/app/signin/TnCView;Ljava/lang/String;)Ljava/lang/String;
    .locals 0

    .prologue
    .line 108
    iput-object p1, p0, Lcom/osp/app/signin/TnCView;->bd:Ljava/lang/String;

    return-object p1
.end method

.method private c(Ljava/lang/String;)V
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 7474
    invoke-virtual {p0}, Lcom/osp/app/signin/TnCView;->isFinishing()Z

    move-result v0

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/osp/app/signin/TnCView;->ai:Landroid/app/ProgressDialog;

    if-eqz v0, :cond_1

    :try_start_0
    iget-object v0, p0, Lcom/osp/app/signin/TnCView;->ai:Landroid/app/ProgressDialog;

    invoke-virtual {v0}, Landroid/app/ProgressDialog;->isShowing()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/osp/app/signin/TnCView;->ai:Landroid/app/ProgressDialog;

    invoke-virtual {v0}, Landroid/app/ProgressDialog;->dismiss()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :cond_0
    :goto_0
    iput-object v1, p0, Lcom/osp/app/signin/TnCView;->ai:Landroid/app/ProgressDialog;

    .line 7476
    :cond_1
    const-string v0, "450"

    iget-object v1, p0, Lcom/osp/app/signin/TnCView;->Y:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_2

    invoke-static {p0}, Lcom/osp/app/util/r;->s(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 7480
    :cond_2
    invoke-static {p0}, Landroid/preference/PreferenceManager;->getDefaultSharedPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    .line 7481
    const-string v1, "NAME_VALIDATION_KEY"

    sget-object v2, Ljava/util/Locale;->ENGLISH:Ljava/util/Locale;

    invoke-virtual {p1, v2}, Ljava/lang/String;->toLowerCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    .line 7482
    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 7486
    :cond_3
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/osp/app/signin/SignUpCompleteView;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 7487
    const-string v1, "email_id"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 7489
    iget-boolean v1, p0, Lcom/osp/app/signin/TnCView;->aH:Z

    if-eqz v1, :cond_4

    .line 7491
    const-string v1, "key_welcome_content"

    iget-object v2, p0, Lcom/osp/app/signin/TnCView;->aG:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 7492
    const-string v1, "key_marketingpopup_mode"

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 7495
    :cond_4
    invoke-static {p0}, Lcom/osp/app/util/r;->p(Landroid/content/Context;)Z

    move-result v1

    if-eqz v1, :cond_5

    .line 7497
    const-string v1, "key_easy_signup_mode"

    iget-boolean v2, p0, Lcom/osp/app/signin/TnCView;->H:Z

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 7498
    const-string v1, "key_easy_signup_receive_marketing"

    iget-object v2, p0, Lcom/osp/app/signin/TnCView;->am:Lcom/osp/app/signin/SignUpinfo;

    invoke-virtual {v2}, Lcom/osp/app/signin/SignUpinfo;->l()Z

    move-result v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 7508
    :cond_5
    const/16 v1, 0xd7

    invoke-virtual {p0, v0, v1}, Lcom/osp/app/signin/TnCView;->startActivityForResult(Landroid/content/Intent;I)V

    .line 7509
    return-void

    .line 7474
    :catch_0
    move-exception v0

    :try_start_1
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v0

    iput-object v1, p0, Lcom/osp/app/signin/TnCView;->ai:Landroid/app/ProgressDialog;

    throw v0
.end method

.method static synthetic c(Lcom/osp/app/signin/TnCView;Z)Z
    .locals 0

    .prologue
    .line 108
    iput-boolean p1, p0, Lcom/osp/app/signin/TnCView;->E:Z

    return p1
.end method

.method static synthetic d(Lcom/osp/app/signin/TnCView;Landroid/widget/CheckBox;)Landroid/widget/CheckBox;
    .locals 0

    .prologue
    .line 108
    iput-object p1, p0, Lcom/osp/app/signin/TnCView;->aQ:Landroid/widget/CheckBox;

    return-object p1
.end method

.method private d()V
    .locals 2

    .prologue
    .line 726
    new-instance v0, Lcom/osp/app/signin/jz;

    invoke-direct {v0, p0}, Lcom/osp/app/signin/jz;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/osp/app/signin/TnCView;->ba:Lcom/osp/app/signin/jz;

    .line 727
    new-instance v0, Lcom/osp/security/identity/f;

    invoke-direct {v0, p0}, Lcom/osp/security/identity/f;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/osp/app/signin/TnCView;->be:Lcom/osp/security/identity/f;

    .line 728
    invoke-static {}, Lcom/osp/app/util/n;->a()Lcom/osp/app/util/n;

    move-result-object v0

    iput-object v0, p0, Lcom/osp/app/signin/TnCView;->an:Lcom/osp/app/util/n;

    .line 734
    invoke-virtual {p0}, Lcom/osp/app/signin/TnCView;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string v1, "country_code_mcc"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/osp/app/signin/TnCView;->Y:Ljava/lang/String;

    .line 736
    iget-object v0, p0, Lcom/osp/app/signin/TnCView;->Y:Ljava/lang/String;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/osp/app/signin/TnCView;->Y:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v0

    if-gtz v0, :cond_1

    .line 738
    :cond_0
    invoke-static {p0}, Lcom/osp/app/util/ad;->p(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/osp/app/signin/TnCView;->Y:Ljava/lang/String;

    .line 740
    :cond_1
    iget-object v0, p0, Lcom/osp/app/signin/TnCView;->Y:Ljava/lang/String;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/osp/app/signin/TnCView;->Y:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v0

    if-lez v0, :cond_2

    .line 742
    invoke-direct {p0}, Lcom/osp/app/signin/TnCView;->e()V

    .line 749
    :goto_0
    return-void

    .line 746
    :cond_2
    new-instance v0, Lcom/osp/app/signin/oc;

    invoke-direct {v0, p0}, Lcom/osp/app/signin/oc;-><init>(Lcom/osp/app/signin/TnCView;)V

    iput-object v0, p0, Lcom/osp/app/signin/TnCView;->Q:Lcom/osp/app/signin/oc;

    .line 747
    iget-object v0, p0, Lcom/osp/app/signin/TnCView;->Q:Lcom/osp/app/signin/oc;

    invoke-virtual {v0}, Lcom/osp/app/signin/oc;->b()V

    goto :goto_0
.end method

.method static synthetic d(Lcom/osp/app/signin/TnCView;Ljava/lang/String;)V
    .locals 7

    .prologue
    const/4 v6, 0x2

    const/4 v3, 0x0

    const/4 v2, 0x1

    .line 108
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "SignUpView::signUpEndProcess loginId : "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;)V

    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "mIsGoogleAccount : "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-boolean v1, p0, Lcom/osp/app/signin/TnCView;->aA:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", mIsFacebookAccount : "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-boolean v1, p0, Lcom/osp/app/signin/TnCView;->aB:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", mIsWeiboAccount : "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-boolean v1, p0, Lcom/osp/app/signin/TnCView;->aC:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;)V

    invoke-static {}, Lcom/osp/app/signin/SamsungService;->a()Z

    move-result v0

    if-eqz v0, :cond_3

    iget-boolean v0, p0, Lcom/osp/app/signin/TnCView;->aA:Z

    if-nez v0, :cond_0

    iget-boolean v0, p0, Lcom/osp/app/signin/TnCView;->aB:Z

    if-nez v0, :cond_0

    iget-boolean v0, p0, Lcom/osp/app/signin/TnCView;->aC:Z

    if-nez v0, :cond_0

    iget-boolean v0, p0, Lcom/osp/app/signin/TnCView;->aD:Z

    if-eqz v0, :cond_1

    :cond_0
    invoke-static {p0}, Landroid/preference/PreferenceManager;->getDefaultSharedPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    const-string v1, "EMAIL_VALIDATION_KEY"

    invoke-static {p0}, Lcom/osp/app/util/ad;->g(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    :cond_1
    invoke-direct {p0, p1}, Lcom/osp/app/signin/TnCView;->c(Ljava/lang/String;)V

    :goto_0
    iget-object v0, p0, Lcom/osp/app/signin/TnCView;->a:Landroid/content/Intent;

    const-string v1, "stay_duration"

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    iget-wide v4, p0, Lcom/osp/app/signin/TnCView;->aI:J

    sub-long/2addr v2, v4

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;J)Landroid/content/Intent;

    iget-object v0, p0, Lcom/osp/app/signin/TnCView;->aJ:Ljava/lang/String;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/osp/app/signin/TnCView;->a:Landroid/content/Intent;

    const-string v1, "previous_activity"

    iget-object v2, p0, Lcom/osp/app/signin/TnCView;->aJ:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    :cond_2
    new-instance v0, Lcom/osp/app/bigdatalog/c;

    invoke-virtual {p0}, Lcom/osp/app/signin/TnCView;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    iget-object v2, p0, Lcom/osp/app/signin/TnCView;->a:Landroid/content/Intent;

    invoke-direct {v0, v1, v6, v6, v2}, Lcom/osp/app/bigdatalog/c;-><init>(Landroid/content/Context;IILandroid/content/Intent;)V

    invoke-static {}, Lcom/osp/app/bigdatalog/e;->a()Lcom/osp/app/bigdatalog/e;

    move-result-object v1

    invoke-virtual {p0}, Lcom/osp/app/signin/TnCView;->getApplicationContext()Landroid/content/Context;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/osp/app/bigdatalog/e;->a(Landroid/content/Context;)V

    invoke-static {}, Lcom/osp/app/bigdatalog/e;->a()Lcom/osp/app/bigdatalog/e;

    move-result-object v1

    invoke-virtual {p0}, Lcom/osp/app/signin/TnCView;->getApplicationContext()Landroid/content/Context;

    move-result-object v2

    invoke-virtual {v1, v2, v0}, Lcom/osp/app/bigdatalog/e;->a(Landroid/content/Context;Lcom/osp/app/bigdatalog/c;)V

    return-void

    :cond_3
    iget-boolean v0, p0, Lcom/osp/app/signin/TnCView;->aA:Z

    if-eqz v0, :cond_4

    invoke-direct {p0, v2}, Lcom/osp/app/signin/TnCView;->b(Z)V

    const/4 v0, 0x0

    invoke-virtual {p0, v0, v3}, Lcom/osp/app/signin/TnCView;->a(ILandroid/content/DialogInterface$OnDismissListener;)V

    goto :goto_0

    :cond_4
    iget-boolean v0, p0, Lcom/osp/app/signin/TnCView;->aB:Z

    if-eqz v0, :cond_5

    invoke-direct {p0, v2}, Lcom/osp/app/signin/TnCView;->b(Z)V

    invoke-virtual {p0, v2, v3}, Lcom/osp/app/signin/TnCView;->a(ILandroid/content/DialogInterface$OnDismissListener;)V

    goto :goto_0

    :cond_5
    iget-boolean v0, p0, Lcom/osp/app/signin/TnCView;->aC:Z

    if-eqz v0, :cond_6

    invoke-direct {p0, v2}, Lcom/osp/app/signin/TnCView;->b(Z)V

    invoke-virtual {p0, v6, v3}, Lcom/osp/app/signin/TnCView;->a(ILandroid/content/DialogInterface$OnDismissListener;)V

    goto :goto_0

    :cond_6
    iget-boolean v0, p0, Lcom/osp/app/signin/TnCView;->aD:Z

    if-eqz v0, :cond_7

    invoke-direct {p0, v2}, Lcom/osp/app/signin/TnCView;->b(Z)V

    const/4 v0, 0x3

    invoke-virtual {p0, v0, v3}, Lcom/osp/app/signin/TnCView;->a(ILandroid/content/DialogInterface$OnDismissListener;)V

    goto :goto_0

    :cond_7
    invoke-static {}, Lcom/osp/device/b;->b()Lcom/osp/device/b;

    move-result-object v0

    invoke-virtual {v0}, Lcom/osp/device/b;->a()Z

    move-result v0

    if-eqz v0, :cond_8

    iget-boolean v0, p0, Lcom/osp/app/signin/TnCView;->aF:Z

    if-eqz v0, :cond_8

    invoke-direct {p0, p1}, Lcom/osp/app/signin/TnCView;->c(Ljava/lang/String;)V

    goto :goto_0

    :cond_8
    invoke-static {p0}, Lcom/osp/app/util/r;->p(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_9

    iget-boolean v0, p0, Lcom/osp/app/signin/TnCView;->H:Z

    if-eqz v0, :cond_9

    invoke-static {}, Lcom/osp/app/util/m;->a()Lcom/osp/app/util/m;

    iget-boolean v0, p0, Lcom/osp/app/signin/TnCView;->aU:Z

    invoke-static {p0, v0}, Lcom/osp/app/util/m;->a(Landroid/app/Activity;Z)V

    goto/16 :goto_0

    :cond_9
    invoke-direct {p0, p1}, Lcom/osp/app/signin/TnCView;->i(Ljava/lang/String;)V

    goto/16 :goto_0
.end method

.method static synthetic d(Lcom/osp/app/signin/TnCView;)Z
    .locals 1

    .prologue
    .line 108
    iget-boolean v0, p0, Lcom/osp/app/signin/TnCView;->al:Z

    return v0
.end method

.method static synthetic d(Lcom/osp/app/signin/TnCView;Z)Z
    .locals 0

    .prologue
    .line 108
    iput-boolean p1, p0, Lcom/osp/app/signin/TnCView;->F:Z

    return p1
.end method

.method static synthetic e(Lcom/osp/app/signin/TnCView;)Landroid/os/Bundle;
    .locals 1

    .prologue
    .line 108
    iget-object v0, p0, Lcom/osp/app/signin/TnCView;->aV:Landroid/os/Bundle;

    return-object v0
.end method

.method static synthetic e(Lcom/osp/app/signin/TnCView;Landroid/widget/CheckBox;)Landroid/widget/CheckBox;
    .locals 0

    .prologue
    .line 108
    iput-object p1, p0, Lcom/osp/app/signin/TnCView;->aR:Landroid/widget/CheckBox;

    return-object p1
.end method

.method static synthetic e(Lcom/osp/app/signin/TnCView;Ljava/lang/String;)Ljava/lang/String;
    .locals 0

    .prologue
    .line 108
    iput-object p1, p0, Lcom/osp/app/signin/TnCView;->ak:Ljava/lang/String;

    return-object p1
.end method

.method private e()V
    .locals 9

    .prologue
    const/16 v8, 0x8

    const/4 v7, 0x2

    const/4 v6, 0x3

    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 853
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v0, "TNC"

    const-string v1, "initializeAfterMcc"

    invoke-static {v0, v1}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 855
    invoke-virtual {p0}, Lcom/osp/app/signin/TnCView;->getIntent()Landroid/content/Intent;

    move-result-object v0

    iput-object v0, p0, Lcom/osp/app/signin/TnCView;->a:Landroid/content/Intent;

    iget-object v0, p0, Lcom/osp/app/signin/TnCView;->a:Landroid/content/Intent;

    const-string v1, "key_internal_sign_up_inforamtion"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/osp/app/signin/SignUpinfo;

    iput-object v0, p0, Lcom/osp/app/signin/TnCView;->am:Lcom/osp/app/signin/SignUpinfo;

    iget-object v0, p0, Lcom/osp/app/signin/TnCView;->a:Landroid/content/Intent;

    const-string v1, "key_google_account"

    invoke-virtual {v0, v1, v4}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v0

    iput-boolean v0, p0, Lcom/osp/app/signin/TnCView;->aA:Z

    iget-object v0, p0, Lcom/osp/app/signin/TnCView;->a:Landroid/content/Intent;

    const-string v1, "key_facebook_account"

    invoke-virtual {v0, v1, v4}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v0

    iput-boolean v0, p0, Lcom/osp/app/signin/TnCView;->aB:Z

    iget-object v0, p0, Lcom/osp/app/signin/TnCView;->a:Landroid/content/Intent;

    const-string v1, "key_weibo_account"

    invoke-virtual {v0, v1, v4}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v0

    iput-boolean v0, p0, Lcom/osp/app/signin/TnCView;->aC:Z

    iget-object v0, p0, Lcom/osp/app/signin/TnCView;->a:Landroid/content/Intent;

    const-string v1, "key_account_manager_account"

    invoke-virtual {v0, v1, v4}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v0

    iput-boolean v0, p0, Lcom/osp/app/signin/TnCView;->aD:Z

    iput-boolean v4, p0, Lcom/osp/app/signin/TnCView;->aE:Z

    invoke-static {}, Lcom/osp/device/b;->b()Lcom/osp/device/b;

    move-result-object v0

    invoke-virtual {v0}, Lcom/osp/device/b;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/osp/app/signin/TnCView;->am:Lcom/osp/app/signin/SignUpinfo;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/osp/app/signin/TnCView;->am:Lcom/osp/app/signin/SignUpinfo;

    invoke-virtual {v0}, Lcom/osp/app/signin/SignUpinfo;->e()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/telephony/PhoneNumberUtils;->isGlobalPhoneNumber(Ljava/lang/String;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/osp/app/signin/TnCView;->aF:Z

    :cond_0
    invoke-static {p0}, Lcom/osp/app/util/r;->p(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/osp/app/signin/TnCView;->am:Lcom/osp/app/signin/SignUpinfo;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/osp/app/signin/TnCView;->am:Lcom/osp/app/signin/SignUpinfo;

    invoke-virtual {v0}, Lcom/osp/app/signin/SignUpinfo;->l()Z

    move-result v0

    iput-boolean v0, p0, Lcom/osp/app/signin/TnCView;->aU:Z

    :cond_1
    iget-object v0, p0, Lcom/osp/app/signin/TnCView;->a:Landroid/content/Intent;

    const-string v1, "key_name_check_familyname"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/osp/app/signin/TnCView;->aw:Ljava/lang/String;

    iget-object v0, p0, Lcom/osp/app/signin/TnCView;->a:Landroid/content/Intent;

    const-string v1, "key_name_check_givenname"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/osp/app/signin/TnCView;->ax:Ljava/lang/String;

    iget-object v0, p0, Lcom/osp/app/signin/TnCView;->a:Landroid/content/Intent;

    const-string v1, "key_name_check_birthdate"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/osp/app/signin/TnCView;->ao:Ljava/lang/String;

    iget-object v0, p0, Lcom/osp/app/signin/TnCView;->a:Landroid/content/Intent;

    const-string v1, "key_name_check_method"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/osp/app/signin/TnCView;->ap:Ljava/lang/String;

    iget-object v0, p0, Lcom/osp/app/signin/TnCView;->a:Landroid/content/Intent;

    const-string v1, "key_name_check_ci"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/osp/app/signin/TnCView;->aq:Ljava/lang/String;

    iget-object v0, p0, Lcom/osp/app/signin/TnCView;->a:Landroid/content/Intent;

    const-string v1, "key_name_check_di"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/osp/app/signin/TnCView;->ar:Ljava/lang/String;

    iget-object v0, p0, Lcom/osp/app/signin/TnCView;->a:Landroid/content/Intent;

    const-string v1, "key_name_check_datetime"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/osp/app/signin/TnCView;->as:Ljava/lang/String;

    iget-object v0, p0, Lcom/osp/app/signin/TnCView;->a:Landroid/content/Intent;

    const-string v1, "key_name_check_foreigner"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/osp/app/signin/TnCView;->ay:Ljava/lang/String;

    iget-object v0, p0, Lcom/osp/app/signin/TnCView;->ao:Ljava/lang/String;

    if-eqz v0, :cond_2

    new-instance v0, Ljava/text/SimpleDateFormat;

    const-string v1, "yyyyMMddHHmmss"

    invoke-direct {v0, v1}, Ljava/text/SimpleDateFormat;-><init>(Ljava/lang/String;)V

    :try_start_0
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v2, p0, Lcom/osp/app/signin/TnCView;->ao:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "235959"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/text/SimpleDateFormat;->parse(Ljava/lang/String;)Ljava/util/Date;

    move-result-object v0

    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/util/Calendar;->setTime(Ljava/util/Date;)V

    const/4 v0, 0x1

    invoke-virtual {v1, v0}, Ljava/util/Calendar;->get(I)I

    move-result v0

    iput v0, p0, Lcom/osp/app/signin/TnCView;->au:I

    const/4 v0, 0x2

    invoke-virtual {v1, v0}, Ljava/util/Calendar;->get(I)I

    move-result v0

    iput v0, p0, Lcom/osp/app/signin/TnCView;->at:I

    const/4 v0, 0x5

    invoke-virtual {v1, v0}, Ljava/util/Calendar;->get(I)I

    move-result v0

    iput v0, p0, Lcom/osp/app/signin/TnCView;->av:I
    :try_end_0
    .catch Ljava/text/ParseException; {:try_start_0 .. :try_end_0} :catch_0

    :cond_2
    :goto_0
    iget-object v0, p0, Lcom/osp/app/signin/TnCView;->a:Landroid/content/Intent;

    const-string v1, "client_id"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/osp/app/signin/TnCView;->b:Ljava/lang/String;

    iget-object v0, p0, Lcom/osp/app/signin/TnCView;->a:Landroid/content/Intent;

    const-string v1, "client_secret"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/osp/app/signin/TnCView;->c:Ljava/lang/String;

    iget-object v0, p0, Lcom/osp/app/signin/TnCView;->a:Landroid/content/Intent;

    const-string v1, "account_mode"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/osp/app/signin/TnCView;->d:Ljava/lang/String;

    iget-object v0, p0, Lcom/osp/app/signin/TnCView;->a:Landroid/content/Intent;

    const-string v1, "service_name"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/osp/app/signin/TnCView;->y:Ljava/lang/String;

    iget-object v0, p0, Lcom/osp/app/signin/TnCView;->a:Landroid/content/Intent;

    const-string v1, "OSP_VER"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/osp/app/util/ad;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/osp/app/signin/TnCView;->e:Ljava/lang/String;

    iget-object v0, p0, Lcom/osp/app/signin/TnCView;->a:Landroid/content/Intent;

    const-string v1, "check_list"

    invoke-virtual {v0, v1, v4}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v0

    iput v0, p0, Lcom/osp/app/signin/TnCView;->af:I

    iget-object v0, p0, Lcom/osp/app/signin/TnCView;->a:Landroid/content/Intent;

    const-string v1, "new_add_account_mode"

    invoke-virtual {v0, v1, v4}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v0

    iput-boolean v0, p0, Lcom/osp/app/signin/TnCView;->ag:Z

    iget-object v0, p0, Lcom/osp/app/signin/TnCView;->a:Landroid/content/Intent;

    const-string v1, "authcode"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/osp/app/signin/TnCView;->ak:Ljava/lang/String;

    iget-object v0, p0, Lcom/osp/app/signin/TnCView;->a:Landroid/content/Intent;

    const-string v1, "key_str_signup_info"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/osp/app/signin/TnCView;->aj:Ljava/lang/String;

    iget-object v0, p0, Lcom/osp/app/signin/TnCView;->a:Landroid/content/Intent;

    const-string v1, "key_is_name_verified"

    invoke-virtual {v0, v1, v4}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v0

    iput-boolean v0, p0, Lcom/osp/app/signin/TnCView;->al:Z

    iget-boolean v0, p0, Lcom/osp/app/signin/TnCView;->al:Z

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/osp/app/signin/TnCView;->a:Landroid/content/Intent;

    const-string v1, "information_after_name_validation"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getBundleExtra(Ljava/lang/String;)Landroid/os/Bundle;

    move-result-object v0

    iput-object v0, p0, Lcom/osp/app/signin/TnCView;->aV:Landroid/os/Bundle;

    :cond_3
    const-string v0, "SamsungApps"

    iget-object v1, p0, Lcom/osp/app/signin/TnCView;->y:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_4

    iput-boolean v5, p0, Lcom/osp/app/signin/TnCView;->U:Z

    :cond_4
    const-string v0, "BG_mode"

    iget-object v1, p0, Lcom/osp/app/signin/TnCView;->f:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_5

    iget-object v0, p0, Lcom/osp/app/signin/TnCView;->a:Landroid/content/Intent;

    const-string v1, "mypackage"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/osp/app/signin/TnCView;->z:Ljava/lang/String;

    iget-object v0, p0, Lcom/osp/app/signin/TnCView;->a:Landroid/content/Intent;

    const-string v1, "key_request_id"

    const-wide/16 v2, 0x0

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/Intent;->getLongExtra(Ljava/lang/String;J)J

    move-result-wide v0

    iput-wide v0, p0, Lcom/osp/app/signin/TnCView;->aW:J

    :cond_5
    iget-object v0, p0, Lcom/osp/app/signin/TnCView;->a:Landroid/content/Intent;

    const-string v1, "MODE"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/osp/app/signin/TnCView;->f:Ljava/lang/String;

    iget-object v0, p0, Lcom/osp/app/signin/TnCView;->a:Landroid/content/Intent;

    const-string v1, "serviceApp_type"

    invoke-virtual {v0, v1, v4}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v0

    iput v0, p0, Lcom/osp/app/signin/TnCView;->A:I

    iget-object v0, p0, Lcom/osp/app/signin/TnCView;->a:Landroid/content/Intent;

    const-string v1, "key_tnc_update_mode"

    invoke-virtual {v0, v1, v4}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v0

    iput-boolean v0, p0, Lcom/osp/app/signin/TnCView;->V:Z

    iget-object v0, p0, Lcom/osp/app/signin/TnCView;->a:Landroid/content/Intent;

    const-string v1, "key_resign_in_another_id"

    invoke-virtual {v0, v1, v4}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v0

    iput-boolean v0, p0, Lcom/osp/app/signin/TnCView;->aL:Z

    iget-object v0, p0, Lcom/osp/app/signin/TnCView;->a:Landroid/content/Intent;

    const-string v1, "is_signup_flow"

    invoke-virtual {v0, v1, v4}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v0

    iput-boolean v0, p0, Lcom/osp/app/signin/TnCView;->I:Z

    iget-object v0, p0, Lcom/osp/app/signin/TnCView;->a:Landroid/content/Intent;

    const-string v1, "key_return_result"

    invoke-virtual {v0, v1, v4}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v0

    iput-boolean v0, p0, Lcom/osp/app/signin/TnCView;->aX:Z

    iget-object v0, p0, Lcom/osp/app/signin/TnCView;->a:Landroid/content/Intent;

    const-string v1, "key_no_notification"

    invoke-virtual {v0, v1, v4}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v0

    iput-boolean v0, p0, Lcom/osp/app/signin/TnCView;->aY:Z

    iget-object v0, p0, Lcom/osp/app/signin/TnCView;->a:Landroid/content/Intent;

    const-string v1, "key_marketingpopup_mode"

    invoke-virtual {v0, v1, v4}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v0

    iput-boolean v0, p0, Lcom/osp/app/signin/TnCView;->aH:Z

    iget-object v0, p0, Lcom/osp/app/signin/TnCView;->a:Landroid/content/Intent;

    const-string v1, "key_welcome_content"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/osp/app/signin/TnCView;->aG:Ljava/lang/String;

    iget-object v0, p0, Lcom/osp/app/signin/TnCView;->a:Landroid/content/Intent;

    const-string v1, "key_hide_tnc_update_popup"

    invoke-virtual {v0, v1, v4}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v0

    iput-boolean v0, p0, Lcom/osp/app/signin/TnCView;->aM:Z

    iget-object v0, p0, Lcom/osp/app/signin/TnCView;->a:Landroid/content/Intent;

    const-string v1, "key_easy_signup_tnc_mode"

    invoke-virtual {v0, v1, v4}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v0

    iput-boolean v0, p0, Lcom/osp/app/signin/TnCView;->aT:Z

    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "TnCView - paramFromServiceApp - m_client_id : "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v1, p0, Lcom/osp/app/signin/TnCView;->b:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;)V

    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "TnCView - paramFromServiceApp - m_client_secret : "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v1, p0, Lcom/osp/app/signin/TnCView;->c:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;)V

    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "TnCView - paramFromServiceApp - m_account_mode : "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v1, p0, Lcom/osp/app/signin/TnCView;->d:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;)V

    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "TnCView - paramFromServiceApp - m_setting_mode : "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v1, p0, Lcom/osp/app/signin/TnCView;->f:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;)V

    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "TnCView - paramFromServiceApp - m_service_name : "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v1, p0, Lcom/osp/app/signin/TnCView;->y:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;)V

    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "TnCView - paramFromServiceApp - m_OspVersion : "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v1, p0, Lcom/osp/app/signin/TnCView;->e:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;)V

    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "TnCView - paramFromServiceApp - m_ServiceAppType : "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v1, p0, Lcom/osp/app/signin/TnCView;->A:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;)V

    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "TnCView - paramFromServiceApp - m_Sourcepackage : "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v1, p0, Lcom/osp/app/signin/TnCView;->z:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;)V

    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "TnCView - paramFromServiceApp - mIsSamsungApps : "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-boolean v1, p0, Lcom/osp/app/signin/TnCView;->U:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;)V

    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "TnCView - paramFromServiceApp - mIsUpdateMode : "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-boolean v1, p0, Lcom/osp/app/signin/TnCView;->V:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;)V

    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "TnCView - paramFromServiceApp - mIsResigninAnotherID : "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-boolean v1, p0, Lcom/osp/app/signin/TnCView;->aL:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;)V

    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "TnCView - paramFromServiceApp - mEasySignupTncMode : "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-boolean v1, p0, Lcom/osp/app/signin/TnCView;->aT:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;)V

    .line 857
    const-string v0, "com.msc.action.samsungaccount.Update_NewTerms"

    iget-object v1, p0, Lcom/osp/app/signin/TnCView;->a:Landroid/content/Intent;

    invoke-virtual {v1}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_6

    invoke-static {p0}, Lcom/osp/app/util/ad;->d(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_6

    invoke-static {p0, v5}, Lcom/msc/c/c;->a(Landroid/content/Context;Z)Z

    move-result v0

    if-nez v0, :cond_6

    .line 859
    invoke-static {p0}, Lcom/osp/app/util/ad;->h(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/osp/app/signin/TnCView;->b(Ljava/lang/String;)V

    .line 860
    invoke-virtual {p0}, Lcom/osp/app/signin/TnCView;->finish()V

    .line 863
    :cond_6
    iget-object v0, p0, Lcom/osp/app/signin/TnCView;->a:Landroid/content/Intent;

    invoke-virtual {v0}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_12

    const-string v0, "com.msc.action.samsungaccount.tncs"

    iget-object v1, p0, Lcom/osp/app/signin/TnCView;->a:Landroid/content/Intent;

    invoke-virtual {v1}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_10

    iput-boolean v5, p0, Lcom/osp/app/signin/TnCView;->W:Z

    .line 865
    :cond_7
    :goto_1
    iget-boolean v0, p0, Lcom/osp/app/signin/TnCView;->I:Z

    if-eqz v0, :cond_13

    .line 867
    invoke-static {}, Lcom/osp/device/b;->b()Lcom/osp/device/b;

    move-result-object v0

    invoke-virtual {v0}, Lcom/osp/device/b;->a()Z

    move-result v0

    if-eqz v0, :cond_8

    iget-object v0, p0, Lcom/osp/app/signin/TnCView;->am:Lcom/osp/app/signin/SignUpinfo;

    invoke-virtual {v0}, Lcom/osp/app/signin/SignUpinfo;->e()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/telephony/PhoneNumberUtils;->isGlobalPhoneNumber(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_8

    iget-boolean v0, p0, Lcom/osp/app/signin/TnCView;->aB:Z

    if-eqz v0, :cond_13

    .line 869
    :cond_8
    new-instance v0, Lcom/osp/app/signin/oe;

    invoke-direct {v0, p0}, Lcom/osp/app/signin/oe;-><init>(Lcom/osp/app/signin/TnCView;)V

    iput-object v0, p0, Lcom/osp/app/signin/TnCView;->S:Lcom/osp/app/signin/oe;

    iget-object v0, p0, Lcom/osp/app/signin/TnCView;->S:Lcom/osp/app/signin/oe;

    invoke-virtual {v0}, Lcom/osp/app/signin/oe;->b()V

    .line 879
    :goto_2
    const-string v0, "450"

    iget-object v1, p0, Lcom/osp/app/signin/TnCView;->Y:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_14

    iput-boolean v5, p0, Lcom/osp/app/signin/TnCView;->X:Z

    :goto_3
    iget-boolean v0, p0, Lcom/osp/app/signin/TnCView;->X:Z

    if-eqz v0, :cond_16

    iget-boolean v0, p0, Lcom/osp/app/signin/TnCView;->W:Z

    if-eqz v0, :cond_15

    iput v6, p0, Lcom/osp/app/signin/TnCView;->ad:I

    :goto_4
    iget-boolean v1, p0, Lcom/osp/app/signin/TnCView;->ae:Z

    iget-boolean v2, p0, Lcom/osp/app/signin/TnCView;->W:Z

    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v3, "TnCView - initializeComponent - _updateTerms : "

    invoke-direct {v0, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;)V

    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v3, "TnCView - initializeComponent - _noButton : "

    invoke-direct {v0, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;)V

    invoke-static {}, Lcom/osp/app/signin/SamsungService;->a()Z

    move-result v0

    if-eqz v0, :cond_9

    invoke-static {}, Lcom/osp/device/b;->b()Lcom/osp/device/b;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/osp/device/b;->i(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_9

    const v0, 0x7f0c0181

    invoke-virtual {p0, v0}, Lcom/osp/app/signin/TnCView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    invoke-virtual {v0, v4}, Landroid/widget/LinearLayout;->setVisibility(I)V

    :cond_9
    if-nez v2, :cond_a

    new-instance v0, Lcom/osp/app/signin/ny;

    invoke-direct {v0, p0, v1}, Lcom/osp/app/signin/ny;-><init>(Lcom/osp/app/signin/TnCView;Z)V

    new-instance v1, Lcom/osp/app/signin/nz;

    invoke-direct {v1, p0}, Lcom/osp/app/signin/nz;-><init>(Lcom/osp/app/signin/TnCView;)V

    iget-object v2, p0, Lcom/osp/app/signin/TnCView;->aK:Lcom/osp/app/signin/BottomSoftkeyLinearLayout;

    if-eqz v2, :cond_a

    invoke-static {}, Lcom/osp/device/b;->b()Lcom/osp/device/b;

    move-result-object v2

    invoke-virtual {v2, p0}, Lcom/osp/device/b;->l(Landroid/content/Context;)Z

    move-result v2

    if-eqz v2, :cond_18

    iget-object v2, p0, Lcom/osp/app/signin/TnCView;->aK:Lcom/osp/app/signin/BottomSoftkeyLinearLayout;

    invoke-static {v4}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/osp/app/signin/BottomSoftkeyLinearLayout;->a(Ljava/lang/Boolean;)V

    iget-object v2, p0, Lcom/osp/app/signin/TnCView;->aK:Lcom/osp/app/signin/BottomSoftkeyLinearLayout;

    invoke-virtual {v2, v0}, Lcom/osp/app/signin/BottomSoftkeyLinearLayout;->a(Landroid/view/View$OnClickListener;)V

    iget-object v0, p0, Lcom/osp/app/signin/TnCView;->aK:Lcom/osp/app/signin/BottomSoftkeyLinearLayout;

    invoke-virtual {v0, v1}, Lcom/osp/app/signin/BottomSoftkeyLinearLayout;->b(Landroid/view/View$OnClickListener;)V

    .line 882
    :cond_a
    :goto_5
    const v0, 0x7f0c0184

    invoke-virtual {p0, v0}, Lcom/osp/app/signin/TnCView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ListView;

    iput-object v0, p0, Lcom/osp/app/signin/TnCView;->K:Landroid/widget/ListView;

    .line 883
    iget-object v0, p0, Lcom/osp/app/signin/TnCView;->K:Landroid/widget/ListView;

    if-eqz v0, :cond_b

    iget-boolean v0, p0, Lcom/osp/app/signin/TnCView;->W:Z

    if-eqz v0, :cond_b

    invoke-static {p0}, Lcom/osp/app/util/r;->g(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_b

    .line 885
    iget-object v0, p0, Lcom/osp/app/signin/TnCView;->K:Landroid/widget/ListView;

    invoke-virtual {v0, v5}, Landroid/widget/ListView;->setFooterDividersEnabled(Z)V

    .line 886
    new-array v0, v5, [I

    const v1, 0x1010214

    aput v1, v0, v4

    .line 887
    invoke-virtual {p0, v0}, Lcom/osp/app/signin/TnCView;->obtainStyledAttributes([I)Landroid/content/res/TypedArray;

    move-result-object v0

    .line 889
    invoke-virtual {v0, v4}, Landroid/content/res/TypedArray;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    .line 891
    invoke-virtual {v0}, Landroid/content/res/TypedArray;->recycle()V

    .line 892
    iget-object v0, p0, Lcom/osp/app/signin/TnCView;->K:Landroid/widget/ListView;

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setDivider(Landroid/graphics/drawable/Drawable;)V

    .line 893
    iget-object v0, p0, Lcom/osp/app/signin/TnCView;->K:Landroid/widget/ListView;

    new-instance v1, Landroid/view/View;

    invoke-direct {v1, p0}, Landroid/view/View;-><init>(Landroid/content/Context;)V

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->addFooterView(Landroid/view/View;)V

    .line 895
    :cond_b
    iget-object v0, p0, Lcom/osp/app/signin/TnCView;->K:Landroid/widget/ListView;

    invoke-virtual {v0, v5}, Landroid/widget/ListView;->setItemsCanFocus(Z)V

    .line 897
    iget v0, p0, Lcom/osp/app/signin/TnCView;->ad:I

    if-eq v0, v5, :cond_c

    iget v0, p0, Lcom/osp/app/signin/TnCView;->ad:I

    if-ne v0, v6, :cond_d

    :cond_c
    iget-object v0, p0, Lcom/osp/app/signin/TnCView;->aK:Lcom/osp/app/signin/BottomSoftkeyLinearLayout;

    if-eqz v0, :cond_d

    iget-object v0, p0, Lcom/osp/app/signin/TnCView;->aK:Lcom/osp/app/signin/BottomSoftkeyLinearLayout;

    invoke-virtual {v0, v8}, Lcom/osp/app/signin/BottomSoftkeyLinearLayout;->setVisibility(I)V

    .line 899
    :cond_d
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "TnCView - onCreate m_account_mode : "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v1, p0, Lcom/osp/app/signin/TnCView;->d:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;)V

    .line 900
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "TnCView - onCreate mTnCListType : "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v1, p0, Lcom/osp/app/signin/TnCView;->ad:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;)V

    .line 901
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "TnCView - onCreate m_intent.getAction() : "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v1, p0, Lcom/osp/app/signin/TnCView;->a:Landroid/content/Intent;

    invoke-virtual {v1}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;)V

    .line 902
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "TnCView - onCreate m_mcc : "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v1, p0, Lcom/osp/app/signin/TnCView;->Y:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;)V

    .line 905
    invoke-virtual {p0}, Lcom/osp/app/signin/TnCView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v0

    iget v0, v0, Landroid/content/res/Configuration;->orientation:I

    invoke-direct {p0, v0}, Lcom/osp/app/signin/TnCView;->a(I)V

    .line 907
    invoke-static {}, Lcom/osp/app/signin/SamsungService;->a()Z

    move-result v0

    if-nez v0, :cond_f

    invoke-static {}, Lcom/osp/device/b;->b()Lcom/osp/device/b;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/osp/device/b;->i(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_f

    iget v0, p0, Lcom/osp/app/signin/TnCView;->ad:I

    if-eq v0, v5, :cond_f

    iget v0, p0, Lcom/osp/app/signin/TnCView;->ad:I

    if-eq v0, v6, :cond_f

    .line 910
    const v0, 0x7f0c0028

    invoke-virtual {p0, v0}, Lcom/osp/app/signin/TnCView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    .line 911
    if-eqz v0, :cond_e

    .line 913
    invoke-virtual {v0, v8}, Landroid/view/View;->setVisibility(I)V

    .line 916
    :cond_e
    const-string v0, "agree_cancel"

    invoke-virtual {p0, v0}, Lcom/osp/app/signin/TnCView;->e(Ljava/lang/String;)V

    .line 923
    :cond_f
    invoke-static {p0}, Lcom/msc/sa/c/d;->a(Landroid/app/Activity;)V

    .line 924
    return-void

    .line 855
    :catch_0
    move-exception v0

    invoke-virtual {v0}, Ljava/text/ParseException;->printStackTrace()V

    goto/16 :goto_0

    .line 863
    :cond_10
    const-string v0, "com.msc.action.samsungaccount.Update_NewTerms"

    iget-object v1, p0, Lcom/osp/app/signin/TnCView;->a:Landroid/content/Intent;

    invoke-virtual {v1}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_11

    const-string v0, "com.msc.action.samsungaccount.Update_NewTerms_external"

    iget-object v1, p0, Lcom/osp/app/signin/TnCView;->a:Landroid/content/Intent;

    invoke-virtual {v1}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_7

    :cond_11
    iput-boolean v5, p0, Lcom/osp/app/signin/TnCView;->ae:Z

    goto/16 :goto_1

    :cond_12
    iget-object v0, p0, Lcom/osp/app/signin/TnCView;->d:Ljava/lang/String;

    if-eqz v0, :cond_7

    iget-object v0, p0, Lcom/osp/app/signin/TnCView;->d:Ljava/lang/String;

    const-string v1, "REQUEST_TNC"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_7

    iput-boolean v5, p0, Lcom/osp/app/signin/TnCView;->W:Z

    goto/16 :goto_1

    .line 876
    :cond_13
    new-instance v0, Lcom/osp/app/signin/od;

    invoke-direct {v0, p0}, Lcom/osp/app/signin/od;-><init>(Lcom/osp/app/signin/TnCView;)V

    iput-object v0, p0, Lcom/osp/app/signin/TnCView;->R:Lcom/osp/app/signin/od;

    iget-object v0, p0, Lcom/osp/app/signin/TnCView;->R:Lcom/osp/app/signin/od;

    invoke-virtual {v0}, Lcom/osp/app/signin/od;->b()V

    goto/16 :goto_2

    .line 879
    :cond_14
    iput-boolean v4, p0, Lcom/osp/app/signin/TnCView;->X:Z

    goto/16 :goto_3

    :cond_15
    iput v7, p0, Lcom/osp/app/signin/TnCView;->ad:I

    goto/16 :goto_4

    :cond_16
    iget-boolean v0, p0, Lcom/osp/app/signin/TnCView;->W:Z

    if-eqz v0, :cond_17

    iput v5, p0, Lcom/osp/app/signin/TnCView;->ad:I

    goto/16 :goto_4

    :cond_17
    iput v4, p0, Lcom/osp/app/signin/TnCView;->ad:I

    goto/16 :goto_4

    :cond_18
    iget-object v2, p0, Lcom/osp/app/signin/TnCView;->aK:Lcom/osp/app/signin/BottomSoftkeyLinearLayout;

    invoke-static {v4}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/osp/app/signin/BottomSoftkeyLinearLayout;->b(Ljava/lang/Boolean;)V

    iget-object v2, p0, Lcom/osp/app/signin/TnCView;->aK:Lcom/osp/app/signin/BottomSoftkeyLinearLayout;

    invoke-virtual {v2, v0}, Lcom/osp/app/signin/BottomSoftkeyLinearLayout;->b(Landroid/view/View$OnClickListener;)V

    iget-object v0, p0, Lcom/osp/app/signin/TnCView;->aK:Lcom/osp/app/signin/BottomSoftkeyLinearLayout;

    invoke-virtual {v0, v1}, Lcom/osp/app/signin/BottomSoftkeyLinearLayout;->a(Landroid/view/View$OnClickListener;)V

    goto/16 :goto_5
.end method

.method static synthetic e(Lcom/osp/app/signin/TnCView;Z)Z
    .locals 0

    .prologue
    .line 108
    iput-boolean p1, p0, Lcom/osp/app/signin/TnCView;->G:Z

    return p1
.end method

.method static synthetic f(Lcom/osp/app/signin/TnCView;Landroid/widget/CheckBox;)Landroid/widget/CheckBox;
    .locals 0

    .prologue
    .line 108
    iput-object p1, p0, Lcom/osp/app/signin/TnCView;->aS:Landroid/widget/CheckBox;

    return-object p1
.end method

.method static synthetic f(Lcom/osp/app/signin/TnCView;Ljava/lang/String;)Ljava/lang/String;
    .locals 0

    .prologue
    .line 108
    iput-object p1, p0, Lcom/osp/app/signin/TnCView;->bb:Ljava/lang/String;

    return-object p1
.end method

.method private f()V
    .locals 2

    .prologue
    .line 5731
    iget-object v0, p0, Lcom/osp/app/signin/TnCView;->aZ:Lcom/osp/app/signin/ok;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/osp/app/signin/TnCView;->aZ:Lcom/osp/app/signin/ok;

    invoke-virtual {v0}, Lcom/osp/app/signin/ok;->getStatus()Landroid/os/AsyncTask$Status;

    move-result-object v0

    sget-object v1, Landroid/os/AsyncTask$Status;->RUNNING:Landroid/os/AsyncTask$Status;

    if-eq v0, v1, :cond_1

    .line 5733
    :cond_0
    new-instance v0, Lcom/osp/app/signin/ok;

    invoke-direct {v0, p0, p0}, Lcom/osp/app/signin/ok;-><init>(Lcom/osp/app/signin/TnCView;Landroid/content/Context;)V

    iput-object v0, p0, Lcom/osp/app/signin/TnCView;->aZ:Lcom/osp/app/signin/ok;

    .line 5735
    iget-object v0, p0, Lcom/osp/app/signin/TnCView;->aZ:Lcom/osp/app/signin/ok;

    invoke-virtual {v0}, Lcom/osp/app/signin/ok;->b()V

    .line 5737
    :cond_1
    return-void
.end method

.method static synthetic f(Lcom/osp/app/signin/TnCView;)Z
    .locals 1

    .prologue
    .line 108
    iget-boolean v0, p0, Lcom/osp/app/signin/TnCView;->O:Z

    return v0
.end method

.method static synthetic f(Lcom/osp/app/signin/TnCView;Z)Z
    .locals 0

    .prologue
    .line 108
    iput-boolean p1, p0, Lcom/osp/app/signin/TnCView;->H:Z

    return p1
.end method

.method static synthetic g(Lcom/osp/app/signin/TnCView;Ljava/lang/String;)V
    .locals 4

    .prologue
    .line 108
    new-instance v0, Landroid/content/Intent;

    const-string v1, "com.msc.action.samsungaccount.resignin"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    const-string v1, "client_id"

    iget-object v2, p0, Lcom/osp/app/signin/TnCView;->b:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    const-string v1, "client_secret"

    iget-object v2, p0, Lcom/osp/app/signin/TnCView;->c:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    const-string v1, "account_mode"

    iget-object v2, p0, Lcom/osp/app/signin/TnCView;->d:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    const-string v1, "OSP_VER"

    iget-object v2, p0, Lcom/osp/app/signin/TnCView;->e:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    const-string v1, "MODE"

    iget-object v2, p0, Lcom/osp/app/signin/TnCView;->f:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    const-string v1, "key_request_id"

    iget-wide v2, p0, Lcom/osp/app/signin/TnCView;->aW:J

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;J)Landroid/content/Intent;

    const-string v1, "key_request_resign_dialog"

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    if-eqz p1, :cond_0

    const-string v1, "email_id"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    :cond_0
    invoke-virtual {p0, v0}, Lcom/osp/app/signin/TnCView;->startActivity(Landroid/content/Intent;)V

    return-void
.end method

.method static synthetic g(Lcom/osp/app/signin/TnCView;)Z
    .locals 1

    .prologue
    .line 108
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/osp/app/signin/TnCView;->O:Z

    return v0
.end method

.method static synthetic h(Lcom/osp/app/signin/TnCView;)I
    .locals 1

    .prologue
    .line 108
    iget v0, p0, Lcom/osp/app/signin/TnCView;->ad:I

    return v0
.end method

.method private i(Ljava/lang/String;)V
    .locals 4

    .prologue
    const/4 v3, 0x1

    .line 7518
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/osp/app/signin/EmailValidationView;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 7523
    iget-object v1, p0, Lcom/osp/app/signin/TnCView;->a:Landroid/content/Intent;

    const-string v2, "from_sign_up"

    invoke-virtual {v1, v2}, Landroid/content/Intent;->hasExtra(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 7525
    const-string v1, "from_sign_up"

    invoke-virtual {v0, v1, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 7528
    :cond_0
    const-string v1, "email_id"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 7529
    const-string v1, "is_resend"

    invoke-virtual {v0, v1, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 7530
    const-string v1, "is_bgmode"

    const/16 v2, 0xcc

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 7531
    const-string v1, "key_request_id"

    iget-wide v2, p0, Lcom/osp/app/signin/TnCView;->aW:J

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;J)Landroid/content/Intent;

    .line 7532
    const-string v1, "is_signup_flow"

    iget-boolean v2, p0, Lcom/osp/app/signin/TnCView;->I:Z

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 7533
    const/16 v1, 0xc9

    invoke-virtual {p0, v0, v1}, Lcom/osp/app/signin/TnCView;->startActivityForResult(Landroid/content/Intent;I)V

    .line 7534
    return-void
.end method

.method static synthetic i(Lcom/osp/app/signin/TnCView;)Z
    .locals 1

    .prologue
    .line 108
    iget-boolean v0, p0, Lcom/osp/app/signin/TnCView;->aT:Z

    return v0
.end method

.method static synthetic j(Lcom/osp/app/signin/TnCView;)Z
    .locals 1

    .prologue
    .line 108
    iget-boolean v0, p0, Lcom/osp/app/signin/TnCView;->W:Z

    return v0
.end method

.method static synthetic k(Lcom/osp/app/signin/TnCView;)Z
    .locals 1

    .prologue
    .line 108
    iget-boolean v0, p0, Lcom/osp/app/signin/TnCView;->D:Z

    return v0
.end method

.method static synthetic l(Lcom/osp/app/signin/TnCView;)Z
    .locals 1

    .prologue
    .line 108
    iget-boolean v0, p0, Lcom/osp/app/signin/TnCView;->E:Z

    return v0
.end method

.method static synthetic m(Lcom/osp/app/signin/TnCView;)Z
    .locals 1

    .prologue
    .line 108
    iget-boolean v0, p0, Lcom/osp/app/signin/TnCView;->F:Z

    return v0
.end method

.method static synthetic n(Lcom/osp/app/signin/TnCView;)Z
    .locals 1

    .prologue
    .line 108
    iget-boolean v0, p0, Lcom/osp/app/signin/TnCView;->G:Z

    return v0
.end method

.method static synthetic o(Lcom/osp/app/signin/TnCView;)V
    .locals 3

    .prologue
    .line 108
    iget-object v0, p0, Lcom/osp/app/signin/TnCView;->K:Landroid/widget/ListView;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/osp/app/signin/TnCView;->K:Landroid/widget/ListView;

    iget-object v1, p0, Lcom/osp/app/signin/TnCView;->M:Lcom/osp/app/signin/oj;

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setAdapter(Landroid/widget/ListAdapter;)V

    iget-object v0, p0, Lcom/osp/app/signin/TnCView;->K:Landroid/widget/ListView;

    new-instance v1, Lcom/osp/app/signin/oh;

    const/4 v2, 0x0

    invoke-direct {v1, p0, v2}, Lcom/osp/app/signin/oh;-><init>(Lcom/osp/app/signin/TnCView;B)V

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V

    iget-object v0, p0, Lcom/osp/app/signin/TnCView;->K:Landroid/widget/ListView;

    invoke-virtual {v0}, Landroid/widget/ListView;->clearFocus()V

    :cond_0
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/osp/app/signin/TnCView;->N:Z

    invoke-static {}, Lcom/osp/app/signin/SamsungService;->a()Z

    move-result v0

    if-eqz v0, :cond_3

    const v0, 0x7f09004f

    invoke-static {p0, v0}, Lcom/msc/sa/c/d;->a(Landroid/app/Activity;I)V

    :goto_0
    iget v0, p0, Lcom/osp/app/signin/TnCView;->ad:I

    if-eqz v0, :cond_1

    iget v0, p0, Lcom/osp/app/signin/TnCView;->ad:I

    const/4 v1, 0x2

    if-ne v0, v1, :cond_2

    :cond_1
    invoke-static {}, Lcom/osp/device/b;->b()Lcom/osp/device/b;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/osp/device/b;->i(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_4

    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/osp/app/signin/TnCView;->e(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/osp/app/signin/TnCView;->x:Landroid/view/Menu;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/osp/app/signin/TnCView;->x:Landroid/view/Menu;

    invoke-interface {v0}, Landroid/view/Menu;->clear()V

    :cond_2
    :goto_1
    return-void

    :cond_3
    const v0, 0x7f0900bd

    invoke-static {p0, v0}, Lcom/msc/sa/c/d;->a(Landroid/app/Activity;I)V

    goto :goto_0

    :cond_4
    const v0, 0x7f0c0065

    invoke-virtual {p0, v0}, Lcom/osp/app/signin/TnCView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    if-eqz v0, :cond_2

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    goto :goto_1
.end method

.method static synthetic p(Lcom/osp/app/signin/TnCView;)Ljava/util/ArrayList;
    .locals 1

    .prologue
    .line 108
    iget-object v0, p0, Lcom/osp/app/signin/TnCView;->ah:Ljava/util/ArrayList;

    return-object v0
.end method

.method static synthetic q(Lcom/osp/app/signin/TnCView;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 108
    iget-object v0, p0, Lcom/osp/app/signin/TnCView;->Y:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic r(Lcom/osp/app/signin/TnCView;)Landroid/widget/CheckBox;
    .locals 1

    .prologue
    .line 108
    iget-object v0, p0, Lcom/osp/app/signin/TnCView;->aO:Landroid/widget/CheckBox;

    return-object v0
.end method

.method static synthetic s(Lcom/osp/app/signin/TnCView;)Landroid/widget/CheckBox;
    .locals 1

    .prologue
    .line 108
    iget-object v0, p0, Lcom/osp/app/signin/TnCView;->aP:Landroid/widget/CheckBox;

    return-object v0
.end method

.method static synthetic t(Lcom/osp/app/signin/TnCView;)Landroid/widget/CheckBox;
    .locals 1

    .prologue
    .line 108
    iget-object v0, p0, Lcom/osp/app/signin/TnCView;->aQ:Landroid/widget/CheckBox;

    return-object v0
.end method

.method static synthetic u(Lcom/osp/app/signin/TnCView;)Landroid/widget/CheckBox;
    .locals 1

    .prologue
    .line 108
    iget-object v0, p0, Lcom/osp/app/signin/TnCView;->aR:Landroid/widget/CheckBox;

    return-object v0
.end method

.method static synthetic v(Lcom/osp/app/signin/TnCView;)Landroid/widget/CheckBox;
    .locals 1

    .prologue
    .line 108
    iget-object v0, p0, Lcom/osp/app/signin/TnCView;->aS:Landroid/widget/CheckBox;

    return-object v0
.end method

.method static synthetic w(Lcom/osp/app/signin/TnCView;)Lcom/osp/app/signin/op;
    .locals 1

    .prologue
    .line 108
    iget-object v0, p0, Lcom/osp/app/signin/TnCView;->L:Lcom/osp/app/signin/op;

    return-object v0
.end method

.method static synthetic x(Lcom/osp/app/signin/TnCView;)Landroid/widget/CheckBox;
    .locals 1

    .prologue
    .line 108
    iget-object v0, p0, Lcom/osp/app/signin/TnCView;->aN:Landroid/widget/CheckBox;

    return-object v0
.end method

.method static synthetic y(Lcom/osp/app/signin/TnCView;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 108
    iget-object v0, p0, Lcom/osp/app/signin/TnCView;->b:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic z(Lcom/osp/app/signin/TnCView;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 108
    iget-object v0, p0, Lcom/osp/app/signin/TnCView;->ab:Ljava/lang/String;

    return-object v0
.end method


# virtual methods
.method protected final a()V
    .locals 5

    .prologue
    const v4, 0x7f0c0199

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 2209
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "TnCView - ConfirmButtonVerify - mcc : "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v1, p0, Lcom/osp/app/signin/TnCView;->Y:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;)V

    .line 2211
    iget-object v0, p0, Lcom/osp/app/signin/TnCView;->aK:Lcom/osp/app/signin/BottomSoftkeyLinearLayout;

    if-eqz v0, :cond_0

    .line 2213
    const-string v0, "450"

    iget-object v1, p0, Lcom/osp/app/signin/TnCView;->Y:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_5

    .line 2215
    iget-boolean v0, p0, Lcom/osp/app/signin/TnCView;->D:Z

    if-eqz v0, :cond_3

    iget-boolean v0, p0, Lcom/osp/app/signin/TnCView;->E:Z

    if-eqz v0, :cond_3

    .line 2217
    invoke-static {}, Lcom/osp/device/b;->b()Lcom/osp/device/b;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/osp/device/b;->l(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 2219
    iget-object v0, p0, Lcom/osp/app/signin/TnCView;->aK:Lcom/osp/app/signin/BottomSoftkeyLinearLayout;

    invoke-static {v3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/osp/app/signin/BottomSoftkeyLinearLayout;->a(Ljava/lang/Boolean;)V

    .line 2257
    :cond_0
    :goto_0
    invoke-static {p0}, Lcom/osp/app/util/r;->e(Landroid/content/Context;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 2259
    iget-object v0, p0, Lcom/osp/app/signin/TnCView;->Y:Ljava/lang/String;

    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v1, "agreeMenuCheck"

    invoke-static {v1}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;)V

    invoke-static {}, Lcom/osp/device/b;->b()Lcom/osp/device/b;

    move-result-object v1

    invoke-virtual {v1, p0}, Lcom/osp/device/b;->i(Landroid/content/Context;)Z

    move-result v1

    if-eqz v1, :cond_1

    iget-object v1, p0, Lcom/osp/app/signin/TnCView;->x:Landroid/view/Menu;

    if-eqz v1, :cond_1

    iget-object v1, p0, Lcom/osp/app/signin/TnCView;->x:Landroid/view/Menu;

    invoke-interface {v1, v4}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v1

    if-eqz v1, :cond_1

    const-string v1, "450"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_a

    iget-boolean v0, p0, Lcom/osp/app/signin/TnCView;->D:Z

    if-eqz v0, :cond_9

    iget-boolean v0, p0, Lcom/osp/app/signin/TnCView;->E:Z

    if-eqz v0, :cond_9

    iget-object v0, p0, Lcom/osp/app/signin/TnCView;->x:Landroid/view/Menu;

    invoke-interface {v0, v4}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v0

    invoke-interface {v0, v3}, Landroid/view/MenuItem;->setEnabled(Z)Landroid/view/MenuItem;

    .line 2262
    :cond_1
    :goto_1
    return-void

    .line 2222
    :cond_2
    iget-object v0, p0, Lcom/osp/app/signin/TnCView;->aK:Lcom/osp/app/signin/BottomSoftkeyLinearLayout;

    invoke-static {v3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/osp/app/signin/BottomSoftkeyLinearLayout;->b(Ljava/lang/Boolean;)V

    goto :goto_0

    .line 2226
    :cond_3
    invoke-static {}, Lcom/osp/device/b;->b()Lcom/osp/device/b;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/osp/device/b;->l(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 2228
    iget-object v0, p0, Lcom/osp/app/signin/TnCView;->aK:Lcom/osp/app/signin/BottomSoftkeyLinearLayout;

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/osp/app/signin/BottomSoftkeyLinearLayout;->a(Ljava/lang/Boolean;)V

    goto :goto_0

    .line 2231
    :cond_4
    iget-object v0, p0, Lcom/osp/app/signin/TnCView;->aK:Lcom/osp/app/signin/BottomSoftkeyLinearLayout;

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/osp/app/signin/BottomSoftkeyLinearLayout;->b(Ljava/lang/Boolean;)V

    goto :goto_0

    .line 2236
    :cond_5
    iget-boolean v0, p0, Lcom/osp/app/signin/TnCView;->D:Z

    if-eqz v0, :cond_7

    iget-boolean v0, p0, Lcom/osp/app/signin/TnCView;->E:Z

    if-eqz v0, :cond_7

    .line 2238
    invoke-static {}, Lcom/osp/device/b;->b()Lcom/osp/device/b;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/osp/device/b;->l(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_6

    .line 2240
    iget-object v0, p0, Lcom/osp/app/signin/TnCView;->aK:Lcom/osp/app/signin/BottomSoftkeyLinearLayout;

    invoke-static {v3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/osp/app/signin/BottomSoftkeyLinearLayout;->a(Ljava/lang/Boolean;)V

    goto/16 :goto_0

    .line 2243
    :cond_6
    iget-object v0, p0, Lcom/osp/app/signin/TnCView;->aK:Lcom/osp/app/signin/BottomSoftkeyLinearLayout;

    invoke-static {v3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/osp/app/signin/BottomSoftkeyLinearLayout;->b(Ljava/lang/Boolean;)V

    goto/16 :goto_0

    .line 2247
    :cond_7
    invoke-static {}, Lcom/osp/device/b;->b()Lcom/osp/device/b;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/osp/device/b;->l(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_8

    .line 2249
    iget-object v0, p0, Lcom/osp/app/signin/TnCView;->aK:Lcom/osp/app/signin/BottomSoftkeyLinearLayout;

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/osp/app/signin/BottomSoftkeyLinearLayout;->a(Ljava/lang/Boolean;)V

    goto/16 :goto_0

    .line 2252
    :cond_8
    iget-object v0, p0, Lcom/osp/app/signin/TnCView;->aK:Lcom/osp/app/signin/BottomSoftkeyLinearLayout;

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/osp/app/signin/BottomSoftkeyLinearLayout;->b(Ljava/lang/Boolean;)V

    goto/16 :goto_0

    .line 2259
    :cond_9
    iget-object v0, p0, Lcom/osp/app/signin/TnCView;->x:Landroid/view/Menu;

    invoke-interface {v0, v4}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v0

    invoke-interface {v0, v2}, Landroid/view/MenuItem;->setEnabled(Z)Landroid/view/MenuItem;

    goto :goto_1

    :cond_a
    iget-boolean v0, p0, Lcom/osp/app/signin/TnCView;->D:Z

    if-eqz v0, :cond_b

    iget-boolean v0, p0, Lcom/osp/app/signin/TnCView;->E:Z

    if-eqz v0, :cond_b

    iget-object v0, p0, Lcom/osp/app/signin/TnCView;->x:Landroid/view/Menu;

    invoke-interface {v0, v4}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v0

    invoke-interface {v0, v3}, Landroid/view/MenuItem;->setEnabled(Z)Landroid/view/MenuItem;

    goto/16 :goto_1

    :cond_b
    iget-object v0, p0, Lcom/osp/app/signin/TnCView;->x:Landroid/view/Menu;

    invoke-interface {v0, v4}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v0

    invoke-interface {v0, v2}, Landroid/view/MenuItem;->setEnabled(Z)Landroid/view/MenuItem;

    goto/16 :goto_1
.end method

.method public final a(Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 2436
    new-instance v0, Landroid/content/Intent;

    const-string v1, "com.msc.action.samsungaccount.web_with_close_button"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 2437
    const-string v1, "ServerUrl"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 2440
    invoke-virtual {p0, v0}, Lcom/osp/app/signin/TnCView;->startActivity(Landroid/content/Intent;)V

    .line 2441
    return-void
.end method

.method protected final b()V
    .locals 0

    .prologue
    .line 5710
    return-void
.end method

.method public final c()V
    .locals 3

    .prologue
    .line 7675
    iget-object v0, p0, Lcom/osp/app/signin/TnCView;->bf:Landroid/app/AlertDialog;

    if-eqz v0, :cond_1

    .line 7677
    iget-object v0, p0, Lcom/osp/app/signin/TnCView;->bf:Landroid/app/AlertDialog;

    invoke-virtual {v0}, Landroid/app/AlertDialog;->isShowing()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 7679
    iget-object v0, p0, Lcom/osp/app/signin/TnCView;->bf:Landroid/app/AlertDialog;

    invoke-virtual {v0}, Landroid/app/AlertDialog;->dismiss()V

    .line 7681
    :cond_0
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/osp/app/signin/TnCView;->bf:Landroid/app/AlertDialog;

    .line 7684
    :cond_1
    iget-object v0, p0, Lcom/osp/app/signin/TnCView;->bf:Landroid/app/AlertDialog;

    if-nez v0, :cond_2

    .line 7686
    new-instance v0, Landroid/app/AlertDialog$Builder;

    invoke-direct {v0, p0}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    const v1, 0x7f090089

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    const v1, 0x7f0900a2

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setMessage(I)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    const v1, 0x7f090042

    new-instance v2, Lcom/osp/app/signin/nu;

    invoke-direct {v2, p0}, Lcom/osp/app/signin/nu;-><init>(Lcom/osp/app/signin/TnCView;)V

    invoke-virtual {v0, v1, v2}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->show()Landroid/app/AlertDialog;

    move-result-object v0

    iput-object v0, p0, Lcom/osp/app/signin/TnCView;->bf:Landroid/app/AlertDialog;

    .line 7704
    :cond_2
    return-void
.end method

.method protected onActivityResult(IILandroid/content/Intent;)V
    .locals 7

    .prologue
    const/16 v6, 0xe

    const/16 v5, 0xc

    const/4 v4, -0x1

    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 1388
    invoke-super {p0, p1, p2, p3}, Lcom/osp/app/util/BaseActivity;->onActivityResult(IILandroid/content/Intent;)V

    .line 1390
    const/16 v2, 0x10

    if-ne p2, v2, :cond_1

    .line 1392
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v0, "TNC"

    const-string v1, "TnCView - onActivityResult - requestCode :"

    const-string v2, "RESULT_EXPIRED_TOKEN"

    invoke-static {v0, v1, v2}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 1393
    const/16 v0, 0x10

    invoke-virtual {p0, v0}, Lcom/osp/app/signin/TnCView;->b(I)V

    .line 1394
    invoke-virtual {p0}, Lcom/osp/app/signin/TnCView;->finish()V

    .line 1606
    :cond_0
    :goto_0
    return-void

    .line 1398
    :cond_1
    const/16 v2, 0xf

    if-ne p2, v2, :cond_2

    .line 1400
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v0, "TNC"

    const-string v1, "FINISH_BY_HOME_BUTTON"

    invoke-static {v0, v1}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 1401
    const/16 v0, 0xf

    invoke-virtual {p0, v0}, Lcom/osp/app/signin/TnCView;->b(I)V

    .line 1402
    invoke-virtual {p0}, Lcom/osp/app/signin/TnCView;->finish()V

    goto :goto_0

    .line 1405
    :cond_2
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "TnCView - onActivityResult - requestCode :"

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;)V

    .line 1406
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "TnCView - onActivityResult - resultCode :"

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;)V

    .line 1408
    const/16 v2, 0xee

    if-ne p1, v2, :cond_6

    .line 1410
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v2, "TNC"

    const-string v3, "onActivityResult - REQUEST_CODE_AUTH_RESULT_FOR_EASYSIGNUP"

    invoke-static {v2, v3}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 1412
    invoke-static {}, Lcom/osp/app/signin/SamsungService;->a()Z

    move-result v2

    if-nez v2, :cond_4

    iget-boolean v2, p0, Lcom/osp/app/signin/TnCView;->aA:Z

    if-nez v2, :cond_4

    iget-boolean v2, p0, Lcom/osp/app/signin/TnCView;->aB:Z

    if-nez v2, :cond_4

    iget-boolean v2, p0, Lcom/osp/app/signin/TnCView;->aC:Z

    if-nez v2, :cond_4

    iget-boolean v2, p0, Lcom/osp/app/signin/TnCView;->aD:Z

    if-nez v2, :cond_4

    invoke-static {}, Lcom/osp/device/b;->b()Lcom/osp/device/b;

    move-result-object v2

    invoke-virtual {v2}, Lcom/osp/device/b;->a()Z

    move-result v2

    if-eqz v2, :cond_3

    iget-boolean v2, p0, Lcom/osp/app/signin/TnCView;->aF:Z

    if-nez v2, :cond_4

    .line 1416
    :cond_3
    iget-object v2, p0, Lcom/osp/app/signin/TnCView;->am:Lcom/osp/app/signin/SignUpinfo;

    if-eqz v2, :cond_6

    .line 1418
    iget-object v0, p0, Lcom/osp/app/signin/TnCView;->am:Lcom/osp/app/signin/SignUpinfo;

    invoke-virtual {v0}, Lcom/osp/app/signin/SignUpinfo;->e()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/osp/app/signin/TnCView;->i(Ljava/lang/String;)V

    goto/16 :goto_0

    .line 1421
    :cond_4
    iget-boolean v2, p0, Lcom/osp/app/signin/TnCView;->aA:Z

    if-nez v2, :cond_5

    iget-boolean v2, p0, Lcom/osp/app/signin/TnCView;->aB:Z

    if-nez v2, :cond_5

    iget-boolean v2, p0, Lcom/osp/app/signin/TnCView;->aC:Z

    if-nez v2, :cond_5

    iget-boolean v2, p0, Lcom/osp/app/signin/TnCView;->aD:Z

    if-eqz v2, :cond_6

    .line 1425
    :cond_5
    iget-object v0, p0, Lcom/osp/app/signin/TnCView;->a:Landroid/content/Intent;

    invoke-virtual {p0, v4, v0}, Lcom/osp/app/signin/TnCView;->a(ILandroid/content/Intent;)V

    .line 1426
    invoke-virtual {p0}, Lcom/osp/app/signin/TnCView;->finish()V

    goto/16 :goto_0

    .line 1432
    :cond_6
    const/16 v2, 0xd8

    if-eq p1, v2, :cond_7

    const/16 v2, 0xca

    if-ne p1, v2, :cond_10

    .line 1434
    :cond_7
    if-ne p2, v0, :cond_8

    .line 1436
    invoke-static {}, Lcom/osp/app/util/ap;->a()Lcom/osp/app/util/ap;

    const v0, 0x7f09004a

    invoke-virtual {p0, v0}, Lcom/osp/app/signin/TnCView;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-static {p0, v0, v1}, Lcom/osp/app/util/ap;->a(Landroid/content/Context;Ljava/lang/String;I)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    goto/16 :goto_0

    .line 1440
    :cond_8
    if-eqz p3, :cond_9

    .line 1442
    const-string v2, "key_is_name_verified"

    invoke-virtual {p3, v2, v1}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v1

    iput-boolean v1, p0, Lcom/osp/app/signin/TnCView;->al:Z

    .line 1443
    iget-boolean v1, p0, Lcom/osp/app/signin/TnCView;->al:Z

    if-eqz v1, :cond_9

    .line 1445
    const-string v1, "information_after_name_validation"

    invoke-virtual {p3, v1}, Landroid/content/Intent;->getBundleExtra(Ljava/lang/String;)Landroid/os/Bundle;

    move-result-object v1

    iput-object v1, p0, Lcom/osp/app/signin/TnCView;->aV:Landroid/os/Bundle;

    .line 1449
    :cond_9
    packed-switch p2, :pswitch_data_0

    .line 1524
    :cond_a
    :goto_1
    :pswitch_0
    iget-object v0, p0, Lcom/osp/app/signin/TnCView;->a:Landroid/content/Intent;

    invoke-virtual {p0, p2, v0}, Lcom/osp/app/signin/TnCView;->a(ILandroid/content/Intent;)V

    .line 1529
    invoke-virtual {p0}, Lcom/osp/app/signin/TnCView;->finish()V

    goto/16 :goto_0

    .line 1452
    :pswitch_1
    if-eqz p3, :cond_a

    .line 1454
    const/16 v1, 0xca

    if-ne p1, v1, :cond_e

    .line 1456
    iput-boolean v0, p0, Lcom/osp/app/signin/TnCView;->al:Z

    .line 1458
    const-string v1, "key_name_check_familyname"

    invoke-virtual {p3, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/osp/app/signin/TnCView;->aw:Ljava/lang/String;

    .line 1459
    const-string v1, "key_name_check_givenname"

    invoke-virtual {p3, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/osp/app/signin/TnCView;->ax:Ljava/lang/String;

    .line 1460
    const-string v1, "key_name_check_birthdate"

    invoke-virtual {p3, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/osp/app/signin/TnCView;->ao:Ljava/lang/String;

    .line 1463
    const-string v1, "key_name_check_result_key"

    invoke-virtual {p3, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    .line 1464
    const-string v1, "key_name_check_method"

    invoke-virtual {p3, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/osp/app/signin/TnCView;->ap:Ljava/lang/String;

    .line 1465
    const-string v1, "key_name_check_ci"

    invoke-virtual {p3, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/osp/app/signin/TnCView;->aq:Ljava/lang/String;

    .line 1466
    const-string v1, "key_name_check_di"

    invoke-virtual {p3, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/osp/app/signin/TnCView;->ar:Ljava/lang/String;

    .line 1467
    const-string v1, "key_name_check_datetime"

    invoke-virtual {p3, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/osp/app/signin/TnCView;->as:Ljava/lang/String;

    .line 1468
    const-string v1, "key_name_check_foreigner"

    invoke-virtual {p3, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/osp/app/signin/TnCView;->ay:Ljava/lang/String;

    .line 1470
    new-instance v1, Landroid/os/Bundle;

    invoke-direct {v1}, Landroid/os/Bundle;-><init>()V

    iput-object v1, p0, Lcom/osp/app/signin/TnCView;->aV:Landroid/os/Bundle;

    .line 1471
    iget-object v1, p0, Lcom/osp/app/signin/TnCView;->aV:Landroid/os/Bundle;

    const-string v2, "key_name_check_familyname"

    iget-object v3, p0, Lcom/osp/app/signin/TnCView;->aw:Ljava/lang/String;

    invoke-virtual {v1, v2, v3}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 1472
    iget-object v1, p0, Lcom/osp/app/signin/TnCView;->aV:Landroid/os/Bundle;

    const-string v2, "key_name_check_givenname"

    iget-object v3, p0, Lcom/osp/app/signin/TnCView;->ax:Ljava/lang/String;

    invoke-virtual {v1, v2, v3}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 1473
    iget-object v1, p0, Lcom/osp/app/signin/TnCView;->aV:Landroid/os/Bundle;

    const-string v2, "key_name_check_birthdate"

    iget-object v3, p0, Lcom/osp/app/signin/TnCView;->ao:Ljava/lang/String;

    invoke-virtual {v1, v2, v3}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 1474
    iget-object v1, p0, Lcom/osp/app/signin/TnCView;->aV:Landroid/os/Bundle;

    const-string v2, "key_name_check_method"

    iget-object v3, p0, Lcom/osp/app/signin/TnCView;->ap:Ljava/lang/String;

    invoke-virtual {v1, v2, v3}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 1475
    iget-object v1, p0, Lcom/osp/app/signin/TnCView;->aV:Landroid/os/Bundle;

    const-string v2, "key_name_check_ci"

    iget-object v3, p0, Lcom/osp/app/signin/TnCView;->aq:Ljava/lang/String;

    invoke-virtual {v1, v2, v3}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 1476
    iget-object v1, p0, Lcom/osp/app/signin/TnCView;->aV:Landroid/os/Bundle;

    const-string v2, "key_name_check_di"

    iget-object v3, p0, Lcom/osp/app/signin/TnCView;->ar:Ljava/lang/String;

    invoke-virtual {v1, v2, v3}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 1477
    iget-object v1, p0, Lcom/osp/app/signin/TnCView;->aV:Landroid/os/Bundle;

    const-string v2, "key_name_check_datetime"

    iget-object v3, p0, Lcom/osp/app/signin/TnCView;->as:Ljava/lang/String;

    invoke-virtual {v1, v2, v3}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 1478
    iget-object v1, p0, Lcom/osp/app/signin/TnCView;->aV:Landroid/os/Bundle;

    const-string v2, "key_name_check_foreigner"

    iget-object v3, p0, Lcom/osp/app/signin/TnCView;->ay:Ljava/lang/String;

    invoke-virtual {v1, v2, v3}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 1480
    new-instance v1, Landroid/content/Intent;

    invoke-direct {v1}, Landroid/content/Intent;-><init>()V

    const-class v2, Lcom/osp/app/signin/SignUpView;

    invoke-virtual {v1, p0, v2}, Landroid/content/Intent;->setClass(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;

    const-string v2, "client_id"

    iget-object v3, p0, Lcom/osp/app/signin/TnCView;->b:Ljava/lang/String;

    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    const-string v2, "client_secret"

    iget-object v3, p0, Lcom/osp/app/signin/TnCView;->c:Ljava/lang/String;

    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    const-string v2, "account_mode"

    iget-object v3, p0, Lcom/osp/app/signin/TnCView;->d:Ljava/lang/String;

    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    const-string v2, "MODE"

    iget-object v3, p0, Lcom/osp/app/signin/TnCView;->f:Ljava/lang/String;

    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    const-string v2, "service_name"

    iget-object v3, p0, Lcom/osp/app/signin/TnCView;->y:Ljava/lang/String;

    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    const-string v2, "OSP_VER"

    iget-object v3, p0, Lcom/osp/app/signin/TnCView;->e:Ljava/lang/String;

    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    const-string v2, "serviceApp_type"

    iget v3, p0, Lcom/osp/app/signin/TnCView;->A:I

    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    const-string v2, "mypackage"

    iget-object v3, p0, Lcom/osp/app/signin/TnCView;->z:Ljava/lang/String;

    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    const-string v2, "key_request_id"

    iget-wide v4, p0, Lcom/osp/app/signin/TnCView;->aW:J

    invoke-virtual {v1, v2, v4, v5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;J)Landroid/content/Intent;

    const-string v2, "key_is_name_verified"

    iget-boolean v3, p0, Lcom/osp/app/signin/TnCView;->al:Z

    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    const-string v2, "country_code_mcc"

    iget-object v3, p0, Lcom/osp/app/signin/TnCView;->Y:Ljava/lang/String;

    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    iget-boolean v2, p0, Lcom/osp/app/signin/TnCView;->al:Z

    if-eqz v2, :cond_b

    iget-object v2, p0, Lcom/osp/app/signin/TnCView;->aV:Landroid/os/Bundle;

    if-eqz v2, :cond_b

    const-string v2, "information_after_name_validation"

    iget-object v3, p0, Lcom/osp/app/signin/TnCView;->aV:Landroid/os/Bundle;

    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Bundle;)Landroid/content/Intent;

    :cond_b
    iget-boolean v2, p0, Lcom/osp/app/signin/TnCView;->ag:Z

    if-eqz v2, :cond_c

    const-string v2, "new_add_account_mode"

    invoke-virtual {v1, v2, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    :cond_c
    const-string v2, "is_cancelable_just_one_activity"

    invoke-virtual {v1, v2, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "TnCView - showSignup - m_client_id : "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v3, p0, Lcom/osp/app/signin/TnCView;->b:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;)V

    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "TnCView - showSignup - m_client_secret : "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v3, p0, Lcom/osp/app/signin/TnCView;->c:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;)V

    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "TnCView - showSignup - m_account_mode : "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v3, p0, Lcom/osp/app/signin/TnCView;->d:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;)V

    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "TnCView - showSignup - m_setting_mode : "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v3, p0, Lcom/osp/app/signin/TnCView;->f:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;)V

    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "TnCView - showSignup - m_service_name : "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v3, p0, Lcom/osp/app/signin/TnCView;->y:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;)V

    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "TnCView - showSignup - m_OspVersion : "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v3, p0, Lcom/osp/app/signin/TnCView;->e:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;)V

    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "TnCView - showSignup - m_ServiceAppType : "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v3, p0, Lcom/osp/app/signin/TnCView;->A:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;)V

    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "TnCView - showSignup - m_Sourcepackage : "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v3, p0, Lcom/osp/app/signin/TnCView;->z:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;)V

    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "TnCView - showSignup - m_FieldInfo : "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v3, p0, Lcom/osp/app/signin/TnCView;->T:Lcom/osp/app/signin/kd;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;)V

    const-string v2, "tncAccepted"

    invoke-virtual {v1, v2, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    const-string v2, "450"

    iget-object v3, p0, Lcom/osp/app/signin/TnCView;->Y:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_d

    const-string v2, "privacyAccepted"

    invoke-virtual {v1, v2, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    :cond_d
    const/16 v0, 0xd8

    invoke-virtual {p0, v1, v0}, Lcom/osp/app/signin/TnCView;->startActivityForResult(Landroid/content/Intent;I)V

    goto/16 :goto_0

    .line 1485
    :cond_e
    const-string v0, "authcode"

    invoke-virtual {p3, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 1486
    iget-object v1, p0, Lcom/osp/app/signin/TnCView;->a:Landroid/content/Intent;

    const-string v2, "authcode"

    invoke-virtual {v1, v2, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 1488
    iget-boolean v0, p0, Lcom/osp/app/signin/TnCView;->U:Z

    if-eqz v0, :cond_a

    .line 1490
    const-string v0, "signUpInfo"

    invoke-virtual {p3, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 1491
    iget-object v1, p0, Lcom/osp/app/signin/TnCView;->a:Landroid/content/Intent;

    const-string v2, "signUpInfo"

    invoke-virtual {v1, v2, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    goto/16 :goto_1

    .line 1496
    :pswitch_2
    iget-object v1, p0, Lcom/osp/app/signin/TnCView;->a:Landroid/content/Intent;

    const-string v2, "email_id"

    if-eqz p3, :cond_f

    const-string v0, "email_id"

    invoke-virtual {p3, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    :goto_2
    invoke-virtual {v1, v2, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    goto/16 :goto_1

    :cond_f
    const/4 v0, 0x0

    goto :goto_2

    .line 1515
    :pswitch_3
    invoke-virtual {p0}, Lcom/osp/app/signin/TnCView;->getApplication()Landroid/app/Application;

    move-result-object v0

    check-cast v0, Lcom/osp/app/signin/SamsungService;

    invoke-virtual {v0, p0}, Lcom/osp/app/signin/SamsungService;->a(Landroid/app/Activity;)V

    goto/16 :goto_0

    .line 1519
    :pswitch_4
    iput-object p3, p0, Lcom/osp/app/signin/TnCView;->a:Landroid/content/Intent;

    goto/16 :goto_1

    .line 1523
    :pswitch_5
    iput-object p3, p0, Lcom/osp/app/signin/TnCView;->a:Landroid/content/Intent;

    goto/16 :goto_1

    .line 1530
    :cond_10
    const/16 v2, 0xde

    if-ne p1, v2, :cond_11

    .line 1532
    if-ne p2, v4, :cond_0

    .line 1534
    iget-boolean v0, p0, Lcom/osp/app/signin/TnCView;->ae:Z

    invoke-direct {p0, v0}, Lcom/osp/app/signin/TnCView;->a(Z)V

    goto/16 :goto_0

    .line 1536
    :cond_11
    const/16 v2, 0xd6

    if-ne p1, v2, :cond_12

    .line 1538
    const/16 v0, 0xa

    invoke-virtual {p0, v0}, Lcom/osp/app/signin/TnCView;->b(I)V

    .line 1539
    invoke-virtual {p0}, Lcom/osp/app/signin/TnCView;->finish()V

    goto/16 :goto_0

    .line 1540
    :cond_12
    const/16 v2, 0xc9

    if-ne p1, v2, :cond_15

    .line 1542
    iput-object p3, p0, Lcom/osp/app/signin/TnCView;->a:Landroid/content/Intent;

    .line 1543
    if-ne p2, v5, :cond_13

    :goto_3
    invoke-direct {p0, v0}, Lcom/osp/app/signin/TnCView;->b(Z)V

    .line 1544
    if-ne p2, v5, :cond_14

    .line 1546
    iget-object v0, p0, Lcom/osp/app/signin/TnCView;->a:Landroid/content/Intent;

    invoke-virtual {p0, v4, v0}, Lcom/osp/app/signin/TnCView;->a(ILandroid/content/Intent;)V

    .line 1551
    :goto_4
    invoke-virtual {p0}, Lcom/osp/app/signin/TnCView;->finish()V

    goto/16 :goto_0

    :cond_13
    move v0, v1

    .line 1543
    goto :goto_3

    .line 1549
    :cond_14
    const/16 v0, 0xd

    iget-object v1, p0, Lcom/osp/app/signin/TnCView;->a:Landroid/content/Intent;

    invoke-virtual {p0, v0, v1}, Lcom/osp/app/signin/TnCView;->a(ILandroid/content/Intent;)V

    goto :goto_4

    .line 1553
    :cond_15
    const/16 v0, 0xd5

    if-ne p1, v0, :cond_18

    .line 1555
    if-ne p2, v4, :cond_16

    .line 1557
    invoke-direct {p0}, Lcom/osp/app/signin/TnCView;->d()V

    goto/16 :goto_0

    .line 1560
    :cond_16
    invoke-virtual {p0}, Lcom/osp/app/signin/TnCView;->getIntent()Landroid/content/Intent;

    move-result-object v0

    .line 1561
    if-eqz v0, :cond_17

    const-string v2, "is_cancelable_just_one_activity"

    invoke-virtual {v0, v2, v1}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v0

    if-eqz v0, :cond_17

    .line 1563
    iget-object v0, p0, Lcom/osp/app/signin/TnCView;->a:Landroid/content/Intent;

    invoke-virtual {p0, v6, v0}, Lcom/osp/app/signin/TnCView;->a(ILandroid/content/Intent;)V

    .line 1565
    :cond_17
    invoke-virtual {p0}, Lcom/osp/app/signin/TnCView;->finish()V

    goto/16 :goto_0

    .line 1567
    :cond_18
    const/16 v0, 0xd0

    if-ne p1, v0, :cond_1a

    .line 1569
    packed-switch p2, :pswitch_data_1

    .line 1587
    invoke-virtual {p0}, Lcom/osp/app/signin/TnCView;->getIntent()Landroid/content/Intent;

    move-result-object v0

    .line 1588
    if-eqz v0, :cond_19

    const-string v2, "is_cancelable_just_one_activity"

    invoke-virtual {v0, v2, v1}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v0

    if-eqz v0, :cond_19

    .line 1590
    iget-object v0, p0, Lcom/osp/app/signin/TnCView;->a:Landroid/content/Intent;

    invoke-virtual {p0, v6, v0}, Lcom/osp/app/signin/TnCView;->a(ILandroid/content/Intent;)V

    .line 1592
    :cond_19
    invoke-virtual {p0}, Lcom/osp/app/signin/TnCView;->finish()V

    goto/16 :goto_0

    .line 1575
    :pswitch_6
    iput-boolean v1, p0, Lcom/osp/app/signin/TnCView;->D:Z

    .line 1576
    iput-boolean v1, p0, Lcom/osp/app/signin/TnCView;->E:Z

    .line 1577
    iput-boolean v1, p0, Lcom/osp/app/signin/TnCView;->F:Z

    .line 1578
    iput-boolean v1, p0, Lcom/osp/app/signin/TnCView;->G:Z

    .line 1579
    iput-boolean v1, p0, Lcom/osp/app/signin/TnCView;->H:Z

    .line 1581
    invoke-static {p0}, Lcom/msc/c/e;->f(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/osp/app/signin/TnCView;->Y:Ljava/lang/String;

    .line 1583
    invoke-direct {p0}, Lcom/osp/app/signin/TnCView;->e()V

    goto/16 :goto_0

    .line 1597
    :cond_1a
    const/16 v0, 0xdc

    if-ne p1, v0, :cond_1b

    .line 1599
    invoke-virtual {p0, p2, p3}, Lcom/osp/app/signin/TnCView;->a(ILandroid/content/Intent;)V

    .line 1600
    invoke-virtual {p0}, Lcom/osp/app/signin/TnCView;->finish()V

    goto/16 :goto_0

    .line 1601
    :cond_1b
    const/16 v0, 0xd7

    if-ne p1, v0, :cond_0

    .line 1603
    invoke-virtual {p0, v4, p3}, Lcom/osp/app/signin/TnCView;->a(ILandroid/content/Intent;)V

    .line 1604
    invoke-virtual {p0}, Lcom/osp/app/signin/TnCView;->finish()V

    goto/16 :goto_0

    .line 1449
    :pswitch_data_0
    .packed-switch -0x1
        :pswitch_1
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_2
        :pswitch_0
        :pswitch_0
        :pswitch_3
        :pswitch_0
        :pswitch_0
        :pswitch_4
        :pswitch_5
    .end packed-switch

    .line 1569
    :pswitch_data_1
    .packed-switch -0x1
        :pswitch_6
    .end packed-switch
.end method

.method public onBackPressed()V
    .locals 6

    .prologue
    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    const/4 v2, 0x0

    .line 1612
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v0, "TnCView - onBackPressed"

    invoke-static {v0}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;)V

    .line 1614
    iget-boolean v0, p0, Lcom/osp/app/signin/TnCView;->N:Z

    if-eqz v0, :cond_4

    .line 1616
    iget-object v0, p0, Lcom/osp/app/signin/TnCView;->K:Landroid/widget/ListView;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/osp/app/signin/TnCView;->K:Landroid/widget/ListView;

    iget-object v1, p0, Lcom/osp/app/signin/TnCView;->L:Lcom/osp/app/signin/op;

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setAdapter(Landroid/widget/ListAdapter;)V

    iget-object v0, p0, Lcom/osp/app/signin/TnCView;->K:Landroid/widget/ListView;

    new-instance v1, Lcom/osp/app/signin/oi;

    invoke-direct {v1, p0, v2}, Lcom/osp/app/signin/oi;-><init>(Lcom/osp/app/signin/TnCView;B)V

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V

    iget-object v0, p0, Lcom/osp/app/signin/TnCView;->K:Landroid/widget/ListView;

    invoke-virtual {v0}, Landroid/widget/ListView;->requestFocus()Z

    iget-object v0, p0, Lcom/osp/app/signin/TnCView;->K:Landroid/widget/ListView;

    invoke-virtual {v0}, Landroid/widget/ListView;->getCount()I

    move-result v0

    if-le v0, v4, :cond_0

    iget-object v0, p0, Lcom/osp/app/signin/TnCView;->K:Landroid/widget/ListView;

    invoke-virtual {v0, v4}, Landroid/widget/ListView;->setSelection(I)V

    :cond_0
    iput-boolean v2, p0, Lcom/osp/app/signin/TnCView;->N:Z

    invoke-static {}, Lcom/osp/app/signin/SamsungService;->a()Z

    const v0, 0x7f09004f

    invoke-static {p0, v0}, Lcom/msc/sa/c/d;->a(Landroid/app/Activity;I)V

    iget v0, p0, Lcom/osp/app/signin/TnCView;->ad:I

    if-eqz v0, :cond_1

    iget v0, p0, Lcom/osp/app/signin/TnCView;->ad:I

    if-ne v0, v5, :cond_2

    :cond_1
    invoke-static {}, Lcom/osp/app/signin/SamsungService;->a()Z

    move-result v0

    if-nez v0, :cond_3

    invoke-static {}, Lcom/osp/device/b;->b()Lcom/osp/device/b;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/osp/device/b;->i(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_3

    const-string v0, "agree_cancel"

    invoke-virtual {p0, v0}, Lcom/osp/app/signin/TnCView;->e(Ljava/lang/String;)V

    invoke-virtual {p0}, Lcom/osp/app/signin/TnCView;->l()V

    .line 1659
    :cond_2
    :goto_0
    return-void

    .line 1616
    :cond_3
    const v0, 0x7f0c0065

    invoke-virtual {p0, v0}, Lcom/osp/app/signin/TnCView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    if-eqz v0, :cond_2

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    goto :goto_0

    .line 1627
    :cond_4
    iget-object v0, p0, Lcom/osp/app/signin/TnCView;->aZ:Lcom/osp/app/signin/ok;

    if-eqz v0, :cond_5

    .line 1629
    iget-object v0, p0, Lcom/osp/app/signin/TnCView;->aZ:Lcom/osp/app/signin/ok;

    invoke-virtual {v0}, Lcom/osp/app/signin/ok;->getStatus()Landroid/os/AsyncTask$Status;

    move-result-object v0

    sget-object v1, Landroid/os/AsyncTask$Status;->FINISHED:Landroid/os/AsyncTask$Status;

    if-eq v0, v1, :cond_5

    .line 1631
    iget-object v0, p0, Lcom/osp/app/signin/TnCView;->aZ:Lcom/osp/app/signin/ok;

    invoke-virtual {v0}, Lcom/osp/app/signin/ok;->d()V

    .line 1636
    :cond_5
    invoke-virtual {p0}, Lcom/osp/app/signin/TnCView;->getIntent()Landroid/content/Intent;

    move-result-object v0

    .line 1637
    if-eqz v0, :cond_7

    const-string v1, "is_cancelable_just_one_activity"

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v0

    if-eqz v0, :cond_7

    .line 1639
    iget-object v0, p0, Lcom/osp/app/signin/TnCView;->a:Landroid/content/Intent;

    const-string v1, "key_is_name_verified"

    iget-boolean v2, p0, Lcom/osp/app/signin/TnCView;->al:Z

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 1640
    iget-boolean v0, p0, Lcom/osp/app/signin/TnCView;->al:Z

    if-eqz v0, :cond_6

    iget-object v0, p0, Lcom/osp/app/signin/TnCView;->aV:Landroid/os/Bundle;

    if-eqz v0, :cond_6

    .line 1642
    iget-object v0, p0, Lcom/osp/app/signin/TnCView;->a:Landroid/content/Intent;

    const-string v1, "information_after_name_validation"

    iget-object v2, p0, Lcom/osp/app/signin/TnCView;->aV:Landroid/os/Bundle;

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Bundle;)Landroid/content/Intent;

    .line 1644
    :cond_6
    const/16 v0, 0xe

    iget-object v1, p0, Lcom/osp/app/signin/TnCView;->a:Landroid/content/Intent;

    invoke-virtual {p0, v0, v1}, Lcom/osp/app/signin/TnCView;->a(ILandroid/content/Intent;)V

    .line 1646
    :cond_7
    iput-object v3, p0, Lcom/osp/app/signin/TnCView;->aa:Ljava/lang/String;

    .line 1647
    iput-object v3, p0, Lcom/osp/app/signin/TnCView;->ab:Ljava/lang/String;

    .line 1648
    iput-object v3, p0, Lcom/osp/app/signin/TnCView;->Z:Ljava/lang/String;

    .line 1649
    iput-object v3, p0, Lcom/osp/app/signin/TnCView;->Y:Ljava/lang/String;

    .line 1654
    new-instance v0, Lcom/osp/app/bigdatalog/c;

    invoke-virtual {p0}, Lcom/osp/app/signin/TnCView;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    const/4 v2, 0x3

    iget-object v3, p0, Lcom/osp/app/signin/TnCView;->a:Landroid/content/Intent;

    invoke-direct {v0, v1, v5, v2, v3}, Lcom/osp/app/bigdatalog/c;-><init>(Landroid/content/Context;IILandroid/content/Intent;)V

    .line 1655
    invoke-static {}, Lcom/osp/app/bigdatalog/e;->a()Lcom/osp/app/bigdatalog/e;

    move-result-object v1

    invoke-virtual {p0}, Lcom/osp/app/signin/TnCView;->getApplicationContext()Landroid/content/Context;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/osp/app/bigdatalog/e;->a(Landroid/content/Context;)V

    .line 1656
    invoke-static {}, Lcom/osp/app/bigdatalog/e;->a()Lcom/osp/app/bigdatalog/e;

    move-result-object v1

    invoke-virtual {p0}, Lcom/osp/app/signin/TnCView;->getApplicationContext()Landroid/content/Context;

    move-result-object v2

    invoke-virtual {v1, v2, v0}, Lcom/osp/app/bigdatalog/e;->a(Landroid/content/Context;Lcom/osp/app/bigdatalog/c;)V

    .line 1658
    invoke-super {p0}, Lcom/osp/app/util/BaseActivity;->onBackPressed()V

    goto :goto_0
.end method

.method public onConfigurationChanged(Landroid/content/res/Configuration;)V
    .locals 3

    .prologue
    const v2, 0x7f09004f

    .line 1072
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v0, "TNC"

    const-string v1, "onConfigurationChanged()"

    invoke-static {v0, v1}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 1074
    iget v0, p1, Landroid/content/res/Configuration;->orientation:I

    invoke-direct {p0, v0}, Lcom/osp/app/signin/TnCView;->a(I)V

    .line 1077
    iget-object v0, p0, Lcom/osp/app/signin/TnCView;->L:Lcom/osp/app/signin/op;

    if-eqz v0, :cond_0

    .line 1079
    iget-object v0, p0, Lcom/osp/app/signin/TnCView;->L:Lcom/osp/app/signin/op;

    invoke-virtual {v0}, Lcom/osp/app/signin/op;->notifyDataSetChanged()V

    .line 1082
    :cond_0
    invoke-static {}, Lcom/osp/app/signin/SamsungService;->a()Z

    move-result v0

    if-nez v0, :cond_3

    .line 1084
    iget-boolean v0, p0, Lcom/osp/app/signin/TnCView;->N:Z

    if-eqz v0, :cond_2

    .line 1086
    const v0, 0x7f0900bd

    invoke-static {p0, v0}, Lcom/msc/sa/c/d;->a(Landroid/app/Activity;I)V

    .line 1102
    :goto_0
    :try_start_0
    invoke-virtual {p0}, Lcom/osp/app/signin/TnCView;->isFinishing()Z

    move-result v0

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/osp/app/signin/TnCView;->J:Landroid/app/AlertDialog;

    iget-object v1, p0, Lcom/osp/app/signin/TnCView;->r:Lcom/msc/sa/c/f;

    invoke-static {v0, v1}, Lcom/msc/sa/c/d;->a(Landroid/app/AlertDialog;Landroid/app/DialogFragment;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 1104
    :cond_1
    :goto_1
    invoke-super {p0, p1}, Lcom/osp/app/util/BaseActivity;->onConfigurationChanged(Landroid/content/res/Configuration;)V

    .line 1105
    return-void

    .line 1089
    :cond_2
    invoke-static {p0, v2}, Lcom/msc/sa/c/d;->a(Landroid/app/Activity;I)V

    goto :goto_0

    .line 1094
    :cond_3
    invoke-static {p0, v2}, Lcom/msc/sa/c/d;->a(Landroid/app/Activity;I)V

    goto :goto_0

    .line 1102
    :catch_0
    move-exception v0

    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_1
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 6

    .prologue
    const/4 v5, 0x1

    const/4 v2, 0x0

    const v4, 0x7f0900cd

    const v3, 0x7f0900cc

    .line 656
    invoke-virtual {p0}, Lcom/osp/app/signin/TnCView;->getIntent()Landroid/content/Intent;

    move-result-object v0

    iput-object v0, p0, Lcom/osp/app/signin/TnCView;->a:Landroid/content/Intent;

    .line 657
    iget-object v0, p0, Lcom/osp/app/signin/TnCView;->a:Landroid/content/Intent;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/osp/app/signin/TnCView;->a:Landroid/content/Intent;

    invoke-virtual {v0}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/osp/app/signin/TnCView;->a:Landroid/content/Intent;

    invoke-virtual {v0}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    const-string v1, "com.msc.action.samsungaccount.tncs"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 659
    invoke-static {v2}, Lcom/osp/app/signin/SamsungService;->a(Z)V

    .line 662
    :cond_0
    invoke-super {p0, p1}, Lcom/osp/app/util/BaseActivity;->onCreate(Landroid/os/Bundle;)V

    .line 664
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v0, "TnCView - onCreate"

    invoke-static {v0}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;)V

    .line 666
    invoke-static {p0}, Lcom/osp/app/util/ad;->d(Landroid/content/Context;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 668
    iget-object v0, p0, Lcom/osp/app/signin/TnCView;->a:Landroid/content/Intent;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/osp/app/signin/TnCView;->a:Landroid/content/Intent;

    const-string v1, "is_signup_flow"

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 670
    sget-object v0, Lcom/osp/app/loggingservice/c;->f:Lcom/osp/app/loggingservice/c;

    invoke-static {v0}, Lcom/osp/app/loggingservice/LoggingService;->a(Lcom/osp/app/loggingservice/c;)V

    .line 674
    :cond_1
    invoke-virtual {p0}, Lcom/osp/app/signin/TnCView;->getCallingActivity()Landroid/content/ComponentName;

    move-result-object v0

    if-eqz v0, :cond_2

    .line 676
    invoke-virtual {p0}, Lcom/osp/app/signin/TnCView;->getCallingActivity()Landroid/content/ComponentName;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/ComponentName;->getShortClassName()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/osp/app/signin/TnCView;->aJ:Ljava/lang/String;

    .line 681
    :cond_2
    iget-object v0, p0, Lcom/osp/app/signin/TnCView;->a:Landroid/content/Intent;

    invoke-virtual {v0}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_4

    iget-object v0, p0, Lcom/osp/app/signin/TnCView;->a:Landroid/content/Intent;

    invoke-virtual {v0}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    const-string v1, "com.msc.action.samsungaccount.Update_NewTerms_external"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 683
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v0, "external value check START"

    invoke-static {v0}, Lcom/osp/app/util/aq;->b(Ljava/lang/String;)V

    .line 685
    const/4 v0, 0x5

    new-array v0, v0, [Ljava/lang/String;

    const-string v1, "client_id"

    aput-object v1, v0, v2

    const-string v1, "client_secret"

    aput-object v1, v0, v5

    const/4 v1, 0x2

    const-string v2, "key_tnc_update_mode"

    aput-object v2, v0, v1

    const/4 v1, 0x3

    const-string v2, "check_list"

    aput-object v2, v0, v1

    const/4 v1, 0x4

    const-string v2, "key_no_notification"

    aput-object v2, v0, v1

    .line 688
    invoke-virtual {p0, v0}, Lcom/osp/app/signin/TnCView;->a([Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_4

    .line 720
    :cond_3
    :goto_0
    return-void

    .line 695
    :cond_4
    const v0, 0x7f030059

    invoke-static {p0}, Lcom/osp/app/util/r;->q(Landroid/content/Context;)Z

    move-result v1

    invoke-virtual {p0, v0, v1}, Lcom/osp/app/signin/TnCView;->a(IZ)V

    .line 696
    const v0, 0x7f09004f

    invoke-static {p0, v0}, Lcom/msc/sa/c/d;->a(Landroid/app/Activity;I)V

    .line 700
    iget-object v0, p0, Lcom/osp/app/signin/TnCView;->az:Landroid/widget/TextView;

    if-nez v0, :cond_5

    const v0, 0x7f0c0182

    invoke-virtual {p0, v0}, Lcom/osp/app/signin/TnCView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/osp/app/signin/TnCView;->az:Landroid/widget/TextView;

    invoke-static {p0}, Lcom/osp/app/util/r;->k(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_5

    iget-object v0, p0, Lcom/osp/app/signin/TnCView;->az:Landroid/widget/TextView;

    invoke-virtual {v0}, Landroid/widget/TextView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    const/4 v1, -0x2

    iput v1, v0, Landroid/view/ViewGroup$LayoutParams;->height:I

    iget-object v1, p0, Lcom/osp/app/signin/TnCView;->az:Landroid/widget/TextView;

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    :cond_5
    const v0, 0x7f0c0065

    invoke-virtual {p0, v0}, Lcom/osp/app/signin/TnCView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/osp/app/signin/BottomSoftkeyLinearLayout;

    iput-object v0, p0, Lcom/osp/app/signin/TnCView;->aK:Lcom/osp/app/signin/BottomSoftkeyLinearLayout;

    iget-object v0, p0, Lcom/osp/app/signin/TnCView;->aK:Lcom/osp/app/signin/BottomSoftkeyLinearLayout;

    if-eqz v0, :cond_6

    iget-object v0, p0, Lcom/osp/app/signin/TnCView;->aK:Lcom/osp/app/signin/BottomSoftkeyLinearLayout;

    invoke-virtual {p0}, Lcom/osp/app/signin/TnCView;->h()Z

    move-result v1

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-virtual {v0, p0, v1}, Lcom/osp/app/signin/BottomSoftkeyLinearLayout;->a(Landroid/content/Context;Ljava/lang/Boolean;)V

    invoke-static {}, Lcom/osp/device/b;->b()Lcom/osp/device/b;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/osp/device/b;->l(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_9

    iget-object v0, p0, Lcom/osp/app/signin/TnCView;->aK:Lcom/osp/app/signin/BottomSoftkeyLinearLayout;

    invoke-virtual {p0, v4}, Lcom/osp/app/signin/TnCView;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/osp/app/signin/BottomSoftkeyLinearLayout;->b(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/osp/app/signin/TnCView;->aK:Lcom/osp/app/signin/BottomSoftkeyLinearLayout;

    invoke-virtual {p0, v4}, Lcom/osp/app/signin/TnCView;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/osp/app/signin/BottomSoftkeyLinearLayout;->d(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/osp/app/signin/TnCView;->aK:Lcom/osp/app/signin/BottomSoftkeyLinearLayout;

    invoke-virtual {p0, v3}, Lcom/osp/app/signin/TnCView;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/osp/app/signin/BottomSoftkeyLinearLayout;->c(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/osp/app/signin/TnCView;->aK:Lcom/osp/app/signin/BottomSoftkeyLinearLayout;

    invoke-virtual {p0, v3}, Lcom/osp/app/signin/TnCView;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/osp/app/signin/BottomSoftkeyLinearLayout;->e(Ljava/lang/String;)V

    :cond_6
    :goto_1
    invoke-static {}, Lcom/osp/app/signin/SamsungService;->a()Z

    move-result v0

    if-nez v0, :cond_7

    invoke-static {}, Lcom/osp/device/b;->b()Lcom/osp/device/b;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/osp/device/b;->i(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_7

    iget-object v0, p0, Lcom/osp/app/signin/TnCView;->aK:Lcom/osp/app/signin/BottomSoftkeyLinearLayout;

    if-eqz v0, :cond_7

    iget-object v0, p0, Lcom/osp/app/signin/TnCView;->aK:Lcom/osp/app/signin/BottomSoftkeyLinearLayout;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Lcom/osp/app/signin/BottomSoftkeyLinearLayout;->setVisibility(I)V

    .line 708
    :cond_7
    invoke-virtual {p0}, Lcom/osp/app/signin/TnCView;->getApplication()Landroid/app/Application;

    move-result-object v0

    check-cast v0, Lcom/osp/app/signin/SamsungService;

    invoke-virtual {v0, p0}, Lcom/osp/app/signin/SamsungService;->a(Landroid/app/Activity;)V

    .line 715
    invoke-static {p0}, Lcom/osp/app/util/ad;->a(Landroid/content/Context;)Z

    move-result v0

    if-nez v0, :cond_8

    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v1, "checkNetwork"

    invoke-static {v1}, Lcom/osp/app/util/aq;->b(Ljava/lang/String;)V

    invoke-static {}, Lcom/osp/app/util/ap;->a()Lcom/osp/app/util/ap;

    const v1, 0x7f09003e

    invoke-virtual {p0, v1}, Lcom/osp/app/signin/TnCView;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-static {p0, v1, v5}, Lcom/osp/app/util/ap;->a(Landroid/content/Context;Ljava/lang/String;I)Landroid/widget/Toast;

    move-result-object v1

    invoke-virtual {v1}, Landroid/widget/Toast;->show()V

    const/16 v1, 0xd5

    invoke-virtual {p0, v1}, Lcom/osp/app/signin/TnCView;->c(I)V

    :cond_8
    if-eqz v0, :cond_3

    .line 717
    invoke-direct {p0}, Lcom/osp/app/signin/TnCView;->d()V

    goto/16 :goto_0

    .line 700
    :cond_9
    iget-object v0, p0, Lcom/osp/app/signin/TnCView;->aK:Lcom/osp/app/signin/BottomSoftkeyLinearLayout;

    invoke-virtual {p0, v3}, Lcom/osp/app/signin/TnCView;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/osp/app/signin/BottomSoftkeyLinearLayout;->b(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/osp/app/signin/TnCView;->aK:Lcom/osp/app/signin/BottomSoftkeyLinearLayout;

    invoke-virtual {p0, v3}, Lcom/osp/app/signin/TnCView;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/osp/app/signin/BottomSoftkeyLinearLayout;->d(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/osp/app/signin/TnCView;->aK:Lcom/osp/app/signin/BottomSoftkeyLinearLayout;

    invoke-virtual {p0, v4}, Lcom/osp/app/signin/TnCView;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/osp/app/signin/BottomSoftkeyLinearLayout;->c(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/osp/app/signin/TnCView;->aK:Lcom/osp/app/signin/BottomSoftkeyLinearLayout;

    invoke-virtual {p0, v4}, Lcom/osp/app/signin/TnCView;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/osp/app/signin/BottomSoftkeyLinearLayout;->e(Ljava/lang/String;)V

    goto :goto_1
.end method

.method public onCreateDialog(I)Landroid/app/Dialog;
    .locals 6

    .prologue
    const v5, 0x7f0901b8

    const v4, 0x7f090064

    const v3, 0x7f09005b

    const/4 v2, 0x0

    .line 754
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "TncView::onCreateDialog id : "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;)V

    .line 756
    packed-switch p1, :pswitch_data_0

    .line 846
    const/4 v0, 0x0

    :goto_0
    return-object v0

    .line 760
    :pswitch_0
    new-instance v0, Landroid/app/AlertDialog$Builder;

    invoke-direct {v0, p0}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    invoke-virtual {v0, v3}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    invoke-virtual {v0, v5}, Landroid/app/AlertDialog$Builder;->setMessage(I)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    new-instance v1, Lcom/osp/app/signin/nt;

    invoke-direct {v1, p0}, Lcom/osp/app/signin/nt;-><init>(Lcom/osp/app/signin/TnCView;)V

    invoke-virtual {v0, v4, v1}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/app/AlertDialog$Builder;->setCancelable(Z)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v0

    goto :goto_0

    .line 781
    :pswitch_1
    new-instance v0, Landroid/app/AlertDialog$Builder;

    invoke-direct {v0, p0}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    invoke-virtual {v0, v3}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    invoke-virtual {p0, v5}, Lcom/osp/app/signin/TnCView;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setMessage(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    new-instance v1, Lcom/osp/app/signin/nv;

    invoke-direct {v1, p0}, Lcom/osp/app/signin/nv;-><init>(Lcom/osp/app/signin/TnCView;)V

    invoke-virtual {v0, v4, v1}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/app/AlertDialog$Builder;->setCancelable(Z)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v0

    goto :goto_0

    .line 803
    :pswitch_2
    new-instance v0, Landroid/app/AlertDialog$Builder;

    invoke-direct {v0, p0}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    invoke-virtual {v0, v3}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    invoke-virtual {p0, v5}, Lcom/osp/app/signin/TnCView;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setMessage(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    new-instance v1, Lcom/osp/app/signin/nw;

    invoke-direct {v1, p0}, Lcom/osp/app/signin/nw;-><init>(Lcom/osp/app/signin/TnCView;)V

    invoke-virtual {v0, v4, v1}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/app/AlertDialog$Builder;->setCancelable(Z)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v0

    goto :goto_0

    .line 823
    :pswitch_3
    new-instance v0, Landroid/app/AlertDialog$Builder;

    invoke-direct {v0, p0}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    invoke-virtual {v0, v3}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    invoke-virtual {p0, v5}, Lcom/osp/app/signin/TnCView;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setMessage(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    new-instance v1, Lcom/osp/app/signin/nx;

    invoke-direct {v1, p0}, Lcom/osp/app/signin/nx;-><init>(Lcom/osp/app/signin/TnCView;)V

    invoke-virtual {v0, v4, v1}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/app/AlertDialog$Builder;->setCancelable(Z)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v0

    goto/16 :goto_0

    .line 756
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method protected onDestroy()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    const/4 v2, 0x0

    .line 1299
    invoke-super {p0}, Lcom/osp/app/util/BaseActivity;->onDestroy()V

    .line 1301
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v0, "TnCView - onDestroy"

    invoke-static {v0}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;)V

    .line 1303
    iget-object v0, p0, Lcom/osp/app/signin/TnCView;->P:Lcom/osp/app/signin/ob;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/osp/app/signin/TnCView;->P:Lcom/osp/app/signin/ob;

    invoke-virtual {v0}, Lcom/osp/app/signin/ob;->getStatus()Landroid/os/AsyncTask$Status;

    move-result-object v0

    sget-object v1, Landroid/os/AsyncTask$Status;->RUNNING:Landroid/os/AsyncTask$Status;

    if-ne v0, v1, :cond_0

    .line 1305
    iput-object v2, p0, Lcom/osp/app/signin/TnCView;->P:Lcom/osp/app/signin/ob;

    .line 1319
    :cond_0
    iget-object v0, p0, Lcom/osp/app/signin/TnCView;->R:Lcom/osp/app/signin/od;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/osp/app/signin/TnCView;->R:Lcom/osp/app/signin/od;

    invoke-virtual {v0}, Lcom/osp/app/signin/od;->getStatus()Landroid/os/AsyncTask$Status;

    move-result-object v0

    sget-object v1, Landroid/os/AsyncTask$Status;->RUNNING:Landroid/os/AsyncTask$Status;

    if-ne v0, v1, :cond_1

    .line 1321
    iget-object v0, p0, Lcom/osp/app/signin/TnCView;->R:Lcom/osp/app/signin/od;

    invoke-virtual {v0}, Lcom/osp/app/signin/od;->d()V

    .line 1322
    iput-object v2, p0, Lcom/osp/app/signin/TnCView;->R:Lcom/osp/app/signin/od;

    .line 1325
    :cond_1
    iget-object v0, p0, Lcom/osp/app/signin/TnCView;->S:Lcom/osp/app/signin/oe;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/osp/app/signin/TnCView;->S:Lcom/osp/app/signin/oe;

    invoke-virtual {v0}, Lcom/osp/app/signin/oe;->getStatus()Landroid/os/AsyncTask$Status;

    move-result-object v0

    sget-object v1, Landroid/os/AsyncTask$Status;->RUNNING:Landroid/os/AsyncTask$Status;

    if-ne v0, v1, :cond_2

    .line 1327
    iget-object v0, p0, Lcom/osp/app/signin/TnCView;->S:Lcom/osp/app/signin/oe;

    invoke-virtual {v0}, Lcom/osp/app/signin/oe;->d()V

    .line 1328
    iput-object v2, p0, Lcom/osp/app/signin/TnCView;->S:Lcom/osp/app/signin/oe;

    .line 1330
    :cond_2
    iget-object v0, p0, Lcom/osp/app/signin/TnCView;->Q:Lcom/osp/app/signin/oc;

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/osp/app/signin/TnCView;->Q:Lcom/osp/app/signin/oc;

    invoke-virtual {v0}, Lcom/osp/app/signin/oc;->getStatus()Landroid/os/AsyncTask$Status;

    move-result-object v0

    sget-object v1, Landroid/os/AsyncTask$Status;->RUNNING:Landroid/os/AsyncTask$Status;

    if-ne v0, v1, :cond_3

    .line 1332
    iget-object v0, p0, Lcom/osp/app/signin/TnCView;->Q:Lcom/osp/app/signin/oc;

    invoke-virtual {v0}, Lcom/osp/app/signin/oc;->d()V

    .line 1333
    iput-object v2, p0, Lcom/osp/app/signin/TnCView;->Q:Lcom/osp/app/signin/oc;

    .line 1336
    :cond_3
    invoke-virtual {p0}, Lcom/osp/app/signin/TnCView;->isFinishing()Z

    move-result v0

    if-nez v0, :cond_5

    iget-object v0, p0, Lcom/osp/app/signin/TnCView;->ai:Landroid/app/ProgressDialog;

    if-eqz v0, :cond_5

    .line 1340
    :try_start_0
    iget-object v0, p0, Lcom/osp/app/signin/TnCView;->ai:Landroid/app/ProgressDialog;

    invoke-virtual {v0}, Landroid/app/ProgressDialog;->isShowing()Z

    move-result v0

    if-eqz v0, :cond_4

    .line 1342
    iget-object v0, p0, Lcom/osp/app/signin/TnCView;->ai:Landroid/app/ProgressDialog;

    invoke-virtual {v0}, Landroid/app/ProgressDialog;->dismiss()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1349
    :cond_4
    :goto_0
    iput-object v2, p0, Lcom/osp/app/signin/TnCView;->ai:Landroid/app/ProgressDialog;

    .line 1353
    :cond_5
    iget-object v0, p0, Lcom/osp/app/signin/TnCView;->aZ:Lcom/osp/app/signin/ok;

    if-eqz v0, :cond_6

    iget-object v0, p0, Lcom/osp/app/signin/TnCView;->aZ:Lcom/osp/app/signin/ok;

    invoke-virtual {v0}, Lcom/osp/app/signin/ok;->getStatus()Landroid/os/AsyncTask$Status;

    move-result-object v0

    sget-object v1, Landroid/os/AsyncTask$Status;->RUNNING:Landroid/os/AsyncTask$Status;

    if-ne v0, v1, :cond_6

    .line 1355
    iget-object v0, p0, Lcom/osp/app/signin/TnCView;->aZ:Lcom/osp/app/signin/ok;

    invoke-virtual {v0}, Lcom/osp/app/signin/ok;->d()V

    .line 1356
    iput-object v2, p0, Lcom/osp/app/signin/TnCView;->aZ:Lcom/osp/app/signin/ok;

    .line 1358
    iget-object v0, p0, Lcom/osp/app/signin/TnCView;->ba:Lcom/osp/app/signin/jz;

    if-eqz v0, :cond_6

    .line 1360
    iput-object v2, p0, Lcom/osp/app/signin/TnCView;->ba:Lcom/osp/app/signin/jz;

    .line 1364
    :cond_6
    iput-object v2, p0, Lcom/osp/app/signin/TnCView;->aa:Ljava/lang/String;

    .line 1365
    iput-object v2, p0, Lcom/osp/app/signin/TnCView;->ab:Ljava/lang/String;

    .line 1366
    iput-object v2, p0, Lcom/osp/app/signin/TnCView;->Z:Ljava/lang/String;

    .line 1368
    iput-boolean v3, p0, Lcom/osp/app/signin/TnCView;->W:Z

    .line 1369
    iput-boolean v3, p0, Lcom/osp/app/signin/TnCView;->X:Z

    .line 1370
    iput-boolean v3, p0, Lcom/osp/app/signin/TnCView;->ae:Z

    .line 1372
    invoke-static {}, Lcom/osp/app/signin/SamsungService;->m()V

    .line 1373
    return-void

    .line 1344
    :catch_0
    move-exception v0

    :try_start_1
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 1349
    :catchall_0
    move-exception v0

    iput-object v2, p0, Lcom/osp/app/signin/TnCView;->ai:Landroid/app/ProgressDialog;

    throw v0
.end method

.method public onOptionsItemSelected(Landroid/view/MenuItem;)Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 1664
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v1, "onOptionsItemSelected"

    invoke-static {v1}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;)V

    .line 1671
    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v1

    sparse-switch v1, :sswitch_data_0

    .line 1710
    :goto_0
    return v0

    .line 1674
    :sswitch_0
    invoke-virtual {p0}, Lcom/osp/app/signin/TnCView;->onBackPressed()V

    .line 1710
    :goto_1
    const/4 v0, 0x1

    goto :goto_0

    .line 1679
    :sswitch_1
    iget-boolean v0, p0, Lcom/osp/app/signin/TnCView;->ae:Z

    invoke-direct {p0, v0}, Lcom/osp/app/signin/TnCView;->a(Z)V

    goto :goto_1

    .line 1694
    :sswitch_2
    invoke-virtual {p0}, Lcom/osp/app/signin/TnCView;->getIntent()Landroid/content/Intent;

    move-result-object v1

    .line 1695
    if-eqz v1, :cond_1

    const-string v2, "is_cancelable_just_one_activity"

    invoke-virtual {v1, v2, v0}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1697
    iget-object v0, p0, Lcom/osp/app/signin/TnCView;->a:Landroid/content/Intent;

    const-string v1, "key_is_name_verified"

    iget-boolean v2, p0, Lcom/osp/app/signin/TnCView;->al:Z

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 1698
    iget-boolean v0, p0, Lcom/osp/app/signin/TnCView;->al:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/osp/app/signin/TnCView;->aV:Landroid/os/Bundle;

    if-eqz v0, :cond_0

    .line 1700
    iget-object v0, p0, Lcom/osp/app/signin/TnCView;->a:Landroid/content/Intent;

    const-string v1, "information_after_name_validation"

    iget-object v2, p0, Lcom/osp/app/signin/TnCView;->aV:Landroid/os/Bundle;

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Bundle;)Landroid/content/Intent;

    .line 1702
    :cond_0
    const/16 v0, 0xe

    iget-object v1, p0, Lcom/osp/app/signin/TnCView;->a:Landroid/content/Intent;

    invoke-virtual {p0, v0, v1}, Lcom/osp/app/signin/TnCView;->a(ILandroid/content/Intent;)V

    .line 1704
    :cond_1
    invoke-virtual {p0}, Lcom/osp/app/signin/TnCView;->finish()V

    goto :goto_1

    .line 1671
    nop

    :sswitch_data_0
    .sparse-switch
        0x102002c -> :sswitch_0
        0x7f0c0199 -> :sswitch_1
        0x7f0c019a -> :sswitch_2
    .end sparse-switch
.end method

.method protected onPause()V
    .locals 2

    .prologue
    .line 1280
    invoke-super {p0}, Lcom/osp/app/util/BaseActivity;->onPause()V

    .line 1282
    const-string v0, "power"

    invoke-virtual {p0, v0}, Lcom/osp/app/signin/TnCView;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/os/PowerManager;

    .line 1283
    invoke-virtual {v0}, Landroid/os/PowerManager;->isScreenOn()Z

    move-result v0

    .line 1284
    if-eqz v0, :cond_0

    .line 1286
    iget-object v0, p0, Lcom/osp/app/signin/TnCView;->aZ:Lcom/osp/app/signin/ok;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/osp/app/signin/TnCView;->aZ:Lcom/osp/app/signin/ok;

    invoke-virtual {v0}, Lcom/osp/app/signin/ok;->getStatus()Landroid/os/AsyncTask$Status;

    move-result-object v0

    sget-object v1, Landroid/os/AsyncTask$Status;->RUNNING:Landroid/os/AsyncTask$Status;

    if-ne v0, v1, :cond_0

    .line 1288
    invoke-static {}, Lcom/osp/app/util/ap;->a()Lcom/osp/app/util/ap;

    const v0, 0x7f09004a

    invoke-virtual {p0, v0}, Lcom/osp/app/signin/TnCView;->getString(I)Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x1

    invoke-static {p0, v0, v1}, Lcom/osp/app/util/ap;->a(Landroid/content/Context;Ljava/lang/String;I)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    .line 1289
    iget-object v0, p0, Lcom/osp/app/signin/TnCView;->aZ:Lcom/osp/app/signin/ok;

    invoke-virtual {v0}, Lcom/osp/app/signin/ok;->d()V

    .line 1290
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/osp/app/signin/TnCView;->aZ:Lcom/osp/app/signin/ok;

    .line 1294
    :cond_0
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v0, "TnCView - onPause"

    invoke-static {v0}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;)V

    .line 1295
    return-void
.end method

.method protected onRestoreInstanceState(Landroid/os/Bundle;)V
    .locals 3

    .prologue
    const/4 v2, 0x1

    .line 622
    invoke-super {p0, p1}, Lcom/osp/app/util/BaseActivity;->onRestoreInstanceState(Landroid/os/Bundle;)V

    .line 624
    if-eqz p1, :cond_0

    .line 626
    const-string v0, "mIsTncAcceptChecked"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->get(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    iput-boolean v0, p0, Lcom/osp/app/signin/TnCView;->D:Z

    .line 627
    const-string v0, "mIsPrivacyAcceptChecked"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->get(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    iput-boolean v0, p0, Lcom/osp/app/signin/TnCView;->E:Z

    .line 628
    const-string v0, "mIsdataCollectionAcceptChecked"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->get(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    iput-boolean v0, p0, Lcom/osp/app/signin/TnCView;->F:Z

    .line 629
    const-string v0, "mIsOnwardTransferAcceptedChecked"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->get(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    iput-boolean v0, p0, Lcom/osp/app/signin/TnCView;->G:Z

    .line 630
    const-string v0, "mIsEsuTncAcceptedChecked"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->get(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    iput-boolean v0, p0, Lcom/osp/app/signin/TnCView;->H:Z

    .line 637
    iget-object v0, p0, Lcom/osp/app/signin/TnCView;->aK:Lcom/osp/app/signin/BottomSoftkeyLinearLayout;

    if-eqz v0, :cond_2

    const-string v0, "mBtnConfirm"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->get(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 639
    invoke-static {}, Lcom/osp/device/b;->b()Lcom/osp/device/b;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/osp/device/b;->l(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 641
    iget-object v0, p0, Lcom/osp/app/signin/TnCView;->aK:Lcom/osp/app/signin/BottomSoftkeyLinearLayout;

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/osp/app/signin/BottomSoftkeyLinearLayout;->a(Ljava/lang/Boolean;)V

    .line 652
    :cond_0
    :goto_0
    return-void

    .line 644
    :cond_1
    iget-object v0, p0, Lcom/osp/app/signin/TnCView;->aK:Lcom/osp/app/signin/BottomSoftkeyLinearLayout;

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/osp/app/signin/BottomSoftkeyLinearLayout;->b(Ljava/lang/Boolean;)V

    goto :goto_0

    .line 646
    :cond_2
    iget-object v0, p0, Lcom/osp/app/signin/TnCView;->x:Landroid/view/Menu;

    if-eqz v0, :cond_0

    const-string v0, "menuConfirm"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->get(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 648
    iget-object v0, p0, Lcom/osp/app/signin/TnCView;->x:Landroid/view/Menu;

    const v1, 0x7f0c0199

    invoke-interface {v0, v1}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v0

    invoke-interface {v0, v2}, Landroid/view/MenuItem;->setEnabled(Z)Landroid/view/MenuItem;

    goto :goto_0
.end method

.method protected onResume()V
    .locals 2

    .prologue
    .line 1257
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/osp/app/signin/TnCView;->aI:J

    .line 1260
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/osp/app/signin/TnCView;->O:Z

    .line 1261
    invoke-super {p0}, Lcom/osp/app/util/BaseActivity;->onResume()V

    .line 1267
    iget-object v0, p0, Lcom/osp/app/signin/TnCView;->aK:Lcom/osp/app/signin/BottomSoftkeyLinearLayout;

    if-nez v0, :cond_0

    invoke-static {p0}, Lcom/osp/app/util/ad;->d(Landroid/content/Context;)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/osp/app/signin/TnCView;->d:Ljava/lang/String;

    if-eqz v0, :cond_0

    .line 1269
    iget-object v0, p0, Lcom/osp/app/signin/TnCView;->d:Ljava/lang/String;

    const-string v1, "REQUEST_TNC"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 1271
    const/16 v0, 0x10

    invoke-virtual {p0, v0}, Lcom/osp/app/signin/TnCView;->b(I)V

    .line 1272
    invoke-virtual {p0}, Lcom/osp/app/signin/TnCView;->finish()V

    .line 1275
    :cond_0
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v0, "TnCView - onResume"

    invoke-static {v0}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;)V

    .line 1276
    return-void
.end method

.method protected onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 3

    .prologue
    const v2, 0x7f0c0199

    .line 2266
    if-eqz p1, :cond_2

    .line 2268
    invoke-super {p0, p1}, Lcom/osp/app/util/BaseActivity;->onSaveInstanceState(Landroid/os/Bundle;)V

    .line 2270
    const-string v0, "mIsTncAcceptChecked"

    iget-boolean v1, p0, Lcom/osp/app/signin/TnCView;->D:Z

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 2271
    const-string v0, "mIsPrivacyAcceptChecked"

    iget-boolean v1, p0, Lcom/osp/app/signin/TnCView;->E:Z

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 2272
    const-string v0, "mIsdataCollectionAcceptChecked"

    iget-boolean v1, p0, Lcom/osp/app/signin/TnCView;->F:Z

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 2273
    const-string v0, "mIsOnwardTransferAcceptedChecked"

    iget-boolean v1, p0, Lcom/osp/app/signin/TnCView;->G:Z

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 2274
    const-string v0, "mIsEsuTncAcceptedChecked"

    iget-boolean v1, p0, Lcom/osp/app/signin/TnCView;->H:Z

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 2275
    iget-object v0, p0, Lcom/osp/app/signin/TnCView;->aK:Lcom/osp/app/signin/BottomSoftkeyLinearLayout;

    if-eqz v0, :cond_0

    .line 2277
    invoke-static {}, Lcom/osp/device/b;->b()Lcom/osp/device/b;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/osp/device/b;->l(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 2279
    const-string v0, "mBtnConfirm"

    iget-object v1, p0, Lcom/osp/app/signin/TnCView;->aK:Lcom/osp/app/signin/BottomSoftkeyLinearLayout;

    invoke-virtual {v1}, Lcom/osp/app/signin/BottomSoftkeyLinearLayout;->a()Ljava/lang/Boolean;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 2289
    :cond_0
    :goto_0
    iget-object v0, p0, Lcom/osp/app/signin/TnCView;->x:Landroid/view/Menu;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/osp/app/signin/TnCView;->x:Landroid/view/Menu;

    invoke-interface {v0, v2}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 2291
    const-string v0, "menuConfirm"

    iget-object v1, p0, Lcom/osp/app/signin/TnCView;->x:Landroid/view/Menu;

    invoke-interface {v1, v2}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v1

    invoke-interface {v1}, Landroid/view/MenuItem;->isEnabled()Z

    move-result v1

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 2293
    :cond_1
    iget-object v0, p0, Lcom/osp/app/signin/TnCView;->T:Lcom/osp/app/signin/kd;

    if-eqz v0, :cond_2

    .line 2295
    const-string v0, "FIELD_INFO"

    iget-object v1, p0, Lcom/osp/app/signin/TnCView;->T:Lcom/osp/app/signin/kd;

    invoke-virtual {v1}, Lcom/osp/app/signin/kd;->X()Landroid/os/Bundle;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putBundle(Ljava/lang/String;Landroid/os/Bundle;)V

    .line 2299
    :cond_2
    return-void

    .line 2282
    :cond_3
    const-string v0, "mBtnConfirm"

    iget-object v1, p0, Lcom/osp/app/signin/TnCView;->aK:Lcom/osp/app/signin/BottomSoftkeyLinearLayout;

    invoke-virtual {v1}, Lcom/osp/app/signin/BottomSoftkeyLinearLayout;->b()Ljava/lang/Boolean;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    goto :goto_0
.end method
