.class final Lcom/osp/app/signin/py;
.super Landroid/webkit/WebViewClient;
.source "WebContentView.java"


# instance fields
.field final synthetic a:Lcom/osp/app/signin/WebContentView;

.field private final b:Ljava/lang/String;


# direct methods
.method private constructor <init>(Lcom/osp/app/signin/WebContentView;)V
    .locals 1

    .prologue
    .line 643
    iput-object p1, p0, Lcom/osp/app/signin/py;->a:Lcom/osp/app/signin/WebContentView;

    invoke-direct {p0}, Landroid/webkit/WebViewClient;-><init>()V

    .line 645
    const-string v0, "/mobile/account/deviceInterfaceCloseOAuth2.do"

    iput-object v0, p0, Lcom/osp/app/signin/py;->b:Ljava/lang/String;

    return-void
.end method

.method synthetic constructor <init>(Lcom/osp/app/signin/WebContentView;B)V
    .locals 0

    .prologue
    .line 643
    invoke-direct {p0, p1}, Lcom/osp/app/signin/py;-><init>(Lcom/osp/app/signin/WebContentView;)V

    return-void
.end method


# virtual methods
.method public final onLoadResource(Landroid/webkit/WebView;Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 700
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v0, "WCV"

    const-string v1, "onLoadResource"

    invoke-static {v0, v1}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 701
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "WebViewClientClass::onLoadResource URL = "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;)V

    .line 703
    invoke-super {p0, p1, p2}, Landroid/webkit/WebViewClient;->onLoadResource(Landroid/webkit/WebView;Ljava/lang/String;)V

    .line 704
    return-void
.end method

.method public final onPageFinished(Landroid/webkit/WebView;Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 681
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v0, "WCV"

    const-string v1, "onPageFinished"

    invoke-static {v0, v1}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 682
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "WebContentView::onPageFinished URL = "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;)V

    .line 686
    :try_start_0
    iget-object v0, p0, Lcom/osp/app/signin/py;->a:Lcom/osp/app/signin/WebContentView;

    invoke-virtual {v0}, Lcom/osp/app/signin/WebContentView;->isFinishing()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/osp/app/signin/py;->a:Lcom/osp/app/signin/WebContentView;

    invoke-static {v0}, Lcom/osp/app/signin/WebContentView;->c(Lcom/osp/app/signin/WebContentView;)Landroid/app/ProgressDialog;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/osp/app/signin/py;->a:Lcom/osp/app/signin/WebContentView;

    invoke-static {v0}, Lcom/osp/app/signin/WebContentView;->c(Lcom/osp/app/signin/WebContentView;)Landroid/app/ProgressDialog;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/ProgressDialog;->isShowing()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 688
    iget-object v0, p0, Lcom/osp/app/signin/py;->a:Lcom/osp/app/signin/WebContentView;

    invoke-static {v0}, Lcom/osp/app/signin/WebContentView;->c(Lcom/osp/app/signin/WebContentView;)Landroid/app/ProgressDialog;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/ProgressDialog;->dismiss()V

    .line 689
    iget-object v0, p0, Lcom/osp/app/signin/py;->a:Lcom/osp/app/signin/WebContentView;

    invoke-static {v0}, Lcom/osp/app/signin/WebContentView;->d(Lcom/osp/app/signin/WebContentView;)Landroid/app/ProgressDialog;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 695
    :cond_0
    :goto_0
    invoke-super {p0, p1, p2}, Landroid/webkit/WebViewClient;->onPageFinished(Landroid/webkit/WebView;Ljava/lang/String;)V

    .line 696
    return-void

    .line 691
    :catch_0
    move-exception v0

    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_0
.end method

.method public final onPageStarted(Landroid/webkit/WebView;Ljava/lang/String;Landroid/graphics/Bitmap;)V
    .locals 2

    .prologue
    .line 661
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v0, "WCV"

    const-string v1, "onPageStarted"

    invoke-static {v0, v1}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 662
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "WebContentView::onPageStarted URL = "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;)V

    .line 664
    const-string v0, "/mobile/account/deviceInterfaceCloseOAuth2.do"

    invoke-virtual {p2, v0}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 666
    invoke-static {p2}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    .line 667
    const-string v1, "close"

    invoke-virtual {v0, v1}, Landroid/net/Uri;->getQueryParameter(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 668
    const-string v1, "true"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 670
    iget-object v0, p0, Lcom/osp/app/signin/py;->a:Lcom/osp/app/signin/WebContentView;

    invoke-virtual {v0}, Lcom/osp/app/signin/WebContentView;->finish()V

    .line 674
    :cond_0
    iget-object v0, p0, Lcom/osp/app/signin/py;->a:Lcom/osp/app/signin/WebContentView;

    invoke-static {v0}, Lcom/osp/app/signin/WebContentView;->e(Lcom/osp/app/signin/WebContentView;)V

    .line 676
    invoke-super {p0, p1, p2, p3}, Landroid/webkit/WebViewClient;->onPageStarted(Landroid/webkit/WebView;Ljava/lang/String;Landroid/graphics/Bitmap;)V

    .line 677
    return-void
.end method

.method public final onReceivedError(Landroid/webkit/WebView;ILjava/lang/String;Ljava/lang/String;)V
    .locals 3

    .prologue
    .line 708
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v0, "WCV"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "onReceivedError errorCode : "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " ["

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "]"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 712
    :try_start_0
    iget-object v0, p0, Lcom/osp/app/signin/py;->a:Lcom/osp/app/signin/WebContentView;

    invoke-virtual {v0}, Lcom/osp/app/signin/WebContentView;->isFinishing()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/osp/app/signin/py;->a:Lcom/osp/app/signin/WebContentView;

    invoke-static {v0}, Lcom/osp/app/signin/WebContentView;->c(Lcom/osp/app/signin/WebContentView;)Landroid/app/ProgressDialog;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/osp/app/signin/py;->a:Lcom/osp/app/signin/WebContentView;

    invoke-static {v0}, Lcom/osp/app/signin/WebContentView;->c(Lcom/osp/app/signin/WebContentView;)Landroid/app/ProgressDialog;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/ProgressDialog;->isShowing()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 714
    iget-object v0, p0, Lcom/osp/app/signin/py;->a:Lcom/osp/app/signin/WebContentView;

    invoke-static {v0}, Lcom/osp/app/signin/WebContentView;->c(Lcom/osp/app/signin/WebContentView;)Landroid/app/ProgressDialog;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/ProgressDialog;->dismiss()V

    .line 715
    iget-object v0, p0, Lcom/osp/app/signin/py;->a:Lcom/osp/app/signin/WebContentView;

    invoke-static {v0}, Lcom/osp/app/signin/WebContentView;->d(Lcom/osp/app/signin/WebContentView;)Landroid/app/ProgressDialog;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 722
    :cond_0
    :goto_0
    invoke-super {p0, p1, p2, p3, p4}, Landroid/webkit/WebViewClient;->onReceivedError(Landroid/webkit/WebView;ILjava/lang/String;Ljava/lang/String;)V

    .line 723
    return-void

    .line 717
    :catch_0
    move-exception v0

    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_0
.end method

.method public final shouldOverrideUrlLoading(Landroid/webkit/WebView;Ljava/lang/String;)Z
    .locals 2

    .prologue
    .line 649
    const-string v0, "com.msc.action.samsungaccount.web_dialog"

    iget-object v1, p0, Lcom/osp/app/signin/py;->a:Lcom/osp/app/signin/WebContentView;

    invoke-virtual {v1}, Lcom/osp/app/signin/WebContentView;->getIntent()Landroid/content/Intent;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 651
    invoke-super {p0, p1, p2}, Landroid/webkit/WebViewClient;->shouldOverrideUrlLoading(Landroid/webkit/WebView;Ljava/lang/String;)Z

    move-result v0

    .line 656
    :goto_0
    return v0

    .line 654
    :cond_0
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "WebContentView::shouldOverrideUrlLoading URL = "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;)V

    .line 655
    invoke-virtual {p1, p2}, Landroid/webkit/WebView;->loadUrl(Ljava/lang/String;)V

    .line 656
    const/4 v0, 0x1

    goto :goto_0
.end method
