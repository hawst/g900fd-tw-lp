.class public final Lcom/osp/app/signin/gd;
.super Landroid/accounts/AbstractAccountAuthenticator;
.source "OspAuthenticationService.java"


# instance fields
.field a:Landroid/content/Context;

.field final synthetic b:Lcom/osp/app/signin/OspAuthenticationService;


# direct methods
.method public constructor <init>(Lcom/osp/app/signin/OspAuthenticationService;Landroid/content/Context;)V
    .locals 0

    .prologue
    .line 176
    iput-object p1, p0, Lcom/osp/app/signin/gd;->b:Lcom/osp/app/signin/OspAuthenticationService;

    .line 177
    invoke-direct {p0, p2}, Landroid/accounts/AbstractAccountAuthenticator;-><init>(Landroid/content/Context;)V

    .line 178
    iput-object p2, p0, Lcom/osp/app/signin/gd;->a:Landroid/content/Context;

    .line 179
    return-void
.end method


# virtual methods
.method public final addAccount(Landroid/accounts/AccountAuthenticatorResponse;Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Landroid/os/Bundle;)Landroid/os/Bundle;
    .locals 9

    .prologue
    const/4 v8, -0x1

    .line 206
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v0, "OAS"

    const-string v1, "addAccount"

    const-string v2, "START"

    invoke-static {v0, v1, v2}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 208
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 210
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v1, " identitymanager initial"

    invoke-static {v1}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;)V

    .line 216
    iget-object v1, p0, Lcom/osp/app/signin/gd;->b:Lcom/osp/app/signin/OspAuthenticationService;

    const-string v2, "SELF_UPGRADE"

    const/4 v3, 0x0

    invoke-virtual {v1, v2, v3}, Lcom/osp/app/signin/OspAuthenticationService;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v1

    .line 217
    const-string v2, "SELF_UPGRADE_START_TIME"

    const-wide/16 v4, -0x1

    invoke-interface {v1, v2, v4, v5}, Landroid/content/SharedPreferences;->getLong(Ljava/lang/String;J)J

    move-result-wide v2

    .line 219
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v4

    sub-long/2addr v4, v2

    .line 221
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v1, "OAS"

    new-instance v6, Ljava/lang/StringBuilder;

    const-string v7, "SELF_UPGRADE_TIME_CHECK duration : "

    invoke-direct {v6, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v6, v4, v5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, " / start point : "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 223
    const-wide/16 v2, 0x0

    cmp-long v1, v4, v2

    if-lez v1, :cond_0

    const-wide/16 v2, 0x2710

    cmp-long v1, v4, v2

    if-gtz v1, :cond_0

    .line 226
    iget-object v1, p0, Lcom/osp/app/signin/gd;->b:Lcom/osp/app/signin/OspAuthenticationService;

    invoke-static {v1}, Lcom/osp/app/signin/OspAuthenticationService;->b(Lcom/osp/app/signin/OspAuthenticationService;)Landroid/os/Handler;

    move-result-object v1

    new-instance v2, Lcom/osp/app/signin/ge;

    invoke-direct {v2, p0}, Lcom/osp/app/signin/ge;-><init>(Lcom/osp/app/signin/gd;)V

    invoke-virtual {v1, v2}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 234
    const-string v1, "errorCode"

    invoke-virtual {v0, v1, v8}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 235
    const-string v1, "errorMessage"

    const-string v2, "Error!"

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 281
    :goto_0
    return-object v0

    .line 241
    :cond_0
    iget-object v1, p0, Lcom/osp/app/signin/gd;->b:Lcom/osp/app/signin/OspAuthenticationService;

    invoke-static {v1}, Lcom/osp/app/util/ad;->d(Landroid/content/Context;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 243
    iget-object v1, p0, Lcom/osp/app/signin/gd;->b:Lcom/osp/app/signin/OspAuthenticationService;

    invoke-static {v1}, Lcom/osp/app/signin/OspAuthenticationService;->b(Lcom/osp/app/signin/OspAuthenticationService;)Landroid/os/Handler;

    move-result-object v1

    new-instance v2, Lcom/osp/app/signin/gf;

    invoke-direct {v2, p0}, Lcom/osp/app/signin/gf;-><init>(Lcom/osp/app/signin/gd;)V

    invoke-virtual {v1, v2}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 251
    const-string v1, "errorCode"

    invoke-virtual {v0, v1, v8}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 252
    const-string v1, "errorMessage"

    const-string v2, "Error!"

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 256
    :cond_1
    new-instance v1, Landroid/content/Intent;

    invoke-direct {v1}, Landroid/content/Intent;-><init>()V

    .line 262
    const-string v2, "com.osp.app.signin.action.ADD_SAMSUNG_ACCOUNT"

    invoke-virtual {v1, v2}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 264
    const-string v2, "MODE"

    const-string v3, "ADD_ACCOUNT"

    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 265
    iget-object v2, p0, Lcom/osp/app/signin/gd;->b:Lcom/osp/app/signin/OspAuthenticationService;

    invoke-static {v2}, Lcom/osp/app/signin/OspAuthenticationService;->c(Lcom/osp/app/signin/OspAuthenticationService;)Ljava/lang/String;

    move-result-object v2

    if-eqz v2, :cond_2

    .line 267
    iget-object v2, p0, Lcom/osp/app/signin/gd;->b:Lcom/osp/app/signin/OspAuthenticationService;

    invoke-static {v2}, Lcom/osp/app/signin/OspAuthenticationService;->c(Lcom/osp/app/signin/OspAuthenticationService;)Ljava/lang/String;

    move-result-object v2

    const-string v3, "OSP 1.0"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    .line 269
    const-string v2, "OSP_VER"

    const-string v3, "OSP_01"

    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 275
    :cond_2
    :goto_1
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v2, "new account"

    invoke-static {v2}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;)V

    .line 276
    const-string v2, "accountAuthenticatorResponse"

    invoke-virtual {v1, v2, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 277
    const-string v2, "manageAccount"

    invoke-virtual {v1, v2, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 278
    const-string v2, "intent"

    invoke-virtual {v0, v2, v1}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 280
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v1, "OAS"

    const-string v2, "addAccount"

    const-string v3, "END"

    invoke-static {v1, v2, v3}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 272
    :cond_3
    const-string v2, "OSP_VER"

    const-string v3, "OSP_02"

    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    goto :goto_1
.end method

.method public final confirmCredentials(Landroid/accounts/AccountAuthenticatorResponse;Landroid/accounts/Account;Landroid/os/Bundle;)Landroid/os/Bundle;
    .locals 1

    .prologue
    .line 298
    const/4 v0, 0x0

    return-object v0
.end method

.method public final editProperties(Landroid/accounts/AccountAuthenticatorResponse;Ljava/lang/String;)Landroid/os/Bundle;
    .locals 1

    .prologue
    .line 318
    const/4 v0, 0x0

    return-object v0
.end method

.method public final getAccountRemovalAllowed(Landroid/accounts/AccountAuthenticatorResponse;Landroid/accounts/Account;)Landroid/os/Bundle;
    .locals 4

    .prologue
    .line 333
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v0, "OAS"

    const-string v1, "getAccountRemovalAllowed"

    const-string v2, "START"

    invoke-static {v0, v1, v2}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 334
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 336
    iget-object v1, p0, Lcom/osp/app/signin/gd;->b:Lcom/osp/app/signin/OspAuthenticationService;

    invoke-static {v1}, Lcom/osp/app/util/ad;->d(Landroid/content/Context;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 338
    const-string v1, "errorCode"

    const/4 v2, -0x1

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 339
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v1, "OAS"

    const-string v2, "getAccountRemovalAllowed no account"

    const-string v3, "END"

    invoke-static {v1, v2, v3}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 463
    :goto_0
    return-object v0

    .line 437
    :cond_0
    const-string v1, "booleanResult"

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 439
    iget-object v1, p0, Lcom/osp/app/signin/gd;->b:Lcom/osp/app/signin/OspAuthenticationService;

    invoke-static {v1}, Landroid/accounts/AccountManager;->get(Landroid/content/Context;)Landroid/accounts/AccountManager;

    move-result-object v1

    .line 441
    const-string v2, "SAMSUNG_ACCOUNT_SIGNOUT_BLOCKED"

    const-string v3, "KEY_SIGNOUT_PERMIT"

    invoke-virtual {v1, p2, v3}, Landroid/accounts/AccountManager;->getUserData(Landroid/accounts/Account;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_1

    .line 444
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v1, "OAS"

    const-string v2, "Try to signout"

    invoke-static {v1, v2}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 445
    new-instance v1, Landroid/content/Intent;

    invoke-direct {v1}, Landroid/content/Intent;-><init>()V

    .line 446
    const-string v2, "OSP_VER"

    iget-object v3, p0, Lcom/osp/app/signin/gd;->b:Lcom/osp/app/signin/OspAuthenticationService;

    invoke-static {v3}, Lcom/osp/app/signin/OspAuthenticationService;->d(Lcom/osp/app/signin/OspAuthenticationService;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 447
    const-string v2, "com.osp.app.signin"

    const-string v3, "com.osp.app.signin.BackgroundModeSignOutService"

    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->setClassName(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 448
    iget-object v2, p0, Lcom/osp/app/signin/gd;->b:Lcom/osp/app/signin/OspAuthenticationService;

    invoke-static {v2}, Lcom/osp/app/signin/OspAuthenticationService;->a(Lcom/osp/app/signin/OspAuthenticationService;)Landroid/content/Context;

    move-result-object v2

    invoke-virtual {v2, v1}, Landroid/content/Context;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;

    .line 462
    :goto_1
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v1, "OAS"

    const-string v2, "getAccountRemovalAllowed"

    const-string v3, "END"

    invoke-static {v1, v2, v3}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 460
    :cond_1
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v1, "OAS"

    const-string v2, "Try to change account"

    invoke-static {v1, v2}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1
.end method

.method public final getAuthToken(Landroid/accounts/AccountAuthenticatorResponse;Landroid/accounts/Account;Ljava/lang/String;Landroid/os/Bundle;)Landroid/os/Bundle;
    .locals 1

    .prologue
    .line 569
    const/4 v0, 0x0

    return-object v0
.end method

.method public final getAuthTokenLabel(Ljava/lang/String;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 876
    const/4 v0, 0x0

    return-object v0
.end method

.method public final hasFeatures(Landroid/accounts/AccountAuthenticatorResponse;Landroid/accounts/Account;[Ljava/lang/String;)Landroid/os/Bundle;
    .locals 1

    .prologue
    .line 892
    const/4 v0, 0x0

    return-object v0
.end method

.method public final updateCredentials(Landroid/accounts/AccountAuthenticatorResponse;Landroid/accounts/Account;Ljava/lang/String;Landroid/os/Bundle;)Landroid/os/Bundle;
    .locals 1

    .prologue
    .line 912
    const/4 v0, 0x0

    return-object v0
.end method
