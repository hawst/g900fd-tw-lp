.class final Lcom/osp/app/signin/jj;
.super Lcom/msc/c/b;
.source "SignInView.java"


# instance fields
.field final synthetic c:Lcom/osp/app/signin/SignInView;

.field private d:Ljava/lang/String;

.field private e:J

.field private f:J

.field private g:J

.field private h:J


# direct methods
.method public constructor <init>(Lcom/osp/app/signin/SignInView;)V
    .locals 0

    .prologue
    .line 4037
    iput-object p1, p0, Lcom/osp/app/signin/jj;->c:Lcom/osp/app/signin/SignInView;

    .line 4038
    invoke-direct {p0, p1}, Lcom/msc/c/b;-><init>(Landroid/content/Context;)V

    .line 4042
    return-void
.end method


# virtual methods
.method protected final varargs a()Ljava/lang/Boolean;
    .locals 4

    .prologue
    .line 4052
    iget-object v0, p0, Lcom/osp/app/signin/jj;->c:Lcom/osp/app/signin/SignInView;

    invoke-static {v0}, Lcom/osp/app/signin/SignInView;->e(Lcom/osp/app/signin/SignInView;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/osp/app/signin/jj;->c:Lcom/osp/app/signin/SignInView;

    invoke-static {v0}, Lcom/osp/app/signin/SignInView;->s(Lcom/osp/app/signin/SignInView;)Landroid/view/View;

    move-result-object v0

    const v1, 0x7f0c0129

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/EditText;

    iget-object v1, p0, Lcom/osp/app/signin/jj;->c:Lcom/osp/app/signin/SignInView;

    invoke-static {v1}, Lcom/osp/app/signin/SignInView;->s(Lcom/osp/app/signin/SignInView;)Landroid/view/View;

    move-result-object v1

    const v2, 0x7f0c012a

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/EditText;

    :goto_0
    invoke-static {}, Lcom/msc/c/g;->a()Lcom/msc/c/g;

    iget-object v2, p0, Lcom/osp/app/signin/jj;->c:Lcom/osp/app/signin/SignInView;

    invoke-virtual {v0}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    sget-object v3, Ljava/util/Locale;->ENGLISH:Ljava/util/Locale;

    invoke-virtual {v0, v3}, Ljava/lang/String;->toLowerCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    const/4 v3, 0x0

    invoke-static {v2, v0, v1, v3, p0}, Lcom/msc/c/g;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Lcom/osp/app/signin/cn;Lcom/msc/b/h;)J

    move-result-wide v0

    iput-wide v0, p0, Lcom/osp/app/signin/jj;->e:J

    iget-wide v0, p0, Lcom/osp/app/signin/jj;->e:J

    invoke-virtual {p0, v0, v1}, Lcom/osp/app/signin/jj;->a(J)V

    iget-wide v0, p0, Lcom/osp/app/signin/jj;->e:J

    const-string v2, "from_json"

    invoke-virtual {p0, v0, v1, v2}, Lcom/osp/app/signin/jj;->a(JLjava/lang/String;)V

    iget-wide v0, p0, Lcom/osp/app/signin/jj;->e:J

    sget-object v2, Lcom/msc/b/g;->c:Lcom/msc/b/g;

    invoke-static {v0, v1, v2}, Lcom/msc/c/g;->a(JLcom/msc/b/g;)V

    .line 4053
    const/4 v0, 0x0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    return-object v0

    .line 4052
    :cond_0
    iget-object v0, p0, Lcom/osp/app/signin/jj;->c:Lcom/osp/app/signin/SignInView;

    const v1, 0x7f0c0121

    invoke-virtual {v0, v1}, Lcom/osp/app/signin/SignInView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/EditText;

    iget-object v1, p0, Lcom/osp/app/signin/jj;->c:Lcom/osp/app/signin/SignInView;

    const v2, 0x7f0c0122

    invoke-virtual {v1, v2}, Lcom/osp/app/signin/SignInView;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/EditText;

    goto :goto_0
.end method

.method public final declared-synchronized a(Lcom/msc/b/c;)V
    .locals 10

    .prologue
    .line 4077
    monitor-enter p0

    :try_start_0
    invoke-super {p0, p1}, Lcom/msc/c/b;->a(Lcom/msc/b/c;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 4079
    if-nez p1, :cond_1

    .line 4150
    :cond_0
    :goto_0
    monitor-exit p0

    return-void

    .line 4084
    :cond_1
    :try_start_1
    invoke-virtual {p1}, Lcom/msc/b/c;->a()J

    move-result-wide v2

    .line 4085
    invoke-virtual {p1}, Lcom/msc/b/c;->e()Ljava/lang/String;

    move-result-object v4

    .line 4087
    iget-object v5, p0, Lcom/osp/app/signin/jj;->c:Lcom/osp/app/signin/SignInView;

    invoke-static {v5}, Lcom/osp/app/signin/SignInView;->j(Lcom/osp/app/signin/SignInView;)Ljava/lang/String;

    move-result-object v5

    .line 4088
    iget-object v6, p0, Lcom/osp/app/signin/jj;->c:Lcom/osp/app/signin/SignInView;

    invoke-static {v6}, Lcom/osp/app/signin/SignInView;->k(Lcom/osp/app/signin/SignInView;)Ljava/lang/String;

    move-result-object v6

    .line 4089
    if-eqz v5, :cond_2

    invoke-virtual {v5}, Ljava/lang/String;->length()I

    move-result v7

    if-lez v7, :cond_2

    if-eqz v6, :cond_2

    invoke-virtual {v6}, Ljava/lang/String;->length()I

    move-result v7

    if-gtz v7, :cond_3

    .line 4091
    :cond_2
    const-string v5, "j5p7ll8g33"

    .line 4092
    const-string v6, "5763D0052DC1462E13751F753384E9A9"
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 4096
    :cond_3
    :try_start_2
    iget-wide v8, p0, Lcom/osp/app/signin/jj;->e:J

    cmp-long v7, v2, v8

    if-nez v7, :cond_5

    .line 4099
    iget-object v2, p0, Lcom/osp/app/signin/jj;->c:Lcom/osp/app/signin/SignInView;

    invoke-static {}, Lcom/msc/c/h;->a()Lcom/msc/c/h;

    invoke-static {v4}, Lcom/msc/c/h;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/osp/app/signin/SignInView;->a(Lcom/osp/app/signin/SignInView;Ljava/lang/String;)Ljava/lang/String;

    .line 4100
    iget-object v2, p0, Lcom/osp/app/signin/jj;->c:Lcom/osp/app/signin/SignInView;

    invoke-static {}, Lcom/msc/c/h;->a()Lcom/msc/c/h;

    invoke-static {v4}, Lcom/msc/c/h;->q(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/osp/app/signin/SignInView;->b(Lcom/osp/app/signin/SignInView;Ljava/lang/String;)Ljava/lang/String;

    .line 4105
    iget-object v2, p0, Lcom/osp/app/signin/jj;->c:Lcom/osp/app/signin/SignInView;

    invoke-static {v2}, Lcom/osp/app/signin/SignInView;->x(Lcom/osp/app/signin/SignInView;)Ljava/lang/String;

    move-result-object v2

    if-eqz v2, :cond_4

    iget-object v2, p0, Lcom/osp/app/signin/jj;->c:Lcom/osp/app/signin/SignInView;

    invoke-static {v2}, Lcom/osp/app/signin/SignInView;->x(Lcom/osp/app/signin/SignInView;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v2

    if-lez v2, :cond_4

    .line 4108
    invoke-virtual {p0}, Lcom/osp/app/signin/jj;->isCancelled()Z

    move-result v2

    if-nez v2, :cond_0

    .line 4112
    iget-object v2, p0, Lcom/osp/app/signin/jj;->c:Lcom/osp/app/signin/SignInView;

    invoke-static {v2}, Lcom/osp/app/signin/SignInView;->x(Lcom/osp/app/signin/SignInView;)Ljava/lang/String;

    move-result-object v2

    invoke-static {}, Lcom/msc/c/g;->a()Lcom/msc/c/g;

    iget-object v3, p0, Lcom/osp/app/signin/jj;->c:Lcom/osp/app/signin/SignInView;

    const/4 v4, 0x0

    invoke-static {v3, v5, v2, v4, p0}, Lcom/msc/c/g;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/msc/b/h;)J

    move-result-wide v2

    iput-wide v2, p0, Lcom/osp/app/signin/jj;->f:J

    iget-wide v2, p0, Lcom/osp/app/signin/jj;->f:J

    invoke-virtual {p0, v2, v3}, Lcom/osp/app/signin/jj;->a(J)V

    iget-wide v2, p0, Lcom/osp/app/signin/jj;->f:J

    const-string v4, "from_json"

    invoke-virtual {p0, v2, v3, v4}, Lcom/osp/app/signin/jj;->a(JLjava/lang/String;)V

    iget-wide v2, p0, Lcom/osp/app/signin/jj;->f:J

    sget-object v4, Lcom/msc/b/g;->b:Lcom/msc/b/g;

    invoke-static {v2, v3, v4}, Lcom/msc/c/g;->a(JLcom/msc/b/g;)V
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_0
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto/16 :goto_0

    .line 4144
    :catch_0
    move-exception v2

    .line 4146
    :try_start_3
    invoke-virtual {v2}, Ljava/lang/Exception;->printStackTrace()V

    .line 4147
    new-instance v3, Lcom/msc/c/f;

    invoke-direct {v3, v2}, Lcom/msc/c/f;-><init>(Ljava/lang/Exception;)V

    iput-object v3, p0, Lcom/osp/app/signin/jj;->b:Lcom/msc/c/f;
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    goto/16 :goto_0

    .line 4077
    :catchall_0
    move-exception v2

    monitor-exit p0

    throw v2

    .line 4115
    :cond_4
    :try_start_4
    new-instance v2, Lcom/msc/c/f;

    invoke-direct {v2}, Lcom/msc/c/f;-><init>()V

    iput-object v2, p0, Lcom/osp/app/signin/jj;->b:Lcom/msc/c/f;

    goto/16 :goto_0

    .line 4118
    :cond_5
    iget-wide v8, p0, Lcom/osp/app/signin/jj;->f:J

    cmp-long v7, v2, v8

    if-nez v7, :cond_6

    .line 4120
    invoke-static {}, Lcom/msc/c/h;->a()Lcom/msc/c/h;

    invoke-static {v4}, Lcom/msc/c/h;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 4121
    invoke-virtual {p0}, Lcom/osp/app/signin/jj;->isCancelled()Z

    move-result v2

    if-nez v2, :cond_0

    .line 4125
    if-eqz v4, :cond_0

    .line 4128
    invoke-static {}, Lcom/msc/c/g;->a()Lcom/msc/c/g;

    iget-object v2, p0, Lcom/osp/app/signin/jj;->c:Lcom/osp/app/signin/SignInView;

    const-string v3, "authorization_code"

    move-object v7, p0

    invoke-static/range {v2 .. v7}, Lcom/msc/c/g;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/msc/b/h;)J

    move-result-wide v2

    iput-wide v2, p0, Lcom/osp/app/signin/jj;->g:J

    iget-wide v2, p0, Lcom/osp/app/signin/jj;->g:J

    invoke-virtual {p0, v2, v3}, Lcom/osp/app/signin/jj;->a(J)V

    iget-wide v2, p0, Lcom/osp/app/signin/jj;->g:J

    const-string v4, "from_json"

    invoke-virtual {p0, v2, v3, v4}, Lcom/osp/app/signin/jj;->a(JLjava/lang/String;)V

    iget-wide v2, p0, Lcom/osp/app/signin/jj;->g:J

    sget-object v4, Lcom/msc/b/g;->c:Lcom/msc/b/g;

    invoke-static {v2, v3, v4}, Lcom/msc/c/g;->a(JLcom/msc/b/g;)V

    goto/16 :goto_0

    .line 4130
    :cond_6
    iget-wide v6, p0, Lcom/osp/app/signin/jj;->g:J

    cmp-long v5, v2, v6

    if-nez v5, :cond_7

    .line 4132
    invoke-static {}, Lcom/msc/c/h;->a()Lcom/msc/c/h;

    iget-object v2, p0, Lcom/osp/app/signin/jj;->c:Lcom/osp/app/signin/SignInView;

    invoke-static {v2, v4}, Lcom/msc/c/h;->a(Landroid/content/Context;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 4134
    invoke-virtual {p0}, Lcom/osp/app/signin/jj;->isCancelled()Z

    move-result v2

    if-nez v2, :cond_0

    .line 4138
    iget-object v2, p0, Lcom/osp/app/signin/jj;->c:Lcom/osp/app/signin/SignInView;

    const v4, 0x7f0c0121

    invoke-virtual {v2, v4}, Lcom/osp/app/signin/SignInView;->findViewById(I)Landroid/view/View;

    move-result-object v2

    move-object v0, v2

    check-cast v0, Landroid/widget/EditText;

    move-object v7, v0

    invoke-static {}, Lcom/msc/c/g;->a()Lcom/msc/c/g;

    iget-object v2, p0, Lcom/osp/app/signin/jj;->c:Lcom/osp/app/signin/SignInView;

    invoke-static {v2}, Lcom/msc/c/e;->d(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v5

    invoke-static {}, Lcom/osp/common/util/i;->a()Lcom/osp/common/util/i;

    iget-object v2, p0, Lcom/osp/app/signin/jj;->c:Lcom/osp/app/signin/SignInView;

    invoke-static {v2}, Lcom/osp/common/util/i;->f(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v6

    iget-object v2, p0, Lcom/osp/app/signin/jj;->c:Lcom/osp/app/signin/SignInView;

    iget-object v4, p0, Lcom/osp/app/signin/jj;->c:Lcom/osp/app/signin/SignInView;

    invoke-static {v4}, Lcom/osp/app/signin/SignInView;->y(Lcom/osp/app/signin/SignInView;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v7}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v7

    sget-object v8, Ljava/util/Locale;->ENGLISH:Ljava/util/Locale;

    invoke-virtual {v7, v8}, Ljava/lang/String;->toLowerCase(Ljava/util/Locale;)Ljava/lang/String;

    move-object v7, p0

    invoke-static/range {v2 .. v7}, Lcom/msc/c/g;->b(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/msc/b/h;)J

    move-result-wide v2

    iput-wide v2, p0, Lcom/osp/app/signin/jj;->h:J

    iget-wide v2, p0, Lcom/osp/app/signin/jj;->h:J

    invoke-virtual {p0, v2, v3}, Lcom/osp/app/signin/jj;->a(J)V

    iget-wide v2, p0, Lcom/osp/app/signin/jj;->h:J

    const-string v4, "from_xml"

    invoke-virtual {p0, v2, v3, v4}, Lcom/osp/app/signin/jj;->a(JLjava/lang/String;)V

    iget-wide v2, p0, Lcom/osp/app/signin/jj;->h:J

    sget-object v4, Lcom/msc/b/g;->d:Lcom/msc/b/g;

    invoke-static {v2, v3, v4}, Lcom/msc/c/g;->a(JLcom/msc/b/g;)V

    goto/16 :goto_0

    .line 4139
    :cond_7
    iget-wide v6, p0, Lcom/osp/app/signin/jj;->h:J

    cmp-long v2, v2, v6

    if-nez v2, :cond_0

    .line 4141
    iget-object v2, p0, Lcom/osp/app/signin/jj;->c:Lcom/osp/app/signin/SignInView;

    const/4 v3, 0x0

    invoke-static {v2, v3}, Lcom/osp/app/signin/SignInView;->b(Lcom/osp/app/signin/SignInView;Ljava/lang/String;)Ljava/lang/String;

    .line 4142
    invoke-static {}, Lcom/msc/c/h;->a()Lcom/msc/c/h;

    invoke-static {v4}, Lcom/msc/c/h;->g(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/osp/app/signin/jj;->d:Ljava/lang/String;
    :try_end_4
    .catch Ljava/lang/Exception; {:try_start_4 .. :try_end_4} :catch_0
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    goto/16 :goto_0
.end method

.method protected final a(Ljava/lang/Boolean;)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 4058
    invoke-super {p0, p1}, Lcom/msc/c/b;->a(Ljava/lang/Boolean;)V

    .line 4059
    iget-object v0, p0, Lcom/osp/app/signin/jj;->b:Lcom/msc/c/f;

    if-eqz v0, :cond_0

    .line 4061
    invoke-virtual {p0, v2}, Lcom/osp/app/signin/jj;->a(Z)V

    .line 4062
    iget-object v0, p0, Lcom/osp/app/signin/jj;->c:Lcom/osp/app/signin/SignInView;

    invoke-static {v0}, Lcom/osp/app/signin/SignInView;->a(Lcom/osp/app/signin/SignInView;)V

    .line 4073
    :goto_0
    return-void

    .line 4065
    :cond_0
    const-string v0, "true"

    iget-object v1, p0, Lcom/osp/app/signin/jj;->d:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 4067
    iget-object v0, p0, Lcom/osp/app/signin/jj;->c:Lcom/osp/app/signin/SignInView;

    invoke-static {v0}, Lcom/osp/app/signin/SignInView;->b(Lcom/osp/app/signin/SignInView;)V

    goto :goto_0

    .line 4070
    :cond_1
    invoke-virtual {p0, v2}, Lcom/osp/app/signin/jj;->a(Z)V

    .line 4071
    iget-object v0, p0, Lcom/osp/app/signin/jj;->c:Lcom/osp/app/signin/SignInView;

    invoke-static {v0}, Lcom/osp/app/signin/SignInView;->a(Lcom/osp/app/signin/SignInView;)V

    goto :goto_0
.end method

.method protected final d()V
    .locals 1

    .prologue
    .line 4046
    invoke-super {p0}, Lcom/msc/c/b;->d()V

    .line 4047
    iget-object v0, p0, Lcom/osp/app/signin/jj;->c:Lcom/osp/app/signin/SignInView;

    invoke-static {v0}, Lcom/osp/app/signin/SignInView;->a(Lcom/osp/app/signin/SignInView;)V

    .line 4048
    return-void
.end method

.method protected final synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 4007
    invoke-virtual {p0}, Lcom/osp/app/signin/jj;->a()Ljava/lang/Boolean;

    move-result-object v0

    return-object v0
.end method

.method protected final synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 4007
    check-cast p1, Ljava/lang/Boolean;

    invoke-virtual {p0, p1}, Lcom/osp/app/signin/jj;->a(Ljava/lang/Boolean;)V

    return-void
.end method
