.class final Lcom/osp/app/signin/ph;
.super Ljava/lang/Object;
.source "UserValidateCheck.java"

# interfaces
.implements Landroid/widget/TextView$OnEditorActionListener;


# instance fields
.field final synthetic a:Lcom/osp/app/signin/UserValidateCheck;


# direct methods
.method constructor <init>(Lcom/osp/app/signin/UserValidateCheck;)V
    .locals 0

    .prologue
    .line 1084
    iput-object p1, p0, Lcom/osp/app/signin/ph;->a:Lcom/osp/app/signin/UserValidateCheck;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onEditorAction(Landroid/widget/TextView;ILandroid/view/KeyEvent;)Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 1088
    const/4 v1, 0x6

    if-ne p2, v1, :cond_0

    .line 1090
    iget-object v1, p0, Lcom/osp/app/signin/ph;->a:Lcom/osp/app/signin/UserValidateCheck;

    invoke-static {v1}, Lcom/osp/app/signin/UserValidateCheck;->e(Lcom/osp/app/signin/UserValidateCheck;)Landroid/widget/EditText;

    move-result-object v1

    invoke-virtual {v1}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v1

    invoke-interface {v1}, Landroid/text/Editable;->length()I

    move-result v1

    if-nez v1, :cond_1

    .line 1092
    invoke-static {}, Lcom/osp/app/util/ap;->a()Lcom/osp/app/util/ap;

    iget-object v1, p0, Lcom/osp/app/signin/ph;->a:Lcom/osp/app/signin/UserValidateCheck;

    const v2, 0x7f090177

    invoke-static {v1, v2, v0}, Lcom/osp/app/util/ap;->a(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    .line 1093
    const/4 v0, 0x1

    .line 1126
    :cond_0
    :goto_0
    return v0

    .line 1094
    :cond_1
    iget-object v1, p0, Lcom/osp/app/signin/ph;->a:Lcom/osp/app/signin/UserValidateCheck;

    invoke-static {v1}, Lcom/osp/app/signin/UserValidateCheck;->e(Lcom/osp/app/signin/UserValidateCheck;)Landroid/widget/EditText;

    move-result-object v1

    invoke-virtual {v1}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v1

    invoke-interface {v1}, Landroid/text/Editable;->length()I

    move-result v1

    if-lez v1, :cond_0

    .line 1111
    iget-object v1, p0, Lcom/osp/app/signin/ph;->a:Lcom/osp/app/signin/UserValidateCheck;

    invoke-static {v1}, Lcom/osp/app/signin/UserValidateCheck;->k(Lcom/osp/app/signin/UserValidateCheck;)Landroid/app/AlertDialog;

    move-result-object v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/osp/app/signin/ph;->a:Lcom/osp/app/signin/UserValidateCheck;

    invoke-static {v1}, Lcom/osp/app/signin/UserValidateCheck;->k(Lcom/osp/app/signin/UserValidateCheck;)Landroid/app/AlertDialog;

    move-result-object v1

    invoke-virtual {v1}, Landroid/app/AlertDialog;->isShowing()Z

    move-result v1

    if-eqz v1, :cond_2

    .line 1113
    iget-object v1, p0, Lcom/osp/app/signin/ph;->a:Lcom/osp/app/signin/UserValidateCheck;

    invoke-static {v1}, Lcom/osp/app/signin/UserValidateCheck;->k(Lcom/osp/app/signin/UserValidateCheck;)Landroid/app/AlertDialog;

    move-result-object v1

    const/4 v2, -0x1

    invoke-virtual {v1, v2}, Landroid/app/AlertDialog;->getButton(I)Landroid/widget/Button;

    move-result-object v1

    invoke-virtual {v1}, Landroid/widget/Button;->performClick()Z

    goto :goto_0

    .line 1114
    :cond_2
    iget-object v1, p0, Lcom/osp/app/signin/ph;->a:Lcom/osp/app/signin/UserValidateCheck;

    invoke-static {v1}, Lcom/osp/app/signin/UserValidateCheck;->l(Lcom/osp/app/signin/UserValidateCheck;)Lcom/osp/app/signin/BottomSoftkeyLinearLayout;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 1116
    invoke-static {}, Lcom/osp/device/b;->b()Lcom/osp/device/b;

    move-result-object v1

    iget-object v2, p0, Lcom/osp/app/signin/ph;->a:Lcom/osp/app/signin/UserValidateCheck;

    invoke-virtual {v1, v2}, Lcom/osp/device/b;->l(Landroid/content/Context;)Z

    move-result v1

    if-eqz v1, :cond_3

    .line 1118
    iget-object v1, p0, Lcom/osp/app/signin/ph;->a:Lcom/osp/app/signin/UserValidateCheck;

    invoke-static {v1}, Lcom/osp/app/signin/UserValidateCheck;->l(Lcom/osp/app/signin/UserValidateCheck;)Lcom/osp/app/signin/BottomSoftkeyLinearLayout;

    move-result-object v1

    invoke-virtual {v1}, Lcom/osp/app/signin/BottomSoftkeyLinearLayout;->d()Landroid/view/View;

    move-result-object v1

    invoke-virtual {v1}, Landroid/view/View;->performClick()Z

    goto :goto_0

    .line 1121
    :cond_3
    iget-object v1, p0, Lcom/osp/app/signin/ph;->a:Lcom/osp/app/signin/UserValidateCheck;

    invoke-static {v1}, Lcom/osp/app/signin/UserValidateCheck;->l(Lcom/osp/app/signin/UserValidateCheck;)Lcom/osp/app/signin/BottomSoftkeyLinearLayout;

    move-result-object v1

    invoke-virtual {v1}, Lcom/osp/app/signin/BottomSoftkeyLinearLayout;->e()Landroid/view/View;

    move-result-object v1

    invoke-virtual {v1}, Landroid/view/View;->performClick()Z

    goto :goto_0
.end method
