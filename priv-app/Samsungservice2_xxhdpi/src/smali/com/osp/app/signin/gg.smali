.class final Lcom/osp/app/signin/gg;
.super Lcom/msc/c/b;
.source "OspReceiver.java"


# instance fields
.field private c:J

.field private d:Ljava/lang/String;

.field private e:Ljava/lang/String;

.field private f:Z


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 2

    .prologue
    .line 682
    invoke-direct {p0, p1}, Lcom/msc/c/b;-><init>(Landroid/content/Context;)V

    .line 674
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/osp/app/signin/gg;->f:Z

    .line 683
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v0, "OR"

    const-string v1, "GetMyCountryZoneTask"

    invoke-static {v0, v1}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 684
    invoke-virtual {p0}, Lcom/osp/app/signin/gg;->c()V

    .line 686
    return-void
.end method

.method private h()V
    .locals 3

    .prologue
    .line 812
    iget-object v0, p0, Lcom/osp/app/signin/gg;->a:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/Context;

    .line 813
    if-nez v0, :cond_0

    .line 822
    :goto_0
    return-void

    .line 818
    :cond_0
    invoke-static {}, Lcom/msc/c/g;->a()Lcom/msc/c/g;

    .line 819
    invoke-static {v0, p0}, Lcom/msc/c/g;->b(Landroid/content/Context;Lcom/msc/b/h;)J

    move-result-wide v0

    iput-wide v0, p0, Lcom/osp/app/signin/gg;->c:J

    .line 820
    iget-wide v0, p0, Lcom/osp/app/signin/gg;->c:J

    invoke-virtual {p0, v0, v1}, Lcom/osp/app/signin/gg;->a(J)V

    .line 821
    iget-wide v0, p0, Lcom/osp/app/signin/gg;->c:J

    sget-object v2, Lcom/msc/b/g;->b:Lcom/msc/b/g;

    invoke-static {v0, v1, v2}, Lcom/msc/c/g;->a(JLcom/msc/b/g;)V

    goto :goto_0
.end method


# virtual methods
.method protected final varargs a()Ljava/lang/Boolean;
    .locals 1

    .prologue
    .line 695
    invoke-direct {p0}, Lcom/osp/app/signin/gg;->h()V

    .line 696
    const/4 v0, 0x0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    return-object v0
.end method

.method public final declared-synchronized a(Lcom/msc/b/c;)V
    .locals 6

    .prologue
    .line 714
    monitor-enter p0

    :try_start_0
    invoke-super {p0, p1}, Lcom/msc/c/b;->a(Lcom/msc/b/c;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 716
    if-nez p1, :cond_1

    .line 781
    :cond_0
    :goto_0
    monitor-exit p0

    return-void

    .line 721
    :cond_1
    :try_start_1
    invoke-virtual {p1}, Lcom/msc/b/c;->a()J

    move-result-wide v0

    .line 722
    invoke-virtual {p1}, Lcom/msc/b/c;->e()Ljava/lang/String;

    move-result-object v2

    .line 724
    iget-wide v4, p0, Lcom/osp/app/signin/gg;->c:J
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    cmp-long v0, v0, v4

    if-nez v0, :cond_0

    .line 728
    :try_start_2
    invoke-static {}, Lcom/msc/c/h;->a()Lcom/msc/c/h;

    invoke-static {v2}, Lcom/msc/c/h;->w(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 729
    if-eqz v1, :cond_3

    .line 732
    iget-object v0, p0, Lcom/osp/app/signin/gg;->a:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/Context;

    .line 733
    if-eqz v0, :cond_0

    .line 738
    invoke-static {}, Lcom/osp/common/util/i;->a()Lcom/osp/common/util/i;

    invoke-static {v0}, Lcom/osp/common/util/i;->o(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v0

    .line 739
    invoke-static {}, Lcom/osp/common/util/i;->a()Lcom/osp/common/util/i;

    invoke-static {v0}, Lcom/osp/common/util/i;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 740
    if-eqz v2, :cond_2

    invoke-virtual {v2, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_2

    .line 742
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v3, "GetMyCountryZoneTask countryCodeFromNetworkMcc = ["

    invoke-direct {v1, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "]"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;)V

    .line 743
    iput-object v0, p0, Lcom/osp/app/signin/gg;->e:Ljava/lang/String;

    .line 753
    :goto_1
    iget-object v0, p0, Lcom/osp/app/signin/gg;->e:Ljava/lang/String;

    if-eqz v0, :cond_4

    iget-object v0, p0, Lcom/osp/app/signin/gg;->e:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v0

    if-lez v0, :cond_4

    .line 755
    iget-object v0, p0, Lcom/osp/app/signin/gg;->a:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/Context;

    iget-object v1, p0, Lcom/osp/app/signin/gg;->e:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/osp/app/util/ad;->e(Landroid/content/Context;Ljava/lang/String;)V

    .line 756
    const-string v0, "Success"

    iput-object v0, p0, Lcom/osp/app/signin/gg;->d:Ljava/lang/String;
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_0
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0

    .line 768
    :catch_0
    move-exception v0

    :try_start_3
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    .line 771
    iget-boolean v0, p0, Lcom/osp/app/signin/gg;->f:Z

    if-nez v0, :cond_6

    .line 773
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/osp/app/signin/gg;->f:Z

    .line 774
    invoke-direct {p0}, Lcom/osp/app/signin/gg;->h()V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    goto/16 :goto_0

    .line 714
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 746
    :cond_2
    :try_start_4
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v2, "GetMyCountryZoneTask countryCode = ["

    invoke-direct {v0, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, "]"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;)V

    .line 747
    invoke-static {}, Lcom/osp/common/util/i;->a()Lcom/osp/common/util/i;

    sget-object v0, Ljava/util/Locale;->ENGLISH:Ljava/util/Locale;

    invoke-virtual {v1, v0}, Ljava/lang/String;->toLowerCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/osp/common/util/i;->c(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/osp/app/signin/gg;->e:Ljava/lang/String;

    goto :goto_1

    .line 751
    :cond_3
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v0, "OR"

    const-string v1, "GetMyCountryZoneTask Success. but countryCode is null!!"

    invoke-static {v0, v1}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1

    .line 759
    :cond_4
    iget-boolean v0, p0, Lcom/osp/app/signin/gg;->f:Z

    if-nez v0, :cond_5

    .line 761
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/osp/app/signin/gg;->f:Z

    .line 762
    invoke-direct {p0}, Lcom/osp/app/signin/gg;->h()V

    goto/16 :goto_0

    .line 765
    :cond_5
    const-string v0, "Fail"

    iput-object v0, p0, Lcom/osp/app/signin/gg;->d:Ljava/lang/String;
    :try_end_4
    .catch Ljava/lang/Exception; {:try_start_4 .. :try_end_4} :catch_0
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    goto/16 :goto_0

    .line 777
    :cond_6
    :try_start_5
    const-string v0, "Fail"

    iput-object v0, p0, Lcom/osp/app/signin/gg;->d:Ljava/lang/String;
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    goto/16 :goto_0
.end method

.method protected final a(Ljava/lang/Boolean;)V
    .locals 3

    .prologue
    .line 701
    invoke-super {p0, p1}, Lcom/msc/c/b;->a(Ljava/lang/Boolean;)V

    .line 702
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v0, "OR"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "GetMyCountryZoneTask mcc = "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, Lcom/osp/app/signin/gg;->e:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 703
    const-string v0, "Success"

    iget-object v1, p0, Lcom/osp/app/signin/gg;->d:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 705
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v0, "OR"

    const-string v1, "GetMyCountryZoneTask Success"

    invoke-static {v0, v1}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 710
    :goto_0
    return-void

    .line 708
    :cond_0
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v0, "OR"

    const-string v1, "GetMyCountryZoneTask Fail"

    invoke-static {v0, v1}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public final b(Lcom/msc/b/c;)V
    .locals 4

    .prologue
    .line 785
    invoke-super {p0, p1}, Lcom/msc/c/b;->b(Lcom/msc/b/c;)V

    .line 787
    if-nez p1, :cond_1

    .line 805
    :cond_0
    :goto_0
    return-void

    .line 792
    :cond_1
    invoke-virtual {p1}, Lcom/msc/b/c;->a()J

    move-result-wide v0

    .line 794
    iget-wide v2, p0, Lcom/osp/app/signin/gg;->c:J

    cmp-long v0, v0, v2

    if-nez v0, :cond_0

    .line 796
    iget-boolean v0, p0, Lcom/osp/app/signin/gg;->f:Z

    if-nez v0, :cond_2

    .line 798
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/osp/app/signin/gg;->f:Z

    .line 799
    invoke-direct {p0}, Lcom/osp/app/signin/gg;->h()V

    goto :goto_0

    .line 802
    :cond_2
    const-string v0, "Fail"

    iput-object v0, p0, Lcom/osp/app/signin/gg;->d:Ljava/lang/String;

    goto :goto_0
.end method

.method protected final synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 657
    invoke-virtual {p0}, Lcom/osp/app/signin/gg;->a()Ljava/lang/Boolean;

    move-result-object v0

    return-object v0
.end method

.method protected final synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 657
    check-cast p1, Ljava/lang/Boolean;

    invoke-virtual {p0, p1}, Lcom/osp/app/signin/gg;->a(Ljava/lang/Boolean;)V

    return-void
.end method

.method protected final onPreExecute()V
    .locals 0

    .prologue
    .line 690
    invoke-super {p0}, Lcom/msc/c/b;->onPreExecute()V

    .line 691
    return-void
.end method
