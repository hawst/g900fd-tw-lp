.class public Lcom/osp/app/signin/AccountView;
.super Lcom/osp/app/util/BaseActivity;
.source "AccountView.java"


# static fields
.field private static aN:I


# instance fields
.field private A:Z

.field private B:Z

.field private C:Ljava/lang/String;

.field private D:Lcom/osp/app/signin/bd;

.field private E:Lcom/osp/app/signin/be;

.field private F:Lcom/osp/app/signin/ba;

.field private G:Lcom/osp/app/signin/ay;

.field private H:Ljava/lang/String;

.field private I:Ljava/lang/String;

.field private J:Landroid/content/Intent;

.field private K:Z

.field private L:Ljava/lang/String;

.field private M:Lcom/msc/a/g;

.field private N:Ljava/lang/String;

.field private O:Ljava/lang/String;

.field private P:Z

.field private Q:Z

.field private R:Ljava/lang/String;

.field private S:Ljava/lang/String;

.field private T:Ljava/lang/String;

.field private U:Z

.field private V:I

.field private W:Ljava/lang/String;

.field private X:Ljava/lang/String;

.field private Y:Ljava/lang/String;

.field private Z:J

.field final a:Landroid/view/View$OnClickListener;

.field private aA:Ljava/lang/String;

.field private aB:Z

.field private aC:Z

.field private aD:Landroid/app/AlertDialog;

.field private aE:Lcom/osp/app/signin/bb;

.field private aF:Z

.field private aG:Ljava/lang/String;

.field private aH:Ljava/util/Timer;

.field private final aI:J

.field private aJ:Landroid/support/v4/view/ViewPager;

.field private aK:I

.field private final aL:Ljava/util/ArrayList;

.field private aM:Lcom/osp/app/signin/h;

.field private aO:Landroid/widget/LinearLayout;

.field private aP:Lcom/osp/app/signin/BottomSoftkeyLinearLayout;

.field private aQ:Z

.field private aR:Landroid/animation/AnimatorSet;

.field private final aS:Landroid/view/ViewTreeObserver$OnPreDrawListener;

.field private final aT:Landroid/view/View$OnClickListener;

.field private aa:Landroid/app/ProgressDialog;

.field private ab:Landroid/app/ProgressDialog;

.field private ac:Ljava/lang/String;

.field private ad:Z

.field private ae:Landroid/os/Bundle;

.field private af:Z

.field private ag:Ljava/lang/String;

.field private ah:Z

.field private ai:Ljava/lang/String;

.field private aj:Z

.field private ak:Lcom/osp/app/signin/fv;

.field private al:Lcom/osp/app/signin/cd;

.field private am:Z

.field private an:Z

.field private ao:Landroid/net/Uri;

.field private ap:Z

.field private aq:Z

.field private ar:Z

.field private as:Lcom/osp/app/signin/bc;

.field private at:Ljava/lang/String;

.field private au:Ljava/lang/String;

.field private av:Lcom/osp/app/signin/az;

.field private aw:Z

.field private ax:Z

.field private ay:Z

.field private az:Z

.field final b:Landroid/view/View$OnClickListener;

.field private c:Z

.field private d:I

.field private e:Z

.field private final f:I

.field private final y:I

.field private z:Lcom/osp/app/signin/ax;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 639
    const/4 v0, 0x0

    sput v0, Lcom/osp/app/signin/AccountView;->aN:I

    return-void
.end method

.method public constructor <init>()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v1, 0x0

    const/4 v2, 0x0

    .line 142
    invoke-direct {p0}, Lcom/osp/app/util/BaseActivity;-><init>()V

    .line 203
    iput-boolean v2, p0, Lcom/osp/app/signin/AccountView;->c:Z

    .line 214
    iput v2, p0, Lcom/osp/app/signin/AccountView;->f:I

    .line 219
    iput v3, p0, Lcom/osp/app/signin/AccountView;->y:I

    .line 224
    iput-object v1, p0, Lcom/osp/app/signin/AccountView;->z:Lcom/osp/app/signin/ax;

    .line 246
    iput-boolean v2, p0, Lcom/osp/app/signin/AccountView;->B:Z

    .line 250
    iput-object v1, p0, Lcom/osp/app/signin/AccountView;->C:Ljava/lang/String;

    .line 259
    iput-object v1, p0, Lcom/osp/app/signin/AccountView;->E:Lcom/osp/app/signin/be;

    .line 279
    iput-object v1, p0, Lcom/osp/app/signin/AccountView;->H:Ljava/lang/String;

    .line 283
    iput-object v1, p0, Lcom/osp/app/signin/AccountView;->I:Ljava/lang/String;

    .line 292
    iput-object v1, p0, Lcom/osp/app/signin/AccountView;->J:Landroid/content/Intent;

    .line 296
    iput-boolean v2, p0, Lcom/osp/app/signin/AccountView;->K:Z

    .line 303
    iput-object v1, p0, Lcom/osp/app/signin/AccountView;->L:Ljava/lang/String;

    .line 327
    iput-object v1, p0, Lcom/osp/app/signin/AccountView;->N:Ljava/lang/String;

    .line 339
    iput-boolean v2, p0, Lcom/osp/app/signin/AccountView;->P:Z

    .line 344
    iput-boolean v2, p0, Lcom/osp/app/signin/AccountView;->Q:Z

    .line 351
    iput-object v1, p0, Lcom/osp/app/signin/AccountView;->R:Ljava/lang/String;

    .line 361
    iput-object v1, p0, Lcom/osp/app/signin/AccountView;->S:Ljava/lang/String;

    .line 367
    iput-object v1, p0, Lcom/osp/app/signin/AccountView;->T:Ljava/lang/String;

    .line 373
    iput-boolean v2, p0, Lcom/osp/app/signin/AccountView;->U:Z

    .line 378
    const/4 v0, -0x1

    iput v0, p0, Lcom/osp/app/signin/AccountView;->V:I

    .line 427
    iput-object v1, p0, Lcom/osp/app/signin/AccountView;->ac:Ljava/lang/String;

    .line 432
    iput-boolean v2, p0, Lcom/osp/app/signin/AccountView;->ad:Z

    .line 442
    iput-boolean v2, p0, Lcom/osp/app/signin/AccountView;->af:Z

    .line 529
    iput-boolean v2, p0, Lcom/osp/app/signin/AccountView;->an:Z

    .line 539
    iput-boolean v2, p0, Lcom/osp/app/signin/AccountView;->ap:Z

    .line 541
    iput-boolean v3, p0, Lcom/osp/app/signin/AccountView;->aq:Z

    .line 543
    iput-boolean v2, p0, Lcom/osp/app/signin/AccountView;->ar:Z

    .line 548
    iput-object v1, p0, Lcom/osp/app/signin/AccountView;->as:Lcom/osp/app/signin/bc;

    .line 553
    iput-object v1, p0, Lcom/osp/app/signin/AccountView;->at:Ljava/lang/String;

    .line 557
    iput-object v1, p0, Lcom/osp/app/signin/AccountView;->au:Ljava/lang/String;

    .line 562
    iput-object v1, p0, Lcom/osp/app/signin/AccountView;->av:Lcom/osp/app/signin/az;

    .line 564
    iput-boolean v2, p0, Lcom/osp/app/signin/AccountView;->aw:Z

    .line 566
    iput-boolean v2, p0, Lcom/osp/app/signin/AccountView;->ax:Z

    .line 567
    iput-boolean v2, p0, Lcom/osp/app/signin/AccountView;->ay:Z

    .line 568
    iput-boolean v2, p0, Lcom/osp/app/signin/AccountView;->az:Z

    .line 575
    iput-boolean v2, p0, Lcom/osp/app/signin/AccountView;->aB:Z

    .line 580
    iput-boolean v2, p0, Lcom/osp/app/signin/AccountView;->aC:Z

    .line 590
    iput-object v1, p0, Lcom/osp/app/signin/AccountView;->aE:Lcom/osp/app/signin/bb;

    .line 595
    iput-boolean v2, p0, Lcom/osp/app/signin/AccountView;->aF:Z

    .line 600
    iput-object v1, p0, Lcom/osp/app/signin/AccountView;->aG:Ljava/lang/String;

    .line 605
    iput-object v1, p0, Lcom/osp/app/signin/AccountView;->aH:Ljava/util/Timer;

    .line 610
    const-wide/32 v0, 0xea60

    iput-wide v0, p0, Lcom/osp/app/signin/AccountView;->aI:J

    .line 620
    iput v2, p0, Lcom/osp/app/signin/AccountView;->aK:I

    .line 633
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/osp/app/signin/AccountView;->aL:Ljava/util/ArrayList;

    .line 645
    new-instance v0, Lcom/osp/app/signin/l;

    invoke-direct {v0, p0}, Lcom/osp/app/signin/l;-><init>(Lcom/osp/app/signin/AccountView;)V

    iput-object v0, p0, Lcom/osp/app/signin/AccountView;->a:Landroid/view/View$OnClickListener;

    .line 775
    new-instance v0, Lcom/osp/app/signin/z;

    invoke-direct {v0, p0}, Lcom/osp/app/signin/z;-><init>(Lcom/osp/app/signin/AccountView;)V

    iput-object v0, p0, Lcom/osp/app/signin/AccountView;->b:Landroid/view/View$OnClickListener;

    .line 792
    iput-boolean v2, p0, Lcom/osp/app/signin/AccountView;->aQ:Z

    .line 795
    new-instance v0, Lcom/osp/app/signin/am;

    invoke-direct {v0, p0}, Lcom/osp/app/signin/am;-><init>(Lcom/osp/app/signin/AccountView;)V

    iput-object v0, p0, Lcom/osp/app/signin/AccountView;->aS:Landroid/view/ViewTreeObserver$OnPreDrawListener;

    .line 9720
    new-instance v0, Lcom/osp/app/signin/ai;

    invoke-direct {v0, p0}, Lcom/osp/app/signin/ai;-><init>(Lcom/osp/app/signin/AccountView;)V

    iput-object v0, p0, Lcom/osp/app/signin/AccountView;->aT:Landroid/view/View$OnClickListener;

    .line 10929
    return-void
.end method

.method static synthetic A(Lcom/osp/app/signin/AccountView;)Lcom/osp/app/signin/ba;
    .locals 1

    .prologue
    .line 142
    iget-object v0, p0, Lcom/osp/app/signin/AccountView;->F:Lcom/osp/app/signin/ba;

    return-object v0
.end method

.method private A()V
    .locals 3

    .prologue
    .line 3222
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    .line 3223
    const-class v1, Lcom/osp/app/signin/AgreeToDisclaimerView;

    invoke-virtual {v0, p0, v1}, Landroid/content/Intent;->setClass(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;

    .line 3224
    const-string v1, "client_id"

    iget-object v2, p0, Lcom/osp/app/signin/AccountView;->H:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 3225
    const-string v1, "client_secret"

    iget-object v2, p0, Lcom/osp/app/signin/AccountView;->I:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 3227
    const/16 v1, 0xd3

    invoke-virtual {p0, v0, v1}, Lcom/osp/app/signin/AccountView;->startActivityForResult(Landroid/content/Intent;I)V

    .line 3228
    return-void
.end method

.method static synthetic B(Lcom/osp/app/signin/AccountView;)Landroid/app/ProgressDialog;
    .locals 1

    .prologue
    .line 142
    iget-object v0, p0, Lcom/osp/app/signin/AccountView;->aa:Landroid/app/ProgressDialog;

    return-object v0
.end method

.method private B()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 4050
    iget-object v0, p0, Lcom/osp/app/signin/AccountView;->ak:Lcom/osp/app/signin/fv;

    if-eqz v0, :cond_1

    .line 4052
    iget-object v0, p0, Lcom/osp/app/signin/AccountView;->ak:Lcom/osp/app/signin/fv;

    invoke-virtual {v0}, Lcom/osp/app/signin/fv;->k()Landroid/graphics/Bitmap;

    move-result-object v0

    .line 4053
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->isRecycled()Z

    move-result v1

    if-nez v1, :cond_0

    .line 4055
    invoke-virtual {v0}, Landroid/graphics/Bitmap;->recycle()V

    .line 4057
    :cond_0
    iget-object v0, p0, Lcom/osp/app/signin/AccountView;->ak:Lcom/osp/app/signin/fv;

    invoke-virtual {v0, v2}, Lcom/osp/app/signin/fv;->a(Landroid/graphics/Bitmap;)V

    .line 4058
    iput-object v2, p0, Lcom/osp/app/signin/AccountView;->ak:Lcom/osp/app/signin/fv;

    .line 4061
    :cond_1
    return-void
.end method

.method static synthetic C(Lcom/osp/app/signin/AccountView;)V
    .locals 1

    .prologue
    .line 142
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/osp/app/signin/AccountView;->a(Z)V

    return-void
.end method

.method private C()Z
    .locals 13

    .prologue
    const/4 v5, 0x0

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 4472
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v0, "AV"

    const-string v1, "initNonUIProcess START"

    invoke-static {v0, v1}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 4476
    invoke-static {p0}, Lcom/osp/app/util/ad;->d(Landroid/content/Context;)Z

    move-result v0

    if-nez v0, :cond_0

    invoke-static {}, Lcom/osp/app/signin/SamsungService;->a()Z

    move-result v0

    if-nez v0, :cond_0

    .line 4480
    :try_start_0
    new-instance v0, Lcom/osp/security/identity/d;

    invoke-direct {v0, p0}, Lcom/osp/security/identity/d;-><init>(Landroid/content/Context;)V

    .line 4481
    invoke-virtual {v0}, Lcom/osp/security/identity/d;->k()V
    :try_end_0
    .catch Lcom/osp/security/identity/IdentityException; {:try_start_0 .. :try_end_0} :catch_0

    .line 4488
    :goto_0
    :try_start_1
    new-instance v0, Lcom/osp/security/credential/a;

    invoke-direct {v0, p0}, Lcom/osp/security/credential/a;-><init>(Landroid/content/Context;)V

    .line 4489
    invoke-virtual {v0}, Lcom/osp/security/credential/a;->a()V
    :try_end_1
    .catch Lcom/osp/security/credential/CredentialException; {:try_start_1 .. :try_end_1} :catch_1

    .line 4496
    :goto_1
    :try_start_2
    new-instance v0, Lcom/osp/social/member/d;

    invoke-direct {v0, p0}, Lcom/osp/social/member/d;-><init>(Landroid/content/Context;)V

    .line 4497
    invoke-virtual {v0}, Lcom/osp/social/member/d;->a()V
    :try_end_2
    .catch Lcom/osp/social/member/MemberServiceException; {:try_start_2 .. :try_end_2} :catch_2

    .line 4503
    :goto_2
    invoke-static {p0}, Lcom/osp/app/util/ad;->r(Landroid/content/Context;)V

    .line 4509
    invoke-static {p0}, Lcom/msc/c/e;->a(Landroid/content/Context;)Z

    move-result v0

    if-eq v0, v3, :cond_0

    .line 4512
    new-instance v0, Lcom/osp/app/signin/t;

    invoke-direct {v0, p0}, Lcom/osp/app/signin/t;-><init>(Lcom/osp/app/signin/AccountView;)V

    invoke-virtual {p0, v0}, Lcom/osp/app/signin/AccountView;->runOnUiThread(Ljava/lang/Runnable;)V

    .line 4626
    :goto_3
    return v2

    .line 4482
    :catch_0
    move-exception v0

    invoke-virtual {v0}, Lcom/osp/security/identity/IdentityException;->printStackTrace()V

    goto :goto_0

    .line 4490
    :catch_1
    move-exception v0

    invoke-virtual {v0}, Lcom/osp/security/credential/CredentialException;->printStackTrace()V

    goto :goto_1

    .line 4498
    :catch_2
    move-exception v0

    invoke-virtual {v0}, Lcom/osp/social/member/MemberServiceException;->printStackTrace()V

    goto :goto_2

    .line 4530
    :cond_0
    invoke-static {p0}, Lcom/osp/app/util/ad;->m(Landroid/content/Context;)V

    .line 4533
    iget-boolean v0, p0, Lcom/osp/app/signin/AccountView;->aq:Z

    if-nez v0, :cond_1

    .line 4535
    invoke-static {}, Lcom/osp/device/b;->b()Lcom/osp/device/b;

    move-result-object v0

    invoke-virtual {v0}, Lcom/osp/device/b;->l()I

    move-result v0

    const/16 v1, 0x10

    if-le v0, v1, :cond_1

    invoke-static {p0}, Lcom/osp/app/util/ad;->d(Landroid/content/Context;)Z

    move-result v0

    if-nez v0, :cond_1

    invoke-static {p0}, Lcom/osp/app/util/ad;->a(Landroid/content/Context;)Z

    move-result v0

    if-ne v0, v3, :cond_1

    .line 4538
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/osp/app/signin/SamsungService$GetSignatureService;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 4539
    invoke-virtual {p0, v0}, Lcom/osp/app/signin/AccountView;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;

    .line 4543
    :cond_1
    const-string v0, "activity"

    invoke-virtual {p0, v0}, Lcom/osp/app/signin/AccountView;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/ActivityManager;

    .line 4544
    const/16 v1, 0x64

    invoke-virtual {v0, v1}, Landroid/app/ActivityManager;->getRunningTasks(I)Ljava/util/List;

    move-result-object v7

    .line 4551
    if-eqz v7, :cond_8

    .line 4553
    invoke-interface {v7}, Ljava/util/List;->size()I

    move-result v8

    move v4, v2

    move-object v1, v5

    .line 4554
    :goto_4
    if-ge v4, v8, :cond_7

    .line 4556
    invoke-interface {v7, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/app/ActivityManager$RunningTaskInfo;

    iget-object v1, v1, Landroid/app/ActivityManager$RunningTaskInfo;->baseActivity:Landroid/content/ComponentName;

    invoke-virtual {v1}, Landroid/content/ComponentName;->getClassName()Ljava/lang/String;

    move-result-object v9

    .line 4557
    invoke-interface {v7, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/app/ActivityManager$RunningTaskInfo;

    iget-object v1, v1, Landroid/app/ActivityManager$RunningTaskInfo;->baseActivity:Landroid/content/ComponentName;

    invoke-virtual {v1}, Landroid/content/ComponentName;->getPackageName()Ljava/lang/String;

    move-result-object v6

    .line 4558
    invoke-interface {v7, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/app/ActivityManager$RunningTaskInfo;

    iget-object v1, v1, Landroid/app/ActivityManager$RunningTaskInfo;->topActivity:Landroid/content/ComponentName;

    invoke-virtual {v1}, Landroid/content/ComponentName;->getClassName()Ljava/lang/String;

    move-result-object v10

    .line 4559
    invoke-interface {v7, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/app/ActivityManager$RunningTaskInfo;

    iget-object v1, v1, Landroid/app/ActivityManager$RunningTaskInfo;->topActivity:Landroid/content/ComponentName;

    invoke-virtual {v1}, Landroid/content/ComponentName;->getPackageName()Ljava/lang/String;

    move-result-object v1

    .line 4561
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    new-instance v11, Ljava/lang/StringBuilder;

    const-string v12, "AccountView::paramFromServiceApp baseActivityClassName : "

    invoke-direct {v11, v12}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v11, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v9}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;)V

    .line 4562
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    new-instance v9, Ljava/lang/StringBuilder;

    const-string v11, "AccountView::paramFromServiceApp baseActivityPackageName : "

    invoke-direct {v9, v11}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v9, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v9}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;)V

    .line 4563
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    new-instance v9, Ljava/lang/StringBuilder;

    const-string v11, "AccountView::paramFromServiceApp topActivityClassName : "

    invoke-direct {v9, v11}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v9}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;)V

    .line 4564
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    new-instance v9, Ljava/lang/StringBuilder;

    const-string v10, "AccountView::paramFromServiceApp topActivityPackageName : "

    invoke-direct {v9, v10}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v9, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;)V

    .line 4566
    if-eqz v4, :cond_4

    invoke-interface {v7, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/app/ActivityManager$RunningTaskInfo;

    iget-object v1, v1, Landroid/app/ActivityManager$RunningTaskInfo;->topActivity:Landroid/content/ComponentName;

    invoke-virtual {v1}, Landroid/content/ComponentName;->getPackageName()Ljava/lang/String;

    move-result-object v1

    const-string v9, "com.osp.app.signin"

    invoke-virtual {v1, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_4

    .line 4568
    invoke-interface {v7, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/app/ActivityManager$RunningTaskInfo;

    iget-object v1, v1, Landroid/app/ActivityManager$RunningTaskInfo;->baseActivity:Landroid/content/ComponentName;

    invoke-virtual {v1}, Landroid/content/ComponentName;->getPackageName()Ljava/lang/String;

    move-result-object v1

    .line 4574
    iget-object v4, p0, Lcom/osp/app/signin/AccountView;->D:Lcom/osp/app/signin/bd;

    if-eqz v4, :cond_7

    iget-object v4, p0, Lcom/osp/app/signin/AccountView;->D:Lcom/osp/app/signin/bd;

    invoke-virtual {v4}, Lcom/osp/app/signin/bd;->getStatus()Landroid/os/AsyncTask$Status;

    move-result-object v4

    sget-object v6, Landroid/os/AsyncTask$Status;->RUNNING:Landroid/os/AsyncTask$Status;

    if-ne v4, v6, :cond_7

    move-object v4, v1

    move v1, v3

    .line 4583
    :goto_5
    if-eqz v1, :cond_6

    .line 4585
    invoke-virtual {v0}, Landroid/app/ActivityManager;->getRunningAppProcesses()Ljava/util/List;

    move-result-object v6

    .line 4586
    if-eqz v6, :cond_2

    .line 4588
    invoke-interface {v6}, Ljava/util/List;->size()I

    move-result v7

    move v1, v2

    .line 4589
    :goto_6
    if-ge v1, v7, :cond_2

    .line 4591
    invoke-interface {v6, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/ActivityManager$RunningAppProcessInfo;

    iget-object v0, v0, Landroid/app/ActivityManager$RunningAppProcessInfo;->processName:Ljava/lang/String;

    invoke-virtual {v0, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_5

    .line 4593
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v0, "AccountView \t\tuse - activityes.get(i).processName =  \t\t"

    invoke-direct {v4, v0}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-interface {v6, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/ActivityManager$RunningAppProcessInfo;

    iget-object v0, v0, Landroid/app/ActivityManager$RunningAppProcessInfo;->processName:Ljava/lang/String;

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;)V

    .line 4599
    :cond_2
    iget v0, p0, Lcom/osp/app/signin/AccountView;->V:I

    const/4 v1, 0x3

    if-eq v0, v1, :cond_6

    invoke-static {p0}, Lcom/msc/c/c;->c(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v0

    if-nez v0, :cond_6

    .line 4601
    invoke-static {}, Lcom/osp/app/signin/SamsungService;->l()Lorg/apache/http/client/methods/HttpUriRequest;

    move-result-object v0

    if-eqz v0, :cond_6

    .line 4603
    invoke-static {}, Lcom/osp/app/util/ap;->a()Lcom/osp/app/util/ap;

    const v0, 0x7f090018

    invoke-virtual {p0, v0}, Lcom/osp/app/signin/AccountView;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-static {p0, v0, v2}, Lcom/osp/app/util/ap;->a(Landroid/content/Context;Ljava/lang/String;I)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    .line 4604
    iget-object v0, p0, Lcom/osp/app/signin/AccountView;->J:Landroid/content/Intent;

    invoke-direct {p0, v2, v0, v5}, Lcom/osp/app/signin/AccountView;->a(ILandroid/content/Intent;Landroid/content/Intent;)V

    move v1, v2

    .line 4611
    :goto_7
    const-string v0, "BOOT_NOTI"

    invoke-virtual {p0, v0, v2}, Lcom/osp/app/signin/AccountView;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v4

    .line 4612
    const-string v0, "NOTI_DISPLAY"

    invoke-interface {v4, v0, v2}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v0

    .line 4614
    if-ne v0, v3, :cond_3

    .line 4616
    const-string v0, "notification"

    invoke-virtual {p0, v0}, Lcom/osp/app/signin/AccountView;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/NotificationManager;

    .line 4617
    const v2, 0x132df82

    invoke-virtual {v0, v2}, Landroid/app/NotificationManager;->cancel(I)V

    .line 4619
    invoke-interface {v4}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    .line 4620
    const-string v2, "NOTI_DISPLAY"

    const/4 v3, 0x2

    invoke-interface {v0, v2, v3}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    .line 4621
    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 4624
    :cond_3
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v0, "AV"

    const-string v2, "initNonUIProcess END"

    invoke-static {v0, v2}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;)V

    move v2, v1

    .line 4626
    goto/16 :goto_3

    .line 4554
    :cond_4
    add-int/lit8 v1, v4, 0x1

    move v4, v1

    move-object v1, v6

    goto/16 :goto_4

    .line 4589
    :cond_5
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto/16 :goto_6

    :cond_6
    move v1, v3

    goto :goto_7

    :cond_7
    move-object v4, v1

    move v1, v2

    goto/16 :goto_5

    :cond_8
    move-object v4, v5

    move v1, v2

    goto/16 :goto_5
.end method

.method static synthetic D(Lcom/osp/app/signin/AccountView;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 142
    iget-object v0, p0, Lcom/osp/app/signin/AccountView;->T:Ljava/lang/String;

    return-object v0
.end method

.method private D()V
    .locals 6

    .prologue
    const/4 v5, 0x0

    const/4 v4, 0x1

    .line 6998
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    .line 6999
    const-class v1, Lcom/osp/app/signin/SignUpView;

    invoke-virtual {v0, p0, v1}, Landroid/content/Intent;->setClass(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;

    .line 7001
    const-string v1, "client_id"

    iget-object v2, p0, Lcom/osp/app/signin/AccountView;->H:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 7002
    const-string v1, "client_secret"

    iget-object v2, p0, Lcom/osp/app/signin/AccountView;->I:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 7003
    const-string v1, "account_mode"

    iget-object v2, p0, Lcom/osp/app/signin/AccountView;->C:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 7004
    const-string v1, "MODE"

    iget-object v2, p0, Lcom/osp/app/signin/AccountView;->S:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 7005
    const-string v1, "service_name"

    iget-object v2, p0, Lcom/osp/app/signin/AccountView;->N:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 7006
    const-string v1, "OSP_VER"

    iget-object v2, p0, Lcom/osp/app/signin/AccountView;->L:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 7007
    const-string v1, "serviceApp_type"

    invoke-virtual {v0, v1, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 7008
    const-string v1, "mypackage"

    iget-object v2, p0, Lcom/osp/app/signin/AccountView;->R:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 7009
    const-string v1, "key_request_id"

    iget-wide v2, p0, Lcom/osp/app/signin/AccountView;->Z:J

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;J)Landroid/content/Intent;

    .line 7010
    const-string v1, "BG_WhoareU"

    iget-object v2, p0, Lcom/osp/app/signin/AccountView;->T:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 7011
    const-string v1, "key_is_name_verified"

    iget-boolean v2, p0, Lcom/osp/app/signin/AccountView;->ad:Z

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 7012
    const-string v1, "country_code_mcc"

    iget-object v2, p0, Lcom/osp/app/signin/AccountView;->ac:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 7013
    const-string v1, "key_easy_signup_tnc_mode"

    iget-boolean v2, p0, Lcom/osp/app/signin/AccountView;->aF:Z

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 7014
    const-string v1, "key_easy_signup_phone_number"

    iget-object v2, p0, Lcom/osp/app/signin/AccountView;->aG:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 7016
    iget-boolean v1, p0, Lcom/osp/app/signin/AccountView;->ad:Z

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/osp/app/signin/AccountView;->ae:Landroid/os/Bundle;

    if-eqz v1, :cond_0

    .line 7018
    const-string v1, "information_after_name_validation"

    iget-object v2, p0, Lcom/osp/app/signin/AccountView;->ae:Landroid/os/Bundle;

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Bundle;)Landroid/content/Intent;

    .line 7021
    :cond_0
    iget-boolean v1, p0, Lcom/osp/app/signin/AccountView;->e:Z

    if-eqz v1, :cond_1

    .line 7023
    const-string v1, "new_add_account_mode"

    invoke-virtual {v0, v1, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 7026
    :cond_1
    const-string v1, "is_cancelable_just_one_activity"

    invoke-virtual {v0, v1, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 7031
    iget-boolean v1, p0, Lcom/osp/app/signin/AccountView;->aw:Z

    if-eqz v1, :cond_2

    .line 7033
    const-string v1, "key_google_account"

    invoke-virtual {v0, v1, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 7034
    iput-boolean v5, p0, Lcom/osp/app/signin/AccountView;->aw:Z

    .line 7037
    :cond_2
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "AccountView - showSignup - m_client_id : "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, Lcom/osp/app/signin/AccountView;->H:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;)V

    .line 7038
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "AccountView - showSignup - m_client_secret : "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, Lcom/osp/app/signin/AccountView;->I:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;)V

    .line 7039
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "AccountView - showSignup - m_account_mode : "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, Lcom/osp/app/signin/AccountView;->C:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;)V

    .line 7040
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "AccountView - showSignup - m_setting_mode : "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, Lcom/osp/app/signin/AccountView;->S:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;)V

    .line 7041
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "AccountView - showSignup - m_service_name : "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, Lcom/osp/app/signin/AccountView;->N:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;)V

    .line 7042
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "AccountView - showSignup - m_OspVersion : "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, Lcom/osp/app/signin/AccountView;->L:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;)V

    .line 7043
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v1, "AccountView - showSignup - m_ServiceAppType : 1"

    invoke-static {v1}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;)V

    .line 7044
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "AccountView - showSignup - m_Sourcepackage : "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, Lcom/osp/app/signin/AccountView;->R:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;)V

    .line 7046
    const-string v1, "tncAccepted"

    invoke-virtual {v0, v1, v5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 7047
    const-string v1, "450"

    iget-object v2, p0, Lcom/osp/app/signin/AccountView;->ac:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_3

    .line 7049
    const-string v1, "privacyAccepted"

    invoke-virtual {v0, v1, v5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 7080
    :cond_3
    iget-object v1, p0, Lcom/osp/app/signin/AccountView;->ak:Lcom/osp/app/signin/fv;

    if-eqz v1, :cond_4

    iget-object v1, p0, Lcom/osp/app/signin/AccountView;->ak:Lcom/osp/app/signin/fv;

    invoke-virtual {v1}, Lcom/osp/app/signin/fv;->j()Z

    move-result v1

    if-eqz v1, :cond_4

    .line 7082
    const-string v1, "key_welcome_content"

    iget-object v2, p0, Lcom/osp/app/signin/AccountView;->ak:Lcom/osp/app/signin/fv;

    invoke-virtual {v2}, Lcom/osp/app/signin/fv;->e()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 7083
    const-string v1, "key_marketingpopup_mode"

    invoke-virtual {v0, v1, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 7086
    :cond_4
    invoke-static {}, Lcom/osp/app/signin/SamsungService;->a()Z

    move-result v1

    if-eqz v1, :cond_5

    .line 7088
    const-string v1, "key_signup_log_is_skip_invoked"

    iget-boolean v2, p0, Lcom/osp/app/signin/AccountView;->aC:Z

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 7091
    :cond_5
    const/16 v1, 0xd8

    invoke-virtual {p0, v0, v1}, Lcom/osp/app/signin/AccountView;->startActivityForResult(Landroid/content/Intent;I)V

    .line 7092
    return-void
.end method

.method static synthetic E(Lcom/osp/app/signin/AccountView;)J
    .locals 2

    .prologue
    .line 142
    iget-wide v0, p0, Lcom/osp/app/signin/AccountView;->Z:J

    return-wide v0
.end method

.method private E()Z
    .locals 2

    .prologue
    .line 7099
    invoke-static {p0}, Landroid/accounts/AccountManager;->get(Landroid/content/Context;)Landroid/accounts/AccountManager;

    move-result-object v0

    .line 7100
    const-string v1, "com.google"

    invoke-virtual {v0, v1}, Landroid/accounts/AccountManager;->getAccountsByType(Ljava/lang/String;)[Landroid/accounts/Account;

    move-result-object v0

    .line 7102
    if-eqz v0, :cond_0

    array-length v0, v0

    if-lez v0, :cond_0

    .line 7104
    const/4 v0, 0x1

    .line 7106
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method static synthetic F(Lcom/osp/app/signin/AccountView;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 142
    iget-object v0, p0, Lcom/osp/app/signin/AccountView;->I:Ljava/lang/String;

    return-object v0
.end method

.method private F()V
    .locals 9

    .prologue
    const/4 v8, 0x2

    const/4 v5, 0x1

    const/4 v7, -0x2

    const/high16 v4, 0x3f800000    # 1.0f

    const/4 v6, 0x0

    .line 7491
    const v0, 0x7f0c0040

    invoke-virtual {p0, v0}, Lcom/osp/app/signin/AccountView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    .line 7492
    if-eqz v0, :cond_2

    .line 7494
    invoke-virtual {v0, v6}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 7496
    invoke-static {p0}, Lcom/osp/app/util/r;->h(Landroid/content/Context;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 7498
    invoke-virtual {v0}, Landroid/widget/LinearLayout;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v1

    check-cast v1, Landroid/widget/LinearLayout$LayoutParams;

    .line 7499
    invoke-virtual {p0}, Lcom/osp/app/signin/AccountView;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v2

    iget v2, v2, Landroid/content/res/Configuration;->orientation:I

    if-ne v2, v8, :cond_a

    .line 7501
    invoke-virtual {p0}, Lcom/osp/app/signin/AccountView;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f080112

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v2

    iput v2, v1, Landroid/widget/LinearLayout$LayoutParams;->topMargin:I

    .line 7506
    :goto_0
    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 7509
    :cond_0
    const v0, 0x7f0c0041

    invoke-virtual {p0, v0}, Lcom/osp/app/signin/AccountView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 7510
    if-eqz v0, :cond_2

    .line 7512
    invoke-static {p0}, Lcom/osp/app/util/an;->a(Lcom/osp/app/util/BaseActivity;)Lcom/osp/app/util/an;

    const v1, 0x7f0800f5

    invoke-static {v1}, Lcom/osp/app/util/an;->b(I)I

    move-result v1

    .line 7513
    invoke-virtual {p0}, Lcom/osp/app/signin/AccountView;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-virtual {v2, v1}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v1

    invoke-virtual {v0, v6, v1}, Landroid/widget/TextView;->setTextSize(IF)V

    .line 7514
    invoke-virtual {p0}, Lcom/osp/app/signin/AccountView;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-static {p0}, Lcom/osp/app/util/an;->a(Lcom/osp/app/util/BaseActivity;)Lcom/osp/app/util/an;

    const v2, 0x7f07001a

    invoke-static {v2}, Lcom/osp/app/util/an;->a(I)I

    move-result v2

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setTextColor(I)V

    .line 7516
    invoke-static {}, Lcom/osp/device/b;->b()Lcom/osp/device/b;

    move-result-object v1

    invoke-virtual {v1, p0}, Lcom/osp/device/b;->i(Landroid/content/Context;)Z

    move-result v1

    if-nez v1, :cond_2

    .line 7518
    invoke-static {p0}, Lcom/osp/app/util/x;->m(Landroid/content/Context;)Z

    move-result v1

    if-eqz v1, :cond_b

    .line 7520
    const/16 v1, 0xbf

    invoke-static {v1, v6, v6, v6}, Landroid/graphics/Color;->argb(IIII)I

    move-result v1

    invoke-virtual {v0, v4, v4, v4, v1}, Landroid/widget/TextView;->setShadowLayer(FFFI)V

    .line 7526
    :goto_1
    invoke-static {p0}, Lcom/osp/app/util/x;->l(Landroid/content/Context;)Z

    move-result v1

    if-nez v1, :cond_1

    invoke-static {p0}, Lcom/osp/app/util/x;->a(Landroid/content/Context;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 7528
    :cond_1
    const/4 v1, 0x0

    invoke-virtual {v0, v1, v6}, Landroid/widget/TextView;->setTypeface(Landroid/graphics/Typeface;I)V

    .line 7534
    :cond_2
    const v0, 0x7f0c0042

    invoke-virtual {p0, v0}, Lcom/osp/app/signin/AccountView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    .line 7535
    if-eqz v0, :cond_3

    .line 7537
    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 7540
    :cond_3
    const v0, 0x7f0c0043

    invoke-virtual {p0, v0}, Lcom/osp/app/signin/AccountView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ScrollView;

    .line 7541
    if-eqz v0, :cond_4

    .line 7543
    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/ScrollView;->setVisibility(I)V

    .line 7546
    :cond_4
    invoke-virtual {p0}, Lcom/osp/app/signin/AccountView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v0

    iget v0, v0, Landroid/content/res/Configuration;->orientation:I

    if-ne v0, v8, :cond_c

    move v4, v5

    .line 7591
    :goto_2
    const v0, 0x7f0c0079

    invoke-virtual {p0, v0}, Lcom/osp/app/signin/AccountView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    .line 7592
    if-eqz v0, :cond_6

    .line 7594
    invoke-virtual {v0}, Landroid/widget/LinearLayout;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v1

    check-cast v1, Landroid/widget/LinearLayout$LayoutParams;

    .line 7596
    if-eqz v4, :cond_d

    .line 7605
    invoke-static {}, Lcom/osp/device/b;->b()Lcom/osp/device/b;

    move-result-object v2

    invoke-virtual {v2, p0}, Lcom/osp/device/b;->i(Landroid/content/Context;)Z

    move-result v2

    if-nez v2, :cond_5

    .line 7607
    const v2, 0x7f0c0066

    invoke-virtual {p0, v2}, Lcom/osp/app/signin/AccountView;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/LinearLayout;

    .line 7609
    invoke-static {v2, v6, v6, v6, v6}, Lcom/msc/sa/c/d;->a(Landroid/view/View;IIII)V

    .line 7611
    invoke-virtual {v2}, Landroid/widget/LinearLayout;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v3

    check-cast v3, Landroid/widget/LinearLayout$LayoutParams;

    .line 7612
    iput v6, v3, Landroid/widget/LinearLayout$LayoutParams;->topMargin:I

    .line 7613
    invoke-virtual {v2, v3}, Landroid/widget/LinearLayout;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 7623
    :cond_5
    :goto_3
    invoke-static {v0, v1}, Lcom/msc/sa/c/d;->a(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    .line 7624
    invoke-virtual {v0, v6}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 7627
    :cond_6
    const v0, 0x7f0c007b

    invoke-virtual {p0, v0}, Lcom/osp/app/signin/AccountView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcom/osp/app/signin/AccountView;->aO:Landroid/widget/LinearLayout;

    .line 7628
    iget-object v0, p0, Lcom/osp/app/signin/AccountView;->aO:Landroid/widget/LinearLayout;

    if-eqz v0, :cond_7

    .line 7630
    iget-object v0, p0, Lcom/osp/app/signin/AccountView;->aO:Landroid/widget/LinearLayout;

    invoke-virtual {v0}, Landroid/widget/LinearLayout;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout$LayoutParams;

    .line 7631
    if-eqz v4, :cond_e

    .line 7633
    invoke-virtual {p0}, Lcom/osp/app/signin/AccountView;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f08003d

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    iput v1, v0, Landroid/widget/LinearLayout$LayoutParams;->topMargin:I

    .line 7634
    invoke-virtual {p0}, Lcom/osp/app/signin/AccountView;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f08003e

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    iput v1, v0, Landroid/widget/LinearLayout$LayoutParams;->bottomMargin:I

    .line 7640
    :goto_4
    iget-object v1, p0, Lcom/osp/app/signin/AccountView;->aO:Landroid/widget/LinearLayout;

    invoke-virtual {v1, v0}, Landroid/widget/LinearLayout;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 7642
    iget v0, p0, Lcom/osp/app/signin/AccountView;->aK:I

    if-gt v0, v5, :cond_7

    .line 7644
    iget-object v0, p0, Lcom/osp/app/signin/AccountView;->aO:Landroid/widget/LinearLayout;

    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setVisibility(I)V

    :cond_7
    move v0, v6

    .line 7650
    :goto_5
    iget v1, p0, Lcom/osp/app/signin/AccountView;->aK:I

    if-ge v0, v1, :cond_10

    new-instance v1, Landroid/widget/ImageView;

    invoke-direct {v1, p0}, Landroid/widget/ImageView;-><init>(Landroid/content/Context;)V

    iget-object v2, p0, Lcom/osp/app/signin/AccountView;->aO:Landroid/widget/LinearLayout;

    if-eqz v2, :cond_8

    iget-object v2, p0, Lcom/osp/app/signin/AccountView;->aO:Landroid/widget/LinearLayout;

    invoke-virtual {v2}, Landroid/widget/LinearLayout;->getChildCount()I

    move-result v2

    iget v3, p0, Lcom/osp/app/signin/AccountView;->aK:I

    if-ge v2, v3, :cond_8

    iget-object v2, p0, Lcom/osp/app/signin/AccountView;->aO:Landroid/widget/LinearLayout;

    invoke-virtual {v2, v1, v0}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;I)V

    :cond_8
    new-instance v2, Landroid/widget/LinearLayout$LayoutParams;

    invoke-direct {v2, v7, v7}, Landroid/widget/LinearLayout$LayoutParams;-><init>(II)V

    iget v3, p0, Lcom/osp/app/signin/AccountView;->aK:I

    add-int/lit8 v3, v3, -0x1

    if-ge v0, v3, :cond_9

    invoke-virtual {p0}, Lcom/osp/app/signin/AccountView;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v5, 0x7f08002a

    invoke-virtual {v3, v5}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v3

    float-to-int v3, v3

    invoke-virtual {v2, v6, v6, v3, v6}, Landroid/widget/LinearLayout$LayoutParams;->setMargins(IIII)V

    :cond_9
    invoke-virtual {v1, v2}, Landroid/widget/ImageView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    if-nez v0, :cond_f

    invoke-static {p0}, Lcom/osp/app/util/an;->a(Lcom/osp/app/util/BaseActivity;)Lcom/osp/app/util/an;

    const v2, 0x7f020094

    invoke-static {v2}, Lcom/osp/app/util/an;->a(I)I

    move-result v2

    invoke-virtual {v1, v2}, Landroid/widget/ImageView;->setImageResource(I)V

    :goto_6
    add-int/lit8 v0, v0, 0x1

    goto :goto_5

    .line 7504
    :cond_a
    invoke-virtual {p0}, Lcom/osp/app/signin/AccountView;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f080111

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v2

    iput v2, v1, Landroid/widget/LinearLayout$LayoutParams;->topMargin:I

    goto/16 :goto_0

    .line 7523
    :cond_b
    const/4 v1, 0x0

    const/high16 v2, 0x40000000    # 2.0f

    const/16 v3, 0xbf

    invoke-static {v3, v6, v6, v6}, Landroid/graphics/Color;->argb(IIII)I

    move-result v3

    invoke-virtual {v0, v4, v1, v2, v3}, Landroid/widget/TextView;->setShadowLayer(FFFI)V

    goto/16 :goto_1

    :cond_c
    move v4, v6

    .line 7546
    goto/16 :goto_2

    .line 7618
    :cond_d
    invoke-virtual {p0}, Lcom/osp/app/signin/AccountView;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f08002e

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v2

    iput v2, v1, Landroid/widget/LinearLayout$LayoutParams;->topMargin:I

    .line 7619
    invoke-virtual {p0}, Lcom/osp/app/signin/AccountView;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f08002c

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v2

    iput v2, v1, Landroid/widget/LinearLayout$LayoutParams;->leftMargin:I

    .line 7620
    invoke-virtual {p0}, Lcom/osp/app/signin/AccountView;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f08002d

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v2

    iput v2, v1, Landroid/widget/LinearLayout$LayoutParams;->rightMargin:I

    goto/16 :goto_3

    .line 7637
    :cond_e
    invoke-virtual {p0}, Lcom/osp/app/signin/AccountView;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f08002f

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    iput v1, v0, Landroid/widget/LinearLayout$LayoutParams;->topMargin:I

    .line 7638
    invoke-virtual {p0}, Lcom/osp/app/signin/AccountView;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f080030

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    iput v1, v0, Landroid/widget/LinearLayout$LayoutParams;->bottomMargin:I

    goto/16 :goto_4

    .line 7650
    :cond_f
    invoke-static {p0}, Lcom/osp/app/util/an;->a(Lcom/osp/app/util/BaseActivity;)Lcom/osp/app/util/an;

    const v2, 0x7f020092

    invoke-static {v2}, Lcom/osp/app/util/an;->a(I)I

    move-result v2

    invoke-virtual {v1, v2}, Landroid/widget/ImageView;->setImageResource(I)V

    goto :goto_6

    .line 7653
    :cond_10
    const v0, 0x7f0c007a

    invoke-virtual {p0, v0}, Lcom/osp/app/signin/AccountView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/support/v4/view/ViewPager;

    iput-object v0, p0, Lcom/osp/app/signin/AccountView;->aJ:Landroid/support/v4/view/ViewPager;

    .line 7654
    iget-object v0, p0, Lcom/osp/app/signin/AccountView;->aJ:Landroid/support/v4/view/ViewPager;

    if-eqz v0, :cond_11

    .line 7656
    iget-object v0, p0, Lcom/osp/app/signin/AccountView;->aJ:Landroid/support/v4/view/ViewPager;

    invoke-virtual {v0}, Landroid/support/v4/view/ViewPager;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v1

    .line 7657
    if-eqz v4, :cond_13

    .line 7659
    iget-object v0, p0, Lcom/osp/app/signin/AccountView;->aJ:Landroid/support/v4/view/ViewPager;

    invoke-virtual {v0}, Landroid/support/v4/view/ViewPager;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup$MarginLayoutParams;

    .line 7660
    invoke-virtual {p0}, Lcom/osp/app/signin/AccountView;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f08003a

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v2

    iput v2, v0, Landroid/view/ViewGroup$MarginLayoutParams;->topMargin:I

    .line 7661
    invoke-virtual {p0}, Lcom/osp/app/signin/AccountView;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f08003b

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v2

    iput v2, v0, Landroid/view/ViewGroup$MarginLayoutParams;->leftMargin:I

    .line 7662
    invoke-virtual {p0}, Lcom/osp/app/signin/AccountView;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f08003c

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v2

    iput v2, v0, Landroid/view/ViewGroup$MarginLayoutParams;->rightMargin:I

    .line 7663
    iget-object v2, p0, Lcom/osp/app/signin/AccountView;->aJ:Landroid/support/v4/view/ViewPager;

    invoke-static {v2, v0}, Lcom/msc/sa/c/d;->a(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    .line 7665
    invoke-static {}, Lcom/osp/device/b;->b()Lcom/osp/device/b;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/osp/device/b;->i(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_12

    .line 7667
    invoke-virtual {p0}, Lcom/osp/app/signin/AccountView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v2, 0x7f080038

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    iput v0, v1, Landroid/view/ViewGroup$LayoutParams;->height:I

    .line 7676
    :goto_7
    iget-object v0, p0, Lcom/osp/app/signin/AccountView;->aJ:Landroid/support/v4/view/ViewPager;

    invoke-virtual {v0, v1}, Landroid/support/v4/view/ViewPager;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 7677
    iget-object v0, p0, Lcom/osp/app/signin/AccountView;->aJ:Landroid/support/v4/view/ViewPager;

    new-instance v1, Lcom/osp/app/signin/bi;

    invoke-direct {v1, p0}, Lcom/osp/app/signin/bi;-><init>(Lcom/osp/app/signin/AccountView;)V

    invoke-virtual {v0, v1}, Landroid/support/v4/view/ViewPager;->a(Landroid/support/v4/view/aa;)V

    .line 7678
    iget-object v0, p0, Lcom/osp/app/signin/AccountView;->aJ:Landroid/support/v4/view/ViewPager;

    new-instance v1, Lcom/osp/app/signin/x;

    invoke-direct {v1, p0}, Lcom/osp/app/signin/x;-><init>(Lcom/osp/app/signin/AccountView;)V

    invoke-virtual {v0, v1}, Landroid/support/v4/view/ViewPager;->a(Landroid/support/v4/view/bo;)V

    .line 7742
    iget-object v0, p0, Lcom/osp/app/signin/AccountView;->aJ:Landroid/support/v4/view/ViewPager;

    invoke-virtual {v0}, Landroid/support/v4/view/ViewPager;->b()I

    move-result v0

    sget v1, Lcom/osp/app/signin/AccountView;->aN:I

    if-eq v0, v1, :cond_11

    .line 7744
    iget-object v0, p0, Lcom/osp/app/signin/AccountView;->aJ:Landroid/support/v4/view/ViewPager;

    sget v1, Lcom/osp/app/signin/AccountView;->aN:I

    invoke-virtual {v0, v1, v6}, Landroid/support/v4/view/ViewPager;->a(IZ)V

    .line 7747
    :cond_11
    return-void

    .line 7670
    :cond_12
    invoke-virtual {p0}, Lcom/osp/app/signin/AccountView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v2, 0x7f080029

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    iput v0, v1, Landroid/view/ViewGroup$LayoutParams;->height:I

    goto :goto_7

    .line 7674
    :cond_13
    invoke-virtual {p0}, Lcom/osp/app/signin/AccountView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v2, 0x7f08002b

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    iput v0, v1, Landroid/view/ViewGroup$LayoutParams;->height:I

    goto :goto_7
.end method

.method static synthetic G(Lcom/osp/app/signin/AccountView;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 142
    iget-object v0, p0, Lcom/osp/app/signin/AccountView;->C:Ljava/lang/String;

    return-object v0
.end method

.method private G()V
    .locals 5

    .prologue
    .line 7754
    invoke-direct {p0}, Lcom/osp/app/signin/AccountView;->L()V

    .line 7756
    iget-object v0, p0, Lcom/osp/app/signin/AccountView;->aL:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    .line 7757
    invoke-static {}, Lcom/osp/app/signin/bg;->values()[Lcom/osp/app/signin/bg;

    move-result-object v1

    array-length v2, v1

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v2, :cond_1

    aget-object v3, v1, v0

    .line 7759
    iget v4, v3, Lcom/osp/app/signin/bg;->e:I

    if-lez v4, :cond_0

    .line 7761
    sget-object v4, Lcom/osp/app/signin/ao;->a:[I

    invoke-virtual {v3}, Lcom/osp/app/signin/bg;->ordinal()I

    move-result v3

    aget v3, v4, v3

    packed-switch v3, :pswitch_data_0

    .line 7757
    :cond_0
    :goto_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 7764
    :pswitch_0
    iget-boolean v3, p0, Lcom/osp/app/signin/AccountView;->ax:Z

    if-eqz v3, :cond_0

    .line 7766
    iget-object v3, p0, Lcom/osp/app/signin/AccountView;->aL:Ljava/util/ArrayList;

    sget-object v4, Lcom/osp/app/signin/bg;->a:Lcom/osp/app/signin/bg;

    invoke-virtual {v3, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 7771
    :pswitch_1
    iget-boolean v3, p0, Lcom/osp/app/signin/AccountView;->az:Z

    if-eqz v3, :cond_0

    .line 7773
    iget-object v3, p0, Lcom/osp/app/signin/AccountView;->aL:Ljava/util/ArrayList;

    sget-object v4, Lcom/osp/app/signin/bg;->b:Lcom/osp/app/signin/bg;

    invoke-virtual {v3, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 7778
    :pswitch_2
    iget-boolean v3, p0, Lcom/osp/app/signin/AccountView;->ay:Z

    if-eqz v3, :cond_0

    .line 7780
    iget-object v3, p0, Lcom/osp/app/signin/AccountView;->aL:Ljava/util/ArrayList;

    sget-object v4, Lcom/osp/app/signin/bg;->c:Lcom/osp/app/signin/bg;

    invoke-virtual {v3, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 7785
    :pswitch_3
    invoke-static {p0}, Lcom/osp/app/util/r;->b(Landroid/content/Context;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 7787
    iget-object v3, p0, Lcom/osp/app/signin/AccountView;->aL:Ljava/util/ArrayList;

    sget-object v4, Lcom/osp/app/signin/bg;->d:Lcom/osp/app/signin/bg;

    invoke-virtual {v3, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 7797
    :cond_1
    iget-object v0, p0, Lcom/osp/app/signin/AccountView;->aL:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    iput v0, p0, Lcom/osp/app/signin/AccountView;->aK:I

    .line 7799
    iget-object v0, p0, Lcom/osp/app/signin/AccountView;->aL:Ljava/util/ArrayList;

    invoke-static {v0}, Ljava/util/Collections;->shuffle(Ljava/util/List;)V

    .line 7800
    return-void

    .line 7761
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method static synthetic H(Lcom/osp/app/signin/AccountView;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 142
    iget-object v0, p0, Lcom/osp/app/signin/AccountView;->N:Ljava/lang/String;

    return-object v0
.end method

.method private H()V
    .locals 12

    .prologue
    const v11, 0x7f0c0067

    const v9, 0x7f0c0042

    const/high16 v8, 0x3f800000    # 1.0f

    const/4 v3, 0x1

    const/4 v4, 0x0

    .line 8227
    .line 8230
    const v0, 0x7f02007c

    .line 8232
    invoke-static {}, Lcom/osp/app/util/r;->a()Z

    move-result v1

    if-eqz v1, :cond_6

    .line 8235
    const v0, 0x7f020080

    move v2, v0

    move v1, v4

    move v5, v3

    .line 8242
    :goto_0
    invoke-static {p0}, Lcom/osp/app/util/r;->m(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_8

    .line 8244
    invoke-static {p0}, Lcom/osp/app/util/x;->p(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_15

    move v6, v3

    .line 8256
    :goto_1
    invoke-virtual {p0}, Lcom/osp/app/signin/AccountView;->getResources()Landroid/content/res/Resources;

    move-result-object v10

    .line 8259
    const v0, 0x7f0c0040

    invoke-virtual {p0, v0}, Lcom/osp/app/signin/AccountView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    .line 8260
    if-eqz v0, :cond_b

    invoke-static {p0}, Lcom/osp/app/util/r;->m(Landroid/content/Context;)Z

    move-result v7

    if-nez v7, :cond_0

    invoke-static {p0}, Lcom/osp/app/util/r;->k(Landroid/content/Context;)Z

    move-result v7

    if-eqz v7, :cond_b

    .line 8262
    :cond_0
    invoke-virtual {v0, v4}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 8264
    invoke-static {p0}, Lcom/osp/app/util/r;->h(Landroid/content/Context;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 8266
    invoke-virtual {v0}, Landroid/widget/LinearLayout;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v1

    check-cast v1, Landroid/widget/LinearLayout$LayoutParams;

    .line 8267
    invoke-virtual {v10}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v3

    iget v3, v3, Landroid/content/res/Configuration;->orientation:I

    const/4 v5, 0x2

    if-ne v3, v5, :cond_9

    .line 8269
    invoke-virtual {p0}, Lcom/osp/app/signin/AccountView;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v5, 0x7f080112

    invoke-virtual {v3, v5}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v3

    iput v3, v1, Landroid/widget/LinearLayout$LayoutParams;->topMargin:I

    .line 8274
    :goto_2
    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 8277
    :cond_1
    const v0, 0x7f0c0041

    invoke-virtual {p0, v0}, Lcom/osp/app/signin/AccountView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 8278
    if-eqz v0, :cond_3

    .line 8280
    invoke-static {p0}, Lcom/osp/app/util/an;->a(Lcom/osp/app/util/BaseActivity;)Lcom/osp/app/util/an;

    const v1, 0x7f0800f5

    invoke-static {v1}, Lcom/osp/app/util/an;->b(I)I

    move-result v1

    .line 8281
    invoke-virtual {p0}, Lcom/osp/app/signin/AccountView;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    invoke-virtual {v3, v1}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v1

    invoke-virtual {v0, v4, v1}, Landroid/widget/TextView;->setTextSize(IF)V

    .line 8283
    invoke-static {p0}, Lcom/osp/app/util/r;->m(Landroid/content/Context;)Z

    move-result v1

    if-eqz v1, :cond_3

    .line 8285
    invoke-virtual {p0}, Lcom/osp/app/signin/AccountView;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-static {p0}, Lcom/osp/app/util/an;->a(Lcom/osp/app/util/BaseActivity;)Lcom/osp/app/util/an;

    const v3, 0x7f07001a

    invoke-static {v3}, Lcom/osp/app/util/an;->a(I)I

    move-result v3

    invoke-virtual {v1, v3}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setTextColor(I)V

    .line 8287
    invoke-static {p0}, Lcom/osp/app/util/x;->m(Landroid/content/Context;)Z

    move-result v1

    if-eqz v1, :cond_a

    .line 8289
    const/16 v1, 0xbf

    invoke-static {v1, v4, v4, v4}, Landroid/graphics/Color;->argb(IIII)I

    move-result v1

    invoke-virtual {v0, v8, v8, v8, v1}, Landroid/widget/TextView;->setShadowLayer(FFFI)V

    .line 8295
    :goto_3
    invoke-static {p0}, Lcom/osp/app/util/x;->l(Landroid/content/Context;)Z

    move-result v1

    if-nez v1, :cond_2

    invoke-static {p0}, Lcom/osp/app/util/x;->a(Landroid/content/Context;)Z

    move-result v1

    if-eqz v1, :cond_3

    .line 8297
    :cond_2
    const/4 v1, 0x0

    invoke-virtual {v0, v1, v4}, Landroid/widget/TextView;->setTypeface(Landroid/graphics/Typeface;I)V

    .line 8302
    :cond_3
    invoke-virtual {p0, v9}, Lcom/osp/app/signin/AccountView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    .line 8303
    if-eqz v0, :cond_5

    .line 8305
    invoke-virtual {v0, v2}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 8306
    if-eqz v6, :cond_4

    .line 8308
    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 8311
    :cond_4
    invoke-static {p0}, Lcom/osp/app/util/r;->k(Landroid/content/Context;)Z

    move-result v1

    if-eqz v1, :cond_5

    .line 8313
    invoke-virtual {v0}, Landroid/widget/ImageView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v2

    move-object v1, v2

    .line 8314
    check-cast v1, Landroid/view/ViewGroup$MarginLayoutParams;

    iput v4, v1, Landroid/view/ViewGroup$MarginLayoutParams;->topMargin:I

    .line 8315
    invoke-virtual {v0, v2}, Landroid/widget/ImageView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 8403
    :cond_5
    :goto_4
    return-void

    .line 8236
    :cond_6
    invoke-static {}, Lcom/osp/app/util/r;->h()Z

    move-result v1

    if-nez v1, :cond_7

    invoke-static {}, Lcom/osp/app/util/r;->j()Z

    move-result v1

    if-eqz v1, :cond_16

    .line 8239
    :cond_7
    const v0, 0x7f02007e

    move v2, v0

    move v1, v3

    move v5, v4

    goto/16 :goto_0

    .line 8250
    :cond_8
    invoke-static {p0}, Lcom/osp/app/util/x;->o(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_15

    move v6, v3

    .line 8252
    goto/16 :goto_1

    .line 8272
    :cond_9
    invoke-virtual {p0}, Lcom/osp/app/signin/AccountView;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v5, 0x7f080111

    invoke-virtual {v3, v5}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v3

    iput v3, v1, Landroid/widget/LinearLayout$LayoutParams;->topMargin:I

    goto/16 :goto_2

    .line 8292
    :cond_a
    const/4 v1, 0x0

    const/high16 v3, 0x40000000    # 2.0f

    const/16 v5, 0xbf

    invoke-static {v5, v4, v4, v4}, Landroid/graphics/Color;->argb(IIII)I

    move-result v5

    invoke-virtual {v0, v8, v1, v3, v5}, Landroid/widget/TextView;->setShadowLayer(FFFI)V

    goto :goto_3

    .line 8320
    :cond_b
    invoke-virtual {p0, v9}, Lcom/osp/app/signin/AccountView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    .line 8322
    if-eqz v6, :cond_14

    invoke-virtual {p0, v11}, Lcom/osp/app/signin/AccountView;->findViewById(I)Landroid/view/View;

    move-result-object v7

    if-eqz v7, :cond_14

    .line 8324
    invoke-virtual {p0, v11}, Lcom/osp/app/signin/AccountView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v4}, Landroid/view/View;->setVisibility(I)V

    .line 8325
    invoke-virtual {p0, v9}, Lcom/osp/app/signin/AccountView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    const/16 v7, 0x8

    invoke-virtual {v0, v7}, Landroid/view/View;->setVisibility(I)V

    .line 8327
    invoke-virtual {p0, v11}, Lcom/osp/app/signin/AccountView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    move-object v9, v0

    .line 8330
    :goto_5
    if-eqz v9, :cond_5

    .line 8334
    const v0, 0x7f020082

    :try_start_0
    invoke-virtual {v10, v0}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    check-cast v0, Landroid/graphics/drawable/LayerDrawable;

    .line 8336
    if-eqz v0, :cond_f

    .line 8338
    invoke-static {p0}, Lcom/osp/app/util/an;->a(Lcom/osp/app/util/BaseActivity;)Lcom/osp/app/util/an;

    const v7, 0x7f020083

    invoke-static {v7}, Lcom/osp/app/util/an;->a(I)I

    move-result v7

    .line 8340
    invoke-static {}, Lcom/osp/device/b;->b()Lcom/osp/device/b;

    move-result-object v8

    invoke-virtual {v8, p0}, Lcom/osp/device/b;->i(Landroid/content/Context;)Z

    move-result v8

    if-eqz v8, :cond_13

    .line 8342
    const v8, 0x7f080019

    invoke-virtual {v10, v8}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v8

    float-to-int v8, v8

    .line 8344
    invoke-virtual {v10}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v11

    iget v11, v11, Landroid/content/res/Configuration;->orientation:I

    .line 8345
    if-ne v11, v3, :cond_12

    .line 8347
    const v3, 0x7f080016

    invoke-virtual {v10, v3}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v3

    float-to-int v3, v3

    if-eq v8, v3, :cond_12

    .line 8349
    const v2, 0x7f080016

    invoke-virtual {v10, v2}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v2

    float-to-int v3, v2

    .line 8351
    invoke-static {p0}, Lcom/osp/app/util/an;->a(Lcom/osp/app/util/BaseActivity;)Lcom/osp/app/util/an;

    const v2, 0x7f020085

    invoke-static {v2}, Lcom/osp/app/util/an;->a(I)I

    move-result v7

    .line 8352
    const v2, 0x7f02007d

    .line 8353
    if-eqz v1, :cond_c

    .line 8355
    const v2, 0x7f02007f

    move v8, v2

    .line 8363
    :goto_6
    invoke-virtual {v0}, Landroid/graphics/drawable/LayerDrawable;->getNumberOfLayers()I

    move-result v2

    move v1, v4

    .line 8364
    :goto_7
    if-ge v1, v2, :cond_10

    .line 8366
    invoke-virtual {v0, v1}, Landroid/graphics/drawable/LayerDrawable;->getId(I)I

    move-result v4

    const v5, 0x7f0c018b

    if-ne v4, v5, :cond_d

    .line 8368
    const/4 v2, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    invoke-virtual/range {v0 .. v5}, Landroid/graphics/drawable/LayerDrawable;->setLayerInset(IIIII)V

    move v1, v7

    move v2, v8

    .line 8374
    :goto_8
    invoke-static {v10, v1}, Landroid/graphics/BitmapFactory;->decodeResource(Landroid/content/res/Resources;I)Landroid/graphics/Bitmap;

    move-result-object v1

    .line 8375
    new-instance v3, Landroid/graphics/drawable/BitmapDrawable;

    invoke-direct {v3, v10, v1}, Landroid/graphics/drawable/BitmapDrawable;-><init>(Landroid/content/res/Resources;Landroid/graphics/Bitmap;)V

    .line 8376
    const/16 v1, 0x30

    invoke-virtual {v3, v1}, Landroid/graphics/drawable/BitmapDrawable;->setGravity(I)V

    .line 8378
    invoke-static {v10, v2}, Landroid/graphics/BitmapFactory;->decodeResource(Landroid/content/res/Resources;I)Landroid/graphics/Bitmap;

    move-result-object v1

    .line 8379
    new-instance v2, Landroid/graphics/drawable/BitmapDrawable;

    invoke-direct {v2, v10, v1}, Landroid/graphics/drawable/BitmapDrawable;-><init>(Landroid/content/res/Resources;Landroid/graphics/Bitmap;)V

    .line 8380
    const/16 v1, 0x50

    invoke-virtual {v2, v1}, Landroid/graphics/drawable/BitmapDrawable;->setGravity(I)V

    .line 8382
    const v1, 0x7f0c0189

    invoke-virtual {v0, v1, v3}, Landroid/graphics/drawable/LayerDrawable;->setDrawableByLayerId(ILandroid/graphics/drawable/Drawable;)Z

    .line 8383
    const v1, 0x7f0c018a

    invoke-virtual {v0, v1, v2}, Landroid/graphics/drawable/LayerDrawable;->setDrawableByLayerId(ILandroid/graphics/drawable/Drawable;)Z

    .line 8385
    if-eqz v6, :cond_e

    .line 8387
    const v1, 0x7f0c0189

    invoke-virtual {v0, v1}, Landroid/graphics/drawable/LayerDrawable;->findDrawableByLayerId(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    invoke-virtual {v9, v0}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto/16 :goto_4

    .line 8397
    :catch_0
    move-exception v0

    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    goto/16 :goto_4

    .line 8356
    :cond_c
    if-eqz v5, :cond_11

    .line 8358
    const v2, 0x7f020081

    move v8, v2

    goto :goto_6

    .line 8364
    :cond_d
    add-int/lit8 v1, v1, 0x1

    goto :goto_7

    .line 8390
    :cond_e
    :try_start_1
    invoke-virtual {v9, v0}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    goto/16 :goto_4

    .line 8395
    :cond_f
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v0, "LayerDrawable is null !!"

    invoke-static {v0}, Lcom/osp/app/util/aq;->c(Ljava/lang/String;)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    goto/16 :goto_4

    :cond_10
    move v1, v7

    move v2, v8

    goto :goto_8

    :cond_11
    move v8, v2

    goto :goto_6

    :cond_12
    move v3, v8

    move v8, v2

    goto :goto_6

    :cond_13
    move v1, v7

    goto :goto_8

    :cond_14
    move-object v9, v0

    goto/16 :goto_5

    :cond_15
    move v6, v4

    goto/16 :goto_1

    :cond_16
    move v2, v0

    move v1, v4

    move v5, v4

    goto/16 :goto_0
.end method

.method private I()Ljava/lang/String;
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 8420
    iget-object v0, p0, Lcom/osp/app/signin/AccountView;->ac:Ljava/lang/String;

    if-eqz v0, :cond_0

    .line 8422
    iget-object v0, p0, Lcom/osp/app/signin/AccountView;->ac:Ljava/lang/String;

    invoke-virtual {p0}, Lcom/osp/app/signin/AccountView;->getApplicationContext()Landroid/content/Context;

    iget-object v1, p0, Lcom/osp/app/signin/AccountView;->ac:Ljava/lang/String;

    invoke-static {v1}, Lcom/osp/common/util/i;->d(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-static {p0, v0, v1}, Lcom/msc/c/e;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)V

    .line 8423
    iget-object v0, p0, Lcom/osp/app/signin/AccountView;->ac:Ljava/lang/String;

    .line 8469
    :goto_0
    return-object v0

    .line 8426
    :cond_0
    invoke-static {}, Lcom/osp/common/util/i;->a()Lcom/osp/common/util/i;

    invoke-static {p0}, Lcom/osp/common/util/i;->e(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 8431
    :try_start_0
    invoke-static {}, Lcom/osp/common/util/i;->a()Lcom/osp/common/util/i;

    invoke-static {p0}, Lcom/osp/common/util/i;->h(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/osp/app/signin/AccountView;->ac:Ljava/lang/String;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 8437
    iget-object v0, p0, Lcom/osp/app/signin/AccountView;->ac:Ljava/lang/String;

    if-nez v0, :cond_8

    .line 8439
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v0, "AV"

    const-string v1, "The sim state is ready but mcc is null!! This device is abnormal. We will make mcc from locale settings."

    invoke-static {v0, v1}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 8441
    new-instance v0, Lcom/osp/app/signin/be;

    invoke-direct {v0, p0, v3}, Lcom/osp/app/signin/be;-><init>(Lcom/osp/app/signin/AccountView;I)V

    iput-object v0, p0, Lcom/osp/app/signin/AccountView;->E:Lcom/osp/app/signin/be;

    .line 8442
    iget-object v0, p0, Lcom/osp/app/signin/AccountView;->E:Lcom/osp/app/signin/be;

    :goto_1
    invoke-virtual {v0}, Lcom/osp/app/signin/be;->b()V

    .line 8469
    :goto_2
    iget-object v0, p0, Lcom/osp/app/signin/AccountView;->ac:Ljava/lang/String;

    goto :goto_0

    .line 8432
    :catch_0
    move-exception v0

    :try_start_1
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 8437
    iget-object v0, p0, Lcom/osp/app/signin/AccountView;->ac:Ljava/lang/String;

    if-nez v0, :cond_5

    .line 8439
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v0, "AV"

    const-string v1, "The sim state is ready but mcc is null!! This device is abnormal. We will make mcc from locale settings."

    invoke-static {v0, v1}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 8441
    new-instance v0, Lcom/osp/app/signin/be;

    invoke-direct {v0, p0, v3}, Lcom/osp/app/signin/be;-><init>(Lcom/osp/app/signin/AccountView;I)V

    iput-object v0, p0, Lcom/osp/app/signin/AccountView;->E:Lcom/osp/app/signin/be;

    .line 8442
    iget-object v0, p0, Lcom/osp/app/signin/AccountView;->E:Lcom/osp/app/signin/be;

    goto :goto_1

    .line 8437
    :catchall_0
    move-exception v0

    iget-object v1, p0, Lcom/osp/app/signin/AccountView;->ac:Ljava/lang/String;

    if-nez v1, :cond_2

    .line 8439
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v1, "AV"

    const-string v2, "The sim state is ready but mcc is null!! This device is abnormal. We will make mcc from locale settings."

    invoke-static {v1, v2}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 8441
    new-instance v1, Lcom/osp/app/signin/be;

    invoke-direct {v1, p0, v3}, Lcom/osp/app/signin/be;-><init>(Lcom/osp/app/signin/AccountView;I)V

    iput-object v1, p0, Lcom/osp/app/signin/AccountView;->E:Lcom/osp/app/signin/be;

    .line 8442
    iget-object v1, p0, Lcom/osp/app/signin/AccountView;->E:Lcom/osp/app/signin/be;

    invoke-virtual {v1}, Lcom/osp/app/signin/be;->b()V

    .line 8437
    :goto_3
    throw v0

    .line 8465
    :cond_1
    new-instance v0, Lcom/osp/app/signin/be;

    invoke-direct {v0, p0, v3}, Lcom/osp/app/signin/be;-><init>(Lcom/osp/app/signin/AccountView;I)V

    iput-object v0, p0, Lcom/osp/app/signin/AccountView;->E:Lcom/osp/app/signin/be;

    .line 8466
    iget-object v0, p0, Lcom/osp/app/signin/AccountView;->E:Lcom/osp/app/signin/be;

    invoke-virtual {v0}, Lcom/osp/app/signin/be;->b()V

    goto :goto_2

    .line 8445
    :cond_2
    iget-object v1, p0, Lcom/osp/app/signin/AccountView;->ac:Ljava/lang/String;

    invoke-virtual {p0}, Lcom/osp/app/signin/AccountView;->getApplicationContext()Landroid/content/Context;

    iget-object v2, p0, Lcom/osp/app/signin/AccountView;->ac:Ljava/lang/String;

    invoke-static {v2}, Lcom/osp/common/util/i;->d(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-static {p0, v1, v2}, Lcom/msc/c/e;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)V

    .line 8446
    iget-object v1, p0, Lcom/osp/app/signin/AccountView;->ac:Ljava/lang/String;

    invoke-static {p0, v1}, Lcom/osp/app/util/ad;->e(Landroid/content/Context;Ljava/lang/String;)V

    .line 8447
    invoke-static {}, Lcom/osp/app/signin/SamsungService;->a()Z

    move-result v1

    if-nez v1, :cond_4

    .line 8449
    const-string v1, "450"

    iget-object v2, p0, Lcom/osp/app/signin/AccountView;->ac:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_3

    invoke-static {p0}, Lcom/osp/app/util/r;->s(Landroid/content/Context;)Z

    move-result v1

    if-eqz v1, :cond_4

    :cond_3
    iget-boolean v1, p0, Lcom/osp/app/signin/AccountView;->ad:Z

    if-nez v1, :cond_4

    .line 8454
    invoke-direct {p0}, Lcom/osp/app/signin/AccountView;->J()V

    goto :goto_3

    .line 8457
    :cond_4
    invoke-direct {p0}, Lcom/osp/app/signin/AccountView;->D()V

    goto :goto_3

    .line 8445
    :cond_5
    iget-object v0, p0, Lcom/osp/app/signin/AccountView;->ac:Ljava/lang/String;

    invoke-virtual {p0}, Lcom/osp/app/signin/AccountView;->getApplicationContext()Landroid/content/Context;

    iget-object v1, p0, Lcom/osp/app/signin/AccountView;->ac:Ljava/lang/String;

    invoke-static {v1}, Lcom/osp/common/util/i;->d(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-static {p0, v0, v1}, Lcom/msc/c/e;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)V

    .line 8446
    iget-object v0, p0, Lcom/osp/app/signin/AccountView;->ac:Ljava/lang/String;

    invoke-static {p0, v0}, Lcom/osp/app/util/ad;->e(Landroid/content/Context;Ljava/lang/String;)V

    .line 8447
    invoke-static {}, Lcom/osp/app/signin/SamsungService;->a()Z

    move-result v0

    if-nez v0, :cond_7

    .line 8449
    const-string v0, "450"

    iget-object v1, p0, Lcom/osp/app/signin/AccountView;->ac:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_6

    invoke-static {p0}, Lcom/osp/app/util/r;->s(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_7

    :cond_6
    iget-boolean v0, p0, Lcom/osp/app/signin/AccountView;->ad:Z

    if-nez v0, :cond_7

    .line 8454
    :goto_4
    invoke-direct {p0}, Lcom/osp/app/signin/AccountView;->J()V

    goto/16 :goto_2

    .line 8457
    :cond_7
    invoke-direct {p0}, Lcom/osp/app/signin/AccountView;->D()V

    goto/16 :goto_2

    .line 8445
    :cond_8
    iget-object v0, p0, Lcom/osp/app/signin/AccountView;->ac:Ljava/lang/String;

    invoke-virtual {p0}, Lcom/osp/app/signin/AccountView;->getApplicationContext()Landroid/content/Context;

    iget-object v1, p0, Lcom/osp/app/signin/AccountView;->ac:Ljava/lang/String;

    invoke-static {v1}, Lcom/osp/common/util/i;->d(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-static {p0, v0, v1}, Lcom/msc/c/e;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)V

    .line 8446
    iget-object v0, p0, Lcom/osp/app/signin/AccountView;->ac:Ljava/lang/String;

    invoke-static {p0, v0}, Lcom/osp/app/util/ad;->e(Landroid/content/Context;Ljava/lang/String;)V

    .line 8447
    invoke-static {}, Lcom/osp/app/signin/SamsungService;->a()Z

    move-result v0

    if-nez v0, :cond_7

    .line 8449
    const-string v0, "450"

    iget-object v1, p0, Lcom/osp/app/signin/AccountView;->ac:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_9

    invoke-static {p0}, Lcom/osp/app/util/r;->s(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_7

    :cond_9
    iget-boolean v0, p0, Lcom/osp/app/signin/AccountView;->ad:Z

    if-nez v0, :cond_7

    goto :goto_4
.end method

.method static synthetic I(Lcom/osp/app/signin/AccountView;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 142
    iget-object v0, p0, Lcom/osp/app/signin/AccountView;->L:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic J(Lcom/osp/app/signin/AccountView;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 142
    iget-object v0, p0, Lcom/osp/app/signin/AccountView;->S:Ljava/lang/String;

    return-object v0
.end method

.method private J()V
    .locals 5

    .prologue
    const/4 v4, 0x1

    .line 8744
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v0, "AccountView - ShowNameValidationView"

    invoke-static {v0}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;)V

    .line 8746
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    .line 8748
    const-class v1, Lcom/osp/app/signin/NameValidationWebView;

    invoke-virtual {v0, p0, v1}, Landroid/content/Intent;->setClass(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;

    .line 8749
    const-string v1, "is_signup_flow"

    invoke-virtual {v0, v1, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 8750
    const-string v1, "key_request_id"

    iget-wide v2, p0, Lcom/osp/app/signin/AccountView;->Z:J

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;J)Landroid/content/Intent;

    .line 8752
    const-string v1, "is_cancelable_just_one_activity"

    invoke-virtual {v0, v1, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 8757
    const/16 v1, 0xca

    invoke-virtual {p0, v0, v1}, Lcom/osp/app/signin/AccountView;->startActivityForResult(Landroid/content/Intent;I)V

    .line 8758
    return-void
.end method

.method static synthetic K(Lcom/osp/app/signin/AccountView;)V
    .locals 4

    .prologue
    .line 142
    new-instance v0, Landroid/content/Intent;

    const-string v1, "com.msc.action.samsungaccount.resignin_with_signout"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    const-string v1, "client_id"

    iget-object v2, p0, Lcom/osp/app/signin/AccountView;->H:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    const-string v1, "client_secret"

    iget-object v2, p0, Lcom/osp/app/signin/AccountView;->I:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    const-string v1, "account_mode"

    iget-object v2, p0, Lcom/osp/app/signin/AccountView;->C:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    const-string v1, "OSP_VER"

    iget-object v2, p0, Lcom/osp/app/signin/AccountView;->L:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    const-string v1, "MODE"

    iget-object v2, p0, Lcom/osp/app/signin/AccountView;->S:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    const-string v1, "BG_WhoareU"

    iget-object v2, p0, Lcom/osp/app/signin/AccountView;->T:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    const-string v1, "BG_mode"

    iget-object v2, p0, Lcom/osp/app/signin/AccountView;->T:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/osp/app/signin/AccountView;->R:Ljava/lang/String;

    if-eqz v1, :cond_0

    const-string v1, "mypackage"

    iget-object v2, p0, Lcom/osp/app/signin/AccountView;->R:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    const-string v1, "key_request_id"

    iget-wide v2, p0, Lcom/osp/app/signin/AccountView;->Z:J

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;J)Landroid/content/Intent;

    :cond_0
    const/16 v1, 0xcd

    invoke-virtual {p0, v0, v1}, Lcom/osp/app/signin/AccountView;->startActivityForResult(Landroid/content/Intent;I)V

    return-void
.end method

.method private K()Z
    .locals 4

    .prologue
    const/4 v0, 0x0

    .line 9786
    iget-object v1, p0, Lcom/osp/app/signin/AccountView;->ao:Landroid/net/Uri;

    if-nez v1, :cond_1

    .line 9807
    :cond_0
    :goto_0
    return v0

    .line 9794
    :cond_1
    iget-object v1, p0, Lcom/osp/app/signin/AccountView;->ao:Landroid/net/Uri;

    invoke-virtual {v1}, Landroid/net/Uri;->getScheme()Ljava/lang/String;

    move-result-object v1

    .line 9795
    iget-object v2, p0, Lcom/osp/app/signin/AccountView;->ao:Landroid/net/Uri;

    invoke-virtual {v2}, Landroid/net/Uri;->getHost()Ljava/lang/String;

    move-result-object v2

    .line 9797
    if-eqz v1, :cond_0

    if-eqz v2, :cond_0

    .line 9802
    const-string v3, "samsungaccount"

    invoke-virtual {v1, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    const-string v1, "MainPage"

    invoke-virtual {v2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 9804
    const/4 v0, 0x1

    goto :goto_0
.end method

.method private L()V
    .locals 9

    .prologue
    const/4 v2, 0x1

    const/4 v3, 0x0

    .line 10414
    invoke-virtual {p0}, Lcom/osp/app/signin/AccountView;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v0

    .line 10417
    :try_start_0
    const-string v1, "com.sec.dsm.system"

    const/16 v4, 0x80

    invoke-virtual {v0, v1, v4}, Landroid/content/pm/PackageManager;->getApplicationInfo(Ljava/lang/String;I)Landroid/content/pm/ApplicationInfo;

    .line 10418
    const/4 v1, 0x1

    iput-boolean v1, p0, Lcom/osp/app/signin/AccountView;->ax:Z

    .line 10419
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v1, "AV"

    const-string v4, "FMM exist"

    invoke-static {v1, v4}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    .line 10432
    :goto_0
    invoke-static {}, Lcom/osp/app/util/r;->h()Z

    move-result v1

    if-eqz v1, :cond_3

    .line 10434
    iput-boolean v3, p0, Lcom/osp/app/signin/AccountView;->ay:Z

    .line 10485
    :cond_0
    :goto_1
    invoke-static {}, Lcom/osp/app/util/r;->a()Z

    move-result v0

    if-nez v0, :cond_1

    invoke-static {}, Lcom/osp/app/util/r;->h()Z

    move-result v0

    if-nez v0, :cond_1

    .line 10487
    iput-boolean v2, p0, Lcom/osp/app/signin/AccountView;->az:Z

    .line 10490
    :cond_1
    invoke-static {}, Lcom/osp/app/util/ah;->a()Lcom/osp/app/util/ah;

    move-result-object v0

    invoke-virtual {v0}, Lcom/osp/app/util/ah;->t()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 10492
    iput-boolean v3, p0, Lcom/osp/app/signin/AccountView;->ay:Z

    .line 10493
    iput-boolean v3, p0, Lcom/osp/app/signin/AccountView;->ax:Z

    .line 10494
    iput-boolean v3, p0, Lcom/osp/app/signin/AccountView;->az:Z

    .line 10497
    :cond_2
    return-void

    .line 10422
    :catch_0
    move-exception v1

    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v1, "AV"

    const-string v4, "FMM doesn\'t exist"

    invoke-static {v1, v4}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 10423
    iput-boolean v3, p0, Lcom/osp/app/signin/AccountView;->ax:Z

    goto :goto_0

    .line 10439
    :cond_3
    :try_start_1
    invoke-static {p0}, Lcom/msc/sa/c/d;->d(Landroid/content/Context;)Z

    move-result v1

    if-eqz v1, :cond_4

    .line 10442
    const-string v1, "com.sec.android.sCloudBackupApp"

    const/16 v4, 0x80

    invoke-virtual {v0, v1, v4}, Landroid/content/pm/PackageManager;->getApplicationInfo(Ljava/lang/String;I)Landroid/content/pm/ApplicationInfo;

    .line 10444
    const/4 v1, 0x1

    iput-boolean v1, p0, Lcom/osp/app/signin/AccountView;->ay:Z

    .line 10445
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v1, "AV"

    const-string v4, "PDM exist"

    invoke-static {v1, v4}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_1
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_1 .. :try_end_1} :catch_1

    .line 10467
    :goto_2
    invoke-static {}, Lcom/osp/app/util/r;->p()Z

    move-result v0

    if-nez v0, :cond_0

    .line 10472
    :try_start_2
    invoke-static {p0}, Lcom/osp/common/util/i;->a(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_7

    invoke-virtual {p0}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v0

    const-string v1, "sms:"

    invoke-static {v1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    new-instance v4, Landroid/content/Intent;

    const-string v5, "android.intent.action.SENDTO"

    invoke-direct {v4, v5, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;Landroid/net/Uri;)V

    const/4 v1, 0x0

    invoke-virtual {v0, v4, v1}, Landroid/content/pm/PackageManager;->queryIntentActivities(Landroid/content/Intent;I)Ljava/util/List;

    move-result-object v5

    invoke-interface {v5}, Ljava/util/List;->size()I

    move-result v6

    const-string v1, ""

    move v4, v3

    :goto_3
    if-ge v4, v6, :cond_5

    invoke-interface {v5, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/pm/ResolveInfo;

    const-string v7, "com.google.android.talk"

    iget-object v8, v0, Landroid/content/pm/ResolveInfo;->activityInfo:Landroid/content/pm/ActivityInfo;

    iget-object v8, v8, Landroid/content/pm/ActivityInfo;->applicationInfo:Landroid/content/pm/ApplicationInfo;

    iget-object v8, v8, Landroid/content/pm/ApplicationInfo;->packageName:Ljava/lang/String;

    invoke-virtual {v7, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-nez v7, :cond_9

    iget-object v0, v0, Landroid/content/pm/ResolveInfo;->activityInfo:Landroid/content/pm/ActivityInfo;

    iget-object v0, v0, Landroid/content/pm/ActivityInfo;->applicationInfo:Landroid/content/pm/ApplicationInfo;

    iget-object v0, v0, Landroid/content/pm/ApplicationInfo;->packageName:Ljava/lang/String;

    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v1, "packageName"

    new-instance v7, Ljava/lang/StringBuilder;

    const-string v8, "packageNamepackageName"

    invoke-direct {v7, v8}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v7, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v1, v7}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_3

    :goto_4
    add-int/lit8 v1, v4, 0x1

    move v4, v1

    move-object v1, v0

    goto :goto_3

    .line 10448
    :cond_4
    :try_start_3
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v1, "AV"

    const-string v4, "Not supported PDM. Not Owner User"

    invoke-static {v1, v4}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_3
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_3 .. :try_end_3} :catch_1

    goto :goto_2

    :catch_1
    move-exception v1

    .line 10456
    :try_start_4
    const-string v1, "com.samsung.android.scloud.backup"

    const/16 v4, 0x80

    invoke-virtual {v0, v1, v4}, Landroid/content/pm/PackageManager;->getApplicationInfo(Ljava/lang/String;I)Landroid/content/pm/ApplicationInfo;

    .line 10458
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/osp/app/signin/AccountView;->ay:Z

    .line 10459
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v0, "AV"

    const-string v1, "PDM exist"

    invoke-static {v0, v1}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_4
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_4 .. :try_end_4} :catch_2

    goto/16 :goto_2

    .line 10462
    :catch_2
    move-exception v0

    iput-boolean v3, p0, Lcom/osp/app/signin/AccountView;->ay:Z

    .line 10463
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v0, "AV"

    const-string v1, "PDM doesn\'t exist"

    invoke-static {v0, v1}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_2

    .line 10472
    :cond_5
    if-eqz v1, :cond_6

    :try_start_5
    const-string v0, ""

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_8

    :cond_6
    move v0, v3

    :goto_5
    if-nez v0, :cond_0

    .line 10474
    :cond_7
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/osp/app/signin/AccountView;->ay:Z

    .line 10475
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v0, "AV"

    const-string v1, "PDM doesn\'t exist"

    invoke-static {v0, v1}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_5
    .catch Ljava/lang/Exception; {:try_start_5 .. :try_end_5} :catch_3

    goto/16 :goto_1

    :catch_3
    move-exception v0

    goto/16 :goto_1

    :cond_8
    move v0, v2

    .line 10472
    goto :goto_5

    :cond_9
    move-object v0, v1

    goto :goto_4
.end method

.method static synthetic L(Lcom/osp/app/signin/AccountView;)Z
    .locals 1

    .prologue
    .line 142
    invoke-direct {p0}, Lcom/osp/app/signin/AccountView;->E()Z

    move-result v0

    return v0
.end method

.method private M()V
    .locals 17

    .prologue
    .line 10559
    invoke-static/range {p0 .. p0}, Lcom/osp/app/util/r;->g(Landroid/content/Context;)Z

    move-result v1

    if-nez v1, :cond_2

    .line 10561
    const v1, 0x7f0c0040

    move-object/from16 v0, p0

    invoke-virtual {v0, v1}, Lcom/osp/app/signin/AccountView;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/LinearLayout;

    .line 10562
    if-eqz v1, :cond_2

    .line 10564
    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 10566
    invoke-static/range {p0 .. p0}, Lcom/osp/app/util/r;->h(Landroid/content/Context;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 10568
    invoke-virtual {v1}, Landroid/widget/LinearLayout;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v2

    check-cast v2, Landroid/widget/LinearLayout$LayoutParams;

    .line 10569
    invoke-virtual/range {p0 .. p0}, Lcom/osp/app/signin/AccountView;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    invoke-virtual {v3}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v3

    iget v3, v3, Landroid/content/res/Configuration;->orientation:I

    const/4 v4, 0x2

    if-ne v3, v4, :cond_c

    .line 10571
    invoke-virtual/range {p0 .. p0}, Lcom/osp/app/signin/AccountView;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f080112

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v3

    iput v3, v2, Landroid/widget/LinearLayout$LayoutParams;->topMargin:I

    .line 10576
    :goto_0
    invoke-virtual {v1, v2}, Landroid/widget/LinearLayout;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 10579
    :cond_0
    const v1, 0x7f0c0041

    move-object/from16 v0, p0

    invoke-virtual {v0, v1}, Lcom/osp/app/signin/AccountView;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    .line 10580
    if-eqz v1, :cond_2

    .line 10582
    invoke-static/range {p0 .. p0}, Lcom/osp/app/util/an;->a(Lcom/osp/app/util/BaseActivity;)Lcom/osp/app/util/an;

    const v2, 0x7f0800f5

    invoke-static {v2}, Lcom/osp/app/util/an;->b(I)I

    move-result v2

    .line 10583
    const/4 v3, 0x0

    invoke-virtual/range {p0 .. p0}, Lcom/osp/app/signin/AccountView;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    invoke-virtual {v4, v2}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v2

    invoke-virtual {v1, v3, v2}, Landroid/widget/TextView;->setTextSize(IF)V

    .line 10584
    invoke-virtual/range {p0 .. p0}, Lcom/osp/app/signin/AccountView;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-static/range {p0 .. p0}, Lcom/osp/app/util/an;->a(Lcom/osp/app/util/BaseActivity;)Lcom/osp/app/util/an;

    const v3, 0x7f07001a

    invoke-static {v3}, Lcom/osp/app/util/an;->a(I)I

    move-result v3

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getColor(I)I

    move-result v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setTextColor(I)V

    .line 10586
    invoke-static {}, Lcom/osp/device/b;->b()Lcom/osp/device/b;

    move-result-object v2

    move-object/from16 v0, p0

    invoke-virtual {v2, v0}, Lcom/osp/device/b;->i(Landroid/content/Context;)Z

    move-result v2

    if-nez v2, :cond_2

    .line 10588
    invoke-static/range {p0 .. p0}, Lcom/osp/app/util/x;->m(Landroid/content/Context;)Z

    move-result v2

    if-eqz v2, :cond_d

    .line 10590
    const/high16 v2, 0x3f800000    # 1.0f

    const/high16 v3, 0x3f800000    # 1.0f

    const/high16 v4, 0x3f800000    # 1.0f

    const/16 v5, 0xbf

    const/4 v6, 0x0

    const/4 v7, 0x0

    const/4 v8, 0x0

    invoke-static {v5, v6, v7, v8}, Landroid/graphics/Color;->argb(IIII)I

    move-result v5

    invoke-virtual {v1, v2, v3, v4, v5}, Landroid/widget/TextView;->setShadowLayer(FFFI)V

    .line 10596
    :goto_1
    invoke-static/range {p0 .. p0}, Lcom/osp/app/util/x;->l(Landroid/content/Context;)Z

    move-result v2

    if-nez v2, :cond_1

    invoke-static/range {p0 .. p0}, Lcom/osp/app/util/x;->a(Landroid/content/Context;)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 10598
    :cond_1
    const/4 v2, 0x0

    const/4 v3, 0x0

    invoke-virtual {v1, v2, v3}, Landroid/widget/TextView;->setTypeface(Landroid/graphics/Typeface;I)V

    .line 10605
    :cond_2
    const v1, 0x7f0c0042

    move-object/from16 v0, p0

    invoke-virtual {v0, v1}, Lcom/osp/app/signin/AccountView;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ImageView;

    .line 10606
    if-eqz v1, :cond_3

    .line 10608
    const/16 v2, 0x8

    invoke-virtual {v1, v2}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 10611
    :cond_3
    const v1, 0x7f0c0043

    move-object/from16 v0, p0

    invoke-virtual {v0, v1}, Lcom/osp/app/signin/AccountView;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ScrollView;

    .line 10612
    if-eqz v1, :cond_4

    .line 10614
    const/16 v2, 0x8

    invoke-virtual {v1, v2}, Landroid/widget/ScrollView;->setVisibility(I)V

    .line 10617
    :cond_4
    invoke-virtual/range {p0 .. p0}, Lcom/osp/app/signin/AccountView;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v1

    iget v1, v1, Landroid/content/res/Configuration;->orientation:I

    const/4 v2, 0x2

    if-ne v1, v2, :cond_e

    const/4 v1, 0x1

    move v8, v1

    .line 10619
    :goto_2
    const v1, 0x7f0c0069

    move-object/from16 v0, p0

    invoke-virtual {v0, v1}, Lcom/osp/app/signin/AccountView;->findViewById(I)Landroid/view/View;

    move-result-object v10

    .line 10620
    const v1, 0x7f0c0072

    move-object/from16 v0, p0

    invoke-virtual {v0, v1}, Lcom/osp/app/signin/AccountView;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ListView;

    .line 10621
    const v2, 0x7f0c006f

    move-object/from16 v0, p0

    invoke-virtual {v0, v2}, Lcom/osp/app/signin/AccountView;->findViewById(I)Landroid/view/View;

    move-result-object v11

    .line 10622
    const v2, 0x7f0c0071

    move-object/from16 v0, p0

    invoke-virtual {v0, v2}, Lcom/osp/app/signin/AccountView;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    .line 10623
    const v3, 0x7f0c006a

    move-object/from16 v0, p0

    invoke-virtual {v0, v3}, Lcom/osp/app/signin/AccountView;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/ImageView;

    .line 10624
    const v4, 0x7f0c006c

    move-object/from16 v0, p0

    invoke-virtual {v0, v4}, Lcom/osp/app/signin/AccountView;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/ImageView;

    .line 10625
    if-eqz v10, :cond_b

    if-eqz v1, :cond_b

    if-eqz v11, :cond_b

    if-eqz v2, :cond_b

    if-eqz v3, :cond_b

    if-eqz v4, :cond_b

    .line 10628
    invoke-virtual {v1}, Landroid/widget/ListView;->getPaddingTop()I

    move-result v12

    .line 10629
    invoke-virtual {v1}, Landroid/widget/ListView;->getPaddingBottom()I

    move-result v13

    .line 10630
    invoke-virtual {v10}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v5

    check-cast v5, Landroid/view/ViewGroup$MarginLayoutParams;

    .line 10634
    invoke-virtual {v1}, Landroid/widget/ListView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v14

    .line 10635
    if-eqz v8, :cond_10

    .line 10637
    invoke-static {}, Lcom/osp/device/b;->b()Lcom/osp/device/b;

    move-result-object v6

    move-object/from16 v0, p0

    invoke-virtual {v6, v0}, Lcom/osp/device/b;->i(Landroid/content/Context;)Z

    move-result v6

    if-nez v6, :cond_5

    .line 10639
    const v6, 0x7f0c0066

    move-object/from16 v0, p0

    invoke-virtual {v0, v6}, Lcom/osp/app/signin/AccountView;->findViewById(I)Landroid/view/View;

    move-result-object v6

    check-cast v6, Landroid/widget/LinearLayout;

    .line 10640
    const/4 v7, 0x0

    const/4 v9, 0x0

    const/4 v15, 0x0

    const/16 v16, 0x0

    move/from16 v0, v16

    invoke-static {v6, v7, v9, v15, v0}, Lcom/msc/sa/c/d;->a(Landroid/view/View;IIII)V

    .line 10642
    invoke-virtual {v6}, Landroid/widget/LinearLayout;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v7

    check-cast v7, Landroid/widget/LinearLayout$LayoutParams;

    .line 10643
    const/4 v9, 0x0

    iput v9, v7, Landroid/widget/LinearLayout$LayoutParams;->topMargin:I

    .line 10644
    invoke-virtual {v6, v7}, Landroid/widget/LinearLayout;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 10647
    :cond_5
    const/4 v6, 0x0

    iput v6, v5, Landroid/view/ViewGroup$MarginLayoutParams;->topMargin:I

    .line 10648
    invoke-virtual/range {p0 .. p0}, Lcom/osp/app/signin/AccountView;->getResources()Landroid/content/res/Resources;

    move-result-object v6

    const v7, 0x7f080046

    invoke-virtual {v6, v7}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v6

    iput v6, v5, Landroid/view/ViewGroup$MarginLayoutParams;->leftMargin:I

    .line 10649
    invoke-virtual/range {p0 .. p0}, Lcom/osp/app/signin/AccountView;->getResources()Landroid/content/res/Resources;

    move-result-object v6

    const v7, 0x7f080047

    invoke-virtual {v6, v7}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v6

    iput v6, v5, Landroid/view/ViewGroup$MarginLayoutParams;->rightMargin:I

    .line 10657
    invoke-virtual/range {p0 .. p0}, Lcom/osp/app/signin/AccountView;->getResources()Landroid/content/res/Resources;

    move-result-object v6

    const v7, 0x7f080049

    invoke-virtual {v6, v7}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v6

    float-to-int v9, v6

    .line 10658
    invoke-virtual/range {p0 .. p0}, Lcom/osp/app/signin/AccountView;->getResources()Landroid/content/res/Resources;

    move-result-object v6

    const v7, 0x7f080048

    invoke-virtual {v6, v7}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v6

    float-to-int v7, v6

    .line 10660
    const v6, 0x7f02003a

    invoke-virtual {v3, v6}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 10661
    const v6, 0x7f020037

    invoke-virtual {v4, v6}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 10663
    invoke-virtual/range {p0 .. p0}, Lcom/osp/app/signin/AccountView;->getResources()Landroid/content/res/Resources;

    move-result-object v6

    invoke-static/range {p0 .. p0}, Lcom/osp/app/util/an;->a(Lcom/osp/app/util/BaseActivity;)Lcom/osp/app/util/an;

    const v15, 0x7f07000a

    invoke-static {v15}, Lcom/osp/app/util/an;->a(I)I

    move-result v15

    invoke-virtual {v6, v15}, Landroid/content/res/Resources;->getColor(I)I

    move-result v6

    invoke-virtual {v2, v6}, Landroid/widget/TextView;->setTextColor(I)V

    .line 10664
    sget v6, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v15, 0x15

    if-lt v6, v15, :cond_6

    .line 10666
    const-string v6, "sec-roboto-light"

    const/4 v15, 0x1

    invoke-static {v6, v15}, Landroid/graphics/Typeface;->create(Ljava/lang/String;I)Landroid/graphics/Typeface;

    move-result-object v6

    invoke-virtual {v2, v6}, Landroid/widget/TextView;->setTypeface(Landroid/graphics/Typeface;)V

    .line 10670
    :cond_6
    const/4 v6, 0x1

    invoke-virtual {v2, v6}, Landroid/widget/TextView;->setVerticalScrollBarEnabled(Z)V

    .line 10671
    invoke-static {}, Landroid/text/method/ScrollingMovementMethod;->getInstance()Landroid/text/method/MovementMethod;

    move-result-object v6

    invoke-virtual {v2, v6}, Landroid/widget/TextView;->setMovementMethod(Landroid/text/method/MovementMethod;)V

    .line 10673
    invoke-static {}, Lcom/osp/device/b;->b()Lcom/osp/device/b;

    move-result-object v6

    move-object/from16 v0, p0

    invoke-virtual {v6, v0}, Lcom/osp/device/b;->i(Landroid/content/Context;)Z

    move-result v6

    if-nez v6, :cond_7

    .line 10675
    const/4 v6, 0x0

    invoke-virtual {v11, v6}, Landroid/view/View;->setVisibility(I)V

    .line 10678
    :cond_7
    invoke-static/range {p0 .. p0}, Lcom/osp/app/util/r;->g(Landroid/content/Context;)Z

    move-result v6

    if-eqz v6, :cond_13

    .line 10680
    const v6, 0x7f0c0070

    move-object/from16 v0, p0

    invoke-virtual {v0, v6}, Lcom/osp/app/signin/AccountView;->findViewById(I)Landroid/view/View;

    move-result-object v6

    check-cast v6, Landroid/widget/TextView;

    .line 10681
    invoke-virtual {v6}, Landroid/widget/TextView;->getVisibility()I

    move-result v6

    if-nez v6, :cond_f

    .line 10683
    const/4 v6, 0x3

    invoke-static {v2, v6}, Lcom/msc/sa/c/d;->a(Landroid/view/View;I)V

    move v6, v7

    move v7, v9

    .line 10714
    :goto_3
    invoke-static/range {p0 .. p0}, Lcom/osp/app/util/r;->g(Landroid/content/Context;)Z

    move-result v2

    if-nez v2, :cond_12

    .line 10716
    invoke-static {v10, v5}, Lcom/msc/sa/c/d;->a(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    .line 10717
    invoke-static {v1, v14}, Lcom/msc/sa/c/d;->a(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    .line 10718
    invoke-static {v1, v6, v12, v7, v13}, Lcom/msc/sa/c/d;->a(Landroid/view/View;IIII)V

    .line 10742
    :cond_8
    :goto_4
    invoke-virtual {v1}, Landroid/widget/ListView;->getAdapter()Landroid/widget/ListAdapter;

    move-result-object v2

    if-nez v2, :cond_9

    .line 10744
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/osp/app/signin/AccountView;->aM:Lcom/osp/app/signin/h;

    const v3, 0x2063bf

    invoke-virtual {v2, v3}, Lcom/osp/app/signin/h;->b(I)Lcom/osp/app/signin/j;

    move-result-object v2

    .line 10745
    invoke-virtual {v1, v2}, Landroid/widget/ListView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 10747
    :cond_9
    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Landroid/widget/ListView;->setClickable(Z)V

    .line 10748
    invoke-static/range {p0 .. p0}, Lcom/osp/app/util/r;->g(Landroid/content/Context;)Z

    move-result v2

    if-eqz v2, :cond_a

    .line 10750
    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Landroid/widget/ListView;->setVerticalFadingEdgeEnabled(Z)V

    .line 10752
    :cond_a
    const/4 v1, 0x0

    invoke-virtual {v10, v1}, Landroid/view/View;->setVisibility(I)V

    .line 10754
    :cond_b
    return-void

    .line 10574
    :cond_c
    invoke-virtual/range {p0 .. p0}, Lcom/osp/app/signin/AccountView;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f080111

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v3

    iput v3, v2, Landroid/widget/LinearLayout$LayoutParams;->topMargin:I

    goto/16 :goto_0

    .line 10593
    :cond_d
    const/high16 v2, 0x3f800000    # 1.0f

    const/4 v3, 0x0

    const/high16 v4, 0x40000000    # 2.0f

    const/16 v5, 0xbf

    const/4 v6, 0x0

    const/4 v7, 0x0

    const/4 v8, 0x0

    invoke-static {v5, v6, v7, v8}, Landroid/graphics/Color;->argb(IIII)I

    move-result v5

    invoke-virtual {v1, v2, v3, v4, v5}, Landroid/widget/TextView;->setShadowLayer(FFFI)V

    goto/16 :goto_1

    .line 10617
    :cond_e
    const/4 v1, 0x0

    move v8, v1

    goto/16 :goto_2

    .line 10686
    :cond_f
    const/16 v6, 0x10

    invoke-virtual {v2, v6}, Landroid/widget/TextView;->setGravity(I)V

    move v6, v7

    move v7, v9

    .line 10688
    goto :goto_3

    .line 10691
    :cond_10
    invoke-virtual/range {p0 .. p0}, Lcom/osp/app/signin/AccountView;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v6, 0x7f08003f

    invoke-virtual {v2, v6}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v2

    iput v2, v5, Landroid/view/ViewGroup$MarginLayoutParams;->topMargin:I

    .line 10692
    const/4 v2, 0x0

    iput v2, v5, Landroid/view/ViewGroup$MarginLayoutParams;->leftMargin:I

    .line 10693
    const/4 v2, 0x0

    iput v2, v5, Landroid/view/ViewGroup$MarginLayoutParams;->rightMargin:I

    .line 10701
    invoke-virtual/range {p0 .. p0}, Lcom/osp/app/signin/AccountView;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v6, 0x7f080043

    invoke-virtual {v2, v6}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v2

    float-to-int v6, v2

    .line 10702
    invoke-virtual/range {p0 .. p0}, Lcom/osp/app/signin/AccountView;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v7, 0x7f080042

    invoke-virtual {v2, v7}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v2

    float-to-int v2, v2

    .line 10704
    const v7, 0x7f020038

    invoke-virtual {v3, v7}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 10705
    const v7, 0x7f020036

    invoke-virtual {v4, v7}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 10708
    invoke-static {}, Lcom/osp/device/b;->b()Lcom/osp/device/b;

    move-result-object v7

    move-object/from16 v0, p0

    invoke-virtual {v7, v0}, Lcom/osp/device/b;->i(Landroid/content/Context;)Z

    move-result v7

    if-nez v7, :cond_11

    .line 10710
    const/16 v7, 0x8

    invoke-virtual {v11, v7}, Landroid/view/View;->setVisibility(I)V

    :cond_11
    move v7, v6

    move v6, v2

    goto/16 :goto_3

    .line 10721
    :cond_12
    const/16 v2, 0x8

    invoke-virtual {v3, v2}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 10722
    const/16 v2, 0x8

    invoke-virtual {v4, v2}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 10724
    const v2, 0x7f0c006d

    move-object/from16 v0, p0

    invoke-virtual {v0, v2}, Lcom/osp/app/signin/AccountView;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Lcom/osp/app/signin/AccountTitleLinearLayout;

    .line 10726
    invoke-virtual/range {p0 .. p0}, Lcom/osp/app/signin/AccountView;->h()Z

    move-result v3

    move-object/from16 v0, p0

    invoke-virtual {v2, v0, v3}, Lcom/osp/app/signin/AccountTitleLinearLayout;->a(Landroid/content/Context;Z)V

    .line 10727
    const v3, 0x7f09004f

    move-object/from16 v0, p0

    invoke-virtual {v0, v3}, Lcom/osp/app/signin/AccountView;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/osp/app/signin/AccountTitleLinearLayout;->a(Ljava/lang/String;)V

    .line 10728
    const/4 v3, 0x1

    invoke-static {v3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/osp/app/signin/AccountTitleLinearLayout;->a(Ljava/lang/Boolean;)V

    .line 10732
    invoke-static {v1, v6, v12, v7, v13}, Lcom/msc/sa/c/d;->a(Landroid/view/View;IIII)V

    .line 10734
    const v2, 0x7f0c0060

    move-object/from16 v0, p0

    invoke-virtual {v0, v2}, Lcom/osp/app/signin/AccountView;->findViewById(I)Landroid/view/View;

    move-result-object v2

    .line 10735
    if-eqz v2, :cond_8

    if-eqz v8, :cond_8

    .line 10737
    invoke-virtual/range {p0 .. p0}, Lcom/osp/app/signin/AccountView;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f080021

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v3

    .line 10738
    const/4 v4, -0x1

    const/4 v5, -0x1

    const/4 v6, -0x1

    invoke-static {v2, v4, v3, v5, v6}, Lcom/msc/sa/c/d;->a(Landroid/view/View;IIII)V

    goto/16 :goto_4

    :cond_13
    move v6, v7

    move v7, v9

    goto/16 :goto_3
.end method

.method static synthetic M(Lcom/osp/app/signin/AccountView;)Z
    .locals 1

    .prologue
    .line 142
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/osp/app/signin/AccountView;->aw:Z

    return v0
.end method

.method private N()V
    .locals 1

    .prologue
    .line 10906
    iget-object v0, p0, Lcom/osp/app/signin/AccountView;->aH:Ljava/util/Timer;

    if-eqz v0, :cond_0

    .line 10908
    iget-object v0, p0, Lcom/osp/app/signin/AccountView;->aH:Ljava/util/Timer;

    invoke-virtual {v0}, Ljava/util/Timer;->cancel()V

    .line 10909
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/osp/app/signin/AccountView;->aH:Ljava/util/Timer;

    .line 10911
    :cond_0
    return-void
.end method

.method static synthetic N(Lcom/osp/app/signin/AccountView;)V
    .locals 0

    .prologue
    .line 142
    invoke-direct {p0}, Lcom/osp/app/signin/AccountView;->y()V

    return-void
.end method

.method static synthetic O(Lcom/osp/app/signin/AccountView;)Landroid/widget/LinearLayout;
    .locals 1

    .prologue
    .line 142
    iget-object v0, p0, Lcom/osp/app/signin/AccountView;->aO:Landroid/widget/LinearLayout;

    return-object v0
.end method

.method static synthetic P(Lcom/osp/app/signin/AccountView;)I
    .locals 1

    .prologue
    .line 142
    iget v0, p0, Lcom/osp/app/signin/AccountView;->aK:I

    return v0
.end method

.method static synthetic Q(Lcom/osp/app/signin/AccountView;)Landroid/support/v4/view/ViewPager;
    .locals 1

    .prologue
    .line 142
    iget-object v0, p0, Lcom/osp/app/signin/AccountView;->aJ:Landroid/support/v4/view/ViewPager;

    return-object v0
.end method

.method static synthetic R(Lcom/osp/app/signin/AccountView;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 142
    iget-object v0, p0, Lcom/osp/app/signin/AccountView;->ac:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic S(Lcom/osp/app/signin/AccountView;)V
    .locals 0

    .prologue
    .line 142
    invoke-direct {p0}, Lcom/osp/app/signin/AccountView;->D()V

    return-void
.end method

.method static synthetic T(Lcom/osp/app/signin/AccountView;)Z
    .locals 1

    .prologue
    .line 142
    iget-boolean v0, p0, Lcom/osp/app/signin/AccountView;->ad:Z

    return v0
.end method

.method static synthetic U(Lcom/osp/app/signin/AccountView;)V
    .locals 0

    .prologue
    .line 142
    invoke-direct {p0}, Lcom/osp/app/signin/AccountView;->J()V

    return-void
.end method

.method static synthetic V(Lcom/osp/app/signin/AccountView;)Z
    .locals 1

    .prologue
    .line 142
    iget-boolean v0, p0, Lcom/osp/app/signin/AccountView;->aj:Z

    return v0
.end method

.method static synthetic W(Lcom/osp/app/signin/AccountView;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 142
    iget-object v0, p0, Lcom/osp/app/signin/AccountView;->ai:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic X(Lcom/osp/app/signin/AccountView;)Z
    .locals 1

    .prologue
    .line 142
    invoke-virtual {p0}, Lcom/osp/app/signin/AccountView;->h()Z

    move-result v0

    return v0
.end method

.method static synthetic Y(Lcom/osp/app/signin/AccountView;)Lcom/osp/app/signin/bc;
    .locals 1

    .prologue
    .line 142
    iget-object v0, p0, Lcom/osp/app/signin/AccountView;->as:Lcom/osp/app/signin/bc;

    return-object v0
.end method

.method static synthetic Z(Lcom/osp/app/signin/AccountView;)V
    .locals 0

    .prologue
    .line 142
    invoke-direct {p0}, Lcom/osp/app/signin/AccountView;->B()V

    return-void
.end method

.method static synthetic a(I)I
    .locals 0

    .prologue
    .line 142
    sput p0, Lcom/osp/app/signin/AccountView;->aN:I

    return p0
.end method

.method private a(Landroid/widget/TextView;Landroid/widget/TextView;II)I
    .locals 9

    .prologue
    const/4 v2, 0x1

    const/4 v4, 0x0

    .line 8054
    new-instance v3, Landroid/graphics/Paint;

    invoke-direct {v3}, Landroid/graphics/Paint;-><init>()V

    .line 8057
    invoke-virtual {p1}, Landroid/widget/TextView;->getTypeface()Landroid/graphics/Typeface;

    move-result-object v0

    invoke-virtual {v3, v0}, Landroid/graphics/Paint;->setTypeface(Landroid/graphics/Typeface;)Landroid/graphics/Typeface;

    .line 8058
    invoke-virtual {p1}, Landroid/widget/TextView;->getTextSize()F

    move-result v0

    invoke-virtual {v3, v0}, Landroid/graphics/Paint;->setTextSize(F)V

    .line 8061
    new-instance v5, Ljava/util/StringTokenizer;

    invoke-virtual {p1}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v0

    invoke-interface {v0}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v1, " "

    invoke-direct {v5, v0, v1}, Ljava/util/StringTokenizer;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 8062
    new-instance v0, Ljava/lang/StringBuffer;

    const-string v1, ""

    invoke-direct {v0, v1}, Ljava/lang/StringBuffer;-><init>(Ljava/lang/String;)V

    move v1, v2

    .line 8064
    :cond_0
    :goto_0
    invoke-virtual {v5}, Ljava/util/StringTokenizer;->hasMoreTokens()Z

    move-result v6

    if-eqz v6, :cond_2

    .line 8066
    new-instance v6, Ljava/lang/String;

    invoke-virtual {v5}, Ljava/util/StringTokenizer;->nextToken()Ljava/lang/String;

    move-result-object v7

    invoke-direct {v6, v7}, Ljava/lang/String;-><init>(Ljava/lang/String;)V

    .line 8068
    invoke-virtual {v0, v6}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 8069
    int-to-float v7, p3

    invoke-virtual {v0}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v3, v8}, Landroid/graphics/Paint;->measureText(Ljava/lang/String;)F

    move-result v8

    cmpg-float v7, v7, v8

    if-gez v7, :cond_1

    .line 8071
    add-int/lit8 v1, v1, 0x1

    .line 8072
    new-instance v0, Ljava/lang/StringBuffer;

    invoke-direct {v0, v6}, Ljava/lang/StringBuffer;-><init>(Ljava/lang/String;)V

    .line 8075
    :cond_1
    invoke-virtual {v5}, Ljava/util/StringTokenizer;->hasMoreTokens()Z

    move-result v6

    if-eqz v6, :cond_0

    .line 8077
    const-string v6, " "

    invoke-virtual {v0, v6}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    goto :goto_0

    .line 8081
    :cond_2
    invoke-virtual {p0}, Lcom/osp/app/signin/AccountView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v3, 0x7f080028

    invoke-virtual {v0, v3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    .line 8082
    invoke-virtual {p1}, Landroid/widget/TextView;->getLineHeight()I

    move-result v3

    mul-int/2addr v3, v1

    if-ge v0, v3, :cond_3

    .line 8084
    invoke-virtual {p1}, Landroid/widget/TextView;->getLineHeight()I

    move-result v0

    mul-int/2addr v0, v1

    .line 8087
    :cond_3
    invoke-static {}, Lcom/osp/device/b;->b()Lcom/osp/device/b;

    move-result-object v3

    invoke-virtual {v3, p0}, Lcom/osp/device/b;->i(Landroid/content/Context;)Z

    move-result v3

    if-nez v3, :cond_4

    invoke-virtual {p0}, Lcom/osp/app/signin/AccountView;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    invoke-virtual {v3}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v3

    iget v3, v3, Landroid/content/res/Configuration;->orientation:I

    const/4 v5, 0x2

    if-ne v3, v5, :cond_4

    move v3, v2

    .line 8088
    :goto_1
    if-le v1, v2, :cond_5

    if-eqz v3, :cond_5

    .line 8090
    invoke-virtual {p0}, Lcom/osp/app/signin/AccountView;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f080036

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    .line 8091
    invoke-virtual {p0}, Lcom/osp/app/signin/AccountView;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f080037

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v2

    .line 8095
    invoke-static {p1, v4, v1, v4, v4}, Lcom/msc/sa/c/d;->a(Landroid/view/View;IIII)V

    .line 8096
    invoke-static {p2, v4, v2, v4, v4}, Lcom/msc/sa/c/d;->a(Landroid/view/View;IIII)V

    .line 8105
    :goto_2
    invoke-virtual {p0}, Lcom/osp/app/signin/AccountView;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f080026

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    add-int/2addr v0, v1

    .line 8106
    invoke-virtual {p0}, Lcom/osp/app/signin/AccountView;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f080027

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    add-int/2addr v0, v1

    .line 8107
    invoke-virtual {p1}, Landroid/widget/TextView;->getPaddingTop()I

    move-result v1

    invoke-virtual {p1}, Landroid/widget/TextView;->getPaddingBottom()I

    move-result v2

    add-int/2addr v1, v2

    add-int/2addr v0, v1

    .line 8108
    invoke-virtual {p2}, Landroid/widget/TextView;->getPaddingTop()I

    move-result v1

    invoke-virtual {p2}, Landroid/widget/TextView;->getPaddingBottom()I

    move-result v2

    add-int/2addr v1, v2

    add-int/2addr v0, v1

    .line 8110
    sub-int v1, p4, v0

    .line 8111
    invoke-virtual {p2}, Landroid/widget/TextView;->getLineHeight()I

    move-result v2

    div-int/2addr v1, v2

    invoke-virtual {p2}, Landroid/widget/TextView;->getLineHeight()I

    move-result v2

    mul-int/2addr v1, v2

    .line 8113
    add-int/2addr v0, v1

    .line 8115
    return v0

    :cond_4
    move v3, v4

    .line 8087
    goto :goto_1

    .line 8101
    :cond_5
    invoke-static {p1, v4, v4, v4, v4}, Lcom/msc/sa/c/d;->a(Landroid/view/View;IIII)V

    .line 8102
    invoke-static {p2, v4, v4, v4, v4}, Lcom/msc/sa/c/d;->a(Landroid/view/View;IIII)V

    goto :goto_2
.end method

.method static synthetic a(Lcom/osp/app/signin/AccountView;Landroid/app/AlertDialog;)Landroid/app/AlertDialog;
    .locals 0

    .prologue
    .line 142
    iput-object p1, p0, Lcom/osp/app/signin/AccountView;->aD:Landroid/app/AlertDialog;

    return-object p1
.end method

.method static synthetic a(Lcom/osp/app/signin/AccountView;Landroid/app/ProgressDialog;)Landroid/app/ProgressDialog;
    .locals 0

    .prologue
    .line 142
    iput-object p1, p0, Lcom/osp/app/signin/AccountView;->aa:Landroid/app/ProgressDialog;

    return-object p1
.end method

.method static synthetic a(Ljava/lang/String;)Landroid/graphics/Bitmap;
    .locals 1

    .prologue
    .line 142
    invoke-static {p0}, Lcom/osp/app/signin/AccountView;->j(Ljava/lang/String;)Landroid/graphics/Bitmap;

    move-result-object v0

    return-object v0
.end method

.method static synthetic a(Lcom/osp/app/signin/AccountView;)Lcom/osp/app/signin/BottomSoftkeyLinearLayout;
    .locals 1

    .prologue
    .line 142
    iget-object v0, p0, Lcom/osp/app/signin/AccountView;->aP:Lcom/osp/app/signin/BottomSoftkeyLinearLayout;

    return-object v0
.end method

.method static synthetic a(Lcom/osp/app/signin/AccountView;Lcom/osp/app/signin/bc;)Lcom/osp/app/signin/bc;
    .locals 0

    .prologue
    .line 142
    iput-object p1, p0, Lcom/osp/app/signin/AccountView;->as:Lcom/osp/app/signin/bc;

    return-object p1
.end method

.method static synthetic a(Lcom/osp/app/signin/AccountView;Lcom/osp/app/signin/be;)Lcom/osp/app/signin/be;
    .locals 0

    .prologue
    .line 142
    iput-object p1, p0, Lcom/osp/app/signin/AccountView;->E:Lcom/osp/app/signin/be;

    return-object p1
.end method

.method static synthetic a(Lcom/osp/app/signin/AccountView;Lcom/osp/app/signin/fv;)Lcom/osp/app/signin/fv;
    .locals 0

    .prologue
    .line 142
    iput-object p1, p0, Lcom/osp/app/signin/AccountView;->ak:Lcom/osp/app/signin/fv;

    return-object p1
.end method

.method private static a(Landroid/content/Intent;)Ljava/lang/String;
    .locals 5

    .prologue
    const/4 v0, 0x0

    .line 1899
    if-eqz p0, :cond_2

    .line 1901
    const/4 v1, 0x4

    new-array v2, v1, [Ljava/lang/String;

    const-string v1, "client_id"

    aput-object v1, v2, v0

    const/4 v1, 0x1

    const-string v3, "client_secret"

    aput-object v3, v2, v1

    const/4 v1, 0x2

    const-string v3, "OSP_VER"

    aput-object v3, v2, v1

    const/4 v1, 0x3

    const-string v3, "mypackage"

    aput-object v3, v2, v1

    .line 1903
    array-length v3, v2

    move v1, v0

    :goto_0
    if-ge v1, v3, :cond_1

    aget-object v0, v2, v1

    .line 1905
    invoke-virtual {p0, v0}, Landroid/content/Intent;->hasExtra(Ljava/lang/String;)Z

    move-result v4

    if-nez v4, :cond_0

    .line 1913
    :goto_1
    return-object v0

    .line 1903
    :cond_0
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 1910
    :cond_1
    const/4 v0, 0x0

    goto :goto_1

    .line 1913
    :cond_2
    const-string v0, "client_id"

    goto :goto_1
.end method

.method private a(ILandroid/content/Intent;Landroid/content/Intent;)V
    .locals 2

    .prologue
    .line 1097
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "AccountView::FinishAcitivityResult result : "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " mIsSetupWizardMode : "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-static {}, Lcom/osp/app/signin/SamsungService;->a()Z

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;)V

    .line 1099
    invoke-static {}, Lcom/osp/app/signin/SamsungService;->a()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1101
    sparse-switch p1, :sswitch_data_0

    .line 1116
    const/4 v0, 0x3

    .line 1119
    :goto_0
    invoke-virtual {p0, v0, p2}, Lcom/osp/app/signin/AccountView;->a(ILandroid/content/Intent;)V

    .line 1142
    :cond_0
    :goto_1
    return-void

    .line 1104
    :sswitch_0
    const/4 v0, 0x0

    .line 1105
    goto :goto_0

    .line 1107
    :sswitch_1
    const/4 v0, -0x1

    .line 1108
    goto :goto_0

    .line 1110
    :sswitch_2
    const/4 v0, 0x7

    .line 1111
    goto :goto_0

    .line 1113
    :sswitch_3
    const/16 v0, 0xa

    .line 1114
    goto :goto_0

    .line 1120
    :cond_1
    const-string v0, "BG_mode"

    iget-object v1, p0, Lcom/osp/app/signin/AccountView;->T:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 1122
    if-nez p3, :cond_2

    .line 1124
    new-instance p3, Landroid/content/Intent;

    const-string v0, "com.msc.action.samsungaccount.response.BackGroundSignin"

    invoke-direct {p3, v0}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 1127
    :cond_2
    const-string v0, "bg_result"

    invoke-virtual {p3, v0, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 1129
    iget-object v0, p0, Lcom/osp/app/signin/AccountView;->R:Ljava/lang/String;

    if-eqz v0, :cond_0

    .line 1131
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v1, p0, Lcom/osp/app/signin/AccountView;->R:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ":"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    invoke-virtual {p3, v0}, Landroid/content/Intent;->setData(Landroid/net/Uri;)Landroid/content/Intent;

    .line 1132
    iget-object v0, p0, Lcom/osp/app/signin/AccountView;->R:Ljava/lang/String;

    invoke-virtual {p3, v0}, Landroid/content/Intent;->setPackage(Ljava/lang/String;)Landroid/content/Intent;

    .line 1135
    const-string v0, "client_id"

    iget-object v1, p0, Lcom/osp/app/signin/AccountView;->H:Ljava/lang/String;

    invoke-virtual {p3, v0, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 1136
    iget-wide v0, p0, Lcom/osp/app/signin/AccountView;->Z:J

    invoke-virtual {p0, p3, v0, v1}, Lcom/osp/app/signin/AccountView;->a(Landroid/content/Intent;J)V

    goto :goto_1

    .line 1140
    :cond_3
    invoke-virtual {p0, p1, p2}, Lcom/osp/app/signin/AccountView;->a(ILandroid/content/Intent;)V

    goto :goto_1

    .line 1101
    :sswitch_data_0
    .sparse-switch
        -0x1 -> :sswitch_1
        0x0 -> :sswitch_0
        0x7 -> :sswitch_2
        0xa -> :sswitch_3
    .end sparse-switch
.end method

.method private a(JLandroid/animation/Animator$AnimatorListener;)V
    .locals 5

    .prologue
    .line 10997
    const v0, 0x7f0c006d

    invoke-virtual {p0, v0}, Lcom/osp/app/signin/AccountView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/osp/app/signin/AccountTitleLinearLayout;

    .line 10999
    iget-object v1, p0, Lcom/osp/app/signin/AccountView;->aP:Lcom/osp/app/signin/BottomSoftkeyLinearLayout;

    if-nez v1, :cond_0

    .line 11001
    const v1, 0x7f0c0065

    invoke-virtual {p0, v1}, Lcom/osp/app/signin/AccountView;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/osp/app/signin/BottomSoftkeyLinearLayout;

    iput-object v1, p0, Lcom/osp/app/signin/AccountView;->aP:Lcom/osp/app/signin/BottomSoftkeyLinearLayout;

    .line 11004
    :cond_0
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 11006
    invoke-virtual {p0}, Lcom/osp/app/signin/AccountView;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f080089

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v2

    .line 11007
    invoke-virtual {v0}, Lcom/osp/app/signin/AccountTitleLinearLayout;->getPaddingLeft()I

    move-result v3

    add-int/lit8 v3, v3, 0x78

    .line 11008
    invoke-virtual {v0}, Lcom/osp/app/signin/AccountTitleLinearLayout;->a()Landroid/widget/TextView;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 11016
    int-to-float v0, v3

    invoke-static {v1, v0}, Lcom/osp/app/util/a;->a(Ljava/util/ArrayList;F)Landroid/animation/AnimatorSet;

    move-result-object v0

    iput-object v0, p0, Lcom/osp/app/signin/AccountView;->aR:Landroid/animation/AnimatorSet;

    .line 11019
    iget-object v0, p0, Lcom/osp/app/signin/AccountView;->aP:Lcom/osp/app/signin/BottomSoftkeyLinearLayout;

    const/4 v1, 0x0

    int-to-float v2, v2

    invoke-static {v0, v1, v2}, Lcom/osp/app/util/a;->a(Landroid/view/View;FF)Landroid/animation/Animator;

    move-result-object v0

    .line 11020
    iget-object v1, p0, Lcom/osp/app/signin/AccountView;->aR:Landroid/animation/AnimatorSet;

    const/4 v2, 0x1

    new-array v2, v2, [Landroid/animation/Animator;

    const/4 v3, 0x0

    aput-object v0, v2, v3

    invoke-virtual {v1, v2}, Landroid/animation/AnimatorSet;->playTogether([Landroid/animation/Animator;)V

    .line 11022
    if-eqz p3, :cond_1

    .line 11024
    iget-object v0, p0, Lcom/osp/app/signin/AccountView;->aR:Landroid/animation/AnimatorSet;

    invoke-virtual {v0, p3}, Landroid/animation/AnimatorSet;->addListener(Landroid/animation/Animator$AnimatorListener;)V

    .line 11026
    :cond_1
    iget-object v0, p0, Lcom/osp/app/signin/AccountView;->aR:Landroid/animation/AnimatorSet;

    new-instance v1, Lcom/osp/app/util/b;

    sget-object v2, Lcom/osp/app/util/c;->b:Lcom/osp/app/util/c;

    invoke-direct {v1, v2}, Lcom/osp/app/util/b;-><init>(Lcom/osp/app/util/c;)V

    invoke-virtual {v0, v1}, Landroid/animation/AnimatorSet;->setInterpolator(Landroid/animation/TimeInterpolator;)V

    .line 11027
    iget-object v0, p0, Lcom/osp/app/signin/AccountView;->aR:Landroid/animation/AnimatorSet;

    invoke-virtual {v0, p1, p2}, Landroid/animation/AnimatorSet;->setStartDelay(J)V

    .line 11028
    iget-object v0, p0, Lcom/osp/app/signin/AccountView;->aR:Landroid/animation/AnimatorSet;

    invoke-virtual {v0}, Landroid/animation/AnimatorSet;->start()V

    .line 11029
    return-void
.end method

.method private a(Lcom/msc/a/g;)V
    .locals 4

    .prologue
    const/4 v3, 0x1

    .line 1009
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v0, "AccountView::doCheckList"

    invoke-static {v0}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;)V

    .line 1010
    invoke-direct {p0}, Lcom/osp/app/signin/AccountView;->K()Z

    move-result v0

    if-ne v0, v3, :cond_2

    .line 1012
    invoke-virtual {p1}, Lcom/msc/a/g;->h()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1018
    invoke-static {p0}, Lcom/osp/app/util/ad;->g(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v0

    .line 1019
    invoke-direct {p0, v0, v3}, Lcom/osp/app/signin/AccountView;->a(Ljava/lang/String;Z)V

    .line 1086
    :cond_0
    :goto_0
    return-void

    .line 1023
    :cond_1
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/osp/app/signin/AccountPreference;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 1024
    const/16 v1, 0xda

    invoke-virtual {p0, v0, v1}, Lcom/osp/app/signin/AccountView;->startActivityForResult(Landroid/content/Intent;I)V

    goto :goto_0

    .line 1029
    :cond_2
    invoke-virtual {p1}, Lcom/msc/a/g;->d()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 1031
    invoke-direct {p0}, Lcom/osp/app/signin/AccountView;->t()V

    goto :goto_0

    .line 1035
    :cond_3
    invoke-virtual {p1}, Lcom/msc/a/g;->e()Z

    move-result v0

    if-eqz v0, :cond_4

    .line 1037
    invoke-direct {p0}, Lcom/osp/app/signin/AccountView;->t()V

    goto :goto_0

    .line 1041
    :cond_4
    invoke-virtual {p1}, Lcom/msc/a/g;->g()Z

    move-result v0

    if-eqz v0, :cond_5

    .line 1043
    invoke-virtual {p1}, Lcom/msc/a/g;->b()Ljava/lang/String;

    move-result-object v0

    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "AccountView::showNameValidationView userID : "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;)V

    invoke-virtual {p0}, Lcom/osp/app/signin/AccountView;->isFinishing()Z

    move-result v1

    if-nez v1, :cond_0

    new-instance v1, Landroid/app/AlertDialog$Builder;

    invoke-direct {v1, p0}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    const v2, 0x7f09002d

    invoke-virtual {v1, v2}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    const v2, 0x7f09005f

    invoke-virtual {v1, v2}, Landroid/app/AlertDialog$Builder;->setMessage(I)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    const v2, 0x7f090064

    new-instance v3, Lcom/osp/app/signin/s;

    invoke-direct {v3, p0, v0}, Lcom/osp/app/signin/s;-><init>(Lcom/osp/app/signin/AccountView;Ljava/lang/String;)V

    invoke-virtual {v1, v2, v3}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setCancelable(Z)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    :try_start_0
    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->show()Landroid/app/AlertDialog;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_0

    .line 1048
    :cond_5
    invoke-virtual {p0}, Lcom/osp/app/signin/AccountView;->getIntent()Landroid/content/Intent;

    move-result-object v0

    .line 1049
    if-eqz v0, :cond_6

    .line 1051
    const-string v1, "MODE"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 1052
    const-string v1, "CHANGE_SIM_ALERT"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_6

    invoke-static {p0}, Lcom/osp/app/util/ad;->d(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_6

    .line 1059
    new-instance v0, Landroid/content/Intent;

    const-string v1, "android.intent.action.REGISTRATION_COMPLETED"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 1060
    const-string v1, "login_id"

    invoke-static {p0}, Lcom/osp/app/util/ad;->h(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 1061
    invoke-virtual {p0, v0}, Lcom/osp/app/signin/AccountView;->sendBroadcast(Landroid/content/Intent;)V

    .line 1064
    :cond_6
    invoke-virtual {p1}, Lcom/msc/a/g;->h()Z

    move-result v0

    if-eqz v0, :cond_7

    .line 1070
    invoke-static {p0}, Lcom/osp/app/util/ad;->g(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v0

    .line 1080
    invoke-direct {p0, v0, v3}, Lcom/osp/app/signin/AccountView;->a(Ljava/lang/String;Z)V

    goto/16 :goto_0

    .line 1084
    :cond_7
    invoke-direct {p0}, Lcom/osp/app/signin/AccountView;->s()V

    goto/16 :goto_0
.end method

.method static synthetic a(Lcom/osp/app/signin/AccountView;I)V
    .locals 0

    .prologue
    .line 142
    invoke-direct {p0, p1}, Lcom/osp/app/signin/AccountView;->g(I)V

    return-void
.end method

.method static synthetic a(Lcom/osp/app/signin/AccountView;ILandroid/content/Intent;Landroid/content/Intent;)V
    .locals 0

    .prologue
    .line 142
    invoke-direct {p0, p1, p2, p3}, Lcom/osp/app/signin/AccountView;->a(ILandroid/content/Intent;Landroid/content/Intent;)V

    return-void
.end method

.method static synthetic a(Lcom/osp/app/signin/AccountView;Landroid/animation/Animator$AnimatorListener;)V
    .locals 2

    .prologue
    .line 142
    const-wide/16 v0, 0x64

    invoke-direct {p0, v0, v1, p1}, Lcom/osp/app/signin/AccountView;->a(JLandroid/animation/Animator$AnimatorListener;)V

    return-void
.end method

.method static synthetic a(Lcom/osp/app/signin/AccountView;Lcom/msc/a/g;)V
    .locals 0

    .prologue
    .line 142
    invoke-direct {p0, p1}, Lcom/osp/app/signin/AccountView;->a(Lcom/msc/a/g;)V

    return-void
.end method

.method static synthetic a(Lcom/osp/app/signin/AccountView;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 142
    invoke-direct {p0, p1}, Lcom/osp/app/signin/AccountView;->c(Ljava/lang/String;)V

    return-void
.end method

.method private a(Ljava/lang/String;Z)V
    .locals 4

    .prologue
    .line 2934
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "AccountView::showEmailValidationView emailID : "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " isResendMode : trueisPopupdMode : "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;)V

    .line 2936
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/osp/app/signin/EmailValidationView;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 2937
    const-string v1, "email_id"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 2938
    const-string v1, "is_resend"

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 2939
    const-string v1, "key_verifypopup_mode"

    invoke-virtual {v0, v1, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 2940
    const-string v1, "key_request_id"

    iget-wide v2, p0, Lcom/osp/app/signin/AccountView;->Z:J

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;J)Landroid/content/Intent;

    .line 2941
    const/16 v1, 0xc9

    invoke-virtual {p0, v0, v1}, Lcom/osp/app/signin/AccountView;->startActivityForResult(Landroid/content/Intent;I)V

    .line 2942
    return-void
.end method

.method private a(Z)V
    .locals 6

    .prologue
    const/4 v1, 0x1

    const/4 v0, 0x0

    .line 3171
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "AccountView::ShowVerifyActivity authcode : "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;)V

    .line 3173
    new-instance v2, Landroid/content/Intent;

    invoke-direct {v2}, Landroid/content/Intent;-><init>()V

    .line 3174
    const-class v3, Lcom/osp/app/signin/UserValidateCheck;

    invoke-virtual {v2, p0, v3}, Landroid/content/Intent;->setClass(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;

    .line 3175
    const-string v3, "client_id"

    iget-object v4, p0, Lcom/osp/app/signin/AccountView;->H:Ljava/lang/String;

    invoke-virtual {v2, v3, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 3176
    const-string v3, "client_secret"

    iget-object v4, p0, Lcom/osp/app/signin/AccountView;->I:Ljava/lang/String;

    invoke-virtual {v2, v3, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 3177
    const-string v3, "account_mode"

    iget-object v4, p0, Lcom/osp/app/signin/AccountView;->C:Ljava/lang/String;

    invoke-virtual {v2, v3, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 3178
    const-string v3, "service_name"

    iget-object v4, p0, Lcom/osp/app/signin/AccountView;->N:Ljava/lang/String;

    invoke-virtual {v2, v3, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 3179
    const-string v3, "OSP_VER"

    iget-object v4, p0, Lcom/osp/app/signin/AccountView;->L:Ljava/lang/String;

    invoke-virtual {v2, v3, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 3180
    const-string v3, "BG_WhoareU"

    iget-object v4, p0, Lcom/osp/app/signin/AccountView;->T:Ljava/lang/String;

    invoke-virtual {v2, v3, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 3181
    const-string v3, "BG_mode"

    iget-object v4, p0, Lcom/osp/app/signin/AccountView;->T:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 3183
    iget-object v3, p0, Lcom/osp/app/signin/AccountView;->R:Ljava/lang/String;

    if-eqz v3, :cond_0

    .line 3185
    const-string v3, "mypackage"

    iget-object v4, p0, Lcom/osp/app/signin/AccountView;->R:Ljava/lang/String;

    invoke-virtual {v2, v3, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 3187
    :cond_0
    const-string v3, "key_request_id"

    iget-wide v4, p0, Lcom/osp/app/signin/AccountView;->Z:J

    invoke-virtual {v2, v3, v4, v5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;J)Landroid/content/Intent;

    .line 3189
    :cond_1
    if-eqz p1, :cond_2

    .line 3191
    const-string v3, "NEED_AUTHCODE"

    invoke-virtual {v2, v3, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 3193
    :cond_2
    const-string v3, "more_info"

    iget-object v4, p0, Lcom/osp/app/signin/AccountView;->X:Ljava/lang/String;

    invoke-virtual {v2, v3, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 3194
    invoke-static {}, Lcom/osp/app/util/p;->a()Lcom/osp/app/util/p;

    move-result-object v3

    invoke-virtual {p0}, Lcom/osp/app/signin/AccountView;->getApplicationContext()Landroid/content/Context;

    move-result-object v4

    invoke-virtual {v3, v4}, Lcom/osp/app/util/p;->c(Landroid/content/Context;)Z

    move-result v3

    if-eqz v3, :cond_3

    const-string v3, "setting_finger"

    iget-object v4, p0, Lcom/osp/app/signin/AccountView;->X:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3

    .line 3196
    const-string v3, "set_value"

    iget-object v4, p0, Lcom/osp/app/signin/AccountView;->Y:Ljava/lang/String;

    invoke-virtual {v2, v3, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 3199
    :cond_3
    invoke-virtual {p0}, Lcom/osp/app/signin/AccountView;->getCallingPackage()Ljava/lang/String;

    move-result-object v3

    iget-object v4, p0, Lcom/osp/app/signin/AccountView;->J:Landroid/content/Intent;

    if-eqz v4, :cond_5

    iget-object v4, p0, Lcom/osp/app/signin/AccountView;->J:Landroid/content/Intent;

    const-string v5, "more_info"

    invoke-virtual {v4, v5}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v5

    if-nez v5, :cond_5

    const-string v5, "setting_finger"

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_5

    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v3, "AV"

    const-string v4, "isSignOutRequestFromSettings : false (FP)"

    invoke-static {v3, v4}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;)V

    :goto_0
    if-eqz v0, :cond_4

    .line 3211
    const-string v0, "key_signout_request_from_settings"

    invoke-virtual {v2, v0, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 3215
    :cond_4
    const/16 v0, 0xcf

    invoke-virtual {p0, v2, v0}, Lcom/osp/app/signin/AccountView;->startActivityForResult(Landroid/content/Intent;I)V

    .line 3216
    return-void

    .line 3199
    :cond_5
    iget-object v4, p0, Lcom/osp/app/signin/AccountView;->H:Ljava/lang/String;

    if-eqz v4, :cond_6

    if-eqz v3, :cond_6

    const-string v4, "s5d189ajvs"

    iget-object v5, p0, Lcom/osp/app/signin/AccountView;->H:Ljava/lang/String;

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_6

    const-string v4, "com.android.settings"

    invoke-virtual {v4, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_6

    move v0, v1

    :cond_6
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v3, "AV"

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "isSignOutRequestFromSettings : "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method private a(ZZ)V
    .locals 12

    .prologue
    const/16 v11, 0x8

    const/4 v10, 0x1

    const/4 v9, 0x2

    const v8, 0x7f07000c

    const/4 v7, 0x0

    .line 2282
    invoke-static {p0}, Lcom/osp/app/util/r;->g(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-static {}, Lcom/osp/app/signin/SamsungService;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2284
    if-eqz p1, :cond_26

    .line 2286
    iget-object v0, p0, Lcom/osp/app/signin/AccountView;->aR:Landroid/animation/AnimatorSet;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/osp/app/signin/AccountView;->aR:Landroid/animation/AnimatorSet;

    invoke-virtual {v0}, Landroid/animation/AnimatorSet;->isRunning()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2288
    iget-object v0, p0, Lcom/osp/app/signin/AccountView;->aR:Landroid/animation/AnimatorSet;

    invoke-virtual {v0}, Landroid/animation/AnimatorSet;->cancel()V

    .line 2297
    :cond_0
    iput-boolean v7, p0, Lcom/osp/app/signin/AccountView;->aQ:Z

    .line 2298
    const-string v0, ""

    iput-object v0, p0, Lcom/osp/app/signin/AccountView;->W:Ljava/lang/String;

    .line 2301
    const v0, 0x7f030008

    invoke-virtual {p0, v0}, Lcom/osp/app/signin/AccountView;->setContentView(I)V

    .line 2304
    invoke-virtual {p0}, Lcom/osp/app/signin/AccountView;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-static {p0}, Lcom/osp/app/util/r;->w(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_29

    iget-object v0, p0, Lcom/osp/app/signin/AccountView;->aM:Lcom/osp/app/signin/h;

    const v2, 0x2063bf

    invoke-virtual {v0, v2}, Lcom/osp/app/signin/h;->a(I)Z

    move-result v0

    if-eqz v0, :cond_29

    const v0, 0x7f0c005d

    invoke-virtual {p0, v0}, Lcom/osp/app/signin/AccountView;->findViewById(I)Landroid/view/View;

    move-result-object v2

    if-eqz v2, :cond_1

    invoke-virtual {v2}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout$LayoutParams;

    const/4 v3, 0x0

    iput v3, v0, Landroid/widget/LinearLayout$LayoutParams;->weight:F

    invoke-virtual {p0}, Lcom/osp/app/signin/AccountView;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    invoke-virtual {v3}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v3

    iget v3, v3, Landroid/content/res/Configuration;->orientation:I

    if-ne v3, v9, :cond_27

    invoke-virtual {p0}, Lcom/osp/app/signin/AccountView;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f08000f

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v3

    float-to-int v3, v3

    iput v3, v0, Landroid/widget/LinearLayout$LayoutParams;->bottomMargin:I

    :goto_0
    invoke-virtual {v2, v0}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    invoke-virtual {v2}, Landroid/view/View;->getVisibility()I

    move-result v0

    if-ne v0, v11, :cond_1

    invoke-static {p0}, Lcom/osp/app/util/r;->g(Landroid/content/Context;)Z

    move-result v0

    if-nez v0, :cond_28

    const/4 v0, 0x4

    invoke-virtual {v2, v0}, Landroid/view/View;->setVisibility(I)V

    :cond_1
    :goto_1
    const v0, 0x7f0c005f

    invoke-virtual {p0, v0}, Lcom/osp/app/signin/AccountView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    if-eqz v0, :cond_2

    invoke-virtual {v0, v11}, Landroid/view/View;->setVisibility(I)V

    :cond_2
    invoke-static {}, Lcom/osp/device/b;->b()Lcom/osp/device/b;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/osp/device/b;->i(Landroid/content/Context;)Z

    move-result v0

    if-nez v0, :cond_3

    invoke-virtual {p0}, Lcom/osp/app/signin/AccountView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v0

    iget v0, v0, Landroid/content/res/Configuration;->orientation:I

    if-eq v0, v9, :cond_3

    const v0, 0x7f0c003f

    invoke-virtual {p0, v0}, Lcom/osp/app/signin/AccountView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/View;->getPaddingTop()I

    move-result v2

    invoke-virtual {v0}, Landroid/view/View;->getPaddingBottom()I

    move-result v3

    invoke-virtual {p0}, Lcom/osp/app/signin/AccountView;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const v5, 0x7f080041

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v4

    float-to-int v4, v4

    invoke-virtual {p0}, Lcom/osp/app/signin/AccountView;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    const v6, 0x7f080040

    invoke-virtual {v5, v6}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v5

    float-to-int v5, v5

    invoke-static {v0, v5, v2, v4, v3}, Lcom/msc/sa/c/d;->a(Landroid/view/View;IIII)V

    :cond_3
    invoke-direct {p0}, Lcom/osp/app/signin/AccountView;->M()V

    :goto_2
    const v0, 0x7f0c0044

    invoke-virtual {p0, v0}, Lcom/osp/app/signin/AccountView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    if-eqz v0, :cond_5

    invoke-static {p0}, Lcom/osp/app/util/r;->m(Landroid/content/Context;)Z

    move-result v2

    if-eqz v2, :cond_4

    invoke-virtual {v0, v7}, Landroid/widget/TextView;->setIncludeFontPadding(Z)V

    :cond_4
    invoke-static {p0}, Lcom/osp/app/util/an;->a(Lcom/osp/app/util/BaseActivity;)Lcom/osp/app/util/an;

    const v2, 0x7f07000a

    invoke-static {v2}, Lcom/osp/app/util/an;->a(I)I

    move-result v2

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v2

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setTextColor(I)V

    invoke-static {p0}, Lcom/osp/app/util/an;->a(Lcom/osp/app/util/BaseActivity;)Lcom/osp/app/util/an;

    const v2, 0x7f070014

    invoke-static {v2}, Lcom/osp/app/util/an;->a(I)I

    move-result v2

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v2

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setLinkTextColor(I)V

    :cond_5
    const v0, 0x7f0c0053

    invoke-virtual {p0, v0}, Lcom/osp/app/signin/AccountView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    if-eqz v0, :cond_6

    invoke-static {p0}, Lcom/osp/app/util/an;->a(Lcom/osp/app/util/BaseActivity;)Lcom/osp/app/util/an;

    invoke-static {v8}, Lcom/osp/app/util/an;->a(I)I

    move-result v2

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v2

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setTextColor(I)V

    :cond_6
    const v0, 0x7f0c0056

    invoke-virtual {p0, v0}, Lcom/osp/app/signin/AccountView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    if-eqz v0, :cond_7

    invoke-static {p0}, Lcom/osp/app/util/an;->a(Lcom/osp/app/util/BaseActivity;)Lcom/osp/app/util/an;

    invoke-static {v8}, Lcom/osp/app/util/an;->a(I)I

    move-result v2

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v2

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setTextColor(I)V

    :cond_7
    const v0, 0x7f0c0054

    invoke-virtual {p0, v0}, Lcom/osp/app/signin/AccountView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    if-eqz v0, :cond_8

    invoke-static {p0}, Lcom/osp/app/util/an;->a(Lcom/osp/app/util/BaseActivity;)Lcom/osp/app/util/an;

    invoke-static {v8}, Lcom/osp/app/util/an;->a(I)I

    move-result v2

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v2

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setTextColor(I)V

    invoke-static {p0}, Lcom/osp/app/util/an;->a(Lcom/osp/app/util/BaseActivity;)Lcom/osp/app/util/an;

    const v2, 0x7f070014

    invoke-static {v2}, Lcom/osp/app/util/an;->a(I)I

    move-result v2

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v2

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setLinkTextColor(I)V

    invoke-static {}, Lcom/osp/device/b;->b()Lcom/osp/device/b;

    move-result-object v2

    invoke-virtual {v2, p0}, Lcom/osp/device/b;->i(Landroid/content/Context;)Z

    move-result v2

    if-eqz v2, :cond_8

    invoke-static {p0}, Lcom/osp/app/util/an;->a(Lcom/osp/app/util/BaseActivity;)Lcom/osp/app/util/an;

    const v2, 0x7f080001

    invoke-static {v2}, Lcom/osp/app/util/an;->b(I)I

    move-result v2

    invoke-virtual {p0}, Lcom/osp/app/signin/AccountView;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    invoke-virtual {v3, v2}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v2

    invoke-virtual {v0, v7, v2}, Landroid/widget/TextView;->setTextSize(IF)V

    :cond_8
    const v0, 0x7f0c0047

    invoke-virtual {p0, v0}, Lcom/osp/app/signin/AccountView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    if-eqz v0, :cond_9

    invoke-static {p0}, Lcom/osp/app/util/an;->a(Lcom/osp/app/util/BaseActivity;)Lcom/osp/app/util/an;

    invoke-static {v8}, Lcom/osp/app/util/an;->a(I)I

    move-result v2

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v2

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setTextColor(I)V

    :cond_9
    const v0, 0x7f0c004a

    invoke-virtual {p0, v0}, Lcom/osp/app/signin/AccountView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    if-eqz v0, :cond_a

    invoke-static {p0}, Lcom/osp/app/util/an;->a(Lcom/osp/app/util/BaseActivity;)Lcom/osp/app/util/an;

    invoke-static {v8}, Lcom/osp/app/util/an;->a(I)I

    move-result v2

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v2

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setTextColor(I)V

    :cond_a
    const v0, 0x7f0c0048

    invoke-virtual {p0, v0}, Lcom/osp/app/signin/AccountView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    if-eqz v0, :cond_b

    invoke-static {p0}, Lcom/osp/app/util/an;->a(Lcom/osp/app/util/BaseActivity;)Lcom/osp/app/util/an;

    invoke-static {v8}, Lcom/osp/app/util/an;->a(I)I

    move-result v2

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v2

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setTextColor(I)V

    invoke-static {p0}, Lcom/osp/app/util/an;->a(Lcom/osp/app/util/BaseActivity;)Lcom/osp/app/util/an;

    const v2, 0x7f070014

    invoke-static {v2}, Lcom/osp/app/util/an;->a(I)I

    move-result v2

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v2

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setLinkTextColor(I)V

    invoke-static {}, Lcom/osp/device/b;->b()Lcom/osp/device/b;

    move-result-object v2

    invoke-virtual {v2, p0}, Lcom/osp/device/b;->i(Landroid/content/Context;)Z

    move-result v2

    if-eqz v2, :cond_b

    invoke-static {p0}, Lcom/osp/app/util/an;->a(Lcom/osp/app/util/BaseActivity;)Lcom/osp/app/util/an;

    const v2, 0x7f080001

    invoke-static {v2}, Lcom/osp/app/util/an;->b(I)I

    move-result v2

    invoke-virtual {p0}, Lcom/osp/app/signin/AccountView;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    invoke-virtual {v3, v2}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v2

    invoke-virtual {v0, v7, v2}, Landroid/widget/TextView;->setTextSize(IF)V

    :cond_b
    const v0, 0x7f0c004d

    invoke-virtual {p0, v0}, Lcom/osp/app/signin/AccountView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    if-eqz v0, :cond_c

    invoke-static {p0}, Lcom/osp/app/util/an;->a(Lcom/osp/app/util/BaseActivity;)Lcom/osp/app/util/an;

    invoke-static {v8}, Lcom/osp/app/util/an;->a(I)I

    move-result v2

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v2

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setTextColor(I)V

    :cond_c
    const v0, 0x7f0c0050

    invoke-virtual {p0, v0}, Lcom/osp/app/signin/AccountView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    if-eqz v0, :cond_d

    invoke-static {p0}, Lcom/osp/app/util/an;->a(Lcom/osp/app/util/BaseActivity;)Lcom/osp/app/util/an;

    invoke-static {v8}, Lcom/osp/app/util/an;->a(I)I

    move-result v2

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v2

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setTextColor(I)V

    :cond_d
    const v0, 0x7f0c004e

    invoke-virtual {p0, v0}, Lcom/osp/app/signin/AccountView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    if-eqz v0, :cond_e

    invoke-static {p0}, Lcom/osp/app/util/an;->a(Lcom/osp/app/util/BaseActivity;)Lcom/osp/app/util/an;

    invoke-static {v8}, Lcom/osp/app/util/an;->a(I)I

    move-result v2

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v2

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setTextColor(I)V

    invoke-static {p0}, Lcom/osp/app/util/an;->a(Lcom/osp/app/util/BaseActivity;)Lcom/osp/app/util/an;

    const v2, 0x7f070014

    invoke-static {v2}, Lcom/osp/app/util/an;->a(I)I

    move-result v2

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v2

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setLinkTextColor(I)V

    invoke-static {}, Lcom/osp/device/b;->b()Lcom/osp/device/b;

    move-result-object v2

    invoke-virtual {v2, p0}, Lcom/osp/device/b;->i(Landroid/content/Context;)Z

    move-result v2

    if-eqz v2, :cond_e

    invoke-static {p0}, Lcom/osp/app/util/an;->a(Lcom/osp/app/util/BaseActivity;)Lcom/osp/app/util/an;

    const v2, 0x7f080001

    invoke-static {v2}, Lcom/osp/app/util/an;->b(I)I

    move-result v2

    invoke-virtual {p0}, Lcom/osp/app/signin/AccountView;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    invoke-virtual {v3, v2}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v2

    invoke-virtual {v0, v7, v2}, Landroid/widget/TextView;->setTextSize(IF)V

    :cond_e
    const v0, 0x7f0c0059

    invoke-virtual {p0, v0}, Lcom/osp/app/signin/AccountView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    if-eqz v0, :cond_f

    invoke-static {p0}, Lcom/osp/app/util/an;->a(Lcom/osp/app/util/BaseActivity;)Lcom/osp/app/util/an;

    invoke-static {v8}, Lcom/osp/app/util/an;->a(I)I

    move-result v2

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v2

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setTextColor(I)V

    :cond_f
    const v0, 0x7f0c005c

    invoke-virtual {p0, v0}, Lcom/osp/app/signin/AccountView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    if-eqz v0, :cond_10

    invoke-static {p0}, Lcom/osp/app/util/an;->a(Lcom/osp/app/util/BaseActivity;)Lcom/osp/app/util/an;

    invoke-static {v8}, Lcom/osp/app/util/an;->a(I)I

    move-result v2

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v2

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setTextColor(I)V

    :cond_10
    const v0, 0x7f0c005a

    invoke-virtual {p0, v0}, Lcom/osp/app/signin/AccountView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    if-eqz v0, :cond_11

    invoke-static {p0}, Lcom/osp/app/util/an;->a(Lcom/osp/app/util/BaseActivity;)Lcom/osp/app/util/an;

    invoke-static {v8}, Lcom/osp/app/util/an;->a(I)I

    move-result v2

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v2

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setTextColor(I)V

    invoke-static {p0}, Lcom/osp/app/util/an;->a(Lcom/osp/app/util/BaseActivity;)Lcom/osp/app/util/an;

    const v2, 0x7f070014

    invoke-static {v2}, Lcom/osp/app/util/an;->a(I)I

    move-result v2

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setLinkTextColor(I)V

    invoke-static {}, Lcom/osp/device/b;->b()Lcom/osp/device/b;

    move-result-object v1

    invoke-virtual {v1, p0}, Lcom/osp/device/b;->i(Landroid/content/Context;)Z

    move-result v1

    if-eqz v1, :cond_11

    invoke-static {p0}, Lcom/osp/app/util/an;->a(Lcom/osp/app/util/BaseActivity;)Lcom/osp/app/util/an;

    const v1, 0x7f080001

    invoke-static {v1}, Lcom/osp/app/util/an;->b(I)I

    move-result v1

    invoke-virtual {p0}, Lcom/osp/app/signin/AccountView;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-virtual {v2, v1}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v1

    invoke-virtual {v0, v7, v1}, Landroid/widget/TextView;->setTextSize(IF)V

    :cond_11
    invoke-virtual {p0}, Lcom/osp/app/signin/AccountView;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    new-instance v0, Landroid/util/DisplayMetrics;

    invoke-direct {v0}, Landroid/util/DisplayMetrics;-><init>()V

    invoke-virtual {p0}, Lcom/osp/app/signin/AccountView;->getWindowManager()Landroid/view/WindowManager;

    move-result-object v2

    invoke-interface {v2}, Landroid/view/WindowManager;->getDefaultDisplay()Landroid/view/Display;

    move-result-object v2

    invoke-virtual {v2, v0}, Landroid/view/Display;->getMetrics(Landroid/util/DisplayMetrics;)V

    iget v0, v0, Landroid/util/DisplayMetrics;->density:F

    const/high16 v2, 0x41200000    # 10.0f

    mul-float/2addr v0, v2

    float-to-int v2, v0

    const v0, 0x7f0c0063

    invoke-virtual {p0, v0}, Lcom/osp/app/signin/AccountView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    if-eqz v0, :cond_14

    invoke-static {p0}, Lcom/osp/app/util/an;->a(Lcom/osp/app/util/BaseActivity;)Lcom/osp/app/util/an;

    const v3, 0x7f080017

    invoke-static {v3}, Lcom/osp/app/util/an;->a(I)I

    move-result v3

    invoke-virtual {v1, v3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v3

    invoke-virtual {v0, v3}, Landroid/widget/Button;->setMinimumHeight(I)V

    invoke-static {p0}, Lcom/osp/app/util/r;->m(Landroid/content/Context;)Z

    move-result v3

    if-nez v3, :cond_13

    invoke-static {p0}, Lcom/osp/app/util/r;->g(Landroid/content/Context;)Z

    move-result v3

    if-nez v3, :cond_13

    invoke-static {p0}, Lcom/osp/app/util/an;->a(Lcom/osp/app/util/BaseActivity;)Lcom/osp/app/util/an;

    const v3, 0x7f02005a

    invoke-static {v3}, Lcom/osp/app/util/an;->a(I)I

    move-result v3

    invoke-virtual {v0, v3}, Landroid/widget/Button;->setBackgroundResource(I)V

    invoke-static {p0}, Lcom/osp/app/util/an;->a(Lcom/osp/app/util/BaseActivity;)Lcom/osp/app/util/an;

    const v3, 0x7f070018

    invoke-static {v3}, Lcom/osp/app/util/an;->a(I)I

    move-result v3

    invoke-virtual {v1, v3}, Landroid/content/res/Resources;->getColor(I)I

    move-result v3

    invoke-virtual {v0, v3}, Landroid/widget/Button;->setTextColor(I)V

    invoke-static {p0}, Lcom/osp/app/util/r;->j(Landroid/content/Context;)Z

    move-result v3

    if-nez v3, :cond_12

    invoke-static {}, Lcom/osp/app/util/r;->m()Z

    move-result v3

    if-eqz v3, :cond_13

    :cond_12
    invoke-static {p0}, Lcom/osp/app/util/an;->a(Lcom/osp/app/util/BaseActivity;)Lcom/osp/app/util/an;

    const v3, 0x7f02005c

    invoke-static {v3}, Lcom/osp/app/util/an;->a(I)I

    move-result v3

    invoke-virtual {v1, v3}, Landroid/content/res/Resources;->getColorStateList(I)Landroid/content/res/ColorStateList;

    move-result-object v3

    invoke-virtual {v0, v3}, Landroid/widget/Button;->setTextColor(Landroid/content/res/ColorStateList;)V

    :cond_13
    invoke-static {v0, v2, v7, v2, v7}, Lcom/msc/sa/c/d;->a(Landroid/view/View;IIII)V

    :cond_14
    const v0, 0x7f0c0062

    invoke-virtual {p0, v0}, Lcom/osp/app/signin/AccountView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    if-eqz v0, :cond_16

    invoke-static {p0}, Lcom/osp/app/util/an;->a(Lcom/osp/app/util/BaseActivity;)Lcom/osp/app/util/an;

    const v3, 0x7f080017

    invoke-static {v3}, Lcom/osp/app/util/an;->a(I)I

    move-result v3

    invoke-virtual {v1, v3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v3

    invoke-virtual {v0, v3}, Landroid/widget/Button;->setMinimumHeight(I)V

    invoke-static {p0}, Lcom/osp/app/util/r;->g(Landroid/content/Context;)Z

    move-result v3

    if-nez v3, :cond_15

    invoke-static {p0}, Lcom/osp/app/util/r;->m(Landroid/content/Context;)Z

    move-result v3

    if-eqz v3, :cond_2e

    invoke-direct {p0}, Lcom/osp/app/signin/AccountView;->E()Z

    move-result v3

    if-nez v3, :cond_15

    invoke-static {p0}, Lcom/osp/app/util/an;->a(Lcom/osp/app/util/BaseActivity;)Lcom/osp/app/util/an;

    const v3, 0x7f02005a

    invoke-static {v3}, Lcom/osp/app/util/an;->a(I)I

    move-result v3

    invoke-virtual {v0, v3}, Landroid/widget/Button;->setBackgroundResource(I)V

    const/4 v3, -0x1

    invoke-virtual {v0, v3}, Landroid/widget/Button;->setTextColor(I)V

    :cond_15
    :goto_3
    invoke-static {v0, v2, v7, v2, v7}, Lcom/msc/sa/c/d;->a(Landroid/view/View;IIII)V

    :cond_16
    const v0, 0x7f0c0061

    invoke-virtual {p0, v0}, Lcom/osp/app/signin/AccountView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    invoke-static {}, Lcom/osp/app/util/r;->q()Z

    move-result v3

    if-eqz v3, :cond_18

    if-eqz v0, :cond_18

    invoke-static {p0}, Lcom/osp/app/util/an;->a(Lcom/osp/app/util/BaseActivity;)Lcom/osp/app/util/an;

    const v3, 0x7f080017

    invoke-static {v3}, Lcom/osp/app/util/an;->a(I)I

    move-result v3

    invoke-virtual {v1, v3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v3

    invoke-virtual {v0, v3}, Landroid/widget/Button;->setMinimumHeight(I)V

    invoke-static {p0}, Lcom/osp/app/util/r;->g(Landroid/content/Context;)Z

    move-result v3

    if-nez v3, :cond_17

    invoke-static {p0}, Lcom/osp/app/util/an;->a(Lcom/osp/app/util/BaseActivity;)Lcom/osp/app/util/an;

    const v3, 0x7f02005a

    invoke-static {v3}, Lcom/osp/app/util/an;->a(I)I

    move-result v3

    invoke-virtual {v0, v3}, Landroid/widget/Button;->setBackgroundResource(I)V

    invoke-static {p0}, Lcom/osp/app/util/r;->m(Landroid/content/Context;)Z

    move-result v3

    if-eqz v3, :cond_30

    const/4 v1, -0x1

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setTextColor(I)V

    :cond_17
    :goto_4
    invoke-static {v0, v2, v7, v2, v7}, Lcom/msc/sa/c/d;->a(Landroid/view/View;IIII)V

    invoke-direct {p0}, Lcom/osp/app/signin/AccountView;->E()Z

    move-result v1

    if-eqz v1, :cond_31

    invoke-virtual {v0, v7}, Landroid/widget/Button;->setVisibility(I)V

    new-instance v1, Lcom/osp/app/signin/v;

    invoke-direct {v1, p0}, Lcom/osp/app/signin/v;-><init>(Lcom/osp/app/signin/AccountView;)V

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    :cond_18
    :goto_5
    const v0, 0x7f0c0065

    invoke-virtual {p0, v0}, Lcom/osp/app/signin/AccountView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/osp/app/signin/BottomSoftkeyLinearLayout;

    iput-object v0, p0, Lcom/osp/app/signin/AccountView;->aP:Lcom/osp/app/signin/BottomSoftkeyLinearLayout;

    iget-object v0, p0, Lcom/osp/app/signin/AccountView;->aP:Lcom/osp/app/signin/BottomSoftkeyLinearLayout;

    if-eqz v0, :cond_1a

    sget-object v0, Lcom/osp/app/util/u;->o:[Ljava/lang/String;

    invoke-static {v0}, Lcom/osp/app/util/u;->a([Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_19

    iget-object v0, p0, Lcom/osp/app/signin/AccountView;->aP:Lcom/osp/app/signin/BottomSoftkeyLinearLayout;

    invoke-virtual {v0}, Lcom/osp/app/signin/BottomSoftkeyLinearLayout;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup$MarginLayoutParams;

    iput v7, v0, Landroid/view/ViewGroup$MarginLayoutParams;->leftMargin:I

    iput v7, v0, Landroid/view/ViewGroup$MarginLayoutParams;->rightMargin:I

    iget-object v1, p0, Lcom/osp/app/signin/AccountView;->aP:Lcom/osp/app/signin/BottomSoftkeyLinearLayout;

    invoke-static {v1, v0}, Lcom/msc/sa/c/d;->a(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    :cond_19
    iget-object v0, p0, Lcom/osp/app/signin/AccountView;->aP:Lcom/osp/app/signin/BottomSoftkeyLinearLayout;

    invoke-virtual {p0}, Lcom/osp/app/signin/AccountView;->h()Z

    move-result v1

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-virtual {v0, p0, v1}, Lcom/osp/app/signin/BottomSoftkeyLinearLayout;->a(Landroid/content/Context;Ljava/lang/Boolean;)V

    :cond_1a
    invoke-static {}, Lcom/osp/app/signin/SamsungService;->a()Z

    move-result v0

    if-eqz v0, :cond_1b

    invoke-static {p0}, Lcom/osp/app/util/r;->g(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_1b

    iget-object v0, p0, Lcom/osp/app/signin/AccountView;->aR:Landroid/animation/AnimatorSet;

    if-nez v0, :cond_1b

    invoke-direct {p0}, Lcom/osp/app/signin/AccountView;->r()Z

    move-result v0

    if-nez v0, :cond_1b

    const-wide/16 v0, 0x1f4

    const/4 v2, 0x0

    invoke-direct {p0, v0, v1, v2}, Lcom/osp/app/signin/AccountView;->a(JLandroid/animation/Animator$AnimatorListener;)V

    .line 2306
    :cond_1b
    iget-boolean v0, p0, Lcom/osp/app/signin/AccountView;->aq:Z

    if-eqz v0, :cond_1c

    if-nez p1, :cond_1c

    .line 2308
    const/16 v0, 0x12e

    const/4 v1, 0x0

    new-instance v2, Lcom/osp/app/signin/au;

    invoke-direct {v2, p0}, Lcom/osp/app/signin/au;-><init>(Lcom/osp/app/signin/AccountView;)V

    invoke-virtual {p0, v0, v1, v2}, Lcom/osp/app/signin/AccountView;->a(ILandroid/content/DialogInterface$OnDismissListener;Landroid/content/DialogInterface$OnCancelListener;)V

    .line 2324
    :cond_1c
    invoke-direct {p0}, Lcom/osp/app/signin/AccountView;->w()Z

    .line 2333
    const v0, 0x7f0c003e

    invoke-virtual {p0, v0}, Lcom/osp/app/signin/AccountView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    .line 2334
    invoke-static {p0}, Lcom/osp/app/util/r;->g(Landroid/content/Context;)Z

    move-result v1

    if-eqz v1, :cond_1d

    .line 2336
    invoke-virtual {p0}, Lcom/osp/app/signin/AccountView;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f07003a

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/view/View;->setBackgroundColor(I)V

    .line 2337
    const v0, 0x7f0c0069

    invoke-virtual {p0, v0}, Lcom/osp/app/signin/AccountView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    .line 2340
    :cond_1d
    if-eqz v0, :cond_1f

    .line 2342
    invoke-virtual {p0}, Lcom/osp/app/signin/AccountView;->o()I

    move-result v1

    .line 2344
    invoke-static {p0}, Lcom/osp/app/util/r;->i(Landroid/content/Context;)Z

    move-result v2

    if-eqz v2, :cond_32

    .line 2348
    invoke-static {v0, v7, v1, v7, v7}, Lcom/msc/sa/c/d;->a(Landroid/view/View;IIII)V

    .line 2357
    :cond_1e
    :goto_6
    invoke-virtual {p0, v0}, Lcom/osp/app/signin/AccountView;->a(Landroid/view/View;)V

    .line 2371
    :cond_1f
    invoke-virtual {p0}, Lcom/osp/app/signin/AccountView;->h()Z

    move-result v0

    invoke-static {p0, v0}, Lcom/msc/c/n;->a(Landroid/content/Context;Z)Ljava/lang/String;

    move-result-object v2

    .line 2373
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "[AV] linkDestination = [ "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " ]"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;)V

    .line 2375
    const v0, 0x7f0c0044

    invoke-virtual {p0, v0}, Lcom/osp/app/signin/AccountView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    if-eqz v0, :cond_21

    .line 2378
    const v0, 0x7f0c0044

    invoke-virtual {p0, v0}, Lcom/osp/app/signin/AccountView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    const v1, 0x7f0c005a

    invoke-virtual {p0, v1}, Lcom/osp/app/signin/AccountView;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    if-eqz v1, :cond_3a

    const v3, 0x7f09012d

    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setText(I)V

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v0

    invoke-interface {v0}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v3, "\n"

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v3, p0, Lcom/osp/app/signin/AccountView;->W:Ljava/lang/String;

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/osp/app/signin/AccountView;->W:Ljava/lang/String;

    :goto_7
    if-eqz v1, :cond_20

    invoke-virtual {v1}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v0

    check-cast v0, Landroid/text/Spannable;

    new-instance v3, Lcom/osp/app/signin/AccountView$11;

    invoke-direct {v3, p0, v2}, Lcom/osp/app/signin/AccountView$11;-><init>(Lcom/osp/app/signin/AccountView;Ljava/lang/String;)V

    invoke-interface {v0}, Landroid/text/Spannable;->length()I

    move-result v2

    invoke-interface {v0, v3, v7, v2, v7}, Landroid/text/Spannable;->setSpan(Ljava/lang/Object;III)V

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    const-string v2, " "

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->append(Ljava/lang/CharSequence;)V

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setContentDescription(Ljava/lang/CharSequence;)V

    invoke-static {}, Landroid/text/method/LinkMovementMethod;->getInstance()Landroid/text/method/MovementMethod;

    move-result-object v0

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setMovementMethod(Landroid/text/method/MovementMethod;)V

    .line 2397
    :cond_20
    invoke-static {p0}, Lcom/osp/app/util/r;->w(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_33

    iget-object v0, p0, Lcom/osp/app/signin/AccountView;->aM:Lcom/osp/app/signin/h;

    const v1, 0x2063bf

    invoke-virtual {v0, v1}, Lcom/osp/app/signin/h;->a(I)Z

    move-result v0

    if-eqz v0, :cond_33

    .line 2399
    invoke-direct {p0}, Lcom/osp/app/signin/AccountView;->M()V

    .line 2424
    :cond_21
    :goto_8
    const v0, 0x7f0c0063

    invoke-virtual {p0, v0}, Lcom/osp/app/signin/AccountView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    .line 2425
    new-instance v1, Lcom/osp/app/signin/av;

    invoke-direct {v1, p0}, Lcom/osp/app/signin/av;-><init>(Lcom/osp/app/signin/AccountView;)V

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2454
    const v0, 0x7f0c0062

    invoke-virtual {p0, v0}, Lcom/osp/app/signin/AccountView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    .line 2455
    new-instance v1, Lcom/osp/app/signin/p;

    invoke-direct {v1, p0}, Lcom/osp/app/signin/p;-><init>(Lcom/osp/app/signin/AccountView;)V

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2482
    iget-boolean v0, p0, Lcom/osp/app/signin/AccountView;->aj:Z

    if-eqz v0, :cond_22

    invoke-static {p0}, Lcom/osp/app/util/ad;->a(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_22

    .line 2484
    invoke-static {p0}, Lcom/osp/app/util/r;->p(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_36

    .line 2486
    invoke-direct {p0, v10}, Lcom/osp/app/signin/AccountView;->g(I)V

    .line 2493
    :cond_22
    :goto_9
    iget-object v0, p0, Lcom/osp/app/signin/AccountView;->aP:Lcom/osp/app/signin/BottomSoftkeyLinearLayout;

    if-eqz v0, :cond_24

    .line 2495
    invoke-static {}, Lcom/osp/app/signin/SamsungService;->a()Z

    move-result v0

    if-eqz v0, :cond_38

    .line 2497
    iget-object v0, p0, Lcom/osp/app/signin/AccountView;->aP:Lcom/osp/app/signin/BottomSoftkeyLinearLayout;

    const-string v1, "skip_button"

    invoke-virtual {v0, v1}, Lcom/osp/app/signin/BottomSoftkeyLinearLayout;->a(Ljava/lang/String;)V

    .line 2498
    iget-object v0, p0, Lcom/osp/app/signin/AccountView;->aP:Lcom/osp/app/signin/BottomSoftkeyLinearLayout;

    const v1, 0x7f090058

    invoke-virtual {p0, v1}, Lcom/osp/app/signin/AccountView;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/osp/app/signin/BottomSoftkeyLinearLayout;->f(Ljava/lang/String;)V

    .line 2499
    iget-object v0, p0, Lcom/osp/app/signin/AccountView;->aP:Lcom/osp/app/signin/BottomSoftkeyLinearLayout;

    const v1, 0x7f090058

    invoke-virtual {p0, v1}, Lcom/osp/app/signin/AccountView;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/osp/app/signin/BottomSoftkeyLinearLayout;->e(Ljava/lang/String;)V

    .line 2508
    invoke-static {}, Lcom/osp/app/util/r;->f()Z

    move-result v0

    if-eqz v0, :cond_37

    invoke-static {}, Lcom/osp/app/util/r;->h()Z

    move-result v0

    if-eqz v0, :cond_37

    .line 2510
    iget-object v0, p0, Lcom/osp/app/signin/AccountView;->aP:Lcom/osp/app/signin/BottomSoftkeyLinearLayout;

    const-string v1, "one_button"

    invoke-virtual {v0, v1}, Lcom/osp/app/signin/BottomSoftkeyLinearLayout;->a(Ljava/lang/String;)V

    .line 2511
    iget-object v0, p0, Lcom/osp/app/signin/AccountView;->aP:Lcom/osp/app/signin/BottomSoftkeyLinearLayout;

    invoke-static {p0}, Lcom/osp/app/util/an;->a(Lcom/osp/app/util/BaseActivity;)Lcom/osp/app/util/an;

    const v1, 0x7f02008f

    invoke-static {v1}, Lcom/osp/app/util/an;->a(I)I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/osp/app/signin/BottomSoftkeyLinearLayout;->a(I)V

    .line 2512
    iget-object v0, p0, Lcom/osp/app/signin/AccountView;->aP:Lcom/osp/app/signin/BottomSoftkeyLinearLayout;

    const v1, 0x7f0900bf

    invoke-virtual {p0, v1}, Lcom/osp/app/signin/AccountView;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/osp/app/signin/BottomSoftkeyLinearLayout;->f(Ljava/lang/String;)V

    .line 2513
    iget-object v0, p0, Lcom/osp/app/signin/AccountView;->aP:Lcom/osp/app/signin/BottomSoftkeyLinearLayout;

    const v1, 0x7f0900bf

    invoke-virtual {p0, v1}, Lcom/osp/app/signin/AccountView;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/osp/app/signin/BottomSoftkeyLinearLayout;->e(Ljava/lang/String;)V

    .line 2520
    :cond_23
    :goto_a
    iget-object v0, p0, Lcom/osp/app/signin/AccountView;->aP:Lcom/osp/app/signin/BottomSoftkeyLinearLayout;

    iget-object v1, p0, Lcom/osp/app/signin/AccountView;->a:Landroid/view/View$OnClickListener;

    invoke-virtual {v0, v1}, Lcom/osp/app/signin/BottomSoftkeyLinearLayout;->b(Landroid/view/View$OnClickListener;)V

    .line 2545
    :cond_24
    :goto_b
    const v0, 0x7f0c0062

    invoke-virtual {p0, v0}, Lcom/osp/app/signin/AccountView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/View;->requestFocus()Z

    .line 2577
    invoke-static {p0}, Lcom/osp/app/util/ad;->a(Landroid/content/Context;)Z

    move-result v0

    if-ne v0, v10, :cond_25

    invoke-static {}, Lcom/osp/app/signin/SamsungService;->a()Z

    move-result v0

    if-nez v0, :cond_25

    iget-boolean v0, p0, Lcom/osp/app/signin/AccountView;->aq:Z

    if-nez v0, :cond_25

    .line 2582
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v0, "Self Update check"

    invoke-static {v0}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;)V

    .line 2584
    invoke-static {}, Lcom/msc/sa/selfupdate/c;->b()Lcom/msc/sa/selfupdate/c;

    move-result-object v0

    invoke-virtual {p0}, Lcom/osp/app/signin/AccountView;->getApplication()Landroid/app/Application;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v10, v7, v2}, Lcom/msc/sa/selfupdate/c;->a(Landroid/app/Application;ZZLjava/lang/String;)V

    .line 2586
    :cond_25
    :goto_c
    return-void

    .line 2290
    :cond_26
    if-eqz p2, :cond_0

    iget-object v0, p0, Lcom/osp/app/signin/AccountView;->aR:Landroid/animation/AnimatorSet;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/osp/app/signin/AccountView;->aR:Landroid/animation/AnimatorSet;

    invoke-virtual {v0}, Landroid/animation/AnimatorSet;->isRunning()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2292
    iput-boolean v10, p0, Lcom/osp/app/signin/AccountView;->aQ:Z

    goto :goto_c

    .line 2304
    :cond_27
    iput v7, v0, Landroid/widget/LinearLayout$LayoutParams;->bottomMargin:I

    goto/16 :goto_0

    :cond_28
    const v0, 0x7f0c0163

    invoke-virtual {p0, v0}, Lcom/osp/app/signin/AccountView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    if-eqz v0, :cond_1

    invoke-virtual {v0, v11}, Landroid/view/View;->setVisibility(I)V

    goto/16 :goto_1

    :cond_29
    invoke-static {}, Lcom/osp/app/util/r;->p()Z

    move-result v0

    if-eqz v0, :cond_2b

    iget-object v0, p0, Lcom/osp/app/signin/AccountView;->aL:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-lez v0, :cond_2b

    invoke-static {}, Lcom/osp/device/b;->b()Lcom/osp/device/b;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/osp/device/b;->i(Landroid/content/Context;)Z

    move-result v0

    if-nez v0, :cond_2a

    invoke-virtual {p0}, Lcom/osp/app/signin/AccountView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v0

    iget v0, v0, Landroid/content/res/Configuration;->orientation:I

    if-eq v0, v9, :cond_2a

    const v0, 0x7f0c003f

    invoke-virtual {p0, v0}, Lcom/osp/app/signin/AccountView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/View;->getPaddingTop()I

    move-result v2

    invoke-virtual {v0}, Landroid/view/View;->getPaddingBottom()I

    move-result v3

    invoke-virtual {p0}, Lcom/osp/app/signin/AccountView;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const v5, 0x7f08002d

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v4

    float-to-int v4, v4

    invoke-virtual {p0}, Lcom/osp/app/signin/AccountView;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    const v6, 0x7f08002c

    invoke-virtual {v5, v6}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v5

    float-to-int v5, v5

    invoke-static {v0, v5, v2, v4, v3}, Lcom/msc/sa/c/d;->a(Landroid/view/View;IIII)V

    :cond_2a
    invoke-direct {p0}, Lcom/osp/app/signin/AccountView;->F()V

    goto/16 :goto_2

    :cond_2b
    const v0, 0x7f0c005e

    invoke-virtual {p0, v0}, Lcom/osp/app/signin/AccountView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    invoke-static {}, Lcom/osp/device/b;->b()Lcom/osp/device/b;

    move-result-object v2

    invoke-virtual {v2, p0}, Lcom/osp/device/b;->i(Landroid/content/Context;)Z

    move-result v2

    if-nez v2, :cond_2c

    if-eqz v0, :cond_2c

    invoke-virtual {p0}, Lcom/osp/app/signin/AccountView;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v2

    iget v2, v2, Landroid/content/res/Configuration;->orientation:I

    if-ne v2, v9, :cond_2c

    invoke-virtual {v0, v9}, Landroid/widget/TextView;->setMaxLines(I)V

    :cond_2c
    const v0, 0x7f0c005f

    invoke-virtual {p0, v0}, Lcom/osp/app/signin/AccountView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    if-eqz v0, :cond_2d

    invoke-virtual {v0, v11}, Landroid/view/View;->setVisibility(I)V

    :cond_2d
    invoke-direct {p0}, Lcom/osp/app/signin/AccountView;->H()V

    goto/16 :goto_2

    :cond_2e
    invoke-static {p0}, Lcom/osp/app/util/an;->a(Lcom/osp/app/util/BaseActivity;)Lcom/osp/app/util/an;

    const v3, 0x7f02005a

    invoke-static {v3}, Lcom/osp/app/util/an;->a(I)I

    move-result v3

    invoke-virtual {v0, v3}, Landroid/widget/Button;->setBackgroundResource(I)V

    invoke-static {p0}, Lcom/osp/app/util/an;->a(Lcom/osp/app/util/BaseActivity;)Lcom/osp/app/util/an;

    const v3, 0x7f070018

    invoke-static {v3}, Lcom/osp/app/util/an;->a(I)I

    move-result v3

    invoke-virtual {v1, v3}, Landroid/content/res/Resources;->getColor(I)I

    move-result v3

    invoke-virtual {v0, v3}, Landroid/widget/Button;->setTextColor(I)V

    invoke-static {p0}, Lcom/osp/app/util/r;->j(Landroid/content/Context;)Z

    move-result v3

    if-nez v3, :cond_2f

    invoke-static {}, Lcom/osp/app/util/r;->m()Z

    move-result v3

    if-eqz v3, :cond_15

    :cond_2f
    invoke-static {p0}, Lcom/osp/app/util/an;->a(Lcom/osp/app/util/BaseActivity;)Lcom/osp/app/util/an;

    const v3, 0x7f02005c

    invoke-static {v3}, Lcom/osp/app/util/an;->a(I)I

    move-result v3

    invoke-virtual {v1, v3}, Landroid/content/res/Resources;->getColorStateList(I)Landroid/content/res/ColorStateList;

    move-result-object v3

    invoke-virtual {v0, v3}, Landroid/widget/Button;->setTextColor(Landroid/content/res/ColorStateList;)V

    goto/16 :goto_3

    :cond_30
    invoke-static {p0}, Lcom/osp/app/util/an;->a(Lcom/osp/app/util/BaseActivity;)Lcom/osp/app/util/an;

    const v3, 0x7f02005c

    invoke-static {v3}, Lcom/osp/app/util/an;->a(I)I

    move-result v3

    invoke-virtual {v1, v3}, Landroid/content/res/Resources;->getColorStateList(I)Landroid/content/res/ColorStateList;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setTextColor(Landroid/content/res/ColorStateList;)V

    goto/16 :goto_4

    :cond_31
    invoke-virtual {v0, v11}, Landroid/widget/Button;->setVisibility(I)V

    const v0, 0x7f0c0064

    invoke-virtual {p0, v0}, Lcom/osp/app/signin/AccountView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {p0}, Lcom/osp/app/signin/AccountView;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v1

    iget v1, v1, Landroid/content/res/Configuration;->orientation:I

    if-eq v1, v9, :cond_18

    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    goto/16 :goto_5

    .line 2349
    :cond_32
    invoke-static {}, Lcom/osp/app/signin/SamsungService;->a()Z

    move-result v2

    if-nez v2, :cond_1e

    invoke-static {p0}, Lcom/osp/app/util/r;->k(Landroid/content/Context;)Z

    move-result v2

    if-eqz v2, :cond_1e

    .line 2354
    invoke-static {v0, v7, v1, v7, v7}, Lcom/msc/sa/c/d;->a(Landroid/view/View;IIII)V

    goto/16 :goto_6

    .line 2400
    :cond_33
    invoke-static {}, Lcom/osp/app/util/r;->p()Z

    move-result v0

    if-eqz v0, :cond_34

    iget-object v0, p0, Lcom/osp/app/signin/AccountView;->aL:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-lez v0, :cond_34

    .line 2402
    invoke-direct {p0}, Lcom/osp/app/signin/AccountView;->F()V

    goto/16 :goto_8

    .line 2405
    :cond_34
    invoke-static {}, Lcom/osp/app/util/r;->p()Z

    move-result v0

    if-eqz v0, :cond_35

    invoke-static {}, Lcom/osp/device/b;->b()Lcom/osp/device/b;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/osp/device/b;->i(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_35

    .line 2407
    const v0, 0x7f0c0043

    invoke-virtual {p0, v0}, Lcom/osp/app/signin/AccountView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ScrollView;

    .line 2408
    if-eqz v0, :cond_35

    .line 2410
    invoke-virtual {v0}, Landroid/widget/ScrollView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v1

    check-cast v1, Landroid/widget/LinearLayout$LayoutParams;

    .line 2411
    const/4 v2, -0x2

    iput v2, v1, Landroid/widget/LinearLayout$LayoutParams;->height:I

    .line 2412
    const/4 v2, 0x0

    iput v2, v1, Landroid/widget/LinearLayout$LayoutParams;->weight:F

    .line 2413
    invoke-virtual {v0, v1}, Landroid/widget/ScrollView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 2416
    :cond_35
    invoke-direct {p0}, Lcom/osp/app/signin/AccountView;->H()V

    .line 2417
    const v0, 0x7f0c003f

    invoke-virtual {p0, v0}, Lcom/osp/app/signin/AccountView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    .line 2418
    invoke-virtual {v0, v10}, Landroid/view/View;->setClickable(Z)V

    .line 2419
    iget-object v1, p0, Lcom/osp/app/signin/AccountView;->W:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/view/View;->setContentDescription(Ljava/lang/CharSequence;)V

    goto/16 :goto_8

    .line 2489
    :cond_36
    invoke-direct {p0}, Lcom/osp/app/signin/AccountView;->z()V

    goto/16 :goto_9

    .line 2514
    :cond_37
    invoke-static {}, Lcom/osp/app/util/r;->g()Z

    move-result v0

    if-eqz v0, :cond_23

    .line 2516
    iget-object v0, p0, Lcom/osp/app/signin/AccountView;->aP:Lcom/osp/app/signin/BottomSoftkeyLinearLayout;

    const v1, 0x7f0900c6

    invoke-virtual {p0, v1}, Lcom/osp/app/signin/AccountView;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/osp/app/signin/BottomSoftkeyLinearLayout;->f(Ljava/lang/String;)V

    .line 2517
    iget-object v0, p0, Lcom/osp/app/signin/AccountView;->aP:Lcom/osp/app/signin/BottomSoftkeyLinearLayout;

    const v1, 0x7f0900c6

    invoke-virtual {p0, v1}, Lcom/osp/app/signin/AccountView;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/osp/app/signin/BottomSoftkeyLinearLayout;->e(Ljava/lang/String;)V

    goto/16 :goto_a

    .line 2523
    :cond_38
    iget-object v0, p0, Lcom/osp/app/signin/AccountView;->aP:Lcom/osp/app/signin/BottomSoftkeyLinearLayout;

    const-string v1, "help_button"

    invoke-virtual {v0, v1}, Lcom/osp/app/signin/BottomSoftkeyLinearLayout;->a(Ljava/lang/String;)V

    .line 2524
    invoke-static {p0}, Lcom/osp/app/util/r;->m(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_39

    .line 2526
    iget-object v0, p0, Lcom/osp/app/signin/AccountView;->aP:Lcom/osp/app/signin/BottomSoftkeyLinearLayout;

    invoke-static {p0}, Lcom/osp/app/util/an;->a(Lcom/osp/app/util/BaseActivity;)Lcom/osp/app/util/an;

    const v1, 0x7f02011d

    invoke-static {v1}, Lcom/osp/app/util/an;->a(I)I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/osp/app/signin/BottomSoftkeyLinearLayout;->a(I)V

    .line 2528
    :cond_39
    iget-object v0, p0, Lcom/osp/app/signin/AccountView;->aP:Lcom/osp/app/signin/BottomSoftkeyLinearLayout;

    const v1, 0x7f090096

    invoke-virtual {p0, v1}, Lcom/osp/app/signin/AccountView;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/osp/app/signin/BottomSoftkeyLinearLayout;->f(Ljava/lang/String;)V

    .line 2529
    iget-object v0, p0, Lcom/osp/app/signin/AccountView;->aP:Lcom/osp/app/signin/BottomSoftkeyLinearLayout;

    const v1, 0x7f090096

    invoke-virtual {p0, v1}, Lcom/osp/app/signin/AccountView;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/osp/app/signin/BottomSoftkeyLinearLayout;->e(Ljava/lang/String;)V

    .line 2530
    iget-object v0, p0, Lcom/osp/app/signin/AccountView;->aP:Lcom/osp/app/signin/BottomSoftkeyLinearLayout;

    iget-object v1, p0, Lcom/osp/app/signin/AccountView;->b:Landroid/view/View$OnClickListener;

    invoke-virtual {v0, v1}, Lcom/osp/app/signin/BottomSoftkeyLinearLayout;->b(Landroid/view/View$OnClickListener;)V

    goto/16 :goto_b

    :cond_3a
    move-object v1, v0

    goto/16 :goto_7
.end method

.method static synthetic a(Lcom/osp/app/signin/AccountView;Z)Z
    .locals 0

    .prologue
    .line 142
    iput-boolean p1, p0, Lcom/osp/app/signin/AccountView;->aB:Z

    return p1
.end method

.method static synthetic aa(Lcom/osp/app/signin/AccountView;)Lcom/osp/app/signin/fv;
    .locals 1

    .prologue
    .line 142
    iget-object v0, p0, Lcom/osp/app/signin/AccountView;->ak:Lcom/osp/app/signin/fv;

    return-object v0
.end method

.method static synthetic ab(Lcom/osp/app/signin/AccountView;)V
    .locals 2

    .prologue
    .line 142
    const/4 v0, 0x0

    const/4 v1, 0x1

    invoke-direct {p0, v0, v1}, Lcom/osp/app/signin/AccountView;->a(ZZ)V

    return-void
.end method

.method static synthetic ac(Lcom/osp/app/signin/AccountView;)Lcom/msc/sa/c/f;
    .locals 1

    .prologue
    .line 142
    iget-object v0, p0, Lcom/osp/app/signin/AccountView;->r:Lcom/msc/sa/c/f;

    return-object v0
.end method

.method static synthetic ad(Lcom/osp/app/signin/AccountView;)Lcom/msc/sa/c/f;
    .locals 1

    .prologue
    .line 142
    iget-object v0, p0, Lcom/osp/app/signin/AccountView;->r:Lcom/msc/sa/c/f;

    return-object v0
.end method

.method static synthetic ae(Lcom/osp/app/signin/AccountView;)Lcom/msc/sa/c/f;
    .locals 1

    .prologue
    .line 142
    iget-object v0, p0, Lcom/osp/app/signin/AccountView;->r:Lcom/msc/sa/c/f;

    return-object v0
.end method

.method static synthetic af(Lcom/osp/app/signin/AccountView;)Lcom/msc/sa/c/f;
    .locals 1

    .prologue
    .line 142
    iget-object v0, p0, Lcom/osp/app/signin/AccountView;->r:Lcom/msc/sa/c/f;

    return-object v0
.end method

.method static synthetic ag(Lcom/osp/app/signin/AccountView;)V
    .locals 0

    .prologue
    .line 142
    invoke-direct {p0}, Lcom/osp/app/signin/AccountView;->z()V

    return-void
.end method

.method static synthetic ah(Lcom/osp/app/signin/AccountView;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 142
    iget-object v0, p0, Lcom/osp/app/signin/AccountView;->aG:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic ai(Lcom/osp/app/signin/AccountView;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 142
    iget-object v0, p0, Lcom/osp/app/signin/AccountView;->at:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic aj(Lcom/osp/app/signin/AccountView;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 142
    iget-object v0, p0, Lcom/osp/app/signin/AccountView;->au:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic ak(Lcom/osp/app/signin/AccountView;)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 142
    invoke-virtual {p0}, Lcom/osp/app/signin/AccountView;->isFinishing()Z

    move-result v0

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/osp/app/signin/AccountView;->ab:Landroid/app/ProgressDialog;

    if-eqz v0, :cond_1

    :try_start_0
    iget-object v0, p0, Lcom/osp/app/signin/AccountView;->ab:Landroid/app/ProgressDialog;

    invoke-virtual {v0}, Landroid/app/ProgressDialog;->isShowing()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/osp/app/signin/AccountView;->ab:Landroid/app/ProgressDialog;

    invoke-virtual {v0}, Landroid/app/ProgressDialog;->dismiss()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :cond_0
    :goto_0
    iput-object v1, p0, Lcom/osp/app/signin/AccountView;->ab:Landroid/app/ProgressDialog;

    :cond_1
    return-void

    :catch_0
    move-exception v0

    :try_start_1
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v0

    iput-object v1, p0, Lcom/osp/app/signin/AccountView;->ab:Landroid/app/ProgressDialog;

    throw v0
.end method

.method static synthetic al(Lcom/osp/app/signin/AccountView;)V
    .locals 0

    .prologue
    .line 142
    invoke-direct {p0}, Lcom/osp/app/signin/AccountView;->N()V

    return-void
.end method

.method static synthetic am(Lcom/osp/app/signin/AccountView;)V
    .locals 2

    .prologue
    .line 142
    iget-object v0, p0, Lcom/osp/app/signin/AccountView;->av:Lcom/osp/app/signin/az;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/osp/app/signin/AccountView;->av:Lcom/osp/app/signin/az;

    invoke-virtual {v0}, Lcom/osp/app/signin/az;->getStatus()Landroid/os/AsyncTask$Status;

    move-result-object v0

    sget-object v1, Landroid/os/AsyncTask$Status;->RUNNING:Landroid/os/AsyncTask$Status;

    if-ne v0, v1, :cond_0

    iget-object v0, p0, Lcom/osp/app/signin/AccountView;->av:Lcom/osp/app/signin/az;

    invoke-virtual {v0}, Lcom/osp/app/signin/az;->d()V

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/osp/app/signin/AccountView;->av:Lcom/osp/app/signin/az;

    :cond_0
    new-instance v0, Lcom/osp/app/signin/az;

    invoke-direct {v0, p0, p0}, Lcom/osp/app/signin/az;-><init>(Lcom/osp/app/signin/AccountView;Landroid/content/Context;)V

    iput-object v0, p0, Lcom/osp/app/signin/AccountView;->av:Lcom/osp/app/signin/az;

    iget-object v0, p0, Lcom/osp/app/signin/AccountView;->av:Lcom/osp/app/signin/az;

    invoke-virtual {v0}, Lcom/osp/app/signin/az;->b()V

    return-void
.end method

.method static synthetic an(Lcom/osp/app/signin/AccountView;)Z
    .locals 1

    .prologue
    .line 142
    iget-boolean v0, p0, Lcom/osp/app/signin/AccountView;->aF:Z

    return v0
.end method

.method static synthetic ao(Lcom/osp/app/signin/AccountView;)Lcom/osp/app/signin/bb;
    .locals 1

    .prologue
    .line 142
    iget-object v0, p0, Lcom/osp/app/signin/AccountView;->aE:Lcom/osp/app/signin/bb;

    return-object v0
.end method

.method static synthetic b(Lcom/osp/app/signin/AccountView;I)Landroid/view/View;
    .locals 16

    .prologue
    .line 142
    invoke-static {}, Lcom/osp/device/b;->b()Lcom/osp/device/b;

    move-result-object v1

    move-object/from16 v0, p0

    invoke-virtual {v1, v0}, Lcom/osp/device/b;->i(Landroid/content/Context;)Z

    move-result v1

    if-nez v1, :cond_8

    invoke-virtual/range {p0 .. p0}, Lcom/osp/app/signin/AccountView;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v1

    iget v1, v1, Landroid/content/res/Configuration;->orientation:I

    const/4 v2, 0x2

    if-ne v1, v2, :cond_8

    const/4 v1, 0x1

    move v4, v1

    :goto_0
    const/4 v1, 0x0

    const/4 v2, 0x0

    const/4 v3, 0x0

    const/4 v8, 0x0

    new-instance v5, Landroid/view/View;

    move-object/from16 v0, p0

    invoke-direct {v5, v0}, Landroid/view/View;-><init>(Landroid/content/Context;)V

    new-instance v5, Landroid/widget/TextView;

    move-object/from16 v0, p0

    invoke-direct {v5, v0}, Landroid/widget/TextView;-><init>(Landroid/content/Context;)V

    new-instance v5, Landroid/widget/TextView;

    move-object/from16 v0, p0

    invoke-direct {v5, v0}, Landroid/widget/TextView;-><init>(Landroid/content/Context;)V

    invoke-static/range {p0 .. p0}, Lcom/osp/app/util/x;->j(Landroid/content/Context;)Z

    move-result v5

    if-eqz v5, :cond_1b

    const/4 v1, 0x1

    move v5, v1

    :goto_1
    invoke-static/range {p0 .. p0}, Lcom/osp/app/util/x;->k(Landroid/content/Context;)Z

    move-result v1

    if-eqz v1, :cond_1a

    const/4 v1, 0x1

    move v6, v1

    :goto_2
    invoke-static/range {p0 .. p0}, Lcom/osp/app/util/x;->a(Landroid/app/Activity;)Z

    move-result v1

    if-eqz v1, :cond_19

    const/4 v1, 0x1

    move v7, v1

    :goto_3
    invoke-static/range {p0 .. p0}, Lcom/osp/app/util/x;->i(Landroid/content/Context;)Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v1, 0x1

    move v8, v1

    :cond_0
    new-instance v13, Landroid/widget/ImageView;

    move-object/from16 v0, p0

    invoke-direct {v13, v0}, Landroid/widget/ImageView;-><init>(Landroid/content/Context;)V

    sget-object v1, Landroid/widget/ImageView$ScaleType;->CENTER:Landroid/widget/ImageView$ScaleType;

    invoke-virtual {v13, v1}, Landroid/widget/ImageView;->setScaleType(Landroid/widget/ImageView$ScaleType;)V

    new-instance v1, Landroid/view/ViewGroup$LayoutParams;

    const/4 v2, -0x2

    const/4 v3, -0x2

    invoke-direct {v1, v2, v3}, Landroid/view/ViewGroup$LayoutParams;-><init>(II)V

    invoke-virtual {v13, v1}, Landroid/widget/ImageView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    invoke-static/range {p0 .. p0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v1

    const v2, 0x7f03000c

    const/4 v3, 0x0

    invoke-virtual {v1, v2, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v14

    const v1, 0x7f0c007e

    invoke-virtual {v14, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    const v2, 0x7f0c007f

    invoke-virtual {v14, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    sget v3, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v9, 0x15

    if-lt v3, v9, :cond_1

    const-string v3, "sec-roboto-light"

    const/4 v9, 0x0

    invoke-static {v3, v9}, Landroid/graphics/Typeface;->create(Ljava/lang/String;I)Landroid/graphics/Typeface;

    move-result-object v3

    invoke-virtual {v1, v3}, Landroid/widget/TextView;->setTypeface(Landroid/graphics/Typeface;)V

    const-string v3, "sec-roboto-light"

    const/4 v9, 0x0

    invoke-static {v3, v9}, Landroid/graphics/Typeface;->create(Ljava/lang/String;I)Landroid/graphics/Typeface;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setTypeface(Landroid/graphics/Typeface;)V

    :cond_1
    invoke-virtual/range {p0 .. p0}, Lcom/osp/app/signin/AccountView;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    invoke-virtual {v3}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v3

    iget v3, v3, Landroid/content/res/Configuration;->orientation:I

    const/4 v9, 0x2

    if-ne v3, v9, :cond_9

    invoke-static/range {p0 .. p0}, Lcom/osp/app/util/x;->m(Landroid/content/Context;)Z

    move-result v3

    if-eqz v3, :cond_9

    invoke-static/range {p0 .. p0}, Lcom/osp/app/util/an;->a(Lcom/osp/app/util/BaseActivity;)Lcom/osp/app/util/an;

    const v3, 0x7f02006b

    invoke-static {v3}, Lcom/osp/app/util/an;->a(I)I

    move-result v11

    invoke-static/range {p0 .. p0}, Lcom/osp/app/util/an;->a(Lcom/osp/app/util/BaseActivity;)Lcom/osp/app/util/an;

    const v3, 0x7f02006f

    invoke-static {v3}, Lcom/osp/app/util/an;->a(I)I

    move-result v10

    invoke-static/range {p0 .. p0}, Lcom/osp/app/util/an;->a(Lcom/osp/app/util/BaseActivity;)Lcom/osp/app/util/an;

    const v3, 0x7f020073

    invoke-static {v3}, Lcom/osp/app/util/an;->a(I)I

    move-result v9

    invoke-static/range {p0 .. p0}, Lcom/osp/app/util/an;->a(Lcom/osp/app/util/BaseActivity;)Lcom/osp/app/util/an;

    const v3, 0x7f020077

    invoke-static {v3}, Lcom/osp/app/util/an;->a(I)I

    move-result v3

    move v12, v11

    move v11, v10

    move v10, v9

    move v9, v3

    :goto_4
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/osp/app/signin/AccountView;->aL:Ljava/util/ArrayList;

    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    move-result v3

    move/from16 v0, p1

    if-ge v0, v3, :cond_2

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/osp/app/signin/AccountView;->aL:Ljava/util/ArrayList;

    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    move-result v3

    const/4 v15, 0x1

    if-le v3, v15, :cond_a

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/osp/app/signin/AccountView;->aL:Ljava/util/ArrayList;

    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    move-result v3

    add-int/lit8 v3, v3, -0x1

    move/from16 v0, p1

    if-eq v0, v3, :cond_a

    const v3, 0x7f090185

    move-object/from16 v0, p0

    invoke-virtual {v0, v3}, Lcom/osp/app/signin/AccountView;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v13, v3}, Landroid/widget/ImageView;->setContentDescription(Ljava/lang/CharSequence;)V

    :goto_5
    sget-object v15, Lcom/osp/app/signin/ao;->a:[I

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/osp/app/signin/AccountView;->aL:Ljava/util/ArrayList;

    move/from16 v0, p1

    invoke-virtual {v3, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/osp/app/signin/bg;

    invoke-virtual {v3}, Lcom/osp/app/signin/bg;->ordinal()I

    move-result v3

    aget v3, v15, v3

    packed-switch v3, :pswitch_data_0

    :cond_2
    :goto_6
    if-nez v6, :cond_3

    if-nez v7, :cond_3

    if-nez v5, :cond_3

    if-eqz v8, :cond_4

    :cond_3
    invoke-static/range {p0 .. p0}, Lcom/osp/app/util/an;->a(Lcom/osp/app/util/BaseActivity;)Lcom/osp/app/util/an;

    const v3, 0x7f080032

    invoke-static {v3}, Lcom/osp/app/util/an;->b(I)I

    move-result v3

    const/4 v5, 0x0

    invoke-virtual/range {p0 .. p0}, Lcom/osp/app/signin/AccountView;->getResources()Landroid/content/res/Resources;

    move-result-object v9

    invoke-virtual {v9, v3}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v3

    invoke-virtual {v2, v5, v3}, Landroid/widget/TextView;->setTextSize(IF)V

    :cond_4
    if-eqz v1, :cond_5

    if-eqz v2, :cond_5

    invoke-virtual/range {p0 .. p0}, Lcom/osp/app/signin/AccountView;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    invoke-static/range {p0 .. p0}, Lcom/osp/app/util/an;->a(Lcom/osp/app/util/BaseActivity;)Lcom/osp/app/util/an;

    const v5, 0x7f07000a

    invoke-static {v5}, Lcom/osp/app/util/an;->a(I)I

    move-result v5

    invoke-virtual {v3, v5}, Landroid/content/res/Resources;->getColor(I)I

    move-result v3

    invoke-virtual {v1, v3}, Landroid/widget/TextView;->setTextColor(I)V

    invoke-virtual/range {p0 .. p0}, Lcom/osp/app/signin/AccountView;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    invoke-static/range {p0 .. p0}, Lcom/osp/app/util/an;->a(Lcom/osp/app/util/BaseActivity;)Lcom/osp/app/util/an;

    const v5, 0x7f07000c

    invoke-static {v5}, Lcom/osp/app/util/an;->a(I)I

    move-result v5

    invoke-virtual {v3, v5}, Landroid/content/res/Resources;->getColor(I)I

    move-result v3

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setTextColor(I)V

    invoke-static/range {p0 .. p0}, Lcom/osp/app/util/an;->a(Lcom/osp/app/util/BaseActivity;)Lcom/osp/app/util/an;

    const v3, 0x7f080031

    invoke-static {v3}, Lcom/osp/app/util/an;->b(I)I

    move-result v3

    const/4 v5, 0x0

    invoke-virtual/range {p0 .. p0}, Lcom/osp/app/signin/AccountView;->getResources()Landroid/content/res/Resources;

    move-result-object v9

    invoke-virtual {v9, v3}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v3

    invoke-virtual {v1, v5, v3}, Landroid/widget/TextView;->setTextSize(IF)V

    invoke-static/range {p0 .. p0}, Lcom/osp/app/util/an;->a(Lcom/osp/app/util/BaseActivity;)Lcom/osp/app/util/an;

    const v3, 0x7f080001

    invoke-static {v3}, Lcom/osp/app/util/an;->b(I)I

    move-result v3

    const/4 v5, 0x0

    invoke-virtual/range {p0 .. p0}, Lcom/osp/app/signin/AccountView;->getResources()Landroid/content/res/Resources;

    move-result-object v9

    invoke-virtual {v9, v3}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v3

    invoke-virtual {v2, v5, v3}, Landroid/widget/TextView;->setTextSize(IF)V

    :cond_5
    if-nez v4, :cond_13

    invoke-virtual/range {p0 .. p0}, Lcom/osp/app/signin/AccountView;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    invoke-virtual {v3}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v3

    iget v3, v3, Landroid/util/DisplayMetrics;->widthPixels:I

    invoke-virtual/range {p0 .. p0}, Lcom/osp/app/signin/AccountView;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    const v9, 0x7f08002c

    invoke-virtual {v5, v9}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v5

    sub-int/2addr v3, v5

    invoke-virtual/range {p0 .. p0}, Lcom/osp/app/signin/AccountView;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    const v9, 0x7f08002d

    invoke-virtual {v5, v9}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v5

    sub-int/2addr v3, v5

    invoke-static {}, Lcom/osp/device/b;->b()Lcom/osp/device/b;

    move-result-object v5

    move-object/from16 v0, p0

    invoke-virtual {v5, v0}, Lcom/osp/device/b;->i(Landroid/content/Context;)Z

    move-result v5

    if-nez v5, :cond_6

    const v5, 0x7f0c003f

    move-object/from16 v0, p0

    invoke-virtual {v0, v5}, Lcom/osp/app/signin/AccountView;->findViewById(I)Landroid/view/View;

    move-result-object v5

    invoke-virtual {v5}, Landroid/view/View;->getPaddingLeft()I

    move-result v9

    invoke-virtual {v5}, Landroid/view/View;->getPaddingRight()I

    move-result v5

    add-int/2addr v5, v9

    sub-int/2addr v3, v5

    :cond_6
    const v5, 0x7f0c007c

    invoke-virtual {v14, v5}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v5

    invoke-virtual {v5}, Landroid/view/View;->getPaddingLeft()I

    move-result v9

    invoke-virtual {v5}, Landroid/view/View;->getPaddingRight()I

    move-result v5

    add-int/2addr v5, v9

    sub-int/2addr v3, v5

    invoke-virtual/range {p0 .. p0}, Lcom/osp/app/signin/AccountView;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    const v9, 0x7f080033

    invoke-virtual {v5, v9}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v5

    move-object/from16 v0, p0

    invoke-direct {v0, v1, v2, v3, v5}, Lcom/osp/app/signin/AccountView;->a(Landroid/widget/TextView;Landroid/widget/TextView;II)I

    move-result v1

    new-instance v2, Landroid/view/ViewGroup$LayoutParams;

    const/4 v3, -0x1

    invoke-direct {v2, v3, v1}, Landroid/view/ViewGroup$LayoutParams;-><init>(II)V

    invoke-virtual {v14, v2}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    :goto_7
    new-instance v2, Landroid/widget/LinearLayout;

    move-object/from16 v0, p0

    invoke-direct {v2, v0}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;)V

    new-instance v1, Landroid/view/ViewGroup$LayoutParams;

    const/4 v3, -0x1

    const/4 v5, -0x2

    invoke-direct {v1, v3, v5}, Landroid/view/ViewGroup$LayoutParams;-><init>(II)V

    invoke-virtual {v2, v1}, Landroid/widget/LinearLayout;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    if-eqz v7, :cond_14

    const/4 v1, 0x1

    invoke-virtual {v2, v1}, Landroid/widget/LinearLayout;->setOrientation(I)V

    const/4 v1, 0x1

    invoke-virtual {v2, v1}, Landroid/widget/LinearLayout;->setGravity(I)V

    const/4 v1, 0x0

    invoke-virtual {v2, v14, v1}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;I)V

    :cond_7
    :goto_8
    return-object v2

    :cond_8
    const/4 v1, 0x0

    move v4, v1

    goto/16 :goto_0

    :cond_9
    invoke-static/range {p0 .. p0}, Lcom/osp/app/util/an;->a(Lcom/osp/app/util/BaseActivity;)Lcom/osp/app/util/an;

    const v3, 0x7f02006a

    invoke-static {v3}, Lcom/osp/app/util/an;->a(I)I

    move-result v11

    invoke-static/range {p0 .. p0}, Lcom/osp/app/util/an;->a(Lcom/osp/app/util/BaseActivity;)Lcom/osp/app/util/an;

    const v3, 0x7f02006e

    invoke-static {v3}, Lcom/osp/app/util/an;->a(I)I

    move-result v10

    invoke-static/range {p0 .. p0}, Lcom/osp/app/util/an;->a(Lcom/osp/app/util/BaseActivity;)Lcom/osp/app/util/an;

    const v3, 0x7f020072

    invoke-static {v3}, Lcom/osp/app/util/an;->a(I)I

    move-result v9

    invoke-static/range {p0 .. p0}, Lcom/osp/app/util/an;->a(Lcom/osp/app/util/BaseActivity;)Lcom/osp/app/util/an;

    const v3, 0x7f020076

    invoke-static {v3}, Lcom/osp/app/util/an;->a(I)I

    move-result v3

    move v12, v11

    move v11, v10

    move v10, v9

    move v9, v3

    goto/16 :goto_4

    :cond_a
    const/4 v3, 0x0

    invoke-virtual {v13, v3}, Landroid/widget/ImageView;->setContentDescription(Ljava/lang/CharSequence;)V

    goto/16 :goto_5

    :pswitch_0
    if-eqz v1, :cond_b

    if-eqz v2, :cond_b

    invoke-static {}, Lcom/osp/app/util/r;->h()Z

    move-result v3

    if-eqz v3, :cond_c

    const v3, 0x7f090179

    invoke-virtual {v1, v3}, Landroid/widget/TextView;->setText(I)V

    const v3, 0x7f09017a

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setText(I)V

    :cond_b
    :goto_9
    invoke-virtual {v13, v12}, Landroid/widget/ImageView;->setImageResource(I)V

    goto/16 :goto_6

    :cond_c
    const v3, 0x7f09015f

    invoke-virtual {v1, v3}, Landroid/widget/TextView;->setText(I)V

    const v3, 0x7f09015e

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setText(I)V

    goto :goto_9

    :pswitch_1
    if-eqz v1, :cond_f

    if-eqz v2, :cond_f

    const v3, 0x7f09015d

    invoke-virtual {v1, v3}, Landroid/widget/TextView;->setText(I)V

    const v3, 0x7f090168

    move-object/from16 v0, p0

    invoke-virtual {v0, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v9

    invoke-virtual {v9}, Ljava/util/Locale;->getLanguage()Ljava/lang/String;

    move-result-object v9

    const-string v10, "ar"

    invoke-virtual {v10, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-nez v10, :cond_d

    const-string v10, "fa"

    invoke-virtual {v10, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-nez v10, :cond_d

    const-string v10, "ur"

    invoke-virtual {v10, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-nez v10, :cond_d

    const-string v10, "he"

    invoke-virtual {v10, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-nez v10, :cond_d

    const-string v10, "iw"

    invoke-virtual {v10, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_e

    :cond_d
    const/4 v9, 0x0

    invoke-virtual {v3, v9}, Ljava/lang/String;->charAt(I)C

    move-result v9

    invoke-static {v9}, Ljava/lang/Character;->getDirectionality(C)B

    move-result v9

    if-nez v9, :cond_e

    new-instance v3, Ljava/lang/StringBuffer;

    invoke-direct {v3}, Ljava/lang/StringBuffer;-><init>()V

    const/16 v9, 0x200f

    invoke-virtual {v3, v9}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    const v9, 0x7f090168

    move-object/from16 v0, p0

    invoke-virtual {v0, v9}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v3, v9}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    invoke-virtual {v3}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v3

    :cond_e
    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    :cond_f
    invoke-virtual {v13, v11}, Landroid/widget/ImageView;->setImageResource(I)V

    goto/16 :goto_6

    :pswitch_2
    if-eqz v1, :cond_10

    if-eqz v2, :cond_10

    const v3, 0x7f09015b

    invoke-virtual {v1, v3}, Landroid/widget/TextView;->setText(I)V

    const v3, 0x7f09015a

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setText(I)V

    :cond_10
    invoke-virtual {v13, v10}, Landroid/widget/ImageView;->setImageResource(I)V

    goto/16 :goto_6

    :pswitch_3
    if-eqz v1, :cond_11

    if-eqz v2, :cond_11

    const v3, 0x7f090165

    invoke-virtual {v1, v3}, Landroid/widget/TextView;->setText(I)V

    invoke-static/range {p0 .. p0}, Lcom/osp/app/util/r;->s(Landroid/content/Context;)Z

    move-result v3

    if-eqz v3, :cond_12

    const v3, 0x7f090164

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setText(I)V

    :cond_11
    :goto_a
    invoke-virtual {v13, v9}, Landroid/widget/ImageView;->setImageResource(I)V

    goto/16 :goto_6

    :cond_12
    const v3, 0x7f090163

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setText(I)V

    goto :goto_a

    :cond_13
    invoke-virtual/range {p0 .. p0}, Lcom/osp/app/signin/AccountView;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v5, 0x7f080039

    invoke-virtual {v3, v5}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v3

    invoke-virtual/range {p0 .. p0}, Lcom/osp/app/signin/AccountView;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    const v9, 0x7f080029

    invoke-virtual {v5, v9}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v5

    move-object/from16 v0, p0

    invoke-direct {v0, v1, v2, v3, v5}, Lcom/osp/app/signin/AccountView;->a(Landroid/widget/TextView;Landroid/widget/TextView;II)I

    move-result v1

    new-instance v2, Landroid/view/ViewGroup$LayoutParams;

    invoke-direct {v2, v3, v1}, Landroid/view/ViewGroup$LayoutParams;-><init>(II)V

    invoke-virtual {v14, v2}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    goto/16 :goto_7

    :cond_14
    new-instance v3, Landroid/widget/LinearLayout;

    move-object/from16 v0, p0

    invoke-direct {v3, v0}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;)V

    if-eqz v4, :cond_18

    const/4 v1, 0x0

    invoke-virtual {v2, v1}, Landroid/widget/LinearLayout;->setOrientation(I)V

    new-instance v1, Landroid/widget/LinearLayout$LayoutParams;

    const/4 v4, -0x2

    const/4 v5, -0x1

    invoke-direct {v1, v4, v5}, Landroid/widget/LinearLayout$LayoutParams;-><init>(II)V

    invoke-virtual {v3, v1}, Landroid/widget/LinearLayout;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    invoke-virtual {v3}, Landroid/widget/LinearLayout;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v1

    check-cast v1, Landroid/widget/LinearLayout$LayoutParams;

    const/high16 v4, 0x3f800000    # 1.0f

    iput v4, v1, Landroid/widget/LinearLayout$LayoutParams;->weight:F

    invoke-virtual {v3, v1}, Landroid/widget/LinearLayout;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    const/4 v1, 0x0

    invoke-virtual {v2, v13, v1}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;I)V

    const/4 v1, 0x1

    invoke-virtual {v2, v3, v1}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;I)V

    const/4 v1, 0x2

    invoke-virtual {v2, v14, v1}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;I)V

    if-nez v6, :cond_15

    if-eqz v8, :cond_7

    :cond_15
    new-instance v1, Landroid/widget/LinearLayout$LayoutParams;

    const/4 v3, -0x2

    const/4 v4, -0x2

    invoke-direct {v1, v3, v4}, Landroid/widget/LinearLayout$LayoutParams;-><init>(II)V

    if-eqz v6, :cond_16

    const/4 v3, 0x0

    invoke-virtual/range {p0 .. p0}, Lcom/osp/app/signin/AccountView;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const v5, 0x7f080035

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v4

    const/4 v5, 0x0

    const/4 v6, 0x0

    invoke-virtual {v1, v3, v4, v5, v6}, Landroid/widget/LinearLayout$LayoutParams;->setMargins(IIII)V

    :cond_16
    if-eqz v8, :cond_17

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    invoke-virtual/range {p0 .. p0}, Lcom/osp/app/signin/AccountView;->getResources()Landroid/content/res/Resources;

    move-result-object v6

    const v7, 0x7f080035

    invoke-virtual {v6, v7}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v6

    invoke-virtual {v1, v3, v4, v5, v6}, Landroid/widget/LinearLayout$LayoutParams;->setMargins(IIII)V

    :cond_17
    invoke-virtual {v13, v1}, Landroid/widget/ImageView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    goto/16 :goto_8

    :cond_18
    const/4 v1, 0x1

    invoke-virtual {v2, v1}, Landroid/widget/LinearLayout;->setOrientation(I)V

    const/4 v1, 0x1

    invoke-virtual {v2, v1}, Landroid/widget/LinearLayout;->setGravity(I)V

    new-instance v1, Landroid/widget/LinearLayout$LayoutParams;

    const/4 v4, -0x1

    const/4 v5, -0x2

    invoke-direct {v1, v4, v5}, Landroid/widget/LinearLayout$LayoutParams;-><init>(II)V

    invoke-virtual {v3, v1}, Landroid/widget/LinearLayout;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    invoke-virtual {v3}, Landroid/widget/LinearLayout;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v1

    check-cast v1, Landroid/widget/LinearLayout$LayoutParams;

    const/high16 v4, 0x3f800000    # 1.0f

    iput v4, v1, Landroid/widget/LinearLayout$LayoutParams;->weight:F

    invoke-virtual {v3, v1}, Landroid/widget/LinearLayout;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    const/4 v1, 0x0

    invoke-virtual {v2, v14, v1}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;I)V

    const/4 v1, 0x1

    invoke-virtual {v2, v3, v1}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;I)V

    const/4 v1, 0x2

    invoke-virtual {v2, v13, v1}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;I)V

    goto/16 :goto_8

    :cond_19
    move v7, v3

    goto/16 :goto_3

    :cond_1a
    move v6, v2

    goto/16 :goto_2

    :cond_1b
    move v5, v1

    goto/16 :goto_1

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method static synthetic b(Lcom/osp/app/signin/AccountView;Lcom/msc/a/g;)Lcom/msc/a/g;
    .locals 0

    .prologue
    .line 142
    iput-object p1, p0, Lcom/osp/app/signin/AccountView;->M:Lcom/msc/a/g;

    return-object p1
.end method

.method private static b(Landroid/content/Intent;)Ljava/lang/String;
    .locals 5

    .prologue
    const/4 v0, 0x0

    .line 1925
    if-eqz p0, :cond_2

    .line 1927
    const/4 v1, 0x5

    new-array v2, v1, [Ljava/lang/String;

    const-string v1, "client_id"

    aput-object v1, v2, v0

    const/4 v1, 0x1

    const-string v3, "client_secret"

    aput-object v3, v2, v1

    const/4 v1, 0x2

    const-string v3, "OSP_VER"

    aput-object v3, v2, v1

    const/4 v1, 0x3

    const-string v3, "mypackage"

    aput-object v3, v2, v1

    const/4 v1, 0x4

    const-string v3, "MODE"

    aput-object v3, v2, v1

    .line 1929
    array-length v3, v2

    move v1, v0

    :goto_0
    if-ge v1, v3, :cond_1

    aget-object v0, v2, v1

    .line 1931
    invoke-virtual {p0, v0}, Landroid/content/Intent;->hasExtra(Ljava/lang/String;)Z

    move-result v4

    if-nez v4, :cond_0

    .line 1939
    :goto_1
    return-object v0

    .line 1929
    :cond_0
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 1936
    :cond_1
    const/4 v0, 0x0

    goto :goto_1

    .line 1939
    :cond_2
    const-string v0, "client_id"

    goto :goto_1
.end method

.method static synthetic b(Lcom/osp/app/signin/AccountView;Ljava/lang/String;)Ljava/lang/String;
    .locals 0

    .prologue
    .line 142
    iput-object p1, p0, Lcom/osp/app/signin/AccountView;->ag:Ljava/lang/String;

    return-object p1
.end method

.method private b(Ljava/lang/String;)V
    .locals 3

    .prologue
    .line 1745
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v0, "AV"

    const-string v1, "check signature"

    invoke-static {v0, v1}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 1748
    invoke-static {p0}, Lcom/osp/app/signin/SamsungService;->a(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v0

    .line 1750
    if-eqz v0, :cond_1

    .line 1754
    :try_start_0
    invoke-static {}, Lcom/msc/c/h;->a()Lcom/msc/c/h;

    invoke-static {v0}, Lcom/msc/c/h;->A(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v0

    .line 1756
    invoke-static {}, Lcom/msc/c/h;->a()Lcom/msc/c/h;

    iget-object v1, p0, Lcom/osp/app/signin/AccountView;->H:Ljava/lang/String;

    invoke-static {p0, v0, v1, p1}, Lcom/msc/c/h;->a(Landroid/content/Context;Ljava/util/ArrayList;Ljava/lang/String;Ljava/lang/String;)Z

    move-result v0

    .line 1758
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v1, "AV"

    const-string v2, "used cache of signature"

    invoke-static {v1, v2}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 1760
    if-eqz v0, :cond_0

    .line 1764
    invoke-direct {p0}, Lcom/osp/app/signin/AccountView;->x()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 1868
    :goto_0
    return-void

    .line 1771
    :catch_0
    move-exception v0

    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    .line 1775
    :cond_0
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v0, "AV"

    const-string v1, "Not matched cache data of signature"

    invoke-static {v0, v1}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 1779
    :cond_1
    new-instance v0, Lcom/osp/app/signin/aq;

    invoke-direct {v0, p0, p0, p1}, Lcom/osp/app/signin/aq;-><init>(Lcom/osp/app/signin/AccountView;Landroid/content/Context;Ljava/lang/String;)V

    invoke-virtual {v0}, Lcom/osp/app/signin/aq;->b()V

    goto :goto_0
.end method

.method private b(Ljava/lang/String;Z)V
    .locals 5

    .prologue
    const/4 v4, 0x1

    .line 3029
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "AccountView::ShowSigninActivity emailID : "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " isCancelableJustOneActivity : "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;)V

    .line 3030
    iget-boolean v0, p0, Lcom/osp/app/signin/AccountView;->B:Z

    if-nez v0, :cond_4

    .line 3032
    iput-boolean v4, p0, Lcom/osp/app/signin/AccountView;->B:Z

    .line 3033
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    .line 3035
    const-string v1, "client_id"

    iget-object v2, p0, Lcom/osp/app/signin/AccountView;->H:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 3036
    const-string v1, "client_secret"

    iget-object v2, p0, Lcom/osp/app/signin/AccountView;->I:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 3038
    invoke-direct {p0}, Lcom/osp/app/signin/AccountView;->r()Z

    move-result v1

    if-eqz v1, :cond_5

    .line 3040
    const-class v1, Lcom/osp/app/signin/SignInView$SignInPopup;

    invoke-virtual {v0, p0, v1}, Landroid/content/Intent;->setClass(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;

    .line 3041
    const-string v1, "MODE"

    const-string v2, "SIGNIN_POPUP"

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 3042
    const-string v1, "theme"

    iget-object v2, p0, Lcom/osp/app/signin/AccountView;->aA:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 3053
    :goto_0
    const-string v1, "account_mode"

    iget-object v2, p0, Lcom/osp/app/signin/AccountView;->C:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 3054
    const-string v1, "OSP_VER"

    iget-object v2, p0, Lcom/osp/app/signin/AccountView;->L:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 3055
    const-string v1, "BG_WhoareU"

    iget-object v2, p0, Lcom/osp/app/signin/AccountView;->T:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 3056
    const-string v1, "key_easy_signup_tnc_mode"

    iget-boolean v2, p0, Lcom/osp/app/signin/AccountView;->aF:Z

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 3058
    iget-boolean v1, p0, Lcom/osp/app/signin/AccountView;->e:Z

    if-eqz v1, :cond_0

    .line 3060
    const-string v1, "new_add_account_mode"

    invoke-virtual {v0, v1, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 3062
    :cond_0
    const-string v1, "BG_mode"

    iget-object v2, p0, Lcom/osp/app/signin/AccountView;->T:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 3064
    iget-object v1, p0, Lcom/osp/app/signin/AccountView;->R:Ljava/lang/String;

    if-eqz v1, :cond_1

    .line 3066
    const-string v1, "mypackage"

    iget-object v2, p0, Lcom/osp/app/signin/AccountView;->R:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 3067
    const-string v1, "key_request_id"

    iget-wide v2, p0, Lcom/osp/app/signin/AccountView;->Z:J

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;J)Landroid/content/Intent;

    .line 3071
    :cond_1
    if-eqz p1, :cond_2

    .line 3073
    const-string v1, "email_id"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 3076
    :cond_2
    if-eqz p2, :cond_3

    .line 3078
    const-string v1, "is_cancelable_just_one_activity"

    invoke-virtual {v0, v1, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 3106
    :cond_3
    const-string v1, "com.msc.action.MARKETING_POPUP_NOTIFICATION"

    iget-boolean v2, p0, Lcom/osp/app/signin/AccountView;->ap:Z

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 3108
    const/16 v1, 0xcd

    invoke-virtual {p0, v0, v1}, Lcom/osp/app/signin/AccountView;->startActivityForResult(Landroid/content/Intent;I)V

    .line 3110
    :cond_4
    return-void

    .line 3045
    :cond_5
    const-class v1, Lcom/osp/app/signin/SignInView;

    invoke-virtual {v0, p0, v1}, Landroid/content/Intent;->setClass(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;

    .line 3046
    const-string v1, "MODE"

    iget-object v2, p0, Lcom/osp/app/signin/AccountView;->S:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    goto :goto_0
.end method

.method static synthetic b(Lcom/osp/app/signin/AccountView;)Z
    .locals 1

    .prologue
    .line 142
    iget-boolean v0, p0, Lcom/osp/app/signin/AccountView;->aB:Z

    return v0
.end method

.method static synthetic b(Lcom/osp/app/signin/AccountView;Z)Z
    .locals 0

    .prologue
    .line 142
    iput-boolean p1, p0, Lcom/osp/app/signin/AccountView;->ap:Z

    return p1
.end method

.method static synthetic c(Lcom/osp/app/signin/AccountView;I)V
    .locals 0

    .prologue
    .line 142
    invoke-direct {p0, p1}, Lcom/osp/app/signin/AccountView;->e(I)V

    return-void
.end method

.method static synthetic c(Lcom/osp/app/signin/AccountView;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 142
    invoke-direct {p0, p1}, Lcom/osp/app/signin/AccountView;->i(Ljava/lang/String;)V

    return-void
.end method

.method private c(Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 3236
    new-instance v0, Landroid/content/Intent;

    const-string v1, "com.msc.action.samsungaccount.web_with_close_button"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 3237
    const-string v1, "ServerUrl"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 3239
    const/16 v1, 0xd4

    invoke-virtual {p0, v0, v1}, Lcom/osp/app/signin/AccountView;->startActivityForResult(Landroid/content/Intent;I)V

    .line 3240
    return-void
.end method

.method static synthetic c(Lcom/osp/app/signin/AccountView;)Z
    .locals 1

    .prologue
    .line 142
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/osp/app/signin/AccountView;->aC:Z

    return v0
.end method

.method static synthetic c(Lcom/osp/app/signin/AccountView;Z)Z
    .locals 0

    .prologue
    .line 142
    iput-boolean p1, p0, Lcom/osp/app/signin/AccountView;->aF:Z

    return p1
.end method

.method static synthetic d(Lcom/osp/app/signin/AccountView;)Lcom/osp/app/signin/h;
    .locals 1

    .prologue
    .line 142
    iget-object v0, p0, Lcom/osp/app/signin/AccountView;->aM:Lcom/osp/app/signin/h;

    return-object v0
.end method

.method static synthetic d(Lcom/osp/app/signin/AccountView;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 142
    invoke-virtual {p0, p1}, Lcom/osp/app/signin/AccountView;->d(Ljava/lang/String;)V

    return-void
.end method

.method static synthetic e(Lcom/osp/app/signin/AccountView;)Landroid/app/AlertDialog;
    .locals 1

    .prologue
    .line 142
    iget-object v0, p0, Lcom/osp/app/signin/AccountView;->aD:Landroid/app/AlertDialog;

    return-object v0
.end method

.method private e(I)V
    .locals 4

    .prologue
    .line 8706
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    .line 8707
    const-class v1, Lcom/osp/app/signin/SelectCountryView;

    invoke-virtual {v0, p0, v1}, Landroid/content/Intent;->setClass(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;

    .line 8716
    const-string v1, "MODE"

    const-string v2, "ADD_ACCOUNT"

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 8717
    const-string v1, "BG_WhoareU"

    iget-object v2, p0, Lcom/osp/app/signin/AccountView;->T:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 8718
    const-string v1, "com.osp.app.signin.action.SAMSUNG_ACCOUNT_SETUPWIZARD"

    invoke-static {}, Lcom/osp/app/signin/SamsungService;->a()Z

    move-result v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 8722
    const-string v1, "BG_mode"

    iget-object v2, p0, Lcom/osp/app/signin/AccountView;->T:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 8724
    iget-object v1, p0, Lcom/osp/app/signin/AccountView;->R:Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 8726
    const-string v1, "mypackage"

    iget-object v2, p0, Lcom/osp/app/signin/AccountView;->R:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 8727
    const-string v1, "key_request_id"

    iget-wide v2, p0, Lcom/osp/app/signin/AccountView;->Z:J

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;J)Landroid/content/Intent;

    .line 8732
    :cond_0
    invoke-virtual {p0, v0, p1}, Lcom/osp/app/signin/AccountView;->startActivityForResult(Landroid/content/Intent;I)V

    .line 8734
    return-void
.end method

.method static synthetic e(Lcom/osp/app/signin/AccountView;Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 142
    const/4 v0, 0x1

    invoke-direct {p0, p1, v0}, Lcom/osp/app/signin/AccountView;->a(Ljava/lang/String;Z)V

    return-void
.end method

.method private f(I)Landroid/app/Dialog;
    .locals 8

    .prologue
    const/4 v2, 0x0

    .line 9003
    .line 9009
    :try_start_0
    invoke-static {p0}, Lcom/osp/app/util/ad;->b(Landroid/content/Context;)Z

    move-result v0

    .line 9014
    if-eqz v0, :cond_3

    .line 9016
    const v1, 0x7f0900f5

    .line 9017
    const v0, 0x7f090126

    move v3, v0

    move v4, v1

    .line 9038
    :goto_0
    invoke-static {p0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    const v1, 0x7f030019

    const/4 v5, 0x0

    invoke-virtual {v0, v1, v5}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v5

    .line 9039
    const v0, 0x7f0c00be

    invoke-virtual {v5, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 9040
    const v1, 0x7f0c00bf

    invoke-virtual {v5, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/CheckBox;

    .line 9041
    invoke-virtual {p0}, Lcom/osp/app/signin/AccountView;->getResources()Landroid/content/res/Resources;

    move-result-object v6

    .line 9043
    if-eqz v0, :cond_0

    .line 9045
    invoke-static {p0}, Lcom/osp/app/util/an;->a(Lcom/osp/app/util/BaseActivity;)Lcom/osp/app/util/an;

    const v7, 0x7f070020

    invoke-static {v7}, Lcom/osp/app/util/an;->a(I)I

    move-result v7

    invoke-virtual {v6, v7}, Landroid/content/res/Resources;->getColor(I)I

    move-result v7

    invoke-virtual {v0, v7}, Landroid/widget/TextView;->setTextColor(I)V

    .line 9046
    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setText(I)V

    .line 9049
    :cond_0
    if-eqz v1, :cond_2

    .line 9051
    const/4 v0, 0x1

    invoke-virtual {v1, v0}, Landroid/widget/CheckBox;->setChecked(Z)V

    .line 9052
    invoke-static {p0}, Lcom/osp/app/util/an;->a(Lcom/osp/app/util/BaseActivity;)Lcom/osp/app/util/an;

    const v0, 0x7f070020

    invoke-static {v0}, Lcom/osp/app/util/an;->a(I)I

    move-result v0

    invoke-virtual {v6, v0}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    invoke-virtual {v1, v0}, Landroid/widget/CheckBox;->setTextColor(I)V

    .line 9053
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v3, 0x10

    if-le v0, v3, :cond_1

    .line 9055
    invoke-virtual {p0}, Lcom/osp/app/signin/AccountView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v3, 0x7f0800e2

    invoke-virtual {v0, v3}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v0

    float-to-int v0, v0

    .line 9057
    const/4 v3, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    invoke-static {v1, v0, v3, v6, v7}, Lcom/msc/sa/c/d;->a(Landroid/view/View;IIII)V

    .line 9060
    :cond_1
    new-instance v0, Lcom/osp/app/signin/y;

    invoke-direct {v0, p0}, Lcom/osp/app/signin/y;-><init>(Lcom/osp/app/signin/AccountView;)V

    invoke-virtual {v1, v0}, Landroid/widget/CheckBox;->setOnCheckedChangeListener(Landroid/widget/CompoundButton$OnCheckedChangeListener;)V

    .line 9072
    :cond_2
    new-instance v0, Landroid/app/AlertDialog$Builder;

    invoke-direct {v0, p0}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    invoke-virtual {v0, v4}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    invoke-virtual {v0, v5}, Landroid/app/AlertDialog$Builder;->setView(Landroid/view/View;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    const v3, 0x7f090042

    new-instance v4, Lcom/osp/app/signin/ac;

    invoke-direct {v4, p0, v1, p1}, Lcom/osp/app/signin/ac;-><init>(Lcom/osp/app/signin/AccountView;Landroid/widget/CheckBox;I)V

    invoke-virtual {v0, v3, v4}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    const v1, 0x7f090019

    new-instance v3, Lcom/osp/app/signin/ab;

    invoke-direct {v3, p0}, Lcom/osp/app/signin/ab;-><init>(Lcom/osp/app/signin/AccountView;)V

    invoke-virtual {v0, v1, v3}, Landroid/app/AlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    new-instance v1, Lcom/osp/app/signin/aa;

    invoke-direct {v1, p0}, Lcom/osp/app/signin/aa;-><init>(Lcom/osp/app/signin/AccountView;)V

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setOnCancelListener(Landroid/content/DialogInterface$OnCancelListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v0

    .line 9106
    :goto_1
    return-object v0

    .line 9021
    :cond_3
    invoke-static {p0}, Lcom/osp/common/util/i;->a(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 9023
    const v1, 0x7f0900f6

    .line 9024
    const v0, 0x7f0900f7

    move v3, v0

    move v4, v1

    goto/16 :goto_0

    .line 9027
    :cond_4
    const v1, 0x7f090132

    .line 9028
    invoke-static {p0}, Lcom/osp/app/util/ad;->c(Landroid/content/Context;)Z
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result v0

    if-eqz v0, :cond_5

    .line 9030
    const v0, 0x7f090128

    move v3, v0

    move v4, v1

    goto/16 :goto_0

    .line 9033
    :cond_5
    const v0, 0x7f090127

    move v3, v0

    move v4, v1

    goto/16 :goto_0

    .line 9101
    :catch_0
    move-exception v0

    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    move-object v0, v2

    goto :goto_1
.end method

.method static synthetic f(Lcom/osp/app/signin/AccountView;Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 142
    const/4 v0, 0x1

    invoke-direct {p0, p1, v0}, Lcom/osp/app/signin/AccountView;->b(Ljava/lang/String;Z)V

    return-void
.end method

.method static synthetic f(Lcom/osp/app/signin/AccountView;)Z
    .locals 1

    .prologue
    .line 142
    iget-boolean v0, p0, Lcom/osp/app/signin/AccountView;->Q:Z

    return v0
.end method

.method static synthetic g()I
    .locals 1

    .prologue
    .line 142
    sget v0, Lcom/osp/app/signin/AccountView;->aN:I

    return v0
.end method

.method static synthetic g(Lcom/osp/app/signin/AccountView;Ljava/lang/String;)Ljava/lang/String;
    .locals 0

    .prologue
    .line 142
    iput-object p1, p0, Lcom/osp/app/signin/AccountView;->ac:Ljava/lang/String;

    return-object p1
.end method

.method private g(I)V
    .locals 6

    .prologue
    const/4 v5, 0x0

    const/4 v4, 0x1

    .line 10874
    if-ne p1, v4, :cond_0

    invoke-static {}, Lcom/osp/app/signin/SamsungService;->a()Z

    move-result v0

    if-nez v0, :cond_0

    .line 10876
    invoke-direct {p0}, Lcom/osp/app/signin/AccountView;->z()V

    .line 10890
    :goto_0
    return-void

    .line 10879
    :cond_0
    iget-object v0, p0, Lcom/osp/app/signin/AccountView;->ab:Landroid/app/ProgressDialog;

    if-nez v0, :cond_1

    new-instance v0, Landroid/app/ProgressDialog;

    invoke-direct {v0, p0}, Landroid/app/ProgressDialog;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/osp/app/signin/AccountView;->ab:Landroid/app/ProgressDialog;

    iget-object v0, p0, Lcom/osp/app/signin/AccountView;->ab:Landroid/app/ProgressDialog;

    const v1, 0x7f090036

    invoke-static {p0, v1}, Lcom/msc/sa/c/d;->a(Landroid/content/Context;I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/app/ProgressDialog;->setMessage(Ljava/lang/CharSequence;)V

    iget-object v0, p0, Lcom/osp/app/signin/AccountView;->ab:Landroid/app/ProgressDialog;

    invoke-virtual {v0, v4}, Landroid/app/ProgressDialog;->setCancelable(Z)V

    iget-object v0, p0, Lcom/osp/app/signin/AccountView;->ab:Landroid/app/ProgressDialog;

    invoke-virtual {v0, v5}, Landroid/app/ProgressDialog;->setCanceledOnTouchOutside(Z)V

    iget-object v0, p0, Lcom/osp/app/signin/AccountView;->ab:Landroid/app/ProgressDialog;

    new-instance v1, Lcom/osp/app/signin/al;

    invoke-direct {v1, p0}, Lcom/osp/app/signin/al;-><init>(Lcom/osp/app/signin/AccountView;)V

    invoke-virtual {v0, v1}, Landroid/app/ProgressDialog;->setOnCancelListener(Landroid/content/DialogInterface$OnCancelListener;)V

    :try_start_0
    invoke-virtual {p0}, Lcom/osp/app/signin/AccountView;->isFinishing()Z

    move-result v0

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/osp/app/signin/AccountView;->ab:Landroid/app/ProgressDialog;

    invoke-virtual {v0}, Landroid/app/ProgressDialog;->show()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 10881
    :cond_1
    :goto_1
    invoke-direct {p0}, Lcom/osp/app/signin/AccountView;->N()V

    :try_start_1
    new-instance v0, Ljava/util/Timer;

    const/4 v1, 0x1

    invoke-direct {v0, v1}, Ljava/util/Timer;-><init>(Z)V

    iput-object v0, p0, Lcom/osp/app/signin/AccountView;->aH:Ljava/util/Timer;

    iget-object v0, p0, Lcom/osp/app/signin/AccountView;->aH:Ljava/util/Timer;

    new-instance v1, Lcom/osp/app/signin/bh;

    invoke-direct {v1, p0}, Lcom/osp/app/signin/bh;-><init>(Lcom/osp/app/signin/AccountView;)V

    const-wide/32 v2, 0xea60

    invoke-virtual {v0, v1, v2, v3}, Ljava/util/Timer;->schedule(Ljava/util/TimerTask;J)V
    :try_end_1
    .catch Ljava/lang/IllegalStateException; {:try_start_1 .. :try_end_1} :catch_1

    .line 10883
    :goto_2
    iget-object v0, p0, Lcom/osp/app/signin/AccountView;->aE:Lcom/osp/app/signin/bb;

    if-nez v0, :cond_2

    .line 10885
    new-instance v0, Lcom/osp/app/signin/bb;

    invoke-direct {v0, p0, v5}, Lcom/osp/app/signin/bb;-><init>(Lcom/osp/app/signin/AccountView;B)V

    iput-object v0, p0, Lcom/osp/app/signin/AccountView;->aE:Lcom/osp/app/signin/bb;

    .line 10887
    :cond_2
    iget-object v0, p0, Lcom/osp/app/signin/AccountView;->aE:Lcom/osp/app/signin/bb;

    invoke-virtual {v0, p1}, Lcom/osp/app/signin/bb;->a(I)V

    .line 10888
    invoke-static {}, Lcom/osp/app/util/m;->a()Lcom/osp/app/util/m;

    iget-object v0, p0, Lcom/osp/app/signin/AccountView;->aE:Lcom/osp/app/signin/bb;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacksAndMessages(Ljava/lang/Object;)V

    new-instance v1, Landroid/os/Messenger;

    invoke-direct {v1, v0}, Landroid/os/Messenger;-><init>(Landroid/os/Handler;)V

    new-instance v0, Landroid/content/Intent;

    const-string v2, "com.samsung.android.coreapps.easysignup.ACTION_IS_AUTH"

    invoke-direct {v0, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    const/16 v2, 0x20

    invoke-virtual {v0, v2}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    const-string v2, "extra_cb_handler"

    invoke-virtual {v0, v2, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    const-string v1, "token"

    invoke-virtual {v0, v1, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    invoke-virtual {p0, v0}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V

    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v0, "ESUU"

    const-string v1, "sendBoardcastForIsAuth start"

    invoke-static {v0, v1}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 10879
    :catch_0
    move-exception v0

    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_1

    .line 10881
    :catch_1
    move-exception v0

    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v0, "AV"

    const-string v1, "mEasySignupTimer is canceled"

    invoke-static {v0, v1}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_2
.end method

.method static synthetic g(Lcom/osp/app/signin/AccountView;)Z
    .locals 1

    .prologue
    .line 142
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/osp/app/signin/AccountView;->an:Z

    return v0
.end method

.method static synthetic h(Lcom/osp/app/signin/AccountView;Ljava/lang/String;)Ljava/lang/String;
    .locals 0

    .prologue
    .line 142
    iput-object p1, p0, Lcom/osp/app/signin/AccountView;->au:Ljava/lang/String;

    return-object p1
.end method

.method static synthetic h(Lcom/osp/app/signin/AccountView;)Z
    .locals 1

    .prologue
    .line 142
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/osp/app/signin/AccountView;->Q:Z

    return v0
.end method

.method static synthetic i(Lcom/osp/app/signin/AccountView;Ljava/lang/String;)Ljava/lang/String;
    .locals 0

    .prologue
    .line 142
    iput-object p1, p0, Lcom/osp/app/signin/AccountView;->at:Ljava/lang/String;

    return-object p1
.end method

.method private i(Ljava/lang/String;)V
    .locals 4

    .prologue
    .line 4999
    new-instance v0, Landroid/content/Intent;

    const-string v1, "com.msc.action.samsungaccount.resignin"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 5000
    const-string v1, "client_id"

    iget-object v2, p0, Lcom/osp/app/signin/AccountView;->H:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 5001
    const-string v1, "client_secret"

    iget-object v2, p0, Lcom/osp/app/signin/AccountView;->I:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 5002
    const-string v1, "account_mode"

    iget-object v2, p0, Lcom/osp/app/signin/AccountView;->C:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 5003
    const-string v1, "OSP_VER"

    iget-object v2, p0, Lcom/osp/app/signin/AccountView;->L:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 5004
    const-string v1, "MODE"

    iget-object v2, p0, Lcom/osp/app/signin/AccountView;->S:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 5005
    const-string v1, "BG_WhoareU"

    iget-object v2, p0, Lcom/osp/app/signin/AccountView;->T:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 5006
    const-string v1, "BG_mode"

    iget-object v2, p0, Lcom/osp/app/signin/AccountView;->T:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 5008
    iget-object v1, p0, Lcom/osp/app/signin/AccountView;->R:Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 5010
    const-string v1, "mypackage"

    iget-object v2, p0, Lcom/osp/app/signin/AccountView;->R:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 5011
    const-string v1, "key_request_id"

    iget-wide v2, p0, Lcom/osp/app/signin/AccountView;->Z:J

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;J)Landroid/content/Intent;

    .line 5015
    :cond_0
    if-eqz p1, :cond_1

    .line 5017
    const-string v1, "email_id"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 5027
    :cond_1
    const/16 v1, 0xcd

    invoke-virtual {p0, v0, v1}, Lcom/osp/app/signin/AccountView;->startActivityForResult(Landroid/content/Intent;I)V

    .line 5029
    return-void
.end method

.method static synthetic i(Lcom/osp/app/signin/AccountView;)Z
    .locals 1

    .prologue
    .line 142
    iget-boolean v0, p0, Lcom/osp/app/signin/AccountView;->aQ:Z

    return v0
.end method

.method private static j(Ljava/lang/String;)Landroid/graphics/Bitmap;
    .locals 3

    .prologue
    .line 9444
    :try_start_0
    new-instance v0, Ljava/net/URL;

    invoke-direct {v0, p0}, Ljava/net/URL;-><init>(Ljava/lang/String;)V

    .line 9449
    invoke-virtual {v0}, Ljava/net/URL;->openConnection()Ljava/net/URLConnection;

    move-result-object v0

    .line 9450
    invoke-virtual {v0}, Ljava/net/URLConnection;->connect()V

    .line 9452
    invoke-virtual {v0}, Ljava/net/URLConnection;->getContentLength()I

    move-result v1

    .line 9453
    new-instance v2, Ljava/io/BufferedInputStream;

    invoke-virtual {v0}, Ljava/net/URLConnection;->getInputStream()Ljava/io/InputStream;

    move-result-object v0

    invoke-direct {v2, v0, v1}, Ljava/io/BufferedInputStream;-><init>(Ljava/io/InputStream;I)V

    .line 9454
    invoke-static {v2}, Landroid/graphics/BitmapFactory;->decodeStream(Ljava/io/InputStream;)Landroid/graphics/Bitmap;

    move-result-object v0

    .line 9455
    invoke-virtual {v2}, Ljava/io/BufferedInputStream;->close()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 9462
    :goto_0
    return-object v0

    .line 9456
    :catch_0
    move-exception v0

    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    .line 9459
    const/4 v0, 0x0

    goto :goto_0
.end method

.method static synthetic j(Lcom/osp/app/signin/AccountView;Ljava/lang/String;)Ljava/lang/String;
    .locals 0

    .prologue
    .line 142
    iput-object p1, p0, Lcom/osp/app/signin/AccountView;->aG:Ljava/lang/String;

    return-object p1
.end method

.method static synthetic j(Lcom/osp/app/signin/AccountView;)V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 142
    invoke-direct {p0, v0, v0}, Lcom/osp/app/signin/AccountView;->a(ZZ)V

    return-void
.end method

.method static synthetic k(Lcom/osp/app/signin/AccountView;)V
    .locals 0

    .prologue
    .line 142
    invoke-direct {p0}, Lcom/osp/app/signin/AccountView;->x()V

    return-void
.end method

.method static synthetic l(Lcom/osp/app/signin/AccountView;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 142
    iget-object v0, p0, Lcom/osp/app/signin/AccountView;->H:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic m(Lcom/osp/app/signin/AccountView;)Landroid/content/Intent;
    .locals 1

    .prologue
    .line 142
    iget-object v0, p0, Lcom/osp/app/signin/AccountView;->J:Landroid/content/Intent;

    return-object v0
.end method

.method static synthetic n(Lcom/osp/app/signin/AccountView;)Z
    .locals 1

    .prologue
    .line 142
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/osp/app/signin/AccountView;->ar:Z

    return v0
.end method

.method static synthetic o(Lcom/osp/app/signin/AccountView;)Z
    .locals 1

    .prologue
    .line 142
    iget-boolean v0, p0, Lcom/osp/app/signin/AccountView;->P:Z

    return v0
.end method

.method static synthetic p(Lcom/osp/app/signin/AccountView;)Z
    .locals 1

    .prologue
    .line 142
    iget-boolean v0, p0, Lcom/osp/app/signin/AccountView;->aq:Z

    return v0
.end method

.method static synthetic q(Lcom/osp/app/signin/AccountView;)Lcom/osp/app/signin/be;
    .locals 1

    .prologue
    .line 142
    iget-object v0, p0, Lcom/osp/app/signin/AccountView;->E:Lcom/osp/app/signin/be;

    return-object v0
.end method

.method private q()V
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 842
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v1, "AccountView::CheckListExecute"

    invoke-static {v1}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;)V

    .line 852
    invoke-static {p0}, Lcom/osp/app/util/ad;->a(Landroid/content/Context;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 854
    invoke-static {}, Lcom/osp/app/util/ap;->a()Lcom/osp/app/util/ap;

    const v1, 0x7f09003e

    invoke-virtual {p0, v1}, Lcom/osp/app/signin/AccountView;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-static {p0, v1, v0}, Lcom/osp/app/util/ap;->a(Landroid/content/Context;Ljava/lang/String;I)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    .line 865
    :goto_0
    return-void

    .line 855
    :cond_0
    invoke-static {p0}, Lcom/osp/app/util/ad;->d(Landroid/content/Context;)Z

    move-result v1

    if-eqz v1, :cond_2

    const-string v1, "OSP_02"

    iget-object v2, p0, Lcom/osp/app/signin/AccountView;->L:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_1

    const/4 v0, 0x1

    :cond_1
    invoke-static {p0, v0}, Lcom/msc/c/c;->a(Landroid/content/Context;Z)Z

    move-result v0

    if-nez v0, :cond_2

    .line 857
    invoke-static {p0}, Lcom/osp/app/util/ad;->h(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/osp/app/signin/AccountView;->i(Ljava/lang/String;)V

    goto :goto_0

    .line 860
    :cond_2
    new-instance v0, Lcom/osp/app/signin/ba;

    iget-boolean v1, p0, Lcom/osp/app/signin/AccountView;->K:Z

    invoke-direct {v0, p0, v1}, Lcom/osp/app/signin/ba;-><init>(Lcom/osp/app/signin/AccountView;Z)V

    iput-object v0, p0, Lcom/osp/app/signin/AccountView;->F:Lcom/osp/app/signin/ba;

    .line 862
    iget-object v0, p0, Lcom/osp/app/signin/AccountView;->F:Lcom/osp/app/signin/ba;

    invoke-virtual {v0}, Lcom/osp/app/signin/ba;->b()V

    goto :goto_0
.end method

.method private r()Z
    .locals 2

    .prologue
    .line 872
    const-string v0, "com.msc.action.samsungaccount.SIGNIN_POPUP"

    invoke-virtual {p0}, Lcom/osp/app/signin/AccountView;->getIntent()Landroid/content/Intent;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-static {p0}, Lcom/osp/app/util/r;->m(Landroid/content/Context;)Z

    .line 875
    const/4 v0, 0x1

    .line 878
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method static synthetic r(Lcom/osp/app/signin/AccountView;)Z
    .locals 1

    .prologue
    .line 142
    iget-boolean v0, p0, Lcom/osp/app/signin/AccountView;->af:Z

    return v0
.end method

.method private s()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 1189
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "AccountView::getInformation state : "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v1, p0, Lcom/osp/app/signin/AccountView;->V:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;)V

    .line 1191
    iget v0, p0, Lcom/osp/app/signin/AccountView;->V:I

    packed-switch v0, :pswitch_data_0

    .line 1202
    :pswitch_0
    invoke-virtual {p0}, Lcom/osp/app/signin/AccountView;->finish()V

    .line 1205
    :goto_0
    return-void

    .line 1194
    :pswitch_1
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v0, "AccountView::getAuthCode"

    invoke-static {v0}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;)V

    const-string v0, "OSP_02"

    iget-object v1, p0, Lcom/osp/app/signin/AccountView;->L:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    new-instance v0, Lcom/osp/app/signin/bd;

    invoke-direct {v0, p0}, Lcom/osp/app/signin/bd;-><init>(Lcom/osp/app/signin/AccountView;)V

    iput-object v0, p0, Lcom/osp/app/signin/AccountView;->D:Lcom/osp/app/signin/bd;

    iget-object v0, p0, Lcom/osp/app/signin/AccountView;->D:Lcom/osp/app/signin/bd;

    invoke-virtual {v0}, Lcom/osp/app/signin/bd;->b()V

    goto :goto_0

    :cond_0
    iget-boolean v0, p0, Lcom/osp/app/signin/AccountView;->K:Z

    if-eqz v0, :cond_1

    invoke-direct {p0, v2}, Lcom/osp/app/signin/AccountView;->a(Z)V

    goto :goto_0

    :cond_1
    invoke-virtual {p0}, Lcom/osp/app/signin/AccountView;->finish()V

    goto :goto_0

    .line 1198
    :pswitch_2
    invoke-direct {p0, v2}, Lcom/osp/app/signin/AccountView;->a(Z)V

    goto :goto_0

    .line 1191
    :pswitch_data_0
    .packed-switch 0x3
        :pswitch_1
        :pswitch_0
        :pswitch_0
        :pswitch_2
    .end packed-switch
.end method

.method static synthetic s(Lcom/osp/app/signin/AccountView;)Z
    .locals 1

    .prologue
    .line 142
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/osp/app/signin/AccountView;->af:Z

    return v0
.end method

.method private t()V
    .locals 4

    .prologue
    .line 1242
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v0, "AccountView::LaunchNewTnc"

    invoke-static {v0}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;)V

    .line 1245
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/osp/app/signin/TnCView;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 1248
    const-string v1, "com.msc.action.samsungaccount.Update_NewTerms"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 1250
    const/high16 v1, 0x20000

    invoke-virtual {v0, v1}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    .line 1252
    const-string v1, "key_tnc_update_mode"

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 1253
    const-string v1, "key_request_id"

    iget-wide v2, p0, Lcom/osp/app/signin/AccountView;->Z:J

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;J)Landroid/content/Intent;

    .line 1254
    const-string v1, "country_code_mcc"

    iget-object v2, p0, Lcom/osp/app/signin/AccountView;->ag:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 1258
    const/16 v1, 0xd1

    invoke-virtual {p0, v0, v1}, Lcom/osp/app/signin/AccountView;->startActivityForResult(Landroid/content/Intent;I)V

    .line 1261
    return-void
.end method

.method static synthetic t(Lcom/osp/app/signin/AccountView;)Z
    .locals 1

    .prologue
    .line 142
    invoke-virtual {p0}, Lcom/osp/app/signin/AccountView;->h()Z

    move-result v0

    return v0
.end method

.method private u()V
    .locals 2

    .prologue
    .line 1696
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v0, "AV"

    const-string v1, "from full screen"

    invoke-static {v0, v1}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 1698
    new-instance v0, Lcom/osp/app/signin/ap;

    invoke-direct {v0, p0, p0}, Lcom/osp/app/signin/ap;-><init>(Lcom/osp/app/signin/AccountView;Landroid/content/Context;)V

    invoke-virtual {v0}, Lcom/osp/app/signin/ap;->b()V

    .line 1739
    return-void
.end method

.method static synthetic u(Lcom/osp/app/signin/AccountView;)Z
    .locals 1

    .prologue
    .line 142
    invoke-direct {p0}, Lcom/osp/app/signin/AccountView;->C()Z

    move-result v0

    return v0
.end method

.method static synthetic v(Lcom/osp/app/signin/AccountView;)I
    .locals 1

    .prologue
    .line 142
    iget v0, p0, Lcom/osp/app/signin/AccountView;->V:I

    return v0
.end method

.method private v()Ljava/lang/String;
    .locals 4

    .prologue
    .line 1876
    const/4 v1, 0x0

    .line 1877
    invoke-virtual {p0}, Lcom/osp/app/signin/AccountView;->getCallingPackage()Ljava/lang/String;

    move-result-object v0

    .line 1878
    if-eqz v0, :cond_0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_0

    invoke-virtual {p0}, Lcom/osp/app/signin/AccountView;->getPackageName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 1886
    :goto_0
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v1, "AV"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "getSignatureCheckingPackageName-"

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 1888
    return-object v0

    .line 1881
    :cond_0
    iget-object v0, p0, Lcom/osp/app/signin/AccountView;->R:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 1883
    iget-object v0, p0, Lcom/osp/app/signin/AccountView;->R:Ljava/lang/String;

    goto :goto_0

    :cond_1
    move-object v0, v1

    goto :goto_0
.end method

.method static synthetic w(Lcom/osp/app/signin/AccountView;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 142
    iget-object v0, p0, Lcom/osp/app/signin/AccountView;->ag:Ljava/lang/String;

    return-object v0
.end method

.method private w()Z
    .locals 11

    .prologue
    const v10, 0x7f070014

    const v9, 0x7f0c0051

    const/4 v2, 0x1

    const/4 v3, 0x0

    const/16 v8, 0x8

    .line 1971
    const v0, 0x7f0c0045

    invoke-virtual {p0, v0}, Lcom/osp/app/signin/AccountView;->findViewById(I)Landroid/view/View;

    move-result-object v4

    .line 1972
    const v0, 0x7f0c004b

    invoke-virtual {p0, v0}, Lcom/osp/app/signin/AccountView;->findViewById(I)Landroid/view/View;

    move-result-object v5

    .line 1973
    const v0, 0x7f0c0057

    invoke-virtual {p0, v0}, Lcom/osp/app/signin/AccountView;->findViewById(I)Landroid/view/View;

    move-result-object v6

    .line 1975
    invoke-static {}, Lcom/osp/device/b;->b()Lcom/osp/device/b;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/osp/device/b;->d(Landroid/content/Context;)F

    move-result v0

    .line 1977
    invoke-direct {p0}, Lcom/osp/app/signin/AccountView;->L()V

    .line 1982
    invoke-static {p0}, Lcom/osp/app/util/r;->c(Landroid/content/Context;)Z

    move-result v1

    if-eqz v1, :cond_5

    .line 1984
    const v1, 0x7f0c0052

    invoke-virtual {p0, v1}, Lcom/osp/app/signin/AccountView;->findViewById(I)Landroid/view/View;

    move-result-object v1

    invoke-virtual {v1, v8}, Landroid/view/View;->setVisibility(I)V

    .line 1985
    const v1, 0x7f0c0046

    invoke-virtual {p0, v1}, Lcom/osp/app/signin/AccountView;->findViewById(I)Landroid/view/View;

    move-result-object v1

    invoke-virtual {v1, v8}, Landroid/view/View;->setVisibility(I)V

    .line 1986
    const v1, 0x7f0c004c

    invoke-virtual {p0, v1}, Lcom/osp/app/signin/AccountView;->findViewById(I)Landroid/view/View;

    move-result-object v1

    invoke-virtual {v1, v8}, Landroid/view/View;->setVisibility(I)V

    .line 1987
    const v1, 0x7f0c0058

    invoke-virtual {p0, v1}, Lcom/osp/app/signin/AccountView;->findViewById(I)Landroid/view/View;

    move-result-object v1

    invoke-virtual {v1, v8}, Landroid/view/View;->setVisibility(I)V

    .line 1989
    const v1, 0x7f0c0055

    invoke-virtual {p0, v1}, Lcom/osp/app/signin/AccountView;->findViewById(I)Landroid/view/View;

    move-result-object v1

    invoke-virtual {v1, v3}, Landroid/view/View;->setVisibility(I)V

    .line 1990
    const v1, 0x7f0c0049

    invoke-virtual {p0, v1}, Lcom/osp/app/signin/AccountView;->findViewById(I)Landroid/view/View;

    move-result-object v1

    invoke-virtual {v1, v3}, Landroid/view/View;->setVisibility(I)V

    .line 1991
    const v1, 0x7f0c004f

    invoke-virtual {p0, v1}, Lcom/osp/app/signin/AccountView;->findViewById(I)Landroid/view/View;

    move-result-object v1

    invoke-virtual {v1, v3}, Landroid/view/View;->setVisibility(I)V

    .line 1992
    const v1, 0x7f0c005b

    invoke-virtual {p0, v1}, Lcom/osp/app/signin/AccountView;->findViewById(I)Landroid/view/View;

    move-result-object v1

    invoke-virtual {v1, v8}, Landroid/view/View;->setVisibility(I)V

    .line 1999
    :goto_0
    iget-object v1, p0, Lcom/osp/app/signin/AccountView;->ak:Lcom/osp/app/signin/fv;

    if-eqz v1, :cond_a

    iget-object v1, p0, Lcom/osp/app/signin/AccountView;->ak:Lcom/osp/app/signin/fv;

    invoke-virtual {v1}, Lcom/osp/app/signin/fv;->j()Z

    move-result v1

    if-eqz v1, :cond_a

    iget-object v1, p0, Lcom/osp/app/signin/AccountView;->ak:Lcom/osp/app/signin/fv;

    invoke-virtual {v1}, Lcom/osp/app/signin/fv;->c()Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_a

    iget-object v1, p0, Lcom/osp/app/signin/AccountView;->ak:Lcom/osp/app/signin/fv;

    invoke-virtual {v1}, Lcom/osp/app/signin/fv;->c()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_a

    .line 2006
    const v0, 0x7f0c0043

    invoke-virtual {p0, v0}, Lcom/osp/app/signin/AccountView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ScrollView;

    .line 2007
    if-eqz v0, :cond_0

    .line 2009
    invoke-virtual {v0}, Landroid/widget/ScrollView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v1

    check-cast v1, Landroid/widget/LinearLayout$LayoutParams;

    .line 2010
    const/4 v7, -0x2

    iput v7, v1, Landroid/widget/LinearLayout$LayoutParams;->height:I

    .line 2011
    const/4 v7, 0x0

    iput v7, v1, Landroid/widget/LinearLayout$LayoutParams;->weight:F

    .line 2012
    invoke-virtual {v0, v1}, Landroid/widget/ScrollView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 2016
    :cond_0
    if-eqz v4, :cond_1

    .line 2018
    invoke-virtual {v4, v8}, Landroid/view/View;->setVisibility(I)V

    .line 2020
    :cond_1
    if-eqz v5, :cond_2

    .line 2022
    invoke-virtual {v5, v8}, Landroid/view/View;->setVisibility(I)V

    .line 2024
    :cond_2
    if-eqz v6, :cond_3

    .line 2026
    invoke-virtual {v6, v3}, Landroid/view/View;->setVisibility(I)V

    .line 2029
    :cond_3
    new-instance v1, Lcom/osp/app/signin/ar;

    invoke-direct {v1, p0}, Lcom/osp/app/signin/ar;-><init>(Lcom/osp/app/signin/AccountView;)V

    invoke-static {p0}, Lcom/osp/app/util/r;->g(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_6

    iget-object v0, p0, Lcom/osp/app/signin/AccountView;->aM:Lcom/osp/app/signin/h;

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "<u>"

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v5, p0, Lcom/osp/app/signin/AccountView;->ak:Lcom/osp/app/signin/fv;

    invoke-virtual {v5}, Lcom/osp/app/signin/fv;->c()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "</u>"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Landroid/text/Html;->fromHtml(Ljava/lang/String;)Landroid/text/Spanned;

    move-result-object v4

    invoke-virtual {v0, v4, v1}, Lcom/osp/app/signin/h;->a(Ljava/lang/CharSequence;Landroid/view/View$OnClickListener;)V

    const v0, 0x7f0c0070

    invoke-virtual {p0, v0}, Lcom/osp/app/signin/AccountView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    if-eqz v0, :cond_4

    invoke-virtual {p0}, Lcom/osp/app/signin/AccountView;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    invoke-static {p0}, Lcom/osp/app/util/an;->a(Lcom/osp/app/util/BaseActivity;)Lcom/osp/app/util/an;

    const v5, 0x7f07001e

    invoke-static {v5}, Lcom/osp/app/util/an;->a(I)I

    move-result v5

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getColor(I)I

    move-result v4

    invoke-virtual {v0, v4}, Landroid/widget/TextView;->setTextColor(I)V

    invoke-virtual {p0}, Lcom/osp/app/signin/AccountView;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    invoke-static {p0}, Lcom/osp/app/util/an;->a(Lcom/osp/app/util/BaseActivity;)Lcom/osp/app/util/an;

    const v5, 0x7f07001e

    invoke-static {v5}, Lcom/osp/app/util/an;->a(I)I

    move-result v5

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getColor(I)I

    move-result v4

    invoke-virtual {v0, v4}, Landroid/widget/TextView;->setLinkTextColor(I)V

    invoke-static {p0}, Lcom/osp/app/util/an;->a(Lcom/osp/app/util/BaseActivity;)Lcom/osp/app/util/an;

    const v4, 0x7f08004d

    invoke-static {v4}, Lcom/osp/app/util/an;->b(I)I

    move-result v4

    invoke-virtual {p0}, Lcom/osp/app/signin/AccountView;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    invoke-virtual {v5, v4}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v4

    invoke-virtual {v0, v3, v4}, Landroid/widget/TextView;->setTextSize(IF)V

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "<u>"

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v5, p0, Lcom/osp/app/signin/AccountView;->ak:Lcom/osp/app/signin/fv;

    invoke-virtual {v5}, Lcom/osp/app/signin/fv;->c()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "</u>"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Landroid/text/Html;->fromHtml(Ljava/lang/String;)Landroid/text/Spanned;

    move-result-object v4

    invoke-virtual {v0, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setVisibility(I)V

    const v0, 0x7f0c0071

    invoke-virtual {p0, v0}, Lcom/osp/app/signin/AccountView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    if-eqz v0, :cond_4

    const/4 v1, 0x3

    invoke-static {v0, v1}, Lcom/msc/sa/c/d;->a(Landroid/view/View;I)V

    .line 2031
    :cond_4
    :goto_1
    invoke-virtual {p0, v9}, Lcom/osp/app/signin/AccountView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v8}, Landroid/view/View;->setVisibility(I)V

    move v0, v2

    .line 2130
    :goto_2
    return v0

    .line 1995
    :cond_5
    const v1, 0x7f0c0058

    invoke-virtual {p0, v1}, Lcom/osp/app/signin/AccountView;->findViewById(I)Landroid/view/View;

    move-result-object v1

    invoke-virtual {v1, v8}, Landroid/view/View;->setVisibility(I)V

    goto/16 :goto_0

    .line 2029
    :cond_6
    const v0, 0x7f0c005d

    invoke-virtual {p0, v0}, Lcom/osp/app/signin/AccountView;->findViewById(I)Landroid/view/View;

    move-result-object v4

    const v0, 0x7f0c005e

    invoke-virtual {p0, v0}, Lcom/osp/app/signin/AccountView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    if-eqz v4, :cond_8

    invoke-static {}, Lcom/osp/device/b;->b()Lcom/osp/device/b;

    move-result-object v5

    invoke-virtual {v5, p0}, Lcom/osp/device/b;->i(Landroid/content/Context;)Z

    move-result v5

    if-nez v5, :cond_7

    invoke-static {}, Landroid/text/method/LinkMovementMethod;->getInstance()Landroid/text/method/MovementMethod;

    move-result-object v5

    invoke-virtual {v0, v5}, Landroid/widget/TextView;->setMovementMethod(Landroid/text/method/MovementMethod;)V

    :cond_7
    invoke-virtual {v4, v3}, Landroid/view/View;->setVisibility(I)V

    :cond_8
    if-eqz v0, :cond_4

    invoke-static {p0}, Lcom/osp/app/util/x;->i(Landroid/content/Context;)Z

    move-result v4

    if-eqz v4, :cond_9

    invoke-static {p0}, Lcom/osp/app/util/an;->a(Lcom/osp/app/util/BaseActivity;)Lcom/osp/app/util/an;

    const v4, 0x7f080032

    invoke-static {v4}, Lcom/osp/app/util/an;->b(I)I

    move-result v4

    invoke-virtual {p0}, Lcom/osp/app/signin/AccountView;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    invoke-virtual {v5, v4}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v4

    invoke-virtual {v0, v3, v4}, Landroid/widget/TextView;->setTextSize(IF)V

    :cond_9
    invoke-virtual {p0}, Lcom/osp/app/signin/AccountView;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    invoke-static {p0}, Lcom/osp/app/util/an;->a(Lcom/osp/app/util/BaseActivity;)Lcom/osp/app/util/an;

    invoke-static {v10}, Lcom/osp/app/util/an;->a(I)I

    move-result v5

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getColor(I)I

    move-result v4

    invoke-virtual {v0, v4}, Landroid/widget/TextView;->setTextColor(I)V

    invoke-virtual {p0}, Lcom/osp/app/signin/AccountView;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    invoke-static {p0}, Lcom/osp/app/util/an;->a(Lcom/osp/app/util/BaseActivity;)Lcom/osp/app/util/an;

    invoke-static {v10}, Lcom/osp/app/util/an;->a(I)I

    move-result v5

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getColor(I)I

    move-result v4

    invoke-virtual {v0, v4}, Landroid/widget/TextView;->setLinkTextColor(I)V

    const/4 v4, 0x0

    invoke-virtual {v0, v4, v3}, Landroid/widget/TextView;->setTypeface(Landroid/graphics/Typeface;I)V

    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v3, "AV"

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "MaketingPopup Description : "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v5, p0, Lcom/osp/app/signin/AccountView;->ak:Lcom/osp/app/signin/fv;

    invoke-virtual {v5}, Lcom/osp/app/signin/fv;->c()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;)V

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "<u>"

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v4, p0, Lcom/osp/app/signin/AccountView;->ak:Lcom/osp/app/signin/fv;

    invoke-virtual {v4}, Lcom/osp/app/signin/fv;->c()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "</u>"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Landroid/text/Html;->fromHtml(Ljava/lang/String;)Landroid/text/Spanned;

    move-result-object v3

    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    goto/16 :goto_1

    .line 2034
    :cond_a
    invoke-static {p0}, Lcom/osp/app/util/x;->m(Landroid/content/Context;)Z

    move-result v1

    if-nez v1, :cond_b

    const/high16 v1, 0x3f800000    # 1.0f

    cmpg-float v0, v0, v1

    if-ltz v0, :cond_b

    iget-boolean v0, p0, Lcom/osp/app/signin/AccountView;->ax:Z

    if-nez v0, :cond_e

    iget-boolean v0, p0, Lcom/osp/app/signin/AccountView;->ay:Z

    if-nez v0, :cond_e

    .line 2036
    :cond_b
    if-eqz v4, :cond_c

    .line 2038
    invoke-virtual {v4, v8}, Landroid/view/View;->setVisibility(I)V

    .line 2040
    :cond_c
    if-eqz v5, :cond_d

    .line 2042
    invoke-virtual {v5, v8}, Landroid/view/View;->setVisibility(I)V

    .line 2049
    :cond_d
    invoke-virtual {p0, v9}, Lcom/osp/app/signin/AccountView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v8}, Landroid/view/View;->setVisibility(I)V

    move v0, v3

    .line 2051
    goto/16 :goto_2

    .line 2064
    :cond_e
    iget-boolean v0, p0, Lcom/osp/app/signin/AccountView;->ay:Z

    if-ne v0, v2, :cond_f

    if-eqz v4, :cond_f

    .line 2066
    invoke-virtual {v4, v3}, Landroid/view/View;->setVisibility(I)V

    .line 2074
    const v0, 0x7f0c0048

    invoke-virtual {p0, v0}, Lcom/osp/app/signin/AccountView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    .line 2075
    if-eqz v0, :cond_f

    .line 2078
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v4, p0, Lcom/osp/app/signin/AccountView;->W:Ljava/lang/String;

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    check-cast v0, Landroid/widget/TextView;

    invoke-virtual {v0}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v0

    invoke-interface {v0}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "\n"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/osp/app/signin/AccountView;->W:Ljava/lang/String;

    .line 2082
    :cond_f
    iget-boolean v0, p0, Lcom/osp/app/signin/AccountView;->ax:Z

    if-ne v0, v2, :cond_11

    if-eqz v5, :cond_11

    .line 2084
    invoke-virtual {v5, v3}, Landroid/view/View;->setVisibility(I)V

    .line 2092
    const v0, 0x7f0c004e

    invoke-virtual {p0, v0}, Lcom/osp/app/signin/AccountView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    .line 2093
    if-eqz v0, :cond_10

    .line 2096
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v4, p0, Lcom/osp/app/signin/AccountView;->W:Ljava/lang/String;

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    check-cast v0, Landroid/widget/TextView;

    invoke-virtual {v0}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v0

    invoke-interface {v0}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "\n"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/osp/app/signin/AccountView;->W:Ljava/lang/String;

    .line 2099
    :cond_10
    invoke-static {p0}, Lcom/osp/app/util/x;->o(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_11

    .line 2101
    invoke-virtual {v5, v8}, Landroid/view/View;->setVisibility(I)V

    .line 2105
    :cond_11
    if-eqz v6, :cond_12

    .line 2107
    invoke-virtual {v6, v3}, Landroid/view/View;->setVisibility(I)V

    .line 2115
    const v0, 0x7f0c005a

    invoke-virtual {p0, v0}, Lcom/osp/app/signin/AccountView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    .line 2116
    if-eqz v0, :cond_12

    .line 2119
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v3, p0, Lcom/osp/app/signin/AccountView;->W:Ljava/lang/String;

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    check-cast v0, Landroid/widget/TextView;

    invoke-virtual {v0}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v0

    invoke-interface {v0}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "\n"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/osp/app/signin/AccountView;->W:Ljava/lang/String;

    .line 2128
    :cond_12
    invoke-virtual {p0, v9}, Lcom/osp/app/signin/AccountView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v8}, Landroid/view/View;->setVisibility(I)V

    move v0, v2

    .line 2130
    goto/16 :goto_2
.end method

.method static synthetic x(Lcom/osp/app/signin/AccountView;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 142
    iget-object v0, p0, Lcom/osp/app/signin/AccountView;->R:Ljava/lang/String;

    return-object v0
.end method

.method private x()V
    .locals 3

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 2236
    iget-object v0, p0, Lcom/osp/app/signin/AccountView;->ak:Lcom/osp/app/signin/fv;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/osp/app/signin/AccountView;->ak:Lcom/osp/app/signin/fv;

    invoke-virtual {v0}, Lcom/osp/app/signin/fv;->j()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 2238
    iget-object v0, p0, Lcom/osp/app/signin/AccountView;->ak:Lcom/osp/app/signin/fv;

    invoke-virtual {v0}, Lcom/osp/app/signin/fv;->c()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/osp/app/signin/AccountView;->ak:Lcom/osp/app/signin/fv;

    invoke-virtual {v0}, Lcom/osp/app/signin/fv;->c()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v0

    if-lez v0, :cond_1

    .line 2262
    :cond_0
    invoke-direct {p0, v1, v2}, Lcom/osp/app/signin/AccountView;->a(ZZ)V

    .line 2270
    :cond_1
    :goto_0
    return-void

    .line 2244
    :cond_2
    invoke-static {}, Lcom/osp/app/signin/SamsungService;->a()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-static {p0}, Lcom/osp/app/util/ad;->a(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_0

    :cond_3
    iget-boolean v0, p0, Lcom/osp/app/signin/AccountView;->aq:Z

    if-nez v0, :cond_0

    .line 2246
    invoke-static {}, Lcom/osp/app/signin/SamsungService;->a()Z

    move-result v0

    if-eqz v0, :cond_4

    invoke-static {}, Lcom/osp/app/util/r;->h()Z

    move-result v0

    if-nez v0, :cond_0

    .line 2248
    :cond_4
    invoke-static {}, Lcom/osp/app/util/r;->p()Z

    move-result v0

    if-eqz v0, :cond_5

    iget-object v0, p0, Lcom/osp/app/signin/AccountView;->aL:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-lez v0, :cond_5

    .line 2255
    invoke-direct {p0, v1, v2}, Lcom/osp/app/signin/AccountView;->a(ZZ)V

    .line 2257
    :cond_5
    new-instance v0, Lcom/osp/app/signin/bf;

    invoke-direct {v0, p0}, Lcom/osp/app/signin/bf;-><init>(Lcom/osp/app/signin/AccountView;)V

    .line 2258
    invoke-virtual {v0}, Lcom/osp/app/signin/bf;->b()V

    goto :goto_0
.end method

.method static synthetic y(Lcom/osp/app/signin/AccountView;)Lcom/msc/a/g;
    .locals 1

    .prologue
    .line 142
    iget-object v0, p0, Lcom/osp/app/signin/AccountView;->M:Lcom/msc/a/g;

    return-object v0
.end method

.method private y()V
    .locals 2

    .prologue
    .line 2599
    invoke-static {p0}, Lcom/osp/app/util/ad;->a(Landroid/content/Context;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 2630
    const/16 v0, 0xcb

    invoke-virtual {p0, v0}, Lcom/osp/app/signin/AccountView;->c(I)V

    .line 2675
    :goto_0
    return-void

    .line 2640
    :cond_0
    invoke-static {p0}, Lcom/osp/app/util/ad;->d(Landroid/content/Context;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 2642
    invoke-static {}, Lcom/osp/app/signin/SamsungService;->a()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 2644
    iget-object v0, p0, Lcom/osp/app/signin/AccountView;->ac:Ljava/lang/String;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/osp/app/signin/AccountView;->ac:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v0

    if-lez v0, :cond_2

    .line 2646
    invoke-direct {p0}, Lcom/osp/app/signin/AccountView;->D()V

    .line 2673
    :cond_1
    :goto_1
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/osp/app/signin/AccountView;->P:Z

    goto :goto_0

    .line 2649
    :cond_2
    invoke-direct {p0}, Lcom/osp/app/signin/AccountView;->I()Ljava/lang/String;

    goto :goto_1

    .line 2653
    :cond_3
    invoke-static {}, Lcom/osp/app/signin/SamsungService;->e()Z

    move-result v0

    if-eqz v0, :cond_4

    .line 2655
    const/16 v0, 0xd0

    invoke-direct {p0, v0}, Lcom/osp/app/signin/AccountView;->e(I)V

    .line 2656
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v0, "AV"

    const-string v1, "SelectCountryMode = true"

    invoke-static {v0, v1}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1

    .line 2658
    :cond_4
    iget-object v0, p0, Lcom/osp/app/signin/AccountView;->ac:Ljava/lang/String;

    if-eqz v0, :cond_7

    iget-object v0, p0, Lcom/osp/app/signin/AccountView;->ac:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v0

    if-lez v0, :cond_7

    .line 2660
    const-string v0, "450"

    iget-object v1, p0, Lcom/osp/app/signin/AccountView;->ac:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_5

    invoke-static {p0}, Lcom/osp/app/util/r;->s(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_6

    :cond_5
    iget-boolean v0, p0, Lcom/osp/app/signin/AccountView;->ad:Z

    if-nez v0, :cond_6

    .line 2662
    invoke-direct {p0}, Lcom/osp/app/signin/AccountView;->J()V

    goto :goto_1

    .line 2665
    :cond_6
    invoke-direct {p0}, Lcom/osp/app/signin/AccountView;->D()V

    goto :goto_1

    .line 2669
    :cond_7
    invoke-direct {p0}, Lcom/osp/app/signin/AccountView;->I()Ljava/lang/String;

    goto :goto_1
.end method

.method private z()V
    .locals 3

    .prologue
    .line 2688
    invoke-static {}, Lcom/osp/device/b;->b()Lcom/osp/device/b;

    move-result-object v0

    const-string v1, "true"

    invoke-virtual {v0, p0, v1}, Lcom/osp/device/b;->a(Landroid/content/Context;Ljava/lang/String;)V

    .line 2690
    invoke-static {p0}, Lcom/osp/app/util/ad;->a(Landroid/content/Context;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 2693
    const/16 v0, 0xcc

    invoke-virtual {p0, v0}, Lcom/osp/app/signin/AccountView;->c(I)V

    .line 2733
    :goto_0
    return-void

    .line 2699
    :cond_0
    :try_start_0
    invoke-static {}, Lcom/osp/common/util/i;->a()Lcom/osp/common/util/i;

    invoke-static {p0}, Lcom/osp/common/util/i;->e(Landroid/content/Context;)Z
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result v0

    if-eqz v0, :cond_1

    .line 2703
    :try_start_1
    invoke-static {}, Lcom/osp/common/util/i;->a()Lcom/osp/common/util/i;

    invoke-static {p0}, Lcom/osp/common/util/i;->h(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/osp/app/signin/AccountView;->ac:Ljava/lang/String;
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2709
    :try_start_2
    iget-object v0, p0, Lcom/osp/app/signin/AccountView;->ac:Ljava/lang/String;

    if-eqz v0, :cond_4

    iget-object v0, p0, Lcom/osp/app/signin/AccountView;->ac:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v0

    if-lez v0, :cond_4

    .line 2711
    iget-object v0, p0, Lcom/osp/app/signin/AccountView;->ac:Ljava/lang/String;

    invoke-virtual {p0}, Lcom/osp/app/signin/AccountView;->getApplicationContext()Landroid/content/Context;

    iget-object v1, p0, Lcom/osp/app/signin/AccountView;->ac:Ljava/lang/String;

    invoke-static {v1}, Lcom/osp/common/util/i;->d(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-static {p0, v0, v1}, Lcom/msc/c/e;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)V

    .line 2712
    iget-object v0, p0, Lcom/osp/app/signin/AccountView;->ac:Ljava/lang/String;

    invoke-static {p0, v0}, Lcom/osp/app/util/ad;->e(Landroid/content/Context;Ljava/lang/String;)V

    .line 2713
    const/4 v0, 0x0

    const/4 v1, 0x1

    invoke-direct {p0, v0, v1}, Lcom/osp/app/signin/AccountView;->b(Ljava/lang/String;Z)V
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_0

    goto :goto_0

    .line 2727
    :catch_0
    move-exception v0

    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_0

    .line 2704
    :catch_1
    move-exception v0

    :try_start_3
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 2709
    :try_start_4
    iget-object v0, p0, Lcom/osp/app/signin/AccountView;->ac:Ljava/lang/String;

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/osp/app/signin/AccountView;->ac:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v0

    if-lez v0, :cond_3

    .line 2711
    iget-object v0, p0, Lcom/osp/app/signin/AccountView;->ac:Ljava/lang/String;

    invoke-virtual {p0}, Lcom/osp/app/signin/AccountView;->getApplicationContext()Landroid/content/Context;

    iget-object v1, p0, Lcom/osp/app/signin/AccountView;->ac:Ljava/lang/String;

    invoke-static {v1}, Lcom/osp/common/util/i;->d(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-static {p0, v0, v1}, Lcom/msc/c/e;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)V

    .line 2712
    iget-object v0, p0, Lcom/osp/app/signin/AccountView;->ac:Ljava/lang/String;

    invoke-static {p0, v0}, Lcom/osp/app/util/ad;->e(Landroid/content/Context;Ljava/lang/String;)V

    .line 2713
    const/4 v0, 0x0

    const/4 v1, 0x1

    invoke-direct {p0, v0, v1}, Lcom/osp/app/signin/AccountView;->b(Ljava/lang/String;Z)V

    goto :goto_0

    .line 2709
    :catchall_0
    move-exception v0

    iget-object v1, p0, Lcom/osp/app/signin/AccountView;->ac:Ljava/lang/String;

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/osp/app/signin/AccountView;->ac:Ljava/lang/String;

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v1

    if-lez v1, :cond_2

    .line 2711
    iget-object v1, p0, Lcom/osp/app/signin/AccountView;->ac:Ljava/lang/String;

    invoke-virtual {p0}, Lcom/osp/app/signin/AccountView;->getApplicationContext()Landroid/content/Context;

    iget-object v2, p0, Lcom/osp/app/signin/AccountView;->ac:Ljava/lang/String;

    invoke-static {v2}, Lcom/osp/common/util/i;->d(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-static {p0, v1, v2}, Lcom/msc/c/e;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)V

    .line 2712
    iget-object v1, p0, Lcom/osp/app/signin/AccountView;->ac:Ljava/lang/String;

    invoke-static {p0, v1}, Lcom/osp/app/util/ad;->e(Landroid/content/Context;Ljava/lang/String;)V

    .line 2713
    const/4 v1, 0x0

    const/4 v2, 0x1

    invoke-direct {p0, v1, v2}, Lcom/osp/app/signin/AccountView;->b(Ljava/lang/String;Z)V

    .line 2709
    :goto_1
    throw v0

    .line 2724
    :cond_1
    new-instance v0, Lcom/osp/app/signin/be;

    const/4 v1, 0x1

    invoke-direct {v0, p0, v1}, Lcom/osp/app/signin/be;-><init>(Lcom/osp/app/signin/AccountView;I)V

    iput-object v0, p0, Lcom/osp/app/signin/AccountView;->E:Lcom/osp/app/signin/be;

    .line 2725
    iget-object v0, p0, Lcom/osp/app/signin/AccountView;->E:Lcom/osp/app/signin/be;

    invoke-virtual {v0}, Lcom/osp/app/signin/be;->b()V

    goto/16 :goto_0

    .line 2716
    :cond_2
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v1, "AV"

    const-string v2, "The sim state is ready but mcc is null!! This device is abnormal. We will make mcc from locale settings."

    invoke-static {v1, v2}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 2717
    new-instance v1, Lcom/osp/app/signin/be;

    const/4 v2, 0x1

    invoke-direct {v1, p0, v2}, Lcom/osp/app/signin/be;-><init>(Lcom/osp/app/signin/AccountView;I)V

    iput-object v1, p0, Lcom/osp/app/signin/AccountView;->E:Lcom/osp/app/signin/be;

    .line 2718
    iget-object v1, p0, Lcom/osp/app/signin/AccountView;->E:Lcom/osp/app/signin/be;

    invoke-virtual {v1}, Lcom/osp/app/signin/be;->b()V

    goto :goto_1

    .line 2716
    :cond_3
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v0, "AV"

    const-string v1, "The sim state is ready but mcc is null!! This device is abnormal. We will make mcc from locale settings."

    invoke-static {v0, v1}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 2717
    new-instance v0, Lcom/osp/app/signin/be;

    const/4 v1, 0x1

    invoke-direct {v0, p0, v1}, Lcom/osp/app/signin/be;-><init>(Lcom/osp/app/signin/AccountView;I)V

    iput-object v0, p0, Lcom/osp/app/signin/AccountView;->E:Lcom/osp/app/signin/be;

    .line 2718
    iget-object v0, p0, Lcom/osp/app/signin/AccountView;->E:Lcom/osp/app/signin/be;

    invoke-virtual {v0}, Lcom/osp/app/signin/be;->b()V

    goto/16 :goto_0

    .line 2716
    :cond_4
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v0, "AV"

    const-string v1, "The sim state is ready but mcc is null!! This device is abnormal. We will make mcc from locale settings."

    invoke-static {v0, v1}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 2717
    new-instance v0, Lcom/osp/app/signin/be;

    const/4 v1, 0x1

    invoke-direct {v0, p0, v1}, Lcom/osp/app/signin/be;-><init>(Lcom/osp/app/signin/AccountView;I)V

    iput-object v0, p0, Lcom/osp/app/signin/AccountView;->E:Lcom/osp/app/signin/be;

    .line 2718
    iget-object v0, p0, Lcom/osp/app/signin/AccountView;->E:Lcom/osp/app/signin/be;

    invoke-virtual {v0}, Lcom/osp/app/signin/be;->b()V
    :try_end_4
    .catch Ljava/lang/Exception; {:try_start_4 .. :try_end_4} :catch_0

    goto/16 :goto_0
.end method

.method static synthetic z(Lcom/osp/app/signin/AccountView;)V
    .locals 0

    .prologue
    .line 142
    invoke-direct {p0}, Lcom/osp/app/signin/AccountView;->A()V

    return-void
.end method


# virtual methods
.method protected final a()V
    .locals 0

    .prologue
    .line 6977
    return-void
.end method

.method protected final a(ZI)V
    .locals 6

    .prologue
    const/4 v5, 0x0

    const/4 v4, 0x1

    .line 9139
    iput-boolean v5, p0, Lcom/osp/app/signin/AccountView;->aq:Z

    .line 9141
    invoke-static {p0}, Landroid/preference/PreferenceManager;->getDefaultSharedPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v0

    .line 9142
    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    .line 9143
    if-eqz p1, :cond_2

    .line 9145
    const-string v1, "data_popup_hide"

    invoke-interface {v0, v1, v4}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    .line 9150
    :goto_0
    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 9152
    invoke-static {p0}, Landroid/preference/PreferenceManager;->getDefaultSharedPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v0

    .line 9153
    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    .line 9155
    const-string v1, "data_popup_ok_clicked"

    invoke-interface {v0, v1, v4}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    .line 9156
    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 9157
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v0, "AV"

    const-string v1, "Save Pref DataPop_OK CLICKED "

    invoke-static {v0, v1}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 9161
    invoke-static {}, Lcom/osp/app/bigdatalog/e;->a()Lcom/osp/app/bigdatalog/e;

    move-result-object v0

    invoke-virtual {p0}, Lcom/osp/app/signin/AccountView;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/osp/app/bigdatalog/e;->a(Landroid/content/Context;)V

    .line 9163
    invoke-static {}, Lcom/osp/app/bigdatalog/e;->a()Lcom/osp/app/bigdatalog/e;

    move-result-object v0

    invoke-virtual {v0}, Lcom/osp/app/bigdatalog/e;->c()Z

    move-result v0

    if-nez v0, :cond_0

    .line 9165
    invoke-static {}, Lcom/osp/app/bigdatalog/e;->a()Lcom/osp/app/bigdatalog/e;

    move-result-object v0

    invoke-virtual {p0}, Lcom/osp/app/signin/AccountView;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/osp/app/bigdatalog/e;->b(Landroid/content/Context;)V

    .line 9169
    :cond_0
    packed-switch p2, :pswitch_data_0

    .line 9270
    :cond_1
    :goto_1
    :pswitch_0
    const/4 v0, -0x1

    iput v0, p0, Lcom/osp/app/signin/AccountView;->d:I

    .line 9275
    iput-boolean v5, p0, Lcom/osp/app/signin/AccountView;->c:Z

    .line 9278
    :goto_2
    return-void

    .line 9148
    :cond_2
    const-string v1, "data_popup_hide"

    invoke-interface {v0, v1, v5}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    goto :goto_0

    .line 9172
    :pswitch_1
    invoke-static {p0}, Lcom/osp/app/util/ad;->a(Landroid/content/Context;)Z

    move-result v0

    if-nez v0, :cond_3

    .line 9174
    invoke-static {}, Lcom/osp/app/signin/SamsungService;->a()Z

    move-result v0

    if-nez v0, :cond_3

    .line 9176
    const/16 v0, 0xd5

    invoke-virtual {p0, v0}, Lcom/osp/app/signin/AccountView;->c(I)V

    goto :goto_2

    .line 9182
    :cond_3
    invoke-static {p0}, Lcom/osp/app/util/ad;->a(Landroid/content/Context;)Z

    move-result v0

    if-ne v0, v4, :cond_5

    invoke-static {}, Lcom/osp/app/signin/SamsungService;->a()Z

    move-result v0

    if-nez v0, :cond_5

    .line 9185
    invoke-direct {p0}, Lcom/osp/app/signin/AccountView;->v()Ljava/lang/String;

    move-result-object v0

    .line 9186
    invoke-static {}, Lcom/osp/device/b;->b()Lcom/osp/device/b;

    move-result-object v1

    invoke-virtual {v1}, Lcom/osp/device/b;->l()I

    move-result v1

    const/16 v2, 0x10

    if-le v1, v2, :cond_4

    invoke-static {}, Lcom/osp/app/signin/SamsungService;->a()Z

    move-result v1

    if-nez v1, :cond_4

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_4

    .line 9189
    invoke-direct {p0, v0}, Lcom/osp/app/signin/AccountView;->b(Ljava/lang/String;)V

    goto :goto_1

    .line 9194
    :cond_4
    :try_start_0
    invoke-direct {p0}, Lcom/osp/app/signin/AccountView;->x()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_1

    .line 9195
    :catch_0
    move-exception v0

    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_1

    .line 9201
    :cond_5
    invoke-static {}, Lcom/osp/app/signin/SamsungService;->a()Z

    move-result v0

    if-eqz v0, :cond_6

    invoke-static {p0}, Lcom/osp/app/util/ad;->a(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 9205
    :cond_6
    :try_start_1
    invoke-direct {p0}, Lcom/osp/app/signin/AccountView;->x()V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1

    goto :goto_1

    .line 9206
    :catch_1
    move-exception v0

    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_1

    .line 9218
    :pswitch_2
    invoke-virtual {p0}, Lcom/osp/app/signin/AccountView;->d()V

    goto :goto_1

    .line 9223
    :pswitch_3
    invoke-virtual {p0}, Lcom/osp/app/signin/AccountView;->e()V

    goto :goto_1

    .line 9228
    :pswitch_4
    invoke-direct {p0}, Lcom/osp/app/signin/AccountView;->y()V

    goto :goto_1

    .line 9232
    :pswitch_5
    invoke-static {p0}, Lcom/osp/app/util/ad;->p(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_8

    .line 9236
    :try_start_2
    invoke-virtual {p0}, Lcom/osp/app/signin/AccountView;->h()Z

    move-result v0

    invoke-static {p0, v0}, Lcom/msc/c/n;->a(Landroid/content/Context;Z)Ljava/lang/String;

    move-result-object v0

    .line 9238
    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    .line 9239
    new-instance v2, Landroid/content/Intent;

    const-string v3, "android.intent.action.VIEW"

    invoke-direct {v2, v3, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;Landroid/net/Uri;)V

    .line 9240
    const-string v1, "com.android.browser.application_id"

    invoke-virtual {p0}, Lcom/osp/app/signin/AccountView;->getPackageName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v1, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 9242
    invoke-static {}, Lcom/osp/app/signin/SamsungService;->a()Z

    move-result v1

    if-nez v1, :cond_7

    invoke-static {v2, p0}, Lcom/osp/app/util/ad;->a(Landroid/content/Intent;Landroid/content/Context;)Z

    move-result v1

    if-ne v1, v4, :cond_7

    .line 9244
    invoke-virtual {p0, v2}, Lcom/osp/app/signin/AccountView;->startActivity(Landroid/content/Intent;)V
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_2

    goto/16 :goto_1

    .line 9249
    :catch_2
    move-exception v0

    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    goto/16 :goto_1

    .line 9247
    :cond_7
    :try_start_3
    invoke-direct {p0, v0}, Lcom/osp/app/signin/AccountView;->c(Ljava/lang/String;)V
    :try_end_3
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_2

    goto/16 :goto_1

    .line 9256
    :cond_8
    new-instance v0, Lcom/osp/app/signin/be;

    const/4 v1, 0x2

    invoke-direct {v0, p0, v1}, Lcom/osp/app/signin/be;-><init>(Lcom/osp/app/signin/AccountView;I)V

    iput-object v0, p0, Lcom/osp/app/signin/AccountView;->E:Lcom/osp/app/signin/be;

    .line 9257
    iget-object v0, p0, Lcom/osp/app/signin/AccountView;->E:Lcom/osp/app/signin/be;

    invoke-virtual {v0}, Lcom/osp/app/signin/be;->b()V

    goto/16 :goto_1

    .line 9262
    :pswitch_6
    invoke-static {p0}, Lcom/osp/app/util/ad;->a(Landroid/content/Context;)Z

    move-result v0

    if-ne v0, v4, :cond_9

    invoke-static {}, Lcom/osp/app/signin/SamsungService;->a()Z

    move-result v0

    if-nez v0, :cond_9

    .line 9264
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v0, "Self Update check"

    invoke-static {v0}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;)V

    .line 9266
    invoke-static {}, Lcom/msc/sa/selfupdate/c;->b()Lcom/msc/sa/selfupdate/c;

    move-result-object v0

    invoke-virtual {p0}, Lcom/osp/app/signin/AccountView;->getApplication()Landroid/app/Application;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v4, v5, v2}, Lcom/msc/sa/selfupdate/c;->a(Landroid/app/Application;ZZLjava/lang/String;)V

    .line 9268
    :cond_9
    invoke-virtual {p0}, Lcom/osp/app/signin/AccountView;->d()V

    goto/16 :goto_1

    .line 9169
    nop

    :pswitch_data_0
    .packed-switch 0x12e
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_0
        :pswitch_0
        :pswitch_6
    .end packed-switch
.end method

.method protected final b()V
    .locals 11

    .prologue
    const/16 v10, 0xbf

    const/4 v9, -0x1

    const/4 v8, 0x0

    const/high16 v7, 0x3f800000    # 1.0f

    const/4 v6, 0x0

    .line 6981
    invoke-static {}, Lcom/osp/app/signin/SamsungService;->a()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 6983
    invoke-static {}, Lcom/osp/device/b;->b()Lcom/osp/device/b;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/osp/device/b;->i(Landroid/content/Context;)Z

    move-result v0

    if-nez v0, :cond_2

    invoke-static {p0}, Lcom/osp/app/util/x;->o(Landroid/content/Context;)Z

    move-result v0

    if-nez v0, :cond_2

    .line 6985
    invoke-virtual {p0}, Lcom/osp/app/signin/AccountView;->h()Z

    move-result v2

    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0xe

    if-lt v0, v1, :cond_2

    invoke-virtual {p0}, Landroid/app/Activity;->getActionBar()Landroid/app/ActionBar;

    move-result-object v3

    if-eqz v3, :cond_2

    if-eqz v2, :cond_3

    invoke-virtual {p0}, Landroid/app/Activity;->getLayoutInflater()Landroid/view/LayoutInflater;

    move-result-object v0

    const v1, 0x7f03000e

    invoke-virtual {v0, v1, v8}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    move-object v1, v0

    :goto_0
    if-eqz v1, :cond_1

    invoke-static {}, Lcom/osp/app/signin/SamsungService;->a()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-static {p0}, Lcom/osp/app/util/r;->m(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_1

    const v0, 0x7f0c0083

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    if-eqz v0, :cond_1

    invoke-virtual {v3, v8}, Landroid/app/ActionBar;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    const v4, 0x106000d

    invoke-virtual {v0, v4}, Landroid/view/View;->setBackgroundResource(I)V

    const v0, 0x7f0c0084

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    invoke-virtual {p0}, Landroid/app/Activity;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const v5, 0x7f0800f5

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v4

    invoke-virtual {v0, v6, v4}, Landroid/widget/TextView;->setTextSize(IF)V

    invoke-virtual {v0, v6}, Landroid/widget/TextView;->setIncludeFontPadding(Z)V

    if-eqz v2, :cond_4

    invoke-virtual {p0}, Landroid/app/Activity;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v4, 0x7f07001a

    invoke-virtual {v2, v4}, Landroid/content/res/Resources;->getColor(I)I

    move-result v2

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setTextColor(I)V

    :goto_1
    invoke-static {p0}, Lcom/osp/app/util/x;->m(Landroid/content/Context;)Z

    move-result v2

    if-eqz v2, :cond_5

    invoke-static {v10, v6, v6, v6}, Landroid/graphics/Color;->argb(IIII)I

    move-result v2

    invoke-virtual {v0, v7, v7, v7, v2}, Landroid/widget/TextView;->setShadowLayer(FFFI)V

    :goto_2
    invoke-static {p0}, Lcom/osp/app/util/x;->l(Landroid/content/Context;)Z

    move-result v2

    if-nez v2, :cond_0

    invoke-static {p0}, Lcom/osp/app/util/x;->a(Landroid/content/Context;)Z

    move-result v2

    if-eqz v2, :cond_1

    :cond_0
    invoke-virtual {v0, v8, v6}, Landroid/widget/TextView;->setTypeface(Landroid/graphics/Typeface;I)V

    :cond_1
    new-instance v0, Landroid/app/ActionBar$LayoutParams;

    invoke-direct {v0, v9, v9}, Landroid/app/ActionBar$LayoutParams;-><init>(II)V

    invoke-virtual {v3, v1, v0}, Landroid/app/ActionBar;->setCustomView(Landroid/view/View;Landroid/app/ActionBar$LayoutParams;)V

    invoke-virtual {v3, v6}, Landroid/app/ActionBar;->setHomeButtonEnabled(Z)V

    invoke-virtual {v3, v6}, Landroid/app/ActionBar;->setDisplayShowHomeEnabled(Z)V

    invoke-virtual {v3, v6}, Landroid/app/ActionBar;->setDisplayShowTitleEnabled(Z)V

    invoke-virtual {v3, v6}, Landroid/app/ActionBar;->setDisplayShowCustomEnabled(Z)V

    const/16 v0, 0x10

    invoke-virtual {v3, v0}, Landroid/app/ActionBar;->setDisplayOptions(I)V

    .line 6988
    :cond_2
    return-void

    .line 6985
    :cond_3
    invoke-virtual {p0}, Landroid/app/Activity;->getLayoutInflater()Landroid/view/LayoutInflater;

    move-result-object v0

    const v1, 0x7f03000f

    invoke-virtual {v0, v1, v8}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    move-object v1, v0

    goto/16 :goto_0

    :cond_4
    invoke-virtual {p0}, Landroid/app/Activity;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v4, 0x7f07001b

    invoke-virtual {v2, v4}, Landroid/content/res/Resources;->getColor(I)I

    move-result v2

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setTextColor(I)V

    goto :goto_1

    :cond_5
    const/4 v2, 0x0

    const/high16 v4, 0x40000000    # 2.0f

    invoke-static {v10, v6, v6, v6}, Landroid/graphics/Color;->argb(IIII)I

    move-result v5

    invoke-virtual {v0, v7, v2, v4, v5}, Landroid/widget/TextView;->setShadowLayer(FFFI)V

    goto :goto_2
.end method

.method protected final c()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 9111
    invoke-static {}, Lcom/osp/app/signin/SamsungService;->a()Z

    move-result v0

    if-nez v0, :cond_1

    .line 9113
    invoke-virtual {p0, v1}, Lcom/osp/app/signin/AccountView;->b(I)V

    .line 9116
    :try_start_0
    invoke-static {p0}, Lcom/osp/app/util/ad;->b(Landroid/content/Context;)Z

    move-result v0

    if-nez v0, :cond_0

    invoke-static {p0}, Lcom/osp/common/util/i;->a(Landroid/content/Context;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 9118
    invoke-static {}, Lcom/osp/app/util/ap;->a()Lcom/osp/app/util/ap;

    const v0, 0x7f090128

    invoke-virtual {p0, v0}, Lcom/osp/app/signin/AccountView;->getString(I)Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x0

    invoke-static {p0, v0, v1}, Lcom/osp/app/util/ap;->a(Landroid/content/Context;Ljava/lang/String;I)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 9124
    :cond_0
    :goto_0
    invoke-virtual {p0}, Lcom/osp/app/signin/AccountView;->finish()V

    .line 9127
    :cond_1
    return-void

    .line 9120
    :catch_0
    move-exception v0

    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_0
.end method

.method public final d()V
    .locals 7

    .prologue
    const/4 v6, 0x0

    const/4 v5, 0x1

    .line 9844
    new-instance v0, Lcom/osp/app/bigdatalog/c;

    invoke-virtual {p0}, Lcom/osp/app/signin/AccountView;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    const/4 v2, 0x0

    const/16 v3, 0x8

    iget-object v4, p0, Lcom/osp/app/signin/AccountView;->J:Landroid/content/Intent;

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/osp/app/bigdatalog/c;-><init>(Landroid/content/Context;IILandroid/content/Intent;)V

    .line 9845
    invoke-static {}, Lcom/osp/app/bigdatalog/e;->a()Lcom/osp/app/bigdatalog/e;

    move-result-object v1

    invoke-virtual {p0}, Lcom/osp/app/signin/AccountView;->getApplicationContext()Landroid/content/Context;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/osp/app/bigdatalog/e;->a(Landroid/content/Context;)V

    .line 9846
    invoke-static {}, Lcom/osp/app/bigdatalog/e;->a()Lcom/osp/app/bigdatalog/e;

    move-result-object v1

    invoke-virtual {p0}, Lcom/osp/app/signin/AccountView;->getApplicationContext()Landroid/content/Context;

    move-result-object v2

    invoke-virtual {v1, v2, v0}, Lcom/osp/app/bigdatalog/e;->a(Landroid/content/Context;Lcom/osp/app/bigdatalog/c;)V

    .line 9849
    invoke-static {}, Lcom/osp/device/b;->b()Lcom/osp/device/b;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/osp/device/b;->p(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 9851
    iget-object v0, p0, Lcom/osp/app/signin/AccountView;->z:Lcom/osp/app/signin/ax;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/osp/app/signin/AccountView;->z:Lcom/osp/app/signin/ax;

    invoke-virtual {v0}, Lcom/osp/app/signin/ax;->getStatus()Landroid/os/AsyncTask$Status;

    move-result-object v0

    sget-object v1, Landroid/os/AsyncTask$Status;->RUNNING:Landroid/os/AsyncTask$Status;

    if-ne v0, v1, :cond_0

    .line 9853
    iget-object v0, p0, Lcom/osp/app/signin/AccountView;->z:Lcom/osp/app/signin/ax;

    invoke-virtual {v0}, Lcom/osp/app/signin/ax;->d()V

    .line 9854
    iput-object v6, p0, Lcom/osp/app/signin/AccountView;->z:Lcom/osp/app/signin/ax;

    .line 9857
    :cond_0
    new-instance v0, Lcom/osp/app/signin/ax;

    invoke-direct {v0, p0, v5}, Lcom/osp/app/signin/ax;-><init>(Lcom/osp/app/signin/AccountView;I)V

    iput-object v0, p0, Lcom/osp/app/signin/AccountView;->z:Lcom/osp/app/signin/ax;

    .line 9858
    iget-object v0, p0, Lcom/osp/app/signin/AccountView;->z:Lcom/osp/app/signin/ax;

    invoke-virtual {v0}, Lcom/osp/app/signin/ax;->b()V

    .line 9903
    :goto_0
    return-void

    .line 9865
    :cond_1
    iget-object v0, p0, Lcom/osp/app/signin/AccountView;->as:Lcom/osp/app/signin/bc;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/osp/app/signin/AccountView;->as:Lcom/osp/app/signin/bc;

    invoke-virtual {v0}, Lcom/osp/app/signin/bc;->getStatus()Landroid/os/AsyncTask$Status;

    move-result-object v0

    sget-object v1, Landroid/os/AsyncTask$Status;->RUNNING:Landroid/os/AsyncTask$Status;

    if-ne v0, v1, :cond_2

    .line 9867
    iget-object v0, p0, Lcom/osp/app/signin/AccountView;->as:Lcom/osp/app/signin/bc;

    invoke-virtual {v0}, Lcom/osp/app/signin/bc;->d()V

    .line 9868
    iput-object v6, p0, Lcom/osp/app/signin/AccountView;->as:Lcom/osp/app/signin/bc;

    .line 9871
    :cond_2
    invoke-static {p0}, Lcom/osp/app/util/ad;->a(Landroid/content/Context;)Z

    move-result v0

    if-nez v0, :cond_3

    .line 9873
    const/16 v0, 0xcc

    invoke-virtual {p0, v0}, Lcom/osp/app/signin/AccountView;->c(I)V

    goto :goto_0

    .line 9876
    :cond_3
    invoke-static {}, Lcom/osp/app/util/r;->t()Z

    move-result v0

    if-eqz v0, :cond_4

    .line 9878
    new-instance v0, Lcom/osp/app/signin/bc;

    invoke-direct {v0, p0, v5}, Lcom/osp/app/signin/bc;-><init>(Lcom/osp/app/signin/AccountView;I)V

    iput-object v0, p0, Lcom/osp/app/signin/AccountView;->as:Lcom/osp/app/signin/bc;

    .line 9879
    iget-object v0, p0, Lcom/osp/app/signin/AccountView;->as:Lcom/osp/app/signin/bc;

    invoke-virtual {v0}, Lcom/osp/app/signin/bc;->b()V

    goto :goto_0

    .line 9882
    :cond_4
    invoke-static {p0}, Lcom/osp/app/util/r;->p(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_5

    .line 9884
    invoke-direct {p0, v5}, Lcom/osp/app/signin/AccountView;->g(I)V

    goto :goto_0

    .line 9887
    :cond_5
    invoke-direct {p0}, Lcom/osp/app/signin/AccountView;->z()V

    goto :goto_0
.end method

.method public final e()V
    .locals 6

    .prologue
    const/4 v5, 0x0

    const/4 v4, 0x0

    .line 9909
    new-instance v0, Lcom/osp/app/bigdatalog/c;

    invoke-virtual {p0}, Lcom/osp/app/signin/AccountView;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    const/4 v2, 0x7

    iget-object v3, p0, Lcom/osp/app/signin/AccountView;->J:Landroid/content/Intent;

    invoke-direct {v0, v1, v4, v2, v3}, Lcom/osp/app/bigdatalog/c;-><init>(Landroid/content/Context;IILandroid/content/Intent;)V

    .line 9910
    invoke-static {}, Lcom/osp/app/bigdatalog/e;->a()Lcom/osp/app/bigdatalog/e;

    move-result-object v1

    invoke-virtual {p0}, Lcom/osp/app/signin/AccountView;->getApplicationContext()Landroid/content/Context;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/osp/app/bigdatalog/e;->a(Landroid/content/Context;)V

    .line 9911
    invoke-static {}, Lcom/osp/app/bigdatalog/e;->a()Lcom/osp/app/bigdatalog/e;

    move-result-object v1

    invoke-virtual {p0}, Lcom/osp/app/signin/AccountView;->getApplicationContext()Landroid/content/Context;

    move-result-object v2

    invoke-virtual {v1, v2, v0}, Lcom/osp/app/bigdatalog/e;->a(Landroid/content/Context;Lcom/osp/app/bigdatalog/c;)V

    .line 9914
    invoke-static {}, Lcom/osp/device/b;->b()Lcom/osp/device/b;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/osp/device/b;->p(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 9916
    iget-object v0, p0, Lcom/osp/app/signin/AccountView;->z:Lcom/osp/app/signin/ax;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/osp/app/signin/AccountView;->z:Lcom/osp/app/signin/ax;

    invoke-virtual {v0}, Lcom/osp/app/signin/ax;->getStatus()Landroid/os/AsyncTask$Status;

    move-result-object v0

    sget-object v1, Landroid/os/AsyncTask$Status;->RUNNING:Landroid/os/AsyncTask$Status;

    if-ne v0, v1, :cond_0

    .line 9918
    iget-object v0, p0, Lcom/osp/app/signin/AccountView;->z:Lcom/osp/app/signin/ax;

    invoke-virtual {v0}, Lcom/osp/app/signin/ax;->d()V

    .line 9919
    iput-object v5, p0, Lcom/osp/app/signin/AccountView;->z:Lcom/osp/app/signin/ax;

    .line 9922
    :cond_0
    new-instance v0, Lcom/osp/app/signin/ax;

    invoke-direct {v0, p0, v4}, Lcom/osp/app/signin/ax;-><init>(Lcom/osp/app/signin/AccountView;I)V

    iput-object v0, p0, Lcom/osp/app/signin/AccountView;->z:Lcom/osp/app/signin/ax;

    .line 9923
    iget-object v0, p0, Lcom/osp/app/signin/AccountView;->z:Lcom/osp/app/signin/ax;

    invoke-virtual {v0}, Lcom/osp/app/signin/ax;->b()V

    .line 9955
    :goto_0
    return-void

    .line 9930
    :cond_1
    iget-object v0, p0, Lcom/osp/app/signin/AccountView;->as:Lcom/osp/app/signin/bc;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/osp/app/signin/AccountView;->as:Lcom/osp/app/signin/bc;

    invoke-virtual {v0}, Lcom/osp/app/signin/bc;->getStatus()Landroid/os/AsyncTask$Status;

    move-result-object v0

    sget-object v1, Landroid/os/AsyncTask$Status;->RUNNING:Landroid/os/AsyncTask$Status;

    if-ne v0, v1, :cond_2

    .line 9932
    iget-object v0, p0, Lcom/osp/app/signin/AccountView;->as:Lcom/osp/app/signin/bc;

    invoke-virtual {v0}, Lcom/osp/app/signin/bc;->d()V

    .line 9933
    iput-object v5, p0, Lcom/osp/app/signin/AccountView;->as:Lcom/osp/app/signin/bc;

    .line 9935
    :cond_2
    invoke-static {p0}, Lcom/osp/app/util/ad;->a(Landroid/content/Context;)Z

    move-result v0

    if-nez v0, :cond_3

    .line 9937
    const/16 v0, 0xcb

    invoke-virtual {p0, v0}, Lcom/osp/app/signin/AccountView;->c(I)V

    goto :goto_0

    .line 9940
    :cond_3
    new-instance v0, Lcom/osp/app/signin/bc;

    invoke-direct {v0, p0, v4}, Lcom/osp/app/signin/bc;-><init>(Lcom/osp/app/signin/AccountView;I)V

    iput-object v0, p0, Lcom/osp/app/signin/AccountView;->as:Lcom/osp/app/signin/bc;

    .line 9941
    iget-object v0, p0, Lcom/osp/app/signin/AccountView;->as:Lcom/osp/app/signin/bc;

    invoke-virtual {v0}, Lcom/osp/app/signin/bc;->b()V

    goto :goto_0
.end method

.method public final f()V
    .locals 3

    .prologue
    .line 10542
    iget-boolean v0, p0, Lcom/osp/app/signin/AccountView;->aq:Z

    if-nez v0, :cond_0

    .line 10544
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/osp/app/loggingservice/LoggingService;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 10545
    const-string v1, "logging_service_mode"

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 10547
    invoke-virtual {p0, v0}, Lcom/osp/app/signin/AccountView;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;

    .line 10552
    :goto_0
    return-void

    .line 10550
    :cond_0
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v0, "AV"

    const-string v1, "Logging Service not invoked ultimately"

    invoke-static {v0, v1}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method protected onActivityResult(IILandroid/content/Intent;)V
    .locals 12

    .prologue
    const/16 v9, 0xcf

    const/16 v8, 0xce

    const/4 v1, 0x0

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 3265
    invoke-super {p0, p1, p2, p3}, Lcom/osp/app/util/BaseActivity;->onActivityResult(IILandroid/content/Intent;)V

    .line 3267
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v4, "AV"

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v5, "onActivityResult rq_c : "

    invoke-direct {v0, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v5, " & rs_c : "

    invoke-virtual {v0, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v5, " & d_e : "

    invoke-virtual {v0, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_1

    const-string v0, "t"

    :goto_0
    invoke-static {v4, v0}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 3293
    invoke-direct {p0}, Lcom/osp/app/signin/AccountView;->r()Z

    move-result v0

    if-eqz v0, :cond_3b

    .line 3295
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v0, "AV"

    const-string v4, "OAR signInPopup"

    invoke-static {v0, v4}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 3298
    iget-boolean v0, p0, Lcom/osp/app/signin/AccountView;->A:Z

    if-ne v0, v3, :cond_2

    .line 3300
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v0, "AV"

    const-string v4, "RESULT_SIGN_UP_BUTTON_FROM_SIGN_IN_BUTTON from sign up with FB/Weibo"

    invoke-static {v0, v4}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 3301
    iput-boolean v2, p0, Lcom/osp/app/signin/AccountView;->A:Z

    .line 3302
    iget-object v0, p0, Lcom/osp/app/signin/AccountView;->J:Landroid/content/Intent;

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 3303
    const/16 v0, 0xe

    if-eq p2, v0, :cond_0

    if-eqz p2, :cond_0

    const/16 v0, 0x17

    if-ne p2, v0, :cond_3b

    .line 3941
    :cond_0
    :goto_1
    return-void

    .line 3267
    :cond_1
    const-string v0, "f"

    goto :goto_0

    .line 3311
    :cond_2
    const/16 v0, 0xe

    if-ne p2, v0, :cond_3

    .line 3313
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v0, "AV"

    const-string v1, "RESULT_CANCELED_ONLY_ONE_ACTIVITY from sign in popup"

    invoke-static {v0, v1}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 3314
    invoke-virtual {p0}, Lcom/osp/app/signin/AccountView;->getApplication()Landroid/app/Application;

    move-result-object v0

    check-cast v0, Lcom/osp/app/signin/SamsungService;

    invoke-virtual {v0, p0}, Lcom/osp/app/signin/SamsungService;->a(Landroid/app/Activity;)V

    .line 3315
    iget-object v0, p0, Lcom/osp/app/signin/AccountView;->J:Landroid/content/Intent;

    invoke-virtual {p0, v2, v0}, Lcom/osp/app/signin/AccountView;->a(ILandroid/content/Intent;)V

    .line 3316
    invoke-virtual {p0}, Lcom/osp/app/signin/AccountView;->finish()V

    goto :goto_1

    .line 3319
    :cond_3
    if-nez p2, :cond_4

    .line 3321
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v0, "AV"

    const-string v1, "RESULT_CANCELED from sign in popup"

    invoke-static {v0, v1}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 3322
    iget-object v0, p0, Lcom/osp/app/signin/AccountView;->J:Landroid/content/Intent;

    invoke-virtual {p0, v2, v0}, Lcom/osp/app/signin/AccountView;->a(ILandroid/content/Intent;)V

    .line 3323
    invoke-virtual {p0}, Lcom/osp/app/signin/AccountView;->finish()V

    goto :goto_1

    .line 3326
    :cond_4
    const/16 v0, 0x17

    if-ne p2, v0, :cond_5

    .line 3328
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v0, "AV"

    const-string v1, "RESULT_SIGN_UP_BUTTON_FROM_SIGN_IN_BUTTON from sign in popup"

    invoke-static {v0, v1}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 3331
    const/4 v0, 0x0

    :try_start_0
    iput v0, p0, Lcom/osp/app/signin/AccountView;->V:I

    .line 3332
    const/4 v0, 0x0

    const/4 v1, 0x1

    invoke-direct {p0, v0, v1}, Lcom/osp/app/signin/AccountView;->a(ZZ)V

    .line 3333
    iget-object v0, p0, Lcom/osp/app/signin/AccountView;->J:Landroid/content/Intent;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_1

    .line 3334
    :catch_0
    move-exception v0

    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_1

    .line 3340
    :cond_5
    if-eqz p3, :cond_3b

    .line 3342
    const-string v0, "TO_SIGNIN_POPUP_FOR_SIGNUP"

    invoke-virtual {p3, v0, v2}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v0

    if-ne v0, v3, :cond_3b

    .line 3345
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v0, "AV"

    const-string v4, "onActivityResult - bResultToSignInPopupForSignUp: true"

    invoke-static {v0, v4}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;)V

    move v0, v3

    .line 3352
    :goto_2
    const/16 v4, 0xf

    if-ne p2, v4, :cond_6

    .line 3354
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v0, "AV"

    const-string v1, "FINISH_BY_HOME_BUTTON"

    invoke-static {v0, v1}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 3355
    invoke-virtual {p0}, Lcom/osp/app/signin/AccountView;->finish()V

    goto/16 :goto_1

    .line 3359
    :cond_6
    const/16 v4, 0x15

    if-ne p2, v4, :cond_7

    .line 3361
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v0, "AV"

    const-string v1, "RESULT_FAIL_MDM_SECURITY"

    invoke-static {v0, v1}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 3362
    invoke-virtual {p0}, Lcom/osp/app/signin/AccountView;->finish()V

    goto/16 :goto_1

    .line 3366
    :cond_7
    new-instance v5, Landroid/content/Intent;

    const-string v4, "com.msc.action.samsungaccount.response.BackGroundSignin"

    invoke-direct {v5, v4}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 3368
    const/16 v4, 0xc9

    if-ne p1, v4, :cond_a

    .line 3370
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v0, "AV"

    const-string v1, "FINISH_BY_EMAIL_VALIDATION_VIEW"

    invoke-static {v0, v1}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 3371
    const/16 v0, 0xc

    if-ne p2, v0, :cond_9

    .line 3373
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v0, "AV"

    const-string v1, "FINISH_BY_ACTIVATING_EMAIL_VALIDATION"

    invoke-static {v0, v1}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 3374
    const-string v0, "AGREE_TO_DISCLAIMER"

    iget-object v1, p0, Lcom/osp/app/signin/AccountView;->C:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_8

    .line 3376
    invoke-direct {p0}, Lcom/osp/app/signin/AccountView;->A()V

    goto/16 :goto_1

    .line 3379
    :cond_8
    invoke-direct {p0}, Lcom/osp/app/signin/AccountView;->s()V

    goto/16 :goto_1

    .line 3383
    :cond_9
    iget-object v0, p0, Lcom/osp/app/signin/AccountView;->J:Landroid/content/Intent;

    invoke-direct {p0, v3, v0, v5}, Lcom/osp/app/signin/AccountView;->a(ILandroid/content/Intent;Landroid/content/Intent;)V

    .line 3384
    invoke-virtual {p0}, Lcom/osp/app/signin/AccountView;->finish()V

    goto/16 :goto_1

    .line 3389
    :cond_a
    const/16 v4, 0xd8

    if-eq p1, v4, :cond_b

    if-eq p1, v8, :cond_b

    if-eqz v0, :cond_e

    .line 3391
    :cond_b
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v4, "AV"

    const-string v6, "FINISH_BY_SIGN_UP_OR_TNC_OR_SIGN_IN_POPUP"

    invoke-static {v4, v6}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 3393
    if-ne p2, v3, :cond_c

    .line 3395
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v0, "AV"

    const-string v1, "FINISH_BY_SIGN_UP_OR_TNC_OR_SIGN_IN_POPUP_RESULT_FAIL"

    invoke-static {v0, v1}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 3396
    invoke-static {}, Lcom/osp/app/util/ap;->a()Lcom/osp/app/util/ap;

    const v0, 0x7f09004a

    invoke-virtual {p0, v0}, Lcom/osp/app/signin/AccountView;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-static {p0, v0, v2}, Lcom/osp/app/util/ap;->a(Landroid/content/Context;Ljava/lang/String;I)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    goto/16 :goto_1

    .line 3400
    :cond_c
    if-eqz p3, :cond_e

    .line 3402
    const-string v4, "key_is_name_verified"

    invoke-virtual {p3, v4, v2}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v4

    iput-boolean v4, p0, Lcom/osp/app/signin/AccountView;->ad:Z

    .line 3403
    iget-boolean v4, p0, Lcom/osp/app/signin/AccountView;->ad:Z

    if-eqz v4, :cond_d

    .line 3405
    const-string v4, "information_after_name_validation"

    invoke-virtual {p3, v4}, Landroid/content/Intent;->getBundleExtra(Ljava/lang/String;)Landroid/os/Bundle;

    move-result-object v4

    iput-object v4, p0, Lcom/osp/app/signin/AccountView;->ae:Landroid/os/Bundle;

    .line 3408
    :cond_d
    const-string v4, "authcode"

    invoke-virtual {p3, v4}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 3409
    iget-object v6, p0, Lcom/osp/app/signin/AccountView;->J:Landroid/content/Intent;

    const-string v7, "authcode"

    invoke-virtual {v6, v7, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 3413
    :cond_e
    const/16 v4, 0xd0

    if-eq p1, v4, :cond_f

    const/16 v4, 0xdf

    if-ne p1, v4, :cond_10

    .line 3415
    :cond_f
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v4, "AV"

    const-string v6, "FINISH_BY_SCL_OR_SIGN_IN"

    invoke-static {v4, v6}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 3416
    const/4 v4, -0x1

    if-eq p2, v4, :cond_10

    .line 3418
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v0, "AV"

    const-string v1, "FINISH_BY_SIGN_UP_OR_TNC_OR_SIGN_IN_POPUP_RESULT_NOT_OK"

    invoke-static {v0, v1}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 3419
    invoke-virtual {p0}, Lcom/osp/app/signin/AccountView;->getApplication()Landroid/app/Application;

    move-result-object v0

    check-cast v0, Lcom/osp/app/signin/SamsungService;

    invoke-virtual {v0, p0}, Lcom/osp/app/signin/SamsungService;->a(Landroid/app/Activity;)V

    goto/16 :goto_1

    .line 3429
    :cond_10
    packed-switch p2, :pswitch_data_0

    .line 3935
    :cond_11
    :goto_3
    :pswitch_0
    invoke-static {}, Lcom/osp/app/signin/SamsungService;->a()Z

    move-result v0

    if-eqz v0, :cond_12

    const/4 v0, 0x3

    if-eq p2, v0, :cond_0

    .line 3937
    :cond_12
    iget-object v0, p0, Lcom/osp/app/signin/AccountView;->J:Landroid/content/Intent;

    invoke-direct {p0, p2, v0, v5}, Lcom/osp/app/signin/AccountView;->a(ILandroid/content/Intent;Landroid/content/Intent;)V

    .line 3938
    invoke-virtual {p0}, Lcom/osp/app/signin/AccountView;->finish()V

    goto/16 :goto_1

    .line 3433
    :pswitch_1
    if-eqz p3, :cond_14

    .line 3435
    const-string v4, "signup_in_setupwizard"

    invoke-virtual {p3, v4, v2}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v4

    if-eqz v4, :cond_13

    .line 3437
    iget-object v4, p0, Lcom/osp/app/signin/AccountView;->J:Landroid/content/Intent;

    const-string v6, "signup_in_setupwizard"

    invoke-virtual {v4, v6, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 3439
    :cond_13
    const-string v4, "signin_without_email_verification"

    invoke-virtual {p3, v4, v2}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v4

    if-eqz v4, :cond_14

    .line 3441
    iget-object v4, p0, Lcom/osp/app/signin/AccountView;->J:Landroid/content/Intent;

    const-string v6, "signin_without_email_verification"

    invoke-virtual {v4, v6, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 3445
    :cond_14
    const/16 v4, 0xcd

    if-ne p1, v4, :cond_15

    if-eqz v0, :cond_16

    :cond_15
    if-eq p1, v9, :cond_16

    const/16 v0, 0xd2

    if-eq p1, v0, :cond_16

    if-ne p1, v8, :cond_1f

    .line 3454
    :cond_16
    iput-boolean v2, p0, Lcom/osp/app/signin/AccountView;->B:Z

    .line 3466
    const-string v0, "AGREE_TO_DISCLAIMER"

    iget-object v4, p0, Lcom/osp/app/signin/AccountView;->C:Ljava/lang/String;

    invoke-virtual {v0, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_18

    .line 3468
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v0, "AV"

    const-string v1, "FINISH_BY_RESULT_OK_DISCLAIMER_AGREE"

    invoke-static {v0, v1}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 3469
    const-string v0, "is_agree_to_disclaimer"

    invoke-virtual {p3, v0, v2}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v0

    if-eqz v0, :cond_17

    .line 3471
    iget-object v0, p0, Lcom/osp/app/signin/AccountView;->J:Landroid/content/Intent;

    const-string v1, "is_agree_to_disclaimer"

    invoke-virtual {v0, v1, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 3472
    iget-object v0, p0, Lcom/osp/app/signin/AccountView;->J:Landroid/content/Intent;

    invoke-direct {p0, p2, v0, v5}, Lcom/osp/app/signin/AccountView;->a(ILandroid/content/Intent;Landroid/content/Intent;)V

    .line 3473
    invoke-virtual {p0}, Lcom/osp/app/signin/AccountView;->finish()V

    goto/16 :goto_1

    .line 3476
    :cond_17
    iput-boolean v3, p0, Lcom/osp/app/signin/AccountView;->U:Z

    .line 3477
    invoke-direct {p0}, Lcom/osp/app/signin/AccountView;->A()V

    goto/16 :goto_1

    .line 3481
    :cond_18
    if-eqz p3, :cond_1b

    .line 3483
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v0, "AV"

    const-string v3, "FINISH_BY_RESULT_OK_DATA_EXIST"

    invoke-static {v0, v3}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 3485
    const-string v0, "authcode"

    invoke-virtual {p3, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 3487
    const-string v3, "wait_more_info"

    invoke-virtual {p3, v3, v2}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v3

    .line 3488
    iget-object v4, p0, Lcom/osp/app/signin/AccountView;->J:Landroid/content/Intent;

    const-string v6, "wait_more_info"

    invoke-virtual {v4, v6, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 3490
    iget-boolean v4, p0, Lcom/osp/app/signin/AccountView;->K:Z

    if-eqz v4, :cond_19

    .line 3492
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v4, "AV"

    const-string v6, "FINISH_BY_RESULT_OK_DATA_EXIST_APPS"

    invoke-static {v4, v6}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 3493
    const-string v4, "signUpInfo"

    invoke-virtual {p3, v4}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 3494
    const-string v6, "BG_mode"

    iget-object v7, p0, Lcom/osp/app/signin/AccountView;->T:Ljava/lang/String;

    invoke-virtual {v6, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_1c

    .line 3496
    const-string v6, "signUpInfo"

    invoke-virtual {v5, v6, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 3503
    :cond_19
    :goto_4
    iget-object v4, p0, Lcom/osp/app/signin/AccountView;->L:Ljava/lang/String;

    const-string v6, "OSP_02"

    invoke-virtual {v4, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_1a

    .line 3505
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v4, "AV"

    const-string v6, "FINISH_BY_RESULT_OK_DATA_EXIST_V2"

    invoke-static {v4, v6}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 3506
    const-string v4, "BG_mode"

    iget-object v6, p0, Lcom/osp/app/signin/AccountView;->T:Ljava/lang/String;

    invoke-virtual {v4, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_1d

    .line 3508
    const-string v4, "wait_more_info"

    invoke-virtual {v5, v4, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 3509
    const-string v3, "authcode"

    invoke-virtual {v5, v3, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 3510
    const-string v0, "ServerUrl"

    invoke-static {p0}, Lcom/msc/c/c;->f(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v5, v0, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 3518
    :cond_1a
    :goto_5
    const-string v0, "netflix"

    iget-object v3, p0, Lcom/osp/app/signin/AccountView;->X:Ljava/lang/String;

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1b

    .line 3520
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v0, "AV"

    const-string v3, "FINISH_BY_RESULT_OK_DATA_EXIST_NETFLIX"

    invoke-static {v0, v3}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 3521
    if-ne p1, v9, :cond_1b

    .line 3523
    const-string v0, "issuetime"

    const-wide/16 v6, -0x1

    invoke-virtual {p3, v0, v6, v7}, Landroid/content/Intent;->getLongExtra(Ljava/lang/String;J)J

    move-result-wide v6

    .line 3524
    const-string v0, "confirmed"

    invoke-virtual {p3, v0, v2}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v0

    .line 3525
    const-string v2, "expiretime"

    const-wide/16 v8, -0x1

    invoke-virtual {p3, v2, v8, v9}, Landroid/content/Intent;->getLongExtra(Ljava/lang/String;J)J

    move-result-wide v2

    .line 3526
    const-string v4, "userid"

    invoke-virtual {p3, v4}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 3527
    const-string v8, "signerid"

    invoke-virtual {p3, v8}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v8

    .line 3528
    const-string v9, "signature"

    invoke-virtual {p3, v9}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v9

    .line 3530
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    new-instance v10, Ljava/lang/StringBuilder;

    const-string v11, "issueTime : "

    invoke-direct {v10, v11}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v10, v6, v7}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-static {v10}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;)V

    .line 3531
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    new-instance v10, Ljava/lang/StringBuilder;

    const-string v11, "confirmed : "

    invoke-direct {v10, v11}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v10, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-static {v10}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;)V

    .line 3532
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    new-instance v10, Ljava/lang/StringBuilder;

    const-string v11, "expireTime : "

    invoke-direct {v10, v11}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v10, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-static {v10}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;)V

    .line 3533
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    new-instance v10, Ljava/lang/StringBuilder;

    const-string v11, "userId : "

    invoke-direct {v10, v11}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v10, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-static {v10}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;)V

    .line 3534
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    new-instance v10, Ljava/lang/StringBuilder;

    const-string v11, "signerId : "

    invoke-direct {v10, v11}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v10, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-static {v10}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;)V

    .line 3535
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    new-instance v10, Ljava/lang/StringBuilder;

    const-string v11, "signature : "

    invoke-direct {v10, v11}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v10, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-static {v10}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;)V

    .line 3537
    iget-object v10, p0, Lcom/osp/app/signin/AccountView;->J:Landroid/content/Intent;

    const-string v11, "issuetime"

    invoke-virtual {v10, v11, v6, v7}, Landroid/content/Intent;->putExtra(Ljava/lang/String;J)Landroid/content/Intent;

    .line 3538
    iget-object v6, p0, Lcom/osp/app/signin/AccountView;->J:Landroid/content/Intent;

    const-string v7, "confirmed"

    invoke-virtual {v6, v7, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 3539
    iget-object v0, p0, Lcom/osp/app/signin/AccountView;->J:Landroid/content/Intent;

    const-string v6, "expiretime"

    invoke-virtual {v0, v6, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;J)Landroid/content/Intent;

    .line 3540
    iget-object v0, p0, Lcom/osp/app/signin/AccountView;->J:Landroid/content/Intent;

    const-string v2, "userid"

    invoke-virtual {v0, v2, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 3541
    iget-object v0, p0, Lcom/osp/app/signin/AccountView;->J:Landroid/content/Intent;

    const-string v2, "signerid"

    invoke-virtual {v0, v2, v8}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 3542
    iget-object v0, p0, Lcom/osp/app/signin/AccountView;->J:Landroid/content/Intent;

    const-string v2, "signature"

    invoke-virtual {v0, v2, v9}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 3547
    :cond_1b
    iget-boolean v0, p0, Lcom/osp/app/signin/AccountView;->K:Z

    if-eqz v0, :cond_11

    .line 3549
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v0, "AV"

    const-string v2, "FINISH_BY_RESULT_OK_APPS"

    invoke-static {v0, v2}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 3551
    invoke-static {p0}, Lcom/osp/app/util/ad;->h(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v6

    .line 3553
    invoke-static {p0}, Lcom/msc/c/e;->c(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v7

    .line 3554
    invoke-static {p0}, Lcom/msc/c/c;->d(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v8

    .line 3561
    :try_start_1
    invoke-static {}, Lcom/msc/c/a;->a()Lcom/msc/c/a;

    move-result-object v0

    invoke-static {}, Lcom/msc/c/a;->a()Lcom/msc/c/a;

    move-result-object v2

    invoke-virtual {v2}, Lcom/msc/c/a;->b()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2, v6}, Lcom/msc/c/a;->b(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1

    move-result-object v2

    .line 3562
    :try_start_2
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v0, "AV"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-static {}, Lcom/osp/app/util/v;->a()Lcom/osp/app/util/v;

    move-result-object v4

    const-string v9, "EmailID"

    invoke-virtual {v4, v9}, Lcom/osp/app/util/v;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " encrypted."

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v0, v3}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_8

    move-object v4, v2

    .line 3575
    :goto_6
    :try_start_3
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v0, "AV"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-static {}, Lcom/osp/app/util/v;->a()Lcom/osp/app/util/v;

    move-result-object v3

    const-string v9, "Password"

    invoke-virtual {v3, v9}, Lcom/osp/app/util/v;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " encrypted."

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_3
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_2

    .line 3585
    :goto_7
    :try_start_4
    invoke-static {}, Lcom/msc/c/a;->a()Lcom/msc/c/a;

    move-result-object v0

    invoke-static {}, Lcom/msc/c/a;->a()Lcom/msc/c/a;

    move-result-object v2

    invoke-virtual {v2}, Lcom/msc/c/a;->b()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2, v7}, Lcom/msc/c/a;->b(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    :try_end_4
    .catch Ljava/lang/Exception; {:try_start_4 .. :try_end_4} :catch_3

    move-result-object v2

    .line 3586
    :try_start_5
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v0, "AV"

    const-string v3, "external_b encrypted."

    invoke-static {v0, v3}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_5
    .catch Ljava/lang/Exception; {:try_start_5 .. :try_end_5} :catch_7

    move-object v3, v2

    .line 3594
    :goto_8
    :try_start_6
    invoke-static {}, Lcom/msc/c/a;->a()Lcom/msc/c/a;

    move-result-object v0

    invoke-static {}, Lcom/msc/c/a;->a()Lcom/msc/c/a;

    move-result-object v2

    invoke-virtual {v2}, Lcom/msc/c/a;->b()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2, v8}, Lcom/msc/c/a;->b(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    :try_end_6
    .catch Ljava/lang/Exception; {:try_start_6 .. :try_end_6} :catch_4

    move-result-object v0

    .line 3595
    :try_start_7
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v2, "AV"

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    invoke-static {}, Lcom/osp/app/util/v;->a()Lcom/osp/app/util/v;

    move-result-object v10

    const-string v11, "key_user_id"

    invoke-virtual {v10, v11}, Lcom/osp/app/util/v;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v10, " encrypted."

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v2, v9}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_7
    .catch Ljava/lang/Exception; {:try_start_7 .. :try_end_7} :catch_6

    .line 3604
    :goto_9
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v9, "AccountView::onActivityResult id : "

    invoke-direct {v2, v9}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;)V

    .line 3607
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v6, "AccountView::onActivityResult birthDate : "

    invoke-direct {v2, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;)V

    .line 3608
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v6, "AccountView::onActivityResult userid : "

    invoke-direct {v2, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;)V

    .line 3609
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v6, "AccountView::onActivityResult encID : "

    invoke-direct {v2, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;)V

    .line 3610
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v6, "AccountView::onActivityResult encPassword : "

    invoke-direct {v2, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;)V

    .line 3611
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "AccountView::onActivityResult encbirthDate : "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;)V

    .line 3612
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "AccountView::onActivityResult encUserid : "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;)V

    .line 3614
    const-string v1, "BG_mode"

    iget-object v2, p0, Lcom/osp/app/signin/AccountView;->T:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1e

    .line 3616
    const-string v1, "apps_id"

    invoke-virtual {v5, v1, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 3621
    const-string v1, "apps_birthdate"

    invoke-virtual {v5, v1, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 3622
    const-string v1, "apps_userid"

    invoke-virtual {v5, v1, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    goto/16 :goto_3

    .line 3499
    :cond_1c
    iget-object v6, p0, Lcom/osp/app/signin/AccountView;->J:Landroid/content/Intent;

    const-string v7, "signUpInfo"

    invoke-virtual {v6, v7, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    goto/16 :goto_4

    .line 3513
    :cond_1d
    iget-object v3, p0, Lcom/osp/app/signin/AccountView;->J:Landroid/content/Intent;

    const-string v4, "authcode"

    invoke-virtual {v3, v4, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 3514
    iget-object v0, p0, Lcom/osp/app/signin/AccountView;->J:Landroid/content/Intent;

    const-string v3, "ServerUrl"

    invoke-static {p0}, Lcom/msc/c/c;->f(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v0, v3, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    goto/16 :goto_5

    .line 3564
    :catch_1
    move-exception v0

    move-object v2, v1

    :goto_a
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    .line 3567
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v0, "AV"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-static {}, Lcom/osp/app/util/v;->a()Lcom/osp/app/util/v;

    move-result-object v4

    const-string v9, "EmailID"

    invoke-virtual {v4, v9}, Lcom/osp/app/util/v;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " encrypt failed."

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v0, v3}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;)V

    move-object v4, v2

    goto/16 :goto_6

    .line 3577
    :catch_2
    move-exception v0

    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    .line 3580
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v0, "AV"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-static {}, Lcom/osp/app/util/v;->a()Lcom/osp/app/util/v;

    move-result-object v3

    const-string v9, "Password"

    invoke-virtual {v3, v9}, Lcom/osp/app/util/v;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " encrypt failed."

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_7

    .line 3587
    :catch_3
    move-exception v0

    move-object v2, v1

    :goto_b
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    .line 3590
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v0, "AV"

    const-string v3, "external_b encrypt failed."

    invoke-static {v0, v3}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;)V

    move-object v3, v2

    goto/16 :goto_8

    .line 3597
    :catch_4
    move-exception v0

    move-object v2, v0

    move-object v0, v1

    :goto_c
    invoke-virtual {v2}, Ljava/lang/Exception;->printStackTrace()V

    .line 3600
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v2, "AV"

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    invoke-static {}, Lcom/osp/app/util/v;->a()Lcom/osp/app/util/v;

    move-result-object v10

    const-string v11, "key_user_id"

    invoke-virtual {v10, v11}, Lcom/osp/app/util/v;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v10, " encrypt failed."

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v2, v9}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_9

    .line 3625
    :cond_1e
    iget-object v1, p0, Lcom/osp/app/signin/AccountView;->J:Landroid/content/Intent;

    const-string v2, "apps_id"

    invoke-virtual {v1, v2, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 3630
    iget-object v1, p0, Lcom/osp/app/signin/AccountView;->J:Landroid/content/Intent;

    const-string v2, "apps_birthdate"

    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 3631
    iget-object v1, p0, Lcom/osp/app/signin/AccountView;->J:Landroid/content/Intent;

    const-string v2, "apps_userid"

    invoke-virtual {v1, v2, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    goto/16 :goto_3

    .line 3634
    :cond_1f
    const/16 v0, 0xd1

    if-ne p1, v0, :cond_20

    .line 3636
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v0, "AV"

    const-string v1, "FINISH_BY_RESULT_OK_SIGN_IN"

    invoke-static {v0, v1}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 3637
    iget-object v0, p0, Lcom/osp/app/signin/AccountView;->M:Lcom/msc/a/g;

    invoke-virtual {v0, v2}, Lcom/msc/a/g;->a(Z)V

    .line 3638
    iget-object v0, p0, Lcom/osp/app/signin/AccountView;->M:Lcom/msc/a/g;

    invoke-virtual {v0, v2}, Lcom/msc/a/g;->b(Z)V

    .line 3639
    iget-object v0, p0, Lcom/osp/app/signin/AccountView;->M:Lcom/msc/a/g;

    invoke-direct {p0, v0}, Lcom/osp/app/signin/AccountView;->a(Lcom/msc/a/g;)V

    goto/16 :goto_1

    .line 3641
    :cond_20
    const/16 v0, 0xd3

    if-ne p1, v0, :cond_21

    .line 3643
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v0, "AV"

    const-string v1, "FINISH_BY_RESULT_OK_DISCLAIMER"

    invoke-static {v0, v1}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 3644
    iget-object v0, p0, Lcom/osp/app/signin/AccountView;->J:Landroid/content/Intent;

    const-string v1, "is_agree_to_disclaimer"

    invoke-virtual {v0, v1, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    goto/16 :goto_3

    .line 3650
    :cond_21
    const/16 v0, 0xe9

    if-ne p1, v0, :cond_22

    .line 3653
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v0, "AV"

    const-string v1, "FINISH_BY_RESULT_OK_SMS_VERIFY"

    invoke-static {v0, v1}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 3654
    const-string v0, "did"

    invoke-virtual {p3, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 3655
    const-string v1, "imsi"

    invoke-virtual {p3, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 3657
    iget-object v2, p0, Lcom/osp/app/signin/AccountView;->J:Landroid/content/Intent;

    const-string v3, "did"

    invoke-virtual {v2, v3, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 3658
    iget-object v0, p0, Lcom/osp/app/signin/AccountView;->J:Landroid/content/Intent;

    const-string v2, "imsi"

    invoke-virtual {v0, v2, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    goto/16 :goto_3

    .line 3660
    :cond_22
    const/16 v0, 0xcb

    if-ne p1, v0, :cond_24

    .line 3662
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v0, "AV"

    const-string v3, "FINISH_BY_RESULT_OK_WIFI_SIGN_UP"

    invoke-static {v0, v3}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 3665
    iget-object v0, p0, Lcom/osp/app/signin/AccountView;->as:Lcom/osp/app/signin/bc;

    if-eqz v0, :cond_23

    iget-object v0, p0, Lcom/osp/app/signin/AccountView;->as:Lcom/osp/app/signin/bc;

    invoke-virtual {v0}, Lcom/osp/app/signin/bc;->getStatus()Landroid/os/AsyncTask$Status;

    move-result-object v0

    sget-object v3, Landroid/os/AsyncTask$Status;->RUNNING:Landroid/os/AsyncTask$Status;

    if-ne v0, v3, :cond_23

    .line 3667
    iget-object v0, p0, Lcom/osp/app/signin/AccountView;->as:Lcom/osp/app/signin/bc;

    invoke-virtual {v0}, Lcom/osp/app/signin/bc;->d()V

    .line 3668
    iput-object v1, p0, Lcom/osp/app/signin/AccountView;->as:Lcom/osp/app/signin/bc;

    .line 3670
    :cond_23
    new-instance v0, Lcom/osp/app/signin/bc;

    invoke-direct {v0, p0, v2}, Lcom/osp/app/signin/bc;-><init>(Lcom/osp/app/signin/AccountView;I)V

    iput-object v0, p0, Lcom/osp/app/signin/AccountView;->as:Lcom/osp/app/signin/bc;

    .line 3671
    iget-object v0, p0, Lcom/osp/app/signin/AccountView;->as:Lcom/osp/app/signin/bc;

    invoke-virtual {v0}, Lcom/osp/app/signin/bc;->b()V

    goto/16 :goto_1

    .line 3700
    :cond_24
    const/16 v0, 0xcc

    if-ne p1, v0, :cond_28

    .line 3702
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v0, "AV"

    const-string v2, "FINISH_BY_RESULT_OK_WIFI_SIGN_IN"

    invoke-static {v0, v2}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 3705
    iget-object v0, p0, Lcom/osp/app/signin/AccountView;->as:Lcom/osp/app/signin/bc;

    if-eqz v0, :cond_25

    iget-object v0, p0, Lcom/osp/app/signin/AccountView;->as:Lcom/osp/app/signin/bc;

    invoke-virtual {v0}, Lcom/osp/app/signin/bc;->getStatus()Landroid/os/AsyncTask$Status;

    move-result-object v0

    sget-object v2, Landroid/os/AsyncTask$Status;->RUNNING:Landroid/os/AsyncTask$Status;

    if-ne v0, v2, :cond_25

    .line 3707
    iget-object v0, p0, Lcom/osp/app/signin/AccountView;->as:Lcom/osp/app/signin/bc;

    invoke-virtual {v0}, Lcom/osp/app/signin/bc;->d()V

    .line 3708
    iput-object v1, p0, Lcom/osp/app/signin/AccountView;->as:Lcom/osp/app/signin/bc;

    .line 3711
    :cond_25
    invoke-static {}, Lcom/osp/app/util/r;->t()Z

    move-result v0

    if-eqz v0, :cond_26

    .line 3713
    new-instance v0, Lcom/osp/app/signin/bc;

    invoke-direct {v0, p0, v3}, Lcom/osp/app/signin/bc;-><init>(Lcom/osp/app/signin/AccountView;I)V

    iput-object v0, p0, Lcom/osp/app/signin/AccountView;->as:Lcom/osp/app/signin/bc;

    .line 3714
    iget-object v0, p0, Lcom/osp/app/signin/AccountView;->as:Lcom/osp/app/signin/bc;

    invoke-virtual {v0}, Lcom/osp/app/signin/bc;->b()V

    goto/16 :goto_1

    .line 3717
    :cond_26
    invoke-static {p0}, Lcom/osp/app/util/r;->p(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_27

    .line 3719
    invoke-direct {p0, v3}, Lcom/osp/app/signin/AccountView;->g(I)V

    goto/16 :goto_1

    .line 3722
    :cond_27
    invoke-direct {p0}, Lcom/osp/app/signin/AccountView;->z()V

    goto/16 :goto_1

    .line 3740
    :cond_28
    const/16 v0, 0xd5

    if-eq p1, v0, :cond_29

    const/16 v0, 0xe0

    if-ne p1, v0, :cond_2c

    .line 3742
    :cond_29
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v0, "AV"

    const-string v1, "FINISH_BY_RESULT_OK_WIFI_SETTING_OR_FORCE"

    invoke-static {v0, v1}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 3743
    invoke-virtual {p0}, Lcom/osp/app/signin/AccountView;->getCallingActivity()Landroid/content/ComponentName;

    move-result-object v0

    .line 3745
    invoke-direct {p0}, Lcom/osp/app/signin/AccountView;->v()Ljava/lang/String;

    move-result-object v1

    .line 3746
    invoke-static {}, Lcom/osp/device/b;->b()Lcom/osp/device/b;

    move-result-object v2

    invoke-virtual {v2}, Lcom/osp/device/b;->l()I

    move-result v2

    const/16 v3, 0x10

    if-le v2, v3, :cond_2a

    invoke-static {}, Lcom/osp/app/signin/SamsungService;->a()Z

    move-result v2

    if-nez v2, :cond_2a

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_2a

    .line 3749
    invoke-direct {p0, v1}, Lcom/osp/app/signin/AccountView;->b(Ljava/lang/String;)V

    goto/16 :goto_1

    .line 3750
    :cond_2a
    invoke-static {}, Lcom/osp/device/b;->b()Lcom/osp/device/b;

    move-result-object v1

    invoke-virtual {v1, p0}, Lcom/osp/device/b;->i(Landroid/content/Context;)Z

    move-result v1

    if-nez v1, :cond_2b

    invoke-virtual {p0, v0}, Lcom/osp/app/signin/AccountView;->a(Landroid/content/ComponentName;)Z

    move-result v0

    if-eqz v0, :cond_2b

    .line 3752
    invoke-direct {p0}, Lcom/osp/app/signin/AccountView;->u()V

    goto/16 :goto_1

    .line 3760
    :cond_2b
    :try_start_8
    invoke-direct {p0}, Lcom/osp/app/signin/AccountView;->x()V
    :try_end_8
    .catch Ljava/lang/Exception; {:try_start_8 .. :try_end_8} :catch_5

    goto/16 :goto_1

    .line 3765
    :catch_5
    move-exception v0

    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    goto/16 :goto_1

    .line 3771
    :cond_2c
    const/16 v0, 0xca

    if-ne p1, v0, :cond_2d

    .line 3773
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v0, "AV"

    const-string v1, "FINISH_BY_RESULT_OK_NAME_VALID"

    invoke-static {v0, v1}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 3774
    iput-boolean v3, p0, Lcom/osp/app/signin/AccountView;->ad:Z

    const-string v0, "key_name_check_familyname"

    invoke-virtual {p3, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    const-string v1, "key_name_check_givenname"

    invoke-virtual {p3, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    const-string v2, "key_name_check_birthdate"

    invoke-virtual {p3, v2}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    const-string v3, "key_name_check_mobile"

    invoke-virtual {p3, v3}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    const-string v4, "key_name_check_method"

    invoke-virtual {p3, v4}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    const-string v5, "key_name_check_ci"

    invoke-virtual {p3, v5}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    const-string v6, "key_name_check_di"

    invoke-virtual {p3, v6}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    const-string v7, "key_name_check_gender"

    invoke-virtual {p3, v7}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    const-string v8, "key_name_check_foreigner"

    invoke-virtual {p3, v8}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v8

    new-instance v9, Landroid/os/Bundle;

    invoke-direct {v9}, Landroid/os/Bundle;-><init>()V

    iput-object v9, p0, Lcom/osp/app/signin/AccountView;->ae:Landroid/os/Bundle;

    iget-object v9, p0, Lcom/osp/app/signin/AccountView;->ae:Landroid/os/Bundle;

    const-string v10, "key_name_check_familyname"

    invoke-virtual {v9, v10, v0}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/osp/app/signin/AccountView;->ae:Landroid/os/Bundle;

    const-string v9, "key_name_check_givenname"

    invoke-virtual {v0, v9, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/osp/app/signin/AccountView;->ae:Landroid/os/Bundle;

    const-string v1, "key_name_check_birthdate"

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/osp/app/signin/AccountView;->ae:Landroid/os/Bundle;

    const-string v1, "key_name_check_mobile"

    invoke-virtual {v0, v1, v3}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/osp/app/signin/AccountView;->ae:Landroid/os/Bundle;

    const-string v1, "key_name_check_method"

    invoke-virtual {v0, v1, v4}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/osp/app/signin/AccountView;->ae:Landroid/os/Bundle;

    const-string v1, "key_name_check_ci"

    invoke-virtual {v0, v1, v5}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/osp/app/signin/AccountView;->ae:Landroid/os/Bundle;

    const-string v1, "key_name_check_di"

    invoke-virtual {v0, v1, v6}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/osp/app/signin/AccountView;->ae:Landroid/os/Bundle;

    const-string v1, "key_name_check_gender"

    invoke-virtual {v0, v1, v7}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/osp/app/signin/AccountView;->ae:Landroid/os/Bundle;

    const-string v1, "key_name_check_foreigner"

    invoke-virtual {v0, v1, v8}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 3775
    invoke-direct {p0}, Lcom/osp/app/signin/AccountView;->D()V

    goto/16 :goto_1

    .line 3777
    :cond_2d
    const/16 v0, 0xd0

    if-ne p1, v0, :cond_30

    .line 3779
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v0, "AV"

    const-string v1, "FINISH_BY_RESULT_OK_SCL"

    invoke-static {v0, v1}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 3780
    invoke-static {p0}, Lcom/msc/c/e;->f(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/osp/app/signin/AccountView;->ac:Ljava/lang/String;

    .line 3781
    iget-object v0, p0, Lcom/osp/app/signin/AccountView;->ac:Ljava/lang/String;

    invoke-static {p0, v0}, Lcom/osp/app/util/ad;->e(Landroid/content/Context;Ljava/lang/String;)V

    .line 3782
    invoke-static {}, Lcom/osp/app/signin/SamsungService;->a()Z

    move-result v0

    if-nez v0, :cond_2f

    .line 3784
    const-string v0, "450"

    iget-object v1, p0, Lcom/osp/app/signin/AccountView;->ac:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_2e

    invoke-static {p0}, Lcom/osp/app/util/r;->s(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_2f

    :cond_2e
    iget-boolean v0, p0, Lcom/osp/app/signin/AccountView;->ad:Z

    if-nez v0, :cond_2f

    .line 3789
    invoke-direct {p0}, Lcom/osp/app/signin/AccountView;->J()V

    goto/16 :goto_1

    .line 3792
    :cond_2f
    invoke-direct {p0}, Lcom/osp/app/signin/AccountView;->D()V

    goto/16 :goto_1

    .line 3796
    :cond_30
    const/16 v0, 0xdf

    if-ne p1, v0, :cond_11

    .line 3798
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v0, "AV"

    const-string v2, "FINISH_BY_RESULT_OK_SCL_SIGN_IN"

    invoke-static {v0, v2}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 3799
    invoke-static {p0}, Lcom/msc/c/e;->f(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/osp/app/signin/AccountView;->ac:Ljava/lang/String;

    .line 3800
    iget-object v0, p0, Lcom/osp/app/signin/AccountView;->ac:Ljava/lang/String;

    invoke-static {p0, v0}, Lcom/osp/app/util/ad;->e(Landroid/content/Context;Ljava/lang/String;)V

    .line 3801
    invoke-direct {p0, v1, v3}, Lcom/osp/app/signin/AccountView;->b(Ljava/lang/String;Z)V

    goto/16 :goto_1

    .line 3807
    :pswitch_2
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v0, "AV"

    const-string v1, "FINISH_BY_RESULT_OK_SIGNUP_EMAIL_VALIDATED"

    invoke-static {v0, v1}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 3808
    if-eqz p3, :cond_31

    const-string v0, "email_id"

    invoke-virtual {p3, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    :goto_d
    invoke-direct {p0, v0, v2}, Lcom/osp/app/signin/AccountView;->a(Ljava/lang/String;Z)V

    goto/16 :goto_1

    :cond_31
    const-string v0, ""

    goto :goto_d

    .line 3812
    :pswitch_3
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v0, "AV"

    const-string v1, "FINISH_BY_RESULT_CANCELED"

    invoke-static {v0, v1}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 3813
    const/16 v0, 0xd3

    if-ne p1, v0, :cond_32

    .line 3815
    iget-object v0, p0, Lcom/osp/app/signin/AccountView;->J:Landroid/content/Intent;

    const-string v1, "is_agree_to_disclaimer"

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    goto/16 :goto_3

    .line 3816
    :cond_32
    const/16 v0, 0xe0

    if-eq p1, v0, :cond_0

    .line 3819
    const/16 v0, 0xcb

    if-eq p1, v0, :cond_0

    .line 3822
    const/16 v0, 0xcc

    if-eq p1, v0, :cond_0

    .line 3830
    const/16 v0, 0xd4

    if-ne p1, v0, :cond_33

    .line 3832
    invoke-virtual {p0}, Lcom/osp/app/signin/AccountView;->getApplication()Landroid/app/Application;

    move-result-object v0

    check-cast v0, Lcom/osp/app/signin/SamsungService;

    invoke-virtual {v0, p0}, Lcom/osp/app/signin/SamsungService;->a(Landroid/app/Activity;)V

    goto/16 :goto_1

    .line 3834
    :cond_33
    const/16 v0, 0xd6

    if-ne p1, v0, :cond_11

    goto/16 :goto_1

    .line 3841
    :pswitch_4
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v0, "AV"

    const-string v1, "FINISH_BY_RESULT_FAILED"

    invoke-static {v0, v1}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 3842
    iget-object v0, p0, Lcom/osp/app/signin/AccountView;->C:Ljava/lang/String;

    if-eqz v0, :cond_11

    .line 3844
    iget-object v0, p0, Lcom/osp/app/signin/AccountView;->C:Ljava/lang/String;

    const-string v1, "ACCOUNT_VERIFY"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_35

    .line 3846
    if-eqz p3, :cond_11

    .line 3848
    const-string v0, "passwordBlock"

    invoke-virtual {p3, v0, v3}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v0

    .line 3849
    const-string v1, "BG_mode"

    iget-object v2, p0, Lcom/osp/app/signin/AccountView;->T:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_34

    .line 3851
    const-string v1, "passwordBlock"

    invoke-virtual {v5, v1, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    goto/16 :goto_3

    .line 3854
    :cond_34
    iget-object v1, p0, Lcom/osp/app/signin/AccountView;->J:Landroid/content/Intent;

    const-string v2, "passwordBlock"

    invoke-virtual {v1, v2, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    goto/16 :goto_3

    .line 3857
    :cond_35
    iget-object v0, p0, Lcom/osp/app/signin/AccountView;->C:Ljava/lang/String;

    const-string v1, "AGREE_TO_DISCLAIMER"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_11

    .line 3859
    iget-object v0, p0, Lcom/osp/app/signin/AccountView;->J:Landroid/content/Intent;

    const-string v1, "is_agree_to_disclaimer"

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    goto/16 :goto_3

    .line 3877
    :pswitch_5
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v0, "AV"

    const-string v1, "FINISH_BY_RESULT_CANCELED_ONLY_ONE_ACTIVITY"

    invoke-static {v0, v1}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 3881
    invoke-virtual {p0}, Lcom/osp/app/signin/AccountView;->getApplication()Landroid/app/Application;

    move-result-object v0

    check-cast v0, Lcom/osp/app/signin/SamsungService;

    invoke-virtual {v0, p0}, Lcom/osp/app/signin/SamsungService;->a(Landroid/app/Activity;)V

    .line 3882
    const/16 v0, 0xca

    if-ne p1, v0, :cond_0

    .line 3884
    iput-boolean v2, p0, Lcom/osp/app/signin/AccountView;->aw:Z

    goto/16 :goto_1

    .line 3889
    :pswitch_6
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v1, "AV"

    const-string v2, "FINISH_BY_RESULT_FAIL_ACTIVATING_EMAIL_VALIDATION"

    invoke-static {v1, v2}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 3890
    const/16 v1, 0xcd

    if-ne p1, v1, :cond_36

    if-eqz v0, :cond_37

    :cond_36
    if-eq p1, v9, :cond_37

    const/16 v1, 0xd2

    if-eq p1, v1, :cond_37

    if-eq p1, v8, :cond_37

    const/16 v1, 0xd8

    if-eq p1, v1, :cond_37

    if-eqz v0, :cond_0

    .line 3905
    :cond_37
    iget-boolean v0, p0, Lcom/osp/app/signin/AccountView;->e:Z

    if-eqz v0, :cond_38

    .line 3907
    const/4 v0, -0x1

    invoke-virtual {p0, v0}, Lcom/osp/app/signin/AccountView;->b(I)V

    .line 3912
    :goto_e
    invoke-virtual {p0}, Lcom/osp/app/signin/AccountView;->finish()V

    goto/16 :goto_1

    .line 3910
    :cond_38
    iget-object v0, p0, Lcom/osp/app/signin/AccountView;->J:Landroid/content/Intent;

    invoke-direct {p0, v3, v0, v5}, Lcom/osp/app/signin/AccountView;->a(ILandroid/content/Intent;Landroid/content/Intent;)V

    goto :goto_e

    .line 3917
    :pswitch_7
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v0, "AV"

    const-string v2, "FINISH_BY_RESULT_EXIST_FACEBOOK_EMAILID"

    invoke-static {v0, v2}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 3918
    iput-boolean v3, p0, Lcom/osp/app/signin/AccountView;->A:Z

    .line 3919
    if-eqz p3, :cond_39

    const-string v0, "email_id"

    invoke-virtual {p3, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    :goto_f
    iput-object v0, p0, Lcom/osp/app/signin/AccountView;->O:Ljava/lang/String;

    goto/16 :goto_1

    :cond_39
    move-object v0, v1

    goto :goto_f

    .line 3923
    :pswitch_8
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v0, "AV"

    const-string v2, "FINISH_BY_RESULT_EXIST_WEIBO_EMAILID"

    invoke-static {v0, v2}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 3924
    iput-boolean v3, p0, Lcom/osp/app/signin/AccountView;->A:Z

    .line 3925
    if-eqz p3, :cond_3a

    const-string v0, "email_id"

    invoke-virtual {p3, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    :cond_3a
    iput-object v1, p0, Lcom/osp/app/signin/AccountView;->O:Ljava/lang/String;

    goto/16 :goto_1

    .line 3597
    :catch_6
    move-exception v2

    goto/16 :goto_c

    .line 3587
    :catch_7
    move-exception v0

    goto/16 :goto_b

    .line 3564
    :catch_8
    move-exception v0

    goto/16 :goto_a

    :cond_3b
    move v0, v2

    goto/16 :goto_2

    .line 3429
    :pswitch_data_0
    .packed-switch -0x1
        :pswitch_1
        :pswitch_3
        :pswitch_4
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_2
        :pswitch_0
        :pswitch_6
        :pswitch_5
        :pswitch_0
        :pswitch_0
        :pswitch_7
        :pswitch_8
    .end packed-switch
.end method

.method public onBackPressed()V
    .locals 6

    .prologue
    const/4 v5, 0x0

    .line 4365
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v0, "AccountView::onBackPressed"

    invoke-static {v0}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;)V

    .line 4367
    invoke-static {p0}, Lcom/osp/app/util/ad;->d(Landroid/content/Context;)Z

    move-result v0

    if-nez v0, :cond_0

    invoke-static {}, Lcom/osp/app/signin/SamsungService;->a()Z

    move-result v0

    if-nez v0, :cond_0

    .line 4369
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v0, "AV"

    const-string v1, "Logging Service initiated by pressing back key"

    invoke-static {v0, v1}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 4370
    invoke-virtual {p0}, Lcom/osp/app/signin/AccountView;->f()V

    .line 4386
    :cond_0
    iget-object v0, p0, Lcom/osp/app/signin/AccountView;->J:Landroid/content/Intent;

    const/4 v1, 0x0

    invoke-direct {p0, v5, v0, v1}, Lcom/osp/app/signin/AccountView;->a(ILandroid/content/Intent;Landroid/content/Intent;)V

    .line 4392
    new-instance v0, Ljava/util/LinkedList;

    invoke-direct {v0}, Ljava/util/LinkedList;-><init>()V

    .line 4394
    new-instance v1, Lcom/osp/app/bigdatalog/c;

    invoke-virtual {p0}, Lcom/osp/app/signin/AccountView;->getApplicationContext()Landroid/content/Context;

    move-result-object v2

    const/4 v3, 0x1

    iget-object v4, p0, Lcom/osp/app/signin/AccountView;->J:Landroid/content/Intent;

    invoke-direct {v1, v2, v5, v3, v4}, Lcom/osp/app/bigdatalog/c;-><init>(Landroid/content/Context;IILandroid/content/Intent;)V

    .line 4398
    invoke-interface {v0, v1}, Ljava/util/Queue;->add(Ljava/lang/Object;)Z

    .line 4400
    iget-boolean v1, p0, Lcom/osp/app/signin/AccountView;->an:Z

    if-nez v1, :cond_1

    .line 4402
    new-instance v1, Lcom/osp/app/bigdatalog/c;

    invoke-virtual {p0}, Lcom/osp/app/signin/AccountView;->getApplicationContext()Landroid/content/Context;

    move-result-object v2

    const/4 v3, 0x6

    iget-object v4, p0, Lcom/osp/app/signin/AccountView;->J:Landroid/content/Intent;

    invoke-direct {v1, v2, v5, v3, v4}, Lcom/osp/app/bigdatalog/c;-><init>(Landroid/content/Context;IILandroid/content/Intent;)V

    .line 4404
    invoke-interface {v0, v1}, Ljava/util/Queue;->add(Ljava/lang/Object;)Z

    .line 4407
    :cond_1
    invoke-static {}, Lcom/osp/app/bigdatalog/e;->a()Lcom/osp/app/bigdatalog/e;

    move-result-object v1

    invoke-virtual {p0}, Lcom/osp/app/signin/AccountView;->getApplicationContext()Landroid/content/Context;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/osp/app/bigdatalog/e;->a(Landroid/content/Context;)V

    .line 4408
    invoke-static {}, Lcom/osp/app/bigdatalog/e;->a()Lcom/osp/app/bigdatalog/e;

    move-result-object v1

    invoke-virtual {p0}, Lcom/osp/app/signin/AccountView;->getApplicationContext()Landroid/content/Context;

    move-result-object v2

    invoke-virtual {v1, v2, v0}, Lcom/osp/app/bigdatalog/e;->a(Landroid/content/Context;Ljava/util/Queue;)V

    .line 4410
    invoke-super {p0}, Lcom/osp/app/util/BaseActivity;->onBackPressed()V

    .line 4411
    return-void
.end method

.method public onConfigurationChanged(Landroid/content/res/Configuration;)V
    .locals 5

    .prologue
    .line 4417
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v0, "AV"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "onConfigurationChanged(), mState="

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v2, p0, Lcom/osp/app/signin/AccountView;->V:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 4421
    iget v0, p0, Lcom/osp/app/signin/AccountView;->V:I

    if-nez v0, :cond_0

    .line 4425
    :try_start_0
    invoke-virtual {p0}, Lcom/osp/app/signin/AccountView;->getCurrentFocus()Landroid/view/View;

    move-result-object v0

    .line 4426
    const/4 v2, 0x0

    .line 4428
    const/4 v1, 0x0

    .line 4429
    if-eqz v0, :cond_1

    .line 4431
    invoke-virtual {v0}, Landroid/view/View;->getId()I

    move-result v1

    .line 4433
    instance-of v3, v0, Landroid/widget/TextView;

    if-eqz v3, :cond_1

    .line 4435
    check-cast v0, Landroid/widget/TextView;

    invoke-virtual {v0}, Landroid/widget/TextView;->onSaveInstanceState()Landroid/os/Parcelable;

    move-result-object v0

    move v4, v1

    move-object v1, v0

    move v0, v4

    .line 4439
    :goto_0
    const/4 v2, 0x1

    const/4 v3, 0x1

    invoke-direct {p0, v2, v3}, Lcom/osp/app/signin/AccountView;->a(ZZ)V

    .line 4441
    if-lez v0, :cond_0

    .line 4443
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "focusId="

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;)V

    .line 4445
    invoke-virtual {p0, v0}, Lcom/osp/app/signin/AccountView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    .line 4447
    if-eqz v0, :cond_0

    .line 4449
    invoke-virtual {v0}, Landroid/view/View;->requestFocus()Z

    .line 4450
    instance-of v2, v0, Landroid/widget/TextView;

    if-eqz v2, :cond_0

    if-eqz v1, :cond_0

    .line 4452
    check-cast v0, Landroid/widget/TextView;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->onRestoreInstanceState(Landroid/os/Parcelable;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 4462
    :cond_0
    :goto_1
    invoke-super {p0, p1}, Lcom/osp/app/util/BaseActivity;->onConfigurationChanged(Landroid/content/res/Configuration;)V

    .line 4463
    return-void

    .line 4456
    :catch_0
    move-exception v0

    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_1

    :cond_1
    move v0, v1

    move-object v1, v2

    goto :goto_0
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 11

    .prologue
    const/16 v2, 0x8

    const/4 v10, 0x1

    const/4 v1, 0x0

    .line 4632
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v0, "AV"

    const-string v3, "onCreate START"

    invoke-static {v0, v3}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 4633
    invoke-virtual {p0, v10}, Lcom/osp/app/signin/AccountView;->d(I)V

    .line 4635
    invoke-static {p0}, Lcom/osp/app/util/ad;->d(Landroid/content/Context;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 4637
    sget-object v0, Lcom/osp/app/loggingservice/c;->b:Lcom/osp/app/loggingservice/c;

    invoke-static {v0}, Lcom/osp/app/loggingservice/LoggingService;->a(Lcom/osp/app/loggingservice/c;)V

    .line 4640
    :cond_0
    invoke-virtual {p0}, Lcom/osp/app/signin/AccountView;->getApplication()Landroid/app/Application;

    move-result-object v0

    check-cast v0, Lcom/osp/app/signin/SamsungService;

    invoke-virtual {v0, p0}, Lcom/osp/app/signin/SamsungService;->a(Landroid/app/Activity;)V

    .line 4642
    invoke-super {p0, p1}, Lcom/osp/app/util/BaseActivity;->onCreate(Landroid/os/Bundle;)V

    .line 4644
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v3, 0x12

    if-lt v0, v3, :cond_1

    const-string v0, "user"

    invoke-virtual {p0, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/os/UserManager;

    invoke-virtual {v0}, Landroid/os/UserManager;->getUserRestrictions()Landroid/os/Bundle;

    move-result-object v0

    const-string v3, "no_modify_accounts"

    invoke-virtual {v0, v3, v1}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    :goto_0
    if-eqz v0, :cond_2

    .line 4646
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v0, "This app is not avaliable in a restricted profile"

    invoke-static {v0}, Lcom/osp/app/util/aq;->c(Ljava/lang/String;)V

    .line 4648
    new-instance v0, Landroid/content/Intent;

    const-string v1, "com.msc.action.samsungaccount.checklistinfopopup"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 4649
    const-string v1, "key_restricted_profile"

    invoke-virtual {v0, v1, v10}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 4650
    invoke-virtual {p0, v0}, Lcom/osp/app/signin/AccountView;->startActivity(Landroid/content/Intent;)V

    .line 4652
    invoke-virtual {p0, v10}, Lcom/osp/app/signin/AccountView;->setResult(I)V

    .line 4653
    invoke-virtual {p0}, Lcom/osp/app/signin/AccountView;->finish()V

    .line 4746
    :goto_1
    return-void

    :cond_1
    move v0, v1

    .line 4644
    goto :goto_0

    .line 4657
    :cond_2
    sput v1, Lcom/osp/app/signin/AccountView;->aN:I

    .line 4658
    invoke-direct {p0}, Lcom/osp/app/signin/AccountView;->G()V

    .line 4660
    new-instance v0, Lcom/osp/app/signin/h;

    invoke-direct {v0, p0}, Lcom/osp/app/signin/h;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/osp/app/signin/AccountView;->aM:Lcom/osp/app/signin/h;

    .line 4662
    invoke-virtual {p0}, Lcom/osp/app/signin/AccountView;->getIntent()Landroid/content/Intent;

    move-result-object v0

    iput-object v0, p0, Lcom/osp/app/signin/AccountView;->J:Landroid/content/Intent;

    .line 4664
    invoke-static {}, Lcom/osp/app/util/r;->b()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-static {p0}, Landroid/preference/PreferenceManager;->getDefaultSharedPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v3

    const-string v4, "data_popup_hide"

    invoke-interface {v3, v4, v1}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v3

    if-eqz v3, :cond_3

    move v0, v1

    :cond_3
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v3, "SCU"

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "is Data Popup Show   : "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;)V

    iput-boolean v0, p0, Lcom/osp/app/signin/AccountView;->aq:Z

    .line 4665
    invoke-static {p0}, Lcom/osp/app/util/ad;->d(Landroid/content/Context;)Z

    move-result v0

    if-nez v0, :cond_4

    .line 4667
    invoke-static {}, Lcom/osp/app/util/r;->b()Z

    move-result v0

    if-eqz v0, :cond_4

    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v0, "SCU"

    const-string v3, "reset ChineseModel DataPopup OKClickedPref"

    invoke-static {v0, v3}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;)V

    invoke-static {p0}, Landroid/preference/PreferenceManager;->getDefaultSharedPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    const-string v3, "data_popup_ok_clicked"

    invoke-interface {v0, v3, v1}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 4672
    :cond_4
    invoke-static {}, Lcom/osp/app/bigdatalog/e;->a()Lcom/osp/app/bigdatalog/e;

    move-result-object v0

    invoke-virtual {p0}, Lcom/osp/app/signin/AccountView;->getApplicationContext()Landroid/content/Context;

    move-result-object v3

    invoke-virtual {v0, v3}, Lcom/osp/app/bigdatalog/e;->a(Landroid/content/Context;)V

    .line 4674
    invoke-static {}, Lcom/osp/app/bigdatalog/e;->a()Lcom/osp/app/bigdatalog/e;

    move-result-object v0

    invoke-virtual {v0}, Lcom/osp/app/bigdatalog/e;->c()Z

    move-result v0

    if-nez v0, :cond_5

    .line 4676
    invoke-static {}, Lcom/osp/app/bigdatalog/e;->a()Lcom/osp/app/bigdatalog/e;

    move-result-object v0

    invoke-virtual {p0}, Lcom/osp/app/signin/AccountView;->getApplicationContext()Landroid/content/Context;

    move-result-object v3

    invoke-virtual {v0, v3}, Lcom/osp/app/bigdatalog/e;->b(Landroid/content/Context;)V

    .line 4680
    :cond_5
    iget-object v0, p0, Lcom/osp/app/signin/AccountView;->J:Landroid/content/Intent;

    invoke-virtual {v0}, Landroid/content/Intent;->getData()Landroid/net/Uri;

    move-result-object v0

    iput-object v0, p0, Lcom/osp/app/signin/AccountView;->ao:Landroid/net/Uri;

    .line 4681
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v3, "AccountView::onCreate isFromDeepLinkUri   : "

    invoke-direct {v0, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-direct {p0}, Lcom/osp/app/signin/AccountView;->K()Z

    move-result v3

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/osp/app/util/aq;->c(Ljava/lang/String;)V

    .line 4684
    iget-object v0, p0, Lcom/osp/app/signin/AccountView;->ao:Landroid/net/Uri;

    if-eqz v0, :cond_6

    iget-object v0, p0, Lcom/osp/app/signin/AccountView;->ao:Landroid/net/Uri;

    invoke-virtual {v0}, Landroid/net/Uri;->getScheme()Ljava/lang/String;

    move-result-object v0

    iget-object v3, p0, Lcom/osp/app/signin/AccountView;->ao:Landroid/net/Uri;

    invoke-virtual {v3}, Landroid/net/Uri;->getHost()Ljava/lang/String;

    move-result-object v3

    if-eqz v0, :cond_6

    if-nez v3, :cond_e

    .line 4693
    :cond_6
    :goto_2
    iput-boolean v1, p0, Lcom/osp/app/signin/AccountView;->P:Z

    .line 4697
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v3, "AccountView::onCreate m_SoftwareVersion : "

    invoke-direct {v0, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    sget v3, Landroid/os/Build$VERSION;->SDK_INT:I

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/osp/app/util/aq;->c(Ljava/lang/String;)V

    .line 4707
    :try_start_0
    invoke-static {p0}, Lcom/osp/app/util/ad;->d(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_7

    if-eqz p1, :cond_7

    .line 4709
    const-string v0, "is_destoryed_during_signin"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/osp/app/signin/AccountView;->ah:Z

    .line 4710
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v0, "AV"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "restore - destroyed during signin - "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-boolean v4, p0, Lcom/osp/app/signin/AccountView;->ah:Z

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v0, v3}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 4714
    :cond_7
    invoke-static {p0}, Lcom/osp/app/util/r;->g(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_8

    invoke-static {}, Lcom/osp/app/signin/SamsungService;->a()Z

    move-result v0

    if-eqz v0, :cond_8

    .line 4716
    const v0, 0x7f0c006d

    invoke-virtual {p0, v0}, Lcom/osp/app/signin/AccountView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    .line 4717
    if-eqz v0, :cond_8

    .line 4719
    invoke-virtual {v0}, Landroid/view/View;->getViewTreeObserver()Landroid/view/ViewTreeObserver;

    move-result-object v0

    iget-object v3, p0, Lcom/osp/app/signin/AccountView;->aS:Landroid/view/ViewTreeObserver$OnPreDrawListener;

    invoke-virtual {v0, v3}, Landroid/view/ViewTreeObserver;->addOnPreDrawListener(Landroid/view/ViewTreeObserver$OnPreDrawListener;)V

    .line 4723
    :cond_8
    const-string v0, "com.osp.app.signin.action.SAMSUNG_ACCOUNT_SETUPWIZARD"

    iget-object v3, p0, Lcom/osp/app/signin/AccountView;->J:Landroid/content/Intent;

    invoke-virtual {v3}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    const-string v3, "com.osp.app.signin.SetupWizardActivity"

    iget-object v4, p0, Lcom/osp/app/signin/AccountView;->J:Landroid/content/Intent;

    invoke-virtual {v4}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    const-string v4, "com.sec.android.app.SecSetupWizard"

    invoke-virtual {p0}, Lcom/osp/app/signin/AccountView;->getCallingPackage()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v4

    const-string v5, "com.sec.android.app.setupwizard"

    invoke-virtual {p0}, Lcom/osp/app/signin/AccountView;->getCallingPackage()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v5

    const-string v6, "com.sds.test.samsungaccount"

    invoke-virtual {p0}, Lcom/osp/app/signin/AccountView;->getCallingPackage()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v6

    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v7, "AV"

    new-instance v8, Ljava/lang/StringBuilder;

    const-string v9, "Oldaction:"

    invoke-direct {v8, v9}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v8, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, " NewAction:"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, " SetupWizard:"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v4}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, " TestApp:"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v6}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, " SetupWizardVZWModel:"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v5}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v7, v8}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;)V

    if-nez v4, :cond_9

    if-nez v6, :cond_9

    if-eqz v5, :cond_f

    :cond_9
    if-nez v0, :cond_a

    if-eqz v3, :cond_f

    :cond_a
    const/4 v0, 0x1

    invoke-static {v0}, Lcom/osp/app/signin/SamsungService;->a(Z)V

    iget-object v0, p0, Lcom/osp/app/signin/AccountView;->J:Landroid/content/Intent;

    const-string v3, "required_auth"

    const/4 v4, 0x0

    invoke-virtual {v0, v3, v4}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v0

    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v3, "AV"

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "SetupWizard required auth "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;)V

    move v3, v0

    :goto_3
    const-string v0, "com.osp.app.signin.action.ADD_SAMSUNG_ACCOUNT"

    iget-object v4, p0, Lcom/osp/app/signin/AccountView;->J:Landroid/content/Intent;

    invoke-virtual {v4}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v0, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_b

    invoke-direct {p0}, Lcom/osp/app/signin/AccountView;->r()Z

    move-result v0

    if-eqz v0, :cond_12

    :cond_b
    iget-object v0, p0, Lcom/osp/app/signin/AccountView;->J:Landroid/content/Intent;

    const-string v4, "theme"

    invoke-virtual {v0, v4}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/osp/app/signin/AccountView;->aA:Ljava/lang/String;

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/osp/app/signin/AccountView;->e:Z

    invoke-virtual {p0}, Lcom/osp/app/signin/AccountView;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string v4, "login_id"

    invoke-virtual {v0, v4}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/osp/app/signin/AccountView;->ai:Ljava/lang/String;

    iget-object v0, p0, Lcom/osp/app/signin/AccountView;->ai:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_c

    invoke-virtual {p0}, Lcom/osp/app/signin/AccountView;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string v4, "EmailID"

    invoke-virtual {v0, v4}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/osp/app/signin/AccountView;->ai:Ljava/lang/String;

    :cond_c
    iget-object v0, p0, Lcom/osp/app/signin/AccountView;->ai:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_d

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/osp/app/signin/AccountView;->aj:Z

    :cond_d
    iget-object v0, p0, Lcom/osp/app/signin/AccountView;->J:Landroid/content/Intent;

    const-string v4, "manageAccount"

    invoke-virtual {v0, v4}, Landroid/content/Intent;->hasExtra(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_14

    invoke-static {p0}, Lcom/osp/app/util/ad;->d(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_10

    iget-boolean v0, p0, Lcom/osp/app/signin/AccountView;->ah:Z

    if-nez v0, :cond_10

    invoke-static {}, Lcom/osp/app/util/ap;->a()Lcom/osp/app/util/ap;

    const v0, 0x7f09006c

    const/4 v2, 0x0

    invoke-static {p0, v0, v2}, Lcom/osp/app/util/ap;->a(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    const-string v2, "error_message"

    const-string v3, "Samsung account already exists"

    invoke-virtual {v0, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    const/4 v2, 0x1

    invoke-virtual {p0, v2, v0}, Lcom/osp/app/signin/AccountView;->a(ILandroid/content/Intent;)V

    invoke-virtual {p0}, Lcom/osp/app/signin/AccountView;->finish()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 4733
    :goto_4
    new-instance v0, Lcom/osp/app/signin/u;

    invoke-direct {v0, p0, p0}, Lcom/osp/app/signin/u;-><init>(Lcom/osp/app/signin/AccountView;Landroid/content/Context;)V

    invoke-virtual {v0}, Lcom/msc/c/b;->c()V

    invoke-virtual {v0}, Lcom/msc/c/b;->b()V

    .line 4739
    new-instance v0, Lcom/osp/app/bigdatalog/c;

    invoke-virtual {p0}, Lcom/osp/app/signin/AccountView;->getApplicationContext()Landroid/content/Context;

    move-result-object v2

    iget-object v3, p0, Lcom/osp/app/signin/AccountView;->J:Landroid/content/Intent;

    invoke-direct {v0, v2, v1, v1, v3}, Lcom/osp/app/bigdatalog/c;-><init>(Landroid/content/Context;IILandroid/content/Intent;)V

    .line 4740
    invoke-static {}, Lcom/osp/app/bigdatalog/e;->a()Lcom/osp/app/bigdatalog/e;

    move-result-object v1

    invoke-virtual {p0}, Lcom/osp/app/signin/AccountView;->getApplicationContext()Landroid/content/Context;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/osp/app/bigdatalog/e;->a(Landroid/content/Context;)V

    .line 4741
    invoke-static {}, Lcom/osp/app/bigdatalog/e;->a()Lcom/osp/app/bigdatalog/e;

    move-result-object v1

    invoke-virtual {p0}, Lcom/osp/app/signin/AccountView;->getApplicationContext()Landroid/content/Context;

    move-result-object v2

    invoke-virtual {v1, v2, v0}, Lcom/osp/app/bigdatalog/e;->a(Landroid/content/Context;Lcom/osp/app/bigdatalog/c;)V

    .line 4744
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v0, "AV"

    const-string v1, "onCreate END"

    invoke-static {v0, v1}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_1

    .line 4684
    :cond_e
    const-string v4, "samsungaccount"

    invoke-virtual {v0, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_6

    const-string v0, "ProfilePage"

    invoke-virtual {v3, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_6

    new-instance v0, Landroid/content/Intent;

    const-string v3, "com.msc.action.samsungaccount.PROFILE_FOR_SETTING"

    invoke-direct {v0, v3}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0, v0}, Lcom/osp/app/signin/AccountView;->startActivity(Landroid/content/Intent;)V

    invoke-virtual {p0}, Lcom/osp/app/signin/AccountView;->finish()V

    goto/16 :goto_2

    .line 4723
    :cond_f
    const/4 v0, 0x0

    :try_start_1
    invoke-static {v0}, Lcom/osp/app/signin/SamsungService;->a(Z)V

    move v3, v1

    goto/16 :goto_3

    :cond_10
    const-string v0, "com.osp.app.signin.action.ADD_SAMSUNG_ACCOUNT"

    iget-object v4, p0, Lcom/osp/app/signin/AccountView;->J:Landroid/content/Intent;

    invoke-virtual {v4}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v0, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_11

    iget-object v0, p0, Lcom/osp/app/signin/AccountView;->J:Landroid/content/Intent;

    invoke-static {v0}, Lcom/osp/app/signin/AccountView;->b(Landroid/content/Intent;)Ljava/lang/String;

    move-result-object v0

    :goto_5
    if-eqz v0, :cond_14

    new-instance v2, Landroid/content/Intent;

    invoke-direct {v2}, Landroid/content/Intent;-><init>()V

    const-string v3, "error_message"

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "Param ["

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v4, "] must not be null"

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v3, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    const/4 v0, 0x1

    invoke-virtual {p0, v0, v2}, Lcom/osp/app/signin/AccountView;->a(ILandroid/content/Intent;)V

    invoke-virtual {p0}, Lcom/osp/app/signin/AccountView;->finish()V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    goto/16 :goto_4

    .line 4725
    :catch_0
    move-exception v0

    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    goto/16 :goto_4

    .line 4723
    :cond_11
    :try_start_2
    iget-object v0, p0, Lcom/osp/app/signin/AccountView;->J:Landroid/content/Intent;

    invoke-static {v0}, Lcom/osp/app/signin/AccountView;->a(Landroid/content/Intent;)Ljava/lang/String;

    move-result-object v0

    goto :goto_5

    :cond_12
    const-string v0, "com.osp.app.signin.action.SAMSUNG_ACCOUNT_SETUPWIZARD"

    iget-object v4, p0, Lcom/osp/app/signin/AccountView;->J:Landroid/content/Intent;

    invoke-virtual {v4}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v0, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_13

    const-string v0, "com.osp.app.signin.SetupWizardActivity"

    iget-object v4, p0, Lcom/osp/app/signin/AccountView;->J:Landroid/content/Intent;

    invoke-virtual {v4}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v0, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_16

    :cond_13
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/osp/app/signin/AccountView;->e:Z

    :cond_14
    :goto_6
    iget-object v0, p0, Lcom/osp/app/signin/AccountView;->J:Landroid/content/Intent;

    const-string v4, "client_id"

    invoke-virtual {v0, v4}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/osp/app/signin/AccountView;->H:Ljava/lang/String;

    iget-object v0, p0, Lcom/osp/app/signin/AccountView;->J:Landroid/content/Intent;

    const-string v4, "client_secret"

    invoke-virtual {v0, v4}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/osp/app/signin/AccountView;->I:Ljava/lang/String;

    iget-object v0, p0, Lcom/osp/app/signin/AccountView;->J:Landroid/content/Intent;

    const-string v4, "account_mode"

    invoke-virtual {v0, v4}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/osp/app/signin/AccountView;->C:Ljava/lang/String;

    iget-object v0, p0, Lcom/osp/app/signin/AccountView;->J:Landroid/content/Intent;

    const-string v4, "service_name"

    invoke-virtual {v0, v4}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/osp/app/signin/AccountView;->N:Ljava/lang/String;

    iget-object v0, p0, Lcom/osp/app/signin/AccountView;->J:Landroid/content/Intent;

    const-string v4, "MODE"

    invoke-virtual {v0, v4}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/osp/app/signin/AccountView;->S:Ljava/lang/String;

    iget-object v0, p0, Lcom/osp/app/signin/AccountView;->J:Landroid/content/Intent;

    const-string v4, "BG_WhoareU"

    invoke-virtual {v0, v4}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/osp/app/signin/AccountView;->T:Ljava/lang/String;

    iget-object v0, p0, Lcom/osp/app/signin/AccountView;->J:Landroid/content/Intent;

    const-string v4, "OSP_VER"

    invoke-virtual {v0, v4}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/osp/app/util/ad;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/osp/app/signin/AccountView;->L:Ljava/lang/String;

    iget-object v0, p0, Lcom/osp/app/signin/AccountView;->J:Landroid/content/Intent;

    const-string v4, "more_info"

    invoke-virtual {v0, v4}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/osp/app/signin/AccountView;->X:Ljava/lang/String;

    iget-object v0, p0, Lcom/osp/app/signin/AccountView;->J:Landroid/content/Intent;

    const-string v4, "key_request_id"

    const-wide/16 v6, 0x0

    invoke-virtual {v0, v4, v6, v7}, Landroid/content/Intent;->getLongExtra(Ljava/lang/String;J)J

    move-result-wide v4

    iput-wide v4, p0, Lcom/osp/app/signin/AccountView;->Z:J

    iget-object v0, p0, Lcom/osp/app/signin/AccountView;->J:Landroid/content/Intent;

    const-string v4, "force_agree"

    const/4 v5, 0x0

    invoke-virtual {v0, v4, v5}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v0

    iput-boolean v0, p0, Lcom/osp/app/signin/AccountView;->am:Z

    iget-object v0, p0, Lcom/osp/app/signin/AccountView;->J:Landroid/content/Intent;

    const-string v4, "set_value"

    invoke-virtual {v0, v4}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/osp/app/signin/AccountView;->Y:Ljava/lang/String;

    iget-object v0, p0, Lcom/osp/app/signin/AccountView;->J:Landroid/content/Intent;

    const-string v4, "sa_mode"

    invoke-virtual {v0, v4}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    const-string v0, "BG_mode"

    iget-object v4, p0, Lcom/osp/app/signin/AccountView;->T:Ljava/lang/String;

    invoke-virtual {v0, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_15

    iget-object v0, p0, Lcom/osp/app/signin/AccountView;->J:Landroid/content/Intent;

    const-string v4, "mypackage"

    invoke-virtual {v0, v4}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/osp/app/signin/AccountView;->R:Ljava/lang/String;

    :cond_15
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v4, "AccountView::paramFromServiceApp m_client_id : "

    invoke-direct {v0, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v4, p0, Lcom/osp/app/signin/AccountView;->H:Ljava/lang/String;

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;)V

    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v4, "AccountView::paramFromServiceApp m_client_secret : "

    invoke-direct {v0, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v4, p0, Lcom/osp/app/signin/AccountView;->I:Ljava/lang/String;

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;)V

    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v4, "AccountView::paramFromServiceApp m_account_mode : "

    invoke-direct {v0, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v4, p0, Lcom/osp/app/signin/AccountView;->C:Ljava/lang/String;

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;)V

    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v4, "AccountView::paramFromServiceApp m_service_name : "

    invoke-direct {v0, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v4, p0, Lcom/osp/app/signin/AccountView;->N:Ljava/lang/String;

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;)V

    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v4, "AccountView::paramFromServiceApp m_Usermode : "

    invoke-direct {v0, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v4, p0, Lcom/osp/app/signin/AccountView;->S:Ljava/lang/String;

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;)V

    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v4, "AccountView::paramFromServiceApp m_WHOAREU : "

    invoke-direct {v0, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v4, p0, Lcom/osp/app/signin/AccountView;->T:Ljava/lang/String;

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;)V

    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v4, "AccountView::paramFromServiceApp m_OspVersion : "

    invoke-direct {v0, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v4, p0, Lcom/osp/app/signin/AccountView;->L:Ljava/lang/String;

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;)V

    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v4, "AccountView::paramFromServiceApp m_Sourcepackage : "

    invoke-direct {v0, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v4, p0, Lcom/osp/app/signin/AccountView;->R:Ljava/lang/String;

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;)V

    invoke-static {}, Lcom/osp/app/signin/SamsungService;->a()Z

    move-result v0

    if-eqz v0, :cond_17

    if-eqz v3, :cond_17

    invoke-static {}, Lcom/osp/device/b;->b()Lcom/osp/device/b;

    move-result-object v0

    const-string v2, "true"

    invoke-virtual {v0, p0, v2}, Lcom/osp/device/b;->a(Landroid/content/Context;Ljava/lang/String;)V

    new-instance v0, Landroid/content/Intent;

    const-string v2, "com.msc.action.samsungaccount.activate_account"

    invoke-direct {v0, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    const-string v2, "email_id"

    const-string v3, ""

    invoke-virtual {v0, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    const/16 v2, 0xcd

    invoke-virtual {p0, v0, v2}, Lcom/osp/app/signin/AccountView;->startActivityForResult(Landroid/content/Intent;I)V

    goto/16 :goto_4

    :cond_16
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/osp/app/signin/AccountView;->e:Z

    goto/16 :goto_6

    :cond_17
    invoke-static {p0}, Lcom/osp/app/util/ad;->d(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_1a

    iget-boolean v0, p0, Lcom/osp/app/signin/AccountView;->ah:Z

    if-nez v0, :cond_1a

    invoke-static {}, Lcom/osp/app/signin/SamsungService;->a()Z

    move-result v0

    if-eqz v0, :cond_18

    const/4 v0, -0x1

    iget-object v2, p0, Lcom/osp/app/signin/AccountView;->J:Landroid/content/Intent;

    const/4 v3, 0x0

    invoke-direct {p0, v0, v2, v3}, Lcom/osp/app/signin/AccountView;->a(ILandroid/content/Intent;Landroid/content/Intent;)V

    const/4 v0, 0x0

    invoke-static {v0}, Lcom/osp/app/signin/SamsungService;->a(Z)V

    invoke-virtual {p0}, Lcom/osp/app/signin/AccountView;->finish()V

    goto/16 :goto_4

    :cond_18
    const-string v0, "ADD_ACCOUNT"

    iget-object v3, p0, Lcom/osp/app/signin/AccountView;->S:Ljava/lang/String;

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_19

    const-string v0, "TIPS_WIDGET"

    iget-object v3, p0, Lcom/osp/app/signin/AccountView;->S:Ljava/lang/String;

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1a

    :cond_19
    invoke-static {}, Lcom/osp/app/util/ap;->a()Lcom/osp/app/util/ap;

    const v0, 0x7f09006c

    const/4 v2, 0x0

    invoke-static {p0, v0, v2}, Lcom/osp/app/util/ap;->a(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    const/4 v0, 0x0

    iget-object v2, p0, Lcom/osp/app/signin/AccountView;->J:Landroid/content/Intent;

    const/4 v3, 0x0

    invoke-direct {p0, v0, v2, v3}, Lcom/osp/app/signin/AccountView;->a(ILandroid/content/Intent;Landroid/content/Intent;)V

    invoke-virtual {p0}, Lcom/osp/app/signin/AccountView;->finish()V

    goto/16 :goto_4

    :cond_1a
    iget-object v0, p0, Lcom/osp/app/signin/AccountView;->J:Landroid/content/Intent;

    const-string v3, "manageAccount"

    invoke-virtual {v0, v3}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Landroid/accounts/AccountAuthenticatorResponse;

    if-eqz v0, :cond_1b

    const/4 v3, 0x0

    invoke-virtual {v0, v3}, Landroid/accounts/AccountAuthenticatorResponse;->onResult(Landroid/os/Bundle;)V

    :cond_1b
    const-string v0, "SamsungApps"

    iget-object v3, p0, Lcom/osp/app/signin/AccountView;->N:Ljava/lang/String;

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1c

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/osp/app/signin/AccountView;->K:Z

    :cond_1c
    iget-object v3, p0, Lcom/osp/app/signin/AccountView;->C:Ljava/lang/String;

    iget-object v4, p0, Lcom/osp/app/signin/AccountView;->S:Ljava/lang/String;

    iget-object v5, p0, Lcom/osp/app/signin/AccountView;->L:Ljava/lang/String;

    if-nez v3, :cond_1d

    if-eqz v4, :cond_25

    :cond_1d
    const-string v0, "ACCOUNT_VERIFY"

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1f

    invoke-static {p0}, Lcom/osp/app/util/ad;->d(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_1e

    const/4 v0, 0x2

    :goto_7
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    new-instance v6, Ljava/lang/StringBuilder;

    const-string v7, "DbManager::getAccountMode _accountmode : "

    invoke-direct {v6, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v6, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;)V

    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v6, "DbManager::getAccountMode _Usermode : "

    invoke-direct {v3, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;)V

    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "DbManager::getAccountMode _OspVersion : "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;)V

    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "DbManager::getAccountMode state : "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;)V

    iput v0, p0, Lcom/osp/app/signin/AccountView;->V:I

    invoke-static {p0}, Lcom/osp/app/util/ad;->a(Landroid/content/Context;)Z

    move-result v0

    if-nez v0, :cond_26

    invoke-static {}, Lcom/osp/app/signin/SamsungService;->a()Z

    move-result v0

    if-nez v0, :cond_26

    iget v0, p0, Lcom/osp/app/signin/AccountView;->V:I

    if-eqz v0, :cond_26

    iget v0, p0, Lcom/osp/app/signin/AccountView;->V:I

    if-eq v0, v2, :cond_26

    invoke-virtual {p0}, Lcom/osp/app/signin/AccountView;->k()V

    invoke-static {}, Lcom/osp/app/util/ap;->a()Lcom/osp/app/util/ap;

    const v0, 0x7f09003e

    invoke-virtual {p0, v0}, Lcom/osp/app/signin/AccountView;->getString(I)Ljava/lang/String;

    move-result-object v0

    const/4 v2, 0x1

    invoke-static {p0, v0, v2}, Lcom/osp/app/util/ap;->a(Landroid/content/Context;Ljava/lang/String;I)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    const/4 v0, 0x3

    iget-object v2, p0, Lcom/osp/app/signin/AccountView;->J:Landroid/content/Intent;

    const/4 v3, 0x0

    invoke-direct {p0, v0, v2, v3}, Lcom/osp/app/signin/AccountView;->a(ILandroid/content/Intent;Landroid/content/Intent;)V

    invoke-virtual {p0}, Lcom/osp/app/signin/AccountView;->finish()V

    goto/16 :goto_4

    :cond_1e
    move v0, v1

    goto/16 :goto_7

    :cond_1f
    const-string v0, "ACCOUNTINFO_MODIFY"

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_21

    invoke-static {p0}, Lcom/osp/app/util/ad;->d(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_20

    const/4 v0, 0x4

    goto/16 :goto_7

    :cond_20
    move v0, v1

    goto/16 :goto_7

    :cond_21
    const-string v0, "REQUEST_TNC"

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_22

    const/4 v0, 0x5

    goto/16 :goto_7

    :cond_22
    const-string v0, "REQUEST_SAMSUNGAPPS_INFO"

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_23

    const/4 v0, 0x6

    goto/16 :goto_7

    :cond_23
    const-string v0, "PUBLIC_SMS_VALIDATION"

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_24

    move v0, v2

    goto/16 :goto_7

    :cond_24
    invoke-static {p0, v3, v5}, Lcom/msc/c/c;->b(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)I

    move-result v0

    goto/16 :goto_7

    :cond_25
    invoke-static {p0, v3, v5}, Lcom/msc/c/c;->b(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)I

    move-result v0

    goto/16 :goto_7

    :cond_26
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v0, "AV"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "AccountView::paramFromServiceApp entry point state :\t"

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v3, p0, Lcom/osp/app/signin/AccountView;->V:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;)V

    iget v0, p0, Lcom/osp/app/signin/AccountView;->V:I

    if-eqz v0, :cond_27

    invoke-static {p0}, Lcom/osp/app/util/ad;->a(Landroid/content/Context;)Z

    move-result v0

    if-ne v0, v10, :cond_27

    invoke-static {}, Lcom/osp/app/signin/SamsungService;->a()Z

    move-result v0

    if-nez v0, :cond_27

    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v0, "Self Update check"

    invoke-static {v0}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;)V

    invoke-static {}, Lcom/msc/sa/selfupdate/c;->b()Lcom/msc/sa/selfupdate/c;

    move-result-object v0

    invoke-virtual {p0}, Lcom/osp/app/signin/AccountView;->getApplication()Landroid/app/Application;

    move-result-object v2

    const/4 v3, 0x1

    const/4 v4, 0x0

    const/4 v5, 0x0

    invoke-virtual {v0, v2, v3, v4, v5}, Lcom/msc/sa/selfupdate/c;->a(Landroid/app/Application;ZZLjava/lang/String;)V

    :cond_27
    invoke-direct {p0}, Lcom/osp/app/signin/AccountView;->r()Z

    move-result v0

    if-eqz v0, :cond_28

    const/16 v0, 0x9

    iput v0, p0, Lcom/osp/app/signin/AccountView;->V:I

    :cond_28
    iget v0, p0, Lcom/osp/app/signin/AccountView;->V:I

    packed-switch v0, :pswitch_data_0

    invoke-static {p0}, Lcom/osp/app/util/ad;->h(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v0

    const/4 v2, 0x0

    invoke-direct {p0, v0, v2}, Lcom/osp/app/signin/AccountView;->b(Ljava/lang/String;Z)V

    goto/16 :goto_4

    :pswitch_0
    invoke-static {p0}, Lcom/osp/app/util/r;->i(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_29

    invoke-static {p0}, Lcom/msc/sa/c/d;->c(Landroid/app/Activity;)V

    :cond_29
    invoke-virtual {p0}, Lcom/osp/app/signin/AccountView;->h()Z

    move-result v0

    if-eqz v0, :cond_2a

    invoke-virtual {p0}, Lcom/osp/app/signin/AccountView;->i()V

    :goto_8
    invoke-static {p0}, Lcom/osp/app/util/ad;->a(Landroid/content/Context;)Z

    move-result v0

    if-nez v0, :cond_2b

    invoke-static {}, Lcom/osp/app/signin/SamsungService;->a()Z

    move-result v0

    if-nez v0, :cond_2b

    invoke-static {}, Lcom/osp/app/util/r;->b()Z

    move-result v0

    if-nez v0, :cond_2b

    const/16 v0, 0xd5

    invoke-virtual {p0, v0}, Lcom/osp/app/signin/AccountView;->c(I)V

    goto/16 :goto_4

    :cond_2a
    invoke-virtual {p0}, Lcom/osp/app/signin/AccountView;->j()V

    goto :goto_8

    :cond_2b
    invoke-virtual {p0}, Lcom/osp/app/signin/AccountView;->getCallingActivity()Landroid/content/ComponentName;

    move-result-object v0

    invoke-direct {p0}, Lcom/osp/app/signin/AccountView;->v()Ljava/lang/String;

    move-result-object v2

    invoke-static {}, Lcom/osp/device/b;->b()Lcom/osp/device/b;

    move-result-object v3

    invoke-virtual {v3}, Lcom/osp/device/b;->l()I

    move-result v3

    const/16 v4, 0x10

    if-le v3, v4, :cond_2c

    invoke-static {}, Lcom/osp/app/signin/SamsungService;->a()Z

    move-result v3

    if-nez v3, :cond_2c

    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_2c

    iget-boolean v3, p0, Lcom/osp/app/signin/AccountView;->aq:Z

    if-nez v3, :cond_2c

    invoke-direct {p0, v2}, Lcom/osp/app/signin/AccountView;->b(Ljava/lang/String;)V

    goto/16 :goto_4

    :cond_2c
    invoke-static {}, Lcom/osp/device/b;->b()Lcom/osp/device/b;

    move-result-object v2

    invoke-virtual {v2, p0}, Lcom/osp/device/b;->i(Landroid/content/Context;)Z

    move-result v2

    if-nez v2, :cond_2d

    invoke-virtual {p0, v0}, Lcom/osp/app/signin/AccountView;->a(Landroid/content/ComponentName;)Z

    move-result v0

    if-eqz v0, :cond_2d

    invoke-direct {p0}, Lcom/osp/app/signin/AccountView;->u()V

    goto/16 :goto_4

    :cond_2d
    invoke-direct {p0}, Lcom/osp/app/signin/AccountView;->x()V

    goto/16 :goto_4

    :pswitch_1
    invoke-static {p0}, Lcom/osp/app/util/ad;->h(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v0

    const/4 v2, 0x0

    invoke-direct {p0, v0, v2}, Lcom/osp/app/signin/AccountView;->b(Ljava/lang/String;Z)V

    goto/16 :goto_4

    :pswitch_2
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/osp/app/signin/AccountView;->a(Z)V

    goto/16 :goto_4

    :pswitch_3
    invoke-virtual {p0}, Lcom/osp/app/signin/AccountView;->k()V

    invoke-direct {p0}, Lcom/osp/app/signin/AccountView;->q()V

    goto/16 :goto_4

    :pswitch_4
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/osp/app/signin/AccountView;->a(Z)V

    goto/16 :goto_4

    :pswitch_5
    invoke-static {p0}, Lcom/osp/app/util/ad;->p(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/osp/app/signin/AccountView;->ag:Ljava/lang/String;

    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v0, "AccountView::ShowTnCActivity isCancelableJustOneActivity : false"

    invoke-static {v0}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;)V

    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    const-class v2, Lcom/osp/app/signin/TnCView;

    invoke-virtual {v0, p0, v2}, Landroid/content/Intent;->setClass(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;

    const-string v2, "client_id"

    iget-object v3, p0, Lcom/osp/app/signin/AccountView;->H:Ljava/lang/String;

    invoke-virtual {v0, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    const-string v2, "client_secret"

    iget-object v3, p0, Lcom/osp/app/signin/AccountView;->I:Ljava/lang/String;

    invoke-virtual {v0, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    const-string v2, "account_mode"

    iget-object v3, p0, Lcom/osp/app/signin/AccountView;->C:Ljava/lang/String;

    invoke-virtual {v0, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    const-string v2, "service_name"

    iget-object v3, p0, Lcom/osp/app/signin/AccountView;->N:Ljava/lang/String;

    invoke-virtual {v0, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    const-string v2, "OSP_VER"

    iget-object v3, p0, Lcom/osp/app/signin/AccountView;->L:Ljava/lang/String;

    invoke-virtual {v0, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    const-string v2, "key_is_name_verified"

    iget-boolean v3, p0, Lcom/osp/app/signin/AccountView;->ad:Z

    invoke-virtual {v0, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    const-string v2, "country_code_mcc"

    iget-object v3, p0, Lcom/osp/app/signin/AccountView;->ag:Ljava/lang/String;

    invoke-virtual {v0, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    iget-boolean v2, p0, Lcom/osp/app/signin/AccountView;->ad:Z

    if-eqz v2, :cond_2e

    iget-object v2, p0, Lcom/osp/app/signin/AccountView;->ae:Landroid/os/Bundle;

    if-eqz v2, :cond_2e

    const-string v2, "information_after_name_validation"

    iget-object v3, p0, Lcom/osp/app/signin/AccountView;->ae:Landroid/os/Bundle;

    invoke-virtual {v0, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Bundle;)Landroid/content/Intent;

    :cond_2e
    iget-boolean v2, p0, Lcom/osp/app/signin/AccountView;->e:Z

    if-eqz v2, :cond_2f

    const-string v2, "new_add_account_mode"

    const/4 v3, 0x1

    invoke-virtual {v0, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    :cond_2f
    const-string v2, "BG_mode"

    iget-object v3, p0, Lcom/osp/app/signin/AccountView;->T:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_30

    const-string v2, "BG_WhoareU"

    iget-object v3, p0, Lcom/osp/app/signin/AccountView;->T:Ljava/lang/String;

    invoke-virtual {v0, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    iget-object v2, p0, Lcom/osp/app/signin/AccountView;->R:Ljava/lang/String;

    if-eqz v2, :cond_30

    const-string v2, "mypackage"

    iget-object v3, p0, Lcom/osp/app/signin/AccountView;->R:Ljava/lang/String;

    invoke-virtual {v0, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    const-string v2, "key_request_id"

    iget-wide v4, p0, Lcom/osp/app/signin/AccountView;->Z:J

    invoke-virtual {v0, v2, v4, v5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;J)Landroid/content/Intent;

    :cond_30
    const-string v2, "MODE"

    iget-object v3, p0, Lcom/osp/app/signin/AccountView;->S:Ljava/lang/String;

    invoke-virtual {v0, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    const-string v2, "serviceApp_type"

    const/4 v3, 0x1

    invoke-virtual {v0, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    const/16 v2, 0xce

    invoke-virtual {p0, v0, v2}, Lcom/osp/app/signin/AccountView;->startActivityForResult(Landroid/content/Intent;I)V

    goto/16 :goto_4

    :pswitch_6
    invoke-direct {p0}, Lcom/osp/app/signin/AccountView;->q()V

    goto/16 :goto_4

    :pswitch_7
    invoke-virtual {p0}, Lcom/osp/app/signin/AccountView;->k()V

    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v0, "AccountView::CheckListExecute"

    invoke-static {v0}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;)V

    invoke-static {p0}, Lcom/osp/app/util/ad;->a(Landroid/content/Context;)Z

    move-result v0

    if-nez v0, :cond_31

    invoke-static {}, Lcom/osp/app/util/ap;->a()Lcom/osp/app/util/ap;

    const v0, 0x7f09003e

    invoke-virtual {p0, v0}, Lcom/osp/app/signin/AccountView;->getString(I)Ljava/lang/String;

    move-result-object v0

    const/4 v2, 0x0

    invoke-static {p0, v0, v2}, Lcom/osp/app/util/ap;->a(Landroid/content/Context;Ljava/lang/String;I)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    const/4 v0, 0x3

    invoke-virtual {p0, v0}, Lcom/osp/app/signin/AccountView;->b(I)V

    invoke-virtual {p0}, Lcom/osp/app/signin/AccountView;->finish()V

    goto/16 :goto_4

    :cond_31
    iget-boolean v0, p0, Lcom/osp/app/signin/AccountView;->am:Z

    if-eqz v0, :cond_32

    const-string v0, "11nvc8gurp"

    iget-object v2, p0, Lcom/osp/app/signin/AccountView;->H:Ljava/lang/String;

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_32

    new-instance v0, Lcom/osp/app/signin/cd;

    iget-object v2, p0, Lcom/osp/app/signin/AccountView;->H:Ljava/lang/String;

    iget-object v3, p0, Lcom/osp/app/signin/AccountView;->I:Ljava/lang/String;

    invoke-direct {v0, p0, v2, v3}, Lcom/osp/app/signin/cd;-><init>(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)V

    iput-object v0, p0, Lcom/osp/app/signin/AccountView;->al:Lcom/osp/app/signin/cd;

    iget-object v0, p0, Lcom/osp/app/signin/AccountView;->al:Lcom/osp/app/signin/cd;

    invoke-virtual {v0}, Lcom/osp/app/signin/cd;->b()V

    goto/16 :goto_4

    :cond_32
    new-instance v0, Lcom/osp/app/signin/ay;

    invoke-direct {v0, p0}, Lcom/osp/app/signin/ay;-><init>(Lcom/osp/app/signin/AccountView;)V

    iput-object v0, p0, Lcom/osp/app/signin/AccountView;->G:Lcom/osp/app/signin/ay;

    iget-object v0, p0, Lcom/osp/app/signin/AccountView;->G:Lcom/osp/app/signin/ay;

    invoke-virtual {v0}, Lcom/osp/app/signin/ay;->b()V

    goto/16 :goto_4

    :pswitch_8
    invoke-virtual {p0}, Lcom/osp/app/signin/AccountView;->k()V

    invoke-virtual {p0}, Lcom/osp/app/signin/AccountView;->d()V
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_0

    goto/16 :goto_4

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_7
        :pswitch_8
        :pswitch_8
    .end packed-switch
.end method

.method public onCreateDialog(I)Landroid/app/Dialog;
    .locals 13

    .prologue
    .line 8916
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "AccountView::onCreateDialog id : "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;)V

    .line 8918
    packed-switch p1, :pswitch_data_0

    .line 8947
    const/4 v0, 0x0

    :goto_0
    return-object v0

    .line 8921
    :pswitch_0
    iget-boolean v0, p0, Lcom/osp/app/signin/AccountView;->ar:Z

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/osp/app/signin/AccountView;->ak:Lcom/osp/app/signin/fv;

    if-nez v0, :cond_1

    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    :cond_1
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/osp/app/signin/AccountView;->ar:Z

    invoke-virtual {p0}, Lcom/osp/app/signin/AccountView;->getLayoutInflater()Landroid/view/LayoutInflater;

    move-result-object v0

    const v1, 0x7f030022

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v7

    const v0, 0x7f0c00ec

    invoke-virtual {v7, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/RelativeLayout;

    const v1, 0x7f0c00f1

    invoke-virtual {v7, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/LinearLayout;

    sget v2, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v3, 0x15

    if-lt v2, v3, :cond_2

    const v2, 0x7f0c00f2

    invoke-virtual {v7, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/LinearLayout;

    invoke-virtual {v2}, Landroid/widget/LinearLayout;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v4

    instance-of v3, v4, Landroid/view/ViewGroup$MarginLayoutParams;

    if-eqz v3, :cond_2

    move-object v3, v4

    check-cast v3, Landroid/view/ViewGroup$MarginLayoutParams;

    const/4 v5, 0x0

    iput v5, v3, Landroid/view/ViewGroup$MarginLayoutParams;->topMargin:I

    invoke-virtual {v2, v4}, Landroid/widget/LinearLayout;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    :cond_2
    invoke-virtual {p0}, Lcom/osp/app/signin/AccountView;->getResources()Landroid/content/res/Resources;

    move-result-object v8

    invoke-static {p0}, Lcom/osp/app/util/an;->a(Lcom/osp/app/util/BaseActivity;)Lcom/osp/app/util/an;

    const v2, 0x7f0c00ed

    invoke-virtual {v7, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    const v3, 0x7f0c00ee

    invoke-virtual {v7, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/LinearLayout;

    const v4, 0x7f070022

    invoke-static {v4}, Lcom/osp/app/util/an;->a(I)I

    move-result v4

    invoke-virtual {v8, v4}, Landroid/content/res/Resources;->getColor(I)I

    move-result v4

    invoke-virtual {v2, v4}, Landroid/widget/TextView;->setTextColor(I)V

    const/4 v4, 0x0

    invoke-virtual {p0}, Lcom/osp/app/signin/AccountView;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    const v6, 0x7f080104

    invoke-virtual {v5, v6}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v5

    invoke-virtual {v2, v4, v5}, Landroid/widget/TextView;->setTextSize(IF)V

    const v4, 0x7f0c00ef

    invoke-virtual {v7, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/ImageView;

    const v5, 0x7f0c00f0

    invoke-virtual {v7, v5}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v5

    invoke-static {p0}, Lcom/osp/app/util/r;->m(Landroid/content/Context;)Z

    move-result v6

    if-nez v6, :cond_3

    invoke-static {p0}, Lcom/osp/app/util/r;->k(Landroid/content/Context;)Z

    move-result v6

    if-eqz v6, :cond_8

    :cond_3
    const/4 v6, 0x0

    invoke-virtual {v3, v6}, Landroid/widget/LinearLayout;->setBackgroundResource(I)V

    const/16 v6, 0x8

    invoke-virtual {v5, v6}, Landroid/view/View;->setVisibility(I)V

    const/4 v5, 0x0

    invoke-virtual {v4, v5}, Landroid/widget/ImageView;->setBackgroundResource(I)V

    const v5, 0x7f02008d

    invoke-static {v5}, Lcom/osp/app/util/an;->a(I)I

    move-result v5

    invoke-virtual {v4, v5}, Landroid/widget/ImageView;->setImageResource(I)V

    invoke-static {v4}, Lcom/msc/sa/c/d;->a(Landroid/view/View;)V

    :goto_1
    const v4, 0x7f0c00f3

    invoke-virtual {v7, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/TextView;

    const v5, 0x7f070020

    invoke-static {v5}, Lcom/osp/app/util/an;->a(I)I

    move-result v5

    invoke-virtual {v8, v5}, Landroid/content/res/Resources;->getColor(I)I

    move-result v5

    invoke-virtual {v4, v5}, Landroid/widget/TextView;->setTextColor(I)V

    const v5, 0x7f0c00f4

    invoke-virtual {v7, v5}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v5

    check-cast v5, Landroid/widget/ImageView;

    const v6, 0x7f0c00f5

    invoke-virtual {v7, v6}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v9

    const v6, 0x7f0c0062

    invoke-virtual {v7, v6}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v6

    check-cast v6, Landroid/widget/Button;

    const v10, 0x7f0c0063

    invoke-virtual {v7, v10}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v7

    check-cast v7, Landroid/widget/Button;

    iget-object v10, p0, Lcom/osp/app/signin/AccountView;->aT:Landroid/view/View$OnClickListener;

    invoke-virtual {v6, v10}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object v6, p0, Lcom/osp/app/signin/AccountView;->aT:Landroid/view/View$OnClickListener;

    invoke-virtual {v7, v6}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object v6, p0, Lcom/osp/app/signin/AccountView;->ak:Lcom/osp/app/signin/fv;

    invoke-virtual {v6}, Lcom/osp/app/signin/fv;->a()Ljava/lang/String;

    move-result-object v6

    if-eqz v6, :cond_9

    iget-object v6, p0, Lcom/osp/app/signin/AccountView;->ak:Lcom/osp/app/signin/fv;

    invoke-virtual {v6}, Lcom/osp/app/signin/fv;->a()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/String;->length()I

    move-result v6

    if-lez v6, :cond_9

    iget-object v6, p0, Lcom/osp/app/signin/AccountView;->ak:Lcom/osp/app/signin/fv;

    invoke-virtual {v6}, Lcom/osp/app/signin/fv;->a()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v2, v6}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    :goto_2
    iget-object v6, p0, Lcom/osp/app/signin/AccountView;->ak:Lcom/osp/app/signin/fv;

    invoke-virtual {v6}, Lcom/osp/app/signin/fv;->b()Ljava/lang/String;

    move-result-object v6

    if-eqz v6, :cond_4

    iget-object v6, p0, Lcom/osp/app/signin/AccountView;->ak:Lcom/osp/app/signin/fv;

    invoke-virtual {v6}, Lcom/osp/app/signin/fv;->b()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/String;->length()I

    move-result v6

    if-lez v6, :cond_4

    iget-object v6, p0, Lcom/osp/app/signin/AccountView;->ak:Lcom/osp/app/signin/fv;

    invoke-virtual {v6}, Lcom/osp/app/signin/fv;->b()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v4, v6}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    :cond_4
    iget-object v4, p0, Lcom/osp/app/signin/AccountView;->ak:Lcom/osp/app/signin/fv;

    invoke-virtual {v4}, Lcom/osp/app/signin/fv;->k()Landroid/graphics/Bitmap;

    move-result-object v4

    if-eqz v4, :cond_6

    iget-object v4, p0, Lcom/osp/app/signin/AccountView;->ak:Lcom/osp/app/signin/fv;

    invoke-virtual {v4}, Lcom/osp/app/signin/fv;->k()Landroid/graphics/Bitmap;

    move-result-object v4

    if-eqz v5, :cond_6

    if-eqz v4, :cond_6

    invoke-virtual {v4}, Landroid/graphics/Bitmap;->isRecycled()Z

    move-result v6

    if-nez v6, :cond_6

    invoke-virtual {v5}, Landroid/widget/ImageView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v6

    iget v6, v6, Landroid/view/ViewGroup$LayoutParams;->width:I

    invoke-virtual {v4}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v7

    invoke-virtual {v4}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v10

    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    new-instance v11, Ljava/lang/StringBuilder;

    const-string v12, "view width : "

    invoke-direct {v11, v12}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v11, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-static {v11}, Lcom/osp/app/util/aq;->c(Ljava/lang/String;)V

    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    new-instance v11, Ljava/lang/StringBuilder;

    const-string v12, "bm width : "

    invoke-direct {v11, v12}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v11, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-static {v11}, Lcom/osp/app/util/aq;->c(Ljava/lang/String;)V

    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    new-instance v11, Ljava/lang/StringBuilder;

    const-string v12, "bm height : "

    invoke-direct {v11, v12}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v11, v10}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-static {v11}, Lcom/osp/app/util/aq;->c(Ljava/lang/String;)V

    if-lez v6, :cond_5

    if-lez v7, :cond_5

    if-lez v10, :cond_5

    mul-int/2addr v10, v6

    div-int v7, v10, v7

    invoke-virtual {v5}, Landroid/widget/ImageView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v10

    iput v6, v10, Landroid/view/ViewGroup$LayoutParams;->width:I

    iput v7, v10, Landroid/view/ViewGroup$LayoutParams;->height:I

    :cond_5
    invoke-virtual {v5, v4}, Landroid/widget/ImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    :cond_6
    new-instance v4, Landroid/graphics/Paint;

    invoke-direct {v4}, Landroid/graphics/Paint;-><init>()V

    invoke-virtual {v2}, Landroid/widget/TextView;->getTypeface()Landroid/graphics/Typeface;

    move-result-object v5

    invoke-virtual {v4, v5}, Landroid/graphics/Paint;->setTypeface(Landroid/graphics/Typeface;)Landroid/graphics/Typeface;

    invoke-virtual {v2}, Landroid/widget/TextView;->getTextSize()F

    move-result v5

    invoke-virtual {v4, v5}, Landroid/graphics/Paint;->setTextSize(F)V

    invoke-virtual {v2}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v5

    invoke-interface {v5}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Landroid/graphics/Paint;->measureText(Ljava/lang/String;)F

    move-result v4

    invoke-virtual {v2}, Landroid/widget/TextView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v5

    iget v5, v5, Landroid/view/ViewGroup$LayoutParams;->width:I

    if-lez v5, :cond_a

    invoke-virtual {v2}, Landroid/widget/TextView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v5

    iget v5, v5, Landroid/view/ViewGroup$LayoutParams;->width:I

    int-to-float v5, v5

    cmpg-float v4, v5, v4

    if-gez v4, :cond_a

    const/4 v4, 0x0

    const v5, 0x7f080105

    invoke-virtual {v8, v5}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v5

    invoke-virtual {v2, v4, v5}, Landroid/widget/TextView;->setTextSize(IF)V

    :goto_3
    invoke-virtual {v0}, Landroid/widget/RelativeLayout;->getParent()Landroid/view/ViewParent;

    move-result-object v2

    check-cast v2, Landroid/view/ViewGroup;

    invoke-virtual {v2, v0}, Landroid/view/ViewGroup;->removeView(Landroid/view/View;)V

    invoke-virtual {v1}, Landroid/widget/LinearLayout;->getParent()Landroid/view/ViewParent;

    move-result-object v2

    check-cast v2, Landroid/view/ViewGroup;

    invoke-virtual {v2, v1}, Landroid/view/ViewGroup;->removeView(Landroid/view/View;)V

    new-instance v2, Landroid/app/AlertDialog$Builder;

    invoke-direct {v2, p0}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    if-eqz v9, :cond_7

    invoke-virtual {v9}, Landroid/view/View;->getVisibility()I

    move-result v4

    const/16 v5, 0x8

    if-ne v4, v5, :cond_7

    const v4, 0x7f09016a

    new-instance v5, Lcom/osp/app/signin/ae;

    invoke-direct {v5, p0}, Lcom/osp/app/signin/ae;-><init>(Lcom/osp/app/signin/AccountView;)V

    invoke-virtual {v2, v4, v5}, Landroid/app/AlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v4

    const v5, 0x7f090066

    new-instance v6, Lcom/osp/app/signin/ad;

    invoke-direct {v6, p0}, Lcom/osp/app/signin/ad;-><init>(Lcom/osp/app/signin/AccountView;)V

    invoke-virtual {v4, v5, v6}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    :cond_7
    iget-object v4, p0, Lcom/osp/app/signin/AccountView;->ak:Lcom/osp/app/signin/fv;

    invoke-virtual {v4}, Lcom/osp/app/signin/fv;->a()Ljava/lang/String;

    move-result-object v4

    if-eqz v4, :cond_b

    iget-object v4, p0, Lcom/osp/app/signin/AccountView;->ak:Lcom/osp/app/signin/fv;

    invoke-virtual {v4}, Lcom/osp/app/signin/fv;->a()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/String;->length()I

    move-result v4

    if-lez v4, :cond_b

    iget-object v4, p0, Lcom/osp/app/signin/AccountView;->ak:Lcom/osp/app/signin/fv;

    invoke-virtual {v4}, Lcom/osp/app/signin/fv;->a()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v4}, Landroid/app/AlertDialog$Builder;->setTitle(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    :goto_4
    invoke-virtual {v2, v0}, Landroid/app/AlertDialog$Builder;->setCustomTitle(Landroid/view/View;)Landroid/app/AlertDialog$Builder;

    invoke-virtual {v2, v1}, Landroid/app/AlertDialog$Builder;->setView(Landroid/view/View;)Landroid/app/AlertDialog$Builder;

    invoke-virtual {v2}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/AlertDialog;->getWindow()Landroid/view/Window;

    move-result-object v1

    invoke-virtual {v1}, Landroid/view/Window;->getAttributes()Landroid/view/WindowManager$LayoutParams;

    move-result-object v1

    const v2, 0x7f080101

    invoke-virtual {v8, v2}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v2

    float-to-int v2, v2

    iput v2, v1, Landroid/view/WindowManager$LayoutParams;->width:I

    invoke-virtual {v0}, Landroid/app/AlertDialog;->getWindow()Landroid/view/Window;

    move-result-object v2

    invoke-virtual {v2, v1}, Landroid/view/Window;->setAttributes(Landroid/view/WindowManager$LayoutParams;)V

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog;->setCanceledOnTouchOutside(Z)V

    new-instance v1, Lcom/osp/app/signin/af;

    invoke-direct {v1, p0, v0}, Lcom/osp/app/signin/af;-><init>(Lcom/osp/app/signin/AccountView;Landroid/app/AlertDialog;)V

    invoke-virtual {v3, v1}, Landroid/widget/LinearLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    new-instance v1, Lcom/osp/app/signin/ag;

    invoke-direct {v1, p0}, Lcom/osp/app/signin/ag;-><init>(Lcom/osp/app/signin/AccountView;)V

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog;->setOnCancelListener(Landroid/content/DialogInterface$OnCancelListener;)V

    new-instance v1, Lcom/osp/app/signin/ah;

    invoke-direct {v1, p0}, Lcom/osp/app/signin/ah;-><init>(Lcom/osp/app/signin/AccountView;)V

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog;->setOnDismissListener(Landroid/content/DialogInterface$OnDismissListener;)V

    goto/16 :goto_0

    :cond_8
    invoke-virtual {p0}, Lcom/osp/app/signin/AccountView;->getResources()Landroid/content/res/Resources;

    move-result-object v6

    const v9, 0x7f080103

    invoke-virtual {v6, v9}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v6

    float-to-int v6, v6

    const/4 v9, 0x0

    const/4 v10, 0x0

    const/4 v11, 0x0

    invoke-static {v0, v6, v9, v10, v11}, Lcom/msc/sa/c/d;->a(Landroid/view/View;IIII)V

    invoke-virtual {v3}, Landroid/widget/LinearLayout;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v6

    invoke-virtual {v4}, Landroid/widget/ImageView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v9

    iget v9, v9, Landroid/view/ViewGroup$LayoutParams;->width:I

    int-to-float v9, v9

    invoke-virtual {p0}, Lcom/osp/app/signin/AccountView;->getResources()Landroid/content/res/Resources;

    move-result-object v10

    const v11, 0x7f08006c

    invoke-virtual {v10, v11}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v10

    const/high16 v11, 0x40000000    # 2.0f

    mul-float/2addr v10, v11

    add-float/2addr v9, v10

    float-to-int v9, v9

    iput v9, v6, Landroid/view/ViewGroup$LayoutParams;->width:I

    invoke-virtual {v3, v6}, Landroid/widget/LinearLayout;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    invoke-virtual {p0}, Lcom/osp/app/signin/AccountView;->getResources()Landroid/content/res/Resources;

    move-result-object v6

    const v9, 0x7f08006c

    invoke-virtual {v6, v9}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v6

    float-to-int v6, v6

    const/4 v9, 0x0

    const/4 v10, 0x0

    invoke-static {v3, v6, v9, v6, v10}, Lcom/msc/sa/c/d;->a(Landroid/view/View;IIII)V

    const v6, 0x7f020020

    invoke-static {v6}, Lcom/osp/app/util/an;->a(I)I

    move-result v6

    invoke-virtual {v3, v6}, Landroid/widget/LinearLayout;->setBackgroundResource(I)V

    const/4 v6, 0x0

    invoke-virtual {v5, v6}, Landroid/view/View;->setVisibility(I)V

    const v6, 0x7f02012a

    invoke-static {v6}, Lcom/osp/app/util/an;->a(I)I

    move-result v6

    invoke-virtual {v5, v6}, Landroid/view/View;->setBackgroundResource(I)V

    const v5, 0x7f020041

    invoke-static {v5}, Lcom/osp/app/util/an;->a(I)I

    move-result v5

    invoke-virtual {v4, v5}, Landroid/widget/ImageView;->setImageResource(I)V

    goto/16 :goto_1

    :cond_9
    const v6, 0x7f09004f

    invoke-virtual {v2, v6}, Landroid/widget/TextView;->setText(I)V

    goto/16 :goto_2

    :cond_a
    const/4 v4, 0x0

    const v5, 0x7f080104

    invoke-virtual {v8, v5}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v5

    invoke-virtual {v2, v4, v5}, Landroid/widget/TextView;->setTextSize(IF)V

    goto/16 :goto_3

    :cond_b
    const v4, 0x7f09004f

    invoke-virtual {v2, v4}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    goto/16 :goto_4

    .line 8924
    :pswitch_1
    const/16 v0, 0x12e

    invoke-direct {p0, v0}, Lcom/osp/app/signin/AccountView;->f(I)Landroid/app/Dialog;

    move-result-object v0

    goto/16 :goto_0

    .line 8927
    :pswitch_2
    const/16 v0, 0x12f

    invoke-direct {p0, v0}, Lcom/osp/app/signin/AccountView;->f(I)Landroid/app/Dialog;

    move-result-object v0

    goto/16 :goto_0

    .line 8930
    :pswitch_3
    const/16 v0, 0x130

    invoke-direct {p0, v0}, Lcom/osp/app/signin/AccountView;->f(I)Landroid/app/Dialog;

    move-result-object v0

    goto/16 :goto_0

    .line 8933
    :pswitch_4
    const/16 v0, 0x131

    invoke-direct {p0, v0}, Lcom/osp/app/signin/AccountView;->f(I)Landroid/app/Dialog;

    move-result-object v0

    goto/16 :goto_0

    .line 8936
    :pswitch_5
    const/16 v0, 0x132

    invoke-direct {p0, v0}, Lcom/osp/app/signin/AccountView;->f(I)Landroid/app/Dialog;

    move-result-object v0

    goto/16 :goto_0

    .line 8939
    :pswitch_6
    const/4 v0, 0x0

    goto/16 :goto_0

    .line 8942
    :pswitch_7
    const/16 v0, 0x135

    invoke-direct {p0, v0}, Lcom/osp/app/signin/AccountView;->f(I)Landroid/app/Dialog;

    move-result-object v0

    goto/16 :goto_0

    .line 8918
    nop

    :pswitch_data_0
    .packed-switch 0x12d
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_6
        :pswitch_7
    .end packed-switch
.end method

.method protected onDestroy()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    const/4 v2, 0x0

    .line 3945
    invoke-super {p0}, Lcom/osp/app/util/BaseActivity;->onDestroy()V

    .line 3947
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v0, "AccountView::onDestroy"

    invoke-static {v0}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;)V

    .line 3949
    iget-object v0, p0, Lcom/osp/app/signin/AccountView;->aD:Landroid/app/AlertDialog;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/osp/app/signin/AccountView;->aD:Landroid/app/AlertDialog;

    invoke-virtual {v0}, Landroid/app/AlertDialog;->isShowing()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 3951
    iget-object v0, p0, Lcom/osp/app/signin/AccountView;->aD:Landroid/app/AlertDialog;

    invoke-virtual {v0}, Landroid/app/AlertDialog;->dismiss()V

    .line 3959
    :cond_0
    iget-object v0, p0, Lcom/osp/app/signin/AccountView;->E:Lcom/osp/app/signin/be;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/osp/app/signin/AccountView;->E:Lcom/osp/app/signin/be;

    invoke-virtual {v0}, Lcom/osp/app/signin/be;->getStatus()Landroid/os/AsyncTask$Status;

    move-result-object v0

    sget-object v1, Landroid/os/AsyncTask$Status;->RUNNING:Landroid/os/AsyncTask$Status;

    if-ne v0, v1, :cond_1

    .line 3961
    iget-object v0, p0, Lcom/osp/app/signin/AccountView;->E:Lcom/osp/app/signin/be;

    invoke-virtual {v0}, Lcom/osp/app/signin/be;->d()V

    .line 3962
    iput-object v2, p0, Lcom/osp/app/signin/AccountView;->E:Lcom/osp/app/signin/be;

    .line 3965
    :cond_1
    iget-object v0, p0, Lcom/osp/app/signin/AccountView;->z:Lcom/osp/app/signin/ax;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/osp/app/signin/AccountView;->z:Lcom/osp/app/signin/ax;

    invoke-virtual {v0}, Lcom/osp/app/signin/ax;->getStatus()Landroid/os/AsyncTask$Status;

    move-result-object v0

    sget-object v1, Landroid/os/AsyncTask$Status;->RUNNING:Landroid/os/AsyncTask$Status;

    if-ne v0, v1, :cond_2

    .line 3967
    iget-object v0, p0, Lcom/osp/app/signin/AccountView;->z:Lcom/osp/app/signin/ax;

    invoke-virtual {v0}, Lcom/osp/app/signin/ax;->d()V

    .line 3968
    iput-object v2, p0, Lcom/osp/app/signin/AccountView;->z:Lcom/osp/app/signin/ax;

    .line 3971
    :cond_2
    iget-object v0, p0, Lcom/osp/app/signin/AccountView;->D:Lcom/osp/app/signin/bd;

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/osp/app/signin/AccountView;->D:Lcom/osp/app/signin/bd;

    invoke-virtual {v0}, Lcom/osp/app/signin/bd;->getStatus()Landroid/os/AsyncTask$Status;

    move-result-object v0

    sget-object v1, Landroid/os/AsyncTask$Status;->RUNNING:Landroid/os/AsyncTask$Status;

    if-ne v0, v1, :cond_3

    .line 3973
    iget-object v0, p0, Lcom/osp/app/signin/AccountView;->D:Lcom/osp/app/signin/bd;

    invoke-virtual {v0}, Lcom/osp/app/signin/bd;->d()V

    .line 3974
    iput-object v2, p0, Lcom/osp/app/signin/AccountView;->D:Lcom/osp/app/signin/bd;

    .line 3976
    :cond_3
    iget-object v0, p0, Lcom/osp/app/signin/AccountView;->as:Lcom/osp/app/signin/bc;

    if-eqz v0, :cond_4

    iget-object v0, p0, Lcom/osp/app/signin/AccountView;->as:Lcom/osp/app/signin/bc;

    invoke-virtual {v0}, Lcom/osp/app/signin/bc;->getStatus()Landroid/os/AsyncTask$Status;

    move-result-object v0

    sget-object v1, Landroid/os/AsyncTask$Status;->RUNNING:Landroid/os/AsyncTask$Status;

    if-ne v0, v1, :cond_4

    .line 3978
    iget-object v0, p0, Lcom/osp/app/signin/AccountView;->as:Lcom/osp/app/signin/bc;

    invoke-virtual {v0}, Lcom/osp/app/signin/bc;->d()V

    .line 3979
    iput-object v2, p0, Lcom/osp/app/signin/AccountView;->as:Lcom/osp/app/signin/bc;

    .line 3982
    :cond_4
    iget-object v0, p0, Lcom/osp/app/signin/AccountView;->av:Lcom/osp/app/signin/az;

    if-eqz v0, :cond_5

    iget-object v0, p0, Lcom/osp/app/signin/AccountView;->av:Lcom/osp/app/signin/az;

    invoke-virtual {v0}, Lcom/osp/app/signin/az;->getStatus()Landroid/os/AsyncTask$Status;

    move-result-object v0

    sget-object v1, Landroid/os/AsyncTask$Status;->RUNNING:Landroid/os/AsyncTask$Status;

    if-ne v0, v1, :cond_5

    .line 3984
    iget-object v0, p0, Lcom/osp/app/signin/AccountView;->av:Lcom/osp/app/signin/az;

    invoke-virtual {v0}, Lcom/osp/app/signin/az;->d()V

    .line 3985
    iput-object v2, p0, Lcom/osp/app/signin/AccountView;->av:Lcom/osp/app/signin/az;

    .line 3988
    :cond_5
    iget-object v0, p0, Lcom/osp/app/signin/AccountView;->ab:Landroid/app/ProgressDialog;

    if-eqz v0, :cond_6

    iget-object v0, p0, Lcom/osp/app/signin/AccountView;->ab:Landroid/app/ProgressDialog;

    invoke-virtual {v0}, Landroid/app/ProgressDialog;->isShowing()Z

    move-result v0

    if-eqz v0, :cond_6

    .line 3990
    iget-object v0, p0, Lcom/osp/app/signin/AccountView;->ab:Landroid/app/ProgressDialog;

    invoke-virtual {v0}, Landroid/app/ProgressDialog;->dismiss()V

    .line 3991
    iput-object v2, p0, Lcom/osp/app/signin/AccountView;->ab:Landroid/app/ProgressDialog;

    .line 3994
    :cond_6
    iget-object v0, p0, Lcom/osp/app/signin/AccountView;->aE:Lcom/osp/app/signin/bb;

    if-eqz v0, :cond_7

    .line 3996
    iget-object v0, p0, Lcom/osp/app/signin/AccountView;->aE:Lcom/osp/app/signin/bb;

    invoke-virtual {v0, v3}, Lcom/osp/app/signin/bb;->removeMessages(I)V

    .line 3997
    iget-object v0, p0, Lcom/osp/app/signin/AccountView;->aE:Lcom/osp/app/signin/bb;

    invoke-virtual {v0, v2}, Lcom/osp/app/signin/bb;->removeCallbacksAndMessages(Ljava/lang/Object;)V

    .line 4000
    :cond_7
    iget-object v0, p0, Lcom/osp/app/signin/AccountView;->aH:Ljava/util/Timer;

    if-eqz v0, :cond_8

    .line 4002
    iget-object v0, p0, Lcom/osp/app/signin/AccountView;->aH:Ljava/util/Timer;

    invoke-virtual {v0}, Ljava/util/Timer;->cancel()V

    .line 4003
    iput-object v2, p0, Lcom/osp/app/signin/AccountView;->aH:Ljava/util/Timer;

    .line 4031
    :cond_8
    invoke-static {}, Lcom/msc/sa/selfupdate/c;->b()Lcom/msc/sa/selfupdate/c;

    move-result-object v0

    invoke-virtual {v0}, Lcom/msc/sa/selfupdate/c;->d()V

    .line 4032
    invoke-virtual {p0}, Lcom/osp/app/signin/AccountView;->getApplication()Landroid/app/Application;

    move-result-object v0

    check-cast v0, Lcom/osp/app/signin/SamsungService;

    invoke-virtual {v0}, Lcom/osp/app/signin/SamsungService;->o()V

    .line 4037
    invoke-direct {p0}, Lcom/osp/app/signin/AccountView;->B()V

    .line 4039
    invoke-static {}, Lcom/osp/app/signin/SamsungService;->a()Z

    move-result v0

    if-nez v0, :cond_9

    invoke-static {}, Lcom/osp/device/b;->b()Lcom/osp/device/b;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/osp/device/b;->i(Landroid/content/Context;)Z

    move-result v0

    if-nez v0, :cond_9

    .line 4041
    invoke-static {p0, v3}, Lcom/msc/sa/c/d;->a(Landroid/content/Context;Z)V

    .line 4044
    :cond_9
    return-void
.end method

.method protected onPause()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 4065
    invoke-super {p0}, Lcom/osp/app/util/BaseActivity;->onPause()V

    .line 4067
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v0, "AccountView::onPause"

    invoke-static {v0}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;)V

    .line 4069
    const v0, 0x7f0c0044

    invoke-virtual {p0, v0}, Lcom/osp/app/signin/AccountView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 4070
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Landroid/widget/TextView;->isSelected()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 4072
    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setSoundEffectsEnabled(Z)V

    .line 4073
    invoke-virtual {v0, v2}, Landroid/widget/TextView;->playSoundEffect(I)V

    .line 4074
    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setSelected(Z)V

    .line 4076
    :cond_0
    return-void
.end method

.method protected onRestoreInstanceState(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 4211
    invoke-super {p0, p1}, Lcom/osp/app/util/BaseActivity;->onRestoreInstanceState(Landroid/os/Bundle;)V

    .line 4213
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v0, "AV"

    const-string v1, "onRestoreInstanceState"

    invoke-static {v0, v1}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 4215
    if-eqz p1, :cond_0

    .line 4217
    const-string v0, "RESULT_CHECK_LIST"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getBundle(Ljava/lang/String;)Landroid/os/Bundle;

    move-result-object v0

    .line 4218
    if-eqz v0, :cond_0

    .line 4220
    invoke-static {v0}, Lcom/msc/a/g;->a(Landroid/os/Bundle;)Lcom/msc/a/g;

    move-result-object v0

    iput-object v0, p0, Lcom/osp/app/signin/AccountView;->M:Lcom/msc/a/g;

    .line 4235
    :cond_0
    return-void
.end method

.method protected onResume()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 4080
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v0, "AV"

    const-string v1, "onResume START"

    invoke-static {v0, v1}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 4082
    invoke-super {p0}, Lcom/osp/app/util/BaseActivity;->onResume()V

    .line 4084
    invoke-static {}, Lcom/osp/app/signin/SamsungService;->a()Z

    move-result v0

    if-nez v0, :cond_0

    invoke-static {}, Lcom/osp/device/b;->b()Lcom/osp/device/b;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/osp/device/b;->i(Landroid/content/Context;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 4086
    invoke-static {p0, v3}, Lcom/msc/sa/c/d;->a(Landroid/content/Context;Z)V

    .line 4089
    :cond_0
    iput-boolean v2, p0, Lcom/osp/app/signin/AccountView;->P:Z

    .line 4090
    iput-boolean v2, p0, Lcom/osp/app/signin/AccountView;->Q:Z

    .line 4091
    iput-boolean v2, p0, Lcom/osp/app/signin/AccountView;->ar:Z

    .line 4098
    iget v0, p0, Lcom/osp/app/signin/AccountView;->V:I

    if-nez v0, :cond_2

    invoke-static {p0}, Lcom/osp/app/util/ad;->d(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 4110
    iget-boolean v0, p0, Lcom/osp/app/signin/AccountView;->U:Z

    if-nez v0, :cond_1

    .line 4112
    iget-object v0, p0, Lcom/osp/app/signin/AccountView;->J:Landroid/content/Intent;

    const/4 v1, 0x0

    invoke-direct {p0, v3, v0, v1}, Lcom/osp/app/signin/AccountView;->a(ILandroid/content/Intent;Landroid/content/Intent;)V

    .line 4113
    invoke-virtual {p0}, Lcom/osp/app/signin/AccountView;->finish()V

    .line 4157
    :goto_0
    return-void

    .line 4116
    :cond_1
    iput-boolean v2, p0, Lcom/osp/app/signin/AccountView;->U:Z

    goto :goto_0

    .line 4121
    :cond_2
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "AccountView::onResume isShowSigninViewFromSignup : "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-boolean v1, p0, Lcom/osp/app/signin/AccountView;->A:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/osp/app/util/aq;->c(Ljava/lang/String;)V

    .line 4123
    iget-boolean v0, p0, Lcom/osp/app/signin/AccountView;->A:Z

    if-eqz v0, :cond_3

    .line 4125
    iget-object v0, p0, Lcom/osp/app/signin/AccountView;->J:Landroid/content/Intent;

    const-string v1, "com.msc.action.samsungaccount.SIGNIN_POPUP"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 4127
    invoke-static {}, Lcom/osp/app/signin/SamsungService;->a()Z

    move-result v0

    if-eqz v0, :cond_5

    .line 4129
    iget-object v0, p0, Lcom/osp/app/signin/AccountView;->O:Ljava/lang/String;

    invoke-direct {p0, v0, v3}, Lcom/osp/app/signin/AccountView;->b(Ljava/lang/String;Z)V

    .line 4136
    :cond_3
    :goto_1
    iput-boolean v2, p0, Lcom/osp/app/signin/AccountView;->B:Z

    .line 4140
    :try_start_0
    invoke-virtual {p0}, Lcom/osp/app/signin/AccountView;->getWindow()Landroid/view/Window;

    move-result-object v0

    if-eqz v0, :cond_4

    invoke-virtual {p0}, Lcom/osp/app/signin/AccountView;->getWindow()Landroid/view/Window;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/Window;->getDecorView()Landroid/view/View;

    move-result-object v0

    if-eqz v0, :cond_4

    .line 4142
    const v0, 0x7f0c0044

    invoke-virtual {p0, v0}, Lcom/osp/app/signin/AccountView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 4143
    if-eqz v0, :cond_4

    .line 4145
    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setSoundEffectsEnabled(Z)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 4153
    :cond_4
    :goto_2
    iput-boolean v2, p0, Lcom/osp/app/signin/AccountView;->af:Z

    .line 4155
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v0, "AV"

    const-string v1, "onResume END"

    invoke-static {v0, v1}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 4132
    :cond_5
    iget-object v0, p0, Lcom/osp/app/signin/AccountView;->O:Ljava/lang/String;

    invoke-direct {p0, v0, v2}, Lcom/osp/app/signin/AccountView;->b(Ljava/lang/String;Z)V

    goto :goto_1

    :catch_0
    move-exception v0

    goto :goto_2
.end method

.method protected onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 4181
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v0, "AV"

    const-string v1, "onSaveInstanceState"

    invoke-static {v0, v1}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 4182
    if-eqz p1, :cond_2

    .line 4184
    iget-object v0, p0, Lcom/osp/app/signin/AccountView;->M:Lcom/msc/a/g;

    if-eqz v0, :cond_0

    .line 4186
    const-string v0, "RESULT_CHECK_LIST"

    iget-object v1, p0, Lcom/osp/app/signin/AccountView;->M:Lcom/msc/a/g;

    invoke-virtual {v1}, Lcom/msc/a/g;->l()Landroid/os/Bundle;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putBundle(Ljava/lang/String;Landroid/os/Bundle;)V

    .line 4199
    :cond_0
    invoke-static {p0}, Lcom/osp/app/util/ad;->d(Landroid/content/Context;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 4201
    const-string v0, "is_destoryed_during_signin"

    const/4 v1, 0x1

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 4202
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v0, "AV"

    const-string v1, "save - destroyed during signin"

    invoke-static {v0, v1}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 4205
    :cond_1
    invoke-super {p0, p1}, Lcom/osp/app/util/BaseActivity;->onSaveInstanceState(Landroid/os/Bundle;)V

    .line 4207
    :cond_2
    return-void
.end method

.method protected onStop()V
    .locals 1

    .prologue
    .line 4161
    invoke-super {p0}, Lcom/osp/app/util/BaseActivity;->onStop()V

    .line 4163
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v0, "AccountView::onStop"

    invoke-static {v0}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;)V

    .line 4177
    return-void
.end method
