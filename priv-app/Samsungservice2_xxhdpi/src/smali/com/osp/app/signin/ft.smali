.class final Lcom/osp/app/signin/ft;
.super Lcom/msc/c/b;
.source "InterfaceService.java"


# instance fields
.field final synthetic c:Lcom/osp/app/signin/InterfaceService;

.field private final d:I

.field private e:J

.field private final f:Ljava/lang/String;

.field private g:Ljava/lang/String;

.field private h:Ljava/lang/String;

.field private final i:Ljava/lang/String;

.field private j:I

.field private k:I

.field private l:Ljava/lang/String;

.field private m:Ljava/lang/String;

.field private n:Z

.field private o:Ljava/lang/String;

.field private p:Landroid/content/Intent;

.field private final q:Ljava/lang/String;

.field private r:Ljava/lang/String;


# direct methods
.method public constructor <init>(Lcom/osp/app/signin/InterfaceService;Landroid/content/Intent;I)V
    .locals 4

    .prologue
    const/4 v3, 0x0

    const/16 v2, -0x3e7

    .line 1446
    iput-object p1, p0, Lcom/osp/app/signin/ft;->c:Lcom/osp/app/signin/InterfaceService;

    .line 1448
    invoke-direct {p0, p1}, Lcom/msc/c/b;-><init>(Landroid/content/Context;)V

    .line 1420
    const-string v0, "j5p7ll8g33"

    iput-object v0, p0, Lcom/osp/app/signin/ft;->g:Ljava/lang/String;

    .line 1421
    const-string v0, "5763D0052DC1462E13751F753384E9A9"

    iput-object v0, p0, Lcom/osp/app/signin/ft;->h:Ljava/lang/String;

    .line 1422
    invoke-static {}, Lcom/osp/common/util/i;->a()Lcom/osp/common/util/i;

    iget-object v0, p0, Lcom/osp/app/signin/ft;->c:Lcom/osp/app/signin/InterfaceService;

    invoke-static {v0}, Lcom/osp/common/util/i;->l(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v0

    sget-object v1, Ljava/util/Locale;->ENGLISH:Ljava/util/Locale;

    invoke-virtual {v0, v1}, Ljava/lang/String;->toUpperCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/osp/app/signin/ft;->i:Ljava/lang/String;

    .line 1424
    iput v2, p0, Lcom/osp/app/signin/ft;->j:I

    .line 1425
    iput v2, p0, Lcom/osp/app/signin/ft;->k:I

    .line 1426
    iput-object v3, p0, Lcom/osp/app/signin/ft;->l:Ljava/lang/String;

    .line 1428
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/osp/app/signin/ft;->n:Z

    .line 1429
    iput-object v3, p0, Lcom/osp/app/signin/ft;->o:Ljava/lang/String;

    .line 1450
    iput p3, p0, Lcom/osp/app/signin/ft;->d:I

    .line 1452
    const-string v0, "mypackage"

    invoke-virtual {p2, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/osp/app/signin/ft;->f:Ljava/lang/String;

    .line 1454
    const-string v0, "client_id"

    invoke-virtual {p2, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/osp/app/signin/ft;->g:Ljava/lang/String;

    .line 1455
    const-string v0, "client_secret"

    invoke-virtual {p2, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/osp/app/signin/ft;->h:Ljava/lang/String;

    .line 1456
    const-string v0, "MODE"

    invoke-virtual {p2, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/osp/app/signin/ft;->q:Ljava/lang/String;

    .line 1457
    return-void
.end method


# virtual methods
.method protected final varargs a()Ljava/lang/Boolean;
    .locals 3

    .prologue
    .line 1468
    iget-object v0, p0, Lcom/osp/app/signin/ft;->c:Lcom/osp/app/signin/InterfaceService;

    invoke-static {v0}, Lcom/osp/app/util/ad;->p(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/osp/app/signin/ft;->r:Ljava/lang/String;

    .line 1470
    new-instance v0, Lcom/msc/a/h;

    invoke-direct {v0}, Lcom/msc/a/h;-><init>()V

    .line 1471
    iget-object v1, p0, Lcom/osp/app/signin/ft;->c:Lcom/osp/app/signin/InterfaceService;

    invoke-static {v1}, Lcom/osp/app/util/ad;->h(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/msc/a/h;->d(Ljava/lang/String;)V

    .line 1472
    invoke-virtual {v0}, Lcom/msc/a/h;->b()V

    .line 1475
    iget-object v1, p0, Lcom/osp/app/signin/ft;->g:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/msc/a/h;->h(Ljava/lang/String;)V

    .line 1476
    iget-object v1, p0, Lcom/osp/app/signin/ft;->f:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/msc/a/h;->i(Ljava/lang/String;)V

    .line 1477
    iget-object v1, p0, Lcom/osp/app/signin/ft;->g:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/msc/a/h;->j(Ljava/lang/String;)V

    .line 1478
    iget-object v1, p0, Lcom/osp/app/signin/ft;->i:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/msc/a/h;->e(Ljava/lang/String;)V

    .line 1479
    iget-object v1, p0, Lcom/osp/app/signin/ft;->r:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/msc/a/h;->k(Ljava/lang/String;)V

    .line 1480
    const-string v1, "DISCLAIMER_CHECK"

    iget-object v2, p0, Lcom/osp/app/signin/ft;->q:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 1482
    invoke-virtual {v0}, Lcom/msc/a/h;->c()V

    .line 1484
    :cond_0
    invoke-static {}, Lcom/osp/device/b;->b()Lcom/osp/device/b;

    move-result-object v1

    invoke-virtual {v1}, Lcom/osp/device/b;->a()Z

    move-result v1

    if-eqz v1, :cond_1

    iget-object v1, p0, Lcom/osp/app/signin/ft;->c:Lcom/osp/app/signin/InterfaceService;

    invoke-static {v1}, Lcom/osp/app/util/ad;->h(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Landroid/telephony/PhoneNumberUtils;->isGlobalPhoneNumber(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 1486
    iget-object v1, p0, Lcom/osp/app/signin/ft;->c:Lcom/osp/app/signin/InterfaceService;

    invoke-static {v1}, Lcom/msc/c/e;->h(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v1

    .line 1487
    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_1

    .line 1489
    const-string v2, "Y"

    invoke-virtual {v0, v2}, Lcom/msc/a/h;->b(Ljava/lang/String;)V

    .line 1490
    invoke-virtual {v0, v1}, Lcom/msc/a/h;->c(Ljava/lang/String;)V

    .line 1494
    :cond_1
    invoke-static {}, Lcom/msc/c/g;->a()Lcom/msc/c/g;

    iget-object v1, p0, Lcom/osp/app/signin/ft;->c:Lcom/osp/app/signin/InterfaceService;

    invoke-static {v1, v0, p0}, Lcom/msc/c/g;->a(Landroid/content/Context;Lcom/msc/a/h;Lcom/msc/b/h;)J

    move-result-wide v0

    iput-wide v0, p0, Lcom/osp/app/signin/ft;->e:J

    iget-wide v0, p0, Lcom/osp/app/signin/ft;->e:J

    invoke-virtual {p0, v0, v1}, Lcom/osp/app/signin/ft;->a(J)V

    iget-wide v0, p0, Lcom/osp/app/signin/ft;->e:J

    const-string v2, "from_xml"

    invoke-virtual {p0, v0, v1, v2}, Lcom/osp/app/signin/ft;->a(JLjava/lang/String;)V

    iget-wide v0, p0, Lcom/osp/app/signin/ft;->e:J

    sget-object v2, Lcom/msc/b/g;->c:Lcom/msc/b/g;

    invoke-static {v0, v1, v2}, Lcom/msc/c/g;->a(JLcom/msc/b/g;)V

    .line 1496
    const/4 v0, 0x0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    return-object v0
.end method

.method public final declared-synchronized a(Lcom/msc/b/c;)V
    .locals 8

    .prologue
    .line 1554
    monitor-enter p0

    :try_start_0
    invoke-super {p0, p1}, Lcom/msc/c/b;->a(Lcom/msc/b/c;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1556
    if-nez p1, :cond_1

    .line 1610
    :cond_0
    :goto_0
    monitor-exit p0

    return-void

    .line 1561
    :cond_1
    :try_start_1
    invoke-virtual {p1}, Lcom/msc/b/c;->a()J

    move-result-wide v0

    .line 1562
    invoke-virtual {p1}, Lcom/msc/b/c;->e()Ljava/lang/String;

    move-result-object v2

    .line 1564
    iget-wide v4, p0, Lcom/osp/app/signin/ft;->e:J
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    cmp-long v0, v0, v4

    if-nez v0, :cond_0

    .line 1569
    :try_start_2
    new-instance v3, Lcom/msc/a/i;

    invoke-direct {v3}, Lcom/msc/a/i;-><init>()V

    .line 1571
    invoke-static {}, Lcom/msc/c/h;->a()Lcom/msc/c/h;

    iget-object v0, p0, Lcom/osp/app/signin/ft;->c:Lcom/osp/app/signin/InterfaceService;

    iget-object v1, p0, Lcom/osp/app/signin/ft;->g:Ljava/lang/String;

    invoke-static {v0, v1, v2, v3}, Lcom/msc/c/h;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Lcom/msc/a/i;)V

    .line 1573
    const/4 v0, -0x1

    iput v0, p0, Lcom/osp/app/signin/ft;->j:I

    .line 1574
    const-string v0, "DISCLAIMER_CHECK"

    iget-object v1, p0, Lcom/osp/app/signin/ft;->q:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 1576
    iget-object v0, p0, Lcom/osp/app/signin/ft;->c:Lcom/osp/app/signin/InterfaceService;

    invoke-static {v3}, Lcom/osp/app/signin/InterfaceService;->b(Lcom/msc/a/i;)I

    move-result v0

    iput v0, p0, Lcom/osp/app/signin/ft;->k:I

    .line 1577
    iget v0, p0, Lcom/osp/app/signin/ft;->k:I

    if-nez v0, :cond_0

    .line 1579
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/osp/app/signin/ft;->n:Z
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_0
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0

    .line 1605
    :catch_0
    move-exception v0

    :try_start_3
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    goto :goto_0

    .line 1554
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 1584
    :cond_2
    :try_start_4
    iget-object v0, p0, Lcom/osp/app/signin/ft;->c:Lcom/osp/app/signin/InterfaceService;

    invoke-static {v3}, Lcom/osp/app/signin/InterfaceService;->a(Lcom/msc/a/i;)I

    move-result v0

    iput v0, p0, Lcom/osp/app/signin/ft;->k:I

    .line 1587
    iget v0, p0, Lcom/osp/app/signin/ft;->k:I

    if-lez v0, :cond_4

    .line 1589
    iget-object v0, p0, Lcom/osp/app/signin/ft;->c:Lcom/osp/app/signin/InterfaceService;

    iget v1, p0, Lcom/osp/app/signin/ft;->k:I

    invoke-virtual {v3}, Lcom/msc/a/i;->b()Lcom/msc/a/g;

    move-result-object v2

    invoke-virtual {v2}, Lcom/msc/a/g;->b()Ljava/lang/String;

    move-result-object v2

    iget-object v3, p0, Lcom/osp/app/signin/ft;->g:Ljava/lang/String;

    iget-object v4, p0, Lcom/osp/app/signin/ft;->h:Ljava/lang/String;

    const/4 v5, 0x0

    const/4 v6, 0x1

    iget-object v7, p0, Lcom/osp/app/signin/ft;->r:Ljava/lang/String;

    invoke-static/range {v0 .. v7}, Lcom/osp/app/util/ad;->a(Landroid/content/Context;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;ZZLjava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    iput-object v0, p0, Lcom/osp/app/signin/ft;->p:Landroid/content/Intent;

    .line 1591
    invoke-static {}, Lcom/osp/device/b;->b()Lcom/osp/device/b;

    move-result-object v0

    invoke-virtual {v0}, Lcom/osp/device/b;->l()I

    move-result v0

    const/16 v1, 0xa

    if-le v0, v1, :cond_3

    .line 1593
    iget-object v0, p0, Lcom/osp/app/signin/ft;->p:Landroid/content/Intent;

    const v1, 0x8000

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    .line 1596
    :cond_3
    iget-object v0, p0, Lcom/osp/app/signin/ft;->p:Landroid/content/Intent;

    invoke-virtual {v0}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/osp/app/signin/ft;->o:Ljava/lang/String;

    .line 1598
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "REQUIRED_PROCESS_ACTION : "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v1, p0, Lcom/osp/app/signin/ft;->o:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;)V

    goto/16 :goto_0

    .line 1602
    :cond_4
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/osp/app/signin/ft;->n:Z
    :try_end_4
    .catch Ljava/lang/Exception; {:try_start_4 .. :try_end_4} :catch_0
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    goto/16 :goto_0
.end method

.method protected final a(Ljava/lang/Boolean;)V
    .locals 4

    .prologue
    .line 1502
    invoke-static {}, Lcom/osp/device/b;->b()Lcom/osp/device/b;

    move-result-object v0

    invoke-virtual {v0}, Lcom/osp/device/b;->a()Z

    move-result v0

    if-eqz v0, :cond_1

    const-string v0, "USR_3174"

    iget-object v1, p0, Lcom/osp/app/signin/ft;->m:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1504
    iget-object v0, p0, Lcom/osp/app/signin/ft;->c:Lcom/osp/app/signin/InterfaceService;

    iget-object v1, p0, Lcom/osp/app/signin/ft;->g:Ljava/lang/String;

    iget-object v2, p0, Lcom/osp/app/signin/ft;->h:Ljava/lang/String;

    const-string v3, "OSP_02"

    invoke-static {v0, v1, v2, v3}, Lcom/osp/app/signin/InterfaceService;->a(Lcom/osp/app/signin/InterfaceService;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 1522
    :cond_0
    :goto_0
    iget-object v0, p0, Lcom/osp/app/signin/ft;->p:Landroid/content/Intent;

    .line 1524
    const-string v1, "com.msc.action.VALIDATION_CHECK_RESPONSE"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 1525
    iget-object v1, p0, Lcom/osp/app/signin/ft;->f:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setPackage(Ljava/lang/String;)Landroid/content/Intent;

    .line 1526
    const-string v1, "client_id"

    iget-object v2, p0, Lcom/osp/app/signin/ft;->g:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 1527
    const-string v1, "client_secret"

    iget-object v2, p0, Lcom/osp/app/signin/ft;->h:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 1528
    const-string v1, "result_code"

    iget v2, p0, Lcom/osp/app/signin/ft;->j:I

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 1529
    const-string v1, "check_list"

    iget v2, p0, Lcom/osp/app/signin/ft;->k:I

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 1530
    const-string v1, "error_message"

    iget-object v2, p0, Lcom/osp/app/signin/ft;->l:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 1531
    const-string v1, "validation_result"

    iget-boolean v2, p0, Lcom/osp/app/signin/ft;->n:Z

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 1532
    const-string v1, "REQUIRED_PROCESS_ACTION"

    iget-object v2, p0, Lcom/osp/app/signin/ft;->o:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 1533
    const-string v1, "email_id"

    iget-object v2, p0, Lcom/osp/app/signin/ft;->c:Lcom/osp/app/signin/InterfaceService;

    invoke-static {v2}, Lcom/osp/app/util/ad;->g(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 1534
    iget-object v1, p0, Lcom/osp/app/signin/ft;->c:Lcom/osp/app/signin/InterfaceService;

    invoke-virtual {v1, v0}, Lcom/osp/app/signin/InterfaceService;->sendBroadcast(Landroid/content/Intent;)V

    .line 1538
    iget-object v0, p0, Lcom/osp/app/signin/ft;->c:Lcom/osp/app/signin/InterfaceService;

    iget v1, p0, Lcom/osp/app/signin/ft;->d:I

    invoke-virtual {v0, v1}, Lcom/osp/app/signin/InterfaceService;->stopSelf(I)V

    .line 1540
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v0, "TncMandatoryTask complete"

    invoke-static {v0}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;)V

    .line 1541
    return-void

    .line 1505
    :cond_1
    const-string v0, "USR_3113"

    iget-object v1, p0, Lcom/osp/app/signin/ft;->m:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-static {}, Lcom/osp/device/b;->b()Lcom/osp/device/b;

    move-result-object v0

    invoke-virtual {v0}, Lcom/osp/device/b;->l()I

    move-result v0

    const/16 v1, 0x10

    if-le v0, v1, :cond_0

    .line 1507
    iget-object v0, p0, Lcom/osp/app/signin/ft;->c:Lcom/osp/app/signin/InterfaceService;

    invoke-static {v0}, Lcom/osp/app/util/ad;->i(Landroid/content/Context;)V

    .line 1509
    new-instance v0, Lcom/osp/app/signin/fu;

    invoke-direct {v0, p0}, Lcom/osp/app/signin/fu;-><init>(Lcom/osp/app/signin/ft;)V

    const/4 v1, 0x0

    const-wide/16 v2, 0x3e8

    invoke-virtual {v0, v1, v2, v3}, Lcom/osp/app/signin/fu;->sendEmptyMessageDelayed(IJ)Z

    goto/16 :goto_0
.end method

.method public final b(Lcom/msc/b/c;)V
    .locals 6

    .prologue
    .line 1632
    invoke-super {p0, p1}, Lcom/msc/c/b;->b(Lcom/msc/b/c;)V

    .line 1634
    if-nez p1, :cond_1

    .line 1676
    :cond_0
    :goto_0
    return-void

    .line 1639
    :cond_1
    invoke-virtual {p1}, Lcom/msc/b/c;->a()J

    move-result-wide v0

    .line 1641
    invoke-virtual {p1}, Lcom/msc/b/c;->e()Ljava/lang/String;

    move-result-object v2

    .line 1642
    invoke-virtual {p1}, Lcom/msc/b/c;->f()Ljava/lang/Exception;

    move-result-object v3

    .line 1644
    iget-wide v4, p0, Lcom/osp/app/signin/ft;->e:J

    cmp-long v0, v0, v4

    if-nez v0, :cond_0

    .line 1648
    const/4 v0, 0x1

    iput v0, p0, Lcom/osp/app/signin/ft;->j:I

    .line 1649
    if-eqz v3, :cond_2

    .line 1651
    new-instance v0, Lcom/msc/c/f;

    invoke-direct {v0, v3}, Lcom/msc/c/f;-><init>(Ljava/lang/Exception;)V

    .line 1652
    invoke-virtual {v0}, Lcom/msc/c/f;->b()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/osp/app/signin/ft;->l:Ljava/lang/String;

    .line 1653
    invoke-virtual {v0}, Lcom/msc/c/f;->a()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/osp/app/signin/ft;->m:Ljava/lang/String;

    goto :goto_0

    .line 1655
    :cond_2
    if-eqz v2, :cond_3

    .line 1657
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v0, "SIS"

    const-string v1, "Server request error occured : validation check"

    invoke-static {v0, v1}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 1658
    new-instance v0, Lcom/msc/c/f;

    invoke-direct {v0}, Lcom/msc/c/f;-><init>()V

    .line 1659
    invoke-virtual {v0, v2}, Lcom/msc/c/f;->c(Ljava/lang/String;)V

    .line 1660
    invoke-virtual {v0}, Lcom/msc/c/f;->b()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/osp/app/signin/ft;->l:Ljava/lang/String;

    .line 1662
    invoke-virtual {v0}, Lcom/msc/c/f;->a()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/osp/app/signin/ft;->m:Ljava/lang/String;

    goto :goto_0

    .line 1672
    :cond_3
    const-string v0, "Unknown Error"

    iput-object v0, p0, Lcom/osp/app/signin/ft;->l:Ljava/lang/String;

    goto :goto_0
.end method

.method public final declared-synchronized c(Lcom/msc/b/c;)V
    .locals 4

    .prologue
    .line 1614
    monitor-enter p0

    :try_start_0
    invoke-super {p0, p1}, Lcom/msc/c/b;->c(Lcom/msc/b/c;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1616
    if-nez p1, :cond_1

    .line 1628
    :cond_0
    :goto_0
    monitor-exit p0

    return-void

    .line 1621
    :cond_1
    :try_start_1
    invoke-virtual {p1}, Lcom/msc/b/c;->a()J

    move-result-wide v0

    .line 1623
    iget-wide v2, p0, Lcom/osp/app/signin/ft;->e:J

    cmp-long v0, v0, v2

    if-nez v0, :cond_0

    .line 1625
    const/4 v0, 0x0

    iput v0, p0, Lcom/osp/app/signin/ft;->j:I

    .line 1626
    const-string v0, "Request cancelled"

    iput-object v0, p0, Lcom/osp/app/signin/ft;->l:Ljava/lang/String;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 1614
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method protected final synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 1413
    invoke-virtual {p0}, Lcom/osp/app/signin/ft;->a()Ljava/lang/Boolean;

    move-result-object v0

    return-object v0
.end method

.method protected final synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 1413
    check-cast p1, Ljava/lang/Boolean;

    invoke-virtual {p0, p1}, Lcom/osp/app/signin/ft;->a(Ljava/lang/Boolean;)V

    return-void
.end method

.method protected final onPreExecute()V
    .locals 1

    .prologue
    .line 1461
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v0, "TncMandatoryTask start"

    invoke-static {v0}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;)V

    .line 1462
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    iput-object v0, p0, Lcom/osp/app/signin/ft;->p:Landroid/content/Intent;

    .line 1464
    return-void
.end method
