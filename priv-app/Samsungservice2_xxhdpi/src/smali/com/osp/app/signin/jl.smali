.class final Lcom/osp/app/signin/jl;
.super Lcom/msc/c/b;
.source "SignInView.java"


# instance fields
.field final synthetic c:Lcom/osp/app/signin/SignInView;

.field private d:J

.field private e:J

.field private f:J


# virtual methods
.method protected final varargs a()Ljava/lang/Boolean;
    .locals 6

    .prologue
    const/4 v4, 0x0

    const/4 v5, 0x0

    .line 4279
    iget-object v0, p0, Lcom/osp/app/signin/jl;->c:Lcom/osp/app/signin/SignInView;

    invoke-static {v0}, Lcom/osp/app/signin/SignInView;->e(Lcom/osp/app/signin/SignInView;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 4284
    iget-object v0, p0, Lcom/osp/app/signin/jl;->c:Lcom/osp/app/signin/SignInView;

    invoke-static {v0}, Lcom/osp/app/signin/SignInView;->s(Lcom/osp/app/signin/SignInView;)Landroid/view/View;

    move-result-object v0

    const v1, 0x7f0c0129

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/EditText;

    .line 4285
    iget-object v1, p0, Lcom/osp/app/signin/jl;->c:Lcom/osp/app/signin/SignInView;

    invoke-static {v1}, Lcom/osp/app/signin/SignInView;->s(Lcom/osp/app/signin/SignInView;)Landroid/view/View;

    move-result-object v1

    const v2, 0x7f0c012a

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/EditText;

    .line 4292
    :goto_0
    iget-object v2, p0, Lcom/osp/app/signin/jl;->c:Lcom/osp/app/signin/SignInView;

    invoke-static {v2}, Lcom/osp/app/signin/SignInView;->z(Lcom/osp/app/signin/SignInView;)V

    .line 4294
    iget-object v2, p0, Lcom/osp/app/signin/jl;->c:Lcom/osp/app/signin/SignInView;

    iget-object v3, p0, Lcom/osp/app/signin/jl;->c:Lcom/osp/app/signin/SignInView;

    invoke-static {v3}, Lcom/osp/app/util/ad;->p(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/osp/app/signin/SignInView;->c(Lcom/osp/app/signin/SignInView;Ljava/lang/String;)Ljava/lang/String;

    .line 4296
    new-instance v2, Lcom/msc/a/f;

    invoke-direct {v2}, Lcom/msc/a/f;-><init>()V

    .line 4297
    iget-object v3, p0, Lcom/osp/app/signin/jl;->c:Lcom/osp/app/signin/SignInView;

    invoke-static {v3, v5}, Lcom/osp/app/signin/SignInView;->b(Lcom/osp/app/signin/SignInView;Z)Z

    .line 4298
    iget-object v3, p0, Lcom/osp/app/signin/jl;->c:Lcom/osp/app/signin/SignInView;

    invoke-static {v3, v4}, Lcom/osp/app/signin/SignInView;->d(Lcom/osp/app/signin/SignInView;Ljava/lang/String;)Ljava/lang/String;

    .line 4300
    iget-object v3, p0, Lcom/osp/app/signin/jl;->c:Lcom/osp/app/signin/SignInView;

    invoke-static {v3, v4}, Lcom/osp/app/signin/SignInView;->b(Lcom/osp/app/signin/SignInView;Ljava/lang/String;)Ljava/lang/String;

    .line 4302
    invoke-virtual {v0}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v3

    sget-object v4, Ljava/util/Locale;->ENGLISH:Ljava/util/Locale;

    invoke-virtual {v3, v4}, Ljava/lang/String;->toLowerCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/msc/a/f;->d(Ljava/lang/String;)V

    .line 4303
    invoke-virtual {v2}, Lcom/msc/a/f;->e()V

    .line 4306
    iget-object v3, p0, Lcom/osp/app/signin/jl;->c:Lcom/osp/app/signin/SignInView;

    invoke-static {v3}, Lcom/osp/app/signin/SignInView;->j(Lcom/osp/app/signin/SignInView;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/msc/a/f;->f(Ljava/lang/String;)V

    .line 4307
    iget-object v3, p0, Lcom/osp/app/signin/jl;->c:Lcom/osp/app/signin/SignInView;

    invoke-static {v3}, Lcom/osp/app/signin/SignInView;->A(Lcom/osp/app/signin/SignInView;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/msc/a/f;->h(Ljava/lang/String;)V

    .line 4308
    iget-object v3, p0, Lcom/osp/app/signin/jl;->c:Lcom/osp/app/signin/SignInView;

    invoke-static {v3}, Lcom/osp/app/signin/SignInView;->p(Lcom/osp/app/signin/SignInView;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/msc/a/f;->g(Ljava/lang/String;)V

    .line 4309
    invoke-virtual {v1}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v2, v1}, Lcom/msc/a/f;->e(Ljava/lang/String;)V

    .line 4310
    invoke-static {}, Lcom/osp/device/b;->b()Lcom/osp/device/b;

    move-result-object v1

    invoke-virtual {v1}, Lcom/osp/device/b;->a()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-virtual {v0}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    sget-object v1, Ljava/util/Locale;->ENGLISH:Ljava/util/Locale;

    invoke-virtual {v0, v1}, Ljava/lang/String;->toLowerCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/telephony/PhoneNumberUtils;->isGlobalPhoneNumber(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 4312
    const-string v0, "Y"

    invoke-virtual {v2, v0}, Lcom/msc/a/f;->b(Ljava/lang/String;)V

    .line 4314
    :cond_0
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v0, "SIV"

    const-string v1, "RequestCheckListInfo"

    const-string v3, "START"

    invoke-static {v0, v1, v3}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    invoke-static {}, Lcom/msc/c/g;->a()Lcom/msc/c/g;

    iget-object v0, p0, Lcom/osp/app/signin/jl;->c:Lcom/osp/app/signin/SignInView;

    invoke-static {v0, v2, p0}, Lcom/msc/c/g;->a(Landroid/content/Context;Lcom/msc/a/f;Lcom/msc/b/h;)J

    move-result-wide v0

    iput-wide v0, p0, Lcom/osp/app/signin/jl;->d:J

    iget-wide v0, p0, Lcom/osp/app/signin/jl;->d:J

    invoke-virtual {p0, v0, v1}, Lcom/osp/app/signin/jl;->a(J)V

    iget-wide v0, p0, Lcom/osp/app/signin/jl;->d:J

    const-string v2, "from_xml"

    invoke-virtual {p0, v0, v1, v2}, Lcom/osp/app/signin/jl;->a(JLjava/lang/String;)V

    iget-wide v0, p0, Lcom/osp/app/signin/jl;->d:J

    sget-object v2, Lcom/msc/b/g;->c:Lcom/msc/b/g;

    invoke-static {v0, v1, v2}, Lcom/msc/c/g;->a(JLcom/msc/b/g;)V

    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v0, "SIV"

    const-string v1, "RequestCheckListInfo"

    const-string v2, "END"

    invoke-static {v0, v1, v2}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 4315
    invoke-static {v5}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    return-object v0

    .line 4288
    :cond_1
    iget-object v0, p0, Lcom/osp/app/signin/jl;->c:Lcom/osp/app/signin/SignInView;

    const v1, 0x7f0c0121

    invoke-virtual {v0, v1}, Lcom/osp/app/signin/SignInView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/EditText;

    .line 4289
    iget-object v1, p0, Lcom/osp/app/signin/jl;->c:Lcom/osp/app/signin/SignInView;

    const v2, 0x7f0c0122

    invoke-virtual {v1, v2}, Lcom/osp/app/signin/SignInView;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/EditText;

    goto/16 :goto_0
.end method

.method public final declared-synchronized a(Lcom/msc/b/c;)V
    .locals 6

    .prologue
    .line 4422
    monitor-enter p0

    :try_start_0
    invoke-super {p0, p1}, Lcom/msc/c/b;->a(Lcom/msc/b/c;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 4424
    if-nez p1, :cond_1

    .line 4524
    :cond_0
    :goto_0
    monitor-exit p0

    return-void

    .line 4429
    :cond_1
    :try_start_1
    invoke-virtual {p1}, Lcom/msc/b/c;->a()J

    move-result-wide v0

    .line 4430
    invoke-virtual {p1}, Lcom/msc/b/c;->e()Ljava/lang/String;

    move-result-object v2

    .line 4432
    iget-wide v4, p0, Lcom/osp/app/signin/jl;->d:J
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    cmp-long v3, v0, v4

    if-nez v3, :cond_4

    .line 4437
    :try_start_2
    iget-object v0, p0, Lcom/osp/app/signin/jl;->c:Lcom/osp/app/signin/SignInView;

    invoke-static {}, Lcom/msc/c/h;->a()Lcom/msc/c/h;

    iget-object v1, p0, Lcom/osp/app/signin/jl;->c:Lcom/osp/app/signin/SignInView;

    invoke-static {v1, v2}, Lcom/msc/c/h;->b(Landroid/content/Context;Ljava/lang/String;)Lcom/msc/a/g;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/osp/app/signin/SignInView;->b(Lcom/osp/app/signin/SignInView;Lcom/msc/a/g;)Lcom/msc/a/g;

    .line 4438
    invoke-static {}, Lcom/osp/device/b;->b()Lcom/osp/device/b;

    move-result-object v0

    invoke-virtual {v0}, Lcom/osp/device/b;->a()Z

    move-result v0

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/osp/app/signin/jl;->c:Lcom/osp/app/signin/SignInView;

    invoke-static {v0}, Lcom/osp/app/signin/SignInView;->K(Lcom/osp/app/signin/SignInView;)Lcom/msc/a/g;

    move-result-object v0

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/osp/app/signin/jl;->c:Lcom/osp/app/signin/SignInView;

    invoke-static {v0}, Lcom/osp/app/signin/SignInView;->K(Lcom/osp/app/signin/SignInView;)Lcom/msc/a/g;

    move-result-object v0

    invoke-virtual {v0}, Lcom/msc/a/g;->a()Lcom/msc/c/f;

    move-result-object v0

    if-eqz v0, :cond_2

    .line 4440
    iget-object v0, p0, Lcom/osp/app/signin/jl;->c:Lcom/osp/app/signin/SignInView;

    iget-object v1, p0, Lcom/osp/app/signin/jl;->c:Lcom/osp/app/signin/SignInView;

    iget-object v1, p0, Lcom/osp/app/signin/jl;->c:Lcom/osp/app/signin/SignInView;

    invoke-static {v1}, Lcom/osp/app/signin/SignInView;->K(Lcom/osp/app/signin/SignInView;)Lcom/msc/a/g;

    move-result-object v1

    invoke-virtual {v1}, Lcom/msc/a/g;->c()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/osp/app/signin/SignInView;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/osp/app/signin/SignInView;->e(Lcom/osp/app/signin/SignInView;Ljava/lang/String;)Ljava/lang/String;

    .line 4441
    iget-object v0, p0, Lcom/osp/app/signin/jl;->c:Lcom/osp/app/signin/SignInView;

    iget-object v1, p0, Lcom/osp/app/signin/jl;->c:Lcom/osp/app/signin/SignInView;

    invoke-static {v1}, Lcom/osp/app/signin/SignInView;->K(Lcom/osp/app/signin/SignInView;)Lcom/msc/a/g;

    move-result-object v1

    invoke-virtual {v1}, Lcom/msc/a/g;->b()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/osp/app/signin/SignInView;->b(Lcom/osp/app/signin/SignInView;Ljava/lang/String;)Ljava/lang/String;

    .line 4442
    iget-object v0, p0, Lcom/osp/app/signin/jl;->c:Lcom/osp/app/signin/SignInView;

    invoke-static {v0}, Lcom/osp/app/signin/SignInView;->K(Lcom/osp/app/signin/SignInView;)Lcom/msc/a/g;

    move-result-object v0

    invoke-virtual {v0}, Lcom/msc/a/g;->a()Lcom/msc/c/f;

    move-result-object v0

    iput-object v0, p0, Lcom/osp/app/signin/jl;->b:Lcom/msc/c/f;
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_0
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0

    .line 4451
    :catch_0
    move-exception v0

    .line 4453
    :try_start_3
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    .line 4454
    new-instance v1, Lcom/msc/c/f;

    invoke-direct {v1, v0}, Lcom/msc/c/f;-><init>(Ljava/lang/Exception;)V

    iput-object v1, p0, Lcom/osp/app/signin/jl;->b:Lcom/msc/c/f;
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    goto :goto_0

    .line 4422
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 4445
    :cond_2
    :try_start_4
    iget-object v0, p0, Lcom/osp/app/signin/jl;->c:Lcom/osp/app/signin/SignInView;

    invoke-static {v0}, Lcom/osp/app/signin/SignInView;->K(Lcom/osp/app/signin/SignInView;)Lcom/msc/a/g;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/osp/app/signin/jl;->c:Lcom/osp/app/signin/SignInView;

    invoke-static {v0}, Lcom/osp/app/signin/SignInView;->K(Lcom/osp/app/signin/SignInView;)Lcom/msc/a/g;

    move-result-object v0

    invoke-virtual {v0}, Lcom/msc/a/g;->f()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/osp/app/signin/jl;->c:Lcom/osp/app/signin/SignInView;

    invoke-static {v0}, Lcom/osp/app/signin/SignInView;->K(Lcom/osp/app/signin/SignInView;)Lcom/msc/a/g;

    move-result-object v0

    invoke-virtual {v0}, Lcom/msc/a/g;->f()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v0

    if-gtz v0, :cond_0

    .line 4447
    :cond_3
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v0, "SIV"

    const-string v1, "There is no MCC value in the server response."

    invoke-static {v0, v1}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 4448
    iget-object v0, p0, Lcom/osp/app/signin/jl;->c:Lcom/osp/app/signin/SignInView;

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lcom/osp/app/signin/SignInView;->b(Lcom/osp/app/signin/SignInView;Lcom/msc/a/g;)Lcom/msc/a/g;
    :try_end_4
    .catch Ljava/lang/Exception; {:try_start_4 .. :try_end_4} :catch_0
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    goto/16 :goto_0

    .line 4456
    :cond_4
    :try_start_5
    iget-wide v4, p0, Lcom/osp/app/signin/jl;->e:J
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    cmp-long v3, v0, v4

    if-nez v3, :cond_7

    .line 4460
    :try_start_6
    iget-object v0, p0, Lcom/osp/app/signin/jl;->c:Lcom/osp/app/signin/SignInView;

    invoke-static {}, Lcom/msc/c/h;->a()Lcom/msc/c/h;

    invoke-static {v2}, Lcom/msc/c/h;->e(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/osp/app/signin/SignInView;->b(Lcom/osp/app/signin/SignInView;Ljava/lang/String;)Ljava/lang/String;
    :try_end_6
    .catch Ljava/lang/Exception; {:try_start_6 .. :try_end_6} :catch_1
    .catchall {:try_start_6 .. :try_end_6} :catchall_0

    .line 4465
    :goto_1
    :try_start_7
    iget-object v0, p0, Lcom/osp/app/signin/jl;->c:Lcom/osp/app/signin/SignInView;

    invoke-static {v0}, Lcom/osp/app/signin/SignInView;->y(Lcom/osp/app/signin/SignInView;)Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/osp/app/signin/jl;->c:Lcom/osp/app/signin/SignInView;

    invoke-static {v0}, Lcom/osp/app/signin/SignInView;->y(Lcom/osp/app/signin/SignInView;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v0

    if-lez v0, :cond_0

    .line 4470
    iget-object v0, p0, Lcom/osp/app/signin/jl;->c:Lcom/osp/app/signin/SignInView;

    const/4 v1, 0x1

    invoke-static {v0, v1}, Lcom/osp/app/signin/SignInView;->b(Lcom/osp/app/signin/SignInView;Z)Z

    .line 4471
    invoke-static {}, Lcom/osp/common/util/i;->a()Lcom/osp/common/util/i;

    iget-object v0, p0, Lcom/osp/app/signin/jl;->c:Lcom/osp/app/signin/SignInView;

    invoke-static {v0}, Lcom/osp/common/util/i;->e(Landroid/content/Context;)Z
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_0

    move-result v0

    if-eqz v0, :cond_5

    .line 4476
    :try_start_8
    iget-object v0, p0, Lcom/osp/app/signin/jl;->c:Lcom/osp/app/signin/SignInView;

    invoke-static {}, Lcom/osp/common/util/i;->a()Lcom/osp/common/util/i;

    iget-object v1, p0, Lcom/osp/app/signin/jl;->c:Lcom/osp/app/signin/SignInView;

    invoke-static {v1}, Lcom/osp/common/util/i;->h(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/osp/app/signin/SignInView;->d(Lcom/osp/app/signin/SignInView;Ljava/lang/String;)Ljava/lang/String;

    .line 4477
    iget-object v0, p0, Lcom/osp/app/signin/jl;->c:Lcom/osp/app/signin/SignInView;

    iget-object v1, p0, Lcom/osp/app/signin/jl;->c:Lcom/osp/app/signin/SignInView;

    invoke-static {v1}, Lcom/osp/app/signin/SignInView;->G(Lcom/osp/app/signin/SignInView;)Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/osp/app/signin/jl;->c:Lcom/osp/app/signin/SignInView;

    invoke-virtual {v2}, Lcom/osp/app/signin/SignInView;->getApplicationContext()Landroid/content/Context;

    iget-object v2, p0, Lcom/osp/app/signin/jl;->c:Lcom/osp/app/signin/SignInView;

    invoke-static {v2}, Lcom/osp/app/signin/SignInView;->G(Lcom/osp/app/signin/SignInView;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/osp/common/util/i;->d(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v1, v2}, Lcom/msc/c/e;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)V
    :try_end_8
    .catch Ljava/lang/Exception; {:try_start_8 .. :try_end_8} :catch_2
    .catchall {:try_start_8 .. :try_end_8} :catchall_1

    .line 4483
    :try_start_9
    iget-object v0, p0, Lcom/osp/app/signin/jl;->c:Lcom/osp/app/signin/SignInView;

    invoke-static {v0}, Lcom/osp/app/signin/SignInView;->G(Lcom/osp/app/signin/SignInView;)Ljava/lang/String;

    move-result-object v0

    if-nez v0, :cond_5

    .line 4485
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v0, "SIV"

    const-string v1, "The sim state is ready but mcc is null!!"

    invoke-static {v0, v1}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 4490
    :cond_5
    :goto_2
    iget-object v0, p0, Lcom/osp/app/signin/jl;->c:Lcom/osp/app/signin/SignInView;

    invoke-static {v0}, Lcom/osp/app/signin/SignInView;->G(Lcom/osp/app/signin/SignInView;)Ljava/lang/String;

    move-result-object v0

    if-nez v0, :cond_0

    .line 4492
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v0, "SIV"

    const-string v1, "requestMyCountryZone"

    invoke-static {v0, v1}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;)V

    invoke-static {}, Lcom/msc/c/g;->a()Lcom/msc/c/g;

    iget-object v0, p0, Lcom/osp/app/signin/jl;->c:Lcom/osp/app/signin/SignInView;

    invoke-static {v0, p0}, Lcom/msc/c/g;->b(Landroid/content/Context;Lcom/msc/b/h;)J

    move-result-wide v0

    iput-wide v0, p0, Lcom/osp/app/signin/jl;->f:J

    iget-wide v0, p0, Lcom/osp/app/signin/jl;->f:J

    invoke-virtual {p0, v0, v1}, Lcom/osp/app/signin/jl;->a(J)V

    iget-wide v0, p0, Lcom/osp/app/signin/jl;->f:J

    sget-object v2, Lcom/msc/b/g;->b:Lcom/msc/b/g;

    invoke-static {v0, v1, v2}, Lcom/msc/c/g;->a(JLcom/msc/b/g;)V

    goto/16 :goto_0

    .line 4461
    :catch_1
    move-exception v0

    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V
    :try_end_9
    .catchall {:try_start_9 .. :try_end_9} :catchall_0

    goto/16 :goto_1

    .line 4478
    :catch_2
    move-exception v0

    :try_start_a
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V
    :try_end_a
    .catchall {:try_start_a .. :try_end_a} :catchall_1

    .line 4483
    :try_start_b
    iget-object v0, p0, Lcom/osp/app/signin/jl;->c:Lcom/osp/app/signin/SignInView;

    invoke-static {v0}, Lcom/osp/app/signin/SignInView;->G(Lcom/osp/app/signin/SignInView;)Ljava/lang/String;

    move-result-object v0

    if-nez v0, :cond_5

    .line 4485
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v0, "SIV"

    const-string v1, "The sim state is ready but mcc is null!!"

    invoke-static {v0, v1}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_2

    .line 4483
    :catchall_1
    move-exception v0

    iget-object v1, p0, Lcom/osp/app/signin/jl;->c:Lcom/osp/app/signin/SignInView;

    invoke-static {v1}, Lcom/osp/app/signin/SignInView;->G(Lcom/osp/app/signin/SignInView;)Ljava/lang/String;

    move-result-object v1

    if-nez v1, :cond_6

    .line 4485
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v1, "SIV"

    const-string v2, "The sim state is ready but mcc is null!!"

    invoke-static {v1, v2}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;)V

    :cond_6
    throw v0

    .line 4495
    :cond_7
    iget-wide v4, p0, Lcom/osp/app/signin/jl;->f:J
    :try_end_b
    .catchall {:try_start_b .. :try_end_b} :catchall_0

    cmp-long v0, v0, v4

    if-nez v0, :cond_0

    .line 4499
    :try_start_c
    invoke-static {}, Lcom/msc/c/h;->a()Lcom/msc/c/h;

    invoke-static {v2}, Lcom/msc/c/h;->w(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 4500
    if-eqz v0, :cond_9

    .line 4502
    invoke-static {}, Lcom/osp/common/util/i;->a()Lcom/osp/common/util/i;

    iget-object v1, p0, Lcom/osp/app/signin/jl;->c:Lcom/osp/app/signin/SignInView;

    invoke-static {v1}, Lcom/osp/common/util/i;->o(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v1

    .line 4503
    invoke-static {}, Lcom/osp/common/util/i;->a()Lcom/osp/common/util/i;

    iget-object v2, p0, Lcom/osp/app/signin/jl;->c:Lcom/osp/app/signin/SignInView;

    invoke-static {v1}, Lcom/osp/common/util/i;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 4504
    if-eqz v2, :cond_8

    invoke-virtual {v2, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_8

    .line 4506
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v3, "GetMyCountryZoneTask countryCodeFromNetworkMcc = ["

    invoke-direct {v0, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, "]"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;)V

    .line 4507
    iget-object v0, p0, Lcom/osp/app/signin/jl;->c:Lcom/osp/app/signin/SignInView;

    invoke-static {v0, v1}, Lcom/osp/app/signin/SignInView;->d(Lcom/osp/app/signin/SignInView;Ljava/lang/String;)Ljava/lang/String;
    :try_end_c
    .catch Ljava/lang/Exception; {:try_start_c .. :try_end_c} :catch_3
    .catchall {:try_start_c .. :try_end_c} :catchall_0

    goto/16 :goto_0

    .line 4517
    :catch_3
    move-exception v0

    .line 4519
    :try_start_d
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v1, "GetMyCountryZoneTask fail"

    invoke-static {v1}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;)V

    .line 4520
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    .line 4521
    iget-object v0, p0, Lcom/osp/app/signin/jl;->c:Lcom/osp/app/signin/SignInView;

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lcom/osp/app/signin/SignInView;->d(Lcom/osp/app/signin/SignInView;Ljava/lang/String;)Ljava/lang/String;
    :try_end_d
    .catchall {:try_start_d .. :try_end_d} :catchall_0

    goto/16 :goto_0

    .line 4510
    :cond_8
    :try_start_e
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "GetMyCountryZoneTask countryCode = ["

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "]"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;)V

    .line 4511
    iget-object v1, p0, Lcom/osp/app/signin/jl;->c:Lcom/osp/app/signin/SignInView;

    invoke-static {}, Lcom/osp/common/util/i;->a()Lcom/osp/common/util/i;

    iget-object v2, p0, Lcom/osp/app/signin/jl;->c:Lcom/osp/app/signin/SignInView;

    sget-object v2, Ljava/util/Locale;->ENGLISH:Ljava/util/Locale;

    invoke-virtual {v0, v2}, Ljava/lang/String;->toLowerCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/osp/common/util/i;->c(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Lcom/osp/app/signin/SignInView;->d(Lcom/osp/app/signin/SignInView;Ljava/lang/String;)Ljava/lang/String;

    goto/16 :goto_0

    .line 4515
    :cond_9
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v0, "SIV"

    const-string v1, "GetMyCountryZoneTask Success. but countryCode is null!!"

    invoke-static {v0, v1}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_e
    .catch Ljava/lang/Exception; {:try_start_e .. :try_end_e} :catch_3
    .catchall {:try_start_e .. :try_end_e} :catchall_0

    goto/16 :goto_0
.end method

.method protected final a(Ljava/lang/Boolean;)V
    .locals 7

    .prologue
    const/4 v6, 0x0

    .line 4320
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x15

    if-lt v0, v1, :cond_0

    .line 4322
    invoke-super {p0, p1}, Lcom/msc/c/b;->a(Ljava/lang/Boolean;)V

    .line 4328
    :goto_0
    iget-object v0, p0, Lcom/osp/app/signin/jl;->b:Lcom/msc/c/f;

    if-eqz v0, :cond_a

    .line 4335
    iget-object v0, p0, Lcom/osp/app/signin/jl;->c:Lcom/osp/app/signin/SignInView;

    invoke-static {v0}, Lcom/osp/app/signin/SignInView;->c(Lcom/osp/app/signin/SignInView;)Landroid/content/Intent;

    move-result-object v0

    const-string v1, "signin_duration"

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    iget-object v4, p0, Lcom/osp/app/signin/jl;->c:Lcom/osp/app/signin/SignInView;

    invoke-static {v4}, Lcom/osp/app/signin/SignInView;->B(Lcom/osp/app/signin/SignInView;)J

    move-result-wide v4

    sub-long/2addr v2, v4

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;J)Landroid/content/Intent;

    .line 4336
    iget-object v0, p0, Lcom/osp/app/signin/jl;->c:Lcom/osp/app/signin/SignInView;

    invoke-static {v0}, Lcom/osp/app/signin/SignInView;->c(Lcom/osp/app/signin/SignInView;)Landroid/content/Intent;

    move-result-object v0

    const-string v1, "signin_status"

    invoke-virtual {v0, v1, v6}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 4337
    iget-object v0, p0, Lcom/osp/app/signin/jl;->c:Lcom/osp/app/signin/SignInView;

    invoke-static {v0}, Lcom/osp/app/signin/SignInView;->c(Lcom/osp/app/signin/SignInView;)Landroid/content/Intent;

    move-result-object v0

    const-string v1, "error_code"

    iget-object v2, p0, Lcom/osp/app/signin/jl;->b:Lcom/msc/c/f;

    invoke-virtual {v2}, Lcom/msc/c/f;->a()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 4339
    new-instance v0, Lcom/osp/app/bigdatalog/c;

    iget-object v1, p0, Lcom/osp/app/signin/jl;->c:Lcom/osp/app/signin/SignInView;

    invoke-virtual {v1}, Lcom/osp/app/signin/SignInView;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    const/4 v2, 0x4

    const/16 v3, 0x9

    iget-object v4, p0, Lcom/osp/app/signin/jl;->c:Lcom/osp/app/signin/SignInView;

    invoke-static {v4}, Lcom/osp/app/signin/SignInView;->c(Lcom/osp/app/signin/SignInView;)Landroid/content/Intent;

    move-result-object v4

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/osp/app/bigdatalog/c;-><init>(Landroid/content/Context;IILandroid/content/Intent;)V

    .line 4342
    invoke-static {}, Lcom/osp/app/bigdatalog/e;->a()Lcom/osp/app/bigdatalog/e;

    move-result-object v1

    iget-object v2, p0, Lcom/osp/app/signin/jl;->c:Lcom/osp/app/signin/SignInView;

    invoke-virtual {v2}, Lcom/osp/app/signin/SignInView;->getApplicationContext()Landroid/content/Context;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/osp/app/bigdatalog/e;->a(Landroid/content/Context;)V

    .line 4343
    invoke-static {}, Lcom/osp/app/bigdatalog/e;->a()Lcom/osp/app/bigdatalog/e;

    move-result-object v1

    iget-object v2, p0, Lcom/osp/app/signin/jl;->c:Lcom/osp/app/signin/SignInView;

    invoke-virtual {v2}, Lcom/osp/app/signin/SignInView;->getApplicationContext()Landroid/content/Context;

    move-result-object v2

    invoke-virtual {v1, v2, v0}, Lcom/osp/app/bigdatalog/e;->a(Landroid/content/Context;Lcom/osp/app/bigdatalog/c;)V

    .line 4346
    iget-object v0, p0, Lcom/osp/app/signin/jl;->c:Lcom/osp/app/signin/SignInView;

    invoke-static {v0}, Lcom/osp/app/signin/SignInView;->C(Lcom/osp/app/signin/SignInView;)Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/osp/app/signin/jl;->c:Lcom/osp/app/signin/SignInView;

    invoke-static {v0}, Lcom/osp/app/signin/SignInView;->D(Lcom/osp/app/signin/SignInView;)Z

    move-result v0

    if-nez v0, :cond_1

    const-string v0, "USR_3113"

    iget-object v1, p0, Lcom/osp/app/signin/jl;->b:Lcom/msc/c/f;

    invoke-virtual {v1}, Lcom/msc/c/f;->a()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 4348
    iget-object v0, p0, Lcom/osp/app/signin/jl;->c:Lcom/osp/app/signin/SignInView;

    iget-object v1, p0, Lcom/osp/app/signin/jl;->c:Lcom/osp/app/signin/SignInView;

    invoke-static {v0, v1}, Lcom/osp/app/signin/SignInView;->a(Lcom/osp/app/signin/SignInView;Landroid/content/Context;)V

    .line 4418
    :goto_1
    return-void

    .line 4325
    :cond_0
    invoke-super {p0}, Lcom/msc/c/b;->e()V

    goto/16 :goto_0

    .line 4349
    :cond_1
    iget-object v0, p0, Lcom/osp/app/signin/jl;->c:Lcom/osp/app/signin/SignInView;

    invoke-static {v0}, Lcom/osp/app/signin/SignInView;->C(Lcom/osp/app/signin/SignInView;)Z

    move-result v0

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/osp/app/signin/jl;->c:Lcom/osp/app/signin/SignInView;

    invoke-static {v0}, Lcom/osp/app/signin/SignInView;->D(Lcom/osp/app/signin/SignInView;)Z

    move-result v0

    if-nez v0, :cond_2

    const-string v0, "USR_3192"

    iget-object v1, p0, Lcom/osp/app/signin/jl;->b:Lcom/msc/c/f;

    invoke-virtual {v1}, Lcom/msc/c/f;->a()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 4351
    iget-object v0, p0, Lcom/osp/app/signin/jl;->c:Lcom/osp/app/signin/SignInView;

    iget-object v1, p0, Lcom/osp/app/signin/jl;->c:Lcom/osp/app/signin/SignInView;

    invoke-static {v0, v1}, Lcom/osp/app/signin/SignInView;->b(Lcom/osp/app/signin/SignInView;Landroid/content/Context;)V

    goto :goto_1

    .line 4352
    :cond_2
    invoke-static {}, Lcom/osp/device/b;->b()Lcom/osp/device/b;

    move-result-object v0

    invoke-virtual {v0}, Lcom/osp/device/b;->a()Z

    move-result v0

    if-eqz v0, :cond_3

    const-string v0, "USR_3174"

    iget-object v1, p0, Lcom/osp/app/signin/jl;->b:Lcom/msc/c/f;

    invoke-virtual {v1}, Lcom/msc/c/f;->a()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 4356
    iget-object v0, p0, Lcom/osp/app/signin/jl;->c:Lcom/osp/app/signin/SignInView;

    invoke-static {v0}, Lcom/osp/app/signin/SignInView;->E(Lcom/osp/app/signin/SignInView;)V

    goto :goto_1

    .line 4357
    :cond_3
    invoke-static {}, Lcom/osp/device/b;->b()Lcom/osp/device/b;

    move-result-object v0

    invoke-virtual {v0}, Lcom/osp/device/b;->a()Z

    move-result v0

    if-eqz v0, :cond_4

    const-string v0, "DUPLICATED_ID"

    iget-object v1, p0, Lcom/osp/app/signin/jl;->b:Lcom/msc/c/f;

    invoke-virtual {v1}, Lcom/msc/c/f;->a()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 4360
    iget-object v0, p0, Lcom/osp/app/signin/jl;->c:Lcom/osp/app/signin/SignInView;

    const/4 v1, 0x2

    const/4 v2, 0x0

    iget-object v3, p0, Lcom/osp/app/signin/jl;->c:Lcom/osp/app/signin/SignInView;

    iget-object v3, v3, Lcom/osp/app/signin/SignInView;->d:Landroid/content/DialogInterface$OnCancelListener;

    invoke-virtual {v0, v1, v2, v3}, Lcom/osp/app/signin/SignInView;->a(ILandroid/content/DialogInterface$OnDismissListener;Landroid/content/DialogInterface$OnCancelListener;)V

    goto :goto_1

    .line 4361
    :cond_4
    iget-object v0, p0, Lcom/osp/app/signin/jl;->c:Lcom/osp/app/signin/SignInView;

    invoke-static {v0}, Lcom/osp/app/signin/SignInView;->F(Lcom/osp/app/signin/SignInView;)Z

    move-result v0

    if-eqz v0, :cond_7

    .line 4363
    iget-object v0, p0, Lcom/osp/app/signin/jl;->c:Lcom/osp/app/signin/SignInView;

    invoke-static {v0, v6}, Lcom/osp/app/signin/SignInView;->b(Lcom/osp/app/signin/SignInView;Z)Z

    .line 4365
    iget-object v0, p0, Lcom/osp/app/signin/jl;->c:Lcom/osp/app/signin/SignInView;

    invoke-static {v0}, Lcom/osp/app/signin/SignInView;->G(Lcom/osp/app/signin/SignInView;)Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_5

    .line 4367
    iget-object v0, p0, Lcom/osp/app/signin/jl;->c:Lcom/osp/app/signin/SignInView;

    iget-object v1, p0, Lcom/osp/app/signin/jl;->c:Lcom/osp/app/signin/SignInView;

    invoke-static {v1}, Lcom/osp/app/signin/SignInView;->G(Lcom/osp/app/signin/SignInView;)Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/osp/app/signin/jl;->c:Lcom/osp/app/signin/SignInView;

    invoke-virtual {v2}, Lcom/osp/app/signin/SignInView;->getApplicationContext()Landroid/content/Context;

    iget-object v2, p0, Lcom/osp/app/signin/jl;->c:Lcom/osp/app/signin/SignInView;

    invoke-static {v2}, Lcom/osp/app/signin/SignInView;->G(Lcom/osp/app/signin/SignInView;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/osp/common/util/i;->d(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v1, v2}, Lcom/msc/c/e;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)V

    .line 4368
    iget-object v0, p0, Lcom/osp/app/signin/jl;->c:Lcom/osp/app/signin/SignInView;

    new-instance v1, Lcom/osp/app/signin/jj;

    iget-object v2, p0, Lcom/osp/app/signin/jl;->c:Lcom/osp/app/signin/SignInView;

    invoke-direct {v1, v2}, Lcom/osp/app/signin/jj;-><init>(Lcom/osp/app/signin/SignInView;)V

    invoke-static {v0, v1}, Lcom/osp/app/signin/SignInView;->a(Lcom/osp/app/signin/SignInView;Lcom/osp/app/signin/jj;)Lcom/osp/app/signin/jj;

    .line 4369
    iget-object v0, p0, Lcom/osp/app/signin/jl;->c:Lcom/osp/app/signin/SignInView;

    invoke-static {v0}, Lcom/osp/app/signin/SignInView;->H(Lcom/osp/app/signin/SignInView;)Lcom/osp/app/signin/jj;

    move-result-object v0

    invoke-static {v0}, Lcom/msc/sa/c/d;->a(Landroid/os/AsyncTask;)V

    goto/16 :goto_1

    .line 4372
    :cond_5
    invoke-static {}, Lcom/osp/device/b;->b()Lcom/osp/device/b;

    move-result-object v0

    iget-object v1, p0, Lcom/osp/app/signin/jl;->c:Lcom/osp/app/signin/SignInView;

    invoke-virtual {v0, v1}, Lcom/osp/device/b;->i(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_6

    .line 4374
    iget-object v0, p0, Lcom/osp/app/signin/jl;->c:Lcom/osp/app/signin/SignInView;

    new-instance v1, Lcom/osp/app/signin/jm;

    iget-object v2, p0, Lcom/osp/app/signin/jl;->c:Lcom/osp/app/signin/SignInView;

    invoke-direct {v1, v2}, Lcom/osp/app/signin/jm;-><init>(Lcom/osp/app/signin/SignInView;)V

    invoke-static {v0, v1}, Lcom/osp/app/signin/SignInView;->a(Lcom/osp/app/signin/SignInView;Lcom/osp/app/signin/jm;)Lcom/osp/app/signin/jm;

    .line 4375
    iget-object v0, p0, Lcom/osp/app/signin/jl;->c:Lcom/osp/app/signin/SignInView;

    invoke-static {v0}, Lcom/osp/app/signin/SignInView;->I(Lcom/osp/app/signin/SignInView;)Lcom/osp/app/signin/jm;

    move-result-object v0

    new-array v1, v6, [Ljava/lang/Void;

    invoke-virtual {v0, v1}, Lcom/osp/app/signin/jm;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    goto/16 :goto_1

    .line 4378
    :cond_6
    iget-object v0, p0, Lcom/osp/app/signin/jl;->c:Lcom/osp/app/signin/SignInView;

    invoke-static {v0}, Lcom/osp/app/signin/SignInView;->J(Lcom/osp/app/signin/SignInView;)V

    goto/16 :goto_1

    .line 4384
    :cond_7
    const-string v0, "USR_3113"

    iget-object v1, p0, Lcom/osp/app/signin/jl;->b:Lcom/msc/c/f;

    invoke-virtual {v1}, Lcom/msc/c/f;->a()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_8

    const-string v0, "USR_3121"

    iget-object v1, p0, Lcom/osp/app/signin/jl;->b:Lcom/msc/c/f;

    invoke-virtual {v1}, Lcom/msc/c/f;->a()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_9

    .line 4388
    :cond_8
    iget-object v0, p0, Lcom/osp/app/signin/jl;->b:Lcom/msc/c/f;

    const-string v1, "IVIP"

    invoke-virtual {v0, v1}, Lcom/msc/c/f;->a(Ljava/lang/String;)V

    .line 4391
    :cond_9
    invoke-virtual {p0, v6}, Lcom/osp/app/signin/jl;->a(Z)V

    .line 4392
    iget-object v0, p0, Lcom/osp/app/signin/jl;->c:Lcom/osp/app/signin/SignInView;

    invoke-static {v0}, Lcom/osp/app/signin/SignInView;->a(Lcom/osp/app/signin/SignInView;)V

    goto/16 :goto_1

    .line 4396
    :cond_a
    iget-object v0, p0, Lcom/osp/app/signin/jl;->c:Lcom/osp/app/signin/SignInView;

    invoke-static {v0}, Lcom/osp/app/signin/SignInView;->K(Lcom/osp/app/signin/SignInView;)Lcom/msc/a/g;

    move-result-object v0

    if-eqz v0, :cond_d

    .line 4398
    iget-object v0, p0, Lcom/osp/app/signin/jl;->c:Lcom/osp/app/signin/SignInView;

    iget-object v1, p0, Lcom/osp/app/signin/jl;->c:Lcom/osp/app/signin/SignInView;

    invoke-static {v1}, Lcom/osp/app/signin/SignInView;->K(Lcom/osp/app/signin/SignInView;)Lcom/msc/a/g;

    move-result-object v1

    invoke-virtual {v1}, Lcom/msc/a/g;->f()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/msc/c/e;->d(Landroid/content/Context;Ljava/lang/String;)V

    .line 4399
    invoke-static {}, Lcom/osp/device/b;->b()Lcom/osp/device/b;

    move-result-object v0

    invoke-virtual {v0}, Lcom/osp/device/b;->a()Z

    move-result v0

    if-eqz v0, :cond_b

    .line 4401
    iget-object v0, p0, Lcom/osp/app/signin/jl;->c:Lcom/osp/app/signin/SignInView;

    invoke-static {v0}, Lcom/osp/app/signin/SignInView;->K(Lcom/osp/app/signin/SignInView;)Lcom/msc/a/g;

    move-result-object v0

    invoke-virtual {v0}, Lcom/msc/a/g;->c()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_b

    iget-object v0, p0, Lcom/osp/app/signin/jl;->c:Lcom/osp/app/signin/SignInView;

    invoke-static {v0}, Lcom/osp/app/signin/SignInView;->K(Lcom/osp/app/signin/SignInView;)Lcom/msc/a/g;

    move-result-object v0

    invoke-virtual {v0}, Lcom/msc/a/g;->c()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/telephony/PhoneNumberUtils;->isGlobalPhoneNumber(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_b

    .line 4403
    iget-object v0, p0, Lcom/osp/app/signin/jl;->c:Lcom/osp/app/signin/SignInView;

    invoke-static {v0}, Lcom/osp/app/signin/SignInView;->e(Lcom/osp/app/signin/SignInView;)Z

    move-result v0

    if-eqz v0, :cond_c

    iget-object v0, p0, Lcom/osp/app/signin/jl;->c:Lcom/osp/app/signin/SignInView;

    iget-object v0, v0, Lcom/osp/app/signin/SignInView;->a:Landroid/app/AlertDialog;

    if-eqz v0, :cond_c

    .line 4405
    iget-object v0, p0, Lcom/osp/app/signin/jl;->c:Lcom/osp/app/signin/SignInView;

    invoke-static {v0}, Lcom/osp/app/signin/SignInView;->s(Lcom/osp/app/signin/SignInView;)Landroid/view/View;

    move-result-object v0

    const v1, 0x7f0c0129

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/EditText;

    iget-object v1, p0, Lcom/osp/app/signin/jl;->c:Lcom/osp/app/signin/SignInView;

    invoke-static {v1}, Lcom/osp/app/signin/SignInView;->K(Lcom/osp/app/signin/SignInView;)Lcom/msc/a/g;

    move-result-object v1

    invoke-virtual {v1}, Lcom/msc/a/g;->c()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    .line 4412
    :cond_b
    :goto_2
    iget-object v0, p0, Lcom/osp/app/signin/jl;->c:Lcom/osp/app/signin/SignInView;

    iget-object v1, p0, Lcom/osp/app/signin/jl;->c:Lcom/osp/app/signin/SignInView;

    invoke-static {v1}, Lcom/osp/app/signin/SignInView;->K(Lcom/osp/app/signin/SignInView;)Lcom/msc/a/g;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/osp/app/signin/SignInView;->a(Lcom/osp/app/signin/SignInView;Lcom/msc/a/g;)V

    goto/16 :goto_1

    .line 4408
    :cond_c
    iget-object v0, p0, Lcom/osp/app/signin/jl;->c:Lcom/osp/app/signin/SignInView;

    const v1, 0x7f0c0121

    invoke-virtual {v0, v1}, Lcom/osp/app/signin/SignInView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/EditText;

    iget-object v1, p0, Lcom/osp/app/signin/jl;->c:Lcom/osp/app/signin/SignInView;

    invoke-static {v1}, Lcom/osp/app/signin/SignInView;->K(Lcom/osp/app/signin/SignInView;)Lcom/msc/a/g;

    move-result-object v1

    invoke-virtual {v1}, Lcom/msc/a/g;->c()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    goto :goto_2

    .line 4415
    :cond_d
    invoke-virtual {p0, v6}, Lcom/osp/app/signin/jl;->a(Z)V

    .line 4416
    iget-object v0, p0, Lcom/osp/app/signin/jl;->c:Lcom/osp/app/signin/SignInView;

    invoke-static {v0}, Lcom/osp/app/signin/SignInView;->a(Lcom/osp/app/signin/SignInView;)V

    goto/16 :goto_1
.end method

.method public final b(Lcom/msc/b/c;)V
    .locals 6

    .prologue
    .line 4528
    invoke-super {p0, p1}, Lcom/msc/c/b;->b(Lcom/msc/b/c;)V

    .line 4530
    if-nez p1, :cond_1

    .line 4569
    :cond_0
    :goto_0
    return-void

    .line 4535
    :cond_1
    invoke-virtual {p1}, Lcom/msc/b/c;->a()J

    move-result-wide v0

    .line 4536
    invoke-virtual {p1}, Lcom/msc/b/c;->e()Ljava/lang/String;

    move-result-object v2

    .line 4538
    iget-wide v4, p0, Lcom/osp/app/signin/jl;->d:J

    cmp-long v3, v0, v4

    if-nez v3, :cond_4

    .line 4540
    if-eqz v2, :cond_0

    .line 4544
    :try_start_0
    const-string v0, "SSO_8005"

    iget-object v1, p0, Lcom/osp/app/signin/jl;->b:Lcom/msc/c/f;

    invoke-virtual {v1}, Lcom/msc/c/f;->a()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    const/4 v1, 0x1

    if-ne v0, v1, :cond_2

    .line 4546
    new-instance v0, Lcom/osp/security/time/a;

    iget-object v1, p0, Lcom/osp/app/signin/jl;->c:Lcom/osp/app/signin/SignInView;

    invoke-direct {v0, v1}, Lcom/osp/security/time/a;-><init>(Landroid/content/Context;)V

    .line 4547
    invoke-virtual {v0}, Lcom/osp/security/time/a;->b()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 4556
    :catch_0
    move-exception v0

    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_0

    .line 4548
    :cond_2
    :try_start_1
    const-string v0, "USR_1573"

    iget-object v1, p0, Lcom/osp/app/signin/jl;->b:Lcom/msc/c/f;

    invoke-virtual {v1}, Lcom/msc/c/f;->a()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 4550
    invoke-virtual {p0}, Lcom/osp/app/signin/jl;->isCancelled()Z

    move-result v0

    if-nez v0, :cond_0

    .line 4554
    iget-object v0, p0, Lcom/osp/app/signin/jl;->c:Lcom/osp/app/signin/SignInView;

    invoke-static {v0}, Lcom/osp/app/signin/SignInView;->e(Lcom/osp/app/signin/SignInView;)Z

    move-result v0

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/osp/app/signin/jl;->c:Lcom/osp/app/signin/SignInView;

    invoke-static {v0}, Lcom/osp/app/signin/SignInView;->s(Lcom/osp/app/signin/SignInView;)Landroid/view/View;

    move-result-object v0

    const v1, 0x7f0c0129

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/EditText;

    iget-object v1, p0, Lcom/osp/app/signin/jl;->c:Lcom/osp/app/signin/SignInView;

    invoke-static {v1}, Lcom/osp/app/signin/SignInView;->s(Lcom/osp/app/signin/SignInView;)Landroid/view/View;

    move-result-object v1

    const v2, 0x7f0c012a

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/EditText;

    :goto_1
    invoke-static {}, Lcom/msc/c/g;->a()Lcom/msc/c/g;

    iget-object v2, p0, Lcom/osp/app/signin/jl;->c:Lcom/osp/app/signin/SignInView;

    invoke-virtual {v0}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v2, v0, v1, p0}, Lcom/msc/c/g;->b(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Lcom/msc/b/h;)J

    move-result-wide v0

    iput-wide v0, p0, Lcom/osp/app/signin/jl;->e:J

    iget-wide v0, p0, Lcom/osp/app/signin/jl;->e:J

    invoke-virtual {p0, v0, v1}, Lcom/osp/app/signin/jl;->a(J)V

    iget-wide v0, p0, Lcom/osp/app/signin/jl;->e:J

    const-string v2, "verify_xml"

    invoke-virtual {p0, v0, v1, v2}, Lcom/osp/app/signin/jl;->a(JLjava/lang/String;)V

    iget-wide v0, p0, Lcom/osp/app/signin/jl;->e:J

    sget-object v2, Lcom/msc/b/g;->c:Lcom/msc/b/g;

    invoke-static {v0, v1, v2}, Lcom/msc/c/g;->a(JLcom/msc/b/g;)V

    goto/16 :goto_0

    :cond_3
    iget-object v0, p0, Lcom/osp/app/signin/jl;->c:Lcom/osp/app/signin/SignInView;

    const v1, 0x7f0c0121

    invoke-virtual {v0, v1}, Lcom/osp/app/signin/SignInView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/EditText;

    iget-object v1, p0, Lcom/osp/app/signin/jl;->c:Lcom/osp/app/signin/SignInView;

    const v2, 0x7f0c0122

    invoke-virtual {v1, v2}, Lcom/osp/app/signin/SignInView;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/EditText;
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_1

    .line 4561
    :cond_4
    iget-wide v2, p0, Lcom/osp/app/signin/jl;->f:J

    cmp-long v0, v0, v2

    if-nez v0, :cond_0

    .line 4563
    iget-object v0, p0, Lcom/osp/app/signin/jl;->c:Lcom/osp/app/signin/SignInView;

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lcom/osp/app/signin/SignInView;->d(Lcom/osp/app/signin/SignInView;Ljava/lang/String;)Ljava/lang/String;

    goto/16 :goto_0
.end method

.method protected final d()V
    .locals 1

    .prologue
    .line 4273
    invoke-super {p0}, Lcom/msc/c/b;->d()V

    .line 4274
    iget-object v0, p0, Lcom/osp/app/signin/jl;->c:Lcom/osp/app/signin/SignInView;

    invoke-static {v0}, Lcom/osp/app/signin/SignInView;->a(Lcom/osp/app/signin/SignInView;)V

    .line 4275
    return-void
.end method

.method protected final synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 4244
    invoke-virtual {p0}, Lcom/osp/app/signin/jl;->a()Ljava/lang/Boolean;

    move-result-object v0

    return-object v0
.end method

.method protected final synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 4244
    check-cast p1, Ljava/lang/Boolean;

    invoke-virtual {p0, p1}, Lcom/osp/app/signin/jl;->a(Ljava/lang/Boolean;)V

    return-void
.end method
