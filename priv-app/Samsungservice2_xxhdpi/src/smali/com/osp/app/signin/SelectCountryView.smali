.class public Lcom/osp/app/signin/SelectCountryView;
.super Lcom/osp/app/util/BaseActivity;
.source "SelectCountryView.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# static fields
.field private static Q:F


# instance fields
.field private A:Landroid/widget/LinearLayout;

.field private B:Lcom/osp/app/signin/hl;

.field private C:Landroid/widget/ImageButton;

.field private D:Ljava/lang/String;

.field private E:Ljava/lang/String;

.field private F:Z

.field private G:Z

.field private H:Ljava/lang/String;

.field private I:Ljava/util/Vector;

.field private J:[Ljava/lang/String;

.field private K:I

.field private final L:Ljava/util/List;

.field private M:I

.field private N:I

.field private O:J

.field private P:Ljava/lang/String;

.field private final R:Ljava/util/ArrayList;

.field a:I

.field private final b:Ljava/lang/String;

.field private c:Landroid/widget/EditText;

.field private d:I

.field private e:Ljava/lang/String;

.field private f:Landroid/content/Intent;

.field private y:Landroid/widget/ListView;

.field private z:Lcom/osp/app/signin/ej;


# direct methods
.method public constructor <init>()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    const/4 v1, 0x0

    .line 71
    invoke-direct {p0}, Lcom/osp/app/util/BaseActivity;-><init>()V

    .line 73
    const-string v0, "SCV"

    iput-object v0, p0, Lcom/osp/app/signin/SelectCountryView;->b:Ljava/lang/String;

    .line 91
    const-string v0, ""

    iput-object v0, p0, Lcom/osp/app/signin/SelectCountryView;->e:Ljava/lang/String;

    .line 97
    iput-object v1, p0, Lcom/osp/app/signin/SelectCountryView;->f:Landroid/content/Intent;

    .line 137
    iput-object v1, p0, Lcom/osp/app/signin/SelectCountryView;->D:Ljava/lang/String;

    .line 142
    iput-object v1, p0, Lcom/osp/app/signin/SelectCountryView;->E:Ljava/lang/String;

    .line 147
    iput-boolean v3, p0, Lcom/osp/app/signin/SelectCountryView;->F:Z

    .line 152
    iput-boolean v3, p0, Lcom/osp/app/signin/SelectCountryView;->G:Z

    .line 162
    iput-object v1, p0, Lcom/osp/app/signin/SelectCountryView;->H:Ljava/lang/String;

    .line 180
    iput v2, p0, Lcom/osp/app/signin/SelectCountryView;->K:I

    .line 190
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/osp/app/signin/SelectCountryView;->L:Ljava/util/List;

    .line 516
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/osp/app/signin/SelectCountryView;->R:Ljava/util/ArrayList;

    .line 518
    iput v2, p0, Lcom/osp/app/signin/SelectCountryView;->a:I

    .line 879
    return-void
.end method

.method static synthetic a(F)F
    .locals 0

    .prologue
    .line 71
    sput p0, Lcom/osp/app/signin/SelectCountryView;->Q:F

    return p0
.end method

.method static synthetic a(Lcom/osp/app/signin/SelectCountryView;I)I
    .locals 0

    .prologue
    .line 71
    iput p1, p0, Lcom/osp/app/signin/SelectCountryView;->d:I

    return p1
.end method

.method static synthetic a(Lcom/osp/app/signin/SelectCountryView;Lcom/osp/app/signin/ej;)Lcom/osp/app/signin/ej;
    .locals 0

    .prologue
    .line 71
    iput-object p1, p0, Lcom/osp/app/signin/SelectCountryView;->z:Lcom/osp/app/signin/ej;

    return-object p1
.end method

.method static synthetic a(Lcom/osp/app/signin/SelectCountryView;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 71
    iget-object v0, p0, Lcom/osp/app/signin/SelectCountryView;->P:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic a(Lcom/osp/app/signin/SelectCountryView;Ljava/lang/String;)Ljava/lang/String;
    .locals 0

    .prologue
    .line 71
    iput-object p1, p0, Lcom/osp/app/signin/SelectCountryView;->e:Ljava/lang/String;

    return-object p1
.end method

.method static synthetic a(Lcom/osp/app/signin/SelectCountryView;Ljava/util/Vector;)Ljava/util/Vector;
    .locals 0

    .prologue
    .line 71
    iput-object p1, p0, Lcom/osp/app/signin/SelectCountryView;->I:Ljava/util/Vector;

    return-object p1
.end method

.method static synthetic a(Ljava/lang/String;)Z
    .locals 1

    .prologue
    .line 71
    invoke-static {p0}, Lcom/osp/app/signin/SelectCountryView;->f(Ljava/lang/String;)Z

    move-result v0

    return v0
.end method

.method static synthetic a(Lcom/osp/app/signin/SelectCountryView;[Ljava/lang/String;)[Ljava/lang/String;
    .locals 7

    .prologue
    const/4 v3, 0x0

    .line 71
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v0, "SCV"

    const-string v1, "getIndexedCountry"

    invoke-static {v0, v1}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;)V

    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    const/4 v1, 0x0

    move v2, v3

    :goto_0
    array-length v0, p1

    if-ge v2, v0, :cond_0

    aget-object v5, p1, v2

    const/4 v0, 0x1

    invoke-virtual {v5, v3, v0}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v0

    sget-object v6, Ljava/util/Locale;->ENGLISH:Ljava/util/Locale;

    invoke-virtual {v0, v6}, Ljava/lang/String;->toLowerCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v6

    if-nez v6, :cond_1

    sget-object v1, Ljava/util/Locale;->ENGLISH:Ljava/util/Locale;

    invoke-virtual {v0, v1}, Ljava/lang/String;->toUpperCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v4, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    iget-object v1, p0, Lcom/osp/app/signin/SelectCountryView;->L:Ljava/util/List;

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    invoke-interface {v1, v6}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    :goto_1
    invoke-virtual {v4, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    add-int/lit8 v1, v2, 0x1

    move v2, v1

    move-object v1, v0

    goto :goto_0

    :cond_0
    invoke-virtual {v4}, Ljava/util/ArrayList;->size()I

    move-result v0

    new-array v0, v0, [Ljava/lang/String;

    invoke-virtual {v4, v0}, Ljava/util/ArrayList;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Ljava/lang/String;

    return-object v0

    :cond_1
    move-object v0, v1

    goto :goto_1
.end method

.method static synthetic b(Lcom/osp/app/signin/SelectCountryView;I)I
    .locals 0

    .prologue
    .line 71
    iput p1, p0, Lcom/osp/app/signin/SelectCountryView;->K:I

    return p1
.end method

.method static synthetic b(Lcom/osp/app/signin/SelectCountryView;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 71
    iget-object v0, p0, Lcom/osp/app/signin/SelectCountryView;->D:Ljava/lang/String;

    return-object v0
.end method

.method private static b([Ljava/lang/String;)Ljava/util/ArrayList;
    .locals 10

    .prologue
    const/4 v9, 0x3

    const/4 v8, 0x2

    const/4 v7, 0x1

    const/4 v4, 0x0

    .line 1638
    new-instance v5, Ljava/util/ArrayList;

    invoke-direct {v5}, Ljava/util/ArrayList;-><init>()V

    .line 1642
    const-string v1, ""

    move v3, v4

    move v2, v4

    .line 1645
    :goto_0
    array-length v0, p0

    if-ge v3, v0, :cond_0

    .line 1647
    aget-object v0, p0, v3

    .line 1651
    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-nez v6, :cond_2

    .line 1653
    new-array v6, v9, [Ljava/lang/Object;

    .line 1654
    aput-object v1, v6, v4

    .line 1655
    add-int/lit8 v1, v2, -0x1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    aput-object v1, v6, v7

    .line 1656
    add-int/lit8 v1, v3, -0x1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    aput-object v1, v6, v8

    .line 1659
    add-int/lit8 v1, v3, 0x1

    .line 1660
    invoke-virtual {v5, v6}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1645
    :goto_1
    add-int/lit8 v2, v3, 0x1

    move v3, v2

    move v2, v1

    move-object v1, v0

    goto :goto_0

    .line 1665
    :cond_0
    new-array v0, v9, [Ljava/lang/Object;

    .line 1666
    aput-object v1, v0, v4

    .line 1667
    add-int/lit8 v1, v2, -0x1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    aput-object v1, v0, v7

    .line 1668
    array-length v1, p0

    add-int/lit8 v1, v1, -0x1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    aput-object v1, v0, v8

    .line 1669
    invoke-virtual {v5, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1672
    invoke-virtual {v5}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-lez v0, :cond_1

    .line 1674
    invoke-virtual {v5, v4}, Ljava/util/ArrayList;->remove(I)Ljava/lang/Object;

    .line 1677
    :cond_1
    return-object v5

    :cond_2
    move-object v0, v1

    move v1, v2

    goto :goto_1
.end method

.method static synthetic c(Lcom/osp/app/signin/SelectCountryView;I)I
    .locals 0

    .prologue
    .line 71
    iput p1, p0, Lcom/osp/app/signin/SelectCountryView;->M:I

    return p1
.end method

.method static synthetic c(Lcom/osp/app/signin/SelectCountryView;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 71
    iget-object v0, p0, Lcom/osp/app/signin/SelectCountryView;->E:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic d(Lcom/osp/app/signin/SelectCountryView;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 71
    iget-object v0, p0, Lcom/osp/app/signin/SelectCountryView;->H:Ljava/lang/String;

    return-object v0
.end method

.method private d()V
    .locals 5

    .prologue
    .line 246
    invoke-static {}, Lcom/osp/device/b;->b()Lcom/osp/device/b;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/osp/device/b;->i(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 258
    invoke-virtual {p0}, Lcom/osp/app/signin/SelectCountryView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f080137

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v2

    .line 259
    invoke-virtual {p0}, Lcom/osp/app/signin/SelectCountryView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f080138

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    .line 261
    invoke-virtual {p0}, Lcom/osp/app/signin/SelectCountryView;->o()I

    move-result v0

    .line 263
    invoke-static {}, Lcom/osp/app/signin/SamsungService;->a()Z

    move-result v3

    if-nez v3, :cond_0

    invoke-static {p0}, Lcom/osp/app/util/r;->k(Landroid/content/Context;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 265
    const/4 v0, 0x0

    .line 268
    :cond_0
    invoke-virtual {p0}, Lcom/osp/app/signin/SelectCountryView;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    invoke-virtual {v3}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v3

    .line 270
    iget v3, v3, Landroid/util/DisplayMetrics;->heightPixels:I

    sub-int v0, v3, v0

    .line 272
    if-ge v0, v1, :cond_2

    .line 274
    const/4 v0, -0x1

    .line 277
    :goto_0
    invoke-virtual {p0}, Lcom/osp/app/signin/SelectCountryView;->getWindow()Landroid/view/Window;

    move-result-object v1

    invoke-virtual {v1, v2, v0}, Landroid/view/Window;->setLayout(II)V

    .line 279
    invoke-static {p0}, Lcom/osp/app/signin/SelectCountryView;->a(Landroid/app/Activity;)V

    .line 281
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v1, "SCV"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "setDialogSize()- "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ", "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 285
    :cond_1
    return-void

    :cond_2
    move v0, v1

    goto :goto_0
.end method

.method static synthetic e(Lcom/osp/app/signin/SelectCountryView;)Landroid/content/Intent;
    .locals 1

    .prologue
    .line 71
    iget-object v0, p0, Lcom/osp/app/signin/SelectCountryView;->f:Landroid/content/Intent;

    return-object v0
.end method

.method static synthetic f(Lcom/osp/app/signin/SelectCountryView;)J
    .locals 2

    .prologue
    .line 71
    iget-wide v0, p0, Lcom/osp/app/signin/SelectCountryView;->O:J

    return-wide v0
.end method

.method static synthetic g(Lcom/osp/app/signin/SelectCountryView;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 71
    invoke-virtual {p0}, Lcom/osp/app/signin/SelectCountryView;->n()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method static synthetic h(Lcom/osp/app/signin/SelectCountryView;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 71
    iget-object v0, p0, Lcom/osp/app/signin/SelectCountryView;->e:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic i(Lcom/osp/app/signin/SelectCountryView;)I
    .locals 1

    .prologue
    .line 71
    iget v0, p0, Lcom/osp/app/signin/SelectCountryView;->d:I

    return v0
.end method

.method static synthetic j(Lcom/osp/app/signin/SelectCountryView;)Ljava/util/Vector;
    .locals 1

    .prologue
    .line 71
    iget-object v0, p0, Lcom/osp/app/signin/SelectCountryView;->I:Ljava/util/Vector;

    return-object v0
.end method

.method static synthetic k(Lcom/osp/app/signin/SelectCountryView;)Landroid/widget/EditText;
    .locals 1

    .prologue
    .line 71
    iget-object v0, p0, Lcom/osp/app/signin/SelectCountryView;->c:Landroid/widget/EditText;

    return-object v0
.end method

.method static synthetic l(Lcom/osp/app/signin/SelectCountryView;)I
    .locals 1

    .prologue
    .line 71
    iget v0, p0, Lcom/osp/app/signin/SelectCountryView;->K:I

    return v0
.end method

.method static synthetic m(Lcom/osp/app/signin/SelectCountryView;)Z
    .locals 1

    .prologue
    .line 71
    invoke-virtual {p0}, Lcom/osp/app/signin/SelectCountryView;->h()Z

    move-result v0

    return v0
.end method

.method static synthetic n(Lcom/osp/app/signin/SelectCountryView;)Z
    .locals 1

    .prologue
    .line 71
    invoke-virtual {p0}, Lcom/osp/app/signin/SelectCountryView;->h()Z

    move-result v0

    return v0
.end method

.method static synthetic o(Lcom/osp/app/signin/SelectCountryView;)Lcom/osp/app/signin/ej;
    .locals 1

    .prologue
    .line 71
    iget-object v0, p0, Lcom/osp/app/signin/SelectCountryView;->z:Lcom/osp/app/signin/ej;

    return-object v0
.end method

.method static synthetic p(Lcom/osp/app/signin/SelectCountryView;)Landroid/widget/ListView;
    .locals 1

    .prologue
    .line 71
    iget-object v0, p0, Lcom/osp/app/signin/SelectCountryView;->y:Landroid/widget/ListView;

    return-object v0
.end method

.method static synthetic q(Lcom/osp/app/signin/SelectCountryView;)Z
    .locals 1

    .prologue
    .line 71
    iget-boolean v0, p0, Lcom/osp/app/signin/SelectCountryView;->F:Z

    return v0
.end method

.method static synthetic r(Lcom/osp/app/signin/SelectCountryView;)Z
    .locals 1

    .prologue
    .line 71
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/osp/app/signin/SelectCountryView;->F:Z

    return v0
.end method

.method static synthetic s(Lcom/osp/app/signin/SelectCountryView;)Z
    .locals 1

    .prologue
    .line 71
    iget-boolean v0, p0, Lcom/osp/app/signin/SelectCountryView;->G:Z

    return v0
.end method

.method static synthetic t(Lcom/osp/app/signin/SelectCountryView;)Z
    .locals 1

    .prologue
    .line 71
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/osp/app/signin/SelectCountryView;->G:Z

    return v0
.end method

.method static synthetic u(Lcom/osp/app/signin/SelectCountryView;)V
    .locals 3

    .prologue
    .line 71
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v0, "SCV"

    const-string v1, "closeIME"

    invoke-static {v0, v1}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;)V

    const-string v0, "input_method"

    invoke-virtual {p0, v0}, Lcom/osp/app/signin/SelectCountryView;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/inputmethod/InputMethodManager;

    iget-object v1, p0, Lcom/osp/app/signin/SelectCountryView;->c:Landroid/widget/EditText;

    invoke-virtual {v1}, Landroid/widget/EditText;->hasFocus()Z

    move-result v1

    const/4 v2, 0x1

    if-ne v1, v2, :cond_0

    iget-object v1, p0, Lcom/osp/app/signin/SelectCountryView;->c:Landroid/widget/EditText;

    invoke-virtual {v1}, Landroid/widget/EditText;->getWindowToken()Landroid/os/IBinder;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/view/inputmethod/InputMethodManager;->hideSoftInputFromWindow(Landroid/os/IBinder;I)Z

    :cond_0
    return-void
.end method

.method static synthetic v(Lcom/osp/app/signin/SelectCountryView;)Landroid/widget/LinearLayout;
    .locals 1

    .prologue
    .line 71
    iget-object v0, p0, Lcom/osp/app/signin/SelectCountryView;->A:Landroid/widget/LinearLayout;

    return-object v0
.end method

.method static synthetic w(Lcom/osp/app/signin/SelectCountryView;)Landroid/widget/ImageButton;
    .locals 1

    .prologue
    .line 71
    iget-object v0, p0, Lcom/osp/app/signin/SelectCountryView;->C:Landroid/widget/ImageButton;

    return-object v0
.end method

.method static synthetic x(Lcom/osp/app/signin/SelectCountryView;)Ljava/util/ArrayList;
    .locals 1

    .prologue
    .line 71
    iget-object v0, p0, Lcom/osp/app/signin/SelectCountryView;->R:Ljava/util/ArrayList;

    return-object v0
.end method

.method static synthetic y(Lcom/osp/app/signin/SelectCountryView;)Z
    .locals 1

    .prologue
    .line 71
    invoke-virtual {p0}, Lcom/osp/app/signin/SelectCountryView;->h()Z

    move-result v0

    return v0
.end method


# virtual methods
.method protected final a()V
    .locals 0

    .prologue
    .line 1763
    return-void
.end method

.method public final a(Ljava/util/ArrayList;)V
    .locals 1

    .prologue
    .line 1569
    if-eqz p1, :cond_0

    .line 1571
    new-instance v0, Lcom/osp/app/signin/hj;

    invoke-direct {v0, p0}, Lcom/osp/app/signin/hj;-><init>(Lcom/osp/app/signin/SelectCountryView;)V

    .line 1579
    invoke-static {p1, v0}, Ljava/util/Collections;->sort(Ljava/util/List;Ljava/util/Comparator;)V

    .line 1583
    :cond_0
    return-void
.end method

.method protected final b()V
    .locals 0

    .prologue
    .line 1807
    return-void
.end method

.method public final c()V
    .locals 4

    .prologue
    .line 1605
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v0, "SCV"

    const-string v1, " displayListItem"

    invoke-static {v0, v1}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 1607
    iget v0, p0, Lcom/osp/app/signin/SelectCountryView;->M:I

    int-to-double v0, v0

    iget v2, p0, Lcom/osp/app/signin/SelectCountryView;->N:I

    int-to-double v2, v2

    div-double/2addr v0, v2

    .line 1610
    sget v2, Lcom/osp/app/signin/SelectCountryView;->Q:F

    float-to-double v2, v2

    div-double v0, v2, v0

    double-to-int v0, v0

    .line 1612
    if-gez v0, :cond_1

    .line 1614
    const/4 v0, 0x0

    move v1, v0

    .line 1620
    :goto_0
    iget-object v0, p0, Lcom/osp/app/signin/SelectCountryView;->L:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    add-int/2addr v0, v1

    .line 1622
    iget v1, p0, Lcom/osp/app/signin/SelectCountryView;->K:I

    if-le v0, v1, :cond_0

    .line 1624
    iget v0, p0, Lcom/osp/app/signin/SelectCountryView;->K:I

    .line 1627
    :cond_0
    iget-object v1, p0, Lcom/osp/app/signin/SelectCountryView;->y:Landroid/widget/ListView;

    invoke-virtual {v1, v0}, Landroid/widget/ListView;->setSelection(I)V

    .line 1628
    return-void

    .line 1615
    :cond_1
    iget-object v1, p0, Lcom/osp/app/signin/SelectCountryView;->L:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    if-lt v0, v1, :cond_2

    .line 1617
    iget-object v0, p0, Lcom/osp/app/signin/SelectCountryView;->L:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    move v1, v0

    goto :goto_0

    :cond_2
    move v1, v0

    goto :goto_0
.end method

.method public onBackPressed()V
    .locals 2

    .prologue
    .line 1587
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v0, "SCV"

    const-string v1, " onBackPressed"

    invoke-static {v0, v1}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 1588
    iget-object v0, p0, Lcom/osp/app/signin/SelectCountryView;->B:Lcom/osp/app/signin/hl;

    if-eqz v0, :cond_0

    .line 1590
    iget-object v0, p0, Lcom/osp/app/signin/SelectCountryView;->B:Lcom/osp/app/signin/hl;

    invoke-virtual {v0}, Lcom/osp/app/signin/hl;->getStatus()Landroid/os/AsyncTask$Status;

    move-result-object v0

    sget-object v1, Landroid/os/AsyncTask$Status;->FINISHED:Landroid/os/AsyncTask$Status;

    if-eq v0, v1, :cond_0

    .line 1592
    iget-object v0, p0, Lcom/osp/app/signin/SelectCountryView;->B:Lcom/osp/app/signin/hl;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/osp/app/signin/hl;->cancel(Z)Z

    .line 1596
    :cond_0
    const/16 v0, 0xe

    iget-object v1, p0, Lcom/osp/app/signin/SelectCountryView;->f:Landroid/content/Intent;

    invoke-virtual {p0, v0, v1}, Lcom/osp/app/signin/SelectCountryView;->a(ILandroid/content/Intent;)V

    .line 1597
    invoke-super {p0}, Lcom/osp/app/util/BaseActivity;->onBackPressed()V

    .line 1598
    return-void
.end method

.method public onClick(Landroid/view/View;)V
    .locals 2

    .prologue
    .line 1540
    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    .line 1560
    :cond_0
    :goto_0
    return-void

    .line 1548
    :pswitch_0
    iget-object v0, p0, Lcom/osp/app/signin/SelectCountryView;->c:Landroid/widget/EditText;

    invoke-virtual {v0}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-interface {v0}, Landroid/text/Editable;->length()I

    move-result v0

    if-eqz v0, :cond_0

    .line 1550
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v0, "SCV"

    const-string v1, " onClick delete button "

    invoke-static {v0, v1}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 1551
    iget-object v0, p0, Lcom/osp/app/signin/SelectCountryView;->c:Landroid/widget/EditText;

    const-string v1, ""

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    .line 1553
    iget-object v0, p0, Lcom/osp/app/signin/SelectCountryView;->C:Landroid/widget/ImageButton;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setVisibility(I)V

    goto :goto_0

    .line 1540
    nop

    :pswitch_data_0
    .packed-switch 0x7f0c00bb
        :pswitch_0
    .end packed-switch
.end method

.method public onConfigurationChanged(Landroid/content/res/Configuration;)V
    .locals 2

    .prologue
    .line 219
    invoke-super {p0, p1}, Lcom/osp/app/util/BaseActivity;->onConfigurationChanged(Landroid/content/res/Configuration;)V

    .line 226
    iget-object v0, p0, Lcom/osp/app/signin/SelectCountryView;->y:Landroid/widget/ListView;

    invoke-virtual {v0}, Landroid/widget/ListView;->isFastScrollEnabled()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 228
    iget-object v0, p0, Lcom/osp/app/signin/SelectCountryView;->y:Landroid/widget/ListView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setFastScrollEnabled(Z)V

    .line 230
    :cond_0
    iget-object v0, p0, Lcom/osp/app/signin/SelectCountryView;->y:Landroid/widget/ListView;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setFastScrollEnabled(Z)V

    .line 231
    iget-object v0, p0, Lcom/osp/app/signin/SelectCountryView;->z:Lcom/osp/app/signin/ej;

    if-eqz v0, :cond_1

    .line 233
    iget-object v0, p0, Lcom/osp/app/signin/SelectCountryView;->z:Lcom/osp/app/signin/ej;

    invoke-virtual {v0}, Lcom/osp/app/signin/ej;->notifyDataSetChanged()V

    .line 236
    :cond_1
    invoke-direct {p0}, Lcom/osp/app/signin/SelectCountryView;->d()V

    .line 238
    return-void
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 8

    .prologue
    const/16 v7, 0xfa

    const/4 v6, 0x0

    .line 289
    invoke-super {p0, p1}, Lcom/osp/app/util/BaseActivity;->onCreate(Landroid/os/Bundle;)V

    .line 291
    const v0, 0x7f030018

    invoke-virtual {p0, v0}, Lcom/osp/app/signin/SelectCountryView;->setContentView(I)V

    .line 293
    const v0, 0x7f0c00b9

    invoke-virtual {p0, v0}, Lcom/osp/app/signin/SelectCountryView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/FrameLayout;

    if-eqz v0, :cond_0

    invoke-static {p0}, Lcom/osp/app/util/an;->a(Lcom/osp/app/util/BaseActivity;)Lcom/osp/app/util/an;

    const v1, 0x7f02013e

    invoke-static {v1}, Lcom/osp/app/util/an;->a(I)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/FrameLayout;->setBackgroundResource(I)V

    invoke-static {}, Lcom/osp/app/signin/SamsungService;->a()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-static {p0}, Lcom/osp/app/util/r;->m(Landroid/content/Context;)Z

    move-result v1

    if-eqz v1, :cond_0

    const v1, 0x106000d

    invoke-virtual {v0, v1}, Landroid/widget/FrameLayout;->setBackgroundResource(I)V

    :cond_0
    const v0, 0x7f0c00ba

    invoke-virtual {p0, v0}, Lcom/osp/app/signin/SelectCountryView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/EditText;

    iput-object v0, p0, Lcom/osp/app/signin/SelectCountryView;->c:Landroid/widget/EditText;

    const v0, 0x7f0c00bb

    invoke-virtual {p0, v0}, Lcom/osp/app/signin/SelectCountryView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageButton;

    iput-object v0, p0, Lcom/osp/app/signin/SelectCountryView;->C:Landroid/widget/ImageButton;

    iget-object v0, p0, Lcom/osp/app/signin/SelectCountryView;->C:Landroid/widget/ImageButton;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/osp/app/signin/SelectCountryView;->C:Landroid/widget/ImageButton;

    invoke-static {p0}, Lcom/osp/app/util/an;->a(Lcom/osp/app/util/BaseActivity;)Lcom/osp/app/util/an;

    const v1, 0x7f0200cc

    invoke-static {v1}, Lcom/osp/app/util/an;->a(I)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setImageResource(I)V

    iget-object v0, p0, Lcom/osp/app/signin/SelectCountryView;->C:Landroid/widget/ImageButton;

    invoke-static {p0}, Lcom/osp/app/util/an;->a(Lcom/osp/app/util/BaseActivity;)Lcom/osp/app/util/an;

    const v1, 0x7f02008b

    invoke-static {v1}, Lcom/osp/app/util/an;->a(I)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setBackgroundResource(I)V

    iget-object v0, p0, Lcom/osp/app/signin/SelectCountryView;->C:Landroid/widget/ImageButton;

    invoke-static {v0}, Lcom/msc/sa/c/d;->a(Landroid/view/View;)V

    :cond_1
    const v0, 0x7f0c00bc

    invoke-virtual {p0, v0}, Lcom/osp/app/signin/SelectCountryView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ListView;

    iput-object v0, p0, Lcom/osp/app/signin/SelectCountryView;->y:Landroid/widget/ListView;

    iget-object v0, p0, Lcom/osp/app/signin/SelectCountryView;->y:Landroid/widget/ListView;

    const v0, 0x7f0c00bd

    invoke-virtual {p0, v0}, Lcom/osp/app/signin/SelectCountryView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcom/osp/app/signin/SelectCountryView;->A:Landroid/widget/LinearLayout;

    iget-object v0, p0, Lcom/osp/app/signin/SelectCountryView;->A:Landroid/widget/LinearLayout;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/osp/app/signin/SelectCountryView;->A:Landroid/widget/LinearLayout;

    invoke-static {p0}, Lcom/osp/app/util/an;->a(Lcom/osp/app/util/BaseActivity;)Lcom/osp/app/util/an;

    const v1, 0x7f02013a

    invoke-static {v1}, Lcom/osp/app/util/an;->a(I)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setBackgroundResource(I)V

    .line 295
    :cond_2
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v0, "SCV"

    const-string v1, "initComponent start"

    invoke-static {v0, v1}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/osp/app/signin/SelectCountryView;->c:Landroid/widget/EditText;

    invoke-virtual {v0, v6}, Landroid/widget/EditText;->setFocusable(Z)V

    iget-object v0, p0, Lcom/osp/app/signin/SelectCountryView;->c:Landroid/widget/EditText;

    invoke-virtual {v0, v6}, Landroid/widget/EditText;->setFocusableInTouchMode(Z)V

    iget-object v0, p0, Lcom/osp/app/signin/SelectCountryView;->C:Landroid/widget/ImageButton;

    invoke-virtual {v0, p0}, Landroid/widget/ImageButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    new-instance v1, Landroid/text/SpannableStringBuilder;

    const-string v0, "  "

    invoke-direct {v1, v0}, Landroid/text/SpannableStringBuilder;-><init>(Ljava/lang/CharSequence;)V

    const v0, 0x7f09008f

    invoke-virtual {p0, v0}, Lcom/osp/app/signin/SelectCountryView;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Landroid/text/SpannableStringBuilder;->append(Ljava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;

    invoke-virtual {p0}, Lcom/osp/app/signin/SelectCountryView;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-virtual {p0}, Lcom/osp/app/signin/SelectCountryView;->h()Z

    move-result v0

    if-eqz v0, :cond_5

    const v0, 0x7f020134

    :goto_0
    invoke-virtual {v2, v0}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    iget-object v2, p0, Lcom/osp/app/signin/SelectCountryView;->c:Landroid/widget/EditText;

    invoke-virtual {v2}, Landroid/widget/EditText;->getTextSize()F

    move-result v2

    float-to-double v2, v2

    const-wide/high16 v4, 0x3ff4000000000000L    # 1.25

    mul-double/2addr v2, v4

    double-to-int v2, v2

    invoke-virtual {v0, v6, v6, v2, v2}, Landroid/graphics/drawable/Drawable;->setBounds(IIII)V

    new-instance v2, Landroid/text/style/ImageSpan;

    invoke-direct {v2, v0}, Landroid/text/style/ImageSpan;-><init>(Landroid/graphics/drawable/Drawable;)V

    const/4 v0, 0x1

    const/16 v3, 0x21

    invoke-virtual {v1, v2, v6, v0, v3}, Landroid/text/SpannableStringBuilder;->setSpan(Ljava/lang/Object;III)V

    iget-object v0, p0, Lcom/osp/app/signin/SelectCountryView;->c:Landroid/widget/EditText;

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setHint(Ljava/lang/CharSequence;)V

    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x15

    if-ne v0, v1, :cond_3

    iget-object v0, p0, Lcom/osp/app/signin/SelectCountryView;->c:Landroid/widget/EditText;

    const/16 v1, 0xa6

    invoke-static {v1, v6, v6, v6}, Landroid/graphics/Color;->argb(IIII)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setHintTextColor(I)V

    invoke-static {}, Lcom/osp/app/signin/SamsungService;->a()Z

    move-result v0

    if-nez v0, :cond_3

    iget-object v0, p0, Lcom/osp/app/signin/SelectCountryView;->c:Landroid/widget/EditText;

    invoke-static {v7, v7, v7}, Landroid/graphics/Color;->rgb(III)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setBackgroundColor(I)V

    :cond_3
    iget-object v0, p0, Lcom/osp/app/signin/SelectCountryView;->c:Landroid/widget/EditText;

    new-instance v1, Lcom/osp/app/signin/hi;

    invoke-direct {v1, p0}, Lcom/osp/app/signin/hi;-><init>(Lcom/osp/app/signin/SelectCountryView;)V

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setOnEditorActionListener(Landroid/widget/TextView$OnEditorActionListener;)V

    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v0, "SCV"

    const-string v1, "initComponent finish"

    invoke-static {v0, v1}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 297
    invoke-direct {p0}, Lcom/osp/app/signin/SelectCountryView;->d()V

    .line 302
    invoke-virtual {p0}, Lcom/osp/app/signin/SelectCountryView;->getIntent()Landroid/content/Intent;

    move-result-object v0

    iput-object v0, p0, Lcom/osp/app/signin/SelectCountryView;->f:Landroid/content/Intent;

    .line 304
    iget-object v0, p0, Lcom/osp/app/signin/SelectCountryView;->f:Landroid/content/Intent;

    const-string v1, "MODE"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/osp/app/signin/SelectCountryView;->D:Ljava/lang/String;

    .line 305
    iget-object v0, p0, Lcom/osp/app/signin/SelectCountryView;->f:Landroid/content/Intent;

    const-string v1, "BG_WhoareU"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/osp/app/signin/SelectCountryView;->E:Ljava/lang/String;

    .line 308
    iget-object v0, p0, Lcom/osp/app/signin/SelectCountryView;->f:Landroid/content/Intent;

    const-string v1, "key_country_list_mode"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/osp/app/signin/SelectCountryView;->P:Ljava/lang/String;

    .line 310
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "[SCV] mUsermode : "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v1, p0, Lcom/osp/app/signin/SelectCountryView;->D:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;)V

    .line 311
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "[SCV] mWHOAREU : "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v1, p0, Lcom/osp/app/signin/SelectCountryView;->E:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;)V

    .line 329
    const-string v0, "BG_mode"

    iget-object v1, p0, Lcom/osp/app/signin/SelectCountryView;->E:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 331
    iget-object v0, p0, Lcom/osp/app/signin/SelectCountryView;->f:Landroid/content/Intent;

    const-string v1, "mypackage"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/osp/app/signin/SelectCountryView;->H:Ljava/lang/String;

    .line 332
    iget-object v0, p0, Lcom/osp/app/signin/SelectCountryView;->f:Landroid/content/Intent;

    const-string v1, "key_request_id"

    const-wide/16 v2, 0x0

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/Intent;->getLongExtra(Ljava/lang/String;J)J

    move-result-wide v0

    iput-wide v0, p0, Lcom/osp/app/signin/SelectCountryView;->O:J

    .line 333
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "[SCV]mSourcepackage"

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v1, p0, Lcom/osp/app/signin/SelectCountryView;->H:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;)V

    .line 334
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "[SCV]mRequestId"

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-wide v2, p0, Lcom/osp/app/signin/SelectCountryView;->O:J

    invoke-virtual {v0, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;)V

    .line 338
    :cond_4
    new-instance v0, Lcom/osp/app/signin/hl;

    invoke-direct {v0, p0}, Lcom/osp/app/signin/hl;-><init>(Lcom/osp/app/signin/SelectCountryView;)V

    iput-object v0, p0, Lcom/osp/app/signin/SelectCountryView;->B:Lcom/osp/app/signin/hl;

    .line 339
    iget-object v0, p0, Lcom/osp/app/signin/SelectCountryView;->B:Lcom/osp/app/signin/hl;

    new-array v1, v6, [Ljava/lang/Void;

    invoke-virtual {v0, v1}, Lcom/osp/app/signin/hl;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    .line 342
    const-string v0, "language_list"

    iget-object v1, p0, Lcom/osp/app/signin/SelectCountryView;->P:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_6

    .line 344
    const v0, 0x7f0900b9

    invoke-virtual {p0, v0}, Lcom/osp/app/signin/SelectCountryView;->setTitle(I)V

    .line 366
    :goto_1
    invoke-static {p0}, Lcom/msc/sa/c/d;->a(Landroid/app/Activity;)V

    .line 368
    return-void

    .line 295
    :cond_5
    const v0, 0x7f020135

    goto/16 :goto_0

    .line 348
    :cond_6
    const v0, 0x7f09001f

    invoke-virtual {p0, v0}, Lcom/osp/app/signin/SelectCountryView;->setTitle(I)V

    goto :goto_1
.end method

.method protected onDestroy()V
    .locals 2

    .prologue
    .line 691
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v0, "SCV"

    const-string v1, "onDestroy"

    invoke-static {v0, v1}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 692
    iget-object v0, p0, Lcom/osp/app/signin/SelectCountryView;->B:Lcom/osp/app/signin/hl;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/osp/app/signin/SelectCountryView;->B:Lcom/osp/app/signin/hl;

    invoke-virtual {v0}, Lcom/osp/app/signin/hl;->getStatus()Landroid/os/AsyncTask$Status;

    move-result-object v0

    sget-object v1, Landroid/os/AsyncTask$Status;->RUNNING:Landroid/os/AsyncTask$Status;

    if-ne v0, v1, :cond_0

    .line 694
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v0, "SCV"

    const-string v1, "cancelTask"

    invoke-static {v0, v1}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 695
    iget-object v0, p0, Lcom/osp/app/signin/SelectCountryView;->B:Lcom/osp/app/signin/hl;

    invoke-virtual {v0}, Lcom/osp/app/signin/hl;->d()V

    .line 696
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/osp/app/signin/SelectCountryView;->B:Lcom/osp/app/signin/hl;

    .line 698
    :cond_0
    invoke-super {p0}, Lcom/osp/app/util/BaseActivity;->onDestroy()V

    .line 699
    return-void
.end method

.method public onOptionsItemSelected(Landroid/view/MenuItem;)Z
    .locals 2

    .prologue
    .line 1788
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0xb

    if-lt v0, v1, :cond_0

    .line 1790
    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    .line 1794
    :cond_0
    :goto_0
    invoke-super {p0, p1}, Lcom/osp/app/util/BaseActivity;->onOptionsItemSelected(Landroid/view/MenuItem;)Z

    move-result v0

    return v0

    .line 1793
    :pswitch_0
    invoke-virtual {p0}, Lcom/osp/app/signin/SelectCountryView;->onBackPressed()V

    goto :goto_0

    .line 1790
    :pswitch_data_0
    .packed-switch 0x102002c
        :pswitch_0
    .end packed-switch
.end method

.method protected onPause()V
    .locals 2

    .prologue
    .line 1778
    iget-object v0, p0, Lcom/osp/app/signin/SelectCountryView;->y:Landroid/widget/ListView;

    if-eqz v0, :cond_0

    .line 1780
    iget-object v0, p0, Lcom/osp/app/signin/SelectCountryView;->y:Landroid/widget/ListView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setFastScrollEnabled(Z)V

    .line 1782
    :cond_0
    invoke-super {p0}, Lcom/osp/app/util/BaseActivity;->onPause()V

    .line 1783
    return-void
.end method

.method protected onResume()V
    .locals 2

    .prologue
    .line 1767
    iget-object v0, p0, Lcom/osp/app/signin/SelectCountryView;->y:Landroid/widget/ListView;

    if-eqz v0, :cond_0

    .line 1769
    iget-object v0, p0, Lcom/osp/app/signin/SelectCountryView;->y:Landroid/widget/ListView;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setFastScrollEnabled(Z)V

    .line 1773
    :cond_0
    invoke-super {p0}, Lcom/osp/app/util/BaseActivity;->onResume()V

    .line 1774
    return-void
.end method

.method protected onStop()V
    .locals 2

    .prologue
    .line 372
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v0, "SCV"

    const-string v1, "onStop"

    invoke-static {v0, v1}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 380
    invoke-super {p0}, Lcom/osp/app/util/BaseActivity;->onStop()V

    .line 381
    return-void
.end method

.method public onWindowFocusChanged(Z)V
    .locals 9

    .prologue
    const/4 v8, -0x2

    const/4 v1, 0x0

    .line 423
    invoke-super {p0, p1}, Lcom/osp/app/util/BaseActivity;->onWindowFocusChanged(Z)V

    .line 425
    iget-object v0, p0, Lcom/osp/app/signin/SelectCountryView;->J:[Ljava/lang/String;

    if-eqz v0, :cond_3

    .line 427
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v0, "SCV"

    const-string v2, "getDisplayListOnChange"

    invoke-static {v0, v2}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/osp/app/signin/SelectCountryView;->A:Landroid/widget/LinearLayout;

    invoke-virtual {v0}, Landroid/widget/LinearLayout;->getHeight()I

    move-result v0

    iput v0, p0, Lcom/osp/app/signin/SelectCountryView;->M:I

    iget v0, p0, Lcom/osp/app/signin/SelectCountryView;->M:I

    if-nez v0, :cond_0

    invoke-virtual {p0}, Lcom/osp/app/signin/SelectCountryView;->getWindowManager()Landroid/view/WindowManager;

    move-result-object v0

    invoke-interface {v0}, Landroid/view/WindowManager;->getDefaultDisplay()Landroid/view/Display;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/Display;->getHeight()I

    move-result v0

    iput v0, p0, Lcom/osp/app/signin/SelectCountryView;->M:I

    :cond_0
    iget-object v0, p0, Lcom/osp/app/signin/SelectCountryView;->A:Landroid/widget/LinearLayout;

    invoke-virtual {v0}, Landroid/widget/LinearLayout;->removeAllViews()V

    const-string v0, "SCV"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "mCountryListArray LENGTH"

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v3, p0, Lcom/osp/app/signin/SelectCountryView;->J:[Ljava/lang/String;

    array-length v3, v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v0, p0, Lcom/osp/app/signin/SelectCountryView;->J:[Ljava/lang/String;

    array-length v0, v0

    new-array v2, v0, [Ljava/lang/String;

    move v0, v1

    :goto_0
    iget-object v3, p0, Lcom/osp/app/signin/SelectCountryView;->J:[Ljava/lang/String;

    array-length v3, v3

    if-ge v0, v3, :cond_1

    iget-object v3, p0, Lcom/osp/app/signin/SelectCountryView;->J:[Ljava/lang/String;

    aget-object v3, v3, v0

    const/4 v4, 0x1

    invoke-virtual {v3, v1, v4}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v3

    aput-object v3, v2, v0

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_1
    invoke-static {v2}, Lcom/osp/app/signin/SelectCountryView;->b([Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v4

    invoke-virtual {v4}, Ljava/util/ArrayList;->size()I

    move-result v0

    iput v0, p0, Lcom/osp/app/signin/SelectCountryView;->N:I

    iget v0, p0, Lcom/osp/app/signin/SelectCountryView;->N:I

    iget v2, p0, Lcom/osp/app/signin/SelectCountryView;->N:I

    int-to-float v2, v2

    int-to-float v0, v0

    div-float v5, v2, v0

    const-wide/high16 v2, 0x3ff0000000000000L    # 1.0

    :goto_1
    iget v0, p0, Lcom/osp/app/signin/SelectCountryView;->N:I

    int-to-double v6, v0

    cmpg-double v0, v2, v6

    if-gtz v0, :cond_2

    double-to-int v0, v2

    add-int/lit8 v0, v0, -0x1

    invoke-virtual {v4, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Ljava/lang/Object;

    aget-object v0, v0, v1

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    new-instance v6, Landroid/widget/TextView;

    invoke-direct {v6, p0}, Landroid/widget/TextView;-><init>(Landroid/content/Context;)V

    invoke-virtual {v6, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    const/16 v0, 0x11

    invoke-virtual {v6, v0}, Landroid/widget/TextView;->setGravity(I)V

    const/4 v0, -0x1

    invoke-virtual {v6, v0}, Landroid/widget/TextView;->setTextColor(I)V

    new-instance v0, Landroid/widget/LinearLayout$LayoutParams;

    const/high16 v7, 0x3f800000    # 1.0f

    invoke-direct {v0, v8, v8, v7}, Landroid/widget/LinearLayout$LayoutParams;-><init>(IIF)V

    invoke-virtual {v6, v0}, Landroid/widget/TextView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    iget-object v0, p0, Lcom/osp/app/signin/SelectCountryView;->A:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v6}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    float-to-double v6, v5

    add-double/2addr v2, v6

    goto :goto_1

    :cond_2
    iget-object v0, p0, Lcom/osp/app/signin/SelectCountryView;->A:Landroid/widget/LinearLayout;

    new-instance v1, Lcom/osp/app/signin/hk;

    invoke-direct {v1, p0}, Lcom/osp/app/signin/hk;-><init>(Lcom/osp/app/signin/SelectCountryView;)V

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 429
    :cond_3
    return-void
.end method
