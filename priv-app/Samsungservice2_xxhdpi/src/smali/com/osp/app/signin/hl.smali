.class final Lcom/osp/app/signin/hl;
.super Lcom/msc/c/b;
.source "SelectCountryView.java"


# instance fields
.field protected c:Ljava/util/ArrayList;

.field final synthetic d:Lcom/osp/app/signin/SelectCountryView;

.field private e:J

.field private f:J

.field private g:[Ljava/lang/String;


# direct methods
.method public constructor <init>(Lcom/osp/app/signin/SelectCountryView;)V
    .locals 0

    .prologue
    .line 904
    iput-object p1, p0, Lcom/osp/app/signin/hl;->d:Lcom/osp/app/signin/SelectCountryView;

    .line 905
    invoke-direct {p0, p1}, Lcom/msc/c/b;-><init>(Landroid/content/Context;)V

    .line 911
    return-void
.end method

.method private a(Ljava/util/ArrayList;)Ljava/util/ArrayList;
    .locals 9

    .prologue
    const/4 v4, 0x0

    .line 966
    if-nez p1, :cond_0

    .line 968
    const/4 p1, 0x0

    .line 1010
    :goto_0
    return-object p1

    .line 974
    :cond_0
    invoke-virtual {p1}, Ljava/util/ArrayList;->size()I

    move-result v5

    move v3, v4

    .line 980
    :goto_1
    if-ge v3, v5, :cond_2

    .line 982
    invoke-virtual {p1, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/osp/app/countrylist/CountryInfoItem;

    .line 984
    invoke-virtual {v0}, Lcom/osp/app/countrylist/CountryInfoItem;->c()Ljava/lang/String;

    move-result-object v2

    .line 986
    const/16 v1, 0x5f

    invoke-virtual {v2, v1}, Ljava/lang/String;->indexOf(I)I

    move-result v6

    .line 988
    if-lez v6, :cond_1

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v1

    if-ge v6, v1, :cond_1

    .line 990
    invoke-virtual {v2, v4, v6}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v1

    .line 991
    add-int/lit8 v6, v6, 0x1

    invoke-virtual {v2, v6}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v2

    .line 998
    :goto_2
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    new-instance v6, Ljava/lang/StringBuilder;

    const-string v7, "language="

    invoke-direct {v6, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v6, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, ", country="

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v6}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;)V

    .line 1000
    new-instance v6, Ljava/util/Locale;

    invoke-direct {v6, v1, v2}, Ljava/util/Locale;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 1002
    invoke-virtual {v6}, Ljava/util/Locale;->getLanguage()Ljava/lang/String;

    move-result-object v1

    .line 1004
    invoke-virtual {v0, v1}, Lcom/osp/app/countrylist/CountryInfoItem;->b(Ljava/lang/String;)V

    .line 980
    add-int/lit8 v0, v3, 0x1

    move v3, v0

    goto :goto_1

    .line 995
    :cond_1
    const-string v1, ""

    move-object v8, v2

    move-object v2, v1

    move-object v1, v8

    goto :goto_2

    .line 1008
    :cond_2
    iget-object v0, p0, Lcom/osp/app/signin/hl;->d:Lcom/osp/app/signin/SelectCountryView;

    invoke-virtual {v0, p1}, Lcom/osp/app/signin/SelectCountryView;->a(Ljava/util/ArrayList;)V

    goto :goto_0
.end method

.method static synthetic a(Lcom/osp/app/signin/hl;)[Ljava/lang/String;
    .locals 1

    .prologue
    .line 879
    iget-object v0, p0, Lcom/osp/app/signin/hl;->g:[Ljava/lang/String;

    return-object v0
.end method

.method static synthetic a(Lcom/osp/app/signin/hl;[Ljava/lang/String;)[Ljava/lang/String;
    .locals 0

    .prologue
    .line 879
    iput-object p1, p0, Lcom/osp/app/signin/hl;->g:[Ljava/lang/String;

    return-object p1
.end method


# virtual methods
.method protected final varargs a()Ljava/lang/Boolean;
    .locals 6

    .prologue
    .line 915
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v0, "SCV"

    const-string v1, "CountryListTask :: doInBackground"

    invoke-static {v0, v1}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 917
    const-string v0, "country_calling_code_list"

    iget-object v1, p0, Lcom/osp/app/signin/hl;->d:Lcom/osp/app/signin/SelectCountryView;

    invoke-static {v1}, Lcom/osp/app/signin/SelectCountryView;->a(Lcom/osp/app/signin/SelectCountryView;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 919
    :try_start_0
    iget-object v0, p0, Lcom/osp/app/signin/hl;->d:Lcom/osp/app/signin/SelectCountryView;

    const-string v1, "country_calling_code_list.xml"

    invoke-virtual {v0, v1}, Lcom/osp/app/signin/SelectCountryView;->g(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {}, Lcom/msc/c/h;->a()Lcom/msc/c/h;

    invoke-static {v0}, Lcom/msc/c/h;->d(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v0

    iput-object v0, p0, Lcom/osp/app/signin/hl;->c:Ljava/util/ArrayList;

    iget-object v0, p0, Lcom/osp/app/signin/hl;->c:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/osp/app/countrylist/CountryInfoItem;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0}, Lcom/osp/app/countrylist/CountryInfoItem;->b()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " (+"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v0}, Lcom/osp/app/countrylist/CountryInfoItem;->e()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const/16 v3, 0x29

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Lcom/osp/app/countrylist/CountryInfoItem;->c(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    .line 929
    :goto_1
    const/4 v0, 0x0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    return-object v0

    .line 919
    :cond_0
    :try_start_1
    iget-object v0, p0, Lcom/osp/app/signin/hl;->d:Lcom/osp/app/signin/SelectCountryView;

    iget-object v1, p0, Lcom/osp/app/signin/hl;->c:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Lcom/osp/app/signin/SelectCountryView;->a(Ljava/util/ArrayList;)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_1

    .line 921
    :cond_1
    const-string v0, "language_list"

    iget-object v1, p0, Lcom/osp/app/signin/hl;->d:Lcom/osp/app/signin/SelectCountryView;

    invoke-static {v1}, Lcom/osp/app/signin/SelectCountryView;->a(Lcom/osp/app/signin/SelectCountryView;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 923
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v0, "SCV"

    const-string v1, " requestLanguageList"

    invoke-static {v0, v1}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;)V

    invoke-static {}, Lcom/msc/c/g;->a()Lcom/msc/c/g;

    invoke-static {}, Lcom/msc/c/g;->a()Lcom/msc/c/g;

    iget-object v0, p0, Lcom/osp/app/signin/hl;->d:Lcom/osp/app/signin/SelectCountryView;

    invoke-static {}, Lcom/msc/c/k;->d()Ljava/lang/String;

    move-result-object v1

    invoke-static {}, Lcom/osp/app/util/ah;->a()Lcom/osp/app/util/ah;

    move-result-object v0

    invoke-virtual {v0}, Lcom/osp/app/util/ah;->g()Z

    move-result v4

    invoke-static {}, Lcom/msc/b/i;->a()Lcom/msc/b/i;

    move-result-object v0

    invoke-static {}, Lcom/osp/app/signin/SamsungService;->j()Z

    move-result v2

    invoke-static {}, Lcom/osp/app/signin/SamsungService;->b()Z

    move-result v3

    move-object v5, p0

    invoke-virtual/range {v0 .. v5}, Lcom/msc/b/i;->a(Ljava/lang/String;ZZZLcom/msc/b/h;)Lcom/msc/b/d;

    move-result-object v0

    invoke-virtual {v0}, Lcom/msc/b/d;->a()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/osp/app/signin/hl;->f:J

    iget-wide v0, p0, Lcom/osp/app/signin/hl;->f:J

    invoke-virtual {p0, v0, v1}, Lcom/osp/app/signin/hl;->a(J)V

    iget-wide v0, p0, Lcom/osp/app/signin/hl;->f:J

    sget-object v2, Lcom/msc/b/g;->b:Lcom/msc/b/g;

    invoke-static {v0, v1, v2}, Lcom/msc/c/g;->a(JLcom/msc/b/g;)V

    goto :goto_1

    .line 926
    :cond_2
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v0, "SCV"

    const-string v1, " requestCountryList"

    invoke-static {v0, v1}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;)V

    invoke-static {}, Lcom/msc/c/g;->a()Lcom/msc/c/g;

    invoke-static {}, Lcom/msc/c/g;->a()Lcom/msc/c/g;

    iget-object v0, p0, Lcom/osp/app/signin/hl;->d:Lcom/osp/app/signin/SelectCountryView;

    invoke-static {v0, p0}, Lcom/msc/c/g;->a(Landroid/content/Context;Lcom/msc/b/h;)J

    move-result-wide v0

    iput-wide v0, p0, Lcom/osp/app/signin/hl;->e:J

    iget-wide v0, p0, Lcom/osp/app/signin/hl;->e:J

    invoke-virtual {p0, v0, v1}, Lcom/osp/app/signin/hl;->a(J)V

    iget-wide v0, p0, Lcom/osp/app/signin/hl;->e:J

    const-string v2, "from_json"

    invoke-virtual {p0, v0, v1, v2}, Lcom/osp/app/signin/hl;->a(JLjava/lang/String;)V

    iget-wide v0, p0, Lcom/osp/app/signin/hl;->e:J

    sget-object v2, Lcom/msc/b/g;->b:Lcom/msc/b/g;

    invoke-static {v0, v1, v2}, Lcom/msc/c/g;->a(JLcom/msc/b/g;)V

    goto/16 :goto_1
.end method

.method public final declared-synchronized a(Lcom/msc/b/c;)V
    .locals 6

    .prologue
    .line 1479
    monitor-enter p0

    :try_start_0
    invoke-super {p0, p1}, Lcom/msc/c/b;->a(Lcom/msc/b/c;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1481
    if-nez p1, :cond_1

    .line 1504
    :cond_0
    :goto_0
    monitor-exit p0

    return-void

    .line 1486
    :cond_1
    :try_start_1
    invoke-virtual {p1}, Lcom/msc/b/c;->a()J

    move-result-wide v2

    .line 1487
    invoke-virtual {p1}, Lcom/msc/b/c;->e()Ljava/lang/String;

    move-result-object v1

    .line 1489
    iget-wide v4, p0, Lcom/osp/app/signin/hl;->e:J
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    cmp-long v0, v2, v4

    if-nez v0, :cond_2

    .line 1493
    :try_start_2
    invoke-static {}, Lcom/msc/c/h;->a()Lcom/msc/c/h;

    invoke-static {v1}, Lcom/msc/c/h;->c(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v0

    iput-object v0, p0, Lcom/osp/app/signin/hl;->c:Ljava/util/ArrayList;
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_0
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0

    .line 1495
    :catch_0
    move-exception v0

    .line 1497
    :try_start_3
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    .line 1498
    new-instance v1, Lcom/msc/c/f;

    invoke-direct {v1, v0}, Lcom/msc/c/f;-><init>(Ljava/lang/Exception;)V

    iput-object v1, p0, Lcom/osp/app/signin/hl;->b:Lcom/msc/c/f;
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    goto :goto_0

    .line 1479
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 1500
    :cond_2
    :try_start_4
    iget-wide v4, p0, Lcom/osp/app/signin/hl;->f:J

    cmp-long v0, v2, v4

    if-nez v0, :cond_0

    .line 1502
    invoke-static {}, Lcom/msc/c/h;->a()Lcom/msc/c/h;

    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v0, "SaAuthManager::parseLanguageListForACS"

    invoke-static {v0}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;)V

    if-eqz v1, :cond_3

    invoke-virtual {v1}, Ljava/lang/String;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_5

    :cond_3
    const/4 v0, 0x0

    :cond_4
    invoke-direct {p0, v0}, Lcom/osp/app/signin/hl;->a(Ljava/util/ArrayList;)Ljava/util/ArrayList;

    move-result-object v0

    iput-object v0, p0, Lcom/osp/app/signin/hl;->c:Ljava/util/ArrayList;

    goto :goto_0

    :cond_5
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    :goto_1
    const/16 v2, 0xa

    invoke-virtual {v1, v2}, Ljava/lang/String;->indexOf(I)I

    move-result v2

    if-lez v2, :cond_4

    const/16 v3, 0x2c

    invoke-virtual {v1, v3}, Ljava/lang/String;->indexOf(I)I

    move-result v3

    const/4 v4, 0x0

    invoke-virtual {v1, v4, v3}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v4

    add-int/lit8 v3, v3, 0x1

    invoke-virtual {v1, v3, v2}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v3

    new-instance v5, Lcom/osp/app/countrylist/CountryInfoItem;

    invoke-direct {v5}, Lcom/osp/app/countrylist/CountryInfoItem;-><init>()V

    invoke-virtual {v5, v4}, Lcom/osp/app/countrylist/CountryInfoItem;->c(Ljava/lang/String;)V

    const-string v4, ""

    invoke-virtual {v5, v4}, Lcom/osp/app/countrylist/CountryInfoItem;->e(Ljava/lang/String;)V

    const-string v4, ""

    invoke-virtual {v5, v4}, Lcom/osp/app/countrylist/CountryInfoItem;->b(Ljava/lang/String;)V

    invoke-virtual {v5, v3}, Lcom/osp/app/countrylist/CountryInfoItem;->d(Ljava/lang/String;)V

    invoke-virtual {v0, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    add-int/lit8 v2, v2, 0x1

    invoke-virtual {v1, v2}, Ljava/lang/String;->substring(I)Ljava/lang/String;
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    move-result-object v1

    goto :goto_1
.end method

.method protected final a(Ljava/lang/Boolean;)V
    .locals 12

    .prologue
    const/4 v5, 0x0

    const/4 v11, 0x1

    .line 1039
    invoke-super {p0, p1}, Lcom/msc/c/b;->a(Ljava/lang/Boolean;)V

    .line 1040
    iget-object v0, p0, Lcom/osp/app/signin/hl;->b:Lcom/msc/c/f;

    if-eqz v0, :cond_2

    .line 1042
    invoke-virtual {p0, v5}, Lcom/osp/app/signin/hl;->a(Z)V

    .line 1045
    iget-object v0, p0, Lcom/osp/app/signin/hl;->d:Lcom/osp/app/signin/SelectCountryView;

    const v1, 0x7f09003e

    invoke-virtual {v0, v1}, Lcom/osp/app/signin/SelectCountryView;->getString(I)Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/osp/app/signin/hl;->b:Lcom/msc/c/f;

    invoke-virtual {v1}, Lcom/msc/c/f;->b()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1047
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v0, "SCV"

    const-string v1, "CountryListTask :: network error"

    invoke-static {v0, v1}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 1048
    iget-object v0, p0, Lcom/osp/app/signin/hl;->d:Lcom/osp/app/signin/SelectCountryView;

    const/4 v1, 0x3

    invoke-virtual {v0, v1}, Lcom/osp/app/signin/SelectCountryView;->b(I)V

    .line 1053
    :goto_0
    iget-object v0, p0, Lcom/osp/app/signin/hl;->d:Lcom/osp/app/signin/SelectCountryView;

    invoke-virtual {v0}, Lcom/osp/app/signin/SelectCountryView;->finish()V

    .line 1475
    :cond_0
    :goto_1
    return-void

    .line 1051
    :cond_1
    iget-object v0, p0, Lcom/osp/app/signin/hl;->d:Lcom/osp/app/signin/SelectCountryView;

    invoke-virtual {v0, v11}, Lcom/osp/app/signin/SelectCountryView;->b(I)V

    goto :goto_0

    .line 1057
    :cond_2
    iget-object v0, p0, Lcom/osp/app/signin/hl;->c:Ljava/util/ArrayList;

    if-eqz v0, :cond_12

    iget-object v0, p0, Lcom/osp/app/signin/hl;->c:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-lez v0, :cond_12

    .line 1059
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v0, "SCV"

    const-string v1, "CountryListTask :: CountryInfo size>0"

    invoke-static {v0, v1}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 1060
    iget-object v0, p0, Lcom/osp/app/signin/hl;->c:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    new-array v6, v0, [Ljava/lang/String;

    .line 1061
    iget-object v0, p0, Lcom/osp/app/signin/hl;->d:Lcom/osp/app/signin/SelectCountryView;

    new-instance v1, Ljava/util/Vector;

    invoke-direct {v1}, Ljava/util/Vector;-><init>()V

    invoke-static {v0, v1}, Lcom/osp/app/signin/SelectCountryView;->a(Lcom/osp/app/signin/SelectCountryView;Ljava/util/Vector;)Ljava/util/Vector;

    .line 1063
    const-string v0, "language_list"

    iget-object v1, p0, Lcom/osp/app/signin/hl;->d:Lcom/osp/app/signin/SelectCountryView;

    invoke-static {v1}, Lcom/osp/app/signin/SelectCountryView;->a(Lcom/osp/app/signin/SelectCountryView;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_8

    .line 1067
    invoke-static {}, Lcom/osp/common/util/i;->a()Lcom/osp/common/util/i;

    iget-object v0, p0, Lcom/osp/app/signin/hl;->d:Lcom/osp/app/signin/SelectCountryView;

    invoke-static {v0}, Lcom/osp/common/util/i;->l(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v0

    .line 1073
    :goto_2
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v1, "==============================================="

    invoke-static {v1}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;)V

    .line 1074
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "[SCV]"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;)V

    .line 1075
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v1, "==============================================="

    invoke-static {v1}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;)V

    .line 1078
    iget-object v1, p0, Lcom/osp/app/signin/hl;->d:Lcom/osp/app/signin/SelectCountryView;

    invoke-virtual {v1}, Lcom/osp/app/signin/SelectCountryView;->getIntent()Landroid/content/Intent;

    move-result-object v1

    const-string v2, "key_default_country_name"

    invoke-virtual {v1, v2}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 1079
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "[SCV] ::default country name : "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;)V

    .line 1081
    new-instance v7, Ljava/util/HashMap;

    invoke-direct {v7}, Ljava/util/HashMap;-><init>()V

    .line 1085
    iget-object v1, p0, Lcom/osp/app/signin/hl;->c:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v8

    move v2, v5

    move-object v3, v0

    :goto_3
    invoke-interface {v8}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_e

    invoke-interface {v8}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/osp/app/countrylist/CountryInfoItem;

    .line 1087
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    iget-object v1, p0, Lcom/osp/app/signin/hl;->c:Ljava/util/ArrayList;

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/osp/app/countrylist/CountryInfoItem;

    invoke-virtual {v1}, Lcom/osp/app/countrylist/CountryInfoItem;->a()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;)V

    .line 1088
    if-eqz v3, :cond_3

    .line 1090
    iget-object v1, p0, Lcom/osp/app/signin/hl;->c:Ljava/util/ArrayList;

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/osp/app/countrylist/CountryInfoItem;

    invoke-virtual {v1}, Lcom/osp/app/countrylist/CountryInfoItem;->a()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v3, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_3

    .line 1092
    iget-object v9, p0, Lcom/osp/app/signin/hl;->d:Lcom/osp/app/signin/SelectCountryView;

    iget-object v1, p0, Lcom/osp/app/signin/hl;->c:Ljava/util/ArrayList;

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/osp/app/countrylist/CountryInfoItem;

    invoke-virtual {v1}, Lcom/osp/app/countrylist/CountryInfoItem;->c()Ljava/lang/String;

    move-result-object v1

    invoke-static {v9, v1}, Lcom/osp/app/signin/SelectCountryView;->a(Lcom/osp/app/signin/SelectCountryView;Ljava/lang/String;)Ljava/lang/String;

    .line 1093
    iget-object v1, p0, Lcom/osp/app/signin/hl;->d:Lcom/osp/app/signin/SelectCountryView;

    invoke-static {v1, v2}, Lcom/osp/app/signin/SelectCountryView;->a(Lcom/osp/app/signin/SelectCountryView;I)I

    .line 1095
    const-string v1, "country_calling_code_list"

    iget-object v9, p0, Lcom/osp/app/signin/hl;->d:Lcom/osp/app/signin/SelectCountryView;

    invoke-static {v9}, Lcom/osp/app/signin/SelectCountryView;->a(Lcom/osp/app/signin/SelectCountryView;)Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v1, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_3

    iget-object v1, p0, Lcom/osp/app/signin/hl;->d:Lcom/osp/app/signin/SelectCountryView;

    invoke-static {v1}, Lcom/osp/app/signin/SelectCountryView;->h(Lcom/osp/app/signin/SelectCountryView;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_3

    .line 1097
    iget-object v1, p0, Lcom/osp/app/signin/hl;->d:Lcom/osp/app/signin/SelectCountryView;

    invoke-static {v1, v3}, Lcom/osp/app/signin/SelectCountryView;->a(Lcom/osp/app/signin/SelectCountryView;Ljava/lang/String;)Ljava/lang/String;

    .line 1102
    :cond_3
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v9, "[SCV] ::handleMessage mSelectedLocation : "

    invoke-direct {v1, v9}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v9, p0, Lcom/osp/app/signin/hl;->d:Lcom/osp/app/signin/SelectCountryView;

    invoke-static {v9}, Lcom/osp/app/signin/SelectCountryView;->i(Lcom/osp/app/signin/SelectCountryView;)I

    move-result v9

    invoke-virtual {v1, v9}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;)V

    .line 1104
    const-string v1, "country_calling_code_list"

    iget-object v9, p0, Lcom/osp/app/signin/hl;->d:Lcom/osp/app/signin/SelectCountryView;

    invoke-static {v9}, Lcom/osp/app/signin/SelectCountryView;->a(Lcom/osp/app/signin/SelectCountryView;)Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v1, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_9

    .line 1106
    invoke-virtual {v0}, Lcom/osp/app/countrylist/CountryInfoItem;->b()Ljava/lang/String;

    move-result-object v9

    .line 1107
    add-int/lit8 v1, v2, 0x1

    aput-object v9, v6, v2

    .line 1108
    iget-object v2, p0, Lcom/osp/app/signin/hl;->d:Lcom/osp/app/signin/SelectCountryView;

    invoke-static {v2}, Lcom/osp/app/signin/SelectCountryView;->j(Lcom/osp/app/signin/SelectCountryView;)Ljava/util/Vector;

    move-result-object v2

    invoke-virtual {v2, v9}, Ljava/util/Vector;->add(Ljava/lang/Object;)Z

    .line 1110
    invoke-virtual {v0}, Lcom/osp/app/countrylist/CountryInfoItem;->a()Ljava/lang/String;

    move-result-object v2

    .line 1111
    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v10

    if-eqz v10, :cond_4

    .line 1113
    invoke-virtual {v0}, Lcom/osp/app/countrylist/CountryInfoItem;->f()Ljava/lang/String;

    move-result-object v2

    .line 1115
    :cond_4
    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v10

    if-eqz v10, :cond_5

    .line 1117
    invoke-virtual {v0}, Lcom/osp/app/countrylist/CountryInfoItem;->b()Ljava/lang/String;

    move-result-object v2

    .line 1119
    :cond_5
    invoke-virtual {v7, v2, v9}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1121
    if-eqz v9, :cond_d

    invoke-virtual {v9, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_d

    .line 1123
    iget-object v2, p0, Lcom/osp/app/signin/hl;->d:Lcom/osp/app/signin/SelectCountryView;

    invoke-static {v2, v1}, Lcom/osp/app/signin/SelectCountryView;->a(Lcom/osp/app/signin/SelectCountryView;I)I

    .line 1124
    iget-object v2, p0, Lcom/osp/app/signin/hl;->d:Lcom/osp/app/signin/SelectCountryView;

    invoke-virtual {v0}, Lcom/osp/app/countrylist/CountryInfoItem;->a()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/osp/app/signin/SelectCountryView;->a(Lcom/osp/app/signin/SelectCountryView;Ljava/lang/String;)Ljava/lang/String;

    .line 1126
    iget-object v2, p0, Lcom/osp/app/signin/hl;->d:Lcom/osp/app/signin/SelectCountryView;

    invoke-static {v2}, Lcom/osp/app/signin/SelectCountryView;->h(Lcom/osp/app/signin/SelectCountryView;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_6

    .line 1128
    iget-object v2, p0, Lcom/osp/app/signin/hl;->d:Lcom/osp/app/signin/SelectCountryView;

    invoke-virtual {v0}, Lcom/osp/app/countrylist/CountryInfoItem;->f()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/osp/app/signin/SelectCountryView;->a(Lcom/osp/app/signin/SelectCountryView;Ljava/lang/String;)Ljava/lang/String;

    .line 1130
    :cond_6
    iget-object v2, p0, Lcom/osp/app/signin/hl;->d:Lcom/osp/app/signin/SelectCountryView;

    invoke-static {v2}, Lcom/osp/app/signin/SelectCountryView;->h(Lcom/osp/app/signin/SelectCountryView;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_7

    .line 1132
    iget-object v2, p0, Lcom/osp/app/signin/hl;->d:Lcom/osp/app/signin/SelectCountryView;

    invoke-virtual {v0}, Lcom/osp/app/countrylist/CountryInfoItem;->b()Ljava/lang/String;

    move-result-object v0

    invoke-static {v2, v0}, Lcom/osp/app/signin/SelectCountryView;->a(Lcom/osp/app/signin/SelectCountryView;Ljava/lang/String;)Ljava/lang/String;

    .line 1135
    :cond_7
    const/4 v0, 0x0

    .line 1137
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "[SCV] ::default selected country name index : "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v3, p0, Lcom/osp/app/signin/hl;->d:Lcom/osp/app/signin/SelectCountryView;

    invoke-static {v3}, Lcom/osp/app/signin/SelectCountryView;->i(Lcom/osp/app/signin/SelectCountryView;)I

    move-result v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;)V

    move v2, v1

    move-object v3, v0

    goto/16 :goto_3

    .line 1070
    :cond_8
    iget-object v0, p0, Lcom/osp/app/signin/hl;->d:Lcom/osp/app/signin/SelectCountryView;

    invoke-static {v0}, Lcom/osp/app/signin/SelectCountryView;->g(Lcom/osp/app/signin/SelectCountryView;)Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_2

    .line 1142
    :cond_9
    const-string v1, ""

    invoke-virtual {v0}, Lcom/osp/app/countrylist/CountryInfoItem;->d()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v1, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_a

    .line 1144
    add-int/lit8 v1, v2, 0x1

    invoke-virtual {v0}, Lcom/osp/app/countrylist/CountryInfoItem;->b()Ljava/lang/String;

    move-result-object v9

    aput-object v9, v6, v2

    .line 1145
    iget-object v2, p0, Lcom/osp/app/signin/hl;->d:Lcom/osp/app/signin/SelectCountryView;

    invoke-static {v2}, Lcom/osp/app/signin/SelectCountryView;->j(Lcom/osp/app/signin/SelectCountryView;)Ljava/util/Vector;

    move-result-object v2

    invoke-virtual {v0}, Lcom/osp/app/countrylist/CountryInfoItem;->b()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v2, v9}, Ljava/util/Vector;->add(Ljava/lang/Object;)Z

    .line 1147
    invoke-virtual {v0}, Lcom/osp/app/countrylist/CountryInfoItem;->c()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0}, Lcom/osp/app/countrylist/CountryInfoItem;->b()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v7, v2, v0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move v2, v1

    goto/16 :goto_3

    .line 1150
    :cond_a
    invoke-virtual {v0}, Lcom/osp/app/countrylist/CountryInfoItem;->b()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0}, Lcom/osp/app/countrylist/CountryInfoItem;->d()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v1, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_b

    .line 1152
    add-int/lit8 v1, v2, 0x1

    invoke-virtual {v0}, Lcom/osp/app/countrylist/CountryInfoItem;->b()Ljava/lang/String;

    move-result-object v9

    aput-object v9, v6, v2

    .line 1153
    iget-object v2, p0, Lcom/osp/app/signin/hl;->d:Lcom/osp/app/signin/SelectCountryView;

    invoke-static {v2}, Lcom/osp/app/signin/SelectCountryView;->j(Lcom/osp/app/signin/SelectCountryView;)Ljava/util/Vector;

    move-result-object v2

    invoke-virtual {v0}, Lcom/osp/app/countrylist/CountryInfoItem;->b()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v2, v9}, Ljava/util/Vector;->add(Ljava/lang/Object;)Z

    .line 1155
    invoke-virtual {v0}, Lcom/osp/app/countrylist/CountryInfoItem;->c()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0}, Lcom/osp/app/countrylist/CountryInfoItem;->b()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v7, v2, v0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move v2, v1

    goto/16 :goto_3

    .line 1158
    :cond_b
    iget-object v1, p0, Lcom/osp/app/signin/hl;->d:Lcom/osp/app/signin/SelectCountryView;

    invoke-virtual {v0}, Lcom/osp/app/countrylist/CountryInfoItem;->c()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/osp/app/signin/SelectCountryView;->a(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_c

    .line 1160
    add-int/lit8 v1, v2, 0x1

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0}, Lcom/osp/app/countrylist/CountryInfoItem;->b()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v10, " ("

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v0}, Lcom/osp/app/countrylist/CountryInfoItem;->d()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v10, ")"

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    aput-object v9, v6, v2

    .line 1161
    iget-object v2, p0, Lcom/osp/app/signin/hl;->d:Lcom/osp/app/signin/SelectCountryView;

    invoke-static {v2}, Lcom/osp/app/signin/SelectCountryView;->j(Lcom/osp/app/signin/SelectCountryView;)Ljava/util/Vector;

    move-result-object v2

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0}, Lcom/osp/app/countrylist/CountryInfoItem;->b()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v10, " ("

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v0}, Lcom/osp/app/countrylist/CountryInfoItem;->d()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v10, ")"

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v2, v9}, Ljava/util/Vector;->add(Ljava/lang/Object;)Z

    .line 1162
    invoke-virtual {v0}, Lcom/osp/app/countrylist/CountryInfoItem;->c()Ljava/lang/String;

    move-result-object v2

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0}, Lcom/osp/app/countrylist/CountryInfoItem;->b()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v10, " ("

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v0}, Lcom/osp/app/countrylist/CountryInfoItem;->d()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v9, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v9, ")"

    invoke-virtual {v0, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v7, v2, v0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move v2, v1

    goto/16 :goto_3

    .line 1165
    :cond_c
    add-int/lit8 v1, v2, 0x1

    invoke-virtual {v0}, Lcom/osp/app/countrylist/CountryInfoItem;->b()Ljava/lang/String;

    move-result-object v9

    aput-object v9, v6, v2

    .line 1166
    iget-object v2, p0, Lcom/osp/app/signin/hl;->d:Lcom/osp/app/signin/SelectCountryView;

    invoke-static {v2}, Lcom/osp/app/signin/SelectCountryView;->j(Lcom/osp/app/signin/SelectCountryView;)Ljava/util/Vector;

    move-result-object v2

    invoke-virtual {v0}, Lcom/osp/app/countrylist/CountryInfoItem;->b()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v2, v9}, Ljava/util/Vector;->add(Ljava/lang/Object;)Z

    .line 1167
    invoke-virtual {v0}, Lcom/osp/app/countrylist/CountryInfoItem;->c()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0}, Lcom/osp/app/countrylist/CountryInfoItem;->b()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v7, v2, v0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    :cond_d
    move v0, v1

    move v2, v0

    .line 1173
    goto/16 :goto_3

    .line 1177
    :cond_e
    iget-object v0, p0, Lcom/osp/app/signin/hl;->d:Lcom/osp/app/signin/SelectCountryView;

    invoke-virtual {v0}, Lcom/osp/app/signin/SelectCountryView;->isFinishing()Z

    move-result v0

    if-nez v0, :cond_0

    .line 1183
    array-length v0, v6

    if-eqz v0, :cond_0

    .line 1186
    iget-object v0, p0, Lcom/osp/app/signin/hl;->d:Lcom/osp/app/signin/SelectCountryView;

    invoke-static {v0}, Lcom/osp/app/signin/SelectCountryView;->k(Lcom/osp/app/signin/SelectCountryView;)Landroid/widget/EditText;

    move-result-object v0

    invoke-virtual {v0, v11}, Landroid/widget/EditText;->setFocusable(Z)V

    .line 1187
    iget-object v0, p0, Lcom/osp/app/signin/hl;->d:Lcom/osp/app/signin/SelectCountryView;

    invoke-static {v0}, Lcom/osp/app/signin/SelectCountryView;->k(Lcom/osp/app/signin/SelectCountryView;)Landroid/widget/EditText;

    move-result-object v0

    invoke-virtual {v0, v11}, Landroid/widget/EditText;->setFocusableInTouchMode(Z)V

    .line 1188
    iget-object v0, p0, Lcom/osp/app/signin/hl;->d:Lcom/osp/app/signin/SelectCountryView;

    invoke-static {v0}, Lcom/osp/app/signin/SelectCountryView;->k(Lcom/osp/app/signin/SelectCountryView;)Landroid/widget/EditText;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/EditText;->requestFocus()Z

    .line 1191
    iget-object v0, p0, Lcom/osp/app/signin/hl;->d:Lcom/osp/app/signin/SelectCountryView;

    invoke-static {v0, v6}, Lcom/osp/app/signin/SelectCountryView;->a(Lcom/osp/app/signin/SelectCountryView;[Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v2

    .line 1192
    iget-object v0, p0, Lcom/osp/app/signin/hl;->d:Lcom/osp/app/signin/SelectCountryView;

    array-length v1, v2

    invoke-static {v0, v1}, Lcom/osp/app/signin/SelectCountryView;->b(Lcom/osp/app/signin/SelectCountryView;I)I

    .line 1194
    iget-object v0, p0, Lcom/osp/app/signin/hl;->d:Lcom/osp/app/signin/SelectCountryView;

    invoke-static {v0}, Lcom/osp/app/signin/SelectCountryView;->h(Lcom/osp/app/signin/SelectCountryView;)Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_f

    iget-object v0, p0, Lcom/osp/app/signin/hl;->d:Lcom/osp/app/signin/SelectCountryView;

    invoke-static {v0}, Lcom/osp/app/signin/SelectCountryView;->h(Lcom/osp/app/signin/SelectCountryView;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v7, v0}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-ne v0, v11, :cond_f

    move v1, v5

    .line 1196
    :goto_4
    iget-object v0, p0, Lcom/osp/app/signin/hl;->d:Lcom/osp/app/signin/SelectCountryView;

    invoke-static {v0}, Lcom/osp/app/signin/SelectCountryView;->l(Lcom/osp/app/signin/SelectCountryView;)I

    move-result v0

    if-ge v1, v0, :cond_f

    .line 1198
    aget-object v3, v2, v1

    iget-object v0, p0, Lcom/osp/app/signin/hl;->d:Lcom/osp/app/signin/SelectCountryView;

    invoke-static {v0}, Lcom/osp/app/signin/SelectCountryView;->h(Lcom/osp/app/signin/SelectCountryView;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v7, v0}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-virtual {v3, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_10

    .line 1200
    iget-object v0, p0, Lcom/osp/app/signin/hl;->d:Lcom/osp/app/signin/SelectCountryView;

    invoke-static {v0, v1}, Lcom/osp/app/signin/SelectCountryView;->a(Lcom/osp/app/signin/SelectCountryView;I)I

    .line 1207
    :cond_f
    iget-object v0, p0, Lcom/osp/app/signin/hl;->d:Lcom/osp/app/signin/SelectCountryView;

    invoke-static {v0}, Lcom/osp/app/signin/SelectCountryView;->h(Lcom/osp/app/signin/SelectCountryView;)Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_11

    iget-object v0, p0, Lcom/osp/app/signin/hl;->d:Lcom/osp/app/signin/SelectCountryView;

    invoke-static {v0}, Lcom/osp/app/signin/SelectCountryView;->h(Lcom/osp/app/signin/SelectCountryView;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v7, v0}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-ne v0, v11, :cond_11

    .line 1209
    iget-object v8, p0, Lcom/osp/app/signin/hl;->d:Lcom/osp/app/signin/SelectCountryView;

    new-instance v0, Lcom/osp/app/signin/ej;

    iget-object v1, p0, Lcom/osp/app/signin/hl;->d:Lcom/osp/app/signin/SelectCountryView;

    iget-object v3, p0, Lcom/osp/app/signin/hl;->d:Lcom/osp/app/signin/SelectCountryView;

    invoke-static {v3}, Lcom/osp/app/signin/SelectCountryView;->h(Lcom/osp/app/signin/SelectCountryView;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v7, v3}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    iget-object v4, p0, Lcom/osp/app/signin/hl;->d:Lcom/osp/app/signin/SelectCountryView;

    invoke-static {v4}, Lcom/osp/app/signin/SelectCountryView;->m(Lcom/osp/app/signin/SelectCountryView;)Z

    move-result v4

    invoke-direct/range {v0 .. v5}, Lcom/osp/app/signin/ej;-><init>(Landroid/app/Activity;[Ljava/lang/String;Ljava/lang/String;ZB)V

    invoke-static {v8, v0}, Lcom/osp/app/signin/SelectCountryView;->a(Lcom/osp/app/signin/SelectCountryView;Lcom/osp/app/signin/ej;)Lcom/osp/app/signin/ej;

    .line 1216
    :goto_5
    iget-object v0, p0, Lcom/osp/app/signin/hl;->d:Lcom/osp/app/signin/SelectCountryView;

    invoke-static {v0}, Lcom/osp/app/signin/SelectCountryView;->p(Lcom/osp/app/signin/SelectCountryView;)Landroid/widget/ListView;

    move-result-object v0

    iget-object v1, p0, Lcom/osp/app/signin/hl;->d:Lcom/osp/app/signin/SelectCountryView;

    invoke-static {v1}, Lcom/osp/app/signin/SelectCountryView;->o(Lcom/osp/app/signin/SelectCountryView;)Lcom/osp/app/signin/ej;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 1218
    iget-object v0, p0, Lcom/osp/app/signin/hl;->d:Lcom/osp/app/signin/SelectCountryView;

    invoke-static {v0}, Lcom/osp/app/signin/SelectCountryView;->p(Lcom/osp/app/signin/SelectCountryView;)Landroid/widget/ListView;

    move-result-object v0

    iget-object v1, p0, Lcom/osp/app/signin/hl;->d:Lcom/osp/app/signin/SelectCountryView;

    invoke-static {v1}, Lcom/osp/app/signin/SelectCountryView;->i(Lcom/osp/app/signin/SelectCountryView;)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setSelection(I)V

    .line 1219
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "[SCV] ::handleMessage mSelectedLocation : "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v1, p0, Lcom/osp/app/signin/hl;->d:Lcom/osp/app/signin/SelectCountryView;

    invoke-static {v1}, Lcom/osp/app/signin/SelectCountryView;->i(Lcom/osp/app/signin/SelectCountryView;)I

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;)V

    .line 1226
    iget-object v0, p0, Lcom/osp/app/signin/hl;->d:Lcom/osp/app/signin/SelectCountryView;

    invoke-static {v0}, Lcom/osp/app/signin/SelectCountryView;->p(Lcom/osp/app/signin/SelectCountryView;)Landroid/widget/ListView;

    move-result-object v0

    new-instance v1, Lcom/osp/app/signin/hm;

    invoke-direct {v1, p0}, Lcom/osp/app/signin/hm;-><init>(Lcom/osp/app/signin/hl;)V

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 1243
    iget-object v0, p0, Lcom/osp/app/signin/hl;->d:Lcom/osp/app/signin/SelectCountryView;

    invoke-static {v0}, Lcom/osp/app/signin/SelectCountryView;->p(Lcom/osp/app/signin/SelectCountryView;)Landroid/widget/ListView;

    move-result-object v0

    new-instance v1, Lcom/osp/app/signin/hn;

    invoke-direct {v1, p0}, Lcom/osp/app/signin/hn;-><init>(Lcom/osp/app/signin/hl;)V

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setOnFocusChangeListener(Landroid/view/View$OnFocusChangeListener;)V

    .line 1262
    iput-object v2, p0, Lcom/osp/app/signin/hl;->g:[Ljava/lang/String;

    .line 1263
    iget-object v0, p0, Lcom/osp/app/signin/hl;->d:Lcom/osp/app/signin/SelectCountryView;

    invoke-static {v0}, Lcom/osp/app/signin/SelectCountryView;->p(Lcom/osp/app/signin/SelectCountryView;)Landroid/widget/ListView;

    move-result-object v0

    new-instance v1, Lcom/osp/app/signin/ho;

    invoke-direct {v1, p0}, Lcom/osp/app/signin/ho;-><init>(Lcom/osp/app/signin/hl;)V

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V

    .line 1367
    iget-object v0, p0, Lcom/osp/app/signin/hl;->d:Lcom/osp/app/signin/SelectCountryView;

    invoke-static {v0}, Lcom/osp/app/signin/SelectCountryView;->v(Lcom/osp/app/signin/SelectCountryView;)Landroid/widget/LinearLayout;

    move-result-object v0

    iget-object v1, p0, Lcom/osp/app/signin/hl;->d:Lcom/osp/app/signin/SelectCountryView;

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1368
    iget-object v0, p0, Lcom/osp/app/signin/hl;->d:Lcom/osp/app/signin/SelectCountryView;

    iget-object v1, p0, Lcom/osp/app/signin/hl;->d:Lcom/osp/app/signin/SelectCountryView;

    invoke-static {v1}, Lcom/osp/app/signin/SelectCountryView;->v(Lcom/osp/app/signin/SelectCountryView;)Landroid/widget/LinearLayout;

    move-result-object v1

    invoke-virtual {v1}, Landroid/widget/LinearLayout;->getHeight()I

    move-result v1

    invoke-static {v0, v1}, Lcom/osp/app/signin/SelectCountryView;->c(Lcom/osp/app/signin/SelectCountryView;I)I

    .line 1369
    iget-object v0, p0, Lcom/osp/app/signin/hl;->d:Lcom/osp/app/signin/SelectCountryView;

    invoke-virtual {v0, v11}, Lcom/osp/app/signin/SelectCountryView;->onWindowFocusChanged(Z)V

    .line 1374
    iget-object v0, p0, Lcom/osp/app/signin/hl;->d:Lcom/osp/app/signin/SelectCountryView;

    invoke-static {v0}, Lcom/osp/app/signin/SelectCountryView;->k(Lcom/osp/app/signin/SelectCountryView;)Landroid/widget/EditText;

    move-result-object v0

    new-instance v1, Lcom/osp/app/signin/hp;

    invoke-direct {v1, p0, v6}, Lcom/osp/app/signin/hp;-><init>(Lcom/osp/app/signin/hl;[Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->addTextChangedListener(Landroid/text/TextWatcher;)V

    goto/16 :goto_1

    .line 1196
    :cond_10
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto/16 :goto_4

    .line 1212
    :cond_11
    iget-object v0, p0, Lcom/osp/app/signin/hl;->d:Lcom/osp/app/signin/SelectCountryView;

    new-instance v1, Lcom/osp/app/signin/ej;

    iget-object v3, p0, Lcom/osp/app/signin/hl;->d:Lcom/osp/app/signin/SelectCountryView;

    iget-object v4, p0, Lcom/osp/app/signin/hl;->d:Lcom/osp/app/signin/SelectCountryView;

    invoke-static {v4}, Lcom/osp/app/signin/SelectCountryView;->n(Lcom/osp/app/signin/SelectCountryView;)Z

    move-result v4

    invoke-direct {v1, v3, v2, v4}, Lcom/osp/app/signin/ej;-><init>(Landroid/app/Activity;[Ljava/lang/String;Z)V

    invoke-static {v0, v1}, Lcom/osp/app/signin/SelectCountryView;->a(Lcom/osp/app/signin/SelectCountryView;Lcom/osp/app/signin/ej;)Lcom/osp/app/signin/ej;

    goto/16 :goto_5

    .line 1468
    :cond_12
    invoke-virtual {p0, v5}, Lcom/osp/app/signin/hl;->a(Z)V

    .line 1471
    iget-object v0, p0, Lcom/osp/app/signin/hl;->d:Lcom/osp/app/signin/SelectCountryView;

    invoke-virtual {v0, v11}, Lcom/osp/app/signin/SelectCountryView;->b(I)V

    .line 1472
    iget-object v0, p0, Lcom/osp/app/signin/hl;->d:Lcom/osp/app/signin/SelectCountryView;

    invoke-virtual {v0}, Lcom/osp/app/signin/SelectCountryView;->finish()V

    goto/16 :goto_1
.end method

.method protected final d()V
    .locals 4

    .prologue
    const/4 v2, 0x0

    .line 1015
    invoke-super {p0}, Lcom/msc/c/b;->d()V

    .line 1016
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v0, "SCV"

    const-string v1, "CountryListTask :: cancelTask"

    invoke-static {v0, v1}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 1017
    const-string v0, "BG_mode"

    iget-object v1, p0, Lcom/osp/app/signin/hl;->d:Lcom/osp/app/signin/SelectCountryView;

    invoke-static {v1}, Lcom/osp/app/signin/SelectCountryView;->b(Lcom/osp/app/signin/SelectCountryView;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    const-string v0, "BG_mode"

    iget-object v1, p0, Lcom/osp/app/signin/hl;->d:Lcom/osp/app/signin/SelectCountryView;

    invoke-static {v1}, Lcom/osp/app/signin/SelectCountryView;->c(Lcom/osp/app/signin/SelectCountryView;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 1019
    :cond_0
    new-instance v0, Landroid/content/Intent;

    const-string v1, "com.msc.action.samsungaccount.response.BackGroundSignin"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 1020
    const-string v1, "bg_result"

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 1021
    iget-object v1, p0, Lcom/osp/app/signin/hl;->d:Lcom/osp/app/signin/SelectCountryView;

    invoke-static {v1}, Lcom/osp/app/signin/SelectCountryView;->d(Lcom/osp/app/signin/SelectCountryView;)Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_1

    .line 1023
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v2, p0, Lcom/osp/app/signin/hl;->d:Lcom/osp/app/signin/SelectCountryView;

    invoke-static {v2}, Lcom/osp/app/signin/SelectCountryView;->d(Lcom/osp/app/signin/SelectCountryView;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ":"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setData(Landroid/net/Uri;)Landroid/content/Intent;

    .line 1024
    iget-object v1, p0, Lcom/osp/app/signin/hl;->d:Lcom/osp/app/signin/SelectCountryView;

    invoke-static {v1}, Lcom/osp/app/signin/SelectCountryView;->d(Lcom/osp/app/signin/SelectCountryView;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setPackage(Ljava/lang/String;)Landroid/content/Intent;

    .line 1027
    const-string v1, "client_id"

    iget-object v2, p0, Lcom/osp/app/signin/hl;->d:Lcom/osp/app/signin/SelectCountryView;

    invoke-static {v2}, Lcom/osp/app/signin/SelectCountryView;->e(Lcom/osp/app/signin/SelectCountryView;)Landroid/content/Intent;

    move-result-object v2

    const-string v3, "client_id"

    invoke-virtual {v2, v3}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 1028
    iget-object v1, p0, Lcom/osp/app/signin/hl;->d:Lcom/osp/app/signin/SelectCountryView;

    iget-object v2, p0, Lcom/osp/app/signin/hl;->d:Lcom/osp/app/signin/SelectCountryView;

    invoke-static {v2}, Lcom/osp/app/signin/SelectCountryView;->f(Lcom/osp/app/signin/SelectCountryView;)J

    move-result-wide v2

    invoke-virtual {v1, v0, v2, v3}, Lcom/osp/app/signin/SelectCountryView;->a(Landroid/content/Intent;J)V

    .line 1034
    :cond_1
    :goto_0
    iget-object v0, p0, Lcom/osp/app/signin/hl;->d:Lcom/osp/app/signin/SelectCountryView;

    invoke-virtual {v0}, Lcom/osp/app/signin/SelectCountryView;->finish()V

    .line 1035
    return-void

    .line 1032
    :cond_2
    iget-object v0, p0, Lcom/osp/app/signin/hl;->d:Lcom/osp/app/signin/SelectCountryView;

    invoke-virtual {v0, v2}, Lcom/osp/app/signin/SelectCountryView;->b(I)V

    goto :goto_0
.end method

.method protected final synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 879
    invoke-virtual {p0}, Lcom/osp/app/signin/hl;->a()Ljava/lang/Boolean;

    move-result-object v0

    return-object v0
.end method

.method protected final synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 879
    check-cast p1, Ljava/lang/Boolean;

    invoke-virtual {p0, p1}, Lcom/osp/app/signin/hl;->a(Ljava/lang/Boolean;)V

    return-void
.end method
