.class final Lcom/osp/app/signin/po;
.super Lcom/msc/c/b;
.source "UserValidateCheck.java"


# instance fields
.field final synthetic c:Lcom/osp/app/signin/UserValidateCheck;

.field private d:Ljava/lang/String;

.field private e:Z

.field private f:J

.field private g:J

.field private h:J

.field private i:J

.field private j:J

.field private k:J

.field private l:J

.field private m:J

.field private n:J

.field private o:I

.field private p:Ljava/lang/String;

.field private q:Z


# direct methods
.method public constructor <init>(Lcom/osp/app/signin/UserValidateCheck;)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 2168
    iput-object p1, p0, Lcom/osp/app/signin/po;->c:Lcom/osp/app/signin/UserValidateCheck;

    .line 2169
    invoke-direct {p0, p1}, Lcom/msc/c/b;-><init>(Landroid/content/Context;)V

    .line 2105
    const-string v0, "Fail"

    iput-object v0, p0, Lcom/osp/app/signin/po;->d:Ljava/lang/String;

    .line 2110
    iput-boolean v1, p0, Lcom/osp/app/signin/po;->e:Z

    .line 2160
    iput v1, p0, Lcom/osp/app/signin/po;->o:I

    .line 2166
    iput-boolean v1, p0, Lcom/osp/app/signin/po;->q:Z

    .line 2173
    return-void
.end method

.method public constructor <init>(Lcom/osp/app/signin/UserValidateCheck;B)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 2175
    iput-object p1, p0, Lcom/osp/app/signin/po;->c:Lcom/osp/app/signin/UserValidateCheck;

    .line 2176
    invoke-direct {p0, p1}, Lcom/msc/c/b;-><init>(Landroid/content/Context;)V

    .line 2105
    const-string v0, "Fail"

    iput-object v0, p0, Lcom/osp/app/signin/po;->d:Ljava/lang/String;

    .line 2110
    iput-boolean v1, p0, Lcom/osp/app/signin/po;->e:Z

    .line 2160
    iput v1, p0, Lcom/osp/app/signin/po;->o:I

    .line 2166
    iput-boolean v1, p0, Lcom/osp/app/signin/po;->q:Z

    .line 2177
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/osp/app/signin/po;->q:Z

    .line 2180
    return-void
.end method

.method private a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 3

    .prologue
    .line 3045
    invoke-static {}, Lcom/msc/c/g;->a()Lcom/msc/c/g;

    .line 3046
    iget-object v0, p0, Lcom/osp/app/signin/po;->c:Lcom/osp/app/signin/UserValidateCheck;

    invoke-static {v0, p1, p2, p3, p0}, Lcom/msc/c/g;->f(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/msc/b/h;)J

    move-result-wide v0

    iput-wide v0, p0, Lcom/osp/app/signin/po;->j:J

    .line 3047
    iget-wide v0, p0, Lcom/osp/app/signin/po;->j:J

    invoke-virtual {p0, v0, v1}, Lcom/osp/app/signin/po;->a(J)V

    .line 3048
    iget-wide v0, p0, Lcom/osp/app/signin/po;->j:J

    const-string v2, "from_json"

    invoke-virtual {p0, v0, v1, v2}, Lcom/osp/app/signin/po;->a(JLjava/lang/String;)V

    .line 3050
    iget-wide v0, p0, Lcom/osp/app/signin/po;->j:J

    sget-object v2, Lcom/msc/b/g;->c:Lcom/msc/b/g;

    invoke-static {v0, v1, v2}, Lcom/msc/c/g;->a(JLcom/msc/b/g;)V

    .line 3051
    return-void
.end method

.method private b(Ljava/lang/String;)V
    .locals 3

    .prologue
    .line 3035
    invoke-static {}, Lcom/msc/c/g;->a()Lcom/msc/c/g;

    .line 3036
    iget-object v0, p0, Lcom/osp/app/signin/po;->c:Lcom/osp/app/signin/UserValidateCheck;

    const-string v1, "001"

    invoke-static {v0, p1, v1, p0}, Lcom/msc/c/g;->k(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Lcom/msc/b/h;)J

    move-result-wide v0

    iput-wide v0, p0, Lcom/osp/app/signin/po;->k:J

    .line 3037
    iget-wide v0, p0, Lcom/osp/app/signin/po;->k:J

    invoke-virtual {p0, v0, v1}, Lcom/osp/app/signin/po;->a(J)V

    .line 3038
    iget-wide v0, p0, Lcom/osp/app/signin/po;->k:J

    const-string v2, "from_xml"

    invoke-virtual {p0, v0, v1, v2}, Lcom/osp/app/signin/po;->a(JLjava/lang/String;)V

    .line 3040
    iget-wide v0, p0, Lcom/osp/app/signin/po;->k:J

    sget-object v2, Lcom/msc/b/g;->b:Lcom/msc/b/g;

    invoke-static {v0, v1, v2}, Lcom/msc/c/g;->a(JLcom/msc/b/g;)V

    .line 3041
    return-void
.end method

.method private c(Ljava/lang/String;Ljava/lang/String;)V
    .locals 3

    .prologue
    .line 3020
    invoke-static {}, Lcom/msc/c/g;->a()Lcom/msc/c/g;

    .line 3021
    iget-object v0, p0, Lcom/osp/app/signin/po;->c:Lcom/osp/app/signin/UserValidateCheck;

    invoke-static {v0, p1, p2, p0}, Lcom/msc/c/g;->f(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Lcom/msc/b/h;)J

    move-result-wide v0

    iput-wide v0, p0, Lcom/osp/app/signin/po;->i:J

    .line 3022
    iget-wide v0, p0, Lcom/osp/app/signin/po;->i:J

    invoke-virtual {p0, v0, v1}, Lcom/osp/app/signin/po;->a(J)V

    .line 3023
    iget-wide v0, p0, Lcom/osp/app/signin/po;->i:J

    const-string v2, "from_json"

    invoke-virtual {p0, v0, v1, v2}, Lcom/osp/app/signin/po;->a(JLjava/lang/String;)V

    .line 3025
    iget-wide v0, p0, Lcom/osp/app/signin/po;->i:J

    sget-object v2, Lcom/msc/b/g;->c:Lcom/msc/b/g;

    invoke-static {v0, v1, v2}, Lcom/msc/c/g;->a(JLcom/msc/b/g;)V

    .line 3026
    return-void
.end method

.method private d(Ljava/lang/String;Ljava/lang/String;)V
    .locals 3

    .prologue
    .line 3097
    invoke-static {}, Lcom/msc/c/g;->a()Lcom/msc/c/g;

    .line 3099
    iget-object v0, p0, Lcom/osp/app/signin/po;->c:Lcom/osp/app/signin/UserValidateCheck;

    invoke-static {v0, p1, p2, p0}, Lcom/msc/c/g;->h(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Lcom/msc/b/h;)J

    move-result-wide v0

    iput-wide v0, p0, Lcom/osp/app/signin/po;->n:J

    .line 3100
    iget-wide v0, p0, Lcom/osp/app/signin/po;->n:J

    invoke-virtual {p0, v0, v1}, Lcom/osp/app/signin/po;->a(J)V

    .line 3101
    iget-wide v0, p0, Lcom/osp/app/signin/po;->n:J

    const-string v2, "from_xml"

    invoke-virtual {p0, v0, v1, v2}, Lcom/osp/app/signin/po;->a(JLjava/lang/String;)V

    .line 3102
    iget-wide v0, p0, Lcom/osp/app/signin/po;->n:J

    sget-object v2, Lcom/msc/b/g;->c:Lcom/msc/b/g;

    invoke-static {v0, v1, v2}, Lcom/msc/c/g;->a(JLcom/msc/b/g;)V

    .line 3103
    return-void
.end method

.method private h()V
    .locals 4

    .prologue
    .line 3086
    invoke-static {}, Lcom/msc/c/g;->a()Lcom/msc/c/g;

    .line 3087
    iget-object v0, p0, Lcom/osp/app/signin/po;->c:Lcom/osp/app/signin/UserValidateCheck;

    iget-object v1, p0, Lcom/osp/app/signin/po;->c:Lcom/osp/app/signin/UserValidateCheck;

    invoke-static {v1}, Lcom/osp/app/util/ad;->h(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/osp/app/signin/po;->c:Lcom/osp/app/signin/UserValidateCheck;

    invoke-static {v2}, Lcom/osp/app/signin/UserValidateCheck;->e(Lcom/osp/app/signin/UserValidateCheck;)Landroid/widget/EditText;

    move-result-object v2

    invoke-virtual {v2}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x0

    invoke-static {v0, v1, v2, v3, p0}, Lcom/msc/c/g;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Lcom/osp/app/signin/cn;Lcom/msc/b/h;)J

    move-result-wide v0

    iput-wide v0, p0, Lcom/osp/app/signin/po;->l:J

    .line 3089
    iget-wide v0, p0, Lcom/osp/app/signin/po;->l:J

    invoke-virtual {p0, v0, v1}, Lcom/osp/app/signin/po;->a(J)V

    .line 3090
    iget-wide v0, p0, Lcom/osp/app/signin/po;->l:J

    const-string v2, "from_json"

    invoke-virtual {p0, v0, v1, v2}, Lcom/osp/app/signin/po;->a(JLjava/lang/String;)V

    .line 3092
    iget-wide v0, p0, Lcom/osp/app/signin/po;->l:J

    sget-object v2, Lcom/msc/b/g;->c:Lcom/msc/b/g;

    invoke-static {v0, v1, v2}, Lcom/msc/c/g;->a(JLcom/msc/b/g;)V

    .line 3093
    return-void
.end method


# virtual methods
.method protected final varargs a()Ljava/lang/Boolean;
    .locals 3

    .prologue
    .line 2200
    const-string v0, "ACCOUNTINFO_MODIFY"

    iget-object v1, p0, Lcom/osp/app/signin/po;->c:Lcom/osp/app/signin/UserValidateCheck;

    invoke-static {v1}, Lcom/osp/app/signin/UserValidateCheck;->n(Lcom/osp/app/signin/UserValidateCheck;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/osp/app/signin/po;->c:Lcom/osp/app/signin/UserValidateCheck;

    invoke-static {v0}, Lcom/osp/app/signin/UserValidateCheck;->o(Lcom/osp/app/signin/UserValidateCheck;)I

    move-result v0

    const/4 v1, 0x1

    if-ne v0, v1, :cond_4

    .line 2202
    :cond_0
    invoke-static {}, Lcom/osp/device/b;->b()Lcom/osp/device/b;

    move-result-object v0

    invoke-virtual {v0}, Lcom/osp/device/b;->a()Z

    move-result v0

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/osp/app/signin/po;->c:Lcom/osp/app/signin/UserValidateCheck;

    invoke-static {v0}, Lcom/osp/app/util/ad;->h(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/telephony/PhoneNumberUtils;->isGlobalPhoneNumber(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-static {}, Lcom/osp/app/util/p;->a()Lcom/osp/app/util/p;

    move-result-object v0

    iget-object v1, p0, Lcom/osp/app/signin/po;->c:Lcom/osp/app/signin/UserValidateCheck;

    invoke-static {v1}, Lcom/osp/app/signin/UserValidateCheck;->r(Lcom/osp/app/signin/UserValidateCheck;)Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/osp/app/util/p;->c(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_2

    iget-boolean v0, p0, Lcom/osp/app/signin/po;->q:Z

    if-eqz v0, :cond_2

    .line 2206
    iget-object v0, p0, Lcom/osp/app/signin/po;->c:Lcom/osp/app/signin/UserValidateCheck;

    invoke-static {v0}, Lcom/msc/c/e;->h(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/osp/app/signin/po;->b(Ljava/lang/String;)V

    .line 2304
    :cond_1
    :goto_0
    const/4 v0, 0x0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    return-object v0

    .line 2210
    :cond_2
    iget-object v0, p0, Lcom/osp/app/signin/po;->c:Lcom/osp/app/signin/UserValidateCheck;

    invoke-static {v0}, Lcom/osp/app/util/ad;->p(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/osp/app/signin/po;->p:Ljava/lang/String;

    .line 2212
    new-instance v0, Lcom/msc/a/f;

    invoke-direct {v0}, Lcom/msc/a/f;-><init>()V

    .line 2213
    invoke-virtual {v0}, Lcom/msc/a/f;->e()V

    .line 2214
    iget-object v1, p0, Lcom/osp/app/signin/po;->c:Lcom/osp/app/signin/UserValidateCheck;

    invoke-static {v1}, Lcom/osp/app/util/ad;->h(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/msc/a/f;->d(Ljava/lang/String;)V

    .line 2215
    iget-object v1, p0, Lcom/osp/app/signin/po;->p:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/msc/a/f;->h(Ljava/lang/String;)V

    .line 2217
    iget-object v1, p0, Lcom/osp/app/signin/po;->c:Lcom/osp/app/signin/UserValidateCheck;

    invoke-static {v1}, Lcom/osp/app/signin/UserValidateCheck;->p(Lcom/osp/app/signin/UserValidateCheck;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/msc/a/f;->f(Ljava/lang/String;)V

    .line 2218
    iget-object v1, p0, Lcom/osp/app/signin/po;->c:Lcom/osp/app/signin/UserValidateCheck;

    invoke-static {v1}, Lcom/osp/app/signin/UserValidateCheck;->q(Lcom/osp/app/signin/UserValidateCheck;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/msc/a/f;->g(Ljava/lang/String;)V

    .line 2219
    iget-object v1, p0, Lcom/osp/app/signin/po;->c:Lcom/osp/app/signin/UserValidateCheck;

    invoke-static {v1}, Lcom/osp/app/signin/UserValidateCheck;->e(Lcom/osp/app/signin/UserValidateCheck;)Landroid/widget/EditText;

    move-result-object v1

    invoke-virtual {v1}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/msc/a/f;->e(Ljava/lang/String;)V

    .line 2220
    invoke-static {}, Lcom/osp/device/b;->b()Lcom/osp/device/b;

    move-result-object v1

    invoke-virtual {v1}, Lcom/osp/device/b;->a()Z

    move-result v1

    if-eqz v1, :cond_3

    iget-object v1, p0, Lcom/osp/app/signin/po;->c:Lcom/osp/app/signin/UserValidateCheck;

    invoke-static {v1}, Lcom/osp/app/util/ad;->h(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Landroid/telephony/PhoneNumberUtils;->isGlobalPhoneNumber(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_3

    .line 2223
    iget-object v1, p0, Lcom/osp/app/signin/po;->c:Lcom/osp/app/signin/UserValidateCheck;

    invoke-static {v1}, Lcom/msc/c/e;->h(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v1

    .line 2224
    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_3

    .line 2226
    const-string v1, "Y"

    invoke-virtual {v0, v1}, Lcom/msc/a/f;->b(Ljava/lang/String;)V

    .line 2236
    :cond_3
    iget-object v1, p0, Lcom/osp/app/signin/po;->c:Lcom/osp/app/signin/UserValidateCheck;

    invoke-static {v1}, Lcom/msc/c/e;->h(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v1

    .line 2237
    invoke-virtual {v0, v1}, Lcom/msc/a/f;->c(Ljava/lang/String;)V

    .line 2238
    const-string v1, "Y"

    invoke-virtual {v0, v1}, Lcom/msc/a/f;->a(Ljava/lang/String;)V

    .line 2241
    invoke-static {}, Lcom/msc/c/g;->a()Lcom/msc/c/g;

    iget-object v1, p0, Lcom/osp/app/signin/po;->c:Lcom/osp/app/signin/UserValidateCheck;

    invoke-static {v1, v0, p0}, Lcom/msc/c/g;->a(Landroid/content/Context;Lcom/msc/a/f;Lcom/msc/b/h;)J

    move-result-wide v0

    iput-wide v0, p0, Lcom/osp/app/signin/po;->f:J

    iget-wide v0, p0, Lcom/osp/app/signin/po;->f:J

    invoke-virtual {p0, v0, v1}, Lcom/osp/app/signin/po;->a(J)V

    iget-wide v0, p0, Lcom/osp/app/signin/po;->f:J

    const-string v2, "from_xml"

    invoke-virtual {p0, v0, v1, v2}, Lcom/osp/app/signin/po;->a(JLjava/lang/String;)V

    iget-wide v0, p0, Lcom/osp/app/signin/po;->f:J

    sget-object v2, Lcom/msc/b/g;->c:Lcom/msc/b/g;

    invoke-static {v0, v1, v2}, Lcom/msc/c/g;->a(JLcom/msc/b/g;)V

    goto/16 :goto_0

    .line 2245
    :cond_4
    const-string v0, "netflix"

    iget-object v1, p0, Lcom/osp/app/signin/po;->c:Lcom/osp/app/signin/UserValidateCheck;

    invoke-static {v1}, Lcom/osp/app/signin/UserValidateCheck;->x(Lcom/osp/app/signin/UserValidateCheck;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_6

    invoke-static {}, Lcom/osp/app/util/p;->a()Lcom/osp/app/util/p;

    move-result-object v0

    iget-object v1, p0, Lcom/osp/app/signin/po;->c:Lcom/osp/app/signin/UserValidateCheck;

    invoke-static {v1}, Lcom/osp/app/signin/UserValidateCheck;->r(Lcom/osp/app/signin/UserValidateCheck;)Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/osp/app/util/p;->c(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_6

    iget-boolean v0, p0, Lcom/osp/app/signin/po;->q:Z

    if-eqz v0, :cond_6

    .line 2247
    iget-object v0, p0, Lcom/osp/app/signin/po;->c:Lcom/osp/app/signin/UserValidateCheck;

    new-instance v1, Ljava/util/HashMap;

    invoke-direct {v1}, Ljava/util/HashMap;-><init>()V

    invoke-static {v0, v1}, Lcom/osp/app/signin/UserValidateCheck;->a(Lcom/osp/app/signin/UserValidateCheck;Ljava/util/HashMap;)Ljava/util/HashMap;

    .line 2248
    const/4 v0, 0x0

    .line 2251
    :try_start_0
    iget-object v1, p0, Lcom/osp/app/signin/po;->c:Lcom/osp/app/signin/UserValidateCheck;

    invoke-static {v1}, Lcom/osp/device/d;->a(Landroid/content/Context;)Lcom/osp/device/a;

    move-result-object v1

    invoke-virtual {v1}, Lcom/osp/device/a;->c()Ljava/lang/String;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 2256
    :goto_1
    invoke-static {}, Lcom/osp/device/b;->b()Lcom/osp/device/b;

    move-result-object v1

    invoke-virtual {v1}, Lcom/osp/device/b;->a()Z

    move-result v1

    if-eqz v1, :cond_5

    iget-object v1, p0, Lcom/osp/app/signin/po;->c:Lcom/osp/app/signin/UserValidateCheck;

    invoke-static {v1}, Lcom/osp/app/util/ad;->h(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Landroid/telephony/PhoneNumberUtils;->isGlobalPhoneNumber(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_5

    .line 2259
    iget-object v0, p0, Lcom/osp/app/signin/po;->c:Lcom/osp/app/signin/UserValidateCheck;

    invoke-static {v0}, Lcom/msc/c/e;->h(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/osp/app/signin/po;->b(Ljava/lang/String;)V

    .line 2287
    :goto_2
    iget-boolean v0, p0, Lcom/osp/app/signin/po;->e:Z

    if-eqz v0, :cond_1

    .line 2295
    iget-object v0, p0, Lcom/osp/app/signin/po;->c:Lcom/osp/app/signin/UserValidateCheck;

    invoke-static {v0}, Lcom/osp/app/signin/UserValidateCheck;->y(Lcom/osp/app/signin/UserValidateCheck;)Z

    move-result v0

    if-eqz v0, :cond_a

    .line 2297
    invoke-direct {p0}, Lcom/osp/app/signin/po;->h()V

    goto/16 :goto_0

    .line 2252
    :catch_0
    move-exception v1

    invoke-virtual {v1}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_1

    .line 2262
    :cond_5
    iget-object v1, p0, Lcom/osp/app/signin/po;->c:Lcom/osp/app/signin/UserValidateCheck;

    invoke-static {v1}, Lcom/osp/app/signin/UserValidateCheck;->r(Lcom/osp/app/signin/UserValidateCheck;)Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, Lcom/msc/c/e;->g(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/osp/app/signin/po;->c:Lcom/osp/app/signin/UserValidateCheck;

    invoke-static {v2}, Lcom/osp/app/signin/UserValidateCheck;->s(Lcom/osp/app/signin/UserValidateCheck;)Landroid/widget/TextView;

    move-result-object v2

    invoke-virtual {v2}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v2

    invoke-interface {v2}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {p0, v1, v2, v0}, Lcom/osp/app/signin/po;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_2

    .line 2265
    :cond_6
    const-string v0, "netflix"

    iget-object v1, p0, Lcom/osp/app/signin/po;->c:Lcom/osp/app/signin/UserValidateCheck;

    invoke-static {v1}, Lcom/osp/app/signin/UserValidateCheck;->x(Lcom/osp/app/signin/UserValidateCheck;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_8

    .line 2267
    invoke-static {}, Lcom/osp/device/b;->b()Lcom/osp/device/b;

    move-result-object v0

    invoke-virtual {v0}, Lcom/osp/device/b;->a()Z

    move-result v0

    if-eqz v0, :cond_7

    iget-object v0, p0, Lcom/osp/app/signin/po;->c:Lcom/osp/app/signin/UserValidateCheck;

    invoke-static {v0}, Lcom/osp/app/util/ad;->h(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/telephony/PhoneNumberUtils;->isGlobalPhoneNumber(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_7

    .line 2270
    iget-object v0, p0, Lcom/osp/app/signin/po;->c:Lcom/osp/app/signin/UserValidateCheck;

    invoke-static {v0}, Lcom/msc/c/e;->h(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/osp/app/signin/po;->b(Ljava/lang/String;)V

    goto :goto_2

    .line 2273
    :cond_7
    iget-object v0, p0, Lcom/osp/app/signin/po;->c:Lcom/osp/app/signin/UserValidateCheck;

    new-instance v1, Ljava/util/HashMap;

    invoke-direct {v1}, Ljava/util/HashMap;-><init>()V

    invoke-static {v0, v1}, Lcom/osp/app/signin/UserValidateCheck;->a(Lcom/osp/app/signin/UserValidateCheck;Ljava/util/HashMap;)Ljava/util/HashMap;

    .line 2274
    iget-object v0, p0, Lcom/osp/app/signin/po;->c:Lcom/osp/app/signin/UserValidateCheck;

    invoke-static {v0}, Lcom/osp/app/signin/UserValidateCheck;->s(Lcom/osp/app/signin/UserValidateCheck;)Landroid/widget/TextView;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v0

    invoke-interface {v0}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/osp/app/signin/po;->c:Lcom/osp/app/signin/UserValidateCheck;

    invoke-static {v1}, Lcom/osp/app/signin/UserValidateCheck;->e(Lcom/osp/app/signin/UserValidateCheck;)Landroid/widget/EditText;

    move-result-object v1

    invoke-virtual {v1}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {p0, v0, v1}, Lcom/osp/app/signin/po;->c(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_2

    .line 2278
    :cond_8
    invoke-static {}, Lcom/osp/device/b;->b()Lcom/osp/device/b;

    move-result-object v0

    invoke-virtual {v0}, Lcom/osp/device/b;->a()Z

    move-result v0

    if-eqz v0, :cond_9

    iget-object v0, p0, Lcom/osp/app/signin/po;->c:Lcom/osp/app/signin/UserValidateCheck;

    invoke-static {v0}, Lcom/osp/app/util/ad;->h(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/telephony/PhoneNumberUtils;->isGlobalPhoneNumber(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_9

    .line 2281
    iget-object v0, p0, Lcom/osp/app/signin/po;->c:Lcom/osp/app/signin/UserValidateCheck;

    invoke-static {v0}, Lcom/msc/c/e;->h(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/osp/app/signin/po;->b(Ljava/lang/String;)V

    goto/16 :goto_2

    .line 2284
    :cond_9
    iget-object v0, p0, Lcom/osp/app/signin/po;->c:Lcom/osp/app/signin/UserValidateCheck;

    invoke-static {v0}, Lcom/msc/c/e;->h(Landroid/content/Context;)Ljava/lang/String;

    iget-object v0, p0, Lcom/osp/app/signin/po;->c:Lcom/osp/app/signin/UserValidateCheck;

    invoke-static {v0}, Lcom/osp/app/signin/UserValidateCheck;->s(Lcom/osp/app/signin/UserValidateCheck;)Landroid/widget/TextView;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v0

    invoke-interface {v0}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/osp/app/signin/po;->c:Lcom/osp/app/signin/UserValidateCheck;

    invoke-static {v1}, Lcom/osp/app/signin/UserValidateCheck;->e(Lcom/osp/app/signin/UserValidateCheck;)Landroid/widget/EditText;

    move-result-object v1

    invoke-virtual {v1}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {p0, v0, v1}, Lcom/osp/app/signin/po;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_2

    .line 2300
    :cond_a
    const-string v0, "Success"

    iput-object v0, p0, Lcom/osp/app/signin/po;->d:Ljava/lang/String;

    goto/16 :goto_0
.end method

.method public final declared-synchronized a(Lcom/msc/b/c;)V
    .locals 6

    .prologue
    const/4 v1, 0x0

    .line 2381
    monitor-enter p0

    :try_start_0
    invoke-super {p0, p1}, Lcom/msc/c/b;->a(Lcom/msc/b/c;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 2383
    if-nez p1, :cond_1

    .line 2690
    :cond_0
    :goto_0
    monitor-exit p0

    return-void

    .line 2388
    :cond_1
    :try_start_1
    invoke-virtual {p1}, Lcom/msc/b/c;->a()J

    move-result-wide v2

    .line 2389
    invoke-virtual {p1}, Lcom/msc/b/c;->e()Ljava/lang/String;

    move-result-object v0

    .line 2391
    iget-wide v4, p0, Lcom/osp/app/signin/po;->f:J
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    cmp-long v4, v2, v4

    if-nez v4, :cond_6

    .line 2395
    :try_start_2
    invoke-static {}, Lcom/msc/c/h;->a()Lcom/msc/c/h;

    iget-object v2, p0, Lcom/osp/app/signin/po;->c:Lcom/osp/app/signin/UserValidateCheck;

    invoke-static {v2, v0}, Lcom/msc/c/h;->b(Landroid/content/Context;Ljava/lang/String;)Lcom/msc/a/g;

    move-result-object v0

    .line 2397
    if-eqz v0, :cond_0

    .line 2399
    iget-object v0, p0, Lcom/osp/app/signin/po;->c:Lcom/osp/app/signin/UserValidateCheck;

    invoke-static {v0}, Lcom/osp/app/signin/UserValidateCheck;->r(Lcom/osp/app/signin/UserValidateCheck;)Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/msc/openprovider/b;->a(Landroid/content/Context;)I

    move-result v0

    iput v0, p0, Lcom/osp/app/signin/po;->o:I

    .line 2400
    iget v0, p0, Lcom/osp/app/signin/po;->o:I

    if-lez v0, :cond_2

    .line 2402
    const-string v0, "require more process"

    iput-object v0, p0, Lcom/osp/app/signin/po;->d:Ljava/lang/String;
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_0
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0

    .line 2457
    :catch_0
    move-exception v0

    .line 2459
    :try_start_3
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    .line 2460
    new-instance v1, Lcom/msc/c/f;

    invoke-direct {v1, v0}, Lcom/msc/c/f;-><init>(Ljava/lang/Exception;)V

    iput-object v1, p0, Lcom/osp/app/signin/po;->b:Lcom/msc/c/f;
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    goto :goto_0

    .line 2381
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 2405
    :cond_2
    :try_start_4
    invoke-virtual {p0}, Lcom/osp/app/signin/po;->isCancelled()Z

    move-result v0

    if-nez v0, :cond_0

    .line 2409
    invoke-static {}, Lcom/msc/c/g;->a()Lcom/msc/c/g;

    iget-object v0, p0, Lcom/osp/app/signin/po;->c:Lcom/osp/app/signin/UserValidateCheck;

    iget-object v2, p0, Lcom/osp/app/signin/po;->c:Lcom/osp/app/signin/UserValidateCheck;

    invoke-static {v2}, Lcom/osp/app/signin/UserValidateCheck;->p(Lcom/osp/app/signin/UserValidateCheck;)Ljava/lang/String;

    move-result-object v2

    iget-object v3, p0, Lcom/osp/app/signin/po;->c:Lcom/osp/app/signin/UserValidateCheck;

    invoke-static {v3}, Lcom/osp/app/util/ad;->h(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v3

    invoke-static {v0, v2, v3, p0}, Lcom/msc/c/g;->c(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Lcom/msc/b/h;)J

    move-result-wide v2

    iput-wide v2, p0, Lcom/osp/app/signin/po;->g:J

    iget-wide v2, p0, Lcom/osp/app/signin/po;->g:J

    invoke-virtual {p0, v2, v3}, Lcom/osp/app/signin/po;->a(J)V

    iget-wide v2, p0, Lcom/osp/app/signin/po;->g:J

    const-string v0, "none"

    invoke-virtual {p0, v2, v3, v0}, Lcom/osp/app/signin/po;->a(JLjava/lang/String;)V

    iget-wide v2, p0, Lcom/osp/app/signin/po;->g:J

    sget-object v0, Lcom/msc/b/g;->c:Lcom/msc/b/g;

    invoke-static {v2, v3, v0}, Lcom/msc/c/g;->a(JLcom/msc/b/g;)V

    .line 2411
    invoke-virtual {p0}, Lcom/osp/app/signin/po;->isCancelled()Z

    move-result v0

    if-nez v0, :cond_0

    .line 2417
    const-string v0, "netflix"

    iget-object v2, p0, Lcom/osp/app/signin/po;->c:Lcom/osp/app/signin/UserValidateCheck;

    invoke-static {v2}, Lcom/osp/app/signin/UserValidateCheck;->x(Lcom/osp/app/signin/UserValidateCheck;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-static {}, Lcom/osp/app/util/p;->a()Lcom/osp/app/util/p;

    move-result-object v0

    iget-object v2, p0, Lcom/osp/app/signin/po;->c:Lcom/osp/app/signin/UserValidateCheck;

    invoke-static {v2}, Lcom/osp/app/signin/UserValidateCheck;->r(Lcom/osp/app/signin/UserValidateCheck;)Landroid/content/Context;

    move-result-object v2

    invoke-virtual {v0, v2}, Lcom/osp/app/util/p;->c(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_3

    iget-boolean v0, p0, Lcom/osp/app/signin/po;->q:Z

    if-eqz v0, :cond_3

    .line 2420
    iget-object v0, p0, Lcom/osp/app/signin/po;->c:Lcom/osp/app/signin/UserValidateCheck;

    new-instance v2, Ljava/util/HashMap;

    invoke-direct {v2}, Ljava/util/HashMap;-><init>()V

    invoke-static {v0, v2}, Lcom/osp/app/signin/UserValidateCheck;->a(Lcom/osp/app/signin/UserValidateCheck;Ljava/util/HashMap;)Ljava/util/HashMap;
    :try_end_4
    .catch Ljava/lang/Exception; {:try_start_4 .. :try_end_4} :catch_0
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    .line 2424
    :try_start_5
    iget-object v0, p0, Lcom/osp/app/signin/po;->c:Lcom/osp/app/signin/UserValidateCheck;

    invoke-static {v0}, Lcom/osp/device/d;->a(Landroid/content/Context;)Lcom/osp/device/a;

    move-result-object v0

    invoke-virtual {v0}, Lcom/osp/device/a;->c()Ljava/lang/String;
    :try_end_5
    .catch Ljava/lang/Exception; {:try_start_5 .. :try_end_5} :catch_1
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    move-result-object v1

    .line 2429
    :goto_1
    :try_start_6
    iget-object v0, p0, Lcom/osp/app/signin/po;->c:Lcom/osp/app/signin/UserValidateCheck;

    invoke-static {v0}, Lcom/osp/app/signin/UserValidateCheck;->r(Lcom/osp/app/signin/UserValidateCheck;)Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/msc/c/e;->g(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v0

    iget-object v2, p0, Lcom/osp/app/signin/po;->c:Lcom/osp/app/signin/UserValidateCheck;

    invoke-static {v2}, Lcom/osp/app/signin/UserValidateCheck;->s(Lcom/osp/app/signin/UserValidateCheck;)Landroid/widget/TextView;

    move-result-object v2

    invoke-virtual {v2}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v2

    invoke-interface {v2}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {p0, v0, v2, v1}, Lcom/osp/app/signin/po;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 2439
    :goto_2
    iget-boolean v0, p0, Lcom/osp/app/signin/po;->e:Z

    if-eqz v0, :cond_0

    .line 2447
    iget-object v0, p0, Lcom/osp/app/signin/po;->c:Lcom/osp/app/signin/UserValidateCheck;

    invoke-static {v0}, Lcom/osp/app/signin/UserValidateCheck;->y(Lcom/osp/app/signin/UserValidateCheck;)Z

    move-result v0

    if-eqz v0, :cond_5

    .line 2449
    invoke-direct {p0}, Lcom/osp/app/signin/po;->h()V

    goto/16 :goto_0

    .line 2425
    :catch_1
    move-exception v0

    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_1

    .line 2431
    :cond_3
    const-string v0, "netflix"

    iget-object v1, p0, Lcom/osp/app/signin/po;->c:Lcom/osp/app/signin/UserValidateCheck;

    invoke-static {v1}, Lcom/osp/app/signin/UserValidateCheck;->x(Lcom/osp/app/signin/UserValidateCheck;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 2433
    iget-object v0, p0, Lcom/osp/app/signin/po;->c:Lcom/osp/app/signin/UserValidateCheck;

    new-instance v1, Ljava/util/HashMap;

    invoke-direct {v1}, Ljava/util/HashMap;-><init>()V

    invoke-static {v0, v1}, Lcom/osp/app/signin/UserValidateCheck;->a(Lcom/osp/app/signin/UserValidateCheck;Ljava/util/HashMap;)Ljava/util/HashMap;

    .line 2434
    iget-object v0, p0, Lcom/osp/app/signin/po;->c:Lcom/osp/app/signin/UserValidateCheck;

    invoke-static {v0}, Lcom/osp/app/signin/UserValidateCheck;->s(Lcom/osp/app/signin/UserValidateCheck;)Landroid/widget/TextView;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v0

    invoke-interface {v0}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/osp/app/signin/po;->c:Lcom/osp/app/signin/UserValidateCheck;

    invoke-static {v1}, Lcom/osp/app/signin/UserValidateCheck;->e(Lcom/osp/app/signin/UserValidateCheck;)Landroid/widget/EditText;

    move-result-object v1

    invoke-virtual {v1}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {p0, v0, v1}, Lcom/osp/app/signin/po;->c(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_2

    .line 2437
    :cond_4
    iget-object v0, p0, Lcom/osp/app/signin/po;->c:Lcom/osp/app/signin/UserValidateCheck;

    invoke-static {v0}, Lcom/msc/c/e;->h(Landroid/content/Context;)Ljava/lang/String;

    iget-object v0, p0, Lcom/osp/app/signin/po;->c:Lcom/osp/app/signin/UserValidateCheck;

    invoke-static {v0}, Lcom/osp/app/signin/UserValidateCheck;->s(Lcom/osp/app/signin/UserValidateCheck;)Landroid/widget/TextView;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v0

    invoke-interface {v0}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/osp/app/signin/po;->c:Lcom/osp/app/signin/UserValidateCheck;

    invoke-static {v1}, Lcom/osp/app/signin/UserValidateCheck;->e(Lcom/osp/app/signin/UserValidateCheck;)Landroid/widget/EditText;

    move-result-object v1

    invoke-virtual {v1}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {p0, v0, v1}, Lcom/osp/app/signin/po;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_2

    .line 2452
    :cond_5
    const-string v0, "Success"

    iput-object v0, p0, Lcom/osp/app/signin/po;->d:Ljava/lang/String;
    :try_end_6
    .catch Ljava/lang/Exception; {:try_start_6 .. :try_end_6} :catch_0
    .catchall {:try_start_6 .. :try_end_6} :catchall_0

    goto/16 :goto_0

    .line 2462
    :cond_6
    :try_start_7
    iget-wide v4, p0, Lcom/osp/app/signin/po;->g:J
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_0

    cmp-long v4, v2, v4

    if-nez v4, :cond_7

    .line 2467
    :try_start_8
    iget-object v1, p0, Lcom/osp/app/signin/po;->c:Lcom/osp/app/signin/UserValidateCheck;

    invoke-static {}, Lcom/msc/c/h;->a()Lcom/msc/c/h;

    move-result-object v2

    iget-object v3, p0, Lcom/osp/app/signin/po;->c:Lcom/osp/app/signin/UserValidateCheck;

    iget-object v4, p0, Lcom/osp/app/signin/po;->c:Lcom/osp/app/signin/UserValidateCheck;

    invoke-static {v4}, Lcom/osp/app/signin/UserValidateCheck;->p(Lcom/osp/app/signin/UserValidateCheck;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v3, v4, v0}, Lcom/msc/c/h;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)Lcom/osp/app/signin/kd;

    move-result-object v0

    invoke-static {v1, v0}, Lcom/osp/app/signin/UserValidateCheck;->a(Lcom/osp/app/signin/UserValidateCheck;Lcom/osp/app/signin/kd;)Lcom/osp/app/signin/kd;
    :try_end_8
    .catch Ljava/lang/Exception; {:try_start_8 .. :try_end_8} :catch_2
    .catchall {:try_start_8 .. :try_end_8} :catchall_0

    goto/16 :goto_0

    .line 2469
    :catch_2
    move-exception v0

    :try_start_9
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    goto/16 :goto_0

    .line 2473
    :cond_7
    iget-wide v4, p0, Lcom/osp/app/signin/po;->h:J
    :try_end_9
    .catchall {:try_start_9 .. :try_end_9} :catchall_0

    cmp-long v4, v2, v4

    if-nez v4, :cond_8

    .line 2477
    :try_start_a
    invoke-static {}, Lcom/msc/c/h;->a()Lcom/msc/c/h;

    invoke-static {v0}, Lcom/msc/c/h;->t(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 2478
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v0

    if-lez v0, :cond_0

    .line 2480
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/osp/app/signin/po;->e:Z
    :try_end_a
    .catch Ljava/lang/Exception; {:try_start_a .. :try_end_a} :catch_3
    .catchall {:try_start_a .. :try_end_a} :catchall_0

    goto/16 :goto_0

    .line 2482
    :catch_3
    move-exception v0

    .line 2484
    :try_start_b
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    .line 2485
    new-instance v1, Lcom/msc/c/f;

    invoke-direct {v1, v0}, Lcom/msc/c/f;-><init>(Ljava/lang/Exception;)V

    iput-object v1, p0, Lcom/osp/app/signin/po;->b:Lcom/msc/c/f;

    goto/16 :goto_0

    .line 2487
    :cond_8
    iget-wide v4, p0, Lcom/osp/app/signin/po;->i:J

    cmp-long v4, v2, v4

    if-eqz v4, :cond_9

    iget-wide v4, p0, Lcom/osp/app/signin/po;->j:J
    :try_end_b
    .catchall {:try_start_b .. :try_end_b} :catchall_0

    cmp-long v4, v2, v4

    if-nez v4, :cond_a

    .line 2491
    :cond_9
    :try_start_c
    invoke-static {}, Lcom/msc/c/h;->a()Lcom/msc/c/h;

    iget-object v1, p0, Lcom/osp/app/signin/po;->c:Lcom/osp/app/signin/UserValidateCheck;

    invoke-static {v1}, Lcom/osp/app/signin/UserValidateCheck;->w(Lcom/osp/app/signin/UserValidateCheck;)Ljava/util/HashMap;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/msc/c/h;->a(Ljava/lang/String;Ljava/util/HashMap;)Ljava/lang/String;

    move-result-object v0

    .line 2492
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v0

    if-lez v0, :cond_0

    .line 2494
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/osp/app/signin/po;->e:Z
    :try_end_c
    .catch Ljava/lang/Exception; {:try_start_c .. :try_end_c} :catch_4
    .catchall {:try_start_c .. :try_end_c} :catchall_0

    goto/16 :goto_0

    .line 2496
    :catch_4
    move-exception v0

    .line 2498
    :try_start_d
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    .line 2499
    new-instance v1, Lcom/msc/c/f;

    invoke-direct {v1, v0}, Lcom/msc/c/f;-><init>(Ljava/lang/Exception;)V

    iput-object v1, p0, Lcom/osp/app/signin/po;->b:Lcom/msc/c/f;

    goto/16 :goto_0

    .line 2501
    :cond_a
    iget-wide v4, p0, Lcom/osp/app/signin/po;->l:J
    :try_end_d
    .catchall {:try_start_d .. :try_end_d} :catchall_0

    cmp-long v4, v2, v4

    if-nez v4, :cond_c

    .line 2505
    :try_start_e
    iget-object v1, p0, Lcom/osp/app/signin/po;->c:Lcom/osp/app/signin/UserValidateCheck;

    invoke-static {}, Lcom/msc/c/h;->a()Lcom/msc/c/h;

    invoke-static {v0}, Lcom/msc/c/h;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Lcom/osp/app/signin/UserValidateCheck;->c(Lcom/osp/app/signin/UserValidateCheck;Ljava/lang/String;)Ljava/lang/String;

    .line 2506
    iget-object v0, p0, Lcom/osp/app/signin/po;->c:Lcom/osp/app/signin/UserValidateCheck;

    invoke-static {v0}, Lcom/osp/app/signin/UserValidateCheck;->A(Lcom/osp/app/signin/UserValidateCheck;)Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_b

    iget-object v0, p0, Lcom/osp/app/signin/po;->c:Lcom/osp/app/signin/UserValidateCheck;

    invoke-static {v0}, Lcom/osp/app/signin/UserValidateCheck;->A(Lcom/osp/app/signin/UserValidateCheck;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v0

    if-lez v0, :cond_b

    .line 2508
    iget-object v0, p0, Lcom/osp/app/signin/po;->c:Lcom/osp/app/signin/UserValidateCheck;

    iget-object v1, p0, Lcom/osp/app/signin/po;->c:Lcom/osp/app/signin/UserValidateCheck;

    invoke-static {v1}, Lcom/osp/app/signin/UserValidateCheck;->A(Lcom/osp/app/signin/UserValidateCheck;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/msc/c/e;->e(Landroid/content/Context;Ljava/lang/String;)V

    .line 2509
    invoke-virtual {p0}, Lcom/osp/app/signin/po;->isCancelled()Z

    move-result v0

    if-nez v0, :cond_0

    .line 2513
    iget-object v0, p0, Lcom/osp/app/signin/po;->c:Lcom/osp/app/signin/UserValidateCheck;

    invoke-static {v0}, Lcom/osp/app/signin/UserValidateCheck;->A(Lcom/osp/app/signin/UserValidateCheck;)Ljava/lang/String;

    move-result-object v0

    invoke-static {}, Lcom/msc/c/g;->a()Lcom/msc/c/g;

    iget-object v1, p0, Lcom/osp/app/signin/po;->c:Lcom/osp/app/signin/UserValidateCheck;

    iget-object v2, p0, Lcom/osp/app/signin/po;->c:Lcom/osp/app/signin/UserValidateCheck;

    invoke-static {v2}, Lcom/osp/app/signin/UserValidateCheck;->p(Lcom/osp/app/signin/UserValidateCheck;)Ljava/lang/String;

    move-result-object v2

    iget-object v3, p0, Lcom/osp/app/signin/po;->c:Lcom/osp/app/signin/UserValidateCheck;

    invoke-static {v3}, Lcom/osp/app/signin/UserValidateCheck;->n(Lcom/osp/app/signin/UserValidateCheck;)Ljava/lang/String;

    move-result-object v3

    invoke-static {v1, v2, v0, v3, p0}, Lcom/msc/c/g;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/msc/b/h;)J

    move-result-wide v0

    iput-wide v0, p0, Lcom/osp/app/signin/po;->m:J

    iget-wide v0, p0, Lcom/osp/app/signin/po;->m:J

    invoke-virtual {p0, v0, v1}, Lcom/osp/app/signin/po;->a(J)V

    iget-wide v0, p0, Lcom/osp/app/signin/po;->m:J

    const-string v2, "from_json"

    invoke-virtual {p0, v0, v1, v2}, Lcom/osp/app/signin/po;->a(JLjava/lang/String;)V

    iget-wide v0, p0, Lcom/osp/app/signin/po;->m:J

    sget-object v2, Lcom/msc/b/g;->b:Lcom/msc/b/g;

    invoke-static {v0, v1, v2}, Lcom/msc/c/g;->a(JLcom/msc/b/g;)V
    :try_end_e
    .catch Ljava/lang/Exception; {:try_start_e .. :try_end_e} :catch_5
    .catchall {:try_start_e .. :try_end_e} :catchall_0

    goto/16 :goto_0

    .line 2518
    :catch_5
    move-exception v0

    .line 2520
    :try_start_f
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    .line 2521
    new-instance v1, Lcom/msc/c/f;

    invoke-direct {v1, v0}, Lcom/msc/c/f;-><init>(Ljava/lang/Exception;)V

    iput-object v1, p0, Lcom/osp/app/signin/po;->b:Lcom/msc/c/f;
    :try_end_f
    .catchall {:try_start_f .. :try_end_f} :catchall_0

    goto/16 :goto_0

    .line 2516
    :cond_b
    :try_start_10
    new-instance v0, Lcom/msc/c/f;

    invoke-direct {v0}, Lcom/msc/c/f;-><init>()V

    iput-object v0, p0, Lcom/osp/app/signin/po;->b:Lcom/msc/c/f;
    :try_end_10
    .catch Ljava/lang/Exception; {:try_start_10 .. :try_end_10} :catch_5
    .catchall {:try_start_10 .. :try_end_10} :catchall_0

    goto/16 :goto_0

    .line 2523
    :cond_c
    :try_start_11
    iget-wide v4, p0, Lcom/osp/app/signin/po;->m:J

    cmp-long v4, v2, v4

    if-nez v4, :cond_d

    .line 2525
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "onSuccess() : "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;)V
    :try_end_11
    .catchall {:try_start_11 .. :try_end_11} :catchall_0

    .line 2528
    :try_start_12
    iget-object v1, p0, Lcom/osp/app/signin/po;->c:Lcom/osp/app/signin/UserValidateCheck;

    invoke-static {}, Lcom/msc/c/h;->a()Lcom/msc/c/h;

    invoke-static {v0}, Lcom/msc/c/h;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Lcom/osp/app/signin/UserValidateCheck;->d(Lcom/osp/app/signin/UserValidateCheck;Ljava/lang/String;)Ljava/lang/String;

    .line 2529
    const-string v0, "Success"

    iput-object v0, p0, Lcom/osp/app/signin/po;->d:Ljava/lang/String;
    :try_end_12
    .catch Ljava/lang/Exception; {:try_start_12 .. :try_end_12} :catch_6
    .catchall {:try_start_12 .. :try_end_12} :catchall_0

    goto/16 :goto_0

    .line 2530
    :catch_6
    move-exception v0

    .line 2532
    :try_start_13
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    .line 2533
    new-instance v1, Lcom/msc/c/f;

    invoke-direct {v1, v0}, Lcom/msc/c/f;-><init>(Ljava/lang/Exception;)V

    iput-object v1, p0, Lcom/osp/app/signin/po;->b:Lcom/msc/c/f;

    goto/16 :goto_0

    .line 2536
    :cond_d
    invoke-static {}, Lcom/osp/device/b;->b()Lcom/osp/device/b;

    move-result-object v4

    invoke-virtual {v4}, Lcom/osp/device/b;->a()Z

    move-result v4

    if-eqz v4, :cond_15

    iget-wide v4, p0, Lcom/osp/app/signin/po;->k:J
    :try_end_13
    .catchall {:try_start_13 .. :try_end_13} :catchall_0

    cmp-long v4, v2, v4

    if-nez v4, :cond_15

    .line 2540
    :try_start_14
    invoke-static {}, Lcom/msc/c/h;->a()Lcom/msc/c/h;

    invoke-static {v0}, Lcom/msc/c/h;->L(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 2541
    const-string v2, "A"

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_11

    .line 2543
    invoke-static {}, Lcom/osp/app/util/p;->a()Lcom/osp/app/util/p;

    move-result-object v0

    iget-object v2, p0, Lcom/osp/app/signin/po;->c:Lcom/osp/app/signin/UserValidateCheck;

    invoke-static {v2}, Lcom/osp/app/signin/UserValidateCheck;->r(Lcom/osp/app/signin/UserValidateCheck;)Landroid/content/Context;

    move-result-object v2

    invoke-virtual {v0, v2}, Lcom/osp/app/util/p;->c(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_f

    iget-boolean v0, p0, Lcom/osp/app/signin/po;->q:Z

    if-eqz v0, :cond_f

    .line 2545
    const-string v0, "netflix"

    iget-object v2, p0, Lcom/osp/app/signin/po;->c:Lcom/osp/app/signin/UserValidateCheck;

    invoke-static {v2}, Lcom/osp/app/signin/UserValidateCheck;->x(Lcom/osp/app/signin/UserValidateCheck;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_e

    .line 2547
    iget-object v0, p0, Lcom/osp/app/signin/po;->c:Lcom/osp/app/signin/UserValidateCheck;

    new-instance v2, Ljava/util/HashMap;

    invoke-direct {v2}, Ljava/util/HashMap;-><init>()V

    invoke-static {v0, v2}, Lcom/osp/app/signin/UserValidateCheck;->a(Lcom/osp/app/signin/UserValidateCheck;Ljava/util/HashMap;)Ljava/util/HashMap;
    :try_end_14
    .catch Ljava/lang/Exception; {:try_start_14 .. :try_end_14} :catch_7
    .catchall {:try_start_14 .. :try_end_14} :catchall_0

    .line 2551
    :try_start_15
    iget-object v0, p0, Lcom/osp/app/signin/po;->c:Lcom/osp/app/signin/UserValidateCheck;

    invoke-static {v0}, Lcom/osp/device/d;->a(Landroid/content/Context;)Lcom/osp/device/a;

    move-result-object v0

    invoke-virtual {v0}, Lcom/osp/device/a;->c()Ljava/lang/String;
    :try_end_15
    .catch Ljava/lang/Exception; {:try_start_15 .. :try_end_15} :catch_8
    .catchall {:try_start_15 .. :try_end_15} :catchall_0

    move-result-object v0

    .line 2556
    :goto_3
    :try_start_16
    iget-object v1, p0, Lcom/osp/app/signin/po;->c:Lcom/osp/app/signin/UserValidateCheck;

    invoke-static {v1}, Lcom/osp/app/signin/UserValidateCheck;->r(Lcom/osp/app/signin/UserValidateCheck;)Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, Lcom/msc/c/e;->g(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/osp/app/signin/po;->c:Lcom/osp/app/signin/UserValidateCheck;

    invoke-static {v2}, Lcom/osp/app/signin/UserValidateCheck;->s(Lcom/osp/app/signin/UserValidateCheck;)Landroid/widget/TextView;

    move-result-object v2

    invoke-virtual {v2}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v2

    invoke-interface {v2}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {p0, v1, v2, v0}, Lcom/osp/app/signin/po;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    :try_end_16
    .catch Ljava/lang/Exception; {:try_start_16 .. :try_end_16} :catch_7
    .catchall {:try_start_16 .. :try_end_16} :catchall_0

    goto/16 :goto_0

    .line 2607
    :catch_7
    move-exception v0

    .line 2609
    :try_start_17
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    .line 2610
    new-instance v1, Lcom/msc/c/f;

    invoke-direct {v1, v0}, Lcom/msc/c/f;-><init>(Ljava/lang/Exception;)V

    iput-object v1, p0, Lcom/osp/app/signin/po;->b:Lcom/msc/c/f;
    :try_end_17
    .catchall {:try_start_17 .. :try_end_17} :catchall_0

    goto/16 :goto_0

    .line 2552
    :catch_8
    move-exception v0

    :try_start_18
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    move-object v0, v1

    goto :goto_3

    .line 2559
    :cond_e
    const-string v0, "Success"

    iput-object v0, p0, Lcom/osp/app/signin/po;->d:Ljava/lang/String;

    goto/16 :goto_0

    .line 2562
    :cond_f
    const-string v0, "netflix"

    iget-object v1, p0, Lcom/osp/app/signin/po;->c:Lcom/osp/app/signin/UserValidateCheck;

    invoke-static {v1}, Lcom/osp/app/signin/UserValidateCheck;->x(Lcom/osp/app/signin/UserValidateCheck;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_10

    .line 2564
    iget-object v0, p0, Lcom/osp/app/signin/po;->c:Lcom/osp/app/signin/UserValidateCheck;

    new-instance v1, Ljava/util/HashMap;

    invoke-direct {v1}, Ljava/util/HashMap;-><init>()V

    invoke-static {v0, v1}, Lcom/osp/app/signin/UserValidateCheck;->a(Lcom/osp/app/signin/UserValidateCheck;Ljava/util/HashMap;)Ljava/util/HashMap;

    .line 2565
    iget-object v0, p0, Lcom/osp/app/signin/po;->c:Lcom/osp/app/signin/UserValidateCheck;

    invoke-static {v0}, Lcom/osp/app/signin/UserValidateCheck;->s(Lcom/osp/app/signin/UserValidateCheck;)Landroid/widget/TextView;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v0

    invoke-interface {v0}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/osp/app/signin/po;->c:Lcom/osp/app/signin/UserValidateCheck;

    invoke-static {v1}, Lcom/osp/app/signin/UserValidateCheck;->e(Lcom/osp/app/signin/UserValidateCheck;)Landroid/widget/EditText;

    move-result-object v1

    invoke-virtual {v1}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {p0, v0, v1}, Lcom/osp/app/signin/po;->c(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 2568
    :cond_10
    iget-object v0, p0, Lcom/osp/app/signin/po;->c:Lcom/osp/app/signin/UserValidateCheck;

    invoke-static {v0}, Lcom/msc/c/e;->h(Landroid/content/Context;)Ljava/lang/String;

    iget-object v0, p0, Lcom/osp/app/signin/po;->c:Lcom/osp/app/signin/UserValidateCheck;

    invoke-static {v0}, Lcom/osp/app/signin/UserValidateCheck;->s(Lcom/osp/app/signin/UserValidateCheck;)Landroid/widget/TextView;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v0

    invoke-interface {v0}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/osp/app/signin/po;->c:Lcom/osp/app/signin/UserValidateCheck;

    invoke-static {v1}, Lcom/osp/app/signin/UserValidateCheck;->e(Lcom/osp/app/signin/UserValidateCheck;)Landroid/widget/EditText;

    move-result-object v1

    invoke-virtual {v1}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {p0, v0, v1}, Lcom/osp/app/signin/po;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 2571
    :cond_11
    const-string v1, "N"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_13

    .line 2591
    iget-object v0, p0, Lcom/osp/app/signin/po;->c:Lcom/osp/app/signin/UserValidateCheck;

    invoke-static {v0}, Lcom/osp/app/signin/UserValidateCheck;->z(Lcom/osp/app/signin/UserValidateCheck;)Z

    move-result v0

    if-eqz v0, :cond_12

    .line 2593
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/osp/app/signin/po;->b:Lcom/msc/c/f;

    .line 2594
    const-string v0, "IdDeleted"

    iput-object v0, p0, Lcom/osp/app/signin/po;->d:Ljava/lang/String;

    goto/16 :goto_0

    .line 2597
    :cond_12
    new-instance v0, Lcom/msc/c/f;

    const-string v1, "USR_3113"

    const-string v2, "There is no user associated."

    invoke-direct {v0, v1, v2}, Lcom/msc/c/f;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    iput-object v0, p0, Lcom/osp/app/signin/po;->b:Lcom/msc/c/f;

    goto/16 :goto_0

    .line 2599
    :cond_13
    const-string v1, "C"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_14

    .line 2601
    new-instance v0, Lcom/msc/c/f;

    const-string v1, "USR_3192"

    const-string v2, "The loginID is already changed."

    invoke-direct {v0, v1, v2}, Lcom/msc/c/f;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    iput-object v0, p0, Lcom/osp/app/signin/po;->b:Lcom/msc/c/f;

    goto/16 :goto_0

    .line 2604
    :cond_14
    new-instance v0, Lcom/msc/c/f;

    const-string v1, "DUPLICATED_ID"

    const-string v2, "Id duplicated"

    invoke-direct {v0, v1, v2}, Lcom/msc/c/f;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    iput-object v0, p0, Lcom/osp/app/signin/po;->b:Lcom/msc/c/f;
    :try_end_18
    .catch Ljava/lang/Exception; {:try_start_18 .. :try_end_18} :catch_7
    .catchall {:try_start_18 .. :try_end_18} :catchall_0

    goto/16 :goto_0

    .line 2613
    :cond_15
    :try_start_19
    iget-wide v4, p0, Lcom/osp/app/signin/po;->n:J

    cmp-long v1, v2, v4

    if-nez v1, :cond_0

    .line 2615
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "onSuccess() : "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;)V
    :try_end_19
    .catchall {:try_start_19 .. :try_end_19} :catchall_0

    .line 2618
    :try_start_1a
    invoke-static {}, Lcom/msc/c/h;->a()Lcom/msc/c/h;

    invoke-static {v0}, Lcom/msc/c/h;->C(Ljava/lang/String;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/osp/app/signin/po;->e:Z

    .line 2620
    iget-boolean v0, p0, Lcom/osp/app/signin/po;->e:Z

    if-eqz v0, :cond_16

    .line 2622
    const-string v0, "Success"

    iput-object v0, p0, Lcom/osp/app/signin/po;->d:Ljava/lang/String;
    :try_end_1a
    .catch Ljava/lang/Exception; {:try_start_1a .. :try_end_1a} :catch_9
    .catchall {:try_start_1a .. :try_end_1a} :catchall_0

    goto/16 :goto_0

    .line 2684
    :catch_9
    move-exception v0

    .line 2686
    :try_start_1b
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    .line 2687
    new-instance v1, Lcom/msc/c/f;

    invoke-direct {v1, v0}, Lcom/msc/c/f;-><init>(Ljava/lang/Exception;)V

    iput-object v1, p0, Lcom/osp/app/signin/po;->b:Lcom/msc/c/f;
    :try_end_1b
    .catchall {:try_start_1b .. :try_end_1b} :catchall_0

    goto/16 :goto_0

    .line 2625
    :cond_16
    :try_start_1c
    iget-object v0, p0, Lcom/osp/app/signin/po;->c:Lcom/osp/app/signin/UserValidateCheck;

    invoke-static {v0}, Lcom/msc/c/c;->e(Landroid/content/Context;)V

    .line 2626
    iget-object v0, p0, Lcom/osp/app/signin/po;->c:Lcom/osp/app/signin/UserValidateCheck;

    invoke-static {v0}, Lcom/msc/c/e;->i(Landroid/content/Context;)V

    .line 2648
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v1, p0, Lcom/osp/app/signin/po;->c:Lcom/osp/app/signin/UserValidateCheck;

    invoke-static {v1}, Lcom/osp/app/signin/UserValidateCheck;->s(Lcom/osp/app/signin/UserValidateCheck;)Landroid/widget/TextView;

    move-result-object v1

    invoke-virtual {v1}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v1

    invoke-interface {v1}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "/"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/osp/app/signin/po;->c:Lcom/osp/app/signin/UserValidateCheck;

    invoke-static {v1}, Lcom/osp/app/signin/UserValidateCheck;->p(Lcom/osp/app/signin/UserValidateCheck;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 2650
    iget-object v1, p0, Lcom/osp/app/signin/po;->c:Lcom/osp/app/signin/UserValidateCheck;

    invoke-static {v1}, Lcom/osp/app/signin/UserValidateCheck;->B(Lcom/osp/app/signin/UserValidateCheck;)Landroid/content/SharedPreferences;

    move-result-object v1

    if-nez v1, :cond_17

    .line 2652
    iget-object v1, p0, Lcom/osp/app/signin/po;->c:Lcom/osp/app/signin/UserValidateCheck;

    iget-object v2, p0, Lcom/osp/app/signin/po;->c:Lcom/osp/app/signin/UserValidateCheck;

    const-string v3, "VERIFY_COUNT"

    const/4 v4, 0x0

    invoke-virtual {v2, v3, v4}, Lcom/osp/app/signin/UserValidateCheck;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/osp/app/signin/UserValidateCheck;->a(Lcom/osp/app/signin/UserValidateCheck;Landroid/content/SharedPreferences;)Landroid/content/SharedPreferences;

    .line 2653
    iget-object v1, p0, Lcom/osp/app/signin/po;->c:Lcom/osp/app/signin/UserValidateCheck;

    invoke-static {v1}, Lcom/osp/app/signin/UserValidateCheck;->B(Lcom/osp/app/signin/UserValidateCheck;)Landroid/content/SharedPreferences;

    move-result-object v1

    invoke-interface {v1}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v1

    .line 2654
    const/4 v2, 0x0

    invoke-interface {v1, v0, v2}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    .line 2655
    invoke-interface {v1}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 2657
    :cond_17
    iget-object v1, p0, Lcom/osp/app/signin/po;->c:Lcom/osp/app/signin/UserValidateCheck;

    invoke-static {v1}, Lcom/osp/app/signin/UserValidateCheck;->B(Lcom/osp/app/signin/UserValidateCheck;)Landroid/content/SharedPreferences;

    move-result-object v1

    const/4 v2, 0x0

    invoke-interface {v1, v0, v2}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v1

    .line 2658
    add-int/lit8 v1, v1, 0x1

    .line 2659
    const/4 v2, 0x5

    if-lt v1, v2, :cond_19

    .line 2661
    iget-object v1, p0, Lcom/osp/app/signin/po;->c:Lcom/osp/app/signin/UserValidateCheck;

    invoke-static {v1}, Lcom/osp/app/signin/UserValidateCheck;->B(Lcom/osp/app/signin/UserValidateCheck;)Landroid/content/SharedPreferences;

    move-result-object v1

    invoke-interface {v1}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v1

    invoke-interface {v1, v0}, Landroid/content/SharedPreferences$Editor;->remove(Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    .line 2662
    iget-object v0, p0, Lcom/osp/app/signin/po;->c:Lcom/osp/app/signin/UserValidateCheck;

    invoke-static {v0}, Lcom/osp/app/signin/UserValidateCheck;->B(Lcom/osp/app/signin/UserValidateCheck;)Landroid/content/SharedPreferences;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 2663
    iget-object v0, p0, Lcom/osp/app/signin/po;->c:Lcom/osp/app/signin/UserValidateCheck;

    invoke-static {v0}, Lcom/osp/app/signin/UserValidateCheck;->f(Lcom/osp/app/signin/UserValidateCheck;)Landroid/content/Intent;

    move-result-object v0

    const-string v1, "passwordBlock"

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 2665
    iget-object v0, p0, Lcom/osp/app/signin/po;->c:Lcom/osp/app/signin/UserValidateCheck;

    invoke-static {v0}, Lcom/osp/app/signin/UserValidateCheck;->C(Lcom/osp/app/signin/UserValidateCheck;)Z

    move-result v0

    if-eqz v0, :cond_18

    .line 2667
    iget-object v0, p0, Lcom/osp/app/signin/po;->c:Lcom/osp/app/signin/UserValidateCheck;

    invoke-static {v0}, Lcom/osp/app/signin/UserValidateCheck;->f(Lcom/osp/app/signin/UserValidateCheck;)Landroid/content/Intent;

    move-result-object v0

    const-string v1, "error_code"

    const-string v2, "SAC_0202"

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 2668
    iget-object v0, p0, Lcom/osp/app/signin/po;->c:Lcom/osp/app/signin/UserValidateCheck;

    invoke-static {v0}, Lcom/osp/app/signin/UserValidateCheck;->f(Lcom/osp/app/signin/UserValidateCheck;)Landroid/content/Intent;

    move-result-object v0

    const-string v1, "error_message"

    const-string v2, "Password blocked"

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 2671
    :cond_18
    iget-object v0, p0, Lcom/osp/app/signin/po;->c:Lcom/osp/app/signin/UserValidateCheck;

    const/4 v1, 0x1

    iget-object v2, p0, Lcom/osp/app/signin/po;->c:Lcom/osp/app/signin/UserValidateCheck;

    invoke-static {v2}, Lcom/osp/app/signin/UserValidateCheck;->f(Lcom/osp/app/signin/UserValidateCheck;)Landroid/content/Intent;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/osp/app/signin/UserValidateCheck;->a(ILandroid/content/Intent;)V

    .line 2672
    invoke-virtual {p0}, Lcom/osp/app/signin/po;->f()V

    .line 2673
    iget-object v0, p0, Lcom/osp/app/signin/po;->c:Lcom/osp/app/signin/UserValidateCheck;

    invoke-virtual {v0}, Lcom/osp/app/signin/UserValidateCheck;->finish()V

    goto/16 :goto_0

    .line 2676
    :cond_19
    iget-object v2, p0, Lcom/osp/app/signin/po;->c:Lcom/osp/app/signin/UserValidateCheck;

    invoke-static {v2}, Lcom/osp/app/signin/UserValidateCheck;->B(Lcom/osp/app/signin/UserValidateCheck;)Landroid/content/SharedPreferences;

    move-result-object v2

    invoke-interface {v2}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v2

    .line 2677
    invoke-interface {v2, v0, v1}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    .line 2678
    invoke-interface {v2}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 2679
    new-instance v0, Lcom/msc/c/f;

    const-string v1, "IVP"

    const-string v2, "fail"

    invoke-direct {v0, v1, v2}, Lcom/msc/c/f;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    iput-object v0, p0, Lcom/osp/app/signin/po;->b:Lcom/msc/c/f;
    :try_end_1c
    .catch Ljava/lang/Exception; {:try_start_1c .. :try_end_1c} :catch_9
    .catchall {:try_start_1c .. :try_end_1c} :catchall_0

    goto/16 :goto_0
.end method

.method protected final a(Ljava/lang/Boolean;)V
    .locals 8

    .prologue
    const/4 v6, 0x0

    .line 2309
    invoke-super {p0, p1}, Lcom/msc/c/b;->a(Ljava/lang/Boolean;)V

    .line 2310
    iget-object v0, p0, Lcom/osp/app/signin/po;->b:Lcom/msc/c/f;

    if-eqz v0, :cond_9

    .line 2312
    const-string v0, "USR_3113"

    iget-object v1, p0, Lcom/osp/app/signin/po;->b:Lcom/msc/c/f;

    invoke-virtual {v1}, Lcom/msc/c/f;->a()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 2314
    iget-object v0, p0, Lcom/osp/app/signin/po;->c:Lcom/osp/app/signin/UserValidateCheck;

    iget-object v1, p0, Lcom/osp/app/signin/po;->c:Lcom/osp/app/signin/UserValidateCheck;

    invoke-static {v0, v1}, Lcom/osp/app/signin/UserValidateCheck;->a(Lcom/osp/app/signin/UserValidateCheck;Landroid/content/Context;)V

    .line 2377
    :cond_0
    :goto_0
    return-void

    .line 2316
    :cond_1
    const-string v0, "USR_3192"

    iget-object v1, p0, Lcom/osp/app/signin/po;->b:Lcom/msc/c/f;

    invoke-virtual {v1}, Lcom/msc/c/f;->a()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 2318
    iget-object v0, p0, Lcom/osp/app/signin/po;->c:Lcom/osp/app/signin/UserValidateCheck;

    iget-object v1, p0, Lcom/osp/app/signin/po;->c:Lcom/osp/app/signin/UserValidateCheck;

    invoke-static {v0, v1}, Lcom/osp/app/signin/UserValidateCheck;->b(Lcom/osp/app/signin/UserValidateCheck;Landroid/content/Context;)V

    goto :goto_0

    .line 2320
    :cond_2
    invoke-static {}, Lcom/osp/device/b;->b()Lcom/osp/device/b;

    move-result-object v0

    invoke-virtual {v0}, Lcom/osp/device/b;->a()Z

    move-result v0

    if-eqz v0, :cond_3

    const-string v0, "USR_3174"

    iget-object v1, p0, Lcom/osp/app/signin/po;->b:Lcom/msc/c/f;

    invoke-virtual {v1}, Lcom/msc/c/f;->a()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_4

    const-string v0, "DUPLICATED_ID"

    iget-object v1, p0, Lcom/osp/app/signin/po;->b:Lcom/msc/c/f;

    invoke-virtual {v1}, Lcom/msc/c/f;->a()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_4

    :cond_3
    const-string v0, "USR_3113"

    iget-object v1, p0, Lcom/osp/app/signin/po;->b:Lcom/msc/c/f;

    invoke-virtual {v1}, Lcom/msc/c/f;->a()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_5

    .line 2323
    :cond_4
    iget-object v0, p0, Lcom/osp/app/signin/po;->c:Lcom/osp/app/signin/UserValidateCheck;

    iget-object v1, p0, Lcom/osp/app/signin/po;->c:Lcom/osp/app/signin/UserValidateCheck;

    invoke-static {v1}, Lcom/osp/app/util/ad;->h(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/osp/app/signin/UserValidateCheck;->b(Lcom/osp/app/signin/UserValidateCheck;Ljava/lang/String;)V

    .line 2324
    iget-object v0, p0, Lcom/osp/app/signin/po;->c:Lcom/osp/app/signin/UserValidateCheck;

    invoke-virtual {v0}, Lcom/osp/app/signin/UserValidateCheck;->finish()V

    goto :goto_0

    .line 2326
    :cond_5
    iget-object v0, p0, Lcom/osp/app/signin/po;->b:Lcom/msc/c/f;

    invoke-virtual {v0}, Lcom/msc/c/f;->c()Z

    move-result v0

    if-eqz v0, :cond_6

    .line 2328
    iget-object v0, p0, Lcom/osp/app/signin/po;->c:Lcom/osp/app/signin/UserValidateCheck;

    const/4 v1, 0x3

    iget-object v2, p0, Lcom/osp/app/signin/po;->c:Lcom/osp/app/signin/UserValidateCheck;

    invoke-static {v2}, Lcom/osp/app/signin/UserValidateCheck;->f(Lcom/osp/app/signin/UserValidateCheck;)Landroid/content/Intent;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/osp/app/signin/UserValidateCheck;->a(ILandroid/content/Intent;)V

    .line 2330
    :cond_6
    invoke-virtual {p0, v6}, Lcom/osp/app/signin/po;->a(Z)V

    .line 2331
    iget-object v0, p0, Lcom/osp/app/signin/po;->c:Lcom/osp/app/signin/UserValidateCheck;

    invoke-static {v0}, Lcom/osp/app/signin/UserValidateCheck;->y(Lcom/osp/app/signin/UserValidateCheck;)Z

    move-result v0

    if-eqz v0, :cond_7

    .line 2333
    iget-object v0, p0, Lcom/osp/app/signin/po;->c:Lcom/osp/app/signin/UserValidateCheck;

    invoke-virtual {v0}, Lcom/osp/app/signin/UserValidateCheck;->finish()V

    goto :goto_0

    .line 2336
    :cond_7
    iget-object v0, p0, Lcom/osp/app/signin/po;->c:Lcom/osp/app/signin/UserValidateCheck;

    invoke-static {v0}, Lcom/osp/app/signin/UserValidateCheck;->k(Lcom/osp/app/signin/UserValidateCheck;)Landroid/app/AlertDialog;

    move-result-object v0

    if-eqz v0, :cond_8

    iget-object v0, p0, Lcom/osp/app/signin/po;->c:Lcom/osp/app/signin/UserValidateCheck;

    invoke-static {v0}, Lcom/osp/app/signin/UserValidateCheck;->k(Lcom/osp/app/signin/UserValidateCheck;)Landroid/app/AlertDialog;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/AlertDialog;->isShowing()Z

    move-result v0

    if-nez v0, :cond_8

    .line 2338
    iget-object v0, p0, Lcom/osp/app/signin/po;->c:Lcom/osp/app/signin/UserValidateCheck;

    invoke-static {v0}, Lcom/osp/app/signin/UserValidateCheck;->k(Lcom/osp/app/signin/UserValidateCheck;)Landroid/app/AlertDialog;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/AlertDialog;->show()V

    goto/16 :goto_0

    .line 2341
    :cond_8
    iget-object v0, p0, Lcom/osp/app/signin/po;->c:Lcom/osp/app/signin/UserValidateCheck;

    invoke-static {v0}, Lcom/osp/app/signin/UserValidateCheck;->t(Lcom/osp/app/signin/UserValidateCheck;)V

    goto/16 :goto_0

    .line 2346
    :cond_9
    const-string v0, "Success"

    iget-object v1, p0, Lcom/osp/app/signin/po;->d:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_a

    .line 2348
    iget-object v0, p0, Lcom/osp/app/signin/po;->c:Lcom/osp/app/signin/UserValidateCheck;

    iget-boolean v1, p0, Lcom/osp/app/signin/po;->q:Z

    invoke-static {v0, v1}, Lcom/osp/app/signin/UserValidateCheck;->a(Lcom/osp/app/signin/UserValidateCheck;Z)V

    goto/16 :goto_0

    .line 2349
    :cond_a
    const-string v0, "require more process"

    iget-object v1, p0, Lcom/osp/app/signin/po;->d:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_b

    .line 2351
    iget-object v0, p0, Lcom/osp/app/signin/po;->c:Lcom/osp/app/signin/UserValidateCheck;

    iget v1, p0, Lcom/osp/app/signin/po;->o:I

    iget-object v2, p0, Lcom/osp/app/signin/po;->c:Lcom/osp/app/signin/UserValidateCheck;

    invoke-static {v2}, Lcom/msc/c/e;->h(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v2

    iget-object v3, p0, Lcom/osp/app/signin/po;->c:Lcom/osp/app/signin/UserValidateCheck;

    invoke-static {v3}, Lcom/osp/app/signin/UserValidateCheck;->p(Lcom/osp/app/signin/UserValidateCheck;)Ljava/lang/String;

    move-result-object v3

    iget-object v4, p0, Lcom/osp/app/signin/po;->c:Lcom/osp/app/signin/UserValidateCheck;

    invoke-static {v4}, Lcom/osp/app/signin/UserValidateCheck;->v(Lcom/osp/app/signin/UserValidateCheck;)Ljava/lang/String;

    move-result-object v4

    const/4 v5, 0x1

    iget-object v7, p0, Lcom/osp/app/signin/po;->p:Ljava/lang/String;

    invoke-static/range {v0 .. v7}, Lcom/osp/app/util/ad;->a(Landroid/content/Context;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;ZZLjava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    .line 2353
    iget-object v1, p0, Lcom/osp/app/signin/po;->c:Lcom/osp/app/signin/UserValidateCheck;

    const/16 v2, 0xd2

    invoke-virtual {v1, v0, v2}, Lcom/osp/app/signin/UserValidateCheck;->startActivityForResult(Landroid/content/Intent;I)V

    goto/16 :goto_0

    .line 2365
    :cond_b
    iget-object v0, p0, Lcom/osp/app/signin/po;->c:Lcom/osp/app/signin/UserValidateCheck;

    invoke-static {v0}, Lcom/osp/app/signin/UserValidateCheck;->z(Lcom/osp/app/signin/UserValidateCheck;)Z

    move-result v0

    if-eqz v0, :cond_c

    const-string v0, "IdDeleted"

    iget-object v1, p0, Lcom/osp/app/signin/po;->d:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_c

    .line 2367
    iget-object v0, p0, Lcom/osp/app/signin/po;->c:Lcom/osp/app/signin/UserValidateCheck;

    const/4 v1, -0x1

    iget-object v2, p0, Lcom/osp/app/signin/po;->c:Lcom/osp/app/signin/UserValidateCheck;

    invoke-static {v2}, Lcom/osp/app/signin/UserValidateCheck;->f(Lcom/osp/app/signin/UserValidateCheck;)Landroid/content/Intent;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/osp/app/signin/UserValidateCheck;->a(ILandroid/content/Intent;)V

    .line 2368
    iget-object v0, p0, Lcom/osp/app/signin/po;->c:Lcom/osp/app/signin/UserValidateCheck;

    invoke-virtual {v0}, Lcom/osp/app/signin/UserValidateCheck;->finish()V

    goto/16 :goto_0

    .line 2371
    :cond_c
    invoke-virtual {p0, v6}, Lcom/osp/app/signin/po;->a(Z)V

    .line 2372
    iget-object v0, p0, Lcom/osp/app/signin/po;->c:Lcom/osp/app/signin/UserValidateCheck;

    invoke-static {v0}, Lcom/osp/app/signin/UserValidateCheck;->k(Lcom/osp/app/signin/UserValidateCheck;)Landroid/app/AlertDialog;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/osp/app/signin/po;->c:Lcom/osp/app/signin/UserValidateCheck;

    invoke-static {v0}, Lcom/osp/app/signin/UserValidateCheck;->k(Lcom/osp/app/signin/UserValidateCheck;)Landroid/app/AlertDialog;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/AlertDialog;->isShowing()Z

    move-result v0

    if-nez v0, :cond_0

    .line 2374
    iget-object v0, p0, Lcom/osp/app/signin/po;->c:Lcom/osp/app/signin/UserValidateCheck;

    invoke-static {v0}, Lcom/osp/app/signin/UserValidateCheck;->k(Lcom/osp/app/signin/UserValidateCheck;)Landroid/app/AlertDialog;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/AlertDialog;->show()V

    goto/16 :goto_0
.end method

.method public final b(Lcom/msc/b/c;)V
    .locals 9

    .prologue
    const/4 v8, 0x5

    const/4 v7, 0x0

    const/4 v6, 0x1

    .line 2694
    invoke-super {p0, p1}, Lcom/msc/c/b;->b(Lcom/msc/b/c;)V

    .line 2696
    if-nez p1, :cond_1

    .line 2992
    :cond_0
    :goto_0
    return-void

    .line 2701
    :cond_1
    invoke-virtual {p1}, Lcom/msc/b/c;->a()J

    move-result-wide v0

    .line 2702
    invoke-virtual {p1}, Lcom/msc/b/c;->e()Ljava/lang/String;

    move-result-object v2

    .line 2712
    iget-wide v4, p0, Lcom/osp/app/signin/po;->h:J

    cmp-long v3, v0, v4

    if-nez v3, :cond_5

    .line 2714
    if-eqz v2, :cond_0

    .line 2716
    iget-object v0, p0, Lcom/osp/app/signin/po;->c:Lcom/osp/app/signin/UserValidateCheck;

    invoke-virtual {v0, v6}, Lcom/osp/app/signin/UserValidateCheck;->b(I)V

    .line 2717
    iget-object v0, p0, Lcom/osp/app/signin/po;->b:Lcom/msc/c/f;

    invoke-virtual {v0}, Lcom/msc/c/f;->a()Ljava/lang/String;

    move-result-object v0

    const-string v1, "AUT_1805"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2721
    iget-object v0, p0, Lcom/osp/app/signin/po;->c:Lcom/osp/app/signin/UserValidateCheck;

    invoke-static {v0}, Lcom/msc/c/c;->e(Landroid/content/Context;)V

    .line 2725
    iget-object v0, p0, Lcom/osp/app/signin/po;->c:Lcom/osp/app/signin/UserValidateCheck;

    invoke-static {v0}, Lcom/msc/c/e;->i(Landroid/content/Context;)V

    .line 2749
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v1, p0, Lcom/osp/app/signin/po;->c:Lcom/osp/app/signin/UserValidateCheck;

    invoke-static {v1}, Lcom/osp/app/signin/UserValidateCheck;->s(Lcom/osp/app/signin/UserValidateCheck;)Landroid/widget/TextView;

    move-result-object v1

    invoke-virtual {v1}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v1

    invoke-interface {v1}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "/"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/osp/app/signin/po;->c:Lcom/osp/app/signin/UserValidateCheck;

    invoke-static {v1}, Lcom/osp/app/signin/UserValidateCheck;->p(Lcom/osp/app/signin/UserValidateCheck;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 2751
    iget-object v1, p0, Lcom/osp/app/signin/po;->c:Lcom/osp/app/signin/UserValidateCheck;

    invoke-static {v1}, Lcom/osp/app/signin/UserValidateCheck;->B(Lcom/osp/app/signin/UserValidateCheck;)Landroid/content/SharedPreferences;

    move-result-object v1

    if-nez v1, :cond_2

    .line 2753
    iget-object v1, p0, Lcom/osp/app/signin/po;->c:Lcom/osp/app/signin/UserValidateCheck;

    iget-object v2, p0, Lcom/osp/app/signin/po;->c:Lcom/osp/app/signin/UserValidateCheck;

    const-string v3, "VERIFY_COUNT"

    invoke-virtual {v2, v3, v7}, Lcom/osp/app/signin/UserValidateCheck;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/osp/app/signin/UserValidateCheck;->a(Lcom/osp/app/signin/UserValidateCheck;Landroid/content/SharedPreferences;)Landroid/content/SharedPreferences;

    .line 2754
    iget-object v1, p0, Lcom/osp/app/signin/po;->c:Lcom/osp/app/signin/UserValidateCheck;

    invoke-static {v1}, Lcom/osp/app/signin/UserValidateCheck;->B(Lcom/osp/app/signin/UserValidateCheck;)Landroid/content/SharedPreferences;

    move-result-object v1

    invoke-interface {v1}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v1

    .line 2755
    invoke-interface {v1, v0, v7}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    .line 2756
    invoke-interface {v1}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 2758
    :cond_2
    iget-object v1, p0, Lcom/osp/app/signin/po;->c:Lcom/osp/app/signin/UserValidateCheck;

    invoke-static {v1}, Lcom/osp/app/signin/UserValidateCheck;->B(Lcom/osp/app/signin/UserValidateCheck;)Landroid/content/SharedPreferences;

    move-result-object v1

    invoke-interface {v1, v0, v7}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v1

    .line 2759
    add-int/lit8 v1, v1, 0x1

    .line 2760
    if-lt v1, v8, :cond_4

    .line 2762
    iget-object v1, p0, Lcom/osp/app/signin/po;->c:Lcom/osp/app/signin/UserValidateCheck;

    invoke-static {v1}, Lcom/osp/app/signin/UserValidateCheck;->B(Lcom/osp/app/signin/UserValidateCheck;)Landroid/content/SharedPreferences;

    move-result-object v1

    invoke-interface {v1}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v1

    invoke-interface {v1, v0}, Landroid/content/SharedPreferences$Editor;->remove(Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    .line 2763
    iget-object v0, p0, Lcom/osp/app/signin/po;->c:Lcom/osp/app/signin/UserValidateCheck;

    invoke-static {v0}, Lcom/osp/app/signin/UserValidateCheck;->B(Lcom/osp/app/signin/UserValidateCheck;)Landroid/content/SharedPreferences;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 2764
    iget-object v0, p0, Lcom/osp/app/signin/po;->c:Lcom/osp/app/signin/UserValidateCheck;

    invoke-static {v0}, Lcom/osp/app/signin/UserValidateCheck;->f(Lcom/osp/app/signin/UserValidateCheck;)Landroid/content/Intent;

    move-result-object v0

    const-string v1, "passwordBlock"

    invoke-virtual {v0, v1, v6}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 2766
    iget-object v0, p0, Lcom/osp/app/signin/po;->c:Lcom/osp/app/signin/UserValidateCheck;

    invoke-static {v0}, Lcom/osp/app/signin/UserValidateCheck;->C(Lcom/osp/app/signin/UserValidateCheck;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 2768
    iget-object v0, p0, Lcom/osp/app/signin/po;->c:Lcom/osp/app/signin/UserValidateCheck;

    invoke-static {v0}, Lcom/osp/app/signin/UserValidateCheck;->f(Lcom/osp/app/signin/UserValidateCheck;)Landroid/content/Intent;

    move-result-object v0

    const-string v1, "error_code"

    const-string v2, "SAC_0202"

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 2769
    iget-object v0, p0, Lcom/osp/app/signin/po;->c:Lcom/osp/app/signin/UserValidateCheck;

    invoke-static {v0}, Lcom/osp/app/signin/UserValidateCheck;->f(Lcom/osp/app/signin/UserValidateCheck;)Landroid/content/Intent;

    move-result-object v0

    const-string v1, "error_message"

    const-string v2, "Password blocked"

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 2772
    :cond_3
    iget-object v0, p0, Lcom/osp/app/signin/po;->c:Lcom/osp/app/signin/UserValidateCheck;

    iget-object v1, p0, Lcom/osp/app/signin/po;->c:Lcom/osp/app/signin/UserValidateCheck;

    invoke-static {v1}, Lcom/osp/app/signin/UserValidateCheck;->f(Lcom/osp/app/signin/UserValidateCheck;)Landroid/content/Intent;

    move-result-object v1

    invoke-virtual {v0, v6, v1}, Lcom/osp/app/signin/UserValidateCheck;->a(ILandroid/content/Intent;)V

    .line 2773
    invoke-virtual {p0}, Lcom/osp/app/signin/po;->f()V

    .line 2774
    iget-object v0, p0, Lcom/osp/app/signin/po;->c:Lcom/osp/app/signin/UserValidateCheck;

    invoke-virtual {v0}, Lcom/osp/app/signin/UserValidateCheck;->finish()V

    goto/16 :goto_0

    .line 2777
    :cond_4
    iget-object v2, p0, Lcom/osp/app/signin/po;->c:Lcom/osp/app/signin/UserValidateCheck;

    invoke-static {v2}, Lcom/osp/app/signin/UserValidateCheck;->B(Lcom/osp/app/signin/UserValidateCheck;)Landroid/content/SharedPreferences;

    move-result-object v2

    invoke-interface {v2}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v2

    .line 2778
    invoke-interface {v2, v0, v1}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    .line 2779
    invoke-interface {v2}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 2780
    iget-object v0, p0, Lcom/osp/app/signin/po;->b:Lcom/msc/c/f;

    const-string v1, "AUT_1805"

    invoke-virtual {v0, v1}, Lcom/msc/c/f;->a(Ljava/lang/String;)V

    .line 2781
    iget-object v0, p0, Lcom/osp/app/signin/po;->b:Lcom/msc/c/f;

    const-string v1, "fail"

    invoke-virtual {v0, v1}, Lcom/msc/c/f;->b(Ljava/lang/String;)V

    goto/16 :goto_0

    .line 2786
    :cond_5
    iget-wide v4, p0, Lcom/osp/app/signin/po;->i:J

    cmp-long v3, v0, v4

    if-nez v3, :cond_9

    .line 2788
    if-eqz v2, :cond_0

    .line 2790
    iget-object v0, p0, Lcom/osp/app/signin/po;->c:Lcom/osp/app/signin/UserValidateCheck;

    invoke-virtual {v0, v6}, Lcom/osp/app/signin/UserValidateCheck;->b(I)V

    .line 2791
    iget-object v0, p0, Lcom/osp/app/signin/po;->b:Lcom/msc/c/f;

    invoke-virtual {v0}, Lcom/msc/c/f;->a()Ljava/lang/String;

    move-result-object v0

    const-string v1, "AUT_1805"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2795
    iget-object v0, p0, Lcom/osp/app/signin/po;->c:Lcom/osp/app/signin/UserValidateCheck;

    invoke-static {v0}, Lcom/msc/c/c;->e(Landroid/content/Context;)V

    .line 2799
    iget-object v0, p0, Lcom/osp/app/signin/po;->c:Lcom/osp/app/signin/UserValidateCheck;

    invoke-static {v0}, Lcom/msc/c/e;->i(Landroid/content/Context;)V

    .line 2823
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v1, p0, Lcom/osp/app/signin/po;->c:Lcom/osp/app/signin/UserValidateCheck;

    invoke-static {v1}, Lcom/osp/app/signin/UserValidateCheck;->s(Lcom/osp/app/signin/UserValidateCheck;)Landroid/widget/TextView;

    move-result-object v1

    invoke-virtual {v1}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v1

    invoke-interface {v1}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "/"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/osp/app/signin/po;->c:Lcom/osp/app/signin/UserValidateCheck;

    invoke-static {v1}, Lcom/osp/app/signin/UserValidateCheck;->p(Lcom/osp/app/signin/UserValidateCheck;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 2825
    iget-object v1, p0, Lcom/osp/app/signin/po;->c:Lcom/osp/app/signin/UserValidateCheck;

    invoke-static {v1}, Lcom/osp/app/signin/UserValidateCheck;->B(Lcom/osp/app/signin/UserValidateCheck;)Landroid/content/SharedPreferences;

    move-result-object v1

    if-nez v1, :cond_6

    .line 2827
    iget-object v1, p0, Lcom/osp/app/signin/po;->c:Lcom/osp/app/signin/UserValidateCheck;

    iget-object v2, p0, Lcom/osp/app/signin/po;->c:Lcom/osp/app/signin/UserValidateCheck;

    const-string v3, "VERIFY_COUNT"

    invoke-virtual {v2, v3, v7}, Lcom/osp/app/signin/UserValidateCheck;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/osp/app/signin/UserValidateCheck;->a(Lcom/osp/app/signin/UserValidateCheck;Landroid/content/SharedPreferences;)Landroid/content/SharedPreferences;

    .line 2828
    iget-object v1, p0, Lcom/osp/app/signin/po;->c:Lcom/osp/app/signin/UserValidateCheck;

    invoke-static {v1}, Lcom/osp/app/signin/UserValidateCheck;->B(Lcom/osp/app/signin/UserValidateCheck;)Landroid/content/SharedPreferences;

    move-result-object v1

    invoke-interface {v1}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v1

    .line 2829
    invoke-interface {v1, v0, v7}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    .line 2830
    invoke-interface {v1}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 2832
    :cond_6
    iget-object v1, p0, Lcom/osp/app/signin/po;->c:Lcom/osp/app/signin/UserValidateCheck;

    invoke-static {v1}, Lcom/osp/app/signin/UserValidateCheck;->B(Lcom/osp/app/signin/UserValidateCheck;)Landroid/content/SharedPreferences;

    move-result-object v1

    invoke-interface {v1, v0, v7}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v1

    .line 2833
    add-int/lit8 v1, v1, 0x1

    .line 2834
    if-lt v1, v8, :cond_8

    .line 2836
    iget-object v1, p0, Lcom/osp/app/signin/po;->c:Lcom/osp/app/signin/UserValidateCheck;

    invoke-static {v1}, Lcom/osp/app/signin/UserValidateCheck;->B(Lcom/osp/app/signin/UserValidateCheck;)Landroid/content/SharedPreferences;

    move-result-object v1

    invoke-interface {v1}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v1

    invoke-interface {v1, v0}, Landroid/content/SharedPreferences$Editor;->remove(Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    .line 2837
    iget-object v0, p0, Lcom/osp/app/signin/po;->c:Lcom/osp/app/signin/UserValidateCheck;

    invoke-static {v0}, Lcom/osp/app/signin/UserValidateCheck;->B(Lcom/osp/app/signin/UserValidateCheck;)Landroid/content/SharedPreferences;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 2838
    iget-object v0, p0, Lcom/osp/app/signin/po;->c:Lcom/osp/app/signin/UserValidateCheck;

    invoke-static {v0}, Lcom/osp/app/signin/UserValidateCheck;->f(Lcom/osp/app/signin/UserValidateCheck;)Landroid/content/Intent;

    move-result-object v0

    const-string v1, "passwordBlock"

    invoke-virtual {v0, v1, v6}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 2840
    iget-object v0, p0, Lcom/osp/app/signin/po;->c:Lcom/osp/app/signin/UserValidateCheck;

    invoke-static {v0}, Lcom/osp/app/signin/UserValidateCheck;->C(Lcom/osp/app/signin/UserValidateCheck;)Z

    move-result v0

    if-eqz v0, :cond_7

    .line 2842
    iget-object v0, p0, Lcom/osp/app/signin/po;->c:Lcom/osp/app/signin/UserValidateCheck;

    invoke-static {v0}, Lcom/osp/app/signin/UserValidateCheck;->f(Lcom/osp/app/signin/UserValidateCheck;)Landroid/content/Intent;

    move-result-object v0

    const-string v1, "error_code"

    const-string v2, "SAC_0202"

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 2843
    iget-object v0, p0, Lcom/osp/app/signin/po;->c:Lcom/osp/app/signin/UserValidateCheck;

    invoke-static {v0}, Lcom/osp/app/signin/UserValidateCheck;->f(Lcom/osp/app/signin/UserValidateCheck;)Landroid/content/Intent;

    move-result-object v0

    const-string v1, "error_message"

    const-string v2, "Password blocked"

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 2846
    :cond_7
    iget-object v0, p0, Lcom/osp/app/signin/po;->c:Lcom/osp/app/signin/UserValidateCheck;

    iget-object v1, p0, Lcom/osp/app/signin/po;->c:Lcom/osp/app/signin/UserValidateCheck;

    invoke-static {v1}, Lcom/osp/app/signin/UserValidateCheck;->f(Lcom/osp/app/signin/UserValidateCheck;)Landroid/content/Intent;

    move-result-object v1

    invoke-virtual {v0, v6, v1}, Lcom/osp/app/signin/UserValidateCheck;->a(ILandroid/content/Intent;)V

    .line 2847
    invoke-virtual {p0}, Lcom/osp/app/signin/po;->f()V

    .line 2848
    iget-object v0, p0, Lcom/osp/app/signin/po;->c:Lcom/osp/app/signin/UserValidateCheck;

    invoke-virtual {v0}, Lcom/osp/app/signin/UserValidateCheck;->finish()V

    goto/16 :goto_0

    .line 2851
    :cond_8
    iget-object v2, p0, Lcom/osp/app/signin/po;->c:Lcom/osp/app/signin/UserValidateCheck;

    invoke-static {v2}, Lcom/osp/app/signin/UserValidateCheck;->B(Lcom/osp/app/signin/UserValidateCheck;)Landroid/content/SharedPreferences;

    move-result-object v2

    invoke-interface {v2}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v2

    .line 2852
    invoke-interface {v2, v0, v1}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    .line 2853
    invoke-interface {v2}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 2854
    iget-object v0, p0, Lcom/osp/app/signin/po;->b:Lcom/msc/c/f;

    const-string v1, "AUT_1805"

    invoke-virtual {v0, v1}, Lcom/msc/c/f;->a(Ljava/lang/String;)V

    .line 2855
    iget-object v0, p0, Lcom/osp/app/signin/po;->b:Lcom/msc/c/f;

    const-string v1, "fail"

    invoke-virtual {v0, v1}, Lcom/msc/c/f;->b(Ljava/lang/String;)V

    goto/16 :goto_0

    .line 2860
    :cond_9
    iget-wide v4, p0, Lcom/osp/app/signin/po;->j:J

    cmp-long v3, v0, v4

    if-nez v3, :cond_b

    .line 2862
    if-eqz v2, :cond_0

    .line 2864
    iget-object v0, p0, Lcom/osp/app/signin/po;->c:Lcom/osp/app/signin/UserValidateCheck;

    invoke-virtual {v0, v6}, Lcom/osp/app/signin/UserValidateCheck;->b(I)V

    .line 2865
    iget-object v0, p0, Lcom/osp/app/signin/po;->b:Lcom/msc/c/f;

    invoke-virtual {v0}, Lcom/msc/c/f;->a()Ljava/lang/String;

    move-result-object v0

    const-string v1, "AUT_1804"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_a

    .line 2869
    const-string v0, "require re-SignIn"

    iput-object v0, p0, Lcom/osp/app/signin/po;->d:Ljava/lang/String;

    goto/16 :goto_0

    .line 2870
    :cond_a
    iget-object v0, p0, Lcom/osp/app/signin/po;->b:Lcom/msc/c/f;

    invoke-virtual {v0}, Lcom/msc/c/f;->a()Ljava/lang/String;

    move-result-object v0

    const-string v1, "AUT_1806"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 2875
    iget-object v0, p0, Lcom/osp/app/signin/po;->b:Lcom/msc/c/f;

    invoke-virtual {v0}, Lcom/msc/c/f;->a()Ljava/lang/String;

    move-result-object v0

    const-string v1, "AUT_1091"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    goto/16 :goto_0

    .line 2886
    :cond_b
    iget-wide v4, p0, Lcom/osp/app/signin/po;->n:J

    cmp-long v0, v0, v4

    if-nez v0, :cond_0

    .line 2888
    if-eqz v2, :cond_0

    .line 2890
    iget-object v0, p0, Lcom/osp/app/signin/po;->c:Lcom/osp/app/signin/UserValidateCheck;

    invoke-virtual {v0, v6}, Lcom/osp/app/signin/UserValidateCheck;->b(I)V

    .line 2891
    iget-object v0, p0, Lcom/osp/app/signin/po;->b:Lcom/msc/c/f;

    invoke-virtual {v0}, Lcom/msc/c/f;->a()Ljava/lang/String;

    move-result-object v0

    .line 2892
    const-string v1, "USR_1115"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_c

    const-string v1, "USR_1312"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_c

    const-string v1, "USR_1513"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_10

    .line 2896
    :cond_c
    iget-object v1, p0, Lcom/osp/app/signin/po;->c:Lcom/osp/app/signin/UserValidateCheck;

    invoke-static {v1}, Lcom/msc/c/c;->e(Landroid/content/Context;)V

    .line 2900
    iget-object v1, p0, Lcom/osp/app/signin/po;->c:Lcom/osp/app/signin/UserValidateCheck;

    invoke-static {v1}, Lcom/msc/c/e;->i(Landroid/content/Context;)V

    .line 2927
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v2, p0, Lcom/osp/app/signin/po;->c:Lcom/osp/app/signin/UserValidateCheck;

    invoke-static {v2}, Lcom/osp/app/signin/UserValidateCheck;->s(Lcom/osp/app/signin/UserValidateCheck;)Landroid/widget/TextView;

    move-result-object v2

    invoke-virtual {v2}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v2

    invoke-interface {v2}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "/"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/osp/app/signin/po;->c:Lcom/osp/app/signin/UserValidateCheck;

    invoke-static {v2}, Lcom/osp/app/signin/UserValidateCheck;->p(Lcom/osp/app/signin/UserValidateCheck;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 2929
    iget-object v2, p0, Lcom/osp/app/signin/po;->c:Lcom/osp/app/signin/UserValidateCheck;

    invoke-static {v2}, Lcom/osp/app/signin/UserValidateCheck;->B(Lcom/osp/app/signin/UserValidateCheck;)Landroid/content/SharedPreferences;

    move-result-object v2

    if-nez v2, :cond_d

    .line 2931
    iget-object v2, p0, Lcom/osp/app/signin/po;->c:Lcom/osp/app/signin/UserValidateCheck;

    iget-object v3, p0, Lcom/osp/app/signin/po;->c:Lcom/osp/app/signin/UserValidateCheck;

    const-string v4, "VERIFY_COUNT"

    invoke-virtual {v3, v4, v7}, Lcom/osp/app/signin/UserValidateCheck;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/osp/app/signin/UserValidateCheck;->a(Lcom/osp/app/signin/UserValidateCheck;Landroid/content/SharedPreferences;)Landroid/content/SharedPreferences;

    .line 2932
    iget-object v2, p0, Lcom/osp/app/signin/po;->c:Lcom/osp/app/signin/UserValidateCheck;

    invoke-static {v2}, Lcom/osp/app/signin/UserValidateCheck;->B(Lcom/osp/app/signin/UserValidateCheck;)Landroid/content/SharedPreferences;

    move-result-object v2

    invoke-interface {v2}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v2

    .line 2933
    invoke-interface {v2, v1, v7}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    .line 2934
    invoke-interface {v2}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 2936
    :cond_d
    iget-object v2, p0, Lcom/osp/app/signin/po;->c:Lcom/osp/app/signin/UserValidateCheck;

    invoke-static {v2}, Lcom/osp/app/signin/UserValidateCheck;->B(Lcom/osp/app/signin/UserValidateCheck;)Landroid/content/SharedPreferences;

    move-result-object v2

    invoke-interface {v2, v1, v7}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v2

    .line 2937
    add-int/lit8 v2, v2, 0x1

    .line 2938
    if-lt v2, v8, :cond_f

    .line 2940
    iget-object v0, p0, Lcom/osp/app/signin/po;->c:Lcom/osp/app/signin/UserValidateCheck;

    invoke-static {v0}, Lcom/osp/app/signin/UserValidateCheck;->B(Lcom/osp/app/signin/UserValidateCheck;)Landroid/content/SharedPreferences;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    invoke-interface {v0, v1}, Landroid/content/SharedPreferences$Editor;->remove(Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    .line 2941
    iget-object v0, p0, Lcom/osp/app/signin/po;->c:Lcom/osp/app/signin/UserValidateCheck;

    invoke-static {v0}, Lcom/osp/app/signin/UserValidateCheck;->B(Lcom/osp/app/signin/UserValidateCheck;)Landroid/content/SharedPreferences;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 2942
    iget-object v0, p0, Lcom/osp/app/signin/po;->c:Lcom/osp/app/signin/UserValidateCheck;

    invoke-static {v0}, Lcom/osp/app/signin/UserValidateCheck;->f(Lcom/osp/app/signin/UserValidateCheck;)Landroid/content/Intent;

    move-result-object v0

    const-string v1, "passwordBlock"

    invoke-virtual {v0, v1, v6}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 2944
    iget-object v0, p0, Lcom/osp/app/signin/po;->c:Lcom/osp/app/signin/UserValidateCheck;

    invoke-static {v0}, Lcom/osp/app/signin/UserValidateCheck;->C(Lcom/osp/app/signin/UserValidateCheck;)Z

    move-result v0

    if-eqz v0, :cond_e

    .line 2946
    iget-object v0, p0, Lcom/osp/app/signin/po;->c:Lcom/osp/app/signin/UserValidateCheck;

    invoke-static {v0}, Lcom/osp/app/signin/UserValidateCheck;->f(Lcom/osp/app/signin/UserValidateCheck;)Landroid/content/Intent;

    move-result-object v0

    const-string v1, "error_code"

    const-string v2, "SAC_0202"

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 2947
    iget-object v0, p0, Lcom/osp/app/signin/po;->c:Lcom/osp/app/signin/UserValidateCheck;

    invoke-static {v0}, Lcom/osp/app/signin/UserValidateCheck;->f(Lcom/osp/app/signin/UserValidateCheck;)Landroid/content/Intent;

    move-result-object v0

    const-string v1, "error_message"

    const-string v2, "Password blocked"

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 2950
    :cond_e
    iget-object v0, p0, Lcom/osp/app/signin/po;->c:Lcom/osp/app/signin/UserValidateCheck;

    iget-object v1, p0, Lcom/osp/app/signin/po;->c:Lcom/osp/app/signin/UserValidateCheck;

    invoke-static {v1}, Lcom/osp/app/signin/UserValidateCheck;->f(Lcom/osp/app/signin/UserValidateCheck;)Landroid/content/Intent;

    move-result-object v1

    invoke-virtual {v0, v6, v1}, Lcom/osp/app/signin/UserValidateCheck;->a(ILandroid/content/Intent;)V

    .line 2951
    invoke-virtual {p0}, Lcom/osp/app/signin/po;->f()V

    .line 2952
    iget-object v0, p0, Lcom/osp/app/signin/po;->c:Lcom/osp/app/signin/UserValidateCheck;

    invoke-virtual {v0}, Lcom/osp/app/signin/UserValidateCheck;->finish()V

    goto/16 :goto_0

    .line 2955
    :cond_f
    iget-object v3, p0, Lcom/osp/app/signin/po;->c:Lcom/osp/app/signin/UserValidateCheck;

    invoke-static {v3}, Lcom/osp/app/signin/UserValidateCheck;->B(Lcom/osp/app/signin/UserValidateCheck;)Landroid/content/SharedPreferences;

    move-result-object v3

    invoke-interface {v3}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v3

    .line 2956
    invoke-interface {v3, v1, v2}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    .line 2957
    invoke-interface {v3}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 2958
    iget-object v1, p0, Lcom/osp/app/signin/po;->b:Lcom/msc/c/f;

    invoke-virtual {v1, v0}, Lcom/msc/c/f;->a(Ljava/lang/String;)V

    .line 2959
    iget-object v0, p0, Lcom/osp/app/signin/po;->b:Lcom/msc/c/f;

    const-string v1, "fail"

    invoke-virtual {v0, v1}, Lcom/msc/c/f;->b(Ljava/lang/String;)V

    goto/16 :goto_0

    .line 2978
    :cond_10
    iget-object v1, p0, Lcom/osp/app/signin/po;->c:Lcom/osp/app/signin/UserValidateCheck;

    invoke-static {v1}, Lcom/osp/app/signin/UserValidateCheck;->z(Lcom/osp/app/signin/UserValidateCheck;)Z

    move-result v1

    if-eqz v1, :cond_0

    const-string v1, "USR_3113"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2980
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/osp/app/signin/po;->b:Lcom/msc/c/f;

    .line 2981
    const-string v0, "IdDeleted"

    iput-object v0, p0, Lcom/osp/app/signin/po;->d:Ljava/lang/String;

    goto/16 :goto_0
.end method

.method protected final d()V
    .locals 2

    .prologue
    .line 2184
    invoke-super {p0}, Lcom/msc/c/b;->d()V

    .line 2185
    iget-object v0, p0, Lcom/osp/app/signin/po;->c:Lcom/osp/app/signin/UserValidateCheck;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/osp/app/signin/UserValidateCheck;->b(I)V

    .line 2186
    iget-object v0, p0, Lcom/osp/app/signin/po;->c:Lcom/osp/app/signin/UserValidateCheck;

    invoke-virtual {v0}, Lcom/osp/app/signin/UserValidateCheck;->finish()V

    .line 2187
    return-void
.end method

.method protected final synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 2100
    invoke-virtual {p0}, Lcom/osp/app/signin/po;->a()Ljava/lang/Boolean;

    move-result-object v0

    return-object v0
.end method

.method protected final synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 2100
    check-cast p1, Ljava/lang/Boolean;

    invoke-virtual {p0, p1}, Lcom/osp/app/signin/po;->a(Ljava/lang/Boolean;)V

    return-void
.end method
