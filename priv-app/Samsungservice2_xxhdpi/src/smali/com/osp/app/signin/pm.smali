.class final Lcom/osp/app/signin/pm;
.super Lcom/msc/c/b;
.source "UserValidateCheck.java"


# instance fields
.field final synthetic c:Lcom/osp/app/signin/UserValidateCheck;

.field private d:J


# direct methods
.method public constructor <init>(Lcom/osp/app/signin/UserValidateCheck;)V
    .locals 0

    .prologue
    .line 3518
    iput-object p1, p0, Lcom/osp/app/signin/pm;->c:Lcom/osp/app/signin/UserValidateCheck;

    .line 3519
    invoke-direct {p0, p1}, Lcom/msc/c/b;-><init>(Landroid/content/Context;)V

    .line 3523
    return-void
.end method


# virtual methods
.method protected final varargs a()Ljava/lang/Boolean;
    .locals 3

    .prologue
    .line 3538
    new-instance v0, Lcom/msc/a/f;

    invoke-direct {v0}, Lcom/msc/a/f;-><init>()V

    .line 3539
    iget-object v1, p0, Lcom/osp/app/signin/pm;->c:Lcom/osp/app/signin/UserValidateCheck;

    invoke-static {v1}, Lcom/osp/app/util/ad;->h(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/msc/a/f;->d(Ljava/lang/String;)V

    .line 3541
    invoke-static {}, Lcom/osp/device/b;->b()Lcom/osp/device/b;

    move-result-object v1

    invoke-virtual {v1}, Lcom/osp/device/b;->a()Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/osp/app/signin/pm;->c:Lcom/osp/app/signin/UserValidateCheck;

    invoke-static {v1}, Lcom/osp/app/util/ad;->h(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Landroid/telephony/PhoneNumberUtils;->isGlobalPhoneNumber(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 3543
    iget-object v1, p0, Lcom/osp/app/signin/pm;->c:Lcom/osp/app/signin/UserValidateCheck;

    invoke-static {v1}, Lcom/msc/c/e;->h(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v1

    .line 3544
    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 3546
    const-string v2, "Y"

    invoke-virtual {v0, v2}, Lcom/msc/a/f;->b(Ljava/lang/String;)V

    .line 3547
    invoke-virtual {v0, v1}, Lcom/msc/a/f;->c(Ljava/lang/String;)V

    .line 3552
    :cond_0
    invoke-static {}, Lcom/msc/c/g;->a()Lcom/msc/c/g;

    iget-object v1, p0, Lcom/osp/app/signin/pm;->c:Lcom/osp/app/signin/UserValidateCheck;

    invoke-static {v1, v0, p0}, Lcom/msc/c/g;->a(Landroid/content/Context;Lcom/msc/a/f;Lcom/msc/b/h;)J

    move-result-wide v0

    iput-wide v0, p0, Lcom/osp/app/signin/pm;->d:J

    iget-wide v0, p0, Lcom/osp/app/signin/pm;->d:J

    invoke-virtual {p0, v0, v1}, Lcom/osp/app/signin/pm;->a(J)V

    iget-wide v0, p0, Lcom/osp/app/signin/pm;->d:J

    const-string v2, "from_xml"

    invoke-virtual {p0, v0, v1, v2}, Lcom/osp/app/signin/pm;->a(JLjava/lang/String;)V

    iget-wide v0, p0, Lcom/osp/app/signin/pm;->d:J

    sget-object v2, Lcom/msc/b/g;->c:Lcom/msc/b/g;

    invoke-static {v0, v1, v2}, Lcom/msc/c/g;->a(JLcom/msc/b/g;)V

    .line 3554
    const/4 v0, 0x0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    return-object v0
.end method

.method public final declared-synchronized a(Lcom/msc/b/c;)V
    .locals 6

    .prologue
    .line 3589
    monitor-enter p0

    :try_start_0
    invoke-super {p0, p1}, Lcom/msc/c/b;->a(Lcom/msc/b/c;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 3591
    if-nez p1, :cond_1

    .line 3611
    :cond_0
    :goto_0
    monitor-exit p0

    return-void

    .line 3596
    :cond_1
    :try_start_1
    invoke-virtual {p1}, Lcom/msc/b/c;->a()J

    move-result-wide v0

    .line 3597
    invoke-virtual {p1}, Lcom/msc/b/c;->e()Ljava/lang/String;

    move-result-object v2

    .line 3599
    iget-wide v4, p0, Lcom/osp/app/signin/pm;->d:J
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    cmp-long v0, v0, v4

    if-nez v0, :cond_0

    .line 3603
    :try_start_2
    iget-object v0, p0, Lcom/osp/app/signin/pm;->c:Lcom/osp/app/signin/UserValidateCheck;

    invoke-static {}, Lcom/msc/c/h;->a()Lcom/msc/c/h;

    iget-object v1, p0, Lcom/osp/app/signin/pm;->c:Lcom/osp/app/signin/UserValidateCheck;

    invoke-static {v1, v2}, Lcom/msc/c/h;->b(Landroid/content/Context;Ljava/lang/String;)Lcom/msc/a/g;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/osp/app/signin/UserValidateCheck;->a(Lcom/osp/app/signin/UserValidateCheck;Lcom/msc/a/g;)Lcom/msc/a/g;
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_0
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0

    .line 3605
    :catch_0
    move-exception v0

    .line 3607
    :try_start_3
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    .line 3608
    new-instance v1, Lcom/msc/c/f;

    invoke-direct {v1, v0}, Lcom/msc/c/f;-><init>(Ljava/lang/Exception;)V

    iput-object v1, p0, Lcom/osp/app/signin/pm;->b:Lcom/msc/c/f;
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    goto :goto_0

    .line 3589
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method protected final a(Ljava/lang/Boolean;)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 3559
    invoke-super {p0}, Lcom/msc/c/b;->e()V

    .line 3560
    iget-object v0, p0, Lcom/osp/app/signin/pm;->b:Lcom/msc/c/f;

    if-eqz v0, :cond_2

    .line 3562
    invoke-static {}, Lcom/osp/device/b;->b()Lcom/osp/device/b;

    move-result-object v0

    invoke-virtual {v0}, Lcom/osp/device/b;->a()Z

    move-result v0

    if-eqz v0, :cond_1

    const-string v0, "USR_3174"

    iget-object v1, p0, Lcom/osp/app/signin/pm;->b:Lcom/msc/c/f;

    invoke-virtual {v1}, Lcom/msc/c/f;->a()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 3564
    iget-object v0, p0, Lcom/osp/app/signin/pm;->c:Lcom/osp/app/signin/UserValidateCheck;

    iget-object v1, p0, Lcom/osp/app/signin/pm;->c:Lcom/osp/app/signin/UserValidateCheck;

    invoke-static {v1}, Lcom/osp/app/util/ad;->h(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/osp/app/signin/UserValidateCheck;->b(Lcom/osp/app/signin/UserValidateCheck;Ljava/lang/String;)V

    .line 3565
    iget-object v0, p0, Lcom/osp/app/signin/pm;->c:Lcom/osp/app/signin/UserValidateCheck;

    invoke-virtual {v0}, Lcom/osp/app/signin/UserValidateCheck;->finish()V

    .line 3585
    :cond_0
    :goto_0
    return-void

    .line 3569
    :cond_1
    invoke-virtual {p0, v2}, Lcom/osp/app/signin/pm;->a(Z)V

    .line 3570
    iget-object v0, p0, Lcom/osp/app/signin/pm;->c:Lcom/osp/app/signin/UserValidateCheck;

    invoke-virtual {v0}, Lcom/osp/app/signin/UserValidateCheck;->finish()V

    goto :goto_0

    .line 3574
    :cond_2
    iget-object v0, p0, Lcom/osp/app/signin/pm;->c:Lcom/osp/app/signin/UserValidateCheck;

    invoke-static {v0}, Lcom/osp/app/signin/UserValidateCheck;->H(Lcom/osp/app/signin/UserValidateCheck;)Lcom/msc/a/g;

    move-result-object v0

    if-eqz v0, :cond_3

    .line 3576
    iget-object v0, p0, Lcom/osp/app/signin/pm;->c:Lcom/osp/app/signin/UserValidateCheck;

    invoke-static {v0}, Lcom/osp/app/signin/UserValidateCheck;->H(Lcom/osp/app/signin/UserValidateCheck;)Lcom/msc/a/g;

    move-result-object v0

    invoke-virtual {v0}, Lcom/msc/a/g;->h()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 3578
    iget-object v0, p0, Lcom/osp/app/signin/pm;->c:Lcom/osp/app/signin/UserValidateCheck;

    iget-object v1, p0, Lcom/osp/app/signin/pm;->c:Lcom/osp/app/signin/UserValidateCheck;

    invoke-static {v1}, Lcom/osp/app/util/ad;->g(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/osp/app/signin/UserValidateCheck;->e(Lcom/osp/app/signin/UserValidateCheck;Ljava/lang/String;)V

    goto :goto_0

    .line 3582
    :cond_3
    invoke-virtual {p0, v2}, Lcom/osp/app/signin/pm;->a(Z)V

    .line 3583
    iget-object v0, p0, Lcom/osp/app/signin/pm;->c:Lcom/osp/app/signin/UserValidateCheck;

    invoke-virtual {v0}, Lcom/osp/app/signin/UserValidateCheck;->finish()V

    goto :goto_0
.end method

.method protected final d()V
    .locals 1

    .prologue
    .line 3527
    invoke-super {p0}, Lcom/msc/c/b;->d()V

    .line 3528
    iget-object v0, p0, Lcom/osp/app/signin/pm;->c:Lcom/osp/app/signin/UserValidateCheck;

    invoke-virtual {v0}, Lcom/osp/app/signin/UserValidateCheck;->finish()V

    .line 3529
    return-void
.end method

.method protected final synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 3514
    invoke-virtual {p0}, Lcom/osp/app/signin/pm;->a()Ljava/lang/Boolean;

    move-result-object v0

    return-object v0
.end method

.method protected final synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 3514
    check-cast p1, Ljava/lang/Boolean;

    invoke-virtual {p0, p1}, Lcom/osp/app/signin/pm;->a(Ljava/lang/Boolean;)V

    return-void
.end method
