.class public Lcom/osp/app/signin/b;
.super Ljava/lang/Object;
.source "AccountGLD.java"


# instance fields
.field private final a:Landroid/content/Context;

.field private b:Lcom/osp/app/signin/c;

.field private c:Lcom/osp/common/property/a;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 135
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 41
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/osp/app/signin/b;->c:Lcom/osp/common/property/a;

    .line 137
    invoke-virtual {p1}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    iput-object v0, p0, Lcom/osp/app/signin/b;->a:Landroid/content/Context;

    .line 139
    invoke-static {}, Lcom/osp/app/signin/c;->a()Lcom/osp/app/signin/c;

    move-result-object v0

    iput-object v0, p0, Lcom/osp/app/signin/b;->b:Lcom/osp/app/signin/c;

    .line 141
    invoke-direct {p0}, Lcom/osp/app/signin/b;->b()Lcom/osp/common/property/a;

    move-result-object v0

    iput-object v0, p0, Lcom/osp/app/signin/b;->c:Lcom/osp/common/property/a;

    .line 142
    return-void
.end method

.method private b()Lcom/osp/common/property/a;
    .locals 3

    .prologue
    .line 318
    iget-object v0, p0, Lcom/osp/app/signin/b;->c:Lcom/osp/common/property/a;

    if-nez v0, :cond_1

    .line 320
    const-class v1, Lcom/osp/app/signin/b;

    monitor-enter v1

    .line 322
    :try_start_0
    iget-object v0, p0, Lcom/osp/app/signin/b;->c:Lcom/osp/common/property/a;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-nez v0, :cond_0

    .line 326
    :try_start_1
    new-instance v0, Lcom/osp/common/property/a;

    iget-object v2, p0, Lcom/osp/app/signin/b;->a:Landroid/content/Context;

    invoke-direct {v0, v2}, Lcom/osp/common/property/a;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/osp/app/signin/b;->c:Lcom/osp/common/property/a;
    :try_end_1
    .catch Lcom/osp/common/property/PropertyException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 332
    :cond_0
    :goto_0
    :try_start_2
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 334
    :cond_1
    iget-object v0, p0, Lcom/osp/app/signin/b;->c:Lcom/osp/common/property/a;

    return-object v0

    .line 327
    :catch_0
    move-exception v0

    :try_start_3
    invoke-virtual {v0}, Lcom/osp/common/property/PropertyException;->printStackTrace()V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    goto :goto_0

    .line 332
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method


# virtual methods
.method public final a()V
    .locals 4

    .prologue
    .line 146
    iget-object v0, p0, Lcom/osp/app/signin/b;->b:Lcom/osp/app/signin/c;

    if-nez v0, :cond_0

    .line 148
    invoke-static {}, Lcom/osp/app/signin/c;->a()Lcom/osp/app/signin/c;

    move-result-object v0

    iput-object v0, p0, Lcom/osp/app/signin/b;->b:Lcom/osp/app/signin/c;

    .line 150
    :cond_0
    iget-object v0, p0, Lcom/osp/app/signin/b;->c:Lcom/osp/common/property/a;

    if-nez v0, :cond_1

    .line 152
    invoke-direct {p0}, Lcom/osp/app/signin/b;->b()Lcom/osp/common/property/a;

    move-result-object v0

    iput-object v0, p0, Lcom/osp/app/signin/b;->c:Lcom/osp/common/property/a;

    .line 155
    :cond_1
    iget-object v0, p0, Lcom/osp/app/signin/b;->b:Lcom/osp/app/signin/c;

    invoke-static {v0}, Lcom/osp/app/signin/c;->a(Lcom/osp/app/signin/c;)V

    .line 159
    :try_start_0
    iget-object v0, p0, Lcom/osp/app/signin/b;->b:Lcom/osp/app/signin/c;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/osp/app/signin/b;->c:Lcom/osp/common/property/a;

    if-eqz v0, :cond_2

    .line 161
    iget-object v0, p0, Lcom/osp/app/signin/b;->c:Lcom/osp/common/property/a;

    const-string v1, "isAccountProvisioning"

    iget-object v2, p0, Lcom/osp/app/signin/b;->b:Lcom/osp/app/signin/c;

    invoke-static {v2}, Lcom/osp/app/signin/c;->b(Lcom/osp/app/signin/c;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/osp/common/property/a;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 163
    iget-object v0, p0, Lcom/osp/app/signin/b;->c:Lcom/osp/common/property/a;

    const-string v1, "expireDate"

    iget-object v2, p0, Lcom/osp/app/signin/b;->b:Lcom/osp/app/signin/c;

    invoke-static {v2}, Lcom/osp/app/signin/c;->c(Lcom/osp/app/signin/c;)J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/osp/common/property/a;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 164
    iget-object v0, p0, Lcom/osp/app/signin/b;->c:Lcom/osp/common/property/a;

    const-string v1, "accountAppVersion"

    iget-object v2, p0, Lcom/osp/app/signin/b;->b:Lcom/osp/app/signin/c;

    invoke-static {v2}, Lcom/osp/app/signin/c;->d(Lcom/osp/app/signin/c;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/osp/common/property/a;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 166
    iget-object v0, p0, Lcom/osp/app/signin/b;->c:Lcom/osp/common/property/a;

    const-string v1, "OSPV01_P_ADDRESS"

    iget-object v2, p0, Lcom/osp/app/signin/b;->b:Lcom/osp/app/signin/c;

    invoke-static {v2}, Lcom/osp/app/signin/c;->e(Lcom/osp/app/signin/c;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/osp/common/property/a;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 167
    iget-object v0, p0, Lcom/osp/app/signin/b;->c:Lcom/osp/common/property/a;

    const-string v1, "OSPV01_P_PORT"

    iget-object v2, p0, Lcom/osp/app/signin/b;->b:Lcom/osp/app/signin/c;

    invoke-static {v2}, Lcom/osp/app/signin/c;->f(Lcom/osp/app/signin/c;)I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/osp/common/property/a;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 168
    iget-object v0, p0, Lcom/osp/app/signin/b;->c:Lcom/osp/common/property/a;

    const-string v1, "OSPV01_P_REGION"

    iget-object v2, p0, Lcom/osp/app/signin/b;->b:Lcom/osp/app/signin/c;

    invoke-static {v2}, Lcom/osp/app/signin/c;->g(Lcom/osp/app/signin/c;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/osp/common/property/a;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 170
    iget-object v0, p0, Lcom/osp/app/signin/b;->c:Lcom/osp/common/property/a;

    const-string v1, "OSPV01_A_ADDRESS"

    iget-object v2, p0, Lcom/osp/app/signin/b;->b:Lcom/osp/app/signin/c;

    invoke-static {v2}, Lcom/osp/app/signin/c;->h(Lcom/osp/app/signin/c;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/osp/common/property/a;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 171
    iget-object v0, p0, Lcom/osp/app/signin/b;->c:Lcom/osp/common/property/a;

    const-string v1, "OSPV01_A_PORT"

    iget-object v2, p0, Lcom/osp/app/signin/b;->b:Lcom/osp/app/signin/c;

    invoke-static {v2}, Lcom/osp/app/signin/c;->i(Lcom/osp/app/signin/c;)I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/osp/common/property/a;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 172
    iget-object v0, p0, Lcom/osp/app/signin/b;->c:Lcom/osp/common/property/a;

    const-string v1, "OSPV01_A_REGION"

    iget-object v2, p0, Lcom/osp/app/signin/b;->b:Lcom/osp/app/signin/c;

    invoke-static {v2}, Lcom/osp/app/signin/c;->j(Lcom/osp/app/signin/c;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/osp/common/property/a;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 174
    iget-object v0, p0, Lcom/osp/app/signin/b;->c:Lcom/osp/common/property/a;

    const-string v1, "OSPV02_P_ADDRESS"

    iget-object v2, p0, Lcom/osp/app/signin/b;->b:Lcom/osp/app/signin/c;

    invoke-static {v2}, Lcom/osp/app/signin/c;->k(Lcom/osp/app/signin/c;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/osp/common/property/a;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 175
    iget-object v0, p0, Lcom/osp/app/signin/b;->c:Lcom/osp/common/property/a;

    const-string v1, "OSPV02_P_PORT"

    iget-object v2, p0, Lcom/osp/app/signin/b;->b:Lcom/osp/app/signin/c;

    invoke-static {v2}, Lcom/osp/app/signin/c;->l(Lcom/osp/app/signin/c;)I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/osp/common/property/a;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 176
    iget-object v0, p0, Lcom/osp/app/signin/b;->c:Lcom/osp/common/property/a;

    const-string v1, "OSPV02_P_REGION"

    iget-object v2, p0, Lcom/osp/app/signin/b;->b:Lcom/osp/app/signin/c;

    invoke-static {v2}, Lcom/osp/app/signin/c;->m(Lcom/osp/app/signin/c;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/osp/common/property/a;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 178
    iget-object v0, p0, Lcom/osp/app/signin/b;->c:Lcom/osp/common/property/a;

    const-string v1, "OSPV02_A_ADDRESS"

    iget-object v2, p0, Lcom/osp/app/signin/b;->b:Lcom/osp/app/signin/c;

    invoke-static {v2}, Lcom/osp/app/signin/c;->n(Lcom/osp/app/signin/c;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/osp/common/property/a;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 179
    iget-object v0, p0, Lcom/osp/app/signin/b;->c:Lcom/osp/common/property/a;

    const-string v1, "OSPV02_A_PORT"

    iget-object v2, p0, Lcom/osp/app/signin/b;->b:Lcom/osp/app/signin/c;

    invoke-static {v2}, Lcom/osp/app/signin/c;->o(Lcom/osp/app/signin/c;)I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/osp/common/property/a;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 180
    iget-object v0, p0, Lcom/osp/app/signin/b;->c:Lcom/osp/common/property/a;

    const-string v1, "OSPV02_A_REGION"

    iget-object v2, p0, Lcom/osp/app/signin/b;->b:Lcom/osp/app/signin/c;

    invoke-static {v2}, Lcom/osp/app/signin/c;->p(Lcom/osp/app/signin/c;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/osp/common/property/a;->b(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catch Lcom/osp/common/property/PropertyException; {:try_start_0 .. :try_end_0} :catch_0

    .line 186
    :cond_2
    :goto_0
    return-void

    .line 182
    :catch_0
    move-exception v0

    invoke-virtual {v0}, Lcom/osp/common/property/PropertyException;->printStackTrace()V

    goto :goto_0
.end method
