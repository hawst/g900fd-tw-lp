.class public Lcom/osp/app/signin/WipeOutService;
.super Lcom/osp/app/util/AbstractBaseService;
.source "WipeOutService.java"


# instance fields
.field private final a:Ljava/lang/Runnable;

.field private b:Ljava/lang/Thread;

.field private c:Ljava/lang/String;

.field private d:Lcom/osp/app/util/n;

.field private e:Landroid/os/Handler;

.field private f:Landroid/app/ProgressDialog;

.field private g:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 44
    invoke-direct {p0}, Lcom/osp/app/util/AbstractBaseService;-><init>()V

    .line 84
    new-instance v0, Lcom/osp/app/signin/qe;

    invoke-direct {v0, p0}, Lcom/osp/app/signin/qe;-><init>(Lcom/osp/app/signin/WipeOutService;)V

    iput-object v0, p0, Lcom/osp/app/signin/WipeOutService;->a:Ljava/lang/Runnable;

    .line 100
    const-string v0, ""

    iput-object v0, p0, Lcom/osp/app/signin/WipeOutService;->c:Ljava/lang/String;

    .line 118
    const-string v0, ""

    iput-object v0, p0, Lcom/osp/app/signin/WipeOutService;->g:Ljava/lang/String;

    return-void
.end method

.method static synthetic a(Lcom/osp/app/signin/WipeOutService;)V
    .locals 3

    .prologue
    .line 44
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v0, "onSuccess begin"

    invoke-static {v0}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;)V

    const-string v0, "DM_FACTORY_RESET"

    iget-object v1, p0, Lcom/osp/app/signin/WipeOutService;->g:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    new-instance v0, Landroid/content/Intent;

    const-string v1, "android.intent.action.DM_WIPEOUT_SUCCESS"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0, v0}, Lcom/osp/app/signin/WipeOutService;->sendBroadcast(Landroid/content/Intent;)V

    new-instance v0, Landroid/content/Intent;

    const-string v1, "android.intent.action.SAMSUNG_ACCOUNT_SIGNOUT_SUCCESS"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0, v0}, Lcom/osp/app/signin/WipeOutService;->sendBroadcast(Landroid/content/Intent;)V

    :cond_0
    :try_start_0
    new-instance v0, Landroid/content/Intent;

    const-string v1, "android.intent.action.REGISTRATION_CANCELED"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    const-string v1, "login_id"

    iget-object v2, p0, Lcom/osp/app/signin/WipeOutService;->c:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    invoke-static {}, Lcom/osp/device/b;->b()Lcom/osp/device/b;

    move-result-object v1

    invoke-virtual {v1}, Lcom/osp/device/b;->a()Z

    move-result v1

    if-eqz v1, :cond_1

    iget-object v1, p0, Lcom/osp/app/signin/WipeOutService;->c:Ljava/lang/String;

    invoke-static {v1}, Landroid/telephony/PhoneNumberUtils;->isGlobalPhoneNumber(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_2

    const-string v1, "login_id_type"

    const-string v2, "001"

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    :cond_1
    :goto_0
    invoke-virtual {p0, v0}, Lcom/osp/app/signin/WipeOutService;->sendBroadcast(Landroid/content/Intent;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    :goto_1
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v0, "onSuccess end"

    invoke-static {v0}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;)V

    invoke-virtual {p0}, Lcom/osp/app/signin/WipeOutService;->stopSelf()V

    return-void

    :cond_2
    :try_start_1
    const-string v1, "login_id_type"

    const-string v2, "003"

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_0

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_1
.end method

.method static synthetic a(Lcom/osp/app/signin/WipeOutService;Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 44
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v0, "onFailed begin"

    invoke-static {v0}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;)V

    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "failed message : "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;)V

    const-string v0, "DM_FACTORY_RESET"

    iget-object v1, p0, Lcom/osp/app/signin/WipeOutService;->g:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    new-instance v0, Landroid/content/Intent;

    const-string v1, "android.intent.action.DM_WIPEOUT_FAILED"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0, v0}, Lcom/osp/app/signin/WipeOutService;->sendBroadcast(Landroid/content/Intent;)V

    new-instance v0, Landroid/content/Intent;

    const-string v1, "android.intent.action.SAMSUNG_ACCOUNT_SIGNOUT_FAILED"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0, v0}, Lcom/osp/app/signin/WipeOutService;->sendBroadcast(Landroid/content/Intent;)V

    :cond_0
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v0, "onFailed end"

    invoke-static {v0}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;)V

    invoke-virtual {p0}, Lcom/osp/app/signin/WipeOutService;->stopSelf()V

    return-void
.end method

.method static synthetic b(Lcom/osp/app/signin/WipeOutService;)V
    .locals 3

    .prologue
    .line 44
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v0, "WOS"

    const-string v1, "onSignOutCompleted begin"

    invoke-static {v0, v1}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;)V

    :try_start_0
    const-string v0, "DM_FACTORY_RESET"

    iget-object v1, p0, Lcom/osp/app/signin/WipeOutService;->g:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    new-instance v0, Landroid/content/Intent;

    const-string v1, "android.intent.action.SAMSUNG_ACCOUNT_SIGNOUT_SUCCESS"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0, v0}, Lcom/osp/app/signin/WipeOutService;->sendBroadcast(Landroid/content/Intent;)V

    :cond_0
    new-instance v0, Landroid/content/Intent;

    const-string v1, "android.intent.action.REGISTRATION_CANCELED"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    const-string v1, "login_id"

    iget-object v2, p0, Lcom/osp/app/signin/WipeOutService;->c:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    invoke-static {}, Lcom/osp/device/b;->b()Lcom/osp/device/b;

    move-result-object v1

    invoke-virtual {v1}, Lcom/osp/device/b;->a()Z

    move-result v1

    if-eqz v1, :cond_1

    iget-object v1, p0, Lcom/osp/app/signin/WipeOutService;->c:Ljava/lang/String;

    invoke-static {v1}, Landroid/telephony/PhoneNumberUtils;->isGlobalPhoneNumber(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_2

    const-string v1, "login_id_type"

    const-string v2, "001"

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    :cond_1
    :goto_0
    invoke-virtual {p0, v0}, Lcom/osp/app/signin/WipeOutService;->sendBroadcast(Landroid/content/Intent;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    :goto_1
    new-instance v0, Landroid/content/Intent;

    const-string v1, "osp.signin.SAMSUNG_ACCOUNT_SIGNOUT"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0, v0}, Lcom/osp/app/signin/WipeOutService;->sendBroadcast(Landroid/content/Intent;)V

    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v0, "WOS"

    const-string v1, "onSignOutCompleted end"

    invoke-static {v0, v1}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {p0}, Lcom/osp/app/signin/WipeOutService;->stopSelf()V

    return-void

    :cond_2
    :try_start_1
    const-string v1, "login_id_type"

    const-string v2, "003"

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_0

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_1
.end method

.method static synthetic c(Lcom/osp/app/signin/WipeOutService;)V
    .locals 10

    .prologue
    const/4 v1, 0x1

    .line 44
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v0, "signOutUser begin"

    invoke-static {v0}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;)V

    new-instance v2, Landroid/os/Message;

    invoke-direct {v2}, Landroid/os/Message;-><init>()V

    const/16 v0, 0x1f

    iput v0, v2, Landroid/os/Message;->what:I

    :try_start_0
    new-instance v3, Lcom/osp/security/identity/d;

    invoke-direct {v3, p0}, Lcom/osp/security/identity/d;-><init>(Landroid/content/Context;)V

    new-instance v4, Lcom/osp/security/credential/a;

    invoke-direct {v4, p0}, Lcom/osp/security/credential/a;-><init>(Landroid/content/Context;)V

    new-instance v5, Lcom/osp/social/member/d;

    invoke-direct {v5, p0}, Lcom/osp/social/member/d;-><init>(Landroid/content/Context;)V

    new-instance v0, Lcom/osp/device/d;

    invoke-direct {v0, p0}, Lcom/osp/device/d;-><init>(Landroid/content/Context;)V

    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v6, " FACTORY DATA RESET or remove account"

    invoke-static {v6}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;)V

    new-instance v6, Lcom/osp/common/property/a;

    invoke-direct {v6, p0}, Lcom/osp/common/property/a;-><init>(Landroid/content/Context;)V

    const-string v7, "SignOut"

    const-string v8, "true"

    invoke-virtual {v6, v7, v8}, Lcom/osp/common/property/a;->b(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catch Lcom/osp/security/identity/IdentityException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Lcom/osp/common/property/PropertyException; {:try_start_0 .. :try_end_0} :catch_2
    .catch Lcom/osp/security/credential/CredentialException; {:try_start_0 .. :try_end_0} :catch_3
    .catch Lcom/osp/social/member/MemberServiceException; {:try_start_0 .. :try_end_0} :catch_4
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_5

    :try_start_1
    const-string v7, "device.registration.appid"

    const-string v8, "14eev3f64b"

    invoke-virtual {v6, v7, v8}, Lcom/osp/common/property/a;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v7, "WOS"

    const-string v8, "get appid form property."

    invoke-static {v7, v8}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;)V

    const-string v7, "j5p7ll8g33"

    invoke-virtual {v6, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_1

    const-string v6, "j5p7ll8g33"

    const-string v7, "5763D0052DC1462E13751F753384E9A9"

    invoke-virtual {v0, v6, v7}, Lcom/osp/device/d;->b(Ljava/lang/String;Ljava/lang/String;)V

    :goto_0
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v0, "Device: unregisted"

    invoke-static {v0}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;)V
    :try_end_1
    .catch Lcom/osp/device/DeviceException; {:try_start_1 .. :try_end_1} :catch_0
    .catch Lcom/osp/security/identity/IdentityException; {:try_start_1 .. :try_end_1} :catch_1
    .catch Lcom/osp/common/property/PropertyException; {:try_start_1 .. :try_end_1} :catch_2
    .catch Lcom/osp/security/credential/CredentialException; {:try_start_1 .. :try_end_1} :catch_3
    .catch Lcom/osp/social/member/MemberServiceException; {:try_start_1 .. :try_end_1} :catch_4
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_5

    :goto_1
    :try_start_2
    invoke-static {p0}, Lcom/osp/app/util/ad;->h(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/osp/app/signin/WipeOutService;->c:Ljava/lang/String;

    invoke-virtual {v3}, Lcom/osp/security/identity/d;->l()V

    invoke-virtual {v3}, Lcom/osp/security/identity/d;->k()V

    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v0, "User: unauthenticated"

    invoke-static {v0}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;)V

    invoke-virtual {v4}, Lcom/osp/security/credential/a;->a()V

    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v0, "Credentials: cleared"

    invoke-static {v0}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;)V

    invoke-virtual {v5}, Lcom/osp/social/member/d;->a()V

    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v0, "AppIDs: cleared"

    invoke-static {v0}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;)V

    new-instance v0, Lcom/osp/common/property/a;

    invoke-direct {v0, p0}, Lcom/osp/common/property/a;-><init>(Landroid/content/Context;)V

    const-string v3, "SignOut"

    invoke-virtual {v0, v3}, Lcom/osp/common/property/a;->c(Ljava/lang/String;)V
    :try_end_2
    .catch Lcom/osp/security/identity/IdentityException; {:try_start_2 .. :try_end_2} :catch_1
    .catch Lcom/osp/common/property/PropertyException; {:try_start_2 .. :try_end_2} :catch_2
    .catch Lcom/osp/security/credential/CredentialException; {:try_start_2 .. :try_end_2} :catch_3
    .catch Lcom/osp/social/member/MemberServiceException; {:try_start_2 .. :try_end_2} :catch_4
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_5

    :goto_2
    iget-object v0, p0, Lcom/osp/app/signin/WipeOutService;->e:Landroid/os/Handler;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/osp/app/signin/WipeOutService;->e:Landroid/os/Handler;

    invoke-virtual {v0, v2}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    :cond_0
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v0, "signOutUser end"

    invoke-static {v0}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;)V

    return-void

    :cond_1
    :try_start_3
    const-string v6, "14eev3f64b"

    const-string v7, "109E2830E09DB340924B8ABE0D6290C3"

    invoke-virtual {v0, v6, v7}, Lcom/osp/device/d;->b(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_3
    .catch Lcom/osp/device/DeviceException; {:try_start_3 .. :try_end_3} :catch_0
    .catch Lcom/osp/security/identity/IdentityException; {:try_start_3 .. :try_end_3} :catch_1
    .catch Lcom/osp/common/property/PropertyException; {:try_start_3 .. :try_end_3} :catch_2
    .catch Lcom/osp/security/credential/CredentialException; {:try_start_3 .. :try_end_3} :catch_3
    .catch Lcom/osp/social/member/MemberServiceException; {:try_start_3 .. :try_end_3} :catch_4
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_5

    goto :goto_0

    :catch_0
    move-exception v0

    :try_start_4
    iget-object v6, p0, Lcom/osp/app/signin/WipeOutService;->d:Lcom/osp/app/util/n;

    invoke-virtual {v6, p0, v0}, Lcom/osp/app/util/n;->a(Landroid/content/Context;Lcom/osp/common/abstracts/AbstractOSPException;)Ljava/lang/String;

    move-result-object v6

    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v7, "WOS"

    const-string v8, "DeviceException e"

    invoke-static {v7, v8}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;)V

    const-string v7, "SSO_2101"

    invoke-virtual {v0}, Lcom/osp/device/DeviceException;->a()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v7, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    const/16 v0, 0x29

    :goto_3
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v7, "WOS"

    new-instance v8, Ljava/lang/StringBuilder;

    const-string v9, "DeviceException error obj\t:\t"

    invoke-direct {v8, v9}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v8, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v7, v6}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;)V

    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v6, "WOS"

    new-instance v7, Ljava/lang/StringBuilder;

    const-string v8, "DeviceException error what\t:\t"

    invoke-direct {v7, v8}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v7, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v6, v0}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_4
    .catch Lcom/osp/security/identity/IdentityException; {:try_start_4 .. :try_end_4} :catch_1
    .catch Lcom/osp/common/property/PropertyException; {:try_start_4 .. :try_end_4} :catch_2
    .catch Lcom/osp/security/credential/CredentialException; {:try_start_4 .. :try_end_4} :catch_3
    .catch Lcom/osp/social/member/MemberServiceException; {:try_start_4 .. :try_end_4} :catch_4
    .catch Ljava/lang/Exception; {:try_start_4 .. :try_end_4} :catch_5

    goto/16 :goto_1

    :catch_1
    move-exception v0

    iput v1, v2, Landroid/os/Message;->what:I

    iget-object v1, p0, Lcom/osp/app/signin/WipeOutService;->d:Lcom/osp/app/util/n;

    invoke-virtual {v1, p0, v0}, Lcom/osp/app/util/n;->a(Landroid/content/Context;Lcom/osp/common/abstracts/AbstractOSPException;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, v2, Landroid/os/Message;->obj:Ljava/lang/Object;

    invoke-virtual {v0}, Lcom/osp/security/identity/IdentityException;->printStackTrace()V

    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v0, "IdentityException e"

    invoke-static {v0}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;)V

    goto :goto_2

    :catch_2
    move-exception v0

    iput v1, v2, Landroid/os/Message;->what:I

    iget-object v1, p0, Lcom/osp/app/signin/WipeOutService;->d:Lcom/osp/app/util/n;

    invoke-virtual {v1, p0, v0}, Lcom/osp/app/util/n;->a(Landroid/content/Context;Lcom/osp/common/abstracts/AbstractOSPException;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, v2, Landroid/os/Message;->obj:Ljava/lang/Object;

    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v0, "IdentityException e"

    invoke-static {v0}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;)V

    goto/16 :goto_2

    :catch_3
    move-exception v0

    iput v1, v2, Landroid/os/Message;->what:I

    iget-object v1, p0, Lcom/osp/app/signin/WipeOutService;->d:Lcom/osp/app/util/n;

    invoke-virtual {v1, p0, v0}, Lcom/osp/app/util/n;->a(Landroid/content/Context;Lcom/osp/common/abstracts/AbstractOSPException;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, v2, Landroid/os/Message;->obj:Ljava/lang/Object;

    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v0, "IdentityException e"

    invoke-static {v0}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;)V

    goto/16 :goto_2

    :catch_4
    move-exception v0

    iput v1, v2, Landroid/os/Message;->what:I

    iget-object v1, p0, Lcom/osp/app/signin/WipeOutService;->d:Lcom/osp/app/util/n;

    invoke-virtual {v1, p0, v0}, Lcom/osp/app/util/n;->a(Landroid/content/Context;Lcom/osp/common/abstracts/AbstractOSPException;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, v2, Landroid/os/Message;->obj:Ljava/lang/Object;

    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v0, "IdentityException e"

    invoke-static {v0}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;)V

    goto/16 :goto_2

    :catch_5
    move-exception v0

    iput v1, v2, Landroid/os/Message;->what:I

    iget-object v0, p0, Lcom/osp/app/signin/WipeOutService;->d:Lcom/osp/app/util/n;

    const-string v1, "ERR_0000"

    invoke-virtual {v0, p0, v1}, Lcom/osp/app/util/n;->a(Landroid/content/Context;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, v2, Landroid/os/Message;->obj:Ljava/lang/Object;

    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v0, "IdentityException e"

    invoke-static {v0}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;)V

    goto/16 :goto_2

    :cond_2
    move v0, v1

    goto/16 :goto_3
.end method


# virtual methods
.method public onBind(Landroid/content/Intent;)Landroid/os/IBinder;
    .locals 1

    .prologue
    .line 402
    const-string v0, "onBind"

    invoke-virtual {p0, p1, v0}, Lcom/osp/app/signin/WipeOutService;->a(Landroid/content/Intent;Ljava/lang/String;)V

    .line 403
    const/4 v0, 0x0

    return-object v0
.end method

.method public onConfigurationChanged(Landroid/content/res/Configuration;)V
    .locals 1

    .prologue
    .line 408
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v0, "onConfigurationChanged begin"

    invoke-static {v0}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;)V

    .line 409
    invoke-super {p0, p1}, Lcom/osp/app/util/AbstractBaseService;->onConfigurationChanged(Landroid/content/res/Configuration;)V

    .line 410
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v0, "onConfigurationChanged end"

    invoke-static {v0}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;)V

    .line 411
    return-void
.end method

.method public onCreate()V
    .locals 1

    .prologue
    .line 415
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v0, "onCreate begin"

    invoke-static {v0}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;)V

    .line 416
    invoke-super {p0}, Lcom/osp/app/util/AbstractBaseService;->onCreate()V

    .line 417
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v0, "onCreate end"

    invoke-static {v0}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;)V

    .line 418
    return-void
.end method

.method public onDestroy()V
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 422
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v0, "onDestory begin"

    invoke-static {v0}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;)V

    .line 425
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v0, "cleanUpChildThread begin"

    invoke-static {v0}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;)V

    :try_start_0
    iget-object v0, p0, Lcom/osp/app/signin/WipeOutService;->b:Ljava/lang/Thread;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/osp/app/signin/WipeOutService;->b:Ljava/lang/Thread;

    const-wide/16 v2, 0x12c

    invoke-virtual {v0, v2, v3}, Ljava/lang/Thread;->join(J)V
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0

    :cond_0
    :goto_0
    iput-object v4, p0, Lcom/osp/app/signin/WipeOutService;->b:Ljava/lang/Thread;

    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v0, "cleanUpChildThread end"

    invoke-static {v0}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;)V

    .line 427
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v0, "cleanUpProgressDialog begin"

    invoke-static {v0}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;)V

    :try_start_1
    iget-object v0, p0, Lcom/osp/app/signin/WipeOutService;->f:Landroid/app/ProgressDialog;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/osp/app/signin/WipeOutService;->f:Landroid/app/ProgressDialog;

    invoke-virtual {v0}, Landroid/app/ProgressDialog;->isShowing()Z

    move-result v0

    const/4 v1, 0x1

    if-ne v0, v1, :cond_1

    iget-object v0, p0, Lcom/osp/app/signin/WipeOutService;->f:Landroid/app/ProgressDialog;

    invoke-virtual {v0}, Landroid/app/ProgressDialog;->dismiss()V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    :cond_1
    :goto_1
    iput-object v4, p0, Lcom/osp/app/signin/WipeOutService;->f:Landroid/app/ProgressDialog;

    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v0, "cleanUpProgressDialog end"

    invoke-static {v0}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;)V

    .line 429
    iput-object v4, p0, Lcom/osp/app/signin/WipeOutService;->e:Landroid/os/Handler;

    .line 431
    invoke-super {p0}, Lcom/osp/app/util/AbstractBaseService;->onDestroy()V

    .line 432
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v0, "onDestory end"

    invoke-static {v0}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;)V

    .line 433
    return-void

    .line 425
    :catch_0
    move-exception v0

    invoke-virtual {v0}, Ljava/lang/InterruptedException;->printStackTrace()V

    goto :goto_0

    .line 427
    :catch_1
    move-exception v0

    :try_start_2
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_1

    :catchall_0
    move-exception v0

    iput-object v4, p0, Lcom/osp/app/signin/WipeOutService;->f:Landroid/app/ProgressDialog;

    throw v0
.end method

.method public onStartCommand(Landroid/content/Intent;II)I
    .locals 3

    .prologue
    .line 437
    if-eqz p1, :cond_0

    .line 439
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v0, "prepareSignOutUser begin"

    invoke-static {v0}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;)V

    new-instance v0, Ljava/lang/Thread;

    iget-object v1, p0, Lcom/osp/app/signin/WipeOutService;->a:Ljava/lang/Runnable;

    invoke-direct {v0, v1}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;)V

    iput-object v0, p0, Lcom/osp/app/signin/WipeOutService;->b:Ljava/lang/Thread;

    iget-object v0, p0, Lcom/osp/app/signin/WipeOutService;->b:Ljava/lang/Thread;

    invoke-virtual {v0}, Ljava/lang/Thread;->start()V

    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v0, "prepareSignOutUser end"

    invoke-static {v0}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;)V

    .line 440
    new-instance v0, Lcom/osp/app/signin/qf;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/osp/app/signin/qf;-><init>(Lcom/osp/app/signin/WipeOutService;B)V

    iput-object v0, p0, Lcom/osp/app/signin/WipeOutService;->e:Landroid/os/Handler;

    .line 441
    invoke-static {}, Lcom/osp/app/util/n;->a()Lcom/osp/app/util/n;

    move-result-object v0

    iput-object v0, p0, Lcom/osp/app/signin/WipeOutService;->d:Lcom/osp/app/util/n;

    .line 442
    const-string v0, "MODE"

    invoke-virtual {p1, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/osp/app/signin/WipeOutService;->g:Ljava/lang/String;

    .line 443
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v0, "WOS"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Mode : "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, Lcom/osp/app/signin/WipeOutService;->g:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 448
    :goto_0
    const/4 v0, 0x2

    return v0

    .line 446
    :cond_0
    invoke-virtual {p0}, Lcom/osp/app/signin/WipeOutService;->stopSelf()V

    goto :goto_0
.end method
