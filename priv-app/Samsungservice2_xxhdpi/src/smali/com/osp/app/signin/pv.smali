.class final Lcom/osp/app/signin/pv;
.super Ljava/lang/Object;
.source "WebContentView.java"

# interfaces
.implements Landroid/content/DialogInterface$OnKeyListener;


# instance fields
.field final synthetic a:Lcom/osp/app/signin/WebContentView;


# direct methods
.method constructor <init>(Lcom/osp/app/signin/WebContentView;)V
    .locals 0

    .prologue
    .line 438
    iput-object p1, p0, Lcom/osp/app/signin/pv;->a:Lcom/osp/app/signin/WebContentView;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onKey(Landroid/content/DialogInterface;ILandroid/view/KeyEvent;)Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 442
    const/4 v1, 0x4

    invoke-virtual {p3}, Landroid/view/KeyEvent;->getKeyCode()I

    move-result v2

    if-ne v1, v2, :cond_1

    .line 444
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v1, "WCV"

    const-string v2, "ProgressDialog - back key pressed."

    invoke-static {v1, v2}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 448
    :try_start_0
    iget-object v1, p0, Lcom/osp/app/signin/pv;->a:Lcom/osp/app/signin/WebContentView;

    invoke-virtual {v1}, Lcom/osp/app/signin/WebContentView;->isFinishing()Z

    move-result v1

    if-nez v1, :cond_0

    iget-object v1, p0, Lcom/osp/app/signin/pv;->a:Lcom/osp/app/signin/WebContentView;

    invoke-static {v1}, Lcom/osp/app/signin/WebContentView;->c(Lcom/osp/app/signin/WebContentView;)Landroid/app/ProgressDialog;

    move-result-object v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/osp/app/signin/pv;->a:Lcom/osp/app/signin/WebContentView;

    invoke-static {v1}, Lcom/osp/app/signin/WebContentView;->c(Lcom/osp/app/signin/WebContentView;)Landroid/app/ProgressDialog;

    move-result-object v1

    invoke-virtual {v1}, Landroid/app/ProgressDialog;->isShowing()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 450
    iget-object v1, p0, Lcom/osp/app/signin/pv;->a:Lcom/osp/app/signin/WebContentView;

    invoke-static {v1}, Lcom/osp/app/signin/WebContentView;->c(Lcom/osp/app/signin/WebContentView;)Landroid/app/ProgressDialog;

    move-result-object v1

    invoke-virtual {v1}, Landroid/app/ProgressDialog;->dismiss()V

    .line 451
    iget-object v1, p0, Lcom/osp/app/signin/pv;->a:Lcom/osp/app/signin/WebContentView;

    invoke-static {v1}, Lcom/osp/app/signin/WebContentView;->d(Lcom/osp/app/signin/WebContentView;)Landroid/app/ProgressDialog;

    .line 452
    iget-object v1, p0, Lcom/osp/app/signin/pv;->a:Lcom/osp/app/signin/WebContentView;

    invoke-static {v1}, Lcom/osp/app/signin/WebContentView;->a(Lcom/osp/app/signin/WebContentView;)Landroid/webkit/WebView;

    move-result-object v1

    invoke-virtual {v1}, Landroid/webkit/WebView;->stopLoading()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 459
    :cond_0
    :goto_0
    iget-object v1, p0, Lcom/osp/app/signin/pv;->a:Lcom/osp/app/signin/WebContentView;

    invoke-static {v1}, Lcom/osp/app/signin/WebContentView;->a(Lcom/osp/app/signin/WebContentView;)Landroid/webkit/WebView;

    move-result-object v1

    invoke-virtual {v1}, Landroid/webkit/WebView;->canGoBack()Z

    move-result v1

    if-eqz v1, :cond_2

    .line 461
    iget-object v0, p0, Lcom/osp/app/signin/pv;->a:Lcom/osp/app/signin/WebContentView;

    invoke-static {v0}, Lcom/osp/app/signin/WebContentView;->a(Lcom/osp/app/signin/WebContentView;)Landroid/webkit/WebView;

    move-result-object v0

    invoke-virtual {v0}, Landroid/webkit/WebView;->goBack()V

    .line 468
    :goto_1
    const/4 v0, 0x1

    .line 471
    :cond_1
    return v0

    .line 454
    :catch_0
    move-exception v1

    invoke-virtual {v1}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_0

    .line 464
    :cond_2
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v1, "WCV"

    const-string v2, "cannot go back."

    invoke-static {v1, v2}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 465
    iget-object v1, p0, Lcom/osp/app/signin/pv;->a:Lcom/osp/app/signin/WebContentView;

    iget-object v2, p0, Lcom/osp/app/signin/pv;->a:Lcom/osp/app/signin/WebContentView;

    invoke-static {v2}, Lcom/osp/app/signin/WebContentView;->b(Lcom/osp/app/signin/WebContentView;)Landroid/content/Intent;

    move-result-object v2

    invoke-virtual {v1, v0, v2}, Lcom/osp/app/signin/WebContentView;->a(ILandroid/content/Intent;)V

    .line 466
    iget-object v0, p0, Lcom/osp/app/signin/pv;->a:Lcom/osp/app/signin/WebContentView;

    invoke-virtual {v0}, Lcom/osp/app/signin/WebContentView;->finish()V

    goto :goto_1
.end method
