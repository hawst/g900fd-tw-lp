.class final Lcom/osp/app/signin/oi;
.super Ljava/lang/Object;
.source "TnCView.java"

# interfaces
.implements Landroid/widget/AdapterView$OnItemClickListener;


# instance fields
.field final synthetic a:Lcom/osp/app/signin/TnCView;


# direct methods
.method private constructor <init>(Lcom/osp/app/signin/TnCView;)V
    .locals 0

    .prologue
    .line 3535
    iput-object p1, p0, Lcom/osp/app/signin/oi;->a:Lcom/osp/app/signin/TnCView;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/osp/app/signin/TnCView;B)V
    .locals 0

    .prologue
    .line 3535
    invoke-direct {p0, p1}, Lcom/osp/app/signin/oi;-><init>(Lcom/osp/app/signin/TnCView;)V

    return-void
.end method

.method private a()V
    .locals 5

    .prologue
    const/4 v4, 0x1

    const/4 v2, 0x0

    .line 3920
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "TnCView - showPrivacyPolicyDialog - m_PP : "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v1, p0, Lcom/osp/app/signin/oi;->a:Lcom/osp/app/signin/TnCView;

    invoke-static {v1}, Lcom/osp/app/signin/TnCView;->z(Lcom/osp/app/signin/TnCView;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;)V

    .line 3927
    :try_start_0
    iget-object v0, p0, Lcom/osp/app/signin/oi;->a:Lcom/osp/app/signin/TnCView;

    invoke-virtual {v0}, Lcom/osp/app/signin/TnCView;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v0

    iget-object v1, p0, Lcom/osp/app/signin/oi;->a:Lcom/osp/app/signin/TnCView;

    invoke-virtual {v1}, Lcom/osp/app/signin/TnCView;->getPackageName()Ljava/lang/String;

    move-result-object v1

    const/4 v3, 0x0

    invoke-virtual {v0, v1, v3}, Landroid/content/pm/PackageManager;->getPackageInfo(Ljava/lang/String;I)Landroid/content/pm/PackageInfo;

    move-result-object v0

    .line 3928
    iget v0, v0, Landroid/content/pm/PackageInfo;->versionCode:I
    :try_end_0
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    .line 3934
    :goto_0
    const v1, 0x249f0

    if-ge v0, v1, :cond_0

    .line 3936
    new-instance v0, Landroid/content/Intent;

    iget-object v1, p0, Lcom/osp/app/signin/oi;->a:Lcom/osp/app/signin/TnCView;

    const-class v2, Lcom/osp/app/signin/ShowTncInfoView;

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 3937
    const-string v1, "client_id"

    iget-object v2, p0, Lcom/osp/app/signin/oi;->a:Lcom/osp/app/signin/TnCView;

    invoke-static {v2}, Lcom/osp/app/signin/TnCView;->y(Lcom/osp/app/signin/TnCView;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 3938
    const-string v1, "terms_type"

    const-string v2, "pp"

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 3939
    const-string v1, "country_code_mcc"

    iget-object v2, p0, Lcom/osp/app/signin/oi;->a:Lcom/osp/app/signin/TnCView;

    invoke-static {v2}, Lcom/osp/app/signin/TnCView;->q(Lcom/osp/app/signin/TnCView;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 3940
    iget-object v1, p0, Lcom/osp/app/signin/oi;->a:Lcom/osp/app/signin/TnCView;

    invoke-virtual {v1, v0}, Lcom/osp/app/signin/TnCView;->startActivity(Landroid/content/Intent;)V

    .line 3958
    :goto_1
    return-void

    .line 3929
    :catch_0
    move-exception v0

    invoke-virtual {v0}, Landroid/content/pm/PackageManager$NameNotFoundException;->printStackTrace()V

    move v0, v2

    goto :goto_0

    .line 3943
    :cond_0
    iget-object v0, p0, Lcom/osp/app/signin/oi;->a:Lcom/osp/app/signin/TnCView;

    invoke-static {v0}, Lcom/osp/app/signin/TnCView;->p(Lcom/osp/app/signin/TnCView;)Ljava/util/ArrayList;

    move-result-object v0

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Ljava/lang/String;

    .line 3944
    const-string v1, ""

    .line 3945
    aget-object v3, v0, v2

    if-eqz v3, :cond_1

    aget-object v3, v0, v2

    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v3

    if-le v3, v4, :cond_1

    .line 3947
    aget-object v0, v0, v2

    invoke-virtual {v0, v4}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v0

    .line 3950
    :goto_2
    const-string v1, "general.txt"

    const-string v2, "globalpp.html"

    invoke-virtual {v0, v1, v2}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v0

    .line 3952
    invoke-static {v0}, Lcom/msc/c/k;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 3954
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "request PP = "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;)V

    .line 3956
    iget-object v1, p0, Lcom/osp/app/signin/oi;->a:Lcom/osp/app/signin/TnCView;

    invoke-virtual {v1, v0}, Lcom/osp/app/signin/TnCView;->a(Ljava/lang/String;)V

    goto :goto_1

    :cond_1
    move-object v0, v1

    goto :goto_2
.end method

.method private a(I)V
    .locals 6

    .prologue
    const/4 v5, 0x1

    const/4 v2, 0x0

    .line 3962
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "TnCView - showServiceTncDialog - mServiceTNCdata["

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "] : "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/osp/app/signin/oi;->a:Lcom/osp/app/signin/TnCView;

    invoke-static {v1}, Lcom/osp/app/signin/TnCView;->A(Lcom/osp/app/signin/TnCView;)[Ljava/lang/String;

    move-result-object v1

    aget-object v1, v1, p1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;)V

    .line 3963
    iget-object v0, p0, Lcom/osp/app/signin/oi;->a:Lcom/osp/app/signin/TnCView;

    invoke-static {v0}, Lcom/osp/app/signin/TnCView;->p(Lcom/osp/app/signin/TnCView;)Ljava/util/ArrayList;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Ljava/lang/String;

    .line 3964
    aget-object v3, v0, v5

    .line 3965
    const-string v1, ""

    .line 3966
    aget-object v4, v0, v2

    if-eqz v4, :cond_2

    aget-object v4, v0, v2

    invoke-virtual {v4}, Ljava/lang/String;->length()I

    move-result v4

    if-le v4, v5, :cond_2

    .line 3968
    aget-object v0, v0, v2

    invoke-virtual {v0, v5}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v0

    .line 3974
    :goto_0
    :try_start_0
    iget-object v1, p0, Lcom/osp/app/signin/oi;->a:Lcom/osp/app/signin/TnCView;

    invoke-virtual {v1}, Lcom/osp/app/signin/TnCView;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v1

    iget-object v4, p0, Lcom/osp/app/signin/oi;->a:Lcom/osp/app/signin/TnCView;

    invoke-virtual {v4}, Lcom/osp/app/signin/TnCView;->getPackageName()Ljava/lang/String;

    move-result-object v4

    const/4 v5, 0x0

    invoke-virtual {v1, v4, v5}, Landroid/content/pm/PackageManager;->getPackageInfo(Ljava/lang/String;I)Landroid/content/pm/PackageInfo;

    move-result-object v1

    .line 3975
    iget v1, v1, Landroid/content/pm/PackageInfo;->versionCode:I
    :try_end_0
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    .line 3981
    :goto_1
    const v2, 0x249f0

    if-ge v1, v2, :cond_1

    .line 3983
    new-instance v1, Landroid/content/Intent;

    iget-object v2, p0, Lcom/osp/app/signin/oi;->a:Lcom/osp/app/signin/TnCView;

    const-class v4, Lcom/osp/app/signin/ShowTncInfoView;

    invoke-direct {v1, v2, v4}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 3984
    const-string v2, "client_id"

    iget-object v4, p0, Lcom/osp/app/signin/oi;->a:Lcom/osp/app/signin/TnCView;

    invoke-static {v4}, Lcom/osp/app/signin/TnCView;->y(Lcom/osp/app/signin/TnCView;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v1, v2, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 3985
    if-nez p1, :cond_0

    .line 3987
    const-string v2, "terms_type"

    const-string v4, "tnc"

    invoke-virtual {v1, v2, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 3992
    :goto_2
    const-string v2, "country_code_mcc"

    iget-object v4, p0, Lcom/osp/app/signin/oi;->a:Lcom/osp/app/signin/TnCView;

    invoke-static {v4}, Lcom/osp/app/signin/TnCView;->q(Lcom/osp/app/signin/TnCView;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v1, v2, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 3993
    const-string v2, "Title"

    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 3994
    const-string v2, "FileName"

    invoke-virtual {v1, v2, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 3995
    iget-object v0, p0, Lcom/osp/app/signin/oi;->a:Lcom/osp/app/signin/TnCView;

    invoke-virtual {v0, v1}, Lcom/osp/app/signin/TnCView;->startActivity(Landroid/content/Intent;)V

    .line 4005
    :goto_3
    return-void

    .line 3976
    :catch_0
    move-exception v1

    invoke-virtual {v1}, Landroid/content/pm/PackageManager$NameNotFoundException;->printStackTrace()V

    move v1, v2

    goto :goto_1

    .line 3990
    :cond_0
    const-string v2, "terms_type"

    const-string v4, "svctnc"

    invoke-virtual {v1, v2, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    goto :goto_2

    .line 3998
    :cond_1
    const-string v1, ".txt"

    const-string v2, ".html"

    invoke-virtual {v0, v1, v2}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v0

    .line 3999
    invoke-static {v0}, Lcom/msc/c/k;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 4001
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "request TnC = "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;)V

    .line 4003
    iget-object v1, p0, Lcom/osp/app/signin/oi;->a:Lcom/osp/app/signin/TnCView;

    invoke-virtual {v1, v0}, Lcom/osp/app/signin/TnCView;->a(Ljava/lang/String;)V

    goto :goto_3

    :cond_2
    move-object v0, v1

    goto/16 :goto_0
.end method


# virtual methods
.method public final onItemClick(Landroid/widget/AdapterView;Landroid/view/View;IJ)V
    .locals 2

    .prologue
    .line 3540
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "TnCView - onItemClick - position : "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;)V

    .line 3541
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "TnCView - onItemClick - id : "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p4, p5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;)V

    .line 3543
    iget-object v0, p0, Lcom/osp/app/signin/oi;->a:Lcom/osp/app/signin/TnCView;

    invoke-static {v0}, Lcom/osp/app/signin/TnCView;->h(Lcom/osp/app/signin/TnCView;)I

    move-result v0

    packed-switch v0, :pswitch_data_0

    .line 3556
    :cond_0
    :goto_0
    return-void

    .line 3546
    :pswitch_0
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "TnCView - onDefaultListItemClick - position : "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;)V

    if-nez p2, :cond_0

    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v0, "TnCView - onDefaultListItemClick view is null"

    invoke-static {v0}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;)V

    goto :goto_0

    .line 3549
    :pswitch_1
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "TnCView - onDefaultNobuttonListItemClick - position : "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;)V

    if-nez p2, :cond_1

    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v0, "TnCView - onDefaultNobuttonListItemClick view is null"

    invoke-static {v0}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;)V

    :cond_1
    packed-switch p3, :pswitch_data_1

    goto :goto_0

    :pswitch_2
    invoke-direct {p0, p3}, Lcom/osp/app/signin/oi;->a(I)V

    goto :goto_0

    :pswitch_3
    iget-object v0, p0, Lcom/osp/app/signin/oi;->a:Lcom/osp/app/signin/TnCView;

    invoke-static {v0}, Lcom/osp/app/signin/TnCView;->o(Lcom/osp/app/signin/TnCView;)V

    goto :goto_0

    :pswitch_4
    invoke-direct {p0}, Lcom/osp/app/signin/oi;->a()V

    goto :goto_0

    .line 3552
    :pswitch_5
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "TnCView - onKorListItemClick - position : "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;)V

    if-nez p2, :cond_0

    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v0, "TnCView - onKorListItemClick view is null"

    invoke-static {v0}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;)V

    goto :goto_0

    .line 3555
    :pswitch_6
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "TnCView - onKorNobuttonListItemClick - position : "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;)V

    if-nez p2, :cond_2

    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v0, "TnCView - onKorNobuttonListItemClick view is null"

    invoke-static {v0}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;)V

    :cond_2
    packed-switch p3, :pswitch_data_2

    goto/16 :goto_0

    :pswitch_7
    invoke-direct {p0, p3}, Lcom/osp/app/signin/oi;->a(I)V

    goto/16 :goto_0

    :pswitch_8
    iget-object v0, p0, Lcom/osp/app/signin/oi;->a:Lcom/osp/app/signin/TnCView;

    invoke-static {v0}, Lcom/osp/app/signin/TnCView;->o(Lcom/osp/app/signin/TnCView;)V

    goto/16 :goto_0

    :pswitch_9
    invoke-direct {p0}, Lcom/osp/app/signin/oi;->a()V

    goto/16 :goto_0

    .line 3543
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_5
        :pswitch_6
    .end packed-switch

    .line 3549
    :pswitch_data_1
    .packed-switch 0x0
        :pswitch_2
        :pswitch_3
        :pswitch_4
    .end packed-switch

    .line 3555
    :pswitch_data_2
    .packed-switch 0x0
        :pswitch_7
        :pswitch_8
        :pswitch_9
    .end packed-switch
.end method
