.class final Lcom/osp/app/signin/ca;
.super Lcom/msc/c/b;
.source "AccountinfoView.java"


# instance fields
.field final synthetic c:Lcom/osp/app/signin/AccountinfoView;

.field private d:J

.field private e:J

.field private f:J

.field private g:J

.field private h:J

.field private i:Z


# direct methods
.method public constructor <init>(Lcom/osp/app/signin/AccountinfoView;)V
    .locals 1

    .prologue
    .line 3812
    iput-object p1, p0, Lcom/osp/app/signin/ca;->c:Lcom/osp/app/signin/AccountinfoView;

    .line 3813
    invoke-direct {p0, p1}, Lcom/msc/c/b;-><init>(Landroid/content/Context;)V

    .line 3810
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/osp/app/signin/ca;->i:Z

    .line 3817
    return-void
.end method

.method private b(Ljava/lang/String;)V
    .locals 3

    .prologue
    .line 4022
    invoke-static {}, Lcom/msc/c/g;->a()Lcom/msc/c/g;

    .line 4023
    iget-object v0, p0, Lcom/osp/app/signin/ca;->c:Lcom/osp/app/signin/AccountinfoView;

    iget-object v1, p0, Lcom/osp/app/signin/ca;->c:Lcom/osp/app/signin/AccountinfoView;

    invoke-static {v1}, Lcom/msc/c/e;->h(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1, p1, p0}, Lcom/msc/c/g;->d(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Lcom/msc/b/h;)J

    move-result-wide v0

    iput-wide v0, p0, Lcom/osp/app/signin/ca;->e:J

    .line 4024
    iget-wide v0, p0, Lcom/osp/app/signin/ca;->e:J

    invoke-virtual {p0, v0, v1}, Lcom/osp/app/signin/ca;->a(J)V

    .line 4025
    iget-wide v0, p0, Lcom/osp/app/signin/ca;->e:J

    const-string v2, "from_xml"

    invoke-virtual {p0, v0, v1, v2}, Lcom/osp/app/signin/ca;->a(JLjava/lang/String;)V

    .line 4026
    iget-wide v0, p0, Lcom/osp/app/signin/ca;->e:J

    sget-object v2, Lcom/msc/b/g;->b:Lcom/msc/b/g;

    invoke-static {v0, v1, v2}, Lcom/msc/c/g;->a(JLcom/msc/b/g;)V

    .line 4027
    return-void
.end method

.method private h()V
    .locals 4

    .prologue
    .line 4042
    invoke-static {}, Lcom/msc/c/g;->a()Lcom/msc/c/g;

    .line 4043
    iget-object v0, p0, Lcom/osp/app/signin/ca;->c:Lcom/osp/app/signin/AccountinfoView;

    const-string v1, "j5p7ll8g33"

    iget-object v2, p0, Lcom/osp/app/signin/ca;->c:Lcom/osp/app/signin/AccountinfoView;

    invoke-static {v2}, Lcom/msc/c/e;->g(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x0

    invoke-static {v0, v1, v2, v3, p0}, Lcom/msc/c/g;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/msc/b/h;)J

    move-result-wide v0

    iput-wide v0, p0, Lcom/osp/app/signin/ca;->g:J

    .line 4044
    iget-wide v0, p0, Lcom/osp/app/signin/ca;->g:J

    invoke-virtual {p0, v0, v1}, Lcom/osp/app/signin/ca;->a(J)V

    .line 4045
    iget-wide v0, p0, Lcom/osp/app/signin/ca;->g:J

    const-string v2, "from_json"

    invoke-virtual {p0, v0, v1, v2}, Lcom/osp/app/signin/ca;->a(JLjava/lang/String;)V

    .line 4046
    iget-wide v0, p0, Lcom/osp/app/signin/ca;->g:J

    sget-object v2, Lcom/msc/b/g;->b:Lcom/msc/b/g;

    invoke-static {v0, v1, v2}, Lcom/msc/c/g;->a(JLcom/msc/b/g;)V

    .line 4047
    return-void
.end method


# virtual methods
.method protected final varargs a()Ljava/lang/Boolean;
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 3828
    const/4 v0, 0x1

    .line 3829
    const-string v2, "ACCOUNTINFO_MODIFY"

    iget-object v3, p0, Lcom/osp/app/signin/ca;->c:Lcom/osp/app/signin/AccountinfoView;

    invoke-static {v3}, Lcom/osp/app/signin/AccountinfoView;->p(Lcom/osp/app/signin/AccountinfoView;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    move v0, v1

    .line 3833
    :cond_0
    iget-object v2, p0, Lcom/osp/app/signin/ca;->c:Lcom/osp/app/signin/AccountinfoView;

    invoke-static {v2}, Lcom/osp/app/signin/AccountinfoView;->q(Lcom/osp/app/signin/AccountinfoView;)Ljava/lang/String;

    move-result-object v2

    invoke-static {}, Lcom/msc/c/g;->a()Lcom/msc/c/g;

    iget-object v3, p0, Lcom/osp/app/signin/ca;->c:Lcom/osp/app/signin/AccountinfoView;

    invoke-static {v3, v2, v0, p0}, Lcom/msc/c/g;->a(Landroid/content/Context;Ljava/lang/String;ZLcom/msc/b/h;)J

    move-result-wide v2

    iput-wide v2, p0, Lcom/osp/app/signin/ca;->d:J

    iget-wide v2, p0, Lcom/osp/app/signin/ca;->d:J

    invoke-virtual {p0, v2, v3}, Lcom/osp/app/signin/ca;->a(J)V

    iget-wide v2, p0, Lcom/osp/app/signin/ca;->d:J

    const-string v0, "from_xml"

    invoke-virtual {p0, v2, v3, v0}, Lcom/osp/app/signin/ca;->a(JLjava/lang/String;)V

    iget-wide v2, p0, Lcom/osp/app/signin/ca;->d:J

    sget-object v0, Lcom/msc/b/g;->c:Lcom/msc/b/g;

    invoke-static {v2, v3, v0}, Lcom/msc/c/g;->a(JLcom/msc/b/g;)V

    .line 3834
    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    return-object v0
.end method

.method public final declared-synchronized a(Lcom/msc/b/c;)V
    .locals 6

    .prologue
    .line 3871
    monitor-enter p0

    :try_start_0
    invoke-super {p0, p1}, Lcom/msc/c/b;->a(Lcom/msc/b/c;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 3873
    if-nez p1, :cond_1

    .line 3964
    :cond_0
    :goto_0
    monitor-exit p0

    return-void

    .line 3878
    :cond_1
    :try_start_1
    invoke-virtual {p1}, Lcom/msc/b/c;->a()J

    move-result-wide v0

    .line 3879
    invoke-virtual {p1}, Lcom/msc/b/c;->e()Ljava/lang/String;

    move-result-object v2

    .line 3881
    iget-wide v4, p0, Lcom/osp/app/signin/ca;->d:J
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    cmp-long v3, v0, v4

    if-nez v3, :cond_3

    .line 3885
    :try_start_2
    iget-object v0, p0, Lcom/osp/app/signin/ca;->c:Lcom/osp/app/signin/AccountinfoView;

    invoke-static {}, Lcom/msc/c/h;->a()Lcom/msc/c/h;

    invoke-static {v2}, Lcom/msc/c/h;->j(Ljava/lang/String;)Lcom/osp/app/signin/kd;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/osp/app/signin/AccountinfoView;->a(Lcom/osp/app/signin/AccountinfoView;Lcom/osp/app/signin/kd;)Lcom/osp/app/signin/kd;

    .line 3886
    iget-object v0, p0, Lcom/osp/app/signin/ca;->c:Lcom/osp/app/signin/AccountinfoView;

    invoke-static {v0}, Lcom/msc/c/e;->b(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v0

    .line 3887
    if-eqz v0, :cond_2

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v1

    if-lez v1, :cond_2

    .line 3889
    invoke-direct {p0, v0}, Lcom/osp/app/signin/ca;->b(Ljava/lang/String;)V
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_0
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0

    .line 3894
    :catch_0
    move-exception v0

    .line 3896
    :try_start_3
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    .line 3897
    new-instance v1, Lcom/msc/c/f;

    invoke-direct {v1, v0}, Lcom/msc/c/f;-><init>(Ljava/lang/Exception;)V

    iput-object v1, p0, Lcom/osp/app/signin/ca;->b:Lcom/msc/c/f;
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    goto :goto_0

    .line 3871
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 3892
    :cond_2
    :try_start_4
    invoke-direct {p0}, Lcom/osp/app/signin/ca;->h()V
    :try_end_4
    .catch Ljava/lang/Exception; {:try_start_4 .. :try_end_4} :catch_0
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    goto :goto_0

    .line 3899
    :cond_3
    :try_start_5
    iget-wide v4, p0, Lcom/osp/app/signin/ca;->e:J
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    cmp-long v3, v0, v4

    if-nez v3, :cond_4

    .line 3903
    :try_start_6
    iget-object v0, p0, Lcom/osp/app/signin/ca;->c:Lcom/osp/app/signin/AccountinfoView;

    invoke-static {}, Lcom/msc/c/h;->a()Lcom/msc/c/h;

    invoke-static {v2}, Lcom/msc/c/h;->k(Ljava/lang/String;)Lcom/osp/app/signin/SignUpinfo;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/osp/app/signin/AccountinfoView;->a(Lcom/osp/app/signin/AccountinfoView;Lcom/osp/app/signin/SignUpinfo;)Lcom/osp/app/signin/SignUpinfo;
    :try_end_6
    .catch Ljava/lang/Exception; {:try_start_6 .. :try_end_6} :catch_1
    .catchall {:try_start_6 .. :try_end_6} :catchall_0

    goto :goto_0

    .line 3904
    :catch_1
    move-exception v0

    .line 3906
    :try_start_7
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    .line 3907
    new-instance v1, Lcom/msc/c/f;

    invoke-direct {v1, v0}, Lcom/msc/c/f;-><init>(Ljava/lang/Exception;)V

    iput-object v1, p0, Lcom/osp/app/signin/ca;->b:Lcom/msc/c/f;

    goto :goto_0

    .line 3909
    :cond_4
    iget-wide v4, p0, Lcom/osp/app/signin/ca;->h:J
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_0

    cmp-long v3, v0, v4

    if-nez v3, :cond_6

    .line 3913
    :try_start_8
    invoke-static {}, Lcom/msc/c/h;->a()Lcom/msc/c/h;

    invoke-static {v2}, Lcom/msc/c/h;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 3914
    if-eqz v0, :cond_5

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v1

    if-lez v1, :cond_5

    .line 3916
    iget-object v1, p0, Lcom/osp/app/signin/ca;->c:Lcom/osp/app/signin/AccountinfoView;

    invoke-static {v1, v0}, Lcom/msc/c/e;->e(Landroid/content/Context;Ljava/lang/String;)V

    .line 3917
    invoke-virtual {p0}, Lcom/osp/app/signin/ca;->isCancelled()Z

    move-result v0

    if-nez v0, :cond_0

    .line 3921
    invoke-direct {p0}, Lcom/osp/app/signin/ca;->h()V
    :try_end_8
    .catch Ljava/lang/Exception; {:try_start_8 .. :try_end_8} :catch_2
    .catchall {:try_start_8 .. :try_end_8} :catchall_0

    goto/16 :goto_0

    .line 3927
    :catch_2
    move-exception v0

    .line 3929
    :try_start_9
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    .line 3930
    new-instance v1, Lcom/msc/c/f;

    invoke-direct {v1, v0}, Lcom/msc/c/f;-><init>(Ljava/lang/Exception;)V

    iput-object v1, p0, Lcom/osp/app/signin/ca;->b:Lcom/msc/c/f;
    :try_end_9
    .catchall {:try_start_9 .. :try_end_9} :catchall_0

    goto/16 :goto_0

    .line 3925
    :cond_5
    :try_start_a
    new-instance v0, Lcom/msc/c/f;

    invoke-direct {v0}, Lcom/msc/c/f;-><init>()V

    iput-object v0, p0, Lcom/osp/app/signin/ca;->b:Lcom/msc/c/f;
    :try_end_a
    .catch Ljava/lang/Exception; {:try_start_a .. :try_end_a} :catch_2
    .catchall {:try_start_a .. :try_end_a} :catchall_0

    goto/16 :goto_0

    .line 3932
    :cond_6
    :try_start_b
    iget-wide v4, p0, Lcom/osp/app/signin/ca;->g:J
    :try_end_b
    .catchall {:try_start_b .. :try_end_b} :catchall_0

    cmp-long v3, v0, v4

    if-nez v3, :cond_7

    .line 3936
    :try_start_c
    invoke-static {}, Lcom/msc/c/h;->a()Lcom/msc/c/h;

    invoke-static {v2}, Lcom/msc/c/h;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 3937
    invoke-virtual {p0}, Lcom/osp/app/signin/ca;->isCancelled()Z

    move-result v0

    if-nez v0, :cond_0

    .line 3941
    invoke-static {}, Lcom/msc/c/g;->a()Lcom/msc/c/g;

    iget-object v0, p0, Lcom/osp/app/signin/ca;->c:Lcom/osp/app/signin/AccountinfoView;

    const-string v1, "authorization_code"

    const-string v3, "j5p7ll8g33"

    const-string v4, "5763D0052DC1462E13751F753384E9A9"

    move-object v5, p0

    invoke-static/range {v0 .. v5}, Lcom/msc/c/g;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/msc/b/h;)J

    move-result-wide v0

    iput-wide v0, p0, Lcom/osp/app/signin/ca;->f:J

    iget-wide v0, p0, Lcom/osp/app/signin/ca;->f:J

    invoke-virtual {p0, v0, v1}, Lcom/osp/app/signin/ca;->a(J)V

    iget-wide v0, p0, Lcom/osp/app/signin/ca;->f:J

    const-string v2, "from_json"

    invoke-virtual {p0, v0, v1, v2}, Lcom/osp/app/signin/ca;->a(JLjava/lang/String;)V

    iget-wide v0, p0, Lcom/osp/app/signin/ca;->f:J

    sget-object v2, Lcom/msc/b/g;->c:Lcom/msc/b/g;

    invoke-static {v0, v1, v2}, Lcom/msc/c/g;->a(JLcom/msc/b/g;)V
    :try_end_c
    .catch Ljava/lang/Exception; {:try_start_c .. :try_end_c} :catch_3
    .catchall {:try_start_c .. :try_end_c} :catchall_0

    goto/16 :goto_0

    .line 3942
    :catch_3
    move-exception v0

    .line 3944
    :try_start_d
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    .line 3945
    new-instance v1, Lcom/msc/c/f;

    invoke-direct {v1, v0}, Lcom/msc/c/f;-><init>(Ljava/lang/Exception;)V

    iput-object v1, p0, Lcom/osp/app/signin/ca;->b:Lcom/msc/c/f;

    goto/16 :goto_0

    .line 3947
    :cond_7
    iget-wide v4, p0, Lcom/osp/app/signin/ca;->f:J
    :try_end_d
    .catchall {:try_start_d .. :try_end_d} :catchall_0

    cmp-long v0, v0, v4

    if-nez v0, :cond_0

    .line 3951
    :try_start_e
    invoke-static {}, Lcom/msc/c/h;->a()Lcom/msc/c/h;

    iget-object v0, p0, Lcom/osp/app/signin/ca;->c:Lcom/osp/app/signin/AccountinfoView;

    invoke-static {v0, v2}, Lcom/msc/c/h;->a(Landroid/content/Context;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 3952
    iget-object v1, p0, Lcom/osp/app/signin/ca;->c:Lcom/osp/app/signin/AccountinfoView;

    invoke-static {v1, v0}, Lcom/msc/c/e;->b(Landroid/content/Context;Ljava/lang/String;)V

    .line 3953
    invoke-virtual {p0}, Lcom/osp/app/signin/ca;->isCancelled()Z

    move-result v1

    if-nez v1, :cond_0

    .line 3957
    invoke-direct {p0, v0}, Lcom/osp/app/signin/ca;->b(Ljava/lang/String;)V
    :try_end_e
    .catch Ljava/lang/Exception; {:try_start_e .. :try_end_e} :catch_4
    .catchall {:try_start_e .. :try_end_e} :catchall_0

    goto/16 :goto_0

    .line 3958
    :catch_4
    move-exception v0

    .line 3960
    :try_start_f
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    .line 3961
    new-instance v1, Lcom/msc/c/f;

    invoke-direct {v1, v0}, Lcom/msc/c/f;-><init>(Ljava/lang/Exception;)V

    iput-object v1, p0, Lcom/osp/app/signin/ca;->b:Lcom/msc/c/f;
    :try_end_f
    .catchall {:try_start_f .. :try_end_f} :catchall_0

    goto/16 :goto_0
.end method

.method protected final a(Ljava/lang/Boolean;)V
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 3839
    invoke-super {p0, p1}, Lcom/msc/c/b;->a(Ljava/lang/Boolean;)V

    .line 3840
    iget-object v0, p0, Lcom/osp/app/signin/ca;->b:Lcom/msc/c/f;

    if-eqz v0, :cond_3

    .line 3842
    const-string v0, "AUT_1302"

    iget-object v1, p0, Lcom/osp/app/signin/ca;->b:Lcom/msc/c/f;

    invoke-virtual {v1}, Lcom/msc/c/f;->a()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    const-string v0, "AUT_1830"

    iget-object v1, p0, Lcom/osp/app/signin/ca;->b:Lcom/msc/c/f;

    invoke-virtual {v1}, Lcom/msc/c/f;->a()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 3844
    :cond_0
    iget-object v0, p0, Lcom/osp/app/signin/ca;->c:Lcom/osp/app/signin/AccountinfoView;

    invoke-static {v0}, Lcom/msc/c/c;->g(Landroid/content/Context;)V

    .line 3845
    iget-object v0, p0, Lcom/osp/app/signin/ca;->c:Lcom/osp/app/signin/AccountinfoView;

    invoke-static {v0}, Lcom/msc/c/e;->a(Landroid/content/Context;)Z

    .line 3846
    iget-object v0, p0, Lcom/osp/app/signin/ca;->c:Lcom/osp/app/signin/AccountinfoView;

    iget-object v1, p0, Lcom/osp/app/signin/ca;->c:Lcom/osp/app/signin/AccountinfoView;

    invoke-static {v1}, Lcom/osp/app/util/ad;->h(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/osp/app/signin/AccountinfoView;->a(Lcom/osp/app/signin/AccountinfoView;Ljava/lang/String;)V

    .line 3854
    :goto_0
    iget-object v0, p0, Lcom/osp/app/signin/ca;->c:Lcom/osp/app/signin/AccountinfoView;

    iget-object v1, p0, Lcom/osp/app/signin/ca;->c:Lcom/osp/app/signin/AccountinfoView;

    invoke-static {v1}, Lcom/osp/app/signin/AccountinfoView;->l(Lcom/osp/app/signin/AccountinfoView;)Landroid/content/Intent;

    move-result-object v1

    invoke-virtual {v0, v3, v1}, Lcom/osp/app/signin/AccountinfoView;->a(ILandroid/content/Intent;)V

    .line 3855
    iget-object v0, p0, Lcom/osp/app/signin/ca;->c:Lcom/osp/app/signin/AccountinfoView;

    invoke-virtual {v0}, Lcom/osp/app/signin/AccountinfoView;->finish()V

    .line 3867
    :goto_1
    return-void

    .line 3847
    :cond_1
    invoke-static {}, Lcom/osp/device/b;->b()Lcom/osp/device/b;

    move-result-object v0

    invoke-virtual {v0}, Lcom/osp/device/b;->a()Z

    move-result v0

    if-eqz v0, :cond_2

    const-string v0, "AUT_1094"

    iget-object v1, p0, Lcom/osp/app/signin/ca;->b:Lcom/msc/c/f;

    invoke-virtual {v1}, Lcom/msc/c/f;->a()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 3849
    iget-object v0, p0, Lcom/osp/app/signin/ca;->c:Lcom/osp/app/signin/AccountinfoView;

    invoke-static {v0}, Lcom/osp/app/signin/AccountinfoView;->r(Lcom/osp/app/signin/AccountinfoView;)V

    goto :goto_0

    .line 3852
    :cond_2
    invoke-virtual {p0, v2}, Lcom/osp/app/signin/ca;->a(Z)V

    goto :goto_0

    .line 3858
    :cond_3
    iget-object v0, p0, Lcom/osp/app/signin/ca;->c:Lcom/osp/app/signin/AccountinfoView;

    invoke-static {v0}, Lcom/osp/app/signin/AccountinfoView;->s(Lcom/osp/app/signin/AccountinfoView;)Lcom/osp/app/signin/kd;

    move-result-object v0

    if-eqz v0, :cond_4

    iget-object v0, p0, Lcom/osp/app/signin/ca;->c:Lcom/osp/app/signin/AccountinfoView;

    invoke-static {v0}, Lcom/osp/app/signin/AccountinfoView;->j(Lcom/osp/app/signin/AccountinfoView;)Lcom/osp/app/signin/SignUpinfo;

    move-result-object v0

    if-eqz v0, :cond_4

    .line 3860
    iget-object v0, p0, Lcom/osp/app/signin/ca;->c:Lcom/osp/app/signin/AccountinfoView;

    invoke-static {v0}, Lcom/osp/app/signin/AccountinfoView;->t(Lcom/osp/app/signin/AccountinfoView;)V

    goto :goto_1

    .line 3863
    :cond_4
    invoke-virtual {p0, v2}, Lcom/osp/app/signin/ca;->a(Z)V

    .line 3864
    iget-object v0, p0, Lcom/osp/app/signin/ca;->c:Lcom/osp/app/signin/AccountinfoView;

    iget-object v1, p0, Lcom/osp/app/signin/ca;->c:Lcom/osp/app/signin/AccountinfoView;

    invoke-static {v1}, Lcom/osp/app/signin/AccountinfoView;->l(Lcom/osp/app/signin/AccountinfoView;)Landroid/content/Intent;

    move-result-object v1

    invoke-virtual {v0, v3, v1}, Lcom/osp/app/signin/AccountinfoView;->a(ILandroid/content/Intent;)V

    .line 3865
    iget-object v0, p0, Lcom/osp/app/signin/ca;->c:Lcom/osp/app/signin/AccountinfoView;

    invoke-virtual {v0}, Lcom/osp/app/signin/AccountinfoView;->finish()V

    goto :goto_1
.end method

.method public final b(Lcom/msc/b/c;)V
    .locals 6

    .prologue
    .line 3968
    invoke-super {p0, p1}, Lcom/msc/c/b;->b(Lcom/msc/b/c;)V

    .line 3970
    if-nez p1, :cond_1

    .line 4011
    :cond_0
    :goto_0
    return-void

    .line 3975
    :cond_1
    invoke-virtual {p1}, Lcom/msc/b/c;->a()J

    move-result-wide v0

    .line 3976
    invoke-virtual {p1}, Lcom/msc/b/c;->e()Ljava/lang/String;

    move-result-object v2

    .line 3981
    iget-wide v4, p0, Lcom/osp/app/signin/ca;->e:J

    cmp-long v0, v0, v4

    if-nez v0, :cond_0

    .line 3983
    if-eqz v2, :cond_0

    .line 3985
    iget-boolean v0, p0, Lcom/osp/app/signin/ca;->i:Z

    if-nez v0, :cond_0

    const-string v0, "ACF_0403"

    iget-object v1, p0, Lcom/osp/app/signin/ca;->b:Lcom/msc/c/f;

    invoke-virtual {v1}, Lcom/msc/c/f;->a()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 3989
    const/4 v0, 0x0

    :try_start_0
    iput-object v0, p0, Lcom/osp/app/signin/ca;->b:Lcom/msc/c/f;

    .line 3990
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/osp/app/signin/ca;->i:Z

    .line 3991
    iget-object v0, p0, Lcom/osp/app/signin/ca;->c:Lcom/osp/app/signin/AccountinfoView;

    const-string v1, ""

    invoke-static {v0, v1}, Lcom/msc/c/e;->b(Landroid/content/Context;Ljava/lang/String;)V

    .line 3992
    invoke-virtual {p0}, Lcom/osp/app/signin/ca;->isCancelled()Z

    move-result v0

    if-nez v0, :cond_0

    .line 3996
    invoke-direct {p0}, Lcom/osp/app/signin/ca;->h()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 3997
    :catch_0
    move-exception v0

    .line 3999
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    .line 4000
    new-instance v1, Lcom/msc/c/f;

    invoke-direct {v1, v0}, Lcom/msc/c/f;-><init>(Ljava/lang/Exception;)V

    iput-object v1, p0, Lcom/osp/app/signin/ca;->b:Lcom/msc/c/f;

    goto :goto_0
.end method

.method protected final d()V
    .locals 2

    .prologue
    .line 3821
    invoke-super {p0}, Lcom/msc/c/b;->d()V

    .line 3822
    iget-object v0, p0, Lcom/osp/app/signin/ca;->c:Lcom/osp/app/signin/AccountinfoView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/osp/app/signin/AccountinfoView;->b(I)V

    .line 3823
    iget-object v0, p0, Lcom/osp/app/signin/ca;->c:Lcom/osp/app/signin/AccountinfoView;

    invoke-virtual {v0}, Lcom/osp/app/signin/AccountinfoView;->finish()V

    .line 3824
    return-void
.end method

.method protected final synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 3803
    invoke-virtual {p0}, Lcom/osp/app/signin/ca;->a()Ljava/lang/Boolean;

    move-result-object v0

    return-object v0
.end method

.method protected final synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 3803
    check-cast p1, Ljava/lang/Boolean;

    invoke-virtual {p0, p1}, Lcom/osp/app/signin/ca;->a(Ljava/lang/Boolean;)V

    return-void
.end method
