.class final Lcom/osp/app/signin/cc;
.super Lcom/msc/c/b;
.source "AccountinfoView.java"


# instance fields
.field final synthetic c:Lcom/osp/app/signin/AccountinfoView;

.field private d:Z

.field private e:J

.field private f:J

.field private g:J

.field private h:J

.field private i:Z

.field private j:Lcom/osp/app/signin/SignUpinfo;


# direct methods
.method public constructor <init>(Lcom/osp/app/signin/AccountinfoView;)V
    .locals 1

    .prologue
    .line 4302
    iput-object p1, p0, Lcom/osp/app/signin/cc;->c:Lcom/osp/app/signin/AccountinfoView;

    .line 4303
    invoke-direct {p0, p1}, Lcom/msc/c/b;-><init>(Landroid/content/Context;)V

    .line 4299
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/osp/app/signin/cc;->i:Z

    .line 4300
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/osp/app/signin/cc;->j:Lcom/osp/app/signin/SignUpinfo;

    .line 4307
    return-void
.end method

.method private b(Ljava/lang/String;)V
    .locals 3

    .prologue
    .line 4595
    iget-object v0, p0, Lcom/osp/app/signin/cc;->j:Lcom/osp/app/signin/SignUpinfo;

    if-nez v0, :cond_0

    .line 4597
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v0, "AIV"

    const-string v1, "ModifyAccountInfoV02Task: reload info."

    invoke-static {v0, v1}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 4598
    iget-object v0, p0, Lcom/osp/app/signin/cc;->c:Lcom/osp/app/signin/AccountinfoView;

    invoke-static {v0}, Lcom/osp/app/signin/AccountinfoView;->v(Lcom/osp/app/signin/AccountinfoView;)Lcom/osp/app/signin/SignUpinfo;

    move-result-object v0

    iput-object v0, p0, Lcom/osp/app/signin/cc;->j:Lcom/osp/app/signin/SignUpinfo;

    .line 4600
    :cond_0
    invoke-static {}, Lcom/msc/c/g;->a()Lcom/msc/c/g;

    .line 4601
    iget-object v0, p0, Lcom/osp/app/signin/cc;->c:Lcom/osp/app/signin/AccountinfoView;

    iget-object v1, p0, Lcom/osp/app/signin/cc;->c:Lcom/osp/app/signin/AccountinfoView;

    invoke-static {v1}, Lcom/msc/c/e;->h(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/osp/app/signin/cc;->j:Lcom/osp/app/signin/SignUpinfo;

    invoke-static {v0, p1, v1, v2, p0}, Lcom/msc/c/g;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Lcom/osp/app/signin/SignUpinfo;Lcom/msc/b/h;)J

    move-result-wide v0

    iput-wide v0, p0, Lcom/osp/app/signin/cc;->h:J

    .line 4602
    iget-wide v0, p0, Lcom/osp/app/signin/cc;->h:J

    invoke-virtual {p0, v0, v1}, Lcom/osp/app/signin/cc;->a(J)V

    .line 4603
    iget-wide v0, p0, Lcom/osp/app/signin/cc;->h:J

    const-string v2, "from_xml"

    invoke-virtual {p0, v0, v1, v2}, Lcom/osp/app/signin/cc;->a(JLjava/lang/String;)V

    .line 4604
    iget-wide v0, p0, Lcom/osp/app/signin/cc;->h:J

    sget-object v2, Lcom/msc/b/g;->d:Lcom/msc/b/g;

    invoke-static {v0, v1, v2}, Lcom/msc/c/g;->a(JLcom/msc/b/g;)V

    .line 4605
    return-void
.end method

.method private h()V
    .locals 4

    .prologue
    .line 4579
    invoke-static {}, Lcom/msc/c/g;->a()Lcom/msc/c/g;

    .line 4580
    iget-object v0, p0, Lcom/osp/app/signin/cc;->c:Lcom/osp/app/signin/AccountinfoView;

    const-string v1, "j5p7ll8g33"

    iget-object v2, p0, Lcom/osp/app/signin/cc;->c:Lcom/osp/app/signin/AccountinfoView;

    invoke-static {v2}, Lcom/msc/c/e;->g(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x0

    invoke-static {v0, v1, v2, v3, p0}, Lcom/msc/c/g;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/msc/b/h;)J

    move-result-wide v0

    iput-wide v0, p0, Lcom/osp/app/signin/cc;->f:J

    .line 4581
    iget-wide v0, p0, Lcom/osp/app/signin/cc;->f:J

    invoke-virtual {p0, v0, v1}, Lcom/osp/app/signin/cc;->a(J)V

    .line 4582
    iget-wide v0, p0, Lcom/osp/app/signin/cc;->f:J

    const-string v2, "from_json"

    invoke-virtual {p0, v0, v1, v2}, Lcom/osp/app/signin/cc;->a(JLjava/lang/String;)V

    .line 4583
    iget-wide v0, p0, Lcom/osp/app/signin/cc;->f:J

    sget-object v2, Lcom/msc/b/g;->b:Lcom/msc/b/g;

    invoke-static {v0, v1, v2}, Lcom/msc/c/g;->a(JLcom/msc/b/g;)V

    .line 4584
    return-void
.end method


# virtual methods
.method protected final varargs a()Ljava/lang/Boolean;
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 4325
    iget-object v0, p0, Lcom/osp/app/signin/cc;->c:Lcom/osp/app/signin/AccountinfoView;

    invoke-static {v0}, Lcom/msc/c/e;->b(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v0

    .line 4326
    if-eqz v0, :cond_0

    .line 4328
    iput-boolean v1, p0, Lcom/osp/app/signin/cc;->i:Z

    .line 4329
    invoke-direct {p0, v0}, Lcom/osp/app/signin/cc;->b(Ljava/lang/String;)V

    .line 4335
    :goto_0
    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    return-object v0

    .line 4332
    :cond_0
    iput-boolean v1, p0, Lcom/osp/app/signin/cc;->i:Z

    .line 4333
    invoke-direct {p0}, Lcom/osp/app/signin/cc;->h()V

    goto :goto_0
.end method

.method public final declared-synchronized a(Lcom/msc/b/c;)V
    .locals 6

    .prologue
    .line 4445
    monitor-enter p0

    :try_start_0
    invoke-super {p0, p1}, Lcom/msc/c/b;->a(Lcom/msc/b/c;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 4447
    if-nez p1, :cond_1

    .line 4520
    :cond_0
    :goto_0
    monitor-exit p0

    return-void

    .line 4452
    :cond_1
    :try_start_1
    invoke-virtual {p1}, Lcom/msc/b/c;->a()J

    move-result-wide v0

    .line 4453
    invoke-virtual {p1}, Lcom/msc/b/c;->e()Ljava/lang/String;

    move-result-object v2

    .line 4455
    iget-wide v4, p0, Lcom/osp/app/signin/cc;->e:J
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    cmp-long v3, v0, v4

    if-nez v3, :cond_3

    .line 4459
    :try_start_2
    invoke-static {}, Lcom/msc/c/h;->a()Lcom/msc/c/h;

    invoke-static {v2}, Lcom/msc/c/h;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 4460
    if-eqz v0, :cond_2

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v1

    if-lez v1, :cond_2

    .line 4462
    iget-object v1, p0, Lcom/osp/app/signin/cc;->c:Lcom/osp/app/signin/AccountinfoView;

    invoke-static {v1, v0}, Lcom/msc/c/e;->e(Landroid/content/Context;Ljava/lang/String;)V

    .line 4463
    invoke-virtual {p0}, Lcom/osp/app/signin/cc;->isCancelled()Z

    move-result v0

    if-nez v0, :cond_0

    .line 4467
    invoke-direct {p0}, Lcom/osp/app/signin/cc;->h()V
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_0
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0

    .line 4473
    :catch_0
    move-exception v0

    .line 4475
    :try_start_3
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    .line 4476
    new-instance v1, Lcom/msc/c/f;

    invoke-direct {v1, v0}, Lcom/msc/c/f;-><init>(Ljava/lang/Exception;)V

    iput-object v1, p0, Lcom/osp/app/signin/cc;->b:Lcom/msc/c/f;
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    goto :goto_0

    .line 4445
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 4471
    :cond_2
    :try_start_4
    new-instance v0, Lcom/msc/c/f;

    invoke-direct {v0}, Lcom/msc/c/f;-><init>()V

    iput-object v0, p0, Lcom/osp/app/signin/cc;->b:Lcom/msc/c/f;
    :try_end_4
    .catch Ljava/lang/Exception; {:try_start_4 .. :try_end_4} :catch_0
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    goto :goto_0

    .line 4478
    :cond_3
    :try_start_5
    iget-wide v4, p0, Lcom/osp/app/signin/cc;->f:J
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    cmp-long v3, v0, v4

    if-nez v3, :cond_4

    .line 4482
    :try_start_6
    invoke-static {}, Lcom/msc/c/h;->a()Lcom/msc/c/h;

    invoke-static {v2}, Lcom/msc/c/h;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 4483
    invoke-virtual {p0}, Lcom/osp/app/signin/cc;->isCancelled()Z

    move-result v0

    if-nez v0, :cond_0

    .line 4487
    invoke-static {}, Lcom/msc/c/g;->a()Lcom/msc/c/g;

    iget-object v0, p0, Lcom/osp/app/signin/cc;->c:Lcom/osp/app/signin/AccountinfoView;

    const-string v1, "authorization_code"

    const-string v3, "j5p7ll8g33"

    const-string v4, "5763D0052DC1462E13751F753384E9A9"

    move-object v5, p0

    invoke-static/range {v0 .. v5}, Lcom/msc/c/g;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/msc/b/h;)J

    move-result-wide v0

    iput-wide v0, p0, Lcom/osp/app/signin/cc;->g:J

    iget-wide v0, p0, Lcom/osp/app/signin/cc;->g:J

    invoke-virtual {p0, v0, v1}, Lcom/osp/app/signin/cc;->a(J)V

    iget-wide v0, p0, Lcom/osp/app/signin/cc;->g:J

    const-string v2, "from_json"

    invoke-virtual {p0, v0, v1, v2}, Lcom/osp/app/signin/cc;->a(JLjava/lang/String;)V

    iget-wide v0, p0, Lcom/osp/app/signin/cc;->g:J

    sget-object v2, Lcom/msc/b/g;->c:Lcom/msc/b/g;

    invoke-static {v0, v1, v2}, Lcom/msc/c/g;->a(JLcom/msc/b/g;)V
    :try_end_6
    .catch Ljava/lang/Exception; {:try_start_6 .. :try_end_6} :catch_1
    .catchall {:try_start_6 .. :try_end_6} :catchall_0

    goto :goto_0

    .line 4488
    :catch_1
    move-exception v0

    .line 4490
    :try_start_7
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    .line 4491
    new-instance v1, Lcom/msc/c/f;

    invoke-direct {v1, v0}, Lcom/msc/c/f;-><init>(Ljava/lang/Exception;)V

    iput-object v1, p0, Lcom/osp/app/signin/cc;->b:Lcom/msc/c/f;

    goto/16 :goto_0

    .line 4493
    :cond_4
    iget-wide v4, p0, Lcom/osp/app/signin/cc;->g:J
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_0

    cmp-long v3, v0, v4

    if-nez v3, :cond_5

    .line 4497
    :try_start_8
    invoke-static {}, Lcom/msc/c/h;->a()Lcom/msc/c/h;

    iget-object v0, p0, Lcom/osp/app/signin/cc;->c:Lcom/osp/app/signin/AccountinfoView;

    invoke-static {v0, v2}, Lcom/msc/c/h;->a(Landroid/content/Context;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 4498
    iget-object v1, p0, Lcom/osp/app/signin/cc;->c:Lcom/osp/app/signin/AccountinfoView;

    invoke-static {v1, v0}, Lcom/msc/c/e;->b(Landroid/content/Context;Ljava/lang/String;)V

    .line 4499
    invoke-virtual {p0}, Lcom/osp/app/signin/cc;->isCancelled()Z

    move-result v1

    if-nez v1, :cond_0

    .line 4503
    invoke-direct {p0, v0}, Lcom/osp/app/signin/cc;->b(Ljava/lang/String;)V
    :try_end_8
    .catch Ljava/lang/Exception; {:try_start_8 .. :try_end_8} :catch_2
    .catchall {:try_start_8 .. :try_end_8} :catchall_0

    goto/16 :goto_0

    .line 4504
    :catch_2
    move-exception v0

    .line 4506
    :try_start_9
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    .line 4507
    new-instance v1, Lcom/msc/c/f;

    invoke-direct {v1, v0}, Lcom/msc/c/f;-><init>(Ljava/lang/Exception;)V

    iput-object v1, p0, Lcom/osp/app/signin/cc;->b:Lcom/msc/c/f;

    goto/16 :goto_0

    .line 4509
    :cond_5
    iget-wide v4, p0, Lcom/osp/app/signin/cc;->h:J
    :try_end_9
    .catchall {:try_start_9 .. :try_end_9} :catchall_0

    cmp-long v0, v0, v4

    if-nez v0, :cond_0

    .line 4513
    :try_start_a
    invoke-static {}, Lcom/msc/c/h;->a()Lcom/msc/c/h;

    invoke-static {v2}, Lcom/msc/c/h;->o(Ljava/lang/String;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/osp/app/signin/cc;->d:Z
    :try_end_a
    .catch Ljava/lang/Exception; {:try_start_a .. :try_end_a} :catch_3
    .catchall {:try_start_a .. :try_end_a} :catchall_0

    goto/16 :goto_0

    .line 4514
    :catch_3
    move-exception v0

    :try_start_b
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    .line 4517
    new-instance v0, Lcom/msc/c/f;

    invoke-direct {v0}, Lcom/msc/c/f;-><init>()V

    iput-object v0, p0, Lcom/osp/app/signin/cc;->b:Lcom/msc/c/f;
    :try_end_b
    .catchall {:try_start_b .. :try_end_b} :catchall_0

    goto/16 :goto_0
.end method

.method protected final a(Ljava/lang/Boolean;)V
    .locals 9

    .prologue
    const/4 v7, 0x0

    const/4 v8, 0x1

    const/4 v6, 0x0

    .line 4340
    invoke-super {p0, p1}, Lcom/msc/c/b;->a(Ljava/lang/Boolean;)V

    .line 4341
    iget-object v0, p0, Lcom/osp/app/signin/cc;->b:Lcom/msc/c/f;

    if-eqz v0, :cond_6

    .line 4343
    iget-object v0, p0, Lcom/osp/app/signin/cc;->b:Lcom/msc/c/f;

    invoke-virtual {v0}, Lcom/msc/c/f;->a()Ljava/lang/String;

    move-result-object v0

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_5

    iget-object v0, p0, Lcom/osp/app/signin/cc;->b:Lcom/msc/c/f;

    invoke-virtual {v0}, Lcom/msc/c/f;->a()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v0

    if-lez v0, :cond_5

    .line 4345
    const-string v0, "USR_3117"

    iget-object v1, p0, Lcom/osp/app/signin/cc;->b:Lcom/msc/c/f;

    invoke-virtual {v1}, Lcom/msc/c/f;->a()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    const-string v0, "PRT_3011"

    iget-object v1, p0, Lcom/osp/app/signin/cc;->b:Lcom/msc/c/f;

    invoke-virtual {v1}, Lcom/msc/c/f;->a()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 4347
    :cond_0
    iget-object v0, p0, Lcom/osp/app/signin/cc;->c:Lcom/osp/app/signin/AccountinfoView;

    invoke-static {v0}, Lcom/osp/app/signin/AccountinfoView;->w(Lcom/osp/app/signin/AccountinfoView;)Lcom/osp/app/util/n;

    iget-object v0, p0, Lcom/osp/app/signin/cc;->b:Lcom/msc/c/f;

    invoke-virtual {v0}, Lcom/msc/c/f;->b()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/osp/app/util/n;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 4348
    invoke-static {v0}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v0

    .line 4349
    invoke-static {}, Lcom/osp/app/util/ap;->a()Lcom/osp/app/util/ap;

    iget-object v1, p0, Lcom/osp/app/signin/cc;->c:Lcom/osp/app/signin/AccountinfoView;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v3, p0, Lcom/osp/app/signin/cc;->c:Lcom/osp/app/signin/AccountinfoView;

    invoke-virtual {v3}, Lcom/osp/app/signin/AccountinfoView;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f090060

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    new-array v4, v8, [Ljava/lang/Object;

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    aput-object v0, v4, v6

    invoke-static {v3, v4}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, "("

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v2, p0, Lcom/osp/app/signin/cc;->b:Lcom/msc/c/f;

    invoke-virtual {v2}, Lcom/msc/c/f;->a()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, ")"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0, v8}, Lcom/osp/app/util/ap;->a(Landroid/content/Context;Ljava/lang/String;I)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    .line 4439
    :goto_0
    return-void

    .line 4354
    :cond_1
    const-string v0, "AUT_1302"

    iget-object v1, p0, Lcom/osp/app/signin/cc;->b:Lcom/msc/c/f;

    invoke-virtual {v1}, Lcom/msc/c/f;->a()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_2

    const-string v0, "AUT_1830"

    iget-object v1, p0, Lcom/osp/app/signin/cc;->b:Lcom/msc/c/f;

    invoke-virtual {v1}, Lcom/msc/c/f;->a()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 4356
    :cond_2
    iget-object v0, p0, Lcom/osp/app/signin/cc;->c:Lcom/osp/app/signin/AccountinfoView;

    invoke-static {v0}, Lcom/msc/c/c;->g(Landroid/content/Context;)V

    .line 4357
    iget-object v0, p0, Lcom/osp/app/signin/cc;->c:Lcom/osp/app/signin/AccountinfoView;

    invoke-static {v0}, Lcom/msc/c/e;->a(Landroid/content/Context;)Z

    .line 4358
    iget-object v0, p0, Lcom/osp/app/signin/cc;->c:Lcom/osp/app/signin/AccountinfoView;

    iget-object v1, p0, Lcom/osp/app/signin/cc;->c:Lcom/osp/app/signin/AccountinfoView;

    invoke-static {v1}, Lcom/osp/app/util/ad;->h(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/osp/app/signin/AccountinfoView;->a(Lcom/osp/app/signin/AccountinfoView;Ljava/lang/String;)V

    .line 4359
    iget-object v0, p0, Lcom/osp/app/signin/cc;->c:Lcom/osp/app/signin/AccountinfoView;

    invoke-virtual {v0}, Lcom/osp/app/signin/AccountinfoView;->finish()V

    goto :goto_0

    .line 4360
    :cond_3
    invoke-static {}, Lcom/osp/device/b;->b()Lcom/osp/device/b;

    move-result-object v0

    invoke-virtual {v0}, Lcom/osp/device/b;->a()Z

    move-result v0

    if-eqz v0, :cond_4

    const-string v0, "AUT_1094"

    iget-object v1, p0, Lcom/osp/app/signin/cc;->b:Lcom/msc/c/f;

    invoke-virtual {v1}, Lcom/msc/c/f;->a()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 4362
    iget-object v0, p0, Lcom/osp/app/signin/cc;->c:Lcom/osp/app/signin/AccountinfoView;

    invoke-static {v0}, Lcom/osp/app/signin/AccountinfoView;->r(Lcom/osp/app/signin/AccountinfoView;)V

    .line 4363
    iget-object v0, p0, Lcom/osp/app/signin/cc;->c:Lcom/osp/app/signin/AccountinfoView;

    invoke-virtual {v0}, Lcom/osp/app/signin/AccountinfoView;->finish()V

    goto :goto_0

    .line 4366
    :cond_4
    invoke-virtual {p0, v6}, Lcom/osp/app/signin/cc;->a(Z)V

    goto :goto_0

    .line 4372
    :cond_5
    invoke-virtual {p0, v6}, Lcom/osp/app/signin/cc;->a(Z)V

    goto :goto_0

    .line 4376
    :cond_6
    iget-boolean v0, p0, Lcom/osp/app/signin/cc;->d:Z

    if-eqz v0, :cond_c

    .line 4378
    const-string v0, "%04d%02d%02d"

    const/4 v1, 0x3

    new-array v1, v1, [Ljava/lang/Object;

    iget-object v2, p0, Lcom/osp/app/signin/cc;->c:Lcom/osp/app/signin/AccountinfoView;

    invoke-static {v2}, Lcom/osp/app/signin/AccountinfoView;->b(Lcom/osp/app/signin/AccountinfoView;)I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v1, v6

    iget-object v2, p0, Lcom/osp/app/signin/cc;->c:Lcom/osp/app/signin/AccountinfoView;

    invoke-static {v2}, Lcom/osp/app/signin/AccountinfoView;->c(Lcom/osp/app/signin/AccountinfoView;)I

    move-result v2

    add-int/lit8 v2, v2, 0x1

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v1, v8

    const/4 v2, 0x2

    iget-object v3, p0, Lcom/osp/app/signin/cc;->c:Lcom/osp/app/signin/AccountinfoView;

    invoke-static {v3}, Lcom/osp/app/signin/AccountinfoView;->d(Lcom/osp/app/signin/AccountinfoView;)I

    move-result v3

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v1, v2

    invoke-static {v7, v0, v1}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 4380
    iget-object v1, p0, Lcom/osp/app/signin/cc;->c:Lcom/osp/app/signin/AccountinfoView;

    invoke-static {v1, v0}, Lcom/msc/c/e;->c(Landroid/content/Context;Ljava/lang/String;)V

    .line 4381
    invoke-static {}, Lcom/osp/app/util/ap;->a()Lcom/osp/app/util/ap;

    iget-object v0, p0, Lcom/osp/app/signin/cc;->c:Lcom/osp/app/signin/AccountinfoView;

    iget-object v1, p0, Lcom/osp/app/signin/cc;->c:Lcom/osp/app/signin/AccountinfoView;

    const v2, 0x7f090050

    invoke-virtual {v1, v2}, Lcom/osp/app/signin/AccountinfoView;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1, v6}, Lcom/osp/app/util/ap;->a(Landroid/content/Context;Ljava/lang/String;I)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    .line 4383
    invoke-static {}, Lcom/osp/app/signin/SamsungService;->p()Z

    move-result v0

    if-eqz v0, :cond_7

    .line 4385
    iget-object v0, p0, Lcom/osp/app/signin/cc;->c:Lcom/osp/app/signin/AccountinfoView;

    iget-object v1, p0, Lcom/osp/app/signin/cc;->c:Lcom/osp/app/signin/AccountinfoView;

    invoke-static {v1}, Lcom/osp/app/signin/AccountinfoView;->q(Lcom/osp/app/signin/AccountinfoView;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1, v6}, Lcom/msc/openprovider/b;->a(Landroid/content/Context;Ljava/lang/String;Z)V

    .line 4388
    :cond_7
    iget-object v0, p0, Lcom/osp/app/signin/cc;->c:Lcom/osp/app/signin/AccountinfoView;

    invoke-static {v0}, Lcom/osp/app/signin/AccountinfoView;->x(Lcom/osp/app/signin/AccountinfoView;)Z

    move-result v0

    if-eqz v0, :cond_9

    .line 4390
    iget-object v0, p0, Lcom/osp/app/signin/cc;->c:Lcom/osp/app/signin/AccountinfoView;

    invoke-static {v0}, Lcom/osp/app/signin/AccountinfoView;->y(Lcom/osp/app/signin/AccountinfoView;)V

    .line 4408
    :goto_1
    iget-object v0, p0, Lcom/osp/app/signin/cc;->c:Lcom/osp/app/signin/AccountinfoView;

    invoke-static {v0}, Lcom/osp/app/signin/AccountinfoView;->z(Lcom/osp/app/signin/AccountinfoView;)I

    move-result v0

    and-int/lit8 v0, v0, 0x10

    const/16 v1, 0x10

    if-ne v0, v1, :cond_8

    .line 4410
    iget-object v0, p0, Lcom/osp/app/signin/cc;->c:Lcom/osp/app/signin/AccountinfoView;

    invoke-static {v0}, Lcom/osp/app/signin/AccountinfoView;->A(Lcom/osp/app/signin/AccountinfoView;)I

    .line 4413
    :cond_8
    iget-object v0, p0, Lcom/osp/app/signin/cc;->c:Lcom/osp/app/signin/AccountinfoView;

    invoke-static {v0}, Lcom/osp/app/signin/AccountinfoView;->z(Lcom/osp/app/signin/AccountinfoView;)I

    move-result v0

    if-lez v0, :cond_b

    .line 4416
    iget-object v0, p0, Lcom/osp/app/signin/cc;->c:Lcom/osp/app/signin/AccountinfoView;

    iget-object v1, p0, Lcom/osp/app/signin/cc;->c:Lcom/osp/app/signin/AccountinfoView;

    invoke-static {v1}, Lcom/osp/app/signin/AccountinfoView;->z(Lcom/osp/app/signin/AccountinfoView;)I

    move-result v1

    iget-object v2, p0, Lcom/osp/app/signin/cc;->c:Lcom/osp/app/signin/AccountinfoView;

    invoke-static {v2}, Lcom/msc/c/e;->h(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v2

    iget-object v3, p0, Lcom/osp/app/signin/cc;->c:Lcom/osp/app/signin/AccountinfoView;

    invoke-static {v3}, Lcom/osp/app/signin/AccountinfoView;->q(Lcom/osp/app/signin/AccountinfoView;)Ljava/lang/String;

    move-result-object v3

    iget-object v4, p0, Lcom/osp/app/signin/cc;->c:Lcom/osp/app/signin/AccountinfoView;

    invoke-static {v4}, Lcom/osp/app/signin/AccountinfoView;->B(Lcom/osp/app/signin/AccountinfoView;)Ljava/lang/String;

    move-result-object v4

    iget-object v5, p0, Lcom/osp/app/signin/cc;->c:Lcom/osp/app/signin/AccountinfoView;

    invoke-static {v5}, Lcom/osp/app/signin/AccountinfoView;->C(Lcom/osp/app/signin/AccountinfoView;)Z

    move-result v5

    invoke-static/range {v0 .. v7}, Lcom/osp/app/util/ad;->a(Landroid/content/Context;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;ZZLjava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    .line 4418
    iget-object v1, p0, Lcom/osp/app/signin/cc;->c:Lcom/osp/app/signin/AccountinfoView;

    invoke-static {v1}, Lcom/osp/app/signin/AccountinfoView;->D(Lcom/osp/app/signin/AccountinfoView;)Z

    move-result v1

    if-eqz v1, :cond_a

    .line 4420
    const-string v1, "key_return_result"

    invoke-virtual {v0, v1, v8}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 4421
    iget-object v1, p0, Lcom/osp/app/signin/cc;->c:Lcom/osp/app/signin/AccountinfoView;

    const/16 v2, 0xdc

    invoke-virtual {v1, v0, v2}, Lcom/osp/app/signin/AccountinfoView;->startActivityForResult(Landroid/content/Intent;I)V

    goto/16 :goto_0

    .line 4393
    :cond_9
    iget-object v0, p0, Lcom/osp/app/signin/cc;->c:Lcom/osp/app/signin/AccountinfoView;

    const/4 v1, -0x1

    iget-object v2, p0, Lcom/osp/app/signin/cc;->c:Lcom/osp/app/signin/AccountinfoView;

    invoke-static {v2}, Lcom/osp/app/signin/AccountinfoView;->l(Lcom/osp/app/signin/AccountinfoView;)Landroid/content/Intent;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/osp/app/signin/AccountinfoView;->a(ILandroid/content/Intent;)V

    .line 4394
    new-instance v0, Landroid/content/Intent;

    const-string v1, "android.intent.action.SAMSUNGACCOUNT_SIGNUPINFO_CHANGED"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 4397
    const-string v1, "SA_EMAIL_;SA_COUNTRY_;SA_BIRTHDATE_;SA_EMAILRECEIVE_;"

    .line 4405
    const-string v2, "signUpInfo"

    invoke-virtual {v0, v2, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 4406
    iget-object v1, p0, Lcom/osp/app/signin/cc;->c:Lcom/osp/app/signin/AccountinfoView;

    invoke-virtual {v1, v0}, Lcom/osp/app/signin/AccountinfoView;->sendBroadcast(Landroid/content/Intent;)V

    goto :goto_1

    .line 4425
    :cond_a
    iget-object v1, p0, Lcom/osp/app/signin/cc;->c:Lcom/osp/app/signin/AccountinfoView;

    invoke-virtual {v1, v0}, Lcom/osp/app/signin/AccountinfoView;->startActivity(Landroid/content/Intent;)V

    .line 4435
    :cond_b
    iget-object v0, p0, Lcom/osp/app/signin/cc;->c:Lcom/osp/app/signin/AccountinfoView;

    invoke-virtual {v0}, Lcom/osp/app/signin/AccountinfoView;->finish()V

    goto/16 :goto_0

    .line 4438
    :cond_c
    invoke-virtual {p0, v6}, Lcom/osp/app/signin/cc;->a(Z)V

    goto/16 :goto_0
.end method

.method public final b(Lcom/msc/b/c;)V
    .locals 6

    .prologue
    .line 4524
    invoke-super {p0, p1}, Lcom/msc/c/b;->b(Lcom/msc/b/c;)V

    .line 4526
    if-nez p1, :cond_1

    .line 4563
    :cond_0
    :goto_0
    return-void

    .line 4531
    :cond_1
    invoke-virtual {p1}, Lcom/msc/b/c;->a()J

    move-result-wide v0

    .line 4532
    invoke-virtual {p1}, Lcom/msc/b/c;->e()Ljava/lang/String;

    move-result-object v2

    .line 4539
    iget-wide v4, p0, Lcom/osp/app/signin/cc;->h:J

    cmp-long v0, v0, v4

    if-nez v0, :cond_0

    .line 4541
    if-eqz v2, :cond_0

    .line 4543
    iget-boolean v0, p0, Lcom/osp/app/signin/cc;->i:Z

    if-nez v0, :cond_0

    const-string v0, "ACF_0403"

    iget-object v1, p0, Lcom/osp/app/signin/cc;->b:Lcom/msc/c/f;

    invoke-virtual {v1}, Lcom/msc/c/f;->a()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 4547
    const/4 v0, 0x0

    :try_start_0
    iput-object v0, p0, Lcom/osp/app/signin/cc;->b:Lcom/msc/c/f;

    .line 4548
    iget-object v0, p0, Lcom/osp/app/signin/cc;->c:Lcom/osp/app/signin/AccountinfoView;

    const-string v1, ""

    invoke-static {v0, v1}, Lcom/msc/c/e;->b(Landroid/content/Context;Ljava/lang/String;)V

    .line 4549
    invoke-virtual {p0}, Lcom/osp/app/signin/cc;->isCancelled()Z

    move-result v0

    if-nez v0, :cond_0

    .line 4553
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/osp/app/signin/cc;->i:Z

    .line 4554
    invoke-direct {p0}, Lcom/osp/app/signin/cc;->h()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 4555
    :catch_0
    move-exception v0

    .line 4557
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    .line 4558
    new-instance v1, Lcom/msc/c/f;

    invoke-direct {v1, v0}, Lcom/msc/c/f;-><init>(Ljava/lang/Exception;)V

    iput-object v1, p0, Lcom/osp/app/signin/cc;->b:Lcom/msc/c/f;

    goto :goto_0
.end method

.method protected final d()V
    .locals 2

    .prologue
    .line 4311
    invoke-super {p0}, Lcom/msc/c/b;->d()V

    .line 4312
    iget-object v0, p0, Lcom/osp/app/signin/cc;->c:Lcom/osp/app/signin/AccountinfoView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/osp/app/signin/AccountinfoView;->b(I)V

    .line 4313
    iget-object v0, p0, Lcom/osp/app/signin/cc;->c:Lcom/osp/app/signin/AccountinfoView;

    invoke-virtual {v0}, Lcom/osp/app/signin/AccountinfoView;->finish()V

    .line 4314
    return-void
.end method

.method protected final synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 4292
    invoke-virtual {p0}, Lcom/osp/app/signin/cc;->a()Ljava/lang/Boolean;

    move-result-object v0

    return-object v0
.end method

.method protected final synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 4292
    check-cast p1, Ljava/lang/Boolean;

    invoke-virtual {p0, p1}, Lcom/osp/app/signin/cc;->a(Ljava/lang/Boolean;)V

    return-void
.end method

.method protected final onPreExecute()V
    .locals 1

    .prologue
    .line 4318
    invoke-super {p0}, Lcom/msc/c/b;->onPreExecute()V

    .line 4320
    iget-object v0, p0, Lcom/osp/app/signin/cc;->c:Lcom/osp/app/signin/AccountinfoView;

    invoke-static {v0}, Lcom/osp/app/signin/AccountinfoView;->v(Lcom/osp/app/signin/AccountinfoView;)Lcom/osp/app/signin/SignUpinfo;

    move-result-object v0

    iput-object v0, p0, Lcom/osp/app/signin/cc;->j:Lcom/osp/app/signin/SignUpinfo;

    .line 4321
    return-void
.end method
