.class public Lcom/osp/app/signin/CustomSpinner;
.super Landroid/widget/Spinner;
.source "CustomSpinner.java"


# instance fields
.field private a:Landroid/view/View$OnClickListener;

.field private b:Landroid/widget/ListView;


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0

    .prologue
    .line 22
    invoke-direct {p0, p1, p2}, Landroid/widget/Spinner;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 24
    return-void
.end method


# virtual methods
.method public clearFocus()V
    .locals 2

    .prologue
    .line 52
    invoke-super {p0}, Landroid/widget/Spinner;->clearFocus()V

    .line 54
    iget-object v0, p0, Lcom/osp/app/signin/CustomSpinner;->b:Landroid/widget/ListView;

    if-eqz v0, :cond_0

    .line 56
    iget-object v0, p0, Lcom/osp/app/signin/CustomSpinner;->b:Landroid/widget/ListView;

    invoke-virtual {v0}, Landroid/widget/ListView;->getSelectedItemPosition()I

    move-result v0

    const/4 v1, -0x1

    if-eq v0, v1, :cond_0

    iget-object v0, p0, Lcom/osp/app/signin/CustomSpinner;->b:Landroid/widget/ListView;

    invoke-virtual {v0}, Landroid/widget/ListView;->getItemsCanFocus()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 58
    iget-object v0, p0, Lcom/osp/app/signin/CustomSpinner;->b:Landroid/widget/ListView;

    invoke-virtual {v0}, Landroid/widget/ListView;->requestFocus()Z

    .line 61
    :cond_0
    return-void
.end method

.method public performClick()Z
    .locals 3

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 38
    invoke-virtual {p0, v0}, Lcom/osp/app/signin/CustomSpinner;->sendAccessibilityEvent(I)V

    .line 40
    iget-object v2, p0, Lcom/osp/app/signin/CustomSpinner;->a:Landroid/view/View$OnClickListener;

    if-eqz v2, :cond_0

    .line 42
    invoke-virtual {p0, v1}, Lcom/osp/app/signin/CustomSpinner;->playSoundEffect(I)V

    .line 43
    iget-object v1, p0, Lcom/osp/app/signin/CustomSpinner;->a:Landroid/view/View$OnClickListener;

    invoke-interface {v1, p0}, Landroid/view/View$OnClickListener;->onClick(Landroid/view/View;)V

    .line 46
    :goto_0
    return v0

    :cond_0
    move v0, v1

    goto :goto_0
.end method

.method public setOnClickListener(Landroid/view/View$OnClickListener;)V
    .locals 0

    .prologue
    .line 33
    iput-object p1, p0, Lcom/osp/app/signin/CustomSpinner;->a:Landroid/view/View$OnClickListener;

    .line 34
    return-void
.end method
