.class final Lcom/osp/app/signin/lu;
.super Ljava/lang/Object;
.source "SignUpView.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field final synthetic a:Lcom/osp/app/signin/SignUpView;


# direct methods
.method constructor <init>(Lcom/osp/app/signin/SignUpView;)V
    .locals 0

    .prologue
    .line 1603
    iput-object p1, p0, Lcom/osp/app/signin/lu;->a:Lcom/osp/app/signin/SignUpView;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onClick(Landroid/view/View;)V
    .locals 5

    .prologue
    const/4 v2, 0x0

    .line 1609
    iget-object v0, p0, Lcom/osp/app/signin/lu;->a:Lcom/osp/app/signin/SignUpView;

    invoke-static {v0}, Lcom/osp/app/signin/SignUpView;->k(Lcom/osp/app/signin/SignUpView;)Landroid/app/AlertDialog;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 1613
    :try_start_0
    iget-object v0, p0, Lcom/osp/app/signin/lu;->a:Lcom/osp/app/signin/SignUpView;

    invoke-static {v0}, Lcom/osp/app/signin/SignUpView;->k(Lcom/osp/app/signin/SignUpView;)Landroid/app/AlertDialog;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/AlertDialog;->isShowing()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1615
    iget-object v0, p0, Lcom/osp/app/signin/lu;->a:Lcom/osp/app/signin/SignUpView;

    invoke-static {v0}, Lcom/osp/app/signin/SignUpView;->k(Lcom/osp/app/signin/SignUpView;)Landroid/app/AlertDialog;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/AlertDialog;->dismiss()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1622
    :cond_0
    iget-object v0, p0, Lcom/osp/app/signin/lu;->a:Lcom/osp/app/signin/SignUpView;

    :goto_0
    invoke-static {v0, v2}, Lcom/osp/app/signin/SignUpView;->c(Lcom/osp/app/signin/SignUpView;Landroid/app/AlertDialog;)Landroid/app/AlertDialog;

    .line 1626
    :cond_1
    iget-object v0, p0, Lcom/osp/app/signin/lu;->a:Lcom/osp/app/signin/SignUpView;

    invoke-static {v0}, Lcom/osp/app/signin/SignUpView;->k(Lcom/osp/app/signin/SignUpView;)Landroid/app/AlertDialog;

    move-result-object v0

    if-nez v0, :cond_2

    .line 1630
    :try_start_1
    iget-object v0, p0, Lcom/osp/app/signin/lu;->a:Lcom/osp/app/signin/SignUpView;

    const v1, 0x7f0900ca

    const/4 v2, 0x3

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    const/16 v4, 0x8

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v2, v3

    const/4 v3, 0x1

    const/16 v4, 0xf

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v2, v3

    const/4 v3, 0x2

    const/4 v4, 0x3

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-virtual {v0, v1, v2}, Lcom/osp/app/signin/SignUpView;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 1632
    iget-object v1, p0, Lcom/osp/app/signin/lu;->a:Lcom/osp/app/signin/SignUpView;

    new-instance v2, Landroid/app/AlertDialog$Builder;

    iget-object v3, p0, Lcom/osp/app/signin/lu;->a:Lcom/osp/app/signin/SignUpView;

    invoke-direct {v2, v3}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    const v3, 0x7f090045

    invoke-virtual {v2, v3}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    move-result-object v2

    invoke-virtual {v2, v0}, Landroid/app/AlertDialog$Builder;->setMessage(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    const v2, 0x7f090042

    new-instance v3, Lcom/osp/app/signin/lv;

    invoke-direct {v3, p0}, Lcom/osp/app/signin/lv;-><init>(Lcom/osp/app/signin/lu;)V

    invoke-virtual {v0, v2, v3}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->show()Landroid/app/AlertDialog;

    move-result-object v0

    invoke-static {v1, v0}, Lcom/osp/app/signin/SignUpView;->c(Lcom/osp/app/signin/SignUpView;Landroid/app/AlertDialog;)Landroid/app/AlertDialog;
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1

    .line 1645
    :cond_2
    :goto_1
    return-void

    .line 1617
    :catch_0
    move-exception v0

    :try_start_2
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 1622
    iget-object v0, p0, Lcom/osp/app/signin/lu;->a:Lcom/osp/app/signin/SignUpView;

    goto :goto_0

    :catchall_0
    move-exception v0

    iget-object v1, p0, Lcom/osp/app/signin/lu;->a:Lcom/osp/app/signin/SignUpView;

    invoke-static {v1, v2}, Lcom/osp/app/signin/SignUpView;->c(Lcom/osp/app/signin/SignUpView;Landroid/app/AlertDialog;)Landroid/app/AlertDialog;

    throw v0

    .line 1640
    :catch_1
    move-exception v0

    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_1
.end method
