.class final Lcom/osp/app/signin/gb;
.super Landroid/webkit/WebViewClient;
.source "NameValidationWebView.java"


# instance fields
.field final synthetic a:Lcom/osp/app/signin/NameValidationWebView;


# direct methods
.method private constructor <init>(Lcom/osp/app/signin/NameValidationWebView;)V
    .locals 0

    .prologue
    .line 703
    iput-object p1, p0, Lcom/osp/app/signin/gb;->a:Lcom/osp/app/signin/NameValidationWebView;

    invoke-direct {p0}, Landroid/webkit/WebViewClient;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/osp/app/signin/NameValidationWebView;B)V
    .locals 0

    .prologue
    .line 703
    invoke-direct {p0, p1}, Lcom/osp/app/signin/gb;-><init>(Lcom/osp/app/signin/NameValidationWebView;)V

    return-void
.end method


# virtual methods
.method public final onLoadResource(Landroid/webkit/WebView;Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 799
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v0, "NVWV"

    const-string v1, "onLoadResource"

    invoke-static {v0, v1}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 800
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "NameValidationWebView::onLoadResource URL = "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;)V

    .line 802
    invoke-super {p0, p1, p2}, Landroid/webkit/WebViewClient;->onLoadResource(Landroid/webkit/WebView;Ljava/lang/String;)V

    .line 803
    return-void
.end method

.method public final onPageFinished(Landroid/webkit/WebView;Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 780
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v0, "NVWV"

    const-string v1, "onPageFinished"

    invoke-static {v0, v1}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 781
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "NameValidationWebView::onPageFinished URL = "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;)V

    .line 785
    :try_start_0
    iget-object v0, p0, Lcom/osp/app/signin/gb;->a:Lcom/osp/app/signin/NameValidationWebView;

    invoke-virtual {v0}, Lcom/osp/app/signin/NameValidationWebView;->isFinishing()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/osp/app/signin/gb;->a:Lcom/osp/app/signin/NameValidationWebView;

    invoke-static {v0}, Lcom/osp/app/signin/NameValidationWebView;->a(Lcom/osp/app/signin/NameValidationWebView;)Landroid/app/ProgressDialog;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/osp/app/signin/gb;->a:Lcom/osp/app/signin/NameValidationWebView;

    invoke-static {v0}, Lcom/osp/app/signin/NameValidationWebView;->a(Lcom/osp/app/signin/NameValidationWebView;)Landroid/app/ProgressDialog;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/ProgressDialog;->isShowing()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 787
    iget-object v0, p0, Lcom/osp/app/signin/gb;->a:Lcom/osp/app/signin/NameValidationWebView;

    invoke-static {v0}, Lcom/osp/app/signin/NameValidationWebView;->a(Lcom/osp/app/signin/NameValidationWebView;)Landroid/app/ProgressDialog;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/ProgressDialog;->dismiss()V

    .line 788
    iget-object v0, p0, Lcom/osp/app/signin/gb;->a:Lcom/osp/app/signin/NameValidationWebView;

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lcom/osp/app/signin/NameValidationWebView;->a(Lcom/osp/app/signin/NameValidationWebView;Landroid/app/ProgressDialog;)Landroid/app/ProgressDialog;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 794
    :cond_0
    :goto_0
    invoke-super {p0, p1, p2}, Landroid/webkit/WebViewClient;->onPageFinished(Landroid/webkit/WebView;Ljava/lang/String;)V

    .line 795
    return-void

    .line 790
    :catch_0
    move-exception v0

    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_0
.end method

.method public final onPageStarted(Landroid/webkit/WebView;Ljava/lang/String;Landroid/graphics/Bitmap;)V
    .locals 3

    .prologue
    .line 716
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v0, "NVWV"

    const-string v1, "onPageStarted"

    invoke-static {v0, v1}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 717
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "NameValidationWebView::onPageStarted URL = "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;)V

    .line 719
    iget-object v0, p0, Lcom/osp/app/signin/gb;->a:Lcom/osp/app/signin/NameValidationWebView;

    invoke-static {v0, p2}, Lcom/osp/app/signin/NameValidationWebView;->a(Lcom/osp/app/signin/NameValidationWebView;Ljava/lang/String;)Z

    move-result v0

    const/4 v1, 0x1

    if-ne v0, v1, :cond_0

    .line 721
    invoke-virtual {p1}, Landroid/webkit/WebView;->stopLoading()V

    .line 724
    :cond_0
    iget-object v0, p0, Lcom/osp/app/signin/gb;->a:Lcom/osp/app/signin/NameValidationWebView;

    invoke-static {v0}, Lcom/osp/app/signin/NameValidationWebView;->a(Lcom/osp/app/signin/NameValidationWebView;)Landroid/app/ProgressDialog;

    move-result-object v0

    if-nez v0, :cond_1

    .line 726
    iget-object v0, p0, Lcom/osp/app/signin/gb;->a:Lcom/osp/app/signin/NameValidationWebView;

    new-instance v1, Landroid/app/ProgressDialog;

    iget-object v2, p0, Lcom/osp/app/signin/gb;->a:Lcom/osp/app/signin/NameValidationWebView;

    invoke-static {v2}, Lcom/osp/app/signin/NameValidationWebView;->f(Lcom/osp/app/signin/NameValidationWebView;)Landroid/content/Context;

    move-result-object v2

    invoke-direct {v1, v2}, Landroid/app/ProgressDialog;-><init>(Landroid/content/Context;)V

    invoke-static {v0, v1}, Lcom/osp/app/signin/NameValidationWebView;->a(Lcom/osp/app/signin/NameValidationWebView;Landroid/app/ProgressDialog;)Landroid/app/ProgressDialog;

    .line 727
    iget-object v0, p0, Lcom/osp/app/signin/gb;->a:Lcom/osp/app/signin/NameValidationWebView;

    invoke-static {v0}, Lcom/osp/app/signin/NameValidationWebView;->a(Lcom/osp/app/signin/NameValidationWebView;)Landroid/app/ProgressDialog;

    move-result-object v0

    iget-object v1, p0, Lcom/osp/app/signin/gb;->a:Lcom/osp/app/signin/NameValidationWebView;

    invoke-static {v1}, Lcom/osp/app/signin/NameValidationWebView;->f(Lcom/osp/app/signin/NameValidationWebView;)Landroid/content/Context;

    move-result-object v1

    const v2, 0x7f090036

    invoke-static {v1, v2}, Lcom/msc/sa/c/d;->a(Landroid/content/Context;I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/app/ProgressDialog;->setMessage(Ljava/lang/CharSequence;)V

    .line 728
    iget-object v0, p0, Lcom/osp/app/signin/gb;->a:Lcom/osp/app/signin/NameValidationWebView;

    invoke-static {v0}, Lcom/osp/app/signin/NameValidationWebView;->a(Lcom/osp/app/signin/NameValidationWebView;)Landroid/app/ProgressDialog;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/app/ProgressDialog;->setCancelable(Z)V

    .line 729
    iget-object v0, p0, Lcom/osp/app/signin/gb;->a:Lcom/osp/app/signin/NameValidationWebView;

    invoke-static {v0}, Lcom/osp/app/signin/NameValidationWebView;->a(Lcom/osp/app/signin/NameValidationWebView;)Landroid/app/ProgressDialog;

    move-result-object v0

    new-instance v1, Lcom/osp/app/signin/gc;

    invoke-direct {v1, p0}, Lcom/osp/app/signin/gc;-><init>(Lcom/osp/app/signin/gb;)V

    invoke-virtual {v0, v1}, Landroid/app/ProgressDialog;->setOnKeyListener(Landroid/content/DialogInterface$OnKeyListener;)V

    .line 766
    :try_start_0
    iget-object v0, p0, Lcom/osp/app/signin/gb;->a:Lcom/osp/app/signin/NameValidationWebView;

    invoke-virtual {v0}, Lcom/osp/app/signin/NameValidationWebView;->isFinishing()Z

    move-result v0

    if-nez v0, :cond_1

    .line 768
    iget-object v0, p0, Lcom/osp/app/signin/gb;->a:Lcom/osp/app/signin/NameValidationWebView;

    invoke-static {v0}, Lcom/osp/app/signin/NameValidationWebView;->a(Lcom/osp/app/signin/NameValidationWebView;)Landroid/app/ProgressDialog;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/ProgressDialog;->show()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 775
    :cond_1
    :goto_0
    invoke-super {p0, p1, p2, p3}, Landroid/webkit/WebViewClient;->onPageStarted(Landroid/webkit/WebView;Ljava/lang/String;Landroid/graphics/Bitmap;)V

    .line 776
    return-void

    .line 770
    :catch_0
    move-exception v0

    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_0
.end method

.method public final onReceivedError(Landroid/webkit/WebView;ILjava/lang/String;Ljava/lang/String;)V
    .locals 3

    .prologue
    .line 807
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v0, "NVWV"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "onReceivedError errorCode : "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " ["

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "]"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 811
    :try_start_0
    iget-object v0, p0, Lcom/osp/app/signin/gb;->a:Lcom/osp/app/signin/NameValidationWebView;

    invoke-virtual {v0}, Lcom/osp/app/signin/NameValidationWebView;->isFinishing()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/osp/app/signin/gb;->a:Lcom/osp/app/signin/NameValidationWebView;

    invoke-static {v0}, Lcom/osp/app/signin/NameValidationWebView;->a(Lcom/osp/app/signin/NameValidationWebView;)Landroid/app/ProgressDialog;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/osp/app/signin/gb;->a:Lcom/osp/app/signin/NameValidationWebView;

    invoke-static {v0}, Lcom/osp/app/signin/NameValidationWebView;->a(Lcom/osp/app/signin/NameValidationWebView;)Landroid/app/ProgressDialog;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/ProgressDialog;->isShowing()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 813
    iget-object v0, p0, Lcom/osp/app/signin/gb;->a:Lcom/osp/app/signin/NameValidationWebView;

    invoke-static {v0}, Lcom/osp/app/signin/NameValidationWebView;->a(Lcom/osp/app/signin/NameValidationWebView;)Landroid/app/ProgressDialog;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/ProgressDialog;->dismiss()V

    .line 814
    iget-object v0, p0, Lcom/osp/app/signin/gb;->a:Lcom/osp/app/signin/NameValidationWebView;

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lcom/osp/app/signin/NameValidationWebView;->a(Lcom/osp/app/signin/NameValidationWebView;Landroid/app/ProgressDialog;)Landroid/app/ProgressDialog;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 821
    :cond_0
    :goto_0
    invoke-super {p0, p1, p2, p3, p4}, Landroid/webkit/WebViewClient;->onReceivedError(Landroid/webkit/WebView;ILjava/lang/String;Ljava/lang/String;)V

    .line 822
    return-void

    .line 816
    :catch_0
    move-exception v0

    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_0
.end method

.method public final shouldOverrideUrlLoading(Landroid/webkit/WebView;Ljava/lang/String;)Z
    .locals 2

    .prologue
    .line 707
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "NameValidationWebView::shouldOverrideUrlLoading URL = "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;)V

    .line 709
    invoke-virtual {p1, p2}, Landroid/webkit/WebView;->loadUrl(Ljava/lang/String;)V

    .line 711
    const/4 v0, 0x1

    return v0
.end method
