.class final Lcom/osp/app/signin/jm;
.super Lcom/msc/c/b;
.source "SignInView.java"


# instance fields
.field protected c:Ljava/util/ArrayList;

.field final synthetic d:Lcom/osp/app/signin/SignInView;

.field private e:J


# direct methods
.method public constructor <init>(Lcom/osp/app/signin/SignInView;)V
    .locals 0

    .prologue
    .line 7284
    iput-object p1, p0, Lcom/osp/app/signin/jm;->d:Lcom/osp/app/signin/SignInView;

    .line 7285
    invoke-direct {p0, p1}, Lcom/msc/c/b;-><init>(Landroid/content/Context;)V

    .line 7289
    return-void
.end method


# virtual methods
.method protected final varargs a()Ljava/lang/Boolean;
    .locals 3

    .prologue
    .line 7293
    invoke-static {}, Lcom/msc/c/g;->a()Lcom/msc/c/g;

    iget-object v0, p0, Lcom/osp/app/signin/jm;->d:Lcom/osp/app/signin/SignInView;

    invoke-static {v0, p0}, Lcom/msc/c/g;->a(Landroid/content/Context;Lcom/msc/b/h;)J

    move-result-wide v0

    iput-wide v0, p0, Lcom/osp/app/signin/jm;->e:J

    iget-wide v0, p0, Lcom/osp/app/signin/jm;->e:J

    invoke-virtual {p0, v0, v1}, Lcom/osp/app/signin/jm;->a(J)V

    iget-wide v0, p0, Lcom/osp/app/signin/jm;->e:J

    const-string v2, "from_json"

    invoke-virtual {p0, v0, v1, v2}, Lcom/osp/app/signin/jm;->a(JLjava/lang/String;)V

    iget-wide v0, p0, Lcom/osp/app/signin/jm;->e:J

    sget-object v2, Lcom/msc/b/g;->b:Lcom/msc/b/g;

    invoke-static {v0, v1, v2}, Lcom/msc/c/g;->a(JLcom/msc/b/g;)V

    .line 7294
    const/4 v0, 0x0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    return-object v0
.end method

.method public final declared-synchronized a(Lcom/msc/b/c;)V
    .locals 6

    .prologue
    .line 7491
    monitor-enter p0

    :try_start_0
    invoke-super {p0, p1}, Lcom/msc/c/b;->a(Lcom/msc/b/c;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 7493
    if-nez p1, :cond_1

    .line 7512
    :cond_0
    :goto_0
    monitor-exit p0

    return-void

    .line 7498
    :cond_1
    :try_start_1
    invoke-virtual {p1}, Lcom/msc/b/c;->a()J

    move-result-wide v0

    .line 7499
    invoke-virtual {p1}, Lcom/msc/b/c;->e()Ljava/lang/String;

    move-result-object v2

    .line 7501
    iget-wide v4, p0, Lcom/osp/app/signin/jm;->e:J
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    cmp-long v0, v0, v4

    if-nez v0, :cond_0

    .line 7505
    :try_start_2
    invoke-static {}, Lcom/msc/c/h;->a()Lcom/msc/c/h;

    invoke-static {v2}, Lcom/msc/c/h;->c(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v0

    iput-object v0, p0, Lcom/osp/app/signin/jm;->c:Ljava/util/ArrayList;
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_0
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0

    .line 7506
    :catch_0
    move-exception v0

    .line 7508
    :try_start_3
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    .line 7509
    new-instance v1, Lcom/msc/c/f;

    invoke-direct {v1, v0}, Lcom/msc/c/f;-><init>(Ljava/lang/Exception;)V

    iput-object v1, p0, Lcom/osp/app/signin/jm;->b:Lcom/msc/c/f;
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    goto :goto_0

    .line 7491
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method protected final a(Ljava/lang/Boolean;)V
    .locals 10

    .prologue
    const/4 v9, 0x1

    const/4 v4, 0x0

    .line 7305
    invoke-super {p0, p1}, Lcom/msc/c/b;->a(Ljava/lang/Boolean;)V

    .line 7306
    iget-object v0, p0, Lcom/osp/app/signin/jm;->b:Lcom/msc/c/f;

    if-eqz v0, :cond_2

    .line 7308
    invoke-virtual {p0, v4}, Lcom/osp/app/signin/jm;->a(Z)V

    .line 7309
    iget-object v0, p0, Lcom/osp/app/signin/jm;->d:Lcom/osp/app/signin/SignInView;

    const v1, 0x7f09003e

    invoke-virtual {v0, v1}, Lcom/osp/app/signin/SignInView;->getString(I)Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/osp/app/signin/jm;->b:Lcom/msc/c/f;

    invoke-virtual {v1}, Lcom/msc/c/f;->b()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 7311
    iget-object v0, p0, Lcom/osp/app/signin/jm;->d:Lcom/osp/app/signin/SignInView;

    const/4 v1, 0x3

    iget-object v2, p0, Lcom/osp/app/signin/jm;->d:Lcom/osp/app/signin/SignInView;

    invoke-static {v2}, Lcom/osp/app/signin/SignInView;->c(Lcom/osp/app/signin/SignInView;)Landroid/content/Intent;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/osp/app/signin/SignInView;->a(ILandroid/content/Intent;)V

    .line 7316
    :goto_0
    iget-object v0, p0, Lcom/osp/app/signin/jm;->d:Lcom/osp/app/signin/SignInView;

    invoke-virtual {v0}, Lcom/osp/app/signin/SignInView;->finish()V

    .line 7473
    :cond_0
    :goto_1
    return-void

    .line 7314
    :cond_1
    iget-object v0, p0, Lcom/osp/app/signin/jm;->d:Lcom/osp/app/signin/SignInView;

    iget-object v1, p0, Lcom/osp/app/signin/jm;->d:Lcom/osp/app/signin/SignInView;

    invoke-static {v1}, Lcom/osp/app/signin/SignInView;->c(Lcom/osp/app/signin/SignInView;)Landroid/content/Intent;

    move-result-object v1

    invoke-virtual {v0, v9, v1}, Lcom/osp/app/signin/SignInView;->a(ILandroid/content/Intent;)V

    goto :goto_0

    .line 7319
    :cond_2
    iget-object v0, p0, Lcom/osp/app/signin/jm;->c:Ljava/util/ArrayList;

    if-eqz v0, :cond_7

    .line 7321
    iget-object v0, p0, Lcom/osp/app/signin/jm;->c:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    new-array v5, v0, [Ljava/lang/String;

    .line 7322
    const/4 v0, 0x0

    .line 7323
    invoke-static {}, Lcom/osp/common/util/i;->a()Lcom/osp/common/util/i;

    iget-object v1, p0, Lcom/osp/app/signin/jm;->d:Lcom/osp/app/signin/SignInView;

    invoke-static {v1}, Lcom/osp/common/util/i;->e(Landroid/content/Context;)Z

    move-result v1

    if-eqz v1, :cond_4

    .line 7327
    :try_start_0
    invoke-static {}, Lcom/osp/common/util/i;->a()Lcom/osp/common/util/i;

    iget-object v1, p0, Lcom/osp/app/signin/jm;->d:Lcom/osp/app/signin/SignInView;

    invoke-static {v1}, Lcom/osp/common/util/i;->c(Landroid/content/Context;)Ljava/lang/String;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    move-object v2, v0

    .line 7336
    :goto_2
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v0, "==============================================="

    invoke-static {v0}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;)V

    .line 7337
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    invoke-static {v2}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;)V

    .line 7338
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v0, "==============================================="

    invoke-static {v0}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;)V

    .line 7341
    iget-object v0, p0, Lcom/osp/app/signin/jm;->c:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v6

    move v3, v4

    :goto_3
    invoke-interface {v6}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_6

    invoke-interface {v6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/osp/app/countrylist/CountryInfoItem;

    .line 7343
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    iget-object v1, p0, Lcom/osp/app/signin/jm;->c:Ljava/util/ArrayList;

    invoke-virtual {v1, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/osp/app/countrylist/CountryInfoItem;

    invoke-virtual {v1}, Lcom/osp/app/countrylist/CountryInfoItem;->a()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;)V

    .line 7344
    if-eqz v2, :cond_3

    .line 7346
    iget-object v1, p0, Lcom/osp/app/signin/jm;->c:Ljava/util/ArrayList;

    invoke-virtual {v1, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/osp/app/countrylist/CountryInfoItem;

    invoke-virtual {v1}, Lcom/osp/app/countrylist/CountryInfoItem;->a()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_3

    .line 7348
    iget-object v1, p0, Lcom/osp/app/signin/jm;->d:Lcom/osp/app/signin/SignInView;

    invoke-static {v1, v3}, Lcom/osp/app/signin/SignInView;->a(Lcom/osp/app/signin/SignInView;I)I

    .line 7352
    :cond_3
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v7, "CountryListDialogManager::handleMessage mSelectedLocation : "

    invoke-direct {v1, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v7, p0, Lcom/osp/app/signin/jm;->d:Lcom/osp/app/signin/SignInView;

    invoke-static {v7}, Lcom/osp/app/signin/SignInView;->ab(Lcom/osp/app/signin/SignInView;)I

    move-result v7

    invoke-virtual {v1, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;)V

    .line 7354
    const-string v1, ""

    invoke-virtual {v0}, Lcom/osp/app/countrylist/CountryInfoItem;->d()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v1, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_5

    .line 7356
    invoke-virtual {v0}, Lcom/osp/app/countrylist/CountryInfoItem;->b()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0}, Lcom/osp/app/countrylist/CountryInfoItem;->d()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v1, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_5

    .line 7361
    iget-object v1, p0, Lcom/osp/app/signin/jm;->d:Lcom/osp/app/signin/SignInView;

    invoke-virtual {v0}, Lcom/osp/app/countrylist/CountryInfoItem;->c()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/osp/app/signin/SignInView;->b(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_5

    .line 7366
    add-int/lit8 v1, v3, 0x1

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0}, Lcom/osp/app/countrylist/CountryInfoItem;->b()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, " ("

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v0}, Lcom/osp/app/countrylist/CountryInfoItem;->d()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v7, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v7, ")"

    invoke-virtual {v0, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    aput-object v0, v5, v3

    move v3, v1

    goto/16 :goto_3

    .line 7328
    :catch_0
    move-exception v1

    invoke-virtual {v1}, Ljava/lang/Exception;->printStackTrace()V

    move-object v2, v0

    .line 7331
    goto/16 :goto_2

    .line 7334
    :cond_4
    invoke-static {}, Lcom/osp/common/util/i;->a()Lcom/osp/common/util/i;

    iget-object v0, p0, Lcom/osp/app/signin/jm;->d:Lcom/osp/app/signin/SignInView;

    invoke-static {v0}, Lcom/osp/common/util/i;->m(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v0

    move-object v2, v0

    goto/16 :goto_2

    .line 7369
    :cond_5
    add-int/lit8 v1, v3, 0x1

    invoke-virtual {v0}, Lcom/osp/app/countrylist/CountryInfoItem;->b()Ljava/lang/String;

    move-result-object v0

    aput-object v0, v5, v3

    move v3, v1

    .line 7373
    goto/16 :goto_3

    .line 7377
    :cond_6
    iget-object v0, p0, Lcom/osp/app/signin/jm;->d:Lcom/osp/app/signin/SignInView;

    invoke-virtual {v0}, Lcom/osp/app/signin/SignInView;->isFinishing()Z

    move-result v0

    if-nez v0, :cond_0

    .line 7383
    new-instance v0, Landroid/app/AlertDialog$Builder;

    iget-object v1, p0, Lcom/osp/app/signin/jm;->d:Lcom/osp/app/signin/SignInView;

    invoke-direct {v0, v1}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    .line 7384
    iget-object v1, p0, Lcom/osp/app/signin/jm;->d:Lcom/osp/app/signin/SignInView;

    const v2, 0x7f09001f

    invoke-virtual {v0, v2}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    invoke-virtual {v0, v9}, Landroid/app/AlertDialog$Builder;->setCancelable(Z)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    iget-object v2, p0, Lcom/osp/app/signin/jm;->d:Lcom/osp/app/signin/SignInView;

    invoke-static {v2}, Lcom/osp/app/signin/SignInView;->ab(Lcom/osp/app/signin/SignInView;)I

    move-result v2

    new-instance v3, Lcom/osp/app/signin/jp;

    invoke-direct {v3, p0}, Lcom/osp/app/signin/jp;-><init>(Lcom/osp/app/signin/jm;)V

    invoke-virtual {v0, v5, v2, v3}, Landroid/app/AlertDialog$Builder;->setSingleChoiceItems([Ljava/lang/CharSequence;ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    const v2, 0x7f090019

    new-instance v3, Lcom/osp/app/signin/jo;

    invoke-direct {v3, p0}, Lcom/osp/app/signin/jo;-><init>(Lcom/osp/app/signin/jm;)V

    invoke-virtual {v0, v2, v3}, Landroid/app/AlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    const v2, 0x7f090042

    new-instance v3, Lcom/osp/app/signin/jn;

    invoke-direct {v3, p0}, Lcom/osp/app/signin/jn;-><init>(Lcom/osp/app/signin/jm;)V

    invoke-virtual {v0, v2, v3}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->show()Landroid/app/AlertDialog;

    move-result-object v0

    invoke-static {v1, v0}, Lcom/osp/app/signin/SignInView;->a(Lcom/osp/app/signin/SignInView;Landroid/app/AlertDialog;)Landroid/app/AlertDialog;

    .line 7431
    iget-object v0, p0, Lcom/osp/app/signin/jm;->d:Lcom/osp/app/signin/SignInView;

    invoke-static {v0}, Lcom/osp/app/signin/SignInView;->ac(Lcom/osp/app/signin/SignInView;)Landroid/app/AlertDialog;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 7433
    iget-object v0, p0, Lcom/osp/app/signin/jm;->d:Lcom/osp/app/signin/SignInView;

    invoke-static {v0}, Lcom/osp/app/signin/SignInView;->ac(Lcom/osp/app/signin/SignInView;)Landroid/app/AlertDialog;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/AlertDialog;->getListView()Landroid/widget/ListView;

    move-result-object v0

    invoke-virtual {v0, v4}, Landroid/widget/ListView;->setFastScrollEnabled(Z)V

    .line 7434
    iget-object v0, p0, Lcom/osp/app/signin/jm;->d:Lcom/osp/app/signin/SignInView;

    invoke-static {v0}, Lcom/osp/app/signin/SignInView;->ac(Lcom/osp/app/signin/SignInView;)Landroid/app/AlertDialog;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/AlertDialog;->getListView()Landroid/widget/ListView;

    move-result-object v0

    invoke-virtual {v0, v9}, Landroid/widget/ListView;->setVerticalScrollBarEnabled(Z)V

    .line 7450
    iget-object v0, p0, Lcom/osp/app/signin/jm;->d:Lcom/osp/app/signin/SignInView;

    invoke-static {v0}, Lcom/osp/app/signin/SignInView;->ac(Lcom/osp/app/signin/SignInView;)Landroid/app/AlertDialog;

    move-result-object v0

    new-instance v1, Lcom/osp/app/signin/jq;

    invoke-direct {v1, p0}, Lcom/osp/app/signin/jq;-><init>(Lcom/osp/app/signin/jm;)V

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog;->setOnKeyListener(Landroid/content/DialogInterface$OnKeyListener;)V

    goto/16 :goto_1

    .line 7469
    :cond_7
    invoke-virtual {p0, v4}, Lcom/osp/app/signin/jm;->a(Z)V

    .line 7470
    iget-object v0, p0, Lcom/osp/app/signin/jm;->d:Lcom/osp/app/signin/SignInView;

    iget-object v1, p0, Lcom/osp/app/signin/jm;->d:Lcom/osp/app/signin/SignInView;

    invoke-static {v1}, Lcom/osp/app/signin/SignInView;->c(Lcom/osp/app/signin/SignInView;)Landroid/content/Intent;

    move-result-object v1

    invoke-virtual {v0, v9, v1}, Lcom/osp/app/signin/SignInView;->a(ILandroid/content/Intent;)V

    .line 7471
    iget-object v0, p0, Lcom/osp/app/signin/jm;->d:Lcom/osp/app/signin/SignInView;

    invoke-virtual {v0}, Lcom/osp/app/signin/SignInView;->finish()V

    goto/16 :goto_1
.end method

.method protected final d()V
    .locals 1

    .prologue
    .line 7299
    invoke-super {p0}, Lcom/msc/c/b;->d()V

    .line 7300
    iget-object v0, p0, Lcom/osp/app/signin/jm;->d:Lcom/osp/app/signin/SignInView;

    invoke-static {v0}, Lcom/osp/app/signin/SignInView;->a(Lcom/osp/app/signin/SignInView;)V

    .line 7301
    return-void
.end method

.method protected final synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 7269
    invoke-virtual {p0}, Lcom/osp/app/signin/jm;->a()Ljava/lang/Boolean;

    move-result-object v0

    return-object v0
.end method

.method public final h()Lcom/osp/app/countrylist/CountryInfoItem;
    .locals 2

    .prologue
    .line 7481
    const/4 v0, 0x0

    .line 7482
    iget-object v1, p0, Lcom/osp/app/signin/jm;->c:Ljava/util/ArrayList;

    if-eqz v1, :cond_0

    .line 7484
    iget-object v0, p0, Lcom/osp/app/signin/jm;->c:Ljava/util/ArrayList;

    iget-object v1, p0, Lcom/osp/app/signin/jm;->d:Lcom/osp/app/signin/SignInView;

    invoke-static {v1}, Lcom/osp/app/signin/SignInView;->ab(Lcom/osp/app/signin/SignInView;)I

    move-result v1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/osp/app/countrylist/CountryInfoItem;

    .line 7486
    :cond_0
    return-object v0
.end method

.method protected final synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 7269
    check-cast p1, Ljava/lang/Boolean;

    invoke-virtual {p0, p1}, Lcom/osp/app/signin/jm;->a(Ljava/lang/Boolean;)V

    return-void
.end method
