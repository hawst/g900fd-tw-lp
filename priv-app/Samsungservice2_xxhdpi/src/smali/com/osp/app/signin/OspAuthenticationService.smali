.class public Lcom/osp/app/signin/OspAuthenticationService;
.super Lcom/osp/app/util/AbstractBaseService;
.source "OspAuthenticationService.java"


# instance fields
.field private a:Lcom/osp/app/signin/gd;

.field private b:Landroid/content/Context;

.field private final c:Landroid/os/Handler;

.field private d:Ljava/lang/String;
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end field

.field private final e:Ljava/lang/String;
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end field

.field private final f:J


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 51
    invoke-direct {p0}, Lcom/osp/app/util/AbstractBaseService;-><init>()V

    .line 65
    iput-object v1, p0, Lcom/osp/app/signin/OspAuthenticationService;->b:Landroid/content/Context;

    .line 71
    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    iput-object v0, p0, Lcom/osp/app/signin/OspAuthenticationService;->c:Landroid/os/Handler;

    .line 85
    iput-object v1, p0, Lcom/osp/app/signin/OspAuthenticationService;->e:Ljava/lang/String;

    .line 91
    const-wide/16 v0, 0x2710

    iput-wide v0, p0, Lcom/osp/app/signin/OspAuthenticationService;->f:J

    .line 921
    return-void
.end method

.method static synthetic a(Lcom/osp/app/signin/OspAuthenticationService;)Landroid/content/Context;
    .locals 1

    .prologue
    .line 51
    iget-object v0, p0, Lcom/osp/app/signin/OspAuthenticationService;->b:Landroid/content/Context;

    return-object v0
.end method

.method static synthetic b(Lcom/osp/app/signin/OspAuthenticationService;)Landroid/os/Handler;
    .locals 1

    .prologue
    .line 51
    iget-object v0, p0, Lcom/osp/app/signin/OspAuthenticationService;->c:Landroid/os/Handler;

    return-object v0
.end method

.method static synthetic c(Lcom/osp/app/signin/OspAuthenticationService;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 51
    iget-object v0, p0, Lcom/osp/app/signin/OspAuthenticationService;->e:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic d(Lcom/osp/app/signin/OspAuthenticationService;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 51
    iget-object v0, p0, Lcom/osp/app/signin/OspAuthenticationService;->d:Ljava/lang/String;

    return-object v0
.end method


# virtual methods
.method public onBind(Landroid/content/Intent;)Landroid/os/IBinder;
    .locals 3

    .prologue
    .line 154
    const-string v0, "onBind"

    invoke-virtual {p0, p1, v0}, Lcom/osp/app/signin/OspAuthenticationService;->a(Landroid/content/Intent;Ljava/lang/String;)V

    .line 155
    const/4 v0, 0x0

    .line 157
    const-string v1, "android.accounts.AccountAuthenticator"

    invoke-virtual {p1}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 159
    iget-object v0, p0, Lcom/osp/app/signin/OspAuthenticationService;->a:Lcom/osp/app/signin/gd;

    invoke-virtual {v0}, Lcom/osp/app/signin/gd;->getIBinder()Landroid/os/IBinder;

    move-result-object v0

    .line 161
    :cond_0
    return-object v0
.end method

.method public onCreate()V
    .locals 1

    .prologue
    .line 100
    iput-object p0, p0, Lcom/osp/app/signin/OspAuthenticationService;->b:Landroid/content/Context;

    .line 101
    new-instance v0, Lcom/osp/app/signin/gd;

    invoke-direct {v0, p0, p0}, Lcom/osp/app/signin/gd;-><init>(Lcom/osp/app/signin/OspAuthenticationService;Landroid/content/Context;)V

    iput-object v0, p0, Lcom/osp/app/signin/OspAuthenticationService;->a:Lcom/osp/app/signin/gd;

    .line 104
    invoke-virtual {p0}, Lcom/osp/app/signin/OspAuthenticationService;->b()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 106
    invoke-virtual {p0}, Lcom/osp/app/signin/OspAuthenticationService;->c()V

    .line 145
    :goto_0
    return-void

    .line 109
    :cond_0
    invoke-virtual {p0}, Lcom/osp/app/signin/OspAuthenticationService;->d()V

    goto :goto_0
.end method
