.class public Lcom/osp/app/signin/NameValidationWebView;
.super Lcom/osp/app/util/BaseActivity;
.source "NameValidationWebView.java"


# instance fields
.field private A:Ljava/lang/String;

.field private B:Ljava/lang/String;

.field private C:Ljava/lang/String;

.field private D:Ljava/lang/String;

.field private E:Ljava/lang/String;

.field private F:Ljava/lang/String;

.field private G:Ljava/lang/String;

.field private H:Ljava/lang/String;

.field private I:Ljava/lang/String;

.field private J:Ljava/lang/String;

.field private K:Ljava/lang/String;

.field private L:Z

.field private M:I

.field private N:Ljava/lang/String;

.field private O:Ljava/lang/String;

.field private P:J

.field private Q:Z

.field private R:Z

.field private a:Landroid/content/Context;

.field private b:Lcom/osp/app/signin/fz;

.field private c:Lcom/osp/app/signin/ga;

.field private d:Landroid/app/AlertDialog;

.field private e:Landroid/app/ProgressDialog;

.field private f:Landroid/webkit/WebView;

.field private y:Ljava/lang/String;

.field private z:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 48
    invoke-direct {p0}, Lcom/osp/app/util/BaseActivity;-><init>()V

    .line 57
    iput-object v0, p0, Lcom/osp/app/signin/NameValidationWebView;->a:Landroid/content/Context;

    .line 58
    iput-object v0, p0, Lcom/osp/app/signin/NameValidationWebView;->b:Lcom/osp/app/signin/fz;

    .line 59
    iput-object v0, p0, Lcom/osp/app/signin/NameValidationWebView;->c:Lcom/osp/app/signin/ga;

    .line 61
    iput-object v0, p0, Lcom/osp/app/signin/NameValidationWebView;->d:Landroid/app/AlertDialog;

    .line 62
    iput-object v0, p0, Lcom/osp/app/signin/NameValidationWebView;->e:Landroid/app/ProgressDialog;

    .line 64
    iput-object v0, p0, Lcom/osp/app/signin/NameValidationWebView;->f:Landroid/webkit/WebView;

    .line 66
    iput-object v0, p0, Lcom/osp/app/signin/NameValidationWebView;->y:Ljava/lang/String;

    .line 67
    iput-object v0, p0, Lcom/osp/app/signin/NameValidationWebView;->z:Ljava/lang/String;

    .line 68
    iput-object v0, p0, Lcom/osp/app/signin/NameValidationWebView;->A:Ljava/lang/String;

    .line 69
    iput-object v0, p0, Lcom/osp/app/signin/NameValidationWebView;->B:Ljava/lang/String;

    .line 70
    iput-object v0, p0, Lcom/osp/app/signin/NameValidationWebView;->C:Ljava/lang/String;

    .line 71
    iput-object v0, p0, Lcom/osp/app/signin/NameValidationWebView;->D:Ljava/lang/String;

    .line 72
    iput-object v0, p0, Lcom/osp/app/signin/NameValidationWebView;->E:Ljava/lang/String;

    .line 73
    iput-object v0, p0, Lcom/osp/app/signin/NameValidationWebView;->F:Ljava/lang/String;

    .line 74
    iput-object v0, p0, Lcom/osp/app/signin/NameValidationWebView;->G:Ljava/lang/String;

    .line 75
    iput-object v0, p0, Lcom/osp/app/signin/NameValidationWebView;->H:Ljava/lang/String;

    .line 76
    iput-object v0, p0, Lcom/osp/app/signin/NameValidationWebView;->I:Ljava/lang/String;

    .line 77
    iput-object v0, p0, Lcom/osp/app/signin/NameValidationWebView;->J:Ljava/lang/String;

    .line 999
    return-void
.end method

.method static synthetic a(Lcom/osp/app/signin/NameValidationWebView;J)J
    .locals 1

    .prologue
    .line 48
    iput-wide p1, p0, Lcom/osp/app/signin/NameValidationWebView;->P:J

    return-wide p1
.end method

.method static synthetic a(Lcom/osp/app/signin/NameValidationWebView;)Landroid/app/ProgressDialog;
    .locals 1

    .prologue
    .line 48
    iget-object v0, p0, Lcom/osp/app/signin/NameValidationWebView;->e:Landroid/app/ProgressDialog;

    return-object v0
.end method

.method static synthetic a(Lcom/osp/app/signin/NameValidationWebView;Landroid/app/ProgressDialog;)Landroid/app/ProgressDialog;
    .locals 0

    .prologue
    .line 48
    iput-object p1, p0, Lcom/osp/app/signin/NameValidationWebView;->e:Landroid/app/ProgressDialog;

    return-object p1
.end method

.method private a(Ljava/lang/String;)V
    .locals 7

    .prologue
    const/4 v3, 0x0

    const/4 v1, 0x0

    const/4 v6, 0x1

    .line 383
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v2, "parseFromResult params = "

    invoke-direct {v0, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;)V

    .line 385
    iput-object v3, p0, Lcom/osp/app/signin/NameValidationWebView;->y:Ljava/lang/String;

    .line 386
    iput-object v3, p0, Lcom/osp/app/signin/NameValidationWebView;->z:Ljava/lang/String;

    .line 387
    iput-object v3, p0, Lcom/osp/app/signin/NameValidationWebView;->A:Ljava/lang/String;

    .line 388
    iput-object v3, p0, Lcom/osp/app/signin/NameValidationWebView;->B:Ljava/lang/String;

    .line 389
    iput-object v3, p0, Lcom/osp/app/signin/NameValidationWebView;->C:Ljava/lang/String;

    .line 390
    iput-object v3, p0, Lcom/osp/app/signin/NameValidationWebView;->D:Ljava/lang/String;

    .line 391
    iput-object v3, p0, Lcom/osp/app/signin/NameValidationWebView;->E:Ljava/lang/String;

    .line 392
    iput-object v3, p0, Lcom/osp/app/signin/NameValidationWebView;->F:Ljava/lang/String;

    .line 393
    iput-object v3, p0, Lcom/osp/app/signin/NameValidationWebView;->G:Ljava/lang/String;

    .line 394
    iput-object v3, p0, Lcom/osp/app/signin/NameValidationWebView;->H:Ljava/lang/String;

    .line 395
    iput-object v3, p0, Lcom/osp/app/signin/NameValidationWebView;->I:Ljava/lang/String;

    .line 396
    iput-object v3, p0, Lcom/osp/app/signin/NameValidationWebView;->J:Ljava/lang/String;

    .line 398
    if-eqz p1, :cond_11

    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v0

    if-lez v0, :cond_11

    .line 400
    const-string v0, "&"

    invoke-virtual {p1, v0}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v2

    .line 401
    if-eqz v2, :cond_11

    array-length v0, v2

    if-lez v0, :cond_11

    move v0, v1

    .line 403
    :goto_0
    array-length v3, v2

    if-ge v0, v3, :cond_11

    .line 405
    aget-object v3, v2, v0

    const-string v4, "="

    const/4 v5, 0x2

    invoke-virtual {v3, v4, v5}, Ljava/lang/String;->split(Ljava/lang/String;I)[Ljava/lang/String;

    move-result-object v3

    .line 406
    if-eqz v3, :cond_0

    array-length v4, v3

    if-lez v4, :cond_0

    .line 408
    aget-object v4, v3, v1

    const-string v5, "close"

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_1

    .line 410
    aget-object v3, v3, v6

    iput-object v3, p0, Lcom/osp/app/signin/NameValidationWebView;->y:Ljava/lang/String;

    .line 403
    :cond_0
    :goto_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 411
    :cond_1
    aget-object v4, v3, v1

    const-string v5, "familyName"

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_2

    .line 413
    aget-object v3, v3, v6

    invoke-static {v3}, Lcom/osp/app/signin/NameValidationWebView;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    iput-object v3, p0, Lcom/osp/app/signin/NameValidationWebView;->z:Ljava/lang/String;

    goto :goto_1

    .line 414
    :cond_2
    aget-object v4, v3, v1

    const-string v5, "givenName"

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_3

    .line 416
    aget-object v3, v3, v6

    invoke-static {v3}, Lcom/osp/app/signin/NameValidationWebView;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    iput-object v3, p0, Lcom/osp/app/signin/NameValidationWebView;->A:Ljava/lang/String;

    goto :goto_1

    .line 417
    :cond_3
    aget-object v4, v3, v1

    const-string v5, "birthDate"

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_4

    .line 419
    aget-object v3, v3, v6

    iput-object v3, p0, Lcom/osp/app/signin/NameValidationWebView;->B:Ljava/lang/String;

    goto :goto_1

    .line 420
    :cond_4
    aget-object v4, v3, v1

    const-string v5, "genderTypeCode"

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_5

    .line 422
    aget-object v3, v3, v6

    iput-object v3, p0, Lcom/osp/app/signin/NameValidationWebView;->H:Ljava/lang/String;

    goto :goto_1

    .line 423
    :cond_5
    aget-object v4, v3, v1

    const-string v5, "mobileNo"

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_6

    .line 425
    aget-object v3, v3, v6

    iput-object v3, p0, Lcom/osp/app/signin/NameValidationWebView;->C:Ljava/lang/String;

    goto :goto_1

    .line 426
    :cond_6
    aget-object v4, v3, v1

    const-string v5, "nameCheckMethod"

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_c

    .line 428
    aget-object v4, v3, v6

    const-string v5, "IPIN"

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_7

    .line 430
    const-string v3, "2"

    iput-object v3, p0, Lcom/osp/app/signin/NameValidationWebView;->D:Ljava/lang/String;

    goto :goto_1

    .line 431
    :cond_7
    aget-object v4, v3, v6

    const-string v5, "CARD"

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_8

    .line 433
    const-string v3, "3"

    iput-object v3, p0, Lcom/osp/app/signin/NameValidationWebView;->D:Ljava/lang/String;

    goto :goto_1

    .line 434
    :cond_8
    aget-object v4, v3, v6

    const-string v5, "PHONE"

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_9

    .line 436
    const-string v3, "4"

    iput-object v3, p0, Lcom/osp/app/signin/NameValidationWebView;->D:Ljava/lang/String;

    goto/16 :goto_1

    .line 437
    :cond_9
    aget-object v4, v3, v6

    const-string v5, "CN_IDCard"

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_a

    .line 439
    const-string v3, "7"

    iput-object v3, p0, Lcom/osp/app/signin/NameValidationWebView;->D:Ljava/lang/String;

    goto/16 :goto_1

    .line 440
    :cond_a
    aget-object v4, v3, v6

    const-string v5, "CN_MilitaryIDCard"

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_b

    .line 442
    const-string v3, "8"

    iput-object v3, p0, Lcom/osp/app/signin/NameValidationWebView;->D:Ljava/lang/String;

    goto/16 :goto_1

    .line 443
    :cond_b
    aget-object v3, v3, v6

    const-string v4, "CN_PASSPORT"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 445
    const-string v3, "9"

    iput-object v3, p0, Lcom/osp/app/signin/NameValidationWebView;->D:Ljava/lang/String;

    goto/16 :goto_1

    .line 447
    :cond_c
    aget-object v4, v3, v1

    const-string v5, "CI"

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_d

    .line 449
    aget-object v3, v3, v6

    invoke-static {v3}, Lcom/osp/app/signin/NameValidationWebView;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    iput-object v3, p0, Lcom/osp/app/signin/NameValidationWebView;->E:Ljava/lang/String;

    goto/16 :goto_1

    .line 450
    :cond_d
    aget-object v4, v3, v1

    const-string v5, "DI"

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_e

    .line 452
    aget-object v3, v3, v6

    invoke-static {v3}, Lcom/osp/app/signin/NameValidationWebView;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    iput-object v3, p0, Lcom/osp/app/signin/NameValidationWebView;->F:Ljava/lang/String;

    goto/16 :goto_1

    .line 453
    :cond_e
    aget-object v4, v3, v1

    const-string v5, "nameCheckDateTime"

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_f

    .line 455
    aget-object v3, v3, v6

    invoke-static {v3}, Lcom/osp/app/signin/NameValidationWebView;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    iput-object v3, p0, Lcom/osp/app/signin/NameValidationWebView;->G:Ljava/lang/String;

    goto/16 :goto_1

    .line 456
    :cond_f
    aget-object v4, v3, v1

    const-string v5, "foreignerYNFlag"

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_10

    .line 458
    aget-object v3, v3, v6

    invoke-static {v3}, Lcom/osp/app/signin/NameValidationWebView;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    iput-object v3, p0, Lcom/osp/app/signin/NameValidationWebView;->I:Ljava/lang/String;

    goto/16 :goto_1

    .line 459
    :cond_10
    aget-object v4, v3, v1

    const-string v5, "closedAction"

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 461
    aget-object v3, v3, v6

    iput-object v3, p0, Lcom/osp/app/signin/NameValidationWebView;->J:Ljava/lang/String;

    goto/16 :goto_1

    .line 468
    :cond_11
    iget-object v0, p0, Lcom/osp/app/signin/NameValidationWebView;->G:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_12

    .line 470
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    .line 471
    new-instance v2, Ljava/util/Date;

    invoke-direct {v2, v0, v1}, Ljava/util/Date;-><init>(J)V

    .line 472
    new-instance v0, Ljava/text/SimpleDateFormat;

    const-string v1, "yyyyMMddHHmmss"

    invoke-direct {v0, v1}, Ljava/text/SimpleDateFormat;-><init>(Ljava/lang/String;)V

    .line 473
    invoke-virtual {v0, v2}, Ljava/text/SimpleDateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/osp/app/signin/NameValidationWebView;->G:Ljava/lang/String;

    .line 475
    :cond_12
    return-void
.end method

.method static synthetic a(Lcom/osp/app/signin/NameValidationWebView;Ljava/lang/String;)Z
    .locals 1

    .prologue
    .line 48
    invoke-direct {p0, p1}, Lcom/osp/app/signin/NameValidationWebView;->c(Ljava/lang/String;)Z

    move-result v0

    return v0
.end method

.method static synthetic b(Lcom/osp/app/signin/NameValidationWebView;)Landroid/webkit/WebView;
    .locals 1

    .prologue
    .line 48
    iget-object v0, p0, Lcom/osp/app/signin/NameValidationWebView;->f:Landroid/webkit/WebView;

    return-object v0
.end method

.method private static b(Ljava/lang/String;)Ljava/lang/String;
    .locals 11

    .prologue
    const/16 v9, 0x10

    const/4 v8, 0x2

    const/4 v3, 0x0

    .line 484
    const-string v0, "%"

    invoke-virtual {p0, v0}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v4

    .line 486
    const-string v0, ""

    .line 488
    if-eqz v4, :cond_4

    array-length v1, v4

    if-lez v1, :cond_4

    .line 490
    aget-object v1, v4, v3

    if-eqz v1, :cond_0

    aget-object v1, v4, v3

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v1

    if-lez v1, :cond_0

    .line 492
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    aget-object v1, v4, v3

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 495
    :cond_0
    array-length v1, v4

    new-array v5, v1, [B

    .line 496
    const/4 v1, 0x1

    move v2, v3

    move-object v10, v0

    move v0, v1

    move-object v1, v10

    :goto_0
    array-length v6, v4

    if-ge v0, v6, :cond_3

    .line 498
    aget-object v6, v4, v0

    if-eqz v6, :cond_2

    aget-object v6, v4, v0

    invoke-virtual {v6}, Ljava/lang/String;->length()I

    move-result v6

    if-ne v6, v8, :cond_2

    .line 500
    aget-object v6, v4, v0

    invoke-static {v6, v9}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;I)I

    move-result v6

    int-to-byte v6, v6

    aput-byte v6, v5, v2

    .line 501
    add-int/lit8 v2, v2, 0x1

    .line 496
    :cond_1
    :goto_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 504
    :cond_2
    aget-object v6, v4, v0

    invoke-virtual {v6, v3, v8}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v6

    .line 505
    aget-object v7, v4, v0

    invoke-virtual {v7, v8}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v7

    .line 507
    invoke-static {v6, v9}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;I)I

    move-result v6

    int-to-byte v6, v6

    aput-byte v6, v5, v2

    .line 508
    add-int/lit8 v2, v2, 0x1

    .line 510
    if-eqz v7, :cond_1

    invoke-virtual {v7}, Ljava/lang/String;->length()I

    move-result v6

    if-lez v6, :cond_1

    .line 512
    new-array v6, v2, [B

    .line 518
    invoke-static {v5, v3, v6, v3, v2}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 521
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    new-instance v2, Ljava/lang/String;

    invoke-direct {v2, v6}, Ljava/lang/String;-><init>([B)V

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    move v2, v3

    goto :goto_1

    .line 526
    :cond_3
    if-lez v2, :cond_5

    .line 528
    new-array v0, v2, [B

    .line 533
    invoke-static {v5, v3, v0, v3, v2}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 535
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    new-instance v2, Ljava/lang/String;

    invoke-direct {v2, v0}, Ljava/lang/String;-><init>([B)V

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 539
    :cond_4
    :goto_2
    return-object v0

    :cond_5
    move-object v0, v1

    goto :goto_2
.end method

.method private c()Ljava/lang/String;
    .locals 4

    .prologue
    .line 264
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v0, "NVWV"

    const-string v1, "makeNameValidationURL()"

    invoke-static {v0, v1}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 265
    invoke-static {}, Lcom/osp/common/util/i;->a()Lcom/osp/common/util/i;

    invoke-static {}, Lcom/osp/common/util/i;->a()Lcom/osp/common/util/i;

    invoke-static {p0}, Lcom/osp/common/util/i;->f(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/osp/common/util/i;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 266
    invoke-static {}, Lcom/osp/common/util/i;->a()Lcom/osp/common/util/i;

    invoke-static {p0}, Lcom/osp/common/util/i;->l(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v0

    .line 268
    if-eqz v1, :cond_0

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v2

    if-gtz v2, :cond_1

    .line 270
    :cond_0
    invoke-static {p0}, Lcom/osp/app/util/r;->s(Landroid/content/Context;)Z

    move-result v1

    if-eqz v1, :cond_4

    .line 272
    const-string v1, "CN"

    .line 278
    :cond_1
    :goto_0
    if-eqz v0, :cond_2

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v2

    if-gtz v2, :cond_3

    .line 280
    :cond_2
    invoke-static {p0}, Lcom/osp/app/util/r;->s(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_5

    .line 282
    const-string v0, "zh"

    .line 298
    :cond_3
    :goto_1
    const-string v2, "j5p7ll8g33"

    sget-object v3, Ljava/util/Locale;->ENGLISH:Ljava/util/Locale;

    invoke-virtual {v1, v3}, Ljava/lang/String;->toUpperCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v1

    invoke-static {p0, v2, v1, v0}, Lcom/msc/c/n;->b(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0

    .line 275
    :cond_4
    const-string v1, "KR"

    goto :goto_0

    .line 285
    :cond_5
    const-string v0, "ko"

    goto :goto_1
.end method

.method static synthetic c(Lcom/osp/app/signin/NameValidationWebView;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 48
    iget-object v0, p0, Lcom/osp/app/signin/NameValidationWebView;->y:Ljava/lang/String;

    return-object v0
.end method

.method private c(Ljava/lang/String;)Z
    .locals 6

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 549
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "NameValidationWebView::isClosed URL = "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;)V

    .line 551
    const-string v2, "?"

    invoke-virtual {p1, v2}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v2

    .line 552
    if-lez v2, :cond_4

    .line 554
    invoke-virtual {p1, v1, v2}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v3

    .line 555
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "resultURL = "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;)V

    .line 557
    add-int/lit8 v2, v2, 0x1

    invoke-virtual {p1, v2}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v2

    invoke-direct {p0, v2}, Lcom/osp/app/signin/NameValidationWebView;->a(Ljava/lang/String;)V

    .line 559
    iget-object v2, p0, Lcom/osp/app/signin/NameValidationWebView;->J:Ljava/lang/String;

    if-eqz v2, :cond_3

    iget-object v2, p0, Lcom/osp/app/signin/NameValidationWebView;->J:Ljava/lang/String;

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v2

    if-lez v2, :cond_3

    iget-object v2, p0, Lcom/osp/app/signin/NameValidationWebView;->J:Ljava/lang/String;

    const-string v3, "NameCheckSuccess"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    .line 561
    iget-object v1, p0, Lcom/osp/app/signin/NameValidationWebView;->K:Ljava/lang/String;

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/osp/app/signin/NameValidationWebView;->K:Ljava/lang/String;

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v1

    if-lez v1, :cond_2

    .line 563
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "NameValidationWebView::isClosed mUserID = "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, Lcom/osp/app/signin/NameValidationWebView;->K:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;)V

    .line 564
    iget-object v1, p0, Lcom/osp/app/signin/NameValidationWebView;->I:Ljava/lang/String;

    if-eqz v1, :cond_1

    iget-object v1, p0, Lcom/osp/app/signin/NameValidationWebView;->I:Ljava/lang/String;

    const-string v2, "Y"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 566
    new-instance v1, Lcom/osp/app/signin/ga;

    invoke-direct {v1, p0}, Lcom/osp/app/signin/ga;-><init>(Lcom/osp/app/signin/NameValidationWebView;)V

    iput-object v1, p0, Lcom/osp/app/signin/NameValidationWebView;->c:Lcom/osp/app/signin/ga;

    .line 567
    iget-object v1, p0, Lcom/osp/app/signin/NameValidationWebView;->c:Lcom/osp/app/signin/ga;

    invoke-virtual {v1}, Lcom/osp/app/signin/ga;->b()V

    .line 598
    :cond_0
    :goto_0
    return v0

    .line 570
    :cond_1
    new-instance v1, Lcom/osp/app/signin/fz;

    invoke-direct {v1, p0}, Lcom/osp/app/signin/fz;-><init>(Lcom/osp/app/signin/NameValidationWebView;)V

    iput-object v1, p0, Lcom/osp/app/signin/NameValidationWebView;->b:Lcom/osp/app/signin/fz;

    .line 571
    iget-object v1, p0, Lcom/osp/app/signin/NameValidationWebView;->b:Lcom/osp/app/signin/fz;

    invoke-virtual {v1}, Lcom/osp/app/signin/fz;->b()V

    goto :goto_0

    .line 575
    :cond_2
    new-instance v1, Landroid/content/Intent;

    invoke-direct {v1}, Landroid/content/Intent;-><init>()V

    .line 576
    const-string v2, "key_name_check_familyname"

    iget-object v3, p0, Lcom/osp/app/signin/NameValidationWebView;->z:Ljava/lang/String;

    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 577
    const-string v2, "key_name_check_givenname"

    iget-object v3, p0, Lcom/osp/app/signin/NameValidationWebView;->A:Ljava/lang/String;

    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 578
    const-string v2, "key_name_check_birthdate"

    iget-object v3, p0, Lcom/osp/app/signin/NameValidationWebView;->B:Ljava/lang/String;

    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 579
    const-string v2, "key_name_check_mobile"

    iget-object v3, p0, Lcom/osp/app/signin/NameValidationWebView;->C:Ljava/lang/String;

    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 580
    const-string v2, "key_name_check_method"

    iget-object v3, p0, Lcom/osp/app/signin/NameValidationWebView;->D:Ljava/lang/String;

    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 581
    const-string v2, "key_name_check_ci"

    iget-object v3, p0, Lcom/osp/app/signin/NameValidationWebView;->E:Ljava/lang/String;

    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 582
    const-string v2, "key_name_check_di"

    iget-object v3, p0, Lcom/osp/app/signin/NameValidationWebView;->F:Ljava/lang/String;

    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 583
    const-string v2, "key_name_check_datetime"

    iget-object v3, p0, Lcom/osp/app/signin/NameValidationWebView;->G:Ljava/lang/String;

    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 584
    const-string v2, "key_name_check_gender"

    iget-object v3, p0, Lcom/osp/app/signin/NameValidationWebView;->H:Ljava/lang/String;

    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 585
    const-string v2, "key_name_check_foreigner"

    iget-object v3, p0, Lcom/osp/app/signin/NameValidationWebView;->I:Ljava/lang/String;

    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 587
    const/4 v2, -0x1

    invoke-virtual {p0, v2, v1}, Lcom/osp/app/signin/NameValidationWebView;->a(ILandroid/content/Intent;)V

    .line 588
    invoke-virtual {p0}, Lcom/osp/app/signin/NameValidationWebView;->finish()V

    goto :goto_0

    .line 591
    :cond_3
    iget-object v2, p0, Lcom/osp/app/signin/NameValidationWebView;->y:Ljava/lang/String;

    if-eqz v2, :cond_4

    iget-object v2, p0, Lcom/osp/app/signin/NameValidationWebView;->y:Ljava/lang/String;

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v2

    if-lez v2, :cond_4

    iget-object v2, p0, Lcom/osp/app/signin/NameValidationWebView;->y:Ljava/lang/String;

    const-string v3, "true"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_4

    .line 593
    invoke-virtual {p0}, Lcom/osp/app/signin/NameValidationWebView;->isFinishing()Z

    move-result v1

    if-nez v1, :cond_0

    :try_start_0
    new-instance v1, Landroid/app/AlertDialog$Builder;

    invoke-direct {v1, p0}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    const v2, 0x7f0900a9

    invoke-virtual {v1, v2}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    const v2, 0x7f0900aa

    invoke-virtual {v1, v2}, Landroid/app/AlertDialog$Builder;->setMessage(I)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    const v2, 0x7f090042

    new-instance v3, Lcom/osp/app/signin/fy;

    invoke-direct {v3, p0}, Lcom/osp/app/signin/fy;-><init>(Lcom/osp/app/signin/NameValidationWebView;)V

    invoke-virtual {v1, v2, v3}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    new-instance v2, Lcom/osp/app/signin/fx;

    invoke-direct {v2, p0}, Lcom/osp/app/signin/fx;-><init>(Lcom/osp/app/signin/NameValidationWebView;)V

    invoke-virtual {v1, v2}, Landroid/app/AlertDialog$Builder;->setOnCancelListener(Landroid/content/DialogInterface$OnCancelListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    invoke-virtual {v1}, Landroid/app/AlertDialog$Builder;->show()Landroid/app/AlertDialog;

    move-result-object v1

    iput-object v1, p0, Lcom/osp/app/signin/NameValidationWebView;->d:Landroid/app/AlertDialog;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto/16 :goto_0

    :catch_0
    move-exception v1

    invoke-virtual {v1}, Ljava/lang/Exception;->printStackTrace()V

    goto/16 :goto_0

    :cond_4
    move v0, v1

    .line 598
    goto/16 :goto_0
.end method

.method private d()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 363
    :try_start_0
    invoke-virtual {p0}, Lcom/osp/app/signin/NameValidationWebView;->isFinishing()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/osp/app/signin/NameValidationWebView;->d:Landroid/app/AlertDialog;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/osp/app/signin/NameValidationWebView;->d:Landroid/app/AlertDialog;

    invoke-virtual {v0}, Landroid/app/AlertDialog;->isShowing()Z

    move-result v0

    const/4 v1, 0x1

    if-ne v0, v1, :cond_0

    .line 365
    iget-object v0, p0, Lcom/osp/app/signin/NameValidationWebView;->d:Landroid/app/AlertDialog;

    invoke-virtual {v0}, Landroid/app/AlertDialog;->dismiss()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 372
    :cond_0
    :goto_0
    iput-object v2, p0, Lcom/osp/app/signin/NameValidationWebView;->d:Landroid/app/AlertDialog;

    .line 373
    return-void

    .line 367
    :catch_0
    move-exception v0

    :try_start_1
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 372
    :catchall_0
    move-exception v0

    iput-object v2, p0, Lcom/osp/app/signin/NameValidationWebView;->d:Landroid/app/AlertDialog;

    throw v0
.end method

.method static synthetic d(Lcom/osp/app/signin/NameValidationWebView;)V
    .locals 0

    .prologue
    .line 48
    invoke-direct {p0}, Lcom/osp/app/signin/NameValidationWebView;->d()V

    return-void
.end method

.method static synthetic e(Lcom/osp/app/signin/NameValidationWebView;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 48
    invoke-direct {p0}, Lcom/osp/app/signin/NameValidationWebView;->c()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method static synthetic f(Lcom/osp/app/signin/NameValidationWebView;)Landroid/content/Context;
    .locals 1

    .prologue
    .line 48
    iget-object v0, p0, Lcom/osp/app/signin/NameValidationWebView;->a:Landroid/content/Context;

    return-object v0
.end method

.method static synthetic g(Lcom/osp/app/signin/NameValidationWebView;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 48
    iget-object v0, p0, Lcom/osp/app/signin/NameValidationWebView;->K:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic h(Lcom/osp/app/signin/NameValidationWebView;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 48
    iget-object v0, p0, Lcom/osp/app/signin/NameValidationWebView;->z:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic i(Lcom/osp/app/signin/NameValidationWebView;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 48
    iget-object v0, p0, Lcom/osp/app/signin/NameValidationWebView;->A:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic j(Lcom/osp/app/signin/NameValidationWebView;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 48
    iget-object v0, p0, Lcom/osp/app/signin/NameValidationWebView;->B:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic k(Lcom/osp/app/signin/NameValidationWebView;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 48
    iget-object v0, p0, Lcom/osp/app/signin/NameValidationWebView;->D:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic l(Lcom/osp/app/signin/NameValidationWebView;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 48
    iget-object v0, p0, Lcom/osp/app/signin/NameValidationWebView;->E:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic m(Lcom/osp/app/signin/NameValidationWebView;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 48
    iget-object v0, p0, Lcom/osp/app/signin/NameValidationWebView;->F:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic n(Lcom/osp/app/signin/NameValidationWebView;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 48
    iget-object v0, p0, Lcom/osp/app/signin/NameValidationWebView;->G:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic o(Lcom/osp/app/signin/NameValidationWebView;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 48
    iget-object v0, p0, Lcom/osp/app/signin/NameValidationWebView;->H:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic p(Lcom/osp/app/signin/NameValidationWebView;)J
    .locals 2

    .prologue
    .line 48
    iget-wide v0, p0, Lcom/osp/app/signin/NameValidationWebView;->P:J

    return-wide v0
.end method

.method static synthetic q(Lcom/osp/app/signin/NameValidationWebView;)I
    .locals 1

    .prologue
    .line 48
    iget v0, p0, Lcom/osp/app/signin/NameValidationWebView;->M:I

    return v0
.end method

.method static synthetic r(Lcom/osp/app/signin/NameValidationWebView;)I
    .locals 1

    .prologue
    .line 48
    iget v0, p0, Lcom/osp/app/signin/NameValidationWebView;->M:I

    xor-int/lit8 v0, v0, 0x4

    iput v0, p0, Lcom/osp/app/signin/NameValidationWebView;->M:I

    return v0
.end method

.method static synthetic s(Lcom/osp/app/signin/NameValidationWebView;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 48
    iget-object v0, p0, Lcom/osp/app/signin/NameValidationWebView;->N:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic t(Lcom/osp/app/signin/NameValidationWebView;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 48
    iget-object v0, p0, Lcom/osp/app/signin/NameValidationWebView;->O:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic u(Lcom/osp/app/signin/NameValidationWebView;)Z
    .locals 1

    .prologue
    .line 48
    iget-boolean v0, p0, Lcom/osp/app/signin/NameValidationWebView;->R:Z

    return v0
.end method

.method static synthetic v(Lcom/osp/app/signin/NameValidationWebView;)Z
    .locals 1

    .prologue
    .line 48
    iget-boolean v0, p0, Lcom/osp/app/signin/NameValidationWebView;->Q:Z

    return v0
.end method

.method static synthetic w(Lcom/osp/app/signin/NameValidationWebView;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 48
    iget-object v0, p0, Lcom/osp/app/signin/NameValidationWebView;->C:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic x(Lcom/osp/app/signin/NameValidationWebView;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 48
    iget-object v0, p0, Lcom/osp/app/signin/NameValidationWebView;->I:Ljava/lang/String;

    return-object v0
.end method


# virtual methods
.method protected final a()V
    .locals 0

    .prologue
    .line 1178
    return-void
.end method

.method protected final b()V
    .locals 0

    .prologue
    .line 1184
    return-void
.end method

.method protected onActivityResult(IILandroid/content/Intent;)V
    .locals 2

    .prologue
    .line 603
    invoke-super {p0, p1, p2, p3}, Lcom/osp/app/util/BaseActivity;->onActivityResult(IILandroid/content/Intent;)V

    .line 604
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "NameValidationWebView::onActivityResult requestCode : "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ",resultCode : "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;)V

    .line 606
    const/16 v0, 0xdc

    if-ne p1, v0, :cond_0

    .line 608
    invoke-virtual {p0, p2, p3}, Lcom/osp/app/signin/NameValidationWebView;->a(ILandroid/content/Intent;)V

    .line 609
    invoke-virtual {p0}, Lcom/osp/app/signin/NameValidationWebView;->finish()V

    .line 612
    :cond_0
    packed-switch p2, :pswitch_data_0

    .line 617
    :goto_0
    return-void

    .line 615
    :pswitch_0
    const/16 v0, 0xa

    invoke-virtual {p0, v0}, Lcom/osp/app/signin/NameValidationWebView;->b(I)V

    .line 616
    invoke-virtual {p0}, Lcom/osp/app/signin/NameValidationWebView;->finish()V

    goto :goto_0

    .line 612
    nop

    :pswitch_data_0
    .packed-switch 0xa
        :pswitch_0
    .end packed-switch
.end method

.method public onBackPressed()V
    .locals 3

    .prologue
    .line 693
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v0, "NVWV"

    const-string v1, "onBackPressed"

    invoke-static {v0, v1}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 695
    invoke-virtual {p0}, Lcom/osp/app/signin/NameValidationWebView;->getIntent()Landroid/content/Intent;

    move-result-object v0

    .line 696
    if-eqz v0, :cond_0

    const-string v1, "is_cancelable_just_one_activity"

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 698
    const/16 v1, 0xe

    invoke-virtual {p0, v1, v0}, Lcom/osp/app/signin/NameValidationWebView;->a(ILandroid/content/Intent;)V

    .line 700
    :cond_0
    invoke-super {p0}, Lcom/osp/app/util/BaseActivity;->onBackPressed()V

    .line 701
    return-void
.end method

.method public onConfigurationChanged(Landroid/content/res/Configuration;)V
    .locals 2

    .prologue
    .line 95
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v0, "NVWV"

    const-string v1, "onConfigurationChanged()"

    invoke-static {v0, v1}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 97
    invoke-super {p0, p1}, Lcom/osp/app/util/BaseActivity;->onConfigurationChanged(Landroid/content/res/Configuration;)V

    .line 98
    return-void
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 5
    .annotation build Landroid/annotation/SuppressLint;
        value = {
            "SetJavaScriptEnabled"
        }
    .end annotation

    .prologue
    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 103
    invoke-super {p0, p1}, Lcom/osp/app/util/BaseActivity;->onCreate(Landroid/os/Bundle;)V

    .line 105
    invoke-virtual {p0}, Lcom/osp/app/signin/NameValidationWebView;->getIntent()Landroid/content/Intent;

    move-result-object v1

    .line 107
    invoke-static {p0}, Lcom/osp/app/util/ad;->d(Landroid/content/Context;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 109
    if-eqz v1, :cond_0

    const-string v0, "is_signup_flow"

    invoke-virtual {v1, v0, v3}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 111
    sget-object v0, Lcom/osp/app/loggingservice/c;->c:Lcom/osp/app/loggingservice/c;

    invoke-static {v0}, Lcom/osp/app/loggingservice/LoggingService;->a(Lcom/osp/app/loggingservice/c;)V

    .line 115
    :cond_0
    invoke-virtual {p0}, Lcom/osp/app/signin/NameValidationWebView;->getApplication()Landroid/app/Application;

    move-result-object v0

    check-cast v0, Lcom/osp/app/signin/SamsungService;

    invoke-virtual {v0, p0}, Lcom/osp/app/signin/SamsungService;->a(Landroid/app/Activity;)V

    .line 117
    invoke-virtual {v1}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_1

    invoke-virtual {v1}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    const-string v1, "com.msc.action.samsungaccount.namevalidate_external"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 120
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v0, "external value check START"

    invoke-static {v0}, Lcom/osp/app/util/aq;->b(Ljava/lang/String;)V

    .line 122
    const/4 v0, 0x5

    new-array v0, v0, [Ljava/lang/String;

    const-string v1, "client_id"

    aput-object v1, v0, v3

    const-string v1, "client_secret"

    aput-object v1, v0, v4

    const/4 v1, 0x2

    const-string v2, "key_user_id"

    aput-object v2, v0, v1

    const/4 v1, 0x3

    const-string v2, "check_list"

    aput-object v2, v0, v1

    const/4 v1, 0x4

    const-string v2, "key_no_notification"

    aput-object v2, v0, v1

    .line 125
    invoke-virtual {p0, v0}, Lcom/osp/app/signin/NameValidationWebView;->a([Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 219
    :goto_0
    return-void

    .line 132
    :cond_1
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v0, "NVWV"

    const-string v1, "onCreate()"

    invoke-static {v0, v1}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 135
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0xe

    if-ge v0, v1, :cond_3

    .line 137
    invoke-virtual {p0, v4}, Lcom/osp/app/signin/NameValidationWebView;->requestWindowFeature(I)Z

    .line 156
    :goto_1
    const v0, 0x7f030024

    invoke-virtual {p0, v0}, Lcom/osp/app/signin/NameValidationWebView;->setContentView(I)V

    .line 158
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v0, "NVWV"

    const-string v1, "initComponent"

    const-string v2, "START"

    invoke-static {v0, v1, v2}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    iput-object p0, p0, Lcom/osp/app/signin/NameValidationWebView;->a:Landroid/content/Context;

    const v0, 0x7f0c00fd

    invoke-virtual {p0, v0}, Lcom/osp/app/signin/NameValidationWebView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/webkit/WebView;

    iput-object v0, p0, Lcom/osp/app/signin/NameValidationWebView;->f:Landroid/webkit/WebView;

    invoke-virtual {p0}, Lcom/osp/app/signin/NameValidationWebView;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string v1, "key_user_id"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/osp/app/signin/NameValidationWebView;->K:Ljava/lang/String;

    const-string v1, "is_signup_flow"

    invoke-virtual {v0, v1, v3}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v1

    iput-boolean v1, p0, Lcom/osp/app/signin/NameValidationWebView;->L:Z

    const-string v1, "check_list"

    invoke-virtual {v0, v1, v3}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v1

    iput v1, p0, Lcom/osp/app/signin/NameValidationWebView;->M:I

    const-string v1, "client_id"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/osp/app/signin/NameValidationWebView;->N:Ljava/lang/String;

    const-string v1, "client_secret"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/osp/app/signin/NameValidationWebView;->O:Ljava/lang/String;

    const-string v1, "key_return_result"

    invoke-virtual {v0, v1, v3}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v1

    iput-boolean v1, p0, Lcom/osp/app/signin/NameValidationWebView;->Q:Z

    const-string v1, "key_no_notification"

    invoke-virtual {v0, v1, v3}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v0

    iput-boolean v0, p0, Lcom/osp/app/signin/NameValidationWebView;->R:Z

    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v0, "NVWV"

    const-string v1, "initComponent"

    const-string v2, "END"

    invoke-static {v0, v1, v2}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 160
    iget-object v0, p0, Lcom/osp/app/signin/NameValidationWebView;->e:Landroid/app/ProgressDialog;

    if-nez v0, :cond_2

    .line 162
    new-instance v0, Landroid/app/ProgressDialog;

    iget-object v1, p0, Lcom/osp/app/signin/NameValidationWebView;->a:Landroid/content/Context;

    invoke-direct {v0, v1}, Landroid/app/ProgressDialog;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/osp/app/signin/NameValidationWebView;->e:Landroid/app/ProgressDialog;

    .line 163
    iget-object v0, p0, Lcom/osp/app/signin/NameValidationWebView;->e:Landroid/app/ProgressDialog;

    iget-object v1, p0, Lcom/osp/app/signin/NameValidationWebView;->a:Landroid/content/Context;

    const v2, 0x7f090036

    invoke-static {v1, v2}, Lcom/msc/sa/c/d;->a(Landroid/content/Context;I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/app/ProgressDialog;->setMessage(Ljava/lang/CharSequence;)V

    .line 164
    iget-object v0, p0, Lcom/osp/app/signin/NameValidationWebView;->e:Landroid/app/ProgressDialog;

    invoke-virtual {v0, v3}, Landroid/app/ProgressDialog;->setCancelable(Z)V

    .line 165
    iget-object v0, p0, Lcom/osp/app/signin/NameValidationWebView;->e:Landroid/app/ProgressDialog;

    new-instance v1, Lcom/osp/app/signin/fw;

    invoke-direct {v1, p0}, Lcom/osp/app/signin/fw;-><init>(Lcom/osp/app/signin/NameValidationWebView;)V

    invoke-virtual {v0, v1}, Landroid/app/ProgressDialog;->setOnKeyListener(Landroid/content/DialogInterface$OnKeyListener;)V

    .line 202
    :try_start_0
    invoke-virtual {p0}, Lcom/osp/app/signin/NameValidationWebView;->isFinishing()Z

    move-result v0

    if-nez v0, :cond_2

    .line 204
    iget-object v0, p0, Lcom/osp/app/signin/NameValidationWebView;->e:Landroid/app/ProgressDialog;

    invoke-virtual {v0}, Landroid/app/ProgressDialog;->show()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 212
    :cond_2
    :goto_2
    iget-object v0, p0, Lcom/osp/app/signin/NameValidationWebView;->f:Landroid/webkit/WebView;

    invoke-virtual {v0}, Landroid/webkit/WebView;->getSettings()Landroid/webkit/WebSettings;

    move-result-object v0

    invoke-virtual {v0, v4}, Landroid/webkit/WebSettings;->setJavaScriptEnabled(Z)V

    .line 213
    iget-object v0, p0, Lcom/osp/app/signin/NameValidationWebView;->f:Landroid/webkit/WebView;

    new-instance v1, Lcom/osp/app/signin/gb;

    invoke-direct {v1, p0, v3}, Lcom/osp/app/signin/gb;-><init>(Lcom/osp/app/signin/NameValidationWebView;B)V

    invoke-virtual {v0, v1}, Landroid/webkit/WebView;->setWebViewClient(Landroid/webkit/WebViewClient;)V

    .line 217
    iget-object v0, p0, Lcom/osp/app/signin/NameValidationWebView;->f:Landroid/webkit/WebView;

    invoke-virtual {v0}, Landroid/webkit/WebView;->clearHistory()V

    .line 218
    iget-object v0, p0, Lcom/osp/app/signin/NameValidationWebView;->f:Landroid/webkit/WebView;

    invoke-direct {p0}, Lcom/osp/app/signin/NameValidationWebView;->c()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/webkit/WebView;->loadUrl(Ljava/lang/String;)V

    goto/16 :goto_0

    .line 147
    :cond_3
    invoke-virtual {p0}, Lcom/osp/app/signin/NameValidationWebView;->h()Z

    move-result v0

    if-eqz v0, :cond_4

    .line 149
    invoke-virtual {p0}, Lcom/osp/app/signin/NameValidationWebView;->i()V

    goto/16 :goto_1

    .line 152
    :cond_4
    invoke-virtual {p0}, Lcom/osp/app/signin/NameValidationWebView;->j()V

    goto/16 :goto_1

    .line 206
    :catch_0
    move-exception v0

    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_2
.end method

.method protected onDestroy()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 626
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v0, "NVWV"

    const-string v1, "onDestroy"

    invoke-static {v0, v1}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 628
    iget-object v0, p0, Lcom/osp/app/signin/NameValidationWebView;->f:Landroid/webkit/WebView;

    if-eqz v0, :cond_0

    .line 630
    iget-object v0, p0, Lcom/osp/app/signin/NameValidationWebView;->f:Landroid/webkit/WebView;

    invoke-virtual {v0}, Landroid/webkit/WebView;->clearHistory()V

    .line 631
    iget-object v0, p0, Lcom/osp/app/signin/NameValidationWebView;->f:Landroid/webkit/WebView;

    invoke-virtual {v0}, Landroid/webkit/WebView;->destroy()V

    .line 632
    iput-object v2, p0, Lcom/osp/app/signin/NameValidationWebView;->f:Landroid/webkit/WebView;

    .line 635
    :cond_0
    invoke-direct {p0}, Lcom/osp/app/signin/NameValidationWebView;->d()V

    .line 637
    iget-object v0, p0, Lcom/osp/app/signin/NameValidationWebView;->e:Landroid/app/ProgressDialog;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/osp/app/signin/NameValidationWebView;->e:Landroid/app/ProgressDialog;

    invoke-virtual {v0}, Landroid/app/ProgressDialog;->isShowing()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 639
    iget-object v0, p0, Lcom/osp/app/signin/NameValidationWebView;->e:Landroid/app/ProgressDialog;

    invoke-virtual {v0}, Landroid/app/ProgressDialog;->dismiss()V

    .line 640
    iput-object v2, p0, Lcom/osp/app/signin/NameValidationWebView;->e:Landroid/app/ProgressDialog;

    .line 643
    :cond_1
    iget v0, p0, Lcom/osp/app/signin/NameValidationWebView;->M:I

    if-nez v0, :cond_2

    .line 645
    invoke-static {p0}, Lcom/osp/app/util/r;->t(Landroid/content/Context;)Z

    move-result v0

    if-nez v0, :cond_2

    .line 647
    const-string v0, "notification"

    invoke-virtual {p0, v0}, Lcom/osp/app/signin/NameValidationWebView;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/NotificationManager;

    .line 648
    const v1, 0x132df82

    invoke-virtual {v0, v1}, Landroid/app/NotificationManager;->cancel(I)V

    .line 652
    :cond_2
    invoke-super {p0}, Lcom/osp/app/util/BaseActivity;->onDestroy()V

    .line 653
    return-void
.end method

.method protected onPause()V
    .locals 2

    .prologue
    .line 657
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v0, "NVWV"

    const-string v1, "onPause"

    invoke-static {v0, v1}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 659
    invoke-super {p0}, Lcom/osp/app/util/BaseActivity;->onPause()V

    .line 660
    return-void
.end method

.method protected onResume()V
    .locals 2

    .prologue
    .line 664
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v0, "NVWV"

    const-string v1, "onResume"

    invoke-static {v0, v1}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 666
    iget-boolean v0, p0, Lcom/osp/app/signin/NameValidationWebView;->L:Z

    if-eqz v0, :cond_0

    invoke-static {p0}, Lcom/osp/app/util/ad;->d(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 678
    invoke-virtual {p0}, Lcom/osp/app/signin/NameValidationWebView;->finish()V

    .line 681
    :cond_0
    invoke-super {p0}, Lcom/osp/app/util/BaseActivity;->onResume()V

    .line 682
    return-void
.end method

.method protected onStop()V
    .locals 2

    .prologue
    .line 686
    invoke-super {p0}, Lcom/osp/app/util/BaseActivity;->onStop()V

    .line 688
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v0, "NVWV"

    const-string v1, "onStop"

    invoke-static {v0, v1}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 689
    return-void
.end method
