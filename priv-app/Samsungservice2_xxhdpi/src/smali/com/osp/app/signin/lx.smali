.class final Lcom/osp/app/signin/lx;
.super Lcom/msc/c/b;
.source "SignUpView.java"


# instance fields
.field final synthetic c:Lcom/osp/app/signin/SignUpView;

.field private d:I

.field private final e:Lcom/osp/app/signin/fm;

.field private f:J

.field private g:Z


# direct methods
.method public constructor <init>(Lcom/osp/app/signin/SignUpView;Lcom/osp/app/signin/fm;)V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 9432
    iput-object p1, p0, Lcom/osp/app/signin/lx;->c:Lcom/osp/app/signin/SignUpView;

    .line 9433
    invoke-direct {p0, p1}, Lcom/msc/c/b;-><init>(Landroid/content/Context;)V

    .line 9411
    iput v0, p0, Lcom/osp/app/signin/lx;->d:I

    .line 9425
    iput-boolean v0, p0, Lcom/osp/app/signin/lx;->g:Z

    .line 9434
    iput-object p2, p0, Lcom/osp/app/signin/lx;->e:Lcom/osp/app/signin/fm;

    .line 9435
    return-void
.end method

.method private b(Ljava/lang/String;)V
    .locals 3

    .prologue
    .line 9599
    invoke-static {}, Lcom/msc/c/g;->a()Lcom/msc/c/g;

    .line 9601
    iget-object v0, p0, Lcom/osp/app/signin/lx;->c:Lcom/osp/app/signin/SignUpView;

    const-string v1, "password"

    invoke-static {v0, p1, v1, p0}, Lcom/msc/c/g;->e(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Lcom/msc/b/h;)J

    move-result-wide v0

    iput-wide v0, p0, Lcom/osp/app/signin/lx;->f:J

    .line 9602
    iget-wide v0, p0, Lcom/osp/app/signin/lx;->f:J

    invoke-virtual {p0, v0, v1}, Lcom/osp/app/signin/lx;->a(J)V

    .line 9603
    iget-wide v0, p0, Lcom/osp/app/signin/lx;->f:J

    const-string v2, "verify_xml"

    invoke-virtual {p0, v0, v1, v2}, Lcom/osp/app/signin/lx;->a(JLjava/lang/String;)V

    .line 9605
    iget-wide v0, p0, Lcom/osp/app/signin/lx;->f:J

    sget-object v2, Lcom/msc/b/g;->c:Lcom/msc/b/g;

    invoke-static {v0, v1, v2}, Lcom/msc/c/g;->a(JLcom/msc/b/g;)V

    .line 9606
    return-void
.end method


# virtual methods
.method protected final varargs a()Ljava/lang/Boolean;
    .locals 1

    .prologue
    .line 9439
    invoke-static {}, Lcom/osp/device/b;->b()Lcom/osp/device/b;

    move-result-object v0

    invoke-virtual {v0}, Lcom/osp/device/b;->a()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 9441
    iget-object v0, p0, Lcom/osp/app/signin/lx;->e:Lcom/osp/app/signin/fm;

    iget-object v0, v0, Lcom/osp/app/signin/fm;->e:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/osp/app/signin/lx;->e:Lcom/osp/app/signin/fm;

    iget-object v0, v0, Lcom/osp/app/signin/fm;->d:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 9443
    iget-object v0, p0, Lcom/osp/app/signin/lx;->e:Lcom/osp/app/signin/fm;

    iget-object v0, v0, Lcom/osp/app/signin/fm;->e:Ljava/lang/String;

    invoke-direct {p0, v0}, Lcom/osp/app/signin/lx;->b(Ljava/lang/String;)V

    .line 9444
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/osp/app/signin/lx;->g:Z

    .line 9453
    :goto_0
    const/4 v0, 0x0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    return-object v0

    .line 9447
    :cond_0
    iget-object v0, p0, Lcom/osp/app/signin/lx;->e:Lcom/osp/app/signin/fm;

    iget-object v0, v0, Lcom/osp/app/signin/fm;->d:Ljava/lang/String;

    invoke-direct {p0, v0}, Lcom/osp/app/signin/lx;->b(Ljava/lang/String;)V

    goto :goto_0

    .line 9451
    :cond_1
    iget-object v0, p0, Lcom/osp/app/signin/lx;->e:Lcom/osp/app/signin/fm;

    iget-object v0, v0, Lcom/osp/app/signin/fm;->d:Ljava/lang/String;

    invoke-direct {p0, v0}, Lcom/osp/app/signin/lx;->b(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public final declared-synchronized a(Lcom/msc/b/c;)V
    .locals 4

    .prologue
    .line 9545
    monitor-enter p0

    :try_start_0
    invoke-super {p0, p1}, Lcom/msc/c/b;->a(Lcom/msc/b/c;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 9547
    if-nez p1, :cond_1

    .line 9558
    :cond_0
    :goto_0
    monitor-exit p0

    return-void

    .line 9552
    :cond_1
    :try_start_1
    invoke-virtual {p1}, Lcom/msc/b/c;->a()J

    move-result-wide v0

    .line 9554
    iget-wide v2, p0, Lcom/osp/app/signin/lx;->f:J

    cmp-long v0, v0, v2

    if-nez v0, :cond_0

    .line 9556
    const/4 v0, 0x2

    iput v0, p0, Lcom/osp/app/signin/lx;->d:I
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 9545
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method protected final a(Ljava/lang/Boolean;)V
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 9458
    invoke-super {p0, p1}, Lcom/msc/c/b;->a(Ljava/lang/Boolean;)V

    .line 9459
    iget-object v0, p0, Lcom/osp/app/signin/lx;->e:Lcom/osp/app/signin/fm;

    if-eqz v0, :cond_7

    .line 9461
    iget v0, p0, Lcom/osp/app/signin/lx;->d:I

    packed-switch v0, :pswitch_data_0

    .line 9534
    invoke-virtual {p0, v1}, Lcom/osp/app/signin/lx;->a(Z)V

    .line 9541
    :cond_0
    :goto_0
    return-void

    .line 9464
    :pswitch_0
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v0, "SUV"

    const-string v1, "facebook email already exist in samsung account"

    invoke-static {v0, v1}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 9465
    invoke-static {}, Lcom/osp/device/b;->b()Lcom/osp/device/b;

    move-result-object v0

    invoke-virtual {v0}, Lcom/osp/device/b;->a()Z

    move-result v0

    if-eqz v0, :cond_1

    iget-boolean v0, p0, Lcom/osp/app/signin/lx;->g:Z

    if-eqz v0, :cond_1

    .line 9467
    iget-object v0, p0, Lcom/osp/app/signin/lx;->c:Lcom/osp/app/signin/SignUpView;

    iget-object v1, p0, Lcom/osp/app/signin/lx;->e:Lcom/osp/app/signin/fm;

    iget-object v1, v1, Lcom/osp/app/signin/fm;->e:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/osp/app/signin/SignUpView;->b(Ljava/lang/String;)V

    goto :goto_0

    .line 9470
    :cond_1
    iget-object v0, p0, Lcom/osp/app/signin/lx;->c:Lcom/osp/app/signin/SignUpView;

    iget-object v1, p0, Lcom/osp/app/signin/lx;->e:Lcom/osp/app/signin/fm;

    iget-object v1, v1, Lcom/osp/app/signin/fm;->d:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/osp/app/signin/SignUpView;->b(Ljava/lang/String;)V

    goto :goto_0

    .line 9476
    :pswitch_1
    :try_start_0
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v0, "SUV"

    const-string v1, "set facebook email address"

    invoke-static {v0, v1}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 9477
    invoke-static {}, Lcom/osp/device/b;->b()Lcom/osp/device/b;

    move-result-object v0

    invoke-virtual {v0}, Lcom/osp/device/b;->a()Z

    move-result v0

    if-eqz v0, :cond_6

    .line 9479
    iget-object v0, p0, Lcom/osp/app/signin/lx;->c:Lcom/osp/app/signin/SignUpView;

    const v1, 0x7f0c0086

    invoke-virtual {v0, v1}, Lcom/osp/app/signin/SignUpView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    .line 9480
    iget-object v1, p0, Lcom/osp/app/signin/lx;->c:Lcom/osp/app/signin/SignUpView;

    const v2, 0x7f0c0088

    invoke-virtual {v1, v2}, Lcom/osp/app/signin/SignUpView;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/Button;

    .line 9481
    iget-boolean v2, p0, Lcom/osp/app/signin/lx;->g:Z

    if-eqz v2, :cond_5

    .line 9483
    const/4 v2, 0x0

    invoke-virtual {v0, v2}, Landroid/widget/Button;->setSelected(Z)V

    .line 9484
    const/4 v0, 0x1

    invoke-virtual {v1, v0}, Landroid/widget/Button;->setSelected(Z)V

    .line 9485
    iget-object v0, p0, Lcom/osp/app/signin/lx;->c:Lcom/osp/app/signin/SignUpView;

    const/16 v1, 0x12d

    invoke-static {v0, v1}, Lcom/osp/app/signin/SignUpView;->g(Lcom/osp/app/signin/SignUpView;I)V

    .line 9486
    iget-object v0, p0, Lcom/osp/app/signin/lx;->c:Lcom/osp/app/signin/SignUpView;

    iget-object v1, p0, Lcom/osp/app/signin/lx;->e:Lcom/osp/app/signin/fm;

    iget-object v1, v1, Lcom/osp/app/signin/fm;->e:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/osp/app/signin/SignUpView;->g(Lcom/osp/app/signin/SignUpView;Ljava/lang/String;)Ljava/lang/String;

    .line 9487
    iget-object v0, p0, Lcom/osp/app/signin/lx;->c:Lcom/osp/app/signin/SignUpView;

    invoke-static {v0}, Lcom/osp/app/signin/SignUpView;->p(Lcom/osp/app/signin/SignUpView;)Landroid/widget/AutoCompleteTextView;

    move-result-object v0

    iget-object v1, p0, Lcom/osp/app/signin/lx;->c:Lcom/osp/app/signin/SignUpView;

    invoke-static {v1}, Lcom/osp/app/signin/SignUpView;->ai(Lcom/osp/app/signin/SignUpView;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/AutoCompleteTextView;->setText(Ljava/lang/CharSequence;)V

    .line 9496
    :goto_1
    iget-object v0, p0, Lcom/osp/app/signin/lx;->c:Lcom/osp/app/signin/SignUpView;

    invoke-static {v0}, Lcom/osp/app/signin/SignUpView;->x(Lcom/osp/app/signin/SignUpView;)V

    .line 9502
    :goto_2
    iget-object v0, p0, Lcom/osp/app/signin/lx;->c:Lcom/osp/app/signin/SignUpView;

    invoke-static {v0}, Lcom/osp/app/signin/SignUpView;->p(Lcom/osp/app/signin/SignUpView;)Landroid/widget/AutoCompleteTextView;

    move-result-object v0

    iget-object v1, p0, Lcom/osp/app/signin/lx;->c:Lcom/osp/app/signin/SignUpView;

    invoke-static {v1}, Lcom/osp/app/signin/SignUpView;->p(Lcom/osp/app/signin/SignUpView;)Landroid/widget/AutoCompleteTextView;

    move-result-object v1

    invoke-virtual {v1}, Landroid/widget/AutoCompleteTextView;->length()I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/AutoCompleteTextView;->setSelection(I)V

    .line 9504
    iget-object v0, p0, Lcom/osp/app/signin/lx;->c:Lcom/osp/app/signin/SignUpView;

    invoke-static {v0}, Lcom/osp/app/signin/SignUpView;->d(Lcom/osp/app/signin/SignUpView;)Z

    move-result v0

    if-eqz v0, :cond_2

    const-string v0, "Y"

    iget-object v1, p0, Lcom/osp/app/signin/lx;->c:Lcom/osp/app/signin/SignUpView;

    invoke-static {v1}, Lcom/osp/app/signin/SignUpView;->D(Lcom/osp/app/signin/SignUpView;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 9506
    :cond_2
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v0, "SUV"

    const-string v1, "set facebook name and birthday"

    invoke-static {v0, v1}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 9507
    iget-object v0, p0, Lcom/osp/app/signin/lx;->c:Lcom/osp/app/signin/SignUpView;

    invoke-static {v0}, Lcom/osp/app/signin/SignUpView;->E(Lcom/osp/app/signin/SignUpView;)Landroid/widget/EditText;

    move-result-object v0

    if-eqz v0, :cond_3

    .line 9509
    iget-object v0, p0, Lcom/osp/app/signin/lx;->c:Lcom/osp/app/signin/SignUpView;

    invoke-static {v0}, Lcom/osp/app/signin/SignUpView;->E(Lcom/osp/app/signin/SignUpView;)Landroid/widget/EditText;

    move-result-object v0

    iget-object v1, p0, Lcom/osp/app/signin/lx;->e:Lcom/osp/app/signin/fm;

    iget-object v1, v1, Lcom/osp/app/signin/fm;->b:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    .line 9511
    :cond_3
    iget-object v0, p0, Lcom/osp/app/signin/lx;->c:Lcom/osp/app/signin/SignUpView;

    invoke-static {v0}, Lcom/osp/app/signin/SignUpView;->F(Lcom/osp/app/signin/SignUpView;)Landroid/widget/EditText;

    move-result-object v0

    if-eqz v0, :cond_4

    .line 9513
    iget-object v0, p0, Lcom/osp/app/signin/lx;->c:Lcom/osp/app/signin/SignUpView;

    invoke-static {v0}, Lcom/osp/app/signin/SignUpView;->F(Lcom/osp/app/signin/SignUpView;)Landroid/widget/EditText;

    move-result-object v0

    iget-object v1, p0, Lcom/osp/app/signin/lx;->e:Lcom/osp/app/signin/fm;

    iget-object v1, v1, Lcom/osp/app/signin/fm;->a:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    .line 9515
    :cond_4
    iget-object v0, p0, Lcom/osp/app/signin/lx;->c:Lcom/osp/app/signin/SignUpView;

    invoke-static {v0}, Lcom/osp/app/signin/SignUpView;->G(Lcom/osp/app/signin/SignUpView;)Landroid/widget/TextView;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 9517
    new-instance v0, Ljava/text/SimpleDateFormat;

    const-string v1, "yyyyMMddHHmmss"

    invoke-direct {v0, v1}, Ljava/text/SimpleDateFormat;-><init>(Ljava/lang/String;)V

    .line 9518
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v2, p0, Lcom/osp/app/signin/lx;->e:Lcom/osp/app/signin/fm;

    iget-object v2, v2, Lcom/osp/app/signin/fm;->c:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "235959"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/text/SimpleDateFormat;->parse(Ljava/lang/String;)Ljava/util/Date;

    move-result-object v0

    .line 9519
    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object v1

    .line 9520
    invoke-virtual {v1, v0}, Ljava/util/Calendar;->setTime(Ljava/util/Date;)V

    .line 9521
    iget-object v2, p0, Lcom/osp/app/signin/lx;->c:Lcom/osp/app/signin/SignUpView;

    const/4 v3, 0x1

    invoke-virtual {v1, v3}, Ljava/util/Calendar;->get(I)I

    move-result v3

    invoke-static {v2, v3}, Lcom/osp/app/signin/SignUpView;->a(Lcom/osp/app/signin/SignUpView;I)I

    .line 9522
    iget-object v2, p0, Lcom/osp/app/signin/lx;->c:Lcom/osp/app/signin/SignUpView;

    const/4 v3, 0x2

    invoke-virtual {v1, v3}, Ljava/util/Calendar;->get(I)I

    move-result v3

    invoke-static {v2, v3}, Lcom/osp/app/signin/SignUpView;->b(Lcom/osp/app/signin/SignUpView;I)I

    .line 9523
    iget-object v2, p0, Lcom/osp/app/signin/lx;->c:Lcom/osp/app/signin/SignUpView;

    const/4 v3, 0x5

    invoke-virtual {v1, v3}, Ljava/util/Calendar;->get(I)I

    move-result v1

    invoke-static {v2, v1}, Lcom/osp/app/signin/SignUpView;->c(Lcom/osp/app/signin/SignUpView;I)I

    .line 9524
    iget-object v1, p0, Lcom/osp/app/signin/lx;->c:Lcom/osp/app/signin/SignUpView;

    invoke-static {v1}, Lcom/osp/app/signin/SignUpView;->G(Lcom/osp/app/signin/SignUpView;)Landroid/widget/TextView;

    move-result-object v1

    iget-object v2, p0, Lcom/osp/app/signin/lx;->c:Lcom/osp/app/signin/SignUpView;

    invoke-static {v2, v0}, Lcom/osp/app/signin/SignUpView;->a(Lcom/osp/app/signin/SignUpView;Ljava/util/Date;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 9525
    iget-object v1, p0, Lcom/osp/app/signin/lx;->c:Lcom/osp/app/signin/SignUpView;

    invoke-static {v1}, Lcom/osp/app/signin/SignUpView;->G(Lcom/osp/app/signin/SignUpView;)Landroid/widget/TextView;

    move-result-object v1

    iget-object v2, p0, Lcom/osp/app/signin/lx;->c:Lcom/osp/app/signin/SignUpView;

    invoke-static {v2, v0}, Lcom/osp/app/signin/SignUpView;->a(Lcom/osp/app/signin/SignUpView;Ljava/util/Date;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setContentDescription(Ljava/lang/CharSequence;)V
    :try_end_0
    .catch Ljava/text/ParseException; {:try_start_0 .. :try_end_0} :catch_0

    goto/16 :goto_0

    .line 9528
    :catch_0
    move-exception v0

    invoke-virtual {v0}, Ljava/text/ParseException;->printStackTrace()V

    goto/16 :goto_0

    .line 9490
    :cond_5
    const/4 v2, 0x1

    :try_start_1
    invoke-virtual {v0, v2}, Landroid/widget/Button;->setSelected(Z)V

    .line 9491
    const/4 v0, 0x0

    invoke-virtual {v1, v0}, Landroid/widget/Button;->setSelected(Z)V

    .line 9492
    iget-object v0, p0, Lcom/osp/app/signin/lx;->c:Lcom/osp/app/signin/SignUpView;

    const/16 v1, 0x12c

    invoke-static {v0, v1}, Lcom/osp/app/signin/SignUpView;->g(Lcom/osp/app/signin/SignUpView;I)V

    .line 9493
    iget-object v0, p0, Lcom/osp/app/signin/lx;->c:Lcom/osp/app/signin/SignUpView;

    iget-object v1, p0, Lcom/osp/app/signin/lx;->e:Lcom/osp/app/signin/fm;

    iget-object v1, v1, Lcom/osp/app/signin/fm;->d:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/osp/app/signin/SignUpView;->c(Lcom/osp/app/signin/SignUpView;Ljava/lang/String;)Ljava/lang/String;

    .line 9494
    iget-object v0, p0, Lcom/osp/app/signin/lx;->c:Lcom/osp/app/signin/SignUpView;

    invoke-static {v0}, Lcom/osp/app/signin/SignUpView;->p(Lcom/osp/app/signin/SignUpView;)Landroid/widget/AutoCompleteTextView;

    move-result-object v0

    iget-object v1, p0, Lcom/osp/app/signin/lx;->c:Lcom/osp/app/signin/SignUpView;

    invoke-static {v1}, Lcom/osp/app/signin/SignUpView;->C(Lcom/osp/app/signin/SignUpView;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/AutoCompleteTextView;->setText(Ljava/lang/CharSequence;)V

    goto/16 :goto_1

    .line 9499
    :cond_6
    iget-object v0, p0, Lcom/osp/app/signin/lx;->c:Lcom/osp/app/signin/SignUpView;

    iget-object v1, p0, Lcom/osp/app/signin/lx;->e:Lcom/osp/app/signin/fm;

    iget-object v1, v1, Lcom/osp/app/signin/fm;->d:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/osp/app/signin/SignUpView;->c(Lcom/osp/app/signin/SignUpView;Ljava/lang/String;)Ljava/lang/String;

    .line 9500
    iget-object v0, p0, Lcom/osp/app/signin/lx;->c:Lcom/osp/app/signin/SignUpView;

    invoke-static {v0}, Lcom/osp/app/signin/SignUpView;->p(Lcom/osp/app/signin/SignUpView;)Landroid/widget/AutoCompleteTextView;

    move-result-object v0

    iget-object v1, p0, Lcom/osp/app/signin/lx;->c:Lcom/osp/app/signin/SignUpView;

    invoke-static {v1}, Lcom/osp/app/signin/SignUpView;->C(Lcom/osp/app/signin/SignUpView;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/AutoCompleteTextView;->setText(Ljava/lang/CharSequence;)V
    :try_end_1
    .catch Ljava/text/ParseException; {:try_start_1 .. :try_end_1} :catch_0

    goto/16 :goto_2

    .line 9539
    :cond_7
    invoke-virtual {p0, v1}, Lcom/osp/app/signin/lx;->a(Z)V

    goto/16 :goto_0

    .line 9461
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method public final b(Lcom/msc/b/c;)V
    .locals 6

    .prologue
    .line 9562
    invoke-super {p0, p1}, Lcom/msc/c/b;->b(Lcom/msc/b/c;)V

    .line 9564
    if-nez p1, :cond_1

    .line 9590
    :cond_0
    :goto_0
    return-void

    .line 9569
    :cond_1
    invoke-virtual {p1}, Lcom/msc/b/c;->a()J

    move-result-wide v0

    .line 9570
    invoke-virtual {p1}, Lcom/msc/b/c;->e()Ljava/lang/String;

    move-result-object v2

    .line 9571
    invoke-virtual {p1}, Lcom/msc/b/c;->f()Ljava/lang/Exception;

    move-result-object v3

    .line 9573
    iget-wide v4, p0, Lcom/osp/app/signin/lx;->f:J

    cmp-long v0, v0, v4

    if-nez v0, :cond_0

    .line 9575
    if-eqz v2, :cond_3

    .line 9577
    const-string v0, "AUT_1804"

    iget-object v1, p0, Lcom/osp/app/signin/lx;->b:Lcom/msc/c/f;

    invoke-virtual {v1}, Lcom/msc/c/f;->a()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 9579
    const/4 v0, 0x1

    iput v0, p0, Lcom/osp/app/signin/lx;->d:I

    goto :goto_0

    .line 9582
    :cond_2
    const/4 v0, 0x2

    iput v0, p0, Lcom/osp/app/signin/lx;->d:I

    goto :goto_0

    .line 9584
    :cond_3
    if-eqz v3, :cond_0

    .line 9586
    const/4 v0, 0x0

    iput v0, p0, Lcom/osp/app/signin/lx;->d:I

    goto :goto_0
.end method

.method protected final synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 9394
    invoke-virtual {p0}, Lcom/osp/app/signin/lx;->a()Ljava/lang/Boolean;

    move-result-object v0

    return-object v0
.end method

.method protected final synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 9394
    check-cast p1, Ljava/lang/Boolean;

    invoke-virtual {p0, p1}, Lcom/osp/app/signin/lx;->a(Ljava/lang/Boolean;)V

    return-void
.end method
