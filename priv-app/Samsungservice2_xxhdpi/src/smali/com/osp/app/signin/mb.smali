.class final Lcom/osp/app/signin/mb;
.super Lcom/msc/c/b;
.source "SignUpView.java"


# instance fields
.field final synthetic c:Lcom/osp/app/signin/SignUpView;

.field private d:J

.field private e:J

.field private f:J

.field private g:I

.field private h:J


# direct methods
.method public constructor <init>(Lcom/osp/app/signin/SignUpView;)V
    .locals 1

    .prologue
    .line 6327
    iput-object p1, p0, Lcom/osp/app/signin/mb;->c:Lcom/osp/app/signin/SignUpView;

    .line 6328
    invoke-direct {p0, p1}, Lcom/msc/c/b;-><init>(Landroid/content/Context;)V

    .line 6321
    const/4 v0, 0x0

    iput v0, p0, Lcom/osp/app/signin/mb;->g:I

    .line 6330
    return-void
.end method

.method private b(Ljava/lang/String;)V
    .locals 3

    .prologue
    .line 6616
    invoke-static {}, Lcom/msc/c/g;->a()Lcom/msc/c/g;

    .line 6618
    iget-object v0, p0, Lcom/osp/app/signin/mb;->c:Lcom/osp/app/signin/SignUpView;

    const-string v1, "password"

    invoke-static {v0, p1, v1, p0}, Lcom/msc/c/g;->e(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Lcom/msc/b/h;)J

    move-result-wide v0

    iput-wide v0, p0, Lcom/osp/app/signin/mb;->h:J

    .line 6619
    iget-wide v0, p0, Lcom/osp/app/signin/mb;->h:J

    invoke-virtual {p0, v0, v1}, Lcom/osp/app/signin/mb;->a(J)V

    .line 6620
    iget-wide v0, p0, Lcom/osp/app/signin/mb;->h:J

    const-string v2, "verify_xml"

    invoke-virtual {p0, v0, v1, v2}, Lcom/osp/app/signin/mb;->a(JLjava/lang/String;)V

    .line 6622
    iget-wide v0, p0, Lcom/osp/app/signin/mb;->h:J

    sget-object v2, Lcom/msc/b/g;->c:Lcom/msc/b/g;

    invoke-static {v0, v1, v2}, Lcom/msc/c/g;->a(JLcom/msc/b/g;)V

    .line 6623
    return-void
.end method

.method private h()V
    .locals 8

    .prologue
    .line 6590
    invoke-static {}, Lcom/msc/c/g;->a()Lcom/msc/c/g;

    .line 6591
    iget-object v0, p0, Lcom/osp/app/signin/mb;->c:Lcom/osp/app/signin/SignUpView;

    iget-object v0, p0, Lcom/osp/app/signin/mb;->c:Lcom/osp/app/signin/SignUpView;

    invoke-static {v0}, Lcom/osp/app/signin/SignUpView;->H(Lcom/osp/app/signin/SignUpView;)Lcom/osp/app/signin/pz;

    move-result-object v0

    invoke-virtual {v0}, Lcom/osp/app/signin/pz;->a()Ljava/lang/String;

    move-result-object v6

    iget-object v0, p0, Lcom/osp/app/signin/mb;->c:Lcom/osp/app/signin/SignUpView;

    invoke-static {v0}, Lcom/osp/app/signin/SignUpView;->H(Lcom/osp/app/signin/SignUpView;)Lcom/osp/app/signin/pz;

    move-result-object v0

    invoke-virtual {v0}, Lcom/osp/app/signin/pz;->b()Ljava/lang/String;

    move-result-object v7

    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v0, "SRS"

    const-string v1, "prepareWeiboUserInfo()"

    invoke-static {v0, v1}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;)V

    invoke-static {}, Lcom/msc/c/k;->e()Ljava/lang/String;

    move-result-object v1

    invoke-static {}, Lcom/osp/app/util/ah;->a()Lcom/osp/app/util/ah;

    move-result-object v0

    invoke-virtual {v0}, Lcom/osp/app/util/ah;->g()Z

    move-result v4

    invoke-static {}, Lcom/msc/b/i;->a()Lcom/msc/b/i;

    move-result-object v0

    invoke-static {}, Lcom/osp/app/signin/SamsungService;->j()Z

    move-result v2

    const/4 v3, 0x0

    move-object v5, p0

    invoke-virtual/range {v0 .. v5}, Lcom/msc/b/i;->a(Ljava/lang/String;ZZZLcom/msc/b/h;)Lcom/msc/b/d;

    move-result-object v0

    const-string v1, "Content-Type"

    const-string v2, "application/x-www-form-urlencoded;charset=UTF-8"

    invoke-virtual {v0, v1, v2}, Lcom/msc/b/d;->a(Ljava/lang/String;Ljava/lang/String;)V

    const-string v1, "source"

    const-string v2, "2262907817"

    invoke-virtual {v0, v1, v2}, Lcom/msc/b/d;->b(Ljava/lang/String;Ljava/lang/String;)V

    const-string v1, "access_token"

    invoke-virtual {v0, v1, v6}, Lcom/msc/b/d;->b(Ljava/lang/String;Ljava/lang/String;)V

    const-string v1, "uid"

    invoke-virtual {v0, v1, v7}, Lcom/msc/b/d;->b(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v0}, Lcom/msc/b/d;->a()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/osp/app/signin/mb;->e:J

    .line 6593
    iget-wide v0, p0, Lcom/osp/app/signin/mb;->e:J

    invoke-virtual {p0, v0, v1}, Lcom/osp/app/signin/mb;->a(J)V

    .line 6594
    iget-wide v0, p0, Lcom/osp/app/signin/mb;->e:J

    const-string v2, "none"

    invoke-virtual {p0, v0, v1, v2}, Lcom/osp/app/signin/mb;->a(JLjava/lang/String;)V

    .line 6596
    iget-wide v0, p0, Lcom/osp/app/signin/mb;->e:J

    sget-object v2, Lcom/msc/b/g;->b:Lcom/msc/b/g;

    invoke-static {v0, v1, v2}, Lcom/msc/c/g;->a(JLcom/msc/b/g;)V

    .line 6597
    return-void
.end method


# virtual methods
.method protected final varargs a()Ljava/lang/Boolean;
    .locals 1

    .prologue
    .line 6336
    invoke-direct {p0}, Lcom/osp/app/signin/mb;->h()V

    .line 6337
    const/4 v0, 0x0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    return-object v0
.end method

.method public final declared-synchronized a(Lcom/msc/b/c;)V
    .locals 7

    .prologue
    const/4 v6, 0x2

    .line 6465
    monitor-enter p0

    :try_start_0
    invoke-super {p0, p1}, Lcom/msc/c/b;->a(Lcom/msc/b/c;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 6467
    if-nez p1, :cond_1

    .line 6539
    :cond_0
    :goto_0
    monitor-exit p0

    return-void

    .line 6472
    :cond_1
    :try_start_1
    invoke-virtual {p1}, Lcom/msc/b/c;->a()J

    move-result-wide v0

    .line 6473
    invoke-virtual {p1}, Lcom/msc/b/c;->e()Ljava/lang/String;

    move-result-object v2

    .line 6475
    iget-wide v4, p0, Lcom/osp/app/signin/mb;->d:J
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    cmp-long v3, v0, v4

    if-nez v3, :cond_2

    .line 6479
    :try_start_2
    invoke-static {}, Lcom/msc/c/h;->a()Lcom/msc/c/h;

    iget-object v0, p0, Lcom/osp/app/signin/mb;->c:Lcom/osp/app/signin/SignUpView;

    invoke-static {v2}, Lcom/msc/c/h;->z(Ljava/lang/String;)Lcom/osp/app/signin/pz;

    move-result-object v0

    .line 6481
    iget-object v1, p0, Lcom/osp/app/signin/mb;->c:Lcom/osp/app/signin/SignUpView;

    invoke-static {v1}, Lcom/osp/app/signin/SignUpView;->H(Lcom/osp/app/signin/SignUpView;)Lcom/osp/app/signin/pz;

    move-result-object v1

    invoke-virtual {v0}, Lcom/osp/app/signin/pz;->a()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/osp/app/signin/pz;->a(Ljava/lang/String;)V

    .line 6482
    iget-object v1, p0, Lcom/osp/app/signin/mb;->c:Lcom/osp/app/signin/SignUpView;

    invoke-static {v1}, Lcom/osp/app/signin/SignUpView;->H(Lcom/osp/app/signin/SignUpView;)Lcom/osp/app/signin/pz;

    move-result-object v1

    invoke-virtual {v0}, Lcom/osp/app/signin/pz;->b()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Lcom/osp/app/signin/pz;->b(Ljava/lang/String;)V

    .line 6483
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v0, "Request Weibo User Info.."

    invoke-static {v0}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;)V

    .line 6485
    invoke-direct {p0}, Lcom/osp/app/signin/mb;->h()V
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_0
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0

    .line 6487
    :catch_0
    move-exception v0

    :try_start_3
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    goto :goto_0

    .line 6465
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 6491
    :cond_2
    :try_start_4
    iget-wide v4, p0, Lcom/osp/app/signin/mb;->e:J
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    cmp-long v3, v0, v4

    if-nez v3, :cond_4

    .line 6495
    :try_start_5
    invoke-static {}, Lcom/msc/c/h;->a()Lcom/msc/c/h;

    iget-object v0, p0, Lcom/osp/app/signin/mb;->c:Lcom/osp/app/signin/SignUpView;

    invoke-static {v2}, Lcom/msc/c/h;->z(Ljava/lang/String;)Lcom/osp/app/signin/pz;

    move-result-object v0

    .line 6497
    iget-object v1, p0, Lcom/osp/app/signin/mb;->c:Lcom/osp/app/signin/SignUpView;

    invoke-static {v1}, Lcom/osp/app/signin/SignUpView;->H(Lcom/osp/app/signin/SignUpView;)Lcom/osp/app/signin/pz;

    move-result-object v1

    invoke-virtual {v0}, Lcom/osp/app/signin/pz;->d()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/osp/app/signin/pz;->d(Ljava/lang/String;)V

    .line 6498
    iget-object v1, p0, Lcom/osp/app/signin/mb;->c:Lcom/osp/app/signin/SignUpView;

    invoke-static {v1}, Lcom/osp/app/signin/SignUpView;->H(Lcom/osp/app/signin/SignUpView;)Lcom/osp/app/signin/pz;

    move-result-object v1

    invoke-virtual {v0}, Lcom/osp/app/signin/pz;->e()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Lcom/osp/app/signin/pz;->e(Ljava/lang/String;)V

    .line 6500
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "weibo email = "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v1, p0, Lcom/osp/app/signin/mb;->c:Lcom/osp/app/signin/SignUpView;

    invoke-static {v1}, Lcom/osp/app/signin/SignUpView;->H(Lcom/osp/app/signin/SignUpView;)Lcom/osp/app/signin/pz;

    move-result-object v1

    invoke-virtual {v1}, Lcom/osp/app/signin/pz;->c()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;)V

    .line 6501
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "weibo mobile = "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v1, p0, Lcom/osp/app/signin/mb;->c:Lcom/osp/app/signin/SignUpView;

    invoke-static {v1}, Lcom/osp/app/signin/SignUpView;->H(Lcom/osp/app/signin/SignUpView;)Lcom/osp/app/signin/pz;

    move-result-object v1

    invoke-virtual {v1}, Lcom/osp/app/signin/pz;->f()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;)V

    .line 6502
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "weibo name = "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v1, p0, Lcom/osp/app/signin/mb;->c:Lcom/osp/app/signin/SignUpView;

    invoke-static {v1}, Lcom/osp/app/signin/SignUpView;->H(Lcom/osp/app/signin/SignUpView;)Lcom/osp/app/signin/pz;

    move-result-object v1

    invoke-virtual {v1}, Lcom/osp/app/signin/pz;->d()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;)V

    .line 6503
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "weibo birth = "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v1, p0, Lcom/osp/app/signin/mb;->c:Lcom/osp/app/signin/SignUpView;

    invoke-static {v1}, Lcom/osp/app/signin/SignUpView;->H(Lcom/osp/app/signin/SignUpView;)Lcom/osp/app/signin/pz;

    move-result-object v1

    invoke-virtual {v1}, Lcom/osp/app/signin/pz;->e()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;)V

    .line 6505
    invoke-static {}, Lcom/osp/device/b;->b()Lcom/osp/device/b;

    move-result-object v0

    invoke-virtual {v0}, Lcom/osp/device/b;->a()Z

    move-result v0

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/osp/app/signin/mb;->c:Lcom/osp/app/signin/SignUpView;

    invoke-static {v0}, Lcom/osp/app/signin/SignUpView;->H(Lcom/osp/app/signin/SignUpView;)Lcom/osp/app/signin/pz;

    move-result-object v0

    invoke-virtual {v0}, Lcom/osp/app/signin/pz;->g()I

    move-result v0

    if-ne v0, v6, :cond_3

    .line 6507
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v0, "SUV"

    const-string v1, "Check verify weibo of phone number"

    invoke-static {v0, v1}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 6508
    iget-object v0, p0, Lcom/osp/app/signin/mb;->c:Lcom/osp/app/signin/SignUpView;

    invoke-static {v0}, Lcom/osp/app/signin/SignUpView;->H(Lcom/osp/app/signin/SignUpView;)Lcom/osp/app/signin/pz;

    move-result-object v0

    invoke-virtual {v0}, Lcom/osp/app/signin/pz;->f()Ljava/lang/String;

    move-result-object v0

    .line 6509
    const-string v1, "+"

    const-string v2, ""

    invoke-virtual {v0, v1, v2}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/osp/app/signin/mb;->b(Ljava/lang/String;)V
    :try_end_5
    .catch Ljava/lang/Exception; {:try_start_5 .. :try_end_5} :catch_1
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    goto/16 :goto_0

    .line 6515
    :catch_1
    move-exception v0

    :try_start_6
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_0

    goto/16 :goto_0

    .line 6512
    :cond_3
    :try_start_7
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v0, "SUV"

    const-string v1, "Check verify weibo of email"

    invoke-static {v0, v1}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 6513
    iget-object v0, p0, Lcom/osp/app/signin/mb;->c:Lcom/osp/app/signin/SignUpView;

    invoke-static {v0}, Lcom/osp/app/signin/SignUpView;->H(Lcom/osp/app/signin/SignUpView;)Lcom/osp/app/signin/pz;

    move-result-object v0

    invoke-virtual {v0}, Lcom/osp/app/signin/pz;->c()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/osp/app/signin/mb;->b(Ljava/lang/String;)V
    :try_end_7
    .catch Ljava/lang/Exception; {:try_start_7 .. :try_end_7} :catch_1
    .catchall {:try_start_7 .. :try_end_7} :catchall_0

    goto/16 :goto_0

    .line 6519
    :cond_4
    :try_start_8
    iget-wide v4, p0, Lcom/osp/app/signin/mb;->f:J
    :try_end_8
    .catchall {:try_start_8 .. :try_end_8} :catchall_0

    cmp-long v3, v0, v4

    if-nez v3, :cond_6

    .line 6523
    :try_start_9
    invoke-static {}, Lcom/msc/c/h;->a()Lcom/msc/c/h;

    iget-object v0, p0, Lcom/osp/app/signin/mb;->c:Lcom/osp/app/signin/SignUpView;

    new-instance v1, Lorg/json/JSONObject;

    invoke-direct {v1, v2}, Lorg/json/JSONObject;-><init>(Ljava/lang/String;)V

    const-string v0, ""

    const-string v2, "email"

    invoke-virtual {v1, v2}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_5

    const-string v0, "email"

    invoke-virtual {v1, v0}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 6524
    :cond_5
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "weibo email = "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;)V

    .line 6527
    invoke-direct {p0}, Lcom/osp/app/signin/mb;->h()V
    :try_end_9
    .catch Ljava/lang/Exception; {:try_start_9 .. :try_end_9} :catch_2
    .catchall {:try_start_9 .. :try_end_9} :catchall_0

    goto/16 :goto_0

    .line 6529
    :catch_2
    move-exception v0

    :try_start_a
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    goto/16 :goto_0

    .line 6534
    :cond_6
    iget-wide v2, p0, Lcom/osp/app/signin/mb;->h:J

    cmp-long v0, v0, v2

    if-nez v0, :cond_0

    .line 6536
    const/4 v0, 0x2

    iput v0, p0, Lcom/osp/app/signin/mb;->g:I
    :try_end_a
    .catchall {:try_start_a .. :try_end_a} :catchall_0

    goto/16 :goto_0
.end method

.method protected final a(Ljava/lang/Boolean;)V
    .locals 6

    .prologue
    const/4 v2, 0x2

    const/4 v1, 0x0

    .line 6342
    invoke-super {p0, p1}, Lcom/msc/c/b;->a(Ljava/lang/Boolean;)V

    .line 6345
    iget-object v0, p0, Lcom/osp/app/signin/mb;->c:Lcom/osp/app/signin/SignUpView;

    invoke-static {v0}, Lcom/osp/app/signin/SignUpView;->H(Lcom/osp/app/signin/SignUpView;)Lcom/osp/app/signin/pz;

    move-result-object v0

    if-eqz v0, :cond_7

    iget-object v0, p0, Lcom/osp/app/signin/mb;->c:Lcom/osp/app/signin/SignUpView;

    invoke-static {v0}, Lcom/osp/app/signin/SignUpView;->H(Lcom/osp/app/signin/SignUpView;)Lcom/osp/app/signin/pz;

    move-result-object v0

    invoke-virtual {v0}, Lcom/osp/app/signin/pz;->c()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_7

    .line 6347
    iget v0, p0, Lcom/osp/app/signin/mb;->g:I

    packed-switch v0, :pswitch_data_0

    .line 6461
    :cond_0
    :goto_0
    return-void

    .line 6350
    :pswitch_0
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v0, "SUV"

    const-string v1, "Weibo email already exist in Samsung account"

    invoke-static {v0, v1}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 6351
    invoke-static {}, Lcom/osp/device/b;->b()Lcom/osp/device/b;

    move-result-object v0

    invoke-virtual {v0}, Lcom/osp/device/b;->a()Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/osp/app/signin/mb;->c:Lcom/osp/app/signin/SignUpView;

    invoke-static {v0}, Lcom/osp/app/signin/SignUpView;->H(Lcom/osp/app/signin/SignUpView;)Lcom/osp/app/signin/pz;

    move-result-object v0

    invoke-virtual {v0}, Lcom/osp/app/signin/pz;->g()I

    move-result v0

    if-ne v0, v2, :cond_1

    .line 6353
    iget-object v0, p0, Lcom/osp/app/signin/mb;->c:Lcom/osp/app/signin/SignUpView;

    iget-object v1, p0, Lcom/osp/app/signin/mb;->c:Lcom/osp/app/signin/SignUpView;

    invoke-static {v1}, Lcom/osp/app/signin/SignUpView;->H(Lcom/osp/app/signin/SignUpView;)Lcom/osp/app/signin/pz;

    move-result-object v1

    invoke-virtual {v1}, Lcom/osp/app/signin/pz;->f()Ljava/lang/String;

    move-result-object v1

    const-string v2, "+"

    const-string v3, ""

    invoke-virtual {v1, v2, v3}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/osp/app/signin/SignUpView;->c(Ljava/lang/String;)V

    goto :goto_0

    .line 6356
    :cond_1
    iget-object v0, p0, Lcom/osp/app/signin/mb;->c:Lcom/osp/app/signin/SignUpView;

    iget-object v1, p0, Lcom/osp/app/signin/mb;->c:Lcom/osp/app/signin/SignUpView;

    invoke-static {v1}, Lcom/osp/app/signin/SignUpView;->H(Lcom/osp/app/signin/SignUpView;)Lcom/osp/app/signin/pz;

    move-result-object v1

    invoke-virtual {v1}, Lcom/osp/app/signin/pz;->c()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/osp/app/signin/SignUpView;->c(Ljava/lang/String;)V

    goto :goto_0

    .line 6362
    :pswitch_1
    :try_start_0
    iget-object v0, p0, Lcom/osp/app/signin/mb;->c:Lcom/osp/app/signin/SignUpView;

    invoke-static {v0}, Lcom/osp/app/signin/SignUpView;->p(Lcom/osp/app/signin/SignUpView;)Landroid/widget/AutoCompleteTextView;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/AutoCompleteTextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v0, p0, Lcom/osp/app/signin/mb;->c:Lcom/osp/app/signin/SignUpView;

    invoke-static {v0}, Lcom/osp/app/signin/SignUpView;->E(Lcom/osp/app/signin/SignUpView;)Landroid/widget/EditText;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    iget-object v0, p0, Lcom/osp/app/signin/mb;->c:Lcom/osp/app/signin/SignUpView;

    invoke-static {v0}, Lcom/osp/app/signin/SignUpView;->F(Lcom/osp/app/signin/SignUpView;)Landroid/widget/EditText;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    iget-object v0, p0, Lcom/osp/app/signin/mb;->c:Lcom/osp/app/signin/SignUpView;

    invoke-static {v0}, Lcom/osp/app/signin/SignUpView;->G(Lcom/osp/app/signin/SignUpView;)Landroid/widget/TextView;

    move-result-object v0

    iget-object v1, p0, Lcom/osp/app/signin/mb;->c:Lcom/osp/app/signin/SignUpView;

    new-instance v2, Ljava/util/Date;

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v4

    invoke-direct {v2, v4, v5}, Ljava/util/Date;-><init>(J)V

    invoke-static {v1, v2}, Lcom/osp/app/signin/SignUpView;->a(Lcom/osp/app/signin/SignUpView;Ljava/util/Date;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v0, p0, Lcom/osp/app/signin/mb;->c:Lcom/osp/app/signin/SignUpView;

    invoke-static {v0}, Lcom/osp/app/signin/SignUpView;->G(Lcom/osp/app/signin/SignUpView;)Landroid/widget/TextView;

    move-result-object v0

    iget-object v1, p0, Lcom/osp/app/signin/mb;->c:Lcom/osp/app/signin/SignUpView;

    new-instance v2, Ljava/util/Date;

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v4

    invoke-direct {v2, v4, v5}, Ljava/util/Date;-><init>(J)V

    invoke-static {v1, v2}, Lcom/osp/app/signin/SignUpView;->a(Lcom/osp/app/signin/SignUpView;Ljava/util/Date;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 6364
    invoke-static {}, Lcom/osp/device/b;->b()Lcom/osp/device/b;

    move-result-object v0

    invoke-virtual {v0}, Lcom/osp/device/b;->a()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 6366
    iget-object v0, p0, Lcom/osp/app/signin/mb;->c:Lcom/osp/app/signin/SignUpView;

    const v1, 0x7f0c0086

    invoke-virtual {v0, v1}, Lcom/osp/app/signin/SignUpView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    .line 6367
    iget-object v1, p0, Lcom/osp/app/signin/mb;->c:Lcom/osp/app/signin/SignUpView;

    const v2, 0x7f0c0088

    invoke-virtual {v1, v2}, Lcom/osp/app/signin/SignUpView;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/Button;

    .line 6368
    const/4 v2, 0x1

    invoke-virtual {v0, v2}, Landroid/widget/Button;->setSelected(Z)V

    .line 6369
    const/4 v0, 0x0

    invoke-virtual {v1, v0}, Landroid/widget/Button;->setSelected(Z)V

    .line 6370
    iget-object v0, p0, Lcom/osp/app/signin/mb;->c:Lcom/osp/app/signin/SignUpView;

    invoke-static {v0}, Lcom/osp/app/signin/SignUpView;->x(Lcom/osp/app/signin/SignUpView;)V

    .line 6371
    iget-object v0, p0, Lcom/osp/app/signin/mb;->c:Lcom/osp/app/signin/SignUpView;

    const/16 v1, 0x12c

    invoke-static {v0, v1}, Lcom/osp/app/signin/SignUpView;->g(Lcom/osp/app/signin/SignUpView;I)V

    .line 6374
    :cond_2
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v0, "SUV"

    const-string v1, "set weibo email address"

    invoke-static {v0, v1}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 6375
    iget-object v0, p0, Lcom/osp/app/signin/mb;->c:Lcom/osp/app/signin/SignUpView;

    iget-object v1, p0, Lcom/osp/app/signin/mb;->c:Lcom/osp/app/signin/SignUpView;

    invoke-static {v1}, Lcom/osp/app/signin/SignUpView;->H(Lcom/osp/app/signin/SignUpView;)Lcom/osp/app/signin/pz;

    move-result-object v1

    invoke-virtual {v1}, Lcom/osp/app/signin/pz;->c()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/osp/app/signin/SignUpView;->d(Lcom/osp/app/signin/SignUpView;Ljava/lang/String;)Ljava/lang/String;

    .line 6376
    iget-object v0, p0, Lcom/osp/app/signin/mb;->c:Lcom/osp/app/signin/SignUpView;

    invoke-static {v0}, Lcom/osp/app/signin/SignUpView;->p(Lcom/osp/app/signin/SignUpView;)Landroid/widget/AutoCompleteTextView;

    move-result-object v0

    iget-object v1, p0, Lcom/osp/app/signin/mb;->c:Lcom/osp/app/signin/SignUpView;

    invoke-static {v1}, Lcom/osp/app/signin/SignUpView;->I(Lcom/osp/app/signin/SignUpView;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/AutoCompleteTextView;->setText(Ljava/lang/CharSequence;)V

    .line 6377
    iget-object v0, p0, Lcom/osp/app/signin/mb;->c:Lcom/osp/app/signin/SignUpView;

    invoke-static {v0}, Lcom/osp/app/signin/SignUpView;->p(Lcom/osp/app/signin/SignUpView;)Landroid/widget/AutoCompleteTextView;

    move-result-object v0

    iget-object v1, p0, Lcom/osp/app/signin/mb;->c:Lcom/osp/app/signin/SignUpView;

    invoke-static {v1}, Lcom/osp/app/signin/SignUpView;->p(Lcom/osp/app/signin/SignUpView;)Landroid/widget/AutoCompleteTextView;

    move-result-object v1

    invoke-virtual {v1}, Landroid/widget/AutoCompleteTextView;->length()I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/AutoCompleteTextView;->setSelection(I)V

    .line 6379
    iget-object v0, p0, Lcom/osp/app/signin/mb;->c:Lcom/osp/app/signin/SignUpView;

    invoke-static {v0}, Lcom/osp/app/signin/SignUpView;->d(Lcom/osp/app/signin/SignUpView;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 6381
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v0, "SUV"

    const-string v1, "set weibo name and birthday"

    invoke-static {v0, v1}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 6382
    iget-object v0, p0, Lcom/osp/app/signin/mb;->c:Lcom/osp/app/signin/SignUpView;

    invoke-static {v0}, Lcom/osp/app/signin/SignUpView;->E(Lcom/osp/app/signin/SignUpView;)Landroid/widget/EditText;

    move-result-object v0

    if-eqz v0, :cond_3

    .line 6384
    iget-object v0, p0, Lcom/osp/app/signin/mb;->c:Lcom/osp/app/signin/SignUpView;

    invoke-static {v0}, Lcom/osp/app/signin/SignUpView;->E(Lcom/osp/app/signin/SignUpView;)Landroid/widget/EditText;

    move-result-object v0

    iget-object v1, p0, Lcom/osp/app/signin/mb;->c:Lcom/osp/app/signin/SignUpView;

    invoke-static {v1}, Lcom/osp/app/signin/SignUpView;->H(Lcom/osp/app/signin/SignUpView;)Lcom/osp/app/signin/pz;

    move-result-object v1

    invoke-virtual {v1}, Lcom/osp/app/signin/pz;->d()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    .line 6386
    :cond_3
    iget-object v0, p0, Lcom/osp/app/signin/mb;->c:Lcom/osp/app/signin/SignUpView;

    invoke-static {v0}, Lcom/osp/app/signin/SignUpView;->F(Lcom/osp/app/signin/SignUpView;)Landroid/widget/EditText;

    move-result-object v0

    if-eqz v0, :cond_4

    .line 6388
    iget-object v0, p0, Lcom/osp/app/signin/mb;->c:Lcom/osp/app/signin/SignUpView;

    invoke-static {v0}, Lcom/osp/app/signin/SignUpView;->F(Lcom/osp/app/signin/SignUpView;)Landroid/widget/EditText;

    move-result-object v0

    iget-object v1, p0, Lcom/osp/app/signin/mb;->c:Lcom/osp/app/signin/SignUpView;

    invoke-static {v1}, Lcom/osp/app/signin/SignUpView;->H(Lcom/osp/app/signin/SignUpView;)Lcom/osp/app/signin/pz;

    move-result-object v1

    invoke-virtual {v1}, Lcom/osp/app/signin/pz;->d()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    .line 6390
    :cond_4
    iget-object v0, p0, Lcom/osp/app/signin/mb;->c:Lcom/osp/app/signin/SignUpView;

    invoke-static {v0}, Lcom/osp/app/signin/SignUpView;->G(Lcom/osp/app/signin/SignUpView;)Landroid/widget/TextView;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 6392
    new-instance v0, Ljava/text/SimpleDateFormat;

    const-string v1, "yyyy-MM-dd"

    invoke-direct {v0, v1}, Ljava/text/SimpleDateFormat;-><init>(Ljava/lang/String;)V

    .line 6393
    iget-object v1, p0, Lcom/osp/app/signin/mb;->c:Lcom/osp/app/signin/SignUpView;

    invoke-static {v1}, Lcom/osp/app/signin/SignUpView;->H(Lcom/osp/app/signin/SignUpView;)Lcom/osp/app/signin/pz;

    move-result-object v1

    invoke-virtual {v1}, Lcom/osp/app/signin/pz;->e()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/text/SimpleDateFormat;->parse(Ljava/lang/String;)Ljava/util/Date;

    move-result-object v0

    .line 6394
    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object v1

    .line 6395
    invoke-virtual {v1, v0}, Ljava/util/Calendar;->setTime(Ljava/util/Date;)V

    .line 6396
    iget-object v2, p0, Lcom/osp/app/signin/mb;->c:Lcom/osp/app/signin/SignUpView;

    const/4 v3, 0x1

    invoke-virtual {v1, v3}, Ljava/util/Calendar;->get(I)I

    move-result v3

    invoke-static {v2, v3}, Lcom/osp/app/signin/SignUpView;->a(Lcom/osp/app/signin/SignUpView;I)I

    .line 6397
    iget-object v2, p0, Lcom/osp/app/signin/mb;->c:Lcom/osp/app/signin/SignUpView;

    const/4 v3, 0x2

    invoke-virtual {v1, v3}, Ljava/util/Calendar;->get(I)I

    move-result v3

    invoke-static {v2, v3}, Lcom/osp/app/signin/SignUpView;->b(Lcom/osp/app/signin/SignUpView;I)I

    .line 6398
    iget-object v2, p0, Lcom/osp/app/signin/mb;->c:Lcom/osp/app/signin/SignUpView;

    const/4 v3, 0x5

    invoke-virtual {v1, v3}, Ljava/util/Calendar;->get(I)I

    move-result v1

    invoke-static {v2, v1}, Lcom/osp/app/signin/SignUpView;->c(Lcom/osp/app/signin/SignUpView;I)I

    .line 6399
    iget-object v1, p0, Lcom/osp/app/signin/mb;->c:Lcom/osp/app/signin/SignUpView;

    invoke-static {v1}, Lcom/osp/app/signin/SignUpView;->G(Lcom/osp/app/signin/SignUpView;)Landroid/widget/TextView;

    move-result-object v1

    iget-object v2, p0, Lcom/osp/app/signin/mb;->c:Lcom/osp/app/signin/SignUpView;

    invoke-static {v2, v0}, Lcom/osp/app/signin/SignUpView;->a(Lcom/osp/app/signin/SignUpView;Ljava/util/Date;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 6400
    iget-object v1, p0, Lcom/osp/app/signin/mb;->c:Lcom/osp/app/signin/SignUpView;

    invoke-static {v1}, Lcom/osp/app/signin/SignUpView;->G(Lcom/osp/app/signin/SignUpView;)Landroid/widget/TextView;

    move-result-object v1

    iget-object v2, p0, Lcom/osp/app/signin/mb;->c:Lcom/osp/app/signin/SignUpView;

    invoke-static {v2, v0}, Lcom/osp/app/signin/SignUpView;->a(Lcom/osp/app/signin/SignUpView;Ljava/util/Date;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setContentDescription(Ljava/lang/CharSequence;)V
    :try_end_0
    .catch Ljava/text/ParseException; {:try_start_0 .. :try_end_0} :catch_0

    goto/16 :goto_0

    .line 6403
    :catch_0
    move-exception v0

    invoke-virtual {v0}, Ljava/text/ParseException;->printStackTrace()V

    goto/16 :goto_0

    .line 6411
    :pswitch_2
    :try_start_1
    iget-object v0, p0, Lcom/osp/app/signin/mb;->c:Lcom/osp/app/signin/SignUpView;

    const v1, 0x7f0c0086

    invoke-virtual {v0, v1}, Lcom/osp/app/signin/SignUpView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    .line 6412
    iget-object v1, p0, Lcom/osp/app/signin/mb;->c:Lcom/osp/app/signin/SignUpView;

    const v2, 0x7f0c0088

    invoke-virtual {v1, v2}, Lcom/osp/app/signin/SignUpView;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/Button;

    .line 6413
    const/4 v2, 0x0

    invoke-virtual {v0, v2}, Landroid/widget/Button;->setSelected(Z)V

    .line 6414
    const/4 v0, 0x1

    invoke-virtual {v1, v0}, Landroid/widget/Button;->setSelected(Z)V

    .line 6415
    iget-object v0, p0, Lcom/osp/app/signin/mb;->c:Lcom/osp/app/signin/SignUpView;

    invoke-static {v0}, Lcom/osp/app/signin/SignUpView;->x(Lcom/osp/app/signin/SignUpView;)V

    .line 6416
    iget-object v0, p0, Lcom/osp/app/signin/mb;->c:Lcom/osp/app/signin/SignUpView;

    const/16 v1, 0x12d

    invoke-static {v0, v1}, Lcom/osp/app/signin/SignUpView;->g(Lcom/osp/app/signin/SignUpView;I)V

    .line 6418
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v0, "SUV"

    const-string v1, "set weibo phone number id"

    invoke-static {v0, v1}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 6419
    iget-object v0, p0, Lcom/osp/app/signin/mb;->c:Lcom/osp/app/signin/SignUpView;

    iget-object v1, p0, Lcom/osp/app/signin/mb;->c:Lcom/osp/app/signin/SignUpView;

    invoke-static {v1}, Lcom/osp/app/signin/SignUpView;->H(Lcom/osp/app/signin/SignUpView;)Lcom/osp/app/signin/pz;

    move-result-object v1

    invoke-virtual {v1}, Lcom/osp/app/signin/pz;->f()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/osp/app/signin/SignUpView;->e(Lcom/osp/app/signin/SignUpView;Ljava/lang/String;)Ljava/lang/String;

    .line 6420
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "Weibo Phone number ID : "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v1, p0, Lcom/osp/app/signin/mb;->c:Lcom/osp/app/signin/SignUpView;

    invoke-static {v1}, Lcom/osp/app/signin/SignUpView;->J(Lcom/osp/app/signin/SignUpView;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;)V

    .line 6421
    iget-object v0, p0, Lcom/osp/app/signin/mb;->c:Lcom/osp/app/signin/SignUpView;

    invoke-static {v0}, Lcom/osp/app/signin/SignUpView;->p(Lcom/osp/app/signin/SignUpView;)Landroid/widget/AutoCompleteTextView;

    move-result-object v0

    iget-object v1, p0, Lcom/osp/app/signin/mb;->c:Lcom/osp/app/signin/SignUpView;

    invoke-static {v1}, Lcom/osp/app/signin/SignUpView;->J(Lcom/osp/app/signin/SignUpView;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/AutoCompleteTextView;->setText(Ljava/lang/CharSequence;)V

    .line 6422
    iget-object v0, p0, Lcom/osp/app/signin/mb;->c:Lcom/osp/app/signin/SignUpView;

    invoke-static {v0}, Lcom/osp/app/signin/SignUpView;->p(Lcom/osp/app/signin/SignUpView;)Landroid/widget/AutoCompleteTextView;

    move-result-object v0

    iget-object v1, p0, Lcom/osp/app/signin/mb;->c:Lcom/osp/app/signin/SignUpView;

    invoke-static {v1}, Lcom/osp/app/signin/SignUpView;->p(Lcom/osp/app/signin/SignUpView;)Landroid/widget/AutoCompleteTextView;

    move-result-object v1

    invoke-virtual {v1}, Landroid/widget/AutoCompleteTextView;->length()I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/AutoCompleteTextView;->setSelection(I)V

    .line 6424
    iget-object v0, p0, Lcom/osp/app/signin/mb;->c:Lcom/osp/app/signin/SignUpView;

    invoke-static {v0}, Lcom/osp/app/signin/SignUpView;->d(Lcom/osp/app/signin/SignUpView;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 6426
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v0, "SUV"

    const-string v1, "set weibo name and birthday"

    invoke-static {v0, v1}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 6427
    iget-object v0, p0, Lcom/osp/app/signin/mb;->c:Lcom/osp/app/signin/SignUpView;

    invoke-static {v0}, Lcom/osp/app/signin/SignUpView;->E(Lcom/osp/app/signin/SignUpView;)Landroid/widget/EditText;

    move-result-object v0

    if-eqz v0, :cond_5

    .line 6429
    iget-object v0, p0, Lcom/osp/app/signin/mb;->c:Lcom/osp/app/signin/SignUpView;

    invoke-static {v0}, Lcom/osp/app/signin/SignUpView;->E(Lcom/osp/app/signin/SignUpView;)Landroid/widget/EditText;

    move-result-object v0

    iget-object v1, p0, Lcom/osp/app/signin/mb;->c:Lcom/osp/app/signin/SignUpView;

    invoke-static {v1}, Lcom/osp/app/signin/SignUpView;->H(Lcom/osp/app/signin/SignUpView;)Lcom/osp/app/signin/pz;

    move-result-object v1

    invoke-virtual {v1}, Lcom/osp/app/signin/pz;->d()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    .line 6431
    :cond_5
    iget-object v0, p0, Lcom/osp/app/signin/mb;->c:Lcom/osp/app/signin/SignUpView;

    invoke-static {v0}, Lcom/osp/app/signin/SignUpView;->F(Lcom/osp/app/signin/SignUpView;)Landroid/widget/EditText;

    move-result-object v0

    if-eqz v0, :cond_6

    .line 6433
    iget-object v0, p0, Lcom/osp/app/signin/mb;->c:Lcom/osp/app/signin/SignUpView;

    invoke-static {v0}, Lcom/osp/app/signin/SignUpView;->F(Lcom/osp/app/signin/SignUpView;)Landroid/widget/EditText;

    move-result-object v0

    iget-object v1, p0, Lcom/osp/app/signin/mb;->c:Lcom/osp/app/signin/SignUpView;

    invoke-static {v1}, Lcom/osp/app/signin/SignUpView;->H(Lcom/osp/app/signin/SignUpView;)Lcom/osp/app/signin/pz;

    move-result-object v1

    invoke-virtual {v1}, Lcom/osp/app/signin/pz;->d()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    .line 6435
    :cond_6
    iget-object v0, p0, Lcom/osp/app/signin/mb;->c:Lcom/osp/app/signin/SignUpView;

    invoke-static {v0}, Lcom/osp/app/signin/SignUpView;->G(Lcom/osp/app/signin/SignUpView;)Landroid/widget/TextView;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 6437
    new-instance v0, Ljava/text/SimpleDateFormat;

    const-string v1, "yyyy-MM-dd"

    invoke-direct {v0, v1}, Ljava/text/SimpleDateFormat;-><init>(Ljava/lang/String;)V

    .line 6438
    iget-object v1, p0, Lcom/osp/app/signin/mb;->c:Lcom/osp/app/signin/SignUpView;

    invoke-static {v1}, Lcom/osp/app/signin/SignUpView;->H(Lcom/osp/app/signin/SignUpView;)Lcom/osp/app/signin/pz;

    move-result-object v1

    invoke-virtual {v1}, Lcom/osp/app/signin/pz;->e()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/text/SimpleDateFormat;->parse(Ljava/lang/String;)Ljava/util/Date;

    move-result-object v0

    .line 6439
    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object v1

    .line 6440
    invoke-virtual {v1, v0}, Ljava/util/Calendar;->setTime(Ljava/util/Date;)V

    .line 6441
    iget-object v2, p0, Lcom/osp/app/signin/mb;->c:Lcom/osp/app/signin/SignUpView;

    const/4 v3, 0x1

    invoke-virtual {v1, v3}, Ljava/util/Calendar;->get(I)I

    move-result v3

    invoke-static {v2, v3}, Lcom/osp/app/signin/SignUpView;->a(Lcom/osp/app/signin/SignUpView;I)I

    .line 6442
    iget-object v2, p0, Lcom/osp/app/signin/mb;->c:Lcom/osp/app/signin/SignUpView;

    const/4 v3, 0x2

    invoke-virtual {v1, v3}, Ljava/util/Calendar;->get(I)I

    move-result v3

    invoke-static {v2, v3}, Lcom/osp/app/signin/SignUpView;->b(Lcom/osp/app/signin/SignUpView;I)I

    .line 6443
    iget-object v2, p0, Lcom/osp/app/signin/mb;->c:Lcom/osp/app/signin/SignUpView;

    const/4 v3, 0x5

    invoke-virtual {v1, v3}, Ljava/util/Calendar;->get(I)I

    move-result v1

    invoke-static {v2, v1}, Lcom/osp/app/signin/SignUpView;->c(Lcom/osp/app/signin/SignUpView;I)I

    .line 6444
    iget-object v1, p0, Lcom/osp/app/signin/mb;->c:Lcom/osp/app/signin/SignUpView;

    invoke-static {v1}, Lcom/osp/app/signin/SignUpView;->G(Lcom/osp/app/signin/SignUpView;)Landroid/widget/TextView;

    move-result-object v1

    iget-object v2, p0, Lcom/osp/app/signin/mb;->c:Lcom/osp/app/signin/SignUpView;

    invoke-static {v2, v0}, Lcom/osp/app/signin/SignUpView;->a(Lcom/osp/app/signin/SignUpView;Ljava/util/Date;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 6445
    iget-object v1, p0, Lcom/osp/app/signin/mb;->c:Lcom/osp/app/signin/SignUpView;

    invoke-static {v1}, Lcom/osp/app/signin/SignUpView;->G(Lcom/osp/app/signin/SignUpView;)Landroid/widget/TextView;

    move-result-object v1

    iget-object v2, p0, Lcom/osp/app/signin/mb;->c:Lcom/osp/app/signin/SignUpView;

    invoke-static {v2, v0}, Lcom/osp/app/signin/SignUpView;->a(Lcom/osp/app/signin/SignUpView;Ljava/util/Date;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setContentDescription(Ljava/lang/CharSequence;)V
    :try_end_1
    .catch Ljava/text/ParseException; {:try_start_1 .. :try_end_1} :catch_1

    goto/16 :goto_0

    .line 6448
    :catch_1
    move-exception v0

    invoke-virtual {v0}, Ljava/text/ParseException;->printStackTrace()V

    goto/16 :goto_0

    .line 6459
    :cond_7
    invoke-virtual {p0, v1}, Lcom/osp/app/signin/mb;->a(Z)V

    goto/16 :goto_0

    .line 6347
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_0
        :pswitch_2
    .end packed-switch
.end method

.method public final b(Lcom/msc/b/c;)V
    .locals 8

    .prologue
    const/4 v7, 0x2

    const/4 v6, 0x1

    .line 6543
    invoke-super {p0, p1}, Lcom/msc/c/b;->b(Lcom/msc/b/c;)V

    .line 6545
    if-nez p1, :cond_1

    .line 6586
    :cond_0
    :goto_0
    return-void

    .line 6550
    :cond_1
    invoke-virtual {p1}, Lcom/msc/b/c;->a()J

    move-result-wide v0

    .line 6551
    invoke-virtual {p1}, Lcom/msc/b/c;->e()Ljava/lang/String;

    move-result-object v2

    .line 6553
    iget-wide v4, p0, Lcom/osp/app/signin/mb;->d:J

    cmp-long v3, v0, v4

    if-nez v3, :cond_2

    .line 6555
    invoke-static {}, Lcom/osp/app/util/ap;->a()Lcom/osp/app/util/ap;

    iget-object v0, p0, Lcom/osp/app/signin/mb;->c:Lcom/osp/app/signin/SignUpView;

    const-string v1, "Failed to get Weibo access token...Please try again later"

    invoke-static {v0, v1, v6}, Lcom/osp/app/util/ap;->a(Landroid/content/Context;Ljava/lang/String;I)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    goto :goto_0

    .line 6556
    :cond_2
    iget-wide v4, p0, Lcom/osp/app/signin/mb;->e:J

    cmp-long v3, v0, v4

    if-nez v3, :cond_3

    .line 6558
    invoke-static {}, Lcom/osp/app/util/ap;->a()Lcom/osp/app/util/ap;

    iget-object v0, p0, Lcom/osp/app/signin/mb;->c:Lcom/osp/app/signin/SignUpView;

    const-string v1, "Failed to get Weibo user info...Please try again later"

    invoke-static {v0, v1, v6}, Lcom/osp/app/util/ap;->a(Landroid/content/Context;Ljava/lang/String;I)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    goto :goto_0

    .line 6560
    :cond_3
    iget-wide v4, p0, Lcom/osp/app/signin/mb;->f:J

    cmp-long v3, v0, v4

    if-nez v3, :cond_4

    .line 6562
    invoke-static {}, Lcom/osp/app/util/ap;->a()Lcom/osp/app/util/ap;

    iget-object v0, p0, Lcom/osp/app/signin/mb;->c:Lcom/osp/app/signin/SignUpView;

    const-string v1, "Failed to get Weibo user email info...Please try again later"

    invoke-static {v0, v1, v6}, Lcom/osp/app/util/ap;->a(Landroid/content/Context;Ljava/lang/String;I)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    goto :goto_0

    .line 6563
    :cond_4
    iget-wide v4, p0, Lcom/osp/app/signin/mb;->h:J

    cmp-long v0, v0, v4

    if-nez v0, :cond_8

    .line 6565
    if-eqz v2, :cond_0

    .line 6567
    const-string v0, "AUT_1804"

    iget-object v1, p0, Lcom/osp/app/signin/mb;->b:Lcom/msc/c/f;

    invoke-virtual {v1}, Lcom/msc/c/f;->a()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_5

    const-string v0, "AUT_1003"

    iget-object v1, p0, Lcom/osp/app/signin/mb;->b:Lcom/msc/c/f;

    invoke-virtual {v1}, Lcom/msc/c/f;->a()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_7

    .line 6569
    :cond_5
    invoke-static {}, Lcom/osp/device/b;->b()Lcom/osp/device/b;

    move-result-object v0

    invoke-virtual {v0}, Lcom/osp/device/b;->a()Z

    move-result v0

    if-eqz v0, :cond_6

    iget-object v0, p0, Lcom/osp/app/signin/mb;->c:Lcom/osp/app/signin/SignUpView;

    invoke-static {v0}, Lcom/osp/app/signin/SignUpView;->H(Lcom/osp/app/signin/SignUpView;)Lcom/osp/app/signin/pz;

    move-result-object v0

    invoke-virtual {v0}, Lcom/osp/app/signin/pz;->g()I

    move-result v0

    if-ne v0, v7, :cond_6

    .line 6571
    const/4 v0, 0x3

    iput v0, p0, Lcom/osp/app/signin/mb;->g:I

    goto/16 :goto_0

    .line 6574
    :cond_6
    iput v6, p0, Lcom/osp/app/signin/mb;->g:I

    goto/16 :goto_0

    .line 6579
    :cond_7
    iput v7, p0, Lcom/osp/app/signin/mb;->g:I

    goto/16 :goto_0

    .line 6584
    :cond_8
    invoke-static {}, Lcom/osp/app/util/ap;->a()Lcom/osp/app/util/ap;

    iget-object v0, p0, Lcom/osp/app/signin/mb;->c:Lcom/osp/app/signin/SignUpView;

    const-string v1, "Unkown Failure...Please try again later"

    invoke-static {v0, v1, v6}, Lcom/osp/app/util/ap;->a(Landroid/content/Context;Ljava/lang/String;I)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    goto/16 :goto_0
.end method

.method protected final synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 6304
    invoke-virtual {p0}, Lcom/osp/app/signin/mb;->a()Ljava/lang/Boolean;

    move-result-object v0

    return-object v0
.end method

.method protected final synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 6304
    check-cast p1, Ljava/lang/Boolean;

    invoke-virtual {p0, p1}, Lcom/osp/app/signin/mb;->a(Ljava/lang/Boolean;)V

    return-void
.end method
