.class final Lcom/osp/app/signin/pi;
.super Ljava/lang/Object;
.source "UserValidateCheck.java"

# interfaces
.implements Landroid/view/View$OnKeyListener;


# instance fields
.field final synthetic a:Lcom/osp/app/signin/UserValidateCheck;


# direct methods
.method constructor <init>(Lcom/osp/app/signin/UserValidateCheck;)V
    .locals 0

    .prologue
    .line 1130
    iput-object p1, p0, Lcom/osp/app/signin/pi;->a:Lcom/osp/app/signin/UserValidateCheck;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onKey(Landroid/view/View;ILandroid/view/KeyEvent;)Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 1134
    packed-switch p2, :pswitch_data_0

    .line 1161
    :goto_0
    return v0

    .line 1137
    :pswitch_0
    iget-object v1, p0, Lcom/osp/app/signin/pi;->a:Lcom/osp/app/signin/UserValidateCheck;

    invoke-static {v1}, Lcom/osp/app/signin/UserValidateCheck;->e(Lcom/osp/app/signin/UserValidateCheck;)Landroid/widget/EditText;

    move-result-object v1

    invoke-virtual {v1}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v1

    invoke-interface {v1}, Landroid/text/Editable;->length()I

    move-result v1

    if-nez v1, :cond_1

    .line 1139
    invoke-static {}, Lcom/osp/app/util/ap;->a()Lcom/osp/app/util/ap;

    iget-object v1, p0, Lcom/osp/app/signin/pi;->a:Lcom/osp/app/signin/UserValidateCheck;

    const v2, 0x7f090177

    invoke-static {v1, v2, v0}, Lcom/osp/app/util/ap;->a(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    .line 1157
    :cond_0
    :goto_1
    const/4 v0, 0x1

    goto :goto_0

    .line 1140
    :cond_1
    iget-object v0, p0, Lcom/osp/app/signin/pi;->a:Lcom/osp/app/signin/UserValidateCheck;

    invoke-static {v0}, Lcom/osp/app/signin/UserValidateCheck;->e(Lcom/osp/app/signin/UserValidateCheck;)Landroid/widget/EditText;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-interface {v0}, Landroid/text/Editable;->length()I

    move-result v0

    if-lez v0, :cond_0

    .line 1142
    iget-object v0, p0, Lcom/osp/app/signin/pi;->a:Lcom/osp/app/signin/UserValidateCheck;

    invoke-static {v0}, Lcom/osp/app/signin/UserValidateCheck;->a(Lcom/osp/app/signin/UserValidateCheck;)V

    .line 1143
    iget-object v0, p0, Lcom/osp/app/signin/pi;->a:Lcom/osp/app/signin/UserValidateCheck;

    invoke-static {v0}, Lcom/osp/app/signin/UserValidateCheck;->k(Lcom/osp/app/signin/UserValidateCheck;)Landroid/app/AlertDialog;

    move-result-object v0

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/osp/app/signin/pi;->a:Lcom/osp/app/signin/UserValidateCheck;

    invoke-static {v0}, Lcom/osp/app/signin/UserValidateCheck;->k(Lcom/osp/app/signin/UserValidateCheck;)Landroid/app/AlertDialog;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/AlertDialog;->isShowing()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 1145
    iget-object v0, p0, Lcom/osp/app/signin/pi;->a:Lcom/osp/app/signin/UserValidateCheck;

    invoke-static {v0}, Lcom/osp/app/signin/UserValidateCheck;->k(Lcom/osp/app/signin/UserValidateCheck;)Landroid/app/AlertDialog;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/AlertDialog;->dismiss()V

    .line 1147
    :cond_2
    iget-object v0, p0, Lcom/osp/app/signin/pi;->a:Lcom/osp/app/signin/UserValidateCheck;

    invoke-static {v0}, Lcom/osp/app/signin/UserValidateCheck;->b(Lcom/osp/app/signin/UserValidateCheck;)Ljava/lang/String;

    move-result-object v0

    const-string v1, "OSP_01"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 1149
    iget-object v0, p0, Lcom/osp/app/signin/pi;->a:Lcom/osp/app/signin/UserValidateCheck;

    new-instance v1, Lcom/osp/app/signin/pn;

    iget-object v2, p0, Lcom/osp/app/signin/pi;->a:Lcom/osp/app/signin/UserValidateCheck;

    invoke-direct {v1, v2}, Lcom/osp/app/signin/pn;-><init>(Lcom/osp/app/signin/UserValidateCheck;)V

    invoke-static {v0, v1}, Lcom/osp/app/signin/UserValidateCheck;->a(Lcom/osp/app/signin/UserValidateCheck;Lcom/osp/app/signin/pn;)Lcom/osp/app/signin/pn;

    .line 1150
    iget-object v0, p0, Lcom/osp/app/signin/pi;->a:Lcom/osp/app/signin/UserValidateCheck;

    invoke-static {v0}, Lcom/osp/app/signin/UserValidateCheck;->c(Lcom/osp/app/signin/UserValidateCheck;)Lcom/osp/app/signin/pn;

    move-result-object v0

    invoke-virtual {v0}, Lcom/osp/app/signin/pn;->b()V

    goto :goto_1

    .line 1153
    :cond_3
    iget-object v0, p0, Lcom/osp/app/signin/pi;->a:Lcom/osp/app/signin/UserValidateCheck;

    new-instance v1, Lcom/osp/app/signin/po;

    iget-object v2, p0, Lcom/osp/app/signin/pi;->a:Lcom/osp/app/signin/UserValidateCheck;

    invoke-direct {v1, v2}, Lcom/osp/app/signin/po;-><init>(Lcom/osp/app/signin/UserValidateCheck;)V

    invoke-static {v0, v1}, Lcom/osp/app/signin/UserValidateCheck;->a(Lcom/osp/app/signin/UserValidateCheck;Lcom/osp/app/signin/po;)Lcom/osp/app/signin/po;

    .line 1154
    iget-object v0, p0, Lcom/osp/app/signin/pi;->a:Lcom/osp/app/signin/UserValidateCheck;

    invoke-static {v0}, Lcom/osp/app/signin/UserValidateCheck;->d(Lcom/osp/app/signin/UserValidateCheck;)Lcom/osp/app/signin/po;

    move-result-object v0

    invoke-virtual {v0}, Lcom/osp/app/signin/po;->b()V

    goto :goto_1

    .line 1134
    :pswitch_data_0
    .packed-switch 0x42
        :pswitch_0
    .end packed-switch
.end method
