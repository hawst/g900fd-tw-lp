.class final Lcom/osp/app/signin/no;
.super Lcom/msc/c/b;
.source "SmsVerificationActivity.java"


# instance fields
.field protected c:Ljava/lang/Runnable;

.field protected d:Ljava/lang/Runnable;

.field protected e:Ljava/lang/Thread;

.field protected f:Ljava/lang/Thread;

.field final synthetic g:Lcom/osp/app/signin/SmsVerificationActivity;

.field private h:Z

.field private i:Z

.field private j:Z

.field private k:Z

.field private l:Ljava/lang/String;

.field private m:Lcom/msc/a/b;

.field private n:J

.field private o:J

.field private p:J

.field private q:Z

.field private final r:Ljava/lang/String;

.field private s:Lcom/msc/a/d;

.field private t:Ljava/lang/String;

.field private u:J


# direct methods
.method public constructor <init>(Lcom/osp/app/signin/SmsVerificationActivity;Landroid/content/Context;)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 3354
    iput-object p1, p0, Lcom/osp/app/signin/no;->g:Lcom/osp/app/signin/SmsVerificationActivity;

    .line 3355
    const v0, 0x7f090055

    invoke-direct {p0, p2, v0}, Lcom/msc/c/b;-><init>(Landroid/content/Context;I)V

    .line 3266
    iput-boolean v1, p0, Lcom/osp/app/signin/no;->h:Z

    .line 3271
    iput-boolean v1, p0, Lcom/osp/app/signin/no;->i:Z

    .line 3276
    iput-boolean v1, p0, Lcom/osp/app/signin/no;->j:Z

    .line 3281
    iput-boolean v1, p0, Lcom/osp/app/signin/no;->k:Z

    .line 3325
    iput-boolean v1, p0, Lcom/osp/app/signin/no;->q:Z

    .line 3357
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-static {p1}, Lcom/osp/app/signin/SmsVerificationActivity;->j(Lcom/osp/app/signin/SmsVerificationActivity;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-static {p1}, Lcom/osp/app/signin/SmsVerificationActivity;->p(Lcom/osp/app/signin/SmsVerificationActivity;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/osp/app/signin/no;->r:Ljava/lang/String;

    .line 3359
    new-instance v0, Lcom/osp/app/signin/np;

    invoke-direct {v0, p0, p1, p2}, Lcom/osp/app/signin/np;-><init>(Lcom/osp/app/signin/no;Lcom/osp/app/signin/SmsVerificationActivity;Landroid/content/Context;)V

    iput-object v0, p0, Lcom/osp/app/signin/no;->c:Ljava/lang/Runnable;

    .line 3382
    new-instance v0, Lcom/osp/app/signin/nq;

    invoke-direct {v0, p0, p1}, Lcom/osp/app/signin/nq;-><init>(Lcom/osp/app/signin/no;Lcom/osp/app/signin/SmsVerificationActivity;)V

    iput-object v0, p0, Lcom/osp/app/signin/no;->d:Ljava/lang/Runnable;

    .line 3400
    new-instance v0, Ljava/lang/Thread;

    iget-object v1, p0, Lcom/osp/app/signin/no;->c:Ljava/lang/Runnable;

    invoke-direct {v0, v1}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;)V

    iput-object v0, p0, Lcom/osp/app/signin/no;->e:Ljava/lang/Thread;

    .line 3401
    new-instance v0, Ljava/lang/Thread;

    iget-object v1, p0, Lcom/osp/app/signin/no;->d:Ljava/lang/Runnable;

    invoke-direct {v0, v1}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;)V

    iput-object v0, p0, Lcom/osp/app/signin/no;->f:Ljava/lang/Thread;

    .line 3405
    return-void
.end method

.method static synthetic a(Lcom/osp/app/signin/no;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 3241
    iget-object v0, p0, Lcom/osp/app/signin/no;->r:Ljava/lang/String;

    return-object v0
.end method

.method private a(Lcom/msc/a/a;)V
    .locals 3

    .prologue
    .line 3817
    invoke-static {}, Lcom/msc/c/g;->a()Lcom/msc/c/g;

    .line 3818
    iget-object v0, p0, Lcom/osp/app/signin/no;->g:Lcom/osp/app/signin/SmsVerificationActivity;

    iget-object v1, p0, Lcom/osp/app/signin/no;->g:Lcom/osp/app/signin/SmsVerificationActivity;

    invoke-static {v1}, Lcom/osp/app/signin/SmsVerificationActivity;->U(Lcom/osp/app/signin/SmsVerificationActivity;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, p1, v1, p0}, Lcom/msc/c/g;->a(Landroid/content/Context;Lcom/msc/a/a;Ljava/lang/String;Lcom/msc/b/h;)J

    move-result-wide v0

    iput-wide v0, p0, Lcom/osp/app/signin/no;->n:J

    .line 3820
    iget-wide v0, p0, Lcom/osp/app/signin/no;->n:J

    invoke-virtual {p0, v0, v1}, Lcom/osp/app/signin/no;->a(J)V

    .line 3821
    iget-wide v0, p0, Lcom/osp/app/signin/no;->n:J

    const-string v2, "from_xml"

    invoke-virtual {p0, v0, v1, v2}, Lcom/osp/app/signin/no;->a(JLjava/lang/String;)V

    .line 3823
    iget-wide v0, p0, Lcom/osp/app/signin/no;->n:J

    sget-object v2, Lcom/msc/b/g;->c:Lcom/msc/b/g;

    invoke-static {v0, v1, v2}, Lcom/msc/c/g;->a(JLcom/msc/b/g;)V

    .line 3824
    return-void
.end method

.method static synthetic a(Lcom/osp/app/signin/no;Lcom/msc/a/a;)V
    .locals 0

    .prologue
    .line 3241
    invoke-direct {p0, p1}, Lcom/osp/app/signin/no;->a(Lcom/msc/a/a;)V

    return-void
.end method

.method static synthetic a(Lcom/osp/app/signin/no;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 8

    .prologue
    const/4 v5, 0x0

    .line 3241
    invoke-static {}, Lcom/msc/c/g;->a()Lcom/msc/c/g;

    iget-object v0, p0, Lcom/osp/app/signin/no;->g:Lcom/osp/app/signin/SmsVerificationActivity;

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    move-object v6, v5

    move-object v7, p0

    invoke-static/range {v0 .. v7}, Lcom/msc/c/g;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/osp/app/signin/cn;Lcom/msc/b/h;)J

    move-result-wide v0

    iput-wide v0, p0, Lcom/osp/app/signin/no;->o:J

    iget-wide v0, p0, Lcom/osp/app/signin/no;->o:J

    invoke-virtual {p0, v0, v1}, Lcom/osp/app/signin/no;->b(J)V

    iget-wide v0, p0, Lcom/osp/app/signin/no;->o:J

    const-string v2, "from_json"

    invoke-virtual {p0, v0, v1, v2}, Lcom/osp/app/signin/no;->a(JLjava/lang/String;)V

    iget-wide v0, p0, Lcom/osp/app/signin/no;->o:J

    sget-object v2, Lcom/msc/b/g;->c:Lcom/msc/b/g;

    invoke-static {v0, v1, v2}, Lcom/msc/c/g;->a(JLcom/msc/b/g;)V

    return-void
.end method

.method private static a(Ljava/lang/Thread;)V
    .locals 2

    .prologue
    .line 3426
    if-eqz p0, :cond_0

    invoke-virtual {p0}, Ljava/lang/Thread;->isAlive()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 3430
    const-wide/16 v0, 0x12c

    :try_start_0
    invoke-virtual {p0, v0, v1}, Ljava/lang/Thread;->join(J)V
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0

    .line 3435
    :cond_0
    :goto_0
    return-void

    .line 3431
    :catch_0
    move-exception v0

    invoke-virtual {v0}, Ljava/lang/InterruptedException;->printStackTrace()V

    goto :goto_0
.end method

.method private b(Ljava/lang/String;)V
    .locals 8

    .prologue
    const/4 v0, 0x1

    const/4 v3, 0x0

    .line 3902
    const-string v1, "Success"

    invoke-virtual {v1, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_6

    .line 3907
    iget-object v1, p0, Lcom/osp/app/signin/no;->g:Lcom/osp/app/signin/SmsVerificationActivity;

    invoke-static {v1}, Landroid/preference/PreferenceManager;->getDefaultSharedPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v1

    invoke-interface {v1}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v1

    .line 3908
    const-string v2, "FLAG_DO_NOT_SHOW_NOTIFICATION"

    invoke-interface {v1, v2, v3}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    .line 3909
    invoke-interface {v1}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 3911
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v1, "===========================================SignIn_End PROCESSING_SUCCESS==========================================="

    invoke-static {v1}, Lcom/osp/app/util/aq;->c(Ljava/lang/String;)V

    .line 3912
    iget-object v1, p0, Lcom/osp/app/signin/no;->g:Lcom/osp/app/signin/SmsVerificationActivity;

    invoke-static {v1}, Lcom/osp/app/util/ad;->d(Landroid/content/Context;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 3914
    iget-object v0, p0, Lcom/osp/app/signin/no;->g:Lcom/osp/app/signin/SmsVerificationActivity;

    iget-object v1, p0, Lcom/osp/app/signin/no;->r:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/osp/app/util/ad;->a(Landroid/content/Context;Ljava/lang/String;)Z

    move-result v0

    .line 3925
    :goto_0
    if-nez v0, :cond_1

    .line 3927
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v0, "SMSV"

    const-string v1, "SmsVerification SignInNewDualEnd"

    invoke-static {v0, v1}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 3928
    new-instance v0, Lcom/msc/c/f;

    const-string v1, "ERR_MDM_SECURITY"

    const-string v2, "fail_MDM_Security"

    invoke-direct {v0, v1, v2}, Lcom/msc/c/f;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    iput-object v0, p0, Lcom/osp/app/signin/no;->b:Lcom/msc/c/f;

    .line 3929
    iget-object v0, p0, Lcom/osp/app/signin/no;->g:Lcom/osp/app/signin/SmsVerificationActivity;

    const/16 v1, 0x15

    invoke-virtual {v0, v1}, Lcom/osp/app/signin/SmsVerificationActivity;->b(I)V

    .line 3930
    const-string v0, "Fail"

    invoke-direct {p0, v0}, Lcom/osp/app/signin/no;->b(Ljava/lang/String;)V

    .line 3932
    iget-object v0, p0, Lcom/osp/app/signin/no;->g:Lcom/osp/app/signin/SmsVerificationActivity;

    invoke-virtual {v0}, Lcom/osp/app/signin/SmsVerificationActivity;->finish()V

    .line 4067
    :goto_1
    return-void

    .line 3918
    :cond_0
    iget-object v1, p0, Lcom/osp/app/signin/no;->g:Lcom/osp/app/signin/SmsVerificationActivity;

    invoke-static {v1}, Lcom/osp/app/util/ad;->j(Landroid/content/Context;)V

    goto :goto_0

    .line 3943
    :cond_1
    :try_start_0
    iget-object v0, p0, Lcom/osp/app/signin/no;->g:Lcom/osp/app/signin/SmsVerificationActivity;

    sget-object v1, Lcom/msc/c/d;->c:Lcom/msc/c/d;

    invoke-virtual {v1}, Lcom/msc/c/d;->toString()Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/osp/app/signin/no;->m:Lcom/msc/a/b;

    invoke-virtual {v2}, Lcom/msc/a/b;->a()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v1, v2}, Lcom/msc/c/c;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)V

    .line 3944
    iget-object v0, p0, Lcom/osp/app/signin/no;->g:Lcom/osp/app/signin/SmsVerificationActivity;

    sget-object v1, Lcom/msc/c/d;->d:Lcom/msc/c/d;

    invoke-virtual {v1}, Lcom/msc/c/d;->toString()Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/osp/app/signin/no;->m:Lcom/msc/a/b;

    invoke-virtual {v2}, Lcom/msc/a/b;->b()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v1, v2}, Lcom/msc/c/c;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)V

    .line 3945
    iget-object v0, p0, Lcom/osp/app/signin/no;->g:Lcom/osp/app/signin/SmsVerificationActivity;

    sget-object v1, Lcom/msc/c/d;->a:Lcom/msc/c/d;

    invoke-virtual {v1}, Lcom/msc/c/d;->toString()Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/osp/app/signin/no;->m:Lcom/msc/a/b;

    invoke-virtual {v2}, Lcom/msc/a/b;->c()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v1, v2}, Lcom/msc/c/c;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)V

    .line 3946
    iget-object v0, p0, Lcom/osp/app/signin/no;->g:Lcom/osp/app/signin/SmsVerificationActivity;

    sget-object v1, Lcom/msc/c/d;->f:Lcom/msc/c/d;

    invoke-virtual {v1}, Lcom/msc/c/d;->toString()Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/osp/app/signin/no;->m:Lcom/msc/a/b;

    invoke-virtual {v2}, Lcom/msc/a/b;->d()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v1, v2}, Lcom/msc/c/c;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)V

    .line 3947
    iget-object v0, p0, Lcom/osp/app/signin/no;->g:Lcom/osp/app/signin/SmsVerificationActivity;

    sget-object v1, Lcom/msc/c/d;->b:Lcom/msc/c/d;

    invoke-virtual {v1}, Lcom/msc/c/d;->toString()Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/osp/app/signin/no;->r:Ljava/lang/String;

    invoke-static {v0, v1, v2}, Lcom/msc/c/c;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)V

    .line 3950
    iget-object v0, p0, Lcom/osp/app/signin/no;->g:Lcom/osp/app/signin/SmsVerificationActivity;

    sget-object v1, Lcom/msc/c/d;->h:Lcom/msc/c/d;

    invoke-virtual {v1}, Lcom/msc/c/d;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {}, Lcom/osp/common/util/i;->a()Lcom/osp/common/util/i;

    iget-object v2, p0, Lcom/osp/app/signin/no;->g:Lcom/osp/app/signin/SmsVerificationActivity;

    invoke-static {v2}, Lcom/osp/common/util/i;->f(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v1, v2}, Lcom/msc/c/c;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)V

    .line 3952
    iget-object v0, p0, Lcom/osp/app/signin/no;->g:Lcom/osp/app/signin/SmsVerificationActivity;

    sget-object v1, Lcom/msc/c/d;->i:Lcom/msc/c/d;

    invoke-virtual {v1}, Lcom/msc/c/d;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {}, Lcom/osp/common/util/h;->a()Lcom/osp/common/util/h;

    iget-object v2, p0, Lcom/osp/app/signin/no;->g:Lcom/osp/app/signin/SmsVerificationActivity;

    invoke-static {v2}, Lcom/osp/common/util/h;->a(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v1, v2}, Lcom/msc/c/c;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)V

    .line 3954
    iget-object v0, p0, Lcom/osp/app/signin/no;->g:Lcom/osp/app/signin/SmsVerificationActivity;

    sget-object v1, Lcom/msc/c/d;->g:Lcom/msc/c/d;

    invoke-virtual {v1}, Lcom/msc/c/d;->toString()Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/osp/app/signin/no;->m:Lcom/msc/a/b;

    invoke-virtual {v2}, Lcom/msc/a/b;->g()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v1, v2}, Lcom/msc/c/c;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)V

    .line 3955
    iget-object v0, p0, Lcom/osp/app/signin/no;->g:Lcom/osp/app/signin/SmsVerificationActivity;

    const-string v1, "j5p7ll8g33"

    invoke-static {v0, v1}, Lcom/msc/c/c;->a(Landroid/content/Context;Ljava/lang/String;)V

    .line 3956
    iget-object v0, p0, Lcom/osp/app/signin/no;->g:Lcom/osp/app/signin/SmsVerificationActivity;

    const-string v1, "j5p7ll8g33"

    const-string v2, "5763D0052DC1462E13751F753384E9A9"

    iget-object v3, p0, Lcom/osp/app/signin/no;->m:Lcom/msc/a/b;

    invoke-virtual {v3}, Lcom/msc/a/b;->e()Ljava/lang/String;

    move-result-object v3

    iget-object v4, p0, Lcom/osp/app/signin/no;->m:Lcom/msc/a/b;

    invoke-virtual {v4}, Lcom/msc/a/b;->f()Ljava/lang/String;

    move-result-object v4

    invoke-static {v0, v1, v2, v3, v4}, Lcom/msc/c/c;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 3963
    :goto_2
    iget-object v0, p0, Lcom/osp/app/signin/no;->g:Lcom/osp/app/signin/SmsVerificationActivity;

    iget-object v1, p0, Lcom/osp/app/signin/no;->t:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/msc/c/e;->e(Landroid/content/Context;Ljava/lang/String;)V

    .line 3964
    iget-object v0, p0, Lcom/osp/app/signin/no;->g:Lcom/osp/app/signin/SmsVerificationActivity;

    iget-object v1, p0, Lcom/osp/app/signin/no;->g:Lcom/osp/app/signin/SmsVerificationActivity;

    invoke-static {v1}, Lcom/osp/app/signin/SmsVerificationActivity;->U(Lcom/osp/app/signin/SmsVerificationActivity;)Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/osp/app/signin/no;->r:Ljava/lang/String;

    iget-object v3, p0, Lcom/osp/app/signin/no;->m:Lcom/msc/a/b;

    invoke-virtual {v3}, Lcom/msc/a/b;->d()Ljava/lang/String;

    move-result-object v3

    invoke-static {v0, v1, v2, v3}, Lcom/msc/c/e;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 3974
    iget-object v0, p0, Lcom/osp/app/signin/no;->g:Lcom/osp/app/signin/SmsVerificationActivity;

    invoke-static {v0}, Lcom/osp/app/signin/SmsVerificationActivity;->W(Lcom/osp/app/signin/SmsVerificationActivity;)Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_2

    const-string v0, "j5p7ll8g33"

    iget-object v1, p0, Lcom/osp/app/signin/no;->g:Lcom/osp/app/signin/SmsVerificationActivity;

    invoke-static {v1}, Lcom/osp/app/signin/SmsVerificationActivity;->W(Lcom/osp/app/signin/SmsVerificationActivity;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_5

    :cond_2
    iget-object v0, p0, Lcom/osp/app/signin/no;->s:Lcom/msc/a/d;

    invoke-virtual {v0}, Lcom/msc/a/d;->g()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_5

    iget-object v0, p0, Lcom/osp/app/signin/no;->s:Lcom/msc/a/d;

    invoke-virtual {v0}, Lcom/msc/a/d;->g()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_5

    .line 3978
    iget-object v0, p0, Lcom/osp/app/signin/no;->g:Lcom/osp/app/signin/SmsVerificationActivity;

    iget-object v1, p0, Lcom/osp/app/signin/no;->s:Lcom/msc/a/d;

    invoke-virtual {v1}, Lcom/msc/a/d;->g()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/msc/c/e;->b(Landroid/content/Context;Ljava/lang/String;)V

    .line 3987
    :cond_3
    :goto_3
    iget-object v0, p0, Lcom/osp/app/signin/no;->g:Lcom/osp/app/signin/SmsVerificationActivity;

    invoke-static {v0}, Lcom/osp/app/signin/SmsVerificationActivity;->aa(Lcom/osp/app/signin/SmsVerificationActivity;)V

    .line 3989
    iget-object v0, p0, Lcom/osp/app/signin/no;->g:Lcom/osp/app/signin/SmsVerificationActivity;

    invoke-static {v0}, Lcom/osp/app/signin/SmsVerificationActivity;->ab(Lcom/osp/app/signin/SmsVerificationActivity;)V

    .line 3991
    invoke-static {}, Lcom/osp/app/pushmarketing/k;->a()Lcom/osp/app/pushmarketing/k;

    iget-object v0, p0, Lcom/osp/app/signin/no;->g:Lcom/osp/app/signin/SmsVerificationActivity;

    invoke-static {v0}, Lcom/osp/app/pushmarketing/k;->c(Landroid/content/Context;)V

    .line 4065
    :cond_4
    :goto_4
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v0, "SMSV"

    const-string v1, "SignInDual_End"

    const-string v2, "END"

    invoke-static {v0, v1, v2}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_1

    .line 3958
    :catch_0
    move-exception v0

    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_2

    .line 3979
    :cond_5
    const-string v0, "tj9u972o46"

    iget-object v1, p0, Lcom/osp/app/signin/no;->g:Lcom/osp/app/signin/SmsVerificationActivity;

    invoke-static {v1}, Lcom/osp/app/signin/SmsVerificationActivity;->W(Lcom/osp/app/signin/SmsVerificationActivity;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_3

    .line 3982
    iget-object v0, p0, Lcom/osp/app/signin/no;->g:Lcom/osp/app/signin/SmsVerificationActivity;

    iget-object v1, p0, Lcom/osp/app/signin/no;->g:Lcom/osp/app/signin/SmsVerificationActivity;

    invoke-static {v1}, Lcom/osp/app/signin/SmsVerificationActivity;->W(Lcom/osp/app/signin/SmsVerificationActivity;)Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/osp/app/signin/no;->s:Lcom/msc/a/d;

    invoke-virtual {v2}, Lcom/msc/a/d;->g()Ljava/lang/String;

    move-result-object v2

    iget-object v3, p0, Lcom/osp/app/signin/no;->s:Lcom/msc/a/d;

    invoke-virtual {v3}, Lcom/msc/a/d;->j()J

    move-result-wide v3

    iget-object v5, p0, Lcom/osp/app/signin/no;->s:Lcom/msc/a/d;

    invoke-virtual {v5}, Lcom/msc/a/d;->h()Ljava/lang/String;

    move-result-object v5

    iget-object v6, p0, Lcom/osp/app/signin/no;->s:Lcom/msc/a/d;

    invoke-virtual {v6}, Lcom/msc/a/d;->k()J

    move-result-wide v6

    invoke-static/range {v0 .. v7}, Lcom/osp/app/util/ad;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;JLjava/lang/String;J)V

    goto :goto_3

    .line 4044
    :cond_6
    iget-object v1, p0, Lcom/osp/app/signin/no;->g:Lcom/osp/app/signin/SmsVerificationActivity;

    invoke-virtual {v1}, Lcom/osp/app/signin/SmsVerificationActivity;->d()V

    .line 4046
    invoke-static {}, Lcom/osp/device/b;->b()Lcom/osp/device/b;

    move-result-object v1

    invoke-virtual {v1}, Lcom/osp/device/b;->a()Z

    move-result v1

    if-eqz v1, :cond_7

    .line 4048
    iget-boolean v1, p0, Lcom/osp/app/signin/no;->q:Z

    if-nez v1, :cond_8

    .line 4050
    :cond_7
    invoke-virtual {p0, v3}, Lcom/osp/app/signin/no;->a(Z)V

    .line 4057
    :cond_8
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v1, "SMSV"

    const-string v2, "SignInDual_End"

    const-string v3, "END"

    invoke-static {v1, v2, v3}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 4059
    const-string v1, "ERR_MDM_SECURITY"

    iget-object v2, p0, Lcom/osp/app/signin/no;->b:Lcom/msc/c/f;

    invoke-virtual {v2}, Lcom/msc/c/f;->a()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_4

    .line 4061
    iget-object v1, p0, Lcom/osp/app/signin/no;->g:Lcom/osp/app/signin/SmsVerificationActivity;

    invoke-virtual {v1, v0}, Lcom/osp/app/signin/SmsVerificationActivity;->b(I)V

    .line 4062
    iget-object v0, p0, Lcom/osp/app/signin/no;->g:Lcom/osp/app/signin/SmsVerificationActivity;

    invoke-virtual {v0}, Lcom/osp/app/signin/SmsVerificationActivity;->finish()V

    goto :goto_4
.end method

.method static synthetic b(Lcom/osp/app/signin/no;)Z
    .locals 1

    .prologue
    .line 3241
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/osp/app/signin/no;->h:Z

    return v0
.end method

.method static synthetic c(Lcom/osp/app/signin/no;)Z
    .locals 1

    .prologue
    .line 3241
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/osp/app/signin/no;->i:Z

    return v0
.end method


# virtual methods
.method protected final varargs a()Ljava/lang/Boolean;
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 3452
    iget-object v0, p0, Lcom/osp/app/signin/no;->r:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/osp/app/signin/no;->g:Lcom/osp/app/signin/SmsVerificationActivity;

    invoke-static {v0}, Lcom/osp/app/signin/SmsVerificationActivity;->V(Lcom/osp/app/signin/SmsVerificationActivity;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 3454
    :cond_0
    invoke-static {v4}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    .line 3483
    :goto_0
    return-object v0

    .line 3457
    :cond_1
    iget-object v0, p0, Lcom/osp/app/signin/no;->g:Lcom/osp/app/signin/SmsVerificationActivity;

    invoke-static {v0}, Lcom/osp/app/signin/SmsVerificationActivity;->Y(Lcom/osp/app/signin/SmsVerificationActivity;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 3459
    invoke-static {}, Lcom/msc/c/g;->a()Lcom/msc/c/g;

    iget-object v0, p0, Lcom/osp/app/signin/no;->g:Lcom/osp/app/signin/SmsVerificationActivity;

    iget-object v1, p0, Lcom/osp/app/signin/no;->r:Ljava/lang/String;

    sget-object v2, Ljava/util/Locale;->ENGLISH:Ljava/util/Locale;

    invoke-virtual {v1, v2}, Ljava/lang/String;->toLowerCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/osp/app/signin/no;->g:Lcom/osp/app/signin/SmsVerificationActivity;

    invoke-static {v2}, Lcom/osp/app/signin/SmsVerificationActivity;->V(Lcom/osp/app/signin/SmsVerificationActivity;)Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x0

    invoke-static {v0, v1, v2, v3, p0}, Lcom/msc/c/g;->b(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Lcom/osp/app/signin/cn;Lcom/msc/b/h;)J

    move-result-wide v0

    iput-wide v0, p0, Lcom/osp/app/signin/no;->u:J

    iget-wide v0, p0, Lcom/osp/app/signin/no;->u:J

    invoke-virtual {p0, v0, v1}, Lcom/osp/app/signin/no;->b(J)V

    iget-wide v0, p0, Lcom/osp/app/signin/no;->u:J

    const-string v2, "from_json"

    invoke-virtual {p0, v0, v1, v2}, Lcom/osp/app/signin/no;->a(JLjava/lang/String;)V

    iget-wide v0, p0, Lcom/osp/app/signin/no;->u:J

    sget-object v2, Lcom/msc/b/g;->c:Lcom/msc/b/g;

    invoke-static {v0, v1, v2}, Lcom/msc/c/g;->a(JLcom/msc/b/g;)V

    .line 3483
    :goto_1
    invoke-static {v4}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    goto :goto_0

    .line 3464
    :cond_2
    iget-object v0, p0, Lcom/osp/app/signin/no;->e:Ljava/lang/Thread;

    invoke-virtual {v0}, Ljava/lang/Thread;->start()V

    .line 3465
    iget-object v0, p0, Lcom/osp/app/signin/no;->f:Ljava/lang/Thread;

    invoke-virtual {v0}, Ljava/lang/Thread;->start()V

    .line 3467
    :cond_3
    iget-boolean v0, p0, Lcom/osp/app/signin/no;->h:Z

    if-eqz v0, :cond_4

    iget-boolean v0, p0, Lcom/osp/app/signin/no;->i:Z

    if-nez v0, :cond_5

    .line 3469
    :cond_4
    invoke-virtual {p0}, Lcom/osp/app/signin/no;->isCancelled()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 3471
    :cond_5
    iget-boolean v0, p0, Lcom/osp/app/signin/no;->k:Z

    if-eqz v0, :cond_6

    iget-boolean v0, p0, Lcom/osp/app/signin/no;->j:Z

    if-eqz v0, :cond_6

    .line 3477
    const-string v0, "Success"

    iput-object v0, p0, Lcom/osp/app/signin/no;->l:Ljava/lang/String;

    goto :goto_1

    .line 3480
    :cond_6
    const-string v0, "Fail"

    iput-object v0, p0, Lcom/osp/app/signin/no;->l:Ljava/lang/String;

    goto :goto_1
.end method

.method public final a(Lcom/msc/a/f;)V
    .locals 3

    .prologue
    .line 3864
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v0, "SMSV"

    const-string v1, "RequestCheckListInfo"

    const-string v2, "START"

    invoke-static {v0, v1, v2}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 3866
    invoke-static {}, Lcom/msc/c/g;->a()Lcom/msc/c/g;

    .line 3867
    iget-object v0, p0, Lcom/osp/app/signin/no;->g:Lcom/osp/app/signin/SmsVerificationActivity;

    invoke-static {v0, p1, p0}, Lcom/msc/c/g;->a(Landroid/content/Context;Lcom/msc/a/f;Lcom/msc/b/h;)J

    move-result-wide v0

    iput-wide v0, p0, Lcom/osp/app/signin/no;->p:J

    .line 3868
    iget-wide v0, p0, Lcom/osp/app/signin/no;->p:J

    invoke-virtual {p0, v0, v1}, Lcom/osp/app/signin/no;->c(J)V

    .line 3869
    iget-wide v0, p0, Lcom/osp/app/signin/no;->p:J

    const-string v2, "from_xml"

    invoke-virtual {p0, v0, v1, v2}, Lcom/osp/app/signin/no;->a(JLjava/lang/String;)V

    .line 3871
    iget-wide v0, p0, Lcom/osp/app/signin/no;->p:J

    sget-object v2, Lcom/msc/b/g;->c:Lcom/msc/b/g;

    invoke-static {v0, v1, v2}, Lcom/msc/c/g;->a(JLcom/msc/b/g;)V

    .line 3873
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v0, "SMSV"

    const-string v1, "RequestCheckListInfo"

    const-string v2, "END"

    invoke-static {v0, v1, v2}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 3874
    return-void
.end method

.method public final declared-synchronized a(Lcom/msc/b/c;)V
    .locals 6

    .prologue
    .line 3558
    monitor-enter p0

    :try_start_0
    invoke-super {p0, p1}, Lcom/msc/c/b;->a(Lcom/msc/b/c;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 3560
    if-nez p1, :cond_1

    .line 3731
    :cond_0
    :goto_0
    monitor-exit p0

    return-void

    .line 3565
    :cond_1
    :try_start_1
    invoke-virtual {p1}, Lcom/msc/b/c;->a()J

    move-result-wide v0

    .line 3566
    invoke-virtual {p1}, Lcom/msc/b/c;->e()Ljava/lang/String;

    move-result-object v2

    .line 3568
    iget-wide v4, p0, Lcom/osp/app/signin/no;->n:J
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    cmp-long v3, v0, v4

    if-nez v3, :cond_6

    .line 3572
    :try_start_2
    invoke-static {}, Lcom/msc/c/h;->a()Lcom/msc/c/h;

    invoke-static {v2}, Lcom/msc/c/h;->r(Ljava/lang/String;)Lcom/msc/a/b;

    move-result-object v0

    iput-object v0, p0, Lcom/osp/app/signin/no;->m:Lcom/msc/a/b;

    .line 3575
    new-instance v0, Lcom/osp/common/property/a;

    iget-object v1, p0, Lcom/osp/app/signin/no;->g:Lcom/osp/app/signin/SmsVerificationActivity;

    invoke-direct {v0, v1}, Lcom/osp/common/property/a;-><init>(Landroid/content/Context;)V

    .line 3576
    const-string v1, "device.registration.appid"

    invoke-virtual {v0, v1}, Lcom/osp/common/property/a;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_2

    .line 3578
    const-string v1, "device.registration.appid"

    invoke-virtual {v0, v1}, Lcom/osp/common/property/a;->c(Ljava/lang/String;)V

    .line 3580
    :cond_2
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v1, "SMSV"

    const-string v3, "save appid in property."

    invoke-static {v1, v3}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 3581
    const-string v1, "device.registration.appid"

    const-string v3, "j5p7ll8g33"

    invoke-virtual {v0, v1, v3}, Lcom/osp/common/property/a;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 3583
    iget-object v0, p0, Lcom/osp/app/signin/no;->g:Lcom/osp/app/signin/SmsVerificationActivity;

    invoke-static {v0}, Lcom/osp/app/signin/SmsVerificationActivity;->Y(Lcom/osp/app/signin/SmsVerificationActivity;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 3585
    const-string v0, "Success"

    iput-object v0, p0, Lcom/osp/app/signin/no;->l:Ljava/lang/String;

    .line 3588
    :cond_3
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    invoke-static {v2}, Lcom/osp/app/util/aq;->b(Ljava/lang/String;)V
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_0
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 3597
    :try_start_3
    iget-object v0, p0, Lcom/osp/app/signin/no;->m:Lcom/msc/a/b;

    if-eqz v0, :cond_5

    .line 3599
    iget-object v0, p0, Lcom/osp/app/signin/no;->m:Lcom/msc/a/b;

    invoke-virtual {v0}, Lcom/msc/a/b;->e()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_4

    iget-object v0, p0, Lcom/osp/app/signin/no;->m:Lcom/msc/a/b;

    invoke-virtual {v0}, Lcom/msc/a/b;->e()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v0

    if-lez v0, :cond_4

    .line 3601
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/osp/app/signin/no;->k:Z
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    goto :goto_0

    .line 3558
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 3589
    :catch_0
    move-exception v0

    .line 3591
    :try_start_4
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    .line 3592
    const/4 v1, 0x0

    iput-boolean v1, p0, Lcom/osp/app/signin/no;->k:Z

    .line 3593
    new-instance v1, Lcom/msc/c/f;

    invoke-direct {v1, v0}, Lcom/msc/c/f;-><init>(Ljava/lang/Exception;)V

    iput-object v1, p0, Lcom/osp/app/signin/no;->b:Lcom/msc/c/f;

    goto :goto_0

    .line 3604
    :cond_4
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/osp/app/signin/no;->k:Z

    .line 3605
    new-instance v0, Lcom/msc/c/f;

    invoke-direct {v0}, Lcom/msc/c/f;-><init>()V

    iput-object v0, p0, Lcom/osp/app/signin/no;->b:Lcom/msc/c/f;

    .line 3606
    iget-object v0, p0, Lcom/osp/app/signin/no;->g:Lcom/osp/app/signin/SmsVerificationActivity;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/osp/app/signin/SmsVerificationActivity;->b(I)V

    goto/16 :goto_0

    .line 3610
    :cond_5
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/osp/app/signin/no;->k:Z

    .line 3611
    new-instance v0, Lcom/msc/c/f;

    invoke-direct {v0}, Lcom/msc/c/f;-><init>()V

    iput-object v0, p0, Lcom/osp/app/signin/no;->b:Lcom/msc/c/f;

    .line 3612
    iget-object v0, p0, Lcom/osp/app/signin/no;->g:Lcom/osp/app/signin/SmsVerificationActivity;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/osp/app/signin/SmsVerificationActivity;->b(I)V

    goto/16 :goto_0

    .line 3615
    :cond_6
    iget-wide v4, p0, Lcom/osp/app/signin/no;->o:J
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    cmp-long v3, v0, v4

    if-nez v3, :cond_8

    .line 3619
    :try_start_5
    new-instance v0, Lcom/msc/a/d;

    invoke-direct {v0}, Lcom/msc/a/d;-><init>()V

    .line 3620
    invoke-static {}, Lcom/msc/c/h;->a()Lcom/msc/c/h;

    invoke-static {v2, v0}, Lcom/msc/c/h;->a(Ljava/lang/String;Lcom/msc/a/d;)V

    .line 3621
    invoke-virtual {v0}, Lcom/msc/a/d;->f()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/osp/app/signin/no;->t:Ljava/lang/String;

    .line 3622
    iget-object v1, p0, Lcom/osp/app/signin/no;->g:Lcom/osp/app/signin/SmsVerificationActivity;

    invoke-virtual {v0}, Lcom/msc/a/d;->i()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/osp/app/signin/SmsVerificationActivity;->k(Lcom/osp/app/signin/SmsVerificationActivity;Ljava/lang/String;)Ljava/lang/String;

    .line 3623
    iput-object v0, p0, Lcom/osp/app/signin/no;->s:Lcom/msc/a/d;

    .line 3637
    iget-object v0, p0, Lcom/osp/app/signin/no;->g:Lcom/osp/app/signin/SmsVerificationActivity;

    invoke-static {v0}, Lcom/osp/app/signin/SmsVerificationActivity;->U(Lcom/osp/app/signin/SmsVerificationActivity;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_7

    iget-object v0, p0, Lcom/osp/app/signin/no;->t:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_7

    iget-object v0, p0, Lcom/osp/app/signin/no;->s:Lcom/msc/a/d;

    invoke-virtual {v0}, Lcom/msc/a/d;->g()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_7

    .line 3639
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/osp/app/signin/no;->j:Z
    :try_end_5
    .catch Ljava/lang/Exception; {:try_start_5 .. :try_end_5} :catch_1
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    goto/16 :goto_0

    .line 3645
    :catch_1
    move-exception v0

    .line 3647
    :try_start_6
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    .line 3648
    new-instance v1, Lcom/msc/c/f;

    invoke-direct {v1, v0}, Lcom/msc/c/f;-><init>(Ljava/lang/Exception;)V

    iput-object v1, p0, Lcom/osp/app/signin/no;->b:Lcom/msc/c/f;
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_0

    goto/16 :goto_0

    .line 3642
    :cond_7
    :try_start_7
    new-instance v0, Lcom/msc/c/f;

    const-string v1, "ERR_0000"

    const-string v2, "fail"

    invoke-direct {v0, v1, v2}, Lcom/msc/c/f;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    iput-object v0, p0, Lcom/osp/app/signin/no;->b:Lcom/msc/c/f;
    :try_end_7
    .catch Ljava/lang/Exception; {:try_start_7 .. :try_end_7} :catch_1
    .catchall {:try_start_7 .. :try_end_7} :catchall_0

    goto/16 :goto_0

    .line 3650
    :cond_8
    :try_start_8
    iget-wide v4, p0, Lcom/osp/app/signin/no;->p:J
    :try_end_8
    .catchall {:try_start_8 .. :try_end_8} :catchall_0

    cmp-long v3, v0, v4

    if-nez v3, :cond_9

    .line 3654
    :try_start_9
    invoke-static {}, Lcom/msc/c/h;->a()Lcom/msc/c/h;

    iget-object v0, p0, Lcom/osp/app/signin/no;->g:Lcom/osp/app/signin/SmsVerificationActivity;

    invoke-static {v0, v2}, Lcom/msc/c/h;->b(Landroid/content/Context;Ljava/lang/String;)Lcom/msc/a/g;

    move-result-object v0

    .line 3655
    if-eqz v0, :cond_0

    .line 3657
    iget-object v1, p0, Lcom/osp/app/signin/no;->g:Lcom/osp/app/signin/SmsVerificationActivity;

    invoke-virtual {v0}, Lcom/msc/a/g;->f()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Lcom/msc/c/e;->d(Landroid/content/Context;Ljava/lang/String;)V
    :try_end_9
    .catch Ljava/lang/Exception; {:try_start_9 .. :try_end_9} :catch_2
    .catchall {:try_start_9 .. :try_end_9} :catchall_0

    goto/16 :goto_0

    .line 3660
    :catch_2
    move-exception v0

    :try_start_a
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    goto/16 :goto_0

    .line 3664
    :cond_9
    iget-wide v4, p0, Lcom/osp/app/signin/no;->u:J

    cmp-long v0, v0, v4

    if-nez v0, :cond_0

    .line 3667
    new-instance v0, Lcom/msc/a/d;

    invoke-direct {v0}, Lcom/msc/a/d;-><init>()V
    :try_end_a
    .catchall {:try_start_a .. :try_end_a} :catchall_0

    .line 3670
    :try_start_b
    invoke-static {}, Lcom/msc/c/h;->a()Lcom/msc/c/h;

    invoke-static {v2, v0}, Lcom/msc/c/h;->a(Ljava/lang/String;Lcom/msc/a/d;)V

    .line 3671
    invoke-virtual {v0}, Lcom/msc/a/d;->f()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/osp/app/signin/no;->t:Ljava/lang/String;

    .line 3672
    iget-object v1, p0, Lcom/osp/app/signin/no;->g:Lcom/osp/app/signin/SmsVerificationActivity;

    invoke-virtual {v0}, Lcom/msc/a/d;->i()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/osp/app/signin/SmsVerificationActivity;->k(Lcom/osp/app/signin/SmsVerificationActivity;Ljava/lang/String;)Ljava/lang/String;

    .line 3673
    iput-object v0, p0, Lcom/osp/app/signin/no;->s:Lcom/msc/a/d;

    .line 3675
    iget-object v0, p0, Lcom/osp/app/signin/no;->g:Lcom/osp/app/signin/SmsVerificationActivity;

    invoke-static {v0}, Lcom/osp/app/signin/SmsVerificationActivity;->U(Lcom/osp/app/signin/SmsVerificationActivity;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_a

    iget-object v0, p0, Lcom/osp/app/signin/no;->t:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_a

    .line 3677
    new-instance v0, Lcom/msc/a/a;

    invoke-direct {v0}, Lcom/msc/a/a;-><init>()V

    .line 3678
    iget-object v1, p0, Lcom/osp/app/signin/no;->g:Lcom/osp/app/signin/SmsVerificationActivity;

    iget-object v2, p0, Lcom/osp/app/signin/no;->r:Ljava/lang/String;

    sget-object v3, Ljava/util/Locale;->ENGLISH:Ljava/util/Locale;

    invoke-virtual {v2, v3}, Ljava/lang/String;->toLowerCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v2

    iget-object v3, p0, Lcom/osp/app/signin/no;->g:Lcom/osp/app/signin/SmsVerificationActivity;

    invoke-static {v3}, Lcom/osp/app/signin/SmsVerificationActivity;->V(Lcom/osp/app/signin/SmsVerificationActivity;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v1, v2, v3}, Lcom/msc/a/a;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)V

    .line 3680
    invoke-direct {p0, v0}, Lcom/osp/app/signin/no;->a(Lcom/msc/a/a;)V
    :try_end_b
    .catch Ljava/lang/Exception; {:try_start_b .. :try_end_b} :catch_3
    .catchall {:try_start_b .. :try_end_b} :catchall_0

    goto/16 :goto_0

    .line 3686
    :catch_3
    move-exception v0

    :try_start_c
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V
    :try_end_c
    .catchall {:try_start_c .. :try_end_c} :catchall_0

    goto/16 :goto_0

    .line 3683
    :cond_a
    :try_start_d
    new-instance v0, Lcom/msc/c/f;

    const-string v1, "ERR_0000"

    const-string v2, "fail"

    invoke-direct {v0, v1, v2}, Lcom/msc/c/f;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    iput-object v0, p0, Lcom/osp/app/signin/no;->b:Lcom/msc/c/f;
    :try_end_d
    .catch Ljava/lang/Exception; {:try_start_d .. :try_end_d} :catch_3
    .catchall {:try_start_d .. :try_end_d} :catchall_0

    goto/16 :goto_0
.end method

.method protected final a(Ljava/lang/Boolean;)V
    .locals 3

    .prologue
    .line 3501
    invoke-super {p0, p1}, Lcom/msc/c/b;->a(Ljava/lang/Boolean;)V

    .line 3519
    iget-object v0, p0, Lcom/osp/app/signin/no;->b:Lcom/msc/c/f;

    if-eqz v0, :cond_4

    .line 3521
    const-string v0, "SOCKET_TIMEOUT_EXCEPTION"

    iget-object v1, p0, Lcom/osp/app/signin/no;->b:Lcom/msc/c/f;

    invoke-virtual {v1}, Lcom/msc/c/f;->b()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    const-string v0, "CONNECT_TIMEOUT_EXCEPTION"

    iget-object v1, p0, Lcom/osp/app/signin/no;->b:Lcom/msc/c/f;

    invoke-virtual {v1}, Lcom/msc/c/f;->b()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 3523
    :cond_0
    iget-object v0, p0, Lcom/osp/app/signin/no;->g:Lcom/osp/app/signin/SmsVerificationActivity;

    const/4 v1, 0x3

    iget-object v2, p0, Lcom/osp/app/signin/no;->g:Lcom/osp/app/signin/SmsVerificationActivity;

    invoke-static {v2}, Lcom/osp/app/signin/SmsVerificationActivity;->g(Lcom/osp/app/signin/SmsVerificationActivity;)Landroid/content/Intent;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/osp/app/signin/SmsVerificationActivity;->a(ILandroid/content/Intent;)V

    .line 3529
    :goto_0
    iget-boolean v0, p0, Lcom/osp/app/signin/no;->q:Z

    if-eqz v0, :cond_1

    .line 3531
    iget-object v0, p0, Lcom/osp/app/signin/no;->g:Lcom/osp/app/signin/SmsVerificationActivity;

    invoke-static {v0}, Lcom/osp/app/signin/SmsVerificationActivity;->Z(Lcom/osp/app/signin/SmsVerificationActivity;)V

    .line 3534
    :cond_1
    iget-object v0, p0, Lcom/osp/app/signin/no;->g:Lcom/osp/app/signin/SmsVerificationActivity;

    invoke-static {v0}, Lcom/osp/app/signin/SmsVerificationActivity;->Y(Lcom/osp/app/signin/SmsVerificationActivity;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 3536
    invoke-static {}, Lcom/osp/app/signin/gv;->a()Lcom/osp/app/signin/gs;

    move-result-object v0

    iget-object v1, p0, Lcom/osp/app/signin/no;->g:Lcom/osp/app/signin/SmsVerificationActivity;

    new-instance v2, Lcom/osp/app/signin/nr;

    invoke-direct {v2, p0}, Lcom/osp/app/signin/nr;-><init>(Lcom/osp/app/signin/no;)V

    invoke-virtual {v0, v1, v2}, Lcom/osp/app/signin/gs;->a(Landroid/content/Context;Lcom/osp/app/signin/gu;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 3554
    :goto_1
    return-void

    .line 3526
    :cond_2
    iget-object v0, p0, Lcom/osp/app/signin/no;->g:Lcom/osp/app/signin/SmsVerificationActivity;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/osp/app/signin/SmsVerificationActivity;->b(I)V

    goto :goto_0

    .line 3549
    :cond_3
    const-string v0, "Fail"

    invoke-direct {p0, v0}, Lcom/osp/app/signin/no;->b(Ljava/lang/String;)V

    goto :goto_1

    .line 3552
    :cond_4
    iget-object v0, p0, Lcom/osp/app/signin/no;->l:Ljava/lang/String;

    invoke-direct {p0, v0}, Lcom/osp/app/signin/no;->b(Ljava/lang/String;)V

    goto :goto_1
.end method

.method public final b(Lcom/msc/b/c;)V
    .locals 7

    .prologue
    const/4 v6, 0x1

    .line 3735
    invoke-super {p0, p1}, Lcom/msc/c/b;->b(Lcom/msc/b/c;)V

    .line 3737
    if-nez p1, :cond_1

    .line 3807
    :cond_0
    :goto_0
    return-void

    .line 3742
    :cond_1
    invoke-virtual {p1}, Lcom/msc/b/c;->a()J

    move-result-wide v0

    .line 3743
    invoke-virtual {p1}, Lcom/msc/b/c;->e()Ljava/lang/String;

    move-result-object v2

    .line 3744
    invoke-virtual {p1}, Lcom/msc/b/c;->f()Ljava/lang/Exception;

    move-result-object v3

    .line 3746
    iget-wide v4, p0, Lcom/osp/app/signin/no;->n:J

    cmp-long v4, v0, v4

    if-nez v4, :cond_4

    .line 3748
    if-eqz v3, :cond_2

    .line 3750
    iget-object v0, p0, Lcom/osp/app/signin/no;->g:Lcom/osp/app/signin/SmsVerificationActivity;

    invoke-virtual {v0, v6}, Lcom/osp/app/signin/SmsVerificationActivity;->b(I)V

    goto :goto_0

    .line 3751
    :cond_2
    if-eqz v2, :cond_0

    .line 3754
    iget-object v0, p0, Lcom/osp/app/signin/no;->b:Lcom/msc/c/f;

    invoke-virtual {v0}, Lcom/msc/c/f;->a()Ljava/lang/String;

    move-result-object v0

    .line 3756
    const-string v1, "SSO_8005"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-ne v1, v6, :cond_3

    .line 3760
    :try_start_0
    new-instance v0, Lcom/osp/security/time/a;

    iget-object v1, p0, Lcom/osp/app/signin/no;->g:Lcom/osp/app/signin/SmsVerificationActivity;

    invoke-virtual {v1}, Lcom/osp/app/signin/SmsVerificationActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/osp/security/time/a;-><init>(Landroid/content/Context;)V

    .line 3761
    invoke-virtual {v0}, Lcom/osp/security/time/a;->b()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 3762
    :catch_0
    move-exception v0

    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_0

    .line 3766
    :cond_3
    const-string v1, "SSO_2204"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-ne v0, v6, :cond_0

    .line 3768
    iput-boolean v6, p0, Lcom/osp/app/signin/no;->q:Z

    goto :goto_0

    .line 3778
    :cond_4
    iget-wide v4, p0, Lcom/osp/app/signin/no;->o:J

    cmp-long v3, v0, v4

    if-nez v3, :cond_5

    .line 3780
    new-instance v0, Lcom/msc/c/f;

    invoke-direct {v0}, Lcom/msc/c/f;-><init>()V

    iput-object v0, p0, Lcom/osp/app/signin/no;->b:Lcom/msc/c/f;

    goto :goto_0

    .line 3782
    :cond_5
    iget-wide v4, p0, Lcom/osp/app/signin/no;->u:J

    cmp-long v0, v0, v4

    if-nez v0, :cond_0

    .line 3784
    if-eqz v2, :cond_0

    .line 3786
    const-string v0, "AUT_1805"

    iget-object v1, p0, Lcom/osp/app/signin/no;->b:Lcom/msc/c/f;

    invoke-virtual {v1}, Lcom/msc/c/f;->a()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_6

    .line 3788
    iget-object v0, p0, Lcom/osp/app/signin/no;->b:Lcom/msc/c/f;

    const-string v1, "AUT_1805"

    invoke-virtual {v0, v1}, Lcom/msc/c/f;->a(Ljava/lang/String;)V

    goto :goto_0

    .line 3789
    :cond_6
    const-string v0, "AUT_1815"

    iget-object v1, p0, Lcom/osp/app/signin/no;->b:Lcom/msc/c/f;

    invoke-virtual {v1}, Lcom/msc/c/f;->a()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_7

    .line 3793
    invoke-static {}, Lcom/osp/app/signin/gv;->a()Lcom/osp/app/signin/gs;

    move-result-object v0

    invoke-virtual {v0}, Lcom/osp/app/signin/gs;->a()V

    goto/16 :goto_0

    .line 3796
    :cond_7
    const-string v0, "AUT_1093"

    iget-object v1, p0, Lcom/osp/app/signin/no;->b:Lcom/msc/c/f;

    invoke-virtual {v1}, Lcom/msc/c/f;->a()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    const-string v0, "AUT_1092"

    iget-object v1, p0, Lcom/osp/app/signin/no;->b:Lcom/msc/c/f;

    invoke-virtual {v1}, Lcom/msc/c/f;->a()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    goto/16 :goto_0
.end method

.method protected final d()V
    .locals 3

    .prologue
    const/4 v2, 0x1

    .line 3409
    invoke-super {p0}, Lcom/msc/c/b;->d()V

    .line 3411
    iget-object v0, p0, Lcom/osp/app/signin/no;->g:Lcom/osp/app/signin/SmsVerificationActivity;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/osp/app/signin/SmsVerificationActivity;->b(I)V

    .line 3413
    iget-object v0, p0, Lcom/osp/app/signin/no;->e:Ljava/lang/Thread;

    invoke-static {v0}, Lcom/osp/app/signin/no;->a(Ljava/lang/Thread;)V

    .line 3414
    iget-object v0, p0, Lcom/osp/app/signin/no;->f:Ljava/lang/Thread;

    invoke-static {v0}, Lcom/osp/app/signin/no;->a(Ljava/lang/Thread;)V

    .line 3415
    iput-boolean v2, p0, Lcom/osp/app/signin/no;->h:Z

    .line 3416
    iput-boolean v2, p0, Lcom/osp/app/signin/no;->i:Z

    .line 3417
    iget-object v0, p0, Lcom/osp/app/signin/no;->g:Lcom/osp/app/signin/SmsVerificationActivity;

    invoke-virtual {v0}, Lcom/osp/app/signin/SmsVerificationActivity;->finish()V

    .line 3418
    return-void
.end method

.method protected final synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 3241
    invoke-virtual {p0}, Lcom/osp/app/signin/no;->a()Ljava/lang/Boolean;

    move-result-object v0

    return-object v0
.end method

.method protected final synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 3241
    check-cast p1, Ljava/lang/Boolean;

    invoke-virtual {p0, p1}, Lcom/osp/app/signin/no;->a(Ljava/lang/Boolean;)V

    return-void
.end method
