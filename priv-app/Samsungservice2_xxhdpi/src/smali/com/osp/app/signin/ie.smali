.class final Lcom/osp/app/signin/ie;
.super Ljava/lang/Object;
.source "SignInView.java"

# interfaces
.implements Landroid/widget/TextView$OnEditorActionListener;


# instance fields
.field final synthetic a:Landroid/widget/EditText;

.field final synthetic b:Landroid/widget/EditText;

.field final synthetic c:Lcom/osp/app/signin/SignInView;


# direct methods
.method constructor <init>(Lcom/osp/app/signin/SignInView;Landroid/widget/EditText;Landroid/widget/EditText;)V
    .locals 0

    .prologue
    .line 2043
    iput-object p1, p0, Lcom/osp/app/signin/ie;->c:Lcom/osp/app/signin/SignInView;

    iput-object p2, p0, Lcom/osp/app/signin/ie;->a:Landroid/widget/EditText;

    iput-object p3, p0, Lcom/osp/app/signin/ie;->b:Landroid/widget/EditText;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onEditorAction(Landroid/widget/TextView;ILandroid/view/KeyEvent;)Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 2048
    packed-switch p2, :pswitch_data_0

    .line 2079
    :goto_0
    return v0

    .line 2052
    :pswitch_0
    iget-object v1, p0, Lcom/osp/app/signin/ie;->a:Landroid/widget/EditText;

    invoke-virtual {v1}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v1

    invoke-interface {v1}, Landroid/text/Editable;->length()I

    move-result v1

    if-nez v1, :cond_0

    .line 2054
    invoke-static {}, Lcom/osp/app/util/ap;->a()Lcom/osp/app/util/ap;

    iget-object v1, p0, Lcom/osp/app/signin/ie;->c:Lcom/osp/app/signin/SignInView;

    const v2, 0x7f090025

    invoke-static {v1, v2, v0}, Lcom/osp/app/util/ap;->a(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    .line 2055
    iget-object v0, p0, Lcom/osp/app/signin/ie;->a:Landroid/widget/EditText;

    invoke-virtual {v0}, Landroid/widget/EditText;->requestFocus()Z

    .line 2075
    :goto_1
    const/4 v0, 0x1

    goto :goto_0

    .line 2056
    :cond_0
    iget-object v1, p0, Lcom/osp/app/signin/ie;->b:Landroid/widget/EditText;

    invoke-virtual {v1}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v1

    invoke-interface {v1}, Landroid/text/Editable;->length()I

    move-result v1

    if-nez v1, :cond_1

    .line 2058
    invoke-static {}, Lcom/osp/app/util/ap;->a()Lcom/osp/app/util/ap;

    iget-object v1, p0, Lcom/osp/app/signin/ie;->c:Lcom/osp/app/signin/SignInView;

    const v2, 0x7f090177

    invoke-static {v1, v2, v0}, Lcom/osp/app/util/ap;->a(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    goto :goto_1

    .line 2067
    :cond_1
    iget-object v1, p0, Lcom/osp/app/signin/ie;->c:Lcom/osp/app/signin/SignInView;

    invoke-static {v1}, Lcom/osp/app/signin/SignInView;->e(Lcom/osp/app/signin/SignInView;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/osp/app/signin/ie;->c:Lcom/osp/app/signin/SignInView;

    iget-object v1, v1, Lcom/osp/app/signin/SignInView;->a:Landroid/app/AlertDialog;

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/osp/app/signin/ie;->c:Lcom/osp/app/signin/SignInView;

    iget-object v1, v1, Lcom/osp/app/signin/SignInView;->a:Landroid/app/AlertDialog;

    invoke-virtual {v1}, Landroid/app/AlertDialog;->isShowing()Z

    move-result v1

    if-eqz v1, :cond_2

    .line 2069
    iget-object v1, p0, Lcom/osp/app/signin/ie;->c:Lcom/osp/app/signin/SignInView;

    iget-object v1, v1, Lcom/osp/app/signin/SignInView;->a:Landroid/app/AlertDialog;

    const/4 v2, -0x1

    invoke-virtual {v1, v2}, Landroid/app/AlertDialog;->getButton(I)Landroid/widget/Button;

    move-result-object v1

    invoke-virtual {v1}, Landroid/widget/Button;->performClick()Z

    .line 2071
    :cond_2
    iget-object v1, p0, Lcom/osp/app/signin/ie;->c:Lcom/osp/app/signin/SignInView;

    invoke-static {v1, v0}, Lcom/osp/app/signin/SignInView;->a(Lcom/osp/app/signin/SignInView;Z)Z

    goto :goto_1

    .line 2048
    nop

    :pswitch_data_0
    .packed-switch 0x6
        :pswitch_0
    .end packed-switch
.end method
