.class final Lcom/osp/app/signin/gp;
.super Lcom/msc/c/b;
.source "PasswordChangeView.java"


# instance fields
.field final synthetic c:Lcom/osp/app/signin/PasswordChangeView;

.field private d:Ljava/lang/String;


# direct methods
.method public constructor <init>(Lcom/osp/app/signin/PasswordChangeView;)V
    .locals 0

    .prologue
    .line 1692
    iput-object p1, p0, Lcom/osp/app/signin/gp;->c:Lcom/osp/app/signin/PasswordChangeView;

    .line 1693
    invoke-direct {p0, p1}, Lcom/msc/c/b;-><init>(Landroid/content/Context;)V

    .line 1697
    return-void
.end method


# virtual methods
.method protected final varargs a()Ljava/lang/Boolean;
    .locals 9

    .prologue
    const/4 v8, 0x0

    .line 1708
    const/4 v3, 0x0

    .line 1710
    iget-object v0, p0, Lcom/osp/app/signin/gp;->c:Lcom/osp/app/signin/PasswordChangeView;

    const v1, 0x7f0c0108

    invoke-virtual {v0, v1}, Lcom/osp/app/signin/PasswordChangeView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/EditText;

    .line 1711
    iget-object v1, p0, Lcom/osp/app/signin/gp;->c:Lcom/osp/app/signin/PasswordChangeView;

    const v2, 0x7f0c0104

    invoke-virtual {v1, v2}, Lcom/osp/app/signin/PasswordChangeView;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/EditText;

    .line 1712
    if-eqz v0, :cond_0

    if-nez v1, :cond_1

    .line 1714
    :cond_0
    invoke-static {v8}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    .line 1859
    :goto_0
    return-object v0

    .line 1717
    :cond_1
    invoke-virtual {v1}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v4

    .line 1718
    invoke-virtual {v0}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v5

    .line 1721
    :try_start_0
    new-instance v2, Lcom/osp/security/identity/d;

    iget-object v0, p0, Lcom/osp/app/signin/gp;->c:Lcom/osp/app/signin/PasswordChangeView;

    invoke-direct {v2, v0}, Lcom/osp/security/identity/d;-><init>(Landroid/content/Context;)V
    :try_end_0
    .catch Lcom/osp/security/identity/IdentityException; {:try_start_0 .. :try_end_0} :catch_0

    .line 1722
    :try_start_1
    const-string v0, "14eev3f64b"

    const-string v3, "109E2830E09DB340924B8ABE0D6290C3"

    invoke-virtual {v2, v0, v3, v4, v5}, Lcom/osp/security/identity/d;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 1723
    const-string v0, "true"

    iput-object v0, p0, Lcom/osp/app/signin/gp;->d:Ljava/lang/String;
    :try_end_1
    .catch Lcom/osp/security/identity/IdentityException; {:try_start_1 .. :try_end_1} :catch_5

    .line 1859
    :goto_1
    invoke-static {v8}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    goto :goto_0

    .line 1724
    :catch_0
    move-exception v0

    move-object v2, v3

    .line 1726
    :goto_2
    new-instance v3, Lcom/msc/c/f;

    invoke-virtual {v0}, Lcom/osp/security/identity/IdentityException;->a()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v0}, Lcom/osp/security/identity/IdentityException;->getMessage()Ljava/lang/String;

    move-result-object v7

    invoke-direct {v3, v6, v7}, Lcom/msc/c/f;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    iput-object v3, p0, Lcom/osp/app/signin/gp;->b:Lcom/msc/c/f;

    .line 1727
    const-string v3, "SSO_2002"

    invoke-virtual {v0}, Lcom/osp/security/identity/IdentityException;->a()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v3, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3

    .line 1731
    iget-object v0, p0, Lcom/osp/app/signin/gp;->c:Lcom/osp/app/signin/PasswordChangeView;

    invoke-static {v0}, Lcom/msc/c/c;->e(Landroid/content/Context;)V

    .line 1735
    iget-object v0, p0, Lcom/osp/app/signin/gp;->c:Lcom/osp/app/signin/PasswordChangeView;

    invoke-static {v0}, Lcom/msc/c/e;->i(Landroid/content/Context;)V

    .line 1856
    :cond_2
    :goto_3
    iget-object v0, p0, Lcom/osp/app/signin/gp;->c:Lcom/osp/app/signin/PasswordChangeView;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/osp/app/signin/PasswordChangeView;->b(I)V

    .line 1857
    const-string v0, "fail"

    iput-object v0, p0, Lcom/osp/app/signin/gp;->d:Ljava/lang/String;

    goto :goto_1

    .line 1737
    :cond_3
    const-string v3, "SSO_2101"

    invoke-virtual {v0}, Lcom/osp/security/identity/IdentityException;->a()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v3, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_2

    .line 1778
    :try_start_2
    new-instance v3, Lcom/osp/security/credential/a;

    iget-object v6, p0, Lcom/osp/app/signin/gp;->c:Lcom/osp/app/signin/PasswordChangeView;

    invoke-direct {v3, v6}, Lcom/osp/security/credential/a;-><init>(Landroid/content/Context;)V

    .line 1779
    const-string v6, "14eev3f64b"

    invoke-virtual {v3, v6}, Lcom/osp/security/credential/a;->c(Ljava/lang/String;)V
    :try_end_2
    .catch Lcom/osp/security/credential/CredentialException; {:try_start_2 .. :try_end_2} :catch_2

    .line 1786
    :goto_4
    :try_start_3
    const-string v3, "14eev3f64b"

    const-string v6, "109E2830E09DB340924B8ABE0D6290C3"

    invoke-virtual {v2, v3, v6}, Lcom/osp/security/identity/d;->a(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_3
    .catch Lcom/osp/security/identity/IdentityException; {:try_start_3 .. :try_end_3} :catch_3

    .line 1848
    :cond_4
    :goto_5
    :try_start_4
    const-string v1, "14eev3f64b"

    const-string v3, "109E2830E09DB340924B8ABE0D6290C3"

    invoke-virtual {v2, v1, v3, v4, v5}, Lcom/osp/security/identity/d;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 1849
    const-string v1, "true"

    iput-object v1, p0, Lcom/osp/app/signin/gp;->d:Ljava/lang/String;
    :try_end_4
    .catch Lcom/osp/security/identity/IdentityException; {:try_start_4 .. :try_end_4} :catch_1

    goto :goto_3

    .line 1850
    :catch_1
    move-exception v1

    invoke-virtual {v1}, Lcom/osp/security/identity/IdentityException;->printStackTrace()V

    .line 1853
    new-instance v1, Lcom/msc/c/f;

    invoke-virtual {v0}, Lcom/osp/security/identity/IdentityException;->a()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0}, Lcom/osp/security/identity/IdentityException;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v2, v0}, Lcom/msc/c/f;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    iput-object v1, p0, Lcom/osp/app/signin/gp;->b:Lcom/msc/c/f;

    goto :goto_3

    .line 1780
    :catch_2
    move-exception v3

    invoke-virtual {v3}, Lcom/osp/security/credential/CredentialException;->printStackTrace()V

    goto :goto_4

    .line 1792
    :catch_3
    move-exception v3

    invoke-virtual {v3}, Lcom/osp/security/identity/IdentityException;->printStackTrace()V

    .line 1795
    const-string v3, "SSO_2101"

    invoke-virtual {v0}, Lcom/osp/security/identity/IdentityException;->a()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v3, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_4

    .line 1797
    new-instance v3, Lcom/osp/security/identity/k;

    invoke-direct {v3}, Lcom/osp/security/identity/k;-><init>()V

    .line 1798
    invoke-virtual {v3}, Lcom/osp/security/identity/k;->b()V

    .line 1799
    invoke-virtual {v3}, Lcom/osp/security/identity/k;->c()V

    .line 1800
    invoke-virtual {v3}, Lcom/osp/security/identity/k;->d()V

    .line 1804
    iget-object v6, p0, Lcom/osp/app/signin/gp;->c:Lcom/osp/app/signin/PasswordChangeView;

    invoke-static {v6}, Lcom/osp/app/util/r;->b(Landroid/content/Context;)Z

    move-result v6

    if-eqz v6, :cond_5

    .line 1806
    invoke-virtual {v3}, Lcom/osp/security/identity/k;->e()V

    .line 1810
    :cond_5
    :try_start_5
    invoke-virtual {v1}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v2, v3, v1}, Lcom/osp/security/identity/d;->a(Lcom/osp/security/identity/k;Ljava/lang/String;)V
    :try_end_5
    .catch Lcom/osp/security/identity/IdentityException; {:try_start_5 .. :try_end_5} :catch_4

    goto :goto_5

    .line 1816
    :catch_4
    move-exception v1

    .line 1818
    invoke-virtual {v1}, Lcom/osp/security/identity/IdentityException;->printStackTrace()V

    .line 1819
    const-string v3, "SSO_2002"

    invoke-virtual {v1}, Lcom/osp/security/identity/IdentityException;->a()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v3, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_6

    const-string v3, "SSO_1000"

    invoke-virtual {v1}, Lcom/osp/security/identity/IdentityException;->a()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v3, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_4

    .line 1823
    :cond_6
    iget-object v3, p0, Lcom/osp/app/signin/gp;->c:Lcom/osp/app/signin/PasswordChangeView;

    invoke-static {v3}, Lcom/msc/c/c;->e(Landroid/content/Context;)V

    .line 1827
    iget-object v3, p0, Lcom/osp/app/signin/gp;->c:Lcom/osp/app/signin/PasswordChangeView;

    invoke-static {v3}, Lcom/msc/c/e;->i(Landroid/content/Context;)V

    .line 1829
    iget-object v3, p0, Lcom/osp/app/signin/gp;->c:Lcom/osp/app/signin/PasswordChangeView;

    invoke-static {v3}, Lcom/osp/app/signin/PasswordChangeView;->b(Lcom/osp/app/signin/PasswordChangeView;)Landroid/os/Handler;

    move-result-object v3

    new-instance v6, Lcom/osp/app/signin/gq;

    invoke-direct {v6, p0, v1}, Lcom/osp/app/signin/gq;-><init>(Lcom/osp/app/signin/gp;Lcom/osp/security/identity/IdentityException;)V

    invoke-virtual {v3, v6}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 1839
    iget-object v1, p0, Lcom/osp/app/signin/gp;->c:Lcom/osp/app/signin/PasswordChangeView;

    invoke-static {v1}, Lcom/osp/app/signin/PasswordChangeView;->c(Lcom/osp/app/signin/PasswordChangeView;)V

    .line 1840
    const-string v1, "fail"

    iput-object v1, p0, Lcom/osp/app/signin/gp;->d:Ljava/lang/String;

    .line 1841
    iget-object v1, p0, Lcom/osp/app/signin/gp;->c:Lcom/osp/app/signin/PasswordChangeView;

    invoke-virtual {v1}, Lcom/osp/app/signin/PasswordChangeView;->finish()V

    goto/16 :goto_5

    .line 1724
    :catch_5
    move-exception v0

    goto/16 :goto_2
.end method

.method protected final a(Ljava/lang/Boolean;)V
    .locals 5

    .prologue
    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 1864
    invoke-super {p0, p1}, Lcom/msc/c/b;->a(Ljava/lang/Boolean;)V

    .line 1865
    iget-object v0, p0, Lcom/osp/app/signin/gp;->b:Lcom/msc/c/f;

    if-eqz v0, :cond_0

    .line 1867
    invoke-virtual {p0, v3}, Lcom/osp/app/signin/gp;->a(Z)V

    .line 1892
    :goto_0
    return-void

    .line 1870
    :cond_0
    iget-object v0, p0, Lcom/osp/app/signin/gp;->d:Ljava/lang/String;

    if-eqz v0, :cond_2

    .line 1872
    iget-object v0, p0, Lcom/osp/app/signin/gp;->d:Ljava/lang/String;

    const-string v1, "true"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1874
    invoke-static {}, Lcom/osp/app/util/ap;->a()Lcom/osp/app/util/ap;

    iget-object v0, p0, Lcom/osp/app/signin/gp;->c:Lcom/osp/app/signin/PasswordChangeView;

    iget-object v1, p0, Lcom/osp/app/signin/gp;->c:Lcom/osp/app/signin/PasswordChangeView;

    const v2, 0x7f090047

    invoke-virtual {v1, v2}, Lcom/osp/app/signin/PasswordChangeView;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1, v3}, Lcom/osp/app/util/ap;->a(Landroid/content/Context;Ljava/lang/String;I)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    .line 1875
    iget-object v0, p0, Lcom/osp/app/signin/gp;->c:Lcom/osp/app/signin/PasswordChangeView;

    const/4 v1, -0x1

    invoke-virtual {v0, v1}, Lcom/osp/app/signin/PasswordChangeView;->b(I)V

    .line 1878
    new-instance v0, Landroid/content/Intent;

    const-string v1, "android.intent.action.SAMSUNGACCOUNT_CHANGED_PASSWORD_COMPLETED"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 1880
    iget-object v1, p0, Lcom/osp/app/signin/gp;->c:Lcom/osp/app/signin/PasswordChangeView;

    invoke-virtual {v1, v0}, Lcom/osp/app/signin/PasswordChangeView;->sendBroadcast(Landroid/content/Intent;)V

    .line 1881
    iget-object v0, p0, Lcom/osp/app/signin/gp;->c:Lcom/osp/app/signin/PasswordChangeView;

    invoke-virtual {v0}, Lcom/osp/app/signin/PasswordChangeView;->finish()V

    goto :goto_0

    .line 1884
    :cond_1
    invoke-static {}, Lcom/osp/app/util/ap;->a()Lcom/osp/app/util/ap;

    iget-object v0, p0, Lcom/osp/app/signin/gp;->c:Lcom/osp/app/signin/PasswordChangeView;

    iget-object v1, p0, Lcom/osp/app/signin/gp;->c:Lcom/osp/app/signin/PasswordChangeView;

    const v2, 0x7f09004a

    invoke-virtual {v1, v2}, Lcom/osp/app/signin/PasswordChangeView;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1, v3}, Lcom/osp/app/util/ap;->a(Landroid/content/Context;Ljava/lang/String;I)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    .line 1885
    iget-object v0, p0, Lcom/osp/app/signin/gp;->c:Lcom/osp/app/signin/PasswordChangeView;

    invoke-virtual {v0, v4}, Lcom/osp/app/signin/PasswordChangeView;->b(I)V

    goto :goto_0

    .line 1889
    :cond_2
    invoke-virtual {p0, v3}, Lcom/osp/app/signin/gp;->a(Z)V

    .line 1890
    iget-object v0, p0, Lcom/osp/app/signin/gp;->c:Lcom/osp/app/signin/PasswordChangeView;

    invoke-virtual {v0, v4}, Lcom/osp/app/signin/PasswordChangeView;->b(I)V

    goto :goto_0
.end method

.method protected final d()V
    .locals 2

    .prologue
    .line 1701
    invoke-super {p0}, Lcom/msc/c/b;->d()V

    .line 1702
    iget-object v0, p0, Lcom/osp/app/signin/gp;->c:Lcom/osp/app/signin/PasswordChangeView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/osp/app/signin/PasswordChangeView;->b(I)V

    .line 1703
    iget-object v0, p0, Lcom/osp/app/signin/gp;->c:Lcom/osp/app/signin/PasswordChangeView;

    invoke-virtual {v0}, Lcom/osp/app/signin/PasswordChangeView;->finish()V

    .line 1704
    return-void
.end method

.method protected final synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 1682
    invoke-virtual {p0}, Lcom/osp/app/signin/gp;->a()Ljava/lang/Boolean;

    move-result-object v0

    return-object v0
.end method

.method protected final synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 1682
    check-cast p1, Ljava/lang/Boolean;

    invoke-virtual {p0, p1}, Lcom/osp/app/signin/gp;->a(Ljava/lang/Boolean;)V

    return-void
.end method
