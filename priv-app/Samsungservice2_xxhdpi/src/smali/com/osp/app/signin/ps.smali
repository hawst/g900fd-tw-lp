.class final Lcom/osp/app/signin/ps;
.super Lcom/msc/c/b;
.source "VersionPreference.java"


# instance fields
.field final synthetic c:Lcom/osp/app/signin/VersionPreference;

.field private d:J

.field private e:J

.field private f:Z

.field private g:Lcom/msc/sa/selfupdate/n;


# direct methods
.method public constructor <init>(Lcom/osp/app/signin/VersionPreference;)V
    .locals 2

    .prologue
    const-wide/16 v0, 0x0

    .line 394
    iput-object p1, p0, Lcom/osp/app/signin/ps;->c:Lcom/osp/app/signin/VersionPreference;

    .line 395
    invoke-direct {p0, p1}, Lcom/msc/c/b;-><init>(Landroid/content/Context;)V

    .line 389
    iput-wide v0, p0, Lcom/osp/app/signin/ps;->d:J

    .line 390
    iput-wide v0, p0, Lcom/osp/app/signin/ps;->e:J

    .line 391
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/osp/app/signin/ps;->f:Z

    .line 397
    return-void
.end method


# virtual methods
.method protected final varargs a()Ljava/lang/Boolean;
    .locals 7

    .prologue
    const/4 v6, 0x0

    .line 403
    invoke-static {v6}, Lcom/msc/c/k;->b(Z)Ljava/lang/String;

    move-result-object v1

    invoke-static {}, Lcom/osp/app/util/ah;->a()Lcom/osp/app/util/ah;

    move-result-object v0

    invoke-virtual {v0}, Lcom/osp/app/util/ah;->g()Z

    move-result v4

    :try_start_0
    invoke-static {}, Lcom/msc/b/i;->a()Lcom/msc/b/i;

    move-result-object v0

    invoke-static {}, Lcom/osp/app/signin/SamsungService;->j()Z

    move-result v2

    invoke-static {}, Lcom/osp/app/signin/SamsungService;->b()Z

    move-result v3

    move-object v5, p0

    invoke-virtual/range {v0 .. v5}, Lcom/msc/b/i;->a(Ljava/lang/String;ZZZLcom/msc/b/h;)Lcom/msc/b/d;

    move-result-object v1

    invoke-static {}, Lcom/osp/app/util/ah;->a()Lcom/osp/app/util/ah;

    move-result-object v0

    invoke-virtual {v0}, Lcom/osp/app/util/ah;->c()Z

    move-result v0

    if-eqz v0, :cond_0

    const-string v0, "SQELOG"

    invoke-static {}, Lcom/osp/app/util/ah;->a()Lcom/osp/app/util/ah;

    move-result-object v2

    invoke-virtual {v2}, Lcom/osp/app/util/ah;->b()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v0, v2}, Lcom/msc/b/d;->a(Ljava/lang/String;Ljava/lang/String;)V

    :cond_0
    invoke-static {}, Lcom/msc/sa/selfupdate/c;->b()Lcom/msc/sa/selfupdate/c;

    iget-object v0, p0, Lcom/osp/app/signin/ps;->c:Lcom/osp/app/signin/VersionPreference;

    invoke-virtual {v0}, Lcom/osp/app/signin/VersionPreference;->getApplication()Landroid/app/Application;

    move-result-object v0

    check-cast v0, Lcom/osp/app/signin/SamsungService;

    const/4 v2, 0x0

    invoke-static {v0, v2}, Lcom/msc/sa/selfupdate/c;->b(Lcom/osp/app/signin/SamsungService;Z)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Lcom/msc/b/d;->a(Ljava/lang/String;)V

    invoke-virtual {v1}, Lcom/msc/b/d;->a()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/osp/app/signin/ps;->d:J

    invoke-static {}, Lcom/msc/b/i;->a()Lcom/msc/b/i;

    move-result-object v0

    iget-wide v2, p0, Lcom/osp/app/signin/ps;->d:J

    sget-object v1, Lcom/msc/b/g;->c:Lcom/msc/b/g;

    invoke-virtual {v0, v2, v3, v1}, Lcom/msc/b/i;->a(JLcom/msc/b/g;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 409
    :goto_0
    invoke-static {v6}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    return-object v0

    .line 403
    :catch_0
    move-exception v0

    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_0
.end method

.method public final declared-synchronized a(Lcom/msc/b/c;)V
    .locals 6

    .prologue
    .line 484
    monitor-enter p0

    :try_start_0
    invoke-super {p0, p1}, Lcom/msc/c/b;->a(Lcom/msc/b/c;)V

    .line 486
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v0, "VP"

    const-string v1, "onRequestSuccess START"

    invoke-static {v0, v1}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 487
    if-nez p1, :cond_0

    .line 489
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/osp/app/signin/ps;->f:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 527
    :goto_0
    monitor-exit p0

    return-void

    .line 493
    :cond_0
    :try_start_1
    invoke-virtual {p1}, Lcom/msc/b/c;->e()Ljava/lang/String;

    move-result-object v1

    .line 494
    invoke-virtual {p1}, Lcom/msc/b/c;->a()J

    move-result-wide v2

    .line 495
    iget-wide v4, p0, Lcom/osp/app/signin/ps;->d:J
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    cmp-long v0, v2, v4

    if-nez v0, :cond_4

    .line 499
    :try_start_2
    invoke-static {}, Lcom/msc/sa/selfupdate/c;->b()Lcom/msc/sa/selfupdate/c;

    move-result-object v2

    iget-object v0, p0, Lcom/osp/app/signin/ps;->c:Lcom/osp/app/signin/VersionPreference;

    invoke-virtual {v0}, Lcom/osp/app/signin/VersionPreference;->getApplication()Landroid/app/Application;

    move-result-object v0

    check-cast v0, Lcom/osp/app/signin/SamsungService;

    invoke-virtual {v2, v0, v1}, Lcom/msc/sa/selfupdate/c;->a(Lcom/osp/app/signin/SamsungService;Ljava/lang/String;)Lcom/msc/sa/selfupdate/n;

    move-result-object v0

    iput-object v0, p0, Lcom/osp/app/signin/ps;->g:Lcom/msc/sa/selfupdate/n;
    :try_end_2
    .catch Lorg/xmlpull/v1/XmlPullParserException; {:try_start_2 .. :try_end_2} :catch_0
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 505
    :goto_1
    :try_start_3
    iget-object v0, p0, Lcom/osp/app/signin/ps;->g:Lcom/msc/sa/selfupdate/n;

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/osp/app/signin/ps;->g:Lcom/msc/sa/selfupdate/n;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/msc/sa/selfupdate/n;->a(Z)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 507
    const/4 v0, 0x1

    invoke-static {v0}, Lcom/msc/c/k;->b(Z)Ljava/lang/String;

    move-result-object v1

    invoke-static {}, Lcom/osp/app/util/ah;->a()Lcom/osp/app/util/ah;

    move-result-object v0

    invoke-virtual {v0}, Lcom/osp/app/util/ah;->g()Z
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    move-result v4

    :try_start_4
    invoke-static {}, Lcom/msc/b/i;->a()Lcom/msc/b/i;

    move-result-object v0

    invoke-static {}, Lcom/osp/app/signin/SamsungService;->j()Z

    move-result v2

    invoke-static {}, Lcom/osp/app/signin/SamsungService;->b()Z

    move-result v3

    move-object v5, p0

    invoke-virtual/range {v0 .. v5}, Lcom/msc/b/i;->a(Ljava/lang/String;ZZZLcom/msc/b/h;)Lcom/msc/b/d;

    move-result-object v1

    invoke-static {}, Lcom/osp/app/util/ah;->a()Lcom/osp/app/util/ah;

    move-result-object v0

    invoke-virtual {v0}, Lcom/osp/app/util/ah;->c()Z

    move-result v0

    if-eqz v0, :cond_1

    const-string v0, "SQELOG"

    invoke-static {}, Lcom/osp/app/util/ah;->a()Lcom/osp/app/util/ah;

    move-result-object v2

    invoke-virtual {v2}, Lcom/osp/app/util/ah;->b()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v0, v2}, Lcom/msc/b/d;->a(Ljava/lang/String;Ljava/lang/String;)V

    :cond_1
    invoke-static {}, Lcom/msc/sa/selfupdate/c;->b()Lcom/msc/sa/selfupdate/c;

    iget-object v0, p0, Lcom/osp/app/signin/ps;->c:Lcom/osp/app/signin/VersionPreference;

    invoke-virtual {v0}, Lcom/osp/app/signin/VersionPreference;->getApplication()Landroid/app/Application;

    move-result-object v0

    check-cast v0, Lcom/osp/app/signin/SamsungService;

    const/4 v2, 0x1

    invoke-static {v0, v2}, Lcom/msc/sa/selfupdate/c;->b(Lcom/osp/app/signin/SamsungService;Z)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Lcom/msc/b/d;->a(Ljava/lang/String;)V

    invoke-virtual {v1}, Lcom/msc/b/d;->a()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/osp/app/signin/ps;->e:J

    invoke-static {}, Lcom/msc/b/i;->a()Lcom/msc/b/i;

    move-result-object v0

    iget-wide v2, p0, Lcom/osp/app/signin/ps;->e:J

    sget-object v1, Lcom/msc/b/g;->c:Lcom/msc/b/g;

    invoke-virtual {v0, v2, v3, v1}, Lcom/msc/b/i;->a(JLcom/msc/b/g;)V
    :try_end_4
    .catch Ljava/lang/Exception; {:try_start_4 .. :try_end_4} :catch_1
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    .line 526
    :cond_2
    :goto_2
    :try_start_5
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v0, "VP"

    const-string v1, "onRequestSuccess END"

    invoke-static {v0, v1}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    goto/16 :goto_0

    .line 484
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 500
    :catch_0
    move-exception v0

    :try_start_6
    invoke-virtual {v0}, Lorg/xmlpull/v1/XmlPullParserException;->printStackTrace()V

    goto :goto_1

    .line 507
    :catch_1
    move-exception v0

    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_2

    .line 510
    :cond_3
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/osp/app/signin/ps;->f:Z

    goto :goto_2

    .line 513
    :cond_4
    iget-wide v4, p0, Lcom/osp/app/signin/ps;->e:J
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_0

    cmp-long v0, v2, v4

    if-nez v0, :cond_2

    .line 517
    :try_start_7
    invoke-static {}, Lcom/msc/sa/selfupdate/c;->b()Lcom/msc/sa/selfupdate/c;

    move-result-object v2

    iget-object v0, p0, Lcom/osp/app/signin/ps;->c:Lcom/osp/app/signin/VersionPreference;

    invoke-virtual {v0}, Lcom/osp/app/signin/VersionPreference;->getApplication()Landroid/app/Application;

    move-result-object v0

    check-cast v0, Lcom/osp/app/signin/SamsungService;

    invoke-virtual {v2, v0, v1}, Lcom/msc/sa/selfupdate/c;->a(Lcom/osp/app/signin/SamsungService;Ljava/lang/String;)Lcom/msc/sa/selfupdate/n;

    move-result-object v0

    iput-object v0, p0, Lcom/osp/app/signin/ps;->g:Lcom/msc/sa/selfupdate/n;
    :try_end_7
    .catch Lorg/xmlpull/v1/XmlPullParserException; {:try_start_7 .. :try_end_7} :catch_2
    .catchall {:try_start_7 .. :try_end_7} :catchall_0

    .line 523
    :goto_3
    const/4 v0, 0x1

    :try_start_8
    iput-boolean v0, p0, Lcom/osp/app/signin/ps;->f:Z

    goto :goto_2

    .line 519
    :catch_2
    move-exception v0

    invoke-virtual {v0}, Lorg/xmlpull/v1/XmlPullParserException;->printStackTrace()V
    :try_end_8
    .catchall {:try_start_8 .. :try_end_8} :catchall_0

    goto :goto_3
.end method

.method protected final a(Ljava/lang/Boolean;)V
    .locals 4

    .prologue
    const/4 v2, 0x1

    .line 459
    invoke-super {p0, p1}, Lcom/msc/c/b;->a(Ljava/lang/Boolean;)V

    .line 460
    iget-boolean v0, p0, Lcom/osp/app/signin/ps;->f:Z

    if-eqz v0, :cond_0

    .line 462
    iget-object v0, p0, Lcom/osp/app/signin/ps;->g:Lcom/msc/sa/selfupdate/n;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/osp/app/signin/ps;->g:Lcom/msc/sa/selfupdate/n;

    invoke-virtual {v0, v2}, Lcom/msc/sa/selfupdate/n;->a(Z)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 466
    invoke-static {}, Lcom/msc/sa/selfupdate/c;->b()Lcom/msc/sa/selfupdate/c;

    move-result-object v0

    invoke-virtual {v0}, Lcom/msc/sa/selfupdate/c;->a()V

    .line 467
    invoke-static {}, Lcom/msc/sa/selfupdate/c;->b()Lcom/msc/sa/selfupdate/c;

    move-result-object v1

    iget-object v0, p0, Lcom/osp/app/signin/ps;->c:Lcom/osp/app/signin/VersionPreference;

    invoke-virtual {v0}, Lcom/osp/app/signin/VersionPreference;->getApplication()Landroid/app/Application;

    move-result-object v0

    check-cast v0, Lcom/osp/app/signin/SamsungService;

    invoke-virtual {v1, v0, v2}, Lcom/msc/sa/selfupdate/c;->a(Lcom/osp/app/signin/SamsungService;Z)V

    .line 480
    :cond_0
    :goto_0
    return-void

    .line 473
    :cond_1
    new-instance v0, Landroid/app/AlertDialog$Builder;

    iget-object v1, p0, Lcom/osp/app/signin/ps;->c:Lcom/osp/app/signin/VersionPreference;

    invoke-direct {v0, v1}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    .line 474
    const v1, 0x7f090115

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    iget-object v2, p0, Lcom/osp/app/signin/ps;->c:Lcom/osp/app/signin/VersionPreference;

    const v3, 0x7f090155

    invoke-virtual {v2, v3}, Lcom/osp/app/signin/VersionPreference;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/app/AlertDialog$Builder;->setMessage(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    const v2, 0x7f090042

    const/4 v3, 0x0

    invoke-virtual {v1, v2, v3}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 476
    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/AlertDialog;->show()V

    goto :goto_0
.end method

.method public final declared-synchronized b(Lcom/msc/b/c;)V
    .locals 1

    .prologue
    .line 531
    monitor-enter p0

    :try_start_0
    invoke-super {p0, p1}, Lcom/msc/c/b;->b(Lcom/msc/b/c;)V

    .line 532
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/osp/app/signin/ps;->f:Z

    .line 533
    new-instance v0, Lcom/msc/c/f;

    invoke-direct {v0}, Lcom/msc/c/f;-><init>()V

    iput-object v0, p0, Lcom/osp/app/signin/ps;->b:Lcom/msc/c/f;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 534
    monitor-exit p0

    return-void

    .line 531
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method protected final d()V
    .locals 0

    .prologue
    .line 538
    invoke-super {p0}, Lcom/msc/c/b;->d()V

    .line 539
    return-void
.end method

.method protected final synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 387
    invoke-virtual {p0}, Lcom/osp/app/signin/ps;->a()Ljava/lang/Boolean;

    move-result-object v0

    return-object v0
.end method

.method protected final synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 387
    check-cast p1, Ljava/lang/Boolean;

    invoke-virtual {p0, p1}, Lcom/osp/app/signin/ps;->a(Ljava/lang/Boolean;)V

    return-void
.end method
