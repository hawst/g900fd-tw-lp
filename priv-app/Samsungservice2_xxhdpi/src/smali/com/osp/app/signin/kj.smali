.class final Lcom/osp/app/signin/kj;
.super Ljava/lang/Object;
.source "SignUpView.java"

# interfaces
.implements Landroid/text/TextWatcher;


# instance fields
.field final synthetic a:Lcom/osp/app/signin/SignUpView;


# direct methods
.method constructor <init>(Lcom/osp/app/signin/SignUpView;)V
    .locals 0

    .prologue
    .line 1870
    iput-object p1, p0, Lcom/osp/app/signin/kj;->a:Lcom/osp/app/signin/SignUpView;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final afterTextChanged(Landroid/text/Editable;)V
    .locals 0

    .prologue
    .line 1874
    return-void
.end method

.method public final beforeTextChanged(Ljava/lang/CharSequence;III)V
    .locals 0

    .prologue
    .line 1878
    return-void
.end method

.method public final onTextChanged(Ljava/lang/CharSequence;III)V
    .locals 4

    .prologue
    .line 1882
    invoke-interface {p1}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v1, "@yahoo.co.kr"

    invoke-virtual {v0, v1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1885
    new-instance v0, Lcom/osp/app/signin/kk;

    invoke-direct {v0, p0}, Lcom/osp/app/signin/kk;-><init>(Lcom/osp/app/signin/kj;)V

    .line 1896
    new-instance v1, Landroid/app/AlertDialog$Builder;

    iget-object v2, p0, Lcom/osp/app/signin/kj;->a:Lcom/osp/app/signin/SignUpView;

    invoke-direct {v1, v2}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    iget-object v2, p0, Lcom/osp/app/signin/kj;->a:Lcom/osp/app/signin/SignUpView;

    const v3, 0x7f0900ec

    invoke-virtual {v2, v3}, Lcom/osp/app/signin/SignUpView;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/app/AlertDialog$Builder;->setTitle(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    iget-object v2, p0, Lcom/osp/app/signin/kj;->a:Lcom/osp/app/signin/SignUpView;

    const v3, 0x7f090187

    invoke-virtual {v2, v3}, Lcom/osp/app/signin/SignUpView;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/app/AlertDialog$Builder;->setMessage(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    const v2, 0x7f090042

    invoke-virtual {v1, v2, v0}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    .line 1902
    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->show()Landroid/app/AlertDialog;

    .line 1908
    :goto_0
    return-void

    .line 1906
    :cond_0
    iget-object v0, p0, Lcom/osp/app/signin/kj;->a:Lcom/osp/app/signin/SignUpView;

    invoke-virtual {v0}, Lcom/osp/app/signin/SignUpView;->a()V

    goto :goto_0
.end method
