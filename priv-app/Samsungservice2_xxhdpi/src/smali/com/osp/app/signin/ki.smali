.class final Lcom/osp/app/signin/ki;
.super Ljava/lang/Object;
.source "SignUpView.java"

# interfaces
.implements Landroid/text/TextWatcher;


# instance fields
.field final synthetic a:Lcom/osp/app/signin/SignUpView;


# direct methods
.method constructor <init>(Lcom/osp/app/signin/SignUpView;)V
    .locals 0

    .prologue
    .line 1795
    iput-object p1, p0, Lcom/osp/app/signin/ki;->a:Lcom/osp/app/signin/SignUpView;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final afterTextChanged(Landroid/text/Editable;)V
    .locals 0

    .prologue
    .line 1799
    return-void
.end method

.method public final beforeTextChanged(Ljava/lang/CharSequence;III)V
    .locals 0

    .prologue
    .line 1803
    return-void
.end method

.method public final onTextChanged(Ljava/lang/CharSequence;III)V
    .locals 7

    .prologue
    const/4 v6, 0x0

    const/16 v5, 0xf

    .line 1807
    iget-object v0, p0, Lcom/osp/app/signin/ki;->a:Lcom/osp/app/signin/SignUpView;

    invoke-virtual {v0}, Lcom/osp/app/signin/SignUpView;->a()V

    .line 1809
    iget-object v0, p0, Lcom/osp/app/signin/ki;->a:Lcom/osp/app/signin/SignUpView;

    invoke-static {v0}, Lcom/osp/app/signin/SignUpView;->n(Lcom/osp/app/signin/SignUpView;)Landroid/widget/EditText;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-interface {v0}, Landroid/text/Editable;->length()I

    move-result v0

    const/16 v1, 0x10

    if-lt v0, v1, :cond_1

    .line 1823
    iget-object v0, p0, Lcom/osp/app/signin/ki;->a:Lcom/osp/app/signin/SignUpView;

    const v1, 0x7f0900c8

    const/4 v2, 0x2

    new-array v2, v2, [Ljava/lang/Object;

    const/16 v3, 0x8

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v2, v6

    const/4 v3, 0x1

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-virtual {v0, v1, v2}, Lcom/osp/app/signin/SignUpView;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 1845
    iget-object v1, p0, Lcom/osp/app/signin/ki;->a:Lcom/osp/app/signin/SignUpView;

    invoke-static {v1}, Lcom/osp/app/signin/SignUpView;->o(Lcom/osp/app/signin/SignUpView;)Ljava/lang/String;

    move-result-object v1

    .line 1846
    if-eqz v1, :cond_0

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v2

    if-lt v2, v5, :cond_0

    .line 1848
    invoke-virtual {v1, v6, v5}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v1

    .line 1849
    iget-object v2, p0, Lcom/osp/app/signin/ki;->a:Lcom/osp/app/signin/SignUpView;

    invoke-static {v2}, Lcom/osp/app/signin/SignUpView;->n(Lcom/osp/app/signin/SignUpView;)Landroid/widget/EditText;

    move-result-object v2

    invoke-virtual {v2, v1}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    .line 1850
    iget-object v2, p0, Lcom/osp/app/signin/ki;->a:Lcom/osp/app/signin/SignUpView;

    invoke-static {v2}, Lcom/osp/app/signin/SignUpView;->n(Lcom/osp/app/signin/SignUpView;)Landroid/widget/EditText;

    move-result-object v2

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v1

    invoke-virtual {v2, v1}, Landroid/widget/EditText;->setSelection(I)V

    .line 1855
    :cond_0
    new-instance v1, Landroid/app/AlertDialog$Builder;

    iget-object v2, p0, Lcom/osp/app/signin/ki;->a:Lcom/osp/app/signin/SignUpView;

    invoke-direct {v1, v2}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    iget-object v2, p0, Lcom/osp/app/signin/ki;->a:Lcom/osp/app/signin/SignUpView;

    const v3, 0x7f0900ec

    invoke-virtual {v2, v3}, Lcom/osp/app/signin/SignUpView;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/app/AlertDialog$Builder;->setTitle(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/app/AlertDialog$Builder;->setMessage(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    const v1, 0x7f090042

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    .line 1861
    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->show()Landroid/app/AlertDialog;

    .line 1867
    :goto_0
    return-void

    .line 1865
    :cond_1
    iget-object v0, p0, Lcom/osp/app/signin/ki;->a:Lcom/osp/app/signin/SignUpView;

    iget-object v1, p0, Lcom/osp/app/signin/ki;->a:Lcom/osp/app/signin/SignUpView;

    invoke-static {v1}, Lcom/osp/app/signin/SignUpView;->n(Lcom/osp/app/signin/SignUpView;)Landroid/widget/EditText;

    move-result-object v1

    invoke-virtual {v1}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/osp/app/signin/SignUpView;->a(Lcom/osp/app/signin/SignUpView;Ljava/lang/String;)Ljava/lang/String;

    goto :goto_0
.end method
