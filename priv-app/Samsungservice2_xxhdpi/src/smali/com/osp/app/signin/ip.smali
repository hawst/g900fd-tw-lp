.class final Lcom/osp/app/signin/ip;
.super Ljava/lang/Object;
.source "SignInView.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field final synthetic a:Lcom/osp/app/signin/SignInView;


# direct methods
.method constructor <init>(Lcom/osp/app/signin/SignInView;)V
    .locals 0

    .prologue
    .line 2574
    iput-object p1, p0, Lcom/osp/app/signin/ip;->a:Lcom/osp/app/signin/SignInView;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onClick(Landroid/view/View;)V
    .locals 5

    .prologue
    const/4 v3, 0x1

    .line 2579
    iget-object v0, p0, Lcom/osp/app/signin/ip;->a:Lcom/osp/app/signin/SignInView;

    invoke-virtual {v0}, Lcom/osp/app/signin/SignInView;->d()V

    .line 2581
    iget-object v0, p0, Lcom/osp/app/signin/ip;->a:Lcom/osp/app/signin/SignInView;

    invoke-static {v0}, Lcom/osp/app/signin/SignInView;->t(Lcom/osp/app/signin/SignInView;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2614
    :goto_0
    return-void

    .line 2585
    :cond_0
    iget-object v0, p0, Lcom/osp/app/signin/ip;->a:Lcom/osp/app/signin/SignInView;

    invoke-static {v0, v3}, Lcom/osp/app/signin/SignInView;->a(Lcom/osp/app/signin/SignInView;Z)Z

    .line 2587
    invoke-static {}, Lcom/osp/app/signin/SamsungService;->a()Z

    move-result v0

    if-nez v0, :cond_1

    .line 2589
    iget-object v0, p0, Lcom/osp/app/signin/ip;->a:Lcom/osp/app/signin/SignInView;

    invoke-virtual {v0}, Lcom/osp/app/signin/SignInView;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    iget-object v1, p0, Lcom/osp/app/signin/ip;->a:Lcom/osp/app/signin/SignInView;

    invoke-static {v1}, Lcom/osp/app/signin/SignInView;->v(Lcom/osp/app/signin/SignInView;)Z

    move-result v1

    invoke-static {v0, v1}, Lcom/msc/c/g;->a(Landroid/content/Context;Z)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    .line 2595
    new-instance v1, Landroid/content/Intent;

    const-string v2, "android.intent.action.VIEW"

    invoke-direct {v1, v2, v0}, Landroid/content/Intent;-><init>(Ljava/lang/String;Landroid/net/Uri;)V

    .line 2596
    const-string v0, "com.android.browser.application_id"

    iget-object v2, p0, Lcom/osp/app/signin/ip;->a:Lcom/osp/app/signin/SignInView;

    invoke-virtual {v2}, Lcom/osp/app/signin/SignInView;->getPackageName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v0, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 2597
    iget-object v0, p0, Lcom/osp/app/signin/ip;->a:Lcom/osp/app/signin/SignInView;

    invoke-static {v1, v0}, Lcom/osp/app/util/ad;->a(Landroid/content/Intent;Landroid/content/Context;)Z

    move-result v0

    if-ne v0, v3, :cond_1

    .line 2599
    iget-object v0, p0, Lcom/osp/app/signin/ip;->a:Lcom/osp/app/signin/SignInView;

    invoke-virtual {v0, v1}, Lcom/osp/app/signin/SignInView;->startActivity(Landroid/content/Intent;)V

    .line 2609
    :goto_1
    new-instance v0, Lcom/osp/app/bigdatalog/c;

    iget-object v1, p0, Lcom/osp/app/signin/ip;->a:Lcom/osp/app/signin/SignInView;

    invoke-virtual {v1}, Lcom/osp/app/signin/SignInView;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    const/4 v2, 0x4

    const/4 v3, 0x5

    iget-object v4, p0, Lcom/osp/app/signin/ip;->a:Lcom/osp/app/signin/SignInView;

    invoke-static {v4}, Lcom/osp/app/signin/SignInView;->c(Lcom/osp/app/signin/SignInView;)Landroid/content/Intent;

    move-result-object v4

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/osp/app/bigdatalog/c;-><init>(Landroid/content/Context;IILandroid/content/Intent;)V

    .line 2611
    invoke-static {}, Lcom/osp/app/bigdatalog/e;->a()Lcom/osp/app/bigdatalog/e;

    move-result-object v1

    iget-object v2, p0, Lcom/osp/app/signin/ip;->a:Lcom/osp/app/signin/SignInView;

    invoke-virtual {v2}, Lcom/osp/app/signin/SignInView;->getApplicationContext()Landroid/content/Context;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/osp/app/bigdatalog/e;->a(Landroid/content/Context;)V

    .line 2612
    invoke-static {}, Lcom/osp/app/bigdatalog/e;->a()Lcom/osp/app/bigdatalog/e;

    move-result-object v1

    iget-object v2, p0, Lcom/osp/app/signin/ip;->a:Lcom/osp/app/signin/SignInView;

    invoke-virtual {v2}, Lcom/osp/app/signin/SignInView;->getApplicationContext()Landroid/content/Context;

    move-result-object v2

    invoke-virtual {v1, v2, v0}, Lcom/osp/app/bigdatalog/e;->a(Landroid/content/Context;Lcom/osp/app/bigdatalog/c;)V

    goto :goto_0

    .line 2602
    :cond_1
    iget-object v0, p0, Lcom/osp/app/signin/ip;->a:Lcom/osp/app/signin/SignInView;

    invoke-static {v0}, Lcom/osp/app/signin/SignInView;->h(Lcom/osp/app/signin/SignInView;)V

    goto :goto_1
.end method
