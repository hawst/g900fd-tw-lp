.class final Lcom/osp/app/signin/hp;
.super Ljava/lang/Object;
.source "SelectCountryView.java"

# interfaces
.implements Landroid/text/TextWatcher;


# instance fields
.field final synthetic a:[Ljava/lang/String;

.field final synthetic b:Lcom/osp/app/signin/hl;


# direct methods
.method constructor <init>(Lcom/osp/app/signin/hl;[Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 1374
    iput-object p1, p0, Lcom/osp/app/signin/hp;->b:Lcom/osp/app/signin/hl;

    iput-object p2, p0, Lcom/osp/app/signin/hp;->a:[Ljava/lang/String;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final afterTextChanged(Landroid/text/Editable;)V
    .locals 0

    .prologue
    .line 1380
    return-void
.end method

.method public final beforeTextChanged(Ljava/lang/CharSequence;III)V
    .locals 2

    .prologue
    .line 1384
    if-nez p4, :cond_0

    .line 1387
    iget-object v0, p0, Lcom/osp/app/signin/hp;->b:Lcom/osp/app/signin/hl;

    iget-object v0, v0, Lcom/osp/app/signin/hl;->d:Lcom/osp/app/signin/SelectCountryView;

    invoke-static {v0}, Lcom/osp/app/signin/SelectCountryView;->w(Lcom/osp/app/signin/SelectCountryView;)Landroid/widget/ImageButton;

    move-result-object v0

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setVisibility(I)V

    .line 1389
    :cond_0
    return-void
.end method

.method public final onTextChanged(Ljava/lang/CharSequence;III)V
    .locals 6

    .prologue
    const/4 v0, 0x0

    .line 1394
    iget-object v1, p0, Lcom/osp/app/signin/hp;->b:Lcom/osp/app/signin/hl;

    iget-object v1, v1, Lcom/osp/app/signin/hl;->d:Lcom/osp/app/signin/SelectCountryView;

    iget-object v2, p0, Lcom/osp/app/signin/hp;->b:Lcom/osp/app/signin/hl;

    iget-object v2, v2, Lcom/osp/app/signin/hl;->d:Lcom/osp/app/signin/SelectCountryView;

    invoke-static {v2}, Lcom/osp/app/signin/SelectCountryView;->k(Lcom/osp/app/signin/SelectCountryView;)Landroid/widget/EditText;

    move-result-object v2

    invoke-virtual {v2}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v2

    invoke-interface {v2}, Landroid/text/Editable;->length()I

    move-result v2

    iput v2, v1, Lcom/osp/app/signin/SelectCountryView;->a:I

    .line 1396
    iget-object v1, p0, Lcom/osp/app/signin/hp;->b:Lcom/osp/app/signin/hl;

    iget-object v1, v1, Lcom/osp/app/signin/hl;->d:Lcom/osp/app/signin/SelectCountryView;

    iget v1, v1, Lcom/osp/app/signin/SelectCountryView;->a:I

    if-lez v1, :cond_2

    .line 1399
    iget-object v1, p0, Lcom/osp/app/signin/hp;->b:Lcom/osp/app/signin/hl;

    iget-object v1, v1, Lcom/osp/app/signin/hl;->d:Lcom/osp/app/signin/SelectCountryView;

    invoke-static {v1}, Lcom/osp/app/signin/SelectCountryView;->w(Lcom/osp/app/signin/SelectCountryView;)Landroid/widget/ImageButton;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/widget/ImageButton;->setVisibility(I)V

    .line 1406
    :goto_0
    iget-object v1, p0, Lcom/osp/app/signin/hp;->b:Lcom/osp/app/signin/hl;

    iget-object v1, v1, Lcom/osp/app/signin/hl;->d:Lcom/osp/app/signin/SelectCountryView;

    invoke-static {v1}, Lcom/osp/app/signin/SelectCountryView;->x(Lcom/osp/app/signin/SelectCountryView;)Ljava/util/ArrayList;

    move-result-object v1

    invoke-virtual {v1}, Ljava/util/ArrayList;->clear()V

    .line 1409
    :goto_1
    iget-object v1, p0, Lcom/osp/app/signin/hp;->a:[Ljava/lang/String;

    array-length v1, v1

    if-ge v0, v1, :cond_3

    .line 1411
    iget-object v1, p0, Lcom/osp/app/signin/hp;->b:Lcom/osp/app/signin/hl;

    iget-object v1, v1, Lcom/osp/app/signin/hl;->d:Lcom/osp/app/signin/SelectCountryView;

    iget v1, v1, Lcom/osp/app/signin/SelectCountryView;->a:I

    iget-object v2, p0, Lcom/osp/app/signin/hp;->a:[Ljava/lang/String;

    aget-object v2, v2, v0

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v2

    if-gt v1, v2, :cond_1

    .line 1414
    iget-object v1, p0, Lcom/osp/app/signin/hp;->a:[Ljava/lang/String;

    aget-object v1, v1, v0

    const-string v2, "("

    invoke-virtual {v1, v2}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v1

    .line 1415
    add-int/lit8 v1, v1, 0x1

    iget-object v2, p0, Lcom/osp/app/signin/hp;->b:Lcom/osp/app/signin/hl;

    iget-object v2, v2, Lcom/osp/app/signin/hl;->d:Lcom/osp/app/signin/SelectCountryView;

    iget v2, v2, Lcom/osp/app/signin/SelectCountryView;->a:I

    add-int/2addr v1, v2

    .line 1416
    iget-object v2, p0, Lcom/osp/app/signin/hp;->a:[Ljava/lang/String;

    aget-object v2, v2, v0

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v2

    if-le v1, v2, :cond_0

    .line 1418
    iget-object v1, p0, Lcom/osp/app/signin/hp;->a:[Ljava/lang/String;

    aget-object v1, v1, v0

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    .line 1420
    :cond_0
    iget-object v1, p0, Lcom/osp/app/signin/hp;->a:[Ljava/lang/String;

    aget-object v1, v1, v0

    sget-object v2, Ljava/util/Locale;->ENGLISH:Ljava/util/Locale;

    invoke-virtual {v1, v2}, Ljava/lang/String;->toLowerCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/osp/app/signin/hp;->b:Lcom/osp/app/signin/hl;

    iget-object v2, v2, Lcom/osp/app/signin/hl;->d:Lcom/osp/app/signin/SelectCountryView;

    invoke-static {v2}, Lcom/osp/app/signin/SelectCountryView;->k(Lcom/osp/app/signin/SelectCountryView;)Landroid/widget/EditText;

    move-result-object v2

    invoke-virtual {v2}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v2

    sget-object v3, Ljava/util/Locale;->ENGLISH:Ljava/util/Locale;

    invoke-virtual {v2, v3}, Ljava/lang/String;->toLowerCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 1426
    iget-object v1, p0, Lcom/osp/app/signin/hp;->b:Lcom/osp/app/signin/hl;

    iget-object v1, v1, Lcom/osp/app/signin/hl;->d:Lcom/osp/app/signin/SelectCountryView;

    invoke-static {v1}, Lcom/osp/app/signin/SelectCountryView;->x(Lcom/osp/app/signin/SelectCountryView;)Ljava/util/ArrayList;

    move-result-object v1

    iget-object v2, p0, Lcom/osp/app/signin/hp;->a:[Ljava/lang/String;

    aget-object v2, v2, v0

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1409
    :cond_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 1403
    :cond_2
    iget-object v1, p0, Lcom/osp/app/signin/hp;->b:Lcom/osp/app/signin/hl;

    iget-object v1, v1, Lcom/osp/app/signin/hl;->d:Lcom/osp/app/signin/SelectCountryView;

    invoke-static {v1}, Lcom/osp/app/signin/SelectCountryView;->w(Lcom/osp/app/signin/SelectCountryView;)Landroid/widget/ImageButton;

    move-result-object v1

    const/16 v2, 0x8

    invoke-virtual {v1, v2}, Landroid/widget/ImageButton;->setVisibility(I)V

    goto/16 :goto_0

    .line 1445
    :cond_3
    iget-object v0, p0, Lcom/osp/app/signin/hp;->b:Lcom/osp/app/signin/hl;

    iget-object v0, v0, Lcom/osp/app/signin/hl;->d:Lcom/osp/app/signin/SelectCountryView;

    invoke-static {v0}, Lcom/osp/app/signin/SelectCountryView;->x(Lcom/osp/app/signin/SelectCountryView;)Ljava/util/ArrayList;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    new-array v0, v0, [Ljava/lang/String;

    .line 1446
    iget-object v1, p0, Lcom/osp/app/signin/hp;->b:Lcom/osp/app/signin/hl;

    iget-object v1, v1, Lcom/osp/app/signin/hl;->d:Lcom/osp/app/signin/SelectCountryView;

    invoke-static {v1}, Lcom/osp/app/signin/SelectCountryView;->x(Lcom/osp/app/signin/SelectCountryView;)Ljava/util/ArrayList;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Ljava/lang/String;

    .line 1448
    iget-object v1, p0, Lcom/osp/app/signin/hp;->b:Lcom/osp/app/signin/hl;

    iget-object v1, v1, Lcom/osp/app/signin/hl;->d:Lcom/osp/app/signin/SelectCountryView;

    invoke-static {v1}, Lcom/osp/app/signin/SelectCountryView;->k(Lcom/osp/app/signin/SelectCountryView;)Landroid/widget/EditText;

    move-result-object v1

    invoke-virtual {v1}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v1

    invoke-interface {v1}, Landroid/text/Editable;->length()I

    move-result v1

    if-nez v1, :cond_4

    .line 1450
    iget-object v1, p0, Lcom/osp/app/signin/hp;->b:Lcom/osp/app/signin/hl;

    iget-object v1, v1, Lcom/osp/app/signin/hl;->d:Lcom/osp/app/signin/SelectCountryView;

    invoke-static {v1, v0}, Lcom/osp/app/signin/SelectCountryView;->a(Lcom/osp/app/signin/SelectCountryView;[Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v1

    .line 1451
    iget-object v2, p0, Lcom/osp/app/signin/hp;->b:Lcom/osp/app/signin/hl;

    iget-object v2, v2, Lcom/osp/app/signin/hl;->d:Lcom/osp/app/signin/SelectCountryView;

    array-length v0, v0

    invoke-static {v2, v0}, Lcom/osp/app/signin/SelectCountryView;->b(Lcom/osp/app/signin/SelectCountryView;I)I

    move-object v0, v1

    .line 1458
    :cond_4
    iget-object v1, p0, Lcom/osp/app/signin/hp;->b:Lcom/osp/app/signin/hl;

    iget-object v1, v1, Lcom/osp/app/signin/hl;->d:Lcom/osp/app/signin/SelectCountryView;

    new-instance v2, Lcom/osp/app/signin/ej;

    iget-object v3, p0, Lcom/osp/app/signin/hp;->b:Lcom/osp/app/signin/hl;

    iget-object v3, v3, Lcom/osp/app/signin/hl;->d:Lcom/osp/app/signin/SelectCountryView;

    iget-object v4, p0, Lcom/osp/app/signin/hp;->b:Lcom/osp/app/signin/hl;

    iget-object v4, v4, Lcom/osp/app/signin/hl;->d:Lcom/osp/app/signin/SelectCountryView;

    invoke-static {v4}, Lcom/osp/app/signin/SelectCountryView;->k(Lcom/osp/app/signin/SelectCountryView;)Landroid/widget/EditText;

    move-result-object v4

    invoke-virtual {v4}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v4

    iget-object v5, p0, Lcom/osp/app/signin/hp;->b:Lcom/osp/app/signin/hl;

    iget-object v5, v5, Lcom/osp/app/signin/hl;->d:Lcom/osp/app/signin/SelectCountryView;

    invoke-static {v5}, Lcom/osp/app/signin/SelectCountryView;->y(Lcom/osp/app/signin/SelectCountryView;)Z

    move-result v5

    invoke-direct {v2, v3, v0, v4, v5}, Lcom/osp/app/signin/ej;-><init>(Landroid/app/Activity;[Ljava/lang/String;Ljava/lang/String;Z)V

    invoke-static {v1, v2}, Lcom/osp/app/signin/SelectCountryView;->a(Lcom/osp/app/signin/SelectCountryView;Lcom/osp/app/signin/ej;)Lcom/osp/app/signin/ej;

    .line 1459
    iget-object v1, p0, Lcom/osp/app/signin/hp;->b:Lcom/osp/app/signin/hl;

    iget-object v1, v1, Lcom/osp/app/signin/hl;->d:Lcom/osp/app/signin/SelectCountryView;

    invoke-static {v1}, Lcom/osp/app/signin/SelectCountryView;->p(Lcom/osp/app/signin/SelectCountryView;)Landroid/widget/ListView;

    move-result-object v1

    iget-object v2, p0, Lcom/osp/app/signin/hp;->b:Lcom/osp/app/signin/hl;

    iget-object v2, v2, Lcom/osp/app/signin/hl;->d:Lcom/osp/app/signin/SelectCountryView;

    invoke-static {v2}, Lcom/osp/app/signin/SelectCountryView;->o(Lcom/osp/app/signin/SelectCountryView;)Lcom/osp/app/signin/ej;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/ListView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 1460
    iget-object v1, p0, Lcom/osp/app/signin/hp;->b:Lcom/osp/app/signin/hl;

    invoke-static {v1, v0}, Lcom/osp/app/signin/hl;->a(Lcom/osp/app/signin/hl;[Ljava/lang/String;)[Ljava/lang/String;

    .line 1461
    return-void
.end method
