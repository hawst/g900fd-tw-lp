.class final Lcom/osp/app/signin/mx;
.super Ljava/lang/Object;
.source "SmsVerificationActivity.java"

# interfaces
.implements Landroid/content/DialogInterface$OnClickListener;


# instance fields
.field final synthetic a:Lcom/osp/app/signin/SmsVerificationActivity;


# direct methods
.method constructor <init>(Lcom/osp/app/signin/SmsVerificationActivity;)V
    .locals 0

    .prologue
    .line 4382
    iput-object p1, p0, Lcom/osp/app/signin/mx;->a:Lcom/osp/app/signin/SmsVerificationActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onClick(Landroid/content/DialogInterface;I)V
    .locals 4

    .prologue
    .line 4387
    invoke-static {}, Lcom/osp/device/b;->b()Lcom/osp/device/b;

    move-result-object v0

    iget-object v1, p0, Lcom/osp/app/signin/mx;->a:Lcom/osp/app/signin/SmsVerificationActivity;

    invoke-virtual {v0, v1}, Lcom/osp/device/b;->i(Landroid/content/Context;)Z

    move-result v0

    .line 4391
    :try_start_0
    const-string v1, "FROM_EDIT_PROFILE"

    iget-object v2, p0, Lcom/osp/app/signin/mx;->a:Lcom/osp/app/signin/SmsVerificationActivity;

    invoke-static {v2}, Lcom/osp/app/signin/SmsVerificationActivity;->M(Lcom/osp/app/signin/SmsVerificationActivity;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_1

    const-string v1, "FROM_SIGN_IN_FLOW"

    iget-object v2, p0, Lcom/osp/app/signin/mx;->a:Lcom/osp/app/signin/SmsVerificationActivity;

    invoke-static {v2}, Lcom/osp/app/signin/SmsVerificationActivity;->M(Lcom/osp/app/signin/SmsVerificationActivity;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_1

    .line 4393
    iget-object v0, p0, Lcom/osp/app/signin/mx;->a:Lcom/osp/app/signin/SmsVerificationActivity;

    const/16 v1, 0x16

    invoke-virtual {v0, v1}, Lcom/osp/app/signin/SmsVerificationActivity;->b(I)V

    .line 4394
    iget-object v0, p0, Lcom/osp/app/signin/mx;->a:Lcom/osp/app/signin/SmsVerificationActivity;

    invoke-virtual {v0}, Lcom/osp/app/signin/SmsVerificationActivity;->finish()V

    .line 4420
    :cond_0
    :goto_0
    return-void

    .line 4397
    :cond_1
    iget-object v1, p0, Lcom/osp/app/signin/mx;->a:Lcom/osp/app/signin/SmsVerificationActivity;

    if-eqz v0, :cond_2

    const-string v0, "/main/main.do"

    :goto_1
    invoke-virtual {v1, v0}, Lcom/osp/app/signin/SmsVerificationActivity;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 4398
    new-instance v1, Landroid/content/Intent;

    invoke-direct {v1}, Landroid/content/Intent;-><init>()V

    .line 4399
    const-string v2, "android.intent.action.VIEW"

    invoke-virtual {v1, v2}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 4400
    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/content/Intent;->setData(Landroid/net/Uri;)Landroid/content/Intent;

    .line 4401
    iget-object v2, p0, Lcom/osp/app/signin/mx;->a:Lcom/osp/app/signin/SmsVerificationActivity;

    invoke-static {v1, v2}, Lcom/osp/app/util/ad;->a(Landroid/content/Intent;Landroid/content/Context;)Z

    move-result v2

    const/4 v3, 0x1

    if-ne v2, v3, :cond_3

    .line 4403
    iget-object v0, p0, Lcom/osp/app/signin/mx;->a:Lcom/osp/app/signin/SmsVerificationActivity;

    invoke-virtual {v0, v1}, Lcom/osp/app/signin/SmsVerificationActivity;->startActivity(Landroid/content/Intent;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 4414
    :catch_0
    move-exception v0

    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_0

    .line 4397
    :cond_2
    :try_start_1
    const-string v0, "/link/link.do"

    goto :goto_1

    .line 4406
    :cond_3
    if-eqz v0, :cond_0

    .line 4408
    iget-object v1, p0, Lcom/osp/app/signin/mx;->a:Lcom/osp/app/signin/SmsVerificationActivity;

    invoke-static {v1, v0}, Lcom/osp/app/signin/SmsVerificationActivity;->l(Lcom/osp/app/signin/SmsVerificationActivity;Ljava/lang/String;)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_0
.end method
