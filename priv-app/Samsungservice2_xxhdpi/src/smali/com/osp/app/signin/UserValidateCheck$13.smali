.class Lcom/osp/app/signin/UserValidateCheck$13;
.super Ljava/lang/Object;
.source "UserValidateCheck.java"

# interfaces
.implements Lcom/samsung/android/sdk/pass/SpassFingerprint$IdentifyListener;


# instance fields
.field final synthetic this$0:Lcom/osp/app/signin/UserValidateCheck;


# direct methods
.method constructor <init>(Lcom/osp/app/signin/UserValidateCheck;)V
    .locals 0

    .prologue
    .line 3395
    iput-object p1, p0, Lcom/osp/app/signin/UserValidateCheck$13;->this$0:Lcom/osp/app/signin/UserValidateCheck;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onFinished(I)V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 3415
    if-nez p1, :cond_6

    .line 3418
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v0, "UVC"

    const-string v1, "Verify FingerPirnt Success"

    invoke-static {v0, v1}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 3421
    invoke-static {}, Lcom/osp/device/b;->b()Lcom/osp/device/b;

    move-result-object v0

    invoke-virtual {v0}, Lcom/osp/device/b;->a()Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/osp/app/signin/UserValidateCheck$13;->this$0:Lcom/osp/app/signin/UserValidateCheck;

    invoke-static {v0}, Lcom/osp/app/util/ad;->h(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/telephony/PhoneNumberUtils;->isGlobalPhoneNumber(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 3424
    iget-object v0, p0, Lcom/osp/app/signin/UserValidateCheck$13;->this$0:Lcom/osp/app/signin/UserValidateCheck;

    new-instance v1, Lcom/osp/app/signin/po;

    iget-object v2, p0, Lcom/osp/app/signin/UserValidateCheck$13;->this$0:Lcom/osp/app/signin/UserValidateCheck;

    invoke-direct {v1, v2, v3}, Lcom/osp/app/signin/po;-><init>(Lcom/osp/app/signin/UserValidateCheck;B)V

    invoke-static {v0, v1}, Lcom/osp/app/signin/UserValidateCheck;->a(Lcom/osp/app/signin/UserValidateCheck;Lcom/osp/app/signin/po;)Lcom/osp/app/signin/po;

    .line 3426
    iget-object v0, p0, Lcom/osp/app/signin/UserValidateCheck$13;->this$0:Lcom/osp/app/signin/UserValidateCheck;

    invoke-static {v0}, Lcom/osp/app/signin/UserValidateCheck;->d(Lcom/osp/app/signin/UserValidateCheck;)Lcom/osp/app/signin/po;

    move-result-object v0

    invoke-static {v0}, Lcom/msc/sa/c/d;->a(Landroid/os/AsyncTask;)V

    .line 3504
    :cond_0
    :goto_0
    return-void

    .line 3429
    :cond_1
    const-string v0, "netflix"

    iget-object v1, p0, Lcom/osp/app/signin/UserValidateCheck$13;->this$0:Lcom/osp/app/signin/UserValidateCheck;

    invoke-static {v1}, Lcom/osp/app/signin/UserValidateCheck;->x(Lcom/osp/app/signin/UserValidateCheck;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 3432
    iget-object v0, p0, Lcom/osp/app/signin/UserValidateCheck$13;->this$0:Lcom/osp/app/signin/UserValidateCheck;

    invoke-static {v0}, Lcom/osp/app/signin/UserValidateCheck;->b(Lcom/osp/app/signin/UserValidateCheck;)Ljava/lang/String;

    move-result-object v0

    const-string v1, "OSP_01"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 3435
    iget-object v0, p0, Lcom/osp/app/signin/UserValidateCheck$13;->this$0:Lcom/osp/app/signin/UserValidateCheck;

    new-instance v1, Lcom/osp/app/signin/pn;

    iget-object v2, p0, Lcom/osp/app/signin/UserValidateCheck$13;->this$0:Lcom/osp/app/signin/UserValidateCheck;

    invoke-direct {v1, v2, v3}, Lcom/osp/app/signin/pn;-><init>(Lcom/osp/app/signin/UserValidateCheck;B)V

    invoke-static {v0, v1}, Lcom/osp/app/signin/UserValidateCheck;->a(Lcom/osp/app/signin/UserValidateCheck;Lcom/osp/app/signin/pn;)Lcom/osp/app/signin/pn;

    .line 3444
    iget-object v0, p0, Lcom/osp/app/signin/UserValidateCheck$13;->this$0:Lcom/osp/app/signin/UserValidateCheck;

    invoke-static {v0}, Lcom/osp/app/signin/UserValidateCheck;->c(Lcom/osp/app/signin/UserValidateCheck;)Lcom/osp/app/signin/pn;

    move-result-object v0

    invoke-static {v0}, Lcom/msc/sa/c/d;->a(Landroid/os/AsyncTask;)V

    goto :goto_0

    .line 3447
    :cond_2
    iget-object v0, p0, Lcom/osp/app/signin/UserValidateCheck$13;->this$0:Lcom/osp/app/signin/UserValidateCheck;

    new-instance v1, Lcom/osp/app/signin/po;

    iget-object v2, p0, Lcom/osp/app/signin/UserValidateCheck$13;->this$0:Lcom/osp/app/signin/UserValidateCheck;

    invoke-direct {v1, v2, v3}, Lcom/osp/app/signin/po;-><init>(Lcom/osp/app/signin/UserValidateCheck;B)V

    invoke-static {v0, v1}, Lcom/osp/app/signin/UserValidateCheck;->a(Lcom/osp/app/signin/UserValidateCheck;Lcom/osp/app/signin/po;)Lcom/osp/app/signin/po;

    .line 3456
    iget-object v0, p0, Lcom/osp/app/signin/UserValidateCheck$13;->this$0:Lcom/osp/app/signin/UserValidateCheck;

    invoke-static {v0}, Lcom/osp/app/signin/UserValidateCheck;->d(Lcom/osp/app/signin/UserValidateCheck;)Lcom/osp/app/signin/po;

    move-result-object v0

    invoke-static {v0}, Lcom/msc/sa/c/d;->a(Landroid/os/AsyncTask;)V

    goto :goto_0

    .line 3459
    :cond_3
    iget-object v0, p0, Lcom/osp/app/signin/UserValidateCheck$13;->this$0:Lcom/osp/app/signin/UserValidateCheck;

    invoke-static {v0}, Lcom/osp/app/signin/UserValidateCheck;->o(Lcom/osp/app/signin/UserValidateCheck;)I

    move-result v0

    const/4 v1, 0x1

    if-ne v0, v1, :cond_5

    .line 3461
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v0, "start account info"

    invoke-static {v0}, Lcom/osp/app/util/aq;->b(Ljava/lang/String;)V

    .line 3463
    invoke-static {}, Lcom/osp/app/util/r;->u()Z

    move-result v0

    if-eqz v0, :cond_4

    .line 3467
    new-instance v0, Landroid/content/Intent;

    iget-object v1, p0, Lcom/osp/app/signin/UserValidateCheck$13;->this$0:Lcom/osp/app/signin/UserValidateCheck;

    const-class v2, Lcom/msc/sa/myprofile/MyProfileWebView;

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 3468
    const-string v1, "com.msc.action.samsungaccount.myinfowebview_internal"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 3469
    iget-object v1, p0, Lcom/osp/app/signin/UserValidateCheck$13;->this$0:Lcom/osp/app/signin/UserValidateCheck;

    invoke-virtual {v1, v0}, Lcom/osp/app/signin/UserValidateCheck;->startActivity(Landroid/content/Intent;)V

    .line 3470
    iget-object v0, p0, Lcom/osp/app/signin/UserValidateCheck$13;->this$0:Lcom/osp/app/signin/UserValidateCheck;

    invoke-virtual {v0}, Lcom/osp/app/signin/UserValidateCheck;->finish()V

    goto :goto_0

    .line 3473
    :cond_4
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    .line 3474
    const-string v1, "com.osp.app.signin"

    const-string v2, "com.osp.app.signin.AccountinfoView"

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->setClassName(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 3476
    const/high16 v1, 0x10000000

    invoke-virtual {v0, v1}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    .line 3477
    const-string v1, "client_id"

    iget-object v2, p0, Lcom/osp/app/signin/UserValidateCheck$13;->this$0:Lcom/osp/app/signin/UserValidateCheck;

    invoke-static {v2}, Lcom/osp/app/signin/UserValidateCheck;->p(Lcom/osp/app/signin/UserValidateCheck;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 3478
    const-string v1, "client_secret"

    iget-object v2, p0, Lcom/osp/app/signin/UserValidateCheck$13;->this$0:Lcom/osp/app/signin/UserValidateCheck;

    invoke-static {v2}, Lcom/osp/app/signin/UserValidateCheck;->v(Lcom/osp/app/signin/UserValidateCheck;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 3479
    const-string v1, "account_mode"

    iget-object v2, p0, Lcom/osp/app/signin/UserValidateCheck$13;->this$0:Lcom/osp/app/signin/UserValidateCheck;

    invoke-static {v2}, Lcom/osp/app/signin/UserValidateCheck;->n(Lcom/osp/app/signin/UserValidateCheck;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 3480
    const-string v1, "service_name"

    iget-object v2, p0, Lcom/osp/app/signin/UserValidateCheck$13;->this$0:Lcom/osp/app/signin/UserValidateCheck;

    invoke-static {v2}, Lcom/osp/app/signin/UserValidateCheck;->F(Lcom/osp/app/signin/UserValidateCheck;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 3481
    const-string v1, "OSP_VER"

    iget-object v2, p0, Lcom/osp/app/signin/UserValidateCheck$13;->this$0:Lcom/osp/app/signin/UserValidateCheck;

    invoke-static {v2}, Lcom/osp/app/signin/UserValidateCheck;->b(Lcom/osp/app/signin/UserValidateCheck;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 3482
    const-string v1, "key_request_id"

    iget-object v2, p0, Lcom/osp/app/signin/UserValidateCheck$13;->this$0:Lcom/osp/app/signin/UserValidateCheck;

    invoke-static {v2}, Lcom/osp/app/signin/UserValidateCheck;->G(Lcom/osp/app/signin/UserValidateCheck;)J

    move-result-wide v2

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;J)Landroid/content/Intent;

    .line 3483
    const-string v1, "is_authorized_internal_action_for_accountinfo"

    const-string v2, "TIME_PATTERN"

    invoke-static {v2}, Lcom/osp/app/util/ad;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 3485
    iget-object v1, p0, Lcom/osp/app/signin/UserValidateCheck$13;->this$0:Lcom/osp/app/signin/UserValidateCheck;

    invoke-virtual {v1, v0}, Lcom/osp/app/signin/UserValidateCheck;->startActivity(Landroid/content/Intent;)V

    .line 3486
    iget-object v0, p0, Lcom/osp/app/signin/UserValidateCheck$13;->this$0:Lcom/osp/app/signin/UserValidateCheck;

    invoke-virtual {v0}, Lcom/osp/app/signin/UserValidateCheck;->finish()V

    goto/16 :goto_0

    .line 3490
    :cond_5
    iget-object v0, p0, Lcom/osp/app/signin/UserValidateCheck$13;->this$0:Lcom/osp/app/signin/UserValidateCheck;

    const/4 v1, -0x1

    iget-object v2, p0, Lcom/osp/app/signin/UserValidateCheck$13;->this$0:Lcom/osp/app/signin/UserValidateCheck;

    invoke-static {v2}, Lcom/osp/app/signin/UserValidateCheck;->f(Lcom/osp/app/signin/UserValidateCheck;)Landroid/content/Intent;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/osp/app/signin/UserValidateCheck;->a(ILandroid/content/Intent;)V

    .line 3491
    iget-object v0, p0, Lcom/osp/app/signin/UserValidateCheck$13;->this$0:Lcom/osp/app/signin/UserValidateCheck;

    invoke-virtual {v0}, Lcom/osp/app/signin/UserValidateCheck;->finish()V

    goto/16 :goto_0

    .line 3496
    :cond_6
    iget-object v0, p0, Lcom/osp/app/signin/UserValidateCheck$13;->this$0:Lcom/osp/app/signin/UserValidateCheck;

    invoke-virtual {v0}, Lcom/osp/app/signin/UserValidateCheck;->getWindow()Landroid/view/Window;

    move-result-object v0

    const/4 v1, 0x5

    invoke-virtual {v0, v1}, Landroid/view/Window;->setSoftInputMode(I)V

    .line 3497
    iget-object v0, p0, Lcom/osp/app/signin/UserValidateCheck$13;->this$0:Lcom/osp/app/signin/UserValidateCheck;

    invoke-static {v0}, Lcom/osp/app/signin/UserValidateCheck;->e(Lcom/osp/app/signin/UserValidateCheck;)Landroid/widget/EditText;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/EditText;->requestFocus()Z

    .line 3499
    iget-object v0, p0, Lcom/osp/app/signin/UserValidateCheck$13;->this$0:Lcom/osp/app/signin/UserValidateCheck;

    invoke-static {v0}, Lcom/osp/app/signin/UserValidateCheck;->k(Lcom/osp/app/signin/UserValidateCheck;)Landroid/app/AlertDialog;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/osp/app/signin/UserValidateCheck$13;->this$0:Lcom/osp/app/signin/UserValidateCheck;

    invoke-static {v0}, Lcom/osp/app/signin/UserValidateCheck;->k(Lcom/osp/app/signin/UserValidateCheck;)Landroid/app/AlertDialog;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/AlertDialog;->isShowing()Z

    move-result v0

    if-nez v0, :cond_0

    .line 3501
    iget-object v0, p0, Lcom/osp/app/signin/UserValidateCheck$13;->this$0:Lcom/osp/app/signin/UserValidateCheck;

    invoke-static {v0}, Lcom/osp/app/signin/UserValidateCheck;->k(Lcom/osp/app/signin/UserValidateCheck;)Landroid/app/AlertDialog;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/AlertDialog;->show()V

    goto/16 :goto_0
.end method

.method public onReady()V
    .locals 0

    .prologue
    .line 3409
    return-void
.end method

.method public onStarted()V
    .locals 0

    .prologue
    .line 3403
    return-void
.end method
