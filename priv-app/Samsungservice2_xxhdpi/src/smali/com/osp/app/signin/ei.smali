.class final enum Lcom/osp/app/signin/ei;
.super Ljava/lang/Enum;
.source "ConnectionToken.java"


# static fields
.field public static final enum a:Lcom/osp/app/signin/ei;

.field public static final enum b:Lcom/osp/app/signin/ei;

.field public static final enum c:Lcom/osp/app/signin/ei;

.field public static final enum d:Lcom/osp/app/signin/ei;

.field public static final enum e:Lcom/osp/app/signin/ei;

.field public static final enum f:Lcom/osp/app/signin/ei;

.field public static final enum g:Lcom/osp/app/signin/ei;

.field public static final enum h:Lcom/osp/app/signin/ei;

.field public static final enum i:Lcom/osp/app/signin/ei;

.field public static final enum j:Lcom/osp/app/signin/ei;

.field public static final enum k:Lcom/osp/app/signin/ei;

.field public static final enum l:Lcom/osp/app/signin/ei;

.field public static final enum m:Lcom/osp/app/signin/ei;

.field private static final synthetic n:[Lcom/osp/app/signin/ei;


# direct methods
.method static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 127
    new-instance v0, Lcom/osp/app/signin/ei;

    const-string v1, "Start"

    invoke-direct {v0, v1, v3}, Lcom/osp/app/signin/ei;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/osp/app/signin/ei;->a:Lcom/osp/app/signin/ei;

    new-instance v0, Lcom/osp/app/signin/ei;

    const-string v1, "IsContainAppId"

    invoke-direct {v0, v1, v4}, Lcom/osp/app/signin/ei;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/osp/app/signin/ei;->b:Lcom/osp/app/signin/ei;

    new-instance v0, Lcom/osp/app/signin/ei;

    const-string v1, "IsAppRegistered"

    invoke-direct {v0, v1, v5}, Lcom/osp/app/signin/ei;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/osp/app/signin/ei;->c:Lcom/osp/app/signin/ei;

    new-instance v0, Lcom/osp/app/signin/ei;

    const-string v1, "IsNotAppRegistered"

    invoke-direct {v0, v1, v6}, Lcom/osp/app/signin/ei;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/osp/app/signin/ei;->d:Lcom/osp/app/signin/ei;

    new-instance v0, Lcom/osp/app/signin/ei;

    const-string v1, "ContainsAccessToken"

    invoke-direct {v0, v1, v7}, Lcom/osp/app/signin/ei;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/osp/app/signin/ei;->e:Lcom/osp/app/signin/ei;

    new-instance v0, Lcom/osp/app/signin/ei;

    const-string v1, "NotContainsAccessToken"

    const/4 v2, 0x5

    invoke-direct {v0, v1, v2}, Lcom/osp/app/signin/ei;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/osp/app/signin/ei;->f:Lcom/osp/app/signin/ei;

    new-instance v0, Lcom/osp/app/signin/ei;

    const-string v1, "ContainsAuthToken"

    const/4 v2, 0x6

    invoke-direct {v0, v1, v2}, Lcom/osp/app/signin/ei;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/osp/app/signin/ei;->g:Lcom/osp/app/signin/ei;

    new-instance v0, Lcom/osp/app/signin/ei;

    const-string v1, "NotContainsAuthToken"

    const/4 v2, 0x7

    invoke-direct {v0, v1, v2}, Lcom/osp/app/signin/ei;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/osp/app/signin/ei;->h:Lcom/osp/app/signin/ei;

    new-instance v0, Lcom/osp/app/signin/ei;

    const-string v1, "UserAuthentication"

    const/16 v2, 0x8

    invoke-direct {v0, v1, v2}, Lcom/osp/app/signin/ei;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/osp/app/signin/ei;->i:Lcom/osp/app/signin/ei;

    new-instance v0, Lcom/osp/app/signin/ei;

    const-string v1, "Success"

    const/16 v2, 0x9

    invoke-direct {v0, v1, v2}, Lcom/osp/app/signin/ei;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/osp/app/signin/ei;->j:Lcom/osp/app/signin/ei;

    new-instance v0, Lcom/osp/app/signin/ei;

    const-string v1, "Canceled"

    const/16 v2, 0xa

    invoke-direct {v0, v1, v2}, Lcom/osp/app/signin/ei;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/osp/app/signin/ei;->k:Lcom/osp/app/signin/ei;

    new-instance v0, Lcom/osp/app/signin/ei;

    const-string v1, "Failed"

    const/16 v2, 0xb

    invoke-direct {v0, v1, v2}, Lcom/osp/app/signin/ei;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/osp/app/signin/ei;->l:Lcom/osp/app/signin/ei;

    new-instance v0, Lcom/osp/app/signin/ei;

    const-string v1, "RequireEmailValidation"

    const/16 v2, 0xc

    invoke-direct {v0, v1, v2}, Lcom/osp/app/signin/ei;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/osp/app/signin/ei;->m:Lcom/osp/app/signin/ei;

    .line 125
    const/16 v0, 0xd

    new-array v0, v0, [Lcom/osp/app/signin/ei;

    sget-object v1, Lcom/osp/app/signin/ei;->a:Lcom/osp/app/signin/ei;

    aput-object v1, v0, v3

    sget-object v1, Lcom/osp/app/signin/ei;->b:Lcom/osp/app/signin/ei;

    aput-object v1, v0, v4

    sget-object v1, Lcom/osp/app/signin/ei;->c:Lcom/osp/app/signin/ei;

    aput-object v1, v0, v5

    sget-object v1, Lcom/osp/app/signin/ei;->d:Lcom/osp/app/signin/ei;

    aput-object v1, v0, v6

    sget-object v1, Lcom/osp/app/signin/ei;->e:Lcom/osp/app/signin/ei;

    aput-object v1, v0, v7

    const/4 v1, 0x5

    sget-object v2, Lcom/osp/app/signin/ei;->f:Lcom/osp/app/signin/ei;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, Lcom/osp/app/signin/ei;->g:Lcom/osp/app/signin/ei;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, Lcom/osp/app/signin/ei;->h:Lcom/osp/app/signin/ei;

    aput-object v2, v0, v1

    const/16 v1, 0x8

    sget-object v2, Lcom/osp/app/signin/ei;->i:Lcom/osp/app/signin/ei;

    aput-object v2, v0, v1

    const/16 v1, 0x9

    sget-object v2, Lcom/osp/app/signin/ei;->j:Lcom/osp/app/signin/ei;

    aput-object v2, v0, v1

    const/16 v1, 0xa

    sget-object v2, Lcom/osp/app/signin/ei;->k:Lcom/osp/app/signin/ei;

    aput-object v2, v0, v1

    const/16 v1, 0xb

    sget-object v2, Lcom/osp/app/signin/ei;->l:Lcom/osp/app/signin/ei;

    aput-object v2, v0, v1

    const/16 v1, 0xc

    sget-object v2, Lcom/osp/app/signin/ei;->m:Lcom/osp/app/signin/ei;

    aput-object v2, v0, v1

    sput-object v0, Lcom/osp/app/signin/ei;->n:[Lcom/osp/app/signin/ei;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0

    .prologue
    .line 125
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/osp/app/signin/ei;
    .locals 1

    .prologue
    .line 125
    const-class v0, Lcom/osp/app/signin/ei;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/osp/app/signin/ei;

    return-object v0
.end method

.method public static values()[Lcom/osp/app/signin/ei;
    .locals 1

    .prologue
    .line 125
    sget-object v0, Lcom/osp/app/signin/ei;->n:[Lcom/osp/app/signin/ei;

    invoke-virtual {v0}, [Lcom/osp/app/signin/ei;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/osp/app/signin/ei;

    return-object v0
.end method
