.class final Lcom/osp/app/signin/hg;
.super Lcom/msc/c/b;
.source "SecurityInfoChangeActivity.java"


# instance fields
.field final synthetic c:Lcom/osp/app/signin/SecurityInfoChangeActivity;

.field private d:J

.field private e:J

.field private f:J

.field private g:J

.field private h:Ljava/lang/String;

.field private i:Z


# direct methods
.method public constructor <init>(Lcom/osp/app/signin/SecurityInfoChangeActivity;)V
    .locals 2

    .prologue
    .line 961
    iput-object p1, p0, Lcom/osp/app/signin/hg;->c:Lcom/osp/app/signin/SecurityInfoChangeActivity;

    .line 962
    invoke-direct {p0, p1}, Lcom/msc/c/b;-><init>(Landroid/content/Context;)V

    .line 956
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/osp/app/signin/hg;->i:Z

    .line 967
    invoke-static {p1}, Lcom/osp/app/signin/SecurityInfoChangeActivity;->a(Lcom/osp/app/signin/SecurityInfoChangeActivity;)Lcom/msc/a/j;

    move-result-object v0

    if-nez v0, :cond_0

    .line 969
    new-instance v0, Lcom/msc/a/j;

    invoke-direct {v0}, Lcom/msc/a/j;-><init>()V

    invoke-static {p1, v0}, Lcom/osp/app/signin/SecurityInfoChangeActivity;->a(Lcom/osp/app/signin/SecurityInfoChangeActivity;Lcom/msc/a/j;)Lcom/msc/a/j;

    .line 971
    :cond_0
    invoke-static {}, Lcom/osp/app/util/y;->a()Lcom/osp/app/util/y;

    move-result-object v0

    invoke-virtual {v0}, Lcom/osp/app/util/y;->c()Ljava/lang/String;

    move-result-object v0

    .line 972
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_1

    .line 974
    invoke-static {p1}, Lcom/osp/app/signin/SecurityInfoChangeActivity;->a(Lcom/osp/app/signin/SecurityInfoChangeActivity;)Lcom/msc/a/j;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/msc/a/j;->a(Ljava/lang/String;)V

    .line 976
    :cond_1
    invoke-static {p1}, Lcom/osp/app/signin/SecurityInfoChangeActivity;->a(Lcom/osp/app/signin/SecurityInfoChangeActivity;)Lcom/msc/a/j;

    move-result-object v0

    invoke-static {p1}, Lcom/osp/app/signin/SecurityInfoChangeActivity;->b(Lcom/osp/app/signin/SecurityInfoChangeActivity;)Landroid/widget/Button;

    move-result-object v1

    invoke-virtual {v1}, Landroid/widget/Button;->getText()Ljava/lang/CharSequence;

    move-result-object v1

    invoke-interface {v1}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/msc/a/j;->b(Ljava/lang/String;)V

    .line 977
    invoke-static {p1}, Lcom/osp/app/signin/SecurityInfoChangeActivity;->a(Lcom/osp/app/signin/SecurityInfoChangeActivity;)Lcom/msc/a/j;

    move-result-object v0

    invoke-static {p1}, Lcom/osp/app/signin/SecurityInfoChangeActivity;->c(Lcom/osp/app/signin/SecurityInfoChangeActivity;)Landroid/widget/EditText;

    move-result-object v1

    invoke-virtual {v1}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/msc/a/j;->c(Ljava/lang/String;)V

    .line 980
    return-void
.end method

.method private b(Ljava/lang/String;)V
    .locals 3

    .prologue
    .line 1177
    invoke-static {}, Lcom/msc/c/g;->a()Lcom/msc/c/g;

    .line 1178
    iget-object v0, p0, Lcom/osp/app/signin/hg;->c:Lcom/osp/app/signin/SecurityInfoChangeActivity;

    iget-object v1, p0, Lcom/osp/app/signin/hg;->c:Lcom/osp/app/signin/SecurityInfoChangeActivity;

    invoke-static {v1}, Lcom/msc/c/e;->g(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x0

    invoke-static {v0, p1, v1, v2, p0}, Lcom/msc/c/g;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/msc/b/h;)J

    move-result-wide v0

    iput-wide v0, p0, Lcom/osp/app/signin/hg;->f:J

    .line 1180
    iget-wide v0, p0, Lcom/osp/app/signin/hg;->f:J

    invoke-virtual {p0, v0, v1}, Lcom/osp/app/signin/hg;->a(J)V

    .line 1181
    iget-wide v0, p0, Lcom/osp/app/signin/hg;->f:J

    const-string v2, "from_json"

    invoke-virtual {p0, v0, v1, v2}, Lcom/osp/app/signin/hg;->a(JLjava/lang/String;)V

    .line 1183
    iget-wide v0, p0, Lcom/osp/app/signin/hg;->f:J

    sget-object v2, Lcom/msc/b/g;->b:Lcom/msc/b/g;

    invoke-static {v0, v1, v2}, Lcom/msc/c/g;->a(JLcom/msc/b/g;)V

    .line 1184
    return-void
.end method

.method private c(Ljava/lang/String;)V
    .locals 3

    .prologue
    .line 1209
    invoke-static {}, Lcom/msc/c/g;->a()Lcom/msc/c/g;

    .line 1210
    iget-object v0, p0, Lcom/osp/app/signin/hg;->c:Lcom/osp/app/signin/SecurityInfoChangeActivity;

    iget-object v1, p0, Lcom/osp/app/signin/hg;->c:Lcom/osp/app/signin/SecurityInfoChangeActivity;

    invoke-static {v1}, Lcom/msc/c/e;->h(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/osp/app/signin/hg;->c:Lcom/osp/app/signin/SecurityInfoChangeActivity;

    invoke-static {v2}, Lcom/osp/app/signin/SecurityInfoChangeActivity;->a(Lcom/osp/app/signin/SecurityInfoChangeActivity;)Lcom/msc/a/j;

    move-result-object v2

    invoke-static {v0, p1, v1, v2, p0}, Lcom/msc/c/g;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Lcom/msc/a/j;Lcom/msc/b/h;)J

    move-result-wide v0

    iput-wide v0, p0, Lcom/osp/app/signin/hg;->d:J

    .line 1213
    iget-wide v0, p0, Lcom/osp/app/signin/hg;->d:J

    invoke-virtual {p0, v0, v1}, Lcom/osp/app/signin/hg;->a(J)V

    .line 1214
    iget-wide v0, p0, Lcom/osp/app/signin/hg;->d:J

    const-string v2, "from_xml"

    invoke-virtual {p0, v0, v1, v2}, Lcom/osp/app/signin/hg;->a(JLjava/lang/String;)V

    .line 1216
    iget-wide v0, p0, Lcom/osp/app/signin/hg;->d:J

    sget-object v2, Lcom/msc/b/g;->d:Lcom/msc/b/g;

    invoke-static {v0, v1, v2}, Lcom/msc/c/g;->a(JLcom/msc/b/g;)V

    .line 1217
    return-void
.end method


# virtual methods
.method protected final varargs a()Ljava/lang/Boolean;
    .locals 1

    .prologue
    .line 991
    iget-object v0, p0, Lcom/osp/app/signin/hg;->c:Lcom/osp/app/signin/SecurityInfoChangeActivity;

    invoke-static {v0}, Lcom/msc/c/e;->b(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v0

    .line 992
    if-eqz v0, :cond_0

    .line 994
    invoke-direct {p0, v0}, Lcom/osp/app/signin/hg;->c(Ljava/lang/String;)V

    .line 999
    :goto_0
    const/4 v0, 0x0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    return-object v0

    .line 997
    :cond_0
    const-string v0, "j5p7ll8g33"

    invoke-direct {p0, v0}, Lcom/osp/app/signin/hg;->b(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public final declared-synchronized a(Lcom/msc/b/c;)V
    .locals 6

    .prologue
    .line 1062
    monitor-enter p0

    :try_start_0
    invoke-super {p0, p1}, Lcom/msc/c/b;->a(Lcom/msc/b/c;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1064
    if-nez p1, :cond_1

    .line 1138
    :cond_0
    :goto_0
    monitor-exit p0

    return-void

    .line 1069
    :cond_1
    :try_start_1
    invoke-virtual {p1}, Lcom/msc/b/c;->a()J

    move-result-wide v0

    .line 1070
    invoke-virtual {p1}, Lcom/msc/b/c;->e()Ljava/lang/String;

    move-result-object v2

    .line 1072
    iget-wide v4, p0, Lcom/osp/app/signin/hg;->e:J
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    cmp-long v3, v0, v4

    if-nez v3, :cond_3

    .line 1076
    :try_start_2
    invoke-static {}, Lcom/msc/c/h;->a()Lcom/msc/c/h;

    invoke-static {v2}, Lcom/msc/c/h;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 1077
    if-eqz v0, :cond_2

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v1

    if-lez v1, :cond_2

    .line 1079
    iget-object v1, p0, Lcom/osp/app/signin/hg;->c:Lcom/osp/app/signin/SecurityInfoChangeActivity;

    invoke-static {v1, v0}, Lcom/msc/c/e;->e(Landroid/content/Context;Ljava/lang/String;)V

    .line 1080
    invoke-virtual {p0}, Lcom/osp/app/signin/hg;->isCancelled()Z

    move-result v0

    if-nez v0, :cond_0

    .line 1084
    const-string v0, "j5p7ll8g33"

    invoke-direct {p0, v0}, Lcom/osp/app/signin/hg;->b(Ljava/lang/String;)V
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_0
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0

    .line 1089
    :catch_0
    move-exception v0

    .line 1091
    :try_start_3
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    .line 1092
    new-instance v1, Lcom/msc/c/f;

    invoke-direct {v1, v0}, Lcom/msc/c/f;-><init>(Ljava/lang/Exception;)V

    iput-object v1, p0, Lcom/osp/app/signin/hg;->b:Lcom/msc/c/f;
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    goto :goto_0

    .line 1062
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 1087
    :cond_2
    :try_start_4
    new-instance v0, Lcom/msc/c/f;

    invoke-direct {v0}, Lcom/msc/c/f;-><init>()V

    iput-object v0, p0, Lcom/osp/app/signin/hg;->b:Lcom/msc/c/f;
    :try_end_4
    .catch Ljava/lang/Exception; {:try_start_4 .. :try_end_4} :catch_0
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    goto :goto_0

    .line 1094
    :cond_3
    :try_start_5
    iget-wide v4, p0, Lcom/osp/app/signin/hg;->f:J

    cmp-long v3, v0, v4

    if-nez v3, :cond_4

    .line 1096
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "onSuccess() : "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;)V
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    .line 1099
    :try_start_6
    invoke-static {}, Lcom/msc/c/h;->a()Lcom/msc/c/h;

    invoke-static {v2}, Lcom/msc/c/h;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 1100
    invoke-virtual {p0}, Lcom/osp/app/signin/hg;->isCancelled()Z

    move-result v0

    if-nez v0, :cond_0

    .line 1104
    const-string v3, "j5p7ll8g33"

    const-string v4, "5763D0052DC1462E13751F753384E9A9"

    invoke-static {}, Lcom/msc/c/g;->a()Lcom/msc/c/g;

    iget-object v0, p0, Lcom/osp/app/signin/hg;->c:Lcom/osp/app/signin/SecurityInfoChangeActivity;

    const-string v1, "authorization_code"

    move-object v5, p0

    invoke-static/range {v0 .. v5}, Lcom/msc/c/g;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/msc/b/h;)J

    move-result-wide v0

    iput-wide v0, p0, Lcom/osp/app/signin/hg;->g:J

    iget-wide v0, p0, Lcom/osp/app/signin/hg;->g:J

    invoke-virtual {p0, v0, v1}, Lcom/osp/app/signin/hg;->a(J)V

    iget-wide v0, p0, Lcom/osp/app/signin/hg;->g:J

    const-string v2, "from_xml"

    invoke-virtual {p0, v0, v1, v2}, Lcom/osp/app/signin/hg;->a(JLjava/lang/String;)V

    iget-wide v0, p0, Lcom/osp/app/signin/hg;->g:J

    sget-object v2, Lcom/msc/b/g;->c:Lcom/msc/b/g;

    invoke-static {v0, v1, v2}, Lcom/msc/c/g;->a(JLcom/msc/b/g;)V
    :try_end_6
    .catch Ljava/lang/Exception; {:try_start_6 .. :try_end_6} :catch_1
    .catchall {:try_start_6 .. :try_end_6} :catchall_0

    goto/16 :goto_0

    .line 1105
    :catch_1
    move-exception v0

    .line 1107
    :try_start_7
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    .line 1108
    new-instance v1, Lcom/msc/c/f;

    invoke-direct {v1, v0}, Lcom/msc/c/f;-><init>(Ljava/lang/Exception;)V

    iput-object v1, p0, Lcom/osp/app/signin/hg;->b:Lcom/msc/c/f;

    goto/16 :goto_0

    .line 1110
    :cond_4
    iget-wide v4, p0, Lcom/osp/app/signin/hg;->g:J

    cmp-long v3, v0, v4

    if-nez v3, :cond_5

    .line 1112
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "onSuccess() : "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;)V
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_0

    .line 1115
    :try_start_8
    invoke-static {}, Lcom/msc/c/h;->a()Lcom/msc/c/h;

    iget-object v0, p0, Lcom/osp/app/signin/hg;->c:Lcom/osp/app/signin/SecurityInfoChangeActivity;

    invoke-static {v0, v2}, Lcom/msc/c/h;->a(Landroid/content/Context;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 1116
    iget-object v1, p0, Lcom/osp/app/signin/hg;->c:Lcom/osp/app/signin/SecurityInfoChangeActivity;

    invoke-static {v1, v0}, Lcom/msc/c/e;->b(Landroid/content/Context;Ljava/lang/String;)V

    .line 1117
    invoke-virtual {p0}, Lcom/osp/app/signin/hg;->isCancelled()Z

    move-result v1

    if-nez v1, :cond_0

    .line 1121
    invoke-direct {p0, v0}, Lcom/osp/app/signin/hg;->c(Ljava/lang/String;)V
    :try_end_8
    .catch Ljava/lang/Exception; {:try_start_8 .. :try_end_8} :catch_2
    .catchall {:try_start_8 .. :try_end_8} :catchall_0

    goto/16 :goto_0

    .line 1122
    :catch_2
    move-exception v0

    .line 1124
    :try_start_9
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    .line 1125
    new-instance v1, Lcom/msc/c/f;

    invoke-direct {v1, v0}, Lcom/msc/c/f;-><init>(Ljava/lang/Exception;)V

    iput-object v1, p0, Lcom/osp/app/signin/hg;->b:Lcom/msc/c/f;

    goto/16 :goto_0

    .line 1127
    :cond_5
    iget-wide v4, p0, Lcom/osp/app/signin/hg;->d:J
    :try_end_9
    .catchall {:try_start_9 .. :try_end_9} :catchall_0

    cmp-long v0, v0, v4

    if-nez v0, :cond_0

    .line 1131
    :try_start_a
    invoke-static {}, Lcom/msc/c/h;->a()Lcom/msc/c/h;

    iget-object v0, p0, Lcom/osp/app/signin/hg;->c:Lcom/osp/app/signin/SecurityInfoChangeActivity;

    invoke-static {v2}, Lcom/msc/c/h;->J(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/osp/app/signin/hg;->h:Ljava/lang/String;
    :try_end_a
    .catch Ljava/lang/Exception; {:try_start_a .. :try_end_a} :catch_3
    .catchall {:try_start_a .. :try_end_a} :catchall_0

    goto/16 :goto_0

    .line 1132
    :catch_3
    move-exception v0

    .line 1134
    :try_start_b
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    .line 1135
    new-instance v1, Lcom/msc/c/f;

    invoke-direct {v1, v0}, Lcom/msc/c/f;-><init>(Ljava/lang/Exception;)V

    iput-object v1, p0, Lcom/osp/app/signin/hg;->b:Lcom/msc/c/f;
    :try_end_b
    .catchall {:try_start_b .. :try_end_b} :catchall_0

    goto/16 :goto_0
.end method

.method protected final a(Ljava/lang/Boolean;)V
    .locals 5

    .prologue
    const/16 v4, 0x13

    const/4 v2, 0x1

    const/4 v3, 0x0

    .line 1004
    invoke-super {p0, p1}, Lcom/msc/c/b;->a(Ljava/lang/Boolean;)V

    .line 1005
    iget-object v0, p0, Lcom/osp/app/signin/hg;->b:Lcom/msc/c/f;

    if-eqz v0, :cond_4

    .line 1007
    const-string v0, "AUT_1302"

    iget-object v1, p0, Lcom/osp/app/signin/hg;->b:Lcom/msc/c/f;

    invoke-virtual {v1}, Lcom/msc/c/f;->a()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    const-string v0, "AUT_1830"

    iget-object v1, p0, Lcom/osp/app/signin/hg;->b:Lcom/msc/c/f;

    invoke-virtual {v1}, Lcom/msc/c/f;->a()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1009
    :cond_0
    iget-object v0, p0, Lcom/osp/app/signin/hg;->c:Lcom/osp/app/signin/SecurityInfoChangeActivity;

    invoke-static {v0}, Lcom/msc/c/c;->g(Landroid/content/Context;)V

    .line 1010
    iget-object v0, p0, Lcom/osp/app/signin/hg;->c:Lcom/osp/app/signin/SecurityInfoChangeActivity;

    invoke-static {v0}, Lcom/msc/c/e;->a(Landroid/content/Context;)Z

    .line 1011
    iget-object v0, p0, Lcom/osp/app/signin/hg;->c:Lcom/osp/app/signin/SecurityInfoChangeActivity;

    iget-object v1, p0, Lcom/osp/app/signin/hg;->c:Lcom/osp/app/signin/SecurityInfoChangeActivity;

    invoke-static {v1}, Lcom/osp/app/util/ad;->h(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/osp/app/signin/SecurityInfoChangeActivity;->a(Lcom/osp/app/signin/SecurityInfoChangeActivity;Ljava/lang/String;)V

    .line 1012
    iget-object v0, p0, Lcom/osp/app/signin/hg;->c:Lcom/osp/app/signin/SecurityInfoChangeActivity;

    invoke-virtual {v0, v4}, Lcom/osp/app/signin/SecurityInfoChangeActivity;->setResult(I)V

    .line 1013
    iget-object v0, p0, Lcom/osp/app/signin/hg;->c:Lcom/osp/app/signin/SecurityInfoChangeActivity;

    invoke-virtual {v0}, Lcom/osp/app/signin/SecurityInfoChangeActivity;->finish()V

    .line 1058
    :goto_0
    return-void

    .line 1015
    :cond_1
    invoke-static {}, Lcom/osp/device/b;->b()Lcom/osp/device/b;

    move-result-object v0

    invoke-virtual {v0}, Lcom/osp/device/b;->a()Z

    move-result v0

    if-eqz v0, :cond_2

    const-string v0, "AUT_1094"

    iget-object v1, p0, Lcom/osp/app/signin/hg;->b:Lcom/msc/c/f;

    invoke-virtual {v1}, Lcom/msc/c/f;->a()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 1017
    iget-object v0, p0, Lcom/osp/app/signin/hg;->c:Lcom/osp/app/signin/SecurityInfoChangeActivity;

    invoke-static {v0}, Lcom/osp/app/signin/SecurityInfoChangeActivity;->f(Lcom/osp/app/signin/SecurityInfoChangeActivity;)V

    .line 1018
    iget-object v0, p0, Lcom/osp/app/signin/hg;->c:Lcom/osp/app/signin/SecurityInfoChangeActivity;

    invoke-virtual {v0, v4}, Lcom/osp/app/signin/SecurityInfoChangeActivity;->setResult(I)V

    .line 1019
    iget-object v0, p0, Lcom/osp/app/signin/hg;->c:Lcom/osp/app/signin/SecurityInfoChangeActivity;

    invoke-virtual {v0}, Lcom/osp/app/signin/SecurityInfoChangeActivity;->finish()V

    goto :goto_0

    .line 1021
    :cond_2
    iget-object v0, p0, Lcom/osp/app/signin/hg;->b:Lcom/msc/c/f;

    invoke-virtual {v0}, Lcom/msc/c/f;->c()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 1023
    iget-object v0, p0, Lcom/osp/app/signin/hg;->c:Lcom/osp/app/signin/SecurityInfoChangeActivity;

    const/4 v1, 0x3

    invoke-virtual {v0, v1}, Lcom/osp/app/signin/SecurityInfoChangeActivity;->b(I)V

    .line 1025
    :cond_3
    invoke-virtual {p0, v3}, Lcom/osp/app/signin/hg;->a(Z)V

    goto :goto_0

    .line 1028
    :cond_4
    iget-object v0, p0, Lcom/osp/app/signin/hg;->h:Ljava/lang/String;

    if-eqz v0, :cond_6

    .line 1030
    const-string v0, "true"

    iget-object v1, p0, Lcom/osp/app/signin/hg;->h:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_5

    .line 1034
    invoke-static {}, Lcom/osp/app/util/ap;->a()Lcom/osp/app/util/ap;

    iget-object v0, p0, Lcom/osp/app/signin/hg;->c:Lcom/osp/app/signin/SecurityInfoChangeActivity;

    iget-object v1, p0, Lcom/osp/app/signin/hg;->c:Lcom/osp/app/signin/SecurityInfoChangeActivity;

    const v2, 0x7f090050

    invoke-virtual {v1, v2}, Lcom/osp/app/signin/SecurityInfoChangeActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1, v3}, Lcom/osp/app/util/ap;->a(Landroid/content/Context;Ljava/lang/String;I)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    .line 1035
    iget-object v0, p0, Lcom/osp/app/signin/hg;->c:Lcom/osp/app/signin/SecurityInfoChangeActivity;

    const/4 v1, -0x1

    invoke-virtual {v0, v1}, Lcom/osp/app/signin/SecurityInfoChangeActivity;->b(I)V

    .line 1047
    iget-object v0, p0, Lcom/osp/app/signin/hg;->c:Lcom/osp/app/signin/SecurityInfoChangeActivity;

    invoke-virtual {v0}, Lcom/osp/app/signin/SecurityInfoChangeActivity;->finish()V

    goto :goto_0

    .line 1050
    :cond_5
    invoke-virtual {p0, v3}, Lcom/osp/app/signin/hg;->a(Z)V

    .line 1051
    iget-object v0, p0, Lcom/osp/app/signin/hg;->c:Lcom/osp/app/signin/SecurityInfoChangeActivity;

    invoke-virtual {v0, v2}, Lcom/osp/app/signin/SecurityInfoChangeActivity;->b(I)V

    goto :goto_0

    .line 1055
    :cond_6
    invoke-virtual {p0, v3}, Lcom/osp/app/signin/hg;->a(Z)V

    .line 1056
    iget-object v0, p0, Lcom/osp/app/signin/hg;->c:Lcom/osp/app/signin/SecurityInfoChangeActivity;

    invoke-virtual {v0, v2}, Lcom/osp/app/signin/SecurityInfoChangeActivity;->b(I)V

    goto :goto_0
.end method

.method public final b(Lcom/msc/b/c;)V
    .locals 6

    .prologue
    const/4 v3, 0x1

    .line 1142
    invoke-super {p0, p1}, Lcom/msc/c/b;->b(Lcom/msc/b/c;)V

    .line 1144
    if-nez p1, :cond_1

    .line 1168
    :cond_0
    :goto_0
    return-void

    .line 1149
    :cond_1
    invoke-virtual {p1}, Lcom/msc/b/c;->a()J

    move-result-wide v0

    .line 1150
    invoke-virtual {p1}, Lcom/msc/b/c;->e()Ljava/lang/String;

    move-result-object v2

    .line 1152
    iget-wide v4, p0, Lcom/osp/app/signin/hg;->d:J

    cmp-long v0, v0, v4

    if-nez v0, :cond_0

    .line 1154
    if-eqz v2, :cond_0

    .line 1156
    iget-boolean v0, p0, Lcom/osp/app/signin/hg;->i:Z

    if-nez v0, :cond_2

    const-string v0, "ACF_0403"

    iget-object v1, p0, Lcom/osp/app/signin/hg;->b:Lcom/msc/c/f;

    invoke-virtual {v1}, Lcom/msc/c/f;->a()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 1158
    iget-object v0, p0, Lcom/osp/app/signin/hg;->c:Lcom/osp/app/signin/SecurityInfoChangeActivity;

    const-string v1, ""

    invoke-static {v0, v1}, Lcom/msc/c/e;->b(Landroid/content/Context;Ljava/lang/String;)V

    .line 1159
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/osp/app/signin/hg;->b:Lcom/msc/c/f;

    .line 1160
    iput-boolean v3, p0, Lcom/osp/app/signin/hg;->i:Z

    .line 1161
    const-string v0, "j5p7ll8g33"

    invoke-direct {p0, v0}, Lcom/osp/app/signin/hg;->b(Ljava/lang/String;)V

    goto :goto_0

    .line 1164
    :cond_2
    iget-object v0, p0, Lcom/osp/app/signin/hg;->c:Lcom/osp/app/signin/SecurityInfoChangeActivity;

    invoke-virtual {v0, v3}, Lcom/osp/app/signin/SecurityInfoChangeActivity;->b(I)V

    goto :goto_0
.end method

.method protected final d()V
    .locals 2

    .prologue
    .line 984
    invoke-super {p0}, Lcom/msc/c/b;->d()V

    .line 985
    iget-object v0, p0, Lcom/osp/app/signin/hg;->c:Lcom/osp/app/signin/SecurityInfoChangeActivity;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/osp/app/signin/SecurityInfoChangeActivity;->setResult(I)V

    .line 986
    iget-object v0, p0, Lcom/osp/app/signin/hg;->c:Lcom/osp/app/signin/SecurityInfoChangeActivity;

    invoke-virtual {v0}, Lcom/osp/app/signin/SecurityInfoChangeActivity;->finish()V

    .line 987
    return-void
.end method

.method protected final synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 926
    invoke-virtual {p0}, Lcom/osp/app/signin/hg;->a()Ljava/lang/Boolean;

    move-result-object v0

    return-object v0
.end method

.method protected final synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 926
    check-cast p1, Ljava/lang/Boolean;

    invoke-virtual {p0, p1}, Lcom/osp/app/signin/hg;->a(Ljava/lang/Boolean;)V

    return-void
.end method
