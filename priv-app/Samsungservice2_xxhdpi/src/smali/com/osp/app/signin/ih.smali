.class final Lcom/osp/app/signin/ih;
.super Ljava/lang/Object;
.source "SignInView.java"

# interfaces
.implements Landroid/widget/CompoundButton$OnCheckedChangeListener;


# instance fields
.field final synthetic a:Landroid/widget/EditText;

.field final synthetic b:Lcom/osp/app/signin/SignInView;


# direct methods
.method constructor <init>(Lcom/osp/app/signin/SignInView;Landroid/widget/EditText;)V
    .locals 0

    .prologue
    .line 2167
    iput-object p1, p0, Lcom/osp/app/signin/ih;->b:Lcom/osp/app/signin/SignInView;

    iput-object p2, p0, Lcom/osp/app/signin/ih;->a:Landroid/widget/EditText;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onCheckedChanged(Landroid/widget/CompoundButton;Z)V
    .locals 3

    .prologue
    .line 2173
    iget-object v0, p0, Lcom/osp/app/signin/ih;->b:Lcom/osp/app/signin/SignInView;

    invoke-static {v0}, Lcom/osp/app/signin/SignInView;->e(Lcom/osp/app/signin/SignInView;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 2178
    iget-object v0, p0, Lcom/osp/app/signin/ih;->b:Lcom/osp/app/signin/SignInView;

    invoke-static {v0}, Lcom/osp/app/signin/SignInView;->s(Lcom/osp/app/signin/SignInView;)Landroid/view/View;

    move-result-object v0

    const v1, 0x7f0c00b7

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/CheckBox;

    .line 2183
    :goto_0
    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/CheckBox;->playSoundEffect(I)V

    .line 2185
    const/4 v0, 0x1

    if-ne p2, v0, :cond_2

    .line 2187
    iget-object v0, p0, Lcom/osp/app/signin/ih;->a:Landroid/widget/EditText;

    invoke-virtual {v0}, Landroid/widget/EditText;->getSelectionEnd()I

    move-result v0

    .line 2188
    iget-object v1, p0, Lcom/osp/app/signin/ih;->a:Landroid/widget/EditText;

    const/16 v2, 0x91

    invoke-virtual {v1, v2}, Landroid/widget/EditText;->setInputType(I)V

    .line 2189
    if-ltz v0, :cond_0

    .line 2191
    iget-object v1, p0, Lcom/osp/app/signin/ih;->a:Landroid/widget/EditText;

    invoke-virtual {v1, v0}, Landroid/widget/EditText;->setSelection(I)V

    .line 2202
    :cond_0
    :goto_1
    return-void

    .line 2181
    :cond_1
    iget-object v0, p0, Lcom/osp/app/signin/ih;->b:Lcom/osp/app/signin/SignInView;

    const v1, 0x7f0c0125

    invoke-virtual {v0, v1}, Lcom/osp/app/signin/SignInView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/CheckBox;

    goto :goto_0

    .line 2195
    :cond_2
    iget-object v0, p0, Lcom/osp/app/signin/ih;->a:Landroid/widget/EditText;

    invoke-virtual {v0}, Landroid/widget/EditText;->getSelectionEnd()I

    move-result v0

    .line 2196
    iget-object v1, p0, Lcom/osp/app/signin/ih;->a:Landroid/widget/EditText;

    const/16 v2, 0x81

    invoke-virtual {v1, v2}, Landroid/widget/EditText;->setInputType(I)V

    .line 2197
    if-ltz v0, :cond_0

    .line 2199
    iget-object v1, p0, Lcom/osp/app/signin/ih;->a:Landroid/widget/EditText;

    invoke-virtual {v1, v0}, Landroid/widget/EditText;->setSelection(I)V

    goto :goto_1
.end method
