.class final Lcom/osp/app/signin/jr;
.super Lcom/msc/c/b;
.source "SignInView.java"


# instance fields
.field private A:Z

.field protected c:Ljava/lang/Runnable;

.field protected d:Ljava/lang/Runnable;

.field protected e:Ljava/lang/Runnable;

.field protected f:Ljava/lang/Thread;

.field protected g:Ljava/lang/Thread;

.field protected h:Ljava/lang/Thread;

.field protected i:Lcom/osp/app/signin/jz;

.field final synthetic j:Lcom/osp/app/signin/SignInView;

.field private k:Z

.field private l:Z

.field private m:Z

.field private n:Z

.field private o:Z

.field private p:Z

.field private q:Ljava/lang/String;

.field private r:J

.field private s:Z

.field private t:J

.field private u:J

.field private v:J

.field private w:Z

.field private x:J

.field private y:Z

.field private z:Z


# direct methods
.method public constructor <init>(Lcom/osp/app/signin/SignInView;)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 6373
    iput-object p1, p0, Lcom/osp/app/signin/jr;->j:Lcom/osp/app/signin/SignInView;

    .line 6374
    const v0, 0x7f090055

    invoke-direct {p0, p1, v0}, Lcom/msc/c/b;-><init>(Landroid/content/Context;I)V

    .line 6287
    iput-boolean v1, p0, Lcom/osp/app/signin/jr;->k:Z

    .line 6292
    iput-boolean v1, p0, Lcom/osp/app/signin/jr;->l:Z

    .line 6297
    iput-boolean v1, p0, Lcom/osp/app/signin/jr;->m:Z

    .line 6302
    iput-boolean v1, p0, Lcom/osp/app/signin/jr;->n:Z

    .line 6307
    iput-boolean v1, p0, Lcom/osp/app/signin/jr;->o:Z

    .line 6312
    iput-boolean v1, p0, Lcom/osp/app/signin/jr;->p:Z

    .line 6322
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/osp/app/signin/jr;->i:Lcom/osp/app/signin/jz;

    .line 6365
    iput-boolean v1, p0, Lcom/osp/app/signin/jr;->y:Z

    .line 6367
    iput-boolean v1, p0, Lcom/osp/app/signin/jr;->z:Z

    .line 6368
    iput-boolean v1, p0, Lcom/osp/app/signin/jr;->A:Z

    .line 6375
    invoke-static {p1}, Lcom/osp/app/signin/SignInView;->l(Lcom/osp/app/signin/SignInView;)Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 6377
    invoke-static {p1}, Lcom/osp/app/signin/SignInView;->l(Lcom/osp/app/signin/SignInView;)Ljava/lang/String;

    move-result-object v0

    const-string v1, "ACCOUNT_VERIFY"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-static {p1}, Lcom/osp/app/util/ad;->d(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 6379
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/osp/app/signin/jr;->n:Z

    .line 6382
    :cond_0
    new-instance v0, Lcom/osp/app/signin/js;

    invoke-direct {v0, p0, p1}, Lcom/osp/app/signin/js;-><init>(Lcom/osp/app/signin/jr;Lcom/osp/app/signin/SignInView;)V

    iput-object v0, p0, Lcom/osp/app/signin/jr;->c:Ljava/lang/Runnable;

    .line 6438
    new-instance v0, Lcom/osp/app/signin/jt;

    invoke-direct {v0, p0, p1}, Lcom/osp/app/signin/jt;-><init>(Lcom/osp/app/signin/jr;Lcom/osp/app/signin/SignInView;)V

    iput-object v0, p0, Lcom/osp/app/signin/jr;->d:Ljava/lang/Runnable;

    .line 6468
    new-instance v0, Lcom/osp/app/signin/ju;

    invoke-direct {v0, p0, p1}, Lcom/osp/app/signin/ju;-><init>(Lcom/osp/app/signin/jr;Lcom/osp/app/signin/SignInView;)V

    iput-object v0, p0, Lcom/osp/app/signin/jr;->e:Ljava/lang/Runnable;

    .line 6478
    new-instance v0, Ljava/lang/Thread;

    iget-object v1, p0, Lcom/osp/app/signin/jr;->c:Ljava/lang/Runnable;

    invoke-direct {v0, v1}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;)V

    iput-object v0, p0, Lcom/osp/app/signin/jr;->f:Ljava/lang/Thread;

    .line 6479
    new-instance v0, Ljava/lang/Thread;

    iget-object v1, p0, Lcom/osp/app/signin/jr;->d:Ljava/lang/Runnable;

    invoke-direct {v0, v1}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;)V

    iput-object v0, p0, Lcom/osp/app/signin/jr;->g:Ljava/lang/Thread;

    .line 6480
    new-instance v0, Ljava/lang/Thread;

    iget-object v1, p0, Lcom/osp/app/signin/jr;->e:Ljava/lang/Runnable;

    invoke-direct {v0, v1}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;)V

    iput-object v0, p0, Lcom/osp/app/signin/jr;->h:Ljava/lang/Thread;

    .line 6484
    return-void
.end method

.method static synthetic a(Lcom/osp/app/signin/jr;Lcom/msc/c/f;)Lcom/msc/c/f;
    .locals 0

    .prologue
    .line 6251
    iput-object p1, p0, Lcom/osp/app/signin/jr;->b:Lcom/msc/c/f;

    return-object p1
.end method

.method private static a(Ljava/lang/Thread;)V
    .locals 2

    .prologue
    .line 6512
    if-eqz p0, :cond_0

    invoke-virtual {p0}, Ljava/lang/Thread;->isAlive()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 6516
    const-wide/16 v0, 0x12c

    :try_start_0
    invoke-virtual {p0, v0, v1}, Ljava/lang/Thread;->join(J)V
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0

    .line 6521
    :cond_0
    :goto_0
    return-void

    .line 6517
    :catch_0
    move-exception v0

    invoke-virtual {v0}, Ljava/lang/InterruptedException;->printStackTrace()V

    goto :goto_0
.end method

.method static synthetic a(Lcom/osp/app/signin/jr;)Z
    .locals 1

    .prologue
    .line 6251
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/osp/app/signin/jr;->p:Z

    return v0
.end method

.method static synthetic b(Lcom/osp/app/signin/jr;Lcom/msc/c/f;)Lcom/msc/c/f;
    .locals 0

    .prologue
    .line 6251
    iput-object p1, p0, Lcom/osp/app/signin/jr;->b:Lcom/msc/c/f;

    return-object p1
.end method

.method static synthetic b(Lcom/osp/app/signin/jr;)V
    .locals 3

    .prologue
    .line 6251
    iget-object v0, p0, Lcom/osp/app/signin/jr;->j:Lcom/osp/app/signin/SignInView;

    invoke-static {v0}, Lcom/osp/app/signin/SignInView;->e(Lcom/osp/app/signin/SignInView;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/osp/app/signin/jr;->j:Lcom/osp/app/signin/SignInView;

    invoke-static {v0}, Lcom/osp/app/signin/SignInView;->s(Lcom/osp/app/signin/SignInView;)Landroid/view/View;

    move-result-object v0

    const v1, 0x7f0c0129

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/EditText;

    iget-object v1, p0, Lcom/osp/app/signin/jr;->j:Lcom/osp/app/signin/SignInView;

    invoke-static {v1}, Lcom/osp/app/signin/SignInView;->s(Lcom/osp/app/signin/SignInView;)Landroid/view/View;

    move-result-object v1

    const v2, 0x7f0c012a

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/EditText;

    :goto_0
    invoke-static {}, Lcom/msc/c/g;->a()Lcom/msc/c/g;

    iget-object v2, p0, Lcom/osp/app/signin/jr;->j:Lcom/osp/app/signin/SignInView;

    invoke-virtual {v0}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v2, v0, v1, p0}, Lcom/msc/c/g;->b(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Lcom/msc/b/h;)J

    move-result-wide v0

    iput-wide v0, p0, Lcom/osp/app/signin/jr;->u:J

    iget-wide v0, p0, Lcom/osp/app/signin/jr;->u:J

    invoke-virtual {p0, v0, v1}, Lcom/osp/app/signin/jr;->a(J)V

    iget-wide v0, p0, Lcom/osp/app/signin/jr;->u:J

    const-string v2, "verify_xml"

    invoke-virtual {p0, v0, v1, v2}, Lcom/osp/app/signin/jr;->a(JLjava/lang/String;)V

    iget-wide v0, p0, Lcom/osp/app/signin/jr;->u:J

    sget-object v2, Lcom/msc/b/g;->c:Lcom/msc/b/g;

    invoke-static {v0, v1, v2}, Lcom/msc/c/g;->a(JLcom/msc/b/g;)V

    return-void

    :cond_0
    iget-object v0, p0, Lcom/osp/app/signin/jr;->j:Lcom/osp/app/signin/SignInView;

    const v1, 0x7f0c0121

    invoke-virtual {v0, v1}, Lcom/osp/app/signin/SignInView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/EditText;

    iget-object v1, p0, Lcom/osp/app/signin/jr;->j:Lcom/osp/app/signin/SignInView;

    const v2, 0x7f0c0122

    invoke-virtual {v1, v2}, Lcom/osp/app/signin/SignInView;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/EditText;

    goto :goto_0
.end method

.method private b(Z)V
    .locals 5

    .prologue
    .line 6939
    iget-object v0, p0, Lcom/osp/app/signin/jr;->j:Lcom/osp/app/signin/SignInView;

    invoke-static {v0}, Lcom/osp/app/signin/SignInView;->e(Lcom/osp/app/signin/SignInView;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 6944
    iget-object v0, p0, Lcom/osp/app/signin/jr;->j:Lcom/osp/app/signin/SignInView;

    invoke-static {v0}, Lcom/osp/app/signin/SignInView;->s(Lcom/osp/app/signin/SignInView;)Landroid/view/View;

    move-result-object v0

    const v1, 0x7f0c0129

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/EditText;

    .line 6945
    iget-object v1, p0, Lcom/osp/app/signin/jr;->j:Lcom/osp/app/signin/SignInView;

    invoke-static {v1}, Lcom/osp/app/signin/SignInView;->s(Lcom/osp/app/signin/SignInView;)Landroid/view/View;

    move-result-object v1

    const v2, 0x7f0c012a

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/EditText;

    .line 6952
    :goto_0
    iput-boolean p1, p0, Lcom/osp/app/signin/jr;->s:Z

    .line 6953
    invoke-static {}, Lcom/msc/c/g;->a()Lcom/msc/c/g;

    .line 6955
    const/4 v2, 0x0

    .line 6956
    invoke-static {}, Lcom/osp/device/b;->b()Lcom/osp/device/b;

    move-result-object v3

    invoke-virtual {v3}, Lcom/osp/device/b;->a()Z

    move-result v3

    if-eqz v3, :cond_0

    .line 6958
    invoke-virtual {v0}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Landroid/telephony/PhoneNumberUtils;->isGlobalPhoneNumber(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 6960
    new-instance v2, Lcom/osp/app/signin/cn;

    invoke-direct {v2}, Lcom/osp/app/signin/cn;-><init>()V

    .line 6961
    invoke-virtual {v2}, Lcom/osp/app/signin/cn;->a()V

    .line 6965
    :cond_0
    iget-object v3, p0, Lcom/osp/app/signin/jr;->j:Lcom/osp/app/signin/SignInView;

    invoke-virtual {v0}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    sget-object v4, Ljava/util/Locale;->ENGLISH:Ljava/util/Locale;

    invoke-virtual {v0, v4}, Ljava/lang/String;->toLowerCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v3, v0, v1, v2, p0}, Lcom/msc/c/g;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Lcom/osp/app/signin/cn;Lcom/msc/b/h;)J

    move-result-wide v0

    iput-wide v0, p0, Lcom/osp/app/signin/jr;->r:J

    .line 6968
    iget-wide v0, p0, Lcom/osp/app/signin/jr;->r:J

    invoke-virtual {p0, v0, v1}, Lcom/osp/app/signin/jr;->b(J)V

    .line 6969
    iget-wide v0, p0, Lcom/osp/app/signin/jr;->r:J

    const-string v2, "from_json"

    invoke-virtual {p0, v0, v1, v2}, Lcom/osp/app/signin/jr;->a(JLjava/lang/String;)V

    .line 6971
    iget-wide v0, p0, Lcom/osp/app/signin/jr;->r:J

    sget-object v2, Lcom/msc/b/g;->c:Lcom/msc/b/g;

    invoke-static {v0, v1, v2}, Lcom/msc/c/g;->a(JLcom/msc/b/g;)V

    .line 6972
    return-void

    .line 6948
    :cond_1
    iget-object v0, p0, Lcom/osp/app/signin/jr;->j:Lcom/osp/app/signin/SignInView;

    const v1, 0x7f0c0121

    invoke-virtual {v0, v1}, Lcom/osp/app/signin/SignInView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/EditText;

    .line 6949
    iget-object v1, p0, Lcom/osp/app/signin/jr;->j:Lcom/osp/app/signin/SignInView;

    const v2, 0x7f0c0122

    invoke-virtual {v1, v2}, Lcom/osp/app/signin/SignInView;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/EditText;

    goto :goto_0
.end method

.method static synthetic c(Lcom/osp/app/signin/jr;Lcom/msc/c/f;)Lcom/msc/c/f;
    .locals 0

    .prologue
    .line 6251
    iput-object p1, p0, Lcom/osp/app/signin/jr;->b:Lcom/msc/c/f;

    return-object p1
.end method

.method private c(Ljava/lang/String;)V
    .locals 10

    .prologue
    const/16 v9, 0x9

    const/4 v8, 0x4

    const/4 v2, 0x1

    const/4 v3, 0x0

    .line 7043
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v0, "SIV"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v4, "SignInDual_End parsedText=["

    invoke-direct {v1, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v4, "]"

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    const-string v4, "START"

    invoke-static {v0, v1, v4}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 7045
    const-string v0, "Success"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_c

    .line 7049
    iget-object v0, p0, Lcom/osp/app/signin/jr;->j:Lcom/osp/app/signin/SignInView;

    invoke-static {v0}, Lcom/osp/app/signin/SignInView;->e(Lcom/osp/app/signin/SignInView;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 7052
    iget-object v0, p0, Lcom/osp/app/signin/jr;->j:Lcom/osp/app/signin/SignInView;

    invoke-static {v0}, Lcom/osp/app/signin/SignInView;->s(Lcom/osp/app/signin/SignInView;)Landroid/view/View;

    move-result-object v0

    const v1, 0x7f0c0129

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/EditText;

    .line 7059
    :goto_0
    iget-object v1, p0, Lcom/osp/app/signin/jr;->j:Lcom/osp/app/signin/SignInView;

    invoke-static {v1}, Landroid/preference/PreferenceManager;->getDefaultSharedPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v1

    invoke-interface {v1}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v1

    .line 7060
    const-string v4, "FLAG_DO_NOT_SHOW_NOTIFICATION"

    invoke-interface {v1, v4, v3}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    .line 7061
    invoke-interface {v1}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 7063
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v1, "===========================================SignInDual_End PROCESSING_SUCCESS==========================================="

    invoke-static {v1}, Lcom/osp/app/util/aq;->c(Ljava/lang/String;)V

    .line 7064
    iget-object v1, p0, Lcom/osp/app/signin/jr;->j:Lcom/osp/app/signin/SignInView;

    invoke-static {v1}, Lcom/osp/app/util/ad;->d(Landroid/content/Context;)Z

    move-result v1

    if-nez v1, :cond_1

    .line 7066
    iget-object v1, p0, Lcom/osp/app/signin/jr;->j:Lcom/osp/app/signin/SignInView;

    invoke-virtual {v0}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v4

    sget-object v5, Ljava/util/Locale;->ENGLISH:Ljava/util/Locale;

    invoke-virtual {v4, v5}, Ljava/lang/String;->toLowerCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v4

    invoke-static {v1, v4}, Lcom/osp/app/util/ad;->a(Landroid/content/Context;Ljava/lang/String;)Z

    move-result v1

    .line 7085
    :goto_1
    if-nez v1, :cond_2

    .line 7087
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v0, "SIV"

    const-string v1, "SigninView SignInDualEnd"

    invoke-static {v0, v1}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 7088
    new-instance v0, Lcom/msc/c/f;

    const-string v1, "ERR_MDM_SECURITY"

    const-string v2, "fail_MDM_Security"

    invoke-direct {v0, v1, v2}, Lcom/msc/c/f;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    iput-object v0, p0, Lcom/osp/app/signin/jr;->b:Lcom/msc/c/f;

    .line 7089
    iget-object v0, p0, Lcom/osp/app/signin/jr;->j:Lcom/osp/app/signin/SignInView;

    const/16 v1, 0x15

    invoke-virtual {v0, v1}, Lcom/osp/app/signin/SignInView;->b(I)V

    .line 7090
    const-string v0, "Fail"

    invoke-direct {p0, v0}, Lcom/osp/app/signin/jr;->c(Ljava/lang/String;)V

    .line 7091
    iget-object v0, p0, Lcom/osp/app/signin/jr;->j:Lcom/osp/app/signin/SignInView;

    invoke-virtual {v0}, Lcom/osp/app/signin/SignInView;->finish()V

    .line 7259
    :goto_2
    return-void

    .line 7055
    :cond_0
    iget-object v0, p0, Lcom/osp/app/signin/jr;->j:Lcom/osp/app/signin/SignInView;

    const v1, 0x7f0c0121

    invoke-virtual {v0, v1}, Lcom/osp/app/signin/SignInView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/EditText;

    goto :goto_0

    .line 7069
    :cond_1
    iget-object v1, p0, Lcom/osp/app/signin/jr;->j:Lcom/osp/app/signin/SignInView;

    invoke-static {v1}, Lcom/osp/app/signin/SignInView;->U(Lcom/osp/app/signin/SignInView;)Z

    move-result v1

    if-eqz v1, :cond_13

    .line 7071
    iget-object v1, p0, Lcom/osp/app/signin/jr;->j:Lcom/osp/app/signin/SignInView;

    invoke-virtual {v0}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v4

    sget-object v5, Ljava/util/Locale;->ENGLISH:Ljava/util/Locale;

    invoke-virtual {v4, v5}, Ljava/lang/String;->toLowerCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v4

    invoke-static {v1, v4}, Lcom/osp/app/util/ad;->b(Landroid/content/Context;Ljava/lang/String;)Z

    move-result v1

    .line 7075
    :goto_3
    iget-object v4, p0, Lcom/osp/app/signin/jr;->j:Lcom/osp/app/signin/SignInView;

    invoke-static {v4}, Lcom/osp/app/util/ad;->j(Landroid/content/Context;)V

    goto :goto_1

    .line 7097
    :cond_2
    iget-object v1, p0, Lcom/osp/app/signin/jr;->j:Lcom/osp/app/signin/SignInView;

    iget-object v4, p0, Lcom/osp/app/signin/jr;->j:Lcom/osp/app/signin/SignInView;

    invoke-static {v4}, Lcom/osp/app/signin/SignInView;->x(Lcom/osp/app/signin/SignInView;)Ljava/lang/String;

    move-result-object v4

    invoke-static {v1, v4}, Lcom/msc/c/e;->e(Landroid/content/Context;Ljava/lang/String;)V

    .line 7098
    iget-object v1, p0, Lcom/osp/app/signin/jr;->j:Lcom/osp/app/signin/SignInView;

    iget-object v4, p0, Lcom/osp/app/signin/jr;->j:Lcom/osp/app/signin/SignInView;

    invoke-static {v4}, Lcom/msc/c/c;->d(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v0}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v5

    sget-object v6, Ljava/util/Locale;->ENGLISH:Ljava/util/Locale;

    invoke-virtual {v5, v6}, Ljava/lang/String;->toLowerCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v5

    iget-object v6, p0, Lcom/osp/app/signin/jr;->j:Lcom/osp/app/signin/SignInView;

    invoke-static {v6}, Lcom/msc/c/c;->a(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v6

    invoke-static {v1, v4, v5, v6}, Lcom/msc/c/e;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 7103
    iget-object v1, p0, Lcom/osp/app/signin/jr;->j:Lcom/osp/app/signin/SignInView;

    invoke-static {v1}, Lcom/osp/app/signin/SignInView;->V(Lcom/osp/app/signin/SignInView;)V

    .line 7105
    iget-object v1, p0, Lcom/osp/app/signin/jr;->j:Lcom/osp/app/signin/SignInView;

    invoke-static {v1}, Lcom/osp/app/signin/SignInView;->K(Lcom/osp/app/signin/SignInView;)Lcom/msc/a/g;

    move-result-object v1

    invoke-virtual {v1}, Lcom/msc/a/g;->h()Z

    move-result v1

    if-eqz v1, :cond_9

    .line 7107
    invoke-static {}, Lcom/osp/app/signin/SamsungService;->a()Z

    move-result v1

    if-eqz v1, :cond_8

    .line 7113
    iget-object v0, p0, Lcom/osp/app/signin/jr;->j:Lcom/osp/app/signin/SignInView;

    invoke-static {v0, v3}, Lcom/osp/app/signin/SignInView;->c(Lcom/osp/app/signin/SignInView;Z)V

    .line 7135
    :goto_4
    invoke-static {}, Lcom/osp/app/signin/SamsungService;->p()Z

    move-result v0

    if-eqz v0, :cond_6

    .line 7138
    iget-object v0, p0, Lcom/osp/app/signin/jr;->j:Lcom/osp/app/signin/SignInView;

    invoke-static {v0}, Lcom/osp/app/signin/SignInView;->K(Lcom/osp/app/signin/SignInView;)Lcom/msc/a/g;

    move-result-object v0

    if-eqz v0, :cond_4

    .line 7140
    iget-object v1, p0, Lcom/osp/app/signin/jr;->j:Lcom/osp/app/signin/SignInView;

    iget-object v0, p0, Lcom/osp/app/signin/jr;->j:Lcom/osp/app/signin/SignInView;

    invoke-static {v0}, Lcom/osp/app/signin/SignInView;->K(Lcom/osp/app/signin/SignInView;)Lcom/msc/a/g;

    move-result-object v0

    invoke-virtual {v0}, Lcom/msc/a/g;->e()Z

    move-result v0

    if-nez v0, :cond_3

    iget-object v0, p0, Lcom/osp/app/signin/jr;->j:Lcom/osp/app/signin/SignInView;

    invoke-static {v0}, Lcom/osp/app/signin/SignInView;->K(Lcom/osp/app/signin/SignInView;)Lcom/msc/a/g;

    move-result-object v0

    invoke-virtual {v0}, Lcom/msc/a/g;->d()Z

    move-result v0

    if-eqz v0, :cond_b

    :cond_3
    move v0, v2

    :goto_5
    iget-object v4, p0, Lcom/osp/app/signin/jr;->j:Lcom/osp/app/signin/SignInView;

    invoke-static {v4}, Lcom/osp/app/signin/SignInView;->K(Lcom/osp/app/signin/SignInView;)Lcom/msc/a/g;

    move-result-object v4

    invoke-virtual {v4}, Lcom/msc/a/g;->g()Z

    move-result v4

    iget-object v5, p0, Lcom/osp/app/signin/jr;->j:Lcom/osp/app/signin/SignInView;

    invoke-static {v5}, Lcom/osp/app/signin/SignInView;->K(Lcom/osp/app/signin/SignInView;)Lcom/msc/a/g;

    move-result-object v5

    invoke-virtual {v5}, Lcom/msc/a/g;->h()Z

    move-result v5

    invoke-static {v1, v0, v4, v5}, Lcom/msc/openprovider/b;->a(Landroid/content/Context;ZZZ)V

    .line 7143
    :cond_4
    iget-object v0, p0, Lcom/osp/app/signin/jr;->j:Lcom/osp/app/signin/SignInView;

    invoke-static {v0}, Lcom/osp/app/signin/SignInView;->Y(Lcom/osp/app/signin/SignInView;)Lcom/osp/app/signin/kd;

    move-result-object v0

    if-eqz v0, :cond_6

    .line 7145
    iget-object v0, p0, Lcom/osp/app/signin/jr;->j:Lcom/osp/app/signin/SignInView;

    iget-object v1, p0, Lcom/osp/app/signin/jr;->j:Lcom/osp/app/signin/SignInView;

    invoke-static {v1}, Lcom/osp/app/signin/SignInView;->j(Lcom/osp/app/signin/SignInView;)Ljava/lang/String;

    move-result-object v1

    iget-object v4, p0, Lcom/osp/app/signin/jr;->j:Lcom/osp/app/signin/SignInView;

    invoke-static {v4}, Lcom/osp/app/signin/SignInView;->Y(Lcom/osp/app/signin/SignInView;)Lcom/osp/app/signin/kd;

    move-result-object v4

    invoke-virtual {v4}, Lcom/osp/app/signin/kd;->b()I

    move-result v4

    if-eqz v4, :cond_5

    move v3, v2

    :cond_5
    invoke-static {v0, v1, v3}, Lcom/msc/openprovider/b;->a(Landroid/content/Context;Ljava/lang/String;Z)V

    .line 7150
    :cond_6
    invoke-static {}, Lcom/osp/app/pushmarketing/k;->a()Lcom/osp/app/pushmarketing/k;

    iget-object v0, p0, Lcom/osp/app/signin/jr;->j:Lcom/osp/app/signin/SignInView;

    invoke-static {v0}, Lcom/osp/app/pushmarketing/k;->c(Landroid/content/Context;)V

    .line 7156
    new-instance v0, Ljava/util/LinkedList;

    invoke-direct {v0}, Ljava/util/LinkedList;-><init>()V

    .line 7157
    invoke-static {}, Lcom/osp/app/bigdatalog/e;->a()Lcom/osp/app/bigdatalog/e;

    move-result-object v1

    iget-object v3, p0, Lcom/osp/app/signin/jr;->j:Lcom/osp/app/signin/SignInView;

    invoke-virtual {v3}, Lcom/osp/app/signin/SignInView;->getApplicationContext()Landroid/content/Context;

    move-result-object v3

    invoke-virtual {v1, v3}, Lcom/osp/app/bigdatalog/e;->a(Landroid/content/Context;)V

    .line 7159
    iget-object v1, p0, Lcom/osp/app/signin/jr;->j:Lcom/osp/app/signin/SignInView;

    invoke-static {v1}, Lcom/osp/app/signin/SignInView;->c(Lcom/osp/app/signin/SignInView;)Landroid/content/Intent;

    move-result-object v1

    const-string v3, "signin_duration"

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v4

    iget-object v6, p0, Lcom/osp/app/signin/jr;->j:Lcom/osp/app/signin/SignInView;

    invoke-static {v6}, Lcom/osp/app/signin/SignInView;->B(Lcom/osp/app/signin/SignInView;)J

    move-result-wide v6

    sub-long/2addr v4, v6

    invoke-virtual {v1, v3, v4, v5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;J)Landroid/content/Intent;

    .line 7160
    iget-object v1, p0, Lcom/osp/app/signin/jr;->j:Lcom/osp/app/signin/SignInView;

    invoke-static {v1}, Lcom/osp/app/signin/SignInView;->c(Lcom/osp/app/signin/SignInView;)Landroid/content/Intent;

    move-result-object v1

    const-string v3, "signin_status"

    invoke-virtual {v1, v3, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 7162
    new-instance v1, Lcom/osp/app/bigdatalog/c;

    iget-object v3, p0, Lcom/osp/app/signin/jr;->j:Lcom/osp/app/signin/SignInView;

    invoke-virtual {v3}, Lcom/osp/app/signin/SignInView;->getApplicationContext()Landroid/content/Context;

    move-result-object v3

    iget-object v4, p0, Lcom/osp/app/signin/jr;->j:Lcom/osp/app/signin/SignInView;

    invoke-static {v4}, Lcom/osp/app/signin/SignInView;->c(Lcom/osp/app/signin/SignInView;)Landroid/content/Intent;

    move-result-object v4

    invoke-direct {v1, v3, v8, v9, v4}, Lcom/osp/app/bigdatalog/c;-><init>(Landroid/content/Context;IILandroid/content/Intent;)V

    .line 7165
    invoke-interface {v0, v1}, Ljava/util/Queue;->add(Ljava/lang/Object;)Z

    .line 7167
    iget-object v1, p0, Lcom/osp/app/signin/jr;->j:Lcom/osp/app/signin/SignInView;

    invoke-static {v1}, Lcom/osp/app/signin/SignInView;->c(Lcom/osp/app/signin/SignInView;)Landroid/content/Intent;

    move-result-object v1

    const-string v3, "stay_duration"

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v4

    iget-object v6, p0, Lcom/osp/app/signin/jr;->j:Lcom/osp/app/signin/SignInView;

    invoke-static {v6}, Lcom/osp/app/signin/SignInView;->Z(Lcom/osp/app/signin/SignInView;)J

    move-result-wide v6

    sub-long/2addr v4, v6

    invoke-virtual {v1, v3, v4, v5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;J)Landroid/content/Intent;

    .line 7168
    iget-object v1, p0, Lcom/osp/app/signin/jr;->j:Lcom/osp/app/signin/SignInView;

    invoke-static {v1}, Lcom/osp/app/signin/SignInView;->aa(Lcom/osp/app/signin/SignInView;)Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_7

    .line 7170
    iget-object v1, p0, Lcom/osp/app/signin/jr;->j:Lcom/osp/app/signin/SignInView;

    invoke-static {v1}, Lcom/osp/app/signin/SignInView;->c(Lcom/osp/app/signin/SignInView;)Landroid/content/Intent;

    move-result-object v1

    const-string v3, "previous_activity"

    iget-object v4, p0, Lcom/osp/app/signin/jr;->j:Lcom/osp/app/signin/SignInView;

    invoke-static {v4}, Lcom/osp/app/signin/SignInView;->aa(Lcom/osp/app/signin/SignInView;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v1, v3, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 7172
    :cond_7
    new-instance v1, Lcom/osp/app/bigdatalog/c;

    iget-object v3, p0, Lcom/osp/app/signin/jr;->j:Lcom/osp/app/signin/SignInView;

    invoke-virtual {v3}, Lcom/osp/app/signin/SignInView;->getApplicationContext()Landroid/content/Context;

    move-result-object v3

    const/4 v4, 0x2

    iget-object v5, p0, Lcom/osp/app/signin/jr;->j:Lcom/osp/app/signin/SignInView;

    invoke-static {v5}, Lcom/osp/app/signin/SignInView;->c(Lcom/osp/app/signin/SignInView;)Landroid/content/Intent;

    move-result-object v5

    invoke-direct {v1, v3, v8, v4, v5}, Lcom/osp/app/bigdatalog/c;-><init>(Landroid/content/Context;IILandroid/content/Intent;)V

    .line 7175
    invoke-interface {v0, v1}, Ljava/util/Queue;->add(Ljava/lang/Object;)Z

    .line 7177
    new-instance v1, Lcom/osp/app/bigdatalog/c;

    iget-object v3, p0, Lcom/osp/app/signin/jr;->j:Lcom/osp/app/signin/SignInView;

    invoke-virtual {v3}, Lcom/osp/app/signin/SignInView;->getApplicationContext()Landroid/content/Context;

    move-result-object v3

    iget-object v4, p0, Lcom/osp/app/signin/jr;->j:Lcom/osp/app/signin/SignInView;

    invoke-static {v4}, Lcom/osp/app/signin/SignInView;->c(Lcom/osp/app/signin/SignInView;)Landroid/content/Intent;

    move-result-object v4

    invoke-direct {v1, v3, v8, v2, v4}, Lcom/osp/app/bigdatalog/c;-><init>(Landroid/content/Context;IILandroid/content/Intent;)V

    .line 7180
    invoke-interface {v0, v1}, Ljava/util/Queue;->add(Ljava/lang/Object;)Z

    .line 7182
    invoke-static {}, Lcom/osp/app/bigdatalog/e;->a()Lcom/osp/app/bigdatalog/e;

    move-result-object v1

    iget-object v2, p0, Lcom/osp/app/signin/jr;->j:Lcom/osp/app/signin/SignInView;

    invoke-virtual {v2}, Lcom/osp/app/signin/SignInView;->getApplicationContext()Landroid/content/Context;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/osp/app/bigdatalog/e;->a(Landroid/content/Context;)V

    .line 7183
    invoke-static {}, Lcom/osp/app/bigdatalog/e;->a()Lcom/osp/app/bigdatalog/e;

    move-result-object v1

    iget-object v2, p0, Lcom/osp/app/signin/jr;->j:Lcom/osp/app/signin/SignInView;

    invoke-virtual {v2}, Lcom/osp/app/signin/SignInView;->getApplicationContext()Landroid/content/Context;

    move-result-object v2

    invoke-virtual {v1, v2, v0}, Lcom/osp/app/bigdatalog/e;->a(Landroid/content/Context;Ljava/util/Queue;)V

    .line 7258
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v0, "SIV"

    const-string v1, "SignInDual_End"

    const-string v2, "END"

    invoke-static {v0, v1, v2}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_2

    .line 7116
    :cond_8
    iget-object v1, p0, Lcom/osp/app/signin/jr;->j:Lcom/osp/app/signin/SignInView;

    invoke-virtual {v0}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    sget-object v4, Ljava/util/Locale;->ENGLISH:Ljava/util/Locale;

    invoke-virtual {v0, v4}, Ljava/lang/String;->toLowerCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Lcom/osp/app/signin/SignInView;->g(Lcom/osp/app/signin/SignInView;Ljava/lang/String;)V

    goto/16 :goto_4

    .line 7120
    :cond_9
    iget-object v0, p0, Lcom/osp/app/signin/jr;->j:Lcom/osp/app/signin/SignInView;

    invoke-static {v0}, Lcom/osp/app/util/r;->p(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_a

    iget-object v0, p0, Lcom/osp/app/signin/jr;->j:Lcom/osp/app/signin/SignInView;

    invoke-static {v0}, Lcom/osp/app/signin/SignInView;->X(Lcom/osp/app/signin/SignInView;)Z

    move-result v0

    if-eqz v0, :cond_a

    invoke-static {}, Lcom/osp/app/signin/SamsungService;->a()Z

    move-result v0

    if-eqz v0, :cond_a

    .line 7122
    invoke-static {}, Lcom/osp/app/util/m;->a()Lcom/osp/app/util/m;

    iget-object v0, p0, Lcom/osp/app/signin/jr;->j:Lcom/osp/app/signin/SignInView;

    invoke-static {v0, v3}, Lcom/osp/app/util/m;->a(Landroid/app/Activity;Z)V

    .line 7129
    :goto_6
    iget-object v0, p0, Lcom/osp/app/signin/jr;->j:Lcom/osp/app/signin/SignInView;

    invoke-static {v0}, Landroid/preference/PreferenceManager;->getDefaultSharedPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    .line 7130
    const-string v1, "EMAIL_VALIDATION_KEY"

    iget-object v4, p0, Lcom/osp/app/signin/jr;->j:Lcom/osp/app/signin/SignInView;

    invoke-static {v4}, Lcom/osp/app/util/ad;->g(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v4

    invoke-interface {v0, v1, v4}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    .line 7131
    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    goto/16 :goto_4

    .line 7125
    :cond_a
    iget-object v0, p0, Lcom/osp/app/signin/jr;->j:Lcom/osp/app/signin/SignInView;

    invoke-static {v0, v2}, Lcom/osp/app/signin/SignInView;->c(Lcom/osp/app/signin/SignInView;Z)V

    goto :goto_6

    :cond_b
    move v0, v3

    .line 7140
    goto/16 :goto_5

    .line 7193
    :cond_c
    iget-object v0, p0, Lcom/osp/app/signin/jr;->j:Lcom/osp/app/signin/SignInView;

    invoke-static {v0}, Lcom/osp/app/signin/SignInView;->c(Lcom/osp/app/signin/SignInView;)Landroid/content/Intent;

    move-result-object v0

    const-string v1, "signin_duration"

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v4

    iget-object v2, p0, Lcom/osp/app/signin/jr;->j:Lcom/osp/app/signin/SignInView;

    invoke-static {v2}, Lcom/osp/app/signin/SignInView;->B(Lcom/osp/app/signin/SignInView;)J

    move-result-wide v6

    sub-long/2addr v4, v6

    invoke-virtual {v0, v1, v4, v5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;J)Landroid/content/Intent;

    .line 7194
    iget-object v0, p0, Lcom/osp/app/signin/jr;->j:Lcom/osp/app/signin/SignInView;

    invoke-static {v0}, Lcom/osp/app/signin/SignInView;->c(Lcom/osp/app/signin/SignInView;)Landroid/content/Intent;

    move-result-object v0

    const-string v1, "signin_status"

    invoke-virtual {v0, v1, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 7195
    iget-object v0, p0, Lcom/osp/app/signin/jr;->j:Lcom/osp/app/signin/SignInView;

    invoke-static {v0}, Lcom/osp/app/signin/SignInView;->c(Lcom/osp/app/signin/SignInView;)Landroid/content/Intent;

    move-result-object v1

    const-string v2, "error_code"

    iget-object v0, p0, Lcom/osp/app/signin/jr;->b:Lcom/msc/c/f;

    if-eqz v0, :cond_d

    iget-object v0, p0, Lcom/osp/app/signin/jr;->b:Lcom/msc/c/f;

    invoke-virtual {v0}, Lcom/msc/c/f;->a()Ljava/lang/String;

    move-result-object v0

    :goto_7
    invoke-virtual {v1, v2, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 7197
    new-instance v0, Lcom/osp/app/bigdatalog/c;

    iget-object v1, p0, Lcom/osp/app/signin/jr;->j:Lcom/osp/app/signin/SignInView;

    invoke-virtual {v1}, Lcom/osp/app/signin/SignInView;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    iget-object v2, p0, Lcom/osp/app/signin/jr;->j:Lcom/osp/app/signin/SignInView;

    invoke-static {v2}, Lcom/osp/app/signin/SignInView;->c(Lcom/osp/app/signin/SignInView;)Landroid/content/Intent;

    move-result-object v2

    invoke-direct {v0, v1, v8, v9, v2}, Lcom/osp/app/bigdatalog/c;-><init>(Landroid/content/Context;IILandroid/content/Intent;)V

    .line 7200
    invoke-static {}, Lcom/osp/app/bigdatalog/e;->a()Lcom/osp/app/bigdatalog/e;

    move-result-object v1

    iget-object v2, p0, Lcom/osp/app/signin/jr;->j:Lcom/osp/app/signin/SignInView;

    invoke-virtual {v2}, Lcom/osp/app/signin/SignInView;->getApplicationContext()Landroid/content/Context;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/osp/app/bigdatalog/e;->a(Landroid/content/Context;)V

    .line 7201
    invoke-static {}, Lcom/osp/app/bigdatalog/e;->a()Lcom/osp/app/bigdatalog/e;

    move-result-object v1

    iget-object v2, p0, Lcom/osp/app/signin/jr;->j:Lcom/osp/app/signin/SignInView;

    invoke-virtual {v2}, Lcom/osp/app/signin/SignInView;->getApplicationContext()Landroid/content/Context;

    move-result-object v2

    invoke-virtual {v1, v2, v0}, Lcom/osp/app/bigdatalog/e;->a(Landroid/content/Context;Lcom/osp/app/bigdatalog/c;)V

    .line 7203
    iget-object v0, p0, Lcom/osp/app/signin/jr;->j:Lcom/osp/app/signin/SignInView;

    invoke-virtual {v0}, Lcom/osp/app/signin/SignInView;->c()V

    .line 7219
    iget-object v0, p0, Lcom/osp/app/signin/jr;->j:Lcom/osp/app/signin/SignInView;

    invoke-static {v0}, Lcom/osp/app/signin/SignInView;->F(Lcom/osp/app/signin/SignInView;)Z

    move-result v0

    if-eqz v0, :cond_10

    .line 7221
    iget-object v0, p0, Lcom/osp/app/signin/jr;->j:Lcom/osp/app/signin/SignInView;

    invoke-static {v0, v3}, Lcom/osp/app/signin/SignInView;->b(Lcom/osp/app/signin/SignInView;Z)Z

    .line 7222
    iget-object v0, p0, Lcom/osp/app/signin/jr;->j:Lcom/osp/app/signin/SignInView;

    invoke-static {v0}, Lcom/osp/app/signin/SignInView;->G(Lcom/osp/app/signin/SignInView;)Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_e

    .line 7224
    iget-object v0, p0, Lcom/osp/app/signin/jr;->j:Lcom/osp/app/signin/SignInView;

    iget-object v1, p0, Lcom/osp/app/signin/jr;->j:Lcom/osp/app/signin/SignInView;

    invoke-static {v1}, Lcom/osp/app/signin/SignInView;->G(Lcom/osp/app/signin/SignInView;)Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/osp/app/signin/jr;->j:Lcom/osp/app/signin/SignInView;

    invoke-virtual {v2}, Lcom/osp/app/signin/SignInView;->getApplicationContext()Landroid/content/Context;

    iget-object v2, p0, Lcom/osp/app/signin/jr;->j:Lcom/osp/app/signin/SignInView;

    invoke-static {v2}, Lcom/osp/app/signin/SignInView;->G(Lcom/osp/app/signin/SignInView;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/osp/common/util/i;->d(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v1, v2}, Lcom/msc/c/e;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)V

    .line 7225
    iget-object v0, p0, Lcom/osp/app/signin/jr;->j:Lcom/osp/app/signin/SignInView;

    new-instance v1, Lcom/osp/app/signin/jj;

    iget-object v2, p0, Lcom/osp/app/signin/jr;->j:Lcom/osp/app/signin/SignInView;

    invoke-direct {v1, v2}, Lcom/osp/app/signin/jj;-><init>(Lcom/osp/app/signin/SignInView;)V

    invoke-static {v0, v1}, Lcom/osp/app/signin/SignInView;->a(Lcom/osp/app/signin/SignInView;Lcom/osp/app/signin/jj;)Lcom/osp/app/signin/jj;

    .line 7226
    iget-object v0, p0, Lcom/osp/app/signin/jr;->j:Lcom/osp/app/signin/SignInView;

    invoke-static {v0}, Lcom/osp/app/signin/SignInView;->H(Lcom/osp/app/signin/SignInView;)Lcom/osp/app/signin/jj;

    move-result-object v0

    invoke-static {v0}, Lcom/msc/sa/c/d;->a(Landroid/os/AsyncTask;)V

    .line 7239
    :goto_8
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v0, "SIV"

    const-string v1, "SignInDual_End"

    const-string v2, "END"

    invoke-static {v0, v1, v2}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_2

    .line 7195
    :cond_d
    const-string v0, ""

    goto/16 :goto_7

    .line 7229
    :cond_e
    invoke-static {}, Lcom/osp/device/b;->b()Lcom/osp/device/b;

    move-result-object v0

    iget-object v1, p0, Lcom/osp/app/signin/jr;->j:Lcom/osp/app/signin/SignInView;

    invoke-virtual {v0, v1}, Lcom/osp/device/b;->i(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_f

    .line 7231
    iget-object v0, p0, Lcom/osp/app/signin/jr;->j:Lcom/osp/app/signin/SignInView;

    new-instance v1, Lcom/osp/app/signin/jm;

    iget-object v2, p0, Lcom/osp/app/signin/jr;->j:Lcom/osp/app/signin/SignInView;

    invoke-direct {v1, v2}, Lcom/osp/app/signin/jm;-><init>(Lcom/osp/app/signin/SignInView;)V

    invoke-static {v0, v1}, Lcom/osp/app/signin/SignInView;->a(Lcom/osp/app/signin/SignInView;Lcom/osp/app/signin/jm;)Lcom/osp/app/signin/jm;

    .line 7232
    iget-object v0, p0, Lcom/osp/app/signin/jr;->j:Lcom/osp/app/signin/SignInView;

    invoke-static {v0}, Lcom/osp/app/signin/SignInView;->I(Lcom/osp/app/signin/SignInView;)Lcom/osp/app/signin/jm;

    move-result-object v0

    new-array v1, v3, [Ljava/lang/Void;

    invoke-virtual {v0, v1}, Lcom/osp/app/signin/jm;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    goto :goto_8

    .line 7235
    :cond_f
    iget-object v0, p0, Lcom/osp/app/signin/jr;->j:Lcom/osp/app/signin/SignInView;

    invoke-static {v0}, Lcom/osp/app/signin/SignInView;->J(Lcom/osp/app/signin/SignInView;)V

    goto :goto_8

    .line 7242
    :cond_10
    invoke-static {}, Lcom/osp/device/b;->b()Lcom/osp/device/b;

    move-result-object v0

    invoke-virtual {v0}, Lcom/osp/device/b;->a()Z

    move-result v0

    if-eqz v0, :cond_11

    .line 7244
    iget-boolean v0, p0, Lcom/osp/app/signin/jr;->y:Z

    if-nez v0, :cond_12

    .line 7246
    :cond_11
    invoke-virtual {p0, v3}, Lcom/osp/app/signin/jr;->a(Z)V

    .line 7252
    iget-object v0, p0, Lcom/osp/app/signin/jr;->j:Lcom/osp/app/signin/SignInView;

    invoke-static {v0}, Lcom/osp/app/signin/SignInView;->a(Lcom/osp/app/signin/SignInView;)V

    .line 7255
    :cond_12
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v0, "SIV"

    const-string v1, "SignInDual_End"

    const-string v2, "END"

    invoke-static {v0, v1, v2}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_2

    :cond_13
    move v1, v2

    goto/16 :goto_3
.end method

.method static synthetic c(Lcom/osp/app/signin/jr;)Z
    .locals 1

    .prologue
    .line 6251
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/osp/app/signin/jr;->k:Z

    return v0
.end method

.method static synthetic d(Lcom/osp/app/signin/jr;)Z
    .locals 1

    .prologue
    .line 6251
    iget-boolean v0, p0, Lcom/osp/app/signin/jr;->n:Z

    return v0
.end method

.method static synthetic e(Lcom/osp/app/signin/jr;)V
    .locals 1

    .prologue
    .line 6251
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/osp/app/signin/jr;->b(Z)V

    return-void
.end method

.method static synthetic f(Lcom/osp/app/signin/jr;)Z
    .locals 1

    .prologue
    .line 6251
    iget-boolean v0, p0, Lcom/osp/app/signin/jr;->s:Z

    return v0
.end method

.method static synthetic g(Lcom/osp/app/signin/jr;)Z
    .locals 1

    .prologue
    .line 6251
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/osp/app/signin/jr;->o:Z

    return v0
.end method

.method static synthetic h(Lcom/osp/app/signin/jr;)Z
    .locals 1

    .prologue
    .line 6251
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/osp/app/signin/jr;->l:Z

    return v0
.end method

.method static synthetic i(Lcom/osp/app/signin/jr;)Z
    .locals 1

    .prologue
    .line 6251
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/osp/app/signin/jr;->m:Z

    return v0
.end method


# virtual methods
.method protected final varargs a()Ljava/lang/Boolean;
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 6527
    iget-object v0, p0, Lcom/osp/app/signin/jr;->j:Lcom/osp/app/signin/SignInView;

    invoke-static {v0, v2}, Lcom/osp/app/signin/SignInView;->b(Lcom/osp/app/signin/SignInView;Z)Z

    .line 6528
    iget-object v0, p0, Lcom/osp/app/signin/jr;->j:Lcom/osp/app/signin/SignInView;

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lcom/osp/app/signin/SignInView;->d(Lcom/osp/app/signin/SignInView;Ljava/lang/String;)Ljava/lang/String;

    .line 6530
    iget-object v0, p0, Lcom/osp/app/signin/jr;->h:Ljava/lang/Thread;

    invoke-virtual {v0}, Ljava/lang/Thread;->start()V

    .line 6531
    iget-object v0, p0, Lcom/osp/app/signin/jr;->f:Ljava/lang/Thread;

    invoke-virtual {v0}, Ljava/lang/Thread;->start()V

    .line 6532
    iget-object v0, p0, Lcom/osp/app/signin/jr;->g:Ljava/lang/Thread;

    invoke-virtual {v0}, Ljava/lang/Thread;->start()V

    .line 6534
    :cond_0
    iget-boolean v0, p0, Lcom/osp/app/signin/jr;->k:Z

    if-eqz v0, :cond_1

    iget-boolean v0, p0, Lcom/osp/app/signin/jr;->l:Z

    if-eqz v0, :cond_1

    iget-boolean v0, p0, Lcom/osp/app/signin/jr;->m:Z

    if-nez v0, :cond_2

    .line 6536
    :cond_1
    invoke-virtual {p0}, Lcom/osp/app/signin/jr;->isCancelled()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 6538
    :cond_2
    iget-boolean v0, p0, Lcom/osp/app/signin/jr;->p:Z

    if-eqz v0, :cond_3

    iget-boolean v0, p0, Lcom/osp/app/signin/jr;->o:Z

    if-eqz v0, :cond_3

    .line 6544
    const-string v0, "Success"

    iput-object v0, p0, Lcom/osp/app/signin/jr;->q:Ljava/lang/String;

    .line 6549
    :goto_0
    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    return-object v0

    .line 6547
    :cond_3
    const-string v0, "Fail"

    iput-object v0, p0, Lcom/osp/app/signin/jr;->q:Ljava/lang/String;

    goto :goto_0
.end method

.method public final declared-synchronized a(Lcom/msc/b/c;)V
    .locals 8

    .prologue
    const/4 v0, 0x0

    .line 6604
    monitor-enter p0

    :try_start_0
    invoke-super {p0, p1}, Lcom/msc/c/b;->a(Lcom/msc/b/c;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 6606
    if-nez p1, :cond_1

    .line 6791
    :cond_0
    :goto_0
    monitor-exit p0

    return-void

    .line 6611
    :cond_1
    :try_start_1
    invoke-virtual {p1}, Lcom/msc/b/c;->a()J

    move-result-wide v2

    .line 6612
    invoke-virtual {p1}, Lcom/msc/b/c;->e()Ljava/lang/String;

    move-result-object v4

    .line 6614
    iget-wide v6, p0, Lcom/osp/app/signin/jr;->r:J
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    cmp-long v1, v2, v6

    if-nez v1, :cond_9

    .line 6618
    :try_start_2
    invoke-static {}, Lcom/osp/device/b;->b()Lcom/osp/device/b;

    move-result-object v1

    invoke-virtual {v1}, Lcom/osp/device/b;->a()Z

    move-result v1

    if-eqz v1, :cond_4

    .line 6620
    invoke-static {}, Lcom/msc/c/h;->a()Lcom/msc/c/h;

    const-string v1, "N"

    new-instance v2, Lorg/json/JSONObject;

    invoke-direct {v2, v4}, Lorg/json/JSONObject;-><init>(Ljava/lang/String;)V

    const-string v3, "isDuplicationID"

    invoke-virtual {v2, v3}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_11

    const-string v1, "isDuplicationID"

    invoke-virtual {v2, v1}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    move-object v2, v1

    .line 6621
    :goto_1
    invoke-static {}, Lcom/msc/c/h;->a()Lcom/msc/c/h;

    const-string v1, ""

    new-instance v3, Lorg/json/JSONObject;

    invoke-direct {v3, v4}, Lorg/json/JSONObject;-><init>(Ljava/lang/String;)V

    const-string v5, "status"

    invoke-virtual {v3, v5}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_2

    const-string v1, "status"

    invoke-virtual {v3, v1}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 6622
    :cond_2
    const-string v3, "Y"

    invoke-virtual {v3, v2}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v2

    iput-boolean v2, p0, Lcom/osp/app/signin/jr;->z:Z

    .line 6623
    const-string v2, "S"

    invoke-virtual {v2, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v1

    iput-boolean v1, p0, Lcom/osp/app/signin/jr;->A:Z

    .line 6625
    iget-boolean v1, p0, Lcom/osp/app/signin/jr;->z:Z

    if-nez v1, :cond_0

    .line 6628
    iget-boolean v1, p0, Lcom/osp/app/signin/jr;->A:Z

    if-eqz v1, :cond_4

    .line 6632
    iget-object v1, p0, Lcom/osp/app/signin/jr;->j:Lcom/osp/app/signin/SignInView;

    invoke-static {}, Lcom/msc/c/h;->a()Lcom/msc/c/h;

    invoke-static {v4}, Lcom/msc/c/h;->q(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/osp/app/signin/SignInView;->b(Lcom/osp/app/signin/SignInView;Ljava/lang/String;)Ljava/lang/String;

    .line 6633
    iget-object v1, p0, Lcom/osp/app/signin/jr;->j:Lcom/osp/app/signin/SignInView;

    invoke-static {}, Lcom/msc/c/h;->a()Lcom/msc/c/h;

    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    invoke-static {v4}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;)V

    new-instance v2, Lorg/json/JSONObject;

    invoke-direct {v2, v4}, Lorg/json/JSONObject;-><init>(Ljava/lang/String;)V

    const-string v3, "login_id"

    invoke-virtual {v2, v3}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_3

    const-string v0, "login_id"

    invoke-virtual {v2, v0}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    :cond_3
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "RequestSet::parseLoginIdFromJSON login_id : "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;)V

    invoke-static {v1, v0}, Lcom/osp/app/signin/SignInView;->e(Lcom/osp/app/signin/SignInView;Ljava/lang/String;)Ljava/lang/String;
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_0
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto/16 :goto_0

    .line 6669
    :catch_0
    move-exception v0

    .line 6671
    :try_start_3
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    .line 6672
    new-instance v1, Lcom/msc/c/f;

    invoke-direct {v1, v0}, Lcom/msc/c/f;-><init>(Ljava/lang/Exception;)V

    iput-object v1, p0, Lcom/osp/app/signin/jr;->b:Lcom/msc/c/f;
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    goto/16 :goto_0

    .line 6604
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 6638
    :cond_4
    :try_start_4
    iget-object v0, p0, Lcom/osp/app/signin/jr;->j:Lcom/osp/app/signin/SignInView;

    invoke-static {}, Lcom/msc/c/h;->a()Lcom/msc/c/h;

    invoke-static {v4}, Lcom/msc/c/h;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/osp/app/signin/SignInView;->a(Lcom/osp/app/signin/SignInView;Ljava/lang/String;)Ljava/lang/String;

    .line 6647
    iget-object v0, p0, Lcom/osp/app/signin/jr;->j:Lcom/osp/app/signin/SignInView;

    invoke-static {v0}, Lcom/osp/app/signin/SignInView;->x(Lcom/osp/app/signin/SignInView;)Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_8

    iget-object v0, p0, Lcom/osp/app/signin/jr;->j:Lcom/osp/app/signin/SignInView;

    invoke-static {v0}, Lcom/osp/app/signin/SignInView;->x(Lcom/osp/app/signin/SignInView;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v0

    if-lez v0, :cond_8

    .line 6650
    iget-boolean v0, p0, Lcom/osp/app/signin/jr;->n:Z

    if-eqz v0, :cond_5

    .line 6652
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/osp/app/signin/jr;->o:Z

    goto/16 :goto_0

    .line 6653
    :cond_5
    const-string v0, "OSP_02"

    iget-object v1, p0, Lcom/osp/app/signin/jr;->j:Lcom/osp/app/signin/SignInView;

    invoke-static {v1}, Lcom/osp/app/signin/SignInView;->n(Lcom/osp/app/signin/SignInView;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_7

    iget-object v0, p0, Lcom/osp/app/signin/jr;->j:Lcom/osp/app/signin/SignInView;

    invoke-static {v0}, Lcom/osp/app/signin/SignInView;->j(Lcom/osp/app/signin/SignInView;)Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_7

    iget-object v0, p0, Lcom/osp/app/signin/jr;->j:Lcom/osp/app/signin/SignInView;

    invoke-static {v0}, Lcom/osp/app/signin/SignInView;->P(Lcom/osp/app/signin/SignInView;)Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_6

    const-string v0, "BG_mode"

    iget-object v1, p0, Lcom/osp/app/signin/jr;->j:Lcom/osp/app/signin/SignInView;

    invoke-static {v1}, Lcom/osp/app/signin/SignInView;->i(Lcom/osp/app/signin/SignInView;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_7

    .line 6655
    :cond_6
    invoke-virtual {p0}, Lcom/osp/app/signin/jr;->isCancelled()Z

    move-result v0

    if-nez v0, :cond_0

    .line 6659
    iget-boolean v0, p0, Lcom/osp/app/signin/jr;->s:Z

    iget-object v0, p0, Lcom/osp/app/signin/jr;->j:Lcom/osp/app/signin/SignInView;

    invoke-static {v0}, Lcom/osp/app/signin/SignInView;->x(Lcom/osp/app/signin/SignInView;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/osp/app/signin/jr;->b(Ljava/lang/String;)V

    goto/16 :goto_0

    .line 6662
    :cond_7
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/osp/app/signin/jr;->o:Z

    goto/16 :goto_0

    .line 6666
    :cond_8
    new-instance v0, Lcom/msc/c/f;

    const-string v1, "ERR_0000"

    const-string v2, "fail"

    invoke-direct {v0, v1, v2}, Lcom/msc/c/f;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    iput-object v0, p0, Lcom/osp/app/signin/jr;->b:Lcom/msc/c/f;
    :try_end_4
    .catch Ljava/lang/Exception; {:try_start_4 .. :try_end_4} :catch_0
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    goto/16 :goto_0

    .line 6701
    :cond_9
    :try_start_5
    iget-wide v0, p0, Lcom/osp/app/signin/jr;->t:J
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    cmp-long v0, v2, v0

    if-nez v0, :cond_a

    .line 6706
    :try_start_6
    iget-object v0, p0, Lcom/osp/app/signin/jr;->j:Lcom/osp/app/signin/SignInView;

    invoke-static {}, Lcom/msc/c/h;->a()Lcom/msc/c/h;

    move-result-object v1

    iget-object v2, p0, Lcom/osp/app/signin/jr;->j:Lcom/osp/app/signin/SignInView;

    iget-object v3, p0, Lcom/osp/app/signin/jr;->j:Lcom/osp/app/signin/SignInView;

    invoke-static {v3}, Lcom/osp/app/signin/SignInView;->j(Lcom/osp/app/signin/SignInView;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3, v4}, Lcom/msc/c/h;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)Lcom/osp/app/signin/kd;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/osp/app/signin/SignInView;->a(Lcom/osp/app/signin/SignInView;Lcom/osp/app/signin/kd;)Lcom/osp/app/signin/kd;
    :try_end_6
    .catch Ljava/lang/Exception; {:try_start_6 .. :try_end_6} :catch_1
    .catchall {:try_start_6 .. :try_end_6} :catchall_0

    goto/16 :goto_0

    .line 6708
    :catch_1
    move-exception v0

    :try_start_7
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    goto/16 :goto_0

    .line 6712
    :cond_a
    iget-wide v0, p0, Lcom/osp/app/signin/jr;->u:J
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_0

    cmp-long v0, v2, v0

    if-nez v0, :cond_d

    .line 6716
    :try_start_8
    iget-object v0, p0, Lcom/osp/app/signin/jr;->j:Lcom/osp/app/signin/SignInView;

    invoke-static {}, Lcom/msc/c/h;->a()Lcom/msc/c/h;

    invoke-static {v4}, Lcom/msc/c/h;->e(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/osp/app/signin/SignInView;->b(Lcom/osp/app/signin/SignInView;Ljava/lang/String;)Ljava/lang/String;

    .line 6717
    iget-object v0, p0, Lcom/osp/app/signin/jr;->j:Lcom/osp/app/signin/SignInView;

    invoke-static {v0}, Lcom/osp/app/signin/SignInView;->y(Lcom/osp/app/signin/SignInView;)Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/osp/app/signin/jr;->j:Lcom/osp/app/signin/SignInView;

    invoke-static {v0}, Lcom/osp/app/signin/SignInView;->y(Lcom/osp/app/signin/SignInView;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v0

    if-lez v0, :cond_0

    .line 6721
    iget-object v0, p0, Lcom/osp/app/signin/jr;->j:Lcom/osp/app/signin/SignInView;

    const/4 v1, 0x1

    invoke-static {v0, v1}, Lcom/osp/app/signin/SignInView;->b(Lcom/osp/app/signin/SignInView;Z)Z

    .line 6722
    invoke-static {}, Lcom/osp/common/util/i;->a()Lcom/osp/common/util/i;

    iget-object v0, p0, Lcom/osp/app/signin/jr;->j:Lcom/osp/app/signin/SignInView;

    invoke-static {v0}, Lcom/osp/common/util/i;->e(Landroid/content/Context;)Z
    :try_end_8
    .catch Ljava/lang/Exception; {:try_start_8 .. :try_end_8} :catch_2
    .catchall {:try_start_8 .. :try_end_8} :catchall_0

    move-result v0

    if-eqz v0, :cond_b

    .line 6727
    :try_start_9
    iget-object v0, p0, Lcom/osp/app/signin/jr;->j:Lcom/osp/app/signin/SignInView;

    invoke-static {}, Lcom/osp/common/util/i;->a()Lcom/osp/common/util/i;

    iget-object v1, p0, Lcom/osp/app/signin/jr;->j:Lcom/osp/app/signin/SignInView;

    invoke-static {v1}, Lcom/osp/common/util/i;->h(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/osp/app/signin/SignInView;->d(Lcom/osp/app/signin/SignInView;Ljava/lang/String;)Ljava/lang/String;

    .line 6728
    iget-object v0, p0, Lcom/osp/app/signin/jr;->j:Lcom/osp/app/signin/SignInView;

    iget-object v1, p0, Lcom/osp/app/signin/jr;->j:Lcom/osp/app/signin/SignInView;

    invoke-static {v1}, Lcom/osp/app/signin/SignInView;->G(Lcom/osp/app/signin/SignInView;)Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/osp/app/signin/jr;->j:Lcom/osp/app/signin/SignInView;

    invoke-virtual {v2}, Lcom/osp/app/signin/SignInView;->getApplicationContext()Landroid/content/Context;

    iget-object v2, p0, Lcom/osp/app/signin/jr;->j:Lcom/osp/app/signin/SignInView;

    invoke-static {v2}, Lcom/osp/app/signin/SignInView;->G(Lcom/osp/app/signin/SignInView;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/osp/common/util/i;->d(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v1, v2}, Lcom/msc/c/e;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)V
    :try_end_9
    .catch Ljava/lang/Exception; {:try_start_9 .. :try_end_9} :catch_3
    .catchall {:try_start_9 .. :try_end_9} :catchall_1

    .line 6734
    :try_start_a
    iget-object v0, p0, Lcom/osp/app/signin/jr;->j:Lcom/osp/app/signin/SignInView;

    invoke-static {v0}, Lcom/osp/app/signin/SignInView;->G(Lcom/osp/app/signin/SignInView;)Ljava/lang/String;

    move-result-object v0

    if-nez v0, :cond_b

    .line 6736
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v0, "SIV"

    const-string v1, "The sim state is ready but mcc is null!!"

    invoke-static {v0, v1}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 6741
    :cond_b
    :goto_2
    iget-object v0, p0, Lcom/osp/app/signin/jr;->j:Lcom/osp/app/signin/SignInView;

    invoke-static {v0}, Lcom/osp/app/signin/SignInView;->G(Lcom/osp/app/signin/SignInView;)Ljava/lang/String;

    move-result-object v0

    if-nez v0, :cond_0

    .line 6743
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v0, "SIV"

    const-string v1, "requestMyCountryZone"

    invoke-static {v0, v1}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;)V

    invoke-static {}, Lcom/msc/c/g;->a()Lcom/msc/c/g;

    iget-object v0, p0, Lcom/osp/app/signin/jr;->j:Lcom/osp/app/signin/SignInView;

    invoke-static {v0, p0}, Lcom/msc/c/g;->b(Landroid/content/Context;Lcom/msc/b/h;)J

    move-result-wide v0

    iput-wide v0, p0, Lcom/osp/app/signin/jr;->x:J

    iget-wide v0, p0, Lcom/osp/app/signin/jr;->x:J

    invoke-virtual {p0, v0, v1}, Lcom/osp/app/signin/jr;->a(J)V

    iget-wide v0, p0, Lcom/osp/app/signin/jr;->x:J

    sget-object v2, Lcom/msc/b/g;->b:Lcom/msc/b/g;

    invoke-static {v0, v1, v2}, Lcom/msc/c/g;->a(JLcom/msc/b/g;)V
    :try_end_a
    .catch Ljava/lang/Exception; {:try_start_a .. :try_end_a} :catch_2
    .catchall {:try_start_a .. :try_end_a} :catchall_0

    goto/16 :goto_0

    .line 6746
    :catch_2
    move-exception v0

    :try_start_b
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V
    :try_end_b
    .catchall {:try_start_b .. :try_end_b} :catchall_0

    goto/16 :goto_0

    .line 6729
    :catch_3
    move-exception v0

    :try_start_c
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V
    :try_end_c
    .catchall {:try_start_c .. :try_end_c} :catchall_1

    .line 6734
    :try_start_d
    iget-object v0, p0, Lcom/osp/app/signin/jr;->j:Lcom/osp/app/signin/SignInView;

    invoke-static {v0}, Lcom/osp/app/signin/SignInView;->G(Lcom/osp/app/signin/SignInView;)Ljava/lang/String;

    move-result-object v0

    if-nez v0, :cond_b

    .line 6736
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v0, "SIV"

    const-string v1, "The sim state is ready but mcc is null!!"

    invoke-static {v0, v1}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_2

    .line 6734
    :catchall_1
    move-exception v0

    iget-object v1, p0, Lcom/osp/app/signin/jr;->j:Lcom/osp/app/signin/SignInView;

    invoke-static {v1}, Lcom/osp/app/signin/SignInView;->G(Lcom/osp/app/signin/SignInView;)Ljava/lang/String;

    move-result-object v1

    if-nez v1, :cond_c

    .line 6736
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v1, "SIV"

    const-string v2, "The sim state is ready but mcc is null!!"

    invoke-static {v1, v2}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;)V

    :cond_c
    throw v0
    :try_end_d
    .catch Ljava/lang/Exception; {:try_start_d .. :try_end_d} :catch_2
    .catchall {:try_start_d .. :try_end_d} :catchall_0

    .line 6750
    :cond_d
    :try_start_e
    iget-wide v0, p0, Lcom/osp/app/signin/jr;->v:J
    :try_end_e
    .catchall {:try_start_e .. :try_end_e} :catchall_0

    cmp-long v0, v2, v0

    if-nez v0, :cond_e

    .line 6754
    :try_start_f
    iget-object v0, p0, Lcom/osp/app/signin/jr;->j:Lcom/osp/app/signin/SignInView;

    invoke-static {}, Lcom/msc/c/h;->a()Lcom/msc/c/h;

    invoke-static {v4}, Lcom/msc/c/h;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/osp/app/signin/SignInView;->f(Lcom/osp/app/signin/SignInView;Ljava/lang/String;)Ljava/lang/String;

    .line 6755
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/osp/app/signin/jr;->o:Z
    :try_end_f
    .catch Ljava/lang/Exception; {:try_start_f .. :try_end_f} :catch_4
    .catchall {:try_start_f .. :try_end_f} :catchall_0

    goto/16 :goto_0

    .line 6757
    :catch_4
    move-exception v0

    .line 6759
    :try_start_10
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    .line 6760
    new-instance v1, Lcom/msc/c/f;

    invoke-direct {v1, v0}, Lcom/msc/c/f;-><init>(Ljava/lang/Exception;)V

    iput-object v1, p0, Lcom/osp/app/signin/jr;->b:Lcom/msc/c/f;

    goto/16 :goto_0

    .line 6762
    :cond_e
    iget-wide v0, p0, Lcom/osp/app/signin/jr;->x:J
    :try_end_10
    .catchall {:try_start_10 .. :try_end_10} :catchall_0

    cmp-long v0, v2, v0

    if-nez v0, :cond_0

    .line 6766
    :try_start_11
    invoke-static {}, Lcom/msc/c/h;->a()Lcom/msc/c/h;

    invoke-static {v4}, Lcom/msc/c/h;->w(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 6767
    if-eqz v0, :cond_10

    .line 6769
    invoke-static {}, Lcom/osp/common/util/i;->a()Lcom/osp/common/util/i;

    iget-object v1, p0, Lcom/osp/app/signin/jr;->j:Lcom/osp/app/signin/SignInView;

    invoke-static {v1}, Lcom/osp/common/util/i;->o(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v1

    .line 6770
    invoke-static {}, Lcom/osp/common/util/i;->a()Lcom/osp/common/util/i;

    iget-object v2, p0, Lcom/osp/app/signin/jr;->j:Lcom/osp/app/signin/SignInView;

    invoke-static {v1}, Lcom/osp/common/util/i;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 6771
    if-eqz v2, :cond_f

    invoke-virtual {v2, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_f

    .line 6773
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v3, "GetMyCountryZoneTask countryCodeFromNetworkMcc = ["

    invoke-direct {v0, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, "]"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;)V

    .line 6774
    iget-object v0, p0, Lcom/osp/app/signin/jr;->j:Lcom/osp/app/signin/SignInView;

    invoke-static {v0, v1}, Lcom/osp/app/signin/SignInView;->d(Lcom/osp/app/signin/SignInView;Ljava/lang/String;)Ljava/lang/String;
    :try_end_11
    .catch Ljava/lang/Exception; {:try_start_11 .. :try_end_11} :catch_5
    .catchall {:try_start_11 .. :try_end_11} :catchall_0

    goto/16 :goto_0

    .line 6784
    :catch_5
    move-exception v0

    .line 6786
    :try_start_12
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v1, "GetMyCountryZoneTask fail"

    invoke-static {v1}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;)V

    .line 6787
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    .line 6788
    iget-object v0, p0, Lcom/osp/app/signin/jr;->j:Lcom/osp/app/signin/SignInView;

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lcom/osp/app/signin/SignInView;->d(Lcom/osp/app/signin/SignInView;Ljava/lang/String;)Ljava/lang/String;
    :try_end_12
    .catchall {:try_start_12 .. :try_end_12} :catchall_0

    goto/16 :goto_0

    .line 6777
    :cond_f
    :try_start_13
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "GetMyCountryZoneTask countryCode = ["

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "]"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;)V

    .line 6778
    iget-object v1, p0, Lcom/osp/app/signin/jr;->j:Lcom/osp/app/signin/SignInView;

    invoke-static {}, Lcom/osp/common/util/i;->a()Lcom/osp/common/util/i;

    iget-object v2, p0, Lcom/osp/app/signin/jr;->j:Lcom/osp/app/signin/SignInView;

    sget-object v2, Ljava/util/Locale;->ENGLISH:Ljava/util/Locale;

    invoke-virtual {v0, v2}, Ljava/lang/String;->toLowerCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/osp/common/util/i;->c(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Lcom/osp/app/signin/SignInView;->d(Lcom/osp/app/signin/SignInView;Ljava/lang/String;)Ljava/lang/String;

    goto/16 :goto_0

    .line 6782
    :cond_10
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v0, "SIV"

    const-string v1, "GetMyCountryZoneTask Success. but countryCode is null!!"

    invoke-static {v0, v1}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_13
    .catch Ljava/lang/Exception; {:try_start_13 .. :try_end_13} :catch_5
    .catchall {:try_start_13 .. :try_end_13} :catchall_0

    goto/16 :goto_0

    :cond_11
    move-object v2, v1

    goto/16 :goto_1
.end method

.method protected final a(Ljava/lang/Boolean;)V
    .locals 4

    .prologue
    const/4 v3, 0x0

    const/4 v2, 0x3

    .line 6554
    invoke-super {p0, p1}, Lcom/msc/c/b;->a(Ljava/lang/Boolean;)V

    .line 6555
    invoke-static {}, Lcom/osp/device/b;->b()Lcom/osp/device/b;

    move-result-object v0

    invoke-virtual {v0}, Lcom/osp/device/b;->a()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 6557
    iget-boolean v0, p0, Lcom/osp/app/signin/jr;->z:Z

    if-eqz v0, :cond_1

    .line 6560
    iget-object v0, p0, Lcom/osp/app/signin/jr;->j:Lcom/osp/app/signin/SignInView;

    const/4 v1, 0x2

    iget-object v2, p0, Lcom/osp/app/signin/jr;->j:Lcom/osp/app/signin/SignInView;

    iget-object v2, v2, Lcom/osp/app/signin/SignInView;->d:Landroid/content/DialogInterface$OnCancelListener;

    invoke-virtual {v0, v1, v3, v2}, Lcom/osp/app/signin/SignInView;->a(ILandroid/content/DialogInterface$OnDismissListener;Landroid/content/DialogInterface$OnCancelListener;)V

    .line 6600
    :cond_0
    :goto_0
    return-void

    .line 6562
    :cond_1
    iget-boolean v0, p0, Lcom/osp/app/signin/jr;->A:Z

    if-eqz v0, :cond_2

    .line 6565
    iget-object v0, p0, Lcom/osp/app/signin/jr;->j:Lcom/osp/app/signin/SignInView;

    iget-object v1, p0, Lcom/osp/app/signin/jr;->j:Lcom/osp/app/signin/SignInView;

    iget-object v1, v1, Lcom/osp/app/signin/SignInView;->e:Landroid/content/DialogInterface$OnCancelListener;

    invoke-virtual {v0, v2, v3, v1}, Lcom/osp/app/signin/SignInView;->a(ILandroid/content/DialogInterface$OnDismissListener;Landroid/content/DialogInterface$OnCancelListener;)V

    goto :goto_0

    .line 6570
    :cond_2
    iget-object v0, p0, Lcom/osp/app/signin/jr;->b:Lcom/msc/c/f;

    if-eqz v0, :cond_8

    .line 6572
    iget-object v0, p0, Lcom/osp/app/signin/jr;->j:Lcom/osp/app/signin/SignInView;

    invoke-static {v0}, Lcom/osp/app/signin/SignInView;->C(Lcom/osp/app/signin/SignInView;)Z

    move-result v0

    if-eqz v0, :cond_4

    iget-object v0, p0, Lcom/osp/app/signin/jr;->j:Lcom/osp/app/signin/SignInView;

    invoke-static {v0}, Lcom/osp/app/signin/SignInView;->D(Lcom/osp/app/signin/SignInView;)Z

    move-result v0

    if-nez v0, :cond_4

    const-string v0, "USR_3113"

    iget-object v1, p0, Lcom/osp/app/signin/jr;->b:Lcom/msc/c/f;

    invoke-virtual {v1}, Lcom/msc/c/f;->a()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 6574
    iget-object v0, p0, Lcom/osp/app/signin/jr;->j:Lcom/osp/app/signin/SignInView;

    iget-object v1, p0, Lcom/osp/app/signin/jr;->j:Lcom/osp/app/signin/SignInView;

    invoke-static {v0, v1}, Lcom/osp/app/signin/SignInView;->a(Lcom/osp/app/signin/SignInView;Landroid/content/Context;)V

    .line 6585
    :goto_1
    const-string v0, "Fail"

    invoke-direct {p0, v0}, Lcom/osp/app/signin/jr;->c(Ljava/lang/String;)V

    .line 6587
    iget-boolean v0, p0, Lcom/osp/app/signin/jr;->w:Z

    if-eqz v0, :cond_3

    .line 6589
    iget-object v0, p0, Lcom/osp/app/signin/jr;->j:Lcom/osp/app/signin/SignInView;

    invoke-static {v0}, Lcom/osp/app/signin/SignInView;->R(Lcom/osp/app/signin/SignInView;)V

    .line 6591
    :cond_3
    iget-boolean v0, p0, Lcom/osp/app/signin/jr;->y:Z

    if-eqz v0, :cond_0

    .line 6593
    iget-object v0, p0, Lcom/osp/app/signin/jr;->j:Lcom/osp/app/signin/SignInView;

    invoke-static {v0}, Lcom/osp/app/signin/SignInView;->S(Lcom/osp/app/signin/SignInView;)V

    goto :goto_0

    .line 6575
    :cond_4
    iget-object v0, p0, Lcom/osp/app/signin/jr;->j:Lcom/osp/app/signin/SignInView;

    invoke-static {v0}, Lcom/osp/app/signin/SignInView;->C(Lcom/osp/app/signin/SignInView;)Z

    move-result v0

    if-eqz v0, :cond_5

    iget-object v0, p0, Lcom/osp/app/signin/jr;->j:Lcom/osp/app/signin/SignInView;

    invoke-static {v0}, Lcom/osp/app/signin/SignInView;->D(Lcom/osp/app/signin/SignInView;)Z

    move-result v0

    if-nez v0, :cond_5

    const-string v0, "USR_3192"

    iget-object v1, p0, Lcom/osp/app/signin/jr;->b:Lcom/msc/c/f;

    invoke-virtual {v1}, Lcom/msc/c/f;->a()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_5

    .line 6577
    iget-object v0, p0, Lcom/osp/app/signin/jr;->j:Lcom/osp/app/signin/SignInView;

    iget-object v1, p0, Lcom/osp/app/signin/jr;->j:Lcom/osp/app/signin/SignInView;

    invoke-static {v0, v1}, Lcom/osp/app/signin/SignInView;->b(Lcom/osp/app/signin/SignInView;Landroid/content/Context;)V

    goto :goto_1

    .line 6578
    :cond_5
    const-string v0, "SOCKET_TIMEOUT_EXCEPTION"

    iget-object v1, p0, Lcom/osp/app/signin/jr;->b:Lcom/msc/c/f;

    invoke-virtual {v1}, Lcom/msc/c/f;->b()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_6

    const-string v0, "CONNECT_TIMEOUT_EXCEPTION"

    iget-object v1, p0, Lcom/osp/app/signin/jr;->b:Lcom/msc/c/f;

    invoke-virtual {v1}, Lcom/msc/c/f;->b()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_7

    .line 6580
    :cond_6
    iget-object v0, p0, Lcom/osp/app/signin/jr;->j:Lcom/osp/app/signin/SignInView;

    iget-object v1, p0, Lcom/osp/app/signin/jr;->j:Lcom/osp/app/signin/SignInView;

    invoke-static {v1}, Lcom/osp/app/signin/SignInView;->c(Lcom/osp/app/signin/SignInView;)Landroid/content/Intent;

    move-result-object v1

    invoke-virtual {v0, v2, v1}, Lcom/osp/app/signin/SignInView;->a(ILandroid/content/Intent;)V

    goto :goto_1

    .line 6583
    :cond_7
    iget-object v0, p0, Lcom/osp/app/signin/jr;->j:Lcom/osp/app/signin/SignInView;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/osp/app/signin/SignInView;->b(I)V

    goto :goto_1

    .line 6598
    :cond_8
    iget-object v0, p0, Lcom/osp/app/signin/jr;->q:Ljava/lang/String;

    invoke-direct {p0, v0}, Lcom/osp/app/signin/jr;->c(Ljava/lang/String;)V

    goto/16 :goto_0
.end method

.method public final b(Lcom/msc/b/c;)V
    .locals 9

    .prologue
    const/4 v8, 0x0

    const/4 v7, 0x1

    const/4 v6, 0x0

    .line 6795
    invoke-super {p0, p1}, Lcom/msc/c/b;->b(Lcom/msc/b/c;)V

    .line 6797
    if-nez p1, :cond_1

    .line 6880
    :cond_0
    :goto_0
    return-void

    .line 6802
    :cond_1
    invoke-virtual {p1}, Lcom/msc/b/c;->a()J

    move-result-wide v0

    .line 6803
    invoke-virtual {p1}, Lcom/msc/b/c;->e()Ljava/lang/String;

    move-result-object v2

    .line 6805
    iget-wide v4, p0, Lcom/osp/app/signin/jr;->r:J

    cmp-long v3, v0, v4

    if-nez v3, :cond_6

    .line 6807
    if-eqz v2, :cond_0

    .line 6809
    const-string v0, "AUT_1805"

    iget-object v1, p0, Lcom/osp/app/signin/jr;->b:Lcom/msc/c/f;

    invoke-virtual {v1}, Lcom/msc/c/f;->a()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_5

    .line 6811
    iget-object v0, p0, Lcom/osp/app/signin/jr;->j:Lcom/osp/app/signin/SignInView;

    invoke-static {v0}, Lcom/osp/app/signin/SignInView;->e(Lcom/osp/app/signin/SignInView;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 6815
    iget-object v0, p0, Lcom/osp/app/signin/jr;->j:Lcom/osp/app/signin/SignInView;

    invoke-static {v0}, Lcom/osp/app/signin/SignInView;->s(Lcom/osp/app/signin/SignInView;)Landroid/view/View;

    move-result-object v0

    const v1, 0x7f0c0129

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/EditText;

    .line 6821
    :goto_1
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    sget-object v2, Ljava/util/Locale;->ENGLISH:Ljava/util/Locale;

    invoke-virtual {v0, v2}, Ljava/lang/String;->toLowerCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "/"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/osp/app/signin/jr;->j:Lcom/osp/app/signin/SignInView;

    invoke-static {v1}, Lcom/osp/app/signin/SignInView;->j(Lcom/osp/app/signin/SignInView;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 6823
    iget-object v1, p0, Lcom/osp/app/signin/jr;->j:Lcom/osp/app/signin/SignInView;

    invoke-static {v1}, Lcom/osp/app/signin/SignInView;->T(Lcom/osp/app/signin/SignInView;)Landroid/content/SharedPreferences;

    move-result-object v1

    if-nez v1, :cond_2

    .line 6825
    iget-object v1, p0, Lcom/osp/app/signin/jr;->j:Lcom/osp/app/signin/SignInView;

    iget-object v2, p0, Lcom/osp/app/signin/jr;->j:Lcom/osp/app/signin/SignInView;

    const-string v3, "VERIFY_COUNT"

    invoke-virtual {v2, v3, v6}, Lcom/osp/app/signin/SignInView;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/osp/app/signin/SignInView;->a(Lcom/osp/app/signin/SignInView;Landroid/content/SharedPreferences;)Landroid/content/SharedPreferences;

    .line 6826
    iget-object v1, p0, Lcom/osp/app/signin/jr;->j:Lcom/osp/app/signin/SignInView;

    invoke-static {v1}, Lcom/osp/app/signin/SignInView;->T(Lcom/osp/app/signin/SignInView;)Landroid/content/SharedPreferences;

    move-result-object v1

    invoke-interface {v1}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v1

    .line 6827
    invoke-interface {v1, v0, v6}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    .line 6828
    invoke-interface {v1}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 6830
    :cond_2
    iget-object v1, p0, Lcom/osp/app/signin/jr;->j:Lcom/osp/app/signin/SignInView;

    invoke-static {v1}, Lcom/osp/app/signin/SignInView;->T(Lcom/osp/app/signin/SignInView;)Landroid/content/SharedPreferences;

    move-result-object v1

    invoke-interface {v1, v0, v6}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v1

    .line 6831
    add-int/lit8 v1, v1, 0x1

    .line 6832
    const/4 v2, 0x5

    if-lt v1, v2, :cond_4

    .line 6834
    iget-object v1, p0, Lcom/osp/app/signin/jr;->j:Lcom/osp/app/signin/SignInView;

    invoke-static {v1}, Lcom/osp/app/signin/SignInView;->T(Lcom/osp/app/signin/SignInView;)Landroid/content/SharedPreferences;

    move-result-object v1

    invoke-interface {v1}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v1

    invoke-interface {v1, v0}, Landroid/content/SharedPreferences$Editor;->remove(Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    .line 6835
    iget-object v0, p0, Lcom/osp/app/signin/jr;->j:Lcom/osp/app/signin/SignInView;

    invoke-static {v0}, Lcom/osp/app/signin/SignInView;->T(Lcom/osp/app/signin/SignInView;)Landroid/content/SharedPreferences;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 6837
    iput-boolean v7, p0, Lcom/osp/app/signin/jr;->w:Z

    goto/16 :goto_0

    .line 6818
    :cond_3
    iget-object v0, p0, Lcom/osp/app/signin/jr;->j:Lcom/osp/app/signin/SignInView;

    const v1, 0x7f0c0121

    invoke-virtual {v0, v1}, Lcom/osp/app/signin/SignInView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/EditText;

    goto/16 :goto_1

    .line 6841
    :cond_4
    iget-object v2, p0, Lcom/osp/app/signin/jr;->j:Lcom/osp/app/signin/SignInView;

    invoke-static {v2}, Lcom/osp/app/signin/SignInView;->T(Lcom/osp/app/signin/SignInView;)Landroid/content/SharedPreferences;

    move-result-object v2

    invoke-interface {v2}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v2

    .line 6842
    invoke-interface {v2, v0, v1}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    .line 6843
    invoke-interface {v2}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 6844
    iget-object v0, p0, Lcom/osp/app/signin/jr;->b:Lcom/msc/c/f;

    const-string v1, "AUT_1805"

    invoke-virtual {v0, v1}, Lcom/msc/c/f;->a(Ljava/lang/String;)V

    .line 6845
    iget-object v0, p0, Lcom/osp/app/signin/jr;->b:Lcom/msc/c/f;

    const-string v1, "fail"

    invoke-virtual {v0, v1}, Lcom/msc/c/f;->b(Ljava/lang/String;)V

    goto/16 :goto_0

    .line 6847
    :cond_5
    const-string v0, "AUT_1820"

    iget-object v1, p0, Lcom/osp/app/signin/jr;->b:Lcom/msc/c/f;

    invoke-virtual {v1}, Lcom/msc/c/f;->a()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 6849
    iput-boolean v7, p0, Lcom/osp/app/signin/jr;->y:Z

    goto/16 :goto_0

    .line 6865
    :cond_6
    iget-wide v2, p0, Lcom/osp/app/signin/jr;->v:J

    cmp-long v2, v0, v2

    if-nez v2, :cond_8

    .line 6867
    iget-boolean v0, p0, Lcom/osp/app/signin/jr;->s:Z

    if-nez v0, :cond_0

    const-string v0, "AUT_1302"

    iget-object v1, p0, Lcom/osp/app/signin/jr;->b:Lcom/msc/c/f;

    invoke-virtual {v1}, Lcom/msc/c/f;->a()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_7

    const-string v0, "AUT_1830"

    iget-object v1, p0, Lcom/osp/app/signin/jr;->b:Lcom/msc/c/f;

    invoke-virtual {v1}, Lcom/msc/c/f;->a()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 6869
    :cond_7
    invoke-virtual {p0}, Lcom/osp/app/signin/jr;->isCancelled()Z

    move-result v0

    if-nez v0, :cond_0

    .line 6873
    iput-object v8, p0, Lcom/osp/app/signin/jr;->b:Lcom/msc/c/f;

    .line 6874
    invoke-direct {p0, v7}, Lcom/osp/app/signin/jr;->b(Z)V

    goto/16 :goto_0

    .line 6876
    :cond_8
    iget-wide v2, p0, Lcom/osp/app/signin/jr;->x:J

    cmp-long v0, v0, v2

    if-nez v0, :cond_0

    .line 6878
    iget-object v0, p0, Lcom/osp/app/signin/jr;->j:Lcom/osp/app/signin/SignInView;

    invoke-static {v0, v8}, Lcom/osp/app/signin/SignInView;->d(Lcom/osp/app/signin/SignInView;Ljava/lang/String;)Ljava/lang/String;

    goto/16 :goto_0
.end method

.method protected final b(Ljava/lang/String;)V
    .locals 3

    .prologue
    .line 6984
    invoke-static {}, Lcom/msc/c/g;->a()Lcom/msc/c/g;

    .line 6985
    iget-object v0, p0, Lcom/osp/app/signin/jr;->j:Lcom/osp/app/signin/SignInView;

    iget-object v1, p0, Lcom/osp/app/signin/jr;->j:Lcom/osp/app/signin/SignInView;

    invoke-static {v1}, Lcom/osp/app/signin/SignInView;->j(Lcom/osp/app/signin/SignInView;)Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/osp/app/signin/jr;->j:Lcom/osp/app/signin/SignInView;

    invoke-static {v2}, Lcom/osp/app/signin/SignInView;->l(Lcom/osp/app/signin/SignInView;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v1, p1, v2, p0}, Lcom/msc/c/g;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/msc/b/h;)J

    move-result-wide v0

    iput-wide v0, p0, Lcom/osp/app/signin/jr;->v:J

    .line 6987
    iget-wide v0, p0, Lcom/osp/app/signin/jr;->v:J

    invoke-virtual {p0, v0, v1}, Lcom/osp/app/signin/jr;->b(J)V

    .line 6988
    iget-wide v0, p0, Lcom/osp/app/signin/jr;->v:J

    const-string v2, "from_json"

    invoke-virtual {p0, v0, v1, v2}, Lcom/osp/app/signin/jr;->a(JLjava/lang/String;)V

    .line 6990
    iget-wide v0, p0, Lcom/osp/app/signin/jr;->v:J

    sget-object v2, Lcom/msc/b/g;->b:Lcom/msc/b/g;

    invoke-static {v0, v1, v2}, Lcom/msc/c/g;->a(JLcom/msc/b/g;)V

    .line 6991
    return-void
.end method

.method protected final d()V
    .locals 2

    .prologue
    const/4 v1, 0x1

    .line 6488
    invoke-super {p0}, Lcom/msc/c/b;->d()V

    .line 6489
    iget-object v0, p0, Lcom/osp/app/signin/jr;->i:Lcom/osp/app/signin/jz;

    if-eqz v0, :cond_0

    .line 6491
    iget-object v0, p0, Lcom/osp/app/signin/jr;->i:Lcom/osp/app/signin/jz;

    iput-boolean v1, v0, Lcom/osp/app/signin/jz;->e:Z

    .line 6493
    :cond_0
    iget-object v0, p0, Lcom/osp/app/signin/jr;->f:Ljava/lang/Thread;

    invoke-static {v0}, Lcom/osp/app/signin/jr;->a(Ljava/lang/Thread;)V

    .line 6494
    iget-object v0, p0, Lcom/osp/app/signin/jr;->g:Ljava/lang/Thread;

    invoke-static {v0}, Lcom/osp/app/signin/jr;->a(Ljava/lang/Thread;)V

    .line 6495
    iget-object v0, p0, Lcom/osp/app/signin/jr;->h:Ljava/lang/Thread;

    invoke-static {v0}, Lcom/osp/app/signin/jr;->a(Ljava/lang/Thread;)V

    .line 6496
    iput-boolean v1, p0, Lcom/osp/app/signin/jr;->k:Z

    .line 6497
    iput-boolean v1, p0, Lcom/osp/app/signin/jr;->l:Z

    .line 6498
    iput-boolean v1, p0, Lcom/osp/app/signin/jr;->m:Z

    .line 6500
    iget-object v0, p0, Lcom/osp/app/signin/jr;->j:Lcom/osp/app/signin/SignInView;

    invoke-static {v0}, Lcom/osp/app/signin/SignInView;->e(Lcom/osp/app/signin/SignInView;)Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/osp/app/signin/jr;->j:Lcom/osp/app/signin/SignInView;

    iget-object v0, v0, Lcom/osp/app/signin/SignInView;->a:Landroid/app/AlertDialog;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/osp/app/signin/jr;->j:Lcom/osp/app/signin/SignInView;

    iget-object v0, v0, Lcom/osp/app/signin/SignInView;->a:Landroid/app/AlertDialog;

    invoke-virtual {v0}, Landroid/app/AlertDialog;->isShowing()Z

    move-result v0

    if-nez v0, :cond_1

    .line 6502
    iget-object v0, p0, Lcom/osp/app/signin/jr;->j:Lcom/osp/app/signin/SignInView;

    iget-object v0, v0, Lcom/osp/app/signin/SignInView;->a:Landroid/app/AlertDialog;

    invoke-virtual {v0}, Landroid/app/AlertDialog;->show()V

    .line 6504
    :cond_1
    return-void
.end method

.method protected final synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 6251
    invoke-virtual {p0}, Lcom/osp/app/signin/jr;->a()Ljava/lang/Boolean;

    move-result-object v0

    return-object v0
.end method

.method public final h()V
    .locals 4

    .prologue
    .line 6889
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v0, "SIV"

    const-string v1, "RequestGetEmptyMandatoryList"

    const-string v2, "START"

    invoke-static {v0, v1, v2}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 6890
    iget-object v0, p0, Lcom/osp/app/signin/jr;->j:Lcom/osp/app/signin/SignInView;

    invoke-static {v0}, Lcom/osp/app/signin/SignInView;->e(Lcom/osp/app/signin/SignInView;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 6894
    iget-object v0, p0, Lcom/osp/app/signin/jr;->j:Lcom/osp/app/signin/SignInView;

    invoke-static {v0}, Lcom/osp/app/signin/SignInView;->s(Lcom/osp/app/signin/SignInView;)Landroid/view/View;

    move-result-object v0

    const v1, 0x7f0c0129

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/EditText;

    .line 6900
    :goto_0
    invoke-static {}, Lcom/msc/c/g;->a()Lcom/msc/c/g;

    .line 6901
    iget-object v1, p0, Lcom/osp/app/signin/jr;->j:Lcom/osp/app/signin/SignInView;

    iget-object v2, p0, Lcom/osp/app/signin/jr;->j:Lcom/osp/app/signin/SignInView;

    invoke-static {v2}, Lcom/osp/app/signin/SignInView;->j(Lcom/osp/app/signin/SignInView;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    sget-object v3, Ljava/util/Locale;->ENGLISH:Ljava/util/Locale;

    invoke-virtual {v0, v3}, Ljava/lang/String;->toLowerCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v2, v0, p0}, Lcom/msc/c/g;->c(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Lcom/msc/b/h;)J

    move-result-wide v0

    iput-wide v0, p0, Lcom/osp/app/signin/jr;->t:J

    .line 6902
    iget-wide v0, p0, Lcom/osp/app/signin/jr;->t:J

    invoke-virtual {p0, v0, v1}, Lcom/osp/app/signin/jr;->c(J)V

    .line 6907
    iget-wide v0, p0, Lcom/osp/app/signin/jr;->t:J

    sget-object v2, Lcom/msc/b/g;->c:Lcom/msc/b/g;

    invoke-static {v0, v1, v2}, Lcom/msc/c/g;->a(JLcom/msc/b/g;)V

    .line 6908
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v0, "SIV"

    const-string v1, "RequestGetEmptyMandatoryList"

    const-string v2, "END"

    invoke-static {v0, v1, v2}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 6909
    return-void

    .line 6897
    :cond_0
    iget-object v0, p0, Lcom/osp/app/signin/jr;->j:Lcom/osp/app/signin/SignInView;

    const v1, 0x7f0c0121

    invoke-virtual {v0, v1}, Lcom/osp/app/signin/SignInView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/EditText;

    goto :goto_0
.end method

.method protected final synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 6251
    check-cast p1, Ljava/lang/Boolean;

    invoke-virtual {p0, p1}, Lcom/osp/app/signin/jr;->a(Ljava/lang/Boolean;)V

    return-void
.end method
