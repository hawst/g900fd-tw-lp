.class final Lcom/osp/app/signin/et;
.super Lcom/msc/c/b;
.source "DuplicateSMSVerificationActivity.java"


# instance fields
.field final synthetic c:Lcom/osp/app/signin/DuplicateSMSVerificationActivity;

.field private d:I

.field private e:J

.field private f:J

.field private g:J

.field private h:J

.field private i:J

.field private j:Z


# direct methods
.method public constructor <init>(Lcom/osp/app/signin/DuplicateSMSVerificationActivity;)V
    .locals 1

    .prologue
    .line 1263
    iput-object p1, p0, Lcom/osp/app/signin/et;->c:Lcom/osp/app/signin/DuplicateSMSVerificationActivity;

    .line 1264
    invoke-direct {p0, p1}, Lcom/msc/c/b;-><init>(Landroid/content/Context;)V

    .line 1240
    const/4 v0, -0x1

    iput v0, p0, Lcom/osp/app/signin/et;->d:I

    .line 1261
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/osp/app/signin/et;->j:Z

    .line 1265
    return-void
.end method

.method private h()V
    .locals 4

    .prologue
    .line 1599
    invoke-static {}, Lcom/msc/c/g;->a()Lcom/msc/c/g;

    .line 1600
    iget-object v0, p0, Lcom/osp/app/signin/et;->c:Lcom/osp/app/signin/DuplicateSMSVerificationActivity;

    iget-object v1, p0, Lcom/osp/app/signin/et;->c:Lcom/osp/app/signin/DuplicateSMSVerificationActivity;

    invoke-static {v1}, Lcom/osp/app/signin/DuplicateSMSVerificationActivity;->p(Lcom/osp/app/signin/DuplicateSMSVerificationActivity;)Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/osp/app/signin/et;->c:Lcom/osp/app/signin/DuplicateSMSVerificationActivity;

    invoke-static {v2}, Lcom/osp/app/signin/DuplicateSMSVerificationActivity;->q(Lcom/osp/app/signin/DuplicateSMSVerificationActivity;)Ljava/lang/String;

    move-result-object v2

    iget-object v3, p0, Lcom/osp/app/signin/et;->c:Lcom/osp/app/signin/DuplicateSMSVerificationActivity;

    invoke-virtual {v3}, Lcom/osp/app/signin/DuplicateSMSVerificationActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    invoke-virtual {v3}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v3

    iget-object v3, v3, Landroid/content/res/Configuration;->locale:Ljava/util/Locale;

    invoke-virtual {v3}, Ljava/util/Locale;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v0, v1, v2, v3, p0}, Lcom/msc/c/g;->g(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/msc/b/h;)J

    move-result-wide v0

    iput-wide v0, p0, Lcom/osp/app/signin/et;->e:J

    .line 1602
    iget-wide v0, p0, Lcom/osp/app/signin/et;->e:J

    const-string v2, "from_xml"

    invoke-virtual {p0, v0, v1, v2}, Lcom/osp/app/signin/et;->a(JLjava/lang/String;)V

    .line 1603
    iget-wide v0, p0, Lcom/osp/app/signin/et;->e:J

    sget-object v2, Lcom/msc/b/g;->c:Lcom/msc/b/g;

    invoke-static {v0, v1, v2}, Lcom/msc/c/g;->a(JLcom/msc/b/g;)V

    .line 1604
    return-void
.end method


# virtual methods
.method protected final varargs a()Ljava/lang/Boolean;
    .locals 6

    .prologue
    .line 1361
    iget v0, p0, Lcom/osp/app/signin/et;->d:I

    packed-switch v0, :pswitch_data_0

    .line 1388
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v0, "DSMSV"

    const-string v1, "doInBackground : undefined SMSAuthenticateTask Mode!!!"

    invoke-static {v0, v1}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 1392
    :goto_0
    const/4 v0, 0x0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    return-object v0

    .line 1369
    :pswitch_0
    invoke-direct {p0}, Lcom/osp/app/signin/et;->h()V

    goto :goto_0

    .line 1373
    :pswitch_1
    iget-object v0, p0, Lcom/osp/app/signin/et;->c:Lcom/osp/app/signin/DuplicateSMSVerificationActivity;

    invoke-static {v0}, Lcom/osp/app/signin/DuplicateSMSVerificationActivity;->n(Lcom/osp/app/signin/DuplicateSMSVerificationActivity;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1375
    new-instance v0, Lcom/msc/c/f;

    invoke-direct {v0}, Lcom/msc/c/f;-><init>()V

    iput-object v0, p0, Lcom/osp/app/signin/et;->b:Lcom/msc/c/f;

    goto :goto_0

    .line 1383
    :cond_0
    invoke-static {}, Lcom/msc/c/g;->a()Lcom/msc/c/g;

    iget-object v0, p0, Lcom/osp/app/signin/et;->c:Lcom/osp/app/signin/DuplicateSMSVerificationActivity;

    iget-object v1, p0, Lcom/osp/app/signin/et;->c:Lcom/osp/app/signin/DuplicateSMSVerificationActivity;

    invoke-static {v1}, Lcom/osp/app/signin/DuplicateSMSVerificationActivity;->p(Lcom/osp/app/signin/DuplicateSMSVerificationActivity;)Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/osp/app/signin/et;->c:Lcom/osp/app/signin/DuplicateSMSVerificationActivity;

    invoke-static {v2}, Lcom/osp/app/signin/DuplicateSMSVerificationActivity;->q(Lcom/osp/app/signin/DuplicateSMSVerificationActivity;)Ljava/lang/String;

    move-result-object v2

    iget-object v3, p0, Lcom/osp/app/signin/et;->c:Lcom/osp/app/signin/DuplicateSMSVerificationActivity;

    invoke-static {v3}, Lcom/osp/app/signin/DuplicateSMSVerificationActivity;->n(Lcom/osp/app/signin/DuplicateSMSVerificationActivity;)Ljava/lang/String;

    move-result-object v3

    iget-object v4, p0, Lcom/osp/app/signin/et;->c:Lcom/osp/app/signin/DuplicateSMSVerificationActivity;

    invoke-static {v4}, Lcom/osp/app/signin/DuplicateSMSVerificationActivity;->r(Lcom/osp/app/signin/DuplicateSMSVerificationActivity;)Ljava/lang/String;

    move-result-object v4

    move-object v5, p0

    invoke-static/range {v0 .. v5}, Lcom/msc/c/g;->c(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/msc/b/h;)J

    move-result-wide v0

    iput-wide v0, p0, Lcom/osp/app/signin/et;->f:J

    iget-wide v0, p0, Lcom/osp/app/signin/et;->f:J

    const-string v2, "from_xml"

    invoke-virtual {p0, v0, v1, v2}, Lcom/osp/app/signin/et;->a(JLjava/lang/String;)V

    iget-wide v0, p0, Lcom/osp/app/signin/et;->f:J

    sget-object v2, Lcom/msc/b/g;->c:Lcom/msc/b/g;

    invoke-static {v0, v1, v2}, Lcom/msc/c/g;->a(JLcom/msc/b/g;)V

    goto :goto_0

    .line 1361
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method protected final a(I)V
    .locals 0

    .prologue
    .line 1268
    iput p1, p0, Lcom/osp/app/signin/et;->d:I

    .line 1269
    return-void
.end method

.method public final declared-synchronized a(Lcom/msc/b/c;)V
    .locals 7

    .prologue
    .line 1397
    monitor-enter p0

    :try_start_0
    invoke-super {p0, p1}, Lcom/msc/c/b;->a(Lcom/msc/b/c;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1399
    if-nez p1, :cond_1

    .line 1518
    :cond_0
    :goto_0
    monitor-exit p0

    return-void

    .line 1404
    :cond_1
    :try_start_1
    invoke-virtual {p1}, Lcom/msc/b/c;->a()J

    move-result-wide v0

    .line 1405
    invoke-virtual {p1}, Lcom/msc/b/c;->e()Ljava/lang/String;

    move-result-object v2

    .line 1407
    iget-wide v4, p0, Lcom/osp/app/signin/et;->i:J
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    cmp-long v3, v0, v4

    if-nez v3, :cond_3

    .line 1411
    :try_start_2
    invoke-static {}, Lcom/msc/c/h;->a()Lcom/msc/c/h;

    invoke-static {v2}, Lcom/msc/c/h;->h(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 1412
    if-eqz v0, :cond_2

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v0

    if-nez v0, :cond_2

    .line 1419
    invoke-direct {p0}, Lcom/osp/app/signin/et;->h()V
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_0
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0

    .line 1426
    :catch_0
    move-exception v0

    .line 1428
    :try_start_3
    new-instance v1, Lcom/msc/c/f;

    invoke-direct {v1, v0}, Lcom/msc/c/f;-><init>(Ljava/lang/Exception;)V

    iput-object v1, p0, Lcom/osp/app/signin/et;->b:Lcom/msc/c/f;
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    goto :goto_0

    .line 1397
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 1423
    :cond_2
    :try_start_4
    new-instance v0, Lcom/msc/c/f;

    invoke-direct {v0}, Lcom/msc/c/f;-><init>()V

    iput-object v0, p0, Lcom/osp/app/signin/et;->b:Lcom/msc/c/f;

    .line 1424
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/osp/app/signin/et;->j:Z
    :try_end_4
    .catch Ljava/lang/Exception; {:try_start_4 .. :try_end_4} :catch_0
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    goto :goto_0

    .line 1430
    :cond_3
    :try_start_5
    iget-wide v4, p0, Lcom/osp/app/signin/et;->e:J

    cmp-long v3, v0, v4

    if-nez v3, :cond_6

    .line 1432
    iget-object v0, p0, Lcom/osp/app/signin/et;->c:Lcom/osp/app/signin/DuplicateSMSVerificationActivity;

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lcom/osp/app/signin/DuplicateSMSVerificationActivity;->e(Lcom/osp/app/signin/DuplicateSMSVerificationActivity;Ljava/lang/String;)Ljava/lang/String;
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    .line 1435
    :try_start_6
    invoke-static {}, Lcom/msc/c/h;->a()Lcom/msc/c/h;

    invoke-static {v2}, Lcom/msc/c/h;->E(Ljava/lang/String;)Ljava/util/HashMap;

    move-result-object v1

    .line 1436
    const-string v0, "authenticateToken"

    invoke-virtual {v1, v0}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 1438
    iget-object v2, p0, Lcom/osp/app/signin/et;->c:Lcom/osp/app/signin/DuplicateSMSVerificationActivity;

    const-string v0, "authenticateToken"

    invoke-virtual {v1, v0}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-static {v2, v0}, Lcom/osp/app/signin/DuplicateSMSVerificationActivity;->e(Lcom/osp/app/signin/DuplicateSMSVerificationActivity;Ljava/lang/String;)Ljava/lang/String;

    .line 1440
    :cond_4
    const-string v0, "prefix"

    invoke-virtual {v1, v0}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_5

    .line 1442
    iget-object v2, p0, Lcom/osp/app/signin/et;->c:Lcom/osp/app/signin/DuplicateSMSVerificationActivity;

    const-string v0, "prefix"

    invoke-virtual {v1, v0}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-static {v2, v0}, Lcom/osp/app/signin/DuplicateSMSVerificationActivity;->f(Lcom/osp/app/signin/DuplicateSMSVerificationActivity;Ljava/lang/String;)Ljava/lang/String;

    .line 1444
    :cond_5
    const-string v0, "limitExpireTime"

    invoke-virtual {v1, v0}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    .line 1449
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "DSMSV authenticateToken : "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v1, p0, Lcom/osp/app/signin/et;->c:Lcom/osp/app/signin/DuplicateSMSVerificationActivity;

    invoke-static {v1}, Lcom/osp/app/signin/DuplicateSMSVerificationActivity;->n(Lcom/osp/app/signin/DuplicateSMSVerificationActivity;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;)V
    :try_end_6
    .catch Ljava/lang/Exception; {:try_start_6 .. :try_end_6} :catch_1
    .catchall {:try_start_6 .. :try_end_6} :catchall_0

    .line 1456
    :goto_1
    :try_start_7
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "DSMSV onSuccess RequestSMSAuthenticate : "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v1, p0, Lcom/osp/app/signin/et;->c:Lcom/osp/app/signin/DuplicateSMSVerificationActivity;

    invoke-static {v1}, Lcom/osp/app/signin/DuplicateSMSVerificationActivity;->n(Lcom/osp/app/signin/DuplicateSMSVerificationActivity;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;)V

    .line 1457
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v0, "DSMSV"

    const-string v1, "RequestSMSAuthenticate onRequestSuccess"

    invoke-static {v0, v1}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 1459
    iget-object v0, p0, Lcom/osp/app/signin/et;->c:Lcom/osp/app/signin/DuplicateSMSVerificationActivity;

    invoke-static {v0}, Lcom/osp/app/signin/DuplicateSMSVerificationActivity;->n(Lcom/osp/app/signin/DuplicateSMSVerificationActivity;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1461
    new-instance v0, Lcom/msc/c/f;

    invoke-direct {v0}, Lcom/msc/c/f;-><init>()V

    iput-object v0, p0, Lcom/osp/app/signin/et;->b:Lcom/msc/c/f;

    goto/16 :goto_0

    .line 1451
    :catch_1
    move-exception v0

    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_1

    .line 1464
    :cond_6
    iget-wide v4, p0, Lcom/osp/app/signin/et;->f:J
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_0

    cmp-long v3, v0, v4

    if-nez v3, :cond_9

    .line 1466
    const/4 v1, 0x0

    .line 1469
    :try_start_8
    invoke-static {}, Lcom/msc/c/h;->a()Lcom/msc/c/h;

    invoke-static {v2}, Lcom/msc/c/h;->G(Ljava/lang/String;)Ljava/util/HashMap;

    move-result-object v2

    .line 1470
    const-string v0, "validationTime"

    invoke-virtual {v2, v0}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_7

    .line 1472
    iget-object v3, p0, Lcom/osp/app/signin/et;->c:Lcom/osp/app/signin/DuplicateSMSVerificationActivity;

    const-string v0, "validationTime"

    invoke-virtual {v2, v0}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-static {v3, v0}, Lcom/osp/app/signin/DuplicateSMSVerificationActivity;->g(Lcom/osp/app/signin/DuplicateSMSVerificationActivity;Ljava/lang/String;)Ljava/lang/String;

    .line 1474
    :cond_7
    const-string v0, "boolean"

    invoke-virtual {v2, v0}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_8

    .line 1476
    const-string v0, "boolean"

    invoke-virtual {v2, v0}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Ljava/lang/Boolean;->parseBoolean(Ljava/lang/String;)Z
    :try_end_8
    .catch Ljava/lang/Exception; {:try_start_8 .. :try_end_8} :catch_2
    .catchall {:try_start_8 .. :try_end_8} :catchall_0

    move-result v0

    .line 1477
    :try_start_9
    iget-object v1, p0, Lcom/osp/app/signin/et;->c:Lcom/osp/app/signin/DuplicateSMSVerificationActivity;

    invoke-static {v1, v0}, Lcom/osp/app/signin/DuplicateSMSVerificationActivity;->b(Lcom/osp/app/signin/DuplicateSMSVerificationActivity;Z)Z
    :try_end_9
    .catch Ljava/lang/Exception; {:try_start_9 .. :try_end_9} :catch_3
    .catchall {:try_start_9 .. :try_end_9} :catchall_0

    .line 1485
    :goto_2
    :try_start_a
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "DSMSV onSuccess ValidateSMSAuthenticate : "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, Lcom/osp/app/signin/et;->c:Lcom/osp/app/signin/DuplicateSMSVerificationActivity;

    invoke-static {v2}, Lcom/osp/app/signin/DuplicateSMSVerificationActivity;->o(Lcom/osp/app/signin/DuplicateSMSVerificationActivity;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;)V

    .line 1486
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v1, "DSMSV"

    const-string v2, "ValidateSMSAuthenticate onRequestSuccess"

    invoke-static {v1, v2}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 1488
    if-nez v0, :cond_0

    .line 1490
    new-instance v0, Lcom/msc/c/f;

    invoke-direct {v0}, Lcom/msc/c/f;-><init>()V

    iput-object v0, p0, Lcom/osp/app/signin/et;->b:Lcom/msc/c/f;

    goto/16 :goto_0

    .line 1480
    :catch_2
    move-exception v0

    :goto_3
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    :cond_8
    move v0, v1

    goto :goto_2

    .line 1492
    :cond_9
    iget-wide v2, p0, Lcom/osp/app/signin/et;->g:J

    cmp-long v2, v0, v2

    if-nez v2, :cond_a

    .line 1495
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v0, "DSMSV"

    const-string v1, "RequestSMSVerifyCodeForDP onRequestSuccess"

    invoke-static {v0, v1}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 1497
    :cond_a
    iget-wide v2, p0, Lcom/osp/app/signin/et;->h:J

    cmp-long v0, v0, v2

    if-nez v0, :cond_0

    .line 1515
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v0, "DSMSV"

    const-string v1, "RequestSMSValidationForDP onRequestSuccess"

    invoke-static {v0, v1}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_a
    .catchall {:try_start_a .. :try_end_a} :catchall_0

    goto/16 :goto_0

    .line 1480
    :catch_3
    move-exception v1

    move-object v6, v1

    move v1, v0

    move-object v0, v6

    goto :goto_3
.end method

.method protected final a(Ljava/lang/Boolean;)V
    .locals 7

    .prologue
    const v6, 0x7f0c00c6

    const v5, 0x7f0900ec

    const/4 v4, 0x4

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 1274
    invoke-super {p0, p1}, Lcom/msc/c/b;->a(Ljava/lang/Boolean;)V

    .line 1276
    iget-object v0, p0, Lcom/osp/app/signin/et;->b:Lcom/msc/c/f;

    if-eqz v0, :cond_7

    .line 1278
    iget v0, p0, Lcom/osp/app/signin/et;->d:I

    if-nez v0, :cond_1

    .line 1280
    iget-object v0, p0, Lcom/osp/app/signin/et;->c:Lcom/osp/app/signin/DuplicateSMSVerificationActivity;

    invoke-virtual {v0, v6}, Lcom/osp/app/signin/DuplicateSMSVerificationActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 1281
    if-eqz v0, :cond_0

    .line 1283
    invoke-virtual {v0, v4}, Landroid/widget/TextView;->setVisibility(I)V

    .line 1284
    iget-object v0, p0, Lcom/osp/app/signin/et;->c:Lcom/osp/app/signin/DuplicateSMSVerificationActivity;

    const-string v1, ""

    invoke-static {v0, v1}, Lcom/osp/app/signin/DuplicateSMSVerificationActivity;->b(Lcom/osp/app/signin/DuplicateSMSVerificationActivity;Ljava/lang/String;)V

    .line 1286
    :cond_0
    iget-object v0, p0, Lcom/osp/app/signin/et;->c:Lcom/osp/app/signin/DuplicateSMSVerificationActivity;

    const v1, 0x7f0c00c7

    invoke-virtual {v0, v1}, Lcom/osp/app/signin/DuplicateSMSVerificationActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    .line 1287
    invoke-virtual {v0, v3}, Landroid/widget/Button;->setEnabled(Z)V

    .line 1290
    :cond_1
    iget-boolean v0, p0, Lcom/osp/app/signin/et;->j:Z

    if-eqz v0, :cond_2

    .line 1292
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v0, "DSMSV"

    const-string v1, "GetUserIDTask - Account already exists."

    invoke-static {v0, v1}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 1293
    invoke-static {}, Lcom/osp/app/util/ap;->a()Lcom/osp/app/util/ap;

    iget-object v0, p0, Lcom/osp/app/signin/et;->c:Lcom/osp/app/signin/DuplicateSMSVerificationActivity;

    iget-object v1, p0, Lcom/osp/app/signin/et;->c:Lcom/osp/app/signin/DuplicateSMSVerificationActivity;

    const v2, 0x7f090015

    invoke-virtual {v1, v2}, Lcom/osp/app/signin/DuplicateSMSVerificationActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1, v3}, Lcom/osp/app/util/ap;->a(Landroid/content/Context;Ljava/lang/String;I)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    .line 1356
    :goto_0
    return-void

    .line 1294
    :cond_2
    iget-object v0, p0, Lcom/osp/app/signin/et;->b:Lcom/msc/c/f;

    invoke-virtual {v0}, Lcom/msc/c/f;->b()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 1296
    invoke-virtual {p0, v2}, Lcom/osp/app/signin/et;->a(Z)V

    goto :goto_0

    .line 1300
    :cond_3
    const-string v0, "USR_3166"

    iget-object v1, p0, Lcom/osp/app/signin/et;->b:Lcom/msc/c/f;

    invoke-virtual {v1}, Lcom/msc/c/f;->a()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 1302
    invoke-static {}, Lcom/osp/app/util/ap;->a()Lcom/osp/app/util/ap;

    iget-object v0, p0, Lcom/osp/app/signin/et;->c:Lcom/osp/app/signin/DuplicateSMSVerificationActivity;

    iget-object v1, p0, Lcom/osp/app/signin/et;->b:Lcom/msc/c/f;

    invoke-virtual {v1}, Lcom/msc/c/f;->b()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1, v3}, Lcom/osp/app/util/ap;->a(Landroid/content/Context;Ljava/lang/String;I)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    goto :goto_0

    .line 1303
    :cond_4
    const-string v0, "USR_3156"

    iget-object v1, p0, Lcom/osp/app/signin/et;->b:Lcom/msc/c/f;

    invoke-virtual {v1}, Lcom/msc/c/f;->a()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_5

    const-string v0, "400122"

    iget-object v1, p0, Lcom/osp/app/signin/et;->b:Lcom/msc/c/f;

    invoke-virtual {v1}, Lcom/msc/c/f;->a()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_6

    .line 1305
    :cond_5
    iget-object v0, p0, Lcom/osp/app/signin/et;->c:Lcom/osp/app/signin/DuplicateSMSVerificationActivity;

    invoke-virtual {v0, v5}, Lcom/osp/app/signin/DuplicateSMSVerificationActivity;->getString(I)Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/osp/app/signin/et;->b:Lcom/msc/c/f;

    invoke-virtual {v1}, Lcom/msc/c/f;->b()Ljava/lang/String;

    move-result-object v1

    new-instance v2, Lcom/osp/app/signin/eu;

    invoke-direct {v2, p0}, Lcom/osp/app/signin/eu;-><init>(Lcom/osp/app/signin/et;)V

    invoke-virtual {p0, v0, v1, v2}, Lcom/osp/app/signin/et;->a(Ljava/lang/String;Ljava/lang/String;Landroid/content/DialogInterface$OnClickListener;)V

    goto :goto_0

    .line 1316
    :cond_6
    iget-object v0, p0, Lcom/osp/app/signin/et;->c:Lcom/osp/app/signin/DuplicateSMSVerificationActivity;

    invoke-virtual {v0, v5}, Lcom/osp/app/signin/DuplicateSMSVerificationActivity;->getString(I)Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/osp/app/signin/et;->b:Lcom/msc/c/f;

    invoke-virtual {v1}, Lcom/msc/c/f;->b()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lcom/osp/app/signin/et;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 1321
    :cond_7
    iget v0, p0, Lcom/osp/app/signin/et;->d:I

    packed-switch v0, :pswitch_data_0

    .line 1351
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v0, "DSMSV"

    const-string v1, "onPostExecute : undefined SMSAuthenticateTask Mode!!!"

    invoke-static {v0, v1}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 1324
    :pswitch_0
    iget-object v0, p0, Lcom/osp/app/signin/et;->c:Lcom/osp/app/signin/DuplicateSMSVerificationActivity;

    invoke-static {v0}, Lcom/osp/app/signin/DuplicateSMSVerificationActivity;->g(Lcom/osp/app/signin/DuplicateSMSVerificationActivity;)Z

    move-result v0

    if-nez v0, :cond_8

    .line 1326
    invoke-static {}, Lcom/osp/app/util/ap;->a()Lcom/osp/app/util/ap;

    iget-object v0, p0, Lcom/osp/app/signin/et;->c:Lcom/osp/app/signin/DuplicateSMSVerificationActivity;

    const v1, 0x7f0900d4

    invoke-static {v0, v1, v2}, Lcom/osp/app/util/ap;->a(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    .line 1329
    :cond_8
    iget-object v0, p0, Lcom/osp/app/signin/et;->c:Lcom/osp/app/signin/DuplicateSMSVerificationActivity;

    invoke-static {v0}, Lcom/osp/app/signin/DuplicateSMSVerificationActivity;->h(Lcom/osp/app/signin/DuplicateSMSVerificationActivity;)Z

    .line 1330
    iget-object v0, p0, Lcom/osp/app/signin/et;->c:Lcom/osp/app/signin/DuplicateSMSVerificationActivity;

    invoke-static {v0}, Lcom/osp/app/signin/DuplicateSMSVerificationActivity;->i(Lcom/osp/app/signin/DuplicateSMSVerificationActivity;)V

    .line 1331
    iget-object v0, p0, Lcom/osp/app/signin/et;->c:Lcom/osp/app/signin/DuplicateSMSVerificationActivity;

    invoke-static {v0}, Lcom/osp/app/signin/DuplicateSMSVerificationActivity;->j(Lcom/osp/app/signin/DuplicateSMSVerificationActivity;)V

    .line 1332
    iget-object v0, p0, Lcom/osp/app/signin/et;->c:Lcom/osp/app/signin/DuplicateSMSVerificationActivity;

    const v1, 0x7f0c00c4

    invoke-virtual {v0, v1}, Lcom/osp/app/signin/DuplicateSMSVerificationActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/EditText;

    .line 1333
    invoke-virtual {v0}, Landroid/widget/EditText;->requestFocus()Z

    .line 1334
    iget-object v0, p0, Lcom/osp/app/signin/et;->c:Lcom/osp/app/signin/DuplicateSMSVerificationActivity;

    invoke-static {v0}, Lcom/osp/app/signin/DuplicateSMSVerificationActivity;->k(Lcom/osp/app/signin/DuplicateSMSVerificationActivity;)V

    goto/16 :goto_0

    .line 1337
    :pswitch_1
    iget-object v0, p0, Lcom/osp/app/signin/et;->c:Lcom/osp/app/signin/DuplicateSMSVerificationActivity;

    const-string v1, "notification"

    invoke-virtual {v0, v1}, Lcom/osp/app/signin/DuplicateSMSVerificationActivity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/NotificationManager;

    .line 1338
    const v1, 0x132df82

    invoke-virtual {v0, v1}, Landroid/app/NotificationManager;->cancel(I)V

    .line 1339
    iget-object v0, p0, Lcom/osp/app/signin/et;->c:Lcom/osp/app/signin/DuplicateSMSVerificationActivity;

    invoke-static {v0}, Lcom/osp/app/signin/DuplicateSMSVerificationActivity;->l(Lcom/osp/app/signin/DuplicateSMSVerificationActivity;)Landroid/os/Handler;

    move-result-object v0

    if-eqz v0, :cond_9

    .line 1341
    iget-object v0, p0, Lcom/osp/app/signin/et;->c:Lcom/osp/app/signin/DuplicateSMSVerificationActivity;

    invoke-static {v0}, Lcom/osp/app/signin/DuplicateSMSVerificationActivity;->l(Lcom/osp/app/signin/DuplicateSMSVerificationActivity;)Landroid/os/Handler;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/os/Handler;->removeMessages(I)V

    .line 1343
    :cond_9
    iget-object v0, p0, Lcom/osp/app/signin/et;->c:Lcom/osp/app/signin/DuplicateSMSVerificationActivity;

    invoke-static {v0}, Lcom/osp/app/signin/DuplicateSMSVerificationActivity;->m(Lcom/osp/app/signin/DuplicateSMSVerificationActivity;)V

    .line 1344
    iget-object v0, p0, Lcom/osp/app/signin/et;->c:Lcom/osp/app/signin/DuplicateSMSVerificationActivity;

    invoke-virtual {v0, v6}, Lcom/osp/app/signin/DuplicateSMSVerificationActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 1345
    invoke-virtual {v0, v4}, Landroid/widget/TextView;->setVisibility(I)V

    .line 1346
    iget-object v0, p0, Lcom/osp/app/signin/et;->c:Lcom/osp/app/signin/DuplicateSMSVerificationActivity;

    const-string v1, ""

    invoke-static {v0, v1}, Lcom/osp/app/signin/DuplicateSMSVerificationActivity;->b(Lcom/osp/app/signin/DuplicateSMSVerificationActivity;Ljava/lang/String;)V

    .line 1347
    iget-object v0, p0, Lcom/osp/app/signin/et;->c:Lcom/osp/app/signin/DuplicateSMSVerificationActivity;

    invoke-virtual {v0}, Lcom/osp/app/signin/DuplicateSMSVerificationActivity;->c()V

    goto/16 :goto_0

    .line 1321
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public final b(Lcom/msc/b/c;)V
    .locals 11

    .prologue
    const v10, 0x7f0900b0

    const/16 v9, 0x18

    const v8, 0x7f0900b1

    const/4 v7, 0x0

    const/4 v6, 0x1

    .line 1522
    invoke-super {p0, p1}, Lcom/msc/c/b;->b(Lcom/msc/b/c;)V

    .line 1524
    if-nez p1, :cond_1

    .line 1577
    :cond_0
    :goto_0
    return-void

    .line 1529
    :cond_1
    invoke-virtual {p1}, Lcom/msc/b/c;->a()J

    move-result-wide v0

    .line 1530
    invoke-virtual {p1}, Lcom/msc/b/c;->e()Ljava/lang/String;

    move-result-object v2

    .line 1532
    iget-wide v4, p0, Lcom/osp/app/signin/et;->e:J

    cmp-long v3, v0, v4

    if-nez v3, :cond_4

    .line 1534
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v0, "DSMSV"

    const-string v1, "RequestSMSAuthenticate failed"

    invoke-static {v0, v1}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 1535
    if-eqz v2, :cond_3

    .line 1537
    const-string v0, "USR_3156"

    iget-object v1, p0, Lcom/osp/app/signin/et;->b:Lcom/msc/c/f;

    invoke-virtual {v1}, Lcom/msc/c/f;->a()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 1539
    iget-object v0, p0, Lcom/osp/app/signin/et;->b:Lcom/msc/c/f;

    iget-object v1, p0, Lcom/osp/app/signin/et;->c:Lcom/osp/app/signin/DuplicateSMSVerificationActivity;

    const v2, 0x7f0900d3

    invoke-virtual {v1, v2}, Lcom/osp/app/signin/DuplicateSMSVerificationActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    new-array v2, v6, [Ljava/lang/Object;

    invoke-static {v9}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v2, v7

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/msc/c/f;->b(Ljava/lang/String;)V

    goto :goto_0

    .line 1542
    :cond_2
    const-string v0, "USR_3166"

    iget-object v1, p0, Lcom/osp/app/signin/et;->b:Lcom/msc/c/f;

    invoke-virtual {v1}, Lcom/msc/c/f;->a()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 1544
    iget-object v0, p0, Lcom/osp/app/signin/et;->b:Lcom/msc/c/f;

    iget-object v1, p0, Lcom/osp/app/signin/et;->c:Lcom/osp/app/signin/DuplicateSMSVerificationActivity;

    const v2, 0x7f090134

    invoke-virtual {v1, v2}, Lcom/osp/app/signin/DuplicateSMSVerificationActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    new-array v2, v6, [Ljava/lang/Object;

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v2, v7

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/msc/c/f;->b(Ljava/lang/String;)V

    goto :goto_0

    .line 1548
    :cond_3
    iget-object v0, p0, Lcom/osp/app/signin/et;->b:Lcom/msc/c/f;

    iget-object v1, p0, Lcom/osp/app/signin/et;->c:Lcom/osp/app/signin/DuplicateSMSVerificationActivity;

    invoke-virtual {v1, v8}, Lcom/osp/app/signin/DuplicateSMSVerificationActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/msc/c/f;->b(Ljava/lang/String;)V

    goto :goto_0

    .line 1550
    :cond_4
    iget-wide v2, p0, Lcom/osp/app/signin/et;->f:J

    cmp-long v2, v0, v2

    if-nez v2, :cond_5

    .line 1552
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v0, "DSMSV"

    const-string v1, "ValidateSMSAuthenticate failed"

    invoke-static {v0, v1}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 1553
    iget-object v0, p0, Lcom/osp/app/signin/et;->b:Lcom/msc/c/f;

    iget-object v1, p0, Lcom/osp/app/signin/et;->c:Lcom/osp/app/signin/DuplicateSMSVerificationActivity;

    invoke-virtual {v1, v10}, Lcom/osp/app/signin/DuplicateSMSVerificationActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/msc/c/f;->b(Ljava/lang/String;)V

    goto/16 :goto_0

    .line 1554
    :cond_5
    iget-wide v2, p0, Lcom/osp/app/signin/et;->g:J

    cmp-long v2, v0, v2

    if-nez v2, :cond_7

    .line 1556
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v0, "DSMSV"

    const-string v1, "RequestNewSMSVerifyCodeWithDPServer failed"

    invoke-static {v0, v1}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 1558
    const-string v0, "4000401123"

    iget-object v1, p0, Lcom/osp/app/signin/et;->b:Lcom/msc/c/f;

    invoke-virtual {v1}, Lcom/msc/c/f;->a()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_6

    .line 1560
    iget-object v0, p0, Lcom/osp/app/signin/et;->b:Lcom/msc/c/f;

    iget-object v1, p0, Lcom/osp/app/signin/et;->c:Lcom/osp/app/signin/DuplicateSMSVerificationActivity;

    const v2, 0x7f0900d3

    invoke-virtual {v1, v2}, Lcom/osp/app/signin/DuplicateSMSVerificationActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    new-array v2, v6, [Ljava/lang/Object;

    invoke-static {v9}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v2, v7

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/msc/c/f;->b(Ljava/lang/String;)V

    goto/16 :goto_0

    .line 1564
    :cond_6
    iget-object v0, p0, Lcom/osp/app/signin/et;->b:Lcom/msc/c/f;

    iget-object v1, p0, Lcom/osp/app/signin/et;->c:Lcom/osp/app/signin/DuplicateSMSVerificationActivity;

    invoke-virtual {v1, v8}, Lcom/osp/app/signin/DuplicateSMSVerificationActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/msc/c/f;->b(Ljava/lang/String;)V

    goto/16 :goto_0

    .line 1566
    :cond_7
    iget-wide v2, p0, Lcom/osp/app/signin/et;->h:J

    cmp-long v0, v0, v2

    if-nez v0, :cond_0

    .line 1568
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v0, "DSMSV"

    const-string v1, "RequestNewValidate failed"

    invoke-static {v0, v1}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 1569
    const-string v0, "USR_3177"

    iget-object v1, p0, Lcom/osp/app/signin/et;->b:Lcom/msc/c/f;

    invoke-virtual {v1}, Lcom/msc/c/f;->a()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_8

    .line 1571
    iget-object v0, p0, Lcom/osp/app/signin/et;->b:Lcom/msc/c/f;

    iget-object v1, p0, Lcom/osp/app/signin/et;->c:Lcom/osp/app/signin/DuplicateSMSVerificationActivity;

    invoke-virtual {v1, v10}, Lcom/osp/app/signin/DuplicateSMSVerificationActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/msc/c/f;->b(Ljava/lang/String;)V

    goto/16 :goto_0

    .line 1574
    :cond_8
    iget-object v0, p0, Lcom/osp/app/signin/et;->b:Lcom/msc/c/f;

    iget-object v1, p0, Lcom/osp/app/signin/et;->c:Lcom/osp/app/signin/DuplicateSMSVerificationActivity;

    invoke-virtual {v1, v8}, Lcom/osp/app/signin/DuplicateSMSVerificationActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/msc/c/f;->b(Ljava/lang/String;)V

    goto/16 :goto_0
.end method

.method protected final d()V
    .locals 2

    .prologue
    .line 1581
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v0, "DSMSV"

    const-string v1, "SMSAuthenticateTask canceled"

    invoke-static {v0, v1}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 1582
    invoke-super {p0}, Lcom/msc/c/b;->d()V

    .line 1583
    return-void
.end method

.method protected final synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 1235
    invoke-virtual {p0}, Lcom/osp/app/signin/et;->a()Ljava/lang/Boolean;

    move-result-object v0

    return-object v0
.end method

.method protected final synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 1235
    check-cast p1, Ljava/lang/Boolean;

    invoke-virtual {p0, p1}, Lcom/osp/app/signin/et;->a(Ljava/lang/Boolean;)V

    return-void
.end method
