.class final Lcom/osp/app/signin/ns;
.super Lcom/msc/c/b;
.source "SmsVerificationActivity.java"


# instance fields
.field final synthetic c:Lcom/osp/app/signin/SmsVerificationActivity;

.field private d:J

.field private e:J

.field private f:J


# direct methods
.method public constructor <init>(Lcom/osp/app/signin/SmsVerificationActivity;)V
    .locals 1

    .prologue
    .line 2969
    iput-object p1, p0, Lcom/osp/app/signin/ns;->c:Lcom/osp/app/signin/SmsVerificationActivity;

    .line 2970
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/msc/c/b;-><init>(Landroid/content/Context;Z)V

    .line 2971
    return-void
.end method


# virtual methods
.method protected final varargs a()Ljava/lang/Boolean;
    .locals 9

    .prologue
    .line 3011
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v0, "SMSV"

    const-string v1, "requestUpdateUserLoginID()"

    invoke-static {v0, v1}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;)V

    invoke-static {}, Lcom/msc/c/g;->a()Lcom/msc/c/g;

    const-string v0, "FROM_SIGN_IN_FLOW"

    iget-object v1, p0, Lcom/osp/app/signin/ns;->c:Lcom/osp/app/signin/SmsVerificationActivity;

    invoke-static {v1}, Lcom/osp/app/signin/SmsVerificationActivity;->M(Lcom/osp/app/signin/SmsVerificationActivity;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/osp/app/signin/ns;->c:Lcom/osp/app/signin/SmsVerificationActivity;

    invoke-static {v0}, Lcom/osp/app/signin/SmsVerificationActivity;->U(Lcom/osp/app/signin/SmsVerificationActivity;)Ljava/lang/String;

    move-result-object v1

    :goto_0
    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_1

    new-instance v0, Lcom/msc/c/f;

    const-string v1, "USERID IS NULL"

    invoke-direct {v0, v1}, Lcom/msc/c/f;-><init>(Ljava/lang/String;)V

    iput-object v0, p0, Lcom/osp/app/signin/ns;->b:Lcom/msc/c/f;

    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v0, "User id is not exist in DB [check Seungeon about duplicated phone number]"

    invoke-static {v0}, Lcom/osp/app/util/aq;->b(Ljava/lang/String;)V

    .line 3012
    :goto_1
    const/4 v0, 0x0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    return-object v0

    .line 3011
    :cond_0
    iget-object v0, p0, Lcom/osp/app/signin/ns;->c:Lcom/osp/app/signin/SmsVerificationActivity;

    invoke-static {v0}, Lcom/msc/c/e;->h(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v1

    goto :goto_0

    :cond_1
    iget-object v0, p0, Lcom/osp/app/signin/ns;->c:Lcom/osp/app/signin/SmsVerificationActivity;

    invoke-static {v0}, Lcom/osp/app/signin/SmsVerificationActivity;->D(Lcom/osp/app/signin/SmsVerificationActivity;)Ljava/lang/String;

    move-result-object v2

    iget-object v0, p0, Lcom/osp/app/signin/ns;->c:Lcom/osp/app/signin/SmsVerificationActivity;

    invoke-static {v0}, Lcom/osp/app/signin/SmsVerificationActivity;->j(Lcom/osp/app/signin/SmsVerificationActivity;)Ljava/lang/String;

    move-result-object v4

    iget-object v0, p0, Lcom/osp/app/signin/ns;->c:Lcom/osp/app/signin/SmsVerificationActivity;

    const-string v3, "001"

    const-string v5, "Y"

    iget-object v6, p0, Lcom/osp/app/signin/ns;->c:Lcom/osp/app/signin/SmsVerificationActivity;

    invoke-static {v6}, Lcom/osp/app/signin/SmsVerificationActivity;->N(Lcom/osp/app/signin/SmsVerificationActivity;)Ljava/lang/String;

    move-result-object v6

    iget-object v7, p0, Lcom/osp/app/signin/ns;->c:Lcom/osp/app/signin/SmsVerificationActivity;

    invoke-static {v7}, Lcom/osp/app/signin/SmsVerificationActivity;->R(Lcom/osp/app/signin/SmsVerificationActivity;)Ljava/lang/String;

    move-result-object v7

    move-object v8, p0

    invoke-static/range {v0 .. v8}, Lcom/msc/c/g;->b(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/msc/b/h;)J

    move-result-wide v0

    iput-wide v0, p0, Lcom/osp/app/signin/ns;->d:J

    iget-wide v0, p0, Lcom/osp/app/signin/ns;->d:J

    invoke-virtual {p0, v0, v1}, Lcom/osp/app/signin/ns;->b(J)V

    iget-wide v0, p0, Lcom/osp/app/signin/ns;->d:J

    const-string v2, "from_xml"

    invoke-virtual {p0, v0, v1, v2}, Lcom/osp/app/signin/ns;->a(JLjava/lang/String;)V

    iget-wide v0, p0, Lcom/osp/app/signin/ns;->d:J

    sget-object v2, Lcom/msc/b/g;->d:Lcom/msc/b/g;

    invoke-static {v0, v1, v2}, Lcom/msc/c/g;->a(JLcom/msc/b/g;)V

    goto :goto_1
.end method

.method public final declared-synchronized a(Lcom/msc/b/c;)V
    .locals 4

    .prologue
    .line 3017
    monitor-enter p0

    :try_start_0
    invoke-super {p0, p1}, Lcom/msc/c/b;->a(Lcom/msc/b/c;)V

    .line 3018
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v0, "SMSV"

    const-string v1, "requestUpdateUserLoginID onRequestSuccess"

    invoke-static {v0, v1}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 3019
    if-nez p1, :cond_1

    .line 3113
    :cond_0
    :goto_0
    monitor-exit p0

    return-void

    .line 3024
    :cond_1
    :try_start_1
    invoke-virtual {p1}, Lcom/msc/b/c;->a()J

    move-result-wide v0

    .line 3027
    iget-wide v2, p0, Lcom/osp/app/signin/ns;->d:J

    cmp-long v0, v0, v2

    if-nez v0, :cond_0

    .line 3030
    iget-object v0, p0, Lcom/osp/app/signin/ns;->c:Lcom/osp/app/signin/SmsVerificationActivity;

    invoke-static {v0}, Lcom/msc/c/c;->b(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v0

    .line 3031
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_2

    .line 3033
    invoke-static {}, Lcom/msc/c/g;->a()Lcom/msc/c/g;

    iget-object v1, p0, Lcom/osp/app/signin/ns;->c:Lcom/osp/app/signin/SmsVerificationActivity;

    invoke-static {v1, v0, p0}, Lcom/msc/c/g;->b(Landroid/content/Context;Ljava/lang/String;Lcom/msc/b/h;)J

    move-result-wide v0

    iput-wide v0, p0, Lcom/osp/app/signin/ns;->f:J

    iget-wide v0, p0, Lcom/osp/app/signin/ns;->f:J

    sget-object v2, Lcom/msc/b/g;->a:Lcom/msc/b/g;

    invoke-static {v0, v1, v2}, Lcom/msc/c/g;->a(JLcom/msc/b/g;)V

    .line 3036
    :cond_2
    iget-object v0, p0, Lcom/osp/app/signin/ns;->c:Lcom/osp/app/signin/SmsVerificationActivity;

    invoke-static {v0}, Lcom/msc/c/e;->g(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v0

    .line 3037
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_3

    .line 3039
    invoke-static {}, Lcom/msc/c/g;->a()Lcom/msc/c/g;

    iget-object v1, p0, Lcom/osp/app/signin/ns;->c:Lcom/osp/app/signin/SmsVerificationActivity;

    invoke-static {v1, v0, p0}, Lcom/msc/c/g;->a(Landroid/content/Context;Ljava/lang/String;Lcom/msc/b/h;)J

    move-result-wide v0

    iput-wide v0, p0, Lcom/osp/app/signin/ns;->e:J

    iget-wide v0, p0, Lcom/osp/app/signin/ns;->e:J

    sget-object v2, Lcom/msc/b/g;->c:Lcom/msc/b/g;

    invoke-static {v0, v1, v2}, Lcom/msc/c/g;->a(JLcom/msc/b/g;)V

    .line 3043
    :cond_3
    iget-object v0, p0, Lcom/osp/app/signin/ns;->c:Lcom/osp/app/signin/SmsVerificationActivity;

    invoke-static {v0}, Lcom/msc/c/c;->g(Landroid/content/Context;)V

    .line 3044
    iget-object v0, p0, Lcom/osp/app/signin/ns;->c:Lcom/osp/app/signin/SmsVerificationActivity;

    invoke-static {v0}, Lcom/msc/c/e;->a(Landroid/content/Context;)Z

    .line 3045
    iget-object v0, p0, Lcom/osp/app/signin/ns;->c:Lcom/osp/app/signin/SmsVerificationActivity;

    invoke-static {v0}, Lcom/osp/app/util/ad;->r(Landroid/content/Context;)V

    .line 3048
    iget-object v0, p0, Lcom/osp/app/signin/ns;->c:Lcom/osp/app/signin/SmsVerificationActivity;

    invoke-static {v0}, Lcom/osp/app/util/ad;->d(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 3050
    iget-object v0, p0, Lcom/osp/app/signin/ns;->c:Lcom/osp/app/signin/SmsVerificationActivity;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v2, p0, Lcom/osp/app/signin/ns;->c:Lcom/osp/app/signin/SmsVerificationActivity;

    invoke-static {v2}, Lcom/osp/app/signin/SmsVerificationActivity;->j(Lcom/osp/app/signin/SmsVerificationActivity;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/osp/app/signin/ns;->c:Lcom/osp/app/signin/SmsVerificationActivity;

    invoke-static {v2}, Lcom/osp/app/signin/SmsVerificationActivity;->D(Lcom/osp/app/signin/SmsVerificationActivity;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/osp/app/util/ad;->b(Landroid/content/Context;Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 3052
    new-instance v0, Lcom/msc/c/f;

    const-string v1, "ERR_MDM_SECURITY"

    const-string v2, "fail_MDM_Security"

    invoke-direct {v0, v1, v2}, Lcom/msc/c/f;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    iput-object v0, p0, Lcom/osp/app/signin/ns;->b:Lcom/msc/c/f;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto/16 :goto_0

    .line 3017
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method protected final a(Ljava/lang/Boolean;)V
    .locals 2

    .prologue
    .line 2975
    invoke-super {p0, p1}, Lcom/msc/c/b;->a(Ljava/lang/Boolean;)V

    .line 2977
    iget-object v0, p0, Lcom/osp/app/signin/ns;->b:Lcom/msc/c/f;

    if-eqz v0, :cond_3

    .line 2979
    iget-object v0, p0, Lcom/osp/app/signin/ns;->c:Lcom/osp/app/signin/SmsVerificationActivity;

    const v1, 0x7f0c00c7

    invoke-virtual {v0, v1}, Lcom/osp/app/signin/SmsVerificationActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/view/View;->setEnabled(Z)V

    .line 2980
    iget-object v0, p0, Lcom/osp/app/signin/ns;->c:Lcom/osp/app/signin/SmsVerificationActivity;

    invoke-static {v0}, Lcom/osp/app/signin/SmsVerificationActivity;->r(Lcom/osp/app/signin/SmsVerificationActivity;)V

    .line 2982
    const-string v0, "USR_3173"

    iget-object v1, p0, Lcom/osp/app/signin/ns;->b:Lcom/msc/c/f;

    invoke-virtual {v1}, Lcom/msc/c/f;->a()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 2984
    iget-object v0, p0, Lcom/osp/app/signin/ns;->c:Lcom/osp/app/signin/SmsVerificationActivity;

    invoke-static {v0}, Lcom/osp/app/signin/SmsVerificationActivity;->t(Lcom/osp/app/signin/SmsVerificationActivity;)V

    .line 3006
    :cond_0
    :goto_0
    return-void

    .line 2987
    :cond_1
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/osp/app/signin/ns;->a(Z)V

    .line 2988
    const-string v0, "USR_3119"

    iget-object v1, p0, Lcom/osp/app/signin/ns;->b:Lcom/msc/c/f;

    invoke-virtual {v1}, Lcom/msc/c/f;->a()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_2

    const-string v0, "ERR_MDM_SECURITY"

    iget-object v1, p0, Lcom/osp/app/signin/ns;->b:Lcom/msc/c/f;

    invoke-virtual {v1}, Lcom/msc/c/f;->a()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2990
    :cond_2
    iget-object v0, p0, Lcom/osp/app/signin/ns;->c:Lcom/osp/app/signin/SmsVerificationActivity;

    const/16 v1, 0x15

    invoke-virtual {v0, v1}, Lcom/osp/app/signin/SmsVerificationActivity;->b(I)V

    .line 2991
    iget-object v0, p0, Lcom/osp/app/signin/ns;->c:Lcom/osp/app/signin/SmsVerificationActivity;

    invoke-virtual {v0}, Lcom/osp/app/signin/SmsVerificationActivity;->finish()V

    goto :goto_0

    .line 2997
    :cond_3
    const-string v0, "FROM_SIGN_IN_FLOW"

    iget-object v1, p0, Lcom/osp/app/signin/ns;->c:Lcom/osp/app/signin/SmsVerificationActivity;

    invoke-static {v1}, Lcom/osp/app/signin/SmsVerificationActivity;->M(Lcom/osp/app/signin/SmsVerificationActivity;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 2999
    iget-object v0, p0, Lcom/osp/app/signin/ns;->c:Lcom/osp/app/signin/SmsVerificationActivity;

    invoke-static {v0}, Lcom/osp/app/signin/SmsVerificationActivity;->S(Lcom/osp/app/signin/SmsVerificationActivity;)V

    goto :goto_0

    .line 3002
    :cond_4
    iget-object v0, p0, Lcom/osp/app/signin/ns;->c:Lcom/osp/app/signin/SmsVerificationActivity;

    invoke-static {v0}, Lcom/osp/app/signin/SmsVerificationActivity;->T(Lcom/osp/app/signin/SmsVerificationActivity;)V

    goto :goto_0
.end method

.method public final b(Lcom/msc/b/c;)V
    .locals 6

    .prologue
    .line 3117
    invoke-super {p0, p1}, Lcom/msc/c/b;->b(Lcom/msc/b/c;)V

    .line 3118
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v0, "SMSV"

    const-string v1, "requestUpdateUserLoginID onRequestFail"

    invoke-static {v0, v1}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 3119
    if-nez p1, :cond_1

    .line 3157
    :cond_0
    :goto_0
    return-void

    .line 3124
    :cond_1
    invoke-virtual {p1}, Lcom/msc/b/c;->a()J

    move-result-wide v0

    .line 3125
    invoke-virtual {p1}, Lcom/msc/b/c;->e()Ljava/lang/String;

    move-result-object v2

    .line 3127
    iget-wide v4, p0, Lcom/osp/app/signin/ns;->d:J

    cmp-long v0, v0, v4

    if-nez v0, :cond_0

    .line 3129
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v0, "SMSV"

    iget-object v1, p0, Lcom/osp/app/signin/ns;->b:Lcom/msc/c/f;

    invoke-virtual {v1}, Lcom/msc/c/f;->a()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 3130
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v0, "SMSV"

    const-string v1, "requestUpdateUserLoginID failed"

    invoke-static {v0, v1}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 3131
    if-eqz v2, :cond_3

    .line 3133
    const-string v0, "USR_1611"

    iget-object v1, p0, Lcom/osp/app/signin/ns;->b:Lcom/msc/c/f;

    invoke-virtual {v1}, Lcom/msc/c/f;->a()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 3137
    const-string v0, "USR_3111"

    iget-object v1, p0, Lcom/osp/app/signin/ns;->b:Lcom/msc/c/f;

    invoke-virtual {v1}, Lcom/msc/c/f;->a()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 3141
    const-string v0, "USR_3119"

    iget-object v1, p0, Lcom/osp/app/signin/ns;->b:Lcom/msc/c/f;

    invoke-virtual {v1}, Lcom/msc/c/f;->a()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 3145
    const-string v0, "USR_3173"

    iget-object v1, p0, Lcom/osp/app/signin/ns;->b:Lcom/msc/c/f;

    invoke-virtual {v1}, Lcom/msc/c/f;->a()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 3147
    iget-object v0, p0, Lcom/osp/app/signin/ns;->b:Lcom/msc/c/f;

    const-string v1, "The telephone number ID sign-up reached max number."

    invoke-virtual {v0, v1}, Lcom/msc/c/f;->b(Ljava/lang/String;)V

    goto :goto_0

    .line 3149
    :cond_2
    const-string v0, "USR_3151"

    iget-object v1, p0, Lcom/osp/app/signin/ns;->b:Lcom/msc/c/f;

    invoke-virtual {v1}, Lcom/msc/c/f;->a()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 3151
    iget-object v0, p0, Lcom/osp/app/signin/ns;->b:Lcom/msc/c/f;

    const-string v1, "The authenticateNumber or the authenticateToken is not matched"

    invoke-virtual {v0, v1}, Lcom/msc/c/f;->b(Ljava/lang/String;)V

    goto :goto_0

    .line 3155
    :cond_3
    iget-object v0, p0, Lcom/osp/app/signin/ns;->b:Lcom/msc/c/f;

    iget-object v1, p0, Lcom/osp/app/signin/ns;->c:Lcom/osp/app/signin/SmsVerificationActivity;

    const v2, 0x7f0900b1

    invoke-virtual {v1, v2}, Lcom/osp/app/signin/SmsVerificationActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/msc/c/f;->b(Ljava/lang/String;)V

    goto/16 :goto_0
.end method

.method protected final d()V
    .locals 2

    .prologue
    .line 3161
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v0, "SMSV"

    const-string v1, "UpdateUserLoginIDTask canceled"

    invoke-static {v0, v1}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 3162
    invoke-super {p0}, Lcom/msc/c/b;->d()V

    .line 3163
    return-void
.end method

.method protected final synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 2954
    invoke-virtual {p0}, Lcom/osp/app/signin/ns;->a()Ljava/lang/Boolean;

    move-result-object v0

    return-object v0
.end method

.method protected final synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 2954
    check-cast p1, Ljava/lang/Boolean;

    invoke-virtual {p0, p1}, Lcom/osp/app/signin/ns;->a(Ljava/lang/Boolean;)V

    return-void
.end method
