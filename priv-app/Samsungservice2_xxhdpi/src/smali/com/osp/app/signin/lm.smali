.class final Lcom/osp/app/signin/lm;
.super Ljava/lang/Object;
.source "SignUpView.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field final synthetic a:Lcom/osp/app/signin/SignUpView;


# direct methods
.method constructor <init>(Lcom/osp/app/signin/SignUpView;)V
    .locals 0

    .prologue
    .line 730
    iput-object p1, p0, Lcom/osp/app/signin/lm;->a:Lcom/osp/app/signin/SignUpView;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onClick(Landroid/view/View;)V
    .locals 5

    .prologue
    const/4 v2, 0x0

    .line 734
    iget-object v0, p0, Lcom/osp/app/signin/lm;->a:Lcom/osp/app/signin/SignUpView;

    invoke-virtual {v0}, Lcom/osp/app/signin/SignUpView;->getIntent()Landroid/content/Intent;

    move-result-object v0

    .line 735
    if-eqz v0, :cond_2

    const-string v1, "is_cancelable_just_one_activity"

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 737
    const-string v1, "key_is_name_verified"

    iget-object v2, p0, Lcom/osp/app/signin/lm;->a:Lcom/osp/app/signin/SignUpView;

    invoke-static {v2}, Lcom/osp/app/signin/SignUpView;->d(Lcom/osp/app/signin/SignUpView;)Z

    move-result v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 738
    iget-object v1, p0, Lcom/osp/app/signin/lm;->a:Lcom/osp/app/signin/SignUpView;

    invoke-static {v1}, Lcom/osp/app/signin/SignUpView;->d(Lcom/osp/app/signin/SignUpView;)Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/osp/app/signin/lm;->a:Lcom/osp/app/signin/SignUpView;

    invoke-static {v1}, Lcom/osp/app/signin/SignUpView;->e(Lcom/osp/app/signin/SignUpView;)Landroid/os/Bundle;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 740
    const-string v1, "information_after_name_validation"

    iget-object v2, p0, Lcom/osp/app/signin/lm;->a:Lcom/osp/app/signin/SignUpView;

    invoke-static {v2}, Lcom/osp/app/signin/SignUpView;->e(Lcom/osp/app/signin/SignUpView;)Landroid/os/Bundle;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Bundle;)Landroid/content/Intent;

    .line 742
    :cond_0
    iget-object v1, p0, Lcom/osp/app/signin/lm;->a:Lcom/osp/app/signin/SignUpView;

    const/16 v2, 0xe

    invoke-virtual {v1, v2, v0}, Lcom/osp/app/signin/SignUpView;->a(ILandroid/content/Intent;)V

    .line 750
    :goto_0
    invoke-static {}, Lcom/osp/device/b;->b()Lcom/osp/device/b;

    move-result-object v0

    iget-object v1, p0, Lcom/osp/app/signin/lm;->a:Lcom/osp/app/signin/SignUpView;

    invoke-virtual {v0, v1}, Lcom/osp/device/b;->i(Landroid/content/Context;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 754
    new-instance v0, Lcom/osp/app/bigdatalog/c;

    iget-object v1, p0, Lcom/osp/app/signin/lm;->a:Lcom/osp/app/signin/SignUpView;

    invoke-virtual {v1}, Lcom/osp/app/signin/SignUpView;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    const/4 v2, 0x1

    const/4 v3, 0x3

    iget-object v4, p0, Lcom/osp/app/signin/lm;->a:Lcom/osp/app/signin/SignUpView;

    invoke-static {v4}, Lcom/osp/app/signin/SignUpView;->f(Lcom/osp/app/signin/SignUpView;)Landroid/content/Intent;

    move-result-object v4

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/osp/app/bigdatalog/c;-><init>(Landroid/content/Context;IILandroid/content/Intent;)V

    .line 756
    invoke-static {}, Lcom/osp/app/bigdatalog/e;->a()Lcom/osp/app/bigdatalog/e;

    move-result-object v1

    iget-object v2, p0, Lcom/osp/app/signin/lm;->a:Lcom/osp/app/signin/SignUpView;

    invoke-virtual {v2}, Lcom/osp/app/signin/SignUpView;->getApplicationContext()Landroid/content/Context;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/osp/app/bigdatalog/e;->a(Landroid/content/Context;)V

    .line 757
    invoke-static {}, Lcom/osp/app/bigdatalog/e;->a()Lcom/osp/app/bigdatalog/e;

    move-result-object v1

    iget-object v2, p0, Lcom/osp/app/signin/lm;->a:Lcom/osp/app/signin/SignUpView;

    invoke-virtual {v2}, Lcom/osp/app/signin/SignUpView;->getApplicationContext()Landroid/content/Context;

    move-result-object v2

    invoke-virtual {v1, v2, v0}, Lcom/osp/app/bigdatalog/e;->a(Landroid/content/Context;Lcom/osp/app/bigdatalog/c;)V

    .line 760
    :cond_1
    iget-object v0, p0, Lcom/osp/app/signin/lm;->a:Lcom/osp/app/signin/SignUpView;

    invoke-virtual {v0}, Lcom/osp/app/signin/SignUpView;->finish()V

    .line 761
    return-void

    .line 745
    :cond_2
    iget-object v0, p0, Lcom/osp/app/signin/lm;->a:Lcom/osp/app/signin/SignUpView;

    invoke-virtual {v0, v2}, Lcom/osp/app/signin/SignUpView;->b(I)V

    goto :goto_0
.end method
