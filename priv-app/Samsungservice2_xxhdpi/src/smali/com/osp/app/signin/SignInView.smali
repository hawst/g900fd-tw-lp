.class public Lcom/osp/app/signin/SignInView;
.super Lcom/osp/app/util/BaseActivity;
.source "SignInView.java"

# interfaces
.implements Landroid/view/View$OnFocusChangeListener;


# annotations
.annotation build Landroid/annotation/SuppressLint;
    value = {
        "CommitPrefEdits"
    }
.end annotation


# instance fields
.field private A:Ljava/lang/String;

.field private B:Ljava/lang/String;

.field private C:Ljava/lang/String;

.field private D:Ljava/lang/String;

.field private E:Ljava/lang/String;

.field private F:Ljava/lang/String;

.field private G:Ljava/lang/String;

.field private H:Ljava/lang/String;

.field private I:Landroid/content/SharedPreferences;

.field private J:Lcom/osp/app/signin/jj;

.field private K:Lcom/osp/app/signin/jr;

.field private L:Lcom/osp/app/signin/jv;

.field private M:Lcom/osp/app/signin/jm;

.field private N:Lcom/osp/app/signin/jl;

.field private O:Lcom/osp/app/signin/jk;

.field private P:Lcom/osp/app/signin/jy;

.field private Q:Ljava/lang/String;

.field private R:Ljava/lang/String;

.field private S:Landroid/view/View;

.field private T:Lcom/msc/a/d;

.field private U:Lcom/osp/app/signin/kd;

.field private V:Lcom/msc/a/g;

.field private W:Z

.field private final X:Landroid/os/Handler;

.field private final Y:Landroid/os/Handler;

.field private Z:Z

.field a:Landroid/app/AlertDialog;

.field private aa:Z

.field private ab:Z

.field private ac:Z

.field private ad:Ljava/lang/String;

.field private ae:I

.field private af:Landroid/app/AlertDialog;

.field private ag:Z

.field private ah:Z

.field private ai:Z

.field private aj:Z

.field private ak:J

.field private al:Ljava/lang/String;

.field private am:Ljava/lang/String;

.field private an:Z

.field private ao:J

.field private ap:J

.field private aq:Ljava/lang/String;

.field private ar:Ljava/lang/Boolean;

.field private as:Ljava/lang/Boolean;

.field private at:Lcom/osp/app/signin/BottomSoftkeyLinearLayout;

.field private au:Lcom/osp/app/signin/BottomSoftkeyLinearLayout;

.field final b:Landroid/view/View$OnClickListener;

.field final c:Landroid/view/View$OnClickListener;

.field final d:Landroid/content/DialogInterface$OnCancelListener;

.field final e:Landroid/content/DialogInterface$OnCancelListener;

.field final f:Landroid/content/DialogInterface$OnCancelListener;

.field private y:Landroid/content/Intent;

.field private z:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x0

    .line 119
    invoke-direct {p0}, Lcom/osp/app/util/BaseActivity;-><init>()V

    .line 160
    iput-object v1, p0, Lcom/osp/app/signin/SignInView;->a:Landroid/app/AlertDialog;

    .line 172
    iput-object v1, p0, Lcom/osp/app/signin/SignInView;->y:Landroid/content/Intent;

    .line 177
    iput-object v1, p0, Lcom/osp/app/signin/SignInView;->z:Ljava/lang/String;

    .line 182
    iput-object v1, p0, Lcom/osp/app/signin/SignInView;->A:Ljava/lang/String;

    .line 187
    iput-object v1, p0, Lcom/osp/app/signin/SignInView;->B:Ljava/lang/String;

    .line 192
    iput-object v1, p0, Lcom/osp/app/signin/SignInView;->C:Ljava/lang/String;

    .line 197
    iput-object v1, p0, Lcom/osp/app/signin/SignInView;->D:Ljava/lang/String;

    .line 202
    iput-object v1, p0, Lcom/osp/app/signin/SignInView;->E:Ljava/lang/String;

    .line 207
    iput-object v1, p0, Lcom/osp/app/signin/SignInView;->F:Ljava/lang/String;

    .line 212
    iput-object v1, p0, Lcom/osp/app/signin/SignInView;->G:Ljava/lang/String;

    .line 214
    const-string v0, ""

    iput-object v0, p0, Lcom/osp/app/signin/SignInView;->H:Ljava/lang/String;

    .line 294
    iput-object v1, p0, Lcom/osp/app/signin/SignInView;->Q:Ljava/lang/String;

    .line 299
    iput-object v1, p0, Lcom/osp/app/signin/SignInView;->R:Ljava/lang/String;

    .line 301
    iput-object v1, p0, Lcom/osp/app/signin/SignInView;->S:Landroid/view/View;

    .line 306
    iput-object v1, p0, Lcom/osp/app/signin/SignInView;->T:Lcom/msc/a/d;

    .line 312
    iput-object v1, p0, Lcom/osp/app/signin/SignInView;->U:Lcom/osp/app/signin/kd;

    .line 324
    iput-boolean v2, p0, Lcom/osp/app/signin/SignInView;->W:Z

    .line 332
    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    iput-object v0, p0, Lcom/osp/app/signin/SignInView;->X:Landroid/os/Handler;

    .line 334
    new-instance v0, Lcom/osp/app/signin/hu;

    invoke-direct {v0, p0}, Lcom/osp/app/signin/hu;-><init>(Lcom/osp/app/signin/SignInView;)V

    iput-object v0, p0, Lcom/osp/app/signin/SignInView;->Y:Landroid/os/Handler;

    .line 352
    iput-boolean v2, p0, Lcom/osp/app/signin/SignInView;->Z:Z

    .line 358
    iput-boolean v2, p0, Lcom/osp/app/signin/SignInView;->aa:Z

    .line 368
    iput-boolean v2, p0, Lcom/osp/app/signin/SignInView;->ac:Z

    .line 398
    iput-boolean v2, p0, Lcom/osp/app/signin/SignInView;->ai:Z

    .line 403
    iput-boolean v2, p0, Lcom/osp/app/signin/SignInView;->aj:Z

    .line 425
    iput-boolean v2, p0, Lcom/osp/app/signin/SignInView;->an:Z

    .line 443
    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Lcom/osp/app/signin/SignInView;->ar:Ljava/lang/Boolean;

    .line 449
    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Lcom/osp/app/signin/SignInView;->as:Ljava/lang/Boolean;

    .line 467
    new-instance v0, Lcom/osp/app/signin/ig;

    invoke-direct {v0, p0}, Lcom/osp/app/signin/ig;-><init>(Lcom/osp/app/signin/SignInView;)V

    iput-object v0, p0, Lcom/osp/app/signin/SignInView;->b:Landroid/view/View$OnClickListener;

    .line 478
    new-instance v0, Lcom/osp/app/signin/ir;

    invoke-direct {v0, p0}, Lcom/osp/app/signin/ir;-><init>(Lcom/osp/app/signin/SignInView;)V

    iput-object v0, p0, Lcom/osp/app/signin/SignInView;->c:Landroid/view/View$OnClickListener;

    .line 1021
    new-instance v0, Lcom/osp/app/signin/hx;

    invoke-direct {v0, p0}, Lcom/osp/app/signin/hx;-><init>(Lcom/osp/app/signin/SignInView;)V

    iput-object v0, p0, Lcom/osp/app/signin/SignInView;->d:Landroid/content/DialogInterface$OnCancelListener;

    .line 1032
    new-instance v0, Lcom/osp/app/signin/hy;

    invoke-direct {v0, p0}, Lcom/osp/app/signin/hy;-><init>(Lcom/osp/app/signin/SignInView;)V

    iput-object v0, p0, Lcom/osp/app/signin/SignInView;->e:Landroid/content/DialogInterface$OnCancelListener;

    .line 1044
    new-instance v0, Lcom/osp/app/signin/hz;

    invoke-direct {v0, p0}, Lcom/osp/app/signin/hz;-><init>(Lcom/osp/app/signin/SignInView;)V

    iput-object v0, p0, Lcom/osp/app/signin/SignInView;->f:Landroid/content/DialogInterface$OnCancelListener;

    .line 8301
    return-void
.end method

.method static synthetic A(Lcom/osp/app/signin/SignInView;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 119
    iget-object v0, p0, Lcom/osp/app/signin/SignInView;->am:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic B(Lcom/osp/app/signin/SignInView;)J
    .locals 2

    .prologue
    .line 119
    iget-wide v0, p0, Lcom/osp/app/signin/SignInView;->ap:J

    return-wide v0
.end method

.method static synthetic C(Lcom/osp/app/signin/SignInView;)Z
    .locals 1

    .prologue
    .line 119
    iget-boolean v0, p0, Lcom/osp/app/signin/SignInView;->aa:Z

    return v0
.end method

.method static synthetic D(Lcom/osp/app/signin/SignInView;)Z
    .locals 1

    .prologue
    .line 119
    iget-boolean v0, p0, Lcom/osp/app/signin/SignInView;->ab:Z

    return v0
.end method

.method static synthetic E(Lcom/osp/app/signin/SignInView;)V
    .locals 3

    .prologue
    .line 119
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v0, "SIV"

    const-string v1, "SignInExecute"

    const-string v2, "START"

    invoke-static {v0, v1, v2}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    invoke-direct {p0}, Lcom/osp/app/signin/SignInView;->r()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-static {p0}, Lcom/osp/app/util/ad;->a(Landroid/content/Context;)Z

    move-result v0

    if-nez v0, :cond_1

    invoke-static {}, Lcom/osp/app/util/ap;->a()Lcom/osp/app/util/ap;

    const v0, 0x7f09003e

    invoke-virtual {p0, v0}, Lcom/osp/app/signin/SignInView;->getString(I)Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x0

    invoke-static {p0, v0, v1}, Lcom/osp/app/util/ap;->a(Landroid/content/Context;Ljava/lang/String;I)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    :cond_0
    :goto_0
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v0, "SIV"

    const-string v1, "SignInExecute"

    const-string v2, "END"

    invoke-static {v0, v1, v2}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    return-void

    :cond_1
    invoke-virtual {p0}, Lcom/osp/app/signin/SignInView;->d()V

    new-instance v0, Lcom/osp/app/signin/jr;

    invoke-direct {v0, p0}, Lcom/osp/app/signin/jr;-><init>(Lcom/osp/app/signin/SignInView;)V

    iput-object v0, p0, Lcom/osp/app/signin/SignInView;->K:Lcom/osp/app/signin/jr;

    iget-object v0, p0, Lcom/osp/app/signin/SignInView;->K:Lcom/osp/app/signin/jr;

    invoke-virtual {v0}, Lcom/osp/app/signin/jr;->b()V

    goto :goto_0
.end method

.method static synthetic F(Lcom/osp/app/signin/SignInView;)Z
    .locals 1

    .prologue
    .line 119
    iget-boolean v0, p0, Lcom/osp/app/signin/SignInView;->Z:Z

    return v0
.end method

.method static synthetic G(Lcom/osp/app/signin/SignInView;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 119
    iget-object v0, p0, Lcom/osp/app/signin/SignInView;->al:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic H(Lcom/osp/app/signin/SignInView;)Lcom/osp/app/signin/jj;
    .locals 1

    .prologue
    .line 119
    iget-object v0, p0, Lcom/osp/app/signin/SignInView;->J:Lcom/osp/app/signin/jj;

    return-object v0
.end method

.method static synthetic I(Lcom/osp/app/signin/SignInView;)Lcom/osp/app/signin/jm;
    .locals 1

    .prologue
    .line 119
    iget-object v0, p0, Lcom/osp/app/signin/SignInView;->M:Lcom/osp/app/signin/jm;

    return-object v0
.end method

.method static synthetic J(Lcom/osp/app/signin/SignInView;)V
    .locals 4

    .prologue
    .line 119
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    const-class v1, Lcom/osp/app/signin/SelectCountryView;

    invoke-virtual {v0, p0, v1}, Landroid/content/Intent;->setClass(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;

    const-string v1, "MODE"

    iget-object v2, p0, Lcom/osp/app/signin/SignInView;->C:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    const-string v1, "BG_WhoareU"

    iget-object v2, p0, Lcom/osp/app/signin/SignInView;->G:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    const-string v1, "com.osp.app.signin.action.SAMSUNG_ACCOUNT_SETUPWIZARD"

    invoke-static {}, Lcom/osp/app/signin/SamsungService;->a()Z

    move-result v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    const-string v1, "is_signup_flow"

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    const-string v1, "BG_mode"

    iget-object v2, p0, Lcom/osp/app/signin/SignInView;->G:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    iget-object v1, p0, Lcom/osp/app/signin/SignInView;->F:Ljava/lang/String;

    if-eqz v1, :cond_0

    const-string v1, "mypackage"

    iget-object v2, p0, Lcom/osp/app/signin/SignInView;->F:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    :cond_0
    const-string v1, "key_request_id"

    iget-wide v2, p0, Lcom/osp/app/signin/SignInView;->ak:J

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;J)Landroid/content/Intent;

    :cond_1
    const/16 v1, 0xd0

    invoke-virtual {p0, v0, v1}, Lcom/osp/app/signin/SignInView;->startActivityForResult(Landroid/content/Intent;I)V

    return-void
.end method

.method static synthetic K(Lcom/osp/app/signin/SignInView;)Lcom/msc/a/g;
    .locals 1

    .prologue
    .line 119
    iget-object v0, p0, Lcom/osp/app/signin/SignInView;->V:Lcom/msc/a/g;

    return-object v0
.end method

.method static synthetic L(Lcom/osp/app/signin/SignInView;)V
    .locals 0

    .prologue
    .line 119
    invoke-virtual {p0}, Lcom/osp/app/signin/SignInView;->m()V

    return-void
.end method

.method static synthetic M(Lcom/osp/app/signin/SignInView;)V
    .locals 0

    .prologue
    .line 119
    invoke-direct {p0}, Lcom/osp/app/signin/SignInView;->q()V

    return-void
.end method

.method static synthetic N(Lcom/osp/app/signin/SignInView;)Z
    .locals 1

    .prologue
    .line 119
    iget-boolean v0, p0, Lcom/osp/app/signin/SignInView;->ac:Z

    return v0
.end method

.method static synthetic O(Lcom/osp/app/signin/SignInView;)Z
    .locals 1

    .prologue
    .line 119
    iget-boolean v0, p0, Lcom/osp/app/signin/SignInView;->ah:Z

    return v0
.end method

.method static synthetic P(Lcom/osp/app/signin/SignInView;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 119
    iget-object v0, p0, Lcom/osp/app/signin/SignInView;->C:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic Q(Lcom/osp/app/signin/SignInView;)Lcom/msc/a/d;
    .locals 1

    .prologue
    .line 119
    iget-object v0, p0, Lcom/osp/app/signin/SignInView;->T:Lcom/msc/a/d;

    return-object v0
.end method

.method static synthetic R(Lcom/osp/app/signin/SignInView;)V
    .locals 3

    .prologue
    const/4 v2, 0x1

    .line 119
    iget-object v0, p0, Lcom/osp/app/signin/SignInView;->y:Landroid/content/Intent;

    const-string v1, "passwordBlock"

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    invoke-virtual {p0, v2}, Lcom/osp/app/signin/SignInView;->b(I)V

    invoke-virtual {p0}, Lcom/osp/app/signin/SignInView;->finish()V

    return-void
.end method

.method static synthetic S(Lcom/osp/app/signin/SignInView;)V
    .locals 4

    .prologue
    .line 119
    invoke-static {}, Lcom/osp/device/b;->b()Lcom/osp/device/b;

    move-result-object v0

    invoke-virtual {v0}, Lcom/osp/device/b;->a()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v0, "Login try with blocked id"

    invoke-static {v0}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;)V

    new-instance v2, Landroid/content/Intent;

    const-string v0, "com.msc.action.samsungaccount.web_dialog"

    invoke-direct {v2, v0}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    invoke-direct {p0}, Lcom/osp/app/signin/SignInView;->e()Z

    move-result v0

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/osp/app/signin/SignInView;->S:Landroid/view/View;

    const v1, 0x7f0c0129

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/EditText;

    :goto_0
    const-string v1, "j5p7ll8g33"

    iget-object v3, p0, Lcom/osp/app/signin/SignInView;->z:Ljava/lang/String;

    invoke-static {v3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_0

    iget-object v1, p0, Lcom/osp/app/signin/SignInView;->z:Ljava/lang/String;

    :cond_0
    invoke-virtual {v0}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    sget-object v3, Ljava/util/Locale;->ENGLISH:Ljava/util/Locale;

    invoke-virtual {v0, v3}, Ljava/lang/String;->toLowerCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v0

    iget-object v3, p0, Lcom/osp/app/signin/SignInView;->am:Ljava/lang/String;

    invoke-static {p0, v1, v0, v3}, Lcom/msc/c/n;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    const-string v1, "ServerUrl"

    invoke-virtual {v2, v1, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    const/16 v0, 0xf5

    invoke-virtual {p0, v2, v0}, Lcom/osp/app/signin/SignInView;->startActivityForResult(Landroid/content/Intent;I)V

    :cond_1
    return-void

    :cond_2
    const v0, 0x7f0c0121

    invoke-virtual {p0, v0}, Lcom/osp/app/signin/SignInView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/EditText;

    goto :goto_0
.end method

.method static synthetic T(Lcom/osp/app/signin/SignInView;)Landroid/content/SharedPreferences;
    .locals 1

    .prologue
    .line 119
    iget-object v0, p0, Lcom/osp/app/signin/SignInView;->I:Landroid/content/SharedPreferences;

    return-object v0
.end method

.method static synthetic U(Lcom/osp/app/signin/SignInView;)Z
    .locals 1

    .prologue
    .line 119
    invoke-direct {p0}, Lcom/osp/app/signin/SignInView;->v()Z

    move-result v0

    return v0
.end method

.method static synthetic V(Lcom/osp/app/signin/SignInView;)V
    .locals 3

    .prologue
    .line 119
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v0, "SIV"

    const-string v1, "sendSignInCompleteBroadcast"

    invoke-static {v0, v1}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;)V

    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    iget-boolean v1, p0, Lcom/osp/app/signin/SignInView;->aa:Z

    if-eqz v1, :cond_2

    const-string v1, "android.intent.action.SAMSUNGACCOUNT_RESIGNIN_COMPLETED"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    :goto_0
    invoke-static {}, Lcom/osp/device/b;->b()Lcom/osp/device/b;

    move-result-object v1

    invoke-virtual {v1}, Lcom/osp/device/b;->l()I

    move-result v1

    const/16 v2, 0xb

    if-le v1, v2, :cond_0

    const/16 v1, 0x20

    invoke-virtual {v0, v1}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    :cond_0
    invoke-virtual {p0, v0}, Lcom/osp/app/signin/SignInView;->sendBroadcast(Landroid/content/Intent;)V

    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v0, "SIV"

    const-string v1, "send signInCompleteIntentV02"

    invoke-static {v0, v1}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;)V

    new-instance v0, Landroid/content/Intent;

    const-string v1, "android.intent.action.REGISTRATION_COMPLETED"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    const-string v1, "login_id"

    invoke-static {p0}, Lcom/osp/app/util/ad;->h(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    invoke-virtual {p0, v0}, Lcom/osp/app/signin/SignInView;->sendBroadcast(Landroid/content/Intent;)V

    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v0, "SIV"

    const-string v1, "send signInCompleteIntentV01"

    invoke-static {v0, v1}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;)V

    invoke-static {}, Lcom/osp/app/signin/SamsungService;->a()Z

    move-result v0

    if-nez v0, :cond_1

    invoke-virtual {p0}, Lcom/osp/app/signin/SignInView;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string v1, "com.msc.action.MARKETING_POPUP_NOTIFICATION"

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v0

    if-eqz v0, :cond_1

    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    const-string v1, "com.msc.action.MARKETING_POPUP_NOTIFICATION"

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    const-string v1, "com.osp.app.signin"

    const-string v2, "com.osp.app.signin.BackgroundModeService"

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->setClassName(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    invoke-virtual {p0, v0}, Lcom/osp/app/signin/SignInView;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;

    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v0, "SIV"

    const-string v1, "startService MarketingPopupNotiIntent"

    invoke-static {v0, v1}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;)V

    :cond_1
    return-void

    :cond_2
    const-string v1, "android.intent.action.SAMSUNGACCOUNT_SIGNIN_COMPLETED"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    goto :goto_0
.end method

.method static synthetic W(Lcom/osp/app/signin/SignInView;)Lcom/osp/app/signin/jy;
    .locals 1

    .prologue
    .line 119
    iget-object v0, p0, Lcom/osp/app/signin/SignInView;->P:Lcom/osp/app/signin/jy;

    return-object v0
.end method

.method static synthetic X(Lcom/osp/app/signin/SignInView;)Z
    .locals 1

    .prologue
    .line 119
    iget-boolean v0, p0, Lcom/osp/app/signin/SignInView;->aj:Z

    return v0
.end method

.method static synthetic Y(Lcom/osp/app/signin/SignInView;)Lcom/osp/app/signin/kd;
    .locals 1

    .prologue
    .line 119
    iget-object v0, p0, Lcom/osp/app/signin/SignInView;->U:Lcom/osp/app/signin/kd;

    return-object v0
.end method

.method static synthetic Z(Lcom/osp/app/signin/SignInView;)J
    .locals 2

    .prologue
    .line 119
    iget-wide v0, p0, Lcom/osp/app/signin/SignInView;->ao:J

    return-wide v0
.end method

.method static synthetic a(Lcom/osp/app/signin/SignInView;I)I
    .locals 0

    .prologue
    .line 119
    iput p1, p0, Lcom/osp/app/signin/SignInView;->ae:I

    return p1
.end method

.method static synthetic a(Lcom/osp/app/signin/SignInView;Landroid/app/AlertDialog;)Landroid/app/AlertDialog;
    .locals 0

    .prologue
    .line 119
    iput-object p1, p0, Lcom/osp/app/signin/SignInView;->af:Landroid/app/AlertDialog;

    return-object p1
.end method

.method static synthetic a(Lcom/osp/app/signin/SignInView;Landroid/content/Intent;)Landroid/content/Intent;
    .locals 0

    .prologue
    .line 119
    iput-object p1, p0, Lcom/osp/app/signin/SignInView;->y:Landroid/content/Intent;

    return-object p1
.end method

.method static synthetic a(Lcom/osp/app/signin/SignInView;Landroid/content/SharedPreferences;)Landroid/content/SharedPreferences;
    .locals 0

    .prologue
    .line 119
    iput-object p1, p0, Lcom/osp/app/signin/SignInView;->I:Landroid/content/SharedPreferences;

    return-object p1
.end method

.method static synthetic a(Lcom/osp/app/signin/SignInView;Lcom/msc/a/d;)Lcom/msc/a/d;
    .locals 0

    .prologue
    .line 119
    iput-object p1, p0, Lcom/osp/app/signin/SignInView;->T:Lcom/msc/a/d;

    return-object p1
.end method

.method static synthetic a(Lcom/osp/app/signin/SignInView;Lcom/osp/app/signin/jj;)Lcom/osp/app/signin/jj;
    .locals 0

    .prologue
    .line 119
    iput-object p1, p0, Lcom/osp/app/signin/SignInView;->J:Lcom/osp/app/signin/jj;

    return-object p1
.end method

.method static synthetic a(Lcom/osp/app/signin/SignInView;Lcom/osp/app/signin/jm;)Lcom/osp/app/signin/jm;
    .locals 0

    .prologue
    .line 119
    iput-object p1, p0, Lcom/osp/app/signin/SignInView;->M:Lcom/osp/app/signin/jm;

    return-object p1
.end method

.method static synthetic a(Lcom/osp/app/signin/SignInView;Lcom/osp/app/signin/jy;)Lcom/osp/app/signin/jy;
    .locals 0

    .prologue
    .line 119
    iput-object p1, p0, Lcom/osp/app/signin/SignInView;->P:Lcom/osp/app/signin/jy;

    return-object p1
.end method

.method static synthetic a(Lcom/osp/app/signin/SignInView;Lcom/osp/app/signin/kd;)Lcom/osp/app/signin/kd;
    .locals 0

    .prologue
    .line 119
    iput-object p1, p0, Lcom/osp/app/signin/SignInView;->U:Lcom/osp/app/signin/kd;

    return-object p1
.end method

.method static synthetic a(Lcom/osp/app/signin/SignInView;Ljava/lang/String;)Ljava/lang/String;
    .locals 0

    .prologue
    .line 119
    iput-object p1, p0, Lcom/osp/app/signin/SignInView;->Q:Ljava/lang/String;

    return-object p1
.end method

.method static synthetic a(Ljava/lang/String;)Ljava/lang/String;
    .locals 2

    .prologue
    .line 119
    invoke-static {p0}, Landroid/telephony/PhoneNumberUtils;->isGlobalPhoneNumber(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    const-string v0, "_"

    invoke-virtual {p0, v0}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v0

    if-lez v0, :cond_0

    const/4 v1, 0x0

    invoke-virtual {p0, v1, v0}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object p0

    :cond_0
    return-object p0
.end method

.method private a(I)V
    .locals 13

    .prologue
    const v12, 0x7f080127

    const v11, 0x7f080126

    const v10, 0x7f080123

    const v9, 0x7f080122

    const v7, 0x7f08011e

    .line 1125
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v0, "SIV"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "initLayoutParams orientation=["

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "]"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    const-string v2, "START"

    invoke-static {v0, v1, v2}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 1127
    invoke-static {}, Lcom/osp/device/b;->b()Lcom/osp/device/b;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/osp/device/b;->i(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 1129
    const v0, 0x7f0c00b3

    invoke-virtual {p0, v0}, Lcom/osp/app/signin/SignInView;->findViewById(I)Landroid/view/View;

    move-result-object v4

    .line 1130
    const v0, 0x7f0c0124

    invoke-virtual {p0, v0}, Lcom/osp/app/signin/SignInView;->findViewById(I)Landroid/view/View;

    move-result-object v5

    .line 1131
    const v0, 0x7f0c0121

    invoke-virtual {p0, v0}, Lcom/osp/app/signin/SignInView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/EditText;

    .line 1132
    const v1, 0x7f0c0122

    invoke-virtual {p0, v1}, Lcom/osp/app/signin/SignInView;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/EditText;

    .line 1134
    invoke-virtual {p0}, Lcom/osp/app/signin/SignInView;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f08011f

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v2

    float-to-int v2, v2

    .line 1135
    invoke-virtual {p0}, Lcom/osp/app/signin/SignInView;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    invoke-virtual {v3, v7}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v3

    float-to-int v3, v3

    .line 1136
    invoke-static {}, Lcom/osp/app/signin/SamsungService;->a()Z

    move-result v6

    if-nez v6, :cond_0

    .line 1138
    invoke-virtual {p0}, Lcom/osp/app/signin/SignInView;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-virtual {v2, v7}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v2

    float-to-int v2, v2

    .line 1140
    :cond_0
    const v6, 0x7f0c0176

    invoke-virtual {p0, v6}, Lcom/osp/app/signin/SignInView;->findViewById(I)Landroid/view/View;

    move-result-object v6

    .line 1141
    invoke-static {}, Lcom/osp/app/util/q;->a()Lcom/osp/app/util/q;

    invoke-static {v6, v2, v3, p1}, Lcom/osp/app/util/q;->b(Landroid/view/View;III)V

    .line 1143
    invoke-static {}, Lcom/osp/app/util/q;->a()Lcom/osp/app/util/q;

    invoke-virtual {p0}, Lcom/osp/app/signin/SignInView;->getResources()Landroid/content/res/Resources;

    move-result-object v6

    const v7, 0x7f080120

    invoke-virtual {v6, v7}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v6

    float-to-int v6, v6

    invoke-virtual {p0}, Lcom/osp/app/signin/SignInView;->getResources()Landroid/content/res/Resources;

    move-result-object v7

    const v8, 0x7f080124

    invoke-virtual {v7, v8}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v7

    float-to-int v7, v7

    invoke-static {v4, v6, v7, p1}, Lcom/osp/app/util/q;->a(Landroid/view/View;III)V

    .line 1145
    invoke-static {}, Lcom/osp/app/util/q;->a()Lcom/osp/app/util/q;

    invoke-virtual {p0}, Lcom/osp/app/signin/SignInView;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    invoke-virtual {v4, v9}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v4

    float-to-int v4, v4

    invoke-virtual {p0}, Lcom/osp/app/signin/SignInView;->getResources()Landroid/content/res/Resources;

    move-result-object v6

    invoke-virtual {v6, v11}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v6

    float-to-int v6, v6

    invoke-static {v0, v4, v6, p1}, Lcom/osp/app/util/q;->a(Landroid/view/View;III)V

    .line 1147
    invoke-static {}, Lcom/osp/app/util/q;->a()Lcom/osp/app/util/q;

    invoke-virtual {p0}, Lcom/osp/app/signin/SignInView;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    invoke-virtual {v4, v10}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v4

    float-to-int v4, v4

    invoke-virtual {p0}, Lcom/osp/app/signin/SignInView;->getResources()Landroid/content/res/Resources;

    move-result-object v6

    invoke-virtual {v6, v12}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v6

    float-to-int v6, v6

    invoke-static {v0, v4, v6, p1}, Lcom/osp/app/util/q;->c(Landroid/view/View;III)V

    .line 1149
    invoke-static {}, Lcom/osp/app/util/q;->a()Lcom/osp/app/util/q;

    invoke-virtual {p0}, Lcom/osp/app/signin/SignInView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v4, 0x7f080121

    invoke-virtual {v0, v4}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v0

    float-to-int v0, v0

    invoke-virtual {p0}, Lcom/osp/app/signin/SignInView;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const v6, 0x7f080125

    invoke-virtual {v4, v6}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v4

    float-to-int v4, v4

    invoke-static {v5, v0, v4, p1}, Lcom/osp/app/util/q;->a(Landroid/view/View;III)V

    .line 1151
    invoke-static {}, Lcom/osp/app/util/q;->a()Lcom/osp/app/util/q;

    invoke-virtual {p0}, Lcom/osp/app/signin/SignInView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0, v9}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v0

    float-to-int v0, v0

    invoke-virtual {p0}, Lcom/osp/app/signin/SignInView;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    invoke-virtual {v4, v11}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v4

    float-to-int v4, v4

    invoke-static {v1, v0, v4, p1}, Lcom/osp/app/util/q;->a(Landroid/view/View;III)V

    .line 1153
    invoke-static {}, Lcom/osp/app/util/q;->a()Lcom/osp/app/util/q;

    invoke-virtual {p0}, Lcom/osp/app/signin/SignInView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0, v10}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v0

    float-to-int v0, v0

    invoke-virtual {p0}, Lcom/osp/app/signin/SignInView;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    invoke-virtual {v4, v12}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v4

    float-to-int v4, v4

    invoke-static {v1, v0, v4, p1}, Lcom/osp/app/util/q;->c(Landroid/view/View;III)V

    .line 1158
    invoke-static {p0}, Lcom/osp/app/util/r;->k(Landroid/content/Context;)Z

    move-result v0

    if-nez v0, :cond_3

    .line 1160
    invoke-virtual {p0}, Lcom/osp/app/signin/SignInView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f08013d

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v0

    float-to-int v2, v0

    .line 1161
    invoke-virtual {p0}, Lcom/osp/app/signin/SignInView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f08013c

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v0

    float-to-int v0, v0

    .line 1164
    :goto_0
    iget-object v1, p0, Lcom/osp/app/signin/SignInView;->at:Lcom/osp/app/signin/BottomSoftkeyLinearLayout;

    if-eqz v1, :cond_2

    .line 1166
    invoke-static {}, Lcom/osp/app/signin/SamsungService;->a()Z

    move-result v1

    if-eqz v1, :cond_1

    sget-object v1, Lcom/osp/app/util/u;->o:[Ljava/lang/String;

    invoke-static {v1}, Lcom/osp/app/util/u;->a([Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 1168
    const/4 v2, 0x0

    .line 1170
    :cond_1
    invoke-static {}, Lcom/osp/app/util/q;->a()Lcom/osp/app/util/q;

    iget-object v1, p0, Lcom/osp/app/signin/SignInView;->at:Lcom/osp/app/signin/BottomSoftkeyLinearLayout;

    invoke-static {p0, v1, v2, v0, p1}, Lcom/osp/app/util/q;->a(Landroid/content/Context;Landroid/view/View;III)V

    .line 1190
    :cond_2
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v0, "SIV"

    const-string v1, "initLayoutParams"

    const-string v2, "END"

    invoke-static {v0, v1, v2}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 1191
    return-void

    :cond_3
    move v0, v3

    goto :goto_0
.end method

.method private a(Lcom/msc/a/g;)V
    .locals 4

    .prologue
    .line 2882
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v0, "SIV"

    const-string v1, "doCheckList"

    const-string v2, "START"

    invoke-static {v0, v1, v2}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 2889
    invoke-virtual {p1}, Lcom/msc/a/g;->d()Z

    move-result v0

    if-nez v0, :cond_0

    iget-boolean v0, p0, Lcom/osp/app/signin/SignInView;->ai:Z

    if-eqz v0, :cond_1

    .line 2891
    :cond_0
    invoke-direct {p0, p1}, Lcom/osp/app/signin/SignInView;->b(Lcom/msc/a/g;)V

    .line 2960
    :goto_0
    return-void

    .line 2898
    :cond_1
    invoke-virtual {p1}, Lcom/msc/a/g;->e()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 2900
    invoke-direct {p0, p1}, Lcom/osp/app/signin/SignInView;->b(Lcom/msc/a/g;)V

    goto :goto_0

    .line 2908
    :cond_2
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "isRequireNameCheck : "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p1}, Lcom/msc/a/g;->g()Z

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;)V

    .line 2909
    invoke-virtual {p1}, Lcom/msc/a/g;->g()Z

    move-result v0

    if-eqz v0, :cond_5

    .line 2911
    invoke-static {}, Lcom/osp/app/signin/SamsungService;->a()Z

    move-result v0

    if-nez v0, :cond_4

    .line 2913
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v0, "SIV"

    const-string v1, "showNameValidationView"

    const-string v2, "START"

    invoke-static {v0, v1, v2}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    new-instance v0, Landroid/app/AlertDialog$Builder;

    invoke-direct {v0, p0}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    const v1, 0x7f09002d

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    const v1, 0x7f09005f

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setMessage(I)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    const v1, 0x7f090064

    new-instance v2, Lcom/osp/app/signin/it;

    invoke-direct {v2, p0, p1}, Lcom/osp/app/signin/it;-><init>(Lcom/osp/app/signin/SignInView;Lcom/msc/a/g;)V

    invoke-virtual {v0, v1, v2}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    new-instance v1, Lcom/osp/app/signin/is;

    invoke-direct {v1, p0}, Lcom/osp/app/signin/is;-><init>(Lcom/osp/app/signin/SignInView;)V

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setOnCancelListener(Landroid/content/DialogInterface$OnCancelListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    :try_start_0
    invoke-virtual {p0}, Lcom/osp/app/signin/SignInView;->isFinishing()Z

    move-result v1

    if-nez v1, :cond_3

    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->show()Landroid/app/AlertDialog;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    :cond_3
    :goto_1
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v0, "SIV"

    const-string v1, "showNameValidationView"

    const-string v2, "END"

    invoke-static {v0, v1, v2}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_1

    .line 2919
    :cond_4
    invoke-direct {p0}, Lcom/osp/app/signin/SignInView;->e()Z

    move-result v0

    if-eqz v0, :cond_6

    .line 2922
    iget-object v0, p0, Lcom/osp/app/signin/SignInView;->S:Landroid/view/View;

    const v1, 0x7f0c0129

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/EditText;

    .line 2927
    :goto_2
    invoke-static {p0}, Landroid/preference/PreferenceManager;->getDefaultSharedPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v1

    invoke-interface {v1}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v1

    .line 2928
    const-string v2, "NAME_VALIDATION_KEY"

    invoke-virtual {v0}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    sget-object v3, Ljava/util/Locale;->ENGLISH:Ljava/util/Locale;

    invoke-virtual {v0, v3}, Ljava/lang/String;->toLowerCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v0

    invoke-interface {v1, v2, v0}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    .line 2929
    invoke-interface {v1}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 2945
    :cond_5
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/osp/app/signin/SignInView;->Q:Ljava/lang/String;

    .line 2948
    invoke-direct {p0}, Lcom/osp/app/signin/SignInView;->q()V

    .line 2959
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v0, "SIV"

    const-string v1, "doCheckList"

    const-string v2, "END"

    invoke-static {v0, v1, v2}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 2925
    :cond_6
    const v0, 0x7f0c0121

    invoke-virtual {p0, v0}, Lcom/osp/app/signin/SignInView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/EditText;

    goto :goto_2
.end method

.method static synthetic a(Lcom/osp/app/signin/SignInView;)V
    .locals 0

    .prologue
    .line 119
    invoke-direct {p0}, Lcom/osp/app/signin/SignInView;->t()V

    return-void
.end method

.method static synthetic a(Lcom/osp/app/signin/SignInView;Landroid/content/Context;)V
    .locals 3

    .prologue
    .line 119
    if-eqz p1, :cond_0

    invoke-static {p1}, Lcom/osp/app/util/ad;->i(Landroid/content/Context;)V

    new-instance v0, Landroid/app/AlertDialog$Builder;

    invoke-direct {v0, p1}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    const v1, 0x7f09004f

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    const v1, 0x7f0900c0

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setMessage(I)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setCancelable(Z)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    const v1, 0x7f090042

    new-instance v2, Lcom/osp/app/signin/jb;

    invoke-direct {v2, p0, p1}, Lcom/osp/app/signin/jb;-><init>(Lcom/osp/app/signin/SignInView;Landroid/content/Context;)V

    invoke-virtual {v0, v1, v2}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    const v1, 0x7f090161

    new-instance v2, Lcom/osp/app/signin/iz;

    invoke-direct {v2, p0}, Lcom/osp/app/signin/iz;-><init>(Lcom/osp/app/signin/SignInView;)V

    invoke-virtual {v0, v1, v2}, Landroid/app/AlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/AlertDialog;->show()V

    :cond_0
    return-void
.end method

.method static synthetic a(Lcom/osp/app/signin/SignInView;Lcom/msc/a/g;)V
    .locals 0

    .prologue
    .line 119
    invoke-direct {p0, p1}, Lcom/osp/app/signin/SignInView;->a(Lcom/msc/a/g;)V

    return-void
.end method

.method private a(Z)V
    .locals 5

    .prologue
    const/4 v4, -0x1

    const/4 v0, 0x1

    .line 1694
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v1, "SIV"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "sendSignInResult isEmailValid=["

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "]"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    const-string v3, "START"

    invoke-static {v1, v2, v3}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 1696
    if-nez p1, :cond_1

    .line 1698
    invoke-static {}, Lcom/osp/app/signin/SamsungService;->a()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 1703
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v1, "Email validation required, Backup and Restore is skipped"

    invoke-static {v1}, Lcom/osp/app/util/aq;->c(Ljava/lang/String;)V

    .line 1704
    new-instance v1, Landroid/content/Intent;

    invoke-direct {v1}, Landroid/content/Intent;-><init>()V

    .line 1705
    const-string v2, "signin_without_email_verification"

    invoke-virtual {v1, v2, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 1706
    invoke-virtual {p0, v4, v1}, Lcom/osp/app/signin/SignInView;->a(ILandroid/content/Intent;)V

    .line 1707
    invoke-virtual {p0}, Lcom/osp/app/signin/SignInView;->finish()V

    .line 1831
    :goto_0
    return-void

    .line 1712
    :cond_0
    const/16 v0, 0xd

    invoke-virtual {p0, v0}, Lcom/osp/app/signin/SignInView;->b(I)V

    .line 1713
    invoke-virtual {p0}, Lcom/osp/app/signin/SignInView;->finish()V

    goto :goto_0

    .line 1717
    :cond_1
    invoke-static {}, Lcom/osp/app/util/ap;->a()Lcom/osp/app/util/ap;

    const v1, 0x7f090054

    invoke-virtual {p0, v1}, Lcom/osp/app/signin/SignInView;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-static {p0, v1, v0}, Lcom/osp/app/util/ap;->a(Landroid/content/Context;Ljava/lang/String;I)Landroid/widget/Toast;

    move-result-object v1

    invoke-virtual {v1}, Landroid/widget/Toast;->show()V

    .line 1721
    invoke-static {}, Lcom/osp/app/signin/SamsungService;->a()Z

    move-result v1

    if-nez v1, :cond_5

    iget-object v1, p0, Lcom/osp/app/signin/SignInView;->U:Lcom/osp/app/signin/kd;

    if-eqz v1, :cond_5

    iget-object v1, p0, Lcom/osp/app/signin/SignInView;->U:Lcom/osp/app/signin/kd;

    invoke-virtual {v1}, Lcom/osp/app/signin/kd;->b()I

    move-result v1

    if-lez v1, :cond_5

    .line 1724
    iget-object v0, p0, Lcom/osp/app/signin/SignInView;->U:Lcom/osp/app/signin/kd;

    iget-boolean v0, v0, Lcom/osp/app/signin/kd;->e:Z

    if-nez v0, :cond_2

    iget-object v0, p0, Lcom/osp/app/signin/SignInView;->U:Lcom/osp/app/signin/kd;

    iget-boolean v0, v0, Lcom/osp/app/signin/kd;->d:Z

    if-eqz v0, :cond_3

    .line 1726
    :cond_2
    const v0, 0x7f0900c5

    invoke-virtual {p0, v0}, Lcom/osp/app/signin/SignInView;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 1735
    :goto_1
    new-instance v1, Landroid/app/AlertDialog$Builder;

    invoke-direct {v1, p0}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    const v2, 0x7f09002d

    invoke-virtual {v1, v2}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/app/AlertDialog$Builder;->setMessage(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    const v1, 0x7f090064

    new-instance v2, Lcom/osp/app/signin/ia;

    invoke-direct {v2, p0}, Lcom/osp/app/signin/ia;-><init>(Lcom/osp/app/signin/SignInView;)V

    invoke-virtual {v0, v1, v2}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    .line 1802
    :try_start_0
    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->show()Landroid/app/AlertDialog;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 1830
    :goto_2
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v0, "SIV"

    const-string v1, "sendSignInResult"

    const-string v2, "END"

    invoke-static {v0, v1, v2}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 1727
    :cond_3
    iget-object v0, p0, Lcom/osp/app/signin/SignInView;->U:Lcom/osp/app/signin/kd;

    iget-boolean v0, v0, Lcom/osp/app/signin/kd;->b:Z

    if-eqz v0, :cond_4

    .line 1729
    const v0, 0x7f0900c4

    invoke-virtual {p0, v0}, Lcom/osp/app/signin/SignInView;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_1

    .line 1732
    :cond_4
    const v0, 0x7f0900c3

    invoke-virtual {p0, v0}, Lcom/osp/app/signin/SignInView;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_1

    .line 1803
    :catch_0
    move-exception v0

    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_2

    .line 1810
    :cond_5
    const-string v1, "AGREE_TO_DISCLAIMER"

    iget-object v2, p0, Lcom/osp/app/signin/SignInView;->B:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_6

    .line 1812
    iget-object v1, p0, Lcom/osp/app/signin/SignInView;->V:Lcom/msc/a/g;

    invoke-virtual {v1}, Lcom/msc/a/g;->j()Z

    move-result v1

    if-eqz v1, :cond_6

    .line 1814
    iget-object v1, p0, Lcom/osp/app/signin/SignInView;->y:Landroid/content/Intent;

    const-string v2, "is_agree_to_disclaimer"

    iget-object v3, p0, Lcom/osp/app/signin/SignInView;->V:Lcom/msc/a/g;

    invoke-virtual {v3}, Lcom/msc/a/g;->k()Z

    move-result v3

    if-nez v3, :cond_7

    :goto_3
    invoke-virtual {v1, v2, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 1817
    :cond_6
    iget-object v0, p0, Lcom/osp/app/signin/SignInView;->y:Landroid/content/Intent;

    const-string v1, "authcode"

    iget-object v2, p0, Lcom/osp/app/signin/SignInView;->R:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 1818
    iget-object v0, p0, Lcom/osp/app/signin/SignInView;->y:Landroid/content/Intent;

    invoke-virtual {p0, v4, v0}, Lcom/osp/app/signin/SignInView;->a(ILandroid/content/Intent;)V

    .line 1827
    invoke-virtual {p0}, Lcom/osp/app/signin/SignInView;->finish()V

    goto :goto_2

    .line 1814
    :cond_7
    const/4 v0, 0x0

    goto :goto_3
.end method

.method static synthetic a(Lcom/osp/app/signin/SignInView;Z)Z
    .locals 0

    .prologue
    .line 119
    iput-boolean p1, p0, Lcom/osp/app/signin/SignInView;->an:Z

    return p1
.end method

.method static synthetic aa(Lcom/osp/app/signin/SignInView;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 119
    iget-object v0, p0, Lcom/osp/app/signin/SignInView;->aq:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic ab(Lcom/osp/app/signin/SignInView;)I
    .locals 1

    .prologue
    .line 119
    iget v0, p0, Lcom/osp/app/signin/SignInView;->ae:I

    return v0
.end method

.method static synthetic ac(Lcom/osp/app/signin/SignInView;)Landroid/app/AlertDialog;
    .locals 1

    .prologue
    .line 119
    iget-object v0, p0, Lcom/osp/app/signin/SignInView;->af:Landroid/app/AlertDialog;

    return-object v0
.end method

.method static synthetic b(Lcom/osp/app/signin/SignInView;Lcom/msc/a/g;)Lcom/msc/a/g;
    .locals 0

    .prologue
    .line 119
    iput-object p1, p0, Lcom/osp/app/signin/SignInView;->V:Lcom/msc/a/g;

    return-object p1
.end method

.method static synthetic b(Lcom/osp/app/signin/SignInView;Ljava/lang/String;)Ljava/lang/String;
    .locals 0

    .prologue
    .line 119
    iput-object p1, p0, Lcom/osp/app/signin/SignInView;->ad:Ljava/lang/String;

    return-object p1
.end method

.method private b(Landroid/widget/TextView;)V
    .locals 3

    .prologue
    .line 2561
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "<a href=\""

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0}, Lcom/osp/app/signin/SignInView;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {p0}, Lcom/osp/app/signin/SignInView;->h()Z

    move-result v2

    invoke-static {v1, v2}, Lcom/msc/c/g;->a(Landroid/content/Context;Z)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "\">"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 2562
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const v1, 0x7f0900f1

    invoke-virtual {p0, v1}, Lcom/osp/app/signin/SignInView;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "</a>"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/Html;->fromHtml(Ljava/lang/String;)Landroid/text/Spanned;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 2574
    new-instance v0, Lcom/osp/app/signin/ip;

    invoke-direct {v0, p0}, Lcom/osp/app/signin/ip;-><init>(Lcom/osp/app/signin/SignInView;)V

    invoke-virtual {p1, v0}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2616
    new-instance v0, Lcom/osp/app/signin/iq;

    invoke-direct {v0, p0}, Lcom/osp/app/signin/iq;-><init>(Lcom/osp/app/signin/SignInView;)V

    invoke-virtual {p1, v0}, Landroid/widget/TextView;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 2628
    return-void
.end method

.method private b(Lcom/msc/a/g;)V
    .locals 4

    .prologue
    const/4 v3, 0x1

    .line 3288
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v0, "SIV"

    const-string v1, "LaunchNewTnc"

    const-string v2, "START"

    invoke-static {v0, v1, v2}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 3289
    if-nez p1, :cond_0

    .line 3291
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v0, "LaunchNewTnc : checkListResult is null"

    invoke-static {v0}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;)V

    .line 3293
    :cond_0
    invoke-direct {p0}, Lcom/osp/app/signin/SignInView;->e()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 3297
    iget-object v0, p0, Lcom/osp/app/signin/SignInView;->S:Landroid/view/View;

    const v1, 0x7f0c0129

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/EditText;

    .line 3303
    :goto_0
    new-instance v1, Landroid/content/Intent;

    const-class v2, Lcom/osp/app/signin/TnCView;

    invoke-direct {v1, p0, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 3304
    const-string v2, "com.msc.action.samsungaccount.Update_NewTerms"

    invoke-virtual {v1, v2}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 3305
    const/high16 v2, 0x20000

    invoke-virtual {v1, v2}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    .line 3307
    const-string v2, "key_tnc_update_mode"

    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 3308
    const-string v2, "email_id"

    invoke-virtual {v0}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v2, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 3309
    invoke-direct {p0}, Lcom/osp/app/signin/SignInView;->v()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 3311
    const-string v0, "key_resign_in_another_id"

    invoke-virtual {v1, v0, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 3314
    :cond_1
    const-string v0, "country_code_mcc"

    iget-object v2, p0, Lcom/osp/app/signin/SignInView;->am:Ljava/lang/String;

    invoke-virtual {v1, v0, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 3316
    const-string v0, "key_easy_signup_tnc_mode"

    iget-boolean v2, p0, Lcom/osp/app/signin/SignInView;->ai:Z

    invoke-virtual {v1, v0, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 3320
    const/16 v0, 0xd1

    invoke-virtual {p0, v1, v0}, Lcom/osp/app/signin/SignInView;->startActivityForResult(Landroid/content/Intent;I)V

    .line 3324
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v0, "SIV"

    const-string v1, "LaunchNewTnc"

    const-string v2, "END"

    invoke-static {v0, v1, v2}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 3325
    return-void

    .line 3300
    :cond_2
    const v0, 0x7f0c0121

    invoke-virtual {p0, v0}, Lcom/osp/app/signin/SignInView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/EditText;

    goto :goto_0
.end method

.method static synthetic b(Lcom/osp/app/signin/SignInView;)V
    .locals 0

    .prologue
    .line 119
    invoke-direct {p0}, Lcom/osp/app/signin/SignInView;->g()V

    return-void
.end method

.method static synthetic b(Lcom/osp/app/signin/SignInView;Landroid/content/Context;)V
    .locals 3

    .prologue
    .line 119
    if-eqz p1, :cond_0

    invoke-static {p1}, Lcom/osp/app/util/ad;->i(Landroid/content/Context;)V

    invoke-static {p0}, Lcom/osp/app/util/ad;->u(Landroid/content/Context;)V

    new-instance v0, Landroid/app/AlertDialog$Builder;

    invoke-direct {v0, p1}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    const v1, 0x7f09004f

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    const v1, 0x7f090196

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setMessage(I)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setCancelable(Z)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    const v1, 0x7f090042

    new-instance v2, Lcom/osp/app/signin/jd;

    invoke-direct {v2, p0}, Lcom/osp/app/signin/jd;-><init>(Lcom/osp/app/signin/SignInView;)V

    invoke-virtual {v0, v1, v2}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    const v1, 0x7f090161

    new-instance v2, Lcom/osp/app/signin/jc;

    invoke-direct {v2, p0}, Lcom/osp/app/signin/jc;-><init>(Lcom/osp/app/signin/SignInView;)V

    invoke-virtual {v0, v1, v2}, Landroid/app/AlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/AlertDialog;->show()V

    :cond_0
    return-void
.end method

.method static synthetic b(Lcom/osp/app/signin/SignInView;Z)Z
    .locals 0

    .prologue
    .line 119
    iput-boolean p1, p0, Lcom/osp/app/signin/SignInView;->Z:Z

    return p1
.end method

.method static synthetic b(Ljava/lang/String;)Z
    .locals 1

    .prologue
    .line 119
    invoke-static {p0}, Lcom/osp/app/signin/SignInView;->f(Ljava/lang/String;)Z

    move-result v0

    return v0
.end method

.method static synthetic c(Lcom/osp/app/signin/SignInView;)Landroid/content/Intent;
    .locals 1

    .prologue
    .line 119
    iget-object v0, p0, Lcom/osp/app/signin/SignInView;->y:Landroid/content/Intent;

    return-object v0
.end method

.method static synthetic c(Lcom/osp/app/signin/SignInView;Ljava/lang/String;)Ljava/lang/String;
    .locals 0

    .prologue
    .line 119
    iput-object p1, p0, Lcom/osp/app/signin/SignInView;->am:Ljava/lang/String;

    return-object p1
.end method

.method static synthetic c(Lcom/osp/app/signin/SignInView;Z)V
    .locals 0

    .prologue
    .line 119
    invoke-direct {p0, p1}, Lcom/osp/app/signin/SignInView;->a(Z)V

    return-void
.end method

.method static synthetic d(Lcom/osp/app/signin/SignInView;Ljava/lang/String;)Ljava/lang/String;
    .locals 0

    .prologue
    .line 119
    iput-object p1, p0, Lcom/osp/app/signin/SignInView;->al:Ljava/lang/String;

    return-object p1
.end method

.method static synthetic d(Lcom/osp/app/signin/SignInView;)Z
    .locals 1

    .prologue
    .line 119
    iget-boolean v0, p0, Lcom/osp/app/signin/SignInView;->W:Z

    return v0
.end method

.method static synthetic e(Lcom/osp/app/signin/SignInView;Ljava/lang/String;)Ljava/lang/String;
    .locals 0

    .prologue
    .line 119
    iput-object p1, p0, Lcom/osp/app/signin/SignInView;->H:Ljava/lang/String;

    return-object p1
.end method

.method private e()Z
    .locals 3

    .prologue
    .line 517
    const-string v0, "SIGNIN_POPUP"

    invoke-virtual {p0}, Lcom/osp/app/signin/SignInView;->getIntent()Landroid/content/Intent;

    move-result-object v1

    const-string v2, "MODE"

    invoke-virtual {v1, v2}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-static {p0}, Lcom/osp/app/util/r;->m(Landroid/content/Context;)Z

    .line 520
    const/4 v0, 0x1

    .line 522
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method static synthetic e(Lcom/osp/app/signin/SignInView;)Z
    .locals 1

    .prologue
    .line 119
    invoke-direct {p0}, Lcom/osp/app/signin/SignInView;->e()Z

    move-result v0

    return v0
.end method

.method static synthetic f(Lcom/osp/app/signin/SignInView;Ljava/lang/String;)Ljava/lang/String;
    .locals 0

    .prologue
    .line 119
    iput-object p1, p0, Lcom/osp/app/signin/SignInView;->R:Ljava/lang/String;

    return-object p1
.end method

.method private f()V
    .locals 3

    .prologue
    const v1, 0x7f09004f

    const v2, 0x7f090042

    .line 782
    iget-boolean v0, p0, Lcom/osp/app/signin/SignInView;->ab:Z

    if-eqz v0, :cond_0

    .line 784
    new-instance v0, Landroid/app/AlertDialog$Builder;

    invoke-direct {v0, p0}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    const v1, 0x7f0900d2

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setMessage(I)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    new-instance v1, Lcom/osp/app/signin/je;

    invoke-direct {v1, p0}, Lcom/osp/app/signin/je;-><init>(Lcom/osp/app/signin/SignInView;)V

    invoke-virtual {v0, v2, v1}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    new-instance v1, Lcom/osp/app/signin/ja;

    invoke-direct {v1, p0}, Lcom/osp/app/signin/ja;-><init>(Lcom/osp/app/signin/SignInView;)V

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setOnCancelListener(Landroid/content/DialogInterface$OnCancelListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->show()Landroid/app/AlertDialog;

    .line 827
    :goto_0
    return-void

    .line 806
    :cond_0
    new-instance v0, Landroid/app/AlertDialog$Builder;

    invoke-direct {v0, p0}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    const v1, 0x7f09017b

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setMessage(I)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    new-instance v1, Lcom/osp/app/signin/jg;

    invoke-direct {v1, p0}, Lcom/osp/app/signin/jg;-><init>(Lcom/osp/app/signin/SignInView;)V

    invoke-virtual {v0, v2, v1}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    new-instance v1, Lcom/osp/app/signin/jf;

    invoke-direct {v1, p0}, Lcom/osp/app/signin/jf;-><init>(Lcom/osp/app/signin/SignInView;)V

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setOnCancelListener(Landroid/content/DialogInterface$OnCancelListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->show()Landroid/app/AlertDialog;

    goto :goto_0
.end method

.method static synthetic f(Lcom/osp/app/signin/SignInView;)V
    .locals 4

    .prologue
    .line 119
    invoke-direct {p0}, Lcom/osp/app/signin/SignInView;->e()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/osp/app/signin/SignInView;->S:Landroid/view/View;

    const v1, 0x7f0c012a

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/EditText;

    :goto_0
    invoke-virtual {v0}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    new-instance v1, Landroid/content/Intent;

    const-class v2, Lcom/osp/app/signin/CheckNameBirthdateActivity;

    invoke-direct {v1, p0, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const-string v2, "required_auth"

    iget-boolean v3, p0, Lcom/osp/app/signin/SignInView;->ac:Z

    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    const-string v2, "client_id"

    iget-object v3, p0, Lcom/osp/app/signin/SignInView;->z:Ljava/lang/String;

    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    const-string v2, "client_secret"

    iget-object v3, p0, Lcom/osp/app/signin/SignInView;->A:Ljava/lang/String;

    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    const-string v2, "key_login_id"

    iget-object v3, p0, Lcom/osp/app/signin/SignInView;->H:Ljava/lang/String;

    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    const-string v2, "key_password"

    invoke-virtual {v1, v2, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    const-string v0, "key_internal_is_resign_in"

    iget-boolean v2, p0, Lcom/osp/app/signin/SignInView;->aa:Z

    invoke-virtual {v1, v0, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    const-string v0, "is_cancelable_just_one_activity"

    const/4 v2, 0x1

    invoke-virtual {v1, v0, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    const/16 v0, 0xe5

    invoke-virtual {p0, v1, v0}, Lcom/osp/app/signin/SignInView;->startActivityForResult(Landroid/content/Intent;I)V

    return-void

    :cond_0
    const v0, 0x7f0c0122

    invoke-virtual {p0, v0}, Lcom/osp/app/signin/SignInView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/EditText;

    goto :goto_0
.end method

.method private g()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 2788
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v0, "SIV"

    const-string v1, "executeCheckList"

    const-string v2, "START"

    invoke-static {v0, v1, v2}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 2790
    iget-boolean v0, p0, Lcom/osp/app/signin/SignInView;->aa:Z

    if-nez v0, :cond_0

    iget-boolean v0, p0, Lcom/osp/app/signin/SignInView;->ac:Z

    if-eqz v0, :cond_1

    .line 2792
    :cond_0
    invoke-static {p0}, Lcom/msc/c/c;->g(Landroid/content/Context;)V

    .line 2793
    invoke-static {p0}, Lcom/msc/c/e;->a(Landroid/content/Context;)Z

    .line 2794
    invoke-static {p0}, Lcom/osp/app/util/ad;->r(Landroid/content/Context;)V

    .line 2796
    :cond_1
    invoke-direct {p0}, Lcom/osp/app/signin/SignInView;->r()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 2798
    invoke-static {p0}, Lcom/osp/app/util/ad;->a(Landroid/content/Context;)Z

    move-result v0

    if-nez v0, :cond_3

    .line 2800
    const/16 v0, 0xd5

    invoke-virtual {p0, v0}, Lcom/osp/app/signin/SignInView;->c(I)V

    .line 2869
    :cond_2
    :goto_0
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v0, "SIV"

    const-string v1, "CheckListExecute"

    const-string v2, "END"

    invoke-static {v0, v1, v2}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 2870
    return-void

    .line 2804
    :cond_3
    invoke-virtual {p0}, Lcom/osp/app/signin/SignInView;->d()V

    .line 2825
    invoke-static {p0}, Lcom/osp/app/util/ad;->d(Landroid/content/Context;)Z

    move-result v0

    if-nez v0, :cond_4

    .line 2827
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v0, "DBMV2"

    const-string v1, "removeMccCountryCode"

    invoke-static {v0, v1}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;)V

    if-eqz p0, :cond_4

    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    if-eqz v0, :cond_4

    sget-object v1, Lcom/msc/contentprovider/SamsungServiceProvider;->a:Landroid/net/Uri;

    invoke-virtual {v0, v1, v3, v3}, Landroid/content/ContentResolver;->delete(Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;)I

    .line 2832
    :cond_4
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/osp/app/signin/SignInView;->ap:J

    .line 2838
    iget-object v0, p0, Lcom/osp/app/signin/SignInView;->O:Lcom/osp/app/signin/jk;

    if-eqz v0, :cond_5

    iget-object v0, p0, Lcom/osp/app/signin/SignInView;->O:Lcom/osp/app/signin/jk;

    invoke-virtual {v0}, Lcom/osp/app/signin/jk;->getStatus()Landroid/os/AsyncTask$Status;

    move-result-object v0

    sget-object v1, Landroid/os/AsyncTask$Status;->RUNNING:Landroid/os/AsyncTask$Status;

    if-ne v0, v1, :cond_5

    .line 2840
    iget-object v0, p0, Lcom/osp/app/signin/SignInView;->O:Lcom/osp/app/signin/jk;

    invoke-virtual {v0}, Lcom/osp/app/signin/jk;->d()V

    .line 2841
    iput-object v3, p0, Lcom/osp/app/signin/SignInView;->O:Lcom/osp/app/signin/jk;

    .line 2843
    :cond_5
    new-instance v0, Lcom/osp/app/signin/jk;

    invoke-direct {v0, p0}, Lcom/osp/app/signin/jk;-><init>(Lcom/osp/app/signin/SignInView;)V

    iput-object v0, p0, Lcom/osp/app/signin/SignInView;->O:Lcom/osp/app/signin/jk;

    .line 2844
    iget-object v0, p0, Lcom/osp/app/signin/SignInView;->O:Lcom/osp/app/signin/jk;

    invoke-virtual {v0}, Lcom/osp/app/signin/jk;->b()V

    goto :goto_0
.end method

.method static synthetic g(Lcom/osp/app/signin/SignInView;)V
    .locals 6

    .prologue
    .line 119
    invoke-direct {p0}, Lcom/osp/app/signin/SignInView;->e()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/osp/app/signin/SignInView;->S:Landroid/view/View;

    const v1, 0x7f0c012a

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/EditText;

    :goto_0
    invoke-virtual {v0}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    new-instance v1, Landroid/content/Intent;

    const-class v2, Lcom/osp/app/signin/SmsVerificationActivity;

    invoke-direct {v1, p0, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const-string v2, "is_phnumber_verification_mode"

    const-string v3, "FROM_SIGN_IN_FLOW"

    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    const-string v2, "client_id"

    iget-object v3, p0, Lcom/osp/app/signin/SignInView;->z:Ljava/lang/String;

    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    const-string v2, "client_secret"

    iget-object v3, p0, Lcom/osp/app/signin/SignInView;->A:Ljava/lang/String;

    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    const-string v2, "required_auth"

    iget-boolean v3, p0, Lcom/osp/app/signin/SignInView;->ac:Z

    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    const-string v2, "key_internal_is_resign_in"

    iget-boolean v3, p0, Lcom/osp/app/signin/SignInView;->aa:Z

    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "+"

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v3, p0, Lcom/osp/app/signin/SignInView;->H:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {p0}, Lcom/osp/app/util/ad;->p(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/osp/app/util/ad;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/google/i18n/phonenumbers/ac;

    move-result-object v2

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2}, Lcom/google/i18n/phonenumbers/ac;->b()J

    move-result-wide v4

    invoke-virtual {v3, v4, v5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2}, Lcom/google/i18n/phonenumbers/ac;->a()I

    move-result v2

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    const-string v4, "key_phonenumber"

    invoke-virtual {v1, v4, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    const-string v3, "key_country_calling_code"

    invoke-virtual {v1, v3, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    const-string v2, "key_password"

    invoke-virtual {v1, v2, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    const-string v0, "key_user_id"

    iget-object v2, p0, Lcom/osp/app/signin/SignInView;->ad:Ljava/lang/String;

    invoke-virtual {v1, v0, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    const-string v0, "is_cancelable_just_one_activity"

    const/4 v2, 0x1

    invoke-virtual {v1, v0, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    const/16 v0, 0xe6

    invoke-virtual {p0, v1, v0}, Lcom/osp/app/signin/SignInView;->startActivityForResult(Landroid/content/Intent;I)V

    return-void

    :cond_0
    const v0, 0x7f0c0122

    invoke-virtual {p0, v0}, Lcom/osp/app/signin/SignInView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/EditText;

    goto/16 :goto_0
.end method

.method static synthetic g(Lcom/osp/app/signin/SignInView;Ljava/lang/String;)V
    .locals 4

    .prologue
    const/4 v3, 0x1

    .line 119
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v0, "SIV"

    const-string v1, "showEmailValidationView"

    const-string v2, "START"

    invoke-static {v0, v1, v2}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "showEmailValidationView - emailID : "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;)V

    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v0, "showEmailValidationView - isResendMode : true"

    invoke-static {v0}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;)V

    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/osp/app/signin/EmailValidationView;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const-string v1, "email_id"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    const-string v1, "is_resend"

    invoke-virtual {v0, v1, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    invoke-direct {p0}, Lcom/osp/app/signin/SignInView;->e()Z

    move-result v1

    if-eqz v1, :cond_0

    const-string v1, "key_verifypopup_mode"

    invoke-virtual {v0, v1, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    :cond_0
    const-string v1, "is_bgmode"

    const/16 v2, 0xcc

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    const-string v1, "key_request_id"

    iget-wide v2, p0, Lcom/osp/app/signin/SignInView;->ak:J

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;J)Landroid/content/Intent;

    const/16 v1, 0xc9

    invoke-virtual {p0, v0, v1}, Lcom/osp/app/signin/SignInView;->startActivityForResult(Landroid/content/Intent;I)V

    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v0, "SIV"

    const-string v1, "showEmailValidationView"

    const-string v2, "END"

    invoke-static {v0, v1, v2}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method

.method static synthetic h(Lcom/osp/app/signin/SignInView;)V
    .locals 2

    .prologue
    .line 119
    new-instance v0, Landroid/content/Intent;

    const-string v1, "com.msc.action.samsungaccount.web_with_sign_in_screen_button"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    const/16 v1, 0xf4

    invoke-virtual {p0, v0, v1}, Lcom/osp/app/signin/SignInView;->startActivityForResult(Landroid/content/Intent;I)V

    return-void
.end method

.method static synthetic i(Lcom/osp/app/signin/SignInView;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 119
    iget-object v0, p0, Lcom/osp/app/signin/SignInView;->G:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic j(Lcom/osp/app/signin/SignInView;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 119
    iget-object v0, p0, Lcom/osp/app/signin/SignInView;->z:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic k(Lcom/osp/app/signin/SignInView;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 119
    iget-object v0, p0, Lcom/osp/app/signin/SignInView;->A:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic l(Lcom/osp/app/signin/SignInView;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 119
    iget-object v0, p0, Lcom/osp/app/signin/SignInView;->B:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic m(Lcom/osp/app/signin/SignInView;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 119
    iget-object v0, p0, Lcom/osp/app/signin/SignInView;->E:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic n(Lcom/osp/app/signin/SignInView;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 119
    iget-object v0, p0, Lcom/osp/app/signin/SignInView;->D:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic o(Lcom/osp/app/signin/SignInView;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 119
    iget-object v0, p0, Lcom/osp/app/signin/SignInView;->R:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic p(Lcom/osp/app/signin/SignInView;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 119
    iget-object v0, p0, Lcom/osp/app/signin/SignInView;->F:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic q(Lcom/osp/app/signin/SignInView;)J
    .locals 2

    .prologue
    .line 119
    iget-wide v0, p0, Lcom/osp/app/signin/SignInView;->ak:J

    return-wide v0
.end method

.method private q()V
    .locals 1

    .prologue
    .line 3037
    new-instance v0, Lcom/osp/app/signin/jv;

    invoke-direct {v0, p0, p0}, Lcom/osp/app/signin/jv;-><init>(Lcom/osp/app/signin/SignInView;Landroid/content/Context;)V

    iput-object v0, p0, Lcom/osp/app/signin/SignInView;->L:Lcom/osp/app/signin/jv;

    .line 3039
    iget-object v0, p0, Lcom/osp/app/signin/SignInView;->L:Lcom/osp/app/signin/jv;

    invoke-virtual {v0}, Lcom/osp/app/signin/jv;->b()V

    .line 3041
    return-void
.end method

.method static synthetic r(Lcom/osp/app/signin/SignInView;)Landroid/os/Handler;
    .locals 1

    .prologue
    .line 119
    iget-object v0, p0, Lcom/osp/app/signin/SignInView;->X:Landroid/os/Handler;

    return-object v0
.end method

.method private r()Z
    .locals 6

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 3609
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v0, "SIV"

    const-string v1, "validateSignInFields"

    const-string v4, "START"

    invoke-static {v0, v1, v4}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 3610
    invoke-direct {p0}, Lcom/osp/app/signin/SignInView;->e()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 3613
    iget-object v0, p0, Lcom/osp/app/signin/SignInView;->S:Landroid/view/View;

    const v1, 0x7f0c0129

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/EditText;

    .line 3620
    :goto_0
    const/4 v1, 0x0

    .line 3642
    :try_start_0
    invoke-virtual {v0}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v4

    sget-object v5, Ljava/util/Locale;->ENGLISH:Ljava/util/Locale;

    invoke-virtual {v4, v5}, Ljava/lang/String;->toLowerCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v4

    .line 3644
    new-instance v5, Lcom/osp/security/identity/g;

    invoke-direct {v5, p0}, Lcom/osp/security/identity/g;-><init>(Landroid/content/Context;)V

    .line 3646
    invoke-static {}, Lcom/osp/device/b;->b()Lcom/osp/device/b;

    move-result-object v5

    invoke-virtual {v5}, Lcom/osp/device/b;->a()Z

    move-result v5

    if-eqz v5, :cond_2

    invoke-static {v4}, Landroid/telephony/PhoneNumberUtils;->isGlobalPhoneNumber(Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_2

    .line 3651
    sget-object v4, Lcom/osp/security/identity/h;->a:Lcom/osp/security/identity/h;

    .line 3660
    :goto_1
    invoke-static {}, Lcom/osp/device/b;->b()Lcom/osp/device/b;

    move-result-object v5

    invoke-virtual {v5}, Lcom/osp/device/b;->a()Z

    move-result v5

    if-eqz v5, :cond_3

    sget-object v5, Lcom/osp/security/identity/h;->i:Lcom/osp/security/identity/h;

    if-ne v4, v5, :cond_3

    .line 3662
    invoke-virtual {v0}, Landroid/widget/EditText;->requestFocus()Z

    .line 3663
    const v0, 0x7f0900ed

    invoke-virtual {p0, v0}, Lcom/osp/app/signin/SignInView;->getText(I)Ljava/lang/CharSequence;

    move-result-object v0

    invoke-interface {v0}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    move v1, v2

    .line 3731
    :goto_2
    if-nez v1, :cond_0

    if-eqz v0, :cond_0

    .line 3733
    invoke-static {}, Lcom/osp/app/util/ap;->a()Lcom/osp/app/util/ap;

    invoke-static {p0, v0, v2}, Lcom/osp/app/util/ap;->a(Landroid/content/Context;Ljava/lang/String;I)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    .line 3734
    invoke-direct {p0}, Lcom/osp/app/signin/SignInView;->e()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 3736
    iget-object v0, p0, Lcom/osp/app/signin/SignInView;->Y:Landroid/os/Handler;

    invoke-virtual {v0, v2}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    .line 3741
    :cond_0
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v0, "SIV"

    const-string v2, "validateSignInFields"

    const-string v3, "END"

    invoke-static {v0, v2, v3}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 3742
    return v1

    .line 3616
    :cond_1
    const v0, 0x7f0c0121

    invoke-virtual {p0, v0}, Lcom/osp/app/signin/SignInView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/EditText;

    goto :goto_0

    .line 3654
    :cond_2
    const/4 v5, 0x0

    :try_start_1
    invoke-static {v4, v5}, Lcom/osp/security/identity/g;->a(Ljava/lang/String;Z)Lcom/osp/security/identity/h;

    move-result-object v4

    goto :goto_1

    .line 3664
    :cond_3
    sget-object v5, Lcom/osp/security/identity/h;->d:Lcom/osp/security/identity/h;

    if-ne v4, v5, :cond_4

    .line 3666
    invoke-virtual {v0}, Landroid/widget/EditText;->requestFocus()Z

    .line 3667
    const v0, 0x7f0900ed

    invoke-virtual {p0, v0}, Lcom/osp/app/signin/SignInView;->getText(I)Ljava/lang/CharSequence;

    move-result-object v0

    invoke-interface {v0}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v0

    move v1, v2

    goto :goto_2

    .line 3668
    :cond_4
    sget-object v5, Lcom/osp/security/identity/h;->b:Lcom/osp/security/identity/h;

    if-eq v4, v5, :cond_5

    sget-object v5, Lcom/osp/security/identity/h;->c:Lcom/osp/security/identity/h;

    if-ne v4, v5, :cond_6

    .line 3670
    :cond_5
    invoke-virtual {v0}, Landroid/widget/EditText;->requestFocus()Z

    .line 3674
    const v0, 0x7f090091

    const/4 v3, 0x2

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    const/4 v5, 0x5

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v3, v4

    const/4 v4, 0x1

    const/16 v5, 0x32

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v3, v4

    invoke-virtual {p0, v0, v3}, Lcom/osp/app/signin/SignInView;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    move v1, v2

    goto :goto_2

    .line 3675
    :cond_6
    sget-object v5, Lcom/osp/security/identity/h;->f:Lcom/osp/security/identity/h;

    if-ne v4, v5, :cond_7

    .line 3677
    invoke-virtual {v0}, Landroid/widget/EditText;->requestFocus()Z

    .line 3678
    const v0, 0x7f090032

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    const-string v5, "admin, administrator, samsung, samsungapps, supervisor, tizenaccount, tizenstore, tizen"

    aput-object v5, v3, v4

    invoke-virtual {p0, v0, v3}, Lcom/osp/app/signin/SignInView;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    move v1, v2

    goto/16 :goto_2

    .line 3679
    :cond_7
    sget-object v5, Lcom/osp/security/identity/h;->e:Lcom/osp/security/identity/h;

    if-ne v4, v5, :cond_8

    .line 3681
    invoke-virtual {v0}, Landroid/widget/EditText;->requestFocus()Z

    .line 3682
    const v0, 0x7f090070

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    const-string v5, ".-_+ "

    aput-object v5, v3, v4

    invoke-virtual {p0, v0, v3}, Lcom/osp/app/signin/SignInView;->getString(I[Ljava/lang/Object;)Ljava/lang/String;
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    move-result-object v0

    move v1, v2

    goto/16 :goto_2

    :cond_8
    move-object v0, v1

    move v1, v3

    .line 3730
    goto/16 :goto_2

    .line 3727
    :catch_0
    move-exception v0

    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    move-object v0, v1

    move v1, v2

    goto/16 :goto_2
.end method

.method static synthetic s(Lcom/osp/app/signin/SignInView;)Landroid/view/View;
    .locals 1

    .prologue
    .line 119
    iget-object v0, p0, Lcom/osp/app/signin/SignInView;->S:Landroid/view/View;

    return-object v0
.end method

.method private s()V
    .locals 8

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 3754
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v0, "SIV"

    const-string v3, "checkSignInFields"

    const-string v4, "START"

    invoke-static {v0, v3, v4}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 3756
    const/4 v3, 0x0

    .line 3768
    :try_start_0
    new-instance v0, Lcom/osp/security/identity/g;

    invoke-direct {v0, p0}, Lcom/osp/security/identity/g;-><init>(Landroid/content/Context;)V

    .line 3769
    invoke-direct {p0}, Lcom/osp/app/signin/SignInView;->e()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 3775
    iget-object v0, p0, Lcom/osp/app/signin/SignInView;->S:Landroid/view/View;

    const v4, 0x7f0c0129

    invoke-virtual {v0, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/EditText;

    .line 3776
    iget-object v4, p0, Lcom/osp/app/signin/SignInView;->S:Landroid/view/View;

    const v5, 0x7f0c012a

    invoke-virtual {v4, v5}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    .line 3783
    :goto_0
    invoke-virtual {v0}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    sget-object v4, Ljava/util/Locale;->ENGLISH:Ljava/util/Locale;

    invoke-virtual {v0, v4}, Ljava/lang/String;->toLowerCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v0

    .line 3785
    invoke-static {}, Lcom/osp/device/b;->b()Lcom/osp/device/b;

    move-result-object v4

    invoke-virtual {v4}, Lcom/osp/device/b;->a()Z

    move-result v4

    if-eqz v4, :cond_2

    .line 3794
    sget-object v4, Lcom/osp/security/identity/h;->a:Lcom/osp/security/identity/h;

    .line 3795
    sget-object v5, Lcom/osp/security/identity/h;->a:Lcom/osp/security/identity/h;

    if-ne v4, v5, :cond_2

    .line 3886
    :goto_1
    return-void

    .line 3779
    :cond_0
    const v0, 0x7f0c0121

    invoke-virtual {p0, v0}, Lcom/osp/app/signin/SignInView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/EditText;

    .line 3780
    const v4, 0x7f0c0122

    invoke-virtual {p0, v4}, Lcom/osp/app/signin/SignInView;->findViewById(I)Landroid/view/View;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 3876
    :catch_0
    move-exception v0

    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    move v0, v1

    move-object v1, v3

    .line 3880
    :goto_2
    if-nez v0, :cond_1

    if-eqz v1, :cond_1

    .line 3882
    invoke-static {}, Lcom/osp/app/util/ap;->a()Lcom/osp/app/util/ap;

    invoke-static {p0, v1, v2}, Lcom/osp/app/util/ap;->a(Landroid/content/Context;Ljava/lang/String;I)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    .line 3885
    :cond_1
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v0, "SIV"

    const-string v1, "validateSignInFields"

    const-string v2, "END"

    invoke-static {v0, v1, v2}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1

    .line 3801
    :cond_2
    const/4 v4, 0x0

    :try_start_1
    invoke-static {v0, v4}, Lcom/osp/security/identity/g;->a(Ljava/lang/String;Z)Lcom/osp/security/identity/h;

    move-result-object v0

    .line 3803
    sget-object v4, Lcom/osp/security/identity/h;->d:Lcom/osp/security/identity/h;

    if-ne v0, v4, :cond_3

    .line 3805
    const v0, 0x7f0900ed

    invoke-virtual {p0, v0}, Lcom/osp/app/signin/SignInView;->getText(I)Ljava/lang/CharSequence;

    move-result-object v0

    invoke-interface {v0}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v0

    move v7, v1

    move-object v1, v0

    move v0, v7

    goto :goto_2

    .line 3806
    :cond_3
    sget-object v4, Lcom/osp/security/identity/h;->b:Lcom/osp/security/identity/h;

    if-eq v0, v4, :cond_4

    sget-object v4, Lcom/osp/security/identity/h;->c:Lcom/osp/security/identity/h;

    if-ne v0, v4, :cond_5

    .line 3811
    :cond_4
    const v0, 0x7f090091

    const/4 v4, 0x2

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    const/4 v6, 0x5

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    aput-object v6, v4, v5

    const/4 v5, 0x1

    const/16 v6, 0x32

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    aput-object v6, v4, v5

    invoke-virtual {p0, v0, v4}, Lcom/osp/app/signin/SignInView;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    move v7, v1

    move-object v1, v0

    move v0, v7

    goto :goto_2

    .line 3812
    :cond_5
    sget-object v4, Lcom/osp/security/identity/h;->f:Lcom/osp/security/identity/h;

    if-ne v0, v4, :cond_6

    .line 3814
    const v0, 0x7f090032

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    const-string v6, "admin, administrator, samsung, samsungapps, supervisor, tizenaccount, tizenstore, tizen"

    aput-object v6, v4, v5

    invoke-virtual {p0, v0, v4}, Lcom/osp/app/signin/SignInView;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    move v7, v1

    move-object v1, v0

    move v0, v7

    goto :goto_2

    .line 3815
    :cond_6
    sget-object v4, Lcom/osp/security/identity/h;->e:Lcom/osp/security/identity/h;

    if-ne v0, v4, :cond_7

    .line 3817
    const v0, 0x7f090070

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    const-string v6, ".-_+ "

    aput-object v6, v4, v5

    invoke-virtual {p0, v0, v4}, Lcom/osp/app/signin/SignInView;->getString(I[Ljava/lang/Object;)Ljava/lang/String;
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    move-result-object v0

    move v7, v1

    move-object v1, v0

    move v0, v7

    goto/16 :goto_2

    :cond_7
    move v0, v2

    move-object v1, v3

    .line 3824
    goto/16 :goto_2
.end method

.method private t()V
    .locals 2

    .prologue
    .line 7985
    invoke-direct {p0}, Lcom/osp/app/signin/SignInView;->e()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/osp/app/signin/SignInView;->a:Landroid/app/AlertDialog;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/osp/app/signin/SignInView;->a:Landroid/app/AlertDialog;

    invoke-virtual {v0}, Landroid/app/AlertDialog;->isShowing()Z

    move-result v0

    if-nez v0, :cond_0

    .line 7987
    iget-object v0, p0, Lcom/osp/app/signin/SignInView;->a:Landroid/app/AlertDialog;

    invoke-virtual {v0}, Landroid/app/AlertDialog;->getWindow()Landroid/view/Window;

    move-result-object v0

    const/4 v1, 0x3

    invoke-virtual {v0, v1}, Landroid/view/Window;->setSoftInputMode(I)V

    .line 7988
    iget-object v0, p0, Lcom/osp/app/signin/SignInView;->a:Landroid/app/AlertDialog;

    invoke-virtual {v0}, Landroid/app/AlertDialog;->show()V

    .line 7990
    :cond_0
    return-void
.end method

.method static synthetic t(Lcom/osp/app/signin/SignInView;)Z
    .locals 1

    .prologue
    .line 119
    iget-boolean v0, p0, Lcom/osp/app/signin/SignInView;->an:Z

    return v0
.end method

.method private u()V
    .locals 4

    .prologue
    const/16 v3, 0x8

    const/4 v2, 0x0

    .line 8149
    invoke-static {}, Lcom/osp/device/b;->b()Lcom/osp/device/b;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/osp/device/b;->i(Landroid/content/Context;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 8151
    invoke-virtual {p0}, Lcom/osp/app/signin/SignInView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v0

    iget v0, v0, Landroid/content/res/Configuration;->orientation:I

    const/4 v1, 0x1

    if-ne v0, v1, :cond_2

    .line 8153
    iget-object v0, p0, Lcom/osp/app/signin/SignInView;->at:Lcom/osp/app/signin/BottomSoftkeyLinearLayout;

    if-eqz v0, :cond_0

    .line 8155
    iget-object v0, p0, Lcom/osp/app/signin/SignInView;->at:Lcom/osp/app/signin/BottomSoftkeyLinearLayout;

    invoke-virtual {v0, v3}, Lcom/osp/app/signin/BottomSoftkeyLinearLayout;->setVisibility(I)V

    .line 8157
    :cond_0
    iget-object v0, p0, Lcom/osp/app/signin/SignInView;->au:Lcom/osp/app/signin/BottomSoftkeyLinearLayout;

    if-eqz v0, :cond_1

    .line 8159
    iget-object v0, p0, Lcom/osp/app/signin/SignInView;->au:Lcom/osp/app/signin/BottomSoftkeyLinearLayout;

    invoke-virtual {v0, v2}, Lcom/osp/app/signin/BottomSoftkeyLinearLayout;->setVisibility(I)V

    .line 8173
    :cond_1
    :goto_0
    return-void

    .line 8163
    :cond_2
    iget-object v0, p0, Lcom/osp/app/signin/SignInView;->au:Lcom/osp/app/signin/BottomSoftkeyLinearLayout;

    if-eqz v0, :cond_3

    .line 8165
    iget-object v0, p0, Lcom/osp/app/signin/SignInView;->au:Lcom/osp/app/signin/BottomSoftkeyLinearLayout;

    invoke-virtual {v0, v3}, Lcom/osp/app/signin/BottomSoftkeyLinearLayout;->setVisibility(I)V

    .line 8167
    :cond_3
    iget-object v0, p0, Lcom/osp/app/signin/SignInView;->at:Lcom/osp/app/signin/BottomSoftkeyLinearLayout;

    if-eqz v0, :cond_1

    .line 8169
    iget-object v0, p0, Lcom/osp/app/signin/SignInView;->at:Lcom/osp/app/signin/BottomSoftkeyLinearLayout;

    invoke-virtual {v0, v2}, Lcom/osp/app/signin/BottomSoftkeyLinearLayout;->setVisibility(I)V

    goto :goto_0
.end method

.method static synthetic u(Lcom/osp/app/signin/SignInView;)Z
    .locals 1

    .prologue
    .line 119
    invoke-virtual {p0}, Lcom/osp/app/signin/SignInView;->h()Z

    move-result v0

    return v0
.end method

.method private v()Z
    .locals 4

    .prologue
    .line 8176
    const/4 v2, 0x0

    .line 8178
    iget-boolean v0, p0, Lcom/osp/app/signin/SignInView;->aa:Z

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/osp/app/signin/SignInView;->ab:Z

    if-nez v0, :cond_0

    .line 8180
    const/4 v0, 0x0

    .line 8181
    invoke-static {p0}, Lcom/osp/app/util/ad;->d(Landroid/content/Context;)Z

    move-result v1

    if-eqz v1, :cond_3

    .line 8183
    invoke-static {p0}, Lcom/osp/app/util/ad;->h(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v0

    move-object v1, v0

    .line 8186
    :goto_0
    invoke-direct {p0}, Lcom/osp/app/signin/SignInView;->e()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 8189
    iget-object v0, p0, Lcom/osp/app/signin/SignInView;->S:Landroid/view/View;

    const v3, 0x7f0c0129

    invoke-virtual {v0, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/EditText;

    .line 8195
    :goto_1
    if-eqz v1, :cond_2

    if-eqz v0, :cond_2

    invoke-static {p0}, Lcom/osp/app/util/ad;->d(Landroid/content/Context;)Z

    move-result v3

    if-eqz v3, :cond_2

    .line 8197
    invoke-virtual {v0}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_2

    .line 8199
    const/4 v0, 0x1

    .line 8203
    :goto_2
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v1, "SIV"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "is Resignin ID Changed? "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;)V

    move v2, v0

    .line 8206
    :cond_0
    return v2

    .line 8192
    :cond_1
    const v0, 0x7f0c0121

    invoke-virtual {p0, v0}, Lcom/osp/app/signin/SignInView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/EditText;

    goto :goto_1

    :cond_2
    move v0, v2

    goto :goto_2

    :cond_3
    move-object v1, v0

    goto :goto_0
.end method

.method static synthetic v(Lcom/osp/app/signin/SignInView;)Z
    .locals 1

    .prologue
    .line 119
    invoke-virtual {p0}, Lcom/osp/app/signin/SignInView;->h()Z

    move-result v0

    return v0
.end method

.method static synthetic w(Lcom/osp/app/signin/SignInView;)Z
    .locals 1

    .prologue
    .line 119
    invoke-virtual {p0}, Lcom/osp/app/signin/SignInView;->h()Z

    move-result v0

    return v0
.end method

.method static synthetic x(Lcom/osp/app/signin/SignInView;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 119
    iget-object v0, p0, Lcom/osp/app/signin/SignInView;->Q:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic y(Lcom/osp/app/signin/SignInView;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 119
    iget-object v0, p0, Lcom/osp/app/signin/SignInView;->ad:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic z(Lcom/osp/app/signin/SignInView;)V
    .locals 0

    .prologue
    .line 119
    invoke-virtual {p0}, Lcom/osp/app/signin/SignInView;->m()V

    return-void
.end method


# virtual methods
.method protected final a()V
    .locals 8

    .prologue
    const v6, 0x7f0c0197

    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 3430
    const/4 v2, 0x0

    .line 3433
    invoke-direct {p0}, Lcom/osp/app/signin/SignInView;->e()Z

    move-result v0

    if-eqz v0, :cond_4

    .line 3435
    iget-object v0, p0, Lcom/osp/app/signin/SignInView;->S:Landroid/view/View;

    const v1, 0x7f0c0129

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/EditText;

    .line 3436
    iget-object v1, p0, Lcom/osp/app/signin/SignInView;->S:Landroid/view/View;

    const v3, 0x7f0c012a

    invoke-virtual {v1, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/EditText;

    .line 3437
    iget-object v3, p0, Lcom/osp/app/signin/SignInView;->a:Landroid/app/AlertDialog;

    if-eqz v3, :cond_c

    .line 3439
    iget-object v2, p0, Lcom/osp/app/signin/SignInView;->a:Landroid/app/AlertDialog;

    const/4 v3, -0x1

    invoke-virtual {v2, v3}, Landroid/app/AlertDialog;->getButton(I)Landroid/widget/Button;

    move-result-object v2

    move-object v7, v2

    move-object v2, v0

    move-object v0, v7

    .line 3447
    :goto_0
    invoke-virtual {v2}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v2

    invoke-interface {v2}, Landroid/text/Editable;->length()I

    move-result v2

    if-eqz v2, :cond_0

    invoke-virtual {v1}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v1

    invoke-interface {v1}, Landroid/text/Editable;->length()I

    move-result v1

    if-nez v1, :cond_7

    .line 3449
    :cond_0
    iget-object v1, p0, Lcom/osp/app/signin/SignInView;->x:Landroid/view/Menu;

    if-eqz v1, :cond_1

    .line 3451
    iget-object v1, p0, Lcom/osp/app/signin/SignInView;->x:Landroid/view/Menu;

    invoke-interface {v1, v6}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v1

    invoke-interface {v1, v4}, Landroid/view/MenuItem;->setEnabled(Z)Landroid/view/MenuItem;

    .line 3454
    :cond_1
    iget-object v1, p0, Lcom/osp/app/signin/SignInView;->at:Lcom/osp/app/signin/BottomSoftkeyLinearLayout;

    if-eqz v1, :cond_2

    .line 3456
    invoke-static {}, Lcom/osp/device/b;->b()Lcom/osp/device/b;

    move-result-object v1

    invoke-virtual {v1, p0}, Lcom/osp/device/b;->l(Landroid/content/Context;)Z

    move-result v1

    if-eqz v1, :cond_5

    .line 3458
    iget-object v1, p0, Lcom/osp/app/signin/SignInView;->at:Lcom/osp/app/signin/BottomSoftkeyLinearLayout;

    invoke-static {v4}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/osp/app/signin/BottomSoftkeyLinearLayout;->a(Ljava/lang/Boolean;)V

    .line 3463
    :goto_1
    invoke-static {}, Lcom/osp/device/b;->b()Lcom/osp/device/b;

    move-result-object v1

    invoke-virtual {v1, p0}, Lcom/osp/device/b;->i(Landroid/content/Context;)Z

    move-result v1

    if-nez v1, :cond_2

    .line 3465
    invoke-static {}, Lcom/osp/device/b;->b()Lcom/osp/device/b;

    move-result-object v1

    invoke-virtual {v1, p0}, Lcom/osp/device/b;->l(Landroid/content/Context;)Z

    move-result v1

    if-eqz v1, :cond_6

    .line 3467
    iget-object v1, p0, Lcom/osp/app/signin/SignInView;->au:Lcom/osp/app/signin/BottomSoftkeyLinearLayout;

    invoke-static {v4}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/osp/app/signin/BottomSoftkeyLinearLayout;->a(Ljava/lang/Boolean;)V

    .line 3475
    :cond_2
    :goto_2
    if-eqz v0, :cond_3

    .line 3477
    invoke-virtual {v0, v4}, Landroid/widget/Button;->setEnabled(Z)V

    .line 3600
    :cond_3
    :goto_3
    return-void

    .line 3443
    :cond_4
    const v0, 0x7f0c0121

    invoke-virtual {p0, v0}, Lcom/osp/app/signin/SignInView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/EditText;

    .line 3444
    const v1, 0x7f0c0122

    invoke-virtual {p0, v1}, Lcom/osp/app/signin/SignInView;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/EditText;

    move-object v7, v2

    move-object v2, v0

    move-object v0, v7

    goto :goto_0

    .line 3461
    :cond_5
    iget-object v1, p0, Lcom/osp/app/signin/SignInView;->at:Lcom/osp/app/signin/BottomSoftkeyLinearLayout;

    invoke-static {v4}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/osp/app/signin/BottomSoftkeyLinearLayout;->b(Ljava/lang/Boolean;)V

    goto :goto_1

    .line 3470
    :cond_6
    iget-object v1, p0, Lcom/osp/app/signin/SignInView;->au:Lcom/osp/app/signin/BottomSoftkeyLinearLayout;

    invoke-static {v4}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/osp/app/signin/BottomSoftkeyLinearLayout;->b(Ljava/lang/Boolean;)V

    goto :goto_2

    .line 3481
    :cond_7
    iget-object v1, p0, Lcom/osp/app/signin/SignInView;->x:Landroid/view/Menu;

    if-eqz v1, :cond_8

    .line 3483
    iget-object v1, p0, Lcom/osp/app/signin/SignInView;->x:Landroid/view/Menu;

    invoke-interface {v1, v6}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v1

    invoke-interface {v1, v5}, Landroid/view/MenuItem;->setEnabled(Z)Landroid/view/MenuItem;

    .line 3486
    :cond_8
    iget-object v1, p0, Lcom/osp/app/signin/SignInView;->at:Lcom/osp/app/signin/BottomSoftkeyLinearLayout;

    if-eqz v1, :cond_9

    .line 3488
    invoke-static {}, Lcom/osp/device/b;->b()Lcom/osp/device/b;

    move-result-object v1

    invoke-virtual {v1, p0}, Lcom/osp/device/b;->l(Landroid/content/Context;)Z

    move-result v1

    if-eqz v1, :cond_a

    .line 3490
    iget-object v1, p0, Lcom/osp/app/signin/SignInView;->at:Lcom/osp/app/signin/BottomSoftkeyLinearLayout;

    invoke-static {v5}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/osp/app/signin/BottomSoftkeyLinearLayout;->a(Ljava/lang/Boolean;)V

    .line 3496
    :goto_4
    invoke-static {}, Lcom/osp/device/b;->b()Lcom/osp/device/b;

    move-result-object v1

    invoke-virtual {v1, p0}, Lcom/osp/device/b;->i(Landroid/content/Context;)Z

    move-result v1

    if-nez v1, :cond_9

    .line 3498
    invoke-static {}, Lcom/osp/device/b;->b()Lcom/osp/device/b;

    move-result-object v1

    invoke-virtual {v1, p0}, Lcom/osp/device/b;->l(Landroid/content/Context;)Z

    move-result v1

    if-eqz v1, :cond_b

    .line 3500
    iget-object v1, p0, Lcom/osp/app/signin/SignInView;->au:Lcom/osp/app/signin/BottomSoftkeyLinearLayout;

    invoke-static {v5}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/osp/app/signin/BottomSoftkeyLinearLayout;->a(Ljava/lang/Boolean;)V

    .line 3508
    :cond_9
    :goto_5
    if-eqz v0, :cond_3

    .line 3510
    invoke-virtual {v0, v5}, Landroid/widget/Button;->setEnabled(Z)V

    goto :goto_3

    .line 3493
    :cond_a
    iget-object v1, p0, Lcom/osp/app/signin/SignInView;->at:Lcom/osp/app/signin/BottomSoftkeyLinearLayout;

    invoke-static {v5}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/osp/app/signin/BottomSoftkeyLinearLayout;->b(Ljava/lang/Boolean;)V

    goto :goto_4

    .line 3503
    :cond_b
    iget-object v1, p0, Lcom/osp/app/signin/SignInView;->au:Lcom/osp/app/signin/BottomSoftkeyLinearLayout;

    invoke-static {v5}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/osp/app/signin/BottomSoftkeyLinearLayout;->b(Ljava/lang/Boolean;)V

    goto :goto_5

    :cond_c
    move-object v7, v2

    move-object v2, v0

    move-object v0, v7

    goto/16 :goto_0
.end method

.method protected final b()V
    .locals 0

    .prologue
    .line 7535
    return-void
.end method

.method public final c()V
    .locals 3

    .prologue
    .line 1837
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v0, "SIV"

    const-string v1, "CancelSignInDual_DB"

    const-string v2, "START"

    invoke-static {v0, v1, v2}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 1839
    invoke-static {p0}, Lcom/osp/app/util/ad;->d(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1841
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v0, "SIV"

    const-string v1, "cancel - CancelSignInDual_DB"

    invoke-static {v0, v1}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 1875
    :goto_0
    return-void

    .line 1847
    :cond_0
    const-string v0, ""

    const-string v1, ""

    const-string v2, ""

    invoke-static {p0, v0, v1, v2}, Lcom/msc/c/e;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 1852
    :try_start_0
    new-instance v0, Lcom/osp/security/identity/d;

    invoke-direct {v0, p0}, Lcom/osp/security/identity/d;-><init>(Landroid/content/Context;)V

    .line 1853
    invoke-virtual {v0}, Lcom/osp/security/identity/d;->k()V
    :try_end_0
    .catch Lcom/osp/security/identity/IdentityException; {:try_start_0 .. :try_end_0} :catch_0

    .line 1860
    :goto_1
    :try_start_1
    new-instance v0, Lcom/osp/security/credential/a;

    invoke-direct {v0, p0}, Lcom/osp/security/credential/a;-><init>(Landroid/content/Context;)V

    .line 1861
    invoke-virtual {v0}, Lcom/osp/security/credential/a;->a()V
    :try_end_1
    .catch Lcom/osp/security/credential/CredentialException; {:try_start_1 .. :try_end_1} :catch_1

    .line 1868
    :goto_2
    :try_start_2
    new-instance v0, Lcom/osp/social/member/d;

    invoke-direct {v0, p0}, Lcom/osp/social/member/d;-><init>(Landroid/content/Context;)V

    .line 1869
    invoke-virtual {v0}, Lcom/osp/social/member/d;->a()V
    :try_end_2
    .catch Lcom/osp/social/member/MemberServiceException; {:try_start_2 .. :try_end_2} :catch_2

    .line 1874
    :goto_3
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v0, "SIV"

    const-string v1, "CancelSignInDual_DB"

    const-string v2, "END"

    invoke-static {v0, v1, v2}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 1854
    :catch_0
    move-exception v0

    invoke-virtual {v0}, Lcom/osp/security/identity/IdentityException;->printStackTrace()V

    goto :goto_1

    .line 1862
    :catch_1
    move-exception v0

    invoke-virtual {v0}, Lcom/osp/security/credential/CredentialException;->printStackTrace()V

    goto :goto_2

    .line 1870
    :catch_2
    move-exception v0

    invoke-virtual {v0}, Lcom/osp/social/member/MemberServiceException;->printStackTrace()V

    goto :goto_3
.end method

.method public final d()V
    .locals 3

    .prologue
    .line 3894
    invoke-direct {p0}, Lcom/osp/app/signin/SignInView;->e()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 3899
    iget-object v0, p0, Lcom/osp/app/signin/SignInView;->S:Landroid/view/View;

    const v1, 0x7f0c0129

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/EditText;

    .line 3900
    iget-object v1, p0, Lcom/osp/app/signin/SignInView;->S:Landroid/view/View;

    const v2, 0x7f0c012a

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/EditText;

    move-object v2, v0

    .line 3907
    :goto_0
    const-string v0, "input_method"

    invoke-virtual {p0, v0}, Lcom/osp/app/signin/SignInView;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/inputmethod/InputMethodManager;

    .line 3911
    if-eqz v0, :cond_0

    .line 3913
    if-eqz v2, :cond_2

    .line 3915
    :try_start_0
    invoke-virtual {v2}, Landroid/widget/EditText;->getWindowToken()Landroid/os/IBinder;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/view/inputmethod/InputMethodManager;->hideSoftInputFromWindow(Landroid/os/IBinder;I)Z
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 3928
    :cond_0
    :goto_1
    return-void

    .line 3903
    :cond_1
    const v0, 0x7f0c0121

    invoke-virtual {p0, v0}, Lcom/osp/app/signin/SignInView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/EditText;

    .line 3904
    const v1, 0x7f0c0122

    invoke-virtual {p0, v1}, Lcom/osp/app/signin/SignInView;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/EditText;

    move-object v2, v0

    goto :goto_0

    .line 3916
    :cond_2
    if-eqz v1, :cond_0

    .line 3919
    :try_start_1
    invoke-virtual {v1}, Landroid/widget/EditText;->getWindowToken()Landroid/os/IBinder;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/view/inputmethod/InputMethodManager;->hideSoftInputFromWindow(Landroid/os/IBinder;I)Z
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_1

    .line 3922
    :catch_0
    move-exception v0

    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_1
.end method

.method protected onActivityResult(IILandroid/content/Intent;)V
    .locals 8

    .prologue
    const/16 v7, 0xce

    const/16 v6, 0xf

    const/4 v5, 0x1

    const/4 v4, -0x1

    const/4 v3, 0x0

    .line 1366
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v0, "SIV"

    const-string v1, "onActivityResult"

    const-string v2, "START"

    invoke-static {v0, v1, v2}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 1367
    invoke-super {p0, p1, p2, p3}, Lcom/osp/app/util/BaseActivity;->onActivityResult(IILandroid/content/Intent;)V

    .line 1369
    if-ne p2, v6, :cond_1

    .line 1371
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v0, "SIV"

    const-string v1, "FINISH_BY_HOME_BUTTON"

    invoke-static {v0, v1}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 1372
    invoke-virtual {p0, v6}, Lcom/osp/app/signin/SignInView;->b(I)V

    .line 1373
    invoke-virtual {p0}, Lcom/osp/app/signin/SignInView;->finish()V

    .line 1581
    :cond_0
    :goto_0
    return-void

    .line 1377
    :cond_1
    const/16 v0, 0x15

    if-ne p2, v0, :cond_2

    .line 1379
    invoke-virtual {p0, p2}, Lcom/osp/app/signin/SignInView;->b(I)V

    .line 1380
    invoke-virtual {p0}, Lcom/osp/app/signin/SignInView;->finish()V

    goto :goto_0

    .line 1384
    :cond_2
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "onActivityResult - requestCode : "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;)V

    .line 1385
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "onActivityResult - resultCode : "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;)V

    .line 1387
    const/16 v0, 0xee

    if-ne p1, v0, :cond_3

    .line 1389
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v0, "SIV"

    const-string v1, "onActivityResult - REQUEST_CODE_AUTH_RESULT_FOR_EASYSIGNUP"

    invoke-static {v0, v1}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 1390
    invoke-direct {p0, v5}, Lcom/osp/app/signin/SignInView;->a(Z)V

    goto :goto_0

    .line 1394
    :cond_3
    const/16 v0, 0xc9

    if-ne p1, v0, :cond_6

    .line 1397
    const/16 v0, 0xc

    if-ne p2, v0, :cond_5

    .line 1399
    invoke-static {p0}, Lcom/osp/app/util/r;->p(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_4

    iget-boolean v0, p0, Lcom/osp/app/signin/SignInView;->aj:Z

    if-eqz v0, :cond_4

    invoke-static {}, Lcom/osp/app/signin/SamsungService;->a()Z

    move-result v0

    if-eqz v0, :cond_4

    .line 1401
    invoke-static {}, Lcom/osp/app/util/m;->a()Lcom/osp/app/util/m;

    invoke-static {p0, v3}, Lcom/osp/app/util/m;->a(Landroid/app/Activity;Z)V

    goto :goto_0

    .line 1404
    :cond_4
    invoke-direct {p0, v5}, Lcom/osp/app/signin/SignInView;->a(Z)V

    goto :goto_0

    .line 1408
    :cond_5
    invoke-direct {p0, v3}, Lcom/osp/app/signin/SignInView;->a(Z)V

    goto :goto_0

    .line 1413
    :cond_6
    const/16 v0, 0xd5

    if-ne p1, v0, :cond_7

    .line 1415
    if-ne p2, v4, :cond_0

    .line 1417
    invoke-direct {p0}, Lcom/osp/app/signin/SignInView;->g()V

    goto :goto_0

    .line 1422
    :cond_7
    const/16 v0, 0xe5

    if-eq p1, v0, :cond_8

    const/16 v0, 0xe6

    if-ne p1, v0, :cond_9

    .line 1424
    :cond_8
    if-ne p2, v4, :cond_b

    .line 1426
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v0, "Duplicate account auth success"

    invoke-static {v0}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;)V

    .line 1427
    invoke-virtual {p0, p2}, Lcom/osp/app/signin/SignInView;->b(I)V

    .line 1428
    invoke-virtual {p0}, Lcom/osp/app/signin/SignInView;->finish()V

    .line 1439
    :cond_9
    :goto_1
    iget-object v0, p0, Lcom/osp/app/signin/SignInView;->y:Landroid/content/Intent;

    if-eqz v0, :cond_a

    iget-object v0, p0, Lcom/osp/app/signin/SignInView;->V:Lcom/msc/a/g;

    if-nez v0, :cond_c

    .line 1441
    :cond_a
    const/16 v0, 0xd0

    if-eq p1, v0, :cond_c

    const/16 v0, 0xd6

    if-eq p1, v0, :cond_c

    .line 1443
    invoke-direct {p0}, Lcom/osp/app/signin/SignInView;->t()V

    goto/16 :goto_0

    .line 1429
    :cond_b
    const/16 v0, 0xe

    if-ne p2, v0, :cond_9

    .line 1431
    invoke-direct {p0}, Lcom/osp/app/signin/SignInView;->e()Z

    move-result v0

    if-eqz v0, :cond_9

    iget-object v0, p0, Lcom/osp/app/signin/SignInView;->a:Landroid/app/AlertDialog;

    if-eqz v0, :cond_9

    iget-object v0, p0, Lcom/osp/app/signin/SignInView;->a:Landroid/app/AlertDialog;

    invoke-virtual {v0}, Landroid/app/AlertDialog;->isShowing()Z

    move-result v0

    if-nez v0, :cond_9

    .line 1433
    iget-object v0, p0, Lcom/osp/app/signin/SignInView;->a:Landroid/app/AlertDialog;

    invoke-virtual {v0}, Landroid/app/AlertDialog;->show()V

    goto :goto_1

    .line 1448
    :cond_c
    sparse-switch p2, :sswitch_data_0

    .line 1546
    :cond_d
    :goto_2
    const/16 v0, 0xd6

    if-eq p1, v0, :cond_f

    .line 1550
    const/16 v0, 0xd2

    if-ne p1, v0, :cond_f

    .line 1552
    if-eqz p3, :cond_e

    .line 1555
    const-string v0, "authcode"

    invoke-virtual {p3, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 1556
    iget-object v1, p0, Lcom/osp/app/signin/SignInView;->y:Landroid/content/Intent;

    const-string v2, "authcode"

    invoke-virtual {v1, v2, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 1557
    iget-boolean v0, p0, Lcom/osp/app/signin/SignInView;->W:Z

    if-eqz v0, :cond_e

    .line 1559
    const-string v0, "signUpInfo"

    invoke-virtual {p3, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 1560
    iget-object v1, p0, Lcom/osp/app/signin/SignInView;->y:Landroid/content/Intent;

    const-string v2, "signUpInfo"

    invoke-virtual {v1, v2, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 1563
    :cond_e
    iget-object v0, p0, Lcom/osp/app/signin/SignInView;->y:Landroid/content/Intent;

    invoke-virtual {p0, p2, v0}, Lcom/osp/app/signin/SignInView;->a(ILandroid/content/Intent;)V

    .line 1570
    invoke-virtual {p0}, Lcom/osp/app/signin/SignInView;->finish()V

    .line 1572
    :cond_f
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v0, "SIV"

    const-string v1, "onActivityResult"

    const-string v2, "END"

    invoke-static {v0, v1, v2}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 1452
    :sswitch_0
    if-ne p1, v7, :cond_11

    .line 1456
    if-eqz p3, :cond_10

    .line 1458
    const-string v0, "authcode"

    invoke-virtual {p3, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 1459
    iget-object v1, p0, Lcom/osp/app/signin/SignInView;->y:Landroid/content/Intent;

    const-string v2, "authcode"

    invoke-virtual {v1, v2, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 1460
    iget-boolean v0, p0, Lcom/osp/app/signin/SignInView;->W:Z

    if-eqz v0, :cond_10

    .line 1462
    const-string v0, "signUpInfo"

    invoke-virtual {p3, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 1463
    iget-object v1, p0, Lcom/osp/app/signin/SignInView;->y:Landroid/content/Intent;

    const-string v2, "signUpInfo"

    invoke-virtual {v1, v2, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 1466
    :cond_10
    iget-object v0, p0, Lcom/osp/app/signin/SignInView;->y:Landroid/content/Intent;

    invoke-virtual {p0, p2, v0}, Lcom/osp/app/signin/SignInView;->a(ILandroid/content/Intent;)V

    .line 1467
    invoke-virtual {p0}, Lcom/osp/app/signin/SignInView;->finish()V

    goto :goto_2

    .line 1468
    :cond_11
    const/16 v0, 0xd1

    if-ne p1, v0, :cond_13

    .line 1481
    iput-boolean v3, p0, Lcom/osp/app/signin/SignInView;->ai:Z

    .line 1482
    invoke-static {p0}, Lcom/osp/app/util/r;->p(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_12

    if-eqz p3, :cond_12

    .line 1484
    const-string v0, "key_easy_signup_mo_auth_mode"

    invoke-virtual {p3, v0, v3}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v0

    iput-boolean v0, p0, Lcom/osp/app/signin/SignInView;->aj:Z

    .line 1486
    :cond_12
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "onActivityResult mEasySignupMoAuthMode : "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-boolean v1, p0, Lcom/osp/app/signin/SignInView;->aj:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;)V

    .line 1487
    iget-object v0, p0, Lcom/osp/app/signin/SignInView;->V:Lcom/msc/a/g;

    invoke-virtual {v0, v3}, Lcom/msc/a/g;->a(Z)V

    .line 1488
    iget-object v0, p0, Lcom/osp/app/signin/SignInView;->V:Lcom/msc/a/g;

    invoke-virtual {v0, v3}, Lcom/msc/a/g;->b(Z)V

    .line 1489
    iget-object v0, p0, Lcom/osp/app/signin/SignInView;->V:Lcom/msc/a/g;

    invoke-direct {p0, v0}, Lcom/osp/app/signin/SignInView;->a(Lcom/msc/a/g;)V

    goto/16 :goto_2

    .line 1506
    :cond_13
    const/16 v0, 0xca

    if-ne p1, v0, :cond_14

    .line 1508
    iget-object v0, p0, Lcom/osp/app/signin/SignInView;->V:Lcom/msc/a/g;

    invoke-virtual {v0, v3}, Lcom/msc/a/g;->e(Z)V

    .line 1509
    iget-object v0, p0, Lcom/osp/app/signin/SignInView;->V:Lcom/msc/a/g;

    invoke-direct {p0, v0}, Lcom/osp/app/signin/SignInView;->a(Lcom/msc/a/g;)V

    goto/16 :goto_2

    .line 1511
    :cond_14
    const/16 v0, 0xd0

    if-ne p1, v0, :cond_15

    .line 1513
    new-instance v0, Lcom/osp/app/signin/jj;

    invoke-direct {v0, p0}, Lcom/osp/app/signin/jj;-><init>(Lcom/osp/app/signin/SignInView;)V

    iput-object v0, p0, Lcom/osp/app/signin/SignInView;->J:Lcom/osp/app/signin/jj;

    .line 1514
    iget-object v0, p0, Lcom/osp/app/signin/SignInView;->J:Lcom/osp/app/signin/jj;

    invoke-virtual {v0}, Lcom/osp/app/signin/jj;->b()V

    goto/16 :goto_2

    .line 1515
    :cond_15
    const/16 v0, 0xda

    if-ne p1, v0, :cond_d

    .line 1517
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/osp/app/signin/SignInView;->U:Lcom/osp/app/signin/kd;

    .line 1518
    iget-object v0, p0, Lcom/osp/app/signin/SignInView;->V:Lcom/msc/a/g;

    invoke-direct {p0, v0}, Lcom/osp/app/signin/SignInView;->a(Lcom/msc/a/g;)V

    goto/16 :goto_2

    .line 1522
    :sswitch_1
    invoke-direct {p0}, Lcom/osp/app/signin/SignInView;->e()Z

    move-result v0

    if-eqz v0, :cond_16

    .line 1524
    invoke-direct {p0}, Lcom/osp/app/signin/SignInView;->t()V

    goto/16 :goto_2

    .line 1527
    :cond_16
    const v0, 0x7f0c0121

    invoke-virtual {p0, v0}, Lcom/osp/app/signin/SignInView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/EditText;

    .line 1528
    if-ne p1, v7, :cond_d

    if-eqz v0, :cond_d

    .line 1530
    invoke-virtual {v0}, Landroid/widget/EditText;->requestFocus()Z

    goto/16 :goto_2

    .line 1535
    :sswitch_2
    const/16 v0, 0xa

    invoke-virtual {p0, v0}, Lcom/osp/app/signin/SignInView;->b(I)V

    .line 1536
    invoke-virtual {p0}, Lcom/osp/app/signin/SignInView;->finish()V

    goto/16 :goto_2

    .line 1539
    :sswitch_3
    invoke-virtual {p0, p2}, Lcom/osp/app/signin/SignInView;->b(I)V

    .line 1540
    invoke-virtual {p0}, Lcom/osp/app/signin/SignInView;->finish()V

    goto/16 :goto_0

    .line 1448
    nop

    :sswitch_data_0
    .sparse-switch
        -0x1 -> :sswitch_0
        0x0 -> :sswitch_1
        0xa -> :sswitch_2
        0xf -> :sswitch_3
    .end sparse-switch
.end method

.method public onBackPressed()V
    .locals 5

    .prologue
    const/4 v3, 0x0

    .line 1585
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v0, "SIV"

    const-string v1, "onBackPressed"

    const-string v2, "START"

    invoke-static {v0, v1, v2}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 1587
    iget-object v0, p0, Lcom/osp/app/signin/SignInView;->K:Lcom/osp/app/signin/jr;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/osp/app/signin/SignInView;->K:Lcom/osp/app/signin/jr;

    invoke-virtual {v0}, Lcom/osp/app/signin/jr;->getStatus()Landroid/os/AsyncTask$Status;

    move-result-object v0

    sget-object v1, Landroid/os/AsyncTask$Status;->RUNNING:Landroid/os/AsyncTask$Status;

    if-ne v0, v1, :cond_0

    .line 1589
    iget-object v0, p0, Lcom/osp/app/signin/SignInView;->K:Lcom/osp/app/signin/jr;

    invoke-virtual {v0}, Lcom/osp/app/signin/jr;->d()V

    .line 1590
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/osp/app/signin/SignInView;->K:Lcom/osp/app/signin/jr;

    .line 1601
    :cond_0
    iget-boolean v0, p0, Lcom/osp/app/signin/SignInView;->W:Z

    if-eqz v0, :cond_2

    .line 1603
    const/4 v0, 0x7

    iget-object v1, p0, Lcom/osp/app/signin/SignInView;->y:Landroid/content/Intent;

    invoke-virtual {p0, v0, v1}, Lcom/osp/app/signin/SignInView;->a(ILandroid/content/Intent;)V

    .line 1604
    invoke-virtual {p0}, Lcom/osp/app/signin/SignInView;->finish()V

    .line 1620
    :goto_0
    iget-object v0, p0, Lcom/osp/app/signin/SignInView;->y:Landroid/content/Intent;

    if-eqz v0, :cond_1

    .line 1622
    new-instance v0, Lcom/osp/app/bigdatalog/c;

    invoke-virtual {p0}, Lcom/osp/app/signin/SignInView;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    const/4 v2, 0x4

    const/4 v3, 0x3

    iget-object v4, p0, Lcom/osp/app/signin/SignInView;->y:Landroid/content/Intent;

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/osp/app/bigdatalog/c;-><init>(Landroid/content/Context;IILandroid/content/Intent;)V

    .line 1623
    invoke-static {}, Lcom/osp/app/bigdatalog/e;->a()Lcom/osp/app/bigdatalog/e;

    move-result-object v1

    invoke-virtual {p0}, Lcom/osp/app/signin/SignInView;->getApplicationContext()Landroid/content/Context;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/osp/app/bigdatalog/e;->a(Landroid/content/Context;)V

    .line 1624
    invoke-static {}, Lcom/osp/app/bigdatalog/e;->a()Lcom/osp/app/bigdatalog/e;

    move-result-object v1

    invoke-virtual {p0}, Lcom/osp/app/signin/SignInView;->getApplicationContext()Landroid/content/Context;

    move-result-object v2

    invoke-virtual {v1, v2, v0}, Lcom/osp/app/bigdatalog/e;->a(Landroid/content/Context;Lcom/osp/app/bigdatalog/c;)V

    .line 1627
    :cond_1
    invoke-super {p0}, Lcom/osp/app/util/BaseActivity;->onBackPressed()V

    .line 1628
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v0, "SIV"

    const-string v1, "onBackPressed"

    const-string v2, "END"

    invoke-static {v0, v1, v2}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 1629
    return-void

    .line 1609
    :cond_2
    iget-object v0, p0, Lcom/osp/app/signin/SignInView;->y:Landroid/content/Intent;

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/osp/app/signin/SignInView;->y:Landroid/content/Intent;

    const-string v1, "is_cancelable_just_one_activity"

    invoke-virtual {v0, v1, v3}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 1611
    const/16 v0, 0xe

    iget-object v1, p0, Lcom/osp/app/signin/SignInView;->y:Landroid/content/Intent;

    invoke-virtual {p0, v0, v1}, Lcom/osp/app/signin/SignInView;->a(ILandroid/content/Intent;)V

    goto :goto_0

    .line 1614
    :cond_3
    invoke-virtual {p0, v3}, Lcom/osp/app/signin/SignInView;->b(I)V

    goto :goto_0
.end method

.method public onConfigurationChanged(Landroid/content/res/Configuration;)V
    .locals 3

    .prologue
    .line 1060
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v0, "SIV"

    const-string v1, "onConfigurationChanged"

    const-string v2, "START"

    invoke-static {v0, v1, v2}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 1062
    invoke-super {p0, p1}, Lcom/osp/app/util/BaseActivity;->onConfigurationChanged(Landroid/content/res/Configuration;)V

    .line 1063
    invoke-direct {p0}, Lcom/osp/app/signin/SignInView;->u()V

    .line 1064
    iget v0, p1, Landroid/content/res/Configuration;->orientation:I

    invoke-direct {p0, v0}, Lcom/osp/app/signin/SignInView;->a(I)V

    .line 1066
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v0, "SIV"

    const-string v1, "onConfigurationChanged"

    const-string v2, "END"

    invoke-static {v0, v1, v2}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 1067
    return-void
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 12

    .prologue
    const v11, 0x7f090092

    const v10, 0x7f09016a

    const v9, 0x7f090019

    const/4 v8, 0x1

    const/4 v7, 0x0

    .line 527
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v0, "SIV"

    const-string v1, "onCreate"

    const-string v2, "START"

    invoke-static {v0, v1, v2}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 529
    invoke-super {p0, p1}, Lcom/osp/app/util/BaseActivity;->onCreate(Landroid/os/Bundle;)V

    .line 540
    invoke-virtual {p0}, Lcom/osp/app/signin/SignInView;->getIntent()Landroid/content/Intent;

    move-result-object v0

    iput-object v0, p0, Lcom/osp/app/signin/SignInView;->y:Landroid/content/Intent;

    .line 541
    iget-object v0, p0, Lcom/osp/app/signin/SignInView;->y:Landroid/content/Intent;

    const-string v1, "theme"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 542
    invoke-virtual {p0}, Lcom/osp/app/signin/SignInView;->getCallingActivity()Landroid/content/ComponentName;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 544
    invoke-virtual {p0}, Lcom/osp/app/signin/SignInView;->getCallingActivity()Landroid/content/ComponentName;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/ComponentName;->getShortClassName()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/osp/app/signin/SignInView;->aq:Ljava/lang/String;

    .line 549
    :cond_0
    invoke-virtual {p0}, Lcom/osp/app/signin/SignInView;->getApplication()Landroid/app/Application;

    move-result-object v0

    check-cast v0, Lcom/osp/app/signin/SamsungService;

    invoke-virtual {v0, p0}, Lcom/osp/app/signin/SamsungService;->a(Landroid/app/Activity;)V

    .line 551
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v0, "onCreate"

    invoke-static {v0}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;)V

    .line 553
    invoke-direct {p0}, Lcom/osp/app/signin/SignInView;->e()Z

    move-result v0

    if-eqz v0, :cond_14

    .line 555
    invoke-virtual {p0, v8}, Lcom/osp/app/signin/SignInView;->d(I)V

    .line 556
    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 558
    invoke-virtual {p0, v1}, Lcom/osp/app/signin/SignInView;->h(Ljava/lang/String;)V

    .line 560
    :cond_1
    invoke-virtual {p0}, Lcom/osp/app/signin/SignInView;->k()V

    .line 561
    invoke-static {p0}, Lcom/osp/app/signin/SignInView;->a(Landroid/app/Activity;)V

    .line 562
    const v0, 0x7f030016

    invoke-virtual {p0, v0}, Lcom/osp/app/signin/SignInView;->setContentView(I)V

    .line 584
    :goto_0
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v0, "SIV"

    const-string v1, "paramFromServiceApp"

    const-string v2, "START"

    invoke-static {v0, v1, v2}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {p0}, Lcom/osp/app/signin/SignInView;->getIntent()Landroid/content/Intent;

    move-result-object v0

    iput-object v0, p0, Lcom/osp/app/signin/SignInView;->y:Landroid/content/Intent;

    const/4 v0, 0x0

    const-string v1, "com.msc.action.samsungaccount.resignin"

    iget-object v2, p0, Lcom/osp/app/signin/SignInView;->y:Landroid/content/Intent;

    invoke-virtual {v2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_2

    iget-object v1, p0, Lcom/osp/app/signin/SignInView;->y:Landroid/content/Intent;

    const-string v2, "require re-SignIn"

    invoke-virtual {v1, v2, v7}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v1

    if-eqz v1, :cond_15

    :cond_2
    iput-boolean v8, p0, Lcom/osp/app/signin/SignInView;->aa:Z

    :cond_3
    :goto_1
    const-string v1, "com.msc.action.samsungaccount.activate_account"

    iget-object v2, p0, Lcom/osp/app/signin/SignInView;->y:Landroid/content/Intent;

    invoke-virtual {v2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_17

    iput-boolean v8, p0, Lcom/osp/app/signin/SignInView;->ac:Z

    :cond_4
    :goto_2
    iget-object v1, p0, Lcom/osp/app/signin/SignInView;->y:Landroid/content/Intent;

    const-string v2, "client_id"

    invoke-virtual {v1, v2}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/osp/app/signin/SignInView;->z:Ljava/lang/String;

    iget-object v1, p0, Lcom/osp/app/signin/SignInView;->y:Landroid/content/Intent;

    const-string v2, "client_secret"

    invoke-virtual {v1, v2}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/osp/app/signin/SignInView;->A:Ljava/lang/String;

    iget-object v1, p0, Lcom/osp/app/signin/SignInView;->y:Landroid/content/Intent;

    const-string v2, "account_mode"

    invoke-virtual {v1, v2}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/osp/app/signin/SignInView;->B:Ljava/lang/String;

    iget-object v1, p0, Lcom/osp/app/signin/SignInView;->y:Landroid/content/Intent;

    const-string v2, "MODE"

    invoke-virtual {v1, v2}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/osp/app/signin/SignInView;->C:Ljava/lang/String;

    iget-object v1, p0, Lcom/osp/app/signin/SignInView;->y:Landroid/content/Intent;

    const-string v2, "service_name"

    invoke-virtual {v1, v2}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/osp/app/signin/SignInView;->E:Ljava/lang/String;

    iget-object v1, p0, Lcom/osp/app/signin/SignInView;->y:Landroid/content/Intent;

    const-string v2, "new_add_account_mode"

    invoke-virtual {v1, v2, v7}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v1

    iput-boolean v1, p0, Lcom/osp/app/signin/SignInView;->ah:Z

    iget-object v1, p0, Lcom/osp/app/signin/SignInView;->y:Landroid/content/Intent;

    const-string v2, "key_easy_signup_tnc_mode"

    invoke-virtual {v1, v2, v7}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v1

    iput-boolean v1, p0, Lcom/osp/app/signin/SignInView;->ai:Z

    iget-boolean v1, p0, Lcom/osp/app/signin/SignInView;->aa:Z

    if-nez v1, :cond_5

    iget-boolean v1, p0, Lcom/osp/app/signin/SignInView;->ac:Z

    if-eqz v1, :cond_6

    :cond_5
    iput-boolean v8, p0, Lcom/osp/app/signin/SignInView;->ah:Z

    :cond_6
    const-string v1, "SamsungApps"

    iget-object v2, p0, Lcom/osp/app/signin/SignInView;->E:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_7

    iput-boolean v8, p0, Lcom/osp/app/signin/SignInView;->W:Z

    :cond_7
    iget-object v1, p0, Lcom/osp/app/signin/SignInView;->y:Landroid/content/Intent;

    const-string v2, "BG_WhoareU"

    invoke-virtual {v1, v2}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/osp/app/signin/SignInView;->G:Ljava/lang/String;

    const-string v1, "BG_mode"

    iget-object v2, p0, Lcom/osp/app/signin/SignInView;->G:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_8

    iget-object v1, p0, Lcom/osp/app/signin/SignInView;->y:Landroid/content/Intent;

    const-string v2, "mypackage"

    invoke-virtual {v1, v2}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/osp/app/signin/SignInView;->F:Ljava/lang/String;

    iget-object v1, p0, Lcom/osp/app/signin/SignInView;->y:Landroid/content/Intent;

    const-string v2, "key_request_id"

    const-wide/16 v4, 0x0

    invoke-virtual {v1, v2, v4, v5}, Landroid/content/Intent;->getLongExtra(Ljava/lang/String;J)J

    move-result-wide v2

    iput-wide v2, p0, Lcom/osp/app/signin/SignInView;->ak:J

    :cond_8
    iget-object v1, p0, Lcom/osp/app/signin/SignInView;->y:Landroid/content/Intent;

    const-string v2, "OSP_VER"

    invoke-virtual {v1, v2}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/osp/app/signin/SignInView;->D:Ljava/lang/String;

    iget-object v1, p0, Lcom/osp/app/signin/SignInView;->D:Ljava/lang/String;

    invoke-static {v1}, Lcom/osp/app/util/ad;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/osp/app/signin/SignInView;->D:Ljava/lang/String;

    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "paramFromServiceApp - className : "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;)V

    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "paramFromServiceApp - mClientId : "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v1, p0, Lcom/osp/app/signin/SignInView;->z:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;)V

    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "paramFromServiceApp - mClientSecret : "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v1, p0, Lcom/osp/app/signin/SignInView;->A:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;)V

    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "paramFromServiceApp - mAccountMode : "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v1, p0, Lcom/osp/app/signin/SignInView;->B:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;)V

    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "paramFromServiceApp - mSettingMode : "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v1, p0, Lcom/osp/app/signin/SignInView;->C:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;)V

    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "paramFromServiceApp - mServiceName : "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v1, p0, Lcom/osp/app/signin/SignInView;->E:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;)V

    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "paramFromServiceApp - mIsSamsungApps : "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-boolean v1, p0, Lcom/osp/app/signin/SignInView;->W:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;)V

    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "paramFromServiceApp - mSourcepackage : "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v1, p0, Lcom/osp/app/signin/SignInView;->F:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;)V

    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "paramFromServiceApp - mOspVersion : "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v1, p0, Lcom/osp/app/signin/SignInView;->D:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;)V

    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "paramFromServiceApp - mEasySignupTncMode : "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-boolean v1, p0, Lcom/osp/app/signin/SignInView;->ai:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;)V

    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v0, "SIV"

    const-string v1, "paramFromServiceApp"

    const-string v2, "END"

    invoke-static {v0, v1, v2}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 586
    invoke-static {p0}, Lcom/osp/app/util/r;->i(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_9

    .line 588
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x13

    if-ge v0, v1, :cond_9

    .line 590
    invoke-static {}, Lcom/osp/device/b;->b()Lcom/osp/device/b;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/osp/device/b;->i(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_9

    .line 592
    invoke-static {p0}, Lcom/msc/sa/c/d;->c(Landroid/app/Activity;)V

    .line 604
    :cond_9
    invoke-virtual {p0}, Lcom/osp/app/signin/SignInView;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-direct {p0}, Lcom/osp/app/signin/SignInView;->e()Z

    move-result v0

    if-eqz v0, :cond_19

    invoke-virtual {p0}, Lcom/osp/app/signin/SignInView;->getLayoutInflater()Landroid/view/LayoutInflater;

    move-result-object v0

    const v1, 0x7f03002f

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/osp/app/signin/SignInView;->S:Landroid/view/View;

    iget-object v4, p0, Lcom/osp/app/signin/SignInView;->S:Landroid/view/View;

    const v0, 0x7f0c00b7

    invoke-virtual {v4, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/CheckBox;

    const v1, 0x7f0c00b8

    invoke-virtual {v4, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    const v2, 0x7f0c012b

    invoke-virtual {v4, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    const v3, 0x7f0c012c

    invoke-virtual {v4, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/TextView;

    if-eqz v0, :cond_a

    sget v5, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v6, 0x10

    if-le v5, v6, :cond_a

    invoke-virtual {p0}, Lcom/osp/app/signin/SignInView;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    const v6, 0x7f0800e2

    invoke-virtual {v5, v6}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v5

    float-to-int v5, v5

    invoke-static {v0, v5, v7, v7, v7}, Lcom/msc/sa/c/d;->a(Landroid/view/View;IIII)V

    :cond_a
    invoke-virtual {p0}, Lcom/osp/app/signin/SignInView;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    invoke-static {p0}, Lcom/osp/app/util/an;->a(Lcom/osp/app/util/BaseActivity;)Lcom/osp/app/util/an;

    const v6, 0x7f070020

    invoke-static {v6}, Lcom/osp/app/util/an;->a(I)I

    move-result v6

    invoke-virtual {v5, v6}, Landroid/content/res/Resources;->getColor(I)I

    move-result v5

    invoke-virtual {v0, v5}, Landroid/widget/CheckBox;->setTextColor(I)V

    invoke-virtual {p0}, Lcom/osp/app/signin/SignInView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-static {p0}, Lcom/osp/app/util/an;->a(Lcom/osp/app/util/BaseActivity;)Lcom/osp/app/util/an;

    const v5, 0x7f070020

    invoke-static {v5}, Lcom/osp/app/util/an;->a(I)I

    move-result v5

    invoke-virtual {v0, v5}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    invoke-virtual {v2, v0}, Landroid/widget/TextView;->setTextColor(I)V

    const v0, 0x106000d

    invoke-virtual {v4, v0}, Landroid/view/View;->setBackgroundResource(I)V

    invoke-static {}, Lcom/osp/app/util/r;->t()Z

    move-result v0

    if-eqz v0, :cond_b

    const v0, 0x7f0c0129

    invoke-virtual {v4, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/EditText;

    invoke-static {}, Lcom/osp/device/b;->b()Lcom/osp/device/b;

    invoke-static {p0}, Lcom/osp/device/b;->a(Landroid/content/Context;)Z

    move-result v2

    if-eqz v0, :cond_b

    if-nez v2, :cond_b

    const v2, 0x7f090112

    invoke-virtual {v0, v2}, Landroid/widget/EditText;->setHint(I)V

    :cond_b
    if-eqz v1, :cond_c

    invoke-virtual {p0}, Lcom/osp/app/signin/SignInView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v2, 0x7f0800e6

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    invoke-static {p0}, Lcom/osp/app/util/an;->a(Lcom/osp/app/util/BaseActivity;)Lcom/osp/app/util/an;

    const v2, 0x7f020089

    invoke-static {v2}, Lcom/osp/app/util/an;->a(I)I

    move-result v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setBackgroundResource(I)V

    invoke-virtual {p0}, Lcom/osp/app/signin/SignInView;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-static {p0}, Lcom/osp/app/util/an;->a(Lcom/osp/app/util/BaseActivity;)Lcom/osp/app/util/an;

    const v5, 0x7f070014

    invoke-static {v5}, Lcom/osp/app/util/an;->a(I)I

    move-result v5

    invoke-virtual {v2, v5}, Landroid/content/res/Resources;->getColor(I)I

    move-result v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setTextColor(I)V

    invoke-virtual {p0}, Lcom/osp/app/signin/SignInView;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-static {p0}, Lcom/osp/app/util/an;->a(Lcom/osp/app/util/BaseActivity;)Lcom/osp/app/util/an;

    const v5, 0x7f070014

    invoke-static {v5}, Lcom/osp/app/util/an;->a(I)I

    move-result v5

    invoke-virtual {v2, v5}, Landroid/content/res/Resources;->getColor(I)I

    move-result v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setLinkTextColor(I)V

    invoke-static {v1, v0, v0, v0, v0}, Lcom/msc/sa/c/d;->a(Landroid/view/View;IIII)V

    :cond_c
    invoke-direct {p0, v1}, Lcom/osp/app/signin/SignInView;->b(Landroid/widget/TextView;)V

    if-eqz v3, :cond_d

    invoke-virtual {p0}, Lcom/osp/app/signin/SignInView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0800e6

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    invoke-static {p0}, Lcom/osp/app/util/an;->a(Lcom/osp/app/util/BaseActivity;)Lcom/osp/app/util/an;

    const v1, 0x7f020089

    invoke-static {v1}, Lcom/osp/app/util/an;->a(I)I

    move-result v1

    invoke-virtual {v3, v1}, Landroid/widget/TextView;->setBackgroundResource(I)V

    invoke-virtual {p0}, Lcom/osp/app/signin/SignInView;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-static {p0}, Lcom/osp/app/util/an;->a(Lcom/osp/app/util/BaseActivity;)Lcom/osp/app/util/an;

    const v2, 0x7f070014

    invoke-static {v2}, Lcom/osp/app/util/an;->a(I)I

    move-result v2

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    invoke-virtual {v3, v1}, Landroid/widget/TextView;->setTextColor(I)V

    invoke-virtual {p0}, Lcom/osp/app/signin/SignInView;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-static {p0}, Lcom/osp/app/util/an;->a(Lcom/osp/app/util/BaseActivity;)Lcom/osp/app/util/an;

    const v2, 0x7f070014

    invoke-static {v2}, Lcom/osp/app/util/an;->a(I)I

    move-result v2

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    invoke-virtual {v3, v1}, Landroid/widget/TextView;->setLinkTextColor(I)V

    invoke-static {v3, v0, v0, v0, v0}, Lcom/msc/sa/c/d;->a(Landroid/view/View;IIII)V

    :cond_d
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "<a href=\""

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0}, Lcom/osp/app/signin/SignInView;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {p0}, Lcom/osp/app/signin/SignInView;->h()Z

    move-result v2

    invoke-static {v1, v2}, Lcom/msc/c/g;->a(Landroid/content/Context;Z)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "\">"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const v1, 0x7f09006a

    invoke-virtual {p0, v1}, Lcom/osp/app/signin/SignInView;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "</a>"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/Html;->fromHtml(Ljava/lang/String;)Landroid/text/Spanned;

    move-result-object v0

    invoke-virtual {v3, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    new-instance v0, Lcom/osp/app/signin/in;

    invoke-direct {v0, p0}, Lcom/osp/app/signin/in;-><init>(Lcom/osp/app/signin/SignInView;)V

    invoke-virtual {v3, v0}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    new-instance v0, Lcom/osp/app/signin/io;

    invoke-direct {v0, p0}, Lcom/osp/app/signin/io;-><init>(Lcom/osp/app/signin/SignInView;)V

    invoke-virtual {v3, v0}, Landroid/widget/TextView;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    new-instance v2, Landroid/app/AlertDialog$Builder;

    invoke-direct {v2, p0}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    const v0, 0x7f09004f

    invoke-virtual {v2, v0}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    invoke-virtual {v2, v4}, Landroid/app/AlertDialog$Builder;->setView(Landroid/view/View;)Landroid/app/AlertDialog$Builder;

    new-instance v0, Lcom/osp/app/signin/iv;

    invoke-direct {v0, p0}, Lcom/osp/app/signin/iv;-><init>(Lcom/osp/app/signin/SignInView;)V

    invoke-virtual {v2, v9, v0}, Landroid/app/AlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    new-instance v0, Lcom/osp/app/signin/iw;

    invoke-direct {v0, p0}, Lcom/osp/app/signin/iw;-><init>(Lcom/osp/app/signin/SignInView;)V

    invoke-virtual {v2, v10, v0}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    new-instance v0, Lcom/osp/app/signin/ix;

    invoke-direct {v0, p0}, Lcom/osp/app/signin/ix;-><init>(Lcom/osp/app/signin/SignInView;)V

    invoke-virtual {v2, v0}, Landroid/app/AlertDialog$Builder;->setOnCancelListener(Landroid/content/DialogInterface$OnCancelListener;)Landroid/app/AlertDialog$Builder;

    new-instance v0, Lcom/osp/app/signin/iy;

    invoke-direct {v0, p0}, Lcom/osp/app/signin/iy;-><init>(Lcom/osp/app/signin/SignInView;)V

    invoke-virtual {v2, v0}, Landroid/app/AlertDialog$Builder;->setOnKeyListener(Landroid/content/DialogInterface$OnKeyListener;)Landroid/app/AlertDialog$Builder;

    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v0, "SIV"

    const-string v1, "initializeComponent"

    const-string v3, "START"

    invoke-static {v0, v1, v3}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/osp/app/signin/SignInView;->S:Landroid/view/View;

    const v1, 0x7f0c0129

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/EditText;

    invoke-virtual {v0, p0}, Landroid/widget/EditText;->setOnFocusChangeListener(Landroid/view/View$OnFocusChangeListener;)V

    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v3, "initializeComponent - etEmailId : "

    invoke-direct {v1, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;)V

    invoke-static {p0}, Lcom/osp/app/util/ad;->h(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_18

    invoke-static {p0}, Lcom/osp/app/util/ad;->d(Landroid/content/Context;)Z

    move-result v3

    if-eqz v3, :cond_18

    invoke-static {}, Lcom/osp/device/b;->b()Lcom/osp/device/b;

    move-result-object v3

    invoke-virtual {v3}, Lcom/osp/device/b;->a()Z

    move-result v3

    if-eqz v3, :cond_e

    iget-boolean v3, p0, Lcom/osp/app/signin/SignInView;->ab:Z

    if-nez v3, :cond_f

    :cond_e
    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    :cond_f
    :goto_3
    new-instance v1, Lcom/osp/app/signin/ic;

    invoke-direct {v1, p0}, Lcom/osp/app/signin/ic;-><init>(Lcom/osp/app/signin/SignInView;)V

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->addTextChangedListener(Landroid/text/TextWatcher;)V

    iget-object v1, p0, Lcom/osp/app/signin/SignInView;->S:Landroid/view/View;

    const v3, 0x7f0c012a

    invoke-virtual {v1, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/EditText;

    invoke-virtual {v1, p0}, Landroid/widget/EditText;->setOnFocusChangeListener(Landroid/view/View$OnFocusChangeListener;)V

    new-instance v3, Lcom/osp/app/signin/id;

    invoke-direct {v3, p0}, Lcom/osp/app/signin/id;-><init>(Lcom/osp/app/signin/SignInView;)V

    invoke-virtual {v1, v3}, Landroid/widget/EditText;->addTextChangedListener(Landroid/text/TextWatcher;)V

    new-instance v3, Lcom/osp/app/signin/ie;

    invoke-direct {v3, p0, v0, v1}, Lcom/osp/app/signin/ie;-><init>(Lcom/osp/app/signin/SignInView;Landroid/widget/EditText;Landroid/widget/EditText;)V

    invoke-virtual {v1, v3}, Landroid/widget/EditText;->setOnEditorActionListener(Landroid/widget/TextView$OnEditorActionListener;)V

    new-instance v3, Lcom/osp/app/signin/if;

    invoke-direct {v3, p0, v0, v1}, Lcom/osp/app/signin/if;-><init>(Lcom/osp/app/signin/SignInView;Landroid/widget/EditText;Landroid/widget/EditText;)V

    invoke-virtual {v1, v3}, Landroid/widget/EditText;->setOnKeyListener(Landroid/view/View$OnKeyListener;)V

    iget-object v0, p0, Lcom/osp/app/signin/SignInView;->S:Landroid/view/View;

    const v3, 0x7f0c00b7

    invoke-virtual {v0, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/CheckBox;

    new-instance v3, Lcom/osp/app/signin/ih;

    invoke-direct {v3, p0, v1}, Lcom/osp/app/signin/ih;-><init>(Lcom/osp/app/signin/SignInView;Landroid/widget/EditText;)V

    invoke-virtual {v0, v3}, Landroid/widget/CheckBox;->setOnCheckedChangeListener(Landroid/widget/CompoundButton$OnCheckedChangeListener;)V

    invoke-virtual {v2}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v0

    iput-object v0, p0, Lcom/osp/app/signin/SignInView;->a:Landroid/app/AlertDialog;

    iget-object v0, p0, Lcom/osp/app/signin/SignInView;->a:Landroid/app/AlertDialog;

    invoke-virtual {v0}, Landroid/app/AlertDialog;->getWindow()Landroid/view/Window;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/Window;->getAttributes()Landroid/view/WindowManager$LayoutParams;

    move-result-object v0

    invoke-virtual {p0}, Lcom/osp/app/signin/SignInView;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f080101

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v1

    float-to-int v1, v1

    iput v1, v0, Landroid/view/WindowManager$LayoutParams;->width:I

    iget-object v1, p0, Lcom/osp/app/signin/SignInView;->a:Landroid/app/AlertDialog;

    invoke-virtual {v1}, Landroid/app/AlertDialog;->getWindow()Landroid/view/Window;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/view/Window;->setAttributes(Landroid/view/WindowManager$LayoutParams;)V

    .line 606
    :cond_10
    :goto_4
    iget-object v0, p0, Lcom/osp/app/signin/SignInView;->y:Landroid/content/Intent;

    const-string v1, "email_id"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 608
    iget-object v0, p0, Lcom/osp/app/signin/SignInView;->a:Landroid/app/AlertDialog;

    if-eqz v0, :cond_2a

    .line 610
    invoke-direct {p0}, Lcom/osp/app/signin/SignInView;->e()Z

    move-result v0

    if-eqz v0, :cond_11

    iget-object v0, p0, Lcom/osp/app/signin/SignInView;->a:Landroid/app/AlertDialog;

    if-eqz v0, :cond_11

    iget-object v0, p0, Lcom/osp/app/signin/SignInView;->a:Landroid/app/AlertDialog;

    invoke-virtual {v0}, Landroid/app/AlertDialog;->isShowing()Z

    move-result v0

    if-nez v0, :cond_11

    iget-object v0, p0, Lcom/osp/app/signin/SignInView;->a:Landroid/app/AlertDialog;

    invoke-virtual {v0}, Landroid/app/AlertDialog;->getWindow()Landroid/view/Window;

    move-result-object v0

    const/4 v1, 0x3

    invoke-virtual {v0, v1}, Landroid/view/Window;->setSoftInputMode(I)V

    iget-object v0, p0, Lcom/osp/app/signin/SignInView;->a:Landroid/app/AlertDialog;

    invoke-virtual {v0}, Landroid/app/AlertDialog;->show()V

    .line 611
    :cond_11
    invoke-virtual {p0}, Lcom/osp/app/signin/SignInView;->a()V

    .line 615
    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_12

    .line 617
    iget-object v0, p0, Lcom/osp/app/signin/SignInView;->S:Landroid/view/View;

    const v1, 0x7f0c0129

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/EditText;

    .line 618
    iget-object v1, p0, Lcom/osp/app/signin/SignInView;->S:Landroid/view/View;

    const v3, 0x7f0c012a

    invoke-virtual {v1, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/EditText;

    .line 619
    invoke-virtual {v0, v2}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    .line 620
    invoke-virtual {v1}, Landroid/widget/EditText;->requestFocus()Z

    .line 622
    :cond_12
    invoke-virtual {p0}, Lcom/osp/app/signin/SignInView;->getWindow()Landroid/view/Window;

    move-result-object v0

    const/4 v1, 0x3

    invoke-virtual {v0, v1}, Landroid/view/Window;->setSoftInputMode(I)V

    .line 744
    :cond_13
    :goto_5
    return-void

    .line 566
    :cond_14
    const v0, 0x7f03002e

    invoke-static {p0}, Lcom/osp/app/util/r;->q(Landroid/content/Context;)Z

    move-result v1

    invoke-virtual {p0, v0, v1}, Lcom/osp/app/signin/SignInView;->a(IZ)V

    goto/16 :goto_0

    .line 584
    :cond_15
    invoke-static {}, Lcom/osp/device/b;->b()Lcom/osp/device/b;

    move-result-object v1

    invoke-virtual {v1}, Lcom/osp/device/b;->a()Z

    move-result v1

    if-eqz v1, :cond_16

    const-string v1, "com.msc.action.samsungaccount.resignin_with_signout"

    iget-object v2, p0, Lcom/osp/app/signin/SignInView;->y:Landroid/content/Intent;

    invoke-virtual {v2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_16

    invoke-static {p0}, Lcom/osp/app/util/ad;->i(Landroid/content/Context;)V

    iput-boolean v8, p0, Lcom/osp/app/signin/SignInView;->aa:Z

    iput-boolean v8, p0, Lcom/osp/app/signin/SignInView;->ab:Z

    goto/16 :goto_1

    :cond_16
    iget-object v1, p0, Lcom/osp/app/signin/SignInView;->y:Landroid/content/Intent;

    invoke-virtual {v1}, Landroid/content/Intent;->getComponent()Landroid/content/ComponentName;

    move-result-object v1

    if-eqz v1, :cond_3

    invoke-virtual {v1}, Landroid/content/ComponentName;->getClassName()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_3

    const-string v1, "ReSignInView"

    invoke-virtual {v0, v1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_3

    iput-boolean v8, p0, Lcom/osp/app/signin/SignInView;->aa:Z

    goto/16 :goto_1

    :cond_17
    iget-object v1, p0, Lcom/osp/app/signin/SignInView;->y:Landroid/content/Intent;

    invoke-virtual {v1}, Landroid/content/Intent;->getComponent()Landroid/content/ComponentName;

    move-result-object v1

    if-eqz v1, :cond_4

    invoke-virtual {v1}, Landroid/content/ComponentName;->getClassName()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_4

    const-string v1, "ActivateAccountView"

    invoke-virtual {v0, v1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_4

    iput-boolean v8, p0, Lcom/osp/app/signin/SignInView;->ac:Z

    goto/16 :goto_2

    .line 604
    :cond_18
    if-eqz v1, :cond_f

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    goto/16 :goto_3

    :cond_19
    const v0, 0x7f0c00c8

    invoke-virtual {p0, v0}, Lcom/osp/app/signin/SignInView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    if-eqz v0, :cond_1a

    invoke-static {p0}, Lcom/osp/app/util/r;->g(Landroid/content/Context;)Z

    move-result v1

    if-eqz v1, :cond_28

    invoke-virtual {v0, v7}, Landroid/widget/LinearLayout;->setBackgroundColor(I)V

    :cond_1a
    :goto_6
    const v0, 0x7f0c00b4

    invoke-virtual {p0, v0}, Lcom/osp/app/signin/SignInView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    if-eqz v0, :cond_1b

    invoke-static {}, Lcom/osp/device/b;->b()Lcom/osp/device/b;

    move-result-object v1

    invoke-virtual {v1, p0}, Lcom/osp/device/b;->i(Landroid/content/Context;)Z

    move-result v1

    if-nez v1, :cond_1b

    invoke-static {p0}, Lcom/osp/app/util/an;->a(Lcom/osp/app/util/BaseActivity;)Lcom/osp/app/util/an;

    const v1, 0x7f070012

    invoke-static {v1}, Lcom/osp/app/util/an;->a(I)I

    move-result v1

    invoke-virtual {v2, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setTextColor(I)V

    invoke-static {p0}, Lcom/osp/app/util/r;->m(Landroid/content/Context;)Z

    move-result v1

    if-eqz v1, :cond_1b

    invoke-virtual {p0}, Lcom/osp/app/signin/SignInView;->h()Z

    move-result v1

    if-nez v1, :cond_1b

    invoke-virtual {p0}, Lcom/osp/app/signin/SignInView;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v3, 0x7f070006

    invoke-virtual {v1, v3}, Landroid/content/res/Resources;->getColorStateList(I)Landroid/content/res/ColorStateList;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setTextColor(Landroid/content/res/ColorStateList;)V

    :cond_1b
    const v0, 0x7f0c00b3

    invoke-virtual {p0, v0}, Lcom/osp/app/signin/SignInView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    if-eqz v0, :cond_1c

    invoke-static {p0}, Lcom/osp/app/util/an;->a(Lcom/osp/app/util/BaseActivity;)Lcom/osp/app/util/an;

    const v1, 0x7f070006

    invoke-static {v1}, Lcom/osp/app/util/an;->a(I)I

    move-result v1

    invoke-virtual {v2, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setTextColor(I)V

    :cond_1c
    const v0, 0x7f0c0124

    invoke-virtual {p0, v0}, Lcom/osp/app/signin/SignInView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    if-eqz v0, :cond_1d

    invoke-static {p0}, Lcom/osp/app/util/an;->a(Lcom/osp/app/util/BaseActivity;)Lcom/osp/app/util/an;

    const v1, 0x7f070006

    invoke-static {v1}, Lcom/osp/app/util/an;->a(I)I

    move-result v1

    invoke-virtual {v2, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setTextColor(I)V

    :cond_1d
    invoke-direct {p0}, Lcom/osp/app/signin/SignInView;->e()Z

    move-result v0

    if-eqz v0, :cond_29

    iget-object v0, p0, Lcom/osp/app/signin/SignInView;->S:Landroid/view/View;

    const v1, 0x7f0c00b7

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/CheckBox;

    :goto_7
    if-eqz v0, :cond_1e

    invoke-static {p0}, Lcom/osp/app/util/an;->a(Lcom/osp/app/util/BaseActivity;)Lcom/osp/app/util/an;

    const v1, 0x7f070004

    invoke-static {v1}, Lcom/osp/app/util/an;->a(I)I

    move-result v1

    invoke-virtual {v2, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/CheckBox;->setTextColor(I)V

    sget v1, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v3, 0x10

    if-le v1, v3, :cond_1e

    invoke-virtual {p0}, Lcom/osp/app/signin/SignInView;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v3, 0x7f0800e2

    invoke-virtual {v1, v3}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v1

    float-to-int v1, v1

    invoke-static {v0, v1, v7, v7, v7}, Lcom/msc/sa/c/d;->a(Landroid/view/View;IIII)V

    :cond_1e
    invoke-static {}, Lcom/osp/app/util/r;->v()Z

    move-result v1

    if-eqz v1, :cond_21

    const v1, 0x7f0c00e1

    invoke-virtual {p0, v1}, Lcom/osp/app/signin/SignInView;->findViewById(I)Landroid/view/View;

    move-result-object v1

    if-eqz v1, :cond_1f

    const/16 v3, 0x8

    invoke-virtual {v1, v3}, Landroid/view/View;->setVisibility(I)V

    :cond_1f
    if-eqz v0, :cond_20

    invoke-static {}, Lcom/osp/device/b;->b()Lcom/osp/device/b;

    move-result-object v1

    invoke-virtual {v1, p0}, Lcom/osp/device/b;->i(Landroid/content/Context;)Z

    move-result v1

    if-nez v1, :cond_20

    invoke-virtual {p0}, Lcom/osp/app/signin/SignInView;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v3, 0x7f08007c

    invoke-virtual {v1, v3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/CheckBox;->setMinimumHeight(I)V

    :cond_20
    const v0, 0x7f0c0123

    invoke-virtual {p0, v0}, Lcom/osp/app/signin/SignInView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    if-eqz v0, :cond_21

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    :cond_21
    const v0, 0x7f0c00b8

    invoke-virtual {p0, v0}, Lcom/osp/app/signin/SignInView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    if-eqz v0, :cond_22

    invoke-virtual {p0}, Lcom/osp/app/signin/SignInView;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v3, 0x7f0800e6

    invoke-virtual {v1, v3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    invoke-static {p0}, Lcom/osp/app/util/an;->a(Lcom/osp/app/util/BaseActivity;)Lcom/osp/app/util/an;

    const v3, 0x7f020089

    invoke-static {v3}, Lcom/osp/app/util/an;->a(I)I

    move-result v3

    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setBackgroundResource(I)V

    invoke-static {p0}, Lcom/osp/app/util/an;->a(Lcom/osp/app/util/BaseActivity;)Lcom/osp/app/util/an;

    const v3, 0x7f070014

    invoke-static {v3}, Lcom/osp/app/util/an;->a(I)I

    move-result v3

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getColor(I)I

    move-result v3

    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setTextColor(I)V

    invoke-static {p0}, Lcom/osp/app/util/an;->a(Lcom/osp/app/util/BaseActivity;)Lcom/osp/app/util/an;

    const v3, 0x7f070014

    invoke-static {v3}, Lcom/osp/app/util/an;->a(I)I

    move-result v3

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getColor(I)I

    move-result v3

    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setLinkTextColor(I)V

    invoke-static {v0, v1, v1, v1, v1}, Lcom/msc/sa/c/d;->a(Landroid/view/View;IIII)V

    :cond_22
    const v0, 0x7f0c0127

    invoke-virtual {p0, v0}, Lcom/osp/app/signin/SignInView;->findViewById(I)Landroid/view/View;

    move-result-object v1

    iget-boolean v0, p0, Lcom/osp/app/signin/SignInView;->ac:Z

    if-eqz v0, :cond_23

    if-eqz v1, :cond_23

    invoke-static {}, Lcom/osp/common/util/i;->a()Lcom/osp/common/util/i;

    invoke-static {p0}, Lcom/osp/common/util/i;->b(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_23

    move-object v0, v1

    check-cast v0, Landroid/widget/TextView;

    invoke-static {p0}, Lcom/osp/app/util/an;->a(Lcom/osp/app/util/BaseActivity;)Lcom/osp/app/util/an;

    const v3, 0x7f070004

    invoke-static {v3}, Lcom/osp/app/util/an;->a(I)I

    move-result v3

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getColor(I)I

    move-result v2

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setTextColor(I)V

    invoke-virtual {v1, v7}, Landroid/view/View;->setVisibility(I)V

    new-instance v0, Lcom/osp/app/signin/iu;

    invoke-direct {v0, p0}, Lcom/osp/app/signin/iu;-><init>(Lcom/osp/app/signin/SignInView;)V

    invoke-virtual {v1, v0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    :cond_23
    invoke-static {}, Lcom/osp/app/util/r;->t()Z

    move-result v0

    if-eqz v0, :cond_24

    invoke-static {}, Lcom/osp/device/b;->b()Lcom/osp/device/b;

    invoke-static {p0}, Lcom/osp/device/b;->a(Landroid/content/Context;)Z

    move-result v0

    if-nez v0, :cond_24

    const v0, 0x7f0c0121

    invoke-virtual {p0, v0}, Lcom/osp/app/signin/SignInView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/EditText;

    const v1, 0x7f090112

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setHint(I)V

    :cond_24
    const v0, 0x7f0c0065

    invoke-virtual {p0, v0}, Lcom/osp/app/signin/SignInView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/osp/app/signin/BottomSoftkeyLinearLayout;

    iput-object v0, p0, Lcom/osp/app/signin/SignInView;->at:Lcom/osp/app/signin/BottomSoftkeyLinearLayout;

    iget-object v0, p0, Lcom/osp/app/signin/SignInView;->at:Lcom/osp/app/signin/BottomSoftkeyLinearLayout;

    if-eqz v0, :cond_25

    iget-object v0, p0, Lcom/osp/app/signin/SignInView;->at:Lcom/osp/app/signin/BottomSoftkeyLinearLayout;

    invoke-virtual {p0}, Lcom/osp/app/signin/SignInView;->h()Z

    move-result v1

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-virtual {v0, p0, v1}, Lcom/osp/app/signin/BottomSoftkeyLinearLayout;->a(Landroid/content/Context;Ljava/lang/Boolean;)V

    :cond_25
    invoke-static {}, Lcom/osp/device/b;->b()Lcom/osp/device/b;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/osp/device/b;->i(Landroid/content/Context;)Z

    move-result v0

    if-nez v0, :cond_26

    const v0, 0x7f0c0128

    invoke-virtual {p0, v0}, Lcom/osp/app/signin/SignInView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/osp/app/signin/BottomSoftkeyLinearLayout;

    iput-object v0, p0, Lcom/osp/app/signin/SignInView;->au:Lcom/osp/app/signin/BottomSoftkeyLinearLayout;

    iget-object v0, p0, Lcom/osp/app/signin/SignInView;->au:Lcom/osp/app/signin/BottomSoftkeyLinearLayout;

    if-eqz v0, :cond_26

    iget-object v0, p0, Lcom/osp/app/signin/SignInView;->au:Lcom/osp/app/signin/BottomSoftkeyLinearLayout;

    invoke-virtual {p0}, Lcom/osp/app/signin/SignInView;->h()Z

    move-result v1

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-virtual {v0, p0, v1}, Lcom/osp/app/signin/BottomSoftkeyLinearLayout;->a(Landroid/content/Context;Ljava/lang/Boolean;)V

    :cond_26
    invoke-static {}, Lcom/osp/app/signin/SamsungService;->a()Z

    move-result v0

    if-nez v0, :cond_27

    invoke-static {}, Lcom/osp/device/b;->b()Lcom/osp/device/b;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/osp/device/b;->i(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_27

    iget-object v0, p0, Lcom/osp/app/signin/SignInView;->at:Lcom/osp/app/signin/BottomSoftkeyLinearLayout;

    if-eqz v0, :cond_27

    iget-object v0, p0, Lcom/osp/app/signin/SignInView;->at:Lcom/osp/app/signin/BottomSoftkeyLinearLayout;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Lcom/osp/app/signin/BottomSoftkeyLinearLayout;->setVisibility(I)V

    :cond_27
    invoke-static {}, Lcom/osp/app/util/r;->v()Z

    move-result v0

    if-eqz v0, :cond_10

    const v0, 0x7f0c0126

    invoke-virtual {p0, v0}, Lcom/osp/app/signin/SignInView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    goto/16 :goto_4

    :cond_28
    invoke-static {}, Lcom/osp/device/b;->b()Lcom/osp/device/b;

    move-result-object v1

    invoke-virtual {v1, p0}, Lcom/osp/device/b;->i(Landroid/content/Context;)Z

    move-result v1

    if-nez v1, :cond_1a

    invoke-static {p0}, Lcom/osp/app/util/an;->a(Lcom/osp/app/util/BaseActivity;)Lcom/osp/app/util/an;

    const v1, 0x7f020142

    invoke-static {v1}, Lcom/osp/app/util/an;->a(I)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setBackgroundResource(I)V

    goto/16 :goto_6

    :cond_29
    const v0, 0x7f0c0125

    invoke-virtual {p0, v0}, Lcom/osp/app/signin/SignInView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/CheckBox;

    goto/16 :goto_7

    .line 626
    :cond_2a
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v0, "SIV"

    const-string v1, "initializeComponent"

    const-string v3, "START"

    invoke-static {v0, v1, v3}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    const v0, 0x7f0c00c8

    invoke-virtual {p0, v0}, Lcom/osp/app/signin/SignInView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    const v1, 0x7f0c0120

    invoke-virtual {p0, v1}, Lcom/osp/app/signin/SignInView;->findViewById(I)Landroid/view/View;

    move-result-object v1

    const v3, 0x7f0c011f

    invoke-virtual {p0, v3}, Lcom/osp/app/signin/SignInView;->findViewById(I)Landroid/view/View;

    move-result-object v3

    const v4, 0x7f0c0123

    invoke-virtual {p0, v4}, Lcom/osp/app/signin/SignInView;->findViewById(I)Landroid/view/View;

    move-result-object v4

    const v5, 0x7f0c0179

    invoke-virtual {p0, v5}, Lcom/osp/app/signin/SignInView;->findViewById(I)Landroid/view/View;

    move-result-object v5

    const/16 v6, 0x8

    invoke-virtual {v0, v6}, Landroid/view/View;->setVisibility(I)V

    invoke-static {}, Lcom/osp/device/b;->b()Lcom/osp/device/b;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/osp/device/b;->i(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_38

    invoke-virtual {v3, v7}, Landroid/view/View;->setVisibility(I)V

    invoke-virtual {v4, v7}, Landroid/view/View;->setVisibility(I)V

    invoke-virtual {v5, v7}, Landroid/view/View;->setVisibility(I)V

    :cond_2b
    :goto_8
    invoke-static {}, Lcom/osp/app/util/r;->v()Z

    move-result v0

    if-eqz v0, :cond_2d

    if-eqz v3, :cond_2c

    const/4 v0, 0x4

    invoke-virtual {v3, v0}, Landroid/view/View;->setVisibility(I)V

    :cond_2c
    if-eqz v4, :cond_2d

    const/16 v0, 0x8

    invoke-virtual {v4, v0}, Landroid/view/View;->setVisibility(I)V

    :cond_2d
    const v0, 0x7f0c0121

    invoke-virtual {p0, v0}, Lcom/osp/app/signin/SignInView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/EditText;

    invoke-virtual {v0, p0}, Landroid/widget/EditText;->setOnFocusChangeListener(Landroid/view/View$OnFocusChangeListener;)V

    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v3, "initializeComponent - etEmailId : "

    invoke-direct {v1, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;)V

    invoke-static {}, Lcom/osp/app/signin/SamsungService;->a()Z

    move-result v1

    if-eqz v1, :cond_2e

    iget-boolean v1, p0, Lcom/osp/app/signin/SignInView;->ac:Z

    if-eqz v1, :cond_2e

    iget-boolean v1, p0, Lcom/osp/app/signin/SignInView;->aa:Z

    if-nez v1, :cond_2e

    const v1, 0x7f0c011d

    invoke-virtual {p0, v1}, Lcom/osp/app/signin/SignInView;->findViewById(I)Landroid/view/View;

    move-result-object v1

    invoke-virtual {v1, v7}, Landroid/view/View;->setVisibility(I)V

    invoke-static {}, Lcom/osp/device/b;->b()Lcom/osp/device/b;

    move-result-object v1

    invoke-virtual {v1, p0}, Lcom/osp/device/b;->i(Landroid/content/Context;)Z

    move-result v1

    if-eqz v1, :cond_2e

    invoke-static {}, Lcom/osp/app/util/r;->v()Z

    move-result v1

    if-eqz v1, :cond_39

    const v1, 0x7f0c011f

    invoke-virtual {p0, v1}, Lcom/osp/app/signin/SignInView;->findViewById(I)Landroid/view/View;

    move-result-object v1

    const/4 v3, 0x4

    invoke-virtual {v1, v3}, Landroid/view/View;->setVisibility(I)V

    :cond_2e
    :goto_9
    invoke-static {p0}, Lcom/osp/app/util/ad;->h(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_3a

    invoke-static {p0}, Lcom/osp/app/util/ad;->d(Landroid/content/Context;)Z

    move-result v3

    if-eqz v3, :cond_3a

    invoke-static {}, Lcom/osp/device/b;->b()Lcom/osp/device/b;

    move-result-object v3

    invoke-virtual {v3}, Lcom/osp/device/b;->a()Z

    move-result v3

    if-eqz v3, :cond_2f

    iget-boolean v3, p0, Lcom/osp/app/signin/SignInView;->ab:Z

    if-nez v3, :cond_30

    :cond_2f
    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    :cond_30
    :goto_a
    new-instance v1, Lcom/osp/app/signin/ii;

    invoke-direct {v1, p0}, Lcom/osp/app/signin/ii;-><init>(Lcom/osp/app/signin/SignInView;)V

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->addTextChangedListener(Landroid/text/TextWatcher;)V

    const v1, 0x7f0c0122

    invoke-virtual {p0, v1}, Lcom/osp/app/signin/SignInView;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/EditText;

    invoke-virtual {v1, p0}, Landroid/widget/EditText;->setOnFocusChangeListener(Landroid/view/View$OnFocusChangeListener;)V

    new-instance v3, Lcom/osp/app/signin/ij;

    invoke-direct {v3, p0}, Lcom/osp/app/signin/ij;-><init>(Lcom/osp/app/signin/SignInView;)V

    invoke-virtual {v1, v3}, Landroid/widget/EditText;->addTextChangedListener(Landroid/text/TextWatcher;)V

    new-instance v3, Lcom/osp/app/signin/ik;

    invoke-direct {v3, p0, v0, v1}, Lcom/osp/app/signin/ik;-><init>(Lcom/osp/app/signin/SignInView;Landroid/widget/EditText;Landroid/widget/EditText;)V

    invoke-virtual {v1, v3}, Landroid/widget/EditText;->setOnEditorActionListener(Landroid/widget/TextView$OnEditorActionListener;)V

    new-instance v3, Lcom/osp/app/signin/il;

    invoke-direct {v3, p0, v0, v1}, Lcom/osp/app/signin/il;-><init>(Lcom/osp/app/signin/SignInView;Landroid/widget/EditText;Landroid/widget/EditText;)V

    invoke-virtual {v1, v3}, Landroid/widget/EditText;->setOnKeyListener(Landroid/view/View$OnKeyListener;)V

    invoke-direct {p0}, Lcom/osp/app/signin/SignInView;->e()Z

    move-result v0

    if-eqz v0, :cond_3b

    iget-object v0, p0, Lcom/osp/app/signin/SignInView;->S:Landroid/view/View;

    const v3, 0x7f0c00b7

    invoke-virtual {v0, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/CheckBox;

    :goto_b
    new-instance v3, Lcom/osp/app/signin/im;

    invoke-direct {v3, p0, v1}, Lcom/osp/app/signin/im;-><init>(Lcom/osp/app/signin/SignInView;Landroid/widget/EditText;)V

    invoke-virtual {v0, v3}, Landroid/widget/CheckBox;->setOnCheckedChangeListener(Landroid/widget/CompoundButton$OnCheckedChangeListener;)V

    invoke-static {}, Lcom/osp/device/b;->b()Lcom/osp/device/b;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/osp/device/b;->l(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_3c

    iget-object v0, p0, Lcom/osp/app/signin/SignInView;->at:Lcom/osp/app/signin/BottomSoftkeyLinearLayout;

    invoke-virtual {p0, v10}, Lcom/osp/app/signin/SignInView;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/osp/app/signin/BottomSoftkeyLinearLayout;->b(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/osp/app/signin/SignInView;->at:Lcom/osp/app/signin/BottomSoftkeyLinearLayout;

    invoke-virtual {p0, v10}, Lcom/osp/app/signin/SignInView;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/osp/app/signin/BottomSoftkeyLinearLayout;->d(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/osp/app/signin/SignInView;->at:Lcom/osp/app/signin/BottomSoftkeyLinearLayout;

    invoke-virtual {p0, v9}, Lcom/osp/app/signin/SignInView;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/osp/app/signin/BottomSoftkeyLinearLayout;->c(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/osp/app/signin/SignInView;->at:Lcom/osp/app/signin/BottomSoftkeyLinearLayout;

    invoke-virtual {p0, v9}, Lcom/osp/app/signin/SignInView;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/osp/app/signin/BottomSoftkeyLinearLayout;->e(Ljava/lang/String;)V

    :goto_c
    invoke-static {}, Lcom/osp/device/b;->b()Lcom/osp/device/b;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/osp/device/b;->i(Landroid/content/Context;)Z

    move-result v0

    if-nez v0, :cond_31

    invoke-static {}, Lcom/osp/device/b;->b()Lcom/osp/device/b;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/osp/device/b;->l(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_3d

    iget-object v0, p0, Lcom/osp/app/signin/SignInView;->au:Lcom/osp/app/signin/BottomSoftkeyLinearLayout;

    invoke-virtual {p0, v10}, Lcom/osp/app/signin/SignInView;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/osp/app/signin/BottomSoftkeyLinearLayout;->b(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/osp/app/signin/SignInView;->au:Lcom/osp/app/signin/BottomSoftkeyLinearLayout;

    invoke-virtual {p0, v10}, Lcom/osp/app/signin/SignInView;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/osp/app/signin/BottomSoftkeyLinearLayout;->d(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/osp/app/signin/SignInView;->au:Lcom/osp/app/signin/BottomSoftkeyLinearLayout;

    invoke-virtual {p0, v9}, Lcom/osp/app/signin/SignInView;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/osp/app/signin/BottomSoftkeyLinearLayout;->c(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/osp/app/signin/SignInView;->au:Lcom/osp/app/signin/BottomSoftkeyLinearLayout;

    invoke-virtual {p0, v9}, Lcom/osp/app/signin/SignInView;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/osp/app/signin/BottomSoftkeyLinearLayout;->e(Ljava/lang/String;)V

    :cond_31
    :goto_d
    iget-boolean v0, p0, Lcom/osp/app/signin/SignInView;->ac:Z

    if-eqz v0, :cond_32

    invoke-static {}, Lcom/osp/device/b;->b()Lcom/osp/device/b;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/osp/device/b;->l(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_3e

    iget-object v0, p0, Lcom/osp/app/signin/SignInView;->at:Lcom/osp/app/signin/BottomSoftkeyLinearLayout;

    invoke-virtual {p0, v11}, Lcom/osp/app/signin/SignInView;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/osp/app/signin/BottomSoftkeyLinearLayout;->c(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/osp/app/signin/SignInView;->at:Lcom/osp/app/signin/BottomSoftkeyLinearLayout;

    invoke-virtual {p0, v11}, Lcom/osp/app/signin/SignInView;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/osp/app/signin/BottomSoftkeyLinearLayout;->e(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/osp/app/signin/SignInView;->au:Lcom/osp/app/signin/BottomSoftkeyLinearLayout;

    if-eqz v0, :cond_32

    iget-object v0, p0, Lcom/osp/app/signin/SignInView;->au:Lcom/osp/app/signin/BottomSoftkeyLinearLayout;

    invoke-virtual {p0, v11}, Lcom/osp/app/signin/SignInView;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/osp/app/signin/BottomSoftkeyLinearLayout;->c(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/osp/app/signin/SignInView;->au:Lcom/osp/app/signin/BottomSoftkeyLinearLayout;

    invoke-virtual {p0, v11}, Lcom/osp/app/signin/SignInView;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/osp/app/signin/BottomSoftkeyLinearLayout;->e(Ljava/lang/String;)V

    :cond_32
    :goto_e
    invoke-static {}, Lcom/osp/device/b;->b()Lcom/osp/device/b;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/osp/device/b;->l(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_3f

    iget-object v0, p0, Lcom/osp/app/signin/SignInView;->at:Lcom/osp/app/signin/BottomSoftkeyLinearLayout;

    iget-object v1, p0, Lcom/osp/app/signin/SignInView;->b:Landroid/view/View$OnClickListener;

    invoke-virtual {v0, v1}, Lcom/osp/app/signin/BottomSoftkeyLinearLayout;->a(Landroid/view/View$OnClickListener;)V

    iget-object v0, p0, Lcom/osp/app/signin/SignInView;->at:Lcom/osp/app/signin/BottomSoftkeyLinearLayout;

    iget-object v1, p0, Lcom/osp/app/signin/SignInView;->c:Landroid/view/View$OnClickListener;

    invoke-virtual {v0, v1}, Lcom/osp/app/signin/BottomSoftkeyLinearLayout;->b(Landroid/view/View$OnClickListener;)V

    :goto_f
    invoke-static {}, Lcom/osp/device/b;->b()Lcom/osp/device/b;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/osp/device/b;->i(Landroid/content/Context;)Z

    move-result v0

    if-nez v0, :cond_33

    invoke-static {}, Lcom/osp/device/b;->b()Lcom/osp/device/b;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/osp/device/b;->l(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_40

    iget-object v0, p0, Lcom/osp/app/signin/SignInView;->au:Lcom/osp/app/signin/BottomSoftkeyLinearLayout;

    iget-object v1, p0, Lcom/osp/app/signin/SignInView;->b:Landroid/view/View$OnClickListener;

    invoke-virtual {v0, v1}, Lcom/osp/app/signin/BottomSoftkeyLinearLayout;->a(Landroid/view/View$OnClickListener;)V

    iget-object v0, p0, Lcom/osp/app/signin/SignInView;->au:Lcom/osp/app/signin/BottomSoftkeyLinearLayout;

    iget-object v1, p0, Lcom/osp/app/signin/SignInView;->c:Landroid/view/View$OnClickListener;

    invoke-virtual {v0, v1}, Lcom/osp/app/signin/BottomSoftkeyLinearLayout;->b(Landroid/view/View$OnClickListener;)V

    :cond_33
    :goto_10
    const v0, 0x7f0c00b8

    invoke-virtual {p0, v0}, Lcom/osp/app/signin/SignInView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    invoke-direct {p0, v0}, Lcom/osp/app/signin/SignInView;->b(Landroid/widget/TextView;)V

    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v0, "SIV"

    const-string v1, "initializeComponent"

    const-string v3, "END"

    invoke-static {v0, v1, v3}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {p0}, Lcom/osp/app/signin/SignInView;->a()V

    .line 628
    const v0, 0x7f0c0121

    invoke-virtual {p0, v0}, Lcom/osp/app/signin/SignInView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/EditText;

    .line 629
    const v1, 0x7f0c0122

    invoke-virtual {p0, v1}, Lcom/osp/app/signin/SignInView;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/EditText;

    .line 632
    iget-boolean v3, p0, Lcom/osp/app/signin/SignInView;->aa:Z

    if-eqz v3, :cond_34

    iget-boolean v3, p0, Lcom/osp/app/signin/SignInView;->ab:Z

    if-nez v3, :cond_34

    if-eqz v0, :cond_34

    .line 634
    invoke-virtual {v0, v7}, Landroid/widget/EditText;->setEnabled(Z)V

    .line 637
    :cond_34
    if-eqz v2, :cond_41

    invoke-virtual {v2}, Ljava/lang/String;->isEmpty()Z

    move-result v3

    if-nez v3, :cond_41

    .line 639
    iput-boolean v8, p0, Lcom/osp/app/signin/SignInView;->ag:Z

    .line 640
    invoke-virtual {v0, v2}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    .line 641
    invoke-virtual {v1}, Landroid/widget/EditText;->requestFocus()Z

    .line 642
    iget-boolean v0, p0, Lcom/osp/app/signin/SignInView;->aa:Z

    if-eqz v0, :cond_35

    .line 644
    const v0, 0x7f0c00b4

    invoke-virtual {p0, v0}, Lcom/osp/app/signin/SignInView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 645
    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 659
    :cond_35
    :goto_11
    invoke-virtual {p0}, Lcom/osp/app/signin/SignInView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v0

    iget v0, v0, Landroid/content/res/Configuration;->orientation:I

    invoke-direct {p0, v0}, Lcom/osp/app/signin/SignInView;->a(I)V

    .line 661
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v0, "SIV"

    const-string v1, "onCreate"

    const-string v2, "END"

    invoke-static {v0, v1, v2}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 678
    invoke-static {p0}, Lcom/msc/sa/c/d;->a(Landroid/app/Activity;)V

    .line 689
    iget-boolean v0, p0, Lcom/osp/app/signin/SignInView;->aa:Z

    if-eqz v0, :cond_44

    .line 691
    const v0, 0x7f09004f

    invoke-virtual {p0, v0}, Lcom/osp/app/signin/SignInView;->setTitle(I)V

    .line 712
    invoke-static {p0}, Lcom/msc/sa/c/d;->a(Landroid/app/Activity;)V

    .line 714
    invoke-static {}, Lcom/osp/app/signin/SamsungService;->a()Z

    move-result v0

    if-nez v0, :cond_36

    invoke-static {}, Lcom/osp/device/b;->b()Lcom/osp/device/b;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/osp/device/b;->i(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_36

    .line 716
    const-string v0, "done_cancel"

    invoke-virtual {p0, v0}, Lcom/osp/app/signin/SignInView;->e(Ljava/lang/String;)V

    .line 720
    :cond_36
    invoke-virtual {p0}, Lcom/osp/app/signin/SignInView;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string v1, "from_notification"

    invoke-virtual {v0, v1, v7}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v0

    if-eqz v0, :cond_43

    .line 722
    invoke-direct {p0}, Lcom/osp/app/signin/SignInView;->f()V

    .line 738
    :cond_37
    :goto_12
    invoke-direct {p0}, Lcom/osp/app/signin/SignInView;->u()V

    .line 740
    invoke-direct {p0}, Lcom/osp/app/signin/SignInView;->e()Z

    move-result v0

    if-nez v0, :cond_13

    .line 742
    invoke-virtual {p0}, Lcom/osp/app/signin/SignInView;->getWindow()Landroid/view/Window;

    move-result-object v0

    const/4 v1, 0x5

    invoke-virtual {v0, v1}, Landroid/view/Window;->setSoftInputMode(I)V

    goto/16 :goto_5

    .line 626
    :cond_38
    invoke-virtual {v1, v7}, Landroid/view/View;->setVisibility(I)V

    iget-object v0, p0, Lcom/osp/app/signin/SignInView;->at:Lcom/osp/app/signin/BottomSoftkeyLinearLayout;

    invoke-virtual {v0, v7}, Lcom/osp/app/signin/BottomSoftkeyLinearLayout;->setVisibility(I)V

    invoke-static {}, Lcom/osp/device/b;->b()Lcom/osp/device/b;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/osp/device/b;->i(Landroid/content/Context;)Z

    move-result v0

    if-nez v0, :cond_2b

    iget-object v0, p0, Lcom/osp/app/signin/SignInView;->au:Lcom/osp/app/signin/BottomSoftkeyLinearLayout;

    if-eqz v0, :cond_2b

    iget-object v0, p0, Lcom/osp/app/signin/SignInView;->au:Lcom/osp/app/signin/BottomSoftkeyLinearLayout;

    invoke-virtual {v0, v7}, Lcom/osp/app/signin/BottomSoftkeyLinearLayout;->setVisibility(I)V

    goto/16 :goto_8

    :cond_39
    const v1, 0x7f0c011f

    invoke-virtual {p0, v1}, Lcom/osp/app/signin/SignInView;->findViewById(I)Landroid/view/View;

    move-result-object v1

    const/16 v3, 0x8

    invoke-virtual {v1, v3}, Landroid/view/View;->setVisibility(I)V

    goto/16 :goto_9

    :cond_3a
    if-eqz v1, :cond_30

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    goto/16 :goto_a

    :cond_3b
    const v0, 0x7f0c0125

    invoke-virtual {p0, v0}, Lcom/osp/app/signin/SignInView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/CheckBox;

    goto/16 :goto_b

    :cond_3c
    iget-object v0, p0, Lcom/osp/app/signin/SignInView;->at:Lcom/osp/app/signin/BottomSoftkeyLinearLayout;

    invoke-virtual {p0, v9}, Lcom/osp/app/signin/SignInView;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/osp/app/signin/BottomSoftkeyLinearLayout;->b(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/osp/app/signin/SignInView;->at:Lcom/osp/app/signin/BottomSoftkeyLinearLayout;

    invoke-virtual {p0, v9}, Lcom/osp/app/signin/SignInView;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/osp/app/signin/BottomSoftkeyLinearLayout;->d(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/osp/app/signin/SignInView;->at:Lcom/osp/app/signin/BottomSoftkeyLinearLayout;

    invoke-virtual {p0, v10}, Lcom/osp/app/signin/SignInView;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/osp/app/signin/BottomSoftkeyLinearLayout;->c(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/osp/app/signin/SignInView;->at:Lcom/osp/app/signin/BottomSoftkeyLinearLayout;

    invoke-virtual {p0, v10}, Lcom/osp/app/signin/SignInView;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/osp/app/signin/BottomSoftkeyLinearLayout;->e(Ljava/lang/String;)V

    goto/16 :goto_c

    :cond_3d
    iget-object v0, p0, Lcom/osp/app/signin/SignInView;->au:Lcom/osp/app/signin/BottomSoftkeyLinearLayout;

    invoke-virtual {p0, v9}, Lcom/osp/app/signin/SignInView;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/osp/app/signin/BottomSoftkeyLinearLayout;->b(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/osp/app/signin/SignInView;->au:Lcom/osp/app/signin/BottomSoftkeyLinearLayout;

    invoke-virtual {p0, v9}, Lcom/osp/app/signin/SignInView;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/osp/app/signin/BottomSoftkeyLinearLayout;->d(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/osp/app/signin/SignInView;->au:Lcom/osp/app/signin/BottomSoftkeyLinearLayout;

    invoke-virtual {p0, v10}, Lcom/osp/app/signin/SignInView;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/osp/app/signin/BottomSoftkeyLinearLayout;->c(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/osp/app/signin/SignInView;->au:Lcom/osp/app/signin/BottomSoftkeyLinearLayout;

    invoke-virtual {p0, v10}, Lcom/osp/app/signin/SignInView;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/osp/app/signin/BottomSoftkeyLinearLayout;->e(Ljava/lang/String;)V

    goto/16 :goto_d

    :cond_3e
    iget-object v0, p0, Lcom/osp/app/signin/SignInView;->at:Lcom/osp/app/signin/BottomSoftkeyLinearLayout;

    invoke-virtual {p0, v11}, Lcom/osp/app/signin/SignInView;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/osp/app/signin/BottomSoftkeyLinearLayout;->b(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/osp/app/signin/SignInView;->at:Lcom/osp/app/signin/BottomSoftkeyLinearLayout;

    invoke-virtual {p0, v11}, Lcom/osp/app/signin/SignInView;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/osp/app/signin/BottomSoftkeyLinearLayout;->d(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/osp/app/signin/SignInView;->au:Lcom/osp/app/signin/BottomSoftkeyLinearLayout;

    if-eqz v0, :cond_32

    iget-object v0, p0, Lcom/osp/app/signin/SignInView;->au:Lcom/osp/app/signin/BottomSoftkeyLinearLayout;

    invoke-virtual {p0, v11}, Lcom/osp/app/signin/SignInView;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/osp/app/signin/BottomSoftkeyLinearLayout;->b(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/osp/app/signin/SignInView;->au:Lcom/osp/app/signin/BottomSoftkeyLinearLayout;

    invoke-virtual {p0, v11}, Lcom/osp/app/signin/SignInView;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/osp/app/signin/BottomSoftkeyLinearLayout;->d(Ljava/lang/String;)V

    goto/16 :goto_e

    :cond_3f
    iget-object v0, p0, Lcom/osp/app/signin/SignInView;->at:Lcom/osp/app/signin/BottomSoftkeyLinearLayout;

    iget-object v1, p0, Lcom/osp/app/signin/SignInView;->b:Landroid/view/View$OnClickListener;

    invoke-virtual {v0, v1}, Lcom/osp/app/signin/BottomSoftkeyLinearLayout;->b(Landroid/view/View$OnClickListener;)V

    iget-object v0, p0, Lcom/osp/app/signin/SignInView;->at:Lcom/osp/app/signin/BottomSoftkeyLinearLayout;

    iget-object v1, p0, Lcom/osp/app/signin/SignInView;->c:Landroid/view/View$OnClickListener;

    invoke-virtual {v0, v1}, Lcom/osp/app/signin/BottomSoftkeyLinearLayout;->a(Landroid/view/View$OnClickListener;)V

    goto/16 :goto_f

    :cond_40
    iget-object v0, p0, Lcom/osp/app/signin/SignInView;->au:Lcom/osp/app/signin/BottomSoftkeyLinearLayout;

    iget-object v1, p0, Lcom/osp/app/signin/SignInView;->b:Landroid/view/View$OnClickListener;

    invoke-virtual {v0, v1}, Lcom/osp/app/signin/BottomSoftkeyLinearLayout;->b(Landroid/view/View$OnClickListener;)V

    iget-object v0, p0, Lcom/osp/app/signin/SignInView;->au:Lcom/osp/app/signin/BottomSoftkeyLinearLayout;

    iget-object v1, p0, Lcom/osp/app/signin/SignInView;->c:Landroid/view/View$OnClickListener;

    invoke-virtual {v0, v1}, Lcom/osp/app/signin/BottomSoftkeyLinearLayout;->a(Landroid/view/View$OnClickListener;)V

    goto/16 :goto_10

    .line 649
    :cond_41
    iget-boolean v2, p0, Lcom/osp/app/signin/SignInView;->aa:Z

    if-eqz v2, :cond_42

    iget-boolean v2, p0, Lcom/osp/app/signin/SignInView;->ab:Z

    if-nez v2, :cond_42

    .line 651
    invoke-virtual {v1}, Landroid/widget/EditText;->requestFocus()Z

    goto/16 :goto_11

    .line 654
    :cond_42
    invoke-virtual {v0}, Landroid/widget/EditText;->requestFocus()Z

    goto/16 :goto_11

    .line 725
    :cond_43
    invoke-virtual {p0}, Lcom/osp/app/signin/SignInView;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string v1, "key_request_resign_dialog"

    invoke-virtual {v0, v1, v8}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v0

    if-eqz v0, :cond_37

    .line 727
    invoke-direct {p0}, Lcom/osp/app/signin/SignInView;->f()V

    goto/16 :goto_12

    .line 734
    :cond_44
    invoke-static {}, Lcom/osp/app/signin/SamsungService;->a()Z

    move-result v0

    if-nez v0, :cond_37

    invoke-static {}, Lcom/osp/device/b;->b()Lcom/osp/device/b;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/osp/device/b;->i(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_37

    .line 736
    const-string v0, "signin_cancel"

    invoke-virtual {p0, v0}, Lcom/osp/app/signin/SignInView;->e(Ljava/lang/String;)V

    goto/16 :goto_12
.end method

.method public onCreateDialog(I)Landroid/app/Dialog;
    .locals 6

    .prologue
    const v5, 0x7f090107

    const v4, 0x7f090042

    const v3, 0x7f090019

    .line 989
    const/4 v0, 0x0

    .line 991
    packed-switch p1, :pswitch_data_0

    .line 1018
    :goto_0
    return-object v0

    .line 995
    :pswitch_0
    new-instance v0, Landroid/app/AlertDialog$Builder;

    invoke-direct {v0, p0}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    const v1, 0x7f090089

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    const v1, 0x7f090053

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setMessage(I)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    new-instance v1, Lcom/osp/app/signin/hw;

    invoke-direct {v1, p0}, Lcom/osp/app/signin/hw;-><init>(Lcom/osp/app/signin/SignInView;)V

    invoke-virtual {v0, v4, v1}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v0

    goto :goto_0

    .line 1005
    :pswitch_1
    new-instance v0, Lcom/osp/app/signin/jh;

    invoke-direct {v0, p0}, Lcom/osp/app/signin/jh;-><init>(Lcom/osp/app/signin/SignInView;)V

    new-instance v1, Landroid/app/AlertDialog$Builder;

    invoke-direct {v1, p0}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    const v2, 0x7f09004f

    invoke-virtual {v1, v2}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    const v2, 0x7f09010f

    invoke-virtual {v1, v2}, Landroid/app/AlertDialog$Builder;->setMessage(I)Landroid/app/AlertDialog$Builder;

    invoke-virtual {v1, v3, v0}, Landroid/app/AlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    const v2, 0x7f090064

    invoke-virtual {v1, v2, v0}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    iget-object v0, p0, Lcom/osp/app/signin/SignInView;->d:Landroid/content/DialogInterface$OnCancelListener;

    invoke-virtual {v1, v0}, Landroid/app/AlertDialog$Builder;->setOnCancelListener(Landroid/content/DialogInterface$OnCancelListener;)Landroid/app/AlertDialog$Builder;

    invoke-virtual {v1}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v0

    goto :goto_0

    .line 1009
    :pswitch_2
    new-instance v0, Lcom/osp/app/signin/ji;

    invoke-direct {v0, p0}, Lcom/osp/app/signin/ji;-><init>(Lcom/osp/app/signin/SignInView;)V

    new-instance v1, Landroid/app/AlertDialog$Builder;

    invoke-direct {v1, p0}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    invoke-virtual {v1, v5}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    const v2, 0x7f090108

    invoke-virtual {v1, v2}, Landroid/app/AlertDialog$Builder;->setMessage(I)Landroid/app/AlertDialog$Builder;

    invoke-virtual {v1, v5, v0}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    iget-object v0, p0, Lcom/osp/app/signin/SignInView;->e:Landroid/content/DialogInterface$OnCancelListener;

    invoke-virtual {v1, v0}, Landroid/app/AlertDialog$Builder;->setOnCancelListener(Landroid/content/DialogInterface$OnCancelListener;)Landroid/app/AlertDialog$Builder;

    invoke-virtual {v1}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v0

    goto :goto_0

    .line 1012
    :pswitch_3
    new-instance v0, Lcom/osp/app/signin/hv;

    invoke-direct {v0, p0}, Lcom/osp/app/signin/hv;-><init>(Lcom/osp/app/signin/SignInView;)V

    new-instance v1, Landroid/app/AlertDialog$Builder;

    invoke-direct {v1, p0}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    const v2, 0x7f090116

    invoke-virtual {v1, v2}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    const v2, 0x7f09019a

    invoke-virtual {v1, v2}, Landroid/app/AlertDialog$Builder;->setMessage(I)Landroid/app/AlertDialog$Builder;

    invoke-virtual {v1, v3, v0}, Landroid/app/AlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    invoke-virtual {v1, v4, v0}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    iget-object v0, p0, Lcom/osp/app/signin/SignInView;->f:Landroid/content/DialogInterface$OnCancelListener;

    invoke-virtual {v1, v0}, Landroid/app/AlertDialog$Builder;->setOnCancelListener(Landroid/content/DialogInterface$OnCancelListener;)Landroid/app/AlertDialog$Builder;

    invoke-virtual {v1}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v0

    goto/16 :goto_0

    .line 991
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method protected onDestroy()V
    .locals 3

    .prologue
    .line 1333
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v0, "SIV"

    const-string v1, "onDestroy"

    const-string v2, "START"

    invoke-static {v0, v1, v2}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 1334
    invoke-super {p0}, Lcom/osp/app/util/BaseActivity;->onDestroy()V

    .line 1340
    iget-object v0, p0, Lcom/osp/app/signin/SignInView;->K:Lcom/osp/app/signin/jr;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/osp/app/signin/SignInView;->K:Lcom/osp/app/signin/jr;

    invoke-virtual {v0}, Lcom/osp/app/signin/jr;->getStatus()Landroid/os/AsyncTask$Status;

    move-result-object v0

    sget-object v1, Landroid/os/AsyncTask$Status;->RUNNING:Landroid/os/AsyncTask$Status;

    if-ne v0, v1, :cond_0

    .line 1342
    iget-object v0, p0, Lcom/osp/app/signin/SignInView;->K:Lcom/osp/app/signin/jr;

    if-eqz v0, :cond_0

    .line 1344
    iget-object v0, p0, Lcom/osp/app/signin/SignInView;->K:Lcom/osp/app/signin/jr;

    invoke-virtual {v0}, Lcom/osp/app/signin/jr;->d()V

    .line 1345
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/osp/app/signin/SignInView;->K:Lcom/osp/app/signin/jr;

    .line 1349
    :cond_0
    invoke-virtual {p0}, Lcom/osp/app/signin/SignInView;->isTaskRoot()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1351
    invoke-static {}, Lcom/msc/sa/selfupdate/c;->b()Lcom/msc/sa/selfupdate/c;

    move-result-object v0

    invoke-virtual {v0}, Lcom/msc/sa/selfupdate/c;->d()V

    .line 1354
    :cond_1
    iget-object v0, p0, Lcom/osp/app/signin/SignInView;->a:Landroid/app/AlertDialog;

    if-eqz v0, :cond_2

    .line 1356
    iget-object v0, p0, Lcom/osp/app/signin/SignInView;->a:Landroid/app/AlertDialog;

    invoke-virtual {v0}, Landroid/app/AlertDialog;->dismiss()V

    .line 1359
    :cond_2
    invoke-virtual {p0}, Lcom/osp/app/signin/SignInView;->d()V

    .line 1361
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v0, "SIV"

    const-string v1, "onDestroy"

    const-string v2, "END"

    invoke-static {v0, v1, v2}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 1362
    return-void
.end method

.method public onFocusChange(Landroid/view/View;Z)V
    .locals 3

    .prologue
    .line 1072
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "SignInView::onFocusChange hasFocus : "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;)V

    .line 1074
    invoke-direct {p0}, Lcom/osp/app/signin/SignInView;->e()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 1076
    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v0

    const v1, 0x7f0c0129

    if-ne v0, v1, :cond_2

    if-nez p2, :cond_2

    move-object v0, p1

    .line 1078
    check-cast v0, Landroid/widget/EditText;

    invoke-virtual {v0}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-interface {v0}, Landroid/text/Editable;->length()I

    move-result v0

    if-gtz v0, :cond_1

    .line 1115
    :cond_0
    :goto_0
    return-void

    :cond_1
    move-object v0, p1

    .line 1082
    check-cast v0, Landroid/widget/EditText;

    invoke-virtual {v0}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v1, " "

    const-string v2, ""

    invoke-virtual {v0, v1, v2}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v0

    .line 1083
    check-cast p1, Landroid/widget/EditText;

    invoke-virtual {p1, v0}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    .line 1085
    invoke-direct {p0}, Lcom/osp/app/signin/SignInView;->s()V

    goto :goto_0

    .line 1086
    :cond_2
    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v0

    const v1, 0x7f0c012a

    if-ne v0, v1, :cond_0

    if-nez p2, :cond_0

    .line 1088
    check-cast p1, Landroid/widget/EditText;

    invoke-virtual {p1}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-interface {v0}, Landroid/text/Editable;->length()I

    move-result v0

    if-gtz v0, :cond_0

    goto :goto_0

    .line 1096
    :cond_3
    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v0

    const v1, 0x7f0c0121

    if-ne v0, v1, :cond_4

    if-nez p2, :cond_4

    move-object v0, p1

    .line 1098
    check-cast v0, Landroid/widget/EditText;

    invoke-virtual {v0}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-interface {v0}, Landroid/text/Editable;->length()I

    move-result v0

    if-lez v0, :cond_0

    move-object v0, p1

    .line 1102
    check-cast v0, Landroid/widget/EditText;

    invoke-virtual {v0}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v1, " "

    const-string v2, ""

    invoke-virtual {v0, v1, v2}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v0

    .line 1103
    check-cast p1, Landroid/widget/EditText;

    invoke-virtual {p1, v0}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    .line 1105
    invoke-direct {p0}, Lcom/osp/app/signin/SignInView;->s()V

    goto :goto_0

    .line 1106
    :cond_4
    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v0

    const v1, 0x7f0c0122

    if-ne v0, v1, :cond_0

    if-nez p2, :cond_0

    .line 1108
    check-cast p1, Landroid/widget/EditText;

    invoke-virtual {p1}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-interface {v0}, Landroid/text/Editable;->length()I

    move-result v0

    if-gtz v0, :cond_0

    goto :goto_0
.end method

.method public onOptionsItemSelected(Landroid/view/MenuItem;)Z
    .locals 4

    .prologue
    const/4 v0, 0x0

    .line 1633
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v1, "SIV"

    const-string v2, "onOptionsItemSelected"

    const-string v3, "START"

    invoke-static {v1, v2, v3}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 1635
    invoke-static {}, Lcom/osp/device/b;->b()Lcom/osp/device/b;

    move-result-object v1

    invoke-virtual {v1, p0}, Lcom/osp/device/b;->i(Landroid/content/Context;)Z

    move-result v1

    if-nez v1, :cond_0

    sget v1, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v2, 0xb

    if-ge v1, v2, :cond_0

    .line 1681
    :goto_0
    return v0

    .line 1643
    :cond_0
    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v1

    sparse-switch v1, :sswitch_data_0

    .line 1677
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v1, "SIV"

    const-string v2, "onOptionsItemSelected"

    const-string v3, "END"

    invoke-static {v1, v2, v3}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 1647
    :sswitch_0
    invoke-virtual {p0}, Lcom/osp/app/signin/SignInView;->onBackPressed()V

    .line 1680
    :goto_1
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v0, "SIV"

    const-string v1, "onOptionsItemSelected"

    const-string v2, "END"

    invoke-static {v0, v1, v2}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 1681
    const/4 v0, 0x1

    goto :goto_0

    .line 1651
    :sswitch_1
    invoke-direct {p0}, Lcom/osp/app/signin/SignInView;->g()V

    goto :goto_1

    .line 1655
    :sswitch_2
    iget-boolean v1, p0, Lcom/osp/app/signin/SignInView;->W:Z

    if-eqz v1, :cond_1

    .line 1657
    const/4 v0, 0x7

    iget-object v1, p0, Lcom/osp/app/signin/SignInView;->y:Landroid/content/Intent;

    invoke-virtual {p0, v0, v1}, Lcom/osp/app/signin/SignInView;->a(ILandroid/content/Intent;)V

    .line 1658
    invoke-virtual {p0}, Lcom/osp/app/signin/SignInView;->finish()V

    goto :goto_1

    .line 1662
    :cond_1
    iget-object v1, p0, Lcom/osp/app/signin/SignInView;->y:Landroid/content/Intent;

    if-eqz v1, :cond_2

    .line 1664
    iget-object v1, p0, Lcom/osp/app/signin/SignInView;->y:Landroid/content/Intent;

    const-string v2, "is_cancelable_just_one_activity"

    invoke-virtual {v1, v2, v0}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v1

    if-eqz v1, :cond_3

    .line 1666
    const/16 v0, 0xe

    iget-object v1, p0, Lcom/osp/app/signin/SignInView;->y:Landroid/content/Intent;

    invoke-virtual {p0, v0, v1}, Lcom/osp/app/signin/SignInView;->a(ILandroid/content/Intent;)V

    .line 1673
    :cond_2
    :goto_2
    invoke-virtual {p0}, Lcom/osp/app/signin/SignInView;->finish()V

    goto :goto_1

    .line 1669
    :cond_3
    iget-object v1, p0, Lcom/osp/app/signin/SignInView;->y:Landroid/content/Intent;

    invoke-virtual {p0, v0, v1}, Lcom/osp/app/signin/SignInView;->a(ILandroid/content/Intent;)V

    goto :goto_2

    .line 1643
    :sswitch_data_0
    .sparse-switch
        0x102002c -> :sswitch_0
        0x7f0c018e -> :sswitch_2
        0x7f0c0197 -> :sswitch_1
    .end sparse-switch
.end method

.method protected onPause()V
    .locals 8

    .prologue
    const v7, 0x7f09004a

    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 1232
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v0, "SIV"

    const-string v3, "onPause"

    const-string v4, "START"

    invoke-static {v0, v3, v4}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 1233
    invoke-super {p0}, Lcom/osp/app/util/BaseActivity;->onPause()V

    .line 1235
    const-string v0, "power"

    invoke-virtual {p0, v0}, Lcom/osp/app/signin/SignInView;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/os/PowerManager;

    .line 1236
    invoke-virtual {v0}, Landroid/os/PowerManager;->isScreenOn()Z

    move-result v3

    .line 1238
    iget-boolean v0, p0, Lcom/osp/app/signin/SignInView;->aa:Z

    if-eqz v0, :cond_1

    invoke-virtual {p0}, Lcom/osp/app/signin/SignInView;->isFinishing()Z

    move-result v0

    if-nez v0, :cond_1

    const-string v0, "activity"

    invoke-virtual {p0, v0}, Lcom/osp/app/signin/SignInView;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/ActivityManager;

    const/4 v4, 0x2

    invoke-virtual {v0, v4}, Landroid/app/ActivityManager;->getRunningTasks(I)Ljava/util/List;

    move-result-object v0

    if-eqz v0, :cond_9

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    if-eqz v0, :cond_1

    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_1

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/ActivityManager$RunningTaskInfo;

    iget-object v0, v0, Landroid/app/ActivityManager$RunningTaskInfo;->topActivity:Landroid/content/ComponentName;

    invoke-virtual {v0}, Landroid/content/ComponentName;->getClassName()Ljava/lang/String;

    move-result-object v0

    const-class v4, Lcom/osp/app/signin/SignInView$ReSignInView;

    invoke-virtual {v4}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v4

    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "top activity - "

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;)V

    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "resignin activity - "

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v5, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;)V

    if-eqz v4, :cond_1

    invoke-virtual {v4, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    move v0, v1

    .line 1239
    :goto_1
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v4, "SIV"

    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "isCalledThisActivityAgain - "

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 1241
    if-eqz v3, :cond_7

    if-eqz v0, :cond_0

    invoke-static {}, Lcom/msc/sa/selfupdate/c;->b()Lcom/msc/sa/selfupdate/c;

    move-result-object v0

    invoke-virtual {v0}, Lcom/msc/sa/selfupdate/c;->f()Z

    move-result v0

    if-eqz v0, :cond_7

    .line 1243
    :cond_0
    iget-object v0, p0, Lcom/osp/app/signin/SignInView;->J:Lcom/osp/app/signin/jj;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/osp/app/signin/SignInView;->J:Lcom/osp/app/signin/jj;

    invoke-virtual {v0}, Lcom/osp/app/signin/jj;->getStatus()Landroid/os/AsyncTask$Status;

    move-result-object v0

    sget-object v4, Landroid/os/AsyncTask$Status;->RUNNING:Landroid/os/AsyncTask$Status;

    if-ne v0, v4, :cond_2

    .line 1245
    iget-object v0, p0, Lcom/osp/app/signin/SignInView;->J:Lcom/osp/app/signin/jj;

    invoke-virtual {v0}, Lcom/osp/app/signin/jj;->d()V

    .line 1246
    iput-object v2, p0, Lcom/osp/app/signin/SignInView;->J:Lcom/osp/app/signin/jj;

    .line 1289
    :goto_2
    return-void

    .line 1238
    :cond_1
    const/4 v0, 0x0

    goto :goto_1

    .line 1249
    :cond_2
    iget-object v0, p0, Lcom/osp/app/signin/SignInView;->N:Lcom/osp/app/signin/jl;

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/osp/app/signin/SignInView;->N:Lcom/osp/app/signin/jl;

    invoke-virtual {v0}, Lcom/osp/app/signin/jl;->getStatus()Landroid/os/AsyncTask$Status;

    move-result-object v0

    sget-object v4, Landroid/os/AsyncTask$Status;->RUNNING:Landroid/os/AsyncTask$Status;

    if-ne v0, v4, :cond_3

    .line 1254
    iget-object v0, p0, Lcom/osp/app/signin/SignInView;->N:Lcom/osp/app/signin/jl;

    invoke-virtual {v0}, Lcom/osp/app/signin/jl;->d()V

    .line 1255
    iput-object v2, p0, Lcom/osp/app/signin/SignInView;->N:Lcom/osp/app/signin/jl;

    goto :goto_2

    .line 1258
    :cond_3
    iget-object v0, p0, Lcom/osp/app/signin/SignInView;->O:Lcom/osp/app/signin/jk;

    if-eqz v0, :cond_4

    iget-object v0, p0, Lcom/osp/app/signin/SignInView;->O:Lcom/osp/app/signin/jk;

    invoke-virtual {v0}, Lcom/osp/app/signin/jk;->getStatus()Landroid/os/AsyncTask$Status;

    move-result-object v0

    sget-object v4, Landroid/os/AsyncTask$Status;->RUNNING:Landroid/os/AsyncTask$Status;

    if-ne v0, v4, :cond_4

    .line 1260
    iget-object v0, p0, Lcom/osp/app/signin/SignInView;->O:Lcom/osp/app/signin/jk;

    invoke-virtual {v0}, Lcom/osp/app/signin/jk;->d()V

    .line 1261
    iput-object v2, p0, Lcom/osp/app/signin/SignInView;->O:Lcom/osp/app/signin/jk;

    .line 1263
    :cond_4
    iget-object v0, p0, Lcom/osp/app/signin/SignInView;->K:Lcom/osp/app/signin/jr;

    if-eqz v0, :cond_5

    iget-object v0, p0, Lcom/osp/app/signin/SignInView;->K:Lcom/osp/app/signin/jr;

    invoke-virtual {v0}, Lcom/osp/app/signin/jr;->getStatus()Landroid/os/AsyncTask$Status;

    move-result-object v0

    sget-object v4, Landroid/os/AsyncTask$Status;->RUNNING:Landroid/os/AsyncTask$Status;

    if-ne v0, v4, :cond_5

    .line 1265
    invoke-static {}, Lcom/osp/app/util/ap;->a()Lcom/osp/app/util/ap;

    invoke-virtual {p0, v7}, Lcom/osp/app/signin/SignInView;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-static {p0, v0, v1}, Lcom/osp/app/util/ap;->a(Landroid/content/Context;Ljava/lang/String;I)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    .line 1266
    iget-object v0, p0, Lcom/osp/app/signin/SignInView;->K:Lcom/osp/app/signin/jr;

    invoke-virtual {v0}, Lcom/osp/app/signin/jr;->d()V

    .line 1267
    iput-object v2, p0, Lcom/osp/app/signin/SignInView;->K:Lcom/osp/app/signin/jr;

    .line 1269
    :cond_5
    iget-object v0, p0, Lcom/osp/app/signin/SignInView;->L:Lcom/osp/app/signin/jv;

    if-eqz v0, :cond_6

    iget-object v0, p0, Lcom/osp/app/signin/SignInView;->L:Lcom/osp/app/signin/jv;

    invoke-virtual {v0}, Lcom/osp/app/signin/jv;->getStatus()Landroid/os/AsyncTask$Status;

    move-result-object v0

    sget-object v4, Landroid/os/AsyncTask$Status;->RUNNING:Landroid/os/AsyncTask$Status;

    if-ne v0, v4, :cond_6

    .line 1271
    invoke-static {}, Lcom/osp/app/util/ap;->a()Lcom/osp/app/util/ap;

    invoke-virtual {p0, v7}, Lcom/osp/app/signin/SignInView;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-static {p0, v0, v1}, Lcom/osp/app/util/ap;->a(Landroid/content/Context;Ljava/lang/String;I)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    .line 1272
    iget-object v0, p0, Lcom/osp/app/signin/SignInView;->L:Lcom/osp/app/signin/jv;

    invoke-virtual {v0}, Lcom/osp/app/signin/jv;->d()V

    .line 1273
    iput-object v2, p0, Lcom/osp/app/signin/SignInView;->L:Lcom/osp/app/signin/jv;

    .line 1275
    :cond_6
    iget-object v0, p0, Lcom/osp/app/signin/SignInView;->P:Lcom/osp/app/signin/jy;

    if-eqz v0, :cond_7

    iget-object v0, p0, Lcom/osp/app/signin/SignInView;->P:Lcom/osp/app/signin/jy;

    invoke-virtual {v0}, Lcom/osp/app/signin/jy;->getStatus()Landroid/os/AsyncTask$Status;

    move-result-object v0

    sget-object v4, Landroid/os/AsyncTask$Status;->RUNNING:Landroid/os/AsyncTask$Status;

    if-ne v0, v4, :cond_7

    .line 1277
    invoke-static {}, Lcom/osp/app/util/ap;->a()Lcom/osp/app/util/ap;

    invoke-virtual {p0, v7}, Lcom/osp/app/signin/SignInView;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-static {p0, v0, v1}, Lcom/osp/app/util/ap;->a(Landroid/content/Context;Ljava/lang/String;I)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    .line 1278
    iget-object v0, p0, Lcom/osp/app/signin/SignInView;->P:Lcom/osp/app/signin/jy;

    invoke-virtual {v0}, Lcom/osp/app/signin/jy;->d()V

    .line 1279
    iput-object v2, p0, Lcom/osp/app/signin/SignInView;->P:Lcom/osp/app/signin/jy;

    .line 1283
    :cond_7
    if-eqz v3, :cond_8

    .line 1285
    invoke-virtual {p0}, Lcom/osp/app/signin/SignInView;->d()V

    .line 1288
    :cond_8
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v0, "SIV"

    const-string v1, "onPause"

    const-string v2, "END"

    invoke-static {v0, v1, v2}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_2

    :cond_9
    move-object v0, v2

    goto/16 :goto_0
.end method

.method protected onRestoreInstanceState(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 2658
    invoke-super {p0, p1}, Lcom/osp/app/util/BaseActivity;->onRestoreInstanceState(Landroid/os/Bundle;)V

    .line 2660
    invoke-direct {p0}, Lcom/osp/app/signin/SignInView;->e()Z

    move-result v0

    if-eqz v0, :cond_4

    .line 2665
    iget-object v0, p0, Lcom/osp/app/signin/SignInView;->S:Landroid/view/View;

    const v1, 0x7f0c00b7

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/CheckBox;

    .line 2670
    :goto_0
    if-eqz v0, :cond_0

    .line 2672
    const v1, 0x7f09012a

    invoke-virtual {v0, v1}, Landroid/widget/CheckBox;->setText(I)V

    .line 2675
    :cond_0
    if-eqz p1, :cond_3

    .line 2677
    const-string v0, "FIELD_INFO"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getBundle(Ljava/lang/String;)Landroid/os/Bundle;

    move-result-object v0

    .line 2678
    if-eqz v0, :cond_1

    .line 2680
    invoke-static {v0}, Lcom/osp/app/signin/kd;->a(Landroid/os/Bundle;)Lcom/osp/app/signin/kd;

    move-result-object v0

    iput-object v0, p0, Lcom/osp/app/signin/SignInView;->U:Lcom/osp/app/signin/kd;

    .line 2683
    :cond_1
    const-string v0, "RESULT_CHECK_LIST"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getBundle(Ljava/lang/String;)Landroid/os/Bundle;

    move-result-object v0

    .line 2684
    if-eqz v0, :cond_2

    .line 2686
    invoke-static {v0}, Lcom/msc/a/g;->a(Landroid/os/Bundle;)Lcom/msc/a/g;

    move-result-object v0

    iput-object v0, p0, Lcom/osp/app/signin/SignInView;->V:Lcom/msc/a/g;

    .line 2689
    :cond_2
    const-string v0, "AUTH_CODE"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/osp/app/signin/SignInView;->R:Ljava/lang/String;

    .line 2690
    const-string v0, "USER_ID"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/osp/app/signin/SignInView;->ad:Ljava/lang/String;

    .line 2695
    :cond_3
    return-void

    .line 2668
    :cond_4
    const v0, 0x7f0c0125

    invoke-virtual {p0, v0}, Lcom/osp/app/signin/SignInView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/CheckBox;

    goto :goto_0
.end method

.method protected onResume()V
    .locals 3

    .prologue
    .line 1195
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v0, "SIV"

    const-string v1, "onResume"

    const-string v2, "START"

    invoke-static {v0, v1, v2}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 1199
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/osp/app/signin/SignInView;->ao:J

    .line 1201
    invoke-super {p0}, Lcom/osp/app/util/BaseActivity;->onResume()V

    .line 1207
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v0, "SIV"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "is resign in? "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-boolean v2, p0, Lcom/osp/app/signin/SignInView;->aa:Z

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 1209
    invoke-static {p0}, Lcom/osp/app/util/ad;->d(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/osp/app/signin/SignInView;->aa:Z

    if-nez v0, :cond_0

    iget-boolean v0, p0, Lcom/osp/app/signin/SignInView;->ag:Z

    if-nez v0, :cond_0

    iget-boolean v0, p0, Lcom/osp/app/signin/SignInView;->ac:Z

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/osp/app/signin/SignInView;->ar:Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-nez v0, :cond_0

    .line 1222
    invoke-virtual {p0}, Lcom/osp/app/signin/SignInView;->finish()V

    .line 1225
    :cond_0
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/osp/app/signin/SignInView;->an:Z

    .line 1227
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v0, "SIV"

    const-string v1, "onResume"

    const-string v2, "END"

    invoke-static {v0, v1, v2}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 1228
    return-void
.end method

.method protected onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 2633
    if-eqz p1, :cond_2

    .line 2636
    iget-object v0, p0, Lcom/osp/app/signin/SignInView;->U:Lcom/osp/app/signin/kd;

    if-eqz v0, :cond_0

    .line 2638
    const-string v0, "FIELD_INFO"

    iget-object v1, p0, Lcom/osp/app/signin/SignInView;->U:Lcom/osp/app/signin/kd;

    invoke-virtual {v1}, Lcom/osp/app/signin/kd;->X()Landroid/os/Bundle;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putBundle(Ljava/lang/String;Landroid/os/Bundle;)V

    .line 2641
    :cond_0
    iget-object v0, p0, Lcom/osp/app/signin/SignInView;->V:Lcom/msc/a/g;

    if-eqz v0, :cond_1

    .line 2643
    const-string v0, "RESULT_CHECK_LIST"

    iget-object v1, p0, Lcom/osp/app/signin/SignInView;->V:Lcom/msc/a/g;

    invoke-virtual {v1}, Lcom/msc/a/g;->l()Landroid/os/Bundle;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putBundle(Ljava/lang/String;Landroid/os/Bundle;)V

    .line 2646
    :cond_1
    const-string v0, "AUTH_CODE"

    iget-object v1, p0, Lcom/osp/app/signin/SignInView;->R:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 2647
    const-string v0, "USER_ID"

    iget-object v1, p0, Lcom/osp/app/signin/SignInView;->ad:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 2652
    invoke-super {p0, p1}, Lcom/osp/app/util/BaseActivity;->onSaveInstanceState(Landroid/os/Bundle;)V

    .line 2654
    :cond_2
    return-void
.end method
