.class final Lcom/osp/app/signin/jv;
.super Lcom/msc/c/b;
.source "SignInView.java"


# instance fields
.field protected c:Ljava/lang/Runnable;

.field protected d:Ljava/lang/Runnable;

.field protected e:Ljava/lang/Thread;

.field protected f:Ljava/lang/Thread;

.field final synthetic g:Lcom/osp/app/signin/SignInView;

.field private h:Z

.field private i:Z

.field private j:Z

.field private k:Z

.field private l:Z

.field private m:Ljava/lang/String;

.field private n:Lcom/msc/a/b;

.field private o:J

.field private p:J

.field private q:J

.field private r:J

.field private s:Z

.field private t:J

.field private u:J

.field private v:Z


# direct methods
.method public constructor <init>(Lcom/osp/app/signin/SignInView;Landroid/content/Context;)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 5202
    iput-object p1, p0, Lcom/osp/app/signin/jv;->g:Lcom/osp/app/signin/SignInView;

    .line 5203
    const v0, 0x7f090055

    invoke-direct {p0, p2, v0}, Lcom/msc/c/b;-><init>(Landroid/content/Context;I)V

    .line 5125
    iput-boolean v1, p0, Lcom/osp/app/signin/jv;->h:Z

    .line 5130
    iput-boolean v1, p0, Lcom/osp/app/signin/jv;->i:Z

    .line 5135
    iput-boolean v1, p0, Lcom/osp/app/signin/jv;->j:Z

    .line 5140
    iput-boolean v1, p0, Lcom/osp/app/signin/jv;->k:Z

    .line 5145
    iput-boolean v1, p0, Lcom/osp/app/signin/jv;->l:Z

    .line 5195
    iput-boolean v1, p0, Lcom/osp/app/signin/jv;->v:Z

    .line 5205
    invoke-static {p1}, Lcom/osp/app/signin/SignInView;->l(Lcom/osp/app/signin/SignInView;)Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 5207
    invoke-static {p1}, Lcom/osp/app/signin/SignInView;->l(Lcom/osp/app/signin/SignInView;)Ljava/lang/String;

    move-result-object v0

    const-string v1, "ACCOUNT_VERIFY"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-static {p2}, Lcom/osp/app/util/ad;->d(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 5209
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/osp/app/signin/jv;->j:Z

    .line 5212
    :cond_0
    new-instance v0, Lcom/osp/app/signin/jw;

    invoke-direct {v0, p0, p1, p2}, Lcom/osp/app/signin/jw;-><init>(Lcom/osp/app/signin/jv;Lcom/osp/app/signin/SignInView;Landroid/content/Context;)V

    iput-object v0, p0, Lcom/osp/app/signin/jv;->c:Ljava/lang/Runnable;

    .line 5238
    new-instance v0, Lcom/osp/app/signin/jx;

    invoke-direct {v0, p0, p1}, Lcom/osp/app/signin/jx;-><init>(Lcom/osp/app/signin/jv;Lcom/osp/app/signin/SignInView;)V

    iput-object v0, p0, Lcom/osp/app/signin/jv;->d:Ljava/lang/Runnable;

    .line 5289
    new-instance v0, Ljava/lang/Thread;

    iget-object v1, p0, Lcom/osp/app/signin/jv;->c:Ljava/lang/Runnable;

    invoke-direct {v0, v1}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;)V

    iput-object v0, p0, Lcom/osp/app/signin/jv;->e:Ljava/lang/Thread;

    .line 5290
    new-instance v0, Ljava/lang/Thread;

    iget-object v1, p0, Lcom/osp/app/signin/jv;->d:Ljava/lang/Runnable;

    invoke-direct {v0, v1}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;)V

    iput-object v0, p0, Lcom/osp/app/signin/jv;->f:Ljava/lang/Thread;

    .line 5294
    return-void
.end method

.method private a(Lcom/msc/a/a;)V
    .locals 3

    .prologue
    .line 5854
    invoke-static {}, Lcom/msc/c/g;->a()Lcom/msc/c/g;

    .line 5855
    iget-object v0, p0, Lcom/osp/app/signin/jv;->g:Lcom/osp/app/signin/SignInView;

    iget-object v1, p0, Lcom/osp/app/signin/jv;->g:Lcom/osp/app/signin/SignInView;

    invoke-static {v1}, Lcom/osp/app/signin/SignInView;->y(Lcom/osp/app/signin/SignInView;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, p1, v1, p0}, Lcom/msc/c/g;->a(Landroid/content/Context;Lcom/msc/a/a;Ljava/lang/String;Lcom/msc/b/h;)J

    move-result-wide v0

    iput-wide v0, p0, Lcom/osp/app/signin/jv;->o:J

    .line 5857
    iget-wide v0, p0, Lcom/osp/app/signin/jv;->o:J

    invoke-virtual {p0, v0, v1}, Lcom/osp/app/signin/jv;->a(J)V

    .line 5858
    iget-wide v0, p0, Lcom/osp/app/signin/jv;->o:J

    const-string v2, "from_xml"

    invoke-virtual {p0, v0, v1, v2}, Lcom/osp/app/signin/jv;->a(JLjava/lang/String;)V

    .line 5860
    iget-wide v0, p0, Lcom/osp/app/signin/jv;->o:J

    sget-object v2, Lcom/msc/b/g;->c:Lcom/msc/b/g;

    invoke-static {v0, v1, v2}, Lcom/msc/c/g;->a(JLcom/msc/b/g;)V

    .line 5861
    return-void
.end method

.method static synthetic a(Lcom/osp/app/signin/jv;Lcom/msc/a/a;)V
    .locals 0

    .prologue
    .line 5100
    invoke-direct {p0, p1}, Lcom/osp/app/signin/jv;->a(Lcom/msc/a/a;)V

    return-void
.end method

.method static synthetic a(Lcom/osp/app/signin/jv;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 8

    .prologue
    const/4 v5, 0x0

    .line 5100
    invoke-static {}, Lcom/osp/device/b;->b()Lcom/osp/device/b;

    move-result-object v0

    invoke-virtual {v0}, Lcom/osp/device/b;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-static {p3}, Landroid/telephony/PhoneNumberUtils;->isGlobalPhoneNumber(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    new-instance v6, Lcom/osp/app/signin/cn;

    invoke-direct {v6}, Lcom/osp/app/signin/cn;-><init>()V

    invoke-virtual {v6}, Lcom/osp/app/signin/cn;->a()V

    :goto_0
    invoke-static {}, Lcom/msc/c/g;->a()Lcom/msc/c/g;

    iget-object v0, p0, Lcom/osp/app/signin/jv;->g:Lcom/osp/app/signin/SignInView;

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    move-object v7, p0

    invoke-static/range {v0 .. v7}, Lcom/msc/c/g;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/osp/app/signin/cn;Lcom/msc/b/h;)J

    move-result-wide v0

    iput-wide v0, p0, Lcom/osp/app/signin/jv;->q:J

    iget-wide v0, p0, Lcom/osp/app/signin/jv;->q:J

    invoke-virtual {p0, v0, v1}, Lcom/osp/app/signin/jv;->b(J)V

    iget-wide v0, p0, Lcom/osp/app/signin/jv;->q:J

    const-string v2, "from_json"

    invoke-virtual {p0, v0, v1, v2}, Lcom/osp/app/signin/jv;->a(JLjava/lang/String;)V

    iget-wide v0, p0, Lcom/osp/app/signin/jv;->q:J

    sget-object v2, Lcom/msc/b/g;->c:Lcom/msc/b/g;

    invoke-static {v0, v1, v2}, Lcom/msc/c/g;->a(JLcom/msc/b/g;)V

    return-void

    :cond_0
    move-object v6, v5

    goto :goto_0
.end method

.method private static a(Ljava/lang/Thread;)V
    .locals 2

    .prologue
    .line 5313
    if-eqz p0, :cond_0

    invoke-virtual {p0}, Ljava/lang/Thread;->isAlive()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 5317
    const-wide/16 v0, 0x12c

    :try_start_0
    invoke-virtual {p0, v0, v1}, Ljava/lang/Thread;->join(J)V
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0

    .line 5322
    :cond_0
    :goto_0
    return-void

    .line 5318
    :catch_0
    move-exception v0

    invoke-virtual {v0}, Ljava/lang/InterruptedException;->printStackTrace()V

    goto :goto_0
.end method

.method static synthetic a(Lcom/osp/app/signin/jv;)Z
    .locals 1

    .prologue
    .line 5100
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/osp/app/signin/jv;->h:Z

    return v0
.end method

.method static synthetic b(Lcom/osp/app/signin/jv;)Z
    .locals 1

    .prologue
    .line 5100
    iget-boolean v0, p0, Lcom/osp/app/signin/jv;->j:Z

    return v0
.end method

.method private c(Ljava/lang/String;)V
    .locals 13

    .prologue
    const/16 v12, 0x9

    const/4 v11, 0x4

    const/4 v9, 0x1

    const/4 v10, 0x0

    .line 5967
    iget-object v0, p0, Lcom/osp/app/signin/jv;->g:Lcom/osp/app/signin/SignInView;

    invoke-static {v0}, Lcom/osp/app/signin/SignInView;->e(Lcom/osp/app/signin/SignInView;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 5969
    iget-object v0, p0, Lcom/osp/app/signin/jv;->g:Lcom/osp/app/signin/SignInView;

    invoke-virtual {v0}, Lcom/osp/app/signin/SignInView;->d()V

    .line 5971
    :cond_0
    const-string v0, "Success"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_12

    .line 5975
    iget-object v0, p0, Lcom/osp/app/signin/jv;->g:Lcom/osp/app/signin/SignInView;

    invoke-static {v0}, Lcom/osp/app/signin/SignInView;->e(Lcom/osp/app/signin/SignInView;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 5978
    iget-object v0, p0, Lcom/osp/app/signin/jv;->g:Lcom/osp/app/signin/SignInView;

    invoke-static {v0}, Lcom/osp/app/signin/SignInView;->s(Lcom/osp/app/signin/SignInView;)Landroid/view/View;

    move-result-object v0

    const v1, 0x7f0c0129

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/EditText;

    move-object v8, v0

    .line 5985
    :goto_0
    iget-object v0, p0, Lcom/osp/app/signin/jv;->g:Lcom/osp/app/signin/SignInView;

    invoke-static {v0}, Landroid/preference/PreferenceManager;->getDefaultSharedPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    .line 5986
    const-string v1, "FLAG_DO_NOT_SHOW_NOTIFICATION"

    invoke-interface {v0, v1, v10}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    .line 5987
    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 5989
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v0, "===========================================SignIn_End PROCESSING_SUCCESS==========================================="

    invoke-static {v0}, Lcom/osp/app/util/aq;->c(Ljava/lang/String;)V

    .line 5990
    iget-object v0, p0, Lcom/osp/app/signin/jv;->g:Lcom/osp/app/signin/SignInView;

    invoke-static {v0}, Lcom/osp/app/util/ad;->d(Landroid/content/Context;)Z

    move-result v0

    if-nez v0, :cond_2

    .line 5992
    iget-object v0, p0, Lcom/osp/app/signin/jv;->g:Lcom/osp/app/signin/SignInView;

    invoke-virtual {v8}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    sget-object v2, Ljava/util/Locale;->ENGLISH:Ljava/util/Locale;

    invoke-virtual {v1, v2}, Ljava/lang/String;->toLowerCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/osp/app/util/ad;->a(Landroid/content/Context;Ljava/lang/String;)Z

    move-result v0

    .line 6011
    :goto_1
    if-nez v0, :cond_3

    .line 6013
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v0, "SIV"

    const-string v1, "SigninView SignInNewDualEnd"

    invoke-static {v0, v1}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 6014
    new-instance v0, Lcom/msc/c/f;

    const-string v1, "ERR_MDM_SECURITY"

    const-string v2, "fail_MDM_Security"

    invoke-direct {v0, v1, v2}, Lcom/msc/c/f;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    iput-object v0, p0, Lcom/osp/app/signin/jv;->b:Lcom/msc/c/f;

    .line 6015
    iget-object v0, p0, Lcom/osp/app/signin/jv;->g:Lcom/osp/app/signin/SignInView;

    const/16 v1, 0x15

    invoke-virtual {v0, v1}, Lcom/osp/app/signin/SignInView;->b(I)V

    .line 6016
    const-string v0, "Fail"

    invoke-direct {p0, v0}, Lcom/osp/app/signin/jv;->c(Ljava/lang/String;)V

    .line 6017
    iget-object v0, p0, Lcom/osp/app/signin/jv;->g:Lcom/osp/app/signin/SignInView;

    invoke-virtual {v0}, Lcom/osp/app/signin/SignInView;->finish()V

    .line 6240
    :goto_2
    return-void

    .line 5981
    :cond_1
    iget-object v0, p0, Lcom/osp/app/signin/jv;->g:Lcom/osp/app/signin/SignInView;

    const v1, 0x7f0c0121

    invoke-virtual {v0, v1}, Lcom/osp/app/signin/SignInView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/EditText;

    move-object v8, v0

    goto :goto_0

    .line 5995
    :cond_2
    iget-object v0, p0, Lcom/osp/app/signin/jv;->g:Lcom/osp/app/signin/SignInView;

    invoke-static {v0}, Lcom/osp/app/signin/SignInView;->U(Lcom/osp/app/signin/SignInView;)Z

    move-result v0

    if-eqz v0, :cond_18

    .line 5997
    iget-object v0, p0, Lcom/osp/app/signin/jv;->g:Lcom/osp/app/signin/SignInView;

    invoke-virtual {v8}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    sget-object v2, Ljava/util/Locale;->ENGLISH:Ljava/util/Locale;

    invoke-virtual {v1, v2}, Ljava/lang/String;->toLowerCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/osp/app/util/ad;->b(Landroid/content/Context;Ljava/lang/String;)Z

    move-result v0

    .line 6001
    :goto_3
    iget-object v1, p0, Lcom/osp/app/signin/jv;->g:Lcom/osp/app/signin/SignInView;

    invoke-static {v1}, Lcom/osp/app/util/ad;->j(Landroid/content/Context;)V

    goto :goto_1

    .line 6028
    :cond_3
    :try_start_0
    iget-object v0, p0, Lcom/osp/app/signin/jv;->g:Lcom/osp/app/signin/SignInView;

    sget-object v1, Lcom/msc/c/d;->c:Lcom/msc/c/d;

    invoke-virtual {v1}, Lcom/msc/c/d;->toString()Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/osp/app/signin/jv;->n:Lcom/msc/a/b;

    invoke-virtual {v2}, Lcom/msc/a/b;->a()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v1, v2}, Lcom/msc/c/c;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)V

    .line 6029
    iget-object v0, p0, Lcom/osp/app/signin/jv;->g:Lcom/osp/app/signin/SignInView;

    sget-object v1, Lcom/msc/c/d;->d:Lcom/msc/c/d;

    invoke-virtual {v1}, Lcom/msc/c/d;->toString()Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/osp/app/signin/jv;->n:Lcom/msc/a/b;

    invoke-virtual {v2}, Lcom/msc/a/b;->b()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v1, v2}, Lcom/msc/c/c;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)V

    .line 6030
    iget-object v0, p0, Lcom/osp/app/signin/jv;->g:Lcom/osp/app/signin/SignInView;

    sget-object v1, Lcom/msc/c/d;->a:Lcom/msc/c/d;

    invoke-virtual {v1}, Lcom/msc/c/d;->toString()Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/osp/app/signin/jv;->n:Lcom/msc/a/b;

    invoke-virtual {v2}, Lcom/msc/a/b;->c()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v1, v2}, Lcom/msc/c/c;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)V

    .line 6031
    iget-object v0, p0, Lcom/osp/app/signin/jv;->g:Lcom/osp/app/signin/SignInView;

    sget-object v1, Lcom/msc/c/d;->f:Lcom/msc/c/d;

    invoke-virtual {v1}, Lcom/msc/c/d;->toString()Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/osp/app/signin/jv;->n:Lcom/msc/a/b;

    invoke-virtual {v2}, Lcom/msc/a/b;->d()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v1, v2}, Lcom/msc/c/c;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)V

    .line 6032
    iget-object v0, p0, Lcom/osp/app/signin/jv;->g:Lcom/osp/app/signin/SignInView;

    sget-object v1, Lcom/msc/c/d;->b:Lcom/msc/c/d;

    invoke-virtual {v1}, Lcom/msc/c/d;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v8}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v2

    sget-object v3, Ljava/util/Locale;->ENGLISH:Ljava/util/Locale;

    invoke-virtual {v2, v3}, Ljava/lang/String;->toLowerCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v1, v2}, Lcom/msc/c/c;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)V

    .line 6035
    iget-object v0, p0, Lcom/osp/app/signin/jv;->g:Lcom/osp/app/signin/SignInView;

    sget-object v1, Lcom/msc/c/d;->h:Lcom/msc/c/d;

    invoke-virtual {v1}, Lcom/msc/c/d;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {}, Lcom/osp/common/util/i;->a()Lcom/osp/common/util/i;

    iget-object v2, p0, Lcom/osp/app/signin/jv;->g:Lcom/osp/app/signin/SignInView;

    invoke-static {v2}, Lcom/osp/common/util/i;->f(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v1, v2}, Lcom/msc/c/c;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)V

    .line 6037
    iget-object v0, p0, Lcom/osp/app/signin/jv;->g:Lcom/osp/app/signin/SignInView;

    sget-object v1, Lcom/msc/c/d;->i:Lcom/msc/c/d;

    invoke-virtual {v1}, Lcom/msc/c/d;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {}, Lcom/osp/common/util/h;->a()Lcom/osp/common/util/h;

    iget-object v2, p0, Lcom/osp/app/signin/jv;->g:Lcom/osp/app/signin/SignInView;

    invoke-static {v2}, Lcom/osp/common/util/h;->a(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v1, v2}, Lcom/msc/c/c;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)V

    .line 6038
    iget-object v0, p0, Lcom/osp/app/signin/jv;->g:Lcom/osp/app/signin/SignInView;

    sget-object v1, Lcom/msc/c/d;->g:Lcom/msc/c/d;

    invoke-virtual {v1}, Lcom/msc/c/d;->toString()Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/osp/app/signin/jv;->n:Lcom/msc/a/b;

    invoke-virtual {v2}, Lcom/msc/a/b;->g()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v1, v2}, Lcom/msc/c/c;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)V

    .line 6039
    iget-object v0, p0, Lcom/osp/app/signin/jv;->g:Lcom/osp/app/signin/SignInView;

    const-string v1, "j5p7ll8g33"

    invoke-static {v0, v1}, Lcom/msc/c/c;->a(Landroid/content/Context;Ljava/lang/String;)V

    .line 6040
    iget-object v0, p0, Lcom/osp/app/signin/jv;->g:Lcom/osp/app/signin/SignInView;

    const-string v1, "j5p7ll8g33"

    const-string v2, "5763D0052DC1462E13751F753384E9A9"

    iget-object v3, p0, Lcom/osp/app/signin/jv;->n:Lcom/msc/a/b;

    invoke-virtual {v3}, Lcom/msc/a/b;->e()Ljava/lang/String;

    move-result-object v3

    iget-object v4, p0, Lcom/osp/app/signin/jv;->n:Lcom/msc/a/b;

    invoke-virtual {v4}, Lcom/msc/a/b;->f()Ljava/lang/String;

    move-result-object v4

    invoke-static {v0, v1, v2, v3, v4}, Lcom/msc/c/c;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 6047
    :goto_4
    iget-object v0, p0, Lcom/osp/app/signin/jv;->g:Lcom/osp/app/signin/SignInView;

    iget-object v1, p0, Lcom/osp/app/signin/jv;->g:Lcom/osp/app/signin/SignInView;

    invoke-static {v1}, Lcom/osp/app/signin/SignInView;->x(Lcom/osp/app/signin/SignInView;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/msc/c/e;->e(Landroid/content/Context;Ljava/lang/String;)V

    .line 6048
    iget-object v0, p0, Lcom/osp/app/signin/jv;->g:Lcom/osp/app/signin/SignInView;

    iget-object v1, p0, Lcom/osp/app/signin/jv;->g:Lcom/osp/app/signin/SignInView;

    invoke-static {v1}, Lcom/osp/app/signin/SignInView;->y(Lcom/osp/app/signin/SignInView;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v8}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v2

    sget-object v3, Ljava/util/Locale;->ENGLISH:Ljava/util/Locale;

    invoke-virtual {v2, v3}, Ljava/lang/String;->toLowerCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v2

    iget-object v3, p0, Lcom/osp/app/signin/jv;->n:Lcom/msc/a/b;

    invoke-virtual {v3}, Lcom/msc/a/b;->d()Ljava/lang/String;

    move-result-object v3

    invoke-static {v0, v1, v2, v3}, Lcom/msc/c/e;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 6058
    iget-object v0, p0, Lcom/osp/app/signin/jv;->g:Lcom/osp/app/signin/SignInView;

    invoke-static {v0}, Lcom/osp/app/signin/SignInView;->j(Lcom/osp/app/signin/SignInView;)Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_4

    const-string v0, "j5p7ll8g33"

    iget-object v1, p0, Lcom/osp/app/signin/jv;->g:Lcom/osp/app/signin/SignInView;

    invoke-static {v1}, Lcom/osp/app/signin/SignInView;->j(Lcom/osp/app/signin/SignInView;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_c

    :cond_4
    iget-object v0, p0, Lcom/osp/app/signin/jv;->g:Lcom/osp/app/signin/SignInView;

    invoke-static {v0}, Lcom/osp/app/signin/SignInView;->Q(Lcom/osp/app/signin/SignInView;)Lcom/msc/a/d;

    move-result-object v0

    invoke-virtual {v0}, Lcom/msc/a/d;->g()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_c

    iget-object v0, p0, Lcom/osp/app/signin/jv;->g:Lcom/osp/app/signin/SignInView;

    invoke-static {v0}, Lcom/osp/app/signin/SignInView;->Q(Lcom/osp/app/signin/SignInView;)Lcom/msc/a/d;

    move-result-object v0

    invoke-virtual {v0}, Lcom/msc/a/d;->g()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_c

    .line 6062
    iget-object v0, p0, Lcom/osp/app/signin/jv;->g:Lcom/osp/app/signin/SignInView;

    iget-object v1, p0, Lcom/osp/app/signin/jv;->g:Lcom/osp/app/signin/SignInView;

    invoke-static {v1}, Lcom/osp/app/signin/SignInView;->Q(Lcom/osp/app/signin/SignInView;)Lcom/msc/a/d;

    move-result-object v1

    invoke-virtual {v1}, Lcom/msc/a/d;->g()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/msc/c/e;->b(Landroid/content/Context;Ljava/lang/String;)V

    .line 6070
    :cond_5
    :goto_5
    iget-object v0, p0, Lcom/osp/app/signin/jv;->g:Lcom/osp/app/signin/SignInView;

    invoke-static {v0}, Lcom/osp/app/signin/SignInView;->C(Lcom/osp/app/signin/SignInView;)Z

    move-result v0

    if-ne v0, v9, :cond_6

    .line 6073
    iget-object v0, p0, Lcom/osp/app/signin/jv;->g:Lcom/osp/app/signin/SignInView;

    invoke-static {v0}, Lcom/osp/app/util/ad;->u(Landroid/content/Context;)V

    .line 6077
    :cond_6
    iget-object v0, p0, Lcom/osp/app/signin/jv;->g:Lcom/osp/app/signin/SignInView;

    invoke-static {v0}, Lcom/osp/app/signin/SignInView;->V(Lcom/osp/app/signin/SignInView;)V

    .line 6079
    iget-object v0, p0, Lcom/osp/app/signin/jv;->g:Lcom/osp/app/signin/SignInView;

    invoke-static {v0}, Lcom/osp/app/signin/SignInView;->K(Lcom/osp/app/signin/SignInView;)Lcom/msc/a/g;

    move-result-object v0

    invoke-virtual {v0}, Lcom/msc/a/g;->h()Z

    move-result v0

    if-eqz v0, :cond_f

    .line 6082
    iget-object v0, p0, Lcom/osp/app/signin/jv;->g:Lcom/osp/app/signin/SignInView;

    invoke-static {v0}, Lcom/osp/app/util/r;->v(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_d

    .line 6084
    iget-object v0, p0, Lcom/osp/app/signin/jv;->g:Lcom/osp/app/signin/SignInView;

    invoke-static {v0}, Lcom/osp/app/signin/SignInView;->W(Lcom/osp/app/signin/SignInView;)Lcom/osp/app/signin/jy;

    move-result-object v0

    if-eqz v0, :cond_7

    iget-object v0, p0, Lcom/osp/app/signin/jv;->g:Lcom/osp/app/signin/SignInView;

    invoke-static {v0}, Lcom/osp/app/signin/SignInView;->W(Lcom/osp/app/signin/SignInView;)Lcom/osp/app/signin/jy;

    move-result-object v0

    invoke-virtual {v0}, Lcom/osp/app/signin/jy;->getStatus()Landroid/os/AsyncTask$Status;

    move-result-object v0

    sget-object v1, Landroid/os/AsyncTask$Status;->RUNNING:Landroid/os/AsyncTask$Status;

    if-ne v0, v1, :cond_7

    .line 6086
    iget-object v0, p0, Lcom/osp/app/signin/jv;->g:Lcom/osp/app/signin/SignInView;

    invoke-static {v0}, Lcom/osp/app/signin/SignInView;->W(Lcom/osp/app/signin/SignInView;)Lcom/osp/app/signin/jy;

    move-result-object v0

    invoke-virtual {v0}, Lcom/osp/app/signin/jy;->d()V

    .line 6087
    iget-object v0, p0, Lcom/osp/app/signin/jv;->g:Lcom/osp/app/signin/SignInView;

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lcom/osp/app/signin/SignInView;->a(Lcom/osp/app/signin/SignInView;Lcom/osp/app/signin/jy;)Lcom/osp/app/signin/jy;

    .line 6089
    :cond_7
    iget-object v0, p0, Lcom/osp/app/signin/jv;->g:Lcom/osp/app/signin/SignInView;

    new-instance v1, Lcom/osp/app/signin/jy;

    iget-object v2, p0, Lcom/osp/app/signin/jv;->g:Lcom/osp/app/signin/SignInView;

    iget-object v3, p0, Lcom/osp/app/signin/jv;->g:Lcom/osp/app/signin/SignInView;

    invoke-direct {v1, v2, v3}, Lcom/osp/app/signin/jy;-><init>(Lcom/osp/app/signin/SignInView;Landroid/content/Context;)V

    invoke-static {v0, v1}, Lcom/osp/app/signin/SignInView;->a(Lcom/osp/app/signin/SignInView;Lcom/osp/app/signin/jy;)Lcom/osp/app/signin/jy;

    .line 6090
    iget-object v0, p0, Lcom/osp/app/signin/jv;->g:Lcom/osp/app/signin/SignInView;

    invoke-static {v0}, Lcom/osp/app/signin/SignInView;->W(Lcom/osp/app/signin/SignInView;)Lcom/osp/app/signin/jy;

    move-result-object v0

    invoke-static {v0}, Lcom/msc/sa/c/d;->a(Landroid/os/AsyncTask;)V

    .line 6120
    :goto_6
    invoke-static {}, Lcom/osp/app/signin/SamsungService;->p()Z

    move-result v0

    if-eqz v0, :cond_a

    .line 6122
    iget-object v1, p0, Lcom/osp/app/signin/jv;->g:Lcom/osp/app/signin/SignInView;

    iget-object v0, p0, Lcom/osp/app/signin/jv;->g:Lcom/osp/app/signin/SignInView;

    invoke-static {v0}, Lcom/osp/app/signin/SignInView;->K(Lcom/osp/app/signin/SignInView;)Lcom/msc/a/g;

    move-result-object v0

    invoke-virtual {v0}, Lcom/msc/a/g;->d()Z

    move-result v0

    if-nez v0, :cond_8

    iget-object v0, p0, Lcom/osp/app/signin/jv;->g:Lcom/osp/app/signin/SignInView;

    invoke-static {v0}, Lcom/osp/app/signin/SignInView;->K(Lcom/osp/app/signin/SignInView;)Lcom/msc/a/g;

    move-result-object v0

    invoke-virtual {v0}, Lcom/msc/a/g;->e()Z

    move-result v0

    if-eqz v0, :cond_11

    :cond_8
    move v0, v9

    :goto_7
    iget-object v2, p0, Lcom/osp/app/signin/jv;->g:Lcom/osp/app/signin/SignInView;

    invoke-static {v2}, Lcom/osp/app/signin/SignInView;->K(Lcom/osp/app/signin/SignInView;)Lcom/msc/a/g;

    move-result-object v2

    invoke-virtual {v2}, Lcom/msc/a/g;->g()Z

    move-result v2

    iget-object v3, p0, Lcom/osp/app/signin/jv;->g:Lcom/osp/app/signin/SignInView;

    invoke-static {v3}, Lcom/osp/app/signin/SignInView;->K(Lcom/osp/app/signin/SignInView;)Lcom/msc/a/g;

    move-result-object v3

    invoke-virtual {v3}, Lcom/msc/a/g;->h()Z

    move-result v3

    invoke-static {v1, v0, v2, v3}, Lcom/msc/openprovider/b;->a(Landroid/content/Context;ZZZ)V

    .line 6124
    iget-object v0, p0, Lcom/osp/app/signin/jv;->g:Lcom/osp/app/signin/SignInView;

    iget-object v1, p0, Lcom/osp/app/signin/jv;->g:Lcom/osp/app/signin/SignInView;

    invoke-static {v1}, Lcom/osp/app/signin/SignInView;->j(Lcom/osp/app/signin/SignInView;)Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/osp/app/signin/jv;->g:Lcom/osp/app/signin/SignInView;

    invoke-static {v2}, Lcom/osp/app/signin/SignInView;->Y(Lcom/osp/app/signin/SignInView;)Lcom/osp/app/signin/kd;

    move-result-object v2

    invoke-virtual {v2}, Lcom/osp/app/signin/kd;->b()I

    move-result v2

    if-eqz v2, :cond_9

    move v10, v9

    :cond_9
    invoke-static {v0, v1, v10}, Lcom/msc/openprovider/b;->a(Landroid/content/Context;Ljava/lang/String;Z)V

    .line 6127
    :cond_a
    invoke-static {}, Lcom/osp/app/pushmarketing/k;->a()Lcom/osp/app/pushmarketing/k;

    iget-object v0, p0, Lcom/osp/app/signin/jv;->g:Lcom/osp/app/signin/SignInView;

    invoke-static {v0}, Lcom/osp/app/pushmarketing/k;->c(Landroid/content/Context;)V

    .line 6133
    new-instance v0, Ljava/util/LinkedList;

    invoke-direct {v0}, Ljava/util/LinkedList;-><init>()V

    .line 6136
    iget-object v1, p0, Lcom/osp/app/signin/jv;->g:Lcom/osp/app/signin/SignInView;

    invoke-static {v1}, Lcom/osp/app/signin/SignInView;->c(Lcom/osp/app/signin/SignInView;)Landroid/content/Intent;

    move-result-object v1

    const-string v2, "signin_duration"

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v4

    iget-object v3, p0, Lcom/osp/app/signin/jv;->g:Lcom/osp/app/signin/SignInView;

    invoke-static {v3}, Lcom/osp/app/signin/SignInView;->B(Lcom/osp/app/signin/SignInView;)J

    move-result-wide v6

    sub-long/2addr v4, v6

    invoke-virtual {v1, v2, v4, v5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;J)Landroid/content/Intent;

    .line 6137
    iget-object v1, p0, Lcom/osp/app/signin/jv;->g:Lcom/osp/app/signin/SignInView;

    invoke-static {v1}, Lcom/osp/app/signin/SignInView;->c(Lcom/osp/app/signin/SignInView;)Landroid/content/Intent;

    move-result-object v1

    const-string v2, "signin_status"

    invoke-virtual {v1, v2, v9}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 6139
    new-instance v1, Lcom/osp/app/bigdatalog/c;

    iget-object v2, p0, Lcom/osp/app/signin/jv;->g:Lcom/osp/app/signin/SignInView;

    invoke-virtual {v2}, Lcom/osp/app/signin/SignInView;->getApplicationContext()Landroid/content/Context;

    move-result-object v2

    iget-object v3, p0, Lcom/osp/app/signin/jv;->g:Lcom/osp/app/signin/SignInView;

    invoke-static {v3}, Lcom/osp/app/signin/SignInView;->c(Lcom/osp/app/signin/SignInView;)Landroid/content/Intent;

    move-result-object v3

    invoke-direct {v1, v2, v11, v12, v3}, Lcom/osp/app/bigdatalog/c;-><init>(Landroid/content/Context;IILandroid/content/Intent;)V

    .line 6142
    invoke-interface {v0, v1}, Ljava/util/Queue;->add(Ljava/lang/Object;)Z

    .line 6144
    iget-object v1, p0, Lcom/osp/app/signin/jv;->g:Lcom/osp/app/signin/SignInView;

    invoke-static {v1}, Lcom/osp/app/signin/SignInView;->c(Lcom/osp/app/signin/SignInView;)Landroid/content/Intent;

    move-result-object v1

    const-string v2, "stay_duration"

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v4

    iget-object v3, p0, Lcom/osp/app/signin/jv;->g:Lcom/osp/app/signin/SignInView;

    invoke-static {v3}, Lcom/osp/app/signin/SignInView;->Z(Lcom/osp/app/signin/SignInView;)J

    move-result-wide v6

    sub-long/2addr v4, v6

    invoke-virtual {v1, v2, v4, v5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;J)Landroid/content/Intent;

    .line 6145
    iget-object v1, p0, Lcom/osp/app/signin/jv;->g:Lcom/osp/app/signin/SignInView;

    invoke-static {v1}, Lcom/osp/app/signin/SignInView;->aa(Lcom/osp/app/signin/SignInView;)Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_b

    .line 6147
    iget-object v1, p0, Lcom/osp/app/signin/jv;->g:Lcom/osp/app/signin/SignInView;

    invoke-static {v1}, Lcom/osp/app/signin/SignInView;->c(Lcom/osp/app/signin/SignInView;)Landroid/content/Intent;

    move-result-object v1

    const-string v2, "previous_activity"

    iget-object v3, p0, Lcom/osp/app/signin/jv;->g:Lcom/osp/app/signin/SignInView;

    invoke-static {v3}, Lcom/osp/app/signin/SignInView;->aa(Lcom/osp/app/signin/SignInView;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 6149
    :cond_b
    new-instance v1, Lcom/osp/app/bigdatalog/c;

    iget-object v2, p0, Lcom/osp/app/signin/jv;->g:Lcom/osp/app/signin/SignInView;

    invoke-virtual {v2}, Lcom/osp/app/signin/SignInView;->getApplicationContext()Landroid/content/Context;

    move-result-object v2

    const/4 v3, 0x2

    iget-object v4, p0, Lcom/osp/app/signin/jv;->g:Lcom/osp/app/signin/SignInView;

    invoke-static {v4}, Lcom/osp/app/signin/SignInView;->c(Lcom/osp/app/signin/SignInView;)Landroid/content/Intent;

    move-result-object v4

    invoke-direct {v1, v2, v11, v3, v4}, Lcom/osp/app/bigdatalog/c;-><init>(Landroid/content/Context;IILandroid/content/Intent;)V

    .line 6152
    invoke-interface {v0, v1}, Ljava/util/Queue;->add(Ljava/lang/Object;)Z

    .line 6154
    new-instance v1, Lcom/osp/app/bigdatalog/c;

    iget-object v2, p0, Lcom/osp/app/signin/jv;->g:Lcom/osp/app/signin/SignInView;

    invoke-virtual {v2}, Lcom/osp/app/signin/SignInView;->getApplicationContext()Landroid/content/Context;

    move-result-object v2

    iget-object v3, p0, Lcom/osp/app/signin/jv;->g:Lcom/osp/app/signin/SignInView;

    invoke-static {v3}, Lcom/osp/app/signin/SignInView;->c(Lcom/osp/app/signin/SignInView;)Landroid/content/Intent;

    move-result-object v3

    invoke-direct {v1, v2, v11, v9, v3}, Lcom/osp/app/bigdatalog/c;-><init>(Landroid/content/Context;IILandroid/content/Intent;)V

    .line 6157
    invoke-interface {v0, v1}, Ljava/util/Queue;->add(Ljava/lang/Object;)Z

    .line 6159
    invoke-static {}, Lcom/osp/app/bigdatalog/e;->a()Lcom/osp/app/bigdatalog/e;

    move-result-object v1

    iget-object v2, p0, Lcom/osp/app/signin/jv;->g:Lcom/osp/app/signin/SignInView;

    invoke-virtual {v2}, Lcom/osp/app/signin/SignInView;->getApplicationContext()Landroid/content/Context;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/osp/app/bigdatalog/e;->a(Landroid/content/Context;)V

    .line 6160
    invoke-static {}, Lcom/osp/app/bigdatalog/e;->a()Lcom/osp/app/bigdatalog/e;

    move-result-object v1

    iget-object v2, p0, Lcom/osp/app/signin/jv;->g:Lcom/osp/app/signin/SignInView;

    invoke-virtual {v2}, Lcom/osp/app/signin/SignInView;->getApplicationContext()Landroid/content/Context;

    move-result-object v2

    invoke-virtual {v1, v2, v0}, Lcom/osp/app/bigdatalog/e;->a(Landroid/content/Context;Ljava/util/Queue;)V

    .line 6238
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v0, "SIV"

    const-string v1, "SignInDual_End"

    const-string v2, "END"

    invoke-static {v0, v1, v2}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_2

    .line 6042
    :catch_0
    move-exception v0

    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    goto/16 :goto_4

    .line 6063
    :cond_c
    const-string v0, "tj9u972o46"

    iget-object v1, p0, Lcom/osp/app/signin/jv;->g:Lcom/osp/app/signin/SignInView;

    invoke-static {v1}, Lcom/osp/app/signin/SignInView;->j(Lcom/osp/app/signin/SignInView;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_5

    .line 6066
    iget-object v0, p0, Lcom/osp/app/signin/jv;->g:Lcom/osp/app/signin/SignInView;

    iget-object v1, p0, Lcom/osp/app/signin/jv;->g:Lcom/osp/app/signin/SignInView;

    invoke-static {v1}, Lcom/osp/app/signin/SignInView;->j(Lcom/osp/app/signin/SignInView;)Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/osp/app/signin/jv;->g:Lcom/osp/app/signin/SignInView;

    invoke-static {v2}, Lcom/osp/app/signin/SignInView;->Q(Lcom/osp/app/signin/SignInView;)Lcom/msc/a/d;

    move-result-object v2

    invoke-virtual {v2}, Lcom/msc/a/d;->g()Ljava/lang/String;

    move-result-object v2

    iget-object v3, p0, Lcom/osp/app/signin/jv;->g:Lcom/osp/app/signin/SignInView;

    invoke-static {v3}, Lcom/osp/app/signin/SignInView;->Q(Lcom/osp/app/signin/SignInView;)Lcom/msc/a/d;

    move-result-object v3

    invoke-virtual {v3}, Lcom/msc/a/d;->j()J

    move-result-wide v3

    iget-object v5, p0, Lcom/osp/app/signin/jv;->g:Lcom/osp/app/signin/SignInView;

    invoke-static {v5}, Lcom/osp/app/signin/SignInView;->Q(Lcom/osp/app/signin/SignInView;)Lcom/msc/a/d;

    move-result-object v5

    invoke-virtual {v5}, Lcom/msc/a/d;->h()Ljava/lang/String;

    move-result-object v5

    iget-object v6, p0, Lcom/osp/app/signin/jv;->g:Lcom/osp/app/signin/SignInView;

    invoke-static {v6}, Lcom/osp/app/signin/SignInView;->Q(Lcom/osp/app/signin/SignInView;)Lcom/msc/a/d;

    move-result-object v6

    invoke-virtual {v6}, Lcom/msc/a/d;->k()J

    move-result-wide v6

    invoke-static/range {v0 .. v7}, Lcom/osp/app/util/ad;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;JLjava/lang/String;J)V

    goto/16 :goto_5

    .line 6092
    :cond_d
    invoke-static {}, Lcom/osp/app/signin/SamsungService;->a()Z

    move-result v0

    if-eqz v0, :cond_e

    .line 6098
    iget-object v0, p0, Lcom/osp/app/signin/jv;->g:Lcom/osp/app/signin/SignInView;

    invoke-static {v0, v10}, Lcom/osp/app/signin/SignInView;->c(Lcom/osp/app/signin/SignInView;Z)V

    goto/16 :goto_6

    .line 6101
    :cond_e
    iget-object v0, p0, Lcom/osp/app/signin/jv;->g:Lcom/osp/app/signin/SignInView;

    invoke-virtual {v8}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    sget-object v2, Ljava/util/Locale;->ENGLISH:Ljava/util/Locale;

    invoke-virtual {v1, v2}, Ljava/lang/String;->toLowerCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/osp/app/signin/SignInView;->g(Lcom/osp/app/signin/SignInView;Ljava/lang/String;)V

    goto/16 :goto_6

    .line 6105
    :cond_f
    iget-object v0, p0, Lcom/osp/app/signin/jv;->g:Lcom/osp/app/signin/SignInView;

    invoke-static {v0}, Lcom/osp/app/util/r;->p(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_10

    iget-object v0, p0, Lcom/osp/app/signin/jv;->g:Lcom/osp/app/signin/SignInView;

    invoke-static {v0}, Lcom/osp/app/signin/SignInView;->X(Lcom/osp/app/signin/SignInView;)Z

    move-result v0

    if-eqz v0, :cond_10

    invoke-static {}, Lcom/osp/app/signin/SamsungService;->a()Z

    move-result v0

    if-eqz v0, :cond_10

    .line 6107
    invoke-static {}, Lcom/osp/app/util/m;->a()Lcom/osp/app/util/m;

    iget-object v0, p0, Lcom/osp/app/signin/jv;->g:Lcom/osp/app/signin/SignInView;

    invoke-static {v0, v10}, Lcom/osp/app/util/m;->a(Landroid/app/Activity;Z)V

    .line 6114
    :goto_8
    iget-object v0, p0, Lcom/osp/app/signin/jv;->g:Lcom/osp/app/signin/SignInView;

    invoke-static {v0}, Landroid/preference/PreferenceManager;->getDefaultSharedPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    .line 6115
    const-string v1, "EMAIL_VALIDATION_KEY"

    iget-object v2, p0, Lcom/osp/app/signin/jv;->g:Lcom/osp/app/signin/SignInView;

    invoke-static {v2}, Lcom/osp/app/util/ad;->g(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    .line 6116
    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    goto/16 :goto_6

    .line 6110
    :cond_10
    iget-object v0, p0, Lcom/osp/app/signin/jv;->g:Lcom/osp/app/signin/SignInView;

    invoke-static {v0, v9}, Lcom/osp/app/signin/SignInView;->c(Lcom/osp/app/signin/SignInView;Z)V

    goto :goto_8

    :cond_11
    move v0, v10

    .line 6122
    goto/16 :goto_7

    .line 6170
    :cond_12
    iget-object v0, p0, Lcom/osp/app/signin/jv;->g:Lcom/osp/app/signin/SignInView;

    invoke-static {v0}, Lcom/osp/app/signin/SignInView;->c(Lcom/osp/app/signin/SignInView;)Landroid/content/Intent;

    move-result-object v0

    const-string v1, "signin_duration"

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    iget-object v4, p0, Lcom/osp/app/signin/jv;->g:Lcom/osp/app/signin/SignInView;

    invoke-static {v4}, Lcom/osp/app/signin/SignInView;->B(Lcom/osp/app/signin/SignInView;)J

    move-result-wide v4

    sub-long/2addr v2, v4

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;J)Landroid/content/Intent;

    .line 6171
    iget-object v0, p0, Lcom/osp/app/signin/jv;->g:Lcom/osp/app/signin/SignInView;

    invoke-static {v0}, Lcom/osp/app/signin/SignInView;->c(Lcom/osp/app/signin/SignInView;)Landroid/content/Intent;

    move-result-object v0

    const-string v1, "signin_status"

    invoke-virtual {v0, v1, v10}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 6172
    iget-object v0, p0, Lcom/osp/app/signin/jv;->g:Lcom/osp/app/signin/SignInView;

    invoke-static {v0}, Lcom/osp/app/signin/SignInView;->c(Lcom/osp/app/signin/SignInView;)Landroid/content/Intent;

    move-result-object v0

    const-string v1, "error_code"

    iget-object v2, p0, Lcom/osp/app/signin/jv;->b:Lcom/msc/c/f;

    invoke-virtual {v2}, Lcom/msc/c/f;->a()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 6174
    new-instance v0, Lcom/osp/app/bigdatalog/c;

    iget-object v1, p0, Lcom/osp/app/signin/jv;->g:Lcom/osp/app/signin/SignInView;

    invoke-virtual {v1}, Lcom/osp/app/signin/SignInView;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    iget-object v2, p0, Lcom/osp/app/signin/jv;->g:Lcom/osp/app/signin/SignInView;

    invoke-static {v2}, Lcom/osp/app/signin/SignInView;->c(Lcom/osp/app/signin/SignInView;)Landroid/content/Intent;

    move-result-object v2

    invoke-direct {v0, v1, v11, v12, v2}, Lcom/osp/app/bigdatalog/c;-><init>(Landroid/content/Context;IILandroid/content/Intent;)V

    .line 6177
    invoke-static {}, Lcom/osp/app/bigdatalog/e;->a()Lcom/osp/app/bigdatalog/e;

    move-result-object v1

    iget-object v2, p0, Lcom/osp/app/signin/jv;->g:Lcom/osp/app/signin/SignInView;

    invoke-virtual {v2}, Lcom/osp/app/signin/SignInView;->getApplicationContext()Landroid/content/Context;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/osp/app/bigdatalog/e;->a(Landroid/content/Context;)V

    .line 6178
    invoke-static {}, Lcom/osp/app/bigdatalog/e;->a()Lcom/osp/app/bigdatalog/e;

    move-result-object v1

    iget-object v2, p0, Lcom/osp/app/signin/jv;->g:Lcom/osp/app/signin/SignInView;

    invoke-virtual {v2}, Lcom/osp/app/signin/SignInView;->getApplicationContext()Landroid/content/Context;

    move-result-object v2

    invoke-virtual {v1, v2, v0}, Lcom/osp/app/bigdatalog/e;->a(Landroid/content/Context;Lcom/osp/app/bigdatalog/c;)V

    .line 6181
    iget-object v0, p0, Lcom/osp/app/signin/jv;->g:Lcom/osp/app/signin/SignInView;

    invoke-virtual {v0}, Lcom/osp/app/signin/SignInView;->c()V

    .line 6197
    iget-object v0, p0, Lcom/osp/app/signin/jv;->g:Lcom/osp/app/signin/SignInView;

    invoke-static {v0}, Lcom/osp/app/signin/SignInView;->F(Lcom/osp/app/signin/SignInView;)Z

    move-result v0

    if-eqz v0, :cond_15

    .line 6199
    iget-object v0, p0, Lcom/osp/app/signin/jv;->g:Lcom/osp/app/signin/SignInView;

    invoke-static {v0, v10}, Lcom/osp/app/signin/SignInView;->b(Lcom/osp/app/signin/SignInView;Z)Z

    .line 6201
    iget-object v0, p0, Lcom/osp/app/signin/jv;->g:Lcom/osp/app/signin/SignInView;

    invoke-static {v0}, Lcom/osp/app/signin/SignInView;->G(Lcom/osp/app/signin/SignInView;)Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_13

    .line 6203
    iget-object v0, p0, Lcom/osp/app/signin/jv;->g:Lcom/osp/app/signin/SignInView;

    iget-object v1, p0, Lcom/osp/app/signin/jv;->g:Lcom/osp/app/signin/SignInView;

    invoke-static {v1}, Lcom/osp/app/signin/SignInView;->G(Lcom/osp/app/signin/SignInView;)Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/osp/app/signin/jv;->g:Lcom/osp/app/signin/SignInView;

    invoke-virtual {v2}, Lcom/osp/app/signin/SignInView;->getApplicationContext()Landroid/content/Context;

    iget-object v2, p0, Lcom/osp/app/signin/jv;->g:Lcom/osp/app/signin/SignInView;

    invoke-static {v2}, Lcom/osp/app/signin/SignInView;->G(Lcom/osp/app/signin/SignInView;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/osp/common/util/i;->d(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v1, v2}, Lcom/msc/c/e;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)V

    .line 6204
    iget-object v0, p0, Lcom/osp/app/signin/jv;->g:Lcom/osp/app/signin/SignInView;

    new-instance v1, Lcom/osp/app/signin/jj;

    iget-object v2, p0, Lcom/osp/app/signin/jv;->g:Lcom/osp/app/signin/SignInView;

    invoke-direct {v1, v2}, Lcom/osp/app/signin/jj;-><init>(Lcom/osp/app/signin/SignInView;)V

    invoke-static {v0, v1}, Lcom/osp/app/signin/SignInView;->a(Lcom/osp/app/signin/SignInView;Lcom/osp/app/signin/jj;)Lcom/osp/app/signin/jj;

    .line 6205
    iget-object v0, p0, Lcom/osp/app/signin/jv;->g:Lcom/osp/app/signin/SignInView;

    invoke-static {v0}, Lcom/osp/app/signin/SignInView;->H(Lcom/osp/app/signin/SignInView;)Lcom/osp/app/signin/jj;

    move-result-object v0

    invoke-static {v0}, Lcom/msc/sa/c/d;->a(Landroid/os/AsyncTask;)V

    .line 6218
    :goto_9
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v0, "SIV"

    const-string v1, "SignInDual_End"

    const-string v2, "END"

    invoke-static {v0, v1, v2}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_2

    .line 6208
    :cond_13
    invoke-static {}, Lcom/osp/device/b;->b()Lcom/osp/device/b;

    move-result-object v0

    iget-object v1, p0, Lcom/osp/app/signin/jv;->g:Lcom/osp/app/signin/SignInView;

    invoke-virtual {v0, v1}, Lcom/osp/device/b;->i(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_14

    .line 6210
    iget-object v0, p0, Lcom/osp/app/signin/jv;->g:Lcom/osp/app/signin/SignInView;

    new-instance v1, Lcom/osp/app/signin/jm;

    iget-object v2, p0, Lcom/osp/app/signin/jv;->g:Lcom/osp/app/signin/SignInView;

    invoke-direct {v1, v2}, Lcom/osp/app/signin/jm;-><init>(Lcom/osp/app/signin/SignInView;)V

    invoke-static {v0, v1}, Lcom/osp/app/signin/SignInView;->a(Lcom/osp/app/signin/SignInView;Lcom/osp/app/signin/jm;)Lcom/osp/app/signin/jm;

    .line 6211
    iget-object v0, p0, Lcom/osp/app/signin/jv;->g:Lcom/osp/app/signin/SignInView;

    invoke-static {v0}, Lcom/osp/app/signin/SignInView;->I(Lcom/osp/app/signin/SignInView;)Lcom/osp/app/signin/jm;

    move-result-object v0

    invoke-static {v0}, Lcom/msc/sa/c/d;->a(Landroid/os/AsyncTask;)V

    goto :goto_9

    .line 6214
    :cond_14
    iget-object v0, p0, Lcom/osp/app/signin/jv;->g:Lcom/osp/app/signin/SignInView;

    invoke-static {v0}, Lcom/osp/app/signin/SignInView;->J(Lcom/osp/app/signin/SignInView;)V

    goto :goto_9

    .line 6222
    :cond_15
    invoke-static {}, Lcom/osp/device/b;->b()Lcom/osp/device/b;

    move-result-object v0

    invoke-virtual {v0}, Lcom/osp/device/b;->a()Z

    move-result v0

    if-eqz v0, :cond_16

    .line 6224
    iget-boolean v0, p0, Lcom/osp/app/signin/jv;->v:Z

    if-nez v0, :cond_17

    .line 6226
    :cond_16
    invoke-virtual {p0, v10}, Lcom/osp/app/signin/jv;->a(Z)V

    .line 6232
    iget-object v0, p0, Lcom/osp/app/signin/jv;->g:Lcom/osp/app/signin/SignInView;

    invoke-static {v0}, Lcom/osp/app/signin/SignInView;->a(Lcom/osp/app/signin/SignInView;)V

    .line 6235
    :cond_17
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v0, "SIV"

    const-string v1, "SignInDual_End"

    const-string v2, "END"

    invoke-static {v0, v1, v2}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_2

    :cond_18
    move v0, v9

    goto/16 :goto_3
.end method

.method static synthetic c(Lcom/osp/app/signin/jv;)Z
    .locals 1

    .prologue
    .line 5100
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/osp/app/signin/jv;->k:Z

    return v0
.end method

.method static synthetic d(Lcom/osp/app/signin/jv;)Z
    .locals 1

    .prologue
    .line 5100
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/osp/app/signin/jv;->i:Z

    return v0
.end method


# virtual methods
.method protected final varargs a()Ljava/lang/Boolean;
    .locals 6

    .prologue
    const/4 v2, 0x0

    const/4 v5, 0x0

    .line 5328
    iget-object v0, p0, Lcom/osp/app/signin/jv;->g:Lcom/osp/app/signin/SignInView;

    invoke-static {v0, v5}, Lcom/osp/app/signin/SignInView;->b(Lcom/osp/app/signin/SignInView;Z)Z

    .line 5329
    iget-object v0, p0, Lcom/osp/app/signin/jv;->g:Lcom/osp/app/signin/SignInView;

    invoke-static {v0, v2}, Lcom/osp/app/signin/SignInView;->d(Lcom/osp/app/signin/SignInView;Ljava/lang/String;)Ljava/lang/String;

    .line 5331
    iget-object v0, p0, Lcom/osp/app/signin/jv;->g:Lcom/osp/app/signin/SignInView;

    invoke-static {v0}, Lcom/osp/app/signin/SignInView;->N(Lcom/osp/app/signin/SignInView;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 5333
    iget-object v0, p0, Lcom/osp/app/signin/jv;->g:Lcom/osp/app/signin/SignInView;

    invoke-static {v0}, Lcom/osp/app/signin/SignInView;->e(Lcom/osp/app/signin/SignInView;)Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/osp/app/signin/jv;->g:Lcom/osp/app/signin/SignInView;

    invoke-static {v0}, Lcom/osp/app/signin/SignInView;->s(Lcom/osp/app/signin/SignInView;)Landroid/view/View;

    move-result-object v0

    const v1, 0x7f0c0129

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/EditText;

    iget-object v1, p0, Lcom/osp/app/signin/jv;->g:Lcom/osp/app/signin/SignInView;

    invoke-static {v1}, Lcom/osp/app/signin/SignInView;->s(Lcom/osp/app/signin/SignInView;)Landroid/view/View;

    move-result-object v1

    const v3, 0x7f0c012a

    invoke-virtual {v1, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/EditText;

    :goto_0
    invoke-static {}, Lcom/osp/device/b;->b()Lcom/osp/device/b;

    move-result-object v3

    invoke-virtual {v3}, Lcom/osp/device/b;->a()Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-virtual {v0}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Landroid/telephony/PhoneNumberUtils;->isGlobalPhoneNumber(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_0

    new-instance v2, Lcom/osp/app/signin/cn;

    invoke-direct {v2}, Lcom/osp/app/signin/cn;-><init>()V

    invoke-virtual {v2}, Lcom/osp/app/signin/cn;->a()V

    :cond_0
    invoke-static {}, Lcom/msc/c/g;->a()Lcom/msc/c/g;

    iget-object v3, p0, Lcom/osp/app/signin/jv;->g:Lcom/osp/app/signin/SignInView;

    invoke-virtual {v0}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    sget-object v4, Ljava/util/Locale;->ENGLISH:Ljava/util/Locale;

    invoke-virtual {v0, v4}, Ljava/lang/String;->toLowerCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v3, v0, v1, v2, p0}, Lcom/msc/c/g;->b(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Lcom/osp/app/signin/cn;Lcom/msc/b/h;)J

    move-result-wide v0

    iput-wide v0, p0, Lcom/osp/app/signin/jv;->r:J

    iget-wide v0, p0, Lcom/osp/app/signin/jv;->r:J

    invoke-virtual {p0, v0, v1}, Lcom/osp/app/signin/jv;->b(J)V

    iget-wide v0, p0, Lcom/osp/app/signin/jv;->r:J

    const-string v2, "from_json"

    invoke-virtual {p0, v0, v1, v2}, Lcom/osp/app/signin/jv;->a(JLjava/lang/String;)V

    iget-wide v0, p0, Lcom/osp/app/signin/jv;->r:J

    sget-object v2, Lcom/msc/b/g;->c:Lcom/msc/b/g;

    invoke-static {v0, v1, v2}, Lcom/msc/c/g;->a(JLcom/msc/b/g;)V

    .line 5356
    :goto_1
    invoke-static {v5}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    return-object v0

    .line 5333
    :cond_1
    iget-object v0, p0, Lcom/osp/app/signin/jv;->g:Lcom/osp/app/signin/SignInView;

    const v1, 0x7f0c0121

    invoke-virtual {v0, v1}, Lcom/osp/app/signin/SignInView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/EditText;

    iget-object v1, p0, Lcom/osp/app/signin/jv;->g:Lcom/osp/app/signin/SignInView;

    const v3, 0x7f0c0122

    invoke-virtual {v1, v3}, Lcom/osp/app/signin/SignInView;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/EditText;

    goto :goto_0

    .line 5336
    :cond_2
    iget-object v0, p0, Lcom/osp/app/signin/jv;->e:Ljava/lang/Thread;

    invoke-virtual {v0}, Ljava/lang/Thread;->start()V

    .line 5337
    iget-object v0, p0, Lcom/osp/app/signin/jv;->f:Ljava/lang/Thread;

    invoke-virtual {v0}, Ljava/lang/Thread;->start()V

    .line 5339
    :cond_3
    iget-boolean v0, p0, Lcom/osp/app/signin/jv;->h:Z

    if-eqz v0, :cond_4

    iget-boolean v0, p0, Lcom/osp/app/signin/jv;->i:Z

    if-nez v0, :cond_5

    .line 5341
    :cond_4
    invoke-virtual {p0}, Lcom/osp/app/signin/jv;->isCancelled()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 5343
    :cond_5
    iget-boolean v0, p0, Lcom/osp/app/signin/jv;->l:Z

    if-eqz v0, :cond_6

    iget-boolean v0, p0, Lcom/osp/app/signin/jv;->k:Z

    if-eqz v0, :cond_6

    .line 5349
    const-string v0, "Success"

    iput-object v0, p0, Lcom/osp/app/signin/jv;->m:Ljava/lang/String;

    goto :goto_1

    .line 5352
    :cond_6
    const-string v0, "Fail"

    iput-object v0, p0, Lcom/osp/app/signin/jv;->m:Ljava/lang/String;

    goto :goto_1
.end method

.method public final declared-synchronized a(Lcom/msc/b/c;)V
    .locals 6

    .prologue
    .line 5439
    monitor-enter p0

    :try_start_0
    invoke-super {p0, p1}, Lcom/msc/c/b;->a(Lcom/msc/b/c;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 5441
    if-nez p1, :cond_1

    .line 5692
    :cond_0
    :goto_0
    monitor-exit p0

    return-void

    .line 5446
    :cond_1
    :try_start_1
    invoke-virtual {p1}, Lcom/msc/b/c;->a()J

    move-result-wide v0

    .line 5447
    invoke-virtual {p1}, Lcom/msc/b/c;->e()Ljava/lang/String;

    move-result-object v2

    .line 5449
    iget-wide v4, p0, Lcom/osp/app/signin/jv;->o:J
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    cmp-long v3, v0, v4

    if-nez v3, :cond_6

    .line 5453
    :try_start_2
    invoke-static {}, Lcom/msc/c/h;->a()Lcom/msc/c/h;

    invoke-static {v2}, Lcom/msc/c/h;->r(Ljava/lang/String;)Lcom/msc/a/b;

    move-result-object v0

    iput-object v0, p0, Lcom/osp/app/signin/jv;->n:Lcom/msc/a/b;

    .line 5456
    new-instance v0, Lcom/osp/common/property/a;

    iget-object v1, p0, Lcom/osp/app/signin/jv;->g:Lcom/osp/app/signin/SignInView;

    invoke-direct {v0, v1}, Lcom/osp/common/property/a;-><init>(Landroid/content/Context;)V

    .line 5457
    const-string v1, "device.registration.appid"

    invoke-virtual {v0, v1}, Lcom/osp/common/property/a;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_2

    .line 5459
    const-string v1, "device.registration.appid"

    invoke-virtual {v0, v1}, Lcom/osp/common/property/a;->c(Ljava/lang/String;)V

    .line 5461
    :cond_2
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v1, "SIV"

    const-string v3, "save appid in property."

    invoke-static {v1, v3}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 5462
    const-string v1, "device.registration.appid"

    const-string v3, "j5p7ll8g33"

    invoke-virtual {v0, v1, v3}, Lcom/osp/common/property/a;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 5465
    iget-object v0, p0, Lcom/osp/app/signin/jv;->g:Lcom/osp/app/signin/SignInView;

    invoke-static {v0}, Lcom/osp/app/signin/SignInView;->N(Lcom/osp/app/signin/SignInView;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 5467
    const-string v0, "Success"

    iput-object v0, p0, Lcom/osp/app/signin/jv;->m:Ljava/lang/String;

    .line 5469
    :cond_3
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    invoke-static {v2}, Lcom/osp/app/util/aq;->b(Ljava/lang/String;)V
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_0
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 5478
    :try_start_3
    iget-object v0, p0, Lcom/osp/app/signin/jv;->n:Lcom/msc/a/b;

    if-eqz v0, :cond_5

    .line 5480
    iget-object v0, p0, Lcom/osp/app/signin/jv;->n:Lcom/msc/a/b;

    invoke-virtual {v0}, Lcom/msc/a/b;->e()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_4

    iget-object v0, p0, Lcom/osp/app/signin/jv;->n:Lcom/msc/a/b;

    invoke-virtual {v0}, Lcom/msc/a/b;->e()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v0

    if-lez v0, :cond_4

    .line 5482
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/osp/app/signin/jv;->l:Z
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    goto :goto_0

    .line 5439
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 5470
    :catch_0
    move-exception v0

    .line 5472
    :try_start_4
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    .line 5473
    const/4 v1, 0x0

    iput-boolean v1, p0, Lcom/osp/app/signin/jv;->l:Z

    .line 5474
    new-instance v1, Lcom/msc/c/f;

    invoke-direct {v1, v0}, Lcom/msc/c/f;-><init>(Ljava/lang/Exception;)V

    iput-object v1, p0, Lcom/osp/app/signin/jv;->b:Lcom/msc/c/f;

    goto :goto_0

    .line 5485
    :cond_4
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/osp/app/signin/jv;->l:Z

    .line 5486
    new-instance v0, Lcom/msc/c/f;

    invoke-direct {v0}, Lcom/msc/c/f;-><init>()V

    iput-object v0, p0, Lcom/osp/app/signin/jv;->b:Lcom/msc/c/f;

    .line 5487
    iget-object v0, p0, Lcom/osp/app/signin/jv;->g:Lcom/osp/app/signin/SignInView;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/osp/app/signin/SignInView;->b(I)V

    goto/16 :goto_0

    .line 5491
    :cond_5
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/osp/app/signin/jv;->l:Z

    .line 5492
    new-instance v0, Lcom/msc/c/f;

    invoke-direct {v0}, Lcom/msc/c/f;-><init>()V

    iput-object v0, p0, Lcom/osp/app/signin/jv;->b:Lcom/msc/c/f;

    .line 5493
    iget-object v0, p0, Lcom/osp/app/signin/jv;->g:Lcom/osp/app/signin/SignInView;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/osp/app/signin/SignInView;->b(I)V

    goto/16 :goto_0

    .line 5496
    :cond_6
    iget-wide v4, p0, Lcom/osp/app/signin/jv;->p:J
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    cmp-long v3, v0, v4

    if-nez v3, :cond_9

    .line 5500
    :try_start_5
    iget-object v0, p0, Lcom/osp/app/signin/jv;->g:Lcom/osp/app/signin/SignInView;

    invoke-static {}, Lcom/msc/c/h;->a()Lcom/msc/c/h;

    invoke-static {v2}, Lcom/msc/c/h;->e(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/osp/app/signin/SignInView;->b(Lcom/osp/app/signin/SignInView;Ljava/lang/String;)Ljava/lang/String;

    .line 5501
    iget-object v0, p0, Lcom/osp/app/signin/jv;->g:Lcom/osp/app/signin/SignInView;

    invoke-static {v0}, Lcom/osp/app/signin/SignInView;->y(Lcom/osp/app/signin/SignInView;)Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/osp/app/signin/jv;->g:Lcom/osp/app/signin/SignInView;

    invoke-static {v0}, Lcom/osp/app/signin/SignInView;->y(Lcom/osp/app/signin/SignInView;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v0

    if-lez v0, :cond_0

    .line 5505
    iget-object v0, p0, Lcom/osp/app/signin/jv;->g:Lcom/osp/app/signin/SignInView;

    const/4 v1, 0x1

    invoke-static {v0, v1}, Lcom/osp/app/signin/SignInView;->b(Lcom/osp/app/signin/SignInView;Z)Z

    .line 5506
    invoke-static {}, Lcom/osp/common/util/i;->a()Lcom/osp/common/util/i;

    iget-object v0, p0, Lcom/osp/app/signin/jv;->g:Lcom/osp/app/signin/SignInView;

    invoke-static {v0}, Lcom/osp/common/util/i;->e(Landroid/content/Context;)Z
    :try_end_5
    .catch Ljava/lang/Exception; {:try_start_5 .. :try_end_5} :catch_1
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    move-result v0

    if-eqz v0, :cond_7

    .line 5511
    :try_start_6
    iget-object v0, p0, Lcom/osp/app/signin/jv;->g:Lcom/osp/app/signin/SignInView;

    invoke-static {}, Lcom/osp/common/util/i;->a()Lcom/osp/common/util/i;

    iget-object v1, p0, Lcom/osp/app/signin/jv;->g:Lcom/osp/app/signin/SignInView;

    invoke-static {v1}, Lcom/osp/common/util/i;->h(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/osp/app/signin/SignInView;->d(Lcom/osp/app/signin/SignInView;Ljava/lang/String;)Ljava/lang/String;

    .line 5512
    iget-object v0, p0, Lcom/osp/app/signin/jv;->g:Lcom/osp/app/signin/SignInView;

    iget-object v1, p0, Lcom/osp/app/signin/jv;->g:Lcom/osp/app/signin/SignInView;

    invoke-static {v1}, Lcom/osp/app/signin/SignInView;->G(Lcom/osp/app/signin/SignInView;)Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/osp/app/signin/jv;->g:Lcom/osp/app/signin/SignInView;

    invoke-virtual {v2}, Lcom/osp/app/signin/SignInView;->getApplicationContext()Landroid/content/Context;

    iget-object v2, p0, Lcom/osp/app/signin/jv;->g:Lcom/osp/app/signin/SignInView;

    invoke-static {v2}, Lcom/osp/app/signin/SignInView;->G(Lcom/osp/app/signin/SignInView;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/osp/common/util/i;->d(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v1, v2}, Lcom/msc/c/e;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)V
    :try_end_6
    .catch Ljava/lang/Exception; {:try_start_6 .. :try_end_6} :catch_2
    .catchall {:try_start_6 .. :try_end_6} :catchall_1

    .line 5518
    :try_start_7
    iget-object v0, p0, Lcom/osp/app/signin/jv;->g:Lcom/osp/app/signin/SignInView;

    invoke-static {v0}, Lcom/osp/app/signin/SignInView;->G(Lcom/osp/app/signin/SignInView;)Ljava/lang/String;

    move-result-object v0

    if-nez v0, :cond_7

    .line 5520
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v0, "SIV"

    const-string v1, "The sim state is ready but mcc is null!!"

    invoke-static {v0, v1}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 5525
    :cond_7
    :goto_1
    iget-object v0, p0, Lcom/osp/app/signin/jv;->g:Lcom/osp/app/signin/SignInView;

    invoke-static {v0}, Lcom/osp/app/signin/SignInView;->G(Lcom/osp/app/signin/SignInView;)Ljava/lang/String;

    move-result-object v0

    if-nez v0, :cond_0

    .line 5527
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v0, "SIV"

    const-string v1, "requestMyCountryZone"

    invoke-static {v0, v1}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;)V

    invoke-static {}, Lcom/msc/c/g;->a()Lcom/msc/c/g;

    iget-object v0, p0, Lcom/osp/app/signin/jv;->g:Lcom/osp/app/signin/SignInView;

    invoke-static {v0, p0}, Lcom/msc/c/g;->b(Landroid/content/Context;Lcom/msc/b/h;)J

    move-result-wide v0

    iput-wide v0, p0, Lcom/osp/app/signin/jv;->t:J

    iget-wide v0, p0, Lcom/osp/app/signin/jv;->t:J

    invoke-virtual {p0, v0, v1}, Lcom/osp/app/signin/jv;->a(J)V

    iget-wide v0, p0, Lcom/osp/app/signin/jv;->t:J

    sget-object v2, Lcom/msc/b/g;->b:Lcom/msc/b/g;

    invoke-static {v0, v1, v2}, Lcom/msc/c/g;->a(JLcom/msc/b/g;)V
    :try_end_7
    .catch Ljava/lang/Exception; {:try_start_7 .. :try_end_7} :catch_1
    .catchall {:try_start_7 .. :try_end_7} :catchall_0

    goto/16 :goto_0

    .line 5530
    :catch_1
    move-exception v0

    :try_start_8
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V
    :try_end_8
    .catchall {:try_start_8 .. :try_end_8} :catchall_0

    goto/16 :goto_0

    .line 5513
    :catch_2
    move-exception v0

    :try_start_9
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V
    :try_end_9
    .catchall {:try_start_9 .. :try_end_9} :catchall_1

    .line 5518
    :try_start_a
    iget-object v0, p0, Lcom/osp/app/signin/jv;->g:Lcom/osp/app/signin/SignInView;

    invoke-static {v0}, Lcom/osp/app/signin/SignInView;->G(Lcom/osp/app/signin/SignInView;)Ljava/lang/String;

    move-result-object v0

    if-nez v0, :cond_7

    .line 5520
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v0, "SIV"

    const-string v1, "The sim state is ready but mcc is null!!"

    invoke-static {v0, v1}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1

    .line 5518
    :catchall_1
    move-exception v0

    iget-object v1, p0, Lcom/osp/app/signin/jv;->g:Lcom/osp/app/signin/SignInView;

    invoke-static {v1}, Lcom/osp/app/signin/SignInView;->G(Lcom/osp/app/signin/SignInView;)Ljava/lang/String;

    move-result-object v1

    if-nez v1, :cond_8

    .line 5520
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v1, "SIV"

    const-string v2, "The sim state is ready but mcc is null!!"

    invoke-static {v1, v2}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;)V

    :cond_8
    throw v0
    :try_end_a
    .catch Ljava/lang/Exception; {:try_start_a .. :try_end_a} :catch_1
    .catchall {:try_start_a .. :try_end_a} :catchall_0

    .line 5534
    :cond_9
    :try_start_b
    iget-wide v4, p0, Lcom/osp/app/signin/jv;->q:J
    :try_end_b
    .catchall {:try_start_b .. :try_end_b} :catchall_0

    cmp-long v3, v0, v4

    if-nez v3, :cond_f

    .line 5538
    :try_start_c
    new-instance v0, Lcom/msc/a/d;

    invoke-direct {v0}, Lcom/msc/a/d;-><init>()V

    .line 5539
    invoke-static {}, Lcom/msc/c/h;->a()Lcom/msc/c/h;

    invoke-static {v2, v0}, Lcom/msc/c/h;->a(Ljava/lang/String;Lcom/msc/a/d;)V

    .line 5540
    iget-object v1, p0, Lcom/osp/app/signin/jv;->g:Lcom/osp/app/signin/SignInView;

    invoke-virtual {v0}, Lcom/msc/a/d;->f()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/osp/app/signin/SignInView;->a(Lcom/osp/app/signin/SignInView;Ljava/lang/String;)Ljava/lang/String;

    .line 5541
    iget-object v1, p0, Lcom/osp/app/signin/jv;->g:Lcom/osp/app/signin/SignInView;

    invoke-virtual {v0}, Lcom/msc/a/d;->i()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/osp/app/signin/SignInView;->b(Lcom/osp/app/signin/SignInView;Ljava/lang/String;)Ljava/lang/String;

    .line 5542
    iget-object v1, p0, Lcom/osp/app/signin/jv;->g:Lcom/osp/app/signin/SignInView;

    invoke-static {v1, v0}, Lcom/osp/app/signin/SignInView;->a(Lcom/osp/app/signin/SignInView;Lcom/msc/a/d;)Lcom/msc/a/d;

    .line 5551
    invoke-static {}, Lcom/osp/device/b;->b()Lcom/osp/device/b;

    move-result-object v1

    invoke-virtual {v1}, Lcom/osp/device/b;->a()Z

    move-result v1

    if-eqz v1, :cond_a

    .line 5553
    invoke-virtual {v0}, Lcom/msc/a/d;->a()Z

    move-result v1

    if-nez v1, :cond_0

    const-string v1, "S"

    invoke-virtual {v0}, Lcom/msc/a/d;->b()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 5561
    :cond_a
    iget-object v0, p0, Lcom/osp/app/signin/jv;->g:Lcom/osp/app/signin/SignInView;

    invoke-static {v0}, Lcom/osp/app/signin/SignInView;->y(Lcom/osp/app/signin/SignInView;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_e

    iget-object v0, p0, Lcom/osp/app/signin/jv;->g:Lcom/osp/app/signin/SignInView;

    invoke-static {v0}, Lcom/osp/app/signin/SignInView;->x(Lcom/osp/app/signin/SignInView;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_e

    iget-object v0, p0, Lcom/osp/app/signin/jv;->g:Lcom/osp/app/signin/SignInView;

    invoke-static {v0}, Lcom/osp/app/signin/SignInView;->Q(Lcom/osp/app/signin/SignInView;)Lcom/msc/a/d;

    move-result-object v0

    invoke-virtual {v0}, Lcom/msc/a/d;->g()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_e

    .line 5563
    iget-object v0, p0, Lcom/osp/app/signin/jv;->g:Lcom/osp/app/signin/SignInView;

    invoke-static {v0}, Lcom/osp/app/signin/SignInView;->O(Lcom/osp/app/signin/SignInView;)Z

    move-result v0

    if-nez v0, :cond_d

    .line 5565
    const-string v0, "OSP_02"

    iget-object v1, p0, Lcom/osp/app/signin/jv;->g:Lcom/osp/app/signin/SignInView;

    invoke-static {v1}, Lcom/osp/app/signin/SignInView;->n(Lcom/osp/app/signin/SignInView;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_c

    iget-object v0, p0, Lcom/osp/app/signin/jv;->g:Lcom/osp/app/signin/SignInView;

    invoke-static {v0}, Lcom/osp/app/signin/SignInView;->j(Lcom/osp/app/signin/SignInView;)Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_c

    iget-object v0, p0, Lcom/osp/app/signin/jv;->g:Lcom/osp/app/signin/SignInView;

    invoke-static {v0}, Lcom/osp/app/signin/SignInView;->P(Lcom/osp/app/signin/SignInView;)Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_b

    const-string v0, "BG_mode"

    iget-object v1, p0, Lcom/osp/app/signin/jv;->g:Lcom/osp/app/signin/SignInView;

    invoke-static {v1}, Lcom/osp/app/signin/SignInView;->i(Lcom/osp/app/signin/SignInView;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_c

    .line 5567
    :cond_b
    invoke-virtual {p0}, Lcom/osp/app/signin/jv;->isCancelled()Z

    move-result v0

    if-nez v0, :cond_0

    .line 5571
    iget-object v0, p0, Lcom/osp/app/signin/jv;->g:Lcom/osp/app/signin/SignInView;

    invoke-static {v0}, Lcom/osp/app/signin/SignInView;->x(Lcom/osp/app/signin/SignInView;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/osp/app/signin/jv;->b(Ljava/lang/String;)V
    :try_end_c
    .catch Ljava/lang/Exception; {:try_start_c .. :try_end_c} :catch_3
    .catchall {:try_start_c .. :try_end_c} :catchall_0

    goto/16 :goto_0

    .line 5585
    :catch_3
    move-exception v0

    .line 5587
    :try_start_d
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    .line 5588
    new-instance v1, Lcom/msc/c/f;

    invoke-direct {v1, v0}, Lcom/msc/c/f;-><init>(Ljava/lang/Exception;)V

    iput-object v1, p0, Lcom/osp/app/signin/jv;->b:Lcom/msc/c/f;
    :try_end_d
    .catchall {:try_start_d .. :try_end_d} :catchall_0

    goto/16 :goto_0

    .line 5574
    :cond_c
    const/4 v0, 0x1

    :try_start_e
    iput-boolean v0, p0, Lcom/osp/app/signin/jv;->k:Z

    goto/16 :goto_0

    .line 5578
    :cond_d
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/osp/app/signin/jv;->k:Z

    goto/16 :goto_0

    .line 5582
    :cond_e
    new-instance v0, Lcom/msc/c/f;

    const-string v1, "ERR_0000"

    const-string v2, "fail"

    invoke-direct {v0, v1, v2}, Lcom/msc/c/f;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    iput-object v0, p0, Lcom/osp/app/signin/jv;->b:Lcom/msc/c/f;
    :try_end_e
    .catch Ljava/lang/Exception; {:try_start_e .. :try_end_e} :catch_3
    .catchall {:try_start_e .. :try_end_e} :catchall_0

    goto/16 :goto_0

    .line 5590
    :cond_f
    :try_start_f
    iget-wide v4, p0, Lcom/osp/app/signin/jv;->r:J

    cmp-long v3, v0, v4

    if-nez v3, :cond_13

    .line 5593
    new-instance v0, Lcom/msc/a/d;

    invoke-direct {v0}, Lcom/msc/a/d;-><init>()V
    :try_end_f
    .catchall {:try_start_f .. :try_end_f} :catchall_0

    .line 5596
    :try_start_10
    invoke-static {}, Lcom/msc/c/h;->a()Lcom/msc/c/h;

    invoke-static {v2, v0}, Lcom/msc/c/h;->a(Ljava/lang/String;Lcom/msc/a/d;)V

    .line 5597
    iget-object v1, p0, Lcom/osp/app/signin/jv;->g:Lcom/osp/app/signin/SignInView;

    invoke-virtual {v0}, Lcom/msc/a/d;->f()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/osp/app/signin/SignInView;->a(Lcom/osp/app/signin/SignInView;Ljava/lang/String;)Ljava/lang/String;

    .line 5598
    iget-object v1, p0, Lcom/osp/app/signin/jv;->g:Lcom/osp/app/signin/SignInView;

    invoke-virtual {v0}, Lcom/msc/a/d;->i()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/osp/app/signin/SignInView;->b(Lcom/osp/app/signin/SignInView;Ljava/lang/String;)Ljava/lang/String;

    .line 5599
    iget-object v1, p0, Lcom/osp/app/signin/jv;->g:Lcom/osp/app/signin/SignInView;

    invoke-static {v1, v0}, Lcom/osp/app/signin/SignInView;->a(Lcom/osp/app/signin/SignInView;Lcom/msc/a/d;)Lcom/msc/a/d;

    .line 5609
    invoke-static {}, Lcom/osp/device/b;->b()Lcom/osp/device/b;

    move-result-object v1

    invoke-virtual {v1}, Lcom/osp/device/b;->a()Z

    move-result v1

    if-eqz v1, :cond_10

    .line 5611
    invoke-virtual {v0}, Lcom/msc/a/d;->a()Z

    move-result v1

    if-nez v1, :cond_0

    const-string v1, "S"

    invoke-virtual {v0}, Lcom/msc/a/d;->b()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 5619
    :cond_10
    iget-object v0, p0, Lcom/osp/app/signin/jv;->g:Lcom/osp/app/signin/SignInView;

    invoke-static {v0}, Lcom/osp/app/signin/SignInView;->y(Lcom/osp/app/signin/SignInView;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_12

    iget-object v0, p0, Lcom/osp/app/signin/jv;->g:Lcom/osp/app/signin/SignInView;

    invoke-static {v0}, Lcom/osp/app/signin/SignInView;->x(Lcom/osp/app/signin/SignInView;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_12

    .line 5623
    iget-object v0, p0, Lcom/osp/app/signin/jv;->g:Lcom/osp/app/signin/SignInView;

    invoke-static {v0}, Lcom/osp/app/signin/SignInView;->e(Lcom/osp/app/signin/SignInView;)Z

    move-result v0

    if-eqz v0, :cond_11

    .line 5628
    iget-object v0, p0, Lcom/osp/app/signin/jv;->g:Lcom/osp/app/signin/SignInView;

    invoke-static {v0}, Lcom/osp/app/signin/SignInView;->s(Lcom/osp/app/signin/SignInView;)Landroid/view/View;

    move-result-object v0

    const v1, 0x7f0c0129

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/EditText;

    .line 5629
    iget-object v1, p0, Lcom/osp/app/signin/jv;->g:Lcom/osp/app/signin/SignInView;

    invoke-static {v1}, Lcom/osp/app/signin/SignInView;->s(Lcom/osp/app/signin/SignInView;)Landroid/view/View;

    move-result-object v1

    const v2, 0x7f0c012a

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/EditText;

    .line 5636
    :goto_2
    new-instance v2, Lcom/msc/a/a;

    invoke-direct {v2}, Lcom/msc/a/a;-><init>()V

    .line 5637
    iget-object v3, p0, Lcom/osp/app/signin/jv;->g:Lcom/osp/app/signin/SignInView;

    invoke-virtual {v0}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v2, v3, v0, v1}, Lcom/msc/a/a;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)V

    .line 5639
    invoke-direct {p0, v2}, Lcom/osp/app/signin/jv;->a(Lcom/msc/a/a;)V
    :try_end_10
    .catch Ljava/lang/Exception; {:try_start_10 .. :try_end_10} :catch_4
    .catchall {:try_start_10 .. :try_end_10} :catchall_0

    goto/16 :goto_0

    .line 5645
    :catch_4
    move-exception v0

    :try_start_11
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V
    :try_end_11
    .catchall {:try_start_11 .. :try_end_11} :catchall_0

    goto/16 :goto_0

    .line 5632
    :cond_11
    :try_start_12
    iget-object v0, p0, Lcom/osp/app/signin/jv;->g:Lcom/osp/app/signin/SignInView;

    const v1, 0x7f0c0121

    invoke-virtual {v0, v1}, Lcom/osp/app/signin/SignInView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/EditText;

    .line 5633
    iget-object v1, p0, Lcom/osp/app/signin/jv;->g:Lcom/osp/app/signin/SignInView;

    const v2, 0x7f0c0122

    invoke-virtual {v1, v2}, Lcom/osp/app/signin/SignInView;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/EditText;

    goto :goto_2

    .line 5642
    :cond_12
    new-instance v0, Lcom/msc/c/f;

    const-string v1, "ERR_0000"

    const-string v2, "fail"

    invoke-direct {v0, v1, v2}, Lcom/msc/c/f;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    iput-object v0, p0, Lcom/osp/app/signin/jv;->b:Lcom/msc/c/f;
    :try_end_12
    .catch Ljava/lang/Exception; {:try_start_12 .. :try_end_12} :catch_4
    .catchall {:try_start_12 .. :try_end_12} :catchall_0

    goto/16 :goto_0

    .line 5651
    :cond_13
    :try_start_13
    iget-wide v4, p0, Lcom/osp/app/signin/jv;->t:J
    :try_end_13
    .catchall {:try_start_13 .. :try_end_13} :catchall_0

    cmp-long v3, v0, v4

    if-nez v3, :cond_16

    .line 5655
    :try_start_14
    invoke-static {}, Lcom/msc/c/h;->a()Lcom/msc/c/h;

    invoke-static {v2}, Lcom/msc/c/h;->w(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 5656
    if-eqz v0, :cond_15

    .line 5658
    invoke-static {}, Lcom/osp/common/util/i;->a()Lcom/osp/common/util/i;

    iget-object v1, p0, Lcom/osp/app/signin/jv;->g:Lcom/osp/app/signin/SignInView;

    invoke-static {v1}, Lcom/osp/common/util/i;->o(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v1

    .line 5659
    invoke-static {}, Lcom/osp/common/util/i;->a()Lcom/osp/common/util/i;

    iget-object v2, p0, Lcom/osp/app/signin/jv;->g:Lcom/osp/app/signin/SignInView;

    invoke-static {v1}, Lcom/osp/common/util/i;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 5660
    if-eqz v2, :cond_14

    invoke-virtual {v2, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_14

    .line 5662
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v3, "GetMyCountryZoneTask countryCodeFromNetworkMcc = ["

    invoke-direct {v0, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, "]"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;)V

    .line 5663
    iget-object v0, p0, Lcom/osp/app/signin/jv;->g:Lcom/osp/app/signin/SignInView;

    invoke-static {v0, v1}, Lcom/osp/app/signin/SignInView;->d(Lcom/osp/app/signin/SignInView;Ljava/lang/String;)Ljava/lang/String;
    :try_end_14
    .catch Ljava/lang/Exception; {:try_start_14 .. :try_end_14} :catch_5
    .catchall {:try_start_14 .. :try_end_14} :catchall_0

    goto/16 :goto_0

    .line 5673
    :catch_5
    move-exception v0

    .line 5675
    :try_start_15
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v1, "GetMyCountryZoneTask fail"

    invoke-static {v1}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;)V

    .line 5676
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    .line 5677
    iget-object v0, p0, Lcom/osp/app/signin/jv;->g:Lcom/osp/app/signin/SignInView;

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lcom/osp/app/signin/SignInView;->d(Lcom/osp/app/signin/SignInView;Ljava/lang/String;)Ljava/lang/String;
    :try_end_15
    .catchall {:try_start_15 .. :try_end_15} :catchall_0

    goto/16 :goto_0

    .line 5666
    :cond_14
    :try_start_16
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "GetMyCountryZoneTask countryCode = ["

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "]"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;)V

    .line 5667
    iget-object v1, p0, Lcom/osp/app/signin/jv;->g:Lcom/osp/app/signin/SignInView;

    invoke-static {}, Lcom/osp/common/util/i;->a()Lcom/osp/common/util/i;

    iget-object v2, p0, Lcom/osp/app/signin/jv;->g:Lcom/osp/app/signin/SignInView;

    sget-object v2, Ljava/util/Locale;->ENGLISH:Ljava/util/Locale;

    invoke-virtual {v0, v2}, Ljava/lang/String;->toLowerCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/osp/common/util/i;->c(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Lcom/osp/app/signin/SignInView;->d(Lcom/osp/app/signin/SignInView;Ljava/lang/String;)Ljava/lang/String;

    goto/16 :goto_0

    .line 5671
    :cond_15
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v0, "SIV"

    const-string v1, "GetMyCountryZoneTask Success. but countryCode is null!!"

    invoke-static {v0, v1}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_16
    .catch Ljava/lang/Exception; {:try_start_16 .. :try_end_16} :catch_5
    .catchall {:try_start_16 .. :try_end_16} :catchall_0

    goto/16 :goto_0

    .line 5679
    :cond_16
    :try_start_17
    iget-wide v4, p0, Lcom/osp/app/signin/jv;->u:J
    :try_end_17
    .catchall {:try_start_17 .. :try_end_17} :catchall_0

    cmp-long v0, v0, v4

    if-nez v0, :cond_0

    .line 5683
    :try_start_18
    iget-object v0, p0, Lcom/osp/app/signin/jv;->g:Lcom/osp/app/signin/SignInView;

    invoke-static {}, Lcom/msc/c/h;->a()Lcom/msc/c/h;

    invoke-static {v2}, Lcom/msc/c/h;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/osp/app/signin/SignInView;->f(Lcom/osp/app/signin/SignInView;Ljava/lang/String;)Ljava/lang/String;

    .line 5684
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/osp/app/signin/jv;->k:Z
    :try_end_18
    .catch Ljava/lang/Exception; {:try_start_18 .. :try_end_18} :catch_6
    .catchall {:try_start_18 .. :try_end_18} :catchall_0

    goto/16 :goto_0

    .line 5686
    :catch_6
    move-exception v0

    .line 5688
    :try_start_19
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    .line 5689
    new-instance v1, Lcom/msc/c/f;

    invoke-direct {v1, v0}, Lcom/msc/c/f;-><init>(Ljava/lang/Exception;)V

    iput-object v1, p0, Lcom/osp/app/signin/jv;->b:Lcom/msc/c/f;
    :try_end_19
    .catchall {:try_start_19 .. :try_end_19} :catchall_0

    goto/16 :goto_0
.end method

.method protected final a(Ljava/lang/Boolean;)V
    .locals 4

    .prologue
    .line 5364
    :try_start_0
    invoke-static {}, Lcom/osp/device/b;->b()Lcom/osp/device/b;

    move-result-object v0

    invoke-virtual {v0}, Lcom/osp/device/b;->a()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 5366
    iget-object v0, p0, Lcom/osp/app/signin/jv;->g:Lcom/osp/app/signin/SignInView;

    invoke-static {v0}, Lcom/osp/app/signin/SignInView;->Q(Lcom/osp/app/signin/SignInView;)Lcom/msc/a/d;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 5368
    iget-object v0, p0, Lcom/osp/app/signin/jv;->g:Lcom/osp/app/signin/SignInView;

    invoke-static {v0}, Lcom/osp/app/signin/SignInView;->Q(Lcom/osp/app/signin/SignInView;)Lcom/msc/a/d;

    move-result-object v0

    invoke-virtual {v0}, Lcom/msc/a/d;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 5371
    iget-object v0, p0, Lcom/osp/app/signin/jv;->g:Lcom/osp/app/signin/SignInView;

    const/4 v1, 0x2

    const/4 v2, 0x0

    iget-object v3, p0, Lcom/osp/app/signin/jv;->g:Lcom/osp/app/signin/SignInView;

    iget-object v3, v3, Lcom/osp/app/signin/SignInView;->d:Landroid/content/DialogInterface$OnCancelListener;

    invoke-virtual {v0, v1, v2, v3}, Lcom/osp/app/signin/SignInView;->a(ILandroid/content/DialogInterface$OnDismissListener;Landroid/content/DialogInterface$OnCancelListener;)V

    .line 5372
    iget-object v0, p0, Lcom/osp/app/signin/jv;->g:Lcom/osp/app/signin/SignInView;

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lcom/osp/app/signin/SignInView;->a(Lcom/osp/app/signin/SignInView;Lcom/msc/a/d;)Lcom/msc/a/d;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 5433
    invoke-super {p0, p1}, Lcom/msc/c/b;->a(Ljava/lang/Boolean;)V

    .line 5434
    :goto_0
    return-void

    .line 5374
    :cond_0
    :try_start_1
    const-string v0, "S"

    iget-object v1, p0, Lcom/osp/app/signin/jv;->g:Lcom/osp/app/signin/SignInView;

    invoke-static {v1}, Lcom/osp/app/signin/SignInView;->Q(Lcom/osp/app/signin/SignInView;)Lcom/msc/a/d;

    move-result-object v1

    invoke-virtual {v1}, Lcom/msc/a/d;->b()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 5379
    iget-object v0, p0, Lcom/osp/app/signin/jv;->g:Lcom/osp/app/signin/SignInView;

    iget-object v1, p0, Lcom/osp/app/signin/jv;->g:Lcom/osp/app/signin/SignInView;

    invoke-static {v1}, Lcom/osp/app/signin/SignInView;->Q(Lcom/osp/app/signin/SignInView;)Lcom/msc/a/d;

    move-result-object v1

    invoke-virtual {v1}, Lcom/msc/a/d;->i()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/osp/app/signin/SignInView;->b(Lcom/osp/app/signin/SignInView;Ljava/lang/String;)Ljava/lang/String;

    .line 5380
    iget-object v0, p0, Lcom/osp/app/signin/jv;->g:Lcom/osp/app/signin/SignInView;

    iget-object v1, p0, Lcom/osp/app/signin/jv;->g:Lcom/osp/app/signin/SignInView;

    invoke-static {v1}, Lcom/osp/app/signin/SignInView;->Q(Lcom/osp/app/signin/SignInView;)Lcom/msc/a/d;

    move-result-object v1

    invoke-virtual {v1}, Lcom/msc/a/d;->e()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/osp/app/signin/SignInView;->e(Lcom/osp/app/signin/SignInView;Ljava/lang/String;)Ljava/lang/String;

    .line 5382
    iget-object v0, p0, Lcom/osp/app/signin/jv;->g:Lcom/osp/app/signin/SignInView;

    const/4 v1, 0x3

    const/4 v2, 0x0

    iget-object v3, p0, Lcom/osp/app/signin/jv;->g:Lcom/osp/app/signin/SignInView;

    iget-object v3, v3, Lcom/osp/app/signin/SignInView;->e:Landroid/content/DialogInterface$OnCancelListener;

    invoke-virtual {v0, v1, v2, v3}, Lcom/osp/app/signin/SignInView;->a(ILandroid/content/DialogInterface$OnDismissListener;Landroid/content/DialogInterface$OnCancelListener;)V

    .line 5383
    iget-object v0, p0, Lcom/osp/app/signin/jv;->g:Lcom/osp/app/signin/SignInView;

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lcom/osp/app/signin/SignInView;->a(Lcom/osp/app/signin/SignInView;Lcom/msc/a/d;)Lcom/msc/a/d;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 5433
    invoke-super {p0, p1}, Lcom/msc/c/b;->a(Ljava/lang/Boolean;)V

    goto :goto_0

    .line 5388
    :cond_1
    :try_start_2
    iget-object v0, p0, Lcom/osp/app/signin/jv;->b:Lcom/msc/c/f;

    if-eqz v0, :cond_a

    .line 5390
    iget-object v0, p0, Lcom/osp/app/signin/jv;->g:Lcom/osp/app/signin/SignInView;

    invoke-static {v0}, Lcom/osp/app/signin/SignInView;->C(Lcom/osp/app/signin/SignInView;)Z

    move-result v0

    if-eqz v0, :cond_4

    iget-object v0, p0, Lcom/osp/app/signin/jv;->g:Lcom/osp/app/signin/SignInView;

    invoke-static {v0}, Lcom/osp/app/signin/SignInView;->D(Lcom/osp/app/signin/SignInView;)Z

    move-result v0

    if-nez v0, :cond_4

    const-string v0, "USR_3113"

    iget-object v1, p0, Lcom/osp/app/signin/jv;->b:Lcom/msc/c/f;

    invoke-virtual {v1}, Lcom/msc/c/f;->a()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 5392
    iget-object v0, p0, Lcom/osp/app/signin/jv;->g:Lcom/osp/app/signin/SignInView;

    iget-object v1, p0, Lcom/osp/app/signin/jv;->g:Lcom/osp/app/signin/SignInView;

    invoke-static {v0, v1}, Lcom/osp/app/signin/SignInView;->a(Lcom/osp/app/signin/SignInView;Landroid/content/Context;)V

    .line 5404
    :goto_1
    iget-boolean v0, p0, Lcom/osp/app/signin/jv;->s:Z

    if-eqz v0, :cond_2

    .line 5406
    iget-object v0, p0, Lcom/osp/app/signin/jv;->g:Lcom/osp/app/signin/SignInView;

    invoke-static {v0}, Lcom/osp/app/signin/SignInView;->R(Lcom/osp/app/signin/SignInView;)V

    .line 5409
    :cond_2
    iget-boolean v0, p0, Lcom/osp/app/signin/jv;->v:Z

    if-eqz v0, :cond_3

    .line 5411
    iget-object v0, p0, Lcom/osp/app/signin/jv;->g:Lcom/osp/app/signin/SignInView;

    invoke-static {v0}, Lcom/osp/app/signin/SignInView;->S(Lcom/osp/app/signin/SignInView;)V

    .line 5414
    :cond_3
    iget-object v0, p0, Lcom/osp/app/signin/jv;->g:Lcom/osp/app/signin/SignInView;

    invoke-static {v0}, Lcom/osp/app/signin/SignInView;->N(Lcom/osp/app/signin/SignInView;)Z

    move-result v0

    if-nez v0, :cond_8

    const-string v0, "AUT_1885"

    iget-object v1, p0, Lcom/osp/app/signin/jv;->b:Lcom/msc/c/f;

    invoke-virtual {v1}, Lcom/msc/c/f;->a()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_8

    .line 5416
    iget-object v0, p0, Lcom/osp/app/signin/jv;->g:Lcom/osp/app/signin/SignInView;

    const/4 v1, 0x4

    const/4 v2, 0x0

    iget-object v3, p0, Lcom/osp/app/signin/jv;->g:Lcom/osp/app/signin/SignInView;

    iget-object v3, v3, Lcom/osp/app/signin/SignInView;->f:Landroid/content/DialogInterface$OnCancelListener;

    invoke-virtual {v0, v1, v2, v3}, Lcom/osp/app/signin/SignInView;->a(ILandroid/content/DialogInterface$OnDismissListener;Landroid/content/DialogInterface$OnCancelListener;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 5433
    invoke-super {p0, p1}, Lcom/msc/c/b;->a(Ljava/lang/Boolean;)V

    goto/16 :goto_0

    .line 5393
    :cond_4
    :try_start_3
    iget-object v0, p0, Lcom/osp/app/signin/jv;->g:Lcom/osp/app/signin/SignInView;

    invoke-static {v0}, Lcom/osp/app/signin/SignInView;->C(Lcom/osp/app/signin/SignInView;)Z

    move-result v0

    if-eqz v0, :cond_5

    iget-object v0, p0, Lcom/osp/app/signin/jv;->g:Lcom/osp/app/signin/SignInView;

    invoke-static {v0}, Lcom/osp/app/signin/SignInView;->D(Lcom/osp/app/signin/SignInView;)Z

    move-result v0

    if-nez v0, :cond_5

    const-string v0, "USR_3192"

    iget-object v1, p0, Lcom/osp/app/signin/jv;->b:Lcom/msc/c/f;

    invoke-virtual {v1}, Lcom/msc/c/f;->a()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_5

    .line 5395
    iget-object v0, p0, Lcom/osp/app/signin/jv;->g:Lcom/osp/app/signin/SignInView;

    iget-object v1, p0, Lcom/osp/app/signin/jv;->g:Lcom/osp/app/signin/SignInView;

    invoke-static {v0, v1}, Lcom/osp/app/signin/SignInView;->b(Lcom/osp/app/signin/SignInView;Landroid/content/Context;)V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    goto :goto_1

    .line 5433
    :catchall_0
    move-exception v0

    invoke-super {p0, p1}, Lcom/msc/c/b;->a(Ljava/lang/Boolean;)V

    throw v0

    .line 5396
    :cond_5
    :try_start_4
    const-string v0, "SOCKET_TIMEOUT_EXCEPTION"

    iget-object v1, p0, Lcom/osp/app/signin/jv;->b:Lcom/msc/c/f;

    invoke-virtual {v1}, Lcom/msc/c/f;->b()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_6

    const-string v0, "CONNECT_TIMEOUT_EXCEPTION"

    iget-object v1, p0, Lcom/osp/app/signin/jv;->b:Lcom/msc/c/f;

    invoke-virtual {v1}, Lcom/msc/c/f;->b()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_7

    .line 5398
    :cond_6
    iget-object v0, p0, Lcom/osp/app/signin/jv;->g:Lcom/osp/app/signin/SignInView;

    const/4 v1, 0x3

    iget-object v2, p0, Lcom/osp/app/signin/jv;->g:Lcom/osp/app/signin/SignInView;

    invoke-static {v2}, Lcom/osp/app/signin/SignInView;->c(Lcom/osp/app/signin/SignInView;)Landroid/content/Intent;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/osp/app/signin/SignInView;->a(ILandroid/content/Intent;)V

    goto/16 :goto_1

    .line 5401
    :cond_7
    iget-object v0, p0, Lcom/osp/app/signin/jv;->g:Lcom/osp/app/signin/SignInView;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/osp/app/signin/SignInView;->b(I)V

    goto/16 :goto_1

    .line 5420
    :cond_8
    iget-object v0, p0, Lcom/osp/app/signin/jv;->g:Lcom/osp/app/signin/SignInView;

    invoke-static {v0}, Lcom/osp/app/signin/SignInView;->N(Lcom/osp/app/signin/SignInView;)Z

    move-result v0

    if-eqz v0, :cond_9

    invoke-static {}, Lcom/osp/app/signin/gv;->a()Lcom/osp/app/signin/gs;

    move-result-object v0

    iget-object v1, p0, Lcom/osp/app/signin/jv;->g:Lcom/osp/app/signin/SignInView;

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Lcom/osp/app/signin/gs;->a(Landroid/content/Context;Lcom/osp/app/signin/gu;)Z
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    move-result v0

    if-eqz v0, :cond_9

    .line 5433
    invoke-super {p0, p1}, Lcom/msc/c/b;->a(Ljava/lang/Boolean;)V

    goto/16 :goto_0

    .line 5426
    :cond_9
    :try_start_5
    const-string v0, "Fail"

    invoke-direct {p0, v0}, Lcom/osp/app/signin/jv;->c(Ljava/lang/String;)V
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    .line 5433
    invoke-super {p0, p1}, Lcom/msc/c/b;->a(Ljava/lang/Boolean;)V

    goto/16 :goto_0

    .line 5430
    :cond_a
    :try_start_6
    iget-object v0, p0, Lcom/osp/app/signin/jv;->m:Ljava/lang/String;

    invoke-direct {p0, v0}, Lcom/osp/app/signin/jv;->c(Ljava/lang/String;)V
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_0

    .line 5433
    invoke-super {p0, p1}, Lcom/msc/c/b;->a(Ljava/lang/Boolean;)V

    goto/16 :goto_0
.end method

.method public final b(Lcom/msc/b/c;)V
    .locals 10

    .prologue
    const v9, 0x7f0c0129

    const v8, 0x7f0c0121

    const/4 v7, 0x0

    const/4 v6, 0x1

    .line 5696
    invoke-super {p0, p1}, Lcom/msc/c/b;->b(Lcom/msc/b/c;)V

    .line 5698
    if-nez p1, :cond_1

    .line 5818
    :cond_0
    :goto_0
    return-void

    .line 5703
    :cond_1
    invoke-virtual {p1}, Lcom/msc/b/c;->a()J

    move-result-wide v0

    .line 5704
    invoke-virtual {p1}, Lcom/msc/b/c;->e()Ljava/lang/String;

    move-result-object v2

    .line 5705
    invoke-virtual {p1}, Lcom/msc/b/c;->f()Ljava/lang/Exception;

    move-result-object v3

    .line 5707
    iget-wide v4, p0, Lcom/osp/app/signin/jv;->o:J

    cmp-long v4, v0, v4

    if-nez v4, :cond_7

    .line 5709
    if-eqz v3, :cond_2

    .line 5711
    iget-object v0, p0, Lcom/osp/app/signin/jv;->g:Lcom/osp/app/signin/SignInView;

    invoke-virtual {v0, v6}, Lcom/osp/app/signin/SignInView;->b(I)V

    goto :goto_0

    .line 5712
    :cond_2
    if-eqz v2, :cond_0

    .line 5715
    iget-object v0, p0, Lcom/osp/app/signin/jv;->b:Lcom/msc/c/f;

    invoke-virtual {v0}, Lcom/msc/c/f;->a()Ljava/lang/String;

    move-result-object v0

    .line 5717
    const-string v1, "SSO_2012"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_3

    const-string v1, "SSO_2018"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_5

    .line 5719
    :cond_3
    invoke-virtual {p0}, Lcom/osp/app/signin/jv;->isCancelled()Z

    move-result v0

    if-nez v0, :cond_0

    .line 5723
    iget-object v0, p0, Lcom/osp/app/signin/jv;->g:Lcom/osp/app/signin/SignInView;

    invoke-static {v0}, Lcom/osp/app/signin/SignInView;->e(Lcom/osp/app/signin/SignInView;)Z

    move-result v0

    if-eqz v0, :cond_4

    iget-object v0, p0, Lcom/osp/app/signin/jv;->g:Lcom/osp/app/signin/SignInView;

    invoke-static {v0}, Lcom/osp/app/signin/SignInView;->s(Lcom/osp/app/signin/SignInView;)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v9}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/EditText;

    iget-object v1, p0, Lcom/osp/app/signin/jv;->g:Lcom/osp/app/signin/SignInView;

    invoke-static {v1}, Lcom/osp/app/signin/SignInView;->s(Lcom/osp/app/signin/SignInView;)Landroid/view/View;

    move-result-object v1

    const v2, 0x7f0c012a

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/EditText;

    :goto_1
    invoke-static {}, Lcom/msc/c/g;->a()Lcom/msc/c/g;

    iget-object v2, p0, Lcom/osp/app/signin/jv;->g:Lcom/osp/app/signin/SignInView;

    invoke-virtual {v0}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v2, v0, v1, p0}, Lcom/msc/c/g;->b(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Lcom/msc/b/h;)J

    move-result-wide v0

    iput-wide v0, p0, Lcom/osp/app/signin/jv;->p:J

    iget-wide v0, p0, Lcom/osp/app/signin/jv;->p:J

    invoke-virtual {p0, v0, v1}, Lcom/osp/app/signin/jv;->a(J)V

    iget-wide v0, p0, Lcom/osp/app/signin/jv;->p:J

    const-string v2, "verify_xml"

    invoke-virtual {p0, v0, v1, v2}, Lcom/osp/app/signin/jv;->a(JLjava/lang/String;)V

    iget-wide v0, p0, Lcom/osp/app/signin/jv;->p:J

    sget-object v2, Lcom/msc/b/g;->c:Lcom/msc/b/g;

    invoke-static {v0, v1, v2}, Lcom/msc/c/g;->a(JLcom/msc/b/g;)V

    goto/16 :goto_0

    :cond_4
    iget-object v0, p0, Lcom/osp/app/signin/jv;->g:Lcom/osp/app/signin/SignInView;

    invoke-virtual {v0, v8}, Lcom/osp/app/signin/SignInView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/EditText;

    iget-object v1, p0, Lcom/osp/app/signin/jv;->g:Lcom/osp/app/signin/SignInView;

    const v2, 0x7f0c0122

    invoke-virtual {v1, v2}, Lcom/osp/app/signin/SignInView;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/EditText;

    goto :goto_1

    .line 5724
    :cond_5
    const-string v1, "SSO_8005"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-ne v1, v6, :cond_6

    .line 5728
    :try_start_0
    new-instance v0, Lcom/osp/security/time/a;

    iget-object v1, p0, Lcom/osp/app/signin/jv;->g:Lcom/osp/app/signin/SignInView;

    invoke-virtual {v1}, Lcom/osp/app/signin/SignInView;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/osp/security/time/a;-><init>(Landroid/content/Context;)V

    .line 5729
    invoke-virtual {v0}, Lcom/osp/security/time/a;->b()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto/16 :goto_0

    .line 5730
    :catch_0
    move-exception v0

    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    goto/16 :goto_0

    .line 5734
    :cond_6
    const-string v1, "SSO_2204"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-ne v0, v6, :cond_0

    .line 5736
    iput-boolean v6, p0, Lcom/osp/app/signin/jv;->v:Z

    goto/16 :goto_0

    .line 5746
    :cond_7
    iget-wide v4, p0, Lcom/osp/app/signin/jv;->q:J

    cmp-long v3, v0, v4

    if-nez v3, :cond_c

    .line 5748
    if-eqz v2, :cond_0

    .line 5750
    const-string v0, "AUT_1805"

    iget-object v1, p0, Lcom/osp/app/signin/jv;->b:Lcom/msc/c/f;

    invoke-virtual {v1}, Lcom/msc/c/f;->a()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_b

    .line 5752
    iget-object v0, p0, Lcom/osp/app/signin/jv;->g:Lcom/osp/app/signin/SignInView;

    invoke-static {v0}, Lcom/osp/app/signin/SignInView;->e(Lcom/osp/app/signin/SignInView;)Z

    move-result v0

    if-eqz v0, :cond_9

    .line 5756
    iget-object v0, p0, Lcom/osp/app/signin/jv;->g:Lcom/osp/app/signin/SignInView;

    invoke-static {v0}, Lcom/osp/app/signin/SignInView;->s(Lcom/osp/app/signin/SignInView;)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v9}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/EditText;

    .line 5761
    :goto_2
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    sget-object v2, Ljava/util/Locale;->ENGLISH:Ljava/util/Locale;

    invoke-virtual {v0, v2}, Ljava/lang/String;->toLowerCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "/"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/osp/app/signin/jv;->g:Lcom/osp/app/signin/SignInView;

    invoke-static {v1}, Lcom/osp/app/signin/SignInView;->j(Lcom/osp/app/signin/SignInView;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 5763
    iget-object v1, p0, Lcom/osp/app/signin/jv;->g:Lcom/osp/app/signin/SignInView;

    invoke-static {v1}, Lcom/osp/app/signin/SignInView;->T(Lcom/osp/app/signin/SignInView;)Landroid/content/SharedPreferences;

    move-result-object v1

    if-nez v1, :cond_8

    .line 5765
    iget-object v1, p0, Lcom/osp/app/signin/jv;->g:Lcom/osp/app/signin/SignInView;

    iget-object v2, p0, Lcom/osp/app/signin/jv;->g:Lcom/osp/app/signin/SignInView;

    const-string v3, "VERIFY_COUNT"

    invoke-virtual {v2, v3, v7}, Lcom/osp/app/signin/SignInView;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/osp/app/signin/SignInView;->a(Lcom/osp/app/signin/SignInView;Landroid/content/SharedPreferences;)Landroid/content/SharedPreferences;

    .line 5766
    iget-object v1, p0, Lcom/osp/app/signin/jv;->g:Lcom/osp/app/signin/SignInView;

    invoke-static {v1}, Lcom/osp/app/signin/SignInView;->T(Lcom/osp/app/signin/SignInView;)Landroid/content/SharedPreferences;

    move-result-object v1

    invoke-interface {v1}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v1

    .line 5767
    invoke-interface {v1, v0, v7}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    .line 5768
    invoke-interface {v1}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 5770
    :cond_8
    iget-object v1, p0, Lcom/osp/app/signin/jv;->g:Lcom/osp/app/signin/SignInView;

    invoke-static {v1}, Lcom/osp/app/signin/SignInView;->T(Lcom/osp/app/signin/SignInView;)Landroid/content/SharedPreferences;

    move-result-object v1

    invoke-interface {v1, v0, v7}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v1

    .line 5771
    add-int/lit8 v1, v1, 0x1

    .line 5772
    const/4 v2, 0x5

    if-lt v1, v2, :cond_a

    .line 5774
    iget-object v1, p0, Lcom/osp/app/signin/jv;->g:Lcom/osp/app/signin/SignInView;

    invoke-static {v1}, Lcom/osp/app/signin/SignInView;->T(Lcom/osp/app/signin/SignInView;)Landroid/content/SharedPreferences;

    move-result-object v1

    invoke-interface {v1}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v1

    invoke-interface {v1, v0}, Landroid/content/SharedPreferences$Editor;->remove(Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    .line 5775
    iget-object v0, p0, Lcom/osp/app/signin/jv;->g:Lcom/osp/app/signin/SignInView;

    invoke-static {v0}, Lcom/osp/app/signin/SignInView;->T(Lcom/osp/app/signin/SignInView;)Landroid/content/SharedPreferences;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 5777
    iput-boolean v6, p0, Lcom/osp/app/signin/jv;->s:Z

    goto/16 :goto_0

    .line 5759
    :cond_9
    iget-object v0, p0, Lcom/osp/app/signin/jv;->g:Lcom/osp/app/signin/SignInView;

    invoke-virtual {v0, v8}, Lcom/osp/app/signin/SignInView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/EditText;

    goto/16 :goto_2

    .line 5780
    :cond_a
    iget-object v2, p0, Lcom/osp/app/signin/jv;->g:Lcom/osp/app/signin/SignInView;

    invoke-static {v2}, Lcom/osp/app/signin/SignInView;->T(Lcom/osp/app/signin/SignInView;)Landroid/content/SharedPreferences;

    move-result-object v2

    invoke-interface {v2}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v2

    .line 5781
    invoke-interface {v2, v0, v1}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    .line 5782
    invoke-interface {v2}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 5783
    iget-object v0, p0, Lcom/osp/app/signin/jv;->b:Lcom/msc/c/f;

    const-string v1, "AUT_1805"

    invoke-virtual {v0, v1}, Lcom/msc/c/f;->a(Ljava/lang/String;)V

    .line 5784
    iget-object v0, p0, Lcom/osp/app/signin/jv;->b:Lcom/msc/c/f;

    const-string v1, "fail"

    invoke-virtual {v0, v1}, Lcom/msc/c/f;->b(Ljava/lang/String;)V

    goto/16 :goto_0

    .line 5786
    :cond_b
    const-string v0, "AUT_1820"

    iget-object v1, p0, Lcom/osp/app/signin/jv;->b:Lcom/msc/c/f;

    invoke-virtual {v1}, Lcom/msc/c/f;->a()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 5788
    iput-boolean v6, p0, Lcom/osp/app/signin/jv;->v:Z

    goto/16 :goto_0

    .line 5793
    :cond_c
    iget-wide v4, p0, Lcom/osp/app/signin/jv;->r:J

    cmp-long v3, v0, v4

    if-nez v3, :cond_f

    .line 5795
    if-eqz v2, :cond_0

    .line 5797
    const-string v0, "AUT_1805"

    iget-object v1, p0, Lcom/osp/app/signin/jv;->b:Lcom/msc/c/f;

    invoke-virtual {v1}, Lcom/msc/c/f;->a()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_d

    .line 5799
    iget-object v0, p0, Lcom/osp/app/signin/jv;->b:Lcom/msc/c/f;

    const-string v1, "AUT_1805"

    invoke-virtual {v0, v1}, Lcom/msc/c/f;->a(Ljava/lang/String;)V

    goto/16 :goto_0

    .line 5800
    :cond_d
    const-string v0, "AUT_1815"

    iget-object v1, p0, Lcom/osp/app/signin/jv;->b:Lcom/msc/c/f;

    invoke-virtual {v1}, Lcom/msc/c/f;->a()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_e

    .line 5804
    invoke-static {}, Lcom/osp/app/signin/gv;->a()Lcom/osp/app/signin/gs;

    move-result-object v0

    invoke-virtual {v0}, Lcom/osp/app/signin/gs;->a()V

    goto/16 :goto_0

    .line 5807
    :cond_e
    const-string v0, "AUT_1093"

    iget-object v1, p0, Lcom/osp/app/signin/jv;->b:Lcom/msc/c/f;

    invoke-virtual {v1}, Lcom/msc/c/f;->a()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    const-string v0, "AUT_1092"

    iget-object v1, p0, Lcom/osp/app/signin/jv;->b:Lcom/msc/c/f;

    invoke-virtual {v1}, Lcom/msc/c/f;->a()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    goto/16 :goto_0

    .line 5814
    :cond_f
    iget-wide v2, p0, Lcom/osp/app/signin/jv;->t:J

    cmp-long v0, v0, v2

    if-nez v0, :cond_0

    .line 5816
    iget-object v0, p0, Lcom/osp/app/signin/jv;->g:Lcom/osp/app/signin/SignInView;

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lcom/osp/app/signin/SignInView;->d(Lcom/osp/app/signin/SignInView;Ljava/lang/String;)Ljava/lang/String;

    goto/16 :goto_0
.end method

.method protected final b(Ljava/lang/String;)V
    .locals 3

    .prologue
    .line 5952
    invoke-static {}, Lcom/msc/c/g;->a()Lcom/msc/c/g;

    .line 5953
    iget-object v0, p0, Lcom/osp/app/signin/jv;->g:Lcom/osp/app/signin/SignInView;

    iget-object v1, p0, Lcom/osp/app/signin/jv;->g:Lcom/osp/app/signin/SignInView;

    invoke-static {v1}, Lcom/osp/app/signin/SignInView;->j(Lcom/osp/app/signin/SignInView;)Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/osp/app/signin/jv;->g:Lcom/osp/app/signin/SignInView;

    invoke-static {v2}, Lcom/osp/app/signin/SignInView;->l(Lcom/osp/app/signin/SignInView;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v1, p1, v2, p0}, Lcom/msc/c/g;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/msc/b/h;)J

    move-result-wide v0

    iput-wide v0, p0, Lcom/osp/app/signin/jv;->u:J

    .line 5955
    iget-wide v0, p0, Lcom/osp/app/signin/jv;->u:J

    invoke-virtual {p0, v0, v1}, Lcom/osp/app/signin/jv;->b(J)V

    .line 5956
    iget-wide v0, p0, Lcom/osp/app/signin/jv;->u:J

    const-string v2, "from_json"

    invoke-virtual {p0, v0, v1, v2}, Lcom/osp/app/signin/jv;->a(JLjava/lang/String;)V

    .line 5958
    iget-wide v0, p0, Lcom/osp/app/signin/jv;->u:J

    sget-object v2, Lcom/msc/b/g;->b:Lcom/msc/b/g;

    invoke-static {v0, v1, v2}, Lcom/msc/c/g;->a(JLcom/msc/b/g;)V

    .line 5959
    return-void
.end method

.method protected final d()V
    .locals 2

    .prologue
    const/4 v1, 0x1

    .line 5298
    invoke-super {p0}, Lcom/msc/c/b;->d()V

    .line 5299
    iget-object v0, p0, Lcom/osp/app/signin/jv;->e:Ljava/lang/Thread;

    invoke-static {v0}, Lcom/osp/app/signin/jv;->a(Ljava/lang/Thread;)V

    .line 5300
    iget-object v0, p0, Lcom/osp/app/signin/jv;->f:Ljava/lang/Thread;

    invoke-static {v0}, Lcom/osp/app/signin/jv;->a(Ljava/lang/Thread;)V

    .line 5301
    iput-boolean v1, p0, Lcom/osp/app/signin/jv;->h:Z

    .line 5302
    iput-boolean v1, p0, Lcom/osp/app/signin/jv;->i:Z

    .line 5304
    iget-object v0, p0, Lcom/osp/app/signin/jv;->g:Lcom/osp/app/signin/SignInView;

    invoke-static {v0}, Lcom/osp/app/signin/SignInView;->a(Lcom/osp/app/signin/SignInView;)V

    .line 5305
    return-void
.end method

.method protected final synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 5100
    invoke-virtual {p0}, Lcom/osp/app/signin/jv;->a()Ljava/lang/Boolean;

    move-result-object v0

    return-object v0
.end method

.method protected final synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 5100
    check-cast p1, Ljava/lang/Boolean;

    invoke-virtual {p0, p1}, Lcom/osp/app/signin/jv;->a(Ljava/lang/Boolean;)V

    return-void
.end method
