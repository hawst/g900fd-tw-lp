.class public final Lcom/osp/app/signin/gs;
.super Ljava/lang/Object;
.source "ReactivationSendEmailCheckManager.java"


# instance fields
.field private final a:I

.field private b:I

.field private c:Lcom/osp/app/signin/gu;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 19
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 23
    const/4 v0, 0x5

    iput v0, p0, Lcom/osp/app/signin/gs;->a:I

    .line 25
    const/4 v0, 0x0

    iput v0, p0, Lcom/osp/app/signin/gs;->b:I

    .line 27
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/osp/app/signin/gs;->c:Lcom/osp/app/signin/gu;

    .line 82
    return-void
.end method

.method static synthetic a(Lcom/osp/app/signin/gs;)Lcom/osp/app/signin/gu;
    .locals 1

    .prologue
    .line 19
    iget-object v0, p0, Lcom/osp/app/signin/gs;->c:Lcom/osp/app/signin/gu;

    return-object v0
.end method


# virtual methods
.method public final a()V
    .locals 2

    .prologue
    .line 50
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v0, "RSECM"

    const-string v1, "IsCountReachedMax"

    invoke-static {v0, v1}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 52
    iget v0, p0, Lcom/osp/app/signin/gs;->b:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/osp/app/signin/gs;->b:I

    .line 54
    return-void
.end method

.method public final a(Landroid/content/Context;Lcom/osp/app/signin/gu;)Z
    .locals 4

    .prologue
    const/4 v0, 0x0

    .line 58
    iput-object p2, p0, Lcom/osp/app/signin/gs;->c:Lcom/osp/app/signin/gu;

    .line 60
    iget v1, p0, Lcom/osp/app/signin/gs;->b:I

    const/4 v2, 0x5

    if-lt v1, v2, :cond_0

    .line 62
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v1, "RSECM"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "InvalidUserSigninCount = "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v3, p0, Lcom/osp/app/signin/gs;->b:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " Start Task!"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 63
    iput v0, p0, Lcom/osp/app/signin/gs;->b:I

    .line 64
    new-instance v0, Lcom/osp/app/signin/gw;

    invoke-direct {v0, p0, p1}, Lcom/osp/app/signin/gw;-><init>(Lcom/osp/app/signin/gs;Landroid/content/Context;)V

    invoke-virtual {v0}, Lcom/osp/app/signin/gw;->b()V

    .line 65
    const/4 v0, 0x1

    .line 69
    :goto_0
    return v0

    .line 68
    :cond_0
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v1, "RSECM"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "InvalidUserSigninCount = "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v3, p0, Lcom/osp/app/signin/gs;->b:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " Don\'t Start Task!"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method
