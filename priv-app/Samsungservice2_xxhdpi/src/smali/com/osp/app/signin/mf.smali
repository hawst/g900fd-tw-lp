.class final Lcom/osp/app/signin/mf;
.super Ljava/lang/Object;
.source "SignUpView.java"

# interfaces
.implements Landroid/content/DialogInterface$OnClickListener;


# instance fields
.field final synthetic a:Lcom/osp/app/signin/mc;


# direct methods
.method constructor <init>(Lcom/osp/app/signin/mc;)V
    .locals 0

    .prologue
    .line 7921
    iput-object p1, p0, Lcom/osp/app/signin/mf;->a:Lcom/osp/app/signin/mc;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onClick(Landroid/content/DialogInterface;I)V
    .locals 2

    .prologue
    .line 7926
    iget-object v0, p0, Lcom/osp/app/signin/mf;->a:Lcom/osp/app/signin/mc;

    iget-object v0, v0, Lcom/osp/app/signin/mc;->c:Lcom/osp/app/signin/SignUpView;

    invoke-static {v0}, Lcom/osp/app/signin/SignUpView;->d(Lcom/osp/app/signin/SignUpView;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 7928
    iget-object v0, p0, Lcom/osp/app/signin/mf;->a:Lcom/osp/app/signin/mc;

    iget-object v0, v0, Lcom/osp/app/signin/mc;->c:Lcom/osp/app/signin/SignUpView;

    invoke-static {v0}, Lcom/osp/app/signin/SignUpView;->V(Lcom/osp/app/signin/SignUpView;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/osp/app/signin/mf;->a:Lcom/osp/app/signin/mc;

    iget-object v0, v0, Lcom/osp/app/signin/mc;->c:Lcom/osp/app/signin/SignUpView;

    invoke-static {v0}, Lcom/osp/app/signin/SignUpView;->p(Lcom/osp/app/signin/SignUpView;)Landroid/widget/AutoCompleteTextView;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/AutoCompleteTextView;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/osp/app/signin/mf;->a:Lcom/osp/app/signin/mc;

    iget-object v1, v1, Lcom/osp/app/signin/mc;->c:Lcom/osp/app/signin/SignUpView;

    invoke-static {v1}, Lcom/osp/app/signin/SignUpView;->V(Lcom/osp/app/signin/SignUpView;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/osp/app/signin/mf;->a:Lcom/osp/app/signin/mc;

    iget-object v0, v0, Lcom/osp/app/signin/mc;->c:Lcom/osp/app/signin/SignUpView;

    invoke-static {v0}, Lcom/osp/app/signin/SignUpView;->p(Lcom/osp/app/signin/SignUpView;)Landroid/widget/AutoCompleteTextView;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/AutoCompleteTextView;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/osp/app/signin/mf;->a:Lcom/osp/app/signin/mc;

    iget-object v1, v1, Lcom/osp/app/signin/mc;->c:Lcom/osp/app/signin/SignUpView;

    invoke-static {v1}, Lcom/osp/app/signin/SignUpView;->W(Lcom/osp/app/signin/SignUpView;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 7931
    :cond_0
    iget-object v0, p0, Lcom/osp/app/signin/mf;->a:Lcom/osp/app/signin/mc;

    iget-object v0, v0, Lcom/osp/app/signin/mc;->c:Lcom/osp/app/signin/SignUpView;

    invoke-static {v0}, Lcom/osp/app/signin/SignUpView;->X(Lcom/osp/app/signin/SignUpView;)V

    .line 7944
    :goto_0
    return-void

    .line 7932
    :cond_1
    iget-object v0, p0, Lcom/osp/app/signin/mf;->a:Lcom/osp/app/signin/mc;

    iget-object v0, v0, Lcom/osp/app/signin/mc;->c:Lcom/osp/app/signin/SignUpView;

    invoke-static {v0}, Lcom/osp/app/util/r;->p(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/osp/app/signin/mf;->a:Lcom/osp/app/signin/mc;

    iget-object v0, v0, Lcom/osp/app/signin/mc;->c:Lcom/osp/app/signin/SignUpView;

    invoke-static {v0}, Lcom/osp/app/signin/SignUpView;->Y(Lcom/osp/app/signin/SignUpView;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_3

    iget-object v0, p0, Lcom/osp/app/signin/mf;->a:Lcom/osp/app/signin/mc;

    iget-object v0, v0, Lcom/osp/app/signin/mc;->c:Lcom/osp/app/signin/SignUpView;

    invoke-static {v0}, Lcom/osp/app/signin/SignUpView;->p(Lcom/osp/app/signin/SignUpView;)Landroid/widget/AutoCompleteTextView;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/AutoCompleteTextView;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/osp/app/signin/mf;->a:Lcom/osp/app/signin/mc;

    iget-object v1, v1, Lcom/osp/app/signin/mc;->c:Lcom/osp/app/signin/SignUpView;

    invoke-static {v1}, Lcom/osp/app/signin/SignUpView;->Y(Lcom/osp/app/signin/SignUpView;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_2

    iget-object v0, p0, Lcom/osp/app/signin/mf;->a:Lcom/osp/app/signin/mc;

    iget-object v0, v0, Lcom/osp/app/signin/mc;->c:Lcom/osp/app/signin/SignUpView;

    invoke-static {v0}, Lcom/osp/app/signin/SignUpView;->p(Lcom/osp/app/signin/SignUpView;)Landroid/widget/AutoCompleteTextView;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/AutoCompleteTextView;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/osp/app/signin/mf;->a:Lcom/osp/app/signin/mc;

    iget-object v1, v1, Lcom/osp/app/signin/mc;->c:Lcom/osp/app/signin/SignUpView;

    invoke-static {v1}, Lcom/osp/app/signin/SignUpView;->Z(Lcom/osp/app/signin/SignUpView;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 7935
    :cond_2
    iget-object v0, p0, Lcom/osp/app/signin/mf;->a:Lcom/osp/app/signin/mc;

    invoke-static {v0}, Lcom/osp/app/signin/mc;->a(Lcom/osp/app/signin/mc;)V

    goto :goto_0

    .line 7938
    :cond_3
    iget-object v0, p0, Lcom/osp/app/signin/mf;->a:Lcom/osp/app/signin/mc;

    invoke-virtual {v0}, Lcom/osp/app/signin/mc;->h()V

    goto :goto_0

    .line 7942
    :cond_4
    iget-object v0, p0, Lcom/osp/app/signin/mf;->a:Lcom/osp/app/signin/mc;

    invoke-virtual {v0}, Lcom/osp/app/signin/mc;->h()V

    goto :goto_0
.end method
