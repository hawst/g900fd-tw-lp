.class public final Lcom/osp/app/signin/cd;
.super Lcom/msc/c/b;
.source "AgreeDisclaimerTask.java"


# instance fields
.field private c:Z

.field private d:J

.field private e:Z

.field private f:J

.field private g:J

.field private h:J

.field private final i:Ljava/lang/String;

.field private final j:Ljava/lang/String;


# direct methods
.method public constructor <init>(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 64
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/msc/c/b;-><init>(Landroid/content/Context;Z)V

    .line 66
    iput-object p2, p0, Lcom/osp/app/signin/cd;->i:Ljava/lang/String;

    .line 67
    iput-object p3, p0, Lcom/osp/app/signin/cd;->j:Ljava/lang/String;

    .line 70
    return-void
.end method

.method private a(ZLjava/lang/String;)V
    .locals 3

    .prologue
    .line 268
    iput-boolean p1, p0, Lcom/osp/app/signin/cd;->e:Z

    .line 270
    iget-object v0, p0, Lcom/osp/app/signin/cd;->a:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/Context;

    .line 271
    if-nez v0, :cond_0

    .line 292
    :goto_0
    return-void

    .line 276
    :cond_0
    invoke-static {v0}, Lcom/msc/c/e;->g(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v1

    .line 277
    if-nez v1, :cond_1

    .line 279
    invoke-static {v0}, Lcom/osp/app/util/ad;->h(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/osp/app/signin/cd;->b(Ljava/lang/String;)V

    .line 280
    new-instance v0, Lcom/msc/c/f;

    invoke-direct {v0}, Lcom/msc/c/f;-><init>()V

    iput-object v0, p0, Lcom/osp/app/signin/cd;->b:Lcom/msc/c/f;

    goto :goto_0

    .line 284
    :cond_1
    invoke-static {}, Lcom/msc/c/g;->a()Lcom/msc/c/g;

    .line 285
    const/4 v2, 0x0

    invoke-static {v0, p2, v1, v2, p0}, Lcom/msc/c/g;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/msc/b/h;)J

    move-result-wide v0

    iput-wide v0, p0, Lcom/osp/app/signin/cd;->d:J

    .line 287
    iget-wide v0, p0, Lcom/osp/app/signin/cd;->d:J

    invoke-virtual {p0, v0, v1}, Lcom/osp/app/signin/cd;->a(J)V

    .line 288
    iget-wide v0, p0, Lcom/osp/app/signin/cd;->d:J

    const-string v2, "from_json"

    invoke-virtual {p0, v0, v1, v2}, Lcom/osp/app/signin/cd;->a(JLjava/lang/String;)V

    .line 290
    iget-wide v0, p0, Lcom/osp/app/signin/cd;->d:J

    sget-object v2, Lcom/msc/b/g;->b:Lcom/msc/b/g;

    invoke-static {v0, v1, v2}, Lcom/msc/c/g;->a(JLcom/msc/b/g;)V

    goto :goto_0
.end method

.method private b(Ljava/lang/String;)V
    .locals 4

    .prologue
    .line 325
    iget-object v0, p0, Lcom/osp/app/signin/cd;->a:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/Context;

    .line 326
    if-nez v0, :cond_1

    .line 348
    :cond_0
    :goto_0
    return-void

    .line 331
    :cond_1
    instance-of v1, v0, Lcom/osp/app/util/BaseActivity;

    if-eqz v1, :cond_0

    .line 334
    new-instance v1, Landroid/content/Intent;

    const-string v2, "com.msc.action.samsungaccount.resignin"

    invoke-direct {v1, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 335
    const-string v2, "client_id"

    iget-object v3, p0, Lcom/osp/app/signin/cd;->i:Ljava/lang/String;

    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 336
    const-string v2, "client_secret"

    iget-object v3, p0, Lcom/osp/app/signin/cd;->j:Ljava/lang/String;

    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 337
    const-string v2, "OSP_VER"

    const-string v3, "OSP_02"

    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 340
    if-eqz p1, :cond_2

    .line 342
    const-string v2, "email_id"

    invoke-virtual {v1, v2, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 345
    :cond_2
    check-cast v0, Lcom/osp/app/util/BaseActivity;

    invoke-virtual {v0, v1}, Lcom/osp/app/util/BaseActivity;->startActivity(Landroid/content/Intent;)V

    goto :goto_0
.end method


# virtual methods
.method protected final varargs a()Ljava/lang/Boolean;
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 82
    const-string v0, "j5p7ll8g33"

    invoke-direct {p0, v1, v0}, Lcom/osp/app/signin/cd;->a(ZLjava/lang/String;)V

    .line 84
    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    return-object v0
.end method

.method public final declared-synchronized a(Lcom/msc/b/c;)V
    .locals 6

    .prologue
    .line 135
    monitor-enter p0

    :try_start_0
    invoke-super {p0, p1}, Lcom/msc/c/b;->a(Lcom/msc/b/c;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 137
    if-nez p1, :cond_1

    .line 222
    :cond_0
    :goto_0
    monitor-exit p0

    return-void

    .line 142
    :cond_1
    :try_start_1
    invoke-virtual {p1}, Lcom/msc/b/c;->a()J

    move-result-wide v0

    .line 143
    invoke-virtual {p1}, Lcom/msc/b/c;->e()Ljava/lang/String;

    move-result-object v2

    .line 145
    iget-wide v4, p0, Lcom/osp/app/signin/cd;->d:J

    cmp-long v3, v0, v4

    if-nez v3, :cond_2

    .line 147
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "onSuccess() : "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 150
    :try_start_2
    invoke-static {}, Lcom/msc/c/h;->a()Lcom/msc/c/h;

    invoke-static {v2}, Lcom/msc/c/h;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 151
    invoke-virtual {p0}, Lcom/osp/app/signin/cd;->isCancelled()Z

    move-result v0

    if-nez v0, :cond_0

    .line 155
    const-string v3, "j5p7ll8g33"

    const-string v4, "5763D0052DC1462E13751F753384E9A9"

    iget-object v0, p0, Lcom/osp/app/signin/cd;->a:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/Context;

    if-eqz v0, :cond_0

    invoke-static {}, Lcom/msc/c/g;->a()Lcom/msc/c/g;

    const-string v1, "authorization_code"

    move-object v5, p0

    invoke-static/range {v0 .. v5}, Lcom/msc/c/g;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/msc/b/h;)J

    move-result-wide v0

    iput-wide v0, p0, Lcom/osp/app/signin/cd;->f:J

    iget-wide v0, p0, Lcom/osp/app/signin/cd;->f:J

    invoke-virtual {p0, v0, v1}, Lcom/osp/app/signin/cd;->a(J)V

    iget-wide v0, p0, Lcom/osp/app/signin/cd;->f:J

    const-string v2, "from_xml"

    invoke-virtual {p0, v0, v1, v2}, Lcom/osp/app/signin/cd;->a(JLjava/lang/String;)V

    iget-wide v0, p0, Lcom/osp/app/signin/cd;->f:J

    sget-object v2, Lcom/msc/b/g;->c:Lcom/msc/b/g;

    invoke-static {v0, v1, v2}, Lcom/msc/c/g;->a(JLcom/msc/b/g;)V
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_0
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0

    .line 156
    :catch_0
    move-exception v0

    .line 158
    :try_start_3
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    .line 159
    new-instance v1, Lcom/msc/c/f;

    invoke-direct {v1, v0}, Lcom/msc/c/f;-><init>(Ljava/lang/Exception;)V

    iput-object v1, p0, Lcom/osp/app/signin/cd;->b:Lcom/msc/c/f;
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    goto :goto_0

    .line 135
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 161
    :cond_2
    :try_start_4
    iget-wide v4, p0, Lcom/osp/app/signin/cd;->f:J

    cmp-long v3, v0, v4

    if-nez v3, :cond_3

    .line 163
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "onSuccess() : "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;)V
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    .line 166
    :try_start_5
    iget-object v0, p0, Lcom/osp/app/signin/cd;->a:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/Context;

    .line 167
    if-eqz v0, :cond_0

    .line 171
    invoke-static {}, Lcom/msc/c/h;->a()Lcom/msc/c/h;

    invoke-static {v0, v2}, Lcom/msc/c/h;->a(Landroid/content/Context;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 172
    invoke-virtual {p0}, Lcom/osp/app/signin/cd;->isCancelled()Z

    move-result v0

    if-nez v0, :cond_0

    .line 176
    iget-object v0, p0, Lcom/osp/app/signin/cd;->a:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/Context;

    if-eqz v0, :cond_0

    invoke-static {}, Lcom/msc/c/g;->a()Lcom/msc/c/g;

    invoke-static {v0}, Lcom/msc/c/e;->h(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v2

    iget-object v3, p0, Lcom/osp/app/signin/cd;->i:Ljava/lang/String;

    invoke-static {v0, v1, v2, v3, p0}, Lcom/msc/c/g;->c(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/msc/b/h;)J

    move-result-wide v0

    iput-wide v0, p0, Lcom/osp/app/signin/cd;->g:J

    iget-wide v0, p0, Lcom/osp/app/signin/cd;->g:J

    invoke-virtual {p0, v0, v1}, Lcom/osp/app/signin/cd;->a(J)V

    iget-wide v0, p0, Lcom/osp/app/signin/cd;->g:J

    const-string v2, "from_xml"

    invoke-virtual {p0, v0, v1, v2}, Lcom/osp/app/signin/cd;->a(JLjava/lang/String;)V

    iget-wide v0, p0, Lcom/osp/app/signin/cd;->g:J

    sget-object v2, Lcom/msc/b/g;->c:Lcom/msc/b/g;

    invoke-static {v0, v1, v2}, Lcom/msc/c/g;->a(JLcom/msc/b/g;)V
    :try_end_5
    .catch Ljava/lang/Exception; {:try_start_5 .. :try_end_5} :catch_1
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    goto/16 :goto_0

    .line 177
    :catch_1
    move-exception v0

    .line 179
    :try_start_6
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    .line 180
    new-instance v1, Lcom/msc/c/f;

    invoke-direct {v1, v0}, Lcom/msc/c/f;-><init>(Ljava/lang/Exception;)V

    iput-object v1, p0, Lcom/osp/app/signin/cd;->b:Lcom/msc/c/f;

    goto/16 :goto_0

    .line 182
    :cond_3
    iget-wide v4, p0, Lcom/osp/app/signin/cd;->g:J
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_0

    cmp-long v3, v0, v4

    if-nez v3, :cond_4

    .line 186
    :try_start_7
    invoke-static {}, Lcom/msc/c/h;->a()Lcom/msc/c/h;

    invoke-static {v2}, Lcom/msc/c/h;->p(Ljava/lang/String;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/osp/app/signin/cd;->c:Z
    :try_end_7
    .catch Ljava/lang/Exception; {:try_start_7 .. :try_end_7} :catch_2
    .catchall {:try_start_7 .. :try_end_7} :catchall_0

    goto/16 :goto_0

    .line 187
    :catch_2
    move-exception v0

    .line 189
    :try_start_8
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    .line 190
    new-instance v1, Lcom/msc/c/f;

    invoke-direct {v1, v0}, Lcom/msc/c/f;-><init>(Ljava/lang/Exception;)V

    iput-object v1, p0, Lcom/osp/app/signin/cd;->b:Lcom/msc/c/f;

    goto/16 :goto_0

    .line 192
    :cond_4
    iget-wide v4, p0, Lcom/osp/app/signin/cd;->h:J
    :try_end_8
    .catchall {:try_start_8 .. :try_end_8} :catchall_0

    cmp-long v0, v0, v4

    if-nez v0, :cond_0

    .line 196
    :try_start_9
    invoke-static {}, Lcom/msc/c/h;->a()Lcom/msc/c/h;

    invoke-static {v2}, Lcom/msc/c/h;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 197
    if-eqz v1, :cond_5

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v0

    if-lez v0, :cond_5

    .line 199
    iget-object v0, p0, Lcom/osp/app/signin/cd;->a:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/Context;

    .line 201
    if-eqz v0, :cond_0

    .line 205
    invoke-static {v0, v1}, Lcom/msc/c/e;->e(Landroid/content/Context;Ljava/lang/String;)V

    .line 206
    invoke-virtual {p0}, Lcom/osp/app/signin/cd;->isCancelled()Z

    move-result v0

    if-nez v0, :cond_0

    .line 210
    const/4 v0, 0x1

    iget-object v1, p0, Lcom/osp/app/signin/cd;->i:Ljava/lang/String;

    iget-object v2, p0, Lcom/osp/app/signin/cd;->j:Ljava/lang/String;

    invoke-direct {p0, v0, v1}, Lcom/osp/app/signin/cd;->a(ZLjava/lang/String;)V
    :try_end_9
    .catch Ljava/lang/Exception; {:try_start_9 .. :try_end_9} :catch_3
    .catchall {:try_start_9 .. :try_end_9} :catchall_0

    goto/16 :goto_0

    .line 216
    :catch_3
    move-exception v0

    .line 218
    :try_start_a
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    .line 219
    new-instance v1, Lcom/msc/c/f;

    invoke-direct {v1, v0}, Lcom/msc/c/f;-><init>(Ljava/lang/Exception;)V

    iput-object v1, p0, Lcom/osp/app/signin/cd;->b:Lcom/msc/c/f;
    :try_end_a
    .catchall {:try_start_a .. :try_end_a} :catchall_0

    goto/16 :goto_0

    .line 214
    :cond_5
    :try_start_b
    new-instance v0, Lcom/msc/c/f;

    invoke-direct {v0}, Lcom/msc/c/f;-><init>()V

    iput-object v0, p0, Lcom/osp/app/signin/cd;->b:Lcom/msc/c/f;
    :try_end_b
    .catch Ljava/lang/Exception; {:try_start_b .. :try_end_b} :catch_3
    .catchall {:try_start_b .. :try_end_b} :catchall_0

    goto/16 :goto_0
.end method

.method protected final a(Ljava/lang/Boolean;)V
    .locals 6

    .prologue
    const/4 v5, 0x0

    .line 89
    invoke-super {p0, p1}, Lcom/msc/c/b;->a(Ljava/lang/Boolean;)V

    .line 91
    iget-object v0, p0, Lcom/osp/app/signin/cd;->a:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/Context;

    .line 93
    iget-object v1, p0, Lcom/osp/app/signin/cd;->b:Lcom/msc/c/f;

    if-eqz v1, :cond_5

    .line 95
    const-string v1, "AUT_1302"

    iget-object v2, p0, Lcom/osp/app/signin/cd;->b:Lcom/msc/c/f;

    invoke-virtual {v2}, Lcom/msc/c/f;->a()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    const-string v1, "AUT_1830"

    iget-object v2, p0, Lcom/osp/app/signin/cd;->b:Lcom/msc/c/f;

    invoke-virtual {v2}, Lcom/msc/c/f;->a()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 97
    :cond_0
    invoke-static {v0}, Lcom/msc/c/c;->g(Landroid/content/Context;)V

    .line 98
    invoke-static {v0}, Lcom/msc/c/e;->a(Landroid/content/Context;)Z

    .line 99
    invoke-static {v0}, Lcom/osp/app/util/ad;->h(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {p0, v1}, Lcom/osp/app/signin/cd;->b(Ljava/lang/String;)V

    .line 100
    instance-of v1, v0, Lcom/osp/app/util/BaseActivity;

    if-eqz v1, :cond_1

    move-object v1, v0

    .line 102
    check-cast v1, Lcom/osp/app/util/BaseActivity;

    invoke-virtual {v1, v5}, Lcom/osp/app/util/BaseActivity;->b(I)V

    .line 103
    check-cast v0, Landroid/app/Activity;

    invoke-virtual {v0}, Landroid/app/Activity;->finish()V

    .line 131
    :cond_1
    :goto_0
    return-void

    .line 105
    :cond_2
    invoke-static {}, Lcom/osp/device/b;->b()Lcom/osp/device/b;

    move-result-object v1

    invoke-virtual {v1}, Lcom/osp/device/b;->a()Z

    move-result v1

    if-eqz v1, :cond_4

    iget-boolean v1, p0, Lcom/osp/app/signin/cd;->e:Z

    if-nez v1, :cond_4

    iget-object v1, p0, Lcom/osp/app/signin/cd;->b:Lcom/msc/c/f;

    if-eqz v1, :cond_4

    const-string v1, "AUT_1094"

    iget-object v2, p0, Lcom/osp/app/signin/cd;->b:Lcom/msc/c/f;

    invoke-virtual {v2}, Lcom/msc/c/f;->a()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_4

    .line 107
    iget-object v1, p0, Lcom/osp/app/signin/cd;->a:Ljava/lang/ref/WeakReference;

    invoke-virtual {v1}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/content/Context;

    if-eqz v1, :cond_3

    instance-of v2, v1, Lcom/osp/app/util/BaseActivity;

    if-eqz v2, :cond_3

    new-instance v2, Landroid/content/Intent;

    const-string v3, "com.msc.action.samsungaccount.resignin_with_signout"

    invoke-direct {v2, v3}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    const-string v3, "client_id"

    iget-object v4, p0, Lcom/osp/app/signin/cd;->i:Ljava/lang/String;

    invoke-virtual {v2, v3, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    const-string v3, "client_secret"

    iget-object v4, p0, Lcom/osp/app/signin/cd;->j:Ljava/lang/String;

    invoke-virtual {v2, v3, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    const-string v3, "OSP_VER"

    const-string v4, "OSP_02"

    invoke-virtual {v2, v3, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    check-cast v1, Lcom/osp/app/util/BaseActivity;

    invoke-virtual {v1, v2}, Lcom/osp/app/util/BaseActivity;->startActivity(Landroid/content/Intent;)V

    .line 108
    :cond_3
    instance-of v1, v0, Lcom/osp/app/util/BaseActivity;

    if-eqz v1, :cond_1

    move-object v1, v0

    .line 110
    check-cast v1, Lcom/osp/app/util/BaseActivity;

    invoke-virtual {v1, v5}, Lcom/osp/app/util/BaseActivity;->b(I)V

    .line 111
    check-cast v0, Landroid/app/Activity;

    invoke-virtual {v0}, Landroid/app/Activity;->finish()V

    goto :goto_0

    .line 115
    :cond_4
    invoke-virtual {p0, v5}, Lcom/osp/app/signin/cd;->a(Z)V

    goto :goto_0

    .line 119
    :cond_5
    iget-boolean v1, p0, Lcom/osp/app/signin/cd;->c:Z

    if-eqz v1, :cond_6

    .line 121
    instance-of v1, v0, Lcom/osp/app/util/BaseActivity;

    if-eqz v1, :cond_1

    move-object v1, v0

    .line 123
    check-cast v1, Lcom/osp/app/util/BaseActivity;

    const/4 v2, -0x1

    invoke-virtual {v1, v2}, Lcom/osp/app/util/BaseActivity;->b(I)V

    .line 124
    check-cast v0, Landroid/app/Activity;

    invoke-virtual {v0}, Landroid/app/Activity;->finish()V

    goto :goto_0

    .line 129
    :cond_6
    invoke-virtual {p0, v5}, Lcom/osp/app/signin/cd;->a(Z)V

    goto :goto_0
.end method

.method protected final synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 27
    invoke-virtual {p0}, Lcom/osp/app/signin/cd;->a()Ljava/lang/Boolean;

    move-result-object v0

    return-object v0
.end method

.method protected final synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 27
    check-cast p1, Ljava/lang/Boolean;

    invoke-virtual {p0, p1}, Lcom/osp/app/signin/cd;->a(Ljava/lang/Boolean;)V

    return-void
.end method
