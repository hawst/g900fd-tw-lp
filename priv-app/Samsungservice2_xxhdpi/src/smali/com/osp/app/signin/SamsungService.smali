.class public Lcom/osp/app/signin/SamsungService;
.super Landroid/app/Application;
.source "SamsungService.java"


# static fields
.field public static a:I

.field public static b:Lorg/apache/http/client/HttpClient;

.field public static c:Lorg/apache/http/client/HttpClient;
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end field

.field private static d:Lorg/apache/http/client/methods/HttpUriRequest;

.field private static e:Lorg/apache/http/client/methods/HttpUriRequest;

.field private static h:Z

.field private static i:Z

.field private static j:Z

.field private static k:Z

.field private static l:Z

.field private static m:Z

.field private static n:Z

.field private static o:Z

.field private static p:Z

.field private static q:Ljava/lang/String;

.field private static r:Ljava/lang/String;


# instance fields
.field private f:Ljava/lang/ref/WeakReference;

.field private final g:Z

.field private s:Z

.field private final t:Landroid/os/Handler;

.field private final u:Ljava/lang/Runnable;

.field private final v:I


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    const/4 v0, 0x0

    .line 65
    sput-object v1, Lcom/osp/app/signin/SamsungService;->d:Lorg/apache/http/client/methods/HttpUriRequest;

    .line 70
    sput-object v1, Lcom/osp/app/signin/SamsungService;->e:Lorg/apache/http/client/methods/HttpUriRequest;

    .line 88
    sput v0, Lcom/osp/app/signin/SamsungService;->a:I

    .line 95
    sput-object v1, Lcom/osp/app/signin/SamsungService;->b:Lorg/apache/http/client/HttpClient;

    .line 101
    sput-object v1, Lcom/osp/app/signin/SamsungService;->c:Lorg/apache/http/client/HttpClient;

    .line 111
    sput-boolean v0, Lcom/osp/app/signin/SamsungService;->h:Z

    .line 116
    sput-boolean v2, Lcom/osp/app/signin/SamsungService;->i:Z

    .line 121
    sput-boolean v2, Lcom/osp/app/signin/SamsungService;->j:Z

    .line 129
    sput-boolean v0, Lcom/osp/app/signin/SamsungService;->k:Z

    .line 141
    sput-boolean v0, Lcom/osp/app/signin/SamsungService;->m:Z

    .line 145
    sput-boolean v0, Lcom/osp/app/signin/SamsungService;->n:Z

    .line 150
    sput-boolean v0, Lcom/osp/app/signin/SamsungService;->o:Z

    .line 165
    const-string v0, "00"

    sput-object v0, Lcom/osp/app/signin/SamsungService;->r:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 48
    invoke-direct {p0}, Landroid/app/Application;-><init>()V

    .line 106
    iput-boolean v0, p0, Lcom/osp/app/signin/SamsungService;->g:Z

    .line 170
    iput-boolean v0, p0, Lcom/osp/app/signin/SamsungService;->s:Z

    .line 175
    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    iput-object v0, p0, Lcom/osp/app/signin/SamsungService;->t:Landroid/os/Handler;

    .line 180
    new-instance v0, Lcom/osp/app/signin/gx;

    invoke-direct {v0, p0}, Lcom/osp/app/signin/gx;-><init>(Lcom/osp/app/signin/SamsungService;)V

    iput-object v0, p0, Lcom/osp/app/signin/SamsungService;->u:Ljava/lang/Runnable;

    .line 191
    const v0, 0xea60

    iput v0, p0, Lcom/osp/app/signin/SamsungService;->v:I

    .line 1206
    return-void
.end method

.method public static a(Landroid/content/Context;)Ljava/lang/String;
    .locals 6

    .prologue
    const/4 v0, 0x0

    .line 1139
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v1, "IS_USING_CACHE_SIGATURE - true : used cache"

    invoke-static {v1}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;)V

    .line 1145
    if-eqz p0, :cond_3

    .line 1147
    :try_start_0
    invoke-virtual {p0}, Landroid/content/Context;->fileList()[Ljava/lang/String;

    move-result-object v2

    .line 1148
    array-length v3, v2

    const/4 v1, 0x0

    :goto_0
    if-ge v1, v3, :cond_6

    aget-object v4, v2, v1

    .line 1150
    const-string v5, "package_info.xml"

    invoke-virtual {v5, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    const/4 v5, 0x1

    if-ne v4, v5, :cond_1

    .line 1152
    const-string v1, "package_info.xml"

    invoke-virtual {p0, v1}, Landroid/content/Context;->openFileInput(Ljava/lang/String;)Ljava/io/FileInputStream;
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_d
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_7
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v2

    .line 1158
    :goto_1
    if-eqz v2, :cond_4

    .line 1160
    :try_start_1
    new-instance v1, Ljava/io/BufferedReader;

    new-instance v3, Ljava/io/InputStreamReader;

    invoke-direct {v3, v2}, Ljava/io/InputStreamReader;-><init>(Ljava/io/InputStream;)V

    invoke-direct {v1, v3}, Ljava/io/BufferedReader;-><init>(Ljava/io/Reader;)V

    .line 1161
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    .line 1163
    :goto_2
    invoke-virtual {v1}, Ljava/io/BufferedReader;->readLine()Ljava/lang/String;

    move-result-object v4

    if-eqz v4, :cond_2

    .line 1165
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_0
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_c
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    goto :goto_2

    .line 1173
    :catch_0
    move-exception v1

    :goto_3
    :try_start_2
    invoke-virtual {v1}, Ljava/io/IOException;->printStackTrace()V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 1181
    if-eqz v2, :cond_0

    .line 1185
    :try_start_3
    invoke-virtual {v2}, Ljava/io/FileInputStream;->close()V
    :try_end_3
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_5
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_6

    .line 1197
    :cond_0
    :goto_4
    return-object v0

    .line 1148
    :cond_1
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 1168
    :cond_2
    :try_start_4
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
    :try_end_4
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_0
    .catch Ljava/lang/Exception; {:try_start_4 .. :try_end_4} :catch_c
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    move-result-object v0

    .line 1181
    if-eqz v2, :cond_0

    .line 1185
    :try_start_5
    invoke-virtual {v2}, Ljava/io/FileInputStream;->close()V
    :try_end_5
    .catch Ljava/io/IOException; {:try_start_5 .. :try_end_5} :catch_1
    .catch Ljava/lang/Exception; {:try_start_5 .. :try_end_5} :catch_2

    goto :goto_4

    .line 1186
    :catch_1
    move-exception v1

    invoke-virtual {v1}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_4

    .line 1189
    :catch_2
    move-exception v1

    invoke-virtual {v1}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_4

    :cond_3
    move-object v2, v0

    .line 1181
    :cond_4
    if-eqz v2, :cond_0

    .line 1185
    :try_start_6
    invoke-virtual {v2}, Ljava/io/FileInputStream;->close()V
    :try_end_6
    .catch Ljava/io/IOException; {:try_start_6 .. :try_end_6} :catch_3
    .catch Ljava/lang/Exception; {:try_start_6 .. :try_end_6} :catch_4

    goto :goto_4

    .line 1186
    :catch_3
    move-exception v1

    invoke-virtual {v1}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_4

    .line 1189
    :catch_4
    move-exception v1

    invoke-virtual {v1}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_4

    .line 1186
    :catch_5
    move-exception v1

    invoke-virtual {v1}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_4

    .line 1189
    :catch_6
    move-exception v1

    invoke-virtual {v1}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_4

    .line 1176
    :catch_7
    move-exception v1

    move-object v2, v0

    :goto_5
    :try_start_7
    invoke-virtual {v1}, Ljava/lang/Exception;->printStackTrace()V
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_1

    .line 1181
    if-eqz v2, :cond_0

    .line 1185
    :try_start_8
    invoke-virtual {v2}, Ljava/io/FileInputStream;->close()V
    :try_end_8
    .catch Ljava/io/IOException; {:try_start_8 .. :try_end_8} :catch_8
    .catch Ljava/lang/Exception; {:try_start_8 .. :try_end_8} :catch_9

    goto :goto_4

    .line 1186
    :catch_8
    move-exception v1

    invoke-virtual {v1}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_4

    .line 1189
    :catch_9
    move-exception v1

    invoke-virtual {v1}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_4

    .line 1181
    :catchall_0
    move-exception v1

    move-object v2, v0

    move-object v0, v1

    :goto_6
    if-eqz v2, :cond_5

    .line 1185
    :try_start_9
    invoke-virtual {v2}, Ljava/io/FileInputStream;->close()V
    :try_end_9
    .catch Ljava/io/IOException; {:try_start_9 .. :try_end_9} :catch_a
    .catch Ljava/lang/Exception; {:try_start_9 .. :try_end_9} :catch_b

    .line 1192
    :cond_5
    :goto_7
    throw v0

    .line 1186
    :catch_a
    move-exception v1

    invoke-virtual {v1}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_7

    .line 1189
    :catch_b
    move-exception v1

    invoke-virtual {v1}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_7

    .line 1181
    :catchall_1
    move-exception v0

    goto :goto_6

    .line 1176
    :catch_c
    move-exception v1

    goto :goto_5

    .line 1173
    :catch_d
    move-exception v1

    move-object v2, v0

    goto :goto_3

    :cond_6
    move-object v2, v0

    goto/16 :goto_1
.end method

.method public static a(Landroid/content/Context;Ljava/lang/String;)V
    .locals 3

    .prologue
    .line 1086
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v0, "IS_USING_CACHE_SIGATURE - ture : saved cache"

    invoke-static {v0}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;)V

    .line 1088
    const/4 v1, 0x0

    .line 1091
    if-eqz p0, :cond_0

    if-eqz p1, :cond_0

    .line 1094
    :try_start_0
    const-string v0, "package_info.xml"

    const/4 v2, 0x0

    invoke-virtual {p0, v0, v2}, Landroid/content/Context;->openFileOutput(Ljava/lang/String;I)Ljava/io/FileOutputStream;

    move-result-object v1

    .line 1096
    if-eqz v1, :cond_0

    .line 1098
    invoke-virtual {p1}, Ljava/lang/String;->getBytes()[B

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/io/FileOutputStream;->write([B)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1108
    :cond_0
    if-eqz v1, :cond_1

    .line 1112
    :try_start_1
    invoke-virtual {v1}, Ljava/io/FileOutputStream;->close()V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_0

    .line 1120
    :cond_1
    :goto_0
    return-void

    .line 1113
    :catch_0
    move-exception v0

    invoke-virtual {v0}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_0

    .line 1103
    :catch_1
    move-exception v0

    :try_start_2
    invoke-virtual {v0}, Ljava/io/IOException;->printStackTrace()V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 1108
    if-eqz v1, :cond_1

    .line 1112
    :try_start_3
    invoke-virtual {v1}, Ljava/io/FileOutputStream;->close()V
    :try_end_3
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_2

    goto :goto_0

    .line 1113
    :catch_2
    move-exception v0

    invoke-virtual {v0}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_0

    .line 1108
    :catchall_0
    move-exception v0

    if-eqz v1, :cond_2

    .line 1112
    :try_start_4
    invoke-virtual {v1}, Ljava/io/FileOutputStream;->close()V
    :try_end_4
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_3

    .line 1116
    :cond_2
    :goto_1
    throw v0

    .line 1113
    :catch_3
    move-exception v1

    invoke-virtual {v1}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_1
.end method

.method public static a(Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 519
    sput-object p0, Lcom/osp/app/signin/SamsungService;->q:Ljava/lang/String;

    .line 520
    return-void
.end method

.method public static a(Lorg/apache/http/client/HttpClient;)V
    .locals 0

    .prologue
    .line 716
    sput-object p0, Lcom/osp/app/signin/SamsungService;->b:Lorg/apache/http/client/HttpClient;

    .line 717
    return-void
.end method

.method public static a(Lorg/apache/http/client/methods/HttpUriRequest;)V
    .locals 0

    .prologue
    .line 783
    sput-object p0, Lcom/osp/app/signin/SamsungService;->d:Lorg/apache/http/client/methods/HttpUriRequest;

    .line 784
    return-void
.end method

.method public static a(Z)V
    .locals 3

    .prologue
    .line 383
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v0, "SS"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "setSetupWizardMode param = "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 384
    sput-boolean p0, Lcom/osp/app/signin/SamsungService;->l:Z

    .line 385
    return-void
.end method

.method public static a()Z
    .locals 3

    .prologue
    .line 373
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v0, "SS"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "isSetupWizardMode = "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    sget-boolean v2, Lcom/osp/app/signin/SamsungService;->l:Z

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 374
    sget-boolean v0, Lcom/osp/app/signin/SamsungService;->l:Z

    return v0
.end method

.method static synthetic a(Lcom/osp/app/signin/SamsungService;)Z
    .locals 1

    .prologue
    .line 48
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/osp/app/signin/SamsungService;->s:Z

    return v0
.end method

.method public static b(Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 528
    sput-object p0, Lcom/osp/app/signin/SamsungService;->r:Ljava/lang/String;

    .line 529
    return-void
.end method

.method public static b(Z)V
    .locals 0

    .prologue
    .line 452
    sput-boolean p0, Lcom/osp/app/signin/SamsungService;->m:Z

    .line 453
    return-void
.end method

.method public static b()Z
    .locals 3

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 393
    invoke-static {}, Lcom/osp/app/util/ah;->a()Lcom/osp/app/util/ah;

    move-result-object v2

    invoke-virtual {v2}, Lcom/osp/app/util/ah;->u()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 395
    sget-boolean v2, Lcom/osp/app/signin/SamsungService;->n:Z

    if-eqz v2, :cond_0

    .line 397
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "SA Staging Mode of .prop "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-static {}, Lcom/osp/app/util/ah;->a()Lcom/osp/app/util/ah;

    move-result-object v2

    invoke-virtual {v2}, Lcom/osp/app/util/ah;->f()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;)V

    .line 414
    :goto_0
    return v0

    :cond_0
    move v0, v1

    .line 401
    goto :goto_0

    .line 403
    :cond_1
    sget-boolean v2, Lcom/osp/app/signin/SamsungService;->m:Z

    if-eqz v2, :cond_2

    .line 405
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v1, "SA Staging Mode of .INI"

    invoke-static {v1}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;)V

    goto :goto_0

    :cond_2
    move v0, v1

    .line 414
    goto :goto_0
.end method

.method public static c(Z)V
    .locals 0

    .prologue
    .line 501
    sput-boolean p0, Lcom/osp/app/signin/SamsungService;->o:Z

    .line 502
    return-void
.end method

.method public static c()Z
    .locals 1

    .prologue
    .line 424
    sget-boolean v0, Lcom/osp/app/signin/SamsungService;->m:Z

    if-eqz v0, :cond_0

    .line 426
    const/4 v0, 0x1

    .line 428
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static d()V
    .locals 1

    .prologue
    .line 461
    const/4 v0, 0x1

    sput-boolean v0, Lcom/osp/app/signin/SamsungService;->n:Z

    .line 462
    return-void
.end method

.method public static d(Z)V
    .locals 0

    .prologue
    .line 510
    sput-boolean p0, Lcom/osp/app/signin/SamsungService;->p:Z

    .line 511
    return-void
.end method

.method public static e()Z
    .locals 1

    .prologue
    .line 465
    sget-boolean v0, Lcom/osp/app/signin/SamsungService;->o:Z

    return v0
.end method

.method public static f()Z
    .locals 1

    .prologue
    .line 474
    sget-boolean v0, Lcom/osp/app/signin/SamsungService;->p:Z

    return v0
.end method

.method public static g()Ljava/lang/String;
    .locals 1

    .prologue
    .line 483
    sget-object v0, Lcom/osp/app/signin/SamsungService;->q:Ljava/lang/String;

    return-object v0
.end method

.method public static h()Ljava/lang/String;
    .locals 1

    .prologue
    .line 492
    sget-object v0, Lcom/osp/app/signin/SamsungService;->r:Ljava/lang/String;

    return-object v0
.end method

.method public static i()I
    .locals 3

    .prologue
    .line 538
    const v0, 0x7f0200fc

    .line 547
    sget v1, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v2, 0x13

    if-lt v1, v2, :cond_0

    .line 549
    const v0, 0x7f0200fd

    .line 550
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v1, "SS"

    const-string v2, "kitkat noti requested!"

    invoke-static {v1, v2}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 552
    :cond_0
    return v0
.end method

.method public static j()Z
    .locals 1

    .prologue
    .line 723
    sget-boolean v0, Lcom/osp/app/signin/SamsungService;->h:Z

    return v0
.end method

.method public static k()Z
    .locals 1

    .prologue
    .line 733
    sget-boolean v0, Lcom/osp/app/signin/SamsungService;->j:Z

    return v0
.end method

.method public static l()Lorg/apache/http/client/methods/HttpUriRequest;
    .locals 1

    .prologue
    .line 801
    sget-object v0, Lcom/osp/app/signin/SamsungService;->d:Lorg/apache/http/client/methods/HttpUriRequest;

    if-eqz v0, :cond_0

    .line 803
    sget-object v0, Lcom/osp/app/signin/SamsungService;->d:Lorg/apache/http/client/methods/HttpUriRequest;

    .line 806
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static m()V
    .locals 1

    .prologue
    .line 887
    const/4 v0, 0x0

    sput-boolean v0, Lcom/osp/app/signin/SamsungService;->k:Z

    .line 888
    return-void
.end method

.method public static p()Z
    .locals 1

    .prologue
    .line 1067
    sget-boolean v0, Lcom/osp/app/signin/SamsungService;->i:Z

    return v0
.end method


# virtual methods
.method public final a(Landroid/app/Activity;)V
    .locals 1

    .prologue
    .line 1001
    if-nez p1, :cond_0

    .line 1003
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/osp/app/signin/SamsungService;->f:Ljava/lang/ref/WeakReference;

    .line 1008
    :goto_0
    return-void

    .line 1006
    :cond_0
    new-instance v0, Ljava/lang/ref/WeakReference;

    invoke-direct {v0, p1}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    iput-object v0, p0, Lcom/osp/app/signin/SamsungService;->f:Ljava/lang/ref/WeakReference;

    goto :goto_0
.end method

.method public final n()Landroid/app/Activity;
    .locals 2

    .prologue
    .line 1016
    const/4 v0, 0x0

    .line 1017
    iget-object v1, p0, Lcom/osp/app/signin/SamsungService;->f:Ljava/lang/ref/WeakReference;

    if-eqz v1, :cond_0

    .line 1019
    iget-object v0, p0, Lcom/osp/app/signin/SamsungService;->f:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/Activity;

    .line 1021
    :cond_0
    return-object v0
.end method

.method public final o()V
    .locals 1

    .prologue
    .line 1032
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/osp/app/signin/SamsungService;->f:Ljava/lang/ref/WeakReference;

    .line 1033
    return-void
.end method

.method public onCreate()V
    .locals 7

    .prologue
    const/4 v3, 0x0

    .line 557
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v0, "onCreate START"

    invoke-static {v0}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;)V

    .line 573
    :try_start_0
    invoke-static {}, Lcom/osp/app/util/ag;->a()Lcom/osp/app/util/ag;

    const-string v0, "TEST_ENV"

    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->clear()Landroid/content/SharedPreferences$Editor;

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    new-instance v0, Ljava/io/File;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-static {}, Landroid/os/Environment;->getExternalStorageDirectory()Ljava/io/File;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "/SAACT.ini"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    move-result v1

    if-eqz v1, :cond_1

    new-instance v4, Ljava/lang/StringBuffer;

    invoke-direct {v4}, Ljava/lang/StringBuffer;-><init>()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_3

    const/4 v2, 0x0

    :try_start_1
    new-instance v1, Ljava/io/BufferedReader;

    new-instance v5, Ljava/io/FileReader;

    invoke-direct {v5, v0}, Ljava/io/FileReader;-><init>(Ljava/io/File;)V

    invoke-direct {v1, v5}, Ljava/io/BufferedReader;-><init>(Ljava/io/Reader;)V
    :try_end_1
    .catch Ljava/io/FileNotFoundException; {:try_start_1 .. :try_end_1} :catch_9
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    :cond_0
    :goto_0
    :try_start_2
    invoke-virtual {v1}, Ljava/io/BufferedReader;->readLine()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_6

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v2

    if-lez v2, :cond_0

    invoke-virtual {v4, v0}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v2

    const-string v5, "line.separator"

    invoke-static {v5}, Ljava/lang/System;->getProperty(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v2, v5}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    invoke-virtual {v0}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v0

    const-string v2, "="

    invoke-virtual {v0, v2}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v2

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v5

    const/4 v6, 0x0

    invoke-virtual {v0, v6, v2}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v6

    add-int/lit8 v2, v2, 0x1

    invoke-virtual {v0, v2, v5}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v0

    const-string v2, "TEST_ENV"

    const/4 v5, 0x0

    invoke-virtual {p0, v2, v5}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v2

    invoke-interface {v2}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v2

    invoke-interface {v2, v6, v0}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    invoke-interface {v2}, Landroid/content/SharedPreferences$Editor;->commit()Z
    :try_end_2
    .catch Ljava/io/FileNotFoundException; {:try_start_2 .. :try_end_2} :catch_0
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_8
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    goto :goto_0

    :catch_0
    move-exception v0

    :goto_1
    :try_start_3
    invoke-virtual {v0}, Ljava/io/FileNotFoundException;->printStackTrace()V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    if-eqz v1, :cond_1

    :try_start_4
    invoke-virtual {v1}, Ljava/io/BufferedReader;->close()V
    :try_end_4
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_6
    .catch Ljava/lang/Exception; {:try_start_4 .. :try_end_4} :catch_3

    .line 575
    :cond_1
    :goto_2
    :try_start_5
    invoke-static {}, Lcom/osp/app/util/ag;->a()Lcom/osp/app/util/ag;

    invoke-static {p0}, Lcom/osp/app/util/ag;->b(Landroid/content/Context;)V

    .line 578
    invoke-static {}, Lcom/osp/app/util/ah;->a()Lcom/osp/app/util/ah;

    move-result-object v0

    invoke-virtual {v0}, Lcom/osp/app/util/ah;->w()V
    :try_end_5
    .catch Ljava/lang/Exception; {:try_start_5 .. :try_end_5} :catch_3

    .line 587
    :goto_3
    invoke-static {p0}, Lcom/osp/app/util/ad;->d(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 589
    invoke-static {}, Lcom/osp/device/b;->b()Lcom/osp/device/b;

    move-result-object v0

    const-string v1, "true"

    invoke-virtual {v0, p0, v1}, Lcom/osp/device/b;->a(Landroid/content/Context;Ljava/lang/String;)V

    .line 602
    :cond_2
    :try_start_6
    invoke-virtual {p0}, Lcom/osp/app/signin/SamsungService;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v0

    invoke-virtual {p0}, Lcom/osp/app/signin/SamsungService;->getPackageName()Ljava/lang/String;

    move-result-object v1

    const/16 v2, 0x40

    invoke-virtual {v0, v1, v2}, Landroid/content/pm/PackageManager;->getPackageInfo(Ljava/lang/String;I)Landroid/content/pm/PackageInfo;

    move-result-object v0

    iget-object v1, v0, Landroid/content/pm/PackageInfo;->signatures:[Landroid/content/pm/Signature;

    array-length v2, v1

    move v0, v3

    :goto_4
    if-ge v0, v2, :cond_3

    aget-object v3, v1, v0

    invoke-virtual {v3}, Landroid/content/pm/Signature;->hashCode()I

    move-result v3

    const v4, 0x10a6d97e

    if-ne v3, v4, :cond_5

    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v0, "SS"

    const-string v1, "debug mode"

    invoke-static {v0, v1}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;)V

    const/4 v0, 0x1

    sput-boolean v0, Lcom/osp/app/signin/SamsungService;->h:Z
    :try_end_6
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_6 .. :try_end_6} :catch_4

    .line 609
    :cond_3
    :goto_5
    invoke-super {p0}, Landroid/app/Application;->onCreate()V

    .line 611
    invoke-static {}, Lcom/osp/device/b;->b()Lcom/osp/device/b;

    .line 660
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v0, "onCreate END"

    invoke-static {v0}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;)V

    .line 661
    return-void

    .line 573
    :catch_1
    move-exception v0

    move-object v1, v2

    :goto_6
    :try_start_7
    invoke-virtual {v0}, Ljava/io/IOException;->printStackTrace()V
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_1

    if-eqz v1, :cond_1

    :try_start_8
    invoke-virtual {v1}, Ljava/io/BufferedReader;->close()V
    :try_end_8
    .catch Ljava/io/IOException; {:try_start_8 .. :try_end_8} :catch_2
    .catch Ljava/lang/Exception; {:try_start_8 .. :try_end_8} :catch_3

    goto :goto_2

    :catch_2
    move-exception v0

    :try_start_9
    invoke-virtual {v0}, Ljava/io/IOException;->printStackTrace()V
    :try_end_9
    .catch Ljava/lang/Exception; {:try_start_9 .. :try_end_9} :catch_3

    goto :goto_2

    .line 580
    :catch_3
    move-exception v0

    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_3

    .line 573
    :catchall_0
    move-exception v0

    move-object v1, v2

    :goto_7
    if-eqz v1, :cond_4

    :try_start_a
    invoke-virtual {v1}, Ljava/io/BufferedReader;->close()V
    :try_end_a
    .catch Ljava/io/IOException; {:try_start_a .. :try_end_a} :catch_5
    .catch Ljava/lang/Exception; {:try_start_a .. :try_end_a} :catch_3

    :cond_4
    :goto_8
    :try_start_b
    throw v0
    :try_end_b
    .catch Ljava/lang/Exception; {:try_start_b .. :try_end_b} :catch_3

    .line 602
    :cond_5
    add-int/lit8 v0, v0, 0x1

    goto :goto_4

    :catch_4
    move-exception v0

    invoke-virtual {v0}, Landroid/content/pm/PackageManager$NameNotFoundException;->printStackTrace()V

    goto :goto_5

    .line 573
    :catch_5
    move-exception v1

    :try_start_c
    invoke-virtual {v1}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_8

    :catch_6
    move-exception v0

    invoke-virtual {v0}, Ljava/io/IOException;->printStackTrace()V
    :try_end_c
    .catch Ljava/lang/Exception; {:try_start_c .. :try_end_c} :catch_3

    goto/16 :goto_2

    :cond_6
    if-eqz v1, :cond_1

    :try_start_d
    invoke-virtual {v1}, Ljava/io/BufferedReader;->close()V
    :try_end_d
    .catch Ljava/io/IOException; {:try_start_d .. :try_end_d} :catch_7
    .catch Ljava/lang/Exception; {:try_start_d .. :try_end_d} :catch_3

    goto/16 :goto_2

    :catch_7
    move-exception v0

    :try_start_e
    invoke-virtual {v0}, Ljava/io/IOException;->printStackTrace()V
    :try_end_e
    .catch Ljava/lang/Exception; {:try_start_e .. :try_end_e} :catch_3

    goto/16 :goto_2

    :catchall_1
    move-exception v0

    goto :goto_7

    :catch_8
    move-exception v0

    goto :goto_6

    :catch_9
    move-exception v0

    move-object v1, v2

    goto/16 :goto_1
.end method
