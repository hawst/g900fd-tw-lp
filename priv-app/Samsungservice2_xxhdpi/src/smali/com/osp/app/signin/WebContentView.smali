.class public Lcom/osp/app/signin/WebContentView;
.super Lcom/osp/app/util/BaseActivity;
.source "WebContentView.java"


# static fields
.field private static final REQUEST_CODE_UPLOAD_FILES:I = 0x3e9


# instance fields
.field private A:Z

.field private B:Ljava/lang/String;

.field private C:Landroid/app/AlertDialog;

.field private D:Landroid/view/View;

.field private a:Landroid/content/Context;

.field private b:Landroid/app/ProgressDialog;

.field private c:Landroid/webkit/WebView;

.field private d:Landroid/webkit/ValueCallback;

.field private e:Z

.field private f:Z

.field private y:Landroid/content/Intent;

.field private z:Landroid/widget/LinearLayout;


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    const/4 v0, 0x0

    .line 55
    invoke-direct {p0}, Lcom/osp/app/util/BaseActivity;-><init>()V

    .line 64
    iput-object v0, p0, Lcom/osp/app/signin/WebContentView;->a:Landroid/content/Context;

    .line 69
    iput-object v0, p0, Lcom/osp/app/signin/WebContentView;->b:Landroid/app/ProgressDialog;

    .line 74
    iput-object v0, p0, Lcom/osp/app/signin/WebContentView;->c:Landroid/webkit/WebView;

    .line 81
    iput-object v0, p0, Lcom/osp/app/signin/WebContentView;->d:Landroid/webkit/ValueCallback;

    .line 86
    iput-boolean v1, p0, Lcom/osp/app/signin/WebContentView;->e:Z

    .line 91
    iput-boolean v1, p0, Lcom/osp/app/signin/WebContentView;->f:Z

    .line 92
    iput-object v0, p0, Lcom/osp/app/signin/WebContentView;->y:Landroid/content/Intent;

    .line 102
    iput-boolean v1, p0, Lcom/osp/app/signin/WebContentView;->A:Z

    .line 107
    iput-object v0, p0, Lcom/osp/app/signin/WebContentView;->B:Ljava/lang/String;

    .line 111
    iput-object v0, p0, Lcom/osp/app/signin/WebContentView;->D:Landroid/view/View;

    .line 732
    return-void
.end method

.method static synthetic a(Lcom/osp/app/signin/WebContentView;Landroid/webkit/ValueCallback;)Landroid/webkit/ValueCallback;
    .locals 0

    .prologue
    .line 55
    iput-object p1, p0, Lcom/osp/app/signin/WebContentView;->d:Landroid/webkit/ValueCallback;

    return-object p1
.end method

.method static synthetic a(Lcom/osp/app/signin/WebContentView;)Landroid/webkit/WebView;
    .locals 1

    .prologue
    .line 55
    iget-object v0, p0, Lcom/osp/app/signin/WebContentView;->c:Landroid/webkit/WebView;

    return-object v0
.end method

.method private a(Ljava/lang/String;)V
    .locals 3

    .prologue
    .line 850
    new-instance v0, Landroid/content/Intent;

    const-string v1, "com.msc.action.samsungaccount.resignin"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 851
    const-string v1, "client_id"

    const-string v2, "j5p7ll8g33"

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 852
    const-string v1, "client_secret"

    const-string v2, "5763D0052DC1462E13751F753384E9A9"

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 853
    const-string v1, "OSP_VER"

    const-string v2, "OSP_02"

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 855
    if-eqz p1, :cond_0

    .line 857
    const-string v1, "email_id"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 860
    :cond_0
    invoke-virtual {p0, v0}, Lcom/osp/app/signin/WebContentView;->startActivity(Landroid/content/Intent;)V

    .line 861
    return-void
.end method

.method static synthetic b(Lcom/osp/app/signin/WebContentView;)Landroid/content/Intent;
    .locals 1

    .prologue
    .line 55
    iget-object v0, p0, Lcom/osp/app/signin/WebContentView;->y:Landroid/content/Intent;

    return-object v0
.end method

.method static synthetic c(Lcom/osp/app/signin/WebContentView;)Landroid/app/ProgressDialog;
    .locals 1

    .prologue
    .line 55
    iget-object v0, p0, Lcom/osp/app/signin/WebContentView;->b:Landroid/app/ProgressDialog;

    return-object v0
.end method

.method private c()V
    .locals 6

    .prologue
    .line 129
    invoke-virtual {p0}, Lcom/osp/app/signin/WebContentView;->o()I

    move-result v0

    .line 131
    invoke-virtual {p0}, Lcom/osp/app/signin/WebContentView;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0800ee

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    .line 132
    invoke-virtual {p0}, Lcom/osp/app/signin/WebContentView;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v2

    iget v2, v2, Landroid/util/DisplayMetrics;->widthPixels:I

    mul-int/lit8 v3, v1, 0x2

    sub-int/2addr v2, v3

    .line 133
    invoke-virtual {p0}, Lcom/osp/app/signin/WebContentView;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    invoke-virtual {v3}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v3

    iget v3, v3, Landroid/util/DisplayMetrics;->heightPixels:I

    mul-int/lit8 v4, v1, 0x2

    sub-int/2addr v3, v4

    sub-int/2addr v3, v0

    .line 135
    invoke-virtual {p0}, Lcom/osp/app/signin/WebContentView;->getWindow()Landroid/view/Window;

    move-result-object v4

    const/4 v5, 0x2

    invoke-virtual {v4, v5}, Landroid/view/Window;->addFlags(I)V

    .line 136
    invoke-virtual {p0}, Lcom/osp/app/signin/WebContentView;->getWindow()Landroid/view/Window;

    move-result-object v4

    invoke-virtual {v4}, Landroid/view/Window;->getAttributes()Landroid/view/WindowManager$LayoutParams;

    move-result-object v4

    .line 137
    iput v2, v4, Landroid/view/WindowManager$LayoutParams;->width:I

    .line 138
    iput v3, v4, Landroid/view/WindowManager$LayoutParams;->height:I

    .line 139
    const/high16 v5, 0x3f800000    # 1.0f

    iput v5, v4, Landroid/view/WindowManager$LayoutParams;->alpha:F

    .line 140
    const/high16 v5, 0x3f000000    # 0.5f

    iput v5, v4, Landroid/view/WindowManager$LayoutParams;->dimAmount:F

    .line 141
    add-int/2addr v0, v1

    iput v0, v4, Landroid/view/WindowManager$LayoutParams;->y:I

    .line 142
    const/16 v0, 0x31

    iput v0, v4, Landroid/view/WindowManager$LayoutParams;->gravity:I

    .line 144
    invoke-virtual {p0}, Lcom/osp/app/signin/WebContentView;->getWindow()Landroid/view/Window;

    move-result-object v0

    invoke-virtual {v0, v4}, Landroid/view/Window;->setAttributes(Landroid/view/WindowManager$LayoutParams;)V

    .line 146
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v0, "WCV"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v4, "setDialogSize()- "

    invoke-direct {v1, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 147
    return-void
.end method

.method static synthetic d(Lcom/osp/app/signin/WebContentView;)Landroid/app/ProgressDialog;
    .locals 1

    .prologue
    .line 55
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/osp/app/signin/WebContentView;->b:Landroid/app/ProgressDialog;

    return-object v0
.end method

.method private d()V
    .locals 3

    .prologue
    .line 433
    iget-object v0, p0, Lcom/osp/app/signin/WebContentView;->b:Landroid/app/ProgressDialog;

    if-nez v0, :cond_0

    .line 435
    new-instance v0, Landroid/app/ProgressDialog;

    iget-object v1, p0, Lcom/osp/app/signin/WebContentView;->a:Landroid/content/Context;

    invoke-direct {v0, v1}, Landroid/app/ProgressDialog;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/osp/app/signin/WebContentView;->b:Landroid/app/ProgressDialog;

    .line 436
    iget-object v0, p0, Lcom/osp/app/signin/WebContentView;->b:Landroid/app/ProgressDialog;

    iget-object v1, p0, Lcom/osp/app/signin/WebContentView;->a:Landroid/content/Context;

    const v2, 0x7f090036

    invoke-static {v1, v2}, Lcom/msc/sa/c/d;->a(Landroid/content/Context;I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/app/ProgressDialog;->setMessage(Ljava/lang/CharSequence;)V

    .line 437
    iget-object v0, p0, Lcom/osp/app/signin/WebContentView;->b:Landroid/app/ProgressDialog;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/app/ProgressDialog;->setCancelable(Z)V

    .line 438
    iget-object v0, p0, Lcom/osp/app/signin/WebContentView;->b:Landroid/app/ProgressDialog;

    new-instance v1, Lcom/osp/app/signin/pv;

    invoke-direct {v1, p0}, Lcom/osp/app/signin/pv;-><init>(Lcom/osp/app/signin/WebContentView;)V

    invoke-virtual {v0, v1}, Landroid/app/ProgressDialog;->setOnKeyListener(Landroid/content/DialogInterface$OnKeyListener;)V

    .line 477
    :try_start_0
    invoke-virtual {p0}, Lcom/osp/app/signin/WebContentView;->isFinishing()Z

    move-result v0

    if-nez v0, :cond_0

    .line 479
    iget-object v0, p0, Lcom/osp/app/signin/WebContentView;->b:Landroid/app/ProgressDialog;

    invoke-virtual {v0}, Landroid/app/ProgressDialog;->show()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 486
    :cond_0
    :goto_0
    return-void

    .line 481
    :catch_0
    move-exception v0

    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_0
.end method

.method private e()Ljava/lang/String;
    .locals 5

    .prologue
    const/4 v0, 0x0

    .line 795
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v1, "WCV"

    const-string v2, "makeAdsURL()"

    invoke-static {v1, v2}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 803
    :try_start_0
    invoke-static {p0}, Lcom/msc/c/e;->h(Landroid/content/Context;)Ljava/lang/String;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v2

    .line 805
    :try_start_1
    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 807
    invoke-static {p0}, Lcom/msc/c/c;->d(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v2

    .line 810
    :cond_0
    invoke-static {}, Lcom/osp/common/util/i;->a()Lcom/osp/common/util/i;

    invoke-static {p0}, Lcom/osp/app/util/ad;->p(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/osp/common/util/i;->b(Ljava/lang/String;)Ljava/lang/String;
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1

    move-result-object v1

    .line 811
    :try_start_2
    invoke-static {}, Lcom/osp/common/util/i;->a()Lcom/osp/common/util/i;

    invoke-static {p0}, Lcom/osp/common/util/i;->n(Landroid/content/Context;)Ljava/lang/String;
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_2

    move-result-object v0

    .line 818
    :goto_0
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-static {p0}, Lcom/msc/c/n;->g(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "?countryCode="

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v3, "&languageCode="

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "&gUID="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0

    .line 813
    :catch_0
    move-exception v1

    move-object v3, v1

    move-object v2, v0

    move-object v1, v0

    :goto_1
    invoke-virtual {v3}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_0

    :catch_1
    move-exception v1

    move-object v3, v1

    move-object v1, v0

    goto :goto_1

    :catch_2
    move-exception v3

    goto :goto_1
.end method

.method static synthetic e(Lcom/osp/app/signin/WebContentView;)V
    .locals 0

    .prologue
    .line 55
    invoke-direct {p0}, Lcom/osp/app/signin/WebContentView;->d()V

    return-void
.end method


# virtual methods
.method protected final a()V
    .locals 0

    .prologue
    .line 757
    return-void
.end method

.method protected final b()V
    .locals 0

    .prologue
    .line 763
    return-void
.end method

.method protected onActivityResult(IILandroid/content/Intent;)V
    .locals 4

    .prologue
    const/4 v3, -0x1

    const/4 v1, 0x0

    .line 542
    invoke-super {p0, p1, p2, p3}, Lcom/osp/app/util/BaseActivity;->onActivityResult(IILandroid/content/Intent;)V

    .line 543
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v2, "WebView::onActivityResult requestCode : "

    invoke-direct {v0, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, ",resultCode : "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;)V

    .line 545
    const/16 v0, 0x3e9

    if-ne p1, v0, :cond_3

    .line 547
    iget-object v0, p0, Lcom/osp/app/signin/WebContentView;->d:Landroid/webkit/ValueCallback;

    if-nez v0, :cond_1

    .line 549
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v0, "WCV"

    const-string v1, "onActivityResult() UploadMessage is null."

    invoke-static {v0, v1}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 574
    :cond_0
    :goto_0
    return-void

    .line 553
    :cond_1
    if-eqz p3, :cond_2

    if-eq p2, v3, :cond_4

    :cond_2
    move-object v0, v1

    .line 554
    :goto_1
    iget-object v2, p0, Lcom/osp/app/signin/WebContentView;->d:Landroid/webkit/ValueCallback;

    invoke-interface {v2, v0}, Landroid/webkit/ValueCallback;->onReceiveValue(Ljava/lang/Object;)V

    .line 555
    iput-object v1, p0, Lcom/osp/app/signin/WebContentView;->d:Landroid/webkit/ValueCallback;

    .line 558
    :cond_3
    const/16 v0, 0xd5

    if-ne p1, v0, :cond_0

    .line 560
    iput-object v1, p0, Lcom/osp/app/signin/WebContentView;->b:Landroid/app/ProgressDialog;

    .line 562
    if-ne p2, v3, :cond_5

    .line 564
    iget-object v0, p0, Lcom/osp/app/signin/WebContentView;->c:Landroid/webkit/WebView;

    invoke-virtual {v0}, Landroid/webkit/WebView;->getSettings()Landroid/webkit/WebSettings;

    move-result-object v0

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/webkit/WebSettings;->setJavaScriptEnabled(Z)V

    .line 565
    iget-object v0, p0, Lcom/osp/app/signin/WebContentView;->c:Landroid/webkit/WebView;

    iget-object v1, p0, Lcom/osp/app/signin/WebContentView;->B:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/webkit/WebView;->loadUrl(Ljava/lang/String;)V

    .line 566
    iget-object v0, p0, Lcom/osp/app/signin/WebContentView;->c:Landroid/webkit/WebView;

    new-instance v1, Lcom/osp/app/signin/px;

    invoke-direct {v1, p0}, Lcom/osp/app/signin/px;-><init>(Lcom/osp/app/signin/WebContentView;)V

    invoke-virtual {v0, v1}, Landroid/webkit/WebView;->setWebChromeClient(Landroid/webkit/WebChromeClient;)V

    .line 567
    iget-object v0, p0, Lcom/osp/app/signin/WebContentView;->c:Landroid/webkit/WebView;

    new-instance v1, Lcom/osp/app/signin/py;

    const/4 v2, 0x0

    invoke-direct {v1, p0, v2}, Lcom/osp/app/signin/py;-><init>(Lcom/osp/app/signin/WebContentView;B)V

    invoke-virtual {v0, v1}, Landroid/webkit/WebView;->setWebViewClient(Landroid/webkit/WebViewClient;)V

    goto :goto_0

    .line 553
    :cond_4
    invoke-virtual {p3}, Landroid/content/Intent;->getData()Landroid/net/Uri;

    move-result-object v0

    goto :goto_1

    .line 570
    :cond_5
    invoke-virtual {p0}, Lcom/osp/app/signin/WebContentView;->finish()V

    goto :goto_0
.end method

.method public onBackPressed()V
    .locals 2

    .prologue
    .line 625
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v0, "WCV"

    const-string v1, "onBackPressed"

    invoke-static {v0, v1}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 627
    iget-object v0, p0, Lcom/osp/app/signin/WebContentView;->c:Landroid/webkit/WebView;

    invoke-virtual {v0}, Landroid/webkit/WebView;->canGoBack()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 629
    iget-object v0, p0, Lcom/osp/app/signin/WebContentView;->c:Landroid/webkit/WebView;

    invoke-virtual {v0}, Landroid/webkit/WebView;->goBack()V

    .line 635
    :goto_0
    return-void

    .line 633
    :cond_0
    const/4 v0, 0x0

    iget-object v1, p0, Lcom/osp/app/signin/WebContentView;->y:Landroid/content/Intent;

    invoke-virtual {p0, v0, v1}, Lcom/osp/app/signin/WebContentView;->a(ILandroid/content/Intent;)V

    .line 634
    invoke-super {p0}, Lcom/osp/app/util/BaseActivity;->onBackPressed()V

    goto :goto_0
.end method

.method public onConfigurationChanged(Landroid/content/res/Configuration;)V
    .locals 2

    .prologue
    .line 115
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v0, "WCV"

    const-string v1, "onConfigurationChanged()"

    invoke-static {v0, v1}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 117
    invoke-super {p0, p1}, Lcom/osp/app/util/BaseActivity;->onConfigurationChanged(Landroid/content/res/Configuration;)V

    .line 119
    iget-boolean v0, p0, Lcom/osp/app/signin/WebContentView;->f:Z

    const/4 v1, 0x1

    if-ne v0, v1, :cond_0

    .line 121
    invoke-direct {p0}, Lcom/osp/app/signin/WebContentView;->c()V

    .line 123
    :cond_0
    return-void
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 10
    .annotation build Landroid/annotation/SuppressLint;
        value = {
            "SetJavaScriptEnabled"
        }
    .end annotation

    .prologue
    const/16 v9, 0xe

    const/4 v3, 0x2

    const/4 v8, -0x1

    const/4 v7, 0x1

    const/4 v6, 0x0

    .line 152
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v0, "WCV"

    const-string v1, "onCreate()"

    invoke-static {v0, v1}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 154
    invoke-virtual {p0}, Lcom/osp/app/signin/WebContentView;->getIntent()Landroid/content/Intent;

    move-result-object v0

    iput-object v0, p0, Lcom/osp/app/signin/WebContentView;->y:Landroid/content/Intent;

    .line 155
    iget-object v0, p0, Lcom/osp/app/signin/WebContentView;->y:Landroid/content/Intent;

    invoke-virtual {v0}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    .line 156
    if-nez v0, :cond_0

    .line 158
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v1, "WCV"

    const-string v2, "onCreate() - intent Action is null."

    invoke-static {v1, v2}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 159
    iget-object v1, p0, Lcom/osp/app/signin/WebContentView;->y:Landroid/content/Intent;

    invoke-virtual {p0, v6, v1}, Lcom/osp/app/signin/WebContentView;->a(ILandroid/content/Intent;)V

    .line 160
    invoke-virtual {p0}, Lcom/osp/app/signin/WebContentView;->finish()V

    .line 163
    :cond_0
    const-string v1, "com.msc.action.samsungaccount.web_ads"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_1

    const-string v1, "com.msc.action.samsungaccount.web_usage_data_analysis"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 165
    :cond_1
    invoke-static {v6}, Lcom/osp/app/signin/SamsungService;->a(Z)V

    .line 168
    :cond_2
    invoke-static {p0}, Lcom/osp/app/util/r;->r(Landroid/content/Context;)Z

    move-result v1

    .line 170
    if-eqz v1, :cond_3

    .line 172
    const v2, 0x7f0a0002

    invoke-virtual {p0, v2}, Lcom/osp/app/signin/WebContentView;->setTheme(I)V

    .line 173
    invoke-virtual {p0, v3}, Lcom/osp/app/signin/WebContentView;->d(I)V

    .line 176
    :cond_3
    invoke-super {p0, p1}, Lcom/osp/app/util/BaseActivity;->onCreate(Landroid/os/Bundle;)V

    .line 178
    if-eqz v1, :cond_4

    .line 180
    new-instance v2, Landroid/view/WindowManager$LayoutParams;

    invoke-direct {v2}, Landroid/view/WindowManager$LayoutParams;-><init>()V

    .line 181
    iput v3, v2, Landroid/view/WindowManager$LayoutParams;->flags:I

    .line 182
    const/high16 v3, 0x3f400000    # 0.75f

    iput v3, v2, Landroid/view/WindowManager$LayoutParams;->dimAmount:F

    .line 183
    invoke-virtual {p0}, Lcom/osp/app/signin/WebContentView;->getWindow()Landroid/view/Window;

    move-result-object v3

    invoke-virtual {v3, v2}, Landroid/view/Window;->setAttributes(Landroid/view/WindowManager$LayoutParams;)V

    .line 186
    :cond_4
    const-string v2, "com.msc.action.samsungaccount.web_no_button"

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_f

    .line 188
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v0, "WCV"

    invoke-static {}, Lcom/osp/app/util/v;->a()Lcom/osp/app/util/v;

    move-result-object v2

    const-string v3, "com.msc.action.samsungaccount.web_no_button"

    invoke-virtual {v2, v3}, Lcom/osp/app/util/v;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 189
    iget-object v0, p0, Lcom/osp/app/signin/WebContentView;->y:Landroid/content/Intent;

    const-string v2, "ServerUrl"

    invoke-virtual {v0, v2}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/osp/app/signin/WebContentView;->B:Ljava/lang/String;

    .line 249
    :cond_5
    :goto_0
    iget-object v0, p0, Lcom/osp/app/signin/WebContentView;->B:Ljava/lang/String;

    if-nez v0, :cond_6

    .line 251
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v0, "WCV"

    const-string v2, "onCreate() - URL is null."

    invoke-static {v0, v2}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 252
    iget-object v0, p0, Lcom/osp/app/signin/WebContentView;->y:Landroid/content/Intent;

    invoke-virtual {p0, v6, v0}, Lcom/osp/app/signin/WebContentView;->a(ILandroid/content/Intent;)V

    .line 253
    invoke-virtual {p0}, Lcom/osp/app/signin/WebContentView;->finish()V

    .line 255
    :cond_6
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v2, "WebContentView::onCreate() Server URL = "

    invoke-direct {v0, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, Lcom/osp/app/signin/WebContentView;->B:Ljava/lang/String;

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;)V

    .line 258
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    if-ge v0, v9, :cond_1a

    .line 260
    invoke-virtual {p0, v7}, Lcom/osp/app/signin/WebContentView;->requestWindowFeature(I)Z

    .line 280
    :cond_7
    :goto_1
    iget-boolean v0, p0, Lcom/osp/app/signin/WebContentView;->A:Z

    if-eqz v0, :cond_8

    .line 282
    invoke-static {p0}, Lcom/msc/sa/c/d;->b(Landroid/app/Activity;)V

    .line 285
    :cond_8
    if-nez v1, :cond_9

    .line 287
    const v0, 0x7f03005d

    invoke-virtual {p0, v0}, Lcom/osp/app/signin/WebContentView;->setContentView(I)V

    .line 290
    :cond_9
    invoke-virtual {p0}, Lcom/osp/app/signin/WebContentView;->getLayoutInflater()Landroid/view/LayoutInflater;

    move-result-object v0

    const v2, 0x7f03005d

    const/4 v3, 0x0

    invoke-virtual {v0, v2, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/osp/app/signin/WebContentView;->D:Landroid/view/View;

    .line 292
    iget-object v0, p0, Lcom/osp/app/signin/WebContentView;->z:Landroid/widget/LinearLayout;

    if-nez v0, :cond_a

    invoke-static {p0}, Lcom/osp/app/util/r;->r(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_1c

    iget-object v0, p0, Lcom/osp/app/signin/WebContentView;->D:Landroid/view/View;

    const v2, 0x7f0c0028

    invoke-virtual {v0, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcom/osp/app/signin/WebContentView;->z:Landroid/widget/LinearLayout;

    :goto_2
    iget-object v0, p0, Lcom/osp/app/signin/WebContentView;->z:Landroid/widget/LinearLayout;

    invoke-static {p0}, Lcom/osp/app/util/an;->a(Lcom/osp/app/util/BaseActivity;)Lcom/osp/app/util/an;

    const v2, 0x7f0200ff

    invoke-static {v2}, Lcom/osp/app/util/an;->a(I)I

    move-result v2

    invoke-virtual {v0, v2}, Landroid/widget/LinearLayout;->setBackgroundResource(I)V

    :cond_a
    const v0, 0x7f0c0155

    invoke-virtual {p0, v0}, Lcom/osp/app/signin/WebContentView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    if-eqz v0, :cond_b

    invoke-static {p0}, Lcom/osp/app/util/an;->a(Lcom/osp/app/util/BaseActivity;)Lcom/osp/app/util/an;

    const v2, 0x7f020020

    invoke-static {v2}, Lcom/osp/app/util/an;->a(I)I

    move-result v2

    invoke-virtual {v0, v2}, Landroid/widget/Button;->setBackgroundResource(I)V

    invoke-virtual {p0}, Lcom/osp/app/signin/WebContentView;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-static {p0}, Lcom/osp/app/util/an;->a(Lcom/osp/app/util/BaseActivity;)Lcom/osp/app/util/an;

    const v3, 0x7f020030

    invoke-static {v3}, Lcom/osp/app/util/an;->a(I)I

    move-result v3

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getColorStateList(I)Landroid/content/res/ColorStateList;

    move-result-object v2

    invoke-virtual {v0, v2}, Landroid/widget/Button;->setTextColor(Landroid/content/res/ColorStateList;)V

    .line 294
    :cond_b
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v0, "WCV"

    const-string v2, "initComponent"

    const-string v3, "START"

    invoke-static {v0, v2, v3}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    iput-object p0, p0, Lcom/osp/app/signin/WebContentView;->a:Landroid/content/Context;

    invoke-static {p0}, Lcom/osp/app/util/r;->r(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_1d

    iget-object v0, p0, Lcom/osp/app/signin/WebContentView;->D:Landroid/view/View;

    const v2, 0x7f0c00fd

    invoke-virtual {v0, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/webkit/WebView;

    iput-object v0, p0, Lcom/osp/app/signin/WebContentView;->c:Landroid/webkit/WebView;

    iput-boolean v6, p0, Lcom/osp/app/signin/WebContentView;->e:Z

    :cond_c
    iget-object v0, p0, Lcom/osp/app/signin/WebContentView;->z:Landroid/widget/LinearLayout;

    const/16 v2, 0x8

    invoke-virtual {v0, v2}, Landroid/widget/LinearLayout;->setVisibility(I)V

    :goto_3
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v0, "WCV"

    const-string v2, "initComponent"

    const-string v3, "END"

    invoke-static {v0, v2, v3}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 296
    iget-boolean v0, p0, Lcom/osp/app/signin/WebContentView;->f:Z

    if-ne v0, v7, :cond_d

    .line 298
    invoke-direct {p0}, Lcom/osp/app/signin/WebContentView;->c()V

    .line 301
    :cond_d
    invoke-static {p0}, Lcom/osp/app/util/ad;->a(Landroid/content/Context;)Z

    move-result v0

    if-nez v0, :cond_1f

    .line 303
    const/16 v0, 0xd5

    invoke-virtual {p0, v0}, Lcom/osp/app/signin/WebContentView;->c(I)V

    .line 321
    :cond_e
    :goto_4
    return-void

    .line 191
    :cond_f
    const-string v2, "com.msc.action.samsungaccount.web_with_sign_in_screen_button"

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_11

    .line 193
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v0, "WCV"

    invoke-static {}, Lcom/osp/app/util/v;->a()Lcom/osp/app/util/v;

    move-result-object v2

    const-string v3, "com.msc.action.samsungaccount.web_with_sign_in_screen_button"

    invoke-virtual {v2, v3}, Lcom/osp/app/util/v;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 194
    invoke-virtual {p0}, Lcom/osp/app/signin/WebContentView;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {p0}, Lcom/osp/app/signin/WebContentView;->h()Z

    move-result v2

    invoke-static {v0, v2}, Lcom/msc/c/g;->a(Landroid/content/Context;Z)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/osp/app/signin/WebContentView;->B:Ljava/lang/String;

    .line 195
    iput-boolean v7, p0, Lcom/osp/app/signin/WebContentView;->A:Z

    .line 197
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    if-lt v0, v9, :cond_10

    .line 199
    iput-boolean v6, p0, Lcom/osp/app/signin/WebContentView;->e:Z

    goto/16 :goto_0

    .line 202
    :cond_10
    iput-boolean v7, p0, Lcom/osp/app/signin/WebContentView;->e:Z

    goto/16 :goto_0

    .line 204
    :cond_11
    const-string v2, "com.msc.action.samsungaccount.web_dialog"

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_13

    .line 206
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v0, "WCV"

    invoke-static {}, Lcom/osp/app/util/v;->a()Lcom/osp/app/util/v;

    move-result-object v2

    const-string v3, "com.msc.action.samsungaccount.web_dialog"

    invoke-virtual {v2, v3}, Lcom/osp/app/util/v;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 207
    iget-object v0, p0, Lcom/osp/app/signin/WebContentView;->y:Landroid/content/Intent;

    const-string v2, "ServerUrl"

    invoke-virtual {v0, v2}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/osp/app/signin/WebContentView;->B:Ljava/lang/String;

    .line 208
    iput-boolean v7, p0, Lcom/osp/app/signin/WebContentView;->e:Z

    .line 209
    invoke-static {}, Lcom/osp/app/signin/SamsungService;->a()Z

    move-result v0

    if-nez v0, :cond_12

    invoke-static {p0}, Lcom/osp/app/util/r;->k(Landroid/content/Context;)Z

    move-result v0

    if-nez v0, :cond_5

    .line 211
    :cond_12
    iput-boolean v7, p0, Lcom/osp/app/signin/WebContentView;->f:Z

    goto/16 :goto_0

    .line 213
    :cond_13
    const-string v2, "com.msc.action.samsungaccount.web_with_close_button"

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_14

    .line 215
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v0, "WCV"

    invoke-static {}, Lcom/osp/app/util/v;->a()Lcom/osp/app/util/v;

    move-result-object v2

    const-string v3, "com.msc.action.samsungaccount.web_with_close_button"

    invoke-virtual {v2, v3}, Lcom/osp/app/util/v;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 216
    iget-object v0, p0, Lcom/osp/app/signin/WebContentView;->y:Landroid/content/Intent;

    const-string v2, "ServerUrl"

    invoke-virtual {v0, v2}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/osp/app/signin/WebContentView;->B:Ljava/lang/String;

    .line 218
    iput-boolean v7, p0, Lcom/osp/app/signin/WebContentView;->e:Z

    goto/16 :goto_0

    .line 220
    :cond_14
    const-string v2, "com.msc.action.samsungaccount.web_ads"

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_16

    .line 222
    invoke-static {p0, v6}, Lcom/msc/c/c;->a(Landroid/content/Context;Z)Z

    move-result v0

    if-nez v0, :cond_15

    .line 224
    invoke-static {p0}, Lcom/osp/app/util/ad;->h(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/osp/app/signin/WebContentView;->a(Ljava/lang/String;)V

    .line 225
    invoke-virtual {p0}, Lcom/osp/app/signin/WebContentView;->finish()V

    goto/16 :goto_4

    .line 228
    :cond_15
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v0, "WCV"

    invoke-static {}, Lcom/osp/app/util/v;->a()Lcom/osp/app/util/v;

    move-result-object v2

    const-string v3, "com.msc.action.samsungaccount.web_ads"

    invoke-virtual {v2, v3}, Lcom/osp/app/util/v;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 229
    invoke-direct {p0}, Lcom/osp/app/signin/WebContentView;->e()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/osp/app/signin/WebContentView;->B:Ljava/lang/String;

    .line 230
    iput-boolean v7, p0, Lcom/osp/app/signin/WebContentView;->e:Z

    goto/16 :goto_0

    .line 231
    :cond_16
    const-string v2, "com.msc.action.samsungaccount.web_usage_data_analysis"

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_19

    .line 233
    invoke-static {p0, v6}, Lcom/msc/c/c;->a(Landroid/content/Context;Z)Z

    move-result v0

    if-nez v0, :cond_17

    .line 235
    invoke-static {p0}, Lcom/osp/app/util/ad;->h(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/osp/app/signin/WebContentView;->a(Ljava/lang/String;)V

    .line 236
    invoke-virtual {p0}, Lcom/osp/app/signin/WebContentView;->finish()V

    goto/16 :goto_4

    .line 239
    :cond_17
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v0, "WCV"

    invoke-static {}, Lcom/osp/app/util/v;->a()Lcom/osp/app/util/v;

    move-result-object v2

    const-string v3, "com.msc.action.samsungaccount.web_usage_data_analysis"

    invoke-virtual {v2, v3}, Lcom/osp/app/util/v;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 240
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v0, "WCV"

    const-string v2, "makeUsageDataAnalysisURL()"

    invoke-static {v0, v2}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;)V

    invoke-static {p0}, Lcom/msc/c/e;->h(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v2

    invoke-static {}, Lcom/osp/common/util/i;->a()Lcom/osp/common/util/i;

    invoke-static {p0}, Lcom/osp/app/util/ad;->p(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/osp/common/util/i;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-static {}, Lcom/osp/common/util/i;->a()Lcom/osp/common/util/i;

    invoke-static {p0}, Lcom/osp/common/util/i;->n(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v4

    const/4 v0, 0x0

    if-eqz v3, :cond_18

    sget-object v0, Ljava/util/Locale;->ENGLISH:Ljava/util/Locale;

    invoke-virtual {v3, v0}, Ljava/lang/String;->toUpperCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v0

    :cond_18
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-static {p0}, Lcom/msc/c/n;->a(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v5, "?gUID="

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "&countryCode="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, "&languageCode="

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/osp/app/signin/WebContentView;->B:Ljava/lang/String;

    .line 241
    iput-boolean v7, p0, Lcom/osp/app/signin/WebContentView;->e:Z

    goto/16 :goto_0

    .line 244
    :cond_19
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v2, "WCV"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "onCreate() - unknown intent Action : "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v2, v0}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 245
    iget-object v0, p0, Lcom/osp/app/signin/WebContentView;->y:Landroid/content/Intent;

    invoke-virtual {p0, v6, v0}, Lcom/osp/app/signin/WebContentView;->a(ILandroid/content/Intent;)V

    .line 246
    invoke-virtual {p0}, Lcom/osp/app/signin/WebContentView;->finish()V

    goto/16 :goto_0

    .line 265
    :cond_1a
    invoke-static {p0}, Lcom/osp/app/util/r;->r(Landroid/content/Context;)Z

    move-result v0

    if-nez v0, :cond_7

    .line 267
    iget-boolean v0, p0, Lcom/osp/app/signin/WebContentView;->A:Z

    if-nez v0, :cond_7

    .line 269
    invoke-virtual {p0}, Lcom/osp/app/signin/WebContentView;->h()Z

    move-result v0

    if-eqz v0, :cond_1b

    .line 271
    invoke-virtual {p0}, Lcom/osp/app/signin/WebContentView;->i()V

    goto/16 :goto_1

    .line 274
    :cond_1b
    invoke-virtual {p0}, Lcom/osp/app/signin/WebContentView;->j()V

    goto/16 :goto_1

    .line 292
    :cond_1c
    const v0, 0x7f0c0028

    invoke-virtual {p0, v0}, Lcom/osp/app/signin/WebContentView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcom/osp/app/signin/WebContentView;->z:Landroid/widget/LinearLayout;

    goto/16 :goto_2

    .line 294
    :cond_1d
    const v0, 0x7f0c00fd

    invoke-virtual {p0, v0}, Lcom/osp/app/signin/WebContentView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/webkit/WebView;

    iput-object v0, p0, Lcom/osp/app/signin/WebContentView;->c:Landroid/webkit/WebView;

    const v0, 0x7f0c0155

    invoke-virtual {p0, v0}, Lcom/osp/app/signin/WebContentView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iget-object v2, p0, Lcom/osp/app/signin/WebContentView;->a:Landroid/content/Context;

    invoke-virtual {p0}, Lcom/osp/app/signin/WebContentView;->h()Z

    move-result v3

    invoke-static {v2, v0, v3}, Lcom/msc/sa/c/d;->a(Landroid/content/Context;Landroid/view/View;Z)V

    new-instance v2, Lcom/osp/app/signin/pw;

    invoke-direct {v2, p0}, Lcom/osp/app/signin/pw;-><init>(Lcom/osp/app/signin/WebContentView;)V

    invoke-virtual {v0, v2}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-boolean v2, p0, Lcom/osp/app/signin/WebContentView;->e:Z

    if-ne v2, v7, :cond_c

    iget-boolean v2, p0, Lcom/osp/app/signin/WebContentView;->A:Z

    if-eqz v2, :cond_1e

    invoke-static {}, Lcom/osp/app/signin/SamsungService;->a()Z

    move-result v2

    if-eqz v2, :cond_1e

    const v2, 0x7f090135

    invoke-virtual {v0, v2}, Landroid/widget/Button;->setText(I)V

    :goto_5
    iget-object v0, p0, Lcom/osp/app/signin/WebContentView;->z:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v6}, Landroid/widget/LinearLayout;->setVisibility(I)V

    goto/16 :goto_3

    :cond_1e
    const v2, 0x7f0900c7

    invoke-virtual {v0, v2}, Landroid/widget/Button;->setText(I)V

    goto :goto_5

    .line 307
    :cond_1f
    if-nez v1, :cond_20

    .line 309
    invoke-direct {p0}, Lcom/osp/app/signin/WebContentView;->d()V

    .line 312
    :cond_20
    iget-object v0, p0, Lcom/osp/app/signin/WebContentView;->c:Landroid/webkit/WebView;

    invoke-virtual {v0}, Landroid/webkit/WebView;->getSettings()Landroid/webkit/WebSettings;

    move-result-object v0

    invoke-virtual {v0, v7}, Landroid/webkit/WebSettings;->setJavaScriptEnabled(Z)V

    .line 313
    iget-object v0, p0, Lcom/osp/app/signin/WebContentView;->c:Landroid/webkit/WebView;

    iget-object v2, p0, Lcom/osp/app/signin/WebContentView;->B:Ljava/lang/String;

    invoke-virtual {v0, v2}, Landroid/webkit/WebView;->loadUrl(Ljava/lang/String;)V

    .line 314
    iget-object v0, p0, Lcom/osp/app/signin/WebContentView;->c:Landroid/webkit/WebView;

    new-instance v2, Lcom/osp/app/signin/px;

    invoke-direct {v2, p0}, Lcom/osp/app/signin/px;-><init>(Lcom/osp/app/signin/WebContentView;)V

    invoke-virtual {v0, v2}, Landroid/webkit/WebView;->setWebChromeClient(Landroid/webkit/WebChromeClient;)V

    .line 315
    iget-object v0, p0, Lcom/osp/app/signin/WebContentView;->c:Landroid/webkit/WebView;

    new-instance v2, Lcom/osp/app/signin/py;

    invoke-direct {v2, p0, v6}, Lcom/osp/app/signin/py;-><init>(Lcom/osp/app/signin/WebContentView;B)V

    invoke-virtual {v0, v2}, Landroid/webkit/WebView;->setWebViewClient(Landroid/webkit/WebViewClient;)V

    .line 317
    if-eqz v1, :cond_e

    .line 319
    new-instance v1, Landroid/app/AlertDialog$Builder;

    invoke-direct {v1, p0}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    iget-object v0, p0, Lcom/osp/app/signin/WebContentView;->y:Landroid/content/Intent;

    const-string v2, "dynamic_webview_title"

    invoke-virtual {v0, v2}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_21

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v2

    if-lez v2, :cond_21

    invoke-virtual {v1, v0}, Landroid/app/AlertDialog$Builder;->setTitle(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    :cond_21
    iget-object v0, p0, Lcom/osp/app/signin/WebContentView;->a:Landroid/content/Context;

    const v2, 0x7f0900c7

    invoke-virtual {v0, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    new-instance v2, Lcom/osp/app/signin/pt;

    invoke-direct {v2, p0}, Lcom/osp/app/signin/pt;-><init>(Lcom/osp/app/signin/WebContentView;)V

    invoke-virtual {v1, v0, v2}, Landroid/app/AlertDialog$Builder;->setPositiveButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    new-instance v0, Lcom/osp/app/signin/pu;

    invoke-direct {v0, p0}, Lcom/osp/app/signin/pu;-><init>(Lcom/osp/app/signin/WebContentView;)V

    invoke-virtual {v1, v0}, Landroid/app/AlertDialog$Builder;->setOnKeyListener(Landroid/content/DialogInterface$OnKeyListener;)Landroid/app/AlertDialog$Builder;

    iget-object v0, p0, Lcom/osp/app/signin/WebContentView;->D:Landroid/view/View;

    const v2, 0x7f0c0187

    invoke-virtual {v0, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    const v2, 0x7f0200c8

    invoke-virtual {v0, v2}, Landroid/view/View;->setBackgroundResource(I)V

    iget-object v0, p0, Lcom/osp/app/signin/WebContentView;->D:Landroid/view/View;

    const v2, 0x7f0c0188

    invoke-virtual {v0, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    invoke-virtual {v0, v8}, Landroid/widget/LinearLayout;->setBackgroundColor(I)V

    iget-object v0, p0, Lcom/osp/app/signin/WebContentView;->D:Landroid/view/View;

    invoke-virtual {v1, v0}, Landroid/app/AlertDialog$Builder;->setView(Landroid/view/View;)Landroid/app/AlertDialog$Builder;

    invoke-virtual {v1}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v0

    iput-object v0, p0, Lcom/osp/app/signin/WebContentView;->C:Landroid/app/AlertDialog;

    iget-object v0, p0, Lcom/osp/app/signin/WebContentView;->C:Landroid/app/AlertDialog;

    invoke-virtual {v0}, Landroid/app/AlertDialog;->getWindow()Landroid/view/Window;

    move-result-object v0

    const/16 v1, 0x10

    invoke-virtual {v0, v1}, Landroid/view/Window;->setSoftInputMode(I)V

    iget-object v0, p0, Lcom/osp/app/signin/WebContentView;->C:Landroid/app/AlertDialog;

    invoke-virtual {v0, v6}, Landroid/app/AlertDialog;->setCanceledOnTouchOutside(Z)V

    iget-object v0, p0, Lcom/osp/app/signin/WebContentView;->C:Landroid/app/AlertDialog;

    invoke-virtual {v0}, Landroid/app/AlertDialog;->show()V

    iget-object v0, p0, Lcom/osp/app/signin/WebContentView;->C:Landroid/app/AlertDialog;

    invoke-virtual {v0}, Landroid/app/AlertDialog;->getWindow()Landroid/view/Window;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/Window;->getAttributes()Landroid/view/WindowManager$LayoutParams;

    move-result-object v0

    invoke-static {}, Lcom/osp/device/b;->b()Lcom/osp/device/b;

    move-result-object v1

    invoke-virtual {v1, p0}, Lcom/osp/device/b;->i(Landroid/content/Context;)Z

    move-result v1

    if-eqz v1, :cond_22

    invoke-virtual {p0}, Lcom/osp/app/signin/WebContentView;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f080101

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    iput v1, v0, Landroid/view/WindowManager$LayoutParams;->width:I

    :goto_6
    const/4 v1, -0x2

    iput v1, v0, Landroid/view/WindowManager$LayoutParams;->height:I

    iget-object v1, p0, Lcom/osp/app/signin/WebContentView;->C:Landroid/app/AlertDialog;

    invoke-virtual {v1}, Landroid/app/AlertDialog;->getWindow()Landroid/view/Window;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/view/Window;->setAttributes(Landroid/view/WindowManager$LayoutParams;)V

    goto/16 :goto_4

    :cond_22
    iput v8, v0, Landroid/view/WindowManager$LayoutParams;->width:I

    goto :goto_6
.end method

.method protected onDestroy()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 578
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v0, "WCV"

    const-string v1, "onDestroy"

    invoke-static {v0, v1}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 580
    iget-object v0, p0, Lcom/osp/app/signin/WebContentView;->c:Landroid/webkit/WebView;

    if-eqz v0, :cond_0

    .line 582
    iget-object v0, p0, Lcom/osp/app/signin/WebContentView;->c:Landroid/webkit/WebView;

    invoke-virtual {v0}, Landroid/webkit/WebView;->getSettings()Landroid/webkit/WebSettings;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/webkit/WebSettings;->setJavaScriptEnabled(Z)V

    .line 583
    iget-object v0, p0, Lcom/osp/app/signin/WebContentView;->c:Landroid/webkit/WebView;

    invoke-virtual {v0}, Landroid/webkit/WebView;->clearHistory()V

    .line 584
    iget-object v0, p0, Lcom/osp/app/signin/WebContentView;->c:Landroid/webkit/WebView;

    invoke-virtual {v0}, Landroid/webkit/WebView;->destroy()V

    .line 585
    iput-object v2, p0, Lcom/osp/app/signin/WebContentView;->c:Landroid/webkit/WebView;

    .line 588
    :cond_0
    iget-object v0, p0, Lcom/osp/app/signin/WebContentView;->b:Landroid/app/ProgressDialog;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/osp/app/signin/WebContentView;->b:Landroid/app/ProgressDialog;

    invoke-virtual {v0}, Landroid/app/ProgressDialog;->isShowing()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 590
    iget-object v0, p0, Lcom/osp/app/signin/WebContentView;->b:Landroid/app/ProgressDialog;

    invoke-virtual {v0}, Landroid/app/ProgressDialog;->dismiss()V

    .line 591
    iput-object v2, p0, Lcom/osp/app/signin/WebContentView;->b:Landroid/app/ProgressDialog;

    .line 594
    :cond_1
    iget-object v0, p0, Lcom/osp/app/signin/WebContentView;->C:Landroid/app/AlertDialog;

    if-eqz v0, :cond_2

    .line 596
    iput-object v2, p0, Lcom/osp/app/signin/WebContentView;->C:Landroid/app/AlertDialog;

    .line 599
    :cond_2
    invoke-super {p0}, Lcom/osp/app/util/BaseActivity;->onDestroy()V

    .line 600
    return-void
.end method

.method public onOptionsItemSelected(Landroid/view/MenuItem;)Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 325
    sget v1, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v2, 0xb

    if-lt v1, v2, :cond_0

    .line 327
    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v1

    packed-switch v1, :pswitch_data_0

    .line 339
    :goto_0
    return v0

    .line 330
    :pswitch_0
    iget-object v1, p0, Lcom/osp/app/signin/WebContentView;->c:Landroid/webkit/WebView;

    invoke-virtual {v1}, Landroid/webkit/WebView;->clearHistory()V

    .line 331
    iget-object v1, p0, Lcom/osp/app/signin/WebContentView;->y:Landroid/content/Intent;

    invoke-virtual {p0, v0, v1}, Lcom/osp/app/signin/WebContentView;->a(ILandroid/content/Intent;)V

    .line 332
    invoke-virtual {p0}, Lcom/osp/app/signin/WebContentView;->finish()V

    .line 339
    :cond_0
    const/4 v0, 0x1

    goto :goto_0

    .line 327
    :pswitch_data_0
    .packed-switch 0x102002c
        :pswitch_0
    .end packed-switch
.end method

.method protected onPause()V
    .locals 2

    .prologue
    .line 604
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v0, "WCV"

    const-string v1, "onPause"

    invoke-static {v0, v1}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 606
    invoke-super {p0}, Lcom/osp/app/util/BaseActivity;->onPause()V

    .line 607
    return-void
.end method

.method protected onResume()V
    .locals 2

    .prologue
    .line 611
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v0, "WCV"

    const-string v1, "onResume"

    invoke-static {v0, v1}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 613
    invoke-super {p0}, Lcom/osp/app/util/BaseActivity;->onResume()V

    .line 614
    return-void
.end method

.method protected onStop()V
    .locals 2

    .prologue
    .line 618
    invoke-super {p0}, Lcom/osp/app/util/BaseActivity;->onStop()V

    .line 620
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v0, "WCV"

    const-string v1, "onStop"

    invoke-static {v0, v1}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 621
    return-void
.end method
