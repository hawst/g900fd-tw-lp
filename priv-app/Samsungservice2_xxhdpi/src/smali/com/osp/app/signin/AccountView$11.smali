.class Lcom/osp/app/signin/AccountView$11;
.super Landroid/text/style/URLSpan;
.source "AccountView.java"


# instance fields
.field final synthetic a:Lcom/osp/app/signin/AccountView;


# direct methods
.method constructor <init>(Lcom/osp/app/signin/AccountView;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 2781
    iput-object p1, p0, Lcom/osp/app/signin/AccountView$11;->a:Lcom/osp/app/signin/AccountView;

    invoke-direct {p0, p2}, Landroid/text/style/URLSpan;-><init>(Ljava/lang/String;)V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 5

    .prologue
    const/4 v4, 0x1

    .line 2785
    iget-object v0, p0, Lcom/osp/app/signin/AccountView$11;->a:Lcom/osp/app/signin/AccountView;

    invoke-static {v0}, Lcom/osp/app/signin/AccountView;->p(Lcom/osp/app/signin/AccountView;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 2787
    iget-object v0, p0, Lcom/osp/app/signin/AccountView$11;->a:Lcom/osp/app/signin/AccountView;

    const/16 v1, 0x132

    const/4 v2, 0x0

    new-instance v3, Lcom/osp/app/signin/r;

    invoke-direct {v3, p0}, Lcom/osp/app/signin/r;-><init>(Lcom/osp/app/signin/AccountView$11;)V

    invoke-virtual {v0, v1, v2, v3}, Lcom/osp/app/signin/AccountView;->a(ILandroid/content/DialogInterface$OnDismissListener;Landroid/content/DialogInterface$OnCancelListener;)V

    .line 2836
    :cond_0
    :goto_0
    return-void

    .line 2797
    :cond_1
    iget-object v0, p0, Lcom/osp/app/signin/AccountView$11;->a:Lcom/osp/app/signin/AccountView;

    invoke-static {v0}, Lcom/osp/app/util/ad;->p(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 2800
    iget-object v0, p0, Lcom/osp/app/signin/AccountView$11;->a:Lcom/osp/app/signin/AccountView;

    new-instance v1, Lcom/osp/app/signin/be;

    iget-object v2, p0, Lcom/osp/app/signin/AccountView$11;->a:Lcom/osp/app/signin/AccountView;

    const/4 v3, 0x2

    invoke-direct {v1, v2, v3}, Lcom/osp/app/signin/be;-><init>(Lcom/osp/app/signin/AccountView;I)V

    invoke-static {v0, v1}, Lcom/osp/app/signin/AccountView;->a(Lcom/osp/app/signin/AccountView;Lcom/osp/app/signin/be;)Lcom/osp/app/signin/be;

    .line 2801
    iget-object v0, p0, Lcom/osp/app/signin/AccountView$11;->a:Lcom/osp/app/signin/AccountView;

    invoke-static {v0}, Lcom/osp/app/signin/AccountView;->q(Lcom/osp/app/signin/AccountView;)Lcom/osp/app/signin/be;

    move-result-object v0

    invoke-virtual {v0}, Lcom/osp/app/signin/be;->b()V

    goto :goto_0

    .line 2806
    :cond_2
    :try_start_0
    iget-object v0, p0, Lcom/osp/app/signin/AccountView$11;->a:Lcom/osp/app/signin/AccountView;

    invoke-static {v0}, Lcom/osp/app/signin/AccountView;->g(Lcom/osp/app/signin/AccountView;)Z

    .line 2807
    iget-object v0, p0, Lcom/osp/app/signin/AccountView$11;->a:Lcom/osp/app/signin/AccountView;

    invoke-static {v0}, Lcom/osp/app/signin/AccountView;->r(Lcom/osp/app/signin/AccountView;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 2811
    iget-object v0, p0, Lcom/osp/app/signin/AccountView$11;->a:Lcom/osp/app/signin/AccountView;

    invoke-static {v0}, Lcom/osp/app/signin/AccountView;->s(Lcom/osp/app/signin/AccountView;)Z

    .line 2813
    const/4 v0, 0x1

    invoke-virtual {p1, v0}, Landroid/view/View;->setSoundEffectsEnabled(Z)V

    .line 2814
    const/4 v0, 0x0

    invoke-virtual {p1, v0}, Landroid/view/View;->playSoundEffect(I)V

    .line 2816
    iget-object v0, p0, Lcom/osp/app/signin/AccountView$11;->a:Lcom/osp/app/signin/AccountView;

    iget-object v1, p0, Lcom/osp/app/signin/AccountView$11;->a:Lcom/osp/app/signin/AccountView;

    invoke-static {v1}, Lcom/osp/app/signin/AccountView;->t(Lcom/osp/app/signin/AccountView;)Z

    move-result v1

    invoke-static {v0, v1}, Lcom/msc/c/n;->a(Landroid/content/Context;Z)Ljava/lang/String;

    move-result-object v0

    .line 2818
    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    .line 2819
    new-instance v2, Landroid/content/Intent;

    const-string v3, "android.intent.action.VIEW"

    invoke-direct {v2, v3, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;Landroid/net/Uri;)V

    .line 2820
    const-string v1, "com.android.browser.application_id"

    iget-object v3, p0, Lcom/osp/app/signin/AccountView$11;->a:Lcom/osp/app/signin/AccountView;

    invoke-virtual {v3}, Lcom/osp/app/signin/AccountView;->getPackageName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v1, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 2822
    invoke-static {}, Lcom/osp/app/signin/SamsungService;->a()Z

    move-result v1

    if-nez v1, :cond_3

    iget-object v1, p0, Lcom/osp/app/signin/AccountView$11;->a:Lcom/osp/app/signin/AccountView;

    invoke-static {v2, v1}, Lcom/osp/app/util/ad;->a(Landroid/content/Intent;Landroid/content/Context;)Z

    move-result v1

    if-ne v1, v4, :cond_3

    .line 2824
    iget-object v0, p0, Lcom/osp/app/signin/AccountView$11;->a:Lcom/osp/app/signin/AccountView;

    invoke-virtual {v0, v2}, Lcom/osp/app/signin/AccountView;->startActivity(Landroid/content/Intent;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 2830
    :catch_0
    move-exception v0

    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_0

    .line 2827
    :cond_3
    :try_start_1
    iget-object v1, p0, Lcom/osp/app/signin/AccountView$11;->a:Lcom/osp/app/signin/AccountView;

    invoke-static {v1, v0}, Lcom/osp/app/signin/AccountView;->a(Lcom/osp/app/signin/AccountView;Ljava/lang/String;)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_0
.end method
