.class final Lcom/osp/app/signin/fk;
.super Lcom/msc/c/b;
.source "EmailValidationView.java"


# instance fields
.field final synthetic c:Lcom/osp/app/signin/EmailValidationView;

.field private d:J

.field private e:Ljava/lang/String;

.field private f:Z


# direct methods
.method public constructor <init>(Lcom/osp/app/signin/EmailValidationView;Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 926
    iput-object p1, p0, Lcom/osp/app/signin/fk;->c:Lcom/osp/app/signin/EmailValidationView;

    .line 927
    invoke-direct {p0, p2}, Lcom/msc/c/b;-><init>(Landroid/content/Context;)V

    .line 923
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/osp/app/signin/fk;->e:Ljava/lang/String;

    .line 928
    return-void
.end method


# virtual methods
.method protected final varargs a()Ljava/lang/Boolean;
    .locals 3

    .prologue
    .line 938
    invoke-static {}, Lcom/msc/c/g;->a()Lcom/msc/c/g;

    .line 939
    iget-object v0, p0, Lcom/osp/app/signin/fk;->c:Lcom/osp/app/signin/EmailValidationView;

    iget-object v1, p0, Lcom/osp/app/signin/fk;->c:Lcom/osp/app/signin/EmailValidationView;

    invoke-static {v1}, Lcom/osp/app/signin/EmailValidationView;->a(Lcom/osp/app/signin/EmailValidationView;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1, p0}, Lcom/msc/c/g;->f(Landroid/content/Context;Ljava/lang/String;Lcom/msc/b/h;)J

    move-result-wide v0

    iput-wide v0, p0, Lcom/osp/app/signin/fk;->d:J

    .line 940
    iget-wide v0, p0, Lcom/osp/app/signin/fk;->d:J

    invoke-virtual {p0, v0, v1}, Lcom/osp/app/signin/fk;->a(J)V

    .line 941
    iget-wide v0, p0, Lcom/osp/app/signin/fk;->d:J

    const-string v2, "from_xml"

    invoke-virtual {p0, v0, v1, v2}, Lcom/osp/app/signin/fk;->a(JLjava/lang/String;)V

    .line 942
    iget-wide v0, p0, Lcom/osp/app/signin/fk;->d:J

    sget-object v2, Lcom/msc/b/g;->c:Lcom/msc/b/g;

    invoke-static {v0, v1, v2}, Lcom/msc/c/g;->a(JLcom/msc/b/g;)V

    .line 943
    const/4 v0, 0x0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    return-object v0
.end method

.method public final declared-synchronized a(Lcom/msc/b/c;)V
    .locals 6

    .prologue
    .line 981
    monitor-enter p0

    :try_start_0
    invoke-super {p0, p1}, Lcom/msc/c/b;->a(Lcom/msc/b/c;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 983
    if-nez p1, :cond_1

    .line 1004
    :cond_0
    :goto_0
    monitor-exit p0

    return-void

    .line 988
    :cond_1
    :try_start_1
    invoke-virtual {p1}, Lcom/msc/b/c;->a()J

    move-result-wide v0

    .line 989
    invoke-virtual {p1}, Lcom/msc/b/c;->e()Ljava/lang/String;

    move-result-object v2

    .line 991
    iget-wide v4, p0, Lcom/osp/app/signin/fk;->d:J

    cmp-long v0, v0, v4

    if-nez v0, :cond_0

    .line 993
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/osp/app/signin/fk;->f:Z
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 996
    :try_start_2
    invoke-static {}, Lcom/msc/c/h;->a()Lcom/msc/c/h;

    iget-object v0, p0, Lcom/osp/app/signin/fk;->c:Lcom/osp/app/signin/EmailValidationView;

    invoke-static {v2}, Lcom/msc/c/h;->y(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/osp/app/signin/fk;->e:Ljava/lang/String;
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_0
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0

    .line 998
    :catch_0
    move-exception v0

    :try_start_3
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    goto :goto_0

    .line 981
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method protected final a(Ljava/lang/Boolean;)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 948
    invoke-super {p0, p1}, Lcom/msc/c/b;->a(Ljava/lang/Boolean;)V

    .line 949
    iget-boolean v0, p0, Lcom/osp/app/signin/fk;->f:Z

    if-eqz v0, :cond_3

    .line 951
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v0, "EVV"

    const-string v1, "Server API for check domain is Succeed."

    invoke-static {v0, v1}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 952
    iget-object v0, p0, Lcom/osp/app/signin/fk;->e:Ljava/lang/String;

    if-eqz v0, :cond_0

    .line 954
    iget-object v0, p0, Lcom/osp/app/signin/fk;->c:Lcom/osp/app/signin/EmailValidationView;

    iget-object v1, p0, Lcom/osp/app/signin/fk;->e:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/osp/app/signin/EmailValidationView;->b(Lcom/osp/app/signin/EmailValidationView;Ljava/lang/String;)Ljava/lang/String;

    .line 955
    iget-object v0, p0, Lcom/osp/app/signin/fk;->c:Lcom/osp/app/signin/EmailValidationView;

    invoke-static {v0, v2}, Lcom/osp/app/signin/EmailValidationView;->a(Lcom/osp/app/signin/EmailValidationView;I)V

    .line 956
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v0, "EVV"

    const-string v1, "Domain is Top 100."

    invoke-static {v0, v1}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 977
    :goto_0
    return-void

    .line 959
    :cond_0
    iget-object v0, p0, Lcom/osp/app/signin/fk;->c:Lcom/osp/app/signin/EmailValidationView;

    invoke-static {v0}, Lcom/osp/app/signin/EmailValidationView;->n(Lcom/osp/app/signin/EmailValidationView;)Z

    move-result v0

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/osp/app/signin/fk;->c:Lcom/osp/app/signin/EmailValidationView;

    invoke-static {v0}, Lcom/osp/app/signin/EmailValidationView;->o(Lcom/osp/app/signin/EmailValidationView;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 961
    :cond_1
    iget-object v0, p0, Lcom/osp/app/signin/fk;->c:Lcom/osp/app/signin/EmailValidationView;

    const/4 v1, 0x1

    invoke-static {v0, v1}, Lcom/osp/app/signin/EmailValidationView;->a(Lcom/osp/app/signin/EmailValidationView;I)V

    .line 962
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v0, "EVV"

    const-string v1, "Domain is not Top 100."

    invoke-static {v0, v1}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 965
    :cond_2
    iget-object v0, p0, Lcom/osp/app/signin/fk;->c:Lcom/osp/app/signin/EmailValidationView;

    const/4 v1, 0x3

    invoke-static {v0, v1}, Lcom/osp/app/signin/EmailValidationView;->a(Lcom/osp/app/signin/EmailValidationView;I)V

    .line 966
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v0, "EVV"

    const-string v1, "Browser is not available."

    invoke-static {v0, v1}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 972
    :cond_3
    invoke-virtual {p0, v2}, Lcom/osp/app/signin/fk;->a(Z)V

    .line 973
    iget-object v0, p0, Lcom/osp/app/signin/fk;->c:Lcom/osp/app/signin/EmailValidationView;

    const/4 v1, 0x4

    invoke-static {v0, v1}, Lcom/osp/app/signin/EmailValidationView;->a(Lcom/osp/app/signin/EmailValidationView;I)V

    .line 974
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v0, "EVV"

    const-string v1, "Server API for check domain is failed."

    invoke-static {v0, v1}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method protected final d()V
    .locals 1

    .prologue
    .line 932
    invoke-super {p0}, Lcom/msc/c/b;->d()V

    .line 933
    iget-object v0, p0, Lcom/osp/app/signin/fk;->c:Lcom/osp/app/signin/EmailValidationView;

    invoke-virtual {v0}, Lcom/osp/app/signin/EmailValidationView;->finish()V

    .line 934
    return-void
.end method

.method protected final synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 920
    invoke-virtual {p0}, Lcom/osp/app/signin/fk;->a()Ljava/lang/Boolean;

    move-result-object v0

    return-object v0
.end method

.method protected final synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 920
    check-cast p1, Ljava/lang/Boolean;

    invoke-virtual {p0, p1}, Lcom/osp/app/signin/fk;->a(Ljava/lang/Boolean;)V

    return-void
.end method
