.class final Lcom/osp/app/signin/lz;
.super Lcom/msc/c/b;
.source "SignUpView.java"


# instance fields
.field final synthetic c:Lcom/osp/app/signin/SignUpView;

.field private d:J

.field private e:J

.field private f:Z


# direct methods
.method public constructor <init>(Lcom/osp/app/signin/SignUpView;)V
    .locals 1

    .prologue
    .line 7491
    iput-object p1, p0, Lcom/osp/app/signin/lz;->c:Lcom/osp/app/signin/SignUpView;

    .line 7492
    invoke-direct {p0, p1}, Lcom/msc/c/b;-><init>(Landroid/content/Context;)V

    .line 7486
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/osp/app/signin/lz;->f:Z

    .line 7498
    return-void
.end method


# virtual methods
.method protected final varargs a()Ljava/lang/Boolean;
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 7519
    iget-object v0, p0, Lcom/osp/app/signin/lz;->c:Lcom/osp/app/signin/SignUpView;

    invoke-static {v0}, Lcom/osp/app/signin/SignUpView;->N(Lcom/osp/app/signin/SignUpView;)V

    .line 7520
    iget-object v0, p0, Lcom/osp/app/signin/lz;->c:Lcom/osp/app/signin/SignUpView;

    iget-object v1, p0, Lcom/osp/app/signin/lz;->c:Lcom/osp/app/signin/SignUpView;

    invoke-static {v1}, Lcom/osp/app/signin/SignUpView;->K(Lcom/osp/app/signin/SignUpView;)Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/osp/app/signin/lz;->c:Lcom/osp/app/signin/SignUpView;

    invoke-virtual {v2}, Lcom/osp/app/signin/SignUpView;->getApplicationContext()Landroid/content/Context;

    iget-object v2, p0, Lcom/osp/app/signin/lz;->c:Lcom/osp/app/signin/SignUpView;

    invoke-static {v2}, Lcom/osp/app/signin/SignUpView;->K(Lcom/osp/app/signin/SignUpView;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/osp/common/util/i;->d(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v1, v2}, Lcom/msc/c/e;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)V

    .line 7522
    invoke-static {}, Lcom/osp/device/b;->b()Lcom/osp/device/b;

    move-result-object v0

    invoke-virtual {v0}, Lcom/osp/device/b;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 7524
    invoke-static {}, Lcom/msc/c/g;->a()Lcom/msc/c/g;

    iget-object v0, p0, Lcom/osp/app/signin/lz;->c:Lcom/osp/app/signin/SignUpView;

    iget-object v1, p0, Lcom/osp/app/signin/lz;->c:Lcom/osp/app/signin/SignUpView;

    invoke-static {v1}, Lcom/osp/app/signin/SignUpView;->U(Lcom/osp/app/signin/SignUpView;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1, p0}, Lcom/msc/c/g;->c(Landroid/content/Context;Ljava/lang/String;Lcom/msc/b/h;)J

    move-result-wide v0

    iput-wide v0, p0, Lcom/osp/app/signin/lz;->e:J

    iget-wide v0, p0, Lcom/osp/app/signin/lz;->e:J

    invoke-virtual {p0, v0, v1}, Lcom/osp/app/signin/lz;->a(J)V

    iget-wide v0, p0, Lcom/osp/app/signin/lz;->e:J

    sget-object v2, Lcom/msc/b/g;->c:Lcom/msc/b/g;

    invoke-static {v0, v1, v2}, Lcom/msc/c/g;->a(JLcom/msc/b/g;)V

    .line 7529
    :goto_0
    invoke-static {v3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    return-object v0

    .line 7527
    :cond_0
    invoke-static {}, Lcom/msc/c/g;->a()Lcom/msc/c/g;

    iget-object v0, p0, Lcom/osp/app/signin/lz;->c:Lcom/osp/app/signin/SignUpView;

    iget-object v1, p0, Lcom/osp/app/signin/lz;->c:Lcom/osp/app/signin/SignUpView;

    invoke-static {v1}, Lcom/osp/app/signin/SignUpView;->U(Lcom/osp/app/signin/SignUpView;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1, v3, p0}, Lcom/msc/c/g;->a(Landroid/content/Context;Ljava/lang/String;ZLcom/msc/b/h;)J

    move-result-wide v0

    iput-wide v0, p0, Lcom/osp/app/signin/lz;->d:J

    iget-wide v0, p0, Lcom/osp/app/signin/lz;->d:J

    invoke-virtual {p0, v0, v1}, Lcom/osp/app/signin/lz;->a(J)V

    iget-wide v0, p0, Lcom/osp/app/signin/lz;->d:J

    sget-object v2, Lcom/msc/b/g;->c:Lcom/msc/b/g;

    invoke-static {v0, v1, v2}, Lcom/msc/c/g;->a(JLcom/msc/b/g;)V

    goto :goto_0
.end method

.method public final declared-synchronized a(Lcom/msc/b/c;)V
    .locals 12

    .prologue
    const/4 v2, 0x0

    const/4 v11, 0x1

    .line 7625
    monitor-enter p0

    :try_start_0
    invoke-super {p0, p1}, Lcom/msc/c/b;->a(Lcom/msc/b/c;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 7627
    if-nez p1, :cond_1

    .line 7682
    :cond_0
    :goto_0
    monitor-exit p0

    return-void

    .line 7632
    :cond_1
    :try_start_1
    invoke-virtual {p1}, Lcom/msc/b/c;->a()J

    move-result-wide v0

    .line 7633
    invoke-virtual {p1}, Lcom/msc/b/c;->e()Ljava/lang/String;

    move-result-object v3

    .line 7635
    iget-wide v4, p0, Lcom/osp/app/signin/lz;->d:J
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    cmp-long v4, v0, v4

    if-nez v4, :cond_3

    .line 7639
    :try_start_2
    iget-object v0, p0, Lcom/osp/app/signin/lz;->c:Lcom/osp/app/signin/SignUpView;

    invoke-static {v0}, Lcom/osp/app/signin/SignUpView;->O(Lcom/osp/app/signin/SignUpView;)Lcom/osp/app/signin/kd;

    move-result-object v0

    if-eqz v0, :cond_2

    .line 7641
    iget-object v0, p0, Lcom/osp/app/signin/lz;->c:Lcom/osp/app/signin/SignUpView;

    new-instance v1, Lcom/osp/app/signin/kd;

    invoke-direct {v1}, Lcom/osp/app/signin/kd;-><init>()V

    invoke-static {v0, v1}, Lcom/osp/app/signin/SignUpView;->a(Lcom/osp/app/signin/SignUpView;Lcom/osp/app/signin/kd;)Lcom/osp/app/signin/kd;

    .line 7644
    :cond_2
    iget-object v0, p0, Lcom/osp/app/signin/lz;->c:Lcom/osp/app/signin/SignUpView;

    invoke-static {}, Lcom/msc/c/h;->a()Lcom/msc/c/h;

    invoke-static {v3}, Lcom/msc/c/h;->j(Ljava/lang/String;)Lcom/osp/app/signin/kd;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/osp/app/signin/SignUpView;->a(Lcom/osp/app/signin/SignUpView;Lcom/osp/app/signin/kd;)Lcom/osp/app/signin/kd;

    .line 7645
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/osp/app/signin/lz;->f:Z
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_0
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0

    .line 7646
    :catch_0
    move-exception v0

    :try_start_3
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    goto :goto_0

    .line 7625
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 7650
    :cond_3
    :try_start_4
    iget-wide v4, p0, Lcom/osp/app/signin/lz;->e:J
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    cmp-long v0, v0, v4

    if-nez v0, :cond_0

    .line 7654
    :try_start_5
    iget-object v0, p0, Lcom/osp/app/signin/lz;->c:Lcom/osp/app/signin/SignUpView;

    invoke-static {v0}, Lcom/osp/app/signin/SignUpView;->O(Lcom/osp/app/signin/SignUpView;)Lcom/osp/app/signin/kd;

    move-result-object v0

    if-nez v0, :cond_4

    .line 7656
    iget-object v0, p0, Lcom/osp/app/signin/lz;->c:Lcom/osp/app/signin/SignUpView;

    new-instance v1, Lcom/osp/app/signin/kd;

    invoke-direct {v1}, Lcom/osp/app/signin/kd;-><init>()V

    invoke-static {v0, v1}, Lcom/osp/app/signin/SignUpView;->a(Lcom/osp/app/signin/SignUpView;Lcom/osp/app/signin/kd;)Lcom/osp/app/signin/kd;

    .line 7658
    :cond_4
    iget-object v0, p0, Lcom/osp/app/signin/lz;->c:Lcom/osp/app/signin/SignUpView;

    invoke-static {v0}, Lcom/osp/app/signin/SignUpView;->R(Lcom/osp/app/signin/SignUpView;)Lcom/osp/app/signin/kd;

    move-result-object v0

    if-nez v0, :cond_5

    .line 7660
    iget-object v0, p0, Lcom/osp/app/signin/lz;->c:Lcom/osp/app/signin/SignUpView;

    new-instance v1, Lcom/osp/app/signin/kd;

    invoke-direct {v1}, Lcom/osp/app/signin/kd;-><init>()V

    invoke-static {v0, v1}, Lcom/osp/app/signin/SignUpView;->b(Lcom/osp/app/signin/SignUpView;Lcom/osp/app/signin/kd;)Lcom/osp/app/signin/kd;

    .line 7662
    :cond_5
    iget-object v0, p0, Lcom/osp/app/signin/lz;->c:Lcom/osp/app/signin/SignUpView;

    invoke-static {v0}, Lcom/osp/app/signin/SignUpView;->S(Lcom/osp/app/signin/SignUpView;)Lcom/osp/app/signin/kd;

    move-result-object v0

    if-nez v0, :cond_6

    .line 7664
    iget-object v0, p0, Lcom/osp/app/signin/lz;->c:Lcom/osp/app/signin/SignUpView;

    new-instance v1, Lcom/osp/app/signin/kd;

    invoke-direct {v1}, Lcom/osp/app/signin/kd;-><init>()V

    invoke-static {v0, v1}, Lcom/osp/app/signin/SignUpView;->c(Lcom/osp/app/signin/SignUpView;Lcom/osp/app/signin/kd;)Lcom/osp/app/signin/kd;

    .line 7668
    :cond_6
    invoke-static {}, Lcom/msc/c/h;->a()Lcom/msc/c/h;

    iget-object v0, p0, Lcom/osp/app/signin/lz;->c:Lcom/osp/app/signin/SignUpView;

    invoke-static {v0}, Lcom/osp/app/signin/SignUpView;->R(Lcom/osp/app/signin/SignUpView;)Lcom/osp/app/signin/kd;

    move-result-object v5

    iget-object v0, p0, Lcom/osp/app/signin/lz;->c:Lcom/osp/app/signin/SignUpView;

    invoke-static {v0}, Lcom/osp/app/signin/SignUpView;->S(Lcom/osp/app/signin/SignUpView;)Lcom/osp/app/signin/kd;

    move-result-object v6

    new-instance v0, Lcom/osp/app/signin/kd;

    invoke-direct {v0}, Lcom/osp/app/signin/kd;-><init>()V

    invoke-static {}, Lorg/xmlpull/v1/XmlPullParserFactory;->newInstance()Lorg/xmlpull/v1/XmlPullParserFactory;

    move-result-object v1

    const/4 v4, 0x1

    invoke-virtual {v1, v4}, Lorg/xmlpull/v1/XmlPullParserFactory;->setNamespaceAware(Z)V

    invoke-virtual {v1}, Lorg/xmlpull/v1/XmlPullParserFactory;->newPullParser()Lorg/xmlpull/v1/XmlPullParser;

    move-result-object v7

    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v1, "SaAuthManager::infoFieldByEmailPhoneFromXML"

    invoke-static {v1}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;)V
    :try_end_5
    .catch Ljava/lang/Exception; {:try_start_5 .. :try_end_5} :catch_1
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    :try_start_6
    new-instance v1, Ljava/io/StringReader;

    invoke-direct {v1, v3}, Ljava/io/StringReader;-><init>(Ljava/lang/String;)V
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_2

    :try_start_7
    invoke-interface {v7, v1}, Lorg/xmlpull/v1/XmlPullParser;->setInput(Ljava/io/Reader;)V

    invoke-interface {v7}, Lorg/xmlpull/v1/XmlPullParser;->getEventType()I

    move-result v3

    move v4, v3

    move-object v3, v0

    move-object v0, v2

    :goto_1
    if-eq v4, v11, :cond_29

    packed-switch v4, :pswitch_data_0

    :goto_2
    invoke-interface {v7}, Lorg/xmlpull/v1/XmlPullParser;->next()I

    move-result v4

    goto :goto_1

    :pswitch_0
    invoke-interface {v7}, Lorg/xmlpull/v1/XmlPullParser;->getName()Ljava/lang/String;

    move-result-object v4

    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    new-instance v8, Ljava/lang/StringBuilder;

    const-string v9, "SaAuthManager::infoFieldByEmailPhoneFromXML name : "

    invoke-direct {v8, v9}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v8, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v8}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;)V

    const-string v8, "emailID"

    invoke-virtual {v8, v4}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v8

    if-eqz v8, :cond_9

    move-object v3, v5

    :cond_7
    :goto_3
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v8, "SaAuthManager::infoFieldByEmailPhoneFromXML mValueTagName : "

    invoke-direct {v4, v8}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;)V

    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v8, "SaAuthManager::infoFieldByEmailPhoneFromXML mValueTagText : "

    invoke-direct {v4, v8}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;)V
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_1

    goto :goto_2

    :catchall_1
    move-exception v0

    :goto_4
    if-eqz v1, :cond_8

    :try_start_8
    invoke-virtual {v1}, Ljava/io/StringReader;->close()V

    :cond_8
    throw v0
    :try_end_8
    .catch Ljava/lang/Exception; {:try_start_8 .. :try_end_8} :catch_1
    .catchall {:try_start_8 .. :try_end_8} :catchall_0

    .line 7677
    :catch_1
    move-exception v0

    :try_start_9
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V
    :try_end_9
    .catchall {:try_start_9 .. :try_end_9} :catchall_0

    goto/16 :goto_0

    .line 7668
    :cond_9
    :try_start_a
    const-string v8, "phoneNumberID"

    invoke-virtual {v8, v4}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v8

    if-eqz v8, :cond_a

    move-object v3, v6

    goto :goto_3

    :cond_a
    const-string v8, "prefixName"

    invoke-virtual {v8, v4}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v8

    if-eqz v8, :cond_d

    const/4 v0, 0x0

    invoke-interface {v7, v0}, Lorg/xmlpull/v1/XmlPullParser;->getAttributeName(I)Ljava/lang/String;

    move-result-object v0

    const/4 v2, 0x0

    invoke-interface {v7, v2}, Lorg/xmlpull/v1/XmlPullParser;->getAttributeValue(I)Ljava/lang/String;

    move-result-object v2

    const-string v8, "mandatory"

    invoke-virtual {v8, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_b

    const-string v8, "1"

    invoke-virtual {v8, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_b

    invoke-virtual {v3}, Lcom/osp/app/signin/kd;->v()V

    :cond_b
    const/4 v8, 0x1

    invoke-interface {v7, v8}, Lorg/xmlpull/v1/XmlPullParser;->getAttributeName(I)Ljava/lang/String;

    move-result-object v8

    const/4 v9, 0x1

    invoke-interface {v7, v9}, Lorg/xmlpull/v1/XmlPullParser;->getAttributeValue(I)Ljava/lang/String;

    move-result-object v9

    const-string v10, "selectionElementList"

    invoke-virtual {v10, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_c

    invoke-virtual {v3, v9}, Lcom/osp/app/signin/kd;->b(Ljava/lang/String;)V

    :cond_c
    const/4 v8, 0x1

    iput-boolean v8, v3, Lcom/osp/app/signin/kd;->c:Z

    invoke-virtual {v3, v4}, Lcom/osp/app/signin/kd;->a(Ljava/lang/String;)V

    goto/16 :goto_3

    :cond_d
    const-string v8, "givenName"

    invoke-virtual {v8, v4}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v8

    if-eqz v8, :cond_f

    const/4 v0, 0x0

    invoke-interface {v7, v0}, Lorg/xmlpull/v1/XmlPullParser;->getAttributeName(I)Ljava/lang/String;

    move-result-object v0

    const/4 v2, 0x0

    invoke-interface {v7, v2}, Lorg/xmlpull/v1/XmlPullParser;->getAttributeValue(I)Ljava/lang/String;

    move-result-object v2

    const-string v8, "mandatory"

    invoke-virtual {v8, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_e

    const-string v8, "1"

    invoke-virtual {v8, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_e

    invoke-virtual {v3}, Lcom/osp/app/signin/kd;->x()V

    :cond_e
    const/4 v8, 0x1

    iput-boolean v8, v3, Lcom/osp/app/signin/kd;->d:Z

    invoke-virtual {v3, v4}, Lcom/osp/app/signin/kd;->a(Ljava/lang/String;)V

    goto/16 :goto_3

    :cond_f
    const-string v8, "familyName"

    invoke-virtual {v8, v4}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v8

    if-eqz v8, :cond_11

    const/4 v0, 0x0

    invoke-interface {v7, v0}, Lorg/xmlpull/v1/XmlPullParser;->getAttributeName(I)Ljava/lang/String;

    move-result-object v0

    const/4 v2, 0x0

    invoke-interface {v7, v2}, Lorg/xmlpull/v1/XmlPullParser;->getAttributeValue(I)Ljava/lang/String;

    move-result-object v2

    const-string v8, "mandatory"

    invoke-virtual {v8, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_10

    const-string v8, "1"

    invoke-virtual {v8, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_10

    invoke-virtual {v3}, Lcom/osp/app/signin/kd;->z()V

    :cond_10
    const/4 v8, 0x1

    iput-boolean v8, v3, Lcom/osp/app/signin/kd;->e:Z

    invoke-virtual {v3, v4}, Lcom/osp/app/signin/kd;->a(Ljava/lang/String;)V

    goto/16 :goto_3

    :cond_11
    const-string v8, "localityText"

    invoke-virtual {v8, v4}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v8

    if-eqz v8, :cond_13

    const/4 v0, 0x0

    invoke-interface {v7, v0}, Lorg/xmlpull/v1/XmlPullParser;->getAttributeName(I)Ljava/lang/String;

    move-result-object v0

    const/4 v2, 0x0

    invoke-interface {v7, v2}, Lorg/xmlpull/v1/XmlPullParser;->getAttributeValue(I)Ljava/lang/String;

    move-result-object v2

    const-string v8, "mandatory"

    invoke-virtual {v8, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_12

    const-string v8, "1"

    invoke-virtual {v8, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_12

    invoke-virtual {v3}, Lcom/osp/app/signin/kd;->B()V

    :cond_12
    const/4 v8, 0x1

    iput-boolean v8, v3, Lcom/osp/app/signin/kd;->f:Z

    invoke-virtual {v3, v4}, Lcom/osp/app/signin/kd;->a(Ljava/lang/String;)V

    goto/16 :goto_3

    :cond_13
    const-string v8, "postalCodeText"

    invoke-virtual {v8, v4}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v8

    if-eqz v8, :cond_15

    const/4 v0, 0x0

    invoke-interface {v7, v0}, Lorg/xmlpull/v1/XmlPullParser;->getAttributeName(I)Ljava/lang/String;

    move-result-object v0

    const/4 v2, 0x0

    invoke-interface {v7, v2}, Lorg/xmlpull/v1/XmlPullParser;->getAttributeValue(I)Ljava/lang/String;

    move-result-object v2

    const-string v8, "mandatory"

    invoke-virtual {v8, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_14

    const-string v8, "1"

    invoke-virtual {v8, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_14

    invoke-virtual {v3}, Lcom/osp/app/signin/kd;->L()V

    :cond_14
    const/4 v8, 0x1

    iput-boolean v8, v3, Lcom/osp/app/signin/kd;->g:Z

    invoke-virtual {v3, v4}, Lcom/osp/app/signin/kd;->a(Ljava/lang/String;)V

    goto/16 :goto_3

    :cond_15
    const-string v8, "receiveSMSPhoneNumberText"

    invoke-virtual {v8, v4}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v8

    if-eqz v8, :cond_17

    const/4 v0, 0x0

    invoke-interface {v7, v0}, Lorg/xmlpull/v1/XmlPullParser;->getAttributeName(I)Ljava/lang/String;

    move-result-object v0

    const/4 v2, 0x0

    invoke-interface {v7, v2}, Lorg/xmlpull/v1/XmlPullParser;->getAttributeValue(I)Ljava/lang/String;

    move-result-object v2

    const-string v8, "mandatory"

    invoke-virtual {v8, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_16

    const-string v8, "1"

    invoke-virtual {v8, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_16

    invoke-virtual {v3}, Lcom/osp/app/signin/kd;->N()V

    :cond_16
    const/4 v8, 0x1

    iput-boolean v8, v3, Lcom/osp/app/signin/kd;->h:Z

    invoke-virtual {v3, v4}, Lcom/osp/app/signin/kd;->a(Ljava/lang/String;)V

    goto/16 :goto_3

    :cond_17
    const-string v8, "streetText"

    invoke-virtual {v8, v4}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v8

    if-eqz v8, :cond_19

    const/4 v0, 0x0

    invoke-interface {v7, v0}, Lorg/xmlpull/v1/XmlPullParser;->getAttributeName(I)Ljava/lang/String;

    move-result-object v0

    const/4 v2, 0x0

    invoke-interface {v7, v2}, Lorg/xmlpull/v1/XmlPullParser;->getAttributeValue(I)Ljava/lang/String;

    move-result-object v2

    const-string v8, "mandatory"

    invoke-virtual {v8, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_18

    const-string v8, "1"

    invoke-virtual {v8, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_18

    invoke-virtual {v3}, Lcom/osp/app/signin/kd;->D()V

    :cond_18
    const/4 v8, 0x1

    iput-boolean v8, v3, Lcom/osp/app/signin/kd;->j:Z

    invoke-virtual {v3, v4}, Lcom/osp/app/signin/kd;->a(Ljava/lang/String;)V

    goto/16 :goto_3

    :cond_19
    const-string v8, "extendedText"

    invoke-virtual {v8, v4}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v8

    if-eqz v8, :cond_1b

    const/4 v0, 0x0

    invoke-interface {v7, v0}, Lorg/xmlpull/v1/XmlPullParser;->getAttributeName(I)Ljava/lang/String;

    move-result-object v0

    const/4 v2, 0x0

    invoke-interface {v7, v2}, Lorg/xmlpull/v1/XmlPullParser;->getAttributeValue(I)Ljava/lang/String;

    move-result-object v2

    const-string v8, "mandatory"

    invoke-virtual {v8, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_1a

    const-string v8, "1"

    invoke-virtual {v8, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_1a

    invoke-virtual {v3}, Lcom/osp/app/signin/kd;->F()V

    :cond_1a
    const/4 v8, 0x1

    iput-boolean v8, v3, Lcom/osp/app/signin/kd;->k:Z

    invoke-virtual {v3, v4}, Lcom/osp/app/signin/kd;->a(Ljava/lang/String;)V

    goto/16 :goto_3

    :cond_1b
    const-string v8, "postOfficeBoxNumberText"

    invoke-virtual {v8, v4}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v8

    if-eqz v8, :cond_1d

    const/4 v0, 0x0

    invoke-interface {v7, v0}, Lorg/xmlpull/v1/XmlPullParser;->getAttributeName(I)Ljava/lang/String;

    move-result-object v0

    const/4 v2, 0x0

    invoke-interface {v7, v2}, Lorg/xmlpull/v1/XmlPullParser;->getAttributeValue(I)Ljava/lang/String;

    move-result-object v2

    const-string v8, "mandatory"

    invoke-virtual {v8, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_1c

    const-string v8, "1"

    invoke-virtual {v8, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_1c

    invoke-virtual {v3}, Lcom/osp/app/signin/kd;->H()V

    :cond_1c
    const/4 v8, 0x1

    iput-boolean v8, v3, Lcom/osp/app/signin/kd;->l:Z

    invoke-virtual {v3, v4}, Lcom/osp/app/signin/kd;->a(Ljava/lang/String;)V

    goto/16 :goto_3

    :cond_1d
    const-string v8, "regionText"

    invoke-virtual {v8, v4}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v8

    if-eqz v8, :cond_1f

    const/4 v0, 0x0

    invoke-interface {v7, v0}, Lorg/xmlpull/v1/XmlPullParser;->getAttributeName(I)Ljava/lang/String;

    move-result-object v0

    const/4 v2, 0x0

    invoke-interface {v7, v2}, Lorg/xmlpull/v1/XmlPullParser;->getAttributeValue(I)Ljava/lang/String;

    move-result-object v2

    const-string v8, "mandatory"

    invoke-virtual {v8, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_1e

    const-string v8, "1"

    invoke-virtual {v8, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_1e

    invoke-virtual {v3}, Lcom/osp/app/signin/kd;->J()V

    :cond_1e
    const/4 v8, 0x1

    iput-boolean v8, v3, Lcom/osp/app/signin/kd;->m:Z

    invoke-virtual {v3, v4}, Lcom/osp/app/signin/kd;->a(Ljava/lang/String;)V

    goto/16 :goto_3

    :cond_1f
    const-string v8, "genderTypeCode"

    invoke-virtual {v8, v4}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v8

    if-eqz v8, :cond_21

    const/4 v0, 0x0

    invoke-interface {v7, v0}, Lorg/xmlpull/v1/XmlPullParser;->getAttributeName(I)Ljava/lang/String;

    move-result-object v0

    const/4 v2, 0x0

    invoke-interface {v7, v2}, Lorg/xmlpull/v1/XmlPullParser;->getAttributeValue(I)Ljava/lang/String;

    move-result-object v2

    const-string v8, "mandatory"

    invoke-virtual {v8, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_20

    const-string v8, "1"

    invoke-virtual {v8, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_20

    invoke-virtual {v3}, Lcom/osp/app/signin/kd;->Q()V

    :cond_20
    const/4 v8, 0x1

    iput-boolean v8, v3, Lcom/osp/app/signin/kd;->n:Z

    invoke-virtual {v3, v4}, Lcom/osp/app/signin/kd;->a(Ljava/lang/String;)V

    goto/16 :goto_3

    :cond_21
    const-string v8, "userDisplayName"

    invoke-virtual {v8, v4}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v8

    if-eqz v8, :cond_23

    const/4 v0, 0x0

    invoke-interface {v7, v0}, Lorg/xmlpull/v1/XmlPullParser;->getAttributeName(I)Ljava/lang/String;

    move-result-object v0

    const/4 v2, 0x0

    invoke-interface {v7, v2}, Lorg/xmlpull/v1/XmlPullParser;->getAttributeValue(I)Ljava/lang/String;

    move-result-object v2

    const-string v8, "mandatory"

    invoke-virtual {v8, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_22

    const-string v8, "1"

    invoke-virtual {v8, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_22

    invoke-virtual {v3}, Lcom/osp/app/signin/kd;->S()V

    :cond_22
    const/4 v8, 0x1

    iput-boolean v8, v3, Lcom/osp/app/signin/kd;->o:Z

    invoke-virtual {v3, v4}, Lcom/osp/app/signin/kd;->a(Ljava/lang/String;)V

    goto/16 :goto_3

    :cond_23
    const-string v8, "relationshipStatusCode"

    invoke-virtual {v8, v4}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v8

    if-eqz v8, :cond_25

    const/4 v0, 0x0

    invoke-interface {v7, v0}, Lorg/xmlpull/v1/XmlPullParser;->getAttributeName(I)Ljava/lang/String;

    move-result-object v0

    const/4 v2, 0x0

    invoke-interface {v7, v2}, Lorg/xmlpull/v1/XmlPullParser;->getAttributeValue(I)Ljava/lang/String;

    move-result-object v2

    const-string v8, "mandatory"

    invoke-virtual {v8, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_24

    const-string v8, "1"

    invoke-virtual {v8, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_24

    invoke-virtual {v3}, Lcom/osp/app/signin/kd;->U()V

    :cond_24
    const/4 v8, 0x1

    iput-boolean v8, v3, Lcom/osp/app/signin/kd;->p:Z

    invoke-virtual {v3, v4}, Lcom/osp/app/signin/kd;->a(Ljava/lang/String;)V

    goto/16 :goto_3

    :cond_25
    const-string v8, "securityAnswer"

    invoke-virtual {v8, v4}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v8

    if-eqz v8, :cond_27

    const/4 v0, 0x0

    invoke-interface {v7, v0}, Lorg/xmlpull/v1/XmlPullParser;->getAttributeName(I)Ljava/lang/String;

    move-result-object v0

    const/4 v2, 0x0

    invoke-interface {v7, v2}, Lorg/xmlpull/v1/XmlPullParser;->getAttributeValue(I)Ljava/lang/String;

    move-result-object v2

    const-string v8, "mandatory"

    invoke-virtual {v8, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_26

    const-string v8, "1"

    invoke-virtual {v8, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_26

    invoke-virtual {v3}, Lcom/osp/app/signin/kd;->V()V

    :cond_26
    const/4 v8, 0x1

    iput-boolean v8, v3, Lcom/osp/app/signin/kd;->q:Z

    invoke-virtual {v3, v4}, Lcom/osp/app/signin/kd;->a(Ljava/lang/String;)V

    goto/16 :goto_3

    :cond_27
    const-string v8, "securityQuestion"

    invoke-virtual {v8, v4}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v8

    if-eqz v8, :cond_7

    const/4 v0, 0x0

    invoke-interface {v7, v0}, Lorg/xmlpull/v1/XmlPullParser;->getAttributeName(I)Ljava/lang/String;

    move-result-object v0

    const/4 v2, 0x0

    invoke-interface {v7, v2}, Lorg/xmlpull/v1/XmlPullParser;->getAttributeValue(I)Ljava/lang/String;

    move-result-object v2

    const-string v8, "mandatory"

    invoke-virtual {v8, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_28

    const-string v8, "1"

    invoke-virtual {v8, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_28

    invoke-virtual {v3}, Lcom/osp/app/signin/kd;->W()V

    :cond_28
    const/4 v8, 0x1

    iput-boolean v8, v3, Lcom/osp/app/signin/kd;->r:Z

    invoke-virtual {v3, v4}, Lcom/osp/app/signin/kd;->a(Ljava/lang/String;)V
    :try_end_a
    .catchall {:try_start_a .. :try_end_a} :catchall_1

    goto/16 :goto_3

    :cond_29
    :try_start_b
    invoke-virtual {v1}, Ljava/io/StringReader;->close()V

    .line 7669
    iget-object v0, p0, Lcom/osp/app/signin/lz;->c:Lcom/osp/app/signin/SignUpView;

    invoke-static {v0}, Lcom/osp/app/signin/SignUpView;->T(Lcom/osp/app/signin/SignUpView;)Z

    move-result v0

    if-eqz v0, :cond_2a

    .line 7671
    iget-object v0, p0, Lcom/osp/app/signin/lz;->c:Lcom/osp/app/signin/SignUpView;

    iget-object v1, p0, Lcom/osp/app/signin/lz;->c:Lcom/osp/app/signin/SignUpView;

    invoke-static {v1}, Lcom/osp/app/signin/SignUpView;->S(Lcom/osp/app/signin/SignUpView;)Lcom/osp/app/signin/kd;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/osp/app/signin/SignUpView;->a(Lcom/osp/app/signin/SignUpView;Lcom/osp/app/signin/kd;)Lcom/osp/app/signin/kd;

    .line 7676
    :goto_5
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/osp/app/signin/lz;->f:Z

    goto/16 :goto_0

    .line 7674
    :cond_2a
    iget-object v0, p0, Lcom/osp/app/signin/lz;->c:Lcom/osp/app/signin/SignUpView;

    iget-object v1, p0, Lcom/osp/app/signin/lz;->c:Lcom/osp/app/signin/SignUpView;

    invoke-static {v1}, Lcom/osp/app/signin/SignUpView;->R(Lcom/osp/app/signin/SignUpView;)Lcom/osp/app/signin/kd;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/osp/app/signin/SignUpView;->a(Lcom/osp/app/signin/SignUpView;Lcom/osp/app/signin/kd;)Lcom/osp/app/signin/kd;
    :try_end_b
    .catch Ljava/lang/Exception; {:try_start_b .. :try_end_b} :catch_1
    .catchall {:try_start_b .. :try_end_b} :catchall_0

    goto :goto_5

    .line 7668
    :catchall_2
    move-exception v0

    move-object v1, v2

    goto/16 :goto_4

    :pswitch_data_0
    .packed-switch 0x2
        :pswitch_0
    .end packed-switch
.end method

.method protected final a(Ljava/lang/Boolean;)V
    .locals 4

    .prologue
    .line 7534
    invoke-super {p0, p1}, Lcom/msc/c/b;->a(Ljava/lang/Boolean;)V

    .line 7547
    iget-boolean v0, p0, Lcom/osp/app/signin/lz;->f:Z

    if-eqz v0, :cond_5

    .line 7550
    iget-object v0, p0, Lcom/osp/app/signin/lz;->c:Lcom/osp/app/signin/SignUpView;

    iget-object v1, p0, Lcom/osp/app/signin/lz;->c:Lcom/osp/app/signin/SignUpView;

    invoke-static {v1}, Lcom/osp/app/signin/SignUpView;->O(Lcom/osp/app/signin/SignUpView;)Lcom/osp/app/signin/kd;

    move-result-object v1

    iget-object v1, v1, Lcom/osp/app/signin/kd;->u:Ljava/util/ArrayList;

    invoke-static {v0, v1}, Lcom/osp/app/signin/SignUpView;->a(Lcom/osp/app/signin/SignUpView;Ljava/util/ArrayList;)Ljava/util/ArrayList;

    .line 7571
    invoke-static {}, Lcom/osp/app/signin/SamsungService;->a()Z

    move-result v0

    if-nez v0, :cond_4

    .line 7573
    const-string v0, "450"

    iget-object v1, p0, Lcom/osp/app/signin/lz;->c:Lcom/osp/app/signin/SignUpView;

    invoke-static {v1}, Lcom/osp/app/signin/SignUpView;->K(Lcom/osp/app/signin/SignUpView;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/osp/app/signin/lz;->c:Lcom/osp/app/signin/SignUpView;

    invoke-static {v0}, Lcom/osp/app/util/r;->s(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 7575
    :cond_0
    iget-object v0, p0, Lcom/osp/app/signin/lz;->c:Lcom/osp/app/signin/SignUpView;

    invoke-static {v0}, Lcom/osp/app/signin/SignUpView;->d(Lcom/osp/app/signin/SignUpView;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 7577
    iget-object v0, p0, Lcom/osp/app/signin/lz;->c:Lcom/osp/app/signin/SignUpView;

    iget-object v1, p0, Lcom/osp/app/signin/lz;->c:Lcom/osp/app/signin/SignUpView;

    invoke-static {v1}, Lcom/osp/app/signin/SignUpView;->f(Lcom/osp/app/signin/SignUpView;)Landroid/content/Intent;

    move-result-object v1

    const-string v2, "information_after_name_validation"

    invoke-virtual {v1, v2}, Landroid/content/Intent;->getBundleExtra(Ljava/lang/String;)Landroid/os/Bundle;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/osp/app/signin/SignUpView;->a(Lcom/osp/app/signin/SignUpView;Landroid/os/Bundle;)Landroid/os/Bundle;

    .line 7579
    iget-object v0, p0, Lcom/osp/app/signin/lz;->c:Lcom/osp/app/signin/SignUpView;

    invoke-static {v0}, Lcom/osp/app/signin/SignUpView;->e(Lcom/osp/app/signin/SignUpView;)Landroid/os/Bundle;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 7581
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    .line 7582
    const-string v1, "key_name_check_familyname"

    iget-object v2, p0, Lcom/osp/app/signin/lz;->c:Lcom/osp/app/signin/SignUpView;

    invoke-static {v2}, Lcom/osp/app/signin/SignUpView;->e(Lcom/osp/app/signin/SignUpView;)Landroid/os/Bundle;

    move-result-object v2

    const-string v3, "key_name_check_familyname"

    invoke-virtual {v2, v3}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 7584
    const-string v1, "key_name_check_givenname"

    iget-object v2, p0, Lcom/osp/app/signin/lz;->c:Lcom/osp/app/signin/SignUpView;

    invoke-static {v2}, Lcom/osp/app/signin/SignUpView;->e(Lcom/osp/app/signin/SignUpView;)Landroid/os/Bundle;

    move-result-object v2

    const-string v3, "key_name_check_givenname"

    invoke-virtual {v2, v3}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 7586
    const-string v1, "key_name_check_birthdate"

    iget-object v2, p0, Lcom/osp/app/signin/lz;->c:Lcom/osp/app/signin/SignUpView;

    invoke-static {v2}, Lcom/osp/app/signin/SignUpView;->e(Lcom/osp/app/signin/SignUpView;)Landroid/os/Bundle;

    move-result-object v2

    const-string v3, "key_name_check_birthdate"

    invoke-virtual {v2, v3}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 7588
    const-string v1, "key_name_check_mobile"

    iget-object v2, p0, Lcom/osp/app/signin/lz;->c:Lcom/osp/app/signin/SignUpView;

    invoke-static {v2}, Lcom/osp/app/signin/SignUpView;->e(Lcom/osp/app/signin/SignUpView;)Landroid/os/Bundle;

    move-result-object v2

    const-string v3, "key_name_check_mobile"

    invoke-virtual {v2, v3}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 7590
    const-string v1, "key_name_check_method"

    iget-object v2, p0, Lcom/osp/app/signin/lz;->c:Lcom/osp/app/signin/SignUpView;

    invoke-static {v2}, Lcom/osp/app/signin/SignUpView;->e(Lcom/osp/app/signin/SignUpView;)Landroid/os/Bundle;

    move-result-object v2

    const-string v3, "key_name_check_method"

    invoke-virtual {v2, v3}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 7591
    const-string v1, "key_name_check_ci"

    iget-object v2, p0, Lcom/osp/app/signin/lz;->c:Lcom/osp/app/signin/SignUpView;

    invoke-static {v2}, Lcom/osp/app/signin/SignUpView;->e(Lcom/osp/app/signin/SignUpView;)Landroid/os/Bundle;

    move-result-object v2

    const-string v3, "key_name_check_ci"

    invoke-virtual {v2, v3}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 7592
    const-string v1, "key_name_check_di"

    iget-object v2, p0, Lcom/osp/app/signin/lz;->c:Lcom/osp/app/signin/SignUpView;

    invoke-static {v2}, Lcom/osp/app/signin/SignUpView;->e(Lcom/osp/app/signin/SignUpView;)Landroid/os/Bundle;

    move-result-object v2

    const-string v3, "key_name_check_di"

    invoke-virtual {v2, v3}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 7593
    const-string v1, "key_name_check_gender"

    iget-object v2, p0, Lcom/osp/app/signin/lz;->c:Lcom/osp/app/signin/SignUpView;

    invoke-static {v2}, Lcom/osp/app/signin/SignUpView;->e(Lcom/osp/app/signin/SignUpView;)Landroid/os/Bundle;

    move-result-object v2

    const-string v3, "key_name_check_gender"

    invoke-virtual {v2, v3}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 7594
    const-string v1, "key_name_check_foreigner"

    iget-object v2, p0, Lcom/osp/app/signin/lz;->c:Lcom/osp/app/signin/SignUpView;

    invoke-static {v2}, Lcom/osp/app/signin/SignUpView;->e(Lcom/osp/app/signin/SignUpView;)Landroid/os/Bundle;

    move-result-object v2

    const-string v3, "key_name_check_foreigner"

    invoke-virtual {v2, v3}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 7597
    iget-object v1, p0, Lcom/osp/app/signin/lz;->c:Lcom/osp/app/signin/SignUpView;

    invoke-static {v1, v0}, Lcom/osp/app/signin/SignUpView;->a(Lcom/osp/app/signin/SignUpView;Landroid/content/Intent;)V

    .line 7615
    :goto_0
    iget-object v0, p0, Lcom/osp/app/signin/lz;->c:Lcom/osp/app/signin/SignUpView;

    invoke-virtual {v0}, Lcom/osp/app/signin/SignUpView;->a()V

    .line 7621
    :goto_1
    return-void

    .line 7600
    :cond_1
    iget-object v0, p0, Lcom/osp/app/signin/lz;->c:Lcom/osp/app/signin/SignUpView;

    invoke-static {v0}, Lcom/osp/app/signin/SignUpView;->P(Lcom/osp/app/signin/SignUpView;)V

    goto :goto_0

    .line 7604
    :cond_2
    iget-object v0, p0, Lcom/osp/app/signin/lz;->c:Lcom/osp/app/signin/SignUpView;

    invoke-static {v0}, Lcom/osp/app/signin/SignUpView;->P(Lcom/osp/app/signin/SignUpView;)V

    goto :goto_0

    .line 7608
    :cond_3
    iget-object v0, p0, Lcom/osp/app/signin/lz;->c:Lcom/osp/app/signin/SignUpView;

    invoke-static {v0}, Lcom/osp/app/signin/SignUpView;->Q(Lcom/osp/app/signin/SignUpView;)V

    goto :goto_0

    .line 7612
    :cond_4
    iget-object v0, p0, Lcom/osp/app/signin/lz;->c:Lcom/osp/app/signin/SignUpView;

    invoke-static {v0}, Lcom/osp/app/signin/SignUpView;->Q(Lcom/osp/app/signin/SignUpView;)V

    goto :goto_0

    .line 7618
    :cond_5
    iget-object v0, p0, Lcom/osp/app/signin/lz;->c:Lcom/osp/app/signin/SignUpView;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/osp/app/signin/SignUpView;->b(I)V

    .line 7619
    iget-object v0, p0, Lcom/osp/app/signin/lz;->c:Lcom/osp/app/signin/SignUpView;

    invoke-virtual {v0}, Lcom/osp/app/signin/SignUpView;->finish()V

    goto :goto_1
.end method

.method protected final d()V
    .locals 2

    .prologue
    .line 7509
    invoke-super {p0}, Lcom/msc/c/b;->d()V

    .line 7511
    iget-object v0, p0, Lcom/osp/app/signin/lz;->c:Lcom/osp/app/signin/SignUpView;

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lcom/osp/app/signin/SignUpView;->a(Lcom/osp/app/signin/SignUpView;Lcom/osp/app/signin/lz;)Lcom/osp/app/signin/lz;

    .line 7512
    iget-object v0, p0, Lcom/osp/app/signin/lz;->c:Lcom/osp/app/signin/SignUpView;

    const/16 v1, 0xe

    invoke-virtual {v0, v1}, Lcom/osp/app/signin/SignUpView;->setResult(I)V

    .line 7513
    iget-object v0, p0, Lcom/osp/app/signin/lz;->c:Lcom/osp/app/signin/SignUpView;

    invoke-virtual {v0}, Lcom/osp/app/signin/SignUpView;->finish()V

    .line 7514
    return-void
.end method

.method protected final synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 7479
    invoke-virtual {p0}, Lcom/osp/app/signin/lz;->a()Ljava/lang/Boolean;

    move-result-object v0

    return-object v0
.end method

.method protected final synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 7479
    check-cast p1, Ljava/lang/Boolean;

    invoke-virtual {p0, p1}, Lcom/osp/app/signin/lz;->a(Ljava/lang/Boolean;)V

    return-void
.end method

.method protected final onPreExecute()V
    .locals 0

    .prologue
    .line 7503
    invoke-super {p0}, Lcom/msc/c/b;->onPreExecute()V

    .line 7504
    return-void
.end method
