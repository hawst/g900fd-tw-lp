.class final Lcom/osp/app/signin/cb;
.super Lcom/msc/c/b;
.source "AccountinfoView.java"


# instance fields
.field final synthetic c:Lcom/osp/app/signin/AccountinfoView;

.field private d:Z

.field private e:J

.field private f:Lcom/osp/app/signin/SignUpinfo;


# virtual methods
.method protected final varargs a()Ljava/lang/Boolean;
    .locals 3

    .prologue
    .line 4637
    iget-object v0, p0, Lcom/osp/app/signin/cb;->f:Lcom/osp/app/signin/SignUpinfo;

    if-nez v0, :cond_0

    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v0, "AIV"

    const-string v1, "ModifyAccountInfoV01Task: reload info."

    invoke-static {v0, v1}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/osp/app/signin/cb;->c:Lcom/osp/app/signin/AccountinfoView;

    invoke-static {v0}, Lcom/osp/app/signin/AccountinfoView;->v(Lcom/osp/app/signin/AccountinfoView;)Lcom/osp/app/signin/SignUpinfo;

    move-result-object v0

    iput-object v0, p0, Lcom/osp/app/signin/cb;->f:Lcom/osp/app/signin/SignUpinfo;

    :cond_0
    invoke-static {}, Lcom/msc/c/g;->a()Lcom/msc/c/g;

    iget-object v0, p0, Lcom/osp/app/signin/cb;->c:Lcom/osp/app/signin/AccountinfoView;

    iget-object v1, p0, Lcom/osp/app/signin/cb;->c:Lcom/osp/app/signin/AccountinfoView;

    invoke-static {v1}, Lcom/msc/c/c;->d(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/osp/app/signin/cb;->f:Lcom/osp/app/signin/SignUpinfo;

    invoke-static {v0, v1, v2, p0}, Lcom/msc/c/g;->a(Landroid/content/Context;Ljava/lang/String;Lcom/osp/app/signin/SignUpinfo;Lcom/msc/b/h;)J

    move-result-wide v0

    iput-wide v0, p0, Lcom/osp/app/signin/cb;->e:J

    iget-wide v0, p0, Lcom/osp/app/signin/cb;->e:J

    invoke-virtual {p0, v0, v1}, Lcom/osp/app/signin/cb;->a(J)V

    iget-wide v0, p0, Lcom/osp/app/signin/cb;->e:J

    const-string v2, "from_xml"

    invoke-virtual {p0, v0, v1, v2}, Lcom/osp/app/signin/cb;->a(JLjava/lang/String;)V

    iget-wide v0, p0, Lcom/osp/app/signin/cb;->e:J

    sget-object v2, Lcom/msc/b/g;->d:Lcom/msc/b/g;

    invoke-static {v0, v1, v2}, Lcom/msc/c/g;->a(JLcom/msc/b/g;)V

    .line 4638
    const/4 v0, 0x0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    return-object v0
.end method

.method public final declared-synchronized a(Lcom/msc/b/c;)V
    .locals 6

    .prologue
    .line 4734
    monitor-enter p0

    :try_start_0
    invoke-super {p0, p1}, Lcom/msc/c/b;->a(Lcom/msc/b/c;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 4736
    if-nez p1, :cond_1

    .line 4757
    :cond_0
    :goto_0
    monitor-exit p0

    return-void

    .line 4741
    :cond_1
    :try_start_1
    invoke-virtual {p1}, Lcom/msc/b/c;->a()J

    move-result-wide v0

    .line 4742
    invoke-virtual {p1}, Lcom/msc/b/c;->e()Ljava/lang/String;

    move-result-object v2

    .line 4744
    iget-wide v4, p0, Lcom/osp/app/signin/cb;->e:J

    cmp-long v0, v0, v4

    if-nez v0, :cond_0

    .line 4746
    if-eqz v2, :cond_0

    .line 4748
    const-string v0, "true"

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 4750
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/osp/app/signin/cb;->d:Z
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 4734
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 4753
    :cond_2
    :try_start_2
    new-instance v0, Lcom/msc/c/f;

    invoke-direct {v0}, Lcom/msc/c/f;-><init>()V

    iput-object v0, p0, Lcom/osp/app/signin/cb;->b:Lcom/msc/c/f;
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0
.end method

.method protected final a(Ljava/lang/Boolean;)V
    .locals 9

    .prologue
    const/4 v7, 0x0

    const/4 v8, 0x1

    const/4 v6, 0x0

    .line 4643
    invoke-super {p0, p1}, Lcom/msc/c/b;->a(Ljava/lang/Boolean;)V

    .line 4644
    iget-object v0, p0, Lcom/osp/app/signin/cb;->b:Lcom/msc/c/f;

    if-eqz v0, :cond_3

    .line 4646
    iget-object v0, p0, Lcom/osp/app/signin/cb;->b:Lcom/msc/c/f;

    invoke-virtual {v0}, Lcom/msc/c/f;->a()Ljava/lang/String;

    move-result-object v0

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_2

    iget-object v0, p0, Lcom/osp/app/signin/cb;->b:Lcom/msc/c/f;

    invoke-virtual {v0}, Lcom/msc/c/f;->a()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v0

    if-lez v0, :cond_2

    .line 4648
    const-string v0, "USR_3117"

    iget-object v1, p0, Lcom/osp/app/signin/cb;->b:Lcom/msc/c/f;

    invoke-virtual {v1}, Lcom/msc/c/f;->a()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    const-string v0, "PRT_3011"

    iget-object v1, p0, Lcom/osp/app/signin/cb;->b:Lcom/msc/c/f;

    invoke-virtual {v1}, Lcom/msc/c/f;->a()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 4650
    :cond_0
    iget-object v0, p0, Lcom/osp/app/signin/cb;->c:Lcom/osp/app/signin/AccountinfoView;

    invoke-static {v0}, Lcom/osp/app/signin/AccountinfoView;->w(Lcom/osp/app/signin/AccountinfoView;)Lcom/osp/app/util/n;

    iget-object v0, p0, Lcom/osp/app/signin/cb;->b:Lcom/msc/c/f;

    invoke-virtual {v0}, Lcom/msc/c/f;->b()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/osp/app/util/n;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 4651
    invoke-static {v0}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v0

    .line 4652
    invoke-static {}, Lcom/osp/app/util/ap;->a()Lcom/osp/app/util/ap;

    iget-object v1, p0, Lcom/osp/app/signin/cb;->c:Lcom/osp/app/signin/AccountinfoView;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v3, p0, Lcom/osp/app/signin/cb;->c:Lcom/osp/app/signin/AccountinfoView;

    invoke-virtual {v3}, Lcom/osp/app/signin/AccountinfoView;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f090060

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    new-array v4, v8, [Ljava/lang/Object;

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    aput-object v0, v4, v6

    invoke-static {v3, v4}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, "("

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v2, p0, Lcom/osp/app/signin/cb;->b:Lcom/msc/c/f;

    invoke-virtual {v2}, Lcom/msc/c/f;->a()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, ")"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0, v8}, Lcom/osp/app/util/ap;->a(Landroid/content/Context;Ljava/lang/String;I)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    .line 4730
    :goto_0
    return-void

    .line 4659
    :cond_1
    invoke-virtual {p0, v6}, Lcom/osp/app/signin/cb;->a(Z)V

    goto :goto_0

    .line 4664
    :cond_2
    invoke-virtual {p0, v6}, Lcom/osp/app/signin/cb;->a(Z)V

    goto :goto_0

    .line 4668
    :cond_3
    iget-boolean v0, p0, Lcom/osp/app/signin/cb;->d:Z

    if-eqz v0, :cond_9

    .line 4670
    const-string v0, "%04d%02d%02d"

    const/4 v1, 0x3

    new-array v1, v1, [Ljava/lang/Object;

    iget-object v2, p0, Lcom/osp/app/signin/cb;->c:Lcom/osp/app/signin/AccountinfoView;

    invoke-static {v2}, Lcom/osp/app/signin/AccountinfoView;->b(Lcom/osp/app/signin/AccountinfoView;)I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v1, v6

    iget-object v2, p0, Lcom/osp/app/signin/cb;->c:Lcom/osp/app/signin/AccountinfoView;

    invoke-static {v2}, Lcom/osp/app/signin/AccountinfoView;->c(Lcom/osp/app/signin/AccountinfoView;)I

    move-result v2

    add-int/lit8 v2, v2, 0x1

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v1, v8

    const/4 v2, 0x2

    iget-object v3, p0, Lcom/osp/app/signin/cb;->c:Lcom/osp/app/signin/AccountinfoView;

    invoke-static {v3}, Lcom/osp/app/signin/AccountinfoView;->d(Lcom/osp/app/signin/AccountinfoView;)I

    move-result v3

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v1, v2

    invoke-static {v7, v0, v1}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 4672
    iget-object v1, p0, Lcom/osp/app/signin/cb;->c:Lcom/osp/app/signin/AccountinfoView;

    invoke-static {v1, v0}, Lcom/msc/c/e;->c(Landroid/content/Context;Ljava/lang/String;)V

    .line 4673
    invoke-static {}, Lcom/osp/app/util/ap;->a()Lcom/osp/app/util/ap;

    iget-object v0, p0, Lcom/osp/app/signin/cb;->c:Lcom/osp/app/signin/AccountinfoView;

    iget-object v1, p0, Lcom/osp/app/signin/cb;->c:Lcom/osp/app/signin/AccountinfoView;

    const v2, 0x7f090050

    invoke-virtual {v1, v2}, Lcom/osp/app/signin/AccountinfoView;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1, v6}, Lcom/osp/app/util/ap;->a(Landroid/content/Context;Ljava/lang/String;I)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    .line 4675
    invoke-static {}, Lcom/osp/app/signin/SamsungService;->p()Z

    move-result v0

    if-eqz v0, :cond_4

    .line 4677
    iget-object v0, p0, Lcom/osp/app/signin/cb;->c:Lcom/osp/app/signin/AccountinfoView;

    iget-object v1, p0, Lcom/osp/app/signin/cb;->c:Lcom/osp/app/signin/AccountinfoView;

    invoke-static {v1}, Lcom/osp/app/signin/AccountinfoView;->q(Lcom/osp/app/signin/AccountinfoView;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1, v6}, Lcom/msc/openprovider/b;->a(Landroid/content/Context;Ljava/lang/String;Z)V

    .line 4680
    :cond_4
    iget-object v0, p0, Lcom/osp/app/signin/cb;->c:Lcom/osp/app/signin/AccountinfoView;

    invoke-static {v0}, Lcom/osp/app/signin/AccountinfoView;->x(Lcom/osp/app/signin/AccountinfoView;)Z

    move-result v0

    if-eqz v0, :cond_6

    .line 4682
    iget-object v0, p0, Lcom/osp/app/signin/cb;->c:Lcom/osp/app/signin/AccountinfoView;

    invoke-static {v0}, Lcom/osp/app/signin/AccountinfoView;->y(Lcom/osp/app/signin/AccountinfoView;)V

    .line 4699
    :goto_1
    iget-object v0, p0, Lcom/osp/app/signin/cb;->c:Lcom/osp/app/signin/AccountinfoView;

    invoke-static {v0}, Lcom/osp/app/signin/AccountinfoView;->z(Lcom/osp/app/signin/AccountinfoView;)I

    move-result v0

    and-int/lit8 v0, v0, 0x10

    const/16 v1, 0x10

    if-ne v0, v1, :cond_5

    .line 4701
    iget-object v0, p0, Lcom/osp/app/signin/cb;->c:Lcom/osp/app/signin/AccountinfoView;

    invoke-static {v0}, Lcom/osp/app/signin/AccountinfoView;->A(Lcom/osp/app/signin/AccountinfoView;)I

    .line 4704
    :cond_5
    iget-object v0, p0, Lcom/osp/app/signin/cb;->c:Lcom/osp/app/signin/AccountinfoView;

    invoke-static {v0}, Lcom/osp/app/signin/AccountinfoView;->z(Lcom/osp/app/signin/AccountinfoView;)I

    move-result v0

    if-lez v0, :cond_8

    .line 4707
    iget-object v0, p0, Lcom/osp/app/signin/cb;->c:Lcom/osp/app/signin/AccountinfoView;

    iget-object v1, p0, Lcom/osp/app/signin/cb;->c:Lcom/osp/app/signin/AccountinfoView;

    invoke-static {v1}, Lcom/osp/app/signin/AccountinfoView;->z(Lcom/osp/app/signin/AccountinfoView;)I

    move-result v1

    iget-object v2, p0, Lcom/osp/app/signin/cb;->c:Lcom/osp/app/signin/AccountinfoView;

    invoke-static {v2}, Lcom/msc/c/e;->h(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v2

    iget-object v3, p0, Lcom/osp/app/signin/cb;->c:Lcom/osp/app/signin/AccountinfoView;

    invoke-static {v3}, Lcom/osp/app/signin/AccountinfoView;->q(Lcom/osp/app/signin/AccountinfoView;)Ljava/lang/String;

    move-result-object v3

    iget-object v4, p0, Lcom/osp/app/signin/cb;->c:Lcom/osp/app/signin/AccountinfoView;

    invoke-static {v4}, Lcom/osp/app/signin/AccountinfoView;->B(Lcom/osp/app/signin/AccountinfoView;)Ljava/lang/String;

    move-result-object v4

    iget-object v5, p0, Lcom/osp/app/signin/cb;->c:Lcom/osp/app/signin/AccountinfoView;

    invoke-static {v5}, Lcom/osp/app/signin/AccountinfoView;->C(Lcom/osp/app/signin/AccountinfoView;)Z

    move-result v5

    invoke-static/range {v0 .. v7}, Lcom/osp/app/util/ad;->a(Landroid/content/Context;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;ZZLjava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    .line 4709
    iget-object v1, p0, Lcom/osp/app/signin/cb;->c:Lcom/osp/app/signin/AccountinfoView;

    invoke-static {v1}, Lcom/osp/app/signin/AccountinfoView;->D(Lcom/osp/app/signin/AccountinfoView;)Z

    move-result v1

    if-eqz v1, :cond_7

    .line 4711
    const-string v1, "key_return_result"

    invoke-virtual {v0, v1, v8}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 4712
    iget-object v1, p0, Lcom/osp/app/signin/cb;->c:Lcom/osp/app/signin/AccountinfoView;

    const/16 v2, 0xdc

    invoke-virtual {v1, v0, v2}, Lcom/osp/app/signin/AccountinfoView;->startActivityForResult(Landroid/content/Intent;I)V

    goto/16 :goto_0

    .line 4685
    :cond_6
    iget-object v0, p0, Lcom/osp/app/signin/cb;->c:Lcom/osp/app/signin/AccountinfoView;

    const/4 v1, -0x1

    iget-object v2, p0, Lcom/osp/app/signin/cb;->c:Lcom/osp/app/signin/AccountinfoView;

    invoke-static {v2}, Lcom/osp/app/signin/AccountinfoView;->l(Lcom/osp/app/signin/AccountinfoView;)Landroid/content/Intent;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/osp/app/signin/AccountinfoView;->a(ILandroid/content/Intent;)V

    .line 4686
    new-instance v0, Landroid/content/Intent;

    const-string v1, "android.intent.action.SAMSUNGACCOUNT_SIGNUPINFO_CHANGED"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 4688
    const-string v1, "SA_EMAIL_;SA_COUNTRY_;SA_BIRTHDATE_;SA_EMAILRECEIVE_;"

    .line 4696
    const-string v2, "signUpInfo"

    invoke-virtual {v0, v2, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 4697
    iget-object v1, p0, Lcom/osp/app/signin/cb;->c:Lcom/osp/app/signin/AccountinfoView;

    invoke-virtual {v1, v0}, Lcom/osp/app/signin/AccountinfoView;->sendBroadcast(Landroid/content/Intent;)V

    goto :goto_1

    .line 4716
    :cond_7
    iget-object v1, p0, Lcom/osp/app/signin/cb;->c:Lcom/osp/app/signin/AccountinfoView;

    invoke-virtual {v1, v0}, Lcom/osp/app/signin/AccountinfoView;->startActivity(Landroid/content/Intent;)V

    .line 4725
    :cond_8
    iget-object v0, p0, Lcom/osp/app/signin/cb;->c:Lcom/osp/app/signin/AccountinfoView;

    invoke-virtual {v0}, Lcom/osp/app/signin/AccountinfoView;->finish()V

    goto/16 :goto_0

    .line 4728
    :cond_9
    invoke-virtual {p0, v6}, Lcom/osp/app/signin/cb;->a(Z)V

    goto/16 :goto_0
.end method

.method protected final d()V
    .locals 2

    .prologue
    .line 4623
    invoke-super {p0}, Lcom/msc/c/b;->d()V

    .line 4624
    iget-object v0, p0, Lcom/osp/app/signin/cb;->c:Lcom/osp/app/signin/AccountinfoView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/osp/app/signin/AccountinfoView;->b(I)V

    .line 4625
    iget-object v0, p0, Lcom/osp/app/signin/cb;->c:Lcom/osp/app/signin/AccountinfoView;

    invoke-virtual {v0}, Lcom/osp/app/signin/AccountinfoView;->finish()V

    .line 4626
    return-void
.end method

.method protected final synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 4608
    invoke-virtual {p0}, Lcom/osp/app/signin/cb;->a()Ljava/lang/Boolean;

    move-result-object v0

    return-object v0
.end method

.method protected final synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 4608
    check-cast p1, Ljava/lang/Boolean;

    invoke-virtual {p0, p1}, Lcom/osp/app/signin/cb;->a(Ljava/lang/Boolean;)V

    return-void
.end method

.method protected final onPreExecute()V
    .locals 1

    .prologue
    .line 4630
    invoke-super {p0}, Lcom/msc/c/b;->onPreExecute()V

    .line 4632
    iget-object v0, p0, Lcom/osp/app/signin/cb;->c:Lcom/osp/app/signin/AccountinfoView;

    invoke-static {v0}, Lcom/osp/app/signin/AccountinfoView;->v(Lcom/osp/app/signin/AccountinfoView;)Lcom/osp/app/signin/SignUpinfo;

    move-result-object v0

    iput-object v0, p0, Lcom/osp/app/signin/cb;->f:Lcom/osp/app/signin/SignUpinfo;

    .line 4633
    return-void
.end method
