.class final Lcom/osp/app/signin/bz;
.super Lcom/msc/c/b;
.source "AccountinfoView.java"


# instance fields
.field protected c:Z

.field final synthetic d:Lcom/osp/app/signin/AccountinfoView;

.field private e:J

.field private f:J

.field private g:Z


# direct methods
.method private h()V
    .locals 3

    .prologue
    .line 4277
    iget-object v0, p0, Lcom/osp/app/signin/bz;->d:Lcom/osp/app/signin/AccountinfoView;

    invoke-static {v0}, Lcom/msc/c/c;->d(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v0

    .line 4279
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 4281
    new-instance v0, Ljava/lang/Exception;

    invoke-direct {v0}, Ljava/lang/Exception;-><init>()V

    throw v0

    .line 4284
    :cond_0
    invoke-static {}, Lcom/msc/c/g;->a()Lcom/msc/c/g;

    .line 4285
    iget-object v1, p0, Lcom/osp/app/signin/bz;->d:Lcom/osp/app/signin/AccountinfoView;

    invoke-static {v1, v0, p0}, Lcom/msc/c/g;->d(Landroid/content/Context;Ljava/lang/String;Lcom/msc/b/h;)J

    move-result-wide v0

    iput-wide v0, p0, Lcom/osp/app/signin/bz;->f:J

    .line 4286
    iget-wide v0, p0, Lcom/osp/app/signin/bz;->f:J

    invoke-virtual {p0, v0, v1}, Lcom/osp/app/signin/bz;->a(J)V

    .line 4287
    iget-wide v0, p0, Lcom/osp/app/signin/bz;->f:J

    const-string v2, "from_xml"

    invoke-virtual {p0, v0, v1, v2}, Lcom/osp/app/signin/bz;->a(JLjava/lang/String;)V

    .line 4288
    iget-wide v0, p0, Lcom/osp/app/signin/bz;->f:J

    sget-object v2, Lcom/msc/b/g;->b:Lcom/msc/b/g;

    invoke-static {v0, v1, v2}, Lcom/msc/c/g;->a(JLcom/msc/b/g;)V

    .line 4289
    return-void
.end method


# virtual methods
.method protected final varargs a()Ljava/lang/Boolean;
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 4081
    const/4 v0, 0x1

    .line 4082
    const-string v2, "ACCOUNTINFO_MODIFY"

    iget-object v3, p0, Lcom/osp/app/signin/bz;->d:Lcom/osp/app/signin/AccountinfoView;

    invoke-static {v3}, Lcom/osp/app/signin/AccountinfoView;->p(Lcom/osp/app/signin/AccountinfoView;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    move v0, v1

    .line 4086
    :cond_0
    iget-object v2, p0, Lcom/osp/app/signin/bz;->d:Lcom/osp/app/signin/AccountinfoView;

    invoke-static {v2}, Lcom/osp/app/signin/AccountinfoView;->q(Lcom/osp/app/signin/AccountinfoView;)Ljava/lang/String;

    move-result-object v2

    invoke-static {}, Lcom/msc/c/g;->a()Lcom/msc/c/g;

    iget-object v3, p0, Lcom/osp/app/signin/bz;->d:Lcom/osp/app/signin/AccountinfoView;

    invoke-static {v3, v2, v0, p0}, Lcom/msc/c/g;->a(Landroid/content/Context;Ljava/lang/String;ZLcom/msc/b/h;)J

    move-result-wide v2

    iput-wide v2, p0, Lcom/osp/app/signin/bz;->e:J

    iget-wide v2, p0, Lcom/osp/app/signin/bz;->e:J

    invoke-virtual {p0, v2, v3}, Lcom/osp/app/signin/bz;->a(J)V

    iget-wide v2, p0, Lcom/osp/app/signin/bz;->e:J

    const-string v0, "from_xml"

    invoke-virtual {p0, v2, v3, v0}, Lcom/osp/app/signin/bz;->a(JLjava/lang/String;)V

    iget-wide v2, p0, Lcom/osp/app/signin/bz;->e:J

    sget-object v0, Lcom/msc/b/g;->c:Lcom/msc/b/g;

    invoke-static {v2, v3, v0}, Lcom/msc/c/g;->a(JLcom/msc/b/g;)V

    .line 4087
    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    return-object v0
.end method

.method public final declared-synchronized a(Lcom/msc/b/c;)V
    .locals 6

    .prologue
    .line 4117
    monitor-enter p0

    :try_start_0
    invoke-super {p0, p1}, Lcom/msc/c/b;->a(Lcom/msc/b/c;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 4119
    if-nez p1, :cond_1

    .line 4154
    :cond_0
    :goto_0
    monitor-exit p0

    return-void

    .line 4124
    :cond_1
    :try_start_1
    invoke-virtual {p1}, Lcom/msc/b/c;->a()J

    move-result-wide v0

    .line 4125
    invoke-virtual {p1}, Lcom/msc/b/c;->e()Ljava/lang/String;

    move-result-object v2

    .line 4127
    iget-wide v4, p0, Lcom/osp/app/signin/bz;->e:J
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    cmp-long v3, v0, v4

    if-nez v3, :cond_2

    .line 4131
    :try_start_2
    iget-object v0, p0, Lcom/osp/app/signin/bz;->d:Lcom/osp/app/signin/AccountinfoView;

    invoke-static {}, Lcom/msc/c/h;->a()Lcom/msc/c/h;

    invoke-static {v2}, Lcom/msc/c/h;->j(Ljava/lang/String;)Lcom/osp/app/signin/kd;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/osp/app/signin/AccountinfoView;->a(Lcom/osp/app/signin/AccountinfoView;Lcom/osp/app/signin/kd;)Lcom/osp/app/signin/kd;

    .line 4132
    invoke-virtual {p0}, Lcom/osp/app/signin/bz;->isCancelled()Z

    move-result v0

    if-nez v0, :cond_0

    .line 4136
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/osp/app/signin/bz;->g:Z

    .line 4137
    invoke-direct {p0}, Lcom/osp/app/signin/bz;->h()V
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_0
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0

    .line 4138
    :catch_0
    move-exception v0

    .line 4140
    :try_start_3
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    .line 4141
    new-instance v1, Lcom/msc/c/f;

    invoke-direct {v1, v0}, Lcom/msc/c/f;-><init>(Ljava/lang/Exception;)V

    iput-object v1, p0, Lcom/osp/app/signin/bz;->b:Lcom/msc/c/f;
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    goto :goto_0

    .line 4117
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 4143
    :cond_2
    :try_start_4
    iget-wide v4, p0, Lcom/osp/app/signin/bz;->f:J
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    cmp-long v0, v0, v4

    if-nez v0, :cond_0

    .line 4147
    :try_start_5
    iget-object v0, p0, Lcom/osp/app/signin/bz;->d:Lcom/osp/app/signin/AccountinfoView;

    invoke-static {}, Lcom/msc/c/h;->a()Lcom/msc/c/h;

    invoke-static {v2}, Lcom/msc/c/h;->n(Ljava/lang/String;)Lcom/osp/app/signin/SignUpinfo;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/osp/app/signin/AccountinfoView;->a(Lcom/osp/app/signin/AccountinfoView;Lcom/osp/app/signin/SignUpinfo;)Lcom/osp/app/signin/SignUpinfo;
    :try_end_5
    .catch Ljava/lang/Exception; {:try_start_5 .. :try_end_5} :catch_1
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    goto :goto_0

    .line 4148
    :catch_1
    move-exception v0

    .line 4150
    :try_start_6
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    .line 4151
    new-instance v1, Lcom/msc/c/f;

    invoke-direct {v1, v0}, Lcom/msc/c/f;-><init>(Ljava/lang/Exception;)V

    iput-object v1, p0, Lcom/osp/app/signin/bz;->b:Lcom/msc/c/f;
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_0

    goto :goto_0
.end method

.method protected final a(Ljava/lang/Boolean;)V
    .locals 3

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 4092
    invoke-super {p0, p1}, Lcom/msc/c/b;->a(Ljava/lang/Boolean;)V

    .line 4093
    iget-object v0, p0, Lcom/osp/app/signin/bz;->b:Lcom/msc/c/f;

    if-eqz v0, :cond_1

    .line 4095
    invoke-virtual {p0, v1}, Lcom/osp/app/signin/bz;->a(Z)V

    .line 4096
    iget-boolean v0, p0, Lcom/osp/app/signin/bz;->c:Z

    if-eqz v0, :cond_0

    .line 4098
    iget-object v0, p0, Lcom/osp/app/signin/bz;->d:Lcom/osp/app/signin/AccountinfoView;

    invoke-static {v0}, Lcom/osp/app/signin/AccountinfoView;->u(Lcom/osp/app/signin/AccountinfoView;)V

    .line 4100
    :cond_0
    iget-object v0, p0, Lcom/osp/app/signin/bz;->d:Lcom/osp/app/signin/AccountinfoView;

    iget-object v1, p0, Lcom/osp/app/signin/bz;->d:Lcom/osp/app/signin/AccountinfoView;

    invoke-static {v1}, Lcom/osp/app/signin/AccountinfoView;->l(Lcom/osp/app/signin/AccountinfoView;)Landroid/content/Intent;

    move-result-object v1

    invoke-virtual {v0, v2, v1}, Lcom/osp/app/signin/AccountinfoView;->a(ILandroid/content/Intent;)V

    .line 4101
    iget-object v0, p0, Lcom/osp/app/signin/bz;->d:Lcom/osp/app/signin/AccountinfoView;

    invoke-virtual {v0}, Lcom/osp/app/signin/AccountinfoView;->finish()V

    .line 4113
    :goto_0
    return-void

    .line 4104
    :cond_1
    iget-object v0, p0, Lcom/osp/app/signin/bz;->d:Lcom/osp/app/signin/AccountinfoView;

    invoke-static {v0}, Lcom/osp/app/signin/AccountinfoView;->s(Lcom/osp/app/signin/AccountinfoView;)Lcom/osp/app/signin/kd;

    move-result-object v0

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/osp/app/signin/bz;->d:Lcom/osp/app/signin/AccountinfoView;

    invoke-static {v0}, Lcom/osp/app/signin/AccountinfoView;->j(Lcom/osp/app/signin/AccountinfoView;)Lcom/osp/app/signin/SignUpinfo;

    move-result-object v0

    if-eqz v0, :cond_2

    .line 4106
    iget-object v0, p0, Lcom/osp/app/signin/bz;->d:Lcom/osp/app/signin/AccountinfoView;

    invoke-static {v0}, Lcom/osp/app/signin/AccountinfoView;->t(Lcom/osp/app/signin/AccountinfoView;)V

    goto :goto_0

    .line 4109
    :cond_2
    invoke-virtual {p0, v1}, Lcom/osp/app/signin/bz;->a(Z)V

    .line 4110
    iget-object v0, p0, Lcom/osp/app/signin/bz;->d:Lcom/osp/app/signin/AccountinfoView;

    iget-object v1, p0, Lcom/osp/app/signin/bz;->d:Lcom/osp/app/signin/AccountinfoView;

    invoke-static {v1}, Lcom/osp/app/signin/AccountinfoView;->l(Lcom/osp/app/signin/AccountinfoView;)Landroid/content/Intent;

    move-result-object v1

    invoke-virtual {v0, v2, v1}, Lcom/osp/app/signin/AccountinfoView;->a(ILandroid/content/Intent;)V

    .line 4111
    iget-object v0, p0, Lcom/osp/app/signin/bz;->d:Lcom/osp/app/signin/AccountinfoView;

    invoke-virtual {v0}, Lcom/osp/app/signin/AccountinfoView;->finish()V

    goto :goto_0
.end method

.method public final b(Lcom/msc/b/c;)V
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 4158
    invoke-super {p0, p1}, Lcom/msc/c/b;->b(Lcom/msc/b/c;)V

    .line 4160
    if-nez p1, :cond_1

    .line 4261
    :cond_0
    :goto_0
    return-void

    .line 4165
    :cond_1
    invoke-virtual {p1}, Lcom/msc/b/c;->a()J

    move-result-wide v0

    .line 4169
    iget-wide v2, p0, Lcom/osp/app/signin/bz;->f:J

    cmp-long v0, v0, v2

    if-nez v0, :cond_0

    .line 4171
    iget-boolean v0, p0, Lcom/osp/app/signin/bz;->g:Z

    if-nez v0, :cond_0

    const-string v0, "SSO_2101"

    iget-object v1, p0, Lcom/osp/app/signin/bz;->b:Lcom/msc/c/f;

    invoke-virtual {v1}, Lcom/msc/c/f;->a()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 4173
    iput-object v4, p0, Lcom/osp/app/signin/bz;->b:Lcom/msc/c/f;

    .line 4177
    :try_start_0
    new-instance v0, Lcom/osp/security/credential/a;

    iget-object v1, p0, Lcom/osp/app/signin/bz;->d:Lcom/osp/app/signin/AccountinfoView;

    invoke-direct {v0, v1}, Lcom/osp/security/credential/a;-><init>(Landroid/content/Context;)V

    .line 4178
    const-string v1, "14eev3f64b"

    invoke-virtual {v0, v1}, Lcom/osp/security/credential/a;->c(Ljava/lang/String;)V
    :try_end_0
    .catch Lcom/osp/security/credential/CredentialException; {:try_start_0 .. :try_end_0} :catch_1

    .line 4185
    :goto_1
    :try_start_1
    invoke-virtual {p0}, Lcom/osp/app/signin/bz;->isCancelled()Z

    move-result v0

    if-nez v0, :cond_0

    .line 4189
    new-instance v0, Lcom/osp/security/identity/d;

    iget-object v1, p0, Lcom/osp/app/signin/bz;->d:Lcom/osp/app/signin/AccountinfoView;

    invoke-direct {v0, v1}, Lcom/osp/security/identity/d;-><init>(Landroid/content/Context;)V

    .line 4190
    const-string v1, "14eev3f64b"

    const-string v2, "109E2830E09DB340924B8ABE0D6290C3"

    invoke-virtual {v0, v1, v2}, Lcom/osp/security/identity/d;->a(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_1
    .catch Lcom/osp/security/identity/IdentityException; {:try_start_1 .. :try_end_1} :catch_2

    .line 4245
    :cond_2
    :goto_2
    :try_start_2
    iget-object v0, p0, Lcom/osp/app/signin/bz;->b:Lcom/msc/c/f;

    if-nez v0, :cond_0

    .line 4247
    invoke-virtual {p0}, Lcom/osp/app/signin/bz;->isCancelled()Z

    move-result v0

    if-nez v0, :cond_0

    .line 4251
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/osp/app/signin/bz;->g:Z

    .line 4252
    invoke-direct {p0}, Lcom/osp/app/signin/bz;->h()V
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_0

    goto :goto_0

    .line 4254
    :catch_0
    move-exception v0

    .line 4256
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    .line 4257
    new-instance v1, Lcom/msc/c/f;

    invoke-direct {v1, v0}, Lcom/msc/c/f;-><init>(Ljava/lang/Exception;)V

    iput-object v1, p0, Lcom/osp/app/signin/bz;->b:Lcom/msc/c/f;

    goto :goto_0

    .line 4179
    :catch_1
    move-exception v0

    invoke-virtual {v0}, Lcom/osp/security/credential/CredentialException;->printStackTrace()V

    goto :goto_1

    .line 4196
    :catch_2
    move-exception v0

    .line 4198
    invoke-virtual {v0}, Lcom/osp/security/identity/IdentityException;->printStackTrace()V

    .line 4199
    new-instance v1, Lcom/msc/c/f;

    invoke-direct {v1, v0}, Lcom/msc/c/f;-><init>(Ljava/lang/Exception;)V

    iput-object v1, p0, Lcom/osp/app/signin/bz;->b:Lcom/msc/c/f;

    .line 4200
    const-string v1, "SSO_2101"

    invoke-virtual {v0}, Lcom/osp/security/identity/IdentityException;->a()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 4202
    iput-object v4, p0, Lcom/osp/app/signin/bz;->b:Lcom/msc/c/f;

    .line 4203
    new-instance v0, Lcom/osp/security/identity/k;

    invoke-direct {v0}, Lcom/osp/security/identity/k;-><init>()V

    .line 4204
    invoke-virtual {v0}, Lcom/osp/security/identity/k;->b()V

    .line 4205
    invoke-virtual {v0}, Lcom/osp/security/identity/k;->c()V

    .line 4206
    invoke-virtual {v0}, Lcom/osp/security/identity/k;->d()V

    .line 4210
    iget-object v1, p0, Lcom/osp/app/signin/bz;->d:Lcom/osp/app/signin/AccountinfoView;

    invoke-static {v1}, Lcom/osp/app/util/r;->b(Landroid/content/Context;)Z

    move-result v1

    if-eqz v1, :cond_3

    .line 4212
    invoke-virtual {v0}, Lcom/osp/security/identity/k;->e()V

    .line 4216
    :cond_3
    invoke-virtual {p0}, Lcom/osp/app/signin/bz;->isCancelled()Z

    move-result v0

    if-nez v0, :cond_0

    .line 4222
    iget-object v0, p0, Lcom/osp/app/signin/bz;->d:Lcom/osp/app/signin/AccountinfoView;

    iget-object v1, p0, Lcom/osp/app/signin/bz;->d:Lcom/osp/app/signin/AccountinfoView;

    invoke-static {v1}, Lcom/osp/app/util/ad;->h(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/osp/app/signin/AccountinfoView;->a(Lcom/osp/app/signin/AccountinfoView;Ljava/lang/String;)V

    goto :goto_2
.end method

.method protected final d()V
    .locals 2

    .prologue
    .line 4074
    invoke-super {p0}, Lcom/msc/c/b;->d()V

    .line 4075
    iget-object v0, p0, Lcom/osp/app/signin/bz;->d:Lcom/osp/app/signin/AccountinfoView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/osp/app/signin/AccountinfoView;->b(I)V

    .line 4076
    iget-object v0, p0, Lcom/osp/app/signin/bz;->d:Lcom/osp/app/signin/AccountinfoView;

    invoke-virtual {v0}, Lcom/osp/app/signin/AccountinfoView;->finish()V

    .line 4077
    return-void
.end method

.method protected final synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 4058
    invoke-virtual {p0}, Lcom/osp/app/signin/bz;->a()Ljava/lang/Boolean;

    move-result-object v0

    return-object v0
.end method

.method protected final synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 4058
    check-cast p1, Ljava/lang/Boolean;

    invoke-virtual {p0, p1}, Lcom/osp/app/signin/bz;->a(Ljava/lang/Boolean;)V

    return-void
.end method
