.class final Lcom/osp/app/signin/mn;
.super Ljava/lang/Object;
.source "SignUpWithFacebookWebView.java"

# interfaces
.implements Landroid/content/DialogInterface$OnKeyListener;


# instance fields
.field final synthetic a:Lcom/osp/app/signin/mm;


# direct methods
.method constructor <init>(Lcom/osp/app/signin/mm;)V
    .locals 0

    .prologue
    .line 227
    iput-object p1, p0, Lcom/osp/app/signin/mn;->a:Lcom/osp/app/signin/mm;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onKey(Landroid/content/DialogInterface;ILandroid/view/KeyEvent;)Z
    .locals 4

    .prologue
    const/4 v0, 0x0

    .line 231
    const/4 v1, 0x4

    invoke-virtual {p3}, Landroid/view/KeyEvent;->getKeyCode()I

    move-result v2

    if-ne v1, v2, :cond_1

    .line 235
    :try_start_0
    iget-object v1, p0, Lcom/osp/app/signin/mn;->a:Lcom/osp/app/signin/mm;

    iget-object v1, v1, Lcom/osp/app/signin/mm;->a:Lcom/osp/app/signin/SignUpWithFacebookWebView;

    invoke-virtual {v1}, Lcom/osp/app/signin/SignUpWithFacebookWebView;->isFinishing()Z

    move-result v1

    if-nez v1, :cond_0

    iget-object v1, p0, Lcom/osp/app/signin/mn;->a:Lcom/osp/app/signin/mm;

    iget-object v1, v1, Lcom/osp/app/signin/mm;->a:Lcom/osp/app/signin/SignUpWithFacebookWebView;

    invoke-static {v1}, Lcom/osp/app/signin/SignUpWithFacebookWebView;->a(Lcom/osp/app/signin/SignUpWithFacebookWebView;)Landroid/app/ProgressDialog;

    move-result-object v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/osp/app/signin/mn;->a:Lcom/osp/app/signin/mm;

    iget-object v1, v1, Lcom/osp/app/signin/mm;->a:Lcom/osp/app/signin/SignUpWithFacebookWebView;

    invoke-static {v1}, Lcom/osp/app/signin/SignUpWithFacebookWebView;->a(Lcom/osp/app/signin/SignUpWithFacebookWebView;)Landroid/app/ProgressDialog;

    move-result-object v1

    invoke-virtual {v1}, Landroid/app/ProgressDialog;->isShowing()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 237
    iget-object v1, p0, Lcom/osp/app/signin/mn;->a:Lcom/osp/app/signin/mm;

    iget-object v1, v1, Lcom/osp/app/signin/mm;->a:Lcom/osp/app/signin/SignUpWithFacebookWebView;

    invoke-static {v1}, Lcom/osp/app/signin/SignUpWithFacebookWebView;->a(Lcom/osp/app/signin/SignUpWithFacebookWebView;)Landroid/app/ProgressDialog;

    move-result-object v1

    invoke-virtual {v1}, Landroid/app/ProgressDialog;->dismiss()V

    .line 238
    iget-object v1, p0, Lcom/osp/app/signin/mn;->a:Lcom/osp/app/signin/mm;

    iget-object v1, v1, Lcom/osp/app/signin/mm;->a:Lcom/osp/app/signin/SignUpWithFacebookWebView;

    const/4 v2, 0x0

    invoke-static {v1, v2}, Lcom/osp/app/signin/SignUpWithFacebookWebView;->a(Lcom/osp/app/signin/SignUpWithFacebookWebView;Landroid/app/ProgressDialog;)Landroid/app/ProgressDialog;

    .line 239
    iget-object v1, p0, Lcom/osp/app/signin/mn;->a:Lcom/osp/app/signin/mm;

    iget-object v1, v1, Lcom/osp/app/signin/mm;->a:Lcom/osp/app/signin/SignUpWithFacebookWebView;

    invoke-static {v1}, Lcom/osp/app/signin/SignUpWithFacebookWebView;->c(Lcom/osp/app/signin/SignUpWithFacebookWebView;)Landroid/webkit/WebView;

    move-result-object v1

    invoke-virtual {v1}, Landroid/webkit/WebView;->stopLoading()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 246
    :cond_0
    :goto_0
    iget-object v1, p0, Lcom/osp/app/signin/mn;->a:Lcom/osp/app/signin/mm;

    iget-object v1, v1, Lcom/osp/app/signin/mm;->a:Lcom/osp/app/signin/SignUpWithFacebookWebView;

    invoke-static {v1}, Lcom/osp/app/signin/SignUpWithFacebookWebView;->c(Lcom/osp/app/signin/SignUpWithFacebookWebView;)Landroid/webkit/WebView;

    move-result-object v1

    invoke-virtual {v1}, Landroid/webkit/WebView;->canGoBack()Z

    move-result v1

    if-eqz v1, :cond_2

    .line 248
    iget-object v0, p0, Lcom/osp/app/signin/mn;->a:Lcom/osp/app/signin/mm;

    iget-object v0, v0, Lcom/osp/app/signin/mm;->a:Lcom/osp/app/signin/SignUpWithFacebookWebView;

    invoke-static {v0}, Lcom/osp/app/signin/SignUpWithFacebookWebView;->c(Lcom/osp/app/signin/SignUpWithFacebookWebView;)Landroid/webkit/WebView;

    move-result-object v0

    invoke-virtual {v0}, Landroid/webkit/WebView;->goBack()V

    .line 254
    :goto_1
    const/4 v0, 0x1

    .line 257
    :cond_1
    return v0

    .line 241
    :catch_0
    move-exception v1

    .line 243
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "ProgressDialog error message : "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/osp/app/util/aq;->b(Ljava/lang/String;)V

    goto :goto_0

    .line 251
    :cond_2
    iget-object v1, p0, Lcom/osp/app/signin/mn;->a:Lcom/osp/app/signin/mm;

    iget-object v1, v1, Lcom/osp/app/signin/mm;->a:Lcom/osp/app/signin/SignUpWithFacebookWebView;

    invoke-virtual {v1, v0}, Lcom/osp/app/signin/SignUpWithFacebookWebView;->setResult(I)V

    .line 252
    iget-object v0, p0, Lcom/osp/app/signin/mn;->a:Lcom/osp/app/signin/mm;

    iget-object v0, v0, Lcom/osp/app/signin/mm;->a:Lcom/osp/app/signin/SignUpWithFacebookWebView;

    invoke-virtual {v0}, Lcom/osp/app/signin/SignUpWithFacebookWebView;->finish()V

    goto :goto_1
.end method
