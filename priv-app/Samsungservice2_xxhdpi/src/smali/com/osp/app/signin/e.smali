.class final Lcom/osp/app/signin/e;
.super Ljava/lang/Object;
.source "AccountHelpPreference.java"

# interfaces
.implements Landroid/preference/Preference$OnPreferenceClickListener;


# instance fields
.field final synthetic a:Lcom/osp/app/signin/AccountHelpPreference;


# direct methods
.method constructor <init>(Lcom/osp/app/signin/AccountHelpPreference;)V
    .locals 0

    .prologue
    .line 164
    iput-object p1, p0, Lcom/osp/app/signin/e;->a:Lcom/osp/app/signin/AccountHelpPreference;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onPreferenceClick(Landroid/preference/Preference;)Z
    .locals 4

    .prologue
    const/4 v3, 0x1

    .line 168
    invoke-static {}, Lcom/osp/device/b;->b()Lcom/osp/device/b;

    move-result-object v0

    iget-object v1, p0, Lcom/osp/app/signin/e;->a:Lcom/osp/app/signin/AccountHelpPreference;

    invoke-virtual {v0, v1}, Lcom/osp/device/b;->i(Landroid/content/Context;)Z

    move-result v0

    .line 170
    new-instance v1, Landroid/content/Intent;

    invoke-direct {v1}, Landroid/content/Intent;-><init>()V

    .line 171
    const-string v2, "android.intent.action.VIEW"

    invoke-virtual {v1, v2}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 175
    :try_start_0
    iget-object v2, p0, Lcom/osp/app/signin/e;->a:Lcom/osp/app/signin/AccountHelpPreference;

    if-eqz v0, :cond_1

    const-string v0, "/main/main.do"

    :goto_0
    invoke-static {v2, v0}, Lcom/osp/app/signin/AccountHelpPreference;->b(Lcom/osp/app/signin/AccountHelpPreference;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 176
    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/content/Intent;->setData(Landroid/net/Uri;)Landroid/content/Intent;

    .line 177
    iget-object v2, p0, Lcom/osp/app/signin/e;->a:Lcom/osp/app/signin/AccountHelpPreference;

    invoke-static {v1, v2}, Lcom/osp/app/util/ad;->a(Landroid/content/Intent;Landroid/content/Context;)Z

    move-result v2

    if-ne v2, v3, :cond_2

    .line 179
    iget-object v0, p0, Lcom/osp/app/signin/e;->a:Lcom/osp/app/signin/AccountHelpPreference;

    invoke-virtual {v0, v1}, Lcom/osp/app/signin/AccountHelpPreference;->startActivity(Landroid/content/Intent;)V

    .line 193
    :cond_0
    :goto_1
    return v3

    .line 175
    :cond_1
    const-string v0, "/link/link.do"

    goto :goto_0

    .line 182
    :cond_2
    if-eqz v0, :cond_0

    .line 184
    iget-object v1, p0, Lcom/osp/app/signin/e;->a:Lcom/osp/app/signin/AccountHelpPreference;

    invoke-static {v1, v0}, Lcom/osp/app/signin/AccountHelpPreference;->a(Lcom/osp/app/signin/AccountHelpPreference;Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_1

    .line 187
    :catch_0
    move-exception v0

    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_1
.end method
