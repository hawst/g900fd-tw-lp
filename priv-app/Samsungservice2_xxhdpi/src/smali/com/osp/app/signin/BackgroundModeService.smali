.class public Lcom/osp/app/signin/BackgroundModeService;
.super Lcom/osp/app/util/AbstractBaseService;
.source "BackgroundModeService.java"


# static fields
.field private static a:Z


# instance fields
.field private b:Lcom/osp/app/signin/cu;

.field private c:Landroid/os/Looper;

.field private d:Lcom/osp/app/signin/fv;

.field private e:Ljava/util/HashMap;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 68
    invoke-direct {p0}, Lcom/osp/app/util/AbstractBaseService;-><init>()V

    .line 3952
    return-void
.end method

.method private static a(ZZZ)I
    .locals 3

    .prologue
    .line 1046
    const/4 v0, 0x0

    .line 1047
    if-nez p1, :cond_0

    .line 1049
    const/4 v0, 0x4

    .line 1050
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v1, "BMS"

    const-string v2, "requirement : Name verification"

    invoke-static {v1, v2}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 1053
    :cond_0
    if-nez p0, :cond_1

    .line 1055
    or-int/lit8 v0, v0, 0x8

    .line 1056
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v1, "BMS"

    const-string v2, "requirement : Email validation"

    invoke-static {v1, v2}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 1059
    :cond_1
    if-nez p2, :cond_2

    .line 1061
    or-int/lit8 v0, v0, 0x2

    .line 1062
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v1, "BMS"

    const-string v2, "requirement : TNC"

    invoke-static {v1, v2}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 1070
    :cond_2
    return v0
.end method

.method static synthetic a(Lcom/osp/app/signin/BackgroundModeService;)V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 68
    invoke-static {p0}, Lcom/osp/app/util/ad;->a(Landroid/content/Context;)Z

    move-result v0

    const/4 v1, 0x1

    if-ne v0, v1, :cond_0

    invoke-static {}, Lcom/osp/app/signin/SamsungService;->a()Z

    move-result v0

    if-nez v0, :cond_0

    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v0, "Self Update check"

    invoke-static {v0}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;)V

    invoke-static {}, Lcom/msc/sa/selfupdate/c;->b()Lcom/msc/sa/selfupdate/c;

    move-result-object v0

    invoke-virtual {p0}, Lcom/osp/app/signin/BackgroundModeService;->getApplication()Landroid/app/Application;

    move-result-object v1

    const-string v2, "samsungaccount.FORCE_BACKGROUND_UPDATE"

    invoke-virtual {v0, v1, v3, v3, v2}, Lcom/msc/sa/selfupdate/c;->a(Landroid/app/Application;ZZLjava/lang/String;)V

    :cond_0
    return-void
.end method

.method static synthetic a(Lcom/osp/app/signin/BackgroundModeService;Landroid/os/Message;)V
    .locals 14

    .prologue
    .line 68
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v0, "BMS"

    const-string v1, "startBackgroundTask"

    invoke-static {v0, v1}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    move-object v8, v0

    check-cast v8, Lcom/osp/app/util/d;

    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v0, "BMS"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "startBackgroundTask startId : "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v2, v8, Lcom/osp/app/util/d;->s:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v0, v8, Lcom/osp/app/util/d;->l:Ljava/lang/String;

    const-string v1, "OSP_01"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-static {p0}, Lcom/msc/c/c;->c(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v0

    if-nez v0, :cond_0

    new-instance v0, Lcom/osp/app/signin/cr;

    invoke-direct {v0, p0, v8}, Lcom/osp/app/signin/cr;-><init>(Lcom/osp/app/signin/BackgroundModeService;Lcom/osp/app/util/d;)V

    invoke-virtual {v0}, Lcom/osp/app/signin/cr;->b()V

    :goto_0
    return-void

    :cond_0
    new-instance v0, Lcom/osp/app/signin/cs;

    invoke-direct {v0, p0, v8}, Lcom/osp/app/signin/cs;-><init>(Lcom/osp/app/signin/BackgroundModeService;Lcom/osp/app/util/d;)V

    invoke-virtual {v0}, Lcom/osp/app/signin/cs;->b()V

    goto :goto_0

    :cond_1
    invoke-static {}, Lcom/osp/device/b;->b()Lcom/osp/device/b;

    move-result-object v0

    invoke-virtual {v0}, Lcom/osp/device/b;->l()I

    move-result v0

    const/16 v1, 0x10

    if-le v0, v1, :cond_3

    iget-object v0, v8, Lcom/osp/app/util/d;->f:Ljava/lang/String;

    iget-object v1, v8, Lcom/osp/app/util/d;->n:Ljava/lang/String;

    invoke-direct {p0, v0, v1}, Lcom/osp/app/signin/BackgroundModeService;->a(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_3

    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v0, "BMS"

    const-string v1, "fail signature check"

    invoke-static {v0, v1}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v0, v8, Lcom/osp/app/util/d;->n:Ljava/lang/String;

    if-eqz v0, :cond_2

    new-instance v0, Landroid/content/Intent;

    const-string v1, "com.msc.action.samsungaccount.response.BackGroundSignin"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    const-string v1, "bg_result"

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    const-string v1, "request_code"

    iget v2, v8, Lcom/osp/app/util/d;->q:I

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v2, v8, Lcom/osp/app/util/d;->n:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ":"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setData(Landroid/net/Uri;)Landroid/content/Intent;

    iget-object v1, v8, Lcom/osp/app/util/d;->n:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setPackage(Ljava/lang/String;)Landroid/content/Intent;

    const-string v1, "client_id"

    iget-object v2, v8, Lcom/osp/app/util/d;->f:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    iget-wide v2, v8, Lcom/osp/app/util/d;->r:J

    invoke-virtual {p0, v0, v2, v3}, Lcom/osp/app/signin/BackgroundModeService;->a(Landroid/content/Intent;J)V

    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "sendBroadcast to "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v1, v8, Lcom/osp/app/util/d;->n:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;)V

    goto/16 :goto_0

    :cond_2
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v0, "BMS"

    const-string v1, "cancel broadcast - no package name"

    invoke-static {v0, v1}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    :cond_3
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    const-string v2, "REQUEST_ACCESSTOKEN"

    iget-object v3, v8, Lcom/osp/app/util/d;->c:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_b

    iget-object v2, v8, Lcom/osp/app/util/d;->f:Ljava/lang/String;

    invoke-static {p0, v2}, Lcom/osp/app/util/ad;->c(Landroid/content/Context;Ljava/lang/String;)Landroid/os/Bundle;

    move-result-object v2

    if-eqz v2, :cond_a

    const-string v3, "key_license_check_time"

    invoke-virtual {v2, v3}, Landroid/os/Bundle;->getLong(Ljava/lang/String;)J

    move-result-wide v4

    sub-long/2addr v0, v4

    const-string v3, "key_accesstoken"

    invoke-virtual {v2, v3}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v9

    if-eqz v9, :cond_8

    const-wide/16 v2, 0x0

    cmp-long v2, v0, v2

    if-lez v2, :cond_4

    const-wide/32 v2, 0x927c0

    cmp-long v0, v0, v2

    if-ltz v0, :cond_5

    :cond_4
    iget-object v1, v8, Lcom/osp/app/util/d;->f:Ljava/lang/String;

    iget-object v0, v8, Lcom/osp/app/util/d;->g:Ljava/lang/String;

    new-instance v10, Landroid/os/Bundle;

    invoke-direct {v10}, Landroid/os/Bundle;-><init>()V

    new-instance v11, Lcom/msc/c/f;

    invoke-direct {v11}, Lcom/msc/c/f;-><init>()V

    invoke-static {}, Lcom/msc/c/g;->a()Lcom/msc/c/g;

    new-instance v2, Lcom/osp/app/signin/cp;

    invoke-direct {v2, p0, v10, v11}, Lcom/osp/app/signin/cp;-><init>(Lcom/osp/app/signin/BackgroundModeService;Landroid/os/Bundle;Lcom/msc/c/f;)V

    invoke-static {p0, v1, v0, v9, v2}, Lcom/msc/c/g;->d(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/msc/b/h;)J

    move-result-wide v12

    const/4 v2, 0x0

    const-wide/16 v3, 0x0

    const/4 v5, 0x0

    const-wide/16 v6, 0x0

    move-object v0, p0

    invoke-static/range {v0 .. v7}, Lcom/osp/app/util/ad;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;JLjava/lang/String;J)V

    const/4 v0, 0x1

    sput-boolean v0, Lcom/osp/app/signin/BackgroundModeService;->a:Z

    sget-object v0, Lcom/msc/b/g;->b:Lcom/msc/b/g;

    invoke-static {v12, v13, v0}, Lcom/msc/c/g;->a(JLcom/msc/b/g;)V

    const-string v0, "result"

    invoke-virtual {v10, v0}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v10

    if-eqz v10, :cond_6

    const-wide/16 v3, 0x0

    const/4 v5, 0x0

    const-wide/16 v6, 0x0

    move-object v0, p0

    move-object v2, v9

    invoke-static/range {v0 .. v7}, Lcom/osp/app/util/ad;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;JLjava/lang/String;J)V

    const/4 v0, 0x0

    sput-boolean v0, Lcom/osp/app/signin/BackgroundModeService;->a:Z

    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v0, "BMS"

    const-string v1, "check license - ok"

    invoke-static {v0, v1}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;)V

    :goto_1
    if-eqz v10, :cond_a

    :cond_5
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v0, "BMS"

    const-string v1, "Reuse access token"

    invoke-static {v0, v1}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v0, v8, Lcom/osp/app/util/d;->n:Ljava/lang/String;

    if-eqz v0, :cond_7

    new-instance v0, Landroid/content/Intent;

    const-string v1, "com.msc.action.samsungaccount.response.BackGroundSignin"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    const-string v1, "bg_result"

    const/4 v2, -0x1

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    const-string v1, "authcode"

    invoke-virtual {v0, v1, v9}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    const-string v1, "ServerUrl"

    invoke-static {p0}, Lcom/msc/c/c;->f(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    const-string v1, "request_code"

    iget v2, v8, Lcom/osp/app/util/d;->q:I

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v2, v8, Lcom/osp/app/util/d;->n:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ":"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setData(Landroid/net/Uri;)Landroid/content/Intent;

    iget-object v1, v8, Lcom/osp/app/util/d;->n:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setPackage(Ljava/lang/String;)Landroid/content/Intent;

    const-string v1, "client_id"

    iget-object v2, v8, Lcom/osp/app/util/d;->f:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    iget-wide v2, v8, Lcom/osp/app/util/d;->r:J

    invoke-virtual {p0, v0, v2, v3}, Lcom/osp/app/signin/BackgroundModeService;->a(Landroid/content/Intent;J)V

    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "sendBroadcast to "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v1, v8, Lcom/osp/app/util/d;->n:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;)V

    goto/16 :goto_0

    :cond_6
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v0, "BMS"

    const-string v1, "check license - fail"

    invoke-static {v0, v1}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;)V

    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v0, "BMS"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "error message : "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v11}, Lcom/msc/c/f;->b()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_1

    :cond_7
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v0, "BMS"

    const-string v1, "cancel broadcast - no package name"

    invoke-static {v0, v1}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    :cond_8
    const-wide/16 v2, 0x0

    cmp-long v2, v0, v2

    if-lez v2, :cond_a

    const-wide/32 v2, 0x927c0

    cmp-long v0, v0, v2

    if-gez v0, :cond_a

    sget-boolean v0, Lcom/osp/app/signin/BackgroundModeService;->a:Z

    if-eqz v0, :cond_a

    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v0, "BMS"

    const-string v1, "fail access token, wait"

    invoke-static {v0, v1}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v0, v8, Lcom/osp/app/util/d;->n:Ljava/lang/String;

    if-eqz v0, :cond_9

    new-instance v0, Landroid/content/Intent;

    const-string v1, "com.msc.action.samsungaccount.response.BackGroundSignin"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    const-string v1, "bg_result"

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    const-string v1, "request_code"

    iget v2, v8, Lcom/osp/app/util/d;->q:I

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v2, v8, Lcom/osp/app/util/d;->n:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ":"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setData(Landroid/net/Uri;)Landroid/content/Intent;

    iget-object v1, v8, Lcom/osp/app/util/d;->n:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setPackage(Ljava/lang/String;)Landroid/content/Intent;

    const-string v1, "client_id"

    iget-object v2, v8, Lcom/osp/app/util/d;->f:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    iget-wide v2, v8, Lcom/osp/app/util/d;->r:J

    invoke-virtual {p0, v0, v2, v3}, Lcom/osp/app/signin/BackgroundModeService;->a(Landroid/content/Intent;J)V

    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "sendBroadcast to "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v1, v8, Lcom/osp/app/util/d;->n:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;)V

    goto/16 :goto_0

    :cond_9
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v0, "BMS"

    const-string v1, "cancel broadcast - no package name"

    invoke-static {v0, v1}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    :cond_a
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v0, "BMS"

    const-string v1, "get new access token from server"

    invoke-static {v0, v1}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v1, v8, Lcom/osp/app/util/d;->f:Ljava/lang/String;

    const/4 v2, 0x0

    const-wide/16 v3, 0x0

    const/4 v5, 0x0

    const-wide/16 v6, 0x0

    move-object v0, p0

    invoke-static/range {v0 .. v7}, Lcom/osp/app/util/ad;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;JLjava/lang/String;J)V

    :cond_b
    const/4 v0, 0x1

    sput-boolean v0, Lcom/osp/app/signin/BackgroundModeService;->a:Z

    invoke-static {p0}, Lcom/msc/c/e;->g(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v0

    if-nez v0, :cond_c

    invoke-direct {p0, v8}, Lcom/osp/app/signin/BackgroundModeService;->a(Lcom/osp/app/util/d;)V

    goto/16 :goto_0

    :cond_c
    new-instance v0, Lcom/osp/app/signin/cq;

    invoke-direct {v0, p0, v8}, Lcom/osp/app/signin/cq;-><init>(Lcom/osp/app/signin/BackgroundModeService;Lcom/osp/app/util/d;)V

    iget-object v1, p0, Lcom/osp/app/signin/BackgroundModeService;->e:Ljava/util/HashMap;

    iget v2, v8, Lcom/osp/app/util/d;->s:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v1, v2, v0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    invoke-virtual {v0}, Lcom/osp/app/signin/cq;->b()V

    goto/16 :goto_0
.end method

.method static synthetic a(Lcom/osp/app/signin/BackgroundModeService;Lcom/osp/app/signin/fv;)V
    .locals 7

    .prologue
    const/4 v3, 0x0

    const v6, 0x132df83

    .line 68
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v0, "BMS"

    const-string v1, "showMarketingNotification start"

    invoke-static {v0, v1}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {p1}, Lcom/osp/app/signin/fv;->i()I

    new-instance v0, Landroid/content/Intent;

    const-string v1, "com.osp.app.pushmarketing.PushMarketingService"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    const-string v1, "push_marketing_service_mode"

    const/4 v2, 0x4

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    const-string v1, "pref_notification_id"

    invoke-virtual {v0, v1, v6}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    const-string v1, "target_link"

    invoke-virtual {p1}, Lcom/osp/app/signin/fv;->h()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    const-string v1, "link_type"

    const-string v2, "weblink"

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    const/high16 v1, 0x8000000

    invoke-static {p0, v6, v0, v1}, Landroid/app/PendingIntent;->getService(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v1

    new-instance v0, Landroid/content/Intent;

    const-class v2, Lcom/osp/app/signin/OspReceiver;

    invoke-direct {v0, p0, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const-string v2, "samsungaccount_clear_notification"

    invoke-virtual {v0, v2}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    invoke-static {p0, v3, v0, v3}, Landroid/app/PendingIntent;->getBroadcast(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v2

    invoke-virtual {p1}, Lcom/osp/app/signin/fv;->f()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p1}, Lcom/osp/app/signin/fv;->g()Ljava/lang/String;

    move-result-object v4

    invoke-static {}, Lcom/osp/app/signin/SamsungService;->i()I

    move-result v5

    move-object v0, p0

    invoke-static/range {v0 .. v6}, Lcom/msc/sa/c/d;->a(Landroid/content/Context;Landroid/app/PendingIntent;Landroid/app/PendingIntent;Ljava/lang/String;Ljava/lang/String;II)V

    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v0, "BMS"

    const-string v1, "showMarketingNotification end"

    invoke-static {v0, v1}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method

.method static synthetic a(Lcom/osp/app/signin/BackgroundModeService;Lcom/osp/app/util/d;)V
    .locals 0

    .prologue
    .line 68
    invoke-direct {p0, p1}, Lcom/osp/app/signin/BackgroundModeService;->a(Lcom/osp/app/util/d;)V

    return-void
.end method

.method static synthetic a(Lcom/osp/app/signin/BackgroundModeService;Ljava/lang/String;Lcom/osp/app/util/d;)V
    .locals 4

    .prologue
    .line 68
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/osp/app/signin/EmailValidationView;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const/high16 v1, 0x10000000

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    const-string v1, "email_id"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    const-string v1, "is_resend"

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    const-string v1, "is_bgmode"

    const/16 v2, 0xc9

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    const-string v1, "key_request_id"

    iget-wide v2, p2, Lcom/osp/app/util/d;->r:J

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;J)Landroid/content/Intent;

    iget-object v1, p2, Lcom/osp/app/util/d;->f:Ljava/lang/String;

    if-eqz v1, :cond_0

    const-string v1, "client_id"

    iget-object v2, p2, Lcom/osp/app/util/d;->f:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    :cond_0
    iget-object v1, p2, Lcom/osp/app/util/d;->g:Ljava/lang/String;

    if-eqz v1, :cond_1

    const-string v1, "client_secret"

    iget-object v2, p2, Lcom/osp/app/util/d;->g:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    :cond_1
    iget-object v1, p2, Lcom/osp/app/util/d;->c:Ljava/lang/String;

    if-eqz v1, :cond_2

    const-string v1, "account_mode"

    iget-object v2, p2, Lcom/osp/app/util/d;->c:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    :cond_2
    iget-object v1, p2, Lcom/osp/app/util/d;->l:Ljava/lang/String;

    if-eqz v1, :cond_3

    const-string v1, "OSP_VER"

    iget-object v2, p2, Lcom/osp/app/util/d;->l:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    :cond_3
    iget-object v1, p2, Lcom/osp/app/util/d;->m:Ljava/lang/String;

    if-eqz v1, :cond_4

    const-string v1, "service_name"

    iget-object v2, p2, Lcom/osp/app/util/d;->m:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    :cond_4
    iget-object v1, p2, Lcom/osp/app/util/d;->o:Ljava/lang/String;

    if-eqz v1, :cond_5

    const-string v1, "BG_WhoareU"

    iget-object v2, p2, Lcom/osp/app/util/d;->o:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    :cond_5
    iget-object v1, p2, Lcom/osp/app/util/d;->n:Ljava/lang/String;

    if-eqz v1, :cond_6

    const-string v1, "mypackage"

    iget-object v2, p2, Lcom/osp/app/util/d;->n:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    :cond_6
    invoke-virtual {p0, v0}, Lcom/osp/app/signin/BackgroundModeService;->startActivity(Landroid/content/Intent;)V

    return-void
.end method

.method static synthetic a(Lcom/osp/app/signin/BackgroundModeService;ZLcom/osp/app/util/d;)V
    .locals 4

    .prologue
    .line 68
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    const-class v1, Lcom/osp/app/signin/UserValidateCheck;

    invoke-virtual {v0, p0, v1}, Landroid/content/Intent;->setClass(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;

    const/high16 v1, 0x10000000

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    const-string v1, "client_id"

    iget-object v2, p2, Lcom/osp/app/util/d;->f:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    const-string v1, "client_secret"

    iget-object v2, p2, Lcom/osp/app/util/d;->g:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    const-string v1, "account_mode"

    iget-object v2, p2, Lcom/osp/app/util/d;->c:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    const-string v1, "service_name"

    iget-object v2, p2, Lcom/osp/app/util/d;->m:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    const-string v1, "OSP_VER"

    iget-object v2, p2, Lcom/osp/app/util/d;->l:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    const-string v1, "MODE"

    const-string v2, "BG_mode"

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    const-string v1, "mypackage"

    iget-object v2, p2, Lcom/osp/app/util/d;->n:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    const-string v1, "key_request_id"

    iget-wide v2, p2, Lcom/osp/app/util/d;->r:J

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;J)Landroid/content/Intent;

    if-eqz p1, :cond_0

    const-string v1, "NEED_AUTHCODE"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    :cond_0
    invoke-virtual {p0, v0}, Lcom/osp/app/signin/BackgroundModeService;->startActivity(Landroid/content/Intent;)V

    return-void
.end method

.method private a(Lcom/osp/app/util/d;)V
    .locals 7

    .prologue
    .line 1967
    invoke-static {p0}, Lcom/osp/app/util/ad;->d(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1969
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v0, "showResigninProcess()"

    invoke-static {v0}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;)V

    .line 1971
    new-instance v0, Landroid/content/Intent;

    const-string v1, "com.msc.action.samsungaccount.resignin"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 1972
    const/high16 v1, 0x10000000

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    .line 1973
    const-string v1, "client_id"

    iget-object v2, p1, Lcom/osp/app/util/d;->f:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 1974
    const-string v1, "client_secret"

    iget-object v2, p1, Lcom/osp/app/util/d;->g:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 1975
    const-string v1, "account_mode"

    iget-object v2, p1, Lcom/osp/app/util/d;->c:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 1976
    const-string v1, "OSP_VER"

    iget-object v2, p1, Lcom/osp/app/util/d;->l:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 1979
    const-string v1, "BG_WhoareU"

    iget-object v2, p1, Lcom/osp/app/util/d;->o:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 1990
    const-string v1, "email_id"

    invoke-static {p0}, Lcom/osp/app/util/ad;->h(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 1992
    const-string v1, "REQUEST_ACCESSTOKEN"

    iget-object v2, p1, Lcom/osp/app/util/d;->c:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 1997
    const-string v1, "from_notification"

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 1999
    const/4 v1, 0x0

    const/high16 v2, 0x8000000

    invoke-static {p0, v1, v0, v2}, Landroid/app/PendingIntent;->getActivity(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v1

    .line 2001
    const/4 v2, 0x0

    const v0, 0x7f09004f

    invoke-virtual {p0, v0}, Lcom/osp/app/signin/BackgroundModeService;->getString(I)Ljava/lang/String;

    move-result-object v3

    const v0, 0x7f09008d

    invoke-virtual {p0, v0}, Lcom/osp/app/signin/BackgroundModeService;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-static {}, Lcom/osp/app/signin/SamsungService;->i()I

    move-result v5

    const v6, 0x132df82

    move-object v0, p0

    invoke-static/range {v0 .. v6}, Lcom/msc/sa/c/d;->a(Landroid/content/Context;Landroid/app/PendingIntent;Landroid/app/PendingIntent;Ljava/lang/String;Ljava/lang/String;II)V

    .line 2004
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v0, "BMS"

    const-string v1, "Show resign-in Notification"

    invoke-static {v0, v1}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 2013
    :cond_0
    :goto_0
    return-void

    .line 2008
    :cond_1
    invoke-virtual {p0, v0}, Lcom/osp/app/signin/BackgroundModeService;->startActivity(Landroid/content/Intent;)V

    .line 2010
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v0, "BMS"

    const-string v1, "Show session expired Activity"

    invoke-static {v0, v1}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method static synthetic a()Z
    .locals 1

    .prologue
    .line 68
    const/4 v0, 0x0

    sput-boolean v0, Lcom/osp/app/signin/BackgroundModeService;->a:Z

    return v0
.end method

.method static synthetic a(Ljava/lang/String;)Z
    .locals 1

    .prologue
    .line 68
    const-string v0, "tj9u972o46"

    invoke-virtual {v0, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method private a(Ljava/lang/String;Ljava/lang/String;)Z
    .locals 4

    .prologue
    .line 320
    new-instance v1, Landroid/os/Bundle;

    invoke-direct {v1}, Landroid/os/Bundle;-><init>()V

    .line 321
    invoke-static {p0}, Lcom/osp/app/signin/SamsungService;->a(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v0

    .line 327
    if-eqz v0, :cond_1

    .line 332
    :try_start_0
    invoke-static {}, Lcom/msc/c/h;->a()Lcom/msc/c/h;

    invoke-static {v0}, Lcom/msc/c/h;->A(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v0

    .line 334
    invoke-static {}, Lcom/msc/c/h;->a()Lcom/msc/c/h;

    invoke-static {p0, v0, p1, p2}, Lcom/msc/c/h;->a(Landroid/content/Context;Ljava/util/ArrayList;Ljava/lang/String;Ljava/lang/String;)Z

    move-result v0

    .line 335
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v2, "BMS"

    const-string v3, "used cache of signature"

    invoke-static {v2, v3}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 337
    if-eqz v0, :cond_0

    .line 339
    const-string v0, "result"

    const/4 v2, 0x1

    invoke-virtual {v1, v0, v2}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 340
    const-string v0, "result"

    invoke-virtual {v1, v0}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v0

    .line 412
    :goto_0
    return v0

    .line 342
    :cond_0
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v0, "BMS"

    const-string v2, "Not matched cache data of signature"

    invoke-static {v0, v2}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 351
    :cond_1
    :goto_1
    invoke-static {}, Lcom/msc/c/g;->a()Lcom/msc/c/g;

    .line 352
    new-instance v0, Lcom/osp/app/signin/co;

    invoke-direct {v0, p0, v1, p1, p2}, Lcom/osp/app/signin/co;-><init>(Lcom/osp/app/signin/BackgroundModeService;Landroid/os/Bundle;Ljava/lang/String;Ljava/lang/String;)V

    invoke-static {p0, v0}, Lcom/msc/c/g;->c(Landroid/content/Context;Lcom/msc/b/h;)J

    move-result-wide v2

    .line 411
    sget-object v0, Lcom/msc/b/g;->b:Lcom/msc/b/g;

    invoke-static {v2, v3, v0}, Lcom/msc/c/g;->a(JLcom/msc/b/g;)V

    .line 412
    const-string v0, "result"

    invoke-virtual {v1, v0}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v0

    goto :goto_0

    .line 343
    :catch_0
    move-exception v0

    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_1
.end method

.method static synthetic b(Lcom/osp/app/signin/BackgroundModeService;Lcom/osp/app/signin/fv;)Lcom/osp/app/signin/fv;
    .locals 0

    .prologue
    .line 68
    iput-object p1, p0, Lcom/osp/app/signin/BackgroundModeService;->d:Lcom/osp/app/signin/fv;

    return-object p1
.end method

.method static synthetic b(Lcom/osp/app/signin/BackgroundModeService;)Ljava/util/HashMap;
    .locals 1

    .prologue
    .line 68
    iget-object v0, p0, Lcom/osp/app/signin/BackgroundModeService;->e:Ljava/util/HashMap;

    return-object v0
.end method

.method static synthetic b(Lcom/osp/app/signin/BackgroundModeService;Lcom/osp/app/util/d;)V
    .locals 7

    .prologue
    .line 68
    invoke-static {p0}, Lcom/osp/app/util/ad;->d(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v0, "showResigninProcess()"

    invoke-static {v0}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;)V

    new-instance v0, Landroid/content/Intent;

    const-string v1, "com.msc.action.samsungaccount.resignin_with_signout"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    const/high16 v1, 0x10000000

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    const-string v1, "client_id"

    iget-object v2, p1, Lcom/osp/app/util/d;->f:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    const-string v1, "client_secret"

    iget-object v2, p1, Lcom/osp/app/util/d;->g:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    const-string v1, "account_mode"

    iget-object v2, p1, Lcom/osp/app/util/d;->c:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    const-string v1, "OSP_VER"

    iget-object v2, p1, Lcom/osp/app/util/d;->l:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    const-string v1, "BG_WhoareU"

    iget-object v2, p1, Lcom/osp/app/util/d;->o:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    const-string v1, "REQUEST_ACCESSTOKEN"

    iget-object v2, p1, Lcom/osp/app/util/d;->c:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    const-string v1, "from_notification"

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    const/4 v1, 0x0

    const/high16 v2, 0x8000000

    invoke-static {p0, v1, v0, v2}, Landroid/app/PendingIntent;->getActivity(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v1

    const/4 v2, 0x0

    const v0, 0x7f09004f

    invoke-virtual {p0, v0}, Lcom/osp/app/signin/BackgroundModeService;->getString(I)Ljava/lang/String;

    move-result-object v3

    const v0, 0x7f09008d

    invoke-virtual {p0, v0}, Lcom/osp/app/signin/BackgroundModeService;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-static {}, Lcom/osp/app/signin/SamsungService;->i()I

    move-result v5

    const v6, 0x132df82

    move-object v0, p0

    invoke-static/range {v0 .. v6}, Lcom/msc/sa/c/d;->a(Landroid/content/Context;Landroid/app/PendingIntent;Landroid/app/PendingIntent;Ljava/lang/String;Ljava/lang/String;II)V

    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v0, "BMS"

    const-string v1, "Show resign-in with sign out Notification"

    invoke-static {v0, v1}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;)V

    :cond_0
    :goto_0
    return-void

    :cond_1
    invoke-virtual {p0, v0}, Lcom/osp/app/signin/BackgroundModeService;->startActivity(Landroid/content/Intent;)V

    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v0, "BMS"

    const-string v1, "Show resign in with sign out Activity"

    invoke-static {v0, v1}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method static synthetic c(Lcom/osp/app/signin/BackgroundModeService;)V
    .locals 8

    .prologue
    const/4 v2, 0x1

    const/4 v5, 0x0

    .line 68
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v0, "BMS"

    const-string v1, "showEmailValidationNotification start"

    invoke-static {v0, v1}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;)V

    invoke-static {v5, v2, v2}, Lcom/osp/app/signin/BackgroundModeService;->a(ZZZ)I

    move-result v1

    invoke-static {p0}, Lcom/msc/c/e;->h(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v2

    const-string v3, "j5p7ll8g33"

    const-string v4, "5763D0052DC1462E13751F753384E9A9"

    const/4 v7, 0x0

    move-object v0, p0

    move v6, v5

    invoke-static/range {v0 .. v7}, Lcom/osp/app/util/ad;->a(Landroid/content/Context;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;ZZLjava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    const/high16 v1, 0x8000000

    invoke-static {p0, v5, v0, v1}, Landroid/app/PendingIntent;->getActivity(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v1

    new-instance v0, Landroid/content/Intent;

    const-class v2, Lcom/osp/app/signin/OspReceiver;

    invoke-direct {v0, p0, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const-string v2, "samsungaccount_clear_notification"

    invoke-virtual {v0, v2}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    invoke-static {p0, v5, v0, v5}, Landroid/app/PendingIntent;->getBroadcast(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v2

    const v0, 0x7f090137

    invoke-virtual {p0, v0}, Lcom/osp/app/signin/BackgroundModeService;->getString(I)Ljava/lang/String;

    move-result-object v3

    const v0, 0x7f090149

    invoke-virtual {p0, v0}, Lcom/osp/app/signin/BackgroundModeService;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-static {}, Lcom/osp/app/signin/SamsungService;->i()I

    move-result v5

    const v6, 0x132df82

    move-object v0, p0

    invoke-static/range {v0 .. v6}, Lcom/msc/sa/c/d;->a(Landroid/content/Context;Landroid/app/PendingIntent;Landroid/app/PendingIntent;Ljava/lang/String;Ljava/lang/String;II)V

    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v0, "BMS"

    const-string v1, "showEmailValidationNotification end"

    invoke-static {v0, v1}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method

.method static synthetic d(Lcom/osp/app/signin/BackgroundModeService;)V
    .locals 8

    .prologue
    const/4 v2, 0x1

    const/4 v5, 0x0

    .line 68
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v0, "BMS"

    const-string v1, "showRequireTncNotification start"

    invoke-static {v0, v1}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;)V

    invoke-static {v2, v2, v5}, Lcom/osp/app/signin/BackgroundModeService;->a(ZZZ)I

    move-result v1

    invoke-static {p0}, Lcom/msc/c/e;->h(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v2

    const-string v3, "j5p7ll8g33"

    const-string v4, "5763D0052DC1462E13751F753384E9A9"

    const/4 v7, 0x0

    move-object v0, p0

    move v6, v5

    invoke-static/range {v0 .. v7}, Lcom/osp/app/util/ad;->a(Landroid/content/Context;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;ZZLjava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    const/high16 v1, 0x8000000

    invoke-static {p0, v5, v0, v1}, Landroid/app/PendingIntent;->getActivity(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v1

    new-instance v0, Landroid/content/Intent;

    const-class v2, Lcom/osp/app/signin/OspReceiver;

    invoke-direct {v0, p0, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const-string v2, "samsungaccount_clear_notification"

    invoke-virtual {v0, v2}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    invoke-static {p0, v5, v0, v5}, Landroid/app/PendingIntent;->getBroadcast(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v2

    const v0, 0x7f090137

    invoke-virtual {p0, v0}, Lcom/osp/app/signin/BackgroundModeService;->getString(I)Ljava/lang/String;

    move-result-object v3

    const v0, 0x7f090149

    invoke-virtual {p0, v0}, Lcom/osp/app/signin/BackgroundModeService;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-static {}, Lcom/osp/app/signin/SamsungService;->i()I

    move-result v5

    const v6, 0x132df82

    move-object v0, p0

    invoke-static/range {v0 .. v6}, Lcom/msc/sa/c/d;->a(Landroid/content/Context;Landroid/app/PendingIntent;Landroid/app/PendingIntent;Ljava/lang/String;Ljava/lang/String;II)V

    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v0, "BMS"

    const-string v1, "showRequireTncNotification end"

    invoke-static {v0, v1}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method

.method static synthetic e(Lcom/osp/app/signin/BackgroundModeService;)Lcom/osp/app/signin/fv;
    .locals 1

    .prologue
    .line 68
    iget-object v0, p0, Lcom/osp/app/signin/BackgroundModeService;->d:Lcom/osp/app/signin/fv;

    return-object v0
.end method


# virtual methods
.method public onBind(Landroid/content/Intent;)Landroid/os/IBinder;
    .locals 1

    .prologue
    .line 613
    const-string v0, "onBind"

    invoke-virtual {p0, p1, v0}, Lcom/osp/app/signin/BackgroundModeService;->a(Landroid/content/Intent;Ljava/lang/String;)V

    .line 614
    const/4 v0, 0x0

    return-object v0
.end method

.method public onCreate()V
    .locals 3

    .prologue
    .line 619
    invoke-super {p0}, Lcom/osp/app/util/AbstractBaseService;->onCreate()V

    .line 621
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/osp/app/signin/BackgroundModeService;->e:Ljava/util/HashMap;

    .line 623
    new-instance v0, Landroid/os/HandlerThread;

    const-string v1, "ServiceStartArguments"

    const/16 v2, 0xa

    invoke-direct {v0, v1, v2}, Landroid/os/HandlerThread;-><init>(Ljava/lang/String;I)V

    .line 624
    invoke-virtual {v0}, Landroid/os/HandlerThread;->start()V

    .line 627
    invoke-virtual {v0}, Landroid/os/HandlerThread;->getLooper()Landroid/os/Looper;

    move-result-object v0

    iput-object v0, p0, Lcom/osp/app/signin/BackgroundModeService;->c:Landroid/os/Looper;

    .line 628
    iget-object v0, p0, Lcom/osp/app/signin/BackgroundModeService;->c:Landroid/os/Looper;

    if-eqz v0, :cond_0

    .line 630
    new-instance v0, Lcom/osp/app/signin/cu;

    iget-object v1, p0, Lcom/osp/app/signin/BackgroundModeService;->c:Landroid/os/Looper;

    invoke-direct {v0, p0, v1}, Lcom/osp/app/signin/cu;-><init>(Lcom/osp/app/signin/BackgroundModeService;Landroid/os/Looper;)V

    iput-object v0, p0, Lcom/osp/app/signin/BackgroundModeService;->b:Lcom/osp/app/signin/cu;

    .line 641
    :goto_0
    return-void

    .line 633
    :cond_0
    new-instance v0, Lcom/osp/app/signin/cu;

    invoke-direct {v0, p0}, Lcom/osp/app/signin/cu;-><init>(Lcom/osp/app/signin/BackgroundModeService;)V

    iput-object v0, p0, Lcom/osp/app/signin/BackgroundModeService;->b:Lcom/osp/app/signin/cu;

    goto :goto_0
.end method

.method public onDestroy()V
    .locals 5

    .prologue
    .line 654
    invoke-static {}, Lcom/msc/sa/selfupdate/c;->b()Lcom/msc/sa/selfupdate/c;

    move-result-object v0

    invoke-virtual {v0}, Lcom/msc/sa/selfupdate/c;->d()V

    .line 655
    invoke-virtual {p0}, Lcom/osp/app/signin/BackgroundModeService;->getApplication()Landroid/app/Application;

    move-result-object v0

    check-cast v0, Lcom/osp/app/signin/SamsungService;

    invoke-virtual {v0}, Lcom/osp/app/signin/SamsungService;->o()V

    .line 657
    iget-object v0, p0, Lcom/osp/app/signin/BackgroundModeService;->c:Landroid/os/Looper;

    invoke-virtual {v0}, Landroid/os/Looper;->quit()V

    .line 658
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/osp/app/signin/BackgroundModeService;->b:Lcom/osp/app/signin/cu;

    .line 660
    iget-object v0, p0, Lcom/osp/app/signin/BackgroundModeService;->e:Ljava/util/HashMap;

    invoke-virtual {v0}, Ljava/util/HashMap;->keySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->toArray()[Ljava/lang/Object;

    move-result-object v2

    .line 662
    array-length v3, v2

    const/4 v0, 0x0

    move v1, v0

    :goto_0
    if-ge v1, v3, :cond_1

    aget-object v0, v2, v1

    .line 664
    iget-object v4, p0, Lcom/osp/app/signin/BackgroundModeService;->e:Ljava/util/HashMap;

    invoke-virtual {v4, v0}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v4

    if-eqz v4, :cond_0

    .line 666
    iget-object v4, p0, Lcom/osp/app/signin/BackgroundModeService;->e:Ljava/util/HashMap;

    invoke-virtual {v4, v0}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/osp/app/signin/cq;

    const/4 v4, 0x1

    invoke-virtual {v0, v4}, Lcom/osp/app/signin/cq;->cancel(Z)Z

    .line 662
    :cond_0
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 672
    :cond_1
    iget-object v0, p0, Lcom/osp/app/signin/BackgroundModeService;->e:Ljava/util/HashMap;

    invoke-virtual {v0}, Ljava/util/HashMap;->clear()V

    .line 675
    invoke-super {p0}, Lcom/osp/app/util/AbstractBaseService;->onDestroy()V

    .line 676
    return-void
.end method

.method public onStartCommand(Landroid/content/Intent;II)I
    .locals 11

    .prologue
    .line 682
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v0, "BMS"

    const-string v1, "onStartCommand"

    const-string v2, "START"

    invoke-static {v0, v1, v2}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 684
    if-nez p1, :cond_0

    .line 686
    const/4 v0, 0x2

    .line 928
    :goto_0
    return v0

    .line 689
    :cond_0
    new-instance v1, Lcom/osp/app/util/d;

    invoke-direct {v1}, Lcom/osp/app/util/d;-><init>()V

    .line 690
    const-string v0, "com.sec.android.app.secsetupwizard.SETUPWIZARD_COMPLETE"

    const/4 v2, 0x0

    invoke-virtual {p1, v0, v2}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v0

    .line 691
    const-string v2, "com.msc.action.EMAIL_VALIDATION_COMPLETE_NOTIFICATION"

    const/4 v3, 0x0

    invoke-virtual {p1, v2, v3}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v2

    .line 692
    const-string v3, "com.msc.action.SETUPWIZARD_COMPLETE_SELFUPGRADE_NOTIFICATION"

    const/4 v4, 0x0

    invoke-virtual {p1, v3, v4}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v3

    .line 693
    const-string v4, "com.msc.action.MARKETING_POPUP_NOTIFICATION"

    const/4 v5, 0x0

    invoke-virtual {p1, v4, v5}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v4

    .line 694
    const-string v5, "key_easy_signup_validation_completed"

    const/4 v6, 0x0

    invoke-virtual {p1, v5, v6}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    .line 695
    const-string v5, "is_required_easy_signup_is_auth"

    const/4 v6, 0x0

    invoke-virtual {p1, v5, v6}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    .line 696
    const-string v5, "is_return_email_validation_check"

    const/4 v6, 0x0

    invoke-virtual {p1, v5, v6}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v5

    iput-boolean v5, v1, Lcom/osp/app/util/d;->k:Z

    .line 697
    const-string v5, "START_NOTIFICATION_SERVICE"

    const/4 v6, 0x0

    invoke-virtual {p1, v5, v6}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v5

    iput-boolean v5, v1, Lcom/osp/app/util/d;->j:Z

    .line 698
    const/4 v5, 0x0

    iput-boolean v5, v1, Lcom/osp/app/util/d;->b:Z

    .line 699
    iput p3, v1, Lcom/osp/app/util/d;->s:I

    .line 716
    if-eqz v4, :cond_2

    .line 718
    invoke-static {p0}, Lcom/osp/app/util/ad;->k(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 720
    invoke-static {p0}, Lcom/osp/app/util/ad;->a(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 722
    new-instance v0, Lcom/osp/app/signin/ct;

    invoke-direct {v0, p0, p0, p3}, Lcom/osp/app/signin/ct;-><init>(Lcom/osp/app/signin/BackgroundModeService;Landroid/content/Context;I)V

    .line 723
    invoke-virtual {v0}, Lcom/osp/app/signin/ct;->b()V

    .line 928
    :cond_1
    :goto_1
    const/4 v0, 0x2

    goto :goto_0

    .line 727
    :cond_2
    if-eqz v0, :cond_4

    invoke-static {}, Lcom/osp/app/util/r;->d()Z

    move-result v0

    if-nez v0, :cond_4

    .line 736
    invoke-static {p0}, Lcom/osp/app/util/ad;->k(Landroid/content/Context;)Z

    move-result v0

    invoke-static {p0}, Lcom/osp/app/util/ad;->l(Landroid/content/Context;)Z

    move-result v1

    const v2, 0x7f090137

    invoke-virtual {p0, v2}, Lcom/osp/app/signin/BackgroundModeService;->getString(I)Ljava/lang/String;

    move-result-object v8

    const v2, 0x7f090149

    invoke-virtual {p0, v2}, Lcom/osp/app/signin/BackgroundModeService;->getString(I)Ljava/lang/String;

    move-result-object v9

    const/4 v2, 0x1

    invoke-static {v0, v1, v2}, Lcom/osp/app/signin/BackgroundModeService;->a(ZZZ)I

    move-result v1

    if-lez v1, :cond_3

    const/4 v10, 0x0

    invoke-static {p0}, Lcom/msc/c/e;->h(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v2

    const-string v3, "j5p7ll8g33"

    const-string v4, "5763D0052DC1462E13751F753384E9A9"

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    move-object v0, p0

    invoke-static/range {v0 .. v7}, Lcom/osp/app/util/ad;->a(Landroid/content/Context;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;ZZLjava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    const/high16 v1, 0x8000000

    invoke-static {p0, v10, v0, v1}, Landroid/app/PendingIntent;->getActivity(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v1

    new-instance v0, Landroid/content/Intent;

    const-class v2, Lcom/osp/app/signin/OspReceiver;

    invoke-direct {v0, p0, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const-string v2, "samsungaccount_clear_notification"

    invoke-virtual {v0, v2}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    const/4 v2, 0x0

    const/4 v3, 0x0

    invoke-static {p0, v2, v0, v3}, Landroid/app/PendingIntent;->getBroadcast(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v2

    invoke-static {}, Lcom/osp/app/signin/SamsungService;->i()I

    move-result v5

    const v6, 0x132df82

    move-object v0, p0

    move-object v3, v8

    move-object v4, v9

    invoke-static/range {v0 .. v6}, Lcom/msc/sa/c/d;->a(Landroid/content/Context;Landroid/app/PendingIntent;Landroid/app/PendingIntent;Ljava/lang/String;Ljava/lang/String;II)V

    .line 739
    :cond_3
    invoke-static {p0}, Lcom/osp/app/util/ad;->m(Landroid/content/Context;)V

    .line 741
    invoke-virtual {p0, p3}, Lcom/osp/app/signin/BackgroundModeService;->stopSelf(I)V

    goto :goto_1

    .line 742
    :cond_4
    if-eqz v2, :cond_7

    invoke-static {}, Lcom/osp/app/util/r;->d()Z

    move-result v0

    if-nez v0, :cond_7

    .line 750
    invoke-virtual {p0}, Lcom/osp/app/signin/BackgroundModeService;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v0

    .line 751
    const/4 v1, 0x0

    .line 752
    const/16 v2, 0x80

    invoke-virtual {v0, v2}, Landroid/content/pm/PackageManager;->getInstalledApplications(I)Ljava/util/List;

    move-result-object v0

    .line 753
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_5
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_e

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/pm/ApplicationInfo;

    .line 755
    iget-object v0, v0, Landroid/content/pm/ApplicationInfo;->packageName:Ljava/lang/String;

    const-string v3, "com.sec.android.app.samsungapps"

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_5

    .line 757
    const/4 v0, 0x1

    .line 762
    :goto_2
    new-instance v1, Landroid/content/Intent;

    invoke-direct {v1}, Landroid/content/Intent;-><init>()V

    .line 763
    if-eqz v0, :cond_6

    .line 765
    new-instance v0, Landroid/content/ComponentName;

    const-string v2, "com.sec.android.app.samsungapps"

    const-string v3, "com.sec.android.app.samsungapps.Main"

    invoke-direct {v0, v2, v3}, Landroid/content/ComponentName;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v1, v0}, Landroid/content/Intent;->setComponent(Landroid/content/ComponentName;)Landroid/content/Intent;

    .line 766
    const-string v0, "android.intent.category.LAUNCHER"

    invoke-virtual {v1, v0}, Landroid/content/Intent;->addCategory(Ljava/lang/String;)Landroid/content/Intent;

    .line 767
    const-string v0, "android.intent.action.MAIN"

    invoke-virtual {v1, v0}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 769
    const/4 v0, 0x0

    const/high16 v2, 0x8000000

    invoke-static {p0, v0, v1, v2}, Landroid/app/PendingIntent;->getActivity(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v1

    .line 771
    new-instance v0, Landroid/content/Intent;

    const-class v2, Lcom/osp/app/signin/OspReceiver;

    invoke-direct {v0, p0, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 772
    const-string v2, "samsungaccount_clear_notification"

    invoke-virtual {v0, v2}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 774
    const/4 v2, 0x0

    const/4 v3, 0x0

    invoke-static {p0, v2, v0, v3}, Landroid/app/PendingIntent;->getBroadcast(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v2

    .line 776
    const v0, 0x7f09004f

    invoke-virtual {p0, v0}, Lcom/osp/app/signin/BackgroundModeService;->getString(I)Ljava/lang/String;

    move-result-object v3

    const v0, 0x7f090028

    invoke-virtual {p0, v0}, Lcom/osp/app/signin/BackgroundModeService;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-static {}, Lcom/osp/app/signin/SamsungService;->i()I

    move-result v5

    const v6, 0x132df82

    move-object v0, p0

    invoke-static/range {v0 .. v6}, Lcom/msc/sa/c/d;->a(Landroid/content/Context;Landroid/app/PendingIntent;Landroid/app/PendingIntent;Ljava/lang/String;Ljava/lang/String;II)V

    .line 779
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v0, "BMS"

    const-string v1, "Show Samsung apps start Notification from email validation complete"

    invoke-static {v0, v1}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 781
    :cond_6
    invoke-virtual {p0, p3}, Lcom/osp/app/signin/BackgroundModeService;->stopSelf(I)V

    goto/16 :goto_1

    .line 783
    :cond_7
    if-eqz v3, :cond_8

    .line 799
    invoke-static {}, Lcom/msc/sa/selfupdate/c;->b()Lcom/msc/sa/selfupdate/c;

    move-result-object v0

    invoke-virtual {v0}, Lcom/msc/sa/selfupdate/c;->d()V

    .line 800
    invoke-virtual {p0}, Lcom/osp/app/signin/BackgroundModeService;->getApplication()Landroid/app/Application;

    move-result-object v0

    check-cast v0, Lcom/osp/app/signin/SamsungService;

    invoke-virtual {v0}, Lcom/osp/app/signin/SamsungService;->o()V

    goto/16 :goto_1

    .line 867
    :cond_8
    const-string v0, "client_id"

    invoke-virtual {p1, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, v1, Lcom/osp/app/util/d;->f:Ljava/lang/String;

    .line 868
    const-string v0, "client_secret"

    invoke-virtual {p1, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, v1, Lcom/osp/app/util/d;->g:Ljava/lang/String;

    .line 869
    const-string v0, "account_mode"

    invoke-virtual {p1, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, v1, Lcom/osp/app/util/d;->c:Ljava/lang/String;

    .line 870
    const-string v0, "OSP_VER"

    invoke-virtual {p1, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, v1, Lcom/osp/app/util/d;->l:Ljava/lang/String;

    .line 871
    const-string v0, "service_name"

    invoke-virtual {p1, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, v1, Lcom/osp/app/util/d;->m:Ljava/lang/String;

    .line 872
    const-string v0, "BG_WhoareU"

    invoke-virtual {p1, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, v1, Lcom/osp/app/util/d;->o:Ljava/lang/String;

    .line 873
    const-string v0, "key_request_id"

    const-wide/16 v2, 0x0

    invoke-virtual {p1, v0, v2, v3}, Landroid/content/Intent;->getLongExtra(Ljava/lang/String;J)J

    move-result-wide v2

    iput-wide v2, v1, Lcom/osp/app/util/d;->r:J

    .line 876
    const-string v0, "request_code"

    const/4 v2, 0x0

    invoke-virtual {p1, v0, v2}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v0

    iput v0, v1, Lcom/osp/app/util/d;->q:I

    .line 878
    const-string v0, "BG_mode"

    iget-object v2, v1, Lcom/osp/app/util/d;->o:Ljava/lang/String;

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_9

    .line 880
    const-string v0, "mypackage"

    invoke-virtual {p1, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, v1, Lcom/osp/app/util/d;->n:Ljava/lang/String;

    .line 888
    :cond_9
    new-instance v0, Lcom/osp/http/impl/a;

    invoke-direct {v0}, Lcom/osp/http/impl/a;-><init>()V

    iput-object v0, v1, Lcom/osp/app/util/d;->h:Lcom/osp/http/impl/a;

    .line 890
    const-string v0, "SamsungApps"

    iget-object v2, v1, Lcom/osp/app/util/d;->m:Ljava/lang/String;

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_a

    .line 892
    const/4 v0, 0x1

    iput-boolean v0, v1, Lcom/osp/app/util/d;->a:Z

    .line 896
    :cond_a
    iget-object v0, v1, Lcom/osp/app/util/d;->l:Ljava/lang/String;

    invoke-static {v0}, Lcom/osp/app/util/ad;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, v1, Lcom/osp/app/util/d;->l:Ljava/lang/String;

    .line 898
    invoke-virtual {p0}, Lcom/osp/app/signin/BackgroundModeService;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Landroid/preference/PreferenceManager;->getDefaultSharedPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v0

    .line 899
    const-string v2, "FLAG_DO_NOT_SHOW_NOTIFICATION"

    const/4 v3, 0x0

    invoke-interface {v0, v2, v3}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v2

    .line 900
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v0, "BMS"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "do not show again noti : "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v0, v3}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 901
    invoke-static {p0}, Lcom/osp/app/util/ad;->d(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_d

    const-string v0, "OSP_02"

    iget-object v3, v1, Lcom/osp/app/util/d;->l:Ljava/lang/String;

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_c

    const/4 v0, 0x1

    :goto_3
    invoke-static {p0, v0}, Lcom/msc/c/c;->a(Landroid/content/Context;Z)Z

    move-result v0

    if-nez v0, :cond_d

    .line 903
    if-nez v2, :cond_b

    .line 905
    invoke-direct {p0, v1}, Lcom/osp/app/signin/BackgroundModeService;->a(Lcom/osp/app/util/d;)V

    .line 907
    :cond_b
    new-instance v0, Landroid/content/Intent;

    const-string v2, "com.msc.action.samsungaccount.response.BackGroundSignin"

    invoke-direct {v0, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 908
    const-string v2, "bg_result"

    const/4 v3, 0x1

    invoke-virtual {v0, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 911
    const-string v2, "request_code"

    iget v3, v1, Lcom/osp/app/util/d;->q:I

    invoke-virtual {v0, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 913
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v3, v1, Lcom/osp/app/util/d;->n:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ":"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v2

    invoke-virtual {v0, v2}, Landroid/content/Intent;->setData(Landroid/net/Uri;)Landroid/content/Intent;

    .line 914
    iget-object v2, v1, Lcom/osp/app/util/d;->n:Ljava/lang/String;

    invoke-virtual {v0, v2}, Landroid/content/Intent;->setPackage(Ljava/lang/String;)Landroid/content/Intent;

    .line 917
    const-string v2, "client_id"

    iget-object v3, v1, Lcom/osp/app/util/d;->f:Ljava/lang/String;

    invoke-virtual {v0, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 918
    iget-wide v2, v1, Lcom/osp/app/util/d;->r:J

    invoke-virtual {p0, v0, v2, v3}, Lcom/osp/app/signin/BackgroundModeService;->a(Landroid/content/Intent;J)V

    .line 919
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v2, "sendBroadcast to "

    invoke-direct {v0, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v1, v1, Lcom/osp/app/util/d;->n:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;)V

    goto/16 :goto_1

    .line 901
    :cond_c
    const/4 v0, 0x0

    goto :goto_3

    .line 923
    :cond_d
    iget-object v0, p0, Lcom/osp/app/signin/BackgroundModeService;->b:Lcom/osp/app/signin/cu;

    invoke-virtual {v0}, Lcom/osp/app/signin/cu;->obtainMessage()Landroid/os/Message;

    move-result-object v0

    .line 924
    iput-object v1, v0, Landroid/os/Message;->obj:Ljava/lang/Object;

    .line 925
    iget-object v1, p0, Lcom/osp/app/signin/BackgroundModeService;->b:Lcom/osp/app/signin/cu;

    invoke-virtual {v1, v0}, Lcom/osp/app/signin/cu;->sendMessage(Landroid/os/Message;)Z

    goto/16 :goto_1

    :cond_e
    move v0, v1

    goto/16 :goto_2
.end method
