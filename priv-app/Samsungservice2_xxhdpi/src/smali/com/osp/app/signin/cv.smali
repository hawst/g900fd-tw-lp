.class final Lcom/osp/app/signin/cv;
.super Lcom/msc/c/b;
.source "BackgroundModeSignOutService.java"


# instance fields
.field protected c:Ljava/lang/Runnable;

.field protected d:Ljava/lang/Runnable;

.field protected e:Ljava/lang/Thread;

.field protected f:Ljava/lang/Thread;

.field final synthetic g:Lcom/osp/app/signin/BackgroundModeSignOutService;

.field private h:I

.field private i:I

.field private j:Z

.field private k:J

.field private l:J

.field private m:J

.field private n:J


# direct methods
.method public constructor <init>(Lcom/osp/app/signin/BackgroundModeSignOutService;)V
    .locals 2

    .prologue
    .line 286
    iput-object p1, p0, Lcom/osp/app/signin/cv;->g:Lcom/osp/app/signin/BackgroundModeSignOutService;

    .line 287
    invoke-direct {p0, p1}, Lcom/msc/c/b;-><init>(Landroid/content/Context;)V

    .line 290
    new-instance v0, Lcom/osp/app/signin/cw;

    invoke-direct {v0, p0, p1}, Lcom/osp/app/signin/cw;-><init>(Lcom/osp/app/signin/cv;Lcom/osp/app/signin/BackgroundModeSignOutService;)V

    iput-object v0, p0, Lcom/osp/app/signin/cv;->c:Ljava/lang/Runnable;

    .line 305
    new-instance v0, Lcom/osp/app/signin/cx;

    invoke-direct {v0, p0, p1}, Lcom/osp/app/signin/cx;-><init>(Lcom/osp/app/signin/cv;Lcom/osp/app/signin/BackgroundModeSignOutService;)V

    iput-object v0, p0, Lcom/osp/app/signin/cv;->d:Ljava/lang/Runnable;

    .line 313
    new-instance v0, Ljava/lang/Thread;

    iget-object v1, p0, Lcom/osp/app/signin/cv;->c:Ljava/lang/Runnable;

    invoke-direct {v0, v1}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;)V

    iput-object v0, p0, Lcom/osp/app/signin/cv;->e:Ljava/lang/Thread;

    .line 314
    new-instance v0, Ljava/lang/Thread;

    iget-object v1, p0, Lcom/osp/app/signin/cv;->d:Ljava/lang/Runnable;

    invoke-direct {v0, v1}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;)V

    iput-object v0, p0, Lcom/osp/app/signin/cv;->f:Ljava/lang/Thread;

    .line 317
    return-void
.end method

.method static synthetic a(Lcom/osp/app/signin/cv;I)I
    .locals 0

    .prologue
    .line 266
    iput p1, p0, Lcom/osp/app/signin/cv;->h:I

    return p1
.end method

.method private a(Ljava/lang/String;Z)V
    .locals 3

    .prologue
    .line 534
    iput-boolean p2, p0, Lcom/osp/app/signin/cv;->j:Z

    .line 536
    invoke-static {}, Lcom/msc/c/g;->a()Lcom/msc/c/g;

    .line 537
    iget-object v0, p0, Lcom/osp/app/signin/cv;->g:Lcom/osp/app/signin/BackgroundModeSignOutService;

    invoke-static {v0, p1, p0}, Lcom/msc/c/g;->a(Landroid/content/Context;Ljava/lang/String;Lcom/msc/b/h;)J

    move-result-wide v0

    iput-wide v0, p0, Lcom/osp/app/signin/cv;->m:J

    .line 539
    iget-wide v0, p0, Lcom/osp/app/signin/cv;->m:J

    invoke-virtual {p0, v0, v1}, Lcom/osp/app/signin/cv;->b(J)V

    .line 540
    iget-wide v0, p0, Lcom/osp/app/signin/cv;->m:J

    const-string v2, "from_json"

    invoke-virtual {p0, v0, v1, v2}, Lcom/osp/app/signin/cv;->a(JLjava/lang/String;)V

    .line 542
    iget-wide v0, p0, Lcom/osp/app/signin/cv;->m:J

    sget-object v2, Lcom/msc/b/g;->c:Lcom/msc/b/g;

    invoke-static {v0, v1, v2}, Lcom/msc/c/g;->a(JLcom/msc/b/g;)V

    .line 543
    return-void
.end method

.method private static a(Ljava/lang/Thread;)V
    .locals 2

    .prologue
    .line 328
    if-eqz p0, :cond_0

    invoke-virtual {p0}, Ljava/lang/Thread;->isAlive()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 332
    const-wide/16 v0, 0x12c

    :try_start_0
    invoke-virtual {p0, v0, v1}, Ljava/lang/Thread;->join(J)V
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0

    .line 337
    :cond_0
    :goto_0
    return-void

    .line 333
    :catch_0
    move-exception v0

    invoke-virtual {v0}, Ljava/lang/InterruptedException;->printStackTrace()V

    goto :goto_0
.end method

.method static synthetic a(Lcom/osp/app/signin/cv;)Z
    .locals 1

    .prologue
    .line 266
    invoke-direct {p0}, Lcom/osp/app/signin/cv;->h()Z

    move-result v0

    return v0
.end method

.method static synthetic b(Lcom/osp/app/signin/cv;)V
    .locals 3

    .prologue
    .line 266
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v0, "BSOP"

    const-string v1, "signOutUser"

    const-string v2, "START"

    invoke-static {v0, v1, v2}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/osp/app/signin/cv;->g:Lcom/osp/app/signin/BackgroundModeSignOutService;

    invoke-static {v0}, Lcom/msc/c/e;->g(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x0

    invoke-direct {p0, v0, v1}, Lcom/osp/app/signin/cv;->a(Ljava/lang/String;Z)V

    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v0, "BSOP"

    const-string v1, "signOutUser"

    const-string v2, "END"

    invoke-static {v0, v1, v2}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method

.method private h()Z
    .locals 4

    .prologue
    .line 485
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v0, "BSOP"

    const-string v1, "deleteDB_V01"

    const-string v2, "START"

    invoke-static {v0, v1, v2}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 486
    iget-object v0, p0, Lcom/osp/app/signin/cv;->g:Lcom/osp/app/signin/BackgroundModeSignOutService;

    const-string v1, "SignOut"

    const-string v2, "true"

    :try_start_0
    new-instance v3, Lcom/osp/common/property/a;

    invoke-direct {v3, v0}, Lcom/osp/common/property/a;-><init>(Landroid/content/Context;)V

    invoke-virtual {v3, v1, v2}, Lcom/osp/common/property/a;->b(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catch Lcom/osp/common/property/PropertyException; {:try_start_0 .. :try_end_0} :catch_0

    .line 488
    :goto_0
    iget-object v0, p0, Lcom/osp/app/signin/cv;->g:Lcom/osp/app/signin/BackgroundModeSignOutService;

    invoke-static {v0}, Lcom/msc/c/c;->d(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v0

    .line 489
    iget-object v1, p0, Lcom/osp/app/signin/cv;->g:Lcom/osp/app/signin/BackgroundModeSignOutService;

    invoke-static {v1}, Lcom/msc/c/c;->c(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v1

    .line 491
    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_0

    invoke-static {}, Lcom/msc/c/g;->a()Lcom/msc/c/g;

    iget-object v2, p0, Lcom/osp/app/signin/cv;->g:Lcom/osp/app/signin/BackgroundModeSignOutService;

    invoke-static {v2, v0, v1, p0}, Lcom/msc/c/g;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Lcom/msc/b/h;)J

    move-result-wide v0

    iput-wide v0, p0, Lcom/osp/app/signin/cv;->k:J

    iget-wide v0, p0, Lcom/osp/app/signin/cv;->k:J

    invoke-virtual {p0, v0, v1}, Lcom/osp/app/signin/cv;->a(J)V

    iget-wide v0, p0, Lcom/osp/app/signin/cv;->k:J

    const-string v2, "none"

    invoke-virtual {p0, v0, v1, v2}, Lcom/osp/app/signin/cv;->a(JLjava/lang/String;)V

    iget-wide v0, p0, Lcom/osp/app/signin/cv;->k:J

    sget-object v2, Lcom/msc/b/g;->a:Lcom/msc/b/g;

    invoke-static {v0, v1, v2}, Lcom/msc/c/g;->a(JLcom/msc/b/g;)V

    .line 493
    :cond_0
    iget-object v0, p0, Lcom/osp/app/signin/cv;->g:Lcom/osp/app/signin/BackgroundModeSignOutService;

    const-string v1, "SignOut"

    invoke-static {v0, v1}, Lcom/msc/c/c;->b(Landroid/content/Context;Ljava/lang/String;)V

    .line 495
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v0, "BSOP"

    const-string v1, "remove appid form property."

    invoke-static {v0, v1}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 496
    iget-object v0, p0, Lcom/osp/app/signin/cv;->g:Lcom/osp/app/signin/BackgroundModeSignOutService;

    const-string v1, "device.registration.appid"

    invoke-static {v0, v1}, Lcom/msc/c/c;->b(Landroid/content/Context;Ljava/lang/String;)V

    .line 498
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v0, "BSOP"

    const-string v1, "deleteDB_V01"

    const-string v2, "END"

    invoke-static {v0, v1, v2}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 499
    const/4 v0, 0x1

    return v0

    .line 486
    :catch_0
    move-exception v0

    invoke-virtual {v0}, Lcom/osp/common/property/PropertyException;->printStackTrace()V

    goto :goto_0
.end method


# virtual methods
.method protected final varargs a()Ljava/lang/Boolean;
    .locals 2

    .prologue
    const/4 v1, -0x1

    .line 343
    iget-object v0, p0, Lcom/osp/app/signin/cv;->e:Ljava/lang/Thread;

    invoke-virtual {v0}, Ljava/lang/Thread;->start()V

    .line 344
    iget-object v0, p0, Lcom/osp/app/signin/cv;->f:Ljava/lang/Thread;

    invoke-virtual {v0}, Ljava/lang/Thread;->start()V

    .line 346
    :cond_0
    iget-object v0, p0, Lcom/osp/app/signin/cv;->g:Lcom/osp/app/signin/BackgroundModeSignOutService;

    invoke-static {v0}, Lcom/osp/app/signin/BackgroundModeSignOutService;->c(Lcom/osp/app/signin/BackgroundModeSignOutService;)Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/osp/app/signin/cv;->g:Lcom/osp/app/signin/BackgroundModeSignOutService;

    invoke-static {v0}, Lcom/osp/app/signin/BackgroundModeSignOutService;->d(Lcom/osp/app/signin/BackgroundModeSignOutService;)Z

    move-result v0

    if-nez v0, :cond_2

    .line 348
    :cond_1
    iget-object v0, p0, Lcom/osp/app/signin/cv;->g:Lcom/osp/app/signin/BackgroundModeSignOutService;

    invoke-static {v0}, Lcom/osp/app/signin/BackgroundModeSignOutService;->c(Lcom/osp/app/signin/BackgroundModeSignOutService;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/osp/app/signin/cv;->g:Lcom/osp/app/signin/BackgroundModeSignOutService;

    invoke-static {v0}, Lcom/osp/app/signin/BackgroundModeSignOutService;->d(Lcom/osp/app/signin/BackgroundModeSignOutService;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 350
    :cond_2
    iget v0, p0, Lcom/osp/app/signin/cv;->h:I

    if-ne v0, v1, :cond_3

    iget v0, p0, Lcom/osp/app/signin/cv;->i:I

    if-ne v0, v1, :cond_3

    .line 355
    iget-object v0, p0, Lcom/osp/app/signin/cv;->g:Lcom/osp/app/signin/BackgroundModeSignOutService;

    invoke-static {v0}, Lcom/osp/app/signin/BackgroundModeSignOutService;->e(Lcom/osp/app/signin/BackgroundModeSignOutService;)Z

    move-result v0

    if-nez v0, :cond_3

    .line 358
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v0, "LogOut Success"

    invoke-static {v0}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;)V

    .line 361
    :cond_3
    const/4 v0, 0x0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    return-object v0
.end method

.method public final declared-synchronized a(Lcom/msc/b/c;)V
    .locals 6

    .prologue
    .line 378
    monitor-enter p0

    :try_start_0
    invoke-super {p0, p1}, Lcom/msc/c/b;->a(Lcom/msc/b/c;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 380
    if-nez p1, :cond_1

    .line 428
    :cond_0
    :goto_0
    monitor-exit p0

    return-void

    .line 385
    :cond_1
    :try_start_1
    invoke-virtual {p1}, Lcom/msc/b/c;->a()J

    move-result-wide v0

    .line 386
    invoke-virtual {p1}, Lcom/msc/b/c;->e()Ljava/lang/String;

    move-result-object v2

    .line 388
    iget-wide v4, p0, Lcom/osp/app/signin/cv;->k:J

    cmp-long v3, v0, v4

    if-nez v3, :cond_2

    .line 390
    iget-object v0, p0, Lcom/osp/app/signin/cv;->g:Lcom/osp/app/signin/BackgroundModeSignOutService;

    invoke-static {v0}, Lcom/msc/c/c;->b(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v0

    .line 391
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_0

    invoke-static {}, Lcom/msc/c/g;->a()Lcom/msc/c/g;

    iget-object v1, p0, Lcom/osp/app/signin/cv;->g:Lcom/osp/app/signin/BackgroundModeSignOutService;

    invoke-static {v1, v0, p0}, Lcom/msc/c/g;->b(Landroid/content/Context;Ljava/lang/String;Lcom/msc/b/h;)J

    move-result-wide v0

    iput-wide v0, p0, Lcom/osp/app/signin/cv;->l:J

    iget-wide v0, p0, Lcom/osp/app/signin/cv;->l:J

    invoke-virtual {p0, v0, v1}, Lcom/osp/app/signin/cv;->a(J)V

    iget-wide v0, p0, Lcom/osp/app/signin/cv;->l:J

    const-string v2, "none"

    invoke-virtual {p0, v0, v1, v2}, Lcom/osp/app/signin/cv;->a(JLjava/lang/String;)V

    iget-wide v0, p0, Lcom/osp/app/signin/cv;->l:J

    sget-object v2, Lcom/msc/b/g;->a:Lcom/msc/b/g;

    invoke-static {v0, v1, v2}, Lcom/msc/c/g;->a(JLcom/msc/b/g;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 378
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 392
    :cond_2
    :try_start_2
    iget-wide v4, p0, Lcom/osp/app/signin/cv;->m:J
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    cmp-long v3, v0, v4

    if-nez v3, :cond_8

    .line 397
    :try_start_3
    invoke-static {}, Lcom/msc/c/h;->a()Lcom/msc/c/h;

    const/4 v0, 0x0

    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    invoke-static {v2}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;)V

    new-instance v1, Lorg/json/JSONObject;

    invoke-direct {v1, v2}, Lorg/json/JSONObject;-><init>(Ljava/lang/String;)V

    const-string v2, "result"

    invoke-virtual {v1, v2}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_3

    const-string v0, "result"

    invoke-virtual {v1, v0}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    :cond_3
    const-string v2, "error"

    invoke-virtual {v1, v2}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_4

    const-string v0, "error"

    invoke-virtual {v1, v0}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    :cond_4
    const-string v2, "state"

    invoke-virtual {v1, v2}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_5

    const-string v0, "state"

    invoke-virtual {v1, v0}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    :cond_5
    const-string v2, "error_description"

    invoke-virtual {v1, v2}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_6

    const-string v0, "error_description"

    invoke-virtual {v1, v0}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    :cond_6
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "RequestSet::SignOutfromJSON result : "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;)V

    .line 399
    const-string v1, "true"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_7

    .line 401
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v0, "BSOP"

    const-string v1, "RequestSignOut isSignOut = true"

    const-string v2, "END"

    invoke-static {v0, v1, v2}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 402
    const/4 v0, -0x1

    iput v0, p0, Lcom/osp/app/signin/cv;->i:I
    :try_end_3
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_0
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    goto/16 :goto_0

    .line 408
    :catch_0
    move-exception v0

    :try_start_4
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    goto/16 :goto_0

    .line 403
    :cond_7
    :try_start_5
    const-string v1, "false"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 405
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v0, "BSOP"

    const-string v1, "RequestSignOut isSignOut = false"

    const-string v2, "END"

    invoke-static {v0, v1, v2}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 406
    const/4 v0, 0x1

    iput v0, p0, Lcom/osp/app/signin/cv;->i:I
    :try_end_5
    .catch Ljava/lang/Exception; {:try_start_5 .. :try_end_5} :catch_0
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    goto/16 :goto_0

    .line 412
    :cond_8
    :try_start_6
    iget-wide v4, p0, Lcom/osp/app/signin/cv;->n:J
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_0

    cmp-long v0, v0, v4

    if-nez v0, :cond_0

    .line 416
    :try_start_7
    iget-object v0, p0, Lcom/osp/app/signin/cv;->g:Lcom/osp/app/signin/BackgroundModeSignOutService;

    invoke-static {}, Lcom/msc/c/h;->a()Lcom/msc/c/h;

    invoke-static {v2}, Lcom/msc/c/h;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/osp/app/signin/BackgroundModeSignOutService;->a(Lcom/osp/app/signin/BackgroundModeSignOutService;Ljava/lang/String;)Ljava/lang/String;

    .line 417
    iget-object v0, p0, Lcom/osp/app/signin/cv;->g:Lcom/osp/app/signin/BackgroundModeSignOutService;

    invoke-static {v0}, Lcom/osp/app/signin/BackgroundModeSignOutService;->f(Lcom/osp/app/signin/BackgroundModeSignOutService;)Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 419
    iget-object v0, p0, Lcom/osp/app/signin/cv;->g:Lcom/osp/app/signin/BackgroundModeSignOutService;

    invoke-static {v0}, Lcom/osp/app/signin/BackgroundModeSignOutService;->f(Lcom/osp/app/signin/BackgroundModeSignOutService;)Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x1

    invoke-direct {p0, v0, v1}, Lcom/osp/app/signin/cv;->a(Ljava/lang/String;Z)V
    :try_end_7
    .catch Ljava/lang/Exception; {:try_start_7 .. :try_end_7} :catch_1
    .catchall {:try_start_7 .. :try_end_7} :catchall_0

    goto/16 :goto_0

    .line 421
    :catch_1
    move-exception v0

    :try_start_8
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    .line 424
    const/4 v0, 0x1

    iput v0, p0, Lcom/osp/app/signin/cv;->i:I
    :try_end_8
    .catchall {:try_start_8 .. :try_end_8} :catchall_0

    goto/16 :goto_0
.end method

.method protected final a(Ljava/lang/Boolean;)V
    .locals 2

    .prologue
    .line 369
    iget-object v0, p0, Lcom/osp/app/signin/cv;->g:Lcom/osp/app/signin/BackgroundModeSignOutService;

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lcom/osp/app/signin/BackgroundModeSignOutService;->a(Lcom/osp/app/signin/BackgroundModeSignOutService;Z)V

    .line 374
    return-void
.end method

.method public final b(Lcom/msc/b/c;)V
    .locals 7

    .prologue
    const/4 v6, 0x1

    .line 432
    invoke-super {p0, p1}, Lcom/msc/c/b;->b(Lcom/msc/b/c;)V

    .line 434
    if-nez p1, :cond_1

    .line 482
    :cond_0
    :goto_0
    return-void

    .line 439
    :cond_1
    invoke-virtual {p1}, Lcom/msc/b/c;->a()J

    move-result-wide v0

    .line 440
    invoke-virtual {p1}, Lcom/msc/b/c;->e()Ljava/lang/String;

    move-result-object v2

    .line 441
    invoke-virtual {p1}, Lcom/msc/b/c;->f()Ljava/lang/Exception;

    move-result-object v3

    .line 446
    iget-wide v4, p0, Lcom/osp/app/signin/cv;->m:J

    cmp-long v4, v0, v4

    if-nez v4, :cond_7

    .line 448
    if-eqz v3, :cond_2

    .line 450
    iput v6, p0, Lcom/osp/app/signin/cv;->i:I

    goto :goto_0

    .line 451
    :cond_2
    if-eqz v2, :cond_0

    .line 453
    iget-object v0, p0, Lcom/osp/app/signin/cv;->b:Lcom/msc/c/f;

    invoke-virtual {v0}, Lcom/msc/c/f;->a()Ljava/lang/String;

    move-result-object v0

    .line 454
    iget-boolean v1, p0, Lcom/osp/app/signin/cv;->j:Z

    if-nez v1, :cond_5

    .line 456
    const-string v1, "AUT_1005"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_3

    const-string v1, "AUT_1014"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_3

    const-string v1, "AUT_1810"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 459
    :cond_3
    iput v6, p0, Lcom/osp/app/signin/cv;->i:I

    goto :goto_0

    .line 465
    :cond_4
    iput v6, p0, Lcom/osp/app/signin/cv;->i:I

    goto :goto_0

    .line 469
    :cond_5
    const-string v1, "AUT_1810"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_6

    .line 471
    const/4 v0, -0x1

    iput v0, p0, Lcom/osp/app/signin/cv;->i:I

    goto :goto_0

    .line 474
    :cond_6
    iput v6, p0, Lcom/osp/app/signin/cv;->i:I

    goto :goto_0

    .line 478
    :cond_7
    iget-wide v2, p0, Lcom/osp/app/signin/cv;->n:J

    cmp-long v0, v0, v2

    if-nez v0, :cond_0

    .line 480
    iput v6, p0, Lcom/osp/app/signin/cv;->i:I

    goto :goto_0
.end method

.method protected final d()V
    .locals 2

    .prologue
    .line 321
    invoke-super {p0}, Lcom/msc/c/b;->d()V

    .line 322
    iget-object v0, p0, Lcom/osp/app/signin/cv;->e:Ljava/lang/Thread;

    invoke-static {v0}, Lcom/osp/app/signin/cv;->a(Ljava/lang/Thread;)V

    .line 323
    iget-object v0, p0, Lcom/osp/app/signin/cv;->f:Ljava/lang/Thread;

    invoke-static {v0}, Lcom/osp/app/signin/cv;->a(Ljava/lang/Thread;)V

    .line 324
    iget-object v0, p0, Lcom/osp/app/signin/cv;->g:Lcom/osp/app/signin/BackgroundModeSignOutService;

    const/4 v1, 0x1

    invoke-static {v0, v1}, Lcom/osp/app/signin/BackgroundModeSignOutService;->a(Lcom/osp/app/signin/BackgroundModeSignOutService;Z)V

    .line 325
    return-void
.end method

.method protected final synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 266
    invoke-virtual {p0}, Lcom/osp/app/signin/cv;->a()Ljava/lang/Boolean;

    move-result-object v0

    return-object v0
.end method

.method protected final synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 266
    check-cast p1, Ljava/lang/Boolean;

    invoke-virtual {p0, p1}, Lcom/osp/app/signin/cv;->a(Ljava/lang/Boolean;)V

    return-void
.end method
