.class public Lcom/osp/app/signin/SignUpCompleteView;
.super Lcom/osp/app/util/BaseActivity;
.source "SignUpCompleteView.java"


# instance fields
.field private A:Z

.field private final B:Landroid/view/View$OnClickListener;

.field private a:Landroid/content/Intent;

.field private b:Landroid/widget/TextView;

.field private c:Landroid/widget/TextView;

.field private d:J

.field private e:Ljava/lang/String;

.field private f:Lcom/osp/app/signin/BottomSoftkeyLinearLayout;

.field private y:Landroid/app/ProgressDialog;

.field private z:Z


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 43
    invoke-direct {p0}, Lcom/osp/app/util/BaseActivity;-><init>()V

    .line 81
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/osp/app/signin/SignUpCompleteView;->y:Landroid/app/ProgressDialog;

    .line 86
    iput-boolean v1, p0, Lcom/osp/app/signin/SignUpCompleteView;->z:Z

    .line 91
    iput-boolean v1, p0, Lcom/osp/app/signin/SignUpCompleteView;->A:Z

    .line 216
    new-instance v0, Lcom/osp/app/signin/kc;

    invoke-direct {v0, p0}, Lcom/osp/app/signin/kc;-><init>(Lcom/osp/app/signin/SignUpCompleteView;)V

    iput-object v0, p0, Lcom/osp/app/signin/SignUpCompleteView;->B:Landroid/view/View$OnClickListener;

    return-void
.end method

.method private a(I)V
    .locals 5

    .prologue
    const v3, 0x7f08011e

    .line 391
    invoke-static {}, Lcom/osp/device/b;->b()Lcom/osp/device/b;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/osp/device/b;->i(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 393
    invoke-virtual {p0}, Lcom/osp/app/signin/SignUpCompleteView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f08011f

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v0

    float-to-int v0, v0

    .line 394
    invoke-virtual {p0}, Lcom/osp/app/signin/SignUpCompleteView;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1, v3}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v1

    float-to-int v1, v1

    .line 395
    invoke-static {}, Lcom/osp/app/signin/SamsungService;->a()Z

    move-result v2

    if-nez v2, :cond_0

    .line 397
    invoke-virtual {p0}, Lcom/osp/app/signin/SignUpCompleteView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0, v3}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v0

    float-to-int v0, v0

    .line 400
    :cond_0
    const v2, 0x7f0c012d

    invoke-virtual {p0, v2}, Lcom/osp/app/signin/SignUpCompleteView;->findViewById(I)Landroid/view/View;

    move-result-object v2

    .line 401
    invoke-static {}, Lcom/osp/app/util/q;->a()Lcom/osp/app/util/q;

    invoke-static {v2, v0, v1, p1}, Lcom/osp/app/util/q;->b(Landroid/view/View;III)V

    .line 404
    invoke-static {p0}, Lcom/osp/app/util/r;->k(Landroid/content/Context;)Z

    move-result v2

    if-nez v2, :cond_3

    .line 406
    invoke-virtual {p0}, Lcom/osp/app/signin/SignUpCompleteView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f08013d

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v0

    float-to-int v1, v0

    .line 407
    invoke-virtual {p0}, Lcom/osp/app/signin/SignUpCompleteView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v2, 0x7f08013c

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v0

    float-to-int v0, v0

    .line 410
    :goto_0
    iget-object v2, p0, Lcom/osp/app/signin/SignUpCompleteView;->f:Lcom/osp/app/signin/BottomSoftkeyLinearLayout;

    if-eqz v2, :cond_2

    .line 412
    invoke-static {}, Lcom/osp/app/signin/SamsungService;->a()Z

    move-result v2

    if-eqz v2, :cond_2

    .line 414
    sget-object v2, Lcom/osp/app/util/u;->o:[Ljava/lang/String;

    invoke-static {v2}, Lcom/osp/app/util/u;->a([Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 416
    const/4 v1, 0x0

    .line 418
    :cond_1
    invoke-static {}, Lcom/osp/app/util/q;->a()Lcom/osp/app/util/q;

    iget-object v2, p0, Lcom/osp/app/signin/SignUpCompleteView;->f:Lcom/osp/app/signin/BottomSoftkeyLinearLayout;

    invoke-static {p0, v2, v1, v0, p1}, Lcom/osp/app/util/q;->a(Landroid/content/Context;Landroid/view/View;III)V

    .line 422
    :cond_2
    return-void

    :cond_3
    move v4, v1

    move v1, v0

    move v0, v4

    goto :goto_0
.end method

.method static synthetic a(Lcom/osp/app/signin/SignUpCompleteView;)V
    .locals 0

    .prologue
    .line 43
    invoke-direct {p0}, Lcom/osp/app/signin/SignUpCompleteView;->c()V

    return-void
.end method

.method private c()V
    .locals 10

    .prologue
    const/4 v9, 0x3

    const/4 v8, 0x1

    .line 226
    iget-object v0, p0, Lcom/osp/app/signin/SignUpCompleteView;->a:Landroid/content/Intent;

    const-string v1, "signup_in_setupwizard"

    invoke-virtual {v0, v1, v8}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    const/16 v0, 0xb

    iget-object v1, p0, Lcom/osp/app/signin/SignUpCompleteView;->a:Landroid/content/Intent;

    invoke-virtual {p0, v0, v1}, Lcom/osp/app/signin/SignUpCompleteView;->a(ILandroid/content/Intent;)V

    new-instance v0, Ljava/util/LinkedList;

    invoke-direct {v0}, Ljava/util/LinkedList;-><init>()V

    invoke-static {}, Lcom/osp/app/bigdatalog/e;->a()Lcom/osp/app/bigdatalog/e;

    move-result-object v1

    invoke-virtual {p0}, Lcom/osp/app/signin/SignUpCompleteView;->getApplicationContext()Landroid/content/Context;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/osp/app/bigdatalog/e;->a(Landroid/content/Context;)V

    iget-object v1, p0, Lcom/osp/app/signin/SignUpCompleteView;->a:Landroid/content/Intent;

    const-string v2, "stay_duration"

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v4

    iget-wide v6, p0, Lcom/osp/app/signin/SignUpCompleteView;->d:J

    sub-long/2addr v4, v6

    invoke-virtual {v1, v2, v4, v5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;J)Landroid/content/Intent;

    iget-object v1, p0, Lcom/osp/app/signin/SignUpCompleteView;->e:Ljava/lang/String;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/osp/app/signin/SignUpCompleteView;->a:Landroid/content/Intent;

    const-string v2, "previous_activity"

    iget-object v3, p0, Lcom/osp/app/signin/SignUpCompleteView;->e:Ljava/lang/String;

    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    :cond_0
    new-instance v1, Lcom/osp/app/bigdatalog/c;

    invoke-virtual {p0}, Lcom/osp/app/signin/SignUpCompleteView;->getApplicationContext()Landroid/content/Context;

    move-result-object v2

    const/4 v3, 0x2

    iget-object v4, p0, Lcom/osp/app/signin/SignUpCompleteView;->a:Landroid/content/Intent;

    invoke-direct {v1, v2, v9, v3, v4}, Lcom/osp/app/bigdatalog/c;-><init>(Landroid/content/Context;IILandroid/content/Intent;)V

    invoke-interface {v0, v1}, Ljava/util/Queue;->add(Ljava/lang/Object;)Z

    new-instance v1, Lcom/osp/app/bigdatalog/c;

    invoke-virtual {p0}, Lcom/osp/app/signin/SignUpCompleteView;->getApplicationContext()Landroid/content/Context;

    move-result-object v2

    iget-object v3, p0, Lcom/osp/app/signin/SignUpCompleteView;->a:Landroid/content/Intent;

    invoke-direct {v1, v2, v9, v8, v3}, Lcom/osp/app/bigdatalog/c;-><init>(Landroid/content/Context;IILandroid/content/Intent;)V

    invoke-interface {v0, v1}, Ljava/util/Queue;->add(Ljava/lang/Object;)Z

    invoke-static {}, Lcom/osp/app/bigdatalog/e;->a()Lcom/osp/app/bigdatalog/e;

    move-result-object v1

    invoke-virtual {p0}, Lcom/osp/app/signin/SignUpCompleteView;->getApplicationContext()Landroid/content/Context;

    move-result-object v2

    invoke-virtual {v1, v2, v0}, Lcom/osp/app/bigdatalog/e;->a(Landroid/content/Context;Ljava/util/Queue;)V

    invoke-static {p0}, Lcom/osp/app/util/r;->p(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_1

    iget-boolean v0, p0, Lcom/osp/app/signin/SignUpCompleteView;->z:Z

    if-eqz v0, :cond_1

    invoke-static {}, Lcom/osp/app/util/m;->a()Lcom/osp/app/util/m;

    iget-boolean v0, p0, Lcom/osp/app/signin/SignUpCompleteView;->A:Z

    invoke-static {p0, v0}, Lcom/osp/app/util/m;->a(Landroid/app/Activity;Z)V

    .line 227
    :goto_0
    return-void

    .line 226
    :cond_1
    invoke-virtual {p0}, Lcom/osp/app/signin/SignUpCompleteView;->finish()V

    goto :goto_0
.end method


# virtual methods
.method protected final a()V
    .locals 0

    .prologue
    .line 293
    return-void
.end method

.method protected final b()V
    .locals 0

    .prologue
    .line 298
    return-void
.end method

.method protected onActivityResult(IILandroid/content/Intent;)V
    .locals 1

    .prologue
    .line 656
    invoke-super {p0, p1, p2, p3}, Lcom/osp/app/util/BaseActivity;->onActivityResult(IILandroid/content/Intent;)V

    .line 658
    const/16 v0, 0xee

    if-ne p1, v0, :cond_0

    .line 660
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v0, "onActivityResult - REQUEST_CODE_AUTH_RESULT_FOR_EASYSIGNUP"

    invoke-static {v0}, Lcom/osp/app/util/aq;->c(Ljava/lang/String;)V

    .line 661
    invoke-virtual {p0}, Lcom/osp/app/signin/SignUpCompleteView;->finish()V

    .line 664
    :cond_0
    return-void
.end method

.method public onBackPressed()V
    .locals 4

    .prologue
    const/4 v3, 0x3

    .line 198
    iget-object v0, p0, Lcom/osp/app/signin/SignUpCompleteView;->a:Landroid/content/Intent;

    const-string v1, "signup_in_setupwizard"

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 199
    const/16 v0, 0xb

    iget-object v1, p0, Lcom/osp/app/signin/SignUpCompleteView;->a:Landroid/content/Intent;

    invoke-virtual {p0, v0, v1}, Lcom/osp/app/signin/SignUpCompleteView;->a(ILandroid/content/Intent;)V

    .line 205
    new-instance v0, Lcom/osp/app/bigdatalog/c;

    invoke-virtual {p0}, Lcom/osp/app/signin/SignUpCompleteView;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    iget-object v2, p0, Lcom/osp/app/signin/SignUpCompleteView;->a:Landroid/content/Intent;

    invoke-direct {v0, v1, v3, v3, v2}, Lcom/osp/app/bigdatalog/c;-><init>(Landroid/content/Context;IILandroid/content/Intent;)V

    .line 207
    invoke-static {}, Lcom/osp/app/bigdatalog/e;->a()Lcom/osp/app/bigdatalog/e;

    move-result-object v1

    invoke-virtual {p0}, Lcom/osp/app/signin/SignUpCompleteView;->getApplicationContext()Landroid/content/Context;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/osp/app/bigdatalog/e;->a(Landroid/content/Context;)V

    .line 208
    invoke-static {}, Lcom/osp/app/bigdatalog/e;->a()Lcom/osp/app/bigdatalog/e;

    move-result-object v1

    invoke-virtual {p0}, Lcom/osp/app/signin/SignUpCompleteView;->getApplicationContext()Landroid/content/Context;

    move-result-object v2

    invoke-virtual {v1, v2, v0}, Lcom/osp/app/bigdatalog/e;->a(Landroid/content/Context;Lcom/osp/app/bigdatalog/c;)V

    .line 210
    invoke-super {p0}, Lcom/osp/app/util/BaseActivity;->onBackPressed()V

    .line 211
    return-void
.end method

.method public onConfigurationChanged(Landroid/content/res/Configuration;)V
    .locals 1

    .prologue
    .line 303
    invoke-super {p0, p1}, Lcom/osp/app/util/BaseActivity;->onConfigurationChanged(Landroid/content/res/Configuration;)V

    .line 305
    invoke-static {}, Lcom/osp/app/signin/SamsungService;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-static {p0}, Lcom/osp/app/util/r;->k(Landroid/content/Context;)Z

    move-result v0

    if-nez v0, :cond_0

    invoke-static {p0}, Lcom/osp/app/util/r;->m(Landroid/content/Context;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 307
    const v0, 0x7f0c00c9

    invoke-virtual {p0, v0}, Lcom/osp/app/signin/SignUpCompleteView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/osp/app/signin/SignUpCompleteView;->a(Landroid/view/View;)V

    .line 309
    :cond_0
    iget v0, p1, Landroid/content/res/Configuration;->orientation:I

    invoke-direct {p0, v0}, Lcom/osp/app/signin/SignUpCompleteView;->a(I)V

    .line 311
    return-void
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 12

    .prologue
    const/16 v6, 0xe

    const/16 v11, 0x8

    const v10, 0x7f070004

    const/4 v9, 0x1

    const/4 v8, 0x0

    .line 95
    invoke-super {p0, p1}, Lcom/osp/app/util/BaseActivity;->onCreate(Landroid/os/Bundle;)V

    .line 97
    invoke-virtual {p0}, Lcom/osp/app/signin/SignUpCompleteView;->getIntent()Landroid/content/Intent;

    move-result-object v0

    iput-object v0, p0, Lcom/osp/app/signin/SignUpCompleteView;->a:Landroid/content/Intent;

    .line 98
    invoke-virtual {p0}, Lcom/osp/app/signin/SignUpCompleteView;->getCallingActivity()Landroid/content/ComponentName;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 100
    invoke-virtual {p0}, Lcom/osp/app/signin/SignUpCompleteView;->getCallingActivity()Landroid/content/ComponentName;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/ComponentName;->getShortClassName()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/osp/app/signin/SignUpCompleteView;->e:Ljava/lang/String;

    .line 102
    :cond_0
    iget-object v0, p0, Lcom/osp/app/signin/SignUpCompleteView;->a:Landroid/content/Intent;

    const-string v1, "email_id"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 103
    iget-object v0, p0, Lcom/osp/app/signin/SignUpCompleteView;->a:Landroid/content/Intent;

    const-string v2, "key_marketingpopup_mode"

    invoke-virtual {v0, v2, v8}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v2

    .line 104
    iget-object v0, p0, Lcom/osp/app/signin/SignUpCompleteView;->a:Landroid/content/Intent;

    const-string v3, "key_welcome_content"

    invoke-virtual {v0, v3}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 105
    invoke-static {v1}, Landroid/telephony/PhoneNumberUtils;->isGlobalPhoneNumber(Ljava/lang/String;)Z

    move-result v4

    .line 106
    iget-object v0, p0, Lcom/osp/app/signin/SignUpCompleteView;->a:Landroid/content/Intent;

    const-string v5, "key_easy_signup_mode"

    invoke-virtual {v0, v5, v8}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v0

    iput-boolean v0, p0, Lcom/osp/app/signin/SignUpCompleteView;->z:Z

    .line 107
    iget-object v0, p0, Lcom/osp/app/signin/SignUpCompleteView;->a:Landroid/content/Intent;

    const-string v5, "key_easy_signup_receive_marketing"

    invoke-virtual {v0, v5, v8}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v0

    iput-boolean v0, p0, Lcom/osp/app/signin/SignUpCompleteView;->A:Z

    .line 109
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v5, "mEasySignupMode : "

    invoke-direct {v0, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-boolean v5, p0, Lcom/osp/app/signin/SignUpCompleteView;->z:Z

    invoke-virtual {v0, v5}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;)V

    .line 111
    invoke-static {p0}, Lcom/osp/app/util/r;->n(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_13

    .line 113
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    if-ge v0, v6, :cond_11

    .line 115
    invoke-virtual {p0, v9}, Lcom/osp/app/signin/SignUpCompleteView;->requestWindowFeature(I)Z

    .line 140
    :cond_1
    :goto_0
    const v0, 0x7f030030

    invoke-static {p0}, Lcom/osp/app/util/r;->q(Landroid/content/Context;)Z

    move-result v5

    invoke-virtual {p0, v0, v5}, Lcom/osp/app/signin/SignUpCompleteView;->a(IZ)V

    .line 141
    invoke-virtual {p0}, Lcom/osp/app/signin/SignUpCompleteView;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    invoke-static {p0}, Lcom/osp/app/util/r;->k(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_2

    const v0, 0x7f0c017b

    invoke-virtual {p0, v0}, Lcom/osp/app/signin/SignUpCompleteView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v6

    const/4 v7, -0x2

    iput v7, v6, Landroid/view/ViewGroup$LayoutParams;->height:I

    invoke-virtual {v0, v6}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    :cond_2
    invoke-static {}, Lcom/osp/device/b;->b()Lcom/osp/device/b;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/osp/device/b;->i(Landroid/content/Context;)Z

    move-result v0

    if-nez v0, :cond_5

    invoke-static {}, Lcom/osp/app/signin/SamsungService;->a()Z

    move-result v0

    if-nez v0, :cond_3

    invoke-static {p0}, Lcom/osp/app/util/r;->n(Landroid/content/Context;)Z

    move-result v0

    if-nez v0, :cond_3

    const v0, 0x7f0c012e

    invoke-virtual {p0, v0}, Lcom/osp/app/signin/SignUpCompleteView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    if-eqz v0, :cond_3

    invoke-virtual {v0, v11}, Landroid/view/View;->setVisibility(I)V

    :cond_3
    const v0, 0x7f0c012f

    invoke-virtual {p0, v0}, Lcom/osp/app/signin/SignUpCompleteView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    invoke-static {p0}, Lcom/osp/app/util/r;->m(Landroid/content/Context;)Z

    move-result v6

    if-nez v6, :cond_4

    invoke-static {p0}, Lcom/osp/app/util/r;->k(Landroid/content/Context;)Z

    move-result v6

    if-eqz v6, :cond_17

    :cond_4
    invoke-virtual {v0, v11}, Landroid/widget/ImageView;->setVisibility(I)V

    :cond_5
    :goto_1
    invoke-static {}, Lcom/osp/device/b;->b()Lcom/osp/device/b;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/osp/device/b;->i(Landroid/content/Context;)Z

    move-result v0

    if-nez v0, :cond_18

    invoke-static {p0}, Lcom/osp/app/util/r;->n(Landroid/content/Context;)Z

    move-result v0

    if-nez v0, :cond_18

    const v0, 0x7f0c00d7

    invoke-virtual {p0, v0}, Lcom/osp/app/signin/SignUpCompleteView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/osp/app/signin/SignUpCompleteView;->b:Landroid/widget/TextView;

    iget-object v0, p0, Lcom/osp/app/signin/SignUpCompleteView;->b:Landroid/widget/TextView;

    if-eqz v0, :cond_6

    iget-object v0, p0, Lcom/osp/app/signin/SignUpCompleteView;->b:Landroid/widget/TextView;

    invoke-static {p0}, Lcom/osp/app/util/an;->a(Lcom/osp/app/util/BaseActivity;)Lcom/osp/app/util/an;

    invoke-static {v10}, Lcom/osp/app/util/an;->a(I)I

    move-result v6

    invoke-virtual {v5, v6}, Landroid/content/res/Resources;->getColor(I)I

    move-result v6

    invoke-virtual {v0, v6}, Landroid/widget/TextView;->setTextColor(I)V

    :cond_6
    const v0, 0x7f0c0131

    invoke-virtual {p0, v0}, Lcom/osp/app/signin/SignUpCompleteView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/osp/app/signin/SignUpCompleteView;->c:Landroid/widget/TextView;

    iget-object v0, p0, Lcom/osp/app/signin/SignUpCompleteView;->c:Landroid/widget/TextView;

    if-eqz v0, :cond_7

    iget-object v0, p0, Lcom/osp/app/signin/SignUpCompleteView;->c:Landroid/widget/TextView;

    invoke-static {p0}, Lcom/osp/app/util/an;->a(Lcom/osp/app/util/BaseActivity;)Lcom/osp/app/util/an;

    invoke-static {v10}, Lcom/osp/app/util/an;->a(I)I

    move-result v6

    invoke-virtual {v5, v6}, Landroid/content/res/Resources;->getColor(I)I

    move-result v6

    invoke-virtual {v0, v6}, Landroid/widget/TextView;->setTextColor(I)V

    :cond_7
    const v0, 0x7f0c0130

    invoke-virtual {p0, v0}, Lcom/osp/app/signin/SignUpCompleteView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    if-eqz v0, :cond_8

    invoke-static {p0}, Lcom/osp/app/util/an;->a(Lcom/osp/app/util/BaseActivity;)Lcom/osp/app/util/an;

    invoke-static {v10}, Lcom/osp/app/util/an;->a(I)I

    move-result v6

    invoke-virtual {v5, v6}, Landroid/content/res/Resources;->getColor(I)I

    move-result v5

    invoke-virtual {v0, v5}, Landroid/widget/TextView;->setTextColor(I)V

    invoke-static {}, Lcom/osp/app/signin/SamsungService;->a()Z

    move-result v5

    if-nez v5, :cond_8

    invoke-static {p0}, Lcom/osp/app/util/an;->a(Lcom/osp/app/util/BaseActivity;)Lcom/osp/app/util/an;

    const v5, 0x7f08010b

    invoke-static {v5}, Lcom/osp/app/util/an;->b(I)I

    move-result v5

    invoke-virtual {p0}, Lcom/osp/app/signin/SignUpCompleteView;->getResources()Landroid/content/res/Resources;

    move-result-object v6

    invoke-virtual {v6, v5}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v5

    invoke-virtual {v0, v8, v5}, Landroid/widget/TextView;->setTextSize(IF)V

    :cond_8
    :goto_2
    const v0, 0x7f0c0065

    invoke-virtual {p0, v0}, Lcom/osp/app/signin/SignUpCompleteView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/osp/app/signin/BottomSoftkeyLinearLayout;

    iput-object v0, p0, Lcom/osp/app/signin/SignUpCompleteView;->f:Lcom/osp/app/signin/BottomSoftkeyLinearLayout;

    iget-object v0, p0, Lcom/osp/app/signin/SignUpCompleteView;->f:Lcom/osp/app/signin/BottomSoftkeyLinearLayout;

    if-eqz v0, :cond_9

    iget-object v0, p0, Lcom/osp/app/signin/SignUpCompleteView;->f:Lcom/osp/app/signin/BottomSoftkeyLinearLayout;

    invoke-virtual {p0}, Lcom/osp/app/signin/SignUpCompleteView;->h()Z

    move-result v5

    invoke-static {v5}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v5

    invoke-virtual {v0, p0, v5}, Lcom/osp/app/signin/BottomSoftkeyLinearLayout;->a(Landroid/content/Context;Ljava/lang/Boolean;)V

    iget-object v0, p0, Lcom/osp/app/signin/SignUpCompleteView;->f:Lcom/osp/app/signin/BottomSoftkeyLinearLayout;

    invoke-static {p0}, Lcom/osp/app/util/an;->a(Lcom/osp/app/util/BaseActivity;)Lcom/osp/app/util/an;

    const v5, 0x7f02008f

    invoke-static {v5}, Lcom/osp/app/util/an;->a(I)I

    move-result v5

    invoke-virtual {v0, v5}, Lcom/osp/app/signin/BottomSoftkeyLinearLayout;->a(I)V

    invoke-static {}, Lcom/osp/app/signin/SamsungService;->a()Z

    move-result v0

    if-eqz v0, :cond_1b

    iget-object v0, p0, Lcom/osp/app/signin/SignUpCompleteView;->f:Lcom/osp/app/signin/BottomSoftkeyLinearLayout;

    const-string v5, "next_button"

    invoke-virtual {v0, v5}, Lcom/osp/app/signin/BottomSoftkeyLinearLayout;->a(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/osp/app/signin/SignUpCompleteView;->f:Lcom/osp/app/signin/BottomSoftkeyLinearLayout;

    const v5, 0x7f09006b

    invoke-virtual {p0, v5}, Lcom/osp/app/signin/SignUpCompleteView;->getString(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v0, v5}, Lcom/osp/app/signin/BottomSoftkeyLinearLayout;->f(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/osp/app/signin/SignUpCompleteView;->f:Lcom/osp/app/signin/BottomSoftkeyLinearLayout;

    const v5, 0x7f09006b

    invoke-virtual {p0, v5}, Lcom/osp/app/signin/SignUpCompleteView;->getString(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v0, v5}, Lcom/osp/app/signin/BottomSoftkeyLinearLayout;->e(Ljava/lang/String;)V

    :goto_3
    iget-object v0, p0, Lcom/osp/app/signin/SignUpCompleteView;->f:Lcom/osp/app/signin/BottomSoftkeyLinearLayout;

    iget-object v5, p0, Lcom/osp/app/signin/SignUpCompleteView;->B:Landroid/view/View$OnClickListener;

    invoke-virtual {v0, v5}, Lcom/osp/app/signin/BottomSoftkeyLinearLayout;->b(Landroid/view/View$OnClickListener;)V

    invoke-static {}, Lcom/osp/app/signin/SamsungService;->a()Z

    move-result v0

    if-nez v0, :cond_9

    invoke-static {}, Lcom/osp/device/b;->b()Lcom/osp/device/b;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/osp/device/b;->i(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_9

    const-string v0, "finish"

    invoke-virtual {p0, v0}, Lcom/osp/app/signin/SignUpCompleteView;->e(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/osp/app/signin/SignUpCompleteView;->f:Lcom/osp/app/signin/BottomSoftkeyLinearLayout;

    if-eqz v0, :cond_9

    iget-object v0, p0, Lcom/osp/app/signin/SignUpCompleteView;->f:Lcom/osp/app/signin/BottomSoftkeyLinearLayout;

    invoke-virtual {v0, v11}, Lcom/osp/app/signin/BottomSoftkeyLinearLayout;->setVisibility(I)V

    .line 143
    :cond_9
    if-eqz v4, :cond_1f

    .line 146
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v5, "+"

    invoke-direct {v0, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 149
    :goto_4
    iget-object v1, p0, Lcom/osp/app/signin/SignUpCompleteView;->b:Landroid/widget/TextView;

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 154
    if-eqz v2, :cond_a

    .line 156
    const v0, 0x7f0c0132

    invoke-virtual {p0, v0}, Lcom/osp/app/signin/SignUpCompleteView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 157
    if-eqz v0, :cond_a

    .line 159
    if-eqz v3, :cond_a

    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v1

    if-lez v1, :cond_a

    .line 161
    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 162
    invoke-virtual {v0, v8}, Landroid/widget/TextView;->setVisibility(I)V

    .line 167
    :cond_a
    iget-object v0, p0, Lcom/osp/app/signin/SignUpCompleteView;->c:Landroid/widget/TextView;

    if-eqz v0, :cond_b

    .line 169
    if-eqz v4, :cond_1c

    .line 171
    iget-object v0, p0, Lcom/osp/app/signin/SignUpCompleteView;->c:Landroid/widget/TextView;

    invoke-virtual {p0}, Lcom/osp/app/signin/SignUpCompleteView;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f090117

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 184
    :cond_b
    :goto_5
    invoke-virtual {p0}, Lcom/osp/app/signin/SignUpCompleteView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v0

    iget v0, v0, Landroid/content/res/Configuration;->orientation:I

    invoke-direct {p0, v0}, Lcom/osp/app/signin/SignUpCompleteView;->a(I)V

    .line 185
    invoke-static {p0}, Lcom/osp/app/util/r;->e(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_e

    const v0, 0x7f0c017a

    invoke-virtual {p0, v0}, Lcom/osp/app/signin/SignUpCompleteView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    invoke-static {p0}, Lcom/osp/app/util/r;->m(Landroid/content/Context;)Z

    move-result v1

    if-nez v1, :cond_c

    invoke-static {p0}, Lcom/osp/app/util/r;->k(Landroid/content/Context;)Z

    move-result v1

    if-eqz v1, :cond_1e

    :cond_c
    invoke-virtual {v0, v11}, Landroid/widget/LinearLayout;->setVisibility(I)V

    :cond_d
    :goto_6
    const v0, 0x7f0c00d7

    invoke-virtual {p0, v0}, Lcom/osp/app/signin/SignUpCompleteView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iget-object v1, p0, Lcom/osp/app/signin/SignUpCompleteView;->a:Landroid/content/Intent;

    const-string v2, "email_id"

    invoke-virtual {v1, v2}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    invoke-virtual {v0, v8}, Landroid/widget/TextView;->setVisibility(I)V

    :cond_e
    const v0, 0x7f0c00c9

    invoke-virtual {p0, v0}, Lcom/osp/app/signin/SignUpCompleteView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-static {p0}, Lcom/osp/app/util/r;->i(Landroid/content/Context;)Z

    move-result v1

    if-eqz v1, :cond_f

    invoke-static {}, Lcom/osp/app/signin/SamsungService;->a()Z

    move-result v1

    if-eqz v1, :cond_f

    invoke-static {p0}, Lcom/msc/sa/c/d;->c(Landroid/app/Activity;)V

    invoke-virtual {p0}, Lcom/osp/app/signin/SignUpCompleteView;->o()I

    move-result v1

    invoke-static {v0, v8, v1, v8, v8}, Lcom/msc/sa/c/d;->a(Landroid/view/View;IIII)V

    :cond_f
    invoke-static {}, Lcom/osp/app/signin/SamsungService;->a()Z

    move-result v1

    if-eqz v1, :cond_10

    invoke-static {p0}, Lcom/osp/app/util/r;->k(Landroid/content/Context;)Z

    move-result v1

    if-nez v1, :cond_10

    invoke-static {p0}, Lcom/osp/app/util/r;->m(Landroid/content/Context;)Z

    move-result v1

    if-nez v1, :cond_10

    invoke-virtual {p0, v0}, Lcom/osp/app/signin/SignUpCompleteView;->a(Landroid/view/View;)V

    .line 186
    :cond_10
    return-void

    .line 118
    :cond_11
    invoke-virtual {p0}, Lcom/osp/app/signin/SignUpCompleteView;->h()Z

    move-result v0

    if-eqz v0, :cond_12

    .line 120
    invoke-virtual {p0}, Lcom/osp/app/signin/SignUpCompleteView;->i()V

    goto/16 :goto_0

    .line 123
    :cond_12
    invoke-virtual {p0}, Lcom/osp/app/signin/SignUpCompleteView;->j()V

    goto/16 :goto_0

    .line 128
    :cond_13
    invoke-static {}, Lcom/osp/app/signin/SamsungService;->a()Z

    move-result v0

    if-nez v0, :cond_16

    .line 130
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    if-lt v0, v6, :cond_1

    invoke-static {}, Lcom/osp/app/signin/SamsungService;->a()Z

    move-result v0

    if-nez v0, :cond_1

    invoke-virtual {p0}, Landroid/app/Activity;->getActionBar()Landroid/app/ActionBar;

    move-result-object v0

    if-eqz v0, :cond_1

    invoke-static {p0}, Lcom/osp/app/util/r;->m(Landroid/content/Context;)Z

    move-result v5

    if-nez v5, :cond_14

    invoke-static {p0}, Lcom/osp/app/util/r;->k(Landroid/content/Context;)Z

    move-result v5

    if-eqz v5, :cond_15

    :cond_14
    invoke-virtual {v0, v8}, Landroid/app/ActionBar;->setHomeAsUpIndicator(I)V

    invoke-virtual {v0, v8}, Landroid/app/ActionBar;->setDisplayShowHomeEnabled(Z)V

    :cond_15
    invoke-virtual {v0, v9}, Landroid/app/ActionBar;->setHomeButtonEnabled(Z)V

    invoke-virtual {v0, v9}, Landroid/app/ActionBar;->setDisplayHomeAsUpEnabled(Z)V

    goto/16 :goto_0

    .line 133
    :cond_16
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    if-ge v0, v6, :cond_1

    .line 135
    invoke-virtual {p0, v9}, Lcom/osp/app/signin/SignUpCompleteView;->requestWindowFeature(I)Z

    goto/16 :goto_0

    .line 141
    :cond_17
    invoke-virtual {v0, v8}, Landroid/widget/ImageView;->setVisibility(I)V

    invoke-static {p0}, Lcom/osp/app/util/an;->a(Lcom/osp/app/util/BaseActivity;)Lcom/osp/app/util/an;

    const v6, 0x7f02007a

    invoke-static {v6}, Lcom/osp/app/util/an;->a(I)I

    move-result v6

    invoke-virtual {v0, v6}, Landroid/widget/ImageView;->setBackgroundResource(I)V

    goto/16 :goto_1

    :cond_18
    const v0, 0x7f0c0130

    invoke-virtual {p0, v0}, Lcom/osp/app/signin/SignUpCompleteView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    if-eqz v0, :cond_19

    const/4 v6, 0x3

    invoke-static {v0, v6}, Lcom/msc/sa/c/d;->a(Landroid/view/View;I)V

    const/4 v6, 0x0

    invoke-virtual {v0, v6, v9}, Landroid/widget/TextView;->setTypeface(Landroid/graphics/Typeface;I)V

    invoke-static {p0}, Lcom/osp/app/util/an;->a(Lcom/osp/app/util/BaseActivity;)Lcom/osp/app/util/an;

    invoke-static {v10}, Lcom/osp/app/util/an;->a(I)I

    move-result v6

    invoke-virtual {v5, v6}, Landroid/content/res/Resources;->getColor(I)I

    move-result v6

    invoke-virtual {v0, v6}, Landroid/widget/TextView;->setTextColor(I)V

    :cond_19
    iget-object v0, p0, Lcom/osp/app/signin/SignUpCompleteView;->b:Landroid/widget/TextView;

    if-nez v0, :cond_1a

    const v0, 0x7f0c00d7

    invoke-virtual {p0, v0}, Lcom/osp/app/signin/SignUpCompleteView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/osp/app/signin/SignUpCompleteView;->b:Landroid/widget/TextView;

    iget-object v0, p0, Lcom/osp/app/signin/SignUpCompleteView;->b:Landroid/widget/TextView;

    invoke-static {p0}, Lcom/osp/app/util/an;->a(Lcom/osp/app/util/BaseActivity;)Lcom/osp/app/util/an;

    invoke-static {v10}, Lcom/osp/app/util/an;->a(I)I

    move-result v6

    invoke-virtual {v5, v6}, Landroid/content/res/Resources;->getColor(I)I

    move-result v6

    invoke-virtual {v0, v6}, Landroid/widget/TextView;->setTextColor(I)V

    iget-object v0, p0, Lcom/osp/app/signin/SignUpCompleteView;->b:Landroid/widget/TextView;

    const/4 v6, 0x0

    invoke-virtual {v0, v6, v9}, Landroid/widget/TextView;->setTypeface(Landroid/graphics/Typeface;I)V

    :cond_1a
    iget-object v0, p0, Lcom/osp/app/signin/SignUpCompleteView;->c:Landroid/widget/TextView;

    if-nez v0, :cond_8

    const v0, 0x7f0c0131

    invoke-virtual {p0, v0}, Lcom/osp/app/signin/SignUpCompleteView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/osp/app/signin/SignUpCompleteView;->c:Landroid/widget/TextView;

    iget-object v0, p0, Lcom/osp/app/signin/SignUpCompleteView;->c:Landroid/widget/TextView;

    invoke-static {p0}, Lcom/osp/app/util/an;->a(Lcom/osp/app/util/BaseActivity;)Lcom/osp/app/util/an;

    invoke-static {v10}, Lcom/osp/app/util/an;->a(I)I

    move-result v6

    invoke-virtual {v5, v6}, Landroid/content/res/Resources;->getColor(I)I

    move-result v5

    invoke-virtual {v0, v5}, Landroid/widget/TextView;->setTextColor(I)V

    goto/16 :goto_2

    :cond_1b
    iget-object v0, p0, Lcom/osp/app/signin/SignUpCompleteView;->f:Lcom/osp/app/signin/BottomSoftkeyLinearLayout;

    const-string v5, "one_button"

    invoke-virtual {v0, v5}, Lcom/osp/app/signin/BottomSoftkeyLinearLayout;->a(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/osp/app/signin/SignUpCompleteView;->f:Lcom/osp/app/signin/BottomSoftkeyLinearLayout;

    const v5, 0x7f090043

    invoke-virtual {p0, v5}, Lcom/osp/app/signin/SignUpCompleteView;->getString(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v0, v5}, Lcom/osp/app/signin/BottomSoftkeyLinearLayout;->c(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/osp/app/signin/SignUpCompleteView;->f:Lcom/osp/app/signin/BottomSoftkeyLinearLayout;

    const v5, 0x7f090043

    invoke-virtual {p0, v5}, Lcom/osp/app/signin/SignUpCompleteView;->getString(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v0, v5}, Lcom/osp/app/signin/BottomSoftkeyLinearLayout;->e(Ljava/lang/String;)V

    goto/16 :goto_3

    .line 172
    :cond_1c
    invoke-static {}, Lcom/osp/app/signin/SamsungService;->a()Z

    move-result v0

    if-eqz v0, :cond_b

    .line 174
    invoke-static {p0}, Lcom/osp/app/util/ad;->k(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_1d

    .line 176
    iget-object v0, p0, Lcom/osp/app/signin/SignUpCompleteView;->c:Landroid/widget/TextView;

    invoke-virtual {p0}, Lcom/osp/app/signin/SignUpCompleteView;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0901b8

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto/16 :goto_5

    .line 179
    :cond_1d
    iget-object v0, p0, Lcom/osp/app/signin/SignUpCompleteView;->c:Landroid/widget/TextView;

    invoke-virtual {p0}, Lcom/osp/app/signin/SignUpCompleteView;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f09011d

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto/16 :goto_5

    .line 185
    :cond_1e
    invoke-virtual {v0, v8}, Landroid/widget/LinearLayout;->setVisibility(I)V

    const v0, 0x7f0c012f

    invoke-virtual {p0, v0}, Lcom/osp/app/signin/SignUpCompleteView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    if-eqz v0, :cond_d

    invoke-static {p0}, Lcom/osp/app/util/an;->a(Lcom/osp/app/util/BaseActivity;)Lcom/osp/app/util/an;

    const v1, 0x7f02007a

    invoke-static {v1}, Lcom/osp/app/util/an;->a(I)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setBackgroundResource(I)V

    goto/16 :goto_6

    :cond_1f
    move-object v0, v1

    goto/16 :goto_4
.end method

.method protected onDestroy()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 633
    invoke-super {p0}, Lcom/osp/app/util/BaseActivity;->onDestroy()V

    .line 635
    invoke-virtual {p0}, Lcom/osp/app/signin/SignUpCompleteView;->isFinishing()Z

    move-result v0

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/osp/app/signin/SignUpCompleteView;->y:Landroid/app/ProgressDialog;

    if-eqz v0, :cond_1

    .line 639
    :try_start_0
    iget-object v0, p0, Lcom/osp/app/signin/SignUpCompleteView;->y:Landroid/app/ProgressDialog;

    invoke-virtual {v0}, Landroid/app/ProgressDialog;->isShowing()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 641
    iget-object v0, p0, Lcom/osp/app/signin/SignUpCompleteView;->y:Landroid/app/ProgressDialog;

    invoke-virtual {v0}, Landroid/app/ProgressDialog;->dismiss()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 648
    :cond_0
    :goto_0
    iput-object v1, p0, Lcom/osp/app/signin/SignUpCompleteView;->y:Landroid/app/ProgressDialog;

    .line 651
    :cond_1
    return-void

    .line 643
    :catch_0
    move-exception v0

    :try_start_1
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 648
    :catchall_0
    move-exception v0

    iput-object v1, p0, Lcom/osp/app/signin/SignUpCompleteView;->y:Landroid/app/ProgressDialog;

    throw v0
.end method

.method public onOptionsItemSelected(Landroid/view/MenuItem;)Z
    .locals 2

    .prologue
    .line 231
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0xb

    if-lt v0, v1, :cond_0

    .line 233
    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    .line 245
    const/4 v0, 0x0

    .line 248
    :goto_0
    return v0

    .line 236
    :sswitch_0
    invoke-virtual {p0}, Lcom/osp/app/signin/SignUpCompleteView;->onBackPressed()V

    .line 248
    :cond_0
    :goto_1
    const/4 v0, 0x1

    goto :goto_0

    .line 241
    :sswitch_1
    invoke-direct {p0}, Lcom/osp/app/signin/SignUpCompleteView;->c()V

    goto :goto_1

    .line 233
    :sswitch_data_0
    .sparse-switch
        0x102002c -> :sswitch_0
        0x7f0c0192 -> :sswitch_1
        0x7f0c0196 -> :sswitch_1
    .end sparse-switch
.end method

.method protected onPause()V
    .locals 0

    .prologue
    .line 628
    invoke-super {p0}, Lcom/osp/app/util/BaseActivity;->onPause()V

    .line 629
    return-void
.end method

.method protected onResume()V
    .locals 2

    .prologue
    .line 191
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/osp/app/signin/SignUpCompleteView;->d:J

    .line 192
    invoke-super {p0}, Lcom/osp/app/util/BaseActivity;->onResume()V

    .line 193
    return-void
.end method
