.class final Lcom/osp/app/signin/fl;
.super Lcom/msc/c/b;
.source "EmailValidationView.java"


# instance fields
.field final synthetic c:Lcom/osp/app/signin/EmailValidationView;

.field private d:J

.field private e:Z


# direct methods
.method public constructor <init>(Lcom/osp/app/signin/EmailValidationView;Landroid/content/Context;)V
    .locals 0

    .prologue
    .line 810
    iput-object p1, p0, Lcom/osp/app/signin/fl;->c:Lcom/osp/app/signin/EmailValidationView;

    .line 811
    invoke-direct {p0, p2}, Lcom/msc/c/b;-><init>(Landroid/content/Context;)V

    .line 813
    return-void
.end method


# virtual methods
.method protected final varargs a()Ljava/lang/Boolean;
    .locals 3

    .prologue
    .line 828
    invoke-static {}, Lcom/msc/c/g;->a()Lcom/msc/c/g;

    .line 829
    iget-object v0, p0, Lcom/osp/app/signin/fl;->c:Lcom/osp/app/signin/EmailValidationView;

    iget-object v1, p0, Lcom/osp/app/signin/fl;->c:Lcom/osp/app/signin/EmailValidationView;

    invoke-static {v1}, Lcom/osp/app/signin/EmailValidationView;->a(Lcom/osp/app/signin/EmailValidationView;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1, p0}, Lcom/msc/c/g;->g(Landroid/content/Context;Ljava/lang/String;Lcom/msc/b/h;)J

    move-result-wide v0

    iput-wide v0, p0, Lcom/osp/app/signin/fl;->d:J

    .line 830
    iget-wide v0, p0, Lcom/osp/app/signin/fl;->d:J

    invoke-virtual {p0, v0, v1}, Lcom/osp/app/signin/fl;->a(J)V

    .line 831
    iget-wide v0, p0, Lcom/osp/app/signin/fl;->d:J

    const-string v2, "from_json"

    invoke-virtual {p0, v0, v1, v2}, Lcom/osp/app/signin/fl;->a(JLjava/lang/String;)V

    .line 833
    iget-wide v0, p0, Lcom/osp/app/signin/fl;->d:J

    sget-object v2, Lcom/msc/b/g;->c:Lcom/msc/b/g;

    invoke-static {v0, v1, v2}, Lcom/msc/c/g;->a(JLcom/msc/b/g;)V

    .line 835
    const/4 v0, 0x0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    return-object v0
.end method

.method public final declared-synchronized a(Lcom/msc/b/c;)V
    .locals 4

    .prologue
    .line 856
    monitor-enter p0

    :try_start_0
    invoke-super {p0, p1}, Lcom/msc/c/b;->a(Lcom/msc/b/c;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 858
    if-nez p1, :cond_1

    .line 869
    :cond_0
    :goto_0
    monitor-exit p0

    return-void

    .line 863
    :cond_1
    :try_start_1
    invoke-virtual {p1}, Lcom/msc/b/c;->a()J

    move-result-wide v0

    .line 865
    iget-wide v2, p0, Lcom/osp/app/signin/fl;->d:J

    cmp-long v0, v0, v2

    if-nez v0, :cond_0

    .line 867
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/osp/app/signin/fl;->e:Z
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 856
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method protected final a(Ljava/lang/Boolean;)V
    .locals 3

    .prologue
    .line 840
    invoke-super {p0, p1}, Lcom/msc/c/b;->a(Ljava/lang/Boolean;)V

    .line 842
    iget-boolean v0, p0, Lcom/osp/app/signin/fl;->e:Z

    if-eqz v0, :cond_0

    .line 846
    :try_start_0
    iget-object v0, p0, Lcom/osp/app/signin/fl;->c:Lcom/osp/app/signin/EmailValidationView;

    const/16 v1, 0x66

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Lcom/osp/app/signin/EmailValidationView;->a(ILandroid/content/DialogInterface$OnDismissListener;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 852
    :cond_0
    :goto_0
    return-void

    .line 847
    :catch_0
    move-exception v0

    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_0
.end method

.method protected final d()V
    .locals 1

    .prologue
    .line 817
    invoke-super {p0}, Lcom/msc/c/b;->d()V

    .line 818
    invoke-virtual {p0}, Lcom/osp/app/signin/fl;->f()V

    .line 819
    iget-object v0, p0, Lcom/osp/app/signin/fl;->c:Lcom/osp/app/signin/EmailValidationView;

    invoke-static {v0}, Lcom/osp/app/signin/EmailValidationView;->e(Lcom/osp/app/signin/EmailValidationView;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 821
    iget-object v0, p0, Lcom/osp/app/signin/fl;->c:Lcom/osp/app/signin/EmailValidationView;

    invoke-static {v0}, Lcom/osp/app/signin/EmailValidationView;->f(Lcom/osp/app/signin/EmailValidationView;)V

    .line 823
    :cond_0
    return-void
.end method

.method protected final synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 805
    invoke-virtual {p0}, Lcom/osp/app/signin/fl;->a()Ljava/lang/Boolean;

    move-result-object v0

    return-object v0
.end method

.method protected final synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 805
    check-cast p1, Ljava/lang/Boolean;

    invoke-virtual {p0, p1}, Lcom/osp/app/signin/fl;->a(Ljava/lang/Boolean;)V

    return-void
.end method
