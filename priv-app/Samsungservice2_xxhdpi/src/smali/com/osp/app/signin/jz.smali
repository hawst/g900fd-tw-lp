.class public final Lcom/osp/app/signin/jz;
.super Ljava/lang/Object;
.source "SignIn_Ver01.java"


# instance fields
.field a:Z

.field b:Z

.field c:Z

.field d:Z

.field e:Z

.field private final f:Lcom/osp/app/util/n;

.field private final g:Landroid/content/Context;

.field private h:Ljava/lang/String;

.field private i:Lcom/osp/app/signin/kb;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 107
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 70
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/osp/app/signin/jz;->h:Ljava/lang/String;

    .line 80
    iput-boolean v1, p0, Lcom/osp/app/signin/jz;->a:Z

    .line 85
    iput-boolean v1, p0, Lcom/osp/app/signin/jz;->b:Z

    .line 90
    iput-boolean v1, p0, Lcom/osp/app/signin/jz;->c:Z

    .line 95
    iput-boolean v1, p0, Lcom/osp/app/signin/jz;->d:Z

    .line 100
    iput-boolean v1, p0, Lcom/osp/app/signin/jz;->e:Z

    .line 108
    iput-object p1, p0, Lcom/osp/app/signin/jz;->g:Landroid/content/Context;

    .line 109
    invoke-static {}, Lcom/osp/app/util/n;->a()Lcom/osp/app/util/n;

    move-result-object v0

    iput-object v0, p0, Lcom/osp/app/signin/jz;->f:Lcom/osp/app/util/n;

    .line 110
    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/String;Ljava/lang/String;)Lcom/osp/app/signin/kb;
    .locals 9

    .prologue
    const/4 v8, 0x1

    .line 333
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v0, "SIV01"

    const-string v1, "autosign"

    const-string v2, "START"

    invoke-static {v0, v1, v2}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 335
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/osp/app/signin/jz;->h:Ljava/lang/String;

    .line 336
    sget-object v0, Lcom/osp/app/signin/kb;->i:Lcom/osp/app/signin/kb;

    iput-object v0, p0, Lcom/osp/app/signin/jz;->i:Lcom/osp/app/signin/kb;

    .line 343
    :try_start_0
    new-instance v1, Lcom/osp/security/identity/d;

    iget-object v0, p0, Lcom/osp/app/signin/jz;->g:Landroid/content/Context;

    invoke-direct {v1, v0}, Lcom/osp/security/identity/d;-><init>(Landroid/content/Context;)V

    .line 344
    new-instance v2, Lcom/osp/security/credential/a;

    iget-object v0, p0, Lcom/osp/app/signin/jz;->g:Landroid/content/Context;

    invoke-direct {v2, v0}, Lcom/osp/security/credential/a;-><init>(Landroid/content/Context;)V

    .line 345
    new-instance v3, Lcom/osp/social/member/d;

    iget-object v0, p0, Lcom/osp/app/signin/jz;->g:Landroid/content/Context;

    invoke-direct {v3, v0}, Lcom/osp/social/member/d;-><init>(Landroid/content/Context;)V

    .line 346
    new-instance v4, Lcom/osp/device/d;

    iget-object v0, p0, Lcom/osp/app/signin/jz;->g:Landroid/content/Context;

    invoke-direct {v4, v0}, Lcom/osp/device/d;-><init>(Landroid/content/Context;)V

    .line 347
    new-instance v5, Lcom/osp/common/property/a;

    iget-object v0, p0, Lcom/osp/app/signin/jz;->g:Landroid/content/Context;

    invoke-direct {v5, v0}, Lcom/osp/common/property/a;-><init>(Landroid/content/Context;)V

    .line 349
    :goto_0
    iget-object v0, p0, Lcom/osp/app/signin/jz;->i:Lcom/osp/app/signin/kb;

    sget-object v6, Lcom/osp/app/signin/kb;->j:Lcom/osp/app/signin/kb;

    if-eq v0, v6, :cond_4

    iget-object v0, p0, Lcom/osp/app/signin/jz;->i:Lcom/osp/app/signin/kb;

    sget-object v6, Lcom/osp/app/signin/kb;->a:Lcom/osp/app/signin/kb;

    if-eq v0, v6, :cond_4

    .line 351
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v0, "SIV01"

    new-instance v6, Ljava/lang/StringBuilder;

    const-string v7, "autosign mSignInState=["

    invoke-direct {v6, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v7, p0, Lcom/osp/app/signin/jz;->i:Lcom/osp/app/signin/kb;

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, "]"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v0, v6}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 352
    iget-boolean v0, p0, Lcom/osp/app/signin/jz;->e:Z

    if-eqz v0, :cond_0

    .line 354
    sget-object v0, Lcom/osp/app/signin/kb;->d:Lcom/osp/app/signin/kb;

    iput-object v0, p0, Lcom/osp/app/signin/jz;->i:Lcom/osp/app/signin/kb;

    .line 357
    :cond_0
    sget-object v0, Lcom/osp/app/signin/ka;->a:[I

    iget-object v6, p0, Lcom/osp/app/signin/jz;->i:Lcom/osp/app/signin/kb;

    invoke-virtual {v6}, Lcom/osp/app/signin/kb;->ordinal()I

    move-result v6

    aget v0, v0, v6

    packed-switch v0, :pswitch_data_0

    goto :goto_0

    .line 360
    :pswitch_0
    const-string v0, "SignOut"

    invoke-virtual {v5, v0}, Lcom/osp/common/property/a;->a(Ljava/lang/String;)Z

    move-result v0

    if-ne v0, v8, :cond_1

    .line 362
    invoke-virtual {v1}, Lcom/osp/security/identity/d;->k()V

    .line 363
    invoke-virtual {v2}, Lcom/osp/security/credential/a;->a()V

    .line 364
    invoke-virtual {v3}, Lcom/osp/social/member/d;->a()V

    .line 365
    const-string v0, "SignOut"

    invoke-virtual {v5, v0}, Lcom/osp/common/property/a;->c(Ljava/lang/String;)V

    .line 372
    :cond_1
    new-instance v0, Lcom/osp/security/identity/k;

    invoke-direct {v0}, Lcom/osp/security/identity/k;-><init>()V

    .line 373
    invoke-virtual {v0, p1}, Lcom/osp/security/identity/k;->b(Ljava/lang/String;)V

    .line 374
    invoke-virtual {v0, p2}, Lcom/osp/security/identity/k;->d(Ljava/lang/String;)V

    .line 376
    invoke-static {}, Lcom/osp/device/b;->b()Lcom/osp/device/b;

    move-result-object v6

    invoke-virtual {v6}, Lcom/osp/device/b;->a()Z

    move-result v6

    if-eqz v6, :cond_2

    invoke-static {p1}, Landroid/telephony/PhoneNumberUtils;->isGlobalPhoneNumber(Ljava/lang/String;)Z

    move-result v6

    if-eqz v6, :cond_2

    .line 378
    invoke-static {}, Lcom/osp/app/util/k;->a()Lcom/osp/app/util/k;

    move-result-object v6

    iget-object v7, p0, Lcom/osp/app/signin/jz;->g:Landroid/content/Context;

    invoke-virtual {v6, v7}, Lcom/osp/app/util/k;->a(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v6

    .line 379
    if-eqz v6, :cond_2

    .line 381
    invoke-virtual {v0, v6}, Lcom/osp/security/identity/k;->c(Ljava/lang/String;)V

    .line 385
    :cond_2
    invoke-virtual {v1, v0}, Lcom/osp/security/identity/d;->a(Lcom/osp/security/identity/k;)V

    .line 397
    const-string v0, "14eev3f64b"

    invoke-virtual {v2, v0}, Lcom/osp/security/credential/a;->d(Ljava/lang/String;)Z

    move-result v0

    .line 398
    if-ne v0, v8, :cond_3

    .line 400
    sget-object v0, Lcom/osp/app/signin/kb;->b:Lcom/osp/app/signin/kb;

    iput-object v0, p0, Lcom/osp/app/signin/jz;->i:Lcom/osp/app/signin/kb;

    .line 401
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v0, "SIV01"

    new-instance v6, Ljava/lang/StringBuilder;

    const-string v7, "autosign mSignInState=["

    invoke-direct {v6, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v7, p0, Lcom/osp/app/signin/jz;->i:Lcom/osp/app/signin/kb;

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, "] start 1"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v0, v6}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catch Lcom/osp/security/identity/IdentityException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Lcom/osp/device/DeviceException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Lcom/osp/social/member/MemberServiceException; {:try_start_0 .. :try_end_0} :catch_2
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_4

    goto/16 :goto_0

    .line 486
    :catch_0
    move-exception v0

    .line 488
    iget-object v1, p0, Lcom/osp/app/signin/jz;->f:Lcom/osp/app/util/n;

    iget-object v2, p0, Lcom/osp/app/signin/jz;->g:Landroid/content/Context;

    invoke-virtual {v1, v2, v0}, Lcom/osp/app/util/n;->a(Landroid/content/Context;Lcom/osp/common/abstracts/AbstractOSPException;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/osp/app/signin/jz;->h:Ljava/lang/String;

    .line 489
    sget-object v1, Lcom/osp/app/signin/kb;->a:Lcom/osp/app/signin/kb;

    iput-object v1, p0, Lcom/osp/app/signin/jz;->i:Lcom/osp/app/signin/kb;

    .line 490
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v1, "SIV01"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "autosign IdentityException mSignInState=["

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v3, p0, Lcom/osp/app/signin/jz;->i:Lcom/osp/app/signin/kb;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "] Canceled"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 491
    new-instance v1, Lcom/osp/security/identity/IdentityException;

    invoke-virtual {v0}, Lcom/osp/security/identity/IdentityException;->getMessage()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0}, Lcom/osp/security/identity/IdentityException;->a()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v1, v2, v0, v3}, Lcom/osp/security/identity/IdentityException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;Ljava/lang/String;)V

    throw v1

    .line 404
    :cond_3
    :try_start_1
    sget-object v0, Lcom/osp/app/signin/kb;->g:Lcom/osp/app/signin/kb;

    iput-object v0, p0, Lcom/osp/app/signin/jz;->i:Lcom/osp/app/signin/kb;

    .line 405
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v0, "SIV01"

    new-instance v6, Ljava/lang/StringBuilder;

    const-string v7, "autosign mSignInState=["

    invoke-direct {v6, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v7, p0, Lcom/osp/app/signin/jz;->i:Lcom/osp/app/signin/kb;

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, "] start 2"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v0, v6}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_1
    .catch Lcom/osp/security/identity/IdentityException; {:try_start_1 .. :try_end_1} :catch_0
    .catch Lcom/osp/device/DeviceException; {:try_start_1 .. :try_end_1} :catch_1
    .catch Lcom/osp/social/member/MemberServiceException; {:try_start_1 .. :try_end_1} :catch_2
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_4

    goto/16 :goto_0

    .line 492
    :catch_1
    move-exception v0

    .line 494
    iget-object v1, p0, Lcom/osp/app/signin/jz;->f:Lcom/osp/app/util/n;

    iget-object v2, p0, Lcom/osp/app/signin/jz;->g:Landroid/content/Context;

    invoke-virtual {v1, v2, v0}, Lcom/osp/app/util/n;->a(Landroid/content/Context;Lcom/osp/common/abstracts/AbstractOSPException;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/osp/app/signin/jz;->h:Ljava/lang/String;

    .line 495
    sget-object v1, Lcom/osp/app/signin/kb;->a:Lcom/osp/app/signin/kb;

    iput-object v1, p0, Lcom/osp/app/signin/jz;->i:Lcom/osp/app/signin/kb;

    .line 496
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v1, "SIV01"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "autosign DeviceException mSignInState=["

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v3, p0, Lcom/osp/app/signin/jz;->i:Lcom/osp/app/signin/kb;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "] Canceled"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 497
    invoke-virtual {v0}, Lcom/osp/device/DeviceException;->printStackTrace()V

    .line 498
    new-instance v1, Lcom/osp/device/DeviceException;

    invoke-virtual {v0}, Lcom/osp/device/DeviceException;->getMessage()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0}, Lcom/osp/device/DeviceException;->a()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v1, v2, v0, v3}, Lcom/osp/device/DeviceException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;Ljava/lang/String;)V

    throw v1

    .line 410
    :pswitch_1
    :try_start_2
    invoke-virtual {v1}, Lcom/osp/security/identity/d;->g()Z

    move-result v0

    if-nez v0, :cond_5

    .line 412
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v0, "SIV01"

    const-string v6, "requestDeviceRegistration"

    const-string v7, "START"

    invoke-static {v0, v6, v7}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    :try_end_2
    .catch Lcom/osp/security/identity/IdentityException; {:try_start_2 .. :try_end_2} :catch_0
    .catch Lcom/osp/device/DeviceException; {:try_start_2 .. :try_end_2} :catch_1
    .catch Lcom/osp/social/member/MemberServiceException; {:try_start_2 .. :try_end_2} :catch_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_4

    :try_start_3
    const-string v0, "14eev3f64b"

    const-string v6, "109E2830E09DB340924B8ABE0D6290C3"

    invoke-virtual {v4, v0, v6}, Lcom/osp/device/d;->a(Ljava/lang/String;Ljava/lang/String;)V

    sget-object v0, Lcom/osp/app/signin/kb;->j:Lcom/osp/app/signin/kb;

    iput-object v0, p0, Lcom/osp/app/signin/jz;->i:Lcom/osp/app/signin/kb;

    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v0, "SIV01"

    new-instance v6, Ljava/lang/StringBuilder;

    const-string v7, "requestDeviceRegistration mSignInState=["

    invoke-direct {v6, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v7, p0, Lcom/osp/app/signin/jz;->i:Lcom/osp/app/signin/kb;

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, "]"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v0, v6}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_3
    .catch Lcom/osp/device/DeviceException; {:try_start_3 .. :try_end_3} :catch_3
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_9
    .catch Lcom/osp/security/identity/IdentityException; {:try_start_3 .. :try_end_3} :catch_0
    .catch Lcom/osp/social/member/MemberServiceException; {:try_start_3 .. :try_end_3} :catch_2

    :goto_1
    :try_start_4
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v0, "SIV01"

    const-string v6, "requestDeviceRegistration"

    const-string v7, "END"

    invoke-static {v0, v6, v7}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    :try_end_4
    .catch Lcom/osp/security/identity/IdentityException; {:try_start_4 .. :try_end_4} :catch_0
    .catch Lcom/osp/device/DeviceException; {:try_start_4 .. :try_end_4} :catch_1
    .catch Lcom/osp/social/member/MemberServiceException; {:try_start_4 .. :try_end_4} :catch_2
    .catch Ljava/lang/Exception; {:try_start_4 .. :try_end_4} :catch_4

    goto/16 :goto_0

    .line 499
    :catch_2
    move-exception v0

    .line 501
    iget-object v1, p0, Lcom/osp/app/signin/jz;->f:Lcom/osp/app/util/n;

    iget-object v2, p0, Lcom/osp/app/signin/jz;->g:Landroid/content/Context;

    invoke-virtual {v1, v2, v0}, Lcom/osp/app/util/n;->a(Landroid/content/Context;Lcom/osp/common/abstracts/AbstractOSPException;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/osp/app/signin/jz;->h:Ljava/lang/String;

    .line 502
    sget-object v1, Lcom/osp/app/signin/kb;->a:Lcom/osp/app/signin/kb;

    iput-object v1, p0, Lcom/osp/app/signin/jz;->i:Lcom/osp/app/signin/kb;

    .line 503
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v1, "SIV01"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "autosign MemberServiceException mSignInState=["

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v3, p0, Lcom/osp/app/signin/jz;->i:Lcom/osp/app/signin/kb;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "] Canceled"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 504
    invoke-virtual {v0}, Lcom/osp/social/member/MemberServiceException;->printStackTrace()V

    .line 505
    new-instance v1, Lcom/osp/social/member/MemberServiceException;

    invoke-virtual {v0}, Lcom/osp/social/member/MemberServiceException;->getMessage()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0}, Lcom/osp/social/member/MemberServiceException;->a()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v1, v2, v0, v3}, Lcom/osp/social/member/MemberServiceException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;Ljava/lang/String;)V

    throw v1

    .line 412
    :catch_3
    move-exception v0

    :try_start_5
    invoke-virtual {v1}, Lcom/osp/security/identity/d;->k()V

    invoke-virtual {v2}, Lcom/osp/security/credential/a;->a()V

    invoke-virtual {v3}, Lcom/osp/social/member/d;->a()V

    const-wide/16 v2, 0x32

    invoke-static {v2, v3}, Ljava/lang/Thread;->sleep(J)V
    :try_end_5
    .catch Lcom/osp/security/identity/IdentityException; {:try_start_5 .. :try_end_5} :catch_5
    .catch Lcom/osp/security/credential/CredentialException; {:try_start_5 .. :try_end_5} :catch_6
    .catch Lcom/osp/social/member/MemberServiceException; {:try_start_5 .. :try_end_5} :catch_7
    .catch Ljava/lang/InterruptedException; {:try_start_5 .. :try_end_5} :catch_8
    .catch Lcom/osp/device/DeviceException; {:try_start_5 .. :try_end_5} :catch_1
    .catch Ljava/lang/Exception; {:try_start_5 .. :try_end_5} :catch_4

    :goto_2
    :try_start_6
    iget-object v1, p0, Lcom/osp/app/signin/jz;->f:Lcom/osp/app/util/n;

    iget-object v2, p0, Lcom/osp/app/signin/jz;->g:Landroid/content/Context;

    invoke-virtual {v1, v2, v0}, Lcom/osp/app/util/n;->a(Landroid/content/Context;Lcom/osp/common/abstracts/AbstractOSPException;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/osp/app/signin/jz;->h:Ljava/lang/String;

    sget-object v1, Lcom/osp/app/signin/kb;->d:Lcom/osp/app/signin/kb;

    iput-object v1, p0, Lcom/osp/app/signin/jz;->i:Lcom/osp/app/signin/kb;

    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v1, "SIV01"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "requestDeviceRegistration DeviceException mSignInState=["

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v3, p0, Lcom/osp/app/signin/jz;->i:Lcom/osp/app/signin/kb;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "] Failed"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v0}, Lcom/osp/device/DeviceException;->printStackTrace()V

    new-instance v1, Lcom/osp/device/DeviceException;

    invoke-virtual {v0}, Lcom/osp/device/DeviceException;->getMessage()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0}, Lcom/osp/device/DeviceException;->a()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v1, v2, v0, v3}, Lcom/osp/device/DeviceException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;Ljava/lang/String;)V

    throw v1
    :try_end_6
    .catch Lcom/osp/security/identity/IdentityException; {:try_start_6 .. :try_end_6} :catch_0
    .catch Lcom/osp/device/DeviceException; {:try_start_6 .. :try_end_6} :catch_1
    .catch Lcom/osp/social/member/MemberServiceException; {:try_start_6 .. :try_end_6} :catch_2
    .catch Ljava/lang/Exception; {:try_start_6 .. :try_end_6} :catch_4

    .line 506
    :catch_4
    move-exception v0

    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    .line 512
    sget-object v0, Lcom/osp/app/signin/kb;->a:Lcom/osp/app/signin/kb;

    iput-object v0, p0, Lcom/osp/app/signin/jz;->i:Lcom/osp/app/signin/kb;

    .line 513
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v0, "SIV01"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "autosign Exception mSignInState=["

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, Lcom/osp/app/signin/jz;->i:Lcom/osp/app/signin/kb;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "] Canceled"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 516
    :cond_4
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v0, "SIV01"

    const-string v1, "autosign"

    const-string v2, "END"

    invoke-static {v0, v1, v2}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 517
    iget-object v0, p0, Lcom/osp/app/signin/jz;->i:Lcom/osp/app/signin/kb;

    return-object v0

    .line 412
    :catch_5
    move-exception v1

    :try_start_7
    invoke-virtual {v1}, Lcom/osp/security/identity/IdentityException;->printStackTrace()V

    goto :goto_2

    :catch_6
    move-exception v1

    invoke-virtual {v0}, Lcom/osp/device/DeviceException;->printStackTrace()V

    goto :goto_2

    :catch_7
    move-exception v1

    invoke-virtual {v0}, Lcom/osp/device/DeviceException;->printStackTrace()V

    goto/16 :goto_2

    :catch_8
    move-exception v1

    invoke-virtual {v0}, Lcom/osp/device/DeviceException;->printStackTrace()V

    goto/16 :goto_2

    :catch_9
    move-exception v0

    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v0, "SIV01"

    const-string v6, "requestDeviceRegistration Exception"

    invoke-static {v0, v6}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_1

    .line 415
    :cond_5
    sget-object v0, Lcom/osp/app/signin/kb;->j:Lcom/osp/app/signin/kb;

    iput-object v0, p0, Lcom/osp/app/signin/jz;->i:Lcom/osp/app/signin/kb;

    .line 416
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v0, "SIV01"

    new-instance v6, Ljava/lang/StringBuilder;

    const-string v7, "autosign mSignInState=["

    invoke-direct {v6, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v7, p0, Lcom/osp/app/signin/jz;->i:Lcom/osp/app/signin/kb;

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, "] Success"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v0, v6}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 420
    :pswitch_2
    const-string v0, "14eev3f64b"

    invoke-virtual {v3, v0}, Lcom/osp/social/member/d;->a(Ljava/lang/String;)Z

    move-result v0

    .line 421
    if-ne v0, v8, :cond_6

    .line 423
    sget-object v0, Lcom/osp/app/signin/kb;->e:Lcom/osp/app/signin/kb;

    iput-object v0, p0, Lcom/osp/app/signin/jz;->i:Lcom/osp/app/signin/kb;

    goto/16 :goto_0

    .line 426
    :cond_6
    sget-object v0, Lcom/osp/app/signin/kb;->f:Lcom/osp/app/signin/kb;

    iput-object v0, p0, Lcom/osp/app/signin/jz;->i:Lcom/osp/app/signin/kb;

    goto/16 :goto_0

    .line 430
    :pswitch_3
    invoke-virtual {v1}, Lcom/osp/security/identity/d;->i()Z

    move-result v0

    .line 431
    if-ne v0, v8, :cond_8

    .line 433
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v0, "SIV01"

    const-string v6, "requestAppAccountAuthentication"

    const-string v7, "START"

    invoke-static {v0, v6, v7}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    :try_end_7
    .catch Lcom/osp/security/identity/IdentityException; {:try_start_7 .. :try_end_7} :catch_0
    .catch Lcom/osp/device/DeviceException; {:try_start_7 .. :try_end_7} :catch_1
    .catch Lcom/osp/social/member/MemberServiceException; {:try_start_7 .. :try_end_7} :catch_2
    .catch Ljava/lang/Exception; {:try_start_7 .. :try_end_7} :catch_4

    :try_start_8
    const-string v0, "14eev3f64b"

    const-string v6, "109E2830E09DB340924B8ABE0D6290C3"

    invoke-virtual {v1, v0, v6}, Lcom/osp/security/identity/d;->a(Ljava/lang/String;Ljava/lang/String;)V

    sget-object v0, Lcom/osp/app/signin/kb;->b:Lcom/osp/app/signin/kb;

    iput-object v0, p0, Lcom/osp/app/signin/jz;->i:Lcom/osp/app/signin/kb;

    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v0, "SIV01"

    new-instance v6, Ljava/lang/StringBuilder;

    const-string v7, "requestAppAccountAuthentication mSignInState=["

    invoke-direct {v6, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v7, p0, Lcom/osp/app/signin/jz;->i:Lcom/osp/app/signin/kb;

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, "]"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v0, v6}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_8
    .catch Lcom/osp/security/identity/IdentityException; {:try_start_8 .. :try_end_8} :catch_a
    .catch Ljava/lang/Exception; {:try_start_8 .. :try_end_8} :catch_b
    .catch Lcom/osp/device/DeviceException; {:try_start_8 .. :try_end_8} :catch_1
    .catch Lcom/osp/social/member/MemberServiceException; {:try_start_8 .. :try_end_8} :catch_2

    :goto_3
    :try_start_9
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v0, "SIV01"

    const-string v6, "requestAppAccountAuthentication"

    const-string v7, "END"

    invoke-static {v0, v6, v7}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    :catch_a
    move-exception v0

    const-string v6, "SSO_2101"

    invoke-virtual {v0}, Lcom/osp/security/identity/IdentityException;->a()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-ne v6, v8, :cond_7

    sget-object v0, Lcom/osp/app/signin/kb;->h:Lcom/osp/app/signin/kb;

    iput-object v0, p0, Lcom/osp/app/signin/jz;->i:Lcom/osp/app/signin/kb;

    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v0, "SIV01"

    new-instance v6, Ljava/lang/StringBuilder;

    const-string v7, "requestAppAccountAuthentication IdentityException mSignInState=["

    invoke-direct {v6, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v7, p0, Lcom/osp/app/signin/jz;->i:Lcom/osp/app/signin/kb;

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, "]"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v0, v6}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_3

    :cond_7
    iget-object v1, p0, Lcom/osp/app/signin/jz;->f:Lcom/osp/app/util/n;

    iget-object v2, p0, Lcom/osp/app/signin/jz;->g:Landroid/content/Context;

    invoke-virtual {v1, v2, v0}, Lcom/osp/app/util/n;->a(Landroid/content/Context;Lcom/osp/common/abstracts/AbstractOSPException;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/osp/app/signin/jz;->h:Ljava/lang/String;

    sget-object v1, Lcom/osp/app/signin/kb;->d:Lcom/osp/app/signin/kb;

    iput-object v1, p0, Lcom/osp/app/signin/jz;->i:Lcom/osp/app/signin/kb;

    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v1, "SIV01"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "requestAppAccountAuthentication IdentityException mSignInState=["

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v3, p0, Lcom/osp/app/signin/jz;->i:Lcom/osp/app/signin/kb;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "] Failed"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v0}, Lcom/osp/security/identity/IdentityException;->printStackTrace()V

    new-instance v1, Lcom/osp/security/identity/IdentityException;

    invoke-virtual {v0}, Lcom/osp/security/identity/IdentityException;->getMessage()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0}, Lcom/osp/security/identity/IdentityException;->a()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v1, v2, v0, v3}, Lcom/osp/security/identity/IdentityException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;Ljava/lang/String;)V

    throw v1

    :catch_b
    move-exception v0

    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v0, "SIV01"

    const-string v6, "requestAppAccountAuthentication Exception"

    invoke-static {v0, v6}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_3

    .line 436
    :cond_8
    sget-object v0, Lcom/osp/app/signin/kb;->h:Lcom/osp/app/signin/kb;

    iput-object v0, p0, Lcom/osp/app/signin/jz;->i:Lcom/osp/app/signin/kb;

    goto/16 :goto_0

    .line 440
    :pswitch_4
    invoke-virtual {v1}, Lcom/osp/security/identity/d;->i()Z

    move-result v0

    .line 441
    if-ne v0, v8, :cond_a

    .line 443
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v0, "SIV01"

    const-string v6, "requestAppAccountRegistration"

    const-string v7, "START"

    invoke-static {v0, v6, v7}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    :try_end_9
    .catch Lcom/osp/security/identity/IdentityException; {:try_start_9 .. :try_end_9} :catch_0
    .catch Lcom/osp/device/DeviceException; {:try_start_9 .. :try_end_9} :catch_1
    .catch Lcom/osp/social/member/MemberServiceException; {:try_start_9 .. :try_end_9} :catch_2
    .catch Ljava/lang/Exception; {:try_start_9 .. :try_end_9} :catch_4

    :try_start_a
    new-instance v0, Lcom/osp/social/member/a;

    invoke-direct {v0}, Lcom/osp/social/member/a;-><init>()V

    const-string v6, "14eev3f64b"

    invoke-virtual {v0, v6}, Lcom/osp/social/member/a;->a(Ljava/lang/String;)V

    const-string v6, "109E2830E09DB340924B8ABE0D6290C3"

    invoke-virtual {v0, v6}, Lcom/osp/social/member/a;->b(Ljava/lang/String;)V

    new-instance v6, Lcom/osp/social/member/b;

    invoke-direct {v6}, Lcom/osp/social/member/b;-><init>()V

    invoke-virtual {v0, v6}, Lcom/osp/social/member/a;->a(Lcom/osp/social/member/b;)V

    const-string v7, "N/A"

    invoke-virtual {v6, v7}, Lcom/osp/social/member/b;->a(Ljava/lang/String;)V

    const-string v7, "Device"

    invoke-virtual {v6, v7}, Lcom/osp/social/member/b;->b(Ljava/lang/String;)V

    new-instance v6, Lcom/osp/social/member/c;

    invoke-direct {v6}, Lcom/osp/social/member/c;-><init>()V

    const-string v7, ""

    invoke-virtual {v6, v7}, Lcom/osp/social/member/c;->a(Ljava/lang/String;)V

    const-string v7, ""

    invoke-virtual {v6, v7}, Lcom/osp/social/member/c;->b(Ljava/lang/String;)V

    invoke-virtual {v3, v0}, Lcom/osp/social/member/d;->a(Lcom/osp/social/member/a;)V

    sget-object v0, Lcom/osp/app/signin/kb;->e:Lcom/osp/app/signin/kb;

    iput-object v0, p0, Lcom/osp/app/signin/jz;->i:Lcom/osp/app/signin/kb;
    :try_end_a
    .catch Lcom/osp/social/member/MemberServiceException; {:try_start_a .. :try_end_a} :catch_c
    .catch Ljava/lang/Exception; {:try_start_a .. :try_end_a} :catch_d
    .catch Lcom/osp/security/identity/IdentityException; {:try_start_a .. :try_end_a} :catch_0
    .catch Lcom/osp/device/DeviceException; {:try_start_a .. :try_end_a} :catch_1

    :goto_4
    :try_start_b
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v0, "SIV01"

    const-string v6, "requestAppAccountRegistration"

    const-string v7, "END"

    invoke-static {v0, v6, v7}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    :catch_c
    move-exception v0

    const-string v6, "SSO_2101"

    invoke-virtual {v0}, Lcom/osp/social/member/MemberServiceException;->a()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-ne v6, v8, :cond_9

    sget-object v0, Lcom/osp/app/signin/kb;->h:Lcom/osp/app/signin/kb;

    iput-object v0, p0, Lcom/osp/app/signin/jz;->i:Lcom/osp/app/signin/kb;

    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v0, "SIV01"

    new-instance v6, Ljava/lang/StringBuilder;

    const-string v7, "requestAppAccountRegistration MemberServiceException mSignInState=["

    invoke-direct {v6, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v7, p0, Lcom/osp/app/signin/jz;->i:Lcom/osp/app/signin/kb;

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, "]"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v0, v6}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_4

    :cond_9
    iget-object v1, p0, Lcom/osp/app/signin/jz;->f:Lcom/osp/app/util/n;

    iget-object v2, p0, Lcom/osp/app/signin/jz;->g:Landroid/content/Context;

    invoke-virtual {v1, v2, v0}, Lcom/osp/app/util/n;->a(Landroid/content/Context;Lcom/osp/common/abstracts/AbstractOSPException;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/osp/app/signin/jz;->h:Ljava/lang/String;

    sget-object v1, Lcom/osp/app/signin/kb;->d:Lcom/osp/app/signin/kb;

    iput-object v1, p0, Lcom/osp/app/signin/jz;->i:Lcom/osp/app/signin/kb;

    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v1, "SIV01"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "requestAppAccountRegistration MemberServiceException mSignInState=["

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v3, p0, Lcom/osp/app/signin/jz;->i:Lcom/osp/app/signin/kb;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "] Failed"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v0}, Lcom/osp/social/member/MemberServiceException;->printStackTrace()V

    new-instance v1, Lcom/osp/social/member/MemberServiceException;

    invoke-virtual {v0}, Lcom/osp/social/member/MemberServiceException;->getMessage()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2, v0}, Lcom/osp/social/member/MemberServiceException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v1

    :catch_d
    move-exception v0

    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v0, "SIV01"

    const-string v6, "requestAppAccountRegistration Exception"

    invoke-static {v0, v6}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_4

    .line 446
    :cond_a
    sget-object v0, Lcom/osp/app/signin/kb;->h:Lcom/osp/app/signin/kb;

    iput-object v0, p0, Lcom/osp/app/signin/jz;->i:Lcom/osp/app/signin/kb;

    goto/16 :goto_0

    .line 450
    :pswitch_5
    const-string v0, "14eev3f64b"

    invoke-virtual {v3, v0}, Lcom/osp/social/member/d;->a(Ljava/lang/String;)Z

    move-result v0

    .line 451
    if-ne v0, v8, :cond_b

    .line 453
    sget-object v0, Lcom/osp/app/signin/kb;->e:Lcom/osp/app/signin/kb;

    iput-object v0, p0, Lcom/osp/app/signin/jz;->i:Lcom/osp/app/signin/kb;

    goto/16 :goto_0

    .line 456
    :cond_b
    sget-object v0, Lcom/osp/app/signin/kb;->f:Lcom/osp/app/signin/kb;

    iput-object v0, p0, Lcom/osp/app/signin/jz;->i:Lcom/osp/app/signin/kb;

    goto/16 :goto_0

    .line 460
    :pswitch_6
    invoke-virtual {v1}, Lcom/osp/security/identity/d;->h()Z

    move-result v0

    .line 461
    if-ne v0, v8, :cond_e

    .line 463
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v0, "SIV01"

    const-string v6, "requestUserAuthenticationAuto"

    const-string v7, "START"

    invoke-static {v0, v6, v7}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    :try_end_b
    .catch Lcom/osp/security/identity/IdentityException; {:try_start_b .. :try_end_b} :catch_0
    .catch Lcom/osp/device/DeviceException; {:try_start_b .. :try_end_b} :catch_1
    .catch Lcom/osp/social/member/MemberServiceException; {:try_start_b .. :try_end_b} :catch_2
    .catch Ljava/lang/Exception; {:try_start_b .. :try_end_b} :catch_4

    :try_start_c
    new-instance v0, Lcom/osp/security/identity/k;

    invoke-direct {v0}, Lcom/osp/security/identity/k;-><init>()V

    invoke-virtual {v0}, Lcom/osp/security/identity/k;->b()V

    invoke-virtual {v1, v0, p2}, Lcom/osp/security/identity/d;->a(Lcom/osp/security/identity/k;Ljava/lang/String;)V

    sget-object v0, Lcom/osp/app/signin/kb;->c:Lcom/osp/app/signin/kb;

    iput-object v0, p0, Lcom/osp/app/signin/jz;->i:Lcom/osp/app/signin/kb;
    :try_end_c
    .catch Lcom/osp/security/identity/IdentityException; {:try_start_c .. :try_end_c} :catch_e
    .catch Ljava/lang/Exception; {:try_start_c .. :try_end_c} :catch_10
    .catch Lcom/osp/device/DeviceException; {:try_start_c .. :try_end_c} :catch_1
    .catch Lcom/osp/social/member/MemberServiceException; {:try_start_c .. :try_end_c} :catch_2

    :goto_5
    :try_start_d
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v0, "SIV01"

    const-string v6, "requestUserAuthenticationAuto"

    const-string v7, "END"

    invoke-static {v0, v6, v7}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    :catch_e
    move-exception v0

    const-string v1, "SSO_2011"

    invoke-virtual {v0}, Lcom/osp/security/identity/IdentityException;->a()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eq v1, v8, :cond_c

    const-string v1, "SSO_2012"

    invoke-virtual {v0}, Lcom/osp/security/identity/IdentityException;->a()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eq v1, v8, :cond_c

    const-string v1, "SSO_2015"

    invoke-virtual {v0}, Lcom/osp/security/identity/IdentityException;->a()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_c

    const-string v1, "SSO_2016"

    invoke-virtual {v0}, Lcom/osp/security/identity/IdentityException;->a()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_d

    :cond_c
    sget-object v1, Lcom/osp/app/signin/kb;->d:Lcom/osp/app/signin/kb;

    iput-object v1, p0, Lcom/osp/app/signin/jz;->i:Lcom/osp/app/signin/kb;

    :goto_6
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v1, "SIV01"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "requestUserAuthenticationAuto IdentityException mSignInState=["

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v3, p0, Lcom/osp/app/signin/jz;->i:Lcom/osp/app/signin/kb;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "] Failed"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;)V

    new-instance v1, Lcom/osp/security/identity/IdentityException;

    invoke-virtual {v0}, Lcom/osp/security/identity/IdentityException;->getMessage()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2, v0}, Lcom/osp/security/identity/IdentityException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v1

    :cond_d
    iget-object v1, p0, Lcom/osp/app/signin/jz;->f:Lcom/osp/app/util/n;

    iget-object v2, p0, Lcom/osp/app/signin/jz;->g:Landroid/content/Context;

    invoke-virtual {v1, v2, v0}, Lcom/osp/app/util/n;->a(Landroid/content/Context;Lcom/osp/common/abstracts/AbstractOSPException;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/osp/app/signin/jz;->h:Ljava/lang/String;

    sget-object v1, Lcom/osp/app/signin/kb;->d:Lcom/osp/app/signin/kb;

    iput-object v1, p0, Lcom/osp/app/signin/jz;->i:Lcom/osp/app/signin/kb;
    :try_end_d
    .catch Lcom/osp/security/identity/IdentityException; {:try_start_d .. :try_end_d} :catch_0
    .catch Lcom/osp/device/DeviceException; {:try_start_d .. :try_end_d} :catch_1
    .catch Lcom/osp/social/member/MemberServiceException; {:try_start_d .. :try_end_d} :catch_2
    .catch Ljava/lang/Exception; {:try_start_d .. :try_end_d} :catch_4

    :try_start_e
    invoke-virtual {v0}, Lcom/osp/security/identity/IdentityException;->printStackTrace()V
    :try_end_e
    .catch Ljava/lang/StringIndexOutOfBoundsException; {:try_start_e .. :try_end_e} :catch_f
    .catch Lcom/osp/security/identity/IdentityException; {:try_start_e .. :try_end_e} :catch_0
    .catch Lcom/osp/device/DeviceException; {:try_start_e .. :try_end_e} :catch_1
    .catch Lcom/osp/social/member/MemberServiceException; {:try_start_e .. :try_end_e} :catch_2
    .catch Ljava/lang/Exception; {:try_start_e .. :try_end_e} :catch_4

    goto :goto_6

    :catch_f
    move-exception v1

    :try_start_f
    invoke-virtual {v0}, Lcom/osp/security/identity/IdentityException;->printStackTrace()V

    goto :goto_6

    :catch_10
    move-exception v0

    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v0, "SIV01"

    const-string v6, "requestUserAuthenticationAuto Exception"

    invoke-static {v0, v6}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_5

    .line 466
    :cond_e
    sget-object v0, Lcom/osp/app/signin/kb;->a:Lcom/osp/app/signin/kb;

    iput-object v0, p0, Lcom/osp/app/signin/jz;->i:Lcom/osp/app/signin/kb;

    .line 467
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v0, "SIV01"

    new-instance v6, Ljava/lang/StringBuilder;

    const-string v7, "autosign mSignInState=["

    invoke-direct {v6, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v7, p0, Lcom/osp/app/signin/jz;->i:Lcom/osp/app/signin/kb;

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, "] Canceled"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v0, v6}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 471
    :pswitch_7
    iget-object v0, p0, Lcom/osp/app/signin/jz;->g:Landroid/content/Context;

    invoke-static {v0}, Lcom/osp/app/util/ad;->d(Landroid/content/Context;)Z

    move-result v0

    if-nez v0, :cond_f

    .line 473
    invoke-virtual {v1}, Lcom/osp/security/identity/d;->k()V

    .line 474
    invoke-virtual {v2}, Lcom/osp/security/credential/a;->a()V

    .line 475
    invoke-virtual {v3}, Lcom/osp/social/member/d;->a()V

    .line 478
    :cond_f
    sget-object v0, Lcom/osp/app/signin/kb;->a:Lcom/osp/app/signin/kb;

    iput-object v0, p0, Lcom/osp/app/signin/jz;->i:Lcom/osp/app/signin/kb;

    .line 479
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v0, "SIV01"

    new-instance v6, Ljava/lang/StringBuilder;

    const-string v7, "autosign mSignInState=["

    invoke-direct {v6, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v7, p0, Lcom/osp/app/signin/jz;->i:Lcom/osp/app/signin/kb;

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, "] Canceled"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v0, v6}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 480
    const-wide/16 v6, 0x32

    invoke-static {v6, v7}, Ljava/lang/Thread;->sleep(J)V
    :try_end_f
    .catch Lcom/osp/security/identity/IdentityException; {:try_start_f .. :try_end_f} :catch_0
    .catch Lcom/osp/device/DeviceException; {:try_start_f .. :try_end_f} :catch_1
    .catch Lcom/osp/social/member/MemberServiceException; {:try_start_f .. :try_end_f} :catch_2
    .catch Ljava/lang/Exception; {:try_start_f .. :try_end_f} :catch_4

    goto/16 :goto_0

    .line 357
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_7
    .end packed-switch
.end method

.method public final a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 578
    iget-object v0, p0, Lcom/osp/app/signin/jz;->h:Ljava/lang/String;

    return-object v0
.end method
