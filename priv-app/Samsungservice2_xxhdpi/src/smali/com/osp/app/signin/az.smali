.class final Lcom/osp/app/signin/az;
.super Lcom/msc/c/b;
.source "AccountView.java"


# instance fields
.field final synthetic c:Lcom/osp/app/signin/AccountView;

.field private d:J

.field private e:Ljava/lang/String;


# direct methods
.method public constructor <init>(Lcom/osp/app/signin/AccountView;Landroid/content/Context;)V
    .locals 2

    .prologue
    .line 10287
    iput-object p1, p0, Lcom/osp/app/signin/az;->c:Lcom/osp/app/signin/AccountView;

    .line 10288
    invoke-direct {p0, p2}, Lcom/msc/c/b;-><init>(Landroid/content/Context;)V

    .line 10289
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v0, "AV"

    const-string v1, "CheckAuthenticateForDPServerTask"

    invoke-static {v0, v1}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 10290
    invoke-static {}, Lcom/osp/app/util/ac;->a()Lcom/osp/app/util/ac;

    invoke-static {p2}, Lcom/osp/app/util/ac;->a(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v0

    invoke-static {p1, v0}, Lcom/osp/app/signin/AccountView;->h(Lcom/osp/app/signin/AccountView;Ljava/lang/String;)Ljava/lang/String;

    .line 10291
    invoke-static {}, Lcom/osp/app/util/ac;->a()Lcom/osp/app/util/ac;

    invoke-static {p2}, Lcom/osp/app/util/ac;->c(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v0

    invoke-static {p1, v0}, Lcom/osp/app/signin/AccountView;->i(Lcom/osp/app/signin/AccountView;Ljava/lang/String;)Ljava/lang/String;

    .line 10294
    return-void
.end method


# virtual methods
.method protected final varargs a()Ljava/lang/Boolean;
    .locals 7

    .prologue
    .line 10321
    iget-object v0, p0, Lcom/osp/app/signin/az;->c:Lcom/osp/app/signin/AccountView;

    iget-object v1, p0, Lcom/osp/app/signin/az;->c:Lcom/osp/app/signin/AccountView;

    invoke-static {v1}, Lcom/osp/app/util/ad;->p(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/osp/app/signin/AccountView;->g(Lcom/osp/app/signin/AccountView;Ljava/lang/String;)Ljava/lang/String;

    .line 10323
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v0, "AV"

    const-string v1, "requestCheckAuthenticateForDPServer start"

    invoke-static {v0, v1}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 10324
    invoke-static {}, Lcom/msc/c/g;->a()Lcom/msc/c/g;

    iget-object v6, p0, Lcom/osp/app/signin/az;->c:Lcom/osp/app/signin/AccountView;

    iget-object v0, p0, Lcom/osp/app/signin/az;->c:Lcom/osp/app/signin/AccountView;

    invoke-static {v0}, Lcom/osp/app/signin/AccountView;->ai(Lcom/osp/app/signin/AccountView;)Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/osp/app/signin/az;->c:Lcom/osp/app/signin/AccountView;

    invoke-static {v1}, Lcom/osp/app/signin/AccountView;->aj(Lcom/osp/app/signin/AccountView;)Ljava/lang/String;

    move-result-object v1

    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v2, "SRS"

    const-string v3, "prepareCheckAuthenticateForDP()"

    invoke-static {v2, v3}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;)V

    invoke-static {v0, v1}, Lcom/msc/c/k;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    invoke-static {v1}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;)V

    invoke-static {}, Lcom/msc/b/i;->a()Lcom/msc/b/i;

    move-result-object v0

    invoke-static {}, Lcom/osp/app/util/ah;->a()Lcom/osp/app/util/ah;

    move-result-object v2

    invoke-virtual {v2}, Lcom/osp/app/util/ah;->g()Z

    move-result v4

    invoke-static {}, Lcom/osp/app/signin/SamsungService;->j()Z

    move-result v2

    invoke-static {}, Lcom/osp/app/signin/SamsungService;->b()Z

    move-result v3

    move-object v5, p0

    invoke-virtual/range {v0 .. v5}, Lcom/msc/b/i;->a(Ljava/lang/String;ZZZLcom/msc/b/h;)Lcom/msc/b/d;

    move-result-object v0

    invoke-static {}, Lcom/osp/app/util/ah;->a()Lcom/osp/app/util/ah;

    move-result-object v1

    invoke-virtual {v1}, Lcom/osp/app/util/ah;->c()Z

    move-result v1

    if-eqz v1, :cond_0

    const-string v1, "SQELOG"

    invoke-static {}, Lcom/osp/app/util/ah;->a()Lcom/osp/app/util/ah;

    move-result-object v2

    invoke-virtual {v2}, Lcom/osp/app/util/ah;->b()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/msc/b/d;->a(Ljava/lang/String;Ljava/lang/String;)V

    :cond_0
    const-string v1, "j5p7ll8g33:5763D0052DC1462E13751F753384E9A9"

    invoke-virtual {v1}, Ljava/lang/String;->getBytes()[B

    move-result-object v1

    const/4 v2, 0x2

    invoke-static {v1, v2}, Landroid/util/Base64;->encodeToString([BI)Ljava/lang/String;

    move-result-object v1

    invoke-static {}, Lcom/osp/app/util/ac;->a()Lcom/osp/app/util/ac;

    invoke-static {v6}, Lcom/osp/app/util/ac;->b(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v2

    const-string v3, "User-Agent"

    invoke-virtual {v0, v3, v2}, Lcom/msc/b/d;->a(Ljava/lang/String;Ljava/lang/String;)V

    const-string v2, "Authorization"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "Basic "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v2, v1}, Lcom/msc/b/d;->a(Ljava/lang/String;Ljava/lang/String;)V

    const-string v1, "Content-Type"

    const-string v2, "application/json"

    invoke-virtual {v0, v1, v2}, Lcom/msc/b/d;->a(Ljava/lang/String;Ljava/lang/String;)V

    const-string v1, ""

    invoke-virtual {v0, v1}, Lcom/msc/b/d;->a(Ljava/lang/String;)V

    invoke-virtual {v0}, Lcom/msc/b/d;->a()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/osp/app/signin/az;->d:J

    iget-wide v0, p0, Lcom/osp/app/signin/az;->d:J

    invoke-virtual {p0, v0, v1}, Lcom/osp/app/signin/az;->a(J)V

    iget-wide v0, p0, Lcom/osp/app/signin/az;->d:J

    const-string v2, "from_json"

    invoke-virtual {p0, v0, v1, v2}, Lcom/osp/app/signin/az;->a(JLjava/lang/String;)V

    iget-wide v0, p0, Lcom/osp/app/signin/az;->d:J

    sget-object v2, Lcom/msc/b/g;->b:Lcom/msc/b/g;

    invoke-static {v0, v1, v2}, Lcom/msc/c/g;->a(JLcom/msc/b/g;)V

    .line 10325
    const/4 v0, 0x0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    return-object v0
.end method

.method public final declared-synchronized a(Lcom/msc/b/c;)V
    .locals 6

    .prologue
    .line 10337
    monitor-enter p0

    :try_start_0
    invoke-super {p0, p1}, Lcom/msc/c/b;->a(Lcom/msc/b/c;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 10339
    if-nez p1, :cond_1

    .line 10374
    :cond_0
    :goto_0
    monitor-exit p0

    return-void

    .line 10344
    :cond_1
    :try_start_1
    invoke-virtual {p1}, Lcom/msc/b/c;->a()J

    move-result-wide v0

    .line 10345
    invoke-virtual {p1}, Lcom/msc/b/c;->e()Ljava/lang/String;

    move-result-object v2

    .line 10347
    iget-wide v4, p0, Lcom/osp/app/signin/az;->d:J
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    cmp-long v0, v0, v4

    if-nez v0, :cond_0

    .line 10351
    :try_start_2
    const-string v0, "Fail"

    iput-object v0, p0, Lcom/osp/app/signin/az;->e:Ljava/lang/String;

    .line 10353
    const-string v0, ""

    .line 10354
    invoke-static {}, Lcom/msc/c/h;->a()Lcom/msc/c/h;

    invoke-static {v2}, Lcom/msc/c/h;->F(Ljava/lang/String;)Ljava/util/HashMap;

    move-result-object v1

    .line 10355
    const-string v2, "auth_status"

    invoke-virtual {v1, v2}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 10357
    const-string v0, "auth_status"

    invoke-virtual {v1, v0}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 10359
    :cond_2
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "mEasySignup Authentication : "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;)V

    .line 10361
    const-string v2, "1"

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 10363
    iget-object v2, p0, Lcom/osp/app/signin/az;->c:Lcom/osp/app/signin/AccountView;

    const-string v0, "msisdn"

    invoke-virtual {v1, v0}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-static {v2, v0}, Lcom/osp/app/signin/AccountView;->j(Lcom/osp/app/signin/AccountView;Ljava/lang/String;)Ljava/lang/String;

    .line 10364
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "mEasySignupPhoneNumber : "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v1, p0, Lcom/osp/app/signin/az;->c:Lcom/osp/app/signin/AccountView;

    invoke-static {v1}, Lcom/osp/app/signin/AccountView;->ah(Lcom/osp/app/signin/AccountView;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;)V

    .line 10365
    const-string v0, "Success"

    iput-object v0, p0, Lcom/osp/app/signin/az;->e:Ljava/lang/String;
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_0
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0

    .line 10368
    :catch_0
    move-exception v0

    :try_start_3
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    .line 10371
    const-string v0, "Fail"

    iput-object v0, p0, Lcom/osp/app/signin/az;->e:Ljava/lang/String;
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    goto :goto_0

    .line 10337
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method protected final a(Ljava/lang/Boolean;)V
    .locals 2

    .prologue
    .line 10298
    invoke-super {p0, p1}, Lcom/msc/c/b;->a(Ljava/lang/Boolean;)V

    .line 10299
    const-string v0, "Fail"

    iget-object v1, p0, Lcom/osp/app/signin/az;->e:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 10301
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v0, "AV"

    const-string v1, "CheckAuthenticateForDPServerTask : not authenticated"

    invoke-static {v0, v1}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 10305
    iget-object v0, p0, Lcom/osp/app/signin/az;->c:Lcom/osp/app/signin/AccountView;

    invoke-static {v0}, Lcom/osp/app/signin/AccountView;->N(Lcom/osp/app/signin/AccountView;)V

    .line 10316
    :cond_0
    :goto_0
    return-void

    .line 10308
    :cond_1
    const-string v0, "Success"

    iget-object v1, p0, Lcom/osp/app/signin/az;->e:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 10310
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v0, "AV"

    const-string v1, "CheckAuthenticateForDPServerTask : is already authenticated"

    invoke-static {v0, v1}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 10311
    iget-object v0, p0, Lcom/osp/app/signin/az;->c:Lcom/osp/app/signin/AccountView;

    invoke-static {v0}, Lcom/osp/app/signin/AccountView;->N(Lcom/osp/app/signin/AccountView;)V

    goto :goto_0
.end method

.method public final b(Lcom/msc/b/c;)V
    .locals 4

    .prologue
    .line 10378
    invoke-super {p0, p1}, Lcom/msc/c/b;->b(Lcom/msc/b/c;)V

    .line 10380
    if-nez p1, :cond_1

    .line 10394
    :cond_0
    :goto_0
    return-void

    .line 10385
    :cond_1
    invoke-virtual {p1}, Lcom/msc/b/c;->a()J

    move-result-wide v0

    .line 10388
    iget-wide v2, p0, Lcom/osp/app/signin/az;->d:J

    cmp-long v0, v0, v2

    if-nez v0, :cond_0

    .line 10390
    const-string v0, "Fail"

    iput-object v0, p0, Lcom/osp/app/signin/az;->e:Ljava/lang/String;

    .line 10391
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v0, "AV"

    const-string v1, "CheckAuthenticateForDPServerTask : reponse failed"

    invoke-static {v0, v1}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method protected final d()V
    .locals 2

    .prologue
    .line 10330
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v0, "AV"

    const-string v1, "Cancel CheckAuthenticateForDPServerTask"

    invoke-static {v0, v1}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 10331
    invoke-super {p0}, Lcom/msc/c/b;->d()V

    .line 10333
    return-void
.end method

.method protected final synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 10270
    invoke-virtual {p0}, Lcom/osp/app/signin/az;->a()Ljava/lang/Boolean;

    move-result-object v0

    return-object v0
.end method

.method protected final synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 10270
    check-cast p1, Ljava/lang/Boolean;

    invoke-virtual {p0, p1}, Lcom/osp/app/signin/az;->a(Ljava/lang/Boolean;)V

    return-void
.end method
