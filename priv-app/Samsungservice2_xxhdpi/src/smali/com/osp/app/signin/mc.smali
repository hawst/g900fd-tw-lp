.class final Lcom/osp/app/signin/mc;
.super Lcom/msc/c/b;
.source "SignUpView.java"


# instance fields
.field final synthetic c:Lcom/osp/app/signin/SignUpView;

.field private d:J

.field private e:Lcom/msc/sa/d/f;


# direct methods
.method public constructor <init>(Lcom/osp/app/signin/SignUpView;)V
    .locals 1

    .prologue
    .line 7770
    iput-object p1, p0, Lcom/osp/app/signin/mc;->c:Lcom/osp/app/signin/SignUpView;

    .line 7771
    invoke-direct {p0, p1}, Lcom/msc/c/b;-><init>(Landroid/content/Context;)V

    .line 7768
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/osp/app/signin/mc;->e:Lcom/msc/sa/d/f;

    .line 7773
    return-void
.end method

.method static synthetic a(Lcom/osp/app/signin/mc;)V
    .locals 0

    .prologue
    .line 7763
    invoke-direct {p0}, Lcom/osp/app/signin/mc;->i()V

    return-void
.end method

.method private i()V
    .locals 4

    .prologue
    const/4 v2, 0x0

    .line 8025
    iget-object v0, p0, Lcom/osp/app/signin/mc;->c:Lcom/osp/app/signin/SignUpView;

    invoke-static {v0}, Lcom/osp/app/signin/SignUpView;->ag(Lcom/osp/app/signin/SignUpView;)Landroid/app/AlertDialog;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 8029
    :try_start_0
    iget-object v0, p0, Lcom/osp/app/signin/mc;->c:Lcom/osp/app/signin/SignUpView;

    invoke-static {v0}, Lcom/osp/app/signin/SignUpView;->ag(Lcom/osp/app/signin/SignUpView;)Landroid/app/AlertDialog;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/AlertDialog;->isShowing()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 8031
    iget-object v0, p0, Lcom/osp/app/signin/mc;->c:Lcom/osp/app/signin/SignUpView;

    invoke-static {v0}, Lcom/osp/app/signin/SignUpView;->ag(Lcom/osp/app/signin/SignUpView;)Landroid/app/AlertDialog;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/AlertDialog;->dismiss()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 8038
    :cond_0
    iget-object v0, p0, Lcom/osp/app/signin/mc;->c:Lcom/osp/app/signin/SignUpView;

    :goto_0
    invoke-static {v0, v2}, Lcom/osp/app/signin/SignUpView;->h(Lcom/osp/app/signin/SignUpView;Landroid/app/AlertDialog;)Landroid/app/AlertDialog;

    .line 8042
    :cond_1
    iget-object v0, p0, Lcom/osp/app/signin/mc;->c:Lcom/osp/app/signin/SignUpView;

    invoke-static {v0}, Lcom/osp/app/signin/SignUpView;->ag(Lcom/osp/app/signin/SignUpView;)Landroid/app/AlertDialog;

    move-result-object v0

    if-nez v0, :cond_2

    .line 8046
    :try_start_1
    iget-object v0, p0, Lcom/osp/app/signin/mc;->c:Lcom/osp/app/signin/SignUpView;

    new-instance v1, Landroid/app/AlertDialog$Builder;

    iget-object v2, p0, Lcom/osp/app/signin/mc;->c:Lcom/osp/app/signin/SignUpView;

    invoke-direct {v1, v2}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    const v2, 0x7f09004f

    invoke-virtual {v1, v2}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    const v2, 0x7f090198

    invoke-virtual {v1, v2}, Landroid/app/AlertDialog$Builder;->setMessage(I)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    const v2, 0x7f090041

    new-instance v3, Lcom/osp/app/signin/mj;

    invoke-direct {v3, p0}, Lcom/osp/app/signin/mj;-><init>(Lcom/osp/app/signin/mc;)V

    invoke-virtual {v1, v2, v3}, Landroid/app/AlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    const v2, 0x7f09005c

    new-instance v3, Lcom/osp/app/signin/mi;

    invoke-direct {v3, p0}, Lcom/osp/app/signin/mi;-><init>(Lcom/osp/app/signin/mc;)V

    invoke-virtual {v1, v2, v3}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    invoke-virtual {v1}, Landroid/app/AlertDialog$Builder;->show()Landroid/app/AlertDialog;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/osp/app/signin/SignUpView;->h(Lcom/osp/app/signin/SignUpView;Landroid/app/AlertDialog;)Landroid/app/AlertDialog;
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1

    .line 8067
    :cond_2
    :goto_1
    return-void

    .line 8033
    :catch_0
    move-exception v0

    :try_start_2
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 8038
    iget-object v0, p0, Lcom/osp/app/signin/mc;->c:Lcom/osp/app/signin/SignUpView;

    goto :goto_0

    :catchall_0
    move-exception v0

    iget-object v1, p0, Lcom/osp/app/signin/mc;->c:Lcom/osp/app/signin/SignUpView;

    invoke-static {v1, v2}, Lcom/osp/app/signin/SignUpView;->h(Lcom/osp/app/signin/SignUpView;Landroid/app/AlertDialog;)Landroid/app/AlertDialog;

    throw v0

    .line 8062
    :catch_1
    move-exception v0

    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_1
.end method


# virtual methods
.method protected final varargs a()Ljava/lang/Boolean;
    .locals 6

    .prologue
    .line 7778
    const-string v1, ""

    iget-object v0, p0, Lcom/osp/app/signin/mc;->c:Lcom/osp/app/signin/SignUpView;

    invoke-static {v0}, Lcom/osp/app/signin/SignUpView;->B(Lcom/osp/app/signin/SignUpView;)Lcom/osp/app/signin/SignUpinfo;

    move-result-object v0

    if-eqz v0, :cond_1

    invoke-static {}, Lcom/osp/device/b;->b()Lcom/osp/device/b;

    move-result-object v0

    invoke-virtual {v0}, Lcom/osp/device/b;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/osp/app/signin/mc;->c:Lcom/osp/app/signin/SignUpView;

    invoke-static {v0}, Lcom/osp/app/signin/SignUpView;->T(Lcom/osp/app/signin/SignUpView;)Z

    move-result v0

    if-eqz v0, :cond_0

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v1, p0, Lcom/osp/app/signin/mc;->c:Lcom/osp/app/signin/SignUpView;

    invoke-static {v1}, Lcom/osp/app/signin/SignUpView;->B(Lcom/osp/app/signin/SignUpView;)Lcom/osp/app/signin/SignUpinfo;

    move-result-object v1

    invoke-virtual {v1}, Lcom/osp/app/signin/SignUpinfo;->E()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/osp/app/signin/mc;->c:Lcom/osp/app/signin/SignUpView;

    invoke-static {v1}, Lcom/osp/app/signin/SignUpView;->B(Lcom/osp/app/signin/SignUpView;)Lcom/osp/app/signin/SignUpinfo;

    move-result-object v1

    invoke-virtual {v1}, Lcom/osp/app/signin/SignUpinfo;->F()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    :goto_0
    invoke-static {}, Lcom/msc/c/g;->a()Lcom/msc/c/g;

    iget-object v1, p0, Lcom/osp/app/signin/mc;->c:Lcom/osp/app/signin/SignUpView;

    iget-object v2, p0, Lcom/osp/app/signin/mc;->c:Lcom/osp/app/signin/SignUpView;

    invoke-static {v2}, Lcom/osp/app/signin/SignUpView;->B(Lcom/osp/app/signin/SignUpView;)Lcom/osp/app/signin/SignUpinfo;

    move-result-object v2

    invoke-virtual {v2}, Lcom/osp/app/signin/SignUpinfo;->E()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v0, v2, p0}, Lcom/msc/c/g;->i(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Lcom/msc/b/h;)J

    move-result-wide v0

    iput-wide v0, p0, Lcom/osp/app/signin/mc;->d:J

    iget-wide v0, p0, Lcom/osp/app/signin/mc;->d:J

    invoke-virtual {p0, v0, v1}, Lcom/osp/app/signin/mc;->a(J)V

    iget-wide v0, p0, Lcom/osp/app/signin/mc;->d:J

    sget-object v2, Lcom/msc/b/g;->c:Lcom/msc/b/g;

    invoke-static {v0, v1, v2}, Lcom/msc/c/g;->a(JLcom/msc/b/g;)V

    .line 7779
    const/4 v0, 0x0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    return-object v0

    .line 7778
    :cond_0
    iget-object v0, p0, Lcom/osp/app/signin/mc;->c:Lcom/osp/app/signin/SignUpView;

    invoke-static {v0}, Lcom/osp/app/signin/SignUpView;->B(Lcom/osp/app/signin/SignUpView;)Lcom/osp/app/signin/SignUpinfo;

    move-result-object v0

    invoke-virtual {v0}, Lcom/osp/app/signin/SignUpinfo;->e()Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    :cond_1
    invoke-static {}, Lcom/osp/device/b;->b()Lcom/osp/device/b;

    move-result-object v0

    invoke-virtual {v0}, Lcom/osp/device/b;->a()Z

    move-result v0

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/osp/app/signin/mc;->c:Lcom/osp/app/signin/SignUpView;

    invoke-static {v0}, Lcom/osp/app/signin/SignUpView;->T(Lcom/osp/app/signin/SignUpView;)Z

    move-result v0

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/osp/app/signin/mc;->c:Lcom/osp/app/signin/SignUpView;

    invoke-static {v0}, Lcom/osp/app/signin/SignUpView;->p(Lcom/osp/app/signin/SignUpView;)Landroid/widget/AutoCompleteTextView;

    move-result-object v0

    if-eqz v0, :cond_4

    :try_start_0
    invoke-static {}, Lcom/osp/common/util/i;->a()Lcom/osp/common/util/i;

    iget-object v0, p0, Lcom/osp/app/signin/mc;->c:Lcom/osp/app/signin/SignUpView;

    iget-object v0, p0, Lcom/osp/app/signin/mc;->c:Lcom/osp/app/signin/SignUpView;

    invoke-static {v0}, Lcom/osp/app/signin/SignUpView;->K(Lcom/osp/app/signin/SignUpView;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/osp/common/util/i;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_2

    sget-object v2, Ljava/util/Locale;->ENGLISH:Ljava/util/Locale;

    invoke-virtual {v0, v2}, Ljava/lang/String;->toUpperCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v0

    :cond_2
    invoke-static {}, Lcom/google/i18n/phonenumbers/h;->a()Lcom/google/i18n/phonenumbers/h;

    move-result-object v2

    iget-object v3, p0, Lcom/osp/app/signin/mc;->c:Lcom/osp/app/signin/SignUpView;

    invoke-static {v3}, Lcom/osp/app/signin/SignUpView;->p(Lcom/osp/app/signin/SignUpView;)Landroid/widget/AutoCompleteTextView;

    move-result-object v3

    invoke-virtual {v3}, Landroid/widget/AutoCompleteTextView;->getText()Landroid/text/Editable;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3, v0}, Lcom/google/i18n/phonenumbers/h;->a(Ljava/lang/CharSequence;Ljava/lang/String;)Ljava/lang/Iterable;

    move-result-object v0

    invoke-interface {v0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/i18n/phonenumbers/d;

    invoke-virtual {v0}, Lcom/google/i18n/phonenumbers/d;->a()Lcom/google/i18n/phonenumbers/ac;

    move-result-object v0

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0}, Lcom/google/i18n/phonenumbers/ac;->a()I

    move-result v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v0}, Lcom/google/i18n/phonenumbers/ac;->b()J

    move-result-wide v4

    invoke-virtual {v2, v4, v5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    goto/16 :goto_0

    :catch_0
    move-exception v0

    move-object v0, v1

    goto/16 :goto_0

    :cond_3
    iget-object v0, p0, Lcom/osp/app/signin/mc;->c:Lcom/osp/app/signin/SignUpView;

    invoke-static {v0}, Lcom/osp/app/signin/SignUpView;->p(Lcom/osp/app/signin/SignUpView;)Landroid/widget/AutoCompleteTextView;

    move-result-object v0

    if-eqz v0, :cond_4

    iget-object v0, p0, Lcom/osp/app/signin/mc;->c:Lcom/osp/app/signin/SignUpView;

    invoke-static {v0}, Lcom/osp/app/signin/SignUpView;->p(Lcom/osp/app/signin/SignUpView;)Landroid/widget/AutoCompleteTextView;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/AutoCompleteTextView;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    sget-object v1, Ljava/util/Locale;->ENGLISH:Ljava/util/Locale;

    invoke-virtual {v0, v1}, Ljava/lang/String;->toLowerCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_0

    :cond_4
    move-object v0, v1

    goto/16 :goto_0
.end method

.method public final declared-synchronized a(Lcom/msc/b/c;)V
    .locals 6

    .prologue
    .line 8071
    monitor-enter p0

    :try_start_0
    invoke-super {p0, p1}, Lcom/msc/c/b;->a(Lcom/msc/b/c;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 8072
    const/4 v1, 0x0

    .line 8073
    if-nez p1, :cond_0

    .line 8100
    :goto_0
    monitor-exit p0

    return-void

    .line 8078
    :cond_0
    :try_start_1
    invoke-virtual {p1}, Lcom/msc/b/c;->a()J

    move-result-wide v2

    .line 8079
    invoke-virtual {p1}, Lcom/msc/b/c;->e()Ljava/lang/String;

    move-result-object v0

    .line 8081
    iget-wide v4, p0, Lcom/osp/app/signin/mc;->d:J
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    cmp-long v2, v2, v4

    if-nez v2, :cond_1

    .line 8085
    :try_start_2
    invoke-static {}, Lcom/msc/c/h;->a()Lcom/msc/c/h;

    invoke-static {v0}, Lcom/msc/c/h;->f(Ljava/lang/String;)Lcom/msc/sa/d/f;
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_0
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    move-result-object v0

    .line 8099
    :goto_1
    :try_start_3
    iput-object v0, p0, Lcom/osp/app/signin/mc;->e:Lcom/msc/sa/d/f;
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    goto :goto_0

    .line 8071
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 8093
    :catch_0
    move-exception v0

    .line 8095
    :try_start_4
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    .line 8096
    new-instance v2, Lcom/msc/c/f;

    invoke-direct {v2, v0}, Lcom/msc/c/f;-><init>(Ljava/lang/Exception;)V

    iput-object v2, p0, Lcom/osp/app/signin/mc;->b:Lcom/msc/c/f;
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    :cond_1
    move-object v0, v1

    goto :goto_1
.end method

.method protected final a(Ljava/lang/Boolean;)V
    .locals 4

    .prologue
    const/4 v2, 0x0

    .line 7784
    invoke-super {p0, p1}, Lcom/msc/c/b;->a(Ljava/lang/Boolean;)V

    .line 7786
    iget-object v0, p0, Lcom/osp/app/signin/mc;->e:Lcom/msc/sa/d/f;

    if-eqz v0, :cond_1

    .line 7788
    iget-object v0, p0, Lcom/osp/app/signin/mc;->e:Lcom/msc/sa/d/f;

    invoke-virtual {v0}, Lcom/msc/sa/d/f;->a()Z

    move-result v0

    const/4 v1, 0x1

    if-ne v0, v1, :cond_5

    .line 7790
    iget-object v0, p0, Lcom/osp/app/signin/mc;->c:Lcom/osp/app/signin/SignUpView;

    invoke-static {v0}, Lcom/osp/app/signin/SignUpView;->V(Lcom/osp/app/signin/SignUpView;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_2

    iget-object v0, p0, Lcom/osp/app/signin/mc;->c:Lcom/osp/app/signin/SignUpView;

    invoke-static {v0}, Lcom/osp/app/signin/SignUpView;->p(Lcom/osp/app/signin/SignUpView;)Landroid/widget/AutoCompleteTextView;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/AutoCompleteTextView;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/osp/app/signin/mc;->c:Lcom/osp/app/signin/SignUpView;

    invoke-static {v1}, Lcom/osp/app/signin/SignUpView;->V(Lcom/osp/app/signin/SignUpView;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/osp/app/signin/mc;->c:Lcom/osp/app/signin/SignUpView;

    invoke-static {v0}, Lcom/osp/app/signin/SignUpView;->p(Lcom/osp/app/signin/SignUpView;)Landroid/widget/AutoCompleteTextView;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/AutoCompleteTextView;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/osp/app/signin/mc;->c:Lcom/osp/app/signin/SignUpView;

    invoke-static {v1}, Lcom/osp/app/signin/SignUpView;->W(Lcom/osp/app/signin/SignUpView;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 7793
    :cond_0
    iget-object v0, p0, Lcom/osp/app/signin/mc;->c:Lcom/osp/app/signin/SignUpView;

    invoke-static {v0}, Lcom/osp/app/signin/SignUpView;->X(Lcom/osp/app/signin/SignUpView;)V

    .line 7899
    :cond_1
    :goto_0
    return-void

    .line 7794
    :cond_2
    iget-object v0, p0, Lcom/osp/app/signin/mc;->c:Lcom/osp/app/signin/SignUpView;

    invoke-static {v0}, Lcom/osp/app/util/r;->p(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_4

    iget-object v0, p0, Lcom/osp/app/signin/mc;->c:Lcom/osp/app/signin/SignUpView;

    invoke-static {v0}, Lcom/osp/app/signin/SignUpView;->Y(Lcom/osp/app/signin/SignUpView;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_4

    iget-object v0, p0, Lcom/osp/app/signin/mc;->c:Lcom/osp/app/signin/SignUpView;

    invoke-static {v0}, Lcom/osp/app/signin/SignUpView;->p(Lcom/osp/app/signin/SignUpView;)Landroid/widget/AutoCompleteTextView;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/AutoCompleteTextView;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/osp/app/signin/mc;->c:Lcom/osp/app/signin/SignUpView;

    invoke-static {v1}, Lcom/osp/app/signin/SignUpView;->Y(Lcom/osp/app/signin/SignUpView;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_3

    iget-object v0, p0, Lcom/osp/app/signin/mc;->c:Lcom/osp/app/signin/SignUpView;

    invoke-static {v0}, Lcom/osp/app/signin/SignUpView;->p(Lcom/osp/app/signin/SignUpView;)Landroid/widget/AutoCompleteTextView;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/AutoCompleteTextView;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/osp/app/signin/mc;->c:Lcom/osp/app/signin/SignUpView;

    invoke-static {v1}, Lcom/osp/app/signin/SignUpView;->Z(Lcom/osp/app/signin/SignUpView;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 7797
    :cond_3
    invoke-direct {p0}, Lcom/osp/app/signin/mc;->i()V

    goto :goto_0

    .line 7800
    :cond_4
    iget-object v0, p0, Lcom/osp/app/signin/mc;->c:Lcom/osp/app/signin/SignUpView;

    invoke-static {v0}, Lcom/osp/app/signin/SignUpView;->aa(Lcom/osp/app/signin/SignUpView;)V

    goto :goto_0

    .line 7802
    :cond_5
    iget-object v0, p0, Lcom/osp/app/signin/mc;->e:Lcom/msc/sa/d/f;

    invoke-virtual {v0}, Lcom/msc/sa/d/f;->a()Z

    move-result v0

    if-nez v0, :cond_1

    .line 7804
    iget-object v0, p0, Lcom/osp/app/signin/mc;->e:Lcom/msc/sa/d/f;

    invoke-virtual {v0}, Lcom/msc/sa/d/f;->b()Z

    move-result v0

    if-eqz v0, :cond_9

    .line 7806
    iget-object v0, p0, Lcom/osp/app/signin/mc;->c:Lcom/osp/app/signin/SignUpView;

    invoke-static {v0}, Lcom/osp/app/signin/SignUpView;->ab(Lcom/osp/app/signin/SignUpView;)Landroid/app/AlertDialog;

    move-result-object v0

    if-eqz v0, :cond_7

    .line 7810
    :try_start_0
    iget-object v0, p0, Lcom/osp/app/signin/mc;->c:Lcom/osp/app/signin/SignUpView;

    invoke-static {v0}, Lcom/osp/app/signin/SignUpView;->ab(Lcom/osp/app/signin/SignUpView;)Landroid/app/AlertDialog;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/AlertDialog;->isShowing()Z

    move-result v0

    if-eqz v0, :cond_6

    .line 7812
    iget-object v0, p0, Lcom/osp/app/signin/mc;->c:Lcom/osp/app/signin/SignUpView;

    invoke-static {v0}, Lcom/osp/app/signin/SignUpView;->ab(Lcom/osp/app/signin/SignUpView;)Landroid/app/AlertDialog;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/AlertDialog;->dismiss()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 7819
    :cond_6
    iget-object v0, p0, Lcom/osp/app/signin/mc;->c:Lcom/osp/app/signin/SignUpView;

    :goto_1
    invoke-static {v0, v2}, Lcom/osp/app/signin/SignUpView;->e(Lcom/osp/app/signin/SignUpView;Landroid/app/AlertDialog;)Landroid/app/AlertDialog;

    .line 7823
    :cond_7
    iget-object v0, p0, Lcom/osp/app/signin/mc;->c:Lcom/osp/app/signin/SignUpView;

    invoke-static {v0}, Lcom/osp/app/signin/SignUpView;->ab(Lcom/osp/app/signin/SignUpView;)Landroid/app/AlertDialog;

    move-result-object v0

    if-nez v0, :cond_1

    .line 7825
    const-string v0, ""

    .line 7826
    iget-object v1, p0, Lcom/osp/app/signin/mc;->c:Lcom/osp/app/signin/SignUpView;

    invoke-static {v1}, Lcom/osp/app/signin/SignUpView;->B(Lcom/osp/app/signin/SignUpView;)Lcom/osp/app/signin/SignUpinfo;

    move-result-object v1

    if-eqz v1, :cond_8

    .line 7828
    iget-object v1, p0, Lcom/osp/app/signin/mc;->c:Lcom/osp/app/signin/SignUpView;

    invoke-static {v1}, Lcom/osp/app/signin/SignUpView;->B(Lcom/osp/app/signin/SignUpView;)Lcom/osp/app/signin/SignUpinfo;

    move-result-object v1

    invoke-virtual {v1}, Lcom/osp/app/signin/SignUpinfo;->e()Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_8

    .line 7830
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "+"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/osp/app/signin/mc;->c:Lcom/osp/app/signin/SignUpView;

    invoke-static {v1}, Lcom/osp/app/signin/SignUpView;->B(Lcom/osp/app/signin/SignUpView;)Lcom/osp/app/signin/SignUpinfo;

    move-result-object v1

    invoke-virtual {v1}, Lcom/osp/app/signin/SignUpinfo;->e()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "\r\n\r\n"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 7833
    :cond_8
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/osp/app/signin/mc;->c:Lcom/osp/app/signin/SignUpView;

    const v2, 0x7f09010c

    invoke-virtual {v1, v2}, Lcom/osp/app/signin/SignUpView;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 7836
    :try_start_1
    iget-object v1, p0, Lcom/osp/app/signin/mc;->c:Lcom/osp/app/signin/SignUpView;

    new-instance v2, Landroid/app/AlertDialog$Builder;

    iget-object v3, p0, Lcom/osp/app/signin/mc;->c:Lcom/osp/app/signin/SignUpView;

    invoke-direct {v2, v3}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    const v3, 0x7f090103

    invoke-virtual {v2, v3}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    move-result-object v2

    invoke-virtual {v2, v0}, Landroid/app/AlertDialog$Builder;->setMessage(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    const v2, 0x7f09010b

    new-instance v3, Lcom/osp/app/signin/md;

    invoke-direct {v3, p0}, Lcom/osp/app/signin/md;-><init>(Lcom/osp/app/signin/mc;)V

    invoke-virtual {v0, v2, v3}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->show()Landroid/app/AlertDialog;

    move-result-object v0

    invoke-static {v1, v0}, Lcom/osp/app/signin/SignUpView;->e(Lcom/osp/app/signin/SignUpView;Landroid/app/AlertDialog;)Landroid/app/AlertDialog;
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    goto/16 :goto_0

    .line 7851
    :catch_0
    move-exception v0

    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    goto/16 :goto_0

    .line 7814
    :catch_1
    move-exception v0

    :try_start_2
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 7819
    iget-object v0, p0, Lcom/osp/app/signin/mc;->c:Lcom/osp/app/signin/SignUpView;

    goto/16 :goto_1

    :catchall_0
    move-exception v0

    iget-object v1, p0, Lcom/osp/app/signin/mc;->c:Lcom/osp/app/signin/SignUpView;

    invoke-static {v1, v2}, Lcom/osp/app/signin/SignUpView;->e(Lcom/osp/app/signin/SignUpView;Landroid/app/AlertDialog;)Landroid/app/AlertDialog;

    throw v0

    .line 7858
    :cond_9
    iget-object v0, p0, Lcom/osp/app/signin/mc;->c:Lcom/osp/app/signin/SignUpView;

    invoke-static {v0}, Lcom/osp/app/signin/SignUpView;->ad(Lcom/osp/app/signin/SignUpView;)Landroid/app/AlertDialog;

    move-result-object v0

    if-eqz v0, :cond_b

    .line 7862
    :try_start_3
    iget-object v0, p0, Lcom/osp/app/signin/mc;->c:Lcom/osp/app/signin/SignUpView;

    invoke-static {v0}, Lcom/osp/app/signin/SignUpView;->ad(Lcom/osp/app/signin/SignUpView;)Landroid/app/AlertDialog;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/AlertDialog;->isShowing()Z

    move-result v0

    if-eqz v0, :cond_a

    .line 7864
    iget-object v0, p0, Lcom/osp/app/signin/mc;->c:Lcom/osp/app/signin/SignUpView;

    invoke-static {v0}, Lcom/osp/app/signin/SignUpView;->ad(Lcom/osp/app/signin/SignUpView;)Landroid/app/AlertDialog;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/AlertDialog;->dismiss()V
    :try_end_3
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    .line 7871
    :cond_a
    iget-object v0, p0, Lcom/osp/app/signin/mc;->c:Lcom/osp/app/signin/SignUpView;

    :goto_2
    invoke-static {v0, v2}, Lcom/osp/app/signin/SignUpView;->f(Lcom/osp/app/signin/SignUpView;Landroid/app/AlertDialog;)Landroid/app/AlertDialog;

    .line 7875
    :cond_b
    iget-object v0, p0, Lcom/osp/app/signin/mc;->c:Lcom/osp/app/signin/SignUpView;

    invoke-static {v0}, Lcom/osp/app/signin/SignUpView;->ad(Lcom/osp/app/signin/SignUpView;)Landroid/app/AlertDialog;

    move-result-object v0

    if-nez v0, :cond_1

    .line 7877
    const-string v0, ""

    .line 7878
    iget-object v1, p0, Lcom/osp/app/signin/mc;->c:Lcom/osp/app/signin/SignUpView;

    invoke-static {v1}, Lcom/osp/app/signin/SignUpView;->B(Lcom/osp/app/signin/SignUpView;)Lcom/osp/app/signin/SignUpinfo;

    move-result-object v1

    if-eqz v1, :cond_c

    .line 7880
    iget-object v1, p0, Lcom/osp/app/signin/mc;->c:Lcom/osp/app/signin/SignUpView;

    invoke-static {v1}, Lcom/osp/app/signin/SignUpView;->B(Lcom/osp/app/signin/SignUpView;)Lcom/osp/app/signin/SignUpinfo;

    move-result-object v1

    invoke-virtual {v1}, Lcom/osp/app/signin/SignUpinfo;->e()Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_c

    .line 7882
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "+"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/osp/app/signin/mc;->c:Lcom/osp/app/signin/SignUpView;

    invoke-static {v1}, Lcom/osp/app/signin/SignUpView;->B(Lcom/osp/app/signin/SignUpView;)Lcom/osp/app/signin/SignUpinfo;

    move-result-object v1

    invoke-virtual {v1}, Lcom/osp/app/signin/SignUpinfo;->e()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "\r\n\r\n"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 7885
    :cond_c
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/osp/app/signin/mc;->c:Lcom/osp/app/signin/SignUpView;

    const v2, 0x7f090140

    invoke-virtual {v1, v2}, Lcom/osp/app/signin/SignUpView;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 7888
    :try_start_4
    iget-object v1, p0, Lcom/osp/app/signin/mc;->c:Lcom/osp/app/signin/SignUpView;

    new-instance v2, Landroid/app/AlertDialog$Builder;

    iget-object v3, p0, Lcom/osp/app/signin/mc;->c:Lcom/osp/app/signin/SignUpView;

    invoke-direct {v2, v3}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    const v3, 0x7f090109

    invoke-virtual {v2, v3}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    move-result-object v2

    invoke-virtual {v2, v0}, Landroid/app/AlertDialog$Builder;->setMessage(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    const v2, 0x7f09005c

    new-instance v3, Lcom/osp/app/signin/mg;

    invoke-direct {v3, p0}, Lcom/osp/app/signin/mg;-><init>(Lcom/osp/app/signin/mc;)V

    invoke-virtual {v0, v2, v3}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    const v2, 0x7f090138

    new-instance v3, Lcom/osp/app/signin/mf;

    invoke-direct {v3, p0}, Lcom/osp/app/signin/mf;-><init>(Lcom/osp/app/signin/mc;)V

    invoke-virtual {v0, v2, v3}, Landroid/app/AlertDialog$Builder;->setNeutralButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    const v2, 0x7f090041

    new-instance v3, Lcom/osp/app/signin/me;

    invoke-direct {v3, p0}, Lcom/osp/app/signin/me;-><init>(Lcom/osp/app/signin/mc;)V

    invoke-virtual {v0, v2, v3}, Landroid/app/AlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->show()Landroid/app/AlertDialog;

    move-result-object v0

    invoke-static {v1, v0}, Lcom/osp/app/signin/SignUpView;->f(Lcom/osp/app/signin/SignUpView;Landroid/app/AlertDialog;)Landroid/app/AlertDialog;
    :try_end_4
    .catch Ljava/lang/Exception; {:try_start_4 .. :try_end_4} :catch_2

    goto/16 :goto_0

    .line 7889
    :catch_2
    move-exception v0

    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    goto/16 :goto_0

    .line 7866
    :catch_3
    move-exception v0

    :try_start_5
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_1

    .line 7871
    iget-object v0, p0, Lcom/osp/app/signin/mc;->c:Lcom/osp/app/signin/SignUpView;

    goto/16 :goto_2

    :catchall_1
    move-exception v0

    iget-object v1, p0, Lcom/osp/app/signin/mc;->c:Lcom/osp/app/signin/SignUpView;

    invoke-static {v1, v2}, Lcom/osp/app/signin/SignUpView;->f(Lcom/osp/app/signin/SignUpView;Landroid/app/AlertDialog;)Landroid/app/AlertDialog;

    throw v0
.end method

.method public final b(Lcom/msc/b/c;)V
    .locals 0

    .prologue
    .line 8104
    invoke-super {p0, p1}, Lcom/msc/c/b;->b(Lcom/msc/b/c;)V

    .line 8106
    return-void
.end method

.method protected final synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 7763
    invoke-virtual {p0}, Lcom/osp/app/signin/mc;->a()Ljava/lang/Boolean;

    move-result-object v0

    return-object v0
.end method

.method public final h()V
    .locals 4

    .prologue
    const/4 v2, 0x0

    .line 7975
    iget-object v0, p0, Lcom/osp/app/signin/mc;->c:Lcom/osp/app/signin/SignUpView;

    invoke-static {v0}, Lcom/osp/app/signin/SignUpView;->ae(Lcom/osp/app/signin/SignUpView;)Landroid/app/AlertDialog;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 7979
    :try_start_0
    iget-object v0, p0, Lcom/osp/app/signin/mc;->c:Lcom/osp/app/signin/SignUpView;

    invoke-static {v0}, Lcom/osp/app/signin/SignUpView;->ae(Lcom/osp/app/signin/SignUpView;)Landroid/app/AlertDialog;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/AlertDialog;->isShowing()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 7981
    iget-object v0, p0, Lcom/osp/app/signin/mc;->c:Lcom/osp/app/signin/SignUpView;

    invoke-static {v0}, Lcom/osp/app/signin/SignUpView;->ae(Lcom/osp/app/signin/SignUpView;)Landroid/app/AlertDialog;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/AlertDialog;->dismiss()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 7988
    :cond_0
    iget-object v0, p0, Lcom/osp/app/signin/mc;->c:Lcom/osp/app/signin/SignUpView;

    :goto_0
    invoke-static {v0, v2}, Lcom/osp/app/signin/SignUpView;->g(Lcom/osp/app/signin/SignUpView;Landroid/app/AlertDialog;)Landroid/app/AlertDialog;

    .line 7991
    :cond_1
    iget-object v0, p0, Lcom/osp/app/signin/mc;->c:Lcom/osp/app/signin/SignUpView;

    invoke-static {v0}, Lcom/osp/app/signin/SignUpView;->ae(Lcom/osp/app/signin/SignUpView;)Landroid/app/AlertDialog;

    move-result-object v0

    if-nez v0, :cond_3

    .line 7993
    const-string v0, ""

    .line 7994
    iget-object v1, p0, Lcom/osp/app/signin/mc;->c:Lcom/osp/app/signin/SignUpView;

    invoke-static {v1}, Lcom/osp/app/signin/SignUpView;->B(Lcom/osp/app/signin/SignUpView;)Lcom/osp/app/signin/SignUpinfo;

    move-result-object v1

    if-eqz v1, :cond_2

    .line 7996
    iget-object v1, p0, Lcom/osp/app/signin/mc;->c:Lcom/osp/app/signin/SignUpView;

    invoke-static {v1}, Lcom/osp/app/signin/SignUpView;->B(Lcom/osp/app/signin/SignUpView;)Lcom/osp/app/signin/SignUpinfo;

    move-result-object v1

    invoke-virtual {v1}, Lcom/osp/app/signin/SignUpinfo;->e()Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_2

    .line 7998
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "+"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/osp/app/signin/mc;->c:Lcom/osp/app/signin/SignUpView;

    invoke-static {v1}, Lcom/osp/app/signin/SignUpView;->B(Lcom/osp/app/signin/SignUpView;)Lcom/osp/app/signin/SignUpinfo;

    move-result-object v1

    invoke-virtual {v1}, Lcom/osp/app/signin/SignUpinfo;->e()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "\r\n\r\n"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 8001
    :cond_2
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/osp/app/signin/mc;->c:Lcom/osp/app/signin/SignUpView;

    const v2, 0x7f090141

    invoke-virtual {v1, v2}, Lcom/osp/app/signin/SignUpView;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 8004
    :try_start_1
    iget-object v1, p0, Lcom/osp/app/signin/mc;->c:Lcom/osp/app/signin/SignUpView;

    new-instance v2, Landroid/app/AlertDialog$Builder;

    iget-object v3, p0, Lcom/osp/app/signin/mc;->c:Lcom/osp/app/signin/SignUpView;

    invoke-direct {v2, v3}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    const v3, 0x7f090109

    invoke-virtual {v2, v3}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    move-result-object v2

    invoke-virtual {v2, v0}, Landroid/app/AlertDialog$Builder;->setMessage(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    const v2, 0x7f090064

    new-instance v3, Lcom/osp/app/signin/mh;

    invoke-direct {v3, p0}, Lcom/osp/app/signin/mh;-><init>(Lcom/osp/app/signin/mc;)V

    invoke-virtual {v0, v2, v3}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->show()Landroid/app/AlertDialog;

    move-result-object v0

    invoke-static {v1, v0}, Lcom/osp/app/signin/SignUpView;->g(Lcom/osp/app/signin/SignUpView;Landroid/app/AlertDialog;)Landroid/app/AlertDialog;
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1

    .line 8018
    :cond_3
    :goto_1
    return-void

    .line 7983
    :catch_0
    move-exception v0

    :try_start_2
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 7988
    iget-object v0, p0, Lcom/osp/app/signin/mc;->c:Lcom/osp/app/signin/SignUpView;

    goto/16 :goto_0

    :catchall_0
    move-exception v0

    iget-object v1, p0, Lcom/osp/app/signin/mc;->c:Lcom/osp/app/signin/SignUpView;

    invoke-static {v1, v2}, Lcom/osp/app/signin/SignUpView;->g(Lcom/osp/app/signin/SignUpView;Landroid/app/AlertDialog;)Landroid/app/AlertDialog;

    throw v0

    .line 8012
    :catch_1
    move-exception v0

    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_1
.end method

.method protected final synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 7763
    check-cast p1, Ljava/lang/Boolean;

    invoke-virtual {p0, p1}, Lcom/osp/app/signin/mc;->a(Ljava/lang/Boolean;)V

    return-void
.end method
