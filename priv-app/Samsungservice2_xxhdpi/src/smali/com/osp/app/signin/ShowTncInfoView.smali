.class public Lcom/osp/app/signin/ShowTncInfoView;
.super Lcom/osp/app/util/BaseActivity;
.source "ShowTncInfoView.java"


# annotations
.annotation runtime Ljava/lang/Deprecated;
.end annotation


# instance fields
.field private A:Ljava/lang/String;

.field private B:Ljava/lang/String;

.field a:Lcom/osp/app/signin/hs;

.field b:Lcom/osp/app/signin/ht;

.field private final c:Ljava/lang/String;

.field private d:Landroid/widget/TextView;

.field private e:Landroid/widget/TextView;

.field private f:Landroid/widget/LinearLayout;

.field private y:Ljava/lang/String;

.field private z:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 33
    invoke-direct {p0}, Lcom/osp/app/util/BaseActivity;-><init>()V

    .line 35
    const-string v0, "ShowTncInfoView"

    iput-object v0, p0, Lcom/osp/app/signin/ShowTncInfoView;->c:Ljava/lang/String;

    .line 66
    const-string v0, ""

    iput-object v0, p0, Lcom/osp/app/signin/ShowTncInfoView;->A:Ljava/lang/String;

    .line 71
    const-string v0, ""

    iput-object v0, p0, Lcom/osp/app/signin/ShowTncInfoView;->B:Ljava/lang/String;

    .line 419
    return-void
.end method

.method static synthetic a(Lcom/osp/app/signin/ShowTncInfoView;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 33
    iget-object v0, p0, Lcom/osp/app/signin/ShowTncInfoView;->z:Ljava/lang/String;

    return-object v0
.end method

.method private a(I)V
    .locals 4

    .prologue
    const v3, 0x7f08011e

    .line 222
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "ShowTncInfoNiew::initLayoutParams orientation : "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;)V

    .line 224
    invoke-static {}, Lcom/osp/device/b;->b()Lcom/osp/device/b;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/osp/device/b;->i(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 226
    invoke-virtual {p0}, Lcom/osp/app/signin/ShowTncInfoView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f08011f

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v0

    float-to-int v0, v0

    .line 227
    invoke-virtual {p0}, Lcom/osp/app/signin/ShowTncInfoView;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1, v3}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v1

    float-to-int v1, v1

    .line 228
    invoke-static {}, Lcom/osp/app/signin/SamsungService;->a()Z

    move-result v2

    if-nez v2, :cond_0

    .line 230
    invoke-virtual {p0}, Lcom/osp/app/signin/ShowTncInfoView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0, v3}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v0

    float-to-int v0, v0

    .line 233
    :cond_0
    const v2, 0x7f0c0175

    invoke-virtual {p0, v2}, Lcom/osp/app/signin/ShowTncInfoView;->findViewById(I)Landroid/view/View;

    move-result-object v2

    .line 234
    invoke-static {}, Lcom/osp/app/util/q;->a()Lcom/osp/app/util/q;

    invoke-static {v2, v0, v1, p1}, Lcom/osp/app/util/q;->b(Landroid/view/View;III)V

    .line 236
    :cond_1
    return-void
.end method

.method static synthetic b(Lcom/osp/app/signin/ShowTncInfoView;)Landroid/widget/TextView;
    .locals 1

    .prologue
    .line 33
    iget-object v0, p0, Lcom/osp/app/signin/ShowTncInfoView;->d:Landroid/widget/TextView;

    return-object v0
.end method

.method static synthetic c(Lcom/osp/app/signin/ShowTncInfoView;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 33
    iget-object v0, p0, Lcom/osp/app/signin/ShowTncInfoView;->A:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic d(Lcom/osp/app/signin/ShowTncInfoView;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 33
    iget-object v0, p0, Lcom/osp/app/signin/ShowTncInfoView;->y:Ljava/lang/String;

    return-object v0
.end method


# virtual methods
.method protected final a()V
    .locals 0

    .prologue
    .line 241
    return-void
.end method

.method protected final b()V
    .locals 0

    .prologue
    .line 246
    return-void
.end method

.method public onConfigurationChanged(Landroid/content/res/Configuration;)V
    .locals 1

    .prologue
    .line 211
    iget v0, p1, Landroid/content/res/Configuration;->orientation:I

    invoke-direct {p0, v0}, Lcom/osp/app/signin/ShowTncInfoView;->a(I)V

    .line 212
    invoke-super {p0, p1}, Lcom/osp/app/util/BaseActivity;->onConfigurationChanged(Landroid/content/res/Configuration;)V

    .line 213
    return-void
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 3

    .prologue
    .line 85
    invoke-super {p0, p1}, Lcom/osp/app/util/BaseActivity;->onCreate(Landroid/os/Bundle;)V

    .line 87
    const v0, 0x7f03002d

    invoke-static {p0}, Lcom/osp/app/util/r;->q(Landroid/content/Context;)Z

    move-result v1

    invoke-virtual {p0, v0, v1}, Lcom/osp/app/signin/ShowTncInfoView;->a(IZ)V

    .line 89
    invoke-virtual {p0}, Lcom/osp/app/signin/ShowTncInfoView;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string v1, "country_code_mcc"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/osp/app/signin/ShowTncInfoView;->y:Ljava/lang/String;

    const-string v1, "terms_type"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/osp/app/signin/ShowTncInfoView;->z:Ljava/lang/String;

    iget-object v1, p0, Lcom/osp/app/signin/ShowTncInfoView;->z:Ljava/lang/String;

    if-eqz v1, :cond_8

    iget-object v1, p0, Lcom/osp/app/signin/ShowTncInfoView;->z:Ljava/lang/String;

    const-string v2, "svctnc"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_4

    const-string v1, "Title"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/osp/app/signin/ShowTncInfoView;->B:Ljava/lang/String;

    const-string v1, "FileName"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/osp/app/signin/ShowTncInfoView;->A:Ljava/lang/String;

    .line 100
    :goto_0
    invoke-static {p0}, Lcom/msc/sa/c/d;->a(Landroid/app/Activity;)V

    .line 102
    iget-object v0, p0, Lcom/osp/app/signin/ShowTncInfoView;->f:Landroid/widget/LinearLayout;

    if-nez v0, :cond_0

    const v0, 0x7f0c0089

    invoke-virtual {p0, v0}, Lcom/osp/app/signin/ShowTncInfoView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcom/osp/app/signin/ShowTncInfoView;->f:Landroid/widget/LinearLayout;

    :cond_0
    iget-object v0, p0, Lcom/osp/app/signin/ShowTncInfoView;->e:Landroid/widget/TextView;

    if-nez v0, :cond_1

    const v0, 0x7f0c011b

    invoke-virtual {p0, v0}, Lcom/osp/app/signin/ShowTncInfoView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/osp/app/signin/ShowTncInfoView;->e:Landroid/widget/TextView;

    iget-object v0, p0, Lcom/osp/app/signin/ShowTncInfoView;->e:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/osp/app/signin/ShowTncInfoView;->B:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    :cond_1
    iget-object v0, p0, Lcom/osp/app/signin/ShowTncInfoView;->d:Landroid/widget/TextView;

    if-nez v0, :cond_2

    const v0, 0x7f0c011c

    invoke-virtual {p0, v0}, Lcom/osp/app/signin/ShowTncInfoView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/osp/app/signin/ShowTncInfoView;->d:Landroid/widget/TextView;

    .line 106
    :cond_2
    invoke-virtual {p0}, Lcom/osp/app/signin/ShowTncInfoView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v0

    iget v0, v0, Landroid/content/res/Configuration;->orientation:I

    invoke-direct {p0, v0}, Lcom/osp/app/signin/ShowTncInfoView;->a(I)V

    .line 114
    iget-object v0, p0, Lcom/osp/app/signin/ShowTncInfoView;->z:Ljava/lang/String;

    const-string v1, "svctnc"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_3

    iget-object v0, p0, Lcom/osp/app/signin/ShowTncInfoView;->z:Ljava/lang/String;

    const-string v1, "tnc"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_9

    .line 116
    :cond_3
    new-instance v0, Lcom/osp/app/signin/hs;

    invoke-direct {v0, p0}, Lcom/osp/app/signin/hs;-><init>(Lcom/osp/app/signin/ShowTncInfoView;)V

    iput-object v0, p0, Lcom/osp/app/signin/ShowTncInfoView;->a:Lcom/osp/app/signin/hs;

    .line 117
    iget-object v0, p0, Lcom/osp/app/signin/ShowTncInfoView;->a:Lcom/osp/app/signin/hs;

    invoke-virtual {v0}, Lcom/osp/app/signin/hs;->b()V

    .line 124
    :goto_1
    return-void

    .line 89
    :cond_4
    iget-object v1, p0, Lcom/osp/app/signin/ShowTncInfoView;->z:Ljava/lang/String;

    const-string v2, "tnc"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_5

    const v1, 0x7f090059

    invoke-virtual {p0, v1}, Lcom/osp/app/signin/ShowTncInfoView;->getString(I)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/osp/app/signin/ShowTncInfoView;->B:Ljava/lang/String;

    const-string v1, "FileName"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/osp/app/signin/ShowTncInfoView;->A:Ljava/lang/String;

    goto :goto_0

    :cond_5
    iget-object v0, p0, Lcom/osp/app/signin/ShowTncInfoView;->z:Ljava/lang/String;

    const-string v1, "pp"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_6

    const v0, 0x7f090068

    invoke-virtual {p0, v0}, Lcom/osp/app/signin/ShowTncInfoView;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/osp/app/signin/ShowTncInfoView;->B:Ljava/lang/String;

    goto/16 :goto_0

    :cond_6
    iget-object v0, p0, Lcom/osp/app/signin/ShowTncInfoView;->z:Ljava/lang/String;

    const-string v1, "pdu"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_7

    const v0, 0x7f09001c

    invoke-virtual {p0, v0}, Lcom/osp/app/signin/ShowTncInfoView;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/osp/app/signin/ShowTncInfoView;->B:Ljava/lang/String;

    goto/16 :goto_0

    :cond_7
    invoke-virtual {p0}, Lcom/osp/app/signin/ShowTncInfoView;->finish()V

    goto/16 :goto_0

    :cond_8
    invoke-virtual {p0}, Lcom/osp/app/signin/ShowTncInfoView;->finish()V

    goto/16 :goto_0

    .line 120
    :cond_9
    new-instance v0, Lcom/osp/app/signin/ht;

    invoke-direct {v0, p0}, Lcom/osp/app/signin/ht;-><init>(Lcom/osp/app/signin/ShowTncInfoView;)V

    iput-object v0, p0, Lcom/osp/app/signin/ShowTncInfoView;->b:Lcom/osp/app/signin/ht;

    .line 121
    iget-object v0, p0, Lcom/osp/app/signin/ShowTncInfoView;->b:Lcom/osp/app/signin/ht;

    invoke-virtual {v0}, Lcom/osp/app/signin/ht;->b()V

    goto :goto_1
.end method

.method public onOptionsItemSelected(Landroid/view/MenuItem;)Z
    .locals 2

    .prologue
    .line 128
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0xb

    if-lt v0, v1, :cond_0

    .line 130
    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    .line 134
    :cond_0
    :goto_0
    invoke-super {p0, p1}, Lcom/osp/app/util/BaseActivity;->onOptionsItemSelected(Landroid/view/MenuItem;)Z

    move-result v0

    return v0

    .line 133
    :pswitch_0
    invoke-virtual {p0}, Lcom/osp/app/signin/ShowTncInfoView;->onBackPressed()V

    goto :goto_0

    .line 130
    :pswitch_data_0
    .packed-switch 0x102002c
        :pswitch_0
    .end packed-switch
.end method
