.class final Lcom/osp/app/signin/bi;
.super Landroid/support/v4/view/aa;
.source "AccountView.java"


# instance fields
.field final synthetic a:Lcom/osp/app/signin/AccountView;


# direct methods
.method public constructor <init>(Lcom/osp/app/signin/AccountView;)V
    .locals 0

    .prologue
    .line 7845
    iput-object p1, p0, Lcom/osp/app/signin/bi;->a:Lcom/osp/app/signin/AccountView;

    .line 7847
    invoke-direct {p0}, Landroid/support/v4/view/aa;-><init>()V

    .line 7848
    return-void
.end method


# virtual methods
.method public final a()I
    .locals 2

    .prologue
    .line 7852
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "<< mTotalPageCnt = "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v1, p0, Lcom/osp/app/signin/bi;->a:Lcom/osp/app/signin/AccountView;

    invoke-static {v1}, Lcom/osp/app/signin/AccountView;->P(Lcom/osp/app/signin/AccountView;)I

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " >>"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/osp/app/util/aq;->c(Ljava/lang/String;)V

    .line 7853
    iget-object v0, p0, Lcom/osp/app/signin/bi;->a:Lcom/osp/app/signin/AccountView;

    invoke-static {v0}, Lcom/osp/app/signin/AccountView;->P(Lcom/osp/app/signin/AccountView;)I

    move-result v0

    return v0
.end method

.method public final a(I)Ljava/lang/Object;
    .locals 3

    .prologue
    .line 7859
    iget-object v0, p0, Lcom/osp/app/signin/bi;->a:Lcom/osp/app/signin/AccountView;

    invoke-static {v0, p1}, Lcom/osp/app/signin/AccountView;->b(Lcom/osp/app/signin/AccountView;I)Landroid/view/View;

    move-result-object v0

    .line 7860
    iget-object v1, p0, Lcom/osp/app/signin/bi;->a:Lcom/osp/app/signin/AccountView;

    invoke-static {v1}, Lcom/osp/app/signin/AccountView;->Q(Lcom/osp/app/signin/AccountView;)Landroid/support/v4/view/ViewPager;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v1, v0, v2}, Landroid/support/v4/view/ViewPager;->addView(Landroid/view/View;I)V

    .line 7862
    return-object v0
.end method

.method public final a(Ljava/lang/Object;)V
    .locals 1

    .prologue
    .line 7868
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v0, "<< destroyItem() >>"

    invoke-static {v0}, Lcom/osp/app/util/aq;->c(Ljava/lang/String;)V

    .line 7869
    iget-object v0, p0, Lcom/osp/app/signin/bi;->a:Lcom/osp/app/signin/AccountView;

    invoke-static {v0}, Lcom/osp/app/signin/AccountView;->Q(Lcom/osp/app/signin/AccountView;)Landroid/support/v4/view/ViewPager;

    move-result-object v0

    check-cast p1, Landroid/view/View;

    invoke-virtual {v0, p1}, Landroid/support/v4/view/ViewPager;->removeView(Landroid/view/View;)V

    .line 7871
    return-void
.end method

.method public final a(Landroid/view/View;Ljava/lang/Object;)Z
    .locals 1

    .prologue
    .line 7875
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v0, "<< isViewFromObject() >>"

    invoke-static {v0}, Lcom/osp/app/util/aq;->c(Ljava/lang/String;)V

    .line 7876
    invoke-virtual {p1, p2}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method
