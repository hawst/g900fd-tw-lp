.class public Lcom/osp/app/signin/AccountinfoView;
.super Lcom/osp/app/util/BaseActivity;
.source "AccountinfoView.java"

# interfaces
.implements Landroid/view/View$OnClickListener;
.implements Landroid/view/View$OnFocusChangeListener;


# instance fields
.field private A:Lcom/osp/app/signin/cb;

.field private B:Ljava/lang/String;

.field private C:I

.field private D:I

.field private E:I

.field private F:Landroid/app/AlertDialog;

.field private G:Landroid/app/AlertDialog;

.field private H:Landroid/view/View;

.field private I:Landroid/widget/Button;

.field private J:Landroid/widget/CheckBox;

.field private K:Ljava/lang/String;

.field private L:Ljava/lang/String;

.field private M:Landroid/app/Dialog;

.field private final N:Landroid/app/DatePickerDialog$OnDateSetListener;

.field private O:I

.field private P:Z

.field private Q:Z

.field private R:Landroid/widget/LinearLayout;

.field private S:Landroid/widget/LinearLayout;

.field private T:Landroid/widget/LinearLayout;

.field private U:Landroid/view/ViewGroup;

.field private V:Lcom/osp/app/util/n;

.field private W:Landroid/widget/LinearLayout;

.field private X:Landroid/widget/ImageView;

.field private Y:Landroid/widget/TextView;

.field private Z:Landroid/widget/EditText;

.field private a:Landroid/widget/Button;

.field private aA:Landroid/widget/LinearLayout;

.field private aB:Ljava/lang/String;

.field private aC:Landroid/widget/TextView;

.field private aD:Landroid/widget/TextView;

.field private aE:Landroid/widget/ImageView;

.field private aF:Landroid/widget/TextView;

.field private aG:Ljava/lang/String;

.field private aH:I

.field private aI:I

.field private aJ:J

.field private aK:Ljava/lang/String;

.field private aL:Z

.field private aM:Z

.field private aN:Z

.field private aO:Ljava/util/ArrayList;

.field private aP:Z

.field private final aQ:Landroid/view/View$OnKeyListener;

.field private aa:Landroid/widget/EditText;

.field private ab:Landroid/widget/EditText;

.field private ac:Landroid/widget/EditText;

.field private ad:Landroid/widget/EditText;

.field private ae:Landroid/widget/EditText;

.field private af:Landroid/widget/EditText;

.field private ag:Landroid/widget/EditText;

.field private ah:Landroid/widget/EditText;

.field private ai:Landroid/widget/EditText;

.field private aj:Landroid/widget/EditText;

.field private ak:Landroid/widget/EditText;

.field private al:Lcom/osp/app/signin/kd;

.field private am:Landroid/view/LayoutInflater;

.field private an:Landroid/widget/LinearLayout;

.field private ao:Landroid/widget/LinearLayout;

.field private ap:Landroid/content/Intent;

.field private aq:Z

.field private ar:Z

.field private as:I

.field private at:Ljava/lang/String;

.field private au:Ljava/util/ArrayList;

.field private av:I

.field private aw:Landroid/widget/Button;

.field private ax:Landroid/widget/Button;

.field private ay:Landroid/widget/TextView;

.field private az:Landroid/widget/ImageView;

.field private final b:Landroid/view/View$OnClickListener;

.field private final c:Landroid/view/View$OnClickListener;

.field private d:Lcom/osp/app/signin/SignUpinfo;

.field private e:Ljava/lang/String;

.field private f:Lcom/osp/app/signin/bz;

.field private y:Lcom/osp/app/signin/ca;

.field private z:Lcom/osp/app/signin/cc;


# direct methods
.method public constructor <init>()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x0

    .line 96
    invoke-direct {p0}, Lcom/osp/app/util/BaseActivity;-><init>()V

    .line 112
    new-instance v0, Lcom/osp/app/signin/bj;

    invoke-direct {v0, p0}, Lcom/osp/app/signin/bj;-><init>(Lcom/osp/app/signin/AccountinfoView;)V

    iput-object v0, p0, Lcom/osp/app/signin/AccountinfoView;->b:Landroid/view/View$OnClickListener;

    .line 122
    new-instance v0, Lcom/osp/app/signin/bq;

    invoke-direct {v0, p0}, Lcom/osp/app/signin/bq;-><init>(Lcom/osp/app/signin/AccountinfoView;)V

    iput-object v0, p0, Lcom/osp/app/signin/AccountinfoView;->c:Landroid/view/View$OnClickListener;

    .line 131
    iput-object v1, p0, Lcom/osp/app/signin/AccountinfoView;->d:Lcom/osp/app/signin/SignUpinfo;

    .line 132
    iput-object v1, p0, Lcom/osp/app/signin/AccountinfoView;->e:Ljava/lang/String;

    .line 138
    iput-object v1, p0, Lcom/osp/app/signin/AccountinfoView;->B:Ljava/lang/String;

    .line 149
    iput-object v1, p0, Lcom/osp/app/signin/AccountinfoView;->K:Ljava/lang/String;

    .line 150
    iput-object v1, p0, Lcom/osp/app/signin/AccountinfoView;->L:Ljava/lang/String;

    .line 152
    new-instance v0, Lcom/osp/app/signin/br;

    invoke-direct {v0, p0}, Lcom/osp/app/signin/br;-><init>(Lcom/osp/app/signin/AccountinfoView;)V

    iput-object v0, p0, Lcom/osp/app/signin/AccountinfoView;->N:Landroid/app/DatePickerDialog$OnDateSetListener;

    .line 166
    iput-boolean v2, p0, Lcom/osp/app/signin/AccountinfoView;->P:Z

    .line 167
    iput-boolean v2, p0, Lcom/osp/app/signin/AccountinfoView;->Q:Z

    .line 208
    iput-object v1, p0, Lcom/osp/app/signin/AccountinfoView;->al:Lcom/osp/app/signin/kd;

    .line 219
    iput-object v1, p0, Lcom/osp/app/signin/AccountinfoView;->ap:Landroid/content/Intent;

    .line 221
    iput-boolean v2, p0, Lcom/osp/app/signin/AccountinfoView;->aq:Z

    .line 223
    iput-boolean v2, p0, Lcom/osp/app/signin/AccountinfoView;->ar:Z

    .line 228
    iput-object v1, p0, Lcom/osp/app/signin/AccountinfoView;->at:Ljava/lang/String;

    .line 257
    iput-object v1, p0, Lcom/osp/app/signin/AccountinfoView;->aB:Ljava/lang/String;

    .line 266
    iput-object v1, p0, Lcom/osp/app/signin/AccountinfoView;->aG:Ljava/lang/String;

    .line 274
    iput-object v1, p0, Lcom/osp/app/signin/AccountinfoView;->aK:Ljava/lang/String;

    .line 279
    iput-boolean v2, p0, Lcom/osp/app/signin/AccountinfoView;->aN:Z

    .line 285
    iput-boolean v2, p0, Lcom/osp/app/signin/AccountinfoView;->aP:Z

    .line 287
    new-instance v0, Lcom/osp/app/signin/bs;

    invoke-direct {v0, p0}, Lcom/osp/app/signin/bs;-><init>(Lcom/osp/app/signin/AccountinfoView;)V

    iput-object v0, p0, Lcom/osp/app/signin/AccountinfoView;->aQ:Landroid/view/View$OnKeyListener;

    .line 4608
    return-void
.end method

.method static synthetic A(Lcom/osp/app/signin/AccountinfoView;)I
    .locals 1

    .prologue
    .line 96
    iget v0, p0, Lcom/osp/app/signin/AccountinfoView;->aI:I

    xor-int/lit8 v0, v0, 0x10

    iput v0, p0, Lcom/osp/app/signin/AccountinfoView;->aI:I

    return v0
.end method

.method static synthetic B(Lcom/osp/app/signin/AccountinfoView;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 96
    iget-object v0, p0, Lcom/osp/app/signin/AccountinfoView;->L:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic C(Lcom/osp/app/signin/AccountinfoView;)Z
    .locals 1

    .prologue
    .line 96
    iget-boolean v0, p0, Lcom/osp/app/signin/AccountinfoView;->aM:Z

    return v0
.end method

.method static synthetic D(Lcom/osp/app/signin/AccountinfoView;)Z
    .locals 1

    .prologue
    .line 96
    iget-boolean v0, p0, Lcom/osp/app/signin/AccountinfoView;->aL:Z

    return v0
.end method

.method static synthetic a(Lcom/osp/app/signin/AccountinfoView;I)I
    .locals 0

    .prologue
    .line 96
    iput p1, p0, Lcom/osp/app/signin/AccountinfoView;->aH:I

    return p1
.end method

.method static synthetic a(Lcom/osp/app/signin/AccountinfoView;Landroid/app/AlertDialog;)Landroid/app/AlertDialog;
    .locals 0

    .prologue
    .line 96
    iput-object p1, p0, Lcom/osp/app/signin/AccountinfoView;->F:Landroid/app/AlertDialog;

    return-object p1
.end method

.method private a(Landroid/content/Intent;)Landroid/content/Intent;
    .locals 8

    .prologue
    const/4 v2, 0x0

    .line 2187
    invoke-static {p0}, Lcom/osp/app/util/ad;->h(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v4

    .line 2190
    invoke-static {p0}, Lcom/msc/c/e;->c(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v5

    .line 2196
    :try_start_0
    invoke-static {}, Lcom/msc/c/a;->a()Lcom/msc/c/a;

    move-result-object v0

    invoke-static {}, Lcom/msc/c/a;->a()Lcom/msc/c/a;

    move-result-object v1

    invoke-virtual {v1}, Lcom/msc/c/a;->b()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1, v4}, Lcom/msc/c/a;->b(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    .line 2197
    :try_start_1
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v0, "AIV"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-static {}, Lcom/osp/app/util/v;->a()Lcom/osp/app/util/v;

    move-result-object v6

    const-string v7, "EmailID"

    invoke-virtual {v6, v7}, Lcom/osp/app/util/v;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v3, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v6, " encrypted."

    invoke-virtual {v3, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v0, v3}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_4

    move-object v3, v1

    .line 2208
    :goto_0
    :try_start_2
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v0, "AIV"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-static {}, Lcom/osp/app/util/v;->a()Lcom/osp/app/util/v;

    move-result-object v6

    const-string v7, "Password"

    invoke-virtual {v6, v7}, Lcom/osp/app/util/v;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v1, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v6, " encrypted."

    invoke-virtual {v1, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_1

    .line 2216
    :goto_1
    :try_start_3
    invoke-static {}, Lcom/msc/c/a;->a()Lcom/msc/c/a;

    move-result-object v0

    invoke-static {}, Lcom/msc/c/a;->a()Lcom/msc/c/a;

    move-result-object v1

    invoke-virtual {v1}, Lcom/msc/c/a;->b()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1, v5}, Lcom/msc/c/a;->b(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    :try_end_3
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_2

    move-result-object v0

    .line 2217
    :try_start_4
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v1, "AIV"

    const-string v2, "external_b encrypted."

    invoke-static {v1, v2}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_4
    .catch Ljava/lang/Exception; {:try_start_4 .. :try_end_4} :catch_3

    .line 2223
    :goto_2
    const-string v1, "BG_mode"

    iget-object v2, p0, Lcom/osp/app/signin/AccountinfoView;->aG:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 2225
    const-string v1, "bg_result"

    const/4 v2, -0x1

    invoke-virtual {p1, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 2226
    const-string v1, "apps_id"

    invoke-virtual {p1, v1, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 2231
    const-string v1, "apps_birthdate"

    invoke-virtual {p1, v1, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 2232
    iget-object v1, p0, Lcom/osp/app/signin/AccountinfoView;->aB:Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 2234
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v2, p0, Lcom/osp/app/signin/AccountinfoView;->aB:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ":"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    invoke-virtual {p1, v1}, Landroid/content/Intent;->setData(Landroid/net/Uri;)Landroid/content/Intent;

    .line 2246
    :cond_0
    :goto_3
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "AccountinfoView::SamsungAppsData id : "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;)V

    .line 2249
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "AccountinfoView::SamsungAppsData birthDate : "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;)V

    .line 2250
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "AccountinfoView::SamsungAppsData encID : "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;)V

    .line 2253
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "AccountinfoView::SamsungAppsData encbirthDate : "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;)V

    .line 2255
    return-object p1

    .line 2198
    :catch_0
    move-exception v0

    move-object v1, v2

    :goto_4
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    .line 2201
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v0, "AIV"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-static {}, Lcom/osp/app/util/v;->a()Lcom/osp/app/util/v;

    move-result-object v6

    const-string v7, "EmailID"

    invoke-virtual {v6, v7}, Lcom/osp/app/util/v;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v3, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v6, " encrypt failed."

    invoke-virtual {v3, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v0, v3}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;)V

    move-object v3, v1

    goto/16 :goto_0

    .line 2209
    :catch_1
    move-exception v0

    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    .line 2212
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v0, "AIV"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-static {}, Lcom/osp/app/util/v;->a()Lcom/osp/app/util/v;

    move-result-object v6

    const-string v7, "Password"

    invoke-virtual {v6, v7}, Lcom/osp/app/util/v;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v1, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v6, " encrypt failed."

    invoke-virtual {v1, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_1

    .line 2218
    :catch_2
    move-exception v0

    move-object v1, v0

    move-object v0, v2

    :goto_5
    invoke-virtual {v1}, Ljava/lang/Exception;->printStackTrace()V

    .line 2221
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v1, "AIV"

    const-string v2, "external_b encrypt failed."

    invoke-static {v1, v2}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_2

    .line 2238
    :cond_1
    const-string v1, "apps_id"

    invoke-virtual {p1, v1, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 2243
    const-string v1, "apps_birthdate"

    invoke-virtual {p1, v1, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    goto/16 :goto_3

    .line 2218
    :catch_3
    move-exception v1

    goto :goto_5

    .line 2198
    :catch_4
    move-exception v0

    goto :goto_4
.end method

.method private a(Landroid/view/ViewGroup;Ljava/lang/String;Ljava/lang/String;ZI)Landroid/widget/EditText;
    .locals 14

    .prologue
    .line 325
    iget-object v1, p0, Lcom/osp/app/signin/AccountinfoView;->aO:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->clear()V

    .line 327
    iget-object v1, p0, Lcom/osp/app/signin/AccountinfoView;->am:Landroid/view/LayoutInflater;

    const v2, 0x7f030002

    const/4 v3, 0x0

    invoke-virtual {v1, v2, p1, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    move-object v13, v1

    check-cast v13, Lcom/osp/app/signin/AccountInfoListView;

    .line 328
    invoke-virtual {v13}, Lcom/osp/app/signin/AccountInfoListView;->a()V

    .line 330
    invoke-static/range {p5 .. p5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v13, v1}, Lcom/osp/app/signin/AccountInfoListView;->setTag(Ljava/lang/Object;)V

    .line 331
    iget-object v1, v13, Lcom/osp/app/signin/AccountInfoListView;->f:Landroid/widget/EditText;

    invoke-static/range {p5 .. p5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/EditText;->setTag(Ljava/lang/Object;)V

    .line 333
    iget-object v1, v13, Lcom/osp/app/signin/AccountInfoListView;->c:Landroid/widget/TextView;

    move-object/from16 v0, p2

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 334
    iget-object v2, v13, Lcom/osp/app/signin/AccountInfoListView;->e:Landroid/widget/TextView;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, p2

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    if-eqz p4, :cond_c

    const-string v1, " *"

    :goto_0
    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v2, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 336
    iget-object v1, v13, Lcom/osp/app/signin/AccountInfoListView;->e:Landroid/widget/TextView;

    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setClickable(Z)V

    .line 337
    iget-object v1, v13, Lcom/osp/app/signin/AccountInfoListView;->e:Landroid/widget/TextView;

    move-object/from16 v0, p2

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 340
    iget-object v1, v13, Lcom/osp/app/signin/AccountInfoListView;->f:Landroid/widget/EditText;

    invoke-static {}, Lcom/osp/device/b;->b()Lcom/osp/device/b;

    move-result-object v2

    invoke-virtual {v2, p0}, Lcom/osp/device/b;->i(Landroid/content/Context;)Z

    move-object/from16 v0, p2

    invoke-virtual {p0, v0, v1}, Lcom/osp/app/signin/AccountinfoView;->a(Ljava/lang/String;Landroid/widget/EditText;)Landroid/widget/EditText;

    move-result-object v1

    iput-object v1, v13, Lcom/osp/app/signin/AccountInfoListView;->f:Landroid/widget/EditText;

    .line 342
    const/16 v1, 0x74

    move/from16 v0, p5

    if-ne v0, v1, :cond_10

    .line 344
    iget-object v1, p0, Lcom/osp/app/signin/AccountinfoView;->al:Lcom/osp/app/signin/kd;

    iget-object v1, v1, Lcom/osp/app/signin/kd;->u:Ljava/util/ArrayList;

    iput-object v1, p0, Lcom/osp/app/signin/AccountinfoView;->au:Ljava/util/ArrayList;

    .line 346
    iget-object v1, v13, Lcom/osp/app/signin/AccountInfoListView;->h:Landroid/widget/Spinner;

    const v2, 0x7f09003d

    invoke-virtual {p0, v2}, Lcom/osp/app/signin/AccountinfoView;->getText(I)Ljava/lang/CharSequence;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/Spinner;->setPrompt(Ljava/lang/CharSequence;)V

    .line 348
    invoke-static {}, Lcom/osp/device/b;->b()Lcom/osp/device/b;

    move-result-object v1

    invoke-virtual {v1, p0}, Lcom/osp/device/b;->i(Landroid/content/Context;)Z

    move-result v1

    if-nez v1, :cond_d

    .line 350
    new-instance v1, Landroid/widget/ArrayAdapter;

    const v2, 0x7f030004

    iget-object v3, p0, Lcom/osp/app/signin/AccountinfoView;->au:Ljava/util/ArrayList;

    invoke-direct {v1, p0, v2, v3}, Landroid/widget/ArrayAdapter;-><init>(Landroid/content/Context;ILjava/util/List;)V

    .line 361
    :goto_1
    const v2, 0x1090009

    invoke-virtual {v1, v2}, Landroid/widget/ArrayAdapter;->setDropDownViewResource(I)V

    .line 362
    iget-object v2, v13, Lcom/osp/app/signin/AccountInfoListView;->h:Landroid/widget/Spinner;

    invoke-virtual {v2, v1}, Landroid/widget/Spinner;->setAdapter(Landroid/widget/SpinnerAdapter;)V

    .line 363
    iget-object v2, v13, Lcom/osp/app/signin/AccountInfoListView;->h:Landroid/widget/Spinner;

    iget-object v1, p0, Lcom/osp/app/signin/AccountinfoView;->au:Ljava/util/ArrayList;

    const/4 v3, 0x1

    invoke-virtual {v1, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    move-object/from16 v0, p3

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_f

    const/4 v1, 0x1

    :goto_2
    invoke-virtual {v2, v1}, Landroid/widget/Spinner;->setSelection(I)V

    .line 409
    :cond_0
    :goto_3
    iget-object v1, v13, Lcom/osp/app/signin/AccountInfoListView;->h:Landroid/widget/Spinner;

    iget-object v2, p0, Lcom/osp/app/signin/AccountinfoView;->aQ:Landroid/view/View$OnKeyListener;

    invoke-virtual {v1, v2}, Landroid/widget/Spinner;->setOnKeyListener(Landroid/view/View$OnKeyListener;)V

    .line 410
    iget-object v1, v13, Lcom/osp/app/signin/AccountInfoListView;->h:Landroid/widget/Spinner;

    new-instance v2, Lcom/osp/app/signin/bt;

    invoke-direct {v2, p0, v13}, Lcom/osp/app/signin/bt;-><init>(Lcom/osp/app/signin/AccountinfoView;Lcom/osp/app/signin/AccountInfoListView;)V

    invoke-virtual {v1, v2}, Landroid/widget/Spinner;->setOnItemSelectedListener(Landroid/widget/AdapterView$OnItemSelectedListener;)V

    .line 433
    iget-object v1, v13, Lcom/osp/app/signin/AccountInfoListView;->d:Landroid/widget/TextView;

    move-object/from16 v0, p3

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 435
    const-string v1, "N/A"

    move-object/from16 v0, p3

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_1

    .line 437
    iget-object v1, v13, Lcom/osp/app/signin/AccountInfoListView;->f:Landroid/widget/EditText;

    move-object/from16 v0, p3

    invoke-virtual {v1, v0}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    .line 438
    iget-object v1, v13, Lcom/osp/app/signin/AccountInfoListView;->g:Landroid/widget/TextView;

    move-object/from16 v0, p3

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 442
    :cond_1
    iget-object v1, v13, Lcom/osp/app/signin/AccountInfoListView;->f:Landroid/widget/EditText;

    iget-object v2, p0, Lcom/osp/app/signin/AccountinfoView;->aQ:Landroid/view/View$OnKeyListener;

    invoke-virtual {v1, v2}, Landroid/widget/EditText;->setOnKeyListener(Landroid/view/View$OnKeyListener;)V

    .line 443
    iget-object v1, v13, Lcom/osp/app/signin/AccountInfoListView;->f:Landroid/widget/EditText;

    invoke-virtual {v1, p0}, Landroid/widget/EditText;->setOnFocusChangeListener(Landroid/view/View$OnFocusChangeListener;)V

    .line 444
    iget-object v1, v13, Lcom/osp/app/signin/AccountInfoListView;->f:Landroid/widget/EditText;

    new-instance v2, Lcom/osp/app/signin/bu;

    invoke-direct {v2, p0}, Lcom/osp/app/signin/bu;-><init>(Lcom/osp/app/signin/AccountinfoView;)V

    invoke-virtual {v1, v2}, Landroid/widget/EditText;->addTextChangedListener(Landroid/text/TextWatcher;)V

    .line 460
    invoke-static {}, Lcom/osp/device/b;->b()Lcom/osp/device/b;

    move-result-object v1

    invoke-virtual {v1, p0}, Lcom/osp/device/b;->i(Landroid/content/Context;)Z

    move-result v1

    if-nez v1, :cond_5

    const/16 v1, 0x75

    move/from16 v0, p5

    if-eq v0, v1, :cond_2

    const/16 v1, 0x76

    move/from16 v0, p5

    if-ne v0, v1, :cond_5

    .line 462
    :cond_2
    invoke-virtual {p0}, Lcom/osp/app/signin/AccountinfoView;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v1

    .line 466
    iget v2, v1, Landroid/util/DisplayMetrics;->densityDpi:I

    const/16 v3, 0xa0

    if-gt v2, v3, :cond_18

    .line 468
    const/4 v6, 0x3

    .line 474
    :goto_4
    invoke-virtual {p0}, Lcom/osp/app/signin/AccountinfoView;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0800e7

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v2

    iget v1, v1, Landroid/util/DisplayMetrics;->densityDpi:I

    int-to-float v1, v1

    const/high16 v3, 0x43200000    # 160.0f

    div-float/2addr v1, v3

    div-float v1, v2, v1

    invoke-static {v1}, Ljava/lang/Math;->round(F)I

    move-result v3

    .line 475
    const/16 v1, 0x75

    move/from16 v0, p5

    if-ne v0, v1, :cond_3

    .line 477
    iget-object v1, v13, Lcom/osp/app/signin/AccountInfoListView;->b:Landroid/widget/LinearLayout;

    invoke-virtual {v1}, Landroid/widget/LinearLayout;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v1

    .line 478
    invoke-virtual {p0}, Lcom/osp/app/signin/AccountinfoView;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v4, 0x7f0800f2

    invoke-virtual {v2, v4}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v2

    iput v2, v1, Landroid/view/ViewGroup$LayoutParams;->height:I

    .line 479
    iget-object v2, v13, Lcom/osp/app/signin/AccountInfoListView;->b:Landroid/widget/LinearLayout;

    invoke-virtual {v2, v1}, Landroid/widget/LinearLayout;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 480
    iget-object v7, v13, Lcom/osp/app/signin/AccountInfoListView;->f:Landroid/widget/EditText;

    invoke-virtual {p0}, Lcom/osp/app/signin/AccountinfoView;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0800f3

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v2

    const/16 v4, 0x9

    move-object v1, p0

    move v5, v3

    invoke-static/range {v1 .. v6}, Lcom/msc/sa/c/d;->a(Landroid/content/Context;IIIII)Landroid/view/ViewGroup$LayoutParams;

    move-result-object v1

    invoke-virtual {v7, v1}, Landroid/widget/EditText;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 484
    :cond_3
    const/16 v1, 0x76

    move/from16 v0, p5

    if-ne v0, v1, :cond_5

    .line 486
    iget-object v1, v13, Lcom/osp/app/signin/AccountInfoListView;->b:Landroid/widget/LinearLayout;

    invoke-virtual {v1}, Landroid/widget/LinearLayout;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v2

    .line 487
    const/4 v1, 0x0

    .line 488
    iget-object v4, v13, Lcom/osp/app/signin/AccountInfoListView;->j:Landroid/view/View;

    if-eqz v4, :cond_4

    iget-object v4, v13, Lcom/osp/app/signin/AccountInfoListView;->j:Landroid/view/View;

    invoke-virtual {v4}, Landroid/view/View;->getVisibility()I

    move-result v4

    if-nez v4, :cond_4

    .line 490
    iget-object v1, v13, Lcom/osp/app/signin/AccountInfoListView;->j:Landroid/view/View;

    invoke-virtual {v1}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v1

    iget v1, v1, Landroid/view/ViewGroup$LayoutParams;->height:I

    .line 492
    :cond_4
    invoke-virtual {p0}, Lcom/osp/app/signin/AccountinfoView;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const v5, 0x7f0800f3

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v4

    add-int/lit8 v5, v6, 0x9

    invoke-static {p0, v5}, Lcom/msc/sa/c/d;->b(Landroid/content/Context;I)I

    move-result v5

    add-int/2addr v4, v5

    iput v4, v2, Landroid/view/ViewGroup$LayoutParams;->height:I

    .line 493
    iget v4, v2, Landroid/view/ViewGroup$LayoutParams;->height:I

    add-int/2addr v1, v4

    iput v1, v2, Landroid/view/ViewGroup$LayoutParams;->height:I

    .line 494
    iget-object v1, v13, Lcom/osp/app/signin/AccountInfoListView;->b:Landroid/widget/LinearLayout;

    invoke-virtual {v1, v2}, Landroid/widget/LinearLayout;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 496
    iget-object v1, v13, Lcom/osp/app/signin/AccountInfoListView;->f:Landroid/widget/EditText;

    invoke-virtual {p0}, Lcom/osp/app/signin/AccountinfoView;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v4, 0x7f0800f3

    invoke-virtual {v2, v4}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v8

    const/16 v12, 0x9

    move-object v7, p0

    move v9, v3

    move v10, v6

    move v11, v3

    invoke-static/range {v7 .. v12}, Lcom/msc/sa/c/d;->a(Landroid/content/Context;IIIII)Landroid/view/ViewGroup$LayoutParams;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/EditText;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 501
    :cond_5
    invoke-virtual {p1, v13}, Landroid/view/ViewGroup;->addView(Landroid/view/View;)V

    .line 503
    const/16 v1, 0x75

    move/from16 v0, p5

    if-eq v0, v1, :cond_6

    const/16 v1, 0x76

    move/from16 v0, p5

    if-ne v0, v1, :cond_9

    .line 505
    :cond_6
    iget-object v1, p0, Lcom/osp/app/signin/AccountinfoView;->aK:Ljava/lang/String;

    if-eqz v1, :cond_8

    iget-object v1, p0, Lcom/osp/app/signin/AccountinfoView;->aK:Ljava/lang/String;

    const-string v2, "450"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_7

    invoke-static {p0}, Lcom/osp/app/util/r;->s(Landroid/content/Context;)Z

    move-result v1

    if-eqz v1, :cond_8

    :cond_7
    iget-object v1, v13, Lcom/osp/app/signin/AccountInfoListView;->f:Landroid/widget/EditText;

    invoke-virtual {v1}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v1

    if-eqz v1, :cond_8

    iget-object v1, v13, Lcom/osp/app/signin/AccountInfoListView;->f:Landroid/widget/EditText;

    invoke-virtual {v1}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v1

    invoke-interface {v1}, Landroid/text/Editable;->length()I

    move-result v1

    if-eqz v1, :cond_8

    .line 510
    iget-object v1, v13, Lcom/osp/app/signin/AccountInfoListView;->f:Landroid/widget/EditText;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Landroid/widget/EditText;->setEnabled(Z)V

    .line 511
    iget-object v1, v13, Lcom/osp/app/signin/AccountInfoListView;->f:Landroid/widget/EditText;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Landroid/widget/EditText;->setFocusable(Z)V

    .line 514
    :cond_8
    invoke-static {}, Lcom/osp/device/b;->b()Lcom/osp/device/b;

    move-result-object v1

    invoke-virtual {v1, p0}, Lcom/osp/device/b;->i(Landroid/content/Context;)Z

    move-result v1

    if-nez v1, :cond_9

    const/16 v1, 0x75

    move/from16 v0, p5

    if-ne v0, v1, :cond_9

    .line 516
    iget-object v1, v13, Lcom/osp/app/signin/AccountInfoListView;->j:Landroid/view/View;

    const/16 v2, 0x8

    invoke-virtual {v1, v2}, Landroid/view/View;->setVisibility(I)V

    .line 520
    :cond_9
    const/16 v1, 0x79

    move/from16 v0, p5

    if-ne v0, v1, :cond_a

    .line 522
    iget-object v1, p0, Lcom/osp/app/signin/AccountinfoView;->aK:Ljava/lang/String;

    if-eqz v1, :cond_a

    iget-object v1, p0, Lcom/osp/app/signin/AccountinfoView;->aK:Ljava/lang/String;

    const-string v2, "450"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_a

    iget-object v1, v13, Lcom/osp/app/signin/AccountInfoListView;->f:Landroid/widget/EditText;

    invoke-virtual {v1}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v1

    if-eqz v1, :cond_a

    iget-object v1, v13, Lcom/osp/app/signin/AccountInfoListView;->f:Landroid/widget/EditText;

    invoke-virtual {v1}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v1

    invoke-interface {v1}, Landroid/text/Editable;->length()I

    move-result v1

    if-eqz v1, :cond_a

    .line 524
    iget-object v1, v13, Lcom/osp/app/signin/AccountInfoListView;->f:Landroid/widget/EditText;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Landroid/widget/EditText;->setEnabled(Z)V

    .line 525
    iget-object v1, v13, Lcom/osp/app/signin/AccountInfoListView;->f:Landroid/widget/EditText;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Landroid/widget/EditText;->setFocusable(Z)V

    .line 529
    :cond_a
    if-eqz p4, :cond_b

    .line 531
    iget-object v1, p0, Lcom/osp/app/signin/AccountinfoView;->aO:Ljava/util/ArrayList;

    iget-object v2, v13, Lcom/osp/app/signin/AccountInfoListView;->f:Landroid/widget/EditText;

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 534
    :cond_b
    iget-object v1, v13, Lcom/osp/app/signin/AccountInfoListView;->f:Landroid/widget/EditText;

    return-object v1

    .line 334
    :cond_c
    const-string v1, ""

    goto/16 :goto_0

    .line 353
    :cond_d
    invoke-virtual {p0}, Lcom/osp/app/signin/AccountinfoView;->h()Z

    move-result v1

    if-eqz v1, :cond_e

    .line 355
    new-instance v1, Landroid/widget/ArrayAdapter;

    const v2, 0x7f03005e

    iget-object v3, p0, Lcom/osp/app/signin/AccountinfoView;->au:Ljava/util/ArrayList;

    invoke-direct {v1, p0, v2, v3}, Landroid/widget/ArrayAdapter;-><init>(Landroid/content/Context;ILjava/util/List;)V

    goto/16 :goto_1

    .line 358
    :cond_e
    new-instance v1, Landroid/widget/ArrayAdapter;

    const v2, 0x7f03005f

    iget-object v3, p0, Lcom/osp/app/signin/AccountinfoView;->au:Ljava/util/ArrayList;

    invoke-direct {v1, p0, v2, v3}, Landroid/widget/ArrayAdapter;-><init>(Landroid/content/Context;ILjava/util/List;)V

    goto/16 :goto_1

    .line 363
    :cond_f
    const/4 v1, 0x0

    goto/16 :goto_2

    .line 365
    :cond_10
    const/16 v1, 0x7e

    move/from16 v0, p5

    if-ne v0, v1, :cond_14

    .line 367
    const/4 v1, 0x2

    new-array v2, v1, [Ljava/lang/String;

    const/4 v1, 0x0

    const v3, 0x7f09003a

    invoke-virtual {p0, v3}, Lcom/osp/app/signin/AccountinfoView;->getString(I)Ljava/lang/String;

    move-result-object v3

    aput-object v3, v2, v1

    const/4 v1, 0x1

    const v3, 0x7f090029

    invoke-virtual {p0, v3}, Lcom/osp/app/signin/AccountinfoView;->getString(I)Ljava/lang/String;

    move-result-object v3

    aput-object v3, v2, v1

    .line 368
    iget-object v1, v13, Lcom/osp/app/signin/AccountInfoListView;->h:Landroid/widget/Spinner;

    const v3, 0x7f09002b

    invoke-virtual {p0, v3}, Lcom/osp/app/signin/AccountinfoView;->getText(I)Ljava/lang/CharSequence;

    move-result-object v3

    invoke-virtual {v1, v3}, Landroid/widget/Spinner;->setPrompt(Ljava/lang/CharSequence;)V

    .line 370
    invoke-static {}, Lcom/osp/device/b;->b()Lcom/osp/device/b;

    move-result-object v1

    invoke-virtual {v1, p0}, Lcom/osp/device/b;->i(Landroid/content/Context;)Z

    move-result v1

    if-nez v1, :cond_11

    .line 372
    new-instance v1, Landroid/widget/ArrayAdapter;

    const v3, 0x7f030004

    invoke-direct {v1, p0, v3, v2}, Landroid/widget/ArrayAdapter;-><init>(Landroid/content/Context;I[Ljava/lang/Object;)V

    .line 383
    :goto_5
    const v3, 0x1090009

    invoke-virtual {v1, v3}, Landroid/widget/ArrayAdapter;->setDropDownViewResource(I)V

    .line 384
    iget-object v3, v13, Lcom/osp/app/signin/AccountInfoListView;->h:Landroid/widget/Spinner;

    invoke-virtual {v3, v1}, Landroid/widget/Spinner;->setAdapter(Landroid/widget/SpinnerAdapter;)V

    .line 385
    iget-object v3, v13, Lcom/osp/app/signin/AccountInfoListView;->h:Landroid/widget/Spinner;

    const/4 v1, 0x1

    aget-object v1, v2, v1

    move-object/from16 v0, p3

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_13

    const/4 v1, 0x1

    :goto_6
    invoke-virtual {v3, v1}, Landroid/widget/Spinner;->setSelection(I)V

    goto/16 :goto_3

    .line 375
    :cond_11
    invoke-virtual {p0}, Lcom/osp/app/signin/AccountinfoView;->h()Z

    move-result v1

    if-eqz v1, :cond_12

    .line 377
    new-instance v1, Landroid/widget/ArrayAdapter;

    const v3, 0x7f03005e

    invoke-direct {v1, p0, v3, v2}, Landroid/widget/ArrayAdapter;-><init>(Landroid/content/Context;I[Ljava/lang/Object;)V

    goto :goto_5

    .line 380
    :cond_12
    new-instance v1, Landroid/widget/ArrayAdapter;

    const v3, 0x7f03005f

    invoke-direct {v1, p0, v3, v2}, Landroid/widget/ArrayAdapter;-><init>(Landroid/content/Context;I[Ljava/lang/Object;)V

    goto :goto_5

    .line 385
    :cond_13
    const/4 v1, 0x0

    goto :goto_6

    .line 386
    :cond_14
    const/16 v1, 0x80

    move/from16 v0, p5

    if-ne v0, v1, :cond_0

    .line 388
    const/4 v1, 0x2

    new-array v2, v1, [Ljava/lang/String;

    const/4 v1, 0x0

    const v3, 0x7f090057

    invoke-virtual {p0, v3}, Lcom/osp/app/signin/AccountinfoView;->getString(I)Ljava/lang/String;

    move-result-object v3

    aput-object v3, v2, v1

    const/4 v1, 0x1

    const v3, 0x7f09003c

    invoke-virtual {p0, v3}, Lcom/osp/app/signin/AccountinfoView;->getString(I)Ljava/lang/String;

    move-result-object v3

    aput-object v3, v2, v1

    .line 389
    iget-object v1, v13, Lcom/osp/app/signin/AccountInfoListView;->h:Landroid/widget/Spinner;

    const v3, 0x7f09003b

    invoke-virtual {p0, v3}, Lcom/osp/app/signin/AccountinfoView;->getText(I)Ljava/lang/CharSequence;

    move-result-object v3

    invoke-virtual {v1, v3}, Landroid/widget/Spinner;->setPrompt(Ljava/lang/CharSequence;)V

    .line 391
    invoke-static {}, Lcom/osp/device/b;->b()Lcom/osp/device/b;

    move-result-object v1

    invoke-virtual {v1, p0}, Lcom/osp/device/b;->i(Landroid/content/Context;)Z

    move-result v1

    if-nez v1, :cond_15

    .line 393
    new-instance v1, Landroid/widget/ArrayAdapter;

    const v3, 0x7f030004

    invoke-direct {v1, p0, v3, v2}, Landroid/widget/ArrayAdapter;-><init>(Landroid/content/Context;I[Ljava/lang/Object;)V

    .line 404
    :goto_7
    const v3, 0x1090009

    invoke-virtual {v1, v3}, Landroid/widget/ArrayAdapter;->setDropDownViewResource(I)V

    .line 405
    iget-object v3, v13, Lcom/osp/app/signin/AccountInfoListView;->h:Landroid/widget/Spinner;

    invoke-virtual {v3, v1}, Landroid/widget/Spinner;->setAdapter(Landroid/widget/SpinnerAdapter;)V

    .line 406
    iget-object v3, v13, Lcom/osp/app/signin/AccountInfoListView;->h:Landroid/widget/Spinner;

    const/4 v1, 0x1

    aget-object v1, v2, v1

    move-object/from16 v0, p3

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_17

    const/4 v1, 0x1

    :goto_8
    invoke-virtual {v3, v1}, Landroid/widget/Spinner;->setSelection(I)V

    goto/16 :goto_3

    .line 396
    :cond_15
    invoke-virtual {p0}, Lcom/osp/app/signin/AccountinfoView;->h()Z

    move-result v1

    if-eqz v1, :cond_16

    .line 398
    new-instance v1, Landroid/widget/ArrayAdapter;

    const v3, 0x7f03005e

    invoke-direct {v1, p0, v3, v2}, Landroid/widget/ArrayAdapter;-><init>(Landroid/content/Context;I[Ljava/lang/Object;)V

    goto :goto_7

    .line 401
    :cond_16
    new-instance v1, Landroid/widget/ArrayAdapter;

    const v3, 0x7f03005f

    invoke-direct {v1, p0, v3, v2}, Landroid/widget/ArrayAdapter;-><init>(Landroid/content/Context;I[Ljava/lang/Object;)V

    goto :goto_7

    .line 406
    :cond_17
    const/4 v1, 0x0

    goto :goto_8

    .line 471
    :cond_18
    const/4 v6, 0x4

    goto/16 :goto_4
.end method

.method static synthetic a(Lcom/osp/app/signin/AccountinfoView;Lcom/osp/app/signin/SignUpinfo;)Lcom/osp/app/signin/SignUpinfo;
    .locals 0

    .prologue
    .line 96
    iput-object p1, p0, Lcom/osp/app/signin/AccountinfoView;->d:Lcom/osp/app/signin/SignUpinfo;

    return-object p1
.end method

.method static synthetic a(Lcom/osp/app/signin/AccountinfoView;Lcom/osp/app/signin/kd;)Lcom/osp/app/signin/kd;
    .locals 0

    .prologue
    .line 96
    iput-object p1, p0, Lcom/osp/app/signin/AccountinfoView;->al:Lcom/osp/app/signin/kd;

    return-object p1
.end method

.method private a(I)V
    .locals 4

    .prologue
    const v3, 0x7f08011e

    .line 1111
    invoke-static {}, Lcom/osp/device/b;->b()Lcom/osp/device/b;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/osp/device/b;->i(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1113
    invoke-virtual {p0}, Lcom/osp/app/signin/AccountinfoView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f08011f

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v0

    float-to-int v0, v0

    .line 1114
    invoke-virtual {p0}, Lcom/osp/app/signin/AccountinfoView;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1, v3}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v1

    float-to-int v1, v1

    .line 1115
    invoke-static {}, Lcom/osp/app/signin/SamsungService;->a()Z

    move-result v2

    if-nez v2, :cond_0

    .line 1117
    invoke-virtual {p0}, Lcom/osp/app/signin/AccountinfoView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0, v3}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v0

    float-to-int v0, v0

    .line 1120
    :cond_0
    const v2, 0x7f0c015b

    invoke-virtual {p0, v2}, Lcom/osp/app/signin/AccountinfoView;->findViewById(I)Landroid/view/View;

    move-result-object v2

    .line 1121
    invoke-static {}, Lcom/osp/app/util/q;->a()Lcom/osp/app/util/q;

    invoke-static {v2, v0, v1, p1}, Lcom/osp/app/util/q;->b(Landroid/view/View;III)V

    .line 1123
    :cond_1
    return-void
.end method

.method private a(Landroid/app/AlertDialog;)V
    .locals 2

    .prologue
    .line 611
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v0, "AIV"

    const-string v1, "AccountinfoView::CloseDialog"

    invoke-static {v0, v1}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 613
    invoke-virtual {p0}, Lcom/osp/app/signin/AccountinfoView;->isFinishing()Z

    move-result v0

    if-nez v0, :cond_0

    if-eqz p1, :cond_0

    .line 617
    :try_start_0
    invoke-virtual {p1}, Landroid/app/AlertDialog;->isShowing()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 619
    invoke-virtual {p1}, Landroid/app/AlertDialog;->dismiss()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 629
    :cond_0
    :goto_0
    return-void

    .line 621
    :catch_0
    move-exception v0

    :try_start_1
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 626
    :catchall_0
    move-exception v0

    throw v0
.end method

.method static synthetic a(Lcom/osp/app/signin/AccountinfoView;)V
    .locals 0

    .prologue
    .line 96
    invoke-direct {p0}, Lcom/osp/app/signin/AccountinfoView;->d()V

    return-void
.end method

.method static synthetic a(Lcom/osp/app/signin/AccountinfoView;Ljava/lang/String;)V
    .locals 3

    .prologue
    .line 96
    new-instance v0, Landroid/content/Intent;

    const-string v1, "com.msc.action.samsungaccount.resignin"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    const-string v1, "client_id"

    iget-object v2, p0, Lcom/osp/app/signin/AccountinfoView;->K:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    const-string v1, "client_secret"

    iget-object v2, p0, Lcom/osp/app/signin/AccountinfoView;->L:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    const-string v1, "OSP_VER"

    const-string v2, "OSP_02"

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    if-eqz p1, :cond_0

    const-string v1, "email_id"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    :cond_0
    const/16 v1, 0xcd

    invoke-virtual {p0, v0, v1}, Lcom/osp/app/signin/AccountinfoView;->startActivityForResult(Landroid/content/Intent;I)V

    return-void
.end method

.method private a(Z)V
    .locals 6

    .prologue
    const v5, 0x7f090024

    const/4 v3, -0x1

    const/4 v4, 0x0

    .line 1726
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "AccountinfoView::SendDataResult iscancel : "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;)V

    .line 1728
    const-string v0, "BG_mode"

    iget-object v1, p0, Lcom/osp/app/signin/AccountinfoView;->aG:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_5

    .line 1730
    new-instance v0, Landroid/content/Intent;

    const-string v1, "com.msc.action.samsungaccount.response.BackGroundSignin"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 1731
    iget-boolean v1, p0, Lcom/osp/app/signin/AccountinfoView;->ar:Z

    if-eqz v1, :cond_3

    .line 1733
    invoke-direct {p0, v0}, Lcom/osp/app/signin/AccountinfoView;->a(Landroid/content/Intent;)Landroid/content/Intent;

    .line 1739
    :goto_0
    if-eqz p1, :cond_4

    .line 1741
    const-string v1, "bg_result"

    invoke-virtual {v0, v1, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 1747
    :goto_1
    iget-object v1, p0, Lcom/osp/app/signin/AccountinfoView;->aB:Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 1749
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v2, p0, Lcom/osp/app/signin/AccountinfoView;->aB:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ":"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setData(Landroid/net/Uri;)Landroid/content/Intent;

    .line 1750
    iget-object v1, p0, Lcom/osp/app/signin/AccountinfoView;->aB:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setPackage(Ljava/lang/String;)Landroid/content/Intent;

    .line 1753
    const-string v1, "client_id"

    iget-object v2, p0, Lcom/osp/app/signin/AccountinfoView;->K:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 1754
    iget-wide v2, p0, Lcom/osp/app/signin/AccountinfoView;->aJ:J

    invoke-virtual {p0, v0, v2, v3}, Lcom/osp/app/signin/AccountinfoView;->a(Landroid/content/Intent;J)V

    .line 1775
    :cond_0
    :goto_2
    if-eqz p1, :cond_2

    .line 1777
    iget-boolean v0, p0, Lcom/osp/app/signin/AccountinfoView;->ar:Z

    if-eqz v0, :cond_9

    .line 1779
    invoke-static {p0}, Lcom/msc/c/e;->c(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_1

    const-string v0, ""

    invoke-static {p0}, Lcom/msc/c/e;->c(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_8

    .line 1781
    :cond_1
    invoke-static {}, Lcom/osp/app/util/ap;->a()Lcom/osp/app/util/ap;

    const v0, 0x7f090031

    invoke-virtual {p0, v0}, Lcom/osp/app/signin/AccountinfoView;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-static {p0, v0, v4}, Lcom/osp/app/util/ap;->a(Landroid/content/Context;Ljava/lang/String;I)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    .line 1791
    :cond_2
    :goto_3
    return-void

    .line 1736
    :cond_3
    const-string v1, "authcode"

    iget-object v2, p0, Lcom/osp/app/signin/AccountinfoView;->B:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 1737
    const-string v1, "ServerUrl"

    invoke-static {p0}, Lcom/msc/c/c;->f(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    goto :goto_0

    .line 1744
    :cond_4
    const-string v1, "bg_result"

    invoke-virtual {v0, v1, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    goto :goto_1

    .line 1759
    :cond_5
    iget-boolean v0, p0, Lcom/osp/app/signin/AccountinfoView;->ar:Z

    if-eqz v0, :cond_6

    .line 1761
    iget-object v0, p0, Lcom/osp/app/signin/AccountinfoView;->ap:Landroid/content/Intent;

    invoke-direct {p0, v0}, Lcom/osp/app/signin/AccountinfoView;->a(Landroid/content/Intent;)Landroid/content/Intent;

    .line 1767
    :goto_4
    if-eqz p1, :cond_7

    .line 1769
    iget-object v0, p0, Lcom/osp/app/signin/AccountinfoView;->ap:Landroid/content/Intent;

    invoke-virtual {p0, v4, v0}, Lcom/osp/app/signin/AccountinfoView;->a(ILandroid/content/Intent;)V

    goto :goto_2

    .line 1764
    :cond_6
    iget-object v0, p0, Lcom/osp/app/signin/AccountinfoView;->ap:Landroid/content/Intent;

    const-string v1, "authcode"

    iget-object v2, p0, Lcom/osp/app/signin/AccountinfoView;->B:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 1765
    iget-object v0, p0, Lcom/osp/app/signin/AccountinfoView;->ap:Landroid/content/Intent;

    const-string v1, "ServerUrl"

    invoke-static {p0}, Lcom/msc/c/c;->f(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    goto :goto_4

    .line 1772
    :cond_7
    iget-object v0, p0, Lcom/osp/app/signin/AccountinfoView;->ap:Landroid/content/Intent;

    invoke-virtual {p0, v3, v0}, Lcom/osp/app/signin/AccountinfoView;->a(ILandroid/content/Intent;)V

    goto :goto_2

    .line 1784
    :cond_8
    invoke-static {}, Lcom/osp/app/util/ap;->a()Lcom/osp/app/util/ap;

    invoke-virtual {p0, v5}, Lcom/osp/app/signin/AccountinfoView;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-static {p0, v0, v4}, Lcom/osp/app/util/ap;->a(Landroid/content/Context;Ljava/lang/String;I)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    goto :goto_3

    .line 1788
    :cond_9
    invoke-static {}, Lcom/osp/app/util/ap;->a()Lcom/osp/app/util/ap;

    invoke-virtual {p0, v5}, Lcom/osp/app/signin/AccountinfoView;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-static {p0, v0, v4}, Lcom/osp/app/util/ap;->a(Landroid/content/Context;Ljava/lang/String;I)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    goto :goto_3
.end method

.method static synthetic b(Lcom/osp/app/signin/AccountinfoView;)I
    .locals 1

    .prologue
    .line 96
    iget v0, p0, Lcom/osp/app/signin/AccountinfoView;->aH:I

    return v0
.end method

.method static synthetic b(Lcom/osp/app/signin/AccountinfoView;I)I
    .locals 0

    .prologue
    .line 96
    iput p1, p0, Lcom/osp/app/signin/AccountinfoView;->as:I

    return p1
.end method

.method static synthetic b(Lcom/osp/app/signin/AccountinfoView;Landroid/app/AlertDialog;)V
    .locals 0

    .prologue
    .line 96
    invoke-direct {p0, p1}, Lcom/osp/app/signin/AccountinfoView;->a(Landroid/app/AlertDialog;)V

    return-void
.end method

.method private b(Z)V
    .locals 7

    .prologue
    const v5, 0x7f0c0019

    const v2, 0x7f0c0007

    const/4 v4, 0x4

    const/16 v6, 0x8

    const/4 v3, 0x0

    .line 1799
    iput-boolean p1, p0, Lcom/osp/app/signin/AccountinfoView;->aq:Z

    .line 1800
    iput-boolean p1, p0, Lcom/osp/app/signin/AccountinfoView;->aN:Z

    .line 1802
    if-eqz p1, :cond_0

    invoke-static {}, Lcom/osp/app/signin/SamsungService;->a()Z

    move-result v0

    if-nez v0, :cond_0

    invoke-static {p0}, Lcom/osp/app/util/r;->k(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1804
    invoke-virtual {p0}, Lcom/osp/app/signin/AccountinfoView;->getWindow()Landroid/view/Window;

    move-result-object v0

    const/16 v1, 0x400

    invoke-virtual {v0, v1}, Landroid/view/Window;->clearFlags(I)V

    .line 1807
    :cond_0
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "AccountinfoView::setEditable m_isEditmode : "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-boolean v1, p0, Lcom/osp/app/signin/AccountinfoView;->aq:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;)V

    .line 1809
    iget-object v0, p0, Lcom/osp/app/signin/AccountinfoView;->d:Lcom/osp/app/signin/SignUpinfo;

    if-eqz v0, :cond_b

    iget-object v0, p0, Lcom/osp/app/signin/AccountinfoView;->d:Lcom/osp/app/signin/SignUpinfo;

    invoke-virtual {v0}, Lcom/osp/app/signin/SignUpinfo;->j()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_b

    iget-object v0, p0, Lcom/osp/app/signin/AccountinfoView;->d:Lcom/osp/app/signin/SignUpinfo;

    invoke-virtual {v0}, Lcom/osp/app/signin/SignUpinfo;->j()Ljava/lang/String;

    move-result-object v0

    const-string v1, "KOR"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/osp/app/signin/AccountinfoView;->d:Lcom/osp/app/signin/SignUpinfo;

    invoke-virtual {v0}, Lcom/osp/app/signin/SignUpinfo;->j()Ljava/lang/String;

    move-result-object v0

    const-string v1, "CHN"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_b

    .line 1812
    :cond_1
    iget-object v0, p0, Lcom/osp/app/signin/AccountinfoView;->d:Lcom/osp/app/signin/SignUpinfo;

    invoke-virtual {v0}, Lcom/osp/app/signin/SignUpinfo;->i()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/osp/app/signin/AccountinfoView;->d:Lcom/osp/app/signin/SignUpinfo;

    invoke-virtual {v0}, Lcom/osp/app/signin/SignUpinfo;->i()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v0

    if-nez v0, :cond_a

    .line 1814
    :cond_2
    iget-object v0, p0, Lcom/osp/app/signin/AccountinfoView;->W:Landroid/widget/LinearLayout;

    invoke-virtual {v0, p1}, Landroid/widget/LinearLayout;->setFocusable(Z)V

    .line 1815
    iget-object v0, p0, Lcom/osp/app/signin/AccountinfoView;->W:Landroid/widget/LinearLayout;

    invoke-virtual {v0, p1}, Landroid/widget/LinearLayout;->setEnabled(Z)V

    .line 1816
    iget-object v0, p0, Lcom/osp/app/signin/AccountinfoView;->Y:Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setEnabled(Z)V

    .line 1817
    iget-object v0, p0, Lcom/osp/app/signin/AccountinfoView;->X:Landroid/widget/ImageView;

    invoke-static {p1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/msc/sa/c/d;->a(Landroid/widget/ImageView;Ljava/lang/Boolean;)V

    .line 1833
    :goto_0
    const v0, 0x7f0c0001

    invoke-virtual {p0, v0}, Lcom/osp/app/signin/AccountinfoView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    .line 1834
    if-eqz p1, :cond_e

    .line 1846
    const v1, 0x7f090097

    invoke-virtual {p0, v1}, Lcom/osp/app/signin/AccountinfoView;->setTitle(I)V

    .line 1848
    invoke-static {}, Lcom/osp/device/b;->b()Lcom/osp/device/b;

    move-result-object v1

    invoke-virtual {v1, p0}, Lcom/osp/device/b;->i(Landroid/content/Context;)Z

    move-result v1

    if-eqz v1, :cond_d

    .line 1850
    invoke-virtual {p0, v2}, Lcom/osp/app/signin/AccountinfoView;->findViewById(I)Landroid/view/View;

    move-result-object v1

    .line 1851
    invoke-static {}, Lcom/osp/app/util/r;->v()Z

    move-result v2

    if-eqz v2, :cond_c

    .line 1853
    invoke-virtual {v1, v4}, Landroid/view/View;->setVisibility(I)V

    .line 1867
    :cond_3
    :goto_1
    invoke-virtual {v0, v3}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 1868
    iget-object v0, p0, Lcom/osp/app/signin/AccountinfoView;->aE:Landroid/widget/ImageView;

    invoke-virtual {v0, v3}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 1870
    iget-object v0, p0, Lcom/osp/app/signin/AccountinfoView;->R:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v3}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 1871
    iget-object v0, p0, Lcom/osp/app/signin/AccountinfoView;->an:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v6}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 1873
    iget-object v0, p0, Lcom/osp/app/signin/AccountinfoView;->T:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v3}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 1874
    iget-object v0, p0, Lcom/osp/app/signin/AccountinfoView;->ao:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v6}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 1876
    iget-object v0, p0, Lcom/osp/app/signin/AccountinfoView;->ax:Landroid/widget/Button;

    if-eqz v0, :cond_4

    .line 1880
    const v0, 0x7f0c0005

    invoke-virtual {p0, v0}, Lcom/osp/app/signin/AccountinfoView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    .line 1881
    if-eqz v0, :cond_4

    .line 1883
    invoke-virtual {v0, v6}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 1887
    :cond_4
    iget-object v0, p0, Lcom/osp/app/signin/AccountinfoView;->S:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v6}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 1888
    iget-object v0, p0, Lcom/osp/app/signin/AccountinfoView;->H:Landroid/view/View;

    invoke-virtual {v0, v6}, Landroid/view/View;->setVisibility(I)V

    .line 1890
    invoke-static {}, Lcom/osp/device/b;->b()Lcom/osp/device/b;

    move-result-object v0

    invoke-virtual {v0}, Lcom/osp/device/b;->a()Z

    move-result v0

    if-eqz v0, :cond_6

    .line 1893
    const v0, 0x7f0c001d

    invoke-virtual {p0, v0}, Lcom/osp/app/signin/AccountinfoView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    .line 1894
    if-eqz v0, :cond_5

    .line 1896
    invoke-virtual {v0, v6}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 1898
    :cond_5
    const v0, 0x7f0c0020

    invoke-virtual {p0, v0}, Lcom/osp/app/signin/AccountinfoView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    .line 1899
    if-eqz v0, :cond_6

    .line 1901
    invoke-virtual {v0, v6}, Landroid/view/View;->setVisibility(I)V

    .line 1906
    :cond_6
    invoke-static {}, Lcom/osp/app/util/r;->v()Z

    move-result v0

    if-eqz v0, :cond_8

    .line 1908
    invoke-virtual {p0, v5}, Lcom/osp/app/signin/AccountinfoView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    .line 1909
    if-eqz v0, :cond_7

    .line 1911
    invoke-virtual {v0, v6}, Landroid/view/View;->setVisibility(I)V

    .line 1914
    :cond_7
    const v0, 0x7f0c0027

    invoke-virtual {p0, v0}, Lcom/osp/app/signin/AccountinfoView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    .line 1915
    if-eqz v0, :cond_8

    .line 1917
    invoke-virtual {v0, v6}, Landroid/view/View;->setVisibility(I)V

    .line 1999
    :cond_8
    :goto_2
    iget-object v0, p0, Lcom/osp/app/signin/AccountinfoView;->U:Landroid/view/ViewGroup;

    invoke-virtual {v0}, Landroid/view/ViewGroup;->getChildCount()I

    move-result v4

    move v2, v3

    .line 2000
    :goto_3
    if-ge v2, v4, :cond_19

    .line 2002
    iget-object v0, p0, Lcom/osp/app/signin/AccountinfoView;->U:Landroid/view/ViewGroup;

    invoke-virtual {v0, v2}, Landroid/view/ViewGroup;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/osp/app/signin/AccountInfoListView;

    .line 2009
    const v1, 0x7f0c002c

    invoke-virtual {v0, v1}, Lcom/osp/app/signin/AccountInfoListView;->findViewById(I)Landroid/view/View;

    move-result-object v1

    invoke-virtual {v1}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    .line 2010
    packed-switch v1, :pswitch_data_0

    .line 2135
    :pswitch_0
    if-eqz p1, :cond_18

    .line 2137
    iget-object v1, v0, Lcom/osp/app/signin/AccountInfoListView;->a:Landroid/widget/LinearLayout;

    invoke-virtual {v1, v6}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 2138
    iget-object v1, v0, Lcom/osp/app/signin/AccountInfoListView;->b:Landroid/widget/LinearLayout;

    invoke-virtual {v1, v3}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 2140
    iget-object v1, v0, Lcom/osp/app/signin/AccountInfoListView;->f:Landroid/widget/EditText;

    invoke-virtual {v1, p1}, Landroid/widget/EditText;->setClickable(Z)V

    .line 2141
    iget-object v0, v0, Lcom/osp/app/signin/AccountInfoListView;->f:Landroid/widget/EditText;

    invoke-virtual {v0, p1}, Landroid/widget/EditText;->setEnabled(Z)V

    .line 2000
    :cond_9
    :goto_4
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_3

    .line 1820
    :cond_a
    iget-object v0, p0, Lcom/osp/app/signin/AccountinfoView;->W:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v3}, Landroid/widget/LinearLayout;->setFocusable(Z)V

    .line 1821
    iget-object v0, p0, Lcom/osp/app/signin/AccountinfoView;->W:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v3}, Landroid/widget/LinearLayout;->setEnabled(Z)V

    .line 1822
    iget-object v0, p0, Lcom/osp/app/signin/AccountinfoView;->Y:Landroid/widget/TextView;

    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setEnabled(Z)V

    .line 1823
    iget-object v0, p0, Lcom/osp/app/signin/AccountinfoView;->X:Landroid/widget/ImageView;

    invoke-static {v3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/msc/sa/c/d;->a(Landroid/widget/ImageView;Ljava/lang/Boolean;)V

    goto/16 :goto_0

    .line 1827
    :cond_b
    iget-object v0, p0, Lcom/osp/app/signin/AccountinfoView;->W:Landroid/widget/LinearLayout;

    invoke-virtual {v0, p1}, Landroid/widget/LinearLayout;->setFocusable(Z)V

    .line 1828
    iget-object v0, p0, Lcom/osp/app/signin/AccountinfoView;->W:Landroid/widget/LinearLayout;

    invoke-virtual {v0, p1}, Landroid/widget/LinearLayout;->setEnabled(Z)V

    .line 1829
    iget-object v0, p0, Lcom/osp/app/signin/AccountinfoView;->Y:Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setEnabled(Z)V

    .line 1830
    iget-object v0, p0, Lcom/osp/app/signin/AccountinfoView;->X:Landroid/widget/ImageView;

    invoke-static {p1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/msc/sa/c/d;->a(Landroid/widget/ImageView;Ljava/lang/Boolean;)V

    goto/16 :goto_0

    .line 1856
    :cond_c
    invoke-virtual {v1, v6}, Landroid/view/View;->setVisibility(I)V

    goto/16 :goto_1

    .line 1860
    :cond_d
    invoke-static {}, Lcom/osp/device/b;->b()Lcom/osp/device/b;

    move-result-object v1

    invoke-virtual {v1, p0}, Lcom/osp/device/b;->e(Landroid/content/Context;)I

    move-result v1

    const/16 v2, 0xa0

    if-gt v1, v2, :cond_3

    .line 1863
    const-string v1, ""

    invoke-virtual {p0, v1}, Lcom/osp/app/signin/AccountinfoView;->setTitle(Ljava/lang/CharSequence;)V

    goto/16 :goto_1

    .line 1932
    :cond_e
    const v1, 0x7f090071

    invoke-virtual {p0, v1}, Lcom/osp/app/signin/AccountinfoView;->setTitle(I)V

    .line 1934
    invoke-static {}, Lcom/osp/device/b;->b()Lcom/osp/device/b;

    move-result-object v1

    invoke-virtual {v1, p0}, Lcom/osp/device/b;->i(Landroid/content/Context;)Z

    move-result v1

    if-eqz v1, :cond_f

    .line 1936
    invoke-virtual {p0, v2}, Lcom/osp/app/signin/AccountinfoView;->findViewById(I)Landroid/view/View;

    move-result-object v1

    .line 1937
    invoke-static {}, Lcom/osp/app/util/r;->v()Z

    move-result v2

    if-eqz v2, :cond_13

    .line 1939
    invoke-virtual {v1, v4}, Landroid/view/View;->setVisibility(I)V

    .line 1945
    :cond_f
    :goto_5
    invoke-virtual {v0, v6}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 1946
    iget-object v0, p0, Lcom/osp/app/signin/AccountinfoView;->aE:Landroid/widget/ImageView;

    invoke-virtual {v0, v6}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 1948
    iget-object v0, p0, Lcom/osp/app/signin/AccountinfoView;->R:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v6}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 1949
    iget-object v0, p0, Lcom/osp/app/signin/AccountinfoView;->an:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v3}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 1951
    iget-object v0, p0, Lcom/osp/app/signin/AccountinfoView;->T:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v6}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 1952
    iget-object v0, p0, Lcom/osp/app/signin/AccountinfoView;->ao:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v3}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 1966
    iget-object v0, p0, Lcom/osp/app/signin/AccountinfoView;->S:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v3}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 1967
    iget-object v0, p0, Lcom/osp/app/signin/AccountinfoView;->H:Landroid/view/View;

    invoke-virtual {v0, v3}, Landroid/view/View;->setVisibility(I)V

    .line 1970
    invoke-static {}, Lcom/osp/device/b;->b()Lcom/osp/device/b;

    move-result-object v0

    invoke-virtual {v0}, Lcom/osp/device/b;->a()Z

    move-result v0

    if-eqz v0, :cond_11

    invoke-static {p0}, Lcom/osp/app/util/ad;->h(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/telephony/PhoneNumberUtils;->isGlobalPhoneNumber(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_11

    .line 1973
    const v0, 0x7f0c001d

    invoke-virtual {p0, v0}, Lcom/osp/app/signin/AccountinfoView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    .line 1974
    if-eqz v0, :cond_10

    .line 1976
    invoke-virtual {v0, v3}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 1978
    :cond_10
    const v0, 0x7f0c0020

    invoke-virtual {p0, v0}, Lcom/osp/app/signin/AccountinfoView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    .line 1979
    if-eqz v0, :cond_11

    .line 1981
    invoke-virtual {v0, v3}, Landroid/view/View;->setVisibility(I)V

    .line 1986
    :cond_11
    invoke-virtual {p0, v5}, Lcom/osp/app/signin/AccountinfoView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    .line 1987
    if-eqz v0, :cond_12

    .line 1989
    invoke-virtual {v0, v3}, Landroid/view/View;->setVisibility(I)V

    .line 1992
    :cond_12
    const v0, 0x7f0c0027

    invoke-virtual {p0, v0}, Lcom/osp/app/signin/AccountinfoView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    .line 1993
    if-eqz v0, :cond_8

    .line 1995
    invoke-virtual {v0, v3}, Landroid/view/View;->setVisibility(I)V

    goto/16 :goto_2

    .line 1942
    :cond_13
    invoke-virtual {v1, v3}, Landroid/view/View;->setVisibility(I)V

    goto :goto_5

    .line 2074
    :pswitch_1
    if-eqz p1, :cond_14

    .line 2076
    iget-object v1, v0, Lcom/osp/app/signin/AccountInfoListView;->a:Landroid/widget/LinearLayout;

    invoke-virtual {v1, v6}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 2077
    iget-object v1, v0, Lcom/osp/app/signin/AccountInfoListView;->b:Landroid/widget/LinearLayout;

    invoke-virtual {v1, v3}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 2078
    iget-object v1, v0, Lcom/osp/app/signin/AccountInfoListView;->f:Landroid/widget/EditText;

    invoke-virtual {v1, v3}, Landroid/widget/EditText;->setFocusable(Z)V

    .line 2079
    iget-object v1, v0, Lcom/osp/app/signin/AccountInfoListView;->f:Landroid/widget/EditText;

    invoke-virtual {v1, v6}, Landroid/widget/EditText;->setVisibility(I)V

    .line 2080
    iget-object v0, v0, Lcom/osp/app/signin/AccountInfoListView;->h:Landroid/widget/Spinner;

    invoke-virtual {v0, v3}, Landroid/widget/Spinner;->setVisibility(I)V

    goto/16 :goto_4

    .line 2083
    :cond_14
    iget-object v1, v0, Lcom/osp/app/signin/AccountInfoListView;->a:Landroid/widget/LinearLayout;

    invoke-virtual {v1, v3}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 2084
    iget-object v0, v0, Lcom/osp/app/signin/AccountInfoListView;->b:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v6}, Landroid/widget/LinearLayout;->setVisibility(I)V

    goto/16 :goto_4

    .line 2089
    :pswitch_2
    if-eqz p1, :cond_16

    .line 2091
    iget-object v1, v0, Lcom/osp/app/signin/AccountInfoListView;->f:Landroid/widget/EditText;

    invoke-virtual {v1, p1}, Landroid/widget/EditText;->setClickable(Z)V

    .line 2092
    iget-object v1, v0, Lcom/osp/app/signin/AccountInfoListView;->f:Landroid/widget/EditText;

    invoke-virtual {v1, p1}, Landroid/widget/EditText;->setEnabled(Z)V

    .line 2093
    iget-object v1, v0, Lcom/osp/app/signin/AccountInfoListView;->a:Landroid/widget/LinearLayout;

    invoke-virtual {v1, v6}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 2094
    iget-object v1, v0, Lcom/osp/app/signin/AccountInfoListView;->b:Landroid/widget/LinearLayout;

    invoke-virtual {v1, v3}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 2096
    iget-object v1, p0, Lcom/osp/app/signin/AccountinfoView;->aK:Ljava/lang/String;

    if-eqz v1, :cond_9

    iget-object v1, p0, Lcom/osp/app/signin/AccountinfoView;->aK:Ljava/lang/String;

    const-string v5, "450"

    invoke-virtual {v1, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_15

    invoke-static {p0}, Lcom/osp/app/util/r;->s(Landroid/content/Context;)Z

    move-result v1

    if-eqz v1, :cond_9

    :cond_15
    iget-object v1, v0, Lcom/osp/app/signin/AccountInfoListView;->f:Landroid/widget/EditText;

    invoke-virtual {v1}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v1

    if-eqz v1, :cond_9

    iget-object v1, v0, Lcom/osp/app/signin/AccountInfoListView;->f:Landroid/widget/EditText;

    invoke-virtual {v1}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v1

    invoke-interface {v1}, Landroid/text/Editable;->length()I

    move-result v1

    if-eqz v1, :cond_9

    .line 2101
    iget-object v1, v0, Lcom/osp/app/signin/AccountInfoListView;->f:Landroid/widget/EditText;

    invoke-virtual {v1, v3}, Landroid/widget/EditText;->setEnabled(Z)V

    .line 2102
    iget-object v0, v0, Lcom/osp/app/signin/AccountInfoListView;->f:Landroid/widget/EditText;

    invoke-virtual {v0, v3}, Landroid/widget/EditText;->setFocusable(Z)V

    goto/16 :goto_4

    .line 2107
    :cond_16
    iget-object v1, v0, Lcom/osp/app/signin/AccountInfoListView;->a:Landroid/widget/LinearLayout;

    invoke-virtual {v1, v3}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 2108
    iget-object v0, v0, Lcom/osp/app/signin/AccountInfoListView;->b:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v6}, Landroid/widget/LinearLayout;->setVisibility(I)V

    goto/16 :goto_4

    .line 2112
    :pswitch_3
    if-eqz p1, :cond_17

    .line 2114
    iget-object v1, v0, Lcom/osp/app/signin/AccountInfoListView;->f:Landroid/widget/EditText;

    invoke-virtual {v1, p1}, Landroid/widget/EditText;->setClickable(Z)V

    .line 2115
    iget-object v1, v0, Lcom/osp/app/signin/AccountInfoListView;->f:Landroid/widget/EditText;

    invoke-virtual {v1, p1}, Landroid/widget/EditText;->setEnabled(Z)V

    .line 2116
    iget-object v1, v0, Lcom/osp/app/signin/AccountInfoListView;->a:Landroid/widget/LinearLayout;

    invoke-virtual {v1, v6}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 2117
    iget-object v1, v0, Lcom/osp/app/signin/AccountInfoListView;->b:Landroid/widget/LinearLayout;

    invoke-virtual {v1, v3}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 2119
    iget-object v1, p0, Lcom/osp/app/signin/AccountinfoView;->aK:Ljava/lang/String;

    if-eqz v1, :cond_9

    iget-object v1, p0, Lcom/osp/app/signin/AccountinfoView;->aK:Ljava/lang/String;

    const-string v5, "450"

    invoke-virtual {v1, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_9

    iget-object v1, v0, Lcom/osp/app/signin/AccountInfoListView;->f:Landroid/widget/EditText;

    invoke-virtual {v1}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v1

    if-eqz v1, :cond_9

    iget-object v1, v0, Lcom/osp/app/signin/AccountInfoListView;->f:Landroid/widget/EditText;

    invoke-virtual {v1}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v1

    invoke-interface {v1}, Landroid/text/Editable;->length()I

    move-result v1

    if-eqz v1, :cond_9

    .line 2124
    iget-object v1, v0, Lcom/osp/app/signin/AccountInfoListView;->f:Landroid/widget/EditText;

    invoke-virtual {v1, v3}, Landroid/widget/EditText;->setEnabled(Z)V

    .line 2125
    iget-object v0, v0, Lcom/osp/app/signin/AccountInfoListView;->f:Landroid/widget/EditText;

    invoke-virtual {v0, v3}, Landroid/widget/EditText;->setFocusable(Z)V

    goto/16 :goto_4

    .line 2130
    :cond_17
    iget-object v1, v0, Lcom/osp/app/signin/AccountInfoListView;->a:Landroid/widget/LinearLayout;

    invoke-virtual {v1, v3}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 2131
    iget-object v0, v0, Lcom/osp/app/signin/AccountInfoListView;->b:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v6}, Landroid/widget/LinearLayout;->setVisibility(I)V

    goto/16 :goto_4

    .line 2145
    :cond_18
    iget-object v1, v0, Lcom/osp/app/signin/AccountInfoListView;->a:Landroid/widget/LinearLayout;

    invoke-virtual {v1, v3}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 2146
    iget-object v0, v0, Lcom/osp/app/signin/AccountInfoListView;->b:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v6}, Landroid/widget/LinearLayout;->setVisibility(I)V

    goto/16 :goto_4

    .line 2154
    :cond_19
    const/high16 v0, 0x7f0c0000

    invoke-virtual {p0, v0}, Lcom/osp/app/signin/AccountinfoView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    .line 2155
    if-eqz v0, :cond_1a

    .line 2157
    invoke-virtual {v0}, Landroid/view/View;->requestFocus()Z

    .line 2160
    :cond_1a
    return-void

    .line 2010
    nop

    :pswitch_data_0
    .packed-switch 0x74
        :pswitch_1
        :pswitch_2
        :pswitch_2
        :pswitch_0
        :pswitch_0
        :pswitch_3
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method static synthetic c(Lcom/osp/app/signin/AccountinfoView;)I
    .locals 1

    .prologue
    .line 96
    iget v0, p0, Lcom/osp/app/signin/AccountinfoView;->as:I

    return v0
.end method

.method static synthetic c(Lcom/osp/app/signin/AccountinfoView;I)I
    .locals 0

    .prologue
    .line 96
    iput p1, p0, Lcom/osp/app/signin/AccountinfoView;->O:I

    return p1
.end method

.method private c()V
    .locals 8

    .prologue
    const/4 v4, 0x1

    const/4 v2, 0x0

    .line 636
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v0, "AccountinfoView::CloseIME"

    invoke-static {v0}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;)V

    .line 638
    const-string v0, "input_method"

    invoke-virtual {p0, v0}, Lcom/osp/app/signin/AccountinfoView;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/inputmethod/InputMethodManager;

    .line 641
    const/16 v1, 0xa

    new-array v5, v1, [Landroid/widget/EditText;

    iget-object v1, p0, Lcom/osp/app/signin/AccountinfoView;->aa:Landroid/widget/EditText;

    aput-object v1, v5, v2

    iget-object v1, p0, Lcom/osp/app/signin/AccountinfoView;->ac:Landroid/widget/EditText;

    aput-object v1, v5, v4

    const/4 v1, 0x2

    iget-object v3, p0, Lcom/osp/app/signin/AccountinfoView;->ad:Landroid/widget/EditText;

    aput-object v3, v5, v1

    const/4 v1, 0x3

    iget-object v3, p0, Lcom/osp/app/signin/AccountinfoView;->ae:Landroid/widget/EditText;

    aput-object v3, v5, v1

    const/4 v1, 0x4

    iget-object v3, p0, Lcom/osp/app/signin/AccountinfoView;->ag:Landroid/widget/EditText;

    aput-object v3, v5, v1

    const/4 v1, 0x5

    iget-object v3, p0, Lcom/osp/app/signin/AccountinfoView;->aj:Landroid/widget/EditText;

    aput-object v3, v5, v1

    const/4 v1, 0x6

    iget-object v3, p0, Lcom/osp/app/signin/AccountinfoView;->Z:Landroid/widget/EditText;

    aput-object v3, v5, v1

    const/4 v1, 0x7

    iget-object v3, p0, Lcom/osp/app/signin/AccountinfoView;->af:Landroid/widget/EditText;

    aput-object v3, v5, v1

    const/16 v1, 0x8

    iget-object v3, p0, Lcom/osp/app/signin/AccountinfoView;->ah:Landroid/widget/EditText;

    aput-object v3, v5, v1

    const/16 v1, 0x9

    iget-object v3, p0, Lcom/osp/app/signin/AccountinfoView;->ak:Landroid/widget/EditText;

    aput-object v3, v5, v1

    move v1, v2

    .line 644
    :goto_0
    array-length v3, v5

    if-ge v1, v3, :cond_0

    .line 646
    aget-object v3, v5, v1

    if-eqz v3, :cond_1

    invoke-virtual {v3}, Landroid/widget/EditText;->hasFocus()Z

    move-result v6

    if-ne v6, v4, :cond_1

    invoke-virtual {v3}, Landroid/widget/EditText;->getWindowToken()Landroid/os/IBinder;

    move-result-object v3

    invoke-virtual {v0, v3, v2}, Landroid/view/inputmethod/InputMethodManager;->hideSoftInputFromWindow(Landroid/os/IBinder;I)Z

    move v3, v4

    :goto_1
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    new-instance v6, Ljava/lang/StringBuilder;

    const-string v7, "AccountinfoView::CheckCloseIME skip : "

    invoke-direct {v6, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v6, v3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v6}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;)V

    if-nez v3, :cond_0

    .line 648
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 651
    :cond_0
    return-void

    :cond_1
    move v3, v2

    goto :goto_1
.end method

.method static synthetic d(Lcom/osp/app/signin/AccountinfoView;)I
    .locals 1

    .prologue
    .line 96
    iget v0, p0, Lcom/osp/app/signin/AccountinfoView;->O:I

    return v0
.end method

.method static synthetic d(Lcom/osp/app/signin/AccountinfoView;I)I
    .locals 0

    .prologue
    .line 96
    iput p1, p0, Lcom/osp/app/signin/AccountinfoView;->av:I

    return p1
.end method

.method private d()V
    .locals 4

    .prologue
    const/4 v2, 0x1

    .line 1466
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "AccountinfoView::PressEditBtn m_isEditmode : "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-boolean v1, p0, Lcom/osp/app/signin/AccountinfoView;->aq:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;)V

    .line 1468
    iget-boolean v0, p0, Lcom/osp/app/signin/AccountinfoView;->aq:Z

    if-eqz v0, :cond_4

    .line 1470
    invoke-static {p0}, Lcom/osp/app/util/ad;->a(Landroid/content/Context;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 1473
    invoke-static {}, Lcom/osp/app/util/ap;->a()Lcom/osp/app/util/ap;

    const v0, 0x7f09003e

    invoke-virtual {p0, v0}, Lcom/osp/app/signin/AccountinfoView;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-static {p0, v0, v2}, Lcom/osp/app/util/ap;->a(Landroid/content/Context;Ljava/lang/String;I)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    .line 1600
    :cond_0
    :goto_0
    return-void

    .line 1476
    :cond_1
    iget-object v0, p0, Lcom/osp/app/signin/AccountinfoView;->x:Landroid/view/Menu;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/osp/app/signin/AccountinfoView;->x:Landroid/view/Menu;

    invoke-interface {v0}, Landroid/view/Menu;->size()I

    move-result v0

    if-le v0, v2, :cond_2

    .line 1478
    iget-object v0, p0, Lcom/osp/app/signin/AccountinfoView;->x:Landroid/view/Menu;

    const v1, 0x7f0c018f

    invoke-interface {v0, v1}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v0

    invoke-interface {v0}, Landroid/view/MenuItem;->isEnabled()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1484
    :cond_2
    invoke-direct {p0}, Lcom/osp/app/signin/AccountinfoView;->c()V

    .line 1485
    new-instance v0, Lcom/osp/security/identity/g;

    invoke-direct {v0, p0}, Lcom/osp/security/identity/g;-><init>(Landroid/content/Context;)V

    .line 1486
    iget-object v1, p0, Lcom/osp/app/signin/AccountinfoView;->Y:Landroid/widget/TextView;

    invoke-virtual {v1}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v1

    invoke-interface {v1}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v1

    .line 1488
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "AccountinfoView::PressEditBtn birthDate : "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;)V

    .line 1554
    :try_start_0
    invoke-virtual {v0, v1}, Lcom/osp/security/identity/g;->a(Ljava/lang/String;)Lcom/osp/security/identity/h;

    move-result-object v0

    .line 1557
    sget-object v1, Lcom/osp/security/identity/h;->d:Lcom/osp/security/identity/h;

    if-ne v0, v1, :cond_3

    .line 1559
    iget-object v0, p0, Lcom/osp/app/signin/AccountinfoView;->W:Landroid/widget/LinearLayout;

    invoke-virtual {v0}, Landroid/widget/LinearLayout;->requestFocus()Z

    .line 1560
    invoke-static {}, Lcom/osp/app/util/ap;->a()Lcom/osp/app/util/ap;

    const v0, 0x7f090031

    invoke-virtual {p0, v0}, Lcom/osp/app/signin/AccountinfoView;->getText(I)Ljava/lang/CharSequence;

    move-result-object v0

    invoke-static {p0, v0}, Lcom/osp/app/util/ap;->a(Landroid/content/Context;Ljava/lang/CharSequence;)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V
    :try_end_0
    .catch Lcom/osp/security/identity/IdentityException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 1563
    :catch_0
    move-exception v0

    invoke-virtual {v0}, Lcom/osp/security/identity/IdentityException;->printStackTrace()V

    .line 1574
    :cond_3
    new-instance v0, Lcom/osp/app/signin/cc;

    invoke-direct {v0, p0}, Lcom/osp/app/signin/cc;-><init>(Lcom/osp/app/signin/AccountinfoView;)V

    iput-object v0, p0, Lcom/osp/app/signin/AccountinfoView;->z:Lcom/osp/app/signin/cc;

    .line 1575
    iget-object v0, p0, Lcom/osp/app/signin/AccountinfoView;->z:Lcom/osp/app/signin/cc;

    invoke-virtual {v0}, Lcom/osp/app/signin/cc;->b()V

    .line 1594
    :goto_1
    invoke-virtual {p0}, Lcom/osp/app/signin/AccountinfoView;->a()V

    .line 1596
    invoke-static {}, Lcom/osp/device/b;->b()Lcom/osp/device/b;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/osp/device/b;->i(Landroid/content/Context;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 1598
    iget-object v0, p0, Lcom/osp/app/signin/AccountinfoView;->I:Landroid/widget/Button;

    const v1, 0x7f090052

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setText(I)V

    goto/16 :goto_0

    .line 1592
    :cond_4
    invoke-direct {p0, v2}, Lcom/osp/app/signin/AccountinfoView;->b(Z)V

    goto :goto_1
.end method

.method static synthetic e(Lcom/osp/app/signin/AccountinfoView;)V
    .locals 9

    .prologue
    const/4 v2, 0x1

    const/4 v3, 0x0

    const/4 v8, 0x5

    const/4 v7, 0x2

    const/16 v6, 0xa

    .line 96
    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object v4

    iget v0, p0, Lcom/osp/app/signin/AccountinfoView;->aH:I

    invoke-static {v0}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v4, v2}, Ljava/util/Calendar;->get(I)I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v1

    iget v5, p0, Lcom/osp/app/signin/AccountinfoView;->as:I

    if-ge v5, v6, :cond_1

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v5, "0"

    invoke-virtual {v0, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v5, p0, Lcom/osp/app/signin/AccountinfoView;->as:I

    invoke-static {v5}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v0, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    :goto_0
    iget v5, p0, Lcom/osp/app/signin/AccountinfoView;->O:I

    if-ge v5, v6, :cond_2

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v5, "0"

    invoke-virtual {v0, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v5, p0, Lcom/osp/app/signin/AccountinfoView;->O:I

    invoke-static {v5}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v0, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    :goto_1
    invoke-virtual {v4, v7}, Ljava/util/Calendar;->get(I)I

    move-result v5

    if-ge v5, v6, :cond_3

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v5, "0"

    invoke-virtual {v1, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v4, v7}, Ljava/util/Calendar;->get(I)I

    move-result v5

    invoke-static {v5}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v1, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    :goto_2
    invoke-virtual {v4, v8}, Ljava/util/Calendar;->get(I)I

    move-result v5

    if-ge v5, v6, :cond_4

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v5, "0"

    invoke-virtual {v1, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v4, v8}, Ljava/util/Calendar;->get(I)I

    move-result v4

    invoke-static {v4}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    :goto_3
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/Integer;->intValue()I

    move-result v4

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/Integer;->intValue()I

    move-result v5

    if-lt v4, v5, :cond_5

    :goto_4
    if-nez v2, :cond_0

    iget v2, p0, Lcom/osp/app/signin/AccountinfoView;->aH:I

    const/16 v4, 0x76d

    if-ge v2, v4, :cond_6

    :cond_0
    const v0, 0x7f090031

    invoke-virtual {p0, v0}, Lcom/osp/app/signin/AccountinfoView;->getText(I)Ljava/lang/CharSequence;

    move-result-object v0

    invoke-interface {v0}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {}, Lcom/osp/app/util/ap;->a()Lcom/osp/app/util/ap;

    invoke-static {p0, v0, v3}, Lcom/osp/app/util/ap;->a(Landroid/content/Context;Ljava/lang/String;I)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    iget v0, p0, Lcom/osp/app/signin/AccountinfoView;->E:I

    iput v0, p0, Lcom/osp/app/signin/AccountinfoView;->aH:I

    iget v0, p0, Lcom/osp/app/signin/AccountinfoView;->D:I

    iput v0, p0, Lcom/osp/app/signin/AccountinfoView;->as:I

    iget v0, p0, Lcom/osp/app/signin/AccountinfoView;->C:I

    iput v0, p0, Lcom/osp/app/signin/AccountinfoView;->O:I

    iget-object v0, p0, Lcom/osp/app/signin/AccountinfoView;->M:Landroid/app/Dialog;

    check-cast v0, Landroid/app/DatePickerDialog;

    iget v1, p0, Lcom/osp/app/signin/AccountinfoView;->aH:I

    iget v2, p0, Lcom/osp/app/signin/AccountinfoView;->as:I

    iget v3, p0, Lcom/osp/app/signin/AccountinfoView;->O:I

    invoke-virtual {v0, v1, v2, v3}, Landroid/app/DatePickerDialog;->updateDate(III)V

    :goto_5
    return-void

    :cond_1
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v5, p0, Lcom/osp/app/signin/AccountinfoView;->as:I

    invoke-static {v5}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v0, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_0

    :cond_2
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v5, p0, Lcom/osp/app/signin/AccountinfoView;->O:I

    invoke-static {v5}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v0, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_1

    :cond_3
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v4, v7}, Ljava/util/Calendar;->get(I)I

    move-result v5

    invoke-static {v5}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v1, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    goto/16 :goto_2

    :cond_4
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v4, v8}, Ljava/util/Calendar;->get(I)I

    move-result v4

    invoke-static {v4}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    goto/16 :goto_3

    :cond_5
    move v2, v3

    goto/16 :goto_4

    :cond_6
    invoke-static {p0}, Lcom/osp/app/util/ad;->q(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v2

    new-instance v3, Ljava/text/SimpleDateFormat;

    invoke-direct {v3, v2}, Ljava/text/SimpleDateFormat;-><init>(Ljava/lang/String;)V

    new-instance v2, Ljava/util/GregorianCalendar;

    iget v4, p0, Lcom/osp/app/signin/AccountinfoView;->aH:I

    iget v5, p0, Lcom/osp/app/signin/AccountinfoView;->as:I

    iget v6, p0, Lcom/osp/app/signin/AccountinfoView;->O:I

    invoke-direct {v2, v4, v5, v6}, Ljava/util/GregorianCalendar;-><init>(III)V

    invoke-virtual {v2}, Ljava/util/Calendar;->getTime()Ljava/util/Date;

    move-result-object v2

    invoke-virtual {v3, v2}, Ljava/text/SimpleDateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v2

    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "AccountinfoView::updateDisplay inputDate : "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v3, " nowDate : "

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " dateString : "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/osp/app/signin/AccountinfoView;->Y:Landroid/widget/TextView;

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v0, p0, Lcom/osp/app/signin/AccountinfoView;->aD:Landroid/widget/TextView;

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v0, p0, Lcom/osp/app/signin/AccountinfoView;->Y:Landroid/widget/TextView;

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setContentDescription(Ljava/lang/CharSequence;)V

    iget-object v0, p0, Lcom/osp/app/signin/AccountinfoView;->aD:Landroid/widget/TextView;

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setContentDescription(Ljava/lang/CharSequence;)V

    goto/16 :goto_5
.end method

.method static synthetic f(Lcom/osp/app/signin/AccountinfoView;)Z
    .locals 1

    .prologue
    .line 96
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/osp/app/signin/AccountinfoView;->aN:Z

    return v0
.end method

.method static synthetic g(Lcom/osp/app/signin/AccountinfoView;)Landroid/app/AlertDialog;
    .locals 1

    .prologue
    .line 96
    iget-object v0, p0, Lcom/osp/app/signin/AccountinfoView;->F:Landroid/app/AlertDialog;

    return-object v0
.end method

.method static synthetic h(Lcom/osp/app/signin/AccountinfoView;)Landroid/widget/CheckBox;
    .locals 1

    .prologue
    .line 96
    iget-object v0, p0, Lcom/osp/app/signin/AccountinfoView;->J:Landroid/widget/CheckBox;

    return-object v0
.end method

.method static synthetic i(Lcom/osp/app/signin/AccountinfoView;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 96
    iget-object v0, p0, Lcom/osp/app/signin/AccountinfoView;->aK:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic j(Lcom/osp/app/signin/AccountinfoView;)Lcom/osp/app/signin/SignUpinfo;
    .locals 1

    .prologue
    .line 96
    iget-object v0, p0, Lcom/osp/app/signin/AccountinfoView;->d:Lcom/osp/app/signin/SignUpinfo;

    return-object v0
.end method

.method static synthetic k(Lcom/osp/app/signin/AccountinfoView;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 96
    iget-object v0, p0, Lcom/osp/app/signin/AccountinfoView;->at:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic l(Lcom/osp/app/signin/AccountinfoView;)Landroid/content/Intent;
    .locals 1

    .prologue
    .line 96
    iget-object v0, p0, Lcom/osp/app/signin/AccountinfoView;->ap:Landroid/content/Intent;

    return-object v0
.end method

.method static synthetic m(Lcom/osp/app/signin/AccountinfoView;)Z
    .locals 1

    .prologue
    .line 96
    iget-boolean v0, p0, Lcom/osp/app/signin/AccountinfoView;->Q:Z

    return v0
.end method

.method static synthetic n(Lcom/osp/app/signin/AccountinfoView;)Z
    .locals 1

    .prologue
    .line 96
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/osp/app/signin/AccountinfoView;->Q:Z

    return v0
.end method

.method static synthetic o(Lcom/osp/app/signin/AccountinfoView;)Landroid/app/AlertDialog;
    .locals 1

    .prologue
    .line 96
    iget-object v0, p0, Lcom/osp/app/signin/AccountinfoView;->G:Landroid/app/AlertDialog;

    return-object v0
.end method

.method static synthetic p(Lcom/osp/app/signin/AccountinfoView;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 96
    iget-object v0, p0, Lcom/osp/app/signin/AccountinfoView;->e:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic q(Lcom/osp/app/signin/AccountinfoView;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 96
    iget-object v0, p0, Lcom/osp/app/signin/AccountinfoView;->K:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic r(Lcom/osp/app/signin/AccountinfoView;)V
    .locals 3

    .prologue
    .line 96
    new-instance v0, Landroid/content/Intent;

    const-string v1, "com.msc.action.samsungaccount.resignin_with_signout"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    sget v1, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v2, 0xa

    if-le v1, v2, :cond_0

    const v1, 0x10008000

    invoke-virtual {v0, v1}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    :cond_0
    const-string v1, "client_id"

    iget-object v2, p0, Lcom/osp/app/signin/AccountinfoView;->K:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    const-string v1, "client_secret"

    iget-object v2, p0, Lcom/osp/app/signin/AccountinfoView;->L:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    const-string v1, "OSP_VER"

    const-string v2, "OSP_02"

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    const/16 v1, 0xcd

    invoke-virtual {p0, v0, v1}, Lcom/osp/app/signin/AccountinfoView;->startActivityForResult(Landroid/content/Intent;I)V

    return-void
.end method

.method static synthetic s(Lcom/osp/app/signin/AccountinfoView;)Lcom/osp/app/signin/kd;
    .locals 1

    .prologue
    .line 96
    iget-object v0, p0, Lcom/osp/app/signin/AccountinfoView;->al:Lcom/osp/app/signin/kd;

    return-object v0
.end method

.method static synthetic t(Lcom/osp/app/signin/AccountinfoView;)V
    .locals 12

    .prologue
    const/4 v6, 0x3

    const v5, 0x7f090159

    const/4 v11, 0x2

    const/4 v10, 0x1

    const/4 v7, 0x0

    .line 96
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v0, "AIV"

    const-string v1, "AccountinfoView::initializeComponent"

    invoke-static {v0, v1}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;)V

    const v0, 0x7f0c0002

    invoke-virtual {p0, v0}, Lcom/osp/app/signin/AccountinfoView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "* "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const v2, 0x7f09008c

    invoke-virtual {p0, v2}, Lcom/osp/app/signin/AccountinfoView;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    const v0, 0x7f0c0014

    invoke-virtual {p0, v0}, Lcom/osp/app/signin/AccountinfoView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const v2, 0x7f090020

    invoke-virtual {p0, v2}, Lcom/osp/app/signin/AccountinfoView;->getText(I)Ljava/lang/CharSequence;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " *"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    const v0, 0x7f0c0004

    invoke-virtual {p0, v0}, Lcom/osp/app/signin/AccountinfoView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/osp/app/signin/AccountinfoView;->aF:Landroid/widget/TextView;

    iget-object v0, p0, Lcom/osp/app/signin/AccountinfoView;->d:Lcom/osp/app/signin/SignUpinfo;

    invoke-virtual {v0}, Lcom/osp/app/signin/SignUpinfo;->f()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-static {}, Lcom/osp/device/b;->b()Lcom/osp/device/b;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/osp/device/b;->i(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_0

    const v0, 0x7f0c00b4

    invoke-virtual {p0, v0}, Lcom/osp/app/signin/AccountinfoView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-static {v0, v7, v7, v7, v7}, Lcom/msc/sa/c/d;->a(Landroid/view/View;IIII)V

    :cond_0
    iget-object v0, p0, Lcom/osp/app/signin/AccountinfoView;->d:Lcom/osp/app/signin/SignUpinfo;

    invoke-virtual {v0}, Lcom/osp/app/signin/SignUpinfo;->f()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_e

    iget-object v0, p0, Lcom/osp/app/signin/AccountinfoView;->d:Lcom/osp/app/signin/SignUpinfo;

    invoke-virtual {v0}, Lcom/osp/app/signin/SignUpinfo;->f()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v0

    if-lez v0, :cond_e

    iget-object v0, p0, Lcom/osp/app/signin/AccountinfoView;->d:Lcom/osp/app/signin/SignUpinfo;

    invoke-virtual {v0}, Lcom/osp/app/signin/SignUpinfo;->g()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_e

    iget-object v0, p0, Lcom/osp/app/signin/AccountinfoView;->d:Lcom/osp/app/signin/SignUpinfo;

    invoke-virtual {v0}, Lcom/osp/app/signin/SignUpinfo;->g()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v0

    if-lez v0, :cond_e

    const v0, 0x7f0c0008

    invoke-virtual {p0, v0}, Lcom/osp/app/signin/AccountinfoView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    if-eqz v0, :cond_1

    invoke-virtual {v0, v7}, Landroid/widget/LinearLayout;->setVisibility(I)V

    const v0, 0x7f0c000b

    invoke-virtual {p0, v0}, Lcom/osp/app/signin/AccountinfoView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    if-eqz v0, :cond_1

    invoke-virtual {v0, v7}, Landroid/view/View;->setVisibility(I)V

    :cond_1
    const v0, 0x7f0c0003

    invoke-virtual {p0, v0}, Lcom/osp/app/signin/AccountinfoView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    if-eqz v0, :cond_2

    const v1, 0x7f090183

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    :cond_2
    iget-object v0, p0, Lcom/osp/app/signin/AccountinfoView;->aF:Landroid/widget/TextView;

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/osp/app/signin/AccountinfoView;->aF:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/osp/app/signin/AccountinfoView;->d:Lcom/osp/app/signin/SignUpinfo;

    invoke-virtual {v1}, Lcom/osp/app/signin/SignUpinfo;->f()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    :cond_3
    invoke-virtual {p0}, Lcom/osp/app/signin/AccountinfoView;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v0, 0x7f0c0009

    invoke-virtual {p0, v0}, Lcom/osp/app/signin/AccountinfoView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    invoke-static {p0}, Lcom/osp/app/util/an;->a(Lcom/osp/app/util/BaseActivity;)Lcom/osp/app/util/an;

    const v2, 0x7f070008

    invoke-static {v2}, Lcom/osp/app/util/an;->a(I)I

    move-result v2

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setTextColor(I)V

    const v0, 0x7f0c000a

    invoke-virtual {p0, v0}, Lcom/osp/app/signin/AccountinfoView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    if-eqz v0, :cond_4

    iget-object v1, p0, Lcom/osp/app/signin/AccountinfoView;->d:Lcom/osp/app/signin/SignUpinfo;

    invoke-virtual {v1}, Lcom/osp/app/signin/SignUpinfo;->g()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    :cond_4
    :goto_0
    const v0, 0x7f0c0024

    invoke-virtual {p0, v0}, Lcom/osp/app/signin/AccountinfoView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcom/osp/app/signin/AccountinfoView;->T:Landroid/widget/LinearLayout;

    iget-object v0, p0, Lcom/osp/app/signin/AccountinfoView;->T:Landroid/widget/LinearLayout;

    if-nez v0, :cond_5

    const v0, 0x7f0c0152

    invoke-virtual {p0, v0}, Lcom/osp/app/signin/AccountinfoView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcom/osp/app/signin/AccountinfoView;->T:Landroid/widget/LinearLayout;

    :cond_5
    iget-object v0, p0, Lcom/osp/app/signin/AccountinfoView;->az:Landroid/widget/ImageView;

    if-eqz v0, :cond_6

    iget-object v0, p0, Lcom/osp/app/signin/AccountinfoView;->az:Landroid/widget/ImageView;

    invoke-virtual {v0, p0}, Landroid/widget/ImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    :cond_6
    iget-object v0, p0, Lcom/osp/app/signin/AccountinfoView;->d:Lcom/osp/app/signin/SignUpinfo;

    if-eqz v0, :cond_7

    iget-object v0, p0, Lcom/osp/app/signin/AccountinfoView;->d:Lcom/osp/app/signin/SignUpinfo;

    invoke-virtual {v0}, Lcom/osp/app/signin/SignUpinfo;->i()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_13

    const-string v0, ""

    iget-object v1, p0, Lcom/osp/app/signin/AccountinfoView;->d:Lcom/osp/app/signin/SignUpinfo;

    invoke-virtual {v1}, Lcom/osp/app/signin/SignUpinfo;->i()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_12

    iget-object v0, p0, Lcom/osp/app/signin/AccountinfoView;->d:Lcom/osp/app/signin/SignUpinfo;

    invoke-virtual {v0}, Lcom/osp/app/signin/SignUpinfo;->i()Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x4

    invoke-virtual {v0, v7, v1}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/osp/app/signin/AccountinfoView;->d:Lcom/osp/app/signin/SignUpinfo;

    invoke-virtual {v1}, Lcom/osp/app/signin/SignUpinfo;->i()Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x4

    const/4 v3, 0x6

    invoke-virtual {v1, v2, v3}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/osp/app/signin/AccountinfoView;->d:Lcom/osp/app/signin/SignUpinfo;

    invoke-virtual {v2}, Lcom/osp/app/signin/SignUpinfo;->i()Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x6

    const/16 v4, 0x8

    invoke-virtual {v2, v3, v4}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v2

    invoke-static {v0}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lcom/osp/app/signin/AccountinfoView;->aH:I

    invoke-static {v1}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lcom/osp/app/signin/AccountinfoView;->as:I

    invoke-static {v2}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lcom/osp/app/signin/AccountinfoView;->O:I

    invoke-virtual {p0}, Lcom/osp/app/signin/AccountinfoView;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const-string v1, "date_format"

    invoke-static {v0, v1}, Landroid/provider/Settings$System;->getString(Landroid/content/ContentResolver;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    const-string v1, "MM-dd-yyyy"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_10

    const-string v0, "%02d/%02d/%04d"

    new-array v1, v6, [Ljava/lang/Object;

    iget v2, p0, Lcom/osp/app/signin/AccountinfoView;->as:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v1, v7

    iget v2, p0, Lcom/osp/app/signin/AccountinfoView;->O:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v1, v10

    iget v2, p0, Lcom/osp/app/signin/AccountinfoView;->aH:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v1, v11

    invoke-static {v0, v1}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    :goto_1
    iget v1, p0, Lcom/osp/app/signin/AccountinfoView;->as:I

    add-int/lit8 v1, v1, -0x1

    iput v1, p0, Lcom/osp/app/signin/AccountinfoView;->as:I

    iget-object v1, p0, Lcom/osp/app/signin/AccountinfoView;->Y:Landroid/widget/TextView;

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v1, p0, Lcom/osp/app/signin/AccountinfoView;->aD:Landroid/widget/TextView;

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v1, p0, Lcom/osp/app/signin/AccountinfoView;->Y:Landroid/widget/TextView;

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setContentDescription(Ljava/lang/CharSequence;)V

    iget-object v1, p0, Lcom/osp/app/signin/AccountinfoView;->aD:Landroid/widget/TextView;

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setContentDescription(Ljava/lang/CharSequence;)V

    :cond_7
    :goto_2
    iget-object v0, p0, Lcom/osp/app/signin/AccountinfoView;->W:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v10}, Landroid/widget/LinearLayout;->setFocusable(Z)V

    iget-object v0, p0, Lcom/osp/app/signin/AccountinfoView;->W:Landroid/widget/LinearLayout;

    invoke-virtual {v0, p0}, Landroid/widget/LinearLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object v0, p0, Lcom/osp/app/signin/AccountinfoView;->W:Landroid/widget/LinearLayout;

    new-instance v1, Lcom/osp/app/signin/bw;

    invoke-direct {v1, p0}, Lcom/osp/app/signin/bw;-><init>(Lcom/osp/app/signin/AccountinfoView;)V

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object v0, p0, Lcom/osp/app/signin/AccountinfoView;->aE:Landroid/widget/ImageView;

    invoke-virtual {v0, v10}, Landroid/widget/ImageView;->setFocusable(Z)V

    iget-object v0, p0, Lcom/osp/app/signin/AccountinfoView;->aE:Landroid/widget/ImageView;

    new-instance v1, Lcom/osp/app/signin/bx;

    invoke-direct {v1, p0}, Lcom/osp/app/signin/bx;-><init>(Lcom/osp/app/signin/AccountinfoView;)V

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object v0, p0, Lcom/osp/app/signin/AccountinfoView;->d:Lcom/osp/app/signin/SignUpinfo;

    invoke-virtual {v0}, Lcom/osp/app/signin/SignUpinfo;->j()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_8

    const-string v0, ""

    iget-object v1, p0, Lcom/osp/app/signin/AccountinfoView;->d:Lcom/osp/app/signin/SignUpinfo;

    invoke-virtual {v1}, Lcom/osp/app/signin/SignUpinfo;->j()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_15

    :cond_8
    invoke-static {p0}, Lcom/osp/common/util/i;->g(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v0

    if-nez v0, :cond_14

    invoke-static {}, Lcom/osp/common/util/i;->a()Lcom/osp/common/util/i;

    invoke-static {p0}, Lcom/osp/common/util/i;->f(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/osp/app/signin/AccountinfoView;->aC:Landroid/widget/TextView;

    invoke-static {}, Lcom/osp/common/util/i;->a()Lcom/osp/common/util/i;

    invoke-static {p0, v0}, Lcom/osp/common/util/i;->a(Landroid/content/Context;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    :goto_3
    iget-object v0, p0, Lcom/osp/app/signin/AccountinfoView;->J:Landroid/widget/CheckBox;

    if-eqz v0, :cond_9

    iget-object v0, p0, Lcom/osp/app/signin/AccountinfoView;->J:Landroid/widget/CheckBox;

    invoke-virtual {v0, v5}, Landroid/widget/CheckBox;->setText(I)V

    iget-object v0, p0, Lcom/osp/app/signin/AccountinfoView;->J:Landroid/widget/CheckBox;

    invoke-virtual {p0, v5}, Lcom/osp/app/signin/AccountinfoView;->getText(I)Ljava/lang/CharSequence;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/CheckBox;->setContentDescription(Ljava/lang/CharSequence;)V

    iget-object v0, p0, Lcom/osp/app/signin/AccountinfoView;->J:Landroid/widget/CheckBox;

    iget-object v1, p0, Lcom/osp/app/signin/AccountinfoView;->aQ:Landroid/view/View$OnKeyListener;

    invoke-virtual {v0, v1}, Landroid/widget/CheckBox;->setOnKeyListener(Landroid/view/View$OnKeyListener;)V

    iget-object v0, p0, Lcom/osp/app/signin/AccountinfoView;->J:Landroid/widget/CheckBox;

    iget-object v1, p0, Lcom/osp/app/signin/AccountinfoView;->d:Lcom/osp/app/signin/SignUpinfo;

    invoke-virtual {v1}, Lcom/osp/app/signin/SignUpinfo;->l()Z

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/CheckBox;->setChecked(Z)V

    :cond_9
    const v0, 0x7f0c0022

    invoke-virtual {p0, v0}, Lcom/osp/app/signin/AccountinfoView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    if-eqz v0, :cond_a

    invoke-virtual {v0, v5}, Landroid/widget/TextView;->setText(I)V

    invoke-virtual {p0, v5}, Lcom/osp/app/signin/AccountinfoView;->getText(I)Ljava/lang/CharSequence;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setContentDescription(Ljava/lang/CharSequence;)V

    :cond_a
    iget-object v1, p0, Lcom/osp/app/signin/AccountinfoView;->ay:Landroid/widget/TextView;

    iget-object v0, p0, Lcom/osp/app/signin/AccountinfoView;->d:Lcom/osp/app/signin/SignUpinfo;

    invoke-virtual {v0}, Lcom/osp/app/signin/SignUpinfo;->l()Z

    move-result v0

    if-eqz v0, :cond_16

    const v0, 0x7f09002e

    invoke-virtual {p0, v0}, Lcom/osp/app/signin/AccountinfoView;->getString(I)Ljava/lang/String;

    move-result-object v0

    :goto_4
    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v0, p0, Lcom/osp/app/signin/AccountinfoView;->az:Landroid/widget/ImageView;

    invoke-virtual {v0, p0}, Landroid/widget/ImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object v0, p0, Lcom/osp/app/signin/AccountinfoView;->az:Landroid/widget/ImageView;

    invoke-virtual {v0, v10}, Landroid/widget/ImageView;->setFocusable(Z)V

    iget-object v0, p0, Lcom/osp/app/signin/AccountinfoView;->J:Landroid/widget/CheckBox;

    new-instance v1, Lcom/osp/app/signin/bk;

    invoke-direct {v1, p0}, Lcom/osp/app/signin/bk;-><init>(Lcom/osp/app/signin/AccountinfoView;)V

    invoke-virtual {v0, v1}, Landroid/widget/CheckBox;->setOnCheckedChangeListener(Landroid/widget/CompoundButton$OnCheckedChangeListener;)V

    invoke-static {}, Lcom/osp/device/b;->b()Lcom/osp/device/b;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/osp/device/b;->i(Landroid/content/Context;)Z

    move-result v0

    if-nez v0, :cond_b

    const v0, 0x7f0c0029

    invoke-virtual {p0, v0}, Lcom/osp/app/signin/AccountinfoView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lcom/osp/app/signin/AccountinfoView;->I:Landroid/widget/Button;

    iget-object v0, p0, Lcom/osp/app/signin/AccountinfoView;->I:Landroid/widget/Button;

    new-instance v1, Lcom/osp/app/signin/bv;

    invoke-direct {v1, p0}, Lcom/osp/app/signin/bv;-><init>(Lcom/osp/app/signin/AccountinfoView;)V

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    :cond_b
    iget-object v0, p0, Lcom/osp/app/signin/AccountinfoView;->al:Lcom/osp/app/signin/kd;

    if-eqz v0, :cond_26

    iget-object v0, p0, Lcom/osp/app/signin/AccountinfoView;->al:Lcom/osp/app/signin/kd;

    invoke-virtual {v0}, Lcom/osp/app/signin/kd;->b()I

    iget-object v0, p0, Lcom/osp/app/signin/AccountinfoView;->al:Lcom/osp/app/signin/kd;

    invoke-virtual {v0}, Lcom/osp/app/signin/kd;->a()Ljava/util/ArrayList;

    move-result-object v0

    if-eqz v0, :cond_26

    iget-object v0, p0, Lcom/osp/app/signin/AccountinfoView;->al:Lcom/osp/app/signin/kd;

    invoke-virtual {v0}, Lcom/osp/app/signin/kd;->a()Ljava/util/ArrayList;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v8

    :cond_c
    :goto_5
    invoke-interface {v8}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_26

    invoke-interface {v8}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "AccountinfoView::initializeComponent cmpString : "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;)V

    iget-object v1, p0, Lcom/osp/app/signin/AccountinfoView;->al:Lcom/osp/app/signin/kd;

    iget-boolean v1, v1, Lcom/osp/app/signin/kd;->c:Z

    if-eqz v1, :cond_18

    const-string v1, "prefixName"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_18

    iget-object v0, p0, Lcom/osp/app/signin/AccountinfoView;->al:Lcom/osp/app/signin/kd;

    iget-object v0, v0, Lcom/osp/app/signin/kd;->u:Ljava/util/ArrayList;

    iput-object v0, p0, Lcom/osp/app/signin/AccountinfoView;->au:Ljava/util/ArrayList;

    iget-object v0, p0, Lcom/osp/app/signin/AccountinfoView;->d:Lcom/osp/app/signin/SignUpinfo;

    invoke-virtual {v0}, Lcom/osp/app/signin/SignUpinfo;->n()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_17

    iget-object v0, p0, Lcom/osp/app/signin/AccountinfoView;->d:Lcom/osp/app/signin/SignUpinfo;

    invoke-virtual {v0}, Lcom/osp/app/signin/SignUpinfo;->n()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v0

    if-lez v0, :cond_17

    iget-object v0, p0, Lcom/osp/app/signin/AccountinfoView;->au:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v9

    move v6, v7

    :goto_6
    if-ge v6, v9, :cond_c

    iget-object v0, p0, Lcom/osp/app/signin/AccountinfoView;->d:Lcom/osp/app/signin/SignUpinfo;

    invoke-virtual {v0}, Lcom/osp/app/signin/SignUpinfo;->n()Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/osp/app/signin/AccountinfoView;->au:Ljava/util/ArrayList;

    invoke-virtual {v1, v6}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_d

    iget-object v1, p0, Lcom/osp/app/signin/AccountinfoView;->U:Landroid/view/ViewGroup;

    const v0, 0x7f09003d

    invoke-virtual {p0, v0}, Lcom/osp/app/signin/AccountinfoView;->getString(I)Ljava/lang/String;

    move-result-object v2

    iget-object v0, p0, Lcom/osp/app/signin/AccountinfoView;->au:Ljava/util/ArrayList;

    invoke-virtual {v0, v6}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    iget-object v0, p0, Lcom/osp/app/signin/AccountinfoView;->al:Lcom/osp/app/signin/kd;

    invoke-virtual {v0}, Lcom/osp/app/signin/kd;->d()Z

    move-result v4

    const/16 v5, 0x74

    move-object v0, p0

    invoke-direct/range {v0 .. v5}, Lcom/osp/app/signin/AccountinfoView;->a(Landroid/view/ViewGroup;Ljava/lang/String;Ljava/lang/String;ZI)Landroid/widget/EditText;

    :cond_d
    add-int/lit8 v0, v6, 0x1

    move v6, v0

    goto :goto_6

    :cond_e
    iget-object v0, p0, Lcom/osp/app/signin/AccountinfoView;->aF:Landroid/widget/TextView;

    if-eqz v0, :cond_4

    iget-object v0, p0, Lcom/osp/app/signin/AccountinfoView;->d:Lcom/osp/app/signin/SignUpinfo;

    invoke-virtual {v0}, Lcom/osp/app/signin/SignUpinfo;->e()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_f

    iget-object v0, p0, Lcom/osp/app/signin/AccountinfoView;->d:Lcom/osp/app/signin/SignUpinfo;

    invoke-virtual {v0}, Lcom/osp/app/signin/SignUpinfo;->e()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v0

    if-lez v0, :cond_f

    iget-object v0, p0, Lcom/osp/app/signin/AccountinfoView;->aF:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/osp/app/signin/AccountinfoView;->d:Lcom/osp/app/signin/SignUpinfo;

    invoke-virtual {v1}, Lcom/osp/app/signin/SignUpinfo;->e()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto/16 :goto_0

    :cond_f
    iget-object v0, p0, Lcom/osp/app/signin/AccountinfoView;->aF:Landroid/widget/TextView;

    invoke-static {p0}, Lcom/osp/app/util/ad;->h(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto/16 :goto_0

    :cond_10
    const-string v1, "dd-MM-yyyy"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_11

    const-string v0, "%02d/%02d/%04d"

    new-array v1, v6, [Ljava/lang/Object;

    iget v2, p0, Lcom/osp/app/signin/AccountinfoView;->O:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v1, v7

    iget v2, p0, Lcom/osp/app/signin/AccountinfoView;->as:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v1, v10

    iget v2, p0, Lcom/osp/app/signin/AccountinfoView;->aH:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v1, v11

    invoke-static {v0, v1}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_1

    :cond_11
    const-string v0, "%04d/%02d/%02d"

    new-array v1, v6, [Ljava/lang/Object;

    iget v2, p0, Lcom/osp/app/signin/AccountinfoView;->aH:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v1, v7

    iget v2, p0, Lcom/osp/app/signin/AccountinfoView;->as:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v1, v10

    iget v2, p0, Lcom/osp/app/signin/AccountinfoView;->O:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v1, v11

    invoke-static {v0, v1}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_1

    :cond_12
    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object v0

    invoke-virtual {v0, v10}, Ljava/util/Calendar;->get(I)I

    move-result v1

    iput v1, p0, Lcom/osp/app/signin/AccountinfoView;->aH:I

    invoke-virtual {v0, v11}, Ljava/util/Calendar;->get(I)I

    move-result v1

    iput v1, p0, Lcom/osp/app/signin/AccountinfoView;->as:I

    const/4 v1, 0x5

    invoke-virtual {v0, v1}, Ljava/util/Calendar;->get(I)I

    move-result v0

    iput v0, p0, Lcom/osp/app/signin/AccountinfoView;->O:I

    goto/16 :goto_2

    :cond_13
    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object v0

    invoke-virtual {v0, v10}, Ljava/util/Calendar;->get(I)I

    move-result v1

    iput v1, p0, Lcom/osp/app/signin/AccountinfoView;->aH:I

    invoke-virtual {v0, v11}, Ljava/util/Calendar;->get(I)I

    move-result v1

    iput v1, p0, Lcom/osp/app/signin/AccountinfoView;->as:I

    const/4 v1, 0x5

    invoke-virtual {v0, v1}, Ljava/util/Calendar;->get(I)I

    move-result v0

    iput v0, p0, Lcom/osp/app/signin/AccountinfoView;->O:I

    goto/16 :goto_2

    :cond_14
    iget-object v1, p0, Lcom/osp/app/signin/AccountinfoView;->aC:Landroid/widget/TextView;

    invoke-static {}, Lcom/osp/common/util/i;->a()Lcom/osp/common/util/i;

    invoke-static {v0}, Lcom/osp/common/util/i;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto/16 :goto_3

    :cond_15
    invoke-static {}, Lcom/osp/common/util/i;->a()Lcom/osp/common/util/i;

    iget-object v0, p0, Lcom/osp/app/signin/AccountinfoView;->d:Lcom/osp/app/signin/SignUpinfo;

    invoke-virtual {v0}, Lcom/osp/app/signin/SignUpinfo;->j()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/osp/common/util/i;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/osp/app/signin/AccountinfoView;->aC:Landroid/widget/TextView;

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto/16 :goto_3

    :cond_16
    const v0, 0x7f09002f

    invoke-virtual {p0, v0}, Lcom/osp/app/signin/AccountinfoView;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_4

    :cond_17
    iget-object v1, p0, Lcom/osp/app/signin/AccountinfoView;->U:Landroid/view/ViewGroup;

    const v0, 0x7f09003d

    invoke-virtual {p0, v0}, Lcom/osp/app/signin/AccountinfoView;->getString(I)Ljava/lang/String;

    move-result-object v2

    const v0, 0x7f09003d

    invoke-virtual {p0, v0}, Lcom/osp/app/signin/AccountinfoView;->getString(I)Ljava/lang/String;

    move-result-object v3

    iget-object v0, p0, Lcom/osp/app/signin/AccountinfoView;->al:Lcom/osp/app/signin/kd;

    invoke-virtual {v0}, Lcom/osp/app/signin/kd;->d()Z

    move-result v4

    const/16 v5, 0x74

    move-object v0, p0

    invoke-direct/range {v0 .. v5}, Lcom/osp/app/signin/AccountinfoView;->a(Landroid/view/ViewGroup;Ljava/lang/String;Ljava/lang/String;ZI)Landroid/widget/EditText;

    goto/16 :goto_5

    :cond_18
    iget-object v1, p0, Lcom/osp/app/signin/AccountinfoView;->al:Lcom/osp/app/signin/kd;

    iget-boolean v1, v1, Lcom/osp/app/signin/kd;->e:Z

    if-eqz v1, :cond_19

    const-string v1, "familyName"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_19

    iget-object v1, p0, Lcom/osp/app/signin/AccountinfoView;->U:Landroid/view/ViewGroup;

    const v0, 0x7f090039

    invoke-virtual {p0, v0}, Lcom/osp/app/signin/AccountinfoView;->getString(I)Ljava/lang/String;

    move-result-object v2

    iget-object v0, p0, Lcom/osp/app/signin/AccountinfoView;->d:Lcom/osp/app/signin/SignUpinfo;

    invoke-virtual {v0}, Lcom/osp/app/signin/SignUpinfo;->p()Ljava/lang/String;

    move-result-object v3

    iget-object v0, p0, Lcom/osp/app/signin/AccountinfoView;->al:Lcom/osp/app/signin/kd;

    invoke-virtual {v0}, Lcom/osp/app/signin/kd;->h()Z

    move-result v4

    const/16 v5, 0x76

    move-object v0, p0

    invoke-direct/range {v0 .. v5}, Lcom/osp/app/signin/AccountinfoView;->a(Landroid/view/ViewGroup;Ljava/lang/String;Ljava/lang/String;ZI)Landroid/widget/EditText;

    move-result-object v0

    iput-object v0, p0, Lcom/osp/app/signin/AccountinfoView;->aa:Landroid/widget/EditText;

    goto/16 :goto_5

    :cond_19
    iget-object v1, p0, Lcom/osp/app/signin/AccountinfoView;->al:Lcom/osp/app/signin/kd;

    iget-boolean v1, v1, Lcom/osp/app/signin/kd;->d:Z

    if-eqz v1, :cond_1a

    const-string v1, "givenName"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_1a

    iget-object v1, p0, Lcom/osp/app/signin/AccountinfoView;->U:Landroid/view/ViewGroup;

    const v0, 0x7f09002a

    invoke-virtual {p0, v0}, Lcom/osp/app/signin/AccountinfoView;->getString(I)Ljava/lang/String;

    move-result-object v2

    iget-object v0, p0, Lcom/osp/app/signin/AccountinfoView;->d:Lcom/osp/app/signin/SignUpinfo;

    invoke-virtual {v0}, Lcom/osp/app/signin/SignUpinfo;->o()Ljava/lang/String;

    move-result-object v3

    iget-object v0, p0, Lcom/osp/app/signin/AccountinfoView;->al:Lcom/osp/app/signin/kd;

    invoke-virtual {v0}, Lcom/osp/app/signin/kd;->f()Z

    move-result v4

    const/16 v5, 0x75

    move-object v0, p0

    invoke-direct/range {v0 .. v5}, Lcom/osp/app/signin/AccountinfoView;->a(Landroid/view/ViewGroup;Ljava/lang/String;Ljava/lang/String;ZI)Landroid/widget/EditText;

    move-result-object v0

    iput-object v0, p0, Lcom/osp/app/signin/AccountinfoView;->ac:Landroid/widget/EditText;

    goto/16 :goto_5

    :cond_1a
    iget-object v1, p0, Lcom/osp/app/signin/AccountinfoView;->al:Lcom/osp/app/signin/kd;

    iget-boolean v1, v1, Lcom/osp/app/signin/kd;->f:Z

    if-eqz v1, :cond_1b

    const-string v1, "localityText"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_1b

    iget-object v1, p0, Lcom/osp/app/signin/AccountinfoView;->U:Landroid/view/ViewGroup;

    const v0, 0x7f09001a

    invoke-virtual {p0, v0}, Lcom/osp/app/signin/AccountinfoView;->getString(I)Ljava/lang/String;

    move-result-object v2

    iget-object v0, p0, Lcom/osp/app/signin/AccountinfoView;->d:Lcom/osp/app/signin/SignUpinfo;

    invoke-virtual {v0}, Lcom/osp/app/signin/SignUpinfo;->q()Ljava/lang/String;

    move-result-object v3

    iget-object v0, p0, Lcom/osp/app/signin/AccountinfoView;->al:Lcom/osp/app/signin/kd;

    invoke-virtual {v0}, Lcom/osp/app/signin/kd;->i()Z

    move-result v4

    const/16 v5, 0x77

    move-object v0, p0

    invoke-direct/range {v0 .. v5}, Lcom/osp/app/signin/AccountinfoView;->a(Landroid/view/ViewGroup;Ljava/lang/String;Ljava/lang/String;ZI)Landroid/widget/EditText;

    move-result-object v0

    iput-object v0, p0, Lcom/osp/app/signin/AccountinfoView;->ad:Landroid/widget/EditText;

    goto/16 :goto_5

    :cond_1b
    iget-object v1, p0, Lcom/osp/app/signin/AccountinfoView;->al:Lcom/osp/app/signin/kd;

    iget-boolean v1, v1, Lcom/osp/app/signin/kd;->g:Z

    if-eqz v1, :cond_1c

    const-string v1, "postalCodeText"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_1c

    iget-object v1, p0, Lcom/osp/app/signin/AccountinfoView;->U:Landroid/view/ViewGroup;

    const v0, 0x7f090049

    invoke-virtual {p0, v0}, Lcom/osp/app/signin/AccountinfoView;->getString(I)Ljava/lang/String;

    move-result-object v2

    iget-object v0, p0, Lcom/osp/app/signin/AccountinfoView;->d:Lcom/osp/app/signin/SignUpinfo;

    invoke-virtual {v0}, Lcom/osp/app/signin/SignUpinfo;->r()Ljava/lang/String;

    move-result-object v3

    iget-object v0, p0, Lcom/osp/app/signin/AccountinfoView;->al:Lcom/osp/app/signin/kd;

    invoke-virtual {v0}, Lcom/osp/app/signin/kd;->n()Z

    move-result v4

    const/16 v5, 0x78

    move-object v0, p0

    invoke-direct/range {v0 .. v5}, Lcom/osp/app/signin/AccountinfoView;->a(Landroid/view/ViewGroup;Ljava/lang/String;Ljava/lang/String;ZI)Landroid/widget/EditText;

    move-result-object v0

    iput-object v0, p0, Lcom/osp/app/signin/AccountinfoView;->ae:Landroid/widget/EditText;

    goto/16 :goto_5

    :cond_1c
    iget-object v1, p0, Lcom/osp/app/signin/AccountinfoView;->al:Lcom/osp/app/signin/kd;

    iget-boolean v1, v1, Lcom/osp/app/signin/kd;->h:Z

    if-eqz v1, :cond_1d

    const-string v1, "receiveSMSPhoneNumberText"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_1d

    iget-object v1, p0, Lcom/osp/app/signin/AccountinfoView;->U:Landroid/view/ViewGroup;

    const v0, 0x7f090048

    invoke-virtual {p0, v0}, Lcom/osp/app/signin/AccountinfoView;->getString(I)Ljava/lang/String;

    move-result-object v2

    iget-object v0, p0, Lcom/osp/app/signin/AccountinfoView;->d:Lcom/osp/app/signin/SignUpinfo;

    invoke-virtual {v0}, Lcom/osp/app/signin/SignUpinfo;->s()Ljava/lang/String;

    move-result-object v3

    iget-object v0, p0, Lcom/osp/app/signin/AccountinfoView;->al:Lcom/osp/app/signin/kd;

    invoke-virtual {v0}, Lcom/osp/app/signin/kd;->o()Z

    move-result v4

    const/16 v5, 0x79

    move-object v0, p0

    invoke-direct/range {v0 .. v5}, Lcom/osp/app/signin/AccountinfoView;->a(Landroid/view/ViewGroup;Ljava/lang/String;Ljava/lang/String;ZI)Landroid/widget/EditText;

    move-result-object v0

    iput-object v0, p0, Lcom/osp/app/signin/AccountinfoView;->ag:Landroid/widget/EditText;

    goto/16 :goto_5

    :cond_1d
    iget-object v1, p0, Lcom/osp/app/signin/AccountinfoView;->al:Lcom/osp/app/signin/kd;

    iget-boolean v1, v1, Lcom/osp/app/signin/kd;->j:Z

    if-eqz v1, :cond_1e

    const-string v1, "streetText"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_1e

    iget-object v1, p0, Lcom/osp/app/signin/AccountinfoView;->U:Landroid/view/ViewGroup;

    const v0, 0x7f090017

    invoke-virtual {p0, v0}, Lcom/osp/app/signin/AccountinfoView;->getString(I)Ljava/lang/String;

    move-result-object v2

    iget-object v0, p0, Lcom/osp/app/signin/AccountinfoView;->d:Lcom/osp/app/signin/SignUpinfo;

    invoke-virtual {v0}, Lcom/osp/app/signin/SignUpinfo;->t()Ljava/lang/String;

    move-result-object v3

    iget-object v0, p0, Lcom/osp/app/signin/AccountinfoView;->al:Lcom/osp/app/signin/kd;

    invoke-virtual {v0}, Lcom/osp/app/signin/kd;->j()Z

    move-result v4

    const/16 v5, 0x7a

    move-object v0, p0

    invoke-direct/range {v0 .. v5}, Lcom/osp/app/signin/AccountinfoView;->a(Landroid/view/ViewGroup;Ljava/lang/String;Ljava/lang/String;ZI)Landroid/widget/EditText;

    move-result-object v0

    iput-object v0, p0, Lcom/osp/app/signin/AccountinfoView;->aj:Landroid/widget/EditText;

    goto/16 :goto_5

    :cond_1e
    iget-object v1, p0, Lcom/osp/app/signin/AccountinfoView;->al:Lcom/osp/app/signin/kd;

    iget-boolean v1, v1, Lcom/osp/app/signin/kd;->k:Z

    if-eqz v1, :cond_1f

    const-string v1, "extendedText"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_1f

    iget-object v1, p0, Lcom/osp/app/signin/AccountinfoView;->U:Landroid/view/ViewGroup;

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const v2, 0x7f090017

    invoke-virtual {p0, v2}, Lcom/osp/app/signin/AccountinfoView;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, "2"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    iget-object v0, p0, Lcom/osp/app/signin/AccountinfoView;->d:Lcom/osp/app/signin/SignUpinfo;

    invoke-virtual {v0}, Lcom/osp/app/signin/SignUpinfo;->u()Ljava/lang/String;

    move-result-object v3

    iget-object v0, p0, Lcom/osp/app/signin/AccountinfoView;->al:Lcom/osp/app/signin/kd;

    invoke-virtual {v0}, Lcom/osp/app/signin/kd;->k()Z

    move-result v4

    const/16 v5, 0x7b

    move-object v0, p0

    invoke-direct/range {v0 .. v5}, Lcom/osp/app/signin/AccountinfoView;->a(Landroid/view/ViewGroup;Ljava/lang/String;Ljava/lang/String;ZI)Landroid/widget/EditText;

    move-result-object v0

    iput-object v0, p0, Lcom/osp/app/signin/AccountinfoView;->Z:Landroid/widget/EditText;

    goto/16 :goto_5

    :cond_1f
    iget-object v1, p0, Lcom/osp/app/signin/AccountinfoView;->al:Lcom/osp/app/signin/kd;

    iget-boolean v1, v1, Lcom/osp/app/signin/kd;->l:Z

    if-eqz v1, :cond_20

    const-string v1, "postOfficeBoxNumberText"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_20

    iget-object v1, p0, Lcom/osp/app/signin/AccountinfoView;->U:Landroid/view/ViewGroup;

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const v2, 0x7f090017

    invoke-virtual {p0, v2}, Lcom/osp/app/signin/AccountinfoView;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, "3"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    iget-object v0, p0, Lcom/osp/app/signin/AccountinfoView;->d:Lcom/osp/app/signin/SignUpinfo;

    invoke-virtual {v0}, Lcom/osp/app/signin/SignUpinfo;->v()Ljava/lang/String;

    move-result-object v3

    iget-object v0, p0, Lcom/osp/app/signin/AccountinfoView;->al:Lcom/osp/app/signin/kd;

    invoke-virtual {v0}, Lcom/osp/app/signin/kd;->l()Z

    move-result v4

    const/16 v5, 0x7c

    move-object v0, p0

    invoke-direct/range {v0 .. v5}, Lcom/osp/app/signin/AccountinfoView;->a(Landroid/view/ViewGroup;Ljava/lang/String;Ljava/lang/String;ZI)Landroid/widget/EditText;

    move-result-object v0

    iput-object v0, p0, Lcom/osp/app/signin/AccountinfoView;->af:Landroid/widget/EditText;

    goto/16 :goto_5

    :cond_20
    iget-object v1, p0, Lcom/osp/app/signin/AccountinfoView;->al:Lcom/osp/app/signin/kd;

    iget-boolean v1, v1, Lcom/osp/app/signin/kd;->m:Z

    if-eqz v1, :cond_21

    const-string v1, "regionText"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_21

    iget-object v1, p0, Lcom/osp/app/signin/AccountinfoView;->U:Landroid/view/ViewGroup;

    const v0, 0x7f09004b

    invoke-virtual {p0, v0}, Lcom/osp/app/signin/AccountinfoView;->getString(I)Ljava/lang/String;

    move-result-object v2

    iget-object v0, p0, Lcom/osp/app/signin/AccountinfoView;->d:Lcom/osp/app/signin/SignUpinfo;

    invoke-virtual {v0}, Lcom/osp/app/signin/SignUpinfo;->w()Ljava/lang/String;

    move-result-object v3

    iget-object v0, p0, Lcom/osp/app/signin/AccountinfoView;->al:Lcom/osp/app/signin/kd;

    invoke-virtual {v0}, Lcom/osp/app/signin/kd;->p()Z

    move-result v4

    const/16 v5, 0x7d

    move-object v0, p0

    invoke-direct/range {v0 .. v5}, Lcom/osp/app/signin/AccountinfoView;->a(Landroid/view/ViewGroup;Ljava/lang/String;Ljava/lang/String;ZI)Landroid/widget/EditText;

    move-result-object v0

    iput-object v0, p0, Lcom/osp/app/signin/AccountinfoView;->ah:Landroid/widget/EditText;

    goto/16 :goto_5

    :cond_21
    iget-object v1, p0, Lcom/osp/app/signin/AccountinfoView;->al:Lcom/osp/app/signin/kd;

    iget-boolean v1, v1, Lcom/osp/app/signin/kd;->n:Z

    if-eqz v1, :cond_23

    const-string v1, "genderTypeCode"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_23

    new-array v0, v11, [Ljava/lang/String;

    const v1, 0x7f09003a

    invoke-virtual {p0, v1}, Lcom/osp/app/signin/AccountinfoView;->getString(I)Ljava/lang/String;

    move-result-object v1

    aput-object v1, v0, v7

    const v1, 0x7f090029

    invoke-virtual {p0, v1}, Lcom/osp/app/signin/AccountinfoView;->getString(I)Ljava/lang/String;

    move-result-object v1

    aput-object v1, v0, v10

    iget-object v1, p0, Lcom/osp/app/signin/AccountinfoView;->d:Lcom/osp/app/signin/SignUpinfo;

    invoke-virtual {v1}, Lcom/osp/app/signin/SignUpinfo;->x()Ljava/lang/String;

    move-result-object v1

    const-string v2, "M"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_22

    iget-object v1, p0, Lcom/osp/app/signin/AccountinfoView;->d:Lcom/osp/app/signin/SignUpinfo;

    invoke-virtual {v1}, Lcom/osp/app/signin/SignUpinfo;->x()Ljava/lang/String;

    move-result-object v1

    const-string v2, "F"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_22

    iget-object v1, p0, Lcom/osp/app/signin/AccountinfoView;->U:Landroid/view/ViewGroup;

    const v2, 0x7f09002b

    invoke-virtual {p0, v2}, Lcom/osp/app/signin/AccountinfoView;->getString(I)Ljava/lang/String;

    move-result-object v2

    aget-object v3, v0, v10

    iget-object v0, p0, Lcom/osp/app/signin/AccountinfoView;->al:Lcom/osp/app/signin/kd;

    invoke-virtual {v0}, Lcom/osp/app/signin/kd;->p()Z

    move-result v4

    const/16 v5, 0x7e

    move-object v0, p0

    invoke-direct/range {v0 .. v5}, Lcom/osp/app/signin/AccountinfoView;->a(Landroid/view/ViewGroup;Ljava/lang/String;Ljava/lang/String;ZI)Landroid/widget/EditText;

    move-result-object v0

    iput-object v0, p0, Lcom/osp/app/signin/AccountinfoView;->ab:Landroid/widget/EditText;

    goto/16 :goto_5

    :cond_22
    iget-object v1, p0, Lcom/osp/app/signin/AccountinfoView;->U:Landroid/view/ViewGroup;

    const v2, 0x7f09002b

    invoke-virtual {p0, v2}, Lcom/osp/app/signin/AccountinfoView;->getString(I)Ljava/lang/String;

    move-result-object v2

    aget-object v3, v0, v7

    iget-object v0, p0, Lcom/osp/app/signin/AccountinfoView;->al:Lcom/osp/app/signin/kd;

    invoke-virtual {v0}, Lcom/osp/app/signin/kd;->p()Z

    move-result v4

    const/16 v5, 0x7e

    move-object v0, p0

    invoke-direct/range {v0 .. v5}, Lcom/osp/app/signin/AccountinfoView;->a(Landroid/view/ViewGroup;Ljava/lang/String;Ljava/lang/String;ZI)Landroid/widget/EditText;

    move-result-object v0

    iput-object v0, p0, Lcom/osp/app/signin/AccountinfoView;->ab:Landroid/widget/EditText;

    goto/16 :goto_5

    :cond_23
    iget-object v1, p0, Lcom/osp/app/signin/AccountinfoView;->al:Lcom/osp/app/signin/kd;

    iget-boolean v1, v1, Lcom/osp/app/signin/kd;->o:Z

    if-eqz v1, :cond_24

    const-string v1, "userDisplayName"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_24

    iget-object v1, p0, Lcom/osp/app/signin/AccountinfoView;->U:Landroid/view/ViewGroup;

    const v0, 0x7f090040

    invoke-virtual {p0, v0}, Lcom/osp/app/signin/AccountinfoView;->getString(I)Ljava/lang/String;

    move-result-object v2

    iget-object v0, p0, Lcom/osp/app/signin/AccountinfoView;->d:Lcom/osp/app/signin/SignUpinfo;

    invoke-virtual {v0}, Lcom/osp/app/signin/SignUpinfo;->y()Ljava/lang/String;

    move-result-object v3

    iget-object v0, p0, Lcom/osp/app/signin/AccountinfoView;->al:Lcom/osp/app/signin/kd;

    invoke-virtual {v0}, Lcom/osp/app/signin/kd;->q()Z

    move-result v4

    const/16 v5, 0x7f

    move-object v0, p0

    invoke-direct/range {v0 .. v5}, Lcom/osp/app/signin/AccountinfoView;->a(Landroid/view/ViewGroup;Ljava/lang/String;Ljava/lang/String;ZI)Landroid/widget/EditText;

    move-result-object v0

    iput-object v0, p0, Lcom/osp/app/signin/AccountinfoView;->ak:Landroid/widget/EditText;

    goto/16 :goto_5

    :cond_24
    iget-object v1, p0, Lcom/osp/app/signin/AccountinfoView;->al:Lcom/osp/app/signin/kd;

    iget-boolean v1, v1, Lcom/osp/app/signin/kd;->p:Z

    if-eqz v1, :cond_c

    const-string v1, "relationshipStatusCode"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_c

    new-array v0, v11, [Ljava/lang/String;

    const v1, 0x7f090057

    invoke-virtual {p0, v1}, Lcom/osp/app/signin/AccountinfoView;->getString(I)Ljava/lang/String;

    move-result-object v1

    aput-object v1, v0, v7

    const v1, 0x7f09003c

    invoke-virtual {p0, v1}, Lcom/osp/app/signin/AccountinfoView;->getString(I)Ljava/lang/String;

    move-result-object v1

    aput-object v1, v0, v10

    iget-object v1, p0, Lcom/osp/app/signin/AccountinfoView;->d:Lcom/osp/app/signin/SignUpinfo;

    invoke-virtual {v1}, Lcom/osp/app/signin/SignUpinfo;->z()Ljava/lang/String;

    move-result-object v1

    const-string v2, "1"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_25

    iget-object v1, p0, Lcom/osp/app/signin/AccountinfoView;->U:Landroid/view/ViewGroup;

    const v2, 0x7f09003b

    invoke-virtual {p0, v2}, Lcom/osp/app/signin/AccountinfoView;->getString(I)Ljava/lang/String;

    move-result-object v2

    aget-object v3, v0, v7

    iget-object v0, p0, Lcom/osp/app/signin/AccountinfoView;->al:Lcom/osp/app/signin/kd;

    invoke-virtual {v0}, Lcom/osp/app/signin/kd;->r()Z

    move-result v4

    const/16 v5, 0x80

    move-object v0, p0

    invoke-direct/range {v0 .. v5}, Lcom/osp/app/signin/AccountinfoView;->a(Landroid/view/ViewGroup;Ljava/lang/String;Ljava/lang/String;ZI)Landroid/widget/EditText;

    move-result-object v0

    iput-object v0, p0, Lcom/osp/app/signin/AccountinfoView;->ai:Landroid/widget/EditText;

    goto/16 :goto_5

    :cond_25
    iget-object v1, p0, Lcom/osp/app/signin/AccountinfoView;->d:Lcom/osp/app/signin/SignUpinfo;

    invoke-virtual {v1}, Lcom/osp/app/signin/SignUpinfo;->z()Ljava/lang/String;

    move-result-object v1

    const-string v2, "2"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_c

    iget-object v1, p0, Lcom/osp/app/signin/AccountinfoView;->U:Landroid/view/ViewGroup;

    const v2, 0x7f09003b

    invoke-virtual {p0, v2}, Lcom/osp/app/signin/AccountinfoView;->getString(I)Ljava/lang/String;

    move-result-object v2

    aget-object v3, v0, v10

    iget-object v0, p0, Lcom/osp/app/signin/AccountinfoView;->al:Lcom/osp/app/signin/kd;

    invoke-virtual {v0}, Lcom/osp/app/signin/kd;->r()Z

    move-result v4

    const/16 v5, 0x80

    move-object v0, p0

    invoke-direct/range {v0 .. v5}, Lcom/osp/app/signin/AccountinfoView;->a(Landroid/view/ViewGroup;Ljava/lang/String;Ljava/lang/String;ZI)Landroid/widget/EditText;

    move-result-object v0

    iput-object v0, p0, Lcom/osp/app/signin/AccountinfoView;->ai:Landroid/widget/EditText;

    goto/16 :goto_5

    :cond_26
    iget-boolean v0, p0, Lcom/osp/app/signin/AccountinfoView;->P:Z

    if-eqz v0, :cond_2c

    iput-boolean v10, p0, Lcom/osp/app/signin/AccountinfoView;->aq:Z

    invoke-static {}, Lcom/osp/device/b;->b()Lcom/osp/device/b;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/osp/device/b;->i(Landroid/content/Context;)Z

    move-result v0

    if-nez v0, :cond_27

    iget-object v0, p0, Lcom/osp/app/signin/AccountinfoView;->I:Landroid/widget/Button;

    const v1, 0x7f090052

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setText(I)V

    :cond_27
    iget-object v0, p0, Lcom/osp/app/signin/AccountinfoView;->d:Lcom/osp/app/signin/SignUpinfo;

    if-eqz v0, :cond_2a

    iget-object v0, p0, Lcom/osp/app/signin/AccountinfoView;->d:Lcom/osp/app/signin/SignUpinfo;

    invoke-virtual {v0}, Lcom/osp/app/signin/SignUpinfo;->j()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_2a

    iget-object v0, p0, Lcom/osp/app/signin/AccountinfoView;->d:Lcom/osp/app/signin/SignUpinfo;

    invoke-virtual {v0}, Lcom/osp/app/signin/SignUpinfo;->j()Ljava/lang/String;

    move-result-object v0

    const-string v1, "KOR"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_28

    iget-object v0, p0, Lcom/osp/app/signin/AccountinfoView;->d:Lcom/osp/app/signin/SignUpinfo;

    invoke-virtual {v0}, Lcom/osp/app/signin/SignUpinfo;->j()Ljava/lang/String;

    move-result-object v0

    const-string v1, "CHN"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2a

    :cond_28
    iget-object v0, p0, Lcom/osp/app/signin/AccountinfoView;->d:Lcom/osp/app/signin/SignUpinfo;

    invoke-virtual {v0}, Lcom/osp/app/signin/SignUpinfo;->i()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_29

    iget-object v0, p0, Lcom/osp/app/signin/AccountinfoView;->d:Lcom/osp/app/signin/SignUpinfo;

    invoke-virtual {v0}, Lcom/osp/app/signin/SignUpinfo;->i()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v0

    if-nez v0, :cond_2b

    :cond_29
    iget-object v0, p0, Lcom/osp/app/signin/AccountinfoView;->W:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v10}, Landroid/widget/LinearLayout;->setFocusable(Z)V

    iget-object v0, p0, Lcom/osp/app/signin/AccountinfoView;->W:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v10}, Landroid/widget/LinearLayout;->setEnabled(Z)V

    iget-object v0, p0, Lcom/osp/app/signin/AccountinfoView;->Y:Landroid/widget/TextView;

    invoke-virtual {v0, v10}, Landroid/widget/TextView;->setEnabled(Z)V

    iget-object v0, p0, Lcom/osp/app/signin/AccountinfoView;->X:Landroid/widget/ImageView;

    invoke-static {v10}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/msc/sa/c/d;->a(Landroid/widget/ImageView;Ljava/lang/Boolean;)V

    :cond_2a
    :goto_7
    invoke-direct {p0, v10}, Lcom/osp/app/signin/AccountinfoView;->b(Z)V

    invoke-virtual {p0}, Lcom/osp/app/signin/AccountinfoView;->a()V

    :goto_8
    invoke-virtual {p0}, Lcom/osp/app/signin/AccountinfoView;->getWindow()Landroid/view/Window;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/Window;->getDecorView()Landroid/view/View;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/osp/app/signin/AccountinfoView;->setInputMethodEmoticonDisabled(Landroid/view/View;)V

    return-void

    :cond_2b
    iget-object v0, p0, Lcom/osp/app/signin/AccountinfoView;->W:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v7}, Landroid/widget/LinearLayout;->setFocusable(Z)V

    iget-object v0, p0, Lcom/osp/app/signin/AccountinfoView;->W:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v7}, Landroid/widget/LinearLayout;->setEnabled(Z)V

    iget-object v0, p0, Lcom/osp/app/signin/AccountinfoView;->Y:Landroid/widget/TextView;

    invoke-virtual {v0, v7}, Landroid/widget/TextView;->setEnabled(Z)V

    iget-object v0, p0, Lcom/osp/app/signin/AccountinfoView;->X:Landroid/widget/ImageView;

    invoke-static {v7}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/msc/sa/c/d;->a(Landroid/widget/ImageView;Ljava/lang/Boolean;)V

    goto :goto_7

    :cond_2c
    invoke-direct {p0, v7}, Lcom/osp/app/signin/AccountinfoView;->b(Z)V

    goto :goto_8
.end method

.method static synthetic u(Lcom/osp/app/signin/AccountinfoView;)V
    .locals 4

    .prologue
    .line 96
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v0, "AccountinfoView::ShowSigninActivity"

    invoke-static {v0}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;)V

    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    const-class v1, Lcom/osp/app/signin/SignInView;

    invoke-virtual {v0, p0, v1}, Landroid/content/Intent;->setClass(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;

    const-string v1, "MODE"

    const-string v2, "ADD_ACCOUNT"

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    const-string v1, "key_request_id"

    iget-wide v2, p0, Lcom/osp/app/signin/AccountinfoView;->aJ:J

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;J)Landroid/content/Intent;

    invoke-virtual {p0, v0}, Lcom/osp/app/signin/AccountinfoView;->startActivity(Landroid/content/Intent;)V

    return-void
.end method

.method static synthetic v(Lcom/osp/app/signin/AccountinfoView;)Lcom/osp/app/signin/SignUpinfo;
    .locals 6

    .prologue
    .line 96
    new-instance v1, Lcom/osp/app/signin/SignUpinfo;

    invoke-direct {v1}, Lcom/osp/app/signin/SignUpinfo;-><init>()V

    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v0, "AccountinfoView::PutAccountInfo"

    invoke-static {v0}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/osp/app/signin/AccountinfoView;->aF:Landroid/widget/TextView;

    invoke-virtual {v0}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v0

    invoke-interface {v0}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Lcom/osp/app/signin/SignUpinfo;->b(Ljava/lang/String;)V

    const/4 v0, 0x0

    const-string v2, "%04d%02d%02d"

    const/4 v3, 0x3

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    iget v5, p0, Lcom/osp/app/signin/AccountinfoView;->aH:I

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v3, v4

    const/4 v4, 0x1

    iget v5, p0, Lcom/osp/app/signin/AccountinfoView;->as:I

    add-int/lit8 v5, v5, 0x1

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v3, v4

    const/4 v4, 0x2

    iget v5, p0, Lcom/osp/app/signin/AccountinfoView;->O:I

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v3, v4

    invoke-static {v0, v2, v3}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Lcom/osp/app/signin/SignUpinfo;->f(Ljava/lang/String;)V

    invoke-static {p0}, Lcom/msc/c/e;->d(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Lcom/osp/app/signin/SignUpinfo;->g(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/osp/app/signin/AccountinfoView;->J:Landroid/widget/CheckBox;

    invoke-virtual {v0}, Landroid/widget/CheckBox;->isChecked()Z

    move-result v0

    invoke-virtual {v1, v0}, Lcom/osp/app/signin/SignUpinfo;->b(Z)V

    iget-object v0, p0, Lcom/osp/app/signin/AccountinfoView;->d:Lcom/osp/app/signin/SignUpinfo;

    if-eqz v0, :cond_0

    const-string v0, "OSP_01"

    iget-object v2, p0, Lcom/osp/app/signin/AccountinfoView;->at:Ljava/lang/String;

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_10

    iget-object v0, p0, Lcom/osp/app/signin/AccountinfoView;->d:Lcom/osp/app/signin/SignUpinfo;

    invoke-virtual {v0}, Lcom/osp/app/signin/SignUpinfo;->B()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Lcom/osp/app/signin/SignUpinfo;->w(Ljava/lang/String;)V

    :cond_0
    :goto_0
    iget-object v0, p0, Lcom/osp/app/signin/AccountinfoView;->al:Lcom/osp/app/signin/kd;

    if-eqz v0, :cond_f

    iget-object v0, p0, Lcom/osp/app/signin/AccountinfoView;->al:Lcom/osp/app/signin/kd;

    iget-boolean v0, v0, Lcom/osp/app/signin/kd;->c:Z

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/osp/app/signin/AccountinfoView;->al:Lcom/osp/app/signin/kd;

    iget-object v0, v0, Lcom/osp/app/signin/kd;->u:Ljava/util/ArrayList;

    iput-object v0, p0, Lcom/osp/app/signin/AccountinfoView;->au:Ljava/util/ArrayList;

    iget-object v0, p0, Lcom/osp/app/signin/AccountinfoView;->au:Ljava/util/ArrayList;

    iget v2, p0, Lcom/osp/app/signin/AccountinfoView;->av:I

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-virtual {v1, v0}, Lcom/osp/app/signin/SignUpinfo;->i(Ljava/lang/String;)V

    :cond_1
    iget-object v0, p0, Lcom/osp/app/signin/AccountinfoView;->al:Lcom/osp/app/signin/kd;

    iget-boolean v0, v0, Lcom/osp/app/signin/kd;->e:Z

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/osp/app/signin/AccountinfoView;->aa:Landroid/widget/EditText;

    invoke-virtual {v0}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Lcom/osp/app/signin/SignUpinfo;->k(Ljava/lang/String;)V

    :cond_2
    iget-object v0, p0, Lcom/osp/app/signin/AccountinfoView;->al:Lcom/osp/app/signin/kd;

    iget-boolean v0, v0, Lcom/osp/app/signin/kd;->d:Z

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/osp/app/signin/AccountinfoView;->ac:Landroid/widget/EditText;

    invoke-virtual {v0}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Lcom/osp/app/signin/SignUpinfo;->j(Ljava/lang/String;)V

    :cond_3
    iget-object v0, p0, Lcom/osp/app/signin/AccountinfoView;->al:Lcom/osp/app/signin/kd;

    iget-boolean v0, v0, Lcom/osp/app/signin/kd;->f:Z

    if-eqz v0, :cond_4

    iget-object v0, p0, Lcom/osp/app/signin/AccountinfoView;->ad:Landroid/widget/EditText;

    invoke-virtual {v0}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Lcom/osp/app/signin/SignUpinfo;->l(Ljava/lang/String;)V

    :cond_4
    iget-object v0, p0, Lcom/osp/app/signin/AccountinfoView;->al:Lcom/osp/app/signin/kd;

    iget-boolean v0, v0, Lcom/osp/app/signin/kd;->g:Z

    if-eqz v0, :cond_5

    iget-object v0, p0, Lcom/osp/app/signin/AccountinfoView;->ae:Landroid/widget/EditText;

    invoke-virtual {v0}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Lcom/osp/app/signin/SignUpinfo;->m(Ljava/lang/String;)V

    :cond_5
    iget-object v0, p0, Lcom/osp/app/signin/AccountinfoView;->al:Lcom/osp/app/signin/kd;

    iget-boolean v0, v0, Lcom/osp/app/signin/kd;->h:Z

    if-eqz v0, :cond_6

    iget-object v0, p0, Lcom/osp/app/signin/AccountinfoView;->ag:Landroid/widget/EditText;

    invoke-virtual {v0}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Lcom/osp/app/signin/SignUpinfo;->n(Ljava/lang/String;)V

    :cond_6
    iget-object v0, p0, Lcom/osp/app/signin/AccountinfoView;->al:Lcom/osp/app/signin/kd;

    iget-boolean v0, v0, Lcom/osp/app/signin/kd;->j:Z

    if-eqz v0, :cond_7

    iget-object v0, p0, Lcom/osp/app/signin/AccountinfoView;->aj:Landroid/widget/EditText;

    invoke-virtual {v0}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Lcom/osp/app/signin/SignUpinfo;->o(Ljava/lang/String;)V

    :cond_7
    iget-object v0, p0, Lcom/osp/app/signin/AccountinfoView;->al:Lcom/osp/app/signin/kd;

    iget-boolean v0, v0, Lcom/osp/app/signin/kd;->k:Z

    if-eqz v0, :cond_8

    iget-object v0, p0, Lcom/osp/app/signin/AccountinfoView;->Z:Landroid/widget/EditText;

    invoke-virtual {v0}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Lcom/osp/app/signin/SignUpinfo;->p(Ljava/lang/String;)V

    :cond_8
    iget-object v0, p0, Lcom/osp/app/signin/AccountinfoView;->al:Lcom/osp/app/signin/kd;

    iget-boolean v0, v0, Lcom/osp/app/signin/kd;->l:Z

    if-eqz v0, :cond_9

    iget-object v0, p0, Lcom/osp/app/signin/AccountinfoView;->af:Landroid/widget/EditText;

    invoke-virtual {v0}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Lcom/osp/app/signin/SignUpinfo;->q(Ljava/lang/String;)V

    :cond_9
    iget-object v0, p0, Lcom/osp/app/signin/AccountinfoView;->al:Lcom/osp/app/signin/kd;

    iget-boolean v0, v0, Lcom/osp/app/signin/kd;->m:Z

    if-eqz v0, :cond_a

    iget-object v0, p0, Lcom/osp/app/signin/AccountinfoView;->ah:Landroid/widget/EditText;

    invoke-virtual {v0}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Lcom/osp/app/signin/SignUpinfo;->r(Ljava/lang/String;)V

    :cond_a
    iget-object v0, p0, Lcom/osp/app/signin/AccountinfoView;->al:Lcom/osp/app/signin/kd;

    iget-boolean v0, v0, Lcom/osp/app/signin/kd;->n:Z

    if-eqz v0, :cond_c

    const-string v0, "M"

    iget-object v2, p0, Lcom/osp/app/signin/AccountinfoView;->ab:Landroid/widget/EditText;

    invoke-virtual {v2}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v2

    const v3, 0x7f09003a

    invoke-virtual {p0, v3}, Lcom/osp/app/signin/AccountinfoView;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_11

    const-string v0, "M"

    :cond_b
    :goto_1
    invoke-virtual {v1, v0}, Lcom/osp/app/signin/SignUpinfo;->s(Ljava/lang/String;)V

    :cond_c
    iget-object v0, p0, Lcom/osp/app/signin/AccountinfoView;->al:Lcom/osp/app/signin/kd;

    iget-boolean v0, v0, Lcom/osp/app/signin/kd;->o:Z

    if-eqz v0, :cond_d

    iget-object v0, p0, Lcom/osp/app/signin/AccountinfoView;->ak:Landroid/widget/EditText;

    invoke-virtual {v0}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Lcom/osp/app/signin/SignUpinfo;->t(Ljava/lang/String;)V

    :cond_d
    iget-object v0, p0, Lcom/osp/app/signin/AccountinfoView;->al:Lcom/osp/app/signin/kd;

    iget-boolean v0, v0, Lcom/osp/app/signin/kd;->p:Z

    if-eqz v0, :cond_f

    const-string v0, "1"

    iget-object v2, p0, Lcom/osp/app/signin/AccountinfoView;->ai:Landroid/widget/EditText;

    invoke-virtual {v2}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v2

    const v3, 0x7f090057

    invoke-virtual {p0, v3}, Lcom/osp/app/signin/AccountinfoView;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_12

    const-string v0, "1"

    :cond_e
    :goto_2
    invoke-virtual {v1, v0}, Lcom/osp/app/signin/SignUpinfo;->u(Ljava/lang/String;)V

    :cond_f
    return-object v1

    :cond_10
    iget-object v0, p0, Lcom/osp/app/signin/AccountinfoView;->d:Lcom/osp/app/signin/SignUpinfo;

    invoke-virtual {v0}, Lcom/osp/app/signin/SignUpinfo;->A()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Lcom/osp/app/signin/SignUpinfo;->v(Ljava/lang/String;)V

    goto/16 :goto_0

    :cond_11
    iget-object v2, p0, Lcom/osp/app/signin/AccountinfoView;->ab:Landroid/widget/EditText;

    invoke-virtual {v2}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v2

    const v3, 0x7f090029

    invoke-virtual {p0, v3}, Lcom/osp/app/signin/AccountinfoView;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_b

    const-string v0, "F"

    goto :goto_1

    :cond_12
    iget-object v2, p0, Lcom/osp/app/signin/AccountinfoView;->ai:Landroid/widget/EditText;

    invoke-virtual {v2}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v2

    const v3, 0x7f09003c

    invoke-virtual {p0, v3}, Lcom/osp/app/signin/AccountinfoView;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_e

    const-string v0, "2"

    goto :goto_2
.end method

.method static synthetic w(Lcom/osp/app/signin/AccountinfoView;)Lcom/osp/app/util/n;
    .locals 1

    .prologue
    .line 96
    iget-object v0, p0, Lcom/osp/app/signin/AccountinfoView;->V:Lcom/osp/app/util/n;

    return-object v0
.end method

.method static synthetic x(Lcom/osp/app/signin/AccountinfoView;)Z
    .locals 1

    .prologue
    .line 96
    iget-boolean v0, p0, Lcom/osp/app/signin/AccountinfoView;->P:Z

    return v0
.end method

.method static synthetic y(Lcom/osp/app/signin/AccountinfoView;)V
    .locals 1

    .prologue
    .line 96
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/osp/app/signin/AccountinfoView;->a(Z)V

    return-void
.end method

.method static synthetic z(Lcom/osp/app/signin/AccountinfoView;)I
    .locals 1

    .prologue
    .line 96
    iget v0, p0, Lcom/osp/app/signin/AccountinfoView;->aI:I

    return v0
.end method


# virtual methods
.method public final a()V
    .locals 8

    .prologue
    const v7, 0x7f0c018f

    const/4 v1, 0x0

    const/4 v2, 0x1

    .line 1248
    iget-boolean v0, p0, Lcom/osp/app/signin/AccountinfoView;->P:Z

    if-nez v0, :cond_0

    iget-boolean v0, p0, Lcom/osp/app/signin/AccountinfoView;->aq:Z

    if-eqz v0, :cond_9

    .line 1250
    :cond_0
    iget-object v0, p0, Lcom/osp/app/signin/AccountinfoView;->aF:Landroid/widget/TextView;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/osp/app/signin/AccountinfoView;->aF:Landroid/widget/TextView;

    invoke-virtual {v0}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v0

    invoke-interface {v0}, Ljava/lang/CharSequence;->length()I

    move-result v0

    if-eqz v0, :cond_6

    :cond_1
    iget-object v0, p0, Lcom/osp/app/signin/AccountinfoView;->Y:Landroid/widget/TextView;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/osp/app/signin/AccountinfoView;->Y:Landroid/widget/TextView;

    invoke-virtual {v0}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v0

    invoke-interface {v0}, Ljava/lang/CharSequence;->length()I

    move-result v0

    if-eqz v0, :cond_6

    :cond_2
    iget-object v0, p0, Lcom/osp/app/signin/AccountinfoView;->al:Lcom/osp/app/signin/kd;

    if-eqz v0, :cond_e

    iget-object v0, p0, Lcom/osp/app/signin/AccountinfoView;->aO:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v4

    move v3, v1

    :goto_0
    if-ge v3, v4, :cond_e

    iget-object v0, p0, Lcom/osp/app/signin/AccountinfoView;->aO:Ljava/util/ArrayList;

    invoke-virtual {v0, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/widget/EditText;

    invoke-virtual {v0}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    new-instance v5, Ljava/util/StringTokenizer;

    invoke-virtual {v0}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v5, v0}, Ljava/util/StringTokenizer;-><init>(Ljava/lang/String;)V

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    :goto_1
    invoke-virtual {v5}, Ljava/util/StringTokenizer;->hasMoreTokens()Z

    move-result v6

    if-eqz v6, :cond_3

    invoke-virtual {v5}, Ljava/util/StringTokenizer;->nextToken()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v0, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_1

    :cond_3
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "isFieldHaveData : "

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v0

    if-nez v0, :cond_a

    move v0, v1

    :goto_2
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "AccountinfoView::isFieldHavedata result : "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;)V

    if-eqz v0, :cond_6

    iget-object v0, p0, Lcom/osp/app/signin/AccountinfoView;->aa:Landroid/widget/EditText;

    if-eqz v0, :cond_4

    iget-object v0, p0, Lcom/osp/app/signin/AccountinfoView;->aa:Landroid/widget/EditText;

    invoke-virtual {v0}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_6

    iget-object v0, p0, Lcom/osp/app/signin/AccountinfoView;->aa:Landroid/widget/EditText;

    invoke-virtual {v0}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v3, "N/A"

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_6

    :cond_4
    iget-object v0, p0, Lcom/osp/app/signin/AccountinfoView;->ac:Landroid/widget/EditText;

    if-eqz v0, :cond_5

    iget-object v0, p0, Lcom/osp/app/signin/AccountinfoView;->ac:Landroid/widget/EditText;

    invoke-virtual {v0}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_6

    iget-object v0, p0, Lcom/osp/app/signin/AccountinfoView;->ac:Landroid/widget/EditText;

    invoke-virtual {v0}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v3, "N/A"

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_6

    :cond_5
    iget-object v0, p0, Lcom/osp/app/signin/AccountinfoView;->ae:Landroid/widget/EditText;

    if-eqz v0, :cond_b

    iget-object v0, p0, Lcom/osp/app/signin/AccountinfoView;->ae:Landroid/widget/EditText;

    invoke-virtual {v0}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-interface {v0}, Landroid/text/Editable;->length()I

    move-result v0

    const/4 v3, 0x5

    if-eq v0, v3, :cond_b

    .line 1255
    :cond_6
    iget-object v0, p0, Lcom/osp/app/signin/AccountinfoView;->I:Landroid/widget/Button;

    if-eqz v0, :cond_7

    .line 1257
    iget-object v0, p0, Lcom/osp/app/signin/AccountinfoView;->I:Landroid/widget/Button;

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setEnabled(Z)V

    .line 1259
    :cond_7
    iget-object v0, p0, Lcom/osp/app/signin/AccountinfoView;->x:Landroid/view/Menu;

    if-eqz v0, :cond_8

    iget-object v0, p0, Lcom/osp/app/signin/AccountinfoView;->x:Landroid/view/Menu;

    invoke-interface {v0}, Landroid/view/Menu;->size()I

    move-result v0

    if-le v0, v2, :cond_8

    .line 1261
    iget-object v0, p0, Lcom/osp/app/signin/AccountinfoView;->x:Landroid/view/Menu;

    invoke-interface {v0, v7}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v0

    invoke-interface {v0, v1}, Landroid/view/MenuItem;->setEnabled(Z)Landroid/view/MenuItem;

    .line 1263
    :cond_8
    iget-object v0, p0, Lcom/osp/app/signin/AccountinfoView;->a:Landroid/widget/Button;

    if-eqz v0, :cond_9

    .line 1265
    iget-object v0, p0, Lcom/osp/app/signin/AccountinfoView;->a:Landroid/widget/Button;

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setEnabled(Z)V

    .line 1308
    :cond_9
    :goto_3
    return-void

    .line 1250
    :cond_a
    add-int/lit8 v0, v3, 0x1

    move v3, v0

    goto/16 :goto_0

    .line 1269
    :cond_b
    iget-object v0, p0, Lcom/osp/app/signin/AccountinfoView;->I:Landroid/widget/Button;

    if-eqz v0, :cond_c

    .line 1271
    iget-object v0, p0, Lcom/osp/app/signin/AccountinfoView;->I:Landroid/widget/Button;

    invoke-virtual {v0, v2}, Landroid/widget/Button;->setEnabled(Z)V

    .line 1273
    :cond_c
    iget-object v0, p0, Lcom/osp/app/signin/AccountinfoView;->x:Landroid/view/Menu;

    if-eqz v0, :cond_d

    iget-object v0, p0, Lcom/osp/app/signin/AccountinfoView;->x:Landroid/view/Menu;

    invoke-interface {v0}, Landroid/view/Menu;->size()I

    move-result v0

    if-le v0, v2, :cond_d

    .line 1275
    iget-object v0, p0, Lcom/osp/app/signin/AccountinfoView;->x:Landroid/view/Menu;

    invoke-interface {v0, v7}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v0

    invoke-interface {v0, v2}, Landroid/view/MenuItem;->setEnabled(Z)Landroid/view/MenuItem;

    .line 1277
    :cond_d
    iget-object v0, p0, Lcom/osp/app/signin/AccountinfoView;->a:Landroid/widget/Button;

    if-eqz v0, :cond_9

    .line 1279
    iget-object v0, p0, Lcom/osp/app/signin/AccountinfoView;->a:Landroid/widget/Button;

    invoke-virtual {v0, v2}, Landroid/widget/Button;->setEnabled(Z)V

    goto :goto_3

    :cond_e
    move v0, v2

    goto/16 :goto_2
.end method

.method public final a(Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 3210
    new-instance v0, Landroid/content/Intent;

    const-string v1, "com.msc.action.samsungaccount.web_with_close_button"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 3211
    const-string v1, "ServerUrl"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 3212
    invoke-virtual {p0, v0}, Lcom/osp/app/signin/AccountinfoView;->startActivity(Landroid/content/Intent;)V

    .line 3213
    return-void
.end method

.method protected final b()V
    .locals 4

    .prologue
    .line 4784
    iget-boolean v0, p0, Lcom/osp/app/signin/AccountinfoView;->P:Z

    if-nez v0, :cond_0

    iget-boolean v0, p0, Lcom/osp/app/signin/AccountinfoView;->aq:Z

    if-eqz v0, :cond_3

    :cond_0
    invoke-static {p0}, Lcom/osp/app/util/r;->m(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 4786
    iget-object v0, p0, Lcom/osp/app/signin/AccountinfoView;->x:Landroid/view/Menu;

    if-eqz v0, :cond_1

    .line 4788
    iget-object v0, p0, Lcom/osp/app/signin/AccountinfoView;->x:Landroid/view/Menu;

    invoke-interface {v0}, Landroid/view/Menu;->clear()V

    .line 4790
    :cond_1
    invoke-virtual {p0}, Lcom/osp/app/signin/AccountinfoView;->h()Z

    const v0, 0x7f090019

    invoke-virtual {p0, v0}, Lcom/osp/app/signin/AccountinfoView;->getString(I)Ljava/lang/String;

    move-result-object v0

    const v1, 0x7f090052

    invoke-virtual {p0, v1}, Lcom/osp/app/signin/AccountinfoView;->getString(I)Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/osp/app/signin/AccountinfoView;->c:Landroid/view/View$OnClickListener;

    iget-object v3, p0, Lcom/osp/app/signin/AccountinfoView;->b:Landroid/view/View$OnClickListener;

    invoke-static {p0, v0, v1, v2, v3}, Lcom/msc/sa/c/d;->a(Landroid/app/Activity;Ljava/lang/String;Ljava/lang/String;Landroid/view/View$OnClickListener;Landroid/view/View$OnClickListener;)V

    .line 4797
    const v0, 0x7f0c0082

    invoke-static {p0, v0}, Lcom/msc/sa/c/d;->b(Landroid/app/Activity;I)Landroid/view/View;

    move-result-object v0

    .line 4798
    if-eqz v0, :cond_2

    .line 4800
    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lcom/osp/app/signin/AccountinfoView;->a:Landroid/widget/Button;

    .line 4803
    :cond_2
    invoke-virtual {p0}, Lcom/osp/app/signin/AccountinfoView;->a()V

    .line 4806
    :cond_3
    return-void
.end method

.method protected onActivityResult(IILandroid/content/Intent;)V
    .locals 4

    .prologue
    .line 549
    invoke-super {p0, p1, p2, p3}, Lcom/osp/app/util/BaseActivity;->onActivityResult(IILandroid/content/Intent;)V

    .line 551
    const/16 v0, 0xdc

    if-ne p1, v0, :cond_2

    .line 553
    invoke-virtual {p0, p2, p3}, Lcom/osp/app/signin/AccountinfoView;->a(ILandroid/content/Intent;)V

    .line 577
    :cond_0
    :goto_0
    invoke-virtual {p0}, Lcom/osp/app/signin/AccountinfoView;->finish()V

    .line 579
    :cond_1
    return-void

    .line 555
    :cond_2
    invoke-static {}, Lcom/osp/device/b;->b()Lcom/osp/device/b;

    move-result-object v0

    invoke-virtual {v0}, Lcom/osp/device/b;->a()Z

    move-result v0

    if-eqz v0, :cond_4

    const/16 v0, 0xef

    if-eq p1, v0, :cond_3

    const/16 v0, 0xf0

    if-ne p1, v0, :cond_4

    .line 558
    :cond_3
    const/16 v0, 0x13

    if-ne p2, v0, :cond_1

    .line 560
    invoke-virtual {p0, p2}, Lcom/osp/app/signin/AccountinfoView;->b(I)V

    goto :goto_0

    .line 563
    :cond_4
    const/16 v0, 0xf2

    if-ne p1, v0, :cond_1

    .line 565
    if-eqz p3, :cond_0

    .line 567
    const-string v0, "key_phonenumber"

    invoke-virtual {p3, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 568
    const-string v1, "key_country_calling_code"

    invoke-virtual {p3, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 570
    iget-object v2, p0, Lcom/osp/app/signin/AccountinfoView;->aF:Landroid/widget/TextView;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 571
    iget-object v2, p0, Lcom/osp/app/signin/AccountinfoView;->d:Lcom/osp/app/signin/SignUpinfo;

    invoke-virtual {v2, v1}, Lcom/osp/app/signin/SignUpinfo;->z(Ljava/lang/String;)V

    .line 572
    iget-object v2, p0, Lcom/osp/app/signin/AccountinfoView;->d:Lcom/osp/app/signin/SignUpinfo;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Lcom/osp/app/signin/SignUpinfo;->b(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public onBackPressed()V
    .locals 2

    .prologue
    .line 3186
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v0, "AIV"

    const-string v1, "AccountinfoView::onBackPressed"

    invoke-static {v0, v1}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 3194
    iget-boolean v0, p0, Lcom/osp/app/signin/AccountinfoView;->P:Z

    if-eqz v0, :cond_0

    .line 3196
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/osp/app/signin/AccountinfoView;->a(Z)V

    .line 3206
    :cond_0
    invoke-super {p0}, Lcom/osp/app/util/BaseActivity;->onBackPressed()V

    .line 3207
    return-void
.end method

.method public onClick(Landroid/view/View;)V
    .locals 8

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x0

    .line 3222
    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    .line 3332
    :cond_0
    :goto_0
    return-void

    .line 3230
    :pswitch_0
    iget-object v0, p0, Lcom/osp/app/signin/AccountinfoView;->G:Landroid/app/AlertDialog;

    if-eqz v0, :cond_2

    .line 3234
    :try_start_0
    iget-object v0, p0, Lcom/osp/app/signin/AccountinfoView;->G:Landroid/app/AlertDialog;

    invoke-virtual {v0}, Landroid/app/AlertDialog;->isShowing()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 3236
    iget-object v0, p0, Lcom/osp/app/signin/AccountinfoView;->G:Landroid/app/AlertDialog;

    invoke-virtual {v0}, Landroid/app/AlertDialog;->dismiss()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 3243
    :cond_1
    :goto_1
    iput-object v2, p0, Lcom/osp/app/signin/AccountinfoView;->G:Landroid/app/AlertDialog;

    .line 3247
    :cond_2
    iget-object v0, p0, Lcom/osp/app/signin/AccountinfoView;->G:Landroid/app/AlertDialog;

    if-nez v0, :cond_0

    .line 3251
    :try_start_1
    new-instance v2, Landroid/text/SpannableStringBuilder;

    invoke-direct {v2}, Landroid/text/SpannableStringBuilder;-><init>()V

    .line 3253
    invoke-static {p0}, Lcom/osp/app/util/r;->b(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 3255
    const v0, 0x7f090158

    invoke-virtual {p0, v0}, Lcom/osp/app/signin/AccountinfoView;->getText(I)Ljava/lang/CharSequence;

    move-result-object v0

    invoke-virtual {v2, v0}, Landroid/text/SpannableStringBuilder;->append(Ljava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;

    .line 3257
    invoke-static {}, Lcom/osp/device/b;->b()Lcom/osp/device/b;

    move-result-object v0

    invoke-virtual {v0}, Lcom/osp/device/b;->n()Ljava/lang/String;

    move-result-object v0

    .line 3258
    const-string v3, "450"

    iget-object v4, p0, Lcom/osp/app/signin/AccountinfoView;->aK:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_3

    const-string v3, "KR"

    invoke-virtual {v3, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_6

    .line 3260
    :cond_3
    invoke-virtual {v2}, Landroid/text/SpannableStringBuilder;->clear()V

    .line 3261
    const v0, 0x7f09019d

    invoke-virtual {p0, v0}, Lcom/osp/app/signin/AccountinfoView;->getText(I)Ljava/lang/CharSequence;

    move-result-object v0

    invoke-virtual {v2, v0}, Landroid/text/SpannableStringBuilder;->append(Ljava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;

    .line 3262
    const-string v0, "\n\n"

    invoke-virtual {v2, v0}, Landroid/text/SpannableStringBuilder;->append(Ljava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;

    .line 3263
    const-string v0, "* "

    invoke-virtual {v2, v0}, Landroid/text/SpannableStringBuilder;->append(Ljava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;

    .line 3264
    const v0, 0x7f09019b

    invoke-virtual {p0, v0}, Lcom/osp/app/signin/AccountinfoView;->getText(I)Ljava/lang/CharSequence;

    move-result-object v0

    invoke-virtual {v2, v0}, Landroid/text/SpannableStringBuilder;->append(Ljava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;

    .line 3266
    const/4 v0, 0x0

    invoke-virtual {v2}, Landroid/text/SpannableStringBuilder;->length()I

    move-result v3

    const-class v4, Landroid/text/style/URLSpan;

    invoke-virtual {v2, v0, v3, v4}, Landroid/text/SpannableStringBuilder;->getSpans(IILjava/lang/Class;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Landroid/text/style/URLSpan;

    .line 3268
    array-length v3, v0

    :goto_2
    if-ge v1, v3, :cond_6

    aget-object v4, v0, v1

    .line 3270
    invoke-virtual {v2, v4}, Landroid/text/SpannableStringBuilder;->getSpanStart(Ljava/lang/Object;)I

    move-result v5

    .line 3271
    invoke-virtual {v2, v4}, Landroid/text/SpannableStringBuilder;->getSpanEnd(Ljava/lang/Object;)I

    move-result v6

    .line 3272
    invoke-virtual {v4}, Landroid/text/style/URLSpan;->getURL()Ljava/lang/String;

    move-result-object v7

    .line 3273
    invoke-virtual {v2, v4}, Landroid/text/SpannableStringBuilder;->removeSpan(Ljava/lang/Object;)V

    .line 3274
    new-instance v4, Lcom/osp/app/signin/AccountinfoView$15;

    invoke-direct {v4, p0, v7}, Lcom/osp/app/signin/AccountinfoView$15;-><init>(Lcom/osp/app/signin/AccountinfoView;Ljava/lang/String;)V

    .line 3290
    const/4 v7, 0x0

    invoke-virtual {v2, v4, v5, v6, v7}, Landroid/text/SpannableStringBuilder;->setSpan(Ljava/lang/Object;III)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1

    .line 3268
    add-int/lit8 v1, v1, 0x1

    goto :goto_2

    .line 3238
    :catch_0
    move-exception v0

    :try_start_2
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto/16 :goto_1

    .line 3243
    :catchall_0
    move-exception v0

    iput-object v2, p0, Lcom/osp/app/signin/AccountinfoView;->G:Landroid/app/AlertDialog;

    throw v0

    .line 3295
    :cond_4
    :try_start_3
    invoke-static {}, Lcom/osp/device/b;->b()Lcom/osp/device/b;

    move-result-object v0

    invoke-virtual {v0}, Lcom/osp/device/b;->n()Ljava/lang/String;

    move-result-object v0

    .line 3296
    const-string v1, "302"

    iget-object v3, p0, Lcom/osp/app/signin/AccountinfoView;->aK:Ljava/lang/String;

    invoke-virtual {v1, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_5

    const-string v1, "CA"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_7

    .line 3301
    :cond_5
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const v1, 0x7f090174

    invoke-virtual {p0, v1}, Lcom/osp/app/signin/AccountinfoView;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const v1, 0x7f090178

    invoke-virtual {p0, v1}, Lcom/osp/app/signin/AccountinfoView;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const v1, 0x7f090175

    invoke-virtual {p0, v1}, Lcom/osp/app/signin/AccountinfoView;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 3312
    :goto_3
    invoke-virtual {v2, v0}, Landroid/text/SpannableStringBuilder;->append(Ljava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;

    .line 3315
    :cond_6
    new-instance v0, Landroid/app/AlertDialog$Builder;

    invoke-direct {v0, p0}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    const v1, 0x7f09008e

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/app/AlertDialog$Builder;->setMessage(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    const v1, 0x7f090042

    new-instance v2, Lcom/osp/app/signin/bp;

    invoke-direct {v2, p0}, Lcom/osp/app/signin/bp;-><init>(Lcom/osp/app/signin/AccountinfoView;)V

    invoke-virtual {v0, v1, v2}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v0

    iput-object v0, p0, Lcom/osp/app/signin/AccountinfoView;->G:Landroid/app/AlertDialog;

    .line 3323
    iget-object v0, p0, Lcom/osp/app/signin/AccountinfoView;->G:Landroid/app/AlertDialog;

    invoke-virtual {v0}, Landroid/app/AlertDialog;->show()V

    .line 3324
    iget-object v0, p0, Lcom/osp/app/signin/AccountinfoView;->G:Landroid/app/AlertDialog;

    const v1, 0x102000b

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 3325
    if-eqz v0, :cond_0

    .line 3327
    invoke-static {}, Landroid/text/method/LinkMovementMethod;->getInstance()Landroid/text/method/MovementMethod;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setMovementMethod(Landroid/text/method/MovementMethod;)V
    :try_end_3
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_1

    goto/16 :goto_0

    .line 3329
    :catch_1
    move-exception v0

    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    goto/16 :goto_0

    .line 3304
    :cond_7
    :try_start_4
    iget-object v1, p0, Lcom/osp/app/signin/AccountinfoView;->aK:Ljava/lang/String;

    invoke-static {v1}, Lcom/osp/app/util/r;->a(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_8

    const-string v1, "US"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_9

    .line 3306
    :cond_8
    const v0, 0x7f09019f

    invoke-virtual {p0, v0}, Lcom/osp/app/signin/AccountinfoView;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_3

    .line 3309
    :cond_9
    const v0, 0x7f09019e

    invoke-virtual {p0, v0}, Lcom/osp/app/signin/AccountinfoView;->getString(I)Ljava/lang/String;
    :try_end_4
    .catch Ljava/lang/Exception; {:try_start_4 .. :try_end_4} :catch_1

    move-result-object v0

    goto :goto_3

    .line 3222
    :pswitch_data_0
    .packed-switch 0x7f0c0026
        :pswitch_0
    .end packed-switch
.end method

.method public onConfigurationChanged(Landroid/content/res/Configuration;)V
    .locals 2

    .prologue
    .line 540
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v0, "AIV"

    const-string v1, "onConfigurationChanged()"

    invoke-static {v0, v1}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 542
    iget v0, p1, Landroid/content/res/Configuration;->orientation:I

    invoke-direct {p0, v0}, Lcom/osp/app/signin/AccountinfoView;->a(I)V

    .line 544
    invoke-super {p0, p1}, Lcom/osp/app/util/BaseActivity;->onConfigurationChanged(Landroid/content/res/Configuration;)V

    .line 545
    return-void
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 11

    .prologue
    const/4 v10, 0x3

    const v9, 0x7f070004

    const v8, 0x7f070008

    const/4 v7, 0x1

    const/4 v6, 0x0

    .line 2367
    invoke-super {p0, p1}, Lcom/osp/app/util/BaseActivity;->onCreate(Landroid/os/Bundle;)V

    .line 2371
    invoke-virtual {p0}, Lcom/osp/app/signin/AccountinfoView;->getApplication()Landroid/app/Application;

    move-result-object v0

    check-cast v0, Lcom/osp/app/signin/SamsungService;

    invoke-virtual {v0, p0}, Lcom/osp/app/signin/SamsungService;->a(Landroid/app/Activity;)V

    .line 2373
    invoke-virtual {p0}, Lcom/osp/app/signin/AccountinfoView;->getIntent()Landroid/content/Intent;

    move-result-object v0

    iput-object v0, p0, Lcom/osp/app/signin/AccountinfoView;->ap:Landroid/content/Intent;

    .line 2377
    iget-object v0, p0, Lcom/osp/app/signin/AccountinfoView;->ap:Landroid/content/Intent;

    const-string v1, "is_authorized_internal_action_for_accountinfo"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 2378
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eq v1, v7, :cond_0

    const-string v1, "TIME_PATTERN"

    invoke-static {v1, v0}, Lcom/osp/app/util/ad;->b(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_2

    .line 2381
    :cond_0
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v0, "AIV"

    const-string v1, "Start AccountInfo Fail. Incorrect enCryptedCode"

    invoke-static {v0, v1}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 2383
    invoke-virtual {p0}, Lcom/osp/app/signin/AccountinfoView;->finish()V

    .line 2553
    :cond_1
    :goto_0
    return-void

    .line 2387
    :cond_2
    iget-object v0, p0, Lcom/osp/app/signin/AccountinfoView;->ap:Landroid/content/Intent;

    invoke-virtual {v0}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/osp/app/signin/AccountinfoView;->ap:Landroid/content/Intent;

    invoke-virtual {v0}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    const-string v1, "com.msc.action.samsungaccount.savemandatoryinfo_external"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 2390
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v0, "external value check START"

    invoke-static {v0}, Lcom/osp/app/util/aq;->b(Ljava/lang/String;)V

    .line 2392
    const/4 v0, 0x6

    new-array v0, v0, [Ljava/lang/String;

    const-string v1, "client_id"

    aput-object v1, v0, v6

    const-string v1, "client_secret"

    aput-object v1, v0, v7

    const/4 v1, 0x2

    const-string v2, "OSP_VER"

    aput-object v2, v0, v1

    const-string v1, "direct_modify"

    aput-object v1, v0, v10

    const/4 v1, 0x4

    const-string v2, "check_list"

    aput-object v2, v0, v1

    const/4 v1, 0x5

    const-string v2, "key_no_notification"

    aput-object v2, v0, v1

    .line 2395
    invoke-virtual {p0, v0}, Lcom/osp/app/signin/AccountinfoView;->a([Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 2403
    :cond_3
    invoke-virtual {p0}, Lcom/osp/app/signin/AccountinfoView;->getLayoutInflater()Landroid/view/LayoutInflater;

    move-result-object v0

    iput-object v0, p0, Lcom/osp/app/signin/AccountinfoView;->am:Landroid/view/LayoutInflater;

    .line 2404
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v0, "AIV"

    const-string v1, "AccountinfoView::onCreate"

    invoke-static {v0, v1}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 2406
    invoke-static {}, Lcom/osp/common/util/i;->a()Lcom/osp/common/util/i;

    invoke-static {p0}, Lcom/osp/common/util/i;->f(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/osp/app/signin/AccountinfoView;->aK:Ljava/lang/String;

    .line 2413
    invoke-static {}, Lcom/osp/app/util/n;->a()Lcom/osp/app/util/n;

    move-result-object v0

    iput-object v0, p0, Lcom/osp/app/signin/AccountinfoView;->V:Lcom/osp/app/util/n;

    .line 2415
    iget-object v0, p0, Lcom/osp/app/signin/AccountinfoView;->ap:Landroid/content/Intent;

    const-string v1, "client_id"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/osp/app/signin/AccountinfoView;->K:Ljava/lang/String;

    iget-object v0, p0, Lcom/osp/app/signin/AccountinfoView;->ap:Landroid/content/Intent;

    const-string v1, "client_secret"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/osp/app/signin/AccountinfoView;->L:Ljava/lang/String;

    iget-object v0, p0, Lcom/osp/app/signin/AccountinfoView;->ap:Landroid/content/Intent;

    const-string v1, "client_secret"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/osp/app/signin/AccountinfoView;->ap:Landroid/content/Intent;

    const-string v2, "account_mode"

    invoke-virtual {v1, v2}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/osp/app/signin/AccountinfoView;->e:Ljava/lang/String;

    iget-object v1, p0, Lcom/osp/app/signin/AccountinfoView;->ap:Landroid/content/Intent;

    const-string v2, "OSP_VER"

    invoke-virtual {v1, v2}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/osp/app/signin/AccountinfoView;->at:Ljava/lang/String;

    iget-object v1, p0, Lcom/osp/app/signin/AccountinfoView;->ap:Landroid/content/Intent;

    const-string v2, "direct_modify"

    invoke-virtual {v1, v2, v6}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v1

    iput-boolean v1, p0, Lcom/osp/app/signin/AccountinfoView;->P:Z

    iget-object v1, p0, Lcom/osp/app/signin/AccountinfoView;->ap:Landroid/content/Intent;

    const-string v2, "check_list"

    invoke-virtual {v1, v2, v6}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v1

    iput v1, p0, Lcom/osp/app/signin/AccountinfoView;->aI:I

    iget-object v1, p0, Lcom/osp/app/signin/AccountinfoView;->ap:Landroid/content/Intent;

    const-string v2, "service_name"

    invoke-virtual {v1, v2}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    const-string v2, "SamsungApps"

    invoke-virtual {v2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_4

    iput-boolean v7, p0, Lcom/osp/app/signin/AccountinfoView;->ar:Z

    :cond_4
    iget-boolean v2, p0, Lcom/osp/app/signin/AccountinfoView;->P:Z

    if-eqz v2, :cond_5

    iget-object v2, p0, Lcom/osp/app/signin/AccountinfoView;->ap:Landroid/content/Intent;

    const-string v3, "authcode"

    invoke-virtual {v2, v3}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/osp/app/signin/AccountinfoView;->B:Ljava/lang/String;

    :cond_5
    iget-object v2, p0, Lcom/osp/app/signin/AccountinfoView;->ap:Landroid/content/Intent;

    const-string v3, "BG_WhoareU"

    invoke-virtual {v2, v3}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/osp/app/signin/AccountinfoView;->aG:Ljava/lang/String;

    const-string v2, "BG_mode"

    iget-object v3, p0, Lcom/osp/app/signin/AccountinfoView;->aG:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_6

    iget-object v2, p0, Lcom/osp/app/signin/AccountinfoView;->ap:Landroid/content/Intent;

    const-string v3, "mypackage"

    invoke-virtual {v2, v3}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/osp/app/signin/AccountinfoView;->aB:Ljava/lang/String;

    iget-object v2, p0, Lcom/osp/app/signin/AccountinfoView;->ap:Landroid/content/Intent;

    const-string v3, "key_request_id"

    const-wide/16 v4, 0x0

    invoke-virtual {v2, v3, v4, v5}, Landroid/content/Intent;->getLongExtra(Ljava/lang/String;J)J

    move-result-wide v2

    iput-wide v2, p0, Lcom/osp/app/signin/AccountinfoView;->aJ:J

    :cond_6
    iget-object v2, p0, Lcom/osp/app/signin/AccountinfoView;->at:Ljava/lang/String;

    invoke-static {v2}, Lcom/osp/app/util/ad;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/osp/app/signin/AccountinfoView;->at:Ljava/lang/String;

    iget-object v2, p0, Lcom/osp/app/signin/AccountinfoView;->ap:Landroid/content/Intent;

    const-string v3, "key_return_result"

    invoke-virtual {v2, v3, v6}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v2

    iput-boolean v2, p0, Lcom/osp/app/signin/AccountinfoView;->aL:Z

    iget-object v2, p0, Lcom/osp/app/signin/AccountinfoView;->ap:Landroid/content/Intent;

    const-string v3, "key_no_notification"

    invoke-virtual {v2, v3, v6}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v2

    iput-boolean v2, p0, Lcom/osp/app/signin/AccountinfoView;->aM:Z

    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "AccountinfoView::paramFromServiceApp m_client_id : "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v3, p0, Lcom/osp/app/signin/AccountinfoView;->K:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;)V

    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "AccountinfoView::paramFromServiceApp m_client_secret : "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;)V

    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v2, "AccountinfoView::paramFromServiceApp m_account_mode : "

    invoke-direct {v0, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, Lcom/osp/app/signin/AccountinfoView;->e:Ljava/lang/String;

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;)V

    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v2, "AccountinfoView::paramFromServiceApp m_OspVersion : "

    invoke-direct {v0, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, Lcom/osp/app/signin/AccountinfoView;->at:Ljava/lang/String;

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;)V

    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v2, "AccountinfoView::paramFromServiceApp m_direct : "

    invoke-direct {v0, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-boolean v2, p0, Lcom/osp/app/signin/AccountinfoView;->P:Z

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;)V

    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v2, "AccountinfoView::paramFromServiceApp m_service_name : "

    invoke-direct {v0, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;)V

    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "AccountinfoView::paramFromServiceApp mAuthCode : "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v1, p0, Lcom/osp/app/signin/AccountinfoView;->B:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;)V

    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "AccountinfoView::paramFromServiceApp m_Sourcepackage : "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v1, p0, Lcom/osp/app/signin/AccountinfoView;->aB:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;)V

    .line 2416
    const/high16 v0, 0x7f030000

    invoke-static {p0}, Lcom/osp/app/util/r;->q(Landroid/content/Context;)Z

    move-result v1

    invoke-virtual {p0, v0, v1}, Lcom/osp/app/signin/AccountinfoView;->a(IZ)V

    .line 2418
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/osp/app/signin/AccountinfoView;->aO:Ljava/util/ArrayList;

    .line 2420
    invoke-virtual {p0}, Lcom/osp/app/signin/AccountinfoView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v0

    iget v0, v0, Landroid/content/res/Configuration;->orientation:I

    invoke-direct {p0, v0}, Lcom/osp/app/signin/AccountinfoView;->a(I)V

    .line 2421
    invoke-virtual {p0}, Lcom/osp/app/signin/AccountinfoView;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v0, 0x7f0c000f

    invoke-virtual {p0, v0}, Lcom/osp/app/signin/AccountinfoView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/osp/app/signin/AccountinfoView;->H:Landroid/view/View;

    const v0, 0x7f0c0003

    invoke-virtual {p0, v0}, Lcom/osp/app/signin/AccountinfoView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    if-eqz v0, :cond_8

    if-eqz v0, :cond_7

    invoke-static {}, Lcom/osp/device/b;->b()Lcom/osp/device/b;

    move-result-object v2

    invoke-virtual {v2}, Lcom/osp/device/b;->a()Z

    move-result v2

    if-eqz v2, :cond_19

    invoke-static {p0}, Lcom/osp/app/util/ad;->h(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Landroid/telephony/PhoneNumberUtils;->isGlobalPhoneNumber(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_19

    const v2, 0x7f090048

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setText(I)V

    :cond_7
    :goto_1
    invoke-static {p0}, Lcom/osp/app/util/an;->a(Lcom/osp/app/util/BaseActivity;)Lcom/osp/app/util/an;

    invoke-static {v8}, Lcom/osp/app/util/an;->a(I)I

    move-result v2

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v2

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setTextColor(I)V

    :cond_8
    const v0, 0x7f0c0004

    invoke-virtual {p0, v0}, Lcom/osp/app/signin/AccountinfoView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/osp/app/signin/AccountinfoView;->aF:Landroid/widget/TextView;

    iget-object v0, p0, Lcom/osp/app/signin/AccountinfoView;->aF:Landroid/widget/TextView;

    if-eqz v0, :cond_9

    iget-object v0, p0, Lcom/osp/app/signin/AccountinfoView;->aF:Landroid/widget/TextView;

    invoke-static {p0}, Lcom/osp/app/util/an;->a(Lcom/osp/app/util/BaseActivity;)Lcom/osp/app/util/an;

    invoke-static {v9}, Lcom/osp/app/util/an;->a(I)I

    move-result v2

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v2

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setTextColor(I)V

    :cond_9
    invoke-static {}, Lcom/osp/device/b;->b()Lcom/osp/device/b;

    move-result-object v0

    invoke-virtual {v0}, Lcom/osp/device/b;->a()Z

    move-result v0

    if-eqz v0, :cond_1a

    invoke-static {p0}, Lcom/osp/app/util/ad;->h(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/telephony/PhoneNumberUtils;->isGlobalPhoneNumber(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1a

    const v0, 0x7f0c0006

    invoke-virtual {p0, v0}, Lcom/osp/app/signin/AccountinfoView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lcom/osp/app/signin/AccountinfoView;->ax:Landroid/widget/Button;

    :goto_2
    const v0, 0x7f0c000c

    invoke-virtual {p0, v0}, Lcom/osp/app/signin/AccountinfoView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcom/osp/app/signin/AccountinfoView;->S:Landroid/widget/LinearLayout;

    const v0, 0x7f0c000d

    invoke-virtual {p0, v0}, Lcom/osp/app/signin/AccountinfoView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    if-eqz v0, :cond_a

    invoke-static {p0}, Lcom/osp/app/util/an;->a(Lcom/osp/app/util/BaseActivity;)Lcom/osp/app/util/an;

    invoke-static {v8}, Lcom/osp/app/util/an;->a(I)I

    move-result v2

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v2

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setTextColor(I)V

    :cond_a
    const v0, 0x7f0c000e

    invoke-virtual {p0, v0}, Lcom/osp/app/signin/AccountinfoView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lcom/osp/app/signin/AccountinfoView;->aw:Landroid/widget/Button;

    const v0, 0x7f0c0010

    invoke-virtual {p0, v0}, Lcom/osp/app/signin/AccountinfoView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcom/osp/app/signin/AccountinfoView;->an:Landroid/widget/LinearLayout;

    const v0, 0x7f0c0011

    invoke-virtual {p0, v0}, Lcom/osp/app/signin/AccountinfoView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    if-eqz v0, :cond_b

    invoke-static {p0}, Lcom/osp/app/util/an;->a(Lcom/osp/app/util/BaseActivity;)Lcom/osp/app/util/an;

    invoke-static {v8}, Lcom/osp/app/util/an;->a(I)I

    move-result v2

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v2

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setTextColor(I)V

    :cond_b
    const v0, 0x7f0c0012

    invoke-virtual {p0, v0}, Lcom/osp/app/signin/AccountinfoView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/osp/app/signin/AccountinfoView;->aD:Landroid/widget/TextView;

    iget-object v0, p0, Lcom/osp/app/signin/AccountinfoView;->aD:Landroid/widget/TextView;

    if-eqz v0, :cond_c

    iget-object v0, p0, Lcom/osp/app/signin/AccountinfoView;->aD:Landroid/widget/TextView;

    invoke-static {p0}, Lcom/osp/app/util/an;->a(Lcom/osp/app/util/BaseActivity;)Lcom/osp/app/util/an;

    invoke-static {v9}, Lcom/osp/app/util/an;->a(I)I

    move-result v2

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v2

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setTextColor(I)V

    :cond_c
    const v0, 0x7f0c0013

    invoke-virtual {p0, v0}, Lcom/osp/app/signin/AccountinfoView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcom/osp/app/signin/AccountinfoView;->R:Landroid/widget/LinearLayout;

    const v0, 0x7f0c0014

    invoke-virtual {p0, v0}, Lcom/osp/app/signin/AccountinfoView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    if-eqz v0, :cond_d

    invoke-static {p0}, Lcom/osp/app/util/an;->a(Lcom/osp/app/util/BaseActivity;)Lcom/osp/app/util/an;

    const v2, 0x7f070006

    invoke-static {v2}, Lcom/osp/app/util/an;->a(I)I

    move-result v2

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v2

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setTextColor(I)V

    :cond_d
    const v0, 0x7f0c0015

    invoke-virtual {p0, v0}, Lcom/osp/app/signin/AccountinfoView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcom/osp/app/signin/AccountinfoView;->W:Landroid/widget/LinearLayout;

    const v0, 0x7f0c0017

    invoke-virtual {p0, v0}, Lcom/osp/app/signin/AccountinfoView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/osp/app/signin/AccountinfoView;->Y:Landroid/widget/TextView;

    const v0, 0x7f0c0016

    invoke-virtual {p0, v0}, Lcom/osp/app/signin/AccountinfoView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/osp/app/signin/AccountinfoView;->X:Landroid/widget/ImageView;

    iget-object v0, p0, Lcom/osp/app/signin/AccountinfoView;->X:Landroid/widget/ImageView;

    if-eqz v0, :cond_e

    iget-object v0, p0, Lcom/osp/app/signin/AccountinfoView;->X:Landroid/widget/ImageView;

    invoke-static {p0}, Lcom/osp/app/util/an;->a(Lcom/osp/app/util/BaseActivity;)Lcom/osp/app/util/an;

    const v2, 0x7f020057

    invoke-static {v2}, Lcom/osp/app/util/an;->a(I)I

    move-result v2

    invoke-virtual {v0, v2}, Landroid/widget/ImageView;->setImageResource(I)V

    :cond_e
    const v0, 0x7f0c0018

    invoke-virtual {p0, v0}, Lcom/osp/app/signin/AccountinfoView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/osp/app/signin/AccountinfoView;->aE:Landroid/widget/ImageView;

    iget-object v0, p0, Lcom/osp/app/signin/AccountinfoView;->aE:Landroid/widget/ImageView;

    if-eqz v0, :cond_f

    iget-object v0, p0, Lcom/osp/app/signin/AccountinfoView;->aE:Landroid/widget/ImageView;

    invoke-static {p0}, Lcom/osp/app/util/an;->a(Lcom/osp/app/util/BaseActivity;)Lcom/osp/app/util/an;

    const v2, 0x7f02003b

    invoke-static {v2}, Lcom/osp/app/util/an;->a(I)I

    move-result v2

    invoke-virtual {v0, v2}, Landroid/widget/ImageView;->setImageResource(I)V

    iget-object v0, p0, Lcom/osp/app/signin/AccountinfoView;->aE:Landroid/widget/ImageView;

    invoke-static {v0}, Lcom/msc/sa/c/d;->a(Landroid/view/View;)V

    :cond_f
    const v0, 0x7f0c001a

    invoke-virtual {p0, v0}, Lcom/osp/app/signin/AccountinfoView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    if-eqz v0, :cond_10

    invoke-static {p0}, Lcom/osp/app/util/an;->a(Lcom/osp/app/util/BaseActivity;)Lcom/osp/app/util/an;

    invoke-static {v8}, Lcom/osp/app/util/an;->a(I)I

    move-result v2

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v2

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setTextColor(I)V

    :cond_10
    const v0, 0x7f0c001b

    invoke-virtual {p0, v0}, Lcom/osp/app/signin/AccountinfoView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/osp/app/signin/AccountinfoView;->aC:Landroid/widget/TextView;

    iget-object v0, p0, Lcom/osp/app/signin/AccountinfoView;->aC:Landroid/widget/TextView;

    if-eqz v0, :cond_11

    iget-object v0, p0, Lcom/osp/app/signin/AccountinfoView;->aC:Landroid/widget/TextView;

    invoke-static {p0}, Lcom/osp/app/util/an;->a(Lcom/osp/app/util/BaseActivity;)Lcom/osp/app/util/an;

    invoke-static {v9}, Lcom/osp/app/util/an;->a(I)I

    move-result v2

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v2

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setTextColor(I)V

    :cond_11
    const v0, 0x7f0c0021

    invoke-virtual {p0, v0}, Lcom/osp/app/signin/AccountinfoView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcom/osp/app/signin/AccountinfoView;->ao:Landroid/widget/LinearLayout;

    const v0, 0x7f0c0022

    invoke-virtual {p0, v0}, Lcom/osp/app/signin/AccountinfoView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    if-eqz v0, :cond_12

    invoke-static {p0}, Lcom/osp/app/util/an;->a(Lcom/osp/app/util/BaseActivity;)Lcom/osp/app/util/an;

    invoke-static {v8}, Lcom/osp/app/util/an;->a(I)I

    move-result v2

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v2

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setTextColor(I)V

    :cond_12
    const v0, 0x7f0c0023

    invoke-virtual {p0, v0}, Lcom/osp/app/signin/AccountinfoView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/osp/app/signin/AccountinfoView;->ay:Landroid/widget/TextView;

    iget-object v0, p0, Lcom/osp/app/signin/AccountinfoView;->ay:Landroid/widget/TextView;

    if-eqz v0, :cond_13

    iget-object v0, p0, Lcom/osp/app/signin/AccountinfoView;->ay:Landroid/widget/TextView;

    invoke-static {p0}, Lcom/osp/app/util/an;->a(Lcom/osp/app/util/BaseActivity;)Lcom/osp/app/util/an;

    invoke-static {v9}, Lcom/osp/app/util/an;->a(I)I

    move-result v2

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v2

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setTextColor(I)V

    :cond_13
    const v0, 0x7f0c0024

    invoke-virtual {p0, v0}, Lcom/osp/app/signin/AccountinfoView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcom/osp/app/signin/AccountinfoView;->T:Landroid/widget/LinearLayout;

    const v0, 0x7f0c0025

    invoke-virtual {p0, v0}, Lcom/osp/app/signin/AccountinfoView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/CheckBox;

    iput-object v0, p0, Lcom/osp/app/signin/AccountinfoView;->J:Landroid/widget/CheckBox;

    iget-object v0, p0, Lcom/osp/app/signin/AccountinfoView;->J:Landroid/widget/CheckBox;

    if-eqz v0, :cond_14

    iget-object v0, p0, Lcom/osp/app/signin/AccountinfoView;->J:Landroid/widget/CheckBox;

    invoke-static {p0}, Lcom/osp/app/util/an;->a(Lcom/osp/app/util/BaseActivity;)Lcom/osp/app/util/an;

    invoke-static {v9}, Lcom/osp/app/util/an;->a(I)I

    move-result v2

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v2

    invoke-virtual {v0, v2}, Landroid/widget/CheckBox;->setTextColor(I)V

    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v2, 0x10

    if-le v0, v2, :cond_14

    invoke-virtual {p0}, Lcom/osp/app/signin/AccountinfoView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v2, 0x7f0800e2

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v0

    float-to-int v0, v0

    iget-object v2, p0, Lcom/osp/app/signin/AccountinfoView;->J:Landroid/widget/CheckBox;

    invoke-static {v2, v0, v6, v6, v6}, Lcom/msc/sa/c/d;->a(Landroid/view/View;IIII)V

    :cond_14
    const v0, 0x7f0c0026

    invoke-virtual {p0, v0}, Lcom/osp/app/signin/AccountinfoView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/osp/app/signin/AccountinfoView;->az:Landroid/widget/ImageView;

    iget-object v0, p0, Lcom/osp/app/signin/AccountinfoView;->az:Landroid/widget/ImageView;

    if-eqz v0, :cond_15

    iget-object v0, p0, Lcom/osp/app/signin/AccountinfoView;->az:Landroid/widget/ImageView;

    invoke-static {p0}, Lcom/osp/app/util/an;->a(Lcom/osp/app/util/BaseActivity;)Lcom/osp/app/util/an;

    const v2, 0x7f02003b

    invoke-static {v2}, Lcom/osp/app/util/an;->a(I)I

    move-result v2

    invoke-virtual {v0, v2}, Landroid/widget/ImageView;->setImageResource(I)V

    iget-object v0, p0, Lcom/osp/app/signin/AccountinfoView;->az:Landroid/widget/ImageView;

    invoke-static {v0}, Lcom/msc/sa/c/d;->a(Landroid/view/View;)V

    :cond_15
    const v0, 0x7f0c0028

    invoke-virtual {p0, v0}, Lcom/osp/app/signin/AccountinfoView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcom/osp/app/signin/AccountinfoView;->aA:Landroid/widget/LinearLayout;

    iget-object v0, p0, Lcom/osp/app/signin/AccountinfoView;->aA:Landroid/widget/LinearLayout;

    if-eqz v0, :cond_16

    iget-object v0, p0, Lcom/osp/app/signin/AccountinfoView;->aA:Landroid/widget/LinearLayout;

    invoke-static {p0}, Lcom/osp/app/util/an;->a(Lcom/osp/app/util/BaseActivity;)Lcom/osp/app/util/an;

    const v2, 0x7f0200d7

    invoke-static {v2}, Lcom/osp/app/util/an;->a(I)I

    move-result v2

    invoke-virtual {v0, v2}, Landroid/widget/LinearLayout;->setBackgroundResource(I)V

    :cond_16
    const v0, 0x7f0c0029

    invoke-virtual {p0, v0}, Lcom/osp/app/signin/AccountinfoView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lcom/osp/app/signin/AccountinfoView;->I:Landroid/widget/Button;

    iget-object v0, p0, Lcom/osp/app/signin/AccountinfoView;->I:Landroid/widget/Button;

    if-eqz v0, :cond_17

    iget-object v0, p0, Lcom/osp/app/signin/AccountinfoView;->I:Landroid/widget/Button;

    invoke-static {p0}, Lcom/osp/app/util/an;->a(Lcom/osp/app/util/BaseActivity;)Lcom/osp/app/util/an;

    const v2, 0x7f020020

    invoke-static {v2}, Lcom/osp/app/util/an;->a(I)I

    move-result v2

    invoke-virtual {v0, v2}, Landroid/widget/Button;->setBackgroundResource(I)V

    iget-object v0, p0, Lcom/osp/app/signin/AccountinfoView;->I:Landroid/widget/Button;

    invoke-static {p0}, Lcom/osp/app/util/an;->a(Lcom/osp/app/util/BaseActivity;)Lcom/osp/app/util/an;

    const v2, 0x7f020030

    invoke-static {v2}, Lcom/osp/app/util/an;->a(I)I

    move-result v2

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getColorStateList(I)Landroid/content/res/ColorStateList;

    move-result-object v2

    invoke-virtual {v0, v2}, Landroid/widget/Button;->setTextColor(Landroid/content/res/ColorStateList;)V

    :cond_17
    invoke-static {}, Lcom/osp/device/b;->b()Lcom/osp/device/b;

    move-result-object v0

    invoke-virtual {v0}, Lcom/osp/device/b;->a()Z

    move-result v0

    if-eqz v0, :cond_18

    const v0, 0x7f0c001e

    invoke-virtual {p0, v0}, Lcom/osp/app/signin/AccountinfoView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    if-eqz v0, :cond_18

    invoke-static {p0}, Lcom/osp/app/util/an;->a(Lcom/osp/app/util/BaseActivity;)Lcom/osp/app/util/an;

    invoke-static {v8}, Lcom/osp/app/util/an;->a(I)I

    move-result v2

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setTextColor(I)V

    .line 2422
    :cond_18
    const v0, 0x7f0c001c

    invoke-virtual {p0, v0}, Lcom/osp/app/signin/AccountinfoView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    iput-object v0, p0, Lcom/osp/app/signin/AccountinfoView;->U:Landroid/view/ViewGroup;

    .line 2425
    invoke-static {p0}, Lcom/osp/app/util/ad;->a(Landroid/content/Context;)Z

    move-result v0

    if-nez v0, :cond_1b

    .line 2427
    invoke-static {}, Lcom/osp/app/util/ap;->a()Lcom/osp/app/util/ap;

    const v0, 0x7f09003e

    invoke-virtual {p0, v0}, Lcom/osp/app/signin/AccountinfoView;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-static {p0, v0, v7}, Lcom/osp/app/util/ap;->a(Landroid/content/Context;Ljava/lang/String;I)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    .line 2428
    invoke-virtual {p0}, Lcom/osp/app/signin/AccountinfoView;->getIntent()Landroid/content/Intent;

    move-result-object v0

    iput-object v0, p0, Lcom/osp/app/signin/AccountinfoView;->ap:Landroid/content/Intent;

    .line 2429
    iget-object v0, p0, Lcom/osp/app/signin/AccountinfoView;->ap:Landroid/content/Intent;

    invoke-virtual {p0, v10, v0}, Lcom/osp/app/signin/AccountinfoView;->a(ILandroid/content/Intent;)V

    .line 2430
    invoke-virtual {p0}, Lcom/osp/app/signin/AccountinfoView;->finish()V

    goto/16 :goto_0

    .line 2421
    :cond_19
    const v2, 0x7f0900e4

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setText(I)V

    goto/16 :goto_1

    :cond_1a
    const v0, 0x7f0c0005

    invoke-virtual {p0, v0}, Lcom/osp/app/signin/AccountinfoView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    const/16 v2, 0x8

    invoke-virtual {v0, v2}, Landroid/widget/LinearLayout;->setVisibility(I)V

    goto/16 :goto_2

    .line 2434
    :cond_1b
    iget-object v0, p0, Lcom/osp/app/signin/AccountinfoView;->aA:Landroid/widget/LinearLayout;

    iget-boolean v1, p0, Lcom/osp/app/signin/AccountinfoView;->P:Z

    sget v2, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v3, 0xe

    if-lt v2, v3, :cond_1f

    invoke-static {}, Lcom/osp/app/signin/SamsungService;->a()Z

    move-result v2

    if-nez v2, :cond_1f

    if-eqz v0, :cond_1c

    const/16 v2, 0x8

    invoke-virtual {v0, v2}, Landroid/widget/LinearLayout;->setVisibility(I)V

    :cond_1c
    invoke-virtual {p0}, Landroid/app/Activity;->getActionBar()Landroid/app/ActionBar;

    move-result-object v0

    if-eqz v0, :cond_1f

    if-nez v1, :cond_1f

    invoke-static {p0}, Lcom/osp/app/util/r;->m(Landroid/content/Context;)Z

    move-result v1

    if-nez v1, :cond_1d

    invoke-static {p0}, Lcom/osp/app/util/r;->k(Landroid/content/Context;)Z

    move-result v1

    if-eqz v1, :cond_1e

    :cond_1d
    invoke-virtual {v0, v6}, Landroid/app/ActionBar;->setDisplayShowHomeEnabled(Z)V

    :cond_1e
    invoke-virtual {v0, v7}, Landroid/app/ActionBar;->setHomeButtonEnabled(Z)V

    invoke-virtual {v0, v7}, Landroid/app/ActionBar;->setDisplayHomeAsUpEnabled(Z)V

    .line 2435
    :cond_1f
    const v0, 0x7f090071

    invoke-virtual {p0, v0}, Lcom/osp/app/signin/AccountinfoView;->setTitle(I)V

    .line 2445
    new-instance v0, Lcom/osp/app/signin/ca;

    invoke-direct {v0, p0}, Lcom/osp/app/signin/ca;-><init>(Lcom/osp/app/signin/AccountinfoView;)V

    iput-object v0, p0, Lcom/osp/app/signin/AccountinfoView;->y:Lcom/osp/app/signin/ca;

    .line 2446
    iget-object v0, p0, Lcom/osp/app/signin/AccountinfoView;->y:Lcom/osp/app/signin/ca;

    invoke-virtual {v0}, Lcom/osp/app/signin/ca;->b()V

    .line 2460
    invoke-static {}, Lcom/osp/device/b;->b()Lcom/osp/device/b;

    move-result-object v0

    invoke-virtual {v0}, Lcom/osp/device/b;->a()Z

    move-result v0

    if-eqz v0, :cond_20

    invoke-static {p0}, Lcom/osp/app/util/ad;->h(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/telephony/PhoneNumberUtils;->isGlobalPhoneNumber(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_20

    .line 2462
    iget-object v0, p0, Lcom/osp/app/signin/AccountinfoView;->ax:Landroid/widget/Button;

    iget-object v1, p0, Lcom/osp/app/signin/AccountinfoView;->aQ:Landroid/view/View$OnKeyListener;

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setOnKeyListener(Landroid/view/View$OnKeyListener;)V

    .line 2463
    iget-object v0, p0, Lcom/osp/app/signin/AccountinfoView;->ax:Landroid/widget/Button;

    new-instance v1, Lcom/osp/app/signin/bl;

    invoke-direct {v1, p0}, Lcom/osp/app/signin/bl;-><init>(Lcom/osp/app/signin/AccountinfoView;)V

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2490
    :cond_20
    iget-object v0, p0, Lcom/osp/app/signin/AccountinfoView;->aw:Landroid/widget/Button;

    iget-object v1, p0, Lcom/osp/app/signin/AccountinfoView;->aQ:Landroid/view/View$OnKeyListener;

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setOnKeyListener(Landroid/view/View$OnKeyListener;)V

    .line 2491
    iget-object v0, p0, Lcom/osp/app/signin/AccountinfoView;->aw:Landroid/widget/Button;

    new-instance v1, Lcom/osp/app/signin/bm;

    invoke-direct {v1, p0}, Lcom/osp/app/signin/bm;-><init>(Lcom/osp/app/signin/AccountinfoView;)V

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2523
    invoke-static {}, Lcom/osp/device/b;->b()Lcom/osp/device/b;

    move-result-object v0

    invoke-virtual {v0}, Lcom/osp/device/b;->a()Z

    move-result v0

    if-eqz v0, :cond_21

    .line 2525
    const v0, 0x7f0c001f

    invoke-virtual {p0, v0}, Lcom/osp/app/signin/AccountinfoView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    .line 2526
    if-eqz v0, :cond_21

    .line 2528
    iget-object v1, p0, Lcom/osp/app/signin/AccountinfoView;->aQ:Landroid/view/View$OnKeyListener;

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setOnKeyListener(Landroid/view/View$OnKeyListener;)V

    .line 2529
    new-instance v1, Lcom/osp/app/signin/bn;

    invoke-direct {v1, p0}, Lcom/osp/app/signin/bn;-><init>(Lcom/osp/app/signin/AccountinfoView;)V

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2543
    :cond_21
    iget-boolean v0, p0, Lcom/osp/app/signin/AccountinfoView;->P:Z

    if-eqz v0, :cond_22

    .line 2545
    invoke-static {p0}, Lcom/osp/app/util/r;->m(Landroid/content/Context;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 2547
    const-string v0, "save_cancel"

    invoke-virtual {p0, v0}, Lcom/osp/app/signin/AccountinfoView;->e(Ljava/lang/String;)V

    goto/16 :goto_0

    .line 2551
    :cond_22
    const-string v0, "edit"

    invoke-virtual {p0, v0}, Lcom/osp/app/signin/AccountinfoView;->e(Ljava/lang/String;)V

    goto/16 :goto_0
.end method

.method public onCreateDialog(I)Landroid/app/Dialog;
    .locals 6

    .prologue
    .line 2565
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "AccountinfoView::onCreateDialog id : "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;)V

    .line 2567
    packed-switch p1, :pswitch_data_0

    .line 2598
    const/4 v0, 0x0

    :goto_0
    return-object v0

    .line 2570
    :pswitch_0
    new-instance v0, Landroid/app/DatePickerDialog;

    iget-object v2, p0, Lcom/osp/app/signin/AccountinfoView;->N:Landroid/app/DatePickerDialog$OnDateSetListener;

    iget v3, p0, Lcom/osp/app/signin/AccountinfoView;->aH:I

    iget v4, p0, Lcom/osp/app/signin/AccountinfoView;->as:I

    iget v5, p0, Lcom/osp/app/signin/AccountinfoView;->O:I

    move-object v1, p0

    invoke-direct/range {v0 .. v5}, Landroid/app/DatePickerDialog;-><init>(Landroid/content/Context;Landroid/app/DatePickerDialog$OnDateSetListener;III)V

    iput-object v0, p0, Lcom/osp/app/signin/AccountinfoView;->M:Landroid/app/Dialog;

    .line 2571
    iget v0, p0, Lcom/osp/app/signin/AccountinfoView;->aH:I

    iput v0, p0, Lcom/osp/app/signin/AccountinfoView;->E:I

    .line 2572
    iget v0, p0, Lcom/osp/app/signin/AccountinfoView;->as:I

    iput v0, p0, Lcom/osp/app/signin/AccountinfoView;->D:I

    .line 2573
    iget v0, p0, Lcom/osp/app/signin/AccountinfoView;->O:I

    iput v0, p0, Lcom/osp/app/signin/AccountinfoView;->C:I

    .line 2586
    iget-object v0, p0, Lcom/osp/app/signin/AccountinfoView;->M:Landroid/app/Dialog;

    check-cast v0, Landroid/app/DatePickerDialog;

    invoke-static {v0}, Lcom/msc/sa/c/d;->a(Landroid/app/DatePickerDialog;)V

    .line 2587
    iget-object v0, p0, Lcom/osp/app/signin/AccountinfoView;->M:Landroid/app/Dialog;

    new-instance v1, Lcom/osp/app/signin/bo;

    invoke-direct {v1, p0}, Lcom/osp/app/signin/bo;-><init>(Lcom/osp/app/signin/AccountinfoView;)V

    invoke-virtual {v0, v1}, Landroid/app/Dialog;->setOnDismissListener(Landroid/content/DialogInterface$OnDismissListener;)V

    .line 2594
    iget-object v0, p0, Lcom/osp/app/signin/AccountinfoView;->M:Landroid/app/Dialog;

    goto :goto_0

    .line 2567
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
    .end packed-switch
.end method

.method protected onDestroy()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 2610
    invoke-super {p0}, Lcom/osp/app/util/BaseActivity;->onDestroy()V

    .line 2612
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v0, "AIV"

    const-string v1, "AccountinfoView::onDestroy"

    invoke-static {v0, v1}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 2618
    iget-object v0, p0, Lcom/osp/app/signin/AccountinfoView;->f:Lcom/osp/app/signin/bz;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/osp/app/signin/AccountinfoView;->f:Lcom/osp/app/signin/bz;

    invoke-virtual {v0}, Lcom/osp/app/signin/bz;->getStatus()Landroid/os/AsyncTask$Status;

    move-result-object v0

    sget-object v1, Landroid/os/AsyncTask$Status;->RUNNING:Landroid/os/AsyncTask$Status;

    if-ne v0, v1, :cond_0

    .line 2620
    iget-object v0, p0, Lcom/osp/app/signin/AccountinfoView;->f:Lcom/osp/app/signin/bz;

    invoke-virtual {v0}, Lcom/osp/app/signin/bz;->d()V

    .line 2621
    iput-object v2, p0, Lcom/osp/app/signin/AccountinfoView;->f:Lcom/osp/app/signin/bz;

    .line 2624
    :cond_0
    iget-object v0, p0, Lcom/osp/app/signin/AccountinfoView;->y:Lcom/osp/app/signin/ca;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/osp/app/signin/AccountinfoView;->y:Lcom/osp/app/signin/ca;

    invoke-virtual {v0}, Lcom/osp/app/signin/ca;->getStatus()Landroid/os/AsyncTask$Status;

    move-result-object v0

    sget-object v1, Landroid/os/AsyncTask$Status;->RUNNING:Landroid/os/AsyncTask$Status;

    if-ne v0, v1, :cond_1

    .line 2626
    iget-object v0, p0, Lcom/osp/app/signin/AccountinfoView;->y:Lcom/osp/app/signin/ca;

    invoke-virtual {v0}, Lcom/osp/app/signin/ca;->d()V

    .line 2627
    iput-object v2, p0, Lcom/osp/app/signin/AccountinfoView;->y:Lcom/osp/app/signin/ca;

    .line 2630
    :cond_1
    iget-object v0, p0, Lcom/osp/app/signin/AccountinfoView;->A:Lcom/osp/app/signin/cb;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/osp/app/signin/AccountinfoView;->A:Lcom/osp/app/signin/cb;

    invoke-virtual {v0}, Lcom/osp/app/signin/cb;->getStatus()Landroid/os/AsyncTask$Status;

    move-result-object v0

    sget-object v1, Landroid/os/AsyncTask$Status;->RUNNING:Landroid/os/AsyncTask$Status;

    if-ne v0, v1, :cond_2

    .line 2632
    iget-object v0, p0, Lcom/osp/app/signin/AccountinfoView;->A:Lcom/osp/app/signin/cb;

    invoke-virtual {v0}, Lcom/osp/app/signin/cb;->d()V

    .line 2633
    iput-object v2, p0, Lcom/osp/app/signin/AccountinfoView;->A:Lcom/osp/app/signin/cb;

    .line 2636
    :cond_2
    iget-object v0, p0, Lcom/osp/app/signin/AccountinfoView;->z:Lcom/osp/app/signin/cc;

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/osp/app/signin/AccountinfoView;->z:Lcom/osp/app/signin/cc;

    invoke-virtual {v0}, Lcom/osp/app/signin/cc;->getStatus()Landroid/os/AsyncTask$Status;

    move-result-object v0

    sget-object v1, Landroid/os/AsyncTask$Status;->RUNNING:Landroid/os/AsyncTask$Status;

    if-ne v0, v1, :cond_3

    .line 2638
    iget-object v0, p0, Lcom/osp/app/signin/AccountinfoView;->z:Lcom/osp/app/signin/cc;

    invoke-virtual {v0}, Lcom/osp/app/signin/cc;->d()V

    .line 2639
    iput-object v2, p0, Lcom/osp/app/signin/AccountinfoView;->z:Lcom/osp/app/signin/cc;

    .line 2664
    :cond_3
    iget-object v0, p0, Lcom/osp/app/signin/AccountinfoView;->F:Landroid/app/AlertDialog;

    invoke-direct {p0, v0}, Lcom/osp/app/signin/AccountinfoView;->a(Landroid/app/AlertDialog;)V

    .line 2665
    return-void
.end method

.method public onFocusChange(Landroid/view/View;Z)V
    .locals 3

    .prologue
    .line 3552
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v0, "AIV"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "AccountinfoView::onFocusChange hasFocus : "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 3554
    iget-boolean v0, p0, Lcom/osp/app/signin/AccountinfoView;->aN:Z

    if-eqz v0, :cond_0

    if-eqz p2, :cond_0

    .line 3556
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/osp/app/signin/AccountinfoView;->aN:Z

    .line 3558
    instance-of v0, p1, Landroid/widget/EditText;

    if-eqz v0, :cond_0

    move-object v0, p1

    .line 3560
    check-cast v0, Landroid/widget/EditText;

    .line 3561
    invoke-virtual {v0}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 3563
    invoke-virtual {v0}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v1

    invoke-interface {v1}, Landroid/text/Editable;->length()I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setSelection(I)V

    .line 3583
    :cond_0
    if-nez p2, :cond_1

    move-object v0, p1

    .line 3585
    check-cast v0, Landroid/widget/EditText;

    invoke-virtual {v0}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v0

    if-gtz v0, :cond_1

    .line 3587
    check-cast p1, Landroid/widget/EditText;

    const-string v0, ""

    invoke-virtual {p1, v0}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    .line 3599
    :cond_1
    return-void
.end method

.method public onOptionsItemSelected(Landroid/view/MenuItem;)Z
    .locals 4

    .prologue
    const/4 v0, 0x0

    .line 3616
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v1, "AIV"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "AccountinfoView::onOptionsItemSelected item.getItemId() : "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 3630
    sget v1, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v2, 0xb

    if-lt v1, v2, :cond_0

    .line 3632
    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v1

    sparse-switch v1, :sswitch_data_0

    .line 3675
    :cond_0
    :goto_0
    return v0

    .line 3636
    :sswitch_0
    invoke-virtual {p0}, Lcom/osp/app/signin/AccountinfoView;->onBackPressed()V

    .line 3672
    :goto_1
    const/4 v0, 0x1

    goto :goto_0

    .line 3640
    :sswitch_1
    iget-object v0, p0, Lcom/osp/app/signin/AccountinfoView;->x:Landroid/view/Menu;

    if-eqz v0, :cond_1

    .line 3642
    iget-object v0, p0, Lcom/osp/app/signin/AccountinfoView;->x:Landroid/view/Menu;

    invoke-interface {v0}, Landroid/view/Menu;->clear()V

    .line 3644
    :cond_1
    invoke-direct {p0}, Lcom/osp/app/signin/AccountinfoView;->d()V

    .line 3645
    invoke-static {p0}, Lcom/osp/app/util/r;->m(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 3647
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/osp/app/signin/AccountinfoView;->e(Ljava/lang/String;)V

    .line 3648
    invoke-virtual {p0}, Lcom/osp/app/signin/AccountinfoView;->b()V

    goto :goto_1

    .line 3651
    :cond_2
    const-string v0, "save_cancel"

    invoke-virtual {p0, v0}, Lcom/osp/app/signin/AccountinfoView;->e(Ljava/lang/String;)V

    .line 3652
    invoke-virtual {p0}, Lcom/osp/app/signin/AccountinfoView;->l()V

    goto :goto_1

    .line 3657
    :sswitch_2
    invoke-direct {p0}, Lcom/osp/app/signin/AccountinfoView;->d()V

    goto :goto_1

    .line 3661
    :sswitch_3
    invoke-virtual {p0}, Lcom/osp/app/signin/AccountinfoView;->onBackPressed()V

    goto :goto_1

    .line 3632
    :sswitch_data_0
    .sparse-switch
        0x102002c -> :sswitch_0
        0x7f0c018c -> :sswitch_1
        0x7f0c018e -> :sswitch_3
        0x7f0c018f -> :sswitch_2
    .end sparse-switch
.end method

.method protected onPause()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 2673
    invoke-super {p0}, Lcom/osp/app/util/BaseActivity;->onPause()V

    .line 2674
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v0, "AIV"

    const-string v1, "AccountinfoView::onPause"

    const-string v2, "START"

    invoke-static {v0, v1, v2}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 2676
    const-string v0, "power"

    invoke-virtual {p0, v0}, Lcom/osp/app/signin/AccountinfoView;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/os/PowerManager;

    .line 2677
    invoke-virtual {v0}, Landroid/os/PowerManager;->isScreenOn()Z

    move-result v0

    .line 2678
    if-eqz v0, :cond_4

    .line 2680
    iget-object v0, p0, Lcom/osp/app/signin/AccountinfoView;->f:Lcom/osp/app/signin/bz;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/osp/app/signin/AccountinfoView;->f:Lcom/osp/app/signin/bz;

    invoke-virtual {v0}, Lcom/osp/app/signin/bz;->getStatus()Landroid/os/AsyncTask$Status;

    move-result-object v0

    sget-object v1, Landroid/os/AsyncTask$Status;->FINISHED:Landroid/os/AsyncTask$Status;

    if-eq v0, v1, :cond_0

    .line 2682
    iget-object v0, p0, Lcom/osp/app/signin/AccountinfoView;->f:Lcom/osp/app/signin/bz;

    invoke-virtual {v0}, Lcom/osp/app/signin/bz;->d()V

    .line 2683
    iput-object v3, p0, Lcom/osp/app/signin/AccountinfoView;->f:Lcom/osp/app/signin/bz;

    .line 2686
    :cond_0
    iget-object v0, p0, Lcom/osp/app/signin/AccountinfoView;->y:Lcom/osp/app/signin/ca;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/osp/app/signin/AccountinfoView;->y:Lcom/osp/app/signin/ca;

    invoke-virtual {v0}, Lcom/osp/app/signin/ca;->getStatus()Landroid/os/AsyncTask$Status;

    move-result-object v0

    sget-object v1, Landroid/os/AsyncTask$Status;->FINISHED:Landroid/os/AsyncTask$Status;

    if-eq v0, v1, :cond_1

    .line 2688
    iget-object v0, p0, Lcom/osp/app/signin/AccountinfoView;->y:Lcom/osp/app/signin/ca;

    invoke-virtual {v0}, Lcom/osp/app/signin/ca;->d()V

    .line 2689
    iput-object v3, p0, Lcom/osp/app/signin/AccountinfoView;->y:Lcom/osp/app/signin/ca;

    .line 2692
    :cond_1
    iget-object v0, p0, Lcom/osp/app/signin/AccountinfoView;->A:Lcom/osp/app/signin/cb;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/osp/app/signin/AccountinfoView;->A:Lcom/osp/app/signin/cb;

    invoke-virtual {v0}, Lcom/osp/app/signin/cb;->getStatus()Landroid/os/AsyncTask$Status;

    move-result-object v0

    sget-object v1, Landroid/os/AsyncTask$Status;->FINISHED:Landroid/os/AsyncTask$Status;

    if-eq v0, v1, :cond_2

    .line 2694
    iget-object v0, p0, Lcom/osp/app/signin/AccountinfoView;->A:Lcom/osp/app/signin/cb;

    invoke-virtual {v0}, Lcom/osp/app/signin/cb;->d()V

    .line 2695
    iput-object v3, p0, Lcom/osp/app/signin/AccountinfoView;->A:Lcom/osp/app/signin/cb;

    .line 2698
    :cond_2
    iget-object v0, p0, Lcom/osp/app/signin/AccountinfoView;->z:Lcom/osp/app/signin/cc;

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/osp/app/signin/AccountinfoView;->z:Lcom/osp/app/signin/cc;

    invoke-virtual {v0}, Lcom/osp/app/signin/cc;->getStatus()Landroid/os/AsyncTask$Status;

    move-result-object v0

    sget-object v1, Landroid/os/AsyncTask$Status;->FINISHED:Landroid/os/AsyncTask$Status;

    if-eq v0, v1, :cond_3

    .line 2700
    iget-object v0, p0, Lcom/osp/app/signin/AccountinfoView;->z:Lcom/osp/app/signin/cc;

    invoke-virtual {v0}, Lcom/osp/app/signin/cc;->d()V

    .line 2701
    iput-object v3, p0, Lcom/osp/app/signin/AccountinfoView;->z:Lcom/osp/app/signin/cc;

    .line 2704
    :cond_3
    iget-boolean v0, p0, Lcom/osp/app/signin/AccountinfoView;->P:Z

    if-eqz v0, :cond_4

    .line 2706
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/osp/app/signin/AccountinfoView;->a(Z)V

    .line 2710
    :cond_4
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v0, "AIV"

    const-string v1, "AccountinfoView::onPause"

    const-string v2, "END"

    invoke-static {v0, v1, v2}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 2711
    return-void
.end method

.method protected onPrepareDialog(ILandroid/app/Dialog;)V
    .locals 4

    .prologue
    .line 2717
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v0, "AIV"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "AccountinfoView::onPrepareDialog id : "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 2719
    if-nez p1, :cond_0

    move-object v0, p2

    .line 2721
    check-cast v0, Landroid/app/DatePickerDialog;

    iget v1, p0, Lcom/osp/app/signin/AccountinfoView;->aH:I

    iget v2, p0, Lcom/osp/app/signin/AccountinfoView;->as:I

    iget v3, p0, Lcom/osp/app/signin/AccountinfoView;->O:I

    invoke-virtual {v0, v1, v2, v3}, Landroid/app/DatePickerDialog;->updateDate(III)V

    .line 2726
    :cond_0
    invoke-super {p0, p1, p2}, Lcom/osp/app/util/BaseActivity;->onPrepareDialog(ILandroid/app/Dialog;)V

    .line 2727
    return-void
.end method

.method protected onResume()V
    .locals 2

    .prologue
    .line 2735
    invoke-super {p0}, Lcom/osp/app/util/BaseActivity;->onResume()V

    .line 2740
    invoke-static {p0}, Lcom/osp/app/util/ad;->d(Landroid/content/Context;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 2742
    const/16 v0, 0x10

    invoke-virtual {p0, v0}, Lcom/osp/app/signin/AccountinfoView;->setResult(I)V

    .line 2743
    invoke-virtual {p0}, Lcom/osp/app/signin/AccountinfoView;->finish()V

    .line 2745
    :cond_0
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v0, "AIV"

    const-string v1, "AccountinfoView::onResume"

    invoke-static {v0, v1}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 2746
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/osp/app/signin/AccountinfoView;->Q:Z

    .line 2747
    return-void
.end method

.method public onUserInteraction()V
    .locals 2

    .prologue
    .line 3689
    invoke-super {p0}, Lcom/osp/app/util/BaseActivity;->onUserInteraction()V

    .line 3690
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v0, "AIV"

    const-string v1, "AccountinfoView::onUserInteraction"

    invoke-static {v0, v1}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 3691
    return-void
.end method

.method protected onUserLeaveHint()V
    .locals 2

    .prologue
    .line 2755
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v0, "AIV"

    const-string v1, "AccountinfoView::onUserLeaveHint"

    invoke-static {v0, v1}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 2757
    iget-object v0, p0, Lcom/osp/app/signin/AccountinfoView;->f:Lcom/osp/app/signin/bz;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/osp/app/signin/AccountinfoView;->f:Lcom/osp/app/signin/bz;

    invoke-virtual {v0}, Lcom/osp/app/signin/bz;->getStatus()Landroid/os/AsyncTask$Status;

    move-result-object v0

    sget-object v1, Landroid/os/AsyncTask$Status;->FINISHED:Landroid/os/AsyncTask$Status;

    if-eq v0, v1, :cond_0

    .line 2759
    iget-object v0, p0, Lcom/osp/app/signin/AccountinfoView;->f:Lcom/osp/app/signin/bz;

    invoke-virtual {v0}, Lcom/osp/app/signin/bz;->d()V

    .line 2762
    :cond_0
    iget-object v0, p0, Lcom/osp/app/signin/AccountinfoView;->y:Lcom/osp/app/signin/ca;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/osp/app/signin/AccountinfoView;->y:Lcom/osp/app/signin/ca;

    invoke-virtual {v0}, Lcom/osp/app/signin/ca;->getStatus()Landroid/os/AsyncTask$Status;

    move-result-object v0

    sget-object v1, Landroid/os/AsyncTask$Status;->FINISHED:Landroid/os/AsyncTask$Status;

    if-eq v0, v1, :cond_1

    .line 2764
    iget-object v0, p0, Lcom/osp/app/signin/AccountinfoView;->y:Lcom/osp/app/signin/ca;

    invoke-virtual {v0}, Lcom/osp/app/signin/ca;->d()V

    .line 2767
    :cond_1
    iget-object v0, p0, Lcom/osp/app/signin/AccountinfoView;->A:Lcom/osp/app/signin/cb;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/osp/app/signin/AccountinfoView;->A:Lcom/osp/app/signin/cb;

    invoke-virtual {v0}, Lcom/osp/app/signin/cb;->getStatus()Landroid/os/AsyncTask$Status;

    move-result-object v0

    sget-object v1, Landroid/os/AsyncTask$Status;->FINISHED:Landroid/os/AsyncTask$Status;

    if-eq v0, v1, :cond_2

    .line 2769
    iget-object v0, p0, Lcom/osp/app/signin/AccountinfoView;->A:Lcom/osp/app/signin/cb;

    invoke-virtual {v0}, Lcom/osp/app/signin/cb;->d()V

    .line 2772
    :cond_2
    iget-object v0, p0, Lcom/osp/app/signin/AccountinfoView;->z:Lcom/osp/app/signin/cc;

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/osp/app/signin/AccountinfoView;->z:Lcom/osp/app/signin/cc;

    invoke-virtual {v0}, Lcom/osp/app/signin/cc;->getStatus()Landroid/os/AsyncTask$Status;

    move-result-object v0

    sget-object v1, Landroid/os/AsyncTask$Status;->FINISHED:Landroid/os/AsyncTask$Status;

    if-eq v0, v1, :cond_3

    .line 2774
    iget-object v0, p0, Lcom/osp/app/signin/AccountinfoView;->z:Lcom/osp/app/signin/cc;

    invoke-virtual {v0}, Lcom/osp/app/signin/cc;->d()V

    .line 2783
    :cond_3
    iget-boolean v0, p0, Lcom/osp/app/signin/AccountinfoView;->P:Z

    if-eqz v0, :cond_4

    .line 2785
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/osp/app/signin/AccountinfoView;->a(Z)V

    .line 2790
    :cond_4
    invoke-super {p0}, Lcom/osp/app/util/BaseActivity;->onUserLeaveHint()V

    .line 2791
    return-void
.end method
