.class final Lcom/osp/app/signin/ck;
.super Lcom/msc/c/b;
.source "AgreeToDisclaimerView.java"


# instance fields
.field protected c:Ljava/lang/Runnable;

.field protected d:Ljava/lang/Runnable;

.field protected e:Ljava/lang/Thread;

.field protected f:Ljava/lang/Thread;

.field final synthetic g:Lcom/osp/app/signin/AgreeToDisclaimerView;

.field private h:Z

.field private i:Z

.field private j:J

.field private k:J

.field private l:I

.field private m:Ljava/lang/String;

.field private n:Ljava/lang/String;


# direct methods
.method public constructor <init>(Lcom/osp/app/signin/AgreeToDisclaimerView;)V
    .locals 4

    .prologue
    .line 330
    iput-object p1, p0, Lcom/osp/app/signin/ck;->g:Lcom/osp/app/signin/AgreeToDisclaimerView;

    .line 331
    invoke-direct {p0, p1}, Lcom/msc/c/b;-><init>(Landroid/content/Context;)V

    .line 333
    invoke-static {}, Lcom/osp/common/util/i;->a()Lcom/osp/common/util/i;

    invoke-static {}, Lcom/osp/common/util/i;->a()Lcom/osp/common/util/i;

    invoke-static {p1}, Lcom/osp/common/util/i;->f(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/osp/common/util/i;->d(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 335
    invoke-static {}, Lcom/osp/common/util/i;->a()Lcom/osp/common/util/i;

    invoke-static {p1}, Lcom/osp/common/util/i;->l(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v1

    .line 336
    invoke-static {}, Lcom/osp/common/util/i;->a()Lcom/osp/common/util/i;

    invoke-static {}, Lcom/osp/common/util/i;->a()Lcom/osp/common/util/i;

    invoke-static {p1}, Lcom/osp/common/util/i;->f(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/osp/common/util/i;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 338
    new-instance v3, Lcom/osp/app/signin/cl;

    invoke-direct {v3, p0, p1, v0, v1}, Lcom/osp/app/signin/cl;-><init>(Lcom/osp/app/signin/ck;Lcom/osp/app/signin/AgreeToDisclaimerView;Ljava/lang/String;Ljava/lang/String;)V

    iput-object v3, p0, Lcom/osp/app/signin/ck;->c:Ljava/lang/Runnable;

    .line 354
    new-instance v0, Lcom/osp/app/signin/cm;

    invoke-direct {v0, p0, p1, v2, v1}, Lcom/osp/app/signin/cm;-><init>(Lcom/osp/app/signin/ck;Lcom/osp/app/signin/AgreeToDisclaimerView;Ljava/lang/String;Ljava/lang/String;)V

    iput-object v0, p0, Lcom/osp/app/signin/ck;->d:Ljava/lang/Runnable;

    .line 371
    new-instance v0, Ljava/lang/Thread;

    iget-object v1, p0, Lcom/osp/app/signin/ck;->c:Ljava/lang/Runnable;

    invoke-direct {v0, v1}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;)V

    iput-object v0, p0, Lcom/osp/app/signin/ck;->e:Ljava/lang/Thread;

    .line 372
    new-instance v0, Ljava/lang/Thread;

    iget-object v1, p0, Lcom/osp/app/signin/ck;->d:Ljava/lang/Runnable;

    invoke-direct {v0, v1}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;)V

    iput-object v0, p0, Lcom/osp/app/signin/ck;->f:Ljava/lang/Thread;

    .line 375
    return-void
.end method

.method private a(ILjava/lang/String;Ljava/lang/String;)V
    .locals 3

    .prologue
    .line 573
    iput p1, p0, Lcom/osp/app/signin/ck;->l:I

    .line 574
    iput-object p3, p0, Lcom/osp/app/signin/ck;->m:Ljava/lang/String;

    .line 576
    invoke-static {}, Lcom/msc/c/g;->a()Lcom/msc/c/g;

    .line 577
    iget-object v0, p0, Lcom/osp/app/signin/ck;->g:Lcom/osp/app/signin/AgreeToDisclaimerView;

    iget-object v1, p0, Lcom/osp/app/signin/ck;->g:Lcom/osp/app/signin/AgreeToDisclaimerView;

    invoke-static {v1}, Lcom/osp/app/signin/AgreeToDisclaimerView;->b(Lcom/osp/app/signin/AgreeToDisclaimerView;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1, p2, p3, p0}, Lcom/msc/c/g;->b(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/msc/b/h;)J

    move-result-wide v0

    iput-wide v0, p0, Lcom/osp/app/signin/ck;->j:J

    .line 579
    iget-wide v0, p0, Lcom/osp/app/signin/ck;->j:J

    invoke-virtual {p0, v0, v1}, Lcom/osp/app/signin/ck;->a(J)V

    .line 580
    iget-wide v0, p0, Lcom/osp/app/signin/ck;->j:J

    const-string v2, "none"

    invoke-virtual {p0, v0, v1, v2}, Lcom/osp/app/signin/ck;->a(JLjava/lang/String;)V

    .line 582
    iget-wide v0, p0, Lcom/osp/app/signin/ck;->j:J

    sget-object v2, Lcom/msc/b/g;->c:Lcom/msc/b/g;

    invoke-static {v0, v1, v2}, Lcom/msc/c/g;->a(JLcom/msc/b/g;)V

    .line 583
    return-void
.end method

.method static synthetic a(Lcom/osp/app/signin/ck;ILjava/lang/String;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 315
    invoke-direct {p0, p1, p2, p3}, Lcom/osp/app/signin/ck;->a(ILjava/lang/String;Ljava/lang/String;)V

    return-void
.end method

.method private static a(Ljava/lang/Thread;)V
    .locals 2

    .prologue
    .line 389
    if-eqz p0, :cond_0

    invoke-virtual {p0}, Ljava/lang/Thread;->isAlive()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 393
    const-wide/16 v0, 0x12c

    :try_start_0
    invoke-virtual {p0, v0, v1}, Ljava/lang/Thread;->join(J)V
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0

    .line 398
    :cond_0
    :goto_0
    return-void

    .line 394
    :catch_0
    move-exception v0

    invoke-virtual {v0}, Ljava/lang/InterruptedException;->printStackTrace()V

    goto :goto_0
.end method

.method static synthetic a(Lcom/osp/app/signin/ck;)Z
    .locals 1

    .prologue
    .line 315
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/osp/app/signin/ck;->h:Z

    return v0
.end method

.method private b(ILjava/lang/String;Ljava/lang/String;)V
    .locals 6

    .prologue
    .line 587
    iput p1, p0, Lcom/osp/app/signin/ck;->l:I

    .line 588
    iput-object p3, p0, Lcom/osp/app/signin/ck;->m:Ljava/lang/String;

    .line 590
    invoke-static {}, Lcom/msc/c/g;->a()Lcom/msc/c/g;

    .line 591
    iget-object v0, p0, Lcom/osp/app/signin/ck;->g:Lcom/osp/app/signin/AgreeToDisclaimerView;

    iget-object v0, p0, Lcom/osp/app/signin/ck;->g:Lcom/osp/app/signin/AgreeToDisclaimerView;

    invoke-static {v0}, Lcom/osp/app/signin/AgreeToDisclaimerView;->o(Lcom/osp/app/signin/AgreeToDisclaimerView;)Ljava/lang/String;

    move-result-object v0

    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v1, "SRS"

    const-string v2, " GetDisclaimer go "

    invoke-static {v1, v2}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;)V

    invoke-static {p2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_0

    const-string p2, "us"

    const-string p3, "default"

    :cond_0
    invoke-static {p2, p3, v0}, Lcom/msc/c/k;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-static {}, Lcom/msc/b/i;->a()Lcom/msc/b/i;

    move-result-object v0

    invoke-static {}, Lcom/osp/app/util/ah;->a()Lcom/osp/app/util/ah;

    move-result-object v2

    invoke-virtual {v2}, Lcom/osp/app/util/ah;->g()Z

    move-result v4

    invoke-static {}, Lcom/osp/app/signin/SamsungService;->j()Z

    move-result v2

    invoke-static {}, Lcom/osp/app/signin/SamsungService;->b()Z

    move-result v3

    move-object v5, p0

    invoke-virtual/range {v0 .. v5}, Lcom/msc/b/i;->a(Ljava/lang/String;ZZZLcom/msc/b/h;)Lcom/msc/b/d;

    move-result-object v0

    invoke-virtual {v0}, Lcom/msc/b/d;->a()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/osp/app/signin/ck;->k:J

    .line 593
    iget-wide v0, p0, Lcom/osp/app/signin/ck;->k:J

    invoke-virtual {p0, v0, v1}, Lcom/osp/app/signin/ck;->a(J)V

    .line 594
    iget-wide v0, p0, Lcom/osp/app/signin/ck;->k:J

    const-string v2, "none"

    invoke-virtual {p0, v0, v1, v2}, Lcom/osp/app/signin/ck;->a(JLjava/lang/String;)V

    .line 596
    iget-wide v0, p0, Lcom/osp/app/signin/ck;->k:J

    sget-object v2, Lcom/msc/b/g;->b:Lcom/msc/b/g;

    invoke-static {v0, v1, v2}, Lcom/msc/c/g;->a(JLcom/msc/b/g;)V

    .line 597
    return-void
.end method

.method static synthetic b(Lcom/osp/app/signin/ck;ILjava/lang/String;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 315
    invoke-direct {p0, p1, p2, p3}, Lcom/osp/app/signin/ck;->b(ILjava/lang/String;Ljava/lang/String;)V

    return-void
.end method

.method static synthetic b(Lcom/osp/app/signin/ck;)Z
    .locals 1

    .prologue
    .line 315
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/osp/app/signin/ck;->i:Z

    return v0
.end method


# virtual methods
.method protected final varargs a()Ljava/lang/Boolean;
    .locals 1

    .prologue
    .line 405
    iget-object v0, p0, Lcom/osp/app/signin/ck;->e:Ljava/lang/Thread;

    invoke-virtual {v0}, Ljava/lang/Thread;->start()V

    .line 406
    iget-object v0, p0, Lcom/osp/app/signin/ck;->f:Ljava/lang/Thread;

    invoke-virtual {v0}, Ljava/lang/Thread;->start()V

    .line 408
    :cond_0
    iget-boolean v0, p0, Lcom/osp/app/signin/ck;->h:Z

    if-eqz v0, :cond_1

    iget-boolean v0, p0, Lcom/osp/app/signin/ck;->i:Z

    if-nez v0, :cond_2

    .line 410
    :cond_1
    invoke-virtual {p0}, Lcom/osp/app/signin/ck;->isCancelled()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 412
    :cond_2
    const/4 v0, 0x0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    return-object v0
.end method

.method public final declared-synchronized a(Lcom/msc/b/c;)V
    .locals 6

    .prologue
    .line 487
    monitor-enter p0

    :try_start_0
    invoke-super {p0, p1}, Lcom/msc/c/b;->a(Lcom/msc/b/c;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 489
    if-nez p1, :cond_1

    .line 522
    :cond_0
    :goto_0
    monitor-exit p0

    return-void

    .line 494
    :cond_1
    :try_start_1
    invoke-virtual {p1}, Lcom/msc/b/c;->a()J

    move-result-wide v0

    .line 495
    invoke-virtual {p1}, Lcom/msc/b/c;->e()Ljava/lang/String;

    move-result-object v2

    .line 497
    iget-wide v4, p0, Lcom/osp/app/signin/ck;->j:J
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    cmp-long v3, v0, v4

    if-nez v3, :cond_4

    .line 501
    :try_start_2
    iget-object v0, p0, Lcom/osp/app/signin/ck;->g:Lcom/osp/app/signin/AgreeToDisclaimerView;

    invoke-static {}, Lcom/msc/c/h;->a()Lcom/msc/c/h;

    iget-object v1, p0, Lcom/osp/app/signin/ck;->g:Lcom/osp/app/signin/AgreeToDisclaimerView;

    invoke-static {v2}, Lcom/msc/c/h;->B(Ljava/lang/String;)Lcom/osp/app/signin/el;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/osp/app/signin/AgreeToDisclaimerView;->a(Lcom/osp/app/signin/AgreeToDisclaimerView;Lcom/osp/app/signin/el;)Lcom/osp/app/signin/el;

    .line 503
    iget-object v0, p0, Lcom/osp/app/signin/ck;->g:Lcom/osp/app/signin/AgreeToDisclaimerView;

    invoke-static {v0}, Lcom/osp/app/signin/AgreeToDisclaimerView;->a(Lcom/osp/app/signin/AgreeToDisclaimerView;)Lcom/osp/app/signin/el;

    move-result-object v0

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/osp/app/signin/ck;->g:Lcom/osp/app/signin/AgreeToDisclaimerView;

    invoke-static {v0}, Lcom/osp/app/signin/AgreeToDisclaimerView;->a(Lcom/osp/app/signin/AgreeToDisclaimerView;)Lcom/osp/app/signin/el;

    move-result-object v0

    iget-object v0, v0, Lcom/osp/app/signin/el;->a:Ljava/lang/String;

    if-nez v0, :cond_3

    :cond_2
    iget-object v0, p0, Lcom/osp/app/signin/ck;->g:Lcom/osp/app/signin/AgreeToDisclaimerView;

    invoke-static {v0}, Lcom/osp/app/signin/AgreeToDisclaimerView;->a(Lcom/osp/app/signin/AgreeToDisclaimerView;)Lcom/osp/app/signin/el;

    move-result-object v0

    iget-object v0, v0, Lcom/osp/app/signin/el;->a:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v0

    if-eqz v0, :cond_0

    .line 505
    :cond_3
    iget-object v0, p0, Lcom/osp/app/signin/ck;->g:Lcom/osp/app/signin/AgreeToDisclaimerView;

    invoke-static {v0}, Lcom/osp/app/signin/AgreeToDisclaimerView;->a(Lcom/osp/app/signin/AgreeToDisclaimerView;)Lcom/osp/app/signin/el;

    move-result-object v0

    iget-object v1, p0, Lcom/osp/app/signin/ck;->g:Lcom/osp/app/signin/AgreeToDisclaimerView;

    iget-object v1, p0, Lcom/osp/app/signin/ck;->g:Lcom/osp/app/signin/AgreeToDisclaimerView;

    invoke-static {v1}, Lcom/osp/app/signin/AgreeToDisclaimerView;->a(Lcom/osp/app/signin/AgreeToDisclaimerView;)Lcom/osp/app/signin/el;

    move-result-object v1

    iget-object v1, v1, Lcom/osp/app/signin/el;->a:Ljava/lang/String;

    invoke-static {v1}, Lcom/osp/app/signin/AgreeToDisclaimerView;->a(Ljava/lang/String;)Landroid/graphics/Bitmap;

    move-result-object v1

    iput-object v1, v0, Lcom/osp/app/signin/el;->f:Landroid/graphics/Bitmap;
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_0
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0

    .line 507
    :catch_0
    move-exception v0

    :try_start_3
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    goto :goto_0

    .line 487
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 512
    :cond_4
    :try_start_4
    iget-wide v4, p0, Lcom/osp/app/signin/ck;->k:J
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    cmp-long v0, v0, v4

    if-nez v0, :cond_0

    .line 516
    :try_start_5
    iput-object v2, p0, Lcom/osp/app/signin/ck;->n:Ljava/lang/String;
    :try_end_5
    .catch Ljava/lang/Exception; {:try_start_5 .. :try_end_5} :catch_1
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    goto :goto_0

    .line 517
    :catch_1
    move-exception v0

    :try_start_6
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_0

    goto :goto_0
.end method

.method protected final a(Ljava/lang/Boolean;)V
    .locals 7

    .prologue
    const v6, 0x7f0900ba

    const/4 v2, 0x1

    const/16 v5, 0x8

    const/4 v1, 0x0

    .line 422
    invoke-super {p0, p1}, Lcom/msc/c/b;->a(Ljava/lang/Boolean;)V

    .line 423
    iget-object v0, p0, Lcom/osp/app/signin/ck;->b:Lcom/msc/c/f;

    if-eqz v0, :cond_0

    .line 425
    invoke-virtual {p0, v1}, Lcom/osp/app/signin/ck;->a(Z)V

    .line 426
    iget-object v0, p0, Lcom/osp/app/signin/ck;->g:Lcom/osp/app/signin/AgreeToDisclaimerView;

    invoke-virtual {v0, v2}, Lcom/osp/app/signin/AgreeToDisclaimerView;->b(I)V

    .line 427
    iget-object v0, p0, Lcom/osp/app/signin/ck;->g:Lcom/osp/app/signin/AgreeToDisclaimerView;

    invoke-virtual {v0}, Lcom/osp/app/signin/AgreeToDisclaimerView;->finish()V

    .line 429
    :cond_0
    iget-object v0, p0, Lcom/osp/app/signin/ck;->n:Ljava/lang/String;

    if-eqz v0, :cond_1

    .line 431
    iget-object v0, p0, Lcom/osp/app/signin/ck;->g:Lcom/osp/app/signin/AgreeToDisclaimerView;

    invoke-static {v0}, Lcom/osp/app/signin/AgreeToDisclaimerView;->e(Lcom/osp/app/signin/AgreeToDisclaimerView;)Landroid/widget/TextView;

    move-result-object v0

    iget-object v3, p0, Lcom/osp/app/signin/ck;->n:Ljava/lang/String;

    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 433
    :cond_1
    iget-object v0, p0, Lcom/osp/app/signin/ck;->g:Lcom/osp/app/signin/AgreeToDisclaimerView;

    invoke-static {v0}, Lcom/osp/app/signin/AgreeToDisclaimerView;->a(Lcom/osp/app/signin/AgreeToDisclaimerView;)Lcom/osp/app/signin/el;

    move-result-object v0

    if-eqz v0, :cond_c

    .line 436
    const-string v0, "Tripadvisor"

    iget-object v3, p0, Lcom/osp/app/signin/ck;->g:Lcom/osp/app/signin/AgreeToDisclaimerView;

    invoke-static {v3}, Lcom/osp/app/signin/AgreeToDisclaimerView;->a(Lcom/osp/app/signin/AgreeToDisclaimerView;)Lcom/osp/app/signin/el;

    move-result-object v3

    iget-object v3, v3, Lcom/osp/app/signin/el;->b:Ljava/lang/String;

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_a

    invoke-static {}, Lcom/osp/device/b;->b()Lcom/osp/device/b;

    move-result-object v0

    invoke-virtual {v0}, Lcom/osp/device/b;->c()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_d

    const-string v3, "CHM"

    invoke-virtual {v3, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_2

    const-string v3, "CHN"

    invoke-virtual {v3, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_2

    const-string v3, "CTC"

    invoke-virtual {v3, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_2

    const-string v3, "CHU"

    invoke-virtual {v3, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_d

    :cond_2
    move v0, v2

    :goto_0
    if-eqz v0, :cond_a

    .line 438
    iget-object v0, p0, Lcom/osp/app/signin/ck;->g:Lcom/osp/app/signin/AgreeToDisclaimerView;

    invoke-static {v0}, Lcom/osp/app/signin/AgreeToDisclaimerView;->f(Lcom/osp/app/signin/AgreeToDisclaimerView;)Landroid/widget/TextView;

    move-result-object v0

    const-string v3, "\u5230\u5230\u65e0\u7ebf"

    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 439
    iget-object v0, p0, Lcom/osp/app/signin/ck;->g:Lcom/osp/app/signin/AgreeToDisclaimerView;

    invoke-static {v0}, Lcom/osp/app/signin/AgreeToDisclaimerView;->g(Lcom/osp/app/signin/AgreeToDisclaimerView;)Landroid/widget/TextView;

    move-result-object v0

    const-string v3, "\u5230\u5230\u65e0\u7ebf"

    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 441
    iget-object v0, p0, Lcom/osp/app/signin/ck;->g:Lcom/osp/app/signin/AgreeToDisclaimerView;

    invoke-static {v0}, Lcom/osp/app/signin/AgreeToDisclaimerView;->h(Lcom/osp/app/signin/AgreeToDisclaimerView;)Landroid/widget/TextView;

    move-result-object v0

    iget-object v3, p0, Lcom/osp/app/signin/ck;->g:Lcom/osp/app/signin/AgreeToDisclaimerView;

    new-array v2, v2, [Ljava/lang/Object;

    const-string v4, "\u5230\u5230\u65e0\u7ebf"

    aput-object v4, v2, v1

    invoke-virtual {v3, v6, v2}, Lcom/osp/app/signin/AgreeToDisclaimerView;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 452
    :goto_1
    iget-object v0, p0, Lcom/osp/app/signin/ck;->g:Lcom/osp/app/signin/AgreeToDisclaimerView;

    invoke-static {v0}, Lcom/osp/app/signin/AgreeToDisclaimerView;->a(Lcom/osp/app/signin/AgreeToDisclaimerView;)Lcom/osp/app/signin/el;

    move-result-object v0

    iget-object v0, v0, Lcom/osp/app/signin/el;->f:Landroid/graphics/Bitmap;

    if-eqz v0, :cond_b

    .line 454
    iget-object v0, p0, Lcom/osp/app/signin/ck;->g:Lcom/osp/app/signin/AgreeToDisclaimerView;

    invoke-static {v0}, Lcom/osp/app/signin/AgreeToDisclaimerView;->i(Lcom/osp/app/signin/AgreeToDisclaimerView;)Landroid/widget/ImageView;

    move-result-object v0

    iget-object v2, p0, Lcom/osp/app/signin/ck;->g:Lcom/osp/app/signin/AgreeToDisclaimerView;

    invoke-static {v2}, Lcom/osp/app/signin/AgreeToDisclaimerView;->a(Lcom/osp/app/signin/AgreeToDisclaimerView;)Lcom/osp/app/signin/el;

    move-result-object v2

    iget-object v2, v2, Lcom/osp/app/signin/el;->f:Landroid/graphics/Bitmap;

    invoke-virtual {v0, v2}, Landroid/widget/ImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    .line 455
    iget-object v0, p0, Lcom/osp/app/signin/ck;->g:Lcom/osp/app/signin/AgreeToDisclaimerView;

    invoke-static {v0}, Lcom/osp/app/signin/AgreeToDisclaimerView;->i(Lcom/osp/app/signin/AgreeToDisclaimerView;)Landroid/widget/ImageView;

    move-result-object v0

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 461
    :goto_2
    iget-object v0, p0, Lcom/osp/app/signin/ck;->g:Lcom/osp/app/signin/AgreeToDisclaimerView;

    invoke-static {v0}, Lcom/osp/app/signin/AgreeToDisclaimerView;->a(Lcom/osp/app/signin/AgreeToDisclaimerView;)Lcom/osp/app/signin/el;

    move-result-object v0

    iget-object v0, v0, Lcom/osp/app/signin/el;->d:Ljava/lang/String;

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/osp/app/signin/ck;->g:Lcom/osp/app/signin/AgreeToDisclaimerView;

    invoke-static {v0}, Lcom/osp/app/signin/AgreeToDisclaimerView;->a(Lcom/osp/app/signin/AgreeToDisclaimerView;)Lcom/osp/app/signin/el;

    move-result-object v0

    iget-object v0, v0, Lcom/osp/app/signin/el;->d:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v0

    if-nez v0, :cond_4

    .line 463
    :cond_3
    iget-object v0, p0, Lcom/osp/app/signin/ck;->g:Lcom/osp/app/signin/AgreeToDisclaimerView;

    invoke-static {v0}, Lcom/osp/app/signin/AgreeToDisclaimerView;->j(Lcom/osp/app/signin/AgreeToDisclaimerView;)Landroid/widget/TextView;

    move-result-object v0

    invoke-virtual {v0, v5}, Landroid/widget/TextView;->setVisibility(I)V

    .line 464
    iget-object v0, p0, Lcom/osp/app/signin/ck;->g:Lcom/osp/app/signin/AgreeToDisclaimerView;

    invoke-static {v0}, Lcom/osp/app/signin/AgreeToDisclaimerView;->k(Lcom/osp/app/signin/AgreeToDisclaimerView;)Landroid/widget/TextView;

    move-result-object v0

    invoke-virtual {v0, v5}, Landroid/widget/TextView;->setVisibility(I)V

    .line 467
    :cond_4
    iget-object v0, p0, Lcom/osp/app/signin/ck;->g:Lcom/osp/app/signin/AgreeToDisclaimerView;

    invoke-static {v0}, Lcom/osp/app/signin/AgreeToDisclaimerView;->a(Lcom/osp/app/signin/AgreeToDisclaimerView;)Lcom/osp/app/signin/el;

    move-result-object v0

    iget-object v0, v0, Lcom/osp/app/signin/el;->e:Ljava/lang/String;

    if-eqz v0, :cond_5

    iget-object v0, p0, Lcom/osp/app/signin/ck;->g:Lcom/osp/app/signin/AgreeToDisclaimerView;

    invoke-static {v0}, Lcom/osp/app/signin/AgreeToDisclaimerView;->a(Lcom/osp/app/signin/AgreeToDisclaimerView;)Lcom/osp/app/signin/el;

    move-result-object v0

    iget-object v0, v0, Lcom/osp/app/signin/el;->e:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v0

    if-nez v0, :cond_6

    .line 469
    :cond_5
    iget-object v0, p0, Lcom/osp/app/signin/ck;->g:Lcom/osp/app/signin/AgreeToDisclaimerView;

    invoke-static {v0}, Lcom/osp/app/signin/AgreeToDisclaimerView;->l(Lcom/osp/app/signin/AgreeToDisclaimerView;)Landroid/widget/TextView;

    move-result-object v0

    invoke-virtual {v0, v5}, Landroid/widget/TextView;->setVisibility(I)V

    .line 470
    iget-object v0, p0, Lcom/osp/app/signin/ck;->g:Lcom/osp/app/signin/AgreeToDisclaimerView;

    invoke-static {v0}, Lcom/osp/app/signin/AgreeToDisclaimerView;->m(Lcom/osp/app/signin/AgreeToDisclaimerView;)Landroid/widget/TextView;

    move-result-object v0

    invoke-virtual {v0, v5}, Landroid/widget/TextView;->setVisibility(I)V

    .line 472
    :cond_6
    iget-object v0, p0, Lcom/osp/app/signin/ck;->g:Lcom/osp/app/signin/AgreeToDisclaimerView;

    invoke-static {v0}, Lcom/osp/app/signin/AgreeToDisclaimerView;->a(Lcom/osp/app/signin/AgreeToDisclaimerView;)Lcom/osp/app/signin/el;

    move-result-object v0

    iget-object v0, v0, Lcom/osp/app/signin/el;->d:Ljava/lang/String;

    if-eqz v0, :cond_7

    iget-object v0, p0, Lcom/osp/app/signin/ck;->g:Lcom/osp/app/signin/AgreeToDisclaimerView;

    invoke-static {v0}, Lcom/osp/app/signin/AgreeToDisclaimerView;->a(Lcom/osp/app/signin/AgreeToDisclaimerView;)Lcom/osp/app/signin/el;

    move-result-object v0

    iget-object v0, v0, Lcom/osp/app/signin/el;->d:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v0

    if-nez v0, :cond_9

    :cond_7
    iget-object v0, p0, Lcom/osp/app/signin/ck;->g:Lcom/osp/app/signin/AgreeToDisclaimerView;

    invoke-static {v0}, Lcom/osp/app/signin/AgreeToDisclaimerView;->a(Lcom/osp/app/signin/AgreeToDisclaimerView;)Lcom/osp/app/signin/el;

    move-result-object v0

    iget-object v0, v0, Lcom/osp/app/signin/el;->e:Ljava/lang/String;

    if-eqz v0, :cond_8

    iget-object v0, p0, Lcom/osp/app/signin/ck;->g:Lcom/osp/app/signin/AgreeToDisclaimerView;

    invoke-static {v0}, Lcom/osp/app/signin/AgreeToDisclaimerView;->a(Lcom/osp/app/signin/AgreeToDisclaimerView;)Lcom/osp/app/signin/el;

    move-result-object v0

    iget-object v0, v0, Lcom/osp/app/signin/el;->e:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v0

    if-nez v0, :cond_9

    .line 474
    :cond_8
    iget-object v0, p0, Lcom/osp/app/signin/ck;->g:Lcom/osp/app/signin/AgreeToDisclaimerView;

    invoke-static {v0}, Lcom/osp/app/signin/AgreeToDisclaimerView;->n(Lcom/osp/app/signin/AgreeToDisclaimerView;)Landroid/widget/LinearLayout;

    move-result-object v0

    invoke-virtual {v0, v5}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 483
    :cond_9
    :goto_3
    return-void

    .line 444
    :cond_a
    iget-object v0, p0, Lcom/osp/app/signin/ck;->g:Lcom/osp/app/signin/AgreeToDisclaimerView;

    invoke-static {v0}, Lcom/osp/app/signin/AgreeToDisclaimerView;->f(Lcom/osp/app/signin/AgreeToDisclaimerView;)Landroid/widget/TextView;

    move-result-object v0

    iget-object v3, p0, Lcom/osp/app/signin/ck;->g:Lcom/osp/app/signin/AgreeToDisclaimerView;

    invoke-static {v3}, Lcom/osp/app/signin/AgreeToDisclaimerView;->a(Lcom/osp/app/signin/AgreeToDisclaimerView;)Lcom/osp/app/signin/el;

    move-result-object v3

    iget-object v3, v3, Lcom/osp/app/signin/el;->b:Ljava/lang/String;

    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 445
    iget-object v0, p0, Lcom/osp/app/signin/ck;->g:Lcom/osp/app/signin/AgreeToDisclaimerView;

    invoke-static {v0}, Lcom/osp/app/signin/AgreeToDisclaimerView;->g(Lcom/osp/app/signin/AgreeToDisclaimerView;)Landroid/widget/TextView;

    move-result-object v0

    iget-object v3, p0, Lcom/osp/app/signin/ck;->g:Lcom/osp/app/signin/AgreeToDisclaimerView;

    invoke-static {v3}, Lcom/osp/app/signin/AgreeToDisclaimerView;->a(Lcom/osp/app/signin/AgreeToDisclaimerView;)Lcom/osp/app/signin/el;

    move-result-object v3

    iget-object v3, v3, Lcom/osp/app/signin/el;->c:Ljava/lang/String;

    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 447
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v3, "[CLU] Disclaimer Info : "

    invoke-direct {v0, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v3, p0, Lcom/osp/app/signin/ck;->g:Lcom/osp/app/signin/AgreeToDisclaimerView;

    invoke-static {v3}, Lcom/osp/app/signin/AgreeToDisclaimerView;->a(Lcom/osp/app/signin/AgreeToDisclaimerView;)Lcom/osp/app/signin/el;

    move-result-object v3

    iget-object v3, v3, Lcom/osp/app/signin/el;->c:Ljava/lang/String;

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/osp/app/util/aq;->b(Ljava/lang/String;)V

    .line 449
    iget-object v0, p0, Lcom/osp/app/signin/ck;->g:Lcom/osp/app/signin/AgreeToDisclaimerView;

    invoke-static {v0}, Lcom/osp/app/signin/AgreeToDisclaimerView;->h(Lcom/osp/app/signin/AgreeToDisclaimerView;)Landroid/widget/TextView;

    move-result-object v0

    iget-object v3, p0, Lcom/osp/app/signin/ck;->g:Lcom/osp/app/signin/AgreeToDisclaimerView;

    new-array v2, v2, [Ljava/lang/Object;

    iget-object v4, p0, Lcom/osp/app/signin/ck;->g:Lcom/osp/app/signin/AgreeToDisclaimerView;

    invoke-static {v4}, Lcom/osp/app/signin/AgreeToDisclaimerView;->a(Lcom/osp/app/signin/AgreeToDisclaimerView;)Lcom/osp/app/signin/el;

    move-result-object v4

    iget-object v4, v4, Lcom/osp/app/signin/el;->b:Ljava/lang/String;

    aput-object v4, v2, v1

    invoke-virtual {v3, v6, v2}, Lcom/osp/app/signin/AgreeToDisclaimerView;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto/16 :goto_1

    .line 458
    :cond_b
    iget-object v0, p0, Lcom/osp/app/signin/ck;->g:Lcom/osp/app/signin/AgreeToDisclaimerView;

    invoke-static {v0}, Lcom/osp/app/signin/AgreeToDisclaimerView;->i(Lcom/osp/app/signin/AgreeToDisclaimerView;)Landroid/widget/ImageView;

    move-result-object v0

    invoke-virtual {v0, v5}, Landroid/widget/ImageView;->setVisibility(I)V

    goto/16 :goto_2

    .line 479
    :cond_c
    invoke-virtual {p0, v1}, Lcom/osp/app/signin/ck;->a(Z)V

    .line 480
    iget-object v0, p0, Lcom/osp/app/signin/ck;->g:Lcom/osp/app/signin/AgreeToDisclaimerView;

    invoke-virtual {v0, v2}, Lcom/osp/app/signin/AgreeToDisclaimerView;->b(I)V

    .line 481
    iget-object v0, p0, Lcom/osp/app/signin/ck;->g:Lcom/osp/app/signin/AgreeToDisclaimerView;

    invoke-virtual {v0}, Lcom/osp/app/signin/AgreeToDisclaimerView;->finish()V

    goto :goto_3

    :cond_d
    move v0, v1

    goto/16 :goto_0
.end method

.method public final b(Lcom/msc/b/c;)V
    .locals 8

    .prologue
    const/4 v7, 0x2

    const/4 v6, 0x1

    .line 526
    invoke-super {p0, p1}, Lcom/msc/c/b;->b(Lcom/msc/b/c;)V

    .line 528
    if-nez p1, :cond_1

    .line 563
    :cond_0
    :goto_0
    return-void

    .line 533
    :cond_1
    invoke-virtual {p1}, Lcom/msc/b/c;->a()J

    move-result-wide v0

    .line 534
    invoke-virtual {p1}, Lcom/msc/b/c;->e()Ljava/lang/String;

    move-result-object v2

    .line 536
    iget-wide v4, p0, Lcom/osp/app/signin/ck;->j:J

    cmp-long v3, v0, v4

    if-nez v3, :cond_2

    .line 538
    if-eqz v2, :cond_0

    .line 540
    iget v0, p0, Lcom/osp/app/signin/ck;->l:I

    packed-switch v0, :pswitch_data_0

    goto :goto_0

    .line 543
    :pswitch_0
    const-string v0, "USA"

    iget-object v1, p0, Lcom/osp/app/signin/ck;->m:Ljava/lang/String;

    invoke-direct {p0, v6, v0, v1}, Lcom/osp/app/signin/ck;->a(ILjava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 546
    :pswitch_1
    const-string v0, "USA"

    const-string v1, "default"

    invoke-direct {p0, v7, v0, v1}, Lcom/osp/app/signin/ck;->a(ILjava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 552
    :cond_2
    iget-wide v4, p0, Lcom/osp/app/signin/ck;->k:J

    cmp-long v0, v0, v4

    if-nez v0, :cond_0

    .line 554
    if-eqz v2, :cond_0

    .line 556
    iget v0, p0, Lcom/osp/app/signin/ck;->l:I

    packed-switch v0, :pswitch_data_1

    goto :goto_0

    .line 559
    :pswitch_2
    const-string v0, "us"

    iget-object v1, p0, Lcom/osp/app/signin/ck;->m:Ljava/lang/String;

    invoke-direct {p0, v6, v0, v1}, Lcom/osp/app/signin/ck;->b(ILjava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 562
    :pswitch_3
    const-string v0, "us"

    const-string v1, "default"

    invoke-direct {p0, v7, v0, v1}, Lcom/osp/app/signin/ck;->b(ILjava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 540
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
    .end packed-switch

    .line 556
    :pswitch_data_1
    .packed-switch 0x0
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method protected final d()V
    .locals 3

    .prologue
    const/4 v2, 0x1

    .line 379
    invoke-super {p0}, Lcom/msc/c/b;->d()V

    .line 380
    iget-object v0, p0, Lcom/osp/app/signin/ck;->g:Lcom/osp/app/signin/AgreeToDisclaimerView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/osp/app/signin/AgreeToDisclaimerView;->b(I)V

    .line 381
    iget-object v0, p0, Lcom/osp/app/signin/ck;->e:Ljava/lang/Thread;

    invoke-static {v0}, Lcom/osp/app/signin/ck;->a(Ljava/lang/Thread;)V

    .line 382
    iget-object v0, p0, Lcom/osp/app/signin/ck;->f:Ljava/lang/Thread;

    invoke-static {v0}, Lcom/osp/app/signin/ck;->a(Ljava/lang/Thread;)V

    .line 383
    iput-boolean v2, p0, Lcom/osp/app/signin/ck;->h:Z

    .line 384
    iput-boolean v2, p0, Lcom/osp/app/signin/ck;->i:Z

    .line 385
    iget-object v0, p0, Lcom/osp/app/signin/ck;->g:Lcom/osp/app/signin/AgreeToDisclaimerView;

    invoke-virtual {v0}, Lcom/osp/app/signin/AgreeToDisclaimerView;->finish()V

    .line 386
    return-void
.end method

.method protected final synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 315
    invoke-virtual {p0}, Lcom/osp/app/signin/ck;->a()Ljava/lang/Boolean;

    move-result-object v0

    return-object v0
.end method

.method protected final synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 315
    check-cast p1, Ljava/lang/Boolean;

    invoke-virtual {p0, p1}, Lcom/osp/app/signin/ck;->a(Ljava/lang/Boolean;)V

    return-void
.end method
