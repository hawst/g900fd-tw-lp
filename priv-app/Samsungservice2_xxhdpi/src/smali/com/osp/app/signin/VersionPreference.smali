.class public Lcom/osp/app/signin/VersionPreference;
.super Lcom/osp/app/util/BasePreferenceActivity;
.source "VersionPreference.java"


# instance fields
.field private final a:Ljava/lang/String;

.field private b:Landroid/app/AlertDialog;

.field private c:Lcom/osp/app/signin/ps;


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 54
    invoke-direct {p0}, Lcom/osp/app/util/BasePreferenceActivity;-><init>()V

    .line 56
    const-string v0, "VP"

    iput-object v0, p0, Lcom/osp/app/signin/VersionPreference;->a:Ljava/lang/String;

    .line 57
    iput-object v1, p0, Lcom/osp/app/signin/VersionPreference;->b:Landroid/app/AlertDialog;

    .line 59
    iput-object v1, p0, Lcom/osp/app/signin/VersionPreference;->c:Lcom/osp/app/signin/ps;

    .line 387
    return-void
.end method

.method static synthetic a(I)I
    .locals 1

    .prologue
    .line 54
    invoke-static {p0}, Lcom/osp/app/signin/VersionPreference;->d(I)I

    move-result v0

    return v0
.end method

.method static synthetic a(Lcom/osp/app/signin/VersionPreference;Lcom/osp/app/signin/ps;)Lcom/osp/app/signin/ps;
    .locals 0

    .prologue
    .line 54
    iput-object p1, p0, Lcom/osp/app/signin/VersionPreference;->c:Lcom/osp/app/signin/ps;

    return-object p1
.end method

.method static synthetic a(Lcom/osp/app/signin/VersionPreference;)V
    .locals 6

    .prologue
    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 54
    iget-object v0, p0, Lcom/osp/app/signin/VersionPreference;->b:Landroid/app/AlertDialog;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/osp/app/signin/VersionPreference;->b:Landroid/app/AlertDialog;

    invoke-virtual {v0}, Landroid/app/AlertDialog;->isShowing()Z

    move-result v0

    if-nez v0, :cond_1

    :cond_0
    const/4 v0, 0x3

    new-array v0, v0, [Ljava/lang/String;

    const v1, 0x7f090166

    invoke-virtual {p0, v1}, Lcom/osp/app/signin/VersionPreference;->getString(I)Ljava/lang/String;

    move-result-object v1

    aput-object v1, v0, v4

    const v1, 0x7f09014c

    invoke-virtual {p0, v1}, Lcom/osp/app/signin/VersionPreference;->getString(I)Ljava/lang/String;

    move-result-object v1

    aput-object v1, v0, v5

    const/4 v1, 0x2

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const v3, 0x7f09014d

    invoke-virtual {p0, v3}, Lcom/osp/app/signin/VersionPreference;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "\n("

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const v3, 0x7f090167

    invoke-virtual {p0, v3}, Lcom/osp/app/signin/VersionPreference;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ")"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    aput-object v2, v0, v1

    new-instance v1, Landroid/app/AlertDialog$Builder;

    invoke-direct {v1, p0}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    sget-object v2, Lcom/msc/sa/selfupdate/j;->b:Lcom/msc/sa/selfupdate/j;

    invoke-static {p0, v2, v4}, Lcom/msc/sa/selfupdate/c;->a(Landroid/content/Context;Lcom/msc/sa/selfupdate/j;I)I

    move-result v2

    invoke-static {v2}, Lcom/osp/app/signin/VersionPreference;->d(I)I

    move-result v2

    const v3, 0x7f09014b

    invoke-virtual {v1, v3}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    new-instance v3, Lcom/osp/app/signin/pr;

    invoke-direct {v3, p0}, Lcom/osp/app/signin/pr;-><init>(Lcom/osp/app/signin/VersionPreference;)V

    invoke-virtual {v1, v0, v2, v3}, Landroid/app/AlertDialog$Builder;->setSingleChoiceItems([Ljava/lang/CharSequence;ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    invoke-virtual {v0, v5}, Landroid/app/AlertDialog$Builder;->setCancelable(Z)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    const v2, 0x7f090019

    const/4 v3, 0x0

    invoke-virtual {v0, v2, v3}, Landroid/app/AlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    invoke-virtual {v1}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v0

    iput-object v0, p0, Lcom/osp/app/signin/VersionPreference;->b:Landroid/app/AlertDialog;

    iget-object v0, p0, Lcom/osp/app/signin/VersionPreference;->b:Landroid/app/AlertDialog;

    invoke-virtual {v0}, Landroid/app/AlertDialog;->show()V

    :cond_1
    return-void
.end method

.method static synthetic a(Lcom/osp/app/signin/VersionPreference;I)V
    .locals 0

    .prologue
    .line 54
    invoke-direct {p0, p1}, Lcom/osp/app/signin/VersionPreference;->c(I)V

    return-void
.end method

.method static synthetic b(Lcom/osp/app/signin/VersionPreference;)Lcom/osp/app/signin/ps;
    .locals 1

    .prologue
    .line 54
    iget-object v0, p0, Lcom/osp/app/signin/VersionPreference;->c:Lcom/osp/app/signin/ps;

    return-object v0
.end method

.method private b(I)V
    .locals 4

    .prologue
    const v3, 0x7f08011e

    .line 221
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "AccountView::initLayoutParams orientation : "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;)V

    .line 223
    invoke-static {}, Lcom/osp/device/b;->b()Lcom/osp/device/b;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/osp/device/b;->i(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 225
    invoke-virtual {p0}, Lcom/osp/app/signin/VersionPreference;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f08011f

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v0

    float-to-int v0, v0

    .line 226
    invoke-virtual {p0}, Lcom/osp/app/signin/VersionPreference;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1, v3}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v1

    float-to-int v1, v1

    .line 227
    invoke-static {}, Lcom/osp/app/signin/SamsungService;->a()Z

    move-result v2

    if-nez v2, :cond_0

    .line 229
    const v0, 0x7f0c015e

    invoke-virtual {p0, v0}, Lcom/osp/app/signin/VersionPreference;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    .line 230
    invoke-virtual {p0, v0, p1}, Lcom/osp/app/signin/VersionPreference;->a(Landroid/view/ViewGroup;I)V

    .line 232
    invoke-virtual {p0}, Lcom/osp/app/signin/VersionPreference;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0, v3}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v0

    float-to-int v0, v0

    .line 234
    :cond_0
    const v2, 0x7f0c0161

    invoke-virtual {p0, v2}, Lcom/osp/app/signin/VersionPreference;->findViewById(I)Landroid/view/View;

    move-result-object v2

    .line 235
    invoke-static {}, Lcom/osp/app/util/q;->a()Lcom/osp/app/util/q;

    invoke-static {v2, v0, v1, p1}, Lcom/osp/app/util/q;->b(Landroid/view/View;III)V

    .line 237
    :cond_1
    return-void
.end method

.method private c(I)V
    .locals 2

    .prologue
    .line 265
    packed-switch p1, :pswitch_data_0

    .line 276
    const v0, 0x7f090166

    invoke-virtual {p0, v0}, Lcom/osp/app/signin/VersionPreference;->getString(I)Ljava/lang/String;

    move-result-object v0

    move-object v1, v0

    .line 280
    :goto_0
    const-string v0, "select_auto_update"

    invoke-virtual {p0, v0}, Lcom/osp/app/signin/VersionPreference;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    check-cast v0, Landroid/preference/PreferenceScreen;

    .line 281
    invoke-virtual {v0, v1}, Landroid/preference/PreferenceScreen;->setSummary(Ljava/lang/CharSequence;)V

    .line 282
    return-void

    .line 268
    :pswitch_0
    const v0, 0x7f09014c

    invoke-virtual {p0, v0}, Lcom/osp/app/signin/VersionPreference;->getString(I)Ljava/lang/String;

    move-result-object v0

    move-object v1, v0

    .line 269
    goto :goto_0

    .line 272
    :pswitch_1
    const v0, 0x7f09014d

    invoke-virtual {p0, v0}, Lcom/osp/app/signin/VersionPreference;->getString(I)Ljava/lang/String;

    move-result-object v0

    move-object v1, v0

    .line 273
    goto :goto_0

    .line 265
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method static synthetic c(Lcom/osp/app/signin/VersionPreference;)V
    .locals 6

    .prologue
    const/4 v5, 0x1

    .line 54
    invoke-static {p0}, Lcom/osp/app/util/ad;->a(Landroid/content/Context;)Z

    move-result v0

    if-nez v0, :cond_3

    new-instance v2, Landroid/content/Intent;

    invoke-direct {v2}, Landroid/content/Intent;-><init>()V

    const-string v0, "android.net.wifi.PICK_WIFI_NETWORK"

    invoke-static {}, Lcom/osp/device/b;->b()Lcom/osp/device/b;

    move-result-object v1

    invoke-virtual {v1}, Lcom/osp/device/b;->l()I

    move-result v1

    const/16 v3, 0x13

    if-ge v1, v3, :cond_0

    invoke-static {}, Lcom/osp/app/util/r;->f()Z

    move-result v1

    if-nez v1, :cond_0

    sget-object v1, Lcom/osp/app/util/u;->j:[Ljava/lang/String;

    invoke-static {v1}, Lcom/osp/app/util/u;->a([Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_0

    sget-object v1, Lcom/osp/app/util/u;->h:[Ljava/lang/String;

    invoke-static {v1}, Lcom/osp/app/util/u;->a([Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_5

    :cond_0
    invoke-static {}, Lcom/osp/app/signin/SamsungService;->a()Z

    move-result v1

    if-eqz v1, :cond_5

    const-string v0, "com.android.net.wifi.SECSETUP_WIFI_NETWORK"

    invoke-virtual {v2, v0}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    invoke-virtual {p0}, Lcom/osp/app/signin/VersionPreference;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v1

    const/4 v3, 0x0

    invoke-virtual {v1, v2, v3}, Landroid/content/pm/PackageManager;->queryIntentActivities(Landroid/content/Intent;I)Ljava/util/List;

    move-result-object v1

    if-eqz v1, :cond_1

    invoke-interface {v1}, Ljava/util/List;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_4

    :cond_1
    const-string v0, "android.net.wifi.PICK_WIFI_NETWORK"

    move-object v1, v0

    :goto_0
    const-string v0, "android.net.wifi.PICK_WIFI_NETWORK"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    const-string v0, "wifi"

    invoke-virtual {p0, v0}, Lcom/osp/app/signin/VersionPreference;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/net/wifi/WifiManager;

    invoke-virtual {v0}, Landroid/net/wifi/WifiManager;->getWifiState()I

    move-result v3

    const/4 v4, 0x3

    if-ne v3, v4, :cond_6

    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v0, "wifi ok"

    invoke-static {v0}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;)V

    :goto_1
    const-string v0, "only_access_points"

    invoke-virtual {v2, v0, v5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    const-string v0, "extra_prefs_show_button_bar"

    invoke-virtual {v2, v0, v5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    const-string v0, "extra_prefs_set_back_text"

    const v3, 0x7f090092

    invoke-virtual {p0, v3}, Lcom/osp/app/signin/VersionPreference;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v0, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    const-string v0, "wifi_enable_next_on_connect"

    invoke-virtual {v2, v0, v5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    :cond_2
    invoke-virtual {v2, v1}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    const/16 v0, 0xd5

    invoke-virtual {p0, v2, v0}, Lcom/osp/app/signin/VersionPreference;->startActivityForResult(Landroid/content/Intent;I)V

    :cond_3
    return-void

    :cond_4
    const-string v1, "extra_samsungaccount_for_wifisetupwizard"

    const-string v3, "true"

    invoke-virtual {v2, v1, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    :cond_5
    move-object v1, v0

    goto :goto_0

    :cond_6
    invoke-virtual {v0, v5}, Landroid/net/wifi/WifiManager;->setWifiEnabled(Z)Z

    goto :goto_1
.end method

.method private static d(I)I
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 331
    packed-switch p0, :pswitch_data_0

    .line 346
    :goto_0
    :pswitch_0
    return v0

    .line 337
    :pswitch_1
    const/4 v0, 0x1

    .line 338
    goto :goto_0

    .line 340
    :pswitch_2
    const/4 v0, 0x2

    .line 341
    goto :goto_0

    .line 331
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method static synthetic d(Lcom/osp/app/signin/VersionPreference;)Landroid/app/AlertDialog;
    .locals 1

    .prologue
    .line 54
    iget-object v0, p0, Lcom/osp/app/signin/VersionPreference;->b:Landroid/app/AlertDialog;

    return-object v0
.end method


# virtual methods
.method protected onActivityResult(IILandroid/content/Intent;)V
    .locals 1

    .prologue
    .line 175
    invoke-super {p0, p1, p2, p3}, Lcom/osp/app/util/BasePreferenceActivity;->onActivityResult(IILandroid/content/Intent;)V

    .line 177
    const/16 v0, 0xd5

    if-ne p1, v0, :cond_0

    .line 179
    const/4 v0, -0x1

    if-ne p2, v0, :cond_0

    .line 181
    new-instance v0, Lcom/osp/app/signin/ps;

    invoke-direct {v0, p0}, Lcom/osp/app/signin/ps;-><init>(Lcom/osp/app/signin/VersionPreference;)V

    iput-object v0, p0, Lcom/osp/app/signin/VersionPreference;->c:Lcom/osp/app/signin/ps;

    .line 182
    iget-object v0, p0, Lcom/osp/app/signin/VersionPreference;->c:Lcom/osp/app/signin/ps;

    invoke-virtual {v0}, Lcom/osp/app/signin/ps;->b()V

    .line 189
    :cond_0
    return-void
.end method

.method public onConfigurationChanged(Landroid/content/res/Configuration;)V
    .locals 1

    .prologue
    .line 129
    iget v0, p1, Landroid/content/res/Configuration;->orientation:I

    invoke-direct {p0, v0}, Lcom/osp/app/signin/VersionPreference;->b(I)V

    .line 131
    invoke-super {p0, p1}, Lcom/osp/app/util/BasePreferenceActivity;->onConfigurationChanged(Landroid/content/res/Configuration;)V

    .line 132
    return-void
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 10

    .prologue
    const v9, 0x7f08011d

    const v8, 0x7f07003f

    const/4 v4, 0x1

    const/4 v7, 0x0

    .line 64
    invoke-super {p0, p1}, Lcom/osp/app/util/BasePreferenceActivity;->onCreate(Landroid/os/Bundle;)V

    .line 66
    const v0, 0x7f03005c

    invoke-virtual {p0, v0}, Lcom/osp/app/signin/VersionPreference;->setContentView(I)V

    .line 67
    const v0, 0x7f040005

    invoke-virtual {p0, v0}, Lcom/osp/app/signin/VersionPreference;->addPreferencesFromResource(I)V

    .line 69
    invoke-static {}, Lcom/osp/device/b;->b()Lcom/osp/device/b;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/osp/device/b;->i(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 71
    invoke-virtual {p0}, Lcom/osp/app/signin/VersionPreference;->a()Z

    move-result v0

    if-eqz v0, :cond_5

    .line 73
    invoke-virtual {p0}, Lcom/osp/app/signin/VersionPreference;->b()V

    .line 80
    :cond_0
    :goto_0
    invoke-static {p0}, Lcom/msc/sa/c/d;->a(Landroid/app/Activity;)V

    .line 82
    invoke-virtual {p0}, Lcom/osp/app/signin/VersionPreference;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v0

    iget v0, v0, Landroid/content/res/Configuration;->orientation:I

    invoke-direct {p0, v0}, Lcom/osp/app/signin/VersionPreference;->b(I)V

    .line 84
    const v0, 0x7f0c00de

    invoke-virtual {p0, v0}, Lcom/osp/app/signin/VersionPreference;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    if-eqz v0, :cond_1

    invoke-static {}, Lcom/osp/device/b;->b()Lcom/osp/device/b;

    move-result-object v1

    invoke-virtual {v1, p0}, Lcom/osp/device/b;->i(Landroid/content/Context;)Z

    move-result v1

    if-nez v1, :cond_1

    invoke-static {p0}, Lcom/osp/app/util/r;->g(Landroid/content/Context;)Z

    move-result v1

    if-eqz v1, :cond_1

    const/4 v1, 0x0

    invoke-virtual {v0, v1, v7}, Landroid/widget/TextView;->setTypeface(Landroid/graphics/Typeface;I)V

    :cond_1
    const v0, 0x7f0c00df

    invoke-virtual {p0, v0}, Lcom/osp/app/signin/VersionPreference;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    if-eqz v0, :cond_3

    const v1, 0x7f090095

    new-array v2, v4, [Ljava/lang/Object;

    invoke-static {}, Lcom/osp/device/b;->b()Lcom/osp/device/b;

    move-result-object v3

    invoke-virtual {v3, p0}, Lcom/osp/device/b;->j(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v3

    aput-object v3, v2, v7

    invoke-virtual {p0, v1, v2}, Lcom/osp/app/signin/VersionPreference;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    invoke-static {p0}, Lcom/osp/app/util/r;->g(Landroid/content/Context;)Z

    move-result v1

    if-eqz v1, :cond_2

    invoke-static {}, Lcom/osp/device/b;->b()Lcom/osp/device/b;

    move-result-object v1

    invoke-virtual {v1, p0}, Lcom/osp/device/b;->i(Landroid/content/Context;)Z

    move-result v1

    if-nez v1, :cond_2

    invoke-virtual {p0}, Lcom/osp/app/signin/VersionPreference;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1, v8}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setTextColor(I)V

    invoke-virtual {p0}, Lcom/osp/app/signin/VersionPreference;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1, v9}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v1

    invoke-virtual {v0, v7, v1}, Landroid/widget/TextView;->setTextSize(IF)V

    :cond_2
    sget-object v0, Lcom/msc/sa/selfupdate/j;->b:Lcom/msc/sa/selfupdate/j;

    invoke-static {p0, v0, v7}, Lcom/msc/sa/selfupdate/c;->a(Landroid/content/Context;Lcom/msc/sa/selfupdate/j;I)I

    move-result v0

    invoke-direct {p0, v0}, Lcom/osp/app/signin/VersionPreference;->c(I)V

    :cond_3
    const v0, 0x7f0c00e0

    invoke-virtual {p0, v0}, Lcom/osp/app/signin/VersionPreference;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    if-eqz v0, :cond_4

    const v1, 0x7f090094

    new-array v2, v4, [Ljava/lang/Object;

    invoke-static {}, Lcom/osp/device/b;->b()Lcom/osp/device/b;

    move-result-object v3

    invoke-virtual {v3, p0}, Lcom/osp/device/b;->c(Landroid/content/Context;)J

    move-result-wide v4

    invoke-static {p0}, Lcom/osp/app/util/ad;->q(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v3

    new-instance v6, Ljava/text/SimpleDateFormat;

    invoke-direct {v6, v3}, Ljava/text/SimpleDateFormat;-><init>(Ljava/lang/String;)V

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    invoke-virtual {v6, v3}, Ljava/text/SimpleDateFormat;->format(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    aput-object v3, v2, v7

    invoke-virtual {p0, v1, v2}, Lcom/osp/app/signin/VersionPreference;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    invoke-static {p0}, Lcom/osp/app/util/r;->g(Landroid/content/Context;)Z

    move-result v1

    if-eqz v1, :cond_4

    invoke-static {}, Lcom/osp/device/b;->b()Lcom/osp/device/b;

    move-result-object v1

    invoke-virtual {v1, p0}, Lcom/osp/device/b;->i(Landroid/content/Context;)Z

    move-result v1

    if-nez v1, :cond_4

    invoke-virtual {p0}, Lcom/osp/app/signin/VersionPreference;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1, v8}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setTextColor(I)V

    invoke-virtual {p0}, Lcom/osp/app/signin/VersionPreference;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1, v9}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v1

    invoke-virtual {v0, v7, v1}, Landroid/widget/TextView;->setTextSize(IF)V

    .line 86
    :cond_4
    const-string v0, "select_auto_update"

    invoke-virtual {p0, v0}, Lcom/osp/app/signin/VersionPreference;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    check-cast v0, Landroid/preference/PreferenceScreen;

    const-string v1, "check_update"

    invoke-virtual {p0, v1}, Lcom/osp/app/signin/VersionPreference;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v1

    check-cast v1, Landroid/preference/PreferenceScreen;

    new-instance v2, Lcom/osp/app/signin/pp;

    invoke-direct {v2, p0}, Lcom/osp/app/signin/pp;-><init>(Lcom/osp/app/signin/VersionPreference;)V

    invoke-virtual {v0, v2}, Landroid/preference/PreferenceScreen;->setOnPreferenceClickListener(Landroid/preference/Preference$OnPreferenceClickListener;)V

    new-instance v0, Lcom/osp/app/signin/pq;

    invoke-direct {v0, p0}, Lcom/osp/app/signin/pq;-><init>(Lcom/osp/app/signin/VersionPreference;)V

    invoke-virtual {v1, v0}, Landroid/preference/PreferenceScreen;->setOnPreferenceClickListener(Landroid/preference/Preference$OnPreferenceClickListener;)V

    .line 89
    return-void

    .line 76
    :cond_5
    invoke-virtual {p0}, Lcom/osp/app/signin/VersionPreference;->c()V

    goto/16 :goto_0
.end method

.method public onOptionsItemSelected(Landroid/view/MenuItem;)Z
    .locals 2

    .prologue
    .line 242
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0xb

    if-lt v0, v1, :cond_0

    .line 244
    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    .line 248
    :cond_0
    :goto_0
    invoke-super {p0, p1}, Lcom/osp/app/util/BasePreferenceActivity;->onOptionsItemSelected(Landroid/view/MenuItem;)Z

    move-result v0

    return v0

    .line 247
    :pswitch_0
    invoke-virtual {p0}, Lcom/osp/app/signin/VersionPreference;->onBackPressed()V

    goto :goto_0

    .line 244
    :pswitch_data_0
    .packed-switch 0x102002c
        :pswitch_0
    .end packed-switch
.end method

.method protected onPause()V
    .locals 0

    .prologue
    .line 200
    invoke-super {p0}, Lcom/osp/app/util/BasePreferenceActivity;->onPause()V

    .line 201
    return-void
.end method
