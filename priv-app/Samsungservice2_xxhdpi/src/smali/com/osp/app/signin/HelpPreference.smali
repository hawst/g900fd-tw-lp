.class public Lcom/osp/app/signin/HelpPreference;
.super Lcom/osp/app/util/BasePreferenceActivity;
.source "HelpPreference.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 32
    invoke-direct {p0}, Lcom/osp/app/util/BasePreferenceActivity;-><init>()V

    return-void
.end method

.method private a(I)V
    .locals 4

    .prologue
    const v3, 0x7f08011e

    .line 249
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "HelpPreference::initLayoutParams orientation : "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;)V

    .line 251
    invoke-static {}, Lcom/osp/device/b;->b()Lcom/osp/device/b;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/osp/device/b;->i(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 253
    invoke-virtual {p0}, Lcom/osp/app/signin/HelpPreference;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f08011f

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v0

    float-to-int v0, v0

    .line 254
    invoke-virtual {p0}, Lcom/osp/app/signin/HelpPreference;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1, v3}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v1

    float-to-int v1, v1

    .line 255
    invoke-static {}, Lcom/osp/app/signin/SamsungService;->a()Z

    move-result v2

    if-nez v2, :cond_0

    .line 257
    const v0, 0x7f0c015e

    invoke-virtual {p0, v0}, Lcom/osp/app/signin/HelpPreference;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    .line 258
    invoke-virtual {p0, v0, p1}, Lcom/osp/app/signin/HelpPreference;->a(Landroid/view/ViewGroup;I)V

    .line 260
    invoke-virtual {p0}, Lcom/osp/app/signin/HelpPreference;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0, v3}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v0

    float-to-int v0, v0

    .line 262
    :cond_0
    const v2, 0x7f0c0161

    invoke-virtual {p0, v2}, Lcom/osp/app/signin/HelpPreference;->findViewById(I)Landroid/view/View;

    move-result-object v2

    .line 263
    invoke-static {}, Lcom/osp/app/util/q;->a()Lcom/osp/app/util/q;

    invoke-static {v2, v0, v1, p1}, Lcom/osp/app/util/q;->b(Landroid/view/View;III)V

    .line 265
    :cond_1
    return-void
.end method

.method static synthetic a(Lcom/osp/app/signin/HelpPreference;Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 32
    new-instance v0, Landroid/content/Intent;

    const-string v1, "com.msc.action.samsungaccount.web_with_close_button"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    const-string v1, "ServerUrl"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    invoke-virtual {p0, v0}, Lcom/osp/app/signin/HelpPreference;->startActivity(Landroid/content/Intent;)V

    return-void
.end method

.method static synthetic a(Lcom/osp/app/signin/HelpPreference;)Z
    .locals 1

    .prologue
    .line 32
    invoke-virtual {p0}, Lcom/osp/app/signin/HelpPreference;->a()Z

    move-result v0

    return v0
.end method

.method static synthetic b(Lcom/osp/app/signin/HelpPreference;Ljava/lang/String;)Ljava/lang/String;
    .locals 4

    .prologue
    .line 32
    invoke-static {}, Lcom/osp/device/b;->b()Lcom/osp/device/b;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/osp/device/b;->i(Landroid/content/Context;)Z

    move-result v0

    invoke-static {}, Lcom/msc/c/g;->a()Lcom/msc/c/g;

    invoke-static {v0}, Lcom/msc/c/k;->a(Z)Ljava/lang/String;

    move-result-object v1

    invoke-static {}, Lcom/msc/c/g;->a()Lcom/msc/c/g;

    invoke-virtual {p0}, Lcom/osp/app/signin/HelpPreference;->getApplicationContext()Landroid/content/Context;

    move-result-object v2

    invoke-static {p0}, Lcom/osp/app/util/ad;->g(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3, p1, v1, v0}, Lcom/msc/c/g;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Z)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method static synthetic b(Lcom/osp/app/signin/HelpPreference;)Z
    .locals 1

    .prologue
    .line 32
    invoke-virtual {p0}, Lcom/osp/app/signin/HelpPreference;->a()Z

    move-result v0

    return v0
.end method


# virtual methods
.method protected onActivityResult(IILandroid/content/Intent;)V
    .locals 2

    .prologue
    .line 295
    invoke-super {p0, p1, p2, p3}, Lcom/osp/app/util/BasePreferenceActivity;->onActivityResult(IILandroid/content/Intent;)V

    .line 296
    packed-switch p2, :pswitch_data_0

    .line 301
    :goto_0
    return-void

    .line 299
    :pswitch_0
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "["

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "] setResultWithLog resultCode=[10]"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/osp/app/util/aq;->c(Ljava/lang/String;)V

    const/16 v0, 0xa

    invoke-virtual {p0, v0}, Lcom/osp/app/util/BasePreferenceActivity;->setResult(I)V

    .line 300
    invoke-virtual {p0}, Lcom/osp/app/signin/HelpPreference;->finish()V

    goto :goto_0

    .line 296
    nop

    :pswitch_data_0
    .packed-switch 0xa
        :pswitch_0
    .end packed-switch
.end method

.method public onBackPressed()V
    .locals 1

    .prologue
    .line 288
    const/16 v0, 0xe

    invoke-virtual {p0, v0}, Lcom/osp/app/signin/HelpPreference;->setResult(I)V

    .line 289
    invoke-super {p0}, Lcom/osp/app/util/BasePreferenceActivity;->onBackPressed()V

    .line 290
    return-void
.end method

.method public onConfigurationChanged(Landroid/content/res/Configuration;)V
    .locals 1

    .prologue
    .line 235
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v0, "onConfigurationChanged()"

    invoke-static {v0}, Lcom/osp/app/util/aq;->c(Ljava/lang/String;)V

    .line 237
    iget v0, p1, Landroid/content/res/Configuration;->orientation:I

    invoke-direct {p0, v0}, Lcom/osp/app/signin/HelpPreference;->a(I)V

    .line 239
    invoke-super {p0, p1}, Lcom/osp/app/util/BasePreferenceActivity;->onConfigurationChanged(Landroid/content/res/Configuration;)V

    .line 240
    return-void
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 50
    const/4 v0, 0x0

    invoke-static {v0}, Lcom/osp/app/signin/SamsungService;->a(Z)V

    .line 51
    invoke-super {p0, p1}, Lcom/osp/app/util/BasePreferenceActivity;->onCreate(Landroid/os/Bundle;)V

    .line 81
    invoke-static {p0}, Lcom/msc/sa/c/d;->a(Landroid/app/Activity;)V

    .line 83
    invoke-virtual {p0}, Lcom/osp/app/signin/HelpPreference;->getApplication()Landroid/app/Application;

    move-result-object v0

    check-cast v0, Lcom/osp/app/signin/SamsungService;

    invoke-virtual {v0, p0}, Lcom/osp/app/signin/SamsungService;->a(Landroid/app/Activity;)V

    .line 87
    const v0, 0x7f030005

    invoke-virtual {p0, v0}, Lcom/osp/app/signin/HelpPreference;->setContentView(I)V

    .line 88
    const v0, 0x7f040002

    invoke-virtual {p0, v0}, Lcom/osp/app/signin/HelpPreference;->addPreferencesFromResource(I)V

    .line 89
    invoke-virtual {p0}, Lcom/osp/app/signin/HelpPreference;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v0

    iget v0, v0, Landroid/content/res/Configuration;->orientation:I

    invoke-direct {p0, v0}, Lcom/osp/app/signin/HelpPreference;->a(I)V

    .line 90
    const v0, 0x7f0c0036

    invoke-virtual {p0, v0}, Lcom/osp/app/signin/HelpPreference;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    .line 91
    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 93
    const-string v0, "Samsung_services"

    invoke-virtual {p0, v0}, Lcom/osp/app/signin/HelpPreference;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    check-cast v0, Landroid/preference/PreferenceScreen;

    .line 94
    new-instance v1, Lcom/osp/app/signin/fn;

    invoke-direct {v1, p0}, Lcom/osp/app/signin/fn;-><init>(Lcom/osp/app/signin/HelpPreference;)V

    invoke-virtual {v0, v1}, Landroid/preference/PreferenceScreen;->setOnPreferenceClickListener(Landroid/preference/Preference$OnPreferenceClickListener;)V

    .line 117
    const-string v0, "find_email_or_password"

    invoke-virtual {p0, v0}, Lcom/osp/app/signin/HelpPreference;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    check-cast v0, Landroid/preference/PreferenceScreen;

    .line 118
    new-instance v1, Lcom/osp/app/signin/fo;

    invoke-direct {v1, p0}, Lcom/osp/app/signin/fo;-><init>(Lcom/osp/app/signin/HelpPreference;)V

    invoke-virtual {v0, v1}, Landroid/preference/PreferenceScreen;->setOnPreferenceClickListener(Landroid/preference/Preference$OnPreferenceClickListener;)V

    .line 139
    const-string v0, "contact_us"

    invoke-virtual {p0, v0}, Lcom/osp/app/signin/HelpPreference;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    check-cast v0, Landroid/preference/PreferenceScreen;

    .line 140
    new-instance v1, Lcom/osp/app/signin/fp;

    invoke-direct {v1, p0}, Lcom/osp/app/signin/fp;-><init>(Lcom/osp/app/signin/HelpPreference;)V

    invoke-virtual {v0, v1}, Landroid/preference/PreferenceScreen;->setOnPreferenceClickListener(Landroid/preference/Preference$OnPreferenceClickListener;)V

    .line 172
    const-string v0, "support"

    invoke-virtual {p0, v0}, Lcom/osp/app/signin/HelpPreference;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    check-cast v0, Landroid/preference/PreferenceScreen;

    .line 173
    new-instance v1, Lcom/osp/app/signin/fq;

    invoke-direct {v1, p0}, Lcom/osp/app/signin/fq;-><init>(Lcom/osp/app/signin/HelpPreference;)V

    invoke-virtual {v0, v1}, Landroid/preference/PreferenceScreen;->setOnPreferenceClickListener(Landroid/preference/Preference$OnPreferenceClickListener;)V

    .line 219
    return-void
.end method

.method public onOptionsItemSelected(Landroid/view/MenuItem;)Z
    .locals 2

    .prologue
    .line 270
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0xb

    if-lt v0, v1, :cond_0

    .line 272
    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    .line 276
    :cond_0
    :goto_0
    invoke-super {p0, p1}, Lcom/osp/app/util/BasePreferenceActivity;->onOptionsItemSelected(Landroid/view/MenuItem;)Z

    move-result v0

    return v0

    .line 275
    :pswitch_0
    invoke-virtual {p0}, Lcom/osp/app/signin/HelpPreference;->onBackPressed()V

    goto :goto_0

    .line 272
    :pswitch_data_0
    .packed-switch 0x102002c
        :pswitch_0
    .end packed-switch
.end method
