.class final Lcom/osp/app/signin/op;
.super Landroid/widget/BaseAdapter;
.source "TnCView.java"


# instance fields
.field final synthetic a:Lcom/osp/app/signin/TnCView;

.field private final b:Landroid/view/LayoutInflater;

.field private final c:Landroid/widget/CompoundButton$OnCheckedChangeListener;

.field private final d:Landroid/widget/CompoundButton$OnCheckedChangeListener;

.field private final e:Landroid/widget/CompoundButton$OnCheckedChangeListener;

.field private final f:Landroid/widget/CompoundButton$OnCheckedChangeListener;

.field private final g:Landroid/widget/CompoundButton$OnCheckedChangeListener;

.field private final h:Landroid/widget/CompoundButton$OnCheckedChangeListener;


# direct methods
.method public constructor <init>(Lcom/osp/app/signin/TnCView;)V
    .locals 1

    .prologue
    .line 2455
    iput-object p1, p0, Lcom/osp/app/signin/op;->a:Lcom/osp/app/signin/TnCView;

    invoke-direct {p0}, Landroid/widget/BaseAdapter;-><init>()V

    .line 3339
    new-instance v0, Lcom/osp/app/signin/oq;

    invoke-direct {v0, p0}, Lcom/osp/app/signin/oq;-><init>(Lcom/osp/app/signin/op;)V

    iput-object v0, p0, Lcom/osp/app/signin/op;->c:Landroid/widget/CompoundButton$OnCheckedChangeListener;

    .line 3452
    new-instance v0, Lcom/osp/app/signin/or;

    invoke-direct {v0, p0}, Lcom/osp/app/signin/or;-><init>(Lcom/osp/app/signin/op;)V

    iput-object v0, p0, Lcom/osp/app/signin/op;->d:Landroid/widget/CompoundButton$OnCheckedChangeListener;

    .line 3468
    new-instance v0, Lcom/osp/app/signin/os;

    invoke-direct {v0, p0}, Lcom/osp/app/signin/os;-><init>(Lcom/osp/app/signin/op;)V

    iput-object v0, p0, Lcom/osp/app/signin/op;->e:Landroid/widget/CompoundButton$OnCheckedChangeListener;

    .line 3484
    new-instance v0, Lcom/osp/app/signin/ot;

    invoke-direct {v0, p0}, Lcom/osp/app/signin/ot;-><init>(Lcom/osp/app/signin/op;)V

    iput-object v0, p0, Lcom/osp/app/signin/op;->f:Landroid/widget/CompoundButton$OnCheckedChangeListener;

    .line 3500
    new-instance v0, Lcom/osp/app/signin/ou;

    invoke-direct {v0, p0}, Lcom/osp/app/signin/ou;-><init>(Lcom/osp/app/signin/op;)V

    iput-object v0, p0, Lcom/osp/app/signin/op;->g:Landroid/widget/CompoundButton$OnCheckedChangeListener;

    .line 3516
    new-instance v0, Lcom/osp/app/signin/ov;

    invoke-direct {v0, p0}, Lcom/osp/app/signin/ov;-><init>(Lcom/osp/app/signin/op;)V

    iput-object v0, p0, Lcom/osp/app/signin/op;->h:Landroid/widget/CompoundButton$OnCheckedChangeListener;

    .line 2456
    invoke-static {p1}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    iput-object v0, p0, Lcom/osp/app/signin/op;->b:Landroid/view/LayoutInflater;

    .line 2457
    return-void
.end method

.method private a(II)Landroid/view/View;
    .locals 9

    .prologue
    const/4 v8, 0x3

    const/4 v7, 0x2

    const/4 v3, 0x0

    const/4 v2, 0x1

    .line 2683
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "TnCView - makeCheckboxItem - strId : "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;)V

    .line 2684
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "TnCView - makeCheckboxItem - position : "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;)V

    .line 2689
    iget-object v0, p0, Lcom/osp/app/signin/op;->b:Landroid/view/LayoutInflater;

    const v1, 0x7f030058

    const/4 v4, 0x0

    invoke-virtual {v0, v1, v4}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v4

    .line 2690
    invoke-virtual {v4, v2}, Landroid/view/View;->setEnabled(Z)V

    .line 2692
    const v0, 0x7f0c017e

    invoke-virtual {v4, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/CheckBox;

    .line 2693
    const v1, 0x7f0c017f

    invoke-virtual {v4, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    .line 2695
    sget v5, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v6, 0x10

    if-le v5, v6, :cond_0

    .line 2697
    iget-object v5, p0, Lcom/osp/app/signin/op;->a:Lcom/osp/app/signin/TnCView;

    invoke-virtual {v5}, Lcom/osp/app/signin/TnCView;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    const v6, 0x7f0800e2

    invoke-virtual {v5, v6}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v5

    float-to-int v5, v5

    .line 2699
    invoke-static {v0, v5, v5, v3, v5}, Lcom/msc/sa/c/d;->a(Landroid/view/View;IIII)V

    .line 2711
    :cond_0
    if-lez p1, :cond_2

    .line 2713
    invoke-virtual {v1, p1}, Landroid/widget/TextView;->setText(I)V

    .line 2716
    invoke-direct {p0, v1}, Lcom/osp/app/signin/op;->a(Landroid/widget/TextView;)V

    .line 2718
    iget-object v5, p0, Lcom/osp/app/signin/op;->a:Lcom/osp/app/signin/TnCView;

    invoke-virtual {v5}, Lcom/osp/app/signin/TnCView;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    iget-object v6, p0, Lcom/osp/app/signin/op;->a:Lcom/osp/app/signin/TnCView;

    invoke-static {v6}, Lcom/osp/app/util/an;->a(Lcom/osp/app/util/BaseActivity;)Lcom/osp/app/util/an;

    const v6, 0x7f070004

    invoke-static {v6}, Lcom/osp/app/util/an;->a(I)I

    move-result v6

    invoke-virtual {v5, v6}, Landroid/content/res/Resources;->getColor(I)I

    move-result v5

    invoke-virtual {v1, v5}, Landroid/widget/TextView;->setTextColor(I)V

    .line 2721
    iget-object v5, p0, Lcom/osp/app/signin/op;->a:Lcom/osp/app/signin/TnCView;

    invoke-static {v5}, Lcom/osp/app/signin/TnCView;->h(Lcom/osp/app/signin/TnCView;)I

    move-result v5

    if-eqz v5, :cond_1

    iget-object v5, p0, Lcom/osp/app/signin/op;->a:Lcom/osp/app/signin/TnCView;

    invoke-static {v5}, Lcom/osp/app/signin/TnCView;->h(Lcom/osp/app/signin/TnCView;)I

    move-result v5

    if-ne v5, v7, :cond_2

    .line 2726
    :cond_1
    iget-object v5, p0, Lcom/osp/app/signin/op;->a:Lcom/osp/app/signin/TnCView;

    invoke-static {v5}, Lcom/osp/app/util/an;->a(Lcom/osp/app/util/BaseActivity;)Lcom/osp/app/util/an;

    const v5, 0x7f080110

    invoke-static {v5}, Lcom/osp/app/util/an;->b(I)I

    move-result v5

    .line 2727
    iget-object v6, p0, Lcom/osp/app/signin/op;->a:Lcom/osp/app/signin/TnCView;

    invoke-virtual {v6}, Lcom/osp/app/signin/TnCView;->getResources()Landroid/content/res/Resources;

    move-result-object v6

    invoke-virtual {v6, v5}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v5

    invoke-virtual {v1, v3, v5}, Landroid/widget/TextView;->setTextSize(IF)V

    .line 2730
    const v5, 0x7f0901ab

    if-ne p1, v5, :cond_2

    .line 2732
    invoke-virtual {v1}, Landroid/widget/TextView;->getTypeface()Landroid/graphics/Typeface;

    move-result-object v5

    invoke-virtual {v1, v5, v2}, Landroid/widget/TextView;->setTypeface(Landroid/graphics/Typeface;I)V

    .line 2737
    :cond_2
    iget-object v1, p0, Lcom/osp/app/signin/op;->a:Lcom/osp/app/signin/TnCView;

    invoke-static {v1}, Lcom/osp/app/signin/TnCView;->h(Lcom/osp/app/signin/TnCView;)I

    move-result v1

    packed-switch v1, :pswitch_data_0

    .line 2822
    :goto_0
    :pswitch_0
    const v0, 0x7f0c00e9

    invoke-virtual {v4, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    .line 2908
    if-eqz v0, :cond_3

    .line 2910
    invoke-virtual {v0, v3}, Landroid/view/View;->setVisibility(I)V

    .line 2915
    :cond_3
    return-object v4

    .line 2760
    :pswitch_1
    if-ne p2, v2, :cond_7

    .line 2762
    iget-object v1, p0, Lcom/osp/app/signin/op;->a:Lcom/osp/app/signin/TnCView;

    invoke-static {v1, v0}, Lcom/osp/app/signin/TnCView;->a(Lcom/osp/app/signin/TnCView;Landroid/widget/CheckBox;)Landroid/widget/CheckBox;

    .line 2763
    iget-object v1, p0, Lcom/osp/app/signin/op;->a:Lcom/osp/app/signin/TnCView;

    invoke-static {v1}, Lcom/osp/app/signin/TnCView;->k(Lcom/osp/app/signin/TnCView;)Z

    move-result v1

    if-eqz v1, :cond_6

    iget-object v1, p0, Lcom/osp/app/signin/op;->a:Lcom/osp/app/signin/TnCView;

    invoke-static {v1}, Lcom/osp/app/signin/TnCView;->l(Lcom/osp/app/signin/TnCView;)Z

    move-result v1

    if-eqz v1, :cond_6

    iget-object v1, p0, Lcom/osp/app/signin/op;->a:Lcom/osp/app/signin/TnCView;

    invoke-static {v1}, Lcom/osp/app/signin/TnCView;->m(Lcom/osp/app/signin/TnCView;)Z

    move-result v1

    if-eqz v1, :cond_6

    iget-object v1, p0, Lcom/osp/app/signin/op;->a:Lcom/osp/app/signin/TnCView;

    invoke-static {v1}, Lcom/osp/app/util/r;->b(Landroid/content/Context;)Z

    move-result v1

    if-eqz v1, :cond_4

    iget-object v1, p0, Lcom/osp/app/signin/op;->a:Lcom/osp/app/signin/TnCView;

    invoke-static {v1}, Lcom/osp/app/signin/TnCView;->n(Lcom/osp/app/signin/TnCView;)Z

    move-result v1

    if-eqz v1, :cond_6

    :cond_4
    iget-object v1, p0, Lcom/osp/app/signin/op;->a:Lcom/osp/app/signin/TnCView;

    invoke-static {v1}, Lcom/osp/app/signin/TnCView;->i(Lcom/osp/app/signin/TnCView;)Z

    move-result v1

    if-eqz v1, :cond_5

    iget-object v1, p0, Lcom/osp/app/signin/op;->a:Lcom/osp/app/signin/TnCView;

    invoke-static {v1}, Lcom/osp/app/signin/TnCView;->a(Lcom/osp/app/signin/TnCView;)Z

    move-result v1

    if-eqz v1, :cond_6

    :cond_5
    move v1, v2

    :goto_1
    invoke-virtual {v0, v1}, Landroid/widget/CheckBox;->setChecked(Z)V

    .line 2766
    iget-object v1, p0, Lcom/osp/app/signin/op;->c:Landroid/widget/CompoundButton$OnCheckedChangeListener;

    invoke-virtual {v0, v1}, Landroid/widget/CheckBox;->setOnCheckedChangeListener(Landroid/widget/CompoundButton$OnCheckedChangeListener;)V

    goto :goto_0

    :cond_6
    move v1, v3

    .line 2763
    goto :goto_1

    .line 2767
    :cond_7
    if-ne p2, v7, :cond_8

    .line 2769
    iget-object v1, p0, Lcom/osp/app/signin/op;->a:Lcom/osp/app/signin/TnCView;

    invoke-static {v1, v0}, Lcom/osp/app/signin/TnCView;->b(Lcom/osp/app/signin/TnCView;Landroid/widget/CheckBox;)Landroid/widget/CheckBox;

    .line 2770
    iget-object v1, p0, Lcom/osp/app/signin/op;->a:Lcom/osp/app/signin/TnCView;

    invoke-static {v1}, Lcom/osp/app/signin/TnCView;->k(Lcom/osp/app/signin/TnCView;)Z

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/CheckBox;->setChecked(Z)V

    .line 2771
    iget-object v1, p0, Lcom/osp/app/signin/op;->d:Landroid/widget/CompoundButton$OnCheckedChangeListener;

    invoke-virtual {v0, v1}, Landroid/widget/CheckBox;->setOnCheckedChangeListener(Landroid/widget/CompoundButton$OnCheckedChangeListener;)V

    goto :goto_0

    .line 2772
    :cond_8
    if-ne p2, v8, :cond_9

    .line 2774
    iget-object v1, p0, Lcom/osp/app/signin/op;->a:Lcom/osp/app/signin/TnCView;

    invoke-static {v1, v0}, Lcom/osp/app/signin/TnCView;->c(Lcom/osp/app/signin/TnCView;Landroid/widget/CheckBox;)Landroid/widget/CheckBox;

    .line 2775
    iget-object v1, p0, Lcom/osp/app/signin/op;->a:Lcom/osp/app/signin/TnCView;

    invoke-static {v1}, Lcom/osp/app/signin/TnCView;->l(Lcom/osp/app/signin/TnCView;)Z

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/CheckBox;->setChecked(Z)V

    .line 2776
    iget-object v1, p0, Lcom/osp/app/signin/op;->e:Landroid/widget/CompoundButton$OnCheckedChangeListener;

    invoke-virtual {v0, v1}, Landroid/widget/CheckBox;->setOnCheckedChangeListener(Landroid/widget/CompoundButton$OnCheckedChangeListener;)V

    goto/16 :goto_0

    .line 2777
    :cond_9
    const/4 v1, 0x4

    if-ne p2, v1, :cond_a

    .line 2779
    iget-object v1, p0, Lcom/osp/app/signin/op;->a:Lcom/osp/app/signin/TnCView;

    invoke-static {v1, v0}, Lcom/osp/app/signin/TnCView;->d(Lcom/osp/app/signin/TnCView;Landroid/widget/CheckBox;)Landroid/widget/CheckBox;

    .line 2780
    iget-object v1, p0, Lcom/osp/app/signin/op;->a:Lcom/osp/app/signin/TnCView;

    invoke-static {v1}, Lcom/osp/app/signin/TnCView;->m(Lcom/osp/app/signin/TnCView;)Z

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/CheckBox;->setChecked(Z)V

    .line 2781
    iget-object v1, p0, Lcom/osp/app/signin/op;->f:Landroid/widget/CompoundButton$OnCheckedChangeListener;

    invoke-virtual {v0, v1}, Landroid/widget/CheckBox;->setOnCheckedChangeListener(Landroid/widget/CompoundButton$OnCheckedChangeListener;)V

    goto/16 :goto_0

    .line 2782
    :cond_a
    iget-object v1, p0, Lcom/osp/app/signin/op;->a:Lcom/osp/app/signin/TnCView;

    invoke-static {v1}, Lcom/osp/app/util/r;->b(Landroid/content/Context;)Z

    move-result v1

    if-eqz v1, :cond_b

    const/4 v1, 0x5

    if-ne p2, v1, :cond_b

    .line 2784
    iget-object v1, p0, Lcom/osp/app/signin/op;->a:Lcom/osp/app/signin/TnCView;

    invoke-static {v1, v0}, Lcom/osp/app/signin/TnCView;->e(Lcom/osp/app/signin/TnCView;Landroid/widget/CheckBox;)Landroid/widget/CheckBox;

    .line 2785
    iget-object v1, p0, Lcom/osp/app/signin/op;->a:Lcom/osp/app/signin/TnCView;

    invoke-static {v1}, Lcom/osp/app/signin/TnCView;->n(Lcom/osp/app/signin/TnCView;)Z

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/CheckBox;->setChecked(Z)V

    .line 2786
    iget-object v1, p0, Lcom/osp/app/signin/op;->g:Landroid/widget/CompoundButton$OnCheckedChangeListener;

    invoke-virtual {v0, v1}, Landroid/widget/CheckBox;->setOnCheckedChangeListener(Landroid/widget/CompoundButton$OnCheckedChangeListener;)V

    goto/16 :goto_0

    .line 2789
    :cond_b
    iget-object v1, p0, Lcom/osp/app/signin/op;->a:Lcom/osp/app/signin/TnCView;

    invoke-static {v1, v0}, Lcom/osp/app/signin/TnCView;->f(Lcom/osp/app/signin/TnCView;Landroid/widget/CheckBox;)Landroid/widget/CheckBox;

    .line 2790
    iget-object v1, p0, Lcom/osp/app/signin/op;->a:Lcom/osp/app/signin/TnCView;

    invoke-static {v1}, Lcom/osp/app/signin/TnCView;->a(Lcom/osp/app/signin/TnCView;)Z

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/CheckBox;->setChecked(Z)V

    .line 2791
    iget-object v1, p0, Lcom/osp/app/signin/op;->h:Landroid/widget/CompoundButton$OnCheckedChangeListener;

    invoke-virtual {v0, v1}, Landroid/widget/CheckBox;->setOnCheckedChangeListener(Landroid/widget/CompoundButton$OnCheckedChangeListener;)V

    goto/16 :goto_0

    .line 2795
    :pswitch_2
    if-nez p2, :cond_e

    .line 2797
    iget-object v1, p0, Lcom/osp/app/signin/op;->a:Lcom/osp/app/signin/TnCView;

    invoke-static {v1, v0}, Lcom/osp/app/signin/TnCView;->a(Lcom/osp/app/signin/TnCView;Landroid/widget/CheckBox;)Landroid/widget/CheckBox;

    .line 2798
    iget-object v1, p0, Lcom/osp/app/signin/op;->a:Lcom/osp/app/signin/TnCView;

    invoke-static {v1}, Lcom/osp/app/signin/TnCView;->k(Lcom/osp/app/signin/TnCView;)Z

    move-result v1

    if-eqz v1, :cond_d

    iget-object v1, p0, Lcom/osp/app/signin/op;->a:Lcom/osp/app/signin/TnCView;

    invoke-static {v1}, Lcom/osp/app/signin/TnCView;->l(Lcom/osp/app/signin/TnCView;)Z

    move-result v1

    if-eqz v1, :cond_d

    iget-object v1, p0, Lcom/osp/app/signin/op;->a:Lcom/osp/app/signin/TnCView;

    invoke-static {v1}, Lcom/osp/app/signin/TnCView;->m(Lcom/osp/app/signin/TnCView;)Z

    move-result v1

    if-eqz v1, :cond_d

    iget-object v1, p0, Lcom/osp/app/signin/op;->a:Lcom/osp/app/signin/TnCView;

    invoke-static {v1}, Lcom/osp/app/signin/TnCView;->i(Lcom/osp/app/signin/TnCView;)Z

    move-result v1

    if-eqz v1, :cond_c

    iget-object v1, p0, Lcom/osp/app/signin/op;->a:Lcom/osp/app/signin/TnCView;

    invoke-static {v1}, Lcom/osp/app/signin/TnCView;->a(Lcom/osp/app/signin/TnCView;)Z

    move-result v1

    if-eqz v1, :cond_d

    :cond_c
    :goto_2
    invoke-virtual {v0, v2}, Landroid/widget/CheckBox;->setChecked(Z)V

    .line 2800
    iget-object v1, p0, Lcom/osp/app/signin/op;->c:Landroid/widget/CompoundButton$OnCheckedChangeListener;

    invoke-virtual {v0, v1}, Landroid/widget/CheckBox;->setOnCheckedChangeListener(Landroid/widget/CompoundButton$OnCheckedChangeListener;)V

    goto/16 :goto_0

    :cond_d
    move v2, v3

    .line 2798
    goto :goto_2

    .line 2801
    :cond_e
    if-ne p2, v2, :cond_f

    .line 2803
    iget-object v1, p0, Lcom/osp/app/signin/op;->a:Lcom/osp/app/signin/TnCView;

    invoke-static {v1, v0}, Lcom/osp/app/signin/TnCView;->b(Lcom/osp/app/signin/TnCView;Landroid/widget/CheckBox;)Landroid/widget/CheckBox;

    .line 2804
    iget-object v1, p0, Lcom/osp/app/signin/op;->a:Lcom/osp/app/signin/TnCView;

    invoke-static {v1}, Lcom/osp/app/signin/TnCView;->k(Lcom/osp/app/signin/TnCView;)Z

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/CheckBox;->setChecked(Z)V

    .line 2805
    iget-object v1, p0, Lcom/osp/app/signin/op;->d:Landroid/widget/CompoundButton$OnCheckedChangeListener;

    invoke-virtual {v0, v1}, Landroid/widget/CheckBox;->setOnCheckedChangeListener(Landroid/widget/CompoundButton$OnCheckedChangeListener;)V

    goto/16 :goto_0

    .line 2806
    :cond_f
    if-ne p2, v7, :cond_10

    .line 2808
    iget-object v1, p0, Lcom/osp/app/signin/op;->a:Lcom/osp/app/signin/TnCView;

    invoke-static {v1, v0}, Lcom/osp/app/signin/TnCView;->c(Lcom/osp/app/signin/TnCView;Landroid/widget/CheckBox;)Landroid/widget/CheckBox;

    .line 2809
    iget-object v1, p0, Lcom/osp/app/signin/op;->a:Lcom/osp/app/signin/TnCView;

    invoke-static {v1}, Lcom/osp/app/signin/TnCView;->l(Lcom/osp/app/signin/TnCView;)Z

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/CheckBox;->setChecked(Z)V

    .line 2810
    iget-object v1, p0, Lcom/osp/app/signin/op;->e:Landroid/widget/CompoundButton$OnCheckedChangeListener;

    invoke-virtual {v0, v1}, Landroid/widget/CheckBox;->setOnCheckedChangeListener(Landroid/widget/CompoundButton$OnCheckedChangeListener;)V

    goto/16 :goto_0

    .line 2811
    :cond_10
    if-ne p2, v8, :cond_11

    .line 2813
    iget-object v1, p0, Lcom/osp/app/signin/op;->a:Lcom/osp/app/signin/TnCView;

    invoke-static {v1, v0}, Lcom/osp/app/signin/TnCView;->d(Lcom/osp/app/signin/TnCView;Landroid/widget/CheckBox;)Landroid/widget/CheckBox;

    .line 2814
    iget-object v1, p0, Lcom/osp/app/signin/op;->a:Lcom/osp/app/signin/TnCView;

    invoke-static {v1}, Lcom/osp/app/signin/TnCView;->m(Lcom/osp/app/signin/TnCView;)Z

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/CheckBox;->setChecked(Z)V

    .line 2815
    iget-object v1, p0, Lcom/osp/app/signin/op;->f:Landroid/widget/CompoundButton$OnCheckedChangeListener;

    invoke-virtual {v0, v1}, Landroid/widget/CheckBox;->setOnCheckedChangeListener(Landroid/widget/CompoundButton$OnCheckedChangeListener;)V

    goto/16 :goto_0

    .line 2818
    :cond_11
    iget-object v1, p0, Lcom/osp/app/signin/op;->a:Lcom/osp/app/signin/TnCView;

    invoke-static {v1, v0}, Lcom/osp/app/signin/TnCView;->f(Lcom/osp/app/signin/TnCView;Landroid/widget/CheckBox;)Landroid/widget/CheckBox;

    .line 2819
    iget-object v1, p0, Lcom/osp/app/signin/op;->a:Lcom/osp/app/signin/TnCView;

    invoke-static {v1}, Lcom/osp/app/signin/TnCView;->a(Lcom/osp/app/signin/TnCView;)Z

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/CheckBox;->setChecked(Z)V

    .line 2820
    iget-object v1, p0, Lcom/osp/app/signin/op;->h:Landroid/widget/CompoundButton$OnCheckedChangeListener;

    invoke-virtual {v0, v1}, Landroid/widget/CheckBox;->setOnCheckedChangeListener(Landroid/widget/CompoundButton$OnCheckedChangeListener;)V

    goto/16 :goto_0

    .line 2737
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_0
        :pswitch_2
    .end packed-switch
.end method

.method private a(Ljava/lang/String;Z)Landroid/view/View;
    .locals 5

    .prologue
    const/4 v4, 0x0

    const/4 v3, 0x0

    .line 2618
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "TnCView - makeNormalItem - str : "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;)V

    .line 2619
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "TnCView - makeNormalItem - hasDivider : "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;)V

    .line 2621
    iget-object v0, p0, Lcom/osp/app/signin/op;->a:Lcom/osp/app/signin/TnCView;

    invoke-static {v0}, Lcom/osp/app/signin/TnCView;->j(Lcom/osp/app/signin/TnCView;)Z

    move-result v0

    if-eqz v0, :cond_5

    iget-object v0, p0, Lcom/osp/app/signin/op;->a:Lcom/osp/app/signin/TnCView;

    invoke-static {v0}, Lcom/osp/app/util/r;->g(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_5

    .line 2625
    iget-object v0, p0, Lcom/osp/app/signin/op;->b:Landroid/view/LayoutInflater;

    const v1, 0x1090003

    invoke-virtual {v0, v1, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v1

    .line 2626
    const v0, 0x1020014

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 2632
    :goto_0
    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Landroid/view/View;->setEnabled(Z)V

    .line 2635
    if-eqz p1, :cond_0

    .line 2637
    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 2640
    :cond_0
    invoke-virtual {v0, v3, v3, v3, v3}, Landroid/widget/TextView;->setCompoundDrawablesWithIntrinsicBounds(Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;)V

    .line 2642
    iget-object v2, p0, Lcom/osp/app/signin/op;->a:Lcom/osp/app/signin/TnCView;

    invoke-static {v2}, Lcom/osp/app/signin/TnCView;->h(Lcom/osp/app/signin/TnCView;)I

    move-result v2

    if-eqz v2, :cond_1

    iget-object v2, p0, Lcom/osp/app/signin/op;->a:Lcom/osp/app/signin/TnCView;

    invoke-static {v2}, Lcom/osp/app/signin/TnCView;->h(Lcom/osp/app/signin/TnCView;)I

    move-result v2

    const/4 v3, 0x2

    if-ne v2, v3, :cond_2

    .line 2646
    :cond_1
    iget-object v2, p0, Lcom/osp/app/signin/op;->a:Lcom/osp/app/signin/TnCView;

    invoke-virtual {v2}, Lcom/osp/app/signin/TnCView;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    iget-object v3, p0, Lcom/osp/app/signin/op;->a:Lcom/osp/app/signin/TnCView;

    invoke-static {v3}, Lcom/osp/app/util/an;->a(Lcom/osp/app/util/BaseActivity;)Lcom/osp/app/util/an;

    const v3, 0x7f070004

    invoke-static {v3}, Lcom/osp/app/util/an;->a(I)I

    move-result v3

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getColor(I)I

    move-result v2

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setTextColor(I)V

    .line 2649
    iget-object v2, p0, Lcom/osp/app/signin/op;->a:Lcom/osp/app/signin/TnCView;

    invoke-static {v2}, Lcom/osp/app/util/an;->a(Lcom/osp/app/util/BaseActivity;)Lcom/osp/app/util/an;

    const v2, 0x7f08010f

    invoke-static {v2}, Lcom/osp/app/util/an;->b(I)I

    move-result v2

    .line 2650
    iget-object v3, p0, Lcom/osp/app/signin/op;->a:Lcom/osp/app/signin/TnCView;

    invoke-virtual {v3}, Lcom/osp/app/signin/TnCView;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    invoke-virtual {v3, v2}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v2

    invoke-virtual {v0, v4, v2}, Landroid/widget/TextView;->setTextSize(IF)V

    .line 2653
    :cond_2
    iget-object v0, p0, Lcom/osp/app/signin/op;->a:Lcom/osp/app/signin/TnCView;

    invoke-static {v0}, Lcom/osp/app/signin/TnCView;->j(Lcom/osp/app/signin/TnCView;)Z

    move-result v0

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/osp/app/signin/op;->a:Lcom/osp/app/signin/TnCView;

    invoke-static {v0}, Lcom/osp/app/util/r;->g(Landroid/content/Context;)Z

    move-result v0

    if-nez v0, :cond_4

    .line 2655
    :cond_3
    const v0, 0x7f0c00e9

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    .line 2658
    if-eqz v0, :cond_4

    .line 2660
    if-eqz p2, :cond_6

    .line 2662
    invoke-virtual {v0, v4}, Landroid/view/View;->setVisibility(I)V

    .line 2670
    :cond_4
    :goto_1
    return-object v1

    .line 2629
    :cond_5
    iget-object v0, p0, Lcom/osp/app/signin/op;->b:Landroid/view/LayoutInflater;

    const v1, 0x7f03005a

    invoke-virtual {v0, v1, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v1

    .line 2630
    const v0, 0x1020016

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    goto :goto_0

    .line 2665
    :cond_6
    const/4 v2, 0x4

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    goto :goto_1
.end method

.method private a(Landroid/widget/TextView;)V
    .locals 12

    .prologue
    const/4 v11, 0x1

    const/4 v4, 0x0

    .line 3217
    new-instance v5, Landroid/text/SpannableString;

    invoke-virtual {p1}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v0

    invoke-direct {v5, v0}, Landroid/text/SpannableString;-><init>(Ljava/lang/CharSequence;)V

    .line 3218
    invoke-interface {v5}, Landroid/text/Spannable;->length()I

    move-result v0

    const-class v1, Landroid/text/style/URLSpan;

    invoke-interface {v5, v4, v0, v1}, Landroid/text/Spannable;->getSpans(IILjava/lang/Class;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Landroid/text/style/URLSpan;

    .line 3220
    if-eqz v0, :cond_0

    array-length v1, v0

    if-gtz v1, :cond_1

    .line 3222
    :cond_0
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v0, "TNC"

    const-string v1, "no URL text."

    invoke-static {v0, v1}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 3334
    :goto_0
    return-void

    .line 3226
    :cond_1
    array-length v6, v0

    move v3, v4

    :goto_1
    if-ge v3, v6, :cond_9

    aget-object v1, v0, v3

    .line 3228
    invoke-interface {v5, v1}, Landroid/text/Spannable;->getSpanStart(Ljava/lang/Object;)I

    move-result v7

    .line 3229
    invoke-interface {v5, v1}, Landroid/text/Spannable;->getSpanEnd(Ljava/lang/Object;)I

    move-result v8

    .line 3230
    invoke-interface {v5, v1}, Landroid/text/Spannable;->removeSpan(Ljava/lang/Object;)V

    .line 3231
    const-string v2, ""

    .line 3232
    invoke-virtual {v1}, Landroid/text/style/URLSpan;->getURL()Ljava/lang/String;

    move-result-object v9

    const-string v10, "/getSpecialTermsList2.do"

    invoke-virtual {v9, v10}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v9

    if-eqz v9, :cond_2

    .line 3235
    new-instance v1, Lcom/osp/app/signin/TnCView$TnCAdapter$2;

    invoke-direct {v1, p0, v2}, Lcom/osp/app/signin/TnCView$TnCAdapter$2;-><init>(Lcom/osp/app/signin/op;Ljava/lang/String;)V

    .line 3246
    invoke-interface {v5, v1, v7, v8, v4}, Landroid/text/Spannable;->setSpan(Ljava/lang/Object;III)V

    .line 3226
    :goto_2
    add-int/lit8 v1, v3, 0x1

    move v3, v1

    goto :goto_1

    .line 3248
    :cond_2
    invoke-virtual {v1}, Landroid/text/style/URLSpan;->getURL()Ljava/lang/String;

    move-result-object v2

    const-string v9, "/general.html"

    invoke-virtual {v2, v9}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_3

    .line 3251
    iget-object v1, p0, Lcom/osp/app/signin/op;->a:Lcom/osp/app/signin/TnCView;

    invoke-static {v1}, Lcom/osp/app/signin/TnCView;->p(Lcom/osp/app/signin/TnCView;)Ljava/util/ArrayList;

    move-result-object v1

    invoke-virtual {v1, v4}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, [Ljava/lang/String;

    .line 3252
    const-string v2, ""

    .line 3253
    aget-object v9, v1, v4

    if-eqz v9, :cond_c

    aget-object v9, v1, v4

    invoke-virtual {v9}, Ljava/lang/String;->length()I

    move-result v9

    if-le v9, v11, :cond_c

    .line 3255
    aget-object v1, v1, v4

    invoke-virtual {v1, v11}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v1

    .line 3258
    :goto_3
    const-string v2, ".txt"

    const-string v9, ".html"

    invoke-virtual {v1, v2, v9}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v1

    .line 3259
    invoke-static {v1}, Lcom/msc/c/k;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 3309
    :goto_4
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v9, "url = "

    invoke-direct {v2, v9}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;)V

    .line 3311
    new-instance v2, Lcom/osp/app/signin/TnCView$TnCAdapter$3;

    invoke-direct {v2, p0, v1}, Lcom/osp/app/signin/TnCView$TnCAdapter$3;-><init>(Lcom/osp/app/signin/op;Ljava/lang/String;)V

    .line 3328
    invoke-interface {v5, v2, v7, v8, v4}, Landroid/text/Spannable;->setSpan(Ljava/lang/Object;III)V

    goto :goto_2

    .line 3260
    :cond_3
    invoke-virtual {v1}, Landroid/text/style/URLSpan;->getURL()Ljava/lang/String;

    move-result-object v2

    const-string v9, "/globalpp.html"

    invoke-virtual {v2, v9}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_4

    .line 3263
    iget-object v1, p0, Lcom/osp/app/signin/op;->a:Lcom/osp/app/signin/TnCView;

    invoke-static {v1}, Lcom/osp/app/signin/TnCView;->p(Lcom/osp/app/signin/TnCView;)Ljava/util/ArrayList;

    move-result-object v1

    invoke-virtual {v1, v4}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, [Ljava/lang/String;

    .line 3264
    const-string v2, ""

    .line 3265
    aget-object v9, v1, v4

    if-eqz v9, :cond_b

    aget-object v9, v1, v4

    invoke-virtual {v9}, Ljava/lang/String;->length()I

    move-result v9

    if-le v9, v11, :cond_b

    .line 3267
    aget-object v1, v1, v4

    invoke-virtual {v1, v11}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v1

    .line 3270
    :goto_5
    const-string v2, "general.txt"

    const-string v9, "globalpp.html"

    invoke-virtual {v1, v2, v9}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v1

    .line 3272
    invoke-static {v1}, Lcom/msc/c/k;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    goto :goto_4

    .line 3273
    :cond_4
    invoke-virtual {v1}, Landroid/text/style/URLSpan;->getURL()Ljava/lang/String;

    move-result-object v2

    const-string v9, "/DataCombinationContentOAuth2.do"

    invoke-virtual {v2, v9}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_7

    .line 3276
    iget-object v1, p0, Lcom/osp/app/signin/op;->a:Lcom/osp/app/signin/TnCView;

    iget-object v1, p0, Lcom/osp/app/signin/op;->a:Lcom/osp/app/signin/TnCView;

    invoke-static {v1}, Lcom/osp/app/signin/TnCView;->q(Lcom/osp/app/signin/TnCView;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/osp/common/util/i;->d(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 3277
    if-nez v1, :cond_5

    .line 3279
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v1, "TNC"

    const-string v2, "showDataCombination - country is null"

    invoke-static {v1, v2}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 3280
    const-string v1, "GBR"

    .line 3283
    :cond_5
    invoke-static {}, Lcom/osp/common/util/i;->a()Lcom/osp/common/util/i;

    iget-object v2, p0, Lcom/osp/app/signin/op;->a:Lcom/osp/app/signin/TnCView;

    invoke-static {v2}, Lcom/osp/common/util/i;->n(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v2

    .line 3284
    if-nez v2, :cond_6

    .line 3286
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v2, "TNC"

    const-string v9, "showDataCombination - language is null"

    invoke-static {v2, v9}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 3287
    const-string v2, "eng"

    .line 3290
    :cond_6
    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v10, p0, Lcom/osp/app/signin/op;->a:Lcom/osp/app/signin/TnCView;

    invoke-static {v10}, Lcom/msc/c/n;->d(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v10, "?countryCode="

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v9, "&languageCode="

    invoke-virtual {v1, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    goto/16 :goto_4

    .line 3291
    :cond_7
    invoke-virtual {v1}, Landroid/text/style/URLSpan;->getURL()Ljava/lang/String;

    move-result-object v2

    const-string v9, "/general_esu.html"

    invoke-virtual {v2, v9}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_8

    .line 3294
    iget-object v1, p0, Lcom/osp/app/signin/op;->a:Lcom/osp/app/signin/TnCView;

    invoke-static {v1}, Lcom/osp/app/signin/TnCView;->p(Lcom/osp/app/signin/TnCView;)Ljava/util/ArrayList;

    move-result-object v1

    invoke-virtual {v1, v4}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, [Ljava/lang/String;

    .line 3295
    const-string v2, ""

    .line 3296
    aget-object v9, v1, v4

    if-eqz v9, :cond_a

    aget-object v9, v1, v4

    invoke-virtual {v9}, Ljava/lang/String;->length()I

    move-result v9

    if-le v9, v11, :cond_a

    .line 3298
    aget-object v1, v1, v4

    invoke-virtual {v1, v11}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v1

    .line 3301
    :goto_6
    const-string v2, "general.txt"

    const-string v9, "general_esu.html"

    invoke-virtual {v1, v2, v9}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v1

    .line 3303
    invoke-static {v1}, Lcom/msc/c/k;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    goto/16 :goto_4

    .line 3307
    :cond_8
    invoke-virtual {v1}, Landroid/text/style/URLSpan;->getURL()Ljava/lang/String;

    move-result-object v1

    goto/16 :goto_4

    .line 3331
    :cond_9
    invoke-virtual {p1, v5}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 3332
    const-string v0, " "

    invoke-virtual {p1, v0}, Landroid/widget/TextView;->append(Ljava/lang/CharSequence;)V

    .line 3333
    invoke-static {}, Landroid/text/method/LinkMovementMethod;->getInstance()Landroid/text/method/MovementMethod;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/widget/TextView;->setMovementMethod(Landroid/text/method/MovementMethod;)V

    goto/16 :goto_0

    :cond_a
    move-object v1, v2

    goto :goto_6

    :cond_b
    move-object v1, v2

    goto/16 :goto_5

    :cond_c
    move-object v1, v2

    goto/16 :goto_3
.end method

.method static synthetic a(Lcom/osp/app/signin/op;)V
    .locals 5

    .prologue
    const/4 v4, 0x1

    const/4 v3, 0x0

    const/4 v2, 0x0

    .line 2449
    iget-object v0, p0, Lcom/osp/app/signin/op;->a:Lcom/osp/app/signin/TnCView;

    invoke-static {v0}, Lcom/osp/app/signin/TnCView;->x(Lcom/osp/app/signin/TnCView;)Landroid/widget/CheckBox;

    move-result-object v0

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/osp/app/signin/op;->a:Lcom/osp/app/signin/TnCView;

    invoke-static {v0}, Lcom/osp/app/signin/TnCView;->h(Lcom/osp/app/signin/TnCView;)I

    move-result v0

    if-nez v0, :cond_4

    iget-object v0, p0, Lcom/osp/app/signin/op;->a:Lcom/osp/app/signin/TnCView;

    invoke-static {v0}, Lcom/osp/app/signin/TnCView;->k(Lcom/osp/app/signin/TnCView;)Z

    move-result v0

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/osp/app/signin/op;->a:Lcom/osp/app/signin/TnCView;

    invoke-static {v0}, Lcom/osp/app/signin/TnCView;->l(Lcom/osp/app/signin/TnCView;)Z

    move-result v0

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/osp/app/signin/op;->a:Lcom/osp/app/signin/TnCView;

    invoke-static {v0}, Lcom/osp/app/signin/TnCView;->m(Lcom/osp/app/signin/TnCView;)Z

    move-result v0

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/osp/app/signin/op;->a:Lcom/osp/app/signin/TnCView;

    invoke-static {v0}, Lcom/osp/app/util/r;->b(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/osp/app/signin/op;->a:Lcom/osp/app/signin/TnCView;

    invoke-static {v0}, Lcom/osp/app/signin/TnCView;->n(Lcom/osp/app/signin/TnCView;)Z

    move-result v0

    if-eqz v0, :cond_3

    :cond_0
    iget-object v0, p0, Lcom/osp/app/signin/op;->a:Lcom/osp/app/signin/TnCView;

    invoke-static {v0}, Lcom/osp/app/signin/TnCView;->i(Lcom/osp/app/signin/TnCView;)Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/osp/app/signin/op;->a:Lcom/osp/app/signin/TnCView;

    invoke-static {v0}, Lcom/osp/app/signin/TnCView;->a(Lcom/osp/app/signin/TnCView;)Z

    move-result v0

    if-eqz v0, :cond_3

    :cond_1
    iget-object v0, p0, Lcom/osp/app/signin/op;->a:Lcom/osp/app/signin/TnCView;

    invoke-static {v0}, Lcom/osp/app/signin/TnCView;->x(Lcom/osp/app/signin/TnCView;)Landroid/widget/CheckBox;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/CheckBox;->isChecked()Z

    move-result v0

    if-nez v0, :cond_2

    iget-object v0, p0, Lcom/osp/app/signin/op;->a:Lcom/osp/app/signin/TnCView;

    invoke-static {v0}, Lcom/osp/app/signin/TnCView;->x(Lcom/osp/app/signin/TnCView;)Landroid/widget/CheckBox;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/widget/CheckBox;->setOnCheckedChangeListener(Landroid/widget/CompoundButton$OnCheckedChangeListener;)V

    iget-object v0, p0, Lcom/osp/app/signin/op;->a:Lcom/osp/app/signin/TnCView;

    invoke-static {v0}, Lcom/osp/app/signin/TnCView;->x(Lcom/osp/app/signin/TnCView;)Landroid/widget/CheckBox;

    move-result-object v0

    invoke-virtual {v0, v4}, Landroid/widget/CheckBox;->setChecked(Z)V

    iget-object v0, p0, Lcom/osp/app/signin/op;->a:Lcom/osp/app/signin/TnCView;

    invoke-static {v0}, Lcom/osp/app/signin/TnCView;->x(Lcom/osp/app/signin/TnCView;)Landroid/widget/CheckBox;

    move-result-object v0

    iget-object v1, p0, Lcom/osp/app/signin/op;->c:Landroid/widget/CompoundButton$OnCheckedChangeListener;

    invoke-virtual {v0, v1}, Landroid/widget/CheckBox;->setOnCheckedChangeListener(Landroid/widget/CompoundButton$OnCheckedChangeListener;)V

    :cond_2
    :goto_0
    return-void

    :cond_3
    iget-object v0, p0, Lcom/osp/app/signin/op;->a:Lcom/osp/app/signin/TnCView;

    invoke-static {v0}, Lcom/osp/app/signin/TnCView;->x(Lcom/osp/app/signin/TnCView;)Landroid/widget/CheckBox;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/CheckBox;->isChecked()Z

    move-result v0

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/osp/app/signin/op;->a:Lcom/osp/app/signin/TnCView;

    invoke-static {v0}, Lcom/osp/app/signin/TnCView;->x(Lcom/osp/app/signin/TnCView;)Landroid/widget/CheckBox;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/widget/CheckBox;->setOnCheckedChangeListener(Landroid/widget/CompoundButton$OnCheckedChangeListener;)V

    iget-object v0, p0, Lcom/osp/app/signin/op;->a:Lcom/osp/app/signin/TnCView;

    invoke-static {v0}, Lcom/osp/app/signin/TnCView;->x(Lcom/osp/app/signin/TnCView;)Landroid/widget/CheckBox;

    move-result-object v0

    invoke-virtual {v0, v3}, Landroid/widget/CheckBox;->setChecked(Z)V

    iget-object v0, p0, Lcom/osp/app/signin/op;->a:Lcom/osp/app/signin/TnCView;

    invoke-static {v0}, Lcom/osp/app/signin/TnCView;->x(Lcom/osp/app/signin/TnCView;)Landroid/widget/CheckBox;

    move-result-object v0

    iget-object v1, p0, Lcom/osp/app/signin/op;->c:Landroid/widget/CompoundButton$OnCheckedChangeListener;

    invoke-virtual {v0, v1}, Landroid/widget/CheckBox;->setOnCheckedChangeListener(Landroid/widget/CompoundButton$OnCheckedChangeListener;)V

    goto :goto_0

    :cond_4
    iget-object v0, p0, Lcom/osp/app/signin/op;->a:Lcom/osp/app/signin/TnCView;

    invoke-static {v0}, Lcom/osp/app/signin/TnCView;->h(Lcom/osp/app/signin/TnCView;)I

    move-result v0

    const/4 v1, 0x2

    if-ne v0, v1, :cond_2

    iget-object v0, p0, Lcom/osp/app/signin/op;->a:Lcom/osp/app/signin/TnCView;

    invoke-static {v0}, Lcom/osp/app/signin/TnCView;->k(Lcom/osp/app/signin/TnCView;)Z

    move-result v0

    if-eqz v0, :cond_6

    iget-object v0, p0, Lcom/osp/app/signin/op;->a:Lcom/osp/app/signin/TnCView;

    invoke-static {v0}, Lcom/osp/app/signin/TnCView;->l(Lcom/osp/app/signin/TnCView;)Z

    move-result v0

    if-eqz v0, :cond_6

    iget-object v0, p0, Lcom/osp/app/signin/op;->a:Lcom/osp/app/signin/TnCView;

    invoke-static {v0}, Lcom/osp/app/signin/TnCView;->m(Lcom/osp/app/signin/TnCView;)Z

    move-result v0

    if-eqz v0, :cond_6

    iget-object v0, p0, Lcom/osp/app/signin/op;->a:Lcom/osp/app/signin/TnCView;

    invoke-static {v0}, Lcom/osp/app/signin/TnCView;->i(Lcom/osp/app/signin/TnCView;)Z

    move-result v0

    if-eqz v0, :cond_5

    iget-object v0, p0, Lcom/osp/app/signin/op;->a:Lcom/osp/app/signin/TnCView;

    invoke-static {v0}, Lcom/osp/app/signin/TnCView;->a(Lcom/osp/app/signin/TnCView;)Z

    move-result v0

    if-eqz v0, :cond_6

    :cond_5
    iget-object v0, p0, Lcom/osp/app/signin/op;->a:Lcom/osp/app/signin/TnCView;

    invoke-static {v0}, Lcom/osp/app/signin/TnCView;->x(Lcom/osp/app/signin/TnCView;)Landroid/widget/CheckBox;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/CheckBox;->isChecked()Z

    move-result v0

    if-nez v0, :cond_2

    iget-object v0, p0, Lcom/osp/app/signin/op;->a:Lcom/osp/app/signin/TnCView;

    invoke-static {v0}, Lcom/osp/app/signin/TnCView;->x(Lcom/osp/app/signin/TnCView;)Landroid/widget/CheckBox;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/widget/CheckBox;->setOnCheckedChangeListener(Landroid/widget/CompoundButton$OnCheckedChangeListener;)V

    iget-object v0, p0, Lcom/osp/app/signin/op;->a:Lcom/osp/app/signin/TnCView;

    invoke-static {v0}, Lcom/osp/app/signin/TnCView;->x(Lcom/osp/app/signin/TnCView;)Landroid/widget/CheckBox;

    move-result-object v0

    invoke-virtual {v0, v4}, Landroid/widget/CheckBox;->setChecked(Z)V

    iget-object v0, p0, Lcom/osp/app/signin/op;->a:Lcom/osp/app/signin/TnCView;

    invoke-static {v0}, Lcom/osp/app/signin/TnCView;->x(Lcom/osp/app/signin/TnCView;)Landroid/widget/CheckBox;

    move-result-object v0

    iget-object v1, p0, Lcom/osp/app/signin/op;->c:Landroid/widget/CompoundButton$OnCheckedChangeListener;

    invoke-virtual {v0, v1}, Landroid/widget/CheckBox;->setOnCheckedChangeListener(Landroid/widget/CompoundButton$OnCheckedChangeListener;)V

    goto/16 :goto_0

    :cond_6
    iget-object v0, p0, Lcom/osp/app/signin/op;->a:Lcom/osp/app/signin/TnCView;

    invoke-static {v0}, Lcom/osp/app/signin/TnCView;->x(Lcom/osp/app/signin/TnCView;)Landroid/widget/CheckBox;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/CheckBox;->isChecked()Z

    move-result v0

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/osp/app/signin/op;->a:Lcom/osp/app/signin/TnCView;

    invoke-static {v0}, Lcom/osp/app/signin/TnCView;->x(Lcom/osp/app/signin/TnCView;)Landroid/widget/CheckBox;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/widget/CheckBox;->setOnCheckedChangeListener(Landroid/widget/CompoundButton$OnCheckedChangeListener;)V

    iget-object v0, p0, Lcom/osp/app/signin/op;->a:Lcom/osp/app/signin/TnCView;

    invoke-static {v0}, Lcom/osp/app/signin/TnCView;->x(Lcom/osp/app/signin/TnCView;)Landroid/widget/CheckBox;

    move-result-object v0

    invoke-virtual {v0, v3}, Landroid/widget/CheckBox;->setChecked(Z)V

    iget-object v0, p0, Lcom/osp/app/signin/op;->a:Lcom/osp/app/signin/TnCView;

    invoke-static {v0}, Lcom/osp/app/signin/TnCView;->x(Lcom/osp/app/signin/TnCView;)Landroid/widget/CheckBox;

    move-result-object v0

    iget-object v1, p0, Lcom/osp/app/signin/op;->c:Landroid/widget/CompoundButton$OnCheckedChangeListener;

    invoke-virtual {v0, v1}, Landroid/widget/CheckBox;->setOnCheckedChangeListener(Landroid/widget/CompoundButton$OnCheckedChangeListener;)V

    goto/16 :goto_0
.end method


# virtual methods
.method public final getCount()I
    .locals 4

    .prologue
    const/4 v0, 0x5

    const/4 v1, 0x3

    .line 2461
    const/4 v2, 0x0

    .line 2462
    iget-object v3, p0, Lcom/osp/app/signin/op;->a:Lcom/osp/app/signin/TnCView;

    invoke-static {v3}, Lcom/osp/app/signin/TnCView;->h(Lcom/osp/app/signin/TnCView;)I

    move-result v3

    packed-switch v3, :pswitch_data_0

    move v0, v2

    .line 2515
    :cond_0
    :goto_0
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "TnCView - getCount - mTnCListType : "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, Lcom/osp/app/signin/op;->a:Lcom/osp/app/signin/TnCView;

    invoke-static {v2}, Lcom/osp/app/signin/TnCView;->h(Lcom/osp/app/signin/TnCView;)I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;)V

    .line 2541
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "TnCView - getCount - count : "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;)V

    .line 2543
    return v0

    .line 2471
    :pswitch_0
    iget-object v1, p0, Lcom/osp/app/signin/op;->a:Lcom/osp/app/signin/TnCView;

    invoke-static {v1}, Lcom/osp/app/util/r;->b(Landroid/content/Context;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 2473
    const/4 v0, 0x6

    .line 2475
    :cond_1
    iget-object v1, p0, Lcom/osp/app/signin/op;->a:Lcom/osp/app/signin/TnCView;

    invoke-static {v1}, Lcom/osp/app/signin/TnCView;->i(Lcom/osp/app/signin/TnCView;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 2477
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :pswitch_1
    move v0, v1

    .line 2492
    goto :goto_0

    .line 2494
    :pswitch_2
    const/4 v1, 0x4

    .line 2497
    iget-object v2, p0, Lcom/osp/app/signin/op;->a:Lcom/osp/app/signin/TnCView;

    invoke-static {v2}, Lcom/osp/app/signin/TnCView;->i(Lcom/osp/app/signin/TnCView;)Z

    move-result v2

    if-nez v2, :cond_0

    move v0, v1

    goto :goto_0

    :pswitch_3
    move v0, v1

    .line 2509
    goto :goto_0

    .line 2462
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method public final getItem(I)Ljava/lang/Object;
    .locals 2

    .prologue
    .line 2548
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "TnCView - getItem - position : "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;)V

    .line 2549
    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    return-object v0
.end method

.method public final getItemId(I)J
    .locals 2

    .prologue
    .line 2554
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "TnCView - getItemId - position : "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;)V

    .line 2555
    int-to-long v0, p1

    return-wide v0
.end method

.method public final getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 7

    .prologue
    const v6, 0x7f090068

    const v5, 0x7f090059

    const v4, 0x7f090193

    const/4 v1, 0x0

    const/4 v3, 0x1

    .line 2566
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v2, "TnCView - getView - position : "

    invoke-direct {v0, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;)V

    .line 2567
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v0, "getView()"

    invoke-static {v0}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;)V

    .line 2569
    iget-object v0, p0, Lcom/osp/app/signin/op;->a:Lcom/osp/app/signin/TnCView;

    invoke-static {v0}, Lcom/osp/app/signin/TnCView;->h(Lcom/osp/app/signin/TnCView;)I

    move-result v0

    packed-switch v0, :pswitch_data_0

    move-object v0, p2

    .line 2582
    :goto_0
    if-nez v0, :cond_0

    .line 2589
    const/4 v0, 0x0

    invoke-direct {p0, v1, v0}, Lcom/osp/app/signin/op;->a(Ljava/lang/String;Z)Landroid/view/View;

    move-result-object v0

    .line 2592
    :cond_0
    return-object v0

    .line 2572
    :pswitch_0
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v2, "TnCView - getDefaultListItem - position : "

    invoke-direct {v0, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;)V

    packed-switch p1, :pswitch_data_1

    :goto_1
    move-object v0, v1

    goto :goto_0

    :pswitch_1
    iget-object v0, p0, Lcom/osp/app/signin/op;->a:Lcom/osp/app/signin/TnCView;

    const v2, 0x7f09018f

    invoke-virtual {v0, v2}, Lcom/osp/app/signin/TnCView;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0, v3}, Lcom/osp/app/signin/op;->a(Ljava/lang/String;Z)Landroid/view/View;

    move-result-object v0

    goto :goto_0

    :pswitch_2
    const v0, 0x7f0901ab

    invoke-direct {p0, v0, p1}, Lcom/osp/app/signin/op;->a(II)Landroid/view/View;

    move-result-object v0

    goto :goto_0

    :pswitch_3
    const v0, 0x7f090190

    invoke-direct {p0, v0, p1}, Lcom/osp/app/signin/op;->a(II)Landroid/view/View;

    move-result-object v0

    goto :goto_0

    :pswitch_4
    const v0, 0x7f090191

    invoke-direct {p0, v0, p1}, Lcom/osp/app/signin/op;->a(II)Landroid/view/View;

    move-result-object v0

    goto :goto_0

    :pswitch_5
    const v0, 0x7f090192

    invoke-direct {p0, v0, p1}, Lcom/osp/app/signin/op;->a(II)Landroid/view/View;

    move-result-object v0

    goto :goto_0

    :pswitch_6
    iget-object v0, p0, Lcom/osp/app/signin/op;->a:Lcom/osp/app/signin/TnCView;

    invoke-static {v0}, Lcom/osp/app/util/r;->b(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_1

    const v0, 0x7f090194

    invoke-direct {p0, v0, p1}, Lcom/osp/app/signin/op;->a(II)Landroid/view/View;

    move-result-object v0

    goto :goto_0

    :cond_1
    invoke-direct {p0, v4, p1}, Lcom/osp/app/signin/op;->a(II)Landroid/view/View;

    move-result-object v0

    goto :goto_0

    :pswitch_7
    invoke-direct {p0, v4, p1}, Lcom/osp/app/signin/op;->a(II)Landroid/view/View;

    move-result-object v0

    goto :goto_0

    .line 2575
    :pswitch_8
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v2, "TnCView - getDefaultNobuttonListItem - position : "

    invoke-direct {v0, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;)V

    packed-switch p1, :pswitch_data_2

    goto :goto_1

    :pswitch_9
    iget-object v0, p0, Lcom/osp/app/signin/op;->a:Lcom/osp/app/signin/TnCView;

    invoke-virtual {v0, v5}, Lcom/osp/app/signin/TnCView;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0, v3}, Lcom/osp/app/signin/op;->a(Ljava/lang/String;Z)Landroid/view/View;

    move-result-object v0

    goto/16 :goto_0

    :pswitch_a
    iget-object v0, p0, Lcom/osp/app/signin/op;->a:Lcom/osp/app/signin/TnCView;

    const v2, 0x7f0900bd

    invoke-virtual {v0, v2}, Lcom/osp/app/signin/TnCView;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0, v3}, Lcom/osp/app/signin/op;->a(Ljava/lang/String;Z)Landroid/view/View;

    move-result-object v0

    goto/16 :goto_0

    :pswitch_b
    iget-object v0, p0, Lcom/osp/app/signin/op;->a:Lcom/osp/app/signin/TnCView;

    invoke-virtual {v0, v6}, Lcom/osp/app/signin/TnCView;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0, v3}, Lcom/osp/app/signin/op;->a(Ljava/lang/String;Z)Landroid/view/View;

    move-result-object v0

    goto/16 :goto_0

    .line 2578
    :pswitch_c
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v2, "TnCView - getKorListItem - position : "

    invoke-direct {v0, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;)V

    packed-switch p1, :pswitch_data_3

    goto/16 :goto_1

    :pswitch_d
    const v0, 0x7f0901ab

    invoke-direct {p0, v0, p1}, Lcom/osp/app/signin/op;->a(II)Landroid/view/View;

    move-result-object v0

    goto/16 :goto_0

    :pswitch_e
    const v0, 0x7f090190

    invoke-direct {p0, v0, p1}, Lcom/osp/app/signin/op;->a(II)Landroid/view/View;

    move-result-object v0

    goto/16 :goto_0

    :pswitch_f
    const v0, 0x7f090195

    invoke-direct {p0, v0, p1}, Lcom/osp/app/signin/op;->a(II)Landroid/view/View;

    move-result-object v0

    goto/16 :goto_0

    :pswitch_10
    const v0, 0x7f090192

    invoke-direct {p0, v0, p1}, Lcom/osp/app/signin/op;->a(II)Landroid/view/View;

    move-result-object v0

    goto/16 :goto_0

    :pswitch_11
    invoke-direct {p0, v4, p1}, Lcom/osp/app/signin/op;->a(II)Landroid/view/View;

    move-result-object v0

    goto/16 :goto_0

    .line 2581
    :pswitch_12
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v2, "TnCView - getKorNobuttonListItem - position : "

    invoke-direct {v0, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;)V

    packed-switch p1, :pswitch_data_4

    goto/16 :goto_1

    :pswitch_13
    iget-object v0, p0, Lcom/osp/app/signin/op;->a:Lcom/osp/app/signin/TnCView;

    invoke-virtual {v0, v5}, Lcom/osp/app/signin/TnCView;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0, v3}, Lcom/osp/app/signin/op;->a(Ljava/lang/String;Z)Landroid/view/View;

    move-result-object v0

    goto/16 :goto_0

    :pswitch_14
    iget-object v0, p0, Lcom/osp/app/signin/op;->a:Lcom/osp/app/signin/TnCView;

    const v2, 0x7f0900bd

    invoke-virtual {v0, v2}, Lcom/osp/app/signin/TnCView;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0, v3}, Lcom/osp/app/signin/op;->a(Ljava/lang/String;Z)Landroid/view/View;

    move-result-object v0

    goto/16 :goto_0

    :pswitch_15
    iget-object v0, p0, Lcom/osp/app/signin/op;->a:Lcom/osp/app/signin/TnCView;

    invoke-virtual {v0, v6}, Lcom/osp/app/signin/TnCView;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0, v3}, Lcom/osp/app/signin/op;->a(Ljava/lang/String;Z)Landroid/view/View;

    move-result-object v0

    goto/16 :goto_0

    .line 2569
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_8
        :pswitch_c
        :pswitch_12
    .end packed-switch

    .line 2572
    :pswitch_data_1
    .packed-switch 0x0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_7
    .end packed-switch

    .line 2575
    :pswitch_data_2
    .packed-switch 0x0
        :pswitch_9
        :pswitch_a
        :pswitch_b
    .end packed-switch

    .line 2578
    :pswitch_data_3
    .packed-switch 0x0
        :pswitch_d
        :pswitch_e
        :pswitch_f
        :pswitch_10
        :pswitch_11
    .end packed-switch

    .line 2581
    :pswitch_data_4
    .packed-switch 0x0
        :pswitch_13
        :pswitch_14
        :pswitch_15
    .end packed-switch
.end method

.method public final isEnabled(I)Z
    .locals 2

    .prologue
    .line 2598
    iget-object v0, p0, Lcom/osp/app/signin/op;->a:Lcom/osp/app/signin/TnCView;

    invoke-static {v0}, Lcom/osp/app/signin/TnCView;->h(Lcom/osp/app/signin/TnCView;)I

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/osp/app/signin/op;->a:Lcom/osp/app/signin/TnCView;

    invoke-static {v0}, Lcom/osp/app/signin/TnCView;->h(Lcom/osp/app/signin/TnCView;)I

    move-result v0

    const/4 v1, 0x2

    if-ne v0, v1, :cond_1

    .line 2600
    :cond_0
    const/4 v0, 0x0

    .line 2603
    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x1

    goto :goto_0
.end method
