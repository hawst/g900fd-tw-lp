.class final Lcom/osp/app/signin/cp;
.super Ljava/lang/Object;
.source "BackgroundModeService.java"

# interfaces
.implements Lcom/msc/b/h;


# instance fields
.field final synthetic a:Landroid/os/Bundle;

.field final synthetic b:Lcom/msc/c/f;

.field final synthetic c:Lcom/osp/app/signin/BackgroundModeService;


# direct methods
.method constructor <init>(Lcom/osp/app/signin/BackgroundModeService;Landroid/os/Bundle;Lcom/msc/c/f;)V
    .locals 0

    .prologue
    .line 432
    iput-object p1, p0, Lcom/osp/app/signin/cp;->c:Lcom/osp/app/signin/BackgroundModeService;

    iput-object p2, p0, Lcom/osp/app/signin/cp;->a:Landroid/os/Bundle;

    iput-object p3, p0, Lcom/osp/app/signin/cp;->b:Lcom/msc/c/f;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Lcom/msc/b/c;)V
    .locals 3

    .prologue
    .line 437
    if-nez p1, :cond_1

    .line 460
    :cond_0
    :goto_0
    return-void

    .line 442
    :cond_1
    invoke-virtual {p1}, Lcom/msc/b/c;->e()Ljava/lang/String;

    move-result-object v0

    .line 444
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "onSuccess() - "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;)V

    .line 448
    :try_start_0
    invoke-static {}, Lcom/msc/c/h;->a()Lcom/msc/c/h;

    invoke-static {v0}, Lcom/msc/c/h;->s(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 451
    iget-object v0, p0, Lcom/osp/app/signin/cp;->a:Landroid/os/Bundle;

    const-string v1, "result"

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 453
    :catch_0
    move-exception v0

    .line 455
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    .line 457
    iget-object v1, p0, Lcom/osp/app/signin/cp;->b:Lcom/msc/c/f;

    invoke-virtual {v0}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Lcom/msc/c/f;->b(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public final b(Lcom/msc/b/c;)V
    .locals 4

    .prologue
    .line 465
    if-nez p1, :cond_1

    .line 510
    :cond_0
    :goto_0
    return-void

    .line 470
    :cond_1
    invoke-virtual {p1}, Lcom/msc/b/c;->b()Ljava/lang/String;

    .line 471
    invoke-virtual {p1}, Lcom/msc/b/c;->e()Ljava/lang/String;

    move-result-object v0

    .line 472
    invoke-virtual {p1}, Lcom/msc/b/c;->f()Ljava/lang/Exception;

    move-result-object v1

    .line 474
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "onFail() - "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;)V

    .line 476
    if-eqz v1, :cond_3

    .line 478
    new-instance v0, Lcom/msc/c/f;

    invoke-direct {v0, v1}, Lcom/msc/c/f;-><init>(Ljava/lang/Exception;)V

    .line 485
    invoke-virtual {v0}, Lcom/msc/c/f;->d()Z

    move-result v1

    if-eqz v1, :cond_2

    .line 487
    iget-object v1, p0, Lcom/osp/app/signin/cp;->c:Lcom/osp/app/signin/BackgroundModeService;

    const v2, 0x7f090027

    invoke-virtual {v1, v2}, Lcom/osp/app/signin/BackgroundModeService;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/msc/c/f;->b(Ljava/lang/String;)V

    .line 488
    const-string v1, "NPC"

    invoke-virtual {v0, v1}, Lcom/msc/c/f;->a(Ljava/lang/String;)V

    .line 491
    :cond_2
    iget-object v1, p0, Lcom/osp/app/signin/cp;->b:Lcom/msc/c/f;

    invoke-virtual {v0}, Lcom/msc/c/f;->a()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/msc/c/f;->a(Ljava/lang/String;)V

    .line 492
    iget-object v1, p0, Lcom/osp/app/signin/cp;->b:Lcom/msc/c/f;

    invoke-virtual {v0}, Lcom/msc/c/f;->b()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Lcom/msc/c/f;->b(Ljava/lang/String;)V

    goto :goto_0

    .line 494
    :cond_3
    if-eqz v0, :cond_0

    .line 496
    iget-object v1, p0, Lcom/osp/app/signin/cp;->b:Lcom/msc/c/f;

    invoke-virtual {v1, v0}, Lcom/msc/c/f;->c(Ljava/lang/String;)V

    .line 498
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v0, "BMS"

    const-string v1, "Server request error occured"

    invoke-static {v0, v1}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public final c(Lcom/msc/b/c;)V
    .locals 0

    .prologue
    .line 515
    return-void
.end method
