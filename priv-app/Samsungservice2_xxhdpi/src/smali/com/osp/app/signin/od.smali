.class final Lcom/osp/app/signin/od;
.super Lcom/msc/c/b;
.source "TnCView.java"


# instance fields
.field final synthetic c:Lcom/osp/app/signin/TnCView;

.field private d:J

.field private e:Ljava/lang/String;

.field private f:Ljava/lang/String;

.field private g:Ljava/lang/String;


# direct methods
.method public constructor <init>(Lcom/osp/app/signin/TnCView;)V
    .locals 2

    .prologue
    .line 5211
    iput-object p1, p0, Lcom/osp/app/signin/od;->c:Lcom/osp/app/signin/TnCView;

    .line 5212
    invoke-direct {p0, p1}, Lcom/msc/c/b;-><init>(Landroid/content/Context;)V

    .line 5214
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v0, "TNC"

    const-string v1, "GetSpecialTermsListTask"

    invoke-static {v0, v1}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 5221
    invoke-static {p1}, Lcom/osp/app/signin/TnCView;->q(Lcom/osp/app/signin/TnCView;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/osp/common/util/i;->d(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/osp/app/signin/od;->f:Ljava/lang/String;

    .line 5222
    iget-object v0, p0, Lcom/osp/app/signin/od;->f:Ljava/lang/String;

    if-nez v0, :cond_0

    .line 5224
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v0, "TNC"

    const-string v1, "GetSpecialTermsListTask - country is null"

    invoke-static {v0, v1}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 5227
    const-string v0, ""

    iput-object v0, p0, Lcom/osp/app/signin/od;->f:Ljava/lang/String;

    .line 5230
    :cond_0
    invoke-static {}, Lcom/osp/common/util/i;->a()Lcom/osp/common/util/i;

    invoke-static {p1}, Lcom/osp/common/util/i;->n(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/osp/app/signin/od;->g:Ljava/lang/String;

    .line 5231
    iget-object v0, p0, Lcom/osp/app/signin/od;->g:Ljava/lang/String;

    if-nez v0, :cond_1

    .line 5233
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v0, "TNC"

    const-string v1, "GetSpecialTermsListTask - language is null"

    invoke-static {v0, v1}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 5234
    const-string v0, ""

    iput-object v0, p0, Lcom/osp/app/signin/od;->g:Ljava/lang/String;

    .line 5239
    :cond_1
    return-void
.end method


# virtual methods
.method protected final varargs a()Ljava/lang/Boolean;
    .locals 3

    .prologue
    .line 5270
    iget-object v0, p0, Lcom/osp/app/signin/od;->c:Lcom/osp/app/signin/TnCView;

    invoke-static {v0}, Lcom/osp/app/signin/TnCView;->Q(Lcom/osp/app/signin/TnCView;)V

    .line 5274
    invoke-static {}, Lcom/msc/c/g;->a()Lcom/msc/c/g;

    iget-object v0, p0, Lcom/osp/app/signin/od;->c:Lcom/osp/app/signin/TnCView;

    iget-object v1, p0, Lcom/osp/app/signin/od;->f:Ljava/lang/String;

    iget-object v2, p0, Lcom/osp/app/signin/od;->g:Ljava/lang/String;

    invoke-static {v0, v1, v2, p0}, Lcom/msc/c/g;->g(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Lcom/msc/b/h;)J

    move-result-wide v0

    iput-wide v0, p0, Lcom/osp/app/signin/od;->d:J

    iget-wide v0, p0, Lcom/osp/app/signin/od;->d:J

    invoke-virtual {p0, v0, v1}, Lcom/osp/app/signin/od;->a(J)V

    iget-wide v0, p0, Lcom/osp/app/signin/od;->d:J

    sget-object v2, Lcom/msc/b/g;->c:Lcom/msc/b/g;

    invoke-static {v0, v1, v2}, Lcom/msc/c/g;->a(JLcom/msc/b/g;)V

    .line 5275
    const/4 v0, 0x0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    return-object v0
.end method

.method public final declared-synchronized a(Lcom/msc/b/c;)V
    .locals 6

    .prologue
    .line 5331
    monitor-enter p0

    :try_start_0
    invoke-super {p0, p1}, Lcom/msc/c/b;->a(Lcom/msc/b/c;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 5333
    if-nez p1, :cond_1

    .line 5355
    :cond_0
    :goto_0
    monitor-exit p0

    return-void

    .line 5338
    :cond_1
    :try_start_1
    invoke-virtual {p1}, Lcom/msc/b/c;->a()J

    move-result-wide v0

    .line 5339
    invoke-virtual {p1}, Lcom/msc/b/c;->e()Ljava/lang/String;

    move-result-object v2

    .line 5341
    iget-wide v4, p0, Lcom/osp/app/signin/od;->d:J
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    cmp-long v0, v0, v4

    if-nez v0, :cond_0

    .line 5345
    :try_start_2
    iget-object v0, p0, Lcom/osp/app/signin/od;->c:Lcom/osp/app/signin/TnCView;

    invoke-static {}, Lcom/msc/c/h;->a()Lcom/msc/c/h;

    invoke-static {v2}, Lcom/msc/c/h;->x(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/osp/app/signin/TnCView;->a(Lcom/osp/app/signin/TnCView;Ljava/util/ArrayList;)Ljava/util/ArrayList;

    .line 5346
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v0, "TNC"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "GetSpecialTermsListTask termsList size = "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, Lcom/osp/app/signin/od;->c:Lcom/osp/app/signin/TnCView;

    invoke-static {v2}, Lcom/osp/app/signin/TnCView;->p(Lcom/osp/app/signin/TnCView;)Ljava/util/ArrayList;

    move-result-object v2

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 5347
    const-string v0, "Success"

    iput-object v0, p0, Lcom/osp/app/signin/od;->e:Ljava/lang/String;
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_0
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0

    .line 5348
    :catch_0
    move-exception v0

    .line 5350
    :try_start_3
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    .line 5351
    const-string v1, "Fail"

    iput-object v1, p0, Lcom/osp/app/signin/od;->e:Ljava/lang/String;

    .line 5352
    new-instance v1, Lcom/msc/c/f;

    invoke-direct {v1, v0}, Lcom/msc/c/f;-><init>(Ljava/lang/Exception;)V

    iput-object v1, p0, Lcom/osp/app/signin/od;->b:Lcom/msc/c/f;
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    goto :goto_0

    .line 5331
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method protected final a(Ljava/lang/Boolean;)V
    .locals 8

    .prologue
    const/4 v7, 0x3

    const/4 v2, 0x0

    .line 5280
    invoke-super {p0, p1}, Lcom/msc/c/b;->a(Ljava/lang/Boolean;)V

    .line 5282
    iget-object v0, p0, Lcom/osp/app/signin/od;->c:Lcom/osp/app/signin/TnCView;

    invoke-static {v0}, Lcom/osp/app/signin/TnCView;->P(Lcom/osp/app/signin/TnCView;)Landroid/app/ProgressDialog;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 5284
    iget-object v0, p0, Lcom/osp/app/signin/od;->c:Lcom/osp/app/signin/TnCView;

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lcom/osp/app/signin/TnCView;->a(Lcom/osp/app/signin/TnCView;Landroid/app/ProgressDialog;)Landroid/app/ProgressDialog;

    .line 5287
    :cond_0
    const-string v0, "Success"

    iget-object v1, p0, Lcom/osp/app/signin/od;->e:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_5

    iget-object v0, p0, Lcom/osp/app/signin/od;->c:Lcom/osp/app/signin/TnCView;

    invoke-static {v0}, Lcom/osp/app/signin/TnCView;->p(Lcom/osp/app/signin/TnCView;)Ljava/util/ArrayList;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_5

    .line 5289
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v0, "TNC"

    const-string v1, "GetSpecialTermsListTask Success"

    invoke-static {v0, v1}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 5291
    iget-object v0, p0, Lcom/osp/app/signin/od;->c:Lcom/osp/app/signin/TnCView;

    invoke-static {v0}, Lcom/osp/app/signin/TnCView;->p(Lcom/osp/app/signin/TnCView;)Ljava/util/ArrayList;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v3

    move v1, v2

    .line 5292
    :goto_0
    if-ge v1, v3, :cond_1

    .line 5294
    iget-object v0, p0, Lcom/osp/app/signin/od;->c:Lcom/osp/app/signin/TnCView;

    invoke-static {v0}, Lcom/osp/app/signin/TnCView;->p(Lcom/osp/app/signin/TnCView;)Ljava/util/ArrayList;

    move-result-object v0

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Ljava/lang/String;

    .line 5295
    aget-object v4, v0, v2

    const-string v5, "/"

    invoke-virtual {v4, v5}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v4

    .line 5296
    if-eqz v4, :cond_4

    array-length v5, v4

    const/4 v6, 0x4

    if-lt v5, v6, :cond_4

    aget-object v5, v4, v7

    if-eqz v5, :cond_4

    aget-object v4, v4, v7

    const-string v5, "general.txt"

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_4

    .line 5298
    const/4 v1, 0x1

    iget-object v3, p0, Lcom/osp/app/signin/od;->c:Lcom/osp/app/signin/TnCView;

    const v4, 0x7f090059

    invoke-virtual {v3, v4}, Lcom/osp/app/signin/TnCView;->getString(I)Ljava/lang/String;

    move-result-object v3

    aput-object v3, v0, v1

    .line 5303
    :cond_1
    iget-object v0, p0, Lcom/osp/app/signin/od;->c:Lcom/osp/app/signin/TnCView;

    new-instance v1, Lcom/osp/app/signin/op;

    iget-object v3, p0, Lcom/osp/app/signin/od;->c:Lcom/osp/app/signin/TnCView;

    invoke-direct {v1, v3}, Lcom/osp/app/signin/op;-><init>(Lcom/osp/app/signin/TnCView;)V

    invoke-static {v0, v1}, Lcom/osp/app/signin/TnCView;->a(Lcom/osp/app/signin/TnCView;Lcom/osp/app/signin/op;)Lcom/osp/app/signin/op;

    .line 5304
    iget-object v0, p0, Lcom/osp/app/signin/od;->c:Lcom/osp/app/signin/TnCView;

    invoke-static {v0}, Lcom/osp/app/signin/TnCView;->B(Lcom/osp/app/signin/TnCView;)Landroid/widget/ListView;

    move-result-object v0

    iget-object v1, p0, Lcom/osp/app/signin/od;->c:Lcom/osp/app/signin/TnCView;

    invoke-static {v1}, Lcom/osp/app/signin/TnCView;->w(Lcom/osp/app/signin/TnCView;)Lcom/osp/app/signin/op;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 5305
    iget-object v0, p0, Lcom/osp/app/signin/od;->c:Lcom/osp/app/signin/TnCView;

    invoke-static {v0}, Lcom/osp/app/signin/TnCView;->B(Lcom/osp/app/signin/TnCView;)Landroid/widget/ListView;

    move-result-object v0

    new-instance v1, Lcom/osp/app/signin/oi;

    iget-object v3, p0, Lcom/osp/app/signin/od;->c:Lcom/osp/app/signin/TnCView;

    invoke-direct {v1, v3, v2}, Lcom/osp/app/signin/oi;-><init>(Lcom/osp/app/signin/TnCView;B)V

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V

    .line 5306
    iget-object v0, p0, Lcom/osp/app/signin/od;->c:Lcom/osp/app/signin/TnCView;

    new-instance v1, Lcom/osp/app/signin/oj;

    iget-object v2, p0, Lcom/osp/app/signin/od;->c:Lcom/osp/app/signin/TnCView;

    invoke-direct {v1, v2}, Lcom/osp/app/signin/oj;-><init>(Lcom/osp/app/signin/TnCView;)V

    invoke-static {v0, v1}, Lcom/osp/app/signin/TnCView;->a(Lcom/osp/app/signin/TnCView;Lcom/osp/app/signin/oj;)Lcom/osp/app/signin/oj;

    .line 5308
    iget-object v0, p0, Lcom/osp/app/signin/od;->c:Lcom/osp/app/signin/TnCView;

    iget-object v1, p0, Lcom/osp/app/signin/od;->c:Lcom/osp/app/signin/TnCView;

    invoke-static {v1}, Lcom/osp/app/signin/TnCView;->p(Lcom/osp/app/signin/TnCView;)Ljava/util/ArrayList;

    move-result-object v1

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    new-array v1, v1, [Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/osp/app/signin/TnCView;->a(Lcom/osp/app/signin/TnCView;[Ljava/lang/String;)[Ljava/lang/String;

    .line 5310
    iget-object v0, p0, Lcom/osp/app/signin/od;->c:Lcom/osp/app/signin/TnCView;

    invoke-static {v0}, Lcom/osp/app/signin/TnCView;->K(Lcom/osp/app/signin/TnCView;)Z

    move-result v0

    if-nez v0, :cond_3

    iget-object v0, p0, Lcom/osp/app/signin/od;->c:Lcom/osp/app/signin/TnCView;

    invoke-static {v0}, Lcom/osp/app/signin/TnCView;->D(Lcom/osp/app/signin/TnCView;)Z

    move-result v0

    if-nez v0, :cond_2

    iget-object v0, p0, Lcom/osp/app/signin/od;->c:Lcom/osp/app/signin/TnCView;

    invoke-static {v0}, Lcom/osp/app/signin/TnCView;->L(Lcom/osp/app/signin/TnCView;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 5312
    :cond_2
    iget-object v0, p0, Lcom/osp/app/signin/od;->c:Lcom/osp/app/signin/TnCView;

    invoke-static {v0}, Lcom/osp/app/signin/TnCView;->M(Lcom/osp/app/signin/TnCView;)V

    .line 5327
    :cond_3
    :goto_1
    return-void

    .line 5292
    :cond_4
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto/16 :goto_0

    .line 5316
    :cond_5
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v0, "TNC"

    const-string v1, "GetSpecialTermsListTask Fail"

    invoke-static {v0, v1}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 5318
    invoke-virtual {p0, v2}, Lcom/osp/app/signin/od;->a(Z)V

    .line 5320
    iget-object v0, p0, Lcom/osp/app/signin/od;->c:Lcom/osp/app/signin/TnCView;

    invoke-virtual {v0}, Lcom/osp/app/signin/TnCView;->getIntent()Landroid/content/Intent;

    move-result-object v0

    .line 5321
    if-eqz v0, :cond_6

    const-string v1, "is_cancelable_just_one_activity"

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v0

    if-eqz v0, :cond_6

    .line 5323
    iget-object v0, p0, Lcom/osp/app/signin/od;->c:Lcom/osp/app/signin/TnCView;

    const/16 v1, 0xe

    iget-object v2, p0, Lcom/osp/app/signin/od;->c:Lcom/osp/app/signin/TnCView;

    invoke-static {v2}, Lcom/osp/app/signin/TnCView;->c(Lcom/osp/app/signin/TnCView;)Landroid/content/Intent;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/osp/app/signin/TnCView;->a(ILandroid/content/Intent;)V

    .line 5325
    :cond_6
    iget-object v0, p0, Lcom/osp/app/signin/od;->c:Lcom/osp/app/signin/TnCView;

    invoke-virtual {v0}, Lcom/osp/app/signin/TnCView;->finish()V

    goto :goto_1
.end method

.method public final b(Lcom/msc/b/c;)V
    .locals 4

    .prologue
    .line 5359
    invoke-super {p0, p1}, Lcom/msc/c/b;->b(Lcom/msc/b/c;)V

    .line 5361
    if-nez p1, :cond_1

    .line 5373
    :cond_0
    :goto_0
    return-void

    .line 5366
    :cond_1
    invoke-virtual {p1}, Lcom/msc/b/c;->a()J

    move-result-wide v0

    .line 5368
    iget-wide v2, p0, Lcom/osp/app/signin/od;->d:J

    cmp-long v0, v0, v2

    if-nez v0, :cond_0

    .line 5371
    const-string v0, "Fail"

    iput-object v0, p0, Lcom/osp/app/signin/od;->e:Ljava/lang/String;

    goto :goto_0
.end method

.method protected final d()V
    .locals 3

    .prologue
    .line 5255
    invoke-super {p0}, Lcom/msc/c/b;->d()V

    .line 5260
    iget-object v0, p0, Lcom/osp/app/signin/od;->c:Lcom/osp/app/signin/TnCView;

    invoke-virtual {v0}, Lcom/osp/app/signin/TnCView;->getIntent()Landroid/content/Intent;

    move-result-object v0

    .line 5261
    if-eqz v0, :cond_0

    const-string v1, "is_cancelable_just_one_activity"

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 5263
    iget-object v0, p0, Lcom/osp/app/signin/od;->c:Lcom/osp/app/signin/TnCView;

    const/16 v1, 0xe

    iget-object v2, p0, Lcom/osp/app/signin/od;->c:Lcom/osp/app/signin/TnCView;

    invoke-static {v2}, Lcom/osp/app/signin/TnCView;->c(Lcom/osp/app/signin/TnCView;)Landroid/content/Intent;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/osp/app/signin/TnCView;->a(ILandroid/content/Intent;)V

    .line 5265
    :cond_0
    iget-object v0, p0, Lcom/osp/app/signin/od;->c:Lcom/osp/app/signin/TnCView;

    invoke-virtual {v0}, Lcom/osp/app/signin/TnCView;->finish()V

    .line 5266
    return-void
.end method

.method protected final synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 5204
    invoke-virtual {p0}, Lcom/osp/app/signin/od;->a()Ljava/lang/Boolean;

    move-result-object v0

    return-object v0
.end method

.method protected final synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 5204
    check-cast p1, Ljava/lang/Boolean;

    invoke-virtual {p0, p1}, Lcom/osp/app/signin/od;->a(Ljava/lang/Boolean;)V

    return-void
.end method

.method protected final onPreExecute()V
    .locals 1

    .prologue
    .line 5244
    iget-object v0, p0, Lcom/osp/app/signin/od;->c:Lcom/osp/app/signin/TnCView;

    invoke-static {v0}, Lcom/osp/app/signin/TnCView;->P(Lcom/osp/app/signin/TnCView;)Landroid/app/ProgressDialog;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/osp/app/signin/od;->c:Lcom/osp/app/signin/TnCView;

    invoke-static {v0}, Lcom/osp/app/signin/TnCView;->P(Lcom/osp/app/signin/TnCView;)Landroid/app/ProgressDialog;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/ProgressDialog;->isShowing()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 5246
    iget-object v0, p0, Lcom/osp/app/signin/od;->c:Lcom/osp/app/signin/TnCView;

    invoke-static {v0}, Lcom/osp/app/signin/TnCView;->P(Lcom/osp/app/signin/TnCView;)Landroid/app/ProgressDialog;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/osp/app/signin/od;->a(Landroid/app/ProgressDialog;)V

    .line 5251
    :goto_0
    return-void

    .line 5249
    :cond_0
    invoke-super {p0}, Lcom/msc/c/b;->onPreExecute()V

    goto :goto_0
.end method
