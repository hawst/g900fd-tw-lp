.class public Lcom/osp/app/signin/BottomSoftkeyLinearLayout;
.super Landroid/widget/LinearLayout;
.source "BottomSoftkeyLinearLayout.java"


# instance fields
.field private a:Landroid/content/Context;

.field private b:Ljava/lang/String;

.field private c:Ljava/lang/Boolean;

.field private d:I

.field private e:I

.field private f:Landroid/widget/LinearLayout;

.field private g:Landroid/widget/LinearLayout;

.field private h:Landroid/widget/LinearLayout;

.field private i:Landroid/widget/TextView;

.field private j:Landroid/widget/LinearLayout;

.field private k:Landroid/widget/LinearLayout;

.field private l:Landroid/widget/TextView;

.field private m:Landroid/widget/ImageView;

.field private n:Landroid/widget/ImageView;

.field private o:Landroid/view/View;

.field private p:Ljava/lang/Boolean;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 78
    invoke-direct {p0, p1}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;)V

    .line 59
    const/4 v0, 0x0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Lcom/osp/app/signin/BottomSoftkeyLinearLayout;->c:Ljava/lang/Boolean;

    .line 75
    const/4 v0, 0x1

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Lcom/osp/app/signin/BottomSoftkeyLinearLayout;->p:Ljava/lang/Boolean;

    .line 79
    invoke-direct {p0, p1}, Lcom/osp/app/signin/BottomSoftkeyLinearLayout;->a(Landroid/content/Context;)V

    .line 80
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .prologue
    .line 83
    invoke-direct {p0, p1, p2}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 59
    const/4 v0, 0x0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Lcom/osp/app/signin/BottomSoftkeyLinearLayout;->c:Ljava/lang/Boolean;

    .line 75
    const/4 v0, 0x1

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Lcom/osp/app/signin/BottomSoftkeyLinearLayout;->p:Ljava/lang/Boolean;

    .line 84
    invoke-direct {p0, p1}, Lcom/osp/app/signin/BottomSoftkeyLinearLayout;->a(Landroid/content/Context;)V

    .line 85
    return-void
.end method

.method private a(Landroid/content/Context;)V
    .locals 2

    .prologue
    .line 93
    iput-object p1, p0, Lcom/osp/app/signin/BottomSoftkeyLinearLayout;->a:Landroid/content/Context;

    .line 95
    const-string v0, "layout_inflater"

    invoke-virtual {p1, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/LayoutInflater;

    .line 96
    const v1, 0x7f030013

    invoke-virtual {v0, v1, p0}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v1

    .line 98
    const v0, 0x7f0c009a

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcom/osp/app/signin/BottomSoftkeyLinearLayout;->f:Landroid/widget/LinearLayout;

    .line 99
    const v0, 0x7f0c009b

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcom/osp/app/signin/BottomSoftkeyLinearLayout;->g:Landroid/widget/LinearLayout;

    .line 100
    const v0, 0x7f0c009c

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcom/osp/app/signin/BottomSoftkeyLinearLayout;->h:Landroid/widget/LinearLayout;

    .line 101
    const v0, 0x7f0c009e

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/osp/app/signin/BottomSoftkeyLinearLayout;->i:Landroid/widget/TextView;

    .line 102
    const v0, 0x7f0c009d

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/osp/app/signin/BottomSoftkeyLinearLayout;->n:Landroid/widget/ImageView;

    .line 103
    const v0, 0x7f0c00a0

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcom/osp/app/signin/BottomSoftkeyLinearLayout;->j:Landroid/widget/LinearLayout;

    .line 104
    const v0, 0x7f0c00a1

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcom/osp/app/signin/BottomSoftkeyLinearLayout;->k:Landroid/widget/LinearLayout;

    .line 105
    const v0, 0x7f0c00a2

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/osp/app/signin/BottomSoftkeyLinearLayout;->l:Landroid/widget/TextView;

    .line 106
    const v0, 0x7f0c00a3

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/osp/app/signin/BottomSoftkeyLinearLayout;->m:Landroid/widget/ImageView;

    .line 107
    const v0, 0x7f0c009f

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/osp/app/signin/BottomSoftkeyLinearLayout;->o:Landroid/view/View;

    .line 109
    invoke-virtual {p0}, Lcom/osp/app/signin/BottomSoftkeyLinearLayout;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/osp/app/signin/BottomSoftkeyLinearLayout;->a(Landroid/content/res/Configuration;)V

    .line 111
    invoke-static {}, Lcom/osp/device/b;->b()Lcom/osp/device/b;

    move-result-object v0

    iget-object v1, p0, Lcom/osp/app/signin/BottomSoftkeyLinearLayout;->a:Landroid/content/Context;

    invoke-virtual {v0, v1}, Lcom/osp/device/b;->i(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-static {}, Lcom/osp/app/signin/SamsungService;->a()Z

    move-result v0

    if-nez v0, :cond_0

    .line 113
    iget-object v0, p0, Lcom/osp/app/signin/BottomSoftkeyLinearLayout;->f:Landroid/widget/LinearLayout;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 118
    :goto_0
    return-void

    .line 116
    :cond_0
    iget-object v0, p0, Lcom/osp/app/signin/BottomSoftkeyLinearLayout;->f:Landroid/widget/LinearLayout;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setVisibility(I)V

    goto :goto_0
.end method

.method private a(Landroid/content/res/Configuration;)V
    .locals 3

    .prologue
    .line 748
    invoke-static {}, Lcom/osp/app/signin/SamsungService;->a()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/osp/app/signin/BottomSoftkeyLinearLayout;->a:Landroid/content/Context;

    instance-of v0, v0, Lcom/osp/app/signin/AccountView;

    if-eqz v0, :cond_2

    :cond_0
    iget-object v0, p0, Lcom/osp/app/signin/BottomSoftkeyLinearLayout;->a:Landroid/content/Context;

    invoke-static {v0}, Lcom/osp/app/util/r;->g(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 750
    invoke-virtual {p0}, Lcom/osp/app/signin/BottomSoftkeyLinearLayout;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f080116

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v0

    float-to-int v0, v0

    .line 752
    invoke-static {}, Lcom/osp/device/b;->b()Lcom/osp/device/b;

    move-result-object v1

    iget-object v2, p0, Lcom/osp/app/signin/BottomSoftkeyLinearLayout;->a:Landroid/content/Context;

    invoke-virtual {v1, v2}, Lcom/osp/device/b;->i(Landroid/content/Context;)Z

    move-result v1

    if-eqz v1, :cond_4

    .line 754
    iget v1, p1, Landroid/content/res/Configuration;->orientation:I

    const/4 v2, 0x1

    if-ne v1, v2, :cond_3

    .line 756
    invoke-virtual {p0}, Lcom/osp/app/signin/BottomSoftkeyLinearLayout;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f08011e

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v1

    float-to-int v1, v1

    add-int/2addr v0, v1

    move v1, v0

    .line 763
    :goto_0
    iget-object v0, p0, Lcom/osp/app/signin/BottomSoftkeyLinearLayout;->g:Landroid/widget/LinearLayout;

    const/16 v2, 0x13

    invoke-static {v0, v2}, Lcom/msc/sa/c/d;->a(Landroid/view/View;I)V

    .line 764
    iget-object v0, p0, Lcom/osp/app/signin/BottomSoftkeyLinearLayout;->h:Landroid/widget/LinearLayout;

    if-eqz v0, :cond_1

    .line 766
    iget-object v0, p0, Lcom/osp/app/signin/BottomSoftkeyLinearLayout;->h:Landroid/widget/LinearLayout;

    invoke-virtual {v0}, Landroid/widget/LinearLayout;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout$LayoutParams;

    .line 767
    iput v1, v0, Landroid/widget/LinearLayout$LayoutParams;->leftMargin:I

    .line 768
    iget-object v2, p0, Lcom/osp/app/signin/BottomSoftkeyLinearLayout;->h:Landroid/widget/LinearLayout;

    invoke-static {v2, v0}, Lcom/msc/sa/c/d;->a(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    .line 772
    :cond_1
    iget-object v0, p0, Lcom/osp/app/signin/BottomSoftkeyLinearLayout;->j:Landroid/widget/LinearLayout;

    const/16 v2, 0x15

    invoke-static {v0, v2}, Lcom/msc/sa/c/d;->a(Landroid/view/View;I)V

    .line 773
    iget-object v0, p0, Lcom/osp/app/signin/BottomSoftkeyLinearLayout;->k:Landroid/widget/LinearLayout;

    if-eqz v0, :cond_2

    .line 775
    iget-object v0, p0, Lcom/osp/app/signin/BottomSoftkeyLinearLayout;->k:Landroid/widget/LinearLayout;

    invoke-virtual {v0}, Landroid/widget/LinearLayout;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout$LayoutParams;

    .line 776
    iput v1, v0, Landroid/widget/LinearLayout$LayoutParams;->rightMargin:I

    .line 777
    iget-object v1, p0, Lcom/osp/app/signin/BottomSoftkeyLinearLayout;->k:Landroid/widget/LinearLayout;

    invoke-static {v1, v0}, Lcom/msc/sa/c/d;->a(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    .line 780
    :cond_2
    return-void

    .line 759
    :cond_3
    invoke-virtual {p0}, Lcom/osp/app/signin/BottomSoftkeyLinearLayout;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f08011f

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v1

    float-to-int v1, v1

    add-int/2addr v0, v1

    move v1, v0

    goto :goto_0

    :cond_4
    move v1, v0

    goto :goto_0
.end method

.method private a(Landroid/widget/TextView;Landroid/widget/ImageView;)V
    .locals 3

    .prologue
    .line 657
    invoke-static {}, Lcom/osp/device/b;->b()Lcom/osp/device/b;

    move-result-object v0

    iget-object v1, p0, Lcom/osp/app/signin/BottomSoftkeyLinearLayout;->a:Landroid/content/Context;

    invoke-virtual {v0, v1}, Lcom/osp/device/b;->i(Landroid/content/Context;)Z

    move-result v0

    if-nez v0, :cond_0

    invoke-virtual {p2}, Landroid/widget/ImageView;->getVisibility()I

    move-result v0

    if-nez v0, :cond_0

    .line 659
    invoke-virtual {p2}, Landroid/widget/ImageView;->getDrawable()Landroid/graphics/drawable/Drawable;

    move-result-object v0

    invoke-virtual {v0}, Landroid/graphics/drawable/Drawable;->getMinimumWidth()I

    move-result v0

    invoke-virtual {p0}, Lcom/osp/app/signin/BottomSoftkeyLinearLayout;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f080085

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    add-int/2addr v0, v1

    invoke-virtual {p0}, Lcom/osp/app/signin/BottomSoftkeyLinearLayout;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f080086

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    add-int/2addr v0, v1

    .line 661
    iget-object v1, p0, Lcom/osp/app/signin/BottomSoftkeyLinearLayout;->a:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v1

    .line 662
    iget v1, v1, Landroid/util/DisplayMetrics;->widthPixels:I

    .line 663
    div-int/lit8 v1, v1, 0x2

    sub-int v0, v1, v0

    .line 665
    invoke-virtual {p1, v0}, Landroid/widget/TextView;->setMaxWidth(I)V

    .line 668
    :cond_0
    return-void
.end method

.method private a(Ljava/lang/String;Landroid/widget/TextView;)V
    .locals 4

    .prologue
    const v3, 0x7f080088

    const/4 v2, 0x0

    .line 677
    iget-object v0, p0, Lcom/osp/app/signin/BottomSoftkeyLinearLayout;->a:Landroid/content/Context;

    check-cast v0, Lcom/osp/app/util/BaseActivity;

    invoke-static {v0}, Lcom/osp/app/util/an;->a(Lcom/osp/app/util/BaseActivity;)Lcom/osp/app/util/an;

    const v0, 0x7f08008a

    invoke-static {v0}, Lcom/osp/app/util/an;->b(I)I

    move-result v0

    .line 678
    invoke-virtual {p0}, Lcom/osp/app/signin/BottomSoftkeyLinearLayout;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v0

    invoke-virtual {p2, v2, v0}, Landroid/widget/TextView;->setTextSize(IF)V

    .line 679
    invoke-static {}, Lcom/osp/app/signin/SamsungService;->a()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/osp/app/signin/BottomSoftkeyLinearLayout;->a:Landroid/content/Context;

    invoke-static {v0}, Lcom/osp/app/util/r;->m(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 681
    iget-object v0, p0, Lcom/osp/app/signin/BottomSoftkeyLinearLayout;->i:Landroid/widget/TextView;

    invoke-virtual {p0}, Lcom/osp/app/signin/BottomSoftkeyLinearLayout;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1, v3}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v1

    float-to-int v1, v1

    int-to-float v1, v1

    invoke-virtual {v0, v2, v1}, Landroid/widget/TextView;->setTextSize(IF)V

    .line 682
    iget-object v0, p0, Lcom/osp/app/signin/BottomSoftkeyLinearLayout;->l:Landroid/widget/TextView;

    invoke-virtual {p0}, Lcom/osp/app/signin/BottomSoftkeyLinearLayout;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1, v3}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v1

    float-to-int v1, v1

    int-to-float v1, v1

    invoke-virtual {v0, v2, v1}, Landroid/widget/TextView;->setTextSize(IF)V

    .line 685
    :cond_0
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x15

    if-lt v0, v1, :cond_1

    .line 689
    sget-object v0, Ljava/util/Locale;->ENGLISH:Ljava/util/Locale;

    invoke-virtual {p1, v0}, Ljava/lang/String;->toUpperCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p2, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 695
    :goto_0
    return-void

    .line 693
    :cond_1
    invoke-virtual {p2, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0
.end method

.method private f()V
    .locals 2

    .prologue
    .line 418
    iget-object v0, p0, Lcom/osp/app/signin/BottomSoftkeyLinearLayout;->a:Landroid/content/Context;

    invoke-static {v0}, Lcom/osp/app/util/r;->k(Landroid/content/Context;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 420
    iget-object v0, p0, Lcom/osp/app/signin/BottomSoftkeyLinearLayout;->o:Landroid/view/View;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 422
    :cond_0
    return-void
.end method


# virtual methods
.method public final a()Ljava/lang/Boolean;
    .locals 1

    .prologue
    .line 481
    iget-object v0, p0, Lcom/osp/app/signin/BottomSoftkeyLinearLayout;->g:Landroid/widget/LinearLayout;

    invoke-virtual {v0}, Landroid/widget/LinearLayout;->isEnabled()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    return-object v0
.end method

.method public final a(I)V
    .locals 0

    .prologue
    .line 436
    iput p1, p0, Lcom/osp/app/signin/BottomSoftkeyLinearLayout;->e:I

    .line 437
    return-void
.end method

.method public final a(Landroid/content/Context;Ljava/lang/Boolean;)V
    .locals 8

    .prologue
    const v7, 0x7f0200f0

    const v6, 0x7f0200ed

    const v5, 0x7f020020

    const v4, 0x7f020030

    const/4 v3, 0x0

    .line 428
    iput-object p2, p0, Lcom/osp/app/signin/BottomSoftkeyLinearLayout;->p:Ljava/lang/Boolean;

    .line 429
    iput-object p1, p0, Lcom/osp/app/signin/BottomSoftkeyLinearLayout;->a:Landroid/content/Context;

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/osp/app/signin/BottomSoftkeyLinearLayout;->b:Ljava/lang/String;

    iget-object v0, p0, Lcom/osp/app/signin/BottomSoftkeyLinearLayout;->a:Landroid/content/Context;

    invoke-static {v0}, Lcom/osp/app/util/r;->n(Landroid/content/Context;)Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Lcom/osp/app/signin/BottomSoftkeyLinearLayout;->c:Ljava/lang/Boolean;

    const/4 v0, 0x0

    iget-object v1, p0, Lcom/osp/app/signin/BottomSoftkeyLinearLayout;->a:Landroid/content/Context;

    instance-of v1, v1, Lcom/osp/app/util/BaseActivity;

    if-eqz v1, :cond_b

    iget-object v0, p0, Lcom/osp/app/signin/BottomSoftkeyLinearLayout;->a:Landroid/content/Context;

    check-cast v0, Lcom/osp/app/util/BaseActivity;

    invoke-static {v0}, Lcom/osp/app/util/an;->a(Lcom/osp/app/util/BaseActivity;)Lcom/osp/app/util/an;

    move-result-object v0

    move-object v1, v0

    :goto_0
    const-string v0, "layout_inflater"

    invoke-virtual {p1, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/LayoutInflater;

    const v2, 0x7f030013

    invoke-virtual {v0, v2, p0}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v2

    const v0, 0x7f0c009a

    invoke-virtual {v2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcom/osp/app/signin/BottomSoftkeyLinearLayout;->f:Landroid/widget/LinearLayout;

    const v0, 0x7f0c009b

    invoke-virtual {v2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcom/osp/app/signin/BottomSoftkeyLinearLayout;->g:Landroid/widget/LinearLayout;

    const v0, 0x7f0c009c

    invoke-virtual {v2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcom/osp/app/signin/BottomSoftkeyLinearLayout;->h:Landroid/widget/LinearLayout;

    const v0, 0x7f0c009e

    invoke-virtual {v2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/osp/app/signin/BottomSoftkeyLinearLayout;->i:Landroid/widget/TextView;

    const v0, 0x7f0c00a0

    invoke-virtual {v2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcom/osp/app/signin/BottomSoftkeyLinearLayout;->j:Landroid/widget/LinearLayout;

    const v0, 0x7f0c00a1

    invoke-virtual {v2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcom/osp/app/signin/BottomSoftkeyLinearLayout;->k:Landroid/widget/LinearLayout;

    const v0, 0x7f0c00a2

    invoke-virtual {v2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/osp/app/signin/BottomSoftkeyLinearLayout;->l:Landroid/widget/TextView;

    const v0, 0x7f0c00a3

    invoke-virtual {v2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/osp/app/signin/BottomSoftkeyLinearLayout;->m:Landroid/widget/ImageView;

    const v0, 0x7f0c009d

    invoke-virtual {v2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/osp/app/signin/BottomSoftkeyLinearLayout;->n:Landroid/widget/ImageView;

    const v0, 0x7f0c009f

    invoke-virtual {v2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/osp/app/signin/BottomSoftkeyLinearLayout;->o:Landroid/view/View;

    if-eqz v1, :cond_1

    const v0, 0x7f02008f

    invoke-static {v0}, Lcom/osp/app/util/an;->a(I)I

    move-result v0

    iput v0, p0, Lcom/osp/app/signin/BottomSoftkeyLinearLayout;->e:I

    const v0, 0x7f020096

    invoke-static {v0}, Lcom/osp/app/util/an;->a(I)I

    move-result v0

    iput v0, p0, Lcom/osp/app/signin/BottomSoftkeyLinearLayout;->d:I

    invoke-static {}, Lcom/osp/app/signin/SamsungService;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    const v0, 0x7f02008f

    invoke-static {v0}, Lcom/osp/app/util/an;->a(I)I

    move-result v0

    iput v0, p0, Lcom/osp/app/signin/BottomSoftkeyLinearLayout;->e:I

    :cond_0
    iget-object v0, p0, Lcom/osp/app/signin/BottomSoftkeyLinearLayout;->c:Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_4

    const v0, 0x7f02011d

    iput v0, p0, Lcom/osp/app/signin/BottomSoftkeyLinearLayout;->e:I

    iget-object v0, p0, Lcom/osp/app/signin/BottomSoftkeyLinearLayout;->f:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v3}, Landroid/widget/LinearLayout;->setVisibility(I)V

    iget-object v0, p0, Lcom/osp/app/signin/BottomSoftkeyLinearLayout;->f:Landroid/widget/LinearLayout;

    const v1, 0x7f0200d7

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setBackgroundResource(I)V

    iget-object v0, p0, Lcom/osp/app/signin/BottomSoftkeyLinearLayout;->o:Landroid/view/View;

    const v1, 0x7f0200f4

    invoke-virtual {v0, v1}, Landroid/view/View;->setBackgroundResource(I)V

    iget-object v0, p0, Lcom/osp/app/signin/BottomSoftkeyLinearLayout;->g:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v5}, Landroid/widget/LinearLayout;->setBackgroundResource(I)V

    iget-object v0, p0, Lcom/osp/app/signin/BottomSoftkeyLinearLayout;->i:Landroid/widget/TextView;

    invoke-virtual {p0}, Lcom/osp/app/signin/BottomSoftkeyLinearLayout;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1, v4}, Landroid/content/res/Resources;->getColorStateList(I)Landroid/content/res/ColorStateList;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setTextColor(Landroid/content/res/ColorStateList;)V

    iget-object v0, p0, Lcom/osp/app/signin/BottomSoftkeyLinearLayout;->j:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v5}, Landroid/widget/LinearLayout;->setBackgroundResource(I)V

    iget-object v0, p0, Lcom/osp/app/signin/BottomSoftkeyLinearLayout;->l:Landroid/widget/TextView;

    invoke-virtual {p0}, Lcom/osp/app/signin/BottomSoftkeyLinearLayout;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1, v4}, Landroid/content/res/Resources;->getColorStateList(I)Landroid/content/res/ColorStateList;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setTextColor(Landroid/content/res/ColorStateList;)V

    :cond_1
    :goto_1
    invoke-static {}, Lcom/osp/app/signin/SamsungService;->a()Z

    move-result v0

    if-nez v0, :cond_2

    invoke-static {p1}, Lcom/osp/app/util/r;->m(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_2

    new-instance v0, Landroid/widget/LinearLayout$LayoutParams;

    const/4 v1, -0x1

    invoke-virtual {p0}, Lcom/osp/app/signin/BottomSoftkeyLinearLayout;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f080087

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v2

    float-to-int v2, v2

    invoke-direct {v0, v1, v2}, Landroid/widget/LinearLayout$LayoutParams;-><init>(II)V

    iget-object v1, p0, Lcom/osp/app/signin/BottomSoftkeyLinearLayout;->f:Landroid/widget/LinearLayout;

    invoke-virtual {v1, v0}, Landroid/widget/LinearLayout;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    :cond_2
    iget-object v0, p0, Lcom/osp/app/signin/BottomSoftkeyLinearLayout;->a:Landroid/content/Context;

    iget-object v1, p0, Lcom/osp/app/signin/BottomSoftkeyLinearLayout;->i:Landroid/widget/TextView;

    iget-object v2, p0, Lcom/osp/app/signin/BottomSoftkeyLinearLayout;->p:Ljava/lang/Boolean;

    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v2

    invoke-static {v0, v1, v2}, Lcom/msc/sa/c/d;->a(Landroid/content/Context;Landroid/view/View;Z)V

    iget-object v0, p0, Lcom/osp/app/signin/BottomSoftkeyLinearLayout;->a:Landroid/content/Context;

    iget-object v1, p0, Lcom/osp/app/signin/BottomSoftkeyLinearLayout;->l:Landroid/widget/TextView;

    iget-object v2, p0, Lcom/osp/app/signin/BottomSoftkeyLinearLayout;->p:Ljava/lang/Boolean;

    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v2

    invoke-static {v0, v1, v2}, Lcom/msc/sa/c/d;->a(Landroid/content/Context;Landroid/view/View;Z)V

    invoke-static {}, Lcom/osp/app/signin/SamsungService;->a()Z

    move-result v0

    if-eqz v0, :cond_3

    :try_start_0
    iget-object v0, p0, Lcom/osp/app/signin/BottomSoftkeyLinearLayout;->a:Landroid/content/Context;

    const-string v1, "com.sec.android.app.SecSetupWizard"

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/content/Context;->createPackageContext(Ljava/lang/String;I)Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const-string v0, "bottom_buttons_height"

    const-string v2, "dimen"

    const-string v3, "com.sec.android.app.SecSetupWizard"

    invoke-virtual {v1, v0, v2, v3}, Landroid/content/res/Resources;->getIdentifier(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)I

    move-result v2

    if-lez v2, :cond_a

    iget-object v0, p0, Lcom/osp/app/signin/BottomSoftkeyLinearLayout;->f:Landroid/widget/LinearLayout;

    invoke-virtual {v0}, Landroid/widget/LinearLayout;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout$LayoutParams;

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v1

    float-to-int v1, v1

    iput v1, v0, Landroid/widget/LinearLayout$LayoutParams;->height:I

    iget-object v1, p0, Lcom/osp/app/signin/BottomSoftkeyLinearLayout;->f:Landroid/widget/LinearLayout;

    invoke-virtual {v1, v0}, Landroid/widget/LinearLayout;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "setupwizard bottom height applied successful : "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v0, v0, Landroid/widget/LinearLayout$LayoutParams;->height:I

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/osp/app/util/aq;->c(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 430
    :cond_3
    :goto_2
    return-void

    .line 429
    :cond_4
    invoke-static {p1}, Lcom/osp/app/util/r;->k(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_7

    iget-object v0, p0, Lcom/osp/app/signin/BottomSoftkeyLinearLayout;->f:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v3}, Landroid/widget/LinearLayout;->setVisibility(I)V

    iget-object v0, p0, Lcom/osp/app/signin/BottomSoftkeyLinearLayout;->o:Landroid/view/View;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    iget-object v0, p0, Lcom/osp/app/signin/BottomSoftkeyLinearLayout;->i:Landroid/widget/TextView;

    invoke-virtual {p0}, Lcom/osp/app/signin/BottomSoftkeyLinearLayout;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f02001d

    invoke-static {v2}, Lcom/osp/app/util/an;->a(I)I

    move-result v2

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getColorStateList(I)Landroid/content/res/ColorStateList;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setTextColor(Landroid/content/res/ColorStateList;)V

    iget-object v0, p0, Lcom/osp/app/signin/BottomSoftkeyLinearLayout;->l:Landroid/widget/TextView;

    invoke-virtual {p0}, Lcom/osp/app/signin/BottomSoftkeyLinearLayout;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f02001d

    invoke-static {v2}, Lcom/osp/app/util/an;->a(I)I

    move-result v2

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getColorStateList(I)Landroid/content/res/ColorStateList;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setTextColor(Landroid/content/res/ColorStateList;)V

    invoke-static {p1}, Lcom/msc/sa/c/d;->c(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_6

    iget-object v0, p0, Lcom/osp/app/signin/BottomSoftkeyLinearLayout;->g:Landroid/widget/LinearLayout;

    const v1, 0x7f02001b

    invoke-static {v1}, Lcom/osp/app/util/an;->a(I)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setBackgroundResource(I)V

    iget-object v0, p0, Lcom/osp/app/signin/BottomSoftkeyLinearLayout;->j:Landroid/widget/LinearLayout;

    const v1, 0x7f020019

    invoke-static {v1}, Lcom/osp/app/util/an;->a(I)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setBackgroundResource(I)V

    :goto_3
    iget-object v0, p0, Lcom/osp/app/signin/BottomSoftkeyLinearLayout;->a:Landroid/content/Context;

    invoke-static {v0}, Lcom/osp/app/util/r;->g(Landroid/content/Context;)Z

    move-result v0

    if-nez v0, :cond_5

    iget-object v0, p0, Lcom/osp/app/signin/BottomSoftkeyLinearLayout;->a:Landroid/content/Context;

    invoke-static {v0}, Lcom/osp/app/util/r;->l(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_5
    iget-object v0, p0, Lcom/osp/app/signin/BottomSoftkeyLinearLayout;->h:Landroid/widget/LinearLayout;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/osp/app/signin/BottomSoftkeyLinearLayout;->k:Landroid/widget/LinearLayout;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/osp/app/signin/BottomSoftkeyLinearLayout;->f:Landroid/widget/LinearLayout;

    const v1, 0x7f0200d7

    invoke-static {v1}, Lcom/osp/app/util/an;->a(I)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setBackgroundResource(I)V

    invoke-static {}, Lcom/osp/app/signin/SamsungService;->a()Z

    move-result v0

    if-eqz v0, :cond_9

    iget-object v0, p0, Lcom/osp/app/signin/BottomSoftkeyLinearLayout;->i:Landroid/widget/TextView;

    invoke-virtual {p0}, Lcom/osp/app/signin/BottomSoftkeyLinearLayout;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-static {v7}, Lcom/osp/app/util/an;->a(I)I

    move-result v2

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getColorStateList(I)Landroid/content/res/ColorStateList;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setTextColor(Landroid/content/res/ColorStateList;)V

    iget-object v0, p0, Lcom/osp/app/signin/BottomSoftkeyLinearLayout;->l:Landroid/widget/TextView;

    invoke-virtual {p0}, Lcom/osp/app/signin/BottomSoftkeyLinearLayout;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-static {v7}, Lcom/osp/app/util/an;->a(I)I

    move-result v2

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getColorStateList(I)Landroid/content/res/ColorStateList;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setTextColor(Landroid/content/res/ColorStateList;)V

    :goto_4
    iget-object v0, p0, Lcom/osp/app/signin/BottomSoftkeyLinearLayout;->g:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v3}, Landroid/widget/LinearLayout;->setBackgroundResource(I)V

    iget-object v0, p0, Lcom/osp/app/signin/BottomSoftkeyLinearLayout;->j:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v3}, Landroid/widget/LinearLayout;->setBackgroundResource(I)V

    iget-object v0, p0, Lcom/osp/app/signin/BottomSoftkeyLinearLayout;->h:Landroid/widget/LinearLayout;

    invoke-static {v6}, Lcom/osp/app/util/an;->a(I)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setBackgroundResource(I)V

    iget-object v0, p0, Lcom/osp/app/signin/BottomSoftkeyLinearLayout;->k:Landroid/widget/LinearLayout;

    invoke-static {v6}, Lcom/osp/app/util/an;->a(I)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setBackgroundResource(I)V

    goto/16 :goto_1

    :cond_6
    iget-object v0, p0, Lcom/osp/app/signin/BottomSoftkeyLinearLayout;->g:Landroid/widget/LinearLayout;

    const v1, 0x7f020019

    invoke-static {v1}, Lcom/osp/app/util/an;->a(I)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setBackgroundResource(I)V

    iget-object v0, p0, Lcom/osp/app/signin/BottomSoftkeyLinearLayout;->j:Landroid/widget/LinearLayout;

    const v1, 0x7f02001b

    invoke-static {v1}, Lcom/osp/app/util/an;->a(I)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setBackgroundResource(I)V

    goto/16 :goto_3

    :cond_7
    iget-object v0, p0, Lcom/osp/app/signin/BottomSoftkeyLinearLayout;->f:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v3}, Landroid/widget/LinearLayout;->setVisibility(I)V

    iget-object v0, p0, Lcom/osp/app/signin/BottomSoftkeyLinearLayout;->f:Landroid/widget/LinearLayout;

    const v1, 0x7f0200d7

    invoke-static {v1}, Lcom/osp/app/util/an;->a(I)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setBackgroundResource(I)V

    iget-object v0, p0, Lcom/osp/app/signin/BottomSoftkeyLinearLayout;->o:Landroid/view/View;

    const v1, 0x7f0200f4

    invoke-static {v1}, Lcom/osp/app/util/an;->a(I)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/view/View;->setBackgroundResource(I)V

    invoke-static {}, Lcom/osp/app/signin/SamsungService;->a()Z

    move-result v0

    if-eqz v0, :cond_8

    invoke-static {p1}, Lcom/osp/app/util/r;->n(Landroid/content/Context;)Z

    move-result v0

    if-nez v0, :cond_8

    iget-object v0, p0, Lcom/osp/app/signin/BottomSoftkeyLinearLayout;->g:Landroid/widget/LinearLayout;

    invoke-static {v6}, Lcom/osp/app/util/an;->a(I)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setBackgroundResource(I)V

    iget-object v0, p0, Lcom/osp/app/signin/BottomSoftkeyLinearLayout;->i:Landroid/widget/TextView;

    invoke-virtual {p0}, Lcom/osp/app/signin/BottomSoftkeyLinearLayout;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-static {v7}, Lcom/osp/app/util/an;->a(I)I

    move-result v2

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getColorStateList(I)Landroid/content/res/ColorStateList;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setTextColor(Landroid/content/res/ColorStateList;)V

    iget-object v0, p0, Lcom/osp/app/signin/BottomSoftkeyLinearLayout;->j:Landroid/widget/LinearLayout;

    invoke-static {v6}, Lcom/osp/app/util/an;->a(I)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setBackgroundResource(I)V

    iget-object v0, p0, Lcom/osp/app/signin/BottomSoftkeyLinearLayout;->l:Landroid/widget/TextView;

    invoke-virtual {p0}, Lcom/osp/app/signin/BottomSoftkeyLinearLayout;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-static {v7}, Lcom/osp/app/util/an;->a(I)I

    move-result v2

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getColorStateList(I)Landroid/content/res/ColorStateList;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setTextColor(Landroid/content/res/ColorStateList;)V

    goto/16 :goto_3

    :cond_8
    iget-object v0, p0, Lcom/osp/app/signin/BottomSoftkeyLinearLayout;->g:Landroid/widget/LinearLayout;

    invoke-static {v5}, Lcom/osp/app/util/an;->a(I)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setBackgroundResource(I)V

    iget-object v0, p0, Lcom/osp/app/signin/BottomSoftkeyLinearLayout;->i:Landroid/widget/TextView;

    invoke-virtual {p0}, Lcom/osp/app/signin/BottomSoftkeyLinearLayout;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-static {v4}, Lcom/osp/app/util/an;->a(I)I

    move-result v2

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getColorStateList(I)Landroid/content/res/ColorStateList;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setTextColor(Landroid/content/res/ColorStateList;)V

    iget-object v0, p0, Lcom/osp/app/signin/BottomSoftkeyLinearLayout;->j:Landroid/widget/LinearLayout;

    invoke-static {v5}, Lcom/osp/app/util/an;->a(I)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setBackgroundResource(I)V

    iget-object v0, p0, Lcom/osp/app/signin/BottomSoftkeyLinearLayout;->l:Landroid/widget/TextView;

    invoke-virtual {p0}, Lcom/osp/app/signin/BottomSoftkeyLinearLayout;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-static {v4}, Lcom/osp/app/util/an;->a(I)I

    move-result v2

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getColorStateList(I)Landroid/content/res/ColorStateList;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setTextColor(Landroid/content/res/ColorStateList;)V

    goto/16 :goto_3

    :cond_9
    iget-object v0, p0, Lcom/osp/app/signin/BottomSoftkeyLinearLayout;->i:Landroid/widget/TextView;

    invoke-virtual {p0}, Lcom/osp/app/signin/BottomSoftkeyLinearLayout;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-static {v4}, Lcom/osp/app/util/an;->a(I)I

    move-result v2

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getColorStateList(I)Landroid/content/res/ColorStateList;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setTextColor(Landroid/content/res/ColorStateList;)V

    iget-object v0, p0, Lcom/osp/app/signin/BottomSoftkeyLinearLayout;->l:Landroid/widget/TextView;

    invoke-virtual {p0}, Lcom/osp/app/signin/BottomSoftkeyLinearLayout;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-static {v4}, Lcom/osp/app/util/an;->a(I)I

    move-result v2

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getColorStateList(I)Landroid/content/res/ColorStateList;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setTextColor(Landroid/content/res/ColorStateList;)V

    goto/16 :goto_4

    :cond_a
    :try_start_1
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v0, "setupwizard bottom height applied unsuccessful : refId error"

    invoke-static {v0}, Lcom/osp/app/util/aq;->c(Ljava/lang/String;)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    goto/16 :goto_2

    :catch_0
    move-exception v0

    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "setupwizard bottom height applied unsuccessful : "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/osp/app/util/aq;->c(Ljava/lang/String;)V

    goto/16 :goto_2

    :cond_b
    move-object v1, v0

    goto/16 :goto_0
.end method

.method public final a(Landroid/view/View$OnClickListener;)V
    .locals 3

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 495
    iget-object v0, p0, Lcom/osp/app/signin/BottomSoftkeyLinearLayout;->a:Landroid/content/Context;

    invoke-static {v0}, Lcom/osp/app/util/r;->g(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/osp/app/signin/BottomSoftkeyLinearLayout;->h:Landroid/widget/LinearLayout;

    if-eqz v0, :cond_0

    .line 497
    iget-object v0, p0, Lcom/osp/app/signin/BottomSoftkeyLinearLayout;->g:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setClickable(Z)V

    .line 498
    iget-object v0, p0, Lcom/osp/app/signin/BottomSoftkeyLinearLayout;->g:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setFocusable(Z)V

    .line 500
    iget-object v0, p0, Lcom/osp/app/signin/BottomSoftkeyLinearLayout;->h:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setDuplicateParentStateEnabled(Z)V

    .line 501
    iget-object v0, p0, Lcom/osp/app/signin/BottomSoftkeyLinearLayout;->h:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v2}, Landroid/widget/LinearLayout;->setClickable(Z)V

    .line 502
    iget-object v0, p0, Lcom/osp/app/signin/BottomSoftkeyLinearLayout;->h:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v2}, Landroid/widget/LinearLayout;->setFocusable(Z)V

    .line 503
    iget-object v0, p0, Lcom/osp/app/signin/BottomSoftkeyLinearLayout;->h:Landroid/widget/LinearLayout;

    invoke-virtual {v0, p1}, Landroid/widget/LinearLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 508
    :goto_0
    return-void

    .line 506
    :cond_0
    iget-object v0, p0, Lcom/osp/app/signin/BottomSoftkeyLinearLayout;->g:Landroid/widget/LinearLayout;

    invoke-virtual {v0, p1}, Landroid/widget/LinearLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    goto :goto_0
.end method

.method public final a(Ljava/lang/Boolean;)V
    .locals 3

    .prologue
    .line 443
    iget-object v0, p0, Lcom/osp/app/signin/BottomSoftkeyLinearLayout;->g:Landroid/widget/LinearLayout;

    invoke-virtual {p1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setEnabled(Z)V

    .line 444
    iget-object v0, p0, Lcom/osp/app/signin/BottomSoftkeyLinearLayout;->h:Landroid/widget/LinearLayout;

    if-eqz v0, :cond_0

    .line 446
    iget-object v0, p0, Lcom/osp/app/signin/BottomSoftkeyLinearLayout;->h:Landroid/widget/LinearLayout;

    invoke-virtual {p1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setEnabled(Z)V

    .line 448
    :cond_0
    iget-object v0, p0, Lcom/osp/app/signin/BottomSoftkeyLinearLayout;->i:Landroid/widget/TextView;

    invoke-virtual {p1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setEnabled(Z)V

    .line 449
    iget-object v0, p0, Lcom/osp/app/signin/BottomSoftkeyLinearLayout;->g:Landroid/widget/LinearLayout;

    invoke-virtual {p1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setClickable(Z)V

    .line 450
    iget-object v0, p0, Lcom/osp/app/signin/BottomSoftkeyLinearLayout;->i:Landroid/widget/TextView;

    invoke-virtual {v0}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 452
    iget-object v0, p0, Lcom/osp/app/signin/BottomSoftkeyLinearLayout;->g:Landroid/widget/LinearLayout;

    invoke-virtual {p1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setFocusable(Z)V

    .line 454
    :cond_1
    iget-object v0, p0, Lcom/osp/app/signin/BottomSoftkeyLinearLayout;->n:Landroid/widget/ImageView;

    invoke-static {v0, p1}, Lcom/msc/sa/c/d;->a(Landroid/widget/ImageView;Ljava/lang/Boolean;)V

    .line 455
    iget-object v0, p0, Lcom/osp/app/signin/BottomSoftkeyLinearLayout;->a:Landroid/content/Context;

    iget-object v1, p0, Lcom/osp/app/signin/BottomSoftkeyLinearLayout;->i:Landroid/widget/TextView;

    iget-object v2, p0, Lcom/osp/app/signin/BottomSoftkeyLinearLayout;->p:Ljava/lang/Boolean;

    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v2

    invoke-static {v0, v1, v2}, Lcom/msc/sa/c/d;->a(Landroid/content/Context;Landroid/view/View;Z)V

    .line 456
    return-void
.end method

.method public final a(Ljava/lang/String;)V
    .locals 7

    .prologue
    const/4 v6, 0x5

    const/4 v5, 0x0

    const v4, 0x7f0200f9

    const/16 v2, 0x8

    const/4 v3, 0x0

    .line 267
    const/4 v0, 0x0

    .line 268
    iget-object v1, p0, Lcom/osp/app/signin/BottomSoftkeyLinearLayout;->a:Landroid/content/Context;

    instance-of v1, v1, Lcom/osp/app/util/BaseActivity;

    if-eqz v1, :cond_0

    .line 270
    iget-object v0, p0, Lcom/osp/app/signin/BottomSoftkeyLinearLayout;->a:Landroid/content/Context;

    check-cast v0, Lcom/osp/app/util/BaseActivity;

    invoke-static {v0}, Lcom/osp/app/util/an;->a(Lcom/osp/app/util/BaseActivity;)Lcom/osp/app/util/an;

    move-result-object v0

    .line 273
    :cond_0
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_1

    if-eqz v0, :cond_1

    .line 275
    iput-object p1, p0, Lcom/osp/app/signin/BottomSoftkeyLinearLayout;->b:Ljava/lang/String;

    .line 277
    invoke-virtual {p0}, Lcom/osp/app/signin/BottomSoftkeyLinearLayout;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f080084

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v0

    float-to-int v0, v0

    .line 278
    const-string v1, "help_button"

    invoke-virtual {p1, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_7

    .line 280
    iget-object v1, p0, Lcom/osp/app/signin/BottomSoftkeyLinearLayout;->c:Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    if-eqz v1, :cond_2

    .line 282
    iget-object v1, p0, Lcom/osp/app/signin/BottomSoftkeyLinearLayout;->f:Landroid/widget/LinearLayout;

    invoke-virtual {v1, v3}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 283
    iget-object v1, p0, Lcom/osp/app/signin/BottomSoftkeyLinearLayout;->o:Landroid/view/View;

    invoke-virtual {v1, v2}, Landroid/view/View;->setVisibility(I)V

    .line 284
    iget-object v1, p0, Lcom/osp/app/signin/BottomSoftkeyLinearLayout;->f:Landroid/widget/LinearLayout;

    invoke-virtual {v1, v3}, Landroid/widget/LinearLayout;->setBackgroundResource(I)V

    .line 285
    iget-object v1, p0, Lcom/osp/app/signin/BottomSoftkeyLinearLayout;->g:Landroid/widget/LinearLayout;

    invoke-virtual {v1, v3}, Landroid/widget/LinearLayout;->setBackgroundResource(I)V

    .line 286
    iget-object v1, p0, Lcom/osp/app/signin/BottomSoftkeyLinearLayout;->j:Landroid/widget/LinearLayout;

    invoke-virtual {v1, v3}, Landroid/widget/LinearLayout;->setBackgroundResource(I)V

    .line 288
    iget-object v1, p0, Lcom/osp/app/signin/BottomSoftkeyLinearLayout;->j:Landroid/widget/LinearLayout;

    invoke-static {v1, v6}, Lcom/msc/sa/c/d;->a(Landroid/view/View;I)V

    .line 291
    iget-object v1, p0, Lcom/osp/app/signin/BottomSoftkeyLinearLayout;->j:Landroid/widget/LinearLayout;

    const/16 v2, 0x1e

    invoke-static {v1, v3, v0, v2, v3}, Lcom/msc/sa/c/d;->a(Landroid/view/View;IIII)V

    .line 292
    iget-object v0, p0, Lcom/osp/app/signin/BottomSoftkeyLinearLayout;->l:Landroid/widget/TextView;

    invoke-virtual {p0}, Lcom/osp/app/signin/BottomSoftkeyLinearLayout;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-static {v4}, Lcom/osp/app/util/an;->a(I)I

    move-result v2

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getColorStateList(I)Landroid/content/res/ColorStateList;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setTextColor(Landroid/content/res/ColorStateList;)V

    .line 333
    :goto_0
    invoke-static {v3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/osp/app/signin/BottomSoftkeyLinearLayout;->a(Ljava/lang/Boolean;)V

    .line 334
    iget-object v0, p0, Lcom/osp/app/signin/BottomSoftkeyLinearLayout;->a:Landroid/content/Context;

    invoke-static {v0}, Lcom/osp/app/util/r;->k(Landroid/content/Context;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 336
    iget-object v0, p0, Lcom/osp/app/signin/BottomSoftkeyLinearLayout;->g:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v3}, Landroid/widget/LinearLayout;->setBackgroundResource(I)V

    .line 411
    :cond_1
    :goto_1
    return-void

    .line 295
    :cond_2
    invoke-direct {p0}, Lcom/osp/app/signin/BottomSoftkeyLinearLayout;->f()V

    .line 296
    invoke-static {}, Lcom/osp/device/b;->b()Lcom/osp/device/b;

    move-result-object v0

    iget-object v1, p0, Lcom/osp/app/signin/BottomSoftkeyLinearLayout;->a:Landroid/content/Context;

    invoke-virtual {v0, v1}, Lcom/osp/device/b;->i(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 298
    iget-object v0, p0, Lcom/osp/app/signin/BottomSoftkeyLinearLayout;->a:Landroid/content/Context;

    invoke-static {v0}, Lcom/osp/app/util/r;->l(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 300
    iget-object v0, p0, Lcom/osp/app/signin/BottomSoftkeyLinearLayout;->f:Landroid/widget/LinearLayout;

    const v1, 0x7f0200dc

    invoke-static {v1}, Lcom/osp/app/util/an;->a(I)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setBackgroundResource(I)V

    .line 301
    iget-object v0, p0, Lcom/osp/app/signin/BottomSoftkeyLinearLayout;->o:Landroid/view/View;

    const v1, 0x7f0200f8

    invoke-static {v1}, Lcom/osp/app/util/an;->a(I)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/view/View;->setBackgroundResource(I)V

    .line 302
    iget-object v0, p0, Lcom/osp/app/signin/BottomSoftkeyLinearLayout;->g:Landroid/widget/LinearLayout;

    const v1, 0x7f0200ef

    invoke-static {v1}, Lcom/osp/app/util/an;->a(I)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setBackgroundResource(I)V

    .line 303
    iget-object v0, p0, Lcom/osp/app/signin/BottomSoftkeyLinearLayout;->i:Landroid/widget/TextView;

    invoke-virtual {p0}, Lcom/osp/app/signin/BottomSoftkeyLinearLayout;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0200fb

    invoke-static {v2}, Lcom/osp/app/util/an;->a(I)I

    move-result v2

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getColorStateList(I)Landroid/content/res/ColorStateList;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setTextColor(Landroid/content/res/ColorStateList;)V

    .line 304
    iget-object v0, p0, Lcom/osp/app/signin/BottomSoftkeyLinearLayout;->j:Landroid/widget/LinearLayout;

    const v1, 0x7f0200ef

    invoke-static {v1}, Lcom/osp/app/util/an;->a(I)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setBackgroundResource(I)V

    .line 305
    iget-object v0, p0, Lcom/osp/app/signin/BottomSoftkeyLinearLayout;->l:Landroid/widget/TextView;

    invoke-virtual {p0}, Lcom/osp/app/signin/BottomSoftkeyLinearLayout;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0200fb

    invoke-static {v2}, Lcom/osp/app/util/an;->a(I)I

    move-result v2

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getColorStateList(I)Landroid/content/res/ColorStateList;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setTextColor(Landroid/content/res/ColorStateList;)V

    .line 329
    :cond_3
    :goto_2
    iget-object v0, p0, Lcom/osp/app/signin/BottomSoftkeyLinearLayout;->j:Landroid/widget/LinearLayout;

    invoke-static {v0, v3, v3, v3, v3}, Lcom/msc/sa/c/d;->a(Landroid/view/View;IIII)V

    .line 330
    iget-object v0, p0, Lcom/osp/app/signin/BottomSoftkeyLinearLayout;->l:Landroid/widget/TextView;

    invoke-virtual {v0, v5, v5, v5, v3}, Landroid/widget/TextView;->setShadowLayer(FFFI)V

    goto/16 :goto_0

    .line 309
    :cond_4
    iget-object v0, p0, Lcom/osp/app/signin/BottomSoftkeyLinearLayout;->a:Landroid/content/Context;

    invoke-static {v0}, Lcom/osp/app/util/r;->m(Landroid/content/Context;)Z

    move-result v0

    if-nez v0, :cond_5

    iget-object v0, p0, Lcom/osp/app/signin/BottomSoftkeyLinearLayout;->p:Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 311
    :cond_5
    iget-object v0, p0, Lcom/osp/app/signin/BottomSoftkeyLinearLayout;->a:Landroid/content/Context;

    invoke-static {v0}, Lcom/osp/app/util/r;->g(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_6

    .line 313
    iget-object v0, p0, Lcom/osp/app/signin/BottomSoftkeyLinearLayout;->f:Landroid/widget/LinearLayout;

    const v1, 0x7f0200d7

    invoke-static {v1}, Lcom/osp/app/util/an;->a(I)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setBackgroundResource(I)V

    .line 314
    iget-object v0, p0, Lcom/osp/app/signin/BottomSoftkeyLinearLayout;->o:Landroid/view/View;

    const v1, 0x7f0200f4

    invoke-static {v1}, Lcom/osp/app/util/an;->a(I)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/view/View;->setBackgroundResource(I)V

    .line 320
    :goto_3
    iget-object v0, p0, Lcom/osp/app/signin/BottomSoftkeyLinearLayout;->g:Landroid/widget/LinearLayout;

    const v1, 0x7f0200ed

    invoke-static {v1}, Lcom/osp/app/util/an;->a(I)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setBackgroundResource(I)V

    .line 321
    iget-object v0, p0, Lcom/osp/app/signin/BottomSoftkeyLinearLayout;->i:Landroid/widget/TextView;

    invoke-virtual {p0}, Lcom/osp/app/signin/BottomSoftkeyLinearLayout;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-static {v4}, Lcom/osp/app/util/an;->a(I)I

    move-result v2

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getColorStateList(I)Landroid/content/res/ColorStateList;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setTextColor(Landroid/content/res/ColorStateList;)V

    .line 322
    iget-object v0, p0, Lcom/osp/app/signin/BottomSoftkeyLinearLayout;->j:Landroid/widget/LinearLayout;

    const v1, 0x7f0200ed

    invoke-static {v1}, Lcom/osp/app/util/an;->a(I)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setBackgroundResource(I)V

    .line 323
    iget-object v0, p0, Lcom/osp/app/signin/BottomSoftkeyLinearLayout;->l:Landroid/widget/TextView;

    invoke-virtual {p0}, Lcom/osp/app/signin/BottomSoftkeyLinearLayout;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-static {v4}, Lcom/osp/app/util/an;->a(I)I

    move-result v2

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getColorStateList(I)Landroid/content/res/ColorStateList;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setTextColor(Landroid/content/res/ColorStateList;)V

    goto :goto_2

    .line 317
    :cond_6
    iget-object v0, p0, Lcom/osp/app/signin/BottomSoftkeyLinearLayout;->f:Landroid/widget/LinearLayout;

    const v1, 0x7f0200d8

    invoke-static {v1}, Lcom/osp/app/util/an;->a(I)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setBackgroundResource(I)V

    .line 318
    iget-object v0, p0, Lcom/osp/app/signin/BottomSoftkeyLinearLayout;->o:Landroid/view/View;

    const v1, 0x7f0200f5

    invoke-static {v1}, Lcom/osp/app/util/an;->a(I)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/view/View;->setBackgroundResource(I)V

    goto :goto_3

    .line 339
    :cond_7
    const-string v1, "skip_button"

    invoke-virtual {p1, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_9

    .line 341
    iget-object v1, p0, Lcom/osp/app/signin/BottomSoftkeyLinearLayout;->c:Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    if-eqz v1, :cond_8

    .line 343
    iget-object v1, p0, Lcom/osp/app/signin/BottomSoftkeyLinearLayout;->f:Landroid/widget/LinearLayout;

    invoke-virtual {v1, v3}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 344
    iget-object v1, p0, Lcom/osp/app/signin/BottomSoftkeyLinearLayout;->o:Landroid/view/View;

    invoke-virtual {v1, v2}, Landroid/view/View;->setVisibility(I)V

    .line 345
    iget-object v1, p0, Lcom/osp/app/signin/BottomSoftkeyLinearLayout;->f:Landroid/widget/LinearLayout;

    invoke-virtual {v1, v3}, Landroid/widget/LinearLayout;->setBackgroundResource(I)V

    .line 346
    iget-object v1, p0, Lcom/osp/app/signin/BottomSoftkeyLinearLayout;->g:Landroid/widget/LinearLayout;

    invoke-virtual {v1, v3}, Landroid/widget/LinearLayout;->setBackgroundResource(I)V

    .line 347
    iget-object v1, p0, Lcom/osp/app/signin/BottomSoftkeyLinearLayout;->j:Landroid/widget/LinearLayout;

    invoke-virtual {v1, v3}, Landroid/widget/LinearLayout;->setBackgroundResource(I)V

    .line 350
    iget-object v1, p0, Lcom/osp/app/signin/BottomSoftkeyLinearLayout;->j:Landroid/widget/LinearLayout;

    const/16 v2, 0x1e

    invoke-static {v1, v3, v0, v2, v3}, Lcom/msc/sa/c/d;->a(Landroid/view/View;IIII)V

    .line 352
    iget-object v0, p0, Lcom/osp/app/signin/BottomSoftkeyLinearLayout;->j:Landroid/widget/LinearLayout;

    invoke-static {v0, v6}, Lcom/msc/sa/c/d;->a(Landroid/view/View;I)V

    .line 353
    iget-object v0, p0, Lcom/osp/app/signin/BottomSoftkeyLinearLayout;->l:Landroid/widget/TextView;

    invoke-virtual {p0}, Lcom/osp/app/signin/BottomSoftkeyLinearLayout;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-static {v4}, Lcom/osp/app/util/an;->a(I)I

    move-result v2

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getColorStateList(I)Landroid/content/res/ColorStateList;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setTextColor(Landroid/content/res/ColorStateList;)V

    .line 361
    :goto_4
    invoke-static {v3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/osp/app/signin/BottomSoftkeyLinearLayout;->a(Ljava/lang/Boolean;)V

    .line 362
    iget-object v0, p0, Lcom/osp/app/signin/BottomSoftkeyLinearLayout;->a:Landroid/content/Context;

    invoke-static {v0}, Lcom/osp/app/util/r;->k(Landroid/content/Context;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 364
    iget-object v0, p0, Lcom/osp/app/signin/BottomSoftkeyLinearLayout;->g:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v3}, Landroid/widget/LinearLayout;->setBackgroundResource(I)V

    goto/16 :goto_1

    .line 356
    :cond_8
    invoke-direct {p0}, Lcom/osp/app/signin/BottomSoftkeyLinearLayout;->f()V

    .line 358
    iget-object v0, p0, Lcom/osp/app/signin/BottomSoftkeyLinearLayout;->j:Landroid/widget/LinearLayout;

    invoke-static {v0, v3, v3, v3, v3}, Lcom/msc/sa/c/d;->a(Landroid/view/View;IIII)V

    goto :goto_4

    .line 367
    :cond_9
    const-string v1, "next_button"

    invoke-virtual {p1, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_b

    .line 369
    iget-object v1, p0, Lcom/osp/app/signin/BottomSoftkeyLinearLayout;->c:Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    if-eqz v1, :cond_a

    .line 371
    iget-object v1, p0, Lcom/osp/app/signin/BottomSoftkeyLinearLayout;->f:Landroid/widget/LinearLayout;

    invoke-virtual {v1, v3}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 372
    iget-object v1, p0, Lcom/osp/app/signin/BottomSoftkeyLinearLayout;->o:Landroid/view/View;

    invoke-virtual {v1, v2}, Landroid/view/View;->setVisibility(I)V

    .line 373
    iget-object v1, p0, Lcom/osp/app/signin/BottomSoftkeyLinearLayout;->f:Landroid/widget/LinearLayout;

    invoke-virtual {v1, v3}, Landroid/widget/LinearLayout;->setBackgroundResource(I)V

    .line 374
    iget-object v1, p0, Lcom/osp/app/signin/BottomSoftkeyLinearLayout;->g:Landroid/widget/LinearLayout;

    invoke-virtual {v1, v3}, Landroid/widget/LinearLayout;->setBackgroundResource(I)V

    .line 375
    iget-object v1, p0, Lcom/osp/app/signin/BottomSoftkeyLinearLayout;->j:Landroid/widget/LinearLayout;

    invoke-virtual {v1, v3}, Landroid/widget/LinearLayout;->setBackgroundResource(I)V

    .line 378
    iget-object v1, p0, Lcom/osp/app/signin/BottomSoftkeyLinearLayout;->j:Landroid/widget/LinearLayout;

    const/16 v2, 0x1e

    invoke-static {v1, v3, v0, v2, v3}, Lcom/msc/sa/c/d;->a(Landroid/view/View;IIII)V

    .line 380
    iget-object v0, p0, Lcom/osp/app/signin/BottomSoftkeyLinearLayout;->j:Landroid/widget/LinearLayout;

    invoke-static {v0, v6}, Lcom/msc/sa/c/d;->a(Landroid/view/View;I)V

    .line 381
    iget-object v0, p0, Lcom/osp/app/signin/BottomSoftkeyLinearLayout;->l:Landroid/widget/TextView;

    invoke-virtual {p0}, Lcom/osp/app/signin/BottomSoftkeyLinearLayout;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-static {v4}, Lcom/osp/app/util/an;->a(I)I

    move-result v2

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getColorStateList(I)Landroid/content/res/ColorStateList;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setTextColor(Landroid/content/res/ColorStateList;)V

    .line 389
    :goto_5
    invoke-static {v3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/osp/app/signin/BottomSoftkeyLinearLayout;->a(Ljava/lang/Boolean;)V

    .line 390
    iget-object v0, p0, Lcom/osp/app/signin/BottomSoftkeyLinearLayout;->a:Landroid/content/Context;

    invoke-static {v0}, Lcom/osp/app/util/r;->k(Landroid/content/Context;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 392
    iget-object v0, p0, Lcom/osp/app/signin/BottomSoftkeyLinearLayout;->g:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v3}, Landroid/widget/LinearLayout;->setBackgroundResource(I)V

    goto/16 :goto_1

    .line 384
    :cond_a
    invoke-direct {p0}, Lcom/osp/app/signin/BottomSoftkeyLinearLayout;->f()V

    .line 386
    iget-object v0, p0, Lcom/osp/app/signin/BottomSoftkeyLinearLayout;->j:Landroid/widget/LinearLayout;

    invoke-static {v0, v3, v3, v3, v3}, Lcom/msc/sa/c/d;->a(Landroid/view/View;IIII)V

    goto :goto_5

    .line 395
    :cond_b
    const-string v0, "one_button"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 397
    iget-object v0, p0, Lcom/osp/app/signin/BottomSoftkeyLinearLayout;->f:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v3}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 398
    iget-object v0, p0, Lcom/osp/app/signin/BottomSoftkeyLinearLayout;->o:Landroid/view/View;

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    .line 399
    iget-object v0, p0, Lcom/osp/app/signin/BottomSoftkeyLinearLayout;->g:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v2}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 400
    iget-object v0, p0, Lcom/osp/app/signin/BottomSoftkeyLinearLayout;->a:Landroid/content/Context;

    invoke-static {v0}, Lcom/osp/app/util/r;->k(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_c

    .line 402
    iget-object v0, p0, Lcom/osp/app/signin/BottomSoftkeyLinearLayout;->j:Landroid/widget/LinearLayout;

    const v1, 0x7f020019

    invoke-static {v1}, Lcom/osp/app/util/an;->a(I)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setBackgroundResource(I)V

    .line 405
    :cond_c
    iget-object v0, p0, Lcom/osp/app/signin/BottomSoftkeyLinearLayout;->a:Landroid/content/Context;

    invoke-static {v0}, Lcom/osp/app/util/r;->g(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 407
    iget-object v0, p0, Lcom/osp/app/signin/BottomSoftkeyLinearLayout;->j:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v3}, Landroid/widget/LinearLayout;->setBackgroundResource(I)V

    goto/16 :goto_1
.end method

.method public final b()Ljava/lang/Boolean;
    .locals 1

    .prologue
    .line 488
    iget-object v0, p0, Lcom/osp/app/signin/BottomSoftkeyLinearLayout;->j:Landroid/widget/LinearLayout;

    invoke-virtual {v0}, Landroid/widget/LinearLayout;->isEnabled()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    return-object v0
.end method

.method public final b(Landroid/view/View$OnClickListener;)V
    .locals 5

    .prologue
    const/4 v4, 0x5

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 515
    iget-object v0, p0, Lcom/osp/app/signin/BottomSoftkeyLinearLayout;->a:Landroid/content/Context;

    invoke-static {v0}, Lcom/osp/app/util/r;->g(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/osp/app/signin/BottomSoftkeyLinearLayout;->j:Landroid/widget/LinearLayout;

    if-eqz v0, :cond_1

    .line 517
    iget-object v0, p0, Lcom/osp/app/signin/BottomSoftkeyLinearLayout;->j:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v2}, Landroid/widget/LinearLayout;->setClickable(Z)V

    .line 518
    iget-object v0, p0, Lcom/osp/app/signin/BottomSoftkeyLinearLayout;->j:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v2}, Landroid/widget/LinearLayout;->setFocusable(Z)V

    .line 520
    iget-object v0, p0, Lcom/osp/app/signin/BottomSoftkeyLinearLayout;->k:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v2}, Landroid/widget/LinearLayout;->setDuplicateParentStateEnabled(Z)V

    .line 521
    iget-object v0, p0, Lcom/osp/app/signin/BottomSoftkeyLinearLayout;->k:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v3}, Landroid/widget/LinearLayout;->setClickable(Z)V

    .line 522
    iget-object v0, p0, Lcom/osp/app/signin/BottomSoftkeyLinearLayout;->k:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v3}, Landroid/widget/LinearLayout;->setFocusable(Z)V

    .line 523
    iget-object v0, p0, Lcom/osp/app/signin/BottomSoftkeyLinearLayout;->k:Landroid/widget/LinearLayout;

    invoke-virtual {v0, p1}, Landroid/widget/LinearLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 543
    :cond_0
    :goto_0
    return-void

    .line 526
    :cond_1
    iget-object v0, p0, Lcom/osp/app/signin/BottomSoftkeyLinearLayout;->j:Landroid/widget/LinearLayout;

    invoke-virtual {v0, p1}, Landroid/widget/LinearLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 527
    iget-object v0, p0, Lcom/osp/app/signin/BottomSoftkeyLinearLayout;->c:Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/osp/app/signin/BottomSoftkeyLinearLayout;->b:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 529
    iget-object v0, p0, Lcom/osp/app/signin/BottomSoftkeyLinearLayout;->b:Ljava/lang/String;

    const-string v1, "help_button"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_2

    iget-object v0, p0, Lcom/osp/app/signin/BottomSoftkeyLinearLayout;->b:Ljava/lang/String;

    const-string v1, "next_button"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_2

    iget-object v0, p0, Lcom/osp/app/signin/BottomSoftkeyLinearLayout;->b:Ljava/lang/String;

    const-string v1, "skip_button"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 531
    :cond_2
    iget-object v0, p0, Lcom/osp/app/signin/BottomSoftkeyLinearLayout;->j:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v2}, Landroid/widget/LinearLayout;->setClickable(Z)V

    .line 532
    iget-object v0, p0, Lcom/osp/app/signin/BottomSoftkeyLinearLayout;->j:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v2}, Landroid/widget/LinearLayout;->setFocusable(Z)V

    .line 534
    iget-object v0, p0, Lcom/osp/app/signin/BottomSoftkeyLinearLayout;->k:Landroid/widget/LinearLayout;

    invoke-static {v0, v4, v4, v4, v4}, Lcom/msc/sa/c/d;->a(Landroid/view/View;IIII)V

    .line 535
    iget-object v0, p0, Lcom/osp/app/signin/BottomSoftkeyLinearLayout;->k:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v2}, Landroid/widget/LinearLayout;->setDuplicateParentStateEnabled(Z)V

    .line 536
    iget-object v0, p0, Lcom/osp/app/signin/BottomSoftkeyLinearLayout;->k:Landroid/widget/LinearLayout;

    const v1, 0x7f020089

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setBackgroundResource(I)V

    .line 537
    iget-object v0, p0, Lcom/osp/app/signin/BottomSoftkeyLinearLayout;->k:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v3}, Landroid/widget/LinearLayout;->setClickable(Z)V

    .line 538
    iget-object v0, p0, Lcom/osp/app/signin/BottomSoftkeyLinearLayout;->k:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v3}, Landroid/widget/LinearLayout;->setFocusable(Z)V

    .line 539
    iget-object v0, p0, Lcom/osp/app/signin/BottomSoftkeyLinearLayout;->k:Landroid/widget/LinearLayout;

    invoke-virtual {v0, p1}, Landroid/widget/LinearLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    goto :goto_0
.end method

.method public final b(Ljava/lang/Boolean;)V
    .locals 3

    .prologue
    .line 462
    iget-object v0, p0, Lcom/osp/app/signin/BottomSoftkeyLinearLayout;->j:Landroid/widget/LinearLayout;

    invoke-virtual {p1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setEnabled(Z)V

    .line 463
    iget-object v0, p0, Lcom/osp/app/signin/BottomSoftkeyLinearLayout;->k:Landroid/widget/LinearLayout;

    if-eqz v0, :cond_0

    .line 465
    iget-object v0, p0, Lcom/osp/app/signin/BottomSoftkeyLinearLayout;->k:Landroid/widget/LinearLayout;

    invoke-virtual {p1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setEnabled(Z)V

    .line 467
    :cond_0
    iget-object v0, p0, Lcom/osp/app/signin/BottomSoftkeyLinearLayout;->l:Landroid/widget/TextView;

    invoke-virtual {p1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setEnabled(Z)V

    .line 468
    iget-object v0, p0, Lcom/osp/app/signin/BottomSoftkeyLinearLayout;->j:Landroid/widget/LinearLayout;

    invoke-virtual {p1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setClickable(Z)V

    .line 469
    iget-object v0, p0, Lcom/osp/app/signin/BottomSoftkeyLinearLayout;->l:Landroid/widget/TextView;

    invoke-virtual {v0}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 471
    iget-object v0, p0, Lcom/osp/app/signin/BottomSoftkeyLinearLayout;->j:Landroid/widget/LinearLayout;

    invoke-virtual {p1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setFocusable(Z)V

    .line 473
    :cond_1
    iget-object v0, p0, Lcom/osp/app/signin/BottomSoftkeyLinearLayout;->m:Landroid/widget/ImageView;

    invoke-static {v0, p1}, Lcom/msc/sa/c/d;->a(Landroid/widget/ImageView;Ljava/lang/Boolean;)V

    .line 474
    iget-object v0, p0, Lcom/osp/app/signin/BottomSoftkeyLinearLayout;->a:Landroid/content/Context;

    iget-object v1, p0, Lcom/osp/app/signin/BottomSoftkeyLinearLayout;->l:Landroid/widget/TextView;

    iget-object v2, p0, Lcom/osp/app/signin/BottomSoftkeyLinearLayout;->p:Ljava/lang/Boolean;

    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v2

    invoke-static {v0, v1, v2}, Lcom/msc/sa/c/d;->a(Landroid/content/Context;Landroid/view/View;Z)V

    .line 475
    return-void
.end method

.method public final b(Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 549
    iget-object v0, p0, Lcom/osp/app/signin/BottomSoftkeyLinearLayout;->i:Landroid/widget/TextView;

    invoke-direct {p0, p1, v0}, Lcom/osp/app/signin/BottomSoftkeyLinearLayout;->a(Ljava/lang/String;Landroid/widget/TextView;)V

    .line 550
    return-void
.end method

.method public final c()Ljava/lang/String;
    .locals 1

    .prologue
    .line 631
    iget-object v0, p0, Lcom/osp/app/signin/BottomSoftkeyLinearLayout;->l:Landroid/widget/TextView;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/osp/app/signin/BottomSoftkeyLinearLayout;->l:Landroid/widget/TextView;

    invoke-virtual {v0}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 633
    iget-object v0, p0, Lcom/osp/app/signin/BottomSoftkeyLinearLayout;->l:Landroid/widget/TextView;

    invoke-virtual {v0}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v0

    invoke-interface {v0}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v0

    .line 636
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final c(Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 557
    invoke-static {}, Lcom/osp/app/signin/SamsungService;->a()Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/osp/app/signin/BottomSoftkeyLinearLayout;->a:Landroid/content/Context;

    invoke-static {v0}, Lcom/osp/app/util/r;->g(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 559
    invoke-virtual {p0, p1}, Lcom/osp/app/signin/BottomSoftkeyLinearLayout;->f(Ljava/lang/String;)V

    .line 564
    :goto_0
    iget-object v0, p0, Lcom/osp/app/signin/BottomSoftkeyLinearLayout;->b:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 566
    invoke-direct {p0}, Lcom/osp/app/signin/BottomSoftkeyLinearLayout;->f()V

    .line 568
    :cond_0
    return-void

    .line 562
    :cond_1
    iget-object v0, p0, Lcom/osp/app/signin/BottomSoftkeyLinearLayout;->l:Landroid/widget/TextView;

    invoke-direct {p0, p1, v0}, Lcom/osp/app/signin/BottomSoftkeyLinearLayout;->a(Ljava/lang/String;Landroid/widget/TextView;)V

    goto :goto_0
.end method

.method public final d()Landroid/view/View;
    .locals 1

    .prologue
    .line 701
    iget-object v0, p0, Lcom/osp/app/signin/BottomSoftkeyLinearLayout;->g:Landroid/widget/LinearLayout;

    return-object v0
.end method

.method public final d(Ljava/lang/String;)V
    .locals 3

    .prologue
    .line 574
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p0}, Lcom/osp/app/signin/BottomSoftkeyLinearLayout;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0900c2

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 575
    iget-object v1, p0, Lcom/osp/app/signin/BottomSoftkeyLinearLayout;->i:Landroid/widget/TextView;

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 576
    return-void
.end method

.method public final e()Landroid/view/View;
    .locals 1

    .prologue
    .line 708
    iget-object v0, p0, Lcom/osp/app/signin/BottomSoftkeyLinearLayout;->j:Landroid/widget/LinearLayout;

    return-object v0
.end method

.method public final e(Ljava/lang/String;)V
    .locals 3

    .prologue
    .line 582
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p0}, Lcom/osp/app/signin/BottomSoftkeyLinearLayout;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0900c2

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 583
    iget-object v1, p0, Lcom/osp/app/signin/BottomSoftkeyLinearLayout;->l:Landroid/widget/TextView;

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 584
    return-void
.end method

.method public final f(Ljava/lang/String;)V
    .locals 3

    .prologue
    .line 591
    iget-object v0, p0, Lcom/osp/app/signin/BottomSoftkeyLinearLayout;->l:Landroid/widget/TextView;

    invoke-direct {p0, p1, v0}, Lcom/osp/app/signin/BottomSoftkeyLinearLayout;->a(Ljava/lang/String;Landroid/widget/TextView;)V

    .line 592
    iget-object v0, p0, Lcom/osp/app/signin/BottomSoftkeyLinearLayout;->m:Landroid/widget/ImageView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 593
    iget-object v0, p0, Lcom/osp/app/signin/BottomSoftkeyLinearLayout;->m:Landroid/widget/ImageView;

    iget v1, p0, Lcom/osp/app/signin/BottomSoftkeyLinearLayout;->e:I

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 594
    invoke-static {}, Lcom/osp/app/signin/SamsungService;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/osp/app/signin/BottomSoftkeyLinearLayout;->a:Landroid/content/Context;

    invoke-static {v0}, Lcom/osp/app/util/r;->g(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 596
    iget-object v0, p0, Lcom/osp/app/signin/BottomSoftkeyLinearLayout;->m:Landroid/widget/ImageView;

    const v1, 0x7f020120

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 597
    iget-object v0, p0, Lcom/osp/app/signin/BottomSoftkeyLinearLayout;->l:Landroid/widget/TextView;

    invoke-virtual {p0}, Lcom/osp/app/signin/BottomSoftkeyLinearLayout;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0200f3

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getColorStateList(I)Landroid/content/res/ColorStateList;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setTextColor(Landroid/content/res/ColorStateList;)V

    .line 601
    :cond_0
    iget-object v0, p0, Lcom/osp/app/signin/BottomSoftkeyLinearLayout;->a:Landroid/content/Context;

    iget-object v1, p0, Lcom/osp/app/signin/BottomSoftkeyLinearLayout;->m:Landroid/widget/ImageView;

    invoke-static {v0, v1}, Lcom/msc/sa/c/d;->a(Landroid/content/Context;Landroid/view/View;)V

    .line 603
    iget-object v0, p0, Lcom/osp/app/signin/BottomSoftkeyLinearLayout;->l:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/osp/app/signin/BottomSoftkeyLinearLayout;->m:Landroid/widget/ImageView;

    invoke-direct {p0, v0, v1}, Lcom/osp/app/signin/BottomSoftkeyLinearLayout;->a(Landroid/widget/TextView;Landroid/widget/ImageView;)V

    .line 604
    iget-object v0, p0, Lcom/osp/app/signin/BottomSoftkeyLinearLayout;->b:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 606
    invoke-direct {p0}, Lcom/osp/app/signin/BottomSoftkeyLinearLayout;->f()V

    .line 608
    :cond_1
    return-void
.end method

.method public final g(Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 615
    iget-object v0, p0, Lcom/osp/app/signin/BottomSoftkeyLinearLayout;->i:Landroid/widget/TextView;

    invoke-direct {p0, p1, v0}, Lcom/osp/app/signin/BottomSoftkeyLinearLayout;->a(Ljava/lang/String;Landroid/widget/TextView;)V

    .line 616
    iget-object v0, p0, Lcom/osp/app/signin/BottomSoftkeyLinearLayout;->n:Landroid/widget/ImageView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 617
    iget-object v0, p0, Lcom/osp/app/signin/BottomSoftkeyLinearLayout;->n:Landroid/widget/ImageView;

    iget v1, p0, Lcom/osp/app/signin/BottomSoftkeyLinearLayout;->d:I

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 619
    iget-object v0, p0, Lcom/osp/app/signin/BottomSoftkeyLinearLayout;->a:Landroid/content/Context;

    iget-object v1, p0, Lcom/osp/app/signin/BottomSoftkeyLinearLayout;->n:Landroid/widget/ImageView;

    invoke-static {v0, v1}, Lcom/msc/sa/c/d;->a(Landroid/content/Context;Landroid/view/View;)V

    .line 621
    iget-object v0, p0, Lcom/osp/app/signin/BottomSoftkeyLinearLayout;->i:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/osp/app/signin/BottomSoftkeyLinearLayout;->n:Landroid/widget/ImageView;

    invoke-direct {p0, v0, v1}, Lcom/osp/app/signin/BottomSoftkeyLinearLayout;->a(Landroid/widget/TextView;Landroid/widget/ImageView;)V

    .line 623
    return-void
.end method

.method protected onConfigurationChanged(Landroid/content/res/Configuration;)V
    .locals 1

    .prologue
    .line 252
    iget-object v0, p0, Lcom/osp/app/signin/BottomSoftkeyLinearLayout;->g:Landroid/widget/LinearLayout;

    if-eqz v0, :cond_0

    .line 254
    invoke-direct {p0, p1}, Lcom/osp/app/signin/BottomSoftkeyLinearLayout;->a(Landroid/content/res/Configuration;)V

    .line 256
    :cond_0
    invoke-super {p0, p1}, Landroid/widget/LinearLayout;->onConfigurationChanged(Landroid/content/res/Configuration;)V

    .line 257
    return-void
.end method
