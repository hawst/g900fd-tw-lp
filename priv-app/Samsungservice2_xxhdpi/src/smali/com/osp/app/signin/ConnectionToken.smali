.class public Lcom/osp/app/signin/ConnectionToken;
.super Lcom/osp/app/util/AbstractBaseService;
.source "ConnectionToken.java"


# instance fields
.field private a:Lcom/osp/app/util/n;

.field private b:Lcom/osp/app/signin/eg;

.field private c:Lcom/osp/app/signin/eh;

.field private d:Landroid/os/Looper;

.field private e:Lcom/msc/a/g;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 62
    invoke-direct {p0}, Lcom/osp/app/util/AbstractBaseService;-><init>()V

    .line 130
    return-void
.end method

.method private a(Ljava/lang/String;Ljava/lang/String;Lcom/osp/security/identity/d;)Landroid/os/Message;
    .locals 4

    .prologue
    .line 669
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v0, "CT"

    const-string v1, "AAA"

    const-string v2, "START"

    invoke-static {v0, v1, v2}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 670
    sget-object v1, Lcom/osp/app/signin/ei;->a:Lcom/osp/app/signin/ei;

    .line 671
    const-string v0, ""

    .line 676
    :try_start_0
    invoke-virtual {p3, p1, p2}, Lcom/osp/security/identity/d;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 682
    sget-object v1, Lcom/osp/app/signin/ei;->e:Lcom/osp/app/signin/ei;

    .line 683
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v2, "CT"

    const-string v3, "AAA success"

    invoke-static {v2, v3}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catch Lcom/osp/security/identity/IdentityException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_1

    .line 717
    :goto_0
    new-instance v2, Landroid/os/Message;

    invoke-direct {v2}, Landroid/os/Message;-><init>()V

    .line 718
    iput-object v1, v2, Landroid/os/Message;->obj:Ljava/lang/Object;

    .line 719
    new-instance v1, Landroid/os/Bundle;

    invoke-direct {v1}, Landroid/os/Bundle;-><init>()V

    .line 720
    const-string v3, "signInError"

    invoke-virtual {v1, v3, v0}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 721
    invoke-virtual {v2, v1}, Landroid/os/Message;->setData(Landroid/os/Bundle;)V

    .line 723
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v0, "CT"

    const-string v1, "AAA"

    const-string v3, "END"

    invoke-static {v0, v1, v3}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 724
    return-object v2

    .line 684
    :catch_0
    move-exception v0

    .line 686
    invoke-virtual {v0}, Lcom/osp/security/identity/IdentityException;->printStackTrace()V

    .line 688
    const-string v1, "SSO_2101"

    invoke-virtual {v0}, Lcom/osp/security/identity/IdentityException;->a()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 693
    invoke-direct {p0, p1}, Lcom/osp/app/signin/ConnectionToken;->a(Ljava/lang/String;)V

    .line 695
    invoke-virtual {v0}, Lcom/osp/security/identity/IdentityException;->printStackTrace()V

    .line 696
    iget-object v1, p0, Lcom/osp/app/signin/ConnectionToken;->a:Lcom/osp/app/util/n;

    invoke-virtual {v1, p0, v0}, Lcom/osp/app/util/n;->a(Landroid/content/Context;Lcom/osp/common/abstracts/AbstractOSPException;)Ljava/lang/String;

    move-result-object v0

    .line 697
    sget-object v1, Lcom/osp/app/signin/ei;->l:Lcom/osp/app/signin/ei;

    .line 698
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v2, "CT"

    const-string v3, "ETC ERROR"

    invoke-static {v2, v3}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 706
    :cond_0
    invoke-virtual {v0}, Lcom/osp/security/identity/IdentityException;->printStackTrace()V

    .line 707
    iget-object v1, p0, Lcom/osp/app/signin/ConnectionToken;->a:Lcom/osp/app/util/n;

    invoke-virtual {v1, p0, v0}, Lcom/osp/app/util/n;->a(Landroid/content/Context;Lcom/osp/common/abstracts/AbstractOSPException;)Ljava/lang/String;

    move-result-object v0

    .line 708
    sget-object v1, Lcom/osp/app/signin/ei;->l:Lcom/osp/app/signin/ei;

    .line 709
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v2, "CT"

    const-string v3, "ETC ERROR"

    invoke-static {v2, v3}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 711
    :catch_1
    move-exception v0

    move-object v2, v0

    .line 713
    iget-object v0, p0, Lcom/osp/app/signin/ConnectionToken;->a:Lcom/osp/app/util/n;

    const-string v3, "ERR_0000"

    invoke-virtual {v0, p0, v3}, Lcom/osp/app/util/n;->a(Landroid/content/Context;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 714
    invoke-virtual {v2}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_0
.end method

.method private a(Ljava/lang/String;Ljava/lang/String;Lcom/osp/social/member/d;)Landroid/os/Message;
    .locals 5

    .prologue
    .line 790
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v0, "CT"

    const-string v1, "AAR"

    const-string v2, "START"

    invoke-static {v0, v1, v2}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 791
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "AAR appID:"

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", appSecret:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;)V

    .line 792
    sget-object v1, Lcom/osp/app/signin/ei;->a:Lcom/osp/app/signin/ei;

    .line 793
    const-string v0, ""

    .line 797
    :try_start_0
    new-instance v2, Lcom/osp/social/member/a;

    invoke-direct {v2}, Lcom/osp/social/member/a;-><init>()V

    .line 798
    invoke-virtual {v2, p1}, Lcom/osp/social/member/a;->a(Ljava/lang/String;)V

    .line 799
    invoke-virtual {v2, p2}, Lcom/osp/social/member/a;->b(Ljava/lang/String;)V

    .line 801
    new-instance v3, Lcom/osp/social/member/b;

    invoke-direct {v3}, Lcom/osp/social/member/b;-><init>()V

    .line 802
    invoke-virtual {v2, v3}, Lcom/osp/social/member/a;->a(Lcom/osp/social/member/b;)V

    .line 804
    const-string v4, "N/A"

    invoke-virtual {v3, v4}, Lcom/osp/social/member/b;->a(Ljava/lang/String;)V

    .line 805
    const-string v4, "Device"

    invoke-virtual {v3, v4}, Lcom/osp/social/member/b;->b(Ljava/lang/String;)V

    .line 807
    new-instance v3, Lcom/osp/social/member/c;

    invoke-direct {v3}, Lcom/osp/social/member/c;-><init>()V

    .line 808
    const-string v4, ""

    invoke-virtual {v3, v4}, Lcom/osp/social/member/c;->a(Ljava/lang/String;)V

    .line 809
    const-string v4, ""

    invoke-virtual {v3, v4}, Lcom/osp/social/member/c;->b(Ljava/lang/String;)V

    .line 811
    invoke-virtual {p3, v2}, Lcom/osp/social/member/d;->a(Lcom/osp/social/member/a;)V

    .line 812
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v2, "CT"

    const-string v3, "AAR success"

    invoke-static {v2, v3}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 813
    sget-object v1, Lcom/osp/app/signin/ei;->c:Lcom/osp/app/signin/ei;
    :try_end_0
    .catch Lcom/osp/social/member/MemberServiceException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_1

    .line 842
    :goto_0
    new-instance v2, Landroid/os/Message;

    invoke-direct {v2}, Landroid/os/Message;-><init>()V

    .line 843
    iput-object v1, v2, Landroid/os/Message;->obj:Ljava/lang/Object;

    .line 844
    new-instance v1, Landroid/os/Bundle;

    invoke-direct {v1}, Landroid/os/Bundle;-><init>()V

    .line 845
    const-string v3, "signInError"

    invoke-virtual {v1, v3, v0}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 846
    invoke-virtual {v2, v1}, Landroid/os/Message;->setData(Landroid/os/Bundle;)V

    .line 848
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v0, "CT"

    const-string v1, "AAR"

    const-string v3, "END"

    invoke-static {v0, v1, v3}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 849
    return-object v2

    .line 814
    :catch_0
    move-exception v0

    move-object v2, v0

    .line 816
    const-string v0, "SSO_2101"

    invoke-virtual {v2}, Lcom/osp/social/member/MemberServiceException;->a()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 821
    invoke-direct {p0, p1}, Lcom/osp/app/signin/ConnectionToken;->a(Ljava/lang/String;)V

    .line 824
    invoke-virtual {v2}, Lcom/osp/social/member/MemberServiceException;->printStackTrace()V

    .line 825
    iget-object v0, p0, Lcom/osp/app/signin/ConnectionToken;->a:Lcom/osp/app/util/n;

    invoke-virtual {v0, p0, v2}, Lcom/osp/app/util/n;->a(Landroid/content/Context;Lcom/osp/common/abstracts/AbstractOSPException;)Ljava/lang/String;

    move-result-object v0

    .line 826
    sget-object v1, Lcom/osp/app/signin/ei;->l:Lcom/osp/app/signin/ei;

    .line 827
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v2, "CT"

    const-string v3, "ETC ERROR"

    invoke-static {v2, v3}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 831
    :cond_0
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v0, "CT"

    const-string v1, "ETC ERROR"

    invoke-static {v0, v1}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 832
    iget-object v0, p0, Lcom/osp/app/signin/ConnectionToken;->a:Lcom/osp/app/util/n;

    invoke-virtual {v0, p0, v2}, Lcom/osp/app/util/n;->a(Landroid/content/Context;Lcom/osp/common/abstracts/AbstractOSPException;)Ljava/lang/String;

    move-result-object v0

    .line 833
    sget-object v1, Lcom/osp/app/signin/ei;->l:Lcom/osp/app/signin/ei;

    .line 834
    invoke-virtual {v2}, Lcom/osp/social/member/MemberServiceException;->printStackTrace()V

    goto :goto_0

    .line 836
    :catch_1
    move-exception v0

    move-object v2, v0

    .line 838
    iget-object v0, p0, Lcom/osp/app/signin/ConnectionToken;->a:Lcom/osp/app/util/n;

    const-string v3, "ERR_0000"

    invoke-virtual {v0, p0, v3}, Lcom/osp/app/util/n;->a(Landroid/content/Context;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 839
    invoke-virtual {v2}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_0
.end method

.method static synthetic a(Lcom/osp/app/signin/ConnectionToken;)Lcom/msc/a/g;
    .locals 1

    .prologue
    .line 62
    iget-object v0, p0, Lcom/osp/app/signin/ConnectionToken;->e:Lcom/msc/a/g;

    return-object v0
.end method

.method static synthetic a(Lcom/osp/app/signin/ConnectionToken;Lcom/msc/a/g;)Lcom/msc/a/g;
    .locals 0

    .prologue
    .line 62
    iput-object p1, p0, Lcom/osp/app/signin/ConnectionToken;->e:Lcom/msc/a/g;

    return-object p1
.end method

.method private a()Ljava/lang/String;
    .locals 3

    .prologue
    .line 490
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v0, "CT"

    const-string v1, "isRequireEmailValidation"

    const-string v2, "START"

    invoke-static {v0, v1, v2}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 493
    invoke-static {}, Lcom/msc/c/g;->a()Lcom/msc/c/g;

    .line 494
    new-instance v0, Lcom/msc/a/f;

    invoke-direct {v0}, Lcom/msc/a/f;-><init>()V

    .line 495
    invoke-static {p0}, Lcom/osp/app/util/ad;->h(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/msc/a/f;->d(Ljava/lang/String;)V

    .line 496
    invoke-static {}, Lcom/osp/device/b;->b()Lcom/osp/device/b;

    move-result-object v1

    invoke-virtual {v1}, Lcom/osp/device/b;->a()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-static {p0}, Lcom/osp/app/util/ad;->h(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Landroid/telephony/PhoneNumberUtils;->isGlobalPhoneNumber(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 498
    invoke-static {p0}, Lcom/msc/c/e;->h(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v1

    .line 499
    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 501
    const-string v2, "Y"

    invoke-virtual {v0, v2}, Lcom/msc/a/f;->b(Ljava/lang/String;)V

    .line 502
    invoke-virtual {v0, v1}, Lcom/msc/a/f;->c(Ljava/lang/String;)V

    .line 508
    :cond_0
    new-instance v1, Lcom/osp/app/signin/ed;

    invoke-direct {v1, p0}, Lcom/osp/app/signin/ed;-><init>(Lcom/osp/app/signin/ConnectionToken;)V

    invoke-static {p0, v0, v1}, Lcom/msc/c/g;->a(Landroid/content/Context;Lcom/msc/a/f;Lcom/msc/b/h;)J

    move-result-wide v0

    .line 555
    sget-object v2, Lcom/msc/b/g;->c:Lcom/msc/b/g;

    invoke-static {v0, v1, v2}, Lcom/msc/c/g;->a(JLcom/msc/b/g;)V

    .line 557
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v0, "CT"

    const-string v1, "isRequireEmailValidation"

    const-string v2, "END"

    invoke-static {v0, v1, v2}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 558
    iget-object v0, p0, Lcom/osp/app/signin/ConnectionToken;->e:Lcom/msc/a/g;

    if-eqz v0, :cond_3

    .line 560
    invoke-static {}, Lcom/osp/device/b;->b()Lcom/osp/device/b;

    move-result-object v0

    invoke-virtual {v0}, Lcom/osp/device/b;->a()Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/osp/app/signin/ConnectionToken;->e:Lcom/msc/a/g;

    invoke-virtual {v0}, Lcom/msc/a/g;->a()Lcom/msc/c/f;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 562
    iget-object v0, p0, Lcom/osp/app/signin/ConnectionToken;->e:Lcom/msc/a/g;

    invoke-virtual {v0}, Lcom/msc/a/g;->a()Lcom/msc/c/f;

    move-result-object v0

    invoke-virtual {v0}, Lcom/msc/c/f;->a()Ljava/lang/String;

    move-result-object v0

    .line 569
    :goto_0
    return-object v0

    .line 565
    :cond_1
    iget-object v0, p0, Lcom/osp/app/signin/ConnectionToken;->e:Lcom/msc/a/g;

    invoke-virtual {v0}, Lcom/msc/a/g;->h()Z

    move-result v0

    if-eqz v0, :cond_2

    const-string v0, "required_email_validation"

    goto :goto_0

    :cond_2
    const-string v0, "completed_email_validation"

    goto :goto_0

    .line 569
    :cond_3
    const-string v0, "server_error"

    goto :goto_0
.end method

.method static synthetic a(Lcom/osp/app/signin/ConnectionToken;ILjava/lang/String;Ljava/lang/String;)V
    .locals 3

    .prologue
    .line 62
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v0, "CT"

    const-string v1, "onProcessSuccess"

    const-string v2, "START"

    invoke-static {v0, v1, v2}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    const-string v0, "DM_FACTORY_RESET"

    invoke-virtual {v0, p2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    const-string v1, "MODE"

    const-string v2, "DM_FACTORY_RESET"

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    const-string v1, "com.osp.app.signin"

    const-string v2, "com.osp.app.signin.WipeOutService"

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->setClassName(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    invoke-virtual {p0, v0}, Lcom/osp/app/signin/ConnectionToken;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;

    :cond_0
    :goto_0
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v0, "CT"

    const-string v1, "onProcessSuccess"

    const-string v2, "END"

    invoke-static {v0, v1, v2}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {p0, p1}, Lcom/osp/app/signin/ConnectionToken;->stopSelf(I)V

    return-void

    :cond_1
    const-string v0, "APP_REQUEST"

    invoke-virtual {v0, p2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    new-instance v0, Landroid/content/Intent;

    const-string v1, "com.osp.ACCESSTOKEN.RESPONSE"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    const-string v1, "APPID"

    invoke-virtual {v0, v1, p3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v1, "CT"

    const-string v2, "send ACCESSTOKEN.RESPONSE "

    invoke-static {v1, v2}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;)V

    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "onProcessSuccess appID=["

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " ]"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;)V

    invoke-virtual {p0, v0}, Lcom/osp/app/signin/ConnectionToken;->sendBroadcast(Landroid/content/Intent;)V

    goto :goto_0
.end method

.method static synthetic a(Lcom/osp/app/signin/ConnectionToken;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 3

    .prologue
    .line 62
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v0, "CT"

    const-string v1, "onRequireEmailValidation"

    const-string v2, "START"

    invoke-static {v0, v1, v2}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    const-string v0, "Y"

    invoke-virtual {v0, p4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-static {p0}, Lcom/osp/app/util/ad;->g(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v0

    new-instance v1, Landroid/content/Intent;

    const-class v2, Lcom/osp/app/signin/EmailValidationView;

    invoke-direct {v1, p0, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const/high16 v2, 0x10000000

    invoke-virtual {v1, v2}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    const-string v2, "email_id"

    invoke-virtual {v1, v2, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    const-string v0, "is_resend"

    const/4 v2, 0x1

    invoke-virtual {v1, v0, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    const-string v0, "is_bgmode"

    const/16 v2, 0xcb

    invoke-virtual {v1, v0, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    const-string v0, "key app id"

    invoke-virtual {v1, v0, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    const-string v0, "key app pass"

    invoke-virtual {v1, v0, p3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    const-string v0, "is_show_email_validation"

    invoke-virtual {v1, v0, p4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    invoke-virtual {p0, v1}, Lcom/osp/app/signin/ConnectionToken;->startActivity(Landroid/content/Intent;)V

    :goto_0
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v0, "CT"

    const-string v1, "onRequireEmailValidation"

    const-string v2, "END"

    invoke-static {v0, v1, v2}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {p0, p1}, Lcom/osp/app/signin/ConnectionToken;->stopSelf(I)V

    return-void

    :cond_0
    new-instance v0, Landroid/content/Intent;

    const-string v1, "com.osp.ACCESSTOKEN.FAIL"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    const-string v1, "APPID"

    invoke-virtual {v0, v1, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    const-string v1, "bg_result"

    const/16 v2, 0xd

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v1, "CT"

    const-string v2, "send intent com.osp.ACCESSTOKEN.FAIL for email validation"

    invoke-static {v1, v2}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {p0, v0}, Lcom/osp/app/signin/ConnectionToken;->sendBroadcast(Landroid/content/Intent;)V

    goto :goto_0
.end method

.method private a(Ljava/lang/String;)V
    .locals 7

    .prologue
    .line 735
    invoke-static {p0}, Lcom/osp/app/util/ad;->d(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 740
    invoke-static {p0}, Lcom/osp/app/util/ad;->h(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v0

    .line 742
    new-instance v1, Landroid/content/Intent;

    const-string v2, "com.msc.action.samsungaccount.resignin"

    invoke-direct {v1, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 743
    const/high16 v2, 0x10000000

    invoke-virtual {v1, v2}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    .line 745
    const-string v2, "email_id"

    invoke-virtual {v1, v2, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 752
    const-string v0, "OSP_VER"

    const-string v2, "OSP_01"

    invoke-virtual {v1, v0, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 760
    const-string v0, "tj9u972o46"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 762
    const-string v0, "from_notification"

    const/4 v2, 0x1

    invoke-virtual {v1, v0, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 764
    const/4 v0, 0x0

    const/high16 v2, 0x8000000

    invoke-static {p0, v0, v1, v2}, Landroid/app/PendingIntent;->getActivity(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v1

    .line 766
    const/4 v2, 0x0

    const v0, 0x7f09004f

    invoke-virtual {p0, v0}, Lcom/osp/app/signin/ConnectionToken;->getString(I)Ljava/lang/String;

    move-result-object v3

    const v0, 0x7f09008d

    invoke-virtual {p0, v0}, Lcom/osp/app/signin/ConnectionToken;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-static {}, Lcom/osp/app/signin/SamsungService;->i()I

    move-result v5

    const v6, 0x132df82

    move-object v0, p0

    invoke-static/range {v0 .. v6}, Lcom/msc/sa/c/d;->a(Landroid/content/Context;Landroid/app/PendingIntent;Landroid/app/PendingIntent;Ljava/lang/String;Ljava/lang/String;II)V

    .line 769
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v0, "CT"

    const-string v1, "Show session expired Notification"

    invoke-static {v0, v1}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 777
    :cond_0
    :goto_0
    return-void

    .line 772
    :cond_1
    invoke-virtual {p0, v1}, Lcom/osp/app/signin/ConnectionToken;->startActivity(Landroid/content/Intent;)V

    .line 774
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v0, "CT"

    const-string v1, "Show session expired Activity"

    invoke-static {v0, v1}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method static synthetic b(Lcom/osp/app/signin/ConnectionToken;ILjava/lang/String;Ljava/lang/String;)V
    .locals 3

    .prologue
    .line 62
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v0, "CT"

    const-string v1, "onProcessFailed"

    const-string v2, "START"

    invoke-static {v0, v1, v2}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    const-string v0, "DM_FACTORY_RESET"

    invoke-virtual {v0, p2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    const-string v1, "MODE"

    const-string v2, "DM_FACTORY_RESET"

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    const-string v1, "com.osp.app.signin"

    const-string v2, "com.osp.app.signin.WipeOutService"

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->setClassName(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    invoke-virtual {p0, v0}, Lcom/osp/app/signin/ConnectionToken;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;

    :cond_0
    :goto_0
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v0, "CT"

    const-string v1, "onProcessFailed"

    const-string v2, "END"

    invoke-static {v0, v1, v2}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {p0, p1}, Lcom/osp/app/signin/ConnectionToken;->stopSelf(I)V

    return-void

    :cond_1
    const-string v0, "APP_REQUEST"

    invoke-virtual {v0, p2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    new-instance v0, Landroid/content/Intent;

    const-string v1, "com.osp.ACCESSTOKEN.FAIL"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    const-string v1, "APPID"

    invoke-virtual {v0, v1, p3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v1, "CT"

    const-string v2, "send ACCESSTOKEN.FAIL "

    invoke-static {v1, v2}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;)V

    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "onProcessFailed appID=["

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " ]"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;)V

    invoke-virtual {p0, v0}, Lcom/osp/app/signin/ConnectionToken;->sendBroadcast(Landroid/content/Intent;)V

    goto :goto_0
.end method


# virtual methods
.method public final a(Landroid/os/Message;)V
    .locals 12

    .prologue
    const/4 v11, 0x1

    .line 267
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v0, "CT"

    const-string v1, "autosign"

    const-string v2, "START"

    invoke-static {v0, v1, v2}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 268
    sget-object v3, Lcom/osp/app/signin/ei;->a:Lcom/osp/app/signin/ei;

    .line 272
    iget v4, p1, Landroid/os/Message;->arg1:I

    .line 274
    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v0, Landroid/content/Intent;

    .line 275
    const-string v1, "APPID"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 276
    const-string v1, "APPSECRET"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 277
    const-string v5, "MODE"

    invoke-virtual {v0, v5}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    .line 278
    const-string v6, "is_show_email_validation"

    invoke-virtual {v0, v6}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    .line 280
    const-string v0, "DM_ACCOUNT_SIGNIN_CHECK"

    invoke-virtual {v0, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 282
    const-string v1, "14eev3f64b"

    .line 283
    const-string v0, "109E2830E09DB340924B8ABE0D6290C3"

    move-object v2, v1

    move-object v1, v0

    .line 285
    :cond_0
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v7, "mode : "

    invoke-direct {v0, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v7, ", startId : "

    invoke-virtual {v0, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v7, ", appID : "

    invoke-virtual {v0, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v7, ", appSecret : "

    invoke-virtual {v0, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v7, ", isShowEmailValidation :"

    invoke-virtual {v0, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;)V

    .line 288
    new-instance v0, Landroid/os/Message;

    invoke-direct {v0}, Landroid/os/Message;-><init>()V

    .line 289
    :try_start_0
    new-instance v7, Lcom/osp/security/identity/d;

    invoke-direct {v7, p0}, Lcom/osp/security/identity/d;-><init>(Landroid/content/Context;)V

    .line 297
    new-instance v8, Lcom/osp/social/member/d;

    invoke-direct {v8, p0}, Lcom/osp/social/member/d;-><init>(Landroid/content/Context;)V

    .line 299
    invoke-static {p0}, Lcom/osp/app/util/ad;->d(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_c

    const/4 v0, 0x1

    invoke-static {p0, v0}, Lcom/msc/c/c;->a(Landroid/content/Context;Z)Z

    move-result v0

    if-nez v0, :cond_c

    .line 301
    invoke-direct {p0, v2}, Lcom/osp/app/signin/ConnectionToken;->a(Ljava/lang/String;)V

    .line 302
    sget-object v0, Lcom/osp/app/signin/ei;->l:Lcom/osp/app/signin/ei;

    .line 305
    :goto_0
    sget-object v3, Lcom/osp/app/signin/ei;->j:Lcom/osp/app/signin/ei;

    if-eq v0, v3, :cond_3

    sget-object v3, Lcom/osp/app/signin/ei;->l:Lcom/osp/app/signin/ei;

    if-eq v0, v3, :cond_3

    sget-object v3, Lcom/osp/app/signin/ei;->m:Lcom/osp/app/signin/ei;

    if-eq v0, v3, :cond_3

    .line 307
    sget-object v3, Lcom/osp/app/signin/ee;->a:[I

    invoke-virtual {v0}, Lcom/osp/app/signin/ei;->ordinal()I

    move-result v9

    aget v3, v3, v9

    packed-switch v3, :pswitch_data_0

    goto :goto_0

    .line 310
    :pswitch_0
    if-eqz v6, :cond_1

    const-string v0, "required_email_validation"

    invoke-direct {p0}, Lcom/osp/app/signin/ConnectionToken;->a()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 312
    sget-object v0, Lcom/osp/app/signin/ei;->m:Lcom/osp/app/signin/ei;

    goto :goto_0

    .line 313
    :cond_1
    invoke-static {}, Lcom/osp/device/b;->b()Lcom/osp/device/b;

    move-result-object v0

    invoke-virtual {v0}, Lcom/osp/device/b;->a()Z

    move-result v0

    if-eqz v0, :cond_2

    const-string v0, "USR_3174"

    invoke-direct {p0}, Lcom/osp/app/signin/ConnectionToken;->a()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 317
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/osp/app/signin/ConnectionToken;->a(Ljava/lang/String;)V

    .line 318
    sget-object v0, Lcom/osp/app/signin/ei;->l:Lcom/osp/app/signin/ei;

    goto :goto_0

    .line 322
    :cond_2
    sget-object v0, Lcom/osp/app/signin/ei;->b:Lcom/osp/app/signin/ei;

    goto :goto_0

    .line 339
    :pswitch_1
    sget-object v0, Lcom/osp/app/signin/ei;->f:Lcom/osp/app/signin/ei;

    .line 340
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v3, "CT"

    const-string v9, "Start"

    invoke-static {v3, v9}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 438
    :catch_0
    move-exception v0

    move-object v3, v0

    .line 441
    sget-object v0, Lcom/osp/app/signin/ei;->l:Lcom/osp/app/signin/ei;

    .line 442
    invoke-virtual {v3}, Ljava/lang/Exception;->printStackTrace()V

    .line 445
    :cond_3
    iget-object v3, p0, Lcom/osp/app/signin/ConnectionToken;->b:Lcom/osp/app/signin/eg;

    if-eqz v3, :cond_4

    .line 447
    new-instance v3, Landroid/os/Message;

    invoke-direct {v3}, Landroid/os/Message;-><init>()V

    .line 448
    new-instance v7, Lcom/osp/app/signin/ef;

    const/4 v8, 0x0

    invoke-direct {v7, v8}, Lcom/osp/app/signin/ef;-><init>(B)V

    .line 449
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v8, "CT"

    const-string v9, "ResultMessageHandler"

    const-string v10, "START"

    invoke-static {v8, v9, v10}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 450
    sget-object v8, Lcom/osp/app/signin/ei;->j:Lcom/osp/app/signin/ei;

    if-ne v0, v8, :cond_9

    .line 452
    const/4 v0, -0x1

    iput v0, v3, Landroid/os/Message;->what:I

    .line 453
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v0, "CT"

    const-string v8, "SignInState.Success"

    invoke-static {v0, v8}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 471
    :goto_1
    invoke-static {v7, v4}, Lcom/osp/app/signin/ef;->a(Lcom/osp/app/signin/ef;I)I

    .line 472
    invoke-static {v7, v2}, Lcom/osp/app/signin/ef;->a(Lcom/osp/app/signin/ef;Ljava/lang/String;)Ljava/lang/String;

    .line 473
    invoke-static {v7, v1}, Lcom/osp/app/signin/ef;->b(Lcom/osp/app/signin/ef;Ljava/lang/String;)Ljava/lang/String;

    .line 474
    invoke-static {v7, v5}, Lcom/osp/app/signin/ef;->c(Lcom/osp/app/signin/ef;Ljava/lang/String;)Ljava/lang/String;

    .line 475
    invoke-static {v7, v6}, Lcom/osp/app/signin/ef;->d(Lcom/osp/app/signin/ef;Ljava/lang/String;)Ljava/lang/String;

    .line 476
    iput-object v7, v3, Landroid/os/Message;->obj:Ljava/lang/Object;

    .line 478
    iget-object v0, p0, Lcom/osp/app/signin/ConnectionToken;->b:Lcom/osp/app/signin/eg;

    invoke-virtual {v0, v3}, Lcom/osp/app/signin/eg;->sendMessage(Landroid/os/Message;)Z

    .line 479
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v0, "CT"

    const-string v1, "ResultMessageHandler"

    const-string v2, "END"

    invoke-static {v0, v1, v2}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 481
    :cond_4
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v0, "CT"

    const-string v1, "autosign"

    const-string v2, "END"

    invoke-static {v0, v1, v2}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 482
    return-void

    .line 344
    :pswitch_2
    :try_start_1
    sget-object v0, Lcom/osp/app/signin/ei;->j:Lcom/osp/app/signin/ei;

    .line 345
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v3, "CT"

    const-string v9, "A_Token is true"

    invoke-static {v3, v9}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 348
    :pswitch_3
    invoke-virtual {v8, v2}, Lcom/osp/social/member/d;->a(Ljava/lang/String;)Z

    move-result v0

    .line 349
    if-ne v0, v11, :cond_5

    .line 351
    sget-object v0, Lcom/osp/app/signin/ei;->c:Lcom/osp/app/signin/ei;

    .line 352
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v3, "CT"

    const-string v9, "No A_Token is true"

    invoke-static {v3, v9}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 355
    :cond_5
    sget-object v0, Lcom/osp/app/signin/ei;->d:Lcom/osp/app/signin/ei;

    .line 356
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v3, "CT"

    const-string v9, "No A_Token is false"

    invoke-static {v3, v9}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 360
    :pswitch_4
    invoke-virtual {v7}, Lcom/osp/security/identity/d;->i()Z

    move-result v0

    .line 361
    if-ne v0, v11, :cond_6

    .line 363
    invoke-direct {p0, v2, v1, v7}, Lcom/osp/app/signin/ConnectionToken;->a(Ljava/lang/String;Ljava/lang/String;Lcom/osp/security/identity/d;)Landroid/os/Message;

    move-result-object v3

    .line 364
    iget-object v0, v3, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v0, Lcom/osp/app/signin/ei;

    .line 365
    invoke-virtual {v3}, Landroid/os/Message;->getData()Landroid/os/Bundle;

    move-result-object v3

    .line 366
    const-string v9, "signInError"

    invoke-virtual {v3, v9}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    .line 367
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v3, "CT"

    const-string v9, "App is true"

    invoke-static {v3, v9}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 374
    :cond_6
    sget-object v0, Lcom/osp/app/signin/ei;->h:Lcom/osp/app/signin/ei;

    .line 375
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v3, "CT"

    const-string v9, "App is false"

    invoke-static {v3, v9}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 379
    :pswitch_5
    invoke-virtual {v7}, Lcom/osp/security/identity/d;->i()Z

    move-result v0

    .line 380
    if-ne v0, v11, :cond_7

    .line 382
    invoke-direct {p0, v2, v1, v8}, Lcom/osp/app/signin/ConnectionToken;->a(Ljava/lang/String;Ljava/lang/String;Lcom/osp/social/member/d;)Landroid/os/Message;

    move-result-object v3

    .line 383
    iget-object v0, v3, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v0, Lcom/osp/app/signin/ei;

    .line 384
    invoke-virtual {v3}, Landroid/os/Message;->getData()Landroid/os/Bundle;

    move-result-object v3

    .line 385
    const-string v9, "signInError"

    invoke-virtual {v3, v9}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    .line 386
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v3, "CT"

    const-string v9, "No App is true"

    invoke-static {v3, v9}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 393
    :cond_7
    sget-object v0, Lcom/osp/app/signin/ei;->h:Lcom/osp/app/signin/ei;

    .line 394
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v3, "CT"

    const-string v9, "No App is false"

    invoke-static {v3, v9}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 398
    :pswitch_6
    invoke-virtual {v8, v2}, Lcom/osp/social/member/d;->a(Ljava/lang/String;)Z

    move-result v0

    .line 399
    if-ne v0, v11, :cond_8

    .line 401
    sget-object v0, Lcom/osp/app/signin/ei;->c:Lcom/osp/app/signin/ei;

    .line 402
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v3, "CT"

    const-string v9, "B_Token is true"

    invoke-static {v3, v9}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 405
    :cond_8
    sget-object v0, Lcom/osp/app/signin/ei;->d:Lcom/osp/app/signin/ei;

    .line 406
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v3, "CT"

    const-string v9, "B_Token is false"

    invoke-static {v3, v9}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 410
    :pswitch_7
    invoke-direct {p0, v2}, Lcom/osp/app/signin/ConnectionToken;->a(Ljava/lang/String;)V

    .line 411
    sget-object v0, Lcom/osp/app/signin/ei;->l:Lcom/osp/app/signin/ei;
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    goto/16 :goto_0

    .line 454
    :cond_9
    sget-object v8, Lcom/osp/app/signin/ei;->l:Lcom/osp/app/signin/ei;

    if-ne v0, v8, :cond_a

    .line 456
    const/16 v0, 0x15

    iput v0, v3, Landroid/os/Message;->what:I

    .line 458
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v0, "CT"

    const-string v8, "SignInState.Failed"

    invoke-static {v0, v8}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_1

    .line 459
    :cond_a
    sget-object v8, Lcom/osp/app/signin/ei;->m:Lcom/osp/app/signin/ei;

    if-ne v0, v8, :cond_b

    .line 461
    const/16 v0, 0x17

    iput v0, v3, Landroid/os/Message;->what:I

    .line 463
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v0, "CT"

    const-string v8, "SignInState.RequireEmailValidation"

    invoke-static {v0, v8}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_1

    .line 466
    :cond_b
    iput v11, v3, Landroid/os/Message;->what:I

    .line 468
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v0, "CT"

    const-string v8, "State mode is etc"

    invoke-static {v0, v8}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_1

    :cond_c
    move-object v0, v3

    goto/16 :goto_0

    .line 307
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_7
    .end packed-switch
.end method

.method public onBind(Landroid/content/Intent;)Landroid/os/IBinder;
    .locals 1

    .prologue
    .line 576
    const-string v0, "onBind"

    invoke-virtual {p0, p1, v0}, Lcom/osp/app/signin/ConnectionToken;->a(Landroid/content/Intent;Ljava/lang/String;)V

    .line 577
    const/4 v0, 0x0

    return-object v0
.end method

.method public onCreate()V
    .locals 3

    .prologue
    .line 582
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v0, "CT"

    const-string v1, "onCreate"

    const-string v2, "START"

    invoke-static {v0, v1, v2}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 583
    invoke-super {p0}, Lcom/osp/app/util/AbstractBaseService;->onCreate()V

    .line 585
    new-instance v0, Landroid/os/HandlerThread;

    const-string v1, "ServiceStartArguments"

    const/16 v2, 0xa

    invoke-direct {v0, v1, v2}, Landroid/os/HandlerThread;-><init>(Ljava/lang/String;I)V

    .line 586
    invoke-virtual {v0}, Landroid/os/HandlerThread;->start()V

    .line 589
    invoke-virtual {v0}, Landroid/os/HandlerThread;->getLooper()Landroid/os/Looper;

    move-result-object v0

    iput-object v0, p0, Lcom/osp/app/signin/ConnectionToken;->d:Landroid/os/Looper;

    .line 590
    iget-object v0, p0, Lcom/osp/app/signin/ConnectionToken;->d:Landroid/os/Looper;

    if-eqz v0, :cond_0

    .line 592
    new-instance v0, Lcom/osp/app/signin/eh;

    iget-object v1, p0, Lcom/osp/app/signin/ConnectionToken;->d:Landroid/os/Looper;

    invoke-direct {v0, p0, v1}, Lcom/osp/app/signin/eh;-><init>(Lcom/osp/app/signin/ConnectionToken;Landroid/os/Looper;)V

    iput-object v0, p0, Lcom/osp/app/signin/ConnectionToken;->c:Lcom/osp/app/signin/eh;

    .line 595
    :cond_0
    new-instance v0, Lcom/osp/app/signin/eg;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/osp/app/signin/eg;-><init>(Lcom/osp/app/signin/ConnectionToken;B)V

    iput-object v0, p0, Lcom/osp/app/signin/ConnectionToken;->b:Lcom/osp/app/signin/eg;

    .line 597
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v0, "CT"

    const-string v1, "onCreate"

    const-string v2, "END"

    invoke-static {v0, v1, v2}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 598
    return-void
.end method

.method public onDestroy()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 602
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v0, "CT"

    const-string v1, "onDestroy"

    const-string v2, "START"

    invoke-static {v0, v1, v2}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 603
    iput-object v3, p0, Lcom/osp/app/signin/ConnectionToken;->b:Lcom/osp/app/signin/eg;

    .line 604
    iget-object v0, p0, Lcom/osp/app/signin/ConnectionToken;->d:Landroid/os/Looper;

    invoke-virtual {v0}, Landroid/os/Looper;->quit()V

    .line 605
    iput-object v3, p0, Lcom/osp/app/signin/ConnectionToken;->c:Lcom/osp/app/signin/eh;

    .line 607
    invoke-super {p0}, Lcom/osp/app/util/AbstractBaseService;->onDestroy()V

    .line 608
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v0, "CT"

    const-string v1, "onDestroy"

    const-string v2, "END"

    invoke-static {v0, v1, v2}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 609
    return-void
.end method

.method public onStartCommand(Landroid/content/Intent;II)I
    .locals 3

    .prologue
    .line 613
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v0, "CT"

    const-string v1, "onStartCommand"

    const-string v2, "START"

    invoke-static {v0, v1, v2}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 614
    if-eqz p1, :cond_0

    .line 626
    invoke-static {}, Lcom/osp/app/util/n;->a()Lcom/osp/app/util/n;

    move-result-object v0

    iput-object v0, p0, Lcom/osp/app/signin/ConnectionToken;->a:Lcom/osp/app/util/n;

    .line 642
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v0, "CT"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Has SA : "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-static {p0}, Lcom/osp/app/util/ad;->d(Landroid/content/Context;)Z

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 643
    iget-object v0, p0, Lcom/osp/app/signin/ConnectionToken;->c:Lcom/osp/app/signin/eh;

    invoke-virtual {v0}, Lcom/osp/app/signin/eh;->obtainMessage()Landroid/os/Message;

    move-result-object v0

    .line 644
    iput p3, v0, Landroid/os/Message;->arg1:I

    .line 645
    iput-object p1, v0, Landroid/os/Message;->obj:Ljava/lang/Object;

    .line 646
    iget-object v1, p0, Lcom/osp/app/signin/ConnectionToken;->c:Lcom/osp/app/signin/eh;

    invoke-virtual {v1, v0}, Lcom/osp/app/signin/eh;->sendMessage(Landroid/os/Message;)Z

    .line 648
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v0, "CT"

    const-string v1, "Connection token start"

    invoke-static {v0, v1}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 654
    :goto_0
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v0, "CT"

    const-string v1, "onStartCommand"

    const-string v2, "END"

    invoke-static {v0, v1, v2}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 655
    const/4 v0, 0x2

    return v0

    .line 652
    :cond_0
    invoke-virtual {p0, p3}, Lcom/osp/app/signin/ConnectionToken;->stopSelf(I)V

    goto :goto_0
.end method
