.class final Lcom/osp/app/signin/ay;
.super Lcom/msc/c/b;
.source "AccountView.java"


# instance fields
.field final synthetic c:Lcom/osp/app/signin/AccountView;

.field private d:J

.field private e:Lcom/msc/a/g;


# direct methods
.method public constructor <init>(Lcom/osp/app/signin/AccountView;)V
    .locals 2

    .prologue
    .line 6413
    iput-object p1, p0, Lcom/osp/app/signin/ay;->c:Lcom/osp/app/signin/AccountView;

    .line 6414
    invoke-direct {p0, p1}, Lcom/msc/c/b;-><init>(Landroid/content/Context;)V

    .line 6418
    invoke-virtual {p1}, Lcom/osp/app/signin/AccountView;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string v1, "progress_theme"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 6419
    if-eqz v0, :cond_0

    .line 6421
    const-string v1, "invisible"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 6423
    invoke-virtual {p0}, Lcom/osp/app/signin/ay;->c()V

    .line 6429
    :cond_0
    :goto_0
    return-void

    .line 6426
    :cond_1
    invoke-static {p1, v0}, Lcom/osp/app/signin/AccountView;->d(Lcom/osp/app/signin/AccountView;Ljava/lang/String;)V

    goto :goto_0
.end method


# virtual methods
.method protected final varargs a()Ljava/lang/Boolean;
    .locals 4

    .prologue
    .line 6445
    new-instance v0, Lcom/msc/a/f;

    invoke-direct {v0}, Lcom/msc/a/f;-><init>()V

    .line 6447
    iget-object v1, p0, Lcom/osp/app/signin/ay;->c:Lcom/osp/app/signin/AccountView;

    invoke-static {v1}, Lcom/osp/app/util/ad;->h(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/msc/a/f;->d(Ljava/lang/String;)V

    .line 6449
    iget-object v1, p0, Lcom/osp/app/signin/ay;->c:Lcom/osp/app/signin/AccountView;

    invoke-static {v1}, Lcom/osp/app/signin/AccountView;->l(Lcom/osp/app/signin/AccountView;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/msc/a/f;->f(Ljava/lang/String;)V

    .line 6450
    invoke-virtual {v0}, Lcom/msc/a/f;->c()V

    .line 6451
    iget-object v1, p0, Lcom/osp/app/signin/ay;->c:Lcom/osp/app/signin/AccountView;

    invoke-static {v1}, Lcom/osp/app/signin/AccountView;->x(Lcom/osp/app/signin/AccountView;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/msc/a/f;->g(Ljava/lang/String;)V

    .line 6452
    invoke-static {}, Lcom/osp/device/b;->b()Lcom/osp/device/b;

    move-result-object v1

    invoke-virtual {v1}, Lcom/osp/device/b;->a()Z

    move-result v1

    if-eqz v1, :cond_1

    iget-object v1, p0, Lcom/osp/app/signin/ay;->c:Lcom/osp/app/signin/AccountView;

    invoke-static {v1}, Lcom/osp/app/util/ad;->h(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Landroid/telephony/PhoneNumberUtils;->isGlobalPhoneNumber(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 6454
    iget-object v1, p0, Lcom/osp/app/signin/ay;->c:Lcom/osp/app/signin/AccountView;

    invoke-static {v1}, Lcom/msc/c/e;->h(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v1

    .line 6455
    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 6457
    invoke-virtual {v0, v1}, Lcom/msc/a/f;->c(Ljava/lang/String;)V

    .line 6459
    :cond_0
    const-string v1, "Y"

    invoke-virtual {v0, v1}, Lcom/msc/a/f;->b(Ljava/lang/String;)V

    .line 6461
    :cond_1
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v1, "AV"

    const-string v2, "RequestCheckListInfo"

    const-string v3, "START"

    invoke-static {v1, v2, v3}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    invoke-static {}, Lcom/msc/c/g;->a()Lcom/msc/c/g;

    iget-object v1, p0, Lcom/osp/app/signin/ay;->c:Lcom/osp/app/signin/AccountView;

    invoke-static {v1, v0, p0}, Lcom/msc/c/g;->a(Landroid/content/Context;Lcom/msc/a/f;Lcom/msc/b/h;)J

    move-result-wide v0

    iput-wide v0, p0, Lcom/osp/app/signin/ay;->d:J

    iget-wide v0, p0, Lcom/osp/app/signin/ay;->d:J

    invoke-virtual {p0, v0, v1}, Lcom/osp/app/signin/ay;->a(J)V

    iget-wide v0, p0, Lcom/osp/app/signin/ay;->d:J

    const-string v2, "from_xml"

    invoke-virtual {p0, v0, v1, v2}, Lcom/osp/app/signin/ay;->a(JLjava/lang/String;)V

    iget-wide v0, p0, Lcom/osp/app/signin/ay;->d:J

    sget-object v2, Lcom/msc/b/g;->c:Lcom/msc/b/g;

    invoke-static {v0, v1, v2}, Lcom/msc/c/g;->a(JLcom/msc/b/g;)V

    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v0, "AV"

    const-string v1, "RequestCheckListInfo"

    const-string v2, "END"

    invoke-static {v0, v1, v2}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 6462
    const/4 v0, 0x0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    return-object v0
.end method

.method public final declared-synchronized a(Lcom/msc/b/c;)V
    .locals 6

    .prologue
    .line 6522
    monitor-enter p0

    :try_start_0
    invoke-super {p0, p1}, Lcom/msc/c/b;->a(Lcom/msc/b/c;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 6524
    if-nez p1, :cond_1

    .line 6544
    :cond_0
    :goto_0
    monitor-exit p0

    return-void

    .line 6529
    :cond_1
    :try_start_1
    invoke-virtual {p1}, Lcom/msc/b/c;->a()J

    move-result-wide v0

    .line 6530
    invoke-virtual {p1}, Lcom/msc/b/c;->e()Ljava/lang/String;

    move-result-object v2

    .line 6532
    iget-wide v4, p0, Lcom/osp/app/signin/ay;->d:J
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    cmp-long v0, v0, v4

    if-nez v0, :cond_0

    .line 6536
    :try_start_2
    invoke-static {}, Lcom/msc/c/h;->a()Lcom/msc/c/h;

    iget-object v0, p0, Lcom/osp/app/signin/ay;->c:Lcom/osp/app/signin/AccountView;

    invoke-static {v0, v2}, Lcom/msc/c/h;->b(Landroid/content/Context;Ljava/lang/String;)Lcom/msc/a/g;

    move-result-object v0

    iput-object v0, p0, Lcom/osp/app/signin/ay;->e:Lcom/msc/a/g;
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_0
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0

    .line 6538
    :catch_0
    move-exception v0

    .line 6540
    :try_start_3
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    .line 6541
    new-instance v1, Lcom/msc/c/f;

    invoke-direct {v1, v0}, Lcom/msc/c/f;-><init>(Ljava/lang/Exception;)V

    iput-object v1, p0, Lcom/osp/app/signin/ay;->b:Lcom/msc/c/f;
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    goto :goto_0

    .line 6522
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method protected final a(Ljava/lang/Boolean;)V
    .locals 5

    .prologue
    const/4 v4, 0x0

    const/4 v3, 0x0

    const/4 v2, 0x1

    .line 6467
    invoke-super {p0, p1}, Lcom/msc/c/b;->a(Ljava/lang/Boolean;)V

    .line 6468
    iget-object v0, p0, Lcom/osp/app/signin/ay;->b:Lcom/msc/c/f;

    if-eqz v0, :cond_1

    .line 6470
    invoke-static {}, Lcom/osp/device/b;->b()Lcom/osp/device/b;

    move-result-object v0

    invoke-virtual {v0}, Lcom/osp/device/b;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    const-string v0, "USR_3174"

    iget-object v1, p0, Lcom/osp/app/signin/ay;->b:Lcom/msc/c/f;

    invoke-virtual {v1}, Lcom/msc/c/f;->a()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-ne v0, v2, :cond_0

    .line 6472
    iget-object v0, p0, Lcom/osp/app/signin/ay;->c:Lcom/osp/app/signin/AccountView;

    iget-object v1, p0, Lcom/osp/app/signin/ay;->c:Lcom/osp/app/signin/AccountView;

    invoke-static {v1}, Lcom/osp/app/util/ad;->h(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/osp/app/signin/AccountView;->c(Lcom/osp/app/signin/AccountView;Ljava/lang/String;)V

    .line 6473
    iget-object v0, p0, Lcom/osp/app/signin/ay;->c:Lcom/osp/app/signin/AccountView;

    invoke-virtual {v0}, Lcom/osp/app/signin/AccountView;->finish()V

    .line 6518
    :goto_0
    return-void

    .line 6477
    :cond_0
    invoke-virtual {p0, v4}, Lcom/osp/app/signin/ay;->a(Z)V

    .line 6478
    iget-object v0, p0, Lcom/osp/app/signin/ay;->c:Lcom/osp/app/signin/AccountView;

    iget-object v1, p0, Lcom/osp/app/signin/ay;->c:Lcom/osp/app/signin/AccountView;

    invoke-static {v1}, Lcom/osp/app/signin/AccountView;->m(Lcom/osp/app/signin/AccountView;)Landroid/content/Intent;

    move-result-object v1

    invoke-static {v0, v2, v1, v3}, Lcom/osp/app/signin/AccountView;->a(Lcom/osp/app/signin/AccountView;ILandroid/content/Intent;Landroid/content/Intent;)V

    .line 6479
    iget-object v0, p0, Lcom/osp/app/signin/ay;->c:Lcom/osp/app/signin/AccountView;

    invoke-virtual {v0}, Lcom/osp/app/signin/AccountView;->finish()V

    goto :goto_0

    .line 6483
    :cond_1
    iget-object v0, p0, Lcom/osp/app/signin/ay;->e:Lcom/msc/a/g;

    if-eqz v0, :cond_5

    .line 6485
    iget-object v0, p0, Lcom/osp/app/signin/ay;->e:Lcom/msc/a/g;

    invoke-virtual {v0}, Lcom/msc/a/g;->h()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 6488
    iget-object v0, p0, Lcom/osp/app/signin/ay;->c:Lcom/osp/app/signin/AccountView;

    iget-object v1, p0, Lcom/osp/app/signin/ay;->c:Lcom/osp/app/signin/AccountView;

    invoke-static {v1}, Lcom/osp/app/util/ad;->g(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/osp/app/signin/AccountView;->e(Lcom/osp/app/signin/AccountView;Ljava/lang/String;)V

    goto :goto_0

    .line 6491
    :cond_2
    iget-object v0, p0, Lcom/osp/app/signin/ay;->e:Lcom/msc/a/g;

    invoke-virtual {v0}, Lcom/msc/a/g;->j()Z

    move-result v0

    if-eqz v0, :cond_4

    .line 6493
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v0, "AV"

    const-string v1, "is3rdParty"

    invoke-static {v0, v1}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 6494
    iget-object v0, p0, Lcom/osp/app/signin/ay;->e:Lcom/msc/a/g;

    invoke-virtual {v0}, Lcom/msc/a/g;->k()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 6496
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v0, "AV"

    const-string v1, "isRequireDisclaimer"

    invoke-static {v0, v1}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 6497
    iget-object v0, p0, Lcom/osp/app/signin/ay;->c:Lcom/osp/app/signin/AccountView;

    invoke-static {v0}, Lcom/osp/app/signin/AccountView;->z(Lcom/osp/app/signin/AccountView;)V

    goto :goto_0

    .line 6500
    :cond_3
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v0, "AV"

    const-string v1, "already agree to Disclaimer"

    invoke-static {v0, v1}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 6501
    iget-object v0, p0, Lcom/osp/app/signin/ay;->c:Lcom/osp/app/signin/AccountView;

    invoke-static {v0}, Lcom/osp/app/signin/AccountView;->m(Lcom/osp/app/signin/AccountView;)Landroid/content/Intent;

    move-result-object v0

    const-string v1, "is_agree_to_disclaimer"

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 6502
    iget-object v0, p0, Lcom/osp/app/signin/ay;->c:Lcom/osp/app/signin/AccountView;

    const/4 v1, -0x1

    iget-object v2, p0, Lcom/osp/app/signin/ay;->c:Lcom/osp/app/signin/AccountView;

    invoke-static {v2}, Lcom/osp/app/signin/AccountView;->m(Lcom/osp/app/signin/AccountView;)Landroid/content/Intent;

    move-result-object v2

    invoke-static {v0, v1, v2, v3}, Lcom/osp/app/signin/AccountView;->a(Lcom/osp/app/signin/AccountView;ILandroid/content/Intent;Landroid/content/Intent;)V

    .line 6503
    iget-object v0, p0, Lcom/osp/app/signin/ay;->c:Lcom/osp/app/signin/AccountView;

    invoke-virtual {v0}, Lcom/osp/app/signin/AccountView;->finish()V

    goto :goto_0

    .line 6508
    :cond_4
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v0, "AV"

    const-string v1, "is not 3rdParty"

    invoke-static {v0, v1}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 6509
    iget-object v0, p0, Lcom/osp/app/signin/ay;->c:Lcom/osp/app/signin/AccountView;

    iget-object v1, p0, Lcom/osp/app/signin/ay;->c:Lcom/osp/app/signin/AccountView;

    invoke-static {v1}, Lcom/osp/app/signin/AccountView;->m(Lcom/osp/app/signin/AccountView;)Landroid/content/Intent;

    move-result-object v1

    invoke-static {v0, v2, v1, v3}, Lcom/osp/app/signin/AccountView;->a(Lcom/osp/app/signin/AccountView;ILandroid/content/Intent;Landroid/content/Intent;)V

    .line 6510
    iget-object v0, p0, Lcom/osp/app/signin/ay;->c:Lcom/osp/app/signin/AccountView;

    invoke-virtual {v0}, Lcom/osp/app/signin/AccountView;->finish()V

    goto/16 :goto_0

    .line 6514
    :cond_5
    invoke-virtual {p0, v4}, Lcom/osp/app/signin/ay;->a(Z)V

    .line 6515
    iget-object v0, p0, Lcom/osp/app/signin/ay;->c:Lcom/osp/app/signin/AccountView;

    iget-object v1, p0, Lcom/osp/app/signin/ay;->c:Lcom/osp/app/signin/AccountView;

    invoke-static {v1}, Lcom/osp/app/signin/AccountView;->m(Lcom/osp/app/signin/AccountView;)Landroid/content/Intent;

    move-result-object v1

    invoke-static {v0, v2, v1, v3}, Lcom/osp/app/signin/AccountView;->a(Lcom/osp/app/signin/AccountView;ILandroid/content/Intent;Landroid/content/Intent;)V

    .line 6516
    iget-object v0, p0, Lcom/osp/app/signin/ay;->c:Lcom/osp/app/signin/AccountView;

    invoke-virtual {v0}, Lcom/osp/app/signin/AccountView;->finish()V

    goto/16 :goto_0
.end method

.method protected final d()V
    .locals 2

    .prologue
    .line 6433
    invoke-super {p0}, Lcom/msc/c/b;->d()V

    .line 6434
    iget-object v0, p0, Lcom/osp/app/signin/ay;->c:Lcom/osp/app/signin/AccountView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/osp/app/signin/AccountView;->b(I)V

    .line 6435
    iget-object v0, p0, Lcom/osp/app/signin/ay;->c:Lcom/osp/app/signin/AccountView;

    invoke-virtual {v0}, Lcom/osp/app/signin/AccountView;->finish()V

    .line 6436
    return-void
.end method

.method protected final synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 6399
    invoke-virtual {p0}, Lcom/osp/app/signin/ay;->a()Ljava/lang/Boolean;

    move-result-object v0

    return-object v0
.end method

.method protected final synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 6399
    check-cast p1, Ljava/lang/Boolean;

    invoke-virtual {p0, p1}, Lcom/osp/app/signin/ay;->a(Ljava/lang/Boolean;)V

    return-void
.end method
