.class final Lcom/osp/app/signin/fz;
.super Lcom/msc/c/b;
.source "NameValidationWebView.java"


# instance fields
.field final synthetic c:Lcom/osp/app/signin/NameValidationWebView;


# direct methods
.method public constructor <init>(Lcom/osp/app/signin/NameValidationWebView;)V
    .locals 0

    .prologue
    .line 828
    iput-object p1, p0, Lcom/osp/app/signin/fz;->c:Lcom/osp/app/signin/NameValidationWebView;

    .line 829
    invoke-direct {p0, p1}, Lcom/msc/c/b;-><init>(Landroid/content/Context;)V

    .line 833
    return-void
.end method


# virtual methods
.method protected final varargs a()Ljava/lang/Boolean;
    .locals 11

    .prologue
    .line 843
    invoke-static {}, Lcom/msc/c/g;->a()Lcom/msc/c/g;

    .line 844
    iget-object v0, p0, Lcom/osp/app/signin/fz;->c:Lcom/osp/app/signin/NameValidationWebView;

    invoke-static {v0}, Lcom/osp/app/util/r;->s(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 846
    iget-object v9, p0, Lcom/osp/app/signin/fz;->c:Lcom/osp/app/signin/NameValidationWebView;

    iget-object v0, p0, Lcom/osp/app/signin/fz;->c:Lcom/osp/app/signin/NameValidationWebView;

    invoke-static {v0}, Lcom/osp/app/signin/NameValidationWebView;->f(Lcom/osp/app/signin/NameValidationWebView;)Landroid/content/Context;

    move-result-object v0

    iget-object v1, p0, Lcom/osp/app/signin/fz;->c:Lcom/osp/app/signin/NameValidationWebView;

    invoke-static {v1}, Lcom/osp/app/signin/NameValidationWebView;->g(Lcom/osp/app/signin/NameValidationWebView;)Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/osp/app/signin/fz;->c:Lcom/osp/app/signin/NameValidationWebView;

    invoke-static {v2}, Lcom/osp/app/signin/NameValidationWebView;->h(Lcom/osp/app/signin/NameValidationWebView;)Ljava/lang/String;

    move-result-object v2

    iget-object v3, p0, Lcom/osp/app/signin/fz;->c:Lcom/osp/app/signin/NameValidationWebView;

    invoke-static {v3}, Lcom/osp/app/signin/NameValidationWebView;->i(Lcom/osp/app/signin/NameValidationWebView;)Ljava/lang/String;

    move-result-object v3

    iget-object v4, p0, Lcom/osp/app/signin/fz;->c:Lcom/osp/app/signin/NameValidationWebView;

    invoke-static {v4}, Lcom/osp/app/signin/NameValidationWebView;->j(Lcom/osp/app/signin/NameValidationWebView;)Ljava/lang/String;

    move-result-object v4

    iget-object v5, p0, Lcom/osp/app/signin/fz;->c:Lcom/osp/app/signin/NameValidationWebView;

    invoke-static {v5}, Lcom/osp/app/signin/NameValidationWebView;->k(Lcom/osp/app/signin/NameValidationWebView;)Ljava/lang/String;

    move-result-object v5

    iget-object v6, p0, Lcom/osp/app/signin/fz;->c:Lcom/osp/app/signin/NameValidationWebView;

    invoke-static {v6}, Lcom/osp/app/signin/NameValidationWebView;->l(Lcom/osp/app/signin/NameValidationWebView;)Ljava/lang/String;

    move-result-object v6

    iget-object v7, p0, Lcom/osp/app/signin/fz;->c:Lcom/osp/app/signin/NameValidationWebView;

    invoke-static {v7}, Lcom/osp/app/signin/NameValidationWebView;->m(Lcom/osp/app/signin/NameValidationWebView;)Ljava/lang/String;

    iget-object v7, p0, Lcom/osp/app/signin/fz;->c:Lcom/osp/app/signin/NameValidationWebView;

    invoke-static {v7}, Lcom/osp/app/signin/NameValidationWebView;->n(Lcom/osp/app/signin/NameValidationWebView;)Ljava/lang/String;

    move-result-object v7

    move-object v8, p0

    invoke-static/range {v0 .. v8}, Lcom/msc/c/g;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/msc/b/h;)J

    move-result-wide v0

    invoke-static {v9, v0, v1}, Lcom/osp/app/signin/NameValidationWebView;->a(Lcom/osp/app/signin/NameValidationWebView;J)J

    .line 853
    :goto_0
    iget-object v0, p0, Lcom/osp/app/signin/fz;->c:Lcom/osp/app/signin/NameValidationWebView;

    invoke-static {v0}, Lcom/osp/app/signin/NameValidationWebView;->p(Lcom/osp/app/signin/NameValidationWebView;)J

    move-result-wide v0

    invoke-virtual {p0, v0, v1}, Lcom/osp/app/signin/fz;->a(J)V

    .line 854
    iget-object v0, p0, Lcom/osp/app/signin/fz;->c:Lcom/osp/app/signin/NameValidationWebView;

    invoke-static {v0}, Lcom/osp/app/signin/NameValidationWebView;->p(Lcom/osp/app/signin/NameValidationWebView;)J

    move-result-wide v0

    const-string v2, "from_xml"

    invoke-virtual {p0, v0, v1, v2}, Lcom/osp/app/signin/fz;->a(JLjava/lang/String;)V

    .line 856
    iget-object v0, p0, Lcom/osp/app/signin/fz;->c:Lcom/osp/app/signin/NameValidationWebView;

    invoke-static {v0}, Lcom/osp/app/signin/NameValidationWebView;->p(Lcom/osp/app/signin/NameValidationWebView;)J

    move-result-wide v0

    sget-object v2, Lcom/msc/b/g;->c:Lcom/msc/b/g;

    invoke-static {v0, v1, v2}, Lcom/msc/c/g;->a(JLcom/msc/b/g;)V

    .line 858
    const/4 v0, 0x0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    return-object v0

    .line 850
    :cond_0
    iget-object v10, p0, Lcom/osp/app/signin/fz;->c:Lcom/osp/app/signin/NameValidationWebView;

    iget-object v0, p0, Lcom/osp/app/signin/fz;->c:Lcom/osp/app/signin/NameValidationWebView;

    invoke-static {v0}, Lcom/osp/app/signin/NameValidationWebView;->f(Lcom/osp/app/signin/NameValidationWebView;)Landroid/content/Context;

    move-result-object v0

    iget-object v1, p0, Lcom/osp/app/signin/fz;->c:Lcom/osp/app/signin/NameValidationWebView;

    invoke-static {v1}, Lcom/osp/app/signin/NameValidationWebView;->g(Lcom/osp/app/signin/NameValidationWebView;)Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/osp/app/signin/fz;->c:Lcom/osp/app/signin/NameValidationWebView;

    invoke-static {v2}, Lcom/osp/app/signin/NameValidationWebView;->h(Lcom/osp/app/signin/NameValidationWebView;)Ljava/lang/String;

    move-result-object v2

    iget-object v3, p0, Lcom/osp/app/signin/fz;->c:Lcom/osp/app/signin/NameValidationWebView;

    invoke-static {v3}, Lcom/osp/app/signin/NameValidationWebView;->i(Lcom/osp/app/signin/NameValidationWebView;)Ljava/lang/String;

    move-result-object v3

    iget-object v4, p0, Lcom/osp/app/signin/fz;->c:Lcom/osp/app/signin/NameValidationWebView;

    invoke-static {v4}, Lcom/osp/app/signin/NameValidationWebView;->j(Lcom/osp/app/signin/NameValidationWebView;)Ljava/lang/String;

    move-result-object v4

    iget-object v5, p0, Lcom/osp/app/signin/fz;->c:Lcom/osp/app/signin/NameValidationWebView;

    invoke-static {v5}, Lcom/osp/app/signin/NameValidationWebView;->k(Lcom/osp/app/signin/NameValidationWebView;)Ljava/lang/String;

    move-result-object v5

    iget-object v6, p0, Lcom/osp/app/signin/fz;->c:Lcom/osp/app/signin/NameValidationWebView;

    invoke-static {v6}, Lcom/osp/app/signin/NameValidationWebView;->l(Lcom/osp/app/signin/NameValidationWebView;)Ljava/lang/String;

    move-result-object v6

    iget-object v7, p0, Lcom/osp/app/signin/fz;->c:Lcom/osp/app/signin/NameValidationWebView;

    invoke-static {v7}, Lcom/osp/app/signin/NameValidationWebView;->m(Lcom/osp/app/signin/NameValidationWebView;)Ljava/lang/String;

    move-result-object v7

    iget-object v8, p0, Lcom/osp/app/signin/fz;->c:Lcom/osp/app/signin/NameValidationWebView;

    invoke-static {v8}, Lcom/osp/app/signin/NameValidationWebView;->o(Lcom/osp/app/signin/NameValidationWebView;)Ljava/lang/String;

    move-result-object v8

    move-object v9, p0

    invoke-static/range {v0 .. v9}, Lcom/msc/c/g;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/msc/b/h;)J

    move-result-wide v0

    invoke-static {v10, v0, v1}, Lcom/osp/app/signin/NameValidationWebView;->a(Lcom/osp/app/signin/NameValidationWebView;J)J

    goto :goto_0
.end method

.method public final declared-synchronized a(Lcom/msc/b/c;)V
    .locals 8

    .prologue
    .line 863
    monitor-enter p0

    :try_start_0
    invoke-super {p0, p1}, Lcom/msc/c/b;->a(Lcom/msc/b/c;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 865
    if-nez p1, :cond_1

    .line 946
    :cond_0
    :goto_0
    monitor-exit p0

    return-void

    .line 870
    :cond_1
    :try_start_1
    invoke-virtual {p1}, Lcom/msc/b/c;->a()J

    move-result-wide v0

    .line 872
    iget-object v2, p0, Lcom/osp/app/signin/fz;->c:Lcom/osp/app/signin/NameValidationWebView;

    invoke-static {v2}, Lcom/osp/app/signin/NameValidationWebView;->p(Lcom/osp/app/signin/NameValidationWebView;)J

    move-result-wide v2

    cmp-long v0, v0, v2

    if-nez v0, :cond_0

    .line 874
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v0, "NVWV"

    const-string v1, "NameValidationWebView - Name Check Success."

    invoke-static {v0, v1}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 876
    iget-object v0, p0, Lcom/osp/app/signin/fz;->c:Lcom/osp/app/signin/NameValidationWebView;

    const-string v1, "notification"

    invoke-virtual {v0, v1}, Lcom/osp/app/signin/NameValidationWebView;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/NotificationManager;

    .line 877
    const v1, 0x132df82

    invoke-virtual {v0, v1}, Landroid/app/NotificationManager;->cancel(I)V

    .line 879
    iget-object v0, p0, Lcom/osp/app/signin/fz;->c:Lcom/osp/app/signin/NameValidationWebView;

    invoke-static {v0}, Lcom/osp/app/signin/NameValidationWebView;->q(Lcom/osp/app/signin/NameValidationWebView;)I

    move-result v0

    and-int/lit8 v0, v0, 0x4

    const/4 v1, 0x4

    if-ne v0, v1, :cond_2

    .line 881
    iget-object v0, p0, Lcom/osp/app/signin/fz;->c:Lcom/osp/app/signin/NameValidationWebView;

    invoke-static {v0}, Lcom/osp/app/signin/NameValidationWebView;->r(Lcom/osp/app/signin/NameValidationWebView;)I

    .line 882
    invoke-static {}, Lcom/osp/app/signin/SamsungService;->p()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 884
    iget-object v0, p0, Lcom/osp/app/signin/fz;->c:Lcom/osp/app/signin/NameValidationWebView;

    invoke-static {v0}, Lcom/msc/openprovider/b;->g(Landroid/content/Context;)V

    .line 888
    :cond_2
    iget-object v0, p0, Lcom/osp/app/signin/fz;->c:Lcom/osp/app/signin/NameValidationWebView;

    invoke-static {v0}, Lcom/osp/app/signin/NameValidationWebView;->q(Lcom/osp/app/signin/NameValidationWebView;)I

    move-result v0

    if-nez v0, :cond_3

    iget-object v0, p0, Lcom/osp/app/signin/fz;->c:Lcom/osp/app/signin/NameValidationWebView;

    invoke-static {v0}, Lcom/osp/app/util/r;->t(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 890
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v0, "NVWV"

    const-string v1, "Show MybenefitNoti"

    invoke-static {v0, v1}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 894
    new-instance v0, Landroid/content/Intent;

    const-string v1, "com.msc.action.samsungaccount.mybenefitwebview_internal"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 906
    iget-object v1, p0, Lcom/osp/app/signin/fz;->c:Lcom/osp/app/signin/NameValidationWebView;

    const/4 v2, 0x0

    const/high16 v3, 0x8000000

    invoke-static {v1, v2, v0, v3}, Landroid/app/PendingIntent;->getActivity(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v1

    .line 908
    iget-object v0, p0, Lcom/osp/app/signin/fz;->c:Lcom/osp/app/signin/NameValidationWebView;

    const/4 v2, 0x0

    iget-object v3, p0, Lcom/osp/app/signin/fz;->c:Lcom/osp/app/signin/NameValidationWebView;

    const v4, 0x7f09004f

    invoke-virtual {v3, v4}, Lcom/osp/app/signin/NameValidationWebView;->getString(I)Ljava/lang/String;

    move-result-object v3

    iget-object v4, p0, Lcom/osp/app/signin/fz;->c:Lcom/osp/app/signin/NameValidationWebView;

    const v5, 0x7f090152

    invoke-virtual {v4, v5}, Lcom/osp/app/signin/NameValidationWebView;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-static {}, Lcom/osp/app/signin/SamsungService;->i()I

    move-result v5

    const v6, 0x132df82

    invoke-static/range {v0 .. v6}, Lcom/msc/sa/c/d;->a(Landroid/content/Context;Landroid/app/PendingIntent;Landroid/app/PendingIntent;Ljava/lang/String;Ljava/lang/String;II)V

    .line 912
    :cond_3
    iget-object v0, p0, Lcom/osp/app/signin/fz;->c:Lcom/osp/app/signin/NameValidationWebView;

    invoke-static {v0}, Lcom/osp/app/signin/NameValidationWebView;->q(Lcom/osp/app/signin/NameValidationWebView;)I

    move-result v0

    if-lez v0, :cond_5

    .line 916
    iget-object v0, p0, Lcom/osp/app/signin/fz;->c:Lcom/osp/app/signin/NameValidationWebView;

    iget-object v1, p0, Lcom/osp/app/signin/fz;->c:Lcom/osp/app/signin/NameValidationWebView;

    invoke-static {v1}, Lcom/osp/app/signin/NameValidationWebView;->q(Lcom/osp/app/signin/NameValidationWebView;)I

    move-result v1

    iget-object v2, p0, Lcom/osp/app/signin/fz;->c:Lcom/osp/app/signin/NameValidationWebView;

    invoke-static {v2}, Lcom/msc/c/e;->h(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v2

    iget-object v3, p0, Lcom/osp/app/signin/fz;->c:Lcom/osp/app/signin/NameValidationWebView;

    invoke-static {v3}, Lcom/osp/app/signin/NameValidationWebView;->s(Lcom/osp/app/signin/NameValidationWebView;)Ljava/lang/String;

    move-result-object v3

    iget-object v4, p0, Lcom/osp/app/signin/fz;->c:Lcom/osp/app/signin/NameValidationWebView;

    invoke-static {v4}, Lcom/osp/app/signin/NameValidationWebView;->t(Lcom/osp/app/signin/NameValidationWebView;)Ljava/lang/String;

    move-result-object v4

    iget-object v5, p0, Lcom/osp/app/signin/fz;->c:Lcom/osp/app/signin/NameValidationWebView;

    invoke-static {v5}, Lcom/osp/app/signin/NameValidationWebView;->u(Lcom/osp/app/signin/NameValidationWebView;)Z

    move-result v5

    const/4 v6, 0x0

    const/4 v7, 0x0

    invoke-static/range {v0 .. v7}, Lcom/osp/app/util/ad;->a(Landroid/content/Context;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;ZZLjava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    .line 919
    iget-object v1, p0, Lcom/osp/app/signin/fz;->c:Lcom/osp/app/signin/NameValidationWebView;

    invoke-static {v1}, Lcom/osp/app/signin/NameValidationWebView;->v(Lcom/osp/app/signin/NameValidationWebView;)Z

    move-result v1

    if-eqz v1, :cond_4

    .line 921
    const-string v1, "key_return_result"

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 922
    iget-object v1, p0, Lcom/osp/app/signin/fz;->c:Lcom/osp/app/signin/NameValidationWebView;

    const/16 v2, 0xdc

    invoke-virtual {v1, v0, v2}, Lcom/osp/app/signin/NameValidationWebView;->startActivityForResult(Landroid/content/Intent;I)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto/16 :goto_0

    .line 863
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 927
    :cond_4
    :try_start_2
    iget-object v1, p0, Lcom/osp/app/signin/fz;->c:Lcom/osp/app/signin/NameValidationWebView;

    invoke-virtual {v1, v0}, Lcom/osp/app/signin/NameValidationWebView;->startActivity(Landroid/content/Intent;)V

    .line 931
    :cond_5
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    .line 932
    const-string v1, "key_name_check_familyname"

    iget-object v2, p0, Lcom/osp/app/signin/fz;->c:Lcom/osp/app/signin/NameValidationWebView;

    invoke-static {v2}, Lcom/osp/app/signin/NameValidationWebView;->h(Lcom/osp/app/signin/NameValidationWebView;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 933
    const-string v1, "key_name_check_givenname"

    iget-object v2, p0, Lcom/osp/app/signin/fz;->c:Lcom/osp/app/signin/NameValidationWebView;

    invoke-static {v2}, Lcom/osp/app/signin/NameValidationWebView;->i(Lcom/osp/app/signin/NameValidationWebView;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 934
    const-string v1, "key_name_check_birthdate"

    iget-object v2, p0, Lcom/osp/app/signin/fz;->c:Lcom/osp/app/signin/NameValidationWebView;

    invoke-static {v2}, Lcom/osp/app/signin/NameValidationWebView;->j(Lcom/osp/app/signin/NameValidationWebView;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 935
    const-string v1, "key_name_check_mobile"

    iget-object v2, p0, Lcom/osp/app/signin/fz;->c:Lcom/osp/app/signin/NameValidationWebView;

    invoke-static {v2}, Lcom/osp/app/signin/NameValidationWebView;->w(Lcom/osp/app/signin/NameValidationWebView;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 936
    const-string v1, "key_name_check_method"

    iget-object v2, p0, Lcom/osp/app/signin/fz;->c:Lcom/osp/app/signin/NameValidationWebView;

    invoke-static {v2}, Lcom/osp/app/signin/NameValidationWebView;->k(Lcom/osp/app/signin/NameValidationWebView;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 937
    const-string v1, "key_name_check_ci"

    iget-object v2, p0, Lcom/osp/app/signin/fz;->c:Lcom/osp/app/signin/NameValidationWebView;

    invoke-static {v2}, Lcom/osp/app/signin/NameValidationWebView;->l(Lcom/osp/app/signin/NameValidationWebView;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 938
    const-string v1, "key_name_check_di"

    iget-object v2, p0, Lcom/osp/app/signin/fz;->c:Lcom/osp/app/signin/NameValidationWebView;

    invoke-static {v2}, Lcom/osp/app/signin/NameValidationWebView;->m(Lcom/osp/app/signin/NameValidationWebView;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 939
    const-string v1, "key_name_check_datetime"

    iget-object v2, p0, Lcom/osp/app/signin/fz;->c:Lcom/osp/app/signin/NameValidationWebView;

    invoke-static {v2}, Lcom/osp/app/signin/NameValidationWebView;->n(Lcom/osp/app/signin/NameValidationWebView;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 940
    const-string v1, "key_name_check_gender"

    iget-object v2, p0, Lcom/osp/app/signin/fz;->c:Lcom/osp/app/signin/NameValidationWebView;

    invoke-static {v2}, Lcom/osp/app/signin/NameValidationWebView;->o(Lcom/osp/app/signin/NameValidationWebView;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 941
    const-string v1, "key_name_check_foreigner"

    iget-object v2, p0, Lcom/osp/app/signin/fz;->c:Lcom/osp/app/signin/NameValidationWebView;

    invoke-static {v2}, Lcom/osp/app/signin/NameValidationWebView;->x(Lcom/osp/app/signin/NameValidationWebView;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 943
    iget-object v1, p0, Lcom/osp/app/signin/fz;->c:Lcom/osp/app/signin/NameValidationWebView;

    const/4 v2, -0x1

    invoke-virtual {v1, v2, v0}, Lcom/osp/app/signin/NameValidationWebView;->a(ILandroid/content/Intent;)V

    .line 944
    iget-object v0, p0, Lcom/osp/app/signin/fz;->c:Lcom/osp/app/signin/NameValidationWebView;

    invoke-virtual {v0}, Lcom/osp/app/signin/NameValidationWebView;->finish()V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto/16 :goto_0
.end method

.method protected final a(Ljava/lang/Boolean;)V
    .locals 0

    .prologue
    .line 995
    invoke-super {p0, p1}, Lcom/msc/c/b;->a(Ljava/lang/Boolean;)V

    .line 996
    return-void
.end method

.method public final b(Lcom/msc/b/c;)V
    .locals 8

    .prologue
    .line 950
    invoke-super {p0, p1}, Lcom/msc/c/b;->b(Lcom/msc/b/c;)V

    .line 952
    if-nez p1, :cond_1

    .line 991
    :cond_0
    :goto_0
    return-void

    .line 957
    :cond_1
    invoke-virtual {p1}, Lcom/msc/b/c;->a()J

    move-result-wide v0

    .line 959
    iget-object v2, p0, Lcom/osp/app/signin/fz;->c:Lcom/osp/app/signin/NameValidationWebView;

    invoke-static {v2}, Lcom/osp/app/signin/NameValidationWebView;->p(Lcom/osp/app/signin/NameValidationWebView;)J

    move-result-wide v2

    cmp-long v0, v0, v2

    if-nez v0, :cond_0

    .line 961
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v0, "NVWV"

    const-string v1, "NameValidationWebView - Name Check Fail."

    invoke-static {v0, v1}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 963
    iget-object v0, p0, Lcom/osp/app/signin/fz;->c:Lcom/osp/app/signin/NameValidationWebView;

    invoke-static {v0}, Lcom/osp/app/signin/NameValidationWebView;->q(Lcom/osp/app/signin/NameValidationWebView;)I

    move-result v0

    and-int/lit8 v0, v0, 0x4

    const/4 v1, 0x4

    if-ne v0, v1, :cond_2

    .line 965
    iget-object v0, p0, Lcom/osp/app/signin/fz;->c:Lcom/osp/app/signin/NameValidationWebView;

    invoke-static {v0}, Lcom/osp/app/signin/NameValidationWebView;->r(Lcom/osp/app/signin/NameValidationWebView;)I

    .line 967
    :cond_2
    iget-object v0, p0, Lcom/osp/app/signin/fz;->c:Lcom/osp/app/signin/NameValidationWebView;

    invoke-static {v0}, Lcom/osp/app/signin/NameValidationWebView;->q(Lcom/osp/app/signin/NameValidationWebView;)I

    move-result v0

    if-lez v0, :cond_3

    .line 971
    iget-object v0, p0, Lcom/osp/app/signin/fz;->c:Lcom/osp/app/signin/NameValidationWebView;

    iget-object v1, p0, Lcom/osp/app/signin/fz;->c:Lcom/osp/app/signin/NameValidationWebView;

    invoke-static {v1}, Lcom/osp/app/signin/NameValidationWebView;->q(Lcom/osp/app/signin/NameValidationWebView;)I

    move-result v1

    iget-object v2, p0, Lcom/osp/app/signin/fz;->c:Lcom/osp/app/signin/NameValidationWebView;

    invoke-static {v2}, Lcom/msc/c/e;->h(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v2

    iget-object v3, p0, Lcom/osp/app/signin/fz;->c:Lcom/osp/app/signin/NameValidationWebView;

    invoke-static {v3}, Lcom/osp/app/signin/NameValidationWebView;->s(Lcom/osp/app/signin/NameValidationWebView;)Ljava/lang/String;

    move-result-object v3

    iget-object v4, p0, Lcom/osp/app/signin/fz;->c:Lcom/osp/app/signin/NameValidationWebView;

    invoke-static {v4}, Lcom/osp/app/signin/NameValidationWebView;->t(Lcom/osp/app/signin/NameValidationWebView;)Ljava/lang/String;

    move-result-object v4

    iget-object v5, p0, Lcom/osp/app/signin/fz;->c:Lcom/osp/app/signin/NameValidationWebView;

    invoke-static {v5}, Lcom/osp/app/signin/NameValidationWebView;->u(Lcom/osp/app/signin/NameValidationWebView;)Z

    move-result v5

    const/4 v6, 0x0

    const/4 v7, 0x0

    invoke-static/range {v0 .. v7}, Lcom/osp/app/util/ad;->a(Landroid/content/Context;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;ZZLjava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    .line 973
    iget-object v1, p0, Lcom/osp/app/signin/fz;->c:Lcom/osp/app/signin/NameValidationWebView;

    invoke-virtual {v1, v0}, Lcom/osp/app/signin/NameValidationWebView;->startActivity(Landroid/content/Intent;)V

    .line 976
    :cond_3
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    .line 977
    const-string v1, "key_name_check_familyname"

    iget-object v2, p0, Lcom/osp/app/signin/fz;->c:Lcom/osp/app/signin/NameValidationWebView;

    invoke-static {v2}, Lcom/osp/app/signin/NameValidationWebView;->h(Lcom/osp/app/signin/NameValidationWebView;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 978
    const-string v1, "key_name_check_givenname"

    iget-object v2, p0, Lcom/osp/app/signin/fz;->c:Lcom/osp/app/signin/NameValidationWebView;

    invoke-static {v2}, Lcom/osp/app/signin/NameValidationWebView;->i(Lcom/osp/app/signin/NameValidationWebView;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 979
    const-string v1, "key_name_check_birthdate"

    iget-object v2, p0, Lcom/osp/app/signin/fz;->c:Lcom/osp/app/signin/NameValidationWebView;

    invoke-static {v2}, Lcom/osp/app/signin/NameValidationWebView;->j(Lcom/osp/app/signin/NameValidationWebView;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 980
    const-string v1, "key_name_check_mobile"

    iget-object v2, p0, Lcom/osp/app/signin/fz;->c:Lcom/osp/app/signin/NameValidationWebView;

    invoke-static {v2}, Lcom/osp/app/signin/NameValidationWebView;->w(Lcom/osp/app/signin/NameValidationWebView;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 981
    const-string v1, "key_name_check_method"

    iget-object v2, p0, Lcom/osp/app/signin/fz;->c:Lcom/osp/app/signin/NameValidationWebView;

    invoke-static {v2}, Lcom/osp/app/signin/NameValidationWebView;->k(Lcom/osp/app/signin/NameValidationWebView;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 982
    const-string v1, "key_name_check_ci"

    iget-object v2, p0, Lcom/osp/app/signin/fz;->c:Lcom/osp/app/signin/NameValidationWebView;

    invoke-static {v2}, Lcom/osp/app/signin/NameValidationWebView;->l(Lcom/osp/app/signin/NameValidationWebView;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 983
    const-string v1, "key_name_check_di"

    iget-object v2, p0, Lcom/osp/app/signin/fz;->c:Lcom/osp/app/signin/NameValidationWebView;

    invoke-static {v2}, Lcom/osp/app/signin/NameValidationWebView;->m(Lcom/osp/app/signin/NameValidationWebView;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 984
    const-string v1, "key_name_check_datetime"

    iget-object v2, p0, Lcom/osp/app/signin/fz;->c:Lcom/osp/app/signin/NameValidationWebView;

    invoke-static {v2}, Lcom/osp/app/signin/NameValidationWebView;->n(Lcom/osp/app/signin/NameValidationWebView;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 985
    const-string v1, "key_name_check_gender"

    iget-object v2, p0, Lcom/osp/app/signin/fz;->c:Lcom/osp/app/signin/NameValidationWebView;

    invoke-static {v2}, Lcom/osp/app/signin/NameValidationWebView;->o(Lcom/osp/app/signin/NameValidationWebView;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 986
    const-string v1, "key_name_check_foreigner"

    iget-object v2, p0, Lcom/osp/app/signin/fz;->c:Lcom/osp/app/signin/NameValidationWebView;

    invoke-static {v2}, Lcom/osp/app/signin/NameValidationWebView;->x(Lcom/osp/app/signin/NameValidationWebView;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 988
    iget-object v1, p0, Lcom/osp/app/signin/fz;->c:Lcom/osp/app/signin/NameValidationWebView;

    const/4 v2, -0x1

    invoke-virtual {v1, v2, v0}, Lcom/osp/app/signin/NameValidationWebView;->a(ILandroid/content/Intent;)V

    .line 989
    iget-object v0, p0, Lcom/osp/app/signin/fz;->c:Lcom/osp/app/signin/NameValidationWebView;

    invoke-virtual {v0}, Lcom/osp/app/signin/NameValidationWebView;->finish()V

    goto/16 :goto_0
.end method

.method protected final d()V
    .locals 0

    .prologue
    .line 837
    invoke-super {p0}, Lcom/msc/c/b;->d()V

    .line 838
    return-void
.end method

.method protected final synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 826
    invoke-virtual {p0}, Lcom/osp/app/signin/fz;->a()Ljava/lang/Boolean;

    move-result-object v0

    return-object v0
.end method

.method protected final synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 826
    check-cast p1, Ljava/lang/Boolean;

    invoke-super {p0, p1}, Lcom/msc/c/b;->a(Ljava/lang/Boolean;)V

    return-void
.end method
