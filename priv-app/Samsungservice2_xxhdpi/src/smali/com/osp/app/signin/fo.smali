.class final Lcom/osp/app/signin/fo;
.super Ljava/lang/Object;
.source "HelpPreference.java"

# interfaces
.implements Landroid/preference/Preference$OnPreferenceClickListener;


# instance fields
.field final synthetic a:Lcom/osp/app/signin/HelpPreference;


# direct methods
.method constructor <init>(Lcom/osp/app/signin/HelpPreference;)V
    .locals 0

    .prologue
    .line 118
    iput-object p1, p0, Lcom/osp/app/signin/fo;->a:Lcom/osp/app/signin/HelpPreference;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onPreferenceClick(Landroid/preference/Preference;)Z
    .locals 5

    .prologue
    const/4 v4, 0x1

    .line 122
    iget-object v0, p0, Lcom/osp/app/signin/fo;->a:Lcom/osp/app/signin/HelpPreference;

    invoke-virtual {v0}, Lcom/osp/app/signin/HelpPreference;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    iget-object v1, p0, Lcom/osp/app/signin/fo;->a:Lcom/osp/app/signin/HelpPreference;

    invoke-static {v1}, Lcom/osp/app/signin/HelpPreference;->b(Lcom/osp/app/signin/HelpPreference;)Z

    move-result v1

    invoke-static {v0, v1}, Lcom/msc/c/g;->a(Landroid/content/Context;Z)Ljava/lang/String;

    move-result-object v0

    .line 124
    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    .line 125
    new-instance v2, Landroid/content/Intent;

    const-string v3, "android.intent.action.VIEW"

    invoke-direct {v2, v3, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;Landroid/net/Uri;)V

    .line 126
    const-string v1, "com.android.browser.application_id"

    iget-object v3, p0, Lcom/osp/app/signin/fo;->a:Lcom/osp/app/signin/HelpPreference;

    invoke-virtual {v3}, Lcom/osp/app/signin/HelpPreference;->getPackageName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v1, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 127
    iget-object v1, p0, Lcom/osp/app/signin/fo;->a:Lcom/osp/app/signin/HelpPreference;

    invoke-static {v2, v1}, Lcom/osp/app/util/ad;->a(Landroid/content/Intent;Landroid/content/Context;)Z

    move-result v1

    if-ne v1, v4, :cond_0

    .line 129
    iget-object v0, p0, Lcom/osp/app/signin/fo;->a:Lcom/osp/app/signin/HelpPreference;

    invoke-virtual {v0, v2}, Lcom/osp/app/signin/HelpPreference;->startActivity(Landroid/content/Intent;)V

    .line 135
    :goto_0
    return v4

    .line 132
    :cond_0
    iget-object v1, p0, Lcom/osp/app/signin/fo;->a:Lcom/osp/app/signin/HelpPreference;

    invoke-static {v1, v0}, Lcom/osp/app/signin/HelpPreference;->a(Lcom/osp/app/signin/HelpPreference;Ljava/lang/String;)V

    goto :goto_0
.end method
