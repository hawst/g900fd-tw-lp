.class public Lcom/osp/app/signin/AccountHelpPreference;
.super Lcom/osp/app/util/BasePreferenceActivity;
.source "AccountHelpPreference.java"


# instance fields
.field private a:Z


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 32
    invoke-direct {p0}, Lcom/osp/app/util/BasePreferenceActivity;-><init>()V

    .line 48
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/osp/app/signin/AccountHelpPreference;->a:Z

    return-void
.end method

.method private a(I)V
    .locals 4

    .prologue
    const v3, 0x7f08011e

    .line 256
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "AccountView::initLayoutParams orientation : "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;)V

    .line 258
    invoke-static {}, Lcom/osp/device/b;->b()Lcom/osp/device/b;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/osp/device/b;->i(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 260
    invoke-virtual {p0}, Lcom/osp/app/signin/AccountHelpPreference;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f08011f

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v0

    float-to-int v0, v0

    .line 261
    invoke-virtual {p0}, Lcom/osp/app/signin/AccountHelpPreference;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1, v3}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v1

    float-to-int v1, v1

    .line 262
    invoke-static {}, Lcom/osp/app/signin/SamsungService;->a()Z

    move-result v2

    if-nez v2, :cond_0

    .line 264
    const v0, 0x7f0c015e

    invoke-virtual {p0, v0}, Lcom/osp/app/signin/AccountHelpPreference;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    .line 265
    invoke-virtual {p0, v0, p1}, Lcom/osp/app/signin/AccountHelpPreference;->a(Landroid/view/ViewGroup;I)V

    .line 267
    invoke-virtual {p0}, Lcom/osp/app/signin/AccountHelpPreference;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0, v3}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v0

    float-to-int v0, v0

    .line 269
    :cond_0
    const v2, 0x7f0c0161

    invoke-virtual {p0, v2}, Lcom/osp/app/signin/AccountHelpPreference;->findViewById(I)Landroid/view/View;

    move-result-object v2

    .line 270
    invoke-static {}, Lcom/osp/app/util/q;->a()Lcom/osp/app/util/q;

    invoke-static {v2, v0, v1, p1}, Lcom/osp/app/util/q;->b(Landroid/view/View;III)V

    .line 272
    :cond_1
    return-void
.end method

.method static synthetic a(Lcom/osp/app/signin/AccountHelpPreference;Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 32
    new-instance v0, Landroid/content/Intent;

    const-string v1, "com.msc.action.samsungaccount.web_with_close_button"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    const-string v1, "ServerUrl"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    invoke-virtual {p0, v0}, Lcom/osp/app/signin/AccountHelpPreference;->startActivity(Landroid/content/Intent;)V

    return-void
.end method

.method static synthetic a(Lcom/osp/app/signin/AccountHelpPreference;)Z
    .locals 1

    .prologue
    .line 32
    invoke-virtual {p0}, Lcom/osp/app/signin/AccountHelpPreference;->a()Z

    move-result v0

    return v0
.end method

.method static synthetic b(Lcom/osp/app/signin/AccountHelpPreference;Ljava/lang/String;)Ljava/lang/String;
    .locals 4

    .prologue
    .line 32
    invoke-static {}, Lcom/osp/device/b;->b()Lcom/osp/device/b;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/osp/device/b;->i(Landroid/content/Context;)Z

    move-result v0

    invoke-static {}, Lcom/msc/c/g;->a()Lcom/msc/c/g;

    invoke-static {v0}, Lcom/msc/c/k;->a(Z)Ljava/lang/String;

    move-result-object v1

    invoke-static {}, Lcom/msc/c/g;->a()Lcom/msc/c/g;

    invoke-virtual {p0}, Lcom/osp/app/signin/AccountHelpPreference;->getApplicationContext()Landroid/content/Context;

    move-result-object v2

    invoke-static {p0}, Lcom/osp/app/util/ad;->g(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3, p1, v1, v0}, Lcom/msc/c/g;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Z)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method protected onActivityResult(IILandroid/content/Intent;)V
    .locals 1

    .prologue
    .line 91
    invoke-super {p0, p1, p2, p3}, Lcom/osp/app/util/BasePreferenceActivity;->onActivityResult(IILandroid/content/Intent;)V

    .line 92
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/osp/app/signin/AccountHelpPreference;->a:Z

    .line 94
    return-void
.end method

.method public onBackPressed()V
    .locals 1

    .prologue
    .line 65
    const/16 v0, 0xe

    invoke-virtual {p0, v0}, Lcom/osp/app/signin/AccountHelpPreference;->setResult(I)V

    .line 67
    invoke-super {p0}, Lcom/osp/app/util/BasePreferenceActivity;->onBackPressed()V

    .line 68
    return-void
.end method

.method public onConfigurationChanged(Landroid/content/res/Configuration;)V
    .locals 2

    .prologue
    .line 246
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v0, "AHP"

    const-string v1, "onConfigurationChanged()"

    invoke-static {v0, v1}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 248
    iget v0, p1, Landroid/content/res/Configuration;->orientation:I

    invoke-direct {p0, v0}, Lcom/osp/app/signin/AccountHelpPreference;->a(I)V

    .line 250
    invoke-super {p0, p1}, Lcom/osp/app/util/BasePreferenceActivity;->onConfigurationChanged(Landroid/content/res/Configuration;)V

    .line 251
    return-void
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 4

    .prologue
    const v3, 0x7f0800e7

    .line 99
    const/4 v0, 0x0

    invoke-static {v0}, Lcom/osp/app/signin/SamsungService;->a(Z)V

    .line 100
    invoke-super {p0, p1}, Lcom/osp/app/util/BasePreferenceActivity;->onCreate(Landroid/os/Bundle;)V

    .line 111
    const v0, 0x7f030005

    invoke-virtual {p0, v0}, Lcom/osp/app/signin/AccountHelpPreference;->setContentView(I)V

    .line 112
    const v0, 0x7f040003

    invoke-virtual {p0, v0}, Lcom/osp/app/signin/AccountHelpPreference;->addPreferencesFromResource(I)V

    .line 114
    const v0, 0x7f0c0037

    invoke-virtual {p0, v0}, Lcom/osp/app/signin/AccountHelpPreference;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 115
    const v1, 0x7f0c0036

    invoke-virtual {p0, v1}, Lcom/osp/app/signin/AccountHelpPreference;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/LinearLayout;

    .line 116
    const/16 v2, 0x8

    invoke-virtual {v1, v2}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 117
    invoke-virtual {v0}, Landroid/widget/TextView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v1

    check-cast v1, Landroid/view/ViewGroup$MarginLayoutParams;

    .line 118
    invoke-virtual {p0}, Lcom/osp/app/signin/AccountHelpPreference;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v2

    iput v2, v1, Landroid/view/ViewGroup$MarginLayoutParams;->leftMargin:I

    .line 119
    invoke-virtual {p0}, Lcom/osp/app/signin/AccountHelpPreference;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v2

    iput v2, v1, Landroid/view/ViewGroup$MarginLayoutParams;->rightMargin:I

    .line 120
    invoke-static {v0, v1}, Lcom/msc/sa/c/d;->a(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    .line 122
    invoke-virtual {p0}, Lcom/osp/app/signin/AccountHelpPreference;->a()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 124
    invoke-virtual {p0}, Lcom/osp/app/signin/AccountHelpPreference;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f070012

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setTextColor(I)V

    .line 129
    :goto_0
    invoke-static {}, Lcom/osp/device/b;->b()Lcom/osp/device/b;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/osp/device/b;->i(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 131
    invoke-virtual {p0}, Lcom/osp/app/signin/AccountHelpPreference;->a()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 133
    invoke-virtual {p0}, Lcom/osp/app/signin/AccountHelpPreference;->b()V

    .line 140
    :cond_0
    :goto_1
    invoke-virtual {p0}, Lcom/osp/app/signin/AccountHelpPreference;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v0

    iget v0, v0, Landroid/content/res/Configuration;->orientation:I

    invoke-direct {p0, v0}, Lcom/osp/app/signin/AccountHelpPreference;->a(I)V

    .line 141
    const-string v0, "find_email_or_password"

    invoke-virtual {p0, v0}, Lcom/osp/app/signin/AccountHelpPreference;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    check-cast v0, Landroid/preference/PreferenceScreen;

    .line 142
    new-instance v1, Lcom/osp/app/signin/d;

    invoke-direct {v1, p0}, Lcom/osp/app/signin/d;-><init>(Lcom/osp/app/signin/AccountHelpPreference;)V

    invoke-virtual {v0, v1}, Landroid/preference/PreferenceScreen;->setOnPreferenceClickListener(Landroid/preference/Preference$OnPreferenceClickListener;)V

    .line 163
    const-string v0, "support"

    invoke-virtual {p0, v0}, Lcom/osp/app/signin/AccountHelpPreference;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    check-cast v0, Landroid/preference/PreferenceScreen;

    .line 164
    new-instance v1, Lcom/osp/app/signin/e;

    invoke-direct {v1, p0}, Lcom/osp/app/signin/e;-><init>(Lcom/osp/app/signin/AccountHelpPreference;)V

    invoke-virtual {v0, v1}, Landroid/preference/PreferenceScreen;->setOnPreferenceClickListener(Landroid/preference/Preference$OnPreferenceClickListener;)V

    .line 196
    const-string v0, "contact_us"

    invoke-virtual {p0, v0}, Lcom/osp/app/signin/AccountHelpPreference;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    check-cast v0, Landroid/preference/PreferenceScreen;

    .line 197
    new-instance v1, Lcom/osp/app/signin/f;

    invoke-direct {v1, p0}, Lcom/osp/app/signin/f;-><init>(Lcom/osp/app/signin/AccountHelpPreference;)V

    invoke-virtual {v0, v1}, Landroid/preference/PreferenceScreen;->setOnPreferenceClickListener(Landroid/preference/Preference$OnPreferenceClickListener;)V

    .line 227
    invoke-static {p0}, Lcom/msc/sa/c/d;->a(Landroid/app/Activity;)V

    .line 229
    return-void

    .line 127
    :cond_1
    invoke-virtual {p0}, Lcom/osp/app/signin/AccountHelpPreference;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f070013

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setTextColor(I)V

    goto :goto_0

    .line 136
    :cond_2
    invoke-virtual {p0}, Lcom/osp/app/signin/AccountHelpPreference;->c()V

    goto :goto_1
.end method

.method public onOptionsItemSelected(Landroid/view/MenuItem;)Z
    .locals 2

    .prologue
    .line 298
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0xb

    if-lt v0, v1, :cond_0

    .line 300
    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    .line 304
    :cond_0
    :goto_0
    invoke-super {p0, p1}, Lcom/osp/app/util/BasePreferenceActivity;->onOptionsItemSelected(Landroid/view/MenuItem;)Z

    move-result v0

    return v0

    .line 303
    :pswitch_0
    invoke-virtual {p0}, Lcom/osp/app/signin/AccountHelpPreference;->onBackPressed()V

    goto :goto_0

    .line 300
    :pswitch_data_0
    .packed-switch 0x102002c
        :pswitch_0
    .end packed-switch
.end method

.method protected onResume()V
    .locals 1

    .prologue
    .line 276
    invoke-super {p0}, Lcom/osp/app/util/BasePreferenceActivity;->onResume()V

    .line 278
    iget-boolean v0, p0, Lcom/osp/app/signin/AccountHelpPreference;->a:Z

    if-eqz v0, :cond_0

    .line 280
    invoke-virtual {p0}, Lcom/osp/app/signin/AccountHelpPreference;->finish()V

    .line 282
    :cond_0
    return-void
.end method
