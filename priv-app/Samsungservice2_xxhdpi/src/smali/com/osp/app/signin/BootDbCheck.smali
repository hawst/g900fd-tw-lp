.class public Lcom/osp/app/signin/BootDbCheck;
.super Lcom/osp/app/util/AbstractBaseService;
.source "BootDbCheck.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 31
    invoke-direct {p0}, Lcom/osp/app/util/AbstractBaseService;-><init>()V

    return-void
.end method


# virtual methods
.method public onBind(Landroid/content/Intent;)Landroid/os/IBinder;
    .locals 1

    .prologue
    .line 93
    const-string v0, "onBind"

    invoke-virtual {p0, p1, v0}, Lcom/osp/app/signin/BootDbCheck;->a(Landroid/content/Intent;Ljava/lang/String;)V

    .line 94
    const/4 v0, 0x0

    return-object v0
.end method

.method public onCreate()V
    .locals 2

    .prologue
    .line 99
    invoke-super {p0}, Lcom/osp/app/util/AbstractBaseService;->onCreate()V

    .line 100
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v0, "BDC"

    const-string v1, "create"

    invoke-static {v0, v1}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 102
    return-void
.end method

.method public onDestroy()V
    .locals 2

    .prologue
    .line 106
    invoke-super {p0}, Lcom/osp/app/util/AbstractBaseService;->onDestroy()V

    .line 107
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v0, "BDC"

    const-string v1, "destroy"

    invoke-static {v0, v1}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 109
    return-void
.end method

.method public onStartCommand(Landroid/content/Intent;II)I
    .locals 7

    .prologue
    .line 113
    if-eqz p1, :cond_4

    .line 115
    const-string v0, "MODE"

    invoke-virtual {p1, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 116
    const-string v1, "SIGNIN_FAIL"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    const-string v1, "SIGNIN_CANCEL"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    const-string v1, "SIGN_UP"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 118
    :cond_0
    :try_start_0
    new-instance v0, Lcom/osp/security/identity/d;

    invoke-direct {v0, p0}, Lcom/osp/security/identity/d;-><init>(Landroid/content/Context;)V

    new-instance v1, Lcom/osp/security/credential/a;

    invoke-direct {v1, p0}, Lcom/osp/security/credential/a;-><init>(Landroid/content/Context;)V

    new-instance v2, Lcom/osp/social/member/d;

    invoke-direct {v2, p0}, Lcom/osp/social/member/d;-><init>(Landroid/content/Context;)V

    invoke-virtual {v0}, Lcom/osp/security/identity/d;->k()V

    invoke-virtual {v1}, Lcom/osp/security/credential/a;->a()V

    invoke-virtual {v2}, Lcom/osp/social/member/d;->a()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    invoke-virtual {p0}, Lcom/osp/app/signin/BootDbCheck;->stopSelf()V

    .line 127
    :goto_1
    const/4 v0, 0x2

    return v0

    .line 118
    :catch_0
    move-exception v0

    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    invoke-virtual {p0}, Lcom/osp/app/signin/BootDbCheck;->stopSelf()V

    goto :goto_0

    .line 121
    :cond_1
    :try_start_1
    new-instance v0, Lcom/osp/security/identity/d;

    invoke-direct {v0, p0}, Lcom/osp/security/identity/d;-><init>(Landroid/content/Context;)V

    new-instance v1, Lcom/osp/security/credential/a;

    invoke-direct {v1, p0}, Lcom/osp/security/credential/a;-><init>(Landroid/content/Context;)V

    new-instance v2, Lcom/osp/social/member/d;

    invoke-direct {v2, p0}, Lcom/osp/social/member/d;-><init>(Landroid/content/Context;)V

    new-instance v3, Lcom/osp/common/property/a;

    invoke-direct {v3, p0}, Lcom/osp/common/property/a;-><init>(Landroid/content/Context;)V

    const-string v4, "SignOut"

    invoke-virtual {v3, v4}, Lcom/osp/common/property/a;->a(Ljava/lang/String;)Z

    move-result v4

    const/4 v5, 0x1

    if-ne v4, v5, :cond_2

    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v4, "BDC"

    const-string v5, "remove db(signout flag is true)"

    const-string v6, "START"

    invoke-static {v4, v5, v6}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v0}, Lcom/osp/security/identity/d;->k()V

    invoke-virtual {v1}, Lcom/osp/security/credential/a;->a()V

    invoke-virtual {v2}, Lcom/osp/social/member/d;->a()V

    const-string v4, "SignOut"

    invoke-virtual {v3, v4}, Lcom/osp/common/property/a;->c(Ljava/lang/String;)V

    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v3, "BDC"

    const-string v4, "remove db(signout flag is true)"

    const-string v5, "END"

    invoke-static {v3, v4, v5}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    :cond_2
    invoke-static {p0}, Landroid/accounts/AccountManager;->get(Landroid/content/Context;)Landroid/accounts/AccountManager;

    move-result-object v3

    const-string v4, "com.osp.app.signin"

    invoke-virtual {v3, v4}, Landroid/accounts/AccountManager;->getAccountsByType(Ljava/lang/String;)[Landroid/accounts/Account;

    move-result-object v3

    array-length v3, v3

    if-nez v3, :cond_3

    invoke-static {p0}, Lcom/osp/app/util/ad;->h(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v3

    if-eqz v3, :cond_3

    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v3, "BDC"

    const-string v4, "remove db(signin flag is false)"

    const-string v5, "START"

    invoke-static {v3, v4, v5}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v0}, Lcom/osp/security/identity/d;->k()V

    invoke-virtual {v1}, Lcom/osp/security/credential/a;->a()V

    invoke-virtual {v2}, Lcom/osp/social/member/d;->a()V

    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v0, "BDC"

    const-string v1, "remove db(signin flag is false)"

    const-string v2, "END"

    invoke-static {v0, v1, v2}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1

    :cond_3
    :goto_2
    invoke-virtual {p0}, Lcom/osp/app/signin/BootDbCheck;->stopSelf()V

    goto/16 :goto_1

    :catch_1
    move-exception v0

    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    invoke-virtual {p0}, Lcom/osp/app/signin/BootDbCheck;->stopSelf()V

    goto :goto_2

    .line 125
    :cond_4
    invoke-virtual {p0}, Lcom/osp/app/signin/BootDbCheck;->stopSelf()V

    goto/16 :goto_1
.end method
