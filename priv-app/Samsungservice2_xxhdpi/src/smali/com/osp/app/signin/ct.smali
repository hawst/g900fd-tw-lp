.class final Lcom/osp/app/signin/ct;
.super Lcom/msc/c/b;
.source "BackgroundModeService.java"


# instance fields
.field final synthetic c:Lcom/osp/app/signin/BackgroundModeService;

.field private d:J

.field private e:J

.field private f:J

.field private g:Ljava/lang/String;

.field private h:Ljava/lang/String;

.field private i:Ljava/lang/String;

.field private j:Ljava/lang/String;

.field private final k:Landroid/content/Context;

.field private l:Z

.field private m:Ljava/lang/String;

.field private final n:I


# direct methods
.method public constructor <init>(Lcom/osp/app/signin/BackgroundModeService;Landroid/content/Context;I)V
    .locals 2

    .prologue
    const/4 v0, 0x0

    .line 3594
    iput-object p1, p0, Lcom/osp/app/signin/ct;->c:Lcom/osp/app/signin/BackgroundModeService;

    .line 3595
    invoke-direct {p0, p2}, Lcom/msc/c/b;-><init>(Landroid/content/Context;)V

    .line 3585
    iput-object v0, p0, Lcom/osp/app/signin/ct;->g:Ljava/lang/String;

    .line 3586
    iput-object v0, p0, Lcom/osp/app/signin/ct;->h:Ljava/lang/String;

    .line 3587
    iput-object v0, p0, Lcom/osp/app/signin/ct;->i:Ljava/lang/String;

    .line 3588
    iput-object v0, p0, Lcom/osp/app/signin/ct;->j:Ljava/lang/String;

    .line 3591
    const-string v0, "Fail"

    iput-object v0, p0, Lcom/osp/app/signin/ct;->m:Ljava/lang/String;

    .line 3596
    invoke-virtual {p0}, Lcom/osp/app/signin/ct;->c()V

    .line 3597
    iput p3, p0, Lcom/osp/app/signin/ct;->n:I

    .line 3598
    iput-object p2, p0, Lcom/osp/app/signin/ct;->k:Landroid/content/Context;

    .line 3599
    invoke-static {}, Lcom/osp/device/b;->b()Lcom/osp/device/b;

    move-result-object v0

    invoke-virtual {v0}, Lcom/osp/device/b;->e()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/osp/app/signin/ct;->g:Ljava/lang/String;

    .line 3600
    invoke-static {}, Lcom/osp/device/b;->b()Lcom/osp/device/b;

    move-result-object v0

    invoke-virtual {v0}, Lcom/osp/device/b;->c()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/osp/app/signin/ct;->h:Ljava/lang/String;

    .line 3601
    iget-object v0, p0, Lcom/osp/app/signin/ct;->k:Landroid/content/Context;

    iget-object v0, p0, Lcom/osp/app/signin/ct;->k:Landroid/content/Context;

    invoke-static {v0}, Lcom/osp/app/util/ad;->p(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/osp/common/util/i;->d(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/osp/app/signin/ct;->i:Ljava/lang/String;

    .line 3602
    invoke-static {}, Lcom/osp/common/util/i;->a()Lcom/osp/common/util/i;

    iget-object v0, p0, Lcom/osp/app/signin/ct;->k:Landroid/content/Context;

    invoke-static {v0}, Lcom/osp/common/util/i;->l(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/osp/app/signin/ct;->j:Ljava/lang/String;

    .line 3604
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "MarketingPopupCheckTask mDevice_Model = ["

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v1, p0, Lcom/osp/app/signin/ct;->g:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "]\t mDevice_CustomerCode = ["

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/osp/app/signin/ct;->h:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "]\t mDevice_CountryCode = ["

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/osp/app/signin/ct;->i:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "]\t mDevice_LanguageCode = ["

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/osp/app/signin/ct;->j:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "]"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;)V

    .line 3609
    return-void
.end method

.method private a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 8

    .prologue
    .line 3819
    invoke-static {}, Lcom/msc/c/g;->a()Lcom/msc/c/g;

    .line 3820
    iget-object v0, p0, Lcom/osp/app/signin/ct;->k:Landroid/content/Context;

    const/4 v6, 0x0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    move-object v5, p5

    move-object v7, p0

    invoke-static/range {v0 .. v7}, Lcom/msc/c/g;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ILcom/msc/b/h;)J

    move-result-wide v0

    iput-wide v0, p0, Lcom/osp/app/signin/ct;->d:J

    .line 3821
    iget-wide v0, p0, Lcom/osp/app/signin/ct;->d:J

    const-string v2, "none"

    invoke-virtual {p0, v0, v1, v2}, Lcom/osp/app/signin/ct;->a(JLjava/lang/String;)V

    .line 3822
    iget-wide v0, p0, Lcom/osp/app/signin/ct;->d:J

    invoke-virtual {p0, v0, v1}, Lcom/osp/app/signin/ct;->a(J)V

    .line 3823
    iget-wide v0, p0, Lcom/osp/app/signin/ct;->d:J

    sget-object v2, Lcom/msc/b/g;->c:Lcom/msc/b/g;

    invoke-static {v0, v1, v2}, Lcom/msc/c/g;->a(JLcom/msc/b/g;)V

    .line 3825
    return-void
.end method

.method private b(Ljava/lang/String;)V
    .locals 3

    .prologue
    .line 3802
    invoke-static {}, Lcom/msc/c/g;->a()Lcom/msc/c/g;

    .line 3803
    iget-object v0, p0, Lcom/osp/app/signin/ct;->k:Landroid/content/Context;

    iget-object v1, p0, Lcom/osp/app/signin/ct;->c:Lcom/osp/app/signin/BackgroundModeService;

    invoke-static {v1}, Lcom/msc/c/e;->h(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1, p1, p0}, Lcom/msc/c/g;->d(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Lcom/msc/b/h;)J

    move-result-wide v0

    iput-wide v0, p0, Lcom/osp/app/signin/ct;->e:J

    .line 3804
    iget-wide v0, p0, Lcom/osp/app/signin/ct;->e:J

    invoke-virtual {p0, v0, v1}, Lcom/osp/app/signin/ct;->a(J)V

    .line 3805
    iget-wide v0, p0, Lcom/osp/app/signin/ct;->e:J

    const-string v2, "from_xml"

    invoke-virtual {p0, v0, v1, v2}, Lcom/osp/app/signin/ct;->a(JLjava/lang/String;)V

    .line 3806
    iget-wide v0, p0, Lcom/osp/app/signin/ct;->e:J

    sget-object v2, Lcom/msc/b/g;->b:Lcom/msc/b/g;

    invoke-static {v0, v1, v2}, Lcom/msc/c/g;->a(JLcom/msc/b/g;)V

    .line 3807
    return-void
.end method

.method private h()V
    .locals 8

    .prologue
    const/4 v3, 0x0

    .line 3791
    invoke-static {}, Lcom/msc/c/g;->a()Lcom/msc/c/g;

    .line 3792
    iget-object v0, p0, Lcom/osp/app/signin/ct;->k:Landroid/content/Context;

    const-string v1, "j5p7ll8g33"

    const-string v2, "5763D0052DC1462E13751F753384E9A9"

    iget-object v4, p0, Lcom/osp/app/signin/ct;->c:Lcom/osp/app/signin/BackgroundModeService;

    invoke-static {v4}, Lcom/msc/c/e;->g(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v5

    move-object v4, v3

    move-object v6, v3

    move-object v7, p0

    invoke-static/range {v0 .. v7}, Lcom/msc/c/g;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/osp/app/signin/cn;Lcom/msc/b/h;)J

    move-result-wide v0

    iput-wide v0, p0, Lcom/osp/app/signin/ct;->f:J

    .line 3795
    iget-wide v0, p0, Lcom/osp/app/signin/ct;->f:J

    invoke-virtual {p0, v0, v1}, Lcom/osp/app/signin/ct;->b(J)V

    .line 3796
    iget-wide v0, p0, Lcom/osp/app/signin/ct;->f:J

    const-string v2, "from_json"

    invoke-virtual {p0, v0, v1, v2}, Lcom/osp/app/signin/ct;->a(JLjava/lang/String;)V

    .line 3798
    iget-wide v0, p0, Lcom/osp/app/signin/ct;->f:J

    sget-object v2, Lcom/msc/b/g;->c:Lcom/msc/b/g;

    invoke-static {v0, v1, v2}, Lcom/msc/c/g;->a(JLcom/msc/b/g;)V

    .line 3799
    return-void
.end method


# virtual methods
.method protected final varargs a()Ljava/lang/Boolean;
    .locals 6

    .prologue
    .line 3619
    invoke-static {}, Lcom/osp/app/signin/SamsungService;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v4

    .line 3621
    iget-object v0, p0, Lcom/osp/app/signin/ct;->i:Ljava/lang/String;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/osp/app/signin/ct;->i:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v0

    if-lez v0, :cond_1

    .line 3623
    iget-object v1, p0, Lcom/osp/app/signin/ct;->g:Ljava/lang/String;

    iget-object v2, p0, Lcom/osp/app/signin/ct;->h:Ljava/lang/String;

    iget-object v3, p0, Lcom/osp/app/signin/ct;->i:Ljava/lang/String;

    iget-object v5, p0, Lcom/osp/app/signin/ct;->j:Ljava/lang/String;

    move-object v0, p0

    invoke-direct/range {v0 .. v5}, Lcom/osp/app/signin/ct;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 3635
    :goto_1
    const/4 v0, 0x0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    return-object v0

    .line 3619
    :cond_0
    const/4 v0, 0x2

    goto :goto_0

    .line 3626
    :cond_1
    iget-object v0, p0, Lcom/osp/app/signin/ct;->h:Ljava/lang/String;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/osp/app/signin/ct;->h:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v0

    if-lez v0, :cond_2

    .line 3628
    iget-object v1, p0, Lcom/osp/app/signin/ct;->g:Ljava/lang/String;

    iget-object v2, p0, Lcom/osp/app/signin/ct;->h:Ljava/lang/String;

    const-string v3, "USA"

    const-string v5, "ENG"

    move-object v0, p0

    invoke-direct/range {v0 .. v5}, Lcom/osp/app/signin/ct;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1

    .line 3631
    :cond_2
    iget-object v1, p0, Lcom/osp/app/signin/ct;->g:Ljava/lang/String;

    const-string v2, "ATT"

    const-string v3, "USA"

    const-string v5, "ENG"

    move-object v0, p0

    invoke-direct/range {v0 .. v5}, Lcom/osp/app/signin/ct;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1
.end method

.method public final declared-synchronized a(Lcom/msc/b/c;)V
    .locals 6

    .prologue
    .line 3658
    monitor-enter p0

    :try_start_0
    invoke-super {p0, p1}, Lcom/msc/c/b;->a(Lcom/msc/b/c;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 3660
    if-nez p1, :cond_1

    .line 3732
    :cond_0
    :goto_0
    monitor-exit p0

    return-void

    .line 3665
    :cond_1
    :try_start_1
    invoke-virtual {p1}, Lcom/msc/b/c;->a()J

    move-result-wide v0

    .line 3666
    invoke-virtual {p1}, Lcom/msc/b/c;->e()Ljava/lang/String;

    move-result-object v2

    .line 3668
    iget-wide v4, p0, Lcom/osp/app/signin/ct;->d:J
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    cmp-long v3, v0, v4

    if-nez v3, :cond_4

    .line 3672
    :try_start_2
    iget-object v0, p0, Lcom/osp/app/signin/ct;->c:Lcom/osp/app/signin/BackgroundModeService;

    invoke-static {}, Lcom/msc/c/h;->a()Lcom/msc/c/h;

    iget-object v1, p0, Lcom/osp/app/signin/ct;->k:Landroid/content/Context;

    invoke-static {v2}, Lcom/msc/c/h;->D(Ljava/lang/String;)Lcom/osp/app/signin/fv;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/osp/app/signin/BackgroundModeService;->b(Lcom/osp/app/signin/BackgroundModeService;Lcom/osp/app/signin/fv;)Lcom/osp/app/signin/fv;

    .line 3673
    iget-object v0, p0, Lcom/osp/app/signin/ct;->c:Lcom/osp/app/signin/BackgroundModeService;

    invoke-static {v0}, Lcom/osp/app/signin/BackgroundModeService;->e(Lcom/osp/app/signin/BackgroundModeService;)Lcom/osp/app/signin/fv;

    move-result-object v0

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/osp/app/signin/ct;->c:Lcom/osp/app/signin/BackgroundModeService;

    invoke-static {v0}, Lcom/osp/app/signin/BackgroundModeService;->e(Lcom/osp/app/signin/BackgroundModeService;)Lcom/osp/app/signin/fv;

    move-result-object v0

    invoke-virtual {v0}, Lcom/osp/app/signin/fv;->j()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 3675
    iget-object v0, p0, Lcom/osp/app/signin/ct;->c:Lcom/osp/app/signin/BackgroundModeService;

    invoke-static {v0}, Lcom/msc/c/e;->b(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v0

    .line 3676
    if-eqz v0, :cond_2

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v1

    if-lez v1, :cond_2

    .line 3678
    invoke-direct {p0, v0}, Lcom/osp/app/signin/ct;->b(Ljava/lang/String;)V
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_0
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0

    .line 3688
    :catch_0
    move-exception v0

    :try_start_3
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    goto :goto_0

    .line 3658
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 3681
    :cond_2
    :try_start_4
    invoke-direct {p0}, Lcom/osp/app/signin/ct;->h()V

    goto :goto_0

    .line 3685
    :cond_3
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v0, "MarketingPopupInfo is null"

    invoke-static {v0}, Lcom/osp/app/util/aq;->c(Ljava/lang/String;)V
    :try_end_4
    .catch Ljava/lang/Exception; {:try_start_4 .. :try_end_4} :catch_0
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    goto :goto_0

    .line 3692
    :cond_4
    :try_start_5
    iget-wide v4, p0, Lcom/osp/app/signin/ct;->f:J
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    cmp-long v3, v0, v4

    if-nez v3, :cond_6

    .line 3696
    :try_start_6
    new-instance v0, Lcom/msc/a/d;

    invoke-direct {v0}, Lcom/msc/a/d;-><init>()V

    .line 3697
    invoke-static {}, Lcom/msc/c/h;->a()Lcom/msc/c/h;

    invoke-static {v2, v0}, Lcom/msc/c/h;->a(Ljava/lang/String;Lcom/msc/a/d;)V

    .line 3698
    invoke-virtual {v0}, Lcom/msc/a/d;->g()Ljava/lang/String;

    move-result-object v0

    .line 3699
    if-eqz v0, :cond_5

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v1

    if-lez v1, :cond_5

    .line 3701
    iget-object v1, p0, Lcom/osp/app/signin/ct;->c:Lcom/osp/app/signin/BackgroundModeService;

    invoke-static {v1, v0}, Lcom/msc/c/e;->b(Landroid/content/Context;Ljava/lang/String;)V

    .line 3702
    invoke-direct {p0, v0}, Lcom/osp/app/signin/ct;->b(Ljava/lang/String;)V
    :try_end_6
    .catch Ljava/lang/Exception; {:try_start_6 .. :try_end_6} :catch_1
    .catchall {:try_start_6 .. :try_end_6} :catchall_0

    goto :goto_0

    .line 3708
    :catch_1
    move-exception v0

    .line 3710
    :try_start_7
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    .line 3711
    new-instance v1, Lcom/msc/c/f;

    invoke-direct {v1, v0}, Lcom/msc/c/f;-><init>(Ljava/lang/Exception;)V

    iput-object v1, p0, Lcom/osp/app/signin/ct;->b:Lcom/msc/c/f;
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_0

    goto/16 :goto_0

    .line 3705
    :cond_5
    :try_start_8
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v0, "Fail to get accessToken"

    invoke-static {v0}, Lcom/osp/app/util/aq;->c(Ljava/lang/String;)V

    .line 3706
    new-instance v0, Lcom/msc/c/f;

    invoke-direct {v0}, Lcom/msc/c/f;-><init>()V

    iput-object v0, p0, Lcom/osp/app/signin/ct;->b:Lcom/msc/c/f;
    :try_end_8
    .catch Ljava/lang/Exception; {:try_start_8 .. :try_end_8} :catch_1
    .catchall {:try_start_8 .. :try_end_8} :catchall_0

    goto/16 :goto_0

    .line 3713
    :cond_6
    :try_start_9
    iget-wide v4, p0, Lcom/osp/app/signin/ct;->e:J
    :try_end_9
    .catchall {:try_start_9 .. :try_end_9} :catchall_0

    cmp-long v0, v0, v4

    if-nez v0, :cond_0

    .line 3717
    :try_start_a
    invoke-static {}, Lcom/msc/c/h;->a()Lcom/msc/c/h;

    invoke-static {v2}, Lcom/msc/c/h;->k(Ljava/lang/String;)Lcom/osp/app/signin/SignUpinfo;

    move-result-object v0

    .line 3718
    if-eqz v0, :cond_7

    invoke-virtual {v0}, Lcom/osp/app/signin/SignUpinfo;->l()Z

    move-result v0

    if-eqz v0, :cond_7

    .line 3720
    const-string v0, "Success"

    iput-object v0, p0, Lcom/osp/app/signin/ct;->m:Ljava/lang/String;

    .line 3721
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v0, "BMS"

    const-string v1, "Marketing notification received"

    invoke-static {v0, v1}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_a
    .catch Ljava/lang/Exception; {:try_start_a .. :try_end_a} :catch_2
    .catchall {:try_start_a .. :try_end_a} :catchall_0

    goto/16 :goto_0

    .line 3726
    :catch_2
    move-exception v0

    .line 3728
    :try_start_b
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    .line 3729
    new-instance v1, Lcom/msc/c/f;

    invoke-direct {v1, v0}, Lcom/msc/c/f;-><init>(Ljava/lang/Exception;)V

    iput-object v1, p0, Lcom/osp/app/signin/ct;->b:Lcom/msc/c/f;
    :try_end_b
    .catchall {:try_start_b .. :try_end_b} :catchall_0

    goto/16 :goto_0

    .line 3724
    :cond_7
    :try_start_c
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v0, "BMS"

    const-string v1, "Marketing notification do not received"

    invoke-static {v0, v1}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_c
    .catch Ljava/lang/Exception; {:try_start_c .. :try_end_c} :catch_2
    .catchall {:try_start_c .. :try_end_c} :catchall_0

    goto/16 :goto_0
.end method

.method protected final a(Ljava/lang/Boolean;)V
    .locals 2

    .prologue
    .line 3640
    invoke-super {p0, p1}, Lcom/msc/c/b;->a(Ljava/lang/Boolean;)V

    .line 3641
    const-string v0, "Success"

    iget-object v1, p0, Lcom/osp/app/signin/ct;->m:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 3643
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v0, "BMS"

    const-string v1, "Marketing Notification success"

    invoke-static {v0, v1}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 3644
    iget-object v0, p0, Lcom/osp/app/signin/ct;->c:Lcom/osp/app/signin/BackgroundModeService;

    invoke-static {v0}, Lcom/osp/app/signin/BackgroundModeService;->e(Lcom/osp/app/signin/BackgroundModeService;)Lcom/osp/app/signin/fv;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/osp/app/signin/ct;->c:Lcom/osp/app/signin/BackgroundModeService;

    invoke-static {v0}, Lcom/osp/app/signin/BackgroundModeService;->e(Lcom/osp/app/signin/BackgroundModeService;)Lcom/osp/app/signin/fv;

    move-result-object v0

    invoke-virtual {v0}, Lcom/osp/app/signin/fv;->j()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 3646
    iget-object v0, p0, Lcom/osp/app/signin/ct;->c:Lcom/osp/app/signin/BackgroundModeService;

    iget-object v1, p0, Lcom/osp/app/signin/ct;->c:Lcom/osp/app/signin/BackgroundModeService;

    invoke-static {v1}, Lcom/osp/app/signin/BackgroundModeService;->e(Lcom/osp/app/signin/BackgroundModeService;)Lcom/osp/app/signin/fv;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/osp/app/signin/BackgroundModeService;->a(Lcom/osp/app/signin/BackgroundModeService;Lcom/osp/app/signin/fv;)V

    .line 3653
    :cond_0
    :goto_0
    iget-object v0, p0, Lcom/osp/app/signin/ct;->c:Lcom/osp/app/signin/BackgroundModeService;

    iget v1, p0, Lcom/osp/app/signin/ct;->n:I

    invoke-virtual {v0, v1}, Lcom/osp/app/signin/BackgroundModeService;->stopSelf(I)V

    .line 3654
    return-void

    .line 3650
    :cond_1
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v0, "BMS"

    const-string v1, "Marketing Notification failed"

    invoke-static {v0, v1}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public final b(Lcom/msc/b/c;)V
    .locals 6

    .prologue
    .line 3736
    invoke-super {p0, p1}, Lcom/msc/c/b;->b(Lcom/msc/b/c;)V

    .line 3738
    if-nez p1, :cond_1

    .line 3787
    :cond_0
    :goto_0
    return-void

    .line 3743
    :cond_1
    invoke-virtual {p1}, Lcom/msc/b/c;->a()J

    move-result-wide v0

    .line 3744
    invoke-virtual {p1}, Lcom/msc/b/c;->e()Ljava/lang/String;

    move-result-object v2

    .line 3746
    iget-wide v4, p0, Lcom/osp/app/signin/ct;->f:J

    cmp-long v3, v0, v4

    if-nez v3, :cond_4

    .line 3748
    if-eqz v2, :cond_0

    .line 3750
    const-string v0, "AUT_1302"

    iget-object v1, p0, Lcom/osp/app/signin/ct;->b:Lcom/msc/c/f;

    invoke-virtual {v1}, Lcom/msc/c/f;->a()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_2

    const-string v0, "AUT_1830"

    iget-object v1, p0, Lcom/osp/app/signin/ct;->b:Lcom/msc/c/f;

    invoke-virtual {v1}, Lcom/msc/c/f;->a()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 3752
    :cond_2
    iget-object v0, p0, Lcom/osp/app/signin/ct;->c:Lcom/osp/app/signin/BackgroundModeService;

    invoke-static {v0}, Lcom/msc/c/c;->g(Landroid/content/Context;)V

    .line 3753
    iget-object v0, p0, Lcom/osp/app/signin/ct;->c:Lcom/osp/app/signin/BackgroundModeService;

    invoke-static {v0}, Lcom/msc/c/e;->a(Landroid/content/Context;)Z

    .line 3756
    iget-object v0, p0, Lcom/osp/app/signin/ct;->c:Lcom/osp/app/signin/BackgroundModeService;

    new-instance v1, Lcom/osp/app/util/d;

    invoke-direct {v1}, Lcom/osp/app/util/d;-><init>()V

    invoke-static {v0, v1}, Lcom/osp/app/signin/BackgroundModeService;->a(Lcom/osp/app/signin/BackgroundModeService;Lcom/osp/app/util/d;)V

    goto :goto_0

    .line 3758
    :cond_3
    invoke-static {}, Lcom/osp/device/b;->b()Lcom/osp/device/b;

    move-result-object v0

    invoke-virtual {v0}, Lcom/osp/device/b;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    const-string v0, "AUT_1094"

    iget-object v1, p0, Lcom/osp/app/signin/ct;->b:Lcom/msc/c/f;

    invoke-virtual {v1}, Lcom/msc/c/f;->a()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 3760
    iget-object v0, p0, Lcom/osp/app/signin/ct;->c:Lcom/osp/app/signin/BackgroundModeService;

    new-instance v1, Lcom/osp/app/util/d;

    invoke-direct {v1}, Lcom/osp/app/util/d;-><init>()V

    invoke-static {v0, v1}, Lcom/osp/app/signin/BackgroundModeService;->b(Lcom/osp/app/signin/BackgroundModeService;Lcom/osp/app/util/d;)V

    goto :goto_0

    .line 3763
    :cond_4
    iget-wide v4, p0, Lcom/osp/app/signin/ct;->e:J

    cmp-long v0, v0, v4

    if-nez v0, :cond_0

    .line 3765
    if-eqz v2, :cond_0

    .line 3767
    iget-boolean v0, p0, Lcom/osp/app/signin/ct;->l:Z

    if-nez v0, :cond_0

    const-string v0, "ACF_0403"

    iget-object v1, p0, Lcom/osp/app/signin/ct;->b:Lcom/msc/c/f;

    invoke-virtual {v1}, Lcom/msc/c/f;->a()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 3771
    const/4 v0, 0x0

    :try_start_0
    iput-object v0, p0, Lcom/osp/app/signin/ct;->b:Lcom/msc/c/f;

    .line 3772
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/osp/app/signin/ct;->l:Z

    .line 3773
    iget-object v0, p0, Lcom/osp/app/signin/ct;->c:Lcom/osp/app/signin/BackgroundModeService;

    const-string v1, ""

    invoke-static {v0, v1}, Lcom/msc/c/e;->b(Landroid/content/Context;Ljava/lang/String;)V

    .line 3774
    invoke-virtual {p0}, Lcom/osp/app/signin/ct;->isCancelled()Z

    move-result v0

    if-nez v0, :cond_0

    .line 3778
    invoke-direct {p0}, Lcom/osp/app/signin/ct;->h()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto/16 :goto_0

    .line 3779
    :catch_0
    move-exception v0

    .line 3781
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    .line 3782
    new-instance v1, Lcom/msc/c/f;

    invoke-direct {v1, v0}, Lcom/msc/c/f;-><init>(Ljava/lang/Exception;)V

    iput-object v1, p0, Lcom/osp/app/signin/ct;->b:Lcom/msc/c/f;

    goto/16 :goto_0
.end method

.method protected final synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 3579
    invoke-virtual {p0}, Lcom/osp/app/signin/ct;->a()Ljava/lang/Boolean;

    move-result-object v0

    return-object v0
.end method

.method protected final synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 3579
    check-cast p1, Ljava/lang/Boolean;

    invoke-virtual {p0, p1}, Lcom/osp/app/signin/ct;->a(Ljava/lang/Boolean;)V

    return-void
.end method

.method protected final onPreExecute()V
    .locals 0

    .prologue
    .line 3613
    invoke-super {p0}, Lcom/msc/c/b;->onPreExecute()V

    .line 3614
    return-void
.end method
