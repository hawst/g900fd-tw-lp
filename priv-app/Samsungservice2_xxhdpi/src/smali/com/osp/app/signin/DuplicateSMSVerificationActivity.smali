.class public Lcom/osp/app/signin/DuplicateSMSVerificationActivity;
.super Lcom/osp/app/util/BaseActivity;
.source "DuplicateSMSVerificationActivity.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field private A:Ljava/lang/String;

.field private B:Ljava/lang/String;

.field private C:I

.field private D:Z

.field private E:Ljava/lang/String;

.field private F:Ljava/lang/String;

.field private G:Lcom/osp/app/signin/et;

.field private H:Z

.field private final I:I

.field private final J:I

.field private K:Landroid/content/Intent;

.field private L:Z

.field private M:Ljava/lang/String;

.field private N:Lcom/osp/app/signin/BottomSoftkeyLinearLayout;

.field private O:Ljava/lang/String;

.field private final P:Landroid/content/BroadcastReceiver;

.field private final Q:Landroid/os/Handler;

.field final a:Landroid/view/View$OnClickListener;

.field final b:Landroid/view/View$OnClickListener;

.field private final c:Ljava/lang/String;

.field private final d:I

.field private e:Ljava/lang/String;

.field private final f:Ljava/lang/String;

.field private y:Ljava/lang/String;

.field private z:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x0

    .line 64
    invoke-direct {p0}, Lcom/osp/app/util/BaseActivity;-><init>()V

    .line 66
    const-string v0, "DSMSV"

    iput-object v0, p0, Lcom/osp/app/signin/DuplicateSMSVerificationActivity;->c:Ljava/lang/String;

    .line 68
    const/16 v0, 0x3c

    iput v0, p0, Lcom/osp/app/signin/DuplicateSMSVerificationActivity;->d:I

    .line 93
    const-string v0, "Account:"

    iput-object v0, p0, Lcom/osp/app/signin/DuplicateSMSVerificationActivity;->e:Ljava/lang/String;

    .line 98
    const-string v0, ":"

    iput-object v0, p0, Lcom/osp/app/signin/DuplicateSMSVerificationActivity;->f:Ljava/lang/String;

    .line 110
    const/4 v0, -0x1

    iput v0, p0, Lcom/osp/app/signin/DuplicateSMSVerificationActivity;->C:I

    .line 111
    iput-boolean v2, p0, Lcom/osp/app/signin/DuplicateSMSVerificationActivity;->D:Z

    .line 115
    iput-object v1, p0, Lcom/osp/app/signin/DuplicateSMSVerificationActivity;->E:Ljava/lang/String;

    .line 116
    iput-object v1, p0, Lcom/osp/app/signin/DuplicateSMSVerificationActivity;->F:Ljava/lang/String;

    .line 123
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/osp/app/signin/DuplicateSMSVerificationActivity;->H:Z

    .line 124
    const/4 v0, 0x4

    iput v0, p0, Lcom/osp/app/signin/DuplicateSMSVerificationActivity;->I:I

    .line 125
    const/4 v0, 0x6

    iput v0, p0, Lcom/osp/app/signin/DuplicateSMSVerificationActivity;->J:I

    .line 128
    iput-boolean v2, p0, Lcom/osp/app/signin/DuplicateSMSVerificationActivity;->L:Z

    .line 133
    iput-object v1, p0, Lcom/osp/app/signin/DuplicateSMSVerificationActivity;->M:Ljava/lang/String;

    .line 137
    new-instance v0, Lcom/osp/app/signin/em;

    invoke-direct {v0, p0}, Lcom/osp/app/signin/em;-><init>(Lcom/osp/app/signin/DuplicateSMSVerificationActivity;)V

    iput-object v0, p0, Lcom/osp/app/signin/DuplicateSMSVerificationActivity;->a:Landroid/view/View$OnClickListener;

    .line 144
    new-instance v0, Lcom/osp/app/signin/en;

    invoke-direct {v0, p0}, Lcom/osp/app/signin/en;-><init>(Lcom/osp/app/signin/DuplicateSMSVerificationActivity;)V

    iput-object v0, p0, Lcom/osp/app/signin/DuplicateSMSVerificationActivity;->b:Landroid/view/View$OnClickListener;

    .line 155
    iput-object v1, p0, Lcom/osp/app/signin/DuplicateSMSVerificationActivity;->O:Ljava/lang/String;

    .line 157
    new-instance v0, Lcom/osp/app/signin/eo;

    invoke-direct {v0, p0}, Lcom/osp/app/signin/eo;-><init>(Lcom/osp/app/signin/DuplicateSMSVerificationActivity;)V

    iput-object v0, p0, Lcom/osp/app/signin/DuplicateSMSVerificationActivity;->P:Landroid/content/BroadcastReceiver;

    .line 322
    new-instance v0, Lcom/osp/app/signin/ep;

    invoke-direct {v0, p0}, Lcom/osp/app/signin/ep;-><init>(Lcom/osp/app/signin/DuplicateSMSVerificationActivity;)V

    iput-object v0, p0, Lcom/osp/app/signin/DuplicateSMSVerificationActivity;->Q:Landroid/os/Handler;

    .line 1235
    return-void
.end method

.method static synthetic a(Lcom/osp/app/signin/DuplicateSMSVerificationActivity;Ljava/lang/String;)Ljava/lang/String;
    .locals 6

    .prologue
    .line 64
    const-string v0, ""

    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_0

    iget-object v1, p0, Lcom/osp/app/signin/DuplicateSMSVerificationActivity;->e:Ljava/lang/String;

    invoke-virtual {p1, v1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_1

    :cond_0
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v1, "DSMSV"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "SMS msg is not contain prefix("

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v3, p0, Lcom/osp/app/signin/DuplicateSMSVerificationActivity;->e:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ") !!!"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;)V

    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v1, "DSMSV"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "SMS MESSAGE : "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;)V

    :goto_0
    return-object v0

    :cond_1
    iget-object v1, p0, Lcom/osp/app/signin/DuplicateSMSVerificationActivity;->e:Ljava/lang/String;

    invoke-virtual {p1, v1}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v1

    iget-object v2, p0, Lcom/osp/app/signin/DuplicateSMSVerificationActivity;->e:Ljava/lang/String;

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v2

    add-int/2addr v1, v2

    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v2

    invoke-virtual {p1, v1, v2}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v2

    new-instance v3, Ljava/lang/StringBuffer;

    invoke-direct {v3}, Ljava/lang/StringBuffer;-><init>()V

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v4

    const/4 v1, 0x0

    :goto_1
    if-ge v1, v4, :cond_3

    invoke-virtual {v2, v1}, Ljava/lang/String;->charAt(I)C

    move-result v5

    invoke-static {v5}, Ljava/lang/Character;->isDigit(C)Z

    move-result v5

    if-eqz v5, :cond_2

    invoke-virtual {v2, v1}, Ljava/lang/String;->charAt(I)C

    move-result v5

    invoke-virtual {v3, v5}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    :cond_2
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v2, "DSMSV"

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "VerifyCode length is "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v2, v1}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;)V

    :cond_3
    invoke-virtual {v3}, Ljava/lang/StringBuffer;->length()I

    move-result v1

    const/4 v2, 0x4

    if-lt v1, v2, :cond_4

    invoke-virtual {v3}, Ljava/lang/StringBuffer;->length()I

    move-result v1

    const/4 v2, 0x6

    if-gt v1, v2, :cond_4

    invoke-virtual {v3}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    :cond_4
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v1, "DSMSV"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v4, "The length of verifyCode("

    invoke-direct {v2, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ") is wrong!!!!"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0
.end method

.method private a(I)V
    .locals 8

    .prologue
    const v6, 0x7f08011e

    .line 601
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "DSMSV::initLayoutParams orientation : "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;)V

    .line 603
    invoke-static {}, Lcom/osp/device/b;->b()Lcom/osp/device/b;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/osp/device/b;->i(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 605
    const v0, 0x7f0c00c0

    invoke-virtual {p0, v0}, Lcom/osp/app/signin/DuplicateSMSVerificationActivity;->findViewById(I)Landroid/view/View;

    move-result-object v2

    .line 606
    const v0, 0x7f0c016b

    invoke-virtual {p0, v0}, Lcom/osp/app/signin/DuplicateSMSVerificationActivity;->findViewById(I)Landroid/view/View;

    move-result-object v3

    .line 607
    const v0, 0x7f0c016c

    invoke-virtual {p0, v0}, Lcom/osp/app/signin/DuplicateSMSVerificationActivity;->findViewById(I)Landroid/view/View;

    move-result-object v4

    .line 609
    invoke-virtual {p0}, Lcom/osp/app/signin/DuplicateSMSVerificationActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f08011f

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v0

    float-to-int v0, v0

    .line 610
    invoke-virtual {p0}, Lcom/osp/app/signin/DuplicateSMSVerificationActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1, v6}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v1

    float-to-int v1, v1

    .line 612
    if-eqz v2, :cond_1

    if-eqz v3, :cond_1

    .line 614
    invoke-static {}, Lcom/osp/app/signin/SamsungService;->a()Z

    move-result v5

    if-nez v5, :cond_0

    .line 616
    invoke-virtual {p0}, Lcom/osp/app/signin/DuplicateSMSVerificationActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0, v6}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v0

    float-to-int v0, v0

    .line 619
    :cond_0
    invoke-static {}, Lcom/osp/app/util/q;->a()Lcom/osp/app/util/q;

    invoke-static {v2, v0, v1, p1}, Lcom/osp/app/util/q;->b(Landroid/view/View;III)V

    .line 620
    invoke-static {}, Lcom/osp/app/util/q;->a()Lcom/osp/app/util/q;

    invoke-static {v3, v0, v1, p1}, Lcom/osp/app/util/q;->b(Landroid/view/View;III)V

    .line 624
    :cond_1
    invoke-static {p0}, Lcom/osp/app/util/r;->k(Landroid/content/Context;)Z

    move-result v2

    if-nez v2, :cond_5

    .line 626
    invoke-virtual {p0}, Lcom/osp/app/signin/DuplicateSMSVerificationActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f08013d

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v0

    float-to-int v1, v0

    .line 627
    invoke-virtual {p0}, Lcom/osp/app/signin/DuplicateSMSVerificationActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v2, 0x7f08013c

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v0

    float-to-int v0, v0

    .line 630
    :goto_0
    invoke-static {}, Lcom/osp/app/util/r;->v()Z

    move-result v2

    if-eqz v2, :cond_2

    .line 632
    const/4 v2, 0x4

    invoke-virtual {v3, v2}, Landroid/view/View;->setVisibility(I)V

    .line 633
    const/16 v2, 0x8

    invoke-virtual {v4, v2}, Landroid/view/View;->setVisibility(I)V

    .line 636
    :cond_2
    iget-object v2, p0, Lcom/osp/app/signin/DuplicateSMSVerificationActivity;->N:Lcom/osp/app/signin/BottomSoftkeyLinearLayout;

    if-eqz v2, :cond_4

    .line 638
    invoke-static {}, Lcom/osp/app/signin/SamsungService;->a()Z

    move-result v2

    if-eqz v2, :cond_3

    sget-object v2, Lcom/osp/app/util/u;->o:[Ljava/lang/String;

    invoke-static {v2}, Lcom/osp/app/util/u;->a([Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_3

    .line 640
    const/4 v1, 0x0

    .line 642
    :cond_3
    invoke-static {}, Lcom/osp/app/util/q;->a()Lcom/osp/app/util/q;

    iget-object v2, p0, Lcom/osp/app/signin/DuplicateSMSVerificationActivity;->N:Lcom/osp/app/signin/BottomSoftkeyLinearLayout;

    invoke-static {p0, v2, v1, v0, p1}, Lcom/osp/app/util/q;->a(Landroid/content/Context;Landroid/view/View;III)V

    .line 645
    :cond_4
    return-void

    :cond_5
    move v7, v1

    move v1, v0

    move v0, v7

    goto :goto_0
.end method

.method static synthetic a(Lcom/osp/app/signin/DuplicateSMSVerificationActivity;)V
    .locals 0

    .prologue
    .line 64
    invoke-direct {p0}, Lcom/osp/app/signin/DuplicateSMSVerificationActivity;->u()V

    return-void
.end method

.method static synthetic a(Lcom/osp/app/signin/DuplicateSMSVerificationActivity;Z)V
    .locals 0

    .prologue
    .line 64
    invoke-direct {p0, p1}, Lcom/osp/app/signin/DuplicateSMSVerificationActivity;->a(Z)V

    return-void
.end method

.method private a(Z)V
    .locals 3

    .prologue
    const v2, 0x7f0c0196

    .line 801
    iget-object v0, p0, Lcom/osp/app/signin/DuplicateSMSVerificationActivity;->N:Lcom/osp/app/signin/BottomSoftkeyLinearLayout;

    if-eqz v0, :cond_0

    .line 803
    invoke-static {}, Lcom/osp/device/b;->b()Lcom/osp/device/b;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/osp/device/b;->l(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 805
    iget-object v0, p0, Lcom/osp/app/signin/DuplicateSMSVerificationActivity;->N:Lcom/osp/app/signin/BottomSoftkeyLinearLayout;

    invoke-static {p1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/osp/app/signin/BottomSoftkeyLinearLayout;->a(Ljava/lang/Boolean;)V

    .line 811
    :cond_0
    :goto_0
    iget-object v0, p0, Lcom/osp/app/signin/DuplicateSMSVerificationActivity;->x:Landroid/view/Menu;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/osp/app/signin/DuplicateSMSVerificationActivity;->x:Landroid/view/Menu;

    invoke-interface {v0, v2}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 813
    iget-object v0, p0, Lcom/osp/app/signin/DuplicateSMSVerificationActivity;->x:Landroid/view/Menu;

    invoke-interface {v0, v2}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v0

    invoke-interface {v0, p1}, Landroid/view/MenuItem;->setEnabled(Z)Landroid/view/MenuItem;

    .line 815
    :cond_1
    return-void

    .line 808
    :cond_2
    iget-object v0, p0, Lcom/osp/app/signin/DuplicateSMSVerificationActivity;->N:Lcom/osp/app/signin/BottomSoftkeyLinearLayout;

    invoke-static {p1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/osp/app/signin/BottomSoftkeyLinearLayout;->b(Ljava/lang/Boolean;)V

    goto :goto_0
.end method

.method private a(Ljava/lang/String;)Z
    .locals 2

    .prologue
    .line 954
    iget-boolean v0, p0, Lcom/osp/app/signin/DuplicateSMSVerificationActivity;->D:Z

    if-eqz v0, :cond_0

    .line 956
    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v0

    const/4 v1, 0x4

    if-lt v0, v1, :cond_0

    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v0

    const/4 v1, 0x6

    if-gt v0, v1, :cond_0

    .line 958
    const/4 v0, 0x1

    .line 961
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method static synthetic b(Lcom/osp/app/signin/DuplicateSMSVerificationActivity;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 64
    iget-object v0, p0, Lcom/osp/app/signin/DuplicateSMSVerificationActivity;->e:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic b(Lcom/osp/app/signin/DuplicateSMSVerificationActivity;Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 64
    const v0, 0x7f0c00c4

    invoke-virtual {p0, v0}, Lcom/osp/app/signin/DuplicateSMSVerificationActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/EditText;

    invoke-virtual {v0, p1}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    return-void
.end method

.method static synthetic b(Lcom/osp/app/signin/DuplicateSMSVerificationActivity;Z)Z
    .locals 0

    .prologue
    .line 64
    iput-boolean p1, p0, Lcom/osp/app/signin/DuplicateSMSVerificationActivity;->L:Z

    return p1
.end method

.method static synthetic c(Lcom/osp/app/signin/DuplicateSMSVerificationActivity;)I
    .locals 2

    .prologue
    .line 64
    iget v0, p0, Lcom/osp/app/signin/DuplicateSMSVerificationActivity;->C:I

    add-int/lit8 v1, v0, -0x1

    iput v1, p0, Lcom/osp/app/signin/DuplicateSMSVerificationActivity;->C:I

    return v0
.end method

.method static synthetic c(Lcom/osp/app/signin/DuplicateSMSVerificationActivity;Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 64
    const v0, 0x7f0c00c7

    invoke-virtual {p0, v0}, Lcom/osp/app/signin/DuplicateSMSVerificationActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    invoke-static {p1}, Landroid/telephony/PhoneNumberUtils;->isGlobalPhoneNumber(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setEnabled(Z)V

    :goto_0
    return-void

    :cond_0
    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setEnabled(Z)V

    goto :goto_0
.end method

.method private d()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 472
    const v0, 0x7f0c00c4

    invoke-virtual {p0, v0}, Lcom/osp/app/signin/DuplicateSMSVerificationActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/EditText;

    .line 473
    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setEnabled(Z)V

    .line 474
    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setFocusable(Z)V

    .line 475
    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setFocusableInTouchMode(Z)V

    .line 476
    return-void
.end method

.method static synthetic d(Lcom/osp/app/signin/DuplicateSMSVerificationActivity;)V
    .locals 0

    .prologue
    .line 64
    invoke-direct {p0}, Lcom/osp/app/signin/DuplicateSMSVerificationActivity;->r()V

    return-void
.end method

.method static synthetic d(Lcom/osp/app/signin/DuplicateSMSVerificationActivity;Ljava/lang/String;)Z
    .locals 1

    .prologue
    .line 64
    invoke-direct {p0, p1}, Lcom/osp/app/signin/DuplicateSMSVerificationActivity;->a(Ljava/lang/String;)Z

    move-result v0

    return v0
.end method

.method static synthetic e(Lcom/osp/app/signin/DuplicateSMSVerificationActivity;Ljava/lang/String;)Ljava/lang/String;
    .locals 0

    .prologue
    .line 64
    iput-object p1, p0, Lcom/osp/app/signin/DuplicateSMSVerificationActivity;->E:Ljava/lang/String;

    return-object p1
.end method

.method private e()V
    .locals 2

    .prologue
    .line 500
    const v0, 0x7f0c00c3

    invoke-virtual {p0, v0}, Lcom/osp/app/signin/DuplicateSMSVerificationActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 501
    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setEnabled(Z)V

    .line 502
    return-void
.end method

.method static synthetic e(Lcom/osp/app/signin/DuplicateSMSVerificationActivity;)V
    .locals 0

    .prologue
    .line 64
    invoke-direct {p0}, Lcom/osp/app/signin/DuplicateSMSVerificationActivity;->v()V

    return-void
.end method

.method static synthetic f(Lcom/osp/app/signin/DuplicateSMSVerificationActivity;Ljava/lang/String;)Ljava/lang/String;
    .locals 0

    .prologue
    .line 64
    iput-object p1, p0, Lcom/osp/app/signin/DuplicateSMSVerificationActivity;->e:Ljava/lang/String;

    return-object p1
.end method

.method private f()V
    .locals 4

    .prologue
    .line 508
    invoke-virtual {p0}, Lcom/osp/app/signin/DuplicateSMSVerificationActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    iput-object v0, p0, Lcom/osp/app/signin/DuplicateSMSVerificationActivity;->K:Landroid/content/Intent;

    .line 509
    iget-object v0, p0, Lcom/osp/app/signin/DuplicateSMSVerificationActivity;->K:Landroid/content/Intent;

    const-string v1, "key_internal_sign_up_inforamtion"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/osp/app/signin/SignUpinfo;

    .line 511
    const v1, 0x7f0c00c3

    invoke-virtual {p0, v1}, Lcom/osp/app/signin/DuplicateSMSVerificationActivity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    .line 512
    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "+"

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Lcom/osp/app/signin/SignUpinfo;->E()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v2, p0, Lcom/osp/app/signin/DuplicateSMSVerificationActivity;->y:Ljava/lang/String;

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 513
    invoke-virtual {p0}, Lcom/osp/app/signin/DuplicateSMSVerificationActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-static {p0}, Lcom/osp/app/util/an;->a(Lcom/osp/app/util/BaseActivity;)Lcom/osp/app/util/an;

    const v2, 0x7f070006

    invoke-static {v2}, Lcom/osp/app/util/an;->a(I)I

    move-result v2

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setTextColor(I)V

    .line 515
    return-void
.end method

.method static synthetic f(Lcom/osp/app/signin/DuplicateSMSVerificationActivity;)V
    .locals 0

    .prologue
    .line 64
    invoke-direct {p0}, Lcom/osp/app/signin/DuplicateSMSVerificationActivity;->t()V

    return-void
.end method

.method private g()Ljava/lang/String;
    .locals 3

    .prologue
    .line 521
    const-string v1, ""

    .line 522
    invoke-virtual {p0}, Lcom/osp/app/signin/DuplicateSMSVerificationActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    iput-object v0, p0, Lcom/osp/app/signin/DuplicateSMSVerificationActivity;->K:Landroid/content/Intent;

    .line 523
    iget-object v0, p0, Lcom/osp/app/signin/DuplicateSMSVerificationActivity;->K:Landroid/content/Intent;

    const-string v2, "key_internal_sign_up_inforamtion"

    invoke-virtual {v0, v2}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/osp/app/signin/SignUpinfo;

    .line 524
    if-eqz v0, :cond_0

    .line 526
    invoke-virtual {v0}, Lcom/osp/app/signin/SignUpinfo;->F()Ljava/lang/String;

    move-result-object v0

    .line 528
    :goto_0
    return-object v0

    :cond_0
    move-object v0, v1

    goto :goto_0
.end method

.method static synthetic g(Lcom/osp/app/signin/DuplicateSMSVerificationActivity;Ljava/lang/String;)Ljava/lang/String;
    .locals 0

    .prologue
    .line 64
    iput-object p1, p0, Lcom/osp/app/signin/DuplicateSMSVerificationActivity;->F:Ljava/lang/String;

    return-object p1
.end method

.method static synthetic g(Lcom/osp/app/signin/DuplicateSMSVerificationActivity;)Z
    .locals 1

    .prologue
    .line 64
    iget-boolean v0, p0, Lcom/osp/app/signin/DuplicateSMSVerificationActivity;->H:Z

    return v0
.end method

.method static synthetic h(Lcom/osp/app/signin/DuplicateSMSVerificationActivity;)Z
    .locals 1

    .prologue
    .line 64
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/osp/app/signin/DuplicateSMSVerificationActivity;->H:Z

    return v0
.end method

.method static synthetic i(Lcom/osp/app/signin/DuplicateSMSVerificationActivity;)V
    .locals 2

    .prologue
    const/4 v1, 0x1

    .line 64
    const v0, 0x7f0c00c4

    invoke-virtual {p0, v0}, Lcom/osp/app/signin/DuplicateSMSVerificationActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/EditText;

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setFocusable(Z)V

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setFocusableInTouchMode(Z)V

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setEnabled(Z)V

    return-void
.end method

.method static synthetic j(Lcom/osp/app/signin/DuplicateSMSVerificationActivity;)V
    .locals 2

    .prologue
    .line 64
    const v0, 0x7f0c00c3

    invoke-virtual {p0, v0}, Lcom/osp/app/signin/DuplicateSMSVerificationActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setEnabled(Z)V

    return-void
.end method

.method static synthetic k(Lcom/osp/app/signin/DuplicateSMSVerificationActivity;)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 64
    const v0, 0x7f0c00c7

    invoke-virtual {p0, v0}, Lcom/osp/app/signin/DuplicateSMSVerificationActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v1}, Landroid/view/View;->setEnabled(Z)V

    iget-object v0, p0, Lcom/osp/app/signin/DuplicateSMSVerificationActivity;->Q:Landroid/os/Handler;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeMessages(I)V

    const v0, 0x7f0c00c6

    invoke-virtual {p0, v0}, Lcom/osp/app/signin/DuplicateSMSVerificationActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/osp/app/signin/DuplicateSMSVerificationActivity;->D:Z

    const/16 v0, 0x3c

    iput v0, p0, Lcom/osp/app/signin/DuplicateSMSVerificationActivity;->C:I

    invoke-direct {p0}, Lcom/osp/app/signin/DuplicateSMSVerificationActivity;->r()V

    return-void
.end method

.method static synthetic l(Lcom/osp/app/signin/DuplicateSMSVerificationActivity;)Landroid/os/Handler;
    .locals 1

    .prologue
    .line 64
    iget-object v0, p0, Lcom/osp/app/signin/DuplicateSMSVerificationActivity;->Q:Landroid/os/Handler;

    return-object v0
.end method

.method static synthetic m(Lcom/osp/app/signin/DuplicateSMSVerificationActivity;)V
    .locals 0

    .prologue
    .line 64
    invoke-direct {p0}, Lcom/osp/app/signin/DuplicateSMSVerificationActivity;->e()V

    return-void
.end method

.method static synthetic n(Lcom/osp/app/signin/DuplicateSMSVerificationActivity;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 64
    iget-object v0, p0, Lcom/osp/app/signin/DuplicateSMSVerificationActivity;->E:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic o(Lcom/osp/app/signin/DuplicateSMSVerificationActivity;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 64
    iget-object v0, p0, Lcom/osp/app/signin/DuplicateSMSVerificationActivity;->F:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic p(Lcom/osp/app/signin/DuplicateSMSVerificationActivity;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 64
    iget-object v0, p0, Lcom/osp/app/signin/DuplicateSMSVerificationActivity;->A:Ljava/lang/String;

    return-object v0
.end method

.method private q()Ljava/lang/String;
    .locals 1

    .prologue
    .line 547
    const v0, 0x7f0c00c4

    invoke-virtual {p0, v0}, Lcom/osp/app/signin/DuplicateSMSVerificationActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/EditText;

    .line 548
    invoke-virtual {v0}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method static synthetic q(Lcom/osp/app/signin/DuplicateSMSVerificationActivity;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 64
    iget-object v0, p0, Lcom/osp/app/signin/DuplicateSMSVerificationActivity;->y:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic r(Lcom/osp/app/signin/DuplicateSMSVerificationActivity;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 64
    iget-object v0, p0, Lcom/osp/app/signin/DuplicateSMSVerificationActivity;->z:Ljava/lang/String;

    return-object v0
.end method

.method private r()V
    .locals 6

    .prologue
    const v5, 0x7f0c00c6

    const/4 v3, 0x1

    const/4 v4, 0x0

    .line 1026
    const-string v0, "%02d:%02d"

    const/4 v1, 0x2

    new-array v1, v1, [Ljava/lang/Object;

    iget v2, p0, Lcom/osp/app/signin/DuplicateSMSVerificationActivity;->C:I

    div-int/lit8 v2, v2, 0x3c

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v1, v4

    iget v2, p0, Lcom/osp/app/signin/DuplicateSMSVerificationActivity;->C:I

    rem-int/lit8 v2, v2, 0x3c

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v1, v3

    invoke-static {v0, v1}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    .line 1029
    invoke-virtual {p0, v5}, Lcom/osp/app/signin/DuplicateSMSVerificationActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 1030
    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 1032
    iget v0, p0, Lcom/osp/app/signin/DuplicateSMSVerificationActivity;->C:I

    if-nez v0, :cond_0

    .line 1034
    const v0, 0x7f0c00c7

    invoke-virtual {p0, v0}, Lcom/osp/app/signin/DuplicateSMSVerificationActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v3}, Landroid/view/View;->setEnabled(Z)V

    .line 1036
    iput-boolean v4, p0, Lcom/osp/app/signin/DuplicateSMSVerificationActivity;->D:Z

    .line 1037
    invoke-direct {p0, v4}, Lcom/osp/app/signin/DuplicateSMSVerificationActivity;->a(Z)V

    .line 1038
    invoke-direct {p0}, Lcom/osp/app/signin/DuplicateSMSVerificationActivity;->e()V

    .line 1039
    invoke-direct {p0}, Lcom/osp/app/signin/DuplicateSMSVerificationActivity;->d()V

    .line 1040
    const/4 v0, -0x1

    iput v0, p0, Lcom/osp/app/signin/DuplicateSMSVerificationActivity;->C:I

    .line 1042
    invoke-virtual {p0, v5}, Lcom/osp/app/signin/DuplicateSMSVerificationActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 1043
    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    .line 1050
    :goto_0
    return-void

    .line 1047
    :cond_0
    iget-object v0, p0, Lcom/osp/app/signin/DuplicateSMSVerificationActivity;->Q:Landroid/os/Handler;

    const-wide/16 v2, 0x3e8

    invoke-virtual {v0, v4, v2, v3}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z

    goto :goto_0
.end method

.method private s()V
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 1056
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v0, "DSMSVstartSMSAuthenticate()"

    invoke-static {v0}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;)V

    .line 1058
    const v0, 0x7f0c00c4

    invoke-virtual {p0, v0}, Lcom/osp/app/signin/DuplicateSMSVerificationActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/EditText;

    .line 1059
    const-string v1, ""

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    .line 1061
    iput-boolean v4, p0, Lcom/osp/app/signin/DuplicateSMSVerificationActivity;->L:Z

    .line 1062
    invoke-direct {p0}, Lcom/osp/app/signin/DuplicateSMSVerificationActivity;->g()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/osp/app/signin/DuplicateSMSVerificationActivity;->y:Ljava/lang/String;

    .line 1064
    invoke-static {}, Lcom/osp/app/util/ac;->a()Lcom/osp/app/util/ac;

    invoke-static {p0}, Lcom/osp/app/util/ac;->a(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/osp/app/signin/DuplicateSMSVerificationActivity;->O:Ljava/lang/String;

    .line 1065
    invoke-static {}, Lcom/osp/app/util/ac;->a()Lcom/osp/app/util/ac;

    invoke-static {p0}, Lcom/osp/app/util/ac;->c(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/osp/app/signin/DuplicateSMSVerificationActivity;->M:Ljava/lang/String;

    .line 1069
    :try_start_0
    invoke-static {}, Lcom/google/i18n/phonenumbers/h;->a()Lcom/google/i18n/phonenumbers/h;

    move-result-object v0

    iget-object v1, p0, Lcom/osp/app/signin/DuplicateSMSVerificationActivity;->y:Ljava/lang/String;

    iget-object v2, p0, Lcom/osp/app/signin/DuplicateSMSVerificationActivity;->B:Ljava/lang/String;

    sget-object v3, Ljava/util/Locale;->ENGLISH:Ljava/util/Locale;

    invoke-virtual {v2, v3}, Ljava/lang/String;->toUpperCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/google/i18n/phonenumbers/h;->a(Ljava/lang/CharSequence;Ljava/lang/String;)Ljava/lang/Iterable;

    move-result-object v0

    invoke-interface {v0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/i18n/phonenumbers/d;

    .line 1070
    invoke-virtual {v0}, Lcom/google/i18n/phonenumbers/d;->a()Lcom/google/i18n/phonenumbers/ac;

    move-result-object v0

    .line 1071
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0}, Lcom/google/i18n/phonenumbers/ac;->b()J

    move-result-wide v2

    invoke-virtual {v1, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/osp/app/signin/DuplicateSMSVerificationActivity;->y:Ljava/lang/String;

    .line 1072
    invoke-direct {p0}, Lcom/osp/app/signin/DuplicateSMSVerificationActivity;->f()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 1077
    :goto_0
    new-instance v0, Lcom/osp/security/identity/g;

    invoke-direct {v0, p0}, Lcom/osp/security/identity/g;-><init>(Landroid/content/Context;)V

    .line 1078
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "+"

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v1, p0, Lcom/osp/app/signin/DuplicateSMSVerificationActivity;->A:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/osp/app/signin/DuplicateSMSVerificationActivity;->y:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/osp/app/signin/DuplicateSMSVerificationActivity;->B:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/osp/security/identity/g;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/osp/security/identity/h;

    move-result-object v0

    .line 1079
    sget-object v1, Lcom/osp/security/identity/h;->a:Lcom/osp/security/identity/h;

    if-ne v0, v1, :cond_0

    .line 1081
    new-instance v0, Lcom/osp/app/signin/et;

    invoke-direct {v0, p0}, Lcom/osp/app/signin/et;-><init>(Lcom/osp/app/signin/DuplicateSMSVerificationActivity;)V

    iput-object v0, p0, Lcom/osp/app/signin/DuplicateSMSVerificationActivity;->G:Lcom/osp/app/signin/et;

    .line 1082
    iget-object v0, p0, Lcom/osp/app/signin/DuplicateSMSVerificationActivity;->G:Lcom/osp/app/signin/et;

    invoke-virtual {v0, v4}, Lcom/osp/app/signin/et;->a(I)V

    .line 1084
    iget-object v0, p0, Lcom/osp/app/signin/DuplicateSMSVerificationActivity;->G:Lcom/osp/app/signin/et;

    invoke-virtual {v0}, Lcom/osp/app/signin/et;->b()V

    .line 1089
    :goto_1
    return-void

    .line 1087
    :cond_0
    invoke-static {}, Lcom/osp/app/util/ap;->a()Lcom/osp/app/util/ap;

    const v0, 0x7f0900ed

    const/4 v1, 0x1

    invoke-static {p0, v0, v1}, Lcom/osp/app/util/ap;->a(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    goto :goto_1

    :catch_0
    move-exception v0

    goto :goto_0
.end method

.method private t()V
    .locals 2

    .prologue
    .line 1096
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v0, "DSMSVstartSMSValidate()"

    invoke-static {v0}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;)V

    .line 1098
    invoke-direct {p0}, Lcom/osp/app/signin/DuplicateSMSVerificationActivity;->q()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/osp/app/signin/DuplicateSMSVerificationActivity;->z:Ljava/lang/String;

    .line 1099
    invoke-direct {p0}, Lcom/osp/app/signin/DuplicateSMSVerificationActivity;->g()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/osp/app/signin/DuplicateSMSVerificationActivity;->y:Ljava/lang/String;

    .line 1101
    iget-boolean v0, p0, Lcom/osp/app/signin/DuplicateSMSVerificationActivity;->L:Z

    if-eqz v0, :cond_0

    .line 1103
    invoke-direct {p0}, Lcom/osp/app/signin/DuplicateSMSVerificationActivity;->e()V

    .line 1104
    invoke-virtual {p0}, Lcom/osp/app/signin/DuplicateSMSVerificationActivity;->c()V

    .line 1112
    :goto_0
    return-void

    .line 1107
    :cond_0
    new-instance v0, Lcom/osp/app/signin/et;

    invoke-direct {v0, p0}, Lcom/osp/app/signin/et;-><init>(Lcom/osp/app/signin/DuplicateSMSVerificationActivity;)V

    iput-object v0, p0, Lcom/osp/app/signin/DuplicateSMSVerificationActivity;->G:Lcom/osp/app/signin/et;

    .line 1108
    iget-object v0, p0, Lcom/osp/app/signin/DuplicateSMSVerificationActivity;->G:Lcom/osp/app/signin/et;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/osp/app/signin/et;->a(I)V

    .line 1110
    iget-object v0, p0, Lcom/osp/app/signin/DuplicateSMSVerificationActivity;->G:Lcom/osp/app/signin/et;

    invoke-virtual {v0}, Lcom/osp/app/signin/et;->b()V

    goto :goto_0
.end method

.method private u()V
    .locals 1

    .prologue
    .line 1123
    invoke-static {p0}, Lcom/osp/app/util/ad;->a(Landroid/content/Context;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 1125
    const/16 v0, 0xdb

    invoke-virtual {p0, v0}, Lcom/osp/app/signin/DuplicateSMSVerificationActivity;->c(I)V

    .line 1130
    :goto_0
    return-void

    .line 1128
    :cond_0
    invoke-direct {p0}, Lcom/osp/app/signin/DuplicateSMSVerificationActivity;->t()V

    goto :goto_0
.end method

.method private v()V
    .locals 5

    .prologue
    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 1161
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "DSMSVmSuccessVerified = "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-boolean v1, p0, Lcom/osp/app/signin/DuplicateSMSVerificationActivity;->L:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;)V

    .line 1163
    iget-boolean v0, p0, Lcom/osp/app/signin/DuplicateSMSVerificationActivity;->L:Z

    if-eqz v0, :cond_0

    .line 1165
    const v0, 0x7f0c00c3

    invoke-virtual {p0, v0}, Lcom/osp/app/signin/DuplicateSMSVerificationActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 1166
    const v1, 0x7f0c00c7

    invoke-virtual {p0, v1}, Lcom/osp/app/signin/DuplicateSMSVerificationActivity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/Button;

    .line 1168
    if-eqz v1, :cond_0

    if-eqz v0, :cond_0

    .line 1170
    iget-object v2, p0, Lcom/osp/app/signin/DuplicateSMSVerificationActivity;->y:Ljava/lang/String;

    invoke-virtual {v0}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v0

    invoke-interface {v0}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1172
    invoke-virtual {v1, v3}, Landroid/widget/Button;->setEnabled(Z)V

    .line 1173
    invoke-direct {p0, v4}, Lcom/osp/app/signin/DuplicateSMSVerificationActivity;->a(Z)V

    .line 1181
    :cond_0
    :goto_0
    return-void

    .line 1176
    :cond_1
    invoke-virtual {v1, v4}, Landroid/widget/Button;->setEnabled(Z)V

    .line 1177
    invoke-direct {p0, v3}, Lcom/osp/app/signin/DuplicateSMSVerificationActivity;->a(Z)V

    goto :goto_0
.end method


# virtual methods
.method protected final a()V
    .locals 1

    .prologue
    .line 684
    iget v0, p0, Lcom/osp/app/signin/DuplicateSMSVerificationActivity;->C:I

    if-lez v0, :cond_0

    invoke-direct {p0}, Lcom/osp/app/signin/DuplicateSMSVerificationActivity;->q()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/osp/app/signin/DuplicateSMSVerificationActivity;->a(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-direct {p0, v0}, Lcom/osp/app/signin/DuplicateSMSVerificationActivity;->a(Z)V

    .line 685
    return-void

    .line 684
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected final b()V
    .locals 0

    .prologue
    .line 689
    return-void
.end method

.method public final c()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    .line 1189
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v0, "DSMSVshowTncActivity()"

    invoke-static {v0}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;)V

    .line 1191
    iget-object v0, p0, Lcom/osp/app/signin/DuplicateSMSVerificationActivity;->K:Landroid/content/Intent;

    const-class v1, Lcom/osp/app/signin/TnCView;

    invoke-virtual {v0, p0, v1}, Landroid/content/Intent;->setClass(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;

    .line 1192
    iget-object v0, p0, Lcom/osp/app/signin/DuplicateSMSVerificationActivity;->K:Landroid/content/Intent;

    const-string v1, "key_internal_sign_up_inforamtion"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/osp/app/signin/SignUpinfo;

    .line 1199
    if-eqz v0, :cond_0

    .line 1201
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v2, p0, Lcom/osp/app/signin/DuplicateSMSVerificationActivity;->A:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/osp/app/signin/DuplicateSMSVerificationActivity;->y:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/osp/app/signin/SignUpinfo;->a(Ljava/lang/String;)V

    .line 1202
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v2, p0, Lcom/osp/app/signin/DuplicateSMSVerificationActivity;->A:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/osp/app/signin/DuplicateSMSVerificationActivity;->y:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/osp/app/signin/SignUpinfo;->b(Ljava/lang/String;)V

    .line 1203
    iget-object v1, p0, Lcom/osp/app/signin/DuplicateSMSVerificationActivity;->y:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/osp/app/signin/SignUpinfo;->A(Ljava/lang/String;)V

    .line 1204
    invoke-virtual {v0}, Lcom/osp/app/signin/SignUpinfo;->a()V

    .line 1205
    iget-object v1, p0, Lcom/osp/app/signin/DuplicateSMSVerificationActivity;->A:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/osp/app/signin/SignUpinfo;->z(Ljava/lang/String;)V

    .line 1206
    invoke-virtual {v0}, Lcom/osp/app/signin/SignUpinfo;->G()V

    .line 1207
    invoke-virtual {v0}, Lcom/osp/app/signin/SignUpinfo;->I()V

    .line 1208
    iget-object v1, p0, Lcom/osp/app/signin/DuplicateSMSVerificationActivity;->E:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/osp/app/signin/SignUpinfo;->E(Ljava/lang/String;)V

    .line 1209
    iget-object v1, p0, Lcom/osp/app/signin/DuplicateSMSVerificationActivity;->z:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/osp/app/signin/SignUpinfo;->F(Ljava/lang/String;)V

    .line 1214
    iget-object v1, p0, Lcom/osp/app/signin/DuplicateSMSVerificationActivity;->K:Landroid/content/Intent;

    const-string v2, "key_internal_sign_up_inforamtion"

    invoke-virtual {v1, v2, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 1217
    :cond_0
    iget-object v0, p0, Lcom/osp/app/signin/DuplicateSMSVerificationActivity;->K:Landroid/content/Intent;

    const-string v1, "is_cancelable_just_one_activity"

    invoke-virtual {v0, v1, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 1220
    iget-object v0, p0, Lcom/osp/app/signin/DuplicateSMSVerificationActivity;->K:Landroid/content/Intent;

    const-string v1, "is_signup_flow"

    invoke-virtual {v0, v1, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 1223
    invoke-direct {p0}, Lcom/osp/app/signin/DuplicateSMSVerificationActivity;->d()V

    .line 1225
    iget-object v0, p0, Lcom/osp/app/signin/DuplicateSMSVerificationActivity;->K:Landroid/content/Intent;

    const/16 v1, 0xce

    invoke-virtual {p0, v0, v1}, Lcom/osp/app/signin/DuplicateSMSVerificationActivity;->startActivityForResult(Landroid/content/Intent;I)V

    .line 1226
    return-void
.end method

.method protected onActivityResult(IILandroid/content/Intent;)V
    .locals 1

    .prologue
    .line 819
    invoke-super {p0, p1, p2, p3}, Lcom/osp/app/util/BaseActivity;->onActivityResult(IILandroid/content/Intent;)V

    .line 821
    const/16 v0, 0xce

    if-ne p1, v0, :cond_1

    .line 823
    sparse-switch p2, :sswitch_data_0

    .line 840
    invoke-virtual {p0, p2, p3}, Lcom/osp/app/signin/DuplicateSMSVerificationActivity;->a(ILandroid/content/Intent;)V

    .line 841
    invoke-virtual {p0}, Lcom/osp/app/signin/DuplicateSMSVerificationActivity;->finish()V

    .line 861
    :cond_0
    :goto_0
    return-void

    .line 826
    :sswitch_0
    invoke-virtual {p0, p2, p3}, Lcom/osp/app/signin/DuplicateSMSVerificationActivity;->a(ILandroid/content/Intent;)V

    .line 827
    invoke-virtual {p0}, Lcom/osp/app/signin/DuplicateSMSVerificationActivity;->finish()V

    goto :goto_0

    .line 830
    :sswitch_1
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v0, "DSMSV RESULT_FAIL_ACTIVATING_EMAIL_VALIDATION"

    invoke-static {v0}, Lcom/osp/app/util/aq;->b(Ljava/lang/String;)V

    .line 831
    invoke-virtual {p0, p2}, Lcom/osp/app/signin/DuplicateSMSVerificationActivity;->b(I)V

    .line 832
    invoke-virtual {p0}, Lcom/osp/app/signin/DuplicateSMSVerificationActivity;->finish()V

    goto :goto_0

    .line 835
    :sswitch_2
    invoke-virtual {p0}, Lcom/osp/app/signin/DuplicateSMSVerificationActivity;->getApplication()Landroid/app/Application;

    move-result-object v0

    check-cast v0, Lcom/osp/app/signin/SamsungService;

    invoke-virtual {v0, p0}, Lcom/osp/app/signin/SamsungService;->a(Landroid/app/Activity;)V

    .line 836
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v0, "DSMSV TnC Canceled"

    invoke-static {v0}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;)V

    goto :goto_0

    .line 844
    :cond_1
    const/16 v0, 0xe2

    if-ne p1, v0, :cond_2

    .line 846
    packed-switch p2, :pswitch_data_0

    goto :goto_0

    .line 849
    :pswitch_0
    invoke-direct {p0}, Lcom/osp/app/signin/DuplicateSMSVerificationActivity;->s()V

    goto :goto_0

    .line 855
    :cond_2
    const/16 v0, 0xdb

    if-ne p1, v0, :cond_0

    .line 857
    packed-switch p2, :pswitch_data_1

    goto :goto_0

    .line 860
    :pswitch_1
    invoke-direct {p0}, Lcom/osp/app/signin/DuplicateSMSVerificationActivity;->t()V

    goto :goto_0

    .line 823
    nop

    :sswitch_data_0
    .sparse-switch
        -0x1 -> :sswitch_0
        0xd -> :sswitch_1
        0xe -> :sswitch_2
    .end sparse-switch

    .line 846
    :pswitch_data_0
    .packed-switch -0x1
        :pswitch_0
    .end packed-switch

    .line 857
    :pswitch_data_1
    .packed-switch -0x1
        :pswitch_1
    .end packed-switch
.end method

.method public onBackPressed()V
    .locals 3

    .prologue
    .line 445
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v0, "DSMSV"

    const-string v1, "onBackPressed"

    invoke-static {v0, v1}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 447
    iget-object v0, p0, Lcom/osp/app/signin/DuplicateSMSVerificationActivity;->K:Landroid/content/Intent;

    const-string v1, "is_cancelable_just_one_activity"

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 449
    const/16 v0, 0xe

    invoke-virtual {p0, v0}, Lcom/osp/app/signin/DuplicateSMSVerificationActivity;->b(I)V

    .line 452
    :cond_0
    invoke-super {p0}, Lcom/osp/app/util/BaseActivity;->onBackPressed()V

    .line 453
    return-void
.end method

.method public onClick(Landroid/view/View;)V
    .locals 1

    .prologue
    .line 1134
    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    .line 1149
    :goto_0
    return-void

    .line 1142
    :pswitch_0
    invoke-static {p0}, Lcom/osp/app/util/ad;->a(Landroid/content/Context;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 1144
    const/16 v0, 0xe2

    invoke-virtual {p0, v0}, Lcom/osp/app/signin/DuplicateSMSVerificationActivity;->c(I)V

    goto :goto_0

    .line 1147
    :cond_0
    invoke-direct {p0}, Lcom/osp/app/signin/DuplicateSMSVerificationActivity;->s()V

    goto :goto_0

    .line 1134
    :pswitch_data_0
    .packed-switch 0x7f0c00c7
        :pswitch_0
    .end packed-switch
.end method

.method public onConfigurationChanged(Landroid/content/res/Configuration;)V
    .locals 1

    .prologue
    .line 589
    invoke-super {p0, p1}, Lcom/osp/app/util/BaseActivity;->onConfigurationChanged(Landroid/content/res/Configuration;)V

    .line 591
    iget v0, p1, Landroid/content/res/Configuration;->orientation:I

    invoke-direct {p0, v0}, Lcom/osp/app/signin/DuplicateSMSVerificationActivity;->a(I)V

    .line 592
    return-void
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 8

    .prologue
    const v7, 0x7f090019

    const/4 v6, 0x1

    const v4, 0x7f070006

    const v5, 0x7f09006b

    const/4 v1, 0x0

    .line 337
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v0, "DSMSV"

    const-string v2, "onCreate"

    invoke-static {v0, v2}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 339
    invoke-super {p0, p1}, Lcom/osp/app/util/BaseActivity;->onCreate(Landroid/os/Bundle;)V

    .line 341
    invoke-virtual {p0}, Lcom/osp/app/signin/DuplicateSMSVerificationActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    iput-object v0, p0, Lcom/osp/app/signin/DuplicateSMSVerificationActivity;->K:Landroid/content/Intent;

    .line 343
    invoke-static {p0}, Lcom/osp/app/util/ad;->d(Landroid/content/Context;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 345
    iget-object v0, p0, Lcom/osp/app/signin/DuplicateSMSVerificationActivity;->K:Landroid/content/Intent;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/osp/app/signin/DuplicateSMSVerificationActivity;->K:Landroid/content/Intent;

    const-string v2, "is_signup_flow"

    invoke-virtual {v0, v2, v1}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 347
    sget-object v0, Lcom/osp/app/loggingservice/c;->e:Lcom/osp/app/loggingservice/c;

    invoke-static {v0}, Lcom/osp/app/loggingservice/LoggingService;->a(Lcom/osp/app/loggingservice/c;)V

    .line 351
    :cond_0
    invoke-static {}, Lcom/osp/device/b;->b()Lcom/osp/device/b;

    move-result-object v0

    invoke-virtual {v0}, Lcom/osp/device/b;->a()Z

    move-result v0

    if-nez v0, :cond_1

    .line 353
    invoke-virtual {p0}, Lcom/osp/app/signin/DuplicateSMSVerificationActivity;->finish()V

    .line 403
    :goto_0
    return-void

    .line 357
    :cond_1
    const v0, 0x7f03001a

    invoke-static {p0}, Lcom/osp/app/util/r;->q(Landroid/content/Context;)Z

    move-result v2

    invoke-virtual {p0, v0, v2}, Lcom/osp/app/signin/DuplicateSMSVerificationActivity;->a(IZ)V

    .line 359
    invoke-static {p0}, Lcom/osp/app/util/r;->i(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 361
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v2, 0x13

    if-ge v0, v2, :cond_2

    .line 363
    invoke-static {}, Lcom/osp/device/b;->b()Lcom/osp/device/b;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/osp/device/b;->i(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 365
    invoke-static {p0}, Lcom/msc/sa/c/d;->c(Landroid/app/Activity;)V

    .line 391
    :cond_2
    invoke-static {p0}, Lcom/msc/sa/c/d;->a(Landroid/app/Activity;)V

    .line 393
    invoke-virtual {p0}, Lcom/osp/app/signin/DuplicateSMSVerificationActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v0, 0x7f0c00c1

    invoke-virtual {p0, v0}, Lcom/osp/app/signin/DuplicateSMSVerificationActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    if-eqz v0, :cond_3

    invoke-static {p0}, Lcom/osp/app/util/an;->a(Lcom/osp/app/util/BaseActivity;)Lcom/osp/app/util/an;

    invoke-static {v4}, Lcom/osp/app/util/an;->a(I)I

    move-result v3

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getColor(I)I

    move-result v3

    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setTextColor(I)V

    :cond_3
    const v0, 0x7f0c00c2

    invoke-virtual {p0, v0}, Lcom/osp/app/signin/DuplicateSMSVerificationActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    if-eqz v0, :cond_4

    invoke-static {p0}, Lcom/osp/app/util/an;->a(Lcom/osp/app/util/BaseActivity;)Lcom/osp/app/util/an;

    invoke-static {v4}, Lcom/osp/app/util/an;->a(I)I

    move-result v3

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getColor(I)I

    move-result v3

    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setTextColor(I)V

    :cond_4
    const v0, 0x7f0c0158

    invoke-virtual {p0, v0}, Lcom/osp/app/signin/DuplicateSMSVerificationActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    if-eqz v0, :cond_5

    invoke-static {p0}, Lcom/osp/app/util/an;->a(Lcom/osp/app/util/BaseActivity;)Lcom/osp/app/util/an;

    invoke-static {v4}, Lcom/osp/app/util/an;->a(I)I

    move-result v3

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getColor(I)I

    move-result v3

    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setTextColor(I)V

    :cond_5
    const v0, 0x7f0c0159

    invoke-virtual {p0, v0}, Lcom/osp/app/signin/DuplicateSMSVerificationActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    if-eqz v0, :cond_6

    invoke-static {p0}, Lcom/osp/app/util/an;->a(Lcom/osp/app/util/BaseActivity;)Lcom/osp/app/util/an;

    invoke-static {v4}, Lcom/osp/app/util/an;->a(I)I

    move-result v3

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getColor(I)I

    move-result v3

    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setTextColor(I)V

    :cond_6
    const v0, 0x7f0c00c5

    invoke-virtual {p0, v0}, Lcom/osp/app/signin/DuplicateSMSVerificationActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    if-eqz v0, :cond_7

    invoke-static {p0}, Lcom/osp/app/util/an;->a(Lcom/osp/app/util/BaseActivity;)Lcom/osp/app/util/an;

    invoke-static {v4}, Lcom/osp/app/util/an;->a(I)I

    move-result v3

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getColor(I)I

    move-result v2

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setTextColor(I)V

    :cond_7
    invoke-static {}, Lcom/osp/app/util/r;->v()Z

    move-result v0

    if-eqz v0, :cond_9

    const/4 v0, 0x2

    new-array v2, v0, [I

    fill-array-data v2, :array_0

    move v0, v1

    :goto_1
    array-length v3, v2

    if-ge v0, v3, :cond_9

    aget v3, v2, v0

    invoke-virtual {p0, v3}, Lcom/osp/app/signin/DuplicateSMSVerificationActivity;->findViewById(I)Landroid/view/View;

    move-result-object v3

    if-eqz v3, :cond_8

    const/16 v4, 0x8

    invoke-virtual {v3, v4}, Landroid/view/View;->setVisibility(I)V

    :cond_8
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    :cond_9
    const v0, 0x7f0c0065

    invoke-virtual {p0, v0}, Lcom/osp/app/signin/DuplicateSMSVerificationActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/osp/app/signin/BottomSoftkeyLinearLayout;

    iput-object v0, p0, Lcom/osp/app/signin/DuplicateSMSVerificationActivity;->N:Lcom/osp/app/signin/BottomSoftkeyLinearLayout;

    iget-object v0, p0, Lcom/osp/app/signin/DuplicateSMSVerificationActivity;->N:Lcom/osp/app/signin/BottomSoftkeyLinearLayout;

    if-eqz v0, :cond_a

    iget-object v0, p0, Lcom/osp/app/signin/DuplicateSMSVerificationActivity;->N:Lcom/osp/app/signin/BottomSoftkeyLinearLayout;

    invoke-virtual {p0}, Lcom/osp/app/signin/DuplicateSMSVerificationActivity;->h()Z

    move-result v2

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    invoke-virtual {v0, p0, v2}, Lcom/osp/app/signin/BottomSoftkeyLinearLayout;->a(Landroid/content/Context;Ljava/lang/Boolean;)V

    invoke-static {}, Lcom/osp/device/b;->b()Lcom/osp/device/b;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/osp/device/b;->l(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_13

    iget-object v0, p0, Lcom/osp/app/signin/DuplicateSMSVerificationActivity;->N:Lcom/osp/app/signin/BottomSoftkeyLinearLayout;

    invoke-virtual {p0, v5}, Lcom/osp/app/signin/DuplicateSMSVerificationActivity;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Lcom/osp/app/signin/BottomSoftkeyLinearLayout;->b(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/osp/app/signin/DuplicateSMSVerificationActivity;->N:Lcom/osp/app/signin/BottomSoftkeyLinearLayout;

    invoke-virtual {p0, v5}, Lcom/osp/app/signin/DuplicateSMSVerificationActivity;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Lcom/osp/app/signin/BottomSoftkeyLinearLayout;->d(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/osp/app/signin/DuplicateSMSVerificationActivity;->N:Lcom/osp/app/signin/BottomSoftkeyLinearLayout;

    invoke-virtual {p0, v7}, Lcom/osp/app/signin/DuplicateSMSVerificationActivity;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Lcom/osp/app/signin/BottomSoftkeyLinearLayout;->c(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/osp/app/signin/DuplicateSMSVerificationActivity;->N:Lcom/osp/app/signin/BottomSoftkeyLinearLayout;

    invoke-virtual {p0, v7}, Lcom/osp/app/signin/DuplicateSMSVerificationActivity;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Lcom/osp/app/signin/BottomSoftkeyLinearLayout;->e(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/osp/app/signin/DuplicateSMSVerificationActivity;->N:Lcom/osp/app/signin/BottomSoftkeyLinearLayout;

    iget-object v2, p0, Lcom/osp/app/signin/DuplicateSMSVerificationActivity;->a:Landroid/view/View$OnClickListener;

    invoke-virtual {v0, v2}, Lcom/osp/app/signin/BottomSoftkeyLinearLayout;->a(Landroid/view/View$OnClickListener;)V

    iget-object v0, p0, Lcom/osp/app/signin/DuplicateSMSVerificationActivity;->N:Lcom/osp/app/signin/BottomSoftkeyLinearLayout;

    iget-object v2, p0, Lcom/osp/app/signin/DuplicateSMSVerificationActivity;->b:Landroid/view/View$OnClickListener;

    invoke-virtual {v0, v2}, Lcom/osp/app/signin/BottomSoftkeyLinearLayout;->b(Landroid/view/View$OnClickListener;)V

    :cond_a
    :goto_2
    invoke-static {}, Lcom/osp/app/signin/SamsungService;->a()Z

    move-result v0

    if-nez v0, :cond_b

    invoke-static {}, Lcom/osp/device/b;->b()Lcom/osp/device/b;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/osp/device/b;->i(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_b

    iget-object v0, p0, Lcom/osp/app/signin/DuplicateSMSVerificationActivity;->N:Lcom/osp/app/signin/BottomSoftkeyLinearLayout;

    if-eqz v0, :cond_b

    iget-object v0, p0, Lcom/osp/app/signin/DuplicateSMSVerificationActivity;->N:Lcom/osp/app/signin/BottomSoftkeyLinearLayout;

    const/16 v2, 0x8

    invoke-virtual {v0, v2}, Lcom/osp/app/signin/BottomSoftkeyLinearLayout;->setVisibility(I)V

    .line 394
    :cond_b
    invoke-static {}, Lcom/osp/device/b;->b()Lcom/osp/device/b;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/osp/device/b;->i(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_14

    invoke-static {}, Lcom/osp/app/signin/SamsungService;->a()Z

    move-result v0

    if-nez v0, :cond_c

    const-string v0, "next"

    invoke-virtual {p0, v0}, Lcom/osp/app/signin/DuplicateSMSVerificationActivity;->e(Ljava/lang/String;)V

    invoke-static {p0}, Lcom/msc/sa/c/d;->e(Landroid/app/Activity;)V

    :cond_c
    :goto_3
    invoke-virtual {p0}, Lcom/osp/app/signin/DuplicateSMSVerificationActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v0

    iget v0, v0, Landroid/content/res/Configuration;->orientation:I

    invoke-direct {p0, v0}, Lcom/osp/app/signin/DuplicateSMSVerificationActivity;->a(I)V

    const v0, 0x7f0c00c5

    invoke-virtual {p0, v0}, Lcom/osp/app/signin/DuplicateSMSVerificationActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    if-eqz v0, :cond_d

    const v2, 0x7f090134

    invoke-virtual {p0, v2}, Lcom/osp/app/signin/DuplicateSMSVerificationActivity;->getString(I)Ljava/lang/String;

    move-result-object v2

    new-array v3, v6, [Ljava/lang/Object;

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v3, v1

    invoke-static {v2, v3}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    :cond_d
    const v0, 0x7f0c00c3

    invoke-virtual {p0, v0}, Lcom/osp/app/signin/DuplicateSMSVerificationActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    if-eqz v0, :cond_e

    const/4 v2, 0x2

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setInputType(I)V

    new-instance v2, Lcom/osp/app/signin/eq;

    invoke-direct {v2, p0}, Lcom/osp/app/signin/eq;-><init>(Lcom/osp/app/signin/DuplicateSMSVerificationActivity;)V

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->addTextChangedListener(Landroid/text/TextWatcher;)V

    :cond_e
    const v0, 0x7f0c00c4

    invoke-virtual {p0, v0}, Lcom/osp/app/signin/DuplicateSMSVerificationActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/EditText;

    if-eqz v0, :cond_f

    const/4 v2, 0x2

    invoke-virtual {v0, v2}, Landroid/widget/EditText;->setInputType(I)V

    new-array v2, v6, [Landroid/text/InputFilter;

    new-instance v3, Landroid/text/InputFilter$LengthFilter;

    const/4 v4, 0x6

    invoke-direct {v3, v4}, Landroid/text/InputFilter$LengthFilter;-><init>(I)V

    aput-object v3, v2, v1

    invoke-virtual {v0, v2}, Landroid/widget/EditText;->setFilters([Landroid/text/InputFilter;)V

    new-instance v2, Lcom/osp/app/signin/er;

    invoke-direct {v2, p0}, Lcom/osp/app/signin/er;-><init>(Lcom/osp/app/signin/DuplicateSMSVerificationActivity;)V

    invoke-virtual {v0, v2}, Landroid/widget/EditText;->addTextChangedListener(Landroid/text/TextWatcher;)V

    new-instance v2, Lcom/osp/app/signin/es;

    invoke-direct {v2, p0, v0}, Lcom/osp/app/signin/es;-><init>(Lcom/osp/app/signin/DuplicateSMSVerificationActivity;Landroid/widget/EditText;)V

    invoke-virtual {v0, v2}, Landroid/widget/EditText;->setOnEditorActionListener(Landroid/widget/TextView$OnEditorActionListener;)V

    :cond_f
    invoke-direct {p0}, Lcom/osp/app/signin/DuplicateSMSVerificationActivity;->d()V

    const v0, 0x7f0c00c6

    invoke-virtual {p0, v0}, Lcom/osp/app/signin/DuplicateSMSVerificationActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    if-eqz v0, :cond_10

    const/4 v2, 0x4

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setVisibility(I)V

    :cond_10
    const v0, 0x7f0c00c7

    invoke-virtual {p0, v0}, Lcom/osp/app/signin/DuplicateSMSVerificationActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    if-eqz v0, :cond_11

    invoke-virtual {v0, p0}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    :cond_11
    invoke-direct {p0, v1}, Lcom/osp/app/signin/DuplicateSMSVerificationActivity;->a(Z)V

    .line 395
    iget-object v0, p0, Lcom/osp/app/signin/DuplicateSMSVerificationActivity;->K:Landroid/content/Intent;

    const-string v1, "key_internal_sign_up_inforamtion"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/osp/app/signin/SignUpinfo;

    if-eqz v0, :cond_15

    invoke-virtual {v0}, Lcom/osp/app/signin/SignUpinfo;->F()Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_12

    invoke-virtual {v0}, Lcom/osp/app/signin/SignUpinfo;->F()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/osp/app/signin/DuplicateSMSVerificationActivity;->y:Ljava/lang/String;

    invoke-virtual {v0}, Lcom/osp/app/signin/SignUpinfo;->E()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/osp/app/signin/DuplicateSMSVerificationActivity;->A:Ljava/lang/String;

    :cond_12
    invoke-direct {p0}, Lcom/osp/app/signin/DuplicateSMSVerificationActivity;->f()V

    .line 396
    :goto_4
    invoke-static {p0}, Lcom/osp/app/util/ad;->a(Landroid/content/Context;)Z

    move-result v0

    if-nez v0, :cond_16

    .line 398
    const/16 v0, 0xe2

    invoke-virtual {p0, v0}, Lcom/osp/app/signin/DuplicateSMSVerificationActivity;->c(I)V

    goto/16 :goto_0

    .line 393
    :cond_13
    iget-object v0, p0, Lcom/osp/app/signin/DuplicateSMSVerificationActivity;->N:Lcom/osp/app/signin/BottomSoftkeyLinearLayout;

    invoke-virtual {p0, v7}, Lcom/osp/app/signin/DuplicateSMSVerificationActivity;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Lcom/osp/app/signin/BottomSoftkeyLinearLayout;->b(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/osp/app/signin/DuplicateSMSVerificationActivity;->N:Lcom/osp/app/signin/BottomSoftkeyLinearLayout;

    invoke-virtual {p0, v7}, Lcom/osp/app/signin/DuplicateSMSVerificationActivity;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Lcom/osp/app/signin/BottomSoftkeyLinearLayout;->d(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/osp/app/signin/DuplicateSMSVerificationActivity;->N:Lcom/osp/app/signin/BottomSoftkeyLinearLayout;

    invoke-virtual {p0, v5}, Lcom/osp/app/signin/DuplicateSMSVerificationActivity;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Lcom/osp/app/signin/BottomSoftkeyLinearLayout;->c(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/osp/app/signin/DuplicateSMSVerificationActivity;->N:Lcom/osp/app/signin/BottomSoftkeyLinearLayout;

    invoke-virtual {p0, v5}, Lcom/osp/app/signin/DuplicateSMSVerificationActivity;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Lcom/osp/app/signin/BottomSoftkeyLinearLayout;->e(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/osp/app/signin/DuplicateSMSVerificationActivity;->N:Lcom/osp/app/signin/BottomSoftkeyLinearLayout;

    iget-object v2, p0, Lcom/osp/app/signin/DuplicateSMSVerificationActivity;->a:Landroid/view/View$OnClickListener;

    invoke-virtual {v0, v2}, Lcom/osp/app/signin/BottomSoftkeyLinearLayout;->b(Landroid/view/View$OnClickListener;)V

    iget-object v0, p0, Lcom/osp/app/signin/DuplicateSMSVerificationActivity;->N:Lcom/osp/app/signin/BottomSoftkeyLinearLayout;

    iget-object v2, p0, Lcom/osp/app/signin/DuplicateSMSVerificationActivity;->b:Landroid/view/View$OnClickListener;

    invoke-virtual {v0, v2}, Lcom/osp/app/signin/BottomSoftkeyLinearLayout;->a(Landroid/view/View$OnClickListener;)V

    goto/16 :goto_2

    .line 394
    :cond_14
    iget-object v0, p0, Lcom/osp/app/signin/DuplicateSMSVerificationActivity;->N:Lcom/osp/app/signin/BottomSoftkeyLinearLayout;

    if-eqz v0, :cond_c

    iget-object v0, p0, Lcom/osp/app/signin/DuplicateSMSVerificationActivity;->N:Lcom/osp/app/signin/BottomSoftkeyLinearLayout;

    invoke-virtual {v0, v1}, Lcom/osp/app/signin/BottomSoftkeyLinearLayout;->setVisibility(I)V

    invoke-static {}, Lcom/osp/app/signin/SamsungService;->a()Z

    move-result v0

    if-nez v0, :cond_c

    iget-object v0, p0, Lcom/osp/app/signin/DuplicateSMSVerificationActivity;->N:Lcom/osp/app/signin/BottomSoftkeyLinearLayout;

    const-string v2, "one_button"

    invoke-virtual {v0, v2}, Lcom/osp/app/signin/BottomSoftkeyLinearLayout;->a(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/osp/app/signin/DuplicateSMSVerificationActivity;->N:Lcom/osp/app/signin/BottomSoftkeyLinearLayout;

    invoke-virtual {p0, v5}, Lcom/osp/app/signin/DuplicateSMSVerificationActivity;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Lcom/osp/app/signin/BottomSoftkeyLinearLayout;->c(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/osp/app/signin/DuplicateSMSVerificationActivity;->N:Lcom/osp/app/signin/BottomSoftkeyLinearLayout;

    invoke-virtual {p0, v5}, Lcom/osp/app/signin/DuplicateSMSVerificationActivity;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Lcom/osp/app/signin/BottomSoftkeyLinearLayout;->e(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/osp/app/signin/DuplicateSMSVerificationActivity;->N:Lcom/osp/app/signin/BottomSoftkeyLinearLayout;

    iget-object v2, p0, Lcom/osp/app/signin/DuplicateSMSVerificationActivity;->a:Landroid/view/View$OnClickListener;

    invoke-virtual {v0, v2}, Lcom/osp/app/signin/BottomSoftkeyLinearLayout;->b(Landroid/view/View$OnClickListener;)V

    goto/16 :goto_3

    .line 395
    :cond_15
    invoke-virtual {p0, v6}, Lcom/osp/app/signin/DuplicateSMSVerificationActivity;->b(I)V

    invoke-virtual {p0}, Lcom/osp/app/signin/DuplicateSMSVerificationActivity;->finish()V

    goto :goto_4

    .line 401
    :cond_16
    invoke-direct {p0}, Lcom/osp/app/signin/DuplicateSMSVerificationActivity;->s()V

    goto/16 :goto_0

    .line 393
    :array_0
    .array-data 4
        0x7f0c00a5
        0x7f0c00a8
    .end array-data
.end method

.method protected onDestroy()V
    .locals 2

    .prologue
    .line 457
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v0, "DSMSV"

    const-string v1, "onDestroy"

    invoke-static {v0, v1}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 458
    iget-object v0, p0, Lcom/osp/app/signin/DuplicateSMSVerificationActivity;->G:Lcom/osp/app/signin/et;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/osp/app/signin/DuplicateSMSVerificationActivity;->G:Lcom/osp/app/signin/et;

    invoke-virtual {v0}, Lcom/osp/app/signin/et;->getStatus()Landroid/os/AsyncTask$Status;

    move-result-object v0

    sget-object v1, Landroid/os/AsyncTask$Status;->RUNNING:Landroid/os/AsyncTask$Status;

    if-ne v0, v1, :cond_0

    .line 460
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v0, "DSMSV"

    const-string v1, "SMSAuthenticateTask cancelTask"

    invoke-static {v0, v1}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 461
    iget-object v0, p0, Lcom/osp/app/signin/DuplicateSMSVerificationActivity;->G:Lcom/osp/app/signin/et;

    invoke-virtual {v0}, Lcom/osp/app/signin/et;->d()V

    .line 462
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/osp/app/signin/DuplicateSMSVerificationActivity;->G:Lcom/osp/app/signin/et;

    .line 465
    :cond_0
    invoke-super {p0}, Lcom/osp/app/util/BaseActivity;->onDestroy()V

    .line 466
    return-void
.end method

.method public onOptionsItemSelected(Landroid/view/MenuItem;)Z
    .locals 2

    .prologue
    .line 984
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0xb

    if-lt v0, v1, :cond_0

    .line 986
    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    .line 997
    const/4 v0, 0x0

    .line 1000
    :goto_0
    return v0

    .line 989
    :sswitch_0
    invoke-virtual {p0}, Lcom/osp/app/signin/DuplicateSMSVerificationActivity;->onBackPressed()V

    .line 1000
    :cond_0
    :goto_1
    const/4 v0, 0x1

    goto :goto_0

    .line 993
    :sswitch_1
    invoke-direct {p0}, Lcom/osp/app/signin/DuplicateSMSVerificationActivity;->u()V

    goto :goto_1

    .line 986
    :sswitch_data_0
    .sparse-switch
        0x102002c -> :sswitch_0
        0x7f0c0196 -> :sswitch_1
    .end sparse-switch
.end method

.method protected onPause()V
    .locals 1

    .prologue
    .line 437
    iget-object v0, p0, Lcom/osp/app/signin/DuplicateSMSVerificationActivity;->P:Landroid/content/BroadcastReceiver;

    invoke-virtual {p0, v0}, Lcom/osp/app/signin/DuplicateSMSVerificationActivity;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    .line 439
    invoke-super {p0}, Lcom/osp/app/util/BaseActivity;->onPause()V

    .line 440
    return-void
.end method

.method protected onResume()V
    .locals 2

    .prologue
    .line 408
    new-instance v0, Landroid/content/IntentFilter;

    const-string v1, "android.provider.Telephony.SMS_RECEIVED"

    invoke-direct {v0, v1}, Landroid/content/IntentFilter;-><init>(Ljava/lang/String;)V

    .line 409
    iget-object v1, p0, Lcom/osp/app/signin/DuplicateSMSVerificationActivity;->P:Landroid/content/BroadcastReceiver;

    invoke-virtual {p0, v1, v0}, Lcom/osp/app/signin/DuplicateSMSVerificationActivity;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    .line 412
    invoke-static {p0}, Lcom/osp/app/util/ad;->d(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 424
    invoke-virtual {p0}, Lcom/osp/app/signin/DuplicateSMSVerificationActivity;->finish()V

    .line 427
    :cond_0
    invoke-virtual {p0}, Lcom/osp/app/signin/DuplicateSMSVerificationActivity;->isFinishing()Z

    move-result v0

    if-nez v0, :cond_1

    .line 429
    invoke-direct {p0}, Lcom/osp/app/signin/DuplicateSMSVerificationActivity;->v()V

    .line 431
    :cond_1
    invoke-super {p0}, Lcom/osp/app/util/BaseActivity;->onResume()V

    .line 432
    return-void
.end method
