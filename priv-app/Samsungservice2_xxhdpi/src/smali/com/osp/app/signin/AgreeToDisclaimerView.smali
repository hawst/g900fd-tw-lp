.class public Lcom/osp/app/signin/AgreeToDisclaimerView;
.super Lcom/osp/app/util/BaseActivity;
.source "AgreeToDisclaimerView.java"


# instance fields
.field private A:Landroid/widget/TextView;

.field private B:Landroid/widget/TextView;

.field private C:Landroid/widget/TextView;

.field private D:Landroid/widget/TextView;

.field private E:Lcom/osp/app/signin/el;

.field private F:Ljava/lang/String;

.field private G:Ljava/lang/String;

.field private H:Ljava/lang/String;

.field private I:Lcom/osp/app/signin/BottomSoftkeyLinearLayout;

.field private J:Lcom/osp/app/signin/cd;

.field private final K:Landroid/view/View$OnClickListener;

.field private final L:Landroid/view/View$OnClickListener;

.field private a:Landroid/widget/TextView;

.field private b:Landroid/widget/ImageView;

.field private c:Landroid/widget/TextView;

.field private d:Landroid/widget/TextView;

.field private e:Landroid/widget/LinearLayout;

.field private f:Landroid/widget/TextView;

.field private y:Landroid/widget/LinearLayout;

.field private z:Landroid/widget/TextView;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 42
    invoke-direct {p0}, Lcom/osp/app/util/BaseActivity;-><init>()V

    .line 77
    new-instance v0, Lcom/osp/app/signin/ce;

    invoke-direct {v0, p0}, Lcom/osp/app/signin/ce;-><init>(Lcom/osp/app/signin/AgreeToDisclaimerView;)V

    iput-object v0, p0, Lcom/osp/app/signin/AgreeToDisclaimerView;->K:Landroid/view/View$OnClickListener;

    .line 106
    new-instance v0, Lcom/osp/app/signin/cf;

    invoke-direct {v0, p0}, Lcom/osp/app/signin/cf;-><init>(Lcom/osp/app/signin/AgreeToDisclaimerView;)V

    iput-object v0, p0, Lcom/osp/app/signin/AgreeToDisclaimerView;->L:Landroid/view/View$OnClickListener;

    .line 315
    return-void
.end method

.method static synthetic a(Ljava/lang/String;)Landroid/graphics/Bitmap;
    .locals 1

    .prologue
    .line 42
    invoke-static {p0}, Lcom/osp/app/signin/AgreeToDisclaimerView;->b(Ljava/lang/String;)Landroid/graphics/Bitmap;

    move-result-object v0

    return-object v0
.end method

.method static synthetic a(Lcom/osp/app/signin/AgreeToDisclaimerView;Lcom/osp/app/signin/cd;)Lcom/osp/app/signin/cd;
    .locals 0

    .prologue
    .line 42
    iput-object p1, p0, Lcom/osp/app/signin/AgreeToDisclaimerView;->J:Lcom/osp/app/signin/cd;

    return-object p1
.end method

.method static synthetic a(Lcom/osp/app/signin/AgreeToDisclaimerView;)Lcom/osp/app/signin/el;
    .locals 1

    .prologue
    .line 42
    iget-object v0, p0, Lcom/osp/app/signin/AgreeToDisclaimerView;->E:Lcom/osp/app/signin/el;

    return-object v0
.end method

.method static synthetic a(Lcom/osp/app/signin/AgreeToDisclaimerView;Lcom/osp/app/signin/el;)Lcom/osp/app/signin/el;
    .locals 0

    .prologue
    .line 42
    iput-object p1, p0, Lcom/osp/app/signin/AgreeToDisclaimerView;->E:Lcom/osp/app/signin/el;

    return-object p1
.end method

.method private a(I)V
    .locals 4

    .prologue
    const v3, 0x7f08011e

    .line 269
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "EmailValidation::initLayoutParams orientation : "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;)V

    .line 271
    invoke-static {}, Lcom/osp/device/b;->b()Lcom/osp/device/b;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/osp/device/b;->i(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 273
    invoke-virtual {p0}, Lcom/osp/app/signin/AgreeToDisclaimerView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f08011f

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v0

    float-to-int v0, v0

    .line 274
    invoke-virtual {p0}, Lcom/osp/app/signin/AgreeToDisclaimerView;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1, v3}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v1

    float-to-int v1, v1

    .line 275
    invoke-static {}, Lcom/osp/app/signin/SamsungService;->a()Z

    move-result v2

    if-nez v2, :cond_0

    .line 277
    invoke-virtual {p0}, Lcom/osp/app/signin/AgreeToDisclaimerView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0, v3}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v0

    float-to-int v0, v0

    .line 280
    :cond_0
    const v2, 0x7f0c0164

    invoke-virtual {p0, v2}, Lcom/osp/app/signin/AgreeToDisclaimerView;->findViewById(I)Landroid/view/View;

    move-result-object v2

    .line 281
    invoke-static {}, Lcom/osp/app/util/q;->a()Lcom/osp/app/util/q;

    invoke-static {v2, v0, v1, p1}, Lcom/osp/app/util/q;->b(Landroid/view/View;III)V

    .line 283
    :cond_1
    return-void
.end method

.method static synthetic a(Lcom/osp/app/signin/AgreeToDisclaimerView;Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 42
    new-instance v0, Landroid/content/Intent;

    const-string v1, "com.msc.action.samsungaccount.web_with_close_button"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    const-string v1, "ServerUrl"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    invoke-virtual {p0, v0}, Lcom/osp/app/signin/AgreeToDisclaimerView;->startActivity(Landroid/content/Intent;)V

    return-void
.end method

.method private static b(Ljava/lang/String;)Landroid/graphics/Bitmap;
    .locals 3

    .prologue
    .line 602
    :try_start_0
    new-instance v0, Ljava/net/URL;

    invoke-direct {v0, p0}, Ljava/net/URL;-><init>(Ljava/lang/String;)V

    .line 607
    invoke-virtual {v0}, Ljava/net/URL;->openConnection()Ljava/net/URLConnection;

    move-result-object v0

    .line 608
    invoke-virtual {v0}, Ljava/net/URLConnection;->connect()V

    .line 610
    invoke-virtual {v0}, Ljava/net/URLConnection;->getContentLength()I

    move-result v1

    .line 611
    new-instance v2, Ljava/io/BufferedInputStream;

    invoke-virtual {v0}, Ljava/net/URLConnection;->getInputStream()Ljava/io/InputStream;

    move-result-object v0

    invoke-direct {v2, v0, v1}, Ljava/io/BufferedInputStream;-><init>(Ljava/io/InputStream;I)V

    .line 612
    invoke-static {v2}, Landroid/graphics/BitmapFactory;->decodeStream(Ljava/io/InputStream;)Landroid/graphics/Bitmap;

    move-result-object v0

    .line 613
    invoke-virtual {v2}, Ljava/io/BufferedInputStream;->close()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 620
    :goto_0
    return-object v0

    .line 614
    :catch_0
    move-exception v0

    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    .line 617
    const/4 v0, 0x0

    goto :goto_0
.end method

.method static synthetic b(Lcom/osp/app/signin/AgreeToDisclaimerView;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 42
    iget-object v0, p0, Lcom/osp/app/signin/AgreeToDisclaimerView;->F:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic c(Lcom/osp/app/signin/AgreeToDisclaimerView;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 42
    iget-object v0, p0, Lcom/osp/app/signin/AgreeToDisclaimerView;->G:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic d(Lcom/osp/app/signin/AgreeToDisclaimerView;)Lcom/osp/app/signin/cd;
    .locals 1

    .prologue
    .line 42
    iget-object v0, p0, Lcom/osp/app/signin/AgreeToDisclaimerView;->J:Lcom/osp/app/signin/cd;

    return-object v0
.end method

.method static synthetic e(Lcom/osp/app/signin/AgreeToDisclaimerView;)Landroid/widget/TextView;
    .locals 1

    .prologue
    .line 42
    iget-object v0, p0, Lcom/osp/app/signin/AgreeToDisclaimerView;->a:Landroid/widget/TextView;

    return-object v0
.end method

.method static synthetic f(Lcom/osp/app/signin/AgreeToDisclaimerView;)Landroid/widget/TextView;
    .locals 1

    .prologue
    .line 42
    iget-object v0, p0, Lcom/osp/app/signin/AgreeToDisclaimerView;->c:Landroid/widget/TextView;

    return-object v0
.end method

.method static synthetic g(Lcom/osp/app/signin/AgreeToDisclaimerView;)Landroid/widget/TextView;
    .locals 1

    .prologue
    .line 42
    iget-object v0, p0, Lcom/osp/app/signin/AgreeToDisclaimerView;->d:Landroid/widget/TextView;

    return-object v0
.end method

.method static synthetic h(Lcom/osp/app/signin/AgreeToDisclaimerView;)Landroid/widget/TextView;
    .locals 1

    .prologue
    .line 42
    iget-object v0, p0, Lcom/osp/app/signin/AgreeToDisclaimerView;->z:Landroid/widget/TextView;

    return-object v0
.end method

.method static synthetic i(Lcom/osp/app/signin/AgreeToDisclaimerView;)Landroid/widget/ImageView;
    .locals 1

    .prologue
    .line 42
    iget-object v0, p0, Lcom/osp/app/signin/AgreeToDisclaimerView;->b:Landroid/widget/ImageView;

    return-object v0
.end method

.method static synthetic j(Lcom/osp/app/signin/AgreeToDisclaimerView;)Landroid/widget/TextView;
    .locals 1

    .prologue
    .line 42
    iget-object v0, p0, Lcom/osp/app/signin/AgreeToDisclaimerView;->A:Landroid/widget/TextView;

    return-object v0
.end method

.method static synthetic k(Lcom/osp/app/signin/AgreeToDisclaimerView;)Landroid/widget/TextView;
    .locals 1

    .prologue
    .line 42
    iget-object v0, p0, Lcom/osp/app/signin/AgreeToDisclaimerView;->B:Landroid/widget/TextView;

    return-object v0
.end method

.method static synthetic l(Lcom/osp/app/signin/AgreeToDisclaimerView;)Landroid/widget/TextView;
    .locals 1

    .prologue
    .line 42
    iget-object v0, p0, Lcom/osp/app/signin/AgreeToDisclaimerView;->C:Landroid/widget/TextView;

    return-object v0
.end method

.method static synthetic m(Lcom/osp/app/signin/AgreeToDisclaimerView;)Landroid/widget/TextView;
    .locals 1

    .prologue
    .line 42
    iget-object v0, p0, Lcom/osp/app/signin/AgreeToDisclaimerView;->D:Landroid/widget/TextView;

    return-object v0
.end method

.method static synthetic n(Lcom/osp/app/signin/AgreeToDisclaimerView;)Landroid/widget/LinearLayout;
    .locals 1

    .prologue
    .line 42
    iget-object v0, p0, Lcom/osp/app/signin/AgreeToDisclaimerView;->y:Landroid/widget/LinearLayout;

    return-object v0
.end method

.method static synthetic o(Lcom/osp/app/signin/AgreeToDisclaimerView;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 42
    iget-object v0, p0, Lcom/osp/app/signin/AgreeToDisclaimerView;->H:Ljava/lang/String;

    return-object v0
.end method


# virtual methods
.method protected final a()V
    .locals 0

    .prologue
    .line 626
    return-void
.end method

.method protected final b()V
    .locals 0

    .prologue
    .line 631
    return-void
.end method

.method public onConfigurationChanged(Landroid/content/res/Configuration;)V
    .locals 2

    .prologue
    .line 260
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v0, "ATDV"

    const-string v1, "onConfigurationChanged()"

    invoke-static {v0, v1}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 262
    iget v0, p1, Landroid/content/res/Configuration;->orientation:I

    invoke-direct {p0, v0}, Lcom/osp/app/signin/AgreeToDisclaimerView;->a(I)V

    .line 264
    invoke-super {p0, p1}, Lcom/osp/app/util/BaseActivity;->onConfigurationChanged(Landroid/content/res/Configuration;)V

    .line 265
    return-void
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 8

    .prologue
    const v7, 0x7f020089

    const v6, 0x7f0900cd

    const v5, 0x7f0900cc

    const/4 v4, 0x0

    const v3, 0x7f070014

    .line 147
    invoke-super {p0, p1}, Lcom/osp/app/util/BaseActivity;->onCreate(Landroid/os/Bundle;)V

    .line 151
    invoke-virtual {p0}, Lcom/osp/app/signin/AgreeToDisclaimerView;->getIntent()Landroid/content/Intent;

    move-result-object v0

    .line 153
    const-string v1, "client_id"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/osp/app/signin/AgreeToDisclaimerView;->F:Ljava/lang/String;

    .line 154
    const-string v1, "client_secret"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/osp/app/signin/AgreeToDisclaimerView;->G:Ljava/lang/String;

    .line 159
    const-string v0, "3rdDisclaimer.txt"

    iput-object v0, p0, Lcom/osp/app/signin/AgreeToDisclaimerView;->H:Ljava/lang/String;

    .line 161
    const v0, 0x7f030011

    invoke-static {p0}, Lcom/osp/app/util/r;->q(Landroid/content/Context;)Z

    move-result v1

    invoke-virtual {p0, v0, v1}, Lcom/osp/app/signin/AgreeToDisclaimerView;->a(IZ)V

    .line 163
    invoke-virtual {p0}, Lcom/osp/app/signin/AgreeToDisclaimerView;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    iget-object v0, p0, Lcom/osp/app/signin/AgreeToDisclaimerView;->a:Landroid/widget/TextView;

    if-nez v0, :cond_0

    const v0, 0x7f0c008f

    invoke-virtual {p0, v0}, Lcom/osp/app/signin/AgreeToDisclaimerView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/osp/app/signin/AgreeToDisclaimerView;->a:Landroid/widget/TextView;

    :cond_0
    iget-object v0, p0, Lcom/osp/app/signin/AgreeToDisclaimerView;->b:Landroid/widget/ImageView;

    if-nez v0, :cond_1

    const v0, 0x7f0c008a

    invoke-virtual {p0, v0}, Lcom/osp/app/signin/AgreeToDisclaimerView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/osp/app/signin/AgreeToDisclaimerView;->b:Landroid/widget/ImageView;

    :cond_1
    iget-object v0, p0, Lcom/osp/app/signin/AgreeToDisclaimerView;->c:Landroid/widget/TextView;

    if-nez v0, :cond_2

    const v0, 0x7f0c008b

    invoke-virtual {p0, v0}, Lcom/osp/app/signin/AgreeToDisclaimerView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/osp/app/signin/AgreeToDisclaimerView;->c:Landroid/widget/TextView;

    :cond_2
    iget-object v0, p0, Lcom/osp/app/signin/AgreeToDisclaimerView;->d:Landroid/widget/TextView;

    if-nez v0, :cond_3

    const v0, 0x7f0c008c

    invoke-virtual {p0, v0}, Lcom/osp/app/signin/AgreeToDisclaimerView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/osp/app/signin/AgreeToDisclaimerView;->d:Landroid/widget/TextView;

    iget-object v0, p0, Lcom/osp/app/signin/AgreeToDisclaimerView;->d:Landroid/widget/TextView;

    invoke-static {p0}, Lcom/osp/app/util/an;->a(Lcom/osp/app/util/BaseActivity;)Lcom/osp/app/util/an;

    const v2, 0x7f070008

    invoke-static {v2}, Lcom/osp/app/util/an;->a(I)I

    move-result v2

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v2

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setTextColor(I)V

    :cond_3
    iget-object v0, p0, Lcom/osp/app/signin/AgreeToDisclaimerView;->e:Landroid/widget/LinearLayout;

    if-nez v0, :cond_4

    const v0, 0x7f0c008d

    invoke-virtual {p0, v0}, Lcom/osp/app/signin/AgreeToDisclaimerView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcom/osp/app/signin/AgreeToDisclaimerView;->e:Landroid/widget/LinearLayout;

    :cond_4
    iget-object v0, p0, Lcom/osp/app/signin/AgreeToDisclaimerView;->f:Landroid/widget/TextView;

    if-nez v0, :cond_5

    const v0, 0x7f0c008e

    invoke-virtual {p0, v0}, Lcom/osp/app/signin/AgreeToDisclaimerView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/osp/app/signin/AgreeToDisclaimerView;->f:Landroid/widget/TextView;

    :cond_5
    iget-object v0, p0, Lcom/osp/app/signin/AgreeToDisclaimerView;->y:Landroid/widget/LinearLayout;

    if-nez v0, :cond_6

    const v0, 0x7f0c0090

    invoke-virtual {p0, v0}, Lcom/osp/app/signin/AgreeToDisclaimerView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcom/osp/app/signin/AgreeToDisclaimerView;->y:Landroid/widget/LinearLayout;

    iget-object v0, p0, Lcom/osp/app/signin/AgreeToDisclaimerView;->y:Landroid/widget/LinearLayout;

    invoke-static {p0}, Lcom/osp/app/util/an;->a(Lcom/osp/app/util/BaseActivity;)Lcom/osp/app/util/an;

    const v2, 0x7f020033

    invoke-static {v2}, Lcom/osp/app/util/an;->a(I)I

    move-result v2

    invoke-virtual {v0, v2}, Landroid/widget/LinearLayout;->setBackgroundResource(I)V

    :cond_6
    iget-object v0, p0, Lcom/osp/app/signin/AgreeToDisclaimerView;->z:Landroid/widget/TextView;

    if-nez v0, :cond_7

    const v0, 0x7f0c0091

    invoke-virtual {p0, v0}, Lcom/osp/app/signin/AgreeToDisclaimerView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/osp/app/signin/AgreeToDisclaimerView;->z:Landroid/widget/TextView;

    iget-object v0, p0, Lcom/osp/app/signin/AgreeToDisclaimerView;->z:Landroid/widget/TextView;

    invoke-static {p0}, Lcom/osp/app/util/an;->a(Lcom/osp/app/util/BaseActivity;)Lcom/osp/app/util/an;

    const v2, 0x7f070004

    invoke-static {v2}, Lcom/osp/app/util/an;->a(I)I

    move-result v2

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v2

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setTextColor(I)V

    iget-object v0, p0, Lcom/osp/app/signin/AgreeToDisclaimerView;->z:Landroid/widget/TextView;

    const-string v2, ""

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    :cond_7
    iget-object v0, p0, Lcom/osp/app/signin/AgreeToDisclaimerView;->A:Landroid/widget/TextView;

    if-nez v0, :cond_8

    const v0, 0x7f0c0092

    invoke-virtual {p0, v0}, Lcom/osp/app/signin/AgreeToDisclaimerView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/osp/app/signin/AgreeToDisclaimerView;->A:Landroid/widget/TextView;

    iget-object v0, p0, Lcom/osp/app/signin/AgreeToDisclaimerView;->A:Landroid/widget/TextView;

    invoke-static {p0}, Lcom/osp/app/util/an;->a(Lcom/osp/app/util/BaseActivity;)Lcom/osp/app/util/an;

    invoke-static {v3}, Lcom/osp/app/util/an;->a(I)I

    move-result v2

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v2

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setTextColor(I)V

    iget-object v0, p0, Lcom/osp/app/signin/AgreeToDisclaimerView;->A:Landroid/widget/TextView;

    invoke-static {p0}, Lcom/osp/app/util/an;->a(Lcom/osp/app/util/BaseActivity;)Lcom/osp/app/util/an;

    invoke-static {v3}, Lcom/osp/app/util/an;->a(I)I

    move-result v2

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v2

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setLinkTextColor(I)V

    :cond_8
    iget-object v0, p0, Lcom/osp/app/signin/AgreeToDisclaimerView;->B:Landroid/widget/TextView;

    if-nez v0, :cond_9

    const v0, 0x7f0c0093

    invoke-virtual {p0, v0}, Lcom/osp/app/signin/AgreeToDisclaimerView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/osp/app/signin/AgreeToDisclaimerView;->B:Landroid/widget/TextView;

    iget-object v0, p0, Lcom/osp/app/signin/AgreeToDisclaimerView;->B:Landroid/widget/TextView;

    invoke-static {p0}, Lcom/osp/app/util/an;->a(Lcom/osp/app/util/BaseActivity;)Lcom/osp/app/util/an;

    invoke-static {v7}, Lcom/osp/app/util/an;->a(I)I

    move-result v2

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setBackgroundResource(I)V

    iget-object v0, p0, Lcom/osp/app/signin/AgreeToDisclaimerView;->B:Landroid/widget/TextView;

    invoke-static {p0}, Lcom/osp/app/util/an;->a(Lcom/osp/app/util/BaseActivity;)Lcom/osp/app/util/an;

    invoke-static {v3}, Lcom/osp/app/util/an;->a(I)I

    move-result v2

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v2

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setTextColor(I)V

    iget-object v0, p0, Lcom/osp/app/signin/AgreeToDisclaimerView;->B:Landroid/widget/TextView;

    invoke-static {p0}, Lcom/osp/app/util/an;->a(Lcom/osp/app/util/BaseActivity;)Lcom/osp/app/util/an;

    invoke-static {v3}, Lcom/osp/app/util/an;->a(I)I

    move-result v2

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v2

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setLinkTextColor(I)V

    :cond_9
    iget-object v0, p0, Lcom/osp/app/signin/AgreeToDisclaimerView;->C:Landroid/widget/TextView;

    if-nez v0, :cond_a

    const v0, 0x7f0c0094

    invoke-virtual {p0, v0}, Lcom/osp/app/signin/AgreeToDisclaimerView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/osp/app/signin/AgreeToDisclaimerView;->C:Landroid/widget/TextView;

    iget-object v0, p0, Lcom/osp/app/signin/AgreeToDisclaimerView;->C:Landroid/widget/TextView;

    invoke-static {p0}, Lcom/osp/app/util/an;->a(Lcom/osp/app/util/BaseActivity;)Lcom/osp/app/util/an;

    invoke-static {v3}, Lcom/osp/app/util/an;->a(I)I

    move-result v2

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v2

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setTextColor(I)V

    iget-object v0, p0, Lcom/osp/app/signin/AgreeToDisclaimerView;->C:Landroid/widget/TextView;

    invoke-static {p0}, Lcom/osp/app/util/an;->a(Lcom/osp/app/util/BaseActivity;)Lcom/osp/app/util/an;

    invoke-static {v3}, Lcom/osp/app/util/an;->a(I)I

    move-result v2

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v2

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setLinkTextColor(I)V

    :cond_a
    iget-object v0, p0, Lcom/osp/app/signin/AgreeToDisclaimerView;->D:Landroid/widget/TextView;

    if-nez v0, :cond_b

    const v0, 0x7f0c0095

    invoke-virtual {p0, v0}, Lcom/osp/app/signin/AgreeToDisclaimerView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/osp/app/signin/AgreeToDisclaimerView;->D:Landroid/widget/TextView;

    iget-object v0, p0, Lcom/osp/app/signin/AgreeToDisclaimerView;->D:Landroid/widget/TextView;

    invoke-static {p0}, Lcom/osp/app/util/an;->a(Lcom/osp/app/util/BaseActivity;)Lcom/osp/app/util/an;

    invoke-static {v7}, Lcom/osp/app/util/an;->a(I)I

    move-result v2

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setBackgroundResource(I)V

    iget-object v0, p0, Lcom/osp/app/signin/AgreeToDisclaimerView;->D:Landroid/widget/TextView;

    invoke-static {p0}, Lcom/osp/app/util/an;->a(Lcom/osp/app/util/BaseActivity;)Lcom/osp/app/util/an;

    invoke-static {v3}, Lcom/osp/app/util/an;->a(I)I

    move-result v2

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v2

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setTextColor(I)V

    iget-object v0, p0, Lcom/osp/app/signin/AgreeToDisclaimerView;->D:Landroid/widget/TextView;

    invoke-static {p0}, Lcom/osp/app/util/an;->a(Lcom/osp/app/util/BaseActivity;)Lcom/osp/app/util/an;

    invoke-static {v3}, Lcom/osp/app/util/an;->a(I)I

    move-result v2

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setLinkTextColor(I)V

    :cond_b
    const v0, 0x7f0c0065

    invoke-virtual {p0, v0}, Lcom/osp/app/signin/AgreeToDisclaimerView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/osp/app/signin/BottomSoftkeyLinearLayout;

    iput-object v0, p0, Lcom/osp/app/signin/AgreeToDisclaimerView;->I:Lcom/osp/app/signin/BottomSoftkeyLinearLayout;

    iget-object v0, p0, Lcom/osp/app/signin/AgreeToDisclaimerView;->I:Lcom/osp/app/signin/BottomSoftkeyLinearLayout;

    if-eqz v0, :cond_c

    iget-object v0, p0, Lcom/osp/app/signin/AgreeToDisclaimerView;->I:Lcom/osp/app/signin/BottomSoftkeyLinearLayout;

    invoke-virtual {p0}, Lcom/osp/app/signin/AgreeToDisclaimerView;->h()Z

    move-result v1

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-virtual {v0, p0, v1}, Lcom/osp/app/signin/BottomSoftkeyLinearLayout;->a(Landroid/content/Context;Ljava/lang/Boolean;)V

    invoke-static {}, Lcom/osp/device/b;->b()Lcom/osp/device/b;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/osp/device/b;->l(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_e

    iget-object v0, p0, Lcom/osp/app/signin/AgreeToDisclaimerView;->I:Lcom/osp/app/signin/BottomSoftkeyLinearLayout;

    invoke-virtual {p0, v6}, Lcom/osp/app/signin/AgreeToDisclaimerView;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/osp/app/signin/BottomSoftkeyLinearLayout;->b(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/osp/app/signin/AgreeToDisclaimerView;->I:Lcom/osp/app/signin/BottomSoftkeyLinearLayout;

    invoke-virtual {p0, v6}, Lcom/osp/app/signin/AgreeToDisclaimerView;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/osp/app/signin/BottomSoftkeyLinearLayout;->d(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/osp/app/signin/AgreeToDisclaimerView;->I:Lcom/osp/app/signin/BottomSoftkeyLinearLayout;

    invoke-virtual {p0, v5}, Lcom/osp/app/signin/AgreeToDisclaimerView;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/osp/app/signin/BottomSoftkeyLinearLayout;->c(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/osp/app/signin/AgreeToDisclaimerView;->I:Lcom/osp/app/signin/BottomSoftkeyLinearLayout;

    invoke-virtual {p0, v5}, Lcom/osp/app/signin/AgreeToDisclaimerView;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/osp/app/signin/BottomSoftkeyLinearLayout;->e(Ljava/lang/String;)V

    .line 165
    :cond_c
    :goto_0
    const v0, 0x7f090059

    invoke-virtual {p0, v0}, Lcom/osp/app/signin/AgreeToDisclaimerView;->getText(I)Ljava/lang/CharSequence;

    move-result-object v0

    .line 166
    new-instance v1, Landroid/text/SpannableString;

    invoke-direct {v1, v0}, Landroid/text/SpannableString;-><init>(Ljava/lang/CharSequence;)V

    .line 167
    new-instance v0, Landroid/text/style/URLSpan;

    const-string v2, ""

    invoke-direct {v0, v2}, Landroid/text/style/URLSpan;-><init>(Ljava/lang/String;)V

    .line 168
    invoke-virtual {v1}, Landroid/text/SpannableString;->length()I

    move-result v2

    invoke-virtual {v1, v0, v4, v2, v4}, Landroid/text/SpannableString;->setSpan(Ljava/lang/Object;III)V

    .line 169
    iget-object v0, p0, Lcom/osp/app/signin/AgreeToDisclaimerView;->B:Landroid/widget/TextView;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 170
    iget-object v0, p0, Lcom/osp/app/signin/AgreeToDisclaimerView;->B:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/osp/app/signin/AgreeToDisclaimerView;->K:Landroid/view/View$OnClickListener;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 172
    const v0, 0x7f090068

    invoke-virtual {p0, v0}, Lcom/osp/app/signin/AgreeToDisclaimerView;->getText(I)Ljava/lang/CharSequence;

    move-result-object v0

    .line 173
    new-instance v1, Landroid/text/SpannableString;

    invoke-direct {v1, v0}, Landroid/text/SpannableString;-><init>(Ljava/lang/CharSequence;)V

    .line 174
    new-instance v0, Landroid/text/style/URLSpan;

    const-string v2, ""

    invoke-direct {v0, v2}, Landroid/text/style/URLSpan;-><init>(Ljava/lang/String;)V

    .line 175
    invoke-virtual {v1}, Landroid/text/SpannableString;->length()I

    move-result v2

    invoke-virtual {v1, v0, v4, v2, v4}, Landroid/text/SpannableString;->setSpan(Ljava/lang/Object;III)V

    .line 176
    iget-object v0, p0, Lcom/osp/app/signin/AgreeToDisclaimerView;->D:Landroid/widget/TextView;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 177
    iget-object v0, p0, Lcom/osp/app/signin/AgreeToDisclaimerView;->D:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/osp/app/signin/AgreeToDisclaimerView;->L:Landroid/view/View$OnClickListener;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 180
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0xb

    if-ge v0, v1, :cond_10

    .line 182
    iget-object v0, p0, Lcom/osp/app/signin/AgreeToDisclaimerView;->I:Lcom/osp/app/signin/BottomSoftkeyLinearLayout;

    if-eqz v0, :cond_d

    .line 184
    iget-object v0, p0, Lcom/osp/app/signin/AgreeToDisclaimerView;->I:Lcom/osp/app/signin/BottomSoftkeyLinearLayout;

    invoke-virtual {v0, v4}, Lcom/osp/app/signin/BottomSoftkeyLinearLayout;->setVisibility(I)V

    .line 186
    new-instance v0, Lcom/osp/app/signin/cg;

    invoke-direct {v0, p0}, Lcom/osp/app/signin/cg;-><init>(Lcom/osp/app/signin/AgreeToDisclaimerView;)V

    .line 194
    new-instance v1, Lcom/osp/app/signin/ch;

    invoke-direct {v1, p0}, Lcom/osp/app/signin/ch;-><init>(Lcom/osp/app/signin/AgreeToDisclaimerView;)V

    .line 202
    invoke-static {}, Lcom/osp/device/b;->b()Lcom/osp/device/b;

    move-result-object v2

    invoke-virtual {v2, p0}, Lcom/osp/device/b;->l(Landroid/content/Context;)Z

    move-result v2

    if-eqz v2, :cond_f

    .line 204
    iget-object v2, p0, Lcom/osp/app/signin/AgreeToDisclaimerView;->I:Lcom/osp/app/signin/BottomSoftkeyLinearLayout;

    invoke-virtual {v2, v0}, Lcom/osp/app/signin/BottomSoftkeyLinearLayout;->a(Landroid/view/View$OnClickListener;)V

    .line 205
    iget-object v0, p0, Lcom/osp/app/signin/AgreeToDisclaimerView;->I:Lcom/osp/app/signin/BottomSoftkeyLinearLayout;

    invoke-virtual {v0, v1}, Lcom/osp/app/signin/BottomSoftkeyLinearLayout;->b(Landroid/view/View$OnClickListener;)V

    .line 227
    :cond_d
    :goto_1
    invoke-virtual {p0}, Lcom/osp/app/signin/AgreeToDisclaimerView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v0

    iget v0, v0, Landroid/content/res/Configuration;->orientation:I

    invoke-direct {p0, v0}, Lcom/osp/app/signin/AgreeToDisclaimerView;->a(I)V

    .line 231
    new-instance v0, Lcom/osp/app/signin/ck;

    invoke-direct {v0, p0}, Lcom/osp/app/signin/ck;-><init>(Lcom/osp/app/signin/AgreeToDisclaimerView;)V

    .line 232
    invoke-virtual {v0}, Lcom/osp/app/signin/ck;->b()V

    .line 233
    return-void

    .line 163
    :cond_e
    iget-object v0, p0, Lcom/osp/app/signin/AgreeToDisclaimerView;->I:Lcom/osp/app/signin/BottomSoftkeyLinearLayout;

    invoke-virtual {p0, v5}, Lcom/osp/app/signin/AgreeToDisclaimerView;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/osp/app/signin/BottomSoftkeyLinearLayout;->b(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/osp/app/signin/AgreeToDisclaimerView;->I:Lcom/osp/app/signin/BottomSoftkeyLinearLayout;

    invoke-virtual {p0, v5}, Lcom/osp/app/signin/AgreeToDisclaimerView;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/osp/app/signin/BottomSoftkeyLinearLayout;->d(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/osp/app/signin/AgreeToDisclaimerView;->I:Lcom/osp/app/signin/BottomSoftkeyLinearLayout;

    invoke-virtual {p0, v6}, Lcom/osp/app/signin/AgreeToDisclaimerView;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/osp/app/signin/BottomSoftkeyLinearLayout;->c(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/osp/app/signin/AgreeToDisclaimerView;->I:Lcom/osp/app/signin/BottomSoftkeyLinearLayout;

    invoke-virtual {p0, v6}, Lcom/osp/app/signin/AgreeToDisclaimerView;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/osp/app/signin/BottomSoftkeyLinearLayout;->e(Ljava/lang/String;)V

    goto/16 :goto_0

    .line 208
    :cond_f
    iget-object v2, p0, Lcom/osp/app/signin/AgreeToDisclaimerView;->I:Lcom/osp/app/signin/BottomSoftkeyLinearLayout;

    invoke-virtual {v2, v0}, Lcom/osp/app/signin/BottomSoftkeyLinearLayout;->b(Landroid/view/View$OnClickListener;)V

    .line 209
    iget-object v0, p0, Lcom/osp/app/signin/AgreeToDisclaimerView;->I:Lcom/osp/app/signin/BottomSoftkeyLinearLayout;

    invoke-virtual {v0, v1}, Lcom/osp/app/signin/BottomSoftkeyLinearLayout;->a(Landroid/view/View$OnClickListener;)V

    goto :goto_1

    .line 215
    :cond_10
    invoke-static {}, Lcom/osp/device/b;->b()Lcom/osp/device/b;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/osp/device/b;->i(Landroid/content/Context;)Z

    move-result v0

    if-nez v0, :cond_11

    .line 217
    iget-object v0, p0, Lcom/osp/app/signin/AgreeToDisclaimerView;->I:Lcom/osp/app/signin/BottomSoftkeyLinearLayout;

    if-eqz v0, :cond_11

    .line 219
    iget-object v0, p0, Lcom/osp/app/signin/AgreeToDisclaimerView;->I:Lcom/osp/app/signin/BottomSoftkeyLinearLayout;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Lcom/osp/app/signin/BottomSoftkeyLinearLayout;->setVisibility(I)V

    .line 224
    :cond_11
    const-string v0, "agree_decliene"

    invoke-virtual {p0, v0}, Lcom/osp/app/signin/AgreeToDisclaimerView;->e(Ljava/lang/String;)V

    goto :goto_1
.end method

.method public onCreateDialog(I)Landroid/app/Dialog;
    .locals 3

    .prologue
    .line 287
    const/4 v0, 0x0

    .line 288
    packed-switch p1, :pswitch_data_0

    .line 308
    :goto_0
    return-object v0

    .line 291
    :pswitch_0
    new-instance v0, Landroid/app/AlertDialog$Builder;

    invoke-direct {v0, p0}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    const v1, 0x7f09002d

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    const v1, 0x7f09006f

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setMessage(I)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    const v1, 0x7f090041

    new-instance v2, Lcom/osp/app/signin/cj;

    invoke-direct {v2, p0}, Lcom/osp/app/signin/cj;-><init>(Lcom/osp/app/signin/AgreeToDisclaimerView;)V

    invoke-virtual {v0, v1, v2}, Landroid/app/AlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    const v1, 0x7f09005c

    new-instance v2, Lcom/osp/app/signin/ci;

    invoke-direct {v2, p0}, Lcom/osp/app/signin/ci;-><init>(Lcom/osp/app/signin/AgreeToDisclaimerView;)V

    invoke-virtual {v0, v1, v2}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v0

    goto :goto_0

    .line 288
    nop

    :pswitch_data_0
    .packed-switch 0x64
        :pswitch_0
    .end packed-switch
.end method

.method public onOptionsItemSelected(Landroid/view/MenuItem;)Z
    .locals 3

    .prologue
    .line 237
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0xb

    if-ge v0, v1, :cond_0

    .line 239
    const/4 v0, 0x0

    .line 250
    :goto_0
    return v0

    .line 242
    :cond_0
    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    .line 250
    :goto_1
    const/4 v0, 0x1

    goto :goto_0

    .line 245
    :pswitch_0
    new-instance v0, Lcom/osp/app/signin/cd;

    iget-object v1, p0, Lcom/osp/app/signin/AgreeToDisclaimerView;->F:Ljava/lang/String;

    iget-object v2, p0, Lcom/osp/app/signin/AgreeToDisclaimerView;->G:Ljava/lang/String;

    invoke-direct {v0, p0, v1, v2}, Lcom/osp/app/signin/cd;-><init>(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)V

    iput-object v0, p0, Lcom/osp/app/signin/AgreeToDisclaimerView;->J:Lcom/osp/app/signin/cd;

    .line 246
    iget-object v0, p0, Lcom/osp/app/signin/AgreeToDisclaimerView;->J:Lcom/osp/app/signin/cd;

    invoke-virtual {v0}, Lcom/osp/app/signin/cd;->b()V

    goto :goto_1

    .line 249
    :pswitch_1
    const/16 v0, 0x64

    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1}, Lcom/osp/app/signin/AgreeToDisclaimerView;->a(ILandroid/content/DialogInterface$OnDismissListener;)V

    goto :goto_1

    .line 242
    nop

    :pswitch_data_0
    .packed-switch 0x7f0c018d
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method protected onResume()V
    .locals 1

    .prologue
    .line 726
    invoke-super {p0}, Lcom/osp/app/util/BaseActivity;->onResume()V

    .line 728
    invoke-static {p0}, Lcom/osp/app/util/ad;->d(Landroid/content/Context;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 730
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/osp/app/signin/AgreeToDisclaimerView;->b(I)V

    .line 731
    invoke-virtual {p0}, Lcom/osp/app/signin/AgreeToDisclaimerView;->finish()V

    .line 733
    :cond_0
    return-void
.end method
