.class final Lcom/osp/app/signin/du;
.super Lcom/msc/c/b;
.source "CheckSecurityInfoActivity.java"


# instance fields
.field final synthetic c:Lcom/osp/app/signin/CheckSecurityInfoActivity;

.field private d:I

.field private final e:Ljava/util/ArrayList;

.field private final f:I

.field private final g:Ljava/lang/String;

.field private final h:Ljava/lang/String;

.field private final i:I

.field private final j:I

.field private final k:I

.field private l:J


# direct methods
.method public constructor <init>(Lcom/osp/app/signin/CheckSecurityInfoActivity;Ljava/lang/String;Ljava/lang/String;)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 896
    iput-object p1, p0, Lcom/osp/app/signin/du;->c:Lcom/osp/app/signin/CheckSecurityInfoActivity;

    .line 897
    invoke-direct {p0, p1}, Lcom/msc/c/b;-><init>(Landroid/content/Context;)V

    .line 885
    iput v1, p0, Lcom/osp/app/signin/du;->i:I

    .line 887
    const/4 v0, 0x1

    iput v0, p0, Lcom/osp/app/signin/du;->j:I

    .line 889
    const/4 v0, 0x2

    iput v0, p0, Lcom/osp/app/signin/du;->k:I

    .line 899
    iput-object p2, p0, Lcom/osp/app/signin/du;->g:Ljava/lang/String;

    .line 900
    iput-object p3, p0, Lcom/osp/app/signin/du;->h:Ljava/lang/String;

    .line 901
    invoke-static {p1}, Lcom/osp/app/signin/CheckSecurityInfoActivity;->c(Lcom/osp/app/signin/CheckSecurityInfoActivity;)Ljava/util/ArrayList;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    iput v0, p0, Lcom/osp/app/signin/du;->f:I

    .line 902
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/osp/app/signin/du;->e:Ljava/util/ArrayList;

    .line 903
    iput v1, p0, Lcom/osp/app/signin/du;->d:I

    .line 904
    return-void
.end method

.method private b(Ljava/lang/String;)V
    .locals 3

    .prologue
    .line 987
    invoke-static {}, Lcom/msc/c/g;->a()Lcom/msc/c/g;

    .line 988
    iget-object v0, p0, Lcom/osp/app/signin/du;->c:Lcom/osp/app/signin/CheckSecurityInfoActivity;

    iget-object v1, p0, Lcom/osp/app/signin/du;->g:Ljava/lang/String;

    iget-object v2, p0, Lcom/osp/app/signin/du;->h:Ljava/lang/String;

    invoke-static {v0, p1, v1, v2, p0}, Lcom/msc/c/g;->h(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/msc/b/h;)J

    move-result-wide v0

    iput-wide v0, p0, Lcom/osp/app/signin/du;->l:J

    .line 989
    iget-wide v0, p0, Lcom/osp/app/signin/du;->l:J

    invoke-virtual {p0, v0, v1}, Lcom/osp/app/signin/du;->a(J)V

    .line 990
    iget-wide v0, p0, Lcom/osp/app/signin/du;->l:J

    const-string v2, "from_xml"

    invoke-virtual {p0, v0, v1, v2}, Lcom/osp/app/signin/du;->a(JLjava/lang/String;)V

    .line 991
    iget-wide v0, p0, Lcom/osp/app/signin/du;->l:J

    sget-object v2, Lcom/msc/b/g;->c:Lcom/msc/b/g;

    invoke-static {v0, v1, v2}, Lcom/msc/c/g;->a(JLcom/msc/b/g;)V

    .line 992
    return-void
.end method


# virtual methods
.method protected final varargs a()Ljava/lang/Boolean;
    .locals 3

    .prologue
    .line 916
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v0, "NEMO first Duplicated User ID : "

    invoke-direct {v1, v0}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/osp/app/signin/du;->c:Lcom/osp/app/signin/CheckSecurityInfoActivity;

    invoke-static {v0}, Lcom/osp/app/signin/CheckSecurityInfoActivity;->c(Lcom/osp/app/signin/CheckSecurityInfoActivity;)Ljava/util/ArrayList;

    move-result-object v0

    iget v2, p0, Lcom/osp/app/signin/du;->d:I

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/osp/app/signin/dw;

    invoke-virtual {v0}, Lcom/osp/app/signin/dw;->a()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;)V

    .line 917
    iget-object v0, p0, Lcom/osp/app/signin/du;->c:Lcom/osp/app/signin/CheckSecurityInfoActivity;

    invoke-static {v0}, Lcom/osp/app/signin/CheckSecurityInfoActivity;->c(Lcom/osp/app/signin/CheckSecurityInfoActivity;)Ljava/util/ArrayList;

    move-result-object v0

    iget v1, p0, Lcom/osp/app/signin/du;->d:I

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/osp/app/signin/dw;

    invoke-virtual {v0}, Lcom/osp/app/signin/dw;->a()Ljava/lang/String;

    move-result-object v0

    .line 918
    invoke-direct {p0, v0}, Lcom/osp/app/signin/du;->b(Ljava/lang/String;)V

    .line 920
    const/4 v0, 0x0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    return-object v0
.end method

.method public final declared-synchronized a(Lcom/msc/b/c;)V
    .locals 6

    .prologue
    .line 925
    monitor-enter p0

    :try_start_0
    invoke-super {p0, p1}, Lcom/msc/c/b;->a(Lcom/msc/b/c;)V

    .line 927
    invoke-virtual {p1}, Lcom/msc/b/c;->a()J

    move-result-wide v0

    .line 928
    invoke-virtual {p1}, Lcom/msc/b/c;->e()Ljava/lang/String;

    move-result-object v2

    .line 930
    iget-wide v4, p0, Lcom/osp/app/signin/du;->l:J
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    cmp-long v0, v0, v4

    if-nez v0, :cond_0

    .line 934
    :try_start_1
    invoke-static {}, Lcom/msc/c/h;->a()Lcom/msc/c/h;

    invoke-static {v2}, Lcom/msc/c/h;->N(Ljava/lang/String;)Z

    move-result v0

    .line 935
    iget-object v1, p0, Lcom/osp/app/signin/du;->e:Ljava/util/ArrayList;

    new-instance v2, Landroid/util/Pair;

    iget-object v3, p0, Lcom/osp/app/signin/du;->c:Lcom/osp/app/signin/CheckSecurityInfoActivity;

    invoke-static {v3}, Lcom/osp/app/signin/CheckSecurityInfoActivity;->c(Lcom/osp/app/signin/CheckSecurityInfoActivity;)Ljava/util/ArrayList;

    move-result-object v3

    iget v4, p0, Lcom/osp/app/signin/du;->d:I

    invoke-virtual {v3, v4}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v3

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    invoke-direct {v2, v3, v0}, Landroid/util/Pair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 936
    iget v0, p0, Lcom/osp/app/signin/du;->d:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/osp/app/signin/du;->d:I

    .line 938
    iget v0, p0, Lcom/osp/app/signin/du;->d:I

    iget v1, p0, Lcom/osp/app/signin/du;->f:I

    if-ge v0, v1, :cond_0

    .line 940
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v0, "NEMO Next Duplicated User ID : "

    invoke-direct {v1, v0}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/osp/app/signin/du;->c:Lcom/osp/app/signin/CheckSecurityInfoActivity;

    invoke-static {v0}, Lcom/osp/app/signin/CheckSecurityInfoActivity;->c(Lcom/osp/app/signin/CheckSecurityInfoActivity;)Ljava/util/ArrayList;

    move-result-object v0

    iget v2, p0, Lcom/osp/app/signin/du;->d:I

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/osp/app/signin/dw;

    invoke-virtual {v0}, Lcom/osp/app/signin/dw;->a()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;)V

    .line 941
    iget-object v0, p0, Lcom/osp/app/signin/du;->c:Lcom/osp/app/signin/CheckSecurityInfoActivity;

    invoke-static {v0}, Lcom/osp/app/signin/CheckSecurityInfoActivity;->c(Lcom/osp/app/signin/CheckSecurityInfoActivity;)Ljava/util/ArrayList;

    move-result-object v0

    iget v1, p0, Lcom/osp/app/signin/du;->d:I

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/osp/app/signin/dw;

    invoke-virtual {v0}, Lcom/osp/app/signin/dw;->a()Ljava/lang/String;

    move-result-object v0

    .line 942
    invoke-direct {p0, v0}, Lcom/osp/app/signin/du;->b(Ljava/lang/String;)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 951
    :cond_0
    :goto_0
    monitor-exit p0

    return-void

    .line 945
    :catch_0
    move-exception v0

    .line 947
    :try_start_2
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    .line 948
    new-instance v1, Lcom/msc/c/f;

    invoke-direct {v1, v0}, Lcom/msc/c/f;-><init>(Ljava/lang/Exception;)V

    iput-object v1, p0, Lcom/osp/app/signin/du;->b:Lcom/msc/c/f;
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0

    .line 925
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method protected final a(Ljava/lang/Boolean;)V
    .locals 7

    .prologue
    const/4 v6, 0x0

    const/4 v2, 0x0

    const/4 v5, 0x1

    .line 968
    invoke-super {p0, p1}, Lcom/msc/c/b;->a(Ljava/lang/Boolean;)V

    .line 970
    iget-object v0, p0, Lcom/osp/app/signin/du;->e:Ljava/util/ArrayList;

    if-eqz v0, :cond_6

    iget-object v0, p0, Lcom/osp/app/signin/du;->e:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    iget-object v1, p0, Lcom/osp/app/signin/du;->c:Lcom/osp/app/signin/CheckSecurityInfoActivity;

    invoke-static {v1}, Lcom/osp/app/signin/CheckSecurityInfoActivity;->c(Lcom/osp/app/signin/CheckSecurityInfoActivity;)Ljava/util/ArrayList;

    move-result-object v1

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    if-ne v0, v1, :cond_6

    .line 972
    const/4 v0, -0x1

    move v1, v2

    move v3, v2

    move v4, v0

    :goto_0
    iget v0, p0, Lcom/osp/app/signin/du;->f:I

    if-ge v1, v0, :cond_1

    iget-object v0, p0, Lcom/osp/app/signin/du;->e:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/util/Pair;

    iget-object v0, v0, Landroid/util/Pair;->second:Ljava/lang/Object;

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-ne v0, v5, :cond_0

    add-int/lit8 v3, v3, 0x1

    move v4, v1

    :cond_0
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    :cond_1
    if-nez v3, :cond_3

    invoke-static {}, Lcom/osp/app/util/ap;->a()Lcom/osp/app/util/ap;

    iget-object v0, p0, Lcom/osp/app/signin/du;->c:Lcom/osp/app/signin/CheckSecurityInfoActivity;

    iget-object v1, p0, Lcom/osp/app/signin/du;->c:Lcom/osp/app/signin/CheckSecurityInfoActivity;

    const v3, 0x7f09018b

    invoke-virtual {v1, v3}, Lcom/osp/app/signin/CheckSecurityInfoActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1, v2}, Lcom/osp/app/util/ap;->a(Landroid/content/Context;Ljava/lang/String;I)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    .line 978
    :cond_2
    :goto_1
    return-void

    .line 972
    :cond_3
    if-ne v3, v5, :cond_5

    iget-object v1, p0, Lcom/osp/app/signin/du;->c:Lcom/osp/app/signin/CheckSecurityInfoActivity;

    iget-object v0, p0, Lcom/osp/app/signin/du;->e:Ljava/util/ArrayList;

    invoke-virtual {v0, v4}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/util/Pair;

    iget-object v0, v0, Landroid/util/Pair;->first:Ljava/lang/Object;

    check-cast v0, Lcom/osp/app/signin/dw;

    invoke-static {v1, v0}, Lcom/osp/app/signin/CheckSecurityInfoActivity;->a(Lcom/osp/app/signin/CheckSecurityInfoActivity;Lcom/osp/app/signin/dw;)Lcom/osp/app/signin/dw;

    const-string v0, "S"

    iget-object v1, p0, Lcom/osp/app/signin/du;->c:Lcom/osp/app/signin/CheckSecurityInfoActivity;

    invoke-static {v1}, Lcom/osp/app/signin/CheckSecurityInfoActivity;->d(Lcom/osp/app/signin/CheckSecurityInfoActivity;)Lcom/osp/app/signin/dw;

    move-result-object v1

    invoke-virtual {v1}, Lcom/osp/app/signin/dw;->c()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_4

    iget-object v0, p0, Lcom/osp/app/signin/du;->c:Lcom/osp/app/signin/CheckSecurityInfoActivity;

    const/16 v1, 0x64

    invoke-virtual {v0, v1, v6}, Lcom/osp/app/signin/CheckSecurityInfoActivity;->a(ILandroid/content/DialogInterface$OnDismissListener;)V

    goto :goto_1

    :cond_4
    iget-object v0, p0, Lcom/osp/app/signin/du;->c:Lcom/osp/app/signin/CheckSecurityInfoActivity;

    invoke-static {v0}, Lcom/osp/app/signin/CheckSecurityInfoActivity;->e(Lcom/osp/app/signin/CheckSecurityInfoActivity;)V

    goto :goto_1

    :cond_5
    const/4 v0, 0x2

    if-lt v3, v0, :cond_2

    iget-object v0, p0, Lcom/osp/app/signin/du;->c:Lcom/osp/app/signin/CheckSecurityInfoActivity;

    const/16 v1, 0x65

    invoke-virtual {v0, v1, v6}, Lcom/osp/app/signin/CheckSecurityInfoActivity;->a(ILandroid/content/DialogInterface$OnDismissListener;)V

    iget-object v0, p0, Lcom/osp/app/signin/du;->c:Lcom/osp/app/signin/CheckSecurityInfoActivity;

    invoke-virtual {v0, v5}, Lcom/osp/app/signin/CheckSecurityInfoActivity;->b(I)V

    goto :goto_1

    .line 975
    :cond_6
    iget-object v0, p0, Lcom/osp/app/signin/du;->c:Lcom/osp/app/signin/CheckSecurityInfoActivity;

    const/16 v1, 0xe

    invoke-virtual {v0, v1}, Lcom/osp/app/signin/CheckSecurityInfoActivity;->b(I)V

    .line 976
    invoke-virtual {p0, v5}, Lcom/osp/app/signin/du;->a(Z)V

    goto :goto_1
.end method

.method public final declared-synchronized b(Lcom/msc/b/c;)V
    .locals 4

    .prologue
    .line 955
    monitor-enter p0

    :try_start_0
    invoke-super {p0, p1}, Lcom/msc/c/b;->b(Lcom/msc/b/c;)V

    .line 957
    invoke-virtual {p1}, Lcom/msc/b/c;->a()J

    move-result-wide v0

    .line 960
    iget-wide v2, p0, Lcom/osp/app/signin/du;->l:J

    cmp-long v0, v0, v2

    if-nez v0, :cond_0

    .line 962
    new-instance v0, Lcom/msc/c/f;

    invoke-direct {v0}, Lcom/msc/c/f;-><init>()V

    iput-object v0, p0, Lcom/osp/app/signin/du;->b:Lcom/msc/c/f;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 964
    :cond_0
    monitor-exit p0

    return-void

    .line 955
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method protected final d()V
    .locals 2

    .prologue
    .line 908
    invoke-super {p0}, Lcom/msc/c/b;->d()V

    .line 909
    iget-object v0, p0, Lcom/osp/app/signin/du;->c:Lcom/osp/app/signin/CheckSecurityInfoActivity;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/osp/app/signin/CheckSecurityInfoActivity;->b(I)V

    .line 910
    iget-object v0, p0, Lcom/osp/app/signin/du;->c:Lcom/osp/app/signin/CheckSecurityInfoActivity;

    invoke-virtual {v0}, Lcom/osp/app/signin/CheckSecurityInfoActivity;->finish()V

    .line 911
    return-void
.end method

.method protected final synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 867
    invoke-virtual {p0}, Lcom/osp/app/signin/du;->a()Ljava/lang/Boolean;

    move-result-object v0

    return-object v0
.end method

.method protected final synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 867
    check-cast p1, Ljava/lang/Boolean;

    invoke-virtual {p0, p1}, Lcom/osp/app/signin/du;->a(Ljava/lang/Boolean;)V

    return-void
.end method
