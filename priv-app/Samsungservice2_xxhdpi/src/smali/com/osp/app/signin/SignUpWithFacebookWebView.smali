.class public Lcom/osp/app/signin/SignUpWithFacebookWebView;
.super Lcom/osp/app/util/BaseActivity;
.source "SignUpWithFacebookWebView.java"


# instance fields
.field private final a:Ljava/lang/String;

.field private final b:Ljava/lang/String;

.field private final c:Ljava/lang/String;

.field private d:Landroid/webkit/WebView;

.field private e:Landroid/app/ProgressDialog;


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 42
    invoke-direct {p0}, Lcom/osp/app/util/BaseActivity;-><init>()V

    .line 89
    const-string v0, "facebook.com/dialog/oauth?"

    iput-object v0, p0, Lcom/osp/app/signin/SignUpWithFacebookWebView;->a:Ljava/lang/String;

    .line 93
    const-string v0, "facebook.com/login.php?"

    iput-object v0, p0, Lcom/osp/app/signin/SignUpWithFacebookWebView;->b:Ljava/lang/String;

    .line 97
    const-string v0, "/mobile/account/facebookUserInfoResult.do?"

    iput-object v0, p0, Lcom/osp/app/signin/SignUpWithFacebookWebView;->c:Ljava/lang/String;

    .line 102
    iput-object v1, p0, Lcom/osp/app/signin/SignUpWithFacebookWebView;->d:Landroid/webkit/WebView;

    .line 107
    iput-object v1, p0, Lcom/osp/app/signin/SignUpWithFacebookWebView;->e:Landroid/app/ProgressDialog;

    .line 353
    return-void
.end method

.method static synthetic a(Lcom/osp/app/signin/SignUpWithFacebookWebView;)Landroid/app/ProgressDialog;
    .locals 1

    .prologue
    .line 42
    iget-object v0, p0, Lcom/osp/app/signin/SignUpWithFacebookWebView;->e:Landroid/app/ProgressDialog;

    return-object v0
.end method

.method static synthetic a(Lcom/osp/app/signin/SignUpWithFacebookWebView;Landroid/app/ProgressDialog;)Landroid/app/ProgressDialog;
    .locals 0

    .prologue
    .line 42
    iput-object p1, p0, Lcom/osp/app/signin/SignUpWithFacebookWebView;->e:Landroid/app/ProgressDialog;

    return-object p1
.end method

.method static synthetic b(Lcom/osp/app/signin/SignUpWithFacebookWebView;)Z
    .locals 1

    .prologue
    .line 42
    invoke-virtual {p0}, Lcom/osp/app/signin/SignUpWithFacebookWebView;->h()Z

    move-result v0

    return v0
.end method

.method static synthetic c(Lcom/osp/app/signin/SignUpWithFacebookWebView;)Landroid/webkit/WebView;
    .locals 1

    .prologue
    .line 42
    iget-object v0, p0, Lcom/osp/app/signin/SignUpWithFacebookWebView;->d:Landroid/webkit/WebView;

    return-object v0
.end method


# virtual methods
.method protected final a()V
    .locals 0

    .prologue
    .line 403
    return-void
.end method

.method protected final b()V
    .locals 0

    .prologue
    .line 407
    return-void
.end method

.method public onBackPressed()V
    .locals 1

    .prologue
    .line 339
    iget-object v0, p0, Lcom/osp/app/signin/SignUpWithFacebookWebView;->d:Landroid/webkit/WebView;

    invoke-virtual {v0}, Landroid/webkit/WebView;->canGoBack()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 341
    iget-object v0, p0, Lcom/osp/app/signin/SignUpWithFacebookWebView;->d:Landroid/webkit/WebView;

    invoke-virtual {v0}, Landroid/webkit/WebView;->goBack()V

    .line 346
    :goto_0
    return-void

    .line 344
    :cond_0
    invoke-super {p0}, Lcom/osp/app/util/BaseActivity;->onBackPressed()V

    goto :goto_0
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 5

    .prologue
    const/4 v4, 0x1

    const/16 v2, 0xe

    const/4 v3, 0x0

    .line 111
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    if-lt v0, v2, :cond_4

    .line 113
    const/4 v0, 0x5

    invoke-virtual {p0, v0}, Lcom/osp/app/signin/SignUpWithFacebookWebView;->d(I)V

    .line 119
    :goto_0
    invoke-super {p0, p1}, Lcom/osp/app/util/BaseActivity;->onCreate(Landroid/os/Bundle;)V

    .line 122
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    if-ge v0, v2, :cond_0

    .line 124
    invoke-virtual {p0, v4}, Lcom/osp/app/signin/SignUpWithFacebookWebView;->requestWindowFeature(I)Z

    .line 139
    :cond_0
    invoke-static {p0}, Lcom/msc/sa/c/d;->b(Landroid/app/Activity;)V

    .line 141
    const v0, 0x7f030032

    invoke-virtual {p0, v0}, Lcom/osp/app/signin/SignUpWithFacebookWebView;->setContentView(I)V

    .line 142
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v0, "SUWFWV"

    const-string v1, "onCreated"

    invoke-static {v0, v1}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 144
    const v0, 0x7f0c0028

    invoke-virtual {p0, v0}, Lcom/osp/app/signin/SignUpWithFacebookWebView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    sget v1, Landroid/os/Build$VERSION;->SDK_INT:I

    if-lt v1, v2, :cond_1

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setVisibility(I)V

    :cond_1
    invoke-static {p0}, Lcom/osp/app/util/r;->k(Landroid/content/Context;)Z

    move-result v1

    if-eqz v1, :cond_2

    invoke-static {p0}, Lcom/osp/app/util/an;->a(Lcom/osp/app/util/BaseActivity;)Lcom/osp/app/util/an;

    const v1, 0x7f0200ff

    invoke-static {v1}, Lcom/osp/app/util/an;->a(I)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setBackgroundResource(I)V

    :cond_2
    const v0, 0x7f0c0155

    invoke-virtual {p0, v0}, Lcom/osp/app/signin/SignUpWithFacebookWebView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    new-instance v1, Lcom/osp/app/signin/mk;

    invoke-direct {v1, p0}, Lcom/osp/app/signin/mk;-><init>(Lcom/osp/app/signin/SignUpWithFacebookWebView;)V

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    if-eqz v0, :cond_3

    invoke-static {p0}, Lcom/osp/app/util/an;->a(Lcom/osp/app/util/BaseActivity;)Lcom/osp/app/util/an;

    const v1, 0x7f020020

    invoke-static {v1}, Lcom/osp/app/util/an;->a(I)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setBackgroundResource(I)V

    invoke-virtual {p0}, Lcom/osp/app/signin/SignUpWithFacebookWebView;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f070001

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setTextColor(I)V

    .line 146
    :cond_3
    const v0, 0x7f0c0154

    invoke-virtual {p0, v0}, Lcom/osp/app/signin/SignUpWithFacebookWebView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/webkit/WebView;

    iput-object v0, p0, Lcom/osp/app/signin/SignUpWithFacebookWebView;->d:Landroid/webkit/WebView;

    .line 148
    iget-object v0, p0, Lcom/osp/app/signin/SignUpWithFacebookWebView;->d:Landroid/webkit/WebView;

    invoke-virtual {v0}, Landroid/webkit/WebView;->getSettings()Landroid/webkit/WebSettings;

    move-result-object v0

    invoke-virtual {v0, v4}, Landroid/webkit/WebSettings;->setJavaScriptEnabled(Z)V

    .line 149
    iget-object v0, p0, Lcom/osp/app/signin/SignUpWithFacebookWebView;->d:Landroid/webkit/WebView;

    invoke-virtual {v0}, Landroid/webkit/WebView;->getSettings()Landroid/webkit/WebSettings;

    move-result-object v0

    invoke-virtual {v0, v3}, Landroid/webkit/WebSettings;->setSavePassword(Z)V

    .line 150
    iget-object v0, p0, Lcom/osp/app/signin/SignUpWithFacebookWebView;->d:Landroid/webkit/WebView;

    new-instance v1, Lcom/osp/app/signin/ml;

    invoke-direct {v1, p0, v3}, Lcom/osp/app/signin/ml;-><init>(Lcom/osp/app/signin/SignUpWithFacebookWebView;B)V

    const-string v2, "JSONOUT"

    invoke-virtual {v0, v1, v2}, Landroid/webkit/WebView;->addJavascriptInterface(Ljava/lang/Object;Ljava/lang/String;)V

    .line 152
    iget-object v0, p0, Lcom/osp/app/signin/SignUpWithFacebookWebView;->d:Landroid/webkit/WebView;

    new-instance v1, Lcom/osp/app/signin/mm;

    invoke-direct {v1, p0, v3}, Lcom/osp/app/signin/mm;-><init>(Lcom/osp/app/signin/SignUpWithFacebookWebView;B)V

    invoke-virtual {v0, v1}, Landroid/webkit/WebView;->setWebViewClient(Landroid/webkit/WebViewClient;)V

    .line 159
    iget-object v0, p0, Lcom/osp/app/signin/SignUpWithFacebookWebView;->d:Landroid/webkit/WebView;

    invoke-static {p0}, Lcom/msc/c/n;->b(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/webkit/WebView;->loadUrl(Ljava/lang/String;)V

    .line 160
    return-void

    .line 116
    :cond_4
    const/4 v0, 0x3

    invoke-virtual {p0, v0}, Lcom/osp/app/signin/SignUpWithFacebookWebView;->d(I)V

    goto/16 :goto_0
.end method

.method protected onDestroy()V
    .locals 3

    .prologue
    const/4 v2, 0x1

    .line 445
    iget-object v0, p0, Lcom/osp/app/signin/SignUpWithFacebookWebView;->d:Landroid/webkit/WebView;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/osp/app/signin/SignUpWithFacebookWebView;->d:Landroid/webkit/WebView;

    invoke-virtual {v0, v2}, Landroid/webkit/WebView;->clearCache(Z)V

    iget-object v0, p0, Lcom/osp/app/signin/SignUpWithFacebookWebView;->d:Landroid/webkit/WebView;

    invoke-virtual {v0}, Landroid/webkit/WebView;->clearHistory()V

    iget-object v0, p0, Lcom/osp/app/signin/SignUpWithFacebookWebView;->d:Landroid/webkit/WebView;

    invoke-virtual {v0}, Landroid/webkit/WebView;->destroy()V

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/osp/app/signin/SignUpWithFacebookWebView;->d:Landroid/webkit/WebView;

    :cond_0
    invoke-static {p0}, Landroid/webkit/CookieSyncManager;->createInstance(Landroid/content/Context;)Landroid/webkit/CookieSyncManager;

    move-result-object v0

    invoke-static {}, Landroid/webkit/CookieManager;->getInstance()Landroid/webkit/CookieManager;

    move-result-object v1

    invoke-virtual {v1, v2}, Landroid/webkit/CookieManager;->setAcceptCookie(Z)V

    invoke-virtual {v1}, Landroid/webkit/CookieManager;->removeAllCookie()V

    invoke-virtual {v0}, Landroid/webkit/CookieSyncManager;->sync()V

    .line 446
    invoke-super {p0}, Lcom/osp/app/util/BaseActivity;->onDestroy()V

    .line 447
    return-void
.end method

.method public onOptionsItemSelected(Landroid/view/MenuItem;)Z
    .locals 2

    .prologue
    .line 164
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0xb

    if-lt v0, v1, :cond_0

    .line 166
    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    .line 174
    const/4 v0, 0x0

    .line 177
    :goto_0
    return v0

    .line 169
    :pswitch_0
    iget-object v0, p0, Lcom/osp/app/signin/SignUpWithFacebookWebView;->d:Landroid/webkit/WebView;

    invoke-virtual {v0}, Landroid/webkit/WebView;->clearHistory()V

    .line 170
    invoke-virtual {p0}, Lcom/osp/app/signin/SignUpWithFacebookWebView;->finish()V

    .line 177
    :cond_0
    const/4 v0, 0x1

    goto :goto_0

    .line 166
    nop

    :pswitch_data_0
    .packed-switch 0x102002c
        :pswitch_0
    .end packed-switch
.end method
