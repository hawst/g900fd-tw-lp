.class final Lcom/osp/app/signin/qc;
.super Landroid/webkit/WebViewClient;
.source "WeiboInfoWebView.java"


# instance fields
.field final synthetic a:Lcom/osp/app/signin/WeiboInfoWebView;


# direct methods
.method public constructor <init>(Lcom/osp/app/signin/WeiboInfoWebView;)V
    .locals 0

    .prologue
    .line 573
    iput-object p1, p0, Lcom/osp/app/signin/qc;->a:Lcom/osp/app/signin/WeiboInfoWebView;

    invoke-direct {p0}, Landroid/webkit/WebViewClient;-><init>()V

    .line 575
    return-void
.end method


# virtual methods
.method public final onLoadResource(Landroid/webkit/WebView;Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 713
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v0, "WIWV"

    const-string v1, "onLoadResource"

    invoke-static {v0, v1}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 714
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "WeiboWebView::onLoadResource URL = "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;)V

    .line 716
    invoke-super {p0, p1, p2}, Landroid/webkit/WebViewClient;->onLoadResource(Landroid/webkit/WebView;Ljava/lang/String;)V

    .line 717
    return-void
.end method

.method public final onPageFinished(Landroid/webkit/WebView;Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 690
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v0, "WIWV"

    const-string v1, "onPageFinished"

    invoke-static {v0, v1}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 691
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "WeiboWebView::onPageFinished URL = "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;)V

    .line 695
    :try_start_0
    iget-object v0, p0, Lcom/osp/app/signin/qc;->a:Lcom/osp/app/signin/WeiboInfoWebView;

    invoke-virtual {v0}, Lcom/osp/app/signin/WeiboInfoWebView;->isFinishing()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/osp/app/signin/qc;->a:Lcom/osp/app/signin/WeiboInfoWebView;

    invoke-static {v0}, Lcom/osp/app/signin/WeiboInfoWebView;->f(Lcom/osp/app/signin/WeiboInfoWebView;)Landroid/app/ProgressDialog;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/osp/app/signin/qc;->a:Lcom/osp/app/signin/WeiboInfoWebView;

    invoke-static {v0}, Lcom/osp/app/signin/WeiboInfoWebView;->f(Lcom/osp/app/signin/WeiboInfoWebView;)Landroid/app/ProgressDialog;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/ProgressDialog;->isShowing()Z

    move-result v0

    if-eqz v0, :cond_0

    if-eqz p2, :cond_0

    const-string v0, "https://api.weibo.com/2/proxy/account/username.json"

    invoke-virtual {p2, v0}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 697
    iget-object v0, p0, Lcom/osp/app/signin/qc;->a:Lcom/osp/app/signin/WeiboInfoWebView;

    invoke-static {v0}, Lcom/osp/app/signin/WeiboInfoWebView;->f(Lcom/osp/app/signin/WeiboInfoWebView;)Landroid/app/ProgressDialog;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/ProgressDialog;->dismiss()V

    .line 698
    iget-object v0, p0, Lcom/osp/app/signin/qc;->a:Lcom/osp/app/signin/WeiboInfoWebView;

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lcom/osp/app/signin/WeiboInfoWebView;->a(Lcom/osp/app/signin/WeiboInfoWebView;Landroid/app/ProgressDialog;)Landroid/app/ProgressDialog;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 704
    :cond_0
    :goto_0
    invoke-super {p0, p1, p2}, Landroid/webkit/WebViewClient;->onPageFinished(Landroid/webkit/WebView;Ljava/lang/String;)V

    .line 705
    invoke-static {p2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_1

    const-string v0, "https://api.weibo.com/2/proxy/account/username.json"

    invoke-virtual {p2, v0}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 707
    const-string v0, "javascript:window.JSONOUT.processJson(document.getElementsByTagName(\'body\')[0].innerHTML);"

    invoke-virtual {p1, v0}, Landroid/webkit/WebView;->loadUrl(Ljava/lang/String;)V

    .line 709
    :cond_1
    return-void

    .line 700
    :catch_0
    move-exception v0

    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_0
.end method

.method public final onPageStarted(Landroid/webkit/WebView;Ljava/lang/String;Landroid/graphics/Bitmap;)V
    .locals 3

    .prologue
    .line 618
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v0, "WIWV"

    const-string v1, "onPageStarted"

    invoke-static {v0, v1}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 619
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "WeiboWebView::onPageStarted URL = "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;)V

    .line 621
    iget-object v0, p0, Lcom/osp/app/signin/qc;->a:Lcom/osp/app/signin/WeiboInfoWebView;

    invoke-static {v0}, Lcom/osp/app/signin/WeiboInfoWebView;->f(Lcom/osp/app/signin/WeiboInfoWebView;)Landroid/app/ProgressDialog;

    move-result-object v0

    if-nez v0, :cond_0

    .line 623
    iget-object v0, p0, Lcom/osp/app/signin/qc;->a:Lcom/osp/app/signin/WeiboInfoWebView;

    new-instance v1, Landroid/app/ProgressDialog;

    iget-object v2, p0, Lcom/osp/app/signin/qc;->a:Lcom/osp/app/signin/WeiboInfoWebView;

    invoke-static {v2}, Lcom/osp/app/signin/WeiboInfoWebView;->b(Lcom/osp/app/signin/WeiboInfoWebView;)Landroid/content/Context;

    move-result-object v2

    invoke-direct {v1, v2}, Landroid/app/ProgressDialog;-><init>(Landroid/content/Context;)V

    invoke-static {v0, v1}, Lcom/osp/app/signin/WeiboInfoWebView;->a(Lcom/osp/app/signin/WeiboInfoWebView;Landroid/app/ProgressDialog;)Landroid/app/ProgressDialog;

    .line 624
    iget-object v0, p0, Lcom/osp/app/signin/qc;->a:Lcom/osp/app/signin/WeiboInfoWebView;

    invoke-static {v0}, Lcom/osp/app/signin/WeiboInfoWebView;->f(Lcom/osp/app/signin/WeiboInfoWebView;)Landroid/app/ProgressDialog;

    move-result-object v0

    iget-object v1, p0, Lcom/osp/app/signin/qc;->a:Lcom/osp/app/signin/WeiboInfoWebView;

    invoke-static {v1}, Lcom/osp/app/signin/WeiboInfoWebView;->b(Lcom/osp/app/signin/WeiboInfoWebView;)Landroid/content/Context;

    move-result-object v1

    const v2, 0x7f090036

    invoke-static {v1, v2}, Lcom/msc/sa/c/d;->a(Landroid/content/Context;I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/app/ProgressDialog;->setMessage(Ljava/lang/CharSequence;)V

    .line 625
    iget-object v0, p0, Lcom/osp/app/signin/qc;->a:Lcom/osp/app/signin/WeiboInfoWebView;

    invoke-static {v0}, Lcom/osp/app/signin/WeiboInfoWebView;->f(Lcom/osp/app/signin/WeiboInfoWebView;)Landroid/app/ProgressDialog;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/app/ProgressDialog;->setCancelable(Z)V

    .line 626
    iget-object v0, p0, Lcom/osp/app/signin/qc;->a:Lcom/osp/app/signin/WeiboInfoWebView;

    invoke-static {v0}, Lcom/osp/app/signin/WeiboInfoWebView;->f(Lcom/osp/app/signin/WeiboInfoWebView;)Landroid/app/ProgressDialog;

    move-result-object v0

    new-instance v1, Lcom/osp/app/signin/qd;

    invoke-direct {v1, p0}, Lcom/osp/app/signin/qd;-><init>(Lcom/osp/app/signin/qc;)V

    invoke-virtual {v0, v1}, Landroid/app/ProgressDialog;->setOnKeyListener(Landroid/content/DialogInterface$OnKeyListener;)V

    .line 663
    :try_start_0
    iget-object v0, p0, Lcom/osp/app/signin/qc;->a:Lcom/osp/app/signin/WeiboInfoWebView;

    invoke-virtual {v0}, Lcom/osp/app/signin/WeiboInfoWebView;->isFinishing()Z

    move-result v0

    if-nez v0, :cond_0

    .line 665
    iget-object v0, p0, Lcom/osp/app/signin/qc;->a:Lcom/osp/app/signin/WeiboInfoWebView;

    invoke-static {v0}, Lcom/osp/app/signin/WeiboInfoWebView;->f(Lcom/osp/app/signin/WeiboInfoWebView;)Landroid/app/ProgressDialog;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/ProgressDialog;->show()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 679
    :cond_0
    :goto_0
    const-string v0, "https://api.weibo.com/2/proxy/account/username.json"

    invoke-virtual {p2, v0}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 681
    const/16 v0, 0x8

    invoke-virtual {p1, v0}, Landroid/webkit/WebView;->setVisibility(I)V

    .line 682
    iget-object v0, p0, Lcom/osp/app/signin/qc;->a:Lcom/osp/app/signin/WeiboInfoWebView;

    const v1, 0x7f0c0187

    invoke-virtual {v0, v1}, Lcom/osp/app/signin/WeiboInfoWebView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    .line 683
    const/4 v1, -0x1

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setBackgroundColor(I)V

    .line 685
    :cond_1
    invoke-super {p0, p1, p2, p3}, Landroid/webkit/WebViewClient;->onPageStarted(Landroid/webkit/WebView;Ljava/lang/String;Landroid/graphics/Bitmap;)V

    .line 686
    return-void

    .line 667
    :catch_0
    move-exception v0

    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_0
.end method

.method public final onReceivedError(Landroid/webkit/WebView;ILjava/lang/String;Ljava/lang/String;)V
    .locals 3

    .prologue
    .line 721
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v0, "WIWV"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "onReceivedError errorCode : "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " ["

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "]"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 725
    :try_start_0
    iget-object v0, p0, Lcom/osp/app/signin/qc;->a:Lcom/osp/app/signin/WeiboInfoWebView;

    invoke-virtual {v0}, Lcom/osp/app/signin/WeiboInfoWebView;->isFinishing()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/osp/app/signin/qc;->a:Lcom/osp/app/signin/WeiboInfoWebView;

    invoke-static {v0}, Lcom/osp/app/signin/WeiboInfoWebView;->f(Lcom/osp/app/signin/WeiboInfoWebView;)Landroid/app/ProgressDialog;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/osp/app/signin/qc;->a:Lcom/osp/app/signin/WeiboInfoWebView;

    invoke-static {v0}, Lcom/osp/app/signin/WeiboInfoWebView;->f(Lcom/osp/app/signin/WeiboInfoWebView;)Landroid/app/ProgressDialog;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/ProgressDialog;->isShowing()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 727
    iget-object v0, p0, Lcom/osp/app/signin/qc;->a:Lcom/osp/app/signin/WeiboInfoWebView;

    invoke-static {v0}, Lcom/osp/app/signin/WeiboInfoWebView;->f(Lcom/osp/app/signin/WeiboInfoWebView;)Landroid/app/ProgressDialog;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/ProgressDialog;->dismiss()V

    .line 728
    iget-object v0, p0, Lcom/osp/app/signin/qc;->a:Lcom/osp/app/signin/WeiboInfoWebView;

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lcom/osp/app/signin/WeiboInfoWebView;->a(Lcom/osp/app/signin/WeiboInfoWebView;Landroid/app/ProgressDialog;)Landroid/app/ProgressDialog;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 735
    :cond_0
    :goto_0
    invoke-super {p0, p1, p2, p3, p4}, Landroid/webkit/WebViewClient;->onReceivedError(Landroid/webkit/WebView;ILjava/lang/String;Ljava/lang/String;)V

    .line 736
    return-void

    .line 730
    :catch_0
    move-exception v0

    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_0
.end method

.method public final shouldOverrideUrlLoading(Landroid/webkit/WebView;Ljava/lang/String;)Z
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 579
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "WeiboInfoWebView::shouldOverrideUrlLoading URL = "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;)V

    .line 581
    const-string v0, "?"

    invoke-virtual {p2, v0}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v0

    .line 582
    if-lez v0, :cond_2

    .line 584
    invoke-virtual {p2, v4, v0}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v1

    .line 585
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "resultURL = "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;)V

    .line 587
    iget-object v1, p0, Lcom/osp/app/signin/qc;->a:Lcom/osp/app/signin/WeiboInfoWebView;

    iget-object v2, p0, Lcom/osp/app/signin/qc;->a:Lcom/osp/app/signin/WeiboInfoWebView;

    add-int/lit8 v0, v0, 0x1

    invoke-virtual {p2, v0}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/osp/app/signin/WeiboInfoWebView;->b(Ljava/lang/String;)Lcom/osp/app/signin/pz;

    move-result-object v0

    invoke-static {v1, v0}, Lcom/osp/app/signin/WeiboInfoWebView;->a(Lcom/osp/app/signin/WeiboInfoWebView;Lcom/osp/app/signin/pz;)Lcom/osp/app/signin/pz;

    .line 589
    iget-object v0, p0, Lcom/osp/app/signin/qc;->a:Lcom/osp/app/signin/WeiboInfoWebView;

    invoke-static {v0}, Lcom/osp/app/signin/WeiboInfoWebView;->a(Lcom/osp/app/signin/WeiboInfoWebView;)Lcom/osp/app/signin/pz;

    move-result-object v0

    invoke-virtual {v0}, Lcom/osp/app/signin/pz;->i()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/osp/app/signin/qc;->a:Lcom/osp/app/signin/WeiboInfoWebView;

    invoke-static {v0}, Lcom/osp/app/signin/WeiboInfoWebView;->a(Lcom/osp/app/signin/WeiboInfoWebView;)Lcom/osp/app/signin/pz;

    move-result-object v0

    invoke-virtual {v0}, Lcom/osp/app/signin/pz;->i()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v0

    if-lez v0, :cond_0

    .line 591
    iget-object v0, p0, Lcom/osp/app/signin/qc;->a:Lcom/osp/app/signin/WeiboInfoWebView;

    invoke-static {v0}, Lcom/osp/app/signin/WeiboInfoWebView;->a(Lcom/osp/app/signin/WeiboInfoWebView;)Lcom/osp/app/signin/pz;

    move-result-object v0

    invoke-virtual {v0}, Lcom/osp/app/signin/pz;->i()Ljava/lang/String;

    move-result-object v0

    const-string v1, "21330"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 593
    iget-object v0, p0, Lcom/osp/app/signin/qc;->a:Lcom/osp/app/signin/WeiboInfoWebView;

    invoke-virtual {v0}, Lcom/osp/app/signin/WeiboInfoWebView;->getIntent()Landroid/content/Intent;

    move-result-object v0

    .line 594
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v1, "WIWV"

    const-string v2, "disagree weibo info"

    invoke-static {v1, v2}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 595
    iget-object v1, p0, Lcom/osp/app/signin/qc;->a:Lcom/osp/app/signin/WeiboInfoWebView;

    invoke-virtual {v1, v4, v0}, Lcom/osp/app/signin/WeiboInfoWebView;->a(ILandroid/content/Intent;)V

    .line 596
    iget-object v0, p0, Lcom/osp/app/signin/qc;->a:Lcom/osp/app/signin/WeiboInfoWebView;

    invoke-virtual {v0}, Lcom/osp/app/signin/WeiboInfoWebView;->finish()V

    .line 600
    :cond_0
    iget-object v0, p0, Lcom/osp/app/signin/qc;->a:Lcom/osp/app/signin/WeiboInfoWebView;

    invoke-static {v0}, Lcom/osp/app/signin/WeiboInfoWebView;->a(Lcom/osp/app/signin/WeiboInfoWebView;)Lcom/osp/app/signin/pz;

    move-result-object v0

    invoke-virtual {v0}, Lcom/osp/app/signin/pz;->a()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/osp/app/signin/qc;->a:Lcom/osp/app/signin/WeiboInfoWebView;

    invoke-static {v0}, Lcom/osp/app/signin/WeiboInfoWebView;->a(Lcom/osp/app/signin/WeiboInfoWebView;)Lcom/osp/app/signin/pz;

    move-result-object v0

    invoke-virtual {v0}, Lcom/osp/app/signin/pz;->a()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v0

    if-lez v0, :cond_1

    .line 602
    iget-object v0, p0, Lcom/osp/app/signin/qc;->a:Lcom/osp/app/signin/WeiboInfoWebView;

    invoke-static {}, Lcom/osp/app/signin/WeiboInfoWebView;->d()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/webkit/WebView;->loadUrl(Ljava/lang/String;)V

    .line 613
    :goto_0
    const/4 v0, 0x1

    return v0

    .line 605
    :cond_1
    iget-object v0, p0, Lcom/osp/app/signin/qc;->a:Lcom/osp/app/signin/WeiboInfoWebView;

    invoke-static {v0}, Lcom/osp/app/signin/WeiboInfoWebView;->d(Lcom/osp/app/signin/WeiboInfoWebView;)Landroid/webkit/WebView;

    move-result-object v0

    invoke-virtual {v0}, Landroid/webkit/WebView;->clearHistory()V

    .line 606
    invoke-virtual {p1, p2}, Landroid/webkit/WebView;->loadUrl(Ljava/lang/String;)V

    goto :goto_0

    .line 610
    :cond_2
    iget-object v0, p0, Lcom/osp/app/signin/qc;->a:Lcom/osp/app/signin/WeiboInfoWebView;

    invoke-static {v0}, Lcom/osp/app/signin/WeiboInfoWebView;->d(Lcom/osp/app/signin/WeiboInfoWebView;)Landroid/webkit/WebView;

    move-result-object v0

    invoke-virtual {v0}, Landroid/webkit/WebView;->clearHistory()V

    .line 611
    invoke-virtual {p1, p2}, Landroid/webkit/WebView;->loadUrl(Ljava/lang/String;)V

    goto :goto_0
.end method
