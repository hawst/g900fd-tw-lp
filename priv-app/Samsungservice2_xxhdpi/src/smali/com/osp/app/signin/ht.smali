.class final Lcom/osp/app/signin/ht;
.super Lcom/msc/c/b;
.source "ShowTncInfoView.java"


# instance fields
.field final synthetic c:Lcom/osp/app/signin/ShowTncInfoView;

.field private d:Ljava/lang/String;

.field private final e:Ljava/lang/String;

.field private final f:Ljava/lang/String;

.field private g:I

.field private h:J

.field private i:J

.field private j:J


# direct methods
.method public constructor <init>(Lcom/osp/app/signin/ShowTncInfoView;)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 465
    iput-object p1, p0, Lcom/osp/app/signin/ht;->c:Lcom/osp/app/signin/ShowTncInfoView;

    .line 466
    invoke-direct {p0, p1}, Lcom/msc/c/b;-><init>(Landroid/content/Context;)V

    .line 467
    iput v2, p0, Lcom/osp/app/signin/ht;->g:I

    .line 474
    invoke-static {}, Lcom/osp/common/util/i;->a()Lcom/osp/common/util/i;

    invoke-static {p1}, Lcom/osp/app/signin/ShowTncInfoView;->d(Lcom/osp/app/signin/ShowTncInfoView;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/osp/common/util/i;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/osp/app/signin/ht;->e:Ljava/lang/String;

    .line 499
    invoke-static {}, Lcom/osp/common/util/i;->a()Lcom/osp/common/util/i;

    invoke-static {p1}, Lcom/osp/common/util/i;->l(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x2

    invoke-virtual {v0, v2, v1}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/osp/app/signin/ht;->f:Ljava/lang/String;

    .line 502
    return-void
.end method

.method private c(Ljava/lang/String;Ljava/lang/String;)V
    .locals 6

    .prologue
    .line 695
    invoke-static {}, Lcom/msc/c/g;->a()Lcom/msc/c/g;

    .line 696
    iget-object v0, p0, Lcom/osp/app/signin/ht;->c:Lcom/osp/app/signin/ShowTncInfoView;

    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v0, "SRS"

    const-string v1, " AccountVerify go "

    invoke-static {v0, v1}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;)V

    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    const-string v0, "GB"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const-string p1, "global"

    const-string p2, "default"

    :cond_1
    const-string v0, "pp.txt"

    invoke-static {p1, p2, v0}, Lcom/msc/c/k;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-static {}, Lcom/msc/b/i;->a()Lcom/msc/b/i;

    move-result-object v0

    invoke-static {}, Lcom/osp/app/util/ah;->a()Lcom/osp/app/util/ah;

    move-result-object v2

    invoke-virtual {v2}, Lcom/osp/app/util/ah;->g()Z

    move-result v4

    invoke-static {}, Lcom/osp/app/signin/SamsungService;->j()Z

    move-result v2

    invoke-static {}, Lcom/osp/app/signin/SamsungService;->b()Z

    move-result v3

    move-object v5, p0

    invoke-virtual/range {v0 .. v5}, Lcom/msc/b/i;->a(Ljava/lang/String;ZZZLcom/msc/b/h;)Lcom/msc/b/d;

    move-result-object v0

    invoke-virtual {v0}, Lcom/msc/b/d;->a()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/osp/app/signin/ht;->j:J

    .line 697
    iget-wide v0, p0, Lcom/osp/app/signin/ht;->j:J

    invoke-virtual {p0, v0, v1}, Lcom/osp/app/signin/ht;->a(J)V

    .line 698
    iget-wide v0, p0, Lcom/osp/app/signin/ht;->j:J

    sget-object v2, Lcom/msc/b/g;->b:Lcom/msc/b/g;

    invoke-static {v0, v1, v2}, Lcom/msc/c/g;->a(JLcom/msc/b/g;)V

    .line 699
    return-void
.end method


# virtual methods
.method protected final varargs a()Ljava/lang/Boolean;
    .locals 6

    .prologue
    .line 518
    const-string v0, "pdu"

    iget-object v1, p0, Lcom/osp/app/signin/ht;->c:Lcom/osp/app/signin/ShowTncInfoView;

    invoke-static {v1}, Lcom/osp/app/signin/ShowTncInfoView;->a(Lcom/osp/app/signin/ShowTncInfoView;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 520
    invoke-static {}, Lcom/msc/c/g;->a()Lcom/msc/c/g;

    iget-object v0, p0, Lcom/osp/app/signin/ht;->c:Lcom/osp/app/signin/ShowTncInfoView;

    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v0, "SRS"

    const-string v1, " AccountVerify go "

    invoke-static {v0, v1}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;)V

    invoke-static {}, Lcom/msc/c/k;->c()Ljava/lang/String;

    move-result-object v1

    invoke-static {}, Lcom/msc/b/i;->a()Lcom/msc/b/i;

    move-result-object v0

    invoke-static {}, Lcom/osp/app/util/ah;->a()Lcom/osp/app/util/ah;

    move-result-object v2

    invoke-virtual {v2}, Lcom/osp/app/util/ah;->g()Z

    move-result v4

    invoke-static {}, Lcom/osp/app/signin/SamsungService;->j()Z

    move-result v2

    invoke-static {}, Lcom/osp/app/signin/SamsungService;->b()Z

    move-result v3

    move-object v5, p0

    invoke-virtual/range {v0 .. v5}, Lcom/msc/b/i;->a(Ljava/lang/String;ZZZLcom/msc/b/h;)Lcom/msc/b/d;

    move-result-object v0

    invoke-virtual {v0}, Lcom/msc/b/d;->a()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/osp/app/signin/ht;->i:J

    iget-wide v0, p0, Lcom/osp/app/signin/ht;->i:J

    invoke-virtual {p0, v0, v1}, Lcom/osp/app/signin/ht;->a(J)V

    iget-wide v0, p0, Lcom/osp/app/signin/ht;->i:J

    sget-object v2, Lcom/msc/b/g;->b:Lcom/msc/b/g;

    invoke-static {v0, v1, v2}, Lcom/msc/c/g;->a(JLcom/msc/b/g;)V

    .line 530
    :cond_0
    :goto_0
    const/4 v0, 0x0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    return-object v0

    .line 522
    :cond_1
    const-string v0, "pp"

    iget-object v1, p0, Lcom/osp/app/signin/ht;->c:Lcom/osp/app/signin/ShowTncInfoView;

    invoke-static {v1}, Lcom/osp/app/signin/ShowTncInfoView;->a(Lcom/osp/app/signin/ShowTncInfoView;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 524
    iget-object v0, p0, Lcom/osp/app/signin/ht;->e:Ljava/lang/String;

    iget-object v1, p0, Lcom/osp/app/signin/ht;->f:Ljava/lang/String;

    invoke-direct {p0, v0, v1}, Lcom/osp/app/signin/ht;->c(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public final declared-synchronized a(Lcom/msc/b/c;)V
    .locals 6

    .prologue
    .line 558
    monitor-enter p0

    :try_start_0
    invoke-super {p0, p1}, Lcom/msc/c/b;->a(Lcom/msc/b/c;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 560
    if-nez p1, :cond_1

    .line 596
    :cond_0
    :goto_0
    monitor-exit p0

    return-void

    .line 565
    :cond_1
    :try_start_1
    invoke-virtual {p1}, Lcom/msc/b/c;->a()J

    move-result-wide v0

    .line 566
    invoke-virtual {p1}, Lcom/msc/b/c;->e()Ljava/lang/String;

    move-result-object v2

    .line 568
    iget-wide v4, p0, Lcom/osp/app/signin/ht;->h:J

    cmp-long v3, v0, v4

    if-nez v3, :cond_2

    .line 570
    if-eqz v2, :cond_0

    .line 572
    iput-object v2, p0, Lcom/osp/app/signin/ht;->d:Ljava/lang/String;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 558
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 575
    :cond_2
    :try_start_2
    iget-wide v4, p0, Lcom/osp/app/signin/ht;->i:J

    cmp-long v3, v0, v4

    if-nez v3, :cond_3

    .line 577
    if-eqz v2, :cond_0

    .line 579
    iput-object v2, p0, Lcom/osp/app/signin/ht;->d:Ljava/lang/String;

    goto :goto_0

    .line 582
    :cond_3
    iget-wide v4, p0, Lcom/osp/app/signin/ht;->j:J

    cmp-long v0, v0, v4

    if-nez v0, :cond_0

    .line 584
    if-eqz v2, :cond_0

    .line 586
    iput-object v2, p0, Lcom/osp/app/signin/ht;->d:Ljava/lang/String;
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0
.end method

.method protected final a(Ljava/lang/Boolean;)V
    .locals 4

    .prologue
    const/4 v3, 0x1

    .line 535
    invoke-super {p0, p1}, Lcom/msc/c/b;->a(Ljava/lang/Boolean;)V

    .line 536
    iget-object v0, p0, Lcom/osp/app/signin/ht;->b:Lcom/msc/c/f;

    if-eqz v0, :cond_1

    .line 538
    invoke-virtual {p0, v3}, Lcom/osp/app/signin/ht;->a(Z)V

    .line 554
    :cond_0
    :goto_0
    return-void

    .line 544
    :cond_1
    iget-object v0, p0, Lcom/osp/app/signin/ht;->d:Ljava/lang/String;

    if-eqz v0, :cond_2

    const-string v0, ""

    iget-object v1, p0, Lcom/osp/app/signin/ht;->d:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 546
    :cond_2
    invoke-static {}, Lcom/osp/app/util/ap;->a()Lcom/osp/app/util/ap;

    iget-object v0, p0, Lcom/osp/app/signin/ht;->c:Lcom/osp/app/signin/ShowTncInfoView;

    iget-object v1, p0, Lcom/osp/app/signin/ht;->c:Lcom/osp/app/signin/ShowTncInfoView;

    const v2, 0x7f09003e

    invoke-virtual {v1, v2}, Lcom/osp/app/signin/ShowTncInfoView;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1, v3}, Lcom/osp/app/util/ap;->a(Landroid/content/Context;Ljava/lang/String;I)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    goto :goto_0

    .line 549
    :cond_3
    iget-object v0, p0, Lcom/osp/app/signin/ht;->c:Lcom/osp/app/signin/ShowTncInfoView;

    invoke-virtual {v0}, Lcom/osp/app/signin/ShowTncInfoView;->isFinishing()Z

    move-result v0

    if-nez v0, :cond_0

    .line 552
    iget-object v0, p0, Lcom/osp/app/signin/ht;->c:Lcom/osp/app/signin/ShowTncInfoView;

    invoke-static {v0}, Lcom/osp/app/signin/ShowTncInfoView;->b(Lcom/osp/app/signin/ShowTncInfoView;)Landroid/widget/TextView;

    move-result-object v0

    iget-object v1, p0, Lcom/osp/app/signin/ht;->d:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0
.end method

.method public final b(Lcom/msc/b/c;)V
    .locals 6

    .prologue
    .line 600
    invoke-super {p0, p1}, Lcom/msc/c/b;->b(Lcom/msc/b/c;)V

    .line 602
    if-nez p1, :cond_0

    .line 667
    :goto_0
    return-void

    .line 607
    :cond_0
    invoke-virtual {p1}, Lcom/msc/b/c;->a()J

    move-result-wide v0

    .line 608
    invoke-virtual {p1}, Lcom/msc/b/c;->e()Ljava/lang/String;

    move-result-object v2

    .line 609
    invoke-virtual {p1}, Lcom/msc/b/c;->f()Ljava/lang/Exception;

    move-result-object v3

    .line 642
    iget-wide v4, p0, Lcom/osp/app/signin/ht;->j:J

    cmp-long v0, v0, v4

    if-nez v0, :cond_2

    .line 644
    if-eqz v2, :cond_2

    .line 646
    iget v0, p0, Lcom/osp/app/signin/ht;->g:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/osp/app/signin/ht;->g:I

    .line 647
    iget v0, p0, Lcom/osp/app/signin/ht;->g:I

    const/4 v1, 0x1

    if-ne v0, v1, :cond_1

    .line 649
    iget-object v0, p0, Lcom/osp/app/signin/ht;->e:Ljava/lang/String;

    const-string v1, "default"

    invoke-direct {p0, v0, v1}, Lcom/osp/app/signin/ht;->c(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 651
    :cond_1
    iget v0, p0, Lcom/osp/app/signin/ht;->g:I

    const/4 v1, 0x2

    if-ne v0, v1, :cond_2

    .line 653
    const-string v0, "global"

    const-string v1, "default"

    invoke-direct {p0, v0, v1}, Lcom/osp/app/signin/ht;->c(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 659
    :cond_2
    if-eqz v3, :cond_3

    .line 661
    new-instance v0, Lcom/msc/c/f;

    invoke-direct {v0, v3}, Lcom/msc/c/f;-><init>(Ljava/lang/Exception;)V

    iput-object v0, p0, Lcom/osp/app/signin/ht;->b:Lcom/msc/c/f;

    goto :goto_0

    .line 664
    :cond_3
    new-instance v0, Lcom/msc/c/f;

    iget-object v1, p0, Lcom/osp/app/signin/ht;->c:Lcom/osp/app/signin/ShowTncInfoView;

    const v2, 0x7f09004a

    invoke-virtual {v1, v2}, Lcom/osp/app/signin/ShowTncInfoView;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/msc/c/f;-><init>(Ljava/lang/String;)V

    iput-object v0, p0, Lcom/osp/app/signin/ht;->b:Lcom/msc/c/f;

    goto :goto_0
.end method

.method protected final synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 419
    invoke-virtual {p0}, Lcom/osp/app/signin/ht;->a()Ljava/lang/Boolean;

    move-result-object v0

    return-object v0
.end method

.method protected final onCancelled()V
    .locals 2

    .prologue
    .line 506
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v0, "ShowTncInfoView"

    const-string v1, "GetTermsAndConditionsTask is canceled"

    invoke-static {v0, v1}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 507
    invoke-super {p0}, Lcom/msc/c/b;->onCancelled()V

    .line 508
    iget-object v0, p0, Lcom/osp/app/signin/ht;->c:Lcom/osp/app/signin/ShowTncInfoView;

    invoke-virtual {v0}, Lcom/osp/app/signin/ShowTncInfoView;->finish()V

    .line 509
    return-void
.end method

.method protected final synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 419
    check-cast p1, Ljava/lang/Boolean;

    invoke-virtual {p0, p1}, Lcom/osp/app/signin/ht;->a(Ljava/lang/Boolean;)V

    return-void
.end method
