.class final Lcom/osp/app/signin/cq;
.super Lcom/msc/c/b;
.source "BackgroundModeService.java"


# instance fields
.field final synthetic c:Lcom/osp/app/signin/BackgroundModeService;

.field private final d:Lcom/osp/app/util/d;

.field private e:Ljava/lang/String;

.field private f:J

.field private g:J

.field private h:J

.field private i:J

.field private j:J

.field private k:J

.field private l:Z

.field private m:Z

.field private n:Z

.field private o:J

.field private p:J

.field private q:Ljava/lang/String;

.field private r:Z

.field private s:Z


# direct methods
.method public constructor <init>(Lcom/osp/app/signin/BackgroundModeService;Lcom/osp/app/util/d;)V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 2766
    iput-object p1, p0, Lcom/osp/app/signin/cq;->c:Lcom/osp/app/signin/BackgroundModeService;

    .line 2767
    invoke-direct {p0, p1}, Lcom/msc/c/b;-><init>(Landroid/content/Context;)V

    .line 2763
    iput-boolean v0, p0, Lcom/osp/app/signin/cq;->r:Z

    .line 2764
    iput-boolean v0, p0, Lcom/osp/app/signin/cq;->s:Z

    .line 2768
    invoke-virtual {p0}, Lcom/osp/app/signin/cq;->c()V

    .line 2769
    iput-object p2, p0, Lcom/osp/app/signin/cq;->d:Lcom/osp/app/util/d;

    .line 2772
    return-void
.end method

.method private b(Ljava/lang/String;)V
    .locals 3

    .prologue
    .line 3510
    invoke-static {}, Lcom/msc/c/g;->a()Lcom/msc/c/g;

    .line 3511
    iget-object v0, p0, Lcom/osp/app/signin/cq;->c:Lcom/osp/app/signin/BackgroundModeService;

    iget-object v1, p0, Lcom/osp/app/signin/cq;->c:Lcom/osp/app/signin/BackgroundModeService;

    invoke-static {v1}, Lcom/msc/c/e;->h(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1, p1, p0}, Lcom/msc/c/g;->d(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Lcom/msc/b/h;)J

    move-result-wide v0

    iput-wide v0, p0, Lcom/osp/app/signin/cq;->h:J

    .line 3512
    iget-wide v0, p0, Lcom/osp/app/signin/cq;->h:J

    invoke-virtual {p0, v0, v1}, Lcom/osp/app/signin/cq;->a(J)V

    .line 3513
    iget-wide v0, p0, Lcom/osp/app/signin/cq;->h:J

    const-string v2, "from_xml"

    invoke-virtual {p0, v0, v1, v2}, Lcom/osp/app/signin/cq;->a(JLjava/lang/String;)V

    .line 3514
    iget-wide v0, p0, Lcom/osp/app/signin/cq;->h:J

    sget-object v2, Lcom/msc/b/g;->b:Lcom/msc/b/g;

    invoke-static {v0, v1, v2}, Lcom/msc/c/g;->a(JLcom/msc/b/g;)V

    .line 3515
    return-void
.end method

.method private h()V
    .locals 4

    .prologue
    .line 3388
    iget-boolean v0, p0, Lcom/osp/app/signin/cq;->s:Z

    if-nez v0, :cond_0

    iget-boolean v0, p0, Lcom/osp/app/signin/cq;->r:Z

    if-nez v0, :cond_2

    iget-object v0, p0, Lcom/osp/app/signin/cq;->d:Lcom/osp/app/util/d;

    iget-object v0, v0, Lcom/osp/app/util/d;->t:Lcom/msc/a/g;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/osp/app/signin/cq;->d:Lcom/osp/app/util/d;

    iget-object v0, v0, Lcom/osp/app/util/d;->t:Lcom/msc/a/g;

    invoke-virtual {v0}, Lcom/msc/a/g;->h()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 3390
    :cond_0
    iget-object v0, p0, Lcom/osp/app/signin/cq;->d:Lcom/osp/app/util/d;

    iget-boolean v0, v0, Lcom/osp/app/util/d;->k:Z

    if-eqz v0, :cond_4

    .line 3392
    iget-object v0, p0, Lcom/osp/app/signin/cq;->d:Lcom/osp/app/util/d;

    iget-object v0, v0, Lcom/osp/app/util/d;->n:Ljava/lang/String;

    if-eqz v0, :cond_3

    .line 3397
    iget-object v0, p0, Lcom/osp/app/signin/cq;->c:Lcom/osp/app/signin/BackgroundModeService;

    sget-object v1, Lcom/osp/app/util/af;->a:Lcom/osp/app/util/af;

    const/4 v2, 0x1

    invoke-static {v0, v1, v2}, Lcom/osp/app/util/ad;->a(Landroid/content/Context;Lcom/osp/app/util/af;Z)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 3399
    iget-object v0, p0, Lcom/osp/app/signin/cq;->c:Lcom/osp/app/signin/BackgroundModeService;

    invoke-static {v0}, Lcom/osp/app/signin/BackgroundModeService;->c(Lcom/osp/app/signin/BackgroundModeService;)V

    .line 3402
    :cond_1
    new-instance v0, Landroid/content/Intent;

    const-string v1, "com.msc.action.samsungaccount.response.BackGroundSignin"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 3403
    const-string v1, "bg_result"

    const/16 v2, 0xd

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 3406
    const-string v1, "request_code"

    iget-object v2, p0, Lcom/osp/app/signin/cq;->d:Lcom/osp/app/util/d;

    iget v2, v2, Lcom/osp/app/util/d;->q:I

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 3408
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v2, p0, Lcom/osp/app/signin/cq;->d:Lcom/osp/app/util/d;

    iget-object v2, v2, Lcom/osp/app/util/d;->n:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ":"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setData(Landroid/net/Uri;)Landroid/content/Intent;

    .line 3409
    iget-object v1, p0, Lcom/osp/app/signin/cq;->d:Lcom/osp/app/util/d;

    iget-object v1, v1, Lcom/osp/app/util/d;->n:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setPackage(Ljava/lang/String;)Landroid/content/Intent;

    .line 3412
    const-string v1, "client_id"

    iget-object v2, p0, Lcom/osp/app/signin/cq;->d:Lcom/osp/app/util/d;

    iget-object v2, v2, Lcom/osp/app/util/d;->f:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 3413
    iget-object v1, p0, Lcom/osp/app/signin/cq;->c:Lcom/osp/app/signin/BackgroundModeService;

    iget-object v2, p0, Lcom/osp/app/signin/cq;->d:Lcom/osp/app/util/d;

    iget-wide v2, v2, Lcom/osp/app/util/d;->r:J

    invoke-virtual {v1, v0, v2, v3}, Lcom/osp/app/signin/BackgroundModeService;->a(Landroid/content/Intent;J)V

    .line 3414
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "sendBroadcast to "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v1, p0, Lcom/osp/app/signin/cq;->d:Lcom/osp/app/util/d;

    iget-object v1, v1, Lcom/osp/app/util/d;->n:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;)V

    .line 3424
    :goto_0
    iget-object v0, p0, Lcom/osp/app/signin/cq;->c:Lcom/osp/app/signin/BackgroundModeService;

    iget-object v1, p0, Lcom/osp/app/signin/cq;->d:Lcom/osp/app/util/d;

    iget v1, v1, Lcom/osp/app/util/d;->s:I

    invoke-virtual {v0, v1}, Lcom/osp/app/signin/BackgroundModeService;->stopSelf(I)V

    .line 3425
    invoke-static {}, Lcom/osp/app/signin/BackgroundModeService;->a()Z

    .line 3428
    :cond_2
    return-void

    .line 3417
    :cond_3
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v0, "BMS"

    const-string v1, "cancel broadcast - no package name"

    invoke-static {v0, v1}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 3422
    :cond_4
    iget-object v0, p0, Lcom/osp/app/signin/cq;->c:Lcom/osp/app/signin/BackgroundModeService;

    iget-object v1, p0, Lcom/osp/app/signin/cq;->c:Lcom/osp/app/signin/BackgroundModeService;

    invoke-static {v1}, Lcom/osp/app/util/ad;->g(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/osp/app/signin/cq;->d:Lcom/osp/app/util/d;

    invoke-static {v0, v1, v2}, Lcom/osp/app/signin/BackgroundModeService;->a(Lcom/osp/app/signin/BackgroundModeService;Ljava/lang/String;Lcom/osp/app/util/d;)V

    goto :goto_0
.end method

.method private i()V
    .locals 3

    .prologue
    .line 3468
    iget-object v0, p0, Lcom/osp/app/signin/cq;->c:Lcom/osp/app/signin/BackgroundModeService;

    invoke-static {v0}, Lcom/osp/app/util/ad;->p(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/osp/app/signin/cq;->q:Ljava/lang/String;

    .line 3470
    new-instance v0, Lcom/msc/a/f;

    invoke-direct {v0}, Lcom/msc/a/f;-><init>()V

    .line 3471
    iget-object v1, p0, Lcom/osp/app/signin/cq;->c:Lcom/osp/app/signin/BackgroundModeService;

    invoke-static {v1}, Lcom/osp/app/util/ad;->h(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/msc/a/f;->d(Ljava/lang/String;)V

    .line 3472
    invoke-virtual {v0}, Lcom/msc/a/f;->e()V

    .line 3473
    iget-object v1, p0, Lcom/osp/app/signin/cq;->q:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/msc/a/f;->h(Ljava/lang/String;)V

    .line 3474
    iget-object v1, p0, Lcom/osp/app/signin/cq;->d:Lcom/osp/app/util/d;

    iget-object v1, v1, Lcom/osp/app/util/d;->f:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/msc/a/f;->f(Ljava/lang/String;)V

    .line 3475
    iget-object v1, p0, Lcom/osp/app/signin/cq;->d:Lcom/osp/app/util/d;

    iget-object v1, v1, Lcom/osp/app/util/d;->n:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/msc/a/f;->g(Ljava/lang/String;)V

    .line 3476
    invoke-static {}, Lcom/osp/device/b;->b()Lcom/osp/device/b;

    move-result-object v1

    invoke-virtual {v1}, Lcom/osp/device/b;->a()Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/osp/app/signin/cq;->c:Lcom/osp/app/signin/BackgroundModeService;

    invoke-static {v1}, Lcom/osp/app/util/ad;->h(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Landroid/telephony/PhoneNumberUtils;->isGlobalPhoneNumber(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 3479
    iget-object v1, p0, Lcom/osp/app/signin/cq;->c:Lcom/osp/app/signin/BackgroundModeService;

    invoke-static {v1}, Lcom/msc/c/e;->h(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v1

    .line 3480
    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 3482
    const-string v2, "Y"

    invoke-virtual {v0, v2}, Lcom/msc/a/f;->b(Ljava/lang/String;)V

    .line 3483
    invoke-virtual {v0, v1}, Lcom/msc/a/f;->c(Ljava/lang/String;)V

    .line 3486
    :cond_0
    invoke-static {}, Lcom/msc/c/g;->a()Lcom/msc/c/g;

    iget-object v1, p0, Lcom/osp/app/signin/cq;->c:Lcom/osp/app/signin/BackgroundModeService;

    invoke-static {v1, v0, p0}, Lcom/msc/c/g;->a(Landroid/content/Context;Lcom/msc/a/f;Lcom/msc/b/h;)J

    move-result-wide v0

    iput-wide v0, p0, Lcom/osp/app/signin/cq;->f:J

    iget-wide v0, p0, Lcom/osp/app/signin/cq;->f:J

    invoke-virtual {p0, v0, v1}, Lcom/osp/app/signin/cq;->a(J)V

    iget-wide v0, p0, Lcom/osp/app/signin/cq;->f:J

    const-string v2, "from_xml"

    invoke-virtual {p0, v0, v1, v2}, Lcom/osp/app/signin/cq;->a(JLjava/lang/String;)V

    iget-wide v0, p0, Lcom/osp/app/signin/cq;->f:J

    sget-object v2, Lcom/msc/b/g;->c:Lcom/msc/b/g;

    invoke-static {v0, v1, v2}, Lcom/msc/c/g;->a(JLcom/msc/b/g;)V

    .line 3487
    return-void
.end method

.method private j()V
    .locals 4

    .prologue
    .line 3532
    invoke-static {}, Lcom/msc/c/g;->a()Lcom/msc/c/g;

    .line 3533
    iget-object v0, p0, Lcom/osp/app/signin/cq;->c:Lcom/osp/app/signin/BackgroundModeService;

    const-string v1, "j5p7ll8g33"

    iget-object v2, p0, Lcom/osp/app/signin/cq;->c:Lcom/osp/app/signin/BackgroundModeService;

    invoke-static {v2}, Lcom/msc/c/e;->g(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x0

    invoke-static {v0, v1, v2, v3, p0}, Lcom/msc/c/g;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/msc/b/h;)J

    move-result-wide v0

    iput-wide v0, p0, Lcom/osp/app/signin/cq;->j:J

    .line 3535
    iget-wide v0, p0, Lcom/osp/app/signin/cq;->j:J

    invoke-virtual {p0, v0, v1}, Lcom/osp/app/signin/cq;->a(J)V

    .line 3536
    iget-wide v0, p0, Lcom/osp/app/signin/cq;->j:J

    const-string v2, "from_json"

    invoke-virtual {p0, v0, v1, v2}, Lcom/osp/app/signin/cq;->a(JLjava/lang/String;)V

    .line 3537
    iget-wide v0, p0, Lcom/osp/app/signin/cq;->j:J

    sget-object v2, Lcom/msc/b/g;->b:Lcom/msc/b/g;

    invoke-static {v0, v1, v2}, Lcom/msc/c/g;->a(JLcom/msc/b/g;)V

    .line 3538
    return-void
.end method

.method private k()V
    .locals 8

    .prologue
    const/4 v5, 0x0

    .line 3564
    invoke-static {}, Lcom/msc/c/g;->a()Lcom/msc/c/g;

    .line 3565
    iget-object v0, p0, Lcom/osp/app/signin/cq;->c:Lcom/osp/app/signin/BackgroundModeService;

    iget-object v1, p0, Lcom/osp/app/signin/cq;->d:Lcom/osp/app/util/d;

    iget-object v1, v1, Lcom/osp/app/util/d;->f:Ljava/lang/String;

    iget-object v2, p0, Lcom/osp/app/signin/cq;->c:Lcom/osp/app/signin/BackgroundModeService;

    invoke-static {v2}, Lcom/msc/c/e;->g(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v2

    iget-object v3, p0, Lcom/osp/app/signin/cq;->d:Lcom/osp/app/util/d;

    iget-object v3, v3, Lcom/osp/app/util/d;->c:Ljava/lang/String;

    iget-object v4, p0, Lcom/osp/app/signin/cq;->d:Lcom/osp/app/util/d;

    iget-object v4, v4, Lcom/osp/app/util/d;->n:Ljava/lang/String;

    move-object v6, v5

    move-object v7, p0

    invoke-static/range {v0 .. v7}, Lcom/msc/c/g;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/msc/b/h;)J

    move-result-wide v0

    iput-wide v0, p0, Lcom/osp/app/signin/cq;->o:J

    .line 3567
    iget-wide v0, p0, Lcom/osp/app/signin/cq;->o:J

    invoke-virtual {p0, v0, v1}, Lcom/osp/app/signin/cq;->a(J)V

    .line 3568
    iget-wide v0, p0, Lcom/osp/app/signin/cq;->o:J

    const-string v2, "from_json"

    invoke-virtual {p0, v0, v1, v2}, Lcom/osp/app/signin/cq;->a(JLjava/lang/String;)V

    .line 3569
    iget-wide v0, p0, Lcom/osp/app/signin/cq;->o:J

    sget-object v2, Lcom/msc/b/g;->b:Lcom/msc/b/g;

    invoke-static {v0, v1, v2}, Lcom/msc/c/g;->a(JLcom/msc/b/g;)V

    .line 3570
    return-void
.end method


# virtual methods
.method protected final varargs a()Ljava/lang/Boolean;
    .locals 3

    .prologue
    const/4 v2, 0x1

    .line 2777
    const-string v0, "REQUEST_ACCESSTOKEN"

    iget-object v1, p0, Lcom/osp/app/signin/cq;->d:Lcom/osp/app/util/d;

    iget-object v1, v1, Lcom/osp/app/util/d;->c:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 2780
    iget-object v0, p0, Lcom/osp/app/signin/cq;->c:Lcom/osp/app/signin/BackgroundModeService;

    invoke-static {v0}, Lcom/msc/openprovider/b;->d(Landroid/content/Context;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 2783
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v0, "BMS"

    const-string v1, "EV : OK"

    invoke-static {v0, v1}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 2784
    iput-boolean v2, p0, Lcom/osp/app/signin/cq;->r:Z

    .line 2786
    iget-object v0, p0, Lcom/osp/app/signin/cq;->c:Lcom/osp/app/signin/BackgroundModeService;

    invoke-static {v0}, Lcom/osp/app/signin/BackgroundModeService;->a(Lcom/osp/app/signin/BackgroundModeService;)V

    .line 2787
    invoke-direct {p0}, Lcom/osp/app/signin/cq;->k()V

    .line 2789
    iget-object v0, p0, Lcom/osp/app/signin/cq;->c:Lcom/osp/app/signin/BackgroundModeService;

    sget-object v1, Lcom/osp/app/util/af;->d:Lcom/osp/app/util/af;

    invoke-static {v0, v1, v2}, Lcom/osp/app/util/ad;->a(Landroid/content/Context;Lcom/osp/app/util/af;Z)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2791
    invoke-direct {p0}, Lcom/osp/app/signin/cq;->i()V

    .line 2817
    :cond_0
    :goto_0
    const/4 v0, 0x0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    return-object v0

    .line 2795
    :cond_1
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v0, "BMS"

    const-string v1, "EV : require"

    invoke-static {v0, v1}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 2797
    iget-object v0, p0, Lcom/osp/app/signin/cq;->c:Lcom/osp/app/signin/BackgroundModeService;

    sget-object v1, Lcom/osp/app/util/af;->b:Lcom/osp/app/util/af;

    invoke-static {v0, v1, v2}, Lcom/osp/app/util/ad;->a(Landroid/content/Context;Lcom/osp/app/util/af;Z)Z

    move-result v0

    if-nez v0, :cond_2

    .line 2800
    iput-boolean v2, p0, Lcom/osp/app/signin/cq;->s:Z

    .line 2808
    invoke-direct {p0}, Lcom/osp/app/signin/cq;->h()V

    goto :goto_0

    .line 2813
    :cond_2
    iget-object v0, p0, Lcom/osp/app/signin/cq;->c:Lcom/osp/app/signin/BackgroundModeService;

    invoke-static {v0}, Lcom/osp/app/signin/BackgroundModeService;->a(Lcom/osp/app/signin/BackgroundModeService;)V

    .line 2814
    invoke-direct {p0}, Lcom/osp/app/signin/cq;->i()V

    goto :goto_0
.end method

.method public final declared-synchronized a(Lcom/msc/b/c;)V
    .locals 6

    .prologue
    .line 2984
    monitor-enter p0

    :try_start_0
    invoke-super {p0, p1}, Lcom/msc/c/b;->a(Lcom/msc/b/c;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 2986
    if-nez p1, :cond_1

    .line 3155
    :cond_0
    :goto_0
    monitor-exit p0

    return-void

    .line 2991
    :cond_1
    :try_start_1
    invoke-virtual {p1}, Lcom/msc/b/c;->a()J

    move-result-wide v0

    .line 2992
    invoke-virtual {p1}, Lcom/msc/b/c;->e()Ljava/lang/String;

    move-result-object v2

    .line 2994
    iget-wide v4, p0, Lcom/osp/app/signin/cq;->f:J
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    cmp-long v3, v0, v4

    if-nez v3, :cond_7

    .line 2998
    :try_start_2
    iget-object v0, p0, Lcom/osp/app/signin/cq;->d:Lcom/osp/app/util/d;

    invoke-static {}, Lcom/msc/c/h;->a()Lcom/msc/c/h;

    iget-object v1, p0, Lcom/osp/app/signin/cq;->c:Lcom/osp/app/signin/BackgroundModeService;

    invoke-static {v1, v2}, Lcom/msc/c/h;->b(Landroid/content/Context;Ljava/lang/String;)Lcom/msc/a/g;

    move-result-object v1

    iput-object v1, v0, Lcom/osp/app/util/d;->t:Lcom/msc/a/g;

    .line 3000
    const-string v0, "REQUEST_ACCESSTOKEN"

    iget-object v1, p0, Lcom/osp/app/signin/cq;->d:Lcom/osp/app/util/d;

    iget-object v1, v1, Lcom/osp/app/util/d;->c:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 3002
    invoke-direct {p0}, Lcom/osp/app/signin/cq;->h()V

    .line 3003
    iget-object v0, p0, Lcom/osp/app/signin/cq;->d:Lcom/osp/app/util/d;

    iget-object v0, v0, Lcom/osp/app/util/d;->t:Lcom/msc/a/g;

    if-eqz v0, :cond_4

    iget-object v0, p0, Lcom/osp/app/signin/cq;->d:Lcom/osp/app/util/d;

    iget-object v0, v0, Lcom/osp/app/util/d;->t:Lcom/msc/a/g;

    invoke-virtual {v0}, Lcom/msc/a/g;->d()Z

    move-result v0

    if-nez v0, :cond_2

    iget-object v0, p0, Lcom/osp/app/signin/cq;->d:Lcom/osp/app/util/d;

    iget-object v0, v0, Lcom/osp/app/util/d;->t:Lcom/msc/a/g;

    invoke-virtual {v0}, Lcom/msc/a/g;->e()Z

    move-result v0

    if-eqz v0, :cond_4

    :cond_2
    iget-object v0, p0, Lcom/osp/app/signin/cq;->d:Lcom/osp/app/util/d;

    iget-object v0, v0, Lcom/osp/app/util/d;->n:Ljava/lang/String;

    if-eqz v0, :cond_6

    iget-object v0, p0, Lcom/osp/app/signin/cq;->c:Lcom/osp/app/signin/BackgroundModeService;

    iget-object v0, p0, Lcom/osp/app/signin/cq;->d:Lcom/osp/app/util/d;

    iget-object v0, v0, Lcom/osp/app/util/d;->f:Ljava/lang/String;

    invoke-static {v0}, Lcom/osp/app/signin/BackgroundModeService;->a(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/osp/app/signin/cq;->c:Lcom/osp/app/signin/BackgroundModeService;

    invoke-static {v0}, Lcom/osp/app/signin/BackgroundModeService;->d(Lcom/osp/app/signin/BackgroundModeService;)V

    :cond_3
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "sendBroadcast to "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v1, p0, Lcom/osp/app/signin/cq;->d:Lcom/osp/app/util/d;

    iget-object v1, v1, Lcom/osp/app/util/d;->n:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;)V

    :goto_1
    iget-object v0, p0, Lcom/osp/app/signin/cq;->c:Lcom/osp/app/signin/BackgroundModeService;

    iget-object v1, p0, Lcom/osp/app/signin/cq;->d:Lcom/osp/app/util/d;

    iget v1, v1, Lcom/osp/app/util/d;->s:I

    invoke-virtual {v0, v1}, Lcom/osp/app/signin/BackgroundModeService;->stopSelf(I)V

    invoke-static {}, Lcom/osp/app/signin/BackgroundModeService;->a()Z

    .line 3006
    :cond_4
    iget-object v0, p0, Lcom/osp/app/signin/cq;->d:Lcom/osp/app/util/d;

    iget-object v0, v0, Lcom/osp/app/util/d;->t:Lcom/msc/a/g;

    if-eqz v0, :cond_5

    iget-object v0, p0, Lcom/osp/app/signin/cq;->d:Lcom/osp/app/util/d;

    iget-object v0, v0, Lcom/osp/app/util/d;->t:Lcom/msc/a/g;

    invoke-virtual {v0}, Lcom/msc/a/g;->f()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_5

    .line 3008
    iget-object v0, p0, Lcom/osp/app/signin/cq;->c:Lcom/osp/app/signin/BackgroundModeService;

    iget-object v1, p0, Lcom/osp/app/signin/cq;->d:Lcom/osp/app/util/d;

    iget-object v1, v1, Lcom/osp/app/util/d;->t:Lcom/msc/a/g;

    invoke-virtual {v1}, Lcom/msc/a/g;->f()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/msc/c/e;->d(Landroid/content/Context;Ljava/lang/String;)V

    .line 3011
    :cond_5
    invoke-virtual {p0}, Lcom/osp/app/signin/cq;->isCancelled()Z

    move-result v0

    if-nez v0, :cond_0

    iget-boolean v0, p0, Lcom/osp/app/signin/cq;->r:Z

    if-nez v0, :cond_0

    .line 3013
    invoke-static {}, Lcom/msc/c/g;->a()Lcom/msc/c/g;

    iget-object v0, p0, Lcom/osp/app/signin/cq;->c:Lcom/osp/app/signin/BackgroundModeService;

    iget-object v1, p0, Lcom/osp/app/signin/cq;->d:Lcom/osp/app/util/d;

    iget-object v1, v1, Lcom/osp/app/util/d;->f:Ljava/lang/String;

    iget-object v2, p0, Lcom/osp/app/signin/cq;->c:Lcom/osp/app/signin/BackgroundModeService;

    invoke-static {v2}, Lcom/osp/app/util/ad;->h(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v1, v2, p0}, Lcom/msc/c/g;->c(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Lcom/msc/b/h;)J

    move-result-wide v0

    iput-wide v0, p0, Lcom/osp/app/signin/cq;->g:J

    iget-wide v0, p0, Lcom/osp/app/signin/cq;->g:J

    invoke-virtual {p0, v0, v1}, Lcom/osp/app/signin/cq;->a(J)V

    iget-wide v0, p0, Lcom/osp/app/signin/cq;->g:J

    const-string v2, "none"

    invoke-virtual {p0, v0, v1, v2}, Lcom/osp/app/signin/cq;->a(JLjava/lang/String;)V

    iget-wide v0, p0, Lcom/osp/app/signin/cq;->g:J

    sget-object v2, Lcom/msc/b/g;->c:Lcom/msc/b/g;

    invoke-static {v0, v1, v2}, Lcom/msc/c/g;->a(JLcom/msc/b/g;)V
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_0
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto/16 :goto_0

    .line 3015
    :catch_0
    move-exception v0

    :try_start_3
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    goto/16 :goto_0

    .line 2984
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 3003
    :cond_6
    :try_start_4
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v0, "BMS"

    const-string v1, "cancel broadcast - no package name"

    invoke-static {v0, v1}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_4
    .catch Ljava/lang/Exception; {:try_start_4 .. :try_end_4} :catch_0
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    goto :goto_1

    .line 3019
    :cond_7
    :try_start_5
    iget-wide v4, p0, Lcom/osp/app/signin/cq;->g:J
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    cmp-long v3, v0, v4

    if-nez v3, :cond_a

    .line 3023
    :try_start_6
    iget-object v0, p0, Lcom/osp/app/signin/cq;->d:Lcom/osp/app/util/d;

    invoke-static {}, Lcom/msc/c/h;->a()Lcom/msc/c/h;

    move-result-object v1

    iget-object v3, p0, Lcom/osp/app/signin/cq;->c:Lcom/osp/app/signin/BackgroundModeService;

    iget-object v4, p0, Lcom/osp/app/signin/cq;->d:Lcom/osp/app/util/d;

    iget-object v4, v4, Lcom/osp/app/util/d;->f:Ljava/lang/String;

    invoke-virtual {v1, v3, v4, v2}, Lcom/msc/c/h;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)Lcom/osp/app/signin/kd;

    move-result-object v1

    iput-object v1, v0, Lcom/osp/app/util/d;->i:Lcom/osp/app/signin/kd;
    :try_end_6
    .catch Ljava/lang/Exception; {:try_start_6 .. :try_end_6} :catch_1
    .catchall {:try_start_6 .. :try_end_6} :catchall_0

    .line 3029
    :goto_2
    :try_start_7
    invoke-virtual {p0}, Lcom/osp/app/signin/cq;->isCancelled()Z

    move-result v0

    if-nez v0, :cond_0

    .line 3031
    iget-object v0, p0, Lcom/osp/app/signin/cq;->d:Lcom/osp/app/util/d;

    iget-boolean v0, v0, Lcom/osp/app/util/d;->a:Z

    if-eqz v0, :cond_9

    .line 3034
    iget-object v0, p0, Lcom/osp/app/signin/cq;->c:Lcom/osp/app/signin/BackgroundModeService;

    invoke-static {v0}, Lcom/msc/c/e;->b(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v0

    .line 3035
    if-eqz v0, :cond_8

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v1

    if-lez v1, :cond_8

    .line 3037
    invoke-direct {p0, v0}, Lcom/osp/app/signin/cq;->b(Ljava/lang/String;)V

    goto/16 :goto_0

    .line 3025
    :catch_1
    move-exception v0

    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_2

    .line 3040
    :cond_8
    invoke-direct {p0}, Lcom/osp/app/signin/cq;->j()V

    goto/16 :goto_0

    .line 3044
    :cond_9
    invoke-direct {p0}, Lcom/osp/app/signin/cq;->k()V

    goto/16 :goto_0

    .line 3047
    :cond_a
    iget-wide v4, p0, Lcom/osp/app/signin/cq;->h:J
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_0

    cmp-long v3, v0, v4

    if-nez v3, :cond_b

    .line 3051
    :try_start_8
    iget-object v0, p0, Lcom/osp/app/signin/cq;->d:Lcom/osp/app/util/d;

    invoke-static {}, Lcom/msc/c/h;->a()Lcom/msc/c/h;

    invoke-static {v2}, Lcom/msc/c/h;->k(Ljava/lang/String;)Lcom/osp/app/signin/SignUpinfo;

    move-result-object v1

    iput-object v1, v0, Lcom/osp/app/util/d;->d:Lcom/osp/app/signin/SignUpinfo;

    .line 3052
    invoke-virtual {p0}, Lcom/osp/app/signin/cq;->isCancelled()Z

    move-result v0

    if-nez v0, :cond_0

    .line 3054
    invoke-direct {p0}, Lcom/osp/app/signin/cq;->k()V
    :try_end_8
    .catch Ljava/lang/Exception; {:try_start_8 .. :try_end_8} :catch_2
    .catchall {:try_start_8 .. :try_end_8} :catchall_0

    goto/16 :goto_0

    .line 3056
    :catch_2
    move-exception v0

    :try_start_9
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    .line 3059
    const-string v0, "Fail"

    iput-object v0, p0, Lcom/osp/app/signin/cq;->e:Ljava/lang/String;

    goto/16 :goto_0

    .line 3061
    :cond_b
    iget-wide v4, p0, Lcom/osp/app/signin/cq;->i:J
    :try_end_9
    .catchall {:try_start_9 .. :try_end_9} :catchall_0

    cmp-long v3, v0, v4

    if-nez v3, :cond_d

    .line 3065
    :try_start_a
    invoke-static {}, Lcom/msc/c/h;->a()Lcom/msc/c/h;

    invoke-static {v2}, Lcom/msc/c/h;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 3066
    if-eqz v0, :cond_c

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v1

    if-lez v1, :cond_c

    .line 3068
    iget-object v1, p0, Lcom/osp/app/signin/cq;->c:Lcom/osp/app/signin/BackgroundModeService;

    invoke-static {v1, v0}, Lcom/msc/c/e;->e(Landroid/content/Context;Ljava/lang/String;)V

    .line 3069
    invoke-virtual {p0}, Lcom/osp/app/signin/cq;->isCancelled()Z

    move-result v0

    if-nez v0, :cond_0

    .line 3071
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/osp/app/signin/cq;->l:Z

    .line 3072
    invoke-direct {p0}, Lcom/osp/app/signin/cq;->j()V
    :try_end_a
    .catch Ljava/lang/Exception; {:try_start_a .. :try_end_a} :catch_3
    .catchall {:try_start_a .. :try_end_a} :catchall_0

    goto/16 :goto_0

    .line 3078
    :catch_3
    move-exception v0

    :try_start_b
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V
    :try_end_b
    .catchall {:try_start_b .. :try_end_b} :catchall_0

    goto/16 :goto_0

    .line 3076
    :cond_c
    :try_start_c
    const-string v0, "Fail"

    iput-object v0, p0, Lcom/osp/app/signin/cq;->e:Ljava/lang/String;
    :try_end_c
    .catch Ljava/lang/Exception; {:try_start_c .. :try_end_c} :catch_3
    .catchall {:try_start_c .. :try_end_c} :catchall_0

    goto/16 :goto_0

    .line 3082
    :cond_d
    :try_start_d
    iget-wide v4, p0, Lcom/osp/app/signin/cq;->j:J
    :try_end_d
    .catchall {:try_start_d .. :try_end_d} :catchall_0

    cmp-long v3, v0, v4

    if-nez v3, :cond_e

    .line 3086
    :try_start_e
    invoke-static {}, Lcom/msc/c/h;->a()Lcom/msc/c/h;

    invoke-static {v2}, Lcom/msc/c/h;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 3087
    invoke-virtual {p0}, Lcom/osp/app/signin/cq;->isCancelled()Z

    move-result v0

    if-nez v0, :cond_0

    .line 3089
    invoke-static {}, Lcom/msc/c/g;->a()Lcom/msc/c/g;

    iget-object v0, p0, Lcom/osp/app/signin/cq;->c:Lcom/osp/app/signin/BackgroundModeService;

    const-string v1, "authorization_code"

    const-string v3, "j5p7ll8g33"

    const-string v4, "5763D0052DC1462E13751F753384E9A9"

    move-object v5, p0

    invoke-static/range {v0 .. v5}, Lcom/msc/c/g;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/msc/b/h;)J

    move-result-wide v0

    iput-wide v0, p0, Lcom/osp/app/signin/cq;->k:J

    iget-wide v0, p0, Lcom/osp/app/signin/cq;->k:J

    invoke-virtual {p0, v0, v1}, Lcom/osp/app/signin/cq;->a(J)V

    iget-wide v0, p0, Lcom/osp/app/signin/cq;->k:J

    const-string v2, "from_json"

    invoke-virtual {p0, v0, v1, v2}, Lcom/osp/app/signin/cq;->a(JLjava/lang/String;)V

    iget-wide v0, p0, Lcom/osp/app/signin/cq;->k:J

    sget-object v2, Lcom/msc/b/g;->c:Lcom/msc/b/g;

    invoke-static {v0, v1, v2}, Lcom/msc/c/g;->a(JLcom/msc/b/g;)V
    :try_end_e
    .catch Ljava/lang/Exception; {:try_start_e .. :try_end_e} :catch_4
    .catchall {:try_start_e .. :try_end_e} :catchall_0

    goto/16 :goto_0

    .line 3091
    :catch_4
    move-exception v0

    :try_start_f
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    goto/16 :goto_0

    .line 3095
    :cond_e
    iget-wide v4, p0, Lcom/osp/app/signin/cq;->k:J
    :try_end_f
    .catchall {:try_start_f .. :try_end_f} :catchall_0

    cmp-long v3, v0, v4

    if-nez v3, :cond_f

    .line 3099
    :try_start_10
    invoke-static {}, Lcom/msc/c/h;->a()Lcom/msc/c/h;

    iget-object v0, p0, Lcom/osp/app/signin/cq;->c:Lcom/osp/app/signin/BackgroundModeService;

    invoke-static {v0, v2}, Lcom/msc/c/h;->a(Landroid/content/Context;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 3100
    iget-object v1, p0, Lcom/osp/app/signin/cq;->c:Lcom/osp/app/signin/BackgroundModeService;

    invoke-static {v1, v0}, Lcom/msc/c/e;->b(Landroid/content/Context;Ljava/lang/String;)V

    .line 3101
    invoke-virtual {p0}, Lcom/osp/app/signin/cq;->isCancelled()Z

    move-result v1

    if-nez v1, :cond_0

    .line 3103
    invoke-direct {p0, v0}, Lcom/osp/app/signin/cq;->b(Ljava/lang/String;)V
    :try_end_10
    .catch Ljava/lang/Exception; {:try_start_10 .. :try_end_10} :catch_5
    .catchall {:try_start_10 .. :try_end_10} :catchall_0

    goto/16 :goto_0

    .line 3105
    :catch_5
    move-exception v0

    :try_start_11
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    goto/16 :goto_0

    .line 3109
    :cond_f
    iget-wide v4, p0, Lcom/osp/app/signin/cq;->p:J
    :try_end_11
    .catchall {:try_start_11 .. :try_end_11} :catchall_0

    cmp-long v3, v0, v4

    if-nez v3, :cond_11

    .line 3113
    :try_start_12
    invoke-static {}, Lcom/msc/c/h;->a()Lcom/msc/c/h;

    invoke-static {v2}, Lcom/msc/c/h;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 3114
    if-eqz v0, :cond_10

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v1

    if-lez v1, :cond_10

    .line 3116
    iget-object v1, p0, Lcom/osp/app/signin/cq;->c:Lcom/osp/app/signin/BackgroundModeService;

    invoke-static {v1, v0}, Lcom/msc/c/e;->e(Landroid/content/Context;Ljava/lang/String;)V

    .line 3117
    invoke-virtual {p0}, Lcom/osp/app/signin/cq;->isCancelled()Z

    move-result v0

    if-nez v0, :cond_0

    .line 3119
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/osp/app/signin/cq;->n:Z

    .line 3120
    invoke-direct {p0}, Lcom/osp/app/signin/cq;->k()V
    :try_end_12
    .catch Ljava/lang/Exception; {:try_start_12 .. :try_end_12} :catch_6
    .catchall {:try_start_12 .. :try_end_12} :catchall_0

    goto/16 :goto_0

    .line 3126
    :catch_6
    move-exception v0

    :try_start_13
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    .line 3129
    const-string v0, "Fail"

    iput-object v0, p0, Lcom/osp/app/signin/cq;->e:Ljava/lang/String;
    :try_end_13
    .catchall {:try_start_13 .. :try_end_13} :catchall_0

    goto/16 :goto_0

    .line 3124
    :cond_10
    :try_start_14
    const-string v0, "Fail"

    iput-object v0, p0, Lcom/osp/app/signin/cq;->e:Ljava/lang/String;
    :try_end_14
    .catch Ljava/lang/Exception; {:try_start_14 .. :try_end_14} :catch_6
    .catchall {:try_start_14 .. :try_end_14} :catchall_0

    goto/16 :goto_0

    .line 3131
    :cond_11
    :try_start_15
    iget-wide v4, p0, Lcom/osp/app/signin/cq;->o:J
    :try_end_15
    .catchall {:try_start_15 .. :try_end_15} :catchall_0

    cmp-long v0, v0, v4

    if-nez v0, :cond_0

    .line 3135
    :try_start_16
    const-string v0, "REQUEST_ACCESSTOKEN"

    iget-object v1, p0, Lcom/osp/app/signin/cq;->d:Lcom/osp/app/util/d;

    iget-object v1, v1, Lcom/osp/app/util/d;->c:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_12

    .line 3137
    iget-object v0, p0, Lcom/osp/app/signin/cq;->d:Lcom/osp/app/util/d;

    invoke-static {}, Lcom/msc/c/h;->a()Lcom/msc/c/h;

    invoke-static {v2}, Lcom/msc/c/h;->v(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lcom/osp/app/util/d;->e:Ljava/lang/String;

    .line 3142
    :goto_3
    iget-object v0, p0, Lcom/osp/app/signin/cq;->d:Lcom/osp/app/util/d;

    iget-object v0, v0, Lcom/osp/app/util/d;->e:Ljava/lang/String;

    if-eqz v0, :cond_13

    iget-object v0, p0, Lcom/osp/app/signin/cq;->d:Lcom/osp/app/util/d;

    iget-object v0, v0, Lcom/osp/app/util/d;->e:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v0

    if-lez v0, :cond_13

    .line 3144
    const-string v0, "Success"

    iput-object v0, p0, Lcom/osp/app/signin/cq;->e:Ljava/lang/String;
    :try_end_16
    .catch Ljava/lang/Exception; {:try_start_16 .. :try_end_16} :catch_7
    .catchall {:try_start_16 .. :try_end_16} :catchall_0

    goto/16 :goto_0

    .line 3149
    :catch_7
    move-exception v0

    :try_start_17
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    .line 3152
    const-string v0, "Fail"

    iput-object v0, p0, Lcom/osp/app/signin/cq;->e:Ljava/lang/String;
    :try_end_17
    .catchall {:try_start_17 .. :try_end_17} :catchall_0

    goto/16 :goto_0

    .line 3140
    :cond_12
    :try_start_18
    iget-object v0, p0, Lcom/osp/app/signin/cq;->d:Lcom/osp/app/util/d;

    invoke-static {}, Lcom/msc/c/h;->a()Lcom/msc/c/h;

    invoke-static {v2}, Lcom/msc/c/h;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lcom/osp/app/util/d;->e:Ljava/lang/String;

    goto :goto_3

    .line 3147
    :cond_13
    const-string v0, "Fail"

    iput-object v0, p0, Lcom/osp/app/signin/cq;->e:Ljava/lang/String;
    :try_end_18
    .catch Ljava/lang/Exception; {:try_start_18 .. :try_end_18} :catch_7
    .catchall {:try_start_18 .. :try_end_18} :catchall_0

    goto/16 :goto_0
.end method

.method protected final a(Ljava/lang/Boolean;)V
    .locals 8

    .prologue
    const-wide/16 v3, 0x0

    const/4 v5, 0x0

    const/high16 v7, 0x10000000

    const/4 v6, 0x1

    .line 2822
    invoke-super {p0, p1}, Lcom/msc/c/b;->a(Ljava/lang/Boolean;)V

    .line 2824
    iget-object v0, p0, Lcom/osp/app/signin/cq;->c:Lcom/osp/app/signin/BackgroundModeService;

    invoke-static {v0}, Lcom/osp/app/signin/BackgroundModeService;->b(Lcom/osp/app/signin/BackgroundModeService;)Ljava/util/HashMap;

    move-result-object v0

    iget-object v1, p0, Lcom/osp/app/signin/cq;->d:Lcom/osp/app/util/d;

    iget v1, v1, Lcom/osp/app/util/d;->s:I

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 2826
    iget-object v0, p0, Lcom/osp/app/signin/cq;->c:Lcom/osp/app/signin/BackgroundModeService;

    invoke-static {v0}, Lcom/osp/app/signin/BackgroundModeService;->b(Lcom/osp/app/signin/BackgroundModeService;)Ljava/util/HashMap;

    move-result-object v0

    iget-object v1, p0, Lcom/osp/app/signin/cq;->d:Lcom/osp/app/util/d;

    iget v1, v1, Lcom/osp/app/util/d;->s:I

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 2829
    :cond_0
    const-string v0, "Success"

    iget-object v1, p0, Lcom/osp/app/signin/cq;->e:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_9

    .line 2831
    const-string v0, "REQUEST_ACCESSTOKEN"

    iget-object v1, p0, Lcom/osp/app/signin/cq;->d:Lcom/osp/app/util/d;

    iget-object v1, v1, Lcom/osp/app/util/d;->c:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 2833
    invoke-direct {p0}, Lcom/osp/app/signin/cq;->h()V

    .line 2836
    :cond_1
    iget-object v0, p0, Lcom/osp/app/signin/cq;->d:Lcom/osp/app/util/d;

    iget-boolean v0, v0, Lcom/osp/app/util/d;->a:Z

    if-eqz v0, :cond_2

    .line 2838
    iget-object v0, p0, Lcom/osp/app/signin/cq;->c:Lcom/osp/app/signin/BackgroundModeService;

    invoke-static {v0}, Lcom/msc/c/e;->c(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v0

    .line 2839
    if-nez v0, :cond_2

    .line 2841
    iget-object v0, p0, Lcom/osp/app/signin/cq;->d:Lcom/osp/app/util/d;

    iget-object v0, v0, Lcom/osp/app/util/d;->d:Lcom/osp/app/signin/SignUpinfo;

    if-eqz v0, :cond_2

    .line 2843
    iget-object v0, p0, Lcom/osp/app/signin/cq;->d:Lcom/osp/app/util/d;

    iget-object v0, v0, Lcom/osp/app/util/d;->d:Lcom/osp/app/signin/SignUpinfo;

    invoke-virtual {v0}, Lcom/osp/app/signin/SignUpinfo;->i()Ljava/lang/String;

    move-result-object v0

    .line 2844
    iget-object v1, p0, Lcom/osp/app/signin/cq;->c:Lcom/osp/app/signin/BackgroundModeService;

    invoke-static {v1, v0}, Lcom/msc/c/e;->c(Landroid/content/Context;Ljava/lang/String;)V

    .line 2849
    :cond_2
    iget-boolean v0, p0, Lcom/osp/app/signin/cq;->r:Z

    if-nez v0, :cond_4

    iget-object v0, p0, Lcom/osp/app/signin/cq;->d:Lcom/osp/app/util/d;

    iget-object v0, v0, Lcom/osp/app/util/d;->i:Lcom/osp/app/signin/kd;

    if-eqz v0, :cond_4

    iget-object v0, p0, Lcom/osp/app/signin/cq;->d:Lcom/osp/app/util/d;

    iget-object v0, v0, Lcom/osp/app/util/d;->i:Lcom/osp/app/signin/kd;

    invoke-virtual {v0}, Lcom/osp/app/signin/kd;->b()I

    move-result v0

    if-lez v0, :cond_4

    .line 2851
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    .line 2852
    const-string v1, "com.osp.app.signin"

    const-string v2, "com.osp.app.signin.AccountinfoView"

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->setClassName(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 2853
    invoke-virtual {v0, v7}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    .line 2854
    const-string v1, "client_id"

    iget-object v2, p0, Lcom/osp/app/signin/cq;->d:Lcom/osp/app/util/d;

    iget-object v2, v2, Lcom/osp/app/util/d;->f:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 2855
    const-string v1, "client_secret"

    iget-object v2, p0, Lcom/osp/app/signin/cq;->d:Lcom/osp/app/util/d;

    iget-object v2, v2, Lcom/osp/app/util/d;->g:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 2856
    const-string v1, "account_mode"

    iget-object v2, p0, Lcom/osp/app/signin/cq;->d:Lcom/osp/app/util/d;

    iget-object v2, v2, Lcom/osp/app/util/d;->c:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 2857
    const-string v1, "service_name"

    iget-object v2, p0, Lcom/osp/app/signin/cq;->d:Lcom/osp/app/util/d;

    iget-object v2, v2, Lcom/osp/app/util/d;->m:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 2858
    const-string v1, "OSP_VER"

    iget-object v2, p0, Lcom/osp/app/signin/cq;->d:Lcom/osp/app/util/d;

    iget-object v2, v2, Lcom/osp/app/util/d;->l:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 2859
    const-string v1, "direct_modify"

    invoke-virtual {v0, v1, v6}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 2860
    const-string v1, "BG_WhoareU"

    iget-object v2, p0, Lcom/osp/app/signin/cq;->d:Lcom/osp/app/util/d;

    iget-object v2, v2, Lcom/osp/app/util/d;->o:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 2861
    const-string v1, "is_authorized_internal_action_for_accountinfo"

    const-string v2, "TIME_PATTERN"

    invoke-static {v2}, Lcom/osp/app/util/ad;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 2863
    const-string v1, "BG_mode"

    iget-object v2, p0, Lcom/osp/app/signin/cq;->d:Lcom/osp/app/util/d;

    iget-object v2, v2, Lcom/osp/app/util/d;->o:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_3

    .line 2865
    const-string v1, "authcode"

    iget-object v2, p0, Lcom/osp/app/signin/cq;->d:Lcom/osp/app/util/d;

    iget-object v2, v2, Lcom/osp/app/util/d;->e:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 2866
    const-string v1, "mypackage"

    iget-object v2, p0, Lcom/osp/app/signin/cq;->d:Lcom/osp/app/util/d;

    iget-object v2, v2, Lcom/osp/app/util/d;->n:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 2867
    const-string v1, "key_request_id"

    iget-object v2, p0, Lcom/osp/app/signin/cq;->d:Lcom/osp/app/util/d;

    iget-wide v2, v2, Lcom/osp/app/util/d;->r:J

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;J)Landroid/content/Intent;

    .line 2869
    :cond_3
    iget-object v1, p0, Lcom/osp/app/signin/cq;->c:Lcom/osp/app/signin/BackgroundModeService;

    invoke-virtual {v1, v0}, Lcom/osp/app/signin/BackgroundModeService;->startActivity(Landroid/content/Intent;)V

    .line 2870
    invoke-static {}, Lcom/osp/app/util/ap;->a()Lcom/osp/app/util/ap;

    iget-object v0, p0, Lcom/osp/app/signin/cq;->c:Lcom/osp/app/signin/BackgroundModeService;

    iget-object v1, p0, Lcom/osp/app/signin/cq;->c:Lcom/osp/app/signin/BackgroundModeService;

    const v2, 0x7f090024

    invoke-virtual {v1, v2}, Lcom/osp/app/signin/BackgroundModeService;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1, v6}, Lcom/osp/app/util/ap;->a(Landroid/content/Context;Ljava/lang/String;I)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    .line 2977
    :goto_0
    iget-object v0, p0, Lcom/osp/app/signin/cq;->c:Lcom/osp/app/signin/BackgroundModeService;

    iget-object v1, p0, Lcom/osp/app/signin/cq;->d:Lcom/osp/app/util/d;

    iget v1, v1, Lcom/osp/app/util/d;->s:I

    invoke-virtual {v0, v1}, Lcom/osp/app/signin/BackgroundModeService;->stopSelf(I)V

    .line 2979
    invoke-static {}, Lcom/osp/app/signin/BackgroundModeService;->a()Z

    .line 2980
    :goto_1
    return-void

    .line 2873
    :cond_4
    iget-boolean v0, p0, Lcom/osp/app/signin/cq;->r:Z

    if-nez v0, :cond_6

    iget-object v0, p0, Lcom/osp/app/signin/cq;->d:Lcom/osp/app/util/d;

    iget-object v0, v0, Lcom/osp/app/util/d;->t:Lcom/msc/a/g;

    if-eqz v0, :cond_6

    iget-object v0, p0, Lcom/osp/app/signin/cq;->d:Lcom/osp/app/util/d;

    iget-object v0, v0, Lcom/osp/app/util/d;->t:Lcom/msc/a/g;

    invoke-virtual {v0}, Lcom/msc/a/g;->d()Z

    move-result v0

    if-nez v0, :cond_5

    iget-object v0, p0, Lcom/osp/app/signin/cq;->d:Lcom/osp/app/util/d;

    iget-object v0, v0, Lcom/osp/app/util/d;->t:Lcom/msc/a/g;

    invoke-virtual {v0}, Lcom/msc/a/g;->e()Z

    move-result v0

    if-eqz v0, :cond_6

    .line 2876
    :cond_5
    new-instance v0, Landroid/content/Intent;

    const-string v1, "com.msc.action.samsungaccount.Update_NewTerms"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 2877
    invoke-virtual {v0, v7}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    .line 2878
    const-string v1, "tncAccepted"

    iget-object v2, p0, Lcom/osp/app/signin/cq;->d:Lcom/osp/app/util/d;

    iget-object v2, v2, Lcom/osp/app/util/d;->t:Lcom/msc/a/g;

    invoke-virtual {v2}, Lcom/msc/a/g;->d()Z

    move-result v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 2879
    const-string v1, "privacyAccepted"

    iget-object v2, p0, Lcom/osp/app/signin/cq;->d:Lcom/osp/app/util/d;

    iget-object v2, v2, Lcom/osp/app/util/d;->t:Lcom/msc/a/g;

    invoke-virtual {v2}, Lcom/msc/a/g;->e()Z

    move-result v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 2880
    const-string v1, "key_tnc_update_mode"

    invoke-virtual {v0, v1, v6}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 2881
    const-string v1, "key_request_id"

    iget-object v2, p0, Lcom/osp/app/signin/cq;->d:Lcom/osp/app/util/d;

    iget-wide v6, v2, Lcom/osp/app/util/d;->r:J

    invoke-virtual {v0, v1, v6, v7}, Landroid/content/Intent;->putExtra(Ljava/lang/String;J)Landroid/content/Intent;

    .line 2882
    const-string v1, "country_code_mcc"

    iget-object v2, p0, Lcom/osp/app/signin/cq;->q:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 2883
    iget-object v1, p0, Lcom/osp/app/signin/cq;->c:Lcom/osp/app/signin/BackgroundModeService;

    invoke-virtual {v1, v0}, Lcom/osp/app/signin/BackgroundModeService;->startActivity(Landroid/content/Intent;)V

    .line 2884
    iget-object v0, p0, Lcom/osp/app/signin/cq;->d:Lcom/osp/app/util/d;

    iput-object v5, v0, Lcom/osp/app/util/d;->t:Lcom/msc/a/g;

    .line 2886
    :cond_6
    iget-object v0, p0, Lcom/osp/app/signin/cq;->d:Lcom/osp/app/util/d;

    iget-object v0, v0, Lcom/osp/app/util/d;->n:Ljava/lang/String;

    if-eqz v0, :cond_8

    .line 2888
    const-string v0, "REQUEST_ACCESSTOKEN"

    iget-object v1, p0, Lcom/osp/app/signin/cq;->d:Lcom/osp/app/util/d;

    iget-object v1, v1, Lcom/osp/app/util/d;->c:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_7

    .line 2898
    iget-object v0, p0, Lcom/osp/app/signin/cq;->c:Lcom/osp/app/signin/BackgroundModeService;

    iget-object v1, p0, Lcom/osp/app/signin/cq;->d:Lcom/osp/app/util/d;

    iget-object v1, v1, Lcom/osp/app/util/d;->f:Ljava/lang/String;

    iget-object v2, p0, Lcom/osp/app/signin/cq;->d:Lcom/osp/app/util/d;

    iget-object v2, v2, Lcom/osp/app/util/d;->e:Ljava/lang/String;

    move-wide v6, v3

    invoke-static/range {v0 .. v7}, Lcom/osp/app/util/ad;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;JLjava/lang/String;J)V

    .line 2902
    :cond_7
    new-instance v0, Landroid/content/Intent;

    const-string v1, "com.msc.action.samsungaccount.response.BackGroundSignin"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 2903
    const-string v1, "bg_result"

    const/4 v2, -0x1

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 2904
    const-string v1, "authcode"

    iget-object v2, p0, Lcom/osp/app/signin/cq;->d:Lcom/osp/app/util/d;

    iget-object v2, v2, Lcom/osp/app/util/d;->e:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 2905
    const-string v1, "ServerUrl"

    iget-object v2, p0, Lcom/osp/app/signin/cq;->c:Lcom/osp/app/signin/BackgroundModeService;

    invoke-static {v2}, Lcom/msc/c/c;->f(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 2908
    const-string v1, "request_code"

    iget-object v2, p0, Lcom/osp/app/signin/cq;->d:Lcom/osp/app/util/d;

    iget v2, v2, Lcom/osp/app/util/d;->q:I

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 2910
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v2, p0, Lcom/osp/app/signin/cq;->d:Lcom/osp/app/util/d;

    iget-object v2, v2, Lcom/osp/app/util/d;->n:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ":"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setData(Landroid/net/Uri;)Landroid/content/Intent;

    .line 2911
    iget-object v1, p0, Lcom/osp/app/signin/cq;->d:Lcom/osp/app/util/d;

    iget-object v1, v1, Lcom/osp/app/util/d;->n:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setPackage(Ljava/lang/String;)Landroid/content/Intent;

    .line 2915
    const-string v1, "client_id"

    iget-object v2, p0, Lcom/osp/app/signin/cq;->d:Lcom/osp/app/util/d;

    iget-object v2, v2, Lcom/osp/app/util/d;->f:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 2916
    iget-object v1, p0, Lcom/osp/app/signin/cq;->c:Lcom/osp/app/signin/BackgroundModeService;

    iget-object v2, p0, Lcom/osp/app/signin/cq;->d:Lcom/osp/app/util/d;

    iget-wide v2, v2, Lcom/osp/app/util/d;->r:J

    invoke-virtual {v1, v0, v2, v3}, Lcom/osp/app/signin/BackgroundModeService;->a(Landroid/content/Intent;J)V

    .line 2917
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "sendBroadcast to "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v1, p0, Lcom/osp/app/signin/cq;->d:Lcom/osp/app/util/d;

    iget-object v1, v1, Lcom/osp/app/util/d;->n:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;)V

    goto/16 :goto_0

    .line 2920
    :cond_8
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v0, "cancel broadcast - no package name"

    invoke-static {v0}, Lcom/osp/app/util/aq;->c(Ljava/lang/String;)V

    goto/16 :goto_0

    .line 2923
    :cond_9
    const-string v0, "Newtork Error"

    iget-object v1, p0, Lcom/osp/app/signin/cq;->e:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_b

    .line 2925
    iget-object v0, p0, Lcom/osp/app/signin/cq;->d:Lcom/osp/app/util/d;

    iget-object v0, v0, Lcom/osp/app/util/d;->n:Ljava/lang/String;

    if-eqz v0, :cond_a

    .line 2927
    new-instance v0, Landroid/content/Intent;

    const-string v1, "com.msc.action.samsungaccount.response.BackGroundSignin"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 2928
    const-string v1, "bg_result"

    const/4 v2, 0x3

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 2931
    const-string v1, "request_code"

    iget-object v2, p0, Lcom/osp/app/signin/cq;->d:Lcom/osp/app/util/d;

    iget v2, v2, Lcom/osp/app/util/d;->q:I

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 2933
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v2, p0, Lcom/osp/app/signin/cq;->d:Lcom/osp/app/util/d;

    iget-object v2, v2, Lcom/osp/app/util/d;->n:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ":"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setData(Landroid/net/Uri;)Landroid/content/Intent;

    .line 2934
    iget-object v1, p0, Lcom/osp/app/signin/cq;->d:Lcom/osp/app/util/d;

    iget-object v1, v1, Lcom/osp/app/util/d;->n:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setPackage(Ljava/lang/String;)Landroid/content/Intent;

    .line 2937
    const-string v1, "client_id"

    iget-object v2, p0, Lcom/osp/app/signin/cq;->d:Lcom/osp/app/util/d;

    iget-object v2, v2, Lcom/osp/app/util/d;->f:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 2938
    iget-object v1, p0, Lcom/osp/app/signin/cq;->c:Lcom/osp/app/signin/BackgroundModeService;

    iget-object v2, p0, Lcom/osp/app/signin/cq;->d:Lcom/osp/app/util/d;

    iget-wide v2, v2, Lcom/osp/app/util/d;->r:J

    invoke-virtual {v1, v0, v2, v3}, Lcom/osp/app/signin/BackgroundModeService;->a(Landroid/content/Intent;J)V

    .line 2939
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "sendBroadcast to "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v1, p0, Lcom/osp/app/signin/cq;->d:Lcom/osp/app/util/d;

    iget-object v1, v1, Lcom/osp/app/util/d;->n:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;)V

    goto/16 :goto_0

    .line 2942
    :cond_a
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v0, "BMS"

    const-string v1, "cancel broadcast - no package name"

    invoke-static {v0, v1}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 2946
    :cond_b
    iget-object v0, p0, Lcom/osp/app/signin/cq;->d:Lcom/osp/app/util/d;

    iget-boolean v0, v0, Lcom/osp/app/util/d;->b:Z

    if-eqz v0, :cond_c

    .line 2948
    iget-object v0, p0, Lcom/osp/app/signin/cq;->d:Lcom/osp/app/util/d;

    const/4 v1, 0x0

    iput-boolean v1, v0, Lcom/osp/app/util/d;->b:Z

    .line 2949
    iget-object v0, p0, Lcom/osp/app/signin/cq;->c:Lcom/osp/app/signin/BackgroundModeService;

    iget-object v1, p0, Lcom/osp/app/signin/cq;->d:Lcom/osp/app/util/d;

    invoke-static {v0, v6, v1}, Lcom/osp/app/signin/BackgroundModeService;->a(Lcom/osp/app/signin/BackgroundModeService;ZLcom/osp/app/util/d;)V

    .line 2950
    invoke-static {}, Lcom/osp/app/signin/BackgroundModeService;->a()Z

    goto/16 :goto_1

    .line 2954
    :cond_c
    iget-object v0, p0, Lcom/osp/app/signin/cq;->d:Lcom/osp/app/util/d;

    iget-object v0, v0, Lcom/osp/app/util/d;->n:Ljava/lang/String;

    if-eqz v0, :cond_d

    .line 2956
    new-instance v0, Landroid/content/Intent;

    const-string v1, "com.msc.action.samsungaccount.response.BackGroundSignin"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 2957
    const-string v1, "bg_result"

    invoke-virtual {v0, v1, v6}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 2960
    const-string v1, "request_code"

    iget-object v2, p0, Lcom/osp/app/signin/cq;->d:Lcom/osp/app/util/d;

    iget v2, v2, Lcom/osp/app/util/d;->q:I

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 2962
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v2, p0, Lcom/osp/app/signin/cq;->d:Lcom/osp/app/util/d;

    iget-object v2, v2, Lcom/osp/app/util/d;->n:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ":"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setData(Landroid/net/Uri;)Landroid/content/Intent;

    .line 2963
    iget-object v1, p0, Lcom/osp/app/signin/cq;->d:Lcom/osp/app/util/d;

    iget-object v1, v1, Lcom/osp/app/util/d;->n:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setPackage(Ljava/lang/String;)Landroid/content/Intent;

    .line 2967
    const-string v1, "client_id"

    iget-object v2, p0, Lcom/osp/app/signin/cq;->d:Lcom/osp/app/util/d;

    iget-object v2, v2, Lcom/osp/app/util/d;->f:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 2968
    iget-object v1, p0, Lcom/osp/app/signin/cq;->c:Lcom/osp/app/signin/BackgroundModeService;

    iget-object v2, p0, Lcom/osp/app/signin/cq;->d:Lcom/osp/app/util/d;

    iget-wide v2, v2, Lcom/osp/app/util/d;->r:J

    invoke-virtual {v1, v0, v2, v3}, Lcom/osp/app/signin/BackgroundModeService;->a(Landroid/content/Intent;J)V

    .line 2969
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "sendBroadcast to "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v1, p0, Lcom/osp/app/signin/cq;->d:Lcom/osp/app/util/d;

    iget-object v1, v1, Lcom/osp/app/util/d;->n:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;)V

    goto/16 :goto_0

    .line 2972
    :cond_d
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v0, "BMS"

    const-string v1, "cancel broadcast - no package name"

    invoke-static {v0, v1}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0
.end method

.method public final b(Lcom/msc/b/c;)V
    .locals 8

    .prologue
    const/4 v7, 0x1

    const/4 v6, 0x0

    .line 3159
    invoke-super {p0, p1}, Lcom/msc/c/b;->b(Lcom/msc/b/c;)V

    .line 3161
    if-nez p1, :cond_1

    .line 3382
    :cond_0
    :goto_0
    return-void

    .line 3166
    :cond_1
    invoke-virtual {p1}, Lcom/msc/b/c;->a()J

    move-result-wide v0

    .line 3167
    invoke-virtual {p1}, Lcom/msc/b/c;->e()Ljava/lang/String;

    move-result-object v2

    .line 3168
    invoke-virtual {p1}, Lcom/msc/b/c;->f()Ljava/lang/Exception;

    move-result-object v3

    .line 3170
    iget-wide v4, p0, Lcom/osp/app/signin/cq;->f:J

    cmp-long v4, v0, v4

    if-nez v4, :cond_4

    .line 3172
    invoke-static {}, Lcom/osp/device/b;->b()Lcom/osp/device/b;

    move-result-object v0

    invoke-virtual {v0}, Lcom/osp/device/b;->a()Z

    move-result v0

    if-eqz v0, :cond_3

    const-string v0, "USR_3174"

    iget-object v1, p0, Lcom/osp/app/signin/cq;->b:Lcom/msc/c/f;

    invoke-virtual {v1}, Lcom/msc/c/f;->a()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 3174
    iget-object v0, p0, Lcom/osp/app/signin/cq;->c:Lcom/osp/app/signin/BackgroundModeService;

    invoke-virtual {v0}, Lcom/osp/app/signin/BackgroundModeService;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Landroid/preference/PreferenceManager;->getDefaultSharedPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v0

    .line 3175
    const-string v1, "FLAG_DO_NOT_SHOW_NOTIFICATION"

    invoke-interface {v0, v1, v6}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    .line 3176
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v1, "BMS"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "do not show again noti : "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 3178
    if-nez v0, :cond_2

    .line 3180
    iget-object v0, p0, Lcom/osp/app/signin/cq;->c:Lcom/osp/app/signin/BackgroundModeService;

    iget-object v1, p0, Lcom/osp/app/signin/cq;->d:Lcom/osp/app/util/d;

    invoke-static {v0, v1}, Lcom/osp/app/signin/BackgroundModeService;->a(Lcom/osp/app/signin/BackgroundModeService;Lcom/osp/app/util/d;)V

    .line 3182
    :cond_2
    const-string v0, "Fail"

    iput-object v0, p0, Lcom/osp/app/signin/cq;->e:Ljava/lang/String;

    goto :goto_0

    .line 3191
    :cond_3
    const-string v0, "Fail"

    iput-object v0, p0, Lcom/osp/app/signin/cq;->e:Ljava/lang/String;

    goto :goto_0

    .line 3198
    :cond_4
    iget-wide v4, p0, Lcom/osp/app/signin/cq;->g:J

    cmp-long v4, v0, v4

    if-nez v4, :cond_d

    .line 3200
    if-eqz v3, :cond_9

    .line 3202
    iget-object v0, p0, Lcom/osp/app/signin/cq;->b:Lcom/msc/c/f;

    invoke-virtual {v0}, Lcom/msc/c/f;->c()Z

    move-result v0

    if-eqz v0, :cond_5

    .line 3204
    const-string v0, "Newtork Error"

    iput-object v0, p0, Lcom/osp/app/signin/cq;->e:Ljava/lang/String;

    goto :goto_0

    .line 3207
    :cond_5
    invoke-virtual {p0}, Lcom/osp/app/signin/cq;->isCancelled()Z

    move-result v0

    if-nez v0, :cond_8

    .line 3209
    iget-object v0, p0, Lcom/osp/app/signin/cq;->d:Lcom/osp/app/util/d;

    iget-boolean v0, v0, Lcom/osp/app/util/d;->a:Z

    if-eqz v0, :cond_7

    .line 3211
    iget-object v0, p0, Lcom/osp/app/signin/cq;->c:Lcom/osp/app/signin/BackgroundModeService;

    invoke-static {v0}, Lcom/msc/c/e;->b(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v0

    .line 3212
    if-eqz v0, :cond_6

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v1

    if-lez v1, :cond_6

    .line 3214
    invoke-direct {p0, v0}, Lcom/osp/app/signin/cq;->b(Ljava/lang/String;)V

    goto/16 :goto_0

    .line 3217
    :cond_6
    invoke-direct {p0}, Lcom/osp/app/signin/cq;->j()V

    goto/16 :goto_0

    .line 3221
    :cond_7
    invoke-direct {p0}, Lcom/osp/app/signin/cq;->k()V

    goto/16 :goto_0

    .line 3225
    :cond_8
    const-string v0, "Fail"

    iput-object v0, p0, Lcom/osp/app/signin/cq;->e:Ljava/lang/String;

    goto/16 :goto_0

    .line 3231
    :cond_9
    invoke-virtual {p0}, Lcom/osp/app/signin/cq;->isCancelled()Z

    move-result v0

    if-nez v0, :cond_c

    .line 3233
    iget-object v0, p0, Lcom/osp/app/signin/cq;->d:Lcom/osp/app/util/d;

    iget-boolean v0, v0, Lcom/osp/app/util/d;->a:Z

    if-eqz v0, :cond_b

    .line 3236
    iget-object v0, p0, Lcom/osp/app/signin/cq;->c:Lcom/osp/app/signin/BackgroundModeService;

    invoke-static {v0}, Lcom/msc/c/e;->b(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v0

    .line 3237
    if-eqz v0, :cond_a

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v1

    if-lez v1, :cond_a

    .line 3239
    invoke-direct {p0, v0}, Lcom/osp/app/signin/cq;->b(Ljava/lang/String;)V

    goto/16 :goto_0

    .line 3242
    :cond_a
    invoke-direct {p0}, Lcom/osp/app/signin/cq;->j()V

    goto/16 :goto_0

    .line 3246
    :cond_b
    invoke-direct {p0}, Lcom/osp/app/signin/cq;->k()V

    goto/16 :goto_0

    .line 3250
    :cond_c
    const-string v0, "Fail"

    iput-object v0, p0, Lcom/osp/app/signin/cq;->e:Ljava/lang/String;

    goto/16 :goto_0

    .line 3253
    :cond_d
    iget-wide v4, p0, Lcom/osp/app/signin/cq;->h:J

    cmp-long v3, v0, v4

    if-nez v3, :cond_10

    .line 3255
    if-eqz v2, :cond_f

    .line 3257
    iget-boolean v0, p0, Lcom/osp/app/signin/cq;->m:Z

    if-nez v0, :cond_e

    const-string v0, "ACF_0403"

    iget-object v1, p0, Lcom/osp/app/signin/cq;->b:Lcom/msc/c/f;

    invoke-virtual {v1}, Lcom/msc/c/f;->a()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_e

    .line 3259
    iput-boolean v7, p0, Lcom/osp/app/signin/cq;->m:Z

    .line 3260
    iget-object v0, p0, Lcom/osp/app/signin/cq;->c:Lcom/osp/app/signin/BackgroundModeService;

    invoke-static {v0}, Lcom/msc/c/e;->b(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/osp/app/signin/cq;->b(Ljava/lang/String;)V

    goto/16 :goto_0

    .line 3263
    :cond_e
    const-string v0, "Fail"

    iput-object v0, p0, Lcom/osp/app/signin/cq;->e:Ljava/lang/String;

    goto/16 :goto_0

    .line 3267
    :cond_f
    const-string v0, "Fail"

    iput-object v0, p0, Lcom/osp/app/signin/cq;->e:Ljava/lang/String;

    goto/16 :goto_0

    .line 3269
    :cond_10
    iget-wide v4, p0, Lcom/osp/app/signin/cq;->i:J

    cmp-long v3, v0, v4

    if-eqz v3, :cond_12

    .line 3271
    iget-wide v4, p0, Lcom/osp/app/signin/cq;->j:J

    cmp-long v3, v0, v4

    if-nez v3, :cond_16

    .line 3274
    if-eqz v2, :cond_15

    .line 3276
    iget-boolean v0, p0, Lcom/osp/app/signin/cq;->l:Z

    if-nez v0, :cond_13

    iget-object v0, p0, Lcom/osp/app/signin/cq;->b:Lcom/msc/c/f;

    if-eqz v0, :cond_13

    const-string v0, "AUT_1302"

    iget-object v1, p0, Lcom/osp/app/signin/cq;->b:Lcom/msc/c/f;

    invoke-virtual {v1}, Lcom/msc/c/f;->a()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_11

    const-string v0, "AUT_1830"

    iget-object v1, p0, Lcom/osp/app/signin/cq;->b:Lcom/msc/c/f;

    invoke-virtual {v1}, Lcom/msc/c/f;->a()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_13

    .line 3278
    :cond_11
    iget-object v0, p0, Lcom/osp/app/signin/cq;->c:Lcom/osp/app/signin/BackgroundModeService;

    invoke-static {v0}, Lcom/msc/c/c;->g(Landroid/content/Context;)V

    .line 3279
    iget-object v0, p0, Lcom/osp/app/signin/cq;->c:Lcom/osp/app/signin/BackgroundModeService;

    invoke-static {v0}, Lcom/msc/c/e;->a(Landroid/content/Context;)Z

    .line 3280
    invoke-virtual {p0}, Lcom/osp/app/signin/cq;->isCancelled()Z

    move-result v0

    if-nez v0, :cond_0

    .line 3284
    iget-object v0, p0, Lcom/osp/app/signin/cq;->c:Lcom/osp/app/signin/BackgroundModeService;

    iget-object v1, p0, Lcom/osp/app/signin/cq;->d:Lcom/osp/app/util/d;

    invoke-static {v0, v1}, Lcom/osp/app/signin/BackgroundModeService;->a(Lcom/osp/app/signin/BackgroundModeService;Lcom/osp/app/util/d;)V

    .line 3379
    :cond_12
    :goto_1
    const-string v0, "Fail"

    iput-object v0, p0, Lcom/osp/app/signin/cq;->e:Ljava/lang/String;

    goto/16 :goto_0

    .line 3291
    :cond_13
    invoke-static {}, Lcom/osp/device/b;->b()Lcom/osp/device/b;

    move-result-object v0

    invoke-virtual {v0}, Lcom/osp/device/b;->a()Z

    move-result v0

    if-eqz v0, :cond_14

    iget-boolean v0, p0, Lcom/osp/app/signin/cq;->l:Z

    if-nez v0, :cond_14

    iget-object v0, p0, Lcom/osp/app/signin/cq;->b:Lcom/msc/c/f;

    if-eqz v0, :cond_14

    const-string v0, "AUT_1094"

    iget-object v1, p0, Lcom/osp/app/signin/cq;->b:Lcom/msc/c/f;

    invoke-virtual {v1}, Lcom/msc/c/f;->a()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_14

    .line 3293
    invoke-virtual {p0}, Lcom/osp/app/signin/cq;->isCancelled()Z

    move-result v0

    if-nez v0, :cond_0

    .line 3295
    iget-object v0, p0, Lcom/osp/app/signin/cq;->c:Lcom/osp/app/signin/BackgroundModeService;

    iget-object v1, p0, Lcom/osp/app/signin/cq;->d:Lcom/osp/app/util/d;

    invoke-static {v0, v1}, Lcom/osp/app/signin/BackgroundModeService;->b(Lcom/osp/app/signin/BackgroundModeService;Lcom/osp/app/util/d;)V

    goto :goto_1

    .line 3300
    :cond_14
    const-string v0, "Fail"

    iput-object v0, p0, Lcom/osp/app/signin/cq;->e:Ljava/lang/String;

    goto/16 :goto_0

    .line 3304
    :cond_15
    const-string v0, "Fail"

    iput-object v0, p0, Lcom/osp/app/signin/cq;->e:Ljava/lang/String;

    goto/16 :goto_0

    .line 3306
    :cond_16
    iget-wide v4, p0, Lcom/osp/app/signin/cq;->k:J

    cmp-long v3, v0, v4

    if-eqz v3, :cond_12

    .line 3308
    iget-wide v4, p0, Lcom/osp/app/signin/cq;->p:J

    cmp-long v3, v0, v4

    if-nez v3, :cond_19

    .line 3311
    if-eqz v2, :cond_18

    .line 3313
    const-string v0, "AUT_1805"

    iget-object v1, p0, Lcom/osp/app/signin/cq;->b:Lcom/msc/c/f;

    invoke-virtual {v1}, Lcom/msc/c/f;->a()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_17

    const-string v0, "AUT_1004"

    iget-object v1, p0, Lcom/osp/app/signin/cq;->b:Lcom/msc/c/f;

    invoke-virtual {v1}, Lcom/msc/c/f;->a()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_18

    .line 3317
    :cond_17
    iget-object v0, p0, Lcom/osp/app/signin/cq;->c:Lcom/osp/app/signin/BackgroundModeService;

    invoke-static {v0}, Lcom/msc/c/c;->e(Landroid/content/Context;)V

    .line 3321
    iget-object v0, p0, Lcom/osp/app/signin/cq;->c:Lcom/osp/app/signin/BackgroundModeService;

    invoke-static {v0}, Lcom/msc/c/e;->i(Landroid/content/Context;)V

    .line 3323
    iget-object v0, p0, Lcom/osp/app/signin/cq;->d:Lcom/osp/app/util/d;

    iput-boolean v7, v0, Lcom/osp/app/util/d;->b:Z

    .line 3326
    :cond_18
    const-string v0, "Fail"

    iput-object v0, p0, Lcom/osp/app/signin/cq;->e:Ljava/lang/String;

    goto/16 :goto_0

    .line 3327
    :cond_19
    iget-wide v4, p0, Lcom/osp/app/signin/cq;->o:J

    cmp-long v0, v0, v4

    if-nez v0, :cond_0

    .line 3329
    if-eqz v2, :cond_12

    .line 3331
    iget-boolean v0, p0, Lcom/osp/app/signin/cq;->n:Z

    if-nez v0, :cond_1e

    .line 3333
    const-string v0, "AUT_1805"

    iget-object v1, p0, Lcom/osp/app/signin/cq;->b:Lcom/msc/c/f;

    invoke-virtual {v1}, Lcom/msc/c/f;->a()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1a

    const-string v0, "AUT_1004"

    iget-object v1, p0, Lcom/osp/app/signin/cq;->b:Lcom/msc/c/f;

    invoke-virtual {v1}, Lcom/msc/c/f;->a()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1b

    .line 3337
    :cond_1a
    iget-object v0, p0, Lcom/osp/app/signin/cq;->c:Lcom/osp/app/signin/BackgroundModeService;

    invoke-static {v0}, Lcom/msc/c/c;->e(Landroid/content/Context;)V

    .line 3341
    iget-object v0, p0, Lcom/osp/app/signin/cq;->c:Lcom/osp/app/signin/BackgroundModeService;

    invoke-static {v0}, Lcom/msc/c/e;->i(Landroid/content/Context;)V

    .line 3343
    iget-object v0, p0, Lcom/osp/app/signin/cq;->d:Lcom/osp/app/util/d;

    iput-boolean v7, v0, Lcom/osp/app/util/d;->b:Z

    goto/16 :goto_1

    .line 3345
    :cond_1b
    const-string v0, "AUT_1302"

    iget-object v1, p0, Lcom/osp/app/signin/cq;->b:Lcom/msc/c/f;

    invoke-virtual {v1}, Lcom/msc/c/f;->a()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1c

    const-string v0, "AUT_1830"

    iget-object v1, p0, Lcom/osp/app/signin/cq;->b:Lcom/msc/c/f;

    invoke-virtual {v1}, Lcom/msc/c/f;->a()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1d

    .line 3347
    :cond_1c
    iget-object v0, p0, Lcom/osp/app/signin/cq;->c:Lcom/osp/app/signin/BackgroundModeService;

    invoke-static {v0}, Lcom/msc/c/c;->g(Landroid/content/Context;)V

    .line 3348
    iget-object v0, p0, Lcom/osp/app/signin/cq;->c:Lcom/osp/app/signin/BackgroundModeService;

    invoke-static {v0}, Lcom/msc/c/e;->a(Landroid/content/Context;)Z

    .line 3349
    iget-object v0, p0, Lcom/osp/app/signin/cq;->c:Lcom/osp/app/signin/BackgroundModeService;

    invoke-virtual {v0}, Lcom/osp/app/signin/BackgroundModeService;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Landroid/preference/PreferenceManager;->getDefaultSharedPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v0

    .line 3350
    const-string v1, "FLAG_DO_NOT_SHOW_NOTIFICATION"

    invoke-interface {v0, v1, v6}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    .line 3351
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v1, "BMS"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "do not show again noti : "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 3353
    if-nez v0, :cond_0

    .line 3355
    iget-object v0, p0, Lcom/osp/app/signin/cq;->c:Lcom/osp/app/signin/BackgroundModeService;

    iget-object v1, p0, Lcom/osp/app/signin/cq;->d:Lcom/osp/app/util/d;

    invoke-static {v0, v1}, Lcom/osp/app/signin/BackgroundModeService;->a(Lcom/osp/app/signin/BackgroundModeService;Lcom/osp/app/util/d;)V

    .line 3356
    const-string v0, "Fail"

    iput-object v0, p0, Lcom/osp/app/signin/cq;->e:Ljava/lang/String;

    goto/16 :goto_0

    .line 3361
    :cond_1d
    invoke-static {}, Lcom/osp/device/b;->b()Lcom/osp/device/b;

    move-result-object v0

    invoke-virtual {v0}, Lcom/osp/device/b;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    const-string v0, "AUT_1094"

    iget-object v1, p0, Lcom/osp/app/signin/cq;->b:Lcom/msc/c/f;

    invoke-virtual {v1}, Lcom/msc/c/f;->a()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 3363
    iget-object v0, p0, Lcom/osp/app/signin/cq;->c:Lcom/osp/app/signin/BackgroundModeService;

    invoke-virtual {v0}, Lcom/osp/app/signin/BackgroundModeService;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Landroid/preference/PreferenceManager;->getDefaultSharedPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v0

    .line 3364
    const-string v1, "FLAG_DO_NOT_SHOW_NOTIFICATION"

    invoke-interface {v0, v1, v6}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    .line 3365
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v1, "BMS"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "do not show again noti : "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 3367
    if-nez v0, :cond_0

    .line 3369
    iget-object v0, p0, Lcom/osp/app/signin/cq;->c:Lcom/osp/app/signin/BackgroundModeService;

    iget-object v1, p0, Lcom/osp/app/signin/cq;->d:Lcom/osp/app/util/d;

    invoke-static {v0, v1}, Lcom/osp/app/signin/BackgroundModeService;->b(Lcom/osp/app/signin/BackgroundModeService;Lcom/osp/app/util/d;)V

    .line 3370
    const-string v0, "Fail"

    iput-object v0, p0, Lcom/osp/app/signin/cq;->e:Ljava/lang/String;

    goto/16 :goto_0

    .line 3375
    :cond_1e
    const-string v0, "Fail"

    iput-object v0, p0, Lcom/osp/app/signin/cq;->e:Ljava/lang/String;

    goto/16 :goto_0
.end method

.method protected final synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 2747
    invoke-virtual {p0}, Lcom/osp/app/signin/cq;->a()Ljava/lang/Boolean;

    move-result-object v0

    return-object v0
.end method

.method protected final onCancelled()V
    .locals 2

    .prologue
    .line 3458
    invoke-super {p0}, Lcom/msc/c/b;->onCancelled()V

    .line 3460
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v0, "BMS"

    const-string v1, "set sIsServerRequestStart false"

    invoke-static {v0, v1}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 3461
    invoke-static {}, Lcom/osp/app/signin/BackgroundModeService;->a()Z

    .line 3462
    return-void
.end method

.method protected final synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 2747
    check-cast p1, Ljava/lang/Boolean;

    invoke-virtual {p0, p1}, Lcom/osp/app/signin/cq;->a(Ljava/lang/Boolean;)V

    return-void
.end method
