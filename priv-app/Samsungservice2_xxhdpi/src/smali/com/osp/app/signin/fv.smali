.class public final Lcom/osp/app/signin/fv;
.super Ljava/lang/Object;
.source "MarketingPopupInfo.java"


# instance fields
.field private a:Ljava/lang/String;

.field private b:Ljava/lang/String;

.field private c:Ljava/lang/String;

.field private d:Ljava/lang/String;

.field private e:Ljava/lang/String;

.field private f:Ljava/lang/String;

.field private g:Ljava/lang/String;

.field private h:Ljava/lang/String;

.field private i:Ljava/lang/String;

.field private j:Ljava/lang/String;

.field private k:Z

.field private l:Landroid/graphics/Bitmap;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 26
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 27
    const-string v0, ""

    iput-object v0, p0, Lcom/osp/app/signin/fv;->a:Ljava/lang/String;

    .line 28
    const-string v0, ""

    iput-object v0, p0, Lcom/osp/app/signin/fv;->b:Ljava/lang/String;

    .line 29
    const-string v0, ""

    iput-object v0, p0, Lcom/osp/app/signin/fv;->c:Ljava/lang/String;

    .line 30
    const-string v0, ""

    iput-object v0, p0, Lcom/osp/app/signin/fv;->d:Ljava/lang/String;

    .line 31
    const-string v0, ""

    iput-object v0, p0, Lcom/osp/app/signin/fv;->e:Ljava/lang/String;

    .line 32
    const-string v0, ""

    iput-object v0, p0, Lcom/osp/app/signin/fv;->f:Ljava/lang/String;

    .line 33
    const-string v0, ""

    iput-object v0, p0, Lcom/osp/app/signin/fv;->g:Ljava/lang/String;

    .line 34
    const-string v0, ""

    iput-object v0, p0, Lcom/osp/app/signin/fv;->h:Ljava/lang/String;

    .line 35
    const-string v0, ""

    iput-object v0, p0, Lcom/osp/app/signin/fv;->i:Ljava/lang/String;

    .line 36
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/osp/app/signin/fv;->k:Z

    .line 37
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/osp/app/signin/fv;->l:Landroid/graphics/Bitmap;

    .line 38
    return-void
.end method


# virtual methods
.method public final a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 53
    iget-object v0, p0, Lcom/osp/app/signin/fv;->b:Ljava/lang/String;

    return-object v0
.end method

.method public final a(Landroid/graphics/Bitmap;)V
    .locals 0

    .prologue
    .line 141
    iput-object p1, p0, Lcom/osp/app/signin/fv;->l:Landroid/graphics/Bitmap;

    .line 142
    return-void
.end method

.method public final a(Ljava/lang/Boolean;)V
    .locals 1

    .prologue
    .line 137
    invoke-virtual {p1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    iput-boolean v0, p0, Lcom/osp/app/signin/fv;->k:Z

    .line 138
    return-void
.end method

.method public final a(Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 97
    iput-object p1, p0, Lcom/osp/app/signin/fv;->a:Ljava/lang/String;

    .line 98
    return-void
.end method

.method public final b()Ljava/lang/String;
    .locals 1

    .prologue
    .line 57
    iget-object v0, p0, Lcom/osp/app/signin/fv;->c:Ljava/lang/String;

    return-object v0
.end method

.method public final b(Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 101
    iput-object p1, p0, Lcom/osp/app/signin/fv;->b:Ljava/lang/String;

    .line 102
    return-void
.end method

.method public final c()Ljava/lang/String;
    .locals 1

    .prologue
    .line 61
    iget-object v0, p0, Lcom/osp/app/signin/fv;->d:Ljava/lang/String;

    return-object v0
.end method

.method public final c(Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 105
    iput-object p1, p0, Lcom/osp/app/signin/fv;->c:Ljava/lang/String;

    .line 106
    return-void
.end method

.method public final d()Ljava/lang/String;
    .locals 1

    .prologue
    .line 65
    iget-object v0, p0, Lcom/osp/app/signin/fv;->e:Ljava/lang/String;

    return-object v0
.end method

.method public final d(Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 109
    iput-object p1, p0, Lcom/osp/app/signin/fv;->d:Ljava/lang/String;

    .line 110
    return-void
.end method

.method public final e()Ljava/lang/String;
    .locals 1

    .prologue
    .line 69
    iget-object v0, p0, Lcom/osp/app/signin/fv;->f:Ljava/lang/String;

    return-object v0
.end method

.method public final e(Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 113
    iput-object p1, p0, Lcom/osp/app/signin/fv;->e:Ljava/lang/String;

    .line 114
    return-void
.end method

.method public final f()Ljava/lang/String;
    .locals 1

    .prologue
    .line 73
    iget-object v0, p0, Lcom/osp/app/signin/fv;->g:Ljava/lang/String;

    return-object v0
.end method

.method public final f(Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 117
    iput-object p1, p0, Lcom/osp/app/signin/fv;->f:Ljava/lang/String;

    .line 118
    return-void
.end method

.method public final g()Ljava/lang/String;
    .locals 1

    .prologue
    .line 77
    iget-object v0, p0, Lcom/osp/app/signin/fv;->h:Ljava/lang/String;

    return-object v0
.end method

.method public final g(Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 121
    iput-object p1, p0, Lcom/osp/app/signin/fv;->g:Ljava/lang/String;

    .line 122
    return-void
.end method

.method public final h()Ljava/lang/String;
    .locals 1

    .prologue
    .line 81
    iget-object v0, p0, Lcom/osp/app/signin/fv;->i:Ljava/lang/String;

    return-object v0
.end method

.method public final h(Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 125
    iput-object p1, p0, Lcom/osp/app/signin/fv;->h:Ljava/lang/String;

    .line 126
    return-void
.end method

.method public final i()I
    .locals 1

    .prologue
    .line 85
    iget-object v0, p0, Lcom/osp/app/signin/fv;->j:Ljava/lang/String;

    invoke-static {v0}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v0

    return v0
.end method

.method public final i(Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 129
    iput-object p1, p0, Lcom/osp/app/signin/fv;->i:Ljava/lang/String;

    .line 130
    return-void
.end method

.method public final j(Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 133
    iput-object p1, p0, Lcom/osp/app/signin/fv;->j:Ljava/lang/String;

    .line 134
    return-void
.end method

.method public final j()Z
    .locals 1

    .prologue
    .line 89
    iget-boolean v0, p0, Lcom/osp/app/signin/fv;->k:Z

    return v0
.end method

.method public final k()Landroid/graphics/Bitmap;
    .locals 1

    .prologue
    .line 93
    iget-object v0, p0, Lcom/osp/app/signin/fv;->l:Landroid/graphics/Bitmap;

    return-object v0
.end method
