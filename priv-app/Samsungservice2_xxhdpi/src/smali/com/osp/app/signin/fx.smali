.class final Lcom/osp/app/signin/fx;
.super Ljava/lang/Object;
.source "NameValidationWebView.java"

# interfaces
.implements Landroid/content/DialogInterface$OnCancelListener;


# instance fields
.field final synthetic a:Lcom/osp/app/signin/NameValidationWebView;


# direct methods
.method constructor <init>(Lcom/osp/app/signin/NameValidationWebView;)V
    .locals 0

    .prologue
    .line 335
    iput-object p1, p0, Lcom/osp/app/signin/fx;->a:Lcom/osp/app/signin/NameValidationWebView;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onCancel(Landroid/content/DialogInterface;)V
    .locals 3

    .prologue
    .line 339
    iget-object v0, p0, Lcom/osp/app/signin/fx;->a:Lcom/osp/app/signin/NameValidationWebView;

    invoke-static {v0}, Lcom/osp/app/signin/NameValidationWebView;->c(Lcom/osp/app/signin/NameValidationWebView;)Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/osp/app/signin/fx;->a:Lcom/osp/app/signin/NameValidationWebView;

    invoke-static {v0}, Lcom/osp/app/signin/NameValidationWebView;->c(Lcom/osp/app/signin/NameValidationWebView;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v0

    if-lez v0, :cond_0

    iget-object v0, p0, Lcom/osp/app/signin/fx;->a:Lcom/osp/app/signin/NameValidationWebView;

    invoke-static {v0}, Lcom/osp/app/signin/NameValidationWebView;->c(Lcom/osp/app/signin/NameValidationWebView;)Ljava/lang/String;

    move-result-object v0

    const-string v1, "true"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 341
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v0, "NVWV"

    const-string v1, "showInfoDialog - canceled"

    invoke-static {v0, v1}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 351
    :goto_0
    iget-object v0, p0, Lcom/osp/app/signin/fx;->a:Lcom/osp/app/signin/NameValidationWebView;

    invoke-static {v0}, Lcom/osp/app/signin/NameValidationWebView;->d(Lcom/osp/app/signin/NameValidationWebView;)V

    .line 352
    return-void

    .line 344
    :cond_0
    iget-object v0, p0, Lcom/osp/app/signin/fx;->a:Lcom/osp/app/signin/NameValidationWebView;

    invoke-virtual {v0}, Lcom/osp/app/signin/NameValidationWebView;->getIntent()Landroid/content/Intent;

    move-result-object v0

    .line 345
    if-eqz v0, :cond_1

    const-string v1, "is_cancelable_just_one_activity"

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 347
    iget-object v1, p0, Lcom/osp/app/signin/fx;->a:Lcom/osp/app/signin/NameValidationWebView;

    const/16 v2, 0xe

    invoke-virtual {v1, v2, v0}, Lcom/osp/app/signin/NameValidationWebView;->a(ILandroid/content/Intent;)V

    .line 349
    :cond_1
    iget-object v0, p0, Lcom/osp/app/signin/fx;->a:Lcom/osp/app/signin/NameValidationWebView;

    invoke-virtual {v0}, Lcom/osp/app/signin/NameValidationWebView;->finish()V

    goto :goto_0
.end method
