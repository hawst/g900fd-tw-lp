.class public Lcom/osp/app/signin/EmailValidationView;
.super Lcom/osp/app/util/BaseActivity;
.source "EmailValidationView.java"


# static fields
.field static c:I


# instance fields
.field private A:I

.field private B:Ljava/lang/String;

.field private C:Landroid/widget/TextView;

.field private D:Landroid/widget/TextView;

.field private E:Landroid/widget/TextView;

.field private F:Landroid/widget/TextView;

.field private G:Landroid/widget/TextView;

.field private H:Landroid/widget/TextView;

.field private I:Landroid/widget/TextView;

.field private J:Z

.field private K:Z

.field private L:Z

.field private M:Z

.field private N:Ljava/lang/String;

.field private O:Lcom/osp/app/signin/BottomSoftkeyLinearLayout;

.field private final P:Landroid/view/View$OnClickListener;

.field private final Q:Landroid/view/View$OnClickListener;

.field private R:Landroid/view/View;

.field private S:Z

.field private T:J

.field private U:Lcom/osp/app/signin/fl;

.field private V:Z

.field private W:Z

.field protected a:I

.field b:Landroid/app/AlertDialog;

.field private d:Ljava/lang/String;

.field private e:Ljava/lang/String;

.field private f:Ljava/lang/String;

.field private y:Ljava/lang/String;

.field private z:Landroid/content/Intent;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 286
    const/4 v0, 0x0

    sput v0, Lcom/osp/app/signin/EmailValidationView;->c:I

    return-void
.end method

.method public constructor <init>()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x0

    .line 55
    invoke-direct {p0}, Lcom/osp/app/util/BaseActivity;-><init>()V

    .line 228
    const/16 v0, 0xc8

    iput v0, p0, Lcom/osp/app/signin/EmailValidationView;->A:I

    .line 243
    iput-boolean v1, p0, Lcom/osp/app/signin/EmailValidationView;->J:Z

    .line 244
    iput-boolean v1, p0, Lcom/osp/app/signin/EmailValidationView;->K:Z

    .line 252
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/osp/app/signin/EmailValidationView;->L:Z

    .line 253
    iput-boolean v1, p0, Lcom/osp/app/signin/EmailValidationView;->M:Z

    .line 254
    iput-object v2, p0, Lcom/osp/app/signin/EmailValidationView;->N:Ljava/lang/String;

    .line 260
    new-instance v0, Lcom/osp/app/signin/ev;

    invoke-direct {v0, p0}, Lcom/osp/app/signin/ev;-><init>(Lcom/osp/app/signin/EmailValidationView;)V

    iput-object v0, p0, Lcom/osp/app/signin/EmailValidationView;->P:Landroid/view/View$OnClickListener;

    .line 271
    new-instance v0, Lcom/osp/app/signin/fb;

    invoke-direct {v0, p0}, Lcom/osp/app/signin/fb;-><init>(Lcom/osp/app/signin/EmailValidationView;)V

    iput-object v0, p0, Lcom/osp/app/signin/EmailValidationView;->Q:Landroid/view/View$OnClickListener;

    .line 284
    iput-object v2, p0, Lcom/osp/app/signin/EmailValidationView;->R:Landroid/view/View;

    .line 285
    iput-object v2, p0, Lcom/osp/app/signin/EmailValidationView;->b:Landroid/app/AlertDialog;

    .line 287
    iput-boolean v1, p0, Lcom/osp/app/signin/EmailValidationView;->S:Z

    .line 920
    return-void
.end method

.method static synthetic a(Lcom/osp/app/signin/EmailValidationView;Landroid/content/Intent;)Landroid/content/Intent;
    .locals 0

    .prologue
    .line 55
    iput-object p1, p0, Lcom/osp/app/signin/EmailValidationView;->z:Landroid/content/Intent;

    return-object p1
.end method

.method static synthetic a(Lcom/osp/app/signin/EmailValidationView;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 55
    iget-object v0, p0, Lcom/osp/app/signin/EmailValidationView;->y:Ljava/lang/String;

    return-object v0
.end method

.method private a(I)V
    .locals 4

    .prologue
    const v3, 0x7f08011e

    .line 789
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "EmailValidation::initLayoutParams orientation : "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;)V

    .line 791
    invoke-static {}, Lcom/osp/device/b;->b()Lcom/osp/device/b;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/osp/device/b;->i(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 793
    invoke-virtual {p0}, Lcom/osp/app/signin/EmailValidationView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f08011f

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v0

    float-to-int v0, v0

    .line 794
    invoke-virtual {p0}, Lcom/osp/app/signin/EmailValidationView;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1, v3}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v1

    float-to-int v1, v1

    .line 795
    invoke-static {}, Lcom/osp/app/signin/SamsungService;->a()Z

    move-result v2

    if-nez v2, :cond_0

    invoke-static {p0}, Lcom/osp/app/util/r;->g(Landroid/content/Context;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 797
    invoke-virtual {p0}, Lcom/osp/app/signin/EmailValidationView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0, v3}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v0

    float-to-int v0, v0

    .line 800
    :cond_0
    const v2, 0x7f0c00d2

    invoke-virtual {p0, v2}, Lcom/osp/app/signin/EmailValidationView;->findViewById(I)Landroid/view/View;

    move-result-object v2

    .line 801
    invoke-static {}, Lcom/osp/app/util/q;->a()Lcom/osp/app/util/q;

    invoke-static {v2, v0, v1, p1}, Lcom/osp/app/util/q;->b(Landroid/view/View;III)V

    .line 803
    :cond_1
    return-void
.end method

.method private static a(Landroid/widget/TextView;Ljava/lang/CharSequence;Landroid/view/View$OnClickListener;)V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 1311
    new-instance v0, Landroid/text/SpannableString;

    invoke-direct {v0, p1}, Landroid/text/SpannableString;-><init>(Ljava/lang/CharSequence;)V

    .line 1314
    new-instance v1, Landroid/text/style/URLSpan;

    const-string v2, ""

    invoke-direct {v1, v2}, Landroid/text/style/URLSpan;-><init>(Ljava/lang/String;)V

    .line 1315
    invoke-virtual {v0}, Landroid/text/SpannableString;->length()I

    move-result v2

    invoke-virtual {v0, v1, v3, v2, v3}, Landroid/text/SpannableString;->setSpan(Ljava/lang/Object;III)V

    .line 1316
    invoke-virtual {p0, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 1317
    invoke-virtual {p0, p2}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1318
    return-void
.end method

.method static synthetic a(Lcom/osp/app/signin/EmailValidationView;I)V
    .locals 4

    .prologue
    const v3, 0x7f0900ff

    const v2, 0x7f09006b

    .line 55
    invoke-direct {p0}, Lcom/osp/app/signin/EmailValidationView;->e()Z

    move-result v0

    if-eqz v0, :cond_1

    sput p1, Lcom/osp/app/signin/EmailValidationView;->c:I

    invoke-direct {p0}, Lcom/osp/app/signin/EmailValidationView;->d()V

    :cond_0
    :goto_0
    return-void

    :cond_1
    const v0, 0x7f09004f

    invoke-virtual {p0, v0}, Lcom/osp/app/signin/EmailValidationView;->setTitle(I)V

    if-nez p1, :cond_4

    invoke-static {}, Lcom/osp/device/b;->b()Lcom/osp/device/b;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/osp/device/b;->i(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/osp/app/signin/EmailValidationView;->x:Landroid/view/Menu;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/osp/app/signin/EmailValidationView;->x:Landroid/view/Menu;

    invoke-interface {v0}, Landroid/view/Menu;->clear()V

    :cond_2
    const-string v0, "verify"

    invoke-virtual {p0, v0}, Lcom/osp/app/signin/EmailValidationView;->e(Ljava/lang/String;)V

    invoke-virtual {p0}, Lcom/osp/app/signin/EmailValidationView;->l()V

    :cond_3
    iget-object v0, p0, Lcom/osp/app/signin/EmailValidationView;->O:Lcom/osp/app/signin/BottomSoftkeyLinearLayout;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/osp/app/signin/EmailValidationView;->O:Lcom/osp/app/signin/BottomSoftkeyLinearLayout;

    const-string v1, "one_button"

    invoke-virtual {v0, v1}, Lcom/osp/app/signin/BottomSoftkeyLinearLayout;->a(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/osp/app/signin/EmailValidationView;->O:Lcom/osp/app/signin/BottomSoftkeyLinearLayout;

    invoke-virtual {p0, v3}, Lcom/osp/app/signin/EmailValidationView;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/osp/app/signin/BottomSoftkeyLinearLayout;->c(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/osp/app/signin/EmailValidationView;->O:Lcom/osp/app/signin/BottomSoftkeyLinearLayout;

    invoke-virtual {p0, v3}, Lcom/osp/app/signin/EmailValidationView;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/osp/app/signin/BottomSoftkeyLinearLayout;->e(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/osp/app/signin/EmailValidationView;->O:Lcom/osp/app/signin/BottomSoftkeyLinearLayout;

    iget-object v1, p0, Lcom/osp/app/signin/EmailValidationView;->Q:Landroid/view/View$OnClickListener;

    invoke-virtual {v0, v1}, Lcom/osp/app/signin/BottomSoftkeyLinearLayout;->b(Landroid/view/View$OnClickListener;)V

    goto :goto_0

    :cond_4
    iget-object v0, p0, Lcom/osp/app/signin/EmailValidationView;->O:Lcom/osp/app/signin/BottomSoftkeyLinearLayout;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/osp/app/signin/EmailValidationView;->O:Lcom/osp/app/signin/BottomSoftkeyLinearLayout;

    const-string v1, "one_button"

    invoke-virtual {v0, v1}, Lcom/osp/app/signin/BottomSoftkeyLinearLayout;->a(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/osp/app/signin/EmailValidationView;->O:Lcom/osp/app/signin/BottomSoftkeyLinearLayout;

    invoke-virtual {p0, v2}, Lcom/osp/app/signin/EmailValidationView;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/osp/app/signin/BottomSoftkeyLinearLayout;->c(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/osp/app/signin/EmailValidationView;->O:Lcom/osp/app/signin/BottomSoftkeyLinearLayout;

    invoke-virtual {p0, v2}, Lcom/osp/app/signin/EmailValidationView;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/osp/app/signin/BottomSoftkeyLinearLayout;->e(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/osp/app/signin/EmailValidationView;->O:Lcom/osp/app/signin/BottomSoftkeyLinearLayout;

    iget-object v1, p0, Lcom/osp/app/signin/EmailValidationView;->P:Landroid/view/View$OnClickListener;

    invoke-virtual {v0, v1}, Lcom/osp/app/signin/BottomSoftkeyLinearLayout;->b(Landroid/view/View$OnClickListener;)V

    goto :goto_0
.end method

.method static synthetic a(Lcom/osp/app/signin/EmailValidationView;Ljava/lang/String;)V
    .locals 3

    .prologue
    .line 55
    new-instance v0, Landroid/content/Intent;

    const-string v1, "com.msc.action.samsungaccount.resignin"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    const-string v1, "client_id"

    const-string v2, "j5p7ll8g33"

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    const-string v1, "client_secret"

    const-string v2, "5763D0052DC1462E13751F753384E9A9"

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    const-string v1, "OSP_VER"

    const-string v2, "OSP_02"

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    if-eqz p1, :cond_0

    const-string v1, "email_id"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    :cond_0
    invoke-virtual {p0, v0}, Lcom/osp/app/signin/EmailValidationView;->startActivity(Landroid/content/Intent;)V

    return-void
.end method

.method private a(Z)V
    .locals 6

    .prologue
    const v5, 0x7f0c00cc

    const v4, 0x7f0c00cb

    const v3, 0x7f0c00ca

    const/16 v2, 0x8

    const/4 v1, 0x0

    .line 608
    invoke-direct {p0}, Lcom/osp/app/signin/EmailValidationView;->e()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 610
    if-eqz p1, :cond_0

    .line 612
    iget-object v0, p0, Lcom/osp/app/signin/EmailValidationView;->R:Landroid/view/View;

    invoke-virtual {v0, v5}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 613
    iget-object v0, p0, Lcom/osp/app/signin/EmailValidationView;->R:Landroid/view/View;

    invoke-virtual {v0, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    .line 614
    iget-object v0, p0, Lcom/osp/app/signin/EmailValidationView;->R:Landroid/view/View;

    invoke-virtual {v0, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    .line 637
    :goto_0
    return-void

    .line 618
    :cond_0
    iget-object v0, p0, Lcom/osp/app/signin/EmailValidationView;->R:Landroid/view/View;

    invoke-virtual {v0, v5}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    .line 619
    iget-object v0, p0, Lcom/osp/app/signin/EmailValidationView;->R:Landroid/view/View;

    invoke-virtual {v0, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 620
    iget-object v0, p0, Lcom/osp/app/signin/EmailValidationView;->R:Landroid/view/View;

    invoke-virtual {v0, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    goto :goto_0

    .line 624
    :cond_1
    if-eqz p1, :cond_2

    .line 626
    iget-object v0, p0, Lcom/osp/app/signin/EmailValidationView;->C:Landroid/widget/TextView;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    .line 627
    iget-object v0, p0, Lcom/osp/app/signin/EmailValidationView;->E:Landroid/widget/TextView;

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setVisibility(I)V

    .line 628
    iget-object v0, p0, Lcom/osp/app/signin/EmailValidationView;->F:Landroid/widget/TextView;

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setVisibility(I)V

    goto :goto_0

    .line 631
    :cond_2
    iget-object v0, p0, Lcom/osp/app/signin/EmailValidationView;->C:Landroid/widget/TextView;

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setVisibility(I)V

    .line 632
    iget-object v0, p0, Lcom/osp/app/signin/EmailValidationView;->E:Landroid/widget/TextView;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    .line 633
    iget-object v0, p0, Lcom/osp/app/signin/EmailValidationView;->F:Landroid/widget/TextView;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    goto :goto_0
.end method

.method static synthetic a(Lcom/osp/app/signin/EmailValidationView;Z)Z
    .locals 0

    .prologue
    .line 55
    iput-boolean p1, p0, Lcom/osp/app/signin/EmailValidationView;->L:Z

    return p1
.end method

.method static synthetic b(Lcom/osp/app/signin/EmailValidationView;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 55
    iget-object v0, p0, Lcom/osp/app/signin/EmailValidationView;->d:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic b(Lcom/osp/app/signin/EmailValidationView;Ljava/lang/String;)Ljava/lang/String;
    .locals 0

    .prologue
    .line 55
    iput-object p1, p0, Lcom/osp/app/signin/EmailValidationView;->N:Ljava/lang/String;

    return-object p1
.end method

.method static synthetic b(Lcom/osp/app/signin/EmailValidationView;Z)V
    .locals 0

    .prologue
    .line 55
    invoke-direct {p0, p1}, Lcom/osp/app/signin/EmailValidationView;->a(Z)V

    return-void
.end method

.method static synthetic c(Lcom/osp/app/signin/EmailValidationView;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 55
    iget-object v0, p0, Lcom/osp/app/signin/EmailValidationView;->B:Ljava/lang/String;

    return-object v0
.end method

.method private c()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    .line 344
    iget-object v0, p0, Lcom/osp/app/signin/EmailValidationView;->N:Ljava/lang/String;

    if-eqz v0, :cond_0

    .line 346
    iget-object v0, p0, Lcom/osp/app/signin/EmailValidationView;->N:Ljava/lang/String;

    new-instance v1, Landroid/content/Intent;

    const-string v2, "com.msc.action.samsungaccount.web_dialog"

    invoke-direct {v1, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    const-string v2, "ServerUrl"

    invoke-virtual {v1, v2, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    invoke-virtual {p0, v1}, Lcom/osp/app/signin/EmailValidationView;->startActivity(Landroid/content/Intent;)V

    .line 358
    :goto_0
    return-void

    .line 349
    :cond_0
    new-instance v0, Landroid/content/Intent;

    const-string v1, "android.intent.action.VIEW"

    const-string v2, "about:blank"

    invoke-static {v2}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;Landroid/net/Uri;)V

    .line 350
    invoke-static {v0, p0}, Lcom/osp/app/util/ad;->a(Landroid/content/Intent;Landroid/content/Context;)Z

    move-result v1

    if-ne v1, v3, :cond_1

    .line 352
    invoke-virtual {p0, v0}, Lcom/osp/app/signin/EmailValidationView;->startActivity(Landroid/content/Intent;)V

    goto :goto_0

    .line 355
    :cond_1
    invoke-static {}, Lcom/osp/app/util/ap;->a()Lcom/osp/app/util/ap;

    const v0, 0x7f09004a

    invoke-virtual {p0, v0}, Lcom/osp/app/signin/EmailValidationView;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-static {p0, v0, v3}, Lcom/osp/app/util/ap;->a(Landroid/content/Context;Ljava/lang/String;I)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    goto :goto_0
.end method

.method private d()V
    .locals 1

    .prologue
    .line 1298
    invoke-direct {p0}, Lcom/osp/app/signin/EmailValidationView;->e()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/osp/app/signin/EmailValidationView;->isFinishing()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/osp/app/signin/EmailValidationView;->b:Landroid/app/AlertDialog;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/osp/app/signin/EmailValidationView;->b:Landroid/app/AlertDialog;

    invoke-virtual {v0}, Landroid/app/AlertDialog;->isShowing()Z

    move-result v0

    if-nez v0, :cond_0

    .line 1300
    iget-object v0, p0, Lcom/osp/app/signin/EmailValidationView;->b:Landroid/app/AlertDialog;

    invoke-virtual {v0}, Landroid/app/AlertDialog;->show()V

    .line 1302
    :cond_0
    return-void
.end method

.method static synthetic d(Lcom/osp/app/signin/EmailValidationView;)Z
    .locals 1

    .prologue
    .line 55
    iget-boolean v0, p0, Lcom/osp/app/signin/EmailValidationView;->L:Z

    return v0
.end method

.method private e()Z
    .locals 1

    .prologue
    .line 1327
    iget-boolean v0, p0, Lcom/osp/app/signin/EmailValidationView;->S:Z

    if-eqz v0, :cond_0

    .line 1329
    const/4 v0, 0x1

    .line 1332
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method static synthetic e(Lcom/osp/app/signin/EmailValidationView;)Z
    .locals 1

    .prologue
    .line 55
    invoke-direct {p0}, Lcom/osp/app/signin/EmailValidationView;->e()Z

    move-result v0

    return v0
.end method

.method static synthetic f(Lcom/osp/app/signin/EmailValidationView;)V
    .locals 0

    .prologue
    .line 55
    invoke-direct {p0}, Lcom/osp/app/signin/EmailValidationView;->d()V

    return-void
.end method

.method static synthetic g(Lcom/osp/app/signin/EmailValidationView;)Z
    .locals 1

    .prologue
    .line 55
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/osp/app/signin/EmailValidationView;->M:Z

    return v0
.end method

.method static synthetic h(Lcom/osp/app/signin/EmailValidationView;)V
    .locals 0

    .prologue
    .line 55
    invoke-direct {p0}, Lcom/osp/app/signin/EmailValidationView;->c()V

    return-void
.end method

.method static synthetic i(Lcom/osp/app/signin/EmailValidationView;)Landroid/content/Intent;
    .locals 1

    .prologue
    .line 55
    iget-object v0, p0, Lcom/osp/app/signin/EmailValidationView;->z:Landroid/content/Intent;

    return-object v0
.end method

.method static synthetic j(Lcom/osp/app/signin/EmailValidationView;)V
    .locals 2

    .prologue
    .line 55
    invoke-static {p0}, Lcom/osp/app/util/ad;->a(Landroid/content/Context;)Z

    move-result v0

    if-nez v0, :cond_1

    invoke-static {}, Lcom/osp/app/util/ap;->a()Lcom/osp/app/util/ap;

    const v0, 0x7f09003e

    invoke-virtual {p0, v0}, Lcom/osp/app/signin/EmailValidationView;->getString(I)Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x0

    invoke-static {p0, v0, v1}, Lcom/osp/app/util/ap;->a(Landroid/content/Context;Ljava/lang/String;I)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    :cond_0
    :goto_0
    return-void

    :cond_1
    invoke-direct {p0}, Lcom/osp/app/signin/EmailValidationView;->e()Z

    move-result v0

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/osp/app/signin/EmailValidationView;->b:Landroid/app/AlertDialog;

    invoke-virtual {v0}, Landroid/app/AlertDialog;->dismiss()V

    :cond_2
    iget-object v0, p0, Lcom/osp/app/signin/EmailValidationView;->U:Lcom/osp/app/signin/fl;

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/osp/app/signin/EmailValidationView;->U:Lcom/osp/app/signin/fl;

    invoke-virtual {v0}, Lcom/osp/app/signin/fl;->getStatus()Landroid/os/AsyncTask$Status;

    move-result-object v0

    sget-object v1, Landroid/os/AsyncTask$Status;->RUNNING:Landroid/os/AsyncTask$Status;

    if-eq v0, v1, :cond_0

    :cond_3
    new-instance v0, Lcom/osp/app/signin/fl;

    invoke-direct {v0, p0, p0}, Lcom/osp/app/signin/fl;-><init>(Lcom/osp/app/signin/EmailValidationView;Landroid/content/Context;)V

    iput-object v0, p0, Lcom/osp/app/signin/EmailValidationView;->U:Lcom/osp/app/signin/fl;

    iget-object v0, p0, Lcom/osp/app/signin/EmailValidationView;->U:Lcom/osp/app/signin/fl;

    invoke-virtual {v0}, Lcom/osp/app/signin/fl;->b()V

    goto :goto_0
.end method

.method static synthetic k(Lcom/osp/app/signin/EmailValidationView;)I
    .locals 1

    .prologue
    .line 55
    iget v0, p0, Lcom/osp/app/signin/EmailValidationView;->A:I

    return v0
.end method

.method static synthetic l(Lcom/osp/app/signin/EmailValidationView;)V
    .locals 2

    .prologue
    .line 55
    const/16 v0, 0xc

    iget-object v1, p0, Lcom/osp/app/signin/EmailValidationView;->z:Landroid/content/Intent;

    invoke-virtual {p0, v0, v1}, Lcom/osp/app/signin/EmailValidationView;->a(ILandroid/content/Intent;)V

    invoke-virtual {p0}, Lcom/osp/app/signin/EmailValidationView;->finish()V

    return-void
.end method

.method static synthetic m(Lcom/osp/app/signin/EmailValidationView;)V
    .locals 8

    .prologue
    .line 55
    iget v0, p0, Lcom/osp/app/signin/EmailValidationView;->a:I

    and-int/lit8 v0, v0, 0x8

    const/16 v1, 0x8

    if-ne v0, v1, :cond_0

    iget v0, p0, Lcom/osp/app/signin/EmailValidationView;->a:I

    xor-int/lit8 v0, v0, 0x8

    iput v0, p0, Lcom/osp/app/signin/EmailValidationView;->a:I

    invoke-static {}, Lcom/osp/app/signin/SamsungService;->p()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-static {p0}, Lcom/msc/openprovider/b;->f(Landroid/content/Context;)V

    :cond_0
    iget v0, p0, Lcom/osp/app/signin/EmailValidationView;->a:I

    if-lez v0, :cond_2

    iget v1, p0, Lcom/osp/app/signin/EmailValidationView;->a:I

    invoke-static {p0}, Lcom/msc/c/e;->h(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v2

    iget-object v3, p0, Lcom/osp/app/signin/EmailValidationView;->e:Ljava/lang/String;

    iget-object v4, p0, Lcom/osp/app/signin/EmailValidationView;->f:Ljava/lang/String;

    iget-boolean v5, p0, Lcom/osp/app/signin/EmailValidationView;->W:Z

    const/4 v6, 0x0

    const/4 v7, 0x0

    move-object v0, p0

    invoke-static/range {v0 .. v7}, Lcom/osp/app/util/ad;->a(Landroid/content/Context;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;ZZLjava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    iget-boolean v1, p0, Lcom/osp/app/signin/EmailValidationView;->V:Z

    if-eqz v1, :cond_1

    const-string v1, "key_return_result"

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    const/16 v1, 0xdc

    invoke-virtual {p0, v0, v1}, Lcom/osp/app/signin/EmailValidationView;->startActivityForResult(Landroid/content/Intent;I)V

    :goto_0
    return-void

    :cond_1
    invoke-virtual {p0, v0}, Lcom/osp/app/signin/EmailValidationView;->startActivity(Landroid/content/Intent;)V

    :cond_2
    iget-boolean v0, p0, Lcom/osp/app/signin/EmailValidationView;->V:Z

    if-eqz v0, :cond_3

    const/4 v0, -0x1

    iget-object v1, p0, Lcom/osp/app/signin/EmailValidationView;->z:Landroid/content/Intent;

    invoke-virtual {p0, v0, v1}, Lcom/osp/app/signin/EmailValidationView;->a(ILandroid/content/Intent;)V

    :goto_1
    invoke-virtual {p0}, Lcom/osp/app/signin/EmailValidationView;->finish()V

    goto :goto_0

    :cond_3
    const/16 v0, 0xc

    iget-object v1, p0, Lcom/osp/app/signin/EmailValidationView;->z:Landroid/content/Intent;

    invoke-virtual {p0, v0, v1}, Lcom/osp/app/signin/EmailValidationView;->a(ILandroid/content/Intent;)V

    goto :goto_1
.end method

.method static synthetic n(Lcom/osp/app/signin/EmailValidationView;)Z
    .locals 1

    .prologue
    .line 55
    iget-boolean v0, p0, Lcom/osp/app/signin/EmailValidationView;->J:Z

    return v0
.end method

.method static synthetic o(Lcom/osp/app/signin/EmailValidationView;)Z
    .locals 1

    .prologue
    .line 55
    iget-boolean v0, p0, Lcom/osp/app/signin/EmailValidationView;->K:Z

    return v0
.end method


# virtual methods
.method protected final a()V
    .locals 0

    .prologue
    .line 1011
    return-void
.end method

.method protected final b()V
    .locals 0

    .prologue
    .line 1016
    return-void
.end method

.method protected onActivityResult(IILandroid/content/Intent;)V
    .locals 1

    .prologue
    .line 515
    invoke-super {p0, p1, p2, p3}, Lcom/osp/app/util/BaseActivity;->onActivityResult(IILandroid/content/Intent;)V

    .line 517
    const/16 v0, 0xdc

    if-ne p1, v0, :cond_0

    .line 519
    invoke-virtual {p0, p2, p3}, Lcom/osp/app/signin/EmailValidationView;->a(ILandroid/content/Intent;)V

    .line 521
    invoke-virtual {p0}, Lcom/osp/app/signin/EmailValidationView;->finish()V

    .line 523
    :cond_0
    return-void
.end method

.method public onBackPressed()V
    .locals 2

    .prologue
    .line 767
    const/4 v0, 0x0

    iget-object v1, p0, Lcom/osp/app/signin/EmailValidationView;->z:Landroid/content/Intent;

    invoke-virtual {p0, v0, v1}, Lcom/osp/app/signin/EmailValidationView;->a(ILandroid/content/Intent;)V

    .line 769
    invoke-super {p0}, Lcom/osp/app/util/BaseActivity;->onBackPressed()V

    .line 770
    return-void
.end method

.method public onConfigurationChanged(Landroid/content/res/Configuration;)V
    .locals 2

    .prologue
    .line 775
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v0, "EVV"

    const-string v1, "onConfigurationChanged()"

    invoke-static {v0, v1}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 777
    iget v0, p1, Landroid/content/res/Configuration;->orientation:I

    invoke-direct {p0, v0}, Lcom/osp/app/signin/EmailValidationView;->a(I)V

    .line 779
    invoke-super {p0, p1}, Lcom/osp/app/util/BaseActivity;->onConfigurationChanged(Landroid/content/res/Configuration;)V

    .line 780
    return-void
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 13

    .prologue
    const v12, 0x7f070020

    const v11, 0x7f070014

    const/4 v10, 0x1

    const v4, 0x7f070004

    const/4 v9, 0x0

    .line 396
    invoke-super {p0, p1}, Lcom/osp/app/util/BaseActivity;->onCreate(Landroid/os/Bundle;)V

    .line 398
    invoke-virtual {p0}, Lcom/osp/app/signin/EmailValidationView;->getIntent()Landroid/content/Intent;

    move-result-object v0

    iput-object v0, p0, Lcom/osp/app/signin/EmailValidationView;->z:Landroid/content/Intent;

    .line 399
    iget-object v0, p0, Lcom/osp/app/signin/EmailValidationView;->z:Landroid/content/Intent;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/osp/app/signin/EmailValidationView;->z:Landroid/content/Intent;

    invoke-virtual {v0}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/osp/app/signin/EmailValidationView;->z:Landroid/content/Intent;

    invoke-virtual {v0}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    const-string v1, "com.msc.action.samsungaccount.emailvalidate_external"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 401
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v0, "external value check START"

    invoke-static {v0}, Lcom/osp/app/util/aq;->b(Ljava/lang/String;)V

    .line 403
    const/4 v0, 0x6

    new-array v0, v0, [Ljava/lang/String;

    const-string v1, "client_id"

    aput-object v1, v0, v9

    const-string v1, "client_secret"

    aput-object v1, v0, v10

    const/4 v1, 0x2

    const-string v2, "email_id"

    aput-object v2, v0, v1

    const/4 v1, 0x3

    const-string v2, "is_resend"

    aput-object v2, v0, v1

    const/4 v1, 0x4

    const-string v2, "check_list"

    aput-object v2, v0, v1

    const/4 v1, 0x5

    const-string v2, "key_no_notification"

    aput-object v2, v0, v1

    .line 406
    invoke-virtual {p0, v0}, Lcom/osp/app/signin/EmailValidationView;->a([Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 511
    :goto_0
    return-void

    .line 412
    :cond_0
    invoke-static {p0}, Lcom/osp/app/util/ad;->d(Landroid/content/Context;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 414
    invoke-virtual {p0, v10}, Lcom/osp/app/signin/EmailValidationView;->b(I)V

    .line 415
    invoke-virtual {p0}, Lcom/osp/app/signin/EmailValidationView;->finish()V

    .line 417
    :cond_1
    iget-object v0, p0, Lcom/osp/app/signin/EmailValidationView;->z:Landroid/content/Intent;

    if-eqz v0, :cond_14

    iget-object v0, p0, Lcom/osp/app/signin/EmailValidationView;->z:Landroid/content/Intent;

    const-string v1, "email_id"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    :goto_1
    iput-object v0, p0, Lcom/osp/app/signin/EmailValidationView;->y:Ljava/lang/String;

    .line 418
    iget-object v0, p0, Lcom/osp/app/signin/EmailValidationView;->z:Landroid/content/Intent;

    if-eqz v0, :cond_15

    iget-object v0, p0, Lcom/osp/app/signin/EmailValidationView;->z:Landroid/content/Intent;

    const-string v1, "check_list"

    invoke-virtual {v0, v1, v9}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    :goto_2
    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    iput v0, p0, Lcom/osp/app/signin/EmailValidationView;->a:I

    .line 420
    iget-object v0, p0, Lcom/osp/app/signin/EmailValidationView;->z:Landroid/content/Intent;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/osp/app/signin/EmailValidationView;->y:Ljava/lang/String;

    if-nez v0, :cond_2

    const-string v0, "com.msc.action.samsungaccount.emailvalidate"

    iget-object v1, p0, Lcom/osp/app/signin/EmailValidationView;->z:Landroid/content/Intent;

    invoke-virtual {v1}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 422
    invoke-static {p0}, Lcom/osp/app/util/ad;->g(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/osp/app/signin/EmailValidationView;->y:Ljava/lang/String;

    .line 425
    :cond_2
    iget-object v0, p0, Lcom/osp/app/signin/EmailValidationView;->z:Landroid/content/Intent;

    const-string v1, "is_bgmode"

    const/16 v2, 0xc8

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v0

    iput v0, p0, Lcom/osp/app/signin/EmailValidationView;->A:I

    .line 427
    iget-object v0, p0, Lcom/osp/app/signin/EmailValidationView;->z:Landroid/content/Intent;

    const-string v1, "key_verifypopup_mode"

    invoke-virtual {v0, v1, v9}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v0

    iput-boolean v0, p0, Lcom/osp/app/signin/EmailValidationView;->S:Z

    .line 430
    iget-object v0, p0, Lcom/osp/app/signin/EmailValidationView;->z:Landroid/content/Intent;

    const-string v1, "key app id"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/osp/app/signin/EmailValidationView;->d:Ljava/lang/String;

    .line 433
    iget-object v0, p0, Lcom/osp/app/signin/EmailValidationView;->z:Landroid/content/Intent;

    const-string v1, "client_id"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/osp/app/signin/EmailValidationView;->e:Ljava/lang/String;

    .line 435
    iget-object v0, p0, Lcom/osp/app/signin/EmailValidationView;->z:Landroid/content/Intent;

    const-string v1, "client_secret"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/osp/app/signin/EmailValidationView;->f:Ljava/lang/String;

    .line 436
    iget-object v0, p0, Lcom/osp/app/signin/EmailValidationView;->z:Landroid/content/Intent;

    const-string v1, "BG_WhoareU"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 437
    const-string v1, "BG_mode"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 439
    iget-object v0, p0, Lcom/osp/app/signin/EmailValidationView;->z:Landroid/content/Intent;

    const-string v1, "mypackage"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/osp/app/signin/EmailValidationView;->B:Ljava/lang/String;

    .line 440
    iget-object v0, p0, Lcom/osp/app/signin/EmailValidationView;->z:Landroid/content/Intent;

    const-string v1, "key_request_id"

    const-wide/16 v2, 0x0

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/Intent;->getLongExtra(Ljava/lang/String;J)J

    move-result-wide v0

    iput-wide v0, p0, Lcom/osp/app/signin/EmailValidationView;->T:J

    .line 443
    :cond_3
    iget-object v0, p0, Lcom/osp/app/signin/EmailValidationView;->z:Landroid/content/Intent;

    const-string v1, "key_return_result"

    invoke-virtual {v0, v1, v9}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v0

    iput-boolean v0, p0, Lcom/osp/app/signin/EmailValidationView;->V:Z

    .line 444
    iget-object v0, p0, Lcom/osp/app/signin/EmailValidationView;->z:Landroid/content/Intent;

    const-string v1, "key_no_notification"

    invoke-virtual {v0, v1, v9}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v0

    iput-boolean v0, p0, Lcom/osp/app/signin/EmailValidationView;->W:Z

    .line 446
    invoke-direct {p0}, Lcom/osp/app/signin/EmailValidationView;->e()Z

    move-result v0

    if-eqz v0, :cond_16

    .line 448
    invoke-virtual {p0, v10}, Lcom/osp/app/signin/EmailValidationView;->d(I)V

    .line 449
    invoke-virtual {p0}, Lcom/osp/app/signin/EmailValidationView;->k()V

    .line 450
    invoke-static {p0}, Lcom/osp/app/signin/EmailValidationView;->a(Landroid/app/Activity;)V

    .line 452
    const v0, 0x7f030016

    invoke-virtual {p0, v0}, Lcom/osp/app/signin/EmailValidationView;->setContentView(I)V

    .line 459
    :goto_3
    invoke-virtual {p0}, Lcom/osp/app/signin/EmailValidationView;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-direct {p0}, Lcom/osp/app/signin/EmailValidationView;->e()Z

    move-result v0

    if-eqz v0, :cond_18

    invoke-virtual {p0}, Lcom/osp/app/signin/EmailValidationView;->getLayoutInflater()Landroid/view/LayoutInflater;

    move-result-object v0

    const v1, 0x7f03001c

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/osp/app/signin/EmailValidationView;->R:Landroid/view/View;

    iget-object v6, p0, Lcom/osp/app/signin/EmailValidationView;->R:Landroid/view/View;

    const v0, 0x7f0c00cc

    invoke-virtual {v6, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    const v1, 0x7f0c00cd

    invoke-virtual {v6, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    const v2, 0x7f0c00ce

    invoke-virtual {v6, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    const v3, 0x7f0c00cf

    invoke-virtual {v6, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/TextView;

    const v4, 0x7f0c00d0

    invoke-virtual {v6, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/TextView;

    const v5, 0x7f0c00d1

    invoke-virtual {v6, v5}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v5

    check-cast v5, Landroid/widget/TextView;

    if-eqz v0, :cond_4

    invoke-virtual {p0}, Lcom/osp/app/signin/EmailValidationView;->getResources()Landroid/content/res/Resources;

    move-result-object v7

    invoke-static {p0}, Lcom/osp/app/util/an;->a(Lcom/osp/app/util/BaseActivity;)Lcom/osp/app/util/an;

    invoke-static {v12}, Lcom/osp/app/util/an;->a(I)I

    move-result v8

    invoke-virtual {v7, v8}, Landroid/content/res/Resources;->getColor(I)I

    move-result v7

    invoke-virtual {v0, v7}, Landroid/widget/TextView;->setTextColor(I)V

    :cond_4
    if-eqz v2, :cond_5

    invoke-virtual {p0}, Lcom/osp/app/signin/EmailValidationView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-static {p0}, Lcom/osp/app/util/an;->a(Lcom/osp/app/util/BaseActivity;)Lcom/osp/app/util/an;

    invoke-static {v12}, Lcom/osp/app/util/an;->a(I)I

    move-result v7

    invoke-virtual {v0, v7}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    invoke-virtual {v2, v0}, Landroid/widget/TextView;->setTextColor(I)V

    :cond_5
    if-eqz v4, :cond_6

    invoke-virtual {p0}, Lcom/osp/app/signin/EmailValidationView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-static {p0}, Lcom/osp/app/util/an;->a(Lcom/osp/app/util/BaseActivity;)Lcom/osp/app/util/an;

    invoke-static {v12}, Lcom/osp/app/util/an;->a(I)I

    move-result v2

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    invoke-virtual {v4, v0}, Landroid/widget/TextView;->setTextColor(I)V

    :cond_6
    if-eqz v1, :cond_7

    iget-object v0, p0, Lcom/osp/app/signin/EmailValidationView;->y:Ljava/lang/String;

    if-eqz v0, :cond_7

    iget-object v0, p0, Lcom/osp/app/signin/EmailValidationView;->y:Ljava/lang/String;

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    invoke-virtual {p0}, Lcom/osp/app/signin/EmailValidationView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-static {p0}, Lcom/osp/app/util/an;->a(Lcom/osp/app/util/BaseActivity;)Lcom/osp/app/util/an;

    invoke-static {v12}, Lcom/osp/app/util/an;->a(I)I

    move-result v2

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setTextColor(I)V

    :cond_7
    const v0, 0x106000d

    invoke-virtual {v6, v0}, Landroid/view/View;->setBackgroundResource(I)V

    if-eqz v3, :cond_8

    invoke-static {p0}, Lcom/osp/app/util/an;->a(Lcom/osp/app/util/BaseActivity;)Lcom/osp/app/util/an;

    const v0, 0x7f020089

    invoke-static {v0}, Lcom/osp/app/util/an;->a(I)I

    move-result v0

    invoke-virtual {v3, v0}, Landroid/widget/TextView;->setBackgroundResource(I)V

    invoke-virtual {p0}, Lcom/osp/app/signin/EmailValidationView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-static {p0}, Lcom/osp/app/util/an;->a(Lcom/osp/app/util/BaseActivity;)Lcom/osp/app/util/an;

    invoke-static {v11}, Lcom/osp/app/util/an;->a(I)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    invoke-virtual {v3, v0}, Landroid/widget/TextView;->setTextColor(I)V

    invoke-virtual {p0}, Lcom/osp/app/signin/EmailValidationView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-static {p0}, Lcom/osp/app/util/an;->a(Lcom/osp/app/util/BaseActivity;)Lcom/osp/app/util/an;

    invoke-static {v11}, Lcom/osp/app/util/an;->a(I)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    invoke-virtual {v3, v0}, Landroid/widget/TextView;->setLinkTextColor(I)V

    new-instance v0, Lcom/osp/app/signin/ew;

    invoke-direct {v0, p0}, Lcom/osp/app/signin/ew;-><init>(Lcom/osp/app/signin/EmailValidationView;)V

    const v1, 0x7f0900fd

    invoke-virtual {p0, v1}, Lcom/osp/app/signin/EmailValidationView;->getText(I)Ljava/lang/CharSequence;

    move-result-object v1

    invoke-static {v3, v1, v0}, Lcom/osp/app/signin/EmailValidationView;->a(Landroid/widget/TextView;Ljava/lang/CharSequence;Landroid/view/View$OnClickListener;)V

    :cond_8
    if-eqz v5, :cond_9

    invoke-static {p0}, Lcom/osp/app/util/an;->a(Lcom/osp/app/util/BaseActivity;)Lcom/osp/app/util/an;

    const v0, 0x7f020089

    invoke-static {v0}, Lcom/osp/app/util/an;->a(I)I

    move-result v0

    invoke-virtual {v5, v0}, Landroid/widget/TextView;->setBackgroundResource(I)V

    invoke-virtual {p0}, Lcom/osp/app/signin/EmailValidationView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-static {p0}, Lcom/osp/app/util/an;->a(Lcom/osp/app/util/BaseActivity;)Lcom/osp/app/util/an;

    invoke-static {v11}, Lcom/osp/app/util/an;->a(I)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    invoke-virtual {v5, v0}, Landroid/widget/TextView;->setTextColor(I)V

    invoke-virtual {p0}, Lcom/osp/app/signin/EmailValidationView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-static {p0}, Lcom/osp/app/util/an;->a(Lcom/osp/app/util/BaseActivity;)Lcom/osp/app/util/an;

    invoke-static {v11}, Lcom/osp/app/util/an;->a(I)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    invoke-virtual {v5, v0}, Landroid/widget/TextView;->setLinkTextColor(I)V

    new-instance v0, Lcom/osp/app/signin/ex;

    invoke-direct {v0, p0}, Lcom/osp/app/signin/ex;-><init>(Lcom/osp/app/signin/EmailValidationView;)V

    const v1, 0x7f0900be

    invoke-virtual {p0, v1}, Lcom/osp/app/signin/EmailValidationView;->getText(I)Ljava/lang/CharSequence;

    move-result-object v1

    invoke-static {v5, v1, v0}, Lcom/osp/app/signin/EmailValidationView;->a(Landroid/widget/TextView;Ljava/lang/CharSequence;Landroid/view/View$OnClickListener;)V

    :cond_9
    new-instance v1, Landroid/app/AlertDialog$Builder;

    invoke-direct {v1, p0}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    const v0, 0x7f090116

    invoke-virtual {v1, v0}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    invoke-virtual {v1, v6}, Landroid/app/AlertDialog$Builder;->setView(Landroid/view/View;)Landroid/app/AlertDialog$Builder;

    const v0, 0x7f090121

    new-instance v2, Lcom/osp/app/signin/ey;

    invoke-direct {v2, p0}, Lcom/osp/app/signin/ey;-><init>(Lcom/osp/app/signin/EmailValidationView;)V

    invoke-virtual {v1, v0, v2}, Landroid/app/AlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    sget v0, Lcom/osp/app/signin/EmailValidationView;->c:I

    if-nez v0, :cond_17

    const v0, 0x7f0900b6

    invoke-virtual {p0, v0}, Lcom/osp/app/signin/EmailValidationView;->getText(I)Ljava/lang/CharSequence;

    move-result-object v0

    invoke-interface {v0}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v0

    :goto_4
    new-instance v2, Lcom/osp/app/signin/ez;

    invoke-direct {v2, p0}, Lcom/osp/app/signin/ez;-><init>(Lcom/osp/app/signin/EmailValidationView;)V

    invoke-virtual {v1, v0, v2}, Landroid/app/AlertDialog$Builder;->setPositiveButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    new-instance v0, Lcom/osp/app/signin/fa;

    invoke-direct {v0, p0}, Lcom/osp/app/signin/fa;-><init>(Lcom/osp/app/signin/EmailValidationView;)V

    invoke-virtual {v1, v0}, Landroid/app/AlertDialog$Builder;->setOnCancelListener(Landroid/content/DialogInterface$OnCancelListener;)Landroid/app/AlertDialog$Builder;

    invoke-virtual {v1}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v0

    iput-object v0, p0, Lcom/osp/app/signin/EmailValidationView;->b:Landroid/app/AlertDialog;

    iget-object v0, p0, Lcom/osp/app/signin/EmailValidationView;->b:Landroid/app/AlertDialog;

    invoke-virtual {v0}, Landroid/app/AlertDialog;->getWindow()Landroid/view/Window;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/Window;->getAttributes()Landroid/view/WindowManager$LayoutParams;

    move-result-object v0

    invoke-virtual {p0}, Lcom/osp/app/signin/EmailValidationView;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f080101

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v1

    float-to-int v1, v1

    iput v1, v0, Landroid/view/WindowManager$LayoutParams;->width:I

    iget-object v1, p0, Lcom/osp/app/signin/EmailValidationView;->b:Landroid/app/AlertDialog;

    invoke-virtual {v1}, Landroid/app/AlertDialog;->getWindow()Landroid/view/Window;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/view/Window;->setAttributes(Landroid/view/WindowManager$LayoutParams;)V

    .line 460
    :cond_a
    :goto_5
    invoke-direct {p0}, Lcom/osp/app/signin/EmailValidationView;->e()Z

    move-result v0

    if-nez v0, :cond_12

    .line 462
    invoke-direct {p0, v10}, Lcom/osp/app/signin/EmailValidationView;->a(Z)V

    iget-object v0, p0, Lcom/osp/app/signin/EmailValidationView;->z:Landroid/content/Intent;

    if-eqz v0, :cond_b

    iget-object v0, p0, Lcom/osp/app/signin/EmailValidationView;->H:Landroid/widget/TextView;

    if-eqz v0, :cond_b

    iget-object v0, p0, Lcom/osp/app/signin/EmailValidationView;->G:Landroid/widget/TextView;

    if-nez v0, :cond_c

    :cond_b
    const/16 v0, 0xb

    iget-object v1, p0, Lcom/osp/app/signin/EmailValidationView;->z:Landroid/content/Intent;

    invoke-virtual {p0, v0, v1}, Lcom/osp/app/signin/EmailValidationView;->a(ILandroid/content/Intent;)V

    invoke-virtual {p0}, Lcom/osp/app/signin/EmailValidationView;->finish()V

    :cond_c
    const v0, 0x7f0c00db

    invoke-virtual {p0, v0}, Lcom/osp/app/signin/EmailValidationView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    if-eqz v0, :cond_d

    invoke-virtual {v0, v9}, Landroid/widget/TextView;->setVisibility(I)V

    :cond_d
    new-instance v0, Lcom/osp/app/signin/fc;

    invoke-direct {v0, p0}, Lcom/osp/app/signin/fc;-><init>(Lcom/osp/app/signin/EmailValidationView;)V

    iget-object v1, p0, Lcom/osp/app/signin/EmailValidationView;->I:Landroid/widget/TextView;

    if-eqz v1, :cond_e

    iget-object v1, p0, Lcom/osp/app/signin/EmailValidationView;->I:Landroid/widget/TextView;

    invoke-virtual {v1, v9}, Landroid/widget/TextView;->setVisibility(I)V

    iget-object v1, p0, Lcom/osp/app/signin/EmailValidationView;->I:Landroid/widget/TextView;

    const v2, 0x7f0900be

    invoke-virtual {p0, v2}, Lcom/osp/app/signin/EmailValidationView;->getText(I)Ljava/lang/CharSequence;

    move-result-object v2

    invoke-static {v1, v2, v0}, Lcom/osp/app/signin/EmailValidationView;->a(Landroid/widget/TextView;Ljava/lang/CharSequence;Landroid/view/View$OnClickListener;)V

    :cond_e
    new-instance v0, Lcom/osp/app/signin/fd;

    invoke-direct {v0, p0}, Lcom/osp/app/signin/fd;-><init>(Lcom/osp/app/signin/EmailValidationView;)V

    iget-object v1, p0, Lcom/osp/app/signin/EmailValidationView;->H:Landroid/widget/TextView;

    if-eqz v1, :cond_f

    iget-object v1, p0, Lcom/osp/app/signin/EmailValidationView;->H:Landroid/widget/TextView;

    const v2, 0x7f0900fd

    invoke-virtual {p0, v2}, Lcom/osp/app/signin/EmailValidationView;->getText(I)Ljava/lang/CharSequence;

    move-result-object v2

    invoke-static {v1, v2, v0}, Lcom/osp/app/signin/EmailValidationView;->a(Landroid/widget/TextView;Ljava/lang/CharSequence;Landroid/view/View$OnClickListener;)V

    :cond_f
    invoke-static {}, Lcom/osp/device/b;->b()Lcom/osp/device/b;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/osp/device/b;->i(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_10

    const-string v0, "next"

    invoke-virtual {p0, v0}, Lcom/osp/app/signin/EmailValidationView;->e(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/osp/app/signin/EmailValidationView;->O:Lcom/osp/app/signin/BottomSoftkeyLinearLayout;

    if-eqz v0, :cond_10

    iget-object v0, p0, Lcom/osp/app/signin/EmailValidationView;->O:Lcom/osp/app/signin/BottomSoftkeyLinearLayout;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Lcom/osp/app/signin/BottomSoftkeyLinearLayout;->setVisibility(I)V

    :cond_10
    iget-object v0, p0, Lcom/osp/app/signin/EmailValidationView;->y:Ljava/lang/String;

    if-eqz v0, :cond_11

    iget-object v0, p0, Lcom/osp/app/signin/EmailValidationView;->G:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/osp/app/signin/EmailValidationView;->y:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    :cond_11
    invoke-virtual {p0}, Lcom/osp/app/signin/EmailValidationView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v0

    iget v0, v0, Landroid/content/res/Configuration;->orientation:I

    invoke-direct {p0, v0}, Lcom/osp/app/signin/EmailValidationView;->a(I)V

    .line 465
    :cond_12
    invoke-virtual {p0}, Lcom/osp/app/signin/EmailValidationView;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v0

    .line 468
    :try_start_0
    const-string v1, "com.android.browser"

    const/16 v2, 0x80

    invoke-virtual {v0, v1, v2}, Landroid/content/pm/PackageManager;->getApplicationInfo(Ljava/lang/String;I)Landroid/content/pm/ApplicationInfo;

    .line 469
    const/4 v1, 0x1

    iput-boolean v1, p0, Lcom/osp/app/signin/EmailValidationView;->J:Z

    .line 470
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v1, "EVV"

    const-string v2, "hasBW exist"

    invoke-static {v1, v2}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    .line 478
    :goto_6
    :try_start_1
    const-string v1, "com.sec.android.app.sbrowser"

    const/16 v2, 0x80

    invoke-virtual {v0, v1, v2}, Landroid/content/pm/PackageManager;->getApplicationInfo(Ljava/lang/String;I)Landroid/content/pm/ApplicationInfo;

    .line 479
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/osp/app/signin/EmailValidationView;->K:Z

    .line 480
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v0, "EVV"

    const-string v1, "hasSBW exist"

    invoke-static {v0, v1}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_1
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_1 .. :try_end_1} :catch_1

    .line 487
    :goto_7
    new-instance v0, Lcom/osp/app/signin/fk;

    invoke-direct {v0, p0, p0}, Lcom/osp/app/signin/fk;-><init>(Lcom/osp/app/signin/EmailValidationView;Landroid/content/Context;)V

    .line 488
    invoke-virtual {v0}, Lcom/osp/app/signin/fk;->b()V

    .line 490
    iget v0, p0, Lcom/osp/app/signin/EmailValidationView;->A:I

    const/16 v1, 0xcb

    if-ne v0, v1, :cond_21

    .line 492
    new-instance v0, Landroid/content/Intent;

    const-string v1, "com.osp.ACCESSTOKEN.FAIL"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 493
    const-string v1, "APPID"

    iget-object v2, p0, Lcom/osp/app/signin/EmailValidationView;->d:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 494
    const-string v1, "bg_result"

    const/16 v2, 0xd

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 495
    invoke-virtual {p0, v0}, Lcom/osp/app/signin/EmailValidationView;->sendBroadcast(Landroid/content/Intent;)V

    .line 496
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v0, "EVV"

    const-string v1, "send intent ACCESSTOKEN.FAIL for email validation"

    invoke-static {v0, v1}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 510
    :cond_13
    :goto_8
    invoke-static {p0}, Lcom/msc/sa/c/d;->a(Landroid/app/Activity;)V

    goto/16 :goto_0

    .line 417
    :cond_14
    const/4 v0, 0x0

    goto/16 :goto_1

    .line 418
    :cond_15
    const/4 v0, 0x0

    goto/16 :goto_2

    .line 455
    :cond_16
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v0, "EVV"

    const-string v1, "setContentView"

    invoke-static {v0, v1}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 456
    const v0, 0x7f03001d

    invoke-static {p0}, Lcom/osp/app/util/r;->q(Landroid/content/Context;)Z

    move-result v1

    invoke-virtual {p0, v0, v1}, Lcom/osp/app/signin/EmailValidationView;->a(IZ)V

    goto/16 :goto_3

    .line 459
    :cond_17
    const v0, 0x7f09006b

    invoke-virtual {p0, v0}, Lcom/osp/app/signin/EmailValidationView;->getText(I)Ljava/lang/CharSequence;

    move-result-object v0

    invoke-interface {v0}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_4

    :cond_18
    invoke-static {}, Lcom/osp/app/util/r;->v()Z

    move-result v0

    if-eqz v0, :cond_19

    const v0, 0x7f0c016d

    invoke-virtual {p0, v0}, Lcom/osp/app/signin/EmailValidationView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    if-eqz v0, :cond_19

    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    :cond_19
    const v0, 0x7f0c00db

    invoke-virtual {p0, v0}, Lcom/osp/app/signin/EmailValidationView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    const v1, 0x7f0c00d9

    invoke-virtual {p0, v1}, Lcom/osp/app/signin/EmailValidationView;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    if-eqz v0, :cond_1a

    invoke-static {p0}, Lcom/osp/app/util/an;->a(Lcom/osp/app/util/BaseActivity;)Lcom/osp/app/util/an;

    invoke-static {v4}, Lcom/osp/app/util/an;->a(I)I

    move-result v3

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getColor(I)I

    move-result v3

    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setTextColor(I)V

    :cond_1a
    if-eqz v1, :cond_1b

    invoke-static {p0}, Lcom/osp/app/util/an;->a(Lcom/osp/app/util/BaseActivity;)Lcom/osp/app/util/an;

    invoke-static {v4}, Lcom/osp/app/util/an;->a(I)I

    move-result v0

    invoke-virtual {v2, v0}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setTextColor(I)V

    :cond_1b
    const v0, 0x7f0c00dc

    invoke-virtual {p0, v0}, Lcom/osp/app/signin/EmailValidationView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/osp/app/signin/EmailValidationView;->I:Landroid/widget/TextView;

    iget-object v0, p0, Lcom/osp/app/signin/EmailValidationView;->I:Landroid/widget/TextView;

    if-eqz v0, :cond_1c

    iget-object v0, p0, Lcom/osp/app/signin/EmailValidationView;->I:Landroid/widget/TextView;

    invoke-static {p0}, Lcom/osp/app/util/an;->a(Lcom/osp/app/util/BaseActivity;)Lcom/osp/app/util/an;

    invoke-static {v4}, Lcom/osp/app/util/an;->a(I)I

    move-result v1

    invoke-virtual {v2, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setTextColor(I)V

    iget-object v0, p0, Lcom/osp/app/signin/EmailValidationView;->I:Landroid/widget/TextView;

    invoke-static {p0}, Lcom/osp/app/util/an;->a(Lcom/osp/app/util/BaseActivity;)Lcom/osp/app/util/an;

    invoke-static {v11}, Lcom/osp/app/util/an;->a(I)I

    move-result v1

    invoke-virtual {v2, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setLinkTextColor(I)V

    :cond_1c
    const v0, 0x7f0c00da

    invoke-virtual {p0, v0}, Lcom/osp/app/signin/EmailValidationView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/osp/app/signin/EmailValidationView;->H:Landroid/widget/TextView;

    iget-object v0, p0, Lcom/osp/app/signin/EmailValidationView;->H:Landroid/widget/TextView;

    if-eqz v0, :cond_1d

    iget-object v0, p0, Lcom/osp/app/signin/EmailValidationView;->H:Landroid/widget/TextView;

    invoke-static {p0}, Lcom/osp/app/util/an;->a(Lcom/osp/app/util/BaseActivity;)Lcom/osp/app/util/an;

    invoke-static {v4}, Lcom/osp/app/util/an;->a(I)I

    move-result v1

    invoke-virtual {v2, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setTextColor(I)V

    iget-object v0, p0, Lcom/osp/app/signin/EmailValidationView;->H:Landroid/widget/TextView;

    invoke-static {p0}, Lcom/osp/app/util/an;->a(Lcom/osp/app/util/BaseActivity;)Lcom/osp/app/util/an;

    invoke-static {v11}, Lcom/osp/app/util/an;->a(I)I

    move-result v1

    invoke-virtual {v2, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setLinkTextColor(I)V

    :cond_1d
    const v0, 0x7f0c00d4

    invoke-virtual {p0, v0}, Lcom/osp/app/signin/EmailValidationView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/osp/app/signin/EmailValidationView;->E:Landroid/widget/TextView;

    const v0, 0x7f0c00d5

    invoke-virtual {p0, v0}, Lcom/osp/app/signin/EmailValidationView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/osp/app/signin/EmailValidationView;->F:Landroid/widget/TextView;

    const v0, 0x7f0c00d3

    invoke-virtual {p0, v0}, Lcom/osp/app/signin/EmailValidationView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/osp/app/signin/EmailValidationView;->C:Landroid/widget/TextView;

    iget-object v0, p0, Lcom/osp/app/signin/EmailValidationView;->C:Landroid/widget/TextView;

    if-eqz v0, :cond_1e

    iget-object v0, p0, Lcom/osp/app/signin/EmailValidationView;->C:Landroid/widget/TextView;

    invoke-static {p0}, Lcom/osp/app/util/an;->a(Lcom/osp/app/util/BaseActivity;)Lcom/osp/app/util/an;

    invoke-static {v4}, Lcom/osp/app/util/an;->a(I)I

    move-result v1

    invoke-virtual {v2, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setTextColor(I)V

    :cond_1e
    const v0, 0x7f0c00d6

    invoke-virtual {p0, v0}, Lcom/osp/app/signin/EmailValidationView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/osp/app/signin/EmailValidationView;->D:Landroid/widget/TextView;

    iget-object v0, p0, Lcom/osp/app/signin/EmailValidationView;->D:Landroid/widget/TextView;

    if-eqz v0, :cond_1f

    iget-object v0, p0, Lcom/osp/app/signin/EmailValidationView;->D:Landroid/widget/TextView;

    invoke-static {p0}, Lcom/osp/app/util/an;->a(Lcom/osp/app/util/BaseActivity;)Lcom/osp/app/util/an;

    invoke-static {v4}, Lcom/osp/app/util/an;->a(I)I

    move-result v1

    invoke-virtual {v2, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setTextColor(I)V

    :cond_1f
    const v0, 0x7f0c00d7

    invoke-virtual {p0, v0}, Lcom/osp/app/signin/EmailValidationView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/osp/app/signin/EmailValidationView;->G:Landroid/widget/TextView;

    iget-object v0, p0, Lcom/osp/app/signin/EmailValidationView;->G:Landroid/widget/TextView;

    if-eqz v0, :cond_20

    iget-object v0, p0, Lcom/osp/app/signin/EmailValidationView;->G:Landroid/widget/TextView;

    invoke-static {p0}, Lcom/osp/app/util/an;->a(Lcom/osp/app/util/BaseActivity;)Lcom/osp/app/util/an;

    invoke-static {v4}, Lcom/osp/app/util/an;->a(I)I

    move-result v1

    invoke-virtual {v2, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setTextColor(I)V

    :cond_20
    const v0, 0x7f0c0065

    invoke-virtual {p0, v0}, Lcom/osp/app/signin/EmailValidationView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/osp/app/signin/BottomSoftkeyLinearLayout;

    iput-object v0, p0, Lcom/osp/app/signin/EmailValidationView;->O:Lcom/osp/app/signin/BottomSoftkeyLinearLayout;

    iget-object v0, p0, Lcom/osp/app/signin/EmailValidationView;->O:Lcom/osp/app/signin/BottomSoftkeyLinearLayout;

    if-eqz v0, :cond_a

    iget-object v0, p0, Lcom/osp/app/signin/EmailValidationView;->O:Lcom/osp/app/signin/BottomSoftkeyLinearLayout;

    invoke-virtual {p0}, Lcom/osp/app/signin/EmailValidationView;->h()Z

    move-result v1

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-virtual {v0, p0, v1}, Lcom/osp/app/signin/BottomSoftkeyLinearLayout;->a(Landroid/content/Context;Ljava/lang/Boolean;)V

    goto/16 :goto_5

    .line 473
    :catch_0
    move-exception v1

    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v1, "EVV"

    const-string v2, "hasBW doesn\'t exist"

    invoke-static {v1, v2}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 474
    iput-boolean v9, p0, Lcom/osp/app/signin/EmailValidationView;->J:Z

    goto/16 :goto_6

    .line 483
    :catch_1
    move-exception v0

    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v0, "EVV"

    const-string v1, "hasSBW doesn\'t exist"

    invoke-static {v0, v1}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 484
    iput-boolean v9, p0, Lcom/osp/app/signin/EmailValidationView;->K:Z

    goto/16 :goto_7

    .line 497
    :cond_21
    iget v0, p0, Lcom/osp/app/signin/EmailValidationView;->A:I

    const/16 v1, 0xc9

    if-ne v0, v1, :cond_13

    .line 499
    new-instance v0, Landroid/content/Intent;

    const-string v1, "com.msc.action.samsungaccount.response.BackGroundSignin"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 500
    const-string v1, "bg_result"

    const/16 v2, 0xd

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 501
    iget-object v1, p0, Lcom/osp/app/signin/EmailValidationView;->B:Ljava/lang/String;

    if-eqz v1, :cond_22

    .line 503
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v2, p0, Lcom/osp/app/signin/EmailValidationView;->B:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ":"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setData(Landroid/net/Uri;)Landroid/content/Intent;

    .line 504
    iget-object v1, p0, Lcom/osp/app/signin/EmailValidationView;->B:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setPackage(Ljava/lang/String;)Landroid/content/Intent;

    .line 505
    const-string v1, "client_id"

    iget-object v2, p0, Lcom/osp/app/signin/EmailValidationView;->e:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 506
    iget-wide v2, p0, Lcom/osp/app/signin/EmailValidationView;->T:J

    invoke-virtual {p0, v0, v2, v3}, Lcom/osp/app/signin/EmailValidationView;->a(Landroid/content/Intent;J)V

    .line 508
    :cond_22
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v0, "EVV"

    const-string v1, "send intent RESPONSE_BACKGROUD_SIGNIN for email validation"

    invoke-static {v0, v1}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_8
.end method

.method public onCreateDialog(I)Landroid/app/Dialog;
    .locals 4

    .prologue
    const v3, 0x7f090064

    const v2, 0x7f09002d

    .line 641
    const/4 v0, 0x0

    .line 642
    packed-switch p1, :pswitch_data_0

    .line 741
    :goto_0
    return-object v0

    .line 645
    :pswitch_0
    const v0, 0x7f090189

    .line 646
    invoke-direct {p0}, Lcom/osp/app/signin/EmailValidationView;->e()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 648
    const v0, 0x7f0900ee

    .line 651
    :cond_0
    new-instance v1, Landroid/app/AlertDialog$Builder;

    invoke-direct {v1, p0}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    invoke-virtual {v1, v2}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/app/AlertDialog$Builder;->setMessage(I)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    new-instance v1, Lcom/osp/app/signin/fe;

    invoke-direct {v1, p0}, Lcom/osp/app/signin/fe;-><init>(Lcom/osp/app/signin/EmailValidationView;)V

    invoke-virtual {v0, v3, v1}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v0

    goto :goto_0

    .line 700
    :pswitch_1
    new-instance v0, Landroid/app/AlertDialog$Builder;

    invoke-direct {v0, p0}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    invoke-virtual {v0, v2}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    const v1, 0x7f09005d

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setMessage(I)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    new-instance v1, Lcom/osp/app/signin/ff;

    invoke-direct {v1, p0}, Lcom/osp/app/signin/ff;-><init>(Lcom/osp/app/signin/EmailValidationView;)V

    invoke-virtual {v0, v3, v1}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v0

    goto :goto_0

    .line 710
    :pswitch_2
    new-instance v0, Landroid/app/AlertDialog$Builder;

    invoke-direct {v0, p0}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    invoke-virtual {v0, v2}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    const v1, 0x7f09006e

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setMessage(I)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    new-instance v1, Lcom/osp/app/signin/fh;

    invoke-direct {v1, p0}, Lcom/osp/app/signin/fh;-><init>(Lcom/osp/app/signin/EmailValidationView;)V

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setOnCancelListener(Landroid/content/DialogInterface$OnCancelListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    new-instance v1, Lcom/osp/app/signin/fg;

    invoke-direct {v1, p0}, Lcom/osp/app/signin/fg;-><init>(Lcom/osp/app/signin/EmailValidationView;)V

    invoke-virtual {v0, v3, v1}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v0

    goto :goto_0

    .line 732
    :pswitch_3
    const-string v0, "Please complete the following email verification process to protect your email address from fraud or improper."

    .line 734
    new-instance v1, Landroid/app/AlertDialog$Builder;

    invoke-direct {v1, p0}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    invoke-virtual {v1, v2}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/app/AlertDialog$Builder;->setMessage(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    new-instance v1, Lcom/osp/app/signin/fi;

    invoke-direct {v1, p0}, Lcom/osp/app/signin/fi;-><init>(Lcom/osp/app/signin/EmailValidationView;)V

    invoke-virtual {v0, v3, v1}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v0

    goto/16 :goto_0

    .line 642
    nop

    :pswitch_data_0
    .packed-switch 0x64
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method protected onDestroy()V
    .locals 5

    .prologue
    .line 527
    invoke-super {p0}, Lcom/osp/app/util/BaseActivity;->onDestroy()V

    .line 532
    iget-object v0, p0, Lcom/osp/app/signin/EmailValidationView;->z:Landroid/content/Intent;

    const-string v1, "from_sign_up"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->hasExtra(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 534
    new-instance v0, Lcom/osp/app/bigdatalog/c;

    invoke-virtual {p0}, Lcom/osp/app/signin/EmailValidationView;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    const/4 v2, 0x6

    const/4 v3, 0x1

    iget-object v4, p0, Lcom/osp/app/signin/EmailValidationView;->z:Landroid/content/Intent;

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/osp/app/bigdatalog/c;-><init>(Landroid/content/Context;IILandroid/content/Intent;)V

    .line 536
    invoke-static {}, Lcom/osp/app/bigdatalog/e;->a()Lcom/osp/app/bigdatalog/e;

    move-result-object v1

    invoke-virtual {p0}, Lcom/osp/app/signin/EmailValidationView;->getApplicationContext()Landroid/content/Context;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/osp/app/bigdatalog/e;->a(Landroid/content/Context;)V

    .line 537
    invoke-static {}, Lcom/osp/app/bigdatalog/e;->a()Lcom/osp/app/bigdatalog/e;

    move-result-object v1

    invoke-virtual {p0}, Lcom/osp/app/signin/EmailValidationView;->getApplicationContext()Landroid/content/Context;

    move-result-object v2

    invoke-virtual {v1, v2, v0}, Lcom/osp/app/bigdatalog/e;->a(Landroid/content/Context;Lcom/osp/app/bigdatalog/c;)V

    .line 540
    :cond_0
    return-void
.end method

.method public onOptionsItemSelected(Landroid/view/MenuItem;)Z
    .locals 5

    .prologue
    const/4 v1, 0x1

    const/4 v0, 0x0

    .line 887
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v2, "EVV"

    const-string v3, "onOptionsItemSelected"

    const-string v4, "START"

    invoke-static {v2, v3, v4}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 889
    invoke-static {}, Lcom/osp/device/b;->b()Lcom/osp/device/b;

    move-result-object v2

    invoke-virtual {v2, p0}, Lcom/osp/device/b;->i(Landroid/content/Context;)Z

    move-result v2

    if-nez v2, :cond_0

    sget v2, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v3, 0xb

    if-ge v2, v3, :cond_0

    .line 917
    :goto_0
    return v0

    .line 894
    :cond_0
    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v2

    sparse-switch v2, :sswitch_data_0

    .line 913
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v1, "EVV"

    const-string v2, "onOptionsItemSelected"

    const-string v3, "END"

    invoke-static {v1, v2, v3}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 898
    :sswitch_0
    invoke-virtual {p0}, Lcom/osp/app/signin/EmailValidationView;->onBackPressed()V

    .line 916
    :goto_1
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v0, "EVV"

    const-string v2, "onOptionsItemSelected"

    const-string v3, "END"

    invoke-static {v0, v2, v3}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    move v0, v1

    .line 917
    goto :goto_0

    .line 903
    :sswitch_1
    iput-boolean v1, p0, Lcom/osp/app/signin/EmailValidationView;->M:Z

    .line 904
    invoke-direct {p0}, Lcom/osp/app/signin/EmailValidationView;->c()V

    goto :goto_1

    .line 908
    :sswitch_2
    new-instance v2, Lcom/osp/app/signin/fj;

    invoke-direct {v2, p0, p0}, Lcom/osp/app/signin/fj;-><init>(Lcom/osp/app/signin/EmailValidationView;Landroid/content/Context;)V

    .line 909
    new-array v0, v0, [Ljava/lang/Void;

    invoke-virtual {v2, v0}, Lcom/osp/app/signin/fj;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    goto :goto_1

    .line 894
    nop

    :sswitch_data_0
    .sparse-switch
        0x102002c -> :sswitch_0
        0x7f0c0196 -> :sswitch_2
        0x7f0c019b -> :sswitch_1
    .end sparse-switch
.end method

.method protected onResume()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 751
    invoke-super {p0}, Lcom/osp/app/util/BaseActivity;->onResume()V

    .line 752
    iget-boolean v0, p0, Lcom/osp/app/signin/EmailValidationView;->M:Z

    if-ne v0, v3, :cond_0

    .line 754
    new-instance v0, Lcom/osp/app/signin/fj;

    invoke-direct {v0, p0, p0}, Lcom/osp/app/signin/fj;-><init>(Lcom/osp/app/signin/EmailValidationView;Landroid/content/Context;)V

    .line 755
    new-array v1, v2, [Ljava/lang/Void;

    invoke-virtual {v0, v1}, Lcom/osp/app/signin/fj;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    .line 756
    iput-boolean v2, p0, Lcom/osp/app/signin/EmailValidationView;->M:Z

    .line 759
    :cond_0
    invoke-static {p0}, Lcom/osp/app/util/ad;->d(Landroid/content/Context;)Z

    move-result v0

    if-eq v0, v3, :cond_1

    .line 761
    invoke-virtual {p0}, Lcom/osp/app/signin/EmailValidationView;->finish()V

    .line 763
    :cond_1
    return-void
.end method
