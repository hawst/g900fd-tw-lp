.class public final Lcom/osp/app/signin/h;
.super Ljava/lang/Object;
.source "AccountServiceList.java"


# instance fields
.field private final a:Landroid/content/Context;

.field private b:Lcom/osp/app/signin/j;

.field private c:Ljava/util/ArrayList;

.field private d:I


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 486
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 141
    iput-object v0, p0, Lcom/osp/app/signin/h;->b:Lcom/osp/app/signin/j;

    .line 145
    iput-object v0, p0, Lcom/osp/app/signin/h;->c:Ljava/util/ArrayList;

    .line 149
    const/4 v0, 0x0

    iput v0, p0, Lcom/osp/app/signin/h;->d:I

    .line 487
    iput-object p1, p0, Lcom/osp/app/signin/h;->a:Landroid/content/Context;

    .line 488
    return-void
.end method

.method static synthetic a(Lcom/osp/app/signin/h;)Landroid/content/Context;
    .locals 1

    .prologue
    .line 39
    iget-object v0, p0, Lcom/osp/app/signin/h;->a:Landroid/content/Context;

    return-object v0
.end method

.method private c(I)Z
    .locals 5

    .prologue
    const/16 v4, 0x80

    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 231
    .line 233
    iget-object v2, p0, Lcom/osp/app/signin/h;->a:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v2

    .line 234
    if-ne v0, p1, :cond_0

    .line 238
    :try_start_0
    const-string v3, "com.sec.dsm.system"

    const/16 v4, 0x80

    invoke-virtual {v2, v3, v4}, Landroid/content/pm/PackageManager;->getApplicationInfo(Ljava/lang/String;I)Landroid/content/pm/ApplicationInfo;

    .line 240
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v2, "ASL"

    const-string v3, "FMM exist"

    invoke-static {v2, v3}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    .line 350
    :goto_0
    invoke-static {}, Lcom/osp/app/util/ah;->a()Lcom/osp/app/util/ah;

    move-result-object v2

    invoke-virtual {v2}, Lcom/osp/app/util/ah;->t()Z

    move-result v2

    if-eqz v2, :cond_a

    .line 355
    :goto_1
    return v1

    .line 243
    :catch_0
    move-exception v0

    .line 244
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v0, "ASL"

    const-string v2, "FMM doesn\'t exist"

    invoke-static {v0, v2}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;)V

    move v0, v1

    .line 245
    goto :goto_0

    .line 246
    :cond_0
    const/4 v3, 0x2

    if-ne v3, p1, :cond_2

    .line 248
    invoke-static {}, Lcom/osp/app/util/r;->a()Z

    move-result v3

    if-eqz v3, :cond_1

    move v0, v1

    .line 251
    goto :goto_0

    .line 256
    :cond_1
    :try_start_1
    const-string v3, "com.sec.android.app.samsungapps"

    const/16 v4, 0x80

    invoke-virtual {v2, v3, v4}, Landroid/content/pm/PackageManager;->getApplicationInfo(Ljava/lang/String;I)Landroid/content/pm/ApplicationInfo;

    .line 258
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v2, "ASL"

    const-string v3, "Galaxy apps exist"

    invoke-static {v2, v3}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_1
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_1 .. :try_end_1} :catch_1

    goto :goto_0

    .line 261
    :catch_1
    move-exception v0

    .line 262
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v0, "ASL"

    const-string v2, "Galaxy apps doesn\'t exist"

    invoke-static {v0, v2}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;)V

    move v0, v1

    .line 263
    goto :goto_0

    .line 265
    :cond_2
    const/4 v3, 0x4

    if-ne v3, p1, :cond_5

    .line 267
    invoke-static {}, Lcom/osp/app/util/r;->h()Z

    move-result v3

    if-eqz v3, :cond_3

    move v0, v1

    .line 269
    goto :goto_0

    .line 274
    :cond_3
    :try_start_2
    iget-object v3, p0, Lcom/osp/app/signin/h;->a:Landroid/content/Context;

    invoke-static {v3}, Lcom/msc/sa/c/d;->d(Landroid/content/Context;)Z

    move-result v3

    if-eqz v3, :cond_4

    .line 277
    const-string v3, "com.sec.android.sCloudBackupApp"

    const/16 v4, 0x80

    invoke-virtual {v2, v3, v4}, Landroid/content/pm/PackageManager;->getApplicationInfo(Ljava/lang/String;I)Landroid/content/pm/ApplicationInfo;

    .line 280
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v3, "ASL"

    const-string v4, "PDM exist"

    invoke-static {v3, v4}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_2
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_2 .. :try_end_2} :catch_2

    goto :goto_0

    :catch_2
    move-exception v3

    .line 291
    :try_start_3
    const-string v3, "com.samsung.android.scloud.backup"

    const/16 v4, 0x80

    invoke-virtual {v2, v3, v4}, Landroid/content/pm/PackageManager;->getApplicationInfo(Ljava/lang/String;I)Landroid/content/pm/ApplicationInfo;

    .line 294
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v2, "ASL"

    const-string v3, "PDM exist"

    invoke-static {v2, v3}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_3
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_3 .. :try_end_3} :catch_3

    goto :goto_0

    .line 297
    :catch_3
    move-exception v0

    .line 298
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v0, "ASL"

    const-string v2, "PDM doesn\'t exist"

    invoke-static {v0, v2}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;)V

    move v0, v1

    .line 300
    goto/16 :goto_0

    .line 283
    :cond_4
    :try_start_4
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v3, "ASL"

    const-string v4, "Not supported PDM. Not Owner User"

    invoke-static {v3, v4}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_4
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_4 .. :try_end_4} :catch_2

    move v0, v1

    .line 300
    goto/16 :goto_0

    .line 302
    :cond_5
    const/16 v3, 0x8

    if-ne v3, p1, :cond_6

    .line 306
    :try_start_5
    const-string v3, "com.sec.android.app.shealth"

    const/16 v4, 0x80

    invoke-virtual {v2, v3, v4}, Landroid/content/pm/PackageManager;->getApplicationInfo(Ljava/lang/String;I)Landroid/content/pm/ApplicationInfo;

    .line 308
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v2, "ASL"

    const-string v3, "S Health exist"

    invoke-static {v2, v3}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_5
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_5 .. :try_end_5} :catch_4

    goto/16 :goto_0

    .line 311
    :catch_4
    move-exception v0

    .line 312
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v0, "ASL"

    const-string v2, "S Health doesn\'t exist"

    invoke-static {v0, v2}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;)V

    move v0, v1

    .line 313
    goto/16 :goto_0

    .line 314
    :cond_6
    const/16 v3, 0x200

    if-ne v3, p1, :cond_7

    .line 318
    :try_start_6
    const-string v3, "com.samsung.milk.milkvideo"

    const/16 v4, 0x80

    invoke-virtual {v2, v3, v4}, Landroid/content/pm/PackageManager;->getApplicationInfo(Ljava/lang/String;I)Landroid/content/pm/ApplicationInfo;

    .line 320
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v2, "ASL"

    const-string v3, "Milk Video exist"

    invoke-static {v2, v3}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_6
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_6 .. :try_end_6} :catch_5

    goto/16 :goto_0

    .line 323
    :catch_5
    move-exception v0

    .line 324
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v0, "ASL"

    const-string v2, "Milk Video doesn\'t exist"

    invoke-static {v0, v2}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;)V

    move v0, v1

    .line 325
    goto/16 :goto_0

    .line 326
    :cond_7
    if-ne v4, p1, :cond_9

    .line 339
    iget-object v2, p0, Lcom/osp/app/signin/h;->a:Landroid/content/Context;

    invoke-static {v2}, Lcom/msc/sa/c/d;->e(Landroid/content/Context;)Z

    move-result v2

    if-eqz v2, :cond_8

    .line 342
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v2, "ASL"

    const-string v3, "Reactivation Lock exist"

    invoke-static {v2, v3}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 346
    :cond_8
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v0, "ASL"

    const-string v2, "Reactivation Lock doesn\'t exist"

    invoke-static {v0, v2}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;)V

    :cond_9
    move v0, v1

    goto/16 :goto_0

    :cond_a
    move v1, v0

    goto/16 :goto_1
.end method

.method private d(I)Z
    .locals 8

    .prologue
    const/4 v6, 0x4

    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 365
    .line 367
    const/4 v0, 0x0

    .line 368
    const/16 v3, 0x10

    if-ne v3, p1, :cond_1

    .line 371
    new-array v0, v6, [Ljava/lang/String;

    const-string v3, "KR"

    aput-object v3, v0, v2

    const-string v3, "US"

    aput-object v3, v0, v1

    const-string v3, "AU"

    aput-object v3, v0, v4

    const-string v3, "NZ"

    aput-object v3, v0, v5

    .line 386
    :cond_0
    :goto_0
    invoke-static {}, Lcom/osp/common/util/i;->a()Lcom/osp/common/util/i;

    iget-object v3, p0, Lcom/osp/app/signin/h;->a:Landroid/content/Context;

    iget-object v3, p0, Lcom/osp/app/signin/h;->a:Landroid/content/Context;

    invoke-static {v3}, Lcom/osp/app/util/ad;->p(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Lcom/osp/common/util/i;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 387
    if-eqz v4, :cond_5

    .line 389
    if-eqz v0, :cond_7

    .line 391
    array-length v5, v0

    move v3, v2

    :goto_1
    if-ge v3, v5, :cond_4

    aget-object v6, v0, v3

    .line 393
    sget-object v7, Ljava/util/Locale;->ENGLISH:Ljava/util/Locale;

    invoke-virtual {v4, v7}, Ljava/lang/String;->toUpperCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_3

    move v0, v1

    .line 417
    :goto_2
    return v0

    .line 373
    :cond_1
    const/16 v3, 0x40

    if-ne v3, p1, :cond_2

    .line 376
    new-array v0, v6, [Ljava/lang/String;

    const-string v3, "KR"

    aput-object v3, v0, v2

    const-string v3, "US"

    aput-object v3, v0, v1

    const-string v3, "GB"

    aput-object v3, v0, v4

    const-string v3, "BR"

    aput-object v3, v0, v5

    goto :goto_0

    .line 378
    :cond_2
    const/16 v3, 0x20

    if-ne v3, p1, :cond_0

    .line 382
    const/16 v0, 0xb

    new-array v0, v0, [Ljava/lang/String;

    const-string v3, "KR"

    aput-object v3, v0, v2

    const-string v3, "US"

    aput-object v3, v0, v1

    const-string v3, "GB"

    aput-object v3, v0, v4

    const-string v3, "FR"

    aput-object v3, v0, v5

    const-string v3, "DE"

    aput-object v3, v0, v6

    const/4 v3, 0x5

    const-string v4, "ES"

    aput-object v4, v0, v3

    const/4 v3, 0x6

    const-string v4, "RU"

    aput-object v4, v0, v3

    const/4 v3, 0x7

    const-string v4, "BR"

    aput-object v4, v0, v3

    const/16 v3, 0x8

    const-string v4, "AU"

    aput-object v4, v0, v3

    const/16 v3, 0x9

    const-string v4, "HK"

    aput-object v4, v0, v3

    const/16 v3, 0xa

    const-string v4, "CN"

    aput-object v4, v0, v3

    goto :goto_0

    .line 391
    :cond_3
    add-int/lit8 v3, v3, 0x1

    goto :goto_1

    :cond_4
    move v0, v2

    goto :goto_2

    .line 402
    :cond_5
    invoke-static {}, Lcom/osp/device/b;->b()Lcom/osp/device/b;

    move-result-object v3

    invoke-virtual {v3}, Lcom/osp/device/b;->n()Ljava/lang/String;

    move-result-object v4

    .line 404
    if-eqz v0, :cond_7

    .line 406
    array-length v5, v0

    move v3, v2

    :goto_3
    if-ge v3, v5, :cond_7

    aget-object v6, v0, v3

    .line 408
    sget-object v7, Ljava/util/Locale;->ENGLISH:Ljava/util/Locale;

    invoke-virtual {v4, v7}, Ljava/lang/String;->toUpperCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_6

    move v0, v1

    .line 411
    goto :goto_2

    .line 406
    :cond_6
    add-int/lit8 v3, v3, 0x1

    goto :goto_3

    :cond_7
    move v0, v2

    goto :goto_2
.end method

.method private e(I)Ljava/util/ArrayList;
    .locals 9

    .prologue
    const/4 v8, 0x4

    const/4 v7, 0x1

    const/4 v6, -0x1

    const/16 v5, 0x8

    const/4 v4, 0x2

    .line 498
    iget-object v0, p0, Lcom/osp/app/signin/h;->c:Ljava/util/ArrayList;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/osp/app/signin/h;->c:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-lez v0, :cond_0

    iget v0, p0, Lcom/osp/app/signin/h;->d:I

    if-ne v0, p1, :cond_0

    .line 500
    iget-object v0, p0, Lcom/osp/app/signin/h;->c:Ljava/util/ArrayList;

    .line 658
    :goto_0
    return-object v0

    .line 503
    :cond_0
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 506
    and-int/lit16 v0, p1, 0x1000

    if-eqz v0, :cond_1

    .line 508
    new-instance v0, Lcom/osp/app/signin/i;

    const v2, 0x7f0901a9

    const/16 v3, 0x1000

    invoke-direct {v0, p0, v6, v2, v3}, Lcom/osp/app/signin/i;-><init>(Lcom/osp/app/signin/h;III)V

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 513
    :cond_1
    and-int/lit16 v0, p1, 0x2000

    if-eqz v0, :cond_2

    .line 515
    new-instance v0, Lcom/osp/app/signin/i;

    const v2, 0x7f0901b5

    const/16 v3, 0x2000

    invoke-direct {v0, p0, v6, v2, v3}, Lcom/osp/app/signin/i;-><init>(Lcom/osp/app/signin/h;III)V

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 519
    :cond_2
    and-int/lit8 v0, p1, 0x1

    if-eqz v0, :cond_3

    .line 521
    invoke-direct {p0, v7}, Lcom/osp/app/signin/h;->c(I)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 523
    new-instance v0, Lcom/osp/app/signin/i;

    const v2, 0x7f02005f

    const v3, 0x7f0901ad

    invoke-direct {v0, p0, v2, v3, v7}, Lcom/osp/app/signin/i;-><init>(Lcom/osp/app/signin/h;III)V

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 528
    :cond_3
    and-int/lit8 v0, p1, 0x2

    if-eqz v0, :cond_4

    .line 530
    invoke-direct {p0, v4}, Lcom/osp/app/signin/h;->c(I)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 532
    invoke-direct {p0, v4}, Lcom/osp/app/signin/h;->f(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    .line 533
    if-eqz v0, :cond_f

    invoke-static {}, Lcom/osp/device/b;->b()Lcom/osp/device/b;

    move-result-object v2

    iget-object v3, p0, Lcom/osp/app/signin/h;->a:Landroid/content/Context;

    invoke-virtual {v2, v3}, Lcom/osp/device/b;->i(Landroid/content/Context;)Z

    move-result v2

    if-nez v2, :cond_f

    .line 535
    new-instance v2, Lcom/osp/app/signin/i;

    const v3, 0x7f0901bb

    invoke-direct {v2, p0, v0, v3, v4}, Lcom/osp/app/signin/i;-><init>(Lcom/osp/app/signin/h;Landroid/graphics/drawable/Drawable;II)V

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 545
    :cond_4
    :goto_1
    and-int/lit8 v0, p1, 0x4

    if-eqz v0, :cond_5

    .line 547
    invoke-direct {p0, v8}, Lcom/osp/app/signin/h;->c(I)Z

    move-result v0

    if-eqz v0, :cond_5

    .line 549
    new-instance v0, Lcom/osp/app/signin/i;

    const v2, 0x7f020064

    const v3, 0x7f0901af

    invoke-direct {v0, p0, v2, v3, v8}, Lcom/osp/app/signin/i;-><init>(Lcom/osp/app/signin/h;III)V

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 554
    :cond_5
    and-int/lit8 v0, p1, 0x8

    if-eqz v0, :cond_6

    .line 556
    invoke-direct {p0, v5}, Lcom/osp/app/signin/h;->c(I)Z

    move-result v0

    if-eqz v0, :cond_6

    .line 558
    invoke-direct {p0, v5}, Lcom/osp/app/signin/h;->f(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    .line 559
    if-eqz v0, :cond_10

    invoke-static {}, Lcom/osp/device/b;->b()Lcom/osp/device/b;

    move-result-object v2

    iget-object v3, p0, Lcom/osp/app/signin/h;->a:Landroid/content/Context;

    invoke-virtual {v2, v3}, Lcom/osp/device/b;->i(Landroid/content/Context;)Z

    move-result v2

    if-nez v2, :cond_10

    .line 561
    new-instance v2, Lcom/osp/app/signin/i;

    const v3, 0x7f0901b7

    invoke-direct {v2, p0, v0, v3, v5}, Lcom/osp/app/signin/i;-><init>(Lcom/osp/app/signin/h;Landroid/graphics/drawable/Drawable;II)V

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 570
    :cond_6
    :goto_2
    and-int/lit8 v0, p1, 0x10

    if-eqz v0, :cond_7

    .line 572
    const/16 v0, 0x10

    invoke-direct {p0, v0}, Lcom/osp/app/signin/h;->d(I)Z

    move-result v0

    if-eqz v0, :cond_7

    .line 574
    new-instance v0, Lcom/osp/app/signin/i;

    const v2, 0x7f020061

    const v3, 0x7f0901bc

    const/16 v4, 0x10

    invoke-direct {v0, p0, v2, v3, v4}, Lcom/osp/app/signin/i;-><init>(Lcom/osp/app/signin/h;III)V

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 579
    :cond_7
    and-int/lit16 v0, p1, 0x200

    if-eqz v0, :cond_8

    .line 581
    const/16 v0, 0x200

    invoke-direct {p0, v0}, Lcom/osp/app/signin/h;->c(I)Z

    move-result v0

    if-eqz v0, :cond_8

    .line 583
    new-instance v0, Lcom/osp/app/signin/i;

    const v2, 0x7f020062

    const v3, 0x7f0901b2

    const/16 v4, 0x200

    invoke-direct {v0, p0, v2, v3, v4}, Lcom/osp/app/signin/i;-><init>(Lcom/osp/app/signin/h;III)V

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 588
    :cond_8
    and-int/lit8 v0, p1, 0x20

    if-eqz v0, :cond_9

    .line 590
    invoke-static {}, Lcom/osp/app/util/r;->h()Z

    move-result v0

    if-nez v0, :cond_9

    const/16 v0, 0x20

    invoke-direct {p0, v0}, Lcom/osp/app/signin/h;->d(I)Z

    move-result v0

    if-eqz v0, :cond_9

    .line 592
    new-instance v0, Lcom/osp/app/signin/i;

    const v2, 0x7f020068

    const v3, 0x7f0901b6

    const/16 v4, 0x20

    invoke-direct {v0, p0, v2, v3, v4}, Lcom/osp/app/signin/i;-><init>(Lcom/osp/app/signin/h;III)V

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 597
    :cond_9
    and-int/lit8 v0, p1, 0x40

    if-eqz v0, :cond_a

    .line 599
    const/16 v0, 0x40

    invoke-direct {p0, v0}, Lcom/osp/app/signin/h;->d(I)Z

    move-result v0

    if-eqz v0, :cond_a

    .line 601
    new-instance v0, Lcom/osp/app/signin/i;

    const v2, 0x7f020063

    const v3, 0x7f0901b3

    const/16 v4, 0x40

    invoke-direct {v0, p0, v2, v3, v4}, Lcom/osp/app/signin/i;-><init>(Lcom/osp/app/signin/h;III)V

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 606
    :cond_a
    and-int/lit16 v0, p1, 0x80

    if-eqz v0, :cond_b

    .line 608
    const/16 v0, 0x80

    invoke-direct {p0, v0}, Lcom/osp/app/signin/h;->c(I)Z

    move-result v0

    if-eqz v0, :cond_b

    .line 610
    new-instance v0, Lcom/osp/app/signin/i;

    const v2, 0x7f020065

    const v3, 0x7f0901ae

    const/16 v4, 0x80

    invoke-direct {v0, p0, v2, v3, v4}, Lcom/osp/app/signin/i;-><init>(Lcom/osp/app/signin/h;III)V

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 615
    :cond_b
    and-int/lit16 v0, p1, 0x100

    if-eqz v0, :cond_c

    .line 617
    iget-object v0, p0, Lcom/osp/app/signin/h;->a:Landroid/content/Context;

    invoke-static {v0}, Lcom/osp/app/util/r;->b(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_c

    .line 619
    iget-object v0, p0, Lcom/osp/app/signin/h;->a:Landroid/content/Context;

    invoke-static {v0}, Lcom/osp/app/util/r;->s(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_11

    .line 623
    const v0, 0x7f020067

    .line 629
    :goto_3
    new-instance v2, Lcom/osp/app/signin/i;

    const v3, 0x7f0901b0

    const/16 v4, 0x100

    invoke-direct {v2, p0, v0, v3, v4}, Lcom/osp/app/signin/i;-><init>(Lcom/osp/app/signin/h;III)V

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 634
    :cond_c
    and-int/lit16 v0, p1, 0x4000

    if-eqz v0, :cond_d

    .line 636
    new-instance v0, Lcom/osp/app/signin/i;

    const v2, 0x7f0901a8

    const/16 v3, 0x4000

    invoke-direct {v0, p0, v6, v2, v3}, Lcom/osp/app/signin/i;-><init>(Lcom/osp/app/signin/h;III)V

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 639
    :cond_d
    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-lez v0, :cond_12

    .line 641
    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v3

    .line 642
    const/high16 v0, 0x300000

    and-int v4, p1, v0

    .line 643
    const/4 v0, 0x0

    move v2, v0

    :goto_4
    if-ge v2, v3, :cond_12

    .line 645
    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/osp/app/signin/i;

    .line 646
    if-eqz v0, :cond_e

    .line 648
    iget v5, v0, Lcom/osp/app/signin/i;->c:I

    or-int/2addr v5, v4

    iput v5, v0, Lcom/osp/app/signin/i;->c:I

    .line 643
    :cond_e
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_4

    .line 538
    :cond_f
    new-instance v0, Lcom/osp/app/signin/i;

    const v2, 0x7f020060

    const v3, 0x7f0901bb

    invoke-direct {v0, p0, v2, v3, v4}, Lcom/osp/app/signin/i;-><init>(Lcom/osp/app/signin/h;III)V

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto/16 :goto_1

    .line 564
    :cond_10
    new-instance v0, Lcom/osp/app/signin/i;

    const v2, 0x7f020069

    const v3, 0x7f0901b7

    invoke-direct {v0, p0, v2, v3, v5}, Lcom/osp/app/signin/i;-><init>(Lcom/osp/app/signin/h;III)V

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto/16 :goto_2

    .line 627
    :cond_11
    const v0, 0x7f020066

    goto :goto_3

    .line 655
    :cond_12
    iput-object v1, p0, Lcom/osp/app/signin/h;->c:Ljava/util/ArrayList;

    .line 656
    iput p1, p0, Lcom/osp/app/signin/h;->d:I

    move-object v0, v1

    .line 658
    goto/16 :goto_0
.end method

.method private f(I)Landroid/graphics/drawable/Drawable;
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 668
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v0, "ASL"

    const-string v2, "getApplicationIconDrawable invoked"

    invoke-static {v0, v2}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 669
    sparse-switch p1, :sswitch_data_0

    .line 681
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v0, "ASL"

    const-string v2, "there is no matching type"

    invoke-static {v0, v2}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 694
    :goto_0
    return-object v1

    .line 673
    :sswitch_0
    const-string v0, "com.sec.android.app.samsungapps"

    .line 674
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v2, "ASL"

    const-string v3, "APPS icon procured"

    invoke-static {v2, v3}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 685
    :goto_1
    iget-object v2, p0, Lcom/osp/app/signin/h;->a:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v2

    .line 689
    :try_start_0
    invoke-virtual {v2, v0}, Landroid/content/pm/PackageManager;->getApplicationIcon(Ljava/lang/String;)Landroid/graphics/drawable/Drawable;
    :try_end_0
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    :goto_2
    move-object v1, v0

    .line 694
    goto :goto_0

    .line 677
    :sswitch_1
    const-string v0, "com.sec.android.app.shealth"

    .line 678
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v2, "ASL"

    const-string v3, "SHEALTH icon procured"

    invoke-static {v2, v3}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1

    .line 692
    :catch_0
    move-exception v0

    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v0, "ASL"

    const-string v2, "there is no application icon."

    invoke-static {v0, v2}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;)V

    move-object v0, v1

    goto :goto_2

    .line 669
    nop

    :sswitch_data_0
    .sparse-switch
        0x2 -> :sswitch_0
        0x8 -> :sswitch_1
    .end sparse-switch
.end method


# virtual methods
.method public final a(Ljava/lang/CharSequence;Landroid/view/View$OnClickListener;)V
    .locals 1

    .prologue
    .line 475
    iget-object v0, p0, Lcom/osp/app/signin/h;->b:Lcom/osp/app/signin/j;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/osp/app/signin/h;->b:Lcom/osp/app/signin/j;

    invoke-virtual {v0}, Lcom/osp/app/signin/j;->getCount()I

    move-result v0

    if-lez v0, :cond_0

    .line 477
    iget-object v0, p0, Lcom/osp/app/signin/h;->b:Lcom/osp/app/signin/j;

    invoke-virtual {v0, p1, p2}, Lcom/osp/app/signin/j;->a(Ljava/lang/CharSequence;Landroid/view/View$OnClickListener;)V

    .line 479
    :cond_0
    return-void
.end method

.method public final a(I)Z
    .locals 3

    .prologue
    .line 442
    const/4 v0, 0x0

    .line 444
    invoke-direct {p0, p1}, Lcom/osp/app/signin/h;->e(I)Ljava/util/ArrayList;

    .line 446
    and-int/lit16 v1, p1, 0x1000

    div-int/lit16 v1, v1, 0x1000

    and-int/lit16 v2, p1, 0x2000

    div-int/lit16 v2, v2, 0x2000

    add-int/2addr v1, v2

    and-int/lit16 v2, p1, 0x4000

    div-int/lit16 v2, v2, 0x4000

    add-int/2addr v1, v2

    .line 448
    iget-object v2, p0, Lcom/osp/app/signin/h;->c:Ljava/util/ArrayList;

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/osp/app/signin/h;->c:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v2

    sub-int v1, v2, v1

    if-lez v1, :cond_0

    .line 450
    const/4 v0, 0x1

    .line 453
    :cond_0
    return v0
.end method

.method public final b(I)Lcom/osp/app/signin/j;
    .locals 3

    .prologue
    .line 463
    new-instance v0, Lcom/osp/app/signin/j;

    iget-object v1, p0, Lcom/osp/app/signin/h;->a:Landroid/content/Context;

    invoke-direct {p0, p1}, Lcom/osp/app/signin/h;->e(I)Ljava/util/ArrayList;

    move-result-object v2

    invoke-direct {v0, p0, v1, v2}, Lcom/osp/app/signin/j;-><init>(Lcom/osp/app/signin/h;Landroid/content/Context;Ljava/util/ArrayList;)V

    iput-object v0, p0, Lcom/osp/app/signin/h;->b:Lcom/osp/app/signin/j;

    .line 465
    iget-object v0, p0, Lcom/osp/app/signin/h;->b:Lcom/osp/app/signin/j;

    return-object v0
.end method
