.class public Lcom/osp/app/signin/AccountInfoListView;
.super Landroid/widget/LinearLayout;
.source "AccountInfoListView.java"


# instance fields
.field public a:Landroid/widget/LinearLayout;

.field public b:Landroid/widget/LinearLayout;

.field public c:Landroid/widget/TextView;

.field public d:Landroid/widget/TextView;

.field public e:Landroid/widget/TextView;

.field public f:Landroid/widget/EditText;

.field public g:Landroid/widget/TextView;

.field public h:Landroid/widget/Spinner;

.field public i:Landroid/view/View;

.field public j:Landroid/view/View;

.field private final k:Lcom/osp/app/util/BaseActivity;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0

    .prologue
    .line 58
    invoke-direct {p0, p1}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;)V

    .line 59
    check-cast p1, Lcom/osp/app/util/BaseActivity;

    iput-object p1, p0, Lcom/osp/app/signin/AccountInfoListView;->k:Lcom/osp/app/util/BaseActivity;

    .line 60
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0

    .prologue
    .line 63
    invoke-direct {p0, p1, p2}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 64
    check-cast p1, Lcom/osp/app/util/BaseActivity;

    iput-object p1, p0, Lcom/osp/app/signin/AccountInfoListView;->k:Lcom/osp/app/util/BaseActivity;

    .line 65
    return-void
.end method


# virtual methods
.method public final a()V
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 71
    const v0, 0x7f0c0031

    invoke-virtual {p0, v0}, Lcom/osp/app/signin/AccountInfoListView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcom/osp/app/signin/AccountInfoListView;->a:Landroid/widget/LinearLayout;

    .line 73
    const v0, 0x7f0c002a

    invoke-virtual {p0, v0}, Lcom/osp/app/signin/AccountInfoListView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcom/osp/app/signin/AccountInfoListView;->b:Landroid/widget/LinearLayout;

    .line 74
    iget-object v0, p0, Lcom/osp/app/signin/AccountInfoListView;->b:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v4}, Landroid/widget/LinearLayout;->setMinimumHeight(I)V

    .line 80
    const v0, 0x7f0c0032

    invoke-virtual {p0, v0}, Lcom/osp/app/signin/AccountInfoListView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/osp/app/signin/AccountInfoListView;->c:Landroid/widget/TextView;

    .line 81
    const v0, 0x7f0c0033

    invoke-virtual {p0, v0}, Lcom/osp/app/signin/AccountInfoListView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/osp/app/signin/AccountInfoListView;->d:Landroid/widget/TextView;

    .line 83
    const v0, 0x7f0c002b

    invoke-virtual {p0, v0}, Lcom/osp/app/signin/AccountInfoListView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/osp/app/signin/AccountInfoListView;->e:Landroid/widget/TextView;

    .line 84
    const v0, 0x7f0c002c

    invoke-virtual {p0, v0}, Lcom/osp/app/signin/AccountInfoListView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/EditText;

    iput-object v0, p0, Lcom/osp/app/signin/AccountInfoListView;->f:Landroid/widget/EditText;

    .line 85
    const v0, 0x7f0c002d

    invoke-virtual {p0, v0}, Lcom/osp/app/signin/AccountInfoListView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/osp/app/signin/AccountInfoListView;->g:Landroid/widget/TextView;

    .line 86
    const v0, 0x7f0c002e

    invoke-virtual {p0, v0}, Lcom/osp/app/signin/AccountInfoListView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Spinner;

    iput-object v0, p0, Lcom/osp/app/signin/AccountInfoListView;->h:Landroid/widget/Spinner;

    .line 88
    const v0, 0x7f0c0034

    invoke-virtual {p0, v0}, Lcom/osp/app/signin/AccountInfoListView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/osp/app/signin/AccountInfoListView;->i:Landroid/view/View;

    .line 89
    const v0, 0x7f0c002f

    invoke-virtual {p0, v0}, Lcom/osp/app/signin/AccountInfoListView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/osp/app/signin/AccountInfoListView;->j:Landroid/view/View;

    .line 93
    iget-object v0, p0, Lcom/osp/app/signin/AccountInfoListView;->k:Lcom/osp/app/util/BaseActivity;

    invoke-static {}, Lcom/osp/app/util/r;->v()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 95
    iget-object v0, p0, Lcom/osp/app/signin/AccountInfoListView;->j:Landroid/view/View;

    if-eqz v0, :cond_0

    .line 97
    iget-object v0, p0, Lcom/osp/app/signin/AccountInfoListView;->j:Landroid/view/View;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 100
    :cond_0
    invoke-virtual {p0}, Lcom/osp/app/signin/AccountInfoListView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    iget-object v1, p0, Lcom/osp/app/signin/AccountInfoListView;->k:Lcom/osp/app/util/BaseActivity;

    invoke-static {v1}, Lcom/osp/app/util/an;->a(Lcom/osp/app/util/BaseActivity;)Lcom/osp/app/util/an;

    const v1, 0x7f070004

    invoke-static {v1}, Lcom/osp/app/util/an;->a(I)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    iget-object v1, p0, Lcom/osp/app/signin/AccountInfoListView;->d:Landroid/widget/TextView;

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setTextColor(I)V

    invoke-virtual {p0}, Lcom/osp/app/signin/AccountInfoListView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    iget-object v1, p0, Lcom/osp/app/signin/AccountInfoListView;->k:Lcom/osp/app/util/BaseActivity;

    invoke-static {v1}, Lcom/osp/app/util/an;->a(Lcom/osp/app/util/BaseActivity;)Lcom/osp/app/util/an;

    const v1, 0x7f070008

    invoke-static {v1}, Lcom/osp/app/util/an;->a(I)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    invoke-virtual {p0}, Lcom/osp/app/signin/AccountInfoListView;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    iget-object v2, p0, Lcom/osp/app/signin/AccountInfoListView;->k:Lcom/osp/app/util/BaseActivity;

    invoke-static {v2}, Lcom/osp/app/util/an;->a(Lcom/osp/app/util/BaseActivity;)Lcom/osp/app/util/an;

    const v2, 0x7f070006

    invoke-static {v2}, Lcom/osp/app/util/an;->a(I)I

    move-result v2

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    iget-object v2, p0, Lcom/osp/app/signin/AccountInfoListView;->c:Landroid/widget/TextView;

    invoke-virtual {v2, v0}, Landroid/widget/TextView;->setTextColor(I)V

    iget-object v0, p0, Lcom/osp/app/signin/AccountInfoListView;->e:Landroid/widget/TextView;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setTextColor(I)V

    invoke-static {}, Lcom/osp/device/b;->b()Lcom/osp/device/b;

    move-result-object v0

    iget-object v1, p0, Lcom/osp/app/signin/AccountInfoListView;->k:Lcom/osp/app/util/BaseActivity;

    invoke-virtual {v0, v1}, Lcom/osp/device/b;->i(Landroid/content/Context;)Z

    move-result v0

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/osp/app/signin/AccountInfoListView;->k:Lcom/osp/app/util/BaseActivity;

    invoke-static {v0}, Lcom/osp/app/util/an;->a(Lcom/osp/app/util/BaseActivity;)Lcom/osp/app/util/an;

    const v0, 0x7f08007e

    invoke-static {v0}, Lcom/osp/app/util/an;->b(I)I

    move-result v1

    iget-object v0, p0, Lcom/osp/app/signin/AccountInfoListView;->k:Lcom/osp/app/util/BaseActivity;

    invoke-static {v0}, Lcom/osp/app/util/an;->a(Lcom/osp/app/util/BaseActivity;)Lcom/osp/app/util/an;

    const v0, 0x7f08007f

    invoke-static {v0}, Lcom/osp/app/util/an;->b(I)I

    move-result v0

    :goto_0
    iget-object v2, p0, Lcom/osp/app/signin/AccountInfoListView;->c:Landroid/widget/TextView;

    invoke-virtual {p0}, Lcom/osp/app/signin/AccountInfoListView;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    invoke-virtual {v3, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    int-to-float v1, v1

    invoke-virtual {v2, v4, v1}, Landroid/widget/TextView;->setTextSize(IF)V

    iget-object v1, p0, Lcom/osp/app/signin/AccountInfoListView;->d:Landroid/widget/TextView;

    invoke-virtual {p0}, Lcom/osp/app/signin/AccountInfoListView;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-virtual {v2, v0}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    int-to-float v0, v0

    invoke-virtual {v1, v4, v0}, Landroid/widget/TextView;->setTextSize(IF)V

    .line 101
    return-void

    .line 100
    :cond_1
    iget-object v0, p0, Lcom/osp/app/signin/AccountInfoListView;->k:Lcom/osp/app/util/BaseActivity;

    invoke-static {v0}, Lcom/osp/app/util/an;->a(Lcom/osp/app/util/BaseActivity;)Lcom/osp/app/util/an;

    const v0, 0x7f0800f4

    invoke-static {v0}, Lcom/osp/app/util/an;->b(I)I

    move-result v1

    iget-object v0, p0, Lcom/osp/app/signin/AccountInfoListView;->k:Lcom/osp/app/util/BaseActivity;

    invoke-static {v0}, Lcom/osp/app/util/an;->a(Lcom/osp/app/util/BaseActivity;)Lcom/osp/app/util/an;

    const v0, 0x7f0800e4

    invoke-static {v0}, Lcom/osp/app/util/an;->b(I)I

    move-result v0

    goto :goto_0
.end method
