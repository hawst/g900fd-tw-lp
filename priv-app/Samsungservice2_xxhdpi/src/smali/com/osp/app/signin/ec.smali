.class final Lcom/osp/app/signin/ec;
.super Ljava/lang/Object;
.source "ChecklistInfoPopupActivity.java"

# interfaces
.implements Landroid/content/DialogInterface$OnClickListener;


# instance fields
.field final synthetic a:Lcom/osp/app/signin/ChecklistInfoPopupActivity;


# direct methods
.method constructor <init>(Lcom/osp/app/signin/ChecklistInfoPopupActivity;)V
    .locals 0

    .prologue
    .line 71
    iput-object p1, p0, Lcom/osp/app/signin/ec;->a:Lcom/osp/app/signin/ChecklistInfoPopupActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onClick(Landroid/content/DialogInterface;I)V
    .locals 4

    .prologue
    const/4 v3, 0x1

    .line 75
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v0, "CIPA"

    const-string v1, "startUpdateTnCActivity()"

    invoke-static {v0, v1}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 76
    new-instance v0, Landroid/content/Intent;

    iget-object v1, p0, Lcom/osp/app/signin/ec;->a:Lcom/osp/app/signin/ChecklistInfoPopupActivity;

    const-class v2, Lcom/osp/app/signin/TnCView;

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 77
    iget-object v1, p0, Lcom/osp/app/signin/ec;->a:Lcom/osp/app/signin/ChecklistInfoPopupActivity;

    invoke-static {v1}, Lcom/osp/app/util/ad;->p(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v1

    .line 78
    const-string v2, "com.msc.action.samsungaccount.Update_NewTerms"

    invoke-virtual {v0, v2}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 79
    const-string v2, "key_tnc_update_mode"

    invoke-virtual {v0, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 80
    const-string v2, "key_hide_tnc_update_popup"

    invoke-virtual {v0, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 81
    const-string v2, "country_code_mcc"

    invoke-virtual {v0, v2, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 82
    iget-object v1, p0, Lcom/osp/app/signin/ec;->a:Lcom/osp/app/signin/ChecklistInfoPopupActivity;

    invoke-virtual {v1, v0}, Lcom/osp/app/signin/ChecklistInfoPopupActivity;->startActivity(Landroid/content/Intent;)V

    .line 84
    iget-object v0, p0, Lcom/osp/app/signin/ec;->a:Lcom/osp/app/signin/ChecklistInfoPopupActivity;

    const/4 v1, -0x1

    invoke-virtual {v0, v1}, Lcom/osp/app/signin/ChecklistInfoPopupActivity;->setResult(I)V

    .line 85
    iget-object v0, p0, Lcom/osp/app/signin/ec;->a:Lcom/osp/app/signin/ChecklistInfoPopupActivity;

    invoke-virtual {v0}, Lcom/osp/app/signin/ChecklistInfoPopupActivity;->finish()V

    .line 86
    return-void
.end method
