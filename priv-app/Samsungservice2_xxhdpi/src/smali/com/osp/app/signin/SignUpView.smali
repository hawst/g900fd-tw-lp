.class public Lcom/osp/app/signin/SignUpView;
.super Lcom/osp/app/util/BaseActivity;
.source "SignUpView.java"

# interfaces
.implements Landroid/view/View$OnClickListener;
.implements Landroid/view/View$OnFocusChangeListener;


# instance fields
.field private A:Landroid/widget/TextView;

.field private B:Landroid/widget/CheckBox;

.field private C:Landroid/widget/CheckBox;

.field private D:Landroid/widget/EditText;

.field private E:Landroid/widget/EditText;

.field private F:Landroid/widget/AutoCompleteTextView;

.field private G:Landroid/widget/EditText;

.field private H:Landroid/widget/EditText;

.field private I:Landroid/widget/EditText;

.field private J:Landroid/widget/EditText;

.field private K:Landroid/widget/EditText;

.field private L:Landroid/widget/EditText;

.field private M:Landroid/widget/EditText;

.field private N:Landroid/widget/EditText;

.field private O:Landroid/widget/EditText;

.field private P:Landroid/widget/EditText;

.field private Q:Landroid/widget/EditText;

.field private R:Landroid/widget/EditText;

.field private S:Landroid/widget/EditText;

.field private T:Landroid/widget/EditText;

.field private U:Z

.field private V:Z

.field private W:Ljava/lang/String;

.field private X:Z

.field private Y:Landroid/app/AlertDialog;

.field private Z:Landroid/app/AlertDialog;

.field final a:Landroid/view/View$OnClickListener;

.field private aA:I

.field private aB:I

.field private aC:I

.field private aD:Ljava/lang/String;

.field private aE:Ljava/lang/String;

.field private aF:Ljava/lang/String;

.field private aG:Ljava/lang/String;

.field private aH:Ljava/lang/String;

.field private aI:Ljava/lang/String;

.field private aJ:Ljava/lang/String;

.field private aK:Ljava/lang/String;

.field private aL:Ljava/lang/String;

.field private aM:Ljava/lang/String;

.field private aN:Ljava/lang/String;

.field private aO:Ljava/lang/String;

.field private aP:Ljava/lang/String;

.field private aQ:Z

.field private aR:J

.field private aS:Ljava/lang/String;

.field private aT:Z

.field private aU:Ljava/lang/String;

.field private aV:Ljava/lang/String;

.field private aW:I

.field private aX:Z

.field private aY:Z

.field private aZ:Ljava/lang/String;

.field private aa:Landroid/app/AlertDialog;

.field private ab:Landroid/app/AlertDialog;

.field private ac:Landroid/app/AlertDialog;

.field private ad:Landroid/app/AlertDialog;

.field private ae:Landroid/app/AlertDialog;

.field private af:Landroid/app/AlertDialog;

.field private ag:Ljava/lang/String;

.field private ah:Ljava/lang/String;

.field private ai:Landroid/content/Intent;

.field private aj:Ljava/lang/String;

.field private ak:Z

.field private al:Ljava/lang/String;

.field private am:Ljava/lang/String;

.field private an:Ljava/lang/String;

.field private ao:Ljava/lang/String;

.field private ap:Landroid/app/AlertDialog;

.field private aq:Lcom/osp/app/signin/SignUpinfo;

.field private ar:Ljava/lang/String;

.field private as:Ljava/lang/String;

.field private at:Landroid/app/Dialog;

.field private final au:Landroid/app/DatePickerDialog$OnDateSetListener;

.field private av:I

.field private aw:Ljava/util/ArrayList;

.field private ax:Landroid/view/LayoutInflater;

.field private ay:I

.field private az:I

.field final b:Landroid/view/View$OnClickListener;

.field private bA:Lcom/osp/app/signin/lz;

.field private bB:Lcom/osp/app/signin/kd;

.field private bC:Lcom/osp/app/signin/kd;

.field private bD:Lcom/osp/app/signin/kd;

.field private ba:Ljava/lang/String;

.field private bb:Landroid/os/Bundle;

.field private bc:Ljava/lang/String;

.field private bd:Z

.field private be:Ljava/util/ArrayList;

.field private bf:Z

.field private bg:Ljava/lang/String;

.field private bh:Ljava/lang/String;

.field private bi:Lcom/osp/app/signin/pz;

.field private bj:Ljava/lang/String;

.field private bk:Z

.field private bl:Z

.field private bm:Ljava/util/ArrayList;

.field private bn:Z

.field private bo:Z

.field private bp:Z

.field private bq:Lcom/msc/a/j;

.field private br:J

.field private bs:Ljava/lang/String;

.field private bt:Lcom/osp/app/signin/ma;

.field private bu:Lcom/osp/app/signin/mc;

.field private bv:Z

.field private bw:Z

.field private bx:Lcom/osp/app/signin/BottomSoftkeyLinearLayout;

.field private by:Z

.field private final bz:Landroid/view/View$OnKeyListener;

.field private final c:Ljava/lang/String;

.field private d:I

.field private e:I

.field private f:I

.field private y:Landroid/widget/LinearLayout;

.field private z:Landroid/widget/ImageView;


# direct methods
.method public constructor <init>()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x0

    .line 112
    invoke-direct {p0}, Lcom/osp/app/util/BaseActivity;-><init>()V

    .line 119
    const-string v0, "key_setup_wizard_mode"

    iput-object v0, p0, Lcom/osp/app/signin/SignUpView;->c:Ljava/lang/String;

    .line 264
    iput-boolean v2, p0, Lcom/osp/app/signin/SignUpView;->U:Z

    .line 269
    iput-boolean v2, p0, Lcom/osp/app/signin/SignUpView;->V:Z

    .line 275
    iput-object v1, p0, Lcom/osp/app/signin/SignUpView;->W:Ljava/lang/String;

    .line 278
    iput-boolean v2, p0, Lcom/osp/app/signin/SignUpView;->X:Z

    .line 309
    iput-object v1, p0, Lcom/osp/app/signin/SignUpView;->ag:Ljava/lang/String;

    .line 313
    iput-object v1, p0, Lcom/osp/app/signin/SignUpView;->ah:Ljava/lang/String;

    .line 319
    iput-object v1, p0, Lcom/osp/app/signin/SignUpView;->ai:Landroid/content/Intent;

    .line 330
    iput-boolean v2, p0, Lcom/osp/app/signin/SignUpView;->ak:Z

    .line 335
    iput-object v1, p0, Lcom/osp/app/signin/SignUpView;->al:Ljava/lang/String;

    .line 339
    iput-object v1, p0, Lcom/osp/app/signin/SignUpView;->am:Ljava/lang/String;

    .line 344
    iput-object v1, p0, Lcom/osp/app/signin/SignUpView;->an:Ljava/lang/String;

    .line 356
    iput-object v1, p0, Lcom/osp/app/signin/SignUpView;->ao:Ljava/lang/String;

    .line 372
    iput-object v1, p0, Lcom/osp/app/signin/SignUpView;->ar:Ljava/lang/String;

    .line 388
    new-instance v0, Lcom/osp/app/signin/kf;

    invoke-direct {v0, p0}, Lcom/osp/app/signin/kf;-><init>(Lcom/osp/app/signin/SignUpView;)V

    iput-object v0, p0, Lcom/osp/app/signin/SignUpView;->au:Landroid/app/DatePickerDialog$OnDateSetListener;

    .line 514
    iput-object v1, p0, Lcom/osp/app/signin/SignUpView;->aU:Ljava/lang/String;

    .line 518
    iput-object v1, p0, Lcom/osp/app/signin/SignUpView;->aV:Ljava/lang/String;

    .line 522
    iput v2, p0, Lcom/osp/app/signin/SignUpView;->aW:I

    .line 529
    iput-boolean v2, p0, Lcom/osp/app/signin/SignUpView;->aX:Z

    .line 570
    iput-boolean v2, p0, Lcom/osp/app/signin/SignUpView;->bd:Z

    .line 610
    iput-boolean v2, p0, Lcom/osp/app/signin/SignUpView;->bn:Z

    .line 615
    iput-boolean v2, p0, Lcom/osp/app/signin/SignUpView;->bo:Z

    .line 616
    iput-boolean v2, p0, Lcom/osp/app/signin/SignUpView;->bp:Z

    .line 642
    iput-object v1, p0, Lcom/osp/app/signin/SignUpView;->bt:Lcom/osp/app/signin/ma;

    .line 643
    iput-object v1, p0, Lcom/osp/app/signin/SignUpView;->bu:Lcom/osp/app/signin/mc;

    .line 645
    iput-boolean v2, p0, Lcom/osp/app/signin/SignUpView;->bv:Z

    .line 647
    iput-boolean v2, p0, Lcom/osp/app/signin/SignUpView;->bw:Z

    .line 654
    iput-boolean v2, p0, Lcom/osp/app/signin/SignUpView;->by:Z

    .line 692
    new-instance v0, Lcom/osp/app/signin/kr;

    invoke-direct {v0, p0}, Lcom/osp/app/signin/kr;-><init>(Lcom/osp/app/signin/SignUpView;)V

    iput-object v0, p0, Lcom/osp/app/signin/SignUpView;->bz:Landroid/view/View$OnKeyListener;

    .line 719
    new-instance v0, Lcom/osp/app/signin/le;

    invoke-direct {v0, p0}, Lcom/osp/app/signin/le;-><init>(Lcom/osp/app/signin/SignUpView;)V

    iput-object v0, p0, Lcom/osp/app/signin/SignUpView;->a:Landroid/view/View$OnClickListener;

    .line 730
    new-instance v0, Lcom/osp/app/signin/lm;

    invoke-direct {v0, p0}, Lcom/osp/app/signin/lm;-><init>(Lcom/osp/app/signin/SignUpView;)V

    iput-object v0, p0, Lcom/osp/app/signin/SignUpView;->b:Landroid/view/View$OnClickListener;

    .line 6859
    iput-object v1, p0, Lcom/osp/app/signin/SignUpView;->bB:Lcom/osp/app/signin/kd;

    .line 6863
    iput-object v1, p0, Lcom/osp/app/signin/SignUpView;->bC:Lcom/osp/app/signin/kd;

    .line 6867
    iput-object v1, p0, Lcom/osp/app/signin/SignUpView;->bD:Lcom/osp/app/signin/kd;

    .line 9394
    return-void
.end method

.method static synthetic A(Lcom/osp/app/signin/SignUpView;)Landroid/app/AlertDialog;
    .locals 1

    .prologue
    .line 112
    iget-object v0, p0, Lcom/osp/app/signin/SignUpView;->Z:Landroid/app/AlertDialog;

    return-object v0
.end method

.method private A()Ljava/lang/String;
    .locals 2

    .prologue
    .line 9253
    invoke-static {p0}, Landroid/accounts/AccountManager;->get(Landroid/content/Context;)Landroid/accounts/AccountManager;

    move-result-object v0

    .line 9254
    const-string v1, "com.google"

    invoke-virtual {v0, v1}, Landroid/accounts/AccountManager;->getAccountsByType(Ljava/lang/String;)[Landroid/accounts/Account;

    move-result-object v0

    .line 9256
    if-eqz v0, :cond_0

    array-length v1, v0

    if-lez v1, :cond_0

    .line 9258
    const/4 v1, 0x0

    aget-object v0, v0, v1

    iget-object v0, v0, Landroid/accounts/Account;->name:Ljava/lang/String;

    .line 9261
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method static synthetic B(Lcom/osp/app/signin/SignUpView;)Lcom/osp/app/signin/SignUpinfo;
    .locals 1

    .prologue
    .line 112
    iget-object v0, p0, Lcom/osp/app/signin/SignUpView;->aq:Lcom/osp/app/signin/SignUpinfo;

    return-object v0
.end method

.method static synthetic C(Lcom/osp/app/signin/SignUpView;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 112
    iget-object v0, p0, Lcom/osp/app/signin/SignUpView;->aZ:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic D(Lcom/osp/app/signin/SignUpView;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 112
    iget-object v0, p0, Lcom/osp/app/signin/SignUpView;->aN:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic E(Lcom/osp/app/signin/SignUpView;)Landroid/widget/EditText;
    .locals 1

    .prologue
    .line 112
    iget-object v0, p0, Lcom/osp/app/signin/SignUpView;->I:Landroid/widget/EditText;

    return-object v0
.end method

.method static synthetic F(Lcom/osp/app/signin/SignUpView;)Landroid/widget/EditText;
    .locals 1

    .prologue
    .line 112
    iget-object v0, p0, Lcom/osp/app/signin/SignUpView;->J:Landroid/widget/EditText;

    return-object v0
.end method

.method static synthetic G(Lcom/osp/app/signin/SignUpView;)Landroid/widget/TextView;
    .locals 1

    .prologue
    .line 112
    iget-object v0, p0, Lcom/osp/app/signin/SignUpView;->A:Landroid/widget/TextView;

    return-object v0
.end method

.method static synthetic H(Lcom/osp/app/signin/SignUpView;)Lcom/osp/app/signin/pz;
    .locals 1

    .prologue
    .line 112
    iget-object v0, p0, Lcom/osp/app/signin/SignUpView;->bi:Lcom/osp/app/signin/pz;

    return-object v0
.end method

.method static synthetic I(Lcom/osp/app/signin/SignUpView;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 112
    iget-object v0, p0, Lcom/osp/app/signin/SignUpView;->bg:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic J(Lcom/osp/app/signin/SignUpView;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 112
    iget-object v0, p0, Lcom/osp/app/signin/SignUpView;->bh:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic K(Lcom/osp/app/signin/SignUpView;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 112
    iget-object v0, p0, Lcom/osp/app/signin/SignUpView;->aj:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic L(Lcom/osp/app/signin/SignUpView;)Lcom/osp/app/signin/lz;
    .locals 1

    .prologue
    .line 112
    iget-object v0, p0, Lcom/osp/app/signin/SignUpView;->bA:Lcom/osp/app/signin/lz;

    return-object v0
.end method

.method static synthetic M(Lcom/osp/app/signin/SignUpView;)V
    .locals 4

    .prologue
    .line 112
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    const-class v1, Lcom/osp/app/signin/SelectCountryView;

    invoke-virtual {v0, p0, v1}, Landroid/content/Intent;->setClass(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;

    const-string v1, "MODE"

    iget-object v2, p0, Lcom/osp/app/signin/SignUpView;->ao:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    const-string v1, "BG_WhoareU"

    iget-object v2, p0, Lcom/osp/app/signin/SignUpView;->aV:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    const-string v1, "com.osp.app.signin.action.SAMSUNG_ACCOUNT_SETUPWIZARD"

    invoke-static {}, Lcom/osp/app/signin/SamsungService;->a()Z

    move-result v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    const-string v1, "BG_mode"

    iget-object v2, p0, Lcom/osp/app/signin/SignUpView;->aV:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/osp/app/signin/SignUpView;->ar:Ljava/lang/String;

    if-eqz v1, :cond_0

    const-string v1, "mypackage"

    iget-object v2, p0, Lcom/osp/app/signin/SignUpView;->ar:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    const-string v1, "key_request_id"

    iget-wide v2, p0, Lcom/osp/app/signin/SignUpView;->aR:J

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;J)Landroid/content/Intent;

    :cond_0
    const/16 v1, 0xd0

    invoke-virtual {p0, v0, v1}, Lcom/osp/app/signin/SignUpView;->startActivityForResult(Landroid/content/Intent;I)V

    return-void
.end method

.method static synthetic N(Lcom/osp/app/signin/SignUpView;)V
    .locals 0

    .prologue
    .line 112
    invoke-virtual {p0}, Lcom/osp/app/signin/SignUpView;->m()V

    return-void
.end method

.method static synthetic O(Lcom/osp/app/signin/SignUpView;)Lcom/osp/app/signin/kd;
    .locals 1

    .prologue
    .line 112
    iget-object v0, p0, Lcom/osp/app/signin/SignUpView;->bB:Lcom/osp/app/signin/kd;

    return-object v0
.end method

.method static synthetic P(Lcom/osp/app/signin/SignUpView;)V
    .locals 5

    .prologue
    const/4 v4, 0x1

    .line 112
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v0, "TnCView - ShowNameValidationView"

    invoke-static {v0}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;)V

    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    const-class v1, Lcom/osp/app/signin/NameValidationWebView;

    invoke-virtual {v0, p0, v1}, Landroid/content/Intent;->setClass(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;

    const-string v1, "is_signup_flow"

    invoke-virtual {v0, v1, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    const-string v1, "key_request_id"

    iget-wide v2, p0, Lcom/osp/app/signin/SignUpView;->aR:J

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;J)Landroid/content/Intent;

    const-string v1, "is_cancelable_just_one_activity"

    invoke-virtual {v0, v1, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    const/16 v1, 0xca

    invoke-virtual {p0, v0, v1}, Lcom/osp/app/signin/SignUpView;->startActivityForResult(Landroid/content/Intent;I)V

    return-void
.end method

.method static synthetic Q(Lcom/osp/app/signin/SignUpView;)V
    .locals 0

    .prologue
    .line 112
    invoke-direct {p0}, Lcom/osp/app/signin/SignUpView;->d()V

    return-void
.end method

.method static synthetic R(Lcom/osp/app/signin/SignUpView;)Lcom/osp/app/signin/kd;
    .locals 1

    .prologue
    .line 112
    iget-object v0, p0, Lcom/osp/app/signin/SignUpView;->bC:Lcom/osp/app/signin/kd;

    return-object v0
.end method

.method static synthetic S(Lcom/osp/app/signin/SignUpView;)Lcom/osp/app/signin/kd;
    .locals 1

    .prologue
    .line 112
    iget-object v0, p0, Lcom/osp/app/signin/SignUpView;->bD:Lcom/osp/app/signin/kd;

    return-object v0
.end method

.method static synthetic T(Lcom/osp/app/signin/SignUpView;)Z
    .locals 1

    .prologue
    .line 112
    iget-boolean v0, p0, Lcom/osp/app/signin/SignUpView;->bl:Z

    return v0
.end method

.method static synthetic U(Lcom/osp/app/signin/SignUpView;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 112
    iget-object v0, p0, Lcom/osp/app/signin/SignUpView;->ag:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic V(Lcom/osp/app/signin/SignUpView;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 112
    iget-object v0, p0, Lcom/osp/app/signin/SignUpView;->aG:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic W(Lcom/osp/app/signin/SignUpView;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 112
    iget-object v0, p0, Lcom/osp/app/signin/SignUpView;->aH:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic X(Lcom/osp/app/signin/SignUpView;)V
    .locals 0

    .prologue
    .line 112
    invoke-direct {p0}, Lcom/osp/app/signin/SignUpView;->u()V

    return-void
.end method

.method static synthetic Y(Lcom/osp/app/signin/SignUpView;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 112
    iget-object v0, p0, Lcom/osp/app/signin/SignUpView;->al:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic Z(Lcom/osp/app/signin/SignUpView;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 112
    iget-object v0, p0, Lcom/osp/app/signin/SignUpView;->am:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic a(Lcom/osp/app/signin/SignUpView;I)I
    .locals 0

    .prologue
    .line 112
    iput p1, p0, Lcom/osp/app/signin/SignUpView;->az:I

    return p1
.end method

.method static synthetic a(Lcom/osp/app/signin/SignUpView;Landroid/app/AlertDialog;)Landroid/app/AlertDialog;
    .locals 0

    .prologue
    .line 112
    iput-object p1, p0, Lcom/osp/app/signin/SignUpView;->Y:Landroid/app/AlertDialog;

    return-object p1
.end method

.method static synthetic a(Lcom/osp/app/signin/SignUpView;Landroid/os/Bundle;)Landroid/os/Bundle;
    .locals 0

    .prologue
    .line 112
    iput-object p1, p0, Lcom/osp/app/signin/SignUpView;->bb:Landroid/os/Bundle;

    return-object p1
.end method

.method private a(Landroid/widget/LinearLayout;Ljava/lang/String;ZII)Landroid/widget/EditText;
    .locals 6

    .prologue
    .line 776
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "SignUpView::addDynamicLayout fieldTitle : "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;)V

    .line 778
    iget-object v0, p0, Lcom/osp/app/signin/SignUpView;->ax:Landroid/view/LayoutInflater;

    const v1, 0x7f030001

    const/4 v2, 0x0

    invoke-virtual {v0, v1, p1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    .line 784
    const v1, 0x7f0c002a

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/LinearLayout;

    .line 785
    const v2, 0x7f0c002b

    invoke-virtual {v0, v2}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    .line 786
    const v3, 0x7f0c002c

    invoke-virtual {v0, v3}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/EditText;

    .line 787
    const v4, 0x7f0c002e

    invoke-virtual {v0, v4}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/Spinner;

    .line 792
    invoke-static {p4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v0, v5}, Landroid/widget/LinearLayout;->setTag(Ljava/lang/Object;)V

    .line 793
    invoke-static {p4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v3, v5}, Landroid/widget/EditText;->setTag(Ljava/lang/Object;)V

    .line 804
    const/4 v5, 0x0

    invoke-virtual {v1, v5}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 805
    const/4 v5, 0x0

    invoke-virtual {v1, v5}, Landroid/widget/LinearLayout;->setMinimumHeight(I)V

    .line 806
    const/16 v1, 0x8

    invoke-virtual {v4, v1}, Landroid/widget/Spinner;->setVisibility(I)V

    .line 807
    const/16 v1, 0x8

    invoke-virtual {v3, v1}, Landroid/widget/EditText;->setVisibility(I)V

    .line 809
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    if-eqz p3, :cond_1

    const-string v1, " *"

    :goto_0
    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v2, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 811
    const/4 v1, 0x1

    invoke-virtual {v2, v1}, Landroid/widget/TextView;->setClickable(Z)V

    .line 812
    invoke-virtual {v2, p2}, Landroid/widget/TextView;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 815
    invoke-virtual {p0}, Lcom/osp/app/signin/SignUpView;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    .line 817
    invoke-static {p0}, Lcom/osp/app/util/an;->a(Lcom/osp/app/util/BaseActivity;)Lcom/osp/app/util/an;

    const v5, 0x7f070006

    invoke-static {v5}, Lcom/osp/app/util/an;->a(I)I

    move-result v5

    invoke-virtual {v1, v5}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    invoke-virtual {v2, v1}, Landroid/widget/TextView;->setTextColor(I)V

    .line 835
    invoke-static {}, Lcom/osp/device/b;->b()Lcom/osp/device/b;

    move-result-object v1

    invoke-virtual {v1, p0}, Lcom/osp/device/b;->i(Landroid/content/Context;)Z

    invoke-virtual {p0, p2, v3}, Lcom/osp/app/signin/SignUpView;->a(Ljava/lang/String;Landroid/widget/EditText;)Landroid/widget/EditText;

    move-result-object v2

    .line 836
    iget-object v1, p0, Lcom/osp/app/signin/SignUpView;->bz:Landroid/view/View$OnKeyListener;

    invoke-virtual {v2, v1}, Landroid/widget/EditText;->setOnKeyListener(Landroid/view/View$OnKeyListener;)V

    .line 837
    invoke-virtual {v2, p0}, Landroid/widget/EditText;->setOnFocusChangeListener(Landroid/view/View$OnFocusChangeListener;)V

    .line 956
    const/16 v1, 0x74

    if-ne p4, v1, :cond_4

    .line 958
    const v1, 0x7f09003d

    invoke-virtual {p0, v1}, Lcom/osp/app/signin/SignUpView;->getText(I)Ljava/lang/CharSequence;

    move-result-object v1

    invoke-virtual {v4, v1}, Landroid/widget/Spinner;->setPrompt(Ljava/lang/CharSequence;)V

    .line 960
    invoke-static {}, Lcom/osp/device/b;->b()Lcom/osp/device/b;

    move-result-object v1

    invoke-virtual {v1, p0}, Lcom/osp/device/b;->i(Landroid/content/Context;)Z

    move-result v1

    if-nez v1, :cond_2

    .line 962
    new-instance v1, Landroid/widget/ArrayAdapter;

    const v3, 0x7f030004

    iget-object v5, p0, Lcom/osp/app/signin/SignUpView;->aw:Ljava/util/ArrayList;

    invoke-direct {v1, p0, v3, v5}, Landroid/widget/ArrayAdapter;-><init>(Landroid/content/Context;ILjava/util/List;)V

    .line 973
    :goto_1
    const v3, 0x1090009

    invoke-virtual {v1, v3}, Landroid/widget/ArrayAdapter;->setDropDownViewResource(I)V

    .line 974
    invoke-virtual {v4, v1}, Landroid/widget/Spinner;->setAdapter(Landroid/widget/SpinnerAdapter;)V

    .line 975
    const/4 v1, 0x0

    invoke-virtual {v4, v1}, Landroid/widget/Spinner;->setSelection(I)V

    .line 977
    const/4 v1, 0x0

    invoke-virtual {v4, v1}, Landroid/widget/Spinner;->setVisibility(I)V

    .line 1072
    :cond_0
    :goto_2
    new-instance v1, Lcom/osp/app/signin/lq;

    invoke-direct {v1, p0, p4}, Lcom/osp/app/signin/lq;-><init>(Lcom/osp/app/signin/SignUpView;I)V

    invoke-virtual {v4, v1}, Landroid/widget/Spinner;->setOnItemSelectedListener(Landroid/widget/AdapterView$OnItemSelectedListener;)V

    .line 1167
    new-instance v1, Lcom/osp/app/signin/lr;

    invoke-direct {v1, p0}, Lcom/osp/app/signin/lr;-><init>(Lcom/osp/app/signin/SignUpView;)V

    invoke-virtual {v2, v1}, Landroid/widget/EditText;->addTextChangedListener(Landroid/text/TextWatcher;)V

    .line 1270
    invoke-static {}, Lcom/osp/device/b;->b()Lcom/osp/device/b;

    move-result-object v1

    invoke-virtual {v1}, Lcom/osp/device/b;->a()Z

    move-result v1

    if-eqz v1, :cond_c

    if-ltz p5, :cond_c

    .line 1272
    invoke-virtual {p1, v0, p5}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;I)V

    .line 1319
    :goto_3
    return-object v2

    .line 809
    :cond_1
    const-string v1, ""

    goto/16 :goto_0

    .line 965
    :cond_2
    invoke-virtual {p0}, Lcom/osp/app/signin/SignUpView;->h()Z

    move-result v1

    if-eqz v1, :cond_3

    .line 967
    new-instance v1, Landroid/widget/ArrayAdapter;

    const v3, 0x7f03005e

    iget-object v5, p0, Lcom/osp/app/signin/SignUpView;->aw:Ljava/util/ArrayList;

    invoke-direct {v1, p0, v3, v5}, Landroid/widget/ArrayAdapter;-><init>(Landroid/content/Context;ILjava/util/List;)V

    goto :goto_1

    .line 970
    :cond_3
    new-instance v1, Landroid/widget/ArrayAdapter;

    const v3, 0x7f03005f

    iget-object v5, p0, Lcom/osp/app/signin/SignUpView;->aw:Ljava/util/ArrayList;

    invoke-direct {v1, p0, v3, v5}, Landroid/widget/ArrayAdapter;-><init>(Landroid/content/Context;ILjava/util/List;)V

    goto :goto_1

    .line 978
    :cond_4
    const/16 v1, 0x7e

    if-ne p4, v1, :cond_7

    .line 980
    const/4 v1, 0x2

    new-array v3, v1, [Ljava/lang/String;

    const/4 v1, 0x0

    const v5, 0x7f09003a

    invoke-virtual {p0, v5}, Lcom/osp/app/signin/SignUpView;->getString(I)Ljava/lang/String;

    move-result-object v5

    aput-object v5, v3, v1

    const/4 v1, 0x1

    const v5, 0x7f090029

    invoke-virtual {p0, v5}, Lcom/osp/app/signin/SignUpView;->getString(I)Ljava/lang/String;

    move-result-object v5

    aput-object v5, v3, v1

    .line 981
    const v1, 0x7f09002b

    invoke-virtual {p0, v1}, Lcom/osp/app/signin/SignUpView;->getText(I)Ljava/lang/CharSequence;

    move-result-object v1

    invoke-virtual {v4, v1}, Landroid/widget/Spinner;->setPrompt(Ljava/lang/CharSequence;)V

    .line 983
    invoke-static {}, Lcom/osp/device/b;->b()Lcom/osp/device/b;

    move-result-object v1

    invoke-virtual {v1, p0}, Lcom/osp/device/b;->i(Landroid/content/Context;)Z

    move-result v1

    if-nez v1, :cond_5

    .line 985
    new-instance v1, Landroid/widget/ArrayAdapter;

    const v5, 0x7f030004

    invoke-direct {v1, p0, v5, v3}, Landroid/widget/ArrayAdapter;-><init>(Landroid/content/Context;I[Ljava/lang/Object;)V

    .line 996
    :goto_4
    const v3, 0x1090009

    invoke-virtual {v1, v3}, Landroid/widget/ArrayAdapter;->setDropDownViewResource(I)V

    .line 998
    const/4 v3, 0x0

    invoke-virtual {v4, v3}, Landroid/widget/Spinner;->setVisibility(I)V

    .line 999
    invoke-virtual {v4, v1}, Landroid/widget/Spinner;->setAdapter(Landroid/widget/SpinnerAdapter;)V

    .line 1000
    const/4 v1, 0x0

    invoke-virtual {v4, v1}, Landroid/widget/Spinner;->setSelection(I)V

    goto/16 :goto_2

    .line 988
    :cond_5
    invoke-virtual {p0}, Lcom/osp/app/signin/SignUpView;->h()Z

    move-result v1

    if-eqz v1, :cond_6

    .line 990
    new-instance v1, Landroid/widget/ArrayAdapter;

    const v5, 0x7f03005e

    invoke-direct {v1, p0, v5, v3}, Landroid/widget/ArrayAdapter;-><init>(Landroid/content/Context;I[Ljava/lang/Object;)V

    goto :goto_4

    .line 993
    :cond_6
    new-instance v1, Landroid/widget/ArrayAdapter;

    const v5, 0x7f03005f

    invoke-direct {v1, p0, v5, v3}, Landroid/widget/ArrayAdapter;-><init>(Landroid/content/Context;I[Ljava/lang/Object;)V

    goto :goto_4

    .line 1001
    :cond_7
    const/16 v1, 0x80

    if-ne p4, v1, :cond_a

    .line 1003
    const/4 v1, 0x2

    new-array v3, v1, [Ljava/lang/String;

    const/4 v1, 0x0

    const v5, 0x7f090057

    invoke-virtual {p0, v5}, Lcom/osp/app/signin/SignUpView;->getString(I)Ljava/lang/String;

    move-result-object v5

    aput-object v5, v3, v1

    const/4 v1, 0x1

    const v5, 0x7f09003c

    invoke-virtual {p0, v5}, Lcom/osp/app/signin/SignUpView;->getString(I)Ljava/lang/String;

    move-result-object v5

    aput-object v5, v3, v1

    .line 1004
    const v1, 0x7f09003b

    invoke-virtual {p0, v1}, Lcom/osp/app/signin/SignUpView;->getText(I)Ljava/lang/CharSequence;

    move-result-object v1

    invoke-virtual {v4, v1}, Landroid/widget/Spinner;->setPrompt(Ljava/lang/CharSequence;)V

    .line 1006
    invoke-static {}, Lcom/osp/device/b;->b()Lcom/osp/device/b;

    move-result-object v1

    invoke-virtual {v1, p0}, Lcom/osp/device/b;->i(Landroid/content/Context;)Z

    move-result v1

    if-nez v1, :cond_8

    .line 1008
    new-instance v1, Landroid/widget/ArrayAdapter;

    const v5, 0x7f030004

    invoke-direct {v1, p0, v5, v3}, Landroid/widget/ArrayAdapter;-><init>(Landroid/content/Context;I[Ljava/lang/Object;)V

    .line 1019
    :goto_5
    const v3, 0x1090009

    invoke-virtual {v1, v3}, Landroid/widget/ArrayAdapter;->setDropDownViewResource(I)V

    .line 1020
    invoke-virtual {v4, v1}, Landroid/widget/Spinner;->setAdapter(Landroid/widget/SpinnerAdapter;)V

    .line 1021
    const/4 v1, 0x0

    invoke-virtual {v4, v1}, Landroid/widget/Spinner;->setSelection(I)V

    .line 1023
    const/4 v1, 0x0

    invoke-virtual {v4, v1}, Landroid/widget/Spinner;->setVisibility(I)V

    goto/16 :goto_2

    .line 1011
    :cond_8
    invoke-virtual {p0}, Lcom/osp/app/signin/SignUpView;->h()Z

    move-result v1

    if-eqz v1, :cond_9

    .line 1013
    new-instance v1, Landroid/widget/ArrayAdapter;

    const v5, 0x7f03005e

    invoke-direct {v1, p0, v5, v3}, Landroid/widget/ArrayAdapter;-><init>(Landroid/content/Context;I[Ljava/lang/Object;)V

    goto :goto_5

    .line 1016
    :cond_9
    new-instance v1, Landroid/widget/ArrayAdapter;

    const v5, 0x7f03005f

    invoke-direct {v1, p0, v5, v3}, Landroid/widget/ArrayAdapter;-><init>(Landroid/content/Context;I[Ljava/lang/Object;)V

    goto :goto_5

    .line 1055
    :cond_a
    const/4 v1, 0x0

    invoke-virtual {v2, v1}, Landroid/widget/EditText;->setVisibility(I)V

    .line 1056
    if-eqz p3, :cond_b

    .line 1058
    iget-object v1, p0, Lcom/osp/app/signin/SignUpView;->be:Ljava/util/ArrayList;

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1062
    :cond_b
    sget v1, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v3, 0x15

    if-lt v1, v3, :cond_0

    .line 1064
    const v1, 0x7f0c002f

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v1

    .line 1065
    if-eqz v1, :cond_0

    .line 1067
    const/16 v3, 0x8

    invoke-virtual {v1, v3}, Landroid/view/View;->setVisibility(I)V

    goto/16 :goto_2

    .line 1275
    :cond_c
    invoke-virtual {p1, v0}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    goto/16 :goto_3
.end method

.method static synthetic a(Lcom/osp/app/signin/SignUpView;Landroid/widget/EditText;)Landroid/widget/EditText;
    .locals 0

    .prologue
    .line 112
    iput-object p1, p0, Lcom/osp/app/signin/SignUpView;->T:Landroid/widget/EditText;

    return-object p1
.end method

.method static synthetic a(Lcom/osp/app/signin/SignUpView;Lcom/msc/a/j;)Lcom/msc/a/j;
    .locals 0

    .prologue
    .line 112
    iput-object p1, p0, Lcom/osp/app/signin/SignUpView;->bq:Lcom/msc/a/j;

    return-object p1
.end method

.method static synthetic a(Lcom/osp/app/signin/SignUpView;Lcom/osp/app/signin/kd;)Lcom/osp/app/signin/kd;
    .locals 0

    .prologue
    .line 112
    iput-object p1, p0, Lcom/osp/app/signin/SignUpView;->bB:Lcom/osp/app/signin/kd;

    return-object p1
.end method

.method static synthetic a(Lcom/osp/app/signin/SignUpView;Lcom/osp/app/signin/lz;)Lcom/osp/app/signin/lz;
    .locals 0

    .prologue
    .line 112
    iput-object p1, p0, Lcom/osp/app/signin/SignUpView;->bA:Lcom/osp/app/signin/lz;

    return-object p1
.end method

.method static synthetic a(Lcom/osp/app/signin/SignUpView;Ljava/lang/String;)Ljava/lang/String;
    .locals 0

    .prologue
    .line 112
    iput-object p1, p0, Lcom/osp/app/signin/SignUpView;->aO:Ljava/lang/String;

    return-object p1
.end method

.method static synthetic a(Lcom/osp/app/signin/SignUpView;Ljava/util/Date;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 112
    invoke-direct {p0, p1}, Lcom/osp/app/signin/SignUpView;->a(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private a(Ljava/util/Date;)Ljava/lang/String;
    .locals 2

    .prologue
    .line 1442
    invoke-static {p0}, Lcom/osp/app/util/ad;->q(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v0

    .line 1444
    new-instance v1, Ljava/text/SimpleDateFormat;

    invoke-direct {v1, v0}, Ljava/text/SimpleDateFormat;-><init>(Ljava/lang/String;)V

    .line 1445
    invoke-virtual {v1, p1}, Ljava/text/SimpleDateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v0

    .line 1446
    return-object v0
.end method

.method static synthetic a(Lcom/osp/app/signin/SignUpView;Ljava/util/ArrayList;)Ljava/util/ArrayList;
    .locals 0

    .prologue
    .line 112
    iput-object p1, p0, Lcom/osp/app/signin/SignUpView;->aw:Ljava/util/ArrayList;

    return-object p1
.end method

.method private a(I)V
    .locals 12

    .prologue
    .line 2920
    const v0, 0x7f0c0149

    invoke-virtual {p0, v0}, Lcom/osp/app/signin/SignUpView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    .line 2921
    const v1, 0x7f0c014d

    invoke-virtual {p0, v1}, Lcom/osp/app/signin/SignUpView;->findViewById(I)Landroid/view/View;

    move-result-object v6

    .line 2922
    const v1, 0x7f0c014e

    invoke-virtual {p0, v1}, Lcom/osp/app/signin/SignUpView;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/LinearLayout;

    .line 2923
    const v2, 0x7f0c0151

    invoke-virtual {p0, v2}, Lcom/osp/app/signin/SignUpView;->findViewById(I)Landroid/view/View;

    move-result-object v7

    .line 2924
    const v2, 0x7f0c0004

    invoke-virtual {p0, v2}, Lcom/osp/app/signin/SignUpView;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    .line 2925
    const v3, 0x7f0c013a

    invoke-virtual {p0, v3}, Lcom/osp/app/signin/SignUpView;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/AutoCompleteTextView;

    .line 2928
    const v4, 0x7f0c0139

    invoke-virtual {p0, v4}, Lcom/osp/app/signin/SignUpView;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/LinearLayout;

    .line 2929
    const v5, 0x7f0c013b

    invoke-virtual {p0, v5}, Lcom/osp/app/signin/SignUpView;->findViewById(I)Landroid/view/View;

    move-result-object v8

    .line 2931
    if-eqz v0, :cond_0

    if-eqz v6, :cond_0

    if-eqz v1, :cond_0

    if-eqz v7, :cond_0

    if-eqz v2, :cond_0

    if-eqz v3, :cond_0

    .line 2934
    packed-switch p1, :pswitch_data_0

    .line 3126
    :goto_0
    invoke-static {}, Lcom/osp/device/b;->b()Lcom/osp/device/b;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/osp/device/b;->i(Landroid/content/Context;)Z

    move-result v0

    if-nez v0, :cond_10

    .line 3152
    const v0, 0x7f0c00c9

    invoke-virtual {p0, v0}, Lcom/osp/app/signin/SignUpView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    const/4 v1, 0x0

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/view/View;->scrollTo(II)V

    .line 3157
    :goto_1
    invoke-virtual {v3}, Landroid/widget/EditText;->requestFocus()Z

    .line 3158
    invoke-direct {p0}, Lcom/osp/app/signin/SignUpView;->r()V

    .line 3160
    :cond_0
    invoke-virtual {p0}, Lcom/osp/app/signin/SignUpView;->a()V

    .line 3161
    return-void

    .line 2937
    :pswitch_0
    const-string v5, "CN"

    invoke-static {}, Lcom/osp/device/b;->b()Lcom/osp/device/b;

    move-result-object v9

    invoke-virtual {v9}, Lcom/osp/device/b;->n()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v5, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-nez v5, :cond_1

    iget-boolean v5, p0, Lcom/osp/app/signin/SignUpView;->bw:Z

    if-nez v5, :cond_1

    .line 2939
    const v5, 0x7f0c0134

    invoke-virtual {p0, v5}, Lcom/osp/app/signin/SignUpView;->findViewById(I)Landroid/view/View;

    move-result-object v5

    check-cast v5, Landroid/widget/LinearLayout;

    .line 2941
    if-eqz v5, :cond_1

    .line 2943
    const/4 v9, 0x0

    invoke-virtual {v5, v9}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 2945
    const v5, 0x7f0c0138

    invoke-virtual {p0, v5}, Lcom/osp/app/signin/SignUpView;->findViewById(I)Landroid/view/View;

    move-result-object v5

    .line 2946
    if-eqz v5, :cond_1

    .line 2948
    invoke-static {}, Lcom/osp/app/util/r;->v()Z

    move-result v9

    if-eqz v9, :cond_4

    .line 2950
    const/16 v9, 0x8

    invoke-virtual {v5, v9}, Landroid/view/View;->setVisibility(I)V

    .line 2958
    :cond_1
    :goto_2
    iget-object v5, p0, Lcom/osp/app/signin/SignUpView;->bC:Lcom/osp/app/signin/kd;

    iput-object v5, p0, Lcom/osp/app/signin/SignUpView;->bB:Lcom/osp/app/signin/kd;

    .line 2959
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const v9, 0x7f090023

    invoke-virtual {p0, v9}, Lcom/osp/app/signin/SignUpView;->getString(I)Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v5, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v9, " *"

    invoke-virtual {v5, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v2, v5}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 2960
    const/4 v2, 0x1

    invoke-virtual {v3, v2}, Landroid/widget/EditText;->setEnabled(Z)V

    .line 2961
    const/4 v2, 0x1

    invoke-virtual {v3, v2}, Landroid/widget/EditText;->setFocusable(Z)V

    .line 2962
    const/4 v2, 0x1

    invoke-virtual {v3, v2}, Landroid/widget/EditText;->setFocusableInTouchMode(Z)V

    .line 2964
    const-string v2, ""

    invoke-virtual {v3, v2}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    .line 2965
    const v2, 0x7f090112

    invoke-virtual {v3, v2}, Landroid/widget/EditText;->setHint(I)V

    .line 2966
    const/16 v2, 0x20

    invoke-virtual {v3, v2}, Landroid/widget/EditText;->setInputType(I)V

    .line 2967
    const/16 v2, 0x8

    invoke-virtual {v0, v2}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 2968
    const/16 v0, 0x8

    invoke-virtual {v6, v0}, Landroid/view/View;->setVisibility(I)V

    .line 2969
    const/16 v0, 0x8

    invoke-virtual {v1, v0}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 2970
    const/16 v0, 0x8

    invoke-virtual {v7, v0}, Landroid/view/View;->setVisibility(I)V

    .line 2972
    if-eqz v4, :cond_3

    if-eqz v8, :cond_3

    iget-object v0, p0, Lcom/osp/app/signin/SignUpView;->bm:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-gtz v0, :cond_2

    invoke-static {}, Lcom/osp/app/util/r;->q()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-direct {p0}, Lcom/osp/app/signin/SignUpView;->z()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 2986
    :cond_2
    const/4 v0, 0x0

    invoke-virtual {v8, v0}, Landroid/view/View;->setVisibility(I)V

    .line 2997
    :cond_3
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/osp/app/signin/SignUpView;->bl:Z

    .line 3000
    invoke-direct {p0}, Lcom/osp/app/signin/SignUpView;->e()V

    goto/16 :goto_0

    .line 2953
    :cond_4
    const/4 v9, 0x0

    invoke-virtual {v5, v9}, Landroid/view/View;->setVisibility(I)V

    goto :goto_2

    .line 3003
    :pswitch_1
    const-string v5, "CN"

    invoke-static {}, Lcom/osp/device/b;->b()Lcom/osp/device/b;

    move-result-object v9

    invoke-virtual {v9}, Lcom/osp/device/b;->n()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v5, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-nez v5, :cond_5

    iget-boolean v5, p0, Lcom/osp/app/signin/SignUpView;->bw:Z

    if-nez v5, :cond_5

    .line 3009
    const v5, 0x7f0c0134

    invoke-virtual {p0, v5}, Lcom/osp/app/signin/SignUpView;->findViewById(I)Landroid/view/View;

    move-result-object v5

    check-cast v5, Landroid/widget/LinearLayout;

    .line 3011
    if-eqz v5, :cond_5

    .line 3013
    const/16 v9, 0x8

    invoke-virtual {v5, v9}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 3015
    const v5, 0x7f0c0138

    invoke-virtual {p0, v5}, Lcom/osp/app/signin/SignUpView;->findViewById(I)Landroid/view/View;

    move-result-object v5

    .line 3016
    if-eqz v5, :cond_5

    .line 3018
    const/16 v9, 0x8

    invoke-virtual {v5, v9}, Landroid/view/View;->setVisibility(I)V

    .line 3022
    :cond_5
    iget-object v5, p0, Lcom/osp/app/signin/SignUpView;->bD:Lcom/osp/app/signin/kd;

    iput-object v5, p0, Lcom/osp/app/signin/SignUpView;->bB:Lcom/osp/app/signin/kd;

    .line 3023
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const v9, 0x7f0900f8

    invoke-virtual {p0, v9}, Lcom/osp/app/signin/SignUpView;->getString(I)Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v5, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v9, " *"

    invoke-virtual {v5, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v2, v5}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 3025
    const/4 v2, 0x0

    .line 3026
    invoke-static {}, Lcom/osp/common/util/i;->a()Lcom/osp/common/util/i;

    invoke-static {p0}, Lcom/osp/common/util/i;->e(Landroid/content/Context;)Z

    move-result v5

    if-nez v5, :cond_6

    iget-boolean v5, p0, Lcom/osp/app/signin/SignUpView;->U:Z

    if-eqz v5, :cond_9

    .line 3028
    :cond_6
    invoke-static {}, Lcom/osp/common/util/i;->a()Lcom/osp/common/util/i;

    const-string v2, "phone"

    invoke-virtual {p0, v2}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/telephony/TelephonyManager;

    invoke-virtual {v2}, Landroid/telephony/TelephonyManager;->getLine1Number()Ljava/lang/String;

    move-result-object v5

    .line 3029
    iget-boolean v2, p0, Lcom/osp/app/signin/SignUpView;->U:Z

    if-eqz v2, :cond_7

    iget-object v2, p0, Lcom/osp/app/signin/SignUpView;->aG:Ljava/lang/String;

    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_7

    .line 3031
    iget-object v5, p0, Lcom/osp/app/signin/SignUpView;->aG:Ljava/lang/String;

    .line 3035
    :cond_7
    :try_start_0
    invoke-static {}, Lcom/osp/common/util/i;->a()Lcom/osp/common/util/i;

    iget-object v2, p0, Lcom/osp/app/signin/SignUpView;->aj:Ljava/lang/String;

    invoke-static {v2}, Lcom/osp/common/util/i;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 3036
    if-eqz v2, :cond_8

    .line 3038
    sget-object v9, Ljava/util/Locale;->ENGLISH:Ljava/util/Locale;

    invoke-virtual {v2, v9}, Ljava/lang/String;->toUpperCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v2

    .line 3040
    :cond_8
    invoke-static {}, Lcom/google/i18n/phonenumbers/h;->a()Lcom/google/i18n/phonenumbers/h;

    move-result-object v9

    invoke-virtual {v9, v5, v2}, Lcom/google/i18n/phonenumbers/h;->a(Ljava/lang/CharSequence;Ljava/lang/String;)Ljava/lang/Iterable;

    move-result-object v2

    invoke-interface {v2}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/google/i18n/phonenumbers/d;

    .line 3041
    invoke-virtual {v2}, Lcom/google/i18n/phonenumbers/d;->a()Lcom/google/i18n/phonenumbers/ac;

    move-result-object v2

    .line 3042
    new-instance v9, Ljava/lang/StringBuilder;

    const-string v10, "+"

    invoke-direct {v9, v10}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2}, Lcom/google/i18n/phonenumbers/ac;->a()I

    move-result v10

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v2}, Lcom/google/i18n/phonenumbers/ac;->b()J

    move-result-wide v10

    invoke-virtual {v9, v10, v11}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v2

    .line 3046
    :goto_3
    iget-boolean v5, p0, Lcom/osp/app/signin/SignUpView;->U:Z

    if-eqz v5, :cond_9

    iget-object v5, p0, Lcom/osp/app/signin/SignUpView;->aG:Ljava/lang/String;

    invoke-static {v5}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v5

    if-nez v5, :cond_9

    .line 3048
    iput-object v2, p0, Lcom/osp/app/signin/SignUpView;->aH:Ljava/lang/String;

    .line 3052
    :cond_9
    invoke-static {}, Lcom/osp/device/b;->b()Lcom/osp/device/b;

    move-result-object v5

    invoke-virtual {v5}, Lcom/osp/device/b;->a()Z

    move-result v5

    if-eqz v5, :cond_a

    iget-boolean v5, p0, Lcom/osp/app/signin/SignUpView;->U:Z

    if-nez v5, :cond_b

    .line 3068
    :cond_a
    const/4 v5, 0x1

    invoke-virtual {v3, v5}, Landroid/widget/EditText;->setEnabled(Z)V

    .line 3071
    :cond_b
    invoke-virtual {v3, v2}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    .line 3072
    invoke-virtual {v3}, Landroid/widget/EditText;->length()I

    move-result v2

    invoke-virtual {v3, v2}, Landroid/widget/EditText;->setSelection(I)V

    .line 3074
    const v2, 0x7f09016b

    invoke-virtual {v3, v2}, Landroid/widget/EditText;->setHint(I)V

    .line 3075
    const/4 v2, 0x3

    invoke-virtual {v3, v2}, Landroid/widget/EditText;->setInputType(I)V

    .line 3076
    const/4 v2, 0x0

    invoke-virtual {v0, v2}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 3077
    const/4 v0, 0x0

    invoke-virtual {v1, v0}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 3081
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x15

    if-lt v0, v1, :cond_f

    .line 3083
    const/16 v0, 0x8

    invoke-virtual {v6, v0}, Landroid/view/View;->setVisibility(I)V

    .line 3084
    const/16 v0, 0x8

    invoke-virtual {v7, v0}, Landroid/view/View;->setVisibility(I)V

    .line 3090
    :goto_4
    if-eqz v4, :cond_d

    if-eqz v8, :cond_d

    .line 3094
    invoke-virtual {p0}, Lcom/osp/app/signin/SignUpView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0800e7

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v0

    float-to-int v0, v0

    .line 3095
    invoke-static {}, Lcom/osp/device/b;->b()Lcom/osp/device/b;

    move-result-object v1

    invoke-virtual {v1, p0}, Lcom/osp/device/b;->i(Landroid/content/Context;)Z

    move-result v1

    if-eqz v1, :cond_c

    .line 3097
    const/4 v0, 0x0

    .line 3105
    :cond_c
    const/4 v1, -0x1

    const/4 v2, -0x1

    const/4 v5, -0x1

    invoke-static {v4, v1, v2, v0, v5}, Lcom/msc/sa/c/d;->a(Landroid/view/View;IIII)V

    .line 3107
    const/16 v0, 0x8

    invoke-virtual {v8, v0}, Landroid/view/View;->setVisibility(I)V

    .line 3110
    :cond_d
    invoke-static {p0}, Lcom/osp/app/util/r;->p(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_e

    iget-object v0, p0, Lcom/osp/app/signin/SignUpView;->al:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_e

    .line 3112
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "+"

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v1, p0, Lcom/osp/app/signin/SignUpView;->al:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 3113
    iput-object v0, p0, Lcom/osp/app/signin/SignUpView;->am:Ljava/lang/String;

    .line 3115
    invoke-virtual {v3, v0}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    .line 3117
    const/4 v0, 0x0

    invoke-virtual {v3, v0}, Landroid/widget/EditText;->setEnabled(Z)V

    .line 3118
    const/4 v0, 0x0

    invoke-virtual {v3, v0}, Landroid/widget/EditText;->setFocusable(Z)V

    .line 3119
    const/4 v0, 0x0

    invoke-virtual {v3, v0}, Landroid/widget/EditText;->setFocusableInTouchMode(Z)V

    .line 3122
    :cond_e
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/osp/app/signin/SignUpView;->bl:Z

    .line 3125
    invoke-direct {p0}, Lcom/osp/app/signin/SignUpView;->f()V

    goto/16 :goto_0

    .line 3043
    :catch_0
    move-exception v2

    move-object v2, v5

    goto/16 :goto_3

    .line 3087
    :cond_f
    const/4 v0, 0x0

    invoke-virtual {v6, v0}, Landroid/view/View;->setVisibility(I)V

    .line 3088
    const/4 v0, 0x0

    invoke-virtual {v7, v0}, Landroid/view/View;->setVisibility(I)V

    goto :goto_4

    .line 3155
    :cond_10
    const v0, 0x7f0c017c

    invoke-virtual {p0, v0}, Lcom/osp/app/signin/SignUpView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    const/4 v1, 0x0

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/view/View;->scrollTo(II)V

    goto/16 :goto_1

    .line 2934
    nop

    :pswitch_data_0
    .packed-switch 0x12c
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method private a(Landroid/app/AlertDialog;)V
    .locals 1

    .prologue
    .line 1368
    if-eqz p1, :cond_0

    .line 1372
    :try_start_0
    invoke-virtual {p0}, Lcom/osp/app/signin/SignUpView;->isFinishing()Z

    move-result v0

    if-nez v0, :cond_0

    invoke-virtual {p1}, Landroid/app/AlertDialog;->isShowing()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1374
    invoke-virtual {p1}, Landroid/app/AlertDialog;->dismiss()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1384
    :cond_0
    :goto_0
    return-void

    .line 1376
    :catch_0
    move-exception v0

    :try_start_1
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 1381
    :catchall_0
    move-exception v0

    throw v0
.end method

.method private a(Landroid/content/Intent;)V
    .locals 5

    .prologue
    const/4 v3, 0x1

    const/4 v4, 0x0

    .line 6954
    iput-boolean v3, p0, Lcom/osp/app/signin/SignUpView;->U:Z

    .line 6956
    const-string v0, "key_name_check_familyname"

    invoke-virtual {p1, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/osp/app/signin/SignUpView;->aD:Ljava/lang/String;

    .line 6957
    const-string v0, "key_name_check_givenname"

    invoke-virtual {p1, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/osp/app/signin/SignUpView;->aE:Ljava/lang/String;

    .line 6958
    const-string v0, "key_name_check_birthdate"

    invoke-virtual {p1, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/osp/app/signin/SignUpView;->aF:Ljava/lang/String;

    .line 6959
    const-string v0, "key_name_check_mobile"

    invoke-virtual {p1, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/osp/app/signin/SignUpView;->aG:Ljava/lang/String;

    .line 6962
    const-string v0, "key_name_check_method"

    invoke-virtual {p1, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/osp/app/signin/SignUpView;->aI:Ljava/lang/String;

    .line 6963
    const-string v0, "key_name_check_ci"

    invoke-virtual {p1, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/osp/app/signin/SignUpView;->aJ:Ljava/lang/String;

    .line 6964
    const-string v0, "key_name_check_di"

    invoke-virtual {p1, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/osp/app/signin/SignUpView;->aK:Ljava/lang/String;

    .line 6965
    const-string v0, "key_name_check_datetime"

    invoke-virtual {p1, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/osp/app/signin/SignUpView;->aL:Ljava/lang/String;

    .line 6966
    const-string v0, "key_name_check_gender"

    invoke-virtual {p1, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/osp/app/signin/SignUpView;->aM:Ljava/lang/String;

    .line 6967
    const-string v0, "key_name_check_foreigner"

    invoke-virtual {p1, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/osp/app/signin/SignUpView;->aN:Ljava/lang/String;

    .line 6969
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    iput-object v0, p0, Lcom/osp/app/signin/SignUpView;->bb:Landroid/os/Bundle;

    .line 6970
    iget-object v0, p0, Lcom/osp/app/signin/SignUpView;->bb:Landroid/os/Bundle;

    const-string v1, "key_name_check_familyname"

    iget-object v2, p0, Lcom/osp/app/signin/SignUpView;->aD:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 6971
    iget-object v0, p0, Lcom/osp/app/signin/SignUpView;->bb:Landroid/os/Bundle;

    const-string v1, "key_name_check_givenname"

    iget-object v2, p0, Lcom/osp/app/signin/SignUpView;->aE:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 6972
    iget-object v0, p0, Lcom/osp/app/signin/SignUpView;->bb:Landroid/os/Bundle;

    const-string v1, "key_name_check_birthdate"

    iget-object v2, p0, Lcom/osp/app/signin/SignUpView;->aF:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 6973
    iget-object v0, p0, Lcom/osp/app/signin/SignUpView;->bb:Landroid/os/Bundle;

    const-string v1, "key_name_check_mobile"

    iget-object v2, p0, Lcom/osp/app/signin/SignUpView;->aG:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 6974
    iget-object v0, p0, Lcom/osp/app/signin/SignUpView;->bb:Landroid/os/Bundle;

    const-string v1, "key_name_check_method"

    iget-object v2, p0, Lcom/osp/app/signin/SignUpView;->aI:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 6975
    iget-object v0, p0, Lcom/osp/app/signin/SignUpView;->bb:Landroid/os/Bundle;

    const-string v1, "key_name_check_ci"

    iget-object v2, p0, Lcom/osp/app/signin/SignUpView;->aJ:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 6976
    iget-object v0, p0, Lcom/osp/app/signin/SignUpView;->bb:Landroid/os/Bundle;

    const-string v1, "key_name_check_di"

    iget-object v2, p0, Lcom/osp/app/signin/SignUpView;->aK:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 6977
    iget-object v0, p0, Lcom/osp/app/signin/SignUpView;->bb:Landroid/os/Bundle;

    const-string v1, "key_name_check_datetime"

    iget-object v2, p0, Lcom/osp/app/signin/SignUpView;->aL:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 6978
    iget-object v0, p0, Lcom/osp/app/signin/SignUpView;->bb:Landroid/os/Bundle;

    const-string v1, "key_name_check_gender"

    iget-object v2, p0, Lcom/osp/app/signin/SignUpView;->aM:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 6979
    iget-object v0, p0, Lcom/osp/app/signin/SignUpView;->bb:Landroid/os/Bundle;

    const-string v1, "key_name_check_foreigner"

    iget-object v2, p0, Lcom/osp/app/signin/SignUpView;->aN:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 6981
    iget-object v0, p0, Lcom/osp/app/signin/SignUpView;->I:Landroid/widget/EditText;

    if-nez v0, :cond_0

    .line 6983
    invoke-static {}, Lcom/osp/device/b;->b()Lcom/osp/device/b;

    move-result-object v0

    invoke-virtual {v0}, Lcom/osp/device/b;->a()Z

    move-result v0

    if-eqz v0, :cond_a

    .line 6985
    iget-object v0, p0, Lcom/osp/app/signin/SignUpView;->bC:Lcom/osp/app/signin/kd;

    iput-boolean v3, v0, Lcom/osp/app/signin/kd;->e:Z

    .line 6986
    iget-object v0, p0, Lcom/osp/app/signin/SignUpView;->bC:Lcom/osp/app/signin/kd;

    const-string v1, "familyName"

    invoke-virtual {v0, v1}, Lcom/osp/app/signin/kd;->a(Ljava/lang/String;)V

    .line 6987
    iget-object v0, p0, Lcom/osp/app/signin/SignUpView;->bD:Lcom/osp/app/signin/kd;

    iput-boolean v3, v0, Lcom/osp/app/signin/kd;->e:Z

    .line 6988
    iget-object v0, p0, Lcom/osp/app/signin/SignUpView;->bD:Lcom/osp/app/signin/kd;

    const-string v1, "familyName"

    invoke-virtual {v0, v1}, Lcom/osp/app/signin/kd;->a(Ljava/lang/String;)V

    .line 6995
    :cond_0
    :goto_0
    iget-object v0, p0, Lcom/osp/app/signin/SignUpView;->J:Landroid/widget/EditText;

    if-nez v0, :cond_1

    .line 6997
    invoke-static {}, Lcom/osp/device/b;->b()Lcom/osp/device/b;

    move-result-object v0

    invoke-virtual {v0}, Lcom/osp/device/b;->a()Z

    move-result v0

    if-eqz v0, :cond_b

    .line 6999
    iget-object v0, p0, Lcom/osp/app/signin/SignUpView;->bC:Lcom/osp/app/signin/kd;

    iput-boolean v3, v0, Lcom/osp/app/signin/kd;->d:Z

    .line 7000
    iget-object v0, p0, Lcom/osp/app/signin/SignUpView;->bC:Lcom/osp/app/signin/kd;

    const-string v1, "givenName"

    invoke-virtual {v0, v1}, Lcom/osp/app/signin/kd;->a(Ljava/lang/String;)V

    .line 7001
    iget-object v0, p0, Lcom/osp/app/signin/SignUpView;->bD:Lcom/osp/app/signin/kd;

    iput-boolean v3, v0, Lcom/osp/app/signin/kd;->d:Z

    .line 7002
    iget-object v0, p0, Lcom/osp/app/signin/SignUpView;->bD:Lcom/osp/app/signin/kd;

    const-string v1, "givenName"

    invoke-virtual {v0, v1}, Lcom/osp/app/signin/kd;->a(Ljava/lang/String;)V

    .line 7010
    :cond_1
    :goto_1
    invoke-direct {p0}, Lcom/osp/app/signin/SignUpView;->d()V

    .line 7012
    const-string v0, "Y"

    iget-object v1, p0, Lcom/osp/app/signin/SignUpView;->aN:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_3

    .line 7014
    iget-object v0, p0, Lcom/osp/app/signin/SignUpView;->I:Landroid/widget/EditText;

    if-eqz v0, :cond_2

    .line 7016
    iget-object v0, p0, Lcom/osp/app/signin/SignUpView;->I:Landroid/widget/EditText;

    iget-object v1, p0, Lcom/osp/app/signin/SignUpView;->aD:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    .line 7018
    :cond_2
    iget-object v0, p0, Lcom/osp/app/signin/SignUpView;->J:Landroid/widget/EditText;

    if-eqz v0, :cond_3

    .line 7020
    iget-object v0, p0, Lcom/osp/app/signin/SignUpView;->J:Landroid/widget/EditText;

    iget-object v1, p0, Lcom/osp/app/signin/SignUpView;->aE:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    .line 7036
    :cond_3
    new-instance v0, Ljava/text/SimpleDateFormat;

    const-string v1, "yyyyMMddHHmmss"

    invoke-direct {v0, v1}, Ljava/text/SimpleDateFormat;-><init>(Ljava/lang/String;)V

    .line 7040
    :try_start_0
    iget-object v1, p0, Lcom/osp/app/signin/SignUpView;->aF:Ljava/lang/String;

    if-eqz v1, :cond_4

    .line 7042
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v2, p0, Lcom/osp/app/signin/SignUpView;->aF:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "235959"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/text/SimpleDateFormat;->parse(Ljava/lang/String;)Ljava/util/Date;

    move-result-object v0

    .line 7044
    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object v1

    .line 7045
    invoke-virtual {v1, v0}, Ljava/util/Calendar;->setTime(Ljava/util/Date;)V

    .line 7046
    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Ljava/util/Calendar;->get(I)I

    move-result v2

    iput v2, p0, Lcom/osp/app/signin/SignUpView;->az:I

    .line 7047
    const/4 v2, 0x2

    invoke-virtual {v1, v2}, Ljava/util/Calendar;->get(I)I

    move-result v2

    iput v2, p0, Lcom/osp/app/signin/SignUpView;->ay:I

    .line 7048
    const/4 v2, 0x5

    invoke-virtual {v1, v2}, Ljava/util/Calendar;->get(I)I

    move-result v1

    iput v1, p0, Lcom/osp/app/signin/SignUpView;->aA:I

    .line 7050
    iget-object v1, p0, Lcom/osp/app/signin/SignUpView;->A:Landroid/widget/TextView;

    if-eqz v1, :cond_4

    .line 7052
    iget-object v1, p0, Lcom/osp/app/signin/SignUpView;->A:Landroid/widget/TextView;

    invoke-direct {p0, v0}, Lcom/osp/app/signin/SignUpView;->a(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 7053
    iget-object v1, p0, Lcom/osp/app/signin/SignUpView;->A:Landroid/widget/TextView;

    invoke-direct {p0, v0}, Lcom/osp/app/signin/SignUpView;->a(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 7055
    iget-object v0, p0, Lcom/osp/app/signin/SignUpView;->y:Landroid/widget/LinearLayout;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setEnabled(Z)V

    .line 7056
    iget-object v0, p0, Lcom/osp/app/signin/SignUpView;->A:Landroid/widget/TextView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setEnabled(Z)V

    .line 7057
    iget-object v0, p0, Lcom/osp/app/signin/SignUpView;->z:Landroid/widget/ImageView;

    const/4 v1, 0x0

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/msc/sa/c/d;->a(Landroid/widget/ImageView;Ljava/lang/Boolean;)V
    :try_end_0
    .catch Ljava/text/ParseException; {:try_start_0 .. :try_end_0} :catch_0

    .line 7073
    :cond_4
    :goto_2
    iget-object v0, p0, Lcom/osp/app/signin/SignUpView;->I:Landroid/widget/EditText;

    if-eqz v0, :cond_5

    iget-object v0, p0, Lcom/osp/app/signin/SignUpView;->aD:Ljava/lang/String;

    if-eqz v0, :cond_5

    iget-object v0, p0, Lcom/osp/app/signin/SignUpView;->aD:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v0

    if-eqz v0, :cond_5

    .line 7075
    iget-object v0, p0, Lcom/osp/app/signin/SignUpView;->I:Landroid/widget/EditText;

    invoke-virtual {v0, v4}, Landroid/widget/EditText;->setEnabled(Z)V

    .line 7076
    iget-object v0, p0, Lcom/osp/app/signin/SignUpView;->I:Landroid/widget/EditText;

    invoke-virtual {v0, v4}, Landroid/widget/EditText;->setFocusable(Z)V

    .line 7078
    :cond_5
    iget-object v0, p0, Lcom/osp/app/signin/SignUpView;->J:Landroid/widget/EditText;

    if-eqz v0, :cond_6

    iget-object v0, p0, Lcom/osp/app/signin/SignUpView;->aE:Ljava/lang/String;

    if-eqz v0, :cond_6

    iget-object v0, p0, Lcom/osp/app/signin/SignUpView;->aE:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v0

    if-eqz v0, :cond_6

    .line 7080
    iget-object v0, p0, Lcom/osp/app/signin/SignUpView;->J:Landroid/widget/EditText;

    invoke-virtual {v0, v4}, Landroid/widget/EditText;->setEnabled(Z)V

    .line 7081
    iget-object v0, p0, Lcom/osp/app/signin/SignUpView;->J:Landroid/widget/EditText;

    invoke-virtual {v0, v4}, Landroid/widget/EditText;->setFocusable(Z)V

    .line 7083
    :cond_6
    iget-object v0, p0, Lcom/osp/app/signin/SignUpView;->y:Landroid/widget/LinearLayout;

    if-eqz v0, :cond_7

    iget-object v0, p0, Lcom/osp/app/signin/SignUpView;->aF:Ljava/lang/String;

    if-eqz v0, :cond_7

    iget-object v0, p0, Lcom/osp/app/signin/SignUpView;->aF:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v0

    if-eqz v0, :cond_7

    .line 7085
    iget-object v0, p0, Lcom/osp/app/signin/SignUpView;->y:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v4}, Landroid/widget/LinearLayout;->setEnabled(Z)V

    .line 7086
    iget-object v0, p0, Lcom/osp/app/signin/SignUpView;->A:Landroid/widget/TextView;

    invoke-virtual {v0, v4}, Landroid/widget/TextView;->setEnabled(Z)V

    .line 7087
    iget-object v0, p0, Lcom/osp/app/signin/SignUpView;->z:Landroid/widget/ImageView;

    invoke-static {v4}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/msc/sa/c/d;->a(Landroid/widget/ImageView;Ljava/lang/Boolean;)V

    .line 7091
    :cond_7
    iget-object v0, p0, Lcom/osp/app/signin/SignUpView;->bB:Lcom/osp/app/signin/kd;

    iget-boolean v0, v0, Lcom/osp/app/signin/kd;->h:Z

    if-eqz v0, :cond_9

    iget-object v0, p0, Lcom/osp/app/signin/SignUpView;->aG:Ljava/lang/String;

    if-eqz v0, :cond_9

    iget-object v0, p0, Lcom/osp/app/signin/SignUpView;->aG:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v0

    if-eqz v0, :cond_9

    .line 7093
    iget-object v0, p0, Lcom/osp/app/signin/SignUpView;->O:Landroid/widget/EditText;

    if-eqz v0, :cond_9

    .line 7095
    const-string v0, "450"

    iget-object v1, p0, Lcom/osp/app/signin/SignUpView;->aj:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_c

    .line 7100
    :try_start_1
    invoke-static {}, Lcom/osp/common/util/i;->a()Lcom/osp/common/util/i;

    iget-object v0, p0, Lcom/osp/app/signin/SignUpView;->aj:Ljava/lang/String;

    invoke-static {v0}, Lcom/osp/common/util/i;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 7101
    if-eqz v0, :cond_8

    .line 7103
    sget-object v1, Ljava/util/Locale;->ENGLISH:Ljava/util/Locale;

    invoke-virtual {v0, v1}, Ljava/lang/String;->toUpperCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v0

    .line 7105
    :cond_8
    invoke-static {}, Lcom/google/i18n/phonenumbers/h;->a()Lcom/google/i18n/phonenumbers/h;

    move-result-object v1

    iget-object v2, p0, Lcom/osp/app/signin/SignUpView;->aG:Ljava/lang/String;

    invoke-virtual {v1, v2, v0}, Lcom/google/i18n/phonenumbers/h;->a(Ljava/lang/CharSequence;Ljava/lang/String;)Ljava/lang/Iterable;

    move-result-object v0

    invoke-interface {v0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/i18n/phonenumbers/d;

    .line 7106
    invoke-virtual {v0}, Lcom/google/i18n/phonenumbers/d;->a()Lcom/google/i18n/phonenumbers/ac;

    move-result-object v0

    .line 7108
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0}, Lcom/google/i18n/phonenumbers/ac;->a()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v0}, Lcom/google/i18n/phonenumbers/ac;->b()J

    move-result-wide v2

    invoke-virtual {v1, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 7109
    iget-object v1, p0, Lcom/osp/app/signin/SignUpView;->O:Landroid/widget/EditText;

    invoke-virtual {v1, v0}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1

    .line 7117
    :goto_3
    iget-object v0, p0, Lcom/osp/app/signin/SignUpView;->O:Landroid/widget/EditText;

    const/4 v1, 0x3

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setInputType(I)V

    .line 7118
    iget-object v0, p0, Lcom/osp/app/signin/SignUpView;->O:Landroid/widget/EditText;

    invoke-virtual {v0, v4}, Landroid/widget/EditText;->setEnabled(Z)V

    .line 7119
    iget-object v0, p0, Lcom/osp/app/signin/SignUpView;->O:Landroid/widget/EditText;

    invoke-virtual {v0, v4}, Landroid/widget/EditText;->setFocusable(Z)V

    .line 7127
    :cond_9
    :goto_4
    return-void

    .line 6991
    :cond_a
    iget-object v0, p0, Lcom/osp/app/signin/SignUpView;->bB:Lcom/osp/app/signin/kd;

    iput-boolean v3, v0, Lcom/osp/app/signin/kd;->e:Z

    .line 6992
    iget-object v0, p0, Lcom/osp/app/signin/SignUpView;->bB:Lcom/osp/app/signin/kd;

    const-string v1, "familyName"

    invoke-virtual {v0, v1}, Lcom/osp/app/signin/kd;->a(Ljava/lang/String;)V

    goto/16 :goto_0

    .line 7005
    :cond_b
    iget-object v0, p0, Lcom/osp/app/signin/SignUpView;->bB:Lcom/osp/app/signin/kd;

    iput-boolean v3, v0, Lcom/osp/app/signin/kd;->d:Z

    .line 7006
    iget-object v0, p0, Lcom/osp/app/signin/SignUpView;->bB:Lcom/osp/app/signin/kd;

    const-string v1, "givenName"

    invoke-virtual {v0, v1}, Lcom/osp/app/signin/kd;->a(Ljava/lang/String;)V

    goto/16 :goto_1

    .line 7060
    :catch_0
    move-exception v0

    invoke-virtual {v0}, Ljava/text/ParseException;->printStackTrace()V

    goto/16 :goto_2

    .line 7114
    :catch_1
    move-exception v0

    iget-object v0, p0, Lcom/osp/app/signin/SignUpView;->O:Landroid/widget/EditText;

    iget-object v1, p0, Lcom/osp/app/signin/SignUpView;->aG:Ljava/lang/String;

    invoke-static {v1}, Landroid/telephony/PhoneNumberUtils;->formatNumber(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    goto :goto_3

    .line 7122
    :cond_c
    iget-object v0, p0, Lcom/osp/app/signin/SignUpView;->O:Landroid/widget/EditText;

    iget-object v1, p0, Lcom/osp/app/signin/SignUpView;->aG:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    goto :goto_4
.end method

.method static synthetic a(Lcom/osp/app/signin/SignUpView;)V
    .locals 9

    .prologue
    const/4 v8, 0x3

    const/16 v7, 0xa

    const/4 v2, 0x1

    const/4 v3, 0x0

    const/4 v6, 0x2

    .line 112
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v0, "SignUpView::updateDisplay"

    invoke-static {v0}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;)V

    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object v4

    iget v0, p0, Lcom/osp/app/signin/SignUpView;->az:I

    invoke-static {v0}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v4, v2}, Ljava/util/Calendar;->get(I)I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v1

    iget v5, p0, Lcom/osp/app/signin/SignUpView;->ay:I

    if-ge v5, v7, :cond_1

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v5, "0"

    invoke-virtual {v0, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v5, p0, Lcom/osp/app/signin/SignUpView;->ay:I

    invoke-static {v5}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v0, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    :goto_0
    iget v5, p0, Lcom/osp/app/signin/SignUpView;->aA:I

    if-ge v5, v7, :cond_2

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v5, "0"

    invoke-virtual {v0, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v5, p0, Lcom/osp/app/signin/SignUpView;->aA:I

    invoke-static {v5}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v0, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    :goto_1
    invoke-virtual {v4, v6}, Ljava/util/Calendar;->get(I)I

    move-result v5

    if-ge v5, v7, :cond_3

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v5, "0"

    invoke-virtual {v1, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v4, v6}, Ljava/util/Calendar;->get(I)I

    move-result v5

    invoke-static {v5}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v1, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    :goto_2
    const/4 v5, 0x5

    invoke-virtual {v4, v5}, Ljava/util/Calendar;->get(I)I

    move-result v5

    if-ge v5, v7, :cond_4

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v5, "0"

    invoke-virtual {v1, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const/4 v5, 0x5

    invoke-virtual {v4, v5}, Ljava/util/Calendar;->get(I)I

    move-result v4

    invoke-static {v4}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    :goto_3
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    if-lt v0, v1, :cond_5

    move v0, v2

    :goto_4
    if-nez v0, :cond_0

    iget v0, p0, Lcom/osp/app/signin/SignUpView;->az:I

    const/16 v1, 0x76d

    if-ge v0, v1, :cond_6

    :cond_0
    const v0, 0x7f090031

    invoke-virtual {p0, v0}, Lcom/osp/app/signin/SignUpView;->getText(I)Ljava/lang/CharSequence;

    move-result-object v0

    invoke-interface {v0}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {}, Lcom/osp/app/util/ap;->a()Lcom/osp/app/util/ap;

    invoke-static {p0, v0, v3}, Lcom/osp/app/util/ap;->a(Landroid/content/Context;Ljava/lang/String;I)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    iget v0, p0, Lcom/osp/app/signin/SignUpView;->f:I

    iput v0, p0, Lcom/osp/app/signin/SignUpView;->az:I

    iget v0, p0, Lcom/osp/app/signin/SignUpView;->e:I

    iput v0, p0, Lcom/osp/app/signin/SignUpView;->ay:I

    iget v0, p0, Lcom/osp/app/signin/SignUpView;->d:I

    iput v0, p0, Lcom/osp/app/signin/SignUpView;->aA:I

    iget-object v0, p0, Lcom/osp/app/signin/SignUpView;->at:Landroid/app/Dialog;

    check-cast v0, Landroid/app/DatePickerDialog;

    iget v1, p0, Lcom/osp/app/signin/SignUpView;->f:I

    iget v2, p0, Lcom/osp/app/signin/SignUpView;->e:I

    iget v3, p0, Lcom/osp/app/signin/SignUpView;->d:I

    invoke-virtual {v0, v1, v2, v3}, Landroid/app/DatePickerDialog;->updateDate(III)V

    :goto_5
    return-void

    :cond_1
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v5, p0, Lcom/osp/app/signin/SignUpView;->ay:I

    invoke-static {v5}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v0, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_0

    :cond_2
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v5, p0, Lcom/osp/app/signin/SignUpView;->aA:I

    invoke-static {v5}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v0, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_1

    :cond_3
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v4, v6}, Ljava/util/Calendar;->get(I)I

    move-result v5

    invoke-static {v5}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v1, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    goto/16 :goto_2

    :cond_4
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const/4 v5, 0x5

    invoke-virtual {v4, v5}, Ljava/util/Calendar;->get(I)I

    move-result v4

    invoke-static {v4}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    goto/16 :goto_3

    :cond_5
    move v0, v3

    goto/16 :goto_4

    :cond_6
    invoke-virtual {p0}, Lcom/osp/app/signin/SignUpView;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const-string v1, "date_format"

    invoke-static {v0, v1}, Landroid/provider/Settings$System;->getString(Landroid/content/ContentResolver;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    const-string v1, "MM-dd-yyyy"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_7

    const-string v0, "%02d/%02d/%04d"

    new-array v1, v8, [Ljava/lang/Object;

    iget v4, p0, Lcom/osp/app/signin/SignUpView;->ay:I

    add-int/lit8 v4, v4, 0x1

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v1, v3

    iget v3, p0, Lcom/osp/app/signin/SignUpView;->aA:I

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v1, v2

    iget v2, p0, Lcom/osp/app/signin/SignUpView;->az:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v1, v6

    invoke-static {v0, v1}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    :goto_6
    iget-object v1, p0, Lcom/osp/app/signin/SignUpView;->A:Landroid/widget/TextView;

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v1, p0, Lcom/osp/app/signin/SignUpView;->A:Landroid/widget/TextView;

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setContentDescription(Ljava/lang/CharSequence;)V

    goto/16 :goto_5

    :cond_7
    const-string v1, "dd-MM-yyyy"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_8

    const-string v0, "%02d/%02d/%04d"

    new-array v1, v8, [Ljava/lang/Object;

    iget v4, p0, Lcom/osp/app/signin/SignUpView;->aA:I

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v1, v3

    iget v3, p0, Lcom/osp/app/signin/SignUpView;->ay:I

    add-int/lit8 v3, v3, 0x1

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v1, v2

    iget v2, p0, Lcom/osp/app/signin/SignUpView;->az:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v1, v6

    invoke-static {v0, v1}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto :goto_6

    :cond_8
    const-string v0, "%04d/%02d/%02d"

    new-array v1, v8, [Ljava/lang/Object;

    iget v4, p0, Lcom/osp/app/signin/SignUpView;->az:I

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v1, v3

    iget v3, p0, Lcom/osp/app/signin/SignUpView;->ay:I

    add-int/lit8 v3, v3, 0x1

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v1, v2

    iget v2, p0, Lcom/osp/app/signin/SignUpView;->aA:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v1, v6

    invoke-static {v0, v1}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto :goto_6
.end method

.method static synthetic a(Lcom/osp/app/signin/SignUpView;Landroid/content/Intent;)V
    .locals 0

    .prologue
    .line 112
    invoke-direct {p0, p1}, Lcom/osp/app/signin/SignUpView;->a(Landroid/content/Intent;)V

    return-void
.end method

.method private a(Landroid/database/Cursor;)[Ljava/lang/String;
    .locals 11

    .prologue
    const/4 v6, 0x0

    .line 9304
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0xe

    if-lt v0, v1, :cond_5

    .line 9307
    invoke-virtual {p0}, Lcom/osp/app/signin/SignUpView;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    .line 9314
    if-eqz p1, :cond_6

    :try_start_0
    invoke-interface {p1}, Landroid/database/Cursor;->getCount()I

    move-result v1

    if-lez v1, :cond_6

    invoke-interface {p1}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v1

    if-eqz v1, :cond_6

    .line 9316
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v1, "SUV"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "localRawProfile count = "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-interface {p1}, Landroid/database/Cursor;->getCount()I

    move-result v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-object v7, v6

    .line 9319
    :goto_0
    const/4 v1, 0x0

    :try_start_1
    invoke-interface {p1, v1}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v8

    .line 9321
    const/4 v1, 0x1

    invoke-static {v1}, Lcom/msc/sa/c/d;->a(I)Landroid/net/Uri;

    move-result-object v1

    .line 9322
    if-eqz v1, :cond_b

    .line 9325
    invoke-virtual {v1}, Landroid/net/Uri;->buildUpon()Landroid/net/Uri$Builder;

    move-result-object v1

    const-string v2, "data"

    invoke-virtual {v1, v2}, Landroid/net/Uri$Builder;->appendPath(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v1

    invoke-virtual {v1}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v1

    const/4 v2, 0x2

    new-array v2, v2, [Ljava/lang/String;

    const/4 v3, 0x0

    const-string v4, "data2"

    aput-object v4, v2, v3

    const/4 v3, 0x1

    const-string v4, "data3"

    aput-object v4, v2, v3

    const-string v3, "raw_contact_id= ? AND mimetype = ?"

    const/4 v4, 0x2

    new-array v4, v4, [Ljava/lang/String;

    const/4 v5, 0x0

    invoke-static {v8, v9}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v8

    aput-object v8, v4, v5

    const/4 v5, 0x1

    const-string v8, "vnd.android.cursor.item/name"

    aput-object v8, v4, v5

    const/4 v5, 0x0

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_2
    .catchall {:try_start_1 .. :try_end_1} :catchall_2

    move-result-object v1

    .line 9330
    :goto_1
    if-eqz v1, :cond_2

    :try_start_2
    invoke-interface {v1}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v2

    if-eqz v2, :cond_2

    .line 9338
    const/4 v0, 0x0

    invoke-interface {v1, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v2

    .line 9339
    const/4 v0, 0x1

    invoke-interface {v1, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v3

    .line 9341
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v0, "SUV"

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "structuredName.getString(0)"

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v0, v4}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 9344
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v0, "SUV"

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "structuredName.getString(1)"

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v0, v4}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 9346
    const/4 v0, 0x2

    new-array v0, v0, [Ljava/lang/String;

    const/4 v4, 0x0

    aput-object v2, v0, v4

    const/4 v2, 0x1

    aput-object v3, v0, v2
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_1
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 9369
    if-eqz p1, :cond_0

    .line 9371
    invoke-interface {p1}, Landroid/database/Cursor;->close()V

    .line 9373
    :cond_0
    if-eqz v1, :cond_1

    .line 9375
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    .line 9384
    :cond_1
    :goto_2
    return-object v0

    .line 9349
    :cond_2
    if-eqz v1, :cond_3

    .line 9351
    :try_start_3
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    move-object v1, v6

    .line 9354
    :cond_3
    invoke-interface {p1}, Landroid/database/Cursor;->moveToNext()Z
    :try_end_3
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_1
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    move-result v2

    if-nez v2, :cond_c

    .line 9369
    :goto_3
    if-eqz p1, :cond_4

    .line 9371
    invoke-interface {p1}, Landroid/database/Cursor;->close()V

    .line 9373
    :cond_4
    if-eqz v1, :cond_5

    .line 9375
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    :cond_5
    :goto_4
    move-object v0, v6

    .line 9384
    goto :goto_2

    .line 9357
    :cond_6
    if-eqz p1, :cond_7

    .line 9359
    :try_start_4
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v0, "SUV"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "localRawProfile count = "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-interface {p1}, Landroid/database/Cursor;->getCount()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_4
    .catch Ljava/lang/Exception; {:try_start_4 .. :try_end_4} :catch_0
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    :cond_7
    move-object v1, v6

    goto :goto_3

    .line 9365
    :catch_0
    move-exception v0

    move-object v0, v6

    :goto_5
    :try_start_5
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v1, "SUV"

    const-string v2, "getLocalProfileGivenName has occurred Exception()"

    invoke-static {v1, v2}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_3

    .line 9369
    if-eqz p1, :cond_8

    .line 9371
    invoke-interface {p1}, Landroid/database/Cursor;->close()V

    .line 9373
    :cond_8
    if-eqz v0, :cond_5

    .line 9375
    invoke-interface {v0}, Landroid/database/Cursor;->close()V

    goto :goto_4

    .line 9369
    :catchall_0
    move-exception v0

    move-object v1, v6

    :goto_6
    if-eqz p1, :cond_9

    .line 9371
    invoke-interface {p1}, Landroid/database/Cursor;->close()V

    .line 9373
    :cond_9
    if-eqz v1, :cond_a

    .line 9375
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    .line 9369
    :cond_a
    throw v0

    :catchall_1
    move-exception v0

    goto :goto_6

    :catchall_2
    move-exception v0

    move-object v1, v7

    goto :goto_6

    :catchall_3
    move-exception v1

    move-object v10, v1

    move-object v1, v0

    move-object v0, v10

    goto :goto_6

    .line 9365
    :catch_1
    move-exception v0

    move-object v0, v1

    goto :goto_5

    :catch_2
    move-exception v0

    move-object v0, v7

    goto :goto_5

    :cond_b
    move-object v1, v7

    goto/16 :goto_1

    :cond_c
    move-object v7, v1

    goto/16 :goto_0
.end method

.method static synthetic aa(Lcom/osp/app/signin/SignUpView;)V
    .locals 7

    .prologue
    const/4 v6, 0x1

    .line 112
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v0, "AccountView::ShowSmsActivity isCancelableJustOneActivity : true"

    invoke-static {v0}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/osp/app/signin/SignUpView;->aq:Lcom/osp/app/signin/SignUpinfo;

    if-nez v0, :cond_3

    iget-object v0, p0, Lcom/osp/app/signin/SignUpView;->aS:Ljava/lang/String;

    :goto_0
    const/4 v1, 0x0

    iput v1, p0, Lcom/osp/app/signin/SignUpView;->aW:I

    new-instance v2, Landroid/content/Intent;

    invoke-direct {v2}, Landroid/content/Intent;-><init>()V

    const-class v1, Lcom/osp/app/signin/SmsVerificationActivity;

    invoke-virtual {v2, p0, v1}, Landroid/content/Intent;->setClass(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;

    const-string v1, "key_is_name_verified"

    iget-boolean v3, p0, Lcom/osp/app/signin/SignUpView;->U:Z

    invoke-virtual {v2, v1, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    const-string v1, "key_name_check_familyname"

    iget-object v3, p0, Lcom/osp/app/signin/SignUpView;->aD:Ljava/lang/String;

    invoke-virtual {v2, v1, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    const-string v1, "key_name_check_givenname"

    iget-object v3, p0, Lcom/osp/app/signin/SignUpView;->aE:Ljava/lang/String;

    invoke-virtual {v2, v1, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    const-string v1, "key_name_check_birthdate"

    iget-object v3, p0, Lcom/osp/app/signin/SignUpView;->aF:Ljava/lang/String;

    invoke-virtual {v2, v1, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    const-string v1, "key_name_check_mobile"

    iget-object v3, p0, Lcom/osp/app/signin/SignUpView;->aG:Ljava/lang/String;

    invoke-virtual {v2, v1, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    const-string v1, "key_name_check_method"

    iget-object v3, p0, Lcom/osp/app/signin/SignUpView;->aI:Ljava/lang/String;

    invoke-virtual {v2, v1, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    const-string v1, "key_name_check_ci"

    iget-object v3, p0, Lcom/osp/app/signin/SignUpView;->aJ:Ljava/lang/String;

    invoke-virtual {v2, v1, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    const-string v1, "key_name_check_di"

    iget-object v3, p0, Lcom/osp/app/signin/SignUpView;->aK:Ljava/lang/String;

    invoke-virtual {v2, v1, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    const-string v1, "key_name_check_datetime"

    iget-object v3, p0, Lcom/osp/app/signin/SignUpView;->aL:Ljava/lang/String;

    invoke-virtual {v2, v1, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    const-string v1, "key_name_check_gender"

    iget-object v3, p0, Lcom/osp/app/signin/SignUpView;->aM:Ljava/lang/String;

    invoke-virtual {v2, v1, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    const-string v1, "key_name_check_foreigner"

    iget-object v3, p0, Lcom/osp/app/signin/SignUpView;->aN:Ljava/lang/String;

    invoke-virtual {v2, v1, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    const-string v1, "client_id"

    iget-object v3, p0, Lcom/osp/app/signin/SignUpView;->ag:Ljava/lang/String;

    invoke-virtual {v2, v1, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    const-string v1, "client_secret"

    iget-object v3, p0, Lcom/osp/app/signin/SignUpView;->ah:Ljava/lang/String;

    invoke-virtual {v2, v1, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    const-string v1, "account_mode"

    iget-object v3, p0, Lcom/osp/app/signin/SignUpView;->W:Ljava/lang/String;

    invoke-virtual {v2, v1, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    const-string v1, "service_name"

    iget-object v3, p0, Lcom/osp/app/signin/SignUpView;->aU:Ljava/lang/String;

    invoke-virtual {v2, v1, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    const-string v1, "OSP_VER"

    iget-object v3, p0, Lcom/osp/app/signin/SignUpView;->an:Ljava/lang/String;

    invoke-virtual {v2, v1, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    const-string v3, "email_id"

    iget-object v1, p0, Lcom/osp/app/signin/SignUpView;->aq:Lcom/osp/app/signin/SignUpinfo;

    if-eqz v1, :cond_4

    iget-object v1, p0, Lcom/osp/app/signin/SignUpView;->aq:Lcom/osp/app/signin/SignUpinfo;

    invoke-virtual {v1}, Lcom/osp/app/signin/SignUpinfo;->e()Ljava/lang/String;

    move-result-object v1

    :goto_1
    invoke-virtual {v2, v3, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    const-string v1, "key_str_signup_info"

    invoke-virtual {v2, v1, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    const-string v0, "authcode"

    iget-object v1, p0, Lcom/osp/app/signin/SignUpView;->as:Ljava/lang/String;

    invoke-virtual {v2, v0, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    const-string v0, "key_internal_sign_up_inforamtion"

    iget-object v1, p0, Lcom/osp/app/signin/SignUpView;->aq:Lcom/osp/app/signin/SignUpinfo;

    invoke-virtual {v2, v0, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    const-string v0, "key_google_account"

    iget-boolean v1, p0, Lcom/osp/app/signin/SignUpView;->aT:Z

    invoke-virtual {v2, v0, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    const-string v0, "key_facebook_account"

    iget-boolean v1, p0, Lcom/osp/app/signin/SignUpView;->aY:Z

    invoke-virtual {v2, v0, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    const-string v0, "key_weibo_account"

    iget-boolean v1, p0, Lcom/osp/app/signin/SignUpView;->bf:Z

    invoke-virtual {v2, v0, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    const-string v0, "country_code_mcc"

    iget-object v1, p0, Lcom/osp/app/signin/SignUpView;->aj:Ljava/lang/String;

    invoke-virtual {v2, v0, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    iget-boolean v0, p0, Lcom/osp/app/signin/SignUpView;->aQ:Z

    if-eqz v0, :cond_0

    const-string v0, "new_add_account_mode"

    invoke-virtual {v2, v0, v6}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    :cond_0
    const-string v0, "BG_mode"

    iget-object v1, p0, Lcom/osp/app/signin/SignUpView;->aV:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    const-string v0, "BG_WhoareU"

    iget-object v1, p0, Lcom/osp/app/signin/SignUpView;->aV:Ljava/lang/String;

    invoke-virtual {v2, v0, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    iget-object v0, p0, Lcom/osp/app/signin/SignUpView;->ar:Ljava/lang/String;

    if-eqz v0, :cond_1

    const-string v0, "mypackage"

    iget-object v1, p0, Lcom/osp/app/signin/SignUpView;->ar:Ljava/lang/String;

    invoke-virtual {v2, v0, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    const-string v0, "key_request_id"

    iget-wide v4, p0, Lcom/osp/app/signin/SignUpView;->aR:J

    invoke-virtual {v2, v0, v4, v5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;J)Landroid/content/Intent;

    :cond_1
    const-string v0, "MODE"

    iget-object v1, p0, Lcom/osp/app/signin/SignUpView;->ao:Ljava/lang/String;

    invoke-virtual {v2, v0, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    const-string v0, "serviceApp_type"

    iget v1, p0, Lcom/osp/app/signin/SignUpView;->aW:I

    invoke-virtual {v2, v0, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    const-string v0, "key_easy_signup_tnc_mode"

    iget-boolean v1, p0, Lcom/osp/app/signin/SignUpView;->ak:Z

    invoke-virtual {v2, v0, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    const-string v0, "is_cancelable_just_one_activity"

    invoke-virtual {v2, v0, v6}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    const-string v0, "is_signup_flow"

    invoke-virtual {v2, v0, v6}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    iget-boolean v0, p0, Lcom/osp/app/signin/SignUpView;->bk:Z

    if-eqz v0, :cond_2

    const-string v0, "key_welcome_content"

    iget-object v1, p0, Lcom/osp/app/signin/SignUpView;->bj:Ljava/lang/String;

    invoke-virtual {v2, v0, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    const-string v0, "key_marketingpopup_mode"

    invoke-virtual {v2, v0, v6}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    :cond_2
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "request Tnc - client_id : "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v1, p0, Lcom/osp/app/signin/SignUpView;->ag:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;)V

    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "request Tnc - client_secret : "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v1, p0, Lcom/osp/app/signin/SignUpView;->ah:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;)V

    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "request Tnc - account_mode : "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v1, p0, Lcom/osp/app/signin/SignUpView;->W:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;)V

    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "request Tnc - service_name : "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v1, p0, Lcom/osp/app/signin/SignUpView;->aU:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;)V

    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "request Tnc - OSP_VER : "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v1, p0, Lcom/osp/app/signin/SignUpView;->an:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;)V

    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v0, "request Tnc - new_add_account_mode : true"

    invoke-static {v0}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;)V

    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "request Tnc - BG_WhoareU : "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v1, p0, Lcom/osp/app/signin/SignUpView;->aV:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;)V

    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "request Tnc - mypackage : "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v1, p0, Lcom/osp/app/signin/SignUpView;->ar:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;)V

    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "request Tnc - key_request_id : "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-wide v4, p0, Lcom/osp/app/signin/SignUpView;->aR:J

    invoke-virtual {v0, v4, v5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;)V

    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "request Tnc - MODE : "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v1, p0, Lcom/osp/app/signin/SignUpView;->ao:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;)V

    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "request Tnc - serviceApp_type : "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v1, p0, Lcom/osp/app/signin/SignUpView;->aW:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;)V

    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v0, "request Tnc - is_cancelable_just_one_activity : true"

    invoke-static {v0}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;)V

    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v0, "request Tnc - is_signup_flow : true"

    invoke-static {v0}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;)V

    const/16 v0, 0xce

    invoke-virtual {p0, v2, v0}, Lcom/osp/app/signin/SignUpView;->startActivityForResult(Landroid/content/Intent;I)V

    return-void

    :cond_3
    const-string v0, "SA_EMAIL_;SA_COUNTRY_;SA_BIRTHDATE_;SA_EMAILRECEIVE_;"

    goto/16 :goto_0

    :cond_4
    const/4 v1, 0x0

    goto/16 :goto_1
.end method

.method static synthetic ab(Lcom/osp/app/signin/SignUpView;)Landroid/app/AlertDialog;
    .locals 1

    .prologue
    .line 112
    iget-object v0, p0, Lcom/osp/app/signin/SignUpView;->aa:Landroid/app/AlertDialog;

    return-object v0
.end method

.method static synthetic ac(Lcom/osp/app/signin/SignUpView;)V
    .locals 0

    .prologue
    .line 112
    invoke-direct {p0}, Lcom/osp/app/signin/SignUpView;->c()V

    return-void
.end method

.method static synthetic ad(Lcom/osp/app/signin/SignUpView;)Landroid/app/AlertDialog;
    .locals 1

    .prologue
    .line 112
    iget-object v0, p0, Lcom/osp/app/signin/SignUpView;->ab:Landroid/app/AlertDialog;

    return-object v0
.end method

.method static synthetic ae(Lcom/osp/app/signin/SignUpView;)Landroid/app/AlertDialog;
    .locals 1

    .prologue
    .line 112
    iget-object v0, p0, Lcom/osp/app/signin/SignUpView;->ac:Landroid/app/AlertDialog;

    return-object v0
.end method

.method static synthetic af(Lcom/osp/app/signin/SignUpView;)V
    .locals 7

    .prologue
    const/4 v6, 0x1

    .line 112
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v0, "AccountView::ShowSmsActivity isCancelableJustOneActivity : true"

    invoke-static {v0}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/osp/app/signin/SignUpView;->aq:Lcom/osp/app/signin/SignUpinfo;

    if-nez v0, :cond_3

    iget-object v0, p0, Lcom/osp/app/signin/SignUpView;->aS:Ljava/lang/String;

    :goto_0
    const/4 v1, 0x0

    iput v1, p0, Lcom/osp/app/signin/SignUpView;->aW:I

    new-instance v2, Landroid/content/Intent;

    invoke-direct {v2}, Landroid/content/Intent;-><init>()V

    const-class v1, Lcom/osp/app/signin/DuplicateSMSVerificationActivity;

    invoke-virtual {v2, p0, v1}, Landroid/content/Intent;->setClass(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;

    const-string v1, "key_is_name_verified"

    iget-boolean v3, p0, Lcom/osp/app/signin/SignUpView;->U:Z

    invoke-virtual {v2, v1, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    const-string v1, "key_name_check_familyname"

    iget-object v3, p0, Lcom/osp/app/signin/SignUpView;->aD:Ljava/lang/String;

    invoke-virtual {v2, v1, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    const-string v1, "key_name_check_givenname"

    iget-object v3, p0, Lcom/osp/app/signin/SignUpView;->aE:Ljava/lang/String;

    invoke-virtual {v2, v1, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    const-string v1, "key_name_check_birthdate"

    iget-object v3, p0, Lcom/osp/app/signin/SignUpView;->aF:Ljava/lang/String;

    invoke-virtual {v2, v1, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    const-string v1, "key_name_check_mobile"

    iget-object v3, p0, Lcom/osp/app/signin/SignUpView;->aG:Ljava/lang/String;

    invoke-virtual {v2, v1, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    const-string v1, "key_name_check_method"

    iget-object v3, p0, Lcom/osp/app/signin/SignUpView;->aI:Ljava/lang/String;

    invoke-virtual {v2, v1, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    const-string v1, "key_name_check_ci"

    iget-object v3, p0, Lcom/osp/app/signin/SignUpView;->aJ:Ljava/lang/String;

    invoke-virtual {v2, v1, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    const-string v1, "key_name_check_di"

    iget-object v3, p0, Lcom/osp/app/signin/SignUpView;->aK:Ljava/lang/String;

    invoke-virtual {v2, v1, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    const-string v1, "key_name_check_datetime"

    iget-object v3, p0, Lcom/osp/app/signin/SignUpView;->aL:Ljava/lang/String;

    invoke-virtual {v2, v1, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    const-string v1, "key_name_check_gender"

    iget-object v3, p0, Lcom/osp/app/signin/SignUpView;->aM:Ljava/lang/String;

    invoke-virtual {v2, v1, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    const-string v1, "key_name_check_foreigner"

    iget-object v3, p0, Lcom/osp/app/signin/SignUpView;->aN:Ljava/lang/String;

    invoke-virtual {v2, v1, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    const-string v1, "client_id"

    iget-object v3, p0, Lcom/osp/app/signin/SignUpView;->ag:Ljava/lang/String;

    invoke-virtual {v2, v1, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    const-string v1, "client_secret"

    iget-object v3, p0, Lcom/osp/app/signin/SignUpView;->ah:Ljava/lang/String;

    invoke-virtual {v2, v1, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    const-string v1, "account_mode"

    iget-object v3, p0, Lcom/osp/app/signin/SignUpView;->W:Ljava/lang/String;

    invoke-virtual {v2, v1, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    const-string v1, "service_name"

    iget-object v3, p0, Lcom/osp/app/signin/SignUpView;->aU:Ljava/lang/String;

    invoke-virtual {v2, v1, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    const-string v1, "OSP_VER"

    iget-object v3, p0, Lcom/osp/app/signin/SignUpView;->an:Ljava/lang/String;

    invoke-virtual {v2, v1, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    const-string v3, "email_id"

    iget-object v1, p0, Lcom/osp/app/signin/SignUpView;->aq:Lcom/osp/app/signin/SignUpinfo;

    if-eqz v1, :cond_4

    iget-object v1, p0, Lcom/osp/app/signin/SignUpView;->aq:Lcom/osp/app/signin/SignUpinfo;

    invoke-virtual {v1}, Lcom/osp/app/signin/SignUpinfo;->e()Ljava/lang/String;

    move-result-object v1

    :goto_1
    invoke-virtual {v2, v3, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    const-string v1, "key_str_signup_info"

    invoke-virtual {v2, v1, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    const-string v0, "authcode"

    iget-object v1, p0, Lcom/osp/app/signin/SignUpView;->as:Ljava/lang/String;

    invoke-virtual {v2, v0, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    const-string v0, "key_internal_sign_up_inforamtion"

    iget-object v1, p0, Lcom/osp/app/signin/SignUpView;->aq:Lcom/osp/app/signin/SignUpinfo;

    invoke-virtual {v2, v0, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    const-string v0, "key_google_account"

    iget-boolean v1, p0, Lcom/osp/app/signin/SignUpView;->aT:Z

    invoke-virtual {v2, v0, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    const-string v0, "key_facebook_account"

    iget-boolean v1, p0, Lcom/osp/app/signin/SignUpView;->aY:Z

    invoke-virtual {v2, v0, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    const-string v0, "key_weibo_account"

    iget-boolean v1, p0, Lcom/osp/app/signin/SignUpView;->bf:Z

    invoke-virtual {v2, v0, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    const-string v0, "country_code_mcc"

    iget-object v1, p0, Lcom/osp/app/signin/SignUpView;->aj:Ljava/lang/String;

    invoke-virtual {v2, v0, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    iget-boolean v0, p0, Lcom/osp/app/signin/SignUpView;->aQ:Z

    if-eqz v0, :cond_0

    const-string v0, "new_add_account_mode"

    invoke-virtual {v2, v0, v6}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    :cond_0
    const-string v0, "BG_mode"

    iget-object v1, p0, Lcom/osp/app/signin/SignUpView;->aV:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    const-string v0, "BG_WhoareU"

    iget-object v1, p0, Lcom/osp/app/signin/SignUpView;->aV:Ljava/lang/String;

    invoke-virtual {v2, v0, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    iget-object v0, p0, Lcom/osp/app/signin/SignUpView;->ar:Ljava/lang/String;

    if-eqz v0, :cond_1

    const-string v0, "mypackage"

    iget-object v1, p0, Lcom/osp/app/signin/SignUpView;->ar:Ljava/lang/String;

    invoke-virtual {v2, v0, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    const-string v0, "key_request_id"

    iget-wide v4, p0, Lcom/osp/app/signin/SignUpView;->aR:J

    invoke-virtual {v2, v0, v4, v5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;J)Landroid/content/Intent;

    :cond_1
    const-string v0, "MODE"

    iget-object v1, p0, Lcom/osp/app/signin/SignUpView;->ao:Ljava/lang/String;

    invoke-virtual {v2, v0, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    const-string v0, "serviceApp_type"

    iget v1, p0, Lcom/osp/app/signin/SignUpView;->aW:I

    invoke-virtual {v2, v0, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    const-string v0, "key_easy_signup_tnc_mode"

    iget-boolean v1, p0, Lcom/osp/app/signin/SignUpView;->ak:Z

    invoke-virtual {v2, v0, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    const-string v0, "is_cancelable_just_one_activity"

    invoke-virtual {v2, v0, v6}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    const-string v0, "is_signup_flow"

    invoke-virtual {v2, v0, v6}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    iget-boolean v0, p0, Lcom/osp/app/signin/SignUpView;->bk:Z

    if-eqz v0, :cond_2

    const-string v0, "key_welcome_content"

    iget-object v1, p0, Lcom/osp/app/signin/SignUpView;->bj:Ljava/lang/String;

    invoke-virtual {v2, v0, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    const-string v0, "key_marketingpopup_mode"

    invoke-virtual {v2, v0, v6}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    :cond_2
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "request Tnc - client_id : "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v1, p0, Lcom/osp/app/signin/SignUpView;->ag:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;)V

    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "request Tnc - client_secret : "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v1, p0, Lcom/osp/app/signin/SignUpView;->ah:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;)V

    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "request Tnc - account_mode : "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v1, p0, Lcom/osp/app/signin/SignUpView;->W:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;)V

    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "request Tnc - service_name : "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v1, p0, Lcom/osp/app/signin/SignUpView;->aU:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;)V

    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "request Tnc - OSP_VER : "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v1, p0, Lcom/osp/app/signin/SignUpView;->an:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;)V

    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v0, "request Tnc - new_add_account_mode : true"

    invoke-static {v0}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;)V

    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "request Tnc - BG_WhoareU : "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v1, p0, Lcom/osp/app/signin/SignUpView;->aV:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;)V

    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "request Tnc - mypackage : "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v1, p0, Lcom/osp/app/signin/SignUpView;->ar:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;)V

    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "request Tnc - key_request_id : "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-wide v4, p0, Lcom/osp/app/signin/SignUpView;->aR:J

    invoke-virtual {v0, v4, v5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;)V

    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "request Tnc - MODE : "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v1, p0, Lcom/osp/app/signin/SignUpView;->ao:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;)V

    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "request Tnc - serviceApp_type : "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v1, p0, Lcom/osp/app/signin/SignUpView;->aW:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;)V

    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v0, "request Tnc - is_cancelable_just_one_activity : true"

    invoke-static {v0}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;)V

    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v0, "request Tnc - is_signup_flow : true"

    invoke-static {v0}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;)V

    const/16 v0, 0xce

    invoke-virtual {p0, v2, v0}, Lcom/osp/app/signin/SignUpView;->startActivityForResult(Landroid/content/Intent;I)V

    return-void

    :cond_3
    const-string v0, "SA_EMAIL_;SA_COUNTRY_;SA_BIRTHDATE_;SA_EMAILRECEIVE_;"

    goto/16 :goto_0

    :cond_4
    const/4 v1, 0x0

    goto/16 :goto_1
.end method

.method static synthetic ag(Lcom/osp/app/signin/SignUpView;)Landroid/app/AlertDialog;
    .locals 1

    .prologue
    .line 112
    iget-object v0, p0, Lcom/osp/app/signin/SignUpView;->ad:Landroid/app/AlertDialog;

    return-object v0
.end method

.method static synthetic ah(Lcom/osp/app/signin/SignUpView;)Ljava/util/ArrayList;
    .locals 1

    .prologue
    .line 112
    iget-object v0, p0, Lcom/osp/app/signin/SignUpView;->bm:Ljava/util/ArrayList;

    return-object v0
.end method

.method static synthetic ai(Lcom/osp/app/signin/SignUpView;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 112
    iget-object v0, p0, Lcom/osp/app/signin/SignUpView;->ba:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic b(Lcom/osp/app/signin/SignUpView;I)I
    .locals 0

    .prologue
    .line 112
    iput p1, p0, Lcom/osp/app/signin/SignUpView;->ay:I

    return p1
.end method

.method static synthetic b(Lcom/osp/app/signin/SignUpView;Lcom/osp/app/signin/kd;)Lcom/osp/app/signin/kd;
    .locals 0

    .prologue
    .line 112
    iput-object p1, p0, Lcom/osp/app/signin/SignUpView;->bC:Lcom/osp/app/signin/kd;

    return-object p1
.end method

.method static synthetic b(Lcom/osp/app/signin/SignUpView;Ljava/lang/String;)Ljava/lang/String;
    .locals 0

    .prologue
    .line 112
    iput-object p1, p0, Lcom/osp/app/signin/SignUpView;->aP:Ljava/lang/String;

    return-object p1
.end method

.method static synthetic b(Lcom/osp/app/signin/SignUpView;Landroid/app/AlertDialog;)V
    .locals 0

    .prologue
    .line 112
    invoke-direct {p0, p1}, Lcom/osp/app/signin/SignUpView;->a(Landroid/app/AlertDialog;)V

    return-void
.end method

.method static synthetic b(Lcom/osp/app/signin/SignUpView;)Z
    .locals 1

    .prologue
    .line 112
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/osp/app/signin/SignUpView;->aX:Z

    return v0
.end method

.method static synthetic c(Lcom/osp/app/signin/SignUpView;I)I
    .locals 0

    .prologue
    .line 112
    iput p1, p0, Lcom/osp/app/signin/SignUpView;->aA:I

    return p1
.end method

.method static synthetic c(Lcom/osp/app/signin/SignUpView;Landroid/app/AlertDialog;)Landroid/app/AlertDialog;
    .locals 0

    .prologue
    .line 112
    iput-object p1, p0, Lcom/osp/app/signin/SignUpView;->ae:Landroid/app/AlertDialog;

    return-object p1
.end method

.method static synthetic c(Lcom/osp/app/signin/SignUpView;Lcom/osp/app/signin/kd;)Lcom/osp/app/signin/kd;
    .locals 0

    .prologue
    .line 112
    iput-object p1, p0, Lcom/osp/app/signin/SignUpView;->bD:Lcom/osp/app/signin/kd;

    return-object p1
.end method

.method static synthetic c(Lcom/osp/app/signin/SignUpView;Ljava/lang/String;)Ljava/lang/String;
    .locals 0

    .prologue
    .line 112
    iput-object p1, p0, Lcom/osp/app/signin/SignUpView;->aZ:Ljava/lang/String;

    return-object p1
.end method

.method private c()V
    .locals 8

    .prologue
    const/4 v4, 0x1

    const/4 v2, 0x0

    .line 1390
    const-string v0, "input_method"

    invoke-virtual {p0, v0}, Lcom/osp/app/signin/SignUpView;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/inputmethod/InputMethodManager;

    .line 1392
    const/16 v1, 0xd

    new-array v5, v1, [Landroid/widget/EditText;

    iget-object v1, p0, Lcom/osp/app/signin/SignUpView;->F:Landroid/widget/AutoCompleteTextView;

    aput-object v1, v5, v2

    iget-object v1, p0, Lcom/osp/app/signin/SignUpView;->E:Landroid/widget/EditText;

    aput-object v1, v5, v4

    const/4 v1, 0x2

    iget-object v3, p0, Lcom/osp/app/signin/SignUpView;->D:Landroid/widget/EditText;

    aput-object v3, v5, v1

    const/4 v1, 0x3

    iget-object v3, p0, Lcom/osp/app/signin/SignUpView;->I:Landroid/widget/EditText;

    aput-object v3, v5, v1

    const/4 v1, 0x4

    iget-object v3, p0, Lcom/osp/app/signin/SignUpView;->J:Landroid/widget/EditText;

    aput-object v3, v5, v1

    const/4 v1, 0x5

    iget-object v3, p0, Lcom/osp/app/signin/SignUpView;->K:Landroid/widget/EditText;

    aput-object v3, v5, v1

    const/4 v1, 0x6

    iget-object v3, p0, Lcom/osp/app/signin/SignUpView;->L:Landroid/widget/EditText;

    aput-object v3, v5, v1

    const/4 v1, 0x7

    iget-object v3, p0, Lcom/osp/app/signin/SignUpView;->O:Landroid/widget/EditText;

    aput-object v3, v5, v1

    const/16 v1, 0x8

    iget-object v3, p0, Lcom/osp/app/signin/SignUpView;->R:Landroid/widget/EditText;

    aput-object v3, v5, v1

    const/16 v1, 0x9

    iget-object v3, p0, Lcom/osp/app/signin/SignUpView;->G:Landroid/widget/EditText;

    aput-object v3, v5, v1

    const/16 v1, 0xa

    iget-object v3, p0, Lcom/osp/app/signin/SignUpView;->M:Landroid/widget/EditText;

    aput-object v3, v5, v1

    const/16 v1, 0xb

    iget-object v3, p0, Lcom/osp/app/signin/SignUpView;->P:Landroid/widget/EditText;

    aput-object v3, v5, v1

    const/16 v1, 0xc

    iget-object v3, p0, Lcom/osp/app/signin/SignUpView;->S:Landroid/widget/EditText;

    aput-object v3, v5, v1

    move v1, v2

    .line 1394
    :goto_0
    array-length v3, v5

    if-ge v1, v3, :cond_0

    .line 1396
    aget-object v3, v5, v1

    if-eqz v3, :cond_1

    invoke-virtual {v3}, Landroid/widget/EditText;->hasFocus()Z

    move-result v6

    if-ne v6, v4, :cond_1

    invoke-virtual {v3}, Landroid/widget/EditText;->getWindowToken()Landroid/os/IBinder;

    move-result-object v3

    invoke-virtual {v0, v3, v2}, Landroid/view/inputmethod/InputMethodManager;->hideSoftInputFromWindow(Landroid/os/IBinder;I)Z

    move v3, v4

    :goto_1
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    new-instance v6, Ljava/lang/StringBuilder;

    const-string v7, "SignUpView::CheckCloseIME _skip : "

    invoke-direct {v6, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v6, v3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v6}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;)V

    if-nez v3, :cond_0

    .line 1398
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 1402
    :cond_0
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v0, "SignUpView::CloseIME skip : false"

    invoke-static {v0}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;)V

    .line 1403
    return-void

    :cond_1
    move v3, v2

    goto :goto_1
.end method

.method static synthetic c(Lcom/osp/app/signin/SignUpView;)V
    .locals 0

    .prologue
    .line 112
    invoke-direct {p0}, Lcom/osp/app/signin/SignUpView;->s()V

    return-void
.end method

.method static synthetic d(Lcom/osp/app/signin/SignUpView;I)I
    .locals 0

    .prologue
    .line 112
    iput p1, p0, Lcom/osp/app/signin/SignUpView;->aB:I

    return p1
.end method

.method static synthetic d(Lcom/osp/app/signin/SignUpView;Landroid/app/AlertDialog;)Landroid/app/AlertDialog;
    .locals 0

    .prologue
    .line 112
    iput-object p1, p0, Lcom/osp/app/signin/SignUpView;->af:Landroid/app/AlertDialog;

    return-object p1
.end method

.method static synthetic d(Lcom/osp/app/signin/SignUpView;Ljava/lang/String;)Ljava/lang/String;
    .locals 0

    .prologue
    .line 112
    iput-object p1, p0, Lcom/osp/app/signin/SignUpView;->bg:Ljava/lang/String;

    return-object p1
.end method

.method private d()V
    .locals 10

    .prologue
    const/16 v9, 0x8

    const/16 v8, 0x91

    const/4 v3, 0x1

    const/4 v4, 0x0

    .line 1482
    iget-object v0, p0, Lcom/osp/app/signin/SignUpView;->be:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    .line 1484
    const v0, 0x7f0c0002

    invoke-virtual {p0, v0}, Lcom/osp/app/signin/SignUpView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 1485
    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "* "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const v2, 0x7f09008c

    invoke-virtual {p0, v2}, Lcom/osp/app/signin/SignUpView;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 1487
    invoke-static {}, Lcom/osp/app/signin/SamsungService;->a()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-static {p0}, Lcom/osp/app/util/r;->m(Landroid/content/Context;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 1489
    const-string v1, "#eeeeee"

    invoke-static {v1}, Landroid/graphics/Color;->parseColor(Ljava/lang/String;)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setTextColor(I)V

    .line 1492
    :cond_0
    invoke-virtual {p0}, Lcom/osp/app/signin/SignUpView;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    .line 1494
    const v0, 0x7f0c013a

    invoke-virtual {p0, v0}, Lcom/osp/app/signin/SignUpView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/AutoCompleteTextView;

    iput-object v0, p0, Lcom/osp/app/signin/SignUpView;->F:Landroid/widget/AutoCompleteTextView;

    .line 1495
    const v0, 0x7f0c00ab

    invoke-virtual {p0, v0}, Lcom/osp/app/signin/SignUpView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/EditText;

    iput-object v0, p0, Lcom/osp/app/signin/SignUpView;->E:Landroid/widget/EditText;

    .line 1496
    const v0, 0x7f0c0140

    invoke-virtual {p0, v0}, Lcom/osp/app/signin/SignUpView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/EditText;

    iput-object v0, p0, Lcom/osp/app/signin/SignUpView;->D:Landroid/widget/EditText;

    .line 1498
    iget-object v0, p0, Lcom/osp/app/signin/SignUpView;->F:Landroid/widget/AutoCompleteTextView;

    iget-object v1, p0, Lcom/osp/app/signin/SignUpView;->E:Landroid/widget/EditText;

    invoke-virtual {v1}, Landroid/widget/EditText;->getHintTextColors()Landroid/content/res/ColorStateList;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/AutoCompleteTextView;->setHintTextColor(Landroid/content/res/ColorStateList;)V

    .line 1499
    iget-object v0, p0, Lcom/osp/app/signin/SignUpView;->F:Landroid/widget/AutoCompleteTextView;

    iget-object v1, p0, Lcom/osp/app/signin/SignUpView;->E:Landroid/widget/EditText;

    invoke-virtual {v1}, Landroid/widget/EditText;->getTypeface()Landroid/graphics/Typeface;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/AutoCompleteTextView;->setTypeface(Landroid/graphics/Typeface;)V

    .line 1500
    iget-object v0, p0, Lcom/osp/app/signin/SignUpView;->F:Landroid/widget/AutoCompleteTextView;

    iget-object v1, p0, Lcom/osp/app/signin/SignUpView;->E:Landroid/widget/EditText;

    invoke-virtual {v1}, Landroid/widget/EditText;->getTextSize()F

    move-result v1

    invoke-virtual {v0, v4, v1}, Landroid/widget/AutoCompleteTextView;->setTextSize(IF)V

    .line 1502
    iget-object v0, p0, Lcom/osp/app/signin/SignUpView;->F:Landroid/widget/AutoCompleteTextView;

    invoke-virtual {v0, p0}, Landroid/widget/AutoCompleteTextView;->setOnFocusChangeListener(Landroid/view/View$OnFocusChangeListener;)V

    .line 1503
    iget-object v0, p0, Lcom/osp/app/signin/SignUpView;->E:Landroid/widget/EditText;

    invoke-virtual {v0, p0}, Landroid/widget/EditText;->setOnFocusChangeListener(Landroid/view/View$OnFocusChangeListener;)V

    .line 1504
    iget-object v0, p0, Lcom/osp/app/signin/SignUpView;->D:Landroid/widget/EditText;

    invoke-virtual {v0, p0}, Landroid/widget/EditText;->setOnFocusChangeListener(Landroid/view/View$OnFocusChangeListener;)V

    .line 1506
    invoke-direct {p0}, Lcom/osp/app/signin/SignUpView;->y()V

    .line 1507
    const-string v0, "CN"

    invoke-static {}, Lcom/osp/device/b;->b()Lcom/osp/device/b;

    move-result-object v1

    invoke-virtual {v1}, Lcom/osp/device/b;->n()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1a

    iget-boolean v0, p0, Lcom/osp/app/signin/SignUpView;->bw:Z

    if-nez v0, :cond_1a

    const v0, 0x7f0c0134

    invoke-virtual {p0, v0}, Lcom/osp/app/signin/SignUpView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    const v1, 0x7f0c0135

    invoke-virtual {p0, v1}, Lcom/osp/app/signin/SignUpView;->findViewById(I)Landroid/view/View;

    move-result-object v1

    invoke-virtual {v0, v4}, Landroid/widget/LinearLayout;->setVisibility(I)V

    new-instance v0, Lcom/osp/app/signin/kp;

    invoke-direct {v0, p0}, Lcom/osp/app/signin/kp;-><init>(Lcom/osp/app/signin/SignUpView;)V

    invoke-virtual {v1, v0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    move v0, v3

    :goto_0
    const-string v1, "CN"

    invoke-static {}, Lcom/osp/device/b;->b()Lcom/osp/device/b;

    move-result-object v2

    invoke-virtual {v2}, Lcom/osp/device/b;->n()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    sget v1, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v2, 0xf

    if-lt v1, v2, :cond_1

    iget-boolean v1, p0, Lcom/osp/app/signin/SignUpView;->bw:Z

    if-nez v1, :cond_1

    const v0, 0x7f0c0136

    invoke-virtual {p0, v0}, Lcom/osp/app/signin/SignUpView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    const v1, 0x7f0c0137

    invoke-virtual {p0, v1}, Lcom/osp/app/signin/SignUpView;->findViewById(I)Landroid/view/View;

    move-result-object v1

    invoke-virtual {v0, v4}, Landroid/widget/LinearLayout;->setVisibility(I)V

    new-instance v0, Lcom/osp/app/signin/kq;

    invoke-direct {v0, p0}, Lcom/osp/app/signin/kq;-><init>(Lcom/osp/app/signin/SignUpView;)V

    invoke-virtual {v1, v0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    move v0, v3

    :cond_1
    if-eqz v0, :cond_2

    const v0, 0x7f0c0138

    invoke-virtual {p0, v0}, Lcom/osp/app/signin/SignUpView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    if-eqz v0, :cond_2

    invoke-static {}, Lcom/osp/app/util/r;->v()Z

    move-result v1

    if-eqz v1, :cond_13

    invoke-virtual {v0, v9}, Landroid/view/View;->setVisibility(I)V

    .line 1509
    :cond_2
    :goto_1
    const v0, 0x7f0c013f

    invoke-virtual {p0, v0}, Lcom/osp/app/signin/SignUpView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 1510
    const v1, 0x7f0c013d

    invoke-virtual {p0, v1}, Lcom/osp/app/signin/SignUpView;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    .line 1511
    const v2, 0x7f0c0004

    invoke-virtual {p0, v2}, Lcom/osp/app/signin/SignUpView;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    .line 1513
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const v7, 0x7f09001b

    invoke-virtual {p0, v7}, Lcom/osp/app/signin/SignUpView;->getText(I)Ljava/lang/CharSequence;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, " *"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v0, v6}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 1514
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const v6, 0x7f090045

    invoke-virtual {p0, v6}, Lcom/osp/app/signin/SignUpView;->getText(I)Ljava/lang/CharSequence;

    move-result-object v6

    invoke-virtual {v0, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v6, " *"

    invoke-virtual {v0, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 1515
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const v1, 0x7f090023

    invoke-virtual {p0, v1}, Lcom/osp/app/signin/SignUpView;->getText(I)Ljava/lang/CharSequence;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " *"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 1517
    invoke-static {}, Lcom/osp/device/b;->b()Lcom/osp/device/b;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/osp/device/b;->i(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 1519
    const v0, 0x7f0c00f7

    invoke-virtual {p0, v0}, Lcom/osp/app/signin/SignUpView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 1520
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const v2, 0x7f090067

    invoke-virtual {p0, v2}, Lcom/osp/app/signin/SignUpView;->getText(I)Ljava/lang/CharSequence;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " *"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 1522
    const v0, 0x7f0c0143

    invoke-virtual {p0, v0}, Lcom/osp/app/signin/SignUpView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 1523
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const v2, 0x7f090020

    invoke-virtual {p0, v2}, Lcom/osp/app/signin/SignUpView;->getText(I)Ljava/lang/CharSequence;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " *"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 1526
    :cond_3
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x15

    if-lt v0, v1, :cond_4

    .line 1528
    const v0, 0x7f0c0144

    invoke-virtual {p0, v0}, Lcom/osp/app/signin/SignUpView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    .line 1529
    if-eqz v0, :cond_4

    .line 1531
    invoke-virtual {v0, v9}, Landroid/view/View;->setVisibility(I)V

    .line 1536
    :cond_4
    const v0, 0x7f0c0026

    invoke-virtual {p0, v0}, Lcom/osp/app/signin/SignUpView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    .line 1537
    if-eqz v0, :cond_5

    .line 1539
    invoke-virtual {v0, p0}, Landroid/widget/ImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1542
    :cond_5
    const v0, 0x7f0c0018

    invoke-virtual {p0, v0}, Lcom/osp/app/signin/SignUpView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    .line 1543
    if-eqz v0, :cond_6

    .line 1545
    new-instance v1, Lcom/osp/app/signin/ls;

    invoke-direct {v1, p0}, Lcom/osp/app/signin/ls;-><init>(Lcom/osp/app/signin/SignUpView;)V

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1590
    :cond_6
    const v0, 0x7f0c0109

    invoke-virtual {p0, v0}, Lcom/osp/app/signin/SignUpView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    .line 1591
    const v1, 0x7f0c0107

    invoke-virtual {p0, v1}, Lcom/osp/app/signin/SignUpView;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/LinearLayout;

    .line 1593
    if-eqz v0, :cond_7

    if-eqz v1, :cond_7

    .line 1602
    invoke-virtual {v0, v4}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 1603
    new-instance v1, Lcom/osp/app/signin/lu;

    invoke-direct {v1, p0}, Lcom/osp/app/signin/lu;-><init>(Lcom/osp/app/signin/SignUpView;)V

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1649
    :cond_7
    const v0, 0x7f0c0146

    invoke-virtual {p0, v0}, Lcom/osp/app/signin/SignUpView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 1650
    const v1, 0x7f0c0145

    invoke-virtual {p0, v1}, Lcom/osp/app/signin/SignUpView;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    .line 1652
    iget-object v2, p0, Lcom/osp/app/signin/SignUpView;->bc:Ljava/lang/String;

    if-nez v2, :cond_14

    .line 1654
    iget-object v2, p0, Lcom/osp/app/signin/SignUpView;->aj:Ljava/lang/String;

    if-eqz v2, :cond_8

    .line 1656
    invoke-static {}, Lcom/osp/common/util/i;->a()Lcom/osp/common/util/i;

    iget-object v2, p0, Lcom/osp/app/signin/SignUpView;->aj:Ljava/lang/String;

    invoke-static {p0, v2}, Lcom/osp/common/util/i;->a(Landroid/content/Context;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 1663
    :cond_8
    :goto_2
    invoke-virtual {p0}, Lcom/osp/app/signin/SignUpView;->h()Z

    move-result v0

    if-eqz v0, :cond_15

    .line 1665
    const v0, 0x7f070006

    invoke-virtual {v5, v0}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setTextColor(I)V

    .line 1673
    :goto_3
    const v0, 0x7f0c0141

    invoke-virtual {p0, v0}, Lcom/osp/app/signin/SignUpView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/CheckBox;

    iput-object v0, p0, Lcom/osp/app/signin/SignUpView;->C:Landroid/widget/CheckBox;

    .line 1674
    iget-object v0, p0, Lcom/osp/app/signin/SignUpView;->C:Landroid/widget/CheckBox;

    iget-object v1, p0, Lcom/osp/app/signin/SignUpView;->bz:Landroid/view/View$OnKeyListener;

    invoke-virtual {v0, v1}, Landroid/widget/CheckBox;->setOnKeyListener(Landroid/view/View$OnKeyListener;)V

    .line 1676
    iget-object v0, p0, Lcom/osp/app/signin/SignUpView;->C:Landroid/widget/CheckBox;

    invoke-static {v0}, Lcom/osp/app/signin/SignUpView;->a(Landroid/widget/TextView;)V

    .line 1679
    iget-object v0, p0, Lcom/osp/app/signin/SignUpView;->C:Landroid/widget/CheckBox;

    if-eqz v0, :cond_9

    .line 1681
    iget-object v0, p0, Lcom/osp/app/signin/SignUpView;->C:Landroid/widget/CheckBox;

    invoke-virtual {v0, v3}, Landroid/widget/CheckBox;->setChecked(Z)V

    .line 1683
    :cond_9
    iget-object v0, p0, Lcom/osp/app/signin/SignUpView;->E:Landroid/widget/EditText;

    if-eqz v0, :cond_a

    .line 1685
    iget-object v0, p0, Lcom/osp/app/signin/SignUpView;->E:Landroid/widget/EditText;

    invoke-virtual {v0, v8}, Landroid/widget/EditText;->setInputType(I)V

    .line 1688
    :cond_a
    iget-object v0, p0, Lcom/osp/app/signin/SignUpView;->C:Landroid/widget/CheckBox;

    new-instance v1, Lcom/osp/app/signin/lw;

    invoke-direct {v1, p0}, Lcom/osp/app/signin/lw;-><init>(Lcom/osp/app/signin/SignUpView;)V

    invoke-virtual {v0, v1}, Landroid/widget/CheckBox;->setOnCheckedChangeListener(Landroid/widget/CompoundButton$OnCheckedChangeListener;)V

    .line 1729
    iget-object v0, p0, Lcom/osp/app/signin/SignUpView;->C:Landroid/widget/CheckBox;

    invoke-virtual {v0}, Landroid/widget/CheckBox;->isChecked()Z

    move-result v0

    if-eqz v0, :cond_c

    .line 1731
    iget-object v0, p0, Lcom/osp/app/signin/SignUpView;->E:Landroid/widget/EditText;

    invoke-virtual {v0, v8}, Landroid/widget/EditText;->setInputType(I)V

    .line 1732
    iget-object v0, p0, Lcom/osp/app/signin/SignUpView;->E:Landroid/widget/EditText;

    invoke-virtual {v0}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v0

    if-eqz v0, :cond_b

    .line 1734
    iget-object v0, p0, Lcom/osp/app/signin/SignUpView;->E:Landroid/widget/EditText;

    iget-object v1, p0, Lcom/osp/app/signin/SignUpView;->E:Landroid/widget/EditText;

    invoke-virtual {v1}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v1

    invoke-interface {v1}, Landroid/text/Editable;->length()I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setSelection(I)V

    .line 1736
    :cond_b
    iget-object v0, p0, Lcom/osp/app/signin/SignUpView;->D:Landroid/widget/EditText;

    invoke-virtual {v0, v8}, Landroid/widget/EditText;->setInputType(I)V

    .line 1737
    iget-object v0, p0, Lcom/osp/app/signin/SignUpView;->D:Landroid/widget/EditText;

    invoke-virtual {v0}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v0

    if-eqz v0, :cond_c

    .line 1739
    iget-object v0, p0, Lcom/osp/app/signin/SignUpView;->D:Landroid/widget/EditText;

    iget-object v1, p0, Lcom/osp/app/signin/SignUpView;->D:Landroid/widget/EditText;

    invoke-virtual {v1}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v1

    invoke-interface {v1}, Landroid/text/Editable;->length()I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setSelection(I)V

    .line 1743
    :cond_c
    invoke-static {}, Lcom/osp/device/b;->b()Lcom/osp/device/b;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/osp/device/b;->l(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_16

    .line 1745
    iget-object v0, p0, Lcom/osp/app/signin/SignUpView;->bx:Lcom/osp/app/signin/BottomSoftkeyLinearLayout;

    iget-object v1, p0, Lcom/osp/app/signin/SignUpView;->a:Landroid/view/View$OnClickListener;

    invoke-virtual {v0, v1}, Lcom/osp/app/signin/BottomSoftkeyLinearLayout;->a(Landroid/view/View$OnClickListener;)V

    .line 1746
    iget-object v0, p0, Lcom/osp/app/signin/SignUpView;->bx:Lcom/osp/app/signin/BottomSoftkeyLinearLayout;

    iget-object v1, p0, Lcom/osp/app/signin/SignUpView;->b:Landroid/view/View$OnClickListener;

    invoke-virtual {v0, v1}, Lcom/osp/app/signin/BottomSoftkeyLinearLayout;->b(Landroid/view/View$OnClickListener;)V

    .line 1753
    :goto_4
    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object v0

    .line 1754
    invoke-virtual {v0, v3}, Ljava/util/Calendar;->get(I)I

    move-result v1

    iput v1, p0, Lcom/osp/app/signin/SignUpView;->az:I

    .line 1755
    const/4 v1, 0x2

    invoke-virtual {v0, v1}, Ljava/util/Calendar;->get(I)I

    move-result v1

    iput v1, p0, Lcom/osp/app/signin/SignUpView;->ay:I

    .line 1756
    const/4 v1, 0x5

    invoke-virtual {v0, v1}, Ljava/util/Calendar;->get(I)I

    move-result v0

    iput v0, p0, Lcom/osp/app/signin/SignUpView;->aA:I

    .line 1758
    iget-object v0, p0, Lcom/osp/app/signin/SignUpView;->A:Landroid/widget/TextView;

    new-instance v1, Ljava/util/Date;

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v6

    invoke-direct {v1, v6, v7}, Ljava/util/Date;-><init>(J)V

    invoke-direct {p0, v1}, Lcom/osp/app/signin/SignUpView;->a(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 1759
    iget-object v0, p0, Lcom/osp/app/signin/SignUpView;->A:Landroid/widget/TextView;

    new-instance v1, Ljava/util/Date;

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v6

    invoke-direct {v1, v6, v7}, Ljava/util/Date;-><init>(J)V

    invoke-direct {p0, v1}, Lcom/osp/app/signin/SignUpView;->a(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 1764
    iget-object v0, p0, Lcom/osp/app/signin/SignUpView;->A:Landroid/widget/TextView;

    new-instance v1, Lcom/osp/app/signin/kg;

    invoke-direct {v1, p0}, Lcom/osp/app/signin/kg;-><init>(Lcom/osp/app/signin/SignUpView;)V

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->addTextChangedListener(Landroid/text/TextWatcher;)V

    .line 1781
    iget-object v0, p0, Lcom/osp/app/signin/SignUpView;->y:Landroid/widget/LinearLayout;

    new-instance v1, Lcom/osp/app/signin/kh;

    invoke-direct {v1, p0}, Lcom/osp/app/signin/kh;-><init>(Lcom/osp/app/signin/SignUpView;)V

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1795
    iget-object v0, p0, Lcom/osp/app/signin/SignUpView;->D:Landroid/widget/EditText;

    new-instance v1, Lcom/osp/app/signin/ki;

    invoke-direct {v1, p0}, Lcom/osp/app/signin/ki;-><init>(Lcom/osp/app/signin/SignUpView;)V

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->addTextChangedListener(Landroid/text/TextWatcher;)V

    .line 1870
    iget-object v0, p0, Lcom/osp/app/signin/SignUpView;->F:Landroid/widget/AutoCompleteTextView;

    new-instance v1, Lcom/osp/app/signin/kj;

    invoke-direct {v1, p0}, Lcom/osp/app/signin/kj;-><init>(Lcom/osp/app/signin/SignUpView;)V

    invoke-virtual {v0, v1}, Landroid/widget/AutoCompleteTextView;->addTextChangedListener(Landroid/text/TextWatcher;)V

    .line 1911
    iget-object v0, p0, Lcom/osp/app/signin/SignUpView;->E:Landroid/widget/EditText;

    new-instance v1, Lcom/osp/app/signin/kl;

    invoke-direct {v1, p0}, Lcom/osp/app/signin/kl;-><init>(Lcom/osp/app/signin/SignUpView;)V

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->addTextChangedListener(Landroid/text/TextWatcher;)V

    .line 1984
    const v0, 0x7f0c00aa

    invoke-virtual {p0, v0}, Lcom/osp/app/signin/SignUpView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/EditText;

    iput-object v0, p0, Lcom/osp/app/signin/SignUpView;->I:Landroid/widget/EditText;

    .line 1985
    iget-object v0, p0, Lcom/osp/app/signin/SignUpView;->I:Landroid/widget/EditText;

    new-instance v1, Lcom/osp/app/signin/km;

    invoke-direct {v1, p0}, Lcom/osp/app/signin/km;-><init>(Lcom/osp/app/signin/SignUpView;)V

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->addTextChangedListener(Landroid/text/TextWatcher;)V

    .line 2001
    const v0, 0x7f0c00a7

    invoke-virtual {p0, v0}, Lcom/osp/app/signin/SignUpView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/EditText;

    iput-object v0, p0, Lcom/osp/app/signin/SignUpView;->J:Landroid/widget/EditText;

    .line 2002
    iget-object v0, p0, Lcom/osp/app/signin/SignUpView;->J:Landroid/widget/EditText;

    new-instance v1, Lcom/osp/app/signin/kn;

    invoke-direct {v1, p0}, Lcom/osp/app/signin/kn;-><init>(Lcom/osp/app/signin/SignUpView;)V

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->addTextChangedListener(Landroid/text/TextWatcher;)V

    .line 2018
    invoke-direct {p0}, Lcom/osp/app/signin/SignUpView;->g()V

    .line 2019
    invoke-static {}, Lcom/osp/device/b;->b()Lcom/osp/device/b;

    move-result-object v0

    invoke-virtual {v0}, Lcom/osp/device/b;->a()Z

    move-result v0

    if-eqz v0, :cond_e

    const v0, 0x7f0c014b

    invoke-virtual {p0, v0}, Lcom/osp/app/signin/SignUpView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    new-instance v1, Lcom/osp/app/signin/kw;

    invoke-direct {v1, p0}, Lcom/osp/app/signin/kw;-><init>(Lcom/osp/app/signin/SignUpView;)V

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    new-instance v1, Lcom/osp/app/signin/kx;

    invoke-direct {v1, p0, v0}, Lcom/osp/app/signin/kx;-><init>(Lcom/osp/app/signin/SignUpView;Landroid/widget/Button;)V

    invoke-virtual {v0, v1}, Landroid/widget/Button;->addTextChangedListener(Landroid/text/TextWatcher;)V

    const v0, 0x7f0c0150

    invoke-virtual {p0, v0}, Lcom/osp/app/signin/SignUpView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/EditText;

    iput-object v0, p0, Lcom/osp/app/signin/SignUpView;->T:Landroid/widget/EditText;

    iget-object v0, p0, Lcom/osp/app/signin/SignUpView;->T:Landroid/widget/EditText;

    new-instance v1, Lcom/osp/app/signin/ky;

    invoke-direct {v1, p0}, Lcom/osp/app/signin/ky;-><init>(Lcom/osp/app/signin/SignUpView;)V

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->addTextChangedListener(Landroid/text/TextWatcher;)V

    iget-object v0, p0, Lcom/osp/app/signin/SignUpView;->bq:Lcom/msc/a/j;

    if-eqz v0, :cond_d

    iget-object v0, p0, Lcom/osp/app/signin/SignUpView;->bq:Lcom/msc/a/j;

    invoke-virtual {v0}, Lcom/msc/a/j;->b()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_17

    :cond_d
    iget-object v0, p0, Lcom/osp/app/signin/SignUpView;->T:Landroid/widget/EditText;

    invoke-virtual {v0, v4}, Landroid/widget/EditText;->setEnabled(Z)V

    iget-object v0, p0, Lcom/osp/app/signin/SignUpView;->T:Landroid/widget/EditText;

    const-string v1, ""

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    :goto_5
    const v0, 0x7f0c014c

    invoke-virtual {p0, v0}, Lcom/osp/app/signin/SignUpView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    new-instance v1, Lcom/osp/app/signin/kz;

    invoke-direct {v1, p0}, Lcom/osp/app/signin/kz;-><init>(Lcom/osp/app/signin/SignUpView;)V

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    :cond_e
    invoke-static {}, Lcom/osp/device/b;->b()Lcom/osp/device/b;

    move-result-object v0

    invoke-virtual {v0}, Lcom/osp/device/b;->a()Z

    move-result v0

    if-eqz v0, :cond_11

    iget-boolean v0, p0, Lcom/osp/app/signin/SignUpView;->aT:Z

    if-nez v0, :cond_11

    const v0, 0x7f0c0086

    invoke-virtual {p0, v0}, Lcom/osp/app/signin/SignUpView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    const v1, 0x7f0c0088

    invoke-virtual {p0, v1}, Lcom/osp/app/signin/SignUpView;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/Button;

    if-nez v0, :cond_f

    if-eqz v1, :cond_11

    :cond_f
    iget-object v2, p0, Lcom/osp/app/signin/SignUpView;->aj:Ljava/lang/String;

    invoke-static {}, Lcom/osp/device/b;->b()Lcom/osp/device/b;

    move-result-object v5

    invoke-virtual {v5}, Lcom/osp/device/b;->n()Ljava/lang/String;

    move-result-object v5

    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v6

    if-nez v6, :cond_18

    invoke-static {v5}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v6

    if-nez v6, :cond_18

    const-string v6, "404"

    invoke-virtual {v6, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_18

    const-string v2, "IN"

    invoke-virtual {v2, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_18

    move v2, v3

    :goto_6
    if-nez v2, :cond_10

    iget-object v2, p0, Lcom/osp/app/signin/SignUpView;->al:Ljava/lang/String;

    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_19

    :cond_10
    invoke-virtual {v1, v3}, Landroid/widget/Button;->setSelected(Z)V

    invoke-virtual {v0, v4}, Landroid/widget/Button;->setSelected(Z)V

    const/16 v2, 0x12d

    invoke-direct {p0, v2}, Lcom/osp/app/signin/SignUpView;->a(I)V

    :goto_7
    new-instance v2, Lcom/osp/app/signin/la;

    invoke-direct {v2, p0, v0, v1}, Lcom/osp/app/signin/la;-><init>(Lcom/osp/app/signin/SignUpView;Landroid/widget/Button;Landroid/widget/Button;)V

    invoke-virtual {v0, v2}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    new-instance v2, Lcom/osp/app/signin/lb;

    invoke-direct {v2, p0, v0, v1}, Lcom/osp/app/signin/lb;-><init>(Lcom/osp/app/signin/SignUpView;Landroid/widget/Button;Landroid/widget/Button;)V

    invoke-virtual {v1, v2}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    invoke-direct {p0}, Lcom/osp/app/signin/SignUpView;->q()V

    .line 2021
    :cond_11
    const v0, 0x7f0c0025

    invoke-virtual {p0, v0}, Lcom/osp/app/signin/SignUpView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/CheckBox;

    iput-object v0, p0, Lcom/osp/app/signin/SignUpView;->B:Landroid/widget/CheckBox;

    .line 2028
    iget-object v0, p0, Lcom/osp/app/signin/SignUpView;->B:Landroid/widget/CheckBox;

    iget-object v1, p0, Lcom/osp/app/signin/SignUpView;->bz:Landroid/view/View$OnKeyListener;

    invoke-virtual {v0, v1}, Landroid/widget/CheckBox;->setOnKeyListener(Landroid/view/View$OnKeyListener;)V

    .line 2030
    iget-object v0, p0, Lcom/osp/app/signin/SignUpView;->aj:Ljava/lang/String;

    if-eqz v0, :cond_12

    invoke-static {p0}, Lcom/osp/app/util/r;->f(Landroid/content/Context;)Z

    move-result v0

    if-nez v0, :cond_12

    .line 2032
    iget-object v0, p0, Lcom/osp/app/signin/SignUpView;->B:Landroid/widget/CheckBox;

    invoke-virtual {v0, v3}, Landroid/widget/CheckBox;->setChecked(Z)V

    .line 2034
    :cond_12
    iget-object v0, p0, Lcom/osp/app/signin/SignUpView;->B:Landroid/widget/CheckBox;

    invoke-static {v0}, Lcom/osp/app/signin/SignUpView;->a(Landroid/widget/TextView;)V

    .line 2035
    iget-object v0, p0, Lcom/osp/app/signin/SignUpView;->B:Landroid/widget/CheckBox;

    new-instance v1, Lcom/osp/app/signin/ko;

    invoke-direct {v1, p0}, Lcom/osp/app/signin/ko;-><init>(Lcom/osp/app/signin/SignUpView;)V

    invoke-virtual {v0, v1}, Landroid/widget/CheckBox;->setOnCheckedChangeListener(Landroid/widget/CompoundButton$OnCheckedChangeListener;)V

    .line 2047
    invoke-direct {p0}, Lcom/osp/app/signin/SignUpView;->e()V

    .line 2048
    invoke-direct {p0}, Lcom/osp/app/signin/SignUpView;->f()V

    .line 2054
    invoke-virtual {p0}, Lcom/osp/app/signin/SignUpView;->getWindow()Landroid/view/Window;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/Window;->getDecorView()Landroid/view/View;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/osp/app/signin/SignUpView;->setInputMethodEmoticonDisabled(Landroid/view/View;)V

    .line 2055
    return-void

    .line 1507
    :cond_13
    invoke-virtual {v0, v4}, Landroid/view/View;->setVisibility(I)V

    goto/16 :goto_1

    .line 1660
    :cond_14
    invoke-static {}, Lcom/osp/common/util/i;->a()Lcom/osp/common/util/i;

    iget-object v2, p0, Lcom/osp/app/signin/SignUpView;->bc:Ljava/lang/String;

    invoke-static {v2}, Lcom/osp/common/util/i;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto/16 :goto_2

    .line 1668
    :cond_15
    const v0, 0x7f070007

    invoke-virtual {v5, v0}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setTextColor(I)V

    goto/16 :goto_3

    .line 1749
    :cond_16
    iget-object v0, p0, Lcom/osp/app/signin/SignUpView;->bx:Lcom/osp/app/signin/BottomSoftkeyLinearLayout;

    iget-object v1, p0, Lcom/osp/app/signin/SignUpView;->a:Landroid/view/View$OnClickListener;

    invoke-virtual {v0, v1}, Lcom/osp/app/signin/BottomSoftkeyLinearLayout;->b(Landroid/view/View$OnClickListener;)V

    .line 1750
    iget-object v0, p0, Lcom/osp/app/signin/SignUpView;->bx:Lcom/osp/app/signin/BottomSoftkeyLinearLayout;

    iget-object v1, p0, Lcom/osp/app/signin/SignUpView;->b:Landroid/view/View$OnClickListener;

    invoke-virtual {v0, v1}, Lcom/osp/app/signin/BottomSoftkeyLinearLayout;->a(Landroid/view/View$OnClickListener;)V

    goto/16 :goto_4

    .line 2019
    :cond_17
    iget-object v0, p0, Lcom/osp/app/signin/SignUpView;->T:Landroid/widget/EditText;

    invoke-virtual {v0, v3}, Landroid/widget/EditText;->setEnabled(Z)V

    goto/16 :goto_5

    :cond_18
    move v2, v4

    goto/16 :goto_6

    :cond_19
    invoke-virtual {v0, v3}, Landroid/widget/Button;->setSelected(Z)V

    invoke-virtual {v1, v4}, Landroid/widget/Button;->setSelected(Z)V

    goto/16 :goto_7

    :cond_1a
    move v0, v4

    goto/16 :goto_0
.end method

.method static synthetic d(Lcom/osp/app/signin/SignUpView;)Z
    .locals 1

    .prologue
    .line 112
    iget-boolean v0, p0, Lcom/osp/app/signin/SignUpView;->U:Z

    return v0
.end method

.method static synthetic e(Lcom/osp/app/signin/SignUpView;I)I
    .locals 0

    .prologue
    .line 112
    iput p1, p0, Lcom/osp/app/signin/SignUpView;->av:I

    return p1
.end method

.method static synthetic e(Lcom/osp/app/signin/SignUpView;Landroid/app/AlertDialog;)Landroid/app/AlertDialog;
    .locals 0

    .prologue
    .line 112
    iput-object p1, p0, Lcom/osp/app/signin/SignUpView;->aa:Landroid/app/AlertDialog;

    return-object p1
.end method

.method static synthetic e(Lcom/osp/app/signin/SignUpView;)Landroid/os/Bundle;
    .locals 1

    .prologue
    .line 112
    iget-object v0, p0, Lcom/osp/app/signin/SignUpView;->bb:Landroid/os/Bundle;

    return-object v0
.end method

.method static synthetic e(Lcom/osp/app/signin/SignUpView;Ljava/lang/String;)Ljava/lang/String;
    .locals 0

    .prologue
    .line 112
    iput-object p1, p0, Lcom/osp/app/signin/SignUpView;->bh:Ljava/lang/String;

    return-object p1
.end method

.method private e()V
    .locals 9

    .prologue
    const/4 v5, 0x0

    const/4 v8, 0x1

    const/4 v7, 0x0

    .line 2139
    invoke-static {}, Lcom/osp/app/util/r;->r()Z

    move-result v0

    if-nez v0, :cond_0

    invoke-static {}, Lcom/osp/app/util/r;->q()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 2141
    :cond_0
    const v0, 0x7f0c013b

    invoke-virtual {p0, v0}, Lcom/osp/app/signin/SignUpView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    .line 2142
    const v1, 0x7f0c0139

    invoke-virtual {p0, v1}, Lcom/osp/app/signin/SignUpView;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/LinearLayout;

    .line 2144
    if-eqz v0, :cond_2

    if-eqz v1, :cond_2

    .line 2146
    new-instance v1, Lcom/osp/app/signin/ks;

    invoke-direct {v1, p0}, Lcom/osp/app/signin/ks;-><init>(Lcom/osp/app/signin/SignUpView;)V

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2190
    iget-boolean v1, p0, Lcom/osp/app/signin/SignUpView;->bl:Z

    if-nez v1, :cond_1

    iget-object v1, p0, Lcom/osp/app/signin/SignUpView;->bm:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    if-nez v1, :cond_7

    invoke-direct {p0}, Lcom/osp/app/signin/SignUpView;->z()Z

    move-result v1

    if-nez v1, :cond_7

    .line 2203
    :cond_1
    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 2221
    :cond_2
    :goto_0
    invoke-static {}, Lcom/osp/app/util/r;->q()Z

    move-result v0

    if-eqz v0, :cond_6

    .line 2223
    invoke-direct {p0}, Lcom/osp/app/signin/SignUpView;->z()Z

    move-result v0

    if-eqz v0, :cond_6

    iget-boolean v0, p0, Lcom/osp/app/signin/SignUpView;->aT:Z

    if-eqz v0, :cond_6

    .line 2225
    invoke-direct {p0}, Lcom/osp/app/signin/SignUpView;->A()Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/osp/app/signin/SignUpView;->F:Landroid/widget/AutoCompleteTextView;

    if-eqz v1, :cond_3

    if-eqz v0, :cond_3

    iget-object v1, p0, Lcom/osp/app/signin/SignUpView;->F:Landroid/widget/AutoCompleteTextView;

    invoke-virtual {v1, v0}, Landroid/widget/AutoCompleteTextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v0, p0, Lcom/osp/app/signin/SignUpView;->F:Landroid/widget/AutoCompleteTextView;

    iget-object v1, p0, Lcom/osp/app/signin/SignUpView;->F:Landroid/widget/AutoCompleteTextView;

    invoke-virtual {v1}, Landroid/widget/AutoCompleteTextView;->length()I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/AutoCompleteTextView;->setSelection(I)V

    :cond_3
    invoke-virtual {p0}, Lcom/osp/app/signin/SignUpView;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    invoke-static {v7}, Lcom/msc/sa/c/d;->a(I)Landroid/net/Uri;

    move-result-object v1

    if-eqz v1, :cond_4

    new-array v2, v8, [Ljava/lang/String;

    const-string v3, "_id"

    aput-object v3, v2, v7

    const-string v3, "account_type = ?"

    new-array v4, v8, [Ljava/lang/String;

    const-string v6, "com.google"

    aput-object v6, v4, v7

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v2

    invoke-direct {p0, v2}, Lcom/osp/app/signin/SignUpView;->a(Landroid/database/Cursor;)[Ljava/lang/String;

    move-result-object v2

    if-nez v2, :cond_9

    new-array v2, v8, [Ljava/lang/String;

    const-string v3, "_id"

    aput-object v3, v2, v7

    const-string v3, "account_type != ?"

    new-array v4, v8, [Ljava/lang/String;

    const-string v6, "com.google"

    aput-object v6, v4, v7

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/osp/app/signin/SignUpView;->a(Landroid/database/Cursor;)[Ljava/lang/String;

    move-result-object v5

    :cond_4
    :goto_1
    if-eqz v5, :cond_6

    iget-object v0, p0, Lcom/osp/app/signin/SignUpView;->J:Landroid/widget/EditText;

    if-eqz v0, :cond_5

    iget-object v0, p0, Lcom/osp/app/signin/SignUpView;->J:Landroid/widget/EditText;

    aget-object v1, v5, v7

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    :cond_5
    iget-object v0, p0, Lcom/osp/app/signin/SignUpView;->I:Landroid/widget/EditText;

    if-eqz v0, :cond_6

    iget-object v0, p0, Lcom/osp/app/signin/SignUpView;->I:Landroid/widget/EditText;

    aget-object v1, v5, v8

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    .line 2229
    :cond_6
    return-void

    .line 2204
    :cond_7
    iget-object v1, p0, Lcom/osp/app/signin/SignUpView;->bm:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    if-gtz v1, :cond_8

    invoke-direct {p0}, Lcom/osp/app/signin/SignUpView;->z()Z

    move-result v1

    if-eqz v1, :cond_2

    .line 2215
    :cond_8
    invoke-virtual {v0, v7}, Landroid/widget/ImageView;->setVisibility(I)V

    goto/16 :goto_0

    :cond_9
    move-object v5, v2

    goto :goto_1
.end method

.method private e(I)V
    .locals 6

    .prologue
    const v4, 0x7f08011e

    const/4 v2, 0x0

    .line 3199
    invoke-static {}, Lcom/osp/device/b;->b()Lcom/osp/device/b;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/osp/device/b;->i(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 3201
    invoke-virtual {p0}, Lcom/osp/app/signin/SignUpView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f08011f

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v0

    float-to-int v0, v0

    .line 3202
    invoke-virtual {p0}, Lcom/osp/app/signin/SignUpView;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1, v4}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v1

    float-to-int v1, v1

    .line 3203
    invoke-static {}, Lcom/osp/app/signin/SamsungService;->a()Z

    move-result v3

    if-nez v3, :cond_0

    invoke-static {p0}, Lcom/osp/app/util/r;->g(Landroid/content/Context;)Z

    move-result v3

    if-nez v3, :cond_0

    .line 3205
    invoke-virtual {p0}, Lcom/osp/app/signin/SignUpView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0, v4}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v0

    float-to-int v0, v0

    .line 3208
    :cond_0
    const v3, 0x7f0c017d

    invoke-virtual {p0, v3}, Lcom/osp/app/signin/SignUpView;->findViewById(I)Landroid/view/View;

    move-result-object v3

    .line 3209
    invoke-static {}, Lcom/osp/app/util/q;->a()Lcom/osp/app/util/q;

    invoke-static {v3, v0, v1, p1}, Lcom/osp/app/util/q;->b(Landroid/view/View;III)V

    .line 3212
    invoke-static {p0}, Lcom/osp/app/util/r;->k(Landroid/content/Context;)Z

    move-result v3

    if-nez v3, :cond_8

    .line 3214
    invoke-virtual {p0}, Lcom/osp/app/signin/SignUpView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f08013d

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v0

    float-to-int v1, v0

    .line 3215
    invoke-virtual {p0}, Lcom/osp/app/signin/SignUpView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v3, 0x7f08013c

    invoke-virtual {v0, v3}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v0

    float-to-int v0, v0

    .line 3218
    :goto_0
    iget-object v3, p0, Lcom/osp/app/signin/SignUpView;->bx:Lcom/osp/app/signin/BottomSoftkeyLinearLayout;

    if-eqz v3, :cond_2

    .line 3220
    invoke-static {}, Lcom/osp/app/signin/SamsungService;->a()Z

    move-result v3

    if-eqz v3, :cond_1

    sget-object v3, Lcom/osp/app/util/u;->o:[Ljava/lang/String;

    invoke-static {v3}, Lcom/osp/app/util/u;->a([Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_1

    move v1, v2

    .line 3224
    :cond_1
    invoke-static {}, Lcom/osp/app/util/q;->a()Lcom/osp/app/util/q;

    iget-object v3, p0, Lcom/osp/app/signin/SignUpView;->bx:Lcom/osp/app/signin/BottomSoftkeyLinearLayout;

    invoke-static {p0, v3, v1, v0, p1}, Lcom/osp/app/util/q;->a(Landroid/content/Context;Landroid/view/View;III)V

    .line 3228
    :cond_2
    invoke-static {}, Lcom/osp/device/b;->b()Lcom/osp/device/b;

    move-result-object v0

    invoke-virtual {v0}, Lcom/osp/device/b;->a()Z

    move-result v0

    if-eqz v0, :cond_4

    .line 3230
    const v0, 0x7f0c0085

    invoke-virtual {p0, v0}, Lcom/osp/app/signin/SignUpView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    .line 3231
    if-eqz v0, :cond_4

    .line 3233
    invoke-static {}, Lcom/osp/app/signin/SamsungService;->a()Z

    move-result v1

    if-nez v1, :cond_5

    .line 3235
    invoke-virtual {p0}, Lcom/osp/app/signin/SignUpView;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1, v4}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v1

    float-to-int v3, v1

    .line 3236
    invoke-virtual {p0}, Lcom/osp/app/signin/SignUpView;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1, v4}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v1

    float-to-int v1, v1

    .line 3243
    :goto_1
    invoke-static {p0}, Lcom/osp/app/util/r;->g(Landroid/content/Context;)Z

    move-result v4

    if-eqz v4, :cond_6

    move v3, v2

    .line 3255
    :cond_3
    :goto_2
    invoke-static {}, Lcom/osp/app/util/q;->a()Lcom/osp/app/util/q;

    invoke-static {v0, v3, v2, p1}, Lcom/osp/app/util/q;->b(Landroid/view/View;III)V

    .line 3260
    :cond_4
    return-void

    .line 3239
    :cond_5
    invoke-virtual {p0}, Lcom/osp/app/signin/SignUpView;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v3, 0x7f080136

    invoke-virtual {v1, v3}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v1

    float-to-int v3, v1

    .line 3240
    invoke-virtual {p0}, Lcom/osp/app/signin/SignUpView;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v4, 0x7f080135

    invoke-virtual {v1, v4}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v1

    float-to-int v1, v1

    goto :goto_1

    .line 3246
    :cond_6
    invoke-static {p0}, Lcom/osp/app/util/r;->l(Landroid/content/Context;)Z

    move-result v4

    if-eqz v4, :cond_7

    .line 3248
    invoke-static {}, Lcom/osp/app/signin/SamsungService;->a()Z

    move-result v1

    if-nez v1, :cond_3

    move v3, v2

    .line 3250
    goto :goto_2

    :cond_7
    move v2, v1

    goto :goto_2

    :cond_8
    move v5, v1

    move v1, v0

    move v0, v5

    goto/16 :goto_0
.end method

.method static synthetic f(Lcom/osp/app/signin/SignUpView;I)I
    .locals 0

    .prologue
    .line 112
    iput p1, p0, Lcom/osp/app/signin/SignUpView;->aC:I

    return p1
.end method

.method static synthetic f(Lcom/osp/app/signin/SignUpView;Landroid/app/AlertDialog;)Landroid/app/AlertDialog;
    .locals 0

    .prologue
    .line 112
    iput-object p1, p0, Lcom/osp/app/signin/SignUpView;->ab:Landroid/app/AlertDialog;

    return-object p1
.end method

.method static synthetic f(Lcom/osp/app/signin/SignUpView;)Landroid/content/Intent;
    .locals 1

    .prologue
    .line 112
    iget-object v0, p0, Lcom/osp/app/signin/SignUpView;->ai:Landroid/content/Intent;

    return-object v0
.end method

.method static synthetic f(Lcom/osp/app/signin/SignUpView;Ljava/lang/String;)Ljava/lang/String;
    .locals 0

    .prologue
    .line 112
    iput-object p1, p0, Lcom/osp/app/signin/SignUpView;->aj:Ljava/lang/String;

    return-object p1
.end method

.method private f()V
    .locals 5

    .prologue
    const/4 v3, 0x0

    const/4 v4, -0x1

    .line 2235
    invoke-static {p0}, Lcom/osp/app/util/r;->p(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/osp/app/signin/SignUpView;->al:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 2237
    const v0, 0x7f0c013b

    invoke-virtual {p0, v0}, Lcom/osp/app/signin/SignUpView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    .line 2238
    const v1, 0x7f0c0139

    invoke-virtual {p0, v1}, Lcom/osp/app/signin/SignUpView;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/LinearLayout;

    .line 2240
    if-eqz v0, :cond_1

    if-eqz v1, :cond_1

    .line 2242
    new-instance v2, Lcom/osp/app/signin/ku;

    invoke-direct {v2, p0}, Lcom/osp/app/signin/ku;-><init>(Lcom/osp/app/signin/SignUpView;)V

    invoke-virtual {v0, v2}, Landroid/widget/ImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2286
    iget-boolean v2, p0, Lcom/osp/app/signin/SignUpView;->bl:Z

    if-eqz v2, :cond_0

    invoke-static {p0}, Lcom/osp/app/util/r;->p(Landroid/content/Context;)Z

    move-result v2

    if-nez v2, :cond_2

    iget-object v2, p0, Lcom/osp/app/signin/SignUpView;->al:Ljava/lang/String;

    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 2289
    :cond_0
    invoke-virtual {p0}, Lcom/osp/app/signin/SignUpView;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0800e7

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v2

    float-to-int v2, v2

    .line 2295
    invoke-static {v1, v4, v4, v2, v4}, Lcom/msc/sa/c/d;->a(Landroid/view/View;IIII)V

    .line 2297
    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 2312
    :cond_1
    :goto_0
    return-void

    .line 2298
    :cond_2
    iget-boolean v2, p0, Lcom/osp/app/signin/SignUpView;->bl:Z

    if-eqz v2, :cond_1

    invoke-static {p0}, Lcom/osp/app/util/r;->p(Landroid/content/Context;)Z

    move-result v2

    if-eqz v2, :cond_1

    iget-object v2, p0, Lcom/osp/app/signin/SignUpView;->al:Ljava/lang/String;

    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_1

    .line 2306
    invoke-static {v1, v4, v4, v3, v4}, Lcom/msc/sa/c/d;->a(Landroid/view/View;IIII)V

    .line 2308
    invoke-virtual {v0, v3}, Landroid/widget/ImageView;->setVisibility(I)V

    goto :goto_0
.end method

.method static synthetic g(Lcom/osp/app/signin/SignUpView;Landroid/app/AlertDialog;)Landroid/app/AlertDialog;
    .locals 0

    .prologue
    .line 112
    iput-object p1, p0, Lcom/osp/app/signin/SignUpView;->ac:Landroid/app/AlertDialog;

    return-object p1
.end method

.method static synthetic g(Lcom/osp/app/signin/SignUpView;)Landroid/widget/EditText;
    .locals 1

    .prologue
    .line 112
    iget-object v0, p0, Lcom/osp/app/signin/SignUpView;->N:Landroid/widget/EditText;

    return-object v0
.end method

.method static synthetic g(Lcom/osp/app/signin/SignUpView;Ljava/lang/String;)Ljava/lang/String;
    .locals 0

    .prologue
    .line 112
    iput-object p1, p0, Lcom/osp/app/signin/SignUpView;->ba:Ljava/lang/String;

    return-object p1
.end method

.method private g()V
    .locals 9

    .prologue
    const v8, 0x7f090017

    const/4 v5, -0x1

    .line 2348
    const v0, 0x7f0c001c

    invoke-virtual {p0, v0}, Lcom/osp/app/signin/SignUpView;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/LinearLayout;

    .line 2350
    iget-object v0, p0, Lcom/osp/app/signin/SignUpView;->bB:Lcom/osp/app/signin/kd;

    if-eqz v0, :cond_b

    .line 2352
    iget-object v0, p0, Lcom/osp/app/signin/SignUpView;->bB:Lcom/osp/app/signin/kd;

    invoke-virtual {v0}, Lcom/osp/app/signin/kd;->a()Ljava/util/ArrayList;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v7

    :cond_0
    :goto_0
    invoke-interface {v7}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_b

    invoke-interface {v7}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    move-object v6, v0

    check-cast v6, Ljava/lang/String;

    .line 2355
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v2, "SignUpView::initializeComponent cmpString : "

    invoke-direct {v0, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;)V

    .line 2357
    iget-object v0, p0, Lcom/osp/app/signin/SignUpView;->bB:Lcom/osp/app/signin/kd;

    iget-boolean v0, v0, Lcom/osp/app/signin/kd;->c:Z

    if-eqz v0, :cond_1

    const-string v0, "prefixName"

    invoke-virtual {v0, v6}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 2359
    const v0, 0x7f09003d

    invoke-virtual {p0, v0}, Lcom/osp/app/signin/SignUpView;->getString(I)Ljava/lang/String;

    move-result-object v2

    iget-object v0, p0, Lcom/osp/app/signin/SignUpView;->bB:Lcom/osp/app/signin/kd;

    invoke-virtual {v0}, Lcom/osp/app/signin/kd;->d()Z

    move-result v3

    const/16 v4, 0x74

    move-object v0, p0

    invoke-direct/range {v0 .. v5}, Lcom/osp/app/signin/SignUpView;->a(Landroid/widget/LinearLayout;Ljava/lang/String;ZII)Landroid/widget/EditText;

    move-result-object v0

    iput-object v0, p0, Lcom/osp/app/signin/SignUpView;->N:Landroid/widget/EditText;

    .line 2379
    :cond_1
    iget-object v0, p0, Lcom/osp/app/signin/SignUpView;->bB:Lcom/osp/app/signin/kd;

    iget-boolean v0, v0, Lcom/osp/app/signin/kd;->f:Z

    if-eqz v0, :cond_2

    const-string v0, "localityText"

    invoke-virtual {v0, v6}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 2381
    const v0, 0x7f09001a

    invoke-virtual {p0, v0}, Lcom/osp/app/signin/SignUpView;->getString(I)Ljava/lang/String;

    move-result-object v2

    iget-object v0, p0, Lcom/osp/app/signin/SignUpView;->bB:Lcom/osp/app/signin/kd;

    invoke-virtual {v0}, Lcom/osp/app/signin/kd;->i()Z

    move-result v3

    const/16 v4, 0x77

    move-object v0, p0

    invoke-direct/range {v0 .. v5}, Lcom/osp/app/signin/SignUpView;->a(Landroid/widget/LinearLayout;Ljava/lang/String;ZII)Landroid/widget/EditText;

    move-result-object v0

    iput-object v0, p0, Lcom/osp/app/signin/SignUpView;->K:Landroid/widget/EditText;

    .line 2384
    :cond_2
    iget-object v0, p0, Lcom/osp/app/signin/SignUpView;->bB:Lcom/osp/app/signin/kd;

    iget-boolean v0, v0, Lcom/osp/app/signin/kd;->g:Z

    if-eqz v0, :cond_3

    const-string v0, "postalCodeText"

    invoke-virtual {v0, v6}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 2386
    const v0, 0x7f090049

    invoke-virtual {p0, v0}, Lcom/osp/app/signin/SignUpView;->getString(I)Ljava/lang/String;

    move-result-object v2

    iget-object v0, p0, Lcom/osp/app/signin/SignUpView;->bB:Lcom/osp/app/signin/kd;

    invoke-virtual {v0}, Lcom/osp/app/signin/kd;->n()Z

    move-result v3

    const/16 v4, 0x78

    move-object v0, p0

    invoke-direct/range {v0 .. v5}, Lcom/osp/app/signin/SignUpView;->a(Landroid/widget/LinearLayout;Ljava/lang/String;ZII)Landroid/widget/EditText;

    move-result-object v0

    iput-object v0, p0, Lcom/osp/app/signin/SignUpView;->L:Landroid/widget/EditText;

    .line 2389
    :cond_3
    iget-object v0, p0, Lcom/osp/app/signin/SignUpView;->bB:Lcom/osp/app/signin/kd;

    iget-boolean v0, v0, Lcom/osp/app/signin/kd;->h:Z

    if-eqz v0, :cond_4

    const-string v0, "receiveSMSPhoneNumberText"

    invoke-virtual {v0, v6}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 2391
    const v0, 0x7f090048

    invoke-virtual {p0, v0}, Lcom/osp/app/signin/SignUpView;->getString(I)Ljava/lang/String;

    move-result-object v2

    iget-object v0, p0, Lcom/osp/app/signin/SignUpView;->bB:Lcom/osp/app/signin/kd;

    invoke-virtual {v0}, Lcom/osp/app/signin/kd;->o()Z

    move-result v3

    const/16 v4, 0x79

    move-object v0, p0

    invoke-direct/range {v0 .. v5}, Lcom/osp/app/signin/SignUpView;->a(Landroid/widget/LinearLayout;Ljava/lang/String;ZII)Landroid/widget/EditText;

    move-result-object v0

    iput-object v0, p0, Lcom/osp/app/signin/SignUpView;->O:Landroid/widget/EditText;

    .line 2393
    iget-object v0, p0, Lcom/osp/app/signin/SignUpView;->O:Landroid/widget/EditText;

    const/4 v2, 0x2

    invoke-virtual {v0, v2}, Landroid/widget/EditText;->setInputType(I)V

    .line 2395
    :cond_4
    iget-object v0, p0, Lcom/osp/app/signin/SignUpView;->bB:Lcom/osp/app/signin/kd;

    iget-boolean v0, v0, Lcom/osp/app/signin/kd;->j:Z

    if-eqz v0, :cond_5

    const-string v0, "streetText"

    invoke-virtual {v0, v6}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_5

    .line 2397
    invoke-virtual {p0, v8}, Lcom/osp/app/signin/SignUpView;->getString(I)Ljava/lang/String;

    move-result-object v2

    iget-object v0, p0, Lcom/osp/app/signin/SignUpView;->bB:Lcom/osp/app/signin/kd;

    invoke-virtual {v0}, Lcom/osp/app/signin/kd;->j()Z

    move-result v3

    const/16 v4, 0x7a

    move-object v0, p0

    invoke-direct/range {v0 .. v5}, Lcom/osp/app/signin/SignUpView;->a(Landroid/widget/LinearLayout;Ljava/lang/String;ZII)Landroid/widget/EditText;

    move-result-object v0

    iput-object v0, p0, Lcom/osp/app/signin/SignUpView;->R:Landroid/widget/EditText;

    .line 2400
    :cond_5
    iget-object v0, p0, Lcom/osp/app/signin/SignUpView;->bB:Lcom/osp/app/signin/kd;

    iget-boolean v0, v0, Lcom/osp/app/signin/kd;->k:Z

    if-eqz v0, :cond_6

    const-string v0, "extendedText"

    invoke-virtual {v0, v6}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_6

    .line 2402
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0, v8}, Lcom/osp/app/signin/SignUpView;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, "2"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    iget-object v0, p0, Lcom/osp/app/signin/SignUpView;->bB:Lcom/osp/app/signin/kd;

    invoke-virtual {v0}, Lcom/osp/app/signin/kd;->k()Z

    move-result v3

    const/16 v4, 0x7b

    move-object v0, p0

    invoke-direct/range {v0 .. v5}, Lcom/osp/app/signin/SignUpView;->a(Landroid/widget/LinearLayout;Ljava/lang/String;ZII)Landroid/widget/EditText;

    move-result-object v0

    iput-object v0, p0, Lcom/osp/app/signin/SignUpView;->G:Landroid/widget/EditText;

    .line 2405
    :cond_6
    iget-object v0, p0, Lcom/osp/app/signin/SignUpView;->bB:Lcom/osp/app/signin/kd;

    iget-boolean v0, v0, Lcom/osp/app/signin/kd;->l:Z

    if-eqz v0, :cond_7

    const-string v0, "postOfficeBoxNumberText"

    invoke-virtual {v0, v6}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_7

    .line 2407
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0, v8}, Lcom/osp/app/signin/SignUpView;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, "3"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    iget-object v0, p0, Lcom/osp/app/signin/SignUpView;->bB:Lcom/osp/app/signin/kd;

    invoke-virtual {v0}, Lcom/osp/app/signin/kd;->l()Z

    move-result v3

    const/16 v4, 0x7c

    move-object v0, p0

    invoke-direct/range {v0 .. v5}, Lcom/osp/app/signin/SignUpView;->a(Landroid/widget/LinearLayout;Ljava/lang/String;ZII)Landroid/widget/EditText;

    move-result-object v0

    iput-object v0, p0, Lcom/osp/app/signin/SignUpView;->M:Landroid/widget/EditText;

    .line 2410
    :cond_7
    iget-object v0, p0, Lcom/osp/app/signin/SignUpView;->bB:Lcom/osp/app/signin/kd;

    iget-boolean v0, v0, Lcom/osp/app/signin/kd;->m:Z

    if-eqz v0, :cond_8

    const-string v0, "regionText"

    invoke-virtual {v0, v6}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_8

    .line 2412
    const v0, 0x7f09004b

    invoke-virtual {p0, v0}, Lcom/osp/app/signin/SignUpView;->getString(I)Ljava/lang/String;

    move-result-object v2

    iget-object v0, p0, Lcom/osp/app/signin/SignUpView;->bB:Lcom/osp/app/signin/kd;

    invoke-virtual {v0}, Lcom/osp/app/signin/kd;->m()Z

    move-result v3

    const/16 v4, 0x7d

    move-object v0, p0

    invoke-direct/range {v0 .. v5}, Lcom/osp/app/signin/SignUpView;->a(Landroid/widget/LinearLayout;Ljava/lang/String;ZII)Landroid/widget/EditText;

    move-result-object v0

    iput-object v0, p0, Lcom/osp/app/signin/SignUpView;->P:Landroid/widget/EditText;

    .line 2415
    :cond_8
    iget-object v0, p0, Lcom/osp/app/signin/SignUpView;->bB:Lcom/osp/app/signin/kd;

    iget-boolean v0, v0, Lcom/osp/app/signin/kd;->n:Z

    if-eqz v0, :cond_9

    const-string v0, "genderTypeCode"

    invoke-virtual {v0, v6}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_9

    .line 2417
    const v0, 0x7f09002b

    invoke-virtual {p0, v0}, Lcom/osp/app/signin/SignUpView;->getString(I)Ljava/lang/String;

    move-result-object v2

    iget-object v0, p0, Lcom/osp/app/signin/SignUpView;->bB:Lcom/osp/app/signin/kd;

    invoke-virtual {v0}, Lcom/osp/app/signin/kd;->p()Z

    move-result v3

    const/16 v4, 0x7e

    move-object v0, p0

    invoke-direct/range {v0 .. v5}, Lcom/osp/app/signin/SignUpView;->a(Landroid/widget/LinearLayout;Ljava/lang/String;ZII)Landroid/widget/EditText;

    move-result-object v0

    iput-object v0, p0, Lcom/osp/app/signin/SignUpView;->H:Landroid/widget/EditText;

    .line 2420
    :cond_9
    iget-object v0, p0, Lcom/osp/app/signin/SignUpView;->bB:Lcom/osp/app/signin/kd;

    iget-boolean v0, v0, Lcom/osp/app/signin/kd;->o:Z

    if-eqz v0, :cond_a

    const-string v0, "userDisplayName"

    invoke-virtual {v0, v6}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_a

    .line 2422
    const v0, 0x7f090040

    invoke-virtual {p0, v0}, Lcom/osp/app/signin/SignUpView;->getString(I)Ljava/lang/String;

    move-result-object v2

    iget-object v0, p0, Lcom/osp/app/signin/SignUpView;->bB:Lcom/osp/app/signin/kd;

    invoke-virtual {v0}, Lcom/osp/app/signin/kd;->q()Z

    move-result v3

    const/16 v4, 0x7f

    move-object v0, p0

    invoke-direct/range {v0 .. v5}, Lcom/osp/app/signin/SignUpView;->a(Landroid/widget/LinearLayout;Ljava/lang/String;ZII)Landroid/widget/EditText;

    move-result-object v0

    iput-object v0, p0, Lcom/osp/app/signin/SignUpView;->S:Landroid/widget/EditText;

    .line 2425
    :cond_a
    iget-object v0, p0, Lcom/osp/app/signin/SignUpView;->bB:Lcom/osp/app/signin/kd;

    iget-boolean v0, v0, Lcom/osp/app/signin/kd;->p:Z

    if-eqz v0, :cond_0

    const-string v0, "relationshipStatusCode"

    invoke-virtual {v0, v6}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2427
    const v0, 0x7f09003b

    invoke-virtual {p0, v0}, Lcom/osp/app/signin/SignUpView;->getString(I)Ljava/lang/String;

    move-result-object v2

    iget-object v0, p0, Lcom/osp/app/signin/SignUpView;->bB:Lcom/osp/app/signin/kd;

    invoke-virtual {v0}, Lcom/osp/app/signin/kd;->r()Z

    move-result v3

    const/16 v4, 0x80

    move-object v0, p0

    invoke-direct/range {v0 .. v5}, Lcom/osp/app/signin/SignUpView;->a(Landroid/widget/LinearLayout;Ljava/lang/String;ZII)Landroid/widget/EditText;

    move-result-object v0

    iput-object v0, p0, Lcom/osp/app/signin/SignUpView;->Q:Landroid/widget/EditText;

    goto/16 :goto_0

    .line 2432
    :cond_b
    return-void
.end method

.method static synthetic g(Lcom/osp/app/signin/SignUpView;I)V
    .locals 0

    .prologue
    .line 112
    invoke-direct {p0, p1}, Lcom/osp/app/signin/SignUpView;->a(I)V

    return-void
.end method

.method static synthetic h(Lcom/osp/app/signin/SignUpView;Landroid/app/AlertDialog;)Landroid/app/AlertDialog;
    .locals 0

    .prologue
    .line 112
    iput-object p1, p0, Lcom/osp/app/signin/SignUpView;->ad:Landroid/app/AlertDialog;

    return-object p1
.end method

.method static synthetic h(Lcom/osp/app/signin/SignUpView;)Landroid/widget/EditText;
    .locals 1

    .prologue
    .line 112
    iget-object v0, p0, Lcom/osp/app/signin/SignUpView;->H:Landroid/widget/EditText;

    return-object v0
.end method

.method static synthetic i(Lcom/osp/app/signin/SignUpView;)Landroid/widget/EditText;
    .locals 1

    .prologue
    .line 112
    iget-object v0, p0, Lcom/osp/app/signin/SignUpView;->Q:Landroid/widget/EditText;

    return-object v0
.end method

.method private i(Ljava/lang/String;)Z
    .locals 2

    .prologue
    .line 8936
    iget-object v0, p0, Lcom/osp/app/signin/SignUpView;->bm:Ljava/util/ArrayList;

    if-eqz v0, :cond_1

    if-eqz p1, :cond_1

    .line 8938
    iget-object v0, p0, Lcom/osp/app/signin/SignUpView;->bm:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/accounts/Account;

    .line 8940
    iget-object v0, v0, Landroid/accounts/Account;->name:Ljava/lang/String;

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 8942
    const/4 v0, 0x1

    .line 8946
    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method static synthetic j(Lcom/osp/app/signin/SignUpView;)Landroid/app/AlertDialog;
    .locals 1

    .prologue
    .line 112
    iget-object v0, p0, Lcom/osp/app/signin/SignUpView;->Y:Landroid/app/AlertDialog;

    return-object v0
.end method

.method static synthetic k(Lcom/osp/app/signin/SignUpView;)Landroid/app/AlertDialog;
    .locals 1

    .prologue
    .line 112
    iget-object v0, p0, Lcom/osp/app/signin/SignUpView;->ae:Landroid/app/AlertDialog;

    return-object v0
.end method

.method static synthetic l(Lcom/osp/app/signin/SignUpView;)Landroid/widget/CheckBox;
    .locals 1

    .prologue
    .line 112
    iget-object v0, p0, Lcom/osp/app/signin/SignUpView;->C:Landroid/widget/CheckBox;

    return-object v0
.end method

.method static synthetic m(Lcom/osp/app/signin/SignUpView;)Landroid/widget/EditText;
    .locals 1

    .prologue
    .line 112
    iget-object v0, p0, Lcom/osp/app/signin/SignUpView;->E:Landroid/widget/EditText;

    return-object v0
.end method

.method static synthetic n(Lcom/osp/app/signin/SignUpView;)Landroid/widget/EditText;
    .locals 1

    .prologue
    .line 112
    iget-object v0, p0, Lcom/osp/app/signin/SignUpView;->D:Landroid/widget/EditText;

    return-object v0
.end method

.method static synthetic o(Lcom/osp/app/signin/SignUpView;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 112
    iget-object v0, p0, Lcom/osp/app/signin/SignUpView;->aO:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic p(Lcom/osp/app/signin/SignUpView;)Landroid/widget/AutoCompleteTextView;
    .locals 1

    .prologue
    .line 112
    iget-object v0, p0, Lcom/osp/app/signin/SignUpView;->F:Landroid/widget/AutoCompleteTextView;

    return-object v0
.end method

.method static synthetic q(Lcom/osp/app/signin/SignUpView;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 112
    iget-object v0, p0, Lcom/osp/app/signin/SignUpView;->aP:Ljava/lang/String;

    return-object v0
.end method

.method private q()V
    .locals 9

    .prologue
    const/high16 v8, 0x3f800000    # 1.0f

    const/4 v4, 0x0

    const/4 v7, 0x0

    const/4 v6, 0x0

    .line 2604
    invoke-static {}, Lcom/osp/device/b;->b()Lcom/osp/device/b;

    move-result-object v0

    invoke-virtual {v0}, Lcom/osp/device/b;->a()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-static {p0}, Lcom/osp/app/util/r;->m(Landroid/content/Context;)Z

    move-result v0

    if-nez v0, :cond_0

    invoke-static {p0}, Lcom/osp/app/util/r;->l(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_3

    :cond_0
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0xe

    if-lt v0, v1, :cond_3

    .line 2607
    const v0, 0x7f0c0086

    invoke-virtual {p0, v0}, Lcom/osp/app/signin/SignUpView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    .line 2608
    const v1, 0x7f0c0088

    invoke-virtual {p0, v1}, Lcom/osp/app/signin/SignUpView;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/Button;

    .line 2610
    sget v2, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v3, 0x15

    if-lt v2, v3, :cond_4

    .line 2612
    invoke-virtual {v0}, Landroid/widget/Button;->isSelected()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 2614
    invoke-virtual {v0, v4, v6}, Landroid/widget/Button;->setTypeface(Landroid/graphics/Typeface;I)V

    .line 2615
    const-string v2, "sec-roboto-light"

    invoke-static {v2, v6}, Landroid/graphics/Typeface;->create(Ljava/lang/String;I)Landroid/graphics/Typeface;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/Button;->setTypeface(Landroid/graphics/Typeface;)V

    .line 2617
    :cond_1
    invoke-virtual {v1}, Landroid/widget/Button;->isSelected()Z

    move-result v2

    if-eqz v2, :cond_2

    .line 2619
    invoke-virtual {v1, v4, v6}, Landroid/widget/Button;->setTypeface(Landroid/graphics/Typeface;I)V

    .line 2620
    const-string v2, "sec-roboto-light"

    invoke-static {v2, v6}, Landroid/graphics/Typeface;->create(Ljava/lang/String;I)Landroid/graphics/Typeface;

    move-result-object v2

    invoke-virtual {v0, v2}, Landroid/widget/Button;->setTypeface(Landroid/graphics/Typeface;)V

    .line 2623
    :cond_2
    invoke-virtual {v0, v7, v7, v7, v6}, Landroid/widget/Button;->setShadowLayer(FFFI)V

    .line 2624
    invoke-virtual {v1, v7, v7, v7, v6}, Landroid/widget/Button;->setShadowLayer(FFFI)V

    .line 2667
    :cond_3
    :goto_0
    return-void

    .line 2628
    :cond_4
    invoke-static {}, Lcom/osp/app/signin/SamsungService;->a()Z

    move-result v2

    if-nez v2, :cond_8

    .line 2630
    const/16 v2, 0xbf

    .line 2631
    invoke-virtual {p0}, Lcom/osp/app/signin/SignUpView;->h()Z

    move-result v3

    if-nez v3, :cond_5

    .line 2633
    const/16 v2, 0x80

    .line 2636
    :cond_5
    invoke-static {p0}, Lcom/osp/app/util/x;->q(Landroid/content/Context;)Z

    move-result v3

    if-eqz v3, :cond_6

    .line 2639
    const/4 v3, 0x1

    .line 2647
    :goto_1
    int-to-float v4, v3

    invoke-static {v2, v6, v6, v6}, Landroid/graphics/Color;->argb(IIII)I

    move-result v5

    invoke-virtual {v0, v8, v7, v4, v5}, Landroid/widget/Button;->setShadowLayer(FFFI)V

    .line 2648
    int-to-float v0, v3

    invoke-static {v2, v6, v6, v6}, Landroid/graphics/Color;->argb(IIII)I

    move-result v2

    invoke-virtual {v1, v8, v7, v0, v2}, Landroid/widget/Button;->setShadowLayer(FFFI)V

    goto :goto_0

    .line 2640
    :cond_6
    invoke-static {p0}, Lcom/osp/app/util/x;->r(Landroid/content/Context;)Z

    move-result v3

    if-eqz v3, :cond_7

    .line 2642
    const/4 v3, 0x2

    goto :goto_1

    .line 2645
    :cond_7
    const/4 v3, 0x3

    goto :goto_1

    .line 2651
    :cond_8
    invoke-virtual {v0}, Landroid/widget/Button;->isSelected()Z

    move-result v2

    if-eqz v2, :cond_a

    .line 2653
    invoke-virtual {v0, v4, v6}, Landroid/widget/Button;->setTypeface(Landroid/graphics/Typeface;I)V

    .line 2654
    const-string v2, "sans-serif-light"

    invoke-static {v2, v6}, Landroid/graphics/Typeface;->create(Ljava/lang/String;I)Landroid/graphics/Typeface;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/Button;->setTypeface(Landroid/graphics/Typeface;)V

    .line 2661
    :cond_9
    :goto_2
    invoke-virtual {v0, v7, v7, v7, v6}, Landroid/widget/Button;->setShadowLayer(FFFI)V

    .line 2662
    invoke-virtual {v1, v7, v7, v7, v6}, Landroid/widget/Button;->setShadowLayer(FFFI)V

    goto :goto_0

    .line 2655
    :cond_a
    invoke-virtual {v1}, Landroid/widget/Button;->isSelected()Z

    move-result v2

    if-eqz v2, :cond_9

    .line 2657
    invoke-virtual {v1, v4, v6}, Landroid/widget/Button;->setTypeface(Landroid/graphics/Typeface;I)V

    .line 2658
    const-string v2, "sans-serif-light"

    invoke-static {v2, v6}, Landroid/graphics/Typeface;->create(Ljava/lang/String;I)Landroid/graphics/Typeface;

    move-result-object v2

    invoke-virtual {v0, v2}, Landroid/widget/Button;->setTypeface(Landroid/graphics/Typeface;)V

    goto :goto_2
.end method

.method static synthetic r(Lcom/osp/app/signin/SignUpView;)Landroid/widget/CheckBox;
    .locals 1

    .prologue
    .line 112
    iget-object v0, p0, Lcom/osp/app/signin/SignUpView;->B:Landroid/widget/CheckBox;

    return-object v0
.end method

.method private r()V
    .locals 8

    .prologue
    .line 2673
    invoke-static {}, Lcom/osp/device/b;->b()Lcom/osp/device/b;

    move-result-object v0

    invoke-virtual {v0}, Lcom/osp/device/b;->a()Z

    move-result v0

    if-eqz v0, :cond_17

    .line 2675
    iget-object v0, p0, Lcom/osp/app/signin/SignUpView;->be:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    .line 2676
    const v0, 0x7f0c001c

    invoke-virtual {p0, v0}, Lcom/osp/app/signin/SignUpView;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/LinearLayout;

    .line 2677
    invoke-virtual {v1}, Landroid/widget/LinearLayout;->getChildCount()I

    move-result v2

    .line 2678
    const/4 v0, 0x0

    :goto_0
    if-ge v0, v2, :cond_0

    .line 2680
    invoke-virtual {v1, v0}, Landroid/widget/LinearLayout;->getChildAt(I)Landroid/view/View;

    move-result-object v3

    const/16 v4, 0x8

    invoke-virtual {v3, v4}, Landroid/view/View;->setVisibility(I)V

    .line 2678
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 2682
    :cond_0
    const/4 v5, 0x0

    .line 2683
    iget-object v0, p0, Lcom/osp/app/signin/SignUpView;->bB:Lcom/osp/app/signin/kd;

    invoke-virtual {v0}, Lcom/osp/app/signin/kd;->a()Ljava/util/ArrayList;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v7

    :goto_1
    invoke-interface {v7}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_17

    invoke-interface {v7}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    move-object v6, v0

    check-cast v6, Ljava/lang/String;

    .line 2685
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v2, "SignUpView::initializeComponent cmpString : "

    invoke-direct {v0, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;)V

    .line 2687
    iget-object v0, p0, Lcom/osp/app/signin/SignUpView;->bB:Lcom/osp/app/signin/kd;

    iget-boolean v0, v0, Lcom/osp/app/signin/kd;->c:Z

    if-eqz v0, :cond_1

    const-string v0, "prefixName"

    invoke-virtual {v0, v6}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 2689
    iget-object v0, p0, Lcom/osp/app/signin/SignUpView;->N:Landroid/widget/EditText;

    if-eqz v0, :cond_c

    .line 2691
    const/16 v0, 0x74

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-virtual {v1, v0}, Landroid/widget/LinearLayout;->findViewWithTag(Ljava/lang/Object;)Landroid/view/View;

    move-result-object v0

    .line 2692
    const/4 v2, 0x0

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    .line 2693
    iget-object v2, p0, Lcom/osp/app/signin/SignUpView;->bB:Lcom/osp/app/signin/kd;

    invoke-virtual {v2}, Lcom/osp/app/signin/kd;->d()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 2695
    iget-object v2, p0, Lcom/osp/app/signin/SignUpView;->be:Ljava/util/ArrayList;

    const v3, 0x7f0c002c

    invoke-virtual {v0, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/EditText;

    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 2746
    :cond_1
    :goto_2
    iget-object v0, p0, Lcom/osp/app/signin/SignUpView;->bB:Lcom/osp/app/signin/kd;

    iget-boolean v0, v0, Lcom/osp/app/signin/kd;->f:Z

    if-eqz v0, :cond_2

    const-string v0, "localityText"

    invoke-virtual {v0, v6}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 2748
    iget-object v0, p0, Lcom/osp/app/signin/SignUpView;->K:Landroid/widget/EditText;

    if-eqz v0, :cond_d

    .line 2750
    const/16 v0, 0x77

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-virtual {v1, v0}, Landroid/widget/LinearLayout;->findViewWithTag(Ljava/lang/Object;)Landroid/view/View;

    move-result-object v0

    .line 2751
    const/4 v2, 0x0

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    .line 2752
    iget-object v2, p0, Lcom/osp/app/signin/SignUpView;->bB:Lcom/osp/app/signin/kd;

    invoke-virtual {v2}, Lcom/osp/app/signin/kd;->i()Z

    move-result v2

    if-eqz v2, :cond_2

    .line 2754
    iget-object v2, p0, Lcom/osp/app/signin/SignUpView;->be:Ljava/util/ArrayList;

    const v3, 0x7f0c002c

    invoke-virtual {v0, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/EditText;

    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 2762
    :cond_2
    :goto_3
    iget-object v0, p0, Lcom/osp/app/signin/SignUpView;->bB:Lcom/osp/app/signin/kd;

    iget-boolean v0, v0, Lcom/osp/app/signin/kd;->g:Z

    if-eqz v0, :cond_3

    const-string v0, "postalCodeText"

    invoke-virtual {v0, v6}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 2764
    iget-object v0, p0, Lcom/osp/app/signin/SignUpView;->L:Landroid/widget/EditText;

    if-eqz v0, :cond_e

    .line 2766
    const/16 v0, 0x78

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-virtual {v1, v0}, Landroid/widget/LinearLayout;->findViewWithTag(Ljava/lang/Object;)Landroid/view/View;

    move-result-object v0

    .line 2767
    const/4 v2, 0x0

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    .line 2768
    iget-object v2, p0, Lcom/osp/app/signin/SignUpView;->bB:Lcom/osp/app/signin/kd;

    invoke-virtual {v2}, Lcom/osp/app/signin/kd;->n()Z

    move-result v2

    if-eqz v2, :cond_3

    .line 2770
    iget-object v2, p0, Lcom/osp/app/signin/SignUpView;->be:Ljava/util/ArrayList;

    const v3, 0x7f0c002c

    invoke-virtual {v0, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/EditText;

    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 2778
    :cond_3
    :goto_4
    iget-object v0, p0, Lcom/osp/app/signin/SignUpView;->bB:Lcom/osp/app/signin/kd;

    iget-boolean v0, v0, Lcom/osp/app/signin/kd;->h:Z

    if-eqz v0, :cond_4

    const-string v0, "receiveSMSPhoneNumberText"

    invoke-virtual {v0, v6}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 2780
    iget-object v0, p0, Lcom/osp/app/signin/SignUpView;->O:Landroid/widget/EditText;

    if-eqz v0, :cond_f

    .line 2782
    const/16 v0, 0x79

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-virtual {v1, v0}, Landroid/widget/LinearLayout;->findViewWithTag(Ljava/lang/Object;)Landroid/view/View;

    move-result-object v0

    .line 2783
    const/4 v2, 0x0

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    .line 2784
    iget-object v2, p0, Lcom/osp/app/signin/SignUpView;->bB:Lcom/osp/app/signin/kd;

    invoke-virtual {v2}, Lcom/osp/app/signin/kd;->o()Z

    move-result v2

    if-eqz v2, :cond_4

    .line 2786
    iget-object v2, p0, Lcom/osp/app/signin/SignUpView;->be:Ljava/util/ArrayList;

    const v3, 0x7f0c002c

    invoke-virtual {v0, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/EditText;

    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 2794
    :cond_4
    :goto_5
    iget-object v0, p0, Lcom/osp/app/signin/SignUpView;->bB:Lcom/osp/app/signin/kd;

    iget-boolean v0, v0, Lcom/osp/app/signin/kd;->j:Z

    if-eqz v0, :cond_5

    const-string v0, "streetText"

    invoke-virtual {v0, v6}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_5

    .line 2796
    iget-object v0, p0, Lcom/osp/app/signin/SignUpView;->R:Landroid/widget/EditText;

    if-eqz v0, :cond_10

    .line 2798
    const/16 v0, 0x7a

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-virtual {v1, v0}, Landroid/widget/LinearLayout;->findViewWithTag(Ljava/lang/Object;)Landroid/view/View;

    move-result-object v0

    .line 2799
    const/4 v2, 0x0

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    .line 2800
    iget-object v2, p0, Lcom/osp/app/signin/SignUpView;->bB:Lcom/osp/app/signin/kd;

    invoke-virtual {v2}, Lcom/osp/app/signin/kd;->j()Z

    move-result v2

    if-eqz v2, :cond_5

    .line 2802
    iget-object v2, p0, Lcom/osp/app/signin/SignUpView;->be:Ljava/util/ArrayList;

    const v3, 0x7f0c002c

    invoke-virtual {v0, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/EditText;

    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 2810
    :cond_5
    :goto_6
    iget-object v0, p0, Lcom/osp/app/signin/SignUpView;->bB:Lcom/osp/app/signin/kd;

    iget-boolean v0, v0, Lcom/osp/app/signin/kd;->k:Z

    if-eqz v0, :cond_6

    const-string v0, "extendedText"

    invoke-virtual {v0, v6}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_6

    .line 2812
    iget-object v0, p0, Lcom/osp/app/signin/SignUpView;->G:Landroid/widget/EditText;

    if-eqz v0, :cond_11

    .line 2814
    const/16 v0, 0x7b

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-virtual {v1, v0}, Landroid/widget/LinearLayout;->findViewWithTag(Ljava/lang/Object;)Landroid/view/View;

    move-result-object v0

    .line 2815
    const/4 v2, 0x0

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    .line 2816
    iget-object v2, p0, Lcom/osp/app/signin/SignUpView;->bB:Lcom/osp/app/signin/kd;

    invoke-virtual {v2}, Lcom/osp/app/signin/kd;->k()Z

    move-result v2

    if-eqz v2, :cond_6

    .line 2818
    iget-object v2, p0, Lcom/osp/app/signin/SignUpView;->be:Ljava/util/ArrayList;

    const v3, 0x7f0c002c

    invoke-virtual {v0, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/EditText;

    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 2826
    :cond_6
    :goto_7
    iget-object v0, p0, Lcom/osp/app/signin/SignUpView;->bB:Lcom/osp/app/signin/kd;

    iget-boolean v0, v0, Lcom/osp/app/signin/kd;->l:Z

    if-eqz v0, :cond_7

    const-string v0, "postOfficeBoxNumberText"

    invoke-virtual {v0, v6}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_7

    .line 2828
    iget-object v0, p0, Lcom/osp/app/signin/SignUpView;->M:Landroid/widget/EditText;

    if-eqz v0, :cond_12

    .line 2830
    const/16 v0, 0x7c

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-virtual {v1, v0}, Landroid/widget/LinearLayout;->findViewWithTag(Ljava/lang/Object;)Landroid/view/View;

    move-result-object v0

    .line 2831
    const/4 v2, 0x0

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    .line 2832
    iget-object v2, p0, Lcom/osp/app/signin/SignUpView;->bB:Lcom/osp/app/signin/kd;

    invoke-virtual {v2}, Lcom/osp/app/signin/kd;->l()Z

    move-result v2

    if-eqz v2, :cond_7

    .line 2834
    iget-object v2, p0, Lcom/osp/app/signin/SignUpView;->be:Ljava/util/ArrayList;

    const v3, 0x7f0c002c

    invoke-virtual {v0, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/EditText;

    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 2843
    :cond_7
    :goto_8
    iget-object v0, p0, Lcom/osp/app/signin/SignUpView;->bB:Lcom/osp/app/signin/kd;

    iget-boolean v0, v0, Lcom/osp/app/signin/kd;->m:Z

    if-eqz v0, :cond_8

    const-string v0, "regionText"

    invoke-virtual {v0, v6}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_8

    .line 2845
    iget-object v0, p0, Lcom/osp/app/signin/SignUpView;->P:Landroid/widget/EditText;

    if-eqz v0, :cond_13

    .line 2847
    const/16 v0, 0x7d

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-virtual {v1, v0}, Landroid/widget/LinearLayout;->findViewWithTag(Ljava/lang/Object;)Landroid/view/View;

    move-result-object v0

    .line 2848
    const/4 v2, 0x0

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    .line 2849
    iget-object v2, p0, Lcom/osp/app/signin/SignUpView;->bB:Lcom/osp/app/signin/kd;

    invoke-virtual {v2}, Lcom/osp/app/signin/kd;->m()Z

    move-result v2

    if-eqz v2, :cond_8

    .line 2851
    iget-object v2, p0, Lcom/osp/app/signin/SignUpView;->be:Ljava/util/ArrayList;

    const v3, 0x7f0c002c

    invoke-virtual {v0, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/EditText;

    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 2859
    :cond_8
    :goto_9
    iget-object v0, p0, Lcom/osp/app/signin/SignUpView;->bB:Lcom/osp/app/signin/kd;

    iget-boolean v0, v0, Lcom/osp/app/signin/kd;->n:Z

    if-eqz v0, :cond_9

    const-string v0, "genderTypeCode"

    invoke-virtual {v0, v6}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_9

    .line 2861
    iget-object v0, p0, Lcom/osp/app/signin/SignUpView;->H:Landroid/widget/EditText;

    if-eqz v0, :cond_14

    .line 2863
    const/16 v0, 0x7e

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-virtual {v1, v0}, Landroid/widget/LinearLayout;->findViewWithTag(Ljava/lang/Object;)Landroid/view/View;

    move-result-object v0

    .line 2864
    const/4 v2, 0x0

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    .line 2865
    iget-object v2, p0, Lcom/osp/app/signin/SignUpView;->bB:Lcom/osp/app/signin/kd;

    invoke-virtual {v2}, Lcom/osp/app/signin/kd;->p()Z

    move-result v2

    if-eqz v2, :cond_9

    .line 2867
    iget-object v2, p0, Lcom/osp/app/signin/SignUpView;->be:Ljava/util/ArrayList;

    const v3, 0x7f0c002c

    invoke-virtual {v0, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/EditText;

    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 2875
    :cond_9
    :goto_a
    iget-object v0, p0, Lcom/osp/app/signin/SignUpView;->bB:Lcom/osp/app/signin/kd;

    iget-boolean v0, v0, Lcom/osp/app/signin/kd;->o:Z

    if-eqz v0, :cond_a

    const-string v0, "userDisplayName"

    invoke-virtual {v0, v6}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_a

    .line 2877
    iget-object v0, p0, Lcom/osp/app/signin/SignUpView;->S:Landroid/widget/EditText;

    if-eqz v0, :cond_15

    .line 2879
    const/16 v0, 0x7f

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-virtual {v1, v0}, Landroid/widget/LinearLayout;->findViewWithTag(Ljava/lang/Object;)Landroid/view/View;

    move-result-object v0

    .line 2880
    const/4 v2, 0x0

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    .line 2881
    iget-object v2, p0, Lcom/osp/app/signin/SignUpView;->bB:Lcom/osp/app/signin/kd;

    invoke-virtual {v2}, Lcom/osp/app/signin/kd;->q()Z

    move-result v2

    if-eqz v2, :cond_a

    .line 2883
    iget-object v2, p0, Lcom/osp/app/signin/SignUpView;->be:Ljava/util/ArrayList;

    const v3, 0x7f0c002c

    invoke-virtual {v0, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/EditText;

    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 2891
    :cond_a
    :goto_b
    iget-object v0, p0, Lcom/osp/app/signin/SignUpView;->bB:Lcom/osp/app/signin/kd;

    iget-boolean v0, v0, Lcom/osp/app/signin/kd;->p:Z

    if-eqz v0, :cond_b

    const-string v0, "relationshipStatusCode"

    invoke-virtual {v0, v6}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_b

    .line 2893
    iget-object v0, p0, Lcom/osp/app/signin/SignUpView;->Q:Landroid/widget/EditText;

    if-eqz v0, :cond_16

    .line 2895
    const/16 v0, 0x80

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-virtual {v1, v0}, Landroid/widget/LinearLayout;->findViewWithTag(Ljava/lang/Object;)Landroid/view/View;

    move-result-object v0

    .line 2896
    const/4 v2, 0x0

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    .line 2897
    iget-object v2, p0, Lcom/osp/app/signin/SignUpView;->bB:Lcom/osp/app/signin/kd;

    invoke-virtual {v2}, Lcom/osp/app/signin/kd;->r()Z

    move-result v2

    if-eqz v2, :cond_b

    .line 2899
    iget-object v2, p0, Lcom/osp/app/signin/SignUpView;->be:Ljava/util/ArrayList;

    const v3, 0x7f0c002c

    invoke-virtual {v0, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/EditText;

    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 2907
    :cond_b
    :goto_c
    add-int/lit8 v5, v5, 0x1

    .line 2908
    goto/16 :goto_1

    .line 2699
    :cond_c
    const v0, 0x7f09003d

    invoke-virtual {p0, v0}, Lcom/osp/app/signin/SignUpView;->getString(I)Ljava/lang/String;

    move-result-object v2

    iget-object v0, p0, Lcom/osp/app/signin/SignUpView;->bB:Lcom/osp/app/signin/kd;

    invoke-virtual {v0}, Lcom/osp/app/signin/kd;->d()Z

    move-result v3

    const/16 v4, 0x74

    move-object v0, p0

    invoke-direct/range {v0 .. v5}, Lcom/osp/app/signin/SignUpView;->a(Landroid/widget/LinearLayout;Ljava/lang/String;ZII)Landroid/widget/EditText;

    move-result-object v0

    iput-object v0, p0, Lcom/osp/app/signin/SignUpView;->N:Landroid/widget/EditText;

    goto/16 :goto_2

    .line 2758
    :cond_d
    const v0, 0x7f09001a

    invoke-virtual {p0, v0}, Lcom/osp/app/signin/SignUpView;->getString(I)Ljava/lang/String;

    move-result-object v2

    iget-object v0, p0, Lcom/osp/app/signin/SignUpView;->bB:Lcom/osp/app/signin/kd;

    invoke-virtual {v0}, Lcom/osp/app/signin/kd;->i()Z

    move-result v3

    const/16 v4, 0x77

    move-object v0, p0

    invoke-direct/range {v0 .. v5}, Lcom/osp/app/signin/SignUpView;->a(Landroid/widget/LinearLayout;Ljava/lang/String;ZII)Landroid/widget/EditText;

    move-result-object v0

    iput-object v0, p0, Lcom/osp/app/signin/SignUpView;->K:Landroid/widget/EditText;

    goto/16 :goto_3

    .line 2774
    :cond_e
    const v0, 0x7f090049

    invoke-virtual {p0, v0}, Lcom/osp/app/signin/SignUpView;->getString(I)Ljava/lang/String;

    move-result-object v2

    iget-object v0, p0, Lcom/osp/app/signin/SignUpView;->bB:Lcom/osp/app/signin/kd;

    invoke-virtual {v0}, Lcom/osp/app/signin/kd;->n()Z

    move-result v3

    const/16 v4, 0x78

    move-object v0, p0

    invoke-direct/range {v0 .. v5}, Lcom/osp/app/signin/SignUpView;->a(Landroid/widget/LinearLayout;Ljava/lang/String;ZII)Landroid/widget/EditText;

    move-result-object v0

    iput-object v0, p0, Lcom/osp/app/signin/SignUpView;->L:Landroid/widget/EditText;

    goto/16 :goto_4

    .line 2790
    :cond_f
    const v0, 0x7f090048

    invoke-virtual {p0, v0}, Lcom/osp/app/signin/SignUpView;->getString(I)Ljava/lang/String;

    move-result-object v2

    iget-object v0, p0, Lcom/osp/app/signin/SignUpView;->bB:Lcom/osp/app/signin/kd;

    invoke-virtual {v0}, Lcom/osp/app/signin/kd;->o()Z

    move-result v3

    const/16 v4, 0x79

    move-object v0, p0

    invoke-direct/range {v0 .. v5}, Lcom/osp/app/signin/SignUpView;->a(Landroid/widget/LinearLayout;Ljava/lang/String;ZII)Landroid/widget/EditText;

    move-result-object v0

    iput-object v0, p0, Lcom/osp/app/signin/SignUpView;->O:Landroid/widget/EditText;

    goto/16 :goto_5

    .line 2806
    :cond_10
    const v0, 0x7f090017

    invoke-virtual {p0, v0}, Lcom/osp/app/signin/SignUpView;->getString(I)Ljava/lang/String;

    move-result-object v2

    iget-object v0, p0, Lcom/osp/app/signin/SignUpView;->bB:Lcom/osp/app/signin/kd;

    invoke-virtual {v0}, Lcom/osp/app/signin/kd;->j()Z

    move-result v3

    const/16 v4, 0x7a

    move-object v0, p0

    invoke-direct/range {v0 .. v5}, Lcom/osp/app/signin/SignUpView;->a(Landroid/widget/LinearLayout;Ljava/lang/String;ZII)Landroid/widget/EditText;

    move-result-object v0

    iput-object v0, p0, Lcom/osp/app/signin/SignUpView;->R:Landroid/widget/EditText;

    goto/16 :goto_6

    .line 2822
    :cond_11
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const v2, 0x7f090017

    invoke-virtual {p0, v2}, Lcom/osp/app/signin/SignUpView;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, "2"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    iget-object v0, p0, Lcom/osp/app/signin/SignUpView;->bB:Lcom/osp/app/signin/kd;

    invoke-virtual {v0}, Lcom/osp/app/signin/kd;->k()Z

    move-result v3

    const/16 v4, 0x7b

    move-object v0, p0

    invoke-direct/range {v0 .. v5}, Lcom/osp/app/signin/SignUpView;->a(Landroid/widget/LinearLayout;Ljava/lang/String;ZII)Landroid/widget/EditText;

    move-result-object v0

    iput-object v0, p0, Lcom/osp/app/signin/SignUpView;->G:Landroid/widget/EditText;

    goto/16 :goto_7

    .line 2838
    :cond_12
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const v2, 0x7f090017

    invoke-virtual {p0, v2}, Lcom/osp/app/signin/SignUpView;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, "3"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    iget-object v0, p0, Lcom/osp/app/signin/SignUpView;->bB:Lcom/osp/app/signin/kd;

    invoke-virtual {v0}, Lcom/osp/app/signin/kd;->l()Z

    move-result v3

    const/16 v4, 0x7c

    move-object v0, p0

    invoke-direct/range {v0 .. v5}, Lcom/osp/app/signin/SignUpView;->a(Landroid/widget/LinearLayout;Ljava/lang/String;ZII)Landroid/widget/EditText;

    move-result-object v0

    iput-object v0, p0, Lcom/osp/app/signin/SignUpView;->M:Landroid/widget/EditText;

    goto/16 :goto_8

    .line 2855
    :cond_13
    const v0, 0x7f09004b

    invoke-virtual {p0, v0}, Lcom/osp/app/signin/SignUpView;->getString(I)Ljava/lang/String;

    move-result-object v2

    iget-object v0, p0, Lcom/osp/app/signin/SignUpView;->bB:Lcom/osp/app/signin/kd;

    invoke-virtual {v0}, Lcom/osp/app/signin/kd;->m()Z

    move-result v3

    const/16 v4, 0x7d

    move-object v0, p0

    invoke-direct/range {v0 .. v5}, Lcom/osp/app/signin/SignUpView;->a(Landroid/widget/LinearLayout;Ljava/lang/String;ZII)Landroid/widget/EditText;

    move-result-object v0

    iput-object v0, p0, Lcom/osp/app/signin/SignUpView;->P:Landroid/widget/EditText;

    goto/16 :goto_9

    .line 2871
    :cond_14
    const v0, 0x7f09002b

    invoke-virtual {p0, v0}, Lcom/osp/app/signin/SignUpView;->getString(I)Ljava/lang/String;

    move-result-object v2

    iget-object v0, p0, Lcom/osp/app/signin/SignUpView;->bB:Lcom/osp/app/signin/kd;

    invoke-virtual {v0}, Lcom/osp/app/signin/kd;->p()Z

    move-result v3

    const/16 v4, 0x7e

    move-object v0, p0

    invoke-direct/range {v0 .. v5}, Lcom/osp/app/signin/SignUpView;->a(Landroid/widget/LinearLayout;Ljava/lang/String;ZII)Landroid/widget/EditText;

    move-result-object v0

    iput-object v0, p0, Lcom/osp/app/signin/SignUpView;->H:Landroid/widget/EditText;

    goto/16 :goto_a

    .line 2887
    :cond_15
    const v0, 0x7f090040

    invoke-virtual {p0, v0}, Lcom/osp/app/signin/SignUpView;->getString(I)Ljava/lang/String;

    move-result-object v2

    iget-object v0, p0, Lcom/osp/app/signin/SignUpView;->bB:Lcom/osp/app/signin/kd;

    invoke-virtual {v0}, Lcom/osp/app/signin/kd;->q()Z

    move-result v3

    const/16 v4, 0x7f

    move-object v0, p0

    invoke-direct/range {v0 .. v5}, Lcom/osp/app/signin/SignUpView;->a(Landroid/widget/LinearLayout;Ljava/lang/String;ZII)Landroid/widget/EditText;

    move-result-object v0

    iput-object v0, p0, Lcom/osp/app/signin/SignUpView;->S:Landroid/widget/EditText;

    goto/16 :goto_b

    .line 2903
    :cond_16
    const v0, 0x7f09003b

    invoke-virtual {p0, v0}, Lcom/osp/app/signin/SignUpView;->getString(I)Ljava/lang/String;

    move-result-object v2

    iget-object v0, p0, Lcom/osp/app/signin/SignUpView;->bB:Lcom/osp/app/signin/kd;

    invoke-virtual {v0}, Lcom/osp/app/signin/kd;->r()Z

    move-result v3

    const/16 v4, 0x80

    move-object v0, p0

    invoke-direct/range {v0 .. v5}, Lcom/osp/app/signin/SignUpView;->a(Landroid/widget/LinearLayout;Ljava/lang/String;ZII)Landroid/widget/EditText;

    move-result-object v0

    iput-object v0, p0, Lcom/osp/app/signin/SignUpView;->Q:Landroid/widget/EditText;

    goto/16 :goto_c

    .line 2911
    :cond_17
    return-void
.end method

.method private s()V
    .locals 8

    .prologue
    const/4 v7, 0x1

    const/4 v6, 0x0

    .line 3721
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v0, "SignUpView::SignUpExecute"

    invoke-static {v0}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;)V

    .line 3732
    invoke-static {p0}, Lcom/osp/app/util/ad;->a(Landroid/content/Context;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 3788
    const/16 v0, 0xcb

    invoke-virtual {p0, v0}, Lcom/osp/app/signin/SignUpView;->c(I)V

    .line 4206
    :cond_0
    :goto_0
    return-void

    .line 3795
    :cond_1
    invoke-direct {p0}, Lcom/osp/app/signin/SignUpView;->t()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 3797
    iget-object v0, p0, Lcom/osp/app/signin/SignUpView;->F:Landroid/widget/AutoCompleteTextView;

    invoke-virtual {v0}, Landroid/widget/AutoCompleteTextView;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    sget-object v1, Ljava/util/Locale;->ENGLISH:Ljava/util/Locale;

    invoke-virtual {v0, v1}, Ljava/lang/String;->toLowerCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v1

    .line 3798
    invoke-direct {p0}, Lcom/osp/app/signin/SignUpView;->c()V

    .line 3799
    new-instance v0, Lcom/osp/app/signin/SignUpinfo;

    invoke-direct {v0}, Lcom/osp/app/signin/SignUpinfo;-><init>()V

    iput-object v0, p0, Lcom/osp/app/signin/SignUpView;->aq:Lcom/osp/app/signin/SignUpinfo;

    .line 3800
    iget-object v0, p0, Lcom/osp/app/signin/SignUpView;->aq:Lcom/osp/app/signin/SignUpinfo;

    invoke-virtual {v0, v1}, Lcom/osp/app/signin/SignUpinfo;->a(Ljava/lang/String;)V

    .line 3801
    iget-object v0, p0, Lcom/osp/app/signin/SignUpView;->aq:Lcom/osp/app/signin/SignUpinfo;

    invoke-virtual {v0, v1}, Lcom/osp/app/signin/SignUpinfo;->b(Ljava/lang/String;)V

    .line 3802
    invoke-static {}, Lcom/osp/device/b;->b()Lcom/osp/device/b;

    move-result-object v0

    invoke-virtual {v0}, Lcom/osp/device/b;->a()Z

    move-result v0

    if-eqz v0, :cond_3

    iget-boolean v0, p0, Lcom/osp/app/signin/SignUpView;->bl:Z

    if-eqz v0, :cond_3

    .line 3807
    :try_start_0
    invoke-static {}, Lcom/osp/common/util/i;->a()Lcom/osp/common/util/i;

    iget-object v0, p0, Lcom/osp/app/signin/SignUpView;->aj:Ljava/lang/String;

    invoke-static {v0}, Lcom/osp/common/util/i;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 3808
    if-eqz v0, :cond_2

    .line 3810
    sget-object v2, Ljava/util/Locale;->ENGLISH:Ljava/util/Locale;

    invoke-virtual {v0, v2}, Ljava/lang/String;->toUpperCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v0

    .line 3812
    :cond_2
    invoke-static {}, Lcom/google/i18n/phonenumbers/h;->a()Lcom/google/i18n/phonenumbers/h;

    move-result-object v2

    iget-object v3, p0, Lcom/osp/app/signin/SignUpView;->F:Landroid/widget/AutoCompleteTextView;

    invoke-virtual {v3}, Landroid/widget/AutoCompleteTextView;->getText()Landroid/text/Editable;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3, v0}, Lcom/google/i18n/phonenumbers/h;->a(Ljava/lang/CharSequence;Ljava/lang/String;)Ljava/lang/Iterable;

    move-result-object v0

    invoke-interface {v0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/i18n/phonenumbers/d;

    .line 3813
    invoke-virtual {v0}, Lcom/google/i18n/phonenumbers/d;->a()Lcom/google/i18n/phonenumbers/ac;

    move-result-object v0

    .line 3814
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0}, Lcom/google/i18n/phonenumbers/ac;->b()J

    move-result-wide v4

    invoke-virtual {v2, v4, v5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 3815
    iget-object v3, p0, Lcom/osp/app/signin/SignUpView;->aq:Lcom/osp/app/signin/SignUpinfo;

    invoke-virtual {v3, v2}, Lcom/osp/app/signin/SignUpinfo;->A(Ljava/lang/String;)V

    .line 3816
    iget-object v3, p0, Lcom/osp/app/signin/SignUpView;->aq:Lcom/osp/app/signin/SignUpinfo;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0}, Lcom/google/i18n/phonenumbers/ac;->a()I

    move-result v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Lcom/osp/app/signin/SignUpinfo;->b(Ljava/lang/String;)V

    .line 3817
    iget-object v3, p0, Lcom/osp/app/signin/SignUpView;->aq:Lcom/osp/app/signin/SignUpinfo;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0}, Lcom/google/i18n/phonenumbers/ac;->a()I

    move-result v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v3, v2}, Lcom/osp/app/signin/SignUpinfo;->a(Ljava/lang/String;)V

    .line 3818
    iget-object v2, p0, Lcom/osp/app/signin/SignUpView;->aq:Lcom/osp/app/signin/SignUpinfo;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0}, Lcom/google/i18n/phonenumbers/ac;->a()I

    move-result v0

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Lcom/osp/app/signin/SignUpinfo;->z(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 3823
    :cond_3
    :goto_1
    iget-object v0, p0, Lcom/osp/app/signin/SignUpView;->aq:Lcom/osp/app/signin/SignUpinfo;

    iget-object v2, p0, Lcom/osp/app/signin/SignUpView;->E:Landroid/widget/EditText;

    invoke-virtual {v2}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Lcom/osp/app/signin/SignUpinfo;->d(Ljava/lang/String;)V

    .line 3824
    iget-object v0, p0, Lcom/osp/app/signin/SignUpView;->aq:Lcom/osp/app/signin/SignUpinfo;

    iget-object v2, p0, Lcom/osp/app/signin/SignUpView;->D:Landroid/widget/EditText;

    invoke-virtual {v2}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Lcom/osp/app/signin/SignUpinfo;->e(Ljava/lang/String;)V

    .line 3828
    const/4 v0, 0x0

    const-string v2, "%04d%02d%02d"

    const/4 v3, 0x3

    new-array v3, v3, [Ljava/lang/Object;

    iget v4, p0, Lcom/osp/app/signin/SignUpView;->az:I

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v3, v6

    iget v4, p0, Lcom/osp/app/signin/SignUpView;->ay:I

    add-int/lit8 v4, v4, 0x1

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v3, v7

    const/4 v4, 0x2

    iget v5, p0, Lcom/osp/app/signin/SignUpView;->aA:I

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v3, v4

    invoke-static {v0, v2, v3}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 3829
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "String Format Modified : "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/osp/app/util/aq;->b(Ljava/lang/String;)V

    .line 3847
    iget-object v2, p0, Lcom/osp/app/signin/SignUpView;->aq:Lcom/osp/app/signin/SignUpinfo;

    invoke-virtual {v2, v0}, Lcom/osp/app/signin/SignUpinfo;->f(Ljava/lang/String;)V

    .line 3849
    invoke-static {p0}, Lcom/msc/c/e;->d(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v0

    .line 3853
    iget-object v2, p0, Lcom/osp/app/signin/SignUpView;->aq:Lcom/osp/app/signin/SignUpinfo;

    invoke-virtual {v2, v0}, Lcom/osp/app/signin/SignUpinfo;->g(Ljava/lang/String;)V

    .line 3854
    iget-object v0, p0, Lcom/osp/app/signin/SignUpView;->aq:Lcom/osp/app/signin/SignUpinfo;

    invoke-static {}, Lcom/osp/common/util/i;->a()Lcom/osp/common/util/i;

    invoke-static {p0}, Lcom/osp/common/util/i;->f(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Lcom/osp/app/signin/SignUpinfo;->h(Ljava/lang/String;)V

    .line 3861
    iget-object v0, p0, Lcom/osp/app/signin/SignUpView;->aq:Lcom/osp/app/signin/SignUpinfo;

    iget-object v2, p0, Lcom/osp/app/signin/SignUpView;->C:Landroid/widget/CheckBox;

    invoke-virtual {v2}, Landroid/widget/CheckBox;->isChecked()Z

    move-result v2

    invoke-virtual {v0, v2}, Lcom/osp/app/signin/SignUpinfo;->a(Z)V

    .line 3862
    iget-object v0, p0, Lcom/osp/app/signin/SignUpView;->aq:Lcom/osp/app/signin/SignUpinfo;

    iget-object v2, p0, Lcom/osp/app/signin/SignUpView;->B:Landroid/widget/CheckBox;

    invoke-virtual {v2}, Landroid/widget/CheckBox;->isChecked()Z

    move-result v2

    invoke-virtual {v0, v2}, Lcom/osp/app/signin/SignUpinfo;->b(Z)V

    .line 3864
    invoke-static {}, Lcom/osp/app/signin/SamsungService;->a()Z

    move-result v0

    if-eqz v0, :cond_1c

    .line 3866
    iget-object v0, p0, Lcom/osp/app/signin/SignUpView;->aq:Lcom/osp/app/signin/SignUpinfo;

    const-string v2, "SW"

    invoke-virtual {v0, v2}, Lcom/osp/app/signin/SignUpinfo;->x(Ljava/lang/String;)V

    .line 3878
    :goto_2
    iget-object v0, p0, Lcom/osp/app/signin/SignUpView;->aZ:Ljava/lang/String;

    if-eqz v0, :cond_4

    iget-object v0, p0, Lcom/osp/app/signin/SignUpView;->aZ:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v0

    if-lez v0, :cond_4

    iget-boolean v0, p0, Lcom/osp/app/signin/SignUpView;->bl:Z

    if-nez v0, :cond_4

    .line 3880
    iget-object v0, p0, Lcom/osp/app/signin/SignUpView;->aZ:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/osp/app/signin/SignUpView;->aY:Z

    .line 3881
    iget-boolean v0, p0, Lcom/osp/app/signin/SignUpView;->aY:Z

    if-eqz v0, :cond_4

    iget-boolean v0, p0, Lcom/osp/app/signin/SignUpView;->aT:Z

    if-eqz v0, :cond_4

    iget-object v0, p0, Lcom/osp/app/signin/SignUpView;->aZ:Ljava/lang/String;

    invoke-static {}, Lcom/osp/app/util/ad;->a()Z

    .line 3883
    :cond_4
    invoke-static {}, Lcom/osp/device/b;->b()Lcom/osp/device/b;

    move-result-object v0

    invoke-virtual {v0}, Lcom/osp/device/b;->a()Z

    move-result v0

    if-eqz v0, :cond_5

    iget-object v0, p0, Lcom/osp/app/signin/SignUpView;->ba:Ljava/lang/String;

    if-eqz v0, :cond_5

    iget-object v0, p0, Lcom/osp/app/signin/SignUpView;->ba:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v0

    if-lez v0, :cond_5

    iget-boolean v0, p0, Lcom/osp/app/signin/SignUpView;->bl:Z

    if-eqz v0, :cond_5

    .line 3895
    iget-object v0, p0, Lcom/osp/app/signin/SignUpView;->ba:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/osp/app/signin/SignUpView;->aY:Z

    .line 3896
    iget-boolean v0, p0, Lcom/osp/app/signin/SignUpView;->aY:Z

    if-eqz v0, :cond_5

    iget-boolean v0, p0, Lcom/osp/app/signin/SignUpView;->aT:Z

    if-eqz v0, :cond_5

    iget-object v0, p0, Lcom/osp/app/signin/SignUpView;->ba:Ljava/lang/String;

    invoke-static {}, Lcom/osp/app/util/ad;->a()Z

    .line 3898
    :cond_5
    iget-object v0, p0, Lcom/osp/app/signin/SignUpView;->bg:Ljava/lang/String;

    if-eqz v0, :cond_6

    iget-object v0, p0, Lcom/osp/app/signin/SignUpView;->bg:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v0

    if-lez v0, :cond_6

    iget-boolean v0, p0, Lcom/osp/app/signin/SignUpView;->bl:Z

    if-nez v0, :cond_6

    .line 3914
    iget-object v0, p0, Lcom/osp/app/signin/SignUpView;->bg:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/osp/app/signin/SignUpView;->bf:Z

    .line 3915
    iget-boolean v0, p0, Lcom/osp/app/signin/SignUpView;->bf:Z

    if-eqz v0, :cond_6

    iget-boolean v0, p0, Lcom/osp/app/signin/SignUpView;->aT:Z

    if-eqz v0, :cond_6

    iget-object v0, p0, Lcom/osp/app/signin/SignUpView;->bg:Ljava/lang/String;

    invoke-static {}, Lcom/osp/app/util/ad;->a()Z

    .line 3917
    :cond_6
    invoke-static {}, Lcom/osp/device/b;->b()Lcom/osp/device/b;

    move-result-object v0

    invoke-virtual {v0}, Lcom/osp/device/b;->a()Z

    move-result v0

    if-eqz v0, :cond_7

    iget-object v0, p0, Lcom/osp/app/signin/SignUpView;->bh:Ljava/lang/String;

    if-eqz v0, :cond_7

    iget-object v0, p0, Lcom/osp/app/signin/SignUpView;->bh:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v0

    if-lez v0, :cond_7

    iget-boolean v0, p0, Lcom/osp/app/signin/SignUpView;->bl:Z

    if-eqz v0, :cond_7

    .line 3929
    iget-object v0, p0, Lcom/osp/app/signin/SignUpView;->bh:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/osp/app/signin/SignUpView;->bf:Z

    .line 3930
    iget-boolean v0, p0, Lcom/osp/app/signin/SignUpView;->bf:Z

    if-eqz v0, :cond_7

    iget-boolean v0, p0, Lcom/osp/app/signin/SignUpView;->aT:Z

    if-eqz v0, :cond_7

    iget-object v0, p0, Lcom/osp/app/signin/SignUpView;->bh:Ljava/lang/String;

    invoke-static {}, Lcom/osp/app/util/ad;->a()Z

    .line 3932
    :cond_7
    iget-boolean v0, p0, Lcom/osp/app/signin/SignUpView;->aY:Z

    if-nez v0, :cond_8

    iget-boolean v0, p0, Lcom/osp/app/signin/SignUpView;->bf:Z

    if-nez v0, :cond_8

    iget-boolean v0, p0, Lcom/osp/app/signin/SignUpView;->aT:Z

    if-nez v0, :cond_8

    iget-boolean v0, p0, Lcom/osp/app/signin/SignUpView;->bl:Z

    if-nez v0, :cond_8

    .line 3948
    iput-boolean v6, p0, Lcom/osp/app/signin/SignUpView;->bn:Z

    .line 3949
    invoke-static {}, Lcom/osp/app/util/r;->r()Z

    move-result v0

    if-eqz v0, :cond_8

    invoke-direct {p0, v1}, Lcom/osp/app/signin/SignUpView;->i(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_8

    .line 3951
    iput-boolean v7, p0, Lcom/osp/app/signin/SignUpView;->bn:Z

    .line 3952
    iput-boolean v6, p0, Lcom/osp/app/signin/SignUpView;->aT:Z

    .line 3953
    iput-boolean v6, p0, Lcom/osp/app/signin/SignUpView;->aY:Z

    .line 3954
    iput-boolean v6, p0, Lcom/osp/app/signin/SignUpView;->bf:Z

    .line 3955
    :cond_8
    iget-boolean v0, p0, Lcom/osp/app/signin/SignUpView;->aY:Z

    if-eqz v0, :cond_1e

    .line 3981
    iget-object v0, p0, Lcom/osp/app/signin/SignUpView;->aq:Lcom/osp/app/signin/SignUpinfo;

    invoke-virtual {v0, v6}, Lcom/osp/app/signin/SignUpinfo;->c(Z)V

    .line 3983
    iget-boolean v0, p0, Lcom/osp/app/signin/SignUpView;->by:Z

    if-eqz v0, :cond_1d

    .line 3985
    iget-object v0, p0, Lcom/osp/app/signin/SignUpView;->aq:Lcom/osp/app/signin/SignUpinfo;

    const-string v1, "FB_S"

    invoke-virtual {v0, v1}, Lcom/osp/app/signin/SignUpinfo;->y(Ljava/lang/String;)V

    .line 4057
    :cond_9
    :goto_3
    invoke-static {}, Lcom/osp/device/b;->b()Lcom/osp/device/b;

    move-result-object v0

    invoke-virtual {v0}, Lcom/osp/device/b;->a()Z

    move-result v0

    if-eqz v0, :cond_a

    iget-boolean v0, p0, Lcom/osp/app/signin/SignUpView;->bl:Z

    if-eqz v0, :cond_a

    .line 4059
    iget-object v0, p0, Lcom/osp/app/signin/SignUpView;->aq:Lcom/osp/app/signin/SignUpinfo;

    invoke-virtual {v0, v6}, Lcom/osp/app/signin/SignUpinfo;->c(Z)V

    .line 4072
    :cond_a
    iget-object v0, p0, Lcom/osp/app/signin/SignUpView;->bB:Lcom/osp/app/signin/kd;

    iget-boolean v0, v0, Lcom/osp/app/signin/kd;->e:Z

    if-eqz v0, :cond_b

    iget-object v0, p0, Lcom/osp/app/signin/SignUpView;->I:Landroid/widget/EditText;

    if-eqz v0, :cond_b

    .line 4074
    iget-object v0, p0, Lcom/osp/app/signin/SignUpView;->aq:Lcom/osp/app/signin/SignUpinfo;

    iget-object v1, p0, Lcom/osp/app/signin/SignUpView;->I:Landroid/widget/EditText;

    invoke-virtual {v1}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/osp/app/signin/SignUpinfo;->k(Ljava/lang/String;)V

    .line 4076
    :cond_b
    iget-object v0, p0, Lcom/osp/app/signin/SignUpView;->bB:Lcom/osp/app/signin/kd;

    iget-boolean v0, v0, Lcom/osp/app/signin/kd;->d:Z

    if-eqz v0, :cond_c

    iget-object v0, p0, Lcom/osp/app/signin/SignUpView;->J:Landroid/widget/EditText;

    if-eqz v0, :cond_c

    .line 4078
    iget-object v0, p0, Lcom/osp/app/signin/SignUpView;->aq:Lcom/osp/app/signin/SignUpinfo;

    iget-object v1, p0, Lcom/osp/app/signin/SignUpView;->J:Landroid/widget/EditText;

    invoke-virtual {v1}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/osp/app/signin/SignUpinfo;->j(Ljava/lang/String;)V

    .line 4080
    :cond_c
    iget-object v0, p0, Lcom/osp/app/signin/SignUpView;->bB:Lcom/osp/app/signin/kd;

    iget-boolean v0, v0, Lcom/osp/app/signin/kd;->c:Z

    if-eqz v0, :cond_d

    iget-object v0, p0, Lcom/osp/app/signin/SignUpView;->aw:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    iget v1, p0, Lcom/osp/app/signin/SignUpView;->aB:I

    if-le v0, v1, :cond_d

    .line 4082
    iget-object v1, p0, Lcom/osp/app/signin/SignUpView;->aq:Lcom/osp/app/signin/SignUpinfo;

    iget-object v0, p0, Lcom/osp/app/signin/SignUpView;->aw:Ljava/util/ArrayList;

    iget v2, p0, Lcom/osp/app/signin/SignUpView;->aB:I

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-virtual {v1, v0}, Lcom/osp/app/signin/SignUpinfo;->i(Ljava/lang/String;)V

    .line 4084
    :cond_d
    iget-object v0, p0, Lcom/osp/app/signin/SignUpView;->bB:Lcom/osp/app/signin/kd;

    iget-boolean v0, v0, Lcom/osp/app/signin/kd;->f:Z

    if-eqz v0, :cond_e

    iget-object v0, p0, Lcom/osp/app/signin/SignUpView;->K:Landroid/widget/EditText;

    if-eqz v0, :cond_e

    .line 4086
    iget-object v0, p0, Lcom/osp/app/signin/SignUpView;->aq:Lcom/osp/app/signin/SignUpinfo;

    iget-object v1, p0, Lcom/osp/app/signin/SignUpView;->K:Landroid/widget/EditText;

    invoke-virtual {v1}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/osp/app/signin/SignUpinfo;->l(Ljava/lang/String;)V

    .line 4088
    :cond_e
    iget-object v0, p0, Lcom/osp/app/signin/SignUpView;->bB:Lcom/osp/app/signin/kd;

    iget-boolean v0, v0, Lcom/osp/app/signin/kd;->g:Z

    if-eqz v0, :cond_f

    iget-object v0, p0, Lcom/osp/app/signin/SignUpView;->L:Landroid/widget/EditText;

    if-eqz v0, :cond_f

    .line 4090
    iget-object v0, p0, Lcom/osp/app/signin/SignUpView;->aq:Lcom/osp/app/signin/SignUpinfo;

    iget-object v1, p0, Lcom/osp/app/signin/SignUpView;->L:Landroid/widget/EditText;

    invoke-virtual {v1}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/osp/app/signin/SignUpinfo;->m(Ljava/lang/String;)V

    .line 4092
    :cond_f
    iget-object v0, p0, Lcom/osp/app/signin/SignUpView;->bB:Lcom/osp/app/signin/kd;

    iget-boolean v0, v0, Lcom/osp/app/signin/kd;->h:Z

    if-eqz v0, :cond_10

    iget-object v0, p0, Lcom/osp/app/signin/SignUpView;->O:Landroid/widget/EditText;

    if-eqz v0, :cond_10

    .line 4094
    iget-object v0, p0, Lcom/osp/app/signin/SignUpView;->aq:Lcom/osp/app/signin/SignUpinfo;

    iget-object v1, p0, Lcom/osp/app/signin/SignUpView;->O:Landroid/widget/EditText;

    invoke-virtual {v1}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/osp/app/signin/SignUpinfo;->n(Ljava/lang/String;)V

    .line 4096
    :cond_10
    iget-object v0, p0, Lcom/osp/app/signin/SignUpView;->bB:Lcom/osp/app/signin/kd;

    iget-boolean v0, v0, Lcom/osp/app/signin/kd;->j:Z

    if-eqz v0, :cond_11

    iget-object v0, p0, Lcom/osp/app/signin/SignUpView;->R:Landroid/widget/EditText;

    if-eqz v0, :cond_11

    .line 4098
    iget-object v0, p0, Lcom/osp/app/signin/SignUpView;->aq:Lcom/osp/app/signin/SignUpinfo;

    iget-object v1, p0, Lcom/osp/app/signin/SignUpView;->R:Landroid/widget/EditText;

    invoke-virtual {v1}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/osp/app/signin/SignUpinfo;->o(Ljava/lang/String;)V

    .line 4100
    :cond_11
    iget-object v0, p0, Lcom/osp/app/signin/SignUpView;->bB:Lcom/osp/app/signin/kd;

    iget-boolean v0, v0, Lcom/osp/app/signin/kd;->k:Z

    if-eqz v0, :cond_12

    iget-object v0, p0, Lcom/osp/app/signin/SignUpView;->G:Landroid/widget/EditText;

    if-eqz v0, :cond_12

    .line 4102
    iget-object v0, p0, Lcom/osp/app/signin/SignUpView;->aq:Lcom/osp/app/signin/SignUpinfo;

    iget-object v1, p0, Lcom/osp/app/signin/SignUpView;->G:Landroid/widget/EditText;

    invoke-virtual {v1}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/osp/app/signin/SignUpinfo;->p(Ljava/lang/String;)V

    .line 4104
    :cond_12
    iget-object v0, p0, Lcom/osp/app/signin/SignUpView;->bB:Lcom/osp/app/signin/kd;

    iget-boolean v0, v0, Lcom/osp/app/signin/kd;->l:Z

    if-eqz v0, :cond_13

    iget-object v0, p0, Lcom/osp/app/signin/SignUpView;->M:Landroid/widget/EditText;

    if-eqz v0, :cond_13

    .line 4106
    iget-object v0, p0, Lcom/osp/app/signin/SignUpView;->aq:Lcom/osp/app/signin/SignUpinfo;

    iget-object v1, p0, Lcom/osp/app/signin/SignUpView;->M:Landroid/widget/EditText;

    invoke-virtual {v1}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/osp/app/signin/SignUpinfo;->q(Ljava/lang/String;)V

    .line 4108
    :cond_13
    iget-object v0, p0, Lcom/osp/app/signin/SignUpView;->bB:Lcom/osp/app/signin/kd;

    iget-boolean v0, v0, Lcom/osp/app/signin/kd;->m:Z

    if-eqz v0, :cond_14

    iget-object v0, p0, Lcom/osp/app/signin/SignUpView;->P:Landroid/widget/EditText;

    if-eqz v0, :cond_14

    .line 4110
    iget-object v0, p0, Lcom/osp/app/signin/SignUpView;->aq:Lcom/osp/app/signin/SignUpinfo;

    iget-object v1, p0, Lcom/osp/app/signin/SignUpView;->P:Landroid/widget/EditText;

    invoke-virtual {v1}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/osp/app/signin/SignUpinfo;->r(Ljava/lang/String;)V

    .line 4112
    :cond_14
    iget-object v0, p0, Lcom/osp/app/signin/SignUpView;->bB:Lcom/osp/app/signin/kd;

    iget-boolean v0, v0, Lcom/osp/app/signin/kd;->n:Z

    if-eqz v0, :cond_15

    .line 4124
    iget-object v1, p0, Lcom/osp/app/signin/SignUpView;->aq:Lcom/osp/app/signin/SignUpinfo;

    iget v0, p0, Lcom/osp/app/signin/SignUpView;->av:I

    if-nez v0, :cond_27

    const-string v0, "M"

    :goto_4
    invoke-virtual {v1, v0}, Lcom/osp/app/signin/SignUpinfo;->s(Ljava/lang/String;)V

    .line 4127
    :cond_15
    iget-object v0, p0, Lcom/osp/app/signin/SignUpView;->bB:Lcom/osp/app/signin/kd;

    iget-boolean v0, v0, Lcom/osp/app/signin/kd;->o:Z

    if-eqz v0, :cond_16

    .line 4129
    iget-object v0, p0, Lcom/osp/app/signin/SignUpView;->aq:Lcom/osp/app/signin/SignUpinfo;

    iget-object v1, p0, Lcom/osp/app/signin/SignUpView;->S:Landroid/widget/EditText;

    invoke-virtual {v1}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/osp/app/signin/SignUpinfo;->t(Ljava/lang/String;)V

    .line 4132
    :cond_16
    iget-object v0, p0, Lcom/osp/app/signin/SignUpView;->bB:Lcom/osp/app/signin/kd;

    iget-boolean v0, v0, Lcom/osp/app/signin/kd;->p:Z

    if-eqz v0, :cond_17

    .line 4146
    iget-object v1, p0, Lcom/osp/app/signin/SignUpView;->aq:Lcom/osp/app/signin/SignUpinfo;

    iget v0, p0, Lcom/osp/app/signin/SignUpView;->aC:I

    if-nez v0, :cond_28

    const-string v0, "1"

    :goto_5
    invoke-virtual {v1, v0}, Lcom/osp/app/signin/SignUpinfo;->u(Ljava/lang/String;)V

    .line 4149
    :cond_17
    invoke-static {}, Lcom/osp/device/b;->b()Lcom/osp/device/b;

    move-result-object v0

    invoke-virtual {v0}, Lcom/osp/device/b;->a()Z

    move-result v0

    if-eqz v0, :cond_18

    iget-boolean v0, p0, Lcom/osp/app/signin/SignUpView;->bl:Z

    if-eqz v0, :cond_18

    .line 4151
    iget-object v0, p0, Lcom/osp/app/signin/SignUpView;->bq:Lcom/msc/a/j;

    if-eqz v0, :cond_18

    .line 4153
    iget-object v0, p0, Lcom/osp/app/signin/SignUpView;->aq:Lcom/osp/app/signin/SignUpinfo;

    iget-object v1, p0, Lcom/osp/app/signin/SignUpView;->bq:Lcom/msc/a/j;

    invoke-virtual {v1}, Lcom/msc/a/j;->a()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/osp/app/signin/SignUpinfo;->B(Ljava/lang/String;)V

    .line 4154
    iget-object v0, p0, Lcom/osp/app/signin/SignUpView;->aq:Lcom/osp/app/signin/SignUpinfo;

    iget-object v1, p0, Lcom/osp/app/signin/SignUpView;->bq:Lcom/msc/a/j;

    invoke-virtual {v1}, Lcom/msc/a/j;->b()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/osp/app/signin/SignUpinfo;->C(Ljava/lang/String;)V

    .line 4155
    iget-object v0, p0, Lcom/osp/app/signin/SignUpView;->bq:Lcom/msc/a/j;

    iget-object v1, p0, Lcom/osp/app/signin/SignUpView;->T:Landroid/widget/EditText;

    invoke-virtual {v1}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/msc/a/j;->c(Ljava/lang/String;)V

    .line 4156
    iget-object v0, p0, Lcom/osp/app/signin/SignUpView;->aq:Lcom/osp/app/signin/SignUpinfo;

    iget-object v1, p0, Lcom/osp/app/signin/SignUpView;->T:Landroid/widget/EditText;

    invoke-virtual {v1}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/osp/app/signin/SignUpinfo;->D(Ljava/lang/String;)V

    .line 4162
    :cond_18
    const-string v0, "450"

    iget-object v1, p0, Lcom/osp/app/signin/SignUpView;->aj:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1a

    iget-boolean v0, p0, Lcom/osp/app/signin/SignUpView;->U:Z

    if-eqz v0, :cond_1a

    .line 4164
    iget-object v0, p0, Lcom/osp/app/signin/SignUpView;->aG:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_19

    .line 4166
    iget-object v0, p0, Lcom/osp/app/signin/SignUpView;->aG:Ljava/lang/String;

    iget-object v1, p0, Lcom/osp/app/signin/SignUpView;->aj:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/osp/app/util/ad;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/google/i18n/phonenumbers/ac;

    move-result-object v0

    .line 4167
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0}, Lcom/google/i18n/phonenumbers/ac;->a()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v0}, Lcom/google/i18n/phonenumbers/ac;->b()J

    move-result-wide v2

    invoke-virtual {v1, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 4168
    iget-object v1, p0, Lcom/osp/app/signin/SignUpView;->aq:Lcom/osp/app/signin/SignUpinfo;

    invoke-virtual {v1, v0}, Lcom/osp/app/signin/SignUpinfo;->n(Ljava/lang/String;)V

    .line 4171
    :cond_19
    iget-object v0, p0, Lcom/osp/app/signin/SignUpView;->bB:Lcom/osp/app/signin/kd;

    iget-boolean v0, v0, Lcom/osp/app/signin/kd;->n:Z

    if-nez v0, :cond_1a

    iget-object v0, p0, Lcom/osp/app/signin/SignUpView;->aM:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_1a

    .line 4173
    iget-object v0, p0, Lcom/osp/app/signin/SignUpView;->aq:Lcom/osp/app/signin/SignUpinfo;

    iget-object v1, p0, Lcom/osp/app/signin/SignUpView;->aM:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/osp/app/signin/SignUpinfo;->s(Ljava/lang/String;)V

    .line 4177
    :cond_1a
    invoke-static {}, Lcom/osp/device/b;->b()Lcom/osp/device/b;

    move-result-object v0

    invoke-virtual {v0}, Lcom/osp/device/b;->a()Z

    move-result v0

    if-eqz v0, :cond_2b

    iget-boolean v0, p0, Lcom/osp/app/signin/SignUpView;->bl:Z

    if-eqz v0, :cond_2b

    iget-boolean v0, p0, Lcom/osp/app/signin/SignUpView;->aY:Z

    if-nez v0, :cond_2b

    iget-boolean v0, p0, Lcom/osp/app/signin/SignUpView;->bf:Z

    if-nez v0, :cond_2b

    iget-boolean v0, p0, Lcom/osp/app/signin/SignUpView;->aT:Z

    if-nez v0, :cond_2b

    iget-boolean v0, p0, Lcom/osp/app/signin/SignUpView;->bn:Z

    if-nez v0, :cond_2b

    .line 4181
    invoke-static {}, Lcom/osp/device/b;->b()Lcom/osp/device/b;

    move-result-object v0

    invoke-virtual {v0}, Lcom/osp/device/b;->a()Z

    move-result v0

    if-eqz v0, :cond_29

    .line 4183
    iget-object v0, p0, Lcom/osp/app/signin/SignUpView;->bu:Lcom/osp/app/signin/mc;

    if-eqz v0, :cond_1b

    iget-object v0, p0, Lcom/osp/app/signin/SignUpView;->bu:Lcom/osp/app/signin/mc;

    invoke-virtual {v0}, Lcom/osp/app/signin/mc;->getStatus()Landroid/os/AsyncTask$Status;

    move-result-object v0

    sget-object v1, Landroid/os/AsyncTask$Status;->RUNNING:Landroid/os/AsyncTask$Status;

    if-eq v0, v1, :cond_0

    .line 4185
    :cond_1b
    new-instance v0, Lcom/osp/app/signin/mc;

    invoke-direct {v0, p0}, Lcom/osp/app/signin/mc;-><init>(Lcom/osp/app/signin/SignUpView;)V

    iput-object v0, p0, Lcom/osp/app/signin/SignUpView;->bu:Lcom/osp/app/signin/mc;

    .line 4187
    iget-object v0, p0, Lcom/osp/app/signin/SignUpView;->bu:Lcom/osp/app/signin/mc;

    invoke-virtual {v0}, Lcom/osp/app/signin/mc;->b()V

    goto/16 :goto_0

    .line 3869
    :cond_1c
    iget-object v0, p0, Lcom/osp/app/signin/SignUpView;->aq:Lcom/osp/app/signin/SignUpinfo;

    const-string v2, "PD"

    invoke-virtual {v0, v2}, Lcom/osp/app/signin/SignUpinfo;->x(Ljava/lang/String;)V

    goto/16 :goto_2

    .line 3988
    :cond_1d
    iget-object v0, p0, Lcom/osp/app/signin/SignUpView;->aq:Lcom/osp/app/signin/SignUpinfo;

    const-string v1, "FB"

    invoke-virtual {v0, v1}, Lcom/osp/app/signin/SignUpinfo;->y(Ljava/lang/String;)V

    goto/16 :goto_3

    .line 3991
    :cond_1e
    iget-boolean v0, p0, Lcom/osp/app/signin/SignUpView;->bf:Z

    if-eqz v0, :cond_21

    .line 3993
    iget-object v0, p0, Lcom/osp/app/signin/SignUpView;->bi:Lcom/osp/app/signin/pz;

    invoke-virtual {v0}, Lcom/osp/app/signin/pz;->h()Ljava/lang/Boolean;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_1f

    .line 3995
    iget-object v0, p0, Lcom/osp/app/signin/SignUpView;->aq:Lcom/osp/app/signin/SignUpinfo;

    invoke-virtual {v0, v6}, Lcom/osp/app/signin/SignUpinfo;->c(Z)V

    .line 4002
    :goto_6
    iget-boolean v0, p0, Lcom/osp/app/signin/SignUpView;->by:Z

    if-eqz v0, :cond_20

    .line 4004
    iget-object v0, p0, Lcom/osp/app/signin/SignUpView;->aq:Lcom/osp/app/signin/SignUpinfo;

    const-string v1, "WB_S"

    invoke-virtual {v0, v1}, Lcom/osp/app/signin/SignUpinfo;->y(Ljava/lang/String;)V

    goto/16 :goto_3

    .line 3998
    :cond_1f
    iget-object v0, p0, Lcom/osp/app/signin/SignUpView;->aq:Lcom/osp/app/signin/SignUpinfo;

    invoke-virtual {v0, v7}, Lcom/osp/app/signin/SignUpinfo;->c(Z)V

    .line 3999
    iput-boolean v6, p0, Lcom/osp/app/signin/SignUpView;->bf:Z

    goto :goto_6

    .line 4007
    :cond_20
    iget-object v0, p0, Lcom/osp/app/signin/SignUpView;->aq:Lcom/osp/app/signin/SignUpinfo;

    const-string v1, "WB"

    invoke-virtual {v0, v1}, Lcom/osp/app/signin/SignUpinfo;->y(Ljava/lang/String;)V

    goto/16 :goto_3

    .line 4010
    :cond_21
    iget-boolean v0, p0, Lcom/osp/app/signin/SignUpView;->aT:Z

    if-eqz v0, :cond_24

    .line 4012
    invoke-direct {p0}, Lcom/osp/app/signin/SignUpView;->A()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_23

    .line 4014
    iget-object v0, p0, Lcom/osp/app/signin/SignUpView;->aq:Lcom/osp/app/signin/SignUpinfo;

    invoke-virtual {v0, v6}, Lcom/osp/app/signin/SignUpinfo;->c(Z)V

    .line 4016
    iget-boolean v0, p0, Lcom/osp/app/signin/SignUpView;->by:Z

    if-eqz v0, :cond_22

    .line 4018
    iget-object v0, p0, Lcom/osp/app/signin/SignUpView;->aq:Lcom/osp/app/signin/SignUpinfo;

    const-string v1, "GG_S"

    invoke-virtual {v0, v1}, Lcom/osp/app/signin/SignUpinfo;->y(Ljava/lang/String;)V

    goto/16 :goto_3

    .line 4021
    :cond_22
    iget-object v0, p0, Lcom/osp/app/signin/SignUpView;->aq:Lcom/osp/app/signin/SignUpinfo;

    const-string v1, "GG"

    invoke-virtual {v0, v1}, Lcom/osp/app/signin/SignUpinfo;->y(Ljava/lang/String;)V

    goto/16 :goto_3

    .line 4025
    :cond_23
    iget-object v0, p0, Lcom/osp/app/signin/SignUpView;->aq:Lcom/osp/app/signin/SignUpinfo;

    invoke-virtual {v0, v7}, Lcom/osp/app/signin/SignUpinfo;->c(Z)V

    .line 4026
    iput-boolean v6, p0, Lcom/osp/app/signin/SignUpView;->aT:Z

    goto/16 :goto_3

    .line 4028
    :cond_24
    iget-boolean v0, p0, Lcom/osp/app/signin/SignUpView;->bn:Z

    if-eqz v0, :cond_26

    .line 4030
    iget-object v0, p0, Lcom/osp/app/signin/SignUpView;->aq:Lcom/osp/app/signin/SignUpinfo;

    invoke-virtual {v0, v6}, Lcom/osp/app/signin/SignUpinfo;->c(Z)V

    .line 4032
    iget-boolean v0, p0, Lcom/osp/app/signin/SignUpView;->by:Z

    if-eqz v0, :cond_25

    .line 4034
    iget-object v0, p0, Lcom/osp/app/signin/SignUpView;->aq:Lcom/osp/app/signin/SignUpinfo;

    const-string v1, "AM_S"

    invoke-virtual {v0, v1}, Lcom/osp/app/signin/SignUpinfo;->y(Ljava/lang/String;)V

    goto/16 :goto_3

    .line 4037
    :cond_25
    iget-object v0, p0, Lcom/osp/app/signin/SignUpView;->aq:Lcom/osp/app/signin/SignUpinfo;

    const-string v1, "AM"

    invoke-virtual {v0, v1}, Lcom/osp/app/signin/SignUpinfo;->y(Ljava/lang/String;)V

    goto/16 :goto_3

    .line 4039
    :cond_26
    iget-object v0, p0, Lcom/osp/app/signin/SignUpView;->aq:Lcom/osp/app/signin/SignUpinfo;

    invoke-virtual {v0, v7}, Lcom/osp/app/signin/SignUpinfo;->c(Z)V

    .line 4051
    iget-boolean v0, p0, Lcom/osp/app/signin/SignUpView;->by:Z

    if-eqz v0, :cond_9

    .line 4053
    iget-object v0, p0, Lcom/osp/app/signin/SignUpView;->aq:Lcom/osp/app/signin/SignUpinfo;

    const-string v1, "SA_S"

    invoke-virtual {v0, v1}, Lcom/osp/app/signin/SignUpinfo;->y(Ljava/lang/String;)V

    goto/16 :goto_3

    .line 4124
    :cond_27
    const-string v0, "F"

    goto/16 :goto_4

    .line 4146
    :cond_28
    const-string v0, "2"

    goto/16 :goto_5

    .line 4192
    :cond_29
    iget-object v0, p0, Lcom/osp/app/signin/SignUpView;->bt:Lcom/osp/app/signin/ma;

    if-eqz v0, :cond_2a

    iget-object v0, p0, Lcom/osp/app/signin/SignUpView;->bt:Lcom/osp/app/signin/ma;

    invoke-virtual {v0}, Lcom/osp/app/signin/ma;->getStatus()Landroid/os/AsyncTask$Status;

    move-result-object v0

    sget-object v1, Landroid/os/AsyncTask$Status;->RUNNING:Landroid/os/AsyncTask$Status;

    if-eq v0, v1, :cond_0

    .line 4194
    :cond_2a
    new-instance v0, Lcom/osp/app/signin/ma;

    invoke-direct {v0, p0}, Lcom/osp/app/signin/ma;-><init>(Lcom/osp/app/signin/SignUpView;)V

    iput-object v0, p0, Lcom/osp/app/signin/SignUpView;->bt:Lcom/osp/app/signin/ma;

    .line 4196
    iget-object v0, p0, Lcom/osp/app/signin/SignUpView;->bt:Lcom/osp/app/signin/ma;

    invoke-virtual {v0}, Lcom/osp/app/signin/ma;->b()V

    goto/16 :goto_0

    .line 4203
    :cond_2b
    invoke-direct {p0}, Lcom/osp/app/signin/SignUpView;->u()V

    goto/16 :goto_0

    :catch_0
    move-exception v0

    goto/16 :goto_1
.end method

.method static synthetic s(Lcom/osp/app/signin/SignUpView;)Z
    .locals 1

    .prologue
    .line 112
    iget-boolean v0, p0, Lcom/osp/app/signin/SignUpView;->bv:Z

    return v0
.end method

.method private t()Z
    .locals 13

    .prologue
    const/16 v2, 0x458

    const/16 v1, 0x457

    const/4 v5, 0x0

    const/4 v0, 0x1

    .line 4322
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v3, "prepare check begin"

    invoke-static {v3}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;)V

    .line 4324
    const/4 v3, 0x0

    .line 4326
    iput-boolean v0, p0, Lcom/osp/app/signin/SignUpView;->bd:Z

    .line 4357
    :try_start_0
    iget-object v4, p0, Lcom/osp/app/signin/SignUpView;->F:Landroid/widget/AutoCompleteTextView;

    invoke-virtual {v4}, Landroid/widget/AutoCompleteTextView;->getText()Landroid/text/Editable;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v4

    sget-object v6, Ljava/util/Locale;->ENGLISH:Ljava/util/Locale;

    invoke-virtual {v4, v6}, Ljava/lang/String;->toLowerCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v8

    .line 4358
    iget-object v4, p0, Lcom/osp/app/signin/SignUpView;->E:Landroid/widget/EditText;

    invoke-virtual {v4}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v9

    .line 4359
    iget-object v4, p0, Lcom/osp/app/signin/SignUpView;->D:Landroid/widget/EditText;

    invoke-virtual {v4}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v10

    .line 4360
    iget-object v4, p0, Lcom/osp/app/signin/SignUpView;->A:Landroid/widget/TextView;

    invoke-virtual {v4}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v4

    invoke-interface {v4}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v11

    .line 4362
    const/4 v4, -0x1

    .line 4364
    if-eqz v8, :cond_0

    const-string v6, "@"

    invoke-virtual {v8, v6}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v6

    if-eqz v6, :cond_0

    .line 4366
    const-string v4, "@"

    invoke-virtual {v8, v4}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v4

    .line 4369
    :cond_0
    if-lez v4, :cond_3

    add-int/lit8 v4, v4, 0x1

    invoke-virtual {v8}, Ljava/lang/String;->length()I

    move-result v6

    invoke-virtual {v8, v4, v6}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v4

    move-object v7, v4

    .line 4370
    :goto_0
    invoke-static {}, Lorg/apache/commons/validator/routines/a;->a()Lorg/apache/commons/validator/routines/a;

    move-result-object v4

    invoke-virtual {v4, v7}, Lorg/apache/commons/validator/routines/a;->a(Ljava/lang/String;)Z

    move-result v6

    .line 4371
    new-instance v12, Lcom/osp/security/identity/g;

    invoke-direct {v12, p0}, Lcom/osp/security/identity/g;-><init>(Landroid/content/Context;)V

    .line 4373
    invoke-static {}, Lcom/osp/device/b;->b()Lcom/osp/device/b;

    move-result-object v4

    invoke-virtual {v4}, Lcom/osp/device/b;->a()Z

    move-result v4

    if-eqz v4, :cond_4

    iget-boolean v4, p0, Lcom/osp/app/signin/SignUpView;->bl:Z

    if-eqz v4, :cond_4

    .line 4377
    invoke-static {}, Lcom/osp/common/util/i;->a()Lcom/osp/common/util/i;

    iget-object v4, p0, Lcom/osp/app/signin/SignUpView;->aj:Ljava/lang/String;

    invoke-static {v4}, Lcom/osp/common/util/i;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-static {v8, v4}, Lcom/osp/security/identity/g;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/osp/security/identity/h;

    move-result-object v4

    move v6, v0

    .line 4384
    :goto_1
    invoke-virtual {v12, v8, v9}, Lcom/osp/security/identity/g;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/osp/security/identity/h;

    move-result-object v8

    .line 4385
    invoke-static {v9, v10}, Lcom/osp/security/identity/g;->c(Ljava/lang/String;Ljava/lang/String;)Lcom/osp/security/identity/h;

    move-result-object v9

    .line 4393
    invoke-virtual {v12, v11}, Lcom/osp/security/identity/g;->a(Ljava/lang/String;)Lcom/osp/security/identity/h;

    move-result-object v10

    .line 4396
    invoke-static {}, Lcom/osp/device/b;->b()Lcom/osp/device/b;

    move-result-object v11

    invoke-virtual {v11}, Lcom/osp/device/b;->a()Z

    move-result v11

    if-eqz v11, :cond_5

    sget-object v11, Lcom/osp/security/identity/h;->i:Lcom/osp/security/identity/h;

    if-ne v4, v11, :cond_5

    .line 4398
    iget-object v0, p0, Lcom/osp/app/signin/SignUpView;->F:Landroid/widget/AutoCompleteTextView;

    invoke-virtual {v0}, Landroid/widget/AutoCompleteTextView;->requestFocus()Z

    .line 4399
    const v0, 0x7f0900ed

    invoke-virtual {p0, v0}, Lcom/osp/app/signin/SignUpView;->getText(I)Ljava/lang/CharSequence;

    move-result-object v0

    invoke-interface {v0}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    move-object v3, v0

    move v4, v5

    move v0, v1

    .line 4527
    :goto_2
    if-nez v4, :cond_2

    if-eq v0, v2, :cond_1

    if-ne v0, v1, :cond_2

    .line 4529
    :cond_1
    new-instance v0, Landroid/app/AlertDialog$Builder;

    invoke-direct {v0, p0}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    const v1, 0x7f0900ec

    invoke-virtual {p0, v1}, Lcom/osp/app/signin/SignUpView;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setTitle(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    invoke-virtual {v0, v3}, Landroid/app/AlertDialog$Builder;->setMessage(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    const v1, 0x7f090042

    new-instance v2, Lcom/osp/app/signin/lc;

    invoke-direct {v2, p0}, Lcom/osp/app/signin/lc;-><init>(Lcom/osp/app/signin/SignUpView;)V

    invoke-virtual {v0, v1, v2}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->show()Landroid/app/AlertDialog;

    .line 4540
    :cond_2
    iput-boolean v5, p0, Lcom/osp/app/signin/SignUpView;->bd:Z

    .line 4541
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "SignUpView::validateSignUpFields result : "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;)V

    .line 4543
    return v4

    .line 4369
    :cond_3
    :try_start_1
    const-string v4, ""

    move-object v7, v4

    goto/16 :goto_0

    .line 4381
    :cond_4
    const/4 v4, 0x1

    invoke-static {v8, v4}, Lcom/osp/security/identity/g;->a(Ljava/lang/String;Z)Lcom/osp/security/identity/h;

    move-result-object v4

    goto :goto_1

    .line 4401
    :cond_5
    sget-object v11, Lcom/osp/security/identity/h;->d:Lcom/osp/security/identity/h;

    if-ne v4, v11, :cond_6

    .line 4403
    iget-object v0, p0, Lcom/osp/app/signin/SignUpView;->F:Landroid/widget/AutoCompleteTextView;

    invoke-virtual {v0}, Landroid/widget/AutoCompleteTextView;->requestFocus()Z

    .line 4404
    const v0, 0x7f090032

    invoke-virtual {p0, v0}, Lcom/osp/app/signin/SignUpView;->getText(I)Ljava/lang/CharSequence;

    move-result-object v0

    invoke-interface {v0}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v0

    move-object v3, v0

    move v4, v5

    move v0, v1

    .line 4405
    goto :goto_2

    .line 4406
    :cond_6
    sget-object v11, Lcom/osp/security/identity/h;->b:Lcom/osp/security/identity/h;

    if-eq v4, v11, :cond_7

    sget-object v11, Lcom/osp/security/identity/h;->c:Lcom/osp/security/identity/h;

    if-ne v4, v11, :cond_8

    .line 4408
    :cond_7
    iget-object v0, p0, Lcom/osp/app/signin/SignUpView;->F:Landroid/widget/AutoCompleteTextView;

    invoke-virtual {v0}, Landroid/widget/AutoCompleteTextView;->requestFocus()Z

    .line 4409
    const v0, 0x7f090091

    const/4 v4, 0x2

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v6, 0x0

    const/4 v7, 0x5

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    aput-object v7, v4, v6

    const/4 v6, 0x1

    const/16 v7, 0x32

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    aput-object v7, v4, v6

    invoke-virtual {p0, v0, v4}, Lcom/osp/app/signin/SignUpView;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    move-object v3, v0

    move v4, v5

    move v0, v1

    .line 4410
    goto/16 :goto_2

    .line 4411
    :cond_8
    sget-object v11, Lcom/osp/security/identity/h;->f:Lcom/osp/security/identity/h;

    if-ne v4, v11, :cond_9

    .line 4413
    iget-object v0, p0, Lcom/osp/app/signin/SignUpView;->F:Landroid/widget/AutoCompleteTextView;

    invoke-virtual {v0}, Landroid/widget/AutoCompleteTextView;->requestFocus()Z

    .line 4414
    const v0, 0x7f090032

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v6, 0x0

    const-string v7, "admin, administrator, samsung, samsungapps, supervisor, tizenaccount, tizenstore, tizen"

    aput-object v7, v4, v6

    invoke-virtual {p0, v0, v4}, Lcom/osp/app/signin/SignUpView;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    move-object v3, v0

    move v4, v5

    move v0, v1

    .line 4415
    goto/16 :goto_2

    .line 4416
    :cond_9
    sget-object v11, Lcom/osp/security/identity/h;->e:Lcom/osp/security/identity/h;

    if-ne v4, v11, :cond_a

    .line 4418
    iget-object v0, p0, Lcom/osp/app/signin/SignUpView;->F:Landroid/widget/AutoCompleteTextView;

    invoke-virtual {v0}, Landroid/widget/AutoCompleteTextView;->requestFocus()Z

    .line 4419
    const v0, 0x7f090070

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v6, 0x0

    const-string v7, ".-_+ "

    aput-object v7, v4, v6

    invoke-virtual {p0, v0, v4}, Lcom/osp/app/signin/SignUpView;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    move-object v3, v0

    move v4, v5

    move v0, v1

    .line 4420
    goto/16 :goto_2

    .line 4426
    :cond_a
    if-nez v6, :cond_b

    .line 4428
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v4, "DomainValidator : "

    invoke-direct {v0, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v4, " / "

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-static {}, Lorg/apache/commons/validator/routines/a;->a()Lorg/apache/commons/validator/routines/a;

    move-result-object v4

    invoke-virtual {v4, v7}, Lorg/apache/commons/validator/routines/a;->a(Ljava/lang/String;)Z

    move-result v4

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;)V

    .line 4429
    iget-object v0, p0, Lcom/osp/app/signin/SignUpView;->F:Landroid/widget/AutoCompleteTextView;

    invoke-virtual {v0}, Landroid/widget/AutoCompleteTextView;->requestFocus()Z

    .line 4430
    const v0, 0x7f090032

    invoke-virtual {p0, v0}, Lcom/osp/app/signin/SignUpView;->getText(I)Ljava/lang/CharSequence;

    move-result-object v0

    invoke-interface {v0}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v0

    move-object v3, v0

    move v4, v5

    move v0, v1

    .line 4431
    goto/16 :goto_2

    .line 4432
    :cond_b
    sget-object v4, Lcom/osp/security/identity/h;->b:Lcom/osp/security/identity/h;

    if-eq v8, v4, :cond_c

    sget-object v4, Lcom/osp/security/identity/h;->c:Lcom/osp/security/identity/h;

    if-ne v8, v4, :cond_d

    .line 4434
    :cond_c
    iget-object v0, p0, Lcom/osp/app/signin/SignUpView;->E:Landroid/widget/EditText;

    invoke-virtual {v0}, Landroid/widget/EditText;->requestFocus()Z

    .line 4464
    const v0, 0x7f0900c8

    const/4 v4, 0x2

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v6, 0x0

    const/16 v7, 0x8

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    aput-object v7, v4, v6

    const/4 v6, 0x1

    const/16 v7, 0xf

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    aput-object v7, v4, v6

    invoke-virtual {p0, v0, v4}, Lcom/osp/app/signin/SignUpView;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    move-object v3, v0

    move v4, v5

    move v0, v2

    .line 4467
    goto/16 :goto_2

    .line 4468
    :cond_d
    sget-object v4, Lcom/osp/security/identity/h;->e:Lcom/osp/security/identity/h;

    if-ne v8, v4, :cond_e

    .line 4470
    iget-object v0, p0, Lcom/osp/app/signin/SignUpView;->E:Landroid/widget/EditText;

    invoke-virtual {v0}, Landroid/widget/EditText;->requestFocus()Z

    .line 4471
    invoke-virtual {p0}, Lcom/osp/app/signin/SignUpView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v4, 0x7f090142

    invoke-virtual {v0, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v6, 0x0

    const/4 v7, 0x3

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    aput-object v7, v4, v6

    invoke-static {v0, v4}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    move-object v3, v0

    move v4, v5

    move v0, v2

    .line 4473
    goto/16 :goto_2

    .line 4474
    :cond_e
    sget-object v4, Lcom/osp/security/identity/h;->f:Lcom/osp/security/identity/h;

    if-ne v8, v4, :cond_f

    .line 4476
    iget-object v0, p0, Lcom/osp/app/signin/SignUpView;->E:Landroid/widget/EditText;

    invoke-virtual {v0}, Landroid/widget/EditText;->requestFocus()Z

    .line 4479
    const v0, 0x7f090090

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v6, 0x0

    const-string v7, "~!@#$%^&*-_+=|\'\";:[]{}()<>,./?\\"

    aput-object v7, v4, v6

    invoke-virtual {p0, v0, v4}, Lcom/osp/app/signin/SignUpView;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    move-object v3, v0

    move v4, v5

    move v0, v2

    .line 4480
    goto/16 :goto_2

    .line 4481
    :cond_f
    sget-object v4, Lcom/osp/security/identity/h;->g:Lcom/osp/security/identity/h;

    if-ne v8, v4, :cond_10

    .line 4485
    const v0, 0x7f09018d

    invoke-virtual {p0, v0}, Lcom/osp/app/signin/SignUpView;->getString(I)Ljava/lang/String;

    move-result-object v0

    move-object v3, v0

    move v4, v5

    move v0, v1

    goto/16 :goto_2

    .line 4490
    :cond_10
    sget-object v4, Lcom/osp/security/identity/h;->h:Lcom/osp/security/identity/h;

    if-ne v8, v4, :cond_11

    .line 4492
    const v0, 0x7f0900c8

    const/4 v4, 0x2

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v6, 0x0

    const/16 v7, 0x8

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    aput-object v7, v4, v6

    const/4 v6, 0x1

    const/16 v7, 0xf

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    aput-object v7, v4, v6

    invoke-virtual {p0, v0, v4}, Lcom/osp/app/signin/SignUpView;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    move-object v3, v0

    move v4, v5

    move v0, v1

    goto/16 :goto_2

    .line 4494
    :cond_11
    sget-object v4, Lcom/osp/security/identity/h;->d:Lcom/osp/security/identity/h;

    if-ne v10, v4, :cond_12

    .line 4496
    iget-object v0, p0, Lcom/osp/app/signin/SignUpView;->y:Landroid/widget/LinearLayout;

    invoke-virtual {v0}, Landroid/widget/LinearLayout;->requestFocus()Z

    .line 4497
    const v0, 0x7f090031

    invoke-virtual {p0, v0}, Lcom/osp/app/signin/SignUpView;->getText(I)Ljava/lang/CharSequence;

    move-result-object v0

    invoke-interface {v0}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v0

    move-object v3, v0

    move v4, v5

    move v0, v1

    .line 4498
    goto/16 :goto_2

    .line 4499
    :cond_12
    sget-object v4, Lcom/osp/security/identity/h;->d:Lcom/osp/security/identity/h;
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    if-ne v9, v4, :cond_13

    move v4, v0

    move v0, v1

    .line 4508
    goto/16 :goto_2

    :cond_13
    move v4, v0

    move v0, v1

    .line 4518
    goto/16 :goto_2

    .line 4515
    :catch_0
    move-exception v0

    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    move v0, v1

    move v4, v5

    goto/16 :goto_2
.end method

.method static synthetic t(Lcom/osp/app/signin/SignUpView;)Z
    .locals 1

    .prologue
    .line 112
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/osp/app/signin/SignUpView;->bv:Z

    return v0
.end method

.method static synthetic u(Lcom/osp/app/signin/SignUpView;)Landroid/app/AlertDialog;
    .locals 1

    .prologue
    .line 112
    iget-object v0, p0, Lcom/osp/app/signin/SignUpView;->af:Landroid/app/AlertDialog;

    return-object v0
.end method

.method private u()V
    .locals 9

    .prologue
    const/4 v8, 0x1

    .line 6723
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v0, "AccountView::ShowTnCActivity isCancelableJustOneActivity : true"

    invoke-static {v0}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;)V

    .line 6727
    iget-boolean v0, p0, Lcom/osp/app/signin/SignUpView;->U:Z

    if-eqz v0, :cond_0

    .line 6729
    iget-object v0, p0, Lcom/osp/app/signin/SignUpView;->aq:Lcom/osp/app/signin/SignUpinfo;

    invoke-virtual {v0}, Lcom/osp/app/signin/SignUpinfo;->a()V

    .line 6732
    :cond_0
    iget-object v0, p0, Lcom/osp/app/signin/SignUpView;->aq:Lcom/osp/app/signin/SignUpinfo;

    if-nez v0, :cond_5

    .line 6734
    iget-object v0, p0, Lcom/osp/app/signin/SignUpView;->aS:Ljava/lang/String;

    .line 6746
    :goto_0
    const/4 v1, 0x0

    iput v1, p0, Lcom/osp/app/signin/SignUpView;->aW:I

    .line 6748
    new-instance v2, Landroid/content/Intent;

    invoke-direct {v2}, Landroid/content/Intent;-><init>()V

    .line 6749
    const-class v1, Lcom/osp/app/signin/TnCView;

    invoke-virtual {v2, p0, v1}, Landroid/content/Intent;->setClass(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;

    .line 6751
    const-string v1, "key_is_name_verified"

    iget-boolean v3, p0, Lcom/osp/app/signin/SignUpView;->U:Z

    invoke-virtual {v2, v1, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 6752
    const-string v1, "key_name_check_familyname"

    iget-object v3, p0, Lcom/osp/app/signin/SignUpView;->aD:Ljava/lang/String;

    invoke-virtual {v2, v1, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 6753
    const-string v1, "key_name_check_givenname"

    iget-object v3, p0, Lcom/osp/app/signin/SignUpView;->aE:Ljava/lang/String;

    invoke-virtual {v2, v1, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 6754
    const-string v1, "key_name_check_birthdate"

    iget-object v3, p0, Lcom/osp/app/signin/SignUpView;->aF:Ljava/lang/String;

    invoke-virtual {v2, v1, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 6755
    const-string v1, "key_name_check_mobile"

    iget-object v3, p0, Lcom/osp/app/signin/SignUpView;->aG:Ljava/lang/String;

    invoke-virtual {v2, v1, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 6756
    const-string v1, "key_name_check_method"

    iget-object v3, p0, Lcom/osp/app/signin/SignUpView;->aI:Ljava/lang/String;

    invoke-virtual {v2, v1, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 6757
    const-string v1, "key_name_check_ci"

    iget-object v3, p0, Lcom/osp/app/signin/SignUpView;->aJ:Ljava/lang/String;

    invoke-virtual {v2, v1, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 6758
    const-string v1, "key_name_check_di"

    iget-object v3, p0, Lcom/osp/app/signin/SignUpView;->aK:Ljava/lang/String;

    invoke-virtual {v2, v1, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 6759
    const-string v1, "key_name_check_datetime"

    iget-object v3, p0, Lcom/osp/app/signin/SignUpView;->aL:Ljava/lang/String;

    invoke-virtual {v2, v1, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 6760
    const-string v1, "key_name_check_gender"

    iget-object v3, p0, Lcom/osp/app/signin/SignUpView;->aM:Ljava/lang/String;

    invoke-virtual {v2, v1, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 6761
    const-string v1, "key_name_check_foreigner"

    iget-object v3, p0, Lcom/osp/app/signin/SignUpView;->aN:Ljava/lang/String;

    invoke-virtual {v2, v1, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 6763
    const-string v1, "client_id"

    iget-object v3, p0, Lcom/osp/app/signin/SignUpView;->ag:Ljava/lang/String;

    invoke-virtual {v2, v1, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 6764
    const-string v1, "client_secret"

    iget-object v3, p0, Lcom/osp/app/signin/SignUpView;->ah:Ljava/lang/String;

    invoke-virtual {v2, v1, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 6765
    const-string v1, "account_mode"

    iget-object v3, p0, Lcom/osp/app/signin/SignUpView;->W:Ljava/lang/String;

    invoke-virtual {v2, v1, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 6766
    const-string v1, "service_name"

    iget-object v3, p0, Lcom/osp/app/signin/SignUpView;->aU:Ljava/lang/String;

    invoke-virtual {v2, v1, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 6767
    const-string v1, "OSP_VER"

    iget-object v3, p0, Lcom/osp/app/signin/SignUpView;->an:Ljava/lang/String;

    invoke-virtual {v2, v1, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 6768
    const-string v3, "email_id"

    iget-object v1, p0, Lcom/osp/app/signin/SignUpView;->aq:Lcom/osp/app/signin/SignUpinfo;

    if-eqz v1, :cond_6

    iget-object v1, p0, Lcom/osp/app/signin/SignUpView;->aq:Lcom/osp/app/signin/SignUpinfo;

    invoke-virtual {v1}, Lcom/osp/app/signin/SignUpinfo;->e()Ljava/lang/String;

    move-result-object v1

    :goto_1
    invoke-virtual {v2, v3, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 6769
    const-string v1, "key_str_signup_info"

    invoke-virtual {v2, v1, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 6770
    const-string v0, "authcode"

    iget-object v1, p0, Lcom/osp/app/signin/SignUpView;->as:Ljava/lang/String;

    invoke-virtual {v2, v0, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 6771
    const-string v0, "key_internal_sign_up_inforamtion"

    iget-object v1, p0, Lcom/osp/app/signin/SignUpView;->aq:Lcom/osp/app/signin/SignUpinfo;

    invoke-virtual {v2, v0, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 6772
    const-string v0, "key_google_account"

    iget-boolean v1, p0, Lcom/osp/app/signin/SignUpView;->aT:Z

    invoke-virtual {v2, v0, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 6773
    const-string v0, "key_facebook_account"

    iget-boolean v1, p0, Lcom/osp/app/signin/SignUpView;->aY:Z

    invoke-virtual {v2, v0, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 6774
    const-string v0, "key_weibo_account"

    iget-boolean v1, p0, Lcom/osp/app/signin/SignUpView;->bf:Z

    invoke-virtual {v2, v0, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 6775
    const-string v0, "key_account_manager_account"

    iget-boolean v1, p0, Lcom/osp/app/signin/SignUpView;->bn:Z

    invoke-virtual {v2, v0, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 6776
    const-string v0, "key_easy_signup_tnc_mode"

    iget-boolean v1, p0, Lcom/osp/app/signin/SignUpView;->ak:Z

    invoke-virtual {v2, v0, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 6783
    const-string v0, "country_code_mcc"

    iget-object v1, p0, Lcom/osp/app/signin/SignUpView;->aj:Ljava/lang/String;

    invoke-virtual {v2, v0, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 6784
    iget-boolean v0, p0, Lcom/osp/app/signin/SignUpView;->aQ:Z

    if-eqz v0, :cond_1

    .line 6786
    const-string v0, "new_add_account_mode"

    invoke-virtual {v2, v0, v8}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 6788
    :cond_1
    const-string v0, "BG_mode"

    iget-object v1, p0, Lcom/osp/app/signin/SignUpView;->aV:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 6790
    const-string v0, "BG_WhoareU"

    iget-object v1, p0, Lcom/osp/app/signin/SignUpView;->aV:Ljava/lang/String;

    invoke-virtual {v2, v0, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 6791
    iget-object v0, p0, Lcom/osp/app/signin/SignUpView;->ar:Ljava/lang/String;

    if-eqz v0, :cond_2

    .line 6793
    const-string v0, "mypackage"

    iget-object v1, p0, Lcom/osp/app/signin/SignUpView;->ar:Ljava/lang/String;

    invoke-virtual {v2, v0, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 6794
    const-string v0, "key_request_id"

    iget-wide v4, p0, Lcom/osp/app/signin/SignUpView;->aR:J

    invoke-virtual {v2, v0, v4, v5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;J)Landroid/content/Intent;

    .line 6797
    :cond_2
    const-string v0, "MODE"

    iget-object v1, p0, Lcom/osp/app/signin/SignUpView;->ao:Ljava/lang/String;

    invoke-virtual {v2, v0, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 6798
    const-string v0, "serviceApp_type"

    iget v1, p0, Lcom/osp/app/signin/SignUpView;->aW:I

    invoke-virtual {v2, v0, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 6800
    const-string v0, "is_cancelable_just_one_activity"

    invoke-virtual {v2, v0, v8}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 6803
    const-string v0, "is_signup_flow"

    invoke-virtual {v2, v0, v8}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 6807
    iget-boolean v0, p0, Lcom/osp/app/signin/SignUpView;->bk:Z

    if-eqz v0, :cond_3

    .line 6809
    const-string v0, "key_welcome_content"

    iget-object v1, p0, Lcom/osp/app/signin/SignUpView;->bj:Ljava/lang/String;

    invoke-virtual {v2, v0, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 6810
    const-string v0, "key_marketingpopup_mode"

    invoke-virtual {v2, v0, v8}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 6813
    :cond_3
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "request Tnc - client_id : "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v1, p0, Lcom/osp/app/signin/SignUpView;->ag:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;)V

    .line 6814
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "request Tnc - client_secret : "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v1, p0, Lcom/osp/app/signin/SignUpView;->ah:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;)V

    .line 6815
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "request Tnc - account_mode : "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v1, p0, Lcom/osp/app/signin/SignUpView;->W:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;)V

    .line 6816
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "request Tnc - service_name : "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v1, p0, Lcom/osp/app/signin/SignUpView;->aU:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;)V

    .line 6817
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "request Tnc - OSP_VER : "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v1, p0, Lcom/osp/app/signin/SignUpView;->an:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;)V

    .line 6818
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v0, "request Tnc - new_add_account_mode : true"

    invoke-static {v0}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;)V

    .line 6819
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "request Tnc - BG_WhoareU : "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v1, p0, Lcom/osp/app/signin/SignUpView;->aV:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;)V

    .line 6820
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "request Tnc - mypackage : "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v1, p0, Lcom/osp/app/signin/SignUpView;->ar:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;)V

    .line 6821
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "request Tnc - key_request_id : "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-wide v4, p0, Lcom/osp/app/signin/SignUpView;->aR:J

    invoke-virtual {v0, v4, v5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;)V

    .line 6822
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "request Tnc - MODE : "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v1, p0, Lcom/osp/app/signin/SignUpView;->ao:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;)V

    .line 6823
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "request Tnc - serviceApp_type : "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v1, p0, Lcom/osp/app/signin/SignUpView;->aW:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;)V

    .line 6824
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v0, "request Tnc - is_cancelable_just_one_activity : true"

    invoke-static {v0}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;)V

    .line 6825
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v0, "request Tnc - is_signup_flow : true"

    invoke-static {v0}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;)V

    .line 6838
    iget-object v0, p0, Lcom/osp/app/signin/SignUpView;->ai:Landroid/content/Intent;

    const-string v1, "stay_duration"

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v4

    iget-wide v6, p0, Lcom/osp/app/signin/SignUpView;->br:J

    sub-long/2addr v4, v6

    invoke-virtual {v0, v1, v4, v5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;J)Landroid/content/Intent;

    .line 6839
    iget-object v0, p0, Lcom/osp/app/signin/SignUpView;->bs:Ljava/lang/String;

    if-eqz v0, :cond_4

    .line 6841
    iget-object v0, p0, Lcom/osp/app/signin/SignUpView;->ai:Landroid/content/Intent;

    const-string v1, "previous_activity"

    iget-object v3, p0, Lcom/osp/app/signin/SignUpView;->bs:Ljava/lang/String;

    invoke-virtual {v0, v1, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 6843
    :cond_4
    new-instance v0, Lcom/osp/app/bigdatalog/c;

    invoke-virtual {p0}, Lcom/osp/app/signin/SignUpView;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    const/4 v3, 0x2

    iget-object v4, p0, Lcom/osp/app/signin/SignUpView;->ai:Landroid/content/Intent;

    invoke-direct {v0, v1, v8, v3, v4}, Lcom/osp/app/bigdatalog/c;-><init>(Landroid/content/Context;IILandroid/content/Intent;)V

    .line 6844
    invoke-static {}, Lcom/osp/app/bigdatalog/e;->a()Lcom/osp/app/bigdatalog/e;

    move-result-object v1

    invoke-virtual {p0}, Lcom/osp/app/signin/SignUpView;->getApplicationContext()Landroid/content/Context;

    move-result-object v3

    invoke-virtual {v1, v3}, Lcom/osp/app/bigdatalog/e;->a(Landroid/content/Context;)V

    .line 6845
    invoke-static {}, Lcom/osp/app/bigdatalog/e;->a()Lcom/osp/app/bigdatalog/e;

    move-result-object v1

    invoke-virtual {p0}, Lcom/osp/app/signin/SignUpView;->getApplicationContext()Landroid/content/Context;

    move-result-object v3

    invoke-virtual {v1, v3, v0}, Lcom/osp/app/bigdatalog/e;->a(Landroid/content/Context;Lcom/osp/app/bigdatalog/c;)V

    .line 6847
    const-string v0, "from_sign_up"

    invoke-virtual {v2, v0, v8}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 6850
    const/16 v0, 0xce

    invoke-virtual {p0, v2, v0}, Lcom/osp/app/signin/SignUpView;->startActivityForResult(Landroid/content/Intent;I)V

    .line 6851
    return-void

    .line 6737
    :cond_5
    const-string v0, "SA_EMAIL_;SA_COUNTRY_;SA_BIRTHDATE_;SA_EMAILRECEIVE_;"

    goto/16 :goto_0

    .line 6768
    :cond_6
    const/4 v1, 0x0

    goto/16 :goto_1
.end method

.method static synthetic v(Lcom/osp/app/signin/SignUpView;)Lcom/msc/a/j;
    .locals 1

    .prologue
    .line 112
    iget-object v0, p0, Lcom/osp/app/signin/SignUpView;->bq:Lcom/msc/a/j;

    return-object v0
.end method

.method private v()V
    .locals 2

    .prologue
    .line 7134
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v0, "startShowSignup"

    invoke-static {v0}, Lcom/osp/app/util/aq;->b(Ljava/lang/String;)V

    .line 7139
    iget-object v0, p0, Lcom/osp/app/signin/SignUpView;->aj:Ljava/lang/String;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/osp/app/signin/SignUpView;->aj:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v0

    if-lez v0, :cond_0

    .line 7141
    new-instance v0, Lcom/osp/app/signin/lz;

    invoke-direct {v0, p0}, Lcom/osp/app/signin/lz;-><init>(Lcom/osp/app/signin/SignUpView;)V

    iput-object v0, p0, Lcom/osp/app/signin/SignUpView;->bA:Lcom/osp/app/signin/lz;

    .line 7143
    iget-object v0, p0, Lcom/osp/app/signin/SignUpView;->bA:Lcom/osp/app/signin/lz;

    invoke-virtual {v0}, Lcom/osp/app/signin/lz;->b()V

    .line 7150
    :goto_0
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "MCC : "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v1, p0, Lcom/osp/app/signin/SignUpView;->aj:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/osp/app/util/aq;->b(Ljava/lang/String;)V

    .line 7152
    return-void

    .line 7147
    :cond_0
    invoke-direct {p0}, Lcom/osp/app/signin/SignUpView;->w()Ljava/lang/String;

    goto :goto_0
.end method

.method static synthetic w(Lcom/osp/app/signin/SignUpView;)Landroid/widget/EditText;
    .locals 1

    .prologue
    .line 112
    iget-object v0, p0, Lcom/osp/app/signin/SignUpView;->T:Landroid/widget/EditText;

    return-object v0
.end method

.method private w()Ljava/lang/String;
    .locals 3

    .prologue
    .line 7168
    iget-object v0, p0, Lcom/osp/app/signin/SignUpView;->aj:Ljava/lang/String;

    if-eqz v0, :cond_0

    .line 7170
    iget-object v0, p0, Lcom/osp/app/signin/SignUpView;->aj:Ljava/lang/String;

    invoke-virtual {p0}, Lcom/osp/app/signin/SignUpView;->getApplicationContext()Landroid/content/Context;

    iget-object v1, p0, Lcom/osp/app/signin/SignUpView;->aj:Ljava/lang/String;

    invoke-static {v1}, Lcom/osp/common/util/i;->d(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-static {p0, v0, v1}, Lcom/msc/c/e;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)V

    .line 7171
    new-instance v0, Lcom/osp/app/signin/lz;

    invoke-direct {v0, p0}, Lcom/osp/app/signin/lz;-><init>(Lcom/osp/app/signin/SignUpView;)V

    iput-object v0, p0, Lcom/osp/app/signin/SignUpView;->bA:Lcom/osp/app/signin/lz;

    .line 7173
    iget-object v0, p0, Lcom/osp/app/signin/SignUpView;->bA:Lcom/osp/app/signin/lz;

    invoke-virtual {v0}, Lcom/osp/app/signin/lz;->b()V

    .line 7174
    iget-object v0, p0, Lcom/osp/app/signin/SignUpView;->aj:Ljava/lang/String;

    .line 7208
    :goto_0
    return-object v0

    .line 7177
    :cond_0
    invoke-static {}, Lcom/osp/common/util/i;->a()Lcom/osp/common/util/i;

    invoke-static {p0}, Lcom/osp/common/util/i;->e(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 7182
    :try_start_0
    invoke-static {}, Lcom/osp/common/util/i;->a()Lcom/osp/common/util/i;

    invoke-static {p0}, Lcom/osp/common/util/i;->h(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/osp/app/signin/SignUpView;->aj:Ljava/lang/String;

    .line 7183
    iget-object v0, p0, Lcom/osp/app/signin/SignUpView;->aj:Ljava/lang/String;

    invoke-virtual {p0}, Lcom/osp/app/signin/SignUpView;->getApplicationContext()Landroid/content/Context;

    iget-object v1, p0, Lcom/osp/app/signin/SignUpView;->aj:Ljava/lang/String;

    invoke-static {v1}, Lcom/osp/common/util/i;->d(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-static {p0, v0, v1}, Lcom/msc/c/e;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)V

    .line 7185
    new-instance v0, Lcom/osp/app/signin/lz;

    invoke-direct {v0, p0}, Lcom/osp/app/signin/lz;-><init>(Lcom/osp/app/signin/SignUpView;)V

    iput-object v0, p0, Lcom/osp/app/signin/SignUpView;->bA:Lcom/osp/app/signin/lz;

    .line 7187
    iget-object v0, p0, Lcom/osp/app/signin/SignUpView;->bA:Lcom/osp/app/signin/lz;

    invoke-virtual {v0}, Lcom/osp/app/signin/lz;->b()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 7194
    iget-object v0, p0, Lcom/osp/app/signin/SignUpView;->aj:Ljava/lang/String;

    if-nez v0, :cond_1

    .line 7196
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v0, "SUV"

    const-string v1, "The sim state is ready but mcc is null!! This device is abnormal. We will make mcc from locale settings."

    invoke-static {v0, v1}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 7198
    :goto_1
    invoke-direct {p0}, Lcom/osp/app/signin/SignUpView;->x()V

    .line 7208
    :cond_1
    :goto_2
    iget-object v0, p0, Lcom/osp/app/signin/SignUpView;->aj:Ljava/lang/String;

    goto :goto_0

    .line 7189
    :catch_0
    move-exception v0

    :try_start_1
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 7194
    iget-object v0, p0, Lcom/osp/app/signin/SignUpView;->aj:Ljava/lang/String;

    if-nez v0, :cond_1

    .line 7196
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v0, "SUV"

    const-string v1, "The sim state is ready but mcc is null!! This device is abnormal. We will make mcc from locale settings."

    invoke-static {v0, v1}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1

    .line 7194
    :catchall_0
    move-exception v0

    iget-object v1, p0, Lcom/osp/app/signin/SignUpView;->aj:Ljava/lang/String;

    if-nez v1, :cond_2

    .line 7196
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v1, "SUV"

    const-string v2, "The sim state is ready but mcc is null!! This device is abnormal. We will make mcc from locale settings."

    invoke-static {v1, v2}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 7198
    invoke-direct {p0}, Lcom/osp/app/signin/SignUpView;->x()V

    .line 7194
    :cond_2
    throw v0

    .line 7205
    :cond_3
    invoke-direct {p0}, Lcom/osp/app/signin/SignUpView;->x()V

    goto :goto_2
.end method

.method private x()V
    .locals 1

    .prologue
    .line 7217
    new-instance v0, Lcom/osp/app/signin/ly;

    invoke-direct {v0, p0}, Lcom/osp/app/signin/ly;-><init>(Lcom/osp/app/signin/SignUpView;)V

    .line 7219
    invoke-virtual {v0}, Lcom/osp/app/signin/ly;->b()V

    .line 7221
    return-void
.end method

.method static synthetic x(Lcom/osp/app/signin/SignUpView;)V
    .locals 0

    .prologue
    .line 112
    invoke-direct {p0}, Lcom/osp/app/signin/SignUpView;->q()V

    return-void
.end method

.method private y()V
    .locals 11

    .prologue
    const/4 v1, 0x0

    .line 8880
    iget-object v0, p0, Lcom/osp/app/signin/SignUpView;->bm:Ljava/util/ArrayList;

    if-nez v0, :cond_0

    .line 8882
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/osp/app/signin/SignUpView;->bm:Ljava/util/ArrayList;

    .line 8888
    :goto_0
    invoke-static {}, Lcom/osp/app/util/r;->r()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 8891
    const/4 v0, 0x4

    new-array v3, v0, [Ljava/lang/String;

    const-string v0, "com.google"

    aput-object v0, v3, v1

    const/4 v0, 0x1

    const-string v2, "com.facebook.auth.login"

    aput-object v2, v3, v0

    const/4 v0, 0x2

    const-string v2, "com.android.email"

    aput-object v2, v3, v0

    const/4 v0, 0x3

    const-string v2, "com.android.exchange"

    aput-object v2, v3, v0

    .line 8892
    invoke-static {p0}, Landroid/accounts/AccountManager;->get(Landroid/content/Context;)Landroid/accounts/AccountManager;

    move-result-object v4

    .line 8894
    new-instance v5, Ljava/util/ArrayList;

    invoke-direct {v5}, Ljava/util/ArrayList;-><init>()V

    .line 8896
    array-length v6, v3

    move v2, v1

    :goto_1
    if-ge v2, v6, :cond_2

    aget-object v0, v3, v2

    .line 8907
    invoke-virtual {v4, v0}, Landroid/accounts/AccountManager;->getAccountsByType(Ljava/lang/String;)[Landroid/accounts/Account;

    move-result-object v7

    array-length v8, v7

    move v0, v1

    :goto_2
    if-ge v0, v8, :cond_1

    aget-object v9, v7, v0

    .line 8916
    iget-object v10, p0, Lcom/osp/app/signin/SignUpView;->bm:Ljava/util/ArrayList;

    invoke-virtual {v10, v9}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 8917
    iget-object v9, v9, Landroid/accounts/Account;->name:Ljava/lang/String;

    invoke-virtual {v5, v9}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 8907
    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    .line 8885
    :cond_0
    iget-object v0, p0, Lcom/osp/app/signin/SignUpView;->bm:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    goto :goto_0

    .line 8896
    :cond_1
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_1

    .line 8921
    :cond_2
    iget-object v0, p0, Lcom/osp/app/signin/SignUpView;->F:Landroid/widget/AutoCompleteTextView;

    if-eqz v0, :cond_3

    .line 8923
    new-instance v0, Landroid/widget/ArrayAdapter;

    const v1, 0x109000a

    invoke-direct {v0, p0, v1, v5}, Landroid/widget/ArrayAdapter;-><init>(Landroid/content/Context;ILjava/util/List;)V

    .line 8924
    iget-object v1, p0, Lcom/osp/app/signin/SignUpView;->F:Landroid/widget/AutoCompleteTextView;

    invoke-virtual {v1, v0}, Landroid/widget/AutoCompleteTextView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 8925
    iget-object v0, p0, Lcom/osp/app/signin/SignUpView;->F:Landroid/widget/AutoCompleteTextView;

    const/16 v1, 0x3e8

    invoke-virtual {v0, v1}, Landroid/widget/AutoCompleteTextView;->setThreshold(I)V

    .line 8928
    :cond_3
    return-void
.end method

.method static synthetic y(Lcom/osp/app/signin/SignUpView;)Z
    .locals 1

    .prologue
    .line 112
    iget-boolean v0, p0, Lcom/osp/app/signin/SignUpView;->X:Z

    return v0
.end method

.method private z()Z
    .locals 2

    .prologue
    .line 8954
    invoke-static {p0}, Landroid/accounts/AccountManager;->get(Landroid/content/Context;)Landroid/accounts/AccountManager;

    move-result-object v0

    .line 8955
    const-string v1, "com.google"

    invoke-virtual {v0, v1}, Landroid/accounts/AccountManager;->getAccountsByType(Ljava/lang/String;)[Landroid/accounts/Account;

    move-result-object v0

    .line 8957
    if-eqz v0, :cond_0

    array-length v0, v0

    if-lez v0, :cond_0

    .line 8959
    const/4 v0, 0x1

    .line 8961
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method static synthetic z(Lcom/osp/app/signin/SignUpView;)Z
    .locals 1

    .prologue
    .line 112
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/osp/app/signin/SignUpView;->X:Z

    return v0
.end method


# virtual methods
.method protected final a()V
    .locals 8

    .prologue
    const v7, 0x7f0c0198

    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 3578
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v0, "SignUpView::SignUpButton_Check"

    invoke-static {v0}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;)V

    .line 3579
    iget-object v0, p0, Lcom/osp/app/signin/SignUpView;->F:Landroid/widget/AutoCompleteTextView;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/osp/app/signin/SignUpView;->F:Landroid/widget/AutoCompleteTextView;

    invoke-virtual {v0}, Landroid/widget/AutoCompleteTextView;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-interface {v0}, Landroid/text/Editable;->length()I

    move-result v0

    if-eqz v0, :cond_7

    :cond_0
    iget-object v0, p0, Lcom/osp/app/signin/SignUpView;->E:Landroid/widget/EditText;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/osp/app/signin/SignUpView;->E:Landroid/widget/EditText;

    invoke-virtual {v0}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-interface {v0}, Landroid/text/Editable;->length()I

    move-result v0

    if-eqz v0, :cond_7

    :cond_1
    iget-object v0, p0, Lcom/osp/app/signin/SignUpView;->A:Landroid/widget/TextView;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/osp/app/signin/SignUpView;->A:Landroid/widget/TextView;

    invoke-virtual {v0}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v0

    invoke-interface {v0}, Ljava/lang/CharSequence;->length()I

    move-result v0

    if-eqz v0, :cond_7

    :cond_2
    iget-object v0, p0, Lcom/osp/app/signin/SignUpView;->I:Landroid/widget/EditText;

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/osp/app/signin/SignUpView;->I:Landroid/widget/EditText;

    invoke-virtual {v0}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-interface {v0}, Landroid/text/Editable;->length()I

    move-result v0

    if-eqz v0, :cond_7

    :cond_3
    iget-object v0, p0, Lcom/osp/app/signin/SignUpView;->J:Landroid/widget/EditText;

    if-eqz v0, :cond_4

    iget-object v0, p0, Lcom/osp/app/signin/SignUpView;->J:Landroid/widget/EditText;

    invoke-virtual {v0}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-interface {v0}, Landroid/text/Editable;->length()I

    move-result v0

    if-eqz v0, :cond_7

    :cond_4
    iget-object v0, p0, Lcom/osp/app/signin/SignUpView;->bB:Lcom/osp/app/signin/kd;

    if-eqz v0, :cond_5

    iget-object v0, p0, Lcom/osp/app/signin/SignUpView;->bB:Lcom/osp/app/signin/kd;

    invoke-virtual {v0}, Lcom/osp/app/signin/kd;->n()Z

    move-result v0

    if-eqz v0, :cond_5

    iget-object v0, p0, Lcom/osp/app/signin/SignUpView;->L:Landroid/widget/EditText;

    if-eqz v0, :cond_5

    iget-object v0, p0, Lcom/osp/app/signin/SignUpView;->L:Landroid/widget/EditText;

    invoke-virtual {v0}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-interface {v0}, Landroid/text/Editable;->length()I

    move-result v0

    const/4 v3, 0x5

    if-ne v0, v3, :cond_7

    :cond_5
    iget-object v0, p0, Lcom/osp/app/signin/SignUpView;->be:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v4

    move v3, v1

    :goto_0
    if-ge v3, v4, :cond_f

    iget-object v0, p0, Lcom/osp/app/signin/SignUpView;->be:Ljava/util/ArrayList;

    invoke-virtual {v0, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/widget/EditText;

    invoke-virtual {v0}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    new-instance v5, Ljava/util/StringTokenizer;

    invoke-virtual {v0}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v5, v0}, Ljava/util/StringTokenizer;-><init>(Ljava/lang/String;)V

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    :goto_1
    invoke-virtual {v5}, Ljava/util/StringTokenizer;->hasMoreTokens()Z

    move-result v6

    if-eqz v6, :cond_6

    invoke-virtual {v5}, Ljava/util/StringTokenizer;->nextToken()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v0, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_1

    :cond_6
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "isFieldHaveData : "

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v0

    if-nez v0, :cond_a

    move v0, v1

    :goto_2
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "SignUpView::isFieldHavedata result : "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;)V

    if-eqz v0, :cond_7

    invoke-static {}, Lcom/osp/device/b;->b()Lcom/osp/device/b;

    move-result-object v0

    invoke-virtual {v0}, Lcom/osp/device/b;->a()Z

    move-result v0

    if-eqz v0, :cond_c

    iget-boolean v0, p0, Lcom/osp/app/signin/SignUpView;->bl:Z

    if-eqz v0, :cond_c

    iget-object v0, p0, Lcom/osp/app/signin/SignUpView;->bq:Lcom/msc/a/j;

    if-eqz v0, :cond_7

    iget-object v0, p0, Lcom/osp/app/signin/SignUpView;->bq:Lcom/msc/a/j;

    invoke-virtual {v0}, Lcom/msc/a/j;->b()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_7

    iget-object v0, p0, Lcom/osp/app/signin/SignUpView;->T:Landroid/widget/EditText;

    invoke-virtual {v0}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_c

    .line 3591
    :cond_7
    iget-object v0, p0, Lcom/osp/app/signin/SignUpView;->x:Landroid/view/Menu;

    if-eqz v0, :cond_8

    .line 3593
    iget-object v0, p0, Lcom/osp/app/signin/SignUpView;->x:Landroid/view/Menu;

    invoke-interface {v0, v7}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v0

    invoke-interface {v0, v1}, Landroid/view/MenuItem;->setEnabled(Z)Landroid/view/MenuItem;

    .line 3595
    :cond_8
    iget-object v0, p0, Lcom/osp/app/signin/SignUpView;->bx:Lcom/osp/app/signin/BottomSoftkeyLinearLayout;

    if-eqz v0, :cond_9

    .line 3597
    invoke-static {}, Lcom/osp/device/b;->b()Lcom/osp/device/b;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/osp/device/b;->l(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_b

    .line 3599
    iget-object v0, p0, Lcom/osp/app/signin/SignUpView;->bx:Lcom/osp/app/signin/BottomSoftkeyLinearLayout;

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/osp/app/signin/BottomSoftkeyLinearLayout;->a(Ljava/lang/Boolean;)V

    .line 3701
    :cond_9
    :goto_3
    return-void

    .line 3579
    :cond_a
    add-int/lit8 v0, v3, 0x1

    move v3, v0

    goto/16 :goto_0

    .line 3602
    :cond_b
    iget-object v0, p0, Lcom/osp/app/signin/SignUpView;->bx:Lcom/osp/app/signin/BottomSoftkeyLinearLayout;

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/osp/app/signin/BottomSoftkeyLinearLayout;->b(Ljava/lang/Boolean;)V

    goto :goto_3

    .line 3607
    :cond_c
    iget-object v0, p0, Lcom/osp/app/signin/SignUpView;->x:Landroid/view/Menu;

    if-eqz v0, :cond_d

    .line 3609
    iget-object v0, p0, Lcom/osp/app/signin/SignUpView;->x:Landroid/view/Menu;

    invoke-interface {v0, v7}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v0

    invoke-interface {v0, v2}, Landroid/view/MenuItem;->setEnabled(Z)Landroid/view/MenuItem;

    .line 3611
    :cond_d
    iget-object v0, p0, Lcom/osp/app/signin/SignUpView;->bx:Lcom/osp/app/signin/BottomSoftkeyLinearLayout;

    if-eqz v0, :cond_9

    .line 3613
    invoke-static {}, Lcom/osp/device/b;->b()Lcom/osp/device/b;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/osp/device/b;->l(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_e

    .line 3615
    iget-object v0, p0, Lcom/osp/app/signin/SignUpView;->bx:Lcom/osp/app/signin/BottomSoftkeyLinearLayout;

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/osp/app/signin/BottomSoftkeyLinearLayout;->a(Ljava/lang/Boolean;)V

    goto :goto_3

    .line 3618
    :cond_e
    iget-object v0, p0, Lcom/osp/app/signin/SignUpView;->bx:Lcom/osp/app/signin/BottomSoftkeyLinearLayout;

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/osp/app/signin/BottomSoftkeyLinearLayout;->b(Ljava/lang/Boolean;)V

    goto :goto_3

    :cond_f
    move v0, v2

    goto/16 :goto_2
.end method

.method public final a(Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 5774
    new-instance v0, Landroid/content/Intent;

    const-string v1, "com.msc.action.samsungaccount.web_with_close_button"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 5775
    const-string v1, "ServerUrl"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 5776
    invoke-virtual {p0, v0}, Lcom/osp/app/signin/SignUpView;->startActivity(Landroid/content/Intent;)V

    .line 5777
    return-void
.end method

.method protected final b()V
    .locals 0

    .prologue
    .line 6710
    return-void
.end method

.method public final b(Ljava/lang/String;)V
    .locals 3

    .prologue
    .line 6267
    new-instance v1, Ljava/lang/StringBuffer;

    invoke-direct {v1}, Ljava/lang/StringBuffer;-><init>()V

    .line 6268
    invoke-static {}, Lcom/osp/device/b;->b()Lcom/osp/device/b;

    move-result-object v0

    invoke-virtual {v0}, Lcom/osp/device/b;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-static {p1}, Landroid/telephony/PhoneNumberUtils;->isGlobalPhoneNumber(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 6270
    const v0, 0x7f0900f3

    invoke-virtual {p0, v0}, Lcom/osp/app/signin/SignUpView;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 6275
    :goto_0
    const-string v0, "<br><br><b>"

    invoke-virtual {v1, v0}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 6276
    invoke-virtual {v1, p1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 6277
    const-string v0, "</b><br><br>"

    invoke-virtual {v1, v0}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 6278
    invoke-static {}, Lcom/osp/device/b;->b()Lcom/osp/device/b;

    move-result-object v0

    invoke-virtual {v0}, Lcom/osp/device/b;->a()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-static {p1}, Landroid/telephony/PhoneNumberUtils;->isGlobalPhoneNumber(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 6281
    const v0, 0x7f0900f2

    invoke-virtual {p0, v0}, Lcom/osp/app/signin/SignUpView;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 6282
    const v0, 0x7f0900ec

    .line 6289
    :goto_1
    new-instance v2, Landroid/app/AlertDialog$Builder;

    invoke-direct {v2, p0}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    invoke-virtual {v2, v0}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    invoke-virtual {v1}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Landroid/text/Html;->fromHtml(Ljava/lang/String;)Landroid/text/Spanned;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setMessage(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    const v1, 0x7f090064

    new-instance v2, Lcom/osp/app/signin/lk;

    invoke-direct {v2, p0, p1}, Lcom/osp/app/signin/lk;-><init>(Lcom/osp/app/signin/SignUpView;Ljava/lang/String;)V

    invoke-virtual {v0, v1, v2}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v0

    .line 6300
    invoke-virtual {v0}, Landroid/app/AlertDialog;->show()V

    .line 6301
    return-void

    .line 6273
    :cond_0
    const v0, 0x7f0900b3

    invoke-virtual {p0, v0}, Lcom/osp/app/signin/SignUpView;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    goto :goto_0

    .line 6285
    :cond_1
    const v0, 0x7f0900b2

    invoke-virtual {p0, v0}, Lcom/osp/app/signin/SignUpView;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 6286
    const v0, 0x7f0900af

    goto :goto_1
.end method

.method public final c(Ljava/lang/String;)V
    .locals 3

    .prologue
    .line 6639
    new-instance v1, Ljava/lang/StringBuffer;

    invoke-direct {v1}, Ljava/lang/StringBuffer;-><init>()V

    .line 6640
    invoke-static {}, Lcom/osp/device/b;->b()Lcom/osp/device/b;

    move-result-object v0

    invoke-virtual {v0}, Lcom/osp/device/b;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-static {p1}, Landroid/telephony/PhoneNumberUtils;->isGlobalPhoneNumber(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 6642
    const v0, 0x7f0900f3

    invoke-virtual {p0, v0}, Lcom/osp/app/signin/SignUpView;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 6647
    :goto_0
    const-string v0, "<br><br><b>"

    invoke-virtual {v1, v0}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 6648
    invoke-virtual {v1, p1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 6649
    const-string v0, "</b><br><br>"

    invoke-virtual {v1, v0}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 6650
    invoke-static {}, Lcom/osp/device/b;->b()Lcom/osp/device/b;

    move-result-object v0

    invoke-virtual {v0}, Lcom/osp/device/b;->a()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-static {p1}, Landroid/telephony/PhoneNumberUtils;->isGlobalPhoneNumber(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 6653
    const v0, 0x7f0900f2

    invoke-virtual {p0, v0}, Lcom/osp/app/signin/SignUpView;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 6654
    const v0, 0x7f0900ec

    .line 6661
    :goto_1
    new-instance v2, Landroid/app/AlertDialog$Builder;

    invoke-direct {v2, p0}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    invoke-virtual {v2, v0}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    invoke-virtual {v1}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Landroid/text/Html;->fromHtml(Ljava/lang/String;)Landroid/text/Spanned;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setMessage(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    const v1, 0x7f090064

    new-instance v2, Lcom/osp/app/signin/ll;

    invoke-direct {v2, p0, p1}, Lcom/osp/app/signin/ll;-><init>(Lcom/osp/app/signin/SignUpView;Ljava/lang/String;)V

    invoke-virtual {v0, v1, v2}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v0

    .line 6672
    invoke-virtual {v0}, Landroid/app/AlertDialog;->show()V

    .line 6673
    return-void

    .line 6645
    :cond_0
    const v0, 0x7f0900b3

    invoke-virtual {p0, v0}, Lcom/osp/app/signin/SignUpView;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    goto :goto_0

    .line 6657
    :cond_1
    const v0, 0x7f0900b2

    invoke-virtual {p0, v0}, Lcom/osp/app/signin/SignUpView;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 6658
    const v0, 0x7f0900af

    goto :goto_1
.end method

.method protected onActivityResult(IILandroid/content/Intent;)V
    .locals 8

    .prologue
    const/16 v7, 0xd

    const/16 v2, 0xe

    const/4 v6, 0x1

    const/4 v5, 0x0

    const/4 v4, -0x1

    .line 4816
    invoke-super {p0, p1, p2, p3}, Lcom/osp/app/util/BaseActivity;->onActivityResult(IILandroid/content/Intent;)V

    .line 4817
    const/16 v0, 0xf

    if-ne p2, v0, :cond_1

    .line 4819
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v0, "SUV"

    const-string v1, "FINISH_BY_HOME_BUTTON"

    invoke-static {v0, v1}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 4820
    const/16 v0, 0xf

    invoke-virtual {p0, v0}, Lcom/osp/app/signin/SignUpView;->b(I)V

    .line 4821
    invoke-virtual {p0}, Lcom/osp/app/signin/SignUpView;->finish()V

    .line 5059
    :cond_0
    :goto_0
    return-void

    .line 4825
    :cond_1
    const/16 v0, 0x15

    if-ne p2, v0, :cond_2

    .line 4827
    const/16 v0, 0x15

    invoke-virtual {p0, v0}, Lcom/osp/app/signin/SignUpView;->b(I)V

    .line 4828
    invoke-virtual {p0}, Lcom/osp/app/signin/SignUpView;->finish()V

    goto :goto_0

    .line 4832
    :cond_2
    const/16 v0, 0xd6

    if-ne p1, v0, :cond_3

    .line 4834
    const/16 v0, 0xa

    invoke-virtual {p0, v0}, Lcom/osp/app/signin/SignUpView;->b(I)V

    .line 4835
    invoke-virtual {p0}, Lcom/osp/app/signin/SignUpView;->finish()V

    goto :goto_0

    .line 4836
    :cond_3
    const/16 v0, 0xd7

    if-ne p1, v0, :cond_4

    .line 4838
    invoke-virtual {p0, v4, p3}, Lcom/osp/app/signin/SignUpView;->a(ILandroid/content/Intent;)V

    .line 4839
    invoke-virtual {p0}, Lcom/osp/app/signin/SignUpView;->finish()V

    goto :goto_0

    .line 4840
    :cond_4
    const/16 v0, 0xc9

    if-ne p1, v0, :cond_9

    .line 4842
    iput-object p3, p0, Lcom/osp/app/signin/SignUpView;->ai:Landroid/content/Intent;

    .line 4843
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v0, "=========================================== SignInDual_End PROCESSING_SUCCESS==========================================="

    invoke-static {v0}, Lcom/osp/app/util/aq;->c(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/osp/app/signin/SignUpView;->aq:Lcom/osp/app/signin/SignUpinfo;

    if-nez v0, :cond_7

    iget-object v0, p0, Lcom/osp/app/signin/SignUpView;->aS:Ljava/lang/String;

    :goto_1
    const-string v1, "OSP_02"

    iget-object v2, p0, Lcom/osp/app/signin/SignUpView;->an:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_5

    const-string v1, "ADD_ACCOUNT"

    iget-object v2, p0, Lcom/osp/app/signin/SignUpView;->ao:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_5

    iget-object v1, p0, Lcom/osp/app/signin/SignUpView;->ai:Landroid/content/Intent;

    const-string v2, "authcode"

    iget-object v3, p0, Lcom/osp/app/signin/SignUpView;->as:Ljava/lang/String;

    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    :cond_5
    iget-boolean v1, p0, Lcom/osp/app/signin/SignUpView;->V:Z

    if-eqz v1, :cond_6

    iget-object v1, p0, Lcom/osp/app/signin/SignUpView;->ai:Landroid/content/Intent;

    const-string v2, "signUpInfo"

    invoke-virtual {v1, v2, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "SignUpProcessEnd  PROCESS_STATE_AUTHORIZATION_COMPLETED\tsignUpInfo\t\t:\t"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/osp/app/util/aq;->b(Ljava/lang/String;)V

    .line 4844
    :cond_6
    const/16 v0, 0xc

    if-ne p2, v0, :cond_8

    .line 4846
    iget-object v0, p0, Lcom/osp/app/signin/SignUpView;->ai:Landroid/content/Intent;

    invoke-virtual {p0, v4, v0}, Lcom/osp/app/signin/SignUpView;->a(ILandroid/content/Intent;)V

    .line 4851
    :goto_2
    invoke-virtual {p0}, Lcom/osp/app/signin/SignUpView;->finish()V

    goto/16 :goto_0

    .line 4843
    :cond_7
    const-string v0, "SA_EMAIL_;SA_COUNTRY_;SA_BIRTHDATE_;SA_EMAILRECEIVE_;"

    goto :goto_1

    .line 4849
    :cond_8
    iget-object v0, p0, Lcom/osp/app/signin/SignUpView;->ai:Landroid/content/Intent;

    invoke-virtual {p0, v7, v0}, Lcom/osp/app/signin/SignUpView;->a(ILandroid/content/Intent;)V

    goto :goto_2

    .line 4852
    :cond_9
    const/16 v0, 0xcb

    if-ne p1, v0, :cond_a

    .line 4854
    if-ne p2, v4, :cond_0

    .line 4856
    invoke-direct {p0}, Lcom/osp/app/signin/SignUpView;->s()V

    goto/16 :goto_0

    .line 4858
    :cond_a
    const/16 v0, 0xd5

    if-ne p1, v0, :cond_c

    .line 4860
    if-eq p2, v4, :cond_0

    .line 4862
    invoke-virtual {p0}, Lcom/osp/app/signin/SignUpView;->getIntent()Landroid/content/Intent;

    move-result-object v0

    .line 4863
    if-eqz v0, :cond_b

    const-string v1, "is_cancelable_just_one_activity"

    invoke-virtual {v0, v1, v5}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v1

    if-eqz v1, :cond_b

    .line 4865
    invoke-virtual {p0, v2, v0}, Lcom/osp/app/signin/SignUpView;->a(ILandroid/content/Intent;)V

    .line 4867
    :cond_b
    invoke-virtual {p0}, Lcom/osp/app/signin/SignUpView;->finish()V

    goto/16 :goto_0

    .line 4870
    :cond_c
    const/16 v0, 0xca

    if-ne p1, v0, :cond_e

    .line 4872
    if-ne p2, v4, :cond_d

    .line 4874
    invoke-direct {p0, p3}, Lcom/osp/app/signin/SignUpView;->a(Landroid/content/Intent;)V

    goto/16 :goto_0

    .line 4877
    :cond_d
    invoke-virtual {p0, v2}, Lcom/osp/app/signin/SignUpView;->setResult(I)V

    .line 4878
    invoke-virtual {p0}, Lcom/osp/app/signin/SignUpView;->finish()V

    goto/16 :goto_0

    .line 4889
    :cond_e
    const/16 v0, 0xee

    if-ne p1, v0, :cond_1c

    .line 4891
    if-eqz p3, :cond_0

    if-ne p2, v6, :cond_0

    .line 4893
    iput-boolean v5, p0, Lcom/osp/app/signin/SignUpView;->aT:Z

    .line 4895
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/osp/app/signin/SignUpView;->aZ:Ljava/lang/String;

    .line 4896
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/osp/app/signin/SignUpView;->ba:Ljava/lang/String;

    .line 4898
    iget-object v0, p0, Lcom/osp/app/signin/SignUpView;->F:Landroid/widget/AutoCompleteTextView;

    if-eqz v0, :cond_f

    .line 4900
    iget-object v0, p0, Lcom/osp/app/signin/SignUpView;->F:Landroid/widget/AutoCompleteTextView;

    const-string v1, ""

    invoke-virtual {v0, v1}, Landroid/widget/AutoCompleteTextView;->setText(Ljava/lang/CharSequence;)V

    .line 4903
    :cond_f
    iget-boolean v0, p0, Lcom/osp/app/signin/SignUpView;->U:Z

    if-nez v0, :cond_12

    .line 4905
    iget-object v0, p0, Lcom/osp/app/signin/SignUpView;->I:Landroid/widget/EditText;

    if-eqz v0, :cond_10

    .line 4907
    iget-object v0, p0, Lcom/osp/app/signin/SignUpView;->I:Landroid/widget/EditText;

    const-string v1, ""

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    .line 4909
    :cond_10
    iget-object v0, p0, Lcom/osp/app/signin/SignUpView;->J:Landroid/widget/EditText;

    if-eqz v0, :cond_11

    .line 4911
    iget-object v0, p0, Lcom/osp/app/signin/SignUpView;->J:Landroid/widget/EditText;

    const-string v1, ""

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    .line 4913
    :cond_11
    iget-object v0, p0, Lcom/osp/app/signin/SignUpView;->A:Landroid/widget/TextView;

    if-eqz v0, :cond_12

    .line 4916
    iget-object v0, p0, Lcom/osp/app/signin/SignUpView;->A:Landroid/widget/TextView;

    new-instance v1, Ljava/util/Date;

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    invoke-direct {v1, v2, v3}, Ljava/util/Date;-><init>(J)V

    invoke-direct {p0, v1}, Lcom/osp/app/signin/SignUpView;->a(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 4917
    iget-object v0, p0, Lcom/osp/app/signin/SignUpView;->A:Landroid/widget/TextView;

    new-instance v1, Ljava/util/Date;

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    invoke-direct {v1, v2, v3}, Ljava/util/Date;-><init>(J)V

    invoke-direct {p0, v1}, Lcom/osp/app/signin/SignUpView;->a(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 4921
    :cond_12
    new-instance v0, Lcom/osp/app/signin/fm;

    invoke-direct {v0}, Lcom/osp/app/signin/fm;-><init>()V

    .line 4922
    const-string v1, "email"

    invoke-virtual {p3, v1}, Landroid/content/Intent;->hasExtra(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_13

    .line 4924
    const-string v1, "email"

    invoke-virtual {p3, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lcom/osp/app/signin/fm;->d:Ljava/lang/String;

    .line 4926
    :cond_13
    const-string v1, "given_name"

    invoke-virtual {p3, v1}, Landroid/content/Intent;->hasExtra(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_14

    .line 4928
    const-string v1, "given_name"

    invoke-virtual {p3, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lcom/osp/app/signin/fm;->a:Ljava/lang/String;

    .line 4930
    :cond_14
    const-string v1, "family_name"

    invoke-virtual {p3, v1}, Landroid/content/Intent;->hasExtra(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_15

    .line 4932
    const-string v1, "family_name"

    invoke-virtual {p3, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lcom/osp/app/signin/fm;->b:Ljava/lang/String;

    .line 4934
    :cond_15
    invoke-static {}, Lcom/osp/device/b;->b()Lcom/osp/device/b;

    move-result-object v1

    invoke-virtual {v1}, Lcom/osp/device/b;->a()Z

    move-result v1

    if-eqz v1, :cond_16

    const-string v1, "phone_number"

    invoke-virtual {p3, v1}, Landroid/content/Intent;->hasExtra(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_16

    .line 4936
    const-string v1, "phone_number"

    invoke-virtual {p3, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lcom/osp/app/signin/fm;->e:Ljava/lang/String;

    .line 4938
    :cond_16
    const-string v1, "birthday"

    invoke-virtual {p3, v1}, Landroid/content/Intent;->hasExtra(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_17

    .line 4940
    const-string v1, "birthday"

    invoke-virtual {p3, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 4941
    const-string v2, "/"

    invoke-virtual {v1, v2}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v1

    .line 4942
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const/4 v3, 0x2

    aget-object v3, v1, v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    aget-object v3, v1, v5

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    aget-object v1, v1, v6

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lcom/osp/app/signin/fm;->c:Ljava/lang/String;

    .line 4945
    :cond_17
    iget-object v1, v0, Lcom/osp/app/signin/fm;->d:Ljava/lang/String;

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_1b

    iget-object v1, v0, Lcom/osp/app/signin/fm;->e:Ljava/lang/String;

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_1b

    .line 4947
    iget-boolean v1, p0, Lcom/osp/app/signin/SignUpView;->U:Z

    if-eqz v1, :cond_18

    const-string v1, "Y"

    iget-object v2, p0, Lcom/osp/app/signin/SignUpView;->aN:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 4951
    :cond_18
    :try_start_0
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v1, "SUV"

    const-string v2, "set facebook name and birthday"

    invoke-static {v1, v2}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 4952
    iget-object v1, p0, Lcom/osp/app/signin/SignUpView;->I:Landroid/widget/EditText;

    if-eqz v1, :cond_19

    .line 4954
    iget-object v1, p0, Lcom/osp/app/signin/SignUpView;->I:Landroid/widget/EditText;

    iget-object v2, v0, Lcom/osp/app/signin/fm;->b:Ljava/lang/String;

    invoke-virtual {v1, v2}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    .line 4956
    :cond_19
    iget-object v1, p0, Lcom/osp/app/signin/SignUpView;->J:Landroid/widget/EditText;

    if-eqz v1, :cond_1a

    .line 4958
    iget-object v1, p0, Lcom/osp/app/signin/SignUpView;->J:Landroid/widget/EditText;

    iget-object v2, v0, Lcom/osp/app/signin/fm;->a:Ljava/lang/String;

    invoke-virtual {v1, v2}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    .line 4960
    :cond_1a
    iget-object v1, p0, Lcom/osp/app/signin/SignUpView;->A:Landroid/widget/TextView;

    if-eqz v1, :cond_0

    .line 4962
    new-instance v1, Ljava/text/SimpleDateFormat;

    const-string v2, "yyyyMMddHHmmss"

    invoke-direct {v1, v2}, Ljava/text/SimpleDateFormat;-><init>(Ljava/lang/String;)V

    .line 4963
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v0, v0, Lcom/osp/app/signin/fm;->c:Ljava/lang/String;

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, "235959"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/text/SimpleDateFormat;->parse(Ljava/lang/String;)Ljava/util/Date;

    move-result-object v0

    .line 4964
    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object v1

    .line 4965
    invoke-virtual {v1, v0}, Ljava/util/Calendar;->setTime(Ljava/util/Date;)V

    .line 4966
    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Ljava/util/Calendar;->get(I)I

    move-result v2

    iput v2, p0, Lcom/osp/app/signin/SignUpView;->az:I

    .line 4967
    const/4 v2, 0x2

    invoke-virtual {v1, v2}, Ljava/util/Calendar;->get(I)I

    move-result v2

    iput v2, p0, Lcom/osp/app/signin/SignUpView;->ay:I

    .line 4968
    const/4 v2, 0x5

    invoke-virtual {v1, v2}, Ljava/util/Calendar;->get(I)I

    move-result v1

    iput v1, p0, Lcom/osp/app/signin/SignUpView;->aA:I

    .line 4969
    iget-object v1, p0, Lcom/osp/app/signin/SignUpView;->A:Landroid/widget/TextView;

    invoke-direct {p0, v0}, Lcom/osp/app/signin/SignUpView;->a(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 4970
    iget-object v1, p0, Lcom/osp/app/signin/SignUpView;->A:Landroid/widget/TextView;

    invoke-direct {p0, v0}, Lcom/osp/app/signin/SignUpView;->a(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setContentDescription(Ljava/lang/CharSequence;)V
    :try_end_0
    .catch Ljava/text/ParseException; {:try_start_0 .. :try_end_0} :catch_0

    goto/16 :goto_0

    .line 4972
    :catch_0
    move-exception v0

    invoke-virtual {v0}, Ljava/text/ParseException;->printStackTrace()V

    goto/16 :goto_0

    .line 4979
    :cond_1b
    new-instance v1, Lcom/osp/app/signin/lx;

    invoke-direct {v1, p0, v0}, Lcom/osp/app/signin/lx;-><init>(Lcom/osp/app/signin/SignUpView;Lcom/osp/app/signin/fm;)V

    .line 4980
    new-array v0, v5, [Ljava/lang/Void;

    invoke-virtual {v1, v0}, Lcom/osp/app/signin/lx;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    goto/16 :goto_0

    .line 4985
    :cond_1c
    const/16 v0, 0xe1

    if-ne p1, v0, :cond_1e

    .line 4987
    if-ne p2, v4, :cond_1d

    .line 4989
    iput-boolean v5, p0, Lcom/osp/app/signin/SignUpView;->aT:Z

    .line 4991
    new-instance v0, Lcom/osp/app/signin/pz;

    invoke-direct {v0}, Lcom/osp/app/signin/pz;-><init>()V

    iput-object v0, p0, Lcom/osp/app/signin/SignUpView;->bi:Lcom/osp/app/signin/pz;

    .line 4992
    iget-object v0, p0, Lcom/osp/app/signin/SignUpView;->bi:Lcom/osp/app/signin/pz;

    const-string v1, "Weibo_AccessToken"

    invoke-virtual {p3, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/osp/app/signin/pz;->a(Ljava/lang/String;)V

    .line 4993
    iget-object v0, p0, Lcom/osp/app/signin/SignUpView;->bi:Lcom/osp/app/signin/pz;

    const-string v1, "Weibo_Uid"

    invoke-virtual {p3, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/osp/app/signin/pz;->b(Ljava/lang/String;)V

    .line 4994
    iget-object v0, p0, Lcom/osp/app/signin/SignUpView;->bi:Lcom/osp/app/signin/pz;

    const-string v1, "Weibo_Email"

    invoke-virtual {p3, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/osp/app/signin/pz;->c(Ljava/lang/String;)V

    .line 4995
    iget-object v0, p0, Lcom/osp/app/signin/SignUpView;->bi:Lcom/osp/app/signin/pz;

    const-string v1, "Weibo_Mobile"

    invoke-virtual {p3, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/osp/app/signin/pz;->f(Ljava/lang/String;)V

    .line 4996
    iget-object v0, p0, Lcom/osp/app/signin/SignUpView;->bi:Lcom/osp/app/signin/pz;

    const-string v1, "Weibo_IdType"

    invoke-virtual {p3, v1, v6}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/osp/app/signin/pz;->a(I)V

    .line 4997
    iget-object v0, p0, Lcom/osp/app/signin/SignUpView;->bi:Lcom/osp/app/signin/pz;

    const-string v1, "Weibo_Valid"

    invoke-virtual {p3, v1, v5}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v1

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/osp/app/signin/pz;->a(Ljava/lang/Boolean;)V

    .line 4999
    new-instance v0, Lcom/osp/app/signin/mb;

    invoke-direct {v0, p0}, Lcom/osp/app/signin/mb;-><init>(Lcom/osp/app/signin/SignUpView;)V

    .line 5001
    invoke-virtual {v0}, Lcom/osp/app/signin/mb;->b()V

    goto/16 :goto_0

    .line 5004
    :cond_1d
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v0, "Weibo Canceled"

    invoke-static {v0}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;)V

    goto/16 :goto_0

    .line 5006
    :cond_1e
    const/16 v0, 0xd0

    if-ne p1, v0, :cond_21

    .line 5008
    invoke-static {p0}, Lcom/msc/c/e;->f(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/osp/app/signin/SignUpView;->aj:Ljava/lang/String;

    .line 5009
    if-ne p2, v4, :cond_1f

    .line 5011
    invoke-direct {p0}, Lcom/osp/app/signin/SignUpView;->v()V

    goto/16 :goto_0

    .line 5014
    :cond_1f
    invoke-virtual {p0}, Lcom/osp/app/signin/SignUpView;->getIntent()Landroid/content/Intent;

    move-result-object v0

    .line 5015
    if-eqz v0, :cond_20

    const-string v1, "is_cancelable_just_one_activity"

    invoke-virtual {v0, v1, v5}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v0

    if-eqz v0, :cond_20

    .line 5017
    iget-object v0, p0, Lcom/osp/app/signin/SignUpView;->ai:Landroid/content/Intent;

    invoke-virtual {p0, v2, v0}, Lcom/osp/app/signin/SignUpView;->a(ILandroid/content/Intent;)V

    .line 5019
    :cond_20
    invoke-virtual {p0}, Lcom/osp/app/signin/SignUpView;->finish()V

    goto/16 :goto_0

    .line 5021
    :cond_21
    const/16 v0, 0xce

    if-ne p1, v0, :cond_26

    .line 5023
    if-ne p2, v4, :cond_22

    .line 5025
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v0, "TnC ok"

    invoke-static {v0}, Lcom/osp/app/util/aq;->b(Ljava/lang/String;)V

    .line 5027
    invoke-virtual {p0, v4, p3}, Lcom/osp/app/signin/SignUpView;->a(ILandroid/content/Intent;)V

    .line 5028
    invoke-virtual {p0}, Lcom/osp/app/signin/SignUpView;->finish()V

    goto/16 :goto_0

    .line 5029
    :cond_22
    if-ne p2, v7, :cond_23

    .line 5031
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v0, "sign up success but Email validation is canceled"

    invoke-static {v0}, Lcom/osp/app/util/aq;->b(Ljava/lang/String;)V

    .line 5032
    invoke-virtual {p0, v7}, Lcom/osp/app/signin/SignUpView;->b(I)V

    .line 5033
    invoke-virtual {p0}, Lcom/osp/app/signin/SignUpView;->finish()V

    goto/16 :goto_0

    .line 5034
    :cond_23
    if-ne p2, v2, :cond_24

    .line 5036
    invoke-virtual {p0}, Lcom/osp/app/signin/SignUpView;->getApplication()Landroid/app/Application;

    move-result-object v0

    check-cast v0, Lcom/osp/app/signin/SamsungService;

    invoke-virtual {v0, p0}, Lcom/osp/app/signin/SamsungService;->a(Landroid/app/Activity;)V

    .line 5037
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v0, "TnC Canceled"

    invoke-static {v0}, Lcom/osp/app/util/aq;->b(Ljava/lang/String;)V

    goto/16 :goto_0

    .line 5039
    :cond_24
    const/16 v0, 0x16

    if-ne p2, v0, :cond_25

    .line 5041
    const v0, 0x7f0c0086

    invoke-virtual {p0, v0}, Lcom/osp/app/signin/SignUpView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    .line 5042
    const v1, 0x7f0c0088

    invoke-virtual {p0, v1}, Lcom/osp/app/signin/SignUpView;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/Button;

    .line 5043
    invoke-virtual {v0, v6}, Landroid/widget/Button;->setSelected(Z)V

    .line 5044
    invoke-virtual {v1, v5}, Landroid/widget/Button;->setSelected(Z)V

    .line 5045
    invoke-direct {p0}, Lcom/osp/app/signin/SignUpView;->q()V

    .line 5046
    const/16 v0, 0x12c

    invoke-direct {p0, v0}, Lcom/osp/app/signin/SignUpView;->a(I)V

    .line 5047
    iget-object v0, p0, Lcom/osp/app/signin/SignUpView;->Y:Landroid/app/AlertDialog;

    invoke-direct {p0, v0}, Lcom/osp/app/signin/SignUpView;->a(Landroid/app/AlertDialog;)V

    .line 5048
    invoke-direct {p0}, Lcom/osp/app/signin/SignUpView;->c()V

    goto/16 :goto_0

    .line 5051
    :cond_25
    invoke-virtual {p0, p2, p3}, Lcom/osp/app/signin/SignUpView;->a(ILandroid/content/Intent;)V

    .line 5052
    invoke-virtual {p0}, Lcom/osp/app/signin/SignUpView;->finish()V

    goto/16 :goto_0

    .line 5056
    :cond_26
    iget-object v0, p0, Lcom/osp/app/signin/SignUpView;->ai:Landroid/content/Intent;

    invoke-virtual {p0, p2, v0}, Lcom/osp/app/signin/SignUpView;->a(ILandroid/content/Intent;)V

    .line 5057
    invoke-virtual {p0}, Lcom/osp/app/signin/SignUpView;->finish()V

    goto/16 :goto_0
.end method

.method public onBackPressed()V
    .locals 5

    .prologue
    .line 5606
    invoke-virtual {p0}, Lcom/osp/app/signin/SignUpView;->getIntent()Landroid/content/Intent;

    move-result-object v0

    .line 5607
    if-eqz v0, :cond_1

    const-string v1, "is_cancelable_just_one_activity"

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 5609
    const-string v1, "key_is_name_verified"

    iget-boolean v2, p0, Lcom/osp/app/signin/SignUpView;->U:Z

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 5610
    iget-boolean v1, p0, Lcom/osp/app/signin/SignUpView;->U:Z

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/osp/app/signin/SignUpView;->bb:Landroid/os/Bundle;

    if-eqz v1, :cond_0

    .line 5612
    const-string v1, "information_after_name_validation"

    iget-object v2, p0, Lcom/osp/app/signin/SignUpView;->bb:Landroid/os/Bundle;

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Bundle;)Landroid/content/Intent;

    .line 5614
    :cond_0
    const/16 v1, 0xe

    invoke-virtual {p0, v1, v0}, Lcom/osp/app/signin/SignUpView;->a(ILandroid/content/Intent;)V

    .line 5621
    :cond_1
    new-instance v0, Lcom/osp/app/bigdatalog/c;

    invoke-virtual {p0}, Lcom/osp/app/signin/SignUpView;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    const/4 v2, 0x1

    const/4 v3, 0x3

    iget-object v4, p0, Lcom/osp/app/signin/SignUpView;->ai:Landroid/content/Intent;

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/osp/app/bigdatalog/c;-><init>(Landroid/content/Context;IILandroid/content/Intent;)V

    .line 5622
    invoke-static {}, Lcom/osp/app/bigdatalog/e;->a()Lcom/osp/app/bigdatalog/e;

    move-result-object v1

    invoke-virtual {p0}, Lcom/osp/app/signin/SignUpView;->getApplicationContext()Landroid/content/Context;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/osp/app/bigdatalog/e;->a(Landroid/content/Context;)V

    .line 5623
    invoke-static {}, Lcom/osp/app/bigdatalog/e;->a()Lcom/osp/app/bigdatalog/e;

    move-result-object v1

    invoke-virtual {p0}, Lcom/osp/app/signin/SignUpView;->getApplicationContext()Landroid/content/Context;

    move-result-object v2

    invoke-virtual {v1, v2, v0}, Lcom/osp/app/bigdatalog/e;->a(Landroid/content/Context;Lcom/osp/app/bigdatalog/c;)V

    .line 5626
    invoke-super {p0}, Lcom/osp/app/util/BaseActivity;->onBackPressed()V

    .line 5627
    return-void
.end method

.method public onClick(Landroid/view/View;)V
    .locals 8

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x0

    .line 5781
    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    .line 5890
    :cond_0
    :goto_0
    return-void

    .line 5785
    :pswitch_0
    iget-object v0, p0, Lcom/osp/app/signin/SignUpView;->Z:Landroid/app/AlertDialog;

    if-eqz v0, :cond_2

    .line 5789
    :try_start_0
    iget-object v0, p0, Lcom/osp/app/signin/SignUpView;->Z:Landroid/app/AlertDialog;

    invoke-virtual {v0}, Landroid/app/AlertDialog;->isShowing()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 5791
    iget-object v0, p0, Lcom/osp/app/signin/SignUpView;->Z:Landroid/app/AlertDialog;

    invoke-virtual {v0}, Landroid/app/AlertDialog;->dismiss()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 5798
    :cond_1
    :goto_1
    iput-object v2, p0, Lcom/osp/app/signin/SignUpView;->Z:Landroid/app/AlertDialog;

    .line 5802
    :cond_2
    iget-object v0, p0, Lcom/osp/app/signin/SignUpView;->Z:Landroid/app/AlertDialog;

    if-nez v0, :cond_0

    .line 5806
    :try_start_1
    new-instance v2, Landroid/text/SpannableStringBuilder;

    invoke-direct {v2}, Landroid/text/SpannableStringBuilder;-><init>()V

    .line 5808
    invoke-static {p0}, Lcom/osp/app/util/r;->b(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 5810
    const v0, 0x7f090158

    invoke-virtual {p0, v0}, Lcom/osp/app/signin/SignUpView;->getText(I)Ljava/lang/CharSequence;

    move-result-object v0

    invoke-virtual {v2, v0}, Landroid/text/SpannableStringBuilder;->append(Ljava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;

    .line 5812
    invoke-static {}, Lcom/osp/device/b;->b()Lcom/osp/device/b;

    move-result-object v0

    invoke-virtual {v0}, Lcom/osp/device/b;->n()Ljava/lang/String;

    move-result-object v0

    .line 5813
    const-string v3, "450"

    iget-object v4, p0, Lcom/osp/app/signin/SignUpView;->aj:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_3

    const-string v3, "KR"

    invoke-virtual {v3, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_6

    .line 5815
    :cond_3
    invoke-virtual {v2}, Landroid/text/SpannableStringBuilder;->clear()V

    .line 5816
    const v0, 0x7f09019d

    invoke-virtual {p0, v0}, Lcom/osp/app/signin/SignUpView;->getText(I)Ljava/lang/CharSequence;

    move-result-object v0

    invoke-virtual {v2, v0}, Landroid/text/SpannableStringBuilder;->append(Ljava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;

    .line 5817
    const-string v0, "\n\n"

    invoke-virtual {v2, v0}, Landroid/text/SpannableStringBuilder;->append(Ljava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;

    .line 5818
    const-string v0, "* "

    invoke-virtual {v2, v0}, Landroid/text/SpannableStringBuilder;->append(Ljava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;

    .line 5819
    const v0, 0x7f09019b

    invoke-virtual {p0, v0}, Lcom/osp/app/signin/SignUpView;->getText(I)Ljava/lang/CharSequence;

    move-result-object v0

    invoke-virtual {v2, v0}, Landroid/text/SpannableStringBuilder;->append(Ljava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;

    .line 5821
    const/4 v0, 0x0

    invoke-virtual {v2}, Landroid/text/SpannableStringBuilder;->length()I

    move-result v3

    const-class v4, Landroid/text/style/URLSpan;

    invoke-virtual {v2, v0, v3, v4}, Landroid/text/SpannableStringBuilder;->getSpans(IILjava/lang/Class;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Landroid/text/style/URLSpan;

    .line 5823
    array-length v3, v0

    :goto_2
    if-ge v1, v3, :cond_6

    aget-object v4, v0, v1

    .line 5825
    invoke-virtual {v2, v4}, Landroid/text/SpannableStringBuilder;->getSpanStart(Ljava/lang/Object;)I

    move-result v5

    .line 5826
    invoke-virtual {v2, v4}, Landroid/text/SpannableStringBuilder;->getSpanEnd(Ljava/lang/Object;)I

    move-result v6

    .line 5827
    invoke-virtual {v4}, Landroid/text/style/URLSpan;->getURL()Ljava/lang/String;

    move-result-object v7

    .line 5828
    invoke-virtual {v2, v4}, Landroid/text/SpannableStringBuilder;->removeSpan(Ljava/lang/Object;)V

    .line 5829
    new-instance v4, Lcom/osp/app/signin/SignUpView$35;

    invoke-direct {v4, p0, v7}, Lcom/osp/app/signin/SignUpView$35;-><init>(Lcom/osp/app/signin/SignUpView;Ljava/lang/String;)V

    .line 5845
    const/4 v7, 0x0

    invoke-virtual {v2, v4, v5, v6, v7}, Landroid/text/SpannableStringBuilder;->setSpan(Ljava/lang/Object;III)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1

    .line 5823
    add-int/lit8 v1, v1, 0x1

    goto :goto_2

    .line 5793
    :catch_0
    move-exception v0

    :try_start_2
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto/16 :goto_1

    .line 5798
    :catchall_0
    move-exception v0

    iput-object v2, p0, Lcom/osp/app/signin/SignUpView;->Z:Landroid/app/AlertDialog;

    throw v0

    .line 5851
    :cond_4
    :try_start_3
    invoke-static {}, Lcom/osp/device/b;->b()Lcom/osp/device/b;

    move-result-object v0

    invoke-virtual {v0}, Lcom/osp/device/b;->n()Ljava/lang/String;

    move-result-object v0

    .line 5852
    const-string v1, "302"

    iget-object v3, p0, Lcom/osp/app/signin/SignUpView;->aj:Ljava/lang/String;

    invoke-virtual {v1, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_5

    const-string v1, "CA"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_7

    .line 5857
    :cond_5
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const v1, 0x7f090174

    invoke-virtual {p0, v1}, Lcom/osp/app/signin/SignUpView;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const v1, 0x7f090178

    invoke-virtual {p0, v1}, Lcom/osp/app/signin/SignUpView;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const v1, 0x7f090175

    invoke-virtual {p0, v1}, Lcom/osp/app/signin/SignUpView;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 5868
    :goto_3
    invoke-virtual {v2, v0}, Landroid/text/SpannableStringBuilder;->append(Ljava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;

    .line 5871
    :cond_6
    new-instance v0, Landroid/app/AlertDialog$Builder;

    invoke-direct {v0, p0}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    const v1, 0x7f090154

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/app/AlertDialog$Builder;->setMessage(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    const v1, 0x7f090042

    new-instance v2, Lcom/osp/app/signin/lj;

    invoke-direct {v2, p0}, Lcom/osp/app/signin/lj;-><init>(Lcom/osp/app/signin/SignUpView;)V

    invoke-virtual {v0, v1, v2}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v0

    iput-object v0, p0, Lcom/osp/app/signin/SignUpView;->Z:Landroid/app/AlertDialog;

    .line 5879
    iget-object v0, p0, Lcom/osp/app/signin/SignUpView;->Z:Landroid/app/AlertDialog;

    invoke-virtual {v0}, Landroid/app/AlertDialog;->show()V

    .line 5881
    iget-object v0, p0, Lcom/osp/app/signin/SignUpView;->Z:Landroid/app/AlertDialog;

    const v1, 0x102000b

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 5882
    if-eqz v0, :cond_0

    .line 5884
    invoke-static {}, Landroid/text/method/LinkMovementMethod;->getInstance()Landroid/text/method/MovementMethod;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setMovementMethod(Landroid/text/method/MovementMethod;)V
    :try_end_3
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_1

    goto/16 :goto_0

    .line 5887
    :catch_1
    move-exception v0

    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    goto/16 :goto_0

    .line 5860
    :cond_7
    :try_start_4
    iget-object v1, p0, Lcom/osp/app/signin/SignUpView;->aj:Ljava/lang/String;

    invoke-static {v1}, Lcom/osp/app/util/r;->a(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_8

    const-string v1, "US"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_9

    .line 5862
    :cond_8
    const v0, 0x7f09019f

    invoke-virtual {p0, v0}, Lcom/osp/app/signin/SignUpView;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_3

    .line 5865
    :cond_9
    const v0, 0x7f09019e

    invoke-virtual {p0, v0}, Lcom/osp/app/signin/SignUpView;->getString(I)Ljava/lang/String;
    :try_end_4
    .catch Ljava/lang/Exception; {:try_start_4 .. :try_end_4} :catch_1

    move-result-object v0

    goto :goto_3

    .line 5781
    :pswitch_data_0
    .packed-switch 0x7f0c0026
        :pswitch_0
    .end packed-switch
.end method

.method public onConfigurationChanged(Landroid/content/res/Configuration;)V
    .locals 2

    .prologue
    .line 5639
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v0, "SUV"

    const-string v1, "onConfigurationChanged()"

    invoke-static {v0, v1}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 5641
    iget v0, p1, Landroid/content/res/Configuration;->orientation:I

    invoke-direct {p0, v0}, Lcom/osp/app/signin/SignUpView;->e(I)V

    .line 5643
    invoke-super {p0, p1}, Lcom/osp/app/util/BaseActivity;->onConfigurationChanged(Landroid/content/res/Configuration;)V

    .line 5644
    return-void
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 10

    .prologue
    const v4, 0x7f09006b

    const/16 v9, 0x8

    const/4 v8, 0x0

    const/16 v7, 0xff

    const/4 v6, 0x0

    .line 5113
    invoke-super {p0, p1}, Lcom/osp/app/util/BaseActivity;->onCreate(Landroid/os/Bundle;)V

    .line 5115
    invoke-static {p0}, Lcom/osp/app/util/ad;->d(Landroid/content/Context;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 5117
    sget-object v0, Lcom/osp/app/loggingservice/c;->d:Lcom/osp/app/loggingservice/c;

    invoke-static {v0}, Lcom/osp/app/loggingservice/LoggingService;->a(Lcom/osp/app/loggingservice/c;)V

    .line 5120
    :cond_0
    invoke-virtual {p0}, Lcom/osp/app/signin/SignUpView;->getIntent()Landroid/content/Intent;

    move-result-object v0

    iput-object v0, p0, Lcom/osp/app/signin/SignUpView;->ai:Landroid/content/Intent;

    .line 5121
    invoke-virtual {p0}, Lcom/osp/app/signin/SignUpView;->getCallingActivity()Landroid/content/ComponentName;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 5123
    invoke-virtual {p0}, Lcom/osp/app/signin/SignUpView;->getCallingActivity()Landroid/content/ComponentName;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/ComponentName;->getShortClassName()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/osp/app/signin/SignUpView;->bs:Ljava/lang/String;

    .line 5128
    :cond_1
    invoke-virtual {p0}, Lcom/osp/app/signin/SignUpView;->getApplication()Landroid/app/Application;

    move-result-object v0

    check-cast v0, Lcom/osp/app/signin/SamsungService;

    invoke-virtual {v0, p0}, Lcom/osp/app/signin/SamsungService;->a(Landroid/app/Activity;)V

    .line 5130
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v0, "SignUpView::onCreate"

    invoke-static {v0}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;)V

    .line 5138
    invoke-virtual {p0}, Lcom/osp/app/signin/SignUpView;->getIntent()Landroid/content/Intent;

    move-result-object v0

    iput-object v0, p0, Lcom/osp/app/signin/SignUpView;->ai:Landroid/content/Intent;

    iget-object v0, p0, Lcom/osp/app/signin/SignUpView;->ai:Landroid/content/Intent;

    const-string v1, "client_id"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/osp/app/signin/SignUpView;->ag:Ljava/lang/String;

    iget-object v0, p0, Lcom/osp/app/signin/SignUpView;->ai:Landroid/content/Intent;

    const-string v1, "client_secret"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/osp/app/signin/SignUpView;->ah:Ljava/lang/String;

    iget-object v0, p0, Lcom/osp/app/signin/SignUpView;->ai:Landroid/content/Intent;

    const-string v1, "account_mode"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/osp/app/signin/SignUpView;->W:Ljava/lang/String;

    iget-object v0, p0, Lcom/osp/app/signin/SignUpView;->ai:Landroid/content/Intent;

    const-string v1, "MODE"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/osp/app/signin/SignUpView;->ao:Ljava/lang/String;

    iget-object v0, p0, Lcom/osp/app/signin/SignUpView;->ai:Landroid/content/Intent;

    const-string v1, "service_name"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/osp/app/signin/SignUpView;->aU:Ljava/lang/String;

    iget-object v0, p0, Lcom/osp/app/signin/SignUpView;->ai:Landroid/content/Intent;

    const-string v1, "OSP_VER"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/osp/app/signin/SignUpView;->an:Ljava/lang/String;

    iget-object v0, p0, Lcom/osp/app/signin/SignUpView;->ai:Landroid/content/Intent;

    const-string v1, "new_add_account_mode"

    invoke-virtual {v0, v1, v6}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v0

    iput-boolean v0, p0, Lcom/osp/app/signin/SignUpView;->aQ:Z

    iget-object v0, p0, Lcom/osp/app/signin/SignUpView;->ai:Landroid/content/Intent;

    const-string v1, "mypackage"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/osp/app/signin/SignUpView;->aV:Ljava/lang/String;

    iget-object v0, p0, Lcom/osp/app/signin/SignUpView;->ai:Landroid/content/Intent;

    const-string v1, "key_is_name_verified"

    invoke-virtual {v0, v1, v6}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v0

    iput-boolean v0, p0, Lcom/osp/app/signin/SignUpView;->U:Z

    iget-object v0, p0, Lcom/osp/app/signin/SignUpView;->ai:Landroid/content/Intent;

    const-string v1, "country_code_mcc"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/osp/app/signin/SignUpView;->aj:Ljava/lang/String;

    iget-object v0, p0, Lcom/osp/app/signin/SignUpView;->ai:Landroid/content/Intent;

    const-string v1, "key_easy_signup_tnc_mode"

    invoke-virtual {v0, v1, v6}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v0

    iput-boolean v0, p0, Lcom/osp/app/signin/SignUpView;->ak:Z

    iget-object v0, p0, Lcom/osp/app/signin/SignUpView;->ai:Landroid/content/Intent;

    const-string v1, "key_easy_signup_phone_number"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/osp/app/signin/SignUpView;->al:Ljava/lang/String;

    iget-object v0, p0, Lcom/osp/app/signin/SignUpView;->ai:Landroid/content/Intent;

    const-string v1, "serviceApp_type"

    invoke-virtual {v0, v1, v6}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v0

    iput v0, p0, Lcom/osp/app/signin/SignUpView;->aW:I

    const-string v0, "BG_mode"

    iget-object v1, p0, Lcom/osp/app/signin/SignUpView;->ao:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/osp/app/signin/SignUpView;->ai:Landroid/content/Intent;

    const-string v1, "mypackage"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/osp/app/signin/SignUpView;->ar:Ljava/lang/String;

    iget-object v0, p0, Lcom/osp/app/signin/SignUpView;->ai:Landroid/content/Intent;

    const-string v1, "key_request_id"

    const-wide/16 v2, 0x0

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/Intent;->getLongExtra(Ljava/lang/String;J)J

    move-result-wide v0

    iput-wide v0, p0, Lcom/osp/app/signin/SignUpView;->aR:J

    :cond_2
    const-string v0, "SamsungApps"

    iget-object v1, p0, Lcom/osp/app/signin/SignUpView;->aU:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/osp/app/signin/SignUpView;->V:Z

    :cond_3
    invoke-static {}, Lcom/osp/app/util/r;->q()Z

    move-result v0

    if-eqz v0, :cond_4

    iget-object v0, p0, Lcom/osp/app/signin/SignUpView;->ai:Landroid/content/Intent;

    const-string v1, "key_google_account"

    invoke-virtual {v0, v1, v6}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v0

    iput-boolean v0, p0, Lcom/osp/app/signin/SignUpView;->aT:Z

    iget-boolean v0, p0, Lcom/osp/app/signin/SignUpView;->aT:Z

    if-eqz v0, :cond_4

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/osp/app/signin/SignUpView;->bw:Z

    :cond_4
    iget-object v0, p0, Lcom/osp/app/signin/SignUpView;->ai:Landroid/content/Intent;

    const-string v1, "key_signup_log_is_skip_invoked"

    invoke-virtual {v0, v1, v6}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v0

    iput-boolean v0, p0, Lcom/osp/app/signin/SignUpView;->by:Z

    iget-object v0, p0, Lcom/osp/app/signin/SignUpView;->ai:Landroid/content/Intent;

    const-string v1, "key_marketingpopup_mode"

    invoke-virtual {v0, v1, v6}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v0

    iput-boolean v0, p0, Lcom/osp/app/signin/SignUpView;->bk:Z

    iget-object v0, p0, Lcom/osp/app/signin/SignUpView;->ai:Landroid/content/Intent;

    const-string v1, "key_welcome_content"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/osp/app/signin/SignUpView;->bj:Ljava/lang/String;

    .line 5139
    iput-boolean v6, p0, Lcom/osp/app/signin/SignUpView;->bv:Z

    .line 5141
    invoke-static {p0}, Lcom/osp/app/util/r;->l(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_30

    .line 5143
    const v0, 0x7f030031

    invoke-virtual {p0, v0}, Lcom/osp/app/signin/SignUpView;->setContentView(I)V

    .line 5149
    :goto_0
    invoke-static {p0}, Lcom/osp/app/util/r;->i(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_5

    .line 5151
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x13

    if-ge v0, v1, :cond_5

    .line 5153
    invoke-static {}, Lcom/osp/device/b;->b()Lcom/osp/device/b;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/osp/device/b;->i(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_5

    .line 5155
    invoke-static {p0}, Lcom/msc/sa/c/d;->c(Landroid/app/Activity;)V

    .line 5172
    :cond_5
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/osp/app/signin/SignUpView;->be:Ljava/util/ArrayList;

    .line 5174
    invoke-virtual {p0}, Lcom/osp/app/signin/SignUpView;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v0, 0x7f0c0004

    invoke-virtual {p0, v0}, Lcom/osp/app/signin/SignUpView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    if-eqz v0, :cond_6

    invoke-static {p0}, Lcom/osp/app/util/an;->a(Lcom/osp/app/util/BaseActivity;)Lcom/osp/app/util/an;

    const v2, 0x7f070006

    invoke-static {v2}, Lcom/osp/app/util/an;->a(I)I

    move-result v2

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v2

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setTextColor(I)V

    :cond_6
    iget-object v0, p0, Lcom/osp/app/signin/SignUpView;->F:Landroid/widget/AutoCompleteTextView;

    if-nez v0, :cond_7

    const v0, 0x7f0c013a

    invoke-virtual {p0, v0}, Lcom/osp/app/signin/SignUpView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/AutoCompleteTextView;

    iput-object v0, p0, Lcom/osp/app/signin/SignUpView;->F:Landroid/widget/AutoCompleteTextView;

    :cond_7
    const v0, 0x7f0c013d

    invoke-virtual {p0, v0}, Lcom/osp/app/signin/SignUpView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    if-eqz v0, :cond_8

    invoke-static {p0}, Lcom/osp/app/util/an;->a(Lcom/osp/app/util/BaseActivity;)Lcom/osp/app/util/an;

    const v2, 0x7f070006

    invoke-static {v2}, Lcom/osp/app/util/an;->a(I)I

    move-result v2

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v2

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setTextColor(I)V

    :cond_8
    iget-object v0, p0, Lcom/osp/app/signin/SignUpView;->E:Landroid/widget/EditText;

    if-nez v0, :cond_9

    const v0, 0x7f0c00ab

    invoke-virtual {p0, v0}, Lcom/osp/app/signin/SignUpView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/EditText;

    iput-object v0, p0, Lcom/osp/app/signin/SignUpView;->E:Landroid/widget/EditText;

    :cond_9
    iget-object v0, p0, Lcom/osp/app/signin/SignUpView;->F:Landroid/widget/AutoCompleteTextView;

    iget-object v2, p0, Lcom/osp/app/signin/SignUpView;->E:Landroid/widget/EditText;

    invoke-virtual {v2}, Landroid/widget/EditText;->getHintTextColors()Landroid/content/res/ColorStateList;

    move-result-object v2

    invoke-virtual {v0, v2}, Landroid/widget/AutoCompleteTextView;->setHintTextColor(Landroid/content/res/ColorStateList;)V

    iget-object v0, p0, Lcom/osp/app/signin/SignUpView;->F:Landroid/widget/AutoCompleteTextView;

    iget-object v2, p0, Lcom/osp/app/signin/SignUpView;->E:Landroid/widget/EditText;

    invoke-virtual {v2}, Landroid/widget/EditText;->getTypeface()Landroid/graphics/Typeface;

    move-result-object v2

    invoke-virtual {v0, v2}, Landroid/widget/AutoCompleteTextView;->setTypeface(Landroid/graphics/Typeface;)V

    iget-object v0, p0, Lcom/osp/app/signin/SignUpView;->F:Landroid/widget/AutoCompleteTextView;

    iget-object v2, p0, Lcom/osp/app/signin/SignUpView;->E:Landroid/widget/EditText;

    invoke-virtual {v2}, Landroid/widget/EditText;->getTextSize()F

    move-result v2

    invoke-virtual {v0, v6, v2}, Landroid/widget/AutoCompleteTextView;->setTextSize(IF)V

    invoke-direct {p0}, Lcom/osp/app/signin/SignUpView;->y()V

    invoke-static {}, Lcom/osp/app/util/r;->r()Z

    move-result v0

    if-eqz v0, :cond_a

    iget-object v0, p0, Lcom/osp/app/signin/SignUpView;->F:Landroid/widget/AutoCompleteTextView;

    new-instance v2, Lcom/osp/app/signin/ln;

    invoke-direct {v2, p0}, Lcom/osp/app/signin/ln;-><init>(Lcom/osp/app/signin/SignUpView;)V

    invoke-virtual {v0, v2}, Landroid/widget/AutoCompleteTextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    :cond_a
    const v0, 0x7f0c013b

    invoke-virtual {p0, v0}, Lcom/osp/app/signin/SignUpView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    if-eqz v0, :cond_b

    invoke-static {p0}, Lcom/osp/app/util/an;->a(Lcom/osp/app/util/BaseActivity;)Lcom/osp/app/util/an;

    const v2, 0x7f02003b

    invoke-static {v2}, Lcom/osp/app/util/an;->a(I)I

    move-result v2

    invoke-virtual {v0, v2}, Landroid/widget/ImageView;->setImageResource(I)V

    invoke-static {v0}, Lcom/msc/sa/c/d;->a(Landroid/view/View;)V

    :cond_b
    const v0, 0x7f0c0109

    invoke-virtual {p0, v0}, Lcom/osp/app/signin/SignUpView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    if-eqz v0, :cond_c

    invoke-static {p0}, Lcom/osp/app/util/an;->a(Lcom/osp/app/util/BaseActivity;)Lcom/osp/app/util/an;

    const v2, 0x7f02003b

    invoke-static {v2}, Lcom/osp/app/util/an;->a(I)I

    move-result v2

    invoke-virtual {v0, v2}, Landroid/widget/ImageView;->setImageResource(I)V

    invoke-static {v0}, Lcom/msc/sa/c/d;->a(Landroid/view/View;)V

    :cond_c
    const v0, 0x7f0c013f

    invoke-virtual {p0, v0}, Lcom/osp/app/signin/SignUpView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    if-eqz v0, :cond_d

    invoke-static {p0}, Lcom/osp/app/util/an;->a(Lcom/osp/app/util/BaseActivity;)Lcom/osp/app/util/an;

    const v2, 0x7f070006

    invoke-static {v2}, Lcom/osp/app/util/an;->a(I)I

    move-result v2

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v2

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setTextColor(I)V

    :cond_d
    iget-object v0, p0, Lcom/osp/app/signin/SignUpView;->D:Landroid/widget/EditText;

    if-nez v0, :cond_e

    const v0, 0x7f0c0140

    invoke-virtual {p0, v0}, Lcom/osp/app/signin/SignUpView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/EditText;

    iput-object v0, p0, Lcom/osp/app/signin/SignUpView;->D:Landroid/widget/EditText;

    :cond_e
    iget-object v0, p0, Lcom/osp/app/signin/SignUpView;->C:Landroid/widget/CheckBox;

    if-nez v0, :cond_f

    const v0, 0x7f0c0141

    invoke-virtual {p0, v0}, Lcom/osp/app/signin/SignUpView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/CheckBox;

    iput-object v0, p0, Lcom/osp/app/signin/SignUpView;->C:Landroid/widget/CheckBox;

    iget-object v0, p0, Lcom/osp/app/signin/SignUpView;->C:Landroid/widget/CheckBox;

    invoke-static {p0}, Lcom/osp/app/util/an;->a(Lcom/osp/app/util/BaseActivity;)Lcom/osp/app/util/an;

    const v2, 0x7f070004

    invoke-static {v2}, Lcom/osp/app/util/an;->a(I)I

    move-result v2

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v2

    invoke-virtual {v0, v2}, Landroid/widget/CheckBox;->setTextColor(I)V

    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v2, 0x10

    if-le v0, v2, :cond_f

    invoke-virtual {p0}, Lcom/osp/app/signin/SignUpView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v2, 0x7f0800e2

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v0

    float-to-int v0, v0

    iget-object v2, p0, Lcom/osp/app/signin/SignUpView;->C:Landroid/widget/CheckBox;

    invoke-static {v2, v0, v6, v6, v6}, Lcom/msc/sa/c/d;->a(Landroid/view/View;IIII)V

    :cond_f
    const v0, 0x7f0c0143

    invoke-virtual {p0, v0}, Lcom/osp/app/signin/SignUpView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    if-eqz v0, :cond_10

    invoke-static {p0}, Lcom/osp/app/util/an;->a(Lcom/osp/app/util/BaseActivity;)Lcom/osp/app/util/an;

    const v2, 0x7f070006

    invoke-static {v2}, Lcom/osp/app/util/an;->a(I)I

    move-result v2

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v2

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setTextColor(I)V

    :cond_10
    const v0, 0x7f0c0015

    invoke-virtual {p0, v0}, Lcom/osp/app/signin/SignUpView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcom/osp/app/signin/SignUpView;->y:Landroid/widget/LinearLayout;

    const v0, 0x7f0c0017

    invoke-virtual {p0, v0}, Lcom/osp/app/signin/SignUpView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/osp/app/signin/SignUpView;->A:Landroid/widget/TextView;

    const v0, 0x7f0c0016

    invoke-virtual {p0, v0}, Lcom/osp/app/signin/SignUpView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/osp/app/signin/SignUpView;->z:Landroid/widget/ImageView;

    iget-object v0, p0, Lcom/osp/app/signin/SignUpView;->z:Landroid/widget/ImageView;

    if-eqz v0, :cond_11

    iget-object v0, p0, Lcom/osp/app/signin/SignUpView;->z:Landroid/widget/ImageView;

    invoke-static {p0}, Lcom/osp/app/util/an;->a(Lcom/osp/app/util/BaseActivity;)Lcom/osp/app/util/an;

    const v2, 0x7f020057

    invoke-static {v2}, Lcom/osp/app/util/an;->a(I)I

    move-result v2

    invoke-virtual {v0, v2}, Landroid/widget/ImageView;->setImageResource(I)V

    :cond_11
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v2, 0x15

    if-lt v0, v2, :cond_12

    const v0, 0x7f0c0144

    invoke-virtual {p0, v0}, Lcom/osp/app/signin/SignUpView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    if-eqz v0, :cond_12

    invoke-virtual {v0, v9}, Landroid/view/View;->setVisibility(I)V

    :cond_12
    const v0, 0x7f0c0018

    invoke-virtual {p0, v0}, Lcom/osp/app/signin/SignUpView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    if-eqz v0, :cond_13

    invoke-static {p0}, Lcom/osp/app/util/an;->a(Lcom/osp/app/util/BaseActivity;)Lcom/osp/app/util/an;

    const v2, 0x7f02003b

    invoke-static {v2}, Lcom/osp/app/util/an;->a(I)I

    move-result v2

    invoke-virtual {v0, v2}, Landroid/widget/ImageView;->setImageResource(I)V

    invoke-static {v0}, Lcom/msc/sa/c/d;->a(Landroid/view/View;)V

    :cond_13
    const v0, 0x7f0c0145

    invoke-virtual {p0, v0}, Lcom/osp/app/signin/SignUpView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    if-eqz v0, :cond_14

    invoke-static {p0}, Lcom/osp/app/util/an;->a(Lcom/osp/app/util/BaseActivity;)Lcom/osp/app/util/an;

    const v2, 0x7f070006

    invoke-static {v2}, Lcom/osp/app/util/an;->a(I)I

    move-result v2

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v2

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setTextColor(I)V

    :cond_14
    const v0, 0x7f0c0146

    invoke-virtual {p0, v0}, Lcom/osp/app/signin/SignUpView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    if-eqz v0, :cond_15

    invoke-static {p0}, Lcom/osp/app/util/an;->a(Lcom/osp/app/util/BaseActivity;)Lcom/osp/app/util/an;

    const v2, 0x7f070004

    invoke-static {v2}, Lcom/osp/app/util/an;->a(I)I

    move-result v2

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v2

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setTextColor(I)V

    :cond_15
    iget-object v0, p0, Lcom/osp/app/signin/SignUpView;->B:Landroid/widget/CheckBox;

    if-nez v0, :cond_16

    const v0, 0x7f0c0025

    invoke-virtual {p0, v0}, Lcom/osp/app/signin/SignUpView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/CheckBox;

    iput-object v0, p0, Lcom/osp/app/signin/SignUpView;->B:Landroid/widget/CheckBox;

    iget-object v0, p0, Lcom/osp/app/signin/SignUpView;->B:Landroid/widget/CheckBox;

    invoke-static {p0}, Lcom/osp/app/util/an;->a(Lcom/osp/app/util/BaseActivity;)Lcom/osp/app/util/an;

    const v2, 0x7f070004

    invoke-static {v2}, Lcom/osp/app/util/an;->a(I)I

    move-result v2

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/CheckBox;->setTextColor(I)V

    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x10

    if-le v0, v1, :cond_16

    invoke-virtual {p0}, Lcom/osp/app/signin/SignUpView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0800e2

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v0

    float-to-int v0, v0

    iget-object v1, p0, Lcom/osp/app/signin/SignUpView;->B:Landroid/widget/CheckBox;

    invoke-static {v1, v0, v6, v6, v6}, Lcom/msc/sa/c/d;->a(Landroid/view/View;IIII)V

    :cond_16
    const v0, 0x7f0c0026

    invoke-virtual {p0, v0}, Lcom/osp/app/signin/SignUpView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    if-eqz v0, :cond_17

    invoke-static {p0}, Lcom/osp/app/util/an;->a(Lcom/osp/app/util/BaseActivity;)Lcom/osp/app/util/an;

    const v1, 0x7f02003b

    invoke-static {v1}, Lcom/osp/app/util/an;->a(I)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageResource(I)V

    invoke-static {v0}, Lcom/msc/sa/c/d;->a(Landroid/view/View;)V

    :cond_17
    const v0, 0x7f0c0065

    invoke-virtual {p0, v0}, Lcom/osp/app/signin/SignUpView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/osp/app/signin/BottomSoftkeyLinearLayout;

    iput-object v0, p0, Lcom/osp/app/signin/SignUpView;->bx:Lcom/osp/app/signin/BottomSoftkeyLinearLayout;

    iget-object v0, p0, Lcom/osp/app/signin/SignUpView;->bx:Lcom/osp/app/signin/BottomSoftkeyLinearLayout;

    if-eqz v0, :cond_18

    iget-object v0, p0, Lcom/osp/app/signin/SignUpView;->bx:Lcom/osp/app/signin/BottomSoftkeyLinearLayout;

    invoke-virtual {p0}, Lcom/osp/app/signin/SignUpView;->h()Z

    move-result v1

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-virtual {v0, p0, v1}, Lcom/osp/app/signin/BottomSoftkeyLinearLayout;->a(Landroid/content/Context;Ljava/lang/Boolean;)V

    invoke-static {}, Lcom/osp/device/b;->b()Lcom/osp/device/b;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/osp/device/b;->l(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_32

    iget-object v0, p0, Lcom/osp/app/signin/SignUpView;->bx:Lcom/osp/app/signin/BottomSoftkeyLinearLayout;

    invoke-virtual {p0, v4}, Lcom/osp/app/signin/SignUpView;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/osp/app/signin/BottomSoftkeyLinearLayout;->b(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/osp/app/signin/SignUpView;->bx:Lcom/osp/app/signin/BottomSoftkeyLinearLayout;

    invoke-virtual {p0, v4}, Lcom/osp/app/signin/SignUpView;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/osp/app/signin/BottomSoftkeyLinearLayout;->d(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/osp/app/signin/SignUpView;->bx:Lcom/osp/app/signin/BottomSoftkeyLinearLayout;

    const v1, 0x7f09019c

    invoke-virtual {p0, v1}, Lcom/osp/app/signin/SignUpView;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/osp/app/signin/BottomSoftkeyLinearLayout;->c(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/osp/app/signin/SignUpView;->bx:Lcom/osp/app/signin/BottomSoftkeyLinearLayout;

    const v1, 0x7f09019c

    invoke-virtual {p0, v1}, Lcom/osp/app/signin/SignUpView;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/osp/app/signin/BottomSoftkeyLinearLayout;->e(Ljava/lang/String;)V

    invoke-static {}, Lcom/osp/app/signin/SamsungService;->a()Z

    move-result v0

    if-eqz v0, :cond_18

    invoke-static {p0}, Lcom/osp/app/util/r;->g(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_31

    iget-object v0, p0, Lcom/osp/app/signin/SignUpView;->bx:Lcom/osp/app/signin/BottomSoftkeyLinearLayout;

    invoke-virtual {p0, v4}, Lcom/osp/app/signin/SignUpView;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/osp/app/signin/BottomSoftkeyLinearLayout;->g(Ljava/lang/String;)V

    :cond_18
    :goto_1
    invoke-static {}, Lcom/osp/app/signin/SamsungService;->a()Z

    move-result v0

    if-nez v0, :cond_19

    invoke-static {}, Lcom/osp/device/b;->b()Lcom/osp/device/b;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/osp/device/b;->i(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_19

    iget-object v0, p0, Lcom/osp/app/signin/SignUpView;->bx:Lcom/osp/app/signin/BottomSoftkeyLinearLayout;

    if-eqz v0, :cond_19

    iget-object v0, p0, Lcom/osp/app/signin/SignUpView;->bx:Lcom/osp/app/signin/BottomSoftkeyLinearLayout;

    invoke-virtual {v0, v9}, Lcom/osp/app/signin/BottomSoftkeyLinearLayout;->setVisibility(I)V

    :cond_19
    const v0, 0x7f0c013c

    invoke-virtual {p0, v0}, Lcom/osp/app/signin/SignUpView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    if-eqz v0, :cond_1a

    invoke-virtual {p0}, Lcom/osp/app/signin/SignUpView;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0800e9

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v1

    float-to-int v1, v1

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setMinimumHeight(I)V

    :cond_1a
    const v0, 0x7f0c013e

    invoke-virtual {p0, v0}, Lcom/osp/app/signin/SignUpView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    if-eqz v0, :cond_1b

    invoke-virtual {v0, v9}, Landroid/widget/LinearLayout;->setVisibility(I)V

    :cond_1b
    const v0, 0x7f0c00ac

    invoke-virtual {p0, v0}, Lcom/osp/app/signin/SignUpView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    if-eqz v0, :cond_1c

    invoke-virtual {v0, v9}, Landroid/view/View;->setVisibility(I)V

    :cond_1c
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x15

    if-lt v0, v1, :cond_1e

    const v0, 0x7f0c00a8

    invoke-virtual {p0, v0}, Lcom/osp/app/signin/SignUpView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    if-eqz v0, :cond_1d

    invoke-virtual {v0, v9}, Landroid/view/View;->setVisibility(I)V

    :cond_1d
    const v0, 0x7f0c0148

    invoke-virtual {p0, v0}, Lcom/osp/app/signin/SignUpView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    if-eqz v0, :cond_1e

    invoke-virtual {v0, v9}, Landroid/view/View;->setVisibility(I)V

    :cond_1e
    const v0, 0x7f0c00ae

    invoke-virtual {p0, v0}, Lcom/osp/app/signin/SignUpView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    if-eqz v0, :cond_1f

    invoke-virtual {v0, v9}, Landroid/view/View;->setVisibility(I)V

    :cond_1f
    iget-object v0, p0, Lcom/osp/app/signin/SignUpView;->C:Landroid/widget/CheckBox;

    if-eqz v0, :cond_20

    invoke-static {}, Lcom/osp/device/b;->b()Lcom/osp/device/b;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/osp/device/b;->i(Landroid/content/Context;)Z

    move-result v0

    if-nez v0, :cond_20

    iget-object v0, p0, Lcom/osp/app/signin/SignUpView;->C:Landroid/widget/CheckBox;

    invoke-virtual {p0}, Lcom/osp/app/signin/SignUpView;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f08007c

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/CheckBox;->setMinimumHeight(I)V

    :cond_20
    invoke-static {}, Lcom/osp/app/util/r;->v()Z

    move-result v0

    if-eqz v0, :cond_21

    const v0, 0x7f0c0142

    invoke-virtual {p0, v0}, Lcom/osp/app/signin/SignUpView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v9}, Landroid/view/View;->setVisibility(I)V

    :cond_21
    invoke-virtual {p0}, Lcom/osp/app/signin/SignUpView;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-static {}, Lcom/osp/device/b;->b()Lcom/osp/device/b;

    move-result-object v0

    invoke-virtual {v0}, Lcom/osp/device/b;->a()Z

    move-result v0

    if-eqz v0, :cond_2d

    iget-boolean v0, p0, Lcom/osp/app/signin/SignUpView;->aT:Z

    if-nez v0, :cond_2d

    const v0, 0x7f0c0085

    invoke-virtual {p0, v0}, Lcom/osp/app/signin/SignUpView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    if-eqz v0, :cond_22

    invoke-virtual {v0, v6}, Landroid/widget/LinearLayout;->setVisibility(I)V

    invoke-static {p0}, Lcom/osp/app/util/r;->g(Landroid/content/Context;)Z

    move-result v1

    if-eqz v1, :cond_35

    invoke-static {}, Lcom/osp/app/signin/SamsungService;->a()Z

    move-result v1

    if-nez v1, :cond_34

    invoke-static {p0}, Lcom/osp/app/util/an;->a(Lcom/osp/app/util/BaseActivity;)Lcom/osp/app/util/an;

    const v1, 0x7f020103

    invoke-static {v1}, Lcom/osp/app/util/an;->a(I)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setBackgroundResource(I)V

    :cond_22
    :goto_2
    const v0, 0x7f0c0086

    invoke-virtual {p0, v0}, Lcom/osp/app/signin/SignUpView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    if-eqz v0, :cond_24

    const v1, 0x7f090183

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setText(I)V

    invoke-static {p0}, Lcom/osp/app/util/an;->a(Lcom/osp/app/util/BaseActivity;)Lcom/osp/app/util/an;

    const v1, 0x7f02000e

    invoke-static {v1}, Lcom/osp/app/util/an;->a(I)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setBackgroundResource(I)V

    invoke-static {p0}, Lcom/osp/app/util/r;->g(Landroid/content/Context;)Z

    move-result v1

    if-eqz v1, :cond_39

    invoke-static {}, Lcom/osp/app/signin/SamsungService;->a()Z

    move-result v1

    if-eqz v1, :cond_23

    const v1, 0x7f0200e9

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setBackgroundResource(I)V

    :cond_23
    const v1, 0x7f020015

    invoke-virtual {v2, v1}, Landroid/content/res/Resources;->getColorStateList(I)Landroid/content/res/ColorStateList;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setTextColor(Landroid/content/res/ColorStateList;)V

    :cond_24
    :goto_3
    const v1, 0x7f0c0088

    invoke-virtual {p0, v1}, Lcom/osp/app/signin/SignUpView;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/Button;

    if-eqz v1, :cond_26

    const v3, 0x7f090184

    invoke-virtual {v1, v3}, Landroid/widget/Button;->setText(I)V

    invoke-static {p0}, Lcom/osp/app/util/an;->a(Lcom/osp/app/util/BaseActivity;)Lcom/osp/app/util/an;

    const v3, 0x7f02000e

    invoke-static {v3}, Lcom/osp/app/util/an;->a(I)I

    move-result v3

    invoke-virtual {v1, v3}, Landroid/widget/Button;->setBackgroundResource(I)V

    invoke-static {p0}, Lcom/osp/app/util/r;->g(Landroid/content/Context;)Z

    move-result v3

    if-eqz v3, :cond_3f

    invoke-static {}, Lcom/osp/app/signin/SamsungService;->a()Z

    move-result v3

    if-eqz v3, :cond_25

    const v3, 0x7f0200e9

    invoke-virtual {v1, v3}, Landroid/widget/Button;->setBackgroundResource(I)V

    :cond_25
    const v3, 0x7f020015

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getColorStateList(I)Landroid/content/res/ColorStateList;

    move-result-object v3

    invoke-virtual {v1, v3}, Landroid/widget/Button;->setTextColor(Landroid/content/res/ColorStateList;)V

    :cond_26
    :goto_4
    invoke-static {}, Lcom/osp/device/b;->b()Lcom/osp/device/b;

    move-result-object v3

    invoke-virtual {v3, p0}, Lcom/osp/device/b;->i(Landroid/content/Context;)Z

    move-result v3

    if-nez v3, :cond_27

    if-eqz v0, :cond_27

    if-eqz v1, :cond_27

    invoke-virtual {v0}, Landroid/widget/Button;->getText()Ljava/lang/CharSequence;

    move-result-object v3

    invoke-interface {v3}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v3

    invoke-virtual {v1}, Landroid/widget/Button;->getText()Ljava/lang/CharSequence;

    move-result-object v4

    invoke-interface {v4}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/String;->length()I

    move-result v4

    if-gt v3, v4, :cond_45

    :goto_5
    invoke-virtual {v1}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    const/4 v3, -0x2

    iput v3, v0, Landroid/view/ViewGroup$LayoutParams;->height:I

    invoke-virtual {v1, v0}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    invoke-virtual {p0}, Lcom/osp/app/signin/SignUpView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v3, 0x7f080100

    invoke-virtual {v0, v3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    invoke-virtual {v1, v0}, Landroid/view/View;->setMinimumHeight(I)V

    :cond_27
    const v0, 0x7f0c0087

    invoke-virtual {p0, v0}, Lcom/osp/app/signin/SignUpView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    if-eqz v0, :cond_2a

    invoke-static {p0}, Lcom/osp/app/util/an;->a(Lcom/osp/app/util/BaseActivity;)Lcom/osp/app/util/an;

    const v1, 0x7f020146

    invoke-static {v1}, Lcom/osp/app/util/an;->a(I)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/view/View;->setBackgroundResource(I)V

    invoke-static {}, Lcom/osp/app/signin/SamsungService;->a()Z

    move-result v1

    if-eqz v1, :cond_28

    const v1, 0x7f0200d2

    invoke-virtual {v0, v1}, Landroid/view/View;->setBackgroundResource(I)V

    :cond_28
    invoke-static {p0}, Lcom/osp/app/util/r;->m(Landroid/content/Context;)Z

    move-result v1

    if-nez v1, :cond_29

    invoke-static {p0}, Lcom/osp/app/util/r;->l(Landroid/content/Context;)Z

    move-result v1

    if-eqz v1, :cond_2a

    :cond_29
    invoke-virtual {v0, v9}, Landroid/view/View;->setVisibility(I)V

    :cond_2a
    invoke-direct {p0}, Lcom/osp/app/signin/SignUpView;->q()V

    const v0, 0x7f0c014a

    invoke-virtual {p0, v0}, Lcom/osp/app/signin/SignUpView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    if-eqz v0, :cond_2b

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const v3, 0x7f0900e2

    invoke-virtual {p0, v3}, Lcom/osp/app/signin/SignUpView;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v3, " *"

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    invoke-static {p0}, Lcom/osp/app/util/an;->a(Lcom/osp/app/util/BaseActivity;)Lcom/osp/app/util/an;

    const v1, 0x7f070006

    invoke-static {v1}, Lcom/osp/app/util/an;->a(I)I

    move-result v1

    invoke-virtual {v2, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setTextColor(I)V

    :cond_2b
    const v0, 0x7f0c014c

    invoke-virtual {p0, v0}, Lcom/osp/app/signin/SignUpView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    if-eqz v0, :cond_2c

    invoke-static {p0}, Lcom/osp/app/util/an;->a(Lcom/osp/app/util/BaseActivity;)Lcom/osp/app/util/an;

    const v1, 0x7f02003b

    invoke-static {v1}, Lcom/osp/app/util/an;->a(I)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageResource(I)V

    invoke-static {v0}, Lcom/msc/sa/c/d;->a(Landroid/view/View;)V

    :cond_2c
    const v0, 0x7f0c014f

    invoke-virtual {p0, v0}, Lcom/osp/app/signin/SignUpView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    if-eqz v0, :cond_2d

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const v3, 0x7f0900e1

    invoke-virtual {p0, v3}, Lcom/osp/app/signin/SignUpView;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v3, " *"

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    invoke-static {p0}, Lcom/osp/app/util/an;->a(Lcom/osp/app/util/BaseActivity;)Lcom/osp/app/util/an;

    const v1, 0x7f070006

    invoke-static {v1}, Lcom/osp/app/util/an;->a(I)I

    move-result v1

    invoke-virtual {v2, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setTextColor(I)V

    .line 5177
    :cond_2d
    invoke-virtual {p0}, Lcom/osp/app/signin/SignUpView;->getLayoutInflater()Landroid/view/LayoutInflater;

    move-result-object v0

    iput-object v0, p0, Lcom/osp/app/signin/SignUpView;->ax:Landroid/view/LayoutInflater;

    .line 5180
    invoke-static {p0}, Lcom/osp/app/util/ad;->a(Landroid/content/Context;)Z

    move-result v0

    if-nez v0, :cond_2e

    .line 5182
    invoke-static {}, Lcom/osp/app/util/ap;->a()Lcom/osp/app/util/ap;

    const v0, 0x7f09003e

    invoke-virtual {p0, v0}, Lcom/osp/app/signin/SignUpView;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-static {p0, v0, v6}, Lcom/osp/app/util/ap;->a(Landroid/content/Context;Ljava/lang/String;I)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    .line 5191
    const/16 v0, 0xd5

    invoke-virtual {p0, v0}, Lcom/osp/app/signin/SignUpView;->c(I)V

    .line 5197
    :cond_2e
    invoke-direct {p0}, Lcom/osp/app/signin/SignUpView;->v()V

    .line 5261
    invoke-static {p0}, Lcom/msc/sa/c/d;->a(Landroid/app/Activity;)V

    .line 5266
    invoke-virtual {p0}, Lcom/osp/app/signin/SignUpView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v0

    iget v0, v0, Landroid/content/res/Configuration;->orientation:I

    invoke-direct {p0, v0}, Lcom/osp/app/signin/SignUpView;->e(I)V

    .line 5268
    invoke-static {}, Lcom/osp/app/signin/SamsungService;->a()Z

    move-result v0

    if-nez v0, :cond_2f

    invoke-static {}, Lcom/osp/device/b;->b()Lcom/osp/device/b;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/osp/device/b;->i(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_2f

    .line 5276
    const-string v0, "signup_cancel"

    invoke-virtual {p0, v0}, Lcom/osp/app/signin/SignUpView;->e(Ljava/lang/String;)V

    .line 5278
    :cond_2f
    return-void

    .line 5146
    :cond_30
    const v0, 0x7f030031

    invoke-static {p0}, Lcom/osp/app/util/r;->q(Landroid/content/Context;)Z

    move-result v1

    invoke-virtual {p0, v0, v1}, Lcom/osp/app/signin/SignUpView;->a(IZ)V

    goto/16 :goto_0

    .line 5174
    :cond_31
    iget-object v0, p0, Lcom/osp/app/signin/SignUpView;->bx:Lcom/osp/app/signin/BottomSoftkeyLinearLayout;

    invoke-virtual {p0, v4}, Lcom/osp/app/signin/SignUpView;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/osp/app/signin/BottomSoftkeyLinearLayout;->g(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/osp/app/signin/SignUpView;->bx:Lcom/osp/app/signin/BottomSoftkeyLinearLayout;

    const v1, 0x7f09019c

    invoke-virtual {p0, v1}, Lcom/osp/app/signin/SignUpView;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/osp/app/signin/BottomSoftkeyLinearLayout;->f(Ljava/lang/String;)V

    goto/16 :goto_1

    :cond_32
    iget-object v0, p0, Lcom/osp/app/signin/SignUpView;->bx:Lcom/osp/app/signin/BottomSoftkeyLinearLayout;

    const v1, 0x7f09019c

    invoke-virtual {p0, v1}, Lcom/osp/app/signin/SignUpView;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/osp/app/signin/BottomSoftkeyLinearLayout;->b(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/osp/app/signin/SignUpView;->bx:Lcom/osp/app/signin/BottomSoftkeyLinearLayout;

    const v1, 0x7f09019c

    invoke-virtual {p0, v1}, Lcom/osp/app/signin/SignUpView;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/osp/app/signin/BottomSoftkeyLinearLayout;->d(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/osp/app/signin/SignUpView;->bx:Lcom/osp/app/signin/BottomSoftkeyLinearLayout;

    invoke-virtual {p0, v4}, Lcom/osp/app/signin/SignUpView;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/osp/app/signin/BottomSoftkeyLinearLayout;->c(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/osp/app/signin/SignUpView;->bx:Lcom/osp/app/signin/BottomSoftkeyLinearLayout;

    invoke-virtual {p0, v4}, Lcom/osp/app/signin/SignUpView;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/osp/app/signin/BottomSoftkeyLinearLayout;->e(Ljava/lang/String;)V

    invoke-static {}, Lcom/osp/app/signin/SamsungService;->a()Z

    move-result v0

    if-eqz v0, :cond_18

    invoke-static {p0}, Lcom/osp/app/util/r;->g(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_33

    iget-object v0, p0, Lcom/osp/app/signin/SignUpView;->bx:Lcom/osp/app/signin/BottomSoftkeyLinearLayout;

    invoke-virtual {p0, v4}, Lcom/osp/app/signin/SignUpView;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/osp/app/signin/BottomSoftkeyLinearLayout;->f(Ljava/lang/String;)V

    goto/16 :goto_1

    :cond_33
    iget-object v0, p0, Lcom/osp/app/signin/SignUpView;->bx:Lcom/osp/app/signin/BottomSoftkeyLinearLayout;

    const v1, 0x7f09019c

    invoke-virtual {p0, v1}, Lcom/osp/app/signin/SignUpView;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/osp/app/signin/BottomSoftkeyLinearLayout;->g(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/osp/app/signin/SignUpView;->bx:Lcom/osp/app/signin/BottomSoftkeyLinearLayout;

    invoke-virtual {p0, v4}, Lcom/osp/app/signin/SignUpView;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/osp/app/signin/BottomSoftkeyLinearLayout;->f(Ljava/lang/String;)V

    goto/16 :goto_1

    :cond_34
    invoke-virtual {p0}, Lcom/osp/app/signin/SignUpView;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v3, 0x7f07003b

    invoke-virtual {v1, v3}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setBackgroundColor(I)V

    goto/16 :goto_2

    :cond_35
    invoke-static {p0}, Lcom/osp/app/util/r;->k(Landroid/content/Context;)Z

    move-result v1

    if-eqz v1, :cond_37

    invoke-static {}, Lcom/osp/app/signin/SamsungService;->a()Z

    move-result v1

    if-eqz v1, :cond_36

    invoke-static {p0}, Lcom/osp/app/util/an;->a(Lcom/osp/app/util/BaseActivity;)Lcom/osp/app/util/an;

    const v1, 0x7f0200ce

    invoke-static {v1}, Lcom/osp/app/util/an;->a(I)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setBackgroundResource(I)V

    goto/16 :goto_2

    :cond_36
    invoke-static {p0}, Lcom/osp/app/util/an;->a(Lcom/osp/app/util/BaseActivity;)Lcom/osp/app/util/an;

    const v1, 0x7f020103

    invoke-static {v1}, Lcom/osp/app/util/an;->a(I)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setBackgroundResource(I)V

    goto/16 :goto_2

    :cond_37
    invoke-static {p0}, Lcom/osp/app/util/r;->m(Landroid/content/Context;)Z

    move-result v1

    if-eqz v1, :cond_22

    invoke-static {}, Lcom/osp/app/signin/SamsungService;->a()Z

    move-result v1

    if-nez v1, :cond_38

    invoke-static {p0}, Lcom/osp/app/util/an;->a(Lcom/osp/app/util/BaseActivity;)Lcom/osp/app/util/an;

    const v1, 0x7f020103

    invoke-static {v1}, Lcom/osp/app/util/an;->a(I)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setBackgroundResource(I)V

    goto/16 :goto_2

    :cond_38
    sget v1, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v3, 0x15

    if-ne v1, v3, :cond_22

    invoke-static {p0}, Lcom/osp/app/util/an;->a(Lcom/osp/app/util/BaseActivity;)Lcom/osp/app/util/an;

    const v1, 0x7f0200e6

    invoke-static {v1}, Lcom/osp/app/util/an;->a(I)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setBackgroundResource(I)V

    goto/16 :goto_2

    :cond_39
    invoke-static {}, Lcom/osp/device/b;->b()Lcom/osp/device/b;

    move-result-object v1

    invoke-virtual {v1, p0}, Lcom/osp/device/b;->i(Landroid/content/Context;)Z

    move-result v1

    if-nez v1, :cond_3c

    invoke-static {}, Lcom/osp/app/signin/SamsungService;->a()Z

    move-result v1

    if-eqz v1, :cond_3b

    invoke-static {p0}, Lcom/osp/app/util/r;->m(Landroid/content/Context;)Z

    move-result v1

    if-eqz v1, :cond_3b

    invoke-static {p0}, Lcom/osp/app/util/an;->a(Lcom/osp/app/util/BaseActivity;)Lcom/osp/app/util/an;

    const v1, 0x7f0200e8

    invoke-static {v1}, Lcom/osp/app/util/an;->a(I)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setBackgroundResource(I)V

    invoke-virtual {p0}, Lcom/osp/app/signin/SignUpView;->h()Z

    move-result v1

    if-nez v1, :cond_3a

    invoke-static {p0}, Lcom/osp/app/util/an;->a(Lcom/osp/app/util/BaseActivity;)Lcom/osp/app/util/an;

    const v1, 0x7f02000c

    invoke-static {v1}, Lcom/osp/app/util/an;->a(I)I

    move-result v1

    invoke-virtual {v2, v1}, Landroid/content/res/Resources;->getColorStateList(I)Landroid/content/res/ColorStateList;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setTextColor(Landroid/content/res/ColorStateList;)V

    goto/16 :goto_3

    :cond_3a
    invoke-static {p0}, Lcom/osp/app/util/an;->a(Lcom/osp/app/util/BaseActivity;)Lcom/osp/app/util/an;

    const v1, 0x7f020013

    invoke-static {v1}, Lcom/osp/app/util/an;->a(I)I

    move-result v1

    invoke-virtual {v2, v1}, Landroid/content/res/Resources;->getColorStateList(I)Landroid/content/res/ColorStateList;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setTextColor(Landroid/content/res/ColorStateList;)V

    goto/16 :goto_3

    :cond_3b
    invoke-static {p0}, Lcom/osp/app/util/an;->a(Lcom/osp/app/util/BaseActivity;)Lcom/osp/app/util/an;

    const v1, 0x7f020013

    invoke-static {v1}, Lcom/osp/app/util/an;->a(I)I

    move-result v1

    invoke-virtual {v2, v1}, Landroid/content/res/Resources;->getColorStateList(I)Landroid/content/res/ColorStateList;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setTextColor(Landroid/content/res/ColorStateList;)V

    goto/16 :goto_3

    :cond_3c
    invoke-virtual {p0}, Lcom/osp/app/signin/SignUpView;->h()Z

    move-result v1

    if-nez v1, :cond_3e

    invoke-static {}, Lcom/osp/app/signin/SamsungService;->a()Z

    move-result v1

    if-eqz v1, :cond_3e

    const v1, 0x7f0200e9

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setBackgroundResource(I)V

    invoke-static {p0}, Lcom/osp/app/util/r;->k(Landroid/content/Context;)Z

    move-result v1

    if-eqz v1, :cond_3d

    const/high16 v1, 0x40400000    # 3.0f

    const/high16 v3, 0x40400000    # 3.0f

    const/16 v4, 0xb3

    invoke-static {v4, v7, v7, v7}, Landroid/graphics/Color;->argb(IIII)I

    move-result v4

    invoke-virtual {v0, v1, v8, v3, v4}, Landroid/widget/Button;->setShadowLayer(FFFI)V

    invoke-static {p0}, Lcom/osp/app/util/an;->a(Lcom/osp/app/util/BaseActivity;)Lcom/osp/app/util/an;

    const v1, 0x7f0200e5

    invoke-static {v1}, Lcom/osp/app/util/an;->a(I)I

    move-result v1

    invoke-virtual {v2, v1}, Landroid/content/res/Resources;->getColorStateList(I)Landroid/content/res/ColorStateList;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setTextColor(Landroid/content/res/ColorStateList;)V

    new-instance v1, Lcom/osp/app/signin/lo;

    invoke-direct {v1, p0, v0}, Lcom/osp/app/signin/lo;-><init>(Lcom/osp/app/signin/SignUpView;Landroid/widget/Button;)V

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    goto/16 :goto_3

    :cond_3d
    const/high16 v1, 0x40000000    # 2.0f

    const/high16 v3, 0x40000000    # 2.0f

    const/16 v4, 0xb3

    invoke-static {v4, v7, v7, v7}, Landroid/graphics/Color;->argb(IIII)I

    move-result v4

    invoke-virtual {v0, v1, v8, v3, v4}, Landroid/widget/Button;->setShadowLayer(FFFI)V

    invoke-static {p0}, Lcom/osp/app/util/an;->a(Lcom/osp/app/util/BaseActivity;)Lcom/osp/app/util/an;

    const v1, 0x7f02000c

    invoke-static {v1}, Lcom/osp/app/util/an;->a(I)I

    move-result v1

    invoke-virtual {v2, v1}, Landroid/content/res/Resources;->getColorStateList(I)Landroid/content/res/ColorStateList;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setTextColor(Landroid/content/res/ColorStateList;)V

    goto/16 :goto_3

    :cond_3e
    invoke-virtual {v0, v8, v8, v8, v6}, Landroid/widget/Button;->setShadowLayer(FFFI)V

    invoke-static {p0}, Lcom/osp/app/util/an;->a(Lcom/osp/app/util/BaseActivity;)Lcom/osp/app/util/an;

    const v1, 0x7f02000c

    invoke-static {v1}, Lcom/osp/app/util/an;->a(I)I

    move-result v1

    invoke-virtual {v2, v1}, Landroid/content/res/Resources;->getColorStateList(I)Landroid/content/res/ColorStateList;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setTextColor(Landroid/content/res/ColorStateList;)V

    goto/16 :goto_3

    :cond_3f
    invoke-static {}, Lcom/osp/device/b;->b()Lcom/osp/device/b;

    move-result-object v3

    invoke-virtual {v3, p0}, Lcom/osp/device/b;->i(Landroid/content/Context;)Z

    move-result v3

    if-nez v3, :cond_42

    invoke-static {}, Lcom/osp/app/signin/SamsungService;->a()Z

    move-result v3

    if-eqz v3, :cond_41

    invoke-static {p0}, Lcom/osp/app/util/r;->m(Landroid/content/Context;)Z

    move-result v3

    if-eqz v3, :cond_41

    invoke-static {p0}, Lcom/osp/app/util/an;->a(Lcom/osp/app/util/BaseActivity;)Lcom/osp/app/util/an;

    const v3, 0x7f0200e8

    invoke-static {v3}, Lcom/osp/app/util/an;->a(I)I

    move-result v3

    invoke-virtual {v1, v3}, Landroid/widget/Button;->setBackgroundResource(I)V

    invoke-virtual {p0}, Lcom/osp/app/signin/SignUpView;->h()Z

    move-result v3

    if-nez v3, :cond_40

    invoke-static {p0}, Lcom/osp/app/util/an;->a(Lcom/osp/app/util/BaseActivity;)Lcom/osp/app/util/an;

    const v3, 0x7f02000c

    invoke-static {v3}, Lcom/osp/app/util/an;->a(I)I

    move-result v3

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getColorStateList(I)Landroid/content/res/ColorStateList;

    move-result-object v3

    invoke-virtual {v1, v3}, Landroid/widget/Button;->setTextColor(Landroid/content/res/ColorStateList;)V

    goto/16 :goto_4

    :cond_40
    invoke-static {p0}, Lcom/osp/app/util/an;->a(Lcom/osp/app/util/BaseActivity;)Lcom/osp/app/util/an;

    const v3, 0x7f020013

    invoke-static {v3}, Lcom/osp/app/util/an;->a(I)I

    move-result v3

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getColorStateList(I)Landroid/content/res/ColorStateList;

    move-result-object v3

    invoke-virtual {v1, v3}, Landroid/widget/Button;->setTextColor(Landroid/content/res/ColorStateList;)V

    goto/16 :goto_4

    :cond_41
    invoke-static {p0}, Lcom/osp/app/util/an;->a(Lcom/osp/app/util/BaseActivity;)Lcom/osp/app/util/an;

    const v3, 0x7f020013

    invoke-static {v3}, Lcom/osp/app/util/an;->a(I)I

    move-result v3

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getColorStateList(I)Landroid/content/res/ColorStateList;

    move-result-object v3

    invoke-virtual {v1, v3}, Landroid/widget/Button;->setTextColor(Landroid/content/res/ColorStateList;)V

    goto/16 :goto_4

    :cond_42
    invoke-virtual {p0}, Lcom/osp/app/signin/SignUpView;->h()Z

    move-result v3

    if-nez v3, :cond_44

    invoke-static {}, Lcom/osp/app/signin/SamsungService;->a()Z

    move-result v3

    if-eqz v3, :cond_44

    const v3, 0x7f0200e9

    invoke-virtual {v1, v3}, Landroid/widget/Button;->setBackgroundResource(I)V

    invoke-static {p0}, Lcom/osp/app/util/r;->k(Landroid/content/Context;)Z

    move-result v3

    if-eqz v3, :cond_43

    const/high16 v3, 0x40400000    # 3.0f

    const/high16 v4, 0x40400000    # 3.0f

    const/16 v5, 0xb3

    invoke-static {v5, v7, v7, v7}, Landroid/graphics/Color;->argb(IIII)I

    move-result v5

    invoke-virtual {v1, v3, v8, v4, v5}, Landroid/widget/Button;->setShadowLayer(FFFI)V

    invoke-static {p0}, Lcom/osp/app/util/an;->a(Lcom/osp/app/util/BaseActivity;)Lcom/osp/app/util/an;

    const v3, 0x7f0200e5

    invoke-static {v3}, Lcom/osp/app/util/an;->a(I)I

    move-result v3

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getColorStateList(I)Landroid/content/res/ColorStateList;

    move-result-object v3

    invoke-virtual {v1, v3}, Landroid/widget/Button;->setTextColor(Landroid/content/res/ColorStateList;)V

    new-instance v3, Lcom/osp/app/signin/lp;

    invoke-direct {v3, p0, v1}, Lcom/osp/app/signin/lp;-><init>(Lcom/osp/app/signin/SignUpView;Landroid/widget/Button;)V

    invoke-virtual {v1, v3}, Landroid/widget/Button;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    goto/16 :goto_4

    :cond_43
    const/high16 v3, 0x40000000    # 2.0f

    const/high16 v4, 0x40000000    # 2.0f

    const/16 v5, 0xb3

    invoke-static {v5, v7, v7, v7}, Landroid/graphics/Color;->argb(IIII)I

    move-result v5

    invoke-virtual {v1, v3, v8, v4, v5}, Landroid/widget/Button;->setShadowLayer(FFFI)V

    invoke-static {p0}, Lcom/osp/app/util/an;->a(Lcom/osp/app/util/BaseActivity;)Lcom/osp/app/util/an;

    const v3, 0x7f02000c

    invoke-static {v3}, Lcom/osp/app/util/an;->a(I)I

    move-result v3

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getColorStateList(I)Landroid/content/res/ColorStateList;

    move-result-object v3

    invoke-virtual {v1, v3}, Landroid/widget/Button;->setTextColor(Landroid/content/res/ColorStateList;)V

    goto/16 :goto_4

    :cond_44
    invoke-virtual {v1, v8, v8, v8, v6}, Landroid/widget/Button;->setShadowLayer(FFFI)V

    invoke-static {p0}, Lcom/osp/app/util/an;->a(Lcom/osp/app/util/BaseActivity;)Lcom/osp/app/util/an;

    const v3, 0x7f02000c

    invoke-static {v3}, Lcom/osp/app/util/an;->a(I)I

    move-result v3

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getColorStateList(I)Landroid/content/res/ColorStateList;

    move-result-object v3

    invoke-virtual {v1, v3}, Landroid/widget/Button;->setTextColor(Landroid/content/res/ColorStateList;)V

    goto/16 :goto_4

    :cond_45
    move-object v1, v0

    goto/16 :goto_5
.end method

.method public onCreateDialog(I)Landroid/app/Dialog;
    .locals 7

    .prologue
    const v6, 0x7f0900b7

    const v2, 0x7f09005b

    const v5, 0x7f0900a7

    const v4, 0x7f090064

    const/4 v3, 0x0

    .line 5289
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "SignUpView::onCreateDialog id : "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;)V

    .line 5291
    sparse-switch p1, :sswitch_data_0

    .line 5432
    const/4 v0, 0x0

    :goto_0
    return-object v0

    .line 5295
    :sswitch_0
    new-instance v0, Landroid/app/DatePickerDialog;

    iget-object v2, p0, Lcom/osp/app/signin/SignUpView;->au:Landroid/app/DatePickerDialog$OnDateSetListener;

    iget v3, p0, Lcom/osp/app/signin/SignUpView;->az:I

    iget v4, p0, Lcom/osp/app/signin/SignUpView;->ay:I

    iget v5, p0, Lcom/osp/app/signin/SignUpView;->aA:I

    move-object v1, p0

    invoke-direct/range {v0 .. v5}, Landroid/app/DatePickerDialog;-><init>(Landroid/content/Context;Landroid/app/DatePickerDialog$OnDateSetListener;III)V

    iput-object v0, p0, Lcom/osp/app/signin/SignUpView;->at:Landroid/app/Dialog;

    .line 5296
    iget v0, p0, Lcom/osp/app/signin/SignUpView;->az:I

    iput v0, p0, Lcom/osp/app/signin/SignUpView;->f:I

    .line 5297
    iget v0, p0, Lcom/osp/app/signin/SignUpView;->ay:I

    iput v0, p0, Lcom/osp/app/signin/SignUpView;->e:I

    .line 5298
    iget v0, p0, Lcom/osp/app/signin/SignUpView;->aA:I

    iput v0, p0, Lcom/osp/app/signin/SignUpView;->d:I

    .line 5311
    iget-object v0, p0, Lcom/osp/app/signin/SignUpView;->at:Landroid/app/Dialog;

    check-cast v0, Landroid/app/DatePickerDialog;

    invoke-static {v0}, Lcom/msc/sa/c/d;->a(Landroid/app/DatePickerDialog;)V

    .line 5312
    iget-object v0, p0, Lcom/osp/app/signin/SignUpView;->at:Landroid/app/Dialog;

    new-instance v1, Lcom/osp/app/signin/ld;

    invoke-direct {v1, p0}, Lcom/osp/app/signin/ld;-><init>(Lcom/osp/app/signin/SignUpView;)V

    invoke-virtual {v0, v1}, Landroid/app/Dialog;->setOnDismissListener(Landroid/content/DialogInterface$OnDismissListener;)V

    .line 5320
    iget-object v0, p0, Lcom/osp/app/signin/SignUpView;->at:Landroid/app/Dialog;

    goto :goto_0

    .line 5322
    :sswitch_1
    const/4 v0, 0x0

    goto :goto_0

    .line 5355
    :sswitch_2
    new-instance v0, Landroid/app/AlertDialog$Builder;

    invoke-direct {v0, p0}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    const v1, 0x7f0900a8

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    invoke-virtual {v0, v5}, Landroid/app/AlertDialog$Builder;->setMessage(I)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    new-instance v1, Lcom/osp/app/signin/lf;

    invoke-direct {v1, p0}, Lcom/osp/app/signin/lf;-><init>(Lcom/osp/app/signin/SignUpView;)V

    invoke-virtual {v0, v4, v1}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    invoke-virtual {v0, v3}, Landroid/app/AlertDialog$Builder;->setCancelable(Z)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v0

    goto :goto_0

    .line 5370
    :sswitch_3
    new-instance v0, Landroid/app/AlertDialog$Builder;

    invoke-direct {v0, p0}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    invoke-virtual {v0, v6}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0, v2}, Lcom/osp/app/signin/SignUpView;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "\n"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0, v5}, Lcom/osp/app/signin/SignUpView;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setMessage(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    new-instance v1, Lcom/osp/app/signin/lg;

    invoke-direct {v1, p0}, Lcom/osp/app/signin/lg;-><init>(Lcom/osp/app/signin/SignUpView;)V

    invoke-virtual {v0, v4, v1}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    invoke-virtual {v0, v3}, Landroid/app/AlertDialog$Builder;->setCancelable(Z)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v0

    goto/16 :goto_0

    .line 5388
    :sswitch_4
    new-instance v0, Landroid/app/AlertDialog$Builder;

    invoke-direct {v0, p0}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    invoke-virtual {v0, v6}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0, v2}, Lcom/osp/app/signin/SignUpView;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "\n"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0, v5}, Lcom/osp/app/signin/SignUpView;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setMessage(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    new-instance v1, Lcom/osp/app/signin/lh;

    invoke-direct {v1, p0}, Lcom/osp/app/signin/lh;-><init>(Lcom/osp/app/signin/SignUpView;)V

    invoke-virtual {v0, v4, v1}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    invoke-virtual {v0, v3}, Landroid/app/AlertDialog$Builder;->setCancelable(Z)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v0

    goto/16 :goto_0

    .line 5405
    :sswitch_5
    new-instance v0, Landroid/app/AlertDialog$Builder;

    invoke-direct {v0, p0}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    invoke-virtual {v0, v6}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0, v2}, Lcom/osp/app/signin/SignUpView;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "\n"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0, v5}, Lcom/osp/app/signin/SignUpView;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setMessage(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    new-instance v1, Lcom/osp/app/signin/li;

    invoke-direct {v1, p0}, Lcom/osp/app/signin/li;-><init>(Lcom/osp/app/signin/SignUpView;)V

    invoke-virtual {v0, v4, v1}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    invoke-virtual {v0, v3}, Landroid/app/AlertDialog$Builder;->setCancelable(Z)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v0

    goto/16 :goto_0

    .line 5423
    :sswitch_6
    invoke-static {}, Lcom/osp/app/util/y;->a()Lcom/osp/app/util/y;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/osp/app/util/y;->a(Landroid/content/Context;)Landroid/app/AlertDialog;

    move-result-object v0

    goto/16 :goto_0

    .line 5426
    :sswitch_7
    const v0, 0x7f0c014b

    invoke-virtual {p0, v0}, Lcom/osp/app/signin/SignUpView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    .line 5427
    invoke-static {}, Lcom/osp/app/util/y;->a()Lcom/osp/app/util/y;

    move-result-object v1

    invoke-virtual {v1, p0, v0}, Lcom/osp/app/util/y;->a(Landroid/content/Context;Landroid/widget/Button;)Landroid/app/AlertDialog;

    move-result-object v0

    goto/16 :goto_0

    .line 5291
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_2
        0x1 -> :sswitch_3
        0x2 -> :sswitch_4
        0x3 -> :sswitch_5
        0x64 -> :sswitch_0
        0x65 -> :sswitch_1
        0x66 -> :sswitch_6
        0x67 -> :sswitch_7
    .end sparse-switch
.end method

.method protected onDestroy()V
    .locals 1

    .prologue
    .line 5440
    invoke-super {p0}, Lcom/osp/app/util/BaseActivity;->onDestroy()V

    .line 5444
    iget-object v0, p0, Lcom/osp/app/signin/SignUpView;->ae:Landroid/app/AlertDialog;

    invoke-direct {p0, v0}, Lcom/osp/app/signin/SignUpView;->a(Landroid/app/AlertDialog;)V

    .line 5445
    iget-object v0, p0, Lcom/osp/app/signin/SignUpView;->Y:Landroid/app/AlertDialog;

    invoke-direct {p0, v0}, Lcom/osp/app/signin/SignUpView;->a(Landroid/app/AlertDialog;)V

    .line 5446
    iget-object v0, p0, Lcom/osp/app/signin/SignUpView;->ap:Landroid/app/AlertDialog;

    invoke-direct {p0, v0}, Lcom/osp/app/signin/SignUpView;->a(Landroid/app/AlertDialog;)V

    .line 5462
    return-void
.end method

.method public onFocusChange(Landroid/view/View;Z)V
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 5656
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "SignUpView::onFocusChange hasFocus : "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;)V

    .line 5658
    iget-boolean v0, p0, Lcom/osp/app/signin/SignUpView;->aX:Z

    if-eqz v0, :cond_0

    if-eqz p2, :cond_0

    .line 5660
    iput-boolean v2, p0, Lcom/osp/app/signin/SignUpView;->aX:Z

    .line 5661
    instance-of v0, p1, Landroid/widget/EditText;

    if-eqz v0, :cond_0

    move-object v0, p1

    .line 5663
    check-cast v0, Landroid/widget/EditText;

    .line 5664
    invoke-virtual {v0}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 5666
    invoke-virtual {v0}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v1

    invoke-interface {v1}, Landroid/text/Editable;->length()I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setSelection(I)V

    .line 5672
    :cond_0
    iget-boolean v0, p0, Lcom/osp/app/signin/SignUpView;->bd:Z

    if-ne v0, v3, :cond_2

    .line 5727
    :cond_1
    :goto_0
    return-void

    .line 5677
    :cond_2
    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v0

    const v1, 0x7f0c0015

    if-ne v0, v1, :cond_3

    if-ne p2, v3, :cond_3

    .line 5679
    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    const-string v1, "input_method"

    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/inputmethod/InputMethodManager;

    .line 5680
    invoke-virtual {p1}, Landroid/view/View;->getWindowToken()Landroid/os/IBinder;

    move-result-object v1

    invoke-virtual {v0, v1, v2}, Landroid/view/inputmethod/InputMethodManager;->hideSoftInputFromWindow(Landroid/os/IBinder;I)Z

    .line 5684
    const/16 v0, 0x64

    const/4 v1, 0x0

    :try_start_0
    invoke-virtual {p0, v0, v1}, Lcom/osp/app/signin/SignUpView;->a(ILandroid/content/DialogInterface$OnDismissListener;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 5685
    :catch_0
    move-exception v0

    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_0

    .line 5689
    :cond_3
    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v0

    const v1, 0x7f0c013a

    if-ne v0, v1, :cond_4

    if-nez p2, :cond_4

    move-object v0, p1

    .line 5691
    check-cast v0, Landroid/widget/EditText;

    invoke-virtual {v0}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-interface {v0}, Landroid/text/Editable;->length()I

    move-result v0

    if-lez v0, :cond_1

    move-object v0, p1

    .line 5696
    check-cast v0, Landroid/widget/EditText;

    invoke-virtual {v0}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v0

    .line 5697
    check-cast p1, Landroid/widget/EditText;

    invoke-virtual {p1, v0}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0

    .line 5701
    :cond_4
    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v0

    const v1, 0x7f0c00ab

    if-eq v0, v1, :cond_5

    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v0

    const v1, 0x7f0c0140

    if-ne v0, v1, :cond_6

    .line 5703
    :cond_5
    if-nez p2, :cond_1

    .line 5705
    check-cast p1, Landroid/widget/EditText;

    invoke-virtual {p1}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-interface {v0}, Landroid/text/Editable;->length()I

    move-result v0

    if-gtz v0, :cond_1

    goto :goto_0

    .line 5714
    :cond_6
    if-nez p2, :cond_1

    move-object v0, p1

    .line 5716
    check-cast v0, Landroid/widget/EditText;

    invoke-virtual {v0}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-interface {v0}, Landroid/text/Editable;->length()I

    move-result v0

    if-lez v0, :cond_1

    .line 5720
    check-cast p1, Landroid/widget/EditText;

    invoke-virtual {p1}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v1, " "

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    goto/16 :goto_0
.end method

.method public onOptionsItemSelected(Landroid/view/MenuItem;)Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 5739
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "SignUpView::onOptionsItemSelected item.getItemId() : "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;)V

    .line 5741
    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v1

    sparse-switch v1, :sswitch_data_0

    .line 5770
    :goto_0
    return v0

    .line 5744
    :sswitch_0
    invoke-virtual {p0}, Lcom/osp/app/signin/SignUpView;->onBackPressed()V

    .line 5770
    :goto_1
    const/4 v0, 0x1

    goto :goto_0

    .line 5748
    :sswitch_1
    invoke-direct {p0}, Lcom/osp/app/signin/SignUpView;->s()V

    goto :goto_1

    .line 5751
    :sswitch_2
    invoke-virtual {p0}, Lcom/osp/app/signin/SignUpView;->getIntent()Landroid/content/Intent;

    move-result-object v1

    .line 5752
    if-eqz v1, :cond_1

    const-string v2, "is_cancelable_just_one_activity"

    invoke-virtual {v1, v2, v0}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 5754
    const-string v0, "key_is_name_verified"

    iget-boolean v2, p0, Lcom/osp/app/signin/SignUpView;->U:Z

    invoke-virtual {v1, v0, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 5755
    iget-boolean v0, p0, Lcom/osp/app/signin/SignUpView;->U:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/osp/app/signin/SignUpView;->bb:Landroid/os/Bundle;

    if-eqz v0, :cond_0

    .line 5757
    const-string v0, "information_after_name_validation"

    iget-object v2, p0, Lcom/osp/app/signin/SignUpView;->bb:Landroid/os/Bundle;

    invoke-virtual {v1, v0, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Bundle;)Landroid/content/Intent;

    .line 5759
    :cond_0
    const/16 v0, 0xe

    invoke-virtual {p0, v0, v1}, Lcom/osp/app/signin/SignUpView;->a(ILandroid/content/Intent;)V

    .line 5764
    :goto_2
    invoke-virtual {p0}, Lcom/osp/app/signin/SignUpView;->finish()V

    goto :goto_1

    .line 5762
    :cond_1
    invoke-virtual {p0, v0}, Lcom/osp/app/signin/SignUpView;->b(I)V

    goto :goto_2

    .line 5741
    :sswitch_data_0
    .sparse-switch
        0x102002c -> :sswitch_0
        0x7f0c018e -> :sswitch_2
        0x7f0c0198 -> :sswitch_1
    .end sparse-switch
.end method

.method protected onPrepareDialog(ILandroid/app/Dialog;)V
    .locals 4

    .prologue
    .line 5479
    const/16 v0, 0x64

    if-ne p1, v0, :cond_0

    move-object v0, p2

    .line 5481
    check-cast v0, Landroid/app/DatePickerDialog;

    iget v1, p0, Lcom/osp/app/signin/SignUpView;->az:I

    iget v2, p0, Lcom/osp/app/signin/SignUpView;->ay:I

    iget v3, p0, Lcom/osp/app/signin/SignUpView;->aA:I

    invoke-virtual {v0, v1, v2, v3}, Landroid/app/DatePickerDialog;->updateDate(III)V

    .line 5482
    iget v0, p0, Lcom/osp/app/signin/SignUpView;->az:I

    iput v0, p0, Lcom/osp/app/signin/SignUpView;->f:I

    .line 5483
    iget v0, p0, Lcom/osp/app/signin/SignUpView;->ay:I

    iput v0, p0, Lcom/osp/app/signin/SignUpView;->e:I

    .line 5484
    iget v0, p0, Lcom/osp/app/signin/SignUpView;->aA:I

    iput v0, p0, Lcom/osp/app/signin/SignUpView;->d:I

    .line 5486
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "SignUpView::onPrepareDialog id : "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " yy : "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lcom/osp/app/signin/SignUpView;->az:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " mm : "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lcom/osp/app/signin/SignUpView;->ay:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " mDd : "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lcom/osp/app/signin/SignUpView;->aA:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;)V

    .line 5488
    :cond_0
    invoke-super {p0, p1, p2}, Lcom/osp/app/util/BaseActivity;->onPrepareDialog(ILandroid/app/Dialog;)V

    .line 5489
    return-void
.end method

.method protected onRestoreInstanceState(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 5563
    invoke-super {p0, p1}, Lcom/osp/app/util/BaseActivity;->onRestoreInstanceState(Landroid/os/Bundle;)V

    .line 5565
    iget-object v0, p0, Lcom/osp/app/signin/SignUpView;->C:Landroid/widget/CheckBox;

    if-nez v0, :cond_0

    .line 5567
    const v0, 0x7f0c0141

    invoke-virtual {p0, v0}, Lcom/osp/app/signin/SignUpView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/CheckBox;

    iput-object v0, p0, Lcom/osp/app/signin/SignUpView;->C:Landroid/widget/CheckBox;

    .line 5569
    :cond_0
    iget-object v0, p0, Lcom/osp/app/signin/SignUpView;->C:Landroid/widget/CheckBox;

    const v1, 0x7f09012a

    invoke-virtual {v0, v1}, Landroid/widget/CheckBox;->setText(I)V

    .line 5571
    iget-object v0, p0, Lcom/osp/app/signin/SignUpView;->B:Landroid/widget/CheckBox;

    if-nez v0, :cond_1

    .line 5573
    const v0, 0x7f0c0025

    invoke-virtual {p0, v0}, Lcom/osp/app/signin/SignUpView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/CheckBox;

    iput-object v0, p0, Lcom/osp/app/signin/SignUpView;->B:Landroid/widget/CheckBox;

    .line 5575
    :cond_1
    iget-object v0, p0, Lcom/osp/app/signin/SignUpView;->B:Landroid/widget/CheckBox;

    const v1, 0x7f090159

    invoke-virtual {v0, v1}, Landroid/widget/CheckBox;->setText(I)V

    .line 5577
    if-eqz p1, :cond_4

    .line 5579
    const-string v0, "OSP_VERSION"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 5580
    if-eqz v0, :cond_2

    .line 5582
    iput-object v0, p0, Lcom/osp/app/signin/SignUpView;->an:Ljava/lang/String;

    .line 5585
    :cond_2
    const-string v0, "SIGN_UP_INFO"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 5586
    if-eqz v0, :cond_3

    .line 5588
    iput-object v0, p0, Lcom/osp/app/signin/SignUpView;->aS:Ljava/lang/String;

    .line 5591
    :cond_3
    const-string v0, "key_setup_wizard_mode"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v0

    invoke-static {v0}, Lcom/osp/app/signin/SamsungService;->a(Z)V

    .line 5592
    const-string v0, "key_is_name_verified"

    const/4 v1, 0x0

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    iput-boolean v0, p0, Lcom/osp/app/signin/SignUpView;->U:Z

    .line 5593
    const-string v0, "information_after_name_validation"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getBundle(Ljava/lang/String;)Landroid/os/Bundle;

    move-result-object v0

    iput-object v0, p0, Lcom/osp/app/signin/SignUpView;->bb:Landroid/os/Bundle;

    .line 5595
    :cond_4
    return-void
.end method

.method protected onResume()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 5497
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/osp/app/signin/SignUpView;->br:J

    .line 5499
    invoke-super {p0}, Lcom/osp/app/util/BaseActivity;->onResume()V

    .line 5501
    iput-boolean v2, p0, Lcom/osp/app/signin/SignUpView;->bv:Z

    .line 5502
    iput-boolean v2, p0, Lcom/osp/app/signin/SignUpView;->X:Z

    .line 5509
    invoke-static {p0}, Lcom/osp/app/util/ad;->d(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 5521
    invoke-virtual {p0}, Lcom/osp/app/signin/SignUpView;->finish()V

    .line 5529
    :cond_0
    invoke-direct {p0}, Lcom/osp/app/signin/SignUpView;->y()V

    .line 5531
    return-void
.end method

.method protected onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 3

    .prologue
    .line 5536
    if-eqz p1, :cond_1

    .line 5538
    const/4 v0, 0x0

    .line 5540
    iget-object v1, p0, Lcom/osp/app/signin/SignUpView;->aq:Lcom/osp/app/signin/SignUpinfo;

    if-eqz v1, :cond_0

    .line 5542
    const-string v0, "SA_EMAIL_;SA_COUNTRY_;SA_BIRTHDATE_;SA_EMAILRECEIVE_;"

    .line 5551
    :cond_0
    const-string v1, "OSP_VERSION"

    iget-object v2, p0, Lcom/osp/app/signin/SignUpView;->an:Ljava/lang/String;

    invoke-virtual {p1, v1, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 5552
    const-string v1, "key_is_name_verified"

    iget-boolean v2, p0, Lcom/osp/app/signin/SignUpView;->U:Z

    invoke-virtual {p1, v1, v2}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 5553
    const-string v1, "key_setup_wizard_mode"

    invoke-static {}, Lcom/osp/app/signin/SamsungService;->a()Z

    move-result v2

    invoke-virtual {p1, v1, v2}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 5554
    const-string v1, "information_after_name_validation"

    iget-object v2, p0, Lcom/osp/app/signin/SignUpView;->bb:Landroid/os/Bundle;

    invoke-virtual {p1, v1, v2}, Landroid/os/Bundle;->putBundle(Ljava/lang/String;Landroid/os/Bundle;)V

    .line 5555
    const-string v1, "SIGN_UP_INFO"

    invoke-virtual {p1, v1, v0}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 5557
    invoke-super {p0, p1}, Lcom/osp/app/util/BaseActivity;->onSaveInstanceState(Landroid/os/Bundle;)V

    .line 5559
    :cond_1
    return-void
.end method
