.class public final Lcom/osp/app/signin/px;
.super Landroid/webkit/WebChromeClient;
.source "WebContentView.java"


# instance fields
.field final synthetic a:Lcom/osp/app/signin/WebContentView;


# direct methods
.method public constructor <init>(Lcom/osp/app/signin/WebContentView;)V
    .locals 0

    .prologue
    .line 732
    iput-object p1, p0, Lcom/osp/app/signin/px;->a:Lcom/osp/app/signin/WebContentView;

    invoke-direct {p0}, Landroid/webkit/WebChromeClient;-><init>()V

    return-void
.end method


# virtual methods
.method public final openFileChooser(Landroid/webkit/ValueCallback;)V
    .locals 1

    .prologue
    .line 736
    const-string v0, ""

    invoke-virtual {p0, p1, v0}, Lcom/osp/app/signin/px;->openFileChooser(Landroid/webkit/ValueCallback;Ljava/lang/String;)V

    .line 737
    return-void
.end method

.method public final openFileChooser(Landroid/webkit/ValueCallback;Ljava/lang/String;)V
    .locals 3

    .prologue
    .line 741
    iget-object v0, p0, Lcom/osp/app/signin/px;->a:Lcom/osp/app/signin/WebContentView;

    invoke-static {v0, p1}, Lcom/osp/app/signin/WebContentView;->a(Lcom/osp/app/signin/WebContentView;Landroid/webkit/ValueCallback;)Landroid/webkit/ValueCallback;

    .line 742
    new-instance v0, Landroid/content/Intent;

    const-string v1, "android.intent.action.GET_CONTENT"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 743
    const-string v1, "android.intent.category.OPENABLE"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->addCategory(Ljava/lang/String;)Landroid/content/Intent;

    .line 744
    const-string v1, "*/*"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setType(Ljava/lang/String;)Landroid/content/Intent;

    .line 745
    iget-object v1, p0, Lcom/osp/app/signin/px;->a:Lcom/osp/app/signin/WebContentView;

    const/16 v2, 0x3e9

    invoke-virtual {v1, v0, v2}, Lcom/osp/app/signin/WebContentView;->startActivityForResult(Landroid/content/Intent;I)V

    .line 746
    return-void
.end method

.method public final openFileChooser(Landroid/webkit/ValueCallback;Ljava/lang/String;Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 750
    const-string v0, ""

    invoke-virtual {p0, p1, v0}, Lcom/osp/app/signin/px;->openFileChooser(Landroid/webkit/ValueCallback;Ljava/lang/String;)V

    .line 751
    return-void
.end method
