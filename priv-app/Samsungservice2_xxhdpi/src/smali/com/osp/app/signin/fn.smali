.class final Lcom/osp/app/signin/fn;
.super Ljava/lang/Object;
.source "HelpPreference.java"

# interfaces
.implements Landroid/preference/Preference$OnPreferenceClickListener;


# instance fields
.field final synthetic a:Lcom/osp/app/signin/HelpPreference;


# direct methods
.method constructor <init>(Lcom/osp/app/signin/HelpPreference;)V
    .locals 0

    .prologue
    .line 94
    iput-object p1, p0, Lcom/osp/app/signin/fn;->a:Lcom/osp/app/signin/HelpPreference;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onPreferenceClick(Landroid/preference/Preference;)Z
    .locals 5

    .prologue
    const/4 v4, 0x1

    .line 98
    iget-object v0, p0, Lcom/osp/app/signin/fn;->a:Lcom/osp/app/signin/HelpPreference;

    iget-object v1, p0, Lcom/osp/app/signin/fn;->a:Lcom/osp/app/signin/HelpPreference;

    invoke-static {v1}, Lcom/osp/app/signin/HelpPreference;->a(Lcom/osp/app/signin/HelpPreference;)Z

    move-result v1

    invoke-static {v0, v1}, Lcom/msc/c/n;->a(Landroid/content/Context;Z)Ljava/lang/String;

    move-result-object v0

    .line 100
    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    .line 101
    new-instance v2, Landroid/content/Intent;

    const-string v3, "android.intent.action.VIEW"

    invoke-direct {v2, v3, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;Landroid/net/Uri;)V

    .line 102
    const-string v1, "com.android.browser.application_id"

    iget-object v3, p0, Lcom/osp/app/signin/fn;->a:Lcom/osp/app/signin/HelpPreference;

    invoke-virtual {v3}, Lcom/osp/app/signin/HelpPreference;->getPackageName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v1, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 104
    iget-object v1, p0, Lcom/osp/app/signin/fn;->a:Lcom/osp/app/signin/HelpPreference;

    invoke-static {v2, v1}, Lcom/osp/app/util/ad;->a(Landroid/content/Intent;Landroid/content/Context;)Z

    move-result v1

    if-ne v1, v4, :cond_0

    .line 106
    iget-object v0, p0, Lcom/osp/app/signin/fn;->a:Lcom/osp/app/signin/HelpPreference;

    invoke-virtual {v0, v2}, Lcom/osp/app/signin/HelpPreference;->startActivity(Landroid/content/Intent;)V

    .line 112
    :goto_0
    return v4

    .line 109
    :cond_0
    iget-object v1, p0, Lcom/osp/app/signin/fn;->a:Lcom/osp/app/signin/HelpPreference;

    invoke-static {v1, v0}, Lcom/osp/app/signin/HelpPreference;->a(Lcom/osp/app/signin/HelpPreference;Ljava/lang/String;)V

    goto :goto_0
.end method
