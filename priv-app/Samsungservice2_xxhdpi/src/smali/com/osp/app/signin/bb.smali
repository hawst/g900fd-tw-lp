.class final Lcom/osp/app/signin/bb;
.super Landroid/os/Handler;
.source "AccountView.java"


# instance fields
.field final synthetic a:Lcom/osp/app/signin/AccountView;

.field private b:I

.field private c:Z

.field private d:Z


# direct methods
.method private constructor <init>(Lcom/osp/app/signin/AccountView;)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 10761
    iput-object p1, p0, Lcom/osp/app/signin/bb;->a:Lcom/osp/app/signin/AccountView;

    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    .line 10763
    const/4 v0, -0x1

    iput v0, p0, Lcom/osp/app/signin/bb;->b:I

    .line 10764
    iput-boolean v1, p0, Lcom/osp/app/signin/bb;->c:Z

    .line 10765
    iput-boolean v1, p0, Lcom/osp/app/signin/bb;->d:Z

    return-void
.end method

.method synthetic constructor <init>(Lcom/osp/app/signin/AccountView;B)V
    .locals 0

    .prologue
    .line 10761
    invoke-direct {p0, p1}, Lcom/osp/app/signin/bb;-><init>(Lcom/osp/app/signin/AccountView;)V

    return-void
.end method


# virtual methods
.method public final a()V
    .locals 1

    .prologue
    .line 10774
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/osp/app/signin/bb;->d:Z

    .line 10775
    return-void
.end method

.method public final a(I)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 10768
    iput p1, p0, Lcom/osp/app/signin/bb;->b:I

    .line 10769
    iget-object v0, p0, Lcom/osp/app/signin/bb;->a:Lcom/osp/app/signin/AccountView;

    invoke-static {v0, v1}, Lcom/osp/app/signin/AccountView;->c(Lcom/osp/app/signin/AccountView;Z)Z

    .line 10770
    iput-boolean v1, p0, Lcom/osp/app/signin/bb;->d:Z

    .line 10771
    return-void
.end method

.method public final handleMessage(Landroid/os/Message;)V
    .locals 8

    .prologue
    const/4 v7, 0x0

    const/4 v0, 0x0

    const/4 v1, 0x1

    .line 10780
    if-nez p1, :cond_0

    .line 10868
    :goto_0
    return-void

    .line 10785
    :cond_0
    iget-boolean v2, p0, Lcom/osp/app/signin/bb;->d:Z

    if-eqz v2, :cond_1

    .line 10787
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v0, "AV"

    const-string v1, "mIsDuplicateResponse = true, no action"

    invoke-static {v0, v1}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 10788
    invoke-virtual {p0, v7}, Lcom/osp/app/signin/bb;->removeCallbacksAndMessages(Ljava/lang/Object;)V

    goto :goto_0

    .line 10792
    :cond_1
    iget v2, p1, Landroid/os/Message;->what:I

    if-ne v2, v1, :cond_7

    .line 10794
    iput-boolean v1, p0, Lcom/osp/app/signin/bb;->d:Z

    .line 10795
    iget-object v2, p0, Lcom/osp/app/signin/bb;->a:Lcom/osp/app/signin/AccountView;

    invoke-static {v2}, Lcom/osp/app/signin/AccountView;->ak(Lcom/osp/app/signin/AccountView;)V

    .line 10796
    iget-object v2, p0, Lcom/osp/app/signin/bb;->a:Lcom/osp/app/signin/AccountView;

    invoke-static {v2}, Lcom/osp/app/signin/AccountView;->al(Lcom/osp/app/signin/AccountView;)V

    .line 10798
    invoke-static {}, Lcom/osp/app/util/m;->a()Lcom/osp/app/util/m;

    invoke-virtual {p1}, Landroid/os/Message;->getData()Landroid/os/Bundle;

    move-result-object v2

    const-string v3, "is_auth"

    invoke-virtual {v2, v3}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v2

    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v3, "ESUU"

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "isAuth for EasySignup : "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;)V

    iput-boolean v2, p0, Lcom/osp/app/signin/bb;->c:Z

    .line 10802
    iget-object v2, p0, Lcom/osp/app/signin/bb;->a:Lcom/osp/app/signin/AccountView;

    invoke-static {}, Lcom/osp/app/util/m;->a()Lcom/osp/app/util/m;

    invoke-virtual {p1}, Landroid/os/Message;->getData()Landroid/os/Bundle;

    move-result-object v3

    const-string v4, "auth_type"

    invoke-virtual {v3, v4}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v4, "ESUU"

    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "auth_type for EasySignup : "

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;)V

    const-string v4, "MO"

    invoke-virtual {v4, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_2

    move v0, v1

    :cond_2
    invoke-static {v2, v0}, Lcom/osp/app/signin/AccountView;->c(Lcom/osp/app/signin/AccountView;Z)Z

    .line 10804
    iget-boolean v0, p0, Lcom/osp/app/signin/bb;->c:Z

    if-ne v0, v1, :cond_5

    iget v0, p0, Lcom/osp/app/signin/bb;->b:I

    if-nez v0, :cond_5

    invoke-static {}, Lcom/osp/device/b;->b()Lcom/osp/device/b;

    move-result-object v0

    invoke-virtual {v0}, Lcom/osp/device/b;->a()Z

    move-result v0

    if-eqz v0, :cond_5

    .line 10806
    iget-object v0, p0, Lcom/osp/app/signin/bb;->a:Lcom/osp/app/signin/AccountView;

    invoke-static {v0}, Lcom/osp/app/signin/AccountView;->am(Lcom/osp/app/signin/AccountView;)V

    .line 10817
    :cond_3
    :goto_1
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v0, "AV"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "mEasySignupTncMode : "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, Lcom/osp/app/signin/bb;->a:Lcom/osp/app/signin/AccountView;

    invoke-static {v2}, Lcom/osp/app/signin/AccountView;->an(Lcom/osp/app/signin/AccountView;)Z

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 10866
    :cond_4
    :goto_2
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "Response esu handleMsg.what : "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v1, p1, Landroid/os/Message;->what:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;)V

    .line 10867
    invoke-virtual {p0, v7}, Lcom/osp/app/signin/bb;->removeCallbacksAndMessages(Ljava/lang/Object;)V

    goto/16 :goto_0

    .line 10809
    :cond_5
    iget v0, p0, Lcom/osp/app/signin/bb;->b:I

    if-ne v0, v1, :cond_6

    .line 10811
    iget-object v0, p0, Lcom/osp/app/signin/bb;->a:Lcom/osp/app/signin/AccountView;

    invoke-static {v0}, Lcom/osp/app/signin/AccountView;->ag(Lcom/osp/app/signin/AccountView;)V

    goto :goto_1

    .line 10812
    :cond_6
    iget v0, p0, Lcom/osp/app/signin/bb;->b:I

    if-nez v0, :cond_3

    .line 10814
    iget-object v0, p0, Lcom/osp/app/signin/bb;->a:Lcom/osp/app/signin/AccountView;

    invoke-static {v0}, Lcom/osp/app/signin/AccountView;->N(Lcom/osp/app/signin/AccountView;)V

    goto :goto_1

    .line 10849
    :cond_7
    iget v2, p1, Landroid/os/Message;->what:I

    const/4 v3, 0x3

    if-ne v2, v3, :cond_4

    .line 10851
    iput-boolean v1, p0, Lcom/osp/app/signin/bb;->d:Z

    .line 10852
    iget-object v1, p0, Lcom/osp/app/signin/bb;->a:Lcom/osp/app/signin/AccountView;

    invoke-static {v1}, Lcom/osp/app/signin/AccountView;->ak(Lcom/osp/app/signin/AccountView;)V

    .line 10853
    iget-object v1, p0, Lcom/osp/app/signin/bb;->a:Lcom/osp/app/signin/AccountView;

    invoke-static {v1}, Lcom/osp/app/signin/AccountView;->al(Lcom/osp/app/signin/AccountView;)V

    .line 10862
    invoke-static {}, Lcom/osp/app/util/ap;->a()Lcom/osp/app/util/ap;

    iget-object v1, p0, Lcom/osp/app/signin/bb;->a:Lcom/osp/app/signin/AccountView;

    iget-object v2, p0, Lcom/osp/app/signin/bb;->a:Lcom/osp/app/signin/AccountView;

    const v3, 0x7f09004a

    invoke-virtual {v2, v3}, Lcom/osp/app/signin/AccountView;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2, v0}, Lcom/osp/app/util/ap;->a(Landroid/content/Context;Ljava/lang/String;I)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    .line 10863
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v0, "AV"

    const-string v1, "is_auth for esu time out"

    invoke-static {v0, v1}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_2
.end method
