.class public Lcom/osp/app/signin/PasswordChangeView;
.super Lcom/osp/app/util/BaseActivity;
.source "PasswordChangeView.java"

# interfaces
.implements Landroid/view/View$OnFocusChangeListener;


# instance fields
.field private A:Landroid/widget/Button;

.field private B:Landroid/widget/Button;

.field private final C:Ljava/lang/String;

.field private final D:Ljava/lang/String;

.field private final E:Landroid/view/View$OnClickListener;

.field private final F:Landroid/view/View$OnClickListener;

.field private final a:Landroid/os/Handler;

.field private b:Lcom/osp/app/signin/gp;

.field private c:Lcom/osp/app/signin/gr;

.field private d:Ljava/lang/String;

.field private e:Lcom/osp/app/signin/BottomSoftkeyLinearLayout;

.field private f:Z

.field private y:Landroid/app/AlertDialog;

.field private z:Landroid/app/AlertDialog;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 63
    invoke-direct {p0}, Lcom/osp/app/util/BaseActivity;-><init>()V

    .line 70
    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    iput-object v0, p0, Lcom/osp/app/signin/PasswordChangeView;->a:Landroid/os/Handler;

    .line 91
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/osp/app/signin/PasswordChangeView;->d:Ljava/lang/String;

    .line 104
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/osp/app/signin/PasswordChangeView;->f:Z

    .line 118
    const-string v0, "save"

    iput-object v0, p0, Lcom/osp/app/signin/PasswordChangeView;->C:Ljava/lang/String;

    .line 119
    const-string v0, "cancel"

    iput-object v0, p0, Lcom/osp/app/signin/PasswordChangeView;->D:Ljava/lang/String;

    .line 121
    new-instance v0, Lcom/osp/app/signin/gh;

    invoke-direct {v0, p0}, Lcom/osp/app/signin/gh;-><init>(Lcom/osp/app/signin/PasswordChangeView;)V

    iput-object v0, p0, Lcom/osp/app/signin/PasswordChangeView;->E:Landroid/view/View$OnClickListener;

    .line 128
    new-instance v0, Lcom/osp/app/signin/gi;

    invoke-direct {v0, p0}, Lcom/osp/app/signin/gi;-><init>(Lcom/osp/app/signin/PasswordChangeView;)V

    iput-object v0, p0, Lcom/osp/app/signin/PasswordChangeView;->F:Landroid/view/View$OnClickListener;

    .line 1900
    return-void
.end method

.method static synthetic a(Lcom/osp/app/signin/PasswordChangeView;)Landroid/app/AlertDialog;
    .locals 1

    .prologue
    .line 63
    iget-object v0, p0, Lcom/osp/app/signin/PasswordChangeView;->z:Landroid/app/AlertDialog;

    return-object v0
.end method

.method static synthetic a(Lcom/osp/app/signin/PasswordChangeView;Landroid/app/AlertDialog;)Landroid/app/AlertDialog;
    .locals 0

    .prologue
    .line 63
    iput-object p1, p0, Lcom/osp/app/signin/PasswordChangeView;->z:Landroid/app/AlertDialog;

    return-object p1
.end method

.method private a(I)V
    .locals 6

    .prologue
    const v4, 0x7f08011e

    .line 1563
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "PCV::initLayoutParams orientation : "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;)V

    .line 1565
    invoke-static {}, Lcom/osp/device/b;->b()Lcom/osp/device/b;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/osp/device/b;->i(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 1567
    const v0, 0x7f0c0171

    invoke-virtual {p0, v0}, Lcom/osp/app/signin/PasswordChangeView;->findViewById(I)Landroid/view/View;

    move-result-object v2

    .line 1569
    invoke-virtual {p0}, Lcom/osp/app/signin/PasswordChangeView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f08011f

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v0

    float-to-int v0, v0

    .line 1570
    invoke-virtual {p0}, Lcom/osp/app/signin/PasswordChangeView;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1, v4}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v1

    float-to-int v1, v1

    .line 1571
    if-eqz v2, :cond_1

    .line 1573
    invoke-static {}, Lcom/osp/app/signin/SamsungService;->a()Z

    move-result v3

    if-nez v3, :cond_0

    .line 1575
    invoke-virtual {p0}, Lcom/osp/app/signin/PasswordChangeView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0, v4}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v0

    float-to-int v0, v0

    .line 1578
    :cond_0
    invoke-static {}, Lcom/osp/app/util/q;->a()Lcom/osp/app/util/q;

    invoke-static {v2, v0, v1, p1}, Lcom/osp/app/util/q;->b(Landroid/view/View;III)V

    .line 1582
    :cond_1
    invoke-static {p0}, Lcom/osp/app/util/r;->k(Landroid/content/Context;)Z

    move-result v2

    if-nez v2, :cond_5

    .line 1584
    invoke-virtual {p0}, Lcom/osp/app/signin/PasswordChangeView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f08013d

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v0

    float-to-int v1, v0

    .line 1585
    invoke-virtual {p0}, Lcom/osp/app/signin/PasswordChangeView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v2, 0x7f08013c

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v0

    float-to-int v0, v0

    .line 1588
    :goto_0
    invoke-static {}, Lcom/osp/app/util/r;->v()Z

    move-result v2

    if-eqz v2, :cond_2

    .line 1590
    const v2, 0x7f0c0105

    invoke-virtual {p0, v2}, Lcom/osp/app/signin/PasswordChangeView;->findViewById(I)Landroid/view/View;

    move-result-object v2

    const/4 v3, 0x4

    invoke-virtual {v2, v3}, Landroid/view/View;->setVisibility(I)V

    .line 1593
    :cond_2
    iget-object v2, p0, Lcom/osp/app/signin/PasswordChangeView;->e:Lcom/osp/app/signin/BottomSoftkeyLinearLayout;

    if-eqz v2, :cond_4

    .line 1595
    invoke-static {}, Lcom/osp/app/signin/SamsungService;->a()Z

    move-result v2

    if-eqz v2, :cond_3

    sget-object v2, Lcom/osp/app/util/u;->o:[Ljava/lang/String;

    invoke-static {v2}, Lcom/osp/app/util/u;->a([Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_3

    .line 1597
    const/4 v1, 0x0

    .line 1599
    :cond_3
    invoke-static {}, Lcom/osp/app/util/q;->a()Lcom/osp/app/util/q;

    iget-object v2, p0, Lcom/osp/app/signin/PasswordChangeView;->e:Lcom/osp/app/signin/BottomSoftkeyLinearLayout;

    invoke-static {p0, v2, v1, v0, p1}, Lcom/osp/app/util/q;->a(Landroid/content/Context;Landroid/view/View;III)V

    .line 1602
    :cond_4
    return-void

    :cond_5
    move v5, v1

    move v1, v0

    move v0, v5

    goto :goto_0
.end method

.method static synthetic a(Lcom/osp/app/signin/PasswordChangeView;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 63
    invoke-direct {p0, p1}, Lcom/osp/app/signin/PasswordChangeView;->a(Ljava/lang/String;)V

    return-void
.end method

.method private a(Ljava/lang/String;)V
    .locals 7

    .prologue
    const/4 v6, 0x1

    const/4 v5, 0x0

    .line 2421
    const-string v0, "save"

    invoke-virtual {p1, v0}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_6

    .line 2423
    invoke-direct {p0}, Lcom/osp/app/signin/PasswordChangeView;->d()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 2425
    const-string v0, "input_method"

    invoke-virtual {p0, v0}, Lcom/osp/app/signin/PasswordChangeView;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/inputmethod/InputMethodManager;

    const v1, 0x7f0c010c

    invoke-virtual {p0, v1}, Lcom/osp/app/signin/PasswordChangeView;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/EditText;

    const v2, 0x7f0c0108

    invoke-virtual {p0, v2}, Lcom/osp/app/signin/PasswordChangeView;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/EditText;

    const v3, 0x7f0c0104

    invoke-virtual {p0, v3}, Lcom/osp/app/signin/PasswordChangeView;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/EditText;

    if-eqz v3, :cond_2

    invoke-virtual {v3}, Landroid/widget/EditText;->hasFocus()Z

    move-result v4

    if-ne v4, v6, :cond_2

    invoke-virtual {v3}, Landroid/widget/EditText;->getWindowToken()Landroid/os/IBinder;

    move-result-object v1

    invoke-virtual {v0, v1, v5}, Landroid/view/inputmethod/InputMethodManager;->hideSoftInputFromWindow(Landroid/os/IBinder;I)Z

    .line 2426
    :cond_0
    :goto_0
    const-string v0, "OSP_01"

    iget-object v1, p0, Lcom/osp/app/signin/PasswordChangeView;->d:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 2428
    new-instance v0, Lcom/osp/app/signin/gp;

    invoke-direct {v0, p0}, Lcom/osp/app/signin/gp;-><init>(Lcom/osp/app/signin/PasswordChangeView;)V

    iput-object v0, p0, Lcom/osp/app/signin/PasswordChangeView;->b:Lcom/osp/app/signin/gp;

    .line 2429
    iget-object v0, p0, Lcom/osp/app/signin/PasswordChangeView;->b:Lcom/osp/app/signin/gp;

    invoke-virtual {v0}, Lcom/osp/app/signin/gp;->b()V

    .line 2455
    :cond_1
    :goto_1
    return-void

    .line 2425
    :cond_2
    if-eqz v2, :cond_3

    invoke-virtual {v2}, Landroid/widget/EditText;->hasFocus()Z

    move-result v3

    if-ne v3, v6, :cond_3

    invoke-virtual {v2}, Landroid/widget/EditText;->getWindowToken()Landroid/os/IBinder;

    move-result-object v1

    invoke-virtual {v0, v1, v5}, Landroid/view/inputmethod/InputMethodManager;->hideSoftInputFromWindow(Landroid/os/IBinder;I)Z

    goto :goto_0

    :cond_3
    if-eqz v1, :cond_0

    invoke-virtual {v1}, Landroid/widget/EditText;->hasFocus()Z

    move-result v2

    if-ne v2, v6, :cond_0

    invoke-virtual {v1}, Landroid/widget/EditText;->getWindowToken()Landroid/os/IBinder;

    move-result-object v1

    invoke-virtual {v0, v1, v5}, Landroid/view/inputmethod/InputMethodManager;->hideSoftInputFromWindow(Landroid/os/IBinder;I)Z

    goto :goto_0

    .line 2432
    :cond_4
    invoke-static {p0}, Lcom/msc/c/e;->h(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v1

    .line 2433
    invoke-static {}, Lcom/osp/device/b;->b()Lcom/osp/device/b;

    move-result-object v0

    invoke-virtual {v0}, Lcom/osp/device/b;->a()Z

    move-result v0

    if-eqz v0, :cond_5

    invoke-virtual {p0}, Lcom/osp/app/signin/PasswordChangeView;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string v2, "key_login_id"

    invoke-virtual {v0, v2}, Landroid/content/Intent;->hasExtra(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_5

    .line 2440
    invoke-virtual {p0}, Lcom/osp/app/signin/PasswordChangeView;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string v2, "key_login_id"

    invoke-virtual {v0, v2}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 2446
    :goto_2
    new-instance v2, Lcom/osp/app/signin/gr;

    invoke-direct {v2, p0, v1, v0}, Lcom/osp/app/signin/gr;-><init>(Lcom/osp/app/signin/PasswordChangeView;Ljava/lang/String;Ljava/lang/String;)V

    iput-object v2, p0, Lcom/osp/app/signin/PasswordChangeView;->c:Lcom/osp/app/signin/gr;

    .line 2447
    iget-object v0, p0, Lcom/osp/app/signin/PasswordChangeView;->c:Lcom/osp/app/signin/gr;

    invoke-virtual {v0}, Lcom/osp/app/signin/gr;->b()V

    goto :goto_1

    .line 2443
    :cond_5
    invoke-static {p0}, Lcom/osp/app/util/ad;->h(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v0

    goto :goto_2

    .line 2450
    :cond_6
    const-string v0, "cancel"

    invoke-virtual {p1, v0}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 2452
    invoke-virtual {p0}, Lcom/osp/app/signin/PasswordChangeView;->finish()V

    goto :goto_1
.end method

.method static synthetic b(Lcom/osp/app/signin/PasswordChangeView;)Landroid/os/Handler;
    .locals 1

    .prologue
    .line 63
    iget-object v0, p0, Lcom/osp/app/signin/PasswordChangeView;->a:Landroid/os/Handler;

    return-object v0
.end method

.method private c()Landroid/widget/Button;
    .locals 1

    .prologue
    .line 572
    invoke-static {}, Lcom/osp/device/b;->b()Lcom/osp/device/b;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/osp/device/b;->l(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 574
    const v0, 0x7f0c0110

    invoke-virtual {p0, v0}, Lcom/osp/app/signin/PasswordChangeView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    .line 579
    :goto_0
    return-object v0

    .line 577
    :cond_0
    const v0, 0x7f0c0172

    invoke-virtual {p0, v0}, Lcom/osp/app/signin/PasswordChangeView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    goto :goto_0
.end method

.method static synthetic c(Lcom/osp/app/signin/PasswordChangeView;)V
    .locals 3

    .prologue
    .line 63
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    const-class v1, Lcom/osp/app/signin/SignInView;

    invoke-virtual {v0, p0, v1}, Landroid/content/Intent;->setClass(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;

    const-string v1, "MODE"

    const-string v2, "ADD_ACCOUNT"

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    invoke-virtual {p0, v0}, Lcom/osp/app/signin/PasswordChangeView;->startActivity(Landroid/content/Intent;)V

    return-void
.end method

.method static synthetic d(Lcom/osp/app/signin/PasswordChangeView;)V
    .locals 3

    .prologue
    .line 63
    new-instance v0, Landroid/content/Intent;

    const-string v1, "com.msc.action.samsungaccount.resignin_with_signout"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    sget v1, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v2, 0xa

    if-le v1, v2, :cond_0

    const v1, 0x10008000

    invoke-virtual {v0, v1}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    :cond_0
    const-string v1, "client_id"

    const-string v2, "j5p7ll8g33"

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    const-string v1, "client_secret"

    const-string v2, "5763D0052DC1462E13751F753384E9A9"

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    const-string v1, "OSP_VER"

    const-string v2, "OSP_02"

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    invoke-virtual {p0, v0}, Lcom/osp/app/signin/PasswordChangeView;->startActivity(Landroid/content/Intent;)V

    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v0, "PCV"

    const-string v1, "Sign out and show resign in for id changed"

    invoke-static {v0, v1}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method

.method private d()Z
    .locals 8

    .prologue
    const/4 v4, 0x1

    const/4 v2, 0x0

    .line 760
    .line 761
    const/4 v3, 0x0

    .line 763
    invoke-static {p0}, Lcom/osp/app/util/ad;->a(Landroid/content/Context;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 765
    invoke-static {}, Lcom/osp/app/util/ap;->a()Lcom/osp/app/util/ap;

    const v0, 0x7f09003e

    invoke-virtual {p0, v0}, Lcom/osp/app/signin/PasswordChangeView;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-static {p0, v0, v2}, Lcom/osp/app/util/ap;->a(Landroid/content/Context;Ljava/lang/String;I)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    move v0, v2

    .line 918
    :cond_0
    :goto_0
    return v0

    .line 771
    :cond_1
    const v0, 0x7f0c0108

    invoke-virtual {p0, v0}, Lcom/osp/app/signin/PasswordChangeView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/EditText;

    .line 773
    if-nez v0, :cond_2

    .line 777
    invoke-static {}, Lcom/osp/app/util/ap;->a()Lcom/osp/app/util/ap;

    const v0, 0x7f09004a

    invoke-virtual {p0, v0}, Lcom/osp/app/signin/PasswordChangeView;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-static {p0, v0, v2}, Lcom/osp/app/util/ap;->a(Landroid/content/Context;Ljava/lang/String;I)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    move v0, v2

    .line 778
    goto :goto_0

    .line 781
    :cond_2
    invoke-virtual {v0}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v5

    .line 806
    :try_start_0
    new-instance v1, Lcom/osp/security/identity/g;

    invoke-direct {v1, p0}, Lcom/osp/security/identity/g;-><init>(Landroid/content/Context;)V

    .line 808
    invoke-static {p0}, Lcom/msc/c/e;->e(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v1, v6, v5}, Lcom/osp/security/identity/g;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/osp/security/identity/h;

    move-result-object v6

    .line 813
    const v1, 0x7f0c0104

    invoke-virtual {p0, v1}, Lcom/osp/app/signin/PasswordChangeView;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/EditText;

    .line 815
    if-eqz v1, :cond_4

    invoke-virtual {v1}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v7

    invoke-interface {v7}, Landroid/text/Editable;->length()I

    move-result v7

    if-lez v7, :cond_4

    invoke-virtual {v1}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_4

    .line 817
    const v1, 0x7f090093

    invoke-virtual {p0, v1}, Lcom/osp/app/signin/PasswordChangeView;->getText(I)Ljava/lang/CharSequence;

    move-result-object v1

    invoke-interface {v1}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;
    :try_end_0
    .catch Lcom/osp/security/identity/IdentityException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v3

    move-object v1, v3

    .line 893
    :goto_1
    if-nez v2, :cond_3

    .line 895
    :try_start_1
    sget-object v3, Lcom/osp/security/identity/h;->a:Lcom/osp/security/identity/h;

    if-eq v6, v3, :cond_3

    .line 897
    invoke-virtual {v0}, Landroid/widget/EditText;->requestFocus()Z
    :try_end_1
    .catch Lcom/osp/security/identity/IdentityException; {:try_start_1 .. :try_end_1} :catch_1

    :cond_3
    move v0, v2

    .line 910
    :goto_2
    if-nez v0, :cond_0

    if-eqz v1, :cond_0

    iget-object v2, p0, Lcom/osp/app/signin/PasswordChangeView;->y:Landroid/app/AlertDialog;

    invoke-virtual {v2}, Landroid/app/AlertDialog;->isShowing()Z

    move-result v2

    if-nez v2, :cond_0

    .line 914
    iget-object v2, p0, Lcom/osp/app/signin/PasswordChangeView;->y:Landroid/app/AlertDialog;

    invoke-virtual {v2, v1}, Landroid/app/AlertDialog;->setMessage(Ljava/lang/CharSequence;)V

    .line 915
    iget-object v1, p0, Lcom/osp/app/signin/PasswordChangeView;->y:Landroid/app/AlertDialog;

    invoke-virtual {v1}, Landroid/app/AlertDialog;->show()V

    goto/16 :goto_0

    .line 818
    :cond_4
    :try_start_2
    sget-object v1, Lcom/osp/security/identity/h;->f:Lcom/osp/security/identity/h;

    if-ne v6, v1, :cond_5

    invoke-virtual {v5}, Ljava/lang/String;->length()I

    move-result v1

    if-eqz v1, :cond_5

    .line 822
    const v1, 0x7f090090

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    const-string v7, "~!@#$%^&*-_+=|\'\";:[]{}()<>,./?\\"

    aput-object v7, v4, v5

    invoke-virtual {p0, v1, v4}, Lcom/osp/app/signin/PasswordChangeView;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    move-object v1, v3

    goto :goto_1

    .line 823
    :cond_5
    sget-object v1, Lcom/osp/security/identity/h;->g:Lcom/osp/security/identity/h;

    if-ne v6, v1, :cond_6

    .line 827
    const v1, 0x7f09018d

    invoke-virtual {p0, v1}, Lcom/osp/app/signin/PasswordChangeView;->getString(I)Ljava/lang/String;

    move-result-object v3

    move-object v1, v3

    goto :goto_1

    .line 832
    :cond_6
    sget-object v1, Lcom/osp/security/identity/h;->h:Lcom/osp/security/identity/h;

    if-ne v6, v1, :cond_7

    .line 834
    const v1, 0x7f0900c8

    const/4 v4, 0x2

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    const/16 v7, 0x8

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    aput-object v7, v4, v5

    const/4 v5, 0x1

    const/16 v7, 0xf

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    aput-object v7, v4, v5

    invoke-virtual {p0, v1, v4}, Lcom/osp/app/signin/PasswordChangeView;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    move-object v1, v3

    goto :goto_1

    .line 836
    :cond_7
    sget-object v1, Lcom/osp/security/identity/h;->c:Lcom/osp/security/identity/h;

    if-eq v6, v1, :cond_8

    sget-object v1, Lcom/osp/security/identity/h;->b:Lcom/osp/security/identity/h;

    if-ne v6, v1, :cond_9

    .line 838
    :cond_8
    invoke-virtual {v5}, Ljava/lang/String;->length()I

    move-result v1

    if-eqz v1, :cond_b

    .line 868
    const v1, 0x7f0900c8

    const/4 v4, 0x2

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    const/16 v7, 0x8

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    aput-object v7, v4, v5

    const/4 v5, 0x1

    const/16 v7, 0xf

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    aput-object v7, v4, v5

    invoke-virtual {p0, v1, v4}, Lcom/osp/app/signin/PasswordChangeView;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    move-object v1, v3

    goto/16 :goto_1

    .line 872
    :cond_9
    sget-object v1, Lcom/osp/security/identity/h;->e:Lcom/osp/security/identity/h;

    if-ne v6, v1, :cond_a

    invoke-virtual {v5}, Ljava/lang/String;->length()I

    move-result v1

    if-eqz v1, :cond_a

    .line 874
    invoke-virtual {p0}, Lcom/osp/app/signin/PasswordChangeView;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v4, 0x7f090142

    invoke-virtual {v1, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    const/4 v7, 0x3

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    aput-object v7, v4, v5

    invoke-static {v1, v4}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    move-object v1, v3

    goto/16 :goto_1

    .line 885
    :cond_a
    invoke-virtual {v5}, Ljava/lang/String;->length()I
    :try_end_2
    .catch Lcom/osp/security/identity/IdentityException; {:try_start_2 .. :try_end_2} :catch_0

    move-result v1

    if-eqz v1, :cond_b

    move-object v1, v3

    move v2, v4

    .line 889
    goto/16 :goto_1

    .line 905
    :catch_0
    move-exception v0

    move-object v1, v3

    :goto_3
    invoke-virtual {v0}, Lcom/osp/security/identity/IdentityException;->printStackTrace()V

    move v0, v2

    goto/16 :goto_2

    :catch_1
    move-exception v0

    goto :goto_3

    :cond_b
    move-object v1, v3

    goto/16 :goto_1
.end method


# virtual methods
.method public final a()V
    .locals 6

    .prologue
    const v5, 0x7f0c018f

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 605
    const v0, 0x7f0c0108

    invoke-virtual {p0, v0}, Lcom/osp/app/signin/PasswordChangeView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/EditText;

    .line 606
    const v1, 0x7f0c0104

    invoke-virtual {p0, v1}, Lcom/osp/app/signin/PasswordChangeView;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/EditText;

    .line 607
    invoke-direct {p0}, Lcom/osp/app/signin/PasswordChangeView;->c()Landroid/widget/Button;

    move-result-object v2

    .line 609
    if-eqz v1, :cond_0

    invoke-virtual {v1}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v1

    invoke-interface {v1}, Landroid/text/Editable;->length()I

    move-result v1

    if-eqz v1, :cond_1

    :cond_0
    if-eqz v0, :cond_6

    invoke-virtual {v0}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-interface {v0}, Landroid/text/Editable;->length()I

    move-result v0

    if-nez v0, :cond_6

    .line 614
    :cond_1
    iget-boolean v0, p0, Lcom/osp/app/signin/PasswordChangeView;->f:Z

    if-nez v0, :cond_5

    .line 616
    if-eqz v2, :cond_2

    .line 618
    invoke-virtual {v2, v3}, Landroid/widget/Button;->setEnabled(Z)V

    .line 620
    :cond_2
    iget-object v0, p0, Lcom/osp/app/signin/PasswordChangeView;->x:Landroid/view/Menu;

    if-eqz v0, :cond_3

    .line 622
    iget-object v0, p0, Lcom/osp/app/signin/PasswordChangeView;->x:Landroid/view/Menu;

    invoke-interface {v0, v5}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v0

    invoke-interface {v0, v3}, Landroid/view/MenuItem;->setEnabled(Z)Landroid/view/MenuItem;

    .line 624
    :cond_3
    iget-object v0, p0, Lcom/osp/app/signin/PasswordChangeView;->B:Landroid/widget/Button;

    if-eqz v0, :cond_4

    .line 626
    iget-object v0, p0, Lcom/osp/app/signin/PasswordChangeView;->B:Landroid/widget/Button;

    invoke-virtual {v0, v3}, Landroid/widget/Button;->setEnabled(Z)V

    .line 659
    :cond_4
    :goto_0
    return-void

    .line 630
    :cond_5
    iget-object v0, p0, Lcom/osp/app/signin/PasswordChangeView;->e:Lcom/osp/app/signin/BottomSoftkeyLinearLayout;

    if-eqz v0, :cond_4

    .line 632
    iget-object v0, p0, Lcom/osp/app/signin/PasswordChangeView;->e:Lcom/osp/app/signin/BottomSoftkeyLinearLayout;

    invoke-static {v3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/osp/app/signin/BottomSoftkeyLinearLayout;->b(Ljava/lang/Boolean;)V

    goto :goto_0

    .line 637
    :cond_6
    iget-boolean v0, p0, Lcom/osp/app/signin/PasswordChangeView;->f:Z

    if-nez v0, :cond_9

    .line 639
    if-eqz v2, :cond_7

    .line 641
    invoke-virtual {v2, v4}, Landroid/widget/Button;->setEnabled(Z)V

    .line 643
    :cond_7
    iget-object v0, p0, Lcom/osp/app/signin/PasswordChangeView;->x:Landroid/view/Menu;

    if-eqz v0, :cond_8

    .line 645
    iget-object v0, p0, Lcom/osp/app/signin/PasswordChangeView;->x:Landroid/view/Menu;

    invoke-interface {v0, v5}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v0

    invoke-interface {v0, v4}, Landroid/view/MenuItem;->setEnabled(Z)Landroid/view/MenuItem;

    .line 647
    :cond_8
    iget-object v0, p0, Lcom/osp/app/signin/PasswordChangeView;->B:Landroid/widget/Button;

    if-eqz v0, :cond_4

    .line 649
    iget-object v0, p0, Lcom/osp/app/signin/PasswordChangeView;->B:Landroid/widget/Button;

    invoke-virtual {v0, v4}, Landroid/widget/Button;->setEnabled(Z)V

    goto :goto_0

    .line 653
    :cond_9
    iget-object v0, p0, Lcom/osp/app/signin/PasswordChangeView;->e:Lcom/osp/app/signin/BottomSoftkeyLinearLayout;

    if-eqz v0, :cond_4

    .line 655
    iget-object v0, p0, Lcom/osp/app/signin/PasswordChangeView;->e:Lcom/osp/app/signin/BottomSoftkeyLinearLayout;

    invoke-static {v4}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/osp/app/signin/BottomSoftkeyLinearLayout;->b(Ljava/lang/Boolean;)V

    goto :goto_0
.end method

.method protected final b()V
    .locals 4

    .prologue
    .line 2252
    invoke-static {}, Lcom/osp/app/signin/SamsungService;->a()Z

    move-result v0

    if-nez v0, :cond_1

    .line 2254
    invoke-static {p0}, Lcom/osp/app/util/r;->m(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 2256
    invoke-virtual {p0}, Lcom/osp/app/signin/PasswordChangeView;->h()Z

    const v0, 0x7f090019

    invoke-virtual {p0, v0}, Lcom/osp/app/signin/PasswordChangeView;->getString(I)Ljava/lang/String;

    move-result-object v0

    const v1, 0x7f090052

    invoke-virtual {p0, v1}, Lcom/osp/app/signin/PasswordChangeView;->getString(I)Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/osp/app/signin/PasswordChangeView;->F:Landroid/view/View$OnClickListener;

    iget-object v3, p0, Lcom/osp/app/signin/PasswordChangeView;->E:Landroid/view/View$OnClickListener;

    invoke-static {p0, v0, v1, v2, v3}, Lcom/msc/sa/c/d;->a(Landroid/app/Activity;Ljava/lang/String;Ljava/lang/String;Landroid/view/View$OnClickListener;Landroid/view/View$OnClickListener;)V

    .line 2258
    const v0, 0x7f0c0081

    invoke-static {p0, v0}, Lcom/msc/sa/c/d;->b(Landroid/app/Activity;I)Landroid/view/View;

    move-result-object v0

    .line 2259
    if-eqz v0, :cond_0

    .line 2261
    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lcom/osp/app/signin/PasswordChangeView;->A:Landroid/widget/Button;

    .line 2263
    :cond_0
    const v0, 0x7f0c0082

    invoke-static {p0, v0}, Lcom/msc/sa/c/d;->b(Landroid/app/Activity;I)Landroid/view/View;

    move-result-object v0

    .line 2264
    if-eqz v0, :cond_1

    .line 2266
    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lcom/osp/app/signin/PasswordChangeView;->B:Landroid/widget/Button;

    .line 2271
    :cond_1
    invoke-virtual {p0}, Lcom/osp/app/signin/PasswordChangeView;->a()V

    .line 2272
    return-void
.end method

.method protected onActivityResult(IILandroid/content/Intent;)V
    .locals 4

    .prologue
    const/16 v3, 0x10

    .line 1379
    invoke-super {p0, p1, p2, p3}, Lcom/osp/app/util/BaseActivity;->onActivityResult(IILandroid/content/Intent;)V

    .line 1381
    if-ne p2, v3, :cond_0

    .line 1383
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v0, "PCV"

    const-string v1, "PasswordChangeView - onActivityResult - requestCode :"

    const-string v2, "RESULT_EXPIRED_TOKEN"

    invoke-static {v0, v1, v2}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 1384
    invoke-virtual {p0, v3}, Lcom/osp/app/signin/PasswordChangeView;->b(I)V

    .line 1385
    invoke-virtual {p0}, Lcom/osp/app/signin/PasswordChangeView;->finish()V

    .line 1390
    :goto_0
    return-void

    .line 1388
    :cond_0
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "PasswordChangeView - onActivityResult - requestCode :"

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;)V

    .line 1389
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "PasswordChangeView - onActivityResult - resultCode :"

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public onConfigurationChanged(Landroid/content/res/Configuration;)V
    .locals 1

    .prologue
    .line 1550
    invoke-super {p0, p1}, Lcom/osp/app/util/BaseActivity;->onConfigurationChanged(Landroid/content/res/Configuration;)V

    .line 1553
    iget v0, p1, Landroid/content/res/Configuration;->orientation:I

    invoke-direct {p0, v0}, Lcom/osp/app/signin/PasswordChangeView;->a(I)V

    .line 1554
    return-void
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 9

    .prologue
    const/4 v8, 0x1

    const v4, 0x7f090052

    const v7, 0x7f090019

    const/16 v6, 0x8

    const/4 v5, 0x0

    .line 983
    invoke-super {p0, p1}, Lcom/osp/app/util/BaseActivity;->onCreate(Landroid/os/Bundle;)V

    .line 987
    invoke-virtual {p0}, Lcom/osp/app/signin/PasswordChangeView;->getApplication()Landroid/app/Application;

    move-result-object v0

    check-cast v0, Lcom/osp/app/signin/SamsungService;

    invoke-virtual {v0, p0}, Lcom/osp/app/signin/SamsungService;->a(Landroid/app/Activity;)V

    .line 997
    invoke-static {p0}, Lcom/osp/app/util/ad;->a(Landroid/content/Context;)Z

    move-result v0

    if-ne v0, v8, :cond_0

    invoke-static {}, Lcom/osp/app/signin/SamsungService;->a()Z

    move-result v0

    if-nez v0, :cond_0

    .line 999
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v0, "Self Update check"

    invoke-static {v0}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;)V

    .line 1001
    invoke-static {}, Lcom/msc/sa/selfupdate/c;->b()Lcom/msc/sa/selfupdate/c;

    move-result-object v0

    invoke-virtual {p0}, Lcom/osp/app/signin/PasswordChangeView;->getApplication()Landroid/app/Application;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v8, v5, v2}, Lcom/msc/sa/selfupdate/c;->a(Landroid/app/Application;ZZLjava/lang/String;)V

    .line 1005
    :cond_0
    invoke-static {}, Lcom/osp/app/signin/SamsungService;->a()Z

    move-result v0

    iput-boolean v0, p0, Lcom/osp/app/signin/PasswordChangeView;->f:Z

    .line 1008
    const v0, 0x7f030027

    invoke-static {p0}, Lcom/osp/app/util/r;->q(Landroid/content/Context;)Z

    move-result v1

    invoke-virtual {p0, v0, v1}, Lcom/osp/app/signin/PasswordChangeView;->a(IZ)V

    .line 1010
    invoke-static {p0}, Lcom/osp/app/util/ad;->d(Landroid/content/Context;)Z

    move-result v0

    if-nez v0, :cond_2

    .line 1012
    invoke-virtual {p0, v8}, Lcom/osp/app/signin/PasswordChangeView;->b(I)V

    .line 1013
    invoke-virtual {p0}, Lcom/osp/app/signin/PasswordChangeView;->finish()V

    .line 1052
    :cond_1
    :goto_0
    return-void

    .line 1017
    :cond_2
    new-instance v0, Landroid/app/AlertDialog$Builder;

    invoke-direct {v0, p0}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    const v1, 0x7f0900ad

    invoke-virtual {p0, v1}, Lcom/osp/app/signin/PasswordChangeView;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setTitle(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    const v1, 0x7f090042

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    .line 1028
    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v0

    iput-object v0, p0, Lcom/osp/app/signin/PasswordChangeView;->y:Landroid/app/AlertDialog;

    .line 1030
    invoke-virtual {p0}, Lcom/osp/app/signin/PasswordChangeView;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v0, 0x7f0c0101

    invoke-virtual {p0, v0}, Lcom/osp/app/signin/PasswordChangeView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    if-eqz v0, :cond_3

    invoke-virtual {v0, v5}, Landroid/widget/LinearLayout;->setVisibility(I)V

    const v0, 0x7f0c000d

    invoke-virtual {p0, v0}, Lcom/osp/app/signin/PasswordChangeView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    if-eqz v0, :cond_3

    invoke-static {p0}, Lcom/osp/app/util/an;->a(Lcom/osp/app/util/BaseActivity;)Lcom/osp/app/util/an;

    const v2, 0x7f070006

    invoke-static {v2}, Lcom/osp/app/util/an;->a(I)I

    move-result v2

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v2

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setTextColor(I)V

    :cond_3
    const v0, 0x7f0c0103

    invoke-virtual {p0, v0}, Lcom/osp/app/signin/PasswordChangeView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    if-eqz v0, :cond_4

    invoke-static {p0}, Lcom/osp/app/util/an;->a(Lcom/osp/app/util/BaseActivity;)Lcom/osp/app/util/an;

    const v2, 0x7f070006

    invoke-static {v2}, Lcom/osp/app/util/an;->a(I)I

    move-result v2

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v2

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setTextColor(I)V

    :cond_4
    const v0, 0x7f0c0106

    invoke-virtual {p0, v0}, Lcom/osp/app/signin/PasswordChangeView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    if-eqz v0, :cond_5

    invoke-static {p0}, Lcom/osp/app/util/an;->a(Lcom/osp/app/util/BaseActivity;)Lcom/osp/app/util/an;

    const v2, 0x7f070006

    invoke-static {v2}, Lcom/osp/app/util/an;->a(I)I

    move-result v2

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v2

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setTextColor(I)V

    :cond_5
    const v0, 0x7f0c010b

    invoke-virtual {p0, v0}, Lcom/osp/app/signin/PasswordChangeView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    if-eqz v0, :cond_6

    invoke-static {p0}, Lcom/osp/app/util/an;->a(Lcom/osp/app/util/BaseActivity;)Lcom/osp/app/util/an;

    const v2, 0x7f070006

    invoke-static {v2}, Lcom/osp/app/util/an;->a(I)I

    move-result v2

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v2

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setTextColor(I)V

    :cond_6
    const v0, 0x7f0c0109

    invoke-virtual {p0, v0}, Lcom/osp/app/signin/PasswordChangeView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    if-eqz v0, :cond_7

    invoke-static {p0}, Lcom/osp/app/util/an;->a(Lcom/osp/app/util/BaseActivity;)Lcom/osp/app/util/an;

    const v2, 0x7f02003b

    invoke-static {v2}, Lcom/osp/app/util/an;->a(I)I

    move-result v2

    invoke-virtual {v0, v2}, Landroid/widget/ImageView;->setImageResource(I)V

    invoke-static {v0}, Lcom/msc/sa/c/d;->a(Landroid/view/View;)V

    :cond_7
    const v0, 0x7f0c00b7

    invoke-virtual {p0, v0}, Lcom/osp/app/signin/PasswordChangeView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/CheckBox;

    if-eqz v0, :cond_8

    invoke-static {p0}, Lcom/osp/app/util/an;->a(Lcom/osp/app/util/BaseActivity;)Lcom/osp/app/util/an;

    const v2, 0x7f070004

    invoke-static {v2}, Lcom/osp/app/util/an;->a(I)I

    move-result v2

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/CheckBox;->setTextColor(I)V

    sget v1, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v2, 0x10

    if-le v1, v2, :cond_8

    invoke-virtual {p0}, Lcom/osp/app/signin/PasswordChangeView;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0800e2

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v1

    float-to-int v1, v1

    invoke-static {v0, v1, v5, v5, v5}, Lcom/msc/sa/c/d;->a(Landroid/view/View;IIII)V

    :cond_8
    invoke-static {}, Lcom/osp/app/util/r;->v()Z

    move-result v1

    if-eqz v1, :cond_f

    const v1, 0x7f0c010d

    invoke-virtual {p0, v1}, Lcom/osp/app/signin/PasswordChangeView;->findViewById(I)Landroid/view/View;

    move-result-object v1

    if-eqz v1, :cond_9

    invoke-virtual {v1, v6}, Landroid/view/View;->setVisibility(I)V

    :cond_9
    const v1, 0x7f0c0102

    invoke-virtual {p0, v1}, Lcom/osp/app/signin/PasswordChangeView;->findViewById(I)Landroid/view/View;

    move-result-object v1

    if-eqz v1, :cond_a

    invoke-virtual {v1, v6}, Landroid/view/View;->setVisibility(I)V

    :cond_a
    const v1, 0x7f0c0105

    invoke-virtual {p0, v1}, Lcom/osp/app/signin/PasswordChangeView;->findViewById(I)Landroid/view/View;

    move-result-object v1

    if-eqz v1, :cond_b

    invoke-virtual {v1, v6}, Landroid/view/View;->setVisibility(I)V

    :cond_b
    const v1, 0x7f0c010a

    invoke-virtual {p0, v1}, Lcom/osp/app/signin/PasswordChangeView;->findViewById(I)Landroid/view/View;

    move-result-object v1

    if-eqz v1, :cond_c

    invoke-virtual {v1, v6}, Landroid/view/View;->setVisibility(I)V

    :cond_c
    const v1, 0x7f0c010e

    invoke-virtual {p0, v1}, Lcom/osp/app/signin/PasswordChangeView;->findViewById(I)Landroid/view/View;

    move-result-object v1

    if-eqz v1, :cond_d

    invoke-virtual {v1, v6}, Landroid/view/View;->setVisibility(I)V

    :cond_d
    const v1, 0x7f0c010f

    invoke-virtual {p0, v1}, Lcom/osp/app/signin/PasswordChangeView;->findViewById(I)Landroid/view/View;

    move-result-object v1

    if-eqz v1, :cond_e

    invoke-virtual {v1, v6}, Landroid/view/View;->setVisibility(I)V

    :cond_e
    if-eqz v0, :cond_f

    invoke-static {}, Lcom/osp/device/b;->b()Lcom/osp/device/b;

    move-result-object v1

    invoke-virtual {v1, p0}, Lcom/osp/device/b;->i(Landroid/content/Context;)Z

    move-result v1

    if-nez v1, :cond_f

    invoke-virtual {p0}, Lcom/osp/app/signin/PasswordChangeView;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f08007c

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/CheckBox;->setMinimumHeight(I)V

    :cond_f
    const v0, 0x7f0c0065

    invoke-virtual {p0, v0}, Lcom/osp/app/signin/PasswordChangeView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/osp/app/signin/BottomSoftkeyLinearLayout;

    iput-object v0, p0, Lcom/osp/app/signin/PasswordChangeView;->e:Lcom/osp/app/signin/BottomSoftkeyLinearLayout;

    iget-object v0, p0, Lcom/osp/app/signin/PasswordChangeView;->e:Lcom/osp/app/signin/BottomSoftkeyLinearLayout;

    if-eqz v0, :cond_10

    iget-object v0, p0, Lcom/osp/app/signin/PasswordChangeView;->e:Lcom/osp/app/signin/BottomSoftkeyLinearLayout;

    invoke-virtual {p0}, Lcom/osp/app/signin/PasswordChangeView;->h()Z

    move-result v1

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-virtual {v0, p0, v1}, Lcom/osp/app/signin/BottomSoftkeyLinearLayout;->a(Landroid/content/Context;Ljava/lang/Boolean;)V

    invoke-static {}, Lcom/osp/device/b;->b()Lcom/osp/device/b;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/osp/device/b;->l(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_1f

    iget-object v0, p0, Lcom/osp/app/signin/PasswordChangeView;->e:Lcom/osp/app/signin/BottomSoftkeyLinearLayout;

    invoke-virtual {p0, v4}, Lcom/osp/app/signin/PasswordChangeView;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/osp/app/signin/BottomSoftkeyLinearLayout;->b(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/osp/app/signin/PasswordChangeView;->e:Lcom/osp/app/signin/BottomSoftkeyLinearLayout;

    invoke-virtual {p0, v4}, Lcom/osp/app/signin/PasswordChangeView;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/osp/app/signin/BottomSoftkeyLinearLayout;->d(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/osp/app/signin/PasswordChangeView;->e:Lcom/osp/app/signin/BottomSoftkeyLinearLayout;

    invoke-virtual {p0, v7}, Lcom/osp/app/signin/PasswordChangeView;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/osp/app/signin/BottomSoftkeyLinearLayout;->c(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/osp/app/signin/PasswordChangeView;->e:Lcom/osp/app/signin/BottomSoftkeyLinearLayout;

    invoke-virtual {p0, v7}, Lcom/osp/app/signin/PasswordChangeView;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/osp/app/signin/BottomSoftkeyLinearLayout;->e(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/osp/app/signin/PasswordChangeView;->e:Lcom/osp/app/signin/BottomSoftkeyLinearLayout;

    iget-object v1, p0, Lcom/osp/app/signin/PasswordChangeView;->E:Landroid/view/View$OnClickListener;

    invoke-virtual {v0, v1}, Lcom/osp/app/signin/BottomSoftkeyLinearLayout;->a(Landroid/view/View$OnClickListener;)V

    iget-object v0, p0, Lcom/osp/app/signin/PasswordChangeView;->e:Lcom/osp/app/signin/BottomSoftkeyLinearLayout;

    iget-object v1, p0, Lcom/osp/app/signin/PasswordChangeView;->F:Landroid/view/View$OnClickListener;

    invoke-virtual {v0, v1}, Lcom/osp/app/signin/BottomSoftkeyLinearLayout;->b(Landroid/view/View$OnClickListener;)V

    :goto_1
    invoke-static {}, Lcom/osp/app/signin/SamsungService;->a()Z

    move-result v0

    if-eqz v0, :cond_10

    iget-object v0, p0, Lcom/osp/app/signin/PasswordChangeView;->e:Lcom/osp/app/signin/BottomSoftkeyLinearLayout;

    invoke-virtual {v0, v5}, Lcom/osp/app/signin/BottomSoftkeyLinearLayout;->setVisibility(I)V

    .line 1032
    :cond_10
    invoke-virtual {p0}, Lcom/osp/app/signin/PasswordChangeView;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string v1, "OSP_VER"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/osp/app/signin/PasswordChangeView;->d:Ljava/lang/String;

    const-string v0, "OSP_02"

    iput-object v0, p0, Lcom/osp/app/signin/PasswordChangeView;->d:Ljava/lang/String;

    .line 1034
    const v0, 0x7f0c0104

    invoke-virtual {p0, v0}, Lcom/osp/app/signin/PasswordChangeView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/EditText;

    if-eqz v0, :cond_11

    invoke-virtual {v0, p0}, Landroid/widget/EditText;->setOnFocusChangeListener(Landroid/view/View$OnFocusChangeListener;)V

    new-instance v1, Lcom/osp/app/signin/gj;

    invoke-direct {v1, p0}, Lcom/osp/app/signin/gj;-><init>(Lcom/osp/app/signin/PasswordChangeView;)V

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->addTextChangedListener(Landroid/text/TextWatcher;)V

    :cond_11
    const v1, 0x7f0c0108

    invoke-virtual {p0, v1}, Lcom/osp/app/signin/PasswordChangeView;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/EditText;

    if-eqz v1, :cond_12

    invoke-virtual {v1, p0}, Landroid/widget/EditText;->setOnFocusChangeListener(Landroid/view/View$OnFocusChangeListener;)V

    new-instance v2, Lcom/osp/app/signin/gk;

    invoke-direct {v2, p0, v1}, Lcom/osp/app/signin/gk;-><init>(Lcom/osp/app/signin/PasswordChangeView;Landroid/widget/EditText;)V

    invoke-virtual {v1, v2}, Landroid/widget/EditText;->addTextChangedListener(Landroid/text/TextWatcher;)V

    :cond_12
    const v2, 0x7f0c010c

    invoke-virtual {p0, v2}, Lcom/osp/app/signin/PasswordChangeView;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/EditText;

    if-eqz v2, :cond_13

    invoke-virtual {v2, p0}, Landroid/widget/EditText;->setOnFocusChangeListener(Landroid/view/View$OnFocusChangeListener;)V

    new-instance v3, Lcom/osp/app/signin/gl;

    invoke-direct {v3, p0, v2}, Lcom/osp/app/signin/gl;-><init>(Lcom/osp/app/signin/PasswordChangeView;Landroid/widget/EditText;)V

    invoke-virtual {v2, v3}, Landroid/widget/EditText;->addTextChangedListener(Landroid/text/TextWatcher;)V

    :cond_13
    invoke-direct {p0}, Lcom/osp/app/signin/PasswordChangeView;->c()Landroid/widget/Button;

    move-result-object v3

    if-eqz v3, :cond_14

    invoke-virtual {v3, v4}, Landroid/widget/Button;->setText(I)V

    invoke-direct {p0}, Lcom/osp/app/signin/PasswordChangeView;->c()Landroid/widget/Button;

    move-result-object v3

    iget-object v4, p0, Lcom/osp/app/signin/PasswordChangeView;->E:Landroid/view/View$OnClickListener;

    invoke-virtual {v3, v4}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    :cond_14
    invoke-static {}, Lcom/osp/device/b;->b()Lcom/osp/device/b;

    move-result-object v3

    invoke-virtual {v3, p0}, Lcom/osp/device/b;->l(Landroid/content/Context;)Z

    move-result v3

    if-eqz v3, :cond_20

    const v3, 0x7f0c0172

    invoke-virtual {p0, v3}, Lcom/osp/app/signin/PasswordChangeView;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/Button;

    :goto_2
    if-eqz v3, :cond_15

    invoke-virtual {v3, v7}, Landroid/widget/Button;->setText(I)V

    iget-object v4, p0, Lcom/osp/app/signin/PasswordChangeView;->F:Landroid/view/View$OnClickListener;

    invoke-virtual {v3, v4}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    :cond_15
    iget-object v3, p0, Lcom/osp/app/signin/PasswordChangeView;->A:Landroid/widget/Button;

    if-eqz v3, :cond_16

    iget-object v3, p0, Lcom/osp/app/signin/PasswordChangeView;->A:Landroid/widget/Button;

    iget-object v4, p0, Lcom/osp/app/signin/PasswordChangeView;->F:Landroid/view/View$OnClickListener;

    invoke-virtual {v3, v4}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    :cond_16
    const v3, 0x7f0c00b7

    invoke-virtual {p0, v3}, Lcom/osp/app/signin/PasswordChangeView;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/CheckBox;

    if-eqz v3, :cond_17

    new-instance v4, Lcom/osp/app/signin/gm;

    invoke-direct {v4, p0, v3}, Lcom/osp/app/signin/gm;-><init>(Lcom/osp/app/signin/PasswordChangeView;Landroid/widget/CheckBox;)V

    invoke-virtual {v3, v4}, Landroid/widget/CheckBox;->setOnCheckedChangeListener(Landroid/widget/CompoundButton$OnCheckedChangeListener;)V

    :cond_17
    if-eqz v3, :cond_18

    invoke-virtual {v3, v8}, Landroid/widget/CheckBox;->setChecked(Z)V

    :cond_18
    if-eqz v1, :cond_19

    const/16 v3, 0x91

    invoke-virtual {v1, v3}, Landroid/widget/EditText;->setInputType(I)V

    :cond_19
    if-eqz v0, :cond_1a

    const/16 v1, 0x91

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setInputType(I)V

    :cond_1a
    if-eqz v2, :cond_1b

    const/16 v0, 0x91

    invoke-virtual {v2, v0}, Landroid/widget/EditText;->setInputType(I)V

    :cond_1b
    const v0, 0x7f0c0109

    invoke-virtual {p0, v0}, Lcom/osp/app/signin/PasswordChangeView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    const v1, 0x7f0c0107

    invoke-virtual {p0, v1}, Lcom/osp/app/signin/PasswordChangeView;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/LinearLayout;

    if-eqz v0, :cond_1c

    if-eqz v1, :cond_1c

    const/4 v2, -0x1

    const/4 v3, -0x1

    const/4 v4, -0x1

    invoke-static {v1, v2, v3, v5, v4}, Lcom/msc/sa/c/d;->a(Landroid/view/View;IIII)V

    invoke-virtual {v0, v5}, Landroid/widget/ImageView;->setVisibility(I)V

    new-instance v1, Lcom/osp/app/signin/gn;

    invoke-direct {v1, p0}, Lcom/osp/app/signin/gn;-><init>(Lcom/osp/app/signin/PasswordChangeView;)V

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    :cond_1c
    const v0, 0x7f0c0028

    invoke-virtual {p0, v0}, Lcom/osp/app/signin/PasswordChangeView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    if-eqz v0, :cond_1d

    sget v1, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v2, 0xb

    if-ge v1, v2, :cond_21

    const v1, 0x108009a

    invoke-virtual {v0, v1}, Landroid/view/View;->setBackgroundResource(I)V

    .line 1035
    :cond_1d
    :goto_3
    invoke-virtual {p0}, Lcom/osp/app/signin/PasswordChangeView;->a()V

    .line 1037
    const v0, 0x7f0c0104

    invoke-virtual {p0, v0}, Lcom/osp/app/signin/PasswordChangeView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/EditText;

    .line 1038
    if-eqz v0, :cond_1e

    .line 1040
    invoke-virtual {v0}, Landroid/widget/EditText;->requestFocus()Z

    .line 1044
    :cond_1e
    invoke-virtual {p0}, Lcom/osp/app/signin/PasswordChangeView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v0

    iget v0, v0, Landroid/content/res/Configuration;->orientation:I

    invoke-direct {p0, v0}, Lcom/osp/app/signin/PasswordChangeView;->a(I)V

    .line 1046
    invoke-static {}, Lcom/osp/app/signin/SamsungService;->a()Z

    move-result v0

    if-nez v0, :cond_1

    invoke-static {p0}, Lcom/osp/app/util/r;->m(Landroid/content/Context;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 1048
    const-string v0, "save_cancel"

    invoke-virtual {p0, v0}, Lcom/osp/app/signin/PasswordChangeView;->e(Ljava/lang/String;)V

    .line 1049
    const-string v0, ""

    invoke-static {p0, v0}, Lcom/msc/sa/c/d;->a(Landroid/app/Activity;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 1030
    :cond_1f
    iget-object v0, p0, Lcom/osp/app/signin/PasswordChangeView;->e:Lcom/osp/app/signin/BottomSoftkeyLinearLayout;

    invoke-virtual {p0, v7}, Lcom/osp/app/signin/PasswordChangeView;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/osp/app/signin/BottomSoftkeyLinearLayout;->b(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/osp/app/signin/PasswordChangeView;->e:Lcom/osp/app/signin/BottomSoftkeyLinearLayout;

    invoke-virtual {p0, v7}, Lcom/osp/app/signin/PasswordChangeView;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/osp/app/signin/BottomSoftkeyLinearLayout;->d(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/osp/app/signin/PasswordChangeView;->e:Lcom/osp/app/signin/BottomSoftkeyLinearLayout;

    invoke-virtual {p0, v4}, Lcom/osp/app/signin/PasswordChangeView;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/osp/app/signin/BottomSoftkeyLinearLayout;->c(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/osp/app/signin/PasswordChangeView;->e:Lcom/osp/app/signin/BottomSoftkeyLinearLayout;

    invoke-virtual {p0, v4}, Lcom/osp/app/signin/PasswordChangeView;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/osp/app/signin/BottomSoftkeyLinearLayout;->e(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/osp/app/signin/PasswordChangeView;->e:Lcom/osp/app/signin/BottomSoftkeyLinearLayout;

    iget-object v1, p0, Lcom/osp/app/signin/PasswordChangeView;->E:Landroid/view/View$OnClickListener;

    invoke-virtual {v0, v1}, Lcom/osp/app/signin/BottomSoftkeyLinearLayout;->b(Landroid/view/View$OnClickListener;)V

    iget-object v0, p0, Lcom/osp/app/signin/PasswordChangeView;->e:Lcom/osp/app/signin/BottomSoftkeyLinearLayout;

    iget-object v1, p0, Lcom/osp/app/signin/PasswordChangeView;->F:Landroid/view/View$OnClickListener;

    invoke-virtual {v0, v1}, Lcom/osp/app/signin/BottomSoftkeyLinearLayout;->a(Landroid/view/View$OnClickListener;)V

    goto/16 :goto_1

    .line 1034
    :cond_20
    const v3, 0x7f0c0110

    invoke-virtual {p0, v3}, Lcom/osp/app/signin/PasswordChangeView;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/Button;

    goto/16 :goto_2

    :cond_21
    invoke-virtual {v0, v6}, Landroid/view/View;->setVisibility(I)V

    goto :goto_3
.end method

.method protected onDestroy()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 1056
    invoke-super {p0}, Lcom/osp/app/util/BaseActivity;->onDestroy()V

    .line 1066
    iget-object v0, p0, Lcom/osp/app/signin/PasswordChangeView;->b:Lcom/osp/app/signin/gp;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/osp/app/signin/PasswordChangeView;->b:Lcom/osp/app/signin/gp;

    invoke-virtual {v0}, Lcom/osp/app/signin/gp;->getStatus()Landroid/os/AsyncTask$Status;

    move-result-object v0

    sget-object v1, Landroid/os/AsyncTask$Status;->RUNNING:Landroid/os/AsyncTask$Status;

    if-ne v0, v1, :cond_0

    .line 1068
    iget-object v0, p0, Lcom/osp/app/signin/PasswordChangeView;->b:Lcom/osp/app/signin/gp;

    invoke-virtual {v0}, Lcom/osp/app/signin/gp;->d()V

    .line 1069
    iput-object v2, p0, Lcom/osp/app/signin/PasswordChangeView;->b:Lcom/osp/app/signin/gp;

    .line 1071
    :cond_0
    iget-object v0, p0, Lcom/osp/app/signin/PasswordChangeView;->c:Lcom/osp/app/signin/gr;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/osp/app/signin/PasswordChangeView;->c:Lcom/osp/app/signin/gr;

    invoke-virtual {v0}, Lcom/osp/app/signin/gr;->getStatus()Landroid/os/AsyncTask$Status;

    move-result-object v0

    sget-object v1, Landroid/os/AsyncTask$Status;->RUNNING:Landroid/os/AsyncTask$Status;

    if-ne v0, v1, :cond_1

    .line 1073
    iget-object v0, p0, Lcom/osp/app/signin/PasswordChangeView;->c:Lcom/osp/app/signin/gr;

    invoke-virtual {v0}, Lcom/osp/app/signin/gr;->d()V

    .line 1074
    iput-object v2, p0, Lcom/osp/app/signin/PasswordChangeView;->c:Lcom/osp/app/signin/gr;

    .line 1083
    :cond_1
    invoke-static {}, Lcom/msc/sa/selfupdate/c;->b()Lcom/msc/sa/selfupdate/c;

    move-result-object v0

    invoke-virtual {v0}, Lcom/msc/sa/selfupdate/c;->d()V

    .line 1084
    invoke-virtual {p0}, Lcom/osp/app/signin/PasswordChangeView;->getApplication()Landroid/app/Application;

    move-result-object v0

    check-cast v0, Lcom/osp/app/signin/SamsungService;

    invoke-virtual {v0}, Lcom/osp/app/signin/SamsungService;->o()V

    .line 1087
    iget-object v0, p0, Lcom/osp/app/signin/PasswordChangeView;->z:Landroid/app/AlertDialog;

    if-eqz v0, :cond_3

    .line 1091
    :try_start_0
    invoke-virtual {p0}, Lcom/osp/app/signin/PasswordChangeView;->isFinishing()Z

    move-result v0

    if-nez v0, :cond_2

    iget-object v0, p0, Lcom/osp/app/signin/PasswordChangeView;->z:Landroid/app/AlertDialog;

    invoke-virtual {v0}, Landroid/app/AlertDialog;->isShowing()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 1093
    iget-object v0, p0, Lcom/osp/app/signin/PasswordChangeView;->z:Landroid/app/AlertDialog;

    invoke-virtual {v0}, Landroid/app/AlertDialog;->dismiss()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1100
    :cond_2
    :goto_0
    iput-object v2, p0, Lcom/osp/app/signin/PasswordChangeView;->z:Landroid/app/AlertDialog;

    .line 1103
    :cond_3
    return-void

    .line 1095
    :catch_0
    move-exception v0

    :try_start_1
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 1100
    :catchall_0
    move-exception v0

    iput-object v2, p0, Lcom/osp/app/signin/PasswordChangeView;->z:Landroid/app/AlertDialog;

    throw v0
.end method

.method public onFocusChange(Landroid/view/View;Z)V
    .locals 4

    .prologue
    const v3, 0x7f0c0108

    const/4 v2, 0x1

    .line 924
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "PasswordChangeView::onFocusChange hasFocus : "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;)V

    .line 926
    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v0

    if-ne v0, v3, :cond_1

    if-ne p2, v2, :cond_1

    .line 928
    const v0, 0x7f0c0104

    invoke-virtual {p0, v0}, Lcom/osp/app/signin/PasswordChangeView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/EditText;

    invoke-virtual {v0}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-interface {v0}, Landroid/text/Editable;->length()I

    move-result v0

    if-gtz v0, :cond_0

    .line 930
    invoke-static {}, Lcom/osp/app/util/ap;->a()Lcom/osp/app/util/ap;

    const v0, 0x7f090026

    invoke-virtual {p0, v0}, Lcom/osp/app/signin/PasswordChangeView;->getText(I)Ljava/lang/CharSequence;

    move-result-object v0

    invoke-interface {v0}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {p0, v0, v2}, Lcom/osp/app/util/ap;->a(Landroid/content/Context;Ljava/lang/String;I)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    .line 970
    :cond_0
    :goto_0
    return-void

    .line 933
    :cond_1
    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v0

    const v1, 0x7f0c010c

    if-ne v0, v1, :cond_0

    if-ne p2, v2, :cond_0

    .line 935
    invoke-virtual {p0, v3}, Lcom/osp/app/signin/PasswordChangeView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/EditText;

    invoke-virtual {v0}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-interface {v0}, Landroid/text/Editable;->length()I

    move-result v0

    if-gtz v0, :cond_0

    .line 937
    invoke-static {}, Lcom/osp/app/util/ap;->a()Lcom/osp/app/util/ap;

    const v0, 0x7f090093

    invoke-virtual {p0, v0}, Lcom/osp/app/signin/PasswordChangeView;->getText(I)Ljava/lang/CharSequence;

    move-result-object v0

    invoke-interface {v0}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {p0, v0, v2}, Lcom/osp/app/util/ap;->a(Landroid/content/Context;Ljava/lang/String;I)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    goto :goto_0
.end method

.method public onOptionsItemSelected(Landroid/view/MenuItem;)Z
    .locals 2

    .prologue
    .line 1606
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0xb

    if-lt v0, v1, :cond_0

    .line 1608
    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    .line 1669
    :cond_0
    :goto_0
    invoke-super {p0, p1}, Lcom/osp/app/util/BaseActivity;->onOptionsItemSelected(Landroid/view/MenuItem;)Z

    move-result v0

    return v0

    .line 1612
    :sswitch_0
    invoke-virtual {p0}, Lcom/osp/app/signin/PasswordChangeView;->onBackPressed()V

    goto :goto_0

    .line 1616
    :sswitch_1
    const-string v0, "save"

    invoke-direct {p0, v0}, Lcom/osp/app/signin/PasswordChangeView;->a(Ljava/lang/String;)V

    goto :goto_0

    .line 1668
    :sswitch_2
    invoke-virtual {p0}, Lcom/osp/app/signin/PasswordChangeView;->onBackPressed()V

    goto :goto_0

    .line 1608
    :sswitch_data_0
    .sparse-switch
        0x102002c -> :sswitch_0
        0x7f0c018e -> :sswitch_2
        0x7f0c018f -> :sswitch_1
    .end sparse-switch
.end method

.method protected onRestoreInstanceState(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 1127
    invoke-super {p0, p1}, Lcom/osp/app/util/BaseActivity;->onRestoreInstanceState(Landroid/os/Bundle;)V

    .line 1129
    const v0, 0x7f0c00b7

    invoke-virtual {p0, v0}, Lcom/osp/app/signin/PasswordChangeView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/CheckBox;

    .line 1130
    if-eqz v0, :cond_0

    .line 1132
    const v1, 0x7f09012a

    invoke-virtual {v0, v1}, Landroid/widget/CheckBox;->setText(I)V

    .line 1135
    :cond_0
    return-void
.end method

.method protected onResume()V
    .locals 1

    .prologue
    .line 1112
    invoke-super {p0}, Lcom/osp/app/util/BaseActivity;->onResume()V

    .line 1117
    invoke-static {p0}, Lcom/osp/app/util/ad;->d(Landroid/content/Context;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 1119
    const/16 v0, 0x10

    invoke-virtual {p0, v0}, Lcom/osp/app/signin/PasswordChangeView;->b(I)V

    .line 1120
    invoke-virtual {p0}, Lcom/osp/app/signin/PasswordChangeView;->finish()V

    .line 1123
    :cond_0
    return-void
.end method
