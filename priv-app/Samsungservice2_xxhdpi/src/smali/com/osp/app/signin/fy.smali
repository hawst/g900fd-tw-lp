.class final Lcom/osp/app/signin/fy;
.super Ljava/lang/Object;
.source "NameValidationWebView.java"

# interfaces
.implements Landroid/content/DialogInterface$OnClickListener;


# instance fields
.field final synthetic a:Lcom/osp/app/signin/NameValidationWebView;


# direct methods
.method constructor <init>(Lcom/osp/app/signin/NameValidationWebView;)V
    .locals 0

    .prologue
    .line 316
    iput-object p1, p0, Lcom/osp/app/signin/fy;->a:Lcom/osp/app/signin/NameValidationWebView;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onClick(Landroid/content/DialogInterface;I)V
    .locals 3

    .prologue
    .line 320
    iget-object v0, p0, Lcom/osp/app/signin/fy;->a:Lcom/osp/app/signin/NameValidationWebView;

    invoke-static {v0}, Lcom/osp/app/signin/NameValidationWebView;->c(Lcom/osp/app/signin/NameValidationWebView;)Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/osp/app/signin/fy;->a:Lcom/osp/app/signin/NameValidationWebView;

    invoke-static {v0}, Lcom/osp/app/signin/NameValidationWebView;->c(Lcom/osp/app/signin/NameValidationWebView;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v0

    if-lez v0, :cond_1

    iget-object v0, p0, Lcom/osp/app/signin/fy;->a:Lcom/osp/app/signin/NameValidationWebView;

    invoke-static {v0}, Lcom/osp/app/signin/NameValidationWebView;->c(Lcom/osp/app/signin/NameValidationWebView;)Ljava/lang/String;

    move-result-object v0

    const-string v1, "true"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 322
    iget-object v0, p0, Lcom/osp/app/signin/fy;->a:Lcom/osp/app/signin/NameValidationWebView;

    invoke-virtual {v0}, Lcom/osp/app/signin/NameValidationWebView;->getIntent()Landroid/content/Intent;

    move-result-object v0

    .line 323
    if-eqz v0, :cond_0

    const-string v1, "is_cancelable_just_one_activity"

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 325
    iget-object v1, p0, Lcom/osp/app/signin/fy;->a:Lcom/osp/app/signin/NameValidationWebView;

    const/16 v2, 0xe

    invoke-virtual {v1, v2, v0}, Lcom/osp/app/signin/NameValidationWebView;->a(ILandroid/content/Intent;)V

    .line 327
    :cond_0
    iget-object v0, p0, Lcom/osp/app/signin/fy;->a:Lcom/osp/app/signin/NameValidationWebView;

    invoke-virtual {v0}, Lcom/osp/app/signin/NameValidationWebView;->finish()V

    .line 333
    :goto_0
    iget-object v0, p0, Lcom/osp/app/signin/fy;->a:Lcom/osp/app/signin/NameValidationWebView;

    invoke-static {v0}, Lcom/osp/app/signin/NameValidationWebView;->d(Lcom/osp/app/signin/NameValidationWebView;)V

    .line 334
    return-void

    .line 330
    :cond_1
    iget-object v0, p0, Lcom/osp/app/signin/fy;->a:Lcom/osp/app/signin/NameValidationWebView;

    invoke-static {v0}, Lcom/osp/app/signin/NameValidationWebView;->b(Lcom/osp/app/signin/NameValidationWebView;)Landroid/webkit/WebView;

    move-result-object v0

    invoke-virtual {v0}, Landroid/webkit/WebView;->clearHistory()V

    .line 331
    iget-object v0, p0, Lcom/osp/app/signin/fy;->a:Lcom/osp/app/signin/NameValidationWebView;

    invoke-static {v0}, Lcom/osp/app/signin/NameValidationWebView;->b(Lcom/osp/app/signin/NameValidationWebView;)Landroid/webkit/WebView;

    move-result-object v0

    iget-object v1, p0, Lcom/osp/app/signin/fy;->a:Lcom/osp/app/signin/NameValidationWebView;

    invoke-static {v1}, Lcom/osp/app/signin/NameValidationWebView;->e(Lcom/osp/app/signin/NameValidationWebView;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/webkit/WebView;->loadUrl(Ljava/lang/String;)V

    goto :goto_0
.end method
