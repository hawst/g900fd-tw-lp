.class final Lcom/osp/app/signin/qb;
.super Ljava/lang/Object;
.source "WeiboInfoWebView.java"


# instance fields
.field final synthetic a:Lcom/osp/app/signin/WeiboInfoWebView;


# direct methods
.method private constructor <init>(Lcom/osp/app/signin/WeiboInfoWebView;)V
    .locals 0

    .prologue
    .line 187
    iput-object p1, p0, Lcom/osp/app/signin/qb;->a:Lcom/osp/app/signin/WeiboInfoWebView;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/osp/app/signin/WeiboInfoWebView;B)V
    .locals 0

    .prologue
    .line 187
    invoke-direct {p0, p1}, Lcom/osp/app/signin/qb;-><init>(Lcom/osp/app/signin/WeiboInfoWebView;)V

    return-void
.end method


# virtual methods
.method public final processJson(Ljava/lang/String;)V
    .locals 6
    .annotation runtime Landroid/webkit/JavascriptInterface;
    .end annotation

    .prologue
    const/4 v3, 0x1

    const/4 v5, 0x0

    const/4 v4, 0x2

    .line 191
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "Weibo Web Page html :"

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;)V

    .line 193
    iget-object v0, p0, Lcom/osp/app/signin/qb;->a:Lcom/osp/app/signin/WeiboInfoWebView;

    invoke-static {p1}, Lcom/osp/app/signin/WeiboInfoWebView;->a(Ljava/lang/String;)Lcom/osp/app/signin/pz;

    move-result-object v0

    .line 195
    invoke-virtual {v0}, Lcom/osp/app/signin/pz;->g()I

    move-result v1

    if-eq v1, v3, :cond_0

    invoke-virtual {v0}, Lcom/osp/app/signin/pz;->g()I

    move-result v1

    if-ne v1, v4, :cond_4

    .line 198
    :cond_0
    iget-object v1, p0, Lcom/osp/app/signin/qb;->a:Lcom/osp/app/signin/WeiboInfoWebView;

    invoke-static {v1}, Lcom/osp/app/signin/WeiboInfoWebView;->a(Lcom/osp/app/signin/WeiboInfoWebView;)Lcom/osp/app/signin/pz;

    move-result-object v1

    invoke-virtual {v0}, Lcom/osp/app/signin/pz;->c()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/osp/app/signin/pz;->c(Ljava/lang/String;)V

    .line 200
    invoke-virtual {v0}, Lcom/osp/app/signin/pz;->g()I

    move-result v1

    if-ne v1, v4, :cond_3

    .line 203
    invoke-virtual {v0}, Lcom/osp/app/signin/pz;->f()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1, v5, v4}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v1

    const-string v2, "00"

    invoke-virtual {v1, v2}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 207
    iget-object v1, p0, Lcom/osp/app/signin/qb;->a:Lcom/osp/app/signin/WeiboInfoWebView;

    invoke-static {v1}, Lcom/osp/app/signin/WeiboInfoWebView;->a(Lcom/osp/app/signin/WeiboInfoWebView;)Lcom/osp/app/signin/pz;

    move-result-object v1

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "+"

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Lcom/osp/app/signin/pz;->f()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3, v4}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/osp/app/signin/pz;->f(Ljava/lang/String;)V

    .line 221
    :goto_0
    iget-object v1, p0, Lcom/osp/app/signin/qb;->a:Lcom/osp/app/signin/WeiboInfoWebView;

    invoke-static {v1}, Lcom/osp/app/signin/WeiboInfoWebView;->a(Lcom/osp/app/signin/WeiboInfoWebView;)Lcom/osp/app/signin/pz;

    move-result-object v1

    invoke-virtual {v0}, Lcom/osp/app/signin/pz;->g()I

    move-result v2

    invoke-virtual {v1, v2}, Lcom/osp/app/signin/pz;->a(I)V

    .line 222
    iget-object v1, p0, Lcom/osp/app/signin/qb;->a:Lcom/osp/app/signin/WeiboInfoWebView;

    invoke-static {v1}, Lcom/osp/app/signin/WeiboInfoWebView;->a(Lcom/osp/app/signin/WeiboInfoWebView;)Lcom/osp/app/signin/pz;

    move-result-object v1

    invoke-virtual {v0}, Lcom/osp/app/signin/pz;->h()Ljava/lang/Boolean;

    move-result-object v0

    invoke-virtual {v1, v0}, Lcom/osp/app/signin/pz;->a(Ljava/lang/Boolean;)V

    .line 224
    iget-object v0, p0, Lcom/osp/app/signin/qb;->a:Lcom/osp/app/signin/WeiboInfoWebView;

    invoke-virtual {v0}, Lcom/osp/app/signin/WeiboInfoWebView;->getIntent()Landroid/content/Intent;

    move-result-object v0

    .line 225
    const-string v1, "Weibo_AccessToken"

    iget-object v2, p0, Lcom/osp/app/signin/qb;->a:Lcom/osp/app/signin/WeiboInfoWebView;

    invoke-static {v2}, Lcom/osp/app/signin/WeiboInfoWebView;->a(Lcom/osp/app/signin/WeiboInfoWebView;)Lcom/osp/app/signin/pz;

    move-result-object v2

    invoke-virtual {v2}, Lcom/osp/app/signin/pz;->a()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 226
    const-string v1, "Weibo_Uid"

    iget-object v2, p0, Lcom/osp/app/signin/qb;->a:Lcom/osp/app/signin/WeiboInfoWebView;

    invoke-static {v2}, Lcom/osp/app/signin/WeiboInfoWebView;->a(Lcom/osp/app/signin/WeiboInfoWebView;)Lcom/osp/app/signin/pz;

    move-result-object v2

    invoke-virtual {v2}, Lcom/osp/app/signin/pz;->b()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 227
    const-string v1, "Weibo_Email"

    iget-object v2, p0, Lcom/osp/app/signin/qb;->a:Lcom/osp/app/signin/WeiboInfoWebView;

    invoke-static {v2}, Lcom/osp/app/signin/WeiboInfoWebView;->a(Lcom/osp/app/signin/WeiboInfoWebView;)Lcom/osp/app/signin/pz;

    move-result-object v2

    invoke-virtual {v2}, Lcom/osp/app/signin/pz;->c()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 228
    const-string v1, "Weibo_Mobile"

    iget-object v2, p0, Lcom/osp/app/signin/qb;->a:Lcom/osp/app/signin/WeiboInfoWebView;

    invoke-static {v2}, Lcom/osp/app/signin/WeiboInfoWebView;->a(Lcom/osp/app/signin/WeiboInfoWebView;)Lcom/osp/app/signin/pz;

    move-result-object v2

    invoke-virtual {v2}, Lcom/osp/app/signin/pz;->f()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 229
    const-string v1, "Weibo_IdType"

    iget-object v2, p0, Lcom/osp/app/signin/qb;->a:Lcom/osp/app/signin/WeiboInfoWebView;

    invoke-static {v2}, Lcom/osp/app/signin/WeiboInfoWebView;->a(Lcom/osp/app/signin/WeiboInfoWebView;)Lcom/osp/app/signin/pz;

    move-result-object v2

    invoke-virtual {v2}, Lcom/osp/app/signin/pz;->g()I

    move-result v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 230
    const-string v1, "Weibo_Valid"

    iget-object v2, p0, Lcom/osp/app/signin/qb;->a:Lcom/osp/app/signin/WeiboInfoWebView;

    invoke-static {v2}, Lcom/osp/app/signin/WeiboInfoWebView;->a(Lcom/osp/app/signin/WeiboInfoWebView;)Lcom/osp/app/signin/pz;

    move-result-object v2

    invoke-virtual {v2}, Lcom/osp/app/signin/pz;->h()Ljava/lang/Boolean;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/io/Serializable;)Landroid/content/Intent;

    .line 232
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Weibo_AccessToken : "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, Lcom/osp/app/signin/qb;->a:Lcom/osp/app/signin/WeiboInfoWebView;

    invoke-static {v2}, Lcom/osp/app/signin/WeiboInfoWebView;->a(Lcom/osp/app/signin/WeiboInfoWebView;)Lcom/osp/app/signin/pz;

    move-result-object v2

    invoke-virtual {v2}, Lcom/osp/app/signin/pz;->a()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;)V

    .line 233
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Weibo_Uid : "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, Lcom/osp/app/signin/qb;->a:Lcom/osp/app/signin/WeiboInfoWebView;

    invoke-static {v2}, Lcom/osp/app/signin/WeiboInfoWebView;->a(Lcom/osp/app/signin/WeiboInfoWebView;)Lcom/osp/app/signin/pz;

    move-result-object v2

    invoke-virtual {v2}, Lcom/osp/app/signin/pz;->b()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;)V

    .line 234
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Weibo_Email : "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, Lcom/osp/app/signin/qb;->a:Lcom/osp/app/signin/WeiboInfoWebView;

    invoke-static {v2}, Lcom/osp/app/signin/WeiboInfoWebView;->a(Lcom/osp/app/signin/WeiboInfoWebView;)Lcom/osp/app/signin/pz;

    move-result-object v2

    invoke-virtual {v2}, Lcom/osp/app/signin/pz;->c()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;)V

    .line 235
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Weibo_Mobile : "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, Lcom/osp/app/signin/qb;->a:Lcom/osp/app/signin/WeiboInfoWebView;

    invoke-static {v2}, Lcom/osp/app/signin/WeiboInfoWebView;->a(Lcom/osp/app/signin/WeiboInfoWebView;)Lcom/osp/app/signin/pz;

    move-result-object v2

    invoke-virtual {v2}, Lcom/osp/app/signin/pz;->f()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;)V

    .line 236
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Weibo_IdType : "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, Lcom/osp/app/signin/qb;->a:Lcom/osp/app/signin/WeiboInfoWebView;

    invoke-static {v2}, Lcom/osp/app/signin/WeiboInfoWebView;->a(Lcom/osp/app/signin/WeiboInfoWebView;)Lcom/osp/app/signin/pz;

    move-result-object v2

    invoke-virtual {v2}, Lcom/osp/app/signin/pz;->g()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;)V

    .line 237
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Weibo_Valid : "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, Lcom/osp/app/signin/qb;->a:Lcom/osp/app/signin/WeiboInfoWebView;

    invoke-static {v2}, Lcom/osp/app/signin/WeiboInfoWebView;->a(Lcom/osp/app/signin/WeiboInfoWebView;)Lcom/osp/app/signin/pz;

    move-result-object v2

    invoke-virtual {v2}, Lcom/osp/app/signin/pz;->h()Ljava/lang/Boolean;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;)V

    .line 239
    iget-object v1, p0, Lcom/osp/app/signin/qb;->a:Lcom/osp/app/signin/WeiboInfoWebView;

    const/4 v2, -0x1

    invoke-virtual {v1, v2, v0}, Lcom/osp/app/signin/WeiboInfoWebView;->a(ILandroid/content/Intent;)V

    .line 240
    iget-object v0, p0, Lcom/osp/app/signin/qb;->a:Lcom/osp/app/signin/WeiboInfoWebView;

    invoke-virtual {v0}, Lcom/osp/app/signin/WeiboInfoWebView;->finish()V

    .line 267
    :goto_1
    return-void

    .line 208
    :cond_1
    invoke-virtual {v0}, Lcom/osp/app/signin/pz;->f()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1, v5, v4}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v1

    const-string v2, "86"

    invoke-virtual {v1, v2}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 211
    iget-object v1, p0, Lcom/osp/app/signin/qb;->a:Lcom/osp/app/signin/WeiboInfoWebView;

    invoke-static {v1}, Lcom/osp/app/signin/WeiboInfoWebView;->a(Lcom/osp/app/signin/WeiboInfoWebView;)Lcom/osp/app/signin/pz;

    move-result-object v1

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "+"

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Lcom/osp/app/signin/pz;->f()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/osp/app/signin/pz;->f(Ljava/lang/String;)V

    goto/16 :goto_0

    .line 215
    :cond_2
    iget-object v1, p0, Lcom/osp/app/signin/qb;->a:Lcom/osp/app/signin/WeiboInfoWebView;

    invoke-static {v1}, Lcom/osp/app/signin/WeiboInfoWebView;->a(Lcom/osp/app/signin/WeiboInfoWebView;)Lcom/osp/app/signin/pz;

    move-result-object v1

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "+86"

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Lcom/osp/app/signin/pz;->f()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/osp/app/signin/pz;->f(Ljava/lang/String;)V

    goto/16 :goto_0

    .line 219
    :cond_3
    iget-object v1, p0, Lcom/osp/app/signin/qb;->a:Lcom/osp/app/signin/WeiboInfoWebView;

    invoke-static {v1}, Lcom/osp/app/signin/WeiboInfoWebView;->a(Lcom/osp/app/signin/WeiboInfoWebView;)Lcom/osp/app/signin/pz;

    move-result-object v1

    invoke-virtual {v0}, Lcom/osp/app/signin/pz;->f()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/osp/app/signin/pz;->f(Ljava/lang/String;)V

    goto/16 :goto_0

    .line 241
    :cond_4
    invoke-virtual {v0}, Lcom/osp/app/signin/pz;->i()Ljava/lang/String;

    move-result-object v0

    const-string v1, "10004"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_6

    .line 243
    iget-object v0, p0, Lcom/osp/app/signin/qb;->a:Lcom/osp/app/signin/WeiboInfoWebView;

    invoke-virtual {v0}, Lcom/osp/app/signin/WeiboInfoWebView;->c()I

    move-result v0

    const/4 v1, 0x5

    if-lt v0, v1, :cond_5

    .line 245
    invoke-static {}, Lcom/osp/app/util/ap;->a()Lcom/osp/app/util/ap;

    iget-object v0, p0, Lcom/osp/app/signin/qb;->a:Lcom/osp/app/signin/WeiboInfoWebView;

    invoke-static {v0}, Lcom/osp/app/signin/WeiboInfoWebView;->b(Lcom/osp/app/signin/WeiboInfoWebView;)Landroid/content/Context;

    move-result-object v0

    iget-object v1, p0, Lcom/osp/app/signin/qb;->a:Lcom/osp/app/signin/WeiboInfoWebView;

    const v2, 0x7f09003e

    invoke-virtual {v1, v2}, Lcom/osp/app/signin/WeiboInfoWebView;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1, v3}, Lcom/osp/app/util/ap;->a(Landroid/content/Context;Ljava/lang/String;I)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    .line 247
    iget-object v0, p0, Lcom/osp/app/signin/qb;->a:Lcom/osp/app/signin/WeiboInfoWebView;

    invoke-virtual {v0}, Lcom/osp/app/signin/WeiboInfoWebView;->getIntent()Landroid/content/Intent;

    move-result-object v0

    .line 248
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v1, "WIWV"

    const-string v2, "weibo processJson() - limited Retry"

    invoke-static {v1, v2}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 249
    iget-object v1, p0, Lcom/osp/app/signin/qb;->a:Lcom/osp/app/signin/WeiboInfoWebView;

    invoke-virtual {v1, v5, v0}, Lcom/osp/app/signin/WeiboInfoWebView;->a(ILandroid/content/Intent;)V

    .line 250
    iget-object v0, p0, Lcom/osp/app/signin/qb;->a:Lcom/osp/app/signin/WeiboInfoWebView;

    invoke-virtual {v0}, Lcom/osp/app/signin/WeiboInfoWebView;->finish()V

    .line 253
    :cond_5
    iget-object v0, p0, Lcom/osp/app/signin/qb;->a:Lcom/osp/app/signin/WeiboInfoWebView;

    iget-object v1, p0, Lcom/osp/app/signin/qb;->a:Lcom/osp/app/signin/WeiboInfoWebView;

    invoke-virtual {v1}, Lcom/osp/app/signin/WeiboInfoWebView;->c()I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    invoke-virtual {v0, v1}, Lcom/osp/app/signin/WeiboInfoWebView;->a(I)V

    .line 254
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "weibo retry :"

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v1, p0, Lcom/osp/app/signin/qb;->a:Lcom/osp/app/signin/WeiboInfoWebView;

    invoke-static {v1}, Lcom/osp/app/signin/WeiboInfoWebView;->c(Lcom/osp/app/signin/WeiboInfoWebView;)I

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;)V

    .line 255
    iget-object v0, p0, Lcom/osp/app/signin/qb;->a:Lcom/osp/app/signin/WeiboInfoWebView;

    invoke-static {v0}, Lcom/osp/app/signin/WeiboInfoWebView;->d(Lcom/osp/app/signin/WeiboInfoWebView;)Landroid/webkit/WebView;

    move-result-object v0

    iget-object v1, p0, Lcom/osp/app/signin/qb;->a:Lcom/osp/app/signin/WeiboInfoWebView;

    invoke-static {}, Lcom/osp/app/signin/WeiboInfoWebView;->d()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/webkit/WebView;->loadUrl(Ljava/lang/String;)V

    goto/16 :goto_1

    .line 259
    :cond_6
    invoke-static {}, Lcom/osp/app/util/ap;->a()Lcom/osp/app/util/ap;

    iget-object v0, p0, Lcom/osp/app/signin/qb;->a:Lcom/osp/app/signin/WeiboInfoWebView;

    invoke-static {v0}, Lcom/osp/app/signin/WeiboInfoWebView;->b(Lcom/osp/app/signin/WeiboInfoWebView;)Landroid/content/Context;

    move-result-object v0

    iget-object v1, p0, Lcom/osp/app/signin/qb;->a:Lcom/osp/app/signin/WeiboInfoWebView;

    const v2, 0x7f09004a

    invoke-virtual {v1, v2}, Lcom/osp/app/signin/WeiboInfoWebView;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1, v3}, Lcom/osp/app/util/ap;->a(Landroid/content/Context;Ljava/lang/String;I)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    .line 261
    iget-object v0, p0, Lcom/osp/app/signin/qb;->a:Lcom/osp/app/signin/WeiboInfoWebView;

    invoke-virtual {v0}, Lcom/osp/app/signin/WeiboInfoWebView;->getIntent()Landroid/content/Intent;

    move-result-object v0

    .line 262
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v1, "WIWV"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "weibo processJson() - failed to get weibo info from "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 263
    iget-object v1, p0, Lcom/osp/app/signin/qb;->a:Lcom/osp/app/signin/WeiboInfoWebView;

    invoke-virtual {v1, v5, v0}, Lcom/osp/app/signin/WeiboInfoWebView;->a(ILandroid/content/Intent;)V

    .line 264
    iget-object v0, p0, Lcom/osp/app/signin/qb;->a:Lcom/osp/app/signin/WeiboInfoWebView;

    invoke-virtual {v0}, Lcom/osp/app/signin/WeiboInfoWebView;->finish()V

    goto/16 :goto_1
.end method
