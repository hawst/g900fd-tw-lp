.class final Lcom/osp/app/signin/nl;
.super Lcom/msc/c/b;
.source "SmsVerificationActivity.java"


# instance fields
.field final synthetic c:Lcom/osp/app/signin/SmsVerificationActivity;


# direct methods
.method public constructor <init>(Lcom/osp/app/signin/SmsVerificationActivity;)V
    .locals 0

    .prologue
    .line 556
    iput-object p1, p0, Lcom/osp/app/signin/nl;->c:Lcom/osp/app/signin/SmsVerificationActivity;

    .line 557
    invoke-direct {p0, p1}, Lcom/msc/c/b;-><init>(Landroid/content/Context;)V

    .line 559
    return-void
.end method


# virtual methods
.method protected final varargs a()Ljava/lang/Boolean;
    .locals 5

    .prologue
    .line 573
    iget-object v0, p0, Lcom/osp/app/signin/nl;->c:Lcom/osp/app/signin/SmsVerificationActivity;

    invoke-static {v0}, Lcom/osp/app/signin/SmsVerificationActivity;->h(Lcom/osp/app/signin/SmsVerificationActivity;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-static {}, Lcom/osp/common/util/i;->a()Lcom/osp/common/util/i;

    iget-object v0, p0, Lcom/osp/app/signin/nl;->c:Lcom/osp/app/signin/SmsVerificationActivity;

    iget-object v0, p0, Lcom/osp/app/signin/nl;->c:Lcom/osp/app/signin/SmsVerificationActivity;

    invoke-static {v0}, Lcom/osp/app/signin/SmsVerificationActivity;->i(Lcom/osp/app/signin/SmsVerificationActivity;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/osp/common/util/i;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/osp/app/signin/nl;->c:Lcom/osp/app/signin/SmsVerificationActivity;

    invoke-static {v1}, Lcom/osp/app/signin/SmsVerificationActivity;->j(Lcom/osp/app/signin/SmsVerificationActivity;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_3

    if-eqz v0, :cond_0

    iget-object v1, p0, Lcom/osp/app/signin/nl;->c:Lcom/osp/app/signin/SmsVerificationActivity;

    invoke-static {v1}, Lcom/osp/app/signin/SmsVerificationActivity;->j(Lcom/osp/app/signin/SmsVerificationActivity;)Ljava/lang/String;

    move-result-object v1

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-static {}, Lcom/google/i18n/phonenumbers/h;->a()Lcom/google/i18n/phonenumbers/h;

    move-result-object v3

    sget-object v4, Ljava/util/Locale;->ENGLISH:Ljava/util/Locale;

    invoke-virtual {v0, v4}, Ljava/lang/String;->toUpperCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Lcom/google/i18n/phonenumbers/h;->d(Ljava/lang/String;)I

    move-result v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/osp/app/signin/nl;->c:Lcom/osp/app/signin/SmsVerificationActivity;

    invoke-static {v1, v0}, Lcom/osp/app/signin/SmsVerificationActivity;->c(Lcom/osp/app/signin/SmsVerificationActivity;Ljava/lang/String;)Ljava/lang/String;

    :cond_0
    :goto_0
    :try_start_0
    iget-object v0, p0, Lcom/osp/app/signin/nl;->c:Lcom/osp/app/signin/SmsVerificationActivity;

    const-string v1, "country_calling_code_list.xml"

    invoke-virtual {v0, v1}, Lcom/osp/app/signin/SmsVerificationActivity;->g(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {}, Lcom/msc/c/h;->a()Lcom/msc/c/h;

    invoke-static {v0}, Lcom/msc/c/h;->d(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v0

    iget-object v1, p0, Lcom/osp/app/signin/nl;->c:Lcom/osp/app/signin/SmsVerificationActivity;

    invoke-static {v1}, Lcom/osp/app/signin/SmsVerificationActivity;->h(Lcom/osp/app/signin/SmsVerificationActivity;)Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_4

    if-eqz v0, :cond_4

    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/osp/app/countrylist/CountryInfoItem;

    iget-object v2, p0, Lcom/osp/app/signin/nl;->c:Lcom/osp/app/signin/SmsVerificationActivity;

    invoke-static {v2}, Lcom/osp/app/signin/SmsVerificationActivity;->h(Lcom/osp/app/signin/SmsVerificationActivity;)Ljava/lang/String;

    move-result-object v2

    if-eqz v2, :cond_1

    iget-object v2, p0, Lcom/osp/app/signin/nl;->c:Lcom/osp/app/signin/SmsVerificationActivity;

    invoke-static {v2}, Lcom/osp/app/signin/SmsVerificationActivity;->h(Lcom/osp/app/signin/SmsVerificationActivity;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0}, Lcom/osp/app/countrylist/CountryInfoItem;->f()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_1

    iget-object v1, p0, Lcom/osp/app/signin/nl;->c:Lcom/osp/app/signin/SmsVerificationActivity;

    invoke-virtual {v0}, Lcom/osp/app/countrylist/CountryInfoItem;->b()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/osp/app/signin/SmsVerificationActivity;->d(Lcom/osp/app/signin/SmsVerificationActivity;Ljava/lang/String;)Ljava/lang/String;

    iget-object v1, p0, Lcom/osp/app/signin/nl;->c:Lcom/osp/app/signin/SmsVerificationActivity;

    invoke-virtual {v0}, Lcom/osp/app/countrylist/CountryInfoItem;->e()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Lcom/osp/app/signin/SmsVerificationActivity;->e(Lcom/osp/app/signin/SmsVerificationActivity;Ljava/lang/String;)Ljava/lang/String;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 575
    :cond_2
    :goto_1
    const/4 v0, 0x0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    return-object v0

    .line 573
    :cond_3
    iget-object v1, p0, Lcom/osp/app/signin/nl;->c:Lcom/osp/app/signin/SmsVerificationActivity;

    invoke-static {v1, v0}, Lcom/osp/app/signin/SmsVerificationActivity;->c(Lcom/osp/app/signin/SmsVerificationActivity;Ljava/lang/String;)Ljava/lang/String;

    goto :goto_0

    :cond_4
    if-eqz v0, :cond_2

    :try_start_1
    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_5
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/osp/app/countrylist/CountryInfoItem;

    iget-object v2, p0, Lcom/osp/app/signin/nl;->c:Lcom/osp/app/signin/SmsVerificationActivity;

    invoke-static {v2}, Lcom/osp/app/signin/SmsVerificationActivity;->j(Lcom/osp/app/signin/SmsVerificationActivity;)Ljava/lang/String;

    move-result-object v2

    if-eqz v2, :cond_5

    iget-object v2, p0, Lcom/osp/app/signin/nl;->c:Lcom/osp/app/signin/SmsVerificationActivity;

    invoke-static {v2}, Lcom/osp/app/signin/SmsVerificationActivity;->j(Lcom/osp/app/signin/SmsVerificationActivity;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0}, Lcom/osp/app/countrylist/CountryInfoItem;->e()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_5

    iget-object v1, p0, Lcom/osp/app/signin/nl;->c:Lcom/osp/app/signin/SmsVerificationActivity;

    invoke-virtual {v0}, Lcom/osp/app/countrylist/CountryInfoItem;->b()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/osp/app/signin/SmsVerificationActivity;->d(Lcom/osp/app/signin/SmsVerificationActivity;Ljava/lang/String;)Ljava/lang/String;

    iget-object v1, p0, Lcom/osp/app/signin/nl;->c:Lcom/osp/app/signin/SmsVerificationActivity;

    invoke-virtual {v0}, Lcom/osp/app/countrylist/CountryInfoItem;->f()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Lcom/osp/app/signin/SmsVerificationActivity;->c(Lcom/osp/app/signin/SmsVerificationActivity;Ljava/lang/String;)Ljava/lang/String;
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_1

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_1
.end method

.method protected final a(Ljava/lang/Boolean;)V
    .locals 1

    .prologue
    .line 564
    iget-object v0, p0, Lcom/osp/app/signin/nl;->c:Lcom/osp/app/signin/SmsVerificationActivity;

    invoke-static {v0}, Lcom/osp/app/signin/SmsVerificationActivity;->e(Lcom/osp/app/signin/SmsVerificationActivity;)V

    .line 565
    iget-object v0, p0, Lcom/osp/app/signin/nl;->c:Lcom/osp/app/signin/SmsVerificationActivity;

    invoke-static {v0}, Lcom/osp/app/signin/SmsVerificationActivity;->f(Lcom/osp/app/signin/SmsVerificationActivity;)V

    .line 567
    invoke-super {p0, p1}, Lcom/msc/c/b;->a(Ljava/lang/Boolean;)V

    .line 568
    return-void
.end method

.method protected final d()V
    .locals 3

    .prologue
    .line 580
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    const-string v0, "SMSV"

    const-string v1, "Cancel PreCountryListTask"

    invoke-static {v0, v1}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 581
    invoke-super {p0}, Lcom/msc/c/b;->d()V

    .line 582
    iget-object v0, p0, Lcom/osp/app/signin/nl;->c:Lcom/osp/app/signin/SmsVerificationActivity;

    invoke-static {v0}, Lcom/osp/app/signin/SmsVerificationActivity;->g(Lcom/osp/app/signin/SmsVerificationActivity;)Landroid/content/Intent;

    move-result-object v0

    const-string v1, "is_cancelable_just_one_activity"

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 584
    iget-object v0, p0, Lcom/osp/app/signin/nl;->c:Lcom/osp/app/signin/SmsVerificationActivity;

    const/16 v1, 0xe

    invoke-virtual {v0, v1}, Lcom/osp/app/signin/SmsVerificationActivity;->b(I)V

    .line 586
    :cond_0
    iget-object v0, p0, Lcom/osp/app/signin/nl;->c:Lcom/osp/app/signin/SmsVerificationActivity;

    invoke-virtual {v0}, Lcom/osp/app/signin/SmsVerificationActivity;->finish()V

    .line 587
    return-void
.end method

.method protected final synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 551
    invoke-virtual {p0}, Lcom/osp/app/signin/nl;->a()Ljava/lang/Boolean;

    move-result-object v0

    return-object v0
.end method

.method protected final synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 551
    check-cast p1, Ljava/lang/Boolean;

    invoke-virtual {p0, p1}, Lcom/osp/app/signin/nl;->a(Ljava/lang/Boolean;)V

    return-void
.end method
