.class final Lcom/osp/app/signin/dv;
.super Lcom/msc/c/b;
.source "CheckSecurityInfoActivity.java"


# instance fields
.field final synthetic c:Lcom/osp/app/signin/CheckSecurityInfoActivity;

.field private d:I

.field private e:J

.field private f:J


# direct methods
.method public constructor <init>(Lcom/osp/app/signin/CheckSecurityInfoActivity;)V
    .locals 1

    .prologue
    .line 1053
    iput-object p1, p0, Lcom/osp/app/signin/dv;->c:Lcom/osp/app/signin/CheckSecurityInfoActivity;

    .line 1055
    invoke-direct {p0, p1}, Lcom/msc/c/b;-><init>(Landroid/content/Context;)V

    .line 1057
    const/4 v0, 0x0

    iput v0, p0, Lcom/osp/app/signin/dv;->d:I

    .line 1059
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    invoke-static {p1, v0}, Lcom/osp/app/signin/CheckSecurityInfoActivity;->a(Lcom/osp/app/signin/CheckSecurityInfoActivity;Ljava/util/ArrayList;)Ljava/util/ArrayList;

    .line 1061
    return-void
.end method

.method private b(Ljava/lang/String;)V
    .locals 3

    .prologue
    .line 1216
    invoke-static {}, Lcom/msc/c/g;->a()Lcom/msc/c/g;

    .line 1217
    iget-object v0, p0, Lcom/osp/app/signin/dv;->c:Lcom/osp/app/signin/CheckSecurityInfoActivity;

    invoke-static {v0, p1, p0}, Lcom/msc/c/g;->j(Landroid/content/Context;Ljava/lang/String;Lcom/msc/b/h;)J

    move-result-wide v0

    iput-wide v0, p0, Lcom/osp/app/signin/dv;->f:J

    .line 1218
    iget-wide v0, p0, Lcom/osp/app/signin/dv;->f:J

    invoke-virtual {p0, v0, v1}, Lcom/osp/app/signin/dv;->a(J)V

    .line 1219
    iget-wide v0, p0, Lcom/osp/app/signin/dv;->f:J

    const-string v2, "from_xml"

    invoke-virtual {p0, v0, v1, v2}, Lcom/osp/app/signin/dv;->a(JLjava/lang/String;)V

    .line 1220
    iget-wide v0, p0, Lcom/osp/app/signin/dv;->f:J

    sget-object v2, Lcom/msc/b/g;->b:Lcom/msc/b/g;

    invoke-static {v0, v1, v2}, Lcom/msc/c/g;->a(JLcom/msc/b/g;)V

    .line 1222
    return-void
.end method


# virtual methods
.method protected final varargs a()Ljava/lang/Boolean;
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 1073
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v0, "requestSecurityInfo : "

    invoke-direct {v1, v0}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/osp/app/signin/dv;->c:Lcom/osp/app/signin/CheckSecurityInfoActivity;

    invoke-static {v0}, Lcom/osp/app/signin/CheckSecurityInfoActivity;->f(Lcom/osp/app/signin/CheckSecurityInfoActivity;)Ljava/util/ArrayList;

    move-result-object v0

    iget v2, p0, Lcom/osp/app/signin/dv;->d:I

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Ljava/lang/String;

    aget-object v0, v0, v3

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;)V

    .line 1077
    iget-object v0, p0, Lcom/osp/app/signin/dv;->c:Lcom/osp/app/signin/CheckSecurityInfoActivity;

    invoke-static {v0}, Lcom/osp/app/signin/CheckSecurityInfoActivity;->f(Lcom/osp/app/signin/CheckSecurityInfoActivity;)Ljava/util/ArrayList;

    move-result-object v0

    iget v1, p0, Lcom/osp/app/signin/dv;->d:I

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Ljava/lang/String;

    aget-object v0, v0, v3

    invoke-direct {p0, v0}, Lcom/osp/app/signin/dv;->b(Ljava/lang/String;)V

    .line 1083
    invoke-static {v3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    return-object v0
.end method

.method public final declared-synchronized a(Lcom/msc/b/c;)V
    .locals 6

    .prologue
    .line 1112
    monitor-enter p0

    :try_start_0
    invoke-super {p0, p1}, Lcom/msc/c/b;->a(Lcom/msc/b/c;)V

    .line 1114
    invoke-virtual {p1}, Lcom/msc/b/c;->a()J

    move-result-wide v0

    .line 1115
    invoke-virtual {p1}, Lcom/msc/b/c;->e()Ljava/lang/String;

    move-result-object v2

    .line 1117
    iget-wide v4, p0, Lcom/osp/app/signin/dv;->e:J

    cmp-long v3, v0, v4

    if-eqz v3, :cond_0

    iget-wide v4, p0, Lcom/osp/app/signin/dv;->f:J
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    cmp-long v0, v0, v4

    if-nez v0, :cond_2

    .line 1125
    :cond_0
    :try_start_1
    invoke-static {}, Lcom/msc/c/h;->a()Lcom/msc/c/h;

    invoke-static {v2}, Lcom/msc/c/h;->H(Ljava/lang/String;)Lcom/msc/a/j;

    move-result-object v1

    .line 1131
    invoke-virtual {v1}, Lcom/msc/a/j;->a()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 1133
    iget-object v0, p0, Lcom/osp/app/signin/dv;->c:Lcom/osp/app/signin/CheckSecurityInfoActivity;

    invoke-static {v0}, Lcom/osp/app/signin/CheckSecurityInfoActivity;->f(Lcom/osp/app/signin/CheckSecurityInfoActivity;)Ljava/util/ArrayList;

    move-result-object v0

    iget v2, p0, Lcom/osp/app/signin/dv;->d:I

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Ljava/lang/String;

    const/4 v2, 0x0

    aget-object v2, v0, v2

    .line 1134
    iget-object v0, p0, Lcom/osp/app/signin/dv;->c:Lcom/osp/app/signin/CheckSecurityInfoActivity;

    invoke-static {v0}, Lcom/osp/app/signin/CheckSecurityInfoActivity;->f(Lcom/osp/app/signin/CheckSecurityInfoActivity;)Ljava/util/ArrayList;

    move-result-object v0

    iget v3, p0, Lcom/osp/app/signin/dv;->d:I

    invoke-virtual {v0, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Ljava/lang/String;

    const/4 v3, 0x1

    aget-object v0, v0, v3

    .line 1135
    iget-object v3, p0, Lcom/osp/app/signin/dv;->c:Lcom/osp/app/signin/CheckSecurityInfoActivity;

    invoke-static {v3}, Lcom/osp/app/signin/CheckSecurityInfoActivity;->c(Lcom/osp/app/signin/CheckSecurityInfoActivity;)Ljava/util/ArrayList;

    move-result-object v3

    new-instance v4, Lcom/osp/app/signin/dw;

    invoke-direct {v4}, Lcom/osp/app/signin/dw;-><init>()V

    invoke-virtual {v4, v2}, Lcom/osp/app/signin/dw;->a(Ljava/lang/String;)V

    invoke-virtual {v4, v0}, Lcom/osp/app/signin/dw;->c(Ljava/lang/String;)V

    invoke-virtual {v1}, Lcom/msc/a/j;->a()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v4, v0}, Lcom/osp/app/signin/dw;->b(Ljava/lang/String;)V

    invoke-virtual {v3, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1137
    :cond_1
    iget v0, p0, Lcom/osp/app/signin/dv;->d:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/osp/app/signin/dv;->d:I

    .line 1138
    iget-object v0, p0, Lcom/osp/app/signin/dv;->c:Lcom/osp/app/signin/CheckSecurityInfoActivity;

    invoke-static {v0}, Lcom/osp/app/signin/CheckSecurityInfoActivity;->f(Lcom/osp/app/signin/CheckSecurityInfoActivity;)Ljava/util/ArrayList;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    iget v1, p0, Lcom/osp/app/signin/dv;->d:I

    if-le v0, v1, :cond_2

    .line 1140
    invoke-static {}, Lcom/osp/app/util/aq;->a()Lcom/osp/app/util/aq;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v0, "requestSecurityInfo : "

    invoke-direct {v1, v0}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/osp/app/signin/dv;->c:Lcom/osp/app/signin/CheckSecurityInfoActivity;

    invoke-static {v0}, Lcom/osp/app/signin/CheckSecurityInfoActivity;->f(Lcom/osp/app/signin/CheckSecurityInfoActivity;)Ljava/util/ArrayList;

    move-result-object v0

    iget v2, p0, Lcom/osp/app/signin/dv;->d:I

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Ljava/lang/String;

    const/4 v2, 0x0

    aget-object v0, v0, v2

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/osp/app/util/aq;->a(Ljava/lang/String;)V

    .line 1143
    iget-object v0, p0, Lcom/osp/app/signin/dv;->c:Lcom/osp/app/signin/CheckSecurityInfoActivity;

    invoke-static {v0}, Lcom/osp/app/signin/CheckSecurityInfoActivity;->f(Lcom/osp/app/signin/CheckSecurityInfoActivity;)Ljava/util/ArrayList;

    move-result-object v0

    iget v1, p0, Lcom/osp/app/signin/dv;->d:I

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Ljava/lang/String;

    const/4 v1, 0x0

    aget-object v0, v0, v1

    invoke-direct {p0, v0}, Lcom/osp/app/signin/dv;->b(Ljava/lang/String;)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1156
    :cond_2
    :goto_0
    monitor-exit p0

    return-void

    .line 1150
    :catch_0
    move-exception v0

    .line 1152
    :try_start_2
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    .line 1153
    new-instance v1, Lcom/msc/c/f;

    invoke-direct {v1, v0}, Lcom/msc/c/f;-><init>(Ljava/lang/Exception;)V

    iput-object v1, p0, Lcom/osp/app/signin/dv;->b:Lcom/msc/c/f;
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0

    .line 1112
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method protected final a(Ljava/lang/Boolean;)V
    .locals 2

    .prologue
    .line 1088
    invoke-super {p0, p1}, Lcom/msc/c/b;->a(Ljava/lang/Boolean;)V

    .line 1090
    iget-object v0, p0, Lcom/osp/app/signin/dv;->c:Lcom/osp/app/signin/CheckSecurityInfoActivity;

    invoke-static {v0}, Lcom/osp/app/signin/CheckSecurityInfoActivity;->c(Lcom/osp/app/signin/CheckSecurityInfoActivity;)Ljava/util/ArrayList;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/osp/app/signin/dv;->c:Lcom/osp/app/signin/CheckSecurityInfoActivity;

    invoke-static {v0}, Lcom/osp/app/signin/CheckSecurityInfoActivity;->c(Lcom/osp/app/signin/CheckSecurityInfoActivity;)Ljava/util/ArrayList;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    iget-object v1, p0, Lcom/osp/app/signin/dv;->c:Lcom/osp/app/signin/CheckSecurityInfoActivity;

    invoke-static {v1}, Lcom/osp/app/signin/CheckSecurityInfoActivity;->f(Lcom/osp/app/signin/CheckSecurityInfoActivity;)Ljava/util/ArrayList;

    move-result-object v1

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    if-ne v0, v1, :cond_0

    .line 1092
    iget-object v0, p0, Lcom/osp/app/signin/dv;->c:Lcom/osp/app/signin/CheckSecurityInfoActivity;

    iget-object v1, p0, Lcom/osp/app/signin/dv;->c:Lcom/osp/app/signin/CheckSecurityInfoActivity;

    invoke-static {v1}, Lcom/osp/app/signin/CheckSecurityInfoActivity;->c(Lcom/osp/app/signin/CheckSecurityInfoActivity;)Ljava/util/ArrayList;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/osp/app/signin/CheckSecurityInfoActivity;->b(Lcom/osp/app/signin/CheckSecurityInfoActivity;Ljava/util/ArrayList;)V

    .line 1093
    iget-object v0, p0, Lcom/osp/app/signin/dv;->c:Lcom/osp/app/signin/CheckSecurityInfoActivity;

    invoke-static {v0}, Lcom/osp/app/signin/CheckSecurityInfoActivity;->g(Lcom/osp/app/signin/CheckSecurityInfoActivity;)V

    .line 1094
    iget-object v0, p0, Lcom/osp/app/signin/dv;->c:Lcom/osp/app/signin/CheckSecurityInfoActivity;

    const v1, 0x7f0c00af

    invoke-virtual {v0, v1}, Lcom/osp/app/signin/CheckSecurityInfoActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 1108
    :goto_0
    return-void

    .line 1105
    :cond_0
    iget-object v0, p0, Lcom/osp/app/signin/dv;->c:Lcom/osp/app/signin/CheckSecurityInfoActivity;

    const/16 v1, 0xe

    invoke-virtual {v0, v1}, Lcom/osp/app/signin/CheckSecurityInfoActivity;->b(I)V

    .line 1106
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/osp/app/signin/dv;->a(Z)V

    goto :goto_0
.end method

.method public final declared-synchronized b(Lcom/msc/b/c;)V
    .locals 4

    .prologue
    .line 1183
    monitor-enter p0

    :try_start_0
    invoke-super {p0, p1}, Lcom/msc/c/b;->b(Lcom/msc/b/c;)V

    .line 1185
    invoke-virtual {p1}, Lcom/msc/b/c;->a()J

    move-result-wide v0

    .line 1188
    iget-wide v2, p0, Lcom/osp/app/signin/dv;->e:J

    cmp-long v2, v0, v2

    if-eqz v2, :cond_0

    iget-wide v2, p0, Lcom/osp/app/signin/dv;->f:J

    cmp-long v0, v0, v2

    if-nez v0, :cond_1

    .line 1190
    :cond_0
    new-instance v0, Lcom/msc/c/f;

    invoke-direct {v0}, Lcom/msc/c/f;-><init>()V

    iput-object v0, p0, Lcom/osp/app/signin/dv;->b:Lcom/msc/c/f;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1192
    :cond_1
    monitor-exit p0

    return-void

    .line 1183
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method protected final d()V
    .locals 2

    .prologue
    .line 1065
    invoke-super {p0}, Lcom/msc/c/b;->d()V

    .line 1066
    iget-object v0, p0, Lcom/osp/app/signin/dv;->c:Lcom/osp/app/signin/CheckSecurityInfoActivity;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/osp/app/signin/CheckSecurityInfoActivity;->b(I)V

    .line 1067
    iget-object v0, p0, Lcom/osp/app/signin/dv;->c:Lcom/osp/app/signin/CheckSecurityInfoActivity;

    invoke-virtual {v0}, Lcom/osp/app/signin/CheckSecurityInfoActivity;->finish()V

    .line 1068
    return-void
.end method

.method protected final synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 1036
    invoke-virtual {p0}, Lcom/osp/app/signin/dv;->a()Ljava/lang/Boolean;

    move-result-object v0

    return-object v0
.end method

.method protected final synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 1036
    check-cast p1, Ljava/lang/Boolean;

    invoke-virtual {p0, p1}, Lcom/osp/app/signin/dv;->a(Ljava/lang/Boolean;)V

    return-void
.end method
