.class final Lcom/osp/app/signin/ib;
.super Ljava/lang/Object;
.source "SignInView.java"

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field final synthetic a:Lcom/osp/app/signin/ia;


# direct methods
.method constructor <init>(Lcom/osp/app/signin/ia;)V
    .locals 0

    .prologue
    .line 1747
    iput-object p1, p0, Lcom/osp/app/signin/ib;->a:Lcom/osp/app/signin/ia;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final run()V
    .locals 4

    .prologue
    .line 1751
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    .line 1752
    const-string v1, "com.osp.app.signin"

    const-string v2, "com.osp.app.signin.AccountinfoView"

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->setClassName(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 1753
    const/high16 v1, 0x10000000

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    .line 1754
    const-string v1, "client_id"

    iget-object v2, p0, Lcom/osp/app/signin/ib;->a:Lcom/osp/app/signin/ia;

    iget-object v2, v2, Lcom/osp/app/signin/ia;->a:Lcom/osp/app/signin/SignInView;

    invoke-static {v2}, Lcom/osp/app/signin/SignInView;->j(Lcom/osp/app/signin/SignInView;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 1755
    const-string v1, "client_secret"

    iget-object v2, p0, Lcom/osp/app/signin/ib;->a:Lcom/osp/app/signin/ia;

    iget-object v2, v2, Lcom/osp/app/signin/ia;->a:Lcom/osp/app/signin/SignInView;

    invoke-static {v2}, Lcom/osp/app/signin/SignInView;->k(Lcom/osp/app/signin/SignInView;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 1756
    const-string v1, "account_mode"

    iget-object v2, p0, Lcom/osp/app/signin/ib;->a:Lcom/osp/app/signin/ia;

    iget-object v2, v2, Lcom/osp/app/signin/ia;->a:Lcom/osp/app/signin/SignInView;

    invoke-static {v2}, Lcom/osp/app/signin/SignInView;->l(Lcom/osp/app/signin/SignInView;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 1757
    const-string v1, "service_name"

    iget-object v2, p0, Lcom/osp/app/signin/ib;->a:Lcom/osp/app/signin/ia;

    iget-object v2, v2, Lcom/osp/app/signin/ia;->a:Lcom/osp/app/signin/SignInView;

    invoke-static {v2}, Lcom/osp/app/signin/SignInView;->m(Lcom/osp/app/signin/SignInView;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 1758
    const-string v1, "OSP_VER"

    iget-object v2, p0, Lcom/osp/app/signin/ib;->a:Lcom/osp/app/signin/ia;

    iget-object v2, v2, Lcom/osp/app/signin/ia;->a:Lcom/osp/app/signin/SignInView;

    invoke-static {v2}, Lcom/osp/app/signin/SignInView;->n(Lcom/osp/app/signin/SignInView;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 1759
    const-string v1, "direct_modify"

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 1760
    const-string v1, "authcode"

    iget-object v2, p0, Lcom/osp/app/signin/ib;->a:Lcom/osp/app/signin/ia;

    iget-object v2, v2, Lcom/osp/app/signin/ia;->a:Lcom/osp/app/signin/SignInView;

    invoke-static {v2}, Lcom/osp/app/signin/SignInView;->o(Lcom/osp/app/signin/SignInView;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 1761
    const-string v1, "is_authorized_internal_action_for_accountinfo"

    const-string v2, "TIME_PATTERN"

    invoke-static {v2}, Lcom/osp/app/util/ad;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 1763
    const-string v1, "BG_mode"

    iget-object v2, p0, Lcom/osp/app/signin/ib;->a:Lcom/osp/app/signin/ia;

    iget-object v2, v2, Lcom/osp/app/signin/ia;->a:Lcom/osp/app/signin/SignInView;

    invoke-static {v2}, Lcom/osp/app/signin/SignInView;->i(Lcom/osp/app/signin/SignInView;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 1765
    const-string v1, "BG_WhoareU"

    iget-object v2, p0, Lcom/osp/app/signin/ib;->a:Lcom/osp/app/signin/ia;

    iget-object v2, v2, Lcom/osp/app/signin/ia;->a:Lcom/osp/app/signin/SignInView;

    invoke-static {v2}, Lcom/osp/app/signin/SignInView;->i(Lcom/osp/app/signin/SignInView;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 1766
    const-string v1, "mypackage"

    iget-object v2, p0, Lcom/osp/app/signin/ib;->a:Lcom/osp/app/signin/ia;

    iget-object v2, v2, Lcom/osp/app/signin/ia;->a:Lcom/osp/app/signin/SignInView;

    invoke-static {v2}, Lcom/osp/app/signin/SignInView;->p(Lcom/osp/app/signin/SignInView;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 1767
    const-string v1, "key_request_id"

    iget-object v2, p0, Lcom/osp/app/signin/ib;->a:Lcom/osp/app/signin/ia;

    iget-object v2, v2, Lcom/osp/app/signin/ia;->a:Lcom/osp/app/signin/SignInView;

    invoke-static {v2}, Lcom/osp/app/signin/SignInView;->q(Lcom/osp/app/signin/SignInView;)J

    move-result-wide v2

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;J)Landroid/content/Intent;

    .line 1772
    iget-object v1, p0, Lcom/osp/app/signin/ib;->a:Lcom/osp/app/signin/ia;

    iget-object v1, v1, Lcom/osp/app/signin/ia;->a:Lcom/osp/app/signin/SignInView;

    invoke-virtual {v1, v0}, Lcom/osp/app/signin/SignInView;->startActivity(Landroid/content/Intent;)V

    .line 1774
    :cond_0
    return-void
.end method
